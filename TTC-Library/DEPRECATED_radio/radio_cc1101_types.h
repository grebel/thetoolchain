#ifndef RADIO_CC1101_TYPES_H
#define RADIO_CC1101_TYPES_H

/*{ radio_cc1101_types.h ***************************************************

 This source is published under the GNU LESSER GENERAL PUBLIC LICENSE (LGPL).
 See file LEGAL.txt in this software package for details.

 Authors: <Sascha Poggemann>, <Gregor Rebel>

 Description: Structures, enums and defines required for radio transceiver cc1101.

}*/
//{ Defines/ TypeDefs ****************************************************

#define CC1101_SIZE_RX_FIFO 64
#define CC1101_FIFO_BYTES_AVAILABLE_MASK 0b1111

//Definitions for radio_cc1101_get_features
#define CC1101_MAX_Channels 255
#define CC1101_MAX_TX_LEVEL 255 //must be changed -> Datasheet
#define CC1101_MAX_RX_LEVEL 255 //must be changed -> Datasheet

//Define for the CC1101 Header size which is put infront of the Data
//#define CC1101_package_Header_size 1 -> defined in makefile

//#define cc1101_send_PacketID 1 //this feature was disabled to keep packet size as small as possible.

#define CC1101_PACKET_NOT_PREPARED 0
#define CC1101_PACKET_PREPARED 1

#define CC1101_FIFO_BYTES_AVAILABLE_MASK 0b1111

//Definitions for radio_cc1101_types_get_features
#define CC1101_MAX_Channels 255
#define CC1101_MAX_TX_LEVEL 255 //must be changed -> Datasheet
#define CC1101_MAX_RX_LEVEL 255 //must be changed -> Datasheet

//Define for the CC1101 Header size which is put infront of the Data
//#define CC1101_package_Header_size 1 -> defined in makefile

//#define cc1101_send_PacketID 1

#define CC1101_PACKET_NOT_PREPARED 0
#define CC1101_PACKET_PREPARED 1

#define CC1020_BUFFERSIZE	128
#define PREAMBLE_SIZE	    6
#define PREAMBLE		    0xAA
#define SYNCWORD_SIZE	    2
#define HDR_SIZE		    (sizeof (struct cc1020_header))
#define CRC_SIZE		    2
#define TAIL_SIZE		    2
#define	TAIL			    0xFA

#define RADIO_CC1101_SPI_TIMEOUT 100  // usecs
#define RADIO_CC1101_TRANSMIT_TIMEOUT 5000  // usecs

#define RADIO_CC1101_ASSERT_FALSE_CHIP_VERSION 1 //will assert if CHIP_VERSION does not match the TTC_RADIOxx_CC1101_SPI_CHIP_VERSION
//} Defines
//{ Includes *************************************************************

#include "ttc_basic_types.h"
#include "ttc_queue_types.h"
#include "ttc_spi_types.h"

//} Includes
//{ Structures/ Enums ****************************************************

typedef enum {         // cc1101_access_mode_e
    cam_burst_write,
    cam_burst_read,
    cam_single_write,
    cam_single_read
} cc1101_access_mode_e;
typedef enum {   // radio_cc1101_marcstate_e
    //    Value  State name  State (Figure 25, page 50)
    //    0 (0x00)  SLEEP  SLEEP
    //    1 (0x01)  IDLE  IDLE
    //    2 (0x02)  XOFF  XOFF
    //    3 (0x03)  VCOON_MC  MANCAL
    //    4 (0x04)  REGON_MC  MANCAL
    //    5 (0x05)  MANCAL  MANCAL
    //    6 (0x06)  VCOON  FS_WAKEUP
    //    7 (0x07)  REGON  FS_WAKEUP
    //    8 (0x08)  STARTCAL  CALIBRATE
    //    9 (0x09)  BWBOOST  SETTLING
    //    10 (0x0A)  FS_LOCK  SETTLING
    //    11 (0x0B)  IFADCON  SETTLING
    //    12 (0x0C)  ENDCAL  CALIBRATE
    //    13 (0x0D)  RX  RX
    //    14 (0x0E)  RX_END  RX
    //    15 (0x0F)  RX_RST  RX
    //    16 (0x10)  TXRX_SWITCH  TXRX_SETTLING
    //    17 (0x11)  RXFIFO_OVERFLOW  RXFIFO_OVERFLOW
    //    18 (0x12)  FSTXON  FSTXON
    //    19 (0x13)  TX  TX
    //    20 (0x14)  TX_END  TX
    //    21 (0x15)  RXTX_SWITCH  RXTX_SETTLING
    //    22 (0x16)  TXFIFO_UNDERFLOW  TXFIFO_UNDERFLOW

    //    Note: it is not possible to read back the SLEEP or XOFF state numbers
    //    because setting CSn low will make the chip enter the IDLE mode from the
    //    SLEEP or XOFF states.

    rcm_SLEEP            = 0,
    rcm_IDLE             = 1,
    rcm_XOFF             = 2,
    rcm_VCOON_MC         = 3,
    rcm_REGON_MC         = 4,
    rcm_MANCAL           = 5,
    rcm_VCOON            = 6,
    rcm_REGON            = 7,
    rcm_STARTCAL         = 8,
    rcm_BWBOOST          = 9,
    rcm_FS_LOCK          = 10,
    rcm_IFADCON          = 11,
    rcm_ENDCAL           = 12,
    rcm_RX               = 13,
    rcm_RX_END           = 14,
    rcm_RX_RST           = 15,
    rcm_TXRX_SWITCH      = 16,
    rcm_RXFIFO_OVERFLOW  = 17,
    rcm_FSTXON           = 18,
    rcm_TX               = 19,
    rcm_TX_END           = 20,
    rcm_RXTX_SWITCH      = 21,
    rcm_TXFIFO_UNDERFLOW = 22,
    rcm_UNKNOWN

} radio_cc1101_marcstate_e;
typedef enum {   // cc1101_RXOFF_Mode_e

    RXOFF_Mode_IDLE,
    RXOFF_Mode_FSTXON,
    RXOFF_Mode_TX,
    RXOFF_Mode_Stay_in_RX

} cc1101_RXOFF_Mode_e;
typedef enum {   // cc1101_TXOFF_Mode_e

    TXOFF_Mode_IDLE,
    TXOFF_Mode_FSTXON,
    TXOFF_Mode_Stay_in_TX,
    TXOFF_Mode_RX

} cc1101_TXOFF_Mode_e;

typedef struct  { // cc1020_header_t              packet header for cc1020
    u8_t  pad;
    u8_t  length;
} __attribute__((packed)) cc1020_header_t;
typedef struct {  // radio_cc1101_pa_table_config_t
    char patable_0;
    char patable_1;
    char patable_2;
    char patable_3;
    char patable_4;
    char patable_5;
    char patable_6;
    char patable_7;
} __attribute__((packed)) radio_cc1101_pa_table_config_t;
typedef struct {  // radio_cc1101_rf_config_t     RF settings for CC1101
    u8_t iocfg2;           // GDO2 Output Pin Configuration
    u8_t iocfg1;           // GDO1 Output Pin Configuration
    u8_t iocfg0;           // GDO0 Output Pin Configuration
    u8_t fifothr;          // RX FIFO and TX FIFO Thresholds
    u8_t sync1;            // Sync Word, High Byte
    u8_t sync0;            // Sync Word, Low Byte
    u8_t pktlen;           // Packet Length
    u8_t pktctrl1;         // Packet Automation Control
    u8_t pktctrl0;         // Packet Automation Control
    u8_t addr;             // Device Address
    u8_t channr;           // Channel Number
    u8_t fsctrl1;          // Frequency Synthesizer Control
    u8_t fsctrl0;          // Frequency Synthesizer Control
    u8_t freq2;            // Frequency Control Word, High Byte
    u8_t freq1;            // Frequency Control Word, Middle Byte
    u8_t freq0;            // Frequency Control Word, Low Byte
    u8_t mdmcfg4;          // Modem Configuration
    u8_t mdmcfg3;          // Modem Configuration
    u8_t mdmcfg2;          // Modem Configuration
    u8_t mdmcfg1;          // Modem Configuration
    u8_t mdmcfg0;          // Modem Configuration
    u8_t deviatn;          // Modem Deviation Setting
    u8_t mcsm2;            // Main Radio Control State Machine Configuration
    u8_t mcsm1;            // Main Radio Control State Machine Configuration
    u8_t mcsm0;            // Main Radio Control State Machine Configuration
    u8_t foccfg;           // Frequency Offset Compensation Configuration
    u8_t bscfg;            // Bit Synchronization Configuration
    u8_t agcctrl2;         // AGC Control
    u8_t agcctrl1;         // AGC Control
    u8_t agcctrl0;         // AGC Control
    u8_t worevt1;          // High Byte Event0 Timeout
    u8_t worevt0;          // Low Byte Event0 Timeout
    u8_t worctrl;          // Wake On Radio Control
    u8_t frend1;           // Front End RX Configuration
    u8_t frend0;           // Front End TX Configuration
    u8_t fscal3;           // Frequency Synthesizer Calibration
    u8_t fscal2;           // Frequency Synthesizer Calibration
    u8_t fscal1;           // Frequency Synthesizer Calibration
    u8_t fscal0;           // Frequency Synthesizer Calibration
    u8_t rcctrl1;          // RC Oscillator Configuration
    u8_t rcctrl0;          // RC Oscillator Configuration
    u8_t fstest;           // Frequency Synthesizer Calibration Control
    u8_t ptest;            // Production Test
    u8_t agctest;          // AGC Test
    u8_t test2;            // Various Test Settings
    u8_t test1;            // Various Test Settings
    u8_t test0;            // Various Test Settings
    u8_t partnum;          // Chip ID
    u8_t version;          // Chip ID
    u8_t freqest;          // Frequency Offset Estimate from Demodulator
    u8_t lqi;              // Demodulator Estimate for Link Quality
    u8_t rssi;             // Received Signal Strength Indication
    u8_t marcstate;        // Main Radio Control State Machine State
    u8_t wortime1;         // High Byte of WOR Time
    u8_t wortime0;         // Low Byte of WOR Time
    u8_t pktstatus;        // Current GDOx Status and Packet Status
    u8_t vco_vc_dac;       // Current Setting from PLL Calibration Module
    u8_t txbytes;          // Underflow and Number of Bytes
    u8_t rxbytes;          // Overflow and Number of Bytes
    u8_t rcctrl1_status;   // Last RC Oscillator Calibration Result
    u8_t rcctrl0_status;   // Last RC Oscillator Calibration Result
} __attribute__((packed)) radio_cc1101_rf_config_t;
typedef struct {  // radio_cc1101_types_driver_t  architecture dependend configuration data for cc1101 connected via SPI ToDo: SP - add comment for each element
    ttc_mutex_t* Lock;        // locks SPI bus transaction from concurrent access
    u8_t SPI_Index;              // logical index of spi device to use (1=TTC_SPI1, ...)

    ttc_spi_generic_t* SPI_Cfg;  // configuration of spi interface used to communicate with radio

    u8_t RadioIndex;             // logical index of radio device
    u8_t CurrentStatus;
    u8_t ChipID;                 // =0: chip-id as being read via SPI
    u8_t ChipVersion;            // =4: chip-version as being read via SPI

    u8_t SyncWord[2];

} __attribute__((__packed__)) radio_cc1101_driver_t;
typedef struct { // radio_cc1101_packet_t         stores packet to be sent to the TX FIFO or read from the RX FIFO

    u16_t Amount;              // contains the length of the data buffer
    u16_t BytesProcessed;      // Must be initialized with 0 will be incremented until complete buffer is transmitted/received
    u8_t  Address_Destination;
    u8_t  Address_Source;
    u8_t  PacketID;
    u8_t* Data;

} __attribute__((__packed__)) radio_cc1101_packet_t;

/* typedef struct {       // cc1101_configuration_t  -> radio_cc1101_driver_t
    radio_cc1101_rf_config_t RF_Config;
    char * txpower;
    char * rxpower;
    char * modulation;
} cc1101_configuration_t; */

//} Structures/ Enums

// radio_cc1101_driver_t is required by ttc_radio_types.h
#include "ttc_radio_types.h"

#endif //RADIO_CC1101_TYPES_H