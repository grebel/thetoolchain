#ifndef RADIO_CC1101_H
#define RADIO_CC1101_H

/*{ radio_cc1101.h ***************************************************

   This source is published under the GNU LESSER GENERAL PUBLIC LICENSE (LGPL).
   See file LEGAL.txt in this software package for details.

   Authors: <Sascha Poggemann>, <Gregor Rebel>

   This is an low-level driver implementation for a Texas Instruments CC1101 Tranceiver
   connected via SPI bus.

   See DEPRECATED_ttc_radio.h for a complete description of how to configure radios in TheToolChain.


   Required constants (n=1..5):

   TTC_RADIOn_SPI_INDEX            # index of spi bus interface to use (requires a configured TTC_SPIn as described in ttc_spi.h)
   TTC_RADIOn_PIN_SPI_NSS=PIN_Pxn  # pin used as spi slave-select
   TTC_RADIOn_PIN_GDO0=PIN_Pxn     # pin connected to GDO0-pin on cc1101
   TTC_RADIOn_PIN_GDO1=PIN_Pxn     # pin connected to GDO1-pin on cc1101
   TTC_RADIOn_PIN_GDO2=PIN_Pxn     # pin connected to GDO2-pin on cc1101

   optional:
   TTC_RADIOn_CC1101_SPI_CHIP_VERSION  sets value expected to be read from cc1101-register 0x31 (default = 0x04)


   Example makefile configuration:

   COMPILE_OPTS += -DTTC_RADIO1=trd_cc1101_spi             # select low-level driver to use
   COMPILE_OPTS += -DTTC_RADIO1_SPI_INDEX=1                # use first configured SPI bus (requires a configured TTC_SPI1 as described in ttc_spi.h)
   COMPILE_OPTS += -DTTC_RADIO1_PIN_SPI_NSS=PIN_PB4        # pin used as spi slave-select
   COMPILE_OPTS += -DTTC_RADIO1_PIN_GDO0=PIN_PC13          # pin connected to GDO0-pin on cc1101
   COMPILE_OPTS += -DTTC_RADIO1_PIN_GDO1=PIN_PC2           # pin connected to GDO1-pin on cc1101
   COMPILE_OPTS += -DTTC_RADIO1_PIN_GDO2=PIN_PC8           # pin connected to GDO2-pin on cc1101
   COMPILE_OPTS += -DTTC_RADIOn_CC1101_SPI_CHIP_VERSION=4  # sets value expected to be read from cc1101-register 0x31 (default = 0x04)

}*/
//{ Defines/ TypeDefs ****************************************************

//} Defines
//{ Includes *************************************************************

#include "ttc_basic.h"
#include "ttc_memory.h"
#include "ttc_gpio.h"
#include "ttc_usart.h"
#include "radio_cc1101_types.h"
#include "radio_cc1101_registers.h"
#include "radio_cc1101_spi.h"
#include "ttc_radio_types.h"

//} Includes
//{ Function prototypes **************************************************

/** fills given structure with default configuration for indexed radio
 * @param RadioIndex     logical index of radio device to use (1 = TTC_RADIO1, ...)
 * @param Radio_Generic  structure must be allocated by caller
 */
void radio_cc1101_get_defaults(u8_t RadioIndex, ttc_radio_generic_t* Radio_Generic);

/** fills given structure with default configuration for indexed radio
 * @param RadioIndex     logical index of radio device to use (1 = TTC_RADIO1, ...)
 * @param Radio_Generic  structure must be allocated by caller
 */
void radio_cc1101_get_features(u8_t RadioIndex, ttc_radio_generic_t* Radio_Generic);

/** checks if radio transceiver still runs in correct operating mode
 * Note: Modes trm_Sleep, trm_PowerDown cannot be checked
 *
 * @param Radio_Generic   pointer to configuration of already initialized radio
 * @return  == 0: radio runs in expected operating mode
 *          != 0: radio is in unexpected operating mode (use radio_cc1101_set_operating_mode() to fix)
 */
BOOL radio_cc1101_check_mode(ttc_radio_generic_t* Radio_Generic);

/** initializes single radio according to given configuration data
 * @param Radio_Generic  structure being filled by radio_cc1101_get_defaults() earlier
 */
ttc_radio_errorcode_e radio_cc1101_init(ttc_radio_generic_t* Radio_Generic);

/** writes given configuration found in file radio_cc1101_smartrf.h via SPI interface into cc1101 registers
 * this function must be called to apply register values after radio power down or sleep mode.
 *
 * @param Radio_Generic  structure being filled by radio_cc1101_get_defaults() earlier
 * @return          = tse_Ok = 0: configuration applied successfully
 */
ttc_radio_errorcode_e radio_cc1101_init_SmartRF(ttc_radio_generic_t* Radio_Generic);

/** send data from given buffer to target radio of given address
 *
 * @param Radio_Generic  structure being filled by radio_cc1101_get_defaults() earlier
 * @param TargetAddress  protocol address of target node (255 = broadcast address)
 * @param Size           0..Radio_Generic->Max_Payload - amount of bytes to send from Data[]
 * @param Data           buffer storing payload data to send
 */
ttc_radio_errorcode_e radio_cc1101_packet_send(ttc_radio_generic_t* Radio_Generic, u8_t TargetAddress, u8_t Size, u8_t* Data);

/** read single received packet from rx-fifo of cc1101 radio
 *
 * @param Radio_Generic  structure being filled by radio_cc1101_get_defaults() earlier
 * @param Buffer         target buffer where to store payload data
 * @param MaxSize        >= Radio_Generic->Max_Payload - maximum allowed amount of bytes to store in Buffer[]
 * @param TargetAddress  will be loaded with target protocol address of received packet (255 = broadcast address)
 * @param RSSI           will be loaded with value of Receive Signal Strength Indicator
 * @param Data           buffer storing payload data to send
 * @return               amount of payload bytes written into Buffer[]
 */
u8_t radio_cc1101_packet_receive(ttc_radio_generic_t* Radio_Generic, u8_t* Buffer, u8_t MaxSize, u8_t* TargetAdress, u8_t* RSSI);

/** Returns index of SPI device to use for indexed radio
 *
 * @param  RadioIndex  >0: logical index of radio device (1=TTC_RADIO1, ...)
 * @return             >0: logical device index of SPI device to use (1=TTC_SPI1, ...)
 *                     =0: indexed radio not available
 */
u8_t radio_cc1101_get_spi_index(u8_t RadioIndex);

/** Set a new address for this radio device
 *
 * @param addr  new device address
 */
ttc_radio_errorcode_e radio_cc1101_set_device_address(ttc_radio_generic_t* Radio_Generic, u8_t NewDeviceAddress);

/** set output transmit power
 * @param Level  output power in dBm
 */
ttc_radio_errorcode_e radio_cc1101_set_power_tx(ttc_radio_generic_t* Radio_Generic, s8_t Level);

/** Set frequency channel to use for transmit/ receive
 *
 * @param chnl  Frequency channel
 */
ttc_radio_errorcode_e radio_cc1101_set_channel(ttc_radio_generic_t* Radio_Generic, Base_t NewChannelIndex);

ttc_radio_errorcode_e radio_cc1101_set_mode_txoff(ttc_radio_generic_t* Radio_Generic, cc1101_TXOFF_Mode_e cc1101_TXOFF_Mode);
ttc_radio_errorcode_e radio_cc1101_set_mode_rxoff(ttc_radio_generic_t* Radio_Generic, cc1101_RXOFF_Mode_e cc1101_RXOFF_Mode);

/** switches operating mode of transceiver to Radio_Generic->Mode
 *
 * Note: If you wanna change the mode, use ttc_radio_set_operating_mode(Radio_Generic->LogicalIndex, NewMode) instead!
 *
 * @param Radio_Generic  structure being filled by radio_cc1101_get_defaults() earlier
 * @return               == tre_OK: mode switched successfully
 */
ttc_radio_errorcode_e radio_cc1101_set_operating_mode(ttc_radio_generic_t* Radio_Generic);

/** delivers pointer to driver configuration for a cc1101 radio transceiver
 *
 * @param Radio_Generic  structure storing pointer to a radio driver configuration
 * @return               pointer of correct type (new struct will be allocated and Radio_Generic updated if pointer was NULL)
 */
radio_cc1101_driver_t* radio_cc1101_get_config(ttc_radio_generic_t* Radio_Generic);

/** reads current operating mode (MARCSTATE) from cc1101 transceiver
 *
 * Note: This function is private and should not be called from outside!
 *
 * @param Radio_Generic  structure storing pointer to a radio driver configuration
 * @return               value read from register MARCSTATE (0x35)
 */
u8_t _radio_cc1101_get_state(ttc_radio_generic_t* Radio_Generic);

/** interrupt service routine that will signal that a packet has been received
 *
 * @param PhysicalIndex   index of configured gpio pin as returned from ttc_gpio_create_index8()
 * @param Argument        ttc_radio_generic_t*
 */
void _radio_cc1101_isr_receive(physical_index_t PhysicalIndex, void* Argument);

/** tries to react to given error condition and leaves spi-bus in unlocked condition
 *
 * Note: This function is private and should not be called from outside!
 *
 * @param Radio_Generic      structure storing pointer to a radio driver configuration
 * @param Error              error condition to handle
 */
ttc_radio_errorcode_e _radio_cc1101_handle_error_unlock(ttc_radio_generic_t* Radio_Generic, ttc_radio_errorcode_e Error);

/** tries to react to given error condition and re-locks spi-bus
 *
 * Note: This function is private and should not be called from outside!
 *
 * @param Radio_Generic      structure storing pointer to a radio driver configuration
 * @param Error              error condition to handle
 */
ttc_radio_errorcode_e _radio_cc1101_handle_error_relock(ttc_radio_generic_t* Radio_Generic, ttc_radio_errorcode_e Error);

//} Function prototypes

#endif //cc1101_H
