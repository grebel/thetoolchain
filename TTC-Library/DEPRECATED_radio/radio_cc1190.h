#ifndef RADIO_CC1190_H
#define RADIO_CC1190_H

/*{ radio_cc1190.h ************************************************

 This source is published under the GNU LESSER GENERAL PUBLIC LICENSE (LGPL).
 See file LEGAL.txt in this software package for details.

 Authors: <Sascha Poggemann>, <Gregor Rebel>

 Description: Low-level driver for radio amplifier cc1190 from Texas Instruments.

}*/
//{ Defines/ TypeDefs ****************************************************

//} Defines
//{ Includes *************************************************************

#include "ttc_basic.h"
#include "ttc_gpio.h"
#include "ttc_radio_types.h"
#include "radio_amplifier_types.h"
#include "radio_cc1190_types.h"

//} Includes
//{ Function prototypes **************************************************

/* initialize connection to radio amplifier as configured externally
 *
 * @param Radio_Generic   filled out struct ttc_radio_generic_t (must be loaded via ttc_radio_get_defaults() before!)
 * @return                == 0: connection to amplifier has been initialized successfully; != 0: error-code
 */
ttc_radio_errorcode_e radio_cc1190_init(ttc_radio_generic_t* RadioGeneric);

/* activate/ deactivate high gain mode
 *
 * @param Radio_Generic   filled out struct ttc_radio_generic_t (must be loaded via ttc_radio_get_defaults() before!)
 * @param Active          == 0: deactivate high gain mode; != 0: activate high gain mode
 */
void radio_cc1190_set_high_gain(ttc_radio_generic_t* RadioGeneric, BOOL Active);

/* set amplifier operating mode
 *
 * @param Radio_Generic   filled out struct ttc_radio_generic_t (must be loaded via ttc_radio_get_defaults() before!)
 * @param Mode            new operating mode
 */
void radio_cc1190_set_mode(ttc_radio_generic_t* RadioGeneric, ttc_radio_amplifier_mode_e Mode);

//} Function prototypes

#endif //RADIO_CC1190_H
