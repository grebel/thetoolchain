/*{ radio_cc1101.c *****************************************************************

 Program Name: cc1101
 
 This source is published under the GNU LESSER GENERAL PUBLIC LICENSE (LGPL).
 See file LEGAL.txt in this software package for details.

 Authors: <Sascha Poggemann>, <Gregor Rebel>

 Description: This is an implementation for the Texas Instruments CC1101 Tranceiver
 
}*/
//{ Private Defines/ TypeDefs ****************************************************

//}  Private Defines
//{ Includes *********************************************************************

#include "radio_cc1101.h"

//} Includes
//{ Private Structures/ Enums ****************************************************

//} Private Structures/ Enums
//{ Global Variables *************************************************************

// required to be called from here
extern void ttc_radio_set_operating_mode(u8_t LogicalIndex, ttc_radio_mode_e Mode);

ttc_heap_array_define(ttc_mutex_smart_t*, TransactionLocks, TTC_AMOUNT_SPIS);

//} Global Variables
//{ Private Function prototypes **************************************************

#define   LockTransaction(SPI_Index, Owner)  while (ttc_mutex_smart_lock( A(TransactionLocks, SPI_Index-1), -1, ( void(*)() ) Owner) != tqe_OK)
#define UnlockTransaction(SPI_Index)         ttc_mutex_smart_unlock( A(TransactionLocks, SPI_Index-1) )
u8_t Amount_NonEmptyPackets = 0; //D

//} Private Function prototypes
//{ Implemented Functions ********************************************************

void radio_cc1101_get_defaults(u8_t RadioIndex, ttc_radio_generic_t* Radio_Generic) {
    Assert_Radio(Radio_Generic, ec_InvalidArgument);
    Radio_Generic->LogicalIndex = RadioIndex;
    radio_cc1101_get_config(Radio_Generic); // will create a driver configuration on first call

    // ToDo: SP - fill Radio_Generic with default values
    Radio_Generic->MaxChannelTx    = CC1101_MAX_Channels;
    Radio_Generic->MaxChannelRx    = CC1101_MAX_Channels;
    Radio_Generic->MaxLevelTx      = CC1101_MAX_TX_LEVEL;
    Radio_Generic->MaxLevelRx      = CC1101_MAX_RX_LEVEL;
    Radio_Generic->Max_Header      = RADIO_CC1101_HEADER_SIZE;  // 2
    Radio_Generic->Max_Footer      = RADIO_CC1101_FOOTER_SIZE;  // 0
    Radio_Generic->Max_Payload     = RADIO_CC1101_PAYLOAD_SIZE; // 60
    Radio_Generic->ChannelRx       = 1;
    Radio_Generic->ChannelTx       = 1;
    Radio_Generic->Size_QueueTx    = 5;
    Radio_Generic->Size_QueueRx    = 5;
    Radio_Generic->TargetAddress   = 0xff; // send to broadcast address
}
void radio_cc1101_get_features(u8_t RadioIndex, ttc_radio_generic_t* Radio_Generic) {
    radio_cc1101_get_config(Radio_Generic); // checks pointer too

    // fill in default values
    radio_cc1101_get_defaults(RadioIndex, Radio_Generic);

    // add max values
    Radio_Generic->Flags.Bit.SleepMode_IDLE = 1;
    Radio_Generic->Flags.Bit.SleepMode_OFF  = 1;
    Radio_Generic->ChannelTx                = CC1101_MAX_Channels;
    Radio_Generic->ChannelRx                = CC1101_MAX_Channels;
    Radio_Generic->LevelTx                  = CC1101_MAX_TX_LEVEL;
    Radio_Generic->LevelRx                  = CC1101_MAX_RX_LEVEL;
}
ttc_radio_errorcode_e radio_cc1101_init(ttc_radio_generic_t* Radio_Generic) {

    radio_cc1101_driver_t* DriverCfg = radio_cc1101_get_config(Radio_Generic); // checks pointer too
    ttc_radio_errorcode_e Error = tre_OK;
    u8_t State = 0;
    u8_t Level = 0;

    if (! A(TransactionLocks, DriverCfg->SPI_Index-1) )
          A(TransactionLocks, DriverCfg->SPI_Index-1) = ttc_mutex_smart_create();

    LockTransaction(DriverCfg->SPI_Index, radio_cc1101_init);

    // no SPI communication allowed before this point!
    radio_cc1101_spi_init(DriverCfg);

    if (!Error) { Level++; Error = radio_cc1101_spi_get_chip_id(DriverCfg); }
    if (!Error) { Level++; Error = radio_cc1101_spi_get_chip_version(DriverCfg);

        if(DriverCfg->ChipVersion != TTC_RADIO1_CC1101_SPI_CHIP_VERSION){ //wrong version read, check again before assert
            Assert_Radio(FALSE,ec_Debug);//D
           // return _radio_cc1101_handle_error_unlock(Radio_Generic,tre_Radio_Reset_Request);
        }
    }
    if (!Error) { Level++; Error = radio_cc1101_spi_reset(DriverCfg); }
    while ( (State = _radio_cc1101_get_state(Radio_Generic)) != rcm_IDLE)
        Radio_Generic->TaskYield();
    if (!Error) { Level++; Error = radio_cc1101_init_SmartRF(Radio_Generic); }

    // set RXOFF + TXOFF modes according to cc1101 datasheet p.81
    if (!Error) { Level++; Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_MCSM1, 0x3F); }

    switch (DriverCfg->SPI_Index) { // configure external interrupt line for incoming data
#ifdef TTC_RADIO1
    case 1: {

        // initialize interrupt for receiving data from radio
        ttc_interrupt_init(tit_GPIO_Rising,
                           ttc_gpio_create_index8(TTC_RADIO1_PIN_GDO2),
                           _radio_cc1101_isr_receive,
                           (void*) Radio_Generic,
                           NULL, NULL
                           );
        ttc_interrupt_enable(tit_GPIO_Rising,
                             ttc_gpio_create_index8(TTC_RADIO1_PIN_GDO2),
                             TRUE
                             );
        break;
    }
#endif
#ifdef TTC_RADIO2
    case 2: {

        // initialize interrupt for receiving data from radio
        ttc_interrupt_init(tit_GPIO_Rising,
                           ttc_gpio_create_index8(TTC_RADIO2_PIN_GDO2),
                           _radio_cc1101_isr_receive,
                           (void*) Radio_Generic,
                           NULL, NULL
                           );
        ttc_interrupt_enable(tit_GPIO_Rising,
                             ttc_gpio_create_index8(TTC_RADIO2_PIN_GDO2),
                             TRUE
                             );
        break;
    }
#endif
#ifdef TTC_RADIO3
    case 3: {

        // initialize interrupt for receiving data from radio
        ttc_interrupt_init(tit_GPIO_Rising,
                           ttc_gpio_create_index8(TTC_RADIO3_PIN_GDO2),
                           _radio_cc1101_isr_receive,
                           (void*) Radio_Generic,
                           NULL, NULL
                           );
        ttc_interrupt_enable(tit_GPIO_Rising,
                             ttc_gpio_create_index8(TTC_RADIO3_PIN_GDO2),
                             TRUE
                             );
        break;
    }
#endif
#ifdef TTC_RADIO4
    case 4: {

        // initialize interrupt for receiving data from radio
        ttc_interrupt_init(tit_GPIO_Rising,
                           ttc_gpio_create_index8(TTC_RADIO4_PIN_GDO2),
                           _radio_cc1101_isr_receive,
                           (void*) Radio_Generic,
                           NULL, NULL
                           );
        ttc_interrupt_enable(tit_GPIO_Rising,
                             ttc_gpio_create_index8(TTC_RADIO4_PIN_GDO2),
                             TRUE
                             );
        break;
    }
#endif
#ifdef TTC_RADIO5
    case 5: {

        // initialize interrupt for receiving data from radio
        ttc_interrupt_init(tit_GPIO_Rising,
                           ttc_gpio_create_index8(TTC_RADIO5_PIN_GDO2),
                           _radio_cc1101_isr_receive,
                           (void*) Radio_Generic,
                           NULL, NULL
                           );
        ttc_interrupt_enable(tit_GPIO_Rising,
                             ttc_gpio_create_index8(TTC_RADIO5_PIN_GDO2),
                             TRUE
                             );
        break;
    }
#endif
    default: Assert(0, ec_InvalidConfiguration); // indexed radio not activated
    }

    UnlockTransaction(DriverCfg->SPI_Index);

    // function calls below lock their own transaction
    ttc_radio_set_operating_mode(Radio_Generic->LogicalIndex, trm_Idle);
    if (!Error) { Level++; Error = radio_cc1101_set_power_tx(Radio_Generic, Radio_Generic->LevelTx); }
    ttc_radio_set_operating_mode(Radio_Generic->LogicalIndex, trm_Receive);
    Assert(Error != tre_TimeOut, ec_InvalidConfiguration); // communication error on SPI-bus (check variable Level)
    Assert(Error == tre_OK,      ec_UNKNOWN);              // unknown error (check variable Level)

    return tre_OK;
}
ttc_radio_errorcode_e radio_cc1101_init_SmartRF(ttc_radio_generic_t* Radio_Generic) {
    radio_cc1101_driver_t* DriverCfg = radio_cc1101_get_config(Radio_Generic); // checks pointer too

    // apply radio configuration
    ttc_radio_errorcode_e Error = tre_OK;
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_IOCFG2,   0x0f); //  0x0B; // GDO2 output pin config.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_IOCFG0,   0x09);//0x04); //  0x06; // GDO0 output pin config.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_FIFOTHR,   0x0F); //  0x06; // GDO0 output pin config.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_PKTLEN,   SMARTRF_SETTING_PKTLEN); //  0xFF; // Packet length.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_PKTCTRL1, 0x8); //0x0f); //  0x05; // Packet automation control.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_PKTCTRL0, SMARTRF_SETTING_PKTCTRL0); //  0x05; // Packet automation control.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_ADDR,     Radio_Generic->DeviceAddress); //  0x01; // Device address.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_CHANNR,   SMARTRF_SETTING_CHANNR); //  0x00; // Channel number.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_FSCTRL1,  SMARTRF_SETTING_FSCTRL1); //  0x0B; // Freq synthesizer control.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_FSCTRL0,  SMARTRF_SETTING_FSCTRL0); //  0x00; // Freq synthesizer control.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_FREQ2,    SMARTRF_SETTING_FREQ2); //  0x21; // Freq control word= high byte
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_FREQ1,    SMARTRF_SETTING_FREQ1); //  0x62; // Freq control word= mid byte.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_FREQ0,    SMARTRF_SETTING_FREQ0); //  0x76; // Freq control word= low byte.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_MDMCFG4,  SMARTRF_SETTING_MDMCFG4); //  0x2D; // Modem configuration.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_MDMCFG3,  SMARTRF_SETTING_MDMCFG3); //  0x3B; // Modem configuration.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_MDMCFG2,  SMARTRF_SETTING_MDMCFG2); //  0x73; // Modem configuration.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_MDMCFG1,  SMARTRF_SETTING_MDMCFG1); //  0x22; // Modem configuration.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_DEVIATN,  SMARTRF_SETTING_DEVIATN); //  0x00; // Modem dev (when FSK mod en)
    //set RXOFF + TXOFF modes according to cc1101 datasheet p.81 in register rcr_MCSM1
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_MDMCFG0,  SMARTRF_SETTING_MDMCFG0); //  0xF8; // Modem configuration.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_MCSM1,    0x3f);                  //  0x3F; // MainRadio Cntrl State Machine
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_MCSM0,    SMARTRF_SETTING_MCSM0); //  0x18; // MainRadio Cntrl State Machine
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_FOCCFG,   SMARTRF_SETTING_FOCCFG); //  0x1D; // Freq Offset Compens. Config
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_BSCFG,    SMARTRF_SETTING_BSCFG); //  0x1C; //  Bit synchronization config.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_AGCCTRL2, SMARTRF_SETTING_AGCCTRL2); //  0xC7; // AGC control.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_AGCCTRL1, SMARTRF_SETTING_AGCCTRL1); //  0x00; // AGC control.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_AGCCTRL0, SMARTRF_SETTING_AGCCTRL0); //  0xB2; // AGC control.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_FREND1,   SMARTRF_SETTING_FREND1); //  0xB6; // Front end RX configuration.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_FREND0,   SMARTRF_SETTING_FREND0); //  0x10; // Front end RX configuration.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_FSCAL3,   SMARTRF_SETTING_FSCAL3); //  0xEA; // Frequency synthesizer cal.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_FSCAL2,   SMARTRF_SETTING_FSCAL2); //  0x0A; // Frequency synthesizer cal.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_FSCAL1,   SMARTRF_SETTING_FSCAL1); //  0x00; // Frequency synthesizer cal.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_FSCAL0,   SMARTRF_SETTING_FSCAL0); //  0x11; // Frequency synthesizer cal.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_FSTEST,   SMARTRF_SETTING_FSTEST); //  0x59; // Frequency synthesizer cal.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_TEST2,    SMARTRF_SETTING_TEST2); //  0x88; // Various test settings.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_TEST1,    SMARTRF_SETTING_TEST1); //  0x31; // Various test settings.
    if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_TEST0,    SMARTRF_SETTING_TEST0); //  0x0B; // Various test settings.

    u8_t Compare = 0;
    if (!Error) Error = radio_cc1101_spi_register_access(DriverCfg, rcr_ADDR, &Compare, cam_single_read, 1); //  0x01; // Device address.
    Assert_Radio(Compare == SMARTRF_SETTING_ADDR, ec_DeviceNotFound); // written value could not be verified

    return Error;
}
ttc_radio_errorcode_e radio_cc1101_packet_send(ttc_radio_generic_t* Radio_Generic, u8_t TargetAddress, u8_t Size, u8_t* Data) {
    radio_cc1101_driver_t* DriverCfg = radio_cc1101_get_config(Radio_Generic); // checks pointer too
    Assert_Radio(Size, ec_InvalidArgument);
    Assert_Radio(Data, ec_InvalidArgument);

    ttc_radio_errorcode_e Error = tre_OK;
    Base_t StartTime =0;
    u16_t TimeTxPreparation = 0 ;

    Assert_Radio(Size, ec_InvalidArgument);
    Assert_Radio(Size <= RADIO_CC1101_PAYLOAD_SIZE, ec_InvalidArgument);

    LockTransaction(DriverCfg->SPI_Index, radio_cc1101_packet_send);

    if (1) { // transmit data via radio
        if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_TXFIFO, Size + 1);
        Assert_Radio(Error == tre_OK, ec_UNKNOWN);
        if (!Error) Error = radio_cc1101_spi_register_single_write(DriverCfg, rcr_TXFIFO, TargetAddress);
        Assert_Radio(Error == tre_OK, ec_UNKNOWN);
        if (!Error) Error = radio_cc1101_spi_register_access(DriverCfg, rcr_TXFIFO, Data, cam_burst_write, Size);
        Assert_Radio(Error == tre_OK, ec_UNKNOWN);

        //ttc_task_begin_criticalsection();

        if (!Error) Error = radio_cc1101_spi_strobe(DriverCfg, rcr_STX);
        Assert_Radio(Error == tre_OK, ec_UNKNOWN);

        u8_t TransmitState = 0;
        StartTime = ttc_task_get_elapsed_usecs();
        while ( (TransmitState = _radio_cc1101_get_state(Radio_Generic)) != rcm_TX) { //wait for radio to reach TX State
            switch (TransmitState) {
            case rcm_TXFIFO_UNDERFLOW:
                return _radio_cc1101_handle_error_unlock(Radio_Generic, tre_TxFIFO_Underrun);
            case rcm_RXFIFO_OVERFLOW:
                return _radio_cc1101_handle_error_unlock(Radio_Generic, tre_RxFIFO_Overrun);
            case rcm_IDLE:
                return _radio_cc1101_handle_error_unlock(Radio_Generic, tre_UnexpectedState);
            case rcm_RX:
                return _radio_cc1101_handle_error_unlock(Radio_Generic, tre_UnexpectedState);
            default :
                break;
            }

            if( (ttc_task_get_elapsed_usecs() - StartTime) > RADIO_CC1101_TRANSMIT_TIMEOUT )
                return _radio_cc1101_handle_error_unlock(Radio_Generic, tre_TX_Timeout);

            Radio_Generic->TaskYield();
        }
        TimeTxPreparation = ttc_task_get_elapsed_usecs() - StartTime;

        StartTime = ttc_task_get_elapsed_usecs();
        while ( (TransmitState = _radio_cc1101_get_state(Radio_Generic)) != rcm_RX) { //wait for radio to reach RX State ttc_task_get_elapsed_usecs() always returns the same value: 11130
            switch(TransmitState){
            case rcm_TXFIFO_UNDERFLOW:
                return _radio_cc1101_handle_error_unlock(Radio_Generic, tre_TxFIFO_Underrun);
            case rcm_RXFIFO_OVERFLOW:
                return _radio_cc1101_handle_error_unlock(Radio_Generic, tre_RxFIFO_Overrun);
            case rcm_IDLE:
                return _radio_cc1101_handle_error_unlock(Radio_Generic, tre_UnexpectedState);
            default :
                break;
            }
            if( (ttc_task_get_elapsed_usecs() - StartTime) > RADIO_CC1101_TRANSMIT_TIMEOUT )
                return _radio_cc1101_handle_error_unlock(Radio_Generic, tre_TX_Timeout);

            Radio_Generic->TaskYield();
        }
        //ttc_task_end_criticalsection();
#ifdef TTC_DEBUG_RADIO
        u16_t TimeTxMode = ttc_task_get_elapsed_usecs() - StartTime - TimeTxPreparation;

        if (Radio_Generic->Max_TimeTxMode < TimeTxMode)
            Radio_Generic->Max_TimeTxMode = TimeTxMode;
        if (Radio_Generic->Max_TimeTxPreparation < TimeTxPreparation)
            Radio_Generic->Max_TimeTxPreparation = TimeTxPreparation;
        Radio_Generic->Amount_Packets_Send ++;
#endif
    } //disable complete radio transmission

    UnlockTransaction(DriverCfg->SPI_Index);

    return Error;
}
                 u8_t radio_cc1101_packet_receive(ttc_radio_generic_t* Radio_Generic, u8_t* Buffer, u8_t MaxSize, u8_t* TargetAdress, u8_t* RSSI) {
    radio_cc1101_driver_t* DriverCfg = radio_cc1101_get_config(Radio_Generic); // checks pointer too
    ttc_radio_errorcode_e Error = tre_OK;
    Assert_Radio(RADIO_CC1101_PAYLOAD_SIZE <= MaxSize, ec_InvalidArgument); // Buffer[] is too small for this packet

    LockTransaction(DriverCfg->SPI_Index, radio_cc1101_packet_receive);

    u8_t RX_BytesPending = 0 ;
    if (!Error) Error = radio_cc1101_spi_register_access(DriverCfg, rcr_RXBYTES, &RX_BytesPending, cam_single_read, 1);
    u8_t PacketSize = 0;

    if (RX_BytesPending) {
        RX_BytesPending = RX_BytesPending& 0x7F; //RX_BytesPending contains MSB that indicates if RXFIFO is overflowed
        if (RX_BytesPending > CC1101_SIZE_RX_FIFO) { // invalid size: reread value via SPI
            //get bytes again, if a value > CC1101_SIZE_RX_FIFO is read from the RX Bytes Register.
            if (!Error) Error = radio_cc1101_spi_register_access(DriverCfg, rcr_RXBYTES, &RX_BytesPending, cam_single_read, 1); //D
            RX_BytesPending = RX_BytesPending& 0x7F;
        }
        if ( (RX_BytesPending > CC1101_SIZE_RX_FIFO) ||
             (RX_BytesPending > TTC_NETWORK_MAX_PAYLOAD_SIZE)
        ) { // invalid packet size: flush rx-buffer
            _radio_cc1101_handle_error_unlock(Radio_Generic, tre_RxFIFO_Overrun);
            return 0;
        }
        Amount_NonEmptyPackets++; //D
        if (!Error) Error = radio_cc1101_spi_register_access(DriverCfg, rcr_RXFIFO, &PacketSize,  cam_single_read, 1);
        if (!Error) Error = radio_cc1101_spi_register_access(DriverCfg, rcr_RXFIFO, TargetAdress, cam_single_read, 1);
        PacketSize--; //remove Target Address


        if (PacketSize > RADIO_CC1101_PAYLOAD_SIZE) { // invalid size: flush fifo
            PacketSize = 0;
            Error = tre_RxCorruptPacket;
        }

        u8_t Bytes2Read = PacketSize;
        while (Bytes2Read--) {
            if (!Error) *Buffer++ = radio_cc1101_spi_register_single_read(DriverCfg, rcr_RXFIFO);
        }
    }

    if (Error != tre_OK) {
        PacketSize = 0;
        _radio_cc1101_handle_error_unlock(Radio_Generic, Error); // unlocks transaction
    }
    else
        UnlockTransaction(DriverCfg->SPI_Index);
#ifdef TTC_DEBUG_RADIO
    Radio_Generic->Amount_Packets_Received++;
#endif
    return PacketSize;
}
ttc_radio_errorcode_e radio_cc1101_set_device_address(ttc_radio_generic_t* Radio_Generic, u8_t NewDeviceAddress) {
    radio_cc1101_driver_t* DriverCfg = radio_cc1101_get_config(Radio_Generic); // checks pointer too

    ttc_radio_errorcode_e Error = tre_OK;
    if (Radio_Generic->DeviceAddress != NewDeviceAddress)
    {
        LockTransaction(DriverCfg->SPI_Index, radio_cc1101_packet_receive);
        if (!Error) Error = radio_cc1101_spi_register_access(DriverCfg, rcr_ADDR, &NewDeviceAddress, cam_single_write, 1 );
        Radio_Generic->DeviceAddress = NewDeviceAddress;
        UnlockTransaction(DriverCfg->SPI_Index);
    }
    return Error;
}
ttc_radio_errorcode_e radio_cc1101_set_channel(ttc_radio_generic_t* Radio_Generic, u32_t NewChannelIndex) {
    radio_cc1101_driver_t* DriverCfg = radio_cc1101_get_config(Radio_Generic); // checks pointer too
    ttc_radio_errorcode_e Error = tre_OK;

    if (Radio_Generic->ChannelTx != NewChannelIndex)
    {
        LockTransaction(DriverCfg->SPI_Index, radio_cc1101_packet_receive);
        u8_t NewChannelIndex8 = (u8_t) 0xff & NewChannelIndex;
        if (!Error) Error = radio_cc1101_spi_register_access(DriverCfg, rcr_CHANNR, &NewChannelIndex8, cam_single_write, 1);
        if (!Error) {
            Radio_Generic->ChannelTx = NewChannelIndex;
            Radio_Generic->ChannelRx = NewChannelIndex;
        }
        UnlockTransaction(DriverCfg->SPI_Index);
    }
    return Error;
}
ttc_radio_errorcode_e radio_cc1101_set_power_tx(ttc_radio_generic_t* Radio_Generic, s8_t Level) {
    radio_cc1101_driver_t* DriverCfg = radio_cc1101_get_config(Radio_Generic); // checks pointer too

    LockTransaction(DriverCfg->SPI_Index, radio_cc1101_set_power_tx);

    // determine power-setting for 868MHz according to cc1101-datasheet p.59
    u8_t PowerSetting = 0;
    if (Level > 11) {
        Level = 10;
        PowerSetting = 0xc0;
    }
    else if (Level > 8) {
        Level = 10;
        PowerSetting = 0xc5;
    }
    else if (Level > 6) {
        Level = 7;
        PowerSetting = 0xcd;
    }
    else if (Level > 2) {
        Level = 5;
        PowerSetting = 0x86;
    }
    else if (Level > -3) {
        Level = 0;
        PowerSetting = 0x50;
    }
    else if (Level > -8) {
        Level = -6;
        PowerSetting = 0x37;
    }
    else if (Level > -13) {
        Level = -10;
        PowerSetting = 0x26;
    }
    else if (Level > -17) {
        Level = -15;
        PowerSetting = 0x1d;
    }
    else if (Level > -25) {
        Level = -20;
        PowerSetting = 0x17;
    }
    else {
        Level = -30;
        PowerSetting = 0x03;
    }

    Radio_Generic->LevelTx = Level;
    u8_t PA_Table[8] = { 0,0,0,0,0,0,0,0 };
    PA_Table[0] = PowerSetting;
    PA_Table[1] = PowerSetting;
    ttc_radio_errorcode_e Error = radio_cc1101_spi_register_access(DriverCfg, rcr_PATABLE, PA_Table, cam_burst_write, 8);

    UnlockTransaction(DriverCfg->SPI_Index);

    return Error;
}
ttc_radio_errorcode_e radio_cc1101_set_operating_mode(ttc_radio_generic_t* Radio_Generic) {
    radio_cc1101_driver_t* DriverCfg = radio_cc1101_get_config(Radio_Generic); // checks pointer too
    ttc_radio_errorcode_e Error = tre_OK;

    LockTransaction(DriverCfg->SPI_Index, radio_cc1101_set_operating_mode);
    radio_cc1101_marcstate_e CurrentMode = 0x1f & radio_cc1101_spi_register_single_read(DriverCfg, rcr_MARCSTATE);

    switch (CurrentMode) {
    case rcm_RXFIFO_OVERFLOW: {
        _radio_cc1101_handle_error_relock(Radio_Generic, tre_RxFIFO_Overrun);
        CurrentMode = 0x1f & radio_cc1101_spi_register_single_read(DriverCfg, rcr_MARCSTATE);
        break;
    }
    case rcm_TXFIFO_UNDERFLOW: {
        _radio_cc1101_handle_error_relock(Radio_Generic, tre_TxFIFO_Underrun);
        CurrentMode = 0x1f & radio_cc1101_spi_register_single_read(DriverCfg, rcr_MARCSTATE);
        break;
    }
    default: break;
    }
    u8_t SetModeCommand = 0;
    switch (Radio_Generic->Mode) {
    case trm_Idle: {
        SetModeCommand = rcr_SIDLE;
        break;
    }
    case trm_Reset: {
        SetModeCommand = rcr_SRES;
        break;
    }
    case trm_PowerOff:
    case trm_Sleep: {
        SetModeCommand = rcr_SPWD;
        break;
    }
    case trm_PowerOn: {
        if (!Error) Error = radio_cc1101_spi_strobe(DriverCfg, rcr_SNOP);
        if (!Error) Error = radio_cc1101_spi_strobe(DriverCfg, rcr_SNOP);
        if (!Error) Error = radio_cc1101_spi_strobe(DriverCfg, rcr_SNOP);

        // reload configuration
        if (!Error) Error = radio_cc1101_init(Radio_Generic);
    }
    case trm_Transmit: {
        Assert_Radio(NULL,ec_Debug);//D should not be called. This might cause TX_FIFO_UNDERFLOW
        SetModeCommand = rcr_STX;
        break;
    }
    case trm_Receive: {
        SetModeCommand = rcr_SRX;
        break;
    }
    case trm_TransmitReceive:
    default: {
            Assert_Radio(0, ec_InvalidArgument); // not supported by this radio
            break;
        }
    }
    if (SetModeCommand)
        radio_cc1101_spi_strobe(DriverCfg, SetModeCommand);

    UnlockTransaction(DriverCfg->SPI_Index);
    Base_t TimeOut = 10000;
    while (radio_cc1101_check_mode(Radio_Generic) && (TimeOut > 0)) {
        if (SetModeCommand) {
            LockTransaction(DriverCfg->SPI_Index, radio_cc1101_set_operating_mode);
            radio_cc1101_spi_strobe(DriverCfg, SetModeCommand);
            UnlockTransaction(DriverCfg->SPI_Index);
        }
        TimeOut--;
        ttc_task_yield();
    }
    if (TimeOut == 0)
        Error = tre_TimeOut;

    return Error;
}
                  BOOL radio_cc1101_check_mode(ttc_radio_generic_t* Radio_Generic) {
    radio_cc1101_driver_t* DriverCfg = radio_cc1101_get_config(Radio_Generic); // checks pointer too

    LockTransaction(DriverCfg->SPI_Index, radio_cc1101_check_mode);
    BOOL IllegalMode = FALSE;

    switch (Radio_Generic->Mode) {
    case trm_Idle: {
        if (radio_cc1101_spi_register_single_read(DriverCfg, rcr_MARCSTATE) != rcm_IDLE)
            IllegalMode = TRUE;
        break;
    }
    case trm_PowerOn: {
        radio_cc1101_marcstate_e CurrentMode = radio_cc1101_spi_register_single_read(DriverCfg, rcr_MARCSTATE);
        if ( (CurrentMode != rcm_IDLE) && (CurrentMode != rcm_TX) && (CurrentMode != rcm_RX) )
             IllegalMode = TRUE;
    }
    case trm_Transmit: {
        radio_cc1101_marcstate_e CurrentMode = radio_cc1101_spi_register_single_read(DriverCfg, rcr_MARCSTATE);
        if (CurrentMode != rcm_TX)
             IllegalMode = TRUE;
        break;
    }
    case trm_Receive: {
        radio_cc1101_marcstate_e CurrentMode = radio_cc1101_spi_register_single_read(DriverCfg, rcr_MARCSTATE);
        if (CurrentMode != rcm_RX)
             IllegalMode = TRUE;
        break;
    }
    default: break;
    }
    UnlockTransaction(DriverCfg->SPI_Index);

    return IllegalMode;
}
                  u8_t radio_cc1101_get_spi_index(u8_t RadioIndex) {
    Assert_Radio(RadioIndex > 0, ec_InvalidArgument); // logical index starts at 1

    switch (RadioIndex) {
#ifdef TTC_RADIO1_SPI_INDEX
    case 1: return TTC_RADIO1_SPI_INDEX;
#endif
#ifdef TTC_RADIO2_SPI_INDEX
    case 2: return TTC_RADIO2_SPI_INDEX;
#endif
#ifdef TTC_RADIO3_SPI_INDEX
    case 3: return TTC_RADIO3_SPI_INDEX;
#endif
#ifdef TTC_RADIO4_SPI_INDEX
    case 4: return TTC_RADIO4_SPI_INDEX;
#endif
#ifdef TTC_RADIO5_SPI_INDEX
    case 5: return TTC_RADIO5_SPI_INDEX;
#endif
    default: break;
    }
    Assert_Radio(0, ec_InvalidConfiguration); // indexed radio is not activated!
    return 0;
}
                  u8_t ISR_Call_DEBUG_VALUE = 0; //D
radio_cc1101_driver_t* radio_cc1101_get_config(ttc_radio_generic_t* Radio_Generic) {
    Assert_Radio(Radio_Generic, ec_InvalidArgument);

    radio_cc1101_driver_t* DriverCfg = (radio_cc1101_driver_t*) Radio_Generic->DriverCfg;
    if (!DriverCfg) {
        DriverCfg = (radio_cc1101_driver_t*) ttc_heap_alloc_zeroed( sizeof(radio_cc1101_driver_t) );
        Radio_Generic->DriverCfg = (ttc_radio_driver_t*) DriverCfg;
        DriverCfg->SPI_Index = radio_cc1101_get_spi_index(Radio_Generic->LogicalIndex);

        // ToDo: SP - fill out default driver configuration
        DriverCfg->RadioIndex = Radio_Generic->LogicalIndex;
        DriverCfg->Lock = ttc_mutex_create();
    }
    return DriverCfg;
}
                 void _radio_cc1101_isr_receive(physical_index_t PhysicalIndex, void* Argument) {
    (void) PhysicalIndex;
    Assert_Radio(Argument, ec_InvalidArgument);
#ifdef TTC_DEBUG_RADIO
    //ttc_gpio_set(TTC_DEBUG_RADIO_PIN2);
    ISR_Call_DEBUG_VALUE = 1 - ISR_Call_DEBUG_VALUE; //D
    //? ttc_gpio_setn(TTC_DEBUG_RADIO_PIN2,ISR_Call_DEBUG_VALUE);//D
#endif
    ttc_radio_generic_t* Radio_Generic = (ttc_radio_generic_t*) Argument;
    if (Radio_Generic->MutexPacketReceived)
        ttc_mutex_unlock(Radio_Generic->MutexPacketReceived);

#ifdef TTC_DEBUG_RADIO
    //ttc_gpio_clr(TTC_DEBUG_RADIO_PIN2);
#endif
}
                 u8_t _radio_cc1101_get_state(ttc_radio_generic_t* Radio_Generic) {
    Assert_Radio(Radio_Generic, ec_InvalidArgument);
    radio_cc1101_driver_t* DriverCfg = (radio_cc1101_driver_t*) Radio_Generic->DriverCfg;

    u8_t MarcState;
    ttc_radio_errorcode_e Error = radio_cc1101_spi_register_access(DriverCfg,
                                                                   rcr_MARCSTATE,
                                                                   &MarcState,
                                                                   cam_single_read,
                                                                   1
                                                                   );
    Assert_Radio(Error == tre_OK, ec_UNKNOWN);

    return MarcState;
}
ttc_radio_errorcode_e _radio_cc1101_handle_error_unlock(ttc_radio_generic_t* Radio_Generic, ttc_radio_errorcode_e Error) {
    radio_cc1101_driver_t* DriverCfg = radio_cc1101_get_config(Radio_Generic); // checks pointer too

    ttc_radio_errorcode_e ReturnCode = tre_OK;
    BOOL ReInitRadio = FALSE;

#ifdef TTC_DEBUG_RADIO
    Radio_Generic->Amount_Error_Handler_Calls++;
    Assert_Radio(Radio_Generic->Amount_Error_Handler_Calls<5000, ec_Debug);//D
#endif
    switch (Error) {
    case tre_TX_Timeout: {
#ifdef TTC_DEBUG_RADIO
        Radio_Generic->Amount_Error_TX_Timeout++;
        Assert_Radio(Radio_Generic->Amount_Error_TX_Timeout<50, ec_Debug);//D
#endif
        u8_t State = _radio_cc1101_get_state(Radio_Generic);
        if (State == rcm_RX) { // assume that we missed the TX State because radio is allready back in RX State
#ifdef TTC_DEBUG_RADIO
            Radio_Generic->Amount_Error_Missed_TXStates++; //D
#endif
            ReturnCode = tre_OK;
        }
        else
            ReInitRadio = TRUE;

        break;
    }
    case tre_RxCorruptPacket: {
#ifdef TTC_DEBUG_RADIO
        Radio_Generic->Amount_Error_RxCorruptPacket++;
#endif
        radio_cc1101_spi_strobe(DriverCfg, rcr_SFRX);
        radio_cc1101_spi_strobe(DriverCfg, rcr_SRX);

        break;
    }
    case tre_UnexpectedState: {
#ifdef TTC_DEBUG_RADIO
        Radio_Generic->Amount_Error_UnexpectedState++;
        Assert_Radio(Radio_Generic->Amount_Error_UnexpectedState<50, ec_Debug);//D
#endif
        u8_t CurrentState = _radio_cc1101_get_state(Radio_Generic) ;
        if(CurrentState == rcm_TXFIFO_UNDERFLOW){
#ifdef TTC_DEBUG_RADIO
            Radio_Generic->Amount_Error_TXFIFO_UNDERFLOW++; //D
#endif
            radio_cc1101_spi_strobe(DriverCfg, rcr_SFTX);
        }
        else if(CurrentState == rcm_RXFIFO_OVERFLOW){
#ifdef TTC_DEBUG_RADIO
            Radio_Generic->Amount_Error_RXFIFO_OVERFLOW++; //D
#endif
            radio_cc1101_spi_strobe(DriverCfg, rcr_SFRX);
        }
        radio_cc1101_spi_strobe(DriverCfg, rcr_SRX);

        break;
    }
    case tre_RxFIFO_Overrun: {
        radio_cc1101_spi_strobe(DriverCfg, rcr_SFRX);
        radio_cc1101_spi_strobe(DriverCfg, rcr_SIDLE);
        radio_cc1101_spi_strobe(DriverCfg, rcr_SRX);

#ifdef TTC_DEBUG_RADIO
        Radio_Generic->Amount_Error_RXFIFO_OVERFLOW++;
#endif

        break;
    }
    case tre_TxFIFO_Underrun: {
        radio_cc1101_spi_strobe(DriverCfg, rcr_SFTX);
        radio_cc1101_spi_strobe(DriverCfg, rcr_SIDLE);
        radio_cc1101_spi_strobe(DriverCfg, rcr_SRX);

#ifdef TTC_DEBUG_RADIO
        Radio_Generic->Amount_Error_RXFIFO_OVERFLOW++;
#endif

        break;
    }
    case tre_Radio_Reset_Request: {
        ReInitRadio = TRUE;
    }
    default:
        ReInitRadio = TRUE;
#ifdef TTC_DEBUG_RADIO
        Radio_Generic->Amount_Error_Unknown++;
#endif
        break;
    }

    if (ReInitRadio) {
#ifdef TTC_DEBUG_RADIO
        Radio_Generic->Amount_Error_Radio_Resets++;
#endif
        radio_cc1101_spi_strobe(DriverCfg, rcr_SFTX);
        radio_cc1101_spi_strobe(DriverCfg, rcr_SFRX);
        radio_cc1101_spi_strobe(DriverCfg, rcr_SIDLE);
        // ensure that spi communications are allowed
        UnlockTransaction(DriverCfg->SPI_Index);

        ReturnCode = radio_cc1101_init(Radio_Generic);

        LockTransaction(DriverCfg->SPI_Index, _radio_cc1101_handle_error_unlock);
        radio_cc1101_spi_strobe(DriverCfg, rcr_SRX);
    }
    Assert_Radio(ReturnCode == tre_OK, ec_UNKNOWN);

    // ensure that spi communications are allowed
    UnlockTransaction(DriverCfg->SPI_Index);

    ttc_radio_set_operating_mode(Radio_Generic->LogicalIndex, Radio_Generic->Mode); //switch radio back to its configured Mode

    return ReturnCode;
}
ttc_radio_errorcode_e _radio_cc1101_handle_error_relock(ttc_radio_generic_t* Radio_Generic, ttc_radio_errorcode_e Error) {
    radio_cc1101_driver_t* DriverCfg = radio_cc1101_get_config(Radio_Generic); // checks pointer too
    ttc_radio_errorcode_e ErrorCode = _radio_cc1101_handle_error_unlock(Radio_Generic, Error);
    LockTransaction(DriverCfg->SPI_Index, _radio_cc1101_handle_error_relock);

    return ErrorCode;
}

//} Implemented Functions
