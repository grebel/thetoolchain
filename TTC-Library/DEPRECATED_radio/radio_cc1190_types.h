#ifndef RADIO_CC190_TYPES_H
#define RADIO_CC190_TYPES_H

/*{ radio_cc1190_types.h ************************************************

 This source is published under the GNU LESSER GENERAL PUBLIC LICENSE (LGPL).
 See file LEGAL.txt in this software package for details.

 Authors: <Sascha Poggemann>, <Gregor Rebel>

 Description: Low-level driver for radio amplifier cc1190 from Texas Instruments.

}*/
//{ Defines/ TypeDefs ****************************************************

//} Defines
//{ Includes *************************************************************

#include "ttc_basic.h"

//} Includes
//{ Structures/ Enums ****************************************************

//} Structures/ Enums
//{ Includes2 ************************************************************

// radio_cc1190_types_config_t is required by ttc_radio_types.h
#include "ttc_radio_types.h"

//} Includes2

#endif //RADIO_CC190_TYPES_H
