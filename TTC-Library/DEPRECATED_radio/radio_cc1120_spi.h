#ifndef radio_cc1120_transport_TRANSPORT_H
#define radio_cc1120_transport_TRANSPORT_H

/*{ cc1120::.h ************************************************

 Program Name: cc1120
 
 This source is published under the GNU LESSER GENERAL PUBLIC LICENSE (LGPL).
 See file LEGAL.txt in this software package for details.

 Authors: <Sascha Poggemann>, <Gregor Rebel>

 Description: This is an implementation for the Texas Instruments CC1120 Tranceiver

 Known constants (n=1..5):
 TTC_RADIOn_CC1120_SPI_CHIP_VERSION  sets value expected to be read from CC1120-register 0x31 (default = 0x04)

}*/
//{ Defines/ TypeDefs ****************************************************

//X #define CC1120_FLAG_SET         1
//X #define CC1120_FLAG_RESET       0

//X #define CC1120_HARDWARE_LOCK     1  GR: changed to mutex
//X #define CC1120_HARDWARE_UNLOCK   0

//Bit masking for Single / Burst and R/W operations
#define CC1120_RW_Bit           (1<<7)
#define CC1120_BURST_Bit        (1<<6)
#define CC1120_dummy_byte       0xff

#define CC1120_FIFO_BYTES_AVAILABLE_MASK 0b1111

//#define CC1120_GDO2_CHIP_RDY
#define CC1120_GDO2_RXNE_IT


//Define for the CC1120 Header size which is put infront of the Data
#define CC1120_package_Header_size 1
//#define CC1120_send_packet_ID 1

#define CC1120_DEFAULT_CHIP_VERSION 0x48

// check for default values
#ifndef TTC_RADIO1_CC1120_SPI_CHIP_VERSION
#define TTC_RADIO1_CC1120_SPI_CHIP_VERSION CC1120_DEFAULT_CHIP_VERSION
#endif
#ifndef TTC_RADIO2_CC1120_SPI_CHIP_VERSION
#define TTC_RADIO2_CC1120_SPI_CHIP_VERSION CC1120_DEFAULT_CHIP_VERSION
#endif
#ifndef TTC_RADIO3_CC1120_SPI_CHIP_VERSION
#define TTC_RADIO3_CC1120_SPI_CHIP_VERSION CC1120_DEFAULT_CHIP_VERSION
#endif
#ifndef TTC_RADIO4_CC1120_SPI_CHIP_VERSION
#define TTC_RADIO4_CC1120_SPI_CHIP_VERSION CC1120_DEFAULT_CHIP_VERSION
#endif
#ifndef TTC_RADIO5_CC1120_SPI_CHIP_VERSION
#define TTC_RADIO5_CC1120_SPI_CHIP_VERSION CC1120_DEFAULT_CHIP_VERSION
#endif


//} Defines
//{ Includes *************************************************************

#include "radio_cc1120_registers.h"
#include "radio_cc1120.h"
#include "ttc_basic.h"
#include "ttc_gpio.h"
#include "ttc_queue.h"
#include "ttc_spi.h"
#include "ttc_spi_types.h"
#include "ttc_usart.h"
#include "ttc_string.h"
#include "radio_cc1120_types.h"
#include "radio_cc1120_smartrf.h"

#ifdef CC1120_STM32W
#include "stm32w108_type.h"
#include "STM32W_types_patch.h"
#include "STM32W_SPI.h"
#endif

//} Includes
//{ Structures/ Enums ****************************************************

// all moved -> radio_cc1120_types.h

//} Structures/ Enums
//{ Global Variables *****************************************************



//} Global Variables
//{ external Function prototypes *****************************************

#ifdef CC1120_GDO2_RXNE_IT
extern void CC1120_Configure_GDO2_IT(void);
extern void CC1120_GDO2_IT_Handler(void * TaskArgument);
#endif

//} external Function prototypes
//{ Function prototypes **************************************************

/** loads default configuration for cc1120 radio transceiver into given structure
 *
 * @param SPI_Index logical index of SPI device to use (1=TTC_SPI1, ...)
 * @param DriverCfg structure to be filled (must be allocated by caller)
 * @return          = tse_Ok = 0: configuration loaded successfully
 */
ttc_radio_errorcode_e radio_cc1120_spi_get_defaults(u8_t SPI_Index, radio_cc1120_driver_t* DriverCfg);

/** CC1120 Initialization
 *
 * @param DriverCfg  structure that has been filled by radio_cc1120_spi_get_defaults() before
 * @param SPIx CC1120 SPI specification
 *
 * @return ttc_radio_errorcode_e
 */
ttc_radio_errorcode_e radio_cc1120_spi_init(radio_cc1120_driver_t* DriverCfg);

/** General access to cc1120 registers via SPI bus
 *
 * @param DriverCfg  structure that has been filled by radio_cc1120_spi_get_defaults() before
 * @param Register   Register address inside cc1120
 * @param buffer Pointer to the data buffer, either for read or write access
 * @param cc1120_access_mode specifies how to access the register
 * @param length when >0 it denotes read access, when <0 it denotes write access of -length
 *        abs(length) greater of 1 implies burst mode.
 *
 * @return ttc_radio_errorcode_e
 */
ttc_radio_errorcode_e radio_cc1120_spi_register_access(radio_cc1120_driver_t* DriverCfg,  u16_t Register, u8_t *Buffer, cc1120_access_mode_e AccessMode, int Amount);

///** Set/ Clear individual bits in cc1120 register
// *
// * @param DriverCfg  structure that has been filled by radio_cc1120_spi_get_defaults() before
// * @param Register   Register address inside cc1120
// * @param BitMask    bits to set/clear in Register
// *
// * @return ttc_radio_errorcode_e
// */
//ttc_radio_errorcode_e radio_cc1120_spi_register_set_bits(radio_cc1120_driver_t* DriverCfg,  u16_t Register, u8_t BitMask);
//ttc_radio_errorcode_e radio_cc1120_spi_register_clr_bits(radio_cc1120_driver_t* DriverCfg,  u16_t Register, u8_t BitMask);

/** General single byte access to cc1120 registers via SPI bus
 *
 * @param DriverCfg  structure that has been filled by radio_cc1120_spi_get_defaults() before
 * @param Register   Register address inside cc1120
 * @param Data       8-bit data either for read or write access
 * @param cc1120_access_mode specifies how to access the register
 * @return ttc_radio_errorcode_e
 */
u8_t radio_cc1120_spi_register_read(radio_cc1120_driver_t* DriverCfg, u16_t Register);
//u8_t radio_cc1120_spi_register_read_extended(radio_cc1120_driver_t* DriverCfg, u16_t Register);

ttc_radio_errorcode_e radio_cc1120_spi_register_write_single(radio_cc1120_driver_t* DriverCfg,  u16_t Register, u8_t Data);

///** General single byte access to cc1120 registers via SPI bus
// *
// * @param DriverCfg  structure that has been filled by radio_cc1120_spi_get_defaults() before
// * @param Register   16-bit Register address inside cc1120 used for extended Configuration Registers
// * @param Data        8-bit data
// * @return ttc_radio_errorcode_e
// */
//ttc_radio_errorcode_e radio_cc1120_spi_register_write_16(radio_cc1120_driver_t* DriverCfg, u16_t Register, u8_t Data);

///** General single byte access to cc1120 registers via SPI bus
// *
// * @param DriverCfg  structure that has been filled by radio_cc1120_spi_get_defaults() before
// * @param Register   8-bit Register address inside cc1120 used for configuration registers < 0x2Fxx
// * @param Data       8-bit data
// * @return ttc_radio_errorcode_e
// */
//ttc_radio_errorcode_e radio_cc1120_spi_register_write_8(radio_cc1120_driver_t* DriverCfg, u8_t Register, u8_t Data);

/** Strobes command and writes chip status byte to buffer
 *
 *  By default commands are send as Write. To a command,
 *  CC1120_READ_SINGLE may be OR'ed to obtain the number of RX bytes
 *  pending in RX FIFO.
 */
ttc_radio_errorcode_e radio_cc1120_spi_strobe(radio_cc1120_driver_t* DriverCfg, u8_t command);

ttc_radio_errorcode_e radio_cc1120_spi_get_chip_id(radio_cc1120_driver_t* DriverCfg);
ttc_radio_errorcode_e radio_cc1120_spi_get_chip_version(radio_cc1120_driver_t* DriverCfg);
ttc_radio_errorcode_e _radio_cc1120_spi_wait_chip_ready(radio_cc1120_driver_t* DriverCfg);
ttc_radio_errorcode_e _radio_cc1120_spi_wait_so_low(radio_cc1120_driver_t* DriverCfg);
ttc_radio_errorcode_e _radio_cc1120_spi_wait_gdo0_low(radio_cc1120_driver_t* DriverCfg);
ttc_radio_errorcode_e _radio_cc1120_spi_wait_gdo0_high(radio_cc1120_driver_t* DriverCfg);
ttc_radio_errorcode_e _radio_cc1120_spi_hardware_lock(radio_cc1120_driver_t* DriverCfg);
ttc_radio_errorcode_e _radio_cc1120_spi_hardware_unlock(radio_cc1120_driver_t* DriverCfg);


/** reset SPI bus interface
 *
 * Note: This function is private and should not be called from outside!
 *
 * @param DriverCfg  structure that has been filled by radio_cc1120_spi_get_defaults() before
 * @return           == tre_OK: SPI bus was reset successfully
 */
ttc_radio_errorcode_e radio_cc1120_spi_reset(radio_cc1120_driver_t* DriverCfg);

/** reads current value of input port GDO0
 *
 * Note: This function is private and should not be called from outside!
 *
 * @param DriverCfg  structure that has been filled by radio_cc1120_spi_get_defaults() before
 * @return           == TRUE: port shows logical 1; logical 0 otherwise
 */
BOOL _radio_cc1120_portGDO0_Get(radio_cc1120_driver_t* DriverCfg);

/** sets SPI slave select pin to logical zero.
 *
 * Note: This function is private and should not be called from outside!
 *
 * @param DriverCfg  structure that has been filled by radio_cc1120_spi_get_defaults() before
 */
void _radio_cc1120_portNSS_Clr(radio_cc1120_driver_t* DriverCfg);

/** sets SPI slave select pin to logical one.
 *
 * Note: This function is private and should not be called from outside!
 *
 * @param DriverCfg  structure that has been filled by radio_cc1120_spi_get_defaults() before
 */
void _radio_cc1120_portNSS_Set(radio_cc1120_driver_t* DriverCfg);

//} Function prototypes

#endif //cc1120_H
