/*{ radio_cc1120.c *****************************************************************

 Program Name: cc1120
 
 This source is published under the GNU LESSER GENERAL PUBLIC LICENSE (LGPL).
 See file LEGAL.txt in this software package for details.

 Authors: <Sascha Poggemann>, <Gregor Rebel>

 Description: This is an implementation for the Texas Instruments cc1120 Tranceiver
 
}*/
//{ Private Defines/ TypeDefs ****************************************************

//}  Private Defines
//{ Includes *********************************************************************

#include "radio_cc1120.h"

//} Includes
//{ Private Structures/ Enums ****************************************************

//} Private Structures/ Enums
//{ Global Variables *************************************************************

// required to be called from here
extern void ttc_radio_set_operating_mode(u8_t LogicalIndex, ttc_radio_mode_e Mode);

ttc_heap_array_define(ttc_mutex_smart_t*, TransactionLocks, TTC_AMOUNT_SPIS);

//} Global Variables
//{ Private Function prototypes **************************************************

#define LockTransaction(SPI_Index, Owner)  while (ttc_mutex_lock( A(TransactionLocks, SPI_Index-1), -1, ( void(*)() ) Owner) != tme_OK)
#define UnlockTransaction(SPI_Index)         ttc_mutex_unlock( A(TransactionLocks, SPI_Index-1) )
u8_t Amount_NonEmptyPackets = 0; //D
u16_t Amount_FIFO_Pointer_Resets;//D
u16_t Amount_TX_STROBES;//D

//} Private Function prototypes
//{ Implemented Functions ********************************************************

void radio_cc1120_get_defaults(u8_t RadioIndex, ttc_radio_generic_t* Radio_Generic) {
    Assert_Radio(Radio_Generic, ec_InvalidArgument);
    Radio_Generic->LogicalIndex = RadioIndex;
    radio_cc1120_get_config(Radio_Generic); // will create a driver configuration on first call

    // ToDo: SP - fill Radio_Generic with default values
    Radio_Generic->MaxChannelTx    = CC1120_MAX_Channels;
    Radio_Generic->MaxChannelRx    = CC1120_MAX_Channels;
    Radio_Generic->MaxLevelTx      = CC1120_MAX_TX_LEVEL;
    Radio_Generic->MaxLevelRx      = CC1120_MAX_RX_LEVEL;
    Radio_Generic->Max_Header      = RADIO_CC1120_HEADER_SIZE;  // 2
    Radio_Generic->Max_Footer      = RADIO_CC1120_FOOTER_SIZE;  // 0
    Radio_Generic->Max_Payload     = RADIO_CC1120_PAYLOAD_SIZE; // 60
    Radio_Generic->ChannelRx       = 1;
    Radio_Generic->ChannelTx       = 1;
    Radio_Generic->Size_QueueTx    = 5;
    Radio_Generic->Size_QueueRx    = 5;
    Radio_Generic->TargetAddress   = 0xff; // send to broadcast address
    Radio_Generic->LBT_Enabled     = 1;
}
void radio_cc1120_get_features(u8_t RadioIndex, ttc_radio_generic_t* Radio_Generic) {
    radio_cc1120_get_config(Radio_Generic); // checks pointer too

    // fill in default values
    radio_cc1120_get_defaults(RadioIndex, Radio_Generic);

    // add max values
    Radio_Generic->Flags.Bit.SleepMode_IDLE = 1;
    Radio_Generic->Flags.Bit.SleepMode_OFF  = 1;
    Radio_Generic->ChannelTx                = CC1120_MAX_Channels;
    Radio_Generic->ChannelRx                = CC1120_MAX_Channels;
    Radio_Generic->MaxLevelTx               = CC1120_MAX_TX_LEVEL;
    Radio_Generic->MaxLevelRx               = CC1120_MAX_RX_LEVEL;
    Radio_Generic->Flags.Bit.LBT            = 1;
}
ttc_radio_errorcode_e radio_cc1120_init(ttc_radio_generic_t* Radio_Generic) {
    radio_cc1120_driver_t* DriverCfg = radio_cc1120_get_config(Radio_Generic); // checks pointer too
    ttc_radio_errorcode_e Error = tre_OK;
    u8_t State = 0;
    u8_t Level = 0;

    if (! A(TransactionLocks, DriverCfg->SPI_Index-1) )
        A(TransactionLocks, DriverCfg->SPI_Index-1) = ttc_mutex_create();

    LockTransaction(DriverCfg->SPI_Index, radio_cc1120_init);

    // no SPI communication allowed before this point!
    radio_cc1120_spi_init(DriverCfg);

    if (!Error) { Level++; Error = radio_cc1120_spi_get_chip_id(DriverCfg); }
    if (!Error) { Level++; Error = radio_cc1120_spi_get_chip_version(DriverCfg); }
    if (!Error) { Level++; Error = radio_cc1120_spi_reset(DriverCfg); }
    while ( (State = _radio_cc1120_get_state(Radio_Generic)) != rcm_IDLE)
        Radio_Generic->TaskYield();

    if (!Error) { Level++; Error = radio_cc1120_spi_strobe(DriverCfg,rcs_CC112X_SRES); }
    UnlockTransaction(DriverCfg->SPI_Index);

    // Calibrate radio according to errataradio_cc1120_init_SmartRF
    radio_cc1120_manualCalibration(Radio_Generic);
    if (!Error) { Level++; Error = radio_cc1120_init_SmartRF(Radio_Generic); }

    LockTransaction(DriverCfg->SPI_Index, radio_cc1120_init);
    if (!Error) { Level++; Error = radio_cc1120_spi_strobe(DriverCfg,rcs_CC112X_SFRX); }
    if (!Error) { Level++; Error = radio_cc1120_spi_strobe(DriverCfg,rcs_CC112X_SFTX); }
    UnlockTransaction(DriverCfg->SPI_Index);
    //enable Listen-before-talk Feature (LBT)
    if(Radio_Generic->LBT_Enabled)
        if (!Error) Error = radio_cc1120_LBT_enable(Radio_Generic, TRUE);

    // set RXOFF + TXOFF modes according to cc1120 datasheet p.81
    // if (!Error) { Level++; Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_MCSM1, 0x3F); }

    switch (DriverCfg->SPI_Index) { // configure external interrupt line for incoming data
#ifdef TTC_RADIO1
    case 1: {
#ifndef UEXT1_USART_RX
//#error Missing definition!
#endif

#ifndef TTC_RADIO1_PIN_GDO2
#error Missing definition!
#endif
        ttc_gpio_init(TTC_RADIO1_PIN_GDO2,    tgm_input_floating);
        // initialize interrupt for receiving data from radio
        ttc_interrupt_init(tit_GPIO_Rising,
                           ttc_gpio_create_index8(TTC_RADIO1_PIN_GDO2),
                           _radio_cc1120_isr_receive,
                           (void*) Radio_Generic,
                           NULL, NULL
                           );
        ttc_interrupt_enable(tit_GPIO_Rising,
                             ttc_gpio_create_index8(TTC_RADIO1_PIN_GDO2),
                             TRUE
                             );
        break;
    }
#endif
#ifdef TTC_RADIO2
    case 2: {

        ttc_gpio_init(TTC_RADIO2_PIN_GDO2,    tgm_input_floating);
        // initialize interrupt for receiving data from radio
        ttc_interrupt_init(tit_GPIO_Rising,
                           ttc_gpio_create_index8(TTC_RADIO2_PIN_GDO2),
                           _radio_cc1120_isr_receive,
                           (void*) Radio_Generic,
                           NULL, NULL
                           );
        ttc_interrupt_enable(tit_GPIO_Rising,
                             ttc_gpio_create_index8(TTC_RADIO2_PIN_GDO2),
                             TRUE
                             );
        break;
    }
#endif
#ifdef TTC_RADIO3
    case 3: {

        ttc_gpio_init(TTC_RADIO3_PIN_GDO2,    tgm_input_floating);
        // initialize interrupt for receiving data from radio
        ttc_interrupt_init(tit_GPIO_Rising,
                           ttc_gpio_create_index8(TTC_RADIO3_PIN_GDO2),
                           _radio_cc1120_isr_receive,
                           (void*) Radio_Generic,
                           NULL, NULL
                           );
        ttc_interrupt_enable(tit_GPIO_Rising,
                             ttc_gpio_create_index8(TTC_RADIO3_PIN_GDO2),
                             TRUE
                             );
        break;
    }
#endif
#ifdef TTC_RADIO4
    case 4: {
        ttc_gpio_init(TTC_RADIO4_PIN_GDO2,    tgm_input_floating);
        // initialize interrupt for receiving data from radio
        ttc_interrupt_init(tit_GPIO_Rising,
                           ttc_gpio_create_index8(TTC_RADIO4_PIN_GDO2),
                           _radio_cc1120_isr_receive,
                           (void*) Radio_Generic,
                           NULL, NULL
                           );
        ttc_interrupt_enable(tit_GPIO_Rising,
                             ttc_gpio_create_index8(TTC_RADIO4_PIN_GDO2),
                             TRUE
                             );
        break;
    }
#endif
#ifdef TTC_RADIO5
    case 5: {

        ttc_gpio_init(TTC_RADIO5_PIN_GDO2,    tgm_input_floating);
        // initialize interrupt for receiving data from radio
        ttc_interrupt_init(tit_GPIO_Rising,
                           ttc_gpio_create_index8(TTC_RADIO5_PIN_GDO2),
                           _radio_cc1120_isr_receive,
                           (void*) Radio_Generic,
                           NULL, NULL
                           );
        ttc_interrupt_enable(tit_GPIO_Rising,
                             ttc_gpio_create_index8(TTC_RADIO5_PIN_GDO2),
                             TRUE
                             );
        break;
    }
#endif
    default: Assert(0, ec_InvalidConfiguration); // indexed radio not activated
    }

    // function calls below lock their own transaction
    ttc_radio_set_operating_mode(Radio_Generic->LogicalIndex, trm_Idle);
    if (!Error) { Level++; Error = radio_cc1120_set_power_tx(Radio_Generic, Radio_Generic->LevelTx); }
    ttc_radio_set_operating_mode(Radio_Generic->LogicalIndex, trm_Receive);
    Assert(Error != tre_TimeOut, ec_InvalidConfiguration); // communication error on SPI-bus (check variable Level)
    Assert(Error == tre_OK,      ec_UNKNOWN);              // unknown error (check variable Level)

    return tre_OK;
}
ttc_radio_errorcode_e radio_cc1120_manualCalibration(ttc_radio_generic_t* Radio_Generic) {
    // Calibrate radio according to TexasInstruments errata sheet
    radio_cc1120_driver_t* DriverCfg = radio_cc1120_get_config(Radio_Generic); // checks pointer too
    ttc_radio_errorcode_e Error = tre_OK;
    u8_t original_fs_cal2;
    //u8_t calResults_for_vcdac_start_high[3];
    ttc_heap_array_define(u8_t, calResults_for_vcdac_start_high,3);
    //u8_t calResults_for_vcdac_start_mid[3];
    ttc_heap_array_define(u8_t, calResults_for_vcdac_start_mid, 3);
    u8_t marcstate;
    u8_t writeByte;
    
    LockTransaction(DriverCfg->SPI_Index, radio_cc1120_manualCalibration);
    // 1) Set VCO cap-array to 0 (FS_VCO2 = 0x00)
    writeByte = 0x00;
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_VCO2, writeByte);

    // 2) Start with high VCDAC (original VCDAC_START + 2):
    if (!Error) original_fs_cal2 = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_FS_CAL2);
    writeByte = original_fs_cal2 + VCDAC_START_OFFSET;
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_CAL2, writeByte);

    // 3) Calibrate and wait for calibration to be done (radio back in IDLE state)
    if (!Error) Error = radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SCAL);

    do
    {
        //marcstate = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_MARCSTATE);
        marcstate = _radio_cc1120_get_state(Radio_Generic);
    } while (marcstate != rcm_IDLE);
    
    // 4) Read FS_VCO2, FS_VCO4 and FS_CHP register obtained with high VCDAC_START value
    if (!Error) A(calResults_for_vcdac_start_high, FS_VCO2_INDEX) = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_FS_VCO2);
    if (!Error) A(calResults_for_vcdac_start_high, FS_VCO4_INDEX) = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_FS_VCO4);
    if (!Error) A(calResults_for_vcdac_start_high, FS_CHP_INDEX)  = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_FS_CHP);
    
    // 5) Set VCO cap-array to 0 (FS_VCO2 = 0x00)
    writeByte = 0x00;
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_VCO2, writeByte);

    // 6) Continue with mid VCDAC (original VCDAC_START):
    writeByte = original_fs_cal2;
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_CAL2, writeByte);

    // 7) Calibrate and wait for calibration to be done (radio back in IDLE state)
    if (!Error) Error = radio_cc1120_spi_strobe(DriverCfg,  rcs_CC112X_SCAL);

    if (!Error){
        do
        {
            //marcstate = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_MARCSTATE);
            marcstate = _radio_cc1120_get_state(Radio_Generic);
        } while (marcstate != rcm_IDLE);
    }
    // 8) Read FS_VCO2, FS_VCO4 and FS_CHP register obtained with mid VCDAC_START value
    if (!Error) A(calResults_for_vcdac_start_mid, FS_VCO2_INDEX) = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_FS_VCO2);
    if (!Error) A(calResults_for_vcdac_start_mid, FS_VCO4_INDEX) = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_FS_VCO4);
    if (!Error) A(calResults_for_vcdac_start_mid, FS_CHP_INDEX)  = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_FS_CHP);

    // 9) Write back highest FS_VCO2 and corresponding FS_VCO and FS_CHP result
    if (A(calResults_for_vcdac_start_high,FS_VCO2_INDEX) > A(calResults_for_vcdac_start_mid, FS_VCO2_INDEX))
    {
        writeByte = A(calResults_for_vcdac_start_high, FS_VCO2_INDEX);
        if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_VCO2, writeByte);
        writeByte = A(calResults_for_vcdac_start_high, FS_VCO4_INDEX);
        if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_VCO4, writeByte);
        writeByte = A(calResults_for_vcdac_start_high, FS_CHP_INDEX);
        if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_CHP, writeByte);
    }
    else
    {
        writeByte = A(calResults_for_vcdac_start_mid, FS_VCO2_INDEX);
        if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_VCO2, writeByte);
        writeByte = A(calResults_for_vcdac_start_mid,FS_VCO4_INDEX);
        if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_VCO4, writeByte);
        writeByte = A(calResults_for_vcdac_start_mid, FS_CHP_INDEX);
        if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_CHP, writeByte);
    }

    UnlockTransaction(DriverCfg->SPI_Index);
    return Error;
}
ttc_radio_errorcode_e radio_cc1120_init_SmartRF(ttc_radio_generic_t* Radio_Generic) {
    radio_cc1120_driver_t* DriverCfg = radio_cc1120_get_config(Radio_Generic); // checks pointer too

    // apply radio configuration
    ttc_radio_errorcode_e Error = tre_OK;
    LockTransaction(DriverCfg->SPI_Index, radio_cc1120_init_SmartRF);
    // Rf settings for CC1120
    //
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_IOCFG3,          0x06);                            //GPIO3 IO Pin Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_IOCFG2,          0x01);                            //GPIO2 IO Pin Configuration
    //  Associated to the RX FIFO. Asserted when the RX FIFO is filled above
    //  FIFO_CFG.FIFO_THR or the end of packet is reached. De-asserted
    //  when the RX FIFO is empty
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_IOCFG1,          0x30);                            //GPIO1 IO Pin Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_IOCFG0,          0x30);                            //GPIO0 IO Pin Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_SYNC3,           SMARTRF_SETTING_SYNC3);           //Sync Word Configuration [31:24]
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_SYNC2,           SMARTRF_SETTING_SYNC2);           //Sync Word Configuration [23:16]
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_SYNC1,           SMARTRF_SETTING_SYNC1);           //Sync Word Configuration [15:8]
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_SYNC0,           SMARTRF_SETTING_SYNC0);           //Sync Word Configuration [7:0]
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_SYNC_CFG1,       SMARTRF_SETTING_SYNC_CFG1);       //Sync Word Detection Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_SYNC_CFG0,       SMARTRF_SETTING_SYNC_CFG0);       //Sync Word Length Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_DEVIATION_M,     SMARTRF_SETTING_DEVIATION_M);     //Frequency Deviation Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_MODCFG_DEV_E,    SMARTRF_SETTING_MODCFG_DEV_E);    //Modulation Format and Frequency Deviation Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_DCFILT_CFG,      SMARTRF_SETTING_DCFILT_CFG);      //Digital DC Removal Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_PREAMBLE_CFG1,   SMARTRF_SETTING_PREAMBLE_CFG1);   //Preamble Length Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_PREAMBLE_CFG0,   SMARTRF_SETTING_PREAMBLE_CFG0);   //
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FREQ_IF_CFG,     SMARTRF_SETTING_FREQ_IF_CFG);     //RX Mixer Frequency Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_IQIC,            SMARTRF_SETTING_IQIC);            //Digital Image Channel Compensation Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_CHAN_BW,         SMARTRF_SETTING_CHAN_BW);         //Channel Filter Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_MDMCFG1,         0x46);                            //General Modem Parameter Configuration
                                                                                                                                        //FIFO Enable. Specifies if data to/from modem will be passed through the FIFOs or directly to the serial pin
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_MDMCFG0,         (0x05 & (~0x40)));////(SMARTRF_SETTING_MDMCFG0 & (~0x40)));//General Modem Parameter Configuration
                                                                                                                                        //Transparent mode disabled
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_DRATE2,          SMARTRF_SETTING_DRATE2);          //Data Rate Configuration Exponent and Mantissa [19:16]
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_DRATE1,          SMARTRF_SETTING_DRATE1);          //Data Rate Configuration Mantissa [15:8]
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_DRATE0,          SMARTRF_SETTING_DRATE0);          //Data Rate Configuration Mantissa [7:0]
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_AGC_REF,         SMARTRF_SETTING_AGC_REF);         //AGC Reference Level Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_AGC_CS_THR,      SMARTRF_SETTING_AGC_CS_THR);      //Carrier Sense Threshold Configuration
#warning how to qualify/measure Carrier Sense treshold: currently set to 0x2D (works good)
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_AGC_GAIN_ADJUST, SMARTRF_SETTING_AGC_GAIN_ADJUST); //RSSI Offset Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_AGC_CFG3,        SMARTRF_SETTING_AGC_CFG3);        //AGC Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_AGC_CFG2,        SMARTRF_SETTING_AGC_CFG2);        //AGC Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_AGC_CFG1,        SMARTRF_SETTING_AGC_CFG1);        //AGC Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_AGC_CFG0,        SMARTRF_SETTING_AGC_CFG0);        //AGC Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FIFO_CFG,        0xFF);                            //FIFO Configuration //Threshold == 0x64 & Automatically flushes the last packet received in the RX FIFO if a CRC error occurred.
                                                                                                                                        //If this bit has been turned off and should be turned on again, an SFRX strobe must first be issued
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_DEV_ADDR,        Radio_Generic->DeviceAddress);    //Device Address Configuration used for RX packet filtering
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_SETTLING_CFG,    SMARTRF_SETTING_SETTLING_CFG);    //Frequency Synthesizer Calibration and Settling Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_CFG,          SMARTRF_SETTING_FS_CFG);          //Frequency Synthesizer Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_WOR_CFG1,        SMARTRF_SETTING_WOR_CFG1);        //eWOR Configuration, Reg 1
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_WOR_CFG0,        SMARTRF_SETTING_WOR_CFG0);        //eWOR Configuration, Reg 0
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_WOR_EVENT0_MSB,  SMARTRF_SETTING_WOR_EVENT0_MSB);  //Event 0 Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_WOR_EVENT0_LSB,  SMARTRF_SETTING_WOR_EVENT0_LSB);  //Event 0 Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_PKT_CFG2,        0x10);                            //Packet Configuration, Reg 2
                                                                                                                                        //Normal mode/FIFO mode
                                                                                                                                        //Indicates clear channel when RSSI is below threshold and ETSI LBT requirements are met
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_PKT_CFG1,        0x75);                            //Packet Configuration, Reg 1
                                                                                                                                        //Append Status Bytes to RX FIFO.
                                                                                                                                        //CRC calculation in TX mode and CRC check in RX mode enabled.
                                                                                                                                        //Address check, 0x00 and 0xFF broadcast
                                                                                                                                        //Data whitening enabled
                                                                                                                                        //Two Status Bytes appended
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_PKT_CFG0,        0x20);                            //Packet Configuration, Reg 0
                                                                                                                                        //Indicates clear channel when RSSI is below threshold and ETSI LBT requirements are met
                                                                                                                                        //Normal mode/FIFO mode (MDMCFG1.FIFO_EN must be set to 1 and MDMCFG0.TRANSPARENT_MODE_EN must be set to 0)
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_RFEND_CFG1,      0x3F);                            //RFEND Configuration, Reg 1
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_RFEND_CFG0,      0x30);                            //RFEND Configuration, Reg 0
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_PA_CFG2,         SMARTRF_SETTING_PA_CFG2);         //Power Amplifier Configuration, Reg 2
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_PA_CFG1,         SMARTRF_SETTING_PA_CFG1);         //Power Amplifier Configuration, Reg 1
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_PA_CFG0,         SMARTRF_SETTING_PA_CFG0);         //Power Amplifier Configuration, Reg 0
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_PKT_LEN,         SMARTRF_SETTING_PKT_LEN);         //Packet Length Configuration

    //extended configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_IF_MIX_CFG,      SMARTRF_SETTING_IF_MIX_CFG);      //IF Mix Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FREQOFF_CFG,     SMARTRF_SETTING_FREQOFF_CFG);     //Frequency Offset Correction Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_TOC_CFG,         SMARTRF_SETTING_TOC_CFG);         //Timing Offset Correction Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_MARC_SPARE,      SMARTRF_SETTING_MARC_SPARE);      //MARC Spare
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_ECG_CFG,         SMARTRF_SETTING_ECG_CFG);         //External Clock Frequency Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_SOFT_TX_DATA_CFG,SMARTRF_SETTING_SOFT_TX_DATA_CFG);//Soft TX Data Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_EXT_CTRL,        SMARTRF_SETTING_EXT_CTRL);        //External Control Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_RCCAL_FINE,      SMARTRF_SETTING_RCCAL_FINE);      //RC Oscillator Calibration (fine)
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_RCCAL_COARSE,    SMARTRF_SETTING_RCCAL_COARSE);    //RC Oscillator Calibration (coarse)
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_RCCAL_OFFSET,    SMARTRF_SETTING_RCCAL_OFFSET);    //RC Oscillator Calibration Clock Offset
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FREQOFF1,        SMARTRF_SETTING_FREQOFF1);        //Frequency Offset (MSB)
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FREQOFF0,        SMARTRF_SETTING_FREQOFF0);        //Frequency Offset (LSB)
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FREQ2,           SMARTRF_SETTING_FREQ2);           //Frequency Configuration [23:16]
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FREQ1,           SMARTRF_SETTING_FREQ1);           //Frequency Configuration [15:8]
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FREQ0,           SMARTRF_SETTING_FREQ0);           //Frequency Configuration [7:0]
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_IF_ADC2,         SMARTRF_SETTING_IF_ADC2);         //Analog to Digital Converter Configuration, Reg 2
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_IF_ADC1,         SMARTRF_SETTING_IF_ADC1);         //Analog to Digital Converter Configuration, Reg 1
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_IF_ADC0,         SMARTRF_SETTING_IF_ADC0);         //Analog to Digital Converter Configuration, Reg 0
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_DIG1,         SMARTRF_SETTING_FS_DIG1);         //
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_DIG0,         SMARTRF_SETTING_FS_DIG0);         //
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_CAL3,         SMARTRF_SETTING_FS_CAL3);         //
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_CAL2,         SMARTRF_SETTING_FS_CAL2);         //
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_CAL1,         SMARTRF_SETTING_FS_CAL1);         //
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_CAL0,         SMARTRF_SETTING_FS_CAL0);         //
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_CHP,          SMARTRF_SETTING_FS_CHP);          //Charge Pump Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_DIVTWO,       SMARTRF_SETTING_FS_DIVTWO);       //Divide by 2
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_DSM1,         SMARTRF_SETTING_FS_DSM1);         //Digital Synthesizer Module Configuration, Reg 1
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_DSM0,         SMARTRF_SETTING_FS_DSM0);         //Digital Synthesizer Module Configuration, Reg 0
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_DVC1,         SMARTRF_SETTING_FS_DVC1);         //Divider Chain Configuration, Reg 1
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_DVC0,         SMARTRF_SETTING_FS_DVC0);         //Divider Chain Configuration, Reg 0
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_LBI,          SMARTRF_SETTING_FS_LBI);          //Local Bias Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_PFD,          SMARTRF_SETTING_FS_PFD);          //Phase Frequency Detector Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_PRE,          SMARTRF_SETTING_FS_PRE);          //Prescaler Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_REG_DIV_CML,  SMARTRF_SETTING_FS_REG_DIV_CML);  //
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_SPARE,        SMARTRF_SETTING_FS_SPARE);        //
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_VCO4,         SMARTRF_SETTING_FS_VCO4);         //VCO Configuration, Reg 4
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_VCO3,         SMARTRF_SETTING_FS_VCO3);         //VCO Configuration, Reg 3
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_VCO2,         SMARTRF_SETTING_FS_VCO2);         //VCO Configuration, Reg 2
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_VCO1,         SMARTRF_SETTING_FS_VCO1);         //VCO Configuration, Reg 1
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_FS_VCO0,         SMARTRF_SETTING_FS_VCO0);         //VCO Configuration, Reg 0
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_GBIAS6,          SMARTRF_SETTING_GBIAS6);          //Global Bias Configuration, Reg 6
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_GBIAS5,          SMARTRF_SETTING_GBIAS5);          //Global Bias Configuration, Reg 5
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_GBIAS4,          SMARTRF_SETTING_GBIAS4);          //Global Bias Configuration, Reg 4
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_GBIAS3,          SMARTRF_SETTING_GBIAS3);          //Global Bias Configuration, Reg 3
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_GBIAS2,          SMARTRF_SETTING_GBIAS2);          //Global Bias Configuration, Reg 2
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_GBIAS1,          SMARTRF_SETTING_GBIAS1);          //Global Bias Configuration, Reg 1
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_GBIAS0,          SMARTRF_SETTING_GBIAS0);          //Global Bias Configuration, Reg 0
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_IFAMP,           SMARTRF_SETTING_IFAMP);           //Intermediate Frequency Amplifier Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_LNA,             SMARTRF_SETTING_LNA);             //Low Noise Amplifier Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_RXMIX,           SMARTRF_SETTING_RXMIX);           //RX Mixer Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_XOSC5,           SMARTRF_SETTING_XOSC5);           //Crystal Oscillator Configuration, Reg 5
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_XOSC4,           SMARTRF_SETTING_XOSC4);           //Crystal Oscillator Configuration, Reg 4
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_XOSC3,           SMARTRF_SETTING_XOSC3);           //Crystal Oscillator Configuration, Reg 3
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_XOSC2,           SMARTRF_SETTING_XOSC2);           //Crystal Oscillator Configuration, Reg 2
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_XOSC1,           SMARTRF_SETTING_XOSC1);           //Crystal Oscillator Configuration, Reg 1
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_XOSC0,           SMARTRF_SETTING_XOSC0);           //Crystal Oscillator Configuration, Reg 0
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_ANALOG_SPARE,    SMARTRF_SETTING_ANALOG_SPARE);    //
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_PA_CFG3,         SMARTRF_SETTING_PA_CFG3);         //Power Amplifier Configuration, Reg 3
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_IRQ0M,           SMARTRF_SETTING_IRQ0M);           //IRQ0 Mask Configuration
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_IRQ0F,           SMARTRF_SETTING_IRQ0F);           //IRQ0 Flag

    // Status Registers (0x2F64 - 0x2FA0) are read only and do not require to be configured
    // DATA FIFO Access Registers (0x2FD2 - 0x2FD9 and 0x003F - 0x00FF) do not require to be configured

    //final test
    u8_t Compare = 0;
    if (!Error) Compare = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_DEV_ADDR);  // Device address.
    Assert_Radio(Compare == Radio_Generic->DeviceAddress, ec_DeviceNotFound);              // written value could not be verified

    //test register in extended Register space
    if (!Error) Compare = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_FREQ0);     // Register rcr_CC112X_FREQ0 in extended Register space.
    Assert_Radio(Compare == SMARTRF_SETTING_FREQ0, ec_DeviceNotFound);                     // written value could not be verified


    UnlockTransaction(DriverCfg->SPI_Index);
    return Error;
}
u32_t radio_cc1120_getDatarate(ttc_radio_generic_t* Radio_Generic){
    //radio_cc1120_driver_t* DriverCfg = radio_cc1120_get_config(Radio_Generic); // checks pointer too

    return SMARTRF_SETTING_DATARATE;

}
u8_t radio_cc1120_getPacketHeaderSize(ttc_radio_generic_t* Radio_Generic){
    //Defines provided within smartRF configuration
    return SMARTRF_SETTING_PREAMBLE_SIZE + SMARTRF_SETTING_AMOUNT_SYNC_WORDS + SMARTRF_SETTING_LENGTHFIELD_SIZE + SMARTRF_SETTING_ADDRESSFIELD_SIZE + SMARTRF_SETTING_CRC_SIZE;
}
ttc_radio_errorcode_e radio_cc1120_packet_send(ttc_radio_generic_t* Radio_Generic, u8_t TargetAddress, u8_t Size, u8_t* Data) {
    radio_cc1120_driver_t* DriverCfg = radio_cc1120_get_config(Radio_Generic); // checks pointer too
    Assert_Radio(Size, ec_InvalidArgument);
    Assert_Radio(Data, ec_InvalidArgument);
    Assert_Radio(Size < CC1120_SIZE_RX_FIFO, ec_InvalidArgument);

    ttc_radio_errorcode_e Error = tre_OK;
    ttc_radio_errorcode_e Error_solved = tre_OK;
    Base_t StartTime        = 0;
#ifdef TTC_DEBUG_RADIO
    u16_t TimeTxPreparation = 0; //ToDo: check if this timecheck still works
#endif
    u8_t TX_First           = 0; //TX FIFO Buffer position of FIRST pointer
    u8_t TX_Last            = 0; //TX FIFO Buffer position of LAST  pointer

    u8_t TransmitState      = 0; //current CC1120 transmit state
    u8_t Marcstate          = 0; //current Marcstate read from the CC1120

    Assert_Radio(Size, ec_InvalidArgument);
    Assert_Radio(Size <= RADIO_CC1120_PAYLOAD_SIZE, ec_InvalidArgument);

    LockTransaction(DriverCfg->SPI_Index, radio_cc1120_packet_send);
    u8_t packetSize = Size + 1;
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_SINGLE_TXFIFO, packetSize);
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_SINGLE_TXFIFO, TargetAddress);
    for (int BytesToSend = 0; BytesToSend < Size; BytesToSend++) {
        if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_SINGLE_TXFIFO, *Data); //ToDo: might be optimized through burst access
        Data++;
    }
    if (!Error) Error = radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_STX);

    StartTime = ttc_task_get_elapsed_usecs();
    while ( (TransmitState = _radio_cc1120_get_state(Radio_Generic)) != rcm_TX) { //wait for radio to reach TX State
        switch(TransmitState){
        case rcm_TX_FIFO_ERR: {
            Error_solved =_radio_cc1120_handle_error(Radio_Generic, tre_TxFIFO_Underrun, TRUE);
            if (Error_solved == tre_OK){
                return tre_TX_Packet_Not_Send;
            }else {
                return tre_Radio_Reset_Request;
            }

        }
        case rcm_RX_FIFO_ERR: {
            Error_solved =_radio_cc1120_handle_error(Radio_Generic, tre_RxFIFO_Overrun, TRUE);
            if (Error_solved == tre_OK){
                return tre_TX_Packet_Not_Send;
            }else {
                return tre_Radio_Reset_Request;
            }

        }
        case rcm_IDLE: {
            Error_solved =_radio_cc1120_handle_error(Radio_Generic, tre_UnexpectedState, TRUE);
            if (Error_solved == tre_OK){
                return tre_TX_Packet_Not_Send;
            }else {
                return tre_Radio_Reset_Request;
            }

        }
        case rcm_RX:{
            if(!Radio_Generic->LBT_Enabled){
                Error_solved =_radio_cc1120_handle_error(Radio_Generic, tre_UnexpectedState, TRUE);
                if (Error_solved == tre_OK){
                    return tre_TX_Packet_Not_Send;
                }else {
                    return tre_Radio_Reset_Request;
                }
            }
        }
        default :
            break;
        }

        if((ttc_task_get_elapsed_usecs() - StartTime)>(RADIO_CC1120_TRANSMIT_TIMEOUT*100)){
            //Assert_Radio(FALSE, ec_Debug);//D
            if (TransmitState != rcm_RX){
                Error_solved =_radio_cc1120_handle_error(Radio_Generic, tre_TX_Timeout, TRUE);
                if (Error_solved == tre_OK){
                    return tre_TX_Packet_Not_Send;
                }else {
                    return tre_Radio_Reset_Request;
                }
            }else {
                break;
            }
        }
        Radio_Generic->TaskYield();
    }
    StartTime = ttc_task_get_elapsed_usecs();
    while ( (TransmitState = _radio_cc1120_get_state(Radio_Generic)) == rcm_TX) { //wait for radio to exit TX State
        switch(TransmitState){
        case rcm_TX_FIFO_ERR: {
            Error_solved =_radio_cc1120_handle_error(Radio_Generic, tre_TxFIFO_Underrun, TRUE);
            if (Error_solved == tre_OK){
                return tre_TX_Packet_Not_Send;
            }else {
                return tre_Radio_Reset_Request;
            }
        }
        case rcm_RX_FIFO_ERR: {
            Error_solved =_radio_cc1120_handle_error(Radio_Generic, tre_RxFIFO_Overrun, TRUE);
            if (Error_solved == tre_OK){
                return tre_TX_Packet_Not_Send;
            }else {
                return tre_Radio_Reset_Request;
            }
        }
        case rcm_IDLE: {
            Error_solved =_radio_cc1120_handle_error(Radio_Generic, tre_UnexpectedState, TRUE);
            if (Error_solved == tre_OK){
                return tre_TX_Packet_Not_Send;
            }else {
                return tre_Radio_Reset_Request;
            }
        }
        case rcm_RX:{
            if(!Radio_Generic->LBT_Enabled){
                Error_solved =_radio_cc1120_handle_error(Radio_Generic, tre_UnexpectedState, TRUE);
                if (Error_solved == tre_OK){
                    return tre_TX_Packet_Not_Send;
                }else {
                    return tre_Radio_Reset_Request;
                }

            }

        }
        default :
            break;

        }

        if((ttc_task_get_elapsed_usecs() - StartTime)>(RADIO_CC1120_TRANSMIT_TIMEOUT*10)){
            Assert_Radio(FALSE, ec_Debug);//D
            Error_solved =_radio_cc1120_handle_error(Radio_Generic, tre_TX_Timeout, TRUE);
            if (Error_solved == tre_OK){
                return tre_TX_Packet_Not_Send;
            }else {
                return tre_Radio_Reset_Request;
            }
        }

        Radio_Generic->TaskYield();
    }
    if (!Error) TX_First = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_TXFIRST);
    if (!Error) TX_Last  = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_TXLAST);

    Marcstate = _radio_cc1120_get_state(Radio_Generic); //D
    Marcstate = Marcstate;
    Amount_TX_STROBES = 0 ;
    while (TX_First != TX_Last){
        if (!Error) TX_First = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_TXFIRST);
        if (!Error) TX_Last  = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_TXLAST);
        // if (!Error) Error = radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_STX);
        Amount_TX_STROBES ++ ;
        Radio_Generic->TaskYield();
        if (Amount_TX_STROBES > 50){
            Error_solved = _radio_cc1120_handle_error(Radio_Generic,rcm_TX_FIFO_ERR, TRUE);
            if (Error_solved == tre_OK){
                Assert_Radio(FALSE, ec_Debug);//D
                return tre_TX_Packet_Not_Send;
            }else {
                Assert_Radio(FALSE, ec_Debug);//D
                return tre_Radio_Reset_Request;
            }

        }

    }


    #ifdef TTC_DEBUG_RADIO
            u16_t TimeTxMode = ttc_task_get_elapsed_usecs() - StartTime - TimeTxPreparation;

            if (Radio_Generic->Max_TimeTxMode < TimeTxMode)
                Radio_Generic->Max_TimeTxMode = TimeTxMode;
            if (Radio_Generic->Max_TimeTxPreparation < TimeTxPreparation)
                Radio_Generic->Max_TimeTxPreparation = TimeTxPreparation;
    #endif

    UnlockTransaction(DriverCfg->SPI_Index);

    return Error;
}
                 u8_t radio_cc1120_packet_receive(ttc_radio_generic_t* Radio_Generic, u8_t* Buffer, u8_t MaxSize, u8_t* TargetAdress, u8_t* RSSI) {
    radio_cc1120_driver_t* DriverCfg = radio_cc1120_get_config(Radio_Generic); // checks pointer too
    ttc_radio_errorcode_e Error = tre_OK;
    Assert_Radio(RADIO_CC1120_PAYLOAD_SIZE <= MaxSize, ec_InvalidArgument); // Buffer[] is too small for this packet

    LockTransaction(DriverCfg->SPI_Index, radio_cc1120_packet_receive);

    u8_t RX_BytesPending = 0 ;
    //if (!Error) Error = radio_cc1120_spi_register_access(DriverCfg, rcr_CC112X_NUM_RXBYTES, &RX_BytesPending, cam_single_read, 1);
    if (!Error) RX_BytesPending = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_NUM_RXBYTES);
    u8_t PacketSize = 0;

    if (RX_BytesPending) {

        if (RX_BytesPending > CC1120_SIZE_RX_FIFO) { // invalid size: reread value via SPI
            //get bytes again, if a value > CC1120_SIZE_RX_FIFO is read from the RX Bytes Register.
            //if (!Error) Error = radio_cc1120_spi_register_access(DriverCfg, rcr_CC112X_NUM_RXBYTES, &RX_BytesPending, cam_single_read, 1); //D
            if (!Error) RX_BytesPending = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_NUM_RXBYTES);

        }
        if ( (RX_BytesPending > CC1120_SIZE_RX_FIFO) ||
             (RX_BytesPending > TTC_NETWORK_MAX_PAYLOAD_SIZE)
             ) { // invalid packet size: flush rx-buffer
            if (1) Assert_Radio(FALSE, ec_Debug);
            _radio_cc1120_handle_error(Radio_Generic, tre_RxFIFO_Overrun, FALSE);
        }
        Amount_NonEmptyPackets++; //D
        if (!Error) PacketSize = radio_cc1120_spi_register_read(DriverCfg,    rcr_CC112X_SINGLE_RXFIFO); //Packet Length
        if (!Error) *TargetAdress = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_SINGLE_RXFIFO); //Address
        PacketSize--; //decrement because Target Address already read
        Assert_Radio(PacketSize > 0, ec_InvalidArgument);

        if (PacketSize > RADIO_CC1120_PAYLOAD_SIZE) { // invalid size: flush fifo
            if (1) Assert_Radio(FALSE, ec_Debug);
            PacketSize = 0;
            Error = tre_RxCorruptPacket;
        }

        if(PacketSize > (RX_BytesPending - 2)){ //less 2 Bytes because RSSI and Status is appended
            if (!Error) RX_BytesPending = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_NUM_RXBYTES);
            if(PacketSize > RX_BytesPending - 2){
                //Assert_Radio(FALSE, ec_Debug); //D avoid RX_FIFO Underflow
                Error = tre_RxCorruptPacket; //go into Error Handler
                //#warning radio_cc1120_packet_receive: PacketSize > (RX_BytesPending - 2) this still  causes problems need Error handler call here
            }
        }
        if(Error == tre_OK){
            for (int i = 0; i < PacketSize; i++) {
                if (!Error) *Buffer++ = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_SINGLE_RXFIFO);
            }

            //Optionally, two status bytes (see Table 22 and Table 23) with RSSI value, Link Quality Indication, and
            //CRC status can be appended in the RX FIFO by setting PKT_CFG1.APPEND_STATUS = 1.
            //First Status Byte:
            //RSSI value (first byte appended after the data)
            //Second Status Byte
            //Bit 7 == 1: CRC for received data OK (or CRC disabled or TX mode)
            //Bit 7 == 0: CRC error in received data
            //Bits 6:0 == LQI (Indicating the link quality)
            u8_t Last_Status_Byte1 = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_SINGLE_RXFIFO);
            *RSSI = Last_Status_Byte1;
            radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_SINGLE_RXFIFO);
        }else{

            //Assert_Radio(FALSE, ec_Debug); //D avoid RX_FIFO Underflow
        }
    }

    if (Error != tre_OK) {
        _radio_cc1120_handle_error(Radio_Generic, Error, TRUE); // unlocks transaction
        PacketSize = 0;
    }
    else
        UnlockTransaction(DriverCfg->SPI_Index);

    return PacketSize;
}
ttc_radio_errorcode_e radio_cc1120_set_device_address(ttc_radio_generic_t* Radio_Generic, u8_t NewDeviceAddress) {
    radio_cc1120_driver_t* DriverCfg = radio_cc1120_get_config(Radio_Generic); // checks pointer too

    ttc_radio_errorcode_e Error = tre_OK;
    if (Radio_Generic->DeviceAddress != NewDeviceAddress)
    {
        LockTransaction(DriverCfg->SPI_Index, radio_cc1120_set_device_address);
        if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_DEV_ADDR, NewDeviceAddress);
        Radio_Generic->DeviceAddress = NewDeviceAddress;
        UnlockTransaction(DriverCfg->SPI_Index);
    }
    return Error;
}
ttc_radio_errorcode_e radio_cc1120_set_channel(ttc_radio_generic_t* Radio_Generic, u32_t NewChannelIndex) {
    radio_cc1120_driver_t* DriverCfg = radio_cc1120_get_config(Radio_Generic); // checks pointer too
    ttc_radio_errorcode_e Error = tre_OK;

    DriverCfg = DriverCfg; //no warning
    //ToDo: Channel hopping without recalibration not supported by CC1120 refer to UserManual Page 67 for further Information
    //No specific channel Register avaliable hence no fast switching with only one register possible
    Assert_Radio(FALSE,ec_NotImplemented); //D
    return Error;
}
ttc_radio_errorcode_e radio_cc1120_set_power_tx(ttc_radio_generic_t* Radio_Generic, s8_t Level) {
    radio_cc1120_driver_t* DriverCfg = radio_cc1120_get_config(Radio_Generic); // checks pointer too

    ttc_radio_errorcode_e Error = tre_OK;

    // determine power-setting for 868MHz according to cc1120- user manual Page 41
    u8_t PA_Power_Ramp_new = 0;
    if((Level<=14)&&(Level>=-16)){
        PA_Power_Ramp_new = (Level+18)*2-1;
    }else {
        if(Level<=-16)
            PA_Power_Ramp_new = 3;
        if(Level >=14)
            PA_Power_Ramp_new = 63;
    }

    u8_t PA_Power_Ramp_current = 0 ;

    LockTransaction(DriverCfg->SPI_Index, radio_cc1120_set_power_tx);
    if (!Error) PA_Power_Ramp_current = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_PA_CFG2);//read current configuration
    PA_Power_Ramp_current &= ~0x3F; //reset the last 6 Bits (PA_CFG2.PA_POWER_RAMP)
    PA_Power_Ramp_current |= (PA_Power_Ramp_new& 0x3F);
    if (!Error) Error = radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_PA_CFG2, PA_Power_Ramp_current);//write back to register
    Radio_Generic->LevelTx = Level;
    UnlockTransaction(DriverCfg->SPI_Index);

    return Error;
}
ttc_radio_errorcode_e radio_cc1120_set_operating_mode(ttc_radio_generic_t* Radio_Generic) {
    radio_cc1120_driver_t* DriverCfg = radio_cc1120_get_config(Radio_Generic); // checks pointer too
    ttc_radio_errorcode_e Error = tre_OK;
    Base_t TimeOut = 10000;
    LockTransaction(DriverCfg->SPI_Index, radio_cc1120_set_operating_mode);
    radio_cc1120_marcstate_e CurrentMode = _radio_cc1120_get_state(Radio_Generic);//radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_MARCSTATE);

    if (CurrentMode == rcm_RX_FIFO_ERR) {
        _radio_cc1120_handle_error(Radio_Generic, tre_RxFIFO_Overrun, FALSE);
    }
    if (CurrentMode == rcm_TX_FIFO_ERR) {
        _radio_cc1120_handle_error(Radio_Generic, tre_TxFIFO_Underrun, FALSE);
    }

    switch (Radio_Generic->Mode) {
    case trm_Idle: {
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SIDLE);
        break;
    }
    case trm_Reset: {
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SRES);
        break;
    }
    case trm_PowerOff:
    case trm_Sleep: {
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SPWD);
        break;
    }
    case trm_PowerOn: {
        if (!Error) Error = radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SNOP);
        if (!Error) Error = radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SNOP);
        if (!Error) Error = radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SNOP);

        // reload configuration
        if (!Error) Error = radio_cc1120_init(Radio_Generic);
    }
    case trm_Transmit: {
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_STX);
        break;
    }
    case trm_Receive: {
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SRX);
        UnlockTransaction(DriverCfg->SPI_Index);
        TimeOut = 10;
        while (radio_cc1120_check_mode(Radio_Generic) && (TimeOut > 0)) {
            TimeOut--;
            radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SRX);
            ttc_task_yield();
        }
        LockTransaction(DriverCfg->SPI_Index, radio_cc1120_set_operating_mode);
        break;
    }
    case trm_TransmitReceive: {
    default:
            Assert_Radio(0, ec_InvalidArgument); // not supported by this radio
            break;
        }
    }

    UnlockTransaction(DriverCfg->SPI_Index);
    TimeOut = 10000;
    while (radio_cc1120_check_mode(Radio_Generic) && (TimeOut > 0)) {
        TimeOut--;
        ttc_task_yield();
    }
    if (TimeOut == 0)
        Error = tre_TimeOut;

    return Error;
}
BOOL radio_cc1120_check_mode(ttc_radio_generic_t* Radio_Generic) {
    radio_cc1120_driver_t* DriverCfg = radio_cc1120_get_config(Radio_Generic); // checks pointer too

    LockTransaction(DriverCfg->SPI_Index, radio_cc1120_set_operating_mode);
    BOOL IllegalMode = FALSE;

    switch (Radio_Generic->Mode) {
    case trm_Idle: {
        if (_radio_cc1120_get_state(Radio_Generic) != rcm_IDLE)
            IllegalMode = TRUE;
        break;
    }
    case trm_PowerOn: {
        radio_cc1120_marcstate_e CurrentMode = _radio_cc1120_get_state(Radio_Generic);
        if ( (CurrentMode != rcm_IDLE) && (CurrentMode != rcm_TX) && (CurrentMode != rcm_RX) )
            IllegalMode = TRUE;
    }
    case trm_Transmit: {
        radio_cc1120_marcstate_e CurrentMode = _radio_cc1120_get_state(Radio_Generic);
        if (CurrentMode != rcm_TX)
            IllegalMode = TRUE;
        break;
    }
    case trm_Receive: {
        radio_cc1120_marcstate_e CurrentMode = _radio_cc1120_get_state(Radio_Generic);;
        if (CurrentMode != rcm_RX)
            IllegalMode = TRUE;
        break;
    }
    default: break;
    }
    UnlockTransaction(DriverCfg->SPI_Index);

    return IllegalMode;
}
u8_t radio_cc1120_get_spi_index(u8_t RadioIndex) {
    Assert_Radio(RadioIndex > 0, ec_InvalidArgument); // logical index starts at 1

    switch (RadioIndex) {
#ifdef TTC_RADIO1_SPI_INDEX
    case 1: return TTC_RADIO1_SPI_INDEX;
#endif
#ifdef TTC_RADIO2_SPI_INDEX
    case 2: return TTC_RADIO2_SPI_INDEX;
#endif
#ifdef TTC_RADIO3_SPI_INDEX
    case 3: return TTC_RADIO3_SPI_INDEX;
#endif
#ifdef TTC_RADIO4_SPI_INDEX
    case 4: return TTC_RADIO4_SPI_INDEX;
#endif
#ifdef TTC_RADIO5_SPI_INDEX
    case 5: return TTC_RADIO5_SPI_INDEX;
#endif
    default: break;
    }
    Assert_Radio(0, ec_InvalidConfiguration); // indexed radio is not activated!
    return 0;
}
u8_t ISR_Call_DEBUG_VALUE = 0; //D
radio_cc1120_driver_t* radio_cc1120_get_config(ttc_radio_generic_t* Radio_Generic) {
    Assert_Radio(Radio_Generic, ec_InvalidArgument);

    radio_cc1120_driver_t* DriverCfg = (radio_cc1120_driver_t*) Radio_Generic->DriverCfg;
    if (!DriverCfg) {
        DriverCfg                = (radio_cc1120_driver_t*) ttc_heap_alloc_zeroed( sizeof(radio_cc1120_driver_t) );
        Radio_Generic->DriverCfg = (ttc_radio_driver_t*) DriverCfg;
        DriverCfg->SPI_Index     = radio_cc1120_get_spi_index(Radio_Generic->LogicalIndex);
        DriverCfg->RadioIndex    = Radio_Generic->LogicalIndex;
        DriverCfg->Lock          = ttc_mutex_create();
    }
    return DriverCfg;
}
ttc_radio_errorcode_e radio_cc1120_LBT_enable(ttc_radio_generic_t* Radio_Generic, bool enable){
    Assert_Radio(Radio_Generic, ec_InvalidArgument);

    radio_cc1120_driver_t* DriverCfg = radio_cc1120_get_config(Radio_Generic); // checks pointer too
    ttc_radio_errorcode_e Error = tre_OK;
    u8_t currentPKT_CFG2 = 0 ;

    if(Radio_Generic->Flags.Bit.LBT != 0){
        LockTransaction(DriverCfg->SPI_Index, radio_cc1120_LBT_enable);
        if(enable == TRUE){ //LBT is supported
            //LBT activation according to CC1120 UserManual Page 40
            currentPKT_CFG2         =  radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_PKT_CFG2);
            currentPKT_CFG2         &= ~0x1C;//~0b00011100; //clear CCA_Mode Bits
            currentPKT_CFG2         |=  0x10;// 0b10000; //set CCA_Mode = 0b100
            //ToDo: 0bxxxx generates GCC Warning maybe implement Registers so that single Bits can be set or cleared?!
            if (!Error) Error       =  radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_PKT_CFG2, currentPKT_CFG2);
        }else {
            u8_t PKT_CFG2origConfig =  SMARTRF_SETTING_PKT_CFG2; //get original config from SmartRF settings
            PKT_CFG2origConfig      &= 0x1C;  // 0b00011100; //clear other bits except CCA_Mode bits
            currentPKT_CFG2         =  radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_PKT_CFG2);
            currentPKT_CFG2         &= ~0x1C; //~0b00011100; //clear CCA_Mode Bits
            currentPKT_CFG2         |= PKT_CFG2origConfig; // apply original configuration form SmartRF settings
            //ToDo: 0bxxxx generates GCC Warning maybe implement Registers so that single Bits can be set or cleared?!
            //restore PKT_CFG2.CCA_Mode configuration from SmartRF config
            if (!Error) Error       =  radio_cc1120_spi_register_write_single(DriverCfg, rcr_CC112X_PKT_CFG2, currentPKT_CFG2);
        }
        UnlockTransaction(DriverCfg->SPI_Index);
    }else{
        return tre_NotImplemented;
    }

    return Error;
}
void _radio_cc1120_isr_receive(physical_index_t PhysicalIndex, void* Argument) {
    (void) PhysicalIndex;
    Assert_Radio(Argument, ec_InvalidArgument);

    ttc_radio_generic_t* Radio_Generic = (ttc_radio_generic_t*) Argument;
    if (Radio_Generic->MutexPacketReceived)
        ttc_mutex_unlock(Radio_Generic->MutexPacketReceived);

}
u8_t _radio_cc1120_get_state(ttc_radio_generic_t* Radio_Generic) {
    Assert_Radio(Radio_Generic, ec_InvalidArgument);
    radio_cc1120_driver_t* DriverCfg = (radio_cc1120_driver_t*) Radio_Generic->DriverCfg;

    u8_t MarcState;
    MarcState = radio_cc1120_spi_register_read(DriverCfg,rcr_CC112X_MARCSTATE);
    MarcState &= 0x1F;
    return MarcState;
}
ttc_radio_errorcode_e _radio_cc1120_handle_error(ttc_radio_generic_t* Radio_Generic, ttc_radio_errorcode_e Error, BOOL UnlockTransaction) {
    radio_cc1120_driver_t* DriverCfg = radio_cc1120_get_config(Radio_Generic); // checks pointer too

    ttc_radio_errorcode_e Error_return = tre_OK;

#ifdef TTC_DEBUG_RADIO
    Radio_Generic->Amount_Error_Handler_Calls++; //D
    Assert_Radio(Radio_Generic->Amount_Error_Handler_Calls<50, ec_Debug);//D
#endif
    if (Error == tre_TX_Timeout){
#ifdef TTC_DEBUG_RADIO
        Radio_Generic->Amount_Error_TX_Timeout++; //D
        Assert_Radio(Radio_Generic->Amount_Error_TX_Timeout<50, ec_Debug);//D
#endif
        if(_radio_cc1120_get_state(Radio_Generic) == rcm_RX){ // assume that we missed the TX State because radio is allready back in RX State
#ifdef TTC_DEBUG_RADIO
            Radio_Generic->Amount_Error_Missed_TXStates++; //D
#endif
            Error_return = tre_OK;
        }
        else {
            Assert_Radio(FALSE, ec_UNKNOWN);
            //assert if state is != rcm_RX because then we have an unexpected state
            //this assert could be replaced by another  _radio_cc1120_handle_error call with tre_UnexpectedState
            //this requires recursive locks.
        }
    } else if(Error == tre_RxCorruptPacket){
#ifdef TTC_DEBUG_RADIO
        Radio_Generic->Amount_Error_RxCorruptPacket++; //D
#endif
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SFRX);
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SRX);
    }
    else if(Error == tre_UnexpectedState){
#ifdef TTC_DEBUG_RADIO
        Radio_Generic->Amount_Error_UnexpectedState++; //D
#endif
        u8_t CurrentState = _radio_cc1120_get_state(Radio_Generic) ;
        if(CurrentState == rcm_TX_FIFO_ERR){
#ifdef TTC_DEBUG_RADIO
            Radio_Generic->Amount_Error_TXFIFO_UNDERFLOW++; //D
#endif
            radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SFTX);
        }
        else if(CurrentState == rcm_RX_FIFO_ERR){
#ifdef TTC_DEBUG_RADIO
            Radio_Generic->Amount_Error_RXFIFO_OVERFLOW++; //D
#endif
            radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SFRX);
        }else{
            radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SFRX);
            radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SIDLE);
            radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SRX);
            Assert_Radio(FALSE, ec_Debug);//D
        }
    }
    else if(Error==tre_RxFIFO_Overrun){
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SFRX);
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SIDLE);
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SRX);
#ifdef TTC_DEBUG_RADIO
        Radio_Generic->Amount_Error_RXFIFO_OVERFLOW++; //D
#endif
    }
    else if(Error==tre_TxFIFO_Underrun){
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SFTX);
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SIDLE);
        //radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SRX);
#ifdef TTC_DEBUG_RADIO
        Radio_Generic->Amount_Error_TXFIFO_UNDERFLOW++; //D
#endif
    }
    else {
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SFTX);
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SFRX);
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SIDLE);
        if (!Error_return) Error_return = radio_cc1120_init(Radio_Generic);
        radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SRX);
    }
    if (Error_return)
        Assert_Halt_EC(ec_UNKNOWN); // Break here


    UnlockTransaction(DriverCfg->SPI_Index);

    ttc_radio_set_operating_mode(Radio_Generic->LogicalIndex, Radio_Generic->Mode); //switch radio back to its configured Mode

    if (UnlockTransaction){

    }
    else {
        LockTransaction(DriverCfg->SPI_Index,_radio_cc1120_handle_error); //Lock the SPI before returning
    }
    return Error_return;
}

//} Implemented Functions
