/* RX filter BW = 100.000000 */
/* Address config = No address check */
/* Packet length = 255 */
/* Symbol rate = 38.3835 */
/* PA ramping = true */
/* Performance mode = High Performance */
/* Carrier frequency = 868.289917 */
/* Bit rate = 38.3835 */
/* Packet bit length = 0 */
/* Whitening = true */
/* Manchester enable = false */
/* Modulation format = 2-GFSK */
/* Packet length mode = Variable */
/* Device address = 0 */
/* TX power = 15 */
/* Deviation = 20.629883 */
/***************************************************************
 *  SmartRF Studio(tm) Export
 *
 *  Radio register settings specifed with C-code
 *  compatible #define statements.
 *
 *  RF device: CC1120
 *
 ***************************************************************/
#ifndef SMARTRF_CC1120_EXTERNAL
//The following defines are needed for duty cycle calculation
#define SMARTRF_SETTING_DATARATE           38383   //data rate in bits/sec
#define SMARTRF_SETTING_PREAMBLE_SIZE      4       //Amount Preamble Bytes
#define SMARTRF_SETTING_AMOUNT_SYNC_WORDS  4       //Amount Sync Words inserted by Radio
#define SMARTRF_SETTING_LENGTHFIELD_SIZE   1       //The size of the length field
#define SMARTRF_SETTING_ADDRESSFIELD_SIZE  1       //The size of the address field
#define SMARTRF_SETTING_CRC_SIZE           1       //The size of the attached CRC checksum


#ifndef SMARTRF_CC1120_H
#define SMARTRF_CC1120_H

#define SMARTRF_RADIO_CC1120
#define SMARTRF_SETTING_IOCFG3             0x06
#define SMARTRF_SETTING_IOCFG2             0x01
#define SMARTRF_SETTING_IOCFG1             0x30
#define SMARTRF_SETTING_IOCFG0             0x30
#define SMARTRF_SETTING_SYNC3              0xCC
#define SMARTRF_SETTING_SYNC2              0xBB
#define SMARTRF_SETTING_SYNC1              0xAA
#define SMARTRF_SETTING_SYNC0              0x33
#define SMARTRF_SETTING_SYNC_CFG1          0x08
#define SMARTRF_SETTING_SYNC_CFG0          0x17
#define SMARTRF_SETTING_DEVIATION_M        0x52
#define SMARTRF_SETTING_MODCFG_DEV_E       0x0D
#define SMARTRF_SETTING_DCFILT_CFG         0x15
#define SMARTRF_SETTING_PREAMBLE_CFG1      0x18
#define SMARTRF_SETTING_PREAMBLE_CFG0      0x2A
#define SMARTRF_SETTING_FREQ_IF_CFG        0x3A
#define SMARTRF_SETTING_IQIC               0x00
#define SMARTRF_SETTING_CHAN_BW            0x02
#define SMARTRF_SETTING_MDMCFG1            0x46
#define SMARTRF_SETTING_MDMCFG0            (0x05 & (~0x40))
#define SMARTRF_SETTING_DRATE2             0x93
#define SMARTRF_SETTING_DRATE1             0xA7
#define SMARTRF_SETTING_DRATE0             0x00
#define SMARTRF_SETTING_AGC_REF            0x3C
#define SMARTRF_SETTING_AGC_CS_THR         0x2D
#define SMARTRF_SETTING_AGC_GAIN_ADJUST    0x00
#define SMARTRF_SETTING_AGC_CFG3           0x91
#define SMARTRF_SETTING_AGC_CFG2           0x20
#define SMARTRF_SETTING_AGC_CFG1           0xA9
#define SMARTRF_SETTING_AGC_CFG0           0xC0
#define SMARTRF_SETTING_FIFO_CFG           0xFF
#define SMARTRF_SETTING_DEV_ADDR           0x00
#define SMARTRF_SETTING_SETTLING_CFG       0x03
#define SMARTRF_SETTING_FS_CFG             0x12
#define SMARTRF_SETTING_WOR_CFG1           0x08
#define SMARTRF_SETTING_WOR_CFG0           0x21
#define SMARTRF_SETTING_WOR_EVENT0_MSB     0x00
#define SMARTRF_SETTING_WOR_EVENT0_LSB     0x00
#define SMARTRF_SETTING_PKT_CFG2           0x10
#define SMARTRF_SETTING_PKT_CFG1           0x75
#define SMARTRF_SETTING_PKT_CFG0           0x20
#define SMARTRF_SETTING_RFEND_CFG1         0x37
#define SMARTRF_SETTING_RFEND_CFG0         0x30
#define SMARTRF_SETTING_PA_CFG2            0x7F
#define SMARTRF_SETTING_PA_CFG1            0x56
#define SMARTRF_SETTING_PA_CFG0            0x7B
#define SMARTRF_SETTING_PKT_LEN            0xFF
#define SMARTRF_SETTING_IF_MIX_CFG         0x00
#define SMARTRF_SETTING_FREQOFF_CFG        0x20
#define SMARTRF_SETTING_TOC_CFG            0x0A
#define SMARTRF_SETTING_MARC_SPARE         0x00
#define SMARTRF_SETTING_ECG_CFG            0x00
#define SMARTRF_SETTING_SOFT_TX_DATA_CFG   0x00
#define SMARTRF_SETTING_EXT_CTRL           0x01
#define SMARTRF_SETTING_RCCAL_FINE         0x00
#define SMARTRF_SETTING_RCCAL_COARSE       0x00
#define SMARTRF_SETTING_RCCAL_OFFSET       0x00
#define SMARTRF_SETTING_FREQOFF1           0x00
#define SMARTRF_SETTING_FREQOFF0           0x00
#define SMARTRF_SETTING_FREQ2              0x6C
#define SMARTRF_SETTING_FREQ1              0x89
#define SMARTRF_SETTING_FREQ0              0x47
#define SMARTRF_SETTING_IF_ADC2            0x02
#define SMARTRF_SETTING_IF_ADC1            0xA6
#define SMARTRF_SETTING_IF_ADC0            0x04
#define SMARTRF_SETTING_FS_DIG1            0x00
#define SMARTRF_SETTING_FS_DIG0            0x5F
#define SMARTRF_SETTING_FS_CAL3            0x00
#define SMARTRF_SETTING_FS_CAL2            0x20
#define SMARTRF_SETTING_FS_CAL1            0x40
#define SMARTRF_SETTING_FS_CAL0            0x0E
#define SMARTRF_SETTING_FS_CHP             0x28
#define SMARTRF_SETTING_FS_DIVTWO          0x03
#define SMARTRF_SETTING_FS_DSM1            0x00
#define SMARTRF_SETTING_FS_DSM0            0x33
#define SMARTRF_SETTING_FS_DVC1            0xFF
#define SMARTRF_SETTING_FS_DVC0            0x17
#define SMARTRF_SETTING_FS_LBI             0x00
#define SMARTRF_SETTING_FS_PFD             0x50
#define SMARTRF_SETTING_FS_PRE             0x6E
#define SMARTRF_SETTING_FS_REG_DIV_CML     0x14
#define SMARTRF_SETTING_FS_SPARE           0xAC
#define SMARTRF_SETTING_FS_VCO4            0x10
#define SMARTRF_SETTING_FS_VCO3            0x00
#define SMARTRF_SETTING_FS_VCO2            0x49
#define SMARTRF_SETTING_FS_VCO1            0x9C
#define SMARTRF_SETTING_FS_VCO0            0xB4
#define SMARTRF_SETTING_GBIAS6             0x00
#define SMARTRF_SETTING_GBIAS5             0x02
#define SMARTRF_SETTING_GBIAS4             0x00
#define SMARTRF_SETTING_GBIAS3             0x00
#define SMARTRF_SETTING_GBIAS2             0x10
#define SMARTRF_SETTING_GBIAS1             0x00
#define SMARTRF_SETTING_GBIAS0             0x00
#define SMARTRF_SETTING_IFAMP              0x01
#define SMARTRF_SETTING_LNA                0x01
#define SMARTRF_SETTING_RXMIX              0x01
#define SMARTRF_SETTING_XOSC5              0x0E
#define SMARTRF_SETTING_XOSC4              0xA0
#define SMARTRF_SETTING_XOSC3              0x03
#define SMARTRF_SETTING_XOSC2              0x04
#define SMARTRF_SETTING_XOSC1              0x03
#define SMARTRF_SETTING_XOSC0              0x00
#define SMARTRF_SETTING_ANALOG_SPARE       0x00
#define SMARTRF_SETTING_PA_CFG3            0x00
#define SMARTRF_SETTING_IRQ0M              0x00
#define SMARTRF_SETTING_IRQ0F              0x00
#define SMARTRF_SETTING_WOR_TIME1          0x00
#define SMARTRF_SETTING_WOR_TIME0          0x00
#define SMARTRF_SETTING_WOR_CAPTURE1       0x00
#define SMARTRF_SETTING_WOR_CAPTURE0       0x00
#define SMARTRF_SETTING_BIST               0x00
#define SMARTRF_SETTING_DCFILTOFFSET_I1    0xF2
#define SMARTRF_SETTING_DCFILTOFFSET_I0    0xA1
#define SMARTRF_SETTING_DCFILTOFFSET_Q1    0x0B
#define SMARTRF_SETTING_DCFILTOFFSET_Q0    0x8D
#define SMARTRF_SETTING_IQIE_I1            0x00
#define SMARTRF_SETTING_IQIE_I0            0x00
#define SMARTRF_SETTING_IQIE_Q1            0x00
#define SMARTRF_SETTING_IQIE_Q0            0x00
#define SMARTRF_SETTING_RSSI1              0xFC
#define SMARTRF_SETTING_RSSI0              0x47
#define SMARTRF_SETTING_MARCSTATE          0x41
#define SMARTRF_SETTING_LQI_VAL            0x00
#define SMARTRF_SETTING_PQT_SYNC_ERR       0x7F
#define SMARTRF_SETTING_DEM_STATUS         0x00
#define SMARTRF_SETTING_FREQOFF_EST1       0xFF
#define SMARTRF_SETTING_FREQOFF_EST0       0x81
#define SMARTRF_SETTING_AGC_GAIN3          0x27
#define SMARTRF_SETTING_AGC_GAIN2          0xD1
#define SMARTRF_SETTING_AGC_GAIN1          0x00
#define SMARTRF_SETTING_AGC_GAIN0          0x3F
#define SMARTRF_SETTING_SOFT_RX_DATA_OUT   0x00
#define SMARTRF_SETTING_SOFT_TX_DATA_IN    0x00
#define SMARTRF_SETTING_ASK_SOFT_RX_DATA   0x30
#define SMARTRF_SETTING_RNDGEN             0x7F
#define SMARTRF_SETTING_MAGN2              0x00
#define SMARTRF_SETTING_MAGN1              0x00
#define SMARTRF_SETTING_MAGN0              0xA8
#define SMARTRF_SETTING_ANG1               0x01
#define SMARTRF_SETTING_ANG0               0xAC
#define SMARTRF_SETTING_CHFILT_I2          0x0F
#define SMARTRF_SETTING_CHFILT_I1          0xFF
#define SMARTRF_SETTING_CHFILT_I0          0x8D
#define SMARTRF_SETTING_CHFILT_Q2          0x00
#define SMARTRF_SETTING_CHFILT_Q1          0x00
#define SMARTRF_SETTING_CHFILT_Q0          0xEF
#define SMARTRF_SETTING_GPIO_STATUS        0x00
#define SMARTRF_SETTING_FSCAL_CTRL         0x01
#define SMARTRF_SETTING_PHASE_ADJUST       0x00
#define SMARTRF_SETTING_PARTNUMBER         0x48
#define SMARTRF_SETTING_PARTVERSION        0x21
#define SMARTRF_SETTING_SERIAL_STATUS      0x00
#define SMARTRF_SETTING_RX_STATUS          0x11
#define SMARTRF_SETTING_TX_STATUS          0x00
#define SMARTRF_SETTING_MARC_STATUS1       0x00
#define SMARTRF_SETTING_MARC_STATUS0       0x00
#define SMARTRF_SETTING_PA_IFAMP_TEST      0x00
#define SMARTRF_SETTING_FSRF_TEST          0x00
#define SMARTRF_SETTING_PRE_TEST           0x00
#define SMARTRF_SETTING_PRE_OVR            0x00
#define SMARTRF_SETTING_ADC_TEST           0x00
#define SMARTRF_SETTING_DVC_TEST           0x0B
#define SMARTRF_SETTING_ATEST              0x40
#define SMARTRF_SETTING_ATEST_LVDS         0x00
#define SMARTRF_SETTING_ATEST_MODE         0x00
#define SMARTRF_SETTING_XOSC_TEST1         0x3C
#define SMARTRF_SETTING_XOSC_TEST0         0x00
#define SMARTRF_SETTING_RXFIRST            0x00
#define SMARTRF_SETTING_TXFIRST            0x00
#define SMARTRF_SETTING_RXLAST             0x00
#define SMARTRF_SETTING_TXLAST             0x00
#define SMARTRF_SETTING_NUM_TXBYTES        0x00
#define SMARTRF_SETTING_NUM_RXBYTES        0x00
#define SMARTRF_SETTING_FIFO_NUM_TXBYTES   0x0F
#define SMARTRF_SETTING_FIFO_NUM_RXBYTES   0x00

#endif //SMARTRF_CC1120_H

#endif //SMARTRF_CC1120_EXTERNAL


