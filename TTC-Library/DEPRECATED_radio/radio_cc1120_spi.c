/*{ cc1120::.c ************************************************

 Program Name: cc1120
 
 This source is published under the GNU LESSER GENERAL PUBLIC LICENSE (LGPL).
 See file LEGAL.txt in this software package for details.

 Authors: <Sascha Poggemann>, <Gregor Rebel>

 Description: This is an implementation for the Texas Instruments cc1120 Tranceiver
 
}*/
//{ Private Defines/ TypeDefs ****************************************************

#define waitforSO_low 1

//}  Private Defines
//{ Includes *********************************************************************

#include "radio_cc1120_spi.h"

//} Includes
//{ Private Structures/ Enums ****************************************************

//} Private Structures/ Enums
//{ Global Variables *************************************************************

// moved into radio_cc1120_driver_t

//} Global Variables
//{ Private Function prototypes **************************************************


//} Private Function prototypes
//{ Implemented Functions ********************************************************

ttc_radio_errorcode_e radio_cc1120_spi_get_defaults(u8_t SPI_Index, radio_cc1120_driver_t* DriverCfg) {
    (void) SPI_Index; // argument not used here
    Assert_Radio(DriverCfg, ec_InvalidArgument);

    return tre_OK;
}
ttc_radio_errorcode_e radio_cc1120_spi_init(radio_cc1120_driver_t* DriverCfg) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);
    ttc_radio_errorcode_e Error = tre_OK;

    if (!DriverCfg->Lock)
        DriverCfg->Lock = ttc_mutex_create();

    if (!ttc_spi_check_initialized(DriverCfg->SPI_Index)) { // init SPI unit

        DriverCfg->SPI_Cfg = ttc_spi_get_configuration(DriverCfg->SPI_Index);
        Assert(DriverCfg->SPI_Cfg, ec_NULL);

        DriverCfg->SPI_Cfg->Flags.Bits.Master = 1;
        DriverCfg->SPI_Cfg->Flags.Bits.HardwareNSS =0;
        DriverCfg->SPI_Cfg->Flags.Bits.CRC8 = 0;
        DriverCfg->SPI_Cfg->Flags.Bits.ClockIdleHigh = 0;
        DriverCfg->SPI_Cfg->Flags.Bits.ClockPhase2ndEdge = 0;
        DriverCfg->SPI_Cfg->BaudRate = 4500000 / 8; // cc1120 is much slower than stm32

        ttc_spi_init(DriverCfg->SPI_Index);
    }
    else
        // SPI already initialized: get pointer to its current configuration
        DriverCfg->SPI_Cfg = ttc_spi_get_configuration(DriverCfg->SPI_Index);

#ifdef TTC_RADIO1
        ttc_gpio_init(TTC_RADIO1_PIN_SPI_NSS, tgm_output_push_pull);
        ttc_gpio_set(TTC_RADIO1_PIN_SPI_NSS); //will pull the NSS pin of Radio high
#endif
#ifdef TTC_RADIO2
        ttc_gpio_init(TTC_RADIO2_PIN_SPI_NSS, tgm_output_push_pull);
        ttc_gpio_set(TTC_RADIO2_PIN_SPI_NSS); //will pull the NSS pin of Radio high
#endif
#ifdef TTC_RADIO3
        ttc_gpio_init(TTC_RADIO3_PIN_SPI_NSS, tgm_output_push_pull);
        ttc_gpio_set(TTC_RADIO3_PIN_SPI_NSS); //will pull the NSS pin of Radio high
#endif
#ifdef TTC_RADIO4
        ttc_gpio_init(TTC_RADIO4_PIN_SPI_NSS, tgm_output_push_pull);
        ttc_gpio_set(TTC_RADIO4_PIN_SPI_NSS); //will pull the NSS pin of Radio high
#endif
#ifdef TTC_RADIO5
        ttc_gpio_init(TTC_RADIO5_PIN_SPI_NSS, tgm_output_push_pull);
        ttc_gpio_set(TTC_RADIO5_PIN_SPI_NSS); //will pull the NSS pin of Radio high
#endif

    switch (DriverCfg->RadioIndex) { // configure additional gpios used for radio
#ifdef TTC_RADIO1
    case 1: {
        ttc_gpio_init(TTC_RADIO1_PIN_GDO0,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO1_PIN_GDO2,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO1_PIN_GDO3,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO1_PIN_RESET,   tgm_output_push_pull);
        ttc_gpio_set (TTC_RADIO1_PIN_RESET);

        break;
    }
#endif
#ifdef TTC_RADIO2
    case 2: {
        ttc_gpio_init(TTC_RADIO2_PIN_GDO0,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO2_PIN_GDO2,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO2_PIN_GDO3,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO2_PIN_RESET,   tgm_output_push_pull);
        ttc_gpio_set (TTC_RADIO2_PIN_RESET);
        break;
    }
#endif
#ifdef TTC_RADIO3
    case 3: {
        ttc_gpio_init(TTC_RADIO3_PIN_GDO0,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO3_PIN_GDO2,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO3_PIN_GDO3,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO3_PIN_RESET,   tgm_output_push_pull);
        ttc_gpio_set (TTC_RADIO3_PIN_RESET);

        break;
    }
#endif
#ifdef TTC_RADIO4
    case 4: {
        ttc_gpio_init(TTC_RADIO4_PIN_GDO0,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO4_PIN_GDO2,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO4_PIN_GDO3,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO4_PIN_RESET,   tgm_output_push_pull);
        ttc_gpio_set (TTC_RADIO4_PIN_RESET);
        break;
    }
#endif
#ifdef TTC_RADIO5
    case 5: {
        ttc_gpio_init(TTC_RADIO5_PIN_GDO0,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO5_PIN_GDO2,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO5_PIN_GDO3,    tgm_input_floating);
        ttc_gpio_init(TTC_RADIO5_PIN_RESET,   tgm_output_push_pull);
        ttc_gpio_set (TTC_RADIO5_PIN_RESET);
        break;
    }
#endif
    default: Assert(0, ec_InvalidConfiguration); // indexed radio not activated
    }

    return Error;
}
//ttc_radio_errorcode_e radio_cc1120_spi_register_set_bits(radio_cc1120_driver_t* DriverCfg,   u16_t Register, u8_t BitMask) {
//    u8_t Data = 0;
//    //radio_cc1120_spi_register_access(DriverCfg, Register, &Data, cam_single_read, 1);
//    Data = radio_cc1120_spi_register_read(DriverCfg,Register);
//    Data |= BitMask;
//    //return radio_cc1120_spi_register_write_single(DriverCfg, Register, Data);
//    return radio_cc1120_spi_register_write_single(DriverCfg,Register,Data);
//}
//ttc_radio_errorcode_e radio_cc1120_spi_register_clr_bits(radio_cc1120_driver_t* DriverCfg, u16_t Register, u8_t BitMask) {
//    u8_t Data = 0;
//    //radio_cc1120_spi_register_access(DriverCfg, Register, &Data, cam_single_read, 1);
//    Data = radio_cc1120_spi_register_read(DriverCfg,Register);
//    Data &= ~BitMask;
//    return radio_cc1120_spi_register_write_single(DriverCfg, Register, Data);
//}
u8_t radio_cc1120_spi_register_read(radio_cc1120_driver_t* DriverCfg, u16_t Register) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);

    ttc_radio_errorcode_e Error = tre_OK;
    u8_t SPI_Index = DriverCfg->SPI_Index;
    u8_t Data = 0;

    _radio_cc1120_portNSS_Clr(DriverCfg);
    _radio_cc1120_spi_wait_so_low(DriverCfg);

    u8_t Temp = Register >> 8; // get High Byte to determine if extended Register space should be accessed
    if(Temp!= 0x2F){ // for regular register space
        //radio_cc1120_spi_register_access(DriverCfg, Register, &Data, cam_single_read, 1);
        Register |= CC1120_RW_Bit;
        Temp = Register & 0xFF; //get low Byte
        if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Temp, 1); //send Address
        if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Status
        Temp = CC1120_dummy_byte;
        if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Temp, 1); //send dummy
        if (!Error) Error = ttc_spi_read_byte(SPI_Index,&Data, 0); //read Data
    }else{ //extended Register space
        Assert_Radio(Temp == 0x2F,ec_InvalidArgument); //D
        Temp |= CC1120_RW_Bit;
        if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Temp, 1); //send High Byte for extended address space
        if (!Error) Error = ttc_spi_read_byte(SPI_Index, &Temp, 0); //read 0 byte as returned by the cc1120
        Temp = Register & 0xFF;
        if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Temp, 1); //send register address
        if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Status
        Temp = CC1120_dummy_byte;
        if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Temp, 1); //send dummy
        if (!Error) Error = ttc_spi_read_byte(SPI_Index,&Data, 0); //read Data

    }
    _radio_cc1120_portNSS_Set(DriverCfg);

    return Data;
}
/*u8_t radio_cc1120_spi_register_read_extended(radio_cc1120_driver_t* DriverCfg, u16_t Register) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);

    ttc_radio_errorcode_e Error = tre_OK;
    u8_t SPI_Index = DriverCfg->SPI_Index;
    u8_t Data = 0;
    u8_t Temp = 0 ;
    _radio_cc1120_portNSS_Clr(DriverCfg);
    _radio_cc1120_spi_wait_so_low(DriverCfg);
    //radio_cc1120_spi_register_access(DriverCfg, Register, &Data, cam_single_read, 1);
    Temp = Register >> 8; // get High Byte
    Assert_Radio(Temp == 0x2F,ec_InvalidArgument); //D
    Temp |= CC1120_RW_Bit;
    if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Temp, 1); //send High Byte for extended address space
    if (!Error) Error = ttc_spi_read_byte(SPI_Index, &Temp, 0); //read 0 byte as returned by the cc1120
    Temp = Register & 0xFF;
    if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Temp, 1); //send register address
    if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Status
    Temp = CC1120_dummy_byte;
    if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Temp, 1); //send dummy
    if (!Error) Error = ttc_spi_read_byte(SPI_Index,&Data, 0); //read Data
    _radio_cc1120_portNSS_Set(DriverCfg);

    return Data;
}*/
ttc_radio_errorcode_e radio_cc1120_spi_register_write_single(radio_cc1120_driver_t* DriverCfg, u16_t Register, u8_t Data) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);

    ttc_radio_errorcode_e Error = tre_OK;
    u8_t SPI_Index = DriverCfg->SPI_Index;
    _radio_cc1120_portNSS_Clr(DriverCfg);
    _radio_cc1120_spi_wait_so_low(DriverCfg);
    u8_t Temp = Register >> 8; // get High Byte to determine if extended Register space should be accessed
    if(Temp!= 0x2F){ // for regular register space 
        Temp = Register & 0xFF; //get low Byte
        if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Temp, 1); //send Address
        if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Status
    }else{ //extended Register space
        Assert_Radio(Temp == 0x2F,ec_InvalidArgument); //D
        Temp = (Register >> 8); // get HighByte (command for extended register space)
        if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Temp, 1);
        if (!Error) Error = ttc_spi_read_byte(SPI_Index, &Temp, 0); //Status will return 0
        Temp = (u8_t) Register & 0xFF; //get LowByte (8-bit register address)
        if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Temp, 1);
        if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Status
    }
    if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Data, 1); // sending first byte only
    if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Data
    _radio_cc1120_portNSS_Set(DriverCfg);

    return Error;

}
/*ttc_radio_errorcode_e radio_cc1120_spi_register_write_16(radio_cc1120_driver_t* DriverCfg, u16_t Register, u8_t Data) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);

    ttc_radio_errorcode_e Error = tre_OK;
    u8_t SPI_Index = DriverCfg->SPI_Index;
    u8_t Dummy = 0;
    _radio_cc1120_portNSS_Clr(DriverCfg);
    _radio_cc1120_spi_wait_so_low(DriverCfg);
    //NOTE: RW Bit and Bust Bit must not be set
    Dummy = (Register >> 8); // get HighByte (command for extended register space)
    if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Dummy, 1);
    if (!Error) Error = ttc_spi_read_byte(SPI_Index, &Dummy, 0); //Status will return 0
    Dummy = (u8_t) Register & 0xFF; //get LowByte (8-bit register address)
    if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Dummy, 1);
    if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Status

    if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Data, 1); // send Data
    if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Status
    _radio_cc1120_portNSS_Set(DriverCfg);

    return Error;
}
ttc_radio_errorcode_e radio_cc1120_spi_register_write_8(radio_cc1120_driver_t* DriverCfg, u8_t Register, u8_t Data) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);

    ttc_radio_errorcode_e Error = tre_OK;
    u8_t SPI_Index = DriverCfg->SPI_Index;

    _radio_cc1120_portNSS_Clr(DriverCfg);
    _radio_cc1120_spi_wait_so_low(DriverCfg);
    //NOTE: RW Bit and Bust Bit must not be set
    if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Register, 1);
    if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Status
    if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Data, 1); // sending first byte only
    if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Data
    _radio_cc1120_portNSS_Set(DriverCfg);

    return Error;
}*/
/*ttc_radio_errorcode_e radio_cc1120_spi_register_access(radio_cc1120_driver_t* DriverCfg,   u16_t Register, u8_t *Buffer, cc1120_access_mode_e AccessMode, int Amount) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);
    Assert_Radio(Buffer, ec_InvalidArgument);
    u8_t SPI_Index = DriverCfg->SPI_Index;

    ttc_radio_errorcode_e Error = tre_OK;
    u8_t AddressByte = Register;

    _radio_cc1120_portNSS_Clr(DriverCfg);
    _radio_cc1120_spi_wait_so_low(DriverCfg);

    if (0) ttc_spi_read_byte(SPI_Index, &AddressByte, 1); //read any pending bytes in RX Buffer
    AddressByte = Register;
    switch (AccessMode) {

    case cam_burst_write: {
        AddressByte |= CC1120_BURST_Bit;

        if (!Error) Error = ttc_spi_send_raw(SPI_Index, &AddressByte, 1);
        if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Status
        if (!Error) Error = ttc_spi_send_raw(SPI_Index, Buffer, Amount);
        if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Status

        break;
    }
    case cam_burst_read: {  //Maybe need to add a SPI Rx Fifo flush here to get the latest data
        AddressByte |= CC1120_RW_Bit | CC1120_BURST_Bit;

        if (!Error) Error = ttc_spi_send_raw(SPI_Index, &AddressByte, 1);
        if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 100); //read Status
        AddressByte = CC1120_dummy_byte;
        for(int i = 0 ; i<=Amount; i++) {
            if (!Error) Error = ttc_spi_send_raw(SPI_Index, &AddressByte, 1);  //sends a dummy byte so that the cc1120 can send the received byte.
            if (!Error) Error = ttc_spi_read_byte(SPI_Index, Buffer, 0); //read the data from the spi registers with Timeout==0 (block here if error occurs).
            Buffer ++;
        }
        break;
    }
    case cam_single_write: {
        if (!Error) Error = ttc_spi_send_raw(SPI_Index, &AddressByte, 1);
        if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Status
        if (!Error) Error = ttc_spi_send_raw(SPI_Index, Buffer, 1); // sending first byte only
        if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Data

        break;
    }
    case cam_single_read: {// Maybe need to add a SPI Rx Fifo flush here to get the latest data
        if (Register>0x2F) {
            AddressByte |= CC1120_BURST_Bit; //different meaning of the bust bits for status registers
        }
        AddressByte |= CC1120_RW_Bit;

        if (!Error) Error = ttc_spi_send_raw(SPI_Index, &AddressByte, 1); //send Address
        if (!Error) Error = ttc_spi_read_byte(SPI_Index, &DriverCfg->CurrentStatus, 0); //read Status
        AddressByte = CC1120_dummy_byte;
        if (!Error) Error = ttc_spi_send_raw(SPI_Index, &AddressByte, 1); //send dummy
        if (!Error) Error = ttc_spi_read_byte(SPI_Index, Buffer, 0); //read Data

        break;
    }
    default:
        Error = tre_InvalidArgument; break; // invalid access mode
    }
    _radio_cc1120_portNSS_Set(DriverCfg);

    return Error;
}*/
ttc_radio_errorcode_e radio_cc1120_spi_reset(radio_cc1120_driver_t* DriverCfg) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);

    return radio_cc1120_spi_strobe(DriverCfg, rcs_CC112X_SRES);
}
ttc_radio_errorcode_e radio_cc1120_spi_strobe(radio_cc1120_driver_t* DriverCfg, u8_t Command) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);
    u8_t SPI_Index = DriverCfg->SPI_Index;

    ttc_radio_errorcode_e Error =tre_OK;

    _radio_cc1120_portNSS_Clr(DriverCfg);
    if (!Error) Error = _radio_cc1120_spi_wait_so_low(DriverCfg);
    if (!Error) Error = ttc_spi_send_raw(SPI_Index, &Command, 1);
    if (!Error) Error = ttc_spi_read_byte(SPI_Index, &(DriverCfg->CurrentStatus), 0); //read last status byte

    _radio_cc1120_portNSS_Set(DriverCfg);

    return Error;
}
ttc_radio_errorcode_e radio_cc1120_spi_get_chip_id(radio_cc1120_driver_t* DriverCfg) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);
    //return  radio_cc1120_spi_register_access(DriverCfg, rcr_CC112X_PARTNUMBER, &DriverCfg->ChipID, cam_single_read, 1);
    DriverCfg->ChipID = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_PARTVERSION);
    return tre_OK;


}
ttc_radio_errorcode_e radio_cc1120_spi_get_chip_version(radio_cc1120_driver_t* DriverCfg) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);
    //ttc_radio_errorcode_e Error = radio_cc1120_spi_register_access(DriverCfg, rcr_CC112X_PARTVERSION, &DriverCfg->ChipVersion, cam_single_read, 1);
    ttc_radio_errorcode_e Error = tre_OK;
    DriverCfg->ChipVersion = radio_cc1120_spi_register_read(DriverCfg, rcr_CC112X_PARTNUMBER);
    switch (DriverCfg->RadioIndex) {
#ifdef TTC_RADIO1
    case 1:
        Assert(DriverCfg->ChipVersion == TTC_RADIO1_CC1120_SPI_CHIP_VERSION, ec_DeviceNotFound);
        break;
#endif
#ifdef TTC_RADIO2
    case 2:
        Assert(DriverCfg->ChipVersion == TTC_RADIO2_CC1120_SPI_CHIP_VERSION, ec_DeviceNotFound);
        break;
#endif
#ifdef TTC_RADIO3
    case 3:
        Assert(DriverCfg->ChipVersion == TTC_RADIO3_CC1120_SPI_CHIP_VERSION, ec_DeviceNotFound);
        break;
#endif
#ifdef TTC_RADIO4
    case 4:
        Assert(DriverCfg->ChipVersion == TTC_RADIO4_CC1120_SPI_CHIP_VERSION, ec_DeviceNotFound);
        break;
#endif
#ifdef TTC_RADIO5
    case 5:
        Assert(DriverCfg->ChipVersion == TTC_RADIO5_CC1120_SPI_CHIP_VERSION, ec_DeviceNotFound);
        break;
#endif
    default:
        Assert_Radio(0, ec_InvalidConfiguration);
    }

    return Error;
}
ttc_radio_errorcode_e _radio_cc1120_spi_wait_chip_ready(radio_cc1120_driver_t* DriverCfg) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);

#ifdef CC1120_GDO2_CHIP_RDY

    u32_t time = RADIO_CC1120_SPI_TIMEOUT;
    while (ttc_gpio_get_variable(&DriverCfg->Port_GDO2_1)==1) { // Wait for SO Pin to go low
        if (RADIO_CC1120_SPI_TIMEOUT>0) {
            time --;
            if (time==0)
                return tre_TimeOut;
        }
        ttc_task_yield();
    }
#endif

    return tre_OK;
}
ttc_radio_errorcode_e _radio_cc1120_spi_wait_so_low(radio_cc1120_driver_t* DriverCfg) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);

    ttc_Port_t* PortMISO = ttc_spi_get_port(DriverCfg->SPI_Index, tsp_MISO);
    u32_t time = RADIO_CC1120_SPI_TIMEOUT;
    while (ttc_gpio_get_variable(PortMISO)==1) { //Wait for SO Pin to go low
        if (RADIO_CC1120_SPI_TIMEOUT>0) {
            time --;
            if (time==0)
                return tre_TimeOut;
        }
        ttc_task_yield();
    }

    return tre_OK;
}
ttc_radio_errorcode_e _radio_cc1120_spi_wait_gdo0_low(radio_cc1120_driver_t* DriverCfg) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);
    u32_t time = RADIO_CC1120_SPI_TIMEOUT;

    while (_radio_cc1120_portGDO0_Get(DriverCfg)==1) //Wait for SO Pin to go low
    {
        if (RADIO_CC1120_SPI_TIMEOUT>0) {
            time --;
            if (time==0)
                return tre_TimeOut;
        }
        ttc_task_yield();
    }

    return tre_OK;
}
ttc_radio_errorcode_e _radio_cc1120_spi_wait_gdo0_high(radio_cc1120_driver_t* DriverCfg) {
    Assert_Radio(DriverCfg, ec_InvalidArgument);
    u32_t time = RADIO_CC1120_SPI_TIMEOUT;

    while (_radio_cc1120_portGDO0_Get(DriverCfg)==0) //Wait for SO Pin to go low
    {
        if (RADIO_CC1120_SPI_TIMEOUT>0) {
            time --;
            if (time==0)
                return tre_TimeOut;
        }
        ttc_task_yield();
    }

    return tre_OK;
}
u8_t radio_cc1120_spi_calculate_rssi(u8_t rssi) {
    if (rssi >= 128) rssi -= 256;
    return (rssi >> 1) - 74;
}

BOOL _radio_cc1120_portGDO0_Get(radio_cc1120_driver_t* DriverCfg) {
    switch (DriverCfg->RadioIndex) {
#ifdef TTC_RADIO1
    case 1: {
        return ttc_gpio_get(TTC_RADIO1_PIN_GDO0);
    }
#endif
#ifdef TTC_RADIO2
    case 2: {
        return ttc_gpio_get(TTC_RADIO2_PIN_GDO0);
    }
#endif
#ifdef TTC_RADIO3
    case 3: {
        return ttc_gpio_get(TTC_RADIO3_PIN_GDO0);
    }
#endif
#ifdef TTC_RADIO4
    case 4: {
        return ttc_gpio_get(TTC_RADIO4_PIN_GDO0);
    }
#endif
#ifdef TTC_RADIO5
    case 5: {
        return ttc_gpio_get(TTC_RADIO5_PIN_GDO0);
    }
#endif
    default:
        Assert_Radio(0, ec_InvalidConfiguration);
        break;
    }
    return FALSE;
}
void _radio_cc1120_portNSS_Clr(radio_cc1120_driver_t* DriverCfg) {
    switch (DriverCfg->RadioIndex) {
#ifdef TTC_RADIO1
    case 1: {
        ttc_gpio_clr(TTC_RADIO1_PIN_SPI_NSS);
        break;
    }
#endif
#ifdef TTC_RADIO2
    case 2: {
        ttc_gpio_clr(TTC_RADIO2_PIN_SPI_NSS);
        break;
    }
#endif
#ifdef TTC_RADIO3
    case 3: {
        ttc_gpio_clr(TTC_RADIO3_PIN_SPI_NSS);
        break;
    }
#endif
#ifdef TTC_RADIO4
    case 4: {
        ttc_gpio_clr(TTC_RADIO4_PIN_SPI_NSS);
        break;
    }
#endif
#ifdef TTC_RADIO5
    case 5: {
        ttc_gpio_clr(TTC_RADIO5_PIN_SPI_NSS);
        break;
    }
#endif
    default:
        Assert_Radio(0, ec_InvalidConfiguration);
        break;
    }
}
void _radio_cc1120_portNSS_Set(radio_cc1120_driver_t* DriverCfg) {
    switch (DriverCfg->RadioIndex) {
#ifdef TTC_RADIO1
    case 1: {
        ttc_gpio_set(TTC_RADIO1_PIN_SPI_NSS);
        break;
    }
#endif
#ifdef TTC_RADIO2
    case 2: {
        ttc_gpio_set(TTC_RADIO2_PIN_SPI_NSS);
        break;
    }
#endif
#ifdef TTC_RADIO3
    case 3: {
        ttc_gpio_set(TTC_RADIO3_PIN_SPI_NSS);
        break;
    }
#endif
#ifdef TTC_RADIO4
    case 4: {
        ttc_gpio_set(TTC_RADIO4_PIN_SPI_NSS);
        break;
    }
#endif
#ifdef TTC_RADIO5
    case 5: {
        ttc_gpio_set(TTC_RADIO5_PIN_SPI_NSS);
        break;
    }
#endif
    default:
        Assert_Radio(0, ec_InvalidConfiguration);
        break;
    }
}

//} Implemented Functions
