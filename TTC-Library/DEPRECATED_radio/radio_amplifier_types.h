#ifndef RADIO_AMPLIFIER_TYPES_H
#define RADIO_AMPLIFIER_TYPES_H

/*{ radio_cc1190_types.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Basic defines, enums and structures for external radio amplifiers (E.g. cc1190)
 *
 * Include this file from 
 * architecture independent header files (ttc_XXX.h) and 
 * architecture depend      header files (e.g. radio_XXX.h)
 * 
}*/
#include "ttc_basic.h"

//{ Defines/ TypeDefs ****************************************************

//} Defines
//{ Structures/ Enums  ***************************************************


//}

#endif // RADIO_AMPLIFIER_TYPES_H
