/*{ radio_cc1190.c ************************************************

 This source is published under the GNU LESSER GENERAL PUBLIC LICENSE (LGPL).
 See file LEGAL.txt in this software package for details.

 Authors: <Sascha Poggemann>, <Gregor Rebel>

 Description: Low-level driver for radio amplifier cc1190 from Texas Instruments.
 
}*/

#include "radio_cc1190.h"

//{ Global Variables *************************************************************

//} Global Variables
//{ Implemented Functions ********************************************************

ttc_radio_errorcode_e radio_cc1190_init(ttc_radio_generic_t* RadioCfg) {

    switch (RadioCfg->LogicalIndex) {  // load port bits required to control amplifier as defined externally

#ifdef TTC_RADIO1_AMPLIFIER
    case 1:
        ttc_gpio_init2(TTC_RADIO1_AMPLIFIER_HGM,                tgm_output_push_pull, tgs_Min);
        ttc_gpio_init2(TTC_RADIO1_AMPLIFIER_PIN_PA_ENABLE,      tgm_output_push_pull, tgs_Min);
        ttc_gpio_init2(TTC_RADIO1_AMPLIFIER_PIN_LNA_ENABLE,     tgm_output_push_pull, tgs_Min);
        ttc_gpio_clr(TTC_RADIO1_AMPLIFIER_HGM);
        ttc_gpio_clr(TTC_RADIO1_AMPLIFIER_PIN_PA_ENABLE);
        ttc_gpio_clr(TTC_RADIO1_AMPLIFIER_PIN_LNA_ENABLE);
        break;
#endif
#ifdef TTC_RADIO2_AMPLIFIER
    case 2:
        ttc_gpio_init2(TTC_RADIO2_AMPLIFIER_HGM,                tgm_output_push_pull, tgs_Min);
        ttc_gpio_init2(TTC_RADIO2_AMPLIFIER_PIN_PA_ENABLE,      tgm_output_push_pull, tgs_Min);
        ttc_gpio_init2(TTC_RADIO2_AMPLIFIER_PIN_LNA_ENABLE,     tgm_output_push_pull, tgs_Min);
        ttc_gpio_clr(TTC_RADIO2_AMPLIFIER_HGM);
        ttc_gpio_clr(TTC_RADIO2_AMPLIFIER_PIN_PA_ENABLE);
        ttc_gpio_clr(TTC_RADIO2_AMPLIFIER_PIN_LNA_ENABLE);
        break;
#endif
#ifdef TTC_RADIO3_AMPLIFIER
    case 3:
        ttc_gpio_init2(TTC_RADIO3_AMPLIFIER_HGM,                tgm_output_push_pull, tgs_Min);
        ttc_gpio_init2(TTC_RADIO3_AMPLIFIER_PIN_PA_ENABLE,      tgm_output_push_pull, tgs_Min);
        ttc_gpio_init2(TTC_RADIO3_AMPLIFIER_PIN_LNA_ENABLE,     tgm_output_push_pull, tgs_Min);
        ttc_gpio_clr(TTC_RADIO3_AMPLIFIER_HGM);
        ttc_gpio_clr(TTC_RADIO3_AMPLIFIER_PIN_PA_ENABLE);
        ttc_gpio_clr(TTC_RADIO3_AMPLIFIER_PIN_LNA_ENABLE);
        break;
#endif
#ifdef TTC_RADIO4_AMPLIFIER
    case 4:
        ttc_gpio_init2(TTC_RADIO4_AMPLIFIER_HGM,                tgm_output_push_pull, tgs_Min);
        ttc_gpio_init2(TTC_RADIO4_AMPLIFIER_PIN_PA_ENABLE,      tgm_output_push_pull, tgs_Min);
        ttc_gpio_init2(TTC_RADIO4_AMPLIFIER_PIN_LNA_ENABLE,     tgm_output_push_pull, tgs_Min);
        ttc_gpio_clr(TTC_RADIO4_AMPLIFIER_HGM);
        ttc_gpio_clr(TTC_RADIO4_AMPLIFIER_PIN_PA_ENABLE);
        ttc_gpio_clr(TTC_RADIO4_AMPLIFIER_PIN_LNA_ENABLE);
        break;
#endif
#ifdef TTC_RADIO5_AMPLIFIER
    case 5:
        ttc_gpio_init2(TTC_RADIO5_AMPLIFIER_HGM,                tgm_output_push_pull, tgs_Min);
        ttc_gpio_init2(TTC_RADIO5_AMPLIFIER_PIN_PA_ENABLE,      tgm_output_push_pull, tgs_Min);
        ttc_gpio_init2(TTC_RADIO5_AMPLIFIER_PIN_LNA_ENABLE,     tgm_output_push_pull, tgs_Min);
        ttc_gpio_clr(TTC_RADIO5_AMPLIFIER_HGM);
        ttc_gpio_clr(TTC_RADIO5_AMPLIFIER_PIN_PA_ENABLE);
        ttc_gpio_clr(TTC_RADIO5_AMPLIFIER_PIN_LNA_ENABLE);
        break;
#endif

    default:
        RadioCfg->AMP_Initialized = FALSE;
        return tre_DeviceNotFound; // Indexed port pin not defined! Check your makefile.100_board_* file!
    }

    RadioCfg->AMP_Initialized = TRUE;
    return tre_OK;
}
void radio_cc1190_set_high_gain(ttc_radio_generic_t* RadioCfg, BOOL Active) {


    if(!RadioCfg->AMP_Initialized) { //only set ports if AMP has been inited before
        return;
    }
    switch (RadioCfg->LogicalIndex) {  // load port bits required to control amplifier as defined externally

#ifdef TTC_RADIO1_AMPLIFIER
    case 1:
        if (Active)
            ttc_gpio_set(TTC_RADIO1_AMPLIFIER_HGM);
        else
            ttc_gpio_clr(TTC_RADIO1_AMPLIFIER_HGM);
        break;
#endif
//#ifdef TTC_RADIO2_AMPLIFIER
    case 2:
        if (Active)
            ttc_gpio_set(TTC_RADIO2_AMPLIFIER_HGM);
        else
            ttc_gpio_clr(TTC_RADIO2_AMPLIFIER_HGM);
        break;
//#endif
#ifdef TTC_RADIO3_AMPLIFIER
    case 3:
        if (Active)
            ttc_gpio_set(TTC_RADIO3_AMPLIFIER_HGM);
        else
            ttc_gpio_clr(TTC_RADIO3_AMPLIFIER_HGM);
        break;
#endif
#ifdef TTC_RADIO4_AMPLIFIER
    case 4:
        if (Active)
            ttc_gpio_set(TTC_RADIO4_AMPLIFIER_HGM);
        else
            ttc_gpio_clr(TTC_RADIO4_AMPLIFIER_HGM);
        break;
#endif
#ifdef TTC_RADIO5_AMPLIFIER
    case 5:
        if (Active)
            ttc_gpio_set(TTC_RADIO5_AMPLIFIER_HGM);
        else
            ttc_gpio_clr(TTC_RADIO5_AMPLIFIER_HGM);
        break;
#endif
    default: Assert_Radio(0, ec_UNKNOWN); break; // Indexed port pin not defined! Check your makefile.100_board_* file!
    }
}
void radio_cc1190_set_mode(ttc_radio_generic_t* RadioCfg, ttc_radio_amplifier_mode_e Mode) {

    if (Mode == tram_Init)
        radio_cc1190_init(RadioCfg);

    if(!RadioCfg->AMP_Initialized) //only set ports if AMP has been inited before
        return;

    switch (Mode) { // check for high-gain mode
    case tram_TransmitHighGain:
    case tram_ReceiveHighGain:
        radio_cc1190_set_high_gain(RadioCfg, TRUE);
        break;
    default:
        radio_cc1190_set_high_gain(RadioCfg, FALSE);
        break;
    }

    switch (RadioCfg->LogicalIndex) {  // load port bits required to control amplifier as defined externally

#ifdef TTC_RADIO1_AMPLIFIER
    case 1: {
        switch (Mode) {
        case tram_TransmitHighGain:
        case tram_Transmit:
            ttc_gpio_clr(TTC_RADIO1_AMPLIFIER_PIN_LNA_ENABLE);
            ttc_gpio_set(TTC_RADIO1_AMPLIFIER_PIN_PA_ENABLE);
            break;
        case tram_ReceiveHighGain:
        case tram_Receive:
            ttc_gpio_clr(TTC_RADIO1_AMPLIFIER_PIN_PA_ENABLE);
            ttc_gpio_set(TTC_RADIO1_AMPLIFIER_PIN_LNA_ENABLE);
            break;
        case tram_PowerDown:
            ttc_gpio_clr(TTC_RADIO1_AMPLIFIER_PIN_LNA_ENABLE);
            ttc_gpio_clr(TTC_RADIO1_AMPLIFIER_PIN_PA_ENABLE);
            break;
        default: break;
        }
        break;
    }
#endif
#ifdef TTC_RADIO2_AMPLIFIER
    case 2: {
        switch (Mode) {
        case tram_TransmitHighGain:
        case tram_Transmit:
            ttc_gpio_clr(TTC_RADIO2_AMPLIFIER_PIN_LNA_ENABLE);
            ttc_gpio_set(TTC_RADIO2_AMPLIFIER_PIN_PA_ENABLE);
            break;
        case tram_ReceiveHighGain:
        case tram_Receive:
            ttc_gpio_clr(TTC_RADIO2_AMPLIFIER_PIN_PA_ENABLE);
            ttc_gpio_set(TTC_RADIO2_AMPLIFIER_PIN_LNA_ENABLE);
            break;
        case tram_PowerDown:
            ttc_gpio_clr(TTC_RADIO2_AMPLIFIER_PIN_PA_ENABLE);
            ttc_gpio_clr(TTC_RADIO2_AMPLIFIER_PIN_LNA_ENABLE);
            break;
        default: break;
        }
        break;
    }
#endif
#ifdef TTC_RADIO3_AMPLIFIER
    case 3: {
        switch (Mode) {
        case tram_TransmitHighGain:
        case tram_Transmit:
            ttc_gpio_clr(TTC_RADIO3_AMPLIFIER_PIN_LNA_ENABLE);
            ttc_gpio_set(TTC_RADIO3_AMPLIFIER_PIN_PA_ENABLE);
            break;
        case tram_ReceiveHighGain:
        case tram_Receive:
            ttc_gpio_clr(TTC_RADIO3_AMPLIFIER_PIN_PA_ENABLE);
            ttc_gpio_set(TTC_RADIO3_AMPLIFIER_PIN_LNA_ENABLE);
            break;
        case tram_PowerDown:
            ttc_gpio_clr(TTC_RADIO3_AMPLIFIER_PIN_PA_ENABLE);
            ttc_gpio_clr(TTC_RADIO3_AMPLIFIER_PIN_LNA_ENABLE);
            break;
        default: break;
        }
        break;
    }
#endif
#ifdef TTC_RADIO4_AMPLIFIER
    case 4: {
        switch (Mode) {
        case tram_TransmitHighGain:
        case tram_Transmit:
            ttc_gpio_clr(TTC_RADIO4_AMPLIFIER_PIN_LNA_ENABLE);
            ttc_gpio_set(TTC_RADIO4_AMPLIFIER_PIN_PA_ENABLE);
            break;
        case tram_ReceiveHighGain:
        case tram_Receive:
            ttc_gpio_clr(TTC_RADIO4_AMPLIFIER_PIN_PA_ENABLE);
            ttc_gpio_set(TTC_RADIO4_AMPLIFIER_PIN_LNA_ENABLE);
            break;
        case tram_PowerDown:
            ttc_gpio_clr(TTC_RADIO4_AMPLIFIER_PIN_PA_ENABLE);
            ttc_gpio_clr(TTC_RADIO4_AMPLIFIER_PIN_LNA_ENABLE);
            break;
        default: break;
        }
        break;
    }
#endif
#ifdef TTC_RADIO5_AMPLIFIER
    case 5: {
        switch (Mode) {
        case tram_TransmitHighGain:
        case tram_Transmit:
            ttc_gpio_clr(TTC_RADIO5_AMPLIFIER_PIN_LNA_ENABLE);
            ttc_gpio_set(TTC_RADIO5_AMPLIFIER_PIN_PA_ENABLE);
            break;
        case tram_ReceiveHighGain:
        case tram_Receive:
            ttc_gpio_clr(TTC_RADIO5_AMPLIFIER_PIN_PA_ENABLE);
            ttc_gpio_set(TTC_RADIO5_AMPLIFIER_PIN_LNA_ENABLE);
            break;
        case tram_PowerDown:
            ttc_gpio_clr(TTC_RADIO5_AMPLIFIER_PIN_PA_ENABLE);
            ttc_gpio_clr(TTC_RADIO5_AMPLIFIER_PIN_LNA_ENABLE);
            break;
        default: break;
        }
        break;
    }
#endif
    default: Assert_Radio(0, ec_InvalidConfiguration); break; // Indexed radio not activated! Check your makefile.100_board_* file!
    }
}

//} Implemented Functions
