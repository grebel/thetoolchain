/*{ iXRG_radio.c ******************************************************

A radio that is connected via USART.
You are able to provide/register your own RX_Interpreter function that will parse incomming Data.


}*/

#include "radio_serial.h"


// stores released memory blocks for reuse as transmit or receive blocks
// Note: Pushes to this queue are allowed from outside interrupt service routine only!
ttc_queue_pointers_t* RS_QEB = NULL;

//{ Function definitions *************************************************
ttc_heap_array_define(radio_serial_generic_t   *, Radio_Serial_Configs, RADIO_SERIAL_AMOUNT_INTERFACES);


radio_serial_generic_t* radio_serial_get_configuration(u8_t LogicalIndex) {
    Assert_RS(LogicalIndex > 0, ec_InvalidArgument); // logical USART-indices start at 1 !
    Assert_RS(LogicalIndex <= RADIO_SERIAL_AMOUNT_INTERFACES, ec_InvalidArgument);
    radio_serial_generic_t * RS_Generic = A(Radio_Serial_Configs, LogicalIndex-1);

    if (!RS_Generic) {
        RS_Generic = A(Radio_Serial_Configs, LogicalIndex-1) = ttc_heap_alloc_zeroed(sizeof(radio_serial_generic_t));
        //ttc_usart_reset(LogicalIndex);

        RS_Generic->Size_Queue_Rx = RADIO_SERIAL_BLOCK_SIZE;

        RS_Generic->USART_Logical_Index = tupi_NotImplemented ;
#ifdef RADIO_SERIAL_USART_LOGICALINDEX1
        if(LogicalIndex == 1){

            RS_Generic->USART_Logical_Index = RADIO_SERIAL_USART_LOGICALINDEX1;
        }
#elif RADIO_SERIAL_USART_LOGICALINDEX2
        if(LogicalIndex == 2){

            RS_Generic->USART_Logical_Index = RADIO_SERIAL_USART_LOGICALINDEX2;
        }
#elif RADIO_SERIAL_USART_LOGICALINDEX3
        if(LogicalIndex == 3){

            RS_Generic->USART_Logical_Index = RADIO_SERIAL_USART_LOGICALINDEX3;
        }
#elif RADIO_SERIAL_USART_LOGICALINDEX4
        if(LogicalIndex == 4){

            RS_Generic->USART_Logical_Index = RADIO_SERIAL_USART_LOGICALINDEX4;
        }
#elif RADIO_SERIAL_USART_LOGICALINDEX5
        if(LogicalIndex == 5){

            RS_Generic->USART_Logical_Index = RADIO_SERIAL_USART_LOGICALINDEX5;
        }
#else
#error no serial Radio defined
#endif
        Assert_RS(RS_Generic->USART_Logical_Index >= tupi_NotImplemented, ec_InvalidConfiguration); //no USART_Logical_Index set, there must be an issue with the configuration

        RS_Generic->USART_Generic = ttc_usart_get_configuration(RS_Generic->USART_Logical_Index);

        ttc_usart_config_t * USART_Generic = RS_Generic->USART_Generic;
        Assert_RS(USART_Generic, ec_NULL);
        if(1) USART_Generic->Flags.Bits.DelayedTransmits = 1;
        // change some settings
        if(USART_Generic->Flags.Bits.IrqOnRxNE == 0)
            USART_Generic->Flags.Bits.IrqOnRxNE  = 1;   // enable IRQ on Rx-buffer not empty
        if(USART_Generic->Char_EndOfLine == 0)
            USART_Generic->Char_EndOfLine        = 13;  // packets end at carriage return
        if(USART_Generic->BaudRate == 0)
            USART_Generic->BaudRate              = RADIO_SERIAL_BAUDRATE_DEF; //baudrate used for communication

        //USART_Generic->receiveSingleByte = _radio_serial_data_grabber_single_byte;
        if(LogicalIndex == 1){
            USART_Generic->receiveSingleByte = _radio_serial_data_grabber_single_byte1;
        }else if(LogicalIndex == 2){
            USART_Generic->receiveSingleByte = _radio_serial_data_grabber_single_byte2;
        }else if(LogicalIndex == 3){
            USART_Generic->receiveSingleByte = _radio_serial_data_grabber_single_byte3;
        }else if(LogicalIndex == 4){
            USART_Generic->receiveSingleByte = _radio_serial_data_grabber_single_byte4;
        }else if(LogicalIndex == 5){
            USART_Generic->receiveSingleByte = _radio_serial_data_grabber_single_byte5;
        }
        USART_Generic->Size_Queue_Rx = RADIO_SERIAL_BLOCK_SIZE;//D one TCM300 Packet needs to fit into queue

    }
    return RS_Generic;
}

ttc_radio_errorcode_e radio_serial_init(u8_t LogicalIndex) {

    Assert_RS(LogicalIndex > 0, ec_InvalidArgument);
    Assert_RS(LogicalIndex <= RADIO_SERIAL_AMOUNT_INTERFACES, ec_InvalidArgument);

    radio_serial_generic_t * RS_Generic = radio_serial_get_configuration(LogicalIndex);
    if (RS_Generic->Queue_Rx == NULL) { // first initialisation of this USART
        RS_Generic->Queue_Rx = ttc_queue_byte_create(RS_Generic->Size_Queue_Rx);
    }


    //EnOcean TCM300 Module connected via USART
    // initialize first USART with default settings
    ttc_channel_errorcode_e Error_USART = tce_OK;

    Error_USART = ttc_usart_init(RS_Generic->USART_Logical_Index);

    // set first logical USART as standard output (otherwise last initialized one would be used)
    Error_USART = ttc_usart_stdout_set(RS_Generic->USART_Logical_Index);
    // set usart as stdout interface
    ttc_string_register_stdout(ttc_usart_stdout_send_block, ttc_usart_stdout_send_string);

    if(Error_USART != tce_OK){
        Assert_RS(FALSE, ec_Debug);//D
        return tre_DeviceNotReady;
    }

    BOOL TaskCreated = ttc_task_create(_task_radio_serial,              // function to start as thread
                       "taskRadioTCM300",            // thread name (just for debugging)
                       TTC_TASK_MINIMAL_STACK_SIZE,  // stack size
                       (void *) RS_Generic,      // passed as argument to taskRadio()
                       1,                            // task priority (higher values mean more process time)
                       (void *) NULL                 // can return a handle to created task
                       );
    if(!TaskCreated)
        return tre_UnknownError;

    return tre_OK;

}
void _task_radio_serial(void *TaskArgument) {

    radio_serial_generic_t * RS_Generic =  (radio_serial_generic_t *) TaskArgument;
    Assert_RS(RS_Generic, ec_InvalidArgument);
    Assert_RS(RS_Generic->Queue_Rx, ec_Malloc);

    u8_t Received_Byte    = 0 ;  //the last received byte from the queue

    Assert_RS(RS_Generic->Queue_Rx, ec_Debug);//D
    while (1) {

        if (ttc_queue_byte_pull_front(RS_Generic->Queue_Rx,&Received_Byte) == tqe_OK) { // single byte received: process it



        }else {
            ttc_task_yield();
            ttc_task_usleep(100);
        }
    }
}
ttc_radio_errorcode_e radio_serial_send_data(u8_t LogicalIndex ,u8_t * Buffer, u8_t Amount){

    Assert_RS(Buffer, ec_InvalidArgument);
    Assert_RS(LogicalIndex > 0, ec_InvalidArgument);
    radio_serial_generic_t* RS_Generic = radio_serial_get_configuration(LogicalIndex);

    ttc_usart_send_raw(RS_Generic->USART_Logical_Index, Buffer, Amount);

    return tre_OK;
}
void _radio_serial_data_grabber_single_byte(u8_t LogicalIndex, void * USART_Generic, u8_t Byte){
    radio_serial_generic_t* RS_Generic = radio_serial_get_configuration(LogicalIndex);
    if(RS_Generic->Queue_Rx)
        ttc_queue_byte_push_back(RS_Generic->Queue_Rx,Byte);
}
void _radio_serial_data_grabber_single_byte1(void * USART_Generic, u8_t Byte){
    Assert_RS(USART_Generic, ec_InvalidArgument);
    _radio_serial_data_grabber_single_byte(1, USART_Generic, Byte);

}
void _radio_serial_data_grabber_single_byte2(void * USART_Generic, u8_t Byte){
    Assert_RS(USART_Generic, ec_InvalidArgument);
    _radio_serial_data_grabber_single_byte(2, USART_Generic, Byte);

}
void _radio_serial_data_grabber_single_byte3(void * USART_Generic, u8_t Byte){
    Assert_RS(USART_Generic, ec_InvalidArgument);
    _radio_serial_data_grabber_single_byte(3, USART_Generic, Byte);

}
void _radio_serial_data_grabber_single_byte4(void * USART_Generic, u8_t Byte){
    Assert_RS(USART_Generic, ec_InvalidArgument);
    _radio_serial_data_grabber_single_byte(4, USART_Generic, Byte);

}
void _radio_serial_data_grabber_single_byte5(void * USART_Generic, u8_t Byte){
    Assert_RS(USART_Generic, ec_InvalidArgument);
    _radio_serial_data_grabber_single_byte(5, USART_Generic, Byte);

}

//ttc_radio_errorcode_e radio_serial_register_rx_intperpreter(u8_t LogicalIndex, void (*UCB)(struct radio_serial_generic_s * RS_Generic, u8_t data)){
ttc_radio_errorcode_e radio_serial_register_rx_intperpreter(u8_t LogicalIndex, void (*UCB)(u8_t Index, u8_t Data)){
    Assert_RS(LogicalIndex > 0, ec_InvalidArgument);
    radio_serial_generic_t* RS_Generic = radio_serial_get_configuration(LogicalIndex);
    Assert_RS(RS_Generic, ec_InvalidArgument);
    Assert_RS(UCB, ec_InvalidArgument);
    if(!RS_Generic->UCB_rx_intperpreter){
        RS_Generic->UCB_rx_intperpreter = UCB;
    }else {
        Assert_RS(FALSE, ec_Debug); //User callback function already registered!!!
        return tre_DeviceNotReady;
    }

    return tre_OK ;
}
u8_t RS_Amount_BlocksAllocated;
ttc_heap_block_t* _radio_serial_get_empty_block() {

    if (RS_QEB == NULL) {
        RS_QEB = ttc_queue_pointer_create(RADIO_SERIAL_MAX_MEMORY_BUFFERS);
    }

    ttc_heap_block_t* Block = NULL;
    while (Block == NULL) {

        ttc_queue_error_e  Error = ttc_queue_pointer_pull_front(RS_QEB, (void**) &Block);

        if (Error != tqe_OK) { // no released memory block available: alloc new block
            if (RS_Amount_BlocksAllocated < RADIO_SERIAL_MAX_MEMORY_BUFFERS) {
                Block = ttc_heap_alloc_block(RADIO_SERIAL_BLOCK_SIZE, 0, _radio_serial_release_block);
                RS_Amount_BlocksAllocated++;
            }else {
                Assert_RS(FALSE,ec_Malloc);//should not get here, where are all the buffers gone
            }

        }

        //Block->Buffer = ( (u8_t*) Block ) + sizeof(ttc_heap_block_t); // correct pointer to buffer in case this block has been used as a virtual block
        Assert_RS(Block->MaxSize == RADIO_SERIAL_BLOCK_SIZE, ec_ArrayIndexOutOfBound);

        if (Block == NULL)  // got no block: wait for some while
            ttc_task_yield();
    }
    ttc_heap_block_use(Block);

    return Block;
}
void _radio_serial_release_block(ttc_heap_block_t* Block) {
    Assert_RS(Block != NULL, ec_NULL);


    if (Block->releaseBuffer == _radio_serial_release_block) {

        ttc_queue_error_e Error = ttc_queue_pointer_push_back(RS_QEB, Block);
        Assert_RS(Error == tqe_OK, ec_UNKNOWN); // could not release memory block (maybe queue is too small?)

    }
    else { // release foreign block
        Assert(Block->releaseBuffer, ec_NULL);
        ttc_heap_block_release(Block);
    }
}

//} Function definitions

