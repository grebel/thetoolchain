/*{ radio_cc1101_registers.h ********************************************************

 This source is published under the GNU LESSER GENERAL PUBLIC LICENSE (LGPL).
 See file LEGAL.txt in this software package for details.

 Authors: <Sascha Poggemann>, <Gregor Rebel>

 Description: Register definitions and structures of spi-controlled cc1101 radio transceiver

}*/
#ifndef RADIO_CC1101_REGISTERS_H
#define RADIO_CC1101_REGISTERS_H

//{ register address definitions ***************************************************

//{ Configuration Registers
typedef enum {

    rcr_IOCFG2     = 0x00,        // GDO2 output pin configuration
    rcr_IOCFG1     = 0x01,        // GDO1 output pin configuration
    rcr_IOCFG0     = 0x02,        // GDO0 output pin configuration
    rcr_FIFOTHR    = 0x03,        // RX FIFO and TX FIFO thresholds
    rcr_SYNC1      = 0x04,        // Sync word, high byte
    rcr_SYNC0      = 0x05,        // Sync word, low byte
    rcr_PKTLEN     = 0x06,        // Packet length
    rcr_PKTCTRL1   = 0x07,        // Packet automation control
    rcr_PKTCTRL0   = 0x08,        // Packet automation control
    rcr_ADDR       = 0x09,        // Device address
    rcr_CHANNR     = 0x0A,        // Channel number
    rcr_FSCTRL1    = 0x0B,        // Frequency synthesizer control
    rcr_FSCTRL0    = 0x0C,        // Frequency synthesizer control
    rcr_FREQ2      = 0x0D,        // Frequency control word, high byte
    rcr_FREQ1      = 0x0E,        // Frequency control word, middle byte
    rcr_FREQ0      = 0x0F,        // Frequency control word, low byte
    rcr_MDMCFG4    = 0x10,        // Modem configuration
    rcr_MDMCFG3    = 0x11,        // Modem configuration
    rcr_MDMCFG2    = 0x12,        // Modem configuration
    rcr_MDMCFG1    = 0x13,        // Modem configuration
    rcr_MDMCFG0    = 0x14,        // Modem configuration
    rcr_DEVIATN    = 0x15,        // Modem deviation setting
    rcr_MCSM2      = 0x16,        // Main Radio Cntrl State Machine config
    rcr_MCSM1      = 0x17,        // Main Radio Cntrl State Machine config
    rcr_MCSM0      = 0x18,        // Main Radio Cntrl State Machine config
    rcr_FOCCFG     = 0x19,        // Frequency Offset Compensation config
    rcr_BSCFG      = 0x1A,        // Bit Synchronization configuration
    rcr_AGCCTRL2   = 0x1B,        // AGC control
    rcr_AGCCTRL1   = 0x1C,        // AGC control
    rcr_AGCCTRL0   = 0x1D,        // AGC control
    rcr_WOREVT1    = 0x1E,        // High byte Event 0 timeout
    rcr_WOREVT0    = 0x1F,        // Low byte Event 0 timeout
    rcr_WORCTRL    = 0x20,        // Wake On Radio control
    rcr_FREND1     = 0x21,        // Front end RX configuration
    rcr_FREND0     = 0x22,        // Front end TX configuration
    rcr_FSCAL3     = 0x23,        // Frequency synthesizer calibration
    rcr_FSCAL2     = 0x24,        // Frequency synthesizer calibration
    rcr_FSCAL1     = 0x25,        // Frequency synthesizer calibration
    rcr_FSCAL0     = 0x26,        // Frequency synthesizer calibration
    rcr_RCCTRL1    = 0x27,        // RC oscillator configuration
    rcr_RCCTRL0    = 0x28,        // RC oscillator configuration
    rcr_FSTEST     = 0x29,        // Frequency synthesizer cal control
    rcr_PTEST      = 0x2A,        // Production test
    rcr_AGCTEST    = 0x2B,        // AGC test
    rcr_TEST2      = 0x2C,        // Various test settings
    rcr_TEST1      = 0x2D,        // Various test settings
    rcr_TEST0      = 0x2E,        // Various test settings

//}Configuration Registers
//{ Status registers

    rcr_PARTNUM    = 0x30,        // Part number
    rcr_VERSION    = 0x31,        // Current version number
    rcr_FREQEST    = 0x32,        // Frequency offset estimate
    rcr_LQI        = 0x33,        // Demodulator estimate for link quality
    rcr_RSSI       = 0x34,        // Received signal strength indication
    rcr_MARCSTATE  = 0x35,        // Control state machine state
    rcr_WORTIME1   = 0x36,        // High byte of WOR timer
    rcr_WORTIME0   = 0x37,        // Low byte of WOR timer
    rcr_PKTSTATUS  = 0x38,        // Current GDOx status and packet status
    rcr_VCO_VC_DAC = 0x39,        // Current setting from PLL cal module
    rcr_TXBYTES    = 0x3A,        // Underflow and # of bytes in TXFIFO
    rcr_RXBYTES    = 0x3B,        // Overflow and # of bytes in RXFIFO

//}Status registers
//{ Multi byte memory locations

    rcr_PATABLE    = 0x3E,        //
    rcr_TXFIFO     = 0x3F,        //
    rcr_RXFIFO     = 0x3F,        //

//}Multi byte memory locations
//{ Definitions for burst/single access to registers

    rcr_WRITE_BURST= 0x40,        //
    rcr_READ_SINGLE= 0x80,        //
    rcr_READ_BURST = 0xC0,        //

//}Definitions for burst/single access to registers
//{ Strobe commands

    rcr_SRES       = 0x30,        // Reset chip.
    rcr_SFSTXON    = 0X31,        // If in RX/TX: Go to a wait state where only the synthesizer is
                                  // running (for quick RX / TX turnaround).
    rcr_SXOFF      = 0x32,        // Turn off crystal oscillator.
    rcr_SCAL       = 0x33,        // Calibrate frequency synthesizer and turn it off
                                  // (enables quick start).
    rcr_SRX        = 0x34,        // Enable RX. Perform calibration first if coming from IDLE and
                                  // MCSM0.FS_AUT=1.
    rcr_STX        = 0x35,        // In IDLE state: Enable TX. Perform calibration first if
                                  // MCSM0.FS_AUT=1. If in RX state and CCA is enabled:
                                  // Only go to TX if channel is clear.
    rcr_SIDLE      = 0x36,        // Exit RX / TX, turn off frequency synthesizer and exit
                                  // Wake-On-Radio mode if applicable.
    rcr_SAFC       = 0x37,        // Perform AFC adjustment of the frequency synthesizer
    rcr_SWOR       = 0x38,        // Start automatic RX polling sequence (Wake-on-Radio)
    rcr_SPWD       = 0x39,        // Enter power down mode when CSn goes high.
    rcr_SFRX       = 0x3A,        // Flush the RX FIFO buffer.
    rcr_SFTX       = 0x3B,        // Flush the TX FIFO buffer.
    rcr_SWORRST    = 0x3C,        // Reset real time clock.
    rcr_SNOP       = 0x3D         // No operation. May be used to pad strobe commands to two
                                  // bytes for simpler software.
} radio_cc1101_register_e;
//}Strobe commands
/* ???
//{ Chip Status Byte

// Bit fields in the chip status byte
#define CC1101_STATUS_CHIP_RDYn_BM             0x80
#define CC1101_STATUS_STATE_BM                 0x70
#define CC1101_STATUS_FIFO_BYTES_AVAILABLE_BM  0x0F

// Chip states
#define CC1101_STATE_IDLE                      0x00
#define CC1101_STATE_RX                        0x10
#define CC1101_STATE_TX                        0x20
#define CC1101_STATE_FSTXON                    0x30
#define CC1101_STATE_CALIBRATE                 0x40
#define CC1101_STATE_SETTLING                  0x50
#define CC1101_STATE_RX_OVERFLOW               0x60
#define CC1101_STATE_TX_UNDERFLOW              0x70

//}Chip Status Byte
//{ Other register bit fields

#define CC1101_LQI_CRC_OK_BM                   0x80
#define CC1101_LQI_EST_BM                      0x7F

//}Other register bit fields
*/

//}register address definitions
//{ register content definitions ***************************************************

typedef struct { // 0x00: IOCFG2 – GDO2 Output Pin Configuration


    unsigned GDO2_CFG           : 6;    //Default is CHP_RDYn (See Table 41 on page 62).
    unsigned GDO2_INV           : 1;    //invert output, i.e. select active low (1) / high (0)
    unsigned Reserved           : 1;    //bit 7 not used

} __attribute__((__packed__)) IOCFG2_t;
typedef struct { //0x01: IOCFG1 – GDO1 Output Pin Configuration

    unsigned GDO1_CFG           : 6;    //Default is 3-state (See Table 41 on page 62).
    unsigned GDO1_INV           : 1;    //Invert output, i.e. select active low (1) / high (0)
    unsigned GDO_DS             : 1;    //Set high (1) or low (0) output drive strength on the GDO pins.

} __attribute__((__packed__)) GDO1_CFG_t;
typedef struct { //0x02: IOCFG0 – GDO0 Output Pin Configuration

    unsigned GDO0_CFG           : 6;    //Default is CLK_XOSC/192 (See Table 41 on page 62). It is recommended to disable the clock output in initialization, in order to optimize RF performance.
    unsigned GDO0_INV           : 1;    //Invert output, i.e. select active low (1) / high (0)
    unsigned TEMP_SENSOR_ENABLE : 1;    //Enable analog temperature sensor. Write 0 in all other register bits when using temperature sensor.

} __attribute__((__packed__)) IOCFG0_t;
typedef struct { //0x03: FIFOTHR – RX FIFO and TX FIFO Thresholds

    unsigned FIFO_THR             : 4;    //Set the threshold for the TX FIFO and RX FIFO. The threshold is exceeded when the number of bytes in the FIFO is equal to or higher than the threshold value.(Page 72)
    unsigned CLOSE_IN_RX          : 2;    //For more details, please see DN010 [8]
    //   Setting    RX Attenuation, Typical Values
    //  0 (00)      0 dB
    //  1 (01)      6 dB
    //  2 (10)      12 dB
    //  3 (11)      18 dB
    unsigned ADC_RETENTION          : 1;    //  0: TEST1 = 0x31 and TEST2= 0x88 when waking up from SLEEP
    //1: TEST1 = 0x35 and TEST2 = 0x81 when waking up from SLEEP
    unsigned Reserved               : 1;

} __attribute__((__packed__)) FIFOTHR_t;
typedef struct { //0x04: SYNC1 – Sync Word, High Byte

    unsigned SYNC                     : 8;    //8 MSB of 16-bit sync word

} __attribute__((__packed__)) SYNC1_t;
typedef struct { //0x05: SYNC0 – Sync Word, Low Byte

    unsigned SYNC                     : 8;    //8 LSB of 16-bit sync word

} __attribute__((__packed__)) SYNC0_t;
typedef struct { //0x04: SYNC1 – Sync Word, High Byte

    SYNC1_t SYNC1 ;
    SYNC0_t SYNC0 ;

} __attribute__((__packed__)) SYNC_t;
typedef struct { //0x06: PKTLEN – Packet Length

    unsigned PACKET_LENGTH            : 8;    // Indicates the packet length when fixed packet length mode is enabled.
    //If variable packet length mode is used, this value indicates the maximum packet length allowed.
    //This value must be different from 0.

} __attribute__((__packed__)) PKTLEN_t;
typedef struct { //0x07: PKTCTRL1 – Packet Automation Control

    unsigned ADR_CHK                  : 2;    //Controls address check configuration of received packages.
    //  Setting     Address check configuration
    //  0 (00)      No address check
    //  1 (01)      Address check, no broadcast
    //  2 (10)      Address check and 0 (0x00) broadcast
    //  3 (11)      Address check and 0 (0x00) and 255 (0xFF) broadcast

    unsigned APPEND_STATUS              : 1;    //When enabled, two status bytes will be appended to the payload of the packet. The status bytes contain RSSI and LQI values, as well as CRC OK

    unsigned CRC_AUTOFLUSH              : 1;    //Enable automatic flush of RX FIFO when CRC is not OK.
    //This requires that only one packet is in the RXIFIFO and that packet length is limited to the RX FIFO size.

    unsigned Reserved                   : 1;    //not used

    unsigned PQT                        : 3;    // Preamble quality estimator threshold. The preamble quality estimator
    //increases an internal counter by one each time a bit is received that is
    //different from the previous bit, and decreases the counter by 8 each time a
    //bit is received that is the same as the last bit.
    //A threshold of 4∙PQT for this counter is used to gate sync word detection.
    //When PQT=0 a sync word is always accepted.

} __attribute__((__packed__)) PKTCTRL1_t;
typedef struct { //0x08: PKTCTRL0 – Packet Automation Control

    unsigned LENGTH_CONFIG            : 2;     //Configure the packet length
    //  Setting     Packet length configuration
    //  0 (00)      Fixed packet length mode. Length configured in PKTLEN register
    //  1 (01)      Variable packet length mode. Packet length configured by the first byte after sync word
    //  2 (10)      Infinite packet length mode
    //  3 (11)      Reserved
    unsigned CRC_EN                 : 1;    //1: CRC calculation in TX and CRC check in RX enabled
    //0: CRC disabled for TX and RX
    unsigned Reserved               : 1;    //not used
    unsigned PKT_FORMAT             : 2;    // Format of RX and TX data
    //    Setting       Packet format
    //    0 (00)        Normal mode, use FIFOs for RX and TX
    //    1 (01)         Synchronous serial mode, Data in on GDO0 and  data out on either of the GDOx pins
    //    2 (10)        Random TX mode; sends random data using PN9    generator. Used for test. Works as normal mode, setting 0 (00), in RX
    //    3 (11)        Asynchronous serial mode, Data in on GDO0 and  data out on either of the GDOx pins
    unsigned WHITE_DATA                : 1;    //Turn data whitening on / off
    //0: Whitening off
    //1: Whitening on
    unsigned Reserved1                 : 1;    //not used
} __attribute__((__packed__)) PKTCTRL0_t;
typedef struct { //0x09: ADDR – Device Address

    unsigned DEVICE_ADDR              : 8;    //Address used for packet filtration. Optional broadcast addresses are 0 (0x00) and 255 (0xFF).


} __attribute__((__packed__)) ADDR_t;
typedef struct { //0x0A: CHANNR – Channel Number

    unsigned CHAN                     : 8;    //The 8-bit unsigned channel number, which is multiplied by the channel spacing setting and added to the base frequency.

} __attribute__((__packed__)) CHANNR_t;
typedef struct { //0x0B: FSCTRL1 – Frequency Synthesizer Control

    unsigned FREQ_IF                  : 5;    // The desired IF frequency to employ in RX. Subtracted from FS base frequency
    //in RX and controls the digital complex mixer in the demodulator.
    //Fif = (Fxosc/2¹⁰)*FREQ_IF
    //The default value gives an IF frequency of 381kHz, assuming a 26.0 MHz crystal.
    unsigned Reserved                 : 1;    //Reserved
    unsigned notUsed                  : 2;    //not used

} __attribute__((__packed__)) FSCTRL1_t;
typedef struct { //0x0C: FSCTRL0 – Frequency Synthesizer Control

    unsigned FREQOFF                  : 8;    //Frequency offset added to the base frequency before being used by the
    //frequency synthesizer. (2s-complement).
    //Resolution is FXTAL/2¹⁴ (1.59kHz-1.65kHz); range is ±202 kHz to ±210 kHz,
    //dependent of XTAL frequency.


} __attribute__((__packed__)) FSCTRL0_t;
typedef struct { //0x0D: FREQ2 – Frequency Control Word, High Byte

    unsigned FREQ                     : 8;    //FREQ[23:22] is always 0 (the FREQ2 register is less than 36 with 26-27 MHz crystal)
    //FREQ[23:0] is the base frequency for the frequency synthesiser in
    //increments of fXOSC/2¹⁶.

} __attribute__((__packed__)) FREQ2_t;
typedef struct { //0x0E: FREQ1 – Frequency Control Word, Middle Byte

    unsigned FREQ             : 8;        //FREQ[23:22] is always 0 (the FREQ2 register is less than 36 with 26-27 MHz crystal)
    //FREQ[23:0] is the base frequency for the frequency synthesiser in
    //increments of fXOSC/2¹⁶.

} __attribute__((__packed__)) FREQ1_t;
typedef struct { //0x0F: FREQ0 – Frequency Control Word, Low Byte

    unsigned FREQ                     : 8;    //FREQ[23:22] is always 0 (the FREQ2 register is less than 36 with 26-27 MHz crystal)
    //FREQ[23:0] is the base frequency for the frequency synthesiser in
    //increments of fXOSC/2¹⁶.

} __attribute__((__packed__)) FREQ0_t;
typedef struct { //Collection of FREQ2_t,FREQ1_t and FREQ0_t

    //FREQ[23:22] is always 0 (the FREQ2 register is less than 36 with 26-27 MHz crystal)
    //FREQ[23:0] is the base frequency for the frequency synthesiser in increments of fXOSC/2¹⁶.
    FREQ2_t FREQ2;
    FREQ1_t FREQ1;
    FREQ0_t FREQ0;

} __attribute__((__packed__)) FREQ_t;
typedef struct { //0x10: MDMCFG4 – Modem Configuration (Datasheet Page 76)

    unsigned DRATE_E                : 4;    //The exponent of the user specified symbol rate
    unsigned CHANBW_M               : 2;    //Sets the decimation ratio for the delta-sigma ADC input stream and thus the channel bandwidth.
    unsigned CHANBW_E               : 2;    //no description avalibable (Datasheet Page 76)

} __attribute__((__packed__)) MDMCFG4_t;
typedef struct { //0x11: MDMCFG3 – Modem Configuration

    unsigned DRATE_M                : 8;    //The mantissa of the user specified symbol rate. The symbol rate is configured
    //using an unsigned, floating-point number with 9-bit mantissa and 4-bit exponent.
    //The 9 th bit is a hidden „1‟. 2The default values give a data rate of 115.051 kBaud (closest setting to 115.2 kBaud), assuming a 26.0 MHz crystal.

} __attribute__((__packed__)) MDMCFG3_t;
typedef struct { //0x12: MDMCFG2 – Modem Configuration

    unsigned SYNC_MODE              : 2;    //Combined sync-word qualifier mode.
    //    The values 0 (000) and 4 (100) disables preamble and sync word
    //    transmission in TX and preamble and sync word detection in RX.
    //    The values 1 (001), 2 (010), 5 (101) and 6 (110) enables 16-bit sync word
    //    transmission in TX and 16-bits sync word detection in RX. Only 15 of 16 bits
    //    need to match in RX when using setting 1 (001) or 5 (101). The values 3 (011)
    //    and 7 (111) enables repeated sync word transmission in TX and 32-bits sync
    //    word detection in RX (only 30 of 32 bits need to match).
    //    Setting       Sync-word qualifier mode
    //    0 (000)       No preamble/sync
    //    1 (001)       15/16 sync word bits detected
    //    2 (010)       16/16 sync word bits detected
    //    3 (011)       30/32 sync word bits detected
    //    4 (100)       No preamble/sync, carrier-sense above threshold
    //    5 (101)       15/16 + carrier-sense above threshold
    //    6 (110)       16/16 + carrier-sense above threshold
    //    7 (111)       30/32 + carrier-sense above threshold
    unsigned MANCHESTER_EN          : 1;    //Enables Manchester encoding/decoding.
    //0 = Disable
    //1 = Enable
    unsigned MOD_FORMAT             : 3;    //The modulation format of the radio signal
    //Setting   Modulation format
    //0 (000)   2-FSK
    //1 (001)   GFSK
    //2 (010)   -
    //3 (011)
    // ASK/OOK
    //4 (100)
    // 4-FSK
    //5 (101)
    // -
    //6 (110)
    // -
    //7 (111)
    // MSK
    //MSK is only supported for data rates above 26 kBaud
    unsigned DEM_DCFILT_OFF         : 1;    //Disable digital DC blocking filter before demodulator.
    //    0 = Enable (better sensitivity)
    //    1 = Disable (current optimized). Only for data rates
    //    ≤ 250 kBaud
    //    The recommended IF frequency changes when the DC blocking is disabled.
    //    Please use SmartRF Studio [5] to calculate correct register setting.




} __attribute__((__packed__)) MDMCFG2_t;
typedef struct { //0x13: MDMCFG1– Modem Configuration

    unsigned CHANSPC_E              : 2;    // 2 bit exponent of channel spacing
    unsigned Reserved               :2;     //not used
    unsigned NUM_PREAMBLE           : 3;    // Sets the minimum number of preamble bytes to be transmitted
    //Setting   Number of preamble bytes
    //0 (000)   2
    //1 (001)   3
    //2 (010)   4
    //3 (011)   6
    //4 (100)   8
    //5 (101)   12
    //6 (110)   16
    //7 (111)   24
    unsigned FEC_EN                     : 1;    // Enable Forward Error Correction (FEC) with interleaving for packet payload
    //0 = Disable
    //1 = Enable (Only supported for fixed packet length mode, i.e.
    //PKTCTRL0.LENGTH_CONFIG=0)

} __attribute__((__packed__)) MDMCFG1_t;
typedef struct { //0x14: MDMCFG0– Modem Configuration

    unsigned CHANSPC_M              : 8;    //8-bit mantissa of channel spacing. The channel spacing is
    //    multiplied by the channel number CHAN and added to the base
    //    frequency. It is unsigned and has the format:
    //The default values give 199.951 kHz channel spacing (the closest
    //setting to 200 kHz), assuming 26.0 MHz crystal frequency.
    //Datasheet Page 78


} __attribute__((__packed__)) MDMCFG0_t;
typedef struct { //0x15: DEVIATN – Modem Deviation Setting


    unsigned DEVIATION_M            : 3;    //TX
    //    2-FSK/            Specifies the nominal frequency deviation from the carrier for a
    //    GFSK/             „0‟ (-DEVIATN) and „1‟ (+DEVIATN) in a mantissa-exponent
    //    4-FSK             format, interpreted as a 4-bit value with MSB implicit 1.
    //                      The default values give ±47.607 kHz deviation assuming 26.0 MHz crystal frequency.
    //    ####################################
    //    MSK               Specifies the fraction of symbol period (1/8-8/8) during which a
    //                      phase change occurs („0‟: +90deg, „1‟:-90deg). Refer to the
    //                      SmartRF Studio software [5] for correct DEVIATN setting when using MSK.
    //    ####################################
    //    ASK/OOK           This setting has no effect.
    //RX
    //    2-FSK/            Specifies the expected frequency deviation of incoming signal,
    //    GFSK/             must be approximately right for demodulation to be performed
    //    4-FSK             reliably and robustly.
    //
    //    #####################################
    //    MSK/              This setting has no effect.
    //    ASK/OOK

    unsigned Reserved               : 1;    //not used
    unsigned DEVIATION_E            : 3;    //Deviation exponent.
    unsigned Reserved1              : 1;    //not used


} __attribute__((__packed__)) DEVIATN_t;
typedef struct { //0x16: MCSM2 – Main Radio Control State Machine Configuration

    unsigned RX_TIME                : 3;    // Timeout for sync word search in RX for both WOR mode and normal RX
    //operation. The timeout is relative to the programmed EVENT0 timeout.
    unsigned RX_TIME_QUAL           : 1;    //When the RX_TIME timer expires, the chip checks if sync word is found when
    //RX_TIME_QUAL=0, or either sync word is found or PQI is set when RX_TIME_QUAL=1.

    unsigned RX_TIME_RSSI           : 1;    //Direct RX termination based on RSSI measurement (carrier sense). For
    //ASK/OOK modulation, RX times out if there is no carrier sense in the first 8 symbol periods.
    unsigned Reserved               : 3;    //not used;

} __attribute__((__packed__)) MCSM2_t;
typedef struct { //0x17: MCSM1– Main Radio Control State Machine Configuration

    unsigned TXOFF_MODE             : 2;    //Select what should happen when a packet has been sent (TX)
    //    Setting   Next state after finishing packet transmission
    //    0 (00)    IDLE
    //    1 (01)    FSTXON
    //    2 (10)    Stay in TX (start sending preamble)
    //    3 (11)    RX

    unsigned RXOFF_MODE             : 2;    //Select what should happen when a packet has been received
    //    Setting   Next state after finishing packet reception
    //    0 (00)    IDLE
    //    1 (01)    FSTXON
    //    2 (10)    TX
    //    3 (11)    Stay in RX
    //    It is not possible to set RXOFF_MODE to be TX or FSTXON and at the same time use CCA.

    //  unsigned CCA_MODE               : 2;    // Selects CCA_MODE Reflected in CCA signal
    unsigned CCA_MODE_SIGNAL   : 2;
    //    Setting   Clear channel indication
    //    0 (00)    Always
    //    1 (01)    If RSSI below threshold
    //    2 (10)    Unless currently receiving a packet
    //    3 (11)    If RSSI below threshold unless currently receiving a packet
    unsigned Reserved               : 2;    //not used

} __attribute__((__packed__)) MCSM1_t;
typedef struct { //0x18: MCSM0– Main Radio Control State Machine Configuration

    unsigned XOSC_FORCE_ON          : 1;    //Force the XOSC to stay on in the SLEEP state.
    unsigned PIN_CTRL_EN            : 1;    //Enables the pin radio control option
    unsigned PO_TIMEOUT             : 2;    //    Programs the number of times the six-bit ripple counter must expire after
    //    XOSC has stabilized before CHP_RDYn goes low .
    //
    //    If XOSC is on (stable) during power-down, PO_TIMEOUT should be set so that
    //    the regulated digital supply voltage has time to stabilize before CHP_RDYn
    //    goes low (PO_TIMEOUT=2 recommended). Typical start-up time for the
    //    voltage regulator is 50 μs.
    //    For robust operation it is recommended to use PO_TIMEOUT = 2 or 3 when
    //    XOSC is off during power-down.
    //
    //    Note that the XOSC_STABLE signal will be asserted at the same time as
    //    the CHP_RDYn signal; i.e. the PO_TIMEOUT delays both signals and does not
    //    insert a delay between the signals
    //    Setting   Expire count    Timeout after XOSC start
    //    0 (00)    1               Approx. 2.3 – 2.4 μs
    //    1 (01)    16              Approx. 37 – 39 μs
    //    2 (10)    64              Approx. 149 – 155 μs
    //    3 (11)    256             Approx. 597 – 620 μs
    //    Exact timeout depends on crystal frequency.

    unsigned FS_AUTOCAL             : 2;    //Automatically calibrate when going to RX or TX, or back to IDLE
    //    Setting   When to perform automatic calibration
    //    0 (00)    Never (manually calibrate using SCAL strobe)
    //    1 (01)    When going from IDLE to RX or TX (or FSTXON)
    //
    //    2 (10)    When going from RX or TX back to IDLE automatically
    //    3 (11)    Every 4th time when going from RX or TX to IDLE automatically
    //
    //    In some automatic wake-on-radio (WOR) applications, using setting 3 (11)
    //    can significantly reduce current consumption.
    unsigned Reserved               : 1;    //not used

} __attribute__((__packed__)) MCSM0_t;
typedef struct { //0x19: FOCCFG – Frequency Offset Compensation Configuration

    unsigned FOC_LIMIT              : 2;    // The saturation point for the frequency offset compensation algorithm:
    //    Setting   Saturation point (max compensated offset)
    //    0 (00)    ±0 (no frequency offset compensation)
    //    1 (01)    ±BWCHAN/8
    //    2 (10)    ±BWCHAN/4
    //    3 (11)    ±BWCHAN/2
    //    Frequency offset compensation is not supported for ASK/OOK. Always use
    //    FOC_LIMIT=0 with these modulation formats.
    unsigned FOC_POST_K             : 1;    // The frequency compensation loop gain to be used after a sync word is detected.
    //    Setting   Freq. compensation loop gain after sync word
    //    0         Same as FOC_PRE_K
    //    1         K/2
    unsigned FOC_PRE_K              : 2;    //The frequency compensation loop gain to be used before a sync word is detected.
    //    Setting   Freq. compensation loop gain before sync word
    //    0 (00)    K
    //    1 (01)    2K
    //    2 (10)    3K
    //    3 (11)    4K

    unsigned FOC_BS_CS_GATE         : 1;    // If set, the demodulator freezes the frequency offset compensation and clock
    //recovery feedback loops until the CS signal goes high.
    unsigned Reserved               : 2;    //not used


} __attribute__((__packed__)) FOCCFG_t;
typedef struct { //0x1A: BSCFG – Bit Synchronization Configuration

    unsigned BS_LIMIT               : 2;    // The saturation point for the data rate offset compensation algorithm:
    //    Setting   Data rate offset saturation (max data rate difference)
    //    0 (00)    ±0 (No data rate offset compensation performed)
    //    1 (01)    ±3.125 % data rate offset
    //    2 (10)    ±6.25 % data rate offset
    //    3 (11)    ±12.5 % data rate offset
    unsigned BS_POST_KP             : 1;    //The clock recovery feedback loop proportional gain to be used after a sync word is detected.
    //    Setting   Clock recovery loop proportional gain after sync word
    //    0         Same as BS_PRE_KP
    //    1         KP
    unsigned BS_POST_KI             : 1;    //The clock recovery feedback loop integral gain to be used after a sync word is detected.
    //    Setting   Clock recovery loop integral gain after sync word
    //    0         Same as BS_PRE_KI
    //    1         KI /2

    unsigned BS_PRE_KP              : 2;    // The clock recovery feedback loop proportional gain to be used before a sync word is detected.
    //    Setting   Clock recovery loop proportional gain before sync word
    //    0 (00)    KP
    //    1 (01)    2KP
    //    2 (10)    3KP
    //    3 (11)    4KP
    unsigned BS_PRE_KI              : 2;    //The clock recovery feedback loop integral gain to be used before a sync word is detected (used to correct offsets in data rate):
    //    Setting   Clock recovery loop integral gain before sync word
    //    0 (00)    KI
    //    1 (01)    2KI
    //    2 (10)    3KI
    //    3 (11)    4KI



} __attribute__((__packed__)) BSCFG_t;
typedef struct { //0x1B: AGCCTRL2 – AGC Control

    unsigned MAGN_TARGET            : 3;    //These bits set the target value for the averaged amplitude from the digital channel filter (1 LSB = 0 dB).
    //    Setting   Target amplitude from channel filter
    //    0 (000)   24 dB
    //    1 (001)   27 dB
    //    2 (010)   30 dB
    //    3 (011)   33 dB
    //    4 (100)   36 dB
    //    5 (101)   38 dB
    //    6 (110)   40 dB
    //    7 (111)   42 dB
    unsigned MAX_LNA_GAIN           : 3;    //Sets the maximum allowable LNA + LNA 2 gain relative to the maximum possible gain.
    //    Setting   Maximum allowable LNA + LNA 2 gain
    //    0 (000)   Maximum possible LNA + LNA 2 gain
    //    1 (001)   Approx. 2.6 dB below maximum possible gain
    //    2 (010)   Approx. 6.1 dB below maximum possible gain
    //    3 (011)   Approx. 7.4 dB below maximum possible gain
    //    4 (100)   Approx. 9.2 dB below maximum possible gain
    //    5 (101)   Approx. 11.5 dB below maximum possible gain
    //    6 (110)   Approx. 14.6 dB below maximum possible gain
    //    7 (111)   Approx. 17.1 dB below maximum possible gain

    unsigned MAX_DVGA_GAIN          : 2;    //Reduces the maximum allowable DVGA gain.
    //    Setting   Allowable DVGA settings
    //    0 (00)    All gain settings can be used
    //    1 (01)    The highest gain setting can not be used
    //    2 (10)    The 2 highest gain settings can not be used
    //    3 (11)    The 3 highest gain settings can not be used


} __attribute__((__packed__)) AGCCTRL2_t;
typedef struct { //0x1C: AGCCTRL1 – AGC Control

    unsigned CARRIER_SENSE_ABS_THR  : 4;    // Sets the absolute RSSI threshold for asserting carrier sense.
    //The 2-complement signed threshold is programmed in steps of 1 dB and is relative to the MAGN_TARGET setting.

    //    Setting       Carrier sense absolute threshold
    //                  (Equal to channel filter amplitude when AGC
    //                  has not decreased gain)
    //    -8 (1000)     Absolute carrier sense threshold disabled
    //    -7 (1001)     7 dB below MAGN_TARGET setting
    //    ...           ...
    //    -1 (1111)     1 dB below MAGN_TARGET setting
    //    0 (0000)      At MAGN_TARGET setting
    //    1 (0001)      1 dB above MAGN_TARGET setting
    //    ...           ...
    //    7 (0111)      7 dB above MAGN_TARGET setting
    unsigned CARRIER_SENSE_REL_THR  : 2;    //Sets the relative change threshold for asserting carrier sense
    //    Setting       Carrier sense relative threshold
    //    0 (00)        Relative carrier sense threshold disabled
    //    1 (01)        6 dB increase in RSSI value
    //    2 (10)        10 dB increase in RSSI value
    //    3 (11)        14 dB increase in RSSI value

    unsigned AGC_LNA_PRIORITY       : 1;    // Selects between two different strategies for LNA and LNA 2 gain
    //adjustment. When 1, the LNA gain is decreased first. When 0, the
    //LNA 2 gain is decreased to minimum before decreasing LNA gain.
    unsigned Reserved               : 1;    //not used



} __attribute__((__packed__)) AGCCTRL1_t;
typedef struct { //0x1D:AGCCTRL0 – AGC Control


    unsigned FILTER_LENGTH          : 2;    //2-FSK, 4-FSK, MSK: Sets the averaging length for the amplitude from the channel filter.
    //ASK, OOK: Sets the OOK/ASK decision boundary for OOK/ASK reception.
    //    Setting   Channel filter samples      OOK/ASK decision boundary
    //    0 (00)    8                           4 dB
    //    1 (01)    16                          8 dB
    //    2 (10)    32                          12 dB
    //    3 (11)    64                          16 dB

    unsigned AGC_FREEZE             : 2;    //Control when the AGC gain should be frozen.
    //    Setting   Function
    //    0 (00)    Normal operation. Always adjust gain when required.
    //    1 (01)    The gain setting is frozen when a sync word has beenfound.
    //    2 (10)    Manually freeze the analogue gain setting and continue to adjust the digital gain.
    //    3 (11)    Manually freezes both the analogue and the digital gain setting. Used for manually overriding the gain.

    unsigned WAIT_TIME              : 2;    //Sets the number of channel filter samples from a gain adjustment has
    //been made until the AGC algorithm starts accumulating new samples.
    //    Setting   Channel filter samples
    //    0 (00)    8
    //    1 (01)    16
    //    2 (10)    24
    //    3 (11)    32
    unsigned HYST_LEVEL             : 2;    //Sets the level of hysteresis on the magnitude deviation (internal AGC
    //    signal that determine gain changes).
    //    Setting   Description
    //    0 (00)    No hysteresis, small symmetric dead zone, high gain
    //    1 (01)    Low hysteresis, small asymmetric dead zone, medium gain
    //    2 (10)    Medium hysteresis, medium asymmetric dead zone, medium gain
    //    3 (11)    Large hysteresis, large asymmetric dead zone, low gain

} __attribute__((__packed__)) AGCCTRL0_t;
typedef struct { //0x1E: WOREVT1 – High Byte Event0 Timeout

    unsigned EVENT0                 : 8;    //High byte of EVENT0 timeout register

} __attribute__((__packed__)) WOREVT1_t;
typedef struct { //0x1F: WOREVT0 –Low Byte Event0 Timeout

    unsigned EVENT0                 : 8;    //Low byte of EVENT0 timeout register.
    //The default EVENT0 value gives 1.0s timeout, assuming a 26.0 MHz crystal.


} __attribute__((__packed__)) WOREVT0_t;
typedef struct { //0x20:WORCTRL– Wake On Radio Control

    unsigned WOR_RES                : 2;    //Controls the Event 0 resolution as well as maximum timeout of the WOR
    //    module and maximum timeout under normal RX operation:
    //    Setting   Resolution (1 LSB)          Max timeout
    //    0 (00)    1 period (28 – 29 μs)       1.8 – 1.9 seconds
    //    1 (01)    2⁵ periods (0.89 – 0.92 ms) 58 – 61 seconds
    //    2 (10)    2¹⁰ periods (28 – 30 ms)    31 – 32 minutes
    //    3 (11)    2¹⁵ periods (0.91 – 0.94 s) 16.5 – 17.2 hours
    //    Note that WOR_RES should be 0 or 1 when using WOR because WOR_RES > 1 will give a very low duty cycle.
    //    In normal RX operation all settings of WOR_RES can be used.

    unsigned Reserved               : 1;    //not used
    unsigned RC_CAL                 : 1;    //Enables (1) or disables (0) the RC oscillator calibration.
    unsigned EVENT1                 : 3;    //Timeout setting from register block. Decoded to Event 1 timeout. RC oscillator
    //    clock frequency equals FXOSC/750, which is 34.7 – 36 kHz, depending on
    //    crystal frequency. The table below lists the number of clock periods after
    //    Event 0 before Event 1 times out.
    //    Setting   tEvent1
    //    0 (000)   4 (0.111 – 0.115 ms)
    //    1 (001)   6 (0.167 – 0.173 ms)
    //    2 (010)   8 (0.222 – 0.230 ms)
    //    3 (011)   12 (0.333 – 0.346 ms)
    //    4 (100)   16 (0.444 – 0.462 ms)
    //    5 (101)   24 (0.667 – 0.692 ms)
    //    6 (110)   32 (0.889 – 0.923 ms)
    //    7 (111)   48 (1.333 – 1.385 ms)

    unsigned RC_PD                  : 1;    //Power down signal to RC oscillator. When written to 0, automatic initial calibration will be performed



} __attribute__((__packed__)) WORCTRL_t;
typedef struct { //0x21: FREND1 – Front End RX Configuration

    unsigned MIX_CURRENT            : 2;    //Adjusts current in mixer
    unsigned LODIV_BUF_CURRENT_RX   : 2;    //Adjusts current in RX LO buffer (LO input to mixer)
    unsigned LNA2MIX_CURRENT        : 2;    //Adjusts front-end PTAT outputs
    unsigned LNA_CURRENT            : 2;    //Adjusts front-end LNA PTAT current output

} __attribute__((__packed__)) FREND1_t;
typedef struct { //0x22: FREND0 – Front End TX Configuration

    unsigned PA_POWER               : 3;    //Selects PA power setting. This value is an index to the
    //PATABLE, which can be programmed with up to 8 different
    //PA settings. In OOK/ASK mode, this selects the PATABLE
    //index to use when transmitting a „1‟. PATABLE index zero is
    //used in OOK/ASK when transmitting a „0‟. The PATABLE
    //settings from index „0‟ to the PA_POWER value are used for
    //ASK TX shaping, and for power ramp-up/ramp-down at the
    //start/end of transmission in all TX modulation formats.
    unsigned Reserved               : 1;    //not used
    unsigned LODIV_BUF_CURRENT_TX   : 2;    //Adjusts current TX LO buffer (input to PA). The value to use
    //in this field is given by the SmartRF Studio software [5].
    unsigned Reserved1              : 2;    //not used

} __attribute__((__packed__)) FREND0_t;
typedef struct { //0x23: FSCAL3 – Frequency Synthesizer Calibration

    unsigned FSCAL3_LOW             : 4;    //Frequency synthesizer calibration result register. Digital bit
    //    vector defining the charge pump output current, on an
    //    FSCAL3[3:0]/4
    //    exponential scale: I_OUT = I0·2
    //    Fast frequency hopping without calibration for each hop can
    //    be done by calibrating upfront for each frequency and saving
    //    the resulting FSCAL3, FSCAL2 and FSCAL1 register values.
    //    Between each frequency hop, calibration can be replaced by
    //    writing the FSCAL3, FSCAL2 and FSCAL1 register values
    //    corresponding to the next RF frequency.

    unsigned CHP_CURR_CAL_EN        : 2;    //Disable charge pump calibration stage when 0.

    unsigned FSCAL3_HIGH            : 2;    //Frequency synthesizer calibration configuration. The value to
    //write in this field before calibration is given by the SmartRF Studio software

} __attribute__((__packed__)) FSCAL3_t;
typedef struct { //0x24: FSCAL2 – Frequency Synthesizer Calibration

    unsigned FSCAL2                 : 5;    //Frequency synthesizer calibration result register. VCO current calibration
    //    result and override value.
    //    Fast frequency hopping without calibration for each hop can be done by
    //    calibrating upfront for each frequency and saving the resulting FSCAL3,
    //    FSCAL2 and FSCAL1 register values. Between each frequency hop,
    //    calibration can be replaced by writing the FSCAL3, FSCAL2 and FSCAL1
    //    register values corresponding to the next RF frequency.

    unsigned VCO_CORE_H_EN          : 1;    //Choose high (1) / low (0) VCO

    unsigned Reserved               : 2;    //not used

} __attribute__((__packed__)) FSCAL2_t;
typedef struct { //0x25: FSCAL1 – Frequency Synthesizer Calibration

    unsigned FSCAL1                 : 6;    //Frequency synthesizer calibration result register. Capacitor array setting for
    //    VCO coarse tuning.
    //    Fast frequency hopping without calibration for each hop can be done by
    //    calibrating upfront for each frequency and saving the resulting FSCAL3,
    //    FSCAL2 and FSCAL1 register values. Between each frequency hop,
    //    calibration can be replaced by writing the FSCAL3, FSCAL2 and FSCAL1
    //    register values corresponding to the next RF frequency.

    unsigned Reserved               : 2;    //not used

} __attribute__((__packed__)) FSCAL1_t;
typedef struct { //0x26: FSCAL0 – Frequency Synthesizer Calibration

    unsigned FSCAL0                 : 7;    // Frequency synthesizer calibration control. The value to use in this register is
    //given by the SmartRF Studio software [5].

    unsigned Reserved               : 1;    //not used

} __attribute__((__packed__)) FSCAL0_t;
typedef struct { //0x27: RCCTRL1 – RC Oscillator Configuration

    unsigned RCCTRL1                : 7;    //RC oscillator configuration.

    unsigned Reserved               : 1;    //not used

} __attribute__((__packed__)) RCCTRL1_t;
typedef struct { //0x28: RCCTRL0 – RC Oscillator Configuration

    unsigned RCCTRL0                : 7;    //RC oscillator configuration.

    unsigned Reserved               : 1;    //not used

} __attribute__((__packed__)) RCCTRL0_t;

//#################################################################
// Configuration Register Details – Registers that Loose Programming in SLEEP State
// Datasheet Page 91
//#################################################################

typedef struct { //0x29: FSTEST – Frequency Synthesizer Calibration Control

    unsigned PTEST                  : 8;    // For test only. Do not write to this register.

} __attribute__((__packed__)) PTEST_t;
typedef struct { //0x2A: PTEST – Production Test

    unsigned FSTEST                : 8;     //Writing 0xBF to this register makes the on-chip temperature sensor
    //available in the IDLE state. The default 0x7F value should then be written
    //back before leaving the IDLE state. Other use of this register is for test only.


} __attribute__((__packed__)) FSTEST_t;
typedef struct { //0x2B: AGCTEST – AGC Test

    unsigned AGCTEST                : 8;     //For test only. Do not write to this register.

} __attribute__((__packed__)) AGCTEST_t;
typedef struct { //0x2C: TEST2 – Various Test Settings

    unsigned TEST2                  : 8;    //The value to use in this register is given by the SmartRF Studio software
    //    [5]. This register will be forced to 0x88 or 0x81 when it wakes up from
    //    SLEEP mode, depending on the configuration of FIFOTHR.
    //    ADC_RETENTION.
    //    Note that the value read from this register when waking up from SLEEP
    //    always is the reset value (0x88) regardless of the ADC_RETENTION
    //    setting. The inverting of some of the bits due to the ADC_RETENTION
    //    setting is only seen INTERNALLY in the analog part.

} __attribute__((__packed__)) TEST2_t;
typedef struct { //0x2D: TEST1 – Various Test Settings

    unsigned TEST1                  : 8;    //The value to use in this register is given by the SmartRF Studio software
    //    [5]. This register will be forced to 0x31 or 0x35 when it wakes up from
    //    SLEEP mode, depending on the configuration of FIFOTHR.
    //    ADC_RETENTION.
    //    Note that the value read from this register when waking up from SLEEP
    //    always is the reset value (0x31) regardless of the ADC_RETENTION
    //    setting. The inverting of some of the bits due to the ADC_RETENTION
    //    setting is only seen INTERNALLY in the analog part.

} __attribute__((__packed__)) TEST1_t;
typedef struct { //0x2E: TEST0 – Various Test Settings

    unsigned TEST0_LOW              : 1;    // The value to use in this register is given by the SmartRF Studio software

    unsigned VCO_SEL_CAL_EN         : 1;    //Enable VCO selection calibration stage when 1

    unsigned TEST0_HIGH             : 6;    //The value to use in this register is given by the SmartRF Studio software

} __attribute__((__packed__)) TEST0_t;

//#################################################################
// Status Register Details
// Datasheet Page 92
//#################################################################

typedef struct { //0x30 (0xF0): PARTNUM – Chip ID

    unsigned PARTNUM                : 8;     //Chip part number

} __attribute__((__packed__)) PARTNUM_t;
typedef struct { //0x31 (0xF1): VERSION – Chip ID

    unsigned VERSION                : 8;     //Chip version number.

} __attribute__((__packed__)) VERSION_t;
typedef struct { //0x32 (0xF2): FREQEST – Frequency Offset Estimate from Demodulator

    unsigned FREQOFF_EST            : 8;    //The estimated frequency offset (2‟s complement) of the carrier. Resolution is
    //    FXTAL/214 (1.59 - 1.65 kHz); range is ±202 kHz to ±210 kHz, depending on XTAL
    //    frequency.
    //    Frequency offset compensation is only supported for 2-FSK, GFSK, 4-FSK, and
    //    MSK modulation. This register will read 0 when using ASK or OOK modulation.

} __attribute__((__packed__)) FREQ_EST_t;
typedef struct { //0x33 (0xF3): LQI – Demodulator Estimate for Link Quality

    unsigned LQI_EST                : 7;    //The Link Quality Indicator estimates how easily a received signal can be
    //demodulated. Calculated over the 64 symbols following the sync word
    unsigned CRC_OK                 : 1;    // The last CRC comparison matched. Cleared when entering/restarting RX mode.

} __attribute__((__packed__)) LQI_t;
typedef struct { //0x34 (0xF4): RSSI – Received Signal Strength Indication

    unsigned RSSI                   : 8;    //Received signal strength indicator

} __attribute__((__packed__)) RSSI_t;
typedef struct { //0x35 (0xF5): MARCSTATE – Main Radio Control State Machine State

    unsigned MARC_STATE             : 5;    // Main Radio Control FSM State
    //    Value         State name          State (Figure 25, page 50)
    //    0 (0x00)      SLEEP               SLEEP
    //    1 (0x01)      IDLE                IDLE
    //    2 (0x02)      XOFF                XOFF
    //    3 (0x03)      VCOON_MC            MANCAL
    //    4 (0x04)      REGON_MC            MANCAL
    //    5 (0x05)      MANCAL              MANCAL
    //    6 (0x06)      VCOON               FS_WAKEUP
    //    7 (0x07)      REGON               FS_WAKEUP
    //    8 (0x08)      STARTCAL            CALIBRATE
    //    9 (0x09)      BWBOOST             SETTLING
    //    10 (0x0A)     FS_LOCK             SETTLING
    //    11 (0x0B)     IFADCON             SETTLING
    //    12 (0x0C)     ENDCAL              CALIBRATE
    //    13 (0x0D)     RX                  RX
    //    14 (0x0E)     RX_END              RX
    //    15 (0x0F)     RX_RST              RX
    //    16 (0x10)     TXRX_SWITCH         TXRX_SETTLING
    //    17 (0x11)     RXFIFO_OVERFLOW     RXFIFO_OVERFLOW
    //    18 (0x12)     FSTXON              FSTXON
    //    19 (0x13)     TX                  TX
    //    20 (0x14)     TX_END              TX
    //    21 (0x15)     RXTX_SWITCH         RXTX_SETTLING
    //    22 (0x16)     TXFIFO_UNDERFLOW    TXFIFO_UNDERFLOW
    //    Note: it is not possible to read back the SLEEP or XOFF state numbers
    //    because setting CSn low will make the chip enter the IDLE mode from the
    //    SLEEP or XOFF states.

    unsigned Reserved               : 3;    //not used

} __attribute__((__packed__)) MARCSTATE_t;
typedef struct { //0x36 (0xF6): WORTIME1 – High Byte of WOR Time

    unsigned TIME                   : 8;    // High byte of timer value in WOR module

} __attribute__((__packed__)) WORTIME1_t;
typedef struct { //0x37 (0xF7): WORTIME0 – Low Byte of WOR Time

    unsigned TIME                   : 8;    //Low byte of timer value in WOR module

} __attribute__((__packed__)) WORTIME0_t;
typedef struct { //0x38 (0xF8): PKTSTATUS – Current GDOx Status and Packet Status

    unsigned GDO0                   : 1;    //Current GDO0 value. Note: the reading gives the non-inverted value
    //    irrespective of what IOCFG0.GDO0_INV is programmed to.
    //    It is not recommended to check for PLL lock by reading PKTSTATUS[0]
    //    with GDO0_CFG=0x0A.

    unsigned Reserved               : 1;    //not used

    unsigned GDO2                   : 1;    //Current GDO2 value. Note: the reading gives the non-inverted value
    //    irrespective of what IOCFG2.GDO2_INV is programmed to.
    //    It is not recommended to check for PLL lock by reading PKTSTATUS[2]
    //    with GDO2_CFG=0x0A.

    unsigned SFD                    : 1;    //Start of Frame Delimiter. In RX, this bit is asserted when sync word has
    //    been received and de-asserted at the end of the packet. It will also de-
    //    assert when a packet is discarded due to address or maximum length
    //    filtering or the radio enters RXFIFO_OVERFLOW state. In TX this bit will
    //    always read as 0.

    unsigned CCA                    : 1;    //Channel is clear

    unsigned PQT_REACHED            : 1;    //Preamble Quality reached. If leaving RX state when this bit is set it will
    //    remain asserted until the chip re-enters RX state (MARCSTATE=0x0D). The
    //    bit will also be cleared if PQI goes below the programmed PQT value.

    unsigned CS                     : 1;    //Carrier sense. Cleared when entering IDLE mode.

    unsigned CRC_OK                 : 1;    //The last CRC comparison matched. Cleared when entering/restarting RX mode.

} __attribute__((__packed__)) PKTSTATUS_t;
typedef struct { //0x39 (0xF9): VCO_VC_DAC – Current Setting from PLL Calibration Module

    unsigned VCO_VC_DAC             : 8;    //Status register for test only.

} __attribute__((__packed__)) VCO_VC_DAC_t;
typedef struct { //0x3A (0xFA): TXBYTES – Underflow and Number of Bytes

    unsigned NUM_TXBYTES            : 7;    //Number of bytes in TX FIFO

    unsigned TXFIFO_UNDERFLOW       : 1;    //no comment

} __attribute__((__packed__)) TXBYTES_t;
typedef struct { //0x3B (0xFB): RXBYTES – Overflow and Number of Bytes

    unsigned NUM_RXBYTES            : 7;    //Number of bytes in RX FIFO

    unsigned RXFIFO_OVERFLOW        : 1;    //no comment

} __attribute__((__packed__)) RXBYTES_t;
typedef struct { //0x3C (0xFC): RCCTRL1_STATUS – Last RC Oscillator Calibration Result

    unsigned  RCCTRL1_STATUS        : 7;    // Contains the value from the last run of the RC oscillator calibration routine.
    //For usage description refer to Application Note AN047 [4]

    unsigned Reserved               : 1;    //not used

} __attribute__((__packed__)) RCCTRL1_STATUS_t;
typedef struct { //0x3D (0xFD): RCCTRL0_STATUS – Last RC Oscillator Calibration Result

    unsigned  RCCTRL0_STATUS        : 7;    //Contains the value from the last run of the RC oscillator calibration routine.
    //For usage description refer to Application Note AN047 [4].

    unsigned Reserved               : 1;    //not used

} __attribute__((__packed__)) RCCTRL0_STATUS_t;
typedef struct { //0x3D (0xFD): RCCTRL0_STATUS – Last RC Oscillator Calibration Result

    IOCFG2_t IOCFG2;
    GDO1_CFG_t GDO1_CFG;
    IOCFG0_t IOCFG0;
    FIFOTHR_t FIFOTHR;
    SYNC1_t SYNC1;
    SYNC0_t SYNC0;
    PKTLEN_t PKTLEN;
    PKTCTRL1_t PKTCTRL1;
    PKTCTRL0_t PKTCTRL0;
    ADDR_t ADDR;
    CHANNR_t CHANNR;
    FSCTRL1_t FSCTRL1;
    FSCTRL0_t FSCTRL0;
    FREQ2_t FREQ2;
    FREQ1_t FREQ1;
    FREQ0_t FREQ0;
    //FREQ_t
    MDMCFG4_t MDMCFG4;
    MDMCFG3_t MDMCFG3;
    MDMCFG2_t MDMCFG2;
    MDMCFG1_t MDMCFG1;
    MDMCFG0_t MDMCFG0;
    DEVIATN_t DEVIATN;
    MCSM2_t MCSM2;
    MCSM1_t MCSM1;
    MCSM0_t MCSM0;
    FOCCFG_t FOCCFG;
    BSCFG_t BSCFG;
    AGCCTRL2_t AGCCTRL2;
    AGCCTRL1_t AGCCTRL1;
    AGCCTRL0_t AGCCTRL0;
    WOREVT1_t WOREVT1;
    WOREVT0_t WOREVT0;
    WORCTRL_t WORCTRL;
    FREND1_t FREND1;
    FREND0_t FREND0;
    FSCAL3_t FSCAL3;
    FSCAL2_t FSCAL2;
    FSCAL1_t FSCAL1;
    FSCAL0_t FSCAL0;
    RCCTRL1_t RCCTRL1;
    RCCTRL0_t RCCTRL0;

    //#################################################################
    //Configuration Register Details – Registers that Loose Programming in SLEEP State
    //Datasheet Page 91
    //#################################################################

    PTEST_t PTEST;
    FSTEST_t FSTEST;
    AGCTEST_t AGCTEST;
    TEST2_t TEST2;
    TEST1_t TEST1;
    TEST0_t TEST0;

    //#################################################################
    // Status Register Details
    //Datasheet Page 92
    //#################################################################

    PARTNUM_t PARTNUM;
    VERSION_t VERSION;
    FREQ_EST_t FREQ_EST;
    LQI_t LQI;
    RSSI_t RSSI;
    MARCSTATE_t MARCSTATE;
    WORTIME1_t WORTIME1;
    WORTIME0_t WORTIME0;
    PKTSTATUS_t PKTSTATUS;
    VCO_VC_DAC_t VCO_VC_DAC;
    TXBYTES_t TXBYTES;
    RXBYTES_t RXBYTES;
    RCCTRL1_STATUS_t RCCTRL1_STATUS;
    RCCTRL0_STATUS_t RCCTRL0_STATUS;

} __attribute__((__packed__)) CC1101_t;

//} register content definitions

#endif //RADIO_CC1101_REGISTERS_H
