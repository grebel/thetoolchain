#ifndef RADIO_SERIAL
#define RADIO_SERIAL

/*{ radio_serial.h *******************************************************
 
A radio that is connected via USART.
You are able to provide/register your own RX_Interpreter function that will parse incomming Data.


}*/
//{ Includes *************************************************************

// Basic set of helper functions
#include "ttc_basic.h"
#include "ttc_gpio.h"
#include "ttc_task.h"
#include "ttc_random.h"
#include "ttc_watchdog.h"
#include "DEPRECATED_ttc_radio.h"
#include "ttc_usart.h"
#include "ttc_string.h"
#include "ttc_queue.h"
#include "ttc_memory.h"

//} Includes
//{ Defines/ TypeDefs ****************************************************

#ifndef TTC_ASSERT_RADIO_SERIAL    // any previous definition set (Makefile)?
#define TTC_ASSERT_RADIO_SERIAL 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_RADIO_SERIAL == 1)  // use Assert()s in USART code (somewhat slower but alot easier to debug)
  #define Assert_RS(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in USART code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_RS(Condition, ErrorCode)
#endif


//typedef enum {   // ttc_radio_errorcode_e
//    tcm_OK,                 // =0: no error
//    tcm_TimeOut,            // timeout occured in called function
//    tcm_DeviceNotFound,     // adressed radio device not available in current uC
//    tcm_NotImplemented,     // function has no implementation for current architecture
//    tcm_InvalidArgument,    // general argument error
//    tcm_USART_Error,        // connected USART caused error
//    tcm_SPI_Error,          // connected SPI   caused error
//    tcm_UnknownError
//} radio_serial_errorcode_e;


typedef struct radio_serial_generic_s {

    ttc_queue_bytes_t* Queue_Rx;
    u8_t                    Size_Queue_Rx;
    //ttc_heap_block_t * (*UCB_TCM300_Answer)(struct ixrg_tcm300_generic_s * , ttc_heap_block_t * );
    void (*UCB_rx_intperpreter)( u8_t Index, u8_t Data);
    u8_t USART_Logical_Index;
    ttc_usart_config_t * USART_Generic;

}radio_serial_generic_t;

//} Defines
//{ Structures/ Enums ****************************************************

//} Structures/ Enums
//{ Global Variables *****************************************************



//} Global Variables
//{ Function prototypes **************************************************
radio_serial_generic_t * radio_serial_get_configuration(u8_t LogicalIndex);
// basic hardware intialization and spawns the required tasks
ttc_radio_errorcode_e radio_serial_init(u8_t LogicalIndex);

// task: transmits and receives data periodically
void _task_radio_serial(void *TaskArgument);

//Serial Input function: Transmitts data to the TCM300 Module
ttc_radio_errorcode_e radio_serial_send_data(u8_t LogicalIndex ,u8_t * Buffer, u8_t Amount);
//user callback function that will provide the latest date packet received from the TCM300 module
//ttc_radio_errorcode_e iXRG_TCM300_UCB_register(u8_t LogicalIndex, ttc_heap_block_t* (*UCB)(struct ixrg_tcm300_generic_s * TCM300_Generic, ttc_heap_block_t * Block));

void _serial_radio_data_grabber_single_byte(u8_t LogicalIndex, void * USART_Generic, u8_t Byte);
void _radio_serial_data_grabber_single_byte1(void * USART_Generic, u8_t Byte);
void _radio_serial_data_grabber_single_byte2(void * USART_Generic, u8_t Byte);
void _radio_serial_data_grabber_single_byte3(void * USART_Generic, u8_t Byte);
void _radio_serial_data_grabber_single_byte4(void * USART_Generic, u8_t Byte);
void _radio_serial_data_grabber_single_byte5(void * USART_Generic, u8_t Byte);
ttc_heap_block_t* _radio_serial_get_empty_block();
void _radio_serial_release_block(ttc_heap_block_t* Block);

//} Function prototypes

#endif //RADIO_SERIAL
