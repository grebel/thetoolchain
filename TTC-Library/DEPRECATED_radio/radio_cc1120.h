#ifndef RADIO_cc1120_H
#define RADIO_cc1120_H

/*{ radio_cc1120.h ***************************************************

   This source is published under the GNU LESSER GENERAL PUBLIC LICENSE (LGPL).
   See file LEGAL.txt in this software package for details.

   Authors: <Sascha Poggemann>, <Gregor Rebel>

   This is an low-level driver implementation for a Texas Instruments cc1120 Tranceiver
   connected via SPI bus.

   See DEPRECATED_ttc_radio.h for a complete description of how to configure radios in TheToolChain.


   Required constants (n=1..5):

   TTC_RADIOn_SPI_INDEX            # index of spi bus interface to use (requires a configured TTC_SPIn as described in ttc_spi.h)
   TTC_RADIOn_PIN_SPI_NSS=PIN_Pxn  # pin used as spi slave-select
   TTC_RADIOn_PIN_GDO0=PIN_Pxn     # pin connected to GDO0-pin on cc1120
   TTC_RADIOn_PIN_GDO1=PIN_Pxn     # pin connected to GDO1-pin on cc1120
   TTC_RADIOn_PIN_GDO2=PIN_Pxn     # pin connected to GDO2-pin on cc1120

   optional:
   TTC_RADIOn_cc1120_SPI_CHIP_VERSION  sets value expected to be read from cc1120-register 0x31 (default = 0x04)


   Example makefile configuration:

   COMPILE_OPTS += -DTTC_RADIO1=trd_cc1120_spi             # select low-level driver to use
   COMPILE_OPTS += -DTTC_RADIO1_SPI_INDEX=1                # use first configured SPI bus (requires a configured TTC_SPI1 as described in ttc_spi.h)
   COMPILE_OPTS += -DTTC_RADIO1_PIN_SPI_NSS=PIN_PB4        # pin used as spi slave-select
   COMPILE_OPTS += -DTTC_RADIO1_PIN_GDO0=PIN_PC13          # pin connected to GDO0-pin on cc1120
   COMPILE_OPTS += -DTTC_RADIO1_PIN_GDO1=PIN_PC2           # pin connected to GDO1-pin on cc1120
   COMPILE_OPTS += -DTTC_RADIO1_PIN_GDO2=PIN_PC8           # pin connected to GDO2-pin on cc1120
   COMPILE_OPTS += -DTTC_RADIOn_cc1120_SPI_CHIP_VERSION=4  # sets value expected to be read from cc1120-register 0x31 (default = 0x04)

}*/

//{ Includes *************************************************************

#include "ttc_basic.h"
#include "ttc_memory.h"
#include "ttc_gpio.h"
#include "ttc_usart.h"
#include "radio_cc1120_types.h"
#include "radio_cc1120_registers.h"
#include "radio_cc1120_spi.h"
#include "radio_cc1120_smartrf.h"
#include "ttc_radio_types.h"

//} Includes
//{ Defines/ TypeDefs ****************************************************

//definitions required for radio_cc1120_manualCalibration
#define VCDAC_START_OFFSET 2
#define FS_VCO2_INDEX 0
#define FS_VCO4_INDEX 1
#define FS_CHP_INDEX 2

//} Defines
//{ Function prototypes **************************************************

/** fills given structure with default configuration for indexed radio
 * @param RadioIndex     logical index of radio device to use (1 = TTC_RADIO1, ...)
 * @param Radio_Generic  structure must be allocated by caller
 */
void radio_cc1120_get_defaults(u8_t RadioIndex, ttc_radio_generic_t* Radio_Generic);

/** fills given structure with default configuration for indexed radio
 * @param RadioIndex     logical index of radio device to use (1 = TTC_RADIO1, ...)
 * @param Radio_Generic  structure must be allocated by caller
 */
void radio_cc1120_get_features(u8_t RadioIndex, ttc_radio_generic_t* Radio_Generic);

/** checks if radio transceiver still runs in correct operating mode
 * Note: Modes trm_Sleep, trm_PowerDown cannot be checked
 *
 * @param Radio_Generic   pointer to configuration of already initialized radio
 * @return  == 0: radio runs in expected operating mode
 *          != 0: radio is in unexpected operating mode (use radio_cc1120_set_operating_mode() to fix)
 */
BOOL radio_cc1120_check_mode(ttc_radio_generic_t* Radio_Generic);

/** initializes single radio according to given configuration data
 * @param Radio_Generic  structure being filled by radio_cc1120_get_defaults() earlier
 */
ttc_radio_errorcode_e radio_cc1120_init(ttc_radio_generic_t* Radio_Generic);

/** perform a manual calibration of the pll in the CC1120 Radio according to the TI Errata Sheet
 * @param Radio_Generic  structure being filled by radio_cc1120_get_defaults() earlier
 */
ttc_radio_errorcode_e radio_cc1120_manualCalibration(ttc_radio_generic_t* Radio_Generic);
/** writes given configuration found in file radio_cc1120_smartrf.h via SPI interface into cc1120 registers
 * this function must be called to apply register values after radio power down or sleep mode.
 *
 * @param Radio_Generic  structure being filled by radio_cc1120_get_defaults() earlier
 * @return          = tse_Ok = 0: configuration applied successfully
 */
ttc_radio_errorcode_e radio_cc1120_init_SmartRF(ttc_radio_generic_t* Radio_Generic);
/** returns the datarate configured for the selected radio
 *
 * @param Radio_Generic  structure being filled by radio_cc1120_get_defaults() earlier
 * @return          == 0: not proper datarate configured
 *                  != 0: Datarate in bits/second
 */
u32_t radio_cc1120_getDatarate(ttc_radio_generic_t* Radio_Generic);
/** returns the amount of additional bytes added by the radio for each transmitted packet
 *
 * @param Radio_Generic  structure being filled by radio_cc1120_get_defaults() earlier
 * @return          == 0: not implemented
 *                  != 0: AMount of additional bytes added by the radio
 */
u8_t radio_cc1120_getPacketHeaderSize(ttc_radio_generic_t* Radio_Generic);
/** send data from given buffer to target radio of given address
 *
 * @param Radio_Generic  structure being filled by radio_cc1120_get_defaults() earlier
 * @param TargetAddress  protocol address of target node (255 = broadcast address)
 * @param Size           0..Radio_Generic->Max_Payload - amount of bytes to send from Data[]
 * @param Data           buffer storing payload data to send
 */
ttc_radio_errorcode_e radio_cc1120_packet_send(ttc_radio_generic_t* Radio_Generic, u8_t TargetAddress, u8_t Size, u8_t* Data);

/** read single received packet from rx-fifo of cc1120 radio
 *
 * @param Radio_Generic  structure being filled by radio_cc1120_get_defaults() earlier
 * @param Buffer         target buffer where to store payload data
 * @param MaxSize        >= Radio_Generic->Max_Payload - maximum allowed amount of bytes to store in Buffer[]
 * @param TargetAddress  will be loaded with target protocol address of received packet (255 = broadcast address)
 * @param RSSI           will be loaded with value of Receive Signal Strength Indicator
 * @param Data           buffer storing payload data to send
 * @return               amount of payload bytes written into Buffer[]
 */
u8_t radio_cc1120_packet_receive(ttc_radio_generic_t* Radio_Generic, u8_t* Buffer, u8_t MaxSize, u8_t* TargetAdress, u8_t* RSSI);

/** Returns index of SPI device to use for indexed radio
 *
 * @param  RadioIndex  >0: logical index of radio device (1=TTC_RADIO1, ...)
 * @return             >0: logical device index of SPI device to use (1=TTC_SPI1, ...)
 *                     =0: indexed radio not available
 */
u8_t radio_cc1120_get_spi_index(u8_t RadioIndex);

/** Set a new address for this radio device
 *
 * @param addr  new device address
 */
ttc_radio_errorcode_e radio_cc1120_set_device_address(ttc_radio_generic_t* Radio_Generic, u8_t NewDeviceAddress);

/** set output transmit power
 * @param Level  output power in dBm
 */
ttc_radio_errorcode_e radio_cc1120_set_power_tx(ttc_radio_generic_t* Radio_Generic, s8_t Level);

/** Set frequency channel to use for transmit/ receive
 *
 * @param chnl  Frequency channel
 */
ttc_radio_errorcode_e radio_cc1120_set_channel(ttc_radio_generic_t* Radio_Generic, Base_t NewChannelIndex);

ttc_radio_errorcode_e radio_cc1120_set_mode_txoff(ttc_radio_generic_t* Radio_Generic, cc1120_TXOFF_Mode_e cc1120_TXOFF_Mode);
ttc_radio_errorcode_e radio_cc1120_set_mode_rxoff(ttc_radio_generic_t* Radio_Generic, cc1120_RXOFF_Mode_e cc1120_RXOFF_Mode);

/** switches operating mode of transceiver to Radio_Generic->Mode
 *
 * Note: If you wanna change the mode, use ttc_radio_set_operating_mode(Radio_Generic->LogicalIndex, NewMode) instead!
 *
 * @param Radio_Generic  structure being filled by radio_cc1120_get_defaults() earlier
 * @return               == tre_OK: mode switched successfully
 */
ttc_radio_errorcode_e radio_cc1120_set_operating_mode(ttc_radio_generic_t* Radio_Generic);

/** delivers pointer to driver configuration for a cc1120 radio transceiver
 *
 * @param Radio_Generic  structure storing pointer to a radio driver configuration
 * @return               pointer of correct type (new struct will be allocated and Radio_Generic updated if pointer was NULL)
 */
radio_cc1120_driver_t* radio_cc1120_get_config(ttc_radio_generic_t* Radio_Generic);

/** activates the CC1120's Listen-Before-Talk(LBT) feature
 *
 * @param Radio_Generic  structure storing pointer to a radio driver configuration
 * @param enable         == TRUE: enables LBT Feature
 * @return               == tre_OK: LBT Feature enabled successfully
 *                       == tre_NotImplemented: if LBT Feature is not supported
 */
ttc_radio_errorcode_e radio_cc1120_LBT_enable(ttc_radio_generic_t* Radio_Generic, bool enable);

/** reads current operating mode (MARCSTATE) from cc1120 transceiver
 *
 * Note: This function is private and should not be called from outside!
 *
 * @param Radio_Generic  structure storing pointer to a radio driver configuration
 * @return               value read from register MARCSTATE (0x35)
 */
u8_t _radio_cc1120_get_state(ttc_radio_generic_t* Radio_Generic);

/** interrupt service routine that will signal that a packet has been received
 *
 * @param PhysicalIndex   index of configured gpio pin as returned from ttc_gpio_create_index8()
 * @param Argument        ttc_radio_generic_t*
 */
void _radio_cc1120_isr_receive(physical_index_t PhysicalIndex, void* Argument);

/** tries to react to given error condition
 *
 * Note: This function is private and should not be called from outside!
 *
 * @param Radio_Generic      structure storing pointer to a radio driver configuration
 * @param Error              error condition to handle
 * @param UnlockTransaction  == TRUE: UnlockTransaction() will be called (allows to return from inside locked bus transaction)
 */
ttc_radio_errorcode_e _radio_cc1120_handle_error(ttc_radio_generic_t* Radio_Generic, ttc_radio_errorcode_e Error, BOOL UnlockTransaction);

//} Function prototypes

#endif //cc1120_H