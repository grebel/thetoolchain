#ifndef TTC_SDCARD_H
#define TTC_SDCARD_H
/** { ttc_sdcard.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for sdcard devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_SDCARD(tc_sdcard_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_sdcard_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_sdcard_init(LogicalIndex);
 *  4) use:         ttc_sdcard_XXX(LogicalIndex);
 *
 *  Device Index
 *  Devices are addressed by a logical index with no correlation to the order of physical devices.
 *  Logical indices start at 1 to simply detect uninitialized values.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level sdcard and application.
 *
 *  Created from template ttc_device.h revision 39 at 20180130 09:42:26 UTC
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of ttc_sdcard (Do not delete this line!)
 *
 * This high-level driver provides architecture independent access to memory cards of these types:
 * - MMC
 * - SDCARD v1.x (<=2GB)
 * - SDCARD >=v2 (>2GB)
 * All of the above are further simply called sdcard.
 *
 * Multiple sdcards are supported, each one in a separated sdcard slot.
 * All sdcard slots can be attached to the same or multiple data busses of same type.
 * Two types of data busses are supported to communicate with sdcard:
 * - sdcard_spi uses serial perihperal (SPI) bus d
 * - sdcard_sdio uses SD I/O parallel bus (to be implemented!)
 *
 * Basic ttc_sdcard functionality:
 * - ttc_sdcard_medium_detect()   check if sdcard medium is inserted into slot
 * - ttc_sdcard_medium_mount()    wake up and initialize sdcard for use
 * - ttc_sdcard_medium_unmount()  shut down sdcard to be allowed for removal
 * - ttc_sdcard_block_read()      read a single data block from sdcard
 * - ttc_sdcard_block_write()     write a single data block to sdcard
 *
 * Check examples/example_ttc_sdcard.c to see a working implementation!
 *
 * Note: ttc_sdcard does not provide any filesystem.
 *       Check ttc_disc to combine ttc_sdcard with ttc_filesystem!
}*/

#ifndef EXTENSION_ttc_sdcard
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_sdcard.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_sdcard.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_sdcard_interface.h" // multi architecture support
#include "sdcard/sdcard_common.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level sdcard only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function Declarations ************************************************{
 *
 * The functions declared below provide the main interface for
 * sdcard devices on all supported architectures.
 * Check sdcard/sdcard_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Return amount of blocks on current card (if any)
 *
 * All blocks on a card have same size (typically 512 bytes).
 *
 * @param LogicalIndex (t_u8)  Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @return             (t_u32)  ==0: no/invalid card found; >0: amount of blocks available on current card
 */
t_u32 ttc_sdcard_blocks_count( t_u8 LogicalIndex );

/** Read data of single block from sdcard into given buffer
 *
 * Note: If BufferSize < ttc_sdcard_blocks_size() then remaining bytes in block will be ignored.
 * Note: Will call ttc_sdcard_medium_mount() if required.
 *
 * @param Config       (t_ttc_sdcard_config*)   Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                   Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @param BlockAddress (t_u32)                  Address of block on sdcard (0..ttc_sdcard_blocks_count()-1)
 * @param Buffer       (t_u8*)                  Memory buffer where to store data
 * @param BufferSize   (t_u16)                  remaining amount of bytes available in Buffer[] (must bee >=512)
 * @return             (e_ttc_sdcard_errorcode) ==0: block was read into Buffer[] successfully; >0: error code
 */
e_ttc_sdcard_errorcode ttc_sdcard_block_read( t_u8 LogicalIndex, t_u32 BlockAddress, t_u8* Buffer, t_u16 BufferSize );
e_ttc_sdcard_errorcode _driver_sdcard_block_read( t_ttc_sdcard_config* Config, t_u32 BlockAddress, t_u8* Buffer, t_u16 BufferSize );

/** Write data from given buffer into single block onto sdcard
 *
 * If BufferSize is less than current block size, then rest of block will be filled with zeroes.
 * Note: Call ttc_sdcard_medium_mount() before calling this function!
 * Note: Will return error-code if sdcard has been removed meanwhile or is not mounted.
 *
 * @param LogicalIndex (t_u8)                   Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @param BlockAddress (t_u32)                  Address of block on sdcard (0..ttc_sdcard_blocks_count()-1)
 * @param Buffer       (const t_u8*)            Memory buffer storing data to be written onto sdcard
 * @param BufferSize   (t_u16)                  Amount of valid bytes in Buffer (will send Buffer[0..BufferSize-1]
 * @return             (e_ttc_sdcard_errorcode) ==0: block was written successfully; >0: error code
 */
e_ttc_sdcard_errorcode ttc_sdcard_block_write( t_u8 LogicalIndex, t_u32 BlockAddress, const t_u8* Buffer, t_u16 BufferSize );

/** Send a special application command (ACMD) with single byte response to sdcard.
 *
 * An application command consists of a usual command value (CMD) following a CMD55.
 * Application commands offer additional functionality and extended return values.
 * Some application commands are card specific.
 *
 * @param LogicalIndex       (t_u8)                               Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @param ApplicationCommand (e_ttc_sdcard_command_application)   Valid ACMD constant
 * @param Argument           (t_u32)                              Value depends on actual command
 * @return                   (u_ttc_sdcard_response)              Single byte response from card
 */
u_ttc_sdcard_response ttc_sdcard_command_application_r1( t_u8 LogicalIndex, e_ttc_sdcard_command_application ApplicationCommand, t_u32 Argument );

/** Send an application specific commandand read given amount of return values
 *
 * Note: Card response is stored in Config->Card.LastResponse.
 *
 * @param LogicalIndex       (t_u8)                               Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @param ApplicationCommand (e_ttc_sdcard_command_application)   A valid sdcard command
 * @param Argument           (t_u32)                              Value depends on actual command
 * @param Buffer             (t_u8*)                              Storage where to store return values
 * @param Amount             (t_u16)                              Amount of bytes to read after sending command + argument
 * @return                   (u_ttc_sdcard_response)              Single byte response from card
 */
u_ttc_sdcard_response    ttc_sdcard_command_application_rn( t_u8 LogicalIndex, e_ttc_sdcard_command_application ApplicationCommand, t_u32 Argument, t_u8* Buffer, t_u16 Amount );

/** Send a command and argument to sdcard and return single value
 *
 * @param LogicalIndex (t_u8)                      Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @param Command      (e_ttc_sdcard_command)      A valid sdcard command
 * @param Argument     (t_u32)                     Value depends on actual command
 * @return             (u_ttc_sdcard_response)  !=0xff: status response from sdcard; ==0xff: no response from sdcard
 */
u_ttc_sdcard_response ttc_sdcard_command_r1( t_u8 LogicalIndex, e_ttc_sdcard_command Command, t_u32 Argument );

/** Send a command and argument to sdcard and read given amount of return values
 *
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @param Command      (e_ttc_sdcard_command)  A valid sdcard command
 * @param Argument     (t_u32)                 Value depends on actual command
 * @param Buffer       (t_u8*)                 Storage where to store return values
 * @param Amount       (t_u16)                 Amount of bytes to read after sending command + argument
 * @return             (t_u16)                 Amount of bytes being read
 */
t_u16 ttc_sdcard_command_rn( t_u8 LogicalIndex, e_ttc_sdcard_command Command, t_u32 Argument, t_u8* Buffer, t_u16 Amount );

/** allocates a new sdcard instance and returns pointer to its configuration
 *
 * @return (t_ttc_sdcard_config*)  pointer to new configuration. Set all Init fields before calling ttc_sdcard_init() on it.
 */
t_ttc_sdcard_config* ttc_sdcard_create();

/** Returns amount of data blocks on inserted sdcard.
 *
 * Note: Will call ttc_sdcard_medium_mount() if required.
 *
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @return             (t_u32)                 amount of available data blocks on current sdcard
 */
t_u32 ttc_sdcard_blocks_count( t_u8 LogicalIndex );

/** Returns size of data blocks on current sdcard
 *
 * All blocks on an sdcard have same size. SDCARDs with larger capacity may have larger block sizes.
 * Note: Will call ttc_sdcard_medium_mount() if required.
 *
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @return             (t_u16)   block size in bytes
 */
t_u16 ttc_sdcard_blocks_size( t_u8 LogicalIndex );

/** Prepares sdcard Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_sdcard_prepare();
void _driver_sdcard_prepare();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    logical index of SDCARD device. Each logical device <n> is defined via TTC_SDCARD<n>* constants in compile_options.h and extensions.active/makefile
 * @return                configuration of indexed device
 */
t_ttc_sdcard_config* ttc_sdcard_get_configuration( t_u8 LogicalIndex );

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_sdcard_get_max_index();

/** initialize indexed device for use
 *
 * @param LogicalIndex    logical index of SDCARD device. Each logical device <n> is defined via TTC_SDCARD<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: sdcard device has been initialized successfully; != 0: error-code
 */
e_ttc_sdcard_errorcode ttc_sdcard_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    logical index of SDCARD device. Each logical device <n> is defined via TTC_SDCARD<n>* constants in compile_options.h and extensions.active/makefile
 */
void ttc_sdcard_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_sdcard_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of SDCARD device. Each logical device <n> is defined via TTC_SDCARD<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: default values have been loaded successfully for indexed sdcard device; != 0: error-code
 */
e_ttc_sdcard_errorcode ttc_sdcard_load_defaults( t_u8 LogicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    logical index of SDCARD device. Each logical device <n> is defined via TTC_SDCARD<n>* constants in compile_options.h and extensions.active/makefile

 */
void ttc_sdcard_reset( t_u8 LogicalIndex );

/** Fast check to see if an sdcard has been inserted or removed in indexed slot
 *
 * This check should be implemented as fast as possible as it will probably
 * be called regular at a high frequency.
 *
 * Note: Low-level driver must only return codes E_ttc_storage_event_media_present or E_ttc_storage_event_no_card!
 *       All other event codes are handled by ttc_sdcard_medium_detect().
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @return             (e_ttc_storage_event)    ==0: no sdcard available; >0: see e_ttc_storage_event for details!
 */
e_ttc_storage_event ttc_sdcard_medium_detect( t_u8 LogicalIndex );
e_ttc_storage_event _driver_sdcard_medium_detect( t_ttc_sdcard_config* Config );

/** Powers up sdcard in slot and prepares it for usage
 *
 * Will do nothing, if no sdcard is detected.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @return             == 0: sdcard device has been mounted successfully and can be used; != 0: error-code
 */
e_ttc_sdcard_errorcode ttc_sdcard_medium_mount( t_u8 LogicalIndex );
e_ttc_sdcard_errorcode _driver_sdcard_medium_go_idle( t_ttc_sdcard_config* Config );

/** Powers down sdcard to be removed safely
 *
 * Will do nothing if sdcard is currently not mounted.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @return             == 0: sdcard device has been unmounted successfully; != 0: error-code (inform user not to remove card before unmounting it)
 */
e_ttc_sdcard_errorcode ttc_sdcard_medium_unmount( t_u8 LogicalIndex );
void _driver_sdcard_medium_unmount( t_ttc_sdcard_config* Config );

/** Reinitializes all initialized devices after changed systemcloc profile.
 *
 * Note: This function is called automatically from _ttc_sysclock_call_update_functions()
 */
void ttc_sdcard_sysclock_changed();

/** Disables Cyclic Redundancy Checksums in communication with SDCARD
 *
 * CRC is enabled in SDCARD by default during power up.
 * Disabling CRC will make communication faster but less secure.
 * Check Config->Flags.CardDisabledCRC for latest state.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @param DisableCRC   (BOOL)                  ==TRUE: disable checksum bytes; ==FALSE: enable checksum bytes
 * @return              == 0: command has been executed successfully; != 0: error-code
 */
e_ttc_sdcard_errorcode ttc_sdcard_disable_crc( t_u8 LogicalIndex, BOOL DisableCRC );

/** Requests current card status
 *
 * @param LogicalIndex (t_u8)                      Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @return             (t_ttc_sdcard_response_r2)  current status returned from card
 */
t_ttc_sdcard_response_r2 ttc_sdcard_status( t_u8 LogicalIndex );

/** Wait while card is busy.
 *
 * This will wait if one of the busy flags (Config->Flags.Busy*) is set.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @return             (BOOL)                  ==TRUE: card is ready now; ==FALSE: card did not finish within timeout (Config->Init.Retries)
 */
BOOL ttc_sdcard_wait_busy( t_u8 LogicalIndex );
BOOL _driver_sdcard_wait_busy( t_ttc_sdcard_config* Config );

/** Read one or more data blocks from storage device into given buffer
 *
 * @param LogicalIndex (t_u8)   Logical index of filesystem instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_FILESYSTEM<n> lines in extensions.active/makefile
 * @param Buffer       (t_u8*)  Storage in RAM big egnough for Config->Init.BlockSize * AmountBlocks
 * @param Address      (t_u32)  Address of first block to read (will read addresses Address..Address+AmountBlocks-1
 * @param AmountBlocks (t_u8)   Amount of consecutive data blocks to read
 * @return             (e_ttc_filesystem_errorcode)   ==0: desired amount of data blocks was read successfully; >0: error code
 */
e_ttc_sdcard_errorcode ttc_sdcard_blocks_read( t_ttc_sdcard_config* Config, t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks );

/** Write one or more data blocks from given buffer onto storage device
 *
 * @param LogicalIndex (t_u8)         Logical index of filesystem instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_FILESYSTEM<n> lines in extensions.active/makefile
 * @param Buffer       (const t_u8*)  Storage in RAM big egnough for Config->Init.BlockSize * AmountBlocks
 * @param Address      (t_u32)        Address of first block to read (will read addresses Address..Address+AmountBlocks-1
 * @param AmountBlocks (t_u8)         Amount of consecutive data blocks to read
 * @return             (e_ttc_filesystem_errorcode)   ==0: desired amount of data blocks was read successfully; >0: error code
 */
e_ttc_sdcard_errorcode ttc_sdcard_blocks_write( t_ttc_sdcard_config* Config, const t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks );

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_sdcard_ are passed to interfaces/ttc_sdcard_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl sdcard UPDATE
 */

/** Write data from given buffer into single block onto sdcard
 *
 * If BufferSize is less than current block size, then rest of block will be filled with zeroes.
 * Notes for implementing low-level driver:
 * - High-level driver ensures that sdcard is present and mounted
 * - Do not wait until card has finished writing. Set Config->Flags.CardBusyWriting=1 instead!
 * - Do not call sdcard_common_status() or issue CMD13 after writing! This is done by high-level driver.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param BlockAddress (t_u32)                 Address of block on sdcard (0..ttc_sdcard_blocks_count()-1)
 * @param Buffer       (t_u8*)                 Memory buffer storing data to be written onto sdcard
 * @param BufferSize   (t_u16)                 Amount of valid bytes in Buffer (will send Buffer[0..BufferSize-1]
 * @return             (e_ttc_sdcard_errorcode)   ==0: block was written successfully; >0: error code
 */
e_ttc_sdcard_errorcode _driver_sdcard_block_write( t_ttc_sdcard_config* Config, t_u32 BlockAddress, const t_u8* Buffer, t_u16 BufferSize );

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_sdcard_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_sdcard_features.
 *
 * @param Config        Configuration of sdcard device
 * @return              Fields of *Config have been adjusted if necessary
 */
void _driver_sdcard_configuration_check( t_ttc_sdcard_config* Config );

/** shutdown single SDCARD unit device
 * @param Config        Configuration of sdcard device
 * @return              == 0: SDCARD has been shutdown successfully; != 0: error-code
 */
e_ttc_sdcard_errorcode _driver_sdcard_deinit( t_ttc_sdcard_config* Config );

/** initializes single SDCARD unit for operation
 * @param Config        Configuration of sdcard device
 * @return              == 0: SDCARD has been initialized successfully; != 0: error-code
 */
e_ttc_sdcard_errorcode _driver_sdcard_init( t_ttc_sdcard_config* Config );

/** loads configuration of indexed SDCARD unit with default values
 * @param Config        Configuration of sdcard device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_sdcard_errorcode _driver_sdcard_load_defaults( t_ttc_sdcard_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of sdcard device
 */
void _driver_sdcard_reset( t_ttc_sdcard_config* Config );

/** Send one commands and its argument to sdcard
 *
 * Note: Card response is stored in Config->Card.LastResponse.
 *
 * @param Config           (t_ttc_sdcard_config*)            Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param CommandArgument  (t_ttc_sdcard_command_argument*)  Pointer to a set of command number and its argument (-> sdcard_common_prepare_command() )
 * @return                 Config->Card.LastResponse.byte    ==0xff: no response from card; !=0xff: response code from card
 */
void _driver_sdcard_command_r1( t_ttc_sdcard_config* Config, const t_ttc_sdcard_command_argument* CommandArgument );

/** Send one commands and its argument to sdcard + read given amount of return values
 *
 * The use of a struct pointer allows to reduce ram usage and data copy operations.
 *
 * @param Config           (t_ttc_sdcard_config*)            Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param CommandArgument  (t_ttc_sdcard_command_argument*)  Pointer to a set of command number and its argument (-> sdcard_common_prepare_command() )
 * @param Buffer           (t_u8*)                           Storage where to store return values
 * @param Amount           (t_u16)                           Amount of bytes to read after sending command + argument
 * @return                 (t_u16)                           Amount of bytes being read
 */
t_u16 _driver_sdcard_command_rn( t_ttc_sdcard_config* Config, const t_ttc_sdcard_command_argument* CommandArgument, t_u8* Buffer, t_u16 Amount );
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_SDCARD_H
