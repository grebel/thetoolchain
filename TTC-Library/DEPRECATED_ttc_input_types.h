#ifndef TTC_INPUT_TYPES_H
#define TTC_INPUT_TYPES_H

#include "ttc_basic.h"

typedef enum {
    tid_None,
    tid_ADS7843, // -> gfx/input_ads7843.h (touch driver on Mini-STM32 prototype board)
    tid_Invalid,
    tid_Analog
} ttc_input_driver_e;

// TTC_INPUTn_DRIVER has to be defined as constant by makefile.100_board_*
#ifdef TTC_INPUT5_DRIVER
  #ifndef TTC_INPUT4_DRIVER
    #error TTC_INPUT5_DRIVER is defined, but not TTC_INPUT4_DRIVER - all lower TTC_INPUTn_DRIVER must be defined!
  #endif
  #ifndef TTC_INPUT3_DRIVER
    #error TTC_INPUT5_DRIVER is defined, but not TTC_INPUT3_DRIVER - all lower TTC_INPUTn_DRIVER must be defined!
  #endif
  #ifndef TTC_INPUT2_DRIVER
    #error TTC_INPUT5_DRIVER is defined, but not TTC_INPUT2_DRIVER - all lower TTC_INPUTn_DRIVER must be defined!
  #endif
  #ifndef TTC_INPUT1_DRIVER
    #error TTC_INPUT5_DRIVER is defined, but not TTC_INPUT1_DRIVER - all lower TTC_INPUTn_DRIVER must be defined!
  #endif

  #define TTC_AMOUNT_INPUTS 5
#else
  #ifdef TTC_INPUT4_DRIVER
    #define TTC_AMOUNT_INPUTS 4

    #ifndef TTC_INPUT3_DRIVER
      #error TTC_INPUT5_DRIVER is defined, but not TTC_INPUT3_DRIVER - all lower TTC_INPUTn_DRIVER must be defined!
    #endif
    #ifndef TTC_INPUT2_DRIVER
      #error TTC_INPUT5_DRIVER is defined, but not TTC_INPUT2_DRIVER - all lower TTC_INPUTn_DRIVER must be defined!
    #endif
    #ifndef TTC_INPUT1_DRIVER
      #error TTC_INPUT5_DRIVER is defined, but not TTC_INPUT1_DRIVER - all lower TTC_INPUTn_DRIVER must be defined!
    #endif
  #else
    #ifdef TTC_INPUT3_DRIVER

      #ifndef TTC_INPUT2_DRIVER
        #error TTC_INPUT5_DRIVER is defined, but not TTC_INPUT2_DRIVER - all lower TTC_INPUTn_DRIVER must be defined!
      #endif
      #ifndef TTC_INPUT1_DRIVER
        #error TTC_INPUT5_DRIVER is defined, but not TTC_INPUT1_DRIVER - all lower TTC_INPUTn_DRIVER must be defined!
      #endif

      #define TTC_AMOUNT_INPUTS 3
    #else
      #ifdef TTC_INPUT2_DRIVER

        #ifndef TTC_INPUT1_DRIVER
          #error TTC_INPUT5_DRIVER is defined, but not TTC_INPUT1_DRIVER - all lower TTC_INPUTn_DRIVER must be defined!
        #endif

        #define TTC_AMOUNT_INPUTS 2
      #else
        #ifdef TTC_INPUT1_DRIVER
          #define TTC_AMOUNT_INPUTS 1
        #else
          #define TTC_AMOUNT_INPUTS 0
        #endif
      #endif
    #endif
  #endif
#endif

#ifndef TTC_ASSERT_INPUT    // any previous definition set (Makefile)?
#define TTC_ASSERT_INPUT 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_INPUT == 1)  // use Assert()s in INPUT code (somewhat slower but alot easier to debug)
  #define Assert_INPUT(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in INPUT code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_INPUT(Condition, ErrorCode)
#endif

#ifndef TTC_INPUT_SINGLE_THREADED // == 1: functions can use static variables to speed up function calls + reduce stack usage
#define TTC_INPUT_SINGLE_THREADED 1
#endif
#if (TTC_INPUT_SINGLE_THREADED == 1)
  #define INPUT_STATIC static
#else
  #define INPUT_STATIC
#endif


typedef struct ttc_input_generic_s { // architecture independent configuration data of single display
    // Note: Write-access to this structure is only allowed before first ttc_input_init() call!
    //       After initialization, only read accesses are allowed for high-level code.

    u8_t LogicalIndex;               // automatically set: logical index of display to use (1 = TTC_INPUT1, ...)

    union  {
        u32_t All;
        struct {
            // Bits
            unsigned Reserved1        : 32; // pad to 32 bits
        } Bits;
    } Flags;

    // type of driver used for this display
    ttc_input_driver_e Driver;

    // functions that can be provided by low-level driver (set during ttc_gfx_reset() )
    BOOL (*function_read_position)(volatile u16_t *x,volatile u16_t *y);

    // dimensions of display
    u16_t Width;
    u16_t Height;
    u16_t Depth;
    int  Key_Sta;    //status of the key on the screen (Up, Down)
    u8_t ChipVersion; //
    float xfac;     //adjusting faktor of the x-value
    float yfac;     //adjusting faktor of the y-value
    short xoff;     //save the offset of the x-axis
    short yoff;     //save the offset of the y-axis

} __attribute__((__packed__)) ttc_input_generic_t;


typedef enum {
    gss_Default,
    gss_Rectangle,
    gss_Rounded
} tit_InputAreaShape_e;

typedef enum {
    me_MouseDown,
    me_MouseUp,
    me_MouseMove,
    me_MoveIn,
    me_MoveOut,
    me_noEvent

} tit_MouseEvent_e;

typedef struct {
    unsigned EventOnMouseDown:1;
    unsigned EventOnMouseUp:1;
    unsigned EventOnMouseIn:1;
    unsigned EventOnMouseContinous:1;
    unsigned EventOnMouseOut:1;
    unsigned Reserved:1;
    unsigned Reserved1:1;
    unsigned Reserved2:1;
}__attribute__((__packed__)) Events_t;

typedef struct tit_InputArea_s {
    u16_t Left;   //left end of the botton
    u16_t Top;    //top of the botton
    u16_t Right;  //right end of the botton
    u16_t Bottom; //buttom of the button
    u16_t X;      //activ X position
    u16_t Y;      //activ Y position
    const char* Text; //Text in the case the area is a bottom or the draw area has got a head
    tit_MouseEvent_e Event; //gives information about the events that is necessary for executing the handler
    tit_MouseEvent_e CurrentEvent;
    tit_InputAreaShape_e Shape; //Layout of the font
    void (*TouchHandler)(void* Argument); //pointer on the handler function
    void* Argument; //argument of the handler funtion
    struct tit_InputArea_s* Next; //pointer on the next InputArea handler

} tit_InputArea_t;


typedef struct ScrollData_s {
    tit_InputArea_t Area; //Area of the Button
    u16_t oldX;      //previous X position
    u16_t oldY;      //previous Y position
} tit_ScrollData_t;

/*
typedef struct
{
        u16_t X0;         //value of the x-position after the refinement
        u16_t Y0;         //value of the y-position after the refinement
        u16_t X;          //value of the x-position
        u16_t Y;          //value of the y-position
        int  Key_Sta;    //status of the key on the screen (Up, Down)
        u8_t ChipVersion; //
        float xfac;     //adjusting faktor of the x-value
        float yfac;     //adjusting faktor of the y-value
        short xoff;     //save the offset of the x-axis
        short yoff;     //save the offset of the y-axis
} ttc_input_generic_t; // ToDo: name?
*/

#endif // TTC_INPUT_TYPES_H
