/** { ttc_dac.c ****************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for dac devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported 
 *  architectures.
 *
 *  See corresponding header file for documentation.
 *
 *  Created from template ttc_device.c revision 27 at 20141215 11:13:24 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_dac.h".
//
#include "ttc_dac.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_DAC_AMOUNT == 0
  #warning No DAC devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of dac devices.
 *
 */
 

// for each initialized device, a pointer to its generic and stm32l1xx definitions is stored
A_define(t_ttc_dac_config*, ttc_dac_configs, TTC_DAC_AMOUNT);

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8 ttc_dac_get_max_index() {
    return TTC_DAC_AMOUNT;
}
t_ttc_dac_config* ttc_dac_get_configuration(t_u8 LogicalIndex) {
    Assert_DAC(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_dac_config* Config = A(ttc_dac_configs, LogicalIndex-1); // will assert if outside array bounds

    if (!Config) {
        Config = A(ttc_dac_configs, LogicalIndex-1) = ttc_heap_alloc_zeroed(sizeof(t_ttc_dac_config));
        Config->Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_dac_load_defaults(LogicalIndex);
    }

    return Config;
}
void ttc_dac_deinit(t_u8 LogicalIndex) {
    Assert_DAC(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_dac_config* Config = ttc_dac_get_configuration(LogicalIndex);
  
    e_ttc_dac_errorcode Result = _driver_dac_deinit(Config);
    if (Result == ec_dac_OK)
      Config->Flags.Bits.Initialized = 0;
}
e_ttc_dac_errorcode ttc_dac_init(t_u8 LogicalIndex) {
    Assert_DAC(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_dac_config* Config = ttc_dac_get_configuration(LogicalIndex);

    e_ttc_dac_errorcode Result = _driver_dac_init(Config);
    if (Result == ec_dac_OK)
      Config->Flags.Bits.Initialized = 1;
    
    return Result;
}
e_ttc_dac_errorcode ttc_dac_load_defaults(t_u8 LogicalIndex) {
    Assert_DAC(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_dac_config* Config = ttc_dac_get_configuration(LogicalIndex);

    if (Config->Flags.Bits.Initialized)
      ttc_dac_deinit(LogicalIndex);
    
    ttc_memory_set( Config, 0, sizeof(t_ttc_dac_config) );

    Config->LogicalIndex = LogicalIndex;
    Config->PhysicalIndex = ttc_dac_logical_2_physical_index(LogicalIndex);
    Config->Channel = (t_u32) 0x00000000;

    //Insert additional generic default values here ...

    return _driver_dac_load_defaults(Config);    
}
t_u8 ttc_dac_logical_2_physical_index(t_u8 LogicalIndex) {
  
  switch (LogicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_DAC1
           case 1: return TTC_DAC1;
#endif
#ifdef TTC_DAC2
           case 2: return TTC_DAC2;
#endif
#ifdef TTC_DAC3
           case 3: return TTC_DAC3;
#endif
#ifdef TTC_DAC4
           case 4: return TTC_DAC4;
#endif
#ifdef TTC_DAC5
           case 5: return TTC_DAC5;
#endif
#ifdef TTC_DAC6
           case 6: return TTC_DAC6;
#endif
#ifdef TTC_DAC7
           case 7: return TTC_DAC7;
#endif
#ifdef TTC_DAC8
           case 8: return TTC_DAC8;
#endif
#ifdef TTC_DAC9
           case 9: return TTC_DAC9;
#endif
#ifdef TTC_DAC10
           case 10: return TTC_DAC10;
#endif
      // extend as required
      
      default: break;
    }
    
  Assert_DAC(0, ttc_assert_origin_auto); // No TTC_DACn defined! Check your makefile.100_board_* file!
  return -1; // program flow should never go here
}
t_u8 ttc_dac_physical_2_logical_index(t_u8 PhysicalIndex) {
  
  switch (PhysicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_DAC1
           case TTC_DAC1: return 1;
#endif
#ifdef TTC_DAC2
           case TTC_DAC2: return 2;
#endif
#ifdef TTC_DAC3
           case TTC_DAC3: return 3;
#endif
#ifdef TTC_DAC4
           case TTC_DAC4: return 4;
#endif
#ifdef TTC_DAC5
           case TTC_DAC5: return 5;
#endif
#ifdef TTC_DAC6
           case TTC_DAC6: return 6;
#endif
#ifdef TTC_DAC7
           case TTC_DAC7: return 7;
#endif
#ifdef TTC_DAC8
           case TTC_DAC8: return 8;
#endif
#ifdef TTC_DAC9
           case TTC_DAC9: return 9;
#endif
#ifdef TTC_DAC10
           case TTC_DAC10: return 10;
#endif
      // extend as required
      
      default: break;
    }
    
  Assert_DAC(0, ttc_assert_origin_auto); // No TTC_DACn defined! Check your makefile.100_board_* file!
  return -1; // program flow should never go here
}
void ttc_dac_prepare() {
  // add your startup code here (Singletasking!)
  _driver_dac_prepare();
}
void ttc_dac_reset(t_u8 LogicalIndex) {
    Assert_DAC(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_dac_config* Config = ttc_dac_get_configuration(LogicalIndex);

    _driver_dac_reset(Config);    
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_dac(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
