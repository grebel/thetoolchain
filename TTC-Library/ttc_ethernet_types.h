/** { ttc_ethernet_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for ETHERNET device.
 *  Structures, Enums and Defines being required by both, high- and low-level ethernet.
 *  This file does not provide function declarations.
 *
 *  Note: See ttc_ethernet.h for description of high-level ethernet implementation!
 *
 *  Created from template ttc_device_types.h revision 45 at 20180130 11:09:47 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_ETHERNET_TYPES_H
#define TTC_ETHERNET_TYPES_H

//{ Includes1 ************************************************************
//
// Includes being required for static configuration.
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_ethernet.h" or "ttc_ethernet.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h"

//} Includes
/** { Static Configuration ***********************************************
 *
 * Most ttc drivers are statically configured by constant definitions in makefiles.
 * A static configuration allows to hide certain code fragments and variables for the
 * compilation process. If used wisely, your code will be lean and fast and do only
 * what is required for the current configuration.
 */

/** {  TTC_ETHERNETn has to be defined as constant in one of your makefiles
 *
 * Here we count how much devices have been defined and if all lower index devices have been defined too.
 */
#ifndef TTC_ETHERNET_AMOUNT //{ 10
    #ifdef TTC_ETHERNET10
        #ifndef TTC_ETHERNET9
            #      error TTC_ETHERNET10 is defined, but not TTC_ETHERNET9 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET8
            #      error TTC_ETHERNET10 is defined, but not TTC_ETHERNET8 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET7
            #      error TTC_ETHERNET10 is defined, but not TTC_ETHERNET7 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET6
            #      error TTC_ETHERNET10 is defined, but not TTC_ETHERNET6 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET5
            #      error TTC_ETHERNET10 is defined, but not TTC_ETHERNET5 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET4
            #      error TTC_ETHERNET10 is defined, but not TTC_ETHERNET4 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET3
            #      error TTC_ETHERNET10 is defined, but not TTC_ETHERNET3 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET2
            #      error TTC_ETHERNET10 is defined, but not TTC_ETHERNET2 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET1
            #      error TTC_ETHERNET10 is defined, but not TTC_ETHERNET1 - all lower TTC_ETHERNETn must be defined!
        #endif

        #define TTC_ETHERNET_AMOUNT 10
    #endif
#endif //}
#ifndef TTC_ETHERNET_AMOUNT //{ 9
    #ifdef TTC_ETHERNET9
        #ifndef TTC_ETHERNET8
            #      error TTC_ETHERNET9 is defined, but not TTC_ETHERNET8 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET7
            #      error TTC_ETHERNET9 is defined, but not TTC_ETHERNET7 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET6
            #      error TTC_ETHERNET9 is defined, but not TTC_ETHERNET6 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET5
            #      error TTC_ETHERNET9 is defined, but not TTC_ETHERNET5 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET4
            #      error TTC_ETHERNET9 is defined, but not TTC_ETHERNET4 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET3
            #      error TTC_ETHERNET9 is defined, but not TTC_ETHERNET3 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET2
            #      error TTC_ETHERNET9 is defined, but not TTC_ETHERNET2 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET1
            #      error TTC_ETHERNET9 is defined, but not TTC_ETHERNET1 - all lower TTC_ETHERNETn must be defined!
        #endif

        #define TTC_ETHERNET_AMOUNT 9
    #endif
#endif //}
#ifndef TTC_ETHERNET_AMOUNT //{ 8
    #ifdef TTC_ETHERNET8
        #ifndef TTC_ETHERNET7
            #      error TTC_ETHERNET8 is defined, but not TTC_ETHERNET7 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET6
            #      error TTC_ETHERNET8 is defined, but not TTC_ETHERNET6 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET5
            #      error TTC_ETHERNET8 is defined, but not TTC_ETHERNET5 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET4
            #      error TTC_ETHERNET8 is defined, but not TTC_ETHERNET4 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET3
            #      error TTC_ETHERNET8 is defined, but not TTC_ETHERNET3 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET2
            #      error TTC_ETHERNET8 is defined, but not TTC_ETHERNET2 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET1
            #      error TTC_ETHERNET8 is defined, but not TTC_ETHERNET1 - all lower TTC_ETHERNETn must be defined!
        #endif

        #define TTC_ETHERNET_AMOUNT 8
    #endif
#endif //}
#ifndef TTC_ETHERNET_AMOUNT //{ 7
    #ifdef TTC_ETHERNET7
        #ifndef TTC_ETHERNET6
            #      error TTC_ETHERNET7 is defined, but not TTC_ETHERNET6 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET5
            #      error TTC_ETHERNET7 is defined, but not TTC_ETHERNET5 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET4
            #      error TTC_ETHERNET7 is defined, but not TTC_ETHERNET4 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET3
            #      error TTC_ETHERNET7 is defined, but not TTC_ETHERNET3 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET2
            #      error TTC_ETHERNET7 is defined, but not TTC_ETHERNET2 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET1
            #      error TTC_ETHERNET7 is defined, but not TTC_ETHERNET1 - all lower TTC_ETHERNETn must be defined!
        #endif

        #define TTC_ETHERNET_AMOUNT 7
    #endif
#endif //}
#ifndef TTC_ETHERNET_AMOUNT //{ 6
    #ifdef TTC_ETHERNET6
        #ifndef TTC_ETHERNET5
            #      error TTC_ETHERNET6 is defined, but not TTC_ETHERNET5 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET4
            #      error TTC_ETHERNET6 is defined, but not TTC_ETHERNET4 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET3
            #      error TTC_ETHERNET6 is defined, but not TTC_ETHERNET3 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET2
            #      error TTC_ETHERNET6 is defined, but not TTC_ETHERNET2 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET1
            #      error TTC_ETHERNET6 is defined, but not TTC_ETHERNET1 - all lower TTC_ETHERNETn must be defined!
        #endif

        #define TTC_ETHERNET_AMOUNT 6
    #endif
#endif //}
#ifndef TTC_ETHERNET_AMOUNT //{ 5
    #ifdef TTC_ETHERNET5
        #ifndef TTC_ETHERNET4
            #      error TTC_ETHERNET5 is defined, but not TTC_ETHERNET4 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET3
            #      error TTC_ETHERNET5 is defined, but not TTC_ETHERNET3 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET2
            #      error TTC_ETHERNET5 is defined, but not TTC_ETHERNET2 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET1
            #      error TTC_ETHERNET5 is defined, but not TTC_ETHERNET1 - all lower TTC_ETHERNETn must be defined!
        #endif

        #define TTC_ETHERNET_AMOUNT 5
    #endif
#endif //}
#ifndef TTC_ETHERNET_AMOUNT //{ 4
    #ifdef TTC_ETHERNET4
        #ifndef TTC_ETHERNET3
            #      error TTC_ETHERNET4 is defined, but not TTC_ETHERNET3 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET2
            #      error TTC_ETHERNET4 is defined, but not TTC_ETHERNET2 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET1
            #      error TTC_ETHERNET4 is defined, but not TTC_ETHERNET1 - all lower TTC_ETHERNETn must be defined!
        #endif

        #define TTC_ETHERNET_AMOUNT 4
    #endif
#endif //}
#ifndef TTC_ETHERNET_AMOUNT //{ 3
    #ifdef TTC_ETHERNET3

        #ifndef TTC_ETHERNET2
            #      error TTC_ETHERNET3 is defined, but not TTC_ETHERNET2 - all lower TTC_ETHERNETn must be defined!
        #endif
        #ifndef TTC_ETHERNET1
            #      error TTC_ETHERNET3 is defined, but not TTC_ETHERNET1 - all lower TTC_ETHERNETn must be defined!
        #endif

        #define TTC_ETHERNET_AMOUNT 3
    #endif
#endif //}
#ifndef TTC_ETHERNET_AMOUNT //{ 2
    #ifdef TTC_ETHERNET2

        #ifndef TTC_ETHERNET1
            #      error TTC_ETHERNET2 is defined, but not TTC_ETHERNET1 - all lower TTC_ETHERNETn must be defined!
        #endif

        #define TTC_ETHERNET_AMOUNT 2
    #endif
#endif //}
#ifndef TTC_ETHERNET_AMOUNT //{ 1
    #ifdef TTC_ETHERNET1
        #define TTC_ETHERNET_AMOUNT 1
    #endif
#endif //}
#ifndef TTC_ETHERNET_AMOUNT //{ no devices defined at all
    #  warning Missing definition for TTC_ETHERNET1. Using default type. Add valid definition to your makefile to get rid if this message!
    // option 1: no devices available
    #define TTC_ETHERNET_AMOUNT 0
    // option 2: define a default device (enable lines below)
    // #define TTC_ETHERNET_AMOUNT 1
    // define TTC_ETHERNET1 ??? // <- place entry from e_ttc_ethernet_architecture here!!!
#endif

//}
//}
/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_ETHERNET 0#         disable default asserts for ethernet driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_ETHERNET_EXTRA 1#   enable extra asserts for ethernet driver
 *
 * When debugging code that has been compiled with optimization (gcc option -O) then
 * certain variables may be optimized away. Declaring variables as VOLATILE_ETHERNET makes them
 * visible in debug session when asserts are enabled. You have no overhead at all if asserts are disabled.
 + foot_t* VOLATILE_ETHERNET VisiblePointer;
 *
 */
#ifndef TTC_ASSERT_ETHERNET    // any previous definition set (Makefile)?
    #define TTC_ASSERT_ETHERNET 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_ETHERNET == 1)  // use Assert()s in ETHERNET code (somewhat slower but alot easier to debug)
    #define Assert_ETHERNET(Condition, Origin)        Assert(Condition, Origin)
    #define Assert_ETHERNET_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_ETHERNET_Readable(Address, Origin) Assert_Readable(Address, Origin)
    #define VOLATILE_ETHERNET                         volatile
#else  // use no default Assert()s in ETHERNET code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_ETHERNET(Condition, Origin)
    #define Assert_ETHERNET_Writable(Address, Origin)
    #define Assert_ETHERNET_Readable(Address, Origin)
    #define VOLATILE_ETHERNET
#endif

#ifndef TTC_ASSERT_ETHERNET_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_ETHERNET_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_ETHERNET_EXTRA == 1)  // use Assert()s in ETHERNET code (somewhat slower but alot easier to debug)
    #define Assert_ETHERNET_EXTRA(Condition, Origin) Assert(Condition, Origin)
    #define Assert_ETHERNET_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_ETHERNET_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in ETHERNET code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_ETHERNET_EXTRA(Condition, Origin)
    #define Assert_ETHERNET_EXTRA_Writable(Address, Origin)
    #define Assert_ETHERNET_EXTRA_Readable(Address, Origin)
#endif
//}
/** { Check static configuration of ethernet devices
 *
 * Each TTC_ETHERNETn must be defined as one from e_ttc_ethernet_architecture.
 * Additional defines of type TTC_ETHERNETn_* may be added.
 *
 * The constants being checked here must be defined in your makefile.
 * Most constants are defined by EXTENSION_100_board_* or EXTENSION_150_board_extension_* files.
 */

#ifdef TTC_ETHERNET1

    // check extra defines for ethernet device #1 here

#endif
#ifdef TTC_ETHERNET2

    // check extra defines for ethernet device #2 here

#endif
#ifdef TTC_ETHERNET3

    // check extra defines for ethernet device #3 here

#endif
#ifdef TTC_ETHERNET4

    // check extra defines for ethernet device #4 here

#endif
#ifdef TTC_ETHERNET5

    // check extra defines for ethernet device #5 here

#endif
#ifdef TTC_ETHERNET6

    // check extra defines for ethernet device #6 here

#endif
#ifdef TTC_ETHERNET7

    // check extra defines for ethernet device #7 here

#endif
#ifdef TTC_ETHERNET8

    // check extra defines for ethernet device #8 here

#endif
#ifdef TTC_ETHERNET9

    // check extra defines for ethernet device #9 here

#endif
#ifdef TTC_ETHERNET10

    // check extra defines for ethernet device #10 here

#endif
//}

//}Static Configuration

//{ Includes2 ************************************************************
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_ethernet.h" or "ttc_ethernet.c"
//

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_ethernet_ste101p
    #include "ethernet/ethernet_ste101p_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

/** { Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_ethernet_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_ethernet_architecture
    #  warning Missing low-level definition for t_ttc_ethernet_architecture (using default)
    #define t_ttc_ethernet_architecture void
#endif
//}

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_ethernet_errorcode       return codes of ETHERNET devices
    E_ttc_ethernet_errorcode_OK = 0,

    // other warnings go here..

    E_ttc_ethernet_errorcode_ERROR,                  // general failure
    E_ttc_ethernet_errorcode_NULL,                   // NULL pointer not accepted
    E_ttc_ethernet_errorcode_DeviceNotFound,         // corresponding device could not be found
    E_ttc_ethernet_errorcode_InvalidConfiguration,   // sanity check of device configuration failed
    E_ttc_ethernet_errorcode_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..

    E_ttc_ethernet_errorcode_unknown                // no valid errorcodes past this entry
} e_ttc_ethernet_errorcode;
typedef enum {   // e_ttc_ethernet_architecture    types of architectures supported by ETHERNET driver
    E_ttc_ethernet_architecture_None = 0,       // no architecture selected


    E_ttc_ethernet_architecture_ste101p, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    E_ttc_ethernet_architecture_unknown        // architecture not supported
} e_ttc_ethernet_architecture;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef union {  // architecture dependent configuration

#ifdef EXTENSION_ethernet_ste101p
    t_ethernet_ste101p_config ste101p;  // automatically added by ./create_DeviceDriver.pl
#endif
    //InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_ethernet_architecture;
typedef struct s_ttc_ethernet_features { // static minimum, maximum and default values of features of single ethernet

    // Add any amount of architecture independent values describing ethernet devices here.
    // You may also want to add a plausibility check for each value to _ttc_ethernet_configuration_check().

    /** Example Features

    t_u16 MinBitRate;   // minimum allowed transfer speed
    t_u16 MaxBitRate;   // maximum allowed transfer speed
    */

    const t_u8 unused; // remove me!

} __attribute__( ( __packed__ ) ) t_ttc_ethernet_features;
typedef struct s_ttc_ethernet_config { //          architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_ethernet_init()
    //       and after ttc_ethernet_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // Hint: Order structure elements for optimal alignment.
    //       You should place smaller datatypes like t_u8 and t_u16 after all t_u32 and pointer types
    //       to prevent insertion of pad-bytes. Most architectures don't support placing 32-bit datatypes at
    //       unaligned address.

    struct { // Init: fields to be set by application before first calling ttc_ethernet_init() --------------
        // Do not change these values after calling ttc_ethernet_init()!

        //...

        struct { // generic initial configuration bits common for all low-level Drivers
            unsigned unused : 1;  // ==1:
            // ToDo: additional high-level flags go here..
        } Flags;
    } Init;

    // Fields below are read-only for application! ----------------------------------------------------------

    u_ttc_ethernet_architecture* LowLevelConfig; // low-level configuration (structure defined by low-level driver)

    const t_ttc_ethernet_features* Features;     // constant features of this ethernet

    struct { // status flags common for all architectures
        unsigned Initialized        : 1;  // ==1: device has been initialized successfully
        // ToDo: additional high-level flags go here..
    } Flags;

    // Last returned error code
    // IMPORTANT: Every ttc_ethernet_*() function that returns e_ttc_ethernet_errorcode has to update this value if it returns an error!
    e_ttc_ethernet_errorcode LastError;

    t_u8  LogicalIndex;                          // automatically set: logical index of device to use (1 = TTC_ETHERNET1, ...)
    t_u8  PhysicalIndex;                         // automatically set: index of physical device to use (0 = ethernet #1, ...)
    e_ttc_ethernet_architecture  Architecture;   // type of architecture used for current ethernet device

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_ethernet_config;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_ETHERNET_TYPES_H
