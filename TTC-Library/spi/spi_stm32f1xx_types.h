#ifndef SPI_STM32F1XX_TYPES_H
#define SPI_STM32F1XX_TYPES_H

/** { spi_stm32f1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for SPI devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by ttc_spi_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140515 11:30:41 UTC
 *
 *  Note: See ttc_spi.h for description of architecture independent SPI implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic.h"
#include "../ttc_register.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_spi_types.h *************************

typedef struct { // register description (adapt according to stm32f1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_spi_register;

typedef struct {  // stm32f1xx specific configuration data of single SPI device
    t_register_stm32f1xx_spi* BaseRegister;       // base address of SPI device registers
} __attribute__( ( __packed__ ) ) t_spi_stm32f1xx_config;

// t_ttc_spi_architecture is required by ttc_spi_types.h
#define t_ttc_spi_architecture t_spi_stm32f1xx_config

//} Structures/ Enums


#endif //SPI_STM32F1XX_TYPES_H
