#ifndef SPI_STM32L1XX_TYPES_H
#define SPI_STM32L1XX_TYPES_H

/** { spi_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for SPI devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_spi_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140423 11:04:41 UTC
 *
 *  Note: See ttc_spi.h for description of architecture independent SPI implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic.h"
#include "../register/register_stm32l1xx_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_spi_types.h *************************

/* DEPRECATED
typedef struct  // stm32l1xx ADC Init structure definition
{
    t_u16 spi_Direction;           // Specifies the SPI unidirectional or bidirectional
    t_u16 spi_Mode;                // SPI operating mode
    t_u16 spi_DataSize;            // Specifies the SPI data size
    t_u16 spi_CPOL;                // Specifies the serial clock steady state.
    t_u16 spi_CPHA;                // Specifies the clock active edge for the bit capture.
    t_u16 spi_NSS;                 // Specifies whether the NSS signal is managed by
    t_u16 spi_BaudRatePrescaler;   // Baud Rate prescaler
    t_u16 spi_FirstBit;            // Specifies whether data transfers start from MSB or LSB bit.
    t_u16 spi_CRCPolynomial;       // Specifies the polynomial used for the CRC calculation.

} spi_init_structure;
*/

typedef struct { // register description (adapt according to stm32l1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_spi_register;

typedef struct {  // stm32l1xx specific configuration data of single SPI device
    volatile t_register_stm32l1xx_spi* BaseRegister;       // base address of SPI device registers
} __attribute__( ( __packed__ ) ) t_spi_stm32l1xx_config;

// t_ttc_spi_architecture is required by ttc_spi_types.h
#define t_ttc_spi_architecture t_spi_stm32l1xx_config

//} Structures/ Enums


#endif //SPI_STM32L1XX_TYPES_H
