/** { spi_stm32f1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for spi devices on stm32f1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 21 at 20140515 11:30:41 UTC
 *
 *  Note: See ttc_spi.h for description of stm32f1xx independent SPI implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "spi_stm32f1xx.h"
#include "../ttc_gpio.h"
#include "../ttc_heap.h"
#include "../ttc_sysclock.h"
#include "../ttc_systick.h"
#include "../ttc_register.h"
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

void                spi_stm32f1xx_deinit( t_ttc_spi_config* Config ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    // reset + disable SPI device
    if ( Config->LowLevelConfig->BaseRegister == &register_stm32f1xx_SPI1 ) {
        register_stm32f1xx_RCC.APB2RSTR.SPI1RST = 1;
        register_stm32f1xx_RCC.APB2RSTR.SPI1RST = 0;
        register_stm32f1xx_RCC.APB2ENR.SPI1_EN  = 0;
        return;
    }
    if ( Config->LowLevelConfig->BaseRegister == &register_stm32f1xx_SPI2 ) {

        register_stm32f1xx_RCC.APB1RSTR.SPI2RST = 1;
        register_stm32f1xx_RCC.APB1RSTR.SPI2RST = 0;
        register_stm32f1xx_RCC.APB1ENR.SPI2_EN   = 0;
        return;
    }
    if ( Config->LowLevelConfig->BaseRegister == &register_stm32f1xx_SPI3 ) {

        register_stm32f1xx_RCC.APB1RSTR.SPI3RST = 1;
        register_stm32f1xx_RCC.APB1RSTR.SPI3RST = 0;
        register_stm32f1xx_RCC.APB1ENR.SPI3_EN   = 0;
        return;
    }
}
e_ttc_spi_errorcode spi_stm32f1xx_get_features( t_ttc_spi_config* Config ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    ttc_memory_set( & Config->Init.Flags, 0, sizeof( Config->Init.Flags ) );
    Config->Init.Flags.Master                = 1;
    Config->Init.Flags.MultiMaster           = 1;
    Config->Init.Flags.ClockIdleHigh         = 1;
    Config->Init.Flags.ClockPhase2ndEdge     = 1;
    Config->Init.Flags.Simplex               = 1;
    Config->Init.Flags.Bidirectional         = 1;
    Config->Init.Flags.Receive               = 1;
    Config->Init.Flags.Transmit              = 1;
    Config->Init.Flags.WordSize16            = 1;
    Config->Init.Flags.HardwareNSS           = 1;
    TODO( "Config->Init.Flags.CRC8                  = 1; " )
    TODO( "Config->Init.Flags.CRC16                 = 1; " )
    Config->Init.Flags.FirstBitMSB           = 1;
    TODO( "Config->Init.Flags.TxDMA                 = 1; " )
    TODO( "Config->Init.Flags.RxDMA                 = 1; " )
    TODO( "Config->Init.Flags.Irq_Error             = 1; " )
    TODO( "Config->Init.Flags.Irq_Received          = 1; " )
    TODO( "Config->Init.Flags.Irq_Transmitted       = 1; " )

    Config->Init.CRC_Polynom                 = 0x7;
    Config->Layout      = 0;

    //X RCC_ClocksTypeDef RCC_Clocks;
    //X RCC_GetClocksFreq( &RCC_Clocks );

    //X Config->Init.BaudRatePrescaler   = 255;                             // max divider
    //    Config->Init.BaudRate  = RCC_Clocks.PCLK2_Frequency / 2;  // max frequency
    // maximum possible baudrate at current system clock frequency
    Config->Init.BaudRate = ttc_sysclock_get_configuration()->LowLevelConfig.frequency_apb2 / 2;

    Config->LowLevelConfig->BaseRegister = NULL;
    switch ( Config->LogicalIndex ) {         // find corresponding SPI as defined by makefile.100_board_*
#ifdef TTC_SPI1
        case 1:
            switch ( TTC_SPI1 ) {
                case 0: Config->LowLevelConfig->BaseRegister = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI1;
                    Config->PhysicalIndex = 0;
                    break;
                case 1: Config->LowLevelConfig->BaseRegister = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI2;
                    Config->PhysicalIndex = 1;
                    break;
                case 2: Config->LowLevelConfig->BaseRegister = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI3;
                    break;
                    Config->PhysicalIndex = 2;
                default: Config->LowLevelConfig->BaseRegister = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI3;
                    Config->PhysicalIndex = 2;
                    break;
            }
            break;
#endif
#ifdef TTC_SPI2
        case 2:
            switch ( TTC_SPI2 ) {
                case 0: Config->LowLevelConfig->BaseRegister = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI1;
                    Config->PhysicalIndex = 0;
                    break;
                case 1: Config->LowLevelConfig->BaseRegister = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI2;
                    Config->PhysicalIndex = 1;
                    break;
                case 2: Config->LowLevelConfig->BaseRegister = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI3;
                    break;
                    Config->PhysicalIndex = 2;
                default: Config->LowLevelConfig->BaseRegister = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI3;
                    Config->PhysicalIndex = 2;
                    break;
            }
            break;
#endif
#ifdef TTC_SPI3
        case 3:
            switch ( TTC_SPI3 ) {
                case 0: Config->LowLevelConfig->BaseRegister = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI1;
                    Config->PhysicalIndex = 0;
                    break;
                case 1: Config->LowLevelConfig->BaseRegister = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI2;
                    Config->PhysicalIndex = 1;
                    break;
                case 2: Config->LowLevelConfig->BaseRegister = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI3;
                    break;
                    Config->PhysicalIndex = 2;
                default: Config->LowLevelConfig->BaseRegister = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI3;
                    Config->PhysicalIndex = 2;
                    break;
            }
            break;
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // No TTC_SPIn defined! Check your makefile.100_board_* file!
    }
    switch ( Config->PhysicalIndex ) { // find amount of available remapping-layouts (-> RM0008 p. 176)
        case 0:
            Config->Layout = 1;   // SPI1 provides 1 alternate pin layout
            break;
        case 1:
            Config->Layout = 0;   // SPI2 provides 0 alternate pin layouts
        case 2:
#ifdef STM32F10X_CL
            Config->Layout = 1; // SPI3 provides 1 alternate pin layout in Connection Line Devices
#else
            Config->Layout = 0; // SPI3 provides 0 alternate pin layout
#endif
            break;
        default: ttc_assert_halt_origin( ttc_assert_origin_auto );
    }
    Config->Init.Flags.HardwareNSS = 0; // must use SoftwareNSS for configured pin

    switch ( Config->LogicalIndex ) {         // disable features requiring unconfigured pins
        case 1: {
#ifndef TTC_SPI1_MOSI
            Config->Init.Flags.Transmit = 0;
#endif
#ifndef TTC_SPI1_MISO
            Config->Init.Flags.Receive  = 0;
#endif
#ifndef TTC_SPI1_NSS
            Config->Init.Flags.HardwareNSS = 0;
#else

#endif
#ifndef TTC_SPI1_SCK
            Config->Init.Flags.Transmit = 0;
            Config->Init.Flags.Receive  = 0;
#endif

            break;
        }
        case 2: {
#ifndef TTC_SPI2_MOSI
            Config->Init.Flags.Transmit = 0;
#endif
#ifndef TTC_SPI2_MISO
            Config->Init.Flags.Receive  = 0;
#endif
#ifndef TTC_SPI2_NSS
            Config->Init.Flags.HardwareNSS  = 0;
#endif
#ifndef TTC_SPI2_SCK
            Config->Init.Flags.Transmit = 0;
            Config->Init.Flags.Receive  = 0;
#endif

            break;
        }
        case 3: {
#ifndef TTC_SPI3_MOSI
            Config->Init.Flags.Transmit = 0;
#endif
#ifndef TTC_SPI3_MISO
            Config->Init.Flags.Receive  = 0;
#endif
#ifndef TTC_SPI3_NSS
            Config->Init.Flags.HardwareNSS  = 0;
#endif
#ifndef TTC_SPI3_SCK
            Config->Init.Flags.Transmit = 0;
            Config->Init.Flags.Receive  = 0;
#endif

            break;
        }
        default: ttc_assert_halt_origin( ttc_assert_origin_auto );
            return ec_spi_DeviceNotFound;; // SPI misconfigured! Check your makefile.100_board_* file!
    }

    return ( e_ttc_spi_errorcode ) 0;
}
e_ttc_spi_errorcode spi_stm32f1xx_init( t_ttc_spi_config* Config ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    t_register_stm32f1xx_spi* SPI = Config->LowLevelConfig->BaseRegister;

    if ( 1 ) { // validate SPI_Features
        switch ( Config->LogicalIndex ) {              // find SPI corresponding to SPI_index as defined by makefile.100_board_*
#ifdef TTC_SPI1
            case 1:
                switch ( TTC_SPI1 ) {
                    case 0: SPI = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI1;
                        Config->PhysicalIndex = 0;
                        break;
                    case 1: SPI = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI2;
                        Config->PhysicalIndex = 1;
                        break;
                    case 2: SPI = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI3;
                        break;
                        Config->PhysicalIndex = 2;
                    default: SPI = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI3;
                        Config->PhysicalIndex = 2;
                        break;
                }
                break;
#endif
#ifdef TTC_SPI2
            case 2:
                switch ( TTC_SPI2 ) {
                    case 0: SPI = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI1;
                        Config->PhysicalIndex = 0;
                        break;
                    case 1: SPI = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI2;
                        Config->PhysicalIndex = 1;
                        break;
                    case 2: SPI = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI3;
                        break;
                        Config->PhysicalIndex = 2;
                    default: SPI = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI3;
                        Config->PhysicalIndex = 2;
                        break;
                }
                break;
#endif
#ifdef TTC_SPI3
            case 3:
                switch ( TTC_SPI3 ) {
                    case 0: SPI = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI1;
                        Config->PhysicalIndex = 0;
                        break;
                    case 1: SPI = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI2;
                        Config->PhysicalIndex = 1;
                        break;
                    case 2: SPI = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI3;
                        break;
                        Config->PhysicalIndex = 2;
                    default: SPI = ( t_register_stm32f1xx_spi* )&register_stm32f1xx_SPI3;
                        Config->PhysicalIndex = 2;
                        break;
                }
                break;
#endif
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // No TTC_SPIn defined! Check your makefile.100_board_* file!
        }

        switch ( Config->LogicalIndex ) {  // check if board has defined the MOSI-pin of SPI to init
#ifdef TTC_SPI1_MOSI
            case 1:
                Config->Init.Pin_MOSI = TTC_SPI1_MOSI;

                break;
#endif
#ifdef TTC_SPI2_MOSI
            case 2:
                Config->Init.Pin_MOSI = TTC_SPI2_MOSI;
                break;
#endif
#ifdef TTC_SPI3_MOSI
            case 3:
                Config->Init.Pin_MOSI = TTC_SPI3_MOSI;
                break;
#endif
            default:
                ttc_assert_halt_origin( ttc_assert_origin_auto ); // No TTC_SPIn_MOSI defined! Check your makefile.100_board_* file!
                break;
        }
        switch ( Config->PhysicalIndex ) { // determine pin layout (-> RM0008 p.176)
            case 0: { // SPI1
                if ( Config->Init.Pin_MOSI == E_ttc_gpio_pin_a15 ) { Config->Layout = 1; }
                else                    { Config->Layout = 0; }

                if ( Config->Layout ) { // alternate pin remapping (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
                    Config->Init.Pin_SCLK   = E_ttc_gpio_pin_b3;
                    Config->Init.Pin_MISO   = E_ttc_gpio_pin_b4;
                    Config->Init.Pin_MOSI   = E_ttc_gpio_pin_a5;
                }
                else {        // no pin remapping
                    Config->Init.Pin_SCLK   = E_ttc_gpio_pin_a5;
                    Config->Init.Pin_MISO   = E_ttc_gpio_pin_a6;
                    Config->Init.Pin_MOSI   = E_ttc_gpio_pin_a7;
                }
                break;
            }
            case 1: { // SPI2 has no alternative layout
                Config->Init.Pin_SCLK   = E_ttc_gpio_pin_b13;
                Config->Init.Pin_MISO   = E_ttc_gpio_pin_b14;
                Config->Init.Pin_MOSI   = E_ttc_gpio_pin_b15;
                break;
            }
            case 2: { // SPI3
                if ( Config->Init.Pin_MOSI == E_ttc_gpio_pin_c12 )      { Config->Layout = 1; } // pin remapping
                else                         { Config->Layout = 0; } // no pin remapping

                if ( Config->Layout ) { // alternate pin remapping (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
                    Config->Init.Pin_SCLK   = E_ttc_gpio_pin_c10;
                    Config->Init.Pin_MISO   = E_ttc_gpio_pin_c11;
                    Config->Init.Pin_MOSI   = E_ttc_gpio_pin_c12;
                }
                else {                     // no pin remapping
                    Config->Init.Pin_SCLK   = E_ttc_gpio_pin_b3;
                    Config->Init.Pin_MISO   = E_ttc_gpio_pin_b4;
                    Config->Init.Pin_MOSI   = E_ttc_gpio_pin_b5;
                }
                break;
            }
            default: { return ttc_assert_origin_auto; }
        }
    }
    Config->LowLevelConfig->BaseRegister = SPI;
    spi_stm32f1xx_deinit( Config );

    if ( 1 ) { // activate clocks to internal functional units

        switch ( Config->PhysicalIndex ) {
            case 0: {
                register_stm32f1xx_RCC.APB2ENR.SPI1_EN = 1;
                break;
            }
            case 1: {
                register_stm32f1xx_RCC.APB1ENR.SPI2_EN = 1;
                break;
            }
            case 2: {
                register_stm32f1xx_RCC.APB1ENR.SPI3_EN = 1;
                break;
            }
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // should never occur!
        }
    }
    if ( 1 ) { // configure GPIOs

        if ( Config->Init.Flags.Master ) {
            ttc_gpio_init( Config->Init.Pin_MISO, E_ttc_gpio_mode_input_pull_up,                E_ttc_gpio_speed_50mhz );
            ttc_gpio_init( Config->Init.Pin_MOSI, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_50mhz );
            ttc_gpio_init( Config->Init.Pin_SCLK, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_50mhz );
        }
        else {
            if ( Config->Init.Pin_MOSI )  { gpio_stm32f1xx_init( Config->Init.Pin_MOSI, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_max ); }
            if ( Config->Init.Pin_MISO )  { gpio_stm32f1xx_init( Config->Init.Pin_MISO, E_ttc_gpio_mode_alternate_function_push_pull,       E_ttc_gpio_speed_max ); }
            if ( Config->Init.Pin_SCLK )  { gpio_stm32f1xx_init( Config->Init.Pin_SCLK, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_max ); }
        }
    }
    if ( 1 ) { // apply remap layout
        register_stm32f1xx_RCC.APB2ENR.AFIO_EN = 1;

        switch ( Config->PhysicalIndex ) {
            case 0: { // SPI1
                register_stm32f1xx_AFIO.MAPR.SPI1_REMAP = Config->Layout;
                break;
            }
#ifdef STM32F10X_CL
            case 2: { // SPI3
                register_stm32f1xx_AFIO.MAPR.SPI3_REMAP = Config->Layout;
                break;
#endif
                default: { break; }
                }
        }
    }
    t_register_stm32f1xx_spi_cr1 CR1; CR1.All = 0;
    t_register_stm32f1xx_spi_cr2 CR2; CR2.All = 0;
    if ( 1 ) { // init SPI (-> RM0008 p. 535)

        // disable SPI during configuration
        SPI->CR1.All = 0;
        SPI->CR2.All = 0;

        CR1.Bits.SPE = 1; // enable peripheral

        if ( Config->Init.Flags.Transmit && Config->Init.Flags.Receive ) {
            CR1.Bits.BIDIMODE = Config->Init.Flags.Bidirectional;
        }
        else
        {CR1.Bits.RXONLY = Config->Init.Flags.Receive;} // only receiving
        CR1.Bits.MSTR     = Config->Init.Flags.Master;
        CR1.Bits.DFF      = Config->Init.Flags.WordSize16;
        CR1.Bits.CPOL     = Config->Init.Flags.ClockIdleHigh;
        CR1.Bits.CPHA     = Config->Init.Flags.ClockPhase2ndEdge ;
        CR1.Bits.SSM      = ( Config->Init.Flags.HardwareNSS ) ? 0 : 1;
        CR1.Bits.SSI      = ( Config->Init.Flags.HardwareNSS ) ? 0 : 1; // must be set too if SSM==1 to avoid MODF error
        CR1.Bits.LSBFIRST = ( Config->Init.Flags.FirstBitMSB ) ? 0 : 1;

        CR2.Bits.SSOE = Config->Init.Flags.MultiMaster ? 0 : 1; // disable SS output to allow working in multimaster mode
        Assert_SPI( Config->Init.BaudRate > 0, ttc_assert_origin_auto );

        // calculate ideal prescaler from baudrate
        //            RCC_ClocksTypeDef RCC_Clocks;
        //            RCC_GetClocksFreq(&RCC_Clocks);
        t_u16 Prescaler;
        t_base Frequency;

        //RCC_Clocks.PCLK2_Frequency / Config->Init.BaudRate;

        t_ttc_sysclock_config* SysclockConfig = ttc_sysclock_get_configuration();
        if ( 1 ) { // activate clocks
            switch ( Config->PhysicalIndex ) {
                case 0: {
                    Prescaler = SysclockConfig->LowLevelConfig.frequency_apb2 / Config->Init.BaudRate;
                    Frequency = SysclockConfig->LowLevelConfig.frequency_apb2;
                    break;
                }
                case 1: {
                    Prescaler = SysclockConfig->LowLevelConfig.frequency_apb1 / Config->Init.BaudRate;
                    Frequency = SysclockConfig->LowLevelConfig.frequency_apb1;
                    break;
                }
                case 2: {
                    Prescaler = SysclockConfig->LowLevelConfig.frequency_apb1 / Config->Init.BaudRate;
                    Frequency = SysclockConfig->LowLevelConfig.frequency_apb1;
                    break;
                }
                default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // should never occur!
            }
        }

        // approximate baudrate prescaler + calculate effective baudrate
        if ( Prescaler < 3 ) {
            CR1.Bits.BR = 0;
            Config->Init.BaudRate = Frequency / 2;
        }
        else if ( Prescaler < 7 ) {
            CR1.Bits.BR = 1;
            Config->Init.BaudRate = Frequency / 4;
        }
        else if ( Prescaler < 13 ) {
            CR1.Bits.BR = 2;
            Config->Init.BaudRate = Frequency / 8;
        }
        else if ( Prescaler < 25 ) {
            CR1.Bits.BR = 3;
            Config->Init.BaudRate = Frequency / 16;
        }
        else if ( Prescaler < 49 ) {
            CR1.Bits.BR = 4;
            Config->Init.BaudRate = Frequency / 32;
        }
        else if ( Prescaler < 97 ) {
            CR1.Bits.BR = 5;
            Config->Init.BaudRate = Frequency / 64;
        }
        else if ( Prescaler < 193 ) {
            CR1.Bits.BR = 6;
            Config->Init.BaudRate = Frequency / 128;
        }
        else {
            CR1.Bits.BR = 7;
            Config->Init.BaudRate = Frequency / 256;
        }

        SPI->SPI_CRCPR.Polynom = ( t_u16 )Config->Init.CRC_Polynom;
        CR1.Bits.CRC_EN = ( Config->Init.Flags.CRC8 || Config->Init.Flags.CRC16 ) ? 1 : 0;

        // write new configuration into spi registers
        SPI->CR2.All = CR2.All;
        SPI->CR1.All = CR1.All;
        Assert_SPI_EXTRA( SPI->CR1.All == CR1.All, ttc_assert_origin_auto ); // Cannot configure SPI interface as requested. Check configuration!
        Assert_SPI( SPI->SR.Bits.MODF == 0, ttc_assert_origin_auto ); // SPI interface shows a master mode fault error (-> RM0008 p.693)

        // calculate maximum time to wait for data from slave as the time of 16 clocks at current baudrate
        Config->TimeOutRxNe_us = 200000000 / Config->Init.BaudRate;
        if ( Config->TimeOutRxNe_us < ttc_systick_delay_get_minimum() ) // timeout is smaller than minimum delay at current system clock frequency: don't use timeouts as spi should be fast egnough
        { Config->TimeOutRxNe_us = 0; }
    }
    if ( 1 ) { // compare chosen pin layout with configured pins
        e_ttc_gpio_pin Port;
        switch ( Config->LogicalIndex ) { // check if board has defined the TX-pin of SPI to init
            case 1: { // check configuration of SPI #1
#ifdef TTC_SPI1_MOSI
                Port = TTC_SPI1_MOSI;
                Assert_SPI( Port == Config->Init.Pin_MOSI, ttc_assert_origin_auto );  // ERROR: configured GPIO cannot be used in current remapping layout!
#else
                Config->Init.Pin_MOSI = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_SPI1_MISO
                Port = TTC_SPI1_MISO;
                Assert_SPI( Port == Config->Init.Pin_MISO, ttc_assert_origin_auto );  // ERROR: configured GPIO cannot be used in current remapping layout!
#else
                Config->Init.Pin_MISO = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_SPI1_SCK
                Port = TTC_SPI1_SCK;
                Assert_SPI( Port == Config->Init.Pin_SCLK, ttc_assert_origin_auto );  // ERROR: configured GPIO cannot be used in current remapping layout!
#else
                Config->Init.Pin_SCLK = E_ttc_gpio_pin_none;
#endif
                break;
            }
            case 2: { // check configuration of SPI #2
#ifdef TTC_SPI2_MOSI
                Port = TTC_SPI2_MOSI;
                Assert_SPI( Port == Config->Init.Pin_MOSI, ttc_assert_origin_auto );  // ERROR: configured GPIO cannot be used in current remapping layout!
#else
                Config->Init.Pin_MOSI = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_SPI2_MISO
                Port = TTC_SPI2_MISO;
                Assert_SPI( Port == Config->Init.Pin_MISO, ttc_assert_origin_auto );  // ERROR: configured GPIO cannot be used in current remapping layout!
#else
                Config->Init.Pin_MISO = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_SPI2_SCK
                Port = TTC_SPI2_SCK;
                Assert_SPI( Port == Config->Init.Pin_SCLK, ttc_assert_origin_auto );  // ERROR: configured GPIO cannot be used in current remapping layout!
#else
                Config->Init.Pin_SCLK = E_ttc_gpio_pin_none;
#endif
                break;
            }
            case 3: { // check configuration of SPI #3
#ifdef TTC_SPI3_MOSI
                Port = TTC_SPI3_MOSI;
                Assert_SPI( Port == Config->Init.Pin_MOSI, ttc_assert_origin_auto );  // ERROR: configured GPIO cannot be used in current remapping layout!
#else
                Config->Init.Pin_MOSI = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_SPI3_MISO
                Port = TTC_SPI3_MISO;
                Assert_SPI( Port == Config->Init.Pin_MISO, ttc_assert_origin_auto );  // ERROR: configured GPIO cannot be used in current remapping layout!
#else
                Config->Init.Pin_MISO = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_SPI3_SCK
                Port = TTC_SPI3_SCK;
                Assert_SPI( Port == Config->Init.Pin_SCLK, ttc_assert_origin_auto );  // ERROR: configured GPIO cannot be used in current remapping layout!
#else
                Config->Init.Pin_SCLK = E_ttc_gpio_pin_none;
#endif
                break;
            }
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // No TTC_SPIn defined! Check your makefile.100_board_* file!
        }
    }


    return ( e_ttc_spi_errorcode ) 0;
}
e_ttc_spi_errorcode spi_stm32f1xx_load_defaults( t_ttc_spi_config* Config ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    ttc_memory_set( &Config->Init.Flags, 0, sizeof( Config->Init.Flags ) );
    Config->Init.Flags.Master                = 1;
    Config->Init.Flags.MultiMaster           = 1;
    Config->Init.Flags.ClockIdleHigh         = 1;
    Config->Init.Flags.ClockPhase2ndEdge     = 1;
    Config->Init.Flags.Simplex               = 0;
    Config->Init.Flags.Bidirectional         = 0;
    Config->Init.Flags.Receive               = 1;
    Config->Init.Flags.Transmit              = 1;
    Config->Init.Flags.WordSize16            = 0;
    Config->Init.Flags.HardwareNSS           = 0;
    Config->Init.Flags.FirstBitMSB           = 1;
    TODO( "Config->Init.Flags.CRC8                  = 1;" )
    TODO( "Config->Init.Flags.CRC16                 = 1;" )
    TODO( "Config->Init.Flags.TxDMA                 = 1;" )
    TODO( "Config->Init.Flags.RxDMA                 = 1;" )
    TODO( "Config->Init.Flags.Irq_Error             = 1;" )
    TODO( "Config->Init.Flags.Irq_Received          = 1;" )
    TODO( "Config->Init.Flags.Irq_Transmitted       = 1;" )

    Config->Init.CRC_Polynom = 7;
    Config->Layout      = 0;

    //X     RCC_ClocksTypeDef RCC_Clocks;
    //X     RCC_GetClocksFreq( &RCC_Clocks );

    //X Config->Init.BaudRatePrescaler   = 16;        // medium speed
    //X Config->Init.BaudRate  = RCC_Clocks.PCLK2_Frequency / 16; // medium speed

    // calculate medium speed for current system clock
    Config->Init.BaudRate  = ttc_sysclock_get_configuration()->LowLevelConfig.frequency_apb2 / 16;

    t_ttc_spi_architecture* ConfigArch = Config->LowLevelConfig;

    if ( ConfigArch == NULL ) {
        ConfigArch = ( t_ttc_spi_architecture* ) ttc_heap_alloc_zeroed( sizeof( t_ttc_spi_architecture ) );
        Config->LowLevelConfig = ConfigArch;
    }

    Config->Flags.Initialized = 1;

    return ( e_ttc_spi_errorcode ) 0;
}
void                spi_stm32f1xx_prepare() {

}
void                spi_stm32f1xx_read_byte( t_ttc_spi_config* Config, t_u8* Byte ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_SPI_Writable( Byte, ttc_assert_origin_auto );  // pointers must not be NULL

    volatile t_register_stm32f1xx_spi_sr* SR = &( Config->LowLevelConfig->BaseRegister->SR );

    if ( SR->Bits.RXNE ) {   // old data in receive buffer: read it
        *Byte = ( t_u8 ) Config->LowLevelConfig->BaseRegister->DR.Data;
    }
    else {                   // sendout single word + read return
        Config->LowLevelConfig->BaseRegister->DR.Data = Config->Init.SendAsZero;
        while ( !SR->Bits.TXE );   // wait until transmit buffer is empty
        while ( SR->Bits.BSY );    // wait until transfer has completed
        if ( SR->Bits.RXNE ) {     // data received
            *Byte = ( t_u8 ) Config->LowLevelConfig->BaseRegister->DR.Data;  // read in byte
        }
    }
}
void                spi_stm32f1xx_read_word( t_ttc_spi_config* Config, t_u16* Word ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_SPI_Writable( Word, ttc_assert_origin_auto );    // pointers must not be NULL

    volatile t_register_stm32f1xx_spi_sr* SR = &( Config->LowLevelConfig->BaseRegister->SR );
    if ( SR->Bits.RXNE ) {   // old data in receive buffer: read it
        *Word = Config->LowLevelConfig->BaseRegister->DR.Data;
    }
    else {                   // sendout single word + read return
        Config->LowLevelConfig->BaseRegister->DR.Data = Config->Init.SendAsZero;
        while ( !SR->Bits.TXE );   // wait until transmit buffer is empty
        while ( SR->Bits.BSY );    // wait until transfer has completed
        if ( SR->Bits.RXNE ) {     // data received
            *Word = Config->LowLevelConfig->BaseRegister->DR.Data;    // read in word
        }
    }
}
e_ttc_spi_errorcode spi_stm32f1xx_reset( t_ttc_spi_config* Config ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    spi_stm32f1xx_deinit( Config );
    spi_stm32f1xx_init( Config );

    return ( e_ttc_spi_errorcode ) 0;
}
e_ttc_spi_errorcode spi_stm32f1xx_send_raw( t_ttc_spi_config* Config, const t_u8* Buffer, t_u16 Amount ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_SPI_Readable( Buffer, ttc_assert_origin_auto );  // pointers must not be NULL

    volatile t_register_stm32f1xx_spi_sr* SR = &( Config->LowLevelConfig->BaseRegister->SR );

    if ( Config->Init.Flags.WordSize16 ) { // 16 bit transfers
        volatile t_u16* DR16 = ( volatile t_u16* ) & ( Config->LowLevelConfig->BaseRegister->DR );
        t_u16* Buffer16 = ( t_u16* ) Buffer;
        while ( Amount > 1 ) {
            while ( !SR->Bits.TXE );
            *DR16 = *Buffer16++;
            Amount -= 2;
        }
        if ( Amount ) { // single byte remaining: send it as 16 bit word
            *DR16 = ( *Buffer16 ) & 0xff;
        }
    }
    else {                                 // 8 bit transfers
        volatile t_u8* DR8 = ( volatile t_u8* ) & ( Config->LowLevelConfig->BaseRegister->DR );
        while ( Amount-- > 0 ) {
            while ( !SR->Bits.TXE );
            *DR8 = *Buffer++;
        }
    }

    // wait until transmission has completed (required to avoid releasing chip select too early!)
    while ( !SR->Bits.TXE );
    while ( SR->Bits.BSY );

    return ( e_ttc_spi_errorcode ) 0;
}
void                spi_stm32f1xx_send_word( t_ttc_spi_config* Config, const t_u16 Word ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    volatile t_register_stm32f1xx_spi_sr* SR = &( Config->LowLevelConfig->BaseRegister->SR );
    while ( !SR->Bits.TXE );
    Config->LowLevelConfig->BaseRegister->DR.Data = Word;

    // wait until transmission has completed (required to avoid releasing chip select too early!)
    while ( !SR->Bits.TXE );
    while ( SR->Bits.BSY );
}
t_u16               spi_stm32f1xx_read_raw( t_ttc_spi_config* Config, t_u8* BufferRx, t_u16 AmountRx ) {
    Assert_SPI_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_SPI_EXTRA_Writable( BufferRx, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    t_u16 AmountRead = 0;
    t_u16 SendAsZero = Config->Init.SendAsZero;
    volatile t_register_stm32f1xx_spi_sr* SR   = &( Config->LowLevelConfig->BaseRegister->SR );
    if ( Config->Init.Flags.WordSize16 ) { // 16 bit transfers
        Assert_SPI( ( ( ( t_base ) BufferRx ) & 1 ) == 0, ttc_assert_origin_auto ); // pointer address must by a multiple of 2. Add this to your array declaration: __attribute__( ( aligned( 2 ) ) )
        Assert_SPI( ( AmountRx & 1 ) == 0, ttc_assert_origin_auto ); // amount must be a multiple of 2 for 16 bit transfers!

        volatile t_u16* DR16 = ( volatile t_u16* ) & ( Config->LowLevelConfig->BaseRegister->DR );
        t_u16* BufferRx16 = ( t_u16* ) BufferRx;
        if ( SR->Bits.RXNE ) {   // old data in receive buffer: ignore it
            *BufferRx16 = *DR16;  // read in word and ignore it
        }

        if ( Config->TimeOutRxNe_us ) { // receive with timeouts (required for slow spi baudrate)
            t_ttc_systick_delay TimeOut;

            while ( AmountRx > 1 ) {
                while ( !SR->Bits.TXE ); // wait for next byte
                *DR16 = SendAsZero;      // send out zero word

                ttc_systick_delay_init( &TimeOut, Config->TimeOutRxNe_us );
                while ( ( !SR->Bits.RXNE ) && !ttc_systick_delay_expired( &TimeOut ) ); // wait until byte received or timeout
                if ( SR->Bits.RXNE ) {   // data received
                    *BufferRx16++ = *DR16;   // read in word
                    AmountRead += 2;
                }
                AmountRx -= 2;  // decreasing amount of bytes to read to avoid waiting for dead slave endlessly
            }
        }
        else {                          // receive without timeouts (faster but may loose bytes when slave is too slow)
            while ( AmountRx > 1 ) {
                while ( !SR->Bits.TXE ); // wait for next byte
                *DR16 = SendAsZero;      // send out zero word
                if ( SR->Bits.RXNE ) {   // data received
                    *BufferRx16++ = *DR16;   // read in word
                    AmountRead += 2;
                }
                AmountRx -= 2;  // decreasing amount of bytes to read to avoid waiting for dead slave endlessly
            }
        }
    }
    else {                                 // 8 bit transfers
        volatile t_u8* DR8 = ( volatile t_u8* ) & ( Config->LowLevelConfig->BaseRegister->DR );

        if ( SR->Bits.RXNE ) {             // old data in receive buffer: ignore it
            *BufferRx = *DR8;              // read in byte and ignore it
        }

        if ( Config->TimeOutRxNe_us ) {    // receive with timeouts (required for slow spi baudrate)
            t_ttc_systick_delay TimeOut;

            while ( AmountRx-- ) {         // decreasing amount of bytes to read to avoid waiting for dead slave endlessly
                while ( !SR->Bits.TXE ); // wait for next byte
                *DR8 = SendAsZero;       // send out zero byte

                if ( SR->Bits.RXNE ) {   // data received: read it
                    *BufferRx++ = *DR8;  // read in byte
                    AmountRead++;
                }
                else {
                    ttc_systick_delay_init( &TimeOut, Config->TimeOutRxNe_us );
                    while ( ( !SR->Bits.RXNE ) && !ttc_systick_delay_expired( &TimeOut ) ); // wait until byte received or timeout
                    if ( SR->Bits.RXNE ) {   // data received
                        *BufferRx++ = *DR8;  // read in byte
                        AmountRead++;
                    }
                }
            }
            while ( !SR->Bits.TXE );       // wait until transmit buffer is empty
            while ( SR->Bits.BSY );        // wait until transfer has completed
        }
        else {                             // receive without timeouts (faster but may loose bytes when slave is too slow)
            while ( AmountRx-- ) {         // decreasing amount of bytes to read to avoid waiting for dead slave endlessly
                while ( !SR->Bits.TXE );   // wait for next byte
                *DR8 = SendAsZero;         // send out zero byte
                while ( !SR->Bits.TXE );   // wait until transmit buffer is empty
                while ( SR->Bits.BSY );    // wait until transfer has completed
                if ( SR->Bits.RXNE ) {     // data received
                    *BufferRx++ = *DR8;    // read in byte
                    AmountRead++;
                }
            }
        }
    }
    return AmountRead;
}
e_ttc_spi_errorcode spi_stm32f1xx_send_read( t_ttc_spi_config* Config, const t_u8* BufferTx, t_u16 AmountTx, t_u16 AmountZerosTx, t_u8* BufferRx, t_u16 AmountRx ) {
    Assert_SPI_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    if ( AmountTx ) {
        Assert_SPI_EXTRA_Readable( BufferTx, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    }
    if ( AmountRx ) {
        Assert_SPI_EXTRA_Writable( BufferRx, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    }

    volatile t_register_stm32f1xx_spi_sr* SR   = &( Config->LowLevelConfig->BaseRegister->SR );

    // Note: Not implementing timeouts as these make transfers much slower and typically do not occur with this implementation.
    t_u16  SendAsZero = Config->Init.SendAsZero;

    if ( Config->Init.Flags.WordSize16 ) { // 16 bit transfers
        Assert_SPI( ( ( ( t_base ) BufferRx ) & 1 ) == 0, ttc_assert_origin_auto ); // pointer address must by a multiple of 2. Add this to your array declaration: __attribute__( ( aligned( 2 ) ) )
        Assert_SPI( ( ( ( t_base ) BufferTx ) & 1 ) == 0, ttc_assert_origin_auto ); // pointer address must by a multiple of 2. Add this to your array declaration: __attribute__( ( aligned( 2 ) ) )
        Assert_SPI( ( AmountRx & 1 ) == 0, ttc_assert_origin_auto ); // value must be a multiple of 2 for 16 bit transfers!
        Assert_SPI( ( AmountTx & 1 ) == 0, ttc_assert_origin_auto ); // value must be a multiple of 2 for 16 bit transfers!
        Assert_SPI( ( AmountZerosTx & 1 ) == 0, ttc_assert_origin_auto ); // value must be a multiple of 2 for 16 bit transfers!

        volatile t_u16* DR16 = ( volatile t_u16* ) & ( Config->LowLevelConfig->BaseRegister->DR );
        t_u16* BufferTx16 = ( t_u16* ) BufferTx;
        t_u16* BufferRx16 = ( t_u16* ) BufferRx;

        while ( ( AmountTx > 1 ) && AmountRx ) { // send out bytes + receive bytes
            while ( !SR->Bits.TXE );           // wait for last byte to be transfered
            *DR16 = *BufferTx16++;
            AmountTx -= 2;

            // wait until transmission has completed
            while ( ! SR->Bits.RXNE );
            *BufferRx16++ = *DR16;
            AmountRx -= 2;
        }
        while ( AmountTx > 1 ) {              // still bytes to send but not to receive
            while ( !SR->Bits.TXE );          // wait for last byte to be transfered
            *DR16 = *BufferTx16++;
            AmountTx -= 2;
        }
        while ( AmountZerosTx && AmountRx ) { // continue sending zeroes + receive bytes
            while ( !SR->Bits.TXE );          // wait for last byte to be transfered
            *DR16 = SendAsZero;
            AmountZerosTx -= 2;

            // wait until transmission has completed
            while ( ! SR->Bits.RXNE );
            *BufferRx++ = *DR16;
            AmountRx -= 2;
        }
        while ( AmountZerosTx ) {             // still zeroes to be send
            while ( !SR->Bits.TXE );          // wait for last byte to be transfered
            *DR16 = SendAsZero;
            AmountZerosTx -= 2;
        }
    }
    else {                                    // 8 bit transfers
        volatile t_u8* DR8 = ( volatile t_u8* ) & ( Config->LowLevelConfig->BaseRegister->DR );

        while ( AmountTx && AmountRx ) {      // send out bytes + receive bytes
            while ( !SR->Bits.TXE );  // wait for last byte to be transfered
            *DR8 = *BufferTx++;
            AmountTx--;

            // wait until transmission has completed
            while ( ! SR->Bits.RXNE );
            *BufferRx++ = *DR8;
            AmountRx--;
        }
        while ( AmountTx ) {                  // still bytes to send but not to receive
            while ( !SR->Bits.TXE );  // wait for last byte to be transfered
            *DR8 = *BufferTx++;
            AmountTx--;
        }
        while ( AmountZerosTx && AmountRx ) { // continue sending zeroes + receive bytes
            while ( !SR->Bits.TXE );  // wait for last byte to be transfered
            *DR8 = SendAsZero;
            AmountZerosTx--;

            // wait until transmission has completed
            while ( ! SR->Bits.RXNE );
            *BufferRx++ = *DR8;
            AmountRx--;
        }
        while ( AmountZerosTx ) {             // still zeroes to be send
            while ( !SR->Bits.TXE );  // wait for last byte to be transfered
            *DR8 = SendAsZero;
            AmountZerosTx--;
        }
    }

    // wait until transmission has completed (required to avoid releasing chip select too early!)
    while ( !SR->Bits.TXE );
    while ( SR->Bits.BSY );

    return ( e_ttc_spi_errorcode ) 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
