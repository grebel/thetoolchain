/** { spi_stm32l0xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for spi devices on stm32l0xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150325 13:44:05 UTC
 *
 *  Note: See ttc_spi.h for description of stm32l0xx independent SPI implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "spi_stm32l0xx.h".
//
#include "spi_stm32l0xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

e_ttc_spi_errorcode  spi_stm32l0xx_get_features(t_ttc_spi_config* Config) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL

    return (e_ttc_spi_errorcode) 0;
}
void spi_stm32l0xx_deinit(t_ttc_spi_config* Config) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL

    if (Config->LogicalIndex == 1)
    {
       __SPI1_CLK_ENABLE();
       __SPI1_CLK_DISABLE();
    }
    else
    {
        __SPI2_CLK_ENABLE();
        __SPI2_CLK_DISABLE();
    }
}
e_ttc_spi_errorcode spi_stm32l0xx_init(t_ttc_spi_config* Config) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL

    spi_init_structure spi_init;

    /*! SPI Periph && GPIO clock enable  && Alternate function enable*/
    if(Config->LogicalIndex == 1){
        __SPI1_CLK_ENABLE();
        __GPIOA_CLK_ENABLE();
        ttc_gpio_alternate_function(E_ttc_gpio_pin_a5, GPIO_AF0_SPI1);
        ttc_gpio_alternate_function(E_ttc_gpio_pin_a6, GPIO_AF0_SPI1);
        ttc_gpio_alternate_function(E_ttc_gpio_pin_a7, GPIO_AF0_SPI1);
    }
    else if(Config->LogicalIndex == 2){
        __SPI2_CLK_ENABLE();
        __GPIOB_CLK_ENABLE();
        ttc_gpio_alternate_function(E_ttc_gpio_pin_b13, GPIO_AF0_SPI2);
        ttc_gpio_alternate_function(E_ttc_gpio_pin_b14, GPIO_AF0_SPI2);
        ttc_gpio_alternate_function(E_ttc_gpio_pin_b15, GPIO_AF0_SPI2);
    }
    else{
        Assert_SPI(0,ttc_assert_origin_auto);
    }


    /*! DeInit CMD SPI */
    _spi_cmd(Config,DISABLE);

    /*! Configure Pins GPIO */
    if(Config->LogicalIndex == 1){
        ttc_gpio_init(E_ttc_gpio_pin_a5, E_ttc_gpio_mode_alternate_function_push_pull_pull_down_resistor, E_ttc_gpio_speed_40mhz);
        ttc_gpio_init(E_ttc_gpio_pin_a6, E_ttc_gpio_mode_alternate_function_push_pull_pull_down_resistor, E_ttc_gpio_speed_40mhz);
        ttc_gpio_init(E_ttc_gpio_pin_a7, E_ttc_gpio_mode_alternate_function_push_pull_pull_down_resistor, E_ttc_gpio_speed_40mhz);
        ttc_gpio_init(E_ttc_gpio_pin_a4, E_ttc_gpio_mode_output_push_pull_pull_up_resistor, E_ttc_gpio_speed_40mhz);
    }
    else if(Config->LogicalIndex == 2){
        ttc_gpio_init(E_ttc_gpio_pin_b13, E_ttc_gpio_mode_alternate_function_push_pull_pull_down_resistor, E_ttc_gpio_speed_40mhz);
        ttc_gpio_init(E_ttc_gpio_pin_b14, E_ttc_gpio_mode_alternate_function_push_pull_pull_down_resistor, E_ttc_gpio_speed_40mhz);
        ttc_gpio_init(E_ttc_gpio_pin_b15, E_ttc_gpio_mode_alternate_function_push_pull_pull_down_resistor, E_ttc_gpio_speed_40mhz);
        ttc_gpio_init(E_ttc_gpio_pin_b12, E_ttc_gpio_mode_output_push_pull_pull_up_resistor, E_ttc_gpio_speed_40mhz);
    }
    else{
        Assert_SPI(0,ttc_assert_origin_auto);
    }

    /*! Init SPI */
    spi_init.spi_Direction = SPI_DIRECTION_2LINES;
    if(Config->Init.Flags.Bits.Master)
        spi_init.spi_Mode = SPI_MODE_MASTER;
    else
        spi_init.spi_Mode = SPI_MODE_SLAVE;
    spi_init.spi_DataSize = SPI_DATASIZE_16BIT;
    spi_init.spi_CPOL = SPI_POLARITY_LOW;
    spi_init.spi_CPHA = SPI_PHASE_1EDGE;
    spi_init.spi_NSS = SPI_NSS_SOFT;
    spi_init.spi_BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
    spi_init.spi_FirstBit = SPI_FIRSTBIT_MSB;
    spi_init.spi_CRCPolynomial = 7;

    _spi_init(Config, &spi_init);

    _spi_cmd(Config, ENABLE);

    return (e_ttc_spi_errorcode) 0;
}
e_ttc_spi_errorcode spi_stm32l0xx_load_defaults(t_ttc_spi_config* Config) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL

    Config->Init.Flags.All                        = 0;
    Config->Init.Flags.Bits.Master                = 1;
    Config->Init.Flags.Bits.MultiMaster           = 0;
    Config->Init.Flags.Bits.ClockIdleHigh         = 0;
    Config->Init.Flags.Bits.ClockPhase2ndEdge     = 0;
    Config->Init.Flags.Bits.Simplex               = 0;
    Config->Init.Flags.Bits.Bidirectional         = 1;
    Config->Init.Flags.Bits.Receive               = 1;
    Config->Init.Flags.Bits.Transmit              = 1;
    Config->Init.Flags.Bits.WordSize16            = 0;
    Config->Init.Flags.Bits.HardwareNSS           = 0;
    Config->Init.Flags.Bits.CRC8                  = 1;
    Config->Init.Flags.Bits.CRC16                 = 0;
    Config->Init.Flags.Bits.FirstBitMSB           = 1;
    Config->Init.Flags.Bits.TxDMA                 = 0; // ToDo
    Config->Init.Flags.Bits.RxDMA                 = 0; // ToDo
    Config->Init.Flags.Bits.Irq_Error             = 0; // ToDo
    Config->Init.Flags.Bits.Irq_Received          = 0; // ToDo
    Config->Init.Flags.Bits.Irq_Transmitted       = 0; // ToDo

    Config->Init.CRC_Polynom = 7;
    Config->Layout      = 0;

    t_ttc_spi_architecture* ConfigArch = Config->LowLevelConfig;

    if (ConfigArch == NULL) {
        ConfigArch = (t_ttc_spi_architecture*)ttc_heap_alloc_zeroed( sizeof(t_ttc_spi_architecture) );
        Config->LowLevelConfig = ConfigArch;
    }

    Config->Init.Flags.Bits.Initialized = 1;
    
    return (e_ttc_spi_errorcode) 0;
}
void spi_stm32l0xx_prepare() {


#warning Function spi_stm32l0xx_prepare() is empty.
    

}
void spi_stm32l0xx_read_byte(t_ttc_spi_config* Config, t_u8* Byte) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL
    Assert_SPI(Byte, ttc_assert_origin_auto); // pointers must not be NULL

//    while(Config->LowLevelConfig->BaseRegister->SR.Bits.RXNE == 0){}
//    *Byte = (t_u8) 0xff & Config->LowLevelConfig->BaseRegister->DR.All;

    while(SPI1->SR && SPI_FLAG_RXNE == RESET){}
    *Byte = (t_u8) 0xff & SPI1->DR;
}
void spi_stm32l0xx_read_word(t_ttc_spi_config* Config, t_u16* Word) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL
    Assert_SPI(Word, ttc_assert_origin_auto); // pointers must not be NULL

//    while(Config->LowLevelConfig->BaseRegister->SR.Bits.RXNE == 0){}
//    *Word = Config->LowLevelConfig->BaseRegister->DR.All;

    while(SPI1->SR && SPI_FLAG_RXNE == RESET){}
    *Word= (t_u16) 0xff & SPI1->DR;
}
e_ttc_spi_errorcode spi_stm32l0xx_reset(t_ttc_spi_config* Config) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL

    spi_stm32l0xx_deinit(Config);
    spi_stm32l0xx_init(Config);
    
    return (e_ttc_spi_errorcode) 0;
}
e_ttc_spi_errorcode spi_stm32l0xx_send_raw(t_ttc_spi_config* Config, const t_u8* Buffer, t_u16 Amount) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL
    Assert_SPI(Buffer, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function spi_stm32l0xx_send_raw() is empty.
    
    return (e_ttc_spi_errorcode) 0;
}
e_ttc_spi_errorcode spi_stm32l0xx_send_string(t_ttc_spi_config* Config, const char* Buffer, t_u16 MaxLength) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL
    Assert_SPI(Buffer, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function spi_stm32l0xx_send_string() is empty.
    
    return (e_ttc_spi_errorcode) 0;
}
void spi_stm32l0xx_send_word(t_ttc_spi_config* Config, const t_u16 Word) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL

    //while(Config->LowLevelConfig->BaseRegister->SR.Bits.TXE == 0){}  // Wait until TX buffer is empty
    while(SPI1->SR && SPI_FLAG_RXNE == RESET){}

    _spi_i2s_send_data(Config, Word);
    ttc_task_msleep(10000);

//    while(Config->LowLevelConfig->BaseRegister->SR.Bits.TXE == 0){}  // Wait until transmit complete
//    while(Config->LowLevelConfig->BaseRegister->SR.Bits.BSY == 1){}  // wait until SPI is not busy anymore

     while(SPI1->SR && SPI_FLAG_RXNE == RESET){}
     while(SPI1->SR && SPI_FLAG_BSY  == SET){}

}
e_ttc_spi_errorcode spi_stm32l0xx_read_raw(t_ttc_spi_config* Config, ct_u8* BufferRx, t_u16 AmountRx) {
    Assert_SPI_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_SPI_EXTRA(ttc_memory_is_writable(BufferRx), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function spi_stm32l0xx_read_raw() is empty.
    
    return (e_ttc_spi_errorcode) 0;
}
e_ttc_spi_errorcode spi_stm32l0xx_send_read(t_ttc_spi_config* Config, const t_u8* BufferTx, t_u16 AmountTx, t_u16 AmountZerosTx, t_u8* BufferRx, t_u16 AmountRx) {
    Assert_SPI_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_SPI_EXTRA(ttc_memory_is_writable(BufferTx), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_SPI_EXTRA(ttc_memory_is_writable(BufferRx), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function spi_stm32l0xx_send_read() is empty.
    
    return (e_ttc_spi_errorcode) 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************


void _spi_cmd(t_ttc_spi_config* Config, FunctionalState NewState){

    /* Check the parameters */
    assert_param(IS_SPI_ALL_INSTANCE(Config->LowLevelConfig->BaseRegister));
    assert_param(IS_FUNCTIONAL_STATE(NewState));

    t_register_stm32l0xx_spi_cr1 CR1;

    if (NewState != DISABLE)
    {
        /* Enable the selected SPI peripheral */
        //CR1 = Config->LowLevelConfig->BaseRegister->CR1;
        CR1.All = SPI1->CR1;
        CR1.All |= (t_u16)0x0040;
        //Config->LowLevelConfig->BaseRegister->CR1 = CR1;
        SPI1->CR1 = CR1.All;
    }
    else
    {
        /* Disable the selected SPI peripheral */
        //CR1 = Config->LowLevelConfig->BaseRegister->CR1;
        CR1.All = SPI1->CR1;
        CR1.All &= (t_u16)~((t_u16)0x0040);
        //Config->LowLevelConfig->BaseRegister->CR1 = CR1;
        SPI1->CR1 = CR1.All;
    }
}

void _spi_init(t_ttc_spi_config* Config, spi_init_structure* spi_init){

    /* Check the SPI parameters */
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL
    Assert_SPI(spi_init, ttc_assert_origin_auto); // pointers must not be NULL

    //Configuration CR1
    t_register_stm32l0xx_spi_cr1 CR1;
    CR1.All = 0;
    CR1.All |= ((t_u32)spi_init->spi_Direction | spi_init->spi_Mode |
                                   spi_init->spi_DataSize | spi_init->spi_CPOL |
                                   spi_init->spi_CPHA | spi_init->spi_NSS |
                                   spi_init->spi_BaudRatePrescaler | spi_init->spi_FirstBit);

    //Config->LowLevelConfig->BaseRegister->CR1 = (t_u32)CR1.All;
    SPI1->CR1 = CR1.All;

    //CRCPOLY
    //Config->LowLevelConfig->BaseRegister->Init.CRC_Polynom.Bits.Polynom = spi_init->spi_CRCPolynomial;
    SPI1->CRCPR = spi_init->spi_CRCPolynomial;
}

void _spi_i2s_send_data(t_ttc_spi_config* Config, const t_u16 Word){

    /* Check the parameters */
    assert_param(IS_SPI_ALL_INSTANCE(Config->LowLevelConfig->BaseRegister));

    /* Write in the DR register the data to be sent */
    //Config->LowLevelConfig->BaseRegister->DR.All = Word;
    SPI1->DR = Word;
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions




























