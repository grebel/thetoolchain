#ifndef SPI_STM32L0XX_H
#define SPI_STM32L0XX_H

/** { spi_stm32l0xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for spi devices on stm32l0xx architectures.
 *  Structures, Enums and Defines being required by low-level driver only.
 *
 *  Created from template device_architecture.h revision 22 at 20150325 13:44:05 UTC
 *
 *  Note: See ttc_spi.h for description of stm32l0xx independent SPI implementation.
 *
 *  Authors: Gregor Rebel
 *
 *
 *  How to configure stm32l0xx spi devices
 *
 *  This low-level driver requires configuration defines in addtion to the howto section
 *  of ttc_spi.h.
 *
 *  PLACE YOUR DESCRIPTION HERE!
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_SPI_STM32L0XX
//
// Implementation status of low-level driver support for spi devices on stm32l0xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_SPI_STM32L0XX '?'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_SPI_STM32L0XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_SPI_STM32L0XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_SPI_STM32L0XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "spi_stm32l0xx.c"
//
#include "spi_stm32l0xx_types.h"
#include "../ttc_spi_types.h"
#include "../ttc_register.h"
#include "../ttc_gpio.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_spi_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_spi_foo
//
#define ttc_driver_spi_deinit(Config) spi_stm32l0xx_deinit(Config)
#define ttc_driver_spi_get_features(Config) spi_stm32l0xx_get_features(Config)
#define ttc_driver_spi_init(Config) spi_stm32l0xx_init(Config)
#define ttc_driver_spi_load_defaults(Config) spi_stm32l0xx_load_defaults(Config)
#define ttc_driver_spi_prepare() spi_stm32l0xx_prepare()
#define ttc_driver_spi_read_byte(Config, Byte) spi_stm32l0xx_read_byte(Config, Byte)
#define ttc_driver_spi_read_word(Config, Word) spi_stm32l0xx_read_word(Config, Word)
#define ttc_driver_spi_reset(Config) spi_stm32l0xx_reset(Config)
#define ttc_driver_spi_send_raw(Config, Buffer, Amount) spi_stm32l0xx_send_raw(Config, Buffer, Amount)
#define ttc_driver_spi_send_string(Config, Buffer, MaxLength) spi_stm32l0xx_send_string(Config, Buffer, MaxLength)
#define ttc_driver_spi_send_word(Config, Word) spi_stm32l0xx_send_word(Config, Word)
#define ttc_driver_spi_read_raw(Config, BufferRx, AmountRx) spi_stm32l0xx_read_raw(Config, BufferRx, AmountRx)
#define ttc_driver_spi_send_read(Config, BufferTx, AmountTx, AmountZerosTx, BufferRx, AmountRx) spi_stm32l0xx_send_read(Config, BufferTx, AmountTx, AmountZerosTx, BufferRx, AmountRx)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_spi.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_spi.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** shutdown single SPI unit device
 * @param Config        pointer to struct t_ttc_spi_config (must have valid value for PhysicalIndex)
 * @return              == 0: SPI has been shutdown successfully; != 0: error-code
 */
void spi_stm32l0xx_deinit( t_ttc_spi_config* Config );


/** fills out given Config with maximum valid values for indexed SPI
 * @param Config        = pointer to struct t_ttc_spi_config (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_spi_errorcode spi_stm32l0xx_get_features( t_ttc_spi_config* Config );


/** initializes single SPI unit for operation
 * @param Config        pointer to struct t_ttc_spi_config (must have valid value for PhysicalIndex)
 * @return              == 0: SPI has been initialized successfully; != 0: error-code
 */
e_ttc_spi_errorcode spi_stm32l0xx_init( t_ttc_spi_config* Config );


/** loads configuration of indexed SPI unit with default values
 *
 * Note: Pin configuration has already been set from TTC_SPIx_* constants but may be reset/ checked by low-level driver
 *
 * @param Config        pointer to struct t_ttc_spi_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_spi_errorcode spi_stm32l0xx_load_defaults( t_ttc_spi_config* Config );


/** Prepares spi Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void spi_stm32l0xx_prepare();


/** Reads single data byte from input buffer.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param Config        pointer to struct t_ttc_spi_config (must have valid value for PhysicalIndex)
 * @param Byte          pointer to 8 bit buffer where to store Byte
 * @return              == 0: Word has been read successfully; != 0: error-code
 */
void spi_stm32l0xx_read_byte( t_ttc_spi_config* Config, t_u8* Byte );


/** Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param Config        pointer to struct t_ttc_spi_config (must have valid value for PhysicalIndex)
 * @param Word          pointer to 16 bit buffer where to store Word
 * @return              == 0: Word has been read successfully; != 0: error-code
 */
void spi_stm32l0xx_read_word( t_ttc_spi_config* Config, t_u16* Word );


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_spi_config (must have valid value for PhysicalIndex)
 * @return   =
 */
e_ttc_spi_errorcode spi_stm32l0xx_reset( t_ttc_spi_config* Config );


/** Send out given amount of raw bytes from buffer.
 * Will not interpret bytes in buffer.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param Config        pointer to struct t_ttc_spi_config (must have valid value for PhysicalIndex)
 * @param Buffer        memory location from which to read data
 * @param Amount        amount of bytes to send from Buffer[]
 * @return              == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_spi_errorcode spi_stm32l0xx_send_raw( t_ttc_spi_config* Config, const t_u8* Buffer, t_u16 Amount );


/** Send out given bytes from buffer until first zero byte or MaxLength reached.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param Config        pointer to struct t_ttc_spi_config (must have valid value for PhysicalIndex)
 * @param Buffer        memory location from which to read data
 * @param MaxLength     no more than this amount of bytes will be send
 * @return              == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_spi_errorcode spi_stm32l0xx_send_string( t_ttc_spi_config* Config, const char* Buffer, t_u16 MaxLength );


/** Send out given data word (8 or 9 bits)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param Config        pointer to struct t_ttc_spi_config (must have valid value for PhysicalIndex)
 * @param Word          16 bits value to send
 * @return              == 0: Buffer has been sent successfully; != 0: error-code
 */
void spi_stm32l0xx_send_word( t_ttc_spi_config* Config, const t_u16 Word );


/** Reads given amount of bytes from slave
 *
 * Will automatically send out one zero byte for every byte to read from slave.
 * Will use 16 bit wide transfers if configured and available in current low-level driver.
 *
 * This function will pull corresponding slave select pin low for complete operation if slave index is given.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param IndexSPI      device index of SPI to init (1..ttc_spi_get_max_index() )
 * @param IndexSlave    >0: index of slave select pin to use (1=TTC_SPI<n>_NSS1, 2=TTC_SPI<n>_NSS2, ...); ==0: no handling of slave select pin
 * @param Config        pointer to struct t_ttc_spi_config (must have valid value for PhysicalIndex)
 * @param BufferRx      memory location where to store received data
 * @param AmountRx      amount of bytes to read from slave
 * @return              == 0: All bytes have been read successfully; != 0: error-code
 */
e_ttc_spi_errorcode spi_stm32l0xx_read_raw( t_ttc_spi_config* Config, ct_u8* BufferRx, t_u16 AmountRx );


/** Combined data sending and receiving.
 *
 * An SPI master always receives one byte of data when it sends one byte to a slave.
 * This combined send+read function allows to send data from given buffer to slave and to store received data
 * in another buffer. BufferTx[] and BufferRx[] must not be of same size.
 *
 * This function will pull corresponding slave select pin low for complete operation if slave index is given.
 *
 * ttc_spi_master_send_read() operates in three phases.
 *   BufferTx)  Send data from BufferTx[]
 *   ZeroBytes) Send zero bytes
 *   BufferRx)  Store received bytes in BufferRx[]
 *
 *   Phase ZeroBytes must not exist. If it exists, it runs after phase BufferTx.
 *   Phase BufferRx runs in parallel to phase BufferTx and phase ZeroBytes and ends with phase ZeroBytes.
 *   <---- BufferTx -------><----- ZeroBytes ----->
 *             <--------------- BufferRx --------->
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param IndexSPI       device index of SPI to init (1..ttc_spi_get_max_index() )
 * @param IndexSlave     >0: index of slave select pin to use (1=TTC_SPI<n>_NSS1, 2=TTC_SPI<n>_NSS2, ...); ==0: no handling of slave select pin
 * @param Config         pointer to struct t_ttc_spi_config (must have valid value for PhysicalIndex)
 * @param BufferTx       memory location from which to read data
 * @param AmountTx       amount of bytes to send from BufferTx[]
 * @param AmountZerosTx  amount of zero bytes to send after sending BufferTx[]
 * @param BufferRx       memory location where to store received data
 * @param AmountRx       amount of bytes to store in BufferRx[]
 * @param Amount         amount of bytes to send from Buffer[]
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_spi_errorcode spi_stm32l0xx_send_read( t_ttc_spi_config* Config, const t_u8* BufferTx, t_u16 AmountTx, t_u16 AmountZerosTx, t_u8* BufferRx, t_u16 AmountRx );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _spi_stm32l0xx_foo(t_ttc_spi_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //SPI_STM32L0XX_H