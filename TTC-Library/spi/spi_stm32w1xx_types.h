#ifndef SPI_STM32W1XX_TYPES_H
#define SPI_STM32W1XX_TYPES_H

/** { spi_stm32w1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for SPI devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by ttc_spi_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140502 23:30:30 UTC
 *
 *  Note: See ttc_spi.h for description of architecture independent SPI implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic.h"
#include "../register/register_stm32w1xx_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_spi_types.h *************************


typedef struct {  // stm32w1xx specific configuration data of single SPI device
    volatile t_u32* DataRegister;     // address of SPI data input/output register
    volatile t_u32* StatusRegister;   // address of SPI status register
    volatile t_u32* Status_SPIRXVAL;  // bit banding address of status bit SPIRXVAL   (set when the receive FIFO contains at least one byte)
    volatile t_u32* Status_SPITXFREE; // bit banding address of status bit SPITXFREE  (set when the transmit FIFO has space to accept at least one byte)
} __attribute__( ( __packed__ ) ) t_spi_stm32w1xx_config;

// t_ttc_spi_architecture is required by ttc_spi_types.h
#define t_ttc_spi_architecture t_spi_stm32w1xx_config

#define TTC_INTERRUPT_SPI_AMOUNT 2

//} Structures/ Enums


#endif //SPI_STM32W1XX_TYPES_H
