#ifndef SPI_STM32L1XX_H
#define SPI_STM32L1XX_H

/** { spi_stm32l1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for spi devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by high-level spi and application.
 *
 *  Created from template device_architecture.h revision 22 at 20140423 11:04:41 UTC
 *
 *  Note: See ttc_spi.h for description of stm32l1xx independent SPI implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_SPI_STM32L1XX
//
// Implementation status of low-level driver support for spi devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_SPI_STM32L1XX 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_SPI_STM32L1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_SPI_STM32L1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_SPI_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "spi_stm32l1xx_types.h"
#include "../ttc_spi_types.h"
#include "../ttc_register.h"
#include "../ttc_gpio.h"

#include "stm32l1xx_gpio.h"
#include "stm32l1xx_spi.h"
#include "stm32l1xx.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

#define spi_Direction_2Lines_FullDuplex ((ut_int16)0x0000)
#define spi_Direction_2Lines_RxOnly     ((ut_int16)0x0400)
#define spi_Direction_1Line_Rx          ((ut_int16)0x8000)
#define spi_Direction_1Line_Tx          ((ut_int16)0xC000)

#define spi_Mode_Master                 ((ut_int16)0x0104)
#define spi_Mode_Slave                  ((ut_int16)0x0000)

#define spi_DataSize_16b                ((ut_int16)0x0800)
#define spi_DataSize_8b                 ((ut_int16)0x0000)

#define spi_CPOL_Low                    ((ut_int16)0x0000)
#define spi_CPOL_High                   ((ut_int16)0x0002)

#define spi_CPHA_1Edge                  ((ut_int16)0x0000)
#define spi_CPHA_2Edge                  ((ut_int16)0x0001)

#define spi_NSS_Soft                    ((ut_int16)0x0200)
#define spi_NSS_Hard                    ((ut_int16)0x0000)

#define spi_BaudRatePrescaler_2         ((ut_int16)0x0000)
#define spi_BaudRatePrescaler_4         ((ut_int16)0x0008)
#define spi_BaudRatePrescaler_8         ((ut_int16)0x0010)
#define spi_BaudRatePrescaler_16        ((ut_int16)0x0018)
#define spi_BaudRatePrescaler_32        ((ut_int16)0x0020)
#define spi_BaudRatePrescaler_64        ((ut_int16)0x0028)
#define spi_BaudRatePrescaler_128       ((ut_int16)0x0030)
#define spi_BaudRatePrescaler_256       ((ut_int16)0x0038)

#define spi_FirstBit_MSB                ((ut_int16)0x0000)
#define spi_FirstBit_LSB                ((ut_int16)0x0080)

#define gpio_AF_SPI1                    ((t_u8)0x05)  /*!< SPI1 Alternate Function mapping */
#define gpio_AF_SPI2                    ((t_u8)0x05)  /*!< SPI2 Alternate Function mapping */

#define rcc_APB2Periph_SPI1             ((t_u32)0x00001000)
#define rcc_APB1Periph_SPI2             ((t_u32)0x00004000)

#define rcc_AHBPeriph_GPIOA             ((t_u32)0x00000001)
#define rcc_AHBPeriph_GPIOB             ((t_u32)0x00000002)



// define all supported low-level functions for ttc_spi_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_spi_foo
//
#define ttc_driver_spi_deinit(Config)                       spi_stm32l1xx_deinit(Config)
#define ttc_driver_spi_get_features(Config)                 spi_stm32l1xx_get_features(Config)
#define ttc_driver_spi_init(Config)                         spi_stm32l1xx_init(Config)
#define ttc_driver_spi_load_defaults(Config)                spi_stm32l1xx_load_defaults(Config)
#define ttc_driver_spi_prepare()                            spi_stm32l1xx_prepare()
#define ttc_driver_spi_reset(Config)                        spi_stm32l1xx_reset(Config)
#define ttc_driver_spi_read_byte(Config, Byte)              spi_stm32l1xx_read_byte(Config, Byte)
#define ttc_driver_spi_read_word(Config, Word)              spi_stm32l1xx_read_word(Config, Word)
#define ttc_driver_spi_send_raw(Config, Buffer, Amount)     spi_stm32l1xx_send_raw(Config, Buffer, Amount)
#define ttc_driver_spi_send_word(Config, Word)              spi_stm32l1xx_send_word(Config, Word)
#define ttc_driver_spi_read_raw(Config, BufferRx, AmountRx) spi_stm32l1xx_read_raw(Config, BufferRx, AmountRx)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

// undefine all not-implemented low-level macros
#undef ttc_driver_spi_send_read

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_spi.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_spi.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** shutdown single SPI unit device
 * @param Config        pointer to struct t_ttc_spi_config
 * @return              == 0: SPI has been shutdown successfully; != 0: error-code
 */
void spi_stm32l1xx_deinit( t_ttc_spi_config* Config );


/** fills out given Config with maximum valid values for indexed SPI
 * @param Config        = pointer to struct t_ttc_spi_config
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_spi_errorcode spi_stm32l1xx_get_features( t_ttc_spi_config* Config );


/** initializes single SPI unit for operation
 * @param Config        pointer to struct t_ttc_spi_config
 * @return              == 0: SPI has been initialized successfully; != 0: error-code
 */
e_ttc_spi_errorcode spi_stm32l1xx_init( t_ttc_spi_config* Config );


/** loads configuration of indexed SPI unit with default values
 * @param Config        pointer to struct t_ttc_spi_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_spi_errorcode spi_stm32l1xx_load_defaults( t_ttc_spi_config* Config );


/** Prepares spi Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void spi_stm32l1xx_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_spi_config
 * @return   =
 */
e_ttc_spi_errorcode spi_stm32l1xx_reset( t_ttc_spi_config* Config );


/** Reads single data byte from input buffer.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param Config        pointer to struct t_ttc_spi_config
 * @param Byte          pointer to 8 bit buffer where to store Byte
 */
void spi_stm32l1xx_read_byte( t_ttc_spi_config* Config, t_u8* Byte );


/** Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param Config        pointer to struct t_ttc_spi_config
 * @param Word          pointer to 16 bit buffer where to store Word
 *
 */
void spi_stm32l1xx_read_word( t_ttc_spi_config* Config, t_u16* Word );


/** Send out given amount of raw bytes from buffer.
 * Will not interpret bytes in buffer.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param Config        pointer to struct t_ttc_spi_config
 * @param Buffer        memory location from which to read data
 * @param Amount        amount of bytes to send from Buffer[]
 * @return              == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_spi_errorcode spi_stm32l1xx_send_raw( t_ttc_spi_config* Config, const t_u8* Buffer, t_u16 Amount );


/** Send out given data word (8 or 9 bits)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param Config        pointer to struct t_ttc_spi_config
 * @param Word          8 or 9 bit value to send
 * @return              == 0: Buffer has been sent successfully; != 0: error-code
 */
void spi_stm32l1xx_send_word( t_ttc_spi_config* Config, const t_u16 Word );



/** Reads given amount of bytes from slave
 *
 * Will automatically send out one zero byte for every byte to read from slave.
 * Will use 16 bit wide transfers if configured and available in current low-level driver.
 *
 * This function will pull corresponding slave select pin low for complete operation if slave index is given.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param IndexSPI      device index of SPI to init (1..ttc_spi_get_max_index() )
 * @param IndexSlave    >0: index of slave select pin to use (1=TTC_SPI<n>_NSS1, 2=TTC_SPI<n>_NSS2, ...); ==0: no handling of slave select pin
 * @param Config        pointer to struct t_ttc_spi_config
 * @param BufferRx      memory location where to store received data
 * @param AmountRx      amount of bytes to read from slave
 * @return              == 0: All bytes have been read successfully; != 0: error-code
 */
t_u16 spi_stm32l1xx_read_raw( t_ttc_spi_config* Config, t_u8* BufferRx, t_u16 AmountRx );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //SPI_STM32L1XX_H
