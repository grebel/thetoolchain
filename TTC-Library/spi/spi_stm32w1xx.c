/** { spi_stm32w1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for spi devices on stm32w1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 21 at 20140502 23:30:30 UTC
 *
 *  Note: See ttc_spi.h for description of stm32w1xx independent SPI implementation.
 * 
 *  Authors: Gregor Rebel
}*/

#include "spi_stm32w1xx.h"

//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

void spi_stm32w1xx_deinit(t_ttc_spi_config* Config) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
}
e_ttc_spi_errorcode spi_stm32w1xx_get_features(t_ttc_spi_config* Config) {
    Assert_SPI(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // pointers must not be NULL

    Config->Flags.All                        = 0;
    Config->Flags.Bits.Master                = 1;
    Config->Flags.Bits.Slave                 = 1; // ToDo
    Config->Flags.Bits.MultiMaster           = 1;
    Config->Flags.Bits.ClockIdleHigh         = 1;
    Config->Flags.Bits.ClockPhase2ndEdge     = 1;
    Config->Flags.Bits.Simplex               = 1;
    Config->Flags.Bits.Bidirectional         = 1;
    Config->Flags.Bits.Receive               = 1;
    Config->Flags.Bits.Transmit              = 1;
    Config->Flags.Bits.WordSize16            = 1;
    Config->Flags.Bits.HardwareNSS           = 0;
    Config->Flags.Bits.CRC8                  = 1;
    Config->Flags.Bits.CRC16                 = 1;
    Config->Flags.Bits.FirstBitMSB           = 1;
    Config->Flags.Bits.TxDMA                 = 0; // ToDo
    Config->Flags.Bits.RxDMA                 = 0; // ToDo
    Config->Flags.Bits.Irq_Error             = 0; // ToDo
    Config->Flags.Bits.Irq_Received          = 0; // ToDo
    Config->Flags.Bits.Irq_Transmitted       = 0; // ToDo

    Config->Init.CRC_Polynom = 0xffff;
    Config->Layout      = 0;
    Config->Init.BaudRate         = 6000000;

    return (e_ttc_spi_errorcode) 0;
}
e_ttc_spi_errorcode spi_stm32w1xx_init(t_ttc_spi_config* Config) {
    Assert_SPI(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // pointers must not be NULL

    Assert_SPI(Config->Flags.Bits.Master != Config->Flags.Bits.Slave, ttc_assert_origin_auto); // one of the two bits must be set!
    t_register_stm32w1xx_scx_spicfg   ConfigSPI;
    t_register_stm32w1xx_scx_mode     Mode;
    t_register_stm32w1xx_scx_twictrl2 ConfigTWI;
    t_register_stm32w1xx_scx_rateexp  RateExp;
    t_register_stm32w1xx_scx_ratelin  RateLin;
    if (Config->PhysicalIndex) {     // read registers for SC2
        ConfigSPI = register_stm32w1xx_SC2_SPICFG;
        Mode      = register_stm32w1xx_SC2_MODE;
        ConfigTWI = register_stm32w1xx_SC2_TWICTRL2;
        RateExp   = register_stm32w1xx_SC2_RATEEXP;
        RateLin   = register_stm32w1xx_SC2_RATELIN;
        Config->LowLevelConfig.DataRegister     = (t_u32*) &register_stm32w1xx_SC2_DATA;
        Config->LowLevelConfig.StatusRegister   = (t_u32*) &register_stm32w1xx_SC2_SPISTAT;
        Config->LowLevelConfig.Status_SPIRXVAL  = cm3_calc_peripheral_BitBandAddress(&register_stm32w1xx_SC2_SPISTAT, 1);
        Config->LowLevelConfig.Status_SPITXFREE = cm3_calc_peripheral_BitBandAddress(&register_stm32w1xx_SC2_SPISTAT, 2);
    }
    else {                           // read registers for SC1
        ConfigSPI = register_stm32w1xx_SC1_SPICFG;
        Mode      = register_stm32w1xx_SC1_MODE;
        ConfigTWI = register_stm32w1xx_SC1_TWICTRL2;
        RateExp   = register_stm32w1xx_SC1_RATEEXP;
        RateLin   = register_stm32w1xx_SC1_RATELIN;
        Config->LowLevelConfig.DataRegister   = (t_u32*) &register_stm32w1xx_SC1_DATA;
        Config->LowLevelConfig.StatusRegister = (t_u32*) &register_stm32w1xx_SC1_SPISTAT;
    }
    if (Config->Flags.Bits.Master) { // configuring master mode
        ConfigSPI.bSC_SPIMST = 1;
        ConfigTWI.bSC_TWIACK = 1;
        Mode.bSC_MODE        = 2;
        if (Config->Flags.Bits.FirstBitMSB) ConfigSPI.bSC_SPIORD = 0;
        else                                ConfigSPI.bSC_SPIORD = 1;
        ConfigSPI.bSC_SPIPHA = Config->Flags.Bits.ClockPhase2ndEdge;
        ConfigSPI.bSC_SPIPOL = Config->Flags.Bits.ClockIdleHigh;

        if (Config->Init.BaudRate > 6000000)
            Config->Init.BaudRate = 6000000;

        t_base Min_Difference = -1;
        for (t_u8 Lin = 0; (Lin < 16) && Min_Difference; Lin++) { // find best Lin,Exp combination with smallest difference
            t_u16 Multiplier = 2;
            for (t_u8 Exp = 1; (Exp < 16) && Min_Difference; Exp++) {
                t_base_signed Difference = Config->Init.BaudRate - (12000000 / ( (Lin+1) * Multiplier));
                Difference = abs(Difference);
                if (Min_Difference > Difference) {
                    Min_Difference = Difference;
                    RateLin.bSC_RATELIN = Lin;
                    RateExp.bSC_RATEEXP = Exp;
                }
                Multiplier *= 2; // = 2 pow Exp
            }
        }
        // Calculate actual baudrate (Datasheet STM32W108HB p.79)
        Config->Init.BaudRate = 12000000 / ( (RateLin.bSC_RATELIN + 1) << RateExp.bSC_RATEEXP );

        ttc_gpio_init(Config->Init.Pin_MISO, E_ttc_gpio_mode_input_pull_up,                   E_ttc_gpio_speed_max);
        ttc_gpio_init(Config->Init.Pin_MOSI, E_ttc_gpio_mode_alternate_function_push_pull,    E_ttc_gpio_speed_max);
        ttc_gpio_init(Config->Init.Pin_SCLK, E_ttc_gpio_mode_alternate_function_special_sclk, E_ttc_gpio_speed_max);
        ttc_gpio_set(Config->Pin_NSS); // avoid glitch on slave select
        ttc_gpio_init(Config->Pin_NSS,  E_ttc_gpio_mode_output_push_pull,                E_ttc_gpio_speed_max);
    }
    else {                           // configuring slave mode

    }
    if (Config->PhysicalIndex) {     // write registers for SC2
        register_stm32w1xx_SC2_SPICFG    = ConfigSPI;
        register_stm32w1xx_SC2_MODE      = Mode;
        register_stm32w1xx_SC2_TWICTRL2  = ConfigTWI;
        register_stm32w1xx_SC2_RATEEXP   = RateExp;
        register_stm32w1xx_SC2_RATELIN   = RateLin;
    }
    else {                           // write registers for SC1
        register_stm32w1xx_SC1_SPICFG    = ConfigSPI;
        register_stm32w1xx_SC1_MODE      = Mode;
        register_stm32w1xx_SC1_TWICTRL2  = ConfigTWI;
        register_stm32w1xx_SC1_RATEEXP   = RateExp;
        register_stm32w1xx_SC1_RATELIN   = RateLin;
    }

    return (e_ttc_spi_errorcode) 0;
}
e_ttc_spi_errorcode spi_stm32w1xx_load_defaults(t_ttc_spi_config* Config) {
    Assert_SPI(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // pointers must not be NULL

    Config->Flags.All                        = 0;
    Config->Flags.Bits.Master                = 1;
    Config->Flags.Bits.Slave                 = 0;
    Config->Flags.Bits.MultiMaster           = 0;
    Config->Flags.Bits.ClockIdleHigh         = 0;
    Config->Flags.Bits.ClockPhase2ndEdge     = 0;
    Config->Flags.Bits.Simplex               = 0;
    Config->Flags.Bits.Bidirectional         = 1;
    Config->Flags.Bits.Receive               = 1;
    Config->Flags.Bits.Transmit              = 1;
    Config->Flags.Bits.WordSize16            = 0;
    Config->Flags.Bits.HardwareNSS           = 0;
    Config->Flags.Bits.CRC8                  = 0;
    Config->Flags.Bits.CRC16                 = 0;
    Config->Flags.Bits.TxDMA                 = 0;
    Config->Flags.Bits.RxDMA                 = 0;
    Config->Flags.Bits.FirstBitMSB           = 1;
    Config->Flags.Bits.Irq_Error             = 0;
    Config->Flags.Bits.Irq_Received          = 0;
    Config->Flags.Bits.Irq_Transmitted       = 0;

    Config->Init.CRC_Polynom = 0x7;
    Config->Layout      = 0;

    Config->Init.BaudRate  = 6000000; // medium speed
    Config->Init.TimeOut   = 100;

    if (Config->Flags.Bits.Master) {  // select pins for Master Mode
        switch (Config->PhysicalIndex) { // Datasheet STM32W108HB (Doc ID 16252 Rev 13) p.79
        case 0: { //SC1
            // mostly fixed pin configuration on STM32W1xx
            Config->Init.Pin_MOSI = E_ttc_gpio_pin_b1;
            Config->Init.Pin_MISO = E_ttc_gpio_pin_b2;
            Config->Init.Pin_SCLK = E_ttc_gpio_pin_b3;
            // Config->Pin_NSS  = E_ttc_gpio_pin_b4; // pin can be chosen freely
            break;
        }
        case 1: { // SC2
            // mostly fixed pin configuration on STM32W1xx
            Config->Init.Pin_MOSI = E_ttc_gpio_pin_a0;
            Config->Init.Pin_MISO = E_ttc_gpio_pin_a1;
            Config->Init.Pin_SCLK = E_ttc_gpio_pin_a2;
            // Config->Pin_NSS  = E_ttc_gpio_pin_a3; // pin can be chosen freely
            break;
        }
        default: ttc_assert_halt_origin(ttc_assert_origin_auto); break; // illegal physical index
        }
    }
    else {                            // select pins for Slave Mode
        switch (Config->PhysicalIndex) { // Datasheet STM32W108HB (Doc ID 16252 Rev 13) p.79
        case 0: { // SC1
            // fixed pin configuration on STM32W1xx in Slave Mode
            Config->Init.Pin_MOSI = E_ttc_gpio_pin_b2;
            Config->Init.Pin_MISO = E_ttc_gpio_pin_b1;
            Config->Init.Pin_SCLK = E_ttc_gpio_pin_b3;
            Config->Pin_NSS  = E_ttc_gpio_pin_b4;
            break;
        }
        case 1: { // SC2
            // fixed pin configuration on STM32W1xx in Slave Mode
            Config->Init.Pin_MOSI = E_ttc_gpio_pin_a0;
            Config->Init.Pin_MISO = E_ttc_gpio_pin_a1;
            Config->Init.Pin_SCLK = E_ttc_gpio_pin_a2;
            Config->Pin_NSS  = E_ttc_gpio_pin_a3;
            break;
        }
        default: ttc_assert_halt_origin(ttc_assert_origin_auto); break; // illegal physical index
        }
    }

    return ec_spi_OK;
}
void spi_stm32w1xx_prepare() {


    

}
e_ttc_spi_errorcode spi_stm32w1xx_reset(t_ttc_spi_config* Config) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (e_ttc_spi_errorcode) 0;
}
void spi_stm32w1xx_read_byte(t_ttc_spi_config* Config, t_u8* Byte) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL
    Assert_SPI(Byte, ttc_assert_origin_auto); // pointers must not be NULL

    *Byte = (t_u8) * Config->LowLevelConfig.DataRegister;
}
void spi_stm32w1xx_read_word(t_ttc_spi_config* Config, t_u16* Word) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL
    Assert_SPI(Word, ttc_assert_origin_auto); // pointers must not be NULL

    *Word = (t_u16) * Config->LowLevelConfig.DataRegister;
}
e_ttc_spi_errorcode spi_stm32w1xx_send_raw(t_ttc_spi_config* Config, const t_u8* Buffer, t_u16 Amount) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL
    Assert_SPI(Buffer, ttc_assert_origin_auto); // pointers must not be NULL

    //ToDo: implement fast send function (default implementation in interface

    return (e_ttc_spi_errorcode) 0;
}
e_ttc_spi_errorcode spi_stm32w1xx_send_string(t_ttc_spi_config* Config, const char* Buffer, t_u16 MaxLength) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL
    Assert_SPI(Buffer, ttc_assert_origin_auto); // pointers must not be NULL

    //ToDo: implement fast send function (default implementation in interface

    return (e_ttc_spi_errorcode) 0;
}
#if (TTC_ASSERT_SPI == 1)
void spi_stm32w1xx_send_word(t_ttc_spi_config* Config, const t_u16 Word) {
    Assert_SPI(Config, ttc_assert_origin_auto); // pointers must not be NULL

    *( (volatile t_u32*) Config->LowLevelConfig.DataRegister ) = Word;

}
#endif
e_ttc_spi_errorcode spi_stm32w1xx_read_raw(t_ttc_spi_config* Config, ct_u8* BufferRx, t_u16 AmountRx) {
    Assert_SPI_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_SPI_EXTRA(ttc_memory_is_writable(BufferRx), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function spi_stm32w1xx_read_raw() is empty.
    
    return (e_ttc_spi_errorcode) 0;
}
e_ttc_spi_errorcode spi_stm32w1xx_send_read(t_ttc_spi_config* Config, const t_u8* BufferTx, t_u16 AmountTx, t_u16 AmountZerosTx, t_u8* BufferRx, t_u16 AmountRx) {
    Assert_SPI_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_SPI_EXTRA(ttc_memory_is_writable(BufferTx), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_SPI_EXTRA(ttc_memory_is_writable(BufferRx), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function spi_stm32w1xx_send_read() is empty.
    
    return (e_ttc_spi_errorcode) 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
