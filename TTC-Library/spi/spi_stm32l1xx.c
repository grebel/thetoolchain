/** { spi_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for spi devices on stm32l1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 21 at 20140423 11:04:41 UTC
 *
 *  Note: See ttc_spi.h for description of stm32l1xx independent SPI implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "spi_stm32l1xx.h"
#include "../ttc_register.h"
#include "../ttc_task.h"
#include "../ttc_heap.h"      // dynamic memory and safe arrays

//{ Global Variables ***********************************************************

/* DEPRECATED
volatile t_register_stm32l1xx_spi_sr*  spi_stm32l1xx_SPI1_SR    = &( register_stm32l1xx_SPI1.SR );
volatile t_register_stm32l1xx_spi_sr*  spi_stm32l1xx_SPI2_SR    = &( register_stm32l1xx_SPI2.SR );
volatile t_register_stm32l1xx_spi_cr1* spi_stm32l1xx_SPI1_CR1  = &( register_stm32l1xx_SPI1.CR1 );
volatile t_register_stm32l1xx_spi_cr1* spi_stm32l1xx_SPI2_CR1  = &( register_stm32l1xx_SPI2.CR1 );
volatile t_u8*                         spi_stm32l1xx_SPI1_DR_8  = ( volatile t_u8* )  & ( register_stm32l1xx_SPI1.DR );
volatile t_u16*                        spi_stm32l1xx_SPI1_DR_16 = ( volatile t_u16* ) & ( register_stm32l1xx_SPI1.DR );
volatile t_u8*                         spi_stm32l1xx_SPI2_DR_8  = ( volatile t_u8* )  & ( register_stm32l1xx_SPI2.DR );
volatile t_u16*                        spi_stm32l1xx_SPI2_DR_16 = ( volatile t_u16* ) & ( register_stm32l1xx_SPI2.DR );
*/

//}Global Variables
//{ Private Function prototypes ******************************************

/** sends out given word via SPI1
 *
 * @param Word (t_u16)  8- or 16-bit value to send (depends on SPI configuration)
 */
void _spi_stm32l1xx_send_word_spi1( t_u16 Word );
#define _spi_stm32l1xx_send_word_spi1(Word) register_stm32l1xx_SPI1.DR.All = Word

/** sends out given word via SPI2
 *
 * @param Word (t_u16)  8- or 16-bit value to send (depends on SPI configuration)
 */
void _spi_stm32l1xx_send_word_spi2( t_u16 Word );
#define _spi_stm32l1xx_send_word_spi2(Word) register_stm32l1xx_SPI2.DR.All = Word

/** reads single byte from SPI1
 *
 * @param Byte (t_u8*)  pointer where to store 8-bit value
 */
void _spi_stm32l1xx_read_byte_spi1( t_u8* Buffer );
#define _spi_stm32l1xx_read_byte_spi1(Buffer) *Buffer = (t_u8) register_stm32l1xx_SPI1.DR.All

/** reads single byte from SPI2
 *
 * @param Byte (t_u8*)  pointer where to store 8-bit value
 */
void _spi_stm32l1xx_read_byte_spi2( t_u8* Buffer );
#define _spi_stm32l1xx_read_byte_spi2(Buffer) *Buffer = (t_u8) register_stm32l1xx_SPI2.DR.All

/** reads two bytes from SPI1
 *
 * @param Word (t_u16*)  pointer where to store 16-bit value
 */
void _spi_stm32l1xx_read_word_spi1( t_u16* Buffer );
#define _spi_stm32l1xx_read_word_spi1(Buffer) *Buffer = (t_u16) register_stm32l1xx_SPI1.DR.Bits.Data

/** reads two bytes from SPI2
 *
 * @param Word (t_u16*)  pointer where to store 16-bit value
 */
void _spi_stm32l1xx_read_word_spi2( t_u16* Buffer );
#define _spi_stm32l1xx_read_word_spi2(Buffer) *Buffer = (t_u16) register_stm32l1xx_SPI2.DR.Bits.Data

t_u8 _spi_wait_TXE_empty( t_ttc_spi_config* Config );

//
// Convention: Private functions start with an underscore "_".
// Example:    void _spi_stm32l1xx_foo(t_ttc_spi_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function Definitions *******************************************************

void                spi_stm32l1xx_deinit( t_ttc_spi_config* Config ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    if ( Config->PhysicalIndex == 0 ) {
        // disable SPI1 device
        register_stm32l1xx_SPI1.CR1.Bits.SPE = 0;

        // Enable SPI1 reset state
        register_stm32l1xx_RCC.APB2RSTR.Bits.SPI1RST = 1;
        //DEPRECATED sysclock_stm32l1xx_RCC_APB2PeriphResetCmd( rcc_APB2Periph_SPI1, ENABLE );

        // Release SPI1 from reset state
        register_stm32l1xx_RCC.APB2RSTR.Bits.SPI1RST = 0;
        //DEPRECATED sysclock_stm32l1xx_RCC_APB2PeriphResetCmd( rcc_APB2Periph_SPI1, DISABLE );
    }
    else {
        // disable SPI2 device
        register_stm32l1xx_SPI2.CR1.Bits.SPE = 0;

        // Enable SPI2 reset state
        register_stm32l1xx_RCC.APB1RSTR.Bits.SPI2RST = 1;
        //DEPRECATED sysclock_stm32l1xx_RCC_APB1PeriphResetCmd( rcc_APB1Periph_SPI2, ENABLE );

        // Release SPI2 from reset state
        register_stm32l1xx_RCC.APB1RSTR.Bits.SPI2RST = 0;
        //DEPRECATED sysclock_stm32l1xx_RCC_APB1PeriphResetCmd( rcc_APB1Periph_SPI2, DISABLE );
    }
}
e_ttc_spi_errorcode spi_stm32l1xx_get_features( t_ttc_spi_config* Config ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    Config->Init.Flags.Master                = 1;
    Config->Init.Flags.MultiMaster           = 1;
    Config->Init.Flags.ClockIdleHigh         = 1;
    Config->Init.Flags.ClockPhase2ndEdge     = 1;
    Config->Init.Flags.Simplex               = 1;
    Config->Init.Flags.Bidirectional         = 1;
    Config->Init.Flags.Receive               = 1;
    Config->Init.Flags.Transmit              = 1;
    Config->Init.Flags.WordSize16            = 1;
    Config->Init.Flags.HardwareNSS           = 0;
    Config->Init.Flags.CRC8                  = 0; // ToDo
    Config->Init.Flags.CRC16                 = 0; // ToDo
    Config->Init.Flags.FirstBitMSB           = 1;
    Config->Init.Flags.TxDMA                 = 0; // ToDo
    Config->Init.Flags.RxDMA                 = 0; // ToDo
    Config->Init.Flags.Irq_Error             = 0; // ToDo
    Config->Init.Flags.Irq_Received          = 0; // ToDo
    Config->Init.Flags.Irq_Transmitted       = 0; // ToDo

    return ec_spi_OK;
}
e_ttc_spi_errorcode spi_stm32l1xx_init( t_ttc_spi_config* Config ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    if ( Config->PhysicalIndex == 0 ) { // power up and disable SPI1 device

        Config->LowLevelConfig->BaseRegister = & register_stm32l1xx_SPI1;
        register_stm32l1xx_RCC.APB2ENR.Bits.SPI1EN = 1;
        //DEPRECATED sysclock_stm32l1xx_RCC_APB2PeriphClockCmd( rcc_APB2Periph_SPI1, ENABLE ); // SPI Periph clock enable

        // disable SPI1 device
        register_stm32l1xx_SPI1.CR1.Bits.SPE = 0;
    }
    else {                            // power up and disable SPI2 device

        Config->LowLevelConfig->BaseRegister = & register_stm32l1xx_SPI2;
        register_stm32l1xx_RCC.APB1ENR.Bits.SPI2_EN = 1;
        //DEPRECATED sysclock_stm32l1xx_RCC_APB1PeriphClockCmd( rcc_APB1Periph_SPI2, ENABLE ); // SPI Periph clock enable

        // disable SPI2 device
        register_stm32l1xx_SPI2.CR1.Bits.SPE = 0;
    }
    volatile t_register_stm32l1xx_spi* BaseRegister = Config->LowLevelConfig->BaseRegister;

    volatile t_register_stm32l1xx_spi_cr1         CR1; CR1.All = 0;
    volatile t_register_stm32l1xx_spi_cr2         CR2; CR2.All = 0;
    volatile t_register_stm32l1xx_spi_i2scfgr I2SCFGR; I2SCFGR.All = 0;

    if ( 1 ) { // configure SPI interface registers (-> RM0038 p.706)

        if ( Config->Init.Flags.Bidirectional ) {
            CR1.Bits.BIDIMODE = 1;                       // 1-line bidirectional data mode
            CR1.Bits.BIDIOE = Config->Init.Flags.Master; // output enabled in master mode
        }
        else
        { CR1.Bits.BIDIMODE = 0; }  // 2-line unidirectional data mode

        CR1.Bits.CRC_EN    = Config->Init.Flags.CRC8 | Config->Init.Flags.CRC16; // enable checksums
        CR1.Bits.DFF      = Config->Init.Flags.WordSize16;        // word size 8 or 16 bits
        CR1.Bits.RXONLY   = 1 - Config->Init.Flags.Transmit;      // set interface to receive only if Transmit==0
        CR1.Bits.SSI      = 1;                                    // disable internal slave select
        CR1.Bits.LSBFIRST = 1 - Config->Init.Flags.FirstBitMSB;   // bit order
        CR1.Bits.MSTR     = Config->Init.Flags.Master;            // enable master-mode
        CR1.Bits.CPOL     = Config->Init.Flags.ClockIdleHigh;     // clock polarity (==1: Idle High)
        CR1.Bits.CPHA     = Config->Init.Flags.ClockPhase2ndEdge; // clock phase (==1: 2nd clock transition is first data capture edge)
        CR2.Bits.FRF      = 0;                                    // deselect TI mode (-> RM0038 p669)
    }
    if ( 1 ) { // configure baudrate prescaler
        t_u16 BaudrateDivider = 2;
        t_base FrequencyAPB = ( Config->PhysicalIndex == 0 ) ? ttc_sysclock_get_configuration()->LowLevelConfig.frequency_apb1 :
                              ttc_sysclock_get_configuration()->LowLevelConfig.frequency_apb2;

        // find lowest divider that gives SPI-Frequency <= Config->Init.BaudRate
        t_u8 RegisterValue = 0;
        while ( ( BaudrateDivider < 256 ) &&
                ( FrequencyAPB / BaudrateDivider > Config->Init.BaudRate )
              )
        { BaudrateDivider *= 2; RegisterValue++; }

        CR1.Bits.BR = RegisterValue;
        Config->Init.BaudRate = FrequencyAPB / BaudrateDivider;
    }
    if ( 1 ) { // configure GPIO pins
        if ( Config->Init.Pin_MISO ) {
            ttc_gpio_init( Config->Init.Pin_MISO, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max );
            ttc_gpio_alternate_function( Config->Init.Pin_MISO, E_ttc_gpio_alternate_function_AF5 ); // == E_ttc_gpio_alternate_function_SPI1 == E_ttc_gpio_alternate_function_SPI2
        }
        if ( Config->Init.Pin_MOSI ) {
            ttc_gpio_init( Config->Init.Pin_MOSI, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max );
            ttc_gpio_alternate_function( Config->Init.Pin_MOSI, E_ttc_gpio_alternate_function_AF5 ); // == E_ttc_gpio_alternate_function_SPI1 == E_ttc_gpio_alternate_function_SPI2
        }
        if ( Config->Init.Pin_SCLK ) {
            ttc_gpio_init( Config->Init.Pin_SCLK, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max );
            ttc_gpio_alternate_function( Config->Init.Pin_SCLK, E_ttc_gpio_alternate_function_AF5 ); // == E_ttc_gpio_alternate_function_SPI1 == E_ttc_gpio_alternate_function_SPI2
        }

        CR1.Bits.SSM  = 0; // ??? software slave management
        CR2.Bits.SSOE = 1; // set NSS pin to output in master mode
    }
    if ( 1 ) { // configure CRC
        BaseRegister->CRC_Polynom.Bits.Polynom = Config->Init.CRC_Polynom;
    }

    // write new settings into registers
    *( ( volatile t_u32* ) & ( BaseRegister->CR1 ) )     = *( ( volatile t_u32* ) &CR1 );
    *( ( volatile t_u32* ) & ( BaseRegister->CR2 ) )     = *( ( volatile t_u32* ) &CR2 );
    *( ( volatile t_u32* ) & ( BaseRegister->I2SCFGR ) ) = *( ( volatile t_u32* ) &I2SCFGR );

    // check if the registers have been updated correctly
    Assert_SPI( BaseRegister->CR1.All     == CR1.All, ttc_assert_origin_auto );   // somehow the register was not written correctly! (compiler optimization issue?)
    Assert_SPI( BaseRegister->CR2.All     == CR2.All, ttc_assert_origin_auto );   // somehow the register was not written correctly! (compiler optimization issue?)
    Assert_SPI( BaseRegister->I2SCFGR.All == I2SCFGR.All, ttc_assert_origin_auto );  // somehow the register was not written correctly! (compiler optimization issue?)

    // enable SPI device
    CR1.Bits.SPE = 1;
    *( ( volatile t_u32* ) & ( BaseRegister->CR1 ) )     = *( ( volatile t_u32* ) &CR1 );

    Assert_SPI( BaseRegister->CR1.All     == CR1.All, ttc_assert_origin_auto );   // somehow the register was not written correctly! (compiler optimization issue?)

    TTC_TASK_RETURN( ec_spi_OK );
}
e_ttc_spi_errorcode spi_stm32l1xx_load_defaults( t_ttc_spi_config* Config ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    Config->Init.Flags.Master                = 1;
    Config->Init.Flags.MultiMaster           = 0;
    Config->Init.Flags.ClockIdleHigh         = 0;
    Config->Init.Flags.ClockPhase2ndEdge     = 0;
    Config->Init.Flags.Simplex               = 0;
    Config->Init.Flags.Bidirectional         = 0;
    Config->Init.Flags.Receive               = 1;
    Config->Init.Flags.Transmit              = 1;
    Config->Init.Flags.WordSize16            = 0;
    Config->Init.Flags.HardwareNSS           = 0;
    Config->Init.Flags.CRC8                  = 1;
    Config->Init.Flags.CRC16                 = 0;
    Config->Init.Flags.FirstBitMSB           = 1;
    Config->Init.Flags.TxDMA                 = 0; // ToDo
    Config->Init.Flags.RxDMA                 = 0; // ToDo
    Config->Init.Flags.Irq_Error             = 0; // ToDo
    Config->Init.Flags.Irq_Received          = 0; // ToDo
    Config->Init.Flags.Irq_Transmitted       = 0; // ToDo

    Config->Init.CRC_Polynom = 7;
    Config->Layout      = 0;

    t_ttc_spi_architecture* ConfigArch = Config->LowLevelConfig;

    if ( ConfigArch == NULL ) {
        ConfigArch = ( t_ttc_spi_architecture* ) ttc_heap_alloc_zeroed( sizeof( t_ttc_spi_architecture ) );
        Config->LowLevelConfig = ConfigArch;
    }

    TTC_TASK_RETURN( ec_spi_OK );
}
void                spi_stm32l1xx_prepare() {

}
e_ttc_spi_errorcode spi_stm32l1xx_reset( t_ttc_spi_config* Config ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    spi_stm32l1xx_deinit( Config );
    spi_stm32l1xx_init( Config );

    return ec_spi_OK;
}
void                spi_stm32l1xx_read_byte( t_ttc_spi_config* Config, t_u8* Byte ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_SPI_Writable( Byte, ttc_assert_origin_auto );  // pointers must not be NULL

    volatile t_u16* DR16                     = &( Config->LowLevelConfig->BaseRegister->DR.Bits.Data );
    volatile t_register_stm32l1xx_spi_sr* SR = &( Config->LowLevelConfig->BaseRegister->SR );

    if ( SR->Bits.RXNE ) // data available: read it
    { *Byte = *( ( t_u8* ) DR16 ); }
    else {                                  // no data available: send out zero word + read
        *( ( t_u8* ) DR16 ) = Config->Init.SendAsZero;
        while ( SR->Bits.RXNE == 0 ) {}
        *Byte = *( ( t_u8* ) DR16 );
    }
}
void                spi_stm32l1xx_read_word( t_ttc_spi_config* Config, t_u16* Word ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_SPI_Writable( Word, ttc_assert_origin_auto );  // pointers must not be NULL

    volatile t_u16* DR16                     = &( Config->LowLevelConfig->BaseRegister->DR.Bits.Data );
    volatile t_register_stm32l1xx_spi_sr* SR = &( Config->LowLevelConfig->BaseRegister->SR );

    if ( SR->Bits.RXNE ) // data available: read it
    { *Word = *DR16; }
    else {                                  // no data available: send out zero word + read
        *DR16 = Config->Init.SendAsZero;
        while ( SR->Bits.RXNE == 0 ) {}
        *Word = *DR16;
    }
}
t_u16 spi_stm32l1xx_read_raw( t_ttc_spi_config* Config, t_u8* BufferRx, t_u16 AmountRx ) {
    Assert_SPI_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_SPI_EXTRA_Writable( BufferRx, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_u16 AmountRead = 0;
    t_u16 SendAsZero = Config->Init.SendAsZero;

    volatile t_u16* DR16                     = &( Config->LowLevelConfig->BaseRegister->DR.Bits.Data );
    volatile t_register_stm32l1xx_spi_sr* SR = &( Config->LowLevelConfig->BaseRegister->SR );

    if ( Config->LowLevelConfig->BaseRegister->CR1.Bits.DFF ) { // 16 bit transfers
        Assert_SPI( ( AmountRx & 1 ) == 0, ttc_assert_origin_auto ); // value must be a multiple of 2 for 16 bit transfers!
        Assert_SPI( ( ( ( ( t_base ) BufferRx ) & 1 ) == 0 ), ttc_assert_origin_auto );  // address must be 16-bit aligned for 16-bit transfers!
        if ( AmountRx ) {
            AmountRx /= 2; // 16-bit transfers require only half amount of loops

            if ( SR->Bits.RXNE )
            { *( ( t_u16* ) BufferRx ) = *DR16; } // empty receive buffer

            do {
                while ( SR->Bits.TXE == 0 )  {}       // Wait until TX buffer is empty
                *DR16 = SendAsZero;                 // send out zero word

                while ( SR->Bits.RXNE == 0 ) {}       // Wait until word received
                *( ( t_u16* ) BufferRx ) = *DR16;    // read 16-bit word
                AmountRead += 2;
                BufferRx += 2;
            }
            while ( AmountRx-- > 0 );
        }
    }
    else  {                                // 8 bit transfers
        if ( SR->Bits.RXNE )
        { *BufferRx = *( ( t_u8* ) DR16 ); } // empty receive buffer

        while ( AmountRx-- > 0 ) {
            while ( SR->Bits.TXE == 0 ) {}            // Wait until TX buffer is empty
            *( ( t_u8* ) DR16 ) = ( t_u8 ) SendAsZero; // send out zero byte

            while ( SR->Bits.RXNE == 0 ) {}           // Wait until byte received
            *BufferRx++ = *( ( t_u8* ) DR16 );            // read single byte
        }
    }

    return AmountRead;
}
e_ttc_spi_errorcode spi_stm32l1xx_send_raw( t_ttc_spi_config* Config, const t_u8* Buffer, t_u16 Amount ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_SPI_Readable( Buffer, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_SPI_EXTRA( register_stm32l1xx_SPI1.CR1.All, ttc_assert_origin_auto );  // SPI not configured at all (CR1==0)!

    volatile t_u16* DR16                     = &( Config->LowLevelConfig->BaseRegister->DR.Bits.Data );
    volatile t_register_stm32l1xx_spi_sr* SR = &( Config->LowLevelConfig->BaseRegister->SR );

    if ( Config->LowLevelConfig->BaseRegister->CR1.Bits.DFF ) { // 16-bit transfers
        if ( Amount ) {
            const t_u16* Buffer16 = ( const t_u16* ) Buffer;
            Amount /= 2;
            while ( Amount-- > 0 ) {
                while ( SR->Bits.TXE == 0 ) {} // Wait until TX buffer is empty
                *DR16 = ( *Buffer16++ );
            }
        }
    }
    else {                               // 8-bit transfers
        while ( Amount-- > 0 ) {
            while ( SR->Bits.TXE == 0 ) {} // Wait until TX buffer is empty
            *DR16 = ( *Buffer++ );
        }
    }
    while ( !SR->Bits.TXE ) {}
    while ( SR->Bits.BSY ) {}

    return ec_spi_OK;
}
void                spi_stm32l1xx_send_word( t_ttc_spi_config* Config, const t_u16 Word ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    volatile t_u16* DR16                     = &( Config->LowLevelConfig->BaseRegister->DR.Bits.Data );
    volatile t_register_stm32l1xx_spi_sr* SR = &( Config->LowLevelConfig->BaseRegister->SR );

    if ( Config->PhysicalIndex == 0 ) {
        while ( SR->Bits.TXE == 0 ) {} // Wait until TX buffer is empty
        *DR16 = Word;
    }
    else                            {
        while ( SR->Bits.TXE == 0 ) {} // Wait until TX buffer is empty
        *DR16 = Word;
    }
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************


//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
