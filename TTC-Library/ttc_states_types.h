/** { ttc_states_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for STATES device.
 *  Structures, Enums and Defines being required by both, high- and low-level states.
 *  This file does not provide function declarations!
 *
 *  Created from template ttc_device_types.h revision 40 at 20180126 14:37:55 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_STATES_TYPES_H
#define TTC_STATES_TYPES_H

//{ Includes1 ************************************************************
//
// Includes being required for static configuration.
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_states.h" or "ttc_states.c"
//
#include "ttc_basic_types.h"
#include "ttc_heap_types.h"
#include "compile_options.h"

//} Includes
/** { Static Configuration *************************************************
 *
 * Most ttc drivers are statically configured by constant definitions in makefiles.
 * A static configuration allows to hide certain code fragments and variables for the
 * compilation process. If used wisely, your code will be lean and fast and do only
 * what is required for the current configuration.
 */


/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_STATES 0#         disable default asserts for states driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_STATES_EXTRA 1#   enable extra asserts for states driver
 *
 */
#ifndef TTC_ASSERT_STATES    // any previous definition set (Makefile)?
    #define TTC_ASSERT_STATES 1  // basic asserts are enabled by default
#endif
#if (TTC_ASSERT_STATES == 1)  // use Assert()s in STATES code (somewhat slower but alot easier to debug)
    #define Assert_STATES(Condition, Origin)        if (! (Condition) ) { ttc_assert_halt_origin(Origin); }
    #define Assert_STATES_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_STATES_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in STATES code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_STATES(Condition, Origin)
    #define Assert_STATES_Writable(Address, Origin)
    #define Assert_STATES_Readable(Address, Origin)
#endif

#ifndef TTC_ASSERT_STATES_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_STATES_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_STATES_EXTRA == 1)  // use Assert()s in STATES code (somewhat slower but alot easier to debug)
    #define Assert_STATES_EXTRA(Condition, Origin) if (! (Condition) ) { ttc_assert_halt_origin(Origin); }
    #define Assert_STATES_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_STATES_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in STATES code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_STATES_EXTRA(Condition, Origin)
    #define Assert_STATES_EXTRA_Writable(Address, Origin)
    #define Assert_STATES_EXTRA_Readable(Address, Origin)
#endif
//}

/* Check if a datatype has been defined for the current state.
 * Use default datatype if not.
 */
#ifndef TTC_STATES_TYPE
    #define TTC_STATES_TYPE  t_u8
#endif

/* Check if we should record previous states to aid debugging.
 *
 * If TTC_STATES_AMOUNT_RECORDS>0 then the curse of previous states is recorded on every switch
 * in Config->State_History[]. You may use ttc_states_history() in a gdb session to display this history:
 * Example:
 * gdb> p *ttc_states_history(1)
 */
#ifndef TTC_STATES_AMOUNT_RECORDS
    #define TTC_STATES_AMOUNT_RECORDS 10 // default: disable state recording
#endif

//}Static Configuration

//{ Includes2 ************************************************************
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_states.h" or "ttc_states.c"
//

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_states_errorcode       return codes of STATES devices
    ec_states_OK = 0,

    // other warnings go here..

    ec_states_ERROR,                  // general failure
    ec_states_NULL,                   // NULL pointer not accepted
    ec_states_DeviceNotFound,         // corresponding device could not be found
    ec_states_InvalidConfiguration,   // sanity check of device configuration failed
    ec_states_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..

    ec_states_unknown                // no valid errorcodes past this entry
} e_ttc_states_errorcode;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef enum { // e_ttc_states_transition  valid types of state transition
    states_transition_None  = 0,
    states_transition_Switch,     // state transition caused by ttc_state_switch() (switch from one state to another)
    states_transition_Call,       // state transition caused by ttc_state_call()   (calling a sub-state)
    states_transition_Return,     // state transition caused by ttc_state_return() (return from sub-state)
    states_transition_unknown     // invalid/ unsupported values from here
} e_ttc_states_transition;
typedef union {  // architecture dependent configuration

    //InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_states_architecture;


typedef struct s_ttc_states_state { // declaration of an individual state

    /** Pointer to a function that will be called while this state is active.
     *
     * @param StatesConfig  (t_ttc_states_config*)    pointer to corresponding, initialized ttc_states instance
     * @param StateData     (application specific*)   value from t_ttc_states_config.StateData
     * @param Transition    (e_ttc_states_transition) >0: state has just been entered (transitioned from a different state); ==0: same state is same as for previous call
     */
    void ( *StateFunction )( void* StatesConfig, void* StateData, e_ttc_states_transition Transition );

    // Unique number identifying this state.
    // Each state must have its own number.
    // It is advised that TTC_STATES_TYPE is an enumeration type to
    // avoid spare entries in t_ttc_states_config.AllStates[].
    TTC_STATES_TYPE Number;

    // Amount of other states to which may be switched from this state.
    // Any other state switch will cause an assert.
    t_u8 AmountReachable;

    struct { // binary flags
        unsigned IsSubstate : 1;  // ==1: this state is to be called via ttc_states_call() instead of ttc_states_switch()

    } Flags;

    // Array of state enums to which this state may switch to.
    // The array must be terminated by a zero (0) for extra safety.
    // The array entries must be sorted in increasing order find entries faster.
    const TTC_STATES_TYPE ReachableStates[];

} __attribute__( ( __packed__ ) ) t_ttc_states_state;

typedef struct s_ttc_states_call {   // data of a single sub-state call
    void*           Value;        // when calling a sub-state: pointer can be read by sub-state using ttc_states_argument()
    // wehen returning from a sub-state: value given as argument Return to ttc_states_return()
    TTC_STATES_TYPE ReturnState;  // state number to return to by next ttc_states_return() call
} __attribute__( ( __packed__ ) ) t_ttc_states_call;

typedef struct s_ttc_states_config { //          architecture independent configuration data
    struct s_ttc_states_config* Next;  // First Entry: pointer to next configuration in single linked list

    struct { // Init: fields to be set by application before first calling ttc_<device>_init() --------------
        // Note: Write-access to this structure is only allowed before ttc_states_init()
        //       and after ttc_states_deinit() call!
        //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.
        // Do not change these values after calling ttc_states_init()!

        //...

        /* AllStates[] must be loaded with a pointer to a zero terminated array of pointers to state configurations.
         * Each valid state of your statemachine must be described here.
         * States[0] stores the initial state that will be called when statemachine is run first.
         * State number S must be stored in States[S-State_Initial] for all S = State_Initial..State_Initial+AmountStates-1.
         *
         * Example:
         * State_Initial = 6; // statemachine starts in state number 6
         * States[0] = { .Number = 6, ... };  // initial state
         * States[1] = { .Number = 7, ... };  // following state increases Number by 1
         * ...
         */
        const t_ttc_states_state** AllStates;

        // pointer to any kind of data being used by statemachine functions
        void* StateData;

        /** !=NULL: pointer to debug function being called before every state transition
         *
         * Note: This function is only called if TTC_ASSERT_STATES == 1
         * Note: Rename _my_states_monitor to avoid name collision with other monitor function!
         * Note: Sometimes a bug occurs after a fixed amount of function calls. monitor() may count its calls and call
         *       ttc_assert_break_origin() automatically. See _my_states_monitor() in ttc_states.h as an example.
         *
         * Usage: 1) Copy declaration and definition of _my_states_monitor() from ttc_states.h into your source code
         *        2) Set StatesConfig->monitor = my_states_monitor before calling ttc_states_init()
         *        3) Start a debug session and place a breakpoint inside:
         *           gdb> b _my_states_monitor
         *           gdb> c
         *
         * @param StatesConfig         (volatile t_ttc_states_config*)  configuration of initialized ttc_states instance
         * @param State_Current        (volatile TTC_STATES_TYPE)       state being transitioned to   (TTC_STATES_TYPE known by application only)
         * @param State_Previous       (volatile TTC_STATES_TYPE)       state being transitioned from (TTC_STATES_TYPE known by application only)
         * @param Transition           (e_ttc_states_transition)        cause of transition
         *
         * Copy this template for your own monitor function (set pointer target datatype of StateData as you like)
         *
           void _my_states_monitor( volatile t_ttc_states_config* StatesConfig, void* StateData, volatile TTC_STATES_TYPE State_Current, volatile TTC_STATES_TYPE State_Previous, e_ttc_states_transition Transition ) { ... }
           MyStateMachine->Init.MonitorTransition = ( void ( * )( volatile void*, void*, volatile TTC_STATES_TYPE, volatile TTC_STATES_TYPE, e_ttc_states_transition) ) &_my_states_monitor;

         */
        void ( * MonitorTransition )( volatile void* StatesConfig, void* StateData, volatile TTC_STATES_TYPE State_Current, volatile TTC_STATES_TYPE State_Previous, e_ttc_states_transition Transition );

        // Maximum allowed amount of consecutive ttc_states_call() calls (size of LIFO stack)
        // This value may not be increased after first ttc_states_init() call!
        t_u8  Stack_Size;

    } Init;




    // Fields below are read-only for application! ---------------------------------------
    t_u8                    LogicalIndex;                             // automatically set: logical index of device to use (1 = TTC_STATES1, ...)
    t_u16                   AmountStates;                             // Amount of non-zero entries of AllStates[] (valid entries: States[0..AmountStates-1])
    t_ttc_states_call*      Stack_States;                             // Dynamic array storing a LIFO (Last In First Out) Stack for ttc_states_call() and _pop()
    TTC_STATES_TYPE         State_Initial;                            // identification number of first state == States[0]->Number
    TTC_STATES_TYPE         State_LastValid;                          // highest valid state identification number
    TTC_STATES_TYPE         State_Current;                            // current state of this statemachine (change via ttc_state_call() only!)
    TTC_STATES_TYPE         State_Previous;                           // previous state for which statemachine function was called (updated after calling function)
    TTC_STATES_TYPE         State_History[TTC_STATES_AMOUNT_RECORDS]; // recorded previous states (updated for each ttc_states_switch())
    e_ttc_states_transition Transition;                               // cause of last state transition
    t_u8                    Index_State_History;                      // array index of next free entry in State_History[]
    t_u8                    Index_Stack;                              // array index of next free entry in Stack_States[] (==0: stack is empty)

    union  { // generic configuration bits common for all low-level Drivers
        t_u8 All;
        struct {
            unsigned Initialized      : 1;  // ==1: device has been initialized successfully
#if (TTC_ASSERT_STATES == 1)
            unsigned AlreadyRunning   : 1;  // ==1: ttc_states_run() is currently running (used for self test)
#endif

        } Bits;
    } Flags;


    // Last returned error code
    // IMPORTANT: Every ttc_states_*() function that returns e_ttc_states_errorcode has to update this value if it returns an error!
    e_ttc_states_errorcode LastError;

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_states_config;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_STATES_TYPES_H
