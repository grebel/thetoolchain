/** { ttc_layer_phy_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for LAYER_PHY device.
 *  Structures, Enums and Defines being required by both, high- and low-level layer_phy.
 *  This file does not provide function declarations!
 *  
 *  Created from template ttc_device_types.h revision 27 at 20150304 08:22:38 UTC
 *
 *  Authors: Gregor Rebel
 * 
}*/

#ifndef TTC_LAYER_PHY_TYPES_H
#define TTC_LAYER_PHY_TYPES_H

//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_layer_phy.h" or "ttc_layer_phy.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h" // only required to help IDE to know constant definitions
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
#ifdef EXTENSION_layer_phy_spi
#  include "layer_phy/layer_phy_spi_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)
 
//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

//{  TTC_LAYER_PHYn has to be defined as constant by makefile.100_board_*
#ifdef TTC_LAYER_PHY5
  #ifndef TTC_LAYER_PHY4
    #error TTC_LAYER_PHY5 is defined, but not TTC_LAYER_PHY4 - all lower TTC_LAYER_PHYn must be defined!
  #endif
  #ifndef TTC_LAYER_PHY3
    #error TTC_LAYER_PHY5 is defined, but not TTC_LAYER_PHY3 - all lower TTC_LAYER_PHYn must be defined!
  #endif
  #ifndef TTC_LAYER_PHY2
    #error TTC_LAYER_PHY5 is defined, but not TTC_LAYER_PHY2 - all lower TTC_LAYER_PHYn must be defined!
  #endif
  #ifndef TTC_LAYER_PHY1
    #error TTC_LAYER_PHY5 is defined, but not TTC_LAYER_PHY1 - all lower TTC_LAYER_PHYn must be defined!
  #endif

  #define TTC_LAYER_PHY_AMOUNT 5
#else
  #ifdef TTC_LAYER_PHY4
    #define TTC_LAYER_PHY_AMOUNT 4

    #ifndef TTC_LAYER_PHY3
      #error TTC_LAYER_PHY5 is defined, but not TTC_LAYER_PHY3 - all lower TTC_LAYER_PHYn must be defined!
    #endif
    #ifndef TTC_LAYER_PHY2
      #error TTC_LAYER_PHY5 is defined, but not TTC_LAYER_PHY2 - all lower TTC_LAYER_PHYn must be defined!
    #endif
    #ifndef TTC_LAYER_PHY1
      #error TTC_LAYER_PHY5 is defined, but not TTC_LAYER_PHY1 - all lower TTC_LAYER_PHYn must be defined!
    #endif
  #else
    #ifdef TTC_LAYER_PHY3

      #ifndef TTC_LAYER_PHY2
        #error TTC_LAYER_PHY5 is defined, but not TTC_LAYER_PHY2 - all lower TTC_LAYER_PHYn must be defined!
      #endif
      #ifndef TTC_LAYER_PHY1
        #error TTC_LAYER_PHY5 is defined, but not TTC_LAYER_PHY1 - all lower TTC_LAYER_PHYn must be defined!
      #endif

      #define TTC_LAYER_PHY_AMOUNT 3
    #else
      #ifdef TTC_LAYER_PHY2

        #ifndef TTC_LAYER_PHY1
          #error TTC_LAYER_PHY5 is defined, but not TTC_LAYER_PHY1 - all lower TTC_LAYER_PHYn must be defined!
        #endif

        #define TTC_LAYER_PHY_AMOUNT 2
      #else
        #ifdef TTC_LAYER_PHY1
          #define TTC_LAYER_PHY_AMOUNT 1
        #else
          #define TTC_LAYER_PHY_AMOUNT 0
        #endif
      #endif
    #endif
  #endif
#endif
//}
/**{ Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop. 
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_LAYER_PHY 0  # disable assert handling for layer_phy devices
 *
 */
#ifndef TTC_ASSERT_LAYER_PHY    // any previous definition set (Makefile)?
#define TTC_ASSERT_LAYER_PHY 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_LAYER_PHY == 1)  // use Assert()s in LAYER_PHY code (somewhat slower but alot easier to debug)
  #define Assert_LAYER_PHY(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in LAYER_PHY code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_LAYER_PHY(Condition, ErrorCode)
#endif
//}
/**{ Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_layer_phy_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_layer_phy_architecture
#warning Missing low-level definition for t_ttc_layer_phy_architecture (using default)
#define t_ttc_layer_phy_architecture void
#endif
//}
/**{ Check if definitions of logical indices are taken from e_ttc_layer_phy_physical_index
 *
 * See definition of e_ttc_layer_phy_physical_index below for details.
 *
 */

#define TTC_INDEX_LAYER_PHY_MAX 10  // must have same value as TTC_INDEX_LAYER_PHY_ERROR
#ifdef TTC_LAYER_PHY1
#  if TTC_LAYER_PHY1 >= TTC_INDEX_LAYER_PHY_MAX
#    error Wrong definition of TTC_LAYER_PHY1, check your makefile (must be one from e_ttc_layer_phy_physical_index)!
#  endif
#  ifndef TTC_LAYER_PHY1_DRIVER
#    warning Missing definition, check your makefile: TTC_LAYER_PHY1_DRIVER (choose one from e_ttc_layer_phy_architecture)
#    define TTC_LAYER_PHY1_DRIVER ta_layer_phy_None
#  endif
#endif
#ifdef TTC_LAYER_PHY2
#  if TTC_LAYER_PHY2 >= TTC_INDEX_LAYER_PHY_MAX
#    error Wrong definition of TTC_LAYER_PHY2, check your makefile (must be one from e_ttc_layer_phy_physical_index)!
#  endif
#  ifndef TTC_LAYER_PHY2_DRIVER
#    warning Missing definition, check your makefile: TTC_LAYER_PHY2_DRIVER (choose one from e_ttc_layer_phy_architecture)
#    define TTC_LAYER_PHY2_DRIVER ta_layer_phy_None
#  endif
#endif
#ifdef TTC_LAYER_PHY3
#  if TTC_LAYER_PHY3 >= TTC_INDEX_LAYER_PHY_MAX
#    error Wrong definition of TTC_LAYER_PHY3, check your makefile (must be one from e_ttc_layer_phy_physical_index)!
#  endif
#  ifndef TTC_LAYER_PHY3_DRIVER
#    warning Missing definition, check your makefile: TTC_LAYER_PHY3_DRIVER (choose one from e_ttc_layer_phy_architecture)
#    define TTC_LAYER_PHY3_DRIVER ta_layer_phy_None
#  endif
#endif
#ifdef TTC_LAYER_PHY4
#  if TTC_LAYER_PHY4 >= TTC_INDEX_LAYER_PHY_MAX
#    error Wrong definition of TTC_LAYER_PHY4, check your makefile (must be one from e_ttc_layer_phy_physical_index)!
#  endif
#  ifndef TTC_LAYER_PHY4_DRIVER
#    warning Missing definition, check your makefile: TTC_LAYER_PHY4_DRIVER (choose one from e_ttc_layer_phy_architecture)
#    define TTC_LAYER_PHY4_DRIVER ta_layer_phy_None
#  endif
#endif
#ifdef TTC_LAYER_PHY5
#  if TTC_LAYER_PHY5 >= TTC_INDEX_LAYER_PHY_MAX
#    error Wrong definition of TTC_LAYER_PHY5, check your makefile (must be one from e_ttc_layer_phy_physical_index)!
#  endif
#  ifndef TTC_LAYER_PHY5_DRIVER
#    warning Missing definition, check your makefile: TTC_LAYER_PHY5_DRIVER (choose one from e_ttc_layer_phy_architecture)
#    define TTC_LAYER_PHY5_DRIVER ta_layer_phy_None
#  endif
#endif
#ifdef TTC_LAYER_PHY6
#  if TTC_LAYER_PHY6 >= TTC_INDEX_LAYER_PHY_MAX
#    error Wrong definition of TTC_LAYER_PHY6, check your makefile (must be one from e_ttc_layer_phy_physical_index)!
#  endif
#  ifndef TTC_LAYER_PHY6_DRIVER
#    warning Missing definition, check your makefile: TTC_LAYER_PHY6_DRIVER (choose one from e_ttc_layer_phy_architecture)
#    define TTC_LAYER_PHY6_DRIVER ta_layer_phy_None
#  endif
#endif
#ifdef TTC_LAYER_PHY7
#  if TTC_LAYER_PHY7 >= TTC_INDEX_LAYER_PHY_MAX
#    error Wrong definition of TTC_LAYER_PHY7, check your makefile (must be one from e_ttc_layer_phy_physical_index)!
#  endif
#  ifndef TTC_LAYER_PHY7_DRIVER
#    warning Missing definition, check your makefile: TTC_LAYER_PHY7_DRIVER (choose one from e_ttc_layer_phy_architecture)
#    define TTC_LAYER_PHY7_DRIVER ta_layer_phy_None
#  endif
#endif
#ifdef TTC_LAYER_PHY8
#  if TTC_LAYER_PHY8 >= TTC_INDEX_LAYER_PHY_MAX
#    error Wrong definition of TTC_LAYER_PHY8, check your makefile (must be one from e_ttc_layer_phy_physical_index)!
#  endif
#  ifndef TTC_LAYER_PHY8_DRIVER
#    warning Missing definition, check your makefile: TTC_LAYER_PHY8_DRIVER (choose one from e_ttc_layer_phy_architecture)
#    define TTC_LAYER_PHY8_DRIVER ta_layer_phy_None
#  endif
#endif
#ifdef TTC_LAYER_PHY9
#  if TTC_LAYER_PHY9 >= TTC_INDEX_LAYER_PHY_MAX
#    error Wrong definition of TTC_LAYER_PHY9, check your makefile (must be one from e_ttc_layer_phy_physical_index)!
#  endif
#  ifndef TTC_LAYER_PHY9_DRIVER
#    warning Missing definition, check your makefile: TTC_LAYER_PHY9_DRIVER (choose one from e_ttc_layer_phy_architecture)
#    define TTC_LAYER_PHY9_DRIVER ta_layer_phy_None
#  endif
#endif
#ifdef TTC_LAYER_PHY10
#  if TTC_LAYER_PHY10 >= TTC_INDEX_LAYER_PHY_MAX
#    error Wrong definition of TTC_LAYER_PHY10, check your makefile (must be one from e_ttc_layer_phy_physical_index)!
#  endif
#  ifndef TTC_LAYER_PHY10_DRIVER
#    warning Missing definition, check your makefile: TTC_LAYER_PHY10_DRIVER (choose one from e_ttc_layer_phy_architecture)
#    define TTC_LAYER_PHY10_DRIVER ta_layer_phy_None
#  endif
#endif
//}

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef e_ttc_physical_index e_ttc_layer_phy_physical_index;
typedef enum {   // e_ttc_layer_phy_errorcode      return codes of LAYER_PHY devices
  ec_layer_phy_OK = 0, 
  
  // other warnings go here..

  ec_layer_phy_ERROR,                  // general failure
  ec_layer_phy_NULL,                   // NULL pointer not accepted
  ec_layer_phy_DeviceNotFound,         // corresponding device could not be found
  ec_layer_phy_InvalidConfiguration,   // sanity check of device configuration failed
  ec_layer_phy_InvalidImplementation,  // your code does not behave as expected

  // other failures go here..
  
  ec_layer_phy_unknown                // no valid errorcodes past this entry 
} e_ttc_layer_phy_errorcode;
typedef enum {   // e_ttc_layer_phy_architecture  types of architectures supported by LAYER_PHY driver
  ta_layer_phy_None,           // no architecture selected
  

  ta_layer_phy_spi, // automatically added by ./create_DeviceDriver.pl
//InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)
  
  ta_layer_phy_unknown        // architecture not supported
} e_ttc_layer_phy_architecture;
typedef struct s_ttc_layer_phy_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_layer_phy_init()
    //       and after ttc_layer_phy_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    e_ttc_layer_phy_architecture Architecture; // type of architecture used for current layer_phy device
    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_LAYER_PHY1, ...)
    t_u8  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)
    
    // low-level configuration (structure defined by low-level driver)
    t_ttc_layer_phy_architecture* LowLevelConfig;      
    
    union  { // generic configuration bits common for all low-level Drivers
        t_u16 All;
        struct {
          unsigned Initialized        : 1;  // ==1: device has been initialized successfully
          // ToDo: additional high-level flags go here..
            
            unsigned Reserved1        : 15; // pad to 16 bits
        } Bits;
    } Flags;

    // Additional high-level attributes go here..
    
} __attribute__((__packed__)) t_ttc_layer_phy_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_LAYER_PHY_TYPES_H
