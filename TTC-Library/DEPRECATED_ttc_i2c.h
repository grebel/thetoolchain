#ifndef TTC_I2C_H
#define TTC_I2C_H
/** { ttc_i2c.h *******************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  High-Level driver and documentation for I2C devices.
 *  The functions defined in this file provide a hardware independent interface 
 *  for your application.
 *                                          
 *  The basic usage scenario for devices:
 *  1) check:       Assert_I2C(tc_i2c_get_max_index() > 0, ec_DeviceNotFound);
 *  2) configure:   ttc_i2c_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_i2c_init(LogicalIndex);
 *  4) use:         ttc_i2c_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level i2c and application.
 *
 *  Created from template ttc_device.h revision 27 at 20140424 05:11:32 UTC
 *
 *  Authors: Gregor Rebel
 *  
 *    Reference:
 *
 *  RM008 - STM32 Reference Manual p. 672
 *  -> http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/REFERENCE_MANUAL/CD00171190.pdf
 *
 *
 *  How it works
 *
 *  Each makefile.100_board* can define one or more I2Cs being routed to connectors.
 *  This is done via lines of this type:
 *    COMPILE_OPTS += -D<OPTION>=<VALUE>
 *  This sets compile option <OPTION> to value <VALUE>
 *
 *  I2Cn are enumerated starting at index 1 (e.g. I2C1, I2C2, ..).
 *  Each I2Cn can be configured via these constants:
 *  TTC_I2C1       <I2C>                  sets internal I2C device to use
 *  TTC_I2C1_MOSI  <GPIO_BANK>,<PIN_NO>   sets port to use for Master Out Slave In pin
 *  TTC_I2C1_MISO  <GPIO_BANK>,<PIN_NO>   sets port to use for Master In Slave Out pin
 *  TTC_I2C1_SCK   <GPIO_BANK>,<PIN_NO>   sets port to use for Serial Clock pin
 *  TTC_I2C1_NSS   <GPIO_BANK>,<PIN_NO>   sets port to use for Slave Select pin
 *
 *  # Example excerpt from makefile.100_board_olimex_p107
 *  COMPILE_OPTS += -DTTC_I2C1=I2C3          # I2C connected to RS232 10-pin male header UEXT and SDCARD slot
 *  COMPILE_OPTS += -DTTC_I2C1_MOSI=GPIOC,12 # UEXT pin 8
 *  COMPILE_OPTS += -DTTC_I2C1_MISO=GPIOC,11 # UEXT pin 7
 *  COMPILE_OPTS += -DTTC_I2C1_SCK=GPIOC,10  # UEXT pin 9
 *  #COMPILE_OPTS += -DTTC_I2C1_NSS=         # NSS not connected
 *
}*/

#ifndef EXTENSION_500_ttc_i2c
#error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_i2c.sh
#endif

//{ Includes *************************************************************

#include "ttc_basic.h"
#include "ttc_memory.h"
#include "interfaces/DEPRECATED_ttc_i2c_interface.h" // multi architecture support
#include "DEPRECATED_ttc_i2c_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level i2c only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * i2c devices on all supported architectures.
 * Check i2c/i2c_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your 
 * activate_project.sh file inside your project folder.
 *
 */
 
/** Prepares i2c Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_i2c_prepare();
void _driver_i2c_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
u8_t ttc_i2c_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_I2C_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
ttc_i2c_config_t* ttc_i2c_get_configuration(u8_t LogicalIndex);

/** returns base address of low-level registers used to controll indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_I2C_get_max_LogicalIndex())
 * @param Config        = pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return                configuration of indexed device
 */
ttc_i2c_base_register_t* ttc_i2c_get_base_register(u8_t LogicalIndex);
ttc_i2c_base_register_t* _driver_i2c_get_base_register(ttc_i2c_config_t* Config);

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_I2C_get_max_LogicalIndex())
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
ttc_i2c_errorcode_e ttc_i2c_init(u8_t LogicalIndex);

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_I2C_get_max_LogicalIndex())
 */
ttc_i2c_errorcode_e  ttc_i2c_deinit(u8_t LogicalIndex);

/** load default setting values for indexed device
 *
 * Seetings of indexed device are being reset to their default values.
 * Will automatically call ttc_i2c_deinit() if device has been initialized.  
 *
 * @param LogicalIndex  logical index of i2c device (1..ttc_i2c_get_max_index() )
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
ttc_i2c_errorcode_e  ttc_i2c_load_defaults(u8_t LogicalIndex);

/** maps from logical to physical device index
 *
 * High-level i2cs (ttc_i2c_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_I2Cn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be 
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any 
 * order.
 *
 * @param LogicalIndex  logical index of i2c device (1..ttc_i2c_get_max_index() )
 * @return              physical index of i2c device (0 = first physical i2c device, ...)
 */
u8_t ttc_i2c_logical_2_physical_index(u8_t LogicalIndex);

/** maps from physical to logical device index
 *
 * This is a reverse function to ttc_i2c_logical_2_physical_index()
 *
 * @param  PhysicalIndex  physical index of i2c device (0 = first physical i2c device, ...)
 * @return                logical index of i2c device (1..ttc_i2c_get_max_index() )
 */
u8_t ttc_i2c_physical_2_logical_index(u8_t PhysicalIndex);

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_I2C_get_max_LogicalIndex())
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
ttc_i2c_errorcode_e  ttc_i2c_reset(u8_t LogicalIndex);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_i2c(u8_t LogicalIndex)

//InsertPrivatePrototypes above (DO NOT DELETE THIS LINE!)

//}private functions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_heap_ are passed to interfaces/ttc_heap_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl heap UPDATE
 */


/** fills out given Config with maximum valid values for indexed I2C
 * @param Config        = pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
ttc_i2c_errorcode_e _driver_i2c_get_features(ttc_i2c_config_t* Config);
#define ttc_i2c_get_features(Config) _driver_i2c_get_features(Config)

/** shutdown single I2C unit device
 * @param Config        pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return              == 0: I2C has been shutdown successfully; != 0: error-code
 */
ttc_i2c_errorcode_e _driver_i2c_deinit(ttc_i2c_config_t* Config);
#define ttc_i2c_get_deinit(Config) _driver_i2c_get_deinit(Config)

/** initializes single I2C unit for operation
 * @param Config        pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return              == 0: I2C has been initialized successfully; != 0: error-code
 */
ttc_i2c_errorcode_e _driver_i2c_init(ttc_i2c_config_t* Config);
#define ttc_i2c_get_init(Config) _driver_i2c_get_init(Config)

/** loads configuration of indexed I2C unit with default values
 * @param Config        pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
ttc_i2c_errorcode_e _driver_i2c_load_defaults(ttc_i2c_config_t* Config);

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 */
ttc_i2c_errorcode_e _driver_i2c_reset(ttc_i2c_config_t* Config);

/** Send out given byte to addressed target device
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param I2C_Index      device index of I2C to init (1..ttc_i2c_get_max_index() )
 * @param TargetAddress  receivers own TargetAddress
 * @param Register       index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Byte           8-bit data to be send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_i2c_errorcode_e _driver_i2c_write_register(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, const char Byte);
#define ttc_i2c_write_register(I2C_Index, TargetAddress,Register,RegisterType,Byte) _driver_i2c_write_register(I2C_Index, TargetAddress,Register,RegisterType,Byte)

/** Send out given amount of bytes to addressed target device.
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param I2C_Index    device index of I2C to init (1..ttc_i2c_get_max_index() )
 * @param TargetAddress      receivers own TargetAddress
 * @param Register     index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Buffer       memory location from which to read data
 * @param Amount       amount of bytes to send from Buffer[]
 * @return             == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_i2c_errorcode_e _driver_i2c_write_registers(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, const u8_t* Buffer, u16_t Amount);
#define ttc_i2c_write_registers(I2C_Index, TargetAddress,Register,RegisterType,Byte,Amount) _driver_i2c_write_registers(I2C_Index, TargetAddress,Register,RegisterType,Byte,Amount)


/** Reads single data byte from addressed target device.
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param I2C_Index      device index of I2C to init (1..ttc_i2c_get_max_index() )
 * @param TargetAddress  receivers own TargetAddress
 * @param Register       index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Byte           pointer to 8 bit buffer where to store received byte
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: Byte has been read successfully; != 0: error-code
 */
ttc_i2c_errorcode_e _driver_i2c_read_register(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, u8_t* Buffer);
#define ttc_i2c_read_register(I2C_Index, TargetAddress,Register,RegisterType,Buffer) _driver_i2c_read_register(I2C_Index, TargetAddress,Register,RegisterType,Buffer)

/** Reads amount of data bytes from addressed target device.
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param I2C_Index      device index of I2C to init (1..ttc_i2c_get_max_index() )
 * @param TargetAddress  receivers own TargetAddress
 * @param Register       index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Byte           pointer to start of 8 bit buffer where to store received bytes
 * @param Amount         amount of bytes to be read from target device
 * @param AmountRead     will be loaded with amount of bytes being read
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: one or more bytes have been read successfully; != 0: error-code
 */
ttc_i2c_errorcode_e _driver_i2c_read_registers(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, u8_t* Buffer, u16_t Amount);
#define ttc_i2c_read_registers(I2C_Index, TargetAddress,Register,RegisterType,Buffer,Amount) _driver_i2c_read_registers(I2C_Index, TargetAddress,Register,RegisterType,Buffer,Amount)


//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_I2C_H
