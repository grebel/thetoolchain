/** ttc_mutex.c ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Architecture independent support for inter task communication & synchronization.
 *
 */
#include "ttc_mutex.h"
#include "ttc_heap.h"
#include "ttc_interrupt.h"
//? #include "ttc_memory.h"

//{ global variables ********************************************

#if TTC_SMART_MUTEX_HISTORY_SIZE > 0

    // stores pointers to smart mutexes given for latest ttc_Mutexsmart_lock() calls
    t_ttc_mutex_smart* tml_LastSmartMutex[TTC_SMART_MUTEX_HISTORY_SIZE];

    // history of smart mutexes without doubles
    t_ttc_mutex_smart* tml_UniqueSmartMutex[TTC_SMART_MUTEX_HISTORY_SIZE];

    // index of next valid entry inside tml_LastSmartMutex[]
    t_u8 tml_Index_LastSmartMutex = 0;
    t_u8 tml_Index_UniqueSmartMutex = 0;

    t_u32 tml_AmountSmartLocks = 0;
#endif

//}global variables
//{ private functions *************************************************

t_ttc_mutex* _ttc_mutex_create() {
#ifdef _driver_mutex_create
    return _driver_mutex_create();
#else
    return NULL;
#endif
}
void _ttc_mutex_init( t_ttc_mutex* Lock ) {
#ifdef _driver_mutex_init
    _driver_mutex_init( Lock );
#endif
}
e_ttc_mutex_error _ttc_mutex_lock( t_ttc_mutex* Mutex, t_base TimeOut ) {
#  ifdef _driver_mutex_lock

    Assert_MUTEX_Writable( ( void* ) Mutex, ttc_assert_origin_auto );
    return _driver_mutex_lock( Mutex, TimeOut );
#  else
    return tme_NotImplemented;
#  endif
}
e_ttc_mutex_error _ttc_mutex_lock_isr( t_ttc_mutex* Mutex ) {

#ifdef _driver_mutex_lock_isr

    Assert_MUTEX_Writable( ( void* ) Mutex, ttc_assert_origin_auto );
    return _driver_mutex_lock_isr( Mutex );
#else
    return tme_NotImplemented;
#endif
}
BOOL _ttc_mutex_is_locked( t_ttc_mutex* Mutex ) {

#ifdef _driver_mutex_is_locked

    Assert_MUTEX_Writable( ( void* ) Mutex, ttc_assert_origin_auto );
    return _driver_mutex_is_locked( Mutex );
#else
    return tme_NotImplemented;
#endif
}
#if TTC_ASSERT_MUTEX == 1
void _ttc_mutex_unlock( t_ttc_mutex* Mutex ) {

#ifdef _driver_mutex_unlock

    Assert_MUTEX_Writable( ( void* ) Mutex, ttc_assert_origin_auto );
    _driver_mutex_unlock( Mutex );
#endif
}
void _ttc_mutex_unlock_isr( t_ttc_mutex* Mutex ) {

#ifdef _driver_mutex_unlock_isr

    Assert_MUTEX_Writable( ( void* ) Mutex, ttc_assert_origin_auto );
    _driver_mutex_unlock_isr( Mutex );
#endif
}
#endif

void _ttc_mutex_wait() {
#ifdef TTC_MULTITASKING_SCHEDULER
    if ( ttc_task_is_inside_critical_section() ) {
        t_u8 Memo;
        ttc_task_critical_all_end( &Memo );
        ttc_task_yield();
        ttc_task_critical_all_restore( Memo );
    }
    else {
#endif
        ttc_task_yield();
#ifdef TTC_MULTITASKING_SCHEDULER
    }
#endif
}

#if TTC_SMART_MUTEXES == 1
t_ttc_mutex_smart* _ttc_mutex_smart_create() {

    t_ttc_mutex_smart* NewMutex = ttc_heap_alloc( sizeof( t_ttc_mutex_smart ) );
    _ttc_mutex_smart_init( NewMutex );

    return NewMutex;
}
void _ttc_mutex_smart_init( t_ttc_mutex_smart* Mutex ) {

    _ttc_mutex_init( &( Mutex->Mutex ) );
    Mutex->LastCaller = NULL;
}
e_ttc_mutex_error _ttc_mutex_smart_lock( t_ttc_mutex_smart* Mutex, t_base TimeOut ) {
    Assert_MUTEX_Writable( ( void* ) Mutex, ttc_assert_origin_auto );

#if TTC_MutexTIMEOUT_USECS > 0
    if ( TimeOut == -1 )
    { TimeOut = TTC_MutexTIMEOUT_USECS; }
#endif


#if TTC_SMART_MUTEX_HISTORY_SIZE > 0
    tml_AmountSmartLocks++;
    tml_LastSmartMutex[tml_Index_LastSmartMutex++] = SmartMutex;
    if ( tml_Index_LastSmartMutex >= TTC_SMART_MUTEX_HISTORY_SIZE )
    { tml_Index_LastSmartMutex = 0; }

    BOOL MutexIsKnown = FALSE;
    for ( int I = 0; ( I < tml_Index_UniqueSmartMutex ) && ( I < TTC_SMART_MUTEX_HISTORY_SIZE ); I++ ) {
        if ( tml_UniqueSmartMutex[I] == SmartMutex ) {
            MutexIsKnown = TRUE;
            break;
        }
    }
    if ( ( !MutexIsKnown ) && ( tml_Index_UniqueSmartMutex < TTC_SMART_MUTEX_HISTORY_SIZE ) )
    { tml_UniqueSmartMutex[tml_Index_UniqueSmartMutex++] = SmartMutex; }
#endif

    e_ttc_mutex_error Error = _ttc_mutex_lock( &( Mutex->Mutex ), TimeOut );
    if ( Error == tme_OK ) {
        // copy info about current task into smart mutex
        if ( ttc_task_is_scheduler_started() ) {
            Mutex->OwnerTask = ttc_task_update_info( NULL );
            Assert_MUTEX( ( t_base ) Mutex->OwnerTask->Handle != 0xa5a5a5a5, ttc_assert_origin_auto );
        }

        // use return address as reference (next line of code after lock() call )
        Mutex->LastCaller = __builtin_return_address( 0 );
    }
#if TTC_MutexTIMEOUT_USECS > 0
    if ( Error == tme_TimeOut ) {
        Assert_MUTEX( TimeOut != TTC_MutexTIMEOUT_USECS, ttc_assert_origin_auto );  // endless mutex timed out!
    }
#endif

    return Error;
}
e_ttc_mutex_error _ttc_mutex_smart_lock_isr( t_ttc_mutex_smart* Mutex ) {
    Assert_MUTEX_Writable( ( void* ) Mutex, ttc_assert_origin_auto );

#if TTC_SMART_MUTEX_HISTORY_SIZE > 0
    tml_AmountSmartLocks++;
    tml_LastSmartMutex[tml_Index_LastSmartMutex++] = SmartMutex;
    if ( tml_Index_LastSmartMutex >= TTC_SMART_MUTEX_HISTORY_SIZE )
    { tml_Index_LastSmartMutex = 0; }

    BOOL MutexIsKnown = FALSE;
    for ( int I = 0; ( I < tml_Index_UniqueSmartMutex ) && ( I < TTC_SMART_MUTEX_HISTORY_SIZE ); I++ ) {
        if ( tml_UniqueSmartMutex[I] == SmartMutex ) {
            MutexIsKnown = TRUE;
            break;
        }
    }
    if ( ( !MutexIsKnown ) && ( tml_Index_UniqueSmartMutex < TTC_SMART_MUTEX_HISTORY_SIZE ) )
    { tml_UniqueSmartMutex[tml_Index_UniqueSmartMutex++] = SmartMutex; }
#endif

    e_ttc_mutex_error Error = _ttc_mutex_lock_isr( &( Mutex->Mutex ) );
    if ( Error == tme_OK ) {
        // copy info about current task into smart mutex
        Mutex->OwnerTask = NULL;

        // use return address as reference (next line of code after lock() call )
        Mutex->LastCaller = __builtin_return_address( 0 );
    }

    return Error;
}
void _ttc_mutex_smart_unlock( t_ttc_mutex_smart* Mutex ) {
    Assert_MUTEX_Writable( ( void* ) Mutex, ttc_assert_origin_auto );

    Mutex->LastCaller = __builtin_return_address( 0 );

    _ttc_mutex_unlock( &( Mutex->Mutex ) );
}
void _ttc_mutex_smart_unlock_isr( t_ttc_mutex_smart* Mutex ) {
    Assert_MUTEX_Writable( ( void* ) Mutex, ttc_assert_origin_auto );

    Mutex->LastCaller = __builtin_return_address( 0 );

    _ttc_mutex_unlock_isr( &( Mutex->Mutex ) );
}
BOOL _ttc_mutex_smart_is_locked( t_ttc_mutex_smart* Mutex ) {
    Assert_MUTEX_Writable( ( void* ) Mutex, ttc_assert_origin_auto );

    return _ttc_mutex_is_locked( &( Mutex->Mutex ) );
}
#endif

//}functions
