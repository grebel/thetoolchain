/*{ stm32_gui::olimex_320x240_lcd_driver.c *******************************



}*/
//{ Defines/ TypDefs *****************************************************
//} Defines

#include "ttc_gfx.h"

//{ Global Variables *****************************************************

// for each initialized gfx, a pointer to its generic and architecture definitions is stored
ttc_heap_array_define(ttc_gfx_generic_t*, ttc_GFXs, TTC_AMOUNT_GFXS);

// logical index of display to use by ttc_gfx_stdout_send_string()/ ttc_gfx_stdout_send_block()
u8_t ttc_gfx_stdout_index = 0;

// updated by _ttc_gfx_update_configuration()
ttc_gfx_generic_t* ttc_gfx_CurrentDisplay = NULL;

// updated by ttc_gfx_display()
u16_t ttc_gfx_CurrentColorFg = 0;
u16_t ttc_gfx_CurrentColorBg = 0;

// function pointer for current display
void (*function_set_color_fg24)(u32_t Color);
void (*function_set_color_bg24)(u32_t Color);

// functions that can be provided by low-level driver (set during ttc_gfx_reset() )
void (*function_circle)(u16_t CenterX, u16_t CenterY, u16_t Radius)                                           = _ttc_gfx_generic_circle;
void (*function_circle_segment)(u16_t CenterX, u16_t CenterY, u16_t Radius, u16_t StartAngle, u16_t EndAngle) = _ttc_gfx_generic_circle_segment;
void (*function_circle_fill)(u16_t CenterX, u16_t CenterY, u16_t Radius)                                      = _ttc_gfx_generic_circle_fill;
void (*function_clear)()                                                                                      = _ttc_gfx_generic_clear;
void (*function_line)(u16_t X1, u16_t Y1, u16_t X2, u16_t Y2)                                                 = _ttc_gfx_generic_line;
void (*function_line_horizontal)(u16_t X, u16_t Y, s16_t Length)                                              = _ttc_gfx_generic_line_horizontal;
void (*function_line_vertical)(u16_t X, u16_t Y, s16_t Length)                                                = _ttc_gfx_generic_line_vertical;
void (*function_rect)(u16_t X,u16_t Y, s16_t Width, s16_t Height)                                             = _ttc_gfx_generic_rect;
void (*function_rect_fill)(u16_t X, u16_t Y, s16_t Width, s16_t Height)                                       = _ttc_gfx_generic_rect_fill;
void (*function_set_backlight)(u8_t Level)                                                                    = _ttc_gfx_generic_set_backlight;



#ifdef EXTENSION_510_ttc_font  // font handling functions
void (*function_put_char)(u8_t Char)                                                                          = _ttc_gfx_generic_put_char;
void (*function_put_char_solid)(u8_t Char)                                                                    = _ttc_gfx_generic_put_char_solid;
#endif

//} Global Variables


//{ Function definitions *************************************************

u8_t ttc_gfx_get_max_display_index() {
    return TTC_AMOUNT_GFXS;
}
ttc_gfx_generic_t* ttc_gfx_get_configuration(u8_t DisplayIndex) {
    _ttc_gfx_update_configuration(DisplayIndex);

    return ttc_gfx_CurrentDisplay;
}
void ttc_gfx_init(u8_t DisplayIndex) {
    _ttc_gfx_update_configuration(DisplayIndex);

    switch (ttc_gfx_CurrentDisplay->Driver) {
#ifdef EXTENSION_400_lcd_320x240_ili9320
    case tgd_ILI9320: ili9320_init(ttc_gfx_CurrentDisplay); break;
#endif
    default: break;
    }

    ttc_gfx_display(DisplayIndex);  // switch to display

#ifdef EXTENSION_510_ttc_font
    // fonts available: load first font as default
    ttc_font_init();
    ttc_gfx_set_font(ttc_font_get_configuration(1));
#endif

    ttc_gfx_clear();                // first clear
}
void ttc_gfx_display(u8_t DisplayIndex) {
    _ttc_gfx_update_configuration(DisplayIndex);

    // load function pointer into global variables for faster access
    function_circle          = ttc_gfx_CurrentDisplay->function_circle;
    function_circle_segment  = ttc_gfx_CurrentDisplay->function_circle_segment;
    function_circle_fill     = ttc_gfx_CurrentDisplay->function_circle_fill;
    function_clear           = ttc_gfx_CurrentDisplay->function_clear;
    function_line            = ttc_gfx_CurrentDisplay->function_line;
    function_line_horizontal = ttc_gfx_CurrentDisplay->function_line_horizontal;
    function_line_vertical   = ttc_gfx_CurrentDisplay->function_line_vertical;
    function_rect            = ttc_gfx_CurrentDisplay->function_rect;
    function_rect_fill       = ttc_gfx_CurrentDisplay->function_rect_fill;
    function_set_backlight   = ttc_gfx_CurrentDisplay->function_set_backlight;

#ifdef EXTENSION_510_ttc_font  // font attributes
    function_put_char        = ttc_gfx_CurrentDisplay->function_put_char;
    function_put_char_solid  = ttc_gfx_CurrentDisplay->function_put_char_solid;
#endif

    ttc_gfx_CurrentColorFg = ttc_gfx_CurrentDisplay->ColorFg;
    ttc_gfx_CurrentColorBg = ttc_gfx_CurrentDisplay->ColorBg;
}
void ttc_gfx_image(u16_t X, u16_t Y, gfxImage *Image) {

    GFX_STATIC u16_t tgi_DX, tgi_DY;
    GFX_STATIC u32_t tgi_Pixel = 0;

    switch (Image->Depth) {
    case 24: {
        for(tgi_DX = 0; tgi_DX < Image->Width; ++tgi_DX) {
            for(tgi_DY = 0; tgi_DY < Image->Height; ++tgi_DY) {
                tgi_Pixel = gfx_image_get_pixel_24bit(tgi_DX, tgi_DY, Image);
                ttc_gfx_set_color_fg24(tgi_Pixel);
                _ttc_gfx_driver_set_pixel_at(X + tgi_DX, Y + tgi_DY);
            }
        }
        break;
    }
    case 16: {
        for(tgi_DX = 0; tgi_DX < Image->Width; ++tgi_DX) {
            for(tgi_DY = 0; tgi_DY < Image->Height; ++tgi_DY) {
                tgi_Pixel = gfx_image_get_pixel_16bit(tgi_DX, tgi_DY, Image);
                ttc_gfx_set_color_fg24(tgi_Pixel);
                _ttc_gfx_driver_set_pixel_at(X + tgi_DX, Y + tgi_DY);
            }
        }
        break;
    }
    case 8: {
        for(tgi_DX = 0; tgi_DX < Image->Width; ++tgi_DX) {
            for(tgi_DY = 0; tgi_DY < Image->Height; ++tgi_DY) {
                tgi_Pixel = gfx_image_get_pixel_8bit(tgi_DX, tgi_DY, Image);
                ttc_gfx_set_color_fg_palette((u8_t) tgi_Pixel);
                _ttc_gfx_driver_set_pixel_at(X + tgi_DX, Y + tgi_DY);
            }
        }
        break;
    }
    case 1: {
        for(tgi_DX = 0; tgi_DX < Image->Width; ++tgi_DX) {
            for(tgi_DY = 0; tgi_DY < Image->Height; ++tgi_DY) {
                if (gfx_image_get_pixel_1bit(tgi_DX, tgi_DY, Image)) {
                    _ttc_gfx_driver_set_pixel_at(X + tgi_DX, Y + tgi_DY);
                }
                else {
                    _ttc_gfx_driver_clr_pixel_at(X + tgi_DX, Y + tgi_DY);
                }
            }
        }
        break;
    }
    }
}
void ttc_gfx_missing_implementation(u16_t X, u16_t Y) {
    (void) X;
    (void) Y;

    Assert(1, ec_NotImplemented);
}
void ttc_gfx_reset(u8_t DisplayIndex) {
    ttc_memory_set( ttc_gfx_CurrentDisplay, 0, sizeof(ttc_gfx_generic_t) );

    // load generic default values
    ttc_gfx_CurrentDisplay->LogicalIndex = DisplayIndex;

#ifdef EXTENSION_510_ttc_font  // font attributes
    ttc_gfx_CurrentDisplay->Font     = NULL;
    ttc_gfx_CurrentDisplay->TextMinX = 0;
    ttc_gfx_CurrentDisplay->TextMinY = 0;

    if (ttc_gfx_CurrentDisplay->Font) {
        if (ttc_gfx_CurrentDisplay->Font->CharWidth)
            ttc_gfx_CurrentDisplay->TextColumns  = ttc_gfx_CurrentDisplay->Width  / ttc_gfx_CurrentDisplay->Font->CharWidth;

        if (ttc_gfx_CurrentDisplay->Font->CharHeight)
            ttc_gfx_CurrentDisplay->TextRows     = ttc_gfx_CurrentDisplay->Height / ttc_gfx_CurrentDisplay->Font->CharHeight;
    }
#endif

    switch (DisplayIndex) { // load settings from makefile
#ifdef TTC_GFX1_DRIVER
    case 1: {
        ttc_gfx_CurrentDisplay->Driver = TTC_GFX1_DRIVER;
#ifdef TTC_GFX1_WIDTH
        ttc_gfx_CurrentDisplay->Width = TTC_GFX1_WIDTH;
#endif
#ifdef TTC_GFX1_HEIGHT
        ttc_gfx_CurrentDisplay->Height = TTC_GFX1_HEIGHT;
#endif
#ifdef TTC_GFX1_DEPTH
        ttc_gfx_CurrentDisplay->Depth = TTC_GFX1_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_GFX2_DRIVER
    case 2: {
        ttc_gfx_CurrentDisplay->Driver = TTC_GFX2_DRIVER;
#ifdef TTC_GFX2_WIDTH
        ttc_gfx_CurrentDisplay->Width = TTC_GFX2_WIDTH;
#endif
#ifdef TTC_GFX2_HEIGHT
        ttc_gfx_CurrentDisplay->Height = TTC_GFX2_HEIGHT;
#endif
#ifdef TTC_GFX2_DEPTH
        ttc_gfx_CurrentDisplay->Depth = TTC_GFX2_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_GFX3_DRIVER
    case 3: {
        ttc_gfx_CurrentDisplay->Driver = TTC_GFX3_DRIVER;
#ifdef TTC_GFX3_WIDTH
        ttc_gfx_CurrentDisplay->Width = TTC_GFX3_WIDTH;
#endif
#ifdef TTC_GFX3_HEIGHT
        ttc_gfx_CurrentDisplay->Height = TTC_GFX3_HEIGHT;
#endif
#ifdef TTC_GFX3_DEPTH
        ttc_gfx_CurrentDisplay->Depth = TTC_GFX3_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_GFX4_DRIVER
    case 4: {
        ttc_gfx_CurrentDisplay->Driver = TTC_GFX4_DRIVER:
        #ifdef TTC_GFX4_WIDTH
                ttc_gfx_CurrentDisplay->Width = TTC_GFX4_WIDTH;
#endif
#ifdef TTC_GFX4_HEIGHT
        ttc_gfx_CurrentDisplay->Height = TTC_GFX4_HEIGHT;
#endif
#ifdef TTC_GFX4_DEPTH
        ttc_gfx_CurrentDisplay->Depth = TTC_GFX4_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_GFX5_DRIVER
    case 5: {
        ttc_gfx_CurrentDisplay->Driver = TTC_GFX5_DRIVER;
#ifdef TTC_GFX5_WIDTH
        ttc_gfx_CurrentDisplay->Width = TTC_GFX5_WIDTH;
#endif
#ifdef TTC_GFX5_HEIGHT
        ttc_gfx_CurrentDisplay->Height = TTC_GFX5_HEIGHT;
#endif
#ifdef TTC_GFX5_DEPTH
        ttc_gfx_CurrentDisplay->Depth = TTC_GFX5_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_GFX6_DRIVER
    case 6: {
        ttc_gfx_CurrentDisplay->Driver = TTC_GFX6_DRIVER;
#ifdef TTC_GFX6_WIDTH
        ttc_gfx_CurrentDisplay->Width = TTC_GFX6_WIDTH;
#endif
#ifdef TTC_GFX6_HEIGHT
        ttc_gfx_CurrentDisplay->Height = TTC_GFX6_HEIGHT;
#endif
#ifdef TTC_GFX6_DEPTH
        ttc_gfx_CurrentDisplay->Depth = TTC_GFX6_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_GFX7_DRIVER
    case 7: {
        ttc_gfx_CurrentDisplay->Driver = TTC_GFX7_DRIVER;
#ifdef TTC_GFX7_WIDTH
        ttc_gfx_CurrentDisplay->Width = TTC_GFX7_WIDTH;
#endif
#ifdef TTC_GFX7_HEIGHT
        ttc_gfx_CurrentDisplay->Height = TTC_GFX7_HEIGHT;
#endif
#ifdef TTC_GFX7_DEPTH
        ttc_gfx_CurrentDisplay->Depth = TTC_GFX7_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_GFX8_DRIVER
    case 8: {
        ttc_gfx_CurrentDisplay->Driver = TTC_GFX8_DRIVER;
#ifdef TTC_GFX8_WIDTH
        ttc_gfx_CurrentDisplay->Width = TTC_GFX8_WIDTH;
#endif
#ifdef TTC_GFX8_HEIGHT
        ttc_gfx_CurrentDisplay->Height = TTC_GFX8_HEIGHT;
#endif
#ifdef TTC_GFX8_DEPTH
        ttc_gfx_CurrentDisplay->Depth = TTC_GFX8_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_GFX9_DRIVER
    case 9: {
        ttc_gfx_CurrentDisplay->Driver = TTC_GFX9_DRIVER;
#ifdef TTC_GFX9_WIDTH
        ttc_gfx_CurrentDisplay->Width = TTC_GFX9_WIDTH;
#endif
#ifdef TTC_GFX9_HEIGHT
        ttc_gfx_CurrentDisplay->Height = TTC_GFX9_HEIGHT;
#endif
#ifdef TTC_GFX9_DEPTH
        ttc_gfx_CurrentDisplay->Depth = TTC_GFX9_DEPTH;
#endif
        break;
    }
#endif

    }

    // generic implementations may be exchanged by low-level driver
    ttc_gfx_CurrentDisplay->function_circle          = _ttc_gfx_generic_circle;
    ttc_gfx_CurrentDisplay->function_circle_fill         = _ttc_gfx_generic_circle_fill;
    ttc_gfx_CurrentDisplay->function_circle_segment  = _ttc_gfx_generic_circle_segment;
    ttc_gfx_CurrentDisplay->function_clear           = _ttc_gfx_generic_clear;
    ttc_gfx_CurrentDisplay->function_line            = _ttc_gfx_generic_line;
    ttc_gfx_CurrentDisplay->function_line_horizontal = _ttc_gfx_generic_line_horizontal;
    ttc_gfx_CurrentDisplay->function_line_vertical   = _ttc_gfx_generic_line_vertical;
#ifdef EXTENSION_510_ttc_font  // font handling functions
    ttc_gfx_CurrentDisplay->function_put_char        = _ttc_gfx_generic_put_char;
    ttc_gfx_CurrentDisplay->function_put_char_solid  = _ttc_gfx_generic_put_char_solid;
#endif
    ttc_gfx_CurrentDisplay->function_rect            = _ttc_gfx_generic_rect;
    ttc_gfx_CurrentDisplay->function_rect_fill       = _ttc_gfx_generic_rect_fill;
    ttc_gfx_CurrentDisplay->function_set_backlight   = _ttc_gfx_generic_set_backlight;

    switch (ttc_gfx_CurrentDisplay->Driver) { // load rest of configuration from low-level driver
#ifdef EXTENSION_400_lcd_320x240_ili9320
    case tgd_ILI9320:
        ili9320_get_configuration(ttc_gfx_CurrentDisplay);
        break;
#endif
#ifdef EXTENSION_400_lcd_320x240_K320QVB
    case tgd_K320QVD:
        ili9320_get_configuration(ttc_gfx_CurrentDisplay);
        // k320qvb_get_configuration(ttc_gfx_CurrentDisplay);
        break;
#endif
    default: Assert_Halt_EC(ec_NotImplemented); break;
    }

    // check for missing low-level implementations that have to be provided by driver
    Assert(ttc_gfx_CurrentDisplay->function_set_color_bg24, ec_NotImplemented);
    Assert(ttc_gfx_CurrentDisplay->function_set_color_fg24, ec_NotImplemented);

    ttc_gfx_set_color_fg24(GFX_COLOR24_BLACK);
    ttc_gfx_set_color_bg24(GFX_COLOR24_WHITE);
}
void ttc_gfx_set_color_bg24(u32_t Color) {
    Assert_GFX(ttc_gfx_CurrentDisplay->function_set_color_bg24, ec_NULL);
    ttc_gfx_CurrentDisplay->function_set_color_bg24(Color);
}
void ttc_gfx_set_color_bg16(u16_t Color) {
    ttc_gfx_CurrentDisplay->ColorBg = ttc_gfx_CurrentColorBg = Color;
}
void ttc_gfx_set_color_bg_palette(u8_t PaletteIndex) {
    Assert_GFX(ttc_gfx_CurrentDisplay->Palette, ec_InvalidConfiguration);
    Assert_GFX(ttc_gfx_CurrentDisplay->PaletteSize > PaletteIndex, ec_InvalidConfiguration);

    ttc_gfx_set_color_bg24(ttc_gfx_CurrentDisplay->Palette[PaletteIndex]);
}
void ttc_gfx_set_color_fg24(u32_t Color) {
    Assert_GFX(ttc_gfx_CurrentDisplay->function_set_color_fg24, ec_NULL);
    ttc_gfx_CurrentDisplay->function_set_color_fg24(Color);
}
void ttc_gfx_set_color_fg16(u16_t Color) {
    ttc_gfx_CurrentDisplay->ColorFg = ttc_gfx_CurrentColorFg = Color;
}
void ttc_gfx_set_color_fg_palette(u8_t PaletteIndex) {
    Assert_GFX(ttc_gfx_CurrentDisplay->Palette, ec_InvalidConfiguration);
    Assert_GFX(ttc_gfx_CurrentDisplay->PaletteSize > PaletteIndex, ec_InvalidConfiguration);

    ttc_gfx_set_color_fg24(ttc_gfx_CurrentDisplay->Palette[PaletteIndex]);
}
void ttc_gfx_set_cursor(u16_t X, u16_t Y) {

    if (X >= ttc_gfx_CurrentDisplay->Width)
        X = ttc_gfx_CurrentDisplay->Width - 1;
    if (Y >= ttc_gfx_CurrentDisplay->Height)
        Y = ttc_gfx_CurrentDisplay->Height - 1;

    _ttc_gfx_set_cursor(X, Y);
}
void ttc_gfx_set_pixel(u16_t X, u16_t Y) {

    ttc_gfx_CurrentDisplay->CursorX = X;
    ttc_gfx_CurrentDisplay->CursorY = Y;

    _ttc_gfx_driver_set_pixel_at(X, Y);
}
void ttc_gfx_set_palette(u16_t* Palette, u8_t Size) {

    ttc_gfx_CurrentDisplay->Palette     = Palette;
    ttc_gfx_CurrentDisplay->PaletteSize = Size;
}

#ifdef EXTENSION_510_ttc_font  // font handling functions

void ttc_gfx_print(const char* String, Base_t MaxSize) {
    Assert_GFX(ttc_gfx_CurrentDisplay->Font, ec_InvalidConfiguration); // no font has been registered/ activated!

    while (*String && MaxSize--) {
        ttc_gfx_put_char(*String++);
        ttc_gfx_text_cursor_right();
    }
}
void ttc_gfx_print_between(u16_t X, u16_t Y, const char* String, Base_t MaxSize)
{
    Assert_GFX(ttc_gfx_CurrentDisplay->Font, ec_InvalidConfiguration); // no font has been registered/ activated!
    ttc_gfx_CurrentDisplay->CursorX=X;
    ttc_gfx_CurrentDisplay->CursorY=Y;
    while (*String && MaxSize--) {
        ttc_gfx_put_char(*String++);
        ttc_gfx_CurrentDisplay->CursorX+=ttc_gfx_CurrentDisplay->Font->CharWidth;
    }
}

void ttc_gfx_print_solid_at(u16_t X, u16_t Y, const char* String, Base_t MaxSize) {
    ttc_gfx_text_cursor_set(X, Y);
    ttc_gfx_print_solid(String, MaxSize);
}


void ttc_gfx_print_at(u16_t X, u16_t Y, const char* String, Base_t MaxSize) {
    ttc_gfx_text_cursor_set(X, Y);
    ttc_gfx_print(String, MaxSize);
}

void ttc_gfx_print_solid(const char* String, Base_t MaxSize) {
    Assert_GFX(ttc_gfx_CurrentDisplay->Font, ec_InvalidConfiguration); // no font has been registered/ activated!

    while (*String && MaxSize--) {

        ttc_gfx_put_char_solid(*String++);
        ttc_gfx_text_cursor_right();
    }
}
void ttc_gfx_print_boxed(const char* String, Base_t MaxSize, u8_t InnerSpacing) {
    Assert_GFX(ttc_gfx_CurrentDisplay->Font, ec_InvalidConfiguration); // no font has been registered/ activated!

    GFX_STATIC u16_t tgpb_StartX; tgpb_StartX = ttc_gfx_CurrentDisplay->CursorX;
    GFX_STATIC u16_t tgpb_StartY; tgpb_StartY = ttc_gfx_CurrentDisplay->CursorY;

    ttc_gfx_set_cursor(tgpb_StartX + InnerSpacing, tgpb_StartY + InnerSpacing);

    ttc_gfx_print(String, MaxSize);

    ttc_gfx_rect(tgpb_StartX, tgpb_StartY, ttc_gfx_CurrentDisplay->CursorX - tgpb_StartX + 2* InnerSpacing, 24 + 2 * InnerSpacing);
    ttc_gfx_set_cursor(ttc_gfx_CurrentDisplay->CursorX + 2 * InnerSpacing, tgpb_StartY);
}
void ttc_gfx_print_block(ttc_heap_block_t* Block) {
    Assert_GFX(Block,         ec_NULL);
    Assert_GFX(Block->Buffer, ec_NULL);
    Assert_GFX(ttc_gfx_CurrentDisplay->Font, ec_InvalidConfiguration); // no font has been registered/ activated!

    GFX_STATIC TTC_MEMORY_BLOCK_BASE tgpb2_Amount; tgpb2_Amount = Block->Size;
    GFX_STATIC u8_t* tgpb2_Reader;                 tgpb2_Reader = Block->Buffer;

    while (tgpb2_Amount--) {
        ttc_gfx_put_char(*tgpb2_Reader++);
        ttc_gfx_text_cursor_right();
    }
}
void ttc_gfx_print_block_solid(ttc_heap_block_t* Block) {
    Assert_GFX(Block,         ec_NULL);
    Assert_GFX(Block->Buffer, ec_NULL);
    Assert_GFX(ttc_gfx_CurrentDisplay->Font, ec_InvalidConfiguration); // no font has been registered/ activated!

    GFX_STATIC TTC_MEMORY_BLOCK_BASE tgpbs_Amount; tgpbs_Amount = Block->Size;
    GFX_STATIC u8_t* tgpbs_Reader;                 tgpbs_Reader = Block->Buffer;

    while (tgpbs_Amount--) {
        ttc_gfx_put_char_solid(*tgpbs_Reader++);
        ttc_gfx_text_cursor_right();
    }
}
void ttc_gfx_set_font(const ttc_font_data_t* Font) {

    ttc_gfx_CurrentDisplay->Font = Font;
    ttc_gfx_CurrentDisplay->TextColumns  = ttc_gfx_CurrentDisplay->Width  / Font->CharWidth;
    ttc_gfx_CurrentDisplay->TextRows     = ttc_gfx_CurrentDisplay->Height / Font->CharHeight;
}
void ttc_gfx_text_cursor_set(u16_t X, u16_t Y) {

    // keep coordinate within screen
    if (X >= ttc_gfx_CurrentDisplay->TextColumns)
        X = ttc_gfx_CurrentDisplay->TextColumns - 1;
    if (Y >= ttc_gfx_CurrentDisplay->TextRows)
        Y = ttc_gfx_CurrentDisplay->TextRows - 1;

    ttc_gfx_CurrentDisplay->TextX = X;
    ttc_gfx_CurrentDisplay->TextY = Y;

    // calculate pixel coordinate
    X *= ttc_gfx_CurrentDisplay->Font->CharWidth;
    Y *= ttc_gfx_CurrentDisplay->Font->CharHeight;

    X += ttc_gfx_CurrentDisplay->TextMinX;
    Y += ttc_gfx_CurrentDisplay->TextMinY;

    ttc_gfx_set_cursor(X, Y);
}
void ttc_gfx_text_cursor_right() {

    ttc_gfx_CurrentDisplay->TextX++;

    if (ttc_gfx_CurrentDisplay->TextX >= ttc_gfx_CurrentDisplay->TextColumns) {
        if (ttc_gfx_CurrentDisplay->TextY < ttc_gfx_CurrentDisplay->TextRows) {
            ttc_gfx_CurrentDisplay->TextX = 0;
            ttc_gfx_CurrentDisplay->TextY++;
        }
        else ttc_gfx_CurrentDisplay->TextX--;
    }
    ttc_gfx_text_cursor_set(ttc_gfx_CurrentDisplay->TextX, ttc_gfx_CurrentDisplay->TextY);
}
void ttc_gfx_text_cursor_left() {

    if (ttc_gfx_CurrentDisplay->TextX == 0) {
        if (ttc_gfx_CurrentDisplay->TextY > 0) {
            ttc_gfx_CurrentDisplay->TextY--;
        }
    }
    else {
        ttc_gfx_CurrentDisplay->TextX--;
    }
    ttc_gfx_text_cursor_set(ttc_gfx_CurrentDisplay->TextX, ttc_gfx_CurrentDisplay->TextY);
}
void ttc_gfx_text_cursor_up() {

    if (ttc_gfx_CurrentDisplay->TextY > 0) {
        ttc_gfx_CurrentDisplay->TextY--;
    }
    ttc_gfx_text_cursor_set(ttc_gfx_CurrentDisplay->TextX, ttc_gfx_CurrentDisplay->TextY);
}
void ttc_gfx_text_cursor_down() {

    if (ttc_gfx_CurrentDisplay->TextY < ttc_gfx_CurrentDisplay->TextRows) {
        ttc_gfx_CurrentDisplay->TextY++;
    }
    ttc_gfx_text_cursor_set(ttc_gfx_CurrentDisplay->TextX, ttc_gfx_CurrentDisplay->TextY);
}
u16_t ttc_gfx_text_center(const char* String, Base_t MaxSize) {

    s16_t StartPos = ( ttc_gfx_CurrentDisplay->TextColumns - ttc_string_length16(String, MaxSize) ) / 2;
    if (StartPos < 0) StartPos = 0;

    return StartPos;
}

// stdout implementation for ttc_string.c
void ttc_gfx_stdout_set(u8_t DisplayIndex) {
    ttc_gfx_display(DisplayIndex);

    ttc_gfx_stdout_index = DisplayIndex;
}
void ttc_gfx_stdout_print_block(ttc_heap_block_t* Block) {
    if (ttc_gfx_stdout_index) {
        if (ttc_gfx_CurrentDisplay->LogicalIndex != ttc_gfx_stdout_index)
            ttc_gfx_display(ttc_gfx_stdout_index); // switch to display registered for stdout

        ttc_gfx_print_block(Block);
    }
}
void ttc_gfx_stdout_print_string(const u8_t* String, Base_t MaxSize) {

    if (ttc_gfx_stdout_index) {
        ttc_gfx_print_solid( (const char*) String, MaxSize);
    }
}
#else

// declare dummy functions to avoid compilation issues
void ttc_gfx_print(const char* String, Base_t MaxSize) {}
void ttc_gfx_print_between(u16_t X, u16_t Y, const char* String, Base_t MaxSize) {}
void ttc_gfx_print_solid_at(u16_t X, u16_t Y, const char* String, Base_t MaxSize) {}
void ttc_gfx_print_at(u16_t X, u16_t Y, const char* String, Base_t MaxSize) {}
void ttc_gfx_print_solid(const char* String, Base_t MaxSize) {}
void ttc_gfx_print_boxed(const char* String, Base_t MaxSize, u8_t InnerSpacing) {}
void ttc_gfx_print_block(ttc_heap_block_t* Block) {}
void ttc_gfx_print_block_solid(ttc_heap_block_t* Block) {}
void ttc_gfx_set_font(const ttc_font_data_t* Font) {}
void ttc_gfx_text_cursor_set(u16_t X, u16_t Y) {}
void ttc_gfx_text_cursor_right() {}
void ttc_gfx_text_cursor_left() {}
void ttc_gfx_text_cursor_up() {}
void ttc_gfx_text_cursor_down() {}
u16_t ttc_gfx_text_center(const char* String, Base_t MaxSize) { return 0; }
void ttc_gfx_stdout_set(u8_t DisplayIndex) {}
void ttc_gfx_stdout_print_block(ttc_heap_block_t* Block) {}
void ttc_gfx_stdout_print_string(const u8_t* String, Base_t MaxSize) { }

#endif

// private functions (do not call from outside!)
void _ttc_gfx_update_configuration(u8_t DisplayIndex) {
    //X Assert_GFX(DisplayIndex > 0, ec_InvalidArgument);
    ttc_gfx_CurrentDisplay = A(ttc_GFXs, DisplayIndex-1); // will assert if outside array bounds

    if (!ttc_gfx_CurrentDisplay) {
        ttc_gfx_CurrentDisplay = A(ttc_GFXs, DisplayIndex-1) = ttc_heap_alloc_zeroed(sizeof(ttc_gfx_generic_t));

        ttc_gfx_reset(DisplayIndex);
    }
}

// generic implementations (used if no special low-level implementation is available)

#ifdef EXTENSION_510_ttc_font  // font handling functions
void _ttc_gfx_generic_put_char(u8_t Char) {
    GFX_STATIC u16_t tgpc_BitLine;
    GFX_STATIC  u8_t tgpc_LineNo;
    GFX_STATIC u16_t tgpc_X;
    GFX_STATIC u16_t tgpc_Y;
    GFX_STATIC const u16_t* tgpc_Bitmap;

    tgpc_Y = ttc_gfx_CurrentDisplay->CursorY;
    Char -= ttc_gfx_CurrentDisplay->Font->FirstASCII;
    tgpc_Bitmap = ttc_gfx_CurrentDisplay->Font->Characters + (Char) * 24;

    for (tgpc_LineNo = 0; tgpc_LineNo < 24; tgpc_LineNo++)
    {
        tgpc_BitLine = *tgpc_Bitmap++;

        if (tgpc_BitLine) {
            tgpc_X = ttc_gfx_CurrentDisplay->CursorX;
            do {
                if (tgpc_BitLine & 1) {
                    _ttc_gfx_driver_set_pixel_at(tgpc_X, tgpc_Y);
                }
                tgpc_X++;
                tgpc_BitLine /= 2;
            } while (tgpc_BitLine);
        }
        tgpc_Y++;
    }
}
void _ttc_gfx_generic_put_char_solid(u8_t Char) {

    GFX_STATIC u16_t tggpcs_BitLine;
    GFX_STATIC  u8_t tggpcs_LineNo;
    GFX_STATIC  u8_t tggpcs_BitCount;
    GFX_STATIC u16_t tggpcs_X;
    GFX_STATIC u16_t tggpcs_Y;              tggpcs_Y = ttc_gfx_CurrentDisplay->CursorY;
    GFX_STATIC const u16_t* tggpcs_Bitmap; tggpcs_Bitmap = ttc_gfx_CurrentDisplay->Font->Characters + (Char - 32) * 24;

    for (tggpcs_LineNo = 0; tggpcs_LineNo < 24; tggpcs_LineNo++)
    {
        tggpcs_BitLine = *tggpcs_Bitmap++;
        tggpcs_X = ttc_gfx_CurrentDisplay->CursorX;

        for (tggpcs_BitCount = 16; tggpcs_BitCount > 0; tggpcs_BitCount--) {
            if (tggpcs_BitLine & 1) {
                _ttc_gfx_driver_set_pixel_at(tggpcs_X, tggpcs_Y);
            }
            else {
                _ttc_gfx_driver_clr_pixel_at(tggpcs_X, tggpcs_Y);
            }
            tggpcs_X++;
            tggpcs_BitLine /= 2;
        }
        tggpcs_Y++;
    }
}
#endif

void _ttc_gfx_generic_clear() {
    _ttc_gfx_set_cursor(0, 0);
    GFX_STATIC u16_t tggc_Index;
    GFX_STATIC u16_t tggc_Length;
    GFX_STATIC void (*tggc_draw_line)(u16_t X, u16_t Y, s16_t Length) = NULL;

    // want to clear in background color
    ttc_gfx_CurrentColorFg = ttc_gfx_CurrentDisplay->ColorBg;

    // use clear direction with less iterations in outer loop
    if (ttc_gfx_CurrentDisplay->Width < ttc_gfx_CurrentDisplay->Height) {
        tggc_draw_line = ttc_gfx_CurrentDisplay->function_line_vertical;

        tggc_Length = ttc_gfx_CurrentDisplay->Height;
        for (tggc_Index = ttc_gfx_CurrentDisplay->Width - 1; tggc_Index > 0; tggc_Index--)
            tggc_draw_line(tggc_Index, 0, tggc_Length);
    }
    else {
        tggc_draw_line = ttc_gfx_CurrentDisplay->function_line_horizontal;

        tggc_Length = ttc_gfx_CurrentDisplay->Width;
        for (tggc_Index = ttc_gfx_CurrentDisplay->Height - 1; tggc_Index > 0; tggc_Index--)
            tggc_draw_line(0, tggc_Index, tggc_Length);
    }

    // restore old foreground color
    ttc_gfx_CurrentColorFg = ttc_gfx_CurrentDisplay->ColorFg;
}
void _ttc_gfx_generic_line(u16_t X1, u16_t Y1, u16_t X2, u16_t Y2) {
    if (X2 == X1) {
        function_line_vertical(X1, Y1, Y2);
        return;
    }
    if (Y2 == Y1) {
        function_line_horizontal(X1, Y1, X2);
        return;
    }

    GFX_STATIC u16_t tggl_T;
    GFX_STATIC s16_t tggl_XError, tggl_YError, tggl_DeltaX, tggl_DeltaY, tggl_Distance;
    GFX_STATIC s16_t tggl_IncrementX, tggl_IncrementY;

    tggl_XError = 0;
    tggl_YError = 0;
    tggl_DeltaX = X2 - X1;
    tggl_DeltaY = Y2 - Y1;

    if (tggl_DeltaX > 0)
        tggl_IncrementX = 1;
    else if (tggl_DeltaX == 0)
        tggl_IncrementX = 0;
    else
    { tggl_IncrementX = -1; tggl_DeltaX = -tggl_DeltaX; }

    if (tggl_DeltaY > 0)
        tggl_IncrementY = 1;
    else if (tggl_DeltaY == 0)
        tggl_IncrementY = 0;
    else
    { tggl_IncrementY = -1; tggl_DeltaY = -tggl_DeltaY; }

    if ( tggl_DeltaX > tggl_DeltaY )
        tggl_Distance = tggl_DeltaX;
    else
        tggl_Distance = tggl_DeltaY;

    for (tggl_T = 0; tggl_T <= tggl_Distance + 1; tggl_T++) {
        _ttc_gfx_driver_set_pixel_at(X1, Y1);
        tggl_XError += tggl_DeltaX ;
        tggl_YError += tggl_DeltaY ;
        if (tggl_XError > tggl_Distance) {
            tggl_XError -= tggl_Distance;
            X1 += tggl_IncrementX;
        }
        if (tggl_YError > tggl_Distance) {
            tggl_YError -= tggl_Distance;
            Y1 += tggl_IncrementY;
        }
    }
}
void _ttc_gfx_generic_line_horizontal(u16_t X, u16_t Y, s16_t Length) {

    if (Length < 0) {
        while (Length++ < 0) {
            _ttc_gfx_driver_set_pixel_at(X--, Y);
        }
    }
    else {
        while (Length-- > 0) {
            _ttc_gfx_driver_set_pixel_at(X++, Y);
        }
    }
}
void _ttc_gfx_generic_line_vertical(u16_t X, u16_t Y, s16_t Length) {

    if (Length < 0) {
        while (Length++ < 0) {
            _ttc_gfx_driver_set_pixel_at(X, Y--);
        }
    }
    else {
        while (Length-- > 0) {
            _ttc_gfx_driver_set_pixel_at(X, Y++);
        }
    }
}
void _ttc_gfx_generic_rect(u16_t X,u16_t Y, s16_t Width, s16_t Height) {

    function_line_horizontal(X, Y, Width);
    function_line_horizontal(X, Y + Height, Width);
    function_line_vertical(X, Y, Height);
    function_line_vertical(X + Width, Y, Height);
}
void _ttc_gfx_generic_rect_fill(u16_t X, u16_t Y, s16_t Width, s16_t Height) {

    if (Width < 0) {
        X += Width;
        Width = - Width;
    }
    if (Height < 0) {
        Y += Height;
        Height = - Height;
    }
    if (X >= ttc_gfx_CurrentDisplay->Width)
        X = ttc_gfx_CurrentDisplay->Width - 1;
    if (Y >= ttc_gfx_CurrentDisplay->Height)
        Y = ttc_gfx_CurrentDisplay->Height - 1;
    if (X + Width >= ttc_gfx_CurrentDisplay->Width)
        Width = ttc_gfx_CurrentDisplay->Width - X - 1;
    if (X + Height >= ttc_gfx_CurrentDisplay->Height)
        Height = ttc_gfx_CurrentDisplay->Height - Y - 1;

    for (; Height >= 0; Height--) {
        _ttc_gfx_generic_line_horizontal(X, Y++, Width);
    }
}
void _ttc_gfx_generic_circle(u16_t CenterX, u16_t CenterY, u16_t Radius) {

    GFX_STATIC int tggc_F;     tggc_F = 1 - Radius;
    GFX_STATIC int tggc_ddF_x; tggc_ddF_x = 0;
    GFX_STATIC int tggc_ddF_y; tggc_ddF_y = -2 * Radius;
    GFX_STATIC int tggc_X;     tggc_X = 0;
    GFX_STATIC int tggc_Y;     tggc_Y = Radius;

    _ttc_gfx_driver_set_pixel_at(CenterX, CenterY + Radius);
    _ttc_gfx_driver_set_pixel_at(CenterX, CenterY - Radius);
    _ttc_gfx_driver_set_pixel_at(CenterX + Radius, CenterY);
    _ttc_gfx_driver_set_pixel_at(CenterX - Radius, CenterY);

    while (tggc_X < tggc_Y) {
        if (tggc_F >= 0) {
            tggc_Y--;
            tggc_ddF_y += 2;
            tggc_F += tggc_ddF_y;
        }
        tggc_X++;
        tggc_ddF_x += 2;
        tggc_F += tggc_ddF_x + 1;

        _ttc_gfx_driver_set_pixel_at(CenterX + tggc_X, CenterY + tggc_Y); //
        _ttc_gfx_driver_set_pixel_at(CenterX - tggc_X, CenterY + tggc_Y); //
        _ttc_gfx_driver_set_pixel_at(CenterX + tggc_X, CenterY - tggc_Y); //
        _ttc_gfx_driver_set_pixel_at(CenterX - tggc_X, CenterY - tggc_Y); //
        _ttc_gfx_driver_set_pixel_at(CenterX + tggc_Y, CenterY + tggc_X);
        _ttc_gfx_driver_set_pixel_at(CenterX - tggc_Y, CenterY + tggc_X);
        _ttc_gfx_driver_set_pixel_at(CenterX + tggc_Y, CenterY - tggc_X);
        _ttc_gfx_driver_set_pixel_at(CenterX - tggc_Y, CenterY - tggc_X);
    }
}
void _ttc_gfx_generic_circle_fill(u16_t CenterX, u16_t CenterY, u16_t Radius) {

    GFX_STATIC int tggcf_F;     tggcf_F = 1 - Radius;
    GFX_STATIC int tggcf_ddF_x; tggcf_ddF_x = 0;
    GFX_STATIC int tggcf_ddF_y; tggcf_ddF_y = -2 * Radius;
    GFX_STATIC int tggcf_X;     tggcf_X = 0;
    GFX_STATIC int tggcf_Y;     tggcf_Y = Radius;
    GFX_STATIC int tggcf_Length1, tggcf_Lenght2;

    _ttc_gfx_driver_set_pixel_at(CenterX, CenterY + Radius);
    _ttc_gfx_driver_set_pixel_at(CenterX, CenterY - Radius);
    function_line_horizontal(CenterX - Radius, CenterY, 2 * Radius);

    while (tggcf_X < tggcf_Y) {
        if (tggcf_F >= 0) {
            tggcf_Y--;
            tggcf_ddF_y += 2;
            tggcf_F += tggcf_ddF_y;
        }
        tggcf_X++;
        tggcf_ddF_x += 2;
        tggcf_F += tggcf_ddF_x + 1;
        tggcf_Length1=(2*tggcf_X);
        tggcf_Lenght2=(2*tggcf_Y);
        function_line_horizontal(CenterX - tggcf_X, CenterY + tggcf_Y,tggcf_Length1);
        function_line_horizontal(CenterX - tggcf_X, CenterY - tggcf_Y,tggcf_Length1);
        function_line_horizontal(CenterX - tggcf_Y, CenterY + tggcf_X,tggcf_Lenght2);
        function_line_horizontal(CenterX - tggcf_Y, CenterY - tggcf_X,tggcf_Lenght2);
        //    function_line_horizontal(CenterX - Radius, CenterY,2*Radius);
        // function_line_horizontal(CenterX - Radius, CenterY,2*Radius);

    }
}
void _ttc_gfx_generic_circle_segment(u16_t CenterX, u16_t CenterY, u16_t Radius, u16_t StartAngle, u16_t EndAngle) {

    GFX_STATIC double tggcs_X, tggcs_Y;
    GFX_STATIC double tggcs_PhiRadA, tggcs_PhiRadE;

    if (StartAngle < EndAngle) {
        tggcs_PhiRadA = (PI/180.0)*StartAngle;
        tggcs_PhiRadE = (PI/180.0)*EndAngle;
    } else {
        tggcs_PhiRadA = (PI/180.0)*EndAngle;
        tggcs_PhiRadE = (PI/180.0)*StartAngle;
    }

    _ttc_gfx_driver_set_pixel_at(CenterX, CenterY);

    while (tggcs_PhiRadE > tggcs_PhiRadA) {

        tggcs_X = math_cos(tggcs_PhiRadA); // ToDo: remove trigonometry from this loop
        tggcs_Y = math_sin(tggcs_PhiRadA);

        tggcs_X *= Radius;
        tggcs_Y *= Radius;

        _ttc_gfx_driver_set_pixel_at(CenterX +(int16_t) tggcs_X, CenterY +(int16_t) tggcs_Y);

        tggcs_PhiRadA += 0.0175;

    }
}
void _ttc_gfx_generic_set_backlight(u8_t Level) {
    (void) Level; // just ignoring
}

//} Function definitions
