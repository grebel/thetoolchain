#ifndef DAC_STM32L1XX_H
#define DAC_STM32L1XX_H

/** { dac_stm32l1xx.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for dac devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by high-level dac and application.
 *
 *  Created from template device_architecture.h revision 22 at 20141215 11:13:24 UTC
 *
 *  Note: See ttc_dac.h for description of stm32l1xx independent DAC implementation.
 *  
 *  Authors: Adrián Romero
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_DAC_STM32L1XX
//
// Implementation status of low-level driver support for dac devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_DAC_STM32L1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_DAC_STM32L1XX == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_DAC_STM32L1XX to one from '-', 'o', '+' 
#endif

//}EXTENSION_STATUS_TTC_DAC_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "dac_stm32l1xx.c"
//
#include "dac_stm32l1xx_types.h"
#include "../ttc_dac_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_dac_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_dac_foo
//
#define ttc_driver_dac_deinit(Config) dac_stm32l1xx_deinit(Config)
#define ttc_driver_dac_get_features(Config) dac_stm32l1xx_get_features(Config)
#define ttc_driver_dac_init(Config) dac_stm32l1xx_init(Config)
#define ttc_driver_dac_load_defaults(Config) dac_stm32l1xx_load_defaults(Config)
#define ttc_driver_dac_prepare() dac_stm32l1xx_prepare()
#define ttc_driver_dac_reset(Config) dac_stm32l1xx_reset(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_dac.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_dac.h for details.
 *
 * You normally should not need to add functions here. 
 * Add your private functions in the private section below. 
 */


/** shutdown single DAC unit device
 * @param Config        pointer to struct t_ttc_dac_config (must have valid value for PhysicalIndex)
 * @return              == 0: DAC has been shutdown successfully; != 0: error-code
 */
e_ttc_dac_errorcode dac_stm32l1xx_deinit(t_ttc_dac_config* Config);


/** fills out given Config with maximum valid values for indexed DAC
 * @param Config        = pointer to struct t_ttc_dac_config (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_dac_errorcode dac_stm32l1xx_get_features(t_ttc_dac_config* Config);


/** initializes single DAC unit for operation
 * @param Config        pointer to struct t_ttc_dac_config (must have valid value for PhysicalIndex)
 * @return              == 0: DAC has been initialized successfully; != 0: error-code
 */
e_ttc_dac_errorcode dac_stm32l1xx_init(t_ttc_dac_config* Config);


/** loads configuration of indexed DAC unit with default values
 * @param Config        pointer to struct t_ttc_dac_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_dac_errorcode dac_stm32l1xx_load_defaults(t_ttc_dac_config* Config);


/** Prepares dac Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void dac_stm32l1xx_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_dac_config (must have valid value for PhysicalIndex)
 */
void dac_stm32l1xx_reset(t_ttc_dac_config* Config);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _dac_stm32l1xx_foo(t_ttc_dac_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)
e_ttc_dac_errorcode _dac_stm32l1xx_deinit(t_ttc_dac_config * Config);

void _dac_stm32l1xx_dma(t_ttc_dac_config* Config, FunctionalState NewState);
/*
 * Enables or disables the selected DAC channel wave generation in the Config.
 */
void _dac_stm32l1xx_wave_generation(t_ttc_dac_config* Config,FunctionalState NewState);
/* Enables or disables the selected DAC channel software trigger */
void _dac_stm32l1xx_software_trigger(t_ttc_dac_config* Config, FunctionalState NewState);


void _dac_stm32l1xx_set_channel1Data(t_u32 Align, t_u16 Data);

e_ttc_dac_errorcode _dac_stm32l1xx_init(t_ttc_dac_config* Config);
//}PrivateFunctions

#endif //DAC_STM32L1XX_H