#ifndef dac_STM32L1XX_TYPES_H
#define dac_STM32L1XX_TYPES_H

/** { dac_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *                     
 *  Low-Level datatypes for dac devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_dac_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20141215 11:13:24 UTC
 *
 *  Note: See ttc_dac.h for description of architecture independent dac implementation.
 * 
 *  Authors: <Adrián Romero>
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "../register/register_stm32l1xx_types.h"
#include "../sysclock/sysclock_stm32l1xx.h"


//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_dac_types.h *************************



typedef struct { // register description (adapt according to stm32l1xx registers)
  unsigned Reserved1 : 16;
  unsigned Reserved2 : 16;
} t_dac_register;


typedef struct {

  t_u32 dma_PeripheralBaseAddr; // Specifies the peripheral base address for DMAy Channelx.

  t_u32 dma_MemoryBaseAddr;     // Specifies the memory base address for DMAy Channelx.

  t_u32 dma_DIR;                // Specifies if the peripheral is the source or destination.

  t_u32 dma_BufferSize;         // Specifies the buffer size, in data unit, of the specified Channel.

  t_u32 dma_PeripheralInc;      // Specifies whether the Peripheral address register is incremented or not.

  t_u32 dma_MemoryInc;          // Specifies whether the memory address register is incremented or not.

  t_u32 dma_PeripheralDataSize; // Specifies the Peripheral data width.

  t_u32 dma_MemoryDataSize;     // Specifies the Memory data width.

  t_u32 dma_Mode;               // Specifies the operation mode of the DMAy Channelx.

  t_u32 dma_Priority;           // Specifies the software priority for the DMAy Channelx.

  t_u32 dma_M2M;                // Specifies if the DMAy Channelx will be used in memory-to-memory transfer.

}dac_dma_init_structure;

typedef enum {
    dac_stm32l1xx_trigger_none = (t_u32)0x00000000,
    dac_stm32l1xx_trigger_T6_TRGO = (t_u32)0x00000004,
    dac_stm32l1xx_trigger_T7_TRGO =(t_u32)0x00000014,
    dac_stm32l1xx_trigger_T9_TRGO =(t_u32)0x0000001C,
    dac_stm32l1xx_trigger_T2_TRGO =(t_u32)0x00000024,
    dac_stm32l1xx_trigger_T4_TRGO =(t_u32)0x0000002C,
    dac_stm32l1xx_trigger_Ext_IT9 =(t_u32)0x00000034,
    dac_stm32l1xx_trigger_Software =(t_u32)0x0000003C

} e_dac_stm32l1xx_trigger_source;

typedef enum{ //is wave generated and what type
    dac_stm32l1xx_WaveGeneration_None =           (t_u32)0x00000000,
    dac_stm32l1xx_WaveGeneration_Noise =          (t_u32)0x00000040,
    dac_stm32l1xx_WaveGeneration_Triangle =       (t_u32)0x00000080

} e_dac_stm32l1xx_wave_gen;

typedef enum{ //LFSR mask for noise wave or amplitude of triangular wave
    dac_stm32l1xx_LFSRUnmask_Bit0             =   (t_u32)0x00000000, //*!< Unmask dac channel LFSR bit0 for noise wave generation */
    dac_stm32l1xx_LFSRUnmask_Bits1_0          =   (t_u32)0x00000100, //*!< Unmask dac channel LFSR bit[1:0] for noise wave generation */
    dac_stm32l1xx_LFSRUnmask_Bits2_0          =   (t_u32)0x00000200, //*!< Unmask dac channel LFSR bit[2:0] for noise wave generation */
    dac_stm32l1xx_LFSRUnmask_Bits3_0          =   (t_u32)0x00000300, //*!< Unmask dac channel LFSR bit[3:0] for noise wave generation */
    dac_stm32l1xx_LFSRUnmask_Bits4_0          =   (t_u32)0x00000400, //*!< Unmask dac channel LFSR bit[4:0] for noise wave generation */
    dac_stm32l1xx_LFSRUnmask_Bits5_0          =   (t_u32)0x00000500, //*!< Unmask dac channel LFSR bit[5:0] for noise wave generation */
    dac_stm32l1xx_LFSRUnmask_Bits6_0          =   (t_u32)0x00000600, //*!< Unmask dac channel LFSR bit[6:0] for noise wave generation */
    dac_stm32l1xx_LFSRUnmask_Bits7_0          =   (t_u32)0x00000700, //*!< Unmask dac channel LFSR bit[7:0] for noise wave generation */
    dac_stm32l1xx_LFSRUnmask_Bits8_0          =   (t_u32)0x00000800, //*!< Unmask dac channel LFSR bit[8:0] for noise wave generation */
    dac_stm32l1xx_LFSRUnmask_Bits9_0          =   (t_u32)0x00000900, //*!< Unmask dac channel LFSR bit[9:0] for noise wave generation */
    dac_stm32l1xx_LFSRUnmask_Bits10_0         =   (t_u32)0x00000A00, //*!< Unmask dac channel LFSR bit[10:0] for noise wave generation */
    dac_stm32l1xx_LFSRUnmask_Bits11_0         =   (t_u32)0x00000B00, //*!< Unmask dac channel LFSR bit[11:0] for noise wave generation */
    dac_stm32l1xx_TriangleAmplitude_1         =   (t_u32)0x00000000, //*!< Select max triangle amplitude of 1 */
    dac_stm32l1xx_TriangleAmplitude_3         =   (t_u32)0x00000100, //*!< Select max triangle amplitude of 3 */
    dac_stm32l1xx_TriangleAmplitude_7         =   (t_u32)0x00000200, //*!< Select max triangle amplitude of 7 */
    dac_stm32l1xx_TriangleAmplitude_15        =   (t_u32)0x00000300, //*!< Select max triangle amplitude of 15 */
    dac_stm32l1xx_TriangleAmplitude_31        =   (t_u32)0x00000400, //*!< Select max triangle amplitude of 31 */
    dac_stm32l1xx_TriangleAmplitude_63        =   (t_u32)0x00000500, //*!< Select max triangle amplitude of 63 */
    dac_stm32l1xx_TriangleAmplitude_127       =   (t_u32)0x00000600, //*!< Select max triangle amplitude of 127 */
    dac_stm32l1xx_TriangleAmplitude_255       =   (t_u32)0x00000700, //*!< Select max triangle amplitude of 255 */
    dac_stm32l1xx_TriangleAmplitude_511       =   (t_u32)0x00000800, //*!< Select max triangle amplitude of 511 */
    dac_stm32l1xx_TriangleAmplitude_1023      =   (t_u32)0x00000900, //*!< Select max triangle amplitude of 1023 */
    dac_stm32l1xx_TriangleAmplitude_2047      =   (t_u32)0x00000A00, //*!< Select max triangle amplitude of 2047 */
    dac_stm32l1xx_TriangleAmplitude_4095      =   (t_u32)0x00000B00 //*!< Select max triangle amplitude of 4095 */

} e_dac_stm32l1xx_mask_or_amplitude;

typedef enum{
    e_dac_stm32l1xx_output_buffernable = (t_u32)0x00000000,
    dac_stm32l1xx_output_buffer_disable = (t_u32)0x00000002

} e_dac_stm32l1xx_output_buffer;

typedef enum{ //kind of wave
    dac_stm32l1xx_wave_none =                       ((t_u32)0x00000000),
    dac_stm32l1xx_wave_noise =                      ((t_u32)0x00000040),
    dac_stm32l1xx_wave_triangle =                   ((t_u32)0x00000080)
}e_dac_stm32l1xx_wave;


typedef struct {  // stm32l1xx specific configuration data of single dac device

    e_dac_stm32l1xx_trigger_source      TriggerType;
    e_dac_stm32l1xx_wave_gen            WaveType;
    e_dac_stm32l1xx_mask_or_amplitude   Mask_or_Amp;
    e_dac_stm32l1xx_output_buffer       Output_Buffer_status;

} __attribute__((__packed__)) t_dac_stm32l1xx_config;


// t_ttc_dac_architecture is required by ttc_dac_types.h
#define t_ttc_dac_architecture t_dac_stm32l1xx_config

//} Structures/ Enums


#endif //dac_STM32L1XX_TYPES_H
