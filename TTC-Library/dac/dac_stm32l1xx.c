/** { dac_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for dac devices on stm32l1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20141215 11:13:24 UTC
 *
 *  Note: See ttc_dac.h for description of stm32l1xx independent DAC implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "dac_stm32l1xx.h".
//
#include "dac_stm32l1xx.h"
#include "../ttc_memory.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

e_ttc_dac_errorcode dac_stm32l1xx_deinit(t_ttc_dac_config* Config) {
    Assert_DAC(Config, ttc_assert_origin_auto); // pointers must not be NULL


    _dac_stm32l1xx_deinit(Config);


    return (e_ttc_dac_errorcode) 0;
}
e_ttc_dac_errorcode dac_stm32l1xx_get_features(t_ttc_dac_config* Config) {


    return (e_ttc_dac_errorcode) 0;
}
e_ttc_dac_errorcode dac_stm32l1xx_init(t_ttc_dac_config* Config) {

    Assert_DAC(Config, ttc_assert_origin_auto); // pointers must not be NULL

    _dac_stm32l1xx_init(Config);

    return (e_ttc_dac_errorcode) 0;
}


e_ttc_dac_errorcode dac_stm32l1xx_load_defaults(t_ttc_dac_config* Config) {
    Assert_DAC(Config, ttc_assert_origin_auto); // pointers must not be NULL


    t_ttc_dac_architecture* ConfigArch = Config->LowLevelConfig;

    if (ConfigArch == NULL) {
        ConfigArch = (t_ttc_dac_architecture*)ttc_heap_alloc_zeroed( sizeof(t_ttc_dac_architecture) );
        Config->LowLevelConfig = ConfigArch;
    }

    Config->LowLevelConfig->Mask_or_Amp = dac_stm32l1xx_LFSRUnmask_Bit0;
    Config->LowLevelConfig->Output_Buffer_status = e_dac_stm32l1xx_output_buffernable;
    Config->LowLevelConfig->TriggerType = dac_stm32l1xx_trigger_none;
    Config->LowLevelConfig->WaveType = dac_stm32l1xx_wave_none;

    return (e_ttc_dac_errorcode) 0;
}

void dac_stm32l1xx_prepare() {
    

}
void dac_stm32l1xx_reset(t_ttc_dac_config* Config) {
    Assert_DAC(Config, ttc_assert_origin_auto); // pointers must not be NULL

}




t_u16 dac_stm32l1xx_get_data_output_value(t_ttc_dac_config* Config) {

    volatile t_u32 tmp = 0;

    tmp = (t_u32) DAC_BASE ;
    tmp += 0x0000002C + ((t_u32)Config->Channel >> 2);

    /* Returns the DAC channel data output register value */
    return (t_u16) (*(volatile t_u32*) tmp);
}


//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

e_ttc_dac_errorcode _dac_stm32l1xx_deinit(t_ttc_dac_config * Config){

    /* Enable dac reset state */
    register_stm32l1xx_RCC.APB1RSTR.All |= 0x40007400;

    /* Release DAC from reset state */
    register_stm32l1xx_RCC.APB1RSTR.All &= ~0x40007400;

    return (e_ttc_dac_errorcode) 0;

}

void _dac_stm32l1xx_dma(t_ttc_dac_config* Config, FunctionalState NewState){

  if (NewState != DISABLE)
  {
    /* Enable the selected DAC channel DMA request */
    register_stm32l1xx_DAC.CR.All |= (0x00001000 << Config->Channel);
  }
  else
  {
    /* Disable the selected DAC channel DMA request */
    register_stm32l1xx_DAC.CR.All &= (~(0x00001000 << Config->Channel));
  }
}

/*
 * Enables or disables the selected DAC channel wave generation in the Config.
 */
void _dac_stm32l1xx_wave_generation(t_ttc_dac_config* Config,FunctionalState NewState){

    if (NewState != DISABLE){

        /* Enable the selected wave generation for the selected DAC channel */
        register_stm32l1xx_DAC.CR.All |= Config->LowLevelConfig->WaveType << Config->Channel;
    }

    else
    {
        /* Disable the selected wave generation for the selected DAC channel */
        register_stm32l1xx_DAC.CR.All &= ~(Config->LowLevelConfig->WaveType << Config->Channel);
    }
}

/* Enables or disables the selected DAC channel software trigger */
void _dac_stm32l1xx_software_trigger(t_ttc_dac_config* Config, FunctionalState NewState){

  if (NewState != DISABLE){

      /* Enable software trigger for the selected DAC channel */
      register_stm32l1xx_DAC.SWTRIGR.All |= (t_u32)0x01 << (Config->Channel >> 4);

  }
  else{

      /* Disable software trigger for the selected DAC channel */
      register_stm32l1xx_DAC.SWTRIGR.All &= ~((t_u32)0x01 << (Config->Channel >> 4));
  }

}

e_ttc_dac_errorcode _dac_stm32l1xx_init(t_ttc_dac_config* Config) {

    /* Enable DAC CLCOK */
    sysclock_stm32l1xx_RCC_APB1PeriphClockCmd(0x20000000, ENABLE);

    t_u32 tmpreg1,tmpreg2;

    /* Get the DAC CR value */
    tmpreg1 = register_stm32l1xx_DAC.CR.All;

    /* Clear BOFFx, TENx, TSELx, WAVEx and MAMPx bits */
    tmpreg1 &= ~(0x00000FFE << Config->Channel);

    /* Configure for the selected DAC channel: buffer output, trigger, wave generation,
       mask/amplitude for wave generation */
    /* Set TSELx and TENx bits according to DAC_Trigger value */
    /* Set WAVEx bits according to DAC_WaveGeneration value */
    /* Set MAMPx bits according to DAC_LFSRUnmask_TriangleAmplitude value */
    /* Set BOFFx bit according to DAC_OutputBuffer value */
    tmpreg2 = (Config->LowLevelConfig->TriggerType|Config->LowLevelConfig->WaveType|Config->LowLevelConfig->Mask_or_Amp|Config->LowLevelConfig->Output_Buffer_status);

    /* Calculate CR register value depending on DAC_Channel */
    tmpreg1 |= tmpreg2 << (Config->Channel);

    /* Write to DAC CR */
    register_stm32l1xx_DAC.CR.All = tmpreg1;

    register_stm32l1xx_DAC.CR.All |= (0x00000001 << Config->Channel);


    return 0;




//    tmpreg1 = DAC->CR;
//    /* Clear BOFFx, TENx, TSELx, WAVEx and MAMPx bits */
//    tmpreg1 &= ~(CR_CLEAR_MASK << DAC_Channel);
//    /* Configure for the selected DAC channel: buffer output, trigger, wave generation,
//       mask/amplitude for wave generation */
//    /* Set TSELx and TENx bits according to DAC_Trigger value */
//    /* Set WAVEx bits according to DAC_WaveGeneration value */
//    /* Set MAMPx bits according to DAC_LFSRUnmask_TriangleAmplitude value */
//    /* Set BOFFx bit according to DAC_OutputBuffer value */
//    tmpreg2 = (DAC_InitStruct->DAC_Trigger | DAC_InitStruct->DAC_WaveGeneration |
//               DAC_InitStruct->DAC_LFSRUnmask_TriangleAmplitude | DAC_InitStruct->DAC_OutputBuffer);
//    /* Calculate CR register value depending on DAC_Channel */
//    tmpreg1 |= tmpreg2 << DAC_Channel;
//    /* Write to DAC CR */
//    DAC->CR = tmpreg1;

}


void _dac_stm32l1xx_set_channel1Data(t_u32 Align, t_u16 Data){

    volatile t_u32 tmp = 0;
    tmp = (t_u32)DAC_BASE;
    tmp += 0x00000008 + Align;

    /* Set the DAC channel1 selected data holding register */
    *(volatile t_u32 *) tmp = Data;
}

//}Private Functions
