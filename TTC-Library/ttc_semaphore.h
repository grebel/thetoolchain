#ifndef ttc_semaphore_H
#define ttc_semaphore_H

/** ttc_semaphore.h ***********************************************{
 *
 * Written by Gregor Rebel 2013-2018
 *
 */
/** Description of ttc_semaphore (Do not delete this line!)
 *
 * Architecture independent support for inter task communication & synchronization.
 *
 * Currently supported architecture/ schedulers: CortexM3 (cm3), FreeRTOS
 *
 * Why using ttc_semaphore.c instead of cm3_semaphore.c or FreeRTOS-functions directly?
 * (1) it is more portable
 *     You may change to another multitasking scheduler once it is supported by The ToolChain
 * (2) extra safety checks
 *     DeadAmounts are a common issue in multitasking applications and sometimes hard to debug.
 *     + ttc_semaphore allows to turn all endless Amounts into a timed one that will assert if expired.
 *       -> ttc_semaphore_types.h/TTC_SEMAPHORE_TIMEOUT_USECS
 *     + ttc_semaphore provides smart semaphores that can record history of operations and
 *       run as fast as normal semaphores if disabled.
 *       -> ttc_semaphore_types.h/TTC_SMART_SEMAPHORE
 *
 * H2: Using Semaphores
 *
 * A semaphore is basically just a counter with some extra logic.
 * It provides the basic counter features:
 * - Add certain amount to the semaphore (->ttc_semaphore_give() )
 * - Subtract certain amount from the semaphore (->ttc_semaphore_take() )
 * - Check if semaphore is at least as high as given amount (->ttc_semaphore_can_provide() )
 *
 * The extra logic of semaphores have to do with synchronization:
 * - A semaphore cannot be negative.
 * - A ttc_semaphore_take() will block if semaphore is lower than requested amount.
 * - A ttc_semaphore_give() call can unblock the ttc_semaphore_take() call of a different task.
 * - If two tasks or a task and an interrupt service routine try to access the same semaphore,
 *   no data corruption will occur.
 *
 * H2: Smart Semaphores
 *
 * When TTC_SMART_SEMAPHORE is defined as 1 then semaphores are replaced by smart semaphores.
 * A smart semaphore stores additional debug informations like the function that used a semaphore last time.
 * Whenever a semaphore stays blocked because of a programming error, then the last blocking
 * function can be identified by taking a look into the extended data fields.
 * Smart semaphores are completely transparent for the application and have no overhead if disabled.
}*/

//{ includes

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)

#ifdef EXTENSION_ttc_semaphore_cortexm3
    #include "cpu/cm3_semaphore.h"
#endif

#ifdef EXTENSION_ttc_semaphore_cortexm0
    #include "cm0/cm0_semaphore.h"
#endif

#ifdef EXTENSION_ttc_semaphore_freertos
    #include "scheduler/freertos_semaphore.h"
#endif

#include "ttc_semaphore_types.h"
#include "ttc_task.h"

//}includes
//{ Types & Structures ***************************************************

/** Types to be defined by low-level driver ******************************
 *
 * t_ttc_semaphore  datatype storing a semaphore
 * E.g.: typedef t_u8 t_ttc_semaphore
 *
 */


//}
#include "ttc_memory.h"

//{ Function prototypes **************************************************
//
// For each function, a _driver_() pendant must be defined by low-level driver to provide an implementation.


/** creates a new semaphore with initial value 0
 *
 * @return                 =! NULL: semaphore has been created successfully (use handle to operate on semaphore)
 */
t_ttc_semaphore_smart* ttc_semaphore_create();
#if TTC_SMART_SEMAPHORE == 1
    #define ttc_semaphore_create() _ttc_semaphore_smart_create()
#else
    #define ttc_semaphore_create() _ttc_semaphore_create()
#endif
t_ttc_semaphore_smart* _ttc_semaphore_smart_create();
t_ttc_semaphore* _ttc_semaphore_create();
#ifndef _driver_semaphore_create
    #warning Missing low-level implementation for _driver_semaphore_create()! // check description above for function to be implemented
#endif


/** initializes given memory as a new empty semaphore
 *
 * @return                 =! NULL: semaphore has been created successfully (use handle to operate on semaphore)
 */
void ttc_semaphore_init( t_ttc_semaphore_smart* Semaphore );
#if TTC_SMART_SEMAPHORE == 1
    #define ttc_semaphore_init(SmartSemaphore) _ttc_semaphore_smart_init(SmartSemaphore)
#else
    #define ttc_semaphore_init(Semaphore)      _ttc_semaphore_init(Semaphore)
#endif
void _ttc_semaphore_smart_init( t_ttc_semaphore_smart* Semaphore );
void _ttc_semaphore_init( t_ttc_semaphore* Semaphore );
#ifndef _driver_semaphore_init
    #warning Missing low-level implementation for _driver_semaphore_init()! // check description above for function to be implemented
#endif

/** Checks if given smart semaphore can provide given amount
 *
 * Note: Calling this function makes only sense from interrupt service routine
 *
 * @param Semaphore  handle of already created semaphore
 * @return         == tsme_OK: Given semaphore can provide given Amount (only valid if no other task or ISR has taken them meanwhile)
 */
BOOL ttc_semaphore_can_provide( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount );
#if TTC_SMART_SEMAPHORE == 1
    #define ttc_semaphore_can_provide(Semaphore, Amount) _ttc_semaphore_smart_can_provide(Semaphore, Amount)
#else
    #define ttc_semaphore_can_provide(Semaphore, Amount) _ttc_semaphore_can_provide(Semaphore, Amount)
#endif
BOOL _ttc_semaphore_smart_can_provide( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount );
BOOL _ttc_semaphore_can_provide( t_ttc_semaphore* Semaphore, t_base Amount );
#ifndef _driver_semaphore_can_provide
    #warning Missing low-level implementation for _driver_semaphore_can_provide()! // check description above for function to be implemented
#endif

/** Blocks until given amount is taken from semaphore
 *
 * @param Semaphore  handle of already created semaphore
 */
void ttc_semaphore_take_endless( t_ttc_semaphore* Semaphore, t_base Amount );
#if TTC_SMART_SEMAPHORE == 1
    #define ttc_semaphore_take_endless(Semaphore, Amount) _t_ttc_semaphore_smartake(Semaphore, Amount, -1)
#else
    #if TTC_SEMAPHORE_TIMEOUT_USECS > 0
        #define ttc_semaphore_take_endless(Semaphore, Amount) _ttc_semaphore_take(Semaphore, Amount, -1)
    #else
        #define ttc_semaphore_take_endless(Semaphore, Amount) _ttc_semaphore_take_endless(Semaphore, Amount)
    #endif
#endif
void _ttc_semaphore_take_endless( t_ttc_semaphore* Semaphore, t_base Amount );

/** Tries to take given amount from a semaphore within timeout
 *
 * @param Semaphore  handle of already created semaphore
 * @param TimeOut    amount of system ticks to wait for Amount
 * @return           == tsme_OK: Amount has been successfully obtained within TimeOut
 */
e_ttc_semaphore_error ttc_semaphore_take( t_ttc_semaphore_smart* Semaphore, t_base Amount, t_base TimeOut );
#if TTC_SMART_SEMAPHORE == 1
    #define ttc_semaphore_take(Semaphore, Amount, TimeOut) _t_ttc_semaphore_smartake(Semaphore, Amount, TimeOut)
#else
    #define ttc_semaphore_take(Semaphore, Amount, TimeOut) _ttc_semaphore_take(Semaphore, Amount, TimeOut)
#endif
e_ttc_semaphore_error _t_ttc_semaphore_smartake( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount, t_base TimeOut );
e_ttc_semaphore_error _ttc_semaphore_take( t_ttc_semaphore* SEMAPHORE_VOLATILE Semaphore, t_base Amount, t_base TimeOut );
#ifndef _driver_semaphore_take
    #warning Missing low-level implementation for _driver_semaphore_take()! // check description above for function to be implemented
#endif


/** Tries to take given amount from semaphore; this function will never block and must only be called with disabled interrupts!
 *
 * @param Semaphore  handle of already created semaphore
 * @param Amount     amount of tokens to give/ take to/from semaphore

 * @return           == tsme_OK: Given Amount has been successfully taken from semaphore
 *                   == tsme_WasAmountedByTask: - a task of higher priority has been interrupted => call ttc_task_yield() at end of your interrupt service routine!
 *                   >= tsme_Error: Error condition
 */
e_ttc_semaphore_error ttc_semaphore_take_try( t_ttc_semaphore_smart* Semaphore, t_base Amount );
#if TTC_SMART_SEMAPHORE == 1
    #define ttc_semaphore_take_try(Semaphore, Amount) _t_ttc_semaphore_smartake_try(Semaphore, Amount)
#else
    #define ttc_semaphore_take_try(Semaphore, Amount) _ttc_semaphore_take_try(Semaphore, Amount)
#endif
e_ttc_semaphore_error _t_ttc_semaphore_smartake_try( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount );
e_ttc_semaphore_error _ttc_semaphore_take_try( t_ttc_semaphore* Semaphore, t_base Amount );
// #ifndef _driver_semaphore_take_try
// #warning Missing low-level implementation for _driver_semaphore_take_try()! // check description above for function to be implemented
// #endif


/** Tries to take given amount from semaphore; this function will never block and must only be called with disabled interrupts!
 *
 * @param Semaphore  handle of already created semaphore
 * @param Amount     amount of tokens to give/ take to/from semaphore

 * @return           == tsme_OK: Given Amount has been successfully taken from semaphore
 *                   == tsme_WasAmountedByTask: - a task of higher priority has been interrupted => call ttc_task_yield() at end of your interrupt service routine!
 *                   >= tsme_Error: Error condition
 */
e_ttc_semaphore_error ttc_semaphore_take_isr( t_ttc_semaphore_smart* Semaphore, t_base Amount );
#if TTC_SMART_SEMAPHORE == 1
    #define ttc_semaphore_take_isr(Semaphore, Amount) _t_ttc_semaphore_smartake_isr(Semaphore, Amount)
#else
    #define ttc_semaphore_take_isr(Semaphore, Amount) _ttc_semaphore_take_isr(Semaphore, Amount)
#endif
e_ttc_semaphore_error _t_ttc_semaphore_smartake_isr( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount );
e_ttc_semaphore_error _ttc_semaphore_take_isr( t_ttc_semaphore* Semaphore, t_base Amount );


/** Gives amount of tokens back to semaphore
 *
 * @param Semaphore  handle of already created smart semaphore
 * @param Amount     amount of tokens to give/ take to/from semaphore
 */
void ttc_semaphore_give( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount );
#if TTC_SMART_SEMAPHORE == 1
    #define ttc_semaphore_give(Semaphore, Amount) _ttc_semaphore_smart_give(Semaphore, Amount)
#else
    #define ttc_semaphore_give(Semaphore, Amount) _ttc_semaphore_give(Semaphore, Amount)
#endif
void _ttc_semaphore_smart_give( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount );
void _ttc_semaphore_give( t_ttc_semaphore* Semaphore, t_base Amount );
#ifndef _driver_semaphore_give
    #warning Missing low-level implementation for _driver_semaphore_give()! // check description above for function to be implemented
#endif

/** Gives amount of tokens back to semaphore (does never block)
 *
 * @param Semaphore  handle of already created semaphore
 */
void ttc_semaphore_give_isr( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount );
#if TTC_SMART_SEMAPHORE == 1
    #define ttc_semaphore_give_isr(Semaphore, Amount) _ttc_semaphore_smart_give_isr(Semaphore, Amount)
#else
    #define ttc_semaphore_give_isr(Semaphore, Amount) _ttc_semaphore_give_isr(Semaphore, Amount)
#endif
void _ttc_semaphore_smart_give_isr( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount );
void _ttc_semaphore_give_isr( t_ttc_semaphore* Semaphore, t_base Amount );

/** Returns current amount of tokens available in given Semaphore (very fast)
 *
 * Note: Use of this function does not require exclusive access and is much faster than all other semaphore functions
 *
 * @param Semaphore  handle of already created semaphore
 * @return           current value stored in given semaphore
 */
t_base ttc_semaphore_available( t_ttc_semaphore_smart* SmartSemaphore );
#if TTC_SMART_SEMAPHORE == 1
    #define ttc_semaphore_available(Semaphore) _ttc_semaphore_smart_available(Semaphore)
#else
    #define ttc_semaphore_available(Semaphore) _driver_semaphore_available(&((Semaphore)->Value))
#endif
t_base _ttc_semaphore_smart_available( t_ttc_semaphore_smart* SmartSemaphore );
#ifndef _driver_semaphore_available
    #warning Missing low-level implementation for _driver_semaphore_available()! // check description above for function to be implemented
#endif

//}Function prototypes

#endif
