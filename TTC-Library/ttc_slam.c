/** { ttc_slam.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for slam devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Most functions of this driver are defined as macros and being implemented
 *  in interfaces/ttc_slam_interface.c or in low-level drivers slam/slam_*.c.
 *
 *  See corresponding ttc_slam.h for documentation of all high-level functions.
 *
 *  Created from template ttc_device.c revision 38 at 20160606 11:58:15 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_slam.h".
//
#include "ttc_slam.h"
#include "ttc_math.h"
#include "ttc_heap.h"
#include "ttc_semaphore.h"
#ifdef EXTENSION_ttc_interrupt
    #include "ttc_interrupt.h"
#endif
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_SLAM_AMOUNT == 0
    #warning No SLAM devices defined, check your makefile! (did you forget to activate something?) - Define at least TTC_SLAM1 as one from e_ttc_slam_architecture !
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of slam devices.
 *
 */


// for each initialized device, a pointer to its generic and architecture specific definitions is stored
A_define( t_ttc_slam_config*, ttc_slam_configs, TTC_SLAM_AMOUNT );

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ********************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_slam(t_ttc_slam_config* Config)

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * @param LogicalIndex    1.. logical index of SLAM device. Each logical device <n> is defined via TTC_SLAM<n>* constants in compile_options.h and extensions.active/makefile
 */
void _ttc_slam_configuration_check( t_u8 LogicalIndex );


/** Function is called whenever low-level driver completes mapping.
 *
 * Note: This is a good place to insert a breakpoint.
 *
 * @param Config       (t_ttc_slam_config*)  Configuration of slam device as returned by ttc_slam_get_configuration()
 */
void _ttc_slam_mapping_completed( t_ttc_slam_config* Config );

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

ttm_number               ttc_slam_calculate_distance( t_u8 LogicalIndex, t_u16 NodeIndexA, t_u16 NodeIndexB ) {
    Assert_SLAM( NodeIndexA > 0, ttc_assert_origin_auto ); // valid node index starts at 1. Check caller implementaion!
    Assert_SLAM( NodeIndexB > 0, ttc_assert_origin_auto ); // valid node index starts at 1. Check caller implementaion!
    if ( NodeIndexA == NodeIndexB )
    { return 0; }

    t_ttc_slam_config* Config = ttc_slam_get_configuration( LogicalIndex );
    Assert_SLAM( NodeIndexA <= Config->Init.Amount_Nodes, ttc_assert_origin_auto ); // invalid node index. Check implementation of caller!
    Assert_SLAM( NodeIndexB <= Config->Init.Amount_Nodes, ttc_assert_origin_auto ); // invalid node index. Check implementation of caller!

    return _driver_slam_calculate_distance( Config, NodeIndexA, NodeIndexB );
}
BOOL                     ttc_slam_calculate_mapping( t_u8 LogicalIndex ) {

    t_ttc_slam_config* Config = ttc_slam_get_configuration( LogicalIndex );
    if (
        ( ! Config->Flags.Completed_Mapping ) && // mapping not yet complete
        ( Config->Flags.Completed_Distances )  // have egnough data: calculate mapping
    ) {
        if ( _driver_slam_calculate_mapping( Config ) ) {

        }

    }

    return ( BOOL ) 0;
}
t_ttc_slam_config*       ttc_slam_create() {

    t_u8 LogicalIndex = ttc_slam_get_max_index();
    t_ttc_slam_config* Config = ttc_slam_get_configuration( LogicalIndex );

    return Config;
}
t_u8                     ttc_slam_get_max_index() {
    return TTC_SLAM_AMOUNT;
}
t_ttc_slam_config*       ttc_slam_get_configuration( t_u8 LogicalIndex ) {
    Assert_SLAM( LogicalIndex > 0, ttc_assert_origin_auto );  // logical index starts at 1
    Assert_SLAM( LogicalIndex <= TTC_SLAM_AMOUNT, ttc_assert_origin_auto );  // invalid index given (did you configure engnough slam devices in your makefile?)
    t_ttc_slam_config* Config = A( ttc_slam_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_disable();                      // ensure that we are not interrupted while creating configuration
#endif

        Config = A( ttc_slam_configs, LogicalIndex - 1 ); // have to reload with disabled interrupts to avoid racing conditions
        if ( !Config ) {
            Config = A( ttc_slam_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_slam_config ) );
            Config->Flags.Initialized = 0;             // make sure that deinit() is not called during load_defaults()
            ttc_slam_load_defaults( LogicalIndex );
        }

#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_enable();
#endif
    }

    Assert_SLAM_EXTRA_Writable( Config, ttc_assert_origin_auto );  // memory corrupted?
    return Config;
}
ttm_number               ttc_slam_get_distance( t_u8 LogicalIndex, t_u16 NodeIndexA, t_u16 NodeIndexB ) {
    Assert_SLAM( NodeIndexA > 0, ttc_assert_origin_auto ); // valid node index starts at 1. Check caller implementaion!
    Assert_SLAM( NodeIndexB > 0, ttc_assert_origin_auto ); // valid node index starts at 1. Check caller implementaion!
    if ( NodeIndexA == NodeIndexB )
    { return 0; }

    t_ttc_slam_config* Config = ttc_slam_get_configuration( LogicalIndex );
    Assert_SLAM( NodeIndexA <= Config->Init.Amount_Nodes, ttc_assert_origin_auto ); // invalid node index. Check implementation of caller!
    Assert_SLAM( NodeIndexB <= Config->Init.Amount_Nodes, ttc_assert_origin_auto ); // invalid node index. Check implementation of caller!

    return _driver_slam_get_distance( Config, NodeIndexA, NodeIndexB );
}
t_ttc_slam_node*         ttc_slam_get_node( t_u8 LogicalIndex, t_u16 NodeIndex ) {

    t_ttc_slam_config* Config = ttc_slam_get_configuration( LogicalIndex );
    Assert_SLAM( NodeIndex <= Config->Init.Amount_Nodes, ttc_assert_origin_auto ); // invalid node index. Check implementation of caller!
    Assert_SLAM( NodeIndex > 0,                          ttc_assert_origin_auto ); // invalid node index. Check implementation of caller!

    // only low-level driver knows structure of node storage
    t_ttc_slam_node* Node = _driver_slam_get_node( Config, NodeIndex );
    Assert_SLAM_EXTRA_Writable( Node, ttc_assert_origin_auto );  // low-level driver could not find indexed node!

    return Node;
}
t_ttc_math_vector3d_xyz* ttc_slam_get_node_position( t_u8 LogicalIndex, t_u16 NodeIndex ) {

    return &( ttc_slam_get_node( LogicalIndex, NodeIndex )->Position );
}
void                     ttc_slam_deinit( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration

    t_ttc_slam_config* Config = ttc_slam_get_configuration( LogicalIndex );

    if ( Config->Flags.Initialized ) {
        e_ttc_slam_errorcode Result = _driver_slam_deinit( Config );
        if ( Result == ec_slam_OK )
        { Config->Flags.Initialized = 0; }
    }
    ttc_task_critical_end(); // other tasks now may access this driver
}
e_ttc_slam_errorcode     ttc_slam_init( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration
    t_ttc_slam_config* Config = ttc_slam_get_configuration( LogicalIndex );

    if ( !Config->Amount_Nodes_Initial )
    { Config->Amount_Nodes_Initial = Config->Init.Amount_Nodes; }

    // check configuration to meet all limits of current architecture
    _ttc_slam_configuration_check( LogicalIndex );

    Config->LastError = _driver_slam_init( Config );
    Assert_SLAM_EXTRA_Writable( Config->Nodes, ttc_assert_origin_auto );  // node memory was not allocated correctly. Check implementation of low-level driver!

    ttc_slam_reset( LogicalIndex );
    if ( ! Config->LastError ) // == 0
    { Config->Flags.Initialized = 1; }

    Assert_SLAM( ttc_semaphore_available( & ( Config->Pool_Distances.BlocksAvailable ) ), ttc_assert_origin_auto );  // Low-Level driver must initialize this pool!
    Assert_SLAM( Config->Architecture != 0, ttc_assert_origin_auto );             // Low-Level driver must set this value!

    ttc_task_critical_end(); // other tasks now may access this driver

    return Config->LastError;
}
e_ttc_math_errorcode     ttc_slam_localize_foreigner( t_u8 LogicalIndex, t_ttc_math_vector3d_xyz* Localized_Left, t_ttc_math_vector3d_xyz* Localized_Right, t_ttc_slam_distance* References, t_u8 AmountReferences ) {
    e_ttc_math_errorcode Error = 0;

    Assert_SLAM( Localized_Left || Localized_Right, ttc_assert_origin_auto );  // need at least one pointer where to store position of localized foreigner
    Assert_SLAM_Writable( References, ttc_assert_origin_auto ); // always check incoming pointer arguments

    if ( AmountReferences < 2 )
    { return E_ttc_math_error_lateration3d_insufficient; }         // not egnough data for localization

#if (TTC_ASSERT_SLAM == 1)  // ensure that all reference nodes are different (makes rest of function faster)
    Assert_SLAM( References[0].NodeIndex != References[1].NodeIndex, ttc_assert_origin_auto );

    t_ttc_math_vector3d_xyz* volatile Position = ttc_slam_get_node_position( LogicalIndex, References[0].NodeIndex );
    Assert_SLAM( ttc_math_vector3d_valid( Position ), ttc_assert_origin_auto ); // this coordinate has no value. Only call this function if all of your coordinates have valid values!

    Position = ttc_slam_get_node_position( LogicalIndex, References[1].NodeIndex );
    Assert_SLAM( ttc_math_vector3d_valid( Position ), ttc_assert_origin_auto ); // this coordinate has no value. Only call this function if all of your coordinates have valid values!

    if ( AmountReferences > 2 ) {
        Assert_SLAM( References[0].NodeIndex != References[2].NodeIndex, ttc_assert_origin_auto );
        Assert_SLAM( References[1].NodeIndex != References[2].NodeIndex, ttc_assert_origin_auto );

        Position = ttc_slam_get_node_position( LogicalIndex, References[2].NodeIndex );
        Assert_SLAM( ttc_math_vector3d_valid( Position ), ttc_assert_origin_auto ); // this coordinate has no value. Only call this function if all of your coordinates have valid values!

        if ( AmountReferences > 3 ) {
            Assert_SLAM( References[0].NodeIndex != References[3].NodeIndex, ttc_assert_origin_auto );
            Assert_SLAM( References[1].NodeIndex != References[3].NodeIndex, ttc_assert_origin_auto );
            Assert_SLAM( References[2].NodeIndex != References[3].NodeIndex, ttc_assert_origin_auto );

            Position = ttc_slam_get_node_position( LogicalIndex, References[3].NodeIndex );
            Assert_SLAM( ttc_math_vector3d_valid( Position ), ttc_assert_origin_auto ); // this coordinate has no value. Only call this function if all of your coordinates have valid values!
        }
    }
#endif

    volatile t_u8 AmountValidReferences = 0;
    t_ttc_slam_node* ReferenceNodes[4]; // pointers to valid reference nodes
    for ( t_u8 Index = 0; ( Index < AmountReferences ) && ( AmountValidReferences < 4 ); Index++ ) { // copy up to 4 valid reference node datas into ReferenceNodes[]
        t_ttc_slam_node* Node = ttc_slam_get_node( LogicalIndex, References[Index].NodeIndex );

        if ( Node->Flags.PositionValid ) // node has valid coordinates: use it as reference
        { ReferenceNodes[AmountValidReferences++] = Node; }
    }
    if ( AmountValidReferences < 2 )
    { return E_ttc_math_error_lateration3d_insufficient; }  // too few references with valid coordinates given!

    t_ttc_math_vector3d_xyz* Position0 = &( ReferenceNodes[0]->Position );
    t_ttc_math_vector3d_xyz* Position1 = &( ReferenceNodes[1]->Position );
    ttm_number Distance0 = abs( References[0].Distance );
    ttm_number Distance1 = abs( References[1].Distance );

    ttm_number* Localized_Left_x, *Localized_Left_y, *Localized_Left_z;
    if ( Localized_Left ) {
        Assert_SLAM_Writable( Localized_Left, ttc_assert_origin_auto );  // always check incoming pointer arguments
        Localized_Left_x = &( Localized_Left->X );
        Localized_Left_y = &( Localized_Left->Y );
        Localized_Left_z = &( Localized_Left->Z );
    }
    else {
        Localized_Left_x = Localized_Left_y = Localized_Left_z = NULL;
    }
    ttm_number* Localized_Right_x, *Localized_Right_y, *Localized_Right_z;
    if ( Localized_Right ) {
        Assert_SLAM_Writable( Localized_Right, ttc_assert_origin_auto ); // always check incoming pointer arguments
        Localized_Right_x = &( Localized_Right->X );
        Localized_Right_y = &( Localized_Right->Y );
        Localized_Right_z = &( Localized_Right->Z );
    }
    else {
        Localized_Right_x = Localized_Right_y = Localized_Right_z = NULL;
    }

    if ( AmountValidReferences < 3 ) {   // special case: calculate 2D position
        if ( Position0->Z == Position1->Z ) { // calculate 2D position in XY plane
            if ( ( Position0->X == 0 ) && ( Position0->Y == 0 ) ) { // simple case: Position0 == origin
                Error = ttc_math_lateration_2d(
                            Distance0,
                            Distance1,
                            Position1->X, Position1->Y,
                            Localized_Left_x,  Localized_Left_y,
                            Localized_Right_x, Localized_Right_y
                        );
            }
            else {
                if ( ( Position1->X == 0 ) && ( Position1->Y == 0 ) ) { // simple case: Position1 == origin
                    Error = ttc_math_lateration_2d(
                                References[1].Distance,
                                References[0].Distance,
                                Position0->X, Position0->Y,
                                Localized_Left_x,  Localized_Left_y,
                                Localized_Right_x, Localized_Right_y
                            );
                }
                else {                                                  // none position is origin: translate positions so that one becomes origin

                    Error = ttc_math_lateration_2d(
                                Distance0,
                                Distance1,
                                Position1->X - Position0->X, Position1->Y - Position0->Y,
                                Localized_Left_x,  Localized_Left_y,
                                Localized_Right_x, Localized_Right_y
                            );

                    if ( Localized_Left ) { // translate coordinate to correct position
                        Localized_Left->X += Position0->X;
                        Localized_Left->Y += Position0->Y;
                    }
                    if ( Localized_Right ) { // translate coordinate to correct position
                        Localized_Right->X += Position0->X;
                        Localized_Right->Y += Position0->Y;
                    }
                }
            }
            if ( Localized_Left )
            { Localized_Left->Z = Position0->Z; }
            if ( Localized_Right )
            { Localized_Right->Z = Position0->Z; }

        }
        else {                                    // calculate 2D position in XZ or YZ plane
            if ( Position0->Y == Position1->Y ) { // calculate 2D position in XZ plane
                if ( ( Position0->X == 0 ) && ( Position0->Z == 0 ) ) { // simple case: Position0 == origin
                    Error = ttc_math_lateration_2d(
                                References[0].Distance,
                                References[1].Distance,
                                Position1->X, Position1->Z,
                                Localized_Left_x,  Localized_Left_z,
                                Localized_Right_x, Localized_Right_z
                            );
                }
                else {
                    if ( ( Position1->X == 0 ) && ( Position1->Z == 0 ) ) { // simple case: Position1 == origin
                        Error = ttc_math_lateration_2d(
                                    References[1].Distance,
                                    References[0].Distance,
                                    Position0->X, Position0->Z,
                                    Localized_Left_x,  Localized_Left_z,
                                    Localized_Right_x, Localized_Right_z
                                );
                    }
                    else {                                                  // none position is origin: translate positions so that one becomes origin

                        Error = ttc_math_lateration_2d(
                                    Distance0,
                                    Distance1,
                                    Position1->X - Position0->X, Position1->Z - Position0->Z,
                                    Localized_Left_x,  Localized_Left_z,
                                    Localized_Right_x, Localized_Right_z
                                );

                        if ( Localized_Left ) { // translate coordinate to correct position
                            Localized_Left->X += Position0->X;
                            Localized_Left->Z += Position0->Z;
                        }
                        if ( Localized_Right ) { // translate coordinate to correct position
                            Localized_Right->X += Position0->X;
                            Localized_Right->Z += Position0->Z;
                        }
                    }
                }
                if ( Localized_Left )
                { Localized_Left->Y  = Position0->Y; }
                if ( Localized_Right )
                { Localized_Right->Y = Position0->Y; }
            }
            else {
                if ( Position0->X == Position1->X ) { // calculate 2D position in YZ plane
                    if ( ( Position0->Z == 0 ) && ( Position0->Y == 0 ) ) { // simple case: Position0 == origin
                        Error = ttc_math_lateration_2d(
                                    Distance0,
                                    Distance1,
                                    Position1->Z, Position1->Y,
                                    Localized_Left_z,  Localized_Left_y,
                                    Localized_Right_z, Localized_Right_y
                                );
                    }
                    else {
                        if ( ( Position1->Z == 0 ) && ( Position1->Y == 0 ) ) { // simple case: Position1 == origin
                            Error = ttc_math_lateration_2d(
                                        References[1].Distance,
                                        References[0].Distance,
                                        Position0->Z, Position0->Y,
                                        Localized_Left_z,  Localized_Left_y,
                                        Localized_Right_z, Localized_Right_y
                                    );
                        }
                        else {                                                  // none position is origin: translate positions so that one becomes origin
                            Error = ttc_math_lateration_2d(
                                        Distance0,
                                        Distance1,
                                        Position1->Z - Position0->Z, Position1->Y - Position0->Y,
                                        Localized_Left_z,  Localized_Left_y,
                                        Localized_Right_z, Localized_Right_y
                                    );

                            if ( Localized_Left ) { // translate coordinate to correct position
                                Localized_Left->Z += Position0->Z;
                                Localized_Left->Y += Position0->Y;
                            }
                            if ( Localized_Right ) { // translate coordinate to correct position
                                Localized_Right->Z += Position0->Z;
                                Localized_Right->Y += Position0->Y;
                            }
                        }
                    }
                    if ( Localized_Left )
                    { Localized_Left->X  = Position0->X; }
                    if ( Localized_Right )
                    { Localized_Right->X = Position0->X; }
                }
                else {
                    ttc_assert_halt_origin( ttc_assert_origin_auto ); // 2D localization only supported for orthogonal planes!
                }
            }
        }
    }
    else {
        t_ttc_math_vector3d_xyz* Position2 = &( ReferenceNodes[2]->Position );
        t_ttc_math_lateration_3d Rotated;
        BOOL PositionsTranslated = FALSE;
        if ( ( Position0->X != 0 ) || ( Position0->Y != 0 ) || ( Position0->Z != 0 ) ) { // Position0 is not origin: subtract from Position1, Position2
            ttc_math_vector3d_sub( Position1, Position0 );
            ttc_math_vector3d_sub( Position2, Position0 );
            PositionsTranslated = TRUE;
        }
        ttm_number Distance2 = abs( References[2].Distance );

        Error = ttc_math_lateration_3d( &Rotated,
                                        FALSE,
                                        Position1,
                                        Position2,
                                        Distance0,
                                        Distance1,
                                        Distance2,
                                        Localized_Left,
                                        Localized_Right
                                      );

        if ( PositionsTranslated ) { // translate result back by Position0
            if ( Localized_Left )  {
                ttc_math_vector3d_add( Localized_Left, Position0 );
            }
            if ( Localized_Right ) {
                ttc_math_vector3d_add( Localized_Right, Position0 );
            }
        }
        if ( AmountValidReferences > 3 ) { // sort Localized_Left, Localized_Right by increasing distance to ReferenceNodes[3]
            t_ttc_math_vector3d_xyz* Position3 = &( ReferenceNodes[3]->Position );
            ttm_number Distance_Left  = ttc_math_vector3d_distance( Localized_Left, Position3 );
            ttm_number Distance_Right = ttc_math_vector3d_distance( Localized_Left, Position3 );
            if ( Distance_Right < Distance_Left ) {
                t_ttc_math_vector3d_xyz Memo;
                ttc_math_vector3d_copy( &Memo, Localized_Left );
                ttc_math_vector3d_copy( Localized_Left, Localized_Right );
                ttc_math_vector3d_copy( Localized_Left, &Memo );
            }
        }
    }

    return Error;
}
e_ttc_slam_errorcode     ttc_slam_load_defaults( t_u8 LogicalIndex ) {
    t_ttc_slam_config* Config = ttc_slam_get_configuration( LogicalIndex );

    if ( Config->Flags.Initialized )
    { ttc_slam_deinit( LogicalIndex ); }

    ttc_memory_set( Config, 0, sizeof( t_ttc_slam_config ) );

    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    switch ( LogicalIndex ) { // physical index of slam device from static configuration
#ifdef TTC_SLAM1
        case  1: Config->PhysicalIndex = TTC_SLAM1; break;
#endif
#ifdef TTC_SLAM2
        case  2: Config->PhysicalIndex = TTC_SLAM2; break;
#endif
#ifdef TTC_SLAM3
        case  3: Config->PhysicalIndex = TTC_SLAM3; break;
#endif
#ifdef TTC_SLAM4
        case  4: Config->PhysicalIndex = TTC_SLAM4; break;
#endif
#ifdef TTC_SLAM5
        case  5: Config->PhysicalIndex = TTC_SLAM5; break;
#endif
#ifdef TTC_SLAM6
        case  6: Config->PhysicalIndex = TTC_SLAM6; break;
#endif
#ifdef TTC_SLAM7
        case  7: Config->PhysicalIndex = TTC_SLAM7; break;
#endif
#ifdef TTC_SLAM8
        case  8: Config->PhysicalIndex = TTC_SLAM8; break;
#endif
#ifdef TTC_SLAM9
        case  9: Config->PhysicalIndex = TTC_SLAM9; break;
#endif
#ifdef TTC_SLAM10
        case 10: Config->PhysicalIndex = TTC_SLAM10; break;
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }

    //Insert additional generic default values here ...
    switch ( Config->LogicalIndex ) { // load settings from static configuration
#ifdef TTC_SLAM1
        case 1: Config->Init.AmountRawMeasures = TTC_SLAM1_AMOUNT_RAW_MEASURES; break;
#endif
#ifdef TTC_SLAM2
        case 2: Config->Init.AmountRawMeasures = TTC_SLAM2_AMOUNT_RAW_MEASURES; break;
#endif
#ifdef TTC_SLAM3
        case 3: Config->Init.AmountRawMeasures = TTC_SLAM3_AMOUNT_RAW_MEASURES; break;
#endif
#ifdef TTC_SLAM4
        case 4: Config->Init.AmountRawMeasures = TTC_SLAM4_AMOUNT_RAW_MEASURES; break;
#endif
#ifdef TTC_SLAM5
        case 5: Config->Init.AmountRawMeasures = TTC_SLAM5_AMOUNT_RAW_MEASURES; break;
#endif
#ifdef TTC_SLAM6
        case 6: Config->Init.AmountRawMeasures = TTC_SLAM6_AMOUNT_RAW_MEASURES; break;
#endif
#ifdef TTC_SLAM7
        case 7: Config->Init.AmountRawMeasures = TTC_SLAM7_AMOUNT_RAW_MEASURES; break;
#endif
#ifdef TTC_SLAM8
        case 8: Config->Init.AmountRawMeasures = TTC_SLAM8_AMOUNT_RAW_MEASURES; break;
#endif
#ifdef TTC_SLAM9
        case 9: Config->Init.AmountRawMeasures = TTC_SLAM9_AMOUNT_RAW_MEASURES; break;
#endif
#ifdef TTC_SLAM10
        case 10: Config->Init.AmountRawMeasures = TTC_SLAM10_AMOUNT_RAW_MEASURES; break;
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid static configuration or invalid logical index. Check implementation!
    }

    // Let low-level driver initialize remaining fields
    Config->LastError = _driver_slam_load_defaults( Config );

    // Check mandatory configuration items
    Assert_SLAM_EXTRA( ( Config->Architecture > ta_slam_None ) && ( Config->Architecture < ta_slam_unknown ), ttc_assert_origin_auto );  // architecture not set properly! Low-Level driver must set this field!

    return Config->LastError;
}
void                     ttc_slam_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)

    A_reset( ttc_slam_configs, TTC_SLAM_AMOUNT ); // safe arrays are placed in data section and must be initialized manually

    _driver_slam_prepare();
}
void                     ttc_slam_reset( t_u8 LogicalIndex ) {
    t_ttc_slam_config* Config = ttc_slam_get_configuration( LogicalIndex );

    // reset coordinates of all nodes
    for ( t_u16 NodeIndex = Config->Init.Amount_Nodes; NodeIndex > 0; NodeIndex-- ) {
        t_ttc_slam_node* Node = ttc_slam_get_node( LogicalIndex, NodeIndex );

        Node->Position.X = TTC_SLAM_UNDEFINED_VALUE;
        Node->Position.Y = TTC_SLAM_UNDEFINED_VALUE;
        Node->Position.Z = TTC_SLAM_UNDEFINED_VALUE;
    }

    // reset generic configuration items
    ttc_memory_set( &( Config->Flags ), 0, sizeof( Config->Flags ) );

    // let low-level driver reset itself
    _driver_slam_reset( Config );
}
void                     ttc_slam_update_distance( t_u8 LogicalIndex, t_u16 NodeIndexA, t_u16 NodeIndexB, ttm_number RawValue ) {
    t_ttc_slam_config* Config = ttc_slam_get_configuration( LogicalIndex );
    Assert_SLAM( ( NodeIndexA > 0 ) && ( NodeIndexA <= Config->Init.Amount_Nodes ), ttc_assert_origin_auto ); // invalid node index. Check implementation of caller!
    Assert_SLAM( ( NodeIndexB > 0 ) && ( NodeIndexB <= Config->Init.Amount_Nodes ), ttc_assert_origin_auto ); // invalid node index. Check implementation of caller!
    Assert_SLAM( NodeIndexA != NodeIndexB, ttc_assert_origin_auto ); // Cannot update distance within same node. Check caller implementation!

    _driver_slam_update_distance( Config, NodeIndexA, NodeIndexB, RawValue );
}
void                     ttc_slam_update_coordinates( t_u8 LogicalIndex, t_u16 NodeIndex, ttm_number X, ttm_number Y, ttm_number Z ) {

    t_ttc_slam_config* Config = ttc_slam_get_configuration( LogicalIndex );
    Assert_SLAM( NodeIndex <= Config->Init.Amount_Nodes, ttc_assert_origin_auto ); // invalid node index. Check implementation of caller!
    Assert_SLAM( NodeIndex > 0,                          ttc_assert_origin_auto ); // invalid node index. Check implementation of caller!

    if ( ( X != TTC_MATH_CONST_NAN ) && ( Y != TTC_MATH_CONST_NAN ) && ( Z != TTC_MATH_CONST_NAN ) ) { // all coordinates are valid: update in model
        _driver_slam_update_coordinates( Config, NodeIndex, X, Y, Z );

        if ( 1 ) { // update bounding box if required
            t_ttc_slam_node* Node = _driver_slam_get_node( Config, NodeIndex );

            if ( Node->Flags.PositionValid ) {
                if ( Config->BoundingBox.X_Min > Node->Position.X )  { Config->BoundingBox.X_Min = Node->Position.X; }
                if ( Config->BoundingBox.X_Max < Node->Position.X )  { Config->BoundingBox.X_Max = Node->Position.X; }

                if ( Config->BoundingBox.Y_Min > Node->Position.Y )  { Config->BoundingBox.Y_Min = Node->Position.Y; }
                if ( Config->BoundingBox.Y_Max < Node->Position.Y )  { Config->BoundingBox.Y_Max = Node->Position.Y; }

                if ( Config->BoundingBox.Z_Min > Node->Position.Z )  { Config->BoundingBox.Z_Min = Node->Position.Z; }
                if ( Config->BoundingBox.Z_Max < Node->Position.Z )  { Config->BoundingBox.Z_Max = Node->Position.Z; }
            }
        }
    }
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_slam(t_u8 LogicalIndex) {  }

void _ttc_slam_configuration_check( t_u8 LogicalIndex ) {
    t_ttc_slam_config* Config = ttc_slam_get_configuration( LogicalIndex );

    // perform some generic plausibility checks
    const t_ttc_slam_features* Features = Features;

    /** Example plausibility check

    if (Config->BitRate > Features->MaxBitRate)   Config->BitRate = Features->MaxBitRate;
    if (Config->BitRate < Features->MinBitRate)   Config->BitRate = Features->MinBitRate;
    */
    // add more architecture independent checks...
    Assert_SLAM( Config->Init.Amount_Nodes > 1, ttc_assert_origin_auto );  // must be set by application to sensefull value!

    // let low-level driver check this configuration too
    _driver_slam_configuration_check( Config );
}
void _ttc_slam_mapping_completed( t_ttc_slam_config* Config ) {
    Assert_SLAM_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments

    if ( Config->Init.Function_MappingComplete ) {
        Assert_Executable( Config->Init.Function_MappingComplete, ttc_assert_origin_auto ); // this does not seem to be a function pointer!
        Config->Init.Function_MappingComplete( Config );
    }
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
