/** { ttc_sdcard.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for sdcard devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Most functions of this driver are defined as macros and being implemented
 *  in interfaces/ttc_sdcard_interface.c or in low-level drivers sdcard/sdcard_*.c.
 *
 *  See corresponding ttc_sdcard.h for documentation of all high-level functions.
 *
 *  Created from template ttc_device.c revision 51 at 20180130 09:42:26 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_sdcard.h".
//
#include "ttc_sdcard.h"
#include "sdcard/sdcard_common.h"
#include "ttc_spi.h"
#include "ttc_heap.h"       // dynamic memory allocationand safe arrays
#include "ttc_task.h"       // disable/ enable multitasking scheduler
#include "ttc_systick.h"    // precise delays from single systick timer
#ifdef EXTENSION_ttc_interrupt
    #include "ttc_interrupt.h"  // globally disable/ enable interrupts
#endif

//}Includes

#if TTC_SDCARD_AMOUNT == 0
    #warning No SDCARD devices defined, cdid you forget to activate something? - Define at least TTC_SDCARD1 as one from e_ttc_sdcard_architecture in your makefile!
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of sdcard devices.
 *
 */


// for each initialized device, a pointer to its generic and architecture specific definitions is stored
A_define( t_ttc_sdcard_config*, ttc_sdcard_configs, TTC_SDCARD_AMOUNT );

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ********************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_sdcard(t_ttc_sdcard_config* Config)

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * @param LogicalIndex    1.. logical index of SDCARD device. Each logical device <n> is defined via TTC_SDCARD<n>* constants in compile_options.h and extensions.active/makefile
 */
void _ttc_sdcard_configuration_check( t_u8 LogicalIndex );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

t_u32                    ttc_sdcard_blocks_count( t_u8 LogicalIndex ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    return sdcard_common_blocks_count( Config );
}
e_ttc_sdcard_errorcode   ttc_sdcard_block_write( t_u8 LogicalIndex, t_u32 Address, const t_u8* Buffer, t_u16 BufferSize ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    return sdcard_common_block_write( Config, Address, Buffer, BufferSize );
}
e_ttc_sdcard_errorcode   ttc_sdcard_block_read( t_u8 LogicalIndex, t_u32 Address, t_u8* Buffer, t_u16 BufferSize ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    return sdcard_common_block_read( Config, Address, Buffer, BufferSize );
}
t_u16                    ttc_sdcard_blocks_size( t_u8 LogicalIndex ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    return sdcard_common_blocks_size( Config );
}
u_ttc_sdcard_response    ttc_sdcard_command_application_r1( t_u8 LogicalIndex, e_ttc_sdcard_command_application ApplicationCommand, t_u32 Argument ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );
    Assert_SDCARD( ( ApplicationCommand > 0 ) && ( ApplicationCommand < E_ttc_sdcard_acmd_unknown ), ttc_assert_origin_auto );

    if ( ttc_sdcard_wait_busy( LogicalIndex ) )
    { sdcard_common_command_application_r1( Config, ApplicationCommand, Argument ); }
    return Config->Card.LastResponse;
}
u_ttc_sdcard_response    ttc_sdcard_command_application_rn( t_u8 LogicalIndex, e_ttc_sdcard_command_application ApplicationCommand, t_u32 Argument, t_u8* Buffer, t_u16 Amount ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );
    Assert_SDCARD( ( ApplicationCommand > 0 ) && ( ApplicationCommand < E_ttc_sdcard_acmd_unknown ), ttc_assert_origin_auto );

    if ( ttc_sdcard_wait_busy( LogicalIndex ) )
    { sdcard_common_command_application_rn( Config, ApplicationCommand, Argument, Buffer, Amount ); }
    return Config->Card.LastResponse;
}
u_ttc_sdcard_response    ttc_sdcard_command_r1( t_u8 LogicalIndex, e_ttc_sdcard_command Command, t_u32 Argument ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    if ( ttc_sdcard_wait_busy( LogicalIndex ) )
    { sdcard_common_command_r1( Config, Command, Argument ); }

    return Config->Card.LastResponse;
}
t_u16                    ttc_sdcard_command_rn( t_u8 LogicalIndex, e_ttc_sdcard_command Command, t_u32 Argument, t_u8* Buffer, t_u16 Amount ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    if ( ttc_sdcard_wait_busy( LogicalIndex ) )
    { return sdcard_common_command_rn( Config, Command, Argument, Buffer, Amount ); }

    return 0;
}
t_ttc_sdcard_config*     ttc_sdcard_create() {

    t_u8 LogicalIndex = ttc_sdcard_get_max_index();
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    TTC_TASK_RETURN( Config ); // will perform stack overflow check + return value
}
void                     ttc_sdcard_deinit( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration

    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    if ( Config->Flags.Initialized ) {
        e_ttc_sdcard_errorcode Result = _driver_sdcard_deinit( Config );
        if ( Result == E_ttc_sdcard_errorcode_OK )
        { Config->Flags.Initialized = 0; }
    }
    ttc_task_critical_end(); // other tasks now may access this driver
}
e_ttc_sdcard_errorcode   ttc_sdcard_disable_crc( t_u8 LogicalIndex, BOOL DisableCRC ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    sdcard_common_disable_crc( Config, DisableCRC );
    if ( Config->Card.LastResponse.Byte )
    { return E_ttc_sdcard_errorcode_card_crc_on_off; }

    return 0;
}
t_ttc_sdcard_config*     ttc_sdcard_get_configuration( t_u8 LogicalIndex ) {
    Assert_SDCARD( LogicalIndex > 0, ttc_assert_origin_auto ); // logical index starts at 1
    Assert_SDCARD( LogicalIndex <= TTC_SDCARD_AMOUNT, ttc_assert_origin_auto ); // invalid index given (did you configure engnough ttc_sdcard devices in your makefile?)
    t_ttc_sdcard_config* Config = A( ttc_sdcard_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_disable();                      // ensure that we are not interrupted while creating configuration
#endif

        Config = A( ttc_sdcard_configs, LogicalIndex - 1 ); // have to reload with disabled interrupts to avoid racing conditions
        if ( !Config ) {
            Config = A( ttc_sdcard_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_sdcard_config ) );
            Config->Flags.Initialized = 0;             // make sure that deinit() is not called during load_defaults()
            ttc_sdcard_load_defaults( LogicalIndex );
        }

#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_enable();
#endif
    }

    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // memory corrupted?
    TTC_TASK_RETURN( Config ); // will perform stack overflow check + return value
}
t_u8                     ttc_sdcard_get_max_index() {
    TTC_TASK_RETURN( TTC_SDCARD_AMOUNT ); // will perform stack overflow check + return value
}
e_ttc_sdcard_errorcode   ttc_sdcard_init( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration
#if (TTC_ASSERT_SDCARD_EXTRA == 1)
    t_u8 InitialCriticalLevel = ttc_interrupt_critical_level();
#endif
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    // check configuration to meet all limits of current architecture
    _ttc_sdcard_configuration_check( LogicalIndex );

    Config->LastError = _driver_sdcard_init( Config );
    if ( ! Config->LastError ) // == 0
    { Config->Flags.Initialized = 1; }

    Assert_SDCARD( Config->Architecture != 0, ttc_assert_origin_auto ); // Low-Level driver must set this value!
    Assert_SDCARD_EXTRA( InitialCriticalLevel == ttc_interrupt_critical_level(), ttc_assert_origin_auto ); // amount of  ttc_task_critical_end() calls does not match. Check driver implementation!
    ttc_task_critical_end(); // other tasks now may access this driver

    TTC_TASK_RETURN( Config->LastError ); // will perform stack overflow check + return value
}
e_ttc_sdcard_errorcode   ttc_sdcard_load_defaults( t_u8 LogicalIndex ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    u_ttc_sdcard_architecture* LowLevelConfig = NULL;
    if ( Config->Flags.Initialized ) {
        ttc_sdcard_deinit( LogicalIndex );
        LowLevelConfig = Config->LowLevelConfig; // rescue pointer to dynamic allocated memory
    }

    ttc_memory_set( Config, 0, sizeof( t_ttc_sdcard_config ) );

    // restore pointer to dynamic allocated memory
    Config->LowLevelConfig = LowLevelConfig;

    // load generic default values
    Config->LogicalIndex  = LogicalIndex;

    //Insert additional generic default values here ...
    Config->Init.Retries = 100;

    e_ttc_sdcard_voltage VoltageMin, VoltageMax;

    switch ( LogicalIndex ) { // obtain minimum + maximum supported voltages from static configuration

#ifdef TTC_SDCARD1
        case 1: {
            VoltageMin = TTC_SDCARD1_VOLTAGE_MIN;
            VoltageMax = TTC_SDCARD1_VOLTAGE_MAX;
            break;
        }
#endif
#ifdef TTC_SDCARD2
        case 2: {
            VoltageMin = TTC_SDCARD2_VOLTAGE_MIN;
            VoltageMax = TTC_SDCARD2_VOLTAGE_MAX;
            break;
        }
#endif
#ifdef TTC_SDCARD3
        case 3: {
            VoltageMin = TTC_SDCARD3_VOLTAGE_MIN;
            VoltageMax = TTC_SDCARD3_VOLTAGE_MAX;
            break;
        }
#endif
#ifdef TTC_SDCARD4
        case 4: {
            VoltageMin = TTC_SDCARD4_VOLTAGE_MIN;
            VoltageMax = TTC_SDCARD4_VOLTAGE_MAX;
            break;
        }
#endif
#ifdef TTC_SDCARD5
        case 5: {
            VoltageMin = TTC_SDCARD5_VOLTAGE_MIN;
            VoltageMax = TTC_SDCARD5_VOLTAGE_MAX;
            break;
        }
#endif
#ifdef TTC_SDCARD6
        case 6: {
            VoltageMin = TTC_SDCARD6_VOLTAGE_MIN;
            VoltageMax = TTC_SDCARD6_VOLTAGE_MAX;
            break;
        }
#endif
#ifdef TTC_SDCARD7
        case 7: {
            VoltageMin = TTC_SDCARD7_VOLTAGE_MIN;
            VoltageMax = TTC_SDCARD7_VOLTAGE_MAX;
            break;
        }
#endif
#ifdef TTC_SDCARD8
        case 8: {
            VoltageMin = TTC_SDCARD8_VOLTAGE_MIN;
            VoltageMax = TTC_SDCARD8_VOLTAGE_MAX;
            break;
        }
#endif
#ifdef TTC_SDCARD9
        case 9: {
            VoltageMin = TTC_SDCARD9_VOLTAGE_MIN;
            VoltageMax = TTC_SDCARD9_VOLTAGE_MAX;
            break;
        }
#endif
#ifdef TTC_SDCARD10
        case 10: {
            VoltageMin = TTC_SDCARD10_VOLTAGE_MIN;
            VoltageMax = TTC_SDCARD10_VOLTAGE_MAX;
            break;
        }
#endif
        default: VoltageMin = VoltageMax = 0; break;
    }
    Assert_SDCARD( ( VoltageMin > 0 ) && ( VoltageMin < E_ttc_sdcard_voltage_unknown ), ttc_assert_origin_auto ); // invalid voltage setting. Check makefile!
    Assert_SDCARD( ( VoltageMax > 0 ) && ( VoltageMax < E_ttc_sdcard_voltage_unknown ), ttc_assert_origin_auto ); // invalid voltage setting. Check makefile!
    Assert_SDCARD( ( VoltageMax >= VoltageMin ), ttc_assert_origin_auto ); // invalid voltage setting. Check makefile!

    if ( ( VoltageMin < E_ttc_sdcard_voltage_2V8 ) && ( VoltageMax >= E_ttc_sdcard_voltage_2V8 ) )
    { Config->Init.Flags.Support_Vdd_27_28 = 1; }
    if ( ( VoltageMin < E_ttc_sdcard_voltage_2V9 ) && ( VoltageMax >= E_ttc_sdcard_voltage_2V9 ) )
    { Config->Init.Flags.Support_Vdd_28_29 = 1; }
    if ( ( VoltageMin < E_ttc_sdcard_voltage_3V0 ) && ( VoltageMax >= E_ttc_sdcard_voltage_3V0 ) )
    { Config->Init.Flags.Support_Vdd_29_30 = 1; }
    if ( ( VoltageMin < E_ttc_sdcard_voltage_3V1 ) && ( VoltageMax >= E_ttc_sdcard_voltage_3V1 ) )
    { Config->Init.Flags.Support_Vdd_30_31 = 1; }
    if ( ( VoltageMin < E_ttc_sdcard_voltage_3V2 ) && ( VoltageMax >= E_ttc_sdcard_voltage_3V2 ) )
    { Config->Init.Flags.Support_Vdd_31_32 = 1; }
    if ( ( VoltageMin < E_ttc_sdcard_voltage_3V3 ) && ( VoltageMax >= E_ttc_sdcard_voltage_3V3 ) )
    { Config->Init.Flags.Support_Vdd_32_33 = 1; }
    if ( ( VoltageMin < E_ttc_sdcard_voltage_3V4 ) && ( VoltageMax >= E_ttc_sdcard_voltage_3V4 ) )
    { Config->Init.Flags.Support_Vdd_33_34 = 1; }
    if ( ( VoltageMin < E_ttc_sdcard_voltage_3V5 ) && ( VoltageMax >= E_ttc_sdcard_voltage_3V5 ) )
    { Config->Init.Flags.Support_Vdd_34_35 = 1; }
    if ( ( VoltageMin <= E_ttc_sdcard_voltage_3V6 ) && ( VoltageMax >= E_ttc_sdcard_voltage_3V6 ) )
    { Config->Init.Flags.Support_Vdd_35_36 = 1; }

    // Let low-level driver initialize remaining fields
    Config->LastError = _driver_sdcard_load_defaults( Config );

    // Check mandatory configuration items
    Assert_SDCARD_EXTRA( ( Config->Architecture > E_ttc_sdcard_architecture_None ) && ( Config->Architecture < E_ttc_sdcard_architecture_unknown ), ttc_assert_origin_auto ); // architecture not set properly! Low-Level driver must set this field!

    TTC_TASK_RETURN( Config->LastError ); // will perform stack overflow check + return value
}
e_ttc_storage_event      ttc_sdcard_medium_detect( t_u8 LogicalIndex ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    return sdcard_common_medium_detect( Config );
}
e_ttc_sdcard_errorcode   ttc_sdcard_medium_mount( t_u8 LogicalIndex ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    return sdcard_common_medium_mount( Config, NULL );
}
e_ttc_sdcard_errorcode   ttc_sdcard_medium_unmount( t_u8 LogicalIndex ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    return sdcard_common_medium_unmount( Config );
}
void                     ttc_sdcard_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)

    A_reset( ttc_sdcard_configs, TTC_SDCARD_AMOUNT ); // safe arrays are placed in data section and must be initialized manually

    if ( 1 ) {                 // optional: register this device for a sysclock change update
        ttc_sysclock_register_for_update( ttc_sdcard_sysclock_changed );
    }
    if ( 0 ) {                 // speed benchmark of current sdcard_common_crc7_calculate() implementation
#ifdef TTC_LED1
        ttc_gpio_init( TTC_LED1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
        while ( 1 ) {
            ttc_gpio_set( TTC_LED1 );
            // calculate crc7 of some dummy bytes
            sdcard_common_crc7_calculate( ( t_u8* ) ttc_sdcard_configs.Data, 1024, 0 );

            ttc_gpio_clr( TTC_LED1 );
            // calculate crc7 of some dummy bytes
            sdcard_common_crc7_calculate( ( t_u8* ) ttc_sdcard_configs.Data, 1024, 0 );
        }
#endif
    }

    sdcard_common_prepare();
    _driver_sdcard_prepare();
}
void                     ttc_sdcard_reset( t_u8 LogicalIndex ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    ttc_sdcard_medium_unmount( LogicalIndex );
    _driver_sdcard_reset( Config );
}
void                     ttc_sdcard_sysclock_changed() {

    // deinit + reinit all initialized SDCARD devices
    for ( t_u8 LogicalIndex = 1; LogicalIndex < ttc_sdcard_get_max_index(); LogicalIndex++ ) {
        t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );
        if ( Config->Flags.Initialized ) {
            ttc_sdcard_deinit( LogicalIndex );
            ttc_sdcard_init( LogicalIndex );
        }
    }
}
t_ttc_sdcard_response_r2 ttc_sdcard_status( t_u8 LogicalIndex ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    sdcard_common_status( Config );

    return Config->Card.LastResponse.R2; // t_ttc_sdcard_status
}
BOOL                     ttc_sdcard_wait_busy( t_u8 LogicalIndex ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    // pass function call to interface or low-level driver function
    return sdcard_common_wait_busy( Config );
}
e_ttc_sdcard_errorcode   ttc_sdcard_blocks_read( t_ttc_sdcard_config* Config, t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_SDCARD_Writable( Buffer, ttc_assert_origin_auto ); // always check incoming pointer arguments

    return sdcard_common_blocks_read( Config, Buffer, Address, AmountBlocks );
}
e_ttc_sdcard_errorcode   ttc_sdcard_blocks_write( t_ttc_sdcard_config* Config, const t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_SDCARD_Readable( Buffer, ttc_assert_origin_auto ); // always check incoming pointer arguments

    return sdcard_common_blocks_write( Config, Buffer, Address, AmountBlocks );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_sdcard(t_u8 LogicalIndex) {  }

void _ttc_sdcard_configuration_check( t_u8 LogicalIndex ) {
    t_ttc_sdcard_config* Config = ttc_sdcard_get_configuration( LogicalIndex );

    // perform some generic plausibility checks
    const t_ttc_sdcard_features* Features = Features;

    /** Example plausibility check

    if (Config->BitRate > Features->MaxBitRate)   Config->BitRate = Features->MaxBitRate;
    if (Config->BitRate < Features->MinBitRate)   Config->BitRate = Features->MinBitRate;
    */
    // add more architecture independent checks...
    Assert_SDCARD( LogicalIndex == Config->LogicalIndex, ttc_assert_origin_auto ); // configuration must store corresponding logical index!

    // let low-level driver check this configuration too
    _driver_sdcard_configuration_check( Config );
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
