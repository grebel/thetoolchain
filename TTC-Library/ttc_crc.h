#ifndef TTC_CRC_H
#define TTC_CRC_H
/** { ttc_crc.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Created from template ttc_device.h revision 39 at 20180323 10:28:15 UTC
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of ttc_crc (Do not delete this line!)
 *
 * High-Level ttc_crc Layer
 *   Provides different cyclic redundancy checksum (CRC) algorithms allowing
 *   to check integrity of data being transfered over communication interfaces
 *   or being read from external storage. Different crc algorithms have been
 *   standardized for different applications. See individual descriptions for
 *   more details!
 *
 * General Use of ttc_crc Funcions
 *   Each function takes
 *   (1) Pointer to byte oriented data
 *   (2) Amount of bytes from data to calculate crc for
 *   (3) Previous CRC value
 *       This value gives the starting value for every new checksum calculation.
 *       It also allows to calculate a single checksum over data in different
 *       buffers as shown in the example section below.
 *
 * Different Low-Level CRC Drivers
 *   Implementations for software based crc calculation
 *   can either be tuned for maximum speed or small code size and RAM usage.
 *   You can switch your favourite optimization by selecting the best low-level
 *   driver for your application.
 *   Check description in crc/crc_*.h header files of available low-level drivers
 *   for more details!
 *
 * Example Usage
 *
}*/

#ifndef EXTENSION_ttc_crc
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_crc.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_crc.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_crc_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level crc only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function Declarations ************************************************{
 *
 * The functions declared below provide the main interface for
 * crc devices on all supported architectures.
 * Check crc/crc_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares crc Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_crc_prepare();
void _driver_crc_prepare();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @return                configuration of indexed device
 */
t_ttc_crc_config* ttc_crc_get_configuration( );

/** initialize indexed device for use
 *
 * @return                == 0: crc device has been initialized successfully; != 0: error-code
 */
e_ttc_crc_errorcode ttc_crc_init( );

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_crc_deinit() if device has been initialized.
 *
 * @return                == 0: default values have been loaded successfully for indexed crc device; != 0: error-code
 */
e_ttc_crc_errorcode  ttc_crc_load_defaults( );

/** Calculate 7-bit checksum for given raw data
 *
 * Generic use
 *     // calculate new 7-bit checksum
 *     t_u8 CRC_SDCard = ttc_crc_crc7_calculate(Buffer, Amount, 0);
 *
 * SDCARD use
 *     This checksum can be used to calculate sdcard command sequences.
 *     For sdcard usage, a special pre- and postprocessing is required:
 *     // calculate new checksum for sdcard commands
 *     t_u8 CRC_SDCard = (ttc_crc_crc7_calculate(Buffer, Amount, 0) << 1) | 1;
 *
 *     // continue calculation for additional data
 *     CRC_SDCard = (ttc_crc_crc7_calculate(Buffer2, Amount2, CRC_SDCard >> 1) << 1) | 1;
 *
 * @param Buffer     (t_u8*) Data buffer to send or check
 * @param Amount     (t_u16) amount of valid bytes in Buffer[] (except crc byte)
 * @param StartValue (t_u8)  initial CRC7 value (=0 to calculate new crc7; return value from previous call when continuing calculation)
 * @return           (t_u8)  calculated 7-bit crc value of Buffer[0..Amount-1] (for SDCard usage: t_u8 CRC = ( ttc_crc_crc7_calculate(...) << 1 ) | 1; )
 */
t_u8 ttc_crc_crc7_calculate( const t_u8* Buffer, t_u16 Amount, t_u8 StartValue );
t_u8 _driver_crc_crc7_calculate( const t_u8* Buffer, t_u16 Amount, t_u8 StartValue );
#define ttc_crc_crc7_calculate  _driver_crc_crc7_calculate  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Calculate 7-bit checksum for given raw data and compare with last byte in buffer
 *
 * Generic use
 *     // calculate 7-bit checksum for all Buffer[0..Amount-1] and compare with Buffer[Amount]
 *     if ( ttc_crc_crc7_check(Buffer, Amount, 0) ) { ... } // check successfull
 *
 * SDCARD use
 *     This checksum can be used to calculate sdcard command sequences.
 *     For sdcard usage, a special pre- and postprocessing is required:
 *     Buffer[Amount] >>= 1; // convert special sdcard crc back to 7 bit crc before check
 *     if ( ttc_crc_crc7_check(Buffer, Amount, 0) ) { ... } // check successfull
 *
 * @param Buffer     (t_u8*) Data buffer to send or check
 * @param Amount     (t_u16) amount of valid bytes in Buffer[] (except crc byte)
 * @param StartValue (t_u8)  initial CRC7 value (=0 to calculate new crc7; return value from previous call when continuing calculation)
 * @return           (BOOL)  ==TRUE: crc of data in Buffer[] matches to Buffer[Amount]; ==FALSE: CRC7 != Buffer[Amount]
 */
BOOL ttc_crc_crc7_check( const t_u8* Buffer, t_u16 Amount, t_u8 StartValue );
BOOL _driver_crc_crc7_check( const t_u8* Buffer, t_u16 Amount, t_u8 StartValue );
#define ttc_crc_crc7_check  _driver_crc_crc7_check  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Calculate 16-bit checksum for given raw data (SDCARD compliant)
 *
 * Polynomial: 0x1021
 * StartValue: 0x0000 (typical)
 *
 * This implementation is compatible to sdcards and most other applications.
 * Though, it seems to have a defect that makes it not compliant with CRC16-CCITT specification.
 * Example CRCs (StartValue=0x0000):
 * (1) 512 bytes of 0xff -> CRC16=0x7fa1 (as described in SDCARD technical specification)
 * (2) "123456789"       -> CRC16=0xe5cc (opposed to http://srecord.sourceforge.net/crc16-ccitt.html)
 *
 * @param Buffer     (t_u8*)  Buffer storing raw data to calculate checksum from
 * @param Amount     (t_u32)  amount of valid bytes in Buffer[] (except crc value)
 * @param StartValue (t_u16)  initial CRC16 value (=0 to calculate new crc16; return value from previous call when continuing calculation)
 * @return           (t_u16)  calculated CRC16 value of Buffer[0..Amount-1] based on given start value.
 */
t_u16 ttc_crc_crc16_calculate_0x1021( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue );
t_u16 _driver_crc_crc16_calculate_0x1021( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue );
#define ttc_crc_crc16_calculate_0x1021  _driver_crc_crc16_calculate_0x1021  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Calculate 16-bit checksum for given raw data and compare it with desired crc16 value at end of data (SDCARD compliant)
 *
 * Polynomial: 0x1021
 * StartValue: 0x0000 (typical)
 *
 * The last two bytes Buffer[Amount] and Buffer[Amount+1] are read as desired CRC16 value:
 * t_u16 DesiredCRC16 = *( (t_u16*) (& Buffer[Amount]) );
 *
 * This implementation is compatible to sdcards and most other applications.
 * Though, it seems to have a defect that makes it not compliant with CRC16-CCITT specification.
 * Example CRCs (StartValue=0x0000):
 * (1) 512 bytes of 0xff -> CRC16==0x7fa1 (as described in SDCARD technical specification)
 * (2) "123456789"       -> CRC16==0x29b1 (opposed to http://srecord.sourceforge.net/crc16-ccitt.html)
 *
 * @param Buffer     (t_u8*)  Buffer storing raw data to calculate checksum from + 16 bit checksum value in Buffer[Amount], Buffer[Amount+1]
 * @param Amount     (t_u32)  amount of valid bytes in Buffer[] (except crc value)
 * @param StartValue (t_u16)  initial CRC16 value (=0 to calculate new crc16; return value from previous call when continuing calculation)
 * @return           (BOOL)   ==TRUE: CRC16 of data in Buffer[] matches to DesiredCRC16; ==FALSE: CRC16 != DesiredCRC16
 */
BOOL ttc_crc_crc16_check_0x1021( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue );
BOOL _driver_crc_crc16_check_0x1021( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue );
#define ttc_crc_crc16_check_0x1021  _driver_crc_crc16_check_0x1021  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Calculate 16-bit checksum for given raw data for polynomial 0x1021 (not SDCARD compliant)
 *
 * Polynomial: 0x1021
 * StartValue: 0xffff (typical)
 *
 * This implementation is compatible to sdcards and most other applications.
 * Though, it seems to have a defect that makes it not compliant with CRC16-CCITT specification.
 * Example CRCs (StartValue=0xffff):
 *
 * @param Buffer     (t_u8*)  Buffer storing raw data to calculate checksum from
 * @param Amount     (t_u32)  amount of valid bytes in Buffer[] (except crc value)
 * @param StartValue (t_u16)  initial CRC16 value (=0 to calculate new crc16; return value from previous call when continuing calculation)
 * @return           (t_u16)  calculated CRC16 value of Buffer[0..Amount-1] based on given start value.
 */
t_u16 ttc_crc_crc16_calculate_ccitt( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue );
t_u16 _driver_crc_crc16_calculate_ccitt( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue );
#define ttc_crc_crc16_calculate_ccitt  _driver_crc_crc16_calculate_ccitt  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Calculate 16-bit checksum for given raw data and compare it with desired crc16 value at end of data (not SDCARD compliant)
 *
 * Polynomial: 0x1021
 * StartValue: 0xffff (typical)
 *
 * The last two bytes Buffer[Amount] and Buffer[Amount+1] are read as desired CRC16 value:
 * t_u16 DesiredCRC16 = *( (t_u16*) (& Buffer[Amount]) );
 *
 * This implementation claims to be the true CCITT compliant CRC16 but it is not compatible to sdcards and some other applications.
 * Read this article for more details: http://srecord.sourceforge.net/crc16-ccitt.html
 * Example CRCs (StartValue=0xffff):
 * (1) 512 bytes of 0xff -> CRC16==?????? (not matching description in SDCARD technical specification)
 * (2) "123456789"       -> CRC16==0xe5cc
 *
 * @param Buffer     (t_u8*)  Buffer storing raw data to calculate checksum from + 16 bit checksum value in Buffer[Amount], Buffer[Amount+1]
 * @param Amount     (t_u32)  amount of valid bytes in Buffer[] (except crc value)
 * @param StartValue (t_u16)  initial CRC16 value (=0 to calculate new crc16; return value from previous call when continuing calculation)
 * @return           (BOOL)   ==TRUE: CRC16 of data in Buffer[] matches to DesiredCRC16; ==FALSE: CRC16 != DesiredCRC16
 */
BOOL ttc_crc_crc16_check_ccitt( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue );
BOOL _driver_crc_crc16_check_ccitt( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue );
#define ttc_crc_crc16_check_ccitt  _driver_crc_crc16_check_ccitt  // enable to directly forward function call to low-level driver (remove function definition before!)

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_crc_ are passed to interfaces/ttc_crc_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl crc UPDATE
 */

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_crc_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_crc_features.
 *
 * @param Config        Configuration of crc device
 * @return              Fields of *Config have been adjusted if necessary
 */
void _driver_crc_configuration_check( t_ttc_crc_config* Config );

/** initializes single CRC unit for operation
 * @param Config        Configuration of crc device
 * @return              == 0: CRC has been initialized successfully; != 0: error-code
 */
e_ttc_crc_errorcode _driver_crc_init( t_ttc_crc_config* Config );

/** loads configuration of indexed CRC unit with default values
 * @param Config        Configuration of crc device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_crc_errorcode _driver_crc_load_defaults( t_ttc_crc_config* Config );

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_CRC_H
