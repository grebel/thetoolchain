#ifndef <FILE>_h
#define <FILE>_h

/** { <FILE>.h *********************************************************
 *
 *                          The ToolChain
 *                     
 *  Created from template ttc-lib/templates/new_file.h revision 11 at <DATE>
 *
 *  Authors: Gregor Rebel
}*/
/** { Includes *********************************************************
 *
 * Put all includes here that are required to include this header file.
 * Do NOT add includes required to compile <FILE>.c here!
 */

//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Constant Defines / Macros ****************************************
 *
 * As a convention, constant definitions are all UPPERCASED to distinguish 
 * them from variables and functions.
 * 
 * Examples:
 
 #define ABC 1
 #define calculate(A,B) (A+B)
 */

//}ConstantDefines
/** { Structure Declarations *******************************************
 *
 * This can be typedef struct or typedef enum declarations.
 * Always prefix structures and enums with filename to avoid double declarations!
 *
 * Examples:
 
   typedef struct <FILE>_list_s {
      struct <FILE>_list_s* Next;
      t_base Value;
   } <FILE>_list_t;
   
   typedef enum {
     <FILE>_None,
     <FILE>_Type1
   } e_<FILE>_types;
 
 */

//}StructureDeclarations
/** { Function Declarations ********************************************
 *
 * Declare all functions here that may be called from outside.
 * Rules of Good Code:
 * - document every function declaration
 * - prefix public function names with their filename to avoid double definitions 
 */
// { Example declaration
 /** calculates the sum of A and B
  *
  * @param A  unsigned integer
  * @param B  unsigned integer
  * @return   A + B
  */
// t_u8 <FILE>_calculate(t_u16 A, t_u16 B);
//}


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //<FILE>_h
