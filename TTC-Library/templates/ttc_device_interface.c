/** { ttc_<device>_interface.c ************************************************
 *
 *                           The ToolChain
 *                           
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for <DEVICE> device.
 *
 *  The functions implemented here are used when additional runtime  
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_<device>_*() functions.
 *  Instead they can call _driver_<device>_*() pendants.
 *  E.g.: ttc_<device>_reset() -> _driver_<device>_reset()
 *
 *  Created from template <TEMPLATE_FILE> revision 25 at <DATE>
 *
 *  Authors: Gregor Rebel
 *   
}*/

//{ Includes *************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_<device>_interface.h".
//
#include "ttc_<device>_interface.h"
#include "../ttc_task.h"             // provides macro TTC_TASK_RETURN()
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

// interface will include low-driver for us. Check if at least one low-level driver could be activated 
#ifndef EXTENSION_<DEVICE>_DRIVER_AVAILABLE
#  error Missing low-level driver for TTC_<DRIVER>! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _<device>_
#endif

//}Includes
/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */
 
//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Private Function declarations ****************************************

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Private Function declarations
//{ Function definitions *************************************************

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_<device>_interface_foo(t_ttc_<device>_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
