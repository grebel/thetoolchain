/** { ttc_<device>.c ****************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for <device> devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported 
 *  architectures.
 *
 *  Most functions of this driver are defined as macros and being implemented
 *  in interfaces/ttc_<device>_interface.c or in low-level drivers <device>/<device>_*.c.
 *
 *  See corresponding ttc_<device>.h for documentation of all high-level functions.
 *
 *  Created from template <TEMPLATE_FILE> revision 54 at <DATE>
 *
 *  Authors: <AUTHOR>
 *  
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_<device>.h".
//
#include "ttc_<device>.h"   // automatically includes current low-level driver
#include "<device>/<device>_common.h" // common <device> functions usable by low- and high-level driver 
#include "ttc_heap.h"       // dynamic memory allocationand safe arrays  
#include "ttc_task.h"       // disable/ enable multitasking scheduler
#ifdef EXTENSION_ttc_interrupt
#include "ttc_interrupt.h"  // globally disable/ enable interrupts
#endif

//}Includes

#if TTC_<DEVICE>_AMOUNT == 0
  #warning No <DEVICE> devices defined, did you forget to activate something? - Define at least TTC_<DEVICE>1 as one from e_ttc_<device>_architecture in your makefile!
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of <device> devices.
 *
 */
 

// for each initialized device, a pointer to its generic and architecture specific definitions is stored
A_define(t_ttc_<device>_config*, ttc_<device>_configs, TTC_<DEVICE>_AMOUNT);

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ********************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_<device>(t_ttc_<device>_config* Config)

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * @param LogicalIndex    1.. logical index of <DEVICE> device. Each logical device <n> is defined via TTC_<DEVICE><n>* constants in compile_options.h and extensions.active/makefile
 */
void _ttc_<device>_configuration_check(t_u8 LogicalIndex);
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

t_ttc_<device>_config*   ttc_<device>_create() {

    t_u8 LogicalIndex = ttc_<device>_get_max_index();
    t_ttc_<device>_config* Config = ttc_<device>_get_configuration( LogicalIndex );

    TTC_TASK_RETURN( Config ); // will perform stack overflow check + return value
}
t_u8                     ttc_<device>_get_max_index() {
    TTC_TASK_RETURN( TTC_<DEVICE>_AMOUNT ); // will perform stack overflow check + return value
}
t_ttc_<device>_config*   ttc_<device>_get_configuration(t_u8 LogicalIndex) {
    Assert_<DEVICE>(LogicalIndex > 0, ttc_assert_origin_auto ); // logical index starts at 1
    Assert_<DEVICE>( LogicalIndex <= TTC_<DEVICE>_AMOUNT, ttc_assert_origin_auto ); // invalid index given (did you configure engnough ttc_<device> devices in your makefile?)
    t_ttc_<device>_config* Config = A(ttc_<device>_configs, LogicalIndex-1); // will assert if outside array bounds

    if (!Config) {
#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_disable();                      // ensure that we are not interrupted while creating configuration
#endif

        Config = A(ttc_<device>_configs, LogicalIndex-1); // have to reload with disabled interrupts to avoid racing conditions
        if (!Config) {
          Config = A(ttc_<device>_configs, LogicalIndex-1) = ttc_heap_alloc_zeroed(sizeof(t_ttc_<device>_config));
          Config->Flags.Initialized = 0;             // make sure that deinit() is not called during load_defaults()
          ttc_<device>_load_defaults(LogicalIndex);
        }
        
#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_enable();
#endif
    }

    Assert_<DEVICE>_EXTRA_Writable(Config, ttc_assert_origin_auto ); // memory corrupted?
    TTC_TASK_RETURN( Config ); // will perform stack overflow check + return value
}
void                     ttc_<device>_deinit(t_u8 LogicalIndex) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration

    t_ttc_<device>_config* Config = ttc_<device>_get_configuration(LogicalIndex);
  
    if (Config->Flags.Initialized) {
      e_ttc_<device>_errorcode Result = _driver_<device>_deinit(Config);
      if (Result == E_ttc_<device>_errorcode_OK)
        Config->Flags.Initialized = 0;
    }
    ttc_task_critical_end(); // other tasks now may access this driver
}
e_ttc_<device>_errorcode ttc_<device>_init(t_u8 LogicalIndex) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration
    #if (TTC_ASSERT_<DEVICE>_EXTRA == 1)
    t_u8 InitialCriticalLevel = ttc_interrupt_critical_level();
    #endif
    t_ttc_<device>_config* Config = ttc_<device>_get_configuration(LogicalIndex);

    // check configuration to meet all limits of current architecture    
    _ttc_<device>_configuration_check(LogicalIndex);

    Config->LastError = _driver_<device>_init(Config);
    if (! Config->LastError) // == 0
      Config->Flags.Initialized = 1;

    Assert_<DEVICE>(Config->Architecture != 0, ttc_assert_origin_auto ); // Low-Level driver must set this value!
    Assert_<DEVICE>_EXTRA( InitialCriticalLevel == ttc_interrupt_critical_level(), ttc_assert_origin_auto ); // amount of  ttc_task_critical_end() calls does not match. Check driver implementation!
    ttc_task_critical_end(); // other tasks now may access this driver

    TTC_TASK_RETURN( Config->LastError ); // will perform stack overflow check + return value
}
e_ttc_<device>_errorcode ttc_<device>_load_defaults(t_u8 LogicalIndex) {
    t_ttc_<device>_config* Config = ttc_<device>_get_configuration(LogicalIndex);

    u_ttc_<device>_architecture* LowLevelConfig = NULL;
    if (Config->Flags.Initialized) {
      ttc_<device>_deinit(LogicalIndex);
      LowLevelConfig = Config->LowLevelConfig; // rescue pointer to dynamic allocated memory
    }
    
    ttc_memory_set( Config, 0, sizeof(t_ttc_<device>_config) );
    
    // restore pointer to dynamic allocated memory
    Config->LowLevelConfig = LowLevelConfig;
    
    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    switch (LogicalIndex) { // physical index of <device> device from static configuration
#ifdef TTC_<DEVICE>1
    case  1: Config->PhysicalIndex = TTC_<DEVICE>1; break;
#endif
#ifdef TTC_<DEVICE>2
    case  2: Config->PhysicalIndex = TTC_<DEVICE>2; break;
#endif
#ifdef TTC_<DEVICE>3
    case  3: Config->PhysicalIndex = TTC_<DEVICE>3; break;
#endif
#ifdef TTC_<DEVICE>4
    case  4: Config->PhysicalIndex = TTC_<DEVICE>4; break;
#endif
#ifdef TTC_<DEVICE>5
    case  5: Config->PhysicalIndex = TTC_<DEVICE>5; break;
#endif
#ifdef TTC_<DEVICE>6
    case  6: Config->PhysicalIndex = TTC_<DEVICE>6; break;
#endif
#ifdef TTC_<DEVICE>7
    case  7: Config->PhysicalIndex = TTC_<DEVICE>7; break;
#endif
#ifdef TTC_<DEVICE>8
    case  8: Config->PhysicalIndex = TTC_<DEVICE>8; break;
#endif
#ifdef TTC_<DEVICE>9
    case  9: Config->PhysicalIndex = TTC_<DEVICE>9; break;
#endif
#ifdef TTC_<DEVICE>10
    case 10: Config->PhysicalIndex = TTC_<DEVICE>10; break;
#endif
    default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }

    //Insert additional generic default values here ...

    // Let low-level driver initialize remaining fields
    Config->LastError = _driver_<device>_load_defaults(Config);

    // Check mandatory configuration items
    Assert_<DEVICE>_EXTRA( (Config->Architecture > E_ttc_<device>_architecture_None) && (Config->Architecture < E_ttc_<device>_architecture_unknown), ttc_assert_origin_auto ); // architecture not set properly! Low-Level driver must set this field!

    TTC_TASK_RETURN( Config->LastError ); // will perform stack overflow check + return value
}
void                     ttc_<device>_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)
    
    A_reset(ttc_<device>_configs, TTC_<DEVICE>_AMOUNT); // safe arrays are placed in data section and must be initialized manually 

    if (0) // optional: register this device for a sysclock change update
      ttc_sysclock_register_for_update(ttc_<device>_sysclock_changed);
    
    _driver_<device>_prepare();
}
void                     ttc_<device>_reset(t_u8 LogicalIndex) {
    t_ttc_<device>_config* Config = ttc_<device>_get_configuration(LogicalIndex);

    _driver_<device>_reset(Config);    
}
void                     ttc_<device>_sysclock_changed() {

    // deinit + reinit all initialized <DEVICE> devices
    for (t_u8 LogicalIndex = 1; LogicalIndex < ttc_<device>_get_max_index(); LogicalIndex++) {
        t_ttc_<device>_config* Config = ttc_<device>_get_configuration(LogicalIndex);
        if (Config->Flags.Initialized) {
            ttc_<device>_deinit(LogicalIndex);
            ttc_<device>_init(LogicalIndex);
        }
    }
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_<device>(t_u8 LogicalIndex) {  }

void _ttc_<device>_configuration_check(t_u8 LogicalIndex) {
    t_ttc_<device>_config* Config = ttc_<device>_get_configuration( LogicalIndex );
    const t_ttc_<device>_features* Features = Config->Features;
    
    // perform plausibility checks on Features...
    Assert_<DEVICE>_EXTRA_Readable(Features, ttc_assert_origin_auto); // low-level driver did not load this pointer with a address of its feature set. Declare and define a constant instance of t_ttc_filesystem_features and assign a reference to it in current filesystem_XXX_load_defaults()!
        
    // perform some generic plausibility checks on Config...
    
    
    /** Example plausibility checks
    
    if (Config->BitRate > Features->MaxBitRate)   Config->BitRate = Features->MaxBitRate;
    if (Config->BitRate < Features->MinBitRate)   Config->BitRate = Features->MinBitRate;
    
    */
    // add more architecture independent checks...
    Assert_<DEVICE>(LogicalIndex == Config->LogicalIndex, ttc_assert_origin_auto); // configuration must store corresponding logical index!
    
    // let low-level driver check this configuration too
    _driver_<device>_configuration_check(Config);
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
