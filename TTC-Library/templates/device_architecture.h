#ifndef <DEVICE>_<ARCHITECTURE>_H
#define <DEVICE>_<ARCHITECTURE>_H

/** { <device>_<architecture>.h **********************************************
 *
 *                               The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_<device>.h providing
 *  function prototypes being required by ttc_<device>.h
 *
 *  <architecture_description>
 *
 *  Created from template <TEMPLATE_FILE> revision 32 at <DATE>
 *
 *  Note: See ttc_<device>.h for description of <architecture> independent <DEVICE> implementation.
 *  
 *  Authors: <AUTHOR>
 *
 */
/** Description of <device>_<architecture> (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_<DEVICE>_DRIVER_AVAILABLE
#define EXTENSION_<DEVICE>_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_<device>_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "<device>_<architecture>.c"
//
#include "../ttc_<device>_types.h" // will include <device>_<architecture>_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_<device>_interface.h
//
// Some driver functions may be optional to implement. 
// Check ttc_<device>_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_<device>_foo
//
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Macro definitions
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_<device>.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_<device>.h for details.
 *
 * You normally should not need to add functions here. 
 * Add your private functions in the private section below. 
 */

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //<DEVICE>_<ARCHITECTURE>_H
