/** { ttc_<device>_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for <DEVICE> device.
 *  Structures, Enums and Defines being required by both, high- and low-level <device>.
 *  This file does not provide function declarations.
 *
 *  Note: See ttc_<device>.h for description of high-level <device> implementation!
 *
 *  Created from template ttc_device_types.h revision 46 at <DATE>
 *
 *  Authors: <AUTHOR>
 * 
}*/

#ifndef TTC_<DEVICE>_TYPES_H
#define TTC_<DEVICE>_TYPES_H

//{ Includes1 ************************************************************
// 
// Includes being required for static configuration.
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_<device>.h" or "ttc_<device>.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h" 

//} Includes
/** { Static Configuration ***********************************************
 *
 * Most ttc drivers are statically configured by constant definitions in makefiles.
 * A static configuration allows to hide certain code fragments and variables for the
 * compilation process. If used wisely, your code will be lean and fast and do only
 * what is required for the current configuration.
 */
 
/** {  TTC_<DEVICE>n has to be defined as constant in one of your makefiles
 *
 * Here we count how much devices have been defined and if all lower index devices have been defined too.
 */
#ifndef TTC_<DEVICE>_AMOUNT //{ 10
#  ifdef TTC_<DEVICE>10
#    ifndef TTC_<DEVICE>9
#      error TTC_<DEVICE>10 is defined, but not TTC_<DEVICE>9 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>8
#      error TTC_<DEVICE>10 is defined, but not TTC_<DEVICE>8 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>7
#      error TTC_<DEVICE>10 is defined, but not TTC_<DEVICE>7 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>6
#      error TTC_<DEVICE>10 is defined, but not TTC_<DEVICE>6 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>5
#      error TTC_<DEVICE>10 is defined, but not TTC_<DEVICE>5 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>4
#      error TTC_<DEVICE>10 is defined, but not TTC_<DEVICE>4 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>3
#      error TTC_<DEVICE>10 is defined, but not TTC_<DEVICE>3 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>2
#      error TTC_<DEVICE>10 is defined, but not TTC_<DEVICE>2 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>1
#      error TTC_<DEVICE>10 is defined, but not TTC_<DEVICE>1 - all lower TTC_<DEVICE>n must be defined!
#    endif
  
#    define TTC_<DEVICE>_AMOUNT 10
#  endif
#endif //}
#ifndef TTC_<DEVICE>_AMOUNT //{ 9
#  ifdef TTC_<DEVICE>9
#    ifndef TTC_<DEVICE>8
#      error TTC_<DEVICE>9 is defined, but not TTC_<DEVICE>8 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>7
#      error TTC_<DEVICE>9 is defined, but not TTC_<DEVICE>7 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>6
#      error TTC_<DEVICE>9 is defined, but not TTC_<DEVICE>6 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>5
#      error TTC_<DEVICE>9 is defined, but not TTC_<DEVICE>5 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>4
#      error TTC_<DEVICE>9 is defined, but not TTC_<DEVICE>4 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>3
#      error TTC_<DEVICE>9 is defined, but not TTC_<DEVICE>3 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>2
#      error TTC_<DEVICE>9 is defined, but not TTC_<DEVICE>2 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>1
#      error TTC_<DEVICE>9 is defined, but not TTC_<DEVICE>1 - all lower TTC_<DEVICE>n must be defined!
#    endif
  
#    define TTC_<DEVICE>_AMOUNT 9
#  endif
#endif //}
#ifndef TTC_<DEVICE>_AMOUNT //{ 8
#  ifdef TTC_<DEVICE>8
#    ifndef TTC_<DEVICE>7
#      error TTC_<DEVICE>8 is defined, but not TTC_<DEVICE>7 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>6
#      error TTC_<DEVICE>8 is defined, but not TTC_<DEVICE>6 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>5
#      error TTC_<DEVICE>8 is defined, but not TTC_<DEVICE>5 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>4
#      error TTC_<DEVICE>8 is defined, but not TTC_<DEVICE>4 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>3
#      error TTC_<DEVICE>8 is defined, but not TTC_<DEVICE>3 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>2
#      error TTC_<DEVICE>8 is defined, but not TTC_<DEVICE>2 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>1
#      error TTC_<DEVICE>8 is defined, but not TTC_<DEVICE>1 - all lower TTC_<DEVICE>n must be defined!
#    endif
  
#    define TTC_<DEVICE>_AMOUNT 8
#  endif
#endif //}
#ifndef TTC_<DEVICE>_AMOUNT //{ 7
#  ifdef TTC_<DEVICE>7
#    ifndef TTC_<DEVICE>6
#      error TTC_<DEVICE>7 is defined, but not TTC_<DEVICE>6 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>5
#      error TTC_<DEVICE>7 is defined, but not TTC_<DEVICE>5 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>4
#      error TTC_<DEVICE>7 is defined, but not TTC_<DEVICE>4 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>3
#      error TTC_<DEVICE>7 is defined, but not TTC_<DEVICE>3 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>2
#      error TTC_<DEVICE>7 is defined, but not TTC_<DEVICE>2 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>1
#      error TTC_<DEVICE>7 is defined, but not TTC_<DEVICE>1 - all lower TTC_<DEVICE>n must be defined!
#    endif
  
#    define TTC_<DEVICE>_AMOUNT 7
#  endif
#endif //}
#ifndef TTC_<DEVICE>_AMOUNT //{ 6
#  ifdef TTC_<DEVICE>6
#    ifndef TTC_<DEVICE>5
#      error TTC_<DEVICE>6 is defined, but not TTC_<DEVICE>5 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>4
#      error TTC_<DEVICE>6 is defined, but not TTC_<DEVICE>4 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>3
#      error TTC_<DEVICE>6 is defined, but not TTC_<DEVICE>3 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>2
#      error TTC_<DEVICE>6 is defined, but not TTC_<DEVICE>2 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>1
#      error TTC_<DEVICE>6 is defined, but not TTC_<DEVICE>1 - all lower TTC_<DEVICE>n must be defined!
#    endif
  
#    define TTC_<DEVICE>_AMOUNT 6
#  endif
#endif //}
#ifndef TTC_<DEVICE>_AMOUNT //{ 5
#  ifdef TTC_<DEVICE>5
#    ifndef TTC_<DEVICE>4
#      error TTC_<DEVICE>5 is defined, but not TTC_<DEVICE>4 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>3
#      error TTC_<DEVICE>5 is defined, but not TTC_<DEVICE>3 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>2
#      error TTC_<DEVICE>5 is defined, but not TTC_<DEVICE>2 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>1
#      error TTC_<DEVICE>5 is defined, but not TTC_<DEVICE>1 - all lower TTC_<DEVICE>n must be defined!
#    endif
  
#    define TTC_<DEVICE>_AMOUNT 5
#  endif
#endif //}
#ifndef TTC_<DEVICE>_AMOUNT //{ 4
#  ifdef TTC_<DEVICE>4
#    ifndef TTC_<DEVICE>3
#      error TTC_<DEVICE>4 is defined, but not TTC_<DEVICE>3 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>2
#      error TTC_<DEVICE>4 is defined, but not TTC_<DEVICE>2 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>1
#      error TTC_<DEVICE>4 is defined, but not TTC_<DEVICE>1 - all lower TTC_<DEVICE>n must be defined!
#    endif

#    define TTC_<DEVICE>_AMOUNT 4
#  endif
#endif //}
#ifndef TTC_<DEVICE>_AMOUNT //{ 3
#  ifdef TTC_<DEVICE>3

#    ifndef TTC_<DEVICE>2
#      error TTC_<DEVICE>3 is defined, but not TTC_<DEVICE>2 - all lower TTC_<DEVICE>n must be defined!
#    endif
#    ifndef TTC_<DEVICE>1
#      error TTC_<DEVICE>3 is defined, but not TTC_<DEVICE>1 - all lower TTC_<DEVICE>n must be defined!
#    endif

#    define TTC_<DEVICE>_AMOUNT 3
#  endif
#endif //}
#ifndef TTC_<DEVICE>_AMOUNT //{ 2
#  ifdef TTC_<DEVICE>2
  
#    ifndef TTC_<DEVICE>1
#      error TTC_<DEVICE>2 is defined, but not TTC_<DEVICE>1 - all lower TTC_<DEVICE>n must be defined!
#    endif
  
#    define TTC_<DEVICE>_AMOUNT 2
#  endif
#endif //}
#ifndef TTC_<DEVICE>_AMOUNT //{ 1
#  ifdef TTC_<DEVICE>1
#    define TTC_<DEVICE>_AMOUNT 1
#  endif
#endif //}
#ifndef TTC_<DEVICE>_AMOUNT //{ no devices defined at all
#  warning Missing definition for TTC_<DEVICE>1. Using default type. Add valid definition to your makefile to get rid if this message!
// option 1: no devices available
#  define TTC_<DEVICE>_AMOUNT 0
// option 2: define a default device (enable lines below)
// #define TTC_<DEVICE>_AMOUNT 1
// #define TTC_<DEVICE>1 ??? // <- place entry from e_ttc_<device>_architecture here!!!
#endif

//}
//}
/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop. 
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_<DEVICE> 0#         disable default asserts for <device> driver
 * 
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_<DEVICE>_EXTRA 1#   enable extra asserts for <device> driver 
 *
 * When debugging code that has been compiled with optimization (gcc option -O) then
 * certain variables may be optimized away. Declaring variables as VOLATILE_<DEVICE> makes them
 * visible in debug session when asserts are enabled. You have no overhead at all if asserts are disabled.
 + foot_t* VOLATILE_<DEVICE> VisiblePointer;
 *
 */
#ifndef TTC_ASSERT_<DEVICE>    // any previous definition set (Makefile)?
#define TTC_ASSERT_<DEVICE> 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_<DEVICE> == 1)  // use Assert()s in <DEVICE> code (somewhat slower but alot easier to debug)
#  define Assert_<DEVICE>(Condition, Origin)        Assert(Condition, Origin)
#  define Assert_<DEVICE>_Writable(Address, Origin) Assert_Writable(Address, Origin)
#  define Assert_<DEVICE>_Readable(Address, Origin) Assert_Readable(Address, Origin)
#  define VOLATILE_<DEVICE>                         volatile 
#else  // use no default Assert()s in <DEVICE> code (somewhat smaller + faster, but crashes are hard to debug)
#  define Assert_<DEVICE>(Condition, Origin)
#  define Assert_<DEVICE>_Writable(Address, Origin)
#  define Assert_<DEVICE>_Readable(Address, Origin)
#  define VOLATILE_<DEVICE>
#endif

#ifndef TTC_ASSERT_<DEVICE>_EXTRA    // any previous definition set (Makefile)?
#define TTC_ASSERT_<DEVICE>_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_<DEVICE>_EXTRA == 1)  // use Assert()s in <DEVICE> code (somewhat slower but alot easier to debug)
#  define Assert_<DEVICE>_EXTRA(Condition, Origin) Assert(Condition, Origin)
#  define Assert_<DEVICE>_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
#  define Assert_<DEVICE>_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in <DEVICE> code (somewhat smaller + faster, but crashes are hard to debug)
#  define Assert_<DEVICE>_EXTRA(Condition, Origin)
#  define Assert_<DEVICE>_EXTRA_Writable(Address, Origin)
#  define Assert_<DEVICE>_EXTRA_Readable(Address, Origin)
#endif
//}
/** { Check static configuration of <device> devices
 *
 * Each TTC_<DEVICE>n must be defined as one from e_ttc_<device>_architecture.
 * Additional defines of type TTC_<DEVICE>n_* may be added.
 *
 * The constants being checked here must be defined in your makefile.
 * Most constants are defined by EXTENSION_100_board_* or EXTENSION_150_board_extension_* files.
 */

#ifdef TTC_<DEVICE>1

// check extra defines for <device> device #1 here

#endif
#ifdef TTC_<DEVICE>2

// check extra defines for <device> device #2 here

#endif
#ifdef TTC_<DEVICE>3

// check extra defines for <device> device #3 here

#endif
#ifdef TTC_<DEVICE>4

// check extra defines for <device> device #4 here

#endif
#ifdef TTC_<DEVICE>5

// check extra defines for <device> device #5 here

#endif
#ifdef TTC_<DEVICE>6

// check extra defines for <device> device #6 here

#endif
#ifdef TTC_<DEVICE>7

// check extra defines for <device> device #7 here

#endif
#ifdef TTC_<DEVICE>8

// check extra defines for <device> device #8 here

#endif
#ifdef TTC_<DEVICE>9

// check extra defines for <device> device #9 here

#endif
#ifdef TTC_<DEVICE>10

// check extra defines for <device> device #10 here

#endif
//}

//}Static Configuration

//{ Includes2 ************************************************************
// 
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_<device>.h" or "ttc_<device>.c"
//

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

/** { Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_<device>_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_<device>_architecture
#  warning Missing low-level definition for t_ttc_<device>_architecture (using default)
#  define t_ttc_<device>_architecture void
#endif
//}

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_<device>_errorcode       return codes of <DEVICE> devices
  E_ttc_<device>_errorcode_OK = 0, 
  
  // other warnings go here..

  E_ttc_<device>_errorcode_ERROR,                  // general failure
  E_ttc_<device>_errorcode_NULL,                   // NULL pointer not accepted
  E_ttc_<device>_errorcode_DeviceNotFound,         // corresponding device could not be found
  E_ttc_<device>_errorcode_InvalidConfiguration,   // sanity check of device configuration failed
  E_ttc_<device>_errorcode_InvalidImplementation,  // your code does not behave as expected

  // other failures go here..
  
  E_ttc_<device>_errorcode_unknown                // no valid errorcodes past this entry 
} e_ttc_<device>_errorcode;
typedef enum {   // e_ttc_<device>_architecture    types of architectures supported by <DEVICE> driver
  E_ttc_<device>_architecture_None = 0,       // no architecture selected
  

//InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)
  
  E_ttc_<device>_architecture_unknown        // architecture not supported
} e_ttc_<device>_architecture;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef union {  // architecture dependent configuration

//InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_<device>_architecture;
typedef struct s_ttc_<device>_features { // static minimum, maximum and default values of features of single <device>
    
  // Add any amount of architecture independent values describing <device> devices here.
  // You may also want to add a plausibility check for each value to _ttc_<device>_configuration_check().
  
  /** Example Features
  
  t_u16 MinBitRate;   // minimum allowed transfer speed
  t_u16 MaxBitRate;   // maximum allowed transfer speed
  */
  
  const t_u8 unused; // remove me!

} __attribute__( ( __packed__ ) ) t_ttc_<device>_features;
typedef struct s_ttc_<device>_config { //          architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_<device>_init()
    //       and after ttc_<device>_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // Hint: Order structure elements for optimal alignment.
    //       You should place smaller datatypes like t_u8 and t_u16 after all t_u32 and pointer types
    //       to prevent insertion of pad-bytes. Most architectures don't support placing 32-bit datatypes at 
    //       unaligned address.  
    
    struct { // Init: fields to be set by application before first calling ttc_<device>_init() --------------
        // Do not change these values after calling ttc_<device>_init()!

        //...
        
        struct { // generic initial configuration bits common for all low-level Drivers
          unsigned unused : 1;  // ==1: 
          // ToDo: additional high-level flags go here..
        } Flags;
    } Init;

    // Fields below are read-only for application! ----------------------------------------------------------

    u_ttc_<device>_architecture* LowLevelConfig; // low-level configuration (structure defined by low-level driver)
    
    const t_ttc_<device>_features* Features;     // constant features of this <device>
    
    struct { // status flags common for all architectures
      unsigned Initialized        : 1;  // ==1: device has been initialized successfully
      // ToDo: additional high-level flags go here..
    } Flags;
    
    // Last returned error code
    // IMPORTANT: Every ttc_<device>_*() function that returns e_ttc_<device>_errorcode has to update this value if it returns an error! 
    e_ttc_<device>_errorcode LastError; 
    
    t_u8  LogicalIndex;                          // automatically set: logical index of device to use (1 = TTC_<DEVICE>1, ...)
    t_u8  PhysicalIndex;                         // automatically set: index of physical device to use (0 = <device> #1, ...)
    e_ttc_<device>_architecture  Architecture;   // type of architecture used for current <device> device

    // Additional high-level attributes go here..
    
} __attribute__((__packed__)) t_ttc_<device>_config;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_<DEVICE>_TYPES_H
