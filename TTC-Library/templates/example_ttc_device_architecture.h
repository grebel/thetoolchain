#ifndef EXAMPLE_<DEVICE>_<ARCHITECTURE>_H
#define EXAMPLE_<DEVICE>_<ARCHITECTURE>_H

/** example_ttc_<DEVICE>_<ARCHITECTURE>.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_<device>_<architecture>.
 *  The basic flow is as follows:
 *  (1) example_ttc_<device>_<architecture>_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_<device>_<architecture>_prepare() gets executed
 *
 *  Created from template <TEMPLATE_FILE> revision 16 at <DATE>
 *
 *  Authors: <AUTHOR>
 *  
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_<DEVICE>.c only do not belong here! 
// This is why header files typically include *_types.h files only (avoids nasty include loops).

#include "../ttc-lib/ttc_basic_types.h"    // basic datatypes for current microcontroller architecture
#include "../ttc-lib/ttc_gpio_types.h"     // allows to store gpio pins in structures 
#include "../ttc-lib/ttc_task_types.h"     // allows to store task delay data in structures
#include "../ttc-lib/ttc_<device>_types.h" // datatypes of <device> devices
#include "../ttc-lib/<device>/<device>_<architecture>_types.h" // datatypes of <architecture> <device> devices

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_<device>_<architecture>_t
    t_ttc_<device>_config* Config_<DEVICE>; // pointer to configuration of <device> device to use
    t_ttc_systick_delay   Delay;            // this type of delay is usable in single- and multitasking setup

    // more arguments...
} example_<device>_<architecture>_data_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_<device>_<architecture>_prepare();


//}Function prototypes

#endif //EXAMPLE_<DEVICE>_<ARCHITECTURE>_H
