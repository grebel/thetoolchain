#ifndef <DEVICE>_<ARCHITECTURE>_TYPES_H
#define <DEVICE>_<ARCHITECTURE>_TYPES_H

/** { <device>_<architecture>.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *                     
 *  Low-level driver for ttc_<device>.h providing
 *  Structures, Enums and Defines being required by ttc_<device>_types.h
 *
 *  <architecture_description>
 *
 *  Created from template <TEMPLATE_FILE> revision 27 at <DATE>
 *
 *  Note: See ttc_<device>.h for description of high-level <device> implementation!
 *  Note: See <device>_<architecture>.h for description of <architecture> specific <device> implementation!
 * 
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#ifndef TTC_<DEVICE>_TYPES_H
#  error Do not include <device>_common_types.h directly. Include ttc_<device>_types.h instead!
#endif

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_<device>_types.h *************************

typedef struct {  // <architecture> specific configuration data of single <device> device
  t_u8 unused;         // remove me (C structs may not be empty)!
  // add architecture specific configuration fields here..
} __attribute__((__packed__)) t_<device>_<architecture>_config;

// t_ttc_<device>_architecture is required by ttc_<device>_types.h
#define t_ttc_<device>_architecture t_<device>_<architecture>_config

//} Structures/ Enums

#endif //<DEVICE>_<ARCHITECTURE>_TYPES_H
