#!/usr/bin/perl

my $Verbose = 1; # 1 = display status messages; 2 = debug messages

my $DeviceName       = lc( shift(@ARGV) );
my $ArchitectureName = shift(@ARGV);
my $Option;
my $ArchitectureDescription;
my $Dependency;
my $Update = 0;
$0 =~ m/.*\/(.+)/;
my $MySelf = $1;
if ($ArchitectureName eq 'UPDATE') { $Update = 1; $ArchitectureName = undef; }
if ($ArchitectureName) {
  $ArchitectureDescription = lc( shift(@ARGV) );
  $Dependency  = lc( shift(@ARGV) ) || '${DriverName}';
  $Option      = shift(@ARGV);
  
  unless ($ArchitectureDescription) { # missing mandatory argument: force help text 
    $ArchitectureName = $Update = undef;
    print "\n$0 - ERROR: Missing mandatory argument <DESCRIPTION>!\n";
  } 
}

my $SkipExistingFiles = ($Option eq 'OVERWRITE') ? 0 : 1;
my $PendingCommitsFile = ".pending_commits";

unless (-d "../../.git/") {  # not inside git repository!
  print <<"END_OF_MESSAGE";


$0 - ERROR: Outside of git repository!

This script is intended to be used for developing new high- and low-level drivers.
It will create new source files that are general for all projects. 
If this is your intention, read Documentation/chapter_ToolChainDevelopment.tex
for details.

END_OF_MESSAGE
  exit 10;
}
unless ($ArchitectureName || $Update) { # help + exit
  print <<"END_OF_TEXT";

$0 <DEVICE> ( UPDATE | (<ARCHITECTURE> <DESCRIPTION> [<DEPENDENCY>] [OVERWRITE]) )

Will create a set of high-level, low-level driver code + interface logic and install scripts.
Will also create additional low-level driver + install script for existing high-level drivers.


Arguments
  [  ]            Means an optional argument.
  (  )            Means a group of arguments
  |               either left or right part
  UPDATE          Propagates all functions found in ttc_<DEVICE>.h into interface- and driver-files.
  <DEVICE>        Name of <device> set to create.
  <ARCHITECTURE>  Name of <architecture> for low-level <device> part.
                  "none" is a special architecture for high-level drivers that do not use special low-level drivers.
  <DESCRIPTION>   single line description of new low-level driver (will be displayed in activate_project.sh as short introduction)
  <DEPENDENCY>    Extension name or wildcard that must be activated before to enable low-level driver.
                  Dependency is fullfilled if a file extensions.active/makefile.<DEPENDENCY> is found.
                  You may use wildcards (*) to match aclass of files.
                  Default: 450_<DEVICE>_<ARCHITECTURE>
                  The dependency can be changed inside git/TheToolChain/InstallData/low_level/install_TTC_<DEVICE>_<ARCHITECTURE>.sh.
                  Tip: Search for checkFeature.
  OVERWRITE       If given, output files will be overwritten silently.


Files being created:

High-Level Driver
  ../ttc_<DEVICE>.c                                                             implementation being common for all architectures 
  ../ttc_<DEVICE>.h                                                             prototypes and documentation of high-level driver
  ../ttc_<DEVICE>_types.c                                                       datatypes used to communicate between high- and low-level driver

Interface Logic
  ../interfaces/ttc_<DEVICE>_interface.c                                        default implementations for missing low-level functions
  ../interfaces/ttc_<DEVICE>_interface.h                                        includes activated low-level driver

Low-level Driver
  ../<ARCHITECTURE>/<ARCHITECTURE>_<DEVICE>.c                                   implementation for certain architecture
  ../<ARCHITECTURE>/<ARCHITECTURE>_<DEVICE>.h                                   prototypes + macros for certain architecture
  ../<ARCHITECTURE>/<ARCHITECTURE>_<DEVICE>_types.h                             architecture dependent datatypes (included by ttc_<DEVICE>_types.c)

Installation Scripts (<NN> = enumeration)
  ../../InstallData/install_<NN>_<DEVICE>.sh                                    high-level driver, examples and regression
  ../../InstallData/low_level/install_TTC_<DEVICE>_<ARCHITECTURE>.sh   low-level driver


Note: Existing files will not be changed unless OVERWRITE or UPDATE has been given.

Examples
  # create low-level driver gpio_stm32f1xx that will activate if extensions.active/makefile.200_cpu_stm32f1* is found
  ./create_DeviceDriver.pl gpio stm32f1xx 200_cpu_stm32f1*

  # recreate low-level driver gpio_stm32f1xx (overwrite existing files)
  ./create_DeviceDriver.pl gpio stm32f1xx 200_cpu_stm32f1* OVERWRITE

  # create missing feature functions in interface- and low-level-sources of gpio driver
  ./create_DeviceDriver.pl gpio UPDATE
  
  # create a high-level only driver (will use other high-level drivers only)
  ./create_DeviceDriver.pl switch none "*"


$0 has been written by Gregor Rebel 2013-2016.

END_OF_TEXT
  exit 5;
}

$SIG{INT}  = sub { print stderr "INT\n";  dieOnError("Program interrupted."); };
$SIG{TERM} = sub { print stderr "TERM\n"; dieOnError("Program terminated.");  };

my $PWD        = `pwd`; chomp($PWD);
my $DateString = `date --utc +"%Y%m%d %T UTC"`; chomp($DateString); # datestring used for timestamps

my @AllTemplateFiles = ( #{ list of required template files
                         'device_common.c',                        # common code for all low-level low-level <device> drivers
                         'device_common.h',                        # common code for all low-level <device> drivers  
                         'device_architecture.c',                  # low-level <device> driver implementation for <architecture> architecture
                         'device_architecture.h',                  # low-level <device> driver function declarations for <architecture> architecture
                         'device_architecture_types.h',            # low-level <device> driver datatypes for <architecture> architecture
                         'ttc_device.c',                           # high-level <device>
                         'ttc_device.h',                           # high-level <device>
                         'ttc_device_interface.c',                 # interface
                         'ttc_device_interface.h',                 # interface
                         'ttc_device_types.h',                     # high-level <device> (architecture independent datatypes)
                         '_install_TTC_DEVICE.sh',                 # high-level install script
                         '_install_TTC_DEVICE_ARCHITECTURE.sh',    # low-level install script
                         'example_ttc_device.c',                   # example code implementation for high-level driver
                         'example_ttc_device.h',                   # example code header for high-level driver
                         'example_ttc_device_architecture.c',      # example code implementation for low-level driver
                         'example_ttc_device_architecture.h',      # example code header for low-level driver 
                         'regression_ttc_device.c',                # compilation and implementation test implementation for high-level driver
                         'regression_ttc_device.h',                # compilation and implementation test header for high-level driver
                         'regression_ttc_device_architecture.c',   # compilation and implementation test implementation for high-level driver
                         'regression_ttc_device_architecture.h'    # compilation and implementation test header for high-level driver
                       ); #}
my %SourceInfo_HighLevel; # return from parseSource(ttc_<device>.c) (used for UPDATE process)
my %AffectedFiles; #{
#
# +----------------+
# | %AffectedFiles |
# +----------------+   +-----------------------+
# | FileName1 =======> |     %AffectedFile     |
# | ...            |   +-----------------------+
# +----------------+   | FileName => $FileName | <-- file name (without directory path)
#                      | FilePath => $FilePath | <-- relative file path of file
#                      | Change   => $Change   | <-- one from (CREATED, CHANGED, READ, SEEN)
#                      +-----------------------+ 
#
#}

my $AmountNewFiles = 0;
unless ($Update) {
  my @InvalidStrings = qw(device architecture);
  foreach my $InvalidString (@InvalidStrings) {
    if (index($DeviceName, $InvalidString) > -1) {
      die("Invalid keyword '$InvalidString' found in device name '$DeviceName'. Change name of your device an retry!");
    }
    if (index($ArchitectureName, $InvalidString) > -1) {
      die("Invalid keyword '$InvalidString' found in architecture name '$ArchitectureName'. Change name of your device an retry!");
    }
  }
  
  if ($ArchitectureName eq 'none') { print "$PWD> $0:  creating driver for $DeviceName as high-level only..\n"; }
  else                             { print "$PWD> $0:  creating driver for $DeviceName on $ArchitectureName architecture..\n"; }
  $AmountNewFiles = createNewFiles($DeviceName, lc($ArchitectureName) );
  $Update = 1; # update everything after adding new driver
}
if ($Update) {

  my $devicename = lc($DeviceName);
  my $TTC_SourceFile = "../ttc_${devicename}.c";
  my $TTC_HeaderFile = "../ttc_${devicename}.h";
  unless (-e $TTC_SourceFile) { die("collect_InterfaceCalls($DeviceName) - ERROR: Missing file '$PWD/$TTC_SourceFile'!"); }
  %SourceInfo_HighLevel = parseSource($TTC_SourceFile);

  my %Architectures = collect_Architectures($DeviceName);
  if ($Verbose > 1) { print "$PWD> $0:  found ".(scalar keys %Architectures)." architectures for device $DeviceName\n"; }

  my %DriverDeclarations = collect_DriverDeclarations($DeviceName, $TTC_HeaderFile);
  if ($ArchitectureName ne 'none') {
    update_InterfaceFiles( $DeviceName, \%DriverDeclarations, \%Architectures);
    update_LowLevelDrivers($DeviceName, \%DriverDeclarations, \%Architectures);
  }
  update_HighLevelDriver($DeviceName, \%DriverDeclarations, \%Architectures);
  
  my @ArchitecturNames = sort keys %Architectures;
  print "\nDevice $DeviceName supports ".(scalar @ArchitecturNames)." architectures: ".join(', ', @ArchitecturNames)."\n";
  print "Affected files:\n";
  my $MaxLength = 0; map { if ($MaxLength < length($_->{Change})) { $MaxLength =  length($_->{Change}); } } values %AffectedFiles;
  
  my @AffectedFiles = sort {  $AffectedFiles{$a}->{FilePath} cmp $AffectedFiles{$b}->{FilePath}; } keys %AffectedFiles;

  print join("\n", map { "    ".lc( substr("         ".$AffectedFiles{$_}->{Change}, - $MaxLength) )." ".$AffectedFiles{$_}->{FilePath}; } 
                   @AffectedFiles
            )."\n\n";
  print "All files of device $devicename:\njedit ".join(" ", map { $AffectedFiles{$_}->{FilePath}; } @AffectedFiles),"\n\n";
}
if ($AmountNewFiles) {
  print <<"END_OF_HINT"; #{

  Note: $AmountNewFiles new files have been added. These files are still missing in ~/Source/TheToolChain_devel/.
        The safest way to add them is to create + install a new development toolchain:
        cd $PWD/../../
        InstallData/scripts/createDevelopmentVersion.pl

END_OF_HINT
#}
}
exit 0;

sub gitComment {                  # adds given lines to file $PendingCommitsFile
  my @Comments = @_;
  
  my $PendingCommits = readFile("$PendingCommitsFile");
  my @PendingCommits = split("\n", $PendingCommits);
  my %PendingCommits = map { $_ => 1; } @PendingCommits;
  @Comments = grep { $_ } @Comments;
  
  # ToDo: implement
}
sub affectFile {                  # remembers given file as being affected
  my $TargetFile = shift; # relative file-path to an affected file
  my $Change     = shift; # one from %IsValidChange
  
  unless ($TargetFile)                   { return; } # no target file 
  if (substr($TargetFile, 0, 2) ne '..') { return; } # target file inside current folder
  
  my %ChangePriority = (
    CREATED => 4,
    CHANGED => 3,
    READ    => 2,
    SEEN    => 1
  );
  
  unless ($ChangePriority{$Change}) {
    die("$0 - ERROR: affectFile($TargetFile,$Change) - got invalid argument Change");
  }
    
  my $Pos = rindex($TargetFile, '/');
  my $TargetFileName = substr($TargetFile, $Pos + 1);
  
  unless ($AffectedFiles{$TargetFileName}) {
    $AffectedFiles{$TargetFileName} = {
      FileName => $TargetFileName,
      FilePath => $TargetFile,
        Change => 0
    };
  }
  if ( $ChangePriority{ $AffectedFiles{$TargetFileName}->{Change} } < $ChangePriority{$Change} ) {
    $AffectedFiles{$TargetFileName}->{Change} = $Change;
  }
}
sub createNewFiles {              # creates new files from templates if target files are missing
  my $DeviceName       = shift;
  my $ArchitectureName = shift || die("createNewFiles() missing argument!");
  
  my $DEVICE_NAME       = uc($DeviceName);
  my $devicename       = lc($DeviceName);
  my $ARCHITECTURENAME = uc($ArchitectureName);
  my $architecturename = lc($ArchitectureName);

  my $AmountNewFiles = 0;
  
  foreach my $TemplateFile (@AllTemplateFiles) {
    ( my $TargetFile,
      my $IsHighLevelDriver,
      my $IsLowLevelDriver,
      my $IsInterface,
      my $IsInstallScript,
      my $IsInstallScriptLow,
      my $IsInstallScriptHigh
    ) = identifyTemplate($TemplateFile, $DeviceName, $ArchitectureName);
    
    if ($TargetFile) { # create target file from source file by replacing strings: <device> <DEVICE> <architecture> <ARCHITECTURE>
      
      if (index($TargetFile, '_none') > -1) { print "FLUSH!\n"; dieOnError("INTERNAL ERRROR: About to create a none architecture! (TargetFile='$TargetFile'"); }
      my $ContentOrig; my $Content;
      my $FileIsNew = 0;
      unless ( $SkipExistingFiles && (-e $TargetFile) ) { # create new file
        print "creating '$TargetFile' from template '$TemplateFile'...\n";
        $FileIsNew = 1;
        $Content = $ContentOrig = readFile($TemplateFile);
        
        $Content =~ s/<device>/$DeviceName/g;
        $Content =~ s/<DEVICE>/$DEVICE_NAME/g;
        $Content =~ s/<architecture>/$architecturename/g;
        $Content =~ s/<ARCHITECTURE>/$ARCHITECTURENAME/g;
        $Content =~ s/<architecture_description>/$ArchitectureDescription/g;
        $Content =~ s/<dependency>/$Dependency/g;
        $Content =~ s/<TEMPLATE_FILE>/$TemplateFile/g;
        $Content =~ s/<DATE>/$DateString/g;
        
        $AmountNewFiles++;
      }
      else {                                              # update existing file
        print "updating existing file '$TargetFile'\n";
        $Content = $ContentOrig = readFile($TargetFile);
      }
      if ($IsInstallScriptHigh) { # update list of default low-level drivers
        if ( $architecturename &&($architecturename ne 'none') ) { 
          my $DefaultLowLevelDriver = "DefaultLowLevelDriver=\"450_${DeviceName}_${architecturename}\"";
          if (index($Content, $DefaultLowLevelDriver) == -1) { # entry missing in script: insert line
            $Content = insertIntoString( $TargetFile, 
                                         $Content,
                                         'InsertDefaultLowLevelDriversAbove',
                                         "#$DefaultLowLevelDriver # enable this line to set this low-level driver as default\n",
                                         PREVIOUS_LINE => 1
                                       );
          }
        }
      }
      if ($ArchitectureDescription) { # add enableFeature line for new low-level driver
        my $Prefix = ($FileIsNew) ? '  ' : '  # ';
        $Content = insertIntoString( $TargetFile,
                                     $Content,
                                     '<INSERT_LOW_LEVEL_FEATURES_ABOVE>',
                                     "enableFeature 450_".$DeviceName.'_'.$architecturename.'   # '.$ArchitectureDescription."\n",
                                     QUIET         => 1,
                                     PREVIOUS_LINE => 1,
                                     PREFIX        => $Prefix
                                   );
      }
      if ($ContentOrig ne $Content) { 
        writeFile($TargetFile, $Content); 
        if ($IsInstallScript) {
          system("chmod +x '$TargetFile'");
        }
      }
    }
    else { print "ignoring template '$TemplateFile'\n"; }
  }

  return $AmountNewFiles;
}
sub collect_Architectures {       # scans filesystem for existing low-level drivers
  my $DeviceName = shift;
  my $devicename = lc($DeviceName);
  my $DEVICE_NAME = uc($DeviceName);

  my $RelativeDirPrefix = "../$devicename/";
  my $RelativeInstallPrefix = "../../InstallData/low_level/";
  my @ArchitectureNames = findAllArchitectures($devicename);
  
  my %Architectures;
  foreach my $ArchitectureName (@ArchitectureNames) {
    unless ($Architectures{$ArchitectureName}) {
      my $ARCHITECTURENAME = uc($ArchitectureName);
      
      my $SourceFile  = "${RelativeDirPrefix}${devicename}_${ArchitectureName}.c";
      my $HeaderFile  = "${RelativeDirPrefix}${devicename}_${ArchitectureName}.h";
      my $TypeFile    = "${RelativeDirPrefix}${devicename}_${ArchitectureName}_types.h";
      my $InstallFile = "${RelativeInstallPrefix}install_TTC_${DEVICE_NAME}_${ARCHITECTURENAME}.sh";
      
      $Architectures{$ArchitectureName} = {
                                            File_Source  => $SourceFile,
                                            File_Header  => $HeaderFile,
                                            File_Types   => $TypeFile,
                                            File_Install => $InstallFile 
                                          };
      
      # check for existing files
      map { my $ca_FilePath = $Architectures{$ArchitectureName}->{$_};
            unless (-e $ca_FilePath) {
              $Architectures{$ArchitectureName}->{$_} = undef;
              if (substr($ca_FilePath, 0, 1) eq '/') 
                { print "file not found: $ca_FilePath\n"; }
              else
                { print "file not found: $PWD/$ca_FilePath\n"; }
            }
            else { affectFile($ca_FilePath, 'SEEN' ); }
          } keys %{$Architectures{$ArchitectureName}};

      $Architectures{$ArchitectureName}->{Name} = $ArchitectureName;

      if ($Verbose>1) { 
        print "$0 - found $DeviceName driver for '$ArchitectureName'\n";
        print "    ".join("\n    ", map { "$_ => '$Architectures{$ArchitectureName}->{$_}'"; } sort keys %{$Architectures{$ArchitectureName}} )."\n";
      }
    }
  }
  
  return %Architectures; #{
  #
  # +----------------+
  # | %Architectures |
  # +----------------+   +-------------------------------+
  # | Architecture1 ===> | %Architecture                 |
  # | ...            |   +-------------------------------+
  # +----------------+   | Name =========> $Name         | <-- name of this architecture
  #                      | File_Source ==> $File_Source  | <-- relative file-path of source file (<device>_<architecture>.c)
  #                      | File_Header ==> $File_Header  | <-- relative file-path of header file (<device>_<architecture>.h)
  #                      | File_Types ===> $File_Types   | <-- relative file-path of datatype file (<device>_<architecture>_types.h)
  #                      | File_Install => $File_Install | <-- relative file-path of install script (install_TTC_<DEVICE>_<ARCHITECTURE>.sh)
  #                      +-------------------------------+
  #}
}
sub collect_InterfaceCalls {      # scan given C-source for function calls to corresponding interface functions
  my $DeviceName = shift;
  my $SourceFile = shift;
  my $devicename = lc($DeviceName);
  
  my $Prefix = 'ttc_'.$devicename.'_';

  print "collecting interface calls from '$PWD/$SourceFile' ..\n";
  my %InterfaceCalls;
  foreach my $FunctionName ( sort keys %{ $SourceInfo_HighLevel{FunctionDefinitions} } ) {
    if (index($FunctionName, $Prefix) == 0) { # found ttc_<device>_*() function
      my $Function = $SourceInfo_HighLevel{FunctionDefinitions}->{$FunctionName};
      my $FunctionArguments = $Function->{Arguments};
      my $ReturnType        = $Function->{Return};
      
      my @FunctionName = split("_", $FunctionName);
      my @InterfaceFunctionName;
      push @InterfaceFunctionName, shift(@FunctionName); # ttc
      push @InterfaceFunctionName, shift(@FunctionName); # device
      push @InterfaceFunctionName, 'interface';
      push @InterfaceFunctionName, @FunctionName;        # rest of functionname
      my $InterfaceFunctionName = join('_', @InterfaceFunctionName);
      
      my $Arguments = join(", ", @$FunctionArguments);
      unless ($InterfaceCalls{$InterfaceFunctionName}) {
        print "  new interface call: '$ReturnType $InterfaceFunctionName($Arguments)'\n";
        $InterfaceCalls{$InterfaceFunctionName} = { Name => $InterfaceFunctionName,
                                               Arguments => $FunctionArguments,
                                                  Caller => { $FunctionName => 1 },
                                                  Return => $ReturnType
                                                  };
      }
      else {
        $InterfaceCalls{$InterfaceFunctionName}->{Caller}->{$FunctionName} = 1;
      }
    }
  }
  
  return %InterfaceCalls; #{
  #
  # +-------------------+
  # | %InterfaceCalls   |
  # +-------------------+
  # |                   |   +--------------------+
  # | FunctionName1 ======> | %InterfaceFunction |
  # | ...               |   +--------------------+
  # +-------------------+   | Name   => $Name    | <-- name of this interface function
  #                         | Return => $Type    | <-- string: type of return value
  #                         |                    |
  #                         |                    |   +--------------------+
  #                         | Arguments ===========> | @FunctionArguments |
  #                         |                    |   +--------------------+
  #                         |                    |   | $Argument1         | string: type+name of argument
  #                         |                    |   | ...                |
  #                         |                    |   +--------------------+
  #                         |                    |
  #                         |                    |   +---------------+
  #                         | Caller ==============> | %Caller       |
  #                         |                    |   +---------------+
  #                         +--------------------+   | $Caller1 => 1 | string: name of function calling this interface function
  #                                                  | ...           |
  #                                                  +---------------+
  #}
}
sub collect_DriverDeclarations {  # scan given header file for function declarations of type _driver_<device>_();
  my $DeviceName = shift;
  my $HeaderFile = shift;
  my $devicename = lc($DeviceName);
  
  my $Prefix = '_driver_'.$devicename.'_';
                                     
  if ($Verbose) { print "collecting _driver_*() functions from '$PWD/$HeaderFile' ..\n"; }
  my %SourceInfo_Header = parseSource($HeaderFile);
  my %DriverDeclarations;
  
  foreach my $FunctionDeclaration ( values %{ $SourceInfo_Header{FunctionDeclarations} } ) {
    my $FunctionName = $FunctionDeclaration->{Name};
    if (substr($FunctionName, 0, length($Prefix)) eq $Prefix) {
      if ($Verbose) { 
        my $Declaration = $FunctionDeclaration->{Return}." $FunctionName(".join(', ', @{ $FunctionDeclaration->{Arguments} }).")";
        print "  found driver declaration: $Declaration\n";
        if (index($Declaration, ';') > -1) {
          dieOnError( "collect_DriverDeclarations($HeaderFile): Invalid declaration found!", 
                      "    Return='$FunctionDeclaration->{Return}.'",
                      "    Name='$FunctionName'",
                      map { "    Argument: '$_'"; } @{ $FunctionDeclaration->{Arguments} }
                    );
        }
      }
      $DriverDeclarations{$FunctionName} = $FunctionDeclaration;
    }
  }
  
  return %DriverDeclarations; #{
  #
  # +---------------------+
  # | %DriverDeclarations |
  # +---------------------+
  # |                     |   +--------------------+
  # | FunctionName1 ========> | %DriverFunction    |
  # | ...                 |   +--------------------+
  # +---------------------+   | Name   => $Name    | <-- name of this driver function
  #                           | Return => $Type    | <-- string: type of return value
  #                           |                    |
  #                           |                    |   +--------------------+
  #                           | Arguments ===========> | @FunctionArguments |
  #                           |                    |   +--------------------+
  #                           |                    |   | $Argument1         | string: type+name of argument
  #                           |                    |   | ...                |
  #                           |                    |   +--------------------+
  #                           |                    |
  #                           |                    |
  #                           |                    |
  #                           +--------------------+
  #}
}
sub compileFunctionDefinition {   # compiles default implementation of a function
  my $DeviceName   = shift;
  my $FunctionName = shift;            # name of function to create
  my $ReturnType   = shift || 'void';  # type of return value (if any)
  my $Arguments    = shift;            # array-ref: list of arguments (strings of type "<type> <name>")
  my $Comment      = shift;            # multiline comment string "/** ... */"
  my $Code;
  my $DEVICE_NAME = uc($DeviceName);
  if (substr($Comment, 0, 2) ne '/*') { $Comment = ''; } # remove malformed comment
  if ($Verbose > 1) {
    my $ArgumentsText = "'".join("',\n                 '", @$Arguments)."'";
    print <<"END_OF_TEXT";

compileFunctionDefinition(
  DeviceName   = '$DeviceName'
  FunctionName = '$FunctionName'
  ReturnType   = '$ReturnType'
  Arguments    = $ArgumentsText
  Comment      = '$Comment'
);

END_OF_TEXT
  }
  
  my $ReturnCode = ($ReturnType ne 'void') ? "    TTC_TASK_RETURN ( 0 );  // will perform stack overflow check and return given value" : '';
  my @AssertCode;
  my @Comments;
  my @CleanedArguments;
  foreach my $Argument ( @$Arguments ) {
    my @Parts = grep { $_; } split(" ", $Argument);
    my $VariableName = pop(@Parts);
    while (substr($VariableName, 0, 1) eq '*') {
      substr($VariableName, 0, 1) = undef;
      $Parts[-1].='*';
    }
    my $VariableType = join(" ", @Parts);
    
    if (index($VariableType, '*') > -1) { # pointer variable
      if (substr($FunctionName, 0, 4) eq 'ttc_') {                               # high-level function: create default assert
        push @AssertCode, "    Assert_${DEVICE_NAME}_Writable($VariableName, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)";
      }
      elsif (substr($FunctionName, 0, length($DeviceName)) eq lc($DeviceName)) { # low-level function:  create extra assert
        push @AssertCode, "    Assert_${DEVICE_NAME}_EXTRA_Writable($VariableName, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)";
      }
      else {                                                                     # other function:      create generic assert
        push @AssertCode, "    Assert(ttc_memory_is_writable($VariableName), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)";
      }
    }
    if (index($Comment, $VariableName) == -1) { # argument is missing in comment text: add it
      push @Comments, " * \@param $VariableName   ";
    }
    push @CleanedArguments, "$VariableType $VariableName";
  }
  if ($ReturnType ne 'void') { 
    if (index($Comment, '@return') == -1) { push @Comments, " * \@return   "; } 
  }
  
  my $MaxCommentLength = 0; #{ make all comments equal length
  map { if ($MaxCommentLength < length($_)) { $MaxCommentLength = length($_); } } @Comments;
  @Comments = map { $_.'= '; } map { substr($_.'                                        ', 0, $MaxCommentLength); } @Comments;
  #}
  
  my $Prototype = "$ReturnType $FunctionName(".join(', ', @CleanedArguments).")";
  my $ArgumentsCode = join(', ', @CleanedArguments);
  my $AssertCode    = join("\n", @AssertCode);
  my $CommentLines  = join("\n", grep { $_; } @Comments);
  if ($Comment) { # integrate @Comments into existing $Comment
    my $InsertPos = rindex($Comment, '*/');
    if ($CommentLines) {
      $Comment = strip(substr($Comment, 0, $InsertPos))."\n".$CommentLines."\n */";
    }
  }
  else { $Comment = <<"END_OF_STRING";
/** $FunctionName()
 *
$CommentLines
 */
END_OF_STRING
  }
  $Comment = strip($Comment)."\n";
  my $Code = <<"END_OF_STRING"; #{
$Prototype {
$AssertCode

#warning Function $FunctionName() is empty.
    
$ReturnCode
}
END_OF_STRING
#}
  
  return $Comment, $Prototype, $Code;
}
sub compileTargetFileName {       # convert given template into target file name
  my $Template         = shift;
  my $DeviceName       = shift;
  my $ArchitectureName = shift;
  
  my $DEVICE_NAME       = uc($DeviceName);
  my $devicename       = lc($DeviceName);
  my $ARCHITECTURENAME = uc($ArchitectureName);
  my $architecturename = lc($ArchitectureName);

  $Template =~ s/DEVICE/$DEVICE_NAME/g;
  $Template =~ s/device/$devicename/g;
  $Template =~ s/ARCHITECTURE/$ARCHITECTURENAME/g;
  $Template =~ s/architecture/$architecturename/g;
  
  return $Template;
}
sub findAllArchitectures {        # collects names of all currently available low-level drivers
  my $DeviceName       = shift;
  my $devicename       = lc($DeviceName);
  
  my $Files = getFileNames("../$devicename/${devicename}_*.c"); # `ls ../$devicename/${devicename}_*.c`;
  my @Files = split("\n", $Files);
  
  my @Architectures = grep { $_ ne 'common'; } # common is not an architecture
                      map {
                        # Architecture is string after firs '_' after last '/' and before first dot.
                        # This definition allows hierarchical architecture names that contain undescores.
                        #
                        # E.g.: '../math/math_software_float.c'
                        #    => $Architecture = 'software_float'
                        my $LastSlashPos = rindex($_, '/') + 1;
                        my $UnderscorePos = index($_, '_', $LastSlashPos) + 1;
                        my $DotPos = index($_, '.', $UnderscorePos);
                        my $Architecture = ($DotPos > -1) ? substr($_, $UnderscorePos, $DotPos - $UnderscorePos) : '';  
                        
                        $Architecture;
                      } @Files;
                                            
  return grep { $_; } sort @Architectures;
}
sub findLineNo {                  # scans given string to determine line number of given position
  my $String   = shift;
  my $Position = shift;
  
  my $LineNo = 0;
  my $LastLFPos = -1;
  my $NextLFPos;
  do {
    $LineNo++;
    $NextLFPos = index($String, "\n",  $LastLFPos + 1);
    $LastLFPos = $NextLFPos;
  } while ( ($NextLFPos < $Position) && ($NextLFPos != -1) );
  
  return $LineNo;
}
sub formatArguments {             # formats given list of arguments
  my @Arguments = @_; # list of strings: "TYPE NAME"
  
  my @FormattedArguments;
  
  foreach my $Argument (@Arguments) { # change multi word argument types like "struct foo_s **A" -> ""struct foo_s** A"
    if ($Argument) {
      my $OldArgument = $Argument;
      my @Parts = grep { $_; } split(" ", $Argument);
      my $NameAndType = strip( pop(@Parts) );
      my $Type = join(" ", @Parts);
      $NameAndType =~ /([a-zA-Z0-9_]+)$/;
      my $Name = $1;
      my $MatchPos = index($NameAndType, $Name);
      $Type .= substr($NameAndType, 0, $MatchPos);
      $Argument = $Type.' '.$Name;
      push @FormattedArguments, $Argument;
    }
  }

  return @Arguments;
}
sub identifyTemplate {            # identifies given entry from @AllTemplateFiles
    my $File = shift;
    my $DeviceName       = shift;
    my $ArchitectureName = shift || 'NONE';
    my $DEVICE_NAME = uc($DeviceName);
    
    my $TargetFile;
    my $IsHighLevelDriver   = 0;
    my $IsLowLevelDriver    = 0;
    my $IsInterface         = 0;
    my $IsInstallScript     = 0;
    my $IsInstallScriptLow  = 0;
    my $IsInstallScriptHigh = 0;
    my $IsExampleCode       = 0;
    my $IsExampleCodeLow    = 0;
    my $IsExampleCodeHigh   = 0;
    
    if (substr($File, 0, 4)   eq 'ttc_') {         # high-level <device> or interface
      if (substr(substr($File, -12), 0, 10) eq '_interface') { # interface
        $TargetFile = "../interfaces/$File";
        $IsInterface = 1;
      }
      else {                                                   # high-level driver
        $TargetFile = "../$File";
        $IsHighLevelDriver = 1;
      }
    }
    if (substr($File, 0, 7)   eq 'device_') {      # low-level <device>
      if (! -d "../$DeviceName") { mkdir("../$DeviceName"); }
      if ($ArchitectureName) { # low-level files require an architecture
        $TargetFile = "../$DeviceName/$File";
      }
      $IsLowLevelDriver = 1;
    }
    if (substr($File, 0, 9)   eq '_install_') {    # install script
      
      if (index($File, 'ARCHITECTURE') > -1) { # found low-level install script
        $IsInstallScriptLow  = 1;
        if ($ArchitectureName) { # low-level files require an architecture
          $TargetFile = "../../InstallData/low_level/".substr($File,1);
        }
      }
      else {                                   # found high-level install script
        $IsInstallScriptHigh = 1;
        $TargetFile = "../../InstallData/".substr($File,1);
      }
        
      my (@ExistingTTC_Files, $ExistingTTC_Files);
      if ($IsInstallScriptLow) {  # collect all corresponding low_level/install_TTC_<DEVICE>_* files
        @ExistingTTC_Files = getFileNames("../../InstallData/low_level/install_TTC_${DEVICE_NAME}_*.sh"); # `ls 2>/dev/null ../../InstallData/low_level/install_*_TTC_* | sort -n`;
      }
      else {                      # collect all install_NN_TTC_* files
        @ExistingTTC_Files = getFileNames("../../InstallData/install_*_TTC_*"); # `ls 2>/dev/null ../../InstallData/install_*_TTC_* | sort -n`;
      }
      @ExistingTTC_Files = sort { $a cmp $b; } @ExistingTTC_Files;
      
      if ( index($TargetFile, '_NN_') > -1 ) { # filename has placeholder for order number: Find new/ existing order number for low-level install script
        my $TargetRank = 0;
        foreach my $ExistingTTC_File (@ExistingTTC_Files) {
          # E.g.: "../../InstallData/install_24_TTC_USART.sh"
          my $LastSlashPos = rindex($ExistingTTC_File, '/');
          my $StartPos = index($ExistingTTC_File, "_",$LastSlashPos) + 1;
          my $EndPos   = index($ExistingTTC_File, "_", $StartPos);
          my $Rank = substr($ExistingTTC_File, $StartPos, $EndPos - $StartPos) + 0;
          my $CompareFile = $TargetFile;
          my $String = $Rank;
          while (length($String) < 3) { $String = "0".$String; }
          $CompareFile =~ s/NN/$String/g;
          $CompareFile = compileTargetFileName($CompareFile, $DeviceName, $ArchitectureName);
          if ($CompareFile eq $ExistingTTC_File) { # target file aready exists: reuse rank
            $TargetRank = $Rank;
            last;
          }
          else {                                   # use next rank as target rank
            $TargetRank = (scalar @ExistingTTC_Files) + 1;
          }
        }
        while (length($TargetRank) < 3) { $TargetRank = "0".$TargetRank; }
        $TargetFile =~ s/NN/$TargetRank/g;
      }
      $IsInstallScript = 1;
    }
    if (substr($File, 0, 8)   eq 'example_') {     # example code
      $TargetFile = "../../Template/examples/$File";
      $IsExampleCode = 1;
      if (index($File, '_architecture.') > -1) { $IsExampleCodeLow = 1; }
      else                                     { $IsExampleCodeHigh = 1; }
    }
    if (substr($File, 0, 11)  eq 'regression_') {  # regression test code
      $TargetFile = "../../Template/regressions/$File";
      $IsExampleCode = 1;
      if (index($File, '_architecture.') > -1) { $IsExampleCodeLow = 1; }
      else                                     { $IsExampleCodeHigh = 1; }
    }

    if ( (!$Update) && (uc($ArchitectureName) eq 'NONE') &&
         ($IsLowLevelDriver || $IsInstallScriptLow || $IsInterface || $IsExampleCodeLow)
       )
    { $TargetFile = undef; } # user wants no low-level files 
    else {
      $TargetFile = compileTargetFileName($TargetFile, $DeviceName, $ArchitectureName);
      if (-e $TargetFile) { affectFile($TargetFile, 'SEEN'); }
    }

    return ( $TargetFile,
             $IsHighLevelDriver,
             $IsLowLevelDriver,
             $IsInterface,
             $IsInstallScript,
             $IsInstallScriptLow,
             $IsInstallScriptHigh
            );
}
sub insertIntoString {            # inserts given text into given string before insert mark
  my $FileName   = shift;  # used as reference in error-message 
  my $String     = shift;  # string to insert into
  my $InsertMark = shift;  # will search for position of $InsertMark in $String and insert $Text before
  my $Text       = shift;  # text to insert
  my %Options = @_;        # optional named argument as listed below
  my $Quiet           = $Options{QUIET};            # ==1: no error message if InsertMark is not found
  my $InPreviousLine  = $Options{PREVIOUS_LINE};    # ==1:insert $Text in line before $InsertMark; ==0: insert in same line directly before $InserMark
  my $Prefix          = $Options{PREFIX};           # if given, will be added in front of $Text (e.g. '#' to insert a comment)
  my $IfNotContains   = $Options{IF_NOT_CONTAINS};  # if given, $Text will be inserted if content of $IfNotContains is not found in $String  
  
  my $AlreadyPresent = ($IfNotContains) ? index($String, $IfNotContains) + 1 : # look for existence of different text
                                          index($String, $Text) + 1;           # check if text to be inserted is already present 
  unless ($AlreadyPresent) { # Text not yet included in String: insert it
    my $LastPos = -1;
    my $InsertPos = index($String, $InsertMark, $LastPos+1);
    if ($InsertPos > -1) {
      while ($InsertPos > -1) { # insert text before all occurences of insert mark
        if ($InPreviousLine) { # find position of previous line
          my $LastNLPos = rindex(substr($String, 0, $InsertPos), "\n"); # position of last newline before insert mark (end of previous line)
          if ($LastNLPos > -1) {
            $InsertPos = $LastNLPos + 1;
            if (substr($Text, -1) ne "\n") { $Text .= "\n"; } # LF missing: ensure that text to be inserted is not inserted into same line
          }
        }

        substr($String, $InsertPos, 0) = $Prefix.$Text;
        
        $LastPos   = index($String, $InsertMark, $InsertPos);   # find same insert mark again (text has been inserted before) 
        $InsertPos = index($String, $InsertMark, $LastPos + 1); # search for other insert marks
      }
    }
    elsif (!$Quiet) { # ERROR
      my $PWD = `pwd`;
      chomp($PWD);
      print "$0 - insertIntoString($FileName) - ERROR: Missing insert mark '$InsertMark'\n";
      print "     Insert this line into file '$PWD/$FileName':\n";
      print "       $InsertMark  above (DO NOT DELETE THIS LINE!)\n";
      print "\n";
      dieOnError("Driver Creation Canceld!");
    }
  }

  return $String;
}
sub parseSource {                 # scans given source for function definitions and declarations + function calls
  my $FileName   = shift; # used for exact error message
  my $PureSource = shift || readPureSource($FileName, $RawSource); # pure source code as returned by readPureSource()
  my $RawSource  = readFile($FileName);
  if ($Verbose) { print "parseSource($FileName)...\n"; }
  
  my @LastLevel;
  push @LastLevel, $Hierarchy;
  my $LastPos = 0;

  my $Hierarchy = []; #{ Hierarchy of nested arrays according to curly brackets { } in $PureSource
  #
  # a { b { c } } d { e } gives
  #
  # [ a, [ b, [c] ], d, [e] ]
  #
  #}
  
  my $Pos;
  for ($Pos = 0; $Pos < length($PureSource); $Pos++) { # build hierarchy
    my $C = substr($PureSource, $Pos, 1);
    if ( ($C eq '/') && (substr($PureSource, $Pos + 1, 1) eq '/') ) { # skip comment until end of line
      $Pos = index($PureSource, "\n", $Pos);
      if ($Pos == -1) { $Pos = length($PureSource); }
    }
 elsif ( $C eq '"' ) { # skip string
      my $StartPos = $Pos;
      do { $Pos = index($PureSource, '"', $Pos + 1); }
      while ( ($Pos > -1) && (substr($PureSource, $Pos-1) eq '\"') );
      if ($Pos == -1) { dieOnError("parseSource($FileName) - ERROR: String runs until end of file: '".strip( substr($PureSource, $StartPos) )."'\n"); }
    }
 elsif ( $C eq "'" ) { # skip character
      $Pos = index($PureSource, "'", $Pos + 1);
      if ($Pos == -1) { $Pos = length($PureSource); }
    }
 elsif ( $C eq '{' ) {
      push @$Hierarchy, substr($PureSource, $LastPos, $Pos - 1 - $LastPos + 1); # append text before opening curly bracket
      my $NewLevel = [$Hierarchy]; # first entry points back to upper level
      push @$Hierarchy, $NewLevel;
      $Hierarchy = $NewLevel;      # move 1 level down in hierarchy
      $LastPos = $Pos + 1;         # position of PureSource after open bracket
    }
 elsif ( $C eq '}' ) {
      push @$Hierarchy, substr($PureSource, $LastPos, $Pos - 1 - $LastPos + 1); # append text before closing curly bracket
      if (ref($Hierarchy->[0]) eq 'ARRAY') {
        my $PreviousHierarchy = $Hierarchy->[0];
        shift(@{ $Hierarchy });           # remove back-ref from current level
        $Hierarchy = $PreviousHierarchy;  # move 1 level up in hierarchy
      }
      else {
        my $NextLFPos = index($PureSource, "\n", $Pos);
        my $LineNo = findLineNo($PureSource, $Pos);
        my $TempSource = `pwd`; chomp($TempSource);
        $TempSource .= "/parseSource_temp.c";
        writeFile($TempSource, $PureSource);
        dieOnError( "parseSource($FileName) - ERROR: Inconsistent curly brackets !\n".
                    "\$Hierarchy->[0]='".strip($Hierarchy->[0])."')\n".
                    "Closing Bracket: '".substr($PureSource, $Pos, $NextLFPos - $Pos)."'\n".
                    "Line $LineNo of file $TempSource"
                  ); 
      }
      $LastPos = $Pos + 1;         # position in PureSource after close bracket
    }
  }
  push @$Hierarchy, substr($PureSource, $LastPos, $Pos - 1 - $LastPos + 1);
  my %AllFunctions;
  my %AllDeclarations;
  my %AllMacros;
  my %AllUndefines;
  my %AllDefines;
  my %AllEnums;
  
  my @Matches = ($RawSource =~ /#\s*define\s+([a-zA-Z0-9_]+)\s*\(([a-zA-Z0-9_\,\ ]*)\)\s+(.+)\n/g);
  while (@Matches) { # collect all macro definitions
    my $Name    = shift(@Matches);
    my $ArgumentList = shift(@Matches);
    my $Definition   = shift(@Matches);
    
    my @ArgumentList = map { strip($_); } split(',', $ArgumentList);
    $AllMacros{$Name} = {
                               MacroName => $Name,
                               Arguments => \@ArgumentList,
                              Definition => $Definition 
                             };
    
    if ($Verbose>1) {
      print "  macro-declaration: '$Name' ('".join("','", @ArgumentList)."')\n";
      print "                 --> '$Definition'\n";
    }
  }
  
  @Matches = ($RawSource =~ /#\s*undef\s+([a-zA-Z0-9_]+)\s*\n/g); # undef
  while (@Matches) { # collect all macro undefs
    my $Name    = shift(@Matches);
    
    $AllUndefines{$Name} = 1;
    
    if ($Verbose>1) {
      print "  undefine: '$Name'\n";
    }
  }
  
  if ($FileName eq '') {  $Verbose=2; } #D
  @Matches = ($RawSource =~ /#\s*define\s+([a-zA-Z0-9_]+)\s*(\w*).*\n/g); # define NAME VALUE
  while (@Matches) { # collect all defines that are not a macro
    my $Name    = shift(@Matches);
    my $Value   = shift(@Matches);
    
    unless ($AllMacros{$Name}) {
      $AllDefines{$Name} = ($Value eq undef) ? 1 : $Value;
    
      if ($Verbose>1) {
        print "  define: '$Name'='$Value'\n";
      }
    }
  }
  
  sub compileSubHierarchy { # reconstructs source code from sub hierarchy
    my $StartLevel = shift; # array-ref: entry from @$Hierarchy or one of its sub entries
    
    my $Source;
    foreach my $Item (@$StartLevel) {
      if (ref($Item) eq 'ARRAY') { $Source .= " {\n".compileSubHierarchy($Item)."\n}\n"; }
      else                       { $Source .= $Item; }
    }
    
    return $Source;
  }
  
  for (my $Index = 0; $Index < scalar(@$Hierarchy); $Index++) { # scan first level of hierarchy for function declarations + definitions
    my $Current = $Hierarchy->[$Index];   # current source code fragment
    my $Next    = $Hierarchy->[$Index+1];
    my $NextNext= $Hierarchy->[$Index+2];
    
    if (ref($Next) eq 'ARRAY') { # foo { ... }
      $Index++; # skip $Next
      
      if (1) { # scan for function definition
        $Current =~ /([a-zA-Z0-9_\*]+)\s+([a-zA-Z0-9_]+)\s*\((.*)\)\s*$/; # find "bla foo(args) " at end of string
        my $ReturnType   = $1;
        my $FunctionName = $2;
        my $ArgumentList = $3;
        my @Arguments = split(',', $ArgumentList);
        if ($Verbose>1) { print "  function-definition: '$ReturnType' '$FunctionName' ('".join("','", @Arguments)."')\n"; }
        
        my $FunctionSource = compileSubHierarchy($Next);
        my @FunctionCalls;
        
        if (1) { # scan for function calls from this function - ToDo: Currently only finds calls ending with ");"
          my $FunctionPureSource = $FunctionSource;
          $FunctionPureSource =~ s/\n//g;
          $FunctionPureSource =~ s/ +/ /g;
          
          my @Calls = split(";", $FunctionPureSource);
          map {
            $_ =~ / ([a-zA-Z0-9_]+)\((.+)\)$/;
            if ($1) {
              my $Call          = "$1";
              my $Arguments     = "$2";
              my @Arguments = split(",", $Arguments);
              push @FunctionCalls, {
                     Name => $Call,
                Arguments => \@Arguments
              };
              if ($Verbose > 1) { print "    function call: '$Call($Arguments)'\n"; }
            }
          } @Calls;
        }
      
        $AllFunctions{$FunctionName} = {
          Return    => ($ReturnType) ? $ReturnType : 'void',
          Arguments => \@Arguments,
          Source    => $FunctionSource,
          Calls     => \@FunctionCalls
        }
      }
    }
    if (1) { # scan for function declarations
      my @Matches = ($Current =~ /([a-zA-Z0-9_\*]+)\s+([a-zA-Z0-9_]+)\s*\((.*)\)\s*;/g);
      
      while (@Matches) {
        my $ReturnType   = shift(@Matches);
        my $FunctionName = shift(@Matches);
        my $ArgumentList = shift(@Matches);
        
        if (index($ArgumentList, ";") > -1) { # ERROR: Invalid function declaration
          dieOnError( 
                      "parseSource($FileName): Invalid argument list extracted. Check above implementation!",
                      "Arguments => '$ArgumentList'",
                      "Current: -------------------------\n$Current\n---------------------------------\n\n"
                    );
        }

        my $FunctionPos = length($RawSource);
        my $Invalid = 1;
        do { # find start + end of function comment block
          $FunctionPos = rindex($RawSource, $FunctionName, $FunctionPos - 1);
          if ($FunctionPos > -1) {
            my ($Line, $PositionInLine) = strip( extractLine($RawSource,$FunctionPos) );
            my $CommentStart = index($Line, "//");
            if ($CommentStart > -1) { substr($Line, $CommentStart) = undef; } 
            $CommentStart = index($Line, "/*");
            if ($CommentStart > -1) { substr($Line, $CommentStart) = undef; }
            $Line = strip($Line);
            if (substr($Line, -1)   eq ';')  { $Invalid = 0; } # declarations end with a semicolon
            if (substr($Line, 0, 1) eq '#')  { $Invalid = 1; 
            } # no preprocessor directives 
            if (rindex($Line, '//', $PositionInLine) > -1) { $Invalid = 2; } # declaration inside single line comment
            
            my $NextCommentEnd   =  index($RawSource, '*/', $FunctionPos); if ($NextCommentEnd   == -1) { $NextCommentEnd   = length($RawSource); }
            my $NextCommentStart =  index($RawSource, '/*', $FunctionPos); if ($NextCommentStart == -1) { $NextCommentStart = length($RawSource); }
            my $PrevCommentEnd   = rindex($RawSource, '*/', $FunctionPos);
            my $PrevCommentStart = rindex($RawSource, '/*', $FunctionPos);
            my $InsideComment = ($NextCommentEnd < $NextCommentStart) && ($PrevCommentEnd <= $PrevCommentStart);
            if ($InsideComment) { $Invalid = 3; }
          }
          else { 
            my $Content = (0) ? "inside\n".$RawSource."\n-------------------------" : undef;
            dieOnError("parseSource($FileName) - No function declaration found for '$FunctionName'! $Content\n"); 
          }
        } while ($Invalid);
        
        my $FunctionComment; #{ extract function comment (if any)
        my $PrevCommentEnd   = rindex($RawSource, '*/', $FunctionPos);
        my $PrevCommentStart = rindex($RawSource, '/*', $PrevCommentEnd);
        my $FunctionComment    = strip( substr($RawSource, $PrevCommentStart, $PrevCommentEnd - $PrevCommentStart + 3) );
        #}
        #{ extract multi word return type like "struct foo_s*"
        my $LastLF_Pos = rindex(substr($RawSource, 0, $FunctionPos), "\n");
        $ReturnType = strip( substr($RawSource, $LastLF_Pos, $FunctionPos - $LastLF_Pos) );
        #}
        my @Arguments = formatArguments( split(',', $ArgumentList) );
        
        $AllDeclarations{$FunctionName} = { Name => $FunctionName,
                                          Return => $ReturnType,
                                       Arguments => \@Arguments,
                                         Comment => $FunctionComment
                                          };
        if ($Verbose>1) { print "  function-declaration: '$ReturnType' '$FunctionName' ('".join("','", @Arguments)."')\n"; }
      }
    }
    if (1) { # scan for enums
      my $LastPos = 0;
      my $PureSourceNoLines = $Current; $PureSourceNoLines =~ s/\n//g;
      my $EnumPos;
      do {
        my $EnumPos1 = index($PureSourceNoLines, 'enum', $LastPos);
        my $EnumPos2 = index($PureSourceNoLines, 'typedef enum', $LastPos);
        
        $EnumPos = -1;
        if ( ($EnumPos1 > -1) && ($EnumPos2 > -1) ) { # both types found: use fist one
          $EnumPos = ($EnumPos1 < $EnumPos2) ? $EnumPos1 : $EnumPos2;
        }
        elsif ($EnumPos1 > -1) { $EnumPos = $EnumPos1; }
        elsif ($EnumPos2 > -1) { $EnumPos = $EnumPos2; }
        
        if ($EnumPos > -1) {
          $LastPos = $EnumPos + 1;
          my %Enum;
          if ($EnumPos == $EnumPos2) { # typedef enum { a,b,c, ... } e_name;
            my $Pos = index($NextNext, ';');
            $Enum{Name} = strip(  substr($NextNext, 0, $Pos) );
            my @Items = grep { $_; } 
                        map { strip($_); }
                        map { 
                          my $Pos = index($_, '='); 
                          if ($Pos > -1) { substr($_, 0, $Pos); }
                          else           { $_; }
                        }
                        split(",", $Next->[0]); # items are stored in sub-level
            $Enum{Items} = \@Items;
          }
          else {                      # ToDo: enum name { a,b,c, ... };
            
          }
          if (%Enum) { 
            if ($Verbose > 0) { print "  found Enum $Enum{Name} = (".join(', ', @{ $Enum{Items} }).")\n"; }
            $AllEnums{$Enum{Name}} = \%Enum;
          }
        }
      } while ($EnumPos > -1);
    }
  }

  my @AllIncludes; #{ extract all include directives
    my @Lines = split("\n", $RawSource);
    my $LineNo = 0;
    foreach my $Line (@Lines) {
      $LineNo++;
      if ( (substr($Line, 0, 1) eq '#') ) { # 
        if (index($Line, 'include') > -1) {
          $Line =~ /#\s*include \"([0-9a-zA-Z _\.\/]+)\"/;
          if ($1) {
            my $ps_FilePath = $1;
            my @Parts = split("/", $ps_FilePath);
            my $IncludeFileName = pop(@Parts);
            push @AllIncludes, {
              FilePath => $ps_FilePath,
              FileName => $IncludeFileName
            };
            if ($Verbose>1) { print "  line $LineNo: #include '$ps_FilePath' ($IncludeFileName)\n"; }
          }
          else {
            print "$0 - ERROR: Cannot parse include in line $LineNo: '$Line'!\n";
          }
        }
      }
    }
  #}
  
  my %SourceInfo = ( #{
                     FileName              => $FileName,             # name of original file on disc
                     PureSource            => $PureSource,           # stripped, complete source code 
                     FunctionDeclarations  => \%AllDeclarations,     #{ list of found function declarations
                     #
                     # +------------------+
                     # | %AllDeclarations |
                     # +------------------+   +----------------------+
                     # | FunctionName1 -----> | %FunctionDeclaration |
                     # | ...              |   +----------------------+
                     # |                  |   | Name    => $Name     | <-- string: name of this function
                     # |                  |   | Return  => $Type     | <-- string: type of returned value
                     # |                  |   | Comment => $Comment  | <-- string: multiline comment of this function
                     # |                  |   |                      |   +------------+
                     # |                  |   | Arguments =============> | @Arguments |
                     # |                  |                          |   +------------+
                     # |                  |   |                      |   | $Argument1 <-- string: first argument (type + name) (E.g. "t_u8* foo")
                     # |                  |   |                      |   | ...        |
                     # |                  |   +----------------------+   +------------+
                     # +------------------+
                     #
                     # Legend:
                     #   $Reference --> Target 
                     #          Key ==> Value
                     #}
                     FunctionDefinitions   => \%AllFunctions,        #{ parsed code by each function definition
                     #
                     # +----------------+
                     # | %AllFunctions  |
                     # +----------------+   +------------+
                     # | FunctionName1 ===> | %Function  |
                     # | ...            |   +------------+
                     #                      | Return     | <-- string: return type of function
                     #                      | Source     | <-- string: pure source code of function
                     #                      |            |
                     #                      |            |   +------------+
                     #                      | Arguments ===> | @Arguments |
                     #                      |            |   +------------+
                     #                      |            |   | $Argument1 | <-- string: first argument (type + name) (E.g. "t_u8* foo")
                     #                      |            |   | ...        |
                     #                      |            |   +------------+
                     #                      |            |
                     #                      |            |   +-----------------+
                     #                      | Calls =======> | @Calls          |
                     #                      |            |   +-----------------+   +----------------+
                     #                      |            |   | $FunctionCall1 ---> | %$FunctionCall |
                     #                      |            |   | ...             |   +----------------+
                     #                      |            |   +-----------------+   | Name => $Name  | <-- string: name of called function
                     #                      |            |                         |                |   +------------+
                     #                      |            |                         | Arguments =======> | @Arguments |
                     #                      |            |                         +----------------+   +------------+
                     #                      |            |                                              | $Argument1 | <-- string: single argument 
                     #                      |            |                                              | ...        |
                     #                      |            |                                              +------------+
                     #                      +------------+
                     # Legend:
                     #   $Reference --> Target 
                     #          Key ==> Value
                     #}
                     Undefines             => \%AllUndefines,        #{ set of #undef statements
                     #
                     # +-----------------+
                     # | %AllUndefines   |
                     # +-----------------+
                     # | MacroName1 => 1 |
                     # | ...             |
                     #
                     #}
                     Defines               => \%AllDefines,          #{ set of #define statements which are no macros
                     #
                     # +------------------+
                     # | %AllDefines      |
                     # +------------------+
                     # | Define1 => 1     | <- constant defined as 1 or without value   #define Define1
                     # | Define2 => 42    | <- constant defined as value 42             #define Define2 42 
                     # | Define3 => "foo" | <- constant defined as string "foo"         #define Define3 "foo"
                     # | ...              |
                     #
                     #}
                     MacroDefinitions      => \%AllMacros,           #{ set of found macro definitions
                     #
                     # +-------------+
                     # | %AllMacros  |
                     # +-------------+   +---------------------------+
                     # | MacroName1 ===> |         %Macro            |
                     # | ...         |   +---------------------------+
                     #                   | MacroName ==> $MacroName  | <-- string: name of macro
                     #                   | Definition => $Definition | <-- string: complete definition 
                     #                   |                           |   +------------+
                     #                   | Arguments ==================> | @Arguments | 
                     #                   |                           |   +------------+
                     #                   |                           |   |$Argument1  | <-- string "TYPE VALUE" (e.g. "struct foo** A)
                     #                   +---------------------------+   | ...        |
                     #}
                     Hierarchy             => $Hierarchy,            #{ pure source code converted into hierarchy by curly braces { }
                     #
                     # Each set of curly braces is replaced by a sub hierarchy array
                     #
                     # +----------------+
                     # | @$Hierarchy    |
                     # +----------------+
                     # | $Source1       | <-- string: source without curly braces
                     # |                |   +----------------+
                     # | $SubHierarchy ---> | @$SubHierarchy |
                     # |                |   +----------------+
                     # |                |   | $Source1       | <-- string: source without curly braces
                     # |                |   | $SubHierarchy ---> ...
                     # |                |   | $Source2       | <-- string: source without curly braces
                     # |                |   | $SubHierarchy ---> ...
                     # |                |   | ...            |   
                     # |                |   +----------------+
                     # | $Source2       | <-- string: source without curly braces
                     # | $SubHierarchy ---> ...
                     # | ...            |
                     # +----------------+
                     #}
                     Includes              => \@AllIncludes,         #{ include directives
                     #
                     # +----------------+
                     # | @Includes      |
                     # +----------------+
                     # |                |   +-----------------------+
                     # | $Include1 -------> |       %Include        |
                     # |                |   +-----------------------+
                     # |                |   | FilePath => $FilePath | <-- string: relative or absolute file path
                     # |                |   | FileName => $FileName | <-- string: filename extracted from FilePath
                     # |                |   +-----------------------+
                     # | ...            |
                     # +----------------+
                     #}
                     Enums                 => \%AllEnums             #{ list of found enumeration definitions
                     #
                     # +------------+
                     # | %Allenums  |
                     # +------------+   +---------------------+
                     # | EnumName1 ===> |         %Enum       |
                     # | ...        |   +---------------------+
                     #                  | Name ==> $EnumName  | <-- string: name of enumeration
                     #                  |                     |   +--------+
                     #                  | Items ================> | @Items | 
                     #                  |                     |   +--------+
                     #                  |                     |   | $Item1 | <-- string: name of single enumeration item
                     #                  +---------------------+   | ...    |
                     #}
                   ); #}
  if (1==0) { # print debug info (example how to access data)
    print "\n";
    print "SourceInfo($PWD/$FileName):\n";
    
    print "  FunctionDefinitions:\n";
    foreach my $Functionname (sort keys %{$SourceInfo{FunctionDefinitions}}) {
      my $FunctionInfo = $SourceInfo{FunctionDefinitions}->{$Functionname};
      print "    ".$FunctionInfo->{Return}.' '.
                   $Functionname.
                   '('.join(', ', @{ $FunctionInfo->{Arguments} }).')'.
                   ";\n";
      if (1==0) { # print Function Source
        my $Source = $FunctionInfo->{Source};
        $Source =~ s/\n/\n        /g;
        print "      { $Source\n";
        print "      }\n";
      }
      if (1==1) { # print Function Calls from this function
        print "    Calls:\n      ";
        print join( "\n      ", 
                    map { $_->{Name}.
                          '('.
                          join(', ', @{ $_->{Arguments} } ).
                          ')'; 
                    } 
                    @{ $FunctionInfo->{Calls} }
                  )."\n";
      }
    }
    
    print "  FunctionDeclarations:\n";
    foreach my $Functionname (sort keys %{$SourceInfo{FunctionDeclarations}}) {
      my $FunctionInfo = $SourceInfo{FunctionDeclarations}->{$Functionname};
      print "    ".$FunctionInfo->{Return}.' '.
                   $Functionname.
                   '('.join(', ', @{ $FunctionInfo->{Arguments} }).')'.
                   ";\n";
      if (1==0) { # print Function Source
        my $Source = $FunctionInfo->{Source};
        $Source =~ s/\n/\n        /g;
        print "      { $Source\n";
        print "      }\n";
      }
      if (1==1) { # print Function Calls from this function
        print "    Calls:\n      ";
        print join( "\n      ", 
                    map { $_->{Name}.
                          '('.
                          join(', ', @{ $_->{Arguments} } ).
                          ')'; 
                    } 
                    @{ $FunctionInfo->{Calls} }
                  )."\n";
      }
    }
    
    print "  MacroDefinitions:\n    ".
          join( "\n    ", 
                keys %{ $SourceInfo_Header{MacroDefinitions} }
              )."\n";
    print "  Defines:\n    ".
          join( "\n    ", 
                map { $_.'="'.$SourceInfo_Header{Defines}->{$_}.'"'; } 
                keys %{ $SourceInfo_Header{Defines} }
              )."\n";

    print "\n";
  }
  
  return %SourceInfo;
}
sub parseSourceSettings {         # scans given source for special setting lines
  my $Source     = shift; # source code to scan (may have several KEY=SETTING lines)
  my $SourcePath = shift; # filepath to corresponding file (used for debug output only) 

  my %Settings = ( #{ ) default settings
                   Rank_HighLevel => 500,
                   Rank_LowLevel  => 450
                 ); #}
  
  foreach my $Key (keys %Settings) {
    my $KEY = uc($Key);
    $Source =~ /$KEY\s*=\s*(\S+)/;
    if ($1) { $Settings{$Key} = $1;  }
  }
  
  return \%Settings;
}
sub extractLine {                 # extracts line which contains given position in given string
  my $String   = shift;
  my $Position = shift;
  
  my $LineStart = rindex($String, "\n", $Position);
  my $LineEnd   =  index($String, "\n", $Position);

  if ($LineEnd   == -1) { $LineEnd   = length($String); }
  
  $LineStart++;
  $LineEnd--;
  my $PositionInLine = $Position - $LineStart;
  my $Line = substr($String, $LineStart, $LineEnd - $LineStart + 1);
  
  
  return ( $Line, $PositionInLine);
}
sub readPureSource {              # reads in source file as pure code (no comments)
  my $SourceFile = shift;
  my $Source     = shift || readFile($SourceFile);
  
  my $Pos = index($Source, "/*");
  while ($Pos > -1) { # remove all multiline comments
      my $Pos2 = index($Source, "*/", $Pos) + 2;
      substr($Source, $Pos, $Pos2 - $Pos) = undef;
      $Pos = index($Source, "/*");
  }
  $Source =~ s/\/\/.*\n/\n/g;  # remove all single line comments
  $Source =~ s/\n\s+/\n/g;     # remove whites spaces at begin of each line
  $Source =~ s/\n#\s+/\n#/g;   # remove whites spaces after # at begin of each line
  $Source =~ s/\n#.+/\n/g;     # remove pragma lines (#ifdef, #endif, ...)
  $Source =~ s/ +\n//g;        # remove whites spaces at end of each line
  $Source =~ s/ +/ /g;         # replace >1 spaces -> 1 space
  $Source =~ s/\r/\n/g;        # remove carriage returns
  $Source =~ s/\n+/\n/g;       # replace any amount of linefeeds by single ones
  $Source =~ s/\t/ /g;         # replace tabs -> space
  $Source =~ s/;/;\n/g;        # ensure that every Semicolon starts a new line
  $Source =~ s/\n\n/\n/g;      # remove doubled newlines
  if (0) { print "readPureSource($PWD/$SourceFile):\n$Source\n"; }
  
  return $Source;
}
sub strip {                       # removes invisible characters at begin + end of given string
  my $Line = shift || "";

  my %IsWhiteChar = ( ' ' => 1, "\n" => 1, "\r" => 1, "\t" => 1 ); 

  $Line =~ s/^\s+//;
  $Line =~ s/\s+$//;
  #$Line =~ s/(?:^ +)||(?: +$)//g;
  #$Line =~ s/^\ *([A-Z,a-z,0-9]*)\ *$/\1/g;
  #$Line =~ s/^\s+//;
  #$Line  =~ s/\s+$//;
  return $Line;
  
  my $lIndex = 0;
  while ( $IsWhiteChar{ substr($Line, $lIndex, 1) } ) { $lIndex++; }

  my $rIndex = length($Line);
  while ( $IsWhiteChar{ substr($Line, $rIndex, 1) } ) { $rIndex--; }
  
  return substr($Line, $lIndex, $rIndex - $lIndex + 1);

}
sub update_InterfaceFiles {       # update entries in interface files
  my $DeviceName         = shift;
  my $DriverDeclarations = shift; # hash-ref: return from collect_DriverDeclarations(ttc_<device>.h)
  my $Architectures      = shift; # hash-ref: return from collect_Architectures($DeviceName)
  my $devicename         = lc($DeviceName);
  
  my @InterfaceTemplates = grep { # gather interface templates
    ( my $TargetFile,
      my $IsHighLevelDriver,
      my $IsLowLevelDriver,
      my $IsInterface,
      my $IsInstallScript,
      my $IsInstallScriptLow,
      my $IsInstallScriptHigh
    ) = identifyTemplate($_, $DeviceName);
    
    $IsInterface;
  } @AllTemplateFiles;

  foreach my $InterfaceTemplate (@InterfaceTemplates) {
     ( my $TargetFile,
       my $IsHighLevelDriver,
       my $IsLowLevelDriver,
       my $IsInterface,
       my $IsInstallScript,
       my $IsInstallScriptLow,
       my $IsInstallScriptHigh
     ) = identifyTemplate($InterfaceTemplate, $DeviceName);
    
     if ($TargetFile) {
       my %SourceInfo = parseSource($TargetFile);
       if (substr($InterfaceTemplate, -2) eq '.c') { # updating interface source
         if ($Verbose) { print "updating interface source file '$TargetFile'..\n"; }
       }
       if (substr($InterfaceTemplate, -2) eq '.h') { # updating interface header
         if ($Verbose) { print "updating interface header file '$TargetFile'..\n"; }
         
         my @Includes_Target = @{ $SourceInfo{Includes}; };
         my %Includes_Target = map { $_->{FileName}, $_ } @Includes_Target;
         foreach my $Architecture (keys %$Architectures) { # check for missing low-level driver includes
           my $IncludeFile = "${devicename}_${Architecture}.h";
           my $IncludePath = "../${devicename}/$IncludeFile";
           unless ($Includes_Target{$IncludeFile}) { # include not found: add it
             print "  creating missing include '".$IncludePath."' (".$IncludeFile.")\n";
             unless ($SourceInfo{Source}) { $SourceInfo{Source} = readFile($SourceInfo{FileName}); }
             my $SettingsRef = parseSourceSettings($SourceInfo{Source}, $SourceInfo{FileName});
             my $Source  = <<"END_OF_STRING"; #{
#ifdef EXTENSION_${devicename}_${Architecture}
#  include "$IncludePath"  // low-level driver for ${devicename} devices on ${Architecture} architecture
#endif
END_OF_STRING
#}
             $SourceInfo{Source} = insertIntoString($TargetFile, $SourceInfo{Source}, '//InsertArchitectureIncludes', $Source);
           }
         }
       }
       
       foreach my $DriverFunctionName (sort keys %$DriverDeclarations) { # create default implementations
         my $Pos = index($DriverFunctionName, $devicename) + length($devicename) + 1; 
         my $FunctionName = substr($DriverFunctionName, $Pos);
         my $DriverInterfaceFunctionName = "ttc_${devicename}_interface_${FunctionName}"; # name of corresponding function in interface
         my $DriverLowLevelFunctionName  = "ttc_driver_${devicename}_${FunctionName}";    # name of corresponding function in low-level driver
         my $DriverDeclaration           = $DriverDeclarations->{$DriverFunctionName};
         
         if (substr($InterfaceTemplate, -2) eq '.c') { # updating interface source
           unless ($SourceInfo{FunctionDefinitions}->{$DriverInterfaceFunctionName}) {
             print "  missing function-definition: '$DriverInterfaceFunctionName'\n";
             unless ($SourceInfo{Source}) { $SourceInfo{Source} = readFile($SourceInfo{FileName}); }
             (my $Comment, my $Prototype, my $Code) = compileFunctionDefinition($DeviceName, 
                                                                                $DriverInterfaceFunctionName, 
                                                                                $DriverDeclaration->{Return}, 
                                                                                $DriverDeclaration->{Arguments}
                                                                               );
             my $Source  = <<"END_OF_STRING"; #{
#ifndef ${DriverLowLevelFunctionName}  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ${DriverLowLevelFunctionName}() (using default implementation)!

$Code
#endif
END_OF_STRING
#}
             
             $SourceInfo{Source} = insertIntoString($TargetFile, $SourceInfo{Source}, '//InsertFunctionDefinitions', $Source);
           }
         }
         if (substr($InterfaceTemplate, -2) eq '.h') { # updating interface header
           unless ($SourceInfo{FunctionDeclarations}->{$DriverInterfaceFunctionName}) {
             print "  inserting missing function-declaration: '$DriverInterfaceFunctionName'\n";
             unless ($SourceInfo{Source}) { $SourceInfo{Source} = readFile($SourceInfo{FileName}); }     
  
             my @ArgNames = map { /\s([a-zA-Z0-9_]+)\s*$/; $1 } @{ $DriverDeclaration->{Arguments} }; # extract Argumentnames
             my $ArgNames = join(", ", @ArgNames);
             
             (my $Comment, my $Prototype, my $Code) = compileFunctionDefinition($DeviceName, $DriverInterfaceFunctionName, $DriverDeclaration->{Return}, $DriverDeclaration->{Arguments});
             my $Source  = <<"END_OF_STRING"; #{
$Prototype; //{  macro _driver_${devicename}_interface_${FunctionName}(${ArgNames})
#ifdef ${DriverLowLevelFunctionName}
// enable following line to forward interface function as a macro definition
#  define _driver_${devicename}_${FunctionName} ${DriverLowLevelFunctionName}
#else
#  define _driver_${devicename}_${FunctionName} $DriverInterfaceFunctionName
#endif
//}
END_OF_STRING
#}
             $SourceInfo{Source} = insertIntoString($TargetFile, $SourceInfo{Source}, '//InsertDriverPrototypes', $Source);
           }
         }
       }
       if ($SourceInfo{Source}) { # source has been modified: write back to disc
         writeFile($SourceInfo{FileName}, $SourceInfo{Source}, '.orig');
       }
     }
  }
}
sub update_LowLevelDrivers {      # update all architecture dependent drivers
  my $DeviceName         = shift;
  my $DriverDeclarations = shift; # hash-ref: return from collect_DriverDeclarations(ttc_<device>.h)
  my $Architectures      = shift; # hash-ref: return from collect_Architectures($DeviceName)
  my $devicename         = lc($DeviceName);
  my $DEVICE_NAME         = uc($DeviceName);

  foreach my $Name (keys %$Architectures) {
    my $Architecture = $Architectures->{$Name};
    if ($Architecture) {
      my $ArchitectureName = $Architecture->{Name};
      my $ARCHITECTURENAME = uc($ArchitectureName);
      my $architecturename = lc($ArchitectureName);
  
      if ($Verbose) { print "updating low-level driver for architecture '$ArchitectureName'\n"; }
  
      my %SourceInfo_Header = parseSource($Architecture->{File_Header}); #{ update low-level header file
      
      foreach my $DriverFunctionName ( sort keys %$DriverDeclarations ) {
        my $DriverFunction = $DriverDeclarations->{$DriverFunctionName};
        my $Pos = index($DriverFunctionName, $devicename) + length($devicename) + 1; 
        my $FunctionName = substr($DriverFunctionName, $Pos);
        my $LowLevelFunctionName = "${devicename}_${architecturename}_${FunctionName}";
        my $DriverMacroName = "ttc_driver_${DeviceName}_${FunctionName}";
  
        unless ( $SourceInfo_Header{FunctionDeclarations}->{$LowLevelFunctionName} ||  # function is missing
                 $SourceInfo_Header{Undefines}->{$DriverMacroName}                 ||  # corresponding macro has not been declared as undefined
                 $SourceInfo_Header{Defines}->{$DriverMacroName}                   ||  # corresponding macro has not been defined to something else
                 $SourceInfo_Header{MacroDefinitions}->{$LowLevelFunctionName}     ||  # function not defined as macro
                 $SourceInfo_Header{MacroDefinitions}->{$DriverMacroName}              # macro not defined at all
               ) { # insert function declaration
          (my $Comment, my $Prototype, my $Code) = compileFunctionDefinition( $DeviceName, 
                                                                              $LowLevelFunctionName, 
                                                                              $DriverFunction->{Return}, 
                                                                              $DriverFunction->{Arguments},
                                                                              $DriverFunction->{Comment}
                                                                            );
          if ($Verbose) { print "  inserting low-level function declaration \"$Prototype;\"\n"; }
          my $ul_FilePath = $SourceInfo_Header{FileName};
          unless ($SourceInfo_Header{Source}) { $SourceInfo_Header{Source} = readFile($ul_FilePath); }
          my $InsertText = <<"END_OF_STRING"; #{

$Comment$Prototype;

END_OF_STRING
#}
          $SourceInfo_Header{Source} = insertIntoString($ul_FilePath, $SourceInfo_Header{Source}, "//InsertFunctionDeclarations", $InsertText);
        }
        
        unless ($SourceInfo_Header{MacroDefinitions}->{$DriverMacroName} || # macro with this name is missing 
                $SourceInfo_Header{Defines}->{$DriverMacroName}          || # macro has not been defined as something else
                $SourceInfo_Header{Undefines}->{$DriverMacroName}           # macro has not been declared as undefined
               ) {
          my $ul_FilePath = $SourceInfo_Header{FileName};
          my $InsertText;
          if (0) { # DEPRECATED: Define low-level function as macro with arguments
          my @ArgNames = map { /\s([a-zA-Z0-9_]+)\s*$/; $1 } @{ $DriverFunction->{Arguments} }; # extract Argumentnames
          my $ArgNames = join(", ", @ArgNames);
          my $ArgNameString = join(', ', @ArgNames);
  
          if ($Verbose>=0) { print "  inserting missing driver macro-definition $DriverMacroName($ArgNameString)\n"; }
          $InsertText = <<"END_OF_STRING";
#define $DriverMacroName($ArgNameString) ${DeviceName}_${architecturename}_${FunctionName}($ArgNameString)
END_OF_STRING
          }
          else {  # Define low-level function as constant replacement without arguments (allows function pointer to low-level function)
          if ($Verbose>=0) { print "  inserting missing driver macro-definition $DriverMacroName\n"; }
          $InsertText = <<"END_OF_STRING";
#define $DriverMacroName ${DeviceName}_${architecturename}_${FunctionName}
END_OF_STRING
          }
          unless ($SourceInfo_Header{Source}) { $SourceInfo_Header{Source} = readFile($ul_FilePath); }
          $SourceInfo_Header{Source} = insertIntoString($ul_FilePath, $SourceInfo_Header{Source}, "//InsertLowLevelMacros", $InsertText);
        }
      }
      
      if ($SourceInfo_Header{Source}) { # source changed: write back to disc
        writeFile($SourceInfo_Header{FileName}, $SourceInfo_Header{Source});
        $SourceInfo_Header{Source} = undef;
      }
      #}
      my %SourceInfo_Source = parseSource($Architecture->{File_Source}); #{ update low-level source file
      foreach my $DriverFunctionName ( sort keys %$DriverDeclarations ) {
        my $DriverFunction = $DriverDeclarations->{$DriverFunctionName};
        my $Pos = index($DriverFunctionName, $devicename) + length($devicename) + 1; 
        my $FunctionName = substr($DriverFunctionName, $Pos);
        my $LowLevelFunctionName = "${devicename}_${architecturename}_${FunctionName}";
        my $DriverMacroName = "ttc_driver_${DeviceName}_${FunctionName}";
  
        unless ( $SourceInfo_Source{FunctionDefinitions}->{$LowLevelFunctionName} || # function definition is missing
                 $SourceInfo_Header{Undefines}->{$DriverMacroName}                || # corresponding macro has not been declared as undefined in header
                 $SourceInfo_Header{Defines}->{$DriverMacroName}                  || # corresponding macro has not been defined as something else
                 $SourceInfo_Header{MacroDefinitions}->{$LowLevelFunctionName}    || # function not defined as macro
                 $SourceInfo_Header{MacroDefinitions}->{$DriverMacroName}            # macro not defined at all
               ) {
          my $ul2_FilePath = $SourceInfo_Source{FileName};
          unless ($SourceInfo_Source{Source}) { $SourceInfo_Source{Source} = readFile($ul2_FilePath); }
          
          (my $Comment, my $Prototype, my $Code) = compileFunctionDefinition($DeviceName, $LowLevelFunctionName, $DriverFunction->{Return}, $DriverFunction->{Arguments});
          if ($Verbose) { print "  inserting low-level function definition \"$Prototype { .. } into $ul2_FilePath\"\n"; }
          $SourceInfo_Source{Source} = insertIntoString($ul2_FilePath, $SourceInfo_Source{Source}, "//InsertFunctionDefinitions", $Code);
        }
      }
      if ($SourceInfo_Source{Source}) { # source changed: write back to disc
        writeFile($SourceInfo_Source{FileName}, $SourceInfo_Source{Source});
        $SourceInfo_Source{Source} = undef;
      }
      #}
      if (1) { # insert example of first device definition into types header
        my $DeviceDefinition = "TTC_${DEVICE_NAME}1";
        my $TypesHeader = readFile($Architecture->{File_Types});
        my $TypesHeader2 = insertIntoString( $Architecture->{File_Types}, 
                                             $TypesHeader,
                                             '//InsertDefines',
                                             "#ifndef $DeviceDefinition   // device not defined in makefile\n".
                                             "#  define $DeviceDefinition    E_ttc_${devicename}_architecture_${architecturename}   // example device definition for current architecture (Disable line if not desired!)\n".
                                             "#endif\n",
                                             IF_NOT_CONTAINS => "$DeviceDefinition",  # will not insert if such a define already exists 
                                             PREVIOUS_LINE   => 1
                                           );
        if ($TypesHeader ne $TypesHeader2) { 
          print "  inserted '#define $DeviceDefinition' into $Architecture->{File_Types}\n";
          writeFile($Architecture->{File_Types}, $TypesHeader2); 
        }
      }
    }
  }
}
sub update_HighLevelDriver {      # update high-level driver sources
  my $DeviceName         = shift;
  my $DriverDeclarations = shift; # hash-ref: return from collect_DriverDeclarations(ttc_<device>.h)
  my $Architectures      = shift; # hash-ref: return from collect_Architectures($DeviceName)
  my $devicename         = lc($DeviceName);

  foreach my $File (@AllTemplateFiles) {
    ( my $TargetFile,
      my $IsHighLevelDriver,
      my $IsLowLevelDriver,
      my $IsInterface,
      my $IsInstallScript,
      my $IsInstallScriptLow,
      my $IsInstallScriptHigh
    ) = identifyTemplate($File, $DeviceName, $ArchitectureName);

    my $Source;
    if ( $IsHighLevelDriver && (index($TargetFile, 'types.h') > -1) ) { # ttc_<device>_types.h
      my %SourceInfo_Header      = parseSource($TargetFile); #{ update types header
      my %Enum_Architecture      = %{ $SourceInfo_Header{Enums}->{"e_ttc_${devicename}_architecture"} };
      my %SupportedArchitectures = map { $_ => 1; } @{ $Enum_Architecture{Items} };
      my %IncludeFiles           = map { $_->{FileName} => $_; } @{ $SourceInfo_Header{Includes} };
      map {
        my $ArchitectureItem = "E_ttc_${devicename}_architecture_${_}";
        my $architecture = lc($_);
        unless ($SupportedArchitectures{$ArchitectureItem}) { # architecture missing in enum: add it to enum
          if ($verbose) { print "  adding to enum $Enum_Architecture{Name}:  '$ArchitectureItem'\n"; }
          unless ($Source) { $Source = readFile($TargetFile); }   
          $Source = insertIntoString($TargetFile, $Source, '//InsertArchitectureEnum', "  $ArchitectureItem, // automatically added by $0\n");
          if (index($Source, '//InsertArchitectureStructs') > -1) { # union u_ttc_<device>_architecture was added to ttc_device_types.h in revision 30
            my $SettingsRef = parseSourceSettings($Source, $TargetFile);
            my $ArchitectureStruct = <<"END_OF_STRUCT";
#ifdef EXTENSION_${devicename}_${architecture}
    t_${devicename}_${architecture}_config ${architecture};  // automatically added by $0
#endif
END_OF_STRUCT
            $Source = insertIntoString($TargetFile, $Source, '//InsertArchitectureStructs', $ArchitectureStruct);
          }
        }
        my $IncludeBaseName = "${devicename}_${architecture}"; # gpio_stm32f1xx_types.h
        my $HeaderIncludeFile = "${IncludeBaseName}_types.h";
        unless ($IncludeFiles{$HeaderIncludeFile}) {  # architecture missing in list of includes: add it 
          unless ($Source) { $Source = readFile($TargetFile); }
          my $SettingsRef = parseSourceSettings($Source, $TargetFile);
          my $TypesInclude = <<"END_OF_STRING";
#ifdef EXTENSION_${IncludeBaseName}
#  include "${devicename}/$HeaderIncludeFile" // automatically added by $0
#endif
END_OF_STRING
          if ($Verbose) { print "inserting types-include '${devicename}/$HeaderIncludeFile' into $TargetFile\n"; }
          $Source = insertIntoString($TargetFile, $Source, '//InsertTypesIncludes', "$TypesInclude");
        }
      } keys %$Architectures;
      #}
    }
    elsif ( $IsHighLevelDriver && (index($TargetFile, '.h') > -1) ) { # ttc_<device>.h
      unless ($Source) { $Source = readFile($TargetFile); }   
      if (%$Architectures) { # at least one architecture supported: add architecture support
        my $InterfaceInclude = "interfaces/ttc_${DeviceName}_interface.h";
        unless ($IncludeFiles{$InterfaceInclude}) {
          if (1 || $Verbose) { print "  inserting interface include '$InterfaceInclude'\n"; }
          $Source = insertIntoString($TargetFile, $Source, '//InsertIncludes', "#include \"$InterfaceInclude\" // multi architecture support\n");
        }
      }
      else {
          my $TypesInclude = "ttc_${DeviceName}_types.h";
          if (1 || $Verbose) { print "  inserting generic types include '$TypesInclude'\n"; }
          $Source = insertIntoString($TargetFile, $Source, '//InsertIncludes', "#include \"$TypesInclude\" // architecture independent $devicename datatypes\n");      }
    }
    if ($Source) {  # source has been modified: write back to disc
      writeFile($TargetFile, $Source, '.orig'); 
    }
  }
}
sub readFile {                    # read in content of given file
  my $rf_FilePath = shift;
  
  affectFile($rf_FilePath, 'READ');
  my $Content;
  my $Error;
  open(IN, "<$rf_FilePath") or $Error = "$0 - ERROR: Cannot read from file '$rf_FilePath' ($!)";
  dieOnError($Error);
  while (<IN>) { $Content .= $_; }
  close(IN);
  
  $Content =~ s/\\\n//g; # concatenate multiline strings
  
  return $Content;
}
sub writeFile {                   # write content to given file
  my $wf_FilePath  = shift;
  my $Content      = shift;
  my $BackUpSuffix = shift; # if given and original file exists, a backup file is being created using this suffix
  
  if (-e $wf_FilePath) { affectFile($wf_FilePath, 'CHANGED'); }
  else                 { affectFile($wf_FilePath, 'CREATED'); }

  if (1) { # enable/ disable changing files  
    if ($Verbose>1) { print "writing to disc: $wf_FilePath\n"; }
    if ($BackUpSuffix) {
      if (-e $wf_FilePath) {
        system("mv \"${wf_FilePath}\" \"${wf_FilePath}$BackUpSuffix\"");
      }
    }
  
    open(OUT, ">$wf_FilePath") or die ("$0 - ERROR: Cannot write to file '$wf_FilePath' ($!)");
    print OUT $Content;
    close(OUT);
  }
  
  return $Content;
}
sub getCallerStack {              # display who has called current function
  my $Level = shift() + 0 || 1;
  
  my @Stack;
  my ($package, $filename, $line, $subroutine, $hasargs, $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash);
  do {
     ($package, $filename, $line, $subroutine, $hasargs,
      $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
      = caller($Level++);
      if ($subroutine) { push @Stack, "$subroutine() called from ${filename}:$line"; }
  } while ($filename);
  ($package, $filename, $line, $subroutine, $hasargs,
   $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
   = caller(0);
  
   push @Stack, " called from ${filename}:$line";
   
   return @Stack;
}
sub dieOnError {                  # dies if error given + prints caller stack
  my @ErrorMessages = grep { $_; } @_;
  unless (@ErrorMessages) { return; } # no error no die
  
  my $PWD = `pwd`; chomp($PWD);
  push @ErrorMessages, "PWD='$PWD'";
  push @ErrorMessages, getCallerStack();

  print stderr "\n\n\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n\n".
               "$0 - ERROR in $subroutine(): ".join("\n    ", @ErrorMessages)."\n";
  exit 10;
}
sub getFileNames {                # does ls on given wildcard and returns list of filenames
  my @Paths = @_;
  
  my $Temp = "tmp.$MySelf";
  my @Files = map {
    my $Files = `ls 2>$Temp $_`;
    my $Error = `cat $Temp; rm $Temp`;
    #dieOnError($Error);
    my @NewFiles = split("\n", $Files);
    @NewFiles;
  } @Paths;
  
  if (wantarray()) { return @Files; }
  return join("\n", @Files);
}
