#ifndef <device>_common_h
#define <device>_common_h

/** <device>_common.h ****************************************************{
 *
 *                          The ToolChain
 *                     
 *  Common source code available to <device> low-level drivers of all architectures.
 *  The functions provided here are common in two ways:
 *  1) They can be called from high-level driver ttc_<device>.c and from current low-level driver <device>_*.c.
 *  2) All common_<device>_*() functions can call low-level driver functions as _driver_<device>_*().
 * 
 *  Created from template ttc-lib/templates/device_common.h revision 15 at <DATE>
 *
 *  Authors: <AUTHOR>
 *
 *  Description of <device>_common (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "<device>_<architecture>.c"
 */
 
#include "../ttc_basic.h"
#include "../ttc_<device>_types.h" // will include <device>_common_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_<device>_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_<device>_foo
 */
 
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions
/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_<device>.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_<device>.h for details.
 *
 * You normally should not need to add functions here. 
 * Global feature functions must be added in ttc_<device>.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl <device> UPDATE".
 */

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //<device>_common_h
