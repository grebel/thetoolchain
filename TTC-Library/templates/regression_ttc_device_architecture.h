#ifndef REGRESSION_<DEVICE>_<ARCHITECTURE>_H
#define REGRESSION_<DEVICE>_<ARCHITECTURE>_H

/** regression_ttc_<DEVICE>_<ARCHITECTURE>.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Implementation test for ttc_<device> using low-level driver 
 *  <device>_<architecture> for <architecture_description>.
 *
 *  Basic flow with enabled multitasking:
 *  (1) regression_ttc_<device>_<architecture>_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by regression_ttc_<device>_<architecture>_prepare() get executed
 *
 *  Basic flow with disabled multitasking:
 *  (1) regression_ttc_<device>_<architecture>_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template <TEMPLATE_FILE> revision 16 at <DATE>
 *
 *  Authors: Gregor Rebel
 *  
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile regression_ttc_<DEVICE>_<ARCHITECTURE>.c only do not belong here! 
#include "../ttc-lib/ttc_basic.h"      // basic datatypes and definitions
#include "../ttc-lib/ttc_task_types.h" // required for task delays

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // regression_<device>_<architecture>_t
    t_u8              Index_<DEVICE>;  // logical index of device to use
    t_ttc_systick_delay  Delay;          // this type of delay is usable in single- and multitasking setup

    // more arguments...
} regression_<device>_<architecture>_data_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this regression
 *
 * - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void regression_ttc_<device>_<architecture>_prepare();


//}Function prototypes

#endif //REGRESSION_<DEVICE>_<ARCHITECTURE>_H
