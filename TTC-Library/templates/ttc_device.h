#ifndef TTC_<DEVICE>_H
#define TTC_<DEVICE>_H
/** { ttc_<device>.h *******************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for <device> devices.
 *  The functions defined in this file provide a hardware independent interface 
 *  for your application.
 *                                          
 *  The basic usage scenario for devices:
 *  1) check:       Assert_<DEVICE>(tc_<device>_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_<device>_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_<device>_init(LogicalIndex);
 *  4) use:         ttc_<device>_XXX(LogicalIndex);
 *
 *  Device Index
 *  Devices are addressed by a logical index with no correlation to the order of physical devices. 
 *  Logical indices start at 1 to simply detect uninitialized values.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level <device> and application.
 *
 *  Created from template <TEMPLATE_FILE> revision 39 at <DATE>
 *
 *  Authors: <AUTHOR>
 *
 */
/** Description of ttc_<device> (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/

#ifndef EXTENSION_ttc_<device>
#error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_<device>.sh
#endif

//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_<device>.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level <device> only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function Declarations ************************************************{
 *
 * The functions declared below provide the main interface for
 * <device> devices on all supported architectures.
 * Check <device>/<device>_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your 
 * activate_project.sh file inside your project folder.
 *
 */
 
/** allocates a new <device> instance and returns pointer to its configuration
 *
 * @return (t_ttc_<device>_config*)  pointer to new configuration. Set all Init fields before calling ttc_<device>_init() on it.
 */
t_ttc_<device>_config* ttc_<device>_create();

/** Prepares <device> Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_<device>_prepare();
void _driver_<device>_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_<device>_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    logical index of <DEVICE> device. Each logical device <n> is defined via TTC_<DEVICE><n>* constants in compile_options.h and extensions.active/makefile
 * @return                configuration of indexed device
 */
t_ttc_<device>_config* ttc_<device>_get_configuration(t_u8 LogicalIndex);

/** initialize indexed device for use
 *
 * @param LogicalIndex    logical index of <DEVICE> device. Each logical device <n> is defined via TTC_<DEVICE><n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: <device> device has been initialized successfully; != 0: error-code
 */
e_ttc_<device>_errorcode ttc_<device>_init(t_u8 LogicalIndex);

/** shutdown indexed device
 *
 * @param LogicalIndex    logical index of <DEVICE> device. Each logical device <n> is defined via TTC_<DEVICE><n>* constants in compile_options.h and extensions.active/makefile
 */
void ttc_<device>_deinit(t_u8 LogicalIndex);

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_<device>_deinit() if device has been initialized.  
 *
 * @param LogicalIndex  logical index of <DEVICE> device. Each logical device <n> is defined via TTC_<DEVICE><n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: default values have been loaded successfully for indexed <device> device; != 0: error-code
 */
e_ttc_<device>_errorcode  ttc_<device>_load_defaults(t_u8 LogicalIndex);

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    logical index of <DEVICE> device. Each logical device <n> is defined via TTC_<DEVICE><n>* constants in compile_options.h and extensions.active/makefile

 */
void ttc_<device>_reset(t_u8 LogicalIndex);

/** Reinitializes all initialized devices after changed systemcloc profile.
 *
 * Note: This function is called automatically from _ttc_sysclock_call_update_functions()
 */
void ttc_<device>_sysclock_changed();

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_<device>_ are passed to interfaces/ttc_<device>_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl <device> UPDATE
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_<device>_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_<device>_features.
 *
 * @param Config        Configuration of <device> device
 * @return              Fields of *Config have been adjusted if necessary
 */
void _driver_<device>_configuration_check(t_ttc_<device>_config* Config);

/** shutdown single <DEVICE> unit device
 * @param Config        Configuration of <device> device
 * @return              == 0: <DEVICE> has been shutdown successfully; != 0: error-code
 */
e_ttc_<device>_errorcode _driver_<device>_deinit(t_ttc_<device>_config* Config);

/** initializes single <DEVICE> unit for operation
 * @param Config        Configuration of <device> device
 * @return              == 0: <DEVICE> has been initialized successfully; != 0: error-code
 */
e_ttc_<device>_errorcode _driver_<device>_init(t_ttc_<device>_config* Config);

/** loads configuration of indexed <DEVICE> unit with default values
 * @param Config        Configuration of <device> device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_<device>_errorcode _driver_<device>_load_defaults(t_ttc_<device>_config* Config);

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of <device> device
 */
void _driver_<device>_reset(t_ttc_<device>_config* Config);
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_<DEVICE>_H
