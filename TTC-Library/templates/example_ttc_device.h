#ifndef EXAMPLE_<DEVICE>_H
#define EXAMPLE_<DEVICE>_H

/** example_ttc_<DEVICE>.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for <architecture_description>
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_<device>_<architecture>_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started) 
 *  (2) Scheduler is started.
 *  (3) Tasks being created by example_ttc_<device>_<architecture>_prepare() get executed.
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_<device>_<architecture>_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) Every registered task function is called one after another in an endless loop
 *      from ttc_task_start_scheduler().
 *
 *  Created from template <TEMPLATE_FILE> revision 19 at <DATE>
 *
 *  Authors: <AUTHOR>
 *
 *  
 *  Description of example_ttc_<DEVICE>
 *
 *  ToDo: Document this example!
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_<DEVICE>.c only do not belong here! 
// This is why header files typically include *_types.h files only (avoids nasty include loops).

#include "../ttc-lib/ttc_basic_types.h"    // basic datatypes for current microcontroller architecture
#include "../ttc-lib/ttc_gpio_types.h"     // allows to store gpio pins in structures 
#include "../ttc-lib/ttc_systick_types.h"  // allows to store task delay data in structures
#include "../ttc-lib/ttc_<device>_types.h" // datatypes of <device> devices

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_<device>_t
    t_ttc_<device>_config* Config_<device>; // pointer to configuration of <device> device to use
    t_ttc_systick_delay    Delay;           // this type of delay is usable in single- and multitasking setup

    // more arguments...
} t_example_<device>_data;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_<device>_prepare();


//}Function prototypes

#endif //EXAMPLE_<DEVICE>_H
