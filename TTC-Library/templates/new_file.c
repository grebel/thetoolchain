/** { <FILE>.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  Created from template ttc-lib/templates/new_file.c revision 12 at <DATE>
 *
 *  Authors: Gregor Rebel
}*/

/** { Includes ********************************************************
 *
 * Put all includes here that are required to compile this source code.
 */

//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Private Function Declarations ***********************************
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc(); 
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** { Global Variables ************************************************
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** { Function Definitions ********************************************
 */

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
