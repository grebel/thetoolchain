/** regression_ttc_<device>_<architecture>.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Implementation test for ttc_<device> using low-level driver 
 *  <device>_<architecture> for <architecture_description>.
 *
 *  Basic flow with enabled multitasking:
 *  (1) regression_ttc_<device>_<architecture>_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by regression_ttc_<device>_<architecture>_prepare() get executed
 *
 *  Basic flow with disabled multitasking:
 *  (1) regression_ttc_<device>_<architecture>_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template <TEMPLATE_FILE> revision 19 at <DATE>
 *
 *  Authors: Gregor Rebel
 *  
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "regression_ttc_<device>_<architecture>.h"
#include "../ttc-lib/ttc_<device>.h" // high-level driver of device to test
#include "../ttc-lib/<device>/<device>_<architecture>.h" // direct access to low-level driver to test
#include "../ttc-lib/ttc_task.h"     // allows to use application in single- and multitasking setup

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this regression
 *
 * This task is created automatically from regression_ttc_<device>_<architecture>_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _regression_<device>_<architecture>_task(void *Argument);

//}Private Function Declarations
//{ Function Definitions *************************************************

void regression_ttc_<device>_<architecture>_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    // get new zeroed memory block for our task to create
    regression_<device>_<architecture>_data_t* Argument = ttc_heap_alloc_zeroed( sizeof(example_<device>_data_t) );
    
    Argument->Index_<DEVICE> = 1; // set logical index of device to use
    
    // get configuration of <device> device
    t_ttc_<device>_config* Config_<DEVICE> = ttc_<device>_get_configuration(Argument->Index_<DEVICE>);
    
    // change configuration before initializing device
    //...
    
    // initializing our <device> device here makes debugging easier (still single-tasking)
    // Logical index is stored inside each device configuration
    ttc_<device>_init(Config_<DEVICE>->LogicalIndex);


    ttc_task_create( _regression_<device>_<architecture>_task,       // function to start as thread
                     "t<DEVICE>_<ARCHITECTURE>",          // thread name (just for debugging)
                     128,                                 // stack size (adjust to amount of local variables of _regression_<device>_<architecture>_task() AND ALL ITS CALLED FUNTIONS!) 
                     Argument,    // passed as argument to _regression_<device>_<architecture>_task()
                     1,                                   // task priority (higher values mean more process time)
                     NULL                                 // can return a handle to created task
                   );
}
void _regression_<device>_<architecture>_task(void *Argument) {
    Assert_<DEVICE>_Writable(Argument, ttc_assert_origin_auto);                  // always check pointer arguments with Assert() before dereferencing them!
    regression_<device>_<architecture>_data_t* Data = ( regression_<device>_<architecture>_data_t* ) Argument; // we're expecting this pointer type

    // get pointer to device configuration (remove if not required)
    t_ttc_<device>_config* Config_<DEVICE> = ttc_<device>_get_configuration(Data->Index_<DEVICE>);

    do { 
        // This loop runs endlessly if scheduler is available.
        // Otherwise it will run only onve and be called periodically from ttc_task_start_scheduler().
  
        if ( ttc_systick_delay_expired( & Data->Delay ) ) { // delay passed: send hello world (zeroed delay always expires)
            ttc_systick_delay_init( & Data->Delay, 1000 ); // set new delay to expire in 1 millisecond = 1000 microseconds
    
          // implement statemachine of this task here...
        }
        ttc_task_yield(); // if scheduler available, give cpu to other processes
    } while (TTC_TASK_SCHEDULER_AVAILABLE);
}

//}Function Definitions
