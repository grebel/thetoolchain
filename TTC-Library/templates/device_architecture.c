/** <device>_<architecture>.c ***************************************{
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_<device>.c.
 *
 *  <architecture_description>
 *    
 *  Created from template <TEMPLATE_FILE> revision 40 at <DATE>
 *
 *  Note: See ttc_<device>.h for description of high-level <device> implementation.
 *  Note: See <device>_<architecture>.h for description of <architecture> <device> implementation.
 * 
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "<device>_<architecture>.h".
 */

#include "<device>_<architecture>.h"
#include "../ttc_heap.h"              // dynamic memory allocation (e.g. malloc() )
#include "../ttc_memory.h"            // basic memory checks
#include "../ttc_task.h"              // stack checking (provides TTC_TASK_RETURN() macro)
#include "<device>_common.h"          // generic functions shared by low-level drivers of all architectures 
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_<device>_features <device>_<architecture>_Features = {
  /** example features
    .MinBitRate       = 4800,  
    .MaxBitRate       = 230400,    
  */
  // set more feature values...
  
  .unused = 0
};

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc(); 
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

e_ttc_<device>_errorcode  <device>_<architecture>_load_defaults(t_ttc_<device>_config* Config) {
    Assert_<DEVICE>_EXTRA_Writable(Config, ttc_assert_origin_auto); // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = E_ttc_<device>_architecture_<architecture>;         // set type of architecture
    Config->Features     = &<device>_<architecture>_Features;  // assign feature set

    if (0) { // enable for memory mapped devices (everything that has a base register)
      switch (Config->PhysicalIndex) { // load address of peripheral base register
      //case 0: Config->BaseRegister = & register_<architecture>_<DEVICE>1; break;
      //case 1: Config->BaseRegister = & register_<architecture>_<DEVICE>2; break;
      default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid physical index: check your makefile and high-level driver implementation!
      }
    }
    
    if (Config->LowLevelConfig == NULL) { // allocate memory for spi specific configuration
      { Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof(t_ttc_<device>_architecture) ); }

      // reset all flags
      ttc_memory_set( &(Config->Flags), 0, sizeof(Config->Flags) );
  
      // set individual feature flags    
      //Config->Flags.Foo = 1;
      // add more flags...
    }
  
    TTC_TASK_RETURN( 0 ); // will perform stack overflow check and return given value
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by 
 * the maintainer of this file.
 */
 
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
