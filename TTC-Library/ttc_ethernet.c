/** { ttc_ethernet.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for ethernet devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Most functions of this driver are defined as macros and being implemented
 *  in interfaces/ttc_ethernet_interface.c or in low-level drivers ethernet/ethernet_*.c.
 *
 *  See corresponding ttc_ethernet.h for documentation of all high-level functions.
 *
 *  Created from template ttc_device.c revision 51 at 20180130 11:09:47 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_ethernet.h".
//
#include "ttc_ethernet.h"
#include "ttc_heap.h"       // dynamic memory allocationand safe arrays  
#include "ttc_task.h"       // disable/ enable multitasking scheduler
#ifdef EXTENSION_ttc_interrupt
    #include "ttc_interrupt.h"  // globally disable/ enable interrupts
#endif

//}Includes

#if TTC_ETHERNET_AMOUNT == 0
    #warning No ETHERNET devices defined, did you forget to activate something? - Define at least TTC_ETHERNET1 as one from e_ttc_ethernet_architecture in your makefile!
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of ethernet devices.
 *
 */


// for each initialized device, a pointer to its generic and architecture specific definitions is stored
A_define( t_ttc_ethernet_config*, ttc_ethernet_configs, TTC_ETHERNET_AMOUNT );

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ********************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_ethernet(t_ttc_ethernet_config* Config)

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * @param LogicalIndex    1.. logical index of ETHERNET device. Each logical device <n> is defined via TTC_ETHERNET<n>* constants in compile_options.h and extensions.active/makefile
 */
void _ttc_ethernet_configuration_check( t_u8 LogicalIndex );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

t_ttc_ethernet_config*   ttc_ethernet_create() {

    t_u8 LogicalIndex = ttc_ethernet_get_max_index();
    t_ttc_ethernet_config* Config = ttc_ethernet_get_configuration( LogicalIndex );

    TTC_TASK_RETURN( Config ); // will perform stack overflow check + return value
}
t_u8                     ttc_ethernet_get_max_index() {
    TTC_TASK_RETURN( TTC_ETHERNET_AMOUNT ); // will perform stack overflow check + return value
}
t_ttc_ethernet_config*   ttc_ethernet_get_configuration( t_u8 LogicalIndex ) {
    Assert_ETHERNET( LogicalIndex > 0, ttc_assert_origin_auto ); // logical index starts at 1
    Assert_ETHERNET( LogicalIndex <= TTC_ETHERNET_AMOUNT, ttc_assert_origin_auto ); // invalid index given (did you configure engnough ttc_ethernet devices in your makefile?)
    t_ttc_ethernet_config* Config = A( ttc_ethernet_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_disable();                      // ensure that we are not interrupted while creating configuration
#endif

        Config = A( ttc_ethernet_configs, LogicalIndex - 1 ); // have to reload with disabled interrupts to avoid racing conditions
        if ( !Config ) {
            Config = A( ttc_ethernet_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ethernet_ste101p_config ) );
            Config->Flags.Initialized = 0;             // make sure that deinit() is not called during load_defaults()
            ttc_ethernet_load_defaults( LogicalIndex );
        }

#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_enable();
#endif
    }

    Assert_ETHERNET_EXTRA_Writable( Config, ttc_assert_origin_auto ); // memory corrupted?
    TTC_TASK_RETURN( Config ); // will perform stack overflow check + return value
}
void                     ttc_ethernet_deinit( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration

    t_ttc_ethernet_config* Config = ttc_ethernet_get_configuration( LogicalIndex );

    if ( Config->Flags.Initialized ) {
        e_ttc_ethernet_errorcode Result = _driver_ethernet_deinit( Config );
        if ( Result == E_ttc_ethernet_errorcode_OK )
        { Config->Flags.Initialized = 0; }
    }
    ttc_task_critical_end(); // other tasks now may access this driver
}
e_ttc_ethernet_errorcode ttc_ethernet_init( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration
#if (TTC_ASSERT_ETHERNET_EXTRA == 1)
    t_u8 InitialCriticalLevel = ttc_interrupt_critical_level();
#endif
    t_ttc_ethernet_config* Config = ttc_ethernet_get_configuration( LogicalIndex );

    // check configuration to meet all limits of current architecture
    _ttc_ethernet_configuration_check( LogicalIndex );

    Config->LastError = _driver_ethernet_init( Config );
    if ( ! Config->LastError ) // == 0
    { Config->Flags.Initialized = 1; }

    Assert_ETHERNET( Config->Architecture != 0, ttc_assert_origin_auto ); // Low-Level driver must set this value!
    Assert_ETHERNET_EXTRA( InitialCriticalLevel == ttc_interrupt_critical_level(), ttc_assert_origin_auto ); // amount of  ttc_task_critical_end() calls does not match. Check driver implementation!
    ttc_task_critical_end(); // other tasks now may access this driver

    TTC_TASK_RETURN( Config->LastError ); // will perform stack overflow check + return value
}
e_ttc_ethernet_errorcode ttc_ethernet_load_defaults( t_u8 LogicalIndex ) {
    t_ttc_ethernet_config* Config = ttc_ethernet_get_configuration( LogicalIndex );

    u_ttc_ethernet_architecture* LowLevelConfig = NULL;
    if ( Config->Flags.Initialized ) {
        ttc_ethernet_deinit( LogicalIndex );
        LowLevelConfig = Config->LowLevelConfig; // rescue pointer to dynamic allocated memory
    }

    ttc_memory_set( Config, 0, sizeof( t_ttc_ethernet_config ) );

    // restore pointer to dynamic allocated memory
    Config->LowLevelConfig = LowLevelConfig;

    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    switch ( LogicalIndex ) { // physical index of ethernet device from static configuration
#ifdef TTC_ETHERNET1
        case  1: Config->PhysicalIndex = TTC_ETHERNET1; break;
#endif
#ifdef TTC_ETHERNET2
        case  2: Config->PhysicalIndex = TTC_ETHERNET2; break;
#endif
#ifdef TTC_ETHERNET3
        case  3: Config->PhysicalIndex = TTC_ETHERNET3; break;
#endif
#ifdef TTC_ETHERNET4
        case  4: Config->PhysicalIndex = TTC_ETHERNET4; break;
#endif
#ifdef TTC_ETHERNET5
        case  5: Config->PhysicalIndex = TTC_ETHERNET5; break;
#endif
#ifdef TTC_ETHERNET6
        case  6: Config->PhysicalIndex = TTC_ETHERNET6; break;
#endif
#ifdef TTC_ETHERNET7
        case  7: Config->PhysicalIndex = TTC_ETHERNET7; break;
#endif
#ifdef TTC_ETHERNET8
        case  8: Config->PhysicalIndex = TTC_ETHERNET8; break;
#endif
#ifdef TTC_ETHERNET9
        case  9: Config->PhysicalIndex = TTC_ETHERNET9; break;
#endif
#ifdef TTC_ETHERNET10
        case 10: Config->PhysicalIndex = TTC_ETHERNET10; break;
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }

    //Insert additional generic default values here ...

    // Let low-level driver initialize remaining fields
    Config->LastError = _driver_ethernet_load_defaults( Config );

    // Check mandatory configuration items
    Assert_ETHERNET_EXTRA( ( Config->Architecture > E_ttc_ethernet_architecture_None ) && ( Config->Architecture < E_ttc_ethernet_architecture_unknown ), ttc_assert_origin_auto ); // architecture not set properly! Low-Level driver must set this field!

    TTC_TASK_RETURN( Config->LastError ); // will perform stack overflow check + return value
}
void                     ttc_ethernet_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)

    A_reset( ttc_ethernet_configs, TTC_ETHERNET_AMOUNT ); // safe arrays are placed in data section and must be initialized manually

    if ( 0 ) // optional: register this device for a sysclock change update
    { ttc_sysclock_register_for_update( ttc_ethernet_sysclock_changed ); }

    _driver_ethernet_prepare();
}
void                     ttc_ethernet_reset( t_u8 LogicalIndex ) {
    t_ttc_ethernet_config* Config = ttc_ethernet_get_configuration( LogicalIndex );

    _driver_ethernet_reset( Config );
}
void                     ttc_ethernet_sysclock_changed() {

    // deinit + reinit all initialized ETHERNET devices
    for ( t_u8 LogicalIndex = 1; LogicalIndex < ttc_ethernet_get_max_index(); LogicalIndex++ ) {
        t_ttc_ethernet_config* Config = ttc_ethernet_get_configuration( LogicalIndex );
        if ( Config->Flags.Initialized ) {
            ttc_ethernet_deinit( LogicalIndex );
            ttc_ethernet_init( LogicalIndex );
        }
    }
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_ethernet(t_u8 LogicalIndex) {  }

void _ttc_ethernet_configuration_check( t_u8 LogicalIndex ) {
    t_ttc_ethernet_config* Config = ttc_ethernet_get_configuration( LogicalIndex );

    // perform some generic plausibility checks
    const t_ttc_ethernet_features* Features = Features;

    /** Example plausibility check

    if (Config->BitRate > Features->MaxBitRate)   Config->BitRate = Features->MaxBitRate;
    if (Config->BitRate < Features->MinBitRate)   Config->BitRate = Features->MinBitRate;
    */
    // add more architecture independent checks...
    Assert_ETHERNET( LogicalIndex == Config->LogicalIndex, ttc_assert_origin_auto ); // configuration must store corresponding logical index!

    // let low-level driver check this configuration too
    _driver_ethernet_configuration_check( Config );
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
