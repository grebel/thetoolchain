/** { ttc_filesystem.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for filesystem devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Most functions of this driver are defined as macros and being implemented
 *  in interfaces/ttc_filesystem_interface.c or in low-level drivers filesystem/filesystem_*.c.
 *
 *  See corresponding ttc_filesystem.h for documentation of all high-level functions.
 *
 *  Created from template ttc_device.c revision 52 at 20180413 11:21:12 UTC
 *
 *  Authors: <AUTHOR>
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_filesystem.h".
//
#include "ttc_filesystem.h"
#include "filesystem/filesystem_common.h"
#include "ttc_heap.h"       // dynamic memory allocationand safe arrays  
#include "ttc_task.h"       // disable/ enable multitasking scheduler
#include "ttc_systick.h"    // precise delays
#ifdef EXTENSION_ttc_interrupt
    #include "ttc_interrupt.h"  // globally disable/ enable interrupts
#endif
#ifdef EXTENSION_ttc_sdcard
    #include "ttc_sdcard.h"           // secure digital card driver can be used as storage device
    #include "sdcard/sdcard_common.h" // we need access to some common functions
#endif

//}Includes

#if TTC_FILESYSTEM_AMOUNT == 0
    #warning No FILESYSTEM devices defined, did you forget to activate something? - Define at least TTC_FILESYSTEM1 as one from e_ttc_filesystem_architecture in your makefile!
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of filesystem devices.
 *
 */

// for each initialized device, a pointer to its generic and architecture specific definitions is stored
A_define( t_ttc_filesystem_config*, ttc_filesystem_configs, TTC_FILESYSTEM_AMOUNT );

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ********************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_filesystem(t_ttc_filesystem_config* Config)

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * @param LogicalIndex    1.. logical index of FILESYSTEM device. Each logical device <n> is defined via TTC_FILESYSTEM<n>* constants in compile_options.h and extensions.active/makefile
 */
void _ttc_filesystem_configuration_check( t_u8 LogicalIndex );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

e_ttc_filesystem_errorcode ttc_filesystem_blocks_read( t_u8 LogicalIndex, t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks ) {
    Assert_FILESYSTEM_Writable( Buffer, ttc_assert_origin_auto ); // always check incoming pointer arguments
    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );

    e_ttc_filesystem_errorcode Result = filesystem_common_blocks_read( Config, Buffer, Address, AmountBlocks );

    return Result; // e_ttc_filesystem_errorcode
}
e_ttc_filesystem_errorcode ttc_filesystem_blocks_write( t_u8 LogicalIndex, const t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks ) {
    Assert_FILESYSTEM_Writable( ( void* ) Buffer, ttc_assert_origin_auto ); // always check incoming pointer arguments
    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );

    e_ttc_filesystem_errorcode Result = filesystem_common_blocks_write( Config, Buffer, Address, AmountBlocks );

    return Result; // e_ttc_filesystem_errorcode
}
t_ttc_filesystem_config*   ttc_filesystem_create() {

    t_u8 LogicalIndex = ttc_filesystem_get_max_index();
    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );

    TTC_TASK_RETURN( Config ); // will perform stack overflow check + return value
}
t_u8                       ttc_filesystem_get_max_index() {
    TTC_TASK_RETURN( TTC_FILESYSTEM_AMOUNT ); // will perform stack overflow check + return value
}
t_ttc_filesystem_config*   ttc_filesystem_get_configuration( t_u8 LogicalIndex ) {
    Assert_FILESYSTEM( LogicalIndex > 0, ttc_assert_origin_auto ); // logical index starts at 1
    Assert_FILESYSTEM( LogicalIndex <= TTC_FILESYSTEM_AMOUNT, ttc_assert_origin_auto ); // invalid index given (did you configure engnough ttc_filesystem devices in your makefile?)
    t_ttc_filesystem_config* Config = A( ttc_filesystem_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_disable();                      // ensure that we are not interrupted while creating configuration
#endif

        Config = A( ttc_filesystem_configs, LogicalIndex - 1 ); // have to reload with disabled interrupts to avoid racing conditions
        if ( !Config ) {
            Config = A( ttc_filesystem_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_filesystem_config ) );
            Config->Flags.Initialized = 0;             // make sure that deinit() is not called during load_defaults()
            ttc_filesystem_load_defaults( LogicalIndex );
        }

#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_enable();
#endif
    }

    Assert_FILESYSTEM_EXTRA_Writable( Config, ttc_assert_origin_auto ); // memory corrupted?
    TTC_TASK_RETURN( Config ); // will perform stack overflow check + return value
}
void                       ttc_filesystem_deinit( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration

    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );

    if ( Config->Flags.Initialized ) {
        e_ttc_filesystem_errorcode Result = _driver_filesystem_deinit( Config );
        if ( Result == E_ttc_filesystem_errorcode_OK )
        { Config->Flags.Initialized = 0; }
    }
    ttc_task_critical_end(); // other tasks now may access this driver
}
e_ttc_filesystem_errorcode ttc_filesystem_init( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration
#if (TTC_ASSERT_FILESYSTEM_EXTRA == 1)
    t_u8 InitialCriticalLevel = ttc_interrupt_critical_level();
#endif
    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );

    // check configuration to meet all limits of current architecture
    _ttc_filesystem_configuration_check( LogicalIndex );

    Config->LastError = _driver_filesystem_init( Config );
    if ( ! Config->LastError ) // == 0
    { Config->Flags.Initialized = 1; }

    Assert_FILESYSTEM( Config->Architecture != 0, ttc_assert_origin_auto ); // Low-Level driver must set this value!
    Assert_FILESYSTEM_EXTRA( InitialCriticalLevel == ttc_interrupt_critical_level(), ttc_assert_origin_auto ); // amount of  ttc_task_critical_end() calls does not match. Check driver implementation!
    ttc_task_critical_end(); // other tasks now may access this driver

    TTC_TASK_RETURN( Config->LastError ); // will perform stack overflow check + return value
}
e_ttc_filesystem_errorcode ttc_filesystem_load_defaults( t_u8 LogicalIndex ) {
    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );

    u_ttc_filesystem_architecture* LowLevelConfig = NULL;
    if ( Config->Flags.Initialized ) {
        ttc_filesystem_deinit( LogicalIndex );
        LowLevelConfig = Config->LowLevelConfig; // rescue pointer to dynamic allocated memory
    }

    ttc_memory_set( Config, 0, sizeof( t_ttc_filesystem_config ) );

    // restore pointer to dynamic allocated memory
    Config->LowLevelConfig = LowLevelConfig;

    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    switch ( LogicalIndex ) { // physical index of filesystem device from static configuration
#ifdef TTC_FILESYSTEM1
        case  1: Config->PhysicalIndex = TTC_FILESYSTEM1; break;
#endif
#ifdef TTC_FILESYSTEM2
        case  2: Config->PhysicalIndex = TTC_FILESYSTEM2; break;
#endif
#ifdef TTC_FILESYSTEM3
        case  3: Config->PhysicalIndex = TTC_FILESYSTEM3; break;
#endif
#ifdef TTC_FILESYSTEM4
        case  4: Config->PhysicalIndex = TTC_FILESYSTEM4; break;
#endif
#ifdef TTC_FILESYSTEM5
        case  5: Config->PhysicalIndex = TTC_FILESYSTEM5; break;
#endif
#ifdef TTC_FILESYSTEM6
        case  6: Config->PhysicalIndex = TTC_FILESYSTEM6; break;
#endif
#ifdef TTC_FILESYSTEM7
        case  7: Config->PhysicalIndex = TTC_FILESYSTEM7; break;
#endif
#ifdef TTC_FILESYSTEM8
        case  8: Config->PhysicalIndex = TTC_FILESYSTEM8; break;
#endif
#ifdef TTC_FILESYSTEM9
        case  9: Config->PhysicalIndex = TTC_FILESYSTEM9; break;
#endif
#ifdef TTC_FILESYSTEM10
        case 10: Config->PhysicalIndex = TTC_FILESYSTEM10; break;
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }

    //Insert additional generic default values here ...

    // Let low-level driver initialize remaining fields
    Config->LastError = _driver_filesystem_load_defaults( Config );

    // Check mandatory configuration items
    Assert_FILESYSTEM_EXTRA( ( Config->Architecture > E_ttc_filesystem_architecture_None ) && ( Config->Architecture < E_ttc_filesystem_architecture_unknown ), ttc_assert_origin_auto ); // architecture not set properly! Low-Level driver must set this field!

    TTC_TASK_RETURN( Config->LastError ); // will perform stack overflow check + return value
}
e_ttc_storage_event        ttc_filesystem_medium_detect( t_u8 LogicalIndex ) {
    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );

    return filesystem_common_medium_detect( Config );
}
e_ttc_filesystem_errorcode ttc_filesystem_medium_mount( t_u8 LogicalIndex ) {
    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );
    Config->BlockSize = 0;

    if ( Config->LastEvent == E_ttc_storage_event_media_inserted ) { // media has just been inserted: wait some time
        ttc_systick_delay_simple( 500000 ); // 0.5 secs
    }
    e_ttc_filesystem_errorcode Error = filesystem_common_medium_mount( Config );
    if ( !Error ) {
        Assert_FILESYSTEM( Config->BlockSize > 0, ttc_assert_origin_auto ); // low-level driver must update this value. Check implementation of filesystem_XXX_medium_mount()
    }

    return Error;
}
t_u8                       ttc_filesystem_medium_unmount( t_u8 LogicalIndex ) {
    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );

    return filesystem_common_medium_unmount( Config );
}
e_ttc_filesystem_errorcode ttc_filesystem_open_directory( t_u8 LogicalIndex, const t_u8* Name, t_u8 Handle ) {
    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );
    Assert_FILESYSTEM( Handle < TTC_FILESYSTEM_MAX_DIRECTORIES, ttc_assert_origin_auto ); // invalid value given. Increase value of TTC_FILESYSTEM_MAX_DIRECTORIES or check implementation of caller!
    e_ttc_filesystem_errorcode Result = 0;

    // high-level code being executed before calling low-level driver...

    // pass function call to interface or low-level driver function
    Result = _driver_filesystem_open_directory( Config, Name, Handle );

    // high-level code may check Result before returning it...

    return Result;
}
void                       ttc_filesystem_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)

    A_reset( ttc_filesystem_configs, TTC_FILESYSTEM_AMOUNT ); // safe arrays are placed in data section and must be initialized manually

    if ( 0 ) // optional: register this device for a sysclock change update
    { ttc_sysclock_register_for_update( ttc_filesystem_sysclock_changed ); }

    _driver_filesystem_prepare();
}
void                       ttc_filesystem_reset( t_u8 LogicalIndex ) {
    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );

    _driver_filesystem_reset( Config );
}
void                       ttc_filesystem_sysclock_changed() {

    // deinit + reinit all initialized FILESYSTEM devices
    for ( t_u8 LogicalIndex = 1; LogicalIndex < ttc_filesystem_get_max_index(); LogicalIndex++ ) {
        t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );
        if ( Config->Flags.Initialized ) {
            ttc_filesystem_deinit( LogicalIndex );
            ttc_filesystem_init( LogicalIndex );
        }
    }
}

#ifdef EXTENSION_ttc_sdcard
e_ttc_filesystem_errorcode ttc_filesystem_init_sdcard( t_u8 LogicalIndex, t_u8 LogicalIndex_SdCard ) {
    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );
    VOLATILE_FILESYSTEM e_ttc_filesystem_errorcode Result = 0;

    t_ttc_sdcard_config* SdCard = ttc_sdcard_get_configuration( LogicalIndex_SdCard );

    // configure storage driver data
    Config->Init.Storage.Config         = SdCard;
    Config->Init.Storage.blocks_read    = &sdcard_common_blocks_read;
    Config->Init.Storage.blocks_write   = &sdcard_common_blocks_write;
    Config->Init.Storage.medium_detect  = &sdcard_common_medium_detect;
    Config->Init.Storage.medium_mount   = &sdcard_common_medium_mount;
    Config->Init.Storage.medium_unmount = &sdcard_common_medium_unmount;

    Result = ttc_filesystem_init( LogicalIndex );

    return Result; // e_ttc_filesystem_errorcode
}
#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_filesystem(t_u8 LogicalIndex) {  }

void _ttc_filesystem_configuration_check( t_u8 LogicalIndex ) {
    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );

    // perform some generic plausibility checks
    const t_ttc_filesystem_features* Features = Config->Features; // this pointer should have been set by current filesystem_XXX_load_defaults()
    Assert_FILESYSTEM_EXTRA_Readable( Features, ttc_assert_origin_auto ); // low-level driver did not load this pointer with a address of its feature set. Declare and define a constant instance of t_ttc_filesystem_features and assign a reference to it in current filesystem_XXX_load_defaults()!
    Assert_FILESYSTEM_EXTRA( Features->Cache_AmountBlocks_Minimum   <= Features->Cache_AmountBlocks_Maximum,   ttc_assert_origin_auto ); // this makes no sende. Check feature set configuration of current low-level driver!
    Assert_FILESYSTEM_EXTRA( Features->Scratch_AmountBlocks_Minimum <= Features->Scratch_AmountBlocks_Maximum, ttc_assert_origin_auto ); // this makes no sende. Check feature set configuration of current low-level driver!
    Assert_FILESYSTEM_EXTRA( Features->BlockSize_Minimum > 0,                                                  ttc_assert_origin_auto ); // this makes no sende. Check feature set configuration of current low-level driver!
    Assert_FILESYSTEM_EXTRA( Features->BlockSize_Minimum            <= Features->BlockSize_Maximum,            ttc_assert_origin_auto ); // this makes no sende. Check feature set configuration of current low-level driver!
    if ( Features->Cache_AmountBlocks_Minimum > 0 ) {
        Assert_FILESYSTEM_Writable( Config->Init.Cache,            ttc_assert_origin_auto ); // must point to writable memory. Read field description in ttc_filesystem_types.h!
    }
    if ( Features->Scratch_AmountBlocks_Minimum > 0 ) {
        Assert_FILESYSTEM_Writable( Config->Init.Scratch,          ttc_assert_origin_auto ); // must point to writable memory. Read field description in ttc_filesystem_types.h!
    }

    // add more architecture independent checks...
    Assert_FILESYSTEM( LogicalIndex == Config->LogicalIndex,       ttc_assert_origin_auto ); // configuration must store corresponding logical index!
    Assert_FILESYSTEM_Readable( Config->Init.Storage.blocks_read,  ttc_assert_origin_auto ); // must point to executable memory.
    Assert_FILESYSTEM_Readable( Config->Init.Storage.blocks_write, ttc_assert_origin_auto ); // must point to executable memory.
    Assert_FILESYSTEM_Writable( Config->Init.Storage.Config,       ttc_assert_origin_auto ); // must point to writable memory.

    // let low-level driver check this configuration too
    _driver_filesystem_configuration_check( Config );
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
