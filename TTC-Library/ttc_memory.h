#ifndef TTC_MEMORY_H
#define TTC_MEMORY_H
/** { ttc_memory.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Architecture independent support for dynamic memory management.
 *
 *  Structures, Enums and Defines being required by high-level memory and application.
 *
 *  Created from template ttc_device.h revision 25 at 20140228 13:50:05 UTC
 *
 *  Authors: Gregor Rebel
 *
 *
 */
/** Description of ttc_memory (Do not delete this line!)
 *
 * ttc_memory provides basic operations on different kind of memories.
 * In this context, memory can mean any of
 * - Random Accessible Memory (RAM)
 * - Read Only Memory         (ROM or FLASH)
 * - Memory Maped IO Areas    (MMIO)
 *
 * H2: Basic Memory Operations
 * - copy amount of bytes from one memory location to another
 *   ->ttc_memory_copy()
 * - compare data at two different memory locations
 *   ->ttc_memory_compare()
 * - fill a memory chunk with a pattern
 *   ->ttc_memory_set_byte()
 *
 * H2: Memory Tests
 * - test if a given address points to writable memory
 *   ->ttc_memory_is_writable()
 * - test if a given address points to readable memory
 *   ->ttc_memory_is_readable()
 * - test if a given address points to readable but not writable memory
 *   ->ttc_memory_is_constant()
 * - test if a given address is inside given boundaries
 *   ->ttc_memory_checkpointer_within()
 *
}*/

#ifndef EXTENSION_ttc_memory
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_memory.sh
#endif

extern void Assert_Halt_NULL();

//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory_types.h"
#include "interfaces/ttc_memory_interface.h" // multi architecture support
//?#include "ttc_list.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

#define memcpy(Destination, Source, AmountBytes)     ttc_memory_copy(Destination, Source, AmountBytes);
#define memcmp(A, B, AmountBytes)                    ttc_memory_compare(A, B, AmountBytes)
#define memset(Destination, Fill, AmountBytes)       ttc_memory_set_byte(Destination, Fill, AmountBytes)

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level memory only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)


#ifndef EXTENSION_ttc_memory
    #  error Extension must be activated: 500_ttc_memory
#endif



/** Safe Pointers

 A common error in C applications is dereferencing an invalid pointer.
 Basically this means a construct of type A->B. If A points to an invalid memory region
 then a corresponding hardware exception should be raised. This often means, that no local
 variables are accessible any more. If the pointer adresses invalid data the processor not
 even recognises the illegal access and reads invalid data or corrupts foreign memory regions.

 In C, dereferenciations are not checked at all.
 One may cast any constant to any type and try to dereference it.
 Assume the following:  t_u8 D = *( (t_u8*) 0x0 );
 Though this is fine for the compiler, your processor won't like it.
 Reading from address zero is forbidden on many architectures.
 Doing so will cause an exception to be raised and a special handler will be called.
 In most cases, this means that the program flow comes to an end because the
 program counter is held in an endless loop.

 TheToolChain supports an implicit NULL-pointer checking.
 For this paradigm, it is required that every unused pointer is initialized with NULL (0).
 Pointers can then be checked against NULL by use of P() macro. It will check if given argument
 is not NULL and return a pointer of same type. You don't have to cast anything. Even completion
 works in your IDE.

 Of course, use of safe pointers inflicts some processing overhead.
 Therefore, all pointer checking can be disabled by defining TTC_SAFE_POINTERS = 0.
 This can be done best in your makefile (yes its -D):
 COMPILE_OPTS += -DTTC_SAFE_POINTERS=0

 Don't overdue pointer checking!
 Not every dereferenciation needs a pointer check. In small functions, it is egnough to check
 pointers int the beginning. Good functions will check all their pointer arguments via Assert()
 in the first lines.
 All ttc_XXX() functions do check their pointer arguments. Therefore it is not necessary to check
 pointers given to a ttc_XXX() function. You can safely write
   ttc_memory_set(B, 0, 100);
 instead of
   ttc_memory_set(P(B), 0, 100);


 Example:

 struct Data_s { t_u8 A; t_u8* B; };

 struct Data_s* MyPointer = NULL;      // always initialize your pointers!

 ...

 t_u8 A2 = P(MyPointer)->A;           // if MyPointer is still NULL, Assert() will be called
 P(MyPointer)->B = &A2;

 t_u8 B2 Data = *(P(P(MyPointer).B)); // double dereferenciation with individual checks
 // The above line equals to:
 //   Assert(MyPointer, ttc_assert_origin_auto);
 //   t_u8* B1 = MyPointer->B;
 //   Assert(B1, ttc_assert_origin_auto);
 //   t_u8 B2 = *B1;

 // BTW: You don't have to encapsulate every dereferenciation with P().
 //      In small code snippets, it is safe to check your pointers only at the beginning.
 //
 void copy(t_u8* Dst, t_u8* Src, t_u8 Amount) {
   t_u8* Writer = P(Dst);
   t_u8* Reader = P(Src);

   while (Amount-- > 0)
     *Writer++ = *Reader++;
 }
 */

#ifndef TTC_SAFE_POINTERS
    #define TTC_SAFE_POINTERS 1  // =1: use safe pointers; =0: use normal pointers (no overhead)
#endif

#if (TTC_SAFE_POINTERS==1)
    #define P(NAME) (&(NAME[ttc_memory_checkpointer(NAME)]))
    #define P2(NAME, MIN, MAX) (&(NAME[ttc_memory_checkpointer_within(NAME, MIN, MAX)]))
#else
    #define P(NAME) NAME
    #define P2(NAME, MIN, MAX) NAME
#endif

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * memory devices on all supported architectures.
 * Check memory/memory_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares memory Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 * Note: This function must be called BEFORE any _ttc_heap_alloc() call!
 * Note: This function is not thread safe!
 *
 * Note: Use nm tool to get an overview of your individual memory layout:
 *       ARM CortexM: arm-none-eabi-nm --numeric-sort main.elf
 */
void ttc_memory_prepare();

/** compares two memory areas byte by byte. Provides optimizations for certain architectures.
 * @param A      starting address of first memory area
 * @param B      starting address of first memory area
 * @param AmountBytes AmountBytes of bytes to compare
 * @return       =0: all checked bytes were the same; >0: index of first differing byte
 */
t_base ttc_memory_compare( const void* A, const void* B, t_base AmountBytes );

/** Writes given fill value into memory area. Provides optimizations for certain architectures.
 * @param Destination  data is written starting at this address
 * @param Fill         value to write into memory
 * @param AmountBytes  amount of bytes to write
 */
void ttc_memory_set( void* Destination, t_base Fill, t_base AmountBytes );
#ifndef memset
    //? #  define memset(Destination, Fill, AmountBytes)  ttc_memory_set(Destination, Fill, AmountBytes)
#endif

/** Writes given fill byte into memory area. Provides optimizations for certain architectures.
 * @param Destination  data is written starting at this address
 * @param Fill         byte value to write into memory
 * @param AmountBytes  amount of bytes to write
 */
void ttc_memory_set_byte( void* Destination, t_u8 Fill, t_base AmountBytes );

/** Copies data from one memory area to another one. Provides optimizations for certain architectures.
 * @param Destination  data is written starting at this address
 * @param Source       data is read starting from this address
 * @param AmountBytes       AmountBytes of bytes to copy
 */
void ttc_memory_copy( void* Destination, const void* Source, t_base AmountBytes );


// Memory Checks *********************************************************

/** checks if given memory address is known as being readonly by a pointer.
 *
 * This function is designed to be used for Assert() checks.
 *
 * E.g.: Assert(ttc_memory_is_constant(Pointer), ttc_assert_origin_auto);
 *
 * @param Address  memory pointer to check
 * @return  ==1: memory is constant (resides in ROM or FLASH);
 *          ==0: memory is known not to be readonly (E.g. resides in RAM or is invalid)
 *         ==-1: memory status not known for current architecture
 */
t_s8 ttc_memory_is_constant( volatile const void* Address );
t_s8 _driver_memory_is_constant( volatile const void* Address );
#define ttc_memory_is_constant(Address) _driver_memory_is_constant(Address)

/** checks if given memory address is known to store program code.
 *
 * This function is designed to be used for Assert() checks.
 *
 * E.g.: Assert(ttc_memory_is_executable(FunctionPointer), ttc_assert_origin_auto);
 *       FunctionPointer();
 *
 * @param Address  memory pointer to check
 * @return  ==1: memory is executable (resides in ROM or RAM);
 *          ==0: memory is known not to be executable
 *         ==-1: memory status not known for current architecture
 */
t_s8 ttc_memory_is_executable( volatile const void* Address );
t_s8 _driver_memory_is_executable( volatile const void* Address );
#define ttc_memory_is_executable(Address) _driver_memory_is_executable(Address)

/** checks if given memory address is known as being writable by a pointer.
 *
 * This function is designed to be used for Assert() checks.
 *
 * E.g.: Assert(ttc_memory_is_writable(Pointer), ttc_assert_origin_auto);
 *       *Pointer = foo;
 *
 * @param Address  memory pointer to check
 * @return  ==1: memory is writable (resides in RAM or Registers);
 *          ==0: memory is known not to be writable != memory is readonly!
 *         ==-1: memory status not known for current architecture
 */
t_s8 ttc_memory_is_writable( volatile const void* Address );
t_s8 _driver_memory_is_writable( volatile const void* Address );
#define ttc_memory_is_writable(Address) _driver_memory_is_writable(Address)

/** checks if given memory address is known as being readable by a pointer.
 *
 * This function is designed to be used for Assert() checks.
 *
 * E.g.: Assert(ttc_memory_is_readable(Pointer), ttc_assert_origin_auto);
 *       foo = *Pointer;
 *
 * @param Address  memory pointer to check
 * @return  ==1: memory is readable (resides in RAM, ROM, FLASH or Registers);
 *          ==0: memory is known not to be writable != memory is readonly!
 *         ==-1: memory status not known for current architecture
 */
t_s8 ttc_memory_is_readable( volatile const void* Address );
t_s8 _driver_memory_is_readable( volatile const void* Address );
#define ttc_memory_is_readable(Address) _driver_memory_is_readable(Address)

/** will assert if given pointer is 0 (NULL)
 *
 * Note: This function is normally used via macro P()
 *
 * @param Pointer  memory pointer to check
 * @return         =0: pointer was not 0; ttc_assert_halt_origin( ttc_assert_origin_auto ) is called otherwise
 */
inline t_u8 ttc_memory_checkpointer( void* Pointer ) {
    if ( !Pointer )
    { ttc_assert_halt_origin( ttc_assert_origin_auto ); }

    return 0;
}

/** will assert if given pointer is outside [Min, .., Max]
 * Note: This function is normally used via macro P()
 *
 * @param Pointer  memory pointer to check
 * @param Min      lowest allowed pointer address
 * @param Max      highest allowed pointer address
 * @return         =0: pointer was not 0
 */
inline t_u8 ttc_memory_checkpointer_within( void* Pointer, void* Min, void* Max ) {
    if ( ( Pointer < Min ) || ( Pointer > Max ) )
    { return 0; }

    return 1;
}

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)


//}

#endif //TTC_MEMORY_H
