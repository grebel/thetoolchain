/** { ttc_rtc.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level interface and documentation for RTC devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_RTC(tc_rtc_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_rtc_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_rtc_init(LogicalIndex);
 *  4) use:         ttc_rtc_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level rtc and application.
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_RTC_H
#define TTC_RTC_H

//{ Includes *************************************************************


#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "interfaces/ttc_rtc_interface.h"
#include "ttc_memory.h"
#include "ttc_interrupt.h"
#include "ttc_task.h"
#include "interfaces/ttc_rtc_interface.h" // multi architecture support
#include "ttc_rtc_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************
#ifndef EXTENSION_ttc_rtc
#error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_rtc.sh
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level rtc only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * rtc devices on all supported architectures.
 * Check rtc/rtc_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares rtc Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_rtc_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_rtc_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_RTC_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_rtc_config* ttc_rtc_get_configuration( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_RTC_get_max_LogicalIndex())
 */
void ttc_rtc_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_RTC_get_max_LogicalIndex())
 */

void ttc_rtc_load_defaults( t_u8 LogicalIndex );

void ttc_rtc_deinit( t_u8 LogicalIndex );

/** maps from logical to physical device index
 *
 * High-level rtcs (ttc_rtc_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_RTCn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any
 * order.
 *
 * @param LogicalIndex  logical index of rtc device (1..ttc_rtc_get_max_index() )
 * @return              physical index of rtc device (0 = first physical rtc device, ...)
 */
t_u8 ttc_rtc_logical_2_physical_index( t_u8 LogicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_RTC_get_max_LogicalIndex())
 */
void ttc_rtc_reset( t_u8 LogicalIndex );

void ttc_rtc_get_time( t_u32 Format, t_ttc_rtc_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_rtc(t_u8 LogicalIndex)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Each function is given as a prototype and checked for a matching DRIVER_*() definition.
 * The more functions are implemented by low-level driver, the more features can be used on current architecture.
 *
 * Note: functions declared below are passed to interfaces/ttc_rtc_interface.h
 *
 * Note: If you add a DRIVER_* prototype here, use create_DeviceDriver.pl to automatically add
 *       empty functions in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 *       cd templates/; ./create_DeviceDriver.pl rtc UPDATE
 */

/** Prepares rtc driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void DRIVER_rtc_prepare();

/** loads configuration of indexed RTC unit with default values
 * @param Config        pointer to struct t_ttc_rtc_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_rtc_errorcode DRIVER_rtc_load_defaults( t_ttc_rtc_config* Config );

/** shutdown single RTC unit device
 * @param Config        pointer to struct t_ttc_rtc_config (must have valid value for LogicalIndex)
 * @return              == 0: RTC has been shutdown successfully; != 0: error-code
 */
e_ttc_rtc_errorcode DRIVER_rtc_deinit( t_ttc_rtc_config* Config );

/** initializes single RTC unit for operation
 * @param Config        pointer to struct t_ttc_rtc_config (must have valid value for LogicalIndex)
 * @return              == 0: RTC has been initialized successfully; != 0: error-code
 */
e_ttc_rtc_errorcode DRIVER_rtc_init( t_ttc_rtc_config* Config );


//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_RTC_H
