/** { ttc_accelerometer_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for ACCELEROMETER device.
 *  Structures, Enums and Defines being required by both, high- and low-level accelerometer.
 *  This file does not provide function declarations!
 *
 *  Created from template ttc_device_types.h revision 27 at 20141020 08:58:47 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_ACCELEROMETER_TYPES_H
#define TTC_ACCELEROMETER_TYPES_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_accelerometer.h" or "ttc_accelerometer.c"
//
#include "ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
#ifdef EXTENSION_accelerometer_mpu6050
#  include "accelerometer/accelerometer_mpu6050_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_accelerometer_lis3lv02dl
#  include "accelerometer/accelerometer_lis3lv02dl_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_accelerometer_bno055
#  include "accelerometer/accelerometer_bno055_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

//{  TTC_ACCELEROMETERn has to be defined as constant by makefile.100_board_*
#ifdef TTC_ACCELEROMETER5
#ifndef TTC_ACCELEROMETER4
#error TTC_ACCELEROMETER5 is defined, but not TTC_ACCELEROMETER4 - all lower TTC_ACCELEROMETERn must be defined!
#endif
#ifndef TTC_ACCELEROMETER3
#error TTC_ACCELEROMETER5 is defined, but not TTC_ACCELEROMETER3 - all lower TTC_ACCELEROMETERn must be defined!
#endif
#ifndef TTC_ACCELEROMETER2
#error TTC_ACCELEROMETER5 is defined, but not TTC_ACCELEROMETER2 - all lower TTC_ACCELEROMETERn must be defined!
#endif
#ifndef TTC_ACCELEROMETER1
#error TTC_ACCELEROMETER5 is defined, but not TTC_ACCELEROMETER1 - all lower TTC_ACCELEROMETERn must be defined!
#endif

#define TTC_ACCELEROMETER_AMOUNT 5
#else
#ifdef TTC_ACCELEROMETER4
#define TTC_ACCELEROMETER_AMOUNT 4

#ifndef TTC_ACCELEROMETER3
#error TTC_ACCELEROMETER5 is defined, but not TTC_ACCELEROMETER3 - all lower TTC_ACCELEROMETERn must be defined!
#endif
#ifndef TTC_ACCELEROMETER2
#error TTC_ACCELEROMETER5 is defined, but not TTC_ACCELEROMETER2 - all lower TTC_ACCELEROMETERn must be defined!
#endif
#ifndef TTC_ACCELEROMETER1
#error TTC_ACCELEROMETER5 is defined, but not TTC_ACCELEROMETER1 - all lower TTC_ACCELEROMETERn must be defined!
#endif
#else
#ifdef TTC_ACCELEROMETER3

#ifndef TTC_ACCELEROMETER2
#error TTC_ACCELEROMETER5 is defined, but not TTC_ACCELEROMETER2 - all lower TTC_ACCELEROMETERn must be defined!
#endif
#ifndef TTC_ACCELEROMETER1
#error TTC_ACCELEROMETER5 is defined, but not TTC_ACCELEROMETER1 - all lower TTC_ACCELEROMETERn must be defined!
#endif

#define TTC_ACCELEROMETER_AMOUNT 3
#else
#ifdef TTC_ACCELEROMETER2

#ifndef TTC_ACCELEROMETER1
#error TTC_ACCELEROMETER5 is defined, but not TTC_ACCELEROMETER1 - all lower TTC_ACCELEROMETERn must be defined!
#endif

#define TTC_ACCELEROMETER_AMOUNT 2
#else
#ifdef TTC_ACCELEROMETER1
#define TTC_ACCELEROMETER_AMOUNT 1
#else
#define TTC_ACCELEROMETER_AMOUNT 0
#endif
#endif
#endif
#endif
#endif
//}
/**{ Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_ACCELEROMETER 0  # disable assert handling for accelerometer devices
 *
 */
#ifndef TTC_ASSERT_ACCELEROMETER    // any previous definition set (Makefile)?
#define TTC_ASSERT_ACCELEROMETER 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_ACCELEROMETER == 1)  // use Assert()s in ACCELEROMETER code (somewhat slower but alot easier to debug)
#define Assert_ACCELEROMETER(Condition, ErrorCode) Assert(Condition, ErrorCode)
#define Assert_ACCELEROMETER_Writable(Address, Origin) Assert_Writable(Address, Origin)
#define Assert_ACCELEROMETER_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // us no Assert()s in ACCELEROMETER code (somewhat smaller + faster, but crashes are hard to debug)
#define Assert_ACCELEROMETER(Condition, ErrorCode)
#define Assert_ACCELEROMETER_Writable(Address, Origin)
#define Assert_ACCELEROMETER_Readable(Address, Origin)
#endif
//}
/**{ Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_accelerometer_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_accelerometer_architecture
#warning Missing low-level definition for t_ttc_accelerometer_architecture (using default)
#define t_ttc_accelerometer_architecture void
#endif
//}
/**{ Check if definitions of logical indices are taken from e_ttc_accelerometer_physical_index
 *
 * See definition of e_ttc_accelerometer_physical_index below for details.
 *
 */

/*
#define TTC_INDEX_ACCELEROMETER_MAX 10  // must have same value as TTC_INDEX_ACCELEROMETER_ERROR
#ifdef TTC_ACCELEROMETER1
#  if TTC_ACCELEROMETER1 >= TTC_INDEX_ACCELEROMETER_MAX
#    error Wrong definition of TTC_ACCELEROMETER1, check your makefile (must be one from e_ttc_accelerometer_physical_index)!
#  endif
#  ifndef TTC_ACCELEROMETER1_DRIVER
#    warning Missing definition, check your makefile: TTC_ACCELEROMETER1_DRIVER (choose one from e_ttc_radio_architecture)
#    define TTC_ACCELEROMETER1_DRIVER ta_radio_None
#  endif
#endif
#ifdef TTC_ACCELEROMETER2
#  if TTC_ACCELEROMETER2 >= TTC_INDEX_ACCELEROMETER_MAX
#    error Wrong definition of TTC_ACCELEROMETER2, check your makefile (must be one from e_ttc_accelerometer_physical_index)!
#  endif
#  ifndef TTC_ACCELEROMETER2_DRIVER
#    warning Missing definition, check your makefile: TTC_ACCELEROMETER2_DRIVER (choose one from e_ttc_radio_architecture)
#    define TTC_ACCELEROMETER2_DRIVER ta_radio_None
#  endif
#endif
#ifdef TTC_ACCELEROMETER3
#  if TTC_ACCELEROMETER3 >= TTC_INDEX_ACCELEROMETER_MAX
#    error Wrong definition of TTC_ACCELEROMETER3, check your makefile (must be one from e_ttc_accelerometer_physical_index)!
#  endif
#  ifndef TTC_ACCELEROMETER3_DRIVER
#    warning Missing definition, check your makefile: TTC_ACCELEROMETER3_DRIVER (choose one from e_ttc_radio_architecture)
#    define TTC_ACCELEROMETER3_DRIVER ta_radio_None
#  endif
#endif
#ifdef TTC_ACCELEROMETER4
#  if TTC_ACCELEROMETER4 >= TTC_INDEX_ACCELEROMETER_MAX
#    error Wrong definition of TTC_ACCELEROMETER4, check your makefile (must be one from e_ttc_accelerometer_physical_index)!
#  endif
#  ifndef TTC_ACCELEROMETER4_DRIVER
#    warning Missing definition, check your makefile: TTC_ACCELEROMETER4_DRIVER (choose one from e_ttc_radio_architecture)
#    define TTC_ACCELEROMETER4_DRIVER ta_radio_None
#  endif
#endif
#ifdef TTC_ACCELEROMETER5
#  if TTC_ACCELEROMETER5 >= TTC_INDEX_ACCELEROMETER_MAX
#    error Wrong definition of TTC_ACCELEROMETER5, check your makefile (must be one from e_ttc_accelerometer_physical_index)!
#  endif
#  ifndef TTC_ACCELEROMETER5_DRIVER
#    warning Missing definition, check your makefile: TTC_ACCELEROMETER5_DRIVER (choose one from e_ttc_radio_architecture)
#    define TTC_ACCELEROMETER5_DRIVER ta_radio_None
#  endif
#endif
#ifdef TTC_ACCELEROMETER6
#  if TTC_ACCELEROMETER6 >= TTC_INDEX_ACCELEROMETER_MAX
#    error Wrong definition of TTC_ACCELEROMETER6, check your makefile (must be one from e_ttc_accelerometer_physical_index)!
#  endif
#  ifndef TTC_ACCELEROMETER6_DRIVER
#    warning Missing definition, check your makefile: TTC_ACCELEROMETER6_DRIVER (choose one from e_ttc_radio_architecture)
#    define TTC_ACCELEROMETER6_DRIVER ta_radio_None
#  endif
#endif
#ifdef TTC_ACCELEROMETER7
#  if TTC_ACCELEROMETER7 >= TTC_INDEX_ACCELEROMETER_MAX
#    error Wrong definition of TTC_ACCELEROMETER7, check your makefile (must be one from e_ttc_accelerometer_physical_index)!
#  endif
#  ifndef TTC_ACCELEROMETER7_DRIVER
#    warning Missing definition, check your makefile: TTC_ACCELEROMETER7_DRIVER (choose one from e_ttc_radio_architecture)
#    define TTC_ACCELEROMETER7_DRIVER ta_radio_None
#  endif
#endif
#ifdef TTC_ACCELEROMETER8
#  if TTC_ACCELEROMETER8 >= TTC_INDEX_ACCELEROMETER_MAX
#    error Wrong definition of TTC_ACCELEROMETER8, check your makefile (must be one from e_ttc_accelerometer_physical_index)!
#  endif
#  ifndef TTC_ACCELEROMETER8_DRIVER
#    warning Missing definition, check your makefile: TTC_ACCELEROMETER8_DRIVER (choose one from e_ttc_radio_architecture)
#    define TTC_ACCELEROMETER8_DRIVER ta_radio_None
#  endif
#endif
#ifdef TTC_ACCELEROMETER9
#  if TTC_ACCELEROMETER9 >= TTC_INDEX_ACCELEROMETER_MAX
#    error Wrong definition of TTC_ACCELEROMETER9, check your makefile (must be one from e_ttc_accelerometer_physical_index)!
#  endif
#  ifndef TTC_ACCELEROMETER9_DRIVER
#    warning Missing definition, check your makefile: TTC_ACCELEROMETER9_DRIVER (choose one from e_ttc_radio_architecture)
#    define TTC_ACCELEROMETER9_DRIVER ta_radio_None
#  endif
#endif
#ifdef TTC_ACCELEROMETER10
#  if TTC_ACCELEROMETER10 >= TTC_INDEX_ACCELEROMETER_MAX
#    error Wrong definition of TTC_ACCELEROMETER10, check your makefile (must be one from e_ttc_accelerometer_physical_index)!
#  endif
#  ifndef TTC_ACCELEROMETER10_DRIVER
#    warning Missing definition, check your makefile: TTC_ACCELEROMETER10_DRIVER (choose one from e_ttc_radio_architecture)
#    define TTC_ACCELEROMETER10_DRIVER ta_radio_None
#  endif
#endif
//}
*/
//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef e_ttc_physical_index e_ttc_accelerometer_physical_index;
typedef enum {   // e_ttc_accelerometer_errorcode      return codes of ACCELEROMETER devices
    ec_accelerometer_OK = 0,

    // other warnings go here..

    ec_accelerometer_ERROR,                  // general failure
    ec_accelerometer_NULL,                   // NULL pointer not accepted
    ec_accelerometer_DeviceNotFound,         // corresponding device could not be found
    ec_accelerometer_CommunicationErrorI2C,  // error occured durin I2C communication
    ec_accelerometer_InvalidConfiguration,   // sanity check of device configuration failed
    ec_accelerometer_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..

    ec_accelerometer_unknown                // no valid errorcodes past this entry
} e_ttc_accelerometer_errorcode;
typedef enum {   // e_ttc_accelerometer_architecture  types of architectures supported by ACCELEROMETER driver
    ta_accelerometer_None,           // no architecture selected


    ta_accelerometer_mpu6050_i2c, // automatically added by ./create_DeviceDriver.pl
    ta_accelerometer_lis3lv02dl,  // automatically added by ./create_DeviceDriver.pl
    ta_accelerometer_bno055,      // automatically added by ./create_DeviceDriver.pl
//InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_accelerometer_unknown        // architecture not supported
} e_ttc_accelerometer_architecture;
typedef struct s_ttc_accelerometer_measures { // measure values
    t_s16 AccelerationX; // G acceleration along X-axis (G/1000)
    t_s16 AccelerationY; // G acceleration along X-axis (G/1000)
    t_s16 AccelerationZ; // G acceleration along X-axis (G/1000)
} t_ttc_accelerometer_measures;


typedef struct s_ttc_accelerometer_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_accelerometer_init()
    //       and after ttc_accelerometer_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // latest measures from accelerometer
    t_ttc_accelerometer_measures Measures;

    // low-level configuration (structure defined by low-level driver)
    t_ttc_accelerometer_architecture LowLevelConfig;

    union  { // generic configuration bits common for all low-level Drivers
        t_u8 All;
        struct {
            unsigned Initialized        : 1;  // ==1: device has been initialized successfully
            // ToDo: additional high-level flags go here..

            unsigned Reserved1        : 7; // pad to 8 bits
        } Bits;
    } Flags;

    e_ttc_accelerometer_architecture Architecture; // type of architecture used for current accelerometer device
    t_u8 LogicalIndex;              // automatically set: logical index of device to use (1 = TTC_ACCELEROMETER1, ...)

    // logical index of I2C/SPI/USART/CAN device to use to communicate with accelerometer
    // Application must initialize device and set LogicalIndex_Interface before calling ttc_accelerometer_init()!
    t_u8 LogicalIndex_Interface;

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_accelerometer_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_ACCELEROMETER_TYPES_H
