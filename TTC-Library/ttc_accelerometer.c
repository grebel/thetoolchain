/** { ttc_accelerometer.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for accelerometer devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 27 at 20141020 08:58:47 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_accelerometer.h".
//
#include "ttc_accelerometer.h"
#include "ttc_heap.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_ACCELEROMETER_AMOUNT == 0
#warning No ACCELEROMETER devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of accelerometer devices.
 *
 */


// for each initialized device, a pointer to its generic and mpu6050 definitions is stored
A_define( t_ttc_accelerometer_config*, ttc_accelerometer_configs, TTC_ACCELEROMETER_AMOUNT );

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8                          ttc_accelerometer_get_max_index() {
    return TTC_ACCELEROMETER_AMOUNT;
}
t_ttc_accelerometer_config*   ttc_accelerometer_get_configuration( t_u8 LogicalIndex ) {

    Assert_ACCELEROMETER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_accelerometer_config* Config = A( ttc_accelerometer_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        Config = A( ttc_accelerometer_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_accelerometer_config ) );
        ttc_accelerometer_reset( LogicalIndex );
    }

    return Config;
}
void                          ttc_accelerometer_deinit( t_u8 LogicalIndex ) {
    Assert_ACCELEROMETER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_accelerometer_config* Config = ttc_accelerometer_get_configuration( LogicalIndex );

    e_ttc_accelerometer_errorcode Result = _driver_accelerometer_deinit( Config );
    if ( Result == ec_accelerometer_OK )
    { Config->Flags.Bits.Initialized = 0; }
}
e_ttc_accelerometer_errorcode ttc_accelerometer_init( t_u8 LogicalIndex ) {
    Assert_ACCELEROMETER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_accelerometer_config* Config = ttc_accelerometer_get_configuration( LogicalIndex );

    e_ttc_accelerometer_errorcode Result = _driver_accelerometer_init( Config );
    if ( Result == ec_accelerometer_OK )
    { Config->Flags.Bits.Initialized = 1; }

    return Result;
}
e_ttc_accelerometer_errorcode ttc_accelerometer_load_defaults( t_u8 LogicalIndex ) {
    Assert_ACCELEROMETER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_accelerometer_config* Config = ttc_accelerometer_get_configuration( LogicalIndex );

    if ( Config->Flags.Bits.Initialized )
    { ttc_accelerometer_deinit( LogicalIndex ); }

    ttc_memory_set( Config, 0, sizeof( t_ttc_accelerometer_config ) );

    // load generic default values
    switch ( LogicalIndex ) { // load type of low-level driver for current architecture
            #ifdef TTC_ACCELEROMETER1_DRIVER
        case  1: Config->Architecture = TTC_ACCELEROMETER1_DRIVER; break;
            #endif
            #ifdef TTC_ACCELEROMETER2_DRIVER
        case  2: Config->Architecture = TTC_ACCELEROMETER2_DRIVER; break;
            #endif
            #ifdef TTC_ACCELEROMETER3_DRIVER
        case  3: Config->Architecture = TTC_ACCELEROMETER3_DRIVER; break;
            #endif
            #ifdef TTC_ACCELEROMETER4_DRIVER
        case  4: Config->Architecture = TTC_ACCELEROMETER4_DRIVER; break;
            #endif
            #ifdef TTC_ACCELEROMETER5_DRIVER
        case  5: Config->Architecture = TTC_ACCELEROMETER5_DRIVER; break;
            #endif
            #ifdef TTC_ACCELEROMETER6_DRIVER
        case  6: Config->Architecture = TTC_ACCELEROMETER6_DRIVER; break;
            #endif
            #ifdef TTC_ACCELEROMETER7_DRIVER
        case  7: Config->Architecture = TTC_ACCELEROMETER7_DRIVER; break;
            #endif
            #ifdef TTC_ACCELEROMETER8_DRIVER
        case  8: Config->Architecture = TTC_ACCELEROMETER8_DRIVER; break;
            #endif
            #ifdef TTC_ACCELEROMETER9_DRIVER
        case  9: Config->Architecture = TTC_ACCELEROMETER9_DRIVER; break;
            #endif
            #ifdef TTC_ACCELEROMETER10_DRIVER
        case 10: Config->Architecture = TTC_ACCELEROMETER10_DRIVER; break;
            #endif
        default: ttc_assert_halt_origin( ec_accelerometer_InvalidImplementation ); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }
    Assert_ACCELEROMETER( ( Config->Architecture > ta_accelerometer_None ) && ( Config->Architecture < ta_accelerometer_unknown ), ttc_assert_origin_auto ); // architecture not set properly! Check makefile for TTC_ACCELEROMETER<n>_DRIVER and compare with e_ttc_accelerometer_architecture

    Config->LogicalIndex = LogicalIndex;

    //Insert additional generic default values here ...

    return _driver_accelerometer_load_defaults( Config );
}
void                          ttc_accelerometer_prepare() {
    // add your startup code here (Singletasking!)
    _driver_accelerometer_prepare();
}
t_ttc_accelerometer_measures* ttc_accelerometer_read_measures( t_u8 LogicalIndex ) {
    Assert_ACCELEROMETER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_accelerometer_config* Config = ttc_accelerometer_get_configuration( LogicalIndex );

    return _driver_accelerometer_read_measures( Config );
}
void                          ttc_accelerometer_reset( t_u8 LogicalIndex ) {
    Assert_ACCELEROMETER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_accelerometer_config* Config = ttc_accelerometer_get_configuration( LogicalIndex );

    _driver_accelerometer_reset( Config );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_accelerometer(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
