/** { ttc_dma_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for DMA device.
 *  Structures, Enums and Defines being required by both, high- and low-level dma.
 *  This file does not provide function declarations!
 *  
 *  Created from template ttc_device_types.h revision 27 at 20150108 10:24:10 UTC
 *
 *  Authors: Gregor Rebel
 * 
}*/

#ifndef TTC_DMA_TYPES_H
#define TTC_DMA_TYPES_H

//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_dma.h" or "ttc_dma.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h" // only required to help IDE to know constant definitions
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
#ifdef EXTENSION_dma_stm32l1xx
#  include "dma/dma_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)
 
//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************


//{  TTC_DMAn has to be defined as constant by makefile.100_board_*
#ifdef TTC_DMA5
  #ifndef TTC_DMA4
    #error TTC_DMA5 is defined, but not TTC_DMA4 - all lower TTC_DMAn must be defined!
  #endif
  #ifndef TTC_DMA3
    #error TTC_DMA5 is defined, but not TTC_DMA3 - all lower TTC_DMAn must be defined!
  #endif
  #ifndef TTC_DMA2
    #error TTC_DMA5 is defined, but not TTC_DMA2 - all lower TTC_DMAn must be defined!
  #endif
  #ifndef TTC_DMA1
    #error TTC_DMA5 is defined, but not TTC_DMA1 - all lower TTC_DMAn must be defined!
  #endif

  #define TTC_DMA_AMOUNT 5
#else
  #ifdef TTC_DMA4
    #define TTC_DMA_AMOUNT 4

    #ifndef TTC_DMA3
      #error TTC_DMA5 is defined, but not TTC_DMA3 - all lower TTC_DMAn must be defined!
    #endif
    #ifndef TTC_DMA2
      #error TTC_DMA5 is defined, but not TTC_DMA2 - all lower TTC_DMAn must be defined!
    #endif
    #ifndef TTC_DMA1
      #error TTC_DMA5 is defined, but not TTC_DMA1 - all lower TTC_DMAn must be defined!
    #endif
  #else
    #ifdef TTC_DMA3

      #ifndef TTC_DMA2
        #error TTC_DMA5 is defined, but not TTC_DMA2 - all lower TTC_DMAn must be defined!
      #endif
      #ifndef TTC_DMA1
        #error TTC_DMA5 is defined, but not TTC_DMA1 - all lower TTC_DMAn must be defined!
      #endif

      #define TTC_DMA_AMOUNT 3
    #else
      #ifdef TTC_DMA2

        #ifndef TTC_DMA1
          #error TTC_DMA5 is defined, but not TTC_DMA1 - all lower TTC_DMAn must be defined!
        #endif

        #define TTC_DMA_AMOUNT 2
      #else
        #ifdef TTC_DMA1
          #define TTC_DMA_AMOUNT 1
        #else
          #define TTC_DMA_AMOUNT 0
        #endif
      #endif
    #endif
  #endif
#endif
//}
/**{ Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop. 
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_DMA 0  # disable assert handling for dma devices
 *
 */
#ifndef TTC_ASSERT_DMA    // any previous definition set (Makefile)?
#define TTC_ASSERT_DMA 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_DMA == 1)  // use Assert()s in DMA code (somewhat slower but alot easier to debug)
  #define Assert_DMA(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in DMA code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_DMA(Condition, ErrorCode)
#endif
//}
/**{ Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_dma_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_dma_architecture
#warning Missing low-level definition for t_ttc_dma_architecture (using default)
#define t_ttc_dma_architecture void
#endif
//}
/**{ Check if definitions of logical indices are taken from e_ttc_dma_physical_index
 *
 * See definition of e_ttc_dma_physical_index below for details.
 *
 */

//}

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef e_ttc_physical_index e_ttc_dma_physical_index;
typedef enum {   // e_ttc_dma_errorcode      return codes of DMA devices
  ec_dma_OK = 0, 
  
  // other warnings go here..

  ec_dma_ERROR,                  // general failure
  ec_dma_NULL,                   // NULL pointer not accepted
  ec_dma_DeviceNotFound,         // corresponding device could not be found
  ec_dma_InvalidConfiguration,   // sanity check of device configuration failed
  ec_dma_InvalidImplementation,  // your code does not behave as expected

  // other failures go here..
  
  ec_dma_unknown                // no valid errorcodes past this entry 
} e_ttc_dma_errorcode;
typedef enum {   // e_ttc_dma_architecture  types of architectures supported by DMA driver
  ta_dma_None,           // no architecture selected
  

  ta_dma_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
//InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)
  
  ta_dma_unknown        // architecture not supported
} e_ttc_dma_architecture;
typedef struct s_ttc_dma_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_dma_init()
    //       and after ttc_dma_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    e_ttc_dma_architecture Architecture; // type of architecture used for current dma device
    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_DMA1, ...)
    t_u8  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)
    
    // low-level configuration (structure defined by low-level driver)
    t_ttc_dma_architecture* LowLevelConfig;      

    t_u32 PeripheralBaseAddr;
    t_u32 MemoryBaseAddr;
    t_u32 DIR;
    t_u32 BufferSize;
    t_u32 PeripheralInc;
    t_u32 MemoryInc;
    t_u32 PeripheralDataSize;
    t_u32 MemoryDataSize;
    t_u32 Mode;
    t_u32 Priority;
    t_u32 M2M;

    t_u8 Channel;
    
    union  { // generic configuration bits common for all low-level Drivers
        t_u16 All;
        struct {
          unsigned Initialized        : 1;  // ==1: device has been initialized successfully
          // ToDo: additional high-level flags go here..
            
            unsigned Reserved1        : 15; // pad to 16 bits
        } Bits;
    } Flags;

    // Additional high-level attributes go here..
    
} __attribute__((__packed__)) t_ttc_dma_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_DMA_TYPES_H
