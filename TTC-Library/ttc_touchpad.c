/** { ttc_touchpad.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for touchpad devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  See corresponding header file for documentation.
 *
 *  Created from template ttc_device.c revision 27 at 20150316 07:48:52 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_touchpad.h".
//
#include "ttc_touchpad.h"
#include "ttc_sysclock.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

//{ Check static configuration

#if TTC_TOUCHPAD_AMOUNT == 0
#warning No TOUCHPAD devices defined, check your makefile! (did you forget to activate something?)
#endif

#ifndef TTC_TOUCH_ROTATION_CLOCKWISE
#  warning  TTC_TOUCH_ROTATION_CLOCKWISE not set, using default direction !
#  define TTC_TOUCH_ROTATION_CLOCKWISE 0
#else
#  if TTC_TOUCH_ROTATION_CLOCKWISE == 0
#    define TTC_TOUCH_ROTATION_CLOCKWISE_OK 1
#  endif
#  if TTC_TOUCH_ROTATION_CLOCKWISE == 90
#    define TTC_TOUCH_ROTATION_CLOCKWISE_OK 1
#  endif
#  if TTC_TOUCH_ROTATION_CLOCKWISE == 180
#    define TTC_TOUCH_ROTATION_CLOCKWISE_OK 1
#  endif
#  if TTC_TOUCH_ROTATION_CLOCKWISE == 270
#    define TTC_TOUCH_ROTATION_CLOCKWISE_OK 1
#  endif
#  ifndef TTC_TOUCH_ROTATION_CLOCKWISE_OK
#    define TTC_TOUCH_ROTATION_CLOCKWISE 0
#    warning Invalid definition of TTC_TOUCH_ROTATION_CLOCKWISE! (only 0,90,180,270 degrees supported)
#  endif
#endif

//}Check static configuration
/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of touchpad devices.
 *
 */


// for each initialized device, a pointer to its generic and analog definitions is stored
A_define( t_ttc_touchpad_config*, ttc_touchpad_configs, TTC_TOUCHPAD_AMOUNT );


//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8 ttc_touchpad_get_max_index() {
    return TTC_TOUCHPAD_AMOUNT;
}
t_ttc_touchpad_config* ttc_touchpad_get_configuration( t_u8 LogicalIndex ) {
    Assert_TOUCHPAD( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_touchpad_config* Config = A( ttc_touchpad_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        Config = A( ttc_touchpad_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_touchpad_config ) );
        Config->Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_touchpad_load_defaults( LogicalIndex );
    }

    return Config;
}
t_ttc_touchpad_config* ttc_touchpad_get_features( t_u8 LogicalIndex ) {
    t_ttc_touchpad_config* Config = ttc_touchpad_get_configuration( LogicalIndex );
    return _driver_touchpad_get_features( Config );
}
void ttc_touchpad_adjust( t_u8 LogicalIndex, t_s16 AdjustX, t_s16 AdjustY ) {
    t_ttc_touchpad_config* Config = ttc_touchpad_get_configuration( LogicalIndex );

    Config->AdjustX = AdjustX;
    Config->AdjustY = AdjustY;
}
void ttc_touchpad_deinit( t_u8 LogicalIndex ) {
    Assert_TOUCHPAD( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_touchpad_config* Config = ttc_touchpad_get_configuration( LogicalIndex );

    e_ttc_touchpad_errorcode Result = _driver_touchpad_deinit( Config );
    if ( Result == ec_touchpad_OK )
    { Config->Flags.Bits.Initialized = 0; }
}
void ttc_touchpad_calibrate( t_u8 LogicalIndex ) {
    t_ttc_touchpad_config* Config = ttc_touchpad_get_configuration( LogicalIndex );

    _driver_touchpad_calibrate( Config );
}
e_ttc_touchpad_errorcode ttc_touchpad_init( t_u8 LogicalIndex ) {
    Assert_TOUCHPAD( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_touchpad_config* Config = ttc_touchpad_get_configuration( LogicalIndex );

    e_ttc_touchpad_errorcode Result = _driver_touchpad_init( Config );
    //? if (Result == ec_touchpad_OK) { ttc_touchpad_calibrate(LogicalIndex); }
    if ( Result == ec_touchpad_OK )   { Config->Flags.Bits.Initialized = 1; }

    return Result;
}
e_ttc_touchpad_errorcode ttc_touchpad_load_defaults( t_u8 LogicalIndex ) {
    Assert_TOUCHPAD( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_touchpad_config* Config = ttc_touchpad_get_configuration( LogicalIndex );

    if ( Config->Flags.Bits.Initialized )
    { ttc_touchpad_deinit( LogicalIndex ); }

    ttc_memory_set( Config, 0, sizeof( t_ttc_touchpad_config ) );

    // load generic default values
    switch ( LogicalIndex ) { // load type of low-level driver for current architecture
            #ifdef TTC_TOUCHPAD1
        case  1: Config->Architecture = TTC_TOUCHPAD1; break;
            #endif
            #ifdef TTC_TOUCHPAD2
        case  2: Config->Architecture = TTC_TOUCHPAD2; break;
            #endif
            #ifdef TTC_TOUCHPAD3
        case  3: Config->Architecture = TTC_TOUCHPAD3; break;
            #endif
            #ifdef TTC_TOUCHPAD4
        case  4: Config->Architecture = TTC_TOUCHPAD4; break;
            #endif
            #ifdef TTC_TOUCHPAD5
        case  5: Config->Architecture = TTC_TOUCHPAD5; break;
            #endif
            #ifdef TTC_TOUCHPAD6
        case  6: Config->Architecture = TTC_TOUCHPAD6; break;
            #endif
            #ifdef TTC_TOUCHPAD7
        case  7: Config->Architecture = TTC_TOUCHPAD7; break;
            #endif
            #ifdef TTC_TOUCHPAD8
        case  8: Config->Architecture = TTC_TOUCHPAD8; break;
            #endif
            #ifdef TTC_TOUCHPAD9
        case  9: Config->Architecture = TTC_TOUCHPAD9; break;
            #endif
            #ifdef TTC_TOUCHPAD10
        case 10: Config->Architecture = TTC_TOUCHPAD10; break;
            #endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }
    Assert_TOUCHPAD( ( Config->Architecture > ta_touchpad_None ) && ( Config->Architecture < ta_touchpad_unknown ), ttc_assert_origin_auto ); // architecture not set properly! Check makefile for TTC_TOUCHPAD<n>_DRIVER and compare with e_ttc_touchpad_architecture

    Config->LogicalIndex = LogicalIndex;

    //Insert additional generic default values here ...

    #if TTC_TOUCH_ROTATION_CLOCKWISE ==0
    Config->Max_X          = 240;
    Config->Max_Y          = 320;
    #endif
    #if TTC_TOUCH_ROTATION_CLOCKWISE ==90
    Config->Max_X          = 240;
    Config->Max_Y          = 320;
    #endif
    #if TTC_TOUCH_ROTATION_CLOCKWISE ==180
    Config->Max_X          = 240;
    Config->Max_Y          = 320;
    #endif
    #if TTC_TOUCH_ROTATION_CLOCKWISE ==270
    Config->Max_X          = 320;
    Config->Max_Y          = 240;
    #endif

    e_ttc_touchpad_errorcode Error = _driver_touchpad_load_defaults( Config );

    if ( !Config->Amount_Rereads )
    { Config->Amount_Rereads = 4; }

    return Error;
}
void ttc_touchpad_prepare() {
    // add your startup code here (Singletasking!)
    _driver_touchpad_prepare();
}
e_ttc_touchpad_update ttc_touchpad_check( t_u8 LogicalIndex ) {
    t_ttc_touchpad_config* Config = ttc_touchpad_get_configuration( LogicalIndex );

    // will update Config->Pressure, Config->Position_X, Config->Position_Y
    _driver_touchpad_check( Config );

    #if TTC_TOUCH_ROTATION_CLOCKWISE ==0
    Config->Rotated_X = Config->Position_X;
    Config->Rotated_Y = Config->Position_Y;
    #endif
    #if TTC_TOUCH_ROTATION_CLOCKWISE ==90
    Config->Rotated_X = ROTATED_TOUCH_X( Config->Position_X, Config->Position_Y );
    Config->Rotated_Y = ROTATED_TOUCH_Y( Config->Position_X, Config->Position_Y );
    #endif
    #if TTC_TOUCH_ROTATION_CLOCKWISE ==180
    Config->Rotated_X = ROTATED_TOUCH_X( Config->Position_X, Config->Position_Y );
    Config->Rotated_Y = ROTATED_TOUCH_Y( Config->Position_X, Config->Position_Y );
    #endif
    #if TTC_TOUCH_ROTATION_CLOCKWISE ==270
    Config->Rotated_X = ROTATED_TOUCH_X( Config->Position_X, Config->Position_Y );
    Config->Rotated_Y = ROTATED_TOUCH_Y( Config->Position_X, Config->Position_Y );
    #endif

    e_ttc_touchpad_update Update = ttu_None;
    Config->Flags.Bits.ContactLost = 0;

    if ( Config->Pressure ) {
        Config->Position_X += Config->AdjustX;
        Config->Position_Y += Config->AdjustY;
        #if TTC_TOUCH_ROTATION_CLOCKWISE ==0
        Config->Rotated_X = Config->Position_X;
        Config->Rotated_Y = Config->Position_Y;
        #endif
        #if TTC_TOUCH_ROTATION_CLOCKWISE ==90
        Config->Rotated_X = ROTATED_TOUCH_X( Config->Position_X, Config->Position_Y );
        Config->Rotated_Y = ROTATED_TOUCH_Y( Config->Position_X, Config->Position_Y );
        #endif
        #if TTC_TOUCH_ROTATION_CLOCKWISE ==180
        Config->Rotated_X = ROTATED_TOUCH_X( Config->Position_X, Config->Position_Y );
        Config->Rotated_Y = ROTATED_TOUCH_Y( Config->Position_X, Config->Position_Y );
        #endif
        #if TTC_TOUCH_ROTATION_CLOCKWISE ==270
        Config->Rotated_X = ROTATED_TOUCH_X( Config->Position_X, Config->Position_Y );
        Config->Rotated_Y = ROTATED_TOUCH_Y( Config->Position_X, Config->Position_Y );
        #endif
        if ( !Config->Pressure_Previous ) { // contact established
            if ( Config->Pressure_Counter < Config->Amount_Rereads )
            { Config->Pressure_Counter++; }
            else { // contact confirmed (established for a while)
                Config->Pressure_Previous = Config->Pressure;
                Config->Pressure_Counter = 0; // reset counter (will be reused to detect contact loss)
                Update = ttu_TouchIn;
            }
        }
        else { // finger is moving on touchpad
            Update = ttu_TouchMove;
        }
    }
    else {
        if ( Config->Pressure_Previous ) { // about to loose contact
            if ( Config->Pressure_Counter < Config->Amount_Rereads )
            { Config->Pressure_Counter++; }
            else { // contact lost (about to loose for a while)
                Config->Pressure_Previous = Config->Pressure;
                Update = ttu_TouchOut;
                Config->Flags.Bits.ContactLost = 1;
            }
        }
        else
        { Config->Pressure_Counter = 0; } // reset counter (will be reused to detect contact confirmation)
    }
    if ( Update ) { // got position update: ensure coordinates are within limits

        if ( Config->Position_X > Config->Max_X )
        { Config->Position_X = Config->Max_X; }

        if ( Config->Position_Y > Config->Max_Y )
        { Config->Position_Y = Config->Max_Y; }

        #if TTC_TOUCH_ROTATION_CLOCKWISE ==0
        Config->Rotated_X = Config->Position_X;
        Config->Rotated_Y = Config->Position_Y;
        #endif
        #if TTC_TOUCH_ROTATION_CLOCKWISE ==90
        Config->Rotated_X = ROTATED_TOUCH_X( Config->Position_X, Config->Position_Y );
        Config->Rotated_Y = ROTATED_TOUCH_Y( Config->Position_X, Config->Position_Y );
        #endif
        #if TTC_TOUCH_ROTATION_CLOCKWISE ==180
        Config->Rotated_X = ROTATED_TOUCH_X( Config->Position_X, Config->Position_Y );
        Config->Rotated_Y = ROTATED_TOUCH_Y( Config->Position_X, Config->Position_Y );
        #endif
        #if TTC_TOUCH_ROTATION_CLOCKWISE ==270
        Config->Rotated_X = ROTATED_TOUCH_X( Config->Position_X, Config->Position_Y );
        Config->Rotated_Y = ROTATED_TOUCH_Y( Config->Position_X, Config->Position_Y );
        #endif
    }


    return Update;
}
void ttc_touchpad_reset( t_u8 LogicalIndex ) {
    Assert_TOUCHPAD( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_touchpad_config* Config = ttc_touchpad_get_configuration( LogicalIndex );

    _driver_touchpad_reset( Config );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_touchpad(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
