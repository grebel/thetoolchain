/** { ttc_gpio_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for GPIO device.
 *
 *  Structures, Enums and Defines being required by both, high- and low-level gpio.
 *
 * Authors: Gregor Rebel
 *
}*/

#ifndef TTC_GPIO_TYPES_H
#define TTC_GPIO_TYPES_H

//{ Enums/ Structures *****************************************
typedef enum {   // e_ttc_gpio_mode         operation mode of single pin
    E_ttc_gpio_mode_none,                                             // 0
    E_ttc_gpio_mode_analog_in,                                        // Analog input to ADC
    E_ttc_gpio_mode_input_floating,                                   // Digital input, High-Z
    E_ttc_gpio_mode_input_pull_down,                                  // Digital input, resistor to GND active
    E_ttc_gpio_mode_input_pull_up,                                    // Digital input, resistor to VCC active
    E_ttc_gpio_mode_output_open_drain,                                // Digital output, can only pull to GND (to low)
    E_ttc_gpio_mode_output_open_drain_pull_up_resistor,               // Digital open drain with pull up resistor (STM32L1xx)
    E_ttc_gpio_mode_output_open_drain_pull_down_resistor,             // Digital open drain with pull down resistor (STM32L1xx)
    E_ttc_gpio_mode_output_push_pull,                                 // Digital output, can pull to GND and VCC (to high and low)
    E_ttc_gpio_mode_output_push_pull_pull_up_resistor,                // Digital push pull with pull up resistor (STM32L1xx)
    E_ttc_gpio_mode_output_push_pull_pull_down_resistor,              // Digital push pull with pull down resistor (STM32L1xx)
    E_ttc_gpio_mode_alternate_function_push_pull,                     // Digital output from functional unit other than GPIO
    E_ttc_gpio_mode_alternate_function_push_pull_pull_up_resistor,    // push pull function with pull up resistor (STM32L1xx)
    E_ttc_gpio_mode_alternate_function_push_pull_pull_down_resistor,  // push pull function with pull up resistor (STM32L1xx)
    E_ttc_gpio_mode_alternate_function_open_drain,                    // Digital output from functional unit other than GPIO
    E_ttc_gpio_mode_alternate_function_open_drain_pull_up_resistor,   // open drain function with pull up resistor (STM32L1xx)
    E_ttc_gpio_mode_alternate_function_open_drain_pull_down_resistor, // open drain function with pull down resistor (STM32L1xx)
    E_ttc_gpio_mode_alternate_function_special_sclk,                  // Special output used by stm32w
    E_ttc_gpio_mode_unknown                                           // Error condition
} e_ttc_gpio_mode;
typedef enum {   // e_ttc_gpio_speed        gpio-speeds supported by different architectures (update for new architectures!)
    E_ttc_gpio_speed_none,
    E_ttc_gpio_speed_min,           // supported by: all
    E_ttc_gpio_speed_400khz,        // supported by: stm32l1xx
    E_ttc_gpio_speed_2mhz,          // supported by: stm32f1xx, stm32l1xx
    E_ttc_gpio_speed_10mhz,         // supported by: stm32f1xx, stm32l1xx
    E_ttc_gpio_speed_40mhz,         // supported by: stm32l1xx
    E_ttc_gpio_speed_50mhz,         // supported by: stm32f1xx
    E_ttc_gpio_speed_max,           // supported by: all
    E_ttc_gpio_speed_unknown        // must be last entry!
} e_ttc_gpio_speed;
typedef enum {    // e_ttc_gpio_error     return codes of GPIO devices
    E_ttc_gpio_error_ok = 0,

    // other warnings go here..

    E_ttc_gpio_error_unknown,           // general failure
    E_ttc_gpio_error_devicenotfound,
    E_ttc_gpio_error_invalidimplementation,
    E_ttc_gpio_error_notimplemented

    // other failures go here..
} e_ttc_gpio_error;
typedef enum {    // e_ttc_gpio_architecture  types of architectures supported by GPIO driver
    E_ttc_gpio_architecture_none,           // no architecture selected

    E_ttc_gpio_architecture_stm32w1xx, // automatically added by ./create_DeviceDriver.pl
    E_ttc_gpio_architecture_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
    E_ttc_gpio_architecture_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
    E_ttc_gpio_architecture_stm32f30x, // automatically added by ./create_DeviceDriver.pl
    E_ttc_gpio_architecture_stm32l0xx, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    E_ttc_gpio_architecture_error           // architecture not supported
} e_ttc_gpio_architecture;

//} Enums/ Structures
//{ Includes *************************************************************

#include "compile_options.h"
#include "ttc_basic_types.h"

#ifdef EXTENSION_gpio_stm32f1xx
    #include "gpio/gpio_stm32f1xx_types.h"
#endif
#ifdef EXTENSION_gpio_stm32l1xx
    #include "gpio/gpio_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_gpio_stm32w1xx
    #include "gpio/gpio_stm32w1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_gpio_stm32f30x
    #include "gpio/gpio_stm32f30x_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_gpio_stm32l0xx
    #include "gpio/gpio_stm32l0xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

// Set defaults for definitions missing in low-level driver
#ifndef t_ttc_gpio_register
    #warning Missing low-level driver definition for t_ttc_gpio_register! Did you forget to call activate.500_ttc_gpio.sh?
    #define t_ttc_gpio_register void*
#endif
#ifndef e_ttc_gpio_alternate_function
    #warning Missing low-level driver definition for e_ttc_gpio_alternate_function! Check implementation of your low-level driver?
    #define e_ttc_gpio_alternate_function t_u8
#endif
#ifndef e_ttc_gpio_pin
    #warning Missing low-level driver definition for e_ttc_gpio_pin! Check implementation of your low-level driver?
    #define e_ttc_gpio_pin t_u8
#endif
#ifndef t_ttc_gpio_values
    #warning Missing low-level driver definition for t_ttc_gpio_values! Check implementation of your low-level driver?
    #define t_ttc_gpio_values t_u8
#endif

//} Includes
//{ Enums/ Structures 2 ***************************************

typedef struct s_ttc_gpio_config { //         architecture independent configuration data

    e_ttc_gpio_architecture       Architecture;      // type of architecture used for current gpio device
    e_ttc_gpio_pin                PhysicalIndex;     // automatically set: physical index of device to use (0 = first hardware device, ...)
    e_ttc_gpio_mode               Mode;              // pin configuration mode
    e_ttc_gpio_speed              Speed;             // configured speed of this pin
    e_ttc_gpio_alternate_function AlternateFunction; // GPIOs may be internally multiplexed to alternate functions
    t_ttc_gpio_register           Register;          // hardware registers of GPIO (structure known by low-level driver only)
    t_ttc_gpio_values             Values;            // architecture specific port setting values
    BOOL                          State;             // latest state reading of gpio pin
    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_gpio_config;

//} Enums/ Structures2
//{ Defines ***************************************************

#ifndef TTC_GPIO_MAX_PINS
    #warning Missing architecture define for TTC_GPIO_MAX_PINS (using default)
    #define TTC_GPIO_MAX_PINS 16
#endif


/** Pin definition shortcuts
 *
 * Makefiles may use TTC_GPIO_xn shortcuts to define pin configurations.
 * E.g.:
 * COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE=TTC_GPIO_C10
 *
 * Each low-level driver must define TTC_GPIO_BANK_A .. TTC_GPIO_BANK_x
 * and all E_ttc_gpio_pin_xn (x = a..g, n = 0..15)
 */
#ifdef TTC_GPIO_BANK_A //{ Define shortcuts for all pins of GPIO Bank A
    #define TTC_GPIO_A0  E_ttc_gpio_pin_a0
    #define TTC_GPIO_A1  E_ttc_gpio_pin_a1
    #define TTC_GPIO_A2  E_ttc_gpio_pin_a2
    #define TTC_GPIO_A3  E_ttc_gpio_pin_a3
    #define TTC_GPIO_A4  E_ttc_gpio_pin_a4
    #define TTC_GPIO_A5  E_ttc_gpio_pin_a5
    #define TTC_GPIO_A6  E_ttc_gpio_pin_a6
    #define TTC_GPIO_A7  E_ttc_gpio_pin_a7

    #if TTC_GPIO_MAX_PINS == 16
        #define TTC_GPIO_A8  E_ttc_gpio_pin_a8
        #define TTC_GPIO_A9  E_ttc_gpio_pin_a9
        #define TTC_GPIO_A10 E_ttc_gpio_pin_a10
        #define TTC_GPIO_A11 E_ttc_gpio_pin_a11
        #define TTC_GPIO_A12 E_ttc_gpio_pin_a12
        #define TTC_GPIO_A13 E_ttc_gpio_pin_a13
        #define TTC_GPIO_A14 E_ttc_gpio_pin_a14
        #define TTC_GPIO_A15 E_ttc_gpio_pin_a15
    #endif

    #undef TTC_GPIO_MAX_AMOUNT
    #define TTC_GPIO_MAX_AMOUNT 1
#else
    //#  error Missing definition TTC_GPIO_BANK_A (Has to be defined by low-level driver)!
#endif //}BankA
#ifdef TTC_GPIO_BANK_B //{ Define shortcuts for all pins of GPIO Bank B
    #ifndef TTC_GPIO_BANK_A
        #  error Missing definition TTC_GPIO_BANK_A (Has to be defined by low-level driver)!
    #endif

    #define TTC_GPIO_B0  E_ttc_gpio_pin_b0
    #define TTC_GPIO_B1  E_ttc_gpio_pin_b1
    #define TTC_GPIO_B2  E_ttc_gpio_pin_b2
    #define TTC_GPIO_B3  E_ttc_gpio_pin_b3
    #define TTC_GPIO_B4  E_ttc_gpio_pin_b4
    #define TTC_GPIO_B5  E_ttc_gpio_pin_b5
    #define TTC_GPIO_B6  E_ttc_gpio_pin_b6
    #define TTC_GPIO_B7  E_ttc_gpio_pin_b7

    #if TTC_GPIO_MAX_PINS == 16
        #define TTC_GPIO_B8  E_ttc_gpio_pin_b8
        #define TTC_GPIO_B9  E_ttc_gpio_pin_b9
        #define TTC_GPIO_B10 E_ttc_gpio_pin_b10
        #define TTC_GPIO_B11 E_ttc_gpio_pin_b11
        #define TTC_GPIO_B12 E_ttc_gpio_pin_b12
        #define TTC_GPIO_B13 E_ttc_gpio_pin_b13
        #define TTC_GPIO_B14 E_ttc_gpio_pin_b14
        #define TTC_GPIO_B15 E_ttc_gpio_pin_b15
    #endif

    #undef TTC_GPIO_MAX_AMOUNT
    #define TTC_GPIO_MAX_AMOUNT 2
#endif //}BankB
#ifdef TTC_GPIO_BANK_C //{ Define shortcuts for all pins of GPIO Bank C
    #ifndef TTC_GPIO_BANK_B
        #  error Missing definition TTC_GPIO_BANK_B (Has to be defined by low-level driver)!
    #endif

    #define TTC_GPIO_C0  E_ttc_gpio_pin_c0
    #define TTC_GPIO_C1  E_ttc_gpio_pin_c1
    #define TTC_GPIO_C2  E_ttc_gpio_pin_c2
    #define TTC_GPIO_C3  E_ttc_gpio_pin_c3
    #define TTC_GPIO_C4  E_ttc_gpio_pin_c4
    #define TTC_GPIO_C5  E_ttc_gpio_pin_c5
    #define TTC_GPIO_C6  E_ttc_gpio_pin_c6
    #define TTC_GPIO_C7  E_ttc_gpio_pin_c7

    #if TTC_GPIO_MAX_PINS == 16
        #define TTC_GPIO_C8  E_ttc_gpio_pin_c8
        #define TTC_GPIO_C9  E_ttc_gpio_pin_c9
        #define TTC_GPIO_C10 E_ttc_gpio_pin_c10
        #define TTC_GPIO_C11 E_ttc_gpio_pin_c11
        #define TTC_GPIO_C12 E_ttc_gpio_pin_c12
        #define TTC_GPIO_C13 E_ttc_gpio_pin_c13
        #define TTC_GPIO_C14 E_ttc_gpio_pin_c14
        #define TTC_GPIO_C15 E_ttc_gpio_pin_c15
    #endif

    #undef TTC_GPIO_MAX_AMOUNT
    #define TTC_GPIO_MAX_AMOUNT 3
#endif //}BankC
#ifdef TTC_GPIO_BANK_D //{ Define shortcuts for all pins of GPIO Bank D
    #ifndef TTC_GPIO_BANK_C
        #  error Missing definition TTC_GPIO_BANK_C (Has to be defined by low-level driver)!
    #endif

    #define TTC_GPIO_D0  E_ttc_gpio_pin_d0
    #define TTC_GPIO_D1  E_ttc_gpio_pin_d1
    #define TTC_GPIO_D2  E_ttc_gpio_pin_d2
    #define TTC_GPIO_D3  E_ttc_gpio_pin_d3
    #define TTC_GPIO_D4  E_ttc_gpio_pin_d4
    #define TTC_GPIO_D5  E_ttc_gpio_pin_d5
    #define TTC_GPIO_D6  E_ttc_gpio_pin_d6
    #define TTC_GPIO_D7  E_ttc_gpio_pin_d7
    #define TTC_GPIO_D8  E_ttc_gpio_pin_d8

    #if TTC_GPIO_MAX_PINS == 16
        #define TTC_GPIO_D9  E_ttc_gpio_pin_d9
        #define TTC_GPIO_D10 E_ttc_gpio_pin_d10
        #define TTC_GPIO_D11 E_ttc_gpio_pin_d11
        #define TTC_GPIO_D12 E_ttc_gpio_pin_d12
        #define TTC_GPIO_D13 E_ttc_gpio_pin_d13
        #define TTC_GPIO_D14 E_ttc_gpio_pin_d14
        #define TTC_GPIO_D15 E_ttc_gpio_pin_d15
    #endif

    #undef TTC_GPIO_MAX_AMOUNT
    #define TTC_GPIO_MAX_AMOUNT 4
#endif //}BankD
#ifdef TTC_GPIO_BANK_E //{ Define shortcuts for all pins of GPIO Bank E
    #ifndef TTC_GPIO_BANK_D
        #  error Missing definition TTC_GPIO_BANK_D (Has to be defined by low-level driver)!
    #endif

    #define TTC_GPIO_E0  E_ttc_gpio_pin_e0
    #define TTC_GPIO_E1  E_ttc_gpio_pin_e1
    #define TTC_GPIO_E2  E_ttc_gpio_pin_e2
    #define TTC_GPIO_E3  E_ttc_gpio_pin_e3
    #define TTC_GPIO_E4  E_ttc_gpio_pin_e4
    #define TTC_GPIO_E5  E_ttc_gpio_pin_e5
    #define TTC_GPIO_E6  E_ttc_gpio_pin_e6
    #define TTC_GPIO_E7  E_ttc_gpio_pin_e7

    #if TTC_GPIO_MAX_PINS == 16
        #define TTC_GPIO_E8  E_ttc_gpio_pin_e8
        #define TTC_GPIO_E9  E_ttc_gpio_pin_e9
        #define TTC_GPIO_E10 E_ttc_gpio_pin_e10
        #define TTC_GPIO_E11 E_ttc_gpio_pin_e11
        #define TTC_GPIO_E12 E_ttc_gpio_pin_e12
        #define TTC_GPIO_E13 E_ttc_gpio_pin_e13
        #define TTC_GPIO_E14 E_ttc_gpio_pin_e14
        #define TTC_GPIO_E15 E_ttc_gpio_pin_e15
    #endif

    #undef TTC_GPIO_MAX_AMOUNT
    #define TTC_GPIO_MAX_AMOUNT 5
#endif //}BankE
#ifdef TTC_GPIO_BANK_F //{ Define shortcuts for all pins of GPIO Bank F
    #ifndef TTC_GPIO_BANK_E
        #  error Missing definition TTC_GPIO_BANK_E (Has to be defined by low-level driver)!
    #endif
    #define TTC_GPIO_F0  E_ttc_gpio_pin_f0
    #define TTC_GPIO_F1  E_ttc_gpio_pin_f1
    #define TTC_GPIO_F2  E_ttc_gpio_pin_f2
    #define TTC_GPIO_F3  E_ttc_gpio_pin_f3
    #define TTC_GPIO_F4  E_ttc_gpio_pin_f4
    #define TTC_GPIO_F5  E_ttc_gpio_pin_f5
    #define TTC_GPIO_F6  E_ttc_gpio_pin_f6
    #define TTC_GPIO_F7  E_ttc_gpio_pin_f7

    #if TTC_GPIO_MAX_PINS == 16
        #define TTC_GPIO_F8  E_ttc_gpio_pin_f8
        #define TTC_GPIO_F9  E_ttc_gpio_pin_f9
        #define TTC_GPIO_F10 E_ttc_gpio_pin_f10
        #define TTC_GPIO_F11 E_ttc_gpio_pin_f11
        #define TTC_GPIO_F12 E_ttc_gpio_pin_f12
        #define TTC_GPIO_F13 E_ttc_gpio_pin_f13
        #define TTC_GPIO_F14 E_ttc_gpio_pin_f14
        #define TTC_GPIO_F15 E_ttc_gpio_pin_f15
    #endif

    #undef TTC_GPIO_MAX_AMOUNT
    #define TTC_GPIO_MAX_AMOUNT 6
#endif //}BankF
#ifdef TTC_GPIO_BANK_G //{ Define shortcuts for all pins of GPIO Bank G
    #ifndef TTC_GPIO_BANK_F
        #  error Missing definition TTC_GPIO_BANK_F (Has to be defined by low-level driver)!
    #endif

    #define TTC_GPIO_G0  E_ttc_gpio_pin_g0
    #define TTC_GPIO_G1  E_ttc_gpio_pin_g1
    #define TTC_GPIO_G2  E_ttc_gpio_pin_g2
    #define TTC_GPIO_G3  E_ttc_gpio_pin_g3
    #define TTC_GPIO_G4  E_ttc_gpio_pin_g4
    #define TTC_GPIO_G5  E_ttc_gpio_pin_g5
    #define TTC_GPIO_G6  E_ttc_gpio_pin_g6
    #define TTC_GPIO_G7  E_ttc_gpio_pin_g7

    #if TTC_GPIO_MAX_PINS == 16
        #define TTC_GPIO_G8  E_ttc_gpio_pin_g8
        #define TTC_GPIO_G9  E_ttc_gpio_pin_g9
        #define TTC_GPIO_G10 E_ttc_gpio_pin_g10
        #define TTC_GPIO_G11 E_ttc_gpio_pin_g11
        #define TTC_GPIO_G12 E_ttc_gpio_pin_g12
        #define TTC_GPIO_G13 E_ttc_gpio_pin_g13
        #define TTC_GPIO_G14 E_ttc_gpio_pin_g14
        #define TTC_GPIO_G15 E_ttc_gpio_pin_g15
    #endif

    #undef TTC_GPIO_MAX_AMOUNT
    #define TTC_GPIO_MAX_AMOUNT 7
    //}BankG
#endif

//{ count amount of configured parallel ports
#ifdef TTC_GPIO_PARALLEL08_1_BANK
    #ifdef TTC_GPIO_PARALLEL08_2_BANK
        #ifdef TTC_GPIO_PARALLEL08_3_BANK
            #ifdef TTC_GPIO_PARALLEL08_4_BANK
                #ifdef TTC_GPIO_PARALLEL08_5_BANK
                    #ifdef TTC_GPIO_PARALLEL08_6_BANK
                        #ifdef TTC_GPIO_PARALLEL08_7_BANK
                            #ifdef TTC_GPIO_PARALLEL08_8_BANK
                                #ifdef TTC_GPIO_PARALLEL08_9_BANK
                                    #ifdef TTC_GPIO_PARALLEL08_10_BANK
                                        #define TTC_GPIO_PARALLEL08_AMOUNT 10
                                    #else
                                        #define TTC_GPIO_PARALLEL08_AMOUNT 9
                                    #endif
                                #else
                                    #define TTC_GPIO_PARALLEL08_AMOUNT 8
                                #endif
                            #else
                                #define TTC_GPIO_PARALLEL08_AMOUNT 7
                            #endif
                        #else
                            #define TTC_GPIO_PARALLEL08_AMOUNT 6
                        #endif
                    #else
                        #define TTC_GPIO_PARALLEL08_AMOUNT 5
                    #endif
                #else
                    #define TTC_GPIO_PARALLEL08_AMOUNT 4
                #endif
            #else
                #define TTC_GPIO_PARALLEL08_AMOUNT 3
            #endif
        #else
            #define TTC_GPIO_PARALLEL08_AMOUNT 2
        #endif
    #else
        #define TTC_GPIO_PARALLEL08_AMOUNT 1
    #endif
#else
    #define TTC_GPIO_PARALLEL08_AMOUNT 0
#endif //}

// check data of all configured 8 bit parallel ports
#ifdef TTC_GPIO_PARALLEL08_1_BANK //{
    #ifndef TTC_GPIO_PARALLEL08_1_FIRSTPIN
        #    error Missing definition TTC_GPIO_PARALLEL08_1_FIRSTPIN. Define as index (0..8) of first gpio pin to use as lowest significant bit (LSB)!
    #else
        #if (TTC_GPIO_PARALLEL08_1_FIRSTPIN < 0) || (TTC_GPIO_PARALLEL08_1_FIRSTPIN > 8)
            #      error Invalid value given for TTC_GPIO_PARALLEL08_1_FIRSTPIN. Allowed values: 0..8
        #endif
    #endif
#endif
//}
#ifdef TTC_GPIO_PARALLEL08_2_BANK //{
    #ifndef TTC_GPIO_PARALLEL08_2_FIRSTPIN
        #    error Missing definition TTC_GPIO_PARALLEL08_2_FIRSTPIN. Define as index (0..8) of first gpio pin to use as lowest significant bit (LSB)!
    #else
        #if (TTC_GPIO_PARALLEL08_2_FIRSTPIN < 0) || (TTC_GPIO_PARALLEL08_2_FIRSTPIN > 8)
            #      error Invalid value given for TTC_GPIO_PARALLEL08_2_FIRSTPIN. Allowed values: 0..8
        #endif
    #endif
#endif
//}
#ifdef TTC_GPIO_PARALLEL08_3_BANK //{
    #ifndef TTC_GPIO_PARALLEL08_3_FIRSTPIN
        #    error Missing definition TTC_GPIO_PARALLEL08_3_FIRSTPIN. Define as index (0..8) of first gpio pin to use as lowest significant bit (LSB)!
    #else
        #if (TTC_GPIO_PARALLEL08_3_FIRSTPIN < 0) || (TTC_GPIO_PARALLEL08_3_FIRSTPIN > 8)
            #      error Invalid value given for TTC_GPIO_PARALLEL08_3_FIRSTPIN. Allowed values: 0..8
        #endif
    #endif
#endif
//}
#ifdef TTC_GPIO_PARALLEL08_4_BANK //{
    #ifndef TTC_GPIO_PARALLEL08_4_FIRSTPIN
        #    error Missing definition TTC_GPIO_PARALLEL08_4_FIRSTPIN. Define as index (0..8) of first gpio pin to use as lowest significant bit (LSB)!
    #else
        #if (TTC_GPIO_PARALLEL08_4_FIRSTPIN < 0) || (TTC_GPIO_PARALLEL08_4_FIRSTPIN > 8)
            #      error Invalid value given for TTC_GPIO_PARALLEL08_4_FIRSTPIN. Allowed values: 0..8
        #endif
    #endif
#endif
//}
#ifdef TTC_GPIO_PARALLEL08_5_BANK //{
    #ifndef TTC_GPIO_PARALLEL08_5_FIRSTPIN
        #    error Missing definition TTC_GPIO_PARALLEL08_5_FIRSTPIN. Define as index (0..8) of first gpio pin to use as lowest significant bit (LSB)!
    #else
        #if (TTC_GPIO_PARALLEL08_5_FIRSTPIN < 0) || (TTC_GPIO_PARALLEL08_5_FIRSTPIN > 8)
            #      error Invalid value given for TTC_GPIO_PARALLEL08_5_FIRSTPIN. Allowed values: 0..8
        #endif
    #endif
#endif
//}
#ifdef TTC_GPIO_PARALLEL08_6_BANK //{
    #ifndef TTC_GPIO_PARALLEL08_6_FIRSTPIN
        #    error Missing definition TTC_GPIO_PARALLEL08_6_FIRSTPIN. Define as index (0..8) of first gpio pin to use as lowest significant bit (LSB)!
    #else
        #if (TTC_GPIO_PARALLEL08_6_FIRSTPIN < 0) || (TTC_GPIO_PARALLEL08_6_FIRSTPIN > 8)
            #      error Invalid value given for TTC_GPIO_PARALLEL08_6_FIRSTPIN. Allowed values: 0..8
        #endif
    #endif
#endif
//}
#ifdef TTC_GPIO_PARALLEL08_7_BANK //{
    #ifndef TTC_GPIO_PARALLEL08_7_FIRSTPIN
        #    error Missing definition TTC_GPIO_PARALLEL08_7_FIRSTPIN. Define as index (0..8) of first gpio pin to use as lowest significant bit (LSB)!
    #else
        #if (TTC_GPIO_PARALLEL08_7_FIRSTPIN < 0) || (TTC_GPIO_PARALLEL08_7_FIRSTPIN > 8)
            #      error Invalid value given for TTC_GPIO_PARALLEL08_7_FIRSTPIN. Allowed values: 0..8
        #endif
    #endif
#endif
//}
#ifdef TTC_GPIO_PARALLEL08_8_BANK //{
    #ifndef TTC_GPIO_PARALLEL08_8_FIRSTPIN
        #    error Missing definition TTC_GPIO_PARALLEL08_8_FIRSTPIN. Define as index (0..8) of first gpio pin to use as lowest significant bit (LSB)!
    #else
        #if (TTC_GPIO_PARALLEL08_8_FIRSTPIN < 0) || (TTC_GPIO_PARALLEL08_8_FIRSTPIN > 8)
            #      error Invalid value given for TTC_GPIO_PARALLEL08_8_FIRSTPIN. Allowed values: 0..8
        #endif
    #endif
#endif
//}
#ifdef TTC_GPIO_PARALLEL08_9_BANK //{
    #ifndef TTC_GPIO_PARALLEL08_9_FIRSTPIN
        #    error Missing definition TTC_GPIO_PARALLEL08_9_FIRSTPIN. Define as index (0..8) of first gpio pin to use as lowest significant bit (LSB)!
    #else
        #if (TTC_GPIO_PARALLEL08_9_FIRSTPIN < 0) || (TTC_GPIO_PARALLEL08_9_FIRSTPIN > 8)
            #      error Invalid value given for TTC_GPIO_PARALLEL08_9_FIRSTPIN. Allowed values: 0..8
        #endif
    #endif
#endif
//}
#ifdef TTC_GPIO_PARALLEL08_10_BANK //{
    #ifndef TTC_GPIO_PARALLEL08_110FIRSTPIN
        #    error Missing definition TTC_GPIO_PARALLEL08_1_10IRSTPIN. Define as index (0..8) of first gpio pin to use as lowest significant bit (LSB)!
    #else
        #if (TTC_GPIO_PARALLEL08_10_FIRSTPIN < 0) || (TTC_GPIO_PARALLEL08_10_FIRSTPIN > 8)
            #      error Invalid value given for TTC_GPIO_PARALLEL08_10_FIRSTPIN. Allowed values: 0..8
        #endif
    #endif
#endif
//}

#if TTC_GPIO_PARALLEL08_AMOUNT == 1 //{ ensure that no higher parallel port is configured
    #ifdef TTC_GPIO_PARALLEL08_2_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_2_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_3_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_3_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_4_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_4_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_5_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_5_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_6_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_6_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_7_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_7_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_8_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_8_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_9_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_9_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_10_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_10_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

#endif //}
#if TTC_GPIO_PARALLEL08_AMOUNT == 2 //{ ensure that no higher parallel port is configured

    #ifdef TTC_GPIO_PARALLEL08_3_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_3_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_4_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_4_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_5_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_5_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_6_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_6_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_7_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_7_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_8_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_8_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_9_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_9_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_10_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_10_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

#endif //}
#if TTC_GPIO_PARALLEL08_AMOUNT == 3 //{ ensure that no higher parallel port is configured

    #ifdef TTC_GPIO_PARALLEL08_4_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_4_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_5_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_5_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_6_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_6_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_7_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_7_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_8_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_8_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_9_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_9_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_10_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_10_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

#endif //}
#if TTC_GPIO_PARALLEL08_AMOUNT == 4 //{ ensure that no higher parallel port is configured

    #ifdef TTC_GPIO_PARALLEL08_5_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_5_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_6_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_6_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_7_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_7_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_8_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_8_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_9_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_9_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_10_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_10_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

#endif //}
#if TTC_GPIO_PARALLEL08_AMOUNT == 5 //{ ensure that no higher parallel port is configured

    #ifdef TTC_GPIO_PARALLEL08_6_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_6_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_7_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_7_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_8_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_8_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_9_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_9_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_10_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_10_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

#endif //}
#if TTC_GPIO_PARALLEL08_AMOUNT == 6 //{ ensure that no higher parallel port is configured

    #ifdef TTC_GPIO_PARALLEL08_7_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_7_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_8_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_8_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_9_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_9_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_10_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_10_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

#endif //}
#if TTC_GPIO_PARALLEL08_AMOUNT == 7 //{ ensure that no higher parallel port is configured

    #ifdef TTC_GPIO_PARALLEL08_8_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_8_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_9_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_9_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_10_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_10_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

#endif //}
#if TTC_GPIO_PARALLEL08_AMOUNT == 8 //{ ensure that no higher parallel port is configured

    #ifdef TTC_GPIO_PARALLEL08_9_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_9_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

    #ifdef TTC_GPIO_PARALLEL08_10_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_10_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

#endif //}
#if TTC_GPIO_PARALLEL08_AMOUNT == 9 //{ ensure that no higher parallel port is configured

    #ifdef TTC_GPIO_PARALLEL08_10_BANK
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif
    #ifdef TTC_GPIO_PARALLEL08_10_FIRSTPIN
        #    error Invalid parallel port configuration. Parallelports must be configured canonical (1,2,3,4,..) without holes. Check your makefiles!
    #endif

#endif //}



//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration **************************************


/* Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_GPIO 0  # disable assert handling for gpio devices
 *
 */
#ifndef TTC_ASSERT_GPIO    // any previous definition set (Makefile)?
    #define TTC_ASSERT_GPIO 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_GPIO == 1)  // use Assert()s in GPIO code (somewhat slower but alot easier to debug)
    #define Assert_GPIO(Condition, Origin) Assert( ((Condition) != 0), Origin)
    #define Assert_GPIO_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_GPIO_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // us no Assert()s in GPIO code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_GPIO(Condition, Origin)
    #define Assert_GPIO_Writable(Address, Origin)
    #define Assert_GPIO_Readable(Address, Origin)
#endif

#ifndef TTC_ASSERT_GPIO_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_GPIO_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_GPIO_EXTRA == 1)  // use Assert()s in GPIO code (somewhat slower but alot easier to debug)
    #define Assert_GPIO_EXTRA(Condition, Origin)         Assert( ((Condition) != 0), Origin)
    #define Assert_GPIO_EXTRA_Writable(Address, Origin)  Assert_Writable(Address, Origin)
    #define Assert_GPIO_EXTRA_Readable(Address, Origin)  Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in GPIO code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_GPIO_EXTRA(Condition, Origin)
    #define Assert_GPIO_EXTRA_Writable(Address, Origin)
    #define Assert_GPIO_EXTRA_Readable(Address, Origin)
#endif


/** Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of register settings during debugging.
 * Simply
 */
#ifndef t_ttc_gpio_register
    //#warning Missing low-level definition for t_ttc_gpio_register (using default)
    #define t_ttc_gpio_register void*
#endif

#ifndef TTC_LED1_LOWACTIVE
    #define TTC_LED1_LOWACTIVE 0
#endif

//}Static Configuration
//{ Enums/ Structures *****************************************

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

/** Defaults for Undefined Constants {
 * Make sure that all constant defines are available to avoid compilation issues
 *
 */

//{ TTC_LED<N>

#ifndef TTC_LED1_LOWACTIVE
    #define TTC_LED1_LOWACTIVE 0
#endif
#ifndef TTC_LED2_LOWACTIVE
    #define TTC_LED2_LOWACTIVE 0
#endif
#ifndef TTC_LED3_LOWACTIVE
    #define TTC_LED3_LOWACTIVE 0
#endif
#ifndef TTC_LED4_LOWACTIVE
    #define TTC_LED4_LOWACTIVE 0
#endif
#ifndef TTC_LED5_LOWACTIVE
    #define TTC_LED5_LOWACTIVE 0
#endif
#ifndef TTC_LED6_LOWACTIVE
    #define TTC_LED6_LOWACTIVE 0
#endif
#ifndef TTC_LED7_LOWACTIVE
    #define TTC_LED7_LOWACTIVE 0
#endif
#ifndef TTC_LED8_LOWACTIVE
    #define TTC_LED8_LOWACTIVE 0
#endif
#ifndef TTC_LED9_LOWACTIVE
    #define TTC_LED9_LOWACTIVE 0
#endif
#ifndef TTC_LED10_LOWACTIVE
    #define TTC_LED10_LOWACTIVE 0
#endif
#ifndef TTC_LED11_LOWACTIVE
    #define TTC_LED11_LOWACTIVE 0
#endif
#ifndef TTC_LED12_LOWACTIVE
    #define TTC_LED12_LOWACTIVE 0
#endif
#ifndef TTC_LED13_LOWACTIVE
    #define TTC_LED13_LOWACTIVE 0
#endif
#ifndef TTC_LED14_LOWACTIVE
    #define TTC_LED14_LOWACTIVE 0
#endif
#ifndef TTC_LED15_LOWACTIVE
    #define TTC_LED15_LOWACTIVE 0
#endif
#ifndef TTC_LED16_LOWACTIVE
    #define TTC_LED16_LOWACTIVE 0
#endif
#ifndef TTC_LED17_LOWACTIVE
    #define TTC_LED17_LOWACTIVE 0
#endif
#ifndef TTC_LED18_LOWACTIVE
    #define TTC_LED18_LOWACTIVE 0
#endif
#ifndef TTC_LED19_LOWACTIVE
    #define TTC_LED19_LOWACTIVE 0
#endif
#ifndef TTC_LED20_LOWACTIVE
    #define TTC_LED20_LOWACTIVE 0
#endif
#ifndef TTC_LED21_LOWACTIVE
    #define TTC_LED21_LOWACTIVE 0
#endif
#ifndef TTC_LED22_LOWACTIVE
    #define TTC_LED22_LOWACTIVE 0
#endif
#ifndef TTC_LED23_LOWACTIVE
    #define TTC_LED23_LOWACTIVE 0
#endif
#ifndef TTC_LED24_LOWACTIVE
    #define TTC_LED24_LOWACTIVE 0
#endif
#ifndef TTC_LED25_LOWACTIVE
    #define TTC_LED25_LOWACTIVE 0
#endif
#ifndef TTC_LED26_LOWACTIVE
    #define TTC_LED26_LOWACTIVE 0
#endif
#ifndef TTC_LED27_LOWACTIVE
    #define TTC_LED27_LOWACTIVE 0
#endif
#ifndef TTC_LED28_LOWACTIVE
    #define TTC_LED28_LOWACTIVE 0
#endif
#ifndef TTC_LED29_LOWACTIVE
    #define TTC_LED29_LOWACTIVE 0
#endif
#ifndef TTC_LED30_LOWACTIVE
    #define TTC_LED30_LOWACTIVE 0
#endif
#ifndef TTC_LED31_LOWACTIVE
    #define TTC_LED31_LOWACTIVE 0
#endif
#ifndef TTC_LED32_LOWACTIVE
    #define TTC_LED32_LOWACTIVE 0
#endif
#ifndef TTC_LED33_LOWACTIVE
    #define TTC_LED33_LOWACTIVE 0
#endif
#ifndef TTC_LED34_LOWACTIVE
    #define TTC_LED34_LOWACTIVE 0
#endif
#ifndef TTC_LED35_LOWACTIVE
    #define TTC_LED35_LOWACTIVE 0
#endif
#ifndef TTC_LED36_LOWACTIVE
    #define TTC_LED36_LOWACTIVE 0
#endif
#ifndef TTC_LED37_LOWACTIVE
    #define TTC_LED37_LOWACTIVE 0
#endif
#ifndef TTC_LED38_LOWACTIVE
    #define TTC_LED38_LOWACTIVE 0
#endif
#ifndef TTC_LED39_LOWACTIVE
    #define TTC_LED39_LOWACTIVE 0
#endif
#ifndef TTC_LED40_LOWACTIVE
    #define TTC_LED40_LOWACTIVE 0
#endif
#ifndef TTC_LED41_LOWACTIVE
    #define TTC_LED41_LOWACTIVE 0
#endif
#ifndef TTC_LED42_LOWACTIVE
    #define TTC_LED42_LOWACTIVE 0
#endif
#ifndef TTC_LED43_LOWACTIVE
    #define TTC_LED43_LOWACTIVE 0
#endif
#ifndef TTC_LED44_LOWACTIVE
    #define TTC_LED44_LOWACTIVE 0
#endif
#ifndef TTC_LED45_LOWACTIVE
    #define TTC_LED45_LOWACTIVE 0
#endif
#ifndef TTC_LED46_LOWACTIVE
    #define TTC_LED46_LOWACTIVE 0
#endif
#ifndef TTC_LED47_LOWACTIVE
    #define TTC_LED47_LOWACTIVE 0
#endif
#ifndef TTC_LED48_LOWACTIVE
    #define TTC_LED48_LOWACTIVE 0
#endif
#ifndef TTC_LED49_LOWACTIVE
    #define TTC_LED49_LOWACTIVE 0
#endif
#ifndef TTC_LED50_LOWACTIVE
    #define TTC_LED50_LOWACTIVE 0
#endif
#ifndef TTC_LED51_LOWACTIVE
    #define TTC_LED51_LOWACTIVE 0
#endif
#ifndef TTC_LED52_LOWACTIVE
    #define TTC_LED52_LOWACTIVE 0
#endif
#ifndef TTC_LED53_LOWACTIVE
    #define TTC_LED53_LOWACTIVE 0
#endif
#ifndef TTC_LED54_LOWACTIVE
    #define TTC_LED54_LOWACTIVE 0
#endif
#ifndef TTC_LED55_LOWACTIVE
    #define TTC_LED55_LOWACTIVE 0
#endif
#ifndef TTC_LED56_LOWACTIVE
    #define TTC_LED56_LOWACTIVE 0
#endif
#ifndef TTC_LED57_LOWACTIVE
    #define TTC_LED57_LOWACTIVE 0
#endif
#ifndef TTC_LED58_LOWACTIVE
    #define TTC_LED58_LOWACTIVE 0
#endif
#ifndef TTC_LED59_LOWACTIVE
    #define TTC_LED59_LOWACTIVE 0
#endif
#ifndef TTC_LED60_LOWACTIVE
    #define TTC_LED60_LOWACTIVE 0
#endif
#ifndef TTC_LED61_LOWACTIVE
    #define TTC_LED61_LOWACTIVE 0
#endif
#ifndef TTC_LED62_LOWACTIVE
    #define TTC_LED62_LOWACTIVE 0
#endif
#ifndef TTC_LED63_LOWACTIVE
    #define TTC_LED63_LOWACTIVE 0
#endif
#ifndef TTC_LED64_LOWACTIVE
    #define TTC_LED64_LOWACTIVE 0
#endif
#ifndef TTC_LED65_LOWACTIVE
    #define TTC_LED65_LOWACTIVE 0
#endif
#ifndef TTC_LED66_LOWACTIVE
    #define TTC_LED66_LOWACTIVE 0
#endif
#ifndef TTC_LED67_LOWACTIVE
    #define TTC_LED67_LOWACTIVE 0
#endif
#ifndef TTC_LED68_LOWACTIVE
    #define TTC_LED68_LOWACTIVE 0
#endif
#ifndef TTC_LED69_LOWACTIVE
    #define TTC_LED69_LOWACTIVE 0
#endif
#ifndef TTC_LED70_LOWACTIVE
    #define TTC_LED70_LOWACTIVE 0
#endif
#ifndef TTC_LED71_LOWACTIVE
    #define TTC_LED71_LOWACTIVE 0
#endif
#ifndef TTC_LED72_LOWACTIVE
    #define TTC_LED72_LOWACTIVE 0
#endif
#ifndef TTC_LED73_LOWACTIVE
    #define TTC_LED73_LOWACTIVE 0
#endif
#ifndef TTC_LED74_LOWACTIVE
    #define TTC_LED74_LOWACTIVE 0
#endif
#ifndef TTC_LED75_LOWACTIVE
    #define TTC_LED75_LOWACTIVE 0
#endif
#ifndef TTC_LED76_LOWACTIVE
    #define TTC_LED76_LOWACTIVE 0
#endif
#ifndef TTC_LED77_LOWACTIVE
    #define TTC_LED77_LOWACTIVE 0
#endif
#ifndef TTC_LED78_LOWACTIVE
    #define TTC_LED78_LOWACTIVE 0
#endif
#ifndef TTC_LED79_LOWACTIVE
    #define TTC_LED79_LOWACTIVE 0
#endif
#ifndef TTC_LED80_LOWACTIVE
    #define TTC_LED80_LOWACTIVE 0
#endif
#ifndef TTC_LED81_LOWACTIVE
    #define TTC_LED81_LOWACTIVE 0
#endif
#ifndef TTC_LED82_LOWACTIVE
    #define TTC_LED82_LOWACTIVE 0
#endif
#ifndef TTC_LED83_LOWACTIVE
    #define TTC_LED83_LOWACTIVE 0
#endif
#ifndef TTC_LED84_LOWACTIVE
    #define TTC_LED84_LOWACTIVE 0
#endif
#ifndef TTC_LED85_LOWACTIVE
    #define TTC_LED85_LOWACTIVE 0
#endif
#ifndef TTC_LED86_LOWACTIVE
    #define TTC_LED86_LOWACTIVE 0
#endif
#ifndef TTC_LED87_LOWACTIVE
    #define TTC_LED87_LOWACTIVE 0
#endif
#ifndef TTC_LED88_LOWACTIVE
    #define TTC_LED88_LOWACTIVE 0
#endif
#ifndef TTC_LED89_LOWACTIVE
    #define TTC_LED89_LOWACTIVE 0
#endif
#ifndef TTC_LED90_LOWACTIVE
    #define TTC_LED90_LOWACTIVE 0
#endif
#ifndef TTC_LED91_LOWACTIVE
    #define TTC_LED91_LOWACTIVE 0
#endif
#ifndef TTC_LED92_LOWACTIVE
    #define TTC_LED92_LOWACTIVE 0
#endif
#ifndef TTC_LED93_LOWACTIVE
    #define TTC_LED93_LOWACTIVE 0
#endif
#ifndef TTC_LED94_LOWACTIVE
    #define TTC_LED94_LOWACTIVE 0
#endif
#ifndef TTC_LED95_LOWACTIVE
    #define TTC_LED95_LOWACTIVE 0
#endif
#ifndef TTC_LED96_LOWACTIVE
    #define TTC_LED96_LOWACTIVE 0
#endif
#ifndef TTC_LED97_LOWACTIVE
    #define TTC_LED97_LOWACTIVE 0
#endif
#ifndef TTC_LED98_LOWACTIVE
    #define TTC_LED98_LOWACTIVE 0
#endif
#ifndef TTC_LED99_LOWACTIVE
    #define TTC_LED99_LOWACTIVE 0
#endif

//}TTC_LED<N>
//{ TTC_SWITCH<N>

#ifndef TTC_SWITCH1_LOWACTIVE
    #define TTC_SWITCH1_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH2_LOWACTIVE
    #define TTC_SWITCH2_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH3_LOWACTIVE
    #define TTC_SWITCH3_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH4_LOWACTIVE
    #define TTC_SWITCH4_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH5_LOWACTIVE
    #define TTC_SWITCH5_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH6_LOWACTIVE
    #define TTC_SWITCH6_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH7_LOWACTIVE
    #define TTC_SWITCH7_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH8_LOWACTIVE
    #define TTC_SWITCH8_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH9_LOWACTIVE
    #define TTC_SWITCH9_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH10_LOWACTIVE
    #define TTC_SWITCH10_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH11_LOWACTIVE
    #define TTC_SWITCH11_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH12_LOWACTIVE
    #define TTC_SWITCH12_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH13_LOWACTIVE
    #define TTC_SWITCH13_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH14_LOWACTIVE
    #define TTC_SWITCH14_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH15_LOWACTIVE
    #define TTC_SWITCH15_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH16_LOWACTIVE
    #define TTC_SWITCH16_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH17_LOWACTIVE
    #define TTC_SWITCH17_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH18_LOWACTIVE
    #define TTC_SWITCH18_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH19_LOWACTIVE
    #define TTC_SWITCH19_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH20_LOWACTIVE
    #define TTC_SWITCH20_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH21_LOWACTIVE
    #define TTC_SWITCH21_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH22_LOWACTIVE
    #define TTC_SWITCH22_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH23_LOWACTIVE
    #define TTC_SWITCH23_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH24_LOWACTIVE
    #define TTC_SWITCH24_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH25_LOWACTIVE
    #define TTC_SWITCH25_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH26_LOWACTIVE
    #define TTC_SWITCH26_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH27_LOWACTIVE
    #define TTC_SWITCH27_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH28_LOWACTIVE
    #define TTC_SWITCH28_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH29_LOWACTIVE
    #define TTC_SWITCH29_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH30_LOWACTIVE
    #define TTC_SWITCH30_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH31_LOWACTIVE
    #define TTC_SWITCH31_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH32_LOWACTIVE
    #define TTC_SWITCH32_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH33_LOWACTIVE
    #define TTC_SWITCH33_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH34_LOWACTIVE
    #define TTC_SWITCH34_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH35_LOWACTIVE
    #define TTC_SWITCH35_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH36_LOWACTIVE
    #define TTC_SWITCH36_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH37_LOWACTIVE
    #define TTC_SWITCH37_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH38_LOWACTIVE
    #define TTC_SWITCH38_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH39_LOWACTIVE
    #define TTC_SWITCH39_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH40_LOWACTIVE
    #define TTC_SWITCH40_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH41_LOWACTIVE
    #define TTC_SWITCH41_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH42_LOWACTIVE
    #define TTC_SWITCH42_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH43_LOWACTIVE
    #define TTC_SWITCH43_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH44_LOWACTIVE
    #define TTC_SWITCH44_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH45_LOWACTIVE
    #define TTC_SWITCH45_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH46_LOWACTIVE
    #define TTC_SWITCH46_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH47_LOWACTIVE
    #define TTC_SWITCH47_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH48_LOWACTIVE
    #define TTC_SWITCH48_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH49_LOWACTIVE
    #define TTC_SWITCH49_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH50_LOWACTIVE
    #define TTC_SWITCH50_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH51_LOWACTIVE
    #define TTC_SWITCH51_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH52_LOWACTIVE
    #define TTC_SWITCH52_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH53_LOWACTIVE
    #define TTC_SWITCH53_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH54_LOWACTIVE
    #define TTC_SWITCH54_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH55_LOWACTIVE
    #define TTC_SWITCH55_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH56_LOWACTIVE
    #define TTC_SWITCH56_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH57_LOWACTIVE
    #define TTC_SWITCH57_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH58_LOWACTIVE
    #define TTC_SWITCH58_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH59_LOWACTIVE
    #define TTC_SWITCH59_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH60_LOWACTIVE
    #define TTC_SWITCH60_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH61_LOWACTIVE
    #define TTC_SWITCH61_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH62_LOWACTIVE
    #define TTC_SWITCH62_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH63_LOWACTIVE
    #define TTC_SWITCH63_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH64_LOWACTIVE
    #define TTC_SWITCH64_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH65_LOWACTIVE
    #define TTC_SWITCH65_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH66_LOWACTIVE
    #define TTC_SWITCH66_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH67_LOWACTIVE
    #define TTC_SWITCH67_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH68_LOWACTIVE
    #define TTC_SWITCH68_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH69_LOWACTIVE
    #define TTC_SWITCH69_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH70_LOWACTIVE
    #define TTC_SWITCH70_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH71_LOWACTIVE
    #define TTC_SWITCH71_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH72_LOWACTIVE
    #define TTC_SWITCH72_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH73_LOWACTIVE
    #define TTC_SWITCH73_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH74_LOWACTIVE
    #define TTC_SWITCH74_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH75_LOWACTIVE
    #define TTC_SWITCH75_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH76_LOWACTIVE
    #define TTC_SWITCH76_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH77_LOWACTIVE
    #define TTC_SWITCH77_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH78_LOWACTIVE
    #define TTC_SWITCH78_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH79_LOWACTIVE
    #define TTC_SWITCH79_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH80_LOWACTIVE
    #define TTC_SWITCH80_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH81_LOWACTIVE
    #define TTC_SWITCH81_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH82_LOWACTIVE
    #define TTC_SWITCH82_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH83_LOWACTIVE
    #define TTC_SWITCH83_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH84_LOWACTIVE
    #define TTC_SWITCH84_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH85_LOWACTIVE
    #define TTC_SWITCH85_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH86_LOWACTIVE
    #define TTC_SWITCH86_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH87_LOWACTIVE
    #define TTC_SWITCH87_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH88_LOWACTIVE
    #define TTC_SWITCH88_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH89_LOWACTIVE
    #define TTC_SWITCH89_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH90_LOWACTIVE
    #define TTC_SWITCH90_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH91_LOWACTIVE
    #define TTC_SWITCH91_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH92_LOWACTIVE
    #define TTC_SWITCH92_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH93_LOWACTIVE
    #define TTC_SWITCH93_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH94_LOWACTIVE
    #define TTC_SWITCH94_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH95_LOWACTIVE
    #define TTC_SWITCH95_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH96_LOWACTIVE
    #define TTC_SWITCH96_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH97_LOWACTIVE
    #define TTC_SWITCH97_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH98_LOWACTIVE
    #define TTC_SWITCH98_LOWACTIVE 0
#endif
#ifndef TTC_SWITCH99_LOWACTIVE
    #define TTC_SWITCH99_LOWACTIVE 0
#endif

//}TTC_SWITCH<N>
//}Defaults
#endif // TTC_GPIO_TYPES_H
