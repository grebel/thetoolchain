#ifndef TTC_STRING_H
#define TTC_STRING_H
/** { ttc_string.h *******************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc_device.h revision 35 at 20151120 13:31:34 UTC
 *
 *  Authors: Gregor Rebel
 *
 *
 *  ttc_string
 *
 *
 */
/** Description of ttc_string (Do not delete this line!)
 *
 *  Note: Check chapter_TextOutput for a wider discussion about string support in The ToolChain.
 *
 *  Architecture independent support for operations on zero terminated character sequences (strings).
 *  Here you can find support for string conversion and output. You will find an implementation
 *  of printf() and snprintf() and several single byte conversions (faster than printf).
 *
 *  Main ttc_string Functions:
 *  ttc_string_snprintf()        - snprintf() pendant to compile formatted string in given memory buffer
 *  ttc_string_appendf()         - snprintf() variant optimized to stepwise append formatted strings to a buffer
 *  ttc_string_compare()         - checks if two given strings are equal
 *  ttc_string_copy()            - copies string from one buffer to another one
 *  ttc_string_register_stdout() - registers given function to output strings passed to ttc_string_printf()
 *
 *  Strings and Third Party Libraries
 *
 *  Many third party libraries include C standard libraries for string operations.
 *  ttc_string and other ttc-drivers provide alternative implementations with better configurability via use
 *  of specialized low-level drivers. Your TTC-projects should never include or link one of these libraries:
 *  - stdio
 *  - stlib
 *  - string
 *
 *  If you include external libraries, add these lines to your install script to replace unwanted includes:

cat >replaceIncludes.sh <<END_OF_REPLACE
#!/bin/bash

source \$HOME/Source/TheToolChain/InstallData/scripts/installFuncs.sh

echo "\$0 patching includes in file \$1 ..."
replaceInFile "\$1" "#include <stdio.h>"  "// #include <stdio.h> // TTC provides optimized version of this library"
replaceInFile "\$1" "#include <stdlib.h>" "// #include <stdlib.h> // TTC provides optimized version of this library"
replaceInFile "\$1" "#include <string.h>"  '#include "ttc_memory.h" // #include <string.h>  // TTC provides optimized version of this library'

END_OF_REPLACE
    find ./ -name "*.c" -exec bash ./replaceIncludes.sh {} \;
    find ./ -name "*.h" -exec bash ./replaceIncludes.sh {} \;
}*/


//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_string.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "ttc_queue.h"
#include "ttc_math_types.h"
#include "stdarg.h"  // required for va_list
#include "string/string_common.h"
#include "interfaces/ttc_string_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

#ifndef EXTENSION_ttc_string
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_string.sh
#endif

// Check macros that must not be defined outside..
#ifdef snprintf
    #  error snprintf() already defined outside ttc_string! Include ttc_string.h instead of <string.h>!
#endif
#ifdef printf
    #  error printf() already defined outside ttc_string! Include ttc_string.h instead of <string.h>!
#endif
#ifdef print
    #  error print() already defined outside ttc_string! Include ttc_string.h instead of <string.h>!
#endif
#ifdef strlen
    #  error strlen() already defined outside ttc_string! Include ttc_string.h instead of <string.h>!
#endif
#ifdef stnrlen
    #  error stnrlen() already defined outside ttc_string! Include ttc_string.h instead of <string.h>!
#endif
#ifdef strcpy
    #  error strcpy() already defined outside ttc_string! Include ttc_string.h instead of <string.h>!
#endif
#ifdef strncpy
    #  error strncpy() already defined outside ttc_string! Include ttc_string.h instead of <string.h>!
#endif


// Compatibility macros for <string.h> library (add more if needed)

#define snprintf(...)                         ttc_string_snprintf(__VA_ARGS__)
#define printf(...)                           ttc_string_printf(__VA_ARGS__)
#define print(String, MaxSize)                ttc_string_print(String, MaxSize)
#define strcpy(Destination, Source)           ttc_string_copy(Destination, Source, -1)
#define strncpy(Destination, Source, MaxSize) ttc_string_copy(Destination, Source, MaxSize)

#if TARGET_DATA_WIDTH == 8
    #ifndef strlen
        #define strlen(String) ttc_string_length8(String, -1)
    #endif
    #ifndef stnrlen
        #define stnrlen(String, MaxLength) ttc_string_length8(String, MaxLength)
    #endif
#endif
#if TARGET_DATA_WIDTH == 16
    #ifndef strlen
        #define strlen(String) ttc_string_length16(String, -1)
    #endif
    #ifndef stnrlen
        #define stnrlen(String, MaxLength) ttc_string_length16(String, MaxLength)
    #endif
#endif
#if TARGET_DATA_WIDTH == 32
    #ifndef strlen
        #define strlen(String) ttc_string_length32(String, -1)
    #endif
    #ifndef stnrlen
        #define stnrlen(String, MaxLength) ttc_string_length32(String, MaxLength)
    #endif
#endif

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level string only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function Declarations ************************************************{
 *
 * The functions declared below provide the main interface for
 * string devices on all supported architectures.
 * Check string/string_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares string Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_string_prepare();
void _driver_string_prepare();

/** Removes leading and trailing invisible characters.
 *
 * @param Start     pointer storing address of first byte of string (will be moved to first non whitespace character)
 * @param Size      pointer storing size of string= (amount of valid bytes starting at *Start (will be adjusted)
 * @return  *Start, *Size get adjusted; String in Start[] is not altered
 */
void ttc_string_strip( t_u8** Start, t_base* Size );
void _driver_string_strip( t_u8** Start, t_base* Size );
#define ttc_string_strip( Start, Size )  _driver_string_strip( Start, Size )

/** Checks if given character is an invisible character
 *
 * @param ASCII   character to check (ASCII code)
 * @return        == TRUE: character is an invisible character
 */
BOOL ttc_string_is_whitechar( t_u8 ASCII );
BOOL _driver_string_is_whitechar( t_u8 ASCII );
#define ttc_string_is_whitechar(  ASCII ) _driver_string_is_whitechar(  ASCII )

/** Copies string from Source to Target address until first zero byte or MaxSize reached.
 *
 * Note: String in target will always be zero terminated, even if Target[] is smaller than Source[].
 *
 * @param Target  start address of buffer to write to
 * @param Source  start address of buffer to read from
 * @param MaxSize no more than this amount of bytes will be copied (safety check)
 * @return        amount of bytes copied (+ zero byte)
 */
t_base ttc_string_copy( t_u8* Target, const t_u8* Source, t_base MaxSize );
t_base _driver_string_copy( t_u8* Target, const t_u8* Source, t_base MaxSize );
#define  ttc_string_copy(  Target, Source,  MaxSize ) _driver_string_copy(  Target, Source, MaxSize )

/** Prints given string on stdout device.
 * Note: A stdout device has to be registered for an interface driver (e.g. ttc_usart) before!
 *       If no interface driver has been registered, then given string is silently ignored.
 *
 * @param String  constant text to output
 * @param MaxSize no more than this amount of bytes will be read from String[]
 */
void ttc_string_print( const t_u8* String, t_base MaxSize );
void _driver_string_print( const t_u8* String, t_base MaxSize );
#define ttc_string_print( String,  MaxSize ) _driver_string_print(  String, MaxSize )

/** printf compatible function which outputs to standard output device
 *
 * This is a convenient function to support a standard output function for formatted strings.
 * The given format string and additional values is compiled into an internal, static memory buffer.
 * The compiled string is then passed to a registered stdout print function.
 *
 * Note: Will allocate TTC_STRING_PRINTF_BUFFERSIZE bytes from heap at first call.
 * Note: This function is very slow and will lock complete multitasking during string compilation.
 * Note: Call ttc_string_register_stdout() before to allow passing the compiled string to an output device!
 *
 * @param Format   formatting string as described in manpage of standard printf
 * @param varargs  variable amount of arguments as defined in Format
 */
void ttc_string_printf( const char* Format, ... );
void _driver_string_printf( const char* Format, ... );
#define ttc_string_printf(  ... ) _driver_string_printf( __VA_ARGS__ )

/** snprintf() replacement which writes a formatted string into given buffer
 *
 * Note: This function is reentrant and thread-safe.
 * Note: This function might not be 100% compatible to standard snprintf().
 *       Check current low-level implementation for details!
 *
 * @param Buffer   string buffer where to write to
 * @param Size     maximum allowed amount of bytes to write into Buffer[]
 * @param Format   formatting string mostly compatible to standard printf() (-> man 3 printf)
 *                 Supported formats (<n> = 1..255):
 *                 %c      single character                                             E.g. ("%c", 64)          -> "@"
 *                 %b      binary integer                                               E.g. ("%b",    0b100111) -> "100111"
 *                 %<n>b   space-prefixed, minimum length binary integer                E.g. ("%8b",     0b1101) -> "    1101"
 *                 %0<n>b  zero-prefixed, minimum length binary integer                 E.g. ("%08b",    0b1101) -> "00001101"
 *                 %d/%i   signed decimal integer                                       E.g. ("%d",        -123) -> "-123"
 *                 %<n>d   space-prefixed, minimum length signed decimal integer        E.g. ("%8d",       -123) -> "    -123"
 *                 %0<n>d  zero-prefixed, minimum length signed decimal integer         E.g. ("%08d",      -123) -> "-0000123"
 *                 %u      unsigned decimal integer                                     E.g. ("%d",         123) -> "123"
 *                 %<n>u   space-prefixed, minimum length unsigned decimal integer      E.g. ("%8u",        123) -> "     123"
 *                 %0<n>u  zero-prefixed, minimum length unsigned decimal integer       E.g. ("%08u",       123) -> "00000123"
 *                 %x      unsigned hexadecimal integer                                 E.g. ("%x",      0xaffe) -> "affe"
 *                 %<n>x   space-prefixed, minimum length unsigned hexadecimal integer  E.g. ("%8x",     0xaffe) -> "    affe"
 *                 %0<n>x  zero-prefixed, minimum length unsigned hexadecimal integer   E.g. ("%08x",    0xaffe) -> "0000affe"
 *                 %s      string from given buffer                                     E.g. ("Hello %s!", Name) -> "Hello Alfred!"
 *
 * @param varargs  variable amount of arguments as defined in Format
 * @return         pointer to first character in Buffer[] after written string
 */
t_u8* ttc_string_snprintf( t_u8* Buffer, t_base Size, const char* Format, ... );
t_u8* _driver_string_snprintf( t_u8* Buffer, t_base Size, const char* Format, ... );
#define  ttc_string_snprintf(... ) _driver_string_snprintf( __VA_ARGS__ )

/** snprintf compatible function which writes a formatted string into given buffer
 *
 * Note: This function is reentrant and thread-safe.
 * Note: This function may be called from functions using VA_ARGS on their own (which call va_list(), va_start())
 *
 * @param Buffer   string buffer where to write to
 * @param Size     maximum allowed amount of bytes to write into Buffer[]
 * @param Format   formatting string as described for ttc_string_snprintf()
 * @param varargs  variable amount of arguments as defined in Format
 * @return         pointer to first character in Buffer[] after written string
 */
t_u8* ttc_string_snprintf4( t_u8* Buffer, t_base Size, const char* Format, va_list Arguments );
t_u8* _driver_string_snprintf4( t_u8* Buffer, t_base Size, const char* Format, va_list Arguments );
#define  ttc_string_snprintf4(... ) _driver_string_snprintf4( __VA_ARGS__ )

/** function similar to snprintf() which appends formatted string to given buffer
 *
 * Note: This function is reentrant and thread-safe.
 *
 * @param Buffer   string buffer where to write to
 * @param Size     pointer to maximum allowed amount of bytes to write into Buffer[] (will be decreased by amount bytes appended)
 * @param Format   formatting string as described for ttc_string_snprintf()
 * @param varargs  variable amount of arguments as defined in Format
 * @return         amount of bytes being appended to Buffer[] (not more than *Size)
 */
t_base ttc_string_appendf( t_u8* Buffer, t_base* Size, const char* Format, ... );
t_base _driver_string_appendf( t_u8* Buffer, t_base* Size, const char* Format, ... );
#define  ttc_string_appendf( ... ) _driver_string_appendf(  __VA_ARGS__ )

/** Prints string in given memory block on stdout device (zero-copy strategy).
 * Note: A stdout device has to be registered for an interface driver (e.g. ttc_usart) before!
 *       If no interface driver has been registered, then given string is silently ignored.
 * Note: Ownership of Block goes to ttc_string_print_block(). Do NOT access it anymore!
 *       After successfull transmission, block will be released to Block->releaseBuffer().
 *
 * @param Block   memory block containing string to print
 * @param MaxSize no more than this amount of bytes will be read from String[]
 */
void ttc_string_print_block( t_ttc_heap_block* Block );
void _driver_string_print_block( t_ttc_heap_block* Block );
#define ttc_string_print_block(  Block ) _driver_string_print_block(  Block )

/** Registers given function pointers for stdout output via ttc_string_print_block()/ ttc_string_print()/ ttc_string_printf().
 *
 * @param StdoutBlock  function that will send out content of given memory block
 * @param StdoutString function that will send out content of given string buffer
 */
void ttc_string_register_stdout( void ( *StdoutBlock )( t_ttc_heap_block* ), void ( *StdoutString )( const t_u8*, t_base ) );
#define ttc_string_register_stdout( StdoutBlock, StdoutString) string_common_register_stdout( StdoutBlock,StdoutString)

/** writes ascii representation of given binary value into given buffer
 *
 * @param   Value   signed integer value to convert
 * @param   Buffer  string buffer in which to write ascii representation
 * @param   MaxSize no more than this amount of bytes will be written into Buffer[]
 * @return          amount of bytes written to Buffer[]
 */
t_u8 ttc_string_btoa( t_base Value, t_u8* Buffer, t_base MaxSize );
t_u8 _driver_string_btoa( t_base Value, t_u8* Buffer, t_base MaxSize );
#define ttc_string_btoa( Value, Buffer,  MaxSize ) _driver_string_btoa(  Value,  Buffer,  MaxSize )

/** writes ascii representation of given integer value into given buffer
 *
 * @param   Value   signed integer value to convert
 * @param   Buffer  string buffer in which to write ascii representation
 * @param   MaxSize no more than this amount of bytes will be written into Buffer[]
 * @return          amount of bytes written to Buffer[]
 */
t_u8 ttc_string_itoa( t_base_signed Value, t_u8* Buffer, t_base MaxSize );
t_u8 _driver_string_itoa( t_base_signed Value, t_u8* Buffer, t_base MaxSize );
#define ttc_string_itoa( Value, Buffer,  MaxSize ) _driver_string_itoa(  Value,  Buffer,  MaxSize )

/** writes ascii representation of given unsigned integer value into given buffer
 *
 * @param   Value   signed integer value to convert
 * @param   Buffer  string buffer in which to write ascii representation
 * @param   MaxSize no more than this amount of bytes will be written into Buffer[]
 * @return          amount of bytes written to Buffer[]
 */
t_u8 ttc_string_itoa_unsigned( t_base Value, t_u8* Buffer, t_base MaxSize );
t_u8 _driver_string_itoa_unsigned( t_base Value, t_u8* Buffer, t_base MaxSize );
#define ttc_string_itoa_unsigned(Value,  Buffer, MaxSize ) _driver_string_itoa_unsigned( Value, Buffer, MaxSize )

/** writes hexadecimal representation of given integer value into given buffer
 * @param   Value   unsigned integer value to convert
 * @param   Buffer  string buffer in which to write ascii representation
 * @param   MaxSize no more than this amount of bytes will be written into Buffer[]
 * @return          amount of bytes written to Buffer[]
 */
t_u8 ttc_string_xtoa( t_base Value, t_u8* Buffer, t_base MaxSize );
t_u8 _driver_string_xtoa( t_base Value, t_u8* Buffer, t_base MaxSize );
#define ttc_string_xtoa( Value, Buffer, MaxSize ) _driver_string_xtoa( Value, Buffer, MaxSize )

/** Fast converter for 8-bit unsigned values.
 * Writes two character hexadecimal representation of given byte value into given buffer
 */
void ttc_string_byte2hex( t_u8* Buffer, t_u8 Byte );
void _driver_string_byte2hex( t_u8* Buffer, t_u8 Byte );
#define ttc_string_byte2hex( Buffer, Byte ) _driver_string_byte2hex( Buffer, Byte )

/** Fast converter for 8-bit unsigned values
 * Writes up to three characters as decimal representation of given byte value into given buffer
 *
 * @param   Value   unsigned integer value to convert
 * @param   Buffer  string buffer in which to write ascii representation
 * @return          amount of bytes written to Buffer[]
 */
t_u8 ttc_string_byte2uint( t_u8 Value, t_u8* Buffer );
t_u8 _driver_string_byte2uint( t_u8 Value, t_u8* Buffer );
#define ttc_string_byte2uint( Value, Buffer ) _driver_string_byte2uint( Value, Buffer )

/** Fast converter for 8-bit signed values
 * Writes up to four characters as decimal representation of given byte value into given buffer
 *
 * @param   Value   signed integer value to convert
 * @param   Buffer  string buffer in which to write ascii representation
 * @return          amount of bytes written to Buffer[]
 */
t_u8 ttc_string_byte2int( t_s8 Value, t_u8* Buffer );
t_u8 _driver_string_byte2int( t_s8 Value, t_u8* Buffer );
#define ttc_string_byte2int( Value, Buffer ) _driver_string_byte2int( Value, Buffer )

/** Calculate logarithmus of given value to base 2, 10 or 16.
 *
 * This function is handy if you wanna know how much digits are required to code a given value
 * in binary, hex or decimal representation.
 *
 * @param   Value   signed integer value to convert
 * @return          amount of digits required in decimal or hexadecimal representation
 */
t_u8 ttc_string_log_binary( t_base Value );
t_u8 _driver_string_log_binary( t_base Value );
#define ttc_string_log_binary( Value ) _driver_string_log_binary( Value )


t_u8 ttc_string_log_decimal( t_base_signed Value );
t_u8 _driver_string_log_decimal( t_base_signed Value );
#define ttc_string_log_decimal( Value ) _driver_string_log_decimal( Value )

t_u8 ttc_string_log_hex( t_base Value );
t_u8 _driver_string_log_hex( t_base Value );
#define ttc_string_log_hex( Value ) _driver_string_log_hex( Value )

/** Calculate length of given string as amount of characters until first zero byte or MaxLen reached.
 * This function can be used for strings of size up to 255 bytes.
 * Note: You may use the architecture aware macro strnlen() instead!
 *
 * @param String  points to buffer storing string to be read
 * @param MaxLen  safety value used to avoid running out of allocated memory in case of missing zero byte
 * @return length of string <= MaxLen
 */
t_u8 ttc_string_length8( const char* String, t_u8 MaxLen );
t_u8 _driver_string_length8( const char* String, t_u8 MaxLen );
#define ttc_string_length8( String, MaxLen ) _driver_string_length8( String, MaxLen )

/** Calculate length of given string as amount of characters until first zero byte or MaxLen reached.
 * This function can be used for strings of size up to 65535 bytes.
 * Note: You may use the architecture aware macro strnlen() instead!
 *
 * @param String  points to buffer storing string to be read
 * @param MaxLen  safety value used to avoid running out of allocated memory in case of missing zero byte
 * @return length of string <= MaxLen
 */
t_u16 ttc_string_length16( const char* String, t_u16 MaxLen );
t_u16 _driver_string_length16( const char* String, t_u16 MaxLen );
#define  ttc_string_length16( String, MaxLen ) _driver_string_length16( String, MaxLen )

/** Calculate length of given string as amount of characters until first zero byte or MaxLen reached.
 * This function can be used for strings of size up to 4 GB (theoretically)
 * Note: You may use the architecture aware macro strnlen() instead!
 *
 * @param String  points to buffer storing string to be read
 * @param MaxLen  safety value used to avoid running out of allocated memory in case of missing zero byte
 * @return length of string <= MaxLen
 */
t_u32 ttc_string_length32( const char* String, t_u32 MaxLen );
t_u32 _driver_string_length32( const char* String, t_u32 MaxLen );
#define  ttc_string_length32( String, MaxLen ) _driver_string_length32( String, MaxLen )

/** Calculate length of given string as amount of characters until first zero byte or MaxLen reached.
 * This function can be used for strings of size up to 4 GB depending on your current architecture
 *
 * @param StringA  points to buffer storing string to compare
 * @param StringB  points to buffer storing string to compare
 * @param MaxLen  safety value used to avoid running out of allocated memory in case of missing zero byte
 * @return  ==0: StringA == StringB; <0 stopping charachter in A < B; >0 stopping charachter in A > B
*/
t_base_signed ttc_string_compare( const char* StringA, const char* StringB, t_base MaxLen );
t_base_signed _driver_string_compare( const char* StringA, const char* StringB, t_base MaxLen );
#define  ttc_string_compare( StringA, StringB, MaxLen ) _driver_string_compare( StringA, StringB, MaxLen )

/** writes ascii representation of given floating point value into given buffer
 *
 * Note: The precision currently used is defined by ttc_math.
 *
 * @param   Value   floating point value of single or duble precision to convert
 * @param   Buffer  string buffer in which to write ascii representation
 * @param   MaxSize no more than this amount of bytes will be written into Buffer[]
 * @return          amount of bytes written to Buffer[]
 */
t_u8 ttc_string_ftoa( ttm_number Value, t_u8* Buffer, t_base MaxSize, t_s8 DecNumber );
t_u8 _driver_string_ftoa( ttm_number Value, t_u8* Buffer, t_base MaxSize, t_s8 DecNumber );
#define  ttc_string_ftoa(Value, Buffer, MaxSize, DecNumber) _driver_string_ftoa( Value, Buffer, MaxSize, DecNumber)

//InsertFunctionDeclarations

//}Function prototypes
//{ private functions

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_STRING_H
