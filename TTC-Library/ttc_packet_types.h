/** { ttc_packet_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for PACKET device.
 *  Structures, Enums and Defines being required by both, high- and low-level packet.
 *  This file does not provide function declarations!
 *  54 at 20150928 09:30:04 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_PACKET_TYPES_H
#define TTC_PACKET_TYPES_H



//{ Early Defines ********************************************************

#ifndef TTC_PACKET_SOCKET_BYTE
    #define TTC_PACKET_SOCKET_BYTE 1 // ==1: t_ttc_packet.Payload stores an extra protocol identifier byte; ==0: t_ttc_packet.Payload stores only raw payload (application must distinguish protocols)
#endif

//} Defines

//{ Includes *************************************************************
//
// _types.h Header files mayshould include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_packet.h" or "ttc_packet.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h"
#include "packet/packet_common_types.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_packet_802154
    #include "packet/packet_802154_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_PACKET 0        # disable default asserts for packet driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_PACKET_EXTRA 1  # enable extra asserts for packet driver
 *
 */
#ifndef TTC_ASSERT_PACKET    // any previous definition set (Makefile)?
    #define TTC_ASSERT_PACKET 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_PACKET == 1)  // use Assert()s in PACKET code (somewhat slower but alot easier to debug)
    #define Assert_PACKET(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_PACKET_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_PACKET_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in PACKET code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_PACKET(Condition, Origin)
    #define Assert_PACKET_Writable(Address, Origin)
    #define Assert_PACKET_Readable(Address, Origin)
#endif


#ifndef TTC_ASSERT_PACKET_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_PACKET_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_PACKET_EXTRA == 1)  // use Assert()s in PACKET code (somewhat slower but alot easier to debug)
    #define Assert_PACKET_EXTRA(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_PACKET_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_PACKET_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in PACKET code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_PACKET_EXTRA(Condition, Origin)
    #define Assert_PACKET_EXTRA_Writable(Address, Origin)
    #define Assert_PACKET_EXTRA_Readable(Address, Origin)
#endif
//}

#ifndef t_ttc_packet_meta_lowlevel
    /** low-level driver may define this to store extra meta data in each packet.
    *
    * Note: sizeof(t_ttc_packet_meta_lowlevel) must be a multiple of 4 (check by ttc_packet_prepare()) !
    */
    //#  warning Missing definition for t_ttc_packet_meta_lowlevel. Low-level driver shall define this datatype as packet meta information (part that is not transmitted). See packet_802154_types.h for an example!
    //#  define t_ttc_packet_meta_lowlevel t_u8
#endif

#ifndef t_ttc_packet_mac
    #  error Missing definition for t_ttc_packet_mac. Low-level driver must define this datatype as packet header + payload (raw data being transmitted/ received). See packet_802154_types.h for an example!
    #define t_ttc_packet_mac t_u8
#endif

// t_ttc_packet_mac is a union of all packet headers. So every header must fit into it.
#define TTC_PACKET_MAX_HEADER_SIZE sizeof(t_ttc_packet_mac)

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef struct s_ttc_radio_address { // combination of physical and logical address
    union {              // 64-bit physical address
        t_u8  Bytes[8];
        t_u32 Words[2];
    } Address64;
    t_u16 Address16;     // 16-bit logical address
    t_u16 PanID;         // Personal Area Network Identification
} __attribute__( ( __packed__ ) ) t_ttc_packet_address;

typedef enum {   // e_ttc_packet_errorcode       return codes of PACKET devices
    ec_packet_OK = 0,

    // other warnings go here..

    ec_packet_ERROR,                  // general failure
    ec_packet_NULL,                   // NULL pointer not accepted
    ec_packet_DeviceNotFound,         // corresponding device could not be found
    ec_packet_InvalidArgument,        // invalid argument given to function
    ec_packet_InvalidConfiguration,   // sanity check of device configuration failed
    ec_packet_InvalidImplementation,  // your code does not behave as expected
    ec_packet_unknownPacketType,      // given packet seems to be corrupt
    ec_packet_UnsupportedPacketType,  // type of given packet currently not supported

    // other failures go here..

    ec_packet_unknown                // no valid errorcodes past this entry
} e_ttc_packet_errorcode;

typedef union { // 40 bit timestamp value
    t_u8 Bytes[5];

    struct { // word access (32 bit low-word may provide faster access than using individual bytes)
        t_u32 Low;
        t_u8  High;
    } __attribute__( ( packed ) ) Words;

    struct {
        t_u32 TIMESTAMP_LOW;            // low 32 bits of 40 bit timestamp
        t_u8  TIMESTAMP_HIGH;           // high 8 bits of 40 bit timestamp
    } __attribute__( ( packed ) ) Fields;
} u_ttc_packetimestamp_40;

typedef enum {  // e_s_ttc_packettatus_tx  - status of packet transmission
    tpst_None,                   // No status up to now.
    tpst_Info_TxStarted,         // Transmission has been started
    tpst_Info_TxPreamble,        // Frame preamble has been sent (optional state)
    tpst_Info_TxHeaderMAC,       // MAC header has been sent     (optional state)

    // States below indicate that application can now safely reuse memory buffer
    // Note: Requires
    tpst_OK_TxComplete,          // Transmission completed successfully.

    tpst_Error,                  // Error conditions below
    tpst_Error_DelayedTxTooLate, // Delayed transmission could not be started (given TransmitTime already passed or too short).
    tpst_Error_TxNotStarted,     // Transmission did not start (unknown reason).

    tpst_unknown
} e_s_ttc_packettatus_tx;


typedef struct s_ttc_packet_meta_small {    // compressed packet info header storing meta (smaller than t_ttc_packet_meta_debug but enum values are just integers in debugger)
    // Note: fields are arranged to allow optimal packing!

    t_s32                   RSSI_mdBm;    // Received Signal Strength Indicator (unit is milli dBm = 1/1000 dBm). This value often requires extra calculations (->ttc_radio_update_meta()).
#if TTC_PACKET_RANGING==1
    u_ttc_packetimestamp_40 ReceiveTime;  // 40-bit timestamp when this frame has been received
    u_ttc_packetimestamp_40 TransmitTime; // 40-bit timestamp when previous frame has been transmitted (only set in packets received after transmitting a ranging request)
#endif

    // 32 bits in total
    unsigned MaxSizeMAC            : 10;  // maximum allowed size of MAC part of packet (== size of memory block - sizeof(t_ttc_packet_meta) )
    unsigned Type                  :  7;  // MAC Header type of this packet
    unsigned Category              :  3;  // (e_ttc_packet_category) protocol independent type of this packet (allows fast classification)
    unsigned AddressFormat         :  6;  // format of addresses stores in packet
    unsigned IndexPhysical         :  3;  // index written by physical transport layer to identify extra packet meta data being stored outside of this packet (e.g. raw receiver quality data)
    unsigned ReceiveAfterTransmit  :  1;  // ==1: switch on receiver after transmission to receive immediate answer
    unsigned RangingMessage        :  1;  // ==1: message is intended to be used for ranging (low-level driver has to copy value to/ from RNG bit of 802.15.4a PHY Header)
    unsigned DoNotReleaseBuffer    :  1;  // ==1: memory buffer of this packet must not be released by low-level driver (set if StatusTX is to be monitored by application!)

    t_u8                            SizeOfMacHeader; // size of packet header (cached by ttc_packet_mac_header_get_size() to avoid recalculation)
    e_s_ttc_packettatus_tx volatile StatusTX;        // transmission status written by low-level radio driver

    // StatusTX Usage:
    //   1) prevent low-level driver from releasing memory buffer of this packet
    //      Packet->Flags.Bits.DoNotReleaseBuffer=1;
    //   2) send packet via ttc_radio
    //   3) monitor StatusTX until update
    //      while (!Packet->StatusTX) ttc_task_yield();
    //   4) release packet on your own
    //      ttc_heap_pool_block_free(Packet);

#ifdef t_ttc_packet_meta_lowlevel
    t_ttc_packet_meta_lowlevel  LowLevel;          // extra meta data provided by low-level driver
#endif
} __attribute__( ( __packed__ ) ) t_ttc_packet_meta_small;

typedef struct s_ttc_packet_meta_debug {         // debug optimized packet meta header (enum values are readable in debugger)
    // Note: fields are arranged to allow optimal packing!

    t_s32                           RSSI_mdBm;                // Received Signal Strength Indicator (unit is milli dBm = 1/1000 dBm). This value often requires extra calculations (->ttc_radio_update_meta()).
#if TTC_PACKET_RANGING==1
    u_ttc_packetimestamp_40       ReceiveTime;              // 40-bit timestamp when this frame has been received
    u_ttc_packetimestamp_40       TransmitTime;             // 40-bit timestamp when previous frame has been transmitted (only set in packets received after transmitting a ranging request)
#endif

    t_u16                           MaxSizeMAC;               // maximum allowed size of MAC part of packet (== size of memory block - sizeof(t_ttc_packet_meta) )
    e_ttc_packet_type               Type;                     // MAC Header type of this packet

    e_ttc_packet_category           Category;                 // protocol independent type of this packet (allows fast classification)
    e_ttc_packet_address_format     AddressFormat;            // format of addresses stores in packet
    unsigned                        IndexPhysical        : 5; // index written by physical transport layer to identify extra packet meta data being stored outside of this packet (e.g. raw receiver quality data)
    unsigned                        ReceiveAfterTransmit : 1; // ==1: switch on receiver after transmission to receive immediate answer
    unsigned                        RangingMessage       : 1; // ==1: message is intended to be used for ranging (low-level driver has to copy value to/ from RNG bit of 802.15.4a PHY Header)
    unsigned                        DoNotReleaseBuffer   : 1; // ==1: memory buffer of this packet must not be released by low-level driver (set if StatusTX is to be monitored by application!)
    t_u8                            SizeOfMacHeader;          // size of packet header (cached by ttc_packet_mac_header_get_size() to avoid recalculation)
    e_s_ttc_packettatus_tx volatile StatusTX;                 // transmission status written by low-level radio driver (declared as volatile allow busy loop checking this field even with enabled compiler optimizations)
    // StatusTX Usage:
    //   1) prevent low-level driver from releasing memory buffer of this packet
    //      Packet->Flags.Bits.DoNotReleaseBuffer=1;
    //   2) send packet via ttc_radio
    //   3) monitor StatusTX until update
    //      while (!Packet->StatusTX) ttc_task_yield();
    //   4) release packet on your own
    //      ttc_heap_pool_block_free(Packet);

#ifdef t_ttc_packet_meta_lowlevel
    t_ttc_packet_meta_lowlevel      LowLevel;                 // extra meta data provided by low-level driver
#endif
} __attribute__( ( __packed__ ) ) t_ttc_packet_meta_debug;

#if (TTC_ASSERT_PACKET == 2)
    #define t_ttc_packet_meta t_ttc_packet_meta_debug // using debug optimized structure
#else
    #define t_ttc_packet_meta t_ttc_packet_meta_small // using minimum size optimized structure
#endif

/* ArchitectureEnum  is not used in this driver (will be recreated by create_DeviceDriver.pl if deleted)
  E_ttc_packet_type_802154, // automatically added by ./create_DeviceDriver.pl
  ta_packet_802154, // automatically added by ./create_DeviceDriver.pl
//InsertArchitectureEnum  above (DO NOT DELETE THIS LINE!)
*/
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef struct s_ttc_packet {      // packet header for generic network packet
    // t_ttc_heap_block_from_pool PoolBlock;   // memory pool header located before t_ttc_packet

    /** Each packet consists of two parts.
     *  One part is transmitted or received while the other one contains extra informations associated with this packet.
     *  The specific structure of Info and Data is defined by current low-level driver.
     */

    // packet meta information (not transmitted)
    t_ttc_packet_meta Meta;

    // Packet data that is being transmitted/ received (address of MAC must be 4 byte aligned for some radio drivers (e.g. radio_dw1000)!)
    t_ttc_packet_mac  MAC  __attribute__( ( aligned( 4 ) ) );
} t_ttc_packet;

/* Not used (will be recreated by create_DeviceDriver.pl if deleted)

typedef union {  // architecture dependent configuration

#ifdef EXTENSION_packet_802154
    t_packet_802154_config 802154;  // automatically added by ./create_DeviceDriver.pl
#endif
//InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)

} u_ttc_packet_architecture;

typedef struct s_ttc_packet_config { //          architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_packet_init()
    //       and after ttc_packet_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // Hint: Order structure elements for optimal alignment.
    //       You should place smaller datatypes like t_u8 and t_u16 after all t_u32 and pointer types
    //       to prevent insertion of pad-bytes. Most architectures don't support placing 32-bit datatypes at
    //       unaligned address.


    e_ttc_packet_architecture  Architecture;   // type of architecture used for current packet device

    // Additional high-level attributes go here..

} __attribute__((__packed__)) t_ttc_packet_config;
*/

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_PACKET_TYPES_H
