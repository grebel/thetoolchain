#ifndef TTC_GFX_H
#define TTC_GFX_H
/** { ttc_gfx.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for GFX devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for ttc_gfx:
 *  2) configure:      ttc_gfx_get_configuration(LogicalIndex);
 *  3) initialize:     ttc_gfx_init(LogicalIndex);
 *  4) use:            ttc_gfx_XXX(LogicalIndex);
 *  5) switch display: ttc_gfx_switch_to(LogicalIndex);
 *
 *  LogicalIndex enumerates all configured grafic display as 1..ttc_gfx_get_max_index().
 *  If more than one display is configured, then ttc_gfx_switch_to() sets the current display.
 *
 *  ttc_gfx_interface.h provides default implementations for functions not being supported by
 *  current gfx_* low-level driver. Basically, only ttc_gfx_pixel_set_at() must be implemented to
 *  allow grafic output. The more optimized functions are implemented, the faster your display will be.
 *
 *  Created from template ttc_device.h revision 26 at 20140313 10:58:25 UTC
 *
 *  Authors: Gregor Rebel
}*/

#include "compile_options.h"
#ifndef EXTENSION_ttc_gfx
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_gfx.sh
#endif

//{ Includes *************************************************************

#include "ttc_font.h"
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_gfx_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

// global variable for quick access to current display reduces amount of function arguments and pointer derefencing
extern t_ttc_gfx_quick ttc_gfx_Quick;

#if TTC_GFX_ROTATION_CLOCKWISE == 90
    #define ROTATED_X(X, Y) (Y)
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 180
    #define ROTATED_X(X, Y) (TTC_GFX1_WIDTH - 1 - X)
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 270
    #define ROTATED_X(X, Y) (TTC_GFX1_WIDTH - 1 - Y)
#endif
#ifndef ROTATED_X
    #define ROTATED_X(X, Y) (X)
#endif

#if TTC_GFX_ROTATION_CLOCKWISE == 90
    #define ROTATED_Y(X, Y) (TTC_GFX1_HEIGHT - 1 - X)
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 180
    #define ROTATED_Y(X, Y) (TTC_GFX1_HEIGHT - 1 - Y)
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 270
    #define ROTATED_Y(X, Y) (X)
#endif
#ifndef ROTATED_Y
    #define ROTATED_Y(X, Y) (Y)
#endif

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level gfx only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * gfx devices on all supported architectures.
 * Check gfx/gfx_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares gfx Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_gfx_prepare();
void _driver_gfx_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_gfx_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_GFX_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_gfx_config* ttc_gfx_get_configuration( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_GFX_get_max_LogicalIndex())
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_gfx_errorcode ttc_gfx_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_GFX_get_max_LogicalIndex())
 */
e_ttc_gfx_errorcode  ttc_gfx_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed devt_ttc_gfx_config* ttc_gfx_Display.Config;ice
 *
 * Seetings of indexed device are being reset to their default values.
 * Will automatically call ttc_gfx_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of gfx device (1..ttc_gfx_get_max_index() )
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_gfx_errorcode  ttc_gfx_load_defaults( t_u8 LogicalIndex );

/** maps from logical to physical device index
 *
 * High-level gfxs (ttc_gfx_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_GFXn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any
 * order.
 *
 * @param LogicalIndex  logical index of gfx device (1..ttc_gfx_get_max_index() )
 * @return              physical index of gfx device (0 = first physical gfx device, ...)
 */
t_u8 ttc_gfx_logical_2_physical_index( t_u8 LogicalIndex );

/** maps from physical to logical device index
 *
 * This is a reverse function to ttc_gfx_logical_2_physical_index()
 *
 * @param  PhysicalIndex  physical index of gfx device (0 = first physical gfx device, ...)
 * @return                logical index of gfx device (1..ttc_gfx_get_max_index() )
 */
t_u8 ttc_gfx_physical_2_logical_index( t_u8 PhysicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_GFX_get_max_LogicalIndex())
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_gfx_errorcode  ttc_gfx_reset( t_u8 LogicalIndex );


/* start graphical functions ********************************************************************************************/

/** draw graphical element of a single- or double-ended arrow
 *
 * Examples:
 *   _etsc2_gfx_arrow(1,1, 100,100, 0,0);    // draw arrow (1,1) --- (100,100) (no arrow but just simple line)
 *   _etsc2_gfx_arrow(1,1, 100,100, 0,4);    // draw arrow (1,1) --> (100,100) (anchor of size 4 pointing at (100,100)
 *   _etsc2_gfx_arrow(1,1, 100,100,10,0);    // draw arrow (1,1) <-- (100,100) (bigger anchor pointing at (1,1)
 *   _etsc2_gfx_arrow(1,1, 100,100, 6,6);    // draw arrow (1,1) <-> (100,100) (equal sized anchor pointing between (1,1) and (100,100)
 *
 * @param X1       x-coordinate of starting point
 * @param Y1       y-coordinate of starting point
 * @param X2       x-coordinate of end point
 * @param Y2       y-coordinate of end point
 * @param Length1  length of arrow anchor lines at (X1,Y1)
 * @param Length2  length of arrow anchor lines at (X2,Y2)
 */
void ttc_gfx_arrow( t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2, t_u8 Length1, t_u8 Length2 );
void _driver_gfx_arrow( t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2, t_u8 Length1, t_u8 Length2 );
#define ttc_gfx_arrow(X1, Y1, X2, Y2, Length1, Length2)  _driver_gfx_arrow(X1, Y1, X2, Y2, Length1, Length2)  // enable to directly forward function call to low-level driver (remove function definition before!)

/** draws line in current ColorFg from (X,Y) to (X+DX-1, Y+DY-1)
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param X1              x-coordinate of starting point
 * @param Y1              y-coordinate of starting point
 * @param X2              x-coordinate of end point
 * @param Y2              y-coordinate of end point
 */
void ttc_gfx_line( t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2 );
void _driver_gfx_line( t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2 );
#define ttc_gfx_line(X1,Y1,X2,Y2) _driver_gfx_line( ROTATED_X(X1,Y1), ROTATED_Y(X1,Y1), ROTATED_X(X2,Y2), ROTATED_Y(X2,Y2) )

/** draws line in current ColorFg from (X, Y) to (Left+Length-1, Y)
 *
 * Note: Before calling this function, make sure that ttc_gfx_CurrentDisplay has correct value!
 *
 * @param Left            x-coordinate of starting point
 * @param Y               y-coordinate of starting point
 * @param Length          distance to end point of line
 */

void ttc_gfx_line_horizontal( t_u16 X, t_u16 Y, t_s16 Length );
void _driver_gfx_line_horizontal( t_u16 X, t_u16 Y, t_s16 Length );
#if TTC_GFX_ROTATION_CLOCKWISE == 90
    #define  ttc_gfx_line_horizontal(X,Y,Length)  _driver_gfx_line_vertical(ROTATED_X(X,Y), ROTATED_Y(X,Y), Length)
#endif
#if  TTC_GFX_ROTATION_CLOCKWISE == 180
    #define ttc_gfx_line_horizontal(X,Y,Length)   _driver_gfx_line_horizontal(ROTATED_X(X,Y), ROTATED_Y(X,Y),-(Length))
#endif
#if  TTC_GFX_ROTATION_CLOCKWISE == 270
    #define  ttc_gfx_line_horizontal(X,Y,Length)  _driver_gfx_line_vertical(ROTATED_X(X,Y), ROTATED_Y(X,Y), -(Length))
#endif
#ifndef ttc_gfx_line_horizontal
    #define ttc_gfx_line_horizontal(X,Y,Length)   _driver_gfx_line_horizontal(X,Y,Length)
#endif

/** draws line in current ColorFg from (X, Y) to (X, Y + Length-1)
 *
 * Note: Before calling this function, make sure that ttc_gfx_CurrentDisplay has correct value!
 *
 * @param Left            x-coordinate of starting point
 * @param Y               y-coordinate of starting point
 * @param Length          distance to end point of line
 * @return                !=NULL: pointer to struct ttc_display_generic_t
 */

void ttc_gfx_line_vertical( t_u16 X, t_u16 Y, t_s16 Length );
void _driver_gfx_line_vertical( t_u16 X, t_u16 Y, t_s16 Length );
#if TTC_GFX_ROTATION_CLOCKWISE == 90
    #define  ttc_gfx_line_vertical(X,Y,Length)  _driver_gfx_line_horizontal(ROTATED_X(X,Y), ROTATED_Y(X,Y), Length)
#endif
#if  TTC_GFX_ROTATION_CLOCKWISE == 180
    #define ttc_gfx_line_vertical(X,Y,Length)   _driver_gfx_line_vertical(ROTATED_X(X,Y), ROTATED_Y(X,Y), -Length)
#endif
#if  TTC_GFX_ROTATION_CLOCKWISE == 270
    #define  ttc_gfx_line_vertical(X,Y,Length)  _driver_gfx_line_horizontal(ROTATED_X(X,Y), ROTATED_Y(X,Y), -Length)
#endif
#ifndef ttc_gfx_line_vertical
    #define ttc_gfx_line_vertical(X,Y,Length)   _driver_gfx_line_vertical(X,Y,Length)
#endif


/** draws rectangle in current ColorFg from (X,Y) to (X+Width-1, Y+Height-1)
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param X              = left border of rectangle  (in rotated orientation)
 * @param Y              = upper border of rectangle (in rotated orientation)
 * @param PhysicalWidth  = width of rectangle        (in native, not rotated, display orientation)
 * @param PhysicalHeight = height of rectangle       (in native, not rotated, display orientation)
 */
void ttc_gfx_rect( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height );
void _driver_gfx_rect( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height );
#if TTC_GFX_ROTATION_CLOCKWISE == 90
    #define ttc_gfx_rect(X,Y,Width,Height) _driver_gfx_rect(ROTATED_X(X,Y),ROTATED_Y((X+ Width),Y),Height,Width)
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 180
    #define ttc_gfx_rect(X,Y,Width,Height) _driver_gfx_rect(ROTATED_X(X,Y),ROTATED_Y(X,Y),-(Width),-(Height))
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 270
    #define ttc_gfx_rect(X,Y,Width,Height) _driver_gfx_rect(ROTATED_X(X,(Y+Height)),ROTATED_Y((X),Y),-(Height),-(Width))
#endif
#ifndef ttc_gfx_rect
    #define ttc_gfx_rect(X,Y,Width,Height) _driver_gfx_rect(X,Y,Width,Height)
#endif


/** draws filled rectangle in current ColorFg from (X,Y) to (X+Width-1, Y+Height-1)
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param X               x-coordinate of starting point
 * @param Y               y-coordinate of starting point
 * @param Width           distance to corner opposing starting point in x-direction
 * @param Height          distance to corner opposing starting point in y-direction
 */

void ttc_gfx_rect_fill( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height );
void _driver_gfx_rect_fill( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height );

/** draws circle in current ColorFg around (CenterX, CenterY) of radius Radius
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param CenterX         x-coordinate of starting point
 * @param CenterY         y-coordinate of starting point
 * @param Radius          distance to corner opposing starting point in x-direction
 */
void ttc_gfx_circle( t_u16 CenterX, t_u16 CenterY, t_u16 Radius );
void _driver_gfx_circle( t_u16 CenterX, t_u16 CenterY, t_u16 Radius );
#define ttc_gfx_circle(CenterX,CenterY,Radius) _driver_gfx_circle(ROTATED_X(CenterX,CenterY),ROTATED_Y(CenterX,CenterY),Radius)

/** draws filled circle in current ColorFg around (CenterX, CenterY) of radius Radius
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param CenterX         x-coordinate of starting point
 * @param CenterY         y-coordinate of starting point
 * @param Radius          distance to corner opposing starting point in x-direction
 */
void ttc_gfx_circle_fill( t_u16 CenterX, t_u16 CenterY, t_u16 Radius );
void _driver_gfx_circle_fill( t_u16 CenterX, t_u16 CenterY, t_u16 Radius );
#define ttc_gfx_circle_fill(CenterX,CenterY,Radius) _driver_gfx_circle_fill(ROTATED_X(CenterX,CenterY),ROTATED_Y(CenterX,CenterY),Radius)

/** draws circle segment in current ColorFg around (CenterX, CenterY) of radius Radius
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param CenterX         x-coordinate of center point
 * @param CenterY         y-coordinate of center point
 * @param StartAngle      0..360 - circle segment is drawn counter clockwise
 * @param EndAngle        0..360 - circle segment is drawn counter clockwise
 * @param Radius          distance from center point to all points on boundary line
 */
void ttc_gfx_circle_segment( t_u16 CenterX, t_u16 CenterY, t_u16 Radius, t_u16 StartAngle, t_u16 EndAngle );
void _driver_gfx_circle_segment( t_u16 CenterX, t_u16 CenterY, t_u16 Radius, t_u16 StartAngle, t_u16 EndAngle );
#define ttc_gfx_circle_segment(CenterX,CenterY,Radius, StartAngle, EndAngle) _driver_gfx_circle_segment(ROTATED_X(CenterX,CenterY),ROTATED_Y(CenterX,CenterY),Radius, StartAngle + TTC_GFX_ROTATION_CLOCKWISE, EndAngle + TTC_GFX_ROTATION_CLOCKWISE)

/** draws filled circle segment in current ColorFg around (CenterX, CenterY) of radius Radius
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param CenterX         x-coordinate of center point
 * @param CenterY         y-coordinate of center point
 * @param StartAngle      0..360 - circle segment is drawn counter clockwise
 * @param EndAngle        0..360 - circle segment is drawn counter clockwise
 * @param Radius          distance from center point to all points on boundary line
 */
void ttc_gfx_circle_segment_fill( t_u16 CenterX, t_u16 CenterY, t_u16 Radius, t_u16 StartAngle, t_u16 EndAngle );
void _driver_gfx_circle_segment_fill( t_u16 CenterX, t_u16 CenterY, t_u16 Radius, t_u16 StartAngle, t_u16 EndAngle );
#define ttc_gfx_circle_segment_fill  _driver_gfx_circle_segment_fill  // enable to directly forward function call to low-level driver (remove function definition before!)

/** fill complete display with current background color
 *
 */
void ttc_gfx_clear();
void _driver_gfx_clear();
#  define ttc_gfx_clear() _driver_gfx_clear()

/** set background color to given 24-bit value
 *
 * The background color is used by ttc_gfx_clear() and solid font operations
 *
 * @param Color  24-bit color (format 0xrrggbb or predefined color TTC_GFX_COLOR24_*)
 */
void ttc_gfx_color_bg24( t_u32 Color );
void _driver_gfx_color_bg24( t_u32 Color );
#define ttc_gfx_color_bg24(Color) _driver_gfx_color_bg24(Color)

/** set foreground color to given 24-bit value
 *
 * The foreground color is used by all further graphic operations
 *
 * @param Color  24-bit color (format 0xrrggbb or predefined color TTC_GFX_COLOR24_*)
 */
void ttc_gfx_color_fg24( t_u32 Color );
void _driver_gfx_color_fg24( t_u32 Color );
#define ttc_gfx_color_fg24(Color) _driver_gfx_color_fg24(Color)

/** set foreground color to one from a previously set palette
 *
 * The foreground color is used by all further graphic operations
 *
 * @param PaletteIndex  index from palette being set before via ttc_gfx_palette_set()
 */
void ttc_gfx_color_fg_palette( t_u8 PaletteIndex );

/** set background color to one from a previously set palette
 *
 * The background color is used by ttc_gfx_clear() and solid font operations
 *
 * @param PaletteIndex  index from palette being set before via ttc_gfx_palette_set()
 */
void ttc_gfx_color_bg_palette( t_u8 PaletteIndex );

/** define a new palette to be used for further palette operations
 *
 * The background color is used by ttc_gfx_clear() and solid font operations
 *
 * @param Palette   pointer to first element of array of 24-bit colors
 * @param Size      amount of valid entries Palette[0..Size-1]
 */
void ttc_gfx_palette_set( const t_u32* Palette, t_u8 Size );

/** Moves pixel cursor to a new location.
  *
  * Many drawing operations start at the current cursor position.
  * Some drawing operations automatically move the cursor.
  *
  * @param X   x-coordinate of new drawing position
  * @param Y   x-coordinate of new drawing position
  */
void ttc_gfx_cursor_set( t_u16 X, t_u16 Y );
#define ttc_gfx_cursor_set(X,Y) _driver_gfx_cursor_set(ROTATED_X(X,Y), ROTATED_Y(X,Y))

/** Moves text cursor to given text location
  *
  * Coordinates for text operations are calculated as follows:
  * CursorX = TextShiftX + TextX * Font->CharWidth
  * CursorY = TextShiftY + TextY * Font->CharWidth
  *
  * @param DisplayIndex  1..ttc_gfx_get_max_DisplayIndex() index of display to use
  * @param TextX         x-coordinate of new position (characters)
  * @param TextY         y-coordinate of new position (characters)
  */
void ttc_gfx_text_cursor_set( t_u16 TextX, t_u16 TextY );

/** Moves text cursor to given pixel location
  *
  * This functions allows to locate text at pixel precision anywhere on the screen.
  *
  * @param DisplayIndex  1..ttc_gfx_get_max_DisplayIndex() index of display to use
  * @param PixelX        x-coordinate of new position (pixels)
  * @param PixelY        y-coordinate of new position (pixels)
  */
void ttc_gfx_text_cursor_set_precise( t_u16 PixelX, t_u16 PixelY );

/** Moves text cursor one step into one direction.
  *
  * Automatic line-break (at end of line) and page-break (at end of last line).
  * Sets text- and pixel cursor as startpoint for following pixel- or text-output.
  */
void ttc_gfx_text_cursor_right();

/** Moves text cursor one step into one direction
  *
  * Automatic line-break (at end of line) and page-break (at end of last line).
  * Sets text- and pixel cursor as startpoint for following pixel- or text-output.
  */
void ttc_gfx_text_cursor_left();

/** Moves text cursor one step into one direction
  *
  * Automatic line-break (at end of line) and page-break (at end of last line).
  * Sets text- and pixel cursor as startpoint for following pixel- or text-output.
  */
void ttc_gfx_text_cursor_up();

/** Moves text cursor one step into one direction
  *
  * Automatic line-break (at end of line) and page-break (at end of last line).
  * Sets text- and pixel cursor as startpoint for following pixel- or text-output.
  */
void ttc_gfx_text_cursor_down();

/** Set single pixel using current foreground color at given coordinate
 *
 * @param X  x-coordinate (0 = left column of display)
 * @param Y  y-coordinate (0 = top row of display)
 */
void ttc_gfx_pixel_set_at( t_u16 X, t_u16 Y );
void _driver_gfx_pixel_set_at( t_u16 X, t_u16 Y );
#define ttc_gfx_pixel_set_at(X,Y) _driver_gfx_pixel_set_at(ROTATED_X(X,Y), ROTATED_Y(X,Y))

/** Set single pixel using current background color at given coordinate
 *
 * @param X  x-coordinate (0 = left column of display)
 * @param Y  y-coordinate (0 = top row of display)
 */
void ttc_gfx_pixel_clr_at( t_u16 X, t_u16 Y );
void _driver_gfx_pixel_clr_at( t_u16 X, t_u16 Y );
#define ttc_gfx_pixel_clr_at(X,Y) _driver_gfx_pixel_clr_at(ROTATED_X(X,Y), ROTATED_Y(X,Y))

/** set foreground color to given 16-bit value
  *
  * @param Color  16-bit values are display specific use ttc_gfx_color_fg24() for conversion
  */
void ttc_gfx_color_fg16( t_u16 Color );
#define ttc_gfx_color_fg16(Color) _driver_gfx_color_fg16(Color)

/** set background color to given 16-bit value
  *
  * @param Color  16-bit values are display specific use ttc_gfx_color_fg16() for conversion
  */
void ttc_gfx_color_bg16( t_u16 Color );
#define ttc_gfx_color_bg16(Color) _driver_gfx_color_bg16(Color)

/** set background intensity to given 8-bit value
  *
  * @param Level  8-bit values with given intensity
  */
void ttc_gfx_backlight( t_u8 Level );


/** set font to be used for text operations
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param Font            font to use for all further text operations
 */
void ttc_gfx_set_font( const t_ttc_font_data* Font );
void _driver_gfx_set_font( const t_ttc_font_data* Font );

/** Put a solid character with forground colour and background colour
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param Char            character to print
 */

void ttc_gfx_char_solid( t_u8 Char );
void _driver_gfx_char_solid( t_u8 Char );
void _driver_gfx_char_solid_090( t_u8 Char );
void _driver_gfx_char_solid_180( t_u8 Char );
void _driver_gfx_char_solid_270( t_u8 Char );

#if TTC_GFX_ROTATION_CLOCKWISE == 90
    #define ttc_gfx_char_solid(Char) _driver_gfx_char_solid_090(Char)
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 180
    #define ttc_gfx_char_solid(Char) _driver_gfx_char_solid_180(Char)
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 270
    #define ttc_gfx_char_solid(Char) _driver_gfx_char_solid_270(Char)
#endif
#ifndef ttc_gfx_char_solid
    #define ttc_gfx_char_solid(Char) _driver_gfx_char_solid(Char)
#endif

/** Put single chararacter onto clockwise rotated display.
 *
 * @param Char         character to print
 */
void ttc_gfx_char( t_u8 Char );
void _driver_gfx_char( t_u8 Char );
void _driver_gfx_char_090( t_u8 Char );
void _driver_gfx_char_270( t_u8 Char );
void _driver_gfx_char_180( t_u8 Char );

#if TTC_GFX_ROTATION_CLOCKWISE == 90
    #define ttc_gfx_char(Char) _driver_gfx_char_090(Char)
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 180
    #define ttc_gfx_char(Char) _driver_gfx_char_180(Char)
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 270
    #define ttc_gfx_char(Char) _driver_gfx_char_270(Char)
#endif
#ifndef ttc_gfx_char
    #define ttc_gfx_char(char) _driver_gfx_char(char)
#endif

//String operations
/** prints given string at current cursor position (not transparent)
  *
  * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
  * @param String       text to print
  * @param MaxSize      >= 0: no more than this amount of characters will be printed (for constant strings it is safe to provide -1 as argument)
  */
void ttc_gfx_text_solid( const void* String, t_base MaxSize );

/** prints given string at given cursor position (not transparent)
  *
  * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
  * @param X             x-coordinate of new position
  * @param Y         gfx_pixel_set_at    y-coordinate of new position
  * @param String       text to print
  * @param MaxSize      >= 0: no more than this amount of characters will be printed (for constant strings it is safe to provide -1 as argument)
  */
void ttc_gfx_text_at( t_u16 X, t_u16 Y, const void* String, t_base MaxSize );

/** calculates start column required to center given string on display for current font
  *
  * @param String   String to center
  * @param MaxSize  >= 0: no more than this amount of characters will be read (for constant strings it is safe to provide -1 as argument)
  */
t_u16 ttc_gfx_text_center( const void* String, t_base MaxSize );

/** prints given string at current cursor position (transparent)
  *
  * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
  * @param String       text to print
  * @param MaxSize      >= 0: no more than this amount of characters will be printed (for constant strings it is safe to provide -1 as argument)
  */
void ttc_gfx_text( const void* String, t_base MaxSize );

/** Clears current text row starting at current position
 *
 * Note: You may set cursor position by calling ttc_gfx_text_cursor_set()
 *       Current cursor position will not be changed
 *
 * @param AmountCharacters  >0: amount of characters to clear; ==-1: clear until end of line
 */
void ttc_gfx_text_clear( t_u16 AmountCharacters );

/** prints given string at given coordinates (transparent)
  *
  * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
  * @param X            horizontal pixel coordinate of upper left corner of text
  * @param Y            vertical pixel coordinate of upper left corner of text
  * @param String       text to print
  * @param MaxSize      >= 0: no more than this amount of characters will be printed (for constant strings it is safe to provide -1 as argument)
  */
//X DEPRECATED void ttc_gfx_text_between(t_u16 X, t_u16 Y, const void* String, t_base MaxSize);

/** prints string in given memory block at current cursor position (transparent)
  *
  * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
  * @param Block       text to print
  */
void ttc_gfx_text_block( t_ttc_heap_block* Block );

/** Prints string in given memory block at current cursor position (intransparent)
  *
  * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
  * @param Block       text to print
  */
void ttc_gfx_text_block_solid( t_ttc_heap_block* Block );

/** Prints string with foreground / background color.
  *
  * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
  * @param X       x position
  * @param Y       y position
  * @param String   given String
  * @param MaxSize   max Length of String
*/

void ttc_gfx_text_solid_at( t_u16 X, t_u16 Y, const void* String, t_base MaxSize );

/** Prints given string at current cursor position + surrounds it by a rectangle
  *
  * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
  * @param String       text to print
  * @param MaxSize      >= 0: no more than this amount of characters will be printed (for constant strings it is safe to provide -1 as argument)
  * @param InnerSpacing extra space between rectangle and text (pixels)
  */
void ttc_gfx_text_boxed( const void* String, t_base MaxSize, t_u8 InnerSpacing );

/** Calculates pixel width of given text when printed with current font
 *
 * @param   (const void*)  Text  text which width on display should be calculated
 * @return        (t_u16)  horizontal amount of pixels required to display Text with current font
 */
t_u16 ttc_gfx_text_width( const void* Text );

/** Set display to be used for stdout functions
  *
  * @param DisplayIndex  1..ttc_gfx_get_max_DisplayIndex() index of display to use
  */
void ttc_gfx_stdout_set( t_u8 DisplayIndex );

/** Print all characters from given block onto stdout display
  *
  * Note: use in conjunction with ttc_string.c:ttc_string_register_stdout()
  * Note: call ttc_gfx_stdout_set() before to change to select desired display (multi-displays only)
  */
void ttc_gfx_stdout_print_block( t_ttc_heap_block* Block );

/** Print all characters from given string until first zero byte onto stdout display
  *
  * Note: use in conjunction with ttc_string.c:ttc_string_register_stdout()
  * Note: call ttc_gfx_stdout_set() before to change to select desired display (multi-displays only)
  */
void ttc_gfx_stdout_print_string( const t_u8* String, t_base MaxSize );

/** Draw L-shaped polygon.
 *
 * L-shaped polygons are quite often used in GUI layout because they require less
 * parameters than two boxes and do not require to set the pixels inside its corner twice.
 *                           //                <- Width ->
 * @param ExternalX (t_s32)  // ExternalX/Y--> X##########          A
 * @param ExternalY (t_s32)  //                ###########          |
 * @param Width     (t_s32)  //                ###X <-- InternalX/Y |Height
 * @param Height    (t_s32)  //                ####                 |
 * @param InternalX (t_s32)  //                ####                 |
 * @param InternalY (t_s32)  //                ####                 V
 */
void ttc_gfx_polygon_l( t_s32 ExternalX, t_s32 ExternalY, t_s32 Width, t_s32 Height, t_s32 InternalX, t_s32 InternalY );


/** Switch to different display
 *
 * All ttc_gfx draw operations operate on the current display only.
 * When using multiple displays, call this function to switch to another display before
 * calling ttc_gfx_*() functions for it.
 *
 * @param LogicalIndex (t_u8)  Logical index of gfx instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_GFX<n> lines in extensions.active/makefile
 */
void ttc_gfx_switch_to( t_u8 LogicalIndex );

/** Select or deselect display to be able to communicate with
 *
 * Many displays have a dedicated select input with makes them listen to a shared bus.
 *
 * @param Config       (t_ttc_gfx_config*)  Configuration of gfx device as returned by ttc_gfx_get_configuration()
 * @param Select       (BOOL)               ==1: display is selected for communication; ==0: display is put on hold and free the bus
 */
void ttc_gfx_select( t_ttc_gfx_config* Config );
// Important: Line below must be kept disabled but is not to be removed!
//            It is required so that create_DeviceDriver.pl can forward it into low-level drivers.
//            It must be disabled as this function is often defined as a special macro to be superfast.
#if 0
    void _driver_gfx_select( t_ttc_gfx_config* Config );
#endif
#define ttc_gfx_select _driver_gfx_select

/** Select or deselect display to be able to communicate with
 *
 * Many displays have a dedicated select input with makes them listen to a shared bus.
 *
 * @param Config       (t_ttc_gfx_config*)  Configuration of gfx device as returned by ttc_gfx_get_configuration()
 * @param Select       (BOOL)               ==1: display is selected for communication; ==0: display is put on hold and free the bus
 */
void ttc_gfx_deselect( t_ttc_gfx_config* Config );
// Important: Line below must be kept disabled but is not to be removed!
//            It is required so that create_DeviceDriver.pl can forward it into low-level drivers.
//            It must be disabled as this function is often defined as a special macro to be superfast.
#if 0
    void _driver_gfx_deselect( t_ttc_gfx_config* Config );
#endif
#define ttc_gfx_deselect _driver_gfx_deselect

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_gfx(t_u8 LogicalIndex)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_heap_ are passed to interfaces/ttc_heap_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declargfx_pixel_set_atation is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl heap UPDATE
 */


/** fills out given Config with maximum valid values for indexed GFX
 * @param Config        = pointer to struct t_ttc_gfx_config
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_gfx_errorcode _driver_gfx_get_features( t_ttc_gfx_config* Config );

/** shutdown single GFX unit device
 * @param Config        pointer to struct t_ttc_gfx_config
 * @return              == 0: GFX has been shutdown successfully; != 0: error-code
 */
e_ttc_gfx_errorcode _driver_gfx_deinit( t_ttc_gfx_config* Config );

/** initializes single GFX unit for operation
 * @param Config        pointer to struct t_ttc_gfx_config
 * @return              == 0: GFX has been initialized successfully; != 0: error-code
 */
e_ttc_gfx_errorcode _driver_gfx_init( t_ttc_gfx_config* Config );

/** loads configuration of indexed GFX unit with default values
 * @param Config        pointer to struct t_ttc_gfx_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_gfx_errorcode _driver_gfx_load_defaults( t_ttc_gfx_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_gfx_config
 */
e_ttc_gfx_errorcode _driver_gfx_reset( t_ttc_gfx_config* Config );

void _driver_gfx_pixel_set();

void _driver_gfx_pixel_clr();



/** Moves pixel cursor to a new location.
  *
  * Many drawing operations start at the current cursor position.
  * Some drawing operations automatically move the cursor.
  *
  * @param X   x-coordinate of new drawing position
  * @param Y   x-coordinate of new drawing position
  */
void _driver_gfx_cursor_set( t_u16 X, t_u16 Y );


void _driver_gfx_color_bg_palette( t_u8 PaletteIndex );

void _driver_gfx_color_fg_palette( t_u8 PaletteIndex );

void _driver_gfx_palette_set( t_u16* Palette, t_u8 Size );


/** set foreground color to given 16-bit value
  *
  * @param Color  16-bit values are display specific use ttc_gfx_color_fg24() for conversion
  */
void _driver_gfx_color_fg16( t_u16 Color );
/** set background color to given 16-bit value
  *
  * @param Color  16-bit values are display specific use ttc_gfx_color_fg24() for conversion
  */
void _driver_gfx_color_bg16( t_u16 Color );
/** set background intensity to given 8-bit value
  *
  * @param Level  8-bit values with given intensity
  */
void _driver_gfx_backlight( t_u8 Level );

//} Function prototypes
//{ private functions

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_GFX_H
