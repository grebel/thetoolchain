#ifndef t_ttc_mutex_types_H
#define t_ttc_mutex_types_H

/*{ t_ttc_mutex_types.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Architecture independent support for inter task communication & synchronization.
 *
 * Currently supported architectures/ schedulers: CortexM3, FreeRTOS
 *
}*/

//{ includes

#include "compile_options.h"

// include low-level driver types
//
// Required defines/ typedefs:
// t_ttc_mutex   must be a typedef to datatype to store data of a single mutex on current architecture

#ifdef EXTENSION_ttc_mutex_cortexm3
    #include "cpu/cm3_mutex_types.h"
#endif

#ifdef EXTENSION_ttc_mutex_cortexm0
    #include "cm0/cm0_mutex_types.h"
#endif

#ifdef EXTENSION_ttc_mutex_freertos
    #include "scheduler/freertos_mutex_types.h"
#endif

#ifndef t_ttc_mutex
    #error Missing low-level definition for t_ttc_mutex
#endif

#include "ttc_basic_types.h"
#include "ttc_task_types.h"

typedef enum ttc_mutex_error_E {      // return type of most functions in ttc_mutex
    tme_OK,                           // no error
    tme_WasLockedByTask,              // an _isr()-call occured while Mutex was locked by a task

    tme_Error,
    // Error Conditions below
    tme_TimeOut,                      // an operation has timed out
    tme_NotANumber,                   // invalid value given
    tme_NotImplemented,               // required function not implemented for current architecture
    tme_MutexAlreadyLocked,           // tried to lock a mutex that is already locked
    tme_UNKNOWN
} e_ttc_mutex_error;

//}includes
//{ Default settings (change via makefile) ******************************


#ifndef TTC_MUTEX_TIMEOUT_USECS
    #define TTC_MUTEX_TIMEOUT_USECS 1000000 // >0: activate timeout-assert for endless mutexes (TimeOut==-1) in usecs
    // #define TTC_MUTEX_TIMEOUT_USECS 0    // no forced timeout
#endif

//}Default settings
//{ Types & Structures ***************************************************

#ifndef TTC_ASSERT_MUTEX    // any previous definition set (Makefile)?
    #define TTC_ASSERT_MUTEX 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_MUTEX == 1)  // use Assert()s in MUTEX code (somewhat slower but alot easier to debug)
    #define Assert_MUTEX(Condition, Origin) Assert( ((Condition) != 0), Origin)
    #define Assert_MUTEX_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_MUTEX_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in MUTEX code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_MUTEX(Condition, Origin)
    #define Assert_MUTEX_Writable(Address, Origin)
    #define Assert_MUTEX_Readable(Address, Origin)
#endif


#ifndef TTC_ASSERT_MUTEX_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_MUTEX_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_MUTEX_EXTRA == 1)  // use Assert()s in MUTEX code (somewhat slower but alot easier to debug)
    #define Assert_MUTEX_EXTRA(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_MUTEX_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_MUTEX_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in MUTEX code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_MUTEX_EXTRA(Condition, Origin)
    #define Assert_MUTEX_EXTRA_Writable(Address, Origin)
    #define Assert_MUTEX_EXTRA_Readable(Address, Origin)
#endif

// enable use of smart mutexes
#ifndef TTC_SMART_MUTEXES
    #define TTC_SMART_MUTEXES 1
#endif

// define amount
#if TTC_SMART_MUTEXES == 1

    // set amount of entries to be stored in history of smart mutex usage (debugging only)
    #ifndef TTC_SMART_MUTEX_HISTORY_SIZE
        #define TTC_SMART_MUTEX_HISTORY_SIZE 0
    #endif

#endif

// debuggable mutex that can store its owner
// This comes handy if you wanna know, who has locked a mutex before.
#if TTC_SMART_MUTEXES == 1
typedef struct s_ttc_mutex_smart {
    t_ttc_mutex Mutex;
#if CM3_MUTEX_SIZE == 8
    t_u8 PadByte1;
    t_u8 PadByte2;
    t_u8 PadByte3;
#endif
#if CM3_MUTEX_SIZE == 16
    t_u8 PadByte1;
    t_u8 PadByte2;
#endif

    // return address of caller of lock() function
    void ( *LastCaller )();

    // task info of owner
    volatile t_ttc_task_info* OwnerTask;

} t_ttc_mutex_smart;
#else
typedef t_ttc_mutex t_ttc_mutex_smart;
#endif

//}

/** called while waiting for mutex lock. Place your breakpoint inside to debug hanging locks
  */
void _ttc_mutex_wait();



#endif
