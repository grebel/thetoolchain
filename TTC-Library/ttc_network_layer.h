#ifndef TTC_NETWORK_LAYER_H
#define TTC_NETWORK_LAYER_H
/** { ttc_network_layer.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for NETWORK_LAYER devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_NETWORK_LAYER(tc_network_layer_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_network_layer_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_network_layer_init(LogicalIndex);
 *  4) use:         ttc_network_layer_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level network_layer and application.
 *
 *  Created from template ttc_device.h revision 25 at 20140224 16:21:32 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef EXTENSION_ttc_network_layer
#error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_network_layer.sh
#endif

//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_network_layer_interface.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level network_layer only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * network_layer devices on all supported architectures.
 * Check network_layer/network_layer_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares network_layer Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_network_layer_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_network_layer_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_NETWORK_LAYER_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_network_layer_config* ttc_network_layer_get_configuration( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_NETWORK_LAYER_get_max_LogicalIndex())
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_network_layer_errorcode ttc_network_layer_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_NETWORK_LAYER_get_max_LogicalIndex())
 */
e_ttc_network_layer_errorcode  ttc_network_layer_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Seetings of indexed device are being reset to their default values.
 * Will automatically call ttc_network_layer_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of network_layer device (1..ttc_network_layer_get_max_index() )
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_network_layer_errorcode  ttc_network_layer_load_defaults( t_u8 LogicalIndex );

/** maps from logical to physical device index
 *
 * High-level network_layers (ttc_network_layer_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_NETWORK_LAYERn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any
 * order.
 *
 * @param LogicalIndex  logical index of network_layer device (1..ttc_network_layer_get_max_index() )
 * @return              physical index of network_layer device (0 = first physical network_layer device, ...)
 */
t_u8 ttc_network_layer_logical_2_physical_index( t_u8 LogicalIndex );

/** maps from physical to logical device index
 *
 * This is a reverse function to ttc_network_layer_logical_2_physical_index()
 *
 * @param  PhysicalIndex  physical index of network_layer device (0 = first physical network_layer device, ...)
 * @return                logical index of network_layer device (1..ttc_network_layer_get_max_index() )
 */
t_u8 ttc_network_layer_physical_2_logical_index( t_u8 PhysicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_NETWORK_LAYER_get_max_LogicalIndex())
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_network_layer_errorcode  ttc_network_layer_reset( t_u8 LogicalIndex );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_network_layer(t_u8 LogicalIndex)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Each function is given as a prototype and checked for a matching _driver_*() definition.
 * The more functions are implemented by low-level driver, the more features can be used on current architecture.
 *
 * Note: functions declared below are passed to interfaces/ttc_network_layer_interface.h
 *
 * Note: If you add a _driver_* prototype here, use create_DeviceDriver.pl to automatically add
 *       empty functions in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 *       cd templates/; ./create_DeviceDriver.pl network_layer UPDATE
 */

/** Prepares network_layer driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void _driver_network_layer_prepare();

/** fills out given Config with maximum valid values for indexed NETWORK_LAYER
 * @param Config        = pointer to struct t_ttc_network_layer_config (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_network_layer_errorcode _driver_network_layer_get_features( t_ttc_network_layer_config* Config );

/** shutdown single NETWORK_LAYER unit device
 * @param Config        pointer to struct t_ttc_network_layer_config (must have valid value for PhysicalIndex)
 * @return              == 0: NETWORK_LAYER has been shutdown successfully; != 0: error-code
 */
e_ttc_network_layer_errorcode _driver_network_layer_deinit( t_ttc_network_layer_config* Config );

/** initializes single NETWORK_LAYER unit for operation
 * @param Config        pointer to struct t_ttc_network_layer_config (must have valid value for PhysicalIndex)
 * @return              == 0: NETWORK_LAYER has been initialized successfully; != 0: error-code
 */
e_ttc_network_layer_errorcode _driver_network_layer_init( t_ttc_network_layer_config* Config );

/** loads configuration of indexed NETWORK_LAYER unit with default values
 * @param Config        pointer to struct t_ttc_network_layer_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_network_layer_errorcode _driver_network_layer_load_defaults( t_ttc_network_layer_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_network_layer_config (must have valid value for PhysicalIndex)
 */
e_ttc_network_layer_errorcode _driver_network_layer_reset( t_ttc_network_layer_config* Config );
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_NETWORK_LAYER_H
