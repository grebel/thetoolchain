/** { ttc_register_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for REGISTER device.
 *  Structures, Enums and Defines being required by both, high- and low-level register.
 *
 *  Created from template ttc_device_types.h revision 22 at 20140223 10:54:39 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_REGISTER_TYPES_H
#define TTC_REGISTER_TYPES_H

/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_REGISTER 0        # disable default asserts for REGISTER driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_REGISTER_EXTRA 1  # enable extra asserts for REGISTER driver
 *
 */
#ifndef TTC_ASSERT_REGISTER    // any previous definition set (Makefile)?
    #define TTC_ASSERT_REGISTER 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_REGISTER == 1)  // use Assert()s in REGISTER code (somewhat slower but alot easier to debug)
    #define Assert_REGISTER(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_REGISTER_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_REGISTER_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in REGISTER code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_REGISTER(Condition, Origin)
    #define Assert_REGISTER_Writable(Address, Origin)
    #define Assert_REGISTER_Readable(Address, Origin)
#endif


#ifndef TTC_ASSERT_REGISTER_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_REGISTER_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_REGISTER_EXTRA == 1)  // use Assert()s in REGISTER code (somewhat slower but alot easier to debug)
    #define Assert_REGISTER_EXTRA(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_REGISTER_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_REGISTER_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in REGISTER code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_REGISTER_EXTRA(Condition, Origin)
    #define Assert_REGISTER_EXTRA_Writable(Address, Origin)
    #define Assert_REGISTER_EXTRA_Readable(Address, Origin)
#endif
//}
//{ Includes *************************************************************

#include "ttc_basic_types.h"
#ifdef EXTENSION_register_stm32w1xx
    #include "register/register_stm32w1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_register_stm32f1xx
    #include "register/register_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_register_cortexm3
    #include "register/register_cortexm3_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_register_stm32l1xx
    #include "register/register_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_register_stm32l0xx
    #include "register/register_stm32l0xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

// TTC_REGISTERn has to be defined as constant by makefile.100_board_*
#ifdef TTC_REGISTER5
    #ifndef TTC_REGISTER4
        #error TTC_REGISTER5 is defined, but not TTC_REGISTER4 - all lower TTC_REGISTERn must be defined!
    #endif
    #ifndef TTC_REGISTER3
        #error TTC_REGISTER5 is defined, but not TTC_REGISTER3 - all lower TTC_REGISTERn must be defined!
    #endif
    #ifndef TTC_REGISTER2
        #error TTC_REGISTER5 is defined, but not TTC_REGISTER2 - all lower TTC_REGISTERn must be defined!
    #endif
    #ifndef TTC_REGISTER1
        #error TTC_REGISTER5 is defined, but not TTC_REGISTER1 - all lower TTC_REGISTERn must be defined!
    #endif

    #define TTC_REGISTER_AMOUNT 5
#else
    #ifdef TTC_REGISTER4
        #define TTC_REGISTER_AMOUNT 4

        #ifndef TTC_REGISTER3
            #error TTC_REGISTER5 is defined, but not TTC_REGISTER3 - all lower TTC_REGISTERn must be defined!
        #endif
        #ifndef TTC_REGISTER2
            #error TTC_REGISTER5 is defined, but not TTC_REGISTER2 - all lower TTC_REGISTERn must be defined!
        #endif
        #ifndef TTC_REGISTER1
            #error TTC_REGISTER5 is defined, but not TTC_REGISTER1 - all lower TTC_REGISTERn must be defined!
        #endif
    #else
        #ifdef TTC_REGISTER3

            #ifndef TTC_REGISTER2
                #error TTC_REGISTER5 is defined, but not TTC_REGISTER2 - all lower TTC_REGISTERn must be defined!
            #endif
            #ifndef TTC_REGISTER1
                #error TTC_REGISTER5 is defined, but not TTC_REGISTER1 - all lower TTC_REGISTERn must be defined!
            #endif

            #define TTC_REGISTER_AMOUNT 3
        #else
            #ifdef TTC_REGISTER2

                #ifndef TTC_REGISTER1
                    #error TTC_REGISTER5 is defined, but not TTC_REGISTER1 - all lower TTC_REGISTERn must be defined!
                #endif

                #define TTC_REGISTER_AMOUNT 2
            #else
                #ifdef TTC_REGISTER1
                    #define TTC_REGISTER_AMOUNT 1
                #else
                    #define TTC_REGISTER_AMOUNT 0
                #endif
            #endif
        #endif
    #endif
#endif

/** Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_register_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_register_architecture
    #warning Missing low-level definition for t_ttc_register_architecture (using default)
    #define t_ttc_register_architecture void
#endif

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_register_physical_index (starts at zero!)
    //
    // You may use this enum to assign physical index to logical TTC_REGISTERn constants in your makefile
    //
    // E.g.: Defining physical device #3 of register as first logical device TTC_REGISTER1
    //       COMPILE_OPTS += -DTTC_REGISTER1=INDEX_REGISTER3
    //
    INDEX_REGISTER1,
    INDEX_REGISTER2,
    INDEX_REGISTER3,
    INDEX_REGISTER4,
    INDEX_REGISTER5,
    INDEX_REGISTER6,
    INDEX_REGISTER7,
    INDEX_REGISTER8,
    INDEX_REGISTER9,
    // add more if needed

    INDEX_REGISTER_ERROR
} e_ttc_register_physical_index;
typedef enum {    // e_ttc_register_errorcode     return codes of REGISTER devices
    ec_register_OK = 0,

    // other warnings go here..

    ec_register_ERROR,                 // general failure
    ec_register_NULL,                  // NULL pointer not accepted
    ec_register_DeviceNotFound,        // corresponding device could not be found
    ec_register_InvalidImplementation, // internal self-test failed
    ec_register_InvalidConfiguration,  // sanity check of device configuration failed

    // other failures go here..

    ec_register_unknown                // no valid errorcodes past this entry
} e_ttc_register_errorcode;
typedef enum {    // e_ttc_register_architecture  types of architectures supported by REGISTER driver
    ta_register_None,           // no architecture selected


    ta_register_stm32w1xx, // automatically added by ./create_DeviceDriver.pl
    ta_register_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
    ta_register_cortexm3, // automatically added by ./create_DeviceDriver.pl
    ta_register_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_register_unknown        // architecture not supported
} e_ttc_register_architecture;

#define t_ttc_register_config t_ttc_register_architecture

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_REGISTER_TYPES_H
