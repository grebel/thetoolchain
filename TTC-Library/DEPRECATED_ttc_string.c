/*{ ttc_string.c ***********************************************
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Architecture independent support for operations on zero terminated character sequences (strings).
 *
}*/
#include "DEPRECATED_ttc_string.h"

//{ global variables *******************************************

// central function providing STDOUT like output of memory blocks
// Note: do not call this function directly, use ttc_string_block() instead!
// Note: do not write to this variable directly, use ttc_string_register_stdout() instead!
//
void ( *_ttc_string_stdout_block )( ttc_heap_block_t* Block ) = NULL;

// central function providing STDOUT like output of strings
// Note: do not call this function directly, use
//       ttc_string_print()/ ttc_string_printf() instead!
// Note: do not write to this variable directly, use ttc_string_register_stdout() instead!
//
void ( *_ttc_string_stdout )( const u8_t* String, Base_t MaxSize ) = NULL;

// buffer for temporary string (allocated on first use)
// ttc_string_Buffer[TTC_STRING_PRINTF_BUFFERSIZE]
static u8_t* ttc_string_Buffer = NULL;

// points to last valid field of ttc_string_Buffer[]
static u8_t* ttc_string_BufferEnd = NULL;

#ifdef TTC_MULTITASKING_SCHEDULER
// Protect single ttc_string_Buffer[] from concurrent calls
static ttc_mutex_smart_t* ttc_string_Lock = NULL;
#endif

//}global variables
//{ functions *************************************************

void ttc_string_strip( u8_t** Start, Base_t* Size ) {
    Assert_String( Start, ec_InvalidArgument );
    Assert_String( Size,  ec_InvalidArgument );

    u8_t* Reader   = *Start;
    Base_t NewSize = *Size;

    // remove leading whitespaces
    while ( ( NewSize > 0 ) && ttc_string_is_whitechar( *Reader ) ) {
        Reader++;
        NewSize--;
    }
    *Start = Reader;

    // remove trailing whitespaces
    Reader = *Start + NewSize - 1;
    while ( ( NewSize > 0 ) && ttc_string_is_whitechar( *Reader ) ) {
        Reader--;
        NewSize--;
    }
    *Size = NewSize;
}
BOOL ttc_string_is_whitechar( u8_t ASCII ) {
    if ( ASCII < 32 ) { return TRUE; }

    return FALSE;
}
Base_t ttc_string_copy( u8_t* Target, const u8_t* Source, Base_t MaxSize ) {
    Assert_String( Target, ec_InvalidArgument );
    Assert_String( Source, ec_InvalidArgument );

    Base_t BytesCopied = 0;
    u8_t C = 0;

    MaxSize--; // reserve 1 byte for string terminator

    while ( ( BytesCopied < MaxSize ) && ( ( C = *Source++ ) != 0 ) ) {
        *Target++ = C;
        BytesCopied++;
    }
    *Target++ = 0;          // terminate string in Target[]

    return BytesCopied;
}
void ttc_string_print( const u8_t* String, Base_t MaxSize ) {
    Assert_String( String, ec_InvalidArgument );

    if ( _ttc_string_stdout ) {
        _ttc_string_stdout( String, MaxSize );
    }
}

typedef struct { // internal flags used by ttc_string_snprintf() only
    unsigned LeadingZeros : 1;
    unsigned InsideFormat : 1;
    signed MinLength    : 6;
} __attribute__( ( __packed__ ) ) tss_flags_t;

u8_t* ttc_string_allocf( const char* Format, ... ) {

    return NULL;
}
void ttc_string_printf( const char* Format, ... ) {
    Assert_String( Format, ec_InvalidArgument );
    _ttc_string_lock();

    va_list Arguments;
    va_start( Arguments, Format );
    if ( 1 ) { // passing VARARGS to ttc_string_snprintf()
        _ttc_string_snprintf( _ttc_string_get_buffer(), TTC_STRING_PRINTF_BUFFERSIZE, Format, Arguments );
        va_end( Arguments );
    }
    else {   // using VARARGS on our own (double implementation of ttc_string_snprintf() )
        // this is a protected function: use of static variables reduces stack usage
        static u8_t*       tsp_Writer;
        static u8_t*       tsp_Reader_Format;
        static tss_flags_t tsp_Flags;
        static u8_t        tsp_C;
        tsp_Writer        = _ttc_string_get_buffer();
        tsp_Reader_Format = ( u8_t* ) Format;
        tsp_C = 0;
        Base_signed_t Value;

        for ( ; ( ( tsp_C = *tsp_Reader_Format ) != 0 ) && ( tsp_Writer < ttc_string_BufferEnd ); tsp_Reader_Format++ ) {
            if ( tsp_C != '%' ) { // copy character
                *tsp_Writer++ = tsp_C;
            }
            else {          // interpret format string
                // reset flags for new format
                tsp_Flags.MinLength    = 0;
                tsp_Flags.LeadingZeros = 0;
                tsp_Flags.InsideFormat = 1;

                do {
                    tsp_Reader_Format++;
                    tsp_C = *tsp_Reader_Format;

                    switch ( tsp_C ) {

                        case 'c': { // single character
                            *tsp_Writer++ = ( u8_t ) va_arg( Arguments, int );
                            tsp_Flags.InsideFormat = 0;
                            break;
                        }
                        case '0': { // ĺeading zeros
                            tsp_Flags.LeadingZeros = 1;
                            break;
                        }
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9': { // fixed length
                            while ( ( tsp_C > '0' ) && ( tsp_C <= '9' ) ) {
                                tsp_Flags.MinLength = tsp_Flags.MinLength * 10 + tsp_C - '0';
                                tsp_C = *++tsp_Reader_Format;
                            }
                            tsp_Reader_Format--;
                            break;
                        }
                        case 'd':
                        case 'i': { // decimal integer
                            Value = va_arg( Arguments, Base_signed_t );

                            if ( tsp_Flags.MinLength ) { // prefix with spaces/ zeroes
                                tsp_Flags.MinLength -= ttc_string_log_decimal( Value );
                                if ( Value < 0 )
                                { tsp_Flags.MinLength--; } // take leading minus into account
                                if ( tsp_Flags.LeadingZeros ) {
                                    while ( ( tsp_Flags.MinLength-- > 0 )  && ( tsp_Writer < ttc_string_BufferEnd ) )
                                    { *tsp_Writer++ = '0'; }
                                }
                                else {
                                    while ( ( tsp_Flags.MinLength-- > 0 )  && ( tsp_Writer < ttc_string_BufferEnd ) )
                                    { *tsp_Writer++ = ' '; }
                                }
                            }

                            tsp_Writer += ttc_itoa( Value, tsp_Writer, ttc_string_BufferEnd - tsp_Writer );
                            tsp_Flags.InsideFormat = 0;
                            break;
                        }
                        case 'x': { // hexadecimal integer
                            Value = va_arg( Arguments, Base_t );

                            if ( tsp_Flags.MinLength ) { // prefix with spaces/ zeroes
                                tsp_Flags.MinLength -= ttc_string_log_hex( Value );
                                if ( tsp_Flags.LeadingZeros ) {
                                    while ( ( tsp_Flags.MinLength-- > 0 )  && ( tsp_Writer < ttc_string_BufferEnd ) )
                                    { *tsp_Writer++ = '0'; }
                                }
                                else {
                                    while ( ( tsp_Flags.MinLength-- > 0 )  && ( tsp_Writer < ttc_string_BufferEnd ) )
                                    { *tsp_Writer++ = ' '; }
                                }
                            }

                            tsp_Writer += ttc_xtoa( Value, tsp_Writer, ttc_string_BufferEnd - tsp_Writer );
                            tsp_Flags.InsideFormat = 0;
                            break;
                        }
                        case 's': { // string
                            tsp_Writer += ttc_string_copy( tsp_Writer, ( u8_t* ) va_arg( Arguments, char* ), ttc_string_BufferEnd - tsp_Writer );
                            tsp_Flags.InsideFormat = 0;
                            break;
                        }
                        default: {
                            if ( tsp_C ) {
                                *tsp_Writer++ = tsp_C;
                            }
                            tsp_Flags.InsideFormat = 0;
                            break;
                        }
                    }
                }
                while ( ( tsp_C != 0 ) && tsp_Flags.InsideFormat && ( tsp_Writer < ttc_string_BufferEnd ) );
            }
        }

        if ( tsp_Writer < ttc_string_BufferEnd )
        { *tsp_Writer = 0; }
        else
        { *( tsp_Writer - 1 ) = 0; }
        va_end( Arguments );
    }

    ttc_string_print( ttc_string_Buffer, ( Base_t ) TTC_STRING_PRINTF_BUFFERSIZE );

    #ifdef TTC_MULTITASKING_SCHEDULER
    ttc_mutex_unlock( ttc_string_Lock );
    #endif
}
u8_t* ttc_string_snprintf( u8_t* Buffer, Base_t Size, const char* Format, ... ) {
    Assert_String( Format, ec_InvalidArgument );

    va_list Arguments;
    va_start( Arguments, Format );
    Buffer = _ttc_string_snprintf( Buffer, Size, Format, Arguments );
    va_end( Arguments );

    return Buffer;
}
Base_t ttc_string_appendf( u8_t* Buffer, Base_t* Size, const char* Format, ... ) {
    Assert_String( Format, ec_InvalidArgument );
    Assert_String( Size,   ec_InvalidArgument );

    va_list Arguments;
    va_start( Arguments, Format );
    u8_t* End = _ttc_string_snprintf( Buffer, *Size, Format, Arguments );
    va_end( Arguments );

    Base_t BytesAppended = End - Buffer;
    *Size -= BytesAppended;

    return BytesAppended;
}
u8_t* _ttc_string_snprintf( u8_t* Buffer, Base_t Size, const char* Format, va_list Arguments ) {
    u8_t* Writer        = ( u8_t* ) Buffer;
    u8_t* BufferEnd     = ( u8_t* ) Buffer + Size;
    u8_t* Reader_Format = ( u8_t* ) Format;
    tss_flags_t Flags;

    u8_t C = 0;
    Base_signed_t Value;

    for ( ; ( ( C = *Reader_Format ) != 0 ) && ( Writer < BufferEnd ); Reader_Format++ ) {
        if ( C != '%' ) { // copy character
            *Writer++ = C;
        }
        else {          // interpret format string
            // reset flags for new format
            Flags.MinLength    = 0;
            Flags.LeadingZeros = 0;
            Flags.InsideFormat = 1;

            do {
                Reader_Format++;
                C = *Reader_Format;

                switch ( C ) {

                    case 'c': { // single character
                        *Writer++ = ( u8_t ) va_arg( Arguments, int );
                        Flags.InsideFormat = 0;
                        break;
                    }
                    case '0': { // Leading zeros
                        Flags.LeadingZeros = 1;
                        break;
                    }
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9': { // fixed length
                        u8_t MinLength = 0;
                        while ( ( C > '0' ) && ( C <= '9' ) ) {
                            MinLength = MinLength * 10 + C - '0';
                            C = *++Reader_Format;
                        }
                        if ( MinLength < 32 )
                        { Flags.MinLength = MinLength; }
                        else
                        { Flags.MinLength = 31; }

                        Reader_Format--;
                        break;
                    }
                    case 'd':
                    case 'u':
                    case 'i': { // decimal integer
                        Value = va_arg( Arguments, Base_signed_t );

                        if ( Flags.MinLength ) { // prefix with spaces/ zeroes
                            Flags.MinLength -= ttc_string_log_decimal( Value );
                            if ( Value < 0 )
                            { Flags.MinLength--; } // take leading minus into account
                            if ( Flags.LeadingZeros ) {
                                while ( ( Flags.MinLength-- > 0 )  && ( Writer < BufferEnd ) )
                                { *Writer++ = '0'; }
                            }
                            else {
                                while ( ( Flags.MinLength-- > 0 )  && ( Writer < BufferEnd ) )
                                { *Writer++ = ' '; }
                            }
                        }

                        if ( C == 'u' ) // unsigned value
                        { Writer += ttc_itoa_unsigned( ( Base_t ) Value, Writer, BufferEnd - Writer ); }
                        else
                        { Writer += ttc_itoa( Value, Writer, BufferEnd - Writer ); }

                        Flags.InsideFormat = 0;
                        break;
                    }
                    case 'x': { // hexadecimal integer
                        Value = va_arg( Arguments, Base_t );

                        if ( Flags.MinLength ) { // prefix with spaces/ zeroes
                            Flags.MinLength -= ttc_string_log_hex( Value );
                            if ( Flags.LeadingZeros ) {
                                while ( ( Flags.MinLength-- > 0 )  && ( Writer < BufferEnd ) )
                                { *Writer++ = '0'; }
                            }
                            else {
                                while ( ( Flags.MinLength-- > 0 )  && ( Writer < BufferEnd ) )
                                { *Writer++ = ' '; }
                            }
                        }

                        Writer += ttc_xtoa( Value, Writer, BufferEnd - Writer );
                        Flags.InsideFormat = 0;
                        break;
                    }
                    case 's': { // string
                        Writer += ttc_string_copy( Writer, ( u8_t* ) va_arg( Arguments, char* ), BufferEnd - Writer );
                        Flags.InsideFormat = 0;
                        break;
                    }
                    default: {
                        if ( C ) {
                            *Writer++ = C;
                        }
                        Flags.InsideFormat = 0;
                        break;
                    }
                }
            }
            while ( ( C != 0 ) && Flags.InsideFormat && ( Writer < BufferEnd ) );
        }
    }

    if ( Writer < BufferEnd )
    { *Writer = 0; }
    else
    { *( Writer - 1 ) = 0; }

    return Writer;
}
void ttc_string_print_block( ttc_heap_block_t* Block ) {
    Assert_String( Block, ec_InvalidArgument );

    if ( _ttc_string_stdout_block ) {
        _ttc_string_stdout_block( Block );
    }
}
u8_t ttc_string_log_decimal( Base_signed_t Value ) {
    if ( Value < 0 )
    { Value = -Value; }

    if ( Value > 999999999 ) { return 10; }
    if ( Value >  99999999 ) { return 9; }
    if ( Value >   9999999 ) { return 8; }
    if ( Value >    999999 ) { return 7; }
    if ( Value >     99999 ) { return 6; }
    if ( Value >      9999 ) { return 5; }
    if ( Value >       999 ) { return 4; }
    if ( Value >        99 ) { return 3; }
    if ( Value >         9 ) { return 2; }
    return 1;
}
u8_t ttc_string_log_hex( Base_t Value ) {

    if ( Value > 0xfffffff ) { return 8; }
    if ( Value > 0x0ffffff ) { return 7; }
    if ( Value > 0x00fffff ) { return 6; }
    if ( Value > 0x000ffff ) { return 5; }
    if ( Value > 0x0000fff ) { return 4; }
    if ( Value > 0x00000ff ) { return 3; }
    if ( Value > 0x000000f ) { return 2; }
    return 1;
}
void ttc_string_register_stdout( void ( *StdoutBlock )( ttc_heap_block_t* ), void ( *StdoutString )( const u8_t*, Base_t ) ) {
    _ttc_string_stdout_block = StdoutBlock;
    _ttc_string_stdout       = StdoutString;
}
u8_t ttc_itoa( Base_signed_t Value, u8_t* Buffer, Base_t MaxSize ) {
    u8_t BytesWritten = 0;

    if ( Value == 0 ) { // Value == 0
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        *Buffer++ = '0';
        return 1;
    }
    if ( Value < 0 )  { // print minus
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        *Buffer++ = '-';
        Value *= -1;
        BytesWritten++;
    }

    #if TARGET_DATA_WIDTH==32
    Base_t Base = 1000000000;
    #endif
    #if TARGET_DATA_WIDTH==16
    Base_t Base = 10000;
    #endif
    #if TARGET_DATA_WIDTH==8
    Base_t Base = 100;
    #endif
    u8_t Digit;

    while ( ( Base > 0 ) && ( Base > Value ) ) // skip leading zeroes
    { Base /= 10; }

    while ( Base > 0 ) {
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        if ( Value >= Base ) {
            Digit = Value / Base;
            Value -= Digit * Base;
        }
        else
        { Digit = 0; }

        *Buffer++ = '0' + Digit;
        BytesWritten++;

        Base /= 10;
    }
    return BytesWritten;
}
u8_t ttc_itoa_unsigned( Base_t Value, u8_t* Buffer, Base_t MaxSize ) {
    u8_t BytesWritten = 0;

    if ( Value == 0 ) { // Value == 0
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        *Buffer++ = '0';
        return 1;
    }

    #if TARGET_DATA_WIDTH==32
    Base_t Base = 1000000000;
    #endif
    #if TARGET_DATA_WIDTH==16
    Base_t Base = 10000;
    #endif
    #if TARGET_DATA_WIDTH==8
    Base_t Base = 100;
    #endif
    u8_t Digit;

    while ( ( Base > 0 ) && ( Base > Value ) ) // skip leading zeroes
    { Base /= 10; }

    while ( Base > 0 ) {
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        if ( Value >= Base ) {
            Digit = Value / Base;
            Value -= Digit * Base;
        }
        else
        { Digit = 0; }

        *Buffer++ = '0' + Digit;
        BytesWritten++;

        Base /= 10;
    }
    return BytesWritten;
}
u8_t ttc_xtoa( Base_t Value, u8_t* Buffer, Base_t MaxSize ) {
    u8_t BytesWritten = 0;

    if ( Value == 0 ) { // Value == 0
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        *Buffer++ = '0';
        return 1;
    }

    #if TARGET_DATA_WIDTH==32
    Base_t Base = 0xf0000000;
    u8_t  Shift = 7 * 4;
    #endif
    #if TARGET_DATA_WIDTH==16
    Base_t Base = 0xf000;
    u8_t  Shift = 3 * 4;
    #endif
    #if TARGET_DATA_WIDTH==8
    Base_t Base = 0xf0;
    u8_t  Shift = 1 * 4;
    #endif
    u8_t Digit;

    while ( ( Base > 0 ) && ( ( Base & Value ) == 0 ) ) { // skip leading zeroes
        Base >>= 4;
        Shift -= 4;
    }

    while ( Base > 0 ) {
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        if ( Value & Base )
        { Digit = ( Value & Base ) >> Shift; }
        else
        { Digit = 0; }

        if ( Digit < 10 )
        { *Buffer++ = ( ( u8_t ) '0' ) + Digit; }
        else
        { *Buffer++ = ( ( u8_t ) 'a' ) + Digit - 10; }

        BytesWritten++;
        Base >>= 4;
        Shift -= 4;
    }
    return BytesWritten;
}
u8_t ttc_byte2uint( u8_t Value, u8_t* Buffer ) {

    if ( Value >= 100 ) {
        u8_t Digit = Value / 100;
        *Buffer++ = '0' + Digit;
        Value -= Digit * 100;

        Digit = Value / 10;
        *Buffer++ = '0' + Digit;
        Value -= Digit * 10;

        *Buffer++ = '0' + Value;
        return 3;
    }
    if ( Value >= 10 ) {
        u8_t Digit = Value / 10;
        *Buffer++ = '0' + Digit;
        Value -= Digit * 10;

        *Buffer++ = '0' + Value;
        return 2;
    }

    *Buffer++ = '0' + Value;
    return 1;
}
u8_t ttc_byte2int( s8_t Value, u8_t* Buffer ) {
    if ( Value < 0 ) { // negative value
        *Buffer++ = '-';
        return 1 + ttc_byte2uint( ( u8_t ) 0x7f & Value, Buffer );
    }
    return ttc_byte2uint( ( u8_t ) Value, Buffer );
}
void ttc_byte2hex( u8_t* Buffer, u8_t Byte ) {
    u8_t Nibble = Byte & 0xf;

    if ( Nibble < 10 )
    { *Buffer++ = '0' + Nibble; }
    else
    { *Buffer++ = 'a' - 10 + Nibble; }

    Nibble = Byte >> 4;
    if ( Nibble < 10 )
    { *Buffer = '0' + Nibble; }
    else
    { *Buffer = 'a' - 10 + Nibble; }
}
u8_t ttc_string_length8( const char* String, u8_t MaxLen ) {
    u8_t Length = 0;

    while ( ( MaxLen > 0 ) && ( *String != 0 ) ) {
        MaxLen--;
        String++;
        Length++;
    }
    return Length;
}
u16_t ttc_string_length16( const char* String, u16_t MaxLen ) {
    u16_t Length = 0;

    while ( ( MaxLen > 0 ) && ( *String != 0 ) ) {
        MaxLen--;
        String++;
        Length++;
    }
    return Length;
}
u32_t ttc_string_length32( const char* String, u32_t MaxLen ) {
    u32_t Length = 0;

    while ( ( MaxLen > 0 ) && ( *String != 0 ) ) {
        MaxLen--;
        String++;
        Length++;
    }
    return Length;
}

u8_t* _ttc_string_get_buffer() {
    if ( ttc_string_Buffer == NULL ) { // first call: allocate buffer
        ttc_string_Buffer = ttc_heap_alloc( TTC_STRING_PRINTF_BUFFERSIZE );
        Assert_String( ttc_string_Buffer, ec_Malloc );

        // reserve 1 byte for string terminator
        ttc_string_BufferEnd = ttc_string_Buffer + TTC_STRING_PRINTF_BUFFERSIZE - 2;
    }

    return ttc_string_Buffer;
}
void _ttc_string_lock() {
    #ifdef TTC_MULTITASKING_SCHEDULER
    if ( ttc_string_Lock == NULL ) { // first call: allocate mutex
        ttc_string_Lock = ttc_mutex_create();
    }
    while ( ttc_mutex_lock( ttc_string_Lock, -1 ) != tme_OK );
    #endif
}

//}functions
