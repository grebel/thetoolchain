/*{ ttc_font_types.h ************************************************

   I'M A TEMPLATE PLEASE CHANGE ME!

   Structures, Enums and Defines being required by both, high- and low-level driver.

}*/

#ifndef TTC_FONT_TYPES_H
#define TTC_FONT_TYPES_H

#include "ttc_basic_types.h"

//{ Static Configuration **************************************

#ifndef TTC_ASSERT_FONT    // any previous definition set (Makefile)?
    #define TTC_ASSERT_FONT 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_FONT == 1)  // use Assert()s in FONT code (somewhat slower but alot easier to debug)
    #define Assert_FONT(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in FONT code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_FONT(Condition, ErrorCode)
#endif


#ifndef TTC_FONT_SINGLE_THREADED // == 1: functions can use static variables to speed up function calls + reduce stack usage
    #define TTC_FONT_SINGLE_THREADED 1
#endif
#if (TTC_FONT_SINGLE_THREADED == 1)
    #define FONT_STATIC static
#else
    #define FONT_STATIC
#endif


// TTC_FONTn has to be defined as constant by makefile.100_board_*
#ifdef TTC_FONT5
    #ifndef TTC_FONT4
        #error TTC_FONT5 is defined, but not TTC_FONT4 - all lower TTC_FONTn must be defined!
    #endif
    #ifndef TTC_FONT3
        #error TTC_FONT5 is defined, but not TTC_FONT3 - all lower TTC_FONTn must be defined!
    #endif
    #ifndef TTC_FONT2
        #error TTC_FONT5 is defined, but not TTC_FONT2 - all lower TTC_FONTn must be defined!
    #endif
    #ifndef TTC_FONT1
        #error TTC_FONT5 is defined, but not TTC_FONT1 - all lower TTC_FONTn must be defined!
    #endif

    #define TTC_FONTS_AMOUNT 5
#else
    #ifdef TTC_FONT4
        #define TTC_FONTS_AMOUNT 4

        #ifndef TTC_FONT3
            #error TTC_FONT5 is defined, but not TTC_FONT3 - all lower TTC_FONTn must be defined!
        #endif
        #ifndef TTC_FONT2
            #error TTC_FONT5 is defined, but not TTC_FONT2 - all lower TTC_FONTn must be defined!
        #endif
        #ifndef TTC_FONT1
            #error TTC_FONT5 is defined, but not TTC_FONT1 - all lower TTC_FONTn must be defined!
        #endif
    #else
        #ifdef TTC_FONT3

            #ifndef TTC_FONT2
                #error TTC_FONT5 is defined, but not TTC_FONT2 - all lower TTC_FONTn must be defined!
            #endif
            #ifndef TTC_FONT1
                #error TTC_FONT5 is defined, but not TTC_FONT1 - all lower TTC_FONTn must be defined!
            #endif

            #define TTC_FONTS_AMOUNT 3
        #else
            #ifdef TTC_FONT2

                #ifndef TTC_FONT1
                    #error TTC_FONT5 is defined, but not TTC_FONT1 - all lower TTC_FONTn must be defined!
                #endif

                #define TTC_FONTS_AMOUNT 2
            #else
                #ifdef TTC_FONT1
                    #define TTC_FONTS_AMOUNT 1
                #else
                    #define TTC_FONT_USING_DEFAULT
                    #define TTC_FONT1 font_type1_16x24
                    #define TTC_FONTS_AMOUNT 1
                #endif
            #endif
        #endif
    #endif
#endif

//}Static Configuration
//{ inline functions ******************************************

//}inline functions
//{ Structures ************************************************

typedef struct s_ttc_font_data { // description of a monospaced, monocolored font
    const char* Name;   // name of this font (without size)
    t_u8 CharWidth;     // width of each character (bits)
    t_u8 CharHeight;    // height of each character (bits)
    t_u8 FirstASCII;    // ASCII-code of first character in Characters[]
    t_u8 Amount;        // amount of characters stored in Characters[]

    const t_u16* Characters;  // data of bitmap font; each character consists of CharWidth * CharHeight bits
} __attribute__( ( __packed__ ) ) t_ttc_font_data;

typedef struct s_ttc_font_generic { // architecture independent configuration data

    // Note: Write-access to this structure is only allowed before first ttc_font_init() call!
    //       After initialization, only read accesses are allowed for high-level code.

    t_u8 LogicalIndex;       // automatically set: logical index of font to use (1 = TTC_FONT1, ...)
    t_ttc_font_data* Fonts;       // points to list of configured fonts

    union  {
        t_u32 All;
        struct {

            // ToDo: additional high-level flags go here..

            unsigned Reserved1        : 32; // pad to 32 bits
        } Bits;
    } Flags;

    // ToDo: additional high-level attributes go here
#if TTC_FONT1 == font_type1_16x24

#endif

} __attribute__( ( __packed__ ) ) t_ttc_font_generic;

//}Structures

#endif // TTC_FONT_TYPES_H
