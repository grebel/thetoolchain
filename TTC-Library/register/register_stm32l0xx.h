#ifndef REGISTER_STM32L0XX_H
#define REGISTER_STM32L0XX_H

/** { register_stm32l0xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for register devices on stm32l0xx architectures.
 *  Structures, Enums and Defines being required by high-level register and application.
 *
 *  Created from template device_architecture.h revision 22 at 20150319 12:51:52 UTC
 *
 *  Note: See ttc_register.h for description of stm32l0xx independent REGISTER implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_REGISTER_STM32L0XX
//
// Implementation status of low-level driver support for register devices on stm32l0xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_REGISTER_STM32L0XX '?'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_REGISTER_STM32L0XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_REGISTER_STM32L0XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_REGISTER_STM32L0XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "register_stm32l0xx.c"
//
#include "stm32l0xx.h"
#include "register_stm32l0xx_types.h"
#include "../ttc_register_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_register_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_register_foo

#define ttc_driver_register_get_configuration() register_stm32l0xx_get_configuration()
#define ttc_driver_register_prepare() register_stm32l0xx_prepare()
#define ttc_driver_register_reset(Register) register_stm32l0xx_reset(Register)

//
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)
extern volatile t_register_stm32l0xx_gpio          register_stm32l0xx_GPIOA;
extern volatile t_register_stm32l0xx_gpio          register_stm32l0xx_GPIOB;
extern volatile t_register_stm32l0xx_gpio          register_stm32l0xx_GPIOC;
extern volatile t_register_stm32l0xx_gpio          register_stm32l0xx_GPIOD;
extern volatile t_register_stm32l0xx_gpio          register_stm32l0xx_GPIOH;
//} Includes
// Register Variable Declarations ***************************************{

//}


/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_register.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_register.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** fills out given Config with maximum valid values for indexed REGISTER
 * @param Config        = pointer to struct t_ttc_register_config (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
t_ttc_register_architecture* register_stm32l0xx_get_configuration();


/** Prepares register driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void register_stm32l0xx_prepare();


/** resets given register to default value
 *
 * @param Register    pointer to one entry of struct t_ttc_register_config as returned by ttc_register_get_configuration()
 * @return            ==0: register has been reset successfully
 */
e_ttc_register_errorcode register_stm32l0xx_reset( void* Register );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _register_stm32l0xx_foo(t_ttc_register_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //REGISTER_STM32L0XX_H