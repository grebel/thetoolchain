#ifndef REGISTER_STM32W1XX_TYPES_H
#define REGISTER_STM32W1XX_TYPES_H

/** { register_stm32w1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for REGISTER devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by ttc_register_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140223 10:54:39 UTC
 *
 *  Note: See ttc_register.h for description of architecture independent REGISTER implementation.
 *
 *  Authors: Greg Knoll, Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifdef EXTENSION_cpu_stm32w1xx
    #ifdef EXTENSION_253_stm32w1xx_hal201 //{ using Standard Peripheral Library
        #include "stm32w108xx.h"
        //}
    #else                                 //{ no Standard Peripheral Library: Define addresses on our own
        #define FLASH_BASE                  ((t_u32)0x08000000)       /*!< FLASH base address in the alias region */
        #define SRAM_BASE                   ((t_u32)0x20000000)       /*!< SRAM base address in the alias region */
        #define PERIPH_BASE                 ((t_u32)0x40000000)       /*!< Peripheral base address in the alias region */

        #define SRAM_BB_BASE                ((t_u32)0x22000000)       /*!< SRAM base address in the bit-band region */
        #define PERIPH_BB_BASE              ((t_u32)0x42000000)       /*!< Peripheral base address in the bit-band region */

        /*!< Peripheral memory map */
        #define PWR_BASE                    (PERIPH_BASE + 0x0004)
        #define CLK_BASE                    (PERIPH_BASE + 0x0008)
        #define RST_BASE                    (PERIPH_BASE + 0x002C)
        #define MACTMR_BASE                 (PERIPH_BASE + 0x2038)
        #define GPIO_DBG_BASE               (PERIPH_BASE + 0x4028)
        #define FLASH_R_BASE                (PERIPH_BASE + 0x402C)
        #define MEM_R_BASE                  (PERIPH_BASE + 0x5000)
        #define WDG_BASE                    (PERIPH_BASE + 0x6000)
        #define SLPTMR_BASE                 (PERIPH_BASE + 0x600C)
        #define ADC_BASE                    (PERIPH_BASE + 0xA810)
        #define EXTI_BASE                   (PERIPH_BASE + 0xA814)
        #define GPIOA_BASE                  (PERIPH_BASE + 0xB000)
        #define GPIOB_BASE                  (PERIPH_BASE + 0xB400)
        #define GPIOC_BASE                  (PERIPH_BASE + 0xB800)
        #define SC1_UART_BASE               (PERIPH_BASE + 0xC83C)
        #define SC1_SPI_BASE                (PERIPH_BASE + 0xC83C)
        #define SC1_I2C_BASE                (PERIPH_BASE + 0xC83C)
        #define SC2_UART_BASE               (PERIPH_BASE + 0xC03C)
        #define SC2_SPI_BASE                (PERIPH_BASE + 0xC03C)
        #define SC2_I2C_BASE                (PERIPH_BASE + 0xC03C)
        #define TIM1_BASE                   (PERIPH_BASE + 0xE000)
        #define TIM2_BASE                   (PERIPH_BASE + 0xF000)
        #define OB_BASE                     ((t_u32)0x08040800)       /*!< Flash Option Bytes base address */
        #define MGMT_IT_BASE                (PERIPH_BASE + 0xA018)
        #define TIM1_IT_BASE                (PERIPH_BASE + 0xA800)
        #define TIM2_IT_BASE                (PERIPH_BASE + 0xA804)
        #define SC1_IT_BASE                 (PERIPH_BASE + 0xA808)
        #define SC2_IT_BASE                 (PERIPH_BASE + 0xA80C)
        #define SC2_DMA_ChannelRx_BASE      (PERIPH_BASE + 0xC000)
        #define SC2_DMA_ChannelTx_BASE      (PERIPH_BASE + 0xC010)
        #define SC2_DMA_BASE                (PERIPH_BASE + 0xC020)
        #define SC1_DMA_ChannelRx_BASE      (PERIPH_BASE + 0xC800)
        #define SC1_DMA_ChannelTx_BASE      (PERIPH_BASE + 0xC810)
        #define SC1_DMA_BASE                (PERIPH_BASE + 0xC820)
        #define PWR                         (PWR_BASE)
        #define CLK                         (CLK_BASE)
        #define RST                         (RST_BASE)
        #define MACTMR                      (MACTMR_BASE)
        #define GPIO_DBG                    (GPIO_DBG_BASE)
        #define FLASH                       (FLASH_R_BASE)
        #define MEM                         (MEM_R_BASE)
        #define WDG                         (WDG_BASE)
        #define SLPTMR                      (SLPTMR_BASE)
        #define EXTI                        (EXTI_BASE)
        #define GPIOA                       (GPIOA_BASE)
        #define GPIOB                       (GPIOB_BASE)
        #define GPIOC                       (GPIOC_BASE)
        #define SC1_UART                    (SC1_UART_BASE)
        #define SC1_SPI                     (SC1_SPI_BASE)
        #define SC1_I2C                     (SC1_I2C_BASE)
        #define SC2_UART                    (SC2_UART_BASE)
        #define SC2_SPI                     (SC2_SPI_BASE)
        #define SC2_I2C                     (SC2_I2C_BASE)
        #define ADC                         (ADC_BASE)
        #define TIM1                        (TIM1_BASE)
        #define TIM2                        (TIM2_BASE)
        #define OB                          (OB_BASE)
        #define MGMT_IT                     (MGMT_IT_BASE)
        #define TIM1_IT                     (TIM1_IT_BASE)
        #define TIM2_IT                     (TIM2_IT_BASE)
        #define SC1_IT                      (SC1_IT_BASE)
        #define SC2_IT                      (SC2_IT_BASE)
        #define SC2_DMA_ChannelRx           (SC2_DMA_ChannelRx_BASE)
        #define SC2_DMA_ChannelTx           (SC2_DMA_ChannelTx_BASE)
        #define SC2_DMA                     (SC2_DMA_BASE)
        #define SC1_DMA_ChannelRx           (SC1_DMA_ChannelRx_BASE)
        #define SC1_DMA_ChannelTx           (SC1_DMA_ChannelTx_BASE)
        #define SC1_DMA                     (SC1_DMA_BASE)
    #endif
#endif //}

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_register_types.h *************************

//---System Clock--//


typedef struct { //OSC24M_CTRL - high speed 24MHz Oscillator (stm32w_constants.h)
    unsigned bSelect   : 1; // =1: select 24MHz oscillator
    unsigned bEnable   : 1; // =1: enable 24MHz oscillator
    unsigned reserved1 : 14;
    unsigned reserved2 : 16;
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_osc24m_ctrl;

// ToDo!: derive structs from all registers in stm32w_constants.h

//---Configuration mode types for GPIO---//

typedef enum { //gpio_port_ioMode_e - possible 4-bit values for GPIO pin mode set in GPIO_PxCFGH/L
    gpmode_analog = 0x0,
    gpmode_input_floating = 0x4,
    gpmode_input_pull_upORdown = 0x8,
    gpmode_output_push_pull = 0x1,
    gpmode_output_open_drain = 0x5,
    gpmode_output_alternate_push_pull = 0x9,
    gpmode_output_alternate_open_drain = 0xD,
    gpmode_output_alternate_pp_SPI_SCLK = 0xB
} e_register_stm32w1xx_gpio_port_iomode;

//----------IRQ modes----------//
typedef enum {
    stm32w_irq_mode_disabled                  = 0,
    stm32w_irq_mode_rising_edge               = 1,
    stm32w_irq_mode_falling_edge              = 2,
    stm32w_irq_mode_rising_and_falling_edge   = 3,
    stm32w_irq_mode_active_high               = 4,
    stm32w_irq_mode_active_low                = 5
} e_register_stm32w1xx_gpio_irqx_mode;

//-----Serial Controller Modes-----//
typedef enum {
    sc_mode_disabled,   //0
    sc_mode_UART,       //1
    sc_mode_SPI,        //2
    sc_mode_I2C         //3

} e_register_stm32w1xx_sc_mode;

//{The basic GPIO registers for each port are kept in blocks at
//Port A -> 0xB000
//Port B -> 0xB400
//Port C -> 0xB800
//The datasheet has set and clear switched
typedef struct { //GPIO_PxCFGL - config of each pin's mode [starting with LSB]  (p.67)
    unsigned bMODE0     : 4; //gpio_port_ioMode_e for GPIOx pin 0
    unsigned bMODE1     : 4; //gpio_port_ioMode_e for GPIOx pin 1
    unsigned bMODE2     : 4; //gpio_port_ioMode_e for GPIOx pin 2
    unsigned bMODE3     : 4; //gpio_port_ioMode_e for GPIOx pin 3
    unsigned breserved  : 16; //- - - - - - - - - - - - - - - - -
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_pxcfgl;

typedef struct { //GPIO_PxCFGH                          (p.67)
    unsigned bMODE4     : 4; //gpio_port_ioMode_e for GPIOx pin 4
    unsigned bMODE5     : 4; //gpio_port_ioMode_e for GPIOx pin 5
    unsigned bMODE6     : 4; //gpio_port_ioMode_e for GPIOx pin 6
    unsigned bMODE7     : 4; //gpio_port_ioMode_e for GPIOx pin 7
    unsigned breserved  : 16; //- - - - - - - - - - - - - - - - -
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_pxcfgh;

typedef struct { //GPIO_PxIN - input data register                      (p.68)
    unsigned bDATA  : 8; //input data register of all 8 port bits
    unsigned breserved  : 24; // ------------------------------------

} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_pxin;

typedef struct { //GPIO_PxOUT - output data register                (p.69)
    unsigned bDATA  : 8; //output data register of all 8 port bits
    unsigned breserved  : 24; // ------------------------------------

} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_pxout;

typedef struct { //GPIO_PxSET - output set register             (p.70)
    unsigned bSET0  : 1; //1 to set, 0 no effect
    unsigned bSET1  : 1; //1 to set, 0 no effect
    unsigned bSET2  : 1; //1 to set, 0 no effect
    unsigned bSET3  : 1; //1 to set, 0 no effect
    unsigned bSET4  : 1; //1 to set, 0 no effect
    unsigned bSET5  : 1; //1 to set, 0 no effect
    unsigned bSET6  : 1; //1 to set, 0 no effect
    unsigned bSET7  : 1; //1 to set, 0 no effect
    unsigned breserved  : 24; //----------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_pxset;

typedef struct { //GPIO_PxCLR - output clear register               (p.69)
    unsigned bCLR0  : 1; //1 to clear, 0 no effect
    unsigned bCLR1  : 1; //1 to clear, 0 no effect
    unsigned bCLR2  : 1; //1 to clear, 0 no effect
    unsigned bCLR3  : 1; //1 to clear, 0 no effect
    unsigned bCLR4  : 1; //1 to clear, 0 no effect
    unsigned bCLR5  : 1; //1 to clear, 0 no effect
    unsigned bCLR6  : 1; //1 to clear, 0 no effect
    unsigned bCLR7  : 1; //1 to clear, 0 no effect
    unsigned breserved  : 24; //----------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_pxclr;

typedef struct {//GPIOx - General Purpose I/O basic registers
    t_register_stm32w1xx_gpio_pxcfgl    CFGL;    //Port configuration register low
    t_register_stm32w1xx_gpio_pxcfgh    CFGH;    //Port configuration register high
    t_register_stm32w1xx_gpio_pxin      IN;      //GPIO input data register
    t_register_stm32w1xx_gpio_pxout     OUT;     //GPIO output data register
    t_register_stm32w1xx_gpio_pxset     SET;     //GPIO set register
    t_register_stm32w1xx_gpio_pxclr     CLR;     //GPIO clear register
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpiox;
//} END GPIO basic registers


//{ GPIO registers for DEBUG, WAKE, and IRQ (for external interrupts)

//-----first block for debug, wake, and some of irq-----//
typedef struct { //GPIO_DBGCFG - GPIO debug config register                        (p.73)
    unsigned breserved1       : 4; //-----------------------
    unsigned bEXTREGEN        : 1; //Disable REG_EN override of PA7's normal GPIO config
    //0: Enable override. 1: Disable override.
    unsigned bDEBUGDIS        : 1; //Disable debug interface override of normal GPIO config
    //0:Permit debug interface to be active. 1: Disable debug interface (if it's not already active)
    unsigned breserved        : 26; //----------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_dbgcfg;

typedef struct { //GPIO_DBGSTAT - GPIO debug status register                       (p.74)
    unsigned bSWEN       : 1; //Status of Serial Wire interface
    //0: Not enabled by SWJ-DP. 1: Enabled by SWJ-DP.
    unsigned bFORCEDBG   : 1; //Status of debugger interface
    //0: Debugger interface not forced active. 1: Debugger interface forced active by debugger cable.
    unsigned breserved   : 1; //------------------------------
    unsigned bBOOTMODE   : 1; //The state of the nBOOTMODE signal sampled at the end of reset
    //0: nBOOTMODE was not asserted (HIGH). 1: asserted (LOW)
    unsigned breserved2        : 28; //----------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_dbgstat;

typedef struct { //GPIO_PxWAKE - enables wakeup monitoring for the pins of port x  (p.70)
    unsigned bWAKE0 : 1;    //1 to enable wakeup monitoring of PA0
    unsigned bWAKE1 : 1;    //1 to enable wakeup monitoring of PA1
    unsigned bWAKE2 : 1;    //1 to enable wakeup monitoring of PA2
    unsigned bWAKE3 : 1;    //1 to enable wakeup monitoring of PA3
    unsigned bWAKE4 : 1;    //1 to enable wakeup monitoring of PA4
    unsigned bWAKE5 : 1;    //1 to enable wakeup monitoring of PA5
    unsigned bWAKE6 : 1;    //1 to enable wakeup monitoring of PA6
    unsigned bWAKE7 : 1;    //1 to enable wakeup monitoring of PA7
    unsigned breserved : 24; //------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_pxwake;

typedef volatile struct __attribute__( ( __packed__ ) ) { //GPIO_IRQCSEL - interrupt C select register                    (p.71)
    unsigned bSEL         : 5; //pin assigned to IRQC (gpio_IRQx_pin_e)
    unsigned breserved    : 27; //--------------------
} t_register_stm32w1xx_gpio_irqcsel;

typedef struct { //GPIO_IRQDSEL - interrupt D select register                    (p.71)
    unsigned bSEL         : 5; //pin assigned to IRQD (gpio_IRQx_pin_e)
    unsigned breserved    : 27; //--------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_irqdsel;

typedef struct { //GPIO_WAKEFILT - wakeup filtering register                    (p.71)
    unsigned bWFILTER_GPIO  : 1; //enable filter on wakeup sources designated in GPIO_PnWake
    unsigned bWFILTER_SC1   : 1; //enable filter on wakeup source SC1(PB2)
    unsigned bWFILTER_SC2   : 1; //enable filter on wakeup source SC2(PA2)
    unsigned bWFILTER_IRQD  : 1; //enable filter on wakeup source IRQD
    unsigned breserved      : 28; //-------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_wakefilt;

typedef volatile struct {//registers_GPIO_debug_wake_irq
    volatile t_register_stm32w1xx_gpio_dbgcfg    DBGCFG;   //GPIO Debug configuration register
    volatile t_register_stm32w1xx_gpio_dbgstat   DBGSTAT;  //GPIO Debug status register
    volatile t_register_stm32w1xx_gpio_pxwake    AWAKE;    //Wake monitoring enable register
    volatile t_register_stm32w1xx_gpio_pxwake    BWAKE;    //Wake monitoring enable register
    volatile t_register_stm32w1xx_gpio_pxwake    CWAKE;    //Wake monitoring enable register
    volatile t_register_stm32w1xx_gpio_irqcsel   IRQCSEL;  //IRQ pin selection register
    volatile t_register_stm32w1xx_gpio_irqdsel   IRQDSEL;  //IRQ pin selection register
    volatile t_register_stm32w1xx_gpio_wakefilt  WAKEFILT; //Enable filter on wakeup register
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_debug_wake_irq;
//-------------------END first block------------------//

//-----second block for config registers for interrupts A-D----//
typedef struct { //GPIO_INTCFGx - interrupt x config register                    (p.72)
    unsigned breserved1    : 5; //-------------------------------------------
    unsigned bINTMOD  : 3; //IRQx triggering mode (gpio_IRQx_mode_e)
    unsigned bINTFILT : 1; //set bit to enable digital filtering on IRQx (increases minimum event width 80ns -> 450ns)
    unsigned breserved2   : 23; //-------------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_intcfgx;

typedef struct { //GPIO_interruptCFG - interrupt x config register                    (p.72)
    t_register_stm32w1xx_gpio_intcfgx INTCFGA;
    t_register_stm32w1xx_gpio_intcfgx INTCFGB;
    t_register_stm32w1xx_gpio_intcfgx INTCFGC;
    t_register_stm32w1xx_gpio_intcfgx INTCFGD;
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_interruptcfg;
//------------------END second block----------------------//

//-------third block for GPIO interrupt flags----------------//
typedef struct { //GPIO_interruptFLAG - GPIO interrupt flag register                   (p.73)
    unsigned bIRQAFLAG    : 1; //IRQA interrupt pending
    unsigned bIRQBFLAG    : 1; //IRQB interrupt pending
    unsigned bIRQCFLAG    : 1; //IRQC interrupt pending
    unsigned bIRQDFLAG    : 1; //IRQD interrupt pending
    unsigned breserved        : 28; //----------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_gpio_interruptflag;
//--------------------END third block-----------------------//

//} END GPIO registers for DEBUG, WAKE, and IRQ

//}
//-------------------------------------END GPIO REGISTERS-----------------------------------------//


//------------------------------------INTERRUPT REGISTERS-----------------------------------------//
//{

//-----------NVIC----------------//

typedef struct { //INT_CFGSET - top-level set interrupts config register  [ENABLE ints]       (p.184)
    unsigned bTIM1       : 1;    //1: enable timer 1      interrupt (0: no effect)
    unsigned bTIM2       : 1;    //1: enable timer 2      interrupt (0: no effect)
    unsigned bMGMT       : 1;    //1: enable management   interrupt (0: no effect)
    unsigned bBB         : 1;    //1: enable baseband     interrupt (0: no effect)
    unsigned bSLEEPTMR   : 1;    //1: enable sleep timer  interrupt (0: no effect)
    unsigned bSC1        : 1;    //1: enable serial 1     interrupt (0: no effect)
    unsigned bSC2        : 1;    //1: enable serial 2     interrupt (0: no effect)
    unsigned bSEC        : 1;    //1: enable security     interrupt (0: no effect)
    unsigned bMACTMR     : 1;    //1: enable MAC timer    interrupt (0: no effect)
    unsigned bMACTX      : 1;    //1: enable MAC transmit interrupt (0: no effect)
    unsigned bMACRX      : 1;    //1: enable MAC receive  interrupt (0: no effect)
    unsigned bADC        : 1;    //1: enable ADC          interrupt (0: no effect)
    unsigned bIRQA       : 1;    //1: enable IRQA         interrupt (0: no effect)
    unsigned bIRQB       : 1;    //1: enable IRQB         interrupt (0: no effect)
    unsigned bIRQC       : 1;    //1: enable IRQC         interrupt (0: no effect)
    unsigned bIRQD       : 1;    //1: enable IRQD         interrupt (0: no effect)
    unsigned bDEBUG      : 1;    //1: enable debug        interrupt (0: no effect)
    unsigned breserved       : 15;   //-----------------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_int_cfgset;

typedef struct { //INT_CFGCLR - top-level clear interrupts config register [DISABLE ints]     (p.185)
    unsigned bTIM1       : 1;    //1: disable timer 1      interrupt (0: no effect)
    unsigned bTIM2       : 1;    //1: disable timer 2      interrupt (0: no effect)
    unsigned bMGMT       : 1;    //1: disable management   interrupt (0: no effect)
    unsigned bBB         : 1;    //1: disable baseband     interrupt (0: no effect)
    unsigned bSLEEPTMR   : 1;    //1: disable sleep timer  interrupt (0: no effect)
    unsigned bSC1        : 1;    //1: disable serial 1     interrupt (0: no effect)
    unsigned bSC2        : 1;    //1: disable serial 2     interrupt (0: no effect)
    unsigned bSEC        : 1;    //1: disable security     interrupt (0: no effect)
    unsigned bMACTMR     : 1;    //1: disable MAC timer    interrupt (0: no effect)
    unsigned bMACTX      : 1;    //1: disable MAC transmit interrupt (0: no effect)
    unsigned bMACRX      : 1;    //1: disable MAC receive  interrupt (0: no effect)
    unsigned bADC        : 1;    //1: disable ADC          interrupt (0: no effect)
    unsigned bIRQA       : 1;    //1: disable IRQA         interrupt (0: no effect)
    unsigned bIRQB       : 1;    //1: disable IRQB         interrupt (0: no effect)
    unsigned bIRQC       : 1;    //1: disable IRQC         interrupt (0: no effect)
    unsigned bIRQD       : 1;    //1: disable IRQD         interrupt (0: no effect)
    unsigned bDEBUG      : 1;    //1: disable debug        interrupt (0: no effect)
    unsigned breserved       : 15;   //-----------------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_int_cfgclr;

typedef struct { //INT_PENDSET - top-level set interrupts pending register [TRIGGER ints]     (p.186)
    unsigned bTIM1       : 1;    //1: pend timer 1      interrupt (0: no effect)
    unsigned bTIM2       : 1;    //1: pend timer 2      interrupt (0: no effect)
    unsigned bMGMT       : 1;    //1: pend management   interrupt (0: no effect)
    unsigned bBB         : 1;    //1: pend baseband     interrupt (0: no effect)
    unsigned bSLEEPTMR   : 1;    //1: pend sleep timer  interrupt (0: no effect)
    unsigned bSC1        : 1;    //1: pend serial 1     interrupt (0: no effect)
    unsigned bSC2        : 1;    //1: pend serial 2     interrupt (0: no effect)
    unsigned bSEC        : 1;    //1: pend security     interrupt (0: no effect)
    unsigned bMACTMR     : 1;    //1: pend MAC timer    interrupt (0: no effect)
    unsigned bMACTX      : 1;    //1: pend MAC transmit interrupt (0: no effect)
    unsigned bMACRX      : 1;    //1: pend MAC receive  interrupt (0: no effect)
    unsigned bADC        : 1;    //1: pend ADC          interrupt (0: no effect)
    unsigned bIRQA       : 1;    //1: pend IRQA         interrupt (0: no effect)
    unsigned bIRQB       : 1;    //1: pend IRQB         interrupt (0: no effect)
    unsigned bIRQC       : 1;    //1: pend IRQC         interrupt (0: no effect)
    unsigned bIRQD       : 1;    //1: pend IRQD         interrupt (0: no effect)
    unsigned bDEBUG      : 1;    //1: pend debug        interrupt (0: no effect)
    unsigned breserved       : 15;   //-----------------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_int_pendset;

typedef struct { //INT_PENDCLR - top-level clear interrupts pending register [CLEAR ints]     (p.187)
    unsigned bTIM1       : 1;    //1: unpend timer 1      interrupt (0: no effect)
    unsigned bTIM2       : 1;    //1: unpend timer 2      interrupt (0: no effect)
    unsigned bMGMT       : 1;    //1: unpend management   interrupt (0: no effect)
    unsigned bBB         : 1;    //1: unpend baseband     interrupt (0: no effect)
    unsigned bSLEEPTMR   : 1;    //1: unpend sleep timer  interrupt (0: no effect)
    unsigned bSC1        : 1;    //1: unpend serial 1     interrupt (0: no effect)
    unsigned bSC2        : 1;    //1: unpend serial 2     interrupt (0: no effect)
    unsigned bSEC        : 1;    //1: unpend security     interrupt (0: no effect)
    unsigned bMACTMR     : 1;    //1: unpend MAC timer    interrupt (0: no effect)
    unsigned bMACTX      : 1;    //1: unpend MAC transmit interrupt (0: no effect)
    unsigned bMACRX      : 1;    //1: unpend MAC receive  interrupt (0: no effect)
    unsigned bADC        : 1;    //1: unpend ADC          interrupt (0: no effect)
    unsigned bIRQA       : 1;    //1: unpend IRQA         interrupt (0: no effect)
    unsigned bIRQB       : 1;    //1: unpend IRQB         interrupt (0: no effect)
    unsigned bIRQC       : 1;    //1: unpend IRQC         interrupt (0: no effect)
    unsigned bIRQD       : 1;    //1: unpend IRQD         interrupt (0: no effect)
    unsigned bDEBUG      : 1;    //1: unpend debug        interrupt (0: no effect)
    unsigned breserved       : 15;   //-----------------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_int_pendclr;

typedef struct { //INT_ACTIVE - top-level active interrupts register                        (p.188)
    unsigned bTIM1       : 1;    // timer 1      interrupt active
    unsigned bTIM2       : 1;    //timer 2      interrupt active
    unsigned bMGMT       : 1;    //management   interrupt active
    unsigned bBB         : 1;    //baseband     interrupt active
    unsigned bSLEEPTMR   : 1;    //sleep timer  interrupt active
    unsigned bSC1        : 1;    //serial 1     interrupt active
    unsigned bSC2        : 1;    //serial 2     interrupt active
    unsigned bSEC        : 1;    //security     interrupt active
    unsigned bMACTMR     : 1;    //MAC timer    interrupt active
    unsigned bMACTX      : 1;    //MAC transmit interrupt active
    unsigned bMACRX      : 1;    //MAC receive  interrupt active
    unsigned bADC        : 1;    //ADC          interrupt active
    unsigned bIRQA       : 1;    //IRQA         interrupt active
    unsigned bIRQB       : 1;    //IRQB         interrupt active
    unsigned bIRQC       : 1;    //IRQC         interrupt active
    unsigned bIRQD       : 1;    //IRQD         interrupt active
    unsigned bDEBUG      : 1;    //debug        interrupt active
    unsigned breserved       : 15;   //----------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_int_active;

typedef struct { //INT_MISS - top-level missed interrupts register                          (p.189)
    unsigned breserved           : 2;    //-------------------------------
    unsigned bMISSMGMT       : 1;    // management   interrupt  missed
    unsigned bMISSBB         : 1;    // baseband     interrupt  missed
    unsigned bMISSSLEEPTMR   : 1;    // sleep timer  interrupt  missed
    unsigned bMISSSC1        : 1;    // serial 1     interrupt  missed
    unsigned bMISSSC2        : 1;    // serial 2     interrupt  missed
    unsigned bMISSSEC        : 1;    // security     interrupt  missed
    unsigned bMISSMACTMR     : 1;    // MAC timer    interrupt  missed
    unsigned bMISSMACTX      : 1;    // MAC transmit interrupt  missed
    unsigned bMISSMACRX      : 1;    // MAC receive  interrupt  missed
    unsigned bMISSADC        : 1;    // ADC          interrupt  missed
    unsigned bMISSIRQA       : 1;    // IRQA         interrupt  missed
    unsigned bMISSIRQB       : 1;    // IRQB         interrupt  missed
    unsigned bMISSIRQC       : 1;    // IRQC         interrupt  missed
    unsigned bMISSIRQD       : 1;    // IRQD         interrupt  missed
    unsigned breserved2          : 16;   //-------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_int_miss;

typedef struct { //SCS_AFSR - Auxiliary fault status register                               (p.190)
    unsigned bMISSED         : 1; //A bus fault occurred when a bit was already set in this register.
    unsigned bRESERVED       : 1; //Read in reserved memory block
    unsigned bPROTECTED      : 1; //Bus fault from unpriveleged write
    unsigned bWRONGSIZE      : 1; //Bus fault from 8 or 16-bit r/w from APB
    unsigned bresereved      : 28; //---------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scs_afsr;

typedef struct { //NVIC_interrupt_register - top-level interrupt registers ToDo: check if OK
    t_register_stm32w1xx_int_cfgset      CFGSET;     //register to enable interrupts
    t_register_stm32w1xx_int_cfgclr      CFGCLR;     //register to disable interrupts
    t_register_stm32w1xx_int_pendset     PENDSET;    //register to trigger interrupts
    t_register_stm32w1xx_int_pendclr     PENDCLR;    //register to clear interrupts
    t_register_stm32w1xx_int_active      ACTIVE;     //indictates which interrupts are currently active
    t_register_stm32w1xx_int_miss        MISS;       //flag register for interrupts not processed by NVIC (set by event manager)
    t_register_stm32w1xx_scs_afsr        AFSR;       //register for recording bus faults

} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_nvic_interrupt_registers;

//----------END NVIC------------//

//EVENT MANAGER REGISTERS grouped with peripherals (i.e.: INT_GPIOCFGx and INT_GPIOFLAG)

//}
//-------------------------------------END INTERRUPT REGISTERS------------------------------------//


//------------------------------------SERIAL REGISTERS-----------------------------------------//
//{

//--------Register Banks for SC1 and SC2--------------//
typedef struct { //SCx_RXBEGA - Receive DMA begin address register A (p.107)
    unsigned bSCx_RXBEGA : 14; //Receive DMA begin address register A
    unsigned breserved   : 18; //------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_rxbega;

typedef struct { //SCx_RXENDA - Receive DMA end address register A      (p.108)
    unsigned bSCx_RXENDA : 14; //Receive DMA end address register A
    unsigned breserved   : 18; //------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_rxenda;

typedef struct { //SCx_RXBEGB - Receive DMA begin address register B    (p.107)
    unsigned bSCx_RXBEGB : 14; //Receive DMA begin address register B
    unsigned breserved   : 18; //------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_rxbegb;

typedef struct { //SCx_RXENDB - Receive DMA end address register B      (p.108)
    unsigned bSCx_RXENDB : 14; //Receive DMA end address register B
    unsigned breserved   : 18; //------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_rxendb;

typedef struct { //SCx_TXBEGA - Transmit DMA begin address register A   (p.105)
    unsigned bSCx_TXBEGA : 14; //Transmit DMA begin address register A
    unsigned breserved   : 18; //------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_txbega;

typedef struct { //SCx_TXENDA - Transmit DMA end address register A     (p.106)
    unsigned bSCx_TXENDA : 14; //Transmit DMA end address register A
    unsigned breserved   : 18; //------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_txenda;

typedef struct { //SCx_TXBEGB - Transmit DMA begin address register B   (p.105)
    unsigned bSCx_TXBEGB : 14; //Transmit DMA begin address register B
    unsigned breserved   : 18; //------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_txbegb;

typedef struct { //SCx_TXENDB - Transmit DMA end address register B     (p.106)
    unsigned bSCx_TXENDB : 14; //Transmit DMA end address register B
    unsigned breserved   : 18; //------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_txendb;

typedef struct { //SCx_RXCNTA - Receive DMA count register A            (p.109)
    unsigned bSCx_RXCNTA : 14; //Receive DMA count register A
    unsigned breserved   : 18; //------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_rxcnta;

typedef struct { //SCx_RXCNTB - Receive DMA count register B            (p.109)
    unsigned bSCx_RXCNTB : 14; //Receive DMA count register B
    unsigned breserved   : 18; //------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_rxcntb;

typedef struct { //SCx_TXCNT - Transmit DMA count register              (p.109)
    unsigned bSCx_TXCNT : 14; //Transmit DMA count register
    unsigned breserved   : 18; //------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_txcnt;

typedef struct { //SCx_DMASTAT - Serial DMA status register             (p.104)
    unsigned bSC_RXACTA  : 1;    //This bit is set when DMA receive buffer A is active.
    unsigned bSC_RXACTB  : 1;    //This bit is set when DMA receive buffer B is active.
    unsigned bSC_TXACTA  : 1;    //This bit is set when DMA transmit buffer A is active.
    unsigned bSC_TXACTB  : 1;    //This bit is set when DMA transmit buffer B is active.
    unsigned bSC_RXOVFA  : 1;    //DMA receive buffer A was passed an overrun error from the receive FIFO
    unsigned bSC_RXOVFB  : 1;    //DMA receive buffer B was passed an overrun error from the receive FIFO.
    unsigned bSC_RXPARA  : 1;    //DMA receive buffer A reads a byte with a parity error from the receive FIFO.
    unsigned bSC_RXPARB  : 1;    //DMA receive buffer B reads a byte with a parity error from the receive FIFO.
    unsigned bSC_RXFRMA  : 1;    //DMA receive buffer A reads a byte with a frame error from the receive FIFO.
    unsigned bSC_RXFRMB  : 1;    //DMA receive buffer B reads a byte with a frame error from the receive FIFO.
    unsigned bSC_RXSSEL  : 3;    //Status of the receive count saved in SCx_RXCNTSAVED (SPI slave mode)
    //when nSSEL deasserts. Cleared when a receive buffer is loaded and
    //when the receive DMA is reset.
    unsigned breserved   : 19;   //------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_dmastat;

typedef struct { //SCx_DMACTRL - Serial DMA control register            (p.103)
    unsigned bSC_RXLODA    : 1;  //Setting this bit loads DMA receive buffer A addresses and allows the DMA
    //controller to start processing receive buffer A.
    unsigned bSC_RXLODB    : 1;  //Same as above for buffer B
    unsigned bSC_TXLODA    : 1;  //Setting this bit loads DMA transmit buffer A addresses and allows the DMA
    //controller to start processing transmit buffer A.
    unsigned bSC_TXLODB    : 1;  //Same as above for buffer B
    unsigned bSC_RXDMARST  : 1;  //Setting this bit resets the receive DMA. The bit clears automatically.
    unsigned bSC_TXDMARST  : 1;  //Setting this bit resets the transmit DMA. The bit clears automatically.
    unsigned breserved     : 26; //------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_dmactrl;

typedef struct { //SCx_RXERRA - DMA first receive error register A      (p.110)
    unsigned bSC_RXERRA      : 14;
    unsigned breserved       : 18;
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_rxerra;

typedef struct { //SCx_RXERRB - DMA first receive error register B      (p.111)
    unsigned bSC_RXERRB      : 14;
    unsigned breserved       : 18;
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_rxerrb;

typedef struct { //SCx_DATA - Serial data register                      (p.111)
    unsigned bSC_DATA        : 8;
    unsigned breserved       : 24;
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_data;

typedef struct { //SCx_SPISTAT - SPI status regiser                     (p.97)
    unsigned bSC_SPIRXOVF        : 1; //This bit is set if a byte is received when the receive FIFO is full.
    //This bit is cleared by reading the data register.
    unsigned bSC_SPIRXVAL        : 1; //This bit is set when the receive FIFO contains at least one byte.
    unsigned bSC_SPITXFREE       : 1; //This bit is set when the transmit FIFO has space to accept at least one byte.
    unsigned bSC_SPITXIDLE       : 1; //This bit is set when both the transmit FIFO and the transmit serializer are empty.
    unsigned breserved           : 28; //-------------------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_spistat;

typedef struct { //SCx_TWISTAT - I2C status regiser                     (p.98)
    unsigned bSC_TWIRXNAK        : 1; //This bit is set when a NACK is received from the slave. It clears on the next I2C bus activity.
    unsigned bSC_TWITXFIN        : 1; //This bit is set when a byte is transmitted. It clears on the next I2C bus activity.
    unsigned bSC_TWIRXFIN        : 1; //This bit is set when a byte is received. It clears on the next I2C bus activity.
    unsigned bSC_TWICMDFIN       : 1; //This bit is set when a START or STOP command completes. It clears on the next I2C bus activity.
    unsigned breserved           : 28; //-------------------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_twistat;

typedef struct { //SC1_UARTSTAT - UART status regiser - ONLY IN SC1     (p.100)
    unsigned bSC_UARTCTS          : 1; //This bit is set when both the transmit FIFO and the transmit serializer are empty.
    unsigned bSC_UARTRXVAL        : 1; //This bit is set when the receive FIFO contains at least one byte.
    unsigned bSC_UARTTXFREE       : 1; //This bit is set when the transmit FIFO has space for at least one byte.
    unsigned bSC_UARTRXOVF        : 1; //This bit is set when the receive FIFO has been overrun. This occurs if a byte
    //is received when the receive FIFO is full. This bit is cleared by reading the data register.
    unsigned bSC_UARTFRMERR       : 1; //This bit is set when the byte in the data register was received with a frame
    //error. This bit is updated when the data register is read, and is cleared if the receive FIFO is empty.
    unsigned bSC_UARTPARERR       : 1; //This bit is set when the byte in the data register was received with a parity
    //error. This bit is updated when the data register is read, and is cleared if the receive FIFO is empty.
    unsigned bSC_UARTTXIDLE       : 1; //This bit is set when both the transmit FIFO and the transmit serializer are empty.
    unsigned breserved            : 25; //-------------------------------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_sc1_uartstat;

typedef struct { //SCx_TWICTRL1 - I2C control register 1                (p.99)
    unsigned bSC_TWIRECV         : 1; //Setting this bit receives a byte. It clears when the command completes.
    unsigned bSC_TWISEND         : 1; //Setting this bit transmits a byte. It clears when the command completes.
    unsigned bSC_TWISTART        : 1; //Setting this bit sends the START or repeated START command. It clears when the command completes.
    unsigned bSC_TWISTOP         : 1; //Setting this bit sends the STOP command. It clears when the command completes.
    unsigned breserved           : 28; //--------------------------------------------------
}  __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_twictrl1;

typedef struct { //SCx_TWICTRL2 - I2C control register 2                (p.99)
    unsigned bSC_TWIACK          : 1; //Setting this bit signals ACK after a received byte. Clearing this bit signals NACK after a received byte.
    unsigned breserved           : 31; //--------------------------------------------------
}  __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_twictrl2;

typedef struct { //SCx_MODE - Serial mode register                      (p.92)
    unsigned bSC_MODE          : 2; //set with e_register_stm32w1xx_sc_mode
    unsigned breserved         : 30; //--------------------------------------------------
}  __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_mode;

typedef struct { //SCx_SPICFG - SPI configuration register              (p.96)
    unsigned bSC_SPIPOL      : 1; //Clock polarity configuration: clear this bit for a rising leading edge and
    //set this bit for a falling leading edge.
    unsigned bSC_SPIPHA      : 1; //Clock phase configuration: clear this bit to sample on the leading (first edge) and
    //set this bit to sample on the second edge.
    unsigned bSC_SPIORD      : 1; //This bit specifies the bit order in which SPI data is transmitted and received.
    //0: Most significant bit first. 1: Least significant bit first.
    unsigned bSC_SPIRPT      : 1; //This bit controls behavior on a transmit buffer underrun condition in slave mode.
    unsigned bSC_SPIMST      : 1; //Set this bit to put the SPI in master mode, clear this bit to put the SPI in slave mode.
    unsigned bSC_SPIRXDRV    : 1; //Receiver-driven mode selection bit (SPI master mode only).
    unsigned breserved       : 26; //--------------------------------------------------
}  __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_spicfg;

typedef struct { //SC1_UARTCFG - UART configuration register - ONLY SC1 (p.101)
    unsigned bSC_UARTRTS          : 1; //nRTS is an output to control the flow of serial data sent to the STM32W108 from another device.
    unsigned bSC_UART8BIT         : 1; //Number of data bits.
    unsigned bSC_UART2STP         : 1; //Number of stop bits transmitted.
    unsigned bSC_UARTPAR          : 1; //Specifies whether to use parity bits.
    unsigned bSC_UARTODD          : 1; //If parity is enabled, specifies the kind of parity.
    unsigned bSC_UARTFLOW         : 1; //Set this bit to enable using nRTS/nCTS flow control signals.
    unsigned bSC_UARTAUTO         : 1; //Set this bit to enable automatic nRTS control by hardware (SC_UARTFLOW must also be set).
    unsigned breserved            : 25; //--------------------------------------------------
}  __attribute__( ( __packed__ ) ) t_register_stm32w1xx_sc1_uartcfg;

typedef struct { //SCx_RATELIN - Serial clock linear prescalar reg       (p.97)
    unsigned bSC_RATELIN         : 4; //The linear component (LIN) of the clock rate in the equation:Rate = 12 MHz / ( (LIN + 1) * (2^EXP) )
    unsigned breserved           : 28; //--------------------------------------------------
}  __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_ratelin;

typedef struct { //SCx_RATEEXP - Serial clock exponential prescalar reg  (p.98)
    unsigned bSC_RATEEXP         : 4; //The exponential component (EXP) of the clock rate in the equation: Rate = 12 MHz / ( (LIN + 1) * (2^EXP) )
    unsigned breserved           : 28; //--------------------------------------------------
}  __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_rateexp;

typedef struct { //SC1_UARTPER - UART baud rate period register          (Ember_Manual_EM35x.pdf p.105)
    unsigned bSC_UARTPER         : 16; //The integer part of baud rate period (N) in the equation: Rate = 24 MHz / ( (2 * N) + F )
    unsigned breserved           : 16; //--------------------------------------------------
}  __attribute__( ( __packed__ ) ) t_register_stm32w1xx_sc1_uartper;

typedef struct { //SC1_UARTFRAC - UART baud rate fractional period reg   (Ember_Manual_EM35x.pdf p.105)
    unsigned bSC_UARTFRAC        : 1; //The fractional part of the baud rate period (F) in the equation: Rate = 24 MHz / ( (2 * N) + F )
    unsigned breserved1          : 15; //
    unsigned breserved2          : 16; // bitfields must not be >16 bits
}  __attribute__( ( __packed__ ) ) t_register_stm32w1xx_sc1_uartfrac;

typedef struct { //SCx_RXCNTSAVED - Saved receive DMA count reg          (p.110)
    unsigned bSC_RXCNTSAVED      : 14; //Receive DMA count saved in SPI slave mode when nSSEL deasserts.
    unsigned breserved           : 18; //--------------------------------------------------
}  __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_rxcntsaved;

typedef struct {
    volatile t_register_stm32w1xx_scx_rxbega         RXBEGA;
    volatile t_register_stm32w1xx_scx_rxenda         RXENDA;
    volatile t_register_stm32w1xx_scx_rxbegb         RXBEGB;
    volatile t_register_stm32w1xx_scx_rxendb         RXENDB;
    volatile t_register_stm32w1xx_scx_txbega         TXBEGA;
    volatile t_register_stm32w1xx_scx_txenda         TXENDA;
    volatile t_register_stm32w1xx_scx_txbegb         TXBEGB;
    volatile t_register_stm32w1xx_scx_txendb         TXENDB;
    volatile t_register_stm32w1xx_scx_rxcnta         RXCNTA;
    volatile t_register_stm32w1xx_scx_rxcntb         RXCNTB;
    volatile t_register_stm32w1xx_scx_txcnt          TXCNT;
    volatile t_register_stm32w1xx_scx_dmastat        DMASTAT;
    volatile t_register_stm32w1xx_scx_dmactrl        DMACTRL;
    volatile t_register_stm32w1xx_scx_rxerra         RXERRA;
    volatile t_register_stm32w1xx_scx_rxerrb         RXERRB;
    volatile t_register_stm32w1xx_scx_data           DATA;
    volatile t_register_stm32w1xx_scx_spistat        SPISTAT;
    volatile t_register_stm32w1xx_scx_twistat        TWISTAT;
    volatile t_register_stm32w1xx_sc1_uartstat       UARTSTAT;
    volatile t_register_stm32w1xx_scx_twictrl1       TWICTRL1;
    volatile t_register_stm32w1xx_scx_twictrl2       TWICTRL2;
    volatile t_register_stm32w1xx_scx_mode           MODE;
    volatile t_register_stm32w1xx_scx_spicfg         SPICFG;
    volatile t_register_stm32w1xx_sc1_uartcfg        UARTCFG;
    volatile t_register_stm32w1xx_scx_ratelin        RATELIN;
    volatile t_register_stm32w1xx_scx_rateexp        RATEEXP;
    volatile t_register_stm32w1xx_sc1_uartper        UARTPER;
    volatile t_register_stm32w1xx_sc1_uartfrac       UARTFRAC;
    volatile t_register_stm32w1xx_scx_rxcntsaved     RXCNTSAVED;
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx;

//--------END Register Banks for SC1 and SC2--------------//

typedef struct { // INT_SCxFLAG - Serial controller interrupt flag reg   (p.93)
    unsigned bSCRXVAL                : 1; //Receive buffer has data interrupt pending.
    unsigned bSCTXFREE               : 1; //Transmit buffer free interrupt pending.
    unsigned bSCTXIDLE               : 1; //Transmitter idle interrupt pending.
    unsigned bSCRXOVF                : 1; //Receive buffer overrun interrupt pending.
    unsigned bSCTXUND                : 1; //Transmit buffer underrun interrupt pending.
    unsigned bSCRXFIN                : 1; //Receive operation complete (I2C) interrupt pending.
    unsigned bSCTXFIN                : 1; //Transmit operation complete (I2C) interrupt pending.
    unsigned bSCCMDFIN               : 1; //START/STOP command complete (I2C) interrupt pending.
    unsigned bSCNAK                  : 1; //NACK received (I2C) interrupt pending.
    unsigned bSCRXULDA               : 1; //DMA receive buffer A unloaded interrupt pending.
    unsigned bSCRXULDB               : 1; //DMA receive buffer B unloaded interrupt pending.
    unsigned bSCTXULDA               : 1; //DMA transmit buffer A unloaded interrupt pending.
    unsigned bSCTXULDB               : 1; //DMA transmit buffer B unloaded interrupt pending.
    unsigned bSC1FRMERR              : 1; //Frame error received (UART) interrupt pending.
    unsigned bSC1PARERR              : 1; //Parity error received (UART) interrupt pending.
    unsigned breserved1              : 1;
    unsigned breserved2              : 16;
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_int_scxflag;

typedef struct { // INT_SCxCFG - Serial controller interrupt config reg  (p.94)
    unsigned bSCRXVAL                : 1; //Receive buffer has data interrupt enable.
    unsigned bSCTXFREE               : 1; //Transmit buffer free interrupt enable.
    unsigned bSCTXIDLE               : 1; //Transmitter idle interrupt enable.
    unsigned bSCRXOVF                : 1; //Receive buffer overrun interrupt enable.
    unsigned bSCTXUND                : 1; //Transmit buffer underrun interrupt enable.
    unsigned bSCRXFIN                : 1; //Receive operation complete (I2C) interrupt enable.
    unsigned bSCTXFIN                : 1; //Transmit operation complete (I2C) interrupt enable.
    unsigned bSCCMDFIN               : 1; //START/STOP command complete (I2C) interrupt enable.
    unsigned bSCNAK                  : 1; //NACK received (I2C) interrupt enable.
    unsigned bSCRXULDA               : 1; //DMA receive buffer A unloaded interrupt enable.
    unsigned bSCRXULDB               : 1; //DMA receive buffer B unloaded interrupt enable.
    unsigned bSCTXULDA               : 1; //DMA transmit buffer A unloaded interrupt enable.
    unsigned bSCTXULDB               : 1; //DMA transmit buffer B unloaded interrupt enable.
    unsigned bSC1FRMERR              : 1; //Frame error received (UART) interrupt enable.
    unsigned bSC1PARERR              : 1; //Parity error received (UART) interrupt enable.
    unsigned breserved1              : 1;
    unsigned breserved2              : 16;
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_int_scxcfg;

typedef struct { // SCx_INTMODE - Serial controller interrupt mode reg   (p.95)
    unsigned bSC_RXVALLEVEL                : 1; //Receive buffer has data interrupt mode  [FOR ALL 3, 0=EDGE TRIGGERED; 1=LEVEL TRIGGERED]
    unsigned bSC_TXFREELEVEL               : 1; //Transmit buffer free interrupt mode
    unsigned bSC_TXIDLELEVEL               : 1; //Transmitter idle interrupt mode
    unsigned breserved                     : 29; //-------------------------
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_scx_intmode;


typedef struct {  // stm32w1xx peripheral registers
    // start at 0x40000000 (->config/memory_stm32w1x.ld)
    volatile t_u8 reserved01[0x401c];
    volatile t_register_stm32w1xx_osc24m_ctrl         rOSC24M_CTRL;        // 0x4000401C
    volatile t_u8 reserved02[0x18];
    volatile t_u32                                    rPERIPHERAL_DISABLE; // 0x40004038
    volatile t_u32                                    rTIM1_INT;           // 0x4000A800
    volatile t_u32                                    rTIM2_INT;           // 0x4000A804
    volatile t_u8 reserved03[0x67c4];
    volatile t_register_stm32w1xx_int_scxflag         rSC1_FLAG;           // 0x4000a808
    volatile t_register_stm32w1xx_int_scxflag         rSC2_FLAG;           // 0x4000a80c
    volatile t_u32                                    rINT_ADCFLAG;        // 0x4000A810
    volatile t_register_stm32w1xx_gpio_interruptflag  rINT_GPIOFLAG;      // 0x4000a814
    volatile t_u32                                    rINT_TIM1MISS;       // 0x4000A818
    volatile t_u32                                    rINT_TIM2MISS;       // 0x4000A81C
    volatile t_register_stm32w1xx_int_miss            rINT_MISS;           // 0x4000a820
    volatile t_u32                                    rINT_TIM1CFG;        // 0x4000A840
    volatile t_u32                                    rINT_TIM2CFG;        // 0x4000A844
    volatile t_u8 reserved4[0x1c];
    volatile t_register_stm32w1xx_int_scxcfg          rSC1_CFG;            // 0x4000a848
    volatile t_register_stm32w1xx_int_scxcfg          rSC2_CFG;            // 0x4000a84c
    volatile t_u32                                    rINT_ADCCFG;         // 0x4000A850
    volatile t_register_stm32w1xx_scx_intmode         rSC1_INTMODE;        // 0x4000a854
    volatile t_register_stm32w1xx_scx_intmode         rSC2_INTMODE;        // 0x4000a858
    volatile t_u8 reserved6[0x04];
    volatile t_register_stm32w1xx_gpio_interruptcfg   rGPIO_INT_CFG;       // 0x4000a860
    volatile t_u8 reserved7[0x790];
    volatile t_register_stm32w1xx_gpiox               rGPIOA;              // 0x4000b000
    volatile t_u8 reserved8[0x3e8];
    volatile t_register_stm32w1xx_gpiox               rGPIOB;              // 0x4000b400
    volatile t_u8 reserved9[0x3e8];
    volatile t_register_stm32w1xx_gpiox               rGPIOC;              // 0x4000b800
    volatile t_u8 reservedA[0x3e8];
    volatile t_register_stm32w1xx_gpio_debug_wake_irq rGPIO_DB_WAKE_IRQ;   // 0x4000bc00
    volatile t_u8 reservedB[0x3e0];
    volatile t_register_stm32w1xx_scx                 rSC2;                // 0x4000c000
    volatile t_u8 reservedC[0x78c];
    volatile t_register_stm32w1xx_scx                 rSC1;                // 0x4000c800
    volatile t_u8 reservedD[0x004];
    volatile t_u32                                    rADC_DATA;           // 0x4000D000
    volatile t_u32                                    rADC_CFG;            // 0x4000D004
    volatile t_u32                                    rADC_OFFSET;         // 0x4000D008
    volatile t_u32                                    rADC_GAIN;           // 0x4000D00C
    volatile t_u32                                    rADC_DMACFG;         // 0x4000D010
    volatile t_u32                                    rADC_DMASTAT;        // 0x4000D014
    volatile t_u32                                    rADC_DMABEG;         // 0x4000D018
    volatile t_u32                                    rADC_DMASIZE;        // 0x4000D01C
    volatile t_u32                                    rADC_DMACUR;         // 0x4000D020
    volatile t_u32                                    rADC_DMACNT;         // 0x4000D024
    volatile t_u8 reservedE[0xfd8];
    volatile t_u32                                    rTIM1_CR1;           // 0x4000E000
    volatile t_u32                                    rTIM1_CR2;           // 0x4000E004
    volatile t_u32                                    rTIM1_SMCR;          // 0x4000E008
    volatile t_u8 reservedF[0x008];
    volatile t_u32                                    rTIM1_EGR;           // 0x4000E014
    volatile t_u32                                    rTIM1_CCMR1;         // 0x4000E018
    volatile t_u32                                    rTIM1_CCMR2;         // 0x4000E01C
    volatile t_u32                                    rTIM1_CCER;          // 0x4000E020
    volatile t_u32                                    rTIM1_CNT;           // 0x4000E024
    volatile t_u32                                    rTIM1_PSC;           // 0x4000E028
    volatile t_u32                                    rTIM1_ARR;           // 0x4000E02C
    volatile t_u8 reservedG[0x004];
    volatile t_u32                                    rTIM1_CCR1;          // 0x4000E034
    volatile t_u32                                    rTIM1_CCR2;          // 0x4000E038
    volatile t_u32                                    rTIM1_CCR3;          // 0x4000E03C
    volatile t_u32                                    rTIM1_CCR4;          // 0x4000E040
    volatile t_u8 reservedH[0x00c];
    volatile t_u32                                    rTIM1_OR;            // 0x4000E050
    volatile t_u8 reservedI[0xfac];
    volatile t_u32                                    rTIM2_CR1;           // 0x4000F000
    volatile t_u32                                    rTIM2_CR2;           // 0x4000F004
    volatile t_u32                                    rTIM2_SMCR;          // 0x4000F008
    volatile t_u8 reservedJ[0x004];
    volatile t_u32                                    rTIM2_EGR;           // 0x4000F014
    volatile t_u32                                    rTIM2_CCMR1;         // 0x4000F018
    volatile t_u32                                    rTIM2_CCMR2;         // 0x4000F01C
    volatile t_u32                                    rTIM2_CCER;          // 0x4000F020
    volatile t_u32                                    rTIM2_CNT;           // 0x4000F024
    volatile t_u32                                    rTIM2_PSC;           // 0x4000F028
    volatile t_u32                                    rTIM2_ARR;           // 0x4000F02C
    volatile t_u8 reservedK[0x004];
    volatile t_u32                                    rTIM2_CCR1;          // 0x4000F034
    volatile t_u32                                    rTIM2_CCR2;          // 0x4000F038
    volatile t_u32                                    rTIM2_CCR3;          // 0x4000F03C
    volatile t_u32                                    rTIM2_CCR4;          // 0x4000F040
    volatile t_u8 reservedL[0x00c];
    volatile t_u32                                    rTIM2_OR;            // 0x4000F050

} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_peripherals;
typedef struct {  // stm32w1xx private peripheral registers

    t_u8 reserved1[0xe100];
    t_register_stm32w1xx_int_cfgset          rINT_CFGSET;
    t_u8 reserved2[0x7c];
    t_register_stm32w1xx_int_cfgclr          rINT_CFGCLR;
    t_u8 reserved3[0x7c];
    t_register_stm32w1xx_int_pendset         rINT_PENDSET;
    t_u8 reserved4[0x7c];
    t_register_stm32w1xx_int_pendclr         rINT_PENDCLR;
    t_u8 reserved5[0x7c];
    t_register_stm32w1xx_int_active          rINT_ACTIVE;
    t_u8 reserved6[0xa38];
    t_register_stm32w1xx_scs_afsr            rBUS_AFSR;

} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_private_peripherals;

typedef struct {  // stm32w1xx private peripheral registers

    volatile t_register_stm32w1xx_peripherals*         Peripherals;
    volatile t_register_stm32w1xx_private_peripherals* PrivatePeripherals;
} __attribute__( ( __packed__ ) ) t_register_stm32w1xx_all;

// t_ttc_register_architecture is required by ttc_register_types.h
#define t_ttc_register_architecture volatile t_register_stm32w1xx_all

//} Structures/ Enums


#endif //REGISTER_STM32W1XX_TYPES_H
