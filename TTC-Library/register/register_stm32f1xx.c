/** { register_stm32f1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for register devices on stm32f1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 20 at 20140223 10:54:39 UTC
 *
 *  Note: See ttc_register.h for description of stm32f1xx independent REGISTER implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "register_stm32f1xx.h"
#include "compile_options.h"
#include "additionals/270_CPAL/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h"

/**  Note: Use of ttc_register_set32()/ ttc_register_get32() is mandatory for these registers.
 *   Bitfield implementation of GCC is broken. DO NOT ACCESS THEM DIRECTLY!
 *
 * Examples:
 * ttc_register_set32(register_stm32w1xx_GPIO_DB_WAKE_IRQ.IRQDSEL, bSEL, 23)
 *
 * t_u8 Value;
 * ttc_register_get32(register_stm32w1xx_GPIO_DB_WAKE_IRQ.IRQDSEL, bSEL, Value);
 */

//}

//{ Function definitions *******************************************************

volatile register_stm32f1xx_all_t* register_stm32f1xx_get_configuration() { // ToDo: implement
    return NULL;
}
void register_stm32f1xx_prepare() {

    register_cortexm3_prepare();

#ifdef EXTENSION_basic_extensions_optimize_1_debug
    // testing each register for correct address also ensures that their structs
    // are available during debugging with enabled optimizations

    ttc_assert_address_matches( &( register_stm32f1xx_ADC1 ), ( void* ) ADC1 );
    ttc_assert_address_matches( &( register_stm32f1xx_ADC2 ), ( void* ) ADC2 );
    ttc_assert_address_matches( &( register_stm32f1xx_ADC3 ), ( void* ) ADC3 );
    ttc_assert_address_matches( &( register_stm32f1xx_AFIO ), ( void* ) AFIO );
    ttc_assert_address_matches( &( register_stm32f1xx_CAN1 ), ( void* ) CAN1 );
    ttc_assert_address_matches( &( register_stm32f1xx_CAN2 ), ( void* ) CAN2 );
    ttc_assert_address_matches( &( register_stm32f1xx_DGBMCU ), ( void* ) DBGMCU );
    ttc_assert_address_matches( &( register_stm32f1xx_EXTI ), ( void* ) EXTI );
    ttc_assert_address_matches( &( register_stm32f1xx_GPIOA.CRL ), ( void* ) GPIOA );
    ttc_assert_address_matches( &( register_stm32f1xx_GPIOB.CRL ), ( void* ) GPIOB );
    ttc_assert_address_matches( &( register_stm32f1xx_GPIOC.CRL ), ( void* ) GPIOC );
    ttc_assert_address_matches( &( register_stm32f1xx_GPIOD.CRL ), ( void* ) GPIOD );
    ttc_assert_address_matches( &( register_stm32f1xx_GPIOE.CRL ), ( void* ) GPIOE );
    ttc_assert_address_matches( &( register_stm32f1xx_GPIOF.CRL ), ( void* ) GPIOF );
    ttc_assert_address_matches( &( register_stm32f1xx_GPIOG.CRL ), ( void* ) GPIOG );
    ttc_assert_address_matches( &( register_stm32f1xx_I2C1 ), ( void* ) I2C1 );
    ttc_assert_address_matches( &( register_stm32f1xx_I2C2 ), ( void* ) I2C2 );
    ttc_assert_address_matches( &( register_stm32f1xx_RCC ), ( void* ) RCC );
    ttc_assert_address_matches( &( register_stm32f1xx_RCC.AHBENR ), ( void* ) RCC + 0x14 );
    ttc_assert_address_matches( &( register_stm32f1xx_RCC.APB1ENR ), ( void* ) RCC + 0x1c );
    ttc_assert_address_matches( &( register_stm32f1xx_RCC.APB1RSTR ), ( void* ) RCC + 0x10 );
    ttc_assert_address_matches( &( register_stm32f1xx_RCC.APB2ENR ), ( void* ) RCC + 0x18 );
    ttc_assert_address_matches( &( register_stm32f1xx_RCC.APB2RSTR ), ( void* ) RCC + 0x0c );
    ttc_assert_address_matches( &( register_stm32f1xx_RCC.BDCR ), ( void* ) RCC + 0x20 );
    ttc_assert_address_matches( &( register_stm32f1xx_RCC.CFGR ), ( void* ) RCC + 0x04 );
    ttc_assert_address_matches( &( register_stm32f1xx_RCC.CIR ), ( void* ) RCC + 0x08 );
    ttc_assert_address_matches( &( register_stm32f1xx_RCC.CR ), ( void* ) RCC + 0x00 );
    ttc_assert_address_matches( &( register_stm32f1xx_RCC.CSR ), ( void* ) RCC + 0x24 );
    ttc_assert_address_matches( &( register_stm32f1xx_SPI1 ), ( void* ) SPI1 );
    ttc_assert_address_matches( &( register_stm32f1xx_SPI2 ), ( void* ) SPI2 );
    ttc_assert_address_matches( &( register_stm32f1xx_SPI3 ), ( void* ) SPI3 );
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER1 ), ( void* ) TIM1 );
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER2 ), ( void* ) TIM2 );
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER3 ), ( void* ) TIM3 );
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER4 ), ( void* ) TIM4 );
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER5 ), ( void* ) TIM5 );
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER6 ), ( void* ) TIM6 );
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER7 ), ( void* ) TIM7 );
#ifdef TIM8
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER8 ), ( void* ) TIM8 );
#endif
#ifdef TIM9
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER9 ), ( void* ) TIM9 );
#endif
#ifdef TIM10
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER10 ), ( void* ) TIM10 );
#endif
#ifdef TIM11
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER11 ), ( void* ) TIM11 );
#endif
#ifdef TIM12
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER12 ), ( void* ) TIM12 );
#endif
#ifdef TIM13
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER13 ), ( void* ) TIM13 );
#endif
#ifdef TIM14
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER14 ), ( void* ) TIM14 );
#endif
#ifdef TIM15
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER15 ), ( void* ) TIM15 );
#endif
#ifdef TIM16
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER16 ), ( void* ) TIM16 );
#endif
#ifdef TIM17
    ttc_assert_address_matches( &( register_stm32f1xx_TIMER17 ), ( void* ) TIM17 );
#endif

    ttc_assert_address_matches( &( register_stm32f1xx_UART4 ), ( void* ) UART4 );
    ttc_assert_address_matches( &( register_stm32f1xx_UART5 ), ( void* ) UART5 );
    ttc_assert_address_matches( &( register_stm32f1xx_USART1 ), ( void* ) USART1 );
    ttc_assert_address_matches( &( register_stm32f1xx_USART2 ), ( void* ) USART2 );
    ttc_assert_address_matches( &( register_stm32f1xx_USART3 ), ( void* ) USART3 );

#ifdef STM32F10X_CL
    ttc_assert_address_matches( &( register_stm32f1xx_RCC.AHBRSTR ), ( void* ) 0x40000000 + 0x21028 );
    ttc_assert_address_matches( &( register_stm32f1xx_RCC.CFGR2 ), ( void* ) 0x40000000 + 0x2102c );
#endif
#endif
}
e_ttc_register_errorcode register_stm32f1xx_reset( void* Register ) {  // ToDo: implement address check as in  register_stm32w1xx_prepare()

    Assert_REGISTER_Writable( Register, ttc_assert_origin_auto );  // pointers must not be NULL
    ttc_assert_halt_origin( ttc_assert_origin_auto );

    return ( e_ttc_register_errorcode ) 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
