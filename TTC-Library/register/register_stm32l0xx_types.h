#ifndef REGISTER_STM32L0XX_TYPES_H
#define REGISTER_STM32L0XX_TYPES_H

/** { register_stm32l0xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for REGISTER devices on stm32l0xx architectures.
 *  Structures, Enums and Defines being required by ttc_register_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150319 12:51:52 UTC
 *
 *  Note: See ttc_register.h for description of architecture independent REGISTER implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_register_types.h *************************

typedef struct { // register description (adapt according to stm32l0xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_register_register;

typedef struct {  // stm32l0xx specific configuration data of single REGISTER device
    t_register_register* BaseRegister;       // base address of REGISTER device registers
} __attribute__( ( __packed__ ) ) t_register_stm32l0xx_config;

// t_ttc_register_architecture is required by ttc_register_types.h
#define t_ttc_register_architecture t_register_stm32l0xx_config

//{ ENUMS ****************************************************************
typedef enum {   // e_stm32l0_gpio_mode -  pin modes
    gpio_input,                    // 00
    gpio_general_purpose_output,   // 01
    gpc_alternate_function,        // 10
    gpc_analog                     // 11
} e_stm32l0_gpio_mode;

typedef enum {
    gpio_otype_push_pull = 0x00,
    gpio_otype_open_drain = 0x01
} e_stm32l0_gpio_otype;

typedef enum {   // e_stm32l0_gpio_speed - speeds of pins
    gpio_speed_400kHz,       // 00
    gpio_speed_2MHz,         // 01
    gpio_speed_10MHz,        // 10
    gpio_speed_40MHz         // 11
} e_stm32l0_gpio_speed;

typedef enum {   //e_stm32l0_gpio_pupd - pull-up/pull-down configuration
    gpio_noPULL,          // 00
    gpio_pull_up,         // 01
    gpio_pull_down        // 10
} e_stm32l0_gpio_pupd;

//} ENUMS ****************************************************************

//{ REGISTER STRUCTS********************************************************

typedef struct { //RESERVED - for whole 32-bit spaces in reg maps
    unsigned Reserved   : 32; //whole 32-bit register is reserved
} __attribute__( ( __packed__ ) ) stm32l0_t_reserved_reg;

//---------------------GPIO-----------------------------//
typedef union { //GPIOx_MODER - GPIO port mode register  (p.216) RM0367
    t_u32 All;
    struct {
        unsigned MODER0   : 2; //Port configuration for one pin - see e_stm32l0_gpio_mode
        unsigned MODER1   : 2; //Port configuration for one pin.
        unsigned MODER2   : 2; //Port configuration for one pin.
        unsigned MODER3   : 2; //Port configuration for one pin.
        unsigned MODER4   : 2; //Port configuration for one pin.
        unsigned MODER5   : 2; //Port configuration for one pin.
        unsigned MODER6   : 2; //Port configuration for one pin.
        unsigned MODER7   : 2; //Port configuration for one pin.
        unsigned MODER8   : 2; //Port configuration for one pin.
        unsigned MODER9   : 2; //Port configuration for one pin.
        unsigned MODER10  : 2; //Port configuration for one pin.
        unsigned MODER11  : 2; //Port configuration for one pin.
        unsigned MODER12  : 2; //Port configuration for one pin.
        unsigned MODER13  : 2; //Port configuration for one pin.
        unsigned MODER14  : 2; //Port configuration for one pin.
        unsigned MODER15  : 2; //Port configuration for one pin.
    } Bits;
} __attribute__( ( __packed__ ) ) t_stm32l0_gpiox_moder;

typedef union { //GPIOx_OTYPER - GPIO port output type register  (p.217) RM0367
    t_u32 All;
    struct {
        unsigned OT0   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT1   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT2   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT3   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT4   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT5   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT6   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT7   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT8   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT9   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT10  : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT11  : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT12  : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT13  : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT14  : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT15  : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned Reserved1: 16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0_gpiox_otyper;

typedef union { //GPIOx_OSPEEDR - GPIO output speed Register  (p.217) RM0367
    t_u32 All;
    struct {
        unsigned OSPEEDR0   : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR1   : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR2   : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR3   : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR4   : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR5   : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR6   : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR7   : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR8   : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR9   : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR10  : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR11  : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR12  : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR13  : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR14  : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
        unsigned OSPEEDR15  : 2; //Speed of port pin - see e_stm32l0_gpio_speed.
    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0_gpiox_ospeedr;

typedef union { //GPIOx_PUPDR - GPIO port pull-up/pull-down Register  (p.218) RM0367
    t_u32 All;
    struct {
        unsigned PUPDR0   : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR1   : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR2   : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR3   : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR4   : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR5   : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR6   : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR7   : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR8   : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR9   : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR10  : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR11  : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR12  : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR13  : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR14  : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
        unsigned PUPDR15  : 2; //pull-up or pull-down configuration - see e_stm32l0_gpio_pupd.
    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0_gpiox_pupdr;

typedef union { //GPIOx_IDR - GPIO port input data Register  (p.218) RM0367
    t_u32 All;
    struct {
        unsigned IDR0   : 1; //Input value of I/O (read-only).
        unsigned IDR1   : 1; //Input value of I/O (read-only).
        unsigned IDR2   : 1; //Input value of I/O (read-only).
        unsigned IDR3   : 1; //Input value of I/O (read-only).
        unsigned IDR4   : 1; //Input value of I/O (read-only).
        unsigned IDR5   : 1; //Input value of I/O (read-only).
        unsigned IDR6   : 1; //Input value of I/O (read-only).
        unsigned IDR7   : 1; //Input value of I/O (read-only).
        unsigned IDR8   : 1; //Input value of I/O (read-only).
        unsigned IDR9   : 1; //Input value of I/O (read-only).
        unsigned IDR10  : 1; //Input value of I/O (read-only).
        unsigned IDR11  : 1; //Input value of I/O (read-only).
        unsigned IDR12  : 1; //Input value of I/O (read-only).
        unsigned IDR13  : 1; //Input value of I/O (read-only).
        unsigned IDR14  : 1; //Input value of I/O (read-only).
        unsigned IDR15  : 1; //Input value of I/O (read-only).
        unsigned Reserved1: 16;
    } Bits;
} __attribute__( ( __packed__ ) )  stm32l0_t_gpiox_idr;

typedef union { //GPIOx_ODR - output data Register  (p.219) RM0367
    t_u32 All;
    struct {
        unsigned ODR0   : 1; //Output value of port (read and write).
        unsigned ODR1   : 1; //Output value of port (read and write).
        unsigned ODR2   : 1; //Output value of port (read and write).
        unsigned ODR3   : 1; //Output value of port (read and write).
        unsigned ODR4   : 1; //Output value of port (read and write).
        unsigned ODR5   : 1; //Output value of port (read and write).
        unsigned ODR6   : 1; //Output value of port (read and write).
        unsigned ODR7   : 1; //Output value of port (read and write).
        unsigned ODR8   : 1; //Output value of port (read and write).
        unsigned ODR9   : 1; //Output value of port (read and write).
        unsigned ODR10  : 1; //Output value of port (read and write).
        unsigned ODR11  : 1; //Output value of port (read and write).
        unsigned ODR12  : 1; //Output value of port (read and write).
        unsigned ODR13  : 1; //Output value of port (read and write).
        unsigned ODR14  : 1; //Output value of port (read and write).
        unsigned ODR15  : 1; //Output value of port (read and write).
        unsigned Reserved1: 16;
    } Bits;
} __attribute__( ( __packed__ ) )  stm32l0_t_gpiox_odr;

typedef union { //GPIOx_BSRR - bit set/reset Register  (p.219) RM0367
    t_u32 All;
    struct {
        unsigned BR0   : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR1   : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR2   : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR3   : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR4   : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR5   : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR6   : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR7   : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR8   : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR9   : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR10  : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR11  : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR12  : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR13  : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR14  : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR15  : 1; //Setting this bit resets corresponding ODR bit (write-only).
        unsigned BS0   : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS1   : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS2   : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS3   : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS4   : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS5   : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS6   : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS7   : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS8   : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS9   : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS10  : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS11  : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS12  : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS13  : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS14  : 1; //Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS15  : 1; //Setting this bit sets corresponding ODR bit (write-only).
    } Bits;
} __attribute__( ( __packed__ ) )  stm32l0_t_gpiox_bsrr;

typedef union { //GPIOx_LCKR - configuration lock Register  (p.219) RM0367
    t_u32 All;
    struct {
        unsigned LCK0   : 1; //1: Port config locked. 0: not locked.
        unsigned LCK1   : 1; //1: Port config locked. 0: not locked.
        unsigned LCK2   : 1; //1: Port config locked. 0: not locked.
        unsigned LCK3   : 1; //1: Port config locked. 0: not locked.
        unsigned LCK4   : 1; //1: Port config locked. 0: not locked.
        unsigned LCK5   : 1; //1: Port config locked. 0: not locked.
        unsigned LCK6   : 1; //1: Port config locked. 0: not locked.
        unsigned LCK7   : 1; //1: Port config locked. 0: not locked.
        unsigned LCK8   : 1; //1: Port config locked. 0: not locked.
        unsigned LCK9   : 1; //1: Port config locked. 0: not locked.
        unsigned LCK10  : 1; //1: Port config locked. 0: not locked.
        unsigned LCK11  : 1; //1: Port config locked. 0: not locked.
        unsigned LCK12  : 1; //1: Port config locked. 0: not locked.
        unsigned LCK13  : 1; //1: Port config locked. 0: not locked.
        unsigned LCK14  : 1; //1: Port config locked. 0: not locked.
        unsigned LCK15  : 1; //1: Port config locked. 0: not locked.
        unsigned LCKK   : 1; //Special bit. See p. 146.
        unsigned Reserved1: 15;
    } Bits;
} __attribute__( ( __packed__ ) )  stm32l0_t_gpiox_lckr;

typedef union { //GPIOx_AFRL - alternate function low Register  (p.221) RM0367
    t_u32 All;
    struct {
        unsigned AFRL0   : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
        unsigned AFRl0   : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
        unsigned AFRL2   : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
        unsigned AFRL3   : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
        unsigned AFRL4   : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
        unsigned AFRL5   : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
        unsigned AFRL6   : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
        unsigned AFRL7   : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0_gpiox_afrl;

typedef union { //GPIOx_AFRH - alternate function high Register  (p.221) RM0367
    t_u32 All;
    struct {
        unsigned AFRL8   : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
        unsigned AFRL9   : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
        unsigned AFRl00  : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
        unsigned AFRl01  : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
        unsigned AFRl02  : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
        unsigned AFRl03  : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
        unsigned AFRl04  : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
        unsigned AFRl05  : 4; //Alternate function selection - see stm32l0_gpio_AF_e.
    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0_gpiox_afrh;

typedef union { //GPIOx_BRR - Port bit reset register (p.222) RM0367
    t_u32 All;
    struct {
        unsigned BR0      : 1;
        unsigned BR1      : 1;
        unsigned BR2      : 1;
        unsigned BR3      : 1;
        unsigned BR4      : 1;
        unsigned BR5      : 1;
        unsigned BR6      : 1;
        unsigned BR7      : 1;
        unsigned BR8      : 1;
        unsigned BR9      : 1;
        unsigned BR10     : 1;
        unsigned BR11     : 1;
        unsigned BR12     : 1;
        unsigned BR13     : 1;
        unsigned BR14     : 1;
        unsigned BR15     : 1;
        unsigned Reserved1: 16;
    } Bits;
} __attribute__( ( __packed__ ) )  stm32l0_t_gpiox_brr;


typedef struct { //  All registers for GPIO
    t_stm32l0_gpiox_moder   MODER;  //Mode of pin (see e_stm32l0_gpio_mode)
    t_stm32l0_gpiox_otyper  TYPE;  //Type of output (push-pull or open-drain)
    t_stm32l0_gpiox_ospeedr SPEED; //Speed of output (see  gpio_speed_e)
    t_stm32l0_gpiox_pupdr   PUPD;  //Pull up/down config (see  gpio_pupd_e)
    stm32l0_t_gpiox_idr     IDR;   //Input data register
    stm32l0_t_gpiox_odr     ODR;   //Output data register
    stm32l0_t_gpiox_bsrr    BSRR;  //Bit set/reset register
    stm32l0_t_gpiox_lckr    LCKR;  //Configuration lock register
    t_stm32l0_gpiox_afrl    AFRL;  //Alternate Function (see stm32l0_gpio_AF_e)
    t_stm32l0_gpiox_afrh    AFRH;  //Alternate Function (see stm32l0_gpio_AF_e)
    stm32l0_t_gpiox_brr     BRR;   //Reset register
} __attribute__( ( __packed__ ) ) t_register_stm32l0xx_gpio;

//-----------------END GPIO-----------------------------//

//------------------- SPI ------------------------------//

typedef union { // t_register_stm32l0xx_spi_cr1                SPI Control Register 1 (->RM0367 p.831)
    t_u32 All;
    struct {
        unsigned CPHA     :  1; // Bit 0 CPHA: Clock phase
        unsigned CPOL     :  1; // Bit 1 CPOL: Clock polarity
        unsigned MSTR     :  1; // Bit 2 MSTR: Master selection
        unsigned BR       :  3; // Bits 5:3 BR[2:0]: Baud rate control
        unsigned SPE      :  1; // Bit 6 SPE: SPI enable
        unsigned LSBFIRST :  1; // Bit 7 LSBFIRST: Frame format
        unsigned SSI      :  1; // Bit 8 SSI: Internal slave select
        unsigned SSM      :  1; // Bit 9 SSM: Software slave management
        unsigned RXONLY   :  1; // Bit 10 RXONLY: Receive only
        unsigned DFF      :  1; // Bit 11 DFF: Data frame format
        unsigned CRCNEXT  :  1; // Bit 12 CRCNEXT: CRC transfer next
        unsigned CRC_EN    :  1; // Bit 13 CRC_EN: Hardware CRC calculation enable
        unsigned BIDIOE   :  1; // Bit 14 BIDIOE: Output enable in bidirectional mode
        unsigned BIDIMODE :  1; // Bit 15 BIDIMODE: Bidirectional data mode enable
        unsigned Reserved : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l0xx_spi_cr1;

typedef union { // t_register_stm32l0xx_spi_cr2                SPI Control Register 2 (->RM0367 p.833)
    t_u32 All;
    struct {
        unsigned RXDMA_EN   :  1; // Bit 0 RXDMA_EN: Rx buffer DMA enable
        unsigned TXDMA_EN   :  1; // Bit 1 TXDMA_EN: Tx buffer DMA enable
        unsigned SSOE      :  1; // Bit 2 SSOE: SS output enable
        unsigned Reserved1 :  1; // Bit 3
        unsigned FRF       :  1; // Bit 4 FRF: Frame format -> Motorola mode / TI mode
        unsigned ERRIE     :  1; // Bit 5 ERRIE: Error interrupt enable
        unsigned RXNEIE    :  1; // Bit 6 RXNEIE: RX buffer not empty interrupt enable
        unsigned TXEIE     :  1; // Bit 7 TXEIE: Tx buffer empty interrupt enable
        unsigned Reserved  :  24; // Bits 15:8 Reserved, must be kept at reset value.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l0xx_spi_cr2;
typedef union { // t_register_stm32l0xx_spi_sr                 SPI Status Register (->RM0367 p.834)
    t_u32 All;
    struct {
        unsigned RXNE      :  1; // Bit 0 RXNE: Receive buffer not empty
        unsigned TXE       :  1; // Bit 1 TXE: Transmit buffer empty
        unsigned CHSIDE    :  1; // Bit 2 CHSIDE: Channel side
        unsigned UDR       :  1; // Bit 3 UDR: Underrun flag
        unsigned CRCERR    :  1; // Bit 4 CRCERR: CRC error flag
        unsigned MODF      :  1; // Bit 5 MODF: Mode fault
        unsigned OVR       :  1; // Bit 6 OVR: Overrun flag
        unsigned BSY       :  1; // Bit 7 BSY: Busy flag
        unsigned FRE       :  1; // Bit 8 FRE: Frame Error
        unsigned Reserved  :  23; // Bits 15:9 Reserved, must be kept at reset value.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l0xx_spi_sr;
typedef union { // t_register_stm32l0xx_spi_dr                 SPI Data Register (->RM0367 p.835)
    t_u32 All;
    struct {
        unsigned Data     : 16;        // data received or transmitted
        unsigned Reserved : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l0xx_spi_dr;
typedef union { // t_register_stm32l0xx_spi_crc                SPI CRC Register (->RM0367 p.836)
    t_u32 All;
    struct {
        unsigned Polynom  : 16;        // polynomial used for CRC calculation
        unsigned Reserved : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l0xx_spi_crc;
typedef union { // t_register_stm32l0xx_spi_rx_crc             SPI Computed CRC value of received data (->RM0367 p.837)
    t_u32 All;
    struct {
        unsigned Value    : 16;        // calculated CRC value (only lower 8 bits valid in 8-bit mode)
        unsigned Reserved : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l0xx_spi_rx_crc;
typedef union { // t_register_stm32l0xx_spix_crc_t             SPI Computed CRC value of transmitted data (->RM0367 p.837)
    t_u32 All;
    struct {
        unsigned Value    : 16;        // calculated CRC value (only lower 8 bits valid in 8-bit mode)
        unsigned Reserved : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l0xx_spix_crc_t;
typedef union { // t_register_stm32l0xx_spi_i2scfgr            SPI I2S-Configuration Register (->RM0367 p.838)
    t_u32 All;
    struct {
        unsigned CHL_EN      :  1; // Bit 0 CHL_EN: Channel length (number of bits per audio channel)
        unsigned DATL_EN     :  2; // Bit 2:1 DATL_EN: Data length to be transferred
        unsigned CKPOL      :  1; // Bit 3 CKPOL: Steady state clock polarity
        unsigned I2SSTD     :  2; // Bit 5:4 I2SSTD: I2S standard selection
        unsigned Reserved1  :  1; // Bit 6 Reserved: forced at 0 by hardware
        unsigned PCMSYNC    :  1; // Bit 7 PCMSYNC: PCM frame synchronization
        unsigned I2SCFG     :  2; // Bit 9:8 I2SCFG: I2S configuration mode
        unsigned I2SE       :  1; // Bit 10 I2SE: I2S Enable
        unsigned I2SMOD     :  1; // Bit 11 I2SMOD: I2S mode selection
        unsigned Reserved2  :  20; // Bits 15:12 Reserved, must be kept at reset value.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l0xx_spi_i2scfgr;
typedef union { // t_register_stm32l0xx_spi_i2spr              SPI I2S Prescaler Register (->RM0367 p.839)
    t_u32 All;
    struct {
        unsigned I2SDIV    :  8; // Bit 7:0 I2SDIV: I2S Linear prescaler
        unsigned ODD       :  1; // Bit 8 ODD: Odd factor for the prescaler
        unsigned MCKOE     :  1; // Bit 9 MCKOE: Master clock output enable
        unsigned Reserved  :  22; // Bits 15:10 Reserved, must be kept at reset value.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l0xx_spi_i2spr;

typedef struct { // t_register_stm32l0xx_spi Serial Peripheral Interface
    t_register_stm32l0xx_spi_cr1     CR1;
    t_register_stm32l0xx_spi_cr2     CR2;
    t_register_stm32l0xx_spi_sr      SR;
    t_register_stm32l0xx_spi_dr      DR;
    t_register_stm32l0xx_spi_crc     CRC_Polynom;
    t_register_stm32l0xx_spi_rx_crc  RX_CRC;
    t_register_stm32l0xx_spix_crc_t  TX_CRC;
    t_register_stm32l0xx_spi_i2scfgr I2SCFGR;
    t_register_stm32l0xx_spi_i2spr   I2SPR;
} __attribute__( ( __packed__ ) ) t_register_stm32l0xx_spi;

//-----------------END SPI-----------------------------//

//----------------- USART -----------------------------//

typedef union { // stm32l0_stm32l0_USART_RDR_t     USART Data Register (p. 753) RM0367
    t_u32 All;
    struct {
        unsigned RDR: 9;
        unsigned Reserved1: 23;
    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0xx_usart_rdr;

typedef union { // stm32l0_stm32l0_USART_TDR_t     USART Transmit Data Register (p. 753) RM0367
    t_u32 All;
    struct {
        unsigned TDR: 9;
        unsigned Reserved1: 23;
    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0xx_usart_tdr;

typedef union { // stm32l0_stm32l0_t_usart_brr    USART Baud Rate Register (p. 744) RM0367
    t_u32 All;
    struct {
        unsigned DIV_Fraction: 4;
        unsigned DIV_Mantissa: 12;
        unsigned Reserved1: 16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0xx_usart_brr;
typedef union { // stm32l0_t_usart_cr1    USART Control Register 1 (p. 733) RM0367
    t_u32 All;
    struct {
        unsigned UE: 1;     // USART Enable
        unsigned UESM: 1;   // USART enable in Stop mode
        unsigned RE: 1;     // Receiver enable
        unsigned TE: 1;     // Transmitter enable
        unsigned IDLEIE: 1; // IDLE interrupt enable
        unsigned RXNEIE: 1; // RXNE interrupt enable
        unsigned TCIE: 1;   // Transmission complete interrupt enable
        unsigned TXEIE: 1;  // TXE interrupt enable
        unsigned PEIE: 1;   // PE interrupt enable
        unsigned PS: 1;     // Parity selection
        unsigned PCE: 1;    // Parity control enable
        unsigned WAKE: 1;   // Wakeup method
        unsigned M0: 1;      // Word length
        unsigned MME: 1;    // Mute mode enable
        unsigned CMIE: 1;   // Character match interrupt enable
        unsigned OVER8: 1;  // Oversampling mode
        unsigned DEDT: 5;   // Driver Enable de-assertion time
        unsigned DEAT: 5;   // Driver Enable assertion time
        unsigned RTOIE: 26; // Receiver timeout interrupt enable
        unsigned EOBIE: 27; // End of Block interrupt enable
        unsigned M1: 1;     // Word length
        unsigned Reserved: 3;
    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0xx_usart_cr1;



typedef union { // stm32l0_t_usart_cr2    USART Control Register 2 (p. 736) RM0367
    t_u32 All;
    struct {
        unsigned Reserved0 : 4;
        unsigned ADDM7: 1;      // 7-bit Address Detection/4bit Address Detection
        unsigned LBDL: 1;       // lin break detection length
        unsigned LBDIE: 1;      // LIN break detection interrupt enable
        unsigned Reserved1 : 1; // forced by hardware to 0.
        unsigned LBCL: 1;       // Last bit clock pulse
        unsigned CPHA: 1;       // Clock phase
        unsigned CPOL: 1;       // Clock polarity
        unsigned CLKEN: 1;      // Clock enable
        unsigned STOP: 2;       // STOP bits
        unsigned LINEN: 1;      // LIN mode enable
        unsigned SWAP: 1;       // Swap TX/RX
        unsigned RXINV: 1;      // RX pin active level inversion
        unsigned TXINV: 1;      // TX pin active level inversion
        unsigned DATAINV: 1;    // Binary data inversion
        unsigned MSBFIRST: 1;   // Most significant bit first
        unsigned ABREN: 1;      // Auto baud rate enable
        unsigned ABRMOD: 2;     // Auto baud rate mode
        unsigned RTOEN: 1;      // Receiver timeout enable
        unsigned ADD1: 4;        // Address of the USART node
        unsigned ADD2: 4;        // Address of the USART node
    } Bits;

} __attribute__( ( __packed__ ) )  t_stm32l0xx_usart_cr2;
typedef union { // stm32l0_t_usart_cr3    USART Control Register 3 (p. 740) RM0367
    t_u32 All;
    struct {
        unsigned EIE: 1;   // Error interrupt enable
        unsigned IREN: 1;  // IrDA mode enable
        unsigned IRLP: 1;  // IrDA low-power
        unsigned HDSEL: 1; // Half-duplex selection
        unsigned NACK: 1;  // Smartcard NACK enable
        unsigned SC_EN: 1;  // Smartcard mode enable
        unsigned DMAR: 1;  // DMA enable receiver
        unsigned DMAT: 1;  // DMA enable transmitter
        unsigned RTSE: 1;  // RTS enable
        unsigned CTSE: 1;  // CTS enable
        unsigned CTSIE: 1; // CTS interrupt enable
        unsigned ONEBIT: 1; //One sample bit method enable
        unsigned OVRDIS: 1; // Overrrun Disable
        unsigned DDRE:  1;  // DMA Disable on Reception Error
        unsigned DEM:   1;  // Driver enable mode
        unsigned DEP:   1;  // Dirver enable polarity selection
        unsigned Reserved1: 1;  // Must be kept at reset value.
        unsigned SCARCNT: 3; //Smartcard auto-retry count
        unsigned WUS: 2;    // Wakeup from Stop mode interrupt flag selection
        unsigned WUFIE: 1;  // Wakeup from Stop mode interrupt enable
        unsigned Reserved2: 9; // forced by hardware to 0.
    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0xx_usart_cr3;

typedef union { // stm32l0_USART_GTPR_t   USART Guard Time and Prescaler Register (p. 744) RM0367
    t_u32 All;
    struct {
        unsigned PSC : 8;       // Prescaler
        unsigned GT  : 8;       // Guard Time
        unsigned Reserved : 16; // forced by hardware to 0.
    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0xx_usart_gtpr;

typedef union { // stm32l0_USART_RTOR_t   Receiver timeout register (p. 745) RM0367
    t_u32 All;
    struct {
        unsigned RTO : 24;      // Receiver tiemout value
        unsigned BLEN: 8;       // Block Length
    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0xx_usart_rtor;

typedef union { // stm32l0_USART_RQR_t   Request register (p. 746) RM0367
    t_u32 All;
    struct {
        unsigned ABRRQ: 1;  // Auto baud rate request
        unsigned SBKRQ: 1;  // Send break request
        unsigned MMRQ:  1;  // Mute mode request
        unsigned RXFRQ: 1;  // Receive data flush request
        unsigned TXFRQ: 1;  // Transmit data flush request
        unsigned Reserved: 27;
    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0xx_usart_rqr;

typedef union { // stm32l0_USART_ISR_t   Interrupt & status register (p. 747) RM0367
    t_u32 All;
    struct {
        unsigned PE: 1;     // Parity error
        unsigned FE: 1;     // Framing error
        unsigned NF: 1;     // START bit Noise detection Flag
        unsigned ORE: 1;    // Overrun error
        unsigned IDLE: 1;   // Idle line detected
        unsigned RXNE: 1;   // Read data register not empty
        unsigned TC: 1;     // Transmission complete
        unsigned TXE: 1;    // Transmit data register empty
        unsigned LBDF: 1;   // LIN break detection flag
        unsigned CTSIF: 1;  // CTS interrupt flag
        unsigned CTS: 1;    // CTS flag
        unsigned RTOF: 1;   // Receiver timeout
        unsigned EOBF: 1;   //End of block flag
        unsigned Reserved: 1; // Must be kept at rest value
        unsigned ABRE: 1;   // Auto baud rate error
        unsigned ABRF: 1;   // Auto baud rate flag
        unsigned BUSY: 1;   // Busy flag
        unsigned CMF: 1;    // Character match flag
        unsigned SBKF: 1;   // Send break flag
        unsigned RWU: 1;    // Receiver wakeup from Mute mode
        unsigned WUF: 1;    // Wakeup from Stop mode flag
        unsigned TEACK: 1;  // Transmit enable acknowledge flag
        unsigned REACK: 1;  // Receive enable acknowledge flag
        unsigned Reserved1: 9; // Must be kept at rest value

    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0xx_usart_isr;

typedef union { // stm32l0_USART_ICR_t   Interrupt flag clear register (p. 752) RM0367
    t_u32 All;
    struct {
        unsigned PECF: 1;       // Parity error clear flag
        unsigned FECF: 1;       // Framing error clear flag
        unsigned NCF: 1;        // Noise detection clear flag
        unsigned ORECF: 1;      // Overrun error clear flag
        unsigned IDLECF: 1;     // Idle line detected clear flag
        unsigned Reserved: 1;   // Must be kept at rest value
        unsigned TCCF: 1;       // Transmission complete clear flag
        unsigned Reserved1: 1;  // Must be kept at rest value
        unsigned LBDCF: 1;      // LIN break detection clear flag
        unsigned CTSCF: 1;      // CTS clear flag
        unsigned Reserved2: 1;  // Must be kept at rest value
        unsigned RTOCF: 1;      // Receiver timeout clear flag
        unsigned EOBCF: 1;      //End of block clear flag
        unsigned Reserved3: 4;  // Must be kept at rest value
        unsigned CMCF:  1;      // Character match clear flag
        unsigned Reserved4: 2;  // Must be kept at rest value
        unsigned WUCF:  1;      // Wakeup from Stop mode clear flag
        unsigned Reserved5: 11;  // Must be kept at rest value

    } Bits;
} __attribute__( ( __packed__ ) )  t_stm32l0xx_usart_icr;

typedef struct { // All USART registers
    t_stm32l0xx_usart_cr1  CR1;
    t_stm32l0xx_usart_cr2  CR2;
    t_stm32l0xx_usart_cr3  CR3;
    t_stm32l0xx_usart_brr  BRR;
    t_stm32l0xx_usart_gtpr GTPR;
    t_stm32l0xx_usart_rtor RTOR;
    t_stm32l0xx_usart_rqr RQR;
    t_stm32l0xx_usart_isr ISR;
    t_stm32l0xx_usart_icr ICR;
    t_stm32l0xx_usart_rdr  RDR;
    t_stm32l0xx_usart_tdr  TDR;
} __attribute__( ( __packed__ ) )  t_register_stm32l0xx_usart;

//--------------END USART-----------------------------//

//} Structures/ Enums

#define register_stm32l0xx_all_t void

// t_ttc_register_architecture is required by ttc_register_types.h
#define t_ttc_register_architecture volatile register_stm32l0xx_all_t

//} Structures/ Enums

#endif //REGISTER_STM32L0XX_TYPES_H
