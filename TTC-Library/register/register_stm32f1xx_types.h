#ifndef REGISTER_STM32F1XX_TYPES_H
#define REGISTER_STM32F1XX_TYPES_H

/** { register_stm32f1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for REGISTER devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by ttc_register_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140223 10:54:39 UTC
 *
 *  Note: See ttc_register.h for description of architecture independent REGISTER implementation.
 *  Note: Many enable bits have been renamed to a uniform name scheme (*_EN).
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs *****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes **************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums *****************************************************

typedef enum {   // e_gpio_port_output         values for t_gpiox_crl.MODEx.CNFx/ t_gpiox_crh.CNFx (use if e_gpio_port_mode!=gpm_input)
    gpc_output_push_pull            = 0b00,
    gpc_output_open_drain           = 0b01,
    gpc_output_alternate_push_pull  = 0b10,
    gpc_output_alternate_open_drain = 0b11
} e_gpio_port_output;
typedef enum {   // e_gpio_port_input          values for t_gpiox_crl.MODEx.CNFx/ t_gpiox_crh.CNFx (use if e_gpio_port_mode==gpm_input)
    gpc_input_analog,          // 00
    gpc_input_floating,        // 01
    gpc_input_pull_up_down,    // 10
    gpc_reserved               // 11
} e_gpio_port_input;
typedef enum {   // e_gpio_port_mode           values for t_gpiox_crl.MODEx.MODEx/ t_gpiox_crh.MODEx
    gpm_input        = 0b00,
    gpm_output_10mhz = 0b01,
    gpm_output_2mhz  = 0b10,
    gpm_output_50mhz = 0b11
} e_gpio_port_mode;
typedef struct { // t_gpiox_crl                Port configuration register low (x = A..G) (-> RM0008 p.166)
    unsigned MODE0    : 2; // e_gpio_port_mode
    unsigned CNF0     : 2; // e_gpio_port_input/ e_gpio_port_output
    unsigned MODE1    : 2; // e_gpio_port_mode
    unsigned CNF1     : 2; // e_gpio_port_input/ e_gpio_port_output
    unsigned MODE2    : 2; // e_gpio_port_mode
    unsigned CNF2     : 2; // e_gpio_port_input/ e_gpio_port_output
    unsigned MODE3    : 2; // e_gpio_port_mode
    unsigned CNF3     : 2; // e_gpio_port_input/ e_gpio_port_output
    unsigned MODE4    : 2; // e_gpio_port_mode
    unsigned CNF4     : 2; // e_gpio_port_input/ e_gpio_port_output
    unsigned MODE5    : 2; // e_gpio_port_mode
    unsigned CNF5     : 2; // e_gpio_port_input/ e_gpio_port_output
    unsigned MODE6    : 2; // e_gpio_port_mode
    unsigned CNF6     : 2; // e_gpio_port_input/ e_gpio_port_output
    unsigned MODE7    : 2; // e_gpio_port_mode
    unsigned CNF7     : 2; // e_gpio_port_input/ e_gpio_port_output
} __attribute__( ( __packed__ ) ) t_gpiox_crl;
typedef struct { // t_gpiox_crh                Port configuration register high (x = A..G) (-> RM0008 p.167)
    unsigned MODE8    : 2; // e_gpio_port_mode
    unsigned CNF8     : 2; // e_gpio_port_input/ e_gpio_port_output
    unsigned MODE9    : 2; // e_gpio_port_mode
    unsigned CNF9     : 2; // e_gpio_port_input/ e_gpio_port_output
    unsigned MODE10   : 2; // e_gpio_port_mode
    unsigned CNF10    : 2; // e_gpio_port_input/ e_gpio_port_output
    unsigned MODE11   : 2; // e_gpio_port_mode
    unsigned CNF11    : 2; // e_gpio_port_input/ e_gpio_port_output
    unsigned MODE12   : 2; // e_gpio_port_mode
    unsigned CNF12    : 2; // e_gpio_port_input/ e_gpio_port_output
    unsigned MODE13   : 2; // e_gpio_port_mode
    unsigned CNF13    : 2; // e_gpio_port_input/ e_gpio_port_output
    unsigned MODE14   : 2; // e_gpio_port_mode
    unsigned CNF14    : 2; // e_gpio_port_input/ e_gpio_port_output
    unsigned MODE15   : 2; // e_gpio_port_mode
    unsigned CNF15    : 2; // e_gpio_port_input/ e_gpio_port_output
} __attribute__( ( __packed__ ) ) t_gpiox_crh;
typedef struct { // t_gpiox_idr                GPIOx input data register (x = A..G) (-> RM0008 p.167)
    unsigned DATA     : 16; // input data register of all 16 port bits
    unsigned reserved : 16;
} __attribute__( ( __packed__ ) ) t_gpiox_idr;
typedef struct { // t_gpiox_odr                GPIOx output data register (x = A..G) (-> RM0008 p.167)
    unsigned DATA     : 16; // output data register of all 16 port bits
    unsigned reserved : 16;
} __attribute__( ( __packed__ ) ) t_gpiox_odr;
typedef struct { // t_gpiox_bsrr               GPIOx set/reset register (x = A..G) (-> RM0008 p.168)
    unsigned BS0      : 1; // port set bit 1=set OSR0 bit; 0=no change on OSR0
    unsigned BS1      : 1; // port set bit 1=set OSR1 bit; 0=no change on OSR1
    unsigned BS2      : 1; // port set bit 1=set OSR2 bit; 0=no change on OSR2
    unsigned BS3      : 1; // port set bit 1=set OSR3 bit; 0=no change on OSR3
    unsigned BS4      : 1; // port set bit 1=set OSR4 bit; 0=no change on OSR4
    unsigned BS5      : 1; // port set bit 1=set OSR5 bit; 0=no change on OSR5
    unsigned BS6      : 1; // port set bit 1=set OSR6 bit; 0=no change on OSR6
    unsigned BS7      : 1; // port set bit 1=set OSR7 bit; 0=no change on OSR7
    unsigned BS8      : 1; // port set bit 1=set OSR8 bit; 0=no change on OSR8
    unsigned BS9      : 1; // port set bit 1=set OSR9 bit; 0=no change on OSR9
    unsigned BS10     : 1; // port set bit 1=set OSR10 bit; 0=no change on OSR10
    unsigned BS11     : 1; // port set bit 1=set OSR11 bit; 0=no change on OSR11
    unsigned BS12     : 1; // port set bit 1=set OSR12 bit; 0=no change on OSR12
    unsigned BS13     : 1; // port set bit 1=set OSR13 bit; 0=no change on OSR13
    unsigned BS14     : 1; // port set bit 1=set OSR14 bit; 0=no change on OSR14
    unsigned BS15     : 1; // port set bit 1=set OSR15 bit; 0=no change on OSR15

    unsigned BR0      : 1; // port reset bit 1=reset OSR0 bit; 0=no change on OSR0
    unsigned BR1      : 1; // port reset bit 1=reset OSR1 bit; 0=no change on OSR1
    unsigned BR2      : 1; // port reset bit 1=reset OSR2 bit; 0=no change on OSR2
    unsigned BR3      : 1; // port reset bit 1=reset OSR3 bit; 0=no change on OSR3
    unsigned BR4      : 1; // port reset bit 1=reset OSR4 bit; 0=no change on OSR4
    unsigned BR5      : 1; // port reset bit 1=reset OSR5 bit; 0=no change on OSR5
    unsigned BR6      : 1; // port reset bit 1=reset OSR6 bit; 0=no change on OSR6
    unsigned BR7      : 1; // port reset bit 1=reset OSR7 bit; 0=no change on OSR7
    unsigned BR8      : 1; // port reset bit 1=reset OSR8 bit; 0=no change on OSR8
    unsigned BR9      : 1; // port reset bit 1=reset OSR9 bit; 0=no change on OSR9
    unsigned BR10     : 1; // port reset bit 1=reset OSR10 bit; 0=no change on OSR10
    unsigned BR11     : 1; // port reset bit 1=reset OSR11 bit; 0=no change on OSR11
    unsigned BR12     : 1; // port reset bit 1=reset OSR12 bit; 0=no change on OSR12
    unsigned BR13     : 1; // port reset bit 1=reset OSR13 bit; 0=no change on OSR13
    unsigned BR14     : 1; // port reset bit 1=reset OSR14 bit; 0=no change on OSR14
    unsigned BR15     : 1; // port reset bit 1=reset OSR15 bit; 0=no change on OSR15
} __attribute__( ( __packed__ ) ) t_gpiox_bsrr;
typedef struct { // t_gpiox_brr                GPIOx reset register (x = A..G) (-> RM0008 p.169)
    unsigned BR0      : 1; // port reset bit 1=reset OSR0 bit; 0=no change on OSR0
    unsigned BR1      : 1; // port reset bit 1=reset OSR1 bit; 0=no change on OSR1
    unsigned BR2      : 1; // port reset bit 1=reset OSR2 bit; 0=no change on OSR2
    unsigned BR3      : 1; // port reset bit 1=reset OSR3 bit; 0=no change on OSR3
    unsigned BR4      : 1; // port reset bit 1=reset OSR4 bit; 0=no change on OSR4
    unsigned BR5      : 1; // port reset bit 1=reset OSR5 bit; 0=no change on OSR5
    unsigned BR6      : 1; // port reset bit 1=reset OSR6 bit; 0=no change on OSR6
    unsigned BR7      : 1; // port reset bit 1=reset OSR7 bit; 0=no change on OSR7
    unsigned BR8      : 1; // port reset bit 1=reset OSR8 bit; 0=no change on OSR8
    unsigned BR9      : 1; // port reset bit 1=reset OSR9 bit; 0=no change on OSR9
    unsigned BR10     : 1; // port reset bit 1=reset OSR10 bit; 0=no change on OSR10
    unsigned BR11     : 1; // port reset bit 1=reset OSR11 bit; 0=no change on OSR11
    unsigned BR12     : 1; // port reset bit 1=reset OSR12 bit; 0=no change on OSR12
    unsigned BR13     : 1; // port reset bit 1=reset OSR13 bit; 0=no change on OSR13
    unsigned BR14     : 1; // port reset bit 1=reset OSR14 bit; 0=no change on OSR14
    unsigned BR15     : 1; // port reset bit 1=reset OSR15 bit; 0=no change on OSR15
    unsigned reserved : 16;
} __attribute__( ( __packed__ ) ) t_gpiox_brr;
typedef struct { // t_gpiox_lckr               GPIOx configuration lock register (x = A..G) (-> RM0008 p.169)

    unsigned LCK0     : 1; // lock key configuration for port bit 0
    unsigned LCK1     : 1; // lock key configuration for port bit 1
    unsigned LCK2     : 1; // lock key configuration for port bit 2
    unsigned LCK3     : 1; // lock key configuration for port bit 3
    unsigned LCK4     : 1; // lock key configuration for port bit 4
    unsigned LCK5     : 1; // lock key configuration for port bit 5
    unsigned LCK6     : 1; // lock key configuration for port bit 6
    unsigned LCK7     : 1; // lock key configuration for port bit 7
    unsigned LCK8     : 1; // lock key configuration for port bit 8
    unsigned LCK9     : 1; // lock key configuration for port bit 9
    unsigned LCK10    : 1; // lock key configuration for port bit 10
    unsigned LCK11    : 1; // lock key configuration for port bit 11
    unsigned LCK12    : 1; // lock key configuration for port bit 12
    unsigned LCK13    : 1; // lock key configuration for port bit 13
    unsigned LCK14    : 1; // lock key configuration for port bit 14
    unsigned LCK15    : 1; // lock key configuration for port bit 15
    unsigned LCKK     : 1; // lock key for all port bits
    // Lock key writing sequence
    // write 1
    // write 0
    // write 1
    // read  0
    // read  1 (confirms lock)
    // Note: Any change of LCK0..LCK15 aborts this sequence
    //       Lock Keys can be read at any time

    unsigned reserved : 15;
} __attribute__( ( __packed__ ) ) t_gpiox_lckr;
typedef struct { // t_register_stm32f1xx_gpio  General Purpose Input Output registers
    volatile t_gpiox_crl    CRL;     // Port configuration register low
    volatile t_gpiox_crh    CRH;     // Port configuration register high
    volatile t_gpiox_idr    IDR;     // GPIOx input data register
    volatile t_gpiox_odr    ODR;     // GPIOx output data register
    volatile t_gpiox_bsrr   BSRR;    // GPIOx set/reset register
    volatile t_gpiox_brr    BRR;     // GPIOx reset register
    volatile t_gpiox_lckr   LCKR;    // GPIOx configuration lock register
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_gpio;
typedef struct { // t_register_stm32f1xx_gpio  General Purpose Input Output registers (32 bit values)
    volatile t_u32   CRL;     // Port configuration register low
    volatile t_u32   CRH;     // Port configuration register high
    volatile t_u32   IDR;     // GPIOx input data register
    volatile t_u32   ODR;     // GPIOx output data register
    volatile t_u32   BSRR;    // GPIOx set/reset register
    volatile t_u32   BRR;     // GPIOx reset register
    volatile t_u32   LCKR;    // GPIOx configuration lock register
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_gpio_32;
typedef enum   { // e_afio_event_pin          selects pin used to output Cortex EVENTOUT signal (-> RM0008 p.178)
    gep_px0,  // 0000: pin Px0 selected
    gep_px1,  // 0001: pin Px1 selected
    gep_px2,  // 0010: pin Px2 selected
    gep_px3,  // 0011: pin Px3 selected
    gep_px4,  // 0100: pin Px4 selected
    gep_px5,  // 0101: pin Px5 selected
    gep_px6,  // 0110: pin Px6 selected
    gep_px7,  // 0101: pin Px7 selected
    gep_px8,  // 0111: pin Px8 selected
    gep_px9,  // 1000: pin Px9 selected
    gep_px10, // 1001: pin Px10 selected
    gep_px11, // 1010: pin Px11 selected
    gep_px12, // 1011: pin Px12 selected
    gep_px13, // 1100: pin Px13 selected
    gep_px14, // 1101: pin Px14 selected
    gep_px15  // 1110: pin Px15 selected
} e_afio_event_pin;
typedef enum   { // e_afio_event_port         selects port used to output Cortex EVENTOUT signal (-> RM0008 p.178)
    ges_pxa,  // 000: port PA selected
    ges_pxb,  // 001: port PB selected
    ges_pxc,  // 010: port PC selected
    ges_pxd,  // 011: port PD selected
    ges_pxe,  // 100: port PE selected
    get_unknown
} e_afio_event_port;
typedef struct { // t_afio_evcr               AFIO event control register (x = A..G) (-> RM0008 p.178)

    unsigned PIN       : 4; // afio_event_select_e
    unsigned PORT      : 3; // e_afio_event_port
    unsigned EVOE      : 1; // event output enable
    unsigned reserved1 : 8;
    unsigned reserved2 : 16;
} __attribute__( ( __packed__ ) ) t_afio_evcr;
typedef enum   { // e_gpio_interrupt_source   selects source input for EXTIx external interrupt (-> RM0008 p.185)
    gis_pa,  // 0000: pin PAx selected
    gis_pb,  // 0001: pin PBx selected
    gis_pc,  // 0010: pin PCx selected
    gis_pd,  // 0011: pin PDx selected
    gis_pe,  // 0100: pin PEx selected
    gis_pf,  // 0101: pin PFx selected
    gis_pg   // 0110: pin PGx selected
} e_gpio_interrupt_source;
typedef struct { // t_afio_mapr (->RM0008 p.181)
    unsigned SPI1_REMAP         : 1; // 0: No remap (NSS/PA4, SCK/PA5, MISO/PA6, MOSI/PA7); 1: Remap (NSS/PA15, SCK/PB3, MISO/PB4, MOSI/PB5)
    unsigned I2C1_REMAP         : 1; // 0: No remap (SCL/PB6, SDA/PB7); 1: Remap (SCL/PB8, SDA/PB9)
    unsigned USART1_REMAP       : 1; // 0: No remap (TX/PA9, RX/PA10);  1: Remap (TX/PB6, RX/PB7)
    unsigned USART2_REMAP       : 1; // 0: No remap (CTS/PA0, RTS/PA1, TX/PA2, RX/PA3, CK/PA4);
    // 1: Remap (CTS/PD3, RTS/PD4, TX/PD5, RX/PD6, CK/PD7)
    unsigned USART3_REMAP       : 2; // 00: No remap (TX/PB10, RX/PB11, CK/PB12, CTS/PB13, RTS/PB14),
    // 01: Partial remap (TX/PC10, RX/PC11, CK/PC12, CTS/PB13, RTS/PB14),
    // 10: not used,
    // 11: Full remap (TX/PD8, RX/PD9, CK/PD10, CTS/PD11, RTS/PD12)
    unsigned TIM1_REMAP         : 2; // 00: No remap (ETR/PA12, CH1/PA8, CH2/PA9, CH3/PA10, CH4/PA11, BKIN/PB12, CH1N/PB13, CH2N/PB14, CH3N/PB15),
    // 01: Partial remap (ETR/PA12, CH1/PA8, CH2/PA9, CH3/PA10, CH4/PA11, BKIN/PA6, CH1N/PA7, CH2N/PB0, CH3N/PB1),
    // 10: not used
    // 11: Full remap (ETR/PE7, CH1/PE9, CH2/PE11, CH3/PE13, CH4/PE14, BKIN/PE15, CH1N/PE8, CH2N/PE10, CH3N/PE12)
    unsigned TIM2_REMAP         : 2; // 00: No remap (CH1/ETR/PA0, CH2/PA1, CH3/PA2, CH4/PA3)
    // 01: Partial remap (CH1/ETR/PA15, CH2/PB3, CH3/PA2, CH4/PA3)
    // 10: Partial remap (CH1/ETR/PA0, CH2/PA1, CH3/PB10, CH4/PB11)
    // 11: Full remap (CH1/ETR/PA15, CH2/PB3, CH3/PB10, CH4/PB11)
    unsigned TIM3_REMAP         : 2; // 00: No remap (CH1/PA6, CH2/PA7, CH3/PB0, CH4/PB1)
    // 01: Not used
    // 10: Partial remap (CH1/PB4, CH2/PB5, CH3/PB0, CH4/PB1)
    // 11: Full remap (CH1/PC6, CH2/PC7, CH3/PC8, CH4/PC9)
    // Note: TIM3_ETR on PE0 is not re-mapped.
    unsigned TIM4_REMAP         : 1; // 0: No remap (TIM4_CH1/PB6, TIM4_CH2/PB7, TIM4_CH3/PB8, TIM4_CH4/PB9)
    // 1: Full remap (TIM4_CH1/PD12, TIM4_CH2/PD13, TIM4_CH3/PD14, TIM4_CH4/PD15)
    unsigned CAN_REMAP          : 2; // 00: CAN_RX mapped to PA11, CAN_TX mapped to PA12
    // 01: Not used
    // 10: CAN_RX mapped to PB8, CAN_TX mapped to PB9 (not available on 36-pin package)
    // 11: CAN_RX mapped to PD0, CAN_TX mapped to PD1
    unsigned PD01_REMAP         : 1; // 0: No remapping of PD0 and PD1
    // 1: PD0 remapped on OSC_IN, PD1 remapped on OSC_OUT,
    unsigned TIM5CH4_IREMAP     : 1; // TIM5 channel4 internal remap
    // Set and cleared by software. This bit controls the TIM5_CH4 internal mapping. When reset
    // the timer TIM5_CH4 is connected to PA3. When set the LSI internal clock is connected to
    // TIM5_CH4 input for calibration purpose.
    // Note: This bit is available only in high density value line devices.
    unsigned ADC1_ETRGINJ_REMAP : 1; // ADC 1 External trigger injected conversion remapping
    // Set and cleared by software. This bit controls the trigger input connected to ADC1
    // External trigger injected conversion. When reset the ADC1 External trigger injected
    // conversion is connected to EXTI15. When set the ADC1 External Event injected conversion
    // is connected to TIM8 Channel4.

    unsigned ADC1_ETRGREG_REMAP : 1; // ADC 1 external trigger regular conversion remapping
    // Set and cleared by software. This bit controls the trigger input connected to ADC1
    // External trigger regular conversion. When reset the ADC1 External trigger regular
    // conversion is connected to EXTI11. When set the ADC1 External Event regular conversion
    // is connected to TIM8 TRGO.

    unsigned ADC2_ETRGINJ_REMAP : 1; // ADC 2 external trigger injected conversion remapping
    // Set and cleared by software. This bit controls the trigger input connected to ADC2 external
    // trigger injected conversion. When this bit is reset, the ADC2 external trigger injected
    // conversion is connected to EXTI15. When this bit is set, the ADC2 external event injected
    // conversion is connected to TIM8_Channel4.

    unsigned ADC2_ETRGREG_REMAP : 1; // ADC 2 external trigger regular conversion remapping
    // Set and cleared by software. This bit controls the trigger input connected to ADC2 external
    // trigger regular conversion. When this bit is reset, the ADC2 external trigger regular
    // conversion is connected to EXTI11. When this bit is set, the ADC2 external event regular
    // conversion is connected to TIM8_TRGO.
    unsigned reserved1         : 3;  //
    unsigned SWJ_CFG           : 3;  // Serial wire JTAG configuration
    // These bits are write-only (when read, the value is undefined). They are used to configure the
    // SWJ and trace alternate function I/Os. The SWJ (Serial Wire JTAG) supports JTAG or SWD
    // access to the Cortex debug port. The default state after reset is SWJ ON without trace. This
    // allows JTAG or SW mode to be enabled by sending a specific sequence on the JTMS / JTCK pin.
    // 000: Full SWJ (JTAG-DP + SW-DP): Reset State
    // 001: Full SWJ (JTAG-DP + SW-DP) but without NJTRST
    // 010: JTAG-DP Disabled and SW-DP Enabled
    // 100: JTAG-DP Disabled and SW-DP Disabled
    // Other combinations: no effect

#ifdef STM32F10X_CL // extra mapping bits for connectivity line
    unsigned reserved2         : 1; //
    unsigned SPI3_REMAP        : 1; //
    unsigned TIM2ITR1_IREMAP   : 1; //
    unsigned PTP_PPS_REMAP     : 1; //
    unsigned reserved3         : 1; //
#else
    unsigned reserved2         : 5; //
#endif
} t_afio_mapr;
typedef struct { // t_afio_mapr
    unsigned reserved1   : 5; //
    unsigned TIM9_REMAP  : 1; // 0: No remap (TIM9_CH1 on PA2 and TIM9_CH2 on PA3), 1: Remap (TIM9_CH1 on PE5 and TIM9_CH2 on PE6)
    unsigned TIM10_REMAP : 1; // 0: No remap (TIM10_CH1 on PB8), 1: Remap (TIM10_CH1 on PF6)
    unsigned TIM11_REMAP : 1; // 0: No remap (TIM11_CH1 on PB9), 1: Remap (TIM11_CH1 on PF7)
    unsigned TIM13_REMAP : 1; // 0: No remap (TIM13_CH1 on PA6), 1: Remap (TIM13_CH1 on PF8)
    unsigned TIM14_REMAP : 1; // 0: No remap (TIM14_CH1 on PA7), 1: Remap (TIM14_CH1 on PF9)
    unsigned FSMC_NADV   : 1; // 0: The NADV signal is connected to the output (default)
    // 1: The NADV signal is not connected. The I/O pin can be used by another peripheral.
    unsigned reserved2   : 5;
    unsigned reserved3   : 16; // usigned bitfields >16 bits sometimes make trouble
} t_afio_mapr2;
typedef struct { // t_afio_exticr1            AFIO external interrupt configuration register 1 (x = A..G) (-> RM0008 p.185)
    unsigned EXTI0    : 4; // e_gpio_interrupt_source - source for external interrupt line 0
    unsigned EXTI1    : 4; // e_gpio_interrupt_source - source for external interrupt line 1
    unsigned EXTI2    : 4; // e_gpio_interrupt_source - source for external interrupt line 2
    unsigned EXTI3    : 4; // e_gpio_interrupt_source - source for external interrupt line 3
    unsigned reserved : 16;
} __attribute__( ( __packed__ ) ) t_afio_exticr1;
typedef struct { // t_afio_exticr2            AFIO external interrupt configuration register 2 (x = A..G) (-> RM0008 p.185)
    unsigned EXTI4    : 4; // e_gpio_interrupt_source - source for external interrupt line 4
    unsigned EXTI5    : 4; // e_gpio_interrupt_source - source for external interrupt line 5
    unsigned EXTI6    : 4; // e_gpio_interrupt_source - source for external interrupt line 6
    unsigned EXTI7    : 4; // e_gpio_interrupt_source - source for external interrupt line 7
    unsigned reserved : 16;
} __attribute__( ( __packed__ ) ) t_afio_exticr2;
typedef struct { // t_afio_exticr3            AFIO external interrupt configuration register 3 (x = A..G) (-> RM0008 p.185)
    unsigned EXTI8    : 4; // e_gpio_interrupt_source - source for external interrupt line 8
    unsigned EXTI9    : 4; // e_gpio_interrupt_source - source for external interrupt line 9
    unsigned EXTI10   : 4; // e_gpio_interrupt_source - source for external interrupt line 10
    unsigned EXTI11   : 4; // e_gpio_interrupt_source - source for external interrupt line 11
    unsigned reserved : 16;
} __attribute__( ( __packed__ ) ) t_afio_exticr3;
typedef struct { // t_afio_exticr4            AFIO external interrupt configuration register 4 (x = A..G) (-> RM0008 p.185)
    unsigned EXTI12   : 4; // e_gpio_interrupt_source - source for external interrupt line 12
    unsigned EXTI13   : 4; // e_gpio_interrupt_source - source for external interrupt line 13
    unsigned EXTI14   : 4; // e_gpio_interrupt_source - source for external interrupt line 14
    unsigned EXTI15   : 4; // e_gpio_interrupt_source - source for external interrupt line 15
    unsigned reserved : 16;
} __attribute__( ( __packed__ ) ) t_afio_exticr4;
typedef struct { // t_register_stm32f1xx_afio Alternate Function IO registers
    volatile t_afio_evcr    EVCR;    // AFIO event control register
    volatile t_afio_mapr    MAPR;    // AF remap and debug I/O configuration register 1
    volatile t_afio_exticr1 EXTICR1; // AFIO external interrupt configuration register 1
    volatile t_afio_exticr2 EXTICR2; // AFIO external interrupt configuration register 2
    volatile t_afio_exticr3 EXTICR3; // AFIO external interrupt configuration register 3
    volatile t_afio_exticr4 EXTICR4; // AFIO external interrupt configuration register 4
    volatile t_afio_mapr2   MAPR2;   // AF remap and debug I/O configuration register 2
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_afio;
typedef union { // t_register_stm32f1xx_exti_imr                EXTI interrupt mask register (-> RM0008 p.202)
    t_u32 All;
    struct {
        unsigned MR0 : 1; // =1: interrupt request from line 0 is not masked/ =0: masked
        unsigned MR1 : 1; // =1: interrupt request from line 1 is not masked/ =0: masked
        unsigned MR2 : 1; // =1: interrupt request from line 2 is not masked/ =0: masked
        unsigned MR3 : 1; // =1: interrupt request from line 3 is not masked/ =0: masked
        unsigned MR4 : 1; // =1: interrupt request from line 4 is not masked/ =0: masked
        unsigned MR5 : 1; // =1: interrupt request from line 5 is not masked/ =0: masked
        unsigned MR6 : 1; // =1: interrupt request from line 6 is not masked/ =0: masked
        unsigned MR7 : 1; // =1: interrupt request from line 7 is not masked/ =0: masked
        unsigned MR8 : 1; // =1: interrupt request from line 8 is not masked/ =0: masked
        unsigned MR9 : 1; // =1: interrupt request from line 9 is not masked/ =0: masked
        unsigned MR10 : 1; // =1: interrupt request from line 10 is not masked/ =0: masked
        unsigned MR11 : 1; // =1: interrupt request from line 11 is not masked/ =0: masked
        unsigned MR12 : 1; // =1: interrupt request from line 12 is not masked/ =0: masked
        unsigned MR13 : 1; // =1: interrupt request from line 13 is not masked/ =0: masked
        unsigned MR14 : 1; // =1: interrupt request from line 14 is not masked/ =0: masked
        unsigned MR15 : 1; // =1: interrupt request from line 15 is not masked/ =0: masked
        unsigned MR16 : 1; // =1: interrupt request from line 16 is not masked/ =0: masked
        unsigned MR17 : 1; // =1: interrupt request from line 17 is not masked/ =0: masked
        unsigned MR18 : 1; // =1: interrupt request from line 18 is not masked/ =0: masked
        unsigned MR19 : 1; // =1: interrupt request from line 19 is not masked/ =0: masked
        unsigned reserved : 12;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_exti_imr;
typedef union { // t_register_stm32f1xx_exti_emr                EXTI event mask register (-> RM0008 p.202)
    t_u32 All;
    struct {
        unsigned MR0  : 1; // =1: event request from line 0 is not masked/ =0: masked
        unsigned MR1  : 1; // =1: event request from line 1 is not masked/ =0: masked
        unsigned MR2  : 1; // =1: event request from line 2 is not masked/ =0: masked
        unsigned MR3  : 1; // =1: event request from line 3 is not masked/ =0: masked
        unsigned MR4  : 1; // =1: event request from line 4 is not masked/ =0: masked
        unsigned MR5  : 1; // =1: event request from line 5 is not masked/ =0: masked
        unsigned MR6  : 1; // =1: event request from line 6 is not masked/ =0: masked
        unsigned MR7  : 1; // =1: event request from line 7 is not masked/ =0: masked
        unsigned MR8  : 1; // =1: event request from line 8 is not masked/ =0: masked
        unsigned MR9  : 1; // =1: event request from line 9 is not masked/ =0: masked
        unsigned MR10 : 1; // =1: event request from line 10 is not masked/ =0: masked
        unsigned MR11 : 1; // =1: event request from line 11 is not masked/ =0: masked
        unsigned MR12 : 1; // =1: event request from line 12 is not masked/ =0: masked
        unsigned MR13 : 1; // =1: event request from line 13 is not masked/ =0: masked
        unsigned MR14 : 1; // =1: event request from line 14 is not masked/ =0: masked
        unsigned MR15 : 1; // =1: event request from line 15 is not masked/ =0: masked
        unsigned MR16 : 1; // =1: event request from line 16 is not masked/ =0: masked
        unsigned MR17 : 1; // =1: event request from line 17 is not masked/ =0: masked
        unsigned MR18 : 1; // =1: event request from line 18 is not masked/ =0: masked
        unsigned MR19 : 1; // =1: event request from line 19 is not masked/ =0: masked
        unsigned reserved : 12;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_exti_emr;
typedef union { // t_register_stm32f1xx_exti_rtsr               EXTI rising trigger selection register (-> RM0008 p.203)
    t_u32 All;
    struct {
        unsigned TR0  : 1; // =1: rising trigger enabled for line 0
        unsigned TR1  : 1; // =1: rising trigger enabled for line 1
        unsigned TR2  : 1; // =1: rising trigger enabled for line 2
        unsigned TR3  : 1; // =1: rising trigger enabled for line 3
        unsigned TR4  : 1; // =1: rising trigger enabled for line 4
        unsigned TR5  : 1; // =1: rising trigger enabled for line 5
        unsigned TR6  : 1; // =1: rising trigger enabled for line 6
        unsigned TR7  : 1; // =1: rising trigger enabled for line 7
        unsigned TR8  : 1; // =1: rising trigger enabled for line 8
        unsigned TR9  : 1; // =1: rising trigger enabled for line 9
        unsigned TR10 : 1; // =1: rising trigger enabled for line 10
        unsigned TR11 : 1; // =1: rising trigger enabled for line 11
        unsigned TR12 : 1; // =1: rising trigger enabled for line 12
        unsigned TR13 : 1; // =1: rising trigger enabled for line 13
        unsigned TR14 : 1; // =1: rising trigger enabled for line 14
        unsigned TR15 : 1; // =1: rising trigger enabled for line 15
        unsigned TR16 : 1; // =1: rising trigger enabled for line 16
        unsigned TR17 : 1; // =1: rising trigger enabled for line 17
        unsigned TR18 : 1; // =1: rising trigger enabled for line 18
        unsigned TR19 : 1; // =1: rising trigger enabled for line 19
        unsigned reserved : 12;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_exti_rtsr;
typedef union { // t_register_stm32f1xx_exti_ftsr               EXTI falling trigger selection register (-> RM0008 p.203)
    t_u32 All;
    struct {
        unsigned TR0  : 1; // =1: falling trigger enabled for line 0
        unsigned TR1  : 1; // =1: falling trigger enabled for line 1
        unsigned TR2  : 1; // =1: falling trigger enabled for line 2
        unsigned TR3  : 1; // =1: falling trigger enabled for line 3
        unsigned TR4  : 1; // =1: falling trigger enabled for line 4
        unsigned TR5  : 1; // =1: falling trigger enabled for line 5
        unsigned TR6  : 1; // =1: falling trigger enabled for line 6
        unsigned TR7  : 1; // =1: falling trigger enabled for line 7
        unsigned TR8  : 1; // =1: falling trigger enabled for line 8
        unsigned TR9  : 1; // =1: falling trigger enabled for line 9
        unsigned TR10 : 1; // =1: falling trigger enabled for line 10
        unsigned TR11 : 1; // =1: falling trigger enabled for line 11
        unsigned TR12 : 1; // =1: falling trigger enabled for line 12
        unsigned TR13 : 1; // =1: falling trigger enabled for line 13
        unsigned TR14 : 1; // =1: falling trigger enabled for line 14
        unsigned TR15 : 1; // =1: falling trigger enabled for line 15
        unsigned TR16 : 1; // =1: falling trigger enabled for line 16
        unsigned TR17 : 1; // =1: falling trigger enabled for line 17
        unsigned TR18 : 1; // =1: falling trigger enabled for line 18
        unsigned TR19 : 1; // =1: falling trigger enabled for line 19
        unsigned reserved : 12;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_exti_ftsr;
typedef union { // t_register_stm32f1xx_exti_swier              EXTI software interrupt event register (-> RM0008 p.204)
    t_u32 All;
    struct {
        // Writing a 1 to SWIERx sets corresponding bit in EXTI_PR.
        // SWIERx is cleared by writing a 1 to corresponding bit of EXTI_PR

        unsigned SWIER0 : 1; //
        unsigned SWIER1 : 1; //
        unsigned SWIER2 : 1; //
        unsigned SWIER3 : 1; //
        unsigned SWIER4 : 1; //
        unsigned SWIER5 : 1; //
        unsigned SWIER6 : 1; //
        unsigned SWIER7 : 1; //
        unsigned SWIER8 : 1; //
        unsigned SWIER9 : 1; //
        unsigned SWIER10 : 1; //
        unsigned SWIER11 : 1; //
        unsigned SWIER12 : 1; //
        unsigned SWIER13 : 1; //
        unsigned SWIER14 : 1; //
        unsigned SWIER15 : 1; //
        unsigned SWIER16 : 1; //
        unsigned SWIER17 : 1; //
        unsigned SWIER18 : 1; //
        unsigned SWIER19 : 1; //
        unsigned reserved : 12;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_exti_swier;
typedef union { // t_register_stm32f1xx_exti_pr                 EXTI pending interrupt register (-> RM0008 p.204)
    t_u32 All;
    struct {

        unsigned PR0  : 1; // =1: trigger request has occured on line 0
        unsigned PR1  : 1; // =1: trigger request has occured on line 1
        unsigned PR2  : 1; // =1: trigger request has occured on line 2
        unsigned PR3  : 1; // =1: trigger request has occured on line 3
        unsigned PR4  : 1; // =1: trigger request has occured on line 4
        unsigned PR5  : 1; // =1: trigger request has occured on line 5
        unsigned PR6  : 1; // =1: trigger request has occured on line 6
        unsigned PR7  : 1; // =1: trigger request has occured on line 7
        unsigned PR8  : 1; // =1: trigger request has occured on line 8
        unsigned PR9  : 1; // =1: trigger request has occured on line 9
        unsigned PR10 : 1; // =1: trigger request has occured on line 10
        unsigned PR11 : 1; // =1: trigger request has occured on line 11
        unsigned PR12 : 1; // =1: trigger request has occured on line 12
        unsigned PR13 : 1; // =1: trigger request has occured on line 13
        unsigned PR14 : 1; // =1: trigger request has occured on line 14
        unsigned PR15 : 1; // =1: trigger request has occured on line 15
        unsigned PR16 : 1; // =1: trigger request has occured on line 16
        unsigned PR17 : 1; // =1: trigger request has occured on line 17
        unsigned PR18 : 1; // =1: trigger request has occured on line 18
        unsigned PR19 : 1; // =1: trigger request has occured on line 19
        unsigned reserved : 12;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_exti_pr;
typedef struct { // t_register_stm32f1xx_exti external interrupt registers (-> RM0008 p.205)

    volatile t_register_stm32f1xx_exti_imr   IMR;   // interrupt mask register
    volatile t_register_stm32f1xx_exti_emr   EMR;   // event mask register
    volatile t_register_stm32f1xx_exti_rtsr  RTSR;  // rising trigger selection register
    volatile t_register_stm32f1xx_exti_ftsr  FTSR;  // falling trigger selection register
    volatile t_register_stm32f1xx_exti_swier SWIER; // software interrupt event register
    volatile t_register_stm32f1xx_exti_pr    PR;    // pending interrupt register

} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_exti;
#ifdef STM32F10X_CL

typedef struct { // t_register_stm32f1xx_rcc_cr                  Clock Control Register (-> RM0008 p.96,129)
    unsigned HSION        : 1; // Internal high-speed clock enable
    unsigned HSIRDY       : 1; // Internal high-speed clock ready flag
    unsigned reserved1    : 1; // Bit 2
    unsigned HSITRIM      : 5; // Internal high-speed clock trimming
    unsigned HSICAL       : 8; // Internal high-speed clock calibration. These bits are initialized automatically at startup.
    unsigned HSEON        : 1; // HSE clock enable
    unsigned HSERDY       : 1; // External high-speed clock ready flag
    unsigned HSEBYP       : 1; // External high-speed clock bypass
    unsigned CSSON        : 1; // Clock security system enable
    unsigned reserved2    : 4; // Bit 20..23
    unsigned PLLON        : 1; // PLL enable
    unsigned PLLRDY       : 1; // PLL clock ready flag
    unsigned PLL2ON       : 1; // PLL2 enable
    unsigned PLL2RDY      : 1; // PLL2 clock ready flag
    unsigned PLL3ON       : 1; // PLL3 enable
    unsigned PLL3RDY      : 1; // PLL3 clock ready flag
    unsigned reserved3    : 2; // Bit 30..31

} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_rcc_cr;

#else
typedef struct { // t_register_stm32f1xx_rcc_cr                  Clock Control Register (-> RM0008 p.96,129)
    // -> RM0008 p.96
    unsigned HSION        : 1; // Internal high-speed clock enable
    unsigned HSIRDY       : 1; // Internal high-speed clock ready flag
    unsigned reserved1    : 1; // Bit 2
    unsigned HSITRIM      : 5; // Internal high-speed clock trimming
    unsigned HSICAL       : 8; // Internal high-speed clock calibration. These bits are initialized automatically at startup.
    unsigned HSEON        : 1; // HSE clock enable
    unsigned HSERDY       : 1; // External high-speed clock ready flag
    unsigned HSEBYP       : 1; // External high-speed clock bypass
    unsigned CSSON        : 1; // Clock security system enable
    unsigned reserved2    : 4; // Bit
    unsigned PLLON        : 1; // PLL enable
    unsigned PLLRDY       : 1; // PLL clock ready flag
    unsigned reserved3    : 6; // 26..31

} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_rcc_cr;
#endif
typedef struct { // t_register_stm32f1xx_rcc_cfgr                Clock Configuration Register (-> RM0008 p.98 or p.131)
    // Structure of this register is same for all STM32F1xx devices.
    // Though the values of some fields have different meanings for Connectivity Line devices.
    // Check datasheet RM0008 for details!
    // -> RM0008 p.131, 98

    unsigned SW           : 2; // System clock switch (0: HSI, 1: HSE, 2: PLL, 3: not applicable)
    unsigned SWS          : 2; // System clock switch status (same values as SW)
    unsigned HPRE         : 4; // AHB prescaler
    unsigned PPRE1        : 3; // APB low-speed prescaler (APB1)
    unsigned PPRE2        : 3; // APB high-speed prescaler (APB2Set and cleared by software to control th(PCLK2).
    unsigned ADCPRE       : 2; // ADC prescaler
    unsigned PLLSRC       : 1; // PLL entry clock source
    unsigned PLLXTPRE     : 1; // HSE divider for PLL entry
    unsigned PLLMUL       : 4; // PLL multiplication factor (different values for Connectivity Line devices!)
    unsigned USBPRE       : 1; // USB prescaler
    unsigned MCO          : 4; // Microcontroller clock output (different values for Connectivity Line devices!)
    unsigned reserved2    : 5; // reserved
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_rcc_cfgr;
#ifdef STM32F10X_CL
typedef struct { // t_register_stm32f1xx_rcc_cir                 Clock interrupt register for Connectivity Line devices
    // -> RM0008 p.134
    unsigned LSIRDYF      : 1; // LSI ready interrupt flag
    unsigned LSERDYF      : 1; // LSE ready interrupt flag
    unsigned HSIRDYF      : 1; // HSI ready interrupt flag
    unsigned HSERDYF      : 1; // HSE ready interrupt flag
    unsigned PLLRDYF      : 1; // PLL ready interrupt flag
    unsigned reserved1    : 2; //
    unsigned CSSF         : 1; // Clock security system interrupt flag
    unsigned LSIRDYIE     : 1; // LSI ready interrupt enable
    unsigned LSERDYIE     : 1; // LSE ready interrupt enable
    unsigned HSIRDYIE     : 1; // HSI ready interrupt enable
    unsigned HSERDYIE     : 1; // HSE ready interrupt enable
    unsigned PLLRDYIE     : 1; // PLL ready interrupt enable
    unsigned reserved2    : 3; //
    unsigned LSIRDYC      : 1; // LSI ready interrupt clear
    unsigned LSERDYC      : 1; // LSE ready interrupt clear
    unsigned HSIRDYC      : 1; // HSI ready interrupt clear
    unsigned HSERDYC      : 1; // HSE ready interrupt clear
    unsigned PLLRDYC      : 1; // PLL ready interrupt clear
    unsigned reserved3    : 2; //
    unsigned CSSC         : 1; // Clock security system interrupt clear
    unsigned reserved     : 8; // Bit 24:31
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_rcc_cir;
#else
typedef struct { // t_register_stm32f1xx_rcc_cir                 Clock interrupt register for non Connectivity Line devices
    // -> RM0008 p.101
    unsigned LSIRDYF      : 1; // LSI ready interrupt flag
    unsigned LSERDYF      : 1; // LSE ready interrupt flag
    unsigned HSIRDYF      : 1; // HSI ready interrupt flag
    unsigned HSERDYF      : 1; // HSE ready interrupt flag
    unsigned PLLRDYF      : 1; // PLL1 ready interrupt flag
    unsigned PLL2RDYF     : 1; // PLL2 ready interrupt flag
    unsigned PLL3RDYF     : 1; // PLL3 ready interrupt flag
    unsigned CSSF         : 1; // Clock security system interrupt flag
    unsigned LSIRDYIE     : 1; // LSI ready interrupt enable
    unsigned LSERDYIE     : 1; // LSE ready interrupt enable
    unsigned HSIRDYIE     : 1; // HSI ready interrupt enable
    unsigned HSERDYIE     : 1; // HSE ready interrupt enable
    unsigned PLLRDYIE     : 1; // PLL1 ready interrupt enable
    unsigned PLL2RDYIE    : 1; // PLL2 ready interrupt enable
    unsigned PLL3RDYIE    : 1; // PLL3 ready interrupt enable
    unsigned reserved2    : 1; // Bit 15
    unsigned LSIRDYC      : 1; // LSI ready interrupt clear
    unsigned LSERDYC      : 1; // LSE ready interrupt clear
    unsigned HSIRDYC      : 1; // HSI ready interrupt clear
    unsigned HSERDYC      : 1; // HSE ready interrupt clear
    unsigned PLLRDYC      : 1; // PLL1 ready interrupt clear
    unsigned PLL2RDYC     : 1; // PLL2 ready interrupt clear
    unsigned PLL3RDYC     : 1; // PLL3 ready interrupt clear
    unsigned CSSC         : 1; // Clock security system interrupt clear
    unsigned reserved     : 8; // Bit 24:31
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_rcc_cir;
#endif
#ifdef STM32F10X_CL
typedef struct { // t_register_stm32f1xx_rcc_apb2rstr            APB2 peripheral reset register for Connectivity Line devices
    // -> RM0008 p.137
    unsigned AFIORST      :  1; //  0  Alternate function I/O reset
    unsigned reserved1    :  1; //  1
    unsigned IOPARST      :  1; //  2  I/O port A reset
    unsigned IOPBRST      :  1; //  3  I/O port B reset
    unsigned IOPCRST      :  1; //  4  I/O port C reset
    unsigned IOPDRST      :  1; //  5  I/O port D reset
    unsigned IOPERST      :  1; //  6  I/O port E reset
    unsigned reserved2    :  2; //  7..8
    unsigned ADC1RST      :  1; //  9  ADC 1 interface reset
    unsigned ADC2RST      :  1; // 10  ADC 2 interface reset
    unsigned TIM1RST      :  1; // 11  TIM1 timer reset
    unsigned SPI1RST      :  1; // 12  SPI 1 reset
    unsigned reserved3    :  1; // 13
    unsigned USART1RST    :  1; // 14  USART1 reset
    unsigned reserved4    : 17; // 15..31

} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_rcc_apb2rstr;
#else
typedef struct { // t_register_stm32f1xx_rcc_apb2rstr            APB2 peripheral reset register for non Connectivity Line devices
    // -> RM0008 p.103
    unsigned AFIORST      :  1; //  0  Alternate function I/O reset
    unsigned reserved1    :  1; //  1
    unsigned IOPARST      :  1; //  2  I/O port A reset
    unsigned IOPBRST      :  1; //  3  I/O port B reset
    unsigned IOPCRST      :  1; //  4  I/O port C reset
    unsigned IOPDRST      :  1; //  5  I/O port D reset
    unsigned IOPERST      :  1; //  6  I/O port E reset
    unsigned IOPFRST      :  1; //  7  I/O port F reset
    unsigned IOPGRST      :  1; //  8  I/O port G reset
    unsigned ADC1RST      :  1; //  9  ADC 1 interface reset
    unsigned ADC2RST      :  1; // 10  ADC 2 interface reset
    unsigned TIM1RST      :  1; // 11  TIM1 timer reset
    unsigned SPI1RST      :  1; // 12  SPI 1 reset
    unsigned TIM8RST      :  1; // 13  TIM8 timer reset
    unsigned USART1RST    :  1; // 14  USART1 reset
    unsigned ADC3RST      :  1; // 15  ADC 3 interface reset
    unsigned reserved2    :  3; // 16
    unsigned TIM9RST      :  1; // 19  TIM9 timer reset
    unsigned TIM10RST     :  1; // 20  TIM10 timer reset
    unsigned TIM11RST     :  1; // 21  TIM11 timer reset
    unsigned reserved3    : 10; // 22

} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_rcc_apb2rstr;
#endif

// use bit numbers for atomic single bit access via cm3_set_peripheral_bit()
#define BITNUMBER_APB2RSTR_AFIORST       0 // Alternate function I/O reset
#define BITNUMBER_APB2RSTR_IOPARST       2 // I/O port A reset
#define BITNUMBER_APB2RSTR_IOPBRST       3 // I/O port B reset
#define BITNUMBER_APB2RSTR_IOPCRST       4 // I/O port C reset
#define BITNUMBER_APB2RSTR_IOPDRST       5 // I/O port D reset
#define BITNUMBER_APB2RSTR_IOPERST       6 // I/O port E reset
#define BITNUMBER_APB2RSTR_IOPFRST       7 // I/O port F reset
#define BITNUMBER_APB2RSTR_IOPGRST       8 // I/O port G reset
#define BITNUMBER_APB2RSTR_ADC1RST       9 // ADC 1 interface reset
#define BITNUMBER_APB2RSTR_ADC2RST      10 // ADC 2 interface reset
#define BITNUMBER_APB2RSTR_TIM1RST      11 // TIM1 timer reset
#define BITNUMBER_APB2RSTR_SPI1RST      12 // SPI 1 reset
#define BITNUMBER_APB2RSTR_TIM8RST      13 // TIM8 timer reset
#define BITNUMBER_APB2RSTR_USART1RST    14 // USART1 reset
#define BITNUMBER_APB2RSTR_ADC3RST      15 // ADC 3 interface reset
#define BITNUMBER_APB2RSTR_TIM9RST      19 // TIM9 timer reset
#define BITNUMBER_APB2RSTR_TIM10RST     20 // TIM10 timer reset
#define BITNUMBER_APB2RSTR_TIM11RST     21 // TIM11 timer reset


#ifdef STM32F10X_CL
typedef struct { // t_register_stm32f1xx_rcc_apb1rstr            APB1 Peripheral Reset Register in Connectivity Line Devices
    // -> RM0008 p.138
    unsigned TIM2RST      : 1; // Timer 2 reset
    unsigned TIM3RST      : 1; // Timer 3 reset
    unsigned TIM4RST      : 1; // Timer 4 reset
    unsigned TIM5RST      : 1; // Timer 5 reset
    unsigned TIM6RST      : 1; // Timer 6 reset
    unsigned TIM7RST      : 1; // Timer 7 reset
    unsigned reserved1    : 5; // Bit 6..10
    unsigned WWDGRST      : 1; // Window watchdog reset
    unsigned reserved2    : 2; // Bit 12..13
    unsigned SPI2RST      : 1; // SPI2 reset
    unsigned SPI3RST      : 1; // SPI3 reset
    unsigned reserved3    : 1; // Bit 16
    unsigned USART2RST    : 1; // USART 2 reset
    unsigned USART3RST    : 1; // USART 3 reset
    unsigned UART4RST     : 1; // USART 4 reset
    unsigned UART5RST     : 1; // USART 5 reset
    unsigned I2C1RST      : 1; // I2C1 reset
    unsigned I2C2RST      : 1; // I2C 2 reset
    unsigned reserved4    : 2; // Bit 23..24
    unsigned CAN1RST      : 1; // CAN1 reset
    unsigned CAN2RST      : 1; // CAN2 reset
    unsigned BKPRST       : 1; // Backup interface reset
    unsigned PWRRST       : 1; // Power interface reset
    unsigned DACRST       : 1; // DAC interface reset
    unsigned reserved5    : 2; // Bit 30..31
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_rcc_apb1rstr;
#else
typedef struct { // t_register_stm32f1xx_rcc_apb1rstr            APB1 Peripheral Reset Register in non Connectivity Line Devices
    // -> RM0008 p.106
    unsigned TIM2RST      : 1; // Timer 2 reset
    unsigned TIM3RST      : 1; // Timer 3 reset
    unsigned TIM4RST      : 1; // Timer 4 reset
    unsigned TIM5RST      : 1; // Timer 5 reset
    unsigned TIM6RST      : 1; // Timer 6 reset
    unsigned TIM7RST      : 1; // Timer 7 reset
    unsigned TIM12RST     : 1; // Timer 7 reset
    unsigned TIM13RST     : 1; // Timer 7 reset
    unsigned TIM14RST     : 1; // Timer 7 reset
    unsigned reserved1    : 2; // Bit 9..10
    unsigned WWDGRST      : 1; // Window watchdog reset
    unsigned reserved2    : 2; // Bit 12..13
    unsigned SPI2RST      : 1; // SPI 2 reset
    unsigned SPI3RST      : 1; // SPI 3 reset
    unsigned reserved3    : 1; // Bit 16
    unsigned USART2RST    : 1; // USART 2 reset
    unsigned USART3RST    : 1; // USART 3 reset
    unsigned UART4RST     : 1; // USART 4 reset
    unsigned UART5RST     : 1; // USART 5 reset
    unsigned I2C1RST      : 1; // I2C 1 reset
    unsigned I2C2RST      : 1; // I2C 2 reset
    unsigned USBRST       : 1; // USB reset
    unsigned reserved4    : 1; // Bit 24
    unsigned CANRST       : 1; // CAN 1 reset
    unsigned reserved5    : 1; // Bit 26
    unsigned BKPRST       : 1; // Backup interface reset
    unsigned PWRRST       : 1; // Power interface reset
    unsigned DACRST       : 1; // DAC interface reset
    unsigned reserved6    : 2; //
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_rcc_apb1rstr;
#endif

#ifdef STM32F10X_CL
typedef struct { // t_register_stm32f1xx_rcc_ahbenr              AHB Peripheral Clock enable register in Connectivity Line Devices
    // -> RM0008 p.141
    unsigned DMA1_EN       : 1; // DMA1 clock enable
    unsigned DMA2_EN       : 1; // DMA2 clock enable
    unsigned SRAM_EN       : 1; // SRAM interface clock enable
    unsigned reserved1    : 1; //
    unsigned FLITF_EN      : 1; // FLITF clock enable
    unsigned reserved2    : 1; //
    unsigned CRC_EN        : 1; // CRC clock enable
    unsigned reserved3    : 5; //
    unsigned OTGFS_EN      : 1; // USB OTG FS clock enable
    unsigned reserved4    : 1; //
    unsigned ETHMAC_EN     : 1; // Ethernet MAC clock enable
    unsigned ETHMACTX_EN   : 1; // Ethernet MAC TX clock enable
    unsigned ETHMACRX_EN   : 1; // Ethernet MAC RX clock enable
    unsigned reserved     : 15; //

} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_rcc_ahbenr;
#else
typedef struct { // t_register_stm32f1xx_rcc_ahbenr              AHB Peripheral Clock enable register in non Connectivity Line Devices
    // -> RM0008 p.108
    unsigned DMA1_EN      : 1;  // DMA1 clock enable
    unsigned DMA2_EN      : 1;  // DMA2 clock enable
    unsigned SRAM_EN      : 1;  // SRAM interface clock enable
    unsigned reserved1    : 1;  // Bit 3
    unsigned FLITF_EN     : 1;  // FLITF clock enable
    unsigned reserved2    : 1;  // Bit 5
    unsigned CRC_EN       : 1;  // CRC clock enable
    unsigned reserved3    : 1;  // Bit 7
    unsigned FSMC_EN      : 1;  // Flexible Static Memory Controller clock enable
    unsigned reserved4    : 1;  // Bit 9
    unsigned SDIO_        : 1;  // Serial Disc IO clock enable
    unsigned reserved     : 21; // Bit 11..31

} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_rcc_ahbenr;
#endif

#ifdef STM32F10X_CL_DISABLED
typedef struct { // t_register_stm32f1xx_rcc_apb2_enr              APB2 Peripheral Clock enable register in Connectivity Line Devices
    // -> RM0008 p.142
    unsigned AFIO_EN      : 1; // Alternate Function IO clock enable
    unsigned reserved1    : 1; // Bit 1
    unsigned IOPA_EN      : 1; // IO port A clock enable
    unsigned IOPB_EN      : 1; // IO port B clock enable
    unsigned IOPC_EN      : 1; // IO port C clock enable
    unsigned IOPD_EN      : 1; // IO port D clock enable
    unsigned IOPE_EN      : 1; // IO port E clock enable
    unsigned reserved2    : 2; // Bit 7..8
    unsigned ADC1_EN      : 1; // ADC1 interface clock enable
    unsigned ADC2_EN      : 1; // ADC2 interface clock enable
    unsigned TIM1_EN      : 1; // Timer1 clock enable
    unsigned SPI1_EN      : 1; // SPI1 clock enable
    unsigned reserved3    : 1; // Bit 13
    unsigned USART1_EN    : 1; // USART1 clock enable
    unsigned reserved4    : 17;// Bit 15..31
} __attribute__( ( __packed__ ) ) volatile t_register_stm32f1xx_rcc_apb2_enr;
#else
typedef struct { // t_register_stm32f1xx_rcc_apb2_enr              APB2 Peripheral Clock enable register in non Connectivity Line Devices
    // -> RM0008 p.109
    unsigned AFIO_EN      : 1; // Alternate Function IO clock enable
    unsigned reserved1    : 1; // Bit 1
    unsigned IOPA_EN      : 1; // IO port A clock enable
    unsigned IOPB_EN      : 1; // IO port B clock enable
    unsigned IOPC_EN      : 1; // IO port C clock enable
    unsigned IOPD_EN      : 1; // IO port D clock enable
    unsigned IOPE_EN      : 1; // IO port E clock enable
    unsigned IOPF_EN      : 1; // IO port F clock enable
    unsigned IOPG_EN      : 1; // IO port G clock enable
    unsigned ADC1_EN      : 1; // ADC1 interface clock enable
    unsigned ADC2_EN      : 1; // ADC2 interface clock enable
    unsigned TIM1_EN      : 1; // Timer1 clock enable
    unsigned SPI1_EN      : 1; // SPI1 clock enable
    unsigned TIM8_EN      : 1; // Timer8 clock enable
    unsigned USART1_EN    : 1; // USART1 clock enable
    unsigned ADC3_EN      : 1; // ADC3 interface clock enable
    unsigned reserved2    : 3; // Bit 16..18
    unsigned TIM9_EN      : 1; // Timer9  clock enable
    unsigned TIM10_EN     : 1; // Timer10 clock enable
    unsigned TIM11_EN     : 1; // Timer11 clock enable
    unsigned reserved3    : 10;// Bit 22..31
} __attribute__( ( __packed__ ) ) volatile t_register_stm32f1xx_rcc_apb2_enr;
#endif

#ifdef STM32F10X_CL
typedef struct { // volatile t_register_stm32f1xx_rcc_apb1_enr            APB1 peripheral clock enable register in Connectivity Line Devices
    // -> RM0008 p. 144
    unsigned TIM2_EN       : 1; // timer  2 clock enable
    unsigned TIM3_EN       : 1; // timer  3 clock enable
    unsigned TIM4_EN       : 1; // timer  4 clock enable
    unsigned TIM5_EN       : 1; // timer  5 clock enable
    unsigned TIM6_EN       : 1; // timer  6 clock enable
    unsigned TIM7_EN       : 1; // timer  7 clock enable
    unsigned reserved6     : 5; // Bit 6..10
    unsigned WWDG_EN       : 1; // Window Watchdog enable
    unsigned reserved5     : 2; // Bit 12..13
    unsigned SPI2_EN       : 1; // SPI2 clock enable
    unsigned SPI3_EN       : 1; // SPI3 clock enable
    unsigned reserved4     : 1; // Bit 16
    unsigned USART2_EN     : 1; // USART2 clock enable
    unsigned USART3_EN     : 1; // USART3 clock enable
    unsigned UART4_EN      : 1; // UART4 clock enable
    unsigned UART5_EN      : 1; // UART5 clock enable
    unsigned I2C1_EN       : 1; // I2C1 clock enable
    unsigned I2C2_EN       : 1; // I2C2 clock enable
    unsigned reserved2     : 2; // Bit 23..24
    unsigned CAN1_EN       : 1; // CAN 1 clock enable
    unsigned CAN2_EN       : 1; // CAN 2 clock enable
    unsigned BKP_EN        : 1; // Backup interface clock enable
    unsigned PWR_EN        : 1; // Power interface clock enable
    unsigned DAC_EN        : 1; // Digital Analog Converter clock enable
    unsigned reserved1    : 2; // Bit 30..31
} __attribute__( ( __packed__ ) ) volatile t_register_stm32f1xx_rcc_apb1_enr;
#else
typedef struct { // volatile t_register_stm32f1xx_rcc_apb1_enr            APB1 peripheral clock enable register in non Connectivity Line devices
    // -> RM0008 p. 112
    unsigned TIM2_EN       : 1; // timer  2 clock enable
    unsigned TIM3_EN       : 1; // timer  3 clock enable
    unsigned TIM4_EN       : 1; // timer  4 clock enable
    unsigned TIM5_EN       : 1; // timer  5 clock enable
    unsigned TIM6_EN       : 1; // timer  6 clock enable
    unsigned TIM7_EN       : 1; // timer  7 clock enable
    unsigned TIM12_EN      : 1; // timer 12 clock enable
    unsigned TIM13_EN      : 1; // timer 13 clock enable
    unsigned TIM14_EN      : 1; // timer 14 clock enable
    unsigned reserved6     : 2; // Bit 6..7
    unsigned WWDG_EN       : 1; // Window Watchdog enable
    unsigned reserved5     : 2; // Bit 12..13
    unsigned SPI2_EN       : 1; // SPI2 clock enable
    unsigned SPI3_EN       : 1; // SPI3 clock enable
    unsigned reserved4     : 1; // Bit 16
    unsigned USART2_EN     : 1; // USART2 clock enable
    unsigned USART3_EN     : 1; // USART3 clock enable
    unsigned UART4_EN      : 1; // UART4 clock enable
    unsigned UART5_EN      : 1; // UART5 clock enable
    unsigned I2C1_EN       : 1; // I2C1 clock enable
    unsigned I2C2_EN       : 1; // I2C2 clock enable
    unsigned USB_EN        : 1; // USB clock enable
    unsigned reserved3     : 1; // Bit 24
    unsigned CAN_EN        : 1; // CAN 1 clock enable
    unsigned reserved2     : 1; // Bit 26
    unsigned BKP_EN        : 1; // Backup interface clock enable
    unsigned PWR_EN        : 1; // Power interface clock enable
    unsigned DAC_EN        : 1; // Digital Analog Converter clock enable
    unsigned reserved1    : 2; // Bit 30..31
} __attribute__( ( __packed__ ) ) volatile t_register_stm32f1xx_rcc_apb1_enr;
#endif

typedef struct { // volatile t_register_stm32f1xx_rcc_bdcr                Backup Domain Control Register
    // Structure of this register is same for all STM32F1xx devices.
    // -> RM0008 p. 146 or 115
    unsigned LSEON      : 1; // External Low Speed oscillator enable
    unsigned LSERDY     : 1; // External Low Speed oscillator ready
    unsigned LSEBYP     : 1; // External Low Speed oscillator bypass
    unsigned reserved1  : 5; //
    unsigned RTCSEL     : 2; // RTC clock source selection
    unsigned reserved2  : 5; //
    unsigned RTC_EN     : 1; // RTC clock enable
    unsigned BDRST      : 1; // Backup domain software reset
    unsigned reserved3  : 15; //
} __attribute__( ( __packed__ ) ) volatile t_register_stm32f1xx_rcc_bdcr;
typedef struct { // t_register_stm32f1xx_rcc_csr                 Control/status register
    // Structure of this register is same for all STM32F1xx devices.
    // -> RM0008 p. 148 or 117
    unsigned LSION      : 1; // Internal low speed oscillator enable
    unsigned LSIRDY     : 1; // Internal low speed oscillator ready
    unsigned reserved1  : 22; //
    unsigned RMVF       : 1; // Remove reset flag
    unsigned reserved2  : 1; //
    unsigned PINRSTF    : 1; // PIN reset flag
    unsigned PORRSTF    : 1; // POR/PDR reset flag
    unsigned SFTRSTF    : 1; // Software reset flag
    unsigned IWDGRSTF   : 1; // Independent watchdog reset flag
    unsigned WWDGRSTF   : 1; // Window watchdog reset flag
    unsigned LPWRRSTF   : 1; // Low-power reset flag
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_rcc_csr;

#ifdef STM32F10X_CL
typedef struct { // t_register_stm32f1xx_rcc_ahbrstr             AHB Peripheral Clock Reset Register
    // -> RM0008 p.149
    unsigned reserved1  : 12; //
    unsigned OTGFSRST   : 1; // USB OTG FS reset
    unsigned reserved2  : 1; //
    unsigned ETHMACRST  : 1; // Ethernet MAC reset
    unsigned reserved3  : 17; //
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_rcc_ahbrstr;
typedef struct { // t_cfgr2               Clock Configuration Register2

    unsigned PREDIV1    : 4; // PREDIV1 division factor
    unsigned PREDIV2    : 4; // PREDIV2 division factor
    unsigned PLL2MUL    : 4; // PLL2 Multiplication Factor
    unsigned PLL3MUL    : 4; // PLL3 Multiplication Factor
    unsigned PREDIV1SRC : 1; // PREDIV1 entry clock source
    unsigned I2S2SRC    : 1; // I2S2 clock source
    unsigned I2S3SRC    : 1; // I2S3 clock source
    unsigned reserved1  : 13; //
}  __attribute__( ( __packed__ ) ) t_cfgr2;
#endif

typedef struct { // t_register_stm32f1xx_rcc  Reset and Clock Control (RCC)

    volatile t_register_stm32f1xx_rcc_cr        CR;
    volatile t_register_stm32f1xx_rcc_cfgr      CFGR;
    volatile t_register_stm32f1xx_rcc_cir       CIR;
    volatile t_register_stm32f1xx_rcc_apb2rstr  APB2RSTR;
    volatile t_register_stm32f1xx_rcc_apb1rstr  APB1RSTR;
    volatile t_register_stm32f1xx_rcc_ahbenr    AHBENR;
    volatile t_register_stm32f1xx_rcc_apb2_enr  APB2ENR;
    volatile t_register_stm32f1xx_rcc_apb1_enr  APB1ENR;
    volatile t_register_stm32f1xx_rcc_bdcr      BDCR;
    volatile t_register_stm32f1xx_rcc_csr       CSR;
#ifdef STM32F10X_CL
    volatile t_register_stm32f1xx_rcc_ahbrstr   AHBRSTR;
    t_cfgr2     CFGR2;
#endif
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_rcc;
typedef union { // register_stm32f1xx_timx_cr1_t               timer control register 1
    volatile t_u16 All;
    struct {
        unsigned  C_EN        : 1;
        unsigned  UDIS        : 1;
        unsigned  URS         : 1;
        unsigned  OPM         : 1;
        unsigned  DIR         : 1;
        unsigned  CMS         : 2;
        unsigned  ARPE        : 1;
        unsigned  CKD         : 2;
        unsigned  reserved1   : 6;
    } Bits;
} __attribute__( ( __packed__ ) ) u_register_stm32f1xx_timx_cr1;
typedef union { // register_stm32f1xx_timx_cr2_t               timer control register 2
    volatile t_u16 All;
    struct {
        unsigned  CCPC        : 1;
        unsigned  reserved2   : 1;
        unsigned  CCUS        : 1;
        unsigned  CCDS        : 1;
        unsigned  MMS         : 2;
        unsigned  TI1S        : 1;
        unsigned  OIS1        : 1;
        unsigned  OIS1N       : 1;
        unsigned  OIS2        : 1;
        unsigned  OIS2N       : 1;
        unsigned  OIS3        : 1;
        unsigned  OIS3N       : 1;
        unsigned  OIS4        : 1;
        unsigned  reserved1   : 1;
    } Bits;
} __attribute__( ( __packed__ ) ) u_register_stm32f1xx_timx_cr2;
typedef union { // register_stm32f1xx_timx_smcr_t              timer slave mode control register
    volatile t_u16 All;
    struct {
        unsigned  SMS         : 3;
        unsigned  reserved    : 1;
        unsigned  TS          : 3;
        unsigned  MSM         : 1;
        unsigned  ETF         : 4;
        unsigned  ETPS        : 2;
        unsigned  EXE         : 1;
        unsigned  ETP         : 1;
    } Bits;
} __attribute__( ( __packed__ ) ) u_register_stm32f1xx_timx_smcr;
typedef union { // register_stm32f1xx_timx_dier_t              timer DMA/interrupt enable register
    volatile t_u16 All;
    struct {
        unsigned  UIE         : 1;
        unsigned  CC1IE       : 1;
        unsigned  CC2IE       : 1;
        unsigned  CC3IE       : 1;
        unsigned  CC4IE       : 1;
        unsigned  COMIE       : 1;
        unsigned  TIE         : 1;
        unsigned  BIE         : 1;
        unsigned  UDE         : 1;
        unsigned  CC1DE       : 1;
        unsigned  CC2DE       : 1;
        unsigned  CC3DE       : 1;
        unsigned  CC4DE       : 1;
        unsigned  COMDE       : 1;
        unsigned  TDE         : 1;
        unsigned  reserved1   : 1;
    } Bits;
} __attribute__( ( __packed__ ) ) u_register_stm32f1xx_timx_dier;
typedef union { // register_stm32f1xx_timx_sr_t                timer status register
    volatile t_u16 All;
    struct {
        unsigned  UIF         : 1;
        unsigned  CC1IF       : 1;
        unsigned  CC2IF       : 1;
        unsigned  CC3IF       : 1;
        unsigned  CC4IF       : 1;
        unsigned  COMIF       : 1;
        unsigned  TIF         : 1;
        unsigned  BIF         : 1;
        unsigned  reserved2   : 1;
        unsigned  CC1OF       : 1;
        unsigned  CC2OF       : 1;
        unsigned  CC3OF       : 1;
        unsigned  CC4OF       : 1;
        unsigned  reserved1   : 3;
    } Bits;
} __attribute__( ( __packed__ ) ) u_register_stm32f1xx_timx_sr;
typedef union { // register_stm32f1xx_timx_egr_t               timer event generation register
    volatile t_u16 All;
    struct {
        unsigned  UG          : 1;
        unsigned  CC1G        : 1;
        unsigned  CC2G        : 1;
        unsigned  CC3G        : 1;
        unsigned  CC4G        : 1;
        unsigned  COMG        : 1;
        unsigned  TG          : 1;
        unsigned  BG          : 1;
        unsigned  reserved1   : 8;
    } Bits;
} __attribute__( ( __packed__ ) ) u_register_stm32f1xx_timx_egr;
typedef union { // t_timx_ccmr1             timer capture/compare register 1
    volatile t_u16 All;
    struct {
        unsigned  CC1S        : 2;
        unsigned  OC1FE       : 1;
        unsigned  OC1PE       : 1;
        unsigned  OC1M        : 3;
        unsigned  OC1CE       : 1;
        unsigned  CC2S        : 2;
        unsigned  OC2FE       : 1;
        unsigned  OC2PE       : 1;
        unsigned  OC2M        : 3;
        unsigned  OC2CE       : 1;
    } Bits;
} __attribute__( ( __packed__ ) ) u_register_stm32f1xx_timx_ccmr1;
typedef union { // t_timx_ccmr2             timer capture/compare register 2
    volatile t_u16 All;
    struct {
        unsigned  CC3S        : 2;
        unsigned  OC3FE       : 1;
        unsigned  OC3PE       : 1;
        unsigned  OC3M        : 3;
        unsigned  OC3CE       : 1;
        unsigned  CC4S        : 2;
        unsigned  OC4FE       : 1;
        unsigned  OC4PE       : 1;
        unsigned  OC4M        : 3;
        unsigned  OC4CE       : 1;
    } Bits;
} __attribute__( ( __packed__ ) ) u_register_stm32f1xx_timx_ccmr2;
typedef union { // register_stm32f1xx_timx_ccer_t              timer DMA/interrupt enable register
    volatile t_u16 All;
    struct {
        unsigned  CC1E      : 1;
        unsigned  CC1P      : 1;
        unsigned  CC1NE     : 1;
        unsigned  CC1NP     : 1;
        unsigned  CC2E      : 1;
        unsigned  CC2P      : 1;
        unsigned  CC2NE     : 1;
        unsigned  CC2NP     : 1;
        unsigned  CC3E      : 1;
        unsigned  CC3P      : 1;
        unsigned  CC3NE     : 1;
        unsigned  CC3NP     : 1;
        unsigned  CC4E      : 1;
        unsigned  CC4P      : 1;
        unsigned  reserved1 : 2;
    } Bits;
} __attribute__( ( __packed__ ) ) u_register_stm32f1xx_timx_ccer;
typedef union { // t_timx_bdtr              timer break and dead-time register
    volatile t_u16 All;
    struct {
        unsigned  DTG         : 8;
        unsigned  LOCK        : 2;
        unsigned  OSSI        : 1;
        unsigned  OSSR        : 1;
        unsigned  BKE         : 1;
        unsigned  BKP_        : 1;
        unsigned  AOE         : 1;
        unsigned  MOE         : 1;
    } Bits;
} __attribute__( ( __packed__ ) ) u_register_stm32f1xx_timx_btdr;
typedef union { // register_stm32f1xx_timx_dcr_t               timer DMA control register
    volatile t_u16 All;
    struct {
        unsigned  DBA         : 5;
        unsigned  reserved2   : 3;
        unsigned  DBL         : 5;
        unsigned  reserved1   : 1;
    } Bits;
} __attribute__( ( __packed__ ) ) u_register_stm32f1xx_timx_dcr;
typedef struct { // t_register_stm32f1xx_timer timer 1 registers
    volatile u_register_stm32f1xx_timx_cr1    CR1;
    volatile t_u16                            reserved0;
    volatile u_register_stm32f1xx_timx_cr2    CR2;
    volatile t_u16                            reserved1;
    volatile u_register_stm32f1xx_timx_smcr   SMCR;
    volatile t_u16                            reserved2;
    volatile u_register_stm32f1xx_timx_dier   DIER;
    volatile t_u16                            reserved3;
    volatile u_register_stm32f1xx_timx_sr     SR;
    volatile t_u16                            reserved4;
    volatile u_register_stm32f1xx_timx_egr    EGR;
    volatile t_u16                            reserved5;
    volatile u_register_stm32f1xx_timx_ccmr1  CCMR1;
    volatile t_u16                            reserved6;
    volatile u_register_stm32f1xx_timx_ccmr2  CCMR2;
    volatile t_u16                            reserved7;
    volatile u_register_stm32f1xx_timx_ccer   CCER;
    volatile t_u16                            reserved8;
    volatile t_u16                            CNT;
    volatile t_u16                            reserved9;
    volatile t_u16                            PSC;
    volatile t_u16                            reserved10;
    volatile t_u16                            ARR;
    volatile t_u16                            reserved11;
    volatile t_u16                            RCR_; // RCC already defined elsewhere
    volatile t_u16                            reserved12;
    volatile t_u16                            CCR1;
    volatile t_u16                            reserved13;
    volatile t_u16                            CCR2;
    volatile t_u16                            reserved14;
    volatile t_u16                            CCR3;
    volatile t_u16                            reserved15;
    volatile t_u16                            CCR4;
    volatile t_u16                            reserved16;
    volatile u_register_stm32f1xx_timx_btdr   BDTR;
    volatile t_u16                            reserved17;
    volatile u_register_stm32f1xx_timx_dcr    DCR;
    volatile t_u16                            reserved18;
    volatile t_u16                            DMAR;
    volatile t_u16                            reserved19;
} t_register_stm32f1xx_timer;
typedef union { // t_register_stm32f1xx_usart_sr               USART Status Register

    volatile t_u32 All;
    struct {
        unsigned PE   : 1; // Parity Error
        unsigned FE   : 1; // Framing Error
        unsigned NE   : 1; // Noise Error
        unsigned ORE  : 1; // Overrun Error
        unsigned IDLE : 1; // IDLE line detected
        unsigned RXNE : 1; // Read Data Register not empty
        unsigned TC   : 1; // Transmission complete
        unsigned TXE  : 1; // Transmission Data Register empty
        unsigned LBD  : 1; // Lin break detection flag
        unsigned CTS  : 1; // CTS Flag
        unsigned reserved1: 6;
        unsigned reserved2: 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_usart_sr;
typedef union { // t_register_stm32f1xx_usart_dr               USART Data Register

    volatile t_u32 All;

    struct {
        unsigned DR: 9;
        unsigned reserved1: 23;
    } Bits;

} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_usart_dr;
typedef union { // t_register_stm32f1xx_usart_brr              USART Baud Rate Register
    volatile t_u32 All;
    struct {
        unsigned DIV_Fraction: 4;
        unsigned DIV_Mantissa: 12;
        unsigned reserved1: 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_usart_brr;
typedef union { // t_register_stm32f1xx_usart_cr1              USART Control Register 1
    volatile t_u32 All;
    struct {
        unsigned SBK: 1;    // Send break
        unsigned RWU: 1;    // Receiver wakeup
        unsigned RE: 1;     // Receiver enable
        unsigned TE: 1;     // Transmitter enable
        unsigned IDLEIE: 1; // IDLE interrupt enable
        unsigned RXNEIE: 1; // RX not empty interrupt enable
        unsigned TCIE: 1;   // Transmission complete interrupt enable
        unsigned TXEIE: 1;  // TX empty interrupt enable
        unsigned PEIE: 1;   // Parity Error interrupt enable
        unsigned PS: 1;     // Parity selection: =0: Parity control disabled, =1: Parity control enabled
        unsigned PCE: 1;    // Parity control enable: =0: Parity control disabled, =1: Parity control enabled
        unsigned WAKE: 1;   // Wakeup method : =0: Idle Line, =1: Address Mark
        unsigned M: 1;      // Word length: =0: 1 Start bit, 8 Data bits, n Stop bit, =1: 1 Start bit, 9 Data bits, n Stop bit
        unsigned UE: 1;     // USART enable
        unsigned reserved1: 1;
        unsigned OVER8: 1;  // =0: Oversampling mode is 16 Samples; =1: 8 Samples  (not documented but found in StdPeripheral code)
        unsigned reserved2: 1;
        unsigned reserved3: 16;
    } Bits;

} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_usart_cr1;
typedef union { // t_register_stm32f1xx_usart_cr2              USART Control Register 2

    volatile t_u32 All;
    struct {

        unsigned ADD: 4;        // Address of the USART node
        unsigned reserved1 : 1; // forced by hardware to 0.
        unsigned LBDL: 1;       // lin break detection length
        unsigned LBDIE: 1;      // LIN break detection interrupt enable
        unsigned reserved2 : 1; // forced by hardware to 0.
        unsigned LBCL: 1;       // Last bit clock pulse
        unsigned CPHA: 1;       // Clock phase
        unsigned CPOL: 1;       // Clock polarity
        unsigned CLKEN: 1;      // Clock enable
        unsigned STOP: 2;       // STOP bits
        unsigned LINEN: 1;      // LIN mode enable
        unsigned reserved4 : 17;

    } Bits;


} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_usart_cr2;
typedef union { // t_register_stm32f1xx_usart_cr3              USART Control Register 3

    volatile t_u32 All;
    struct {

        unsigned EIE: 1;   // Error interrupt enable
        unsigned IREN: 1;  // IrDA mode enable
        unsigned IRLP: 1;  // IrDA low-power
        unsigned HDSEL: 1; // Half-duplex selection
        unsigned NACK: 1;  // Smartcard NACK enable
        unsigned SC_EN: 1;  // Smartcard mode enable
        unsigned DMAR: 1;  // DMA enable receiver
        unsigned DMAT: 1;  // DMA enable transmitter
        unsigned RTSE: 1;  // RTS enable
        unsigned CTSE: 1;  // CTS enable
        unsigned CTSIE: 1; // CTS interrupt enable
        unsigned reserved2 : 21;
    } Bits;

} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_usart_cr3;
typedef union { // t_usart_gptr             USART Guard Time and Prescaler Register

    volatile t_u32 All;
    struct {
        unsigned PSC : 8;       // Prescaler
        unsigned GT  : 8;       // Guard Time
        unsigned Reservado1: 16;
    } Bits;
    //X  unsigned reserved : 16; // forced by hardware to 0.
} __attribute__( ( __packed__ ) ) t_usart_gptr;
typedef struct { // t_register_stm32f1xx_usart USART registers
    volatile t_register_stm32f1xx_usart_sr   SR;
    volatile t_register_stm32f1xx_usart_dr   DR;
    volatile t_register_stm32f1xx_usart_brr  BRR;
    volatile t_register_stm32f1xx_usart_cr1  CR1;
    volatile t_register_stm32f1xx_usart_cr2  CR2;
    volatile t_register_stm32f1xx_usart_cr3  CR3;
    t_usart_gptr GPTR;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_usart;
typedef union { // t_register_stm32f1xx_spi_cr1                SPI Control Register 1 (->RM0008 p.715)
    volatile t_u16 All;
    struct {
        unsigned CPHA     :  1; // Bit 0 CPHA: Clock phase
        unsigned CPOL     :  1; // Bit 1 CPOL: Clock polarity
        unsigned MSTR     :  1; // Bit 2 MSTR: Master selection
        unsigned BR       :  3; // Bits 5:3 BR[2:0]: Baud rate control
        unsigned SPE      :  1; // Bit 6 SPE: SPI enable
        unsigned LSBFIRST :  1; // Bit 7 LSBFIRST: Frame format
        unsigned SSI      :  1; // Bit 8 SSI: Internal slave select
        unsigned SSM      :  1; // Bit 9 SSM: Software slave management
        unsigned RXONLY   :  1; // Bit 10 RXONLY: Receive only
        unsigned DFF      :  1; // Bit 11 DFF: Data frame format
        unsigned CRCNEXT  :  1; // Bit 12 CRCNEXT: CRC transfer next
        unsigned CRC_EN    :  1; // Bit 13 CRC_EN: Hardware CRC calculation enable
        unsigned BIDIOE   :  1; // Bit 14 BIDIOE: Output enable in bidirectional mode
        unsigned BIDIMODE :  1; // Bit 15 BIDIMODE: Bidirectional data mode enable
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_spi_cr1;
typedef union { // t_register_stm32f1xx_spi_cr2                SPI Control Register 2 (->RM0008 p.716)
    volatile t_u16 All;
    struct {
        unsigned RXDMA_EN   :  1; // Bit 0 RXDMA_EN: Rx buffer DMA enable
        unsigned TXDMA_EN   :  1; // Bit 1 TXDMA_EN: Tx buffer DMA enable
        unsigned SSOE      :  1; // Bit 2 SSOE: SS output enable
        unsigned reserved1 :  2; // Bits 4:3 reserved, must be kept at reset value.
        unsigned ERRIE     :  1; // Bit 5 ERRIE: Error interrupt enable
        unsigned RXNEIE    :  1; // Bit 6 RXNEIE: RX buffer not empty interrupt enable
        unsigned TXEIE     :  1; // Bit 7 TXEIE: Tx buffer empty interrupt enable
        unsigned reserved2 :  8; // Bits 15:8 reserved, must be kept at reset value.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_spi_cr2;
typedef union { // t_register_stm32f1xx_spi_sr                 SPI Status Register (->RM0008 p.717)
    volatile t_u16 All;
    struct {
        unsigned RXNE      :  1; // Bit 0 RXNE: Receive buffer not empty
        unsigned TXE       :  1; // Bit 1 TXE: Transmit buffer empty
        unsigned CHSIDE    :  1; // Bit 2 CHSIDE: Channel side
        unsigned UDR       :  1; // Bit 3 UDR: Underrun flag
        unsigned CRCERR    :  1; // Bit 4 CRCERR: CRC error flag
        unsigned MODF      :  1; // Bit 5 MODF: Mode fault
        unsigned OVR       :  1; // Bit 6 OVR: Overrun flag
        unsigned BSY       :  1; // Bit 7 BSY: Busy flag
        unsigned reserved  :  8; // Bits 15:8 reserved, must be kept at reset value.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_spi_sr;
typedef struct { // t_register_stm32f1xx_spi_dr                 SPI Data Register (->RM0008 p.718)
    t_u16 Data;                  // data received or to be transmitted
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_spi_dr;
typedef struct { // t_register_stm32f1xx_spi_crc                SPI CRC Register (->RM0008 p.718)
    unsigned Polynom  : 16;        // polynomial used for CRC calculation
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_spi_crc;
typedef struct { // t_register_stm32f1xx_spi_rx_crc             SPI Computed CRC value of received data (->RM0008 p.719)
    unsigned Value    : 16;        // calculated CRC value (only lower 8 bits valid in 8-bit mode)
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_spi_rx_crc;
typedef struct { // t_register_stm32f1xx_spix_crc_t             SPI Computed CRC value of transmitted data (->RM0008 p.719)
    unsigned Value    : 16;        // calculated CRC value (only lower 8 bits valid in 8-bit mode)
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_spix_crc_t;
typedef union { // t_register_stm32f1xx_spi_i2scfgr            SPI I2S-Configuration Register (->RM0008 p.720)
    volatile t_u16 All;
    struct {
        unsigned CHL_EN      :  1; // Bit 0 CHL_EN: Channel length (number of bits per audio channel)
        unsigned DATL_EN     :  2; // Bit 2:1 DATL_EN: Data length to be transferred
        unsigned CKPOL      :  1; // Bit 3 CKPOL: Steady state clock polarity
        unsigned I2SSTD     :  1; // Bit 5:4 I2SSTD: I2S standard selection
        unsigned reserved1  :  1; // Bit 6 reserved: forced at 0 by hardware
        unsigned PCMSYNC    :  1; // Bit 7 PCMSYNC: PCM frame synchronization
        unsigned I2SCFG     :  2; // Bit 9:8 I2SCFG: I2S configuration mode
        unsigned I2SE       :  1; // Bit 10 I2SE: I2S Enable
        unsigned I2SMOD     :  1; // Bit 11 I2SMOD: I2S mode selection
        unsigned reserved2  : 4; // Bits 15:12 reserved, must be kept at reset value.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_spi_i2scfgr;
typedef union { // t_register_stm32f1xx_spi_i2spr              SPI I2S Prescaler Register (->RM0008 p.721)
    volatile t_u16 All;
    struct {
        unsigned I2SDIV    :  8; // Bit 7:0 I2SDIV: I2S Linear prescaler
        unsigned ODD       :  1; // Bit 8 ODD: Odd factor for the prescaler
        unsigned MCKOE     :  1; // Bit 9 MCKOE: Master clock output enable
        unsigned reserved  :  6; // Bits 15:10 reserved, must be kept at reset value.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_spi_i2spr;
typedef struct { // t_register_stm32f1xx_spi Serial Peripheral Interface
    volatile t_register_stm32f1xx_spi_cr1     CR1;
    volatile t_u16                            reserved0;
    volatile t_register_stm32f1xx_spi_cr2     CR2;
    volatile t_u16                            reserved1;
    volatile t_register_stm32f1xx_spi_sr      SR;
    volatile t_u16                            reserved2;
    volatile t_register_stm32f1xx_spi_dr      DR;
    volatile t_u16                            reserved3;
    volatile t_register_stm32f1xx_spi_crc     SPI_CRCPR;
    volatile t_u16                            reserved4;
    volatile t_register_stm32f1xx_spi_rx_crc  RX_CRC;
    volatile t_u16                            reserved5;
    volatile t_register_stm32f1xx_spix_crc_t  TX_CRC;
    volatile t_u16                            reserved6;
    volatile t_register_stm32f1xx_spi_i2scfgr I2SCFGR;
    volatile t_u16                            reserved7;
    volatile t_register_stm32f1xx_spi_i2spr   I2SPR;
    volatile t_u16                            reserved8;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_spi;
typedef struct { // t_register_stm32f1xx_i2c_cr1                I2C Control Register 1 (->RM0008 p.744)
    unsigned PE            : 1;  // Peripheral enable
    unsigned SMBUS         : 1;  // SMBus mode
    unsigned reserved1     : 1;  // must be kept at reset value
    unsigned SMBTYPE       : 1;  // SMBus type
    unsigned ENARP         : 1;  // ARP enable
    unsigned ENPEC         : 1;  // PEC enable
    unsigned ENGC          : 1;  // General call enable
    unsigned NOSTRETCH     : 1;  // Clock stretching disable (Slave mode)
    unsigned START         : 1;  // Start generation
    unsigned STOP          : 1;  // Stop generation
    unsigned ACK           : 1;  // Acknowledge enable
    unsigned POS           : 1;  // Acknowledge/PEC Position (for data reception)
    unsigned PEC           : 1;  // Packet error checking
    unsigned ALERT         : 1;  // SMBus alert
    unsigned reserved2     : 1;  // must be kept at reset value
    unsigned SWRST         : 1;  // Software reset

} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_i2c_cr1;
typedef struct { // t_register_stm32f1xx_i2c_cr2                I2C Control Register 2 (->RM0008 p.746)
    unsigned FREQ          : 6;  // Peripheral clock frequency
    unsigned reserved1     : 2;  // must be kept at reset value
    unsigned ITERR_EN       : 1;  // Error interrupt enable
    unsigned ITEVT_EN       : 1;  // Event interrupt enable
    unsigned ITBUF_EN       : 1;  // Buffer interrupt enable
    unsigned DMA_EN         : 1;  // DMA requests enable
    unsigned LAST          : 1;  // DMA last transfer
    unsigned reserved2     : 3;  // must be kept at reset value

} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_i2c_cr2;
typedef struct { // t_i2c_oar1_7b            I2C Own Address Register 1 for 7 bit adressing mode (->RM0008 p.748)
    unsigned ADD0          : 1;  // not used
    unsigned ADD           : 7;  // Interface address
    unsigned ADD98         : 2;  // not used
    unsigned reserved1     : 4;  // must be kept at reset value
    unsigned reserved2     : 1;  // must be kept at reset value
    unsigned ADDMODE       : 1;  // Addressing mode (slave mode)

} __attribute__( ( __packed__ ) ) t_i2c_oar1_7b;
typedef struct { // t_i2c_oar1_10b           I2C Own Address Register 1 for 10 bit adressing mode (->RM0008 p.748)
    unsigned ADD           : 10; // Interface address
    unsigned reserved1     : 4;  // must be kept at reset value
    unsigned reserved2     : 1;  // must be kept at reset value
    unsigned ADDMODE       : 1;  // Addressing mode (slave mode)

} __attribute__( ( __packed__ ) ) t_i2c_oar1_10b;
typedef struct { // t_register_stm32f1xx_i2c_oar1               I2C Own Address Register 1 (->RM0008 p.748)
    union {
        t_i2c_oar1_7b  OAR1_7Bit;
        t_i2c_oar1_10b OAR1_10Bit;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_i2c_oar1;
typedef struct { // t_register_stm32f1xx_i2c_oar2               I2C Own Address Register 2 (->RM0008 p.748)
    unsigned ENDUAL        : 1;  // Dual addressing mode enable
    unsigned ADD2          : 7;  // Interface address
    unsigned reserved1     : 8;  // must be kept at reset value
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_i2c_oar2;
typedef struct { // t_register_stm32f1xx_i2c_dr                 I2C Data Register (->RM0008 p.749)
    unsigned DR            : 8;  // 8-bit data register
    unsigned reserved1     : 8;  // must be kept at reset value
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_i2c_dr;
typedef struct { // t_register_stm32f1xx_i2c_sr1                I2C Status Register 1 (->RM0008 p.749)
    unsigned SB            : 1;  // Start bit (Master mode)
    unsigned ADDR          : 1;  // Address sent (master mode)/matched (slave mode)
    unsigned BTF           : 1;  // Byte transfer finished
    unsigned ADD10         : 1;  // 10-bit header sent (Master mode)
    unsigned STOPF         : 1;  // Stop detection (slave mode)
    unsigned reserved1     : 1;  // must be kept at reset value
    unsigned RXNE          : 1;  // Data register not empty (receivers)
    unsigned TxE           : 1;  // Data register empty (transmitters)
    unsigned BERR          : 1;  // Bus error
    unsigned ARLO          : 1;  // Arbitration lost (master mode)
    unsigned AF            : 1;  // Acknowledge failure
    unsigned OVR           : 1;  // Overrun/Underrun
    unsigned PECERR        : 1;  // PEC Error in reception
    unsigned reserved2     : 1;  // must be kept at reset value
    unsigned TIMEOUT       : 1;  // Timeout or Tlow error
    unsigned SMBALERT      : 1;  // SMBus alert
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_i2c_sr1;
typedef struct { // t_register_stm32f1xx_i2c_sr2                I2C Status Register 2 (->RM0008 p.753)
    unsigned MSL           : 1;  // Master/ Slave
    unsigned BUSY          : 1;  // Bus busy
    unsigned TRA           : 1;  // Transmitter/receiver
    unsigned reserved1     : 1;  // must be kept at reset value
    unsigned GENCALL       : 1;  // General call address (Slave mode)
    unsigned SMBDEFAULT    : 1;  // SMBus device default address (Slave mode)
    unsigned SMBHOST       : 1;  // SMBus host header (Slave mode)
    unsigned DUALF         : 1;  // Dual flag (Slave mode)
    unsigned PEC           : 8;  // Packet error checking register
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_i2c_sr2;
typedef struct { // t_register_stm32f1xx_i2c_ccr                I2C Clock Control Register (->RM0008 p.754)
    unsigned CCR           : 12; // Clock control register in Fast/Standard mode (Master mode)
    unsigned reserved1     : 2;  // must be kept at reset value
    unsigned DUTY          : 1;  // Fast mode duty cycle
    unsigned Fast          : 1;  // I2C fast mode selection
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_i2c_ccr;
typedef struct { // t_t_register_stm32f1xx_i2crise              I2C Max Rise Time in Master Mode (->RM0008 p.755)
    unsigned TRISE         : 6;  // Maximum rise time in Fast/Standard mode (Master mode)
    unsigned reserved1     : 10; // must be kept at reset value
} __attribute__( ( __packed__ ) ) t_t_register_stm32f1xx_i2crise;
typedef struct { // t_register_stm32f1xx_i2c Inter-Integrated Circuit Interface

    volatile t_register_stm32f1xx_i2c_cr1    CR1;
    unsigned PadWord1 : 16; // pad to next 32-bit address
    volatile t_register_stm32f1xx_i2c_cr2    CR2;
    unsigned PadWord2 : 16; // pad to next 32-bit address
    volatile t_register_stm32f1xx_i2c_oar1   OAR1;
    unsigned PadWord3 : 16; // pad to next 32-bit address
    volatile t_register_stm32f1xx_i2c_oar2   OAR2;
    unsigned PadWord4 : 16; // pad to next 32-bit address
    volatile t_register_stm32f1xx_i2c_dr     DR;
    unsigned PadWord5 : 16; // pad to next 32-bit address
    volatile t_register_stm32f1xx_i2c_sr1    SR1;
    unsigned PadWord6 : 16; // pad to next 32-bit address
    volatile t_register_stm32f1xx_i2c_sr2    SR2;
    unsigned PadWord7 : 16; // pad to next 32-bit address
    volatile t_register_stm32f1xx_i2c_ccr    CCR;
    unsigned PadWord8 : 16; // pad to next 32-bit address
    volatile t_t_register_stm32f1xx_i2crise  TRISE;
    unsigned PadWord9 : 16; // pad to next 32-bit address
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_i2c;
typedef union { // t_register_stm32f1xx_can_mcr                bxCAN Master Control Register (->RM0008 p.648)
    volatile t_u32 All;
    struct {
        unsigned INRQ       : 1;    // Initialization request
        unsigned SLEEP      : 1;    // Sleep mode request
        unsigned TXFP       : 1;    // Transmit FIFO priority
        unsigned RFLM       : 1;    // Receive FIFO locked mode
        unsigned NART       : 1;    // No automatic retransmission
        unsigned AWUM       : 1;    // Automatic wakeup mode
        unsigned ABOM       : 1;    // Automatic bus-off management
        unsigned TTCM       : 1;    // Time triggered communication mode
        unsigned reserved1  : 7;    // must be kept at reset value
        unsigned RESET      : 1;    // Software master reset
        unsigned DBF        : 1;    // Debug Freeze
        unsigned reserved2  : 15;   // must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_can_mcr;
typedef union { // t_register_stm32f1xx_can_msr                bxCAN Master Status Register (->RM0008 p.650)
    volatile t_u32 All;
    struct {
        unsigned INAK       : 1;    // Initialization acknowledge
        unsigned SLAK       : 1;    // Sleep acknowledge
        unsigned ERRI       : 1;    // Error interrupt
        unsigned WKUI       : 1;    // Wakeup interrupt
        unsigned SLAKI      : 1;    // Sleep acknowledge interrupt
        unsigned reserved1  : 3;    // must be kept at reset value
        unsigned TXM        : 1;    // Transmit mode
        unsigned RXM        : 1;    // Receive mode
        unsigned SAMP       : 1;    // Last sample point
        unsigned RX         : 1;    // Can Rx signal
        unsigned reserved2  : 20;   // must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_can_msr;
typedef union { // t_t_register_stm32f1xx_cansr                bxCAN Transmit Status Register (->RM0008 p.651)
    volatile t_u32 All;
    struct {
        unsigned RQCP0      : 1;    // Request completed mailbox0
        unsigned TXOK0      : 1;    // Transmission OK of mailbox 0
        unsigned ALST0      : 1;    // Arbitration lost for mailbox 0
        unsigned TERR0      : 1;    // Transmission error for mailbox 0
        unsigned reserved1  : 3;    // must be kept at reset value
        unsigned ABRQ0      : 1;    // Abort request for mailbox 0
        unsigned RQCP1      : 1;    // Request completed mailbox1
        unsigned TXOK1      : 1;    // Transmission OK of mailbox 1
        unsigned ALST1      : 1;    // Arbitration lost for mailbox 1
        unsigned TERR1      : 1;    // Transmission error for mailbox 1
        unsigned reserved2  : 3;    // must be kept at reset value
        unsigned ABRQ1      : 1;    // Abort request for mailbox 1
        unsigned RQCP2      : 1;    // Request completed mailbox2
        unsigned TXOK2      : 1;    // Transmission OK of mailbox 2
        unsigned ALST2      : 1;    // Arbitration lost for mailbox 2
        unsigned TERR2      : 1;    // Transmission error for mailbox 2
        unsigned reserved3  : 3;    // must be kept at reset value
        unsigned ABRQ2      : 1;    // Abort request for mailbox 2
        unsigned CODE       : 2;    // Mailbox code
        unsigned TME0       : 1;    // Transmit mailbox 0 empty
        unsigned TME1       : 1;    // Transmit mailbox 1 empty
        unsigned TME2       : 1;    // Transmit mailbox 2 empty
        unsigned LOW0       : 1;    // Lowest priority flag for mailbox 0
        unsigned LOW1       : 1;    // Lowest priority flag for mailbox 1
        unsigned LOW2       : 1;    // Lowest priority flag for mailbox 2
    } Bits;
} __attribute__( ( __packed__ ) ) t_t_register_stm32f1xx_cansr;
typedef union { // t_register_stm32f1xx_can_rf0r                bxCAN Receive FIFO 0 Register (->RM0008 p.653)
    volatile t_u32 All;
    struct {
        unsigned FMP0       : 2;    // FIFO 0 message pending
        unsigned Res        : 1;    // must be kept at reset value
        unsigned FULL0      : 1;    // FIFO 0 full
        unsigned FOVR0      : 1;    // FIFO 0 overrun
        unsigned RFOM0      : 1;    // Release FIFO 0 output mailbox
        unsigned reserved1  : 26;   // must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_can_rf0r;
typedef union { // t_register_stm32f1xx_can_rf1r                bxCAN Receive FIFO 1 Register (->RM0008 p.654)
    volatile t_u32 All;
    struct {
        unsigned FMP1       : 2;    // FIFO 1 message pending
        unsigned reserved1  : 1;    // must be kept at reset value
        unsigned FULL1      : 1;    // FIFO 1 full
        unsigned FOVR1      : 1;    // FIFO 1 overrun
        unsigned RFOM1      : 1;    // Release FIFO 1 output mailbox
        unsigned reserved2  : 26;   // must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_can_rf1r;
typedef union { // t_register_stm32f1xx_can_ier                bxCAN Interrupt Enable Register (->RM0008 p.655)
    volatile t_u32 All;
    struct {
        unsigned TMEIE      : 1;    // Transmit mailbox empty interrupt enable
        unsigned FMPIE0     : 1;    // FIFO 0 message pending interrupt enable
        unsigned FFIE0      : 1;    // FIFO 0 full interrupt enable
        unsigned FOVIE0     : 1;    // FIFO 0 overrun interrupt enable
        unsigned FMPIE1     : 1;    // FIFO 1 message pending interrupt enable
        unsigned FFIE1      : 1;    // FIFO 1 full interrupt enable
        unsigned FOVIE1     : 1;    // FIFO 1 overrun interrupt enable
        unsigned reserved1  : 1;    // must be kept at reset value
        unsigned EWGIE      : 1;    // Error warning interrupt enable
        unsigned EPVIE      : 1;    // Error passive interrupt enable
        unsigned BOFIE      : 1;    // Bus-off interrupt enable
        unsigned LECIE      : 1;    // Last error code interrupt enable
        unsigned reserved2  : 3;    // must be kept at reset value
        unsigned ERRIE      : 1;    // Error interrupt enable
        unsigned WKUIE      : 1;    // Wakeup interrupt enable
        unsigned SLKIE      : 1;    // Sleep interrupt enable
        unsigned reserved3  : 14;   // must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_can_ier;
typedef union { // t_register_stm32f1xx_can_esr                bxCAN Error Status Register (->RM0008 p.656)
    volatile t_u32 All;
    struct {
        unsigned EWGF       : 1;    // Error warning flag
        unsigned EPVF       : 1;    // Error passive flag
        unsigned BOFF       : 1;    // Bus-off flag
        unsigned reserved1  : 1;    // must be kept at reset value
        unsigned LEC        : 3;    // Last error counter
        unsigned reserved2  : 9;    // must be kept at reset value
        unsigned TEC        : 8;    // Least significant byte of the 9-bit transmit error counter
        unsigned REC        : 8;    // Receive error counter
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_can_esr;
typedef union { // t_register_stm32f1xx_can_btr                bxCAN Bit Timing Register (->RM0008 p.657)
    volatile t_u32 All;
    struct {
        unsigned BRP        : 10;   // Baud rate prescaler
        unsigned reserved1  : 6;    // must be kept at reset value
        unsigned TS1        : 4;    // Time segment 1
        unsigned TS2        : 3;    // Time segment 2
        unsigned reserved2  : 1;    // must be kept at reset value
        unsigned SJW        : 2;    // Resynchronization jump width
        unsigned reserved3  : 4;    // must be kept at reset value
        unsigned LBKM       : 1;    // Loop back mode (debug)
        unsigned SILM       : 1;    // Silent mode (debug)
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_can_btr;
typedef union { // t_t_register_stm32f1xx_canixr                bxCAN TX Mailbox identifier register (->RM0008 p.659)
    volatile t_u32 All;
    struct {
        unsigned TXRQ       : 1;     // Transmit mailbox request
        unsigned RTR        : 1;     // Remote transmission request
        unsigned IDE        : 1;     // Identifier Extension
        unsigned EXID       : 18;    // Extended Identifier
        unsigned STID       : 11;    // Standar identifier or extended identifier
    } Bits;
} __attribute__( ( __packed__ ) ) t_t_register_stm32f1xx_canixr;
typedef union { // t_t_register_stm32f1xx_candtxr                bxCAN Mailbox data length control and time stamp register (->RM0008 p.660)
    volatile t_u32 All;
    struct {
        unsigned DLC        : 4;     // Data length code
        unsigned reserved1  : 4;     // must be kept at reset value
        unsigned TGT        : 1;     // Transmit global time
        unsigned reserved2  : 7;     // must be kept at reset value
        unsigned TIME       : 16;    // Message time stamp
    } Bits;
} __attribute__( ( __packed__ ) ) t_t_register_stm32f1xx_candtxr;
typedef struct { // t_register_stm32f1xx_canx_mailbox_t CAN Mailbox register structure
    volatile t_t_register_stm32f1xx_canixr TIR;
    volatile t_t_register_stm32f1xx_candtxr TDTR;
    volatile t_u32 TDLR;
    volatile t_u32 TDHR;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_canx_mailbox_t;
typedef union { // t_register_stm32f1xx_can_rixr                bxCAN Receive FIFO Mailbox identifier register (->RM0008 p.662)
    volatile t_u32 All;
    struct {
        unsigned reserved1  : 1;     // must be kept at reset value
        unsigned RTR        : 1;     // Remote transmission request
        unsigned IDE        : 1;     // Identifier Extension
        unsigned EXID       : 18;    // Extended Identifier
        unsigned STID       : 11;    // Standar identifier or extended identifier
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_can_rixr;
typedef union { // t_register_stm32f1xx_can_rdtxr                bxCAN Receive FIFO Mailbox data length control and time stamp register (->RM0008 p.663)
    volatile t_u32 All;
    struct {
        unsigned DLC        : 4;     // Data length code
        unsigned reserved1  : 4;     // must be kept at reset value
        unsigned FMI        : 8;     // Filter match index
        unsigned TIME       : 16;    // Message time stamp
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_can_rdtxr;
typedef struct { // t_register_stm32f1xx_can_fifo_mailbox CAN FIFO register structure
    volatile t_register_stm32f1xx_can_rixr RIR;
    volatile t_register_stm32f1xx_can_rdtxr RDTR;
    volatile t_u32 RDLR;
    volatile t_u32 RDHR;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_can_fifo_mailbox;
typedef struct { // t_register_stm32f1xx_can_filter CAN Filter register structure
    volatile t_u32 FR1;
    volatile t_u32 FR2;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_can_filter;
typedef struct {
    volatile t_register_stm32f1xx_can_mcr MCR;
    volatile t_register_stm32f1xx_can_msr MSR;
    volatile t_t_register_stm32f1xx_cansr TSR;
    volatile t_register_stm32f1xx_can_rf0r RF0R;
    volatile t_register_stm32f1xx_can_rf1r RF1R;
    volatile t_register_stm32f1xx_can_ier IER;
    volatile t_register_stm32f1xx_can_esr ESR;
    volatile t_register_stm32f1xx_can_btr BTR;
    volatile t_u32  reserved0[88];
    volatile t_register_stm32f1xx_canx_mailbox_t CAN_Tx_Mailbox[3];
    volatile t_register_stm32f1xx_can_fifo_mailbox CAN_FIFO_Mailbox[2];
    volatile t_u32  reserved1[12];
    volatile t_u32 FMR;
    volatile t_u32 FM1R;
    volatile t_u32  reserved2;
    volatile t_u32 FS1R;
    volatile t_u32  reserved3;
    volatile t_u32 FFA1R;
    volatile t_u32  reserved4;
    volatile t_u32 FA1R;
    volatile t_u32  reserved5[8];
#ifndef STM32F10X_CL
    volatile t_register_stm32f1xx_can_filter CAN_Filter[14];
#else
    volatile t_register_stm32f1xx_can_filter CAN_Filter[28];
#endif /* STM32F10X_CL */
}  __attribute__( ( __packed__ ) ) t_register_stm32f1xx_can;
typedef struct {  // USB control register (USB_CNTR) (->RM0008 p.612)
    volatile t_u32 All;
    struct {
        unsigned CTRM         : 1;
        unsigned PMAOVRM      : 1;
        unsigned ERRM         : 1;
        unsigned WKUPM        : 1;
        unsigned SUSPM        : 1;
        unsigned RESETM       : 1;
        unsigned SOFM         : 1;
        unsigned ESOFM        : 1;
        unsigned reserved     : 3;
        unsigned RESUME       : 1;
        unsigned FSUSP        : 1;
        unsigned LP_MODE      : 1;
        unsigned PDWN         : 1;
        unsigned FRES         : 1;
    } Bits;
}  __attribute__( ( __packed__ ) ) t_register_stm32f1xx_usb_cntr;
typedef union {  // USB intertrrupt status register (USB_ISTR) (->RM0008 p.614)
    volatile t_u32 All;
    struct {
        unsigned CTR      : 1 ;
        unsigned PMA_OVR  : 1 ;
        unsigned ERR      : 1 ;
        unsigned WKUPM    : 1 ;
        unsigned SUSP     : 1 ;
        unsigned RESET    : 1 ;
        unsigned SOF      : 1 ;
        unsigned ESOF     : 1 ;
        unsigned reserved : 3 ;
        unsigned DIR      : 1 ;
        unsigned EP_ID    : 4 ;
    } Bits;
}  __attribute__( ( __packed__ ) ) t_register_stm32f1xx_usb_istr;
typedef union {  // USB intertrrupt status register (USB_FNR) (->RM0008 p.617)
    volatile t_u32 All;
    struct {
        unsigned RXDP     : 1 ;
        unsigned RXDM     : 1 ;
        unsigned LCK      : 1 ;
        unsigned LSOF     : 2 ;
        unsigned FN       : 11;
    } Bits;
}  __attribute__( ( __packed__ ) ) t_register_stm32f1xx_usb_fnr;
typedef struct {  // USB status register (->RM0008 p.612 ff.)
    volatile t_register_stm32f1xx_usb_cntr CNTR;
    volatile t_register_stm32f1xx_usb_istr ISTR;
    volatile t_register_stm32f1xx_usb_fnr   FNR;
}  __attribute__( ( __packed__ ) ) t_register_stm32f1xx_usb;
typedef union { // t_s_register_stm32f1xx_adcr                ADC Status Register (->RM0008 p.228)
    volatile t_u32 All;
    struct {
        unsigned AWD        : 1;     // Analog Watchdog Flag
        unsigned EOC        : 1;     // End of conversion
        unsigned JEOC       : 1;     // Injected channel end of conversion
        unsigned JSTRT      : 1;     // Injected channel Start flag
        unsigned STRT       : 1;     // Regular channel Start flag
        unsigned reserved   : 27;
    } Bits;
} __attribute__( ( __packed__ ) ) t_s_register_stm32f1xx_adcr;

// use bit numbers for atomic single bit access via cm3_set_peripheral_bit()
// Caution: Using bit numbers cannot be safe checked by compiler!
#define BITNUMBER_ADC_SR_AWD                 0 // Analog Watchdog Flag
#define BITNUMBER_ADC_SR_EOC                 1 // End of conversion
#define BITNUMBER_ADC_SR_JEOC                2 // Injected channel end of conversion
#define BITNUMBER_ADC_SR_JSTRT               3 // Injected channel Start flag
#define BITNUMBER_ADC_SR_STRT                4 // Regular channel Start flag

typedef union { // t_register_stm32f1xx_adc_cr1                ADC Control Register 1 (->RM0008 p.229)
    volatile t_u32 All;
    struct {
        unsigned AWDCH      : 5;     //  0  Analog Watchdog Flag
        unsigned EOCIE      : 1;     //  5  Interrupt enable for EOC
        unsigned AWDIE      : 1;     //  6  Analog watchdog interrupt enable
        unsigned JEOCIE     : 1;     //  7  Interrupt enable for injected channels
        unsigned SCAN       : 1;     //  8  Scan mode
        unsigned AWDSGL     : 1;     //  9  Enable the watchdog on a single channel in scan mode
        unsigned JAUTO      : 1;     // 10  Automatic Injected Group conversion
        unsigned DISC_EN     : 1;     // 11  Discontinuous mode on regular channels
        unsigned JDISC_EN    : 1;     // 12  Discontinuous mode on injected channels
        unsigned DISCNUM    : 3;     // 13  Discontinuous mode channel count
        unsigned DUALMOD    : 4;     // 16  Dual mode selection
        unsigned reserved1  : 2;     // 20
        unsigned JAWD_EN     : 1;     // 22  Analog watchdog enable on injected channels
        unsigned AWD_EN      : 1;     // 23  Analog watchdog enable on regular channels
        unsigned reserved2  : 8;     // 24
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc_cr1;

// use bit numbers for atomic single bit access via cm3_set_peripheral_bit()
// Caution: Using bit numbers cannot be safe checked by compiler!
#define BITNUMBER_ADC_CR1_AWDCH      0  // Analog Watchdog Flag
#define BITNUMBER_ADC_CR1_EOCIE      5  // Interrupt enable for EOC
#define BITNUMBER_ADC_CR1_AWDIE      6  // Analog watchdog interrupt enable
#define BITNUMBER_ADC_CR1_JEOCIE     7  // Interrupt enable for injected channels
#define BITNUMBER_ADC_CR1_SCAN       8  // Scan mode
#define BITNUMBER_ADC_CR1_AWDSGL     9  // Enable the watchdog on a single channel in scan mode
#define BITNUMBER_ADC_CR1_JAUTO     10  // Automatic Injected Group conversion
#define BITNUMBER_ADC_CR1_DISC_EN   11  // Discontinuous mode on regular channels
#define BITNUMBER_ADC_CR1_JDISC_EN  12  // Discontinuous mode on injected channels
#define BITNUMBER_ADC_CR1_DISCNUM   13  // Discontinuous mode channel count
#define BITNUMBER_ADC_CR1_DUALMOD   16  // Dual mode selection
#define BITNUMBER_ADC_CR1_JAWD_EN   22  // Analog watchdog enable on injected channels
#define BITNUMBER_ADC_CR1_AWD_EN    23  // Analog watchdog enable on regular channels

typedef union { // t_register_stm32f1xx_adc_cr2                ADC Control Register 2 (->RM0008 p.231)
    volatile t_u32 All;
    struct {
        unsigned ADON       : 1;     //  0  A/D converter ON/OFF
        unsigned CONT       : 1;     //  1  Continuous conversion
        unsigned CAL        : 1;     //  2  A/D Calibration
        unsigned RSTCAL     : 1;     //  3  Reset calibration
        unsigned reserved1  : 4;     //  4
        unsigned DMA        : 1;     //  8  Diect memory access mode
        unsigned reserved2  : 2;     //  9
        unsigned ALIGN      : 1;     // 11  Data alignment (=0: right aligned, =1: left aligned) -> RM0008 p.215
        unsigned JEXTSEL    : 3;     // 12  External event select for injected group
        unsigned JEXTTRIG   : 1;     // 15  External trigger conversion mode for injectd channels
        unsigned reserved3  : 1;     // 16
        unsigned EXTSEL     : 3;     // 17  External event select for regular group
        unsigned EXTTRIG    : 1;     // 20  External trigger conversion mode for regular channels
        unsigned JSWSTART   : 1;     // 21  Start conversion of injected channels
        unsigned SWSTART    : 1;     // 22  Start conversion of regular channels
        unsigned TSVREFE    : 1;     // 23  Temperature sensor an Vrefint enable
        unsigned reserved4  : 8;     // 24
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc_cr2;

// use bit numbers for atomic single bit access via cm3_set_peripheral_bit()
// Caution: Using bit numbers cannot be safe checked by compiler!
#define BITNUMBER_ADC_CR2_ADON       0  // A/D converter ON/OFF
#define BITNUMBER_ADC_CR2_CONT       1  // Continuous conversion
#define BITNUMBER_ADC_CR2_CAL        2  // A/D Calibration
#define BITNUMBER_ADC_CR2_RSTCAL     3  // Reset calibration
#define BITNUMBER_ADC_CR2_DMA        8  // Diect memory access mode
#define BITNUMBER_ADC_CR2_ALIGN     11  // Data alignment (=0: right aligned, =1: left aligned) -> RM0008 p.215
#define BITNUMBER_ADC_CR2_JEXTSEL   12  // External event select for injected group
#define BITNUMBER_ADC_CR2_JEXTTRIG  15  // External trigger conversion mode for injectd channels
#define BITNUMBER_ADC_CR2_EXTSEL    17  // External event select for regular group
#define BITNUMBER_ADC_CR2_EXTTRIG   20  // External trigger conversion mode for regular channels
#define BITNUMBER_ADC_CR2_JSWSTART  21  // Start conversion of injected channels
#define BITNUMBER_ADC_CR2_SWSTART   22  // Start conversion of regular channels
#define BITNUMBER_ADC_CR2_TSVREFE   23  // Temperature sensor an Vrefint enable

typedef union { // t_s_register_stm32f1xx_adcmpr1                ADC sample time register 1 (->RM0008 p.235)
    volatile t_u32 All;
    struct {
        unsigned SMP10      : 3;     // Channel 10 Sample time selection
        unsigned SMP11      : 3;     // Channel 11 Sample time selection
        unsigned SMP12      : 3;     // Channel 12 Sample time selection
        unsigned SMP13      : 3;     // Channel 13 Sample time selection
        unsigned SMP14      : 3;     // Channel 14 Sample time selection
        unsigned SMP15      : 3;     // Channel 15 Sample time selection
        unsigned SMP16      : 3;     // Channel 16 Sample time selection
        unsigned SMP17      : 3;     // Channel 17 Sample time selection
        unsigned reserved   : 8;

    } Bits;
} __attribute__( ( __packed__ ) ) t_s_register_stm32f1xx_adcmpr1;
typedef union { // t_s_register_stm32f1xx_adcmpr2                ADC sample time register 2 (->RM0008 p.236)
    volatile t_u32 All;
    struct {
        unsigned SMP0      : 3;     // Channel 0 Sample time selection
        unsigned SMP1      : 3;     // Channel 1 Sample time selection
        unsigned SMP2      : 3;     // Channel 2 Sample time selection
        unsigned SMP3      : 3;     // Channel 3 Sample time selection
        unsigned SMP4      : 3;     // Channel 4 Sample time selection
        unsigned SMP5      : 3;     // Channel 5 Sample time selection
        unsigned SMP6      : 3;     // Channel 6 Sample time selection
        unsigned SMP7      : 3;     // Channel 7 Sample time selection
        unsigned SMP8      : 3;     // Channel 8 Sample time selection
        unsigned SMP9      : 3;     // Channel 9 Sample time selection
        unsigned reserved  : 2;

    } Bits;
} __attribute__( ( __packed__ ) ) t_s_register_stm32f1xx_adcmpr2;
typedef union { // t_register_stm32f1xx_adc_jofr1                ADC injected channel data offset register x (->RM0008 p.236)
    volatile t_u32 All;
    struct {
        unsigned JOFFSET1  : 12;    // Data offset for injected channel 1
        unsigned reserved  : 20;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc_jofr1;
typedef union { // t_register_stm32f1xx_adc_jofr2                ADC injected channel data offset register x (->RM0008 p.236)
    volatile t_u32 All;
    struct {
        unsigned JOFFSET2  : 12;    // Data offset for injected channel 2
        unsigned reserved  : 20;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc_jofr2;
typedef union { // t_register_stm32f1xx_adc_jofr3                ADC injected channel data offset register x (->RM0008 p.236)
    volatile t_u32 All;
    struct {
        unsigned JOFFSET3  : 12;    // Data offset for injected channel 3
        unsigned reserved  : 20;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc_jofr3;
typedef union { // t_register_stm32f1xx_adc_jofr4                ADC injected channel data offset register x (->RM0008 p.236)
    volatile t_u32 All;
    struct {
        unsigned JOFFSET4  : 12;    // Data offset for injected channel 4
        unsigned reserved  : 20;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc_jofr4;
typedef union { // t_register_stm32f1xx_adc_htr                ADC watchdog high threshold register (->RM0008 p.237)
    volatile t_u32 All;
    struct {
        unsigned HT        : 12;     // Channel 0 Sample time selection
        unsigned reserved  : 20;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc_htr;
typedef union { // t_register_stm32f1xx_adc_ltr                ADC watchdog low threshold register (->RM0008 p.237)
    volatile t_u32 All;
    struct {
        unsigned LT        : 12;     // Channel 0 Sample time selection
        unsigned reserved  : 20;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc_ltr;
typedef union { // t_s_register_stm32f1xx_adcqr1                ADC regular sequence register 1 (->RM0008 p.238)
    volatile t_u32 All;
    struct {
        unsigned SQ13      : 5;     // 13th conversion in regular sequence
        unsigned SQ14      : 5;     // 14th conversion in regular sequence
        unsigned SQ15      : 5;     // 15th conversion in regular sequence
        unsigned SQ16      : 5;     // 16th conversion in regular sequence
        unsigned L         : 4;     // Regular channel sequence length
        unsigned reserved  : 8;

    } Bits;
} __attribute__( ( __packed__ ) ) t_s_register_stm32f1xx_adcqr1;
typedef union { // t_s_register_stm32f1xx_adcqr2                ADC regular sequence register 2 (->RM0008 p.239)
    volatile t_u32 All;
    struct {
        unsigned SQ7       : 5;     // 7th conversion in regular sequence
        unsigned SQ8       : 5;     // 8th conversion in regular sequence
        unsigned SQ9       : 5;     // 9th conversion in regular sequence
        unsigned SQ10      : 5;     // 10th conversion in regular sequence
        unsigned SQ11      : 5;     // 11th conversion in regular sequence
        unsigned SQ12      : 5;     // 12th conversion in regular sequence
        unsigned reserved  : 2;

    } Bits;
} __attribute__( ( __packed__ ) ) t_s_register_stm32f1xx_adcqr2;
typedef union { // t_s_register_stm32f1xx_adcqr3                ADC regular sequence register 3 (->RM0008 p.240)
    volatile t_u32 All;
    struct {
        unsigned SQ1       : 5;     // 1st conversion in regular sequence
        unsigned SQ2       : 5;     // 2nd conversion in regular sequence
        unsigned SQ3       : 5;     // 3rd conversion in regular sequence
        unsigned SQ4       : 5;     // 4th conversion in regular sequence
        unsigned SQ5       : 5;     // 5th conversion in regular sequence
        unsigned SQ6       : 5;     // 6th conversion in regular sequence
        unsigned reserved  : 2;

    } Bits;
} __attribute__( ( __packed__ ) ) t_s_register_stm32f1xx_adcqr3;
typedef union { // t_register_stm32f1xx_adc_jsqr                ADC injected sequence register (->RM0008 p.241)
    volatile t_u32 All;
    struct {
        unsigned JSQ1       : 5;     // 1st conversion in injected sequence
        unsigned JSQ2       : 5;     // 2nd conversion in injected sequence
        unsigned JSQ3       : 5;     // 3rd conversion in injected sequence
        unsigned JSQ4       : 5;     // 4th conversion in injected sequence
        unsigned JL         : 2;     // Injected sequence length
        unsigned reserved   : 10;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc_jsqr;
typedef union { // t_register_stm32f1xx_adc_jdr1                ADC injected data register 1 (->RM0008 p.242)
    volatile t_u32 All;
    struct {
        unsigned JDATA      : 16;     // Injected data
        unsigned reserved   : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc_jdr1;
typedef union { // t_register_stm32f1xx_adc_jdr2                ADC injected data register 2 (->RM0008 p.242)
    volatile t_u32 All;
    struct {
        unsigned JDATA      : 16;     // Injected data
        unsigned reserved   : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc_jdr2;
typedef union { // t_register_stm32f1xx_adc_jdr3                ADC injected data register 3 (->RM0008 p.242)
    volatile t_u32 All;
    struct {
        unsigned JDATA      : 16;     // Injected data
        unsigned reserved   : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc_jdr3;
typedef union { // t_register_stm32f1xx_adc_jdr4                ADC injected data register 4 (->RM0008 p.242)
    volatile t_u32 All;
    struct {
        unsigned JDATA      : 16;     // Injected data
        unsigned reserved   : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc_jdr4;
typedef union { // t_register_stm32f1xx_adc_dr                ADC regular data register (->RM0008 p.242)
    volatile t_u32 All;
    struct {
        unsigned DATA       : 16;     // ADC2 data
        unsigned ADC2DATA   : 16;     // ADC2 data
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc_dr;
typedef struct s_register_stm32f1xx_adc {
    volatile t_s_register_stm32f1xx_adcr SR;
    volatile t_register_stm32f1xx_adc_cr1 CR1;
    volatile t_register_stm32f1xx_adc_cr2 CR2;
    volatile t_s_register_stm32f1xx_adcmpr1 SMPR1;
    volatile t_s_register_stm32f1xx_adcmpr2 SMPR2;
    volatile t_register_stm32f1xx_adc_jofr1 JOFR1;
    volatile t_register_stm32f1xx_adc_jofr2 JOFR2;
    volatile t_register_stm32f1xx_adc_jofr3 JOFR3;
    volatile t_register_stm32f1xx_adc_jofr4 JOFR4;
    volatile t_register_stm32f1xx_adc_htr HTR;
    volatile t_register_stm32f1xx_adc_ltr LTR;
    volatile t_s_register_stm32f1xx_adcqr1 SQR1;
    volatile t_s_register_stm32f1xx_adcqr2 SQR2;
    volatile t_s_register_stm32f1xx_adcqr3 SQR3;
    volatile t_register_stm32f1xx_adc_jsqr JSQR;
    volatile t_register_stm32f1xx_adc_jdr1 JDR1;
    volatile t_register_stm32f1xx_adc_jdr2 JDR2;
    volatile t_register_stm32f1xx_adc_jdr3 JDR3;
    volatile t_register_stm32f1xx_adc_jdr4 JDR4;
    volatile t_register_stm32f1xx_adc_dr DR;
}  __attribute__( ( __packed__ ) ) t_register_stm32f1xx_adc;


#define register_stm32f1xx_all_t void

// t_ttc_register_architecture is required by ttc_register_types.h
#define t_ttc_register_architecture volatile register_stm32f1xx_all_t

//}

#endif //REGISTER_STM32F1XX_TYPES_H
