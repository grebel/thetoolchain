#ifndef REGISTER_STM32F1XX_H
#define REGISTER_STM32F1XX_H

/** { register_stm32f1xx.h ***********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for register devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by high-level register and application.
 *
 *  Created from template device_architecture.h revision 21 at 20140223 10:54:39 UTC
 *
 *  Note: See ttc_register.h for description of stm32f1xx independent REGISTER implementation.
 *
 *  Authors: Greg Knoll, Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_REGISTER_STM32F1XX
//
// Implementation status of low-level driver support for register devices on stm32f1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_REGISTER_STM32F1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_REGISTER_STM32F1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_REGISTER_STM32F1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_REGISTER_STM32F1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "stm32f10x.h" // required to be included before register_cortexm3.h (defines IRQn_Type)
#include "../register/register_cortexm3.h"
#include "register_stm32f1xx_types.h"
#include "../ttc_register_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_register_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_<device>_prepare
//
#define ttc_driver_register_get_configuration() register_stm32f1xx_get_configuration()
#define ttc_driver_register_prepare() register_stm32f1xx_prepare()
#define ttc_driver_register_reset(Register) register_stm32f1xx_reset(Register)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
// Register Variable Declarations ***************************************{

extern volatile t_register_stm32f1xx_rcc           register_stm32f1xx_RCC;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER1;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER2;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER3;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER4;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER5;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER6;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER7;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER8;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER9;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER10;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER11;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER12;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER13;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER14;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER15;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER16;
extern volatile t_register_stm32f1xx_timer         register_stm32f1xx_TIMER17;
extern volatile t_register_stm32f1xx_usart         register_stm32f1xx_USART1;
extern volatile t_register_stm32f1xx_usart         register_stm32f1xx_USART2;
extern volatile t_register_stm32f1xx_usart         register_stm32f1xx_USART3;
extern volatile t_register_stm32f1xx_usart         register_stm32f1xx_UART4;
extern volatile t_register_stm32f1xx_usart         register_stm32f1xx_UART5;
extern volatile t_register_stm32f1xx_spi           register_stm32f1xx_SPI1;
extern volatile t_register_stm32f1xx_spi           register_stm32f1xx_SPI2;
extern volatile t_register_stm32f1xx_spi           register_stm32f1xx_SPI3;
extern volatile t_register_stm32f1xx_i2c           register_stm32f1xx_I2C1;
extern volatile t_register_stm32f1xx_i2c           register_stm32f1xx_I2C2;
extern volatile t_register_stm32f1xx_can           register_stm32f1xx_CAN1;
extern volatile t_register_stm32f1xx_can           register_stm32f1xx_CAN2;
extern volatile t_register_stm32f1xx_adc           register_stm32f1xx_ADC1;
extern volatile t_register_stm32f1xx_adc           register_stm32f1xx_ADC2;
extern volatile t_register_stm32f1xx_adc           register_stm32f1xx_ADC3;
extern volatile t_register_stm32f1xx_gpio          register_stm32f1xx_GPIOA;
extern volatile t_register_stm32f1xx_gpio          register_stm32f1xx_GPIOB;
extern volatile t_register_stm32f1xx_gpio          register_stm32f1xx_GPIOC;
extern volatile t_register_stm32f1xx_gpio          register_stm32f1xx_GPIOD;
extern volatile t_register_stm32f1xx_gpio          register_stm32f1xx_GPIOE;
extern volatile t_register_stm32f1xx_gpio          register_stm32f1xx_GPIOF;
extern volatile t_register_stm32f1xx_gpio          register_stm32f1xx_GPIOG;
extern volatile t_register_stm32f1xx_afio          register_stm32f1xx_AFIO;
extern volatile t_register_stm32f1xx_exti          register_stm32f1xx_EXTI;
extern volatile DBGMCU_TypeDef                     register_stm32f1xx_DGBMCU;

//}
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_register.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_register.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

/** returns current register configuration
 * @param Config        = pointer to struct t_ttc_register_config
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
volatile register_stm32f1xx_all_t* register_stm32f1xx_get_configuration();

/** Prepares register driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void register_stm32f1xx_prepare();

/** resets given register to default value
 *
 * @param Register    pointer to one entry of struct t_ttc_register_config as returned by ttc_register_get_configuration()
 * @return            ==0: register has been reset successfully
 */
e_ttc_register_errorcode register_stm32f1xx_reset( void* Register );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _register_stm32f1xx_foo(t_ttc_register_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //REGISTER_STM32F1XX_H
