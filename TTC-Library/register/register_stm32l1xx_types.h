#ifndef REGISTER_STM32L1XX_TYPES_H
#define REGISTER_STM32L1XX_TYPES_H

/** { register_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for REGISTER devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_register_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140423 11:05:10 UTC
 *
 *  Note: See ttc_register.h for description of architecture independent REGISTER implementation.
 *
 *  Authors: Greg Knoll 2013/14.
 *
}*/

// { Structures/ Enums required by ttc_register_types.h *************************
// { Includes *******************************************************************
#include "../ttc_basic_types.h"
// } Includes ___________________________________________________________________
// InsertIncludes above (DO NOT REMOVE THIS LINE!)
// { Defines/ TypeDefs **********************************************************
// } Defines  ___________________________________________________________________
// InsertDefines above (DO NOT REMOVE THIS LINE!)
// InsertTypeDefs above (DO NOT REMOVE THIS LINE!)
// { ENUMS****************************************************************
typedef enum {   // e_stm32l1_gpio_mode -  pin modes
    gpio_input,                    // 00
    gpio_general_purpose_output,   // 01
    gpc_alternate_function,        // 10
    gpc_analog                     // 11
} e_stm32l1_gpio_mode;

typedef enum {
    gpio_otype_push_pull = 0x00,
    gpio_otype_open_drain = 0x01
} e_stm32l1_gpio_otype;

typedef enum {   // e_stm32l1_gpio_speed - speeds of pins
    gpio_speed_400kHz,       // 00
    gpio_speed_2MHz,         // 01
    gpio_speed_10MHz,        // 10
    gpio_speed_40MHz         // 11
} e_stm32l1_gpio_speed;

typedef enum {   // e_stm32l1_gpio_pupd - pull-up/pull-down configuration
    gpio_noPULL,          // 00
    gpio_pull_up,         // 01
    gpio_pull_down        // 10
} e_stm32l1_gpio_pupd;

typedef enum {   // e_stm32l1_gpio_af - alternate functions
    gpio_AF0 = 0x0,          // 00
    gpio_AF1  = 0x1,         // 01
    gpio_AF2  = 0x2,        // 10
    gpio_AF3  = 0x3,
    gpio_AF4  = 0x4,
    gpio_AF5  = 0x5,
    gpio_AF6  = 0x6,
    gpio_AF7  = 0x7,
    gpio_AF8  = 0x8,
    gpio_AF9  = 0x9,
    gpio_AF10  = 0xA,
    gpio_AF11  = 0xB,
    gpio_AF12  = 0xC,
    gpio_AF13  = 0xD,
    gpio_AF14  = 0xE,
    gpio_AF15  = 0xF         // 1111
} e_stm32l1_gpio_af;
// } ENUMS_________________________________________________________________
// InsertEnums above (DO NOT REMOVE THIS LINE!)

// { REGISTER STRUCTS********************************************************
typedef struct { // reserved - for whole 32-bit spaces in reg maps
    unsigned reserved   : 32; // whole 32-bit register is reserved
} __attribute__( ( __packed__ ) ) stm32l1_t_reserved_reg;
typedef union { // GPIOx_MODER - GPIO port mode register  (p.142)
    volatile t_u32 All;
    struct {
        unsigned MODER0   : 2; // Port configuration for one pin - see e_stm32l1_gpio_mode
        unsigned MODER1   : 2; // Port configuration for one pin.
        unsigned MODER2   : 2; // Port configuration for one pin.
        unsigned MODER3   : 2; // Port configuration for one pin.
        unsigned MODER4   : 2; // Port configuration for one pin.
        unsigned MODER5   : 2; // Port configuration for one pin.
        unsigned MODER6   : 2; // Port configuration for one pin.
        unsigned MODER7   : 2; // Port configuration for one pin.
        unsigned MODER8   : 2; // Port configuration for one pin.
        unsigned MODER9   : 2; // Port configuration for one pin.
        unsigned MODER10  : 2; // Port configuration for one pin.
        unsigned MODER11  : 2; // Port configuration for one pin.
        unsigned MODER12  : 2; // Port configuration for one pin.
        unsigned MODER13  : 2; // Port configuration for one pin.
        unsigned MODER14  : 2; // Port configuration for one pin.
        unsigned MODER15  : 2; // Port configuration for one pin.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_gpiox_moder;
typedef union { // GPIOx_OTYPER - GPIO port output type register  (p.143)
    volatile t_u32 All;
    struct {
        unsigned OT0   : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT1   : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT2   : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT3   : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT4   : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT5   : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT6   : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT7   : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT8   : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT9   : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT10  : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT11  : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT12  : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT13  : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT14  : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned OT15  : 1; // Output type: 0 = push-pull (reset), 1=open-drain.
        unsigned reserved1: 16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_gpiox_otyper;
typedef union { // GPIOx_OSPEEDR - GPIO output speed Register  (p.143)
    volatile t_u32 All;
    struct {
        unsigned OSPEEDR0   : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR1   : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR2   : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR3   : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR4   : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR5   : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR6   : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR7   : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR8   : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR9   : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR10  : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR11  : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR12  : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR13  : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR14  : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
        unsigned OSPEEDR15  : 2; // Speed of port pin - see e_stm32l1_gpio_speed.
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_gpiox_ospeedr;
typedef union { // GPIOx_PUPDR - GPIO port pull-up/pull-down Register  (p.144)
    volatile t_u32 All;
    struct {
        unsigned PUPDR0   : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR1   : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR2   : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR3   : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR4   : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR5   : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR6   : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR7   : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR8   : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR9   : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR10  : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR11  : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR12  : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR13  : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR14  : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
        unsigned PUPDR15  : 2; // pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_gpiox_pupdr;
typedef union { // GPIOx_IDR - GPIO port input data Register  (p.144)
    volatile t_u32 All;
    struct {
        unsigned IDR0   : 1; // Input value of I/O (read-only).
        unsigned IDR1   : 1; // Input value of I/O (read-only).
        unsigned IDR2   : 1; // Input value of I/O (read-only).
        unsigned IDR3   : 1; // Input value of I/O (read-only).
        unsigned IDR4   : 1; // Input value of I/O (read-only).
        unsigned IDR5   : 1; // Input value of I/O (read-only).
        unsigned IDR6   : 1; // Input value of I/O (read-only).
        unsigned IDR7   : 1; // Input value of I/O (read-only).
        unsigned IDR8   : 1; // Input value of I/O (read-only).
        unsigned IDR9   : 1; // Input value of I/O (read-only).
        unsigned IDR10  : 1; // Input value of I/O (read-only).
        unsigned IDR11  : 1; // Input value of I/O (read-only).
        unsigned IDR12  : 1; // Input value of I/O (read-only).
        unsigned IDR13  : 1; // Input value of I/O (read-only).
        unsigned IDR14  : 1; // Input value of I/O (read-only).
        unsigned IDR15  : 1; // Input value of I/O (read-only).
        unsigned reserved1: 16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_gpiox_idr;
typedef union { // GPIOx_ODR - output data Register  (p.145)
    volatile t_u32 All;
    struct {
        unsigned ODR0   : 1; // Output value of port (read and write).
        unsigned ODR1   : 1; // Output value of port (read and write).
        unsigned ODR2   : 1; // Output value of port (read and write).
        unsigned ODR3   : 1; // Output value of port (read and write).
        unsigned ODR4   : 1; // Output value of port (read and write).
        unsigned ODR5   : 1; // Output value of port (read and write).
        unsigned ODR6   : 1; // Output value of port (read and write).
        unsigned ODR7   : 1; // Output value of port (read and write).
        unsigned ODR8   : 1; // Output value of port (read and write).
        unsigned ODR9   : 1; // Output value of port (read and write).
        unsigned ODR10  : 1; // Output value of port (read and write).
        unsigned ODR11  : 1; // Output value of port (read and write).
        unsigned ODR12  : 1; // Output value of port (read and write).
        unsigned ODR13  : 1; // Output value of port (read and write).
        unsigned ODR14  : 1; // Output value of port (read and write).
        unsigned ODR15  : 1; // Output value of port (read and write).
        unsigned reserved1: 16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_gpiox_odr;
typedef union { // GPIOx_BSRR - bit set/reset Register  (p.145)
    volatile t_u32 All;
    struct {
        unsigned BR0   : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR1   : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR2   : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR3   : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR4   : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR5   : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR6   : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR7   : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR8   : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR9   : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR10  : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR11  : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR12  : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR13  : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR14  : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BR15  : 1; // Setting this bit resets corresponding ODR bit (write-only).
        unsigned BS0   : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS1   : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS2   : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS3   : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS4   : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS5   : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS6   : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS7   : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS8   : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS9   : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS10  : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS11  : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS12  : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS13  : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS14  : 1; // Setting this bit sets corresponding ODR bit (write-only).
        unsigned BS15  : 1; // Setting this bit sets corresponding ODR bit (write-only).
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_gpiox_bsrr;
typedef union { // GPIOx_LCKR - configuration lock Register  (p.145-146)
    volatile t_u32 All;
    struct {
        unsigned LCK0   : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK1   : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK2   : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK3   : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK4   : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK5   : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK6   : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK7   : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK8   : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK9   : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK10  : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK11  : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK12  : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK13  : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK14  : 1; // 1: Port config locked. 0: not locked.
        unsigned LCK15  : 1; // 1: Port config locked. 0: not locked.
        unsigned LCKK   : 1; // Special bit. See p. 146.
        unsigned reserved1: 15;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_gpiox_lckr;
typedef union { // GPIOx_AFRL - alternate function low Register  (p.147)
    volatile t_u32 All;
    struct {
        unsigned AFRL0   : 4; // Alternate function selection - see e_stm32l1_gpio_af.
        unsigned AFRL1   : 4; // Alternate function selection - see e_stm32l1_gpio_af.
        unsigned AFRL2   : 4; // Alternate function selection - see e_stm32l1_gpio_af.
        unsigned AFRL3   : 4; // Alternate function selection - see e_stm32l1_gpio_af.
        unsigned AFRL4   : 4; // Alternate function selection - see e_stm32l1_gpio_af.
        unsigned AFRL5   : 4; // Alternate function selection - see e_stm32l1_gpio_af.
        unsigned AFRL6   : 4; // Alternate function selection - see e_stm32l1_gpio_af.
        unsigned AFRL7   : 4; // Alternate function selection - see e_stm32l1_gpio_af.
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_gpiox_afrl;
typedef union { // GPIOx_AFRH - alternate function high Register  (p.147)
    volatile t_u32 All;
    struct {
        unsigned AFRL8   : 4; // Alternate function selection - see e_stm32l1_gpio_af.
        unsigned AFRL9   : 4; // Alternate function selection - see e_stm32l1_gpio_af.
        unsigned AFRL10  : 4; // Alternate function selection - see e_stm32l1_gpio_af.
        unsigned AFRL11  : 4; // Alternate function selection - see e_stm32l1_gpio_af.
        unsigned AFRL12  : 4; // Alternate function selection - see e_stm32l1_gpio_af.
        unsigned AFRL13  : 4; // Alternate function selection - see e_stm32l1_gpio_af.
        unsigned AFRL14  : 4; // Alternate function selection - see e_stm32l1_gpio_af.
        unsigned AFRL15  : 4; // Alternate function selection - see e_stm32l1_gpio_af.
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_gpiox_afrh;
typedef struct { // All registers for GPIO
    t_register_stm32l1xx_gpiox_moder   MODER;  // Mode of pin (see e_stm32l1_gpio_mode)
    t_register_stm32l1xx_gpiox_otyper  TYPE;  // Type of output (push-pull or open-drain)
    t_register_stm32l1xx_gpiox_ospeedr SPEED; // Speed of output (see  gpio_speed_e)
    t_register_stm32l1xx_gpiox_pupdr   PUPD;  // Pull up/down config (see  gpio_pupd_e)
    t_register_stm32l1xx_gpiox_idr     IDR;   // Input data register
    t_register_stm32l1xx_gpiox_odr     ODR;   // Output data register
    t_register_stm32l1xx_gpiox_bsrr    BSRR;  // Bit set/reset register
    t_register_stm32l1xx_gpiox_lckr    LCKR;  // Configuration lock register
    t_register_stm32l1xx_gpiox_afrl    AFRL;  // Alternate Function (see e_stm32l1_gpio_af)
    t_register_stm32l1xx_gpiox_afrh    AFRH;  // Alternate Function (see e_stm32l1_gpio_af)
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_gpio;
typedef struct { // All registers for GPIO as plain 32 bit values
    volatile t_u32 MODER; // Mode of pin (see e_stm32l1_gpio_mode)
    volatile t_u32 TYPE;  // Type of output (push-pull or open-drain)
    volatile t_u32 SPEED; // Speed of output (see  gpio_speed_e)
    volatile t_u32 PUPD;  // Pull up/down config (see  gpio_pupd_e)
    volatile t_u32 IDR;   // Input data register
    volatile t_u32 ODR;   // Output data register
    volatile t_u32 BSRR;  // Bit set/reset register
    volatile t_u32 LCKR;  // Configuration lock register
    volatile t_u32 AFRL;  // Alternate Function (see e_stm32l1_gpio_af)
    volatile t_u32 AFRH;  // Alternate Function (see e_stm32l1_gpio_af)
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_gpio_32;

typedef union { // stm32l1_t_stm32l1_usart_sr     USART Status Register  (p.690)
    volatile t_u32 All;
    struct {
        unsigned PE   : 1; // Parity Error
        unsigned FE   : 1; // Framing Error
        unsigned NF   : 1; // Noise Detection Flag
        unsigned ORE  : 1; // Overrun Error
        unsigned IDLE : 1; // IDLE line detected
        unsigned RXNE : 1; // Read Data Register not empty
        unsigned TC   : 1; // Transmission complete
        unsigned TXE  : 1; // Transmission Data Register empty
        unsigned LBD  : 1; // Lin break detection flag
        unsigned CTS  : 1; // CTS Flag
        unsigned reserved1: 22;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_usart_sr;
typedef union { // stm32l1_t_stm32l1_usart_dr     USART Data Register
    volatile t_u32 All;
    struct {
        unsigned DR: 9;
        unsigned reserved1: 23;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_usart_dr;
typedef union { // stm32l1_t_stm32l1_usart_brr    USART Baud Rate Register
    volatile t_u32 All;
    struct {
        unsigned DIV_Fraction: 4;
        unsigned DIV_Mantissa: 12;
        unsigned reserved1: 16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_usart_brr;
typedef union { // t_stm32l1_usart_cr1    USART Control Register 1 (p. 694)
    volatile t_u32 All;
    struct {
        unsigned SBK: 1;    // Send break
        unsigned RWU: 1;    // Receiver wakeup
        unsigned RE: 1;     // Receiver enable
        unsigned TE: 1;     // Transmitter enable
        unsigned IDLEIE: 1; // IDLE interrupt enable
        unsigned RXNEIE: 1; // RXNE interrupt enable
        unsigned TCIE: 1;   // Transmission complete interrupt enable
        unsigned TXEIE: 1;  // TXE interrupt enable
        unsigned PEIE: 1;   // PE interrupt enable
        unsigned PS: 1;     // Parity selection
        unsigned PCE: 1;    // Parity control enable
        unsigned WAKE: 1;   // Wakeup method
        unsigned M: 1;      // Word length
        unsigned UE: 1;     // USART enable
        unsigned Resereved: 1;
        unsigned OVER8: 1;  // Oversampling mode
        unsigned reserved2: 16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_usart_cr1;
typedef union { // t_stm32l1_usart_cr2    USART Control Register 2
    volatile t_u32 All;
    struct {
        unsigned ADD: 4;        // Address of the USART node
        unsigned reserved1 : 1; // forced by hardware to 0.
        unsigned LBDL: 1;       // lin break detection length
        unsigned LBDIE: 1;      // LIN break detection interrupt enable
        unsigned reserved2 : 1; // forced by hardware to 0.
        unsigned LBCL: 1;       // Last bit clock pulse
        unsigned CPHA: 1;       // Clock phase
        unsigned CPOL: 1;       // Clock polarity
        unsigned CLKEN: 1;      // Clock enable
        unsigned STOP: 2;       // Half STOP bits
        unsigned LINEN: 1;      // LIN mode enable
        unsigned reserved3 : 17; // forced by hardware to 0.
    } Bits;

} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_usart_cr2;
typedef union { // t_stm32l1_usart_cr3    USART Control Register 3
    volatile t_u32 All;
    struct {
        unsigned EIE: 1;   // Error interrupt enable
        unsigned IREN: 1;  // IrDA mode enable
        unsigned IRLP: 1;  // IrDA low-power
        unsigned HDSEL: 1; // Half-duplex selection
        unsigned NACK: 1;  // Smartcard NACK enable
        unsigned SC_EN: 1;  // Smartcard mode enable
        unsigned DMAR: 1;  // DMA enable receiver
        unsigned DMAT: 1;  // DMA enable transmitter
        unsigned RTSE: 1;  // RTS enable
        unsigned CTSE: 1;  // CTS enable
        unsigned CTSIE: 1; // CTS interrupt enable
        unsigned ONEBIT: 1; // One sample bit method enable
        unsigned reserved : 20; // forced by hardware to 0.
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_usart_cr3;
typedef union { // t_stm32l1_usart_gtpr   USART Guard Time and Prescaler Register

    volatile t_u32 All;
    struct {
        unsigned PSC : 8;       // Prescaler
        unsigned GT  : 8;       // Guard Time
        unsigned reserved : 16; // forced by hardware to 0.
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_usart_gtpr;
typedef struct { // All USART registers

    volatile t_register_stm32l1xx_usart_sr   SR;
    volatile t_register_stm32l1xx_usart_dr   DR;
    volatile t_register_stm32l1xx_usart_brr  BRR;
    volatile t_register_stm32l1xx_usart_cr1  CR1;
    volatile t_register_stm32l1xx_usart_cr2  CR2;
    volatile t_register_stm32l1xx_usart_cr3  CR3;
    volatile t_register_stm32l1xx_usart_gtpr GTPR;

} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_usart;
typedef union { // t_register_stm32l1xx_i2c_cr1      I2C Control Register 1 (->RM0038 p.598)
    volatile t_u16 All;
    volatile struct {
        unsigned PE            : 1;  // Peripheral enable
        unsigned SMBUS         : 1;  // SMBus mode
        unsigned reserved1     : 1;  // must be kept at reset value
        unsigned SMBTYPE       : 1;  // SMBus type
        unsigned ENARP         : 1;  // ARP enable
        unsigned ENPEC         : 1;  // PEC enable
        unsigned ENGC          : 1;  // General call enable
        unsigned NOSTRETCH     : 1;  // Clock stretching disable (Slave mode)
        unsigned START         : 1;  // Start generation
        unsigned STOP          : 1;  // Stop generation
        unsigned ACK           : 1;  // Acknowledge enable
        unsigned POS           : 1;  // Acknowledge/PEC Position (for data reception)
        unsigned PEC           : 1;  // Packet error checking
        unsigned ALERT         : 1;  // SMBus alert
        unsigned reserved2     : 1;  // must be kept at reset value
        unsigned SWRST         : 1;  // Software reset
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_cr1;
typedef union { // t_register_stm32l1xx_i2c_cr2      I2C Control Register 2 (->RM0038 p.600)
    t_u16 All;
    struct {
        unsigned FREQ          : 6;  // Peripheral clock frequency
        unsigned reserved1     : 2;  // must be kept at reset value
        unsigned ITERR_EN       : 1;  // Error interrupt enable
        unsigned ITEVT_EN       : 1;  // Event interrupt enable
        unsigned ITBUF_EN       : 1;  // Buffer interrupt enable
        unsigned DMA_EN         : 1;  // DMA requests enable
        unsigned LAST          : 1;  // DMA last transfer
        unsigned reserved2     : 3;  // must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_cr2;
typedef union { // t_i2c_oar1_7b  I2C Own Address Register 1 for 7 bit adressing mode (->RM0038 p.602)
    t_u16 All;
    struct {
        unsigned ADD0          : 1;  // not used
        unsigned ADD           : 7;  // Interface address
        unsigned ADD98         : 2;  // not used
        unsigned reserved1     : 4;  // must be kept at reset value
        unsigned reserved2     : 1;  // must be kept at reset value
        unsigned ADDMODE       : 1;  // Addressing mode (slave mode)
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_oar1;
typedef union { // t_register_stm32l1xx_i2c_oar2     I2C Own Address Register 2 (->RM0038 p.602)
    t_u16 All;
    struct {
        unsigned ENDUAL        : 1;  // Dual addressing mode enable
        unsigned ADD2          : 7;  // Interface address
        unsigned reserved1     : 8;  // must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_oar2;
typedef union { // t_register_stm32l1xx_i2c_dr       I2C Data Register (->RM0038 p.603)
    t_u16 All;
    struct {
        unsigned DR            : 8;  // 8-bit data register
        unsigned reserved1     : 8;  // must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_dr;
typedef union { // t_register_stm32l1xx_i2c_sr1      I2C Status Register 1 (->RM0038 p.603)
    t_u16 All;
    struct {
        unsigned SB            : 1;  // Start bit (Master mode)
        unsigned ADDR          : 1;  // Address sent (master mode)/matched (slave mode)
        unsigned BTF           : 1;  // Byte transfer finished
        unsigned ADD10         : 1;  // 10-bit header sent (Master mode)
        unsigned STOPF         : 1;  // Stop detection (slave mode)
        unsigned reserved1     : 1;  // must be kept at reset value
        unsigned RXNE          : 1;  // Data register not empty (receivers)
        unsigned TxE           : 1;  // Data register empty (transmitters)
        unsigned BERR          : 1;  // Bus error
        unsigned ARLO          : 1;  // Arbitration lost (master mode)
        unsigned AF            : 1;  // Acknowledge failure
        unsigned OVR           : 1;  // Overrun/Underrun
        unsigned PECERR        : 1;  // PEC Error in reception
        unsigned reserved2     : 1;  // must be kept at reset value
        unsigned TIMEOUT       : 1;  // Timeout or Tlow error
        unsigned SMBALERT      : 1;  // SMBus alert
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_sr1;
typedef union { // t_register_stm32l1xx_i2c_sr2      I2C Status Register 2 (->RM0038 p.607)
    t_u16 All;
    struct {
        unsigned MSL           : 1;  // Master/ Slave
        unsigned BUSY          : 1;  // Bus busy
        unsigned TRA           : 1;  // Transmitter/receiver
        unsigned reserved1     : 1;  // must be kept at reset value
        unsigned GENCALL       : 1;  // General call address (Slave mode)
        unsigned SMBDEFAULT    : 1;  // SMBus device default address (Slave mode)
        unsigned SMBHOST       : 1;  // SMBus host header (Slave mode)
        unsigned DUALF         : 1;  // Dual flag (Slave mode)
        unsigned PEC           : 8;  // Packet error checking register
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_sr2;
typedef union { // t_register_stm32l1xx_i2c_ccr      I2C Clock Control Register (->RM0038 p.608)
    t_u16 All;
    struct {
        unsigned CCR           : 12; // Clock control register in Fast/Standard mode (Master mode)
        unsigned reserved1     : 2;  // must be kept at reset value
        unsigned DUTY          : 1;  // Fast mode duty cycle
        unsigned Fast          : 1;  // I2C fast mode selection
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_ccr;
typedef union { // t_t_register_stm32l1xx_i2crise    I2C Max Rise Time in Master Mode (->RM0038 p.609)
    t_u16 All;
    struct {
        unsigned TRISE         : 6;  // Maximum rise time in Fast/Standard mode (Master mode)
        unsigned reserved1     : 10; // must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_t_register_stm32l1xx_i2crise;
typedef struct { // t_register_stm32f1xx_i2c          Inter-Integrated Circuit Interface
    volatile t_register_stm32l1xx_i2c_cr1    CR1;
    volatile t_u16  reserved0;
    volatile t_register_stm32l1xx_i2c_cr2    CR2;
    volatile t_u16  reserved1;
    volatile t_register_stm32l1xx_i2c_oar1  OAR1;
    volatile t_u16  reserved2;
    volatile t_register_stm32l1xx_i2c_oar2  OAR2;
    volatile t_u16  reserved3;
    volatile t_register_stm32l1xx_i2c_dr      DR;
    volatile t_u16  reserved4;
    volatile t_register_stm32l1xx_i2c_sr1    SR1;
    volatile t_u16  reserved5;
    volatile t_register_stm32l1xx_i2c_sr2    SR2;
    volatile t_u16  reserved6;
    volatile t_register_stm32l1xx_i2c_ccr    CCR;
    volatile t_u16  reserved7;
    volatile t_t_register_stm32l1xx_i2crise  TRISE;
    volatile t_u16  reserved8;

} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_i2c;
typedef union { // EXTI_IMR - Interrupt Mask Register  (p.200)
    t_u32 All;
    struct {
        unsigned MR0  : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR1  : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR2  : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR3  : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR4  : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR5  : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR6  : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR7  : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR8  : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR9  : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR10 : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR11 : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR12 : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR13 : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR14 : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR15 : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR16 : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR17 : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR18 : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR19 : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR20 : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR21 : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned MR22 : 1; // Interrupt Mask for line x. 1: not masked, 0: masked.
        unsigned reserved1: 9;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_exti_imr;
typedef struct { // EXTI_EMR - Event Mask Register  (p.200)
    union {
        t_u32 All;
        struct {
            unsigned MR0  : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR1  : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR2  : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR3  : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR4  : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR5  : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR6  : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR7  : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR8  : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR9  : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR10 : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR11 : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR12 : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR13 : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR14 : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR15 : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR16 : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR17 : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR18 : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR19 : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR20 : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR21 : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned MR22 : 1; // Event Mask for line x. 1: not masked, 0: masked.
            unsigned reserved1: 9;
        } Bits;
    };
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_exti_emr;
typedef struct { // EXTI_RTSR - Rising Edge Trigger Selection Register  (p.201)
    union {
        t_u32 All;
        struct {
            unsigned TR0  : 1; // Rising edge trigger event configuration bit of line x. (1: enabled)
            unsigned TR1  : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR2  : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR3  : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR4  : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR5  : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR6  : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR7  : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR8  : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR9  : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR10 : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR11 : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR12 : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR13 : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR14 : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR15 : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR16 : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR17 : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR18 : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR19 : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR20 : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR21 : 1; // Rising edge trigger event configuration bit of line x.
            unsigned TR22 : 1; // Rising edge trigger event configuration bit of line x.
            unsigned reserved1: 9;
        } Bits;
    };
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_exti_rtsr;
typedef struct { // EXTI_FTSR - Falling Edge Trigger Selection Register  (p.201)
    union {
        t_u32 All;
        struct {
            unsigned TR0  : 1; // Falling edge trigger event configuration bit of line x. (1: enabled)
            unsigned TR1  : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR2  : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR3  : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR4  : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR5  : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR6  : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR7  : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR8  : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR9  : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR10 : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR11 : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR12 : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR13 : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR14 : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR15 : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR16 : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR17 : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR18 : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR19 : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR20 : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR21 : 1; // Falling edge trigger event configuration bit of line x.
            unsigned TR22 : 1; // Falling edge trigger event configuration bit of line x.
            unsigned reserved1: 9;
        } Bits;
    };
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_exti_ftsr;
typedef struct { // EXTI_SWIER - Software Interrupt Event Register  (p.202)
    union {
        t_u32 All;
        struct {
            unsigned SWIER0  : 1; // Software interrupt on line x.
            unsigned SWIER1  : 1; // Software interrupt on line x.
            unsigned SWIER2  : 1; // Software interrupt on line x.
            unsigned SWIER3  : 1; // Software interrupt on line x.
            unsigned SWIER4  : 1; // Software interrupt on line x.
            unsigned SWIER5  : 1; // Software interrupt on line x.
            unsigned SWIER6  : 1; // Software interrupt on line x.
            unsigned SWIER7  : 1; // Software interrupt on line x.
            unsigned SWIER8  : 1; // Software interrupt on line x.
            unsigned SWIER9  : 1; // Software interrupt on line x.
            unsigned SWIER10 : 1; // Software interrupt on line x.
            unsigned SWIER11 : 1; // Software interrupt on line x.
            unsigned SWIER12 : 1; // Software interrupt on line x.
            unsigned SWIER13 : 1; // Software interrupt on line x.
            unsigned SWIER14 : 1; // Software interrupt on line x.
            unsigned SWIER15 : 1; // Software interrupt on line x.
            unsigned SWIER16 : 1; // Software interrupt on line x.
            unsigned SWIER17 : 1; // Software interrupt on line x.
            unsigned SWIER18 : 1; // Software interrupt on line x.
            unsigned SWIER19 : 1; // Software interrupt on line x.
            unsigned SWIER20 : 1; // Software interrupt on line x.
            unsigned SWIER21 : 1; // Software interrupt on line x.
            unsigned SWIER22 : 1; // Software interrupt on line x.
            unsigned reserved1: 9;
        } Bits;
    };
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_exti_swier;
typedef struct { // EXTI_PR - EXTI Pending Register  (p.203)
    union {
        t_u32 All;
        struct {
            unsigned PR0  : 1; // Pending bit.
            unsigned PR1  : 1; // Pending bit.
            unsigned PR2  : 1; // Pending bit.
            unsigned PR3  : 1; // Pending bit.
            unsigned PR4  : 1; // Pending bit.
            unsigned PR5  : 1; // Pending bit.
            unsigned PR6  : 1; // Pending bit.
            unsigned PR7  : 1; // Pending bit.
            unsigned PR8  : 1; // Pending bit.
            unsigned PR9  : 1; // Pending bit.
            unsigned PR10 : 1; // Pending bit.
            unsigned PR11 : 1; // Pending bit.
            unsigned PR12 : 1; // Pending bit.
            unsigned PR13 : 1; // Pending bit.
            unsigned PR14 : 1; // Pending bit.
            unsigned PR15 : 1; // Pending bit.
            unsigned PR16 : 1; // Pending bit.
            unsigned PR17 : 1; // Pending bit.
            unsigned PR18 : 1; // Pending bit.
            unsigned PR19 : 1; // Pending bit.
            unsigned PR20 : 1; // Pending bit.
            unsigned PR21 : 1; // Pending bit.
            unsigned PR22 : 1; // Pending bit.
            unsigned reserved1: 9;
        } Bits;
    };
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_exti_pr;
typedef struct { // External Interrupt Registers
    volatile t_register_stm32l1xx_exti_imr     IMR;    // Interrupt Mask Register
    volatile t_register_stm32l1xx_exti_emr     EMR;    // Event Mask Register
    volatile t_register_stm32l1xx_exti_rtsr    RTSR;   // Rising Edge Trigger Selection Register
    volatile t_register_stm32l1xx_exti_ftsr    FTSR;   // Falling Edge Trigger Selection Register
    volatile t_register_stm32l1xx_exti_swier   SWIER;  // Software Interrupt Event Register
    volatile t_register_stm32l1xx_exti_pr      PR;     // EXTI Pending Register

} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_exti;
typedef union { // t_register_stm32l1xx_rcc_cr                  Clock Control Register (-> RM0038 p.96,129)
    volatile t_u32 All;
    struct { // -> RM0038 p.96
        unsigned HSION        : 1; // Internal high-speed clock enable
        unsigned HSIRDY       : 1; // Internal high-speed clock ready flag
        unsigned reserved1    : 6; //
        unsigned MSION        : 1; // Internal high-speed clock trimming
        unsigned MSIRDY       : 1; // Internal high-speed clock calibration. These bits are initialized automatically at startup.
        unsigned reserved2    : 6; // HSE clock enable
        unsigned HSEON        : 1; // External high-speed clock ready flag
        unsigned HSERDY       : 1; // External high-speed clock bypass
        unsigned HSEBYP       : 1; // Clock security system enable
        unsigned reserved3    : 5; //
        unsigned PLLON        : 1; // PLL enable
        unsigned PLLRDY       : 1; // PLL clock ready flag
        unsigned reserved4    : 2; // PLL2 enable
        unsigned CSSON        : 1; // PLL2 clock ready flag
        unsigned RTCPRE       : 2; // RealtimeClock / LED prescaler
        unsigned reserved5    : 1; //
        /* DEPRECATED
                                            #else // WORKAROUND: This structure fits to STM32L100C Discovery (has not HSE Crystal)
                                                    unsigned HSEON        : 1; // External high-speed clock ready flag
                                                    unsigned HSERDY       : 1; // External high-speed clock bypass
                                                    unsigned HSEBYP       : 1; // Clock security system enable
                                                    unsigned reserved3    : 5; //
                                                    unsigned MSION        : 1; // Internal high-speed clock trimming
                                                    unsigned MSIRDY       : 1; // Internal high-speed clock calibration. These bits are initialized automatically at startup.
                                                    unsigned reserved2    : 6; // HSE clock enable
                                                    unsigned HSION        : 1; // Internal high-speed clock enable
                                                    unsigned HSIRDY       : 1; // Internal high-speed clock ready flag
                                                    unsigned reserved1    : 6; //
                                                    unsigned PLLON        : 1; // PLL enable
                                                    unsigned PLLRDY       : 1; // PLL clock ready flag
                                                    unsigned reserved4    : 2; // PLL2 enable
                                                    unsigned CSSON        : 1; // PLL2 clock ready flag
                                                    unsigned RTCPRE       : 2; // RealtimeClock / LED prescaler
                                                    unsigned reserved5    : 1; //
                                            */
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc_cr;
typedef enum { // Frequency Ranges of MSI (->RM0038 0.97)
    rcc_icsr_msirange0 = 0b000, // 65536 kHz
    rcc_icsr_msirange1 = 0b001, // 131.072 kHz
    rcc_icsr_msirange2 = 0b010, // 262.144 kHz
    rcc_icsr_msirange3 = 0b011, // 524.288 kHz
    rcc_icsr_msirange4 = 0b100, // 1.048 MHz
    rcc_icsr_msirange5 = 0b101, // 2.097 MHz
    rcc_icsr_msirange6 = 0b110, // 4.194 MHz
    rcc_icsr_error     = 0b111, // not allowed
} e_rcc_icsr_msirange;
typedef union { // t_register_stm32l1xx_rcc_icscr
    volatile t_u32 All;
    struct {
        unsigned HSICAL       : 8; // Internal high speed clock calibration
        unsigned HSITRIM      : 5; // High speed internal clock trimming
        unsigned MSIRANGE     : 3; // MSI clock ranges
        unsigned MSICAL       : 8; // MSI clock calibration
        unsigned MSITRIM      : 8; // MSI clock trimming
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc_icscr;
typedef enum { // System Clock Switch Sources
    rcc_cfgr_sw_msi = 0, // Medium Speed Internal Oscillator
    rcc_cfgr_sw_hsi = 1, // High Speed Internal Oscillator
    rcc_cfgr_sw_hse = 2, // High Speed External Oscillator
    rcc_cfgr_sw_pll = 3, // PLL Multiplier + Divider
} e_cfgr_sw;
typedef e_cfgr_sw e_cfgr_sws; // SW and SWS fields have same meanings
typedef enum { // AHB Prescaler Values (->RM0038 0.100)
    rcc_cfgr_hpre_div1   = 0b0000, // AHB = SYSCLOCK / 1
    rcc_cfgr_hpre_div2   = 0b1000, // AHB = SYSCLOCK / 2
    rcc_cfgr_hpre_div4   = 0b1001, // AHB = SYSCLOCK / 4
    rcc_cfgr_hpre_div8   = 0b1010, // AHB = SYSCLOCK / 8
    rcc_cfgr_hpre_div16  = 0b1011, // AHB = SYSCLOCK / 16
    rcc_cfgr_hpre_div64  = 0b1100, // AHB = SYSCLOCK / 64
    rcc_cfgr_hpre_div128 = 0b1101, // AHB = SYSCLOCK / 128
    rcc_cfgr_hpre_div256 = 0b1110, // AHB = SYSCLOCK / 256
    rcc_cfgr_hpre_div512 = 0b1111, // AHB = SYSCLOCK / 512
} e_cfgr_hpre;
typedef enum { // APB1, APB2 Prescaler Values (->RM0038 0.99)
    rcc_cfgr_ppre_div1   = 0b000, // AHB = SYSCLOCK / 1
    rcc_cfgr_ppre_div2   = 0b100, // AHB = SYSCLOCK / 2
    rcc_cfgr_ppre_div4   = 0b101, // AHB = SYSCLOCK / 4
    rcc_cfgr_ppre_div8   = 0b110, // AHB = SYSCLOCK / 8
    rcc_cfgr_ppre_div16  = 0b111, // AHB = SYSCLOCK / 16
} e_cfgr_ppre1;
typedef e_cfgr_ppre1 e_cfgr_ppre1; // PPRE1 and PPRE2 fields have same meanings
typedef enum { // Microcontroller Clock Output PREscaler (->RM0038 0.99)
    rcc_cfgr_mco_div1   = 0b000, // MCO is divided by 1
    rcc_cfgr_mco_div2   = 0b001, // MCO is divided by 2
    rcc_cfgr_mco_div4   = 0b010, // MCO is divided by 4
    rcc_cfgr_mco_div8   = 0b011, // MCO is divided by 8
    rcc_cfgr_mco_div16  = 0b100, // MCO is divided by 16
} e_cfgr_mcopre;
typedef enum { // Microcontroller Clock Output SELection (->RM0038 0.98)
    rcc_cfgr_mco_none    = 0b000, // MCO output disabled
    rcc_cfgr_mco_sysclk  = 0b001, // SYSCLOCK selected
    rcc_cfgr_mco_hsi     = 0b010, // HSI oscillator selected
    rcc_cfgr_mco_msi     = 0b011, // MSI oscillator selected
    rcc_cfgr_mco_hse     = 0b100, // HSE oscillator selected
    rcc_cfgr_mco_pll     = 0b101, // PLL clock selected
    rcc_cfgr_mco_lsi     = 0b110, // LSI oscillator selected
    rcc_cfgr_mco_lse     = 0b111, // LSE oscillator selected
} e_cfgr_mcosel;
typedef union { // t_register_stm32l1xx_rcc_cfgr                Clock Configuration Register (-> RM0038 p.98)
    volatile t_u32 All;
    struct {
        unsigned SW           : 2; // System clock switch
        unsigned SWS          : 2; // System clock switch status
        unsigned HPRE         : 4; // AHB prescaler
        unsigned PPRE1        : 3; // APB low-speed prescaler (APB1)
        unsigned PPRE2        : 3; // APB high-speed prescaler (APB2Set and cleared by software to control th(PCLK2).
        unsigned reserved1    : 2; // ADC prescaler
        unsigned PLLSRC       : 1; // PLL entry clock source
        unsigned reserved2    : 1; // HSE divider for PLL entry
        unsigned PLLMUL       : 4; // PLL multiplication factor
        unsigned PLLDIV       : 2; // USB prescaler
        unsigned MCOSEL       : 3; // Microcontroller Clock Output SELection ->e_cfgr_mcosel
        unsigned reserved3    : 1; // reserved
        unsigned MCOPRE       : 3; // Microcontroller Clock Output PREscaler ->e_cfgr_mcopre
        unsigned reserved4    : 1; // reserved
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc_cfgr;
typedef union { // t_register_stm32l1xx_rcc_cir                 Clock interrupt register
    volatile t_u32 All;
    struct {
        unsigned LSIRDYF      : 1; // LSI ready interrupt flag
        unsigned LSERDYF      : 1; // LSE ready interrupt flag
        unsigned HSIRDYF      : 1; // HSI ready interrupt flag
        unsigned HSERDYF      : 1; // HSE ready interrupt flag
        unsigned PLLRDYF      : 1; // PLL ready interrupt flag
        unsigned MSIRDYF      : 1; //
        unsigned LDECSSF      : 1;
        unsigned CSSF         : 1; // Clock security system interrupt flag
        unsigned LSIRDYIE     : 1; // LSI ready interrupt enable
        unsigned LSERDYIE     : 1; // LSE ready interrupt enable
        unsigned HSIRDYIE     : 1; // HSI ready interrupt enable
        unsigned HSERDYIE     : 1; // HSE ready interrupt enable
        unsigned PLLRDYIE     : 1; // PLL ready interrupt enable
        unsigned MSIRDYIE     : 1; // PLL ready interrupt enable
        unsigned LSECSSIE     : 1; // PLL ready interrupt enable
        unsigned reserved2    : 1; //
        unsigned LSIRDYC      : 1; // LSI ready interrupt clear
        unsigned LSERDYC      : 1; // LSE ready interrupt clear
        unsigned HSIRDYC      : 1; // HSI ready interrupt clear
        unsigned HSERDYC      : 1; // HSE ready interrupt clear
        unsigned PLLRDYC      : 1; // PLL ready interrupt clear
        unsigned MSIRDYC      : 1; //
        unsigned LSECSSC      : 1; //
        unsigned CSSC         : 1; // Clock security system interrupt clear
        unsigned reserved     : 8; //
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc_cir;
typedef union { // APB2RSTR

    volatile t_u32 All;

    struct {
        unsigned SYSCFGRST     : 1; //
        unsigned reserved1     : 1; //
        unsigned TIM9RST       : 1; //
        unsigned TIM10RST      : 1; //
        unsigned TIM11RST      : 1; //
        unsigned reserved2     : 4; //
        unsigned ADC1RST       : 1; //
        unsigned reserved3     : 1; //
        unsigned SDIORST       : 1; //
        unsigned SPI1RST       : 1; //
        unsigned reserved4     : 1; //
        unsigned USART1RST     : 1; //
        unsigned reserved5     : 17; //
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc_apb2rstr;
typedef union { // t_register_stm32l1xx_rcc_apb1rstr            APB1 Peripheral Reset Register
    volatile t_u32 All;
    struct {
        unsigned TIM2RST      : 1; // Timer 2 reset
        unsigned TIM3RST      : 1; // Timer 3 reset
        unsigned TIM4RST      : 1; // Timer 4 reset
        unsigned TIM5RST      : 1; // Timer 5 reset
        unsigned TIM6RST      : 1; // Timer 6 reset
        unsigned TIM7RST      : 1; // Timer 7 reset
        unsigned reserved1    : 3; //
        unsigned LCDRST       : 1; //
        unsigned reserved6    : 1; //
        unsigned WWDRST       : 1; // Window watchdog reset
        unsigned reserved2    : 2; //
        unsigned SPI2RST      : 1; // SPI2 reset
        unsigned SPI3RST      : 1; // SPI3 reset
        unsigned reserved3    : 1; //
        unsigned USART2RST    : 1; // USART 2 reset
        unsigned USART3RST    : 1; // USART 3 reset
        unsigned UART4RST     : 1; // USART 4 reset
        unsigned UART5RST     : 1; // USART 5 reset
        unsigned I2C1RST      : 1; // I2C1 reset
        unsigned I2C2RST      : 1; // I2C 2 reset
        unsigned USBRST       : 1; //
        unsigned reserved7    : 4; // CAN1 reset
        unsigned PWRRST       : 1; // Power interface reset
        unsigned DACRST       : 1; // DAC interface reset
        unsigned reserved5    : 1; //
        unsigned COMPRST      : 1; //
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc_apb1rstr;
typedef union { // t_register_stm32l1xx_rcc_ahblpenr
    volatile t_u32 All;
    struct {
        unsigned GPIOALPEN      : 1; //
        unsigned GPIOBLPEN      : 1; //
        unsigned GPIOCLPEN      : 1; //
        unsigned GPIODLPEN      : 1; //
        unsigned GPIOELPEN      : 1; //
        unsigned GPIOHLPEN      : 1; //
        unsigned GPIOFLPEN      : 1; //
        unsigned GPIOGLPEN      : 1; //
        unsigned reserved1      : 4; //
        unsigned CRCLPEN        : 1; //
        unsigned reserved2      : 2; //
        unsigned FLITFLPEN      : 1; //
        unsigned SRAMLPEN       : 1; //
        unsigned reserved3      : 7; //
        unsigned DMA1LPEN       : 1; //
        unsigned DMA2LPEN       : 1; //
        unsigned reserved4      : 1; //
        unsigned AESLPEN        : 1; //
        unsigned reserved5      : 2; //
        unsigned FSMCLPEN       : 1; //
        unsigned reserved6      : 1; //
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc_ahblpenr;
typedef union { // t_register_stm32l1xx_rcc_csr                 Control/status register

    volatile t_u32 All;
    struct {
        unsigned LSION      : 1; // Internal low speed oscillator enable
        unsigned LSIRDY     : 1; // Internal low speed oscillator ready
        unsigned reserved1  : 6; //
        unsigned LSEON      : 1; //
        unsigned LSERDY     : 1; //
        unsigned LSEBYP     : 1; //
        unsigned LSECSSON   : 1; //
        unsigned LSECSSD    : 1; //
        unsigned reserved3  : 3; //
        unsigned RTCSEL     : 2; //
        unsigned reserved4  : 4; //
        unsigned RTC_EN      : 1; //
        unsigned RTCRST     : 1; //
        unsigned RMVF       : 1; // Remove reset flag
        unsigned OBLRSTF    : 1; //
        unsigned PINRSTF    : 1; // PIN reset flag
        unsigned PORRSTF    : 1; // POR/PDR reset flag
        unsigned SFTRSTF    : 1; // Software reset flag
        unsigned IWDGRSTF   : 1; // Independent watchdog reset flag
        unsigned WWDGRSTF   : 1; // Window watchdog reset flag
        unsigned LPWRRSTF   : 1; // Low-power reset flag
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc_csr;
typedef union { // AHBRSTR
    volatile t_u32 All;
    struct {
        unsigned GPIOARST      : 1; //
        unsigned GPIOBRST      : 1; //
        unsigned GPIOCRST      : 1; //
        unsigned GPIODRST      : 1; //
        unsigned GPIOERST      : 1; //
        unsigned GPIOHRST      : 1; //
        unsigned GPIOFRST      : 1; //
        unsigned GPIOGRST      : 1; //
        unsigned reserved1     : 4; //
        unsigned CRCRST        : 1; //
        unsigned reserved2     : 2; //
        unsigned FLITFRST      : 1; //
        unsigned reserved3     : 8; //
        unsigned DMA1RST       : 1; //
        unsigned DMA2RST       : 1; //
        unsigned reserved4     : 1; //
        unsigned AESRST        : 1; //
        unsigned reserved5     : 2; //
        unsigned FSMCRST       : 1; //
        unsigned reserved6     : 1; //
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc_ahbrstr;
typedef union { // t_register_stm32l1xx_rcc_ahbenr
    volatile t_u32 All;
    struct {
        unsigned GPIOA_EN      : 1; //
        unsigned GPIOB_EN      : 1; //
        unsigned GPIOC_EN      : 1; //
        unsigned GPIOD_EN      : 1; //
        unsigned GPIOE_EN      : 1; //
        unsigned GPIOH_EN      : 1; //
        unsigned GPIOF_EN      : 1; //
        unsigned GPIOG_EN      : 1; //
        unsigned reserved1     : 4; //
        unsigned CRC_EN        : 1; //
        unsigned reserved2     : 2; //
        unsigned FLITF_EN      : 1; //
        unsigned reserved3     : 8; //
        unsigned DMA1_EN       : 1; //
        unsigned DMA2_EN       : 1; //
        unsigned reserved4     : 1; //
        unsigned AESEN        : 1; //
        unsigned reserved5     : 2; //
        unsigned FSMC_EN       : 1; //
        unsigned reserved6     : 1; //
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc_ahbenr;
typedef union { // t_register_stm32l1xx_rcc_apb2enr
    volatile t_u32 All;
    struct {

        unsigned SYSCFGEN       : 1;
        unsigned reserved1      : 1;
        unsigned TIM9EN         : 1;
        unsigned TIM10EN        : 1;
        unsigned TIM11EN        : 1;
        unsigned reserved2      : 4;
        unsigned ADC1EN         : 1;
        unsigned reserved3      : 1;
        unsigned SDIOEN         : 1;
        unsigned SPI1EN         : 1;
        unsigned reserved4      : 1;
        unsigned USART1EN       : 1;
        unsigned reserved5      : 1;
        unsigned reserved6      : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc_apb2enr;
typedef union { // t_register_stm32l1xx_rcc_apb1enr
    volatile t_u32 All;

    struct {
        unsigned TIM2_EN      : 1; // Timer 2 clock enable
        unsigned TIM3_EN      : 1; // Timer 3 clock enable
        unsigned TIM4_EN      : 1; // Timer 4 clock enable
        unsigned TIM5_EN      : 1; // Timer 5 clock enable
        unsigned TIM6_EN      : 1; // Timer 6 clock enable
        unsigned TIM7_EN      : 1; // Timer 7 clock enable
        unsigned reserved1   : 3; //
        unsigned LCDEN       : 1; // LCD clock enable (STM32L152 only)
        unsigned reserved2   : 1; //
        unsigned WWDG_EN      : 1; // Window watchdog clock enable
        unsigned reserved3   : 2; //
        unsigned SPI2_EN      : 1; // SPI2 clock enable
        unsigned SPI3_EN      : 1; // SPI3 clock enable
        unsigned reserved4   : 1; //
        unsigned USART2_EN    : 1; // USART 2 clock enable
        unsigned USART3_EN    : 1; // USART 3 clock enable
        unsigned USART4EN    : 1; // USART 4 clock enable
        unsigned USART5EN    : 1; // USART 5 clock enable
        unsigned I2C1_EN      : 1; // I2C1 interface clock enable
        unsigned I2C2_EN      : 1; // I2C2 interface clock enable
        unsigned USB_EN       : 1; // USB interface clock enable
        unsigned reserved7   : 4; //
        unsigned PWR_EN       : 1; // Power interface clock enable
        unsigned DAC_EN       : 1; // Digital to Analog Converter clock enable
        unsigned reserved5   : 1; //
        unsigned COMPEN      : 1; // Comparator interface clock enable
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc_apb1enr;
typedef union { // t_register_stm32l1xx_rcc_apb2lpenr
    volatile t_u32 All;
    struct {
        unsigned SYSCFGLPEN     : 1; //
        unsigned reserved1      : 1; //
        unsigned TIM9LPEN       : 1; //
        unsigned TIM10LPEN      : 1; //
        unsigned TIM11LPEN      : 1; //
        unsigned reserved2      : 4; //
        unsigned ADC1LPEN       : 1; //
        unsigned reserved3      : 1; //
        unsigned SDIOLPEN       : 1; //
        unsigned SPI1LPEN       : 1; //
        unsigned reserved4      : 1; //
        unsigned USART1LPEN     : 1; //
        unsigned reserved5      : 17; //
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc_apb2lpenr;
typedef union { // t_register_stm32l1xx_rcc_apb1lpenr
    volatile t_u32 All;
    struct {
        unsigned TIM2LPEN       : 1; // Timer 2 in low power mode clock enable
        unsigned TIM3LPEN       : 1; // Timer 3 in low power mode clock enable
        unsigned TIM4LPEN       : 1; // Timer 4 in low power mode clock enable
        unsigned TIM5LPEN       : 1; // Timer 5 in low power mode clock enable
        unsigned TIM6LPEN       : 1; // Timer 6 in low power mode clock enable
        unsigned TIM7LPEN       : 1; // Timer 7 in low power mode clock enable
        unsigned reserved1      : 3; //
        unsigned LCDLPEN        : 1; //
        unsigned reserved6      : 1; //
        unsigned WWDGLPEN       : 1; // Window watchdog in low power mode clock enable
        unsigned reserved2      : 2; //
        unsigned SPI2LPEN       : 1; // SPI2 in low power mode clock enable
        unsigned SPI3LPEN       : 1; // SPI3 in low power mode clock enable
        unsigned reserved3      : 1; //
        unsigned USART2LPEN     : 1; // USART 2 in low power mode clock enable
        unsigned USART3LPEN     : 1; // USART 3 in low power mode clock enable
        unsigned USART4LPEN     : 1; // USART 4 in low power mode clock enable
        unsigned USART5LPEN     : 1; // USART 5 in low power mode clock enable
        unsigned I2C1LPEN       : 1; // I2C1 in low power mode clock enable
        unsigned I2C2LPEN       : 1; // I2C 2 in low power mode clock enable
        unsigned USBLPEN        : 1; //
        unsigned reserved7      : 4; // CAN1 in low power mode clock enable
        unsigned PWRLPEN        : 1; // Power interface in low power mode clock enable
        unsigned DACLPEN        : 1; // DAC interface in low power mode clock enable
        unsigned reserved5      : 1; //
        unsigned COMPLPEN       : 1; //
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc_apb1lpenr;
typedef struct { // t_register_stm32f1xx_rcc  Reset and Clock Control (RCC)

    t_register_stm32l1xx_rcc_cr        CR; // comment
    t_register_stm32l1xx_rcc_icscr     ICSCR;
    t_register_stm32l1xx_rcc_cfgr      CFGR;
    t_register_stm32l1xx_rcc_cir       CIR;
    t_register_stm32l1xx_rcc_ahbrstr   AHBRSTR;
    t_register_stm32l1xx_rcc_apb2rstr  APB2RSTR;
    t_register_stm32l1xx_rcc_apb1rstr  APB1RSTR;
    t_register_stm32l1xx_rcc_ahbenr    AHBENR;
    t_register_stm32l1xx_rcc_apb2enr   APB2ENR;
    t_register_stm32l1xx_rcc_apb1enr   APB1ENR;
    t_register_stm32l1xx_rcc_ahblpenr  AHBLPENR;
    t_register_stm32l1xx_rcc_apb2lpenr APB2LPENR;
    t_register_stm32l1xx_rcc_apb1lpenr APB1LPENR;
    t_register_stm32l1xx_rcc_csr       CSR;

} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rcc;
typedef union { // t_t_register_stm32l1xx_rtcr
    volatile t_u32 All;
    struct {
        unsigned SU         : 4;
        unsigned ST         : 3;
        unsigned reserved1  : 1;
        unsigned MNU        : 4;
        unsigned MNT        : 3;
        unsigned reserved2  : 1;
        unsigned HU         : 4;
        unsigned HT         : 2;
        unsigned PM         : 1;
        unsigned reserved3  : 9;
    } Bits;
} __attribute__( ( __packed__ ) ) t_t_register_stm32l1xx_rtcr;
typedef union { // t_register_stm32l1xx_rtc_dr
    volatile t_u32 All;
    struct {

        unsigned DU         : 4;
        unsigned DT         : 2;
        unsigned reserved1  : 2;
        unsigned MU         : 4;
        unsigned MT         : 1;
        unsigned WDU        : 3;
        unsigned YU         : 4;
        unsigned YT         : 4;
        unsigned reserved2  : 8;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_dr;
typedef union { // t_register_stm32l1xx_rtc_cr
    volatile t_u32 All;
    struct {

        unsigned WCKSEL     : 3;
        unsigned TSEDGE     : 1;
        unsigned REFCKON    : 1;
        unsigned BYPSHAD    : 1;
        unsigned FMT        : 1;
        unsigned DCE        : 1;
        unsigned ALRAE      : 1;
        unsigned ALRBE      : 1;
        unsigned WUTE       : 1;
        unsigned TSE        : 1;
        unsigned ALRAIE     : 1;
        unsigned ALRBIE     : 1;
        unsigned WUTIE      : 1;
        unsigned TSIE       : 1;
        unsigned ADD1H      : 1;
        unsigned SUB1H      : 1;
        unsigned BKP        : 1;
        unsigned COSEL      : 1;
        unsigned POL        : 1;
        unsigned OSEL       : 2;
        unsigned COE        : 1;
        unsigned reserved1  : 8;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_cr;
typedef union { // t_register_stm32l1xx_rtc_isr
    volatile t_u32 All;
    struct {

        unsigned ALRAWF     : 1;
        unsigned ALRBWF     : 1;
        unsigned WUTWF      : 1;
        unsigned SHPF       : 1;
        unsigned INITS      : 1;
        unsigned RSF        : 1;
        unsigned INITF      : 1;
        unsigned INIT       : 1;
        unsigned ALRAF      : 1;
        unsigned ALRBF      : 1;
        unsigned WUTF       : 1;
        unsigned TSF        : 1;
        unsigned TSOVF      : 1;
        unsigned TAMP1F     : 1;
        unsigned TAMP2F     : 1;
        unsigned TAMP3F     : 1;
        unsigned reserved1  : 16;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_isr;
typedef union { // t_register_stm32l1xx_rtc_prer
    volatile t_u32 All;
    struct {

        unsigned PREDIV_S       : 15;
        unsigned reserved1      : 1;
        unsigned PREDIV_A       : 7;
        unsigned reserved2      : 9;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_prer;
typedef union { // t_register_stm32l1xx_rtc_wutr
    volatile t_u32 All;
    struct {
        unsigned WUT            : 16;
        unsigned reserved1      : 16;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_wutr;
typedef union { // t_register_stm32l1xx_rtc_calibr
    volatile t_u32 All;
    struct {
        unsigned DC         : 5;
        unsigned reserved1  : 2;
        unsigned DCS        : 1;
        unsigned reserved2  : 24;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_calibr;
typedef union { // t_register_stm32l1xx_rtc_alrmar
    volatile t_u32 All;
    struct {
        unsigned SU     : 4;
        unsigned ST     : 3;
        unsigned MSK1   : 1;
        unsigned MNU    : 4;
        unsigned MNT    : 3;
        unsigned MSK2   : 1;
        unsigned HU     : 4;
        unsigned HT     : 2;
        unsigned PM     : 1;
        unsigned MSK3   : 1;
        unsigned DU     : 4;
        unsigned DT     : 2;
        unsigned WDSWEL : 1;
        unsigned MSK4   : 1;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_alrmar;
typedef union { // t_register_stm32l1xx_rtc_alrmar
    volatile t_u32 All;
    struct {
        unsigned SU     : 4;
        unsigned ST     : 3;
        unsigned MSK1   : 1;
        unsigned MNU    : 4;
        unsigned MNT    : 3;
        unsigned MSK2   : 1;
        unsigned HU     : 4;
        unsigned HT     : 2;
        unsigned PM     : 1;
        unsigned MSK3   : 1;
        unsigned DU     : 4;
        unsigned DT     : 2;
        unsigned WDSWEL : 1;
        unsigned MSK4   : 1;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_alrmbr;
typedef union { // t_register_stm32l1xx_rtc_wpr
    volatile t_u32 All;
    struct {
        unsigned KEY        : 8;
        unsigned reserved1  : 24;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_wpr;
typedef union { // t_register_stm32l1xx_rtc_ssr
    volatile t_u32 All;
    struct {
        unsigned SS        : 16;
        unsigned reserved1  : 24;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_ssr;
typedef union { // t_register_stm32l1xx_rtc_shiftr
    volatile t_u32 All;
    struct {
        unsigned SUBFS      : 15;
        unsigned reserved1  : 16;
        unsigned ADD1S      : 1;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_shiftr;
typedef union { // t_t_register_stm32l1xx_rtcstr
    volatile t_u32 All;
    struct {
        unsigned SU         : 4;
        unsigned ST         : 3;
        unsigned reserved1  : 1;
        unsigned MNU        : 4;
        unsigned MNT        : 3;
        unsigned reserved2  : 1;
        unsigned HU         : 4;
        unsigned HT         : 2;
        unsigned PM         : 1;
        unsigned reserved3  : 9;

    } Bits;
} __attribute__( ( __packed__ ) ) t_t_register_stm32l1xx_rtcstr;
typedef union { // t_t_register_stm32l1xx_rtcsdr
    volatile t_u32 All;
    struct {
        unsigned DU         : 4;
        unsigned DT         : 2;
        unsigned reserved1  : 2;
        unsigned MU         : 4;
        unsigned MT         : 1;
        unsigned WDU        : 3;
        unsigned reserved2  : 16;

    } Bits;
} __attribute__( ( __packed__ ) ) t_t_register_stm32l1xx_rtcsdr;
typedef union { // t_t_register_stm32l1xx_rtcsssr
    volatile t_u32 All;
    struct {
        unsigned SS                 : 16;
        unsigned reserved           : 16;

    } Bits;
} __attribute__( ( __packed__ ) ) t_t_register_stm32l1xx_rtcsssr;
typedef union { // t_register_stm32l1xx_rtc_calr
    volatile t_u32 All;
    struct {
        unsigned CALM           : 9;
        unsigned reserved1      : 4;
        unsigned CALW16         : 1;
        unsigned CALW8          : 1;
        unsigned CALP           : 1;
        unsigned reserved2      : 16;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_calr;
typedef union { // t_t_register_stm32l1xx_rtcafcr

    volatile t_u32 All;
    struct {
        unsigned TAMP1E     : 1;
        unsigned TAMP1ETRG  : 1;
        unsigned TAMPIE     : 1;
        unsigned TAMP2E     : 1;
        unsigned TAMP2TRG   : 1;
        unsigned TAMP3E     : 1;
        unsigned TAMP3TRG   : 1;
        unsigned TAMPTS     : 1;
        unsigned TAMPFREQ   : 3;
        unsigned TAMPFLT    : 2;
        unsigned TAMPPRCH   : 2;
        unsigned TAMPPUDIS  : 1;
        unsigned reserved1  : 2;
        unsigned ALARMOUTTYPE: 1;
        unsigned reserved   : 13;

    } Bits;
} __attribute__( ( __packed__ ) ) t_t_register_stm32l1xx_rtcafcr;
typedef union { // t_register_stm32l1xx_rtc_alrmassr
    volatile t_u32 All;
    struct {
        unsigned SS         : 15;
        unsigned reserved1  : 9;
        unsigned MASKSS     : 4;
        unsigned reserved2  : 4;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_alrmassr;
typedef union { // t_register_stm32l1xx_rtc_alrmbssr
    volatile t_u32 All;
    struct {
        unsigned SS         : 15;
        unsigned reserved1  : 9;
        unsigned MASKSS     : 4;
        unsigned reserved2  : 4;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_alrmbssr;
typedef union { // t_register_stm32l1xx_rtc_bkpor
    volatile t_u32 All;

} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc_bkpor;
typedef struct { // t_register_stm32l1xx_rtc

    volatile t_t_register_stm32l1xx_rtcr        TR;
    volatile t_register_stm32l1xx_rtc_dr        DR;
    volatile t_register_stm32l1xx_rtc_cr        CR;
    volatile t_register_stm32l1xx_rtc_isr       ISR;
    volatile t_register_stm32l1xx_rtc_prer      PRER;
    volatile t_register_stm32l1xx_rtc_wutr      WUTR;
    volatile t_register_stm32l1xx_rtc_calibr    CALIBR;
    volatile t_register_stm32l1xx_rtc_alrmar    ALRMAR;
    volatile t_register_stm32l1xx_rtc_alrmbr    ALRMBR;
    volatile t_register_stm32l1xx_rtc_wpr       WPR;
    volatile t_register_stm32l1xx_rtc_ssr       SSR;
    volatile t_register_stm32l1xx_rtc_shiftr    SHIFTR;
    volatile t_t_register_stm32l1xx_rtcstr      TSTR;
    volatile t_t_register_stm32l1xx_rtcsdr      TSDR;
    volatile t_t_register_stm32l1xx_rtcsssr     TSSSR;
    volatile t_register_stm32l1xx_rtc_calr      CALR;
    volatile t_t_register_stm32l1xx_rtcafcr     TAFCR;
    volatile t_register_stm32l1xx_rtc_alrmassr  ALRMASSR;
    volatile t_register_stm32l1xx_rtc_alrmbssr  ALRMBSSR;
    volatile t_register_stm32l1xx_rtc_bkpor     BKPOR;

} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_rtc;
typedef union { // t_register_stm32l1xx_pwr_cr
    volatile t_u32 All;
    struct {
        unsigned LPSDSR     : 1;
        unsigned PDDS       : 1;
        unsigned CWUF       : 1;
        unsigned CSBF       : 1;
        unsigned PVDE       : 1;
        unsigned PLS        : 3;
        unsigned DBP        : 1;
        unsigned ULP        : 1;
        unsigned FWU        : 1;
        unsigned VOS        : 2;
        unsigned reserved1  : 1;
        unsigned LPRUN      : 1;
        unsigned reserved2  : 17;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_pwr_cr;
typedef union { // register_stm32l1xx_pwr_scr_t
    volatile t_u32 All;
    struct {
        unsigned WUF        : 1;
        unsigned SBF        : 1;
        unsigned PVDO       : 1;
        unsigned VREFINTRDYF: 1;
        unsigned VOSF       : 1;
        unsigned REGLPF     : 1;
        unsigned reserved1  : 2;
        unsigned EWUP1      : 1;
        unsigned EWUP2      : 1;
        unsigned EWUP3      : 1;
        unsigned reserved2  : 21;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_pwr_csr;
typedef struct { // t_register_stm32l1xx_pwr

    t_register_stm32l1xx_pwr_cr    CR;  // power control register
    t_register_stm32l1xx_pwr_csr   CSR; // power control & status register

} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_pwr;
typedef union { // t_register_stm32l1xx_dma_isr
    volatile t_u32 All;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_dma_isr;
typedef union { // t_register_stm32l1xx_dma_ifcr
    volatile t_u32 All;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_dma_ifcr;
typedef union { // t_register_stm32l1xx_dma_ccr1
    volatile t_u32 All;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_dma_ccr1;
typedef union { // t_register_stm32l1xx_dma_isr
    volatile t_u32 All;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_dma_cndtr1;
typedef union { // t_register_stm32l1xx_dma_ifcr
    volatile t_u32 All;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_dma_cpar1;
typedef union { // t_register_stm32l1xx_dma_ccr1
    volatile t_u32 All;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_dma_cmar1;
typedef union {// TIMx_CR1 - control reg 1 (p. 375)
    t_u16 All;
    struct {
        unsigned C_EN:  1;
        unsigned UDIS: 1;
        unsigned URS:  1;
        unsigned OPM:  1;
        unsigned DIR:  1;
        unsigned CMS:  2;
        unsigned ARPE: 1;
        unsigned CKD:  2;
        unsigned reserved: 6;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_cr1;
typedef union {// TIMx_CR2 - control reg 2 (p. 377)
    t_u16 All;
    struct {
        unsigned reserved1:  3;
        unsigned CCDS:       1;
        unsigned MMS:        3;
        unsigned TI1S:       1;
        unsigned reserved2:  8;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_cr2;
typedef union {// TIMx_SMCR - slave mode control reg (p.378)
    t_u16 All;
    struct {
        unsigned SMS:        3; // see
        unsigned reserved1:  1;
        unsigned TS:         3;
        unsigned MSM:        1;
        unsigned ETF:        4;
        unsigned ETPS:       2;
        unsigned ECE:        1;
        unsigned ETP:        1;
    } Bits;

} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_smcr;
typedef union {// TIMx_DIER - DMA/Interrupt enable reg (p.380)
    t_u16 All;
    struct {
        unsigned UIE:    1;
        unsigned CC1IE:  1;
        unsigned CC2IE:  1;
        unsigned CC3IE:  1;
        unsigned CC4IE:  1;
        unsigned reserved1:  1;
        unsigned TIE:  1;
        unsigned reserved2:  1;
        unsigned UDE:  1;
        unsigned CC1DE:  1;
        unsigned CC2DE:  1;
        unsigned CC3DE:  1;
        unsigned CC4DE:  1;
        unsigned COMDE:  1;
        unsigned TDE:  1;
        unsigned reserved4:  1;
    } Bits;

} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_dier;
typedef union {// TIMx_SR - Status Register (p.381)
    t_u16 All;
    struct {
        unsigned UIF:       1;
        unsigned CC1IF:     1;
        unsigned CC2IF:     1;
        unsigned CC3IF:     1;
        unsigned CC4IF:     1;
        unsigned reserved1: 1;
        unsigned TIF:       1;
        unsigned reserved2: 2;
        unsigned CC1OF:     1;
        unsigned CC2OF:     1;
        unsigned CC3OF:     1;
        unsigned CC4OF:     1;
        unsigned reserved3: 3;
    } Bits;

} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_sr;
typedef union {// TIMx_EGR - Event Generation Register
    t_u16 All;
    struct {
        unsigned UG:        1;
        unsigned CC1G:      1;
        unsigned CC2G:      1;
        unsigned CC3G:      1;
        unsigned CC4G:      1;
        unsigned reserved1: 1;
        unsigned TG:        1;
        unsigned reserved2: 9;
    } Bits;

} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_egr;
typedef union {// TIMx_CCMR1 (output compare mode) - capture/compare mode reg 1 (p.384)
    t_u16 All;
    struct {
        unsigned CC1S:  2;
        unsigned OC1FE: 1;
        unsigned OC1PE: 1;
        unsigned OC1M:  3;
        unsigned OC1CE: 1;
        unsigned CC2S:  2;
        unsigned OC2FE: 1;
        unsigned OC2PE: 1;
        unsigned OC2M:  3;
        unsigned OC2CE: 1;
    } Bits;

} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_ccmr1_compare_mode;
typedef union {// TIMx_CCMR1 (input capture mode) - capture/compare mode reg 1 (p.384)
    t_u16 All;
    struct {
        unsigned CC1S:   2;
        unsigned IC1PSC: 2;
        unsigned IC1F:   4;
        unsigned CC2S:   2;
        unsigned IC2PSC: 2;
        unsigned IC2F:   4;
    } Bits;

} __attribute__( ( __packed__ ) )  t_timx_ccmr1_input_capture_mode;
typedef union {// TIMx_CCMR2 (output compare mode) - capture/compare mode reg 2 (p.387)
    t_u16 All;
    struct {
        unsigned CC3S:  2;
        unsigned OC3FE: 1;
        unsigned OC3PE: 1;
        unsigned OC3M:  3;
        unsigned OC3CE: 1;
        unsigned CC4S:  2;
        unsigned OC4FE: 1;
        unsigned OC4PE: 1;
        unsigned OC4M:  3;
        unsigned OC4CE: 1;
    } Bits;

} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_ccmr2_compare_mode;
typedef union {// TIMx_CCMR2 (input capture mode) - capture/compare mode reg 2 (p.387)
    t_u16 All;
    struct {
        unsigned CC3S:   2;
        unsigned IC3PSC: 2;
        unsigned IC3F:   4;
        unsigned CC4S:   2;
        unsigned IC4PSC: 2;
        unsigned IC4F:   4;
    } Bits;

} __attribute__( ( __packed__ ) )  t_timx_ccmr2_input_capture_mode;
typedef union {// TIMx_CCER - capture/compare enable reg (p.388)
    t_u16 All;
    struct {
        unsigned CC1E:      1;
        unsigned CC1P:      1;
        unsigned reserved1: 1;
        unsigned CC1NP:     1;
        unsigned CC2E:      1;
        unsigned CC2P:      1;
        unsigned reserved2: 1;
        unsigned CC2NP:     1;
        unsigned CC3E:      1;
        unsigned CC3P:      1;
        unsigned reserved3: 1;
        unsigned CC3NP:     1;
        unsigned CC4E:      1;
        unsigned CC4P:      1;
        unsigned reserved4: 1;
        unsigned CC4NP:     1;
    } Bits;

} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_ccer;
typedef union {// TIMx_CNT - counter (p.390)
    t_u16 All;
    struct {
        unsigned CNT:  16;
    } Bits;

} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_cnt;
typedef union {// TIMx_PSC - prescalar (p.390)
    t_u16 All;
    struct {
        unsigned PSC:  16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_psc;
typedef union {// TIMx_ARR - auto-reload reg (p.390)
    t_u16 All;
    struct {
        unsigned ARR:  16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_arr;
typedef union {// TIMx_CCR1 - capture/compare reg 1 (p.391)
    t_u16 All;
    struct {
        unsigned CCR1:  16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_ccr1;
typedef union {// TIMx_CCR2 - capture/compare reg 2 (p.391)
    t_u16 All;
    struct {
        unsigned CCR2:  16;
    } Bits;

} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_ccr2;
typedef union {// TIMx_CCR3 - capture/compare reg 3 (p.392)
    t_u16 All;
    struct {
        unsigned CCR3:  16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_ccr3;
typedef union {// TIMx_CCR4 - capture/compare reg 4 (p.392)
    t_u16 All;
    struct {
        unsigned CCR4:  16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_ccr4;
typedef union {// TIMx_DCR - DMA control reg (p.393)
    t_u16 All;
    struct {
        unsigned  DBA:          5;
        unsigned  reserved1:    3;
        unsigned  DBL:          5;
        unsigned  reserved2:    3;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_dcr;
typedef union {// TIMx_DMAR - DMA address for full transfer (p.393)
    t_u16 All;
    struct {
        unsigned  DMAB:     16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_timx_dmar;
typedef union {// TIM2_OR - TIM2 option reg (p.395)
    t_u16 All;
    struct {
        unsigned ITR1_RMP:      1;
        unsigned reserved:      15;
    } Bits;
} __attribute__( ( __packed__ ) )  t_tim2_or;
typedef union {// TIM3_OR - TIM3 option reg (p.395)
    t_u16 All;
    struct {
        unsigned ITR2_RMP:      1;
        unsigned reserved:      15;
    } Bits;

} __attribute__( ( __packed__ ) )  t_tim3_or;
typedef struct {// TIM3_OR - TIM3 option reg (p.395)

    t_u16 All;

} __attribute__( ( __packed__ ) ) t_reserved_reg;
typedef union {// TIMx_DMAR - DMA address for full transfer (p.393)
    volatile t_u32 All;
    struct {
        unsigned  TI1_RMP: 2;
        unsigned ITR1_RMP: 1;
        unsigned reserved1: 13;
        unsigned reserved2: 16;
    } Bits;

} __attribute__( ( __packed__ ) )  t_timx_or;
typedef struct { // General-Purpose Timer Registers (TIM2 - TIM4) - with output compare registers
    volatile t_register_stm32l1xx_timx_cr1                  CR1;    //
    volatile t_register_stm32l1xx_timx_cr2                  CR2;    //
    volatile t_register_stm32l1xx_timx_smcr                 SMCR;   //
    volatile t_register_stm32l1xx_timx_dier                 DIER;   //
    volatile t_register_stm32l1xx_timx_sr                   SR;     //
    volatile t_register_stm32l1xx_timx_egr                  ER;     //
    volatile t_register_stm32l1xx_timx_ccmr1_compare_mode   CCMR1;  //
    volatile t_register_stm32l1xx_timx_ccmr2_compare_mode   CCMR2;  //
    volatile t_register_stm32l1xx_timx_ccer                 CCER;   //
    volatile t_register_stm32l1xx_timx_cnt                  CNT;    // counter
    volatile t_register_stm32l1xx_timx_psc                  PSC;    // prescalar
    volatile t_register_stm32l1xx_timx_arr                  ARR;    // auto-reload reg
    volatile t_reserved_reg              XXX;    // reserved
    volatile t_register_stm32l1xx_timx_ccr1                 CCR1;   //
    volatile t_register_stm32l1xx_timx_ccr2                 CCR2;   //
    volatile t_register_stm32l1xx_timx_ccr3                 CCR3;   //
    volatile t_register_stm32l1xx_timx_ccr4                 CCR4;   //
    volatile t_reserved_reg              YYY;    // reserved
    volatile t_register_stm32l1xx_timx_dcr                  DCR;    //
    volatile t_register_stm32l1xx_timx_dmar                 DMAR;   //
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim2_to_tim4_output_compare_mode;
typedef struct { // General-Purpose Timer Registers (TIM2 - TIM4) - with input capture registers
    volatile t_register_stm32l1xx_timx_cr1                      CR1;    //
    volatile t_register_stm32l1xx_timx_cr2                      CR2;    //
    volatile t_register_stm32l1xx_timx_smcr                     SMCR;   //
    volatile t_register_stm32l1xx_timx_dier                     DIER;   //
    volatile t_register_stm32l1xx_timx_sr                       SR;     //
    volatile t_register_stm32l1xx_timx_egr                      EGR;     //
    volatile t_timx_ccmr1_input_capture_mode CCMR1;  //
    volatile t_timx_ccmr2_input_capture_mode CCMR2;  //
    volatile t_register_stm32l1xx_timx_ccer                     CCER;   //
    volatile t_register_stm32l1xx_timx_cnt                      CNT;    // counter
    volatile t_register_stm32l1xx_timx_psc                      PSC;    // prescalar
    volatile t_register_stm32l1xx_timx_arr                      ARR;    // auto-reload reg
    volatile t_reserved_reg                  XXX;    // reserved
    volatile t_register_stm32l1xx_timx_ccr1                     CCR1;   //
    volatile t_register_stm32l1xx_timx_ccr2                     CCR2;   //
    volatile t_register_stm32l1xx_timx_ccr3                     CCR3;   //
    volatile t_register_stm32l1xx_timx_ccr4                     CCR4;   //
    volatile t_reserved_reg                  YYY;    // reserved
    volatile t_register_stm32l1xx_timx_dcr                      DCR;    //
    volatile t_register_stm32l1xx_timx_dmar                     DMAR;   //
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim2_to_tim4_input_capture_mode;
typedef struct { // General-Purpose Timer Registers (TIM2 - TIM4) - with input capture registers
    volatile t_register_stm32l1xx_timx_cr1                      CR1;    //
    volatile t_register_stm32l1xx_timx_cr2                      CR2;    //
    volatile t_register_stm32l1xx_timx_smcr                     SMCR;   //
    volatile t_register_stm32l1xx_timx_dier                     DIER;   //
    volatile t_register_stm32l1xx_timx_sr                       SR;     //
    volatile t_register_stm32l1xx_timx_egr                      EGR;     //
    volatile t_timx_ccmr1_input_capture_mode CCMR1;  //
    volatile t_reserved_reg                  reserved1;    // reserved
    volatile t_register_stm32l1xx_timx_ccer                     CCER;   //
    volatile t_register_stm32l1xx_timx_cnt                      CNT;    // counter
    volatile t_register_stm32l1xx_timx_psc                      PSC;    // prescalar
    volatile t_register_stm32l1xx_timx_arr                      ARR;    // auto-reload reg
    volatile t_reserved_reg                  reserved5;    // reserved
    volatile t_register_stm32l1xx_timx_ccr1                     CCR1;   //
    volatile t_register_stm32l1xx_timx_ccr2                     CCR2;   //
    volatile t_reserved_reg                  reserved2;   //
    volatile t_reserved_reg                  reserved3;   //
    volatile t_reserved_reg                  reserved4;    // reserved
    volatile t_reserved_reg                  reserved6;    //
    volatile t_timx_or                       DMAR;   //
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim9_input_capture;
typedef struct { // General-Purpose Timer Registers (TIM2 - TIM4) - with input capture registers
    volatile t_register_stm32l1xx_timx_cr1                      CR1;    //
    volatile t_register_stm32l1xx_timx_cr2                      CR2;    //
    volatile t_register_stm32l1xx_timx_smcr                     SMCR;   //
    volatile t_register_stm32l1xx_timx_dier                     DIER;   //
    volatile t_register_stm32l1xx_timx_sr                       SR;     //
    volatile t_register_stm32l1xx_timx_egr                      EGR;     //
    volatile t_register_stm32l1xx_timx_ccmr1_compare_mode       CCMR1;  //
    volatile t_reserved_reg                  reserved1;    // reserved
    volatile t_register_stm32l1xx_timx_ccer                     CCER;   //
    volatile t_register_stm32l1xx_timx_cnt                      CNT;    // counter
    volatile t_register_stm32l1xx_timx_psc                      PSC;    // prescalar
    volatile t_register_stm32l1xx_timx_arr                      ARR;    // auto-reload reg
    volatile t_reserved_reg                  reserved5;    // reserved
    volatile t_register_stm32l1xx_timx_ccr1                     CCR1;   //
    volatile t_register_stm32l1xx_timx_ccr2                     CCR2;   //
    volatile t_reserved_reg                  reserved2;   //
    volatile t_reserved_reg                  reserved3;   //
    volatile t_reserved_reg                  reserved4;    // reserved
    volatile t_reserved_reg                 reserved6;    //
    volatile t_timx_or                          DMAR;   //
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim9_output_compare;
typedef struct { // General-Purpose Timer Registers (TIM6 - TIM7) - with input capture registers

    volatile t_register_stm32l1xx_timx_cr1          CR1;
    volatile t_register_stm32l1xx_timx_cr2          CR2;
    volatile t_reserved_reg      reserved1;    // reserved
    volatile t_register_stm32l1xx_timx_dier         DIER;
    volatile t_register_stm32l1xx_timx_sr           SR;
    volatile t_register_stm32l1xx_timx_egr          EGR;
    volatile t_reserved_reg      reserved2;    // reserved
    volatile t_reserved_reg      reserved3;    // reserved
    volatile t_reserved_reg      reserved4;    // reserved
    volatile t_register_stm32l1xx_timx_cnt          CNT;
    volatile t_register_stm32l1xx_timx_psc          PSC;
    volatile t_register_stm32l1xx_timx_arr          ARR;

} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim6_to_tim7;
typedef union {// TIMx_CR1 - control reg 1 (p. 375)
    volatile t_u32 All;
    struct {
        unsigned C_EN:       1;
        unsigned UDIS:      1;
        unsigned URS:       1;
        unsigned reserved1: 4;
        unsigned ARPE:      1;
        unsigned CKD:       2;
        unsigned reserved:  22;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim10_to_tim11_cr1;
typedef union {// TIMx_CR1 - control reg 1 (p. 375)
    volatile t_u32 All;
    struct {
        unsigned reserved:  8;
        unsigned ETF:       4;
        unsigned ETPS:      2;
        unsigned ECE:       1;
        unsigned ETP:       3;
        unsigned reserved1: 14;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim10_to_tim11_smcr;
typedef union {// TIMx_CR1 - control reg 1 (p. 375)
    volatile t_u32 All;
    struct {
        unsigned UIE:       1;
        unsigned CC1IE:     1;
        unsigned reserved1: 30;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim10_to_tim11_dier;
typedef union {// TIMx_CR1 - control reg 1 (p. 375)
    volatile t_u32 All;
    struct {
        unsigned UIF:       1;
        unsigned CC1IF:     1;
        unsigned reserved1: 7;
        unsigned CC1OF:     1;
        unsigned reserved2: 22;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim10_to_tim11_sr;
typedef union {// TIMx_CR1 - control reg 1 (p. 375)
    volatile t_u32 All;
    struct {
        unsigned UG:       1;
        unsigned CC1G:     1;
        unsigned reserved1: 30;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim10_to_tim11_egr;
typedef union {// TIMx_CR1 - control reg 1 (p. 375)
    volatile t_u32 All;
    struct {
        unsigned CC1S:      2;
        unsigned OC1FE:     1;
        unsigned OC1PE:     2;
        unsigned OC1M:      1;
        unsigned OC1CE:     2;
        unsigned reserved1: 24;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim10_to_tim11_ccmr1_input_capture_mode;
typedef union {// TIMx_CR1 - control reg 1 (p. 375)
    volatile t_u32 All;
    struct {
        unsigned CC1S:      2;
        unsigned IC1PSC:    2;
        unsigned IC1F:      4;
        unsigned reserved1: 24;
    } Bits;
} __attribute__( ( __packed__ ) )  t_tim10_to_tim11_ccmr1_output_compare_mode;
typedef union {// TIMx_CR1 - control reg 1 (p. 375)
    volatile t_u32 All;
    struct {
        unsigned CC1E:      1;
        unsigned CC1P:      1;
        unsigned reserved:  1;
        unsigned CC1NP:     1;
        unsigned reserved1: 28;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim10_to_tim11_ccer;
typedef union {// TIMx_CR1 - control reg 1 (p. 375)
    volatile t_u32 All;
    struct {
        unsigned CNT:       16;
        unsigned reserved1: 16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim10_to_tim11_cnt;
typedef union {// TIMx_CR1 - control reg 1 (p. 375)
    volatile t_u32 All;
    struct {
        unsigned PSC:       16;
        unsigned reserved1: 16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim10_to_tim11_psc;
typedef union {// TIMx_CR1 - control reg 1 (p. 375)
    volatile t_u32 All;
    struct {
        unsigned ARR:       16;
        unsigned reserved1: 16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim10_to_tim11_arr;
typedef union {// TIMx_CR1 - control reg 1 (p. 375)
    volatile t_u32 All;
    struct {
        unsigned CCR1:       16;
        unsigned reserved1: 16;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim10_to_tim11_ccr1;
typedef union {// TIMx_CR1 - control reg 1 (p. 375)
    volatile t_u32 All;
    struct {
        unsigned TL1_RMP:       2;
        unsigned ETR_RMP:       1;
        unsigned TL1_RMP_R1:    1;
        unsigned reserved1:     28;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim10_to_tim11_or;
typedef struct { // General-Purpose Timer Registers (TIM2 - TIM4) - with input capture registers
    volatile t_register_stm32l1xx_tim10_to_tim11_cr1                       CR1;    //
    volatile t_register_stm32l1xx_tim10_to_tim11_smcr                      SMCR;   //
    volatile t_register_stm32l1xx_tim10_to_tim11_dier                      DIER;   //
    volatile t_register_stm32l1xx_tim10_to_tim11_sr                        SR;     //
    volatile t_register_stm32l1xx_tim10_to_tim11_egr                       EGR;     //
    volatile t_register_stm32l1xx_tim10_to_tim11_ccmr1_input_capture_mode  CCMR1;  //
    volatile t_reserved_reg                             reserved;  //
    volatile t_register_stm32l1xx_tim10_to_tim11_ccer                      CCER;   //
    volatile t_register_stm32l1xx_tim10_to_tim11_cnt                       CNT;    // counter
    volatile t_register_stm32l1xx_tim10_to_tim11_psc                       PSC;    // prescalar
    volatile t_register_stm32l1xx_tim10_to_tim11_arr                       ARR;    // auto-reload reg
    volatile t_reserved_reg                             reserved1;    // reserved
    volatile t_register_stm32l1xx_tim10_to_tim11_ccr1                      CCR1;   //
    volatile t_reserved_reg                             reserved2;    // reserved
    volatile t_reserved_reg                             reserved3;    // reserved
    volatile t_register_stm32l1xx_tim10_to_tim11_or                        OR;    //
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim10_to_tim11_input_capture_mode;
typedef struct { // General-Purpose Timer Registers (TIM2 - TIM4) - with input capture registers
    volatile t_register_stm32l1xx_tim10_to_tim11_cr1                       CR1;    //
    volatile t_register_stm32l1xx_tim10_to_tim11_smcr                      SMCR;   //
    volatile t_register_stm32l1xx_tim10_to_tim11_dier                      DIER;   //
    volatile t_register_stm32l1xx_tim10_to_tim11_sr                        SR;     //
    volatile t_register_stm32l1xx_tim10_to_tim11_egr                       EGR;     //
    volatile t_tim10_to_tim11_ccmr1_output_compare_mode CCMR1;  //
    volatile t_reserved_reg                             reserved;  //
    volatile t_register_stm32l1xx_tim10_to_tim11_ccer                      CCER;   //
    volatile t_register_stm32l1xx_tim10_to_tim11_cnt                       CNT;    // counter
    volatile t_register_stm32l1xx_tim10_to_tim11_psc                       PSC;    // prescalar
    volatile t_register_stm32l1xx_tim10_to_tim11_arr                       ARR;    // auto-reload reg
    volatile t_reserved_reg                             reserved1;    // reserved
    volatile t_register_stm32l1xx_tim10_to_tim11_ccr1                      CCR1;   //
    volatile t_reserved_reg                             reserved2;    // reserved
    volatile t_reserved_reg                             reserved3;    // reserved
    volatile t_register_stm32l1xx_tim10_to_tim11_or                        OR;    //
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_tim10_to_tim11_output_compare_mode;
typedef union {// CR - control register (p.280)
    volatile t_u32 All;
    struct {
        unsigned  EN1:              1;
        unsigned  BOFF1:            1;
        unsigned  TEN1:             1;
        unsigned  TSEL1:            3;
        unsigned  WAVE1:            2;
        unsigned  MAMP1:            4;
        unsigned  DMA_EN1:           1;
        unsigned  DMAUDRIE1:        1;
        unsigned  reserved1:        2;
        unsigned  EN2:              1;
        unsigned  BOFF2:            1;
        unsigned  TEN2:             1;
        unsigned  TSEL2:            3;
        unsigned  WAVE2:            2;
        unsigned  MAMP2:            4;
        unsigned  DMA_EN2:           1;
        unsigned  DMAUDRIE2:        1;
        unsigned  reserved2:        2;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac_cr;
typedef union {// SWTRIGR
    volatile t_u32 All;
    struct {
        unsigned  SWTRIG1:          1;
        unsigned  SWTRIG2:          1;
        unsigned  reserved:         30;
    } Bits;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac_swtrigr;
typedef struct {// DAC_DHR12R1
    unsigned  DACC1DHR:         12;
    unsigned  reserved:         20;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac_dhr12r1;
typedef struct {// DHR12L1
    unsigned  reserved1:         4;
    unsigned  DACC1DHR:         12;
    unsigned  reserved2:        16;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac_dhr12l1;
typedef struct {// DHR8R1
    unsigned  DACC1DHR:          8;
    unsigned  reserved:         24;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac_dhr8r1;
typedef struct {// DHR12R2
    unsigned  DACC2DHR:         12;
    unsigned  reserved:         20;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac_dhr12r2;
typedef struct {// DHR12L2
    unsigned  reserved1:         4;
    unsigned  DACC2DHR:         12;
    unsigned  reserved2:        16;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac_dhr12l2;
typedef struct {// DHR8R2
    unsigned  DACC2DHR:          8;
    unsigned  reserved:         24;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac_dhr8r2;
typedef struct {// DHR12RD
    unsigned  DACC1DHR:         12;
    unsigned  reserved1:         4;
    unsigned  DACC2DHR:         12;
    unsigned  reserved2:         4;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac_dhr12rd;
typedef struct {// DHR12LD
    unsigned  reserved1:         4;
    unsigned  DACC1DHR:         12;
    unsigned  reserved2:         4;
    unsigned  DACC2DHR:         12;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac_dhr12ld;
typedef struct {// DHR8RD
    unsigned  DACC1DHR:          8;
    unsigned  DACC2DHR:          8;
    unsigned  reserved:         16;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac_dhr8rd;
typedef struct {// DOR1
    unsigned  DACC1DOR:         12;
    unsigned  reserved:         20;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac_dor1;
typedef struct {// DOR2
    unsigned  DACC2DOR:         12;
    unsigned  reserved:         20;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac_dor2;
typedef struct {// SR - status register  (p.288)
    unsigned  reserved1:         13;
    unsigned  DMAUDR1:            1;
    unsigned  reserved2:         15;
    unsigned  DMAUDR2:            1;
    unsigned  reserved3:          2;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac_sr;
typedef struct {// DAC register map
    volatile t_register_stm32l1xx_dac_cr                CR; //
    volatile t_register_stm32l1xx_dac_swtrigr           SWTRIGR;
    volatile t_register_stm32l1xx_dac_dhr12r1           DHR12R1;
    volatile t_register_stm32l1xx_dac_dhr12l1           DHR12L1;
    volatile t_register_stm32l1xx_dac_dhr8r1            DHR8R1;
    volatile t_register_stm32l1xx_dac_dhr12r2           DHR12R2;
    volatile t_register_stm32l1xx_dac_dhr12l2           DHR12L2;
    volatile t_register_stm32l1xx_dac_dhr8r2            DHR8R2;
    volatile t_register_stm32l1xx_dac_dhr12rd           DHR12RD;
    volatile t_register_stm32l1xx_dac_dhr12ld           DHR12LD;
    volatile t_register_stm32l1xx_dac_dhr8rd            DHR8RD;
    volatile t_register_stm32l1xx_dac_dor1              DOR1;
    volatile t_register_stm32l1xx_dac_dor2              DOR2;
    volatile t_register_stm32l1xx_dac_sr                SR;
} __attribute__( ( __packed__ ) )  t_register_stm32l1xx_dac;
typedef union { // t_register_stm32l1xx_spi_cr1                SPI Control Register 1 (->RM0038 p.744)
    volatile t_u32 All;
    volatile struct {
        unsigned CPHA     :  1; // Bit 0 CPHA: Clock phase
        unsigned CPOL     :  1; // Bit 1 CPOL: Clock polarity
        unsigned MSTR     :  1; // Bit 2 MSTR: Master selection
        unsigned BR       :  3; // Bits 5:3 BR[2:0]: Baud rate control
        unsigned SPE      :  1; // Bit 6 SPE: SPI enable
        unsigned LSBFIRST :  1; // Bit 7 LSBFIRST: Frame format
        unsigned SSI      :  1; // Bit 8 SSI: Internal slave select
        unsigned SSM      :  1; // Bit 9 SSM: Software slave management
        unsigned RXONLY   :  1; // Bit 10 RXONLY: Receive only
        unsigned DFF      :  1; // Bit 11 DFF: Data frame format
        unsigned CRCNEXT  :  1; // Bit 12 CRCNEXT: CRC transfer next
        unsigned CRC_EN    :  1; // Bit 13 CRC_EN: Hardware CRC calculation enable
        unsigned BIDIOE   :  1; // Bit 14 BIDIOE: Output enable in bidirectional mode
        unsigned BIDIMODE :  1; // Bit 15 BIDIMODE: Bidirectional data mode enable
        unsigned reserved : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_spi_cr1;
typedef union { // t_register_stm32l1xx_spi_cr2                SPI Control Register 2 (->RM0038 p.746)
    volatile t_u32 All;
    struct {
        unsigned RXDMA_EN   :  1; // Bit 0 RXDMA_EN: Rx buffer DMA enable
        unsigned TXDMA_EN   :  1; // Bit 1 TXDMA_EN: Tx buffer DMA enable
        unsigned SSOE      :  1; // Bit 2 SSOE: SS output enable
        unsigned reserved1 :  1; // Bit 3
        unsigned FRF       :  1; // Bit 4 FRF: Frame format -> Motorola mode / TI mode
        unsigned ERRIE     :  1; // Bit 5 ERRIE: Error interrupt enable
        unsigned RXNEIE    :  1; // Bit 6 RXNEIE: RX buffer not empty interrupt enable
        unsigned TXEIE     :  1; // Bit 7 TXEIE: Tx buffer empty interrupt enable
        unsigned reserved  :  24; // Bits 15:8 reserved, must be kept at reset value.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_spi_cr2;
typedef union { // t_register_stm32l1xx_spi_sr                 SPI Status Register (->RM0038 p.748)
    volatile t_u32 All;
    struct {
        unsigned RXNE      :  1; // Bit 0 RXNE: Receive buffer not empty
        unsigned TXE       :  1; // Bit 1 TXE: Transmit buffer empty
        unsigned CHSIDE    :  1; // Bit 2 CHSIDE: Channel side
        unsigned UDR       :  1; // Bit 3 UDR: Underrun flag
        unsigned CRCERR    :  1; // Bit 4 CRCERR: CRC error flag
        unsigned MODF      :  1; // Bit 5 MODF: Mode fault
        unsigned OVR       :  1; // Bit 6 OVR: Overrun flag
        unsigned BSY       :  1; // Bit 7 BSY: Busy flag
        unsigned FRE       :  1; // Bit 8 FRE: Frame Error
        unsigned reserved  :  23; // Bits 15:9 reserved, must be kept at reset value.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_spi_sr;
typedef union { // t_register_stm32l1xx_spi_dr                 SPI Data Register (->RM0038 p.749)
    volatile t_u32 All;
    struct {
        volatile t_u16 Data;            // data received or transmitted
        unsigned reserved : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_spi_dr;
typedef union { // t_register_stm32l1xx_spi_crc                SPI CRC Register (->RM0038 p.750)
    volatile t_u32 All;
    struct {
        unsigned Polynom  : 16;        // polynomial used for CRC calculation
        unsigned reserved : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_spi_crc;
typedef union { // t_register_stm32l1xx_spi_rx_crc             SPI Computed CRC value of received data (->RM0038 p.751)
    volatile t_u32 All;
    struct {
        unsigned Value    : 16;        // calculated CRC value (only lower 8 bits valid in 8-bit mode)
        unsigned reserved : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_spi_rx_crc;
typedef union { // t_register_stm32l1xx_spix_crc_t             SPI Computed CRC value of transmitted data (->RM0038 p.751)
    volatile t_u32 All;
    struct {
        unsigned Value    : 16;        // calculated CRC value (only lower 8 bits valid in 8-bit mode)
        unsigned reserved : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_spix_crc_t;
typedef union { // t_register_stm32l1xx_spi_i2scfgr            SPI I2S-Configuration Register (->RM0038 p.752)
    volatile t_u32 All;
    struct {
        unsigned CHL_EN      :  1; // Bit 0 CHL_EN: Channel length (number of bits per audio channel)
        unsigned DATL_EN     :  2; // Bit 2:1 DATL_EN: Data length to be transferred
        unsigned CKPOL      :  1; // Bit 3 CKPOL: Steady state clock polarity
        unsigned I2SSTD     :  2; // Bit 5:4 I2SSTD: I2S standard selection
        unsigned reserved1  :  1; // Bit 6 reserved: forced at 0 by hardware
        unsigned PCMSYNC    :  1; // Bit 7 PCMSYNC: PCM frame synchronization
        unsigned I2SCFG     :  2; // Bit 9:8 I2SCFG: I2S configuration mode
        unsigned I2SE       :  1; // Bit 10 I2SE: I2S Enable
        unsigned I2SMOD     :  1; // Bit 11 I2SMOD: I2S mode selection
        unsigned reserved2  :  20; // Bits 15:12 reserved, must be kept at reset value.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_spi_i2scfgr;
typedef union { // t_register_stm32l1xx_spi_i2spr              SPI I2S Prescaler Register (->RM0038 p.753)
    volatile t_u32 All;
    struct {
        unsigned I2SDIV    :  8; // Bit 7:0 I2SDIV: I2S Linear prescaler
        unsigned ODD       :  1; // Bit 8 ODD: Odd factor for the prescaler
        unsigned MCKOE     :  1; // Bit 9 MCKOE: Master clock output enable
        unsigned reserved  :  22; // Bits 15:10 reserved, must be kept at reset value.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_spi_i2spr;
typedef struct { // t_register_stm32l1xx_spi Serial Peripheral Interface
    volatile t_register_stm32l1xx_spi_cr1     CR1;
    volatile t_register_stm32l1xx_spi_cr2     CR2;
    volatile t_register_stm32l1xx_spi_sr      SR;
    volatile t_register_stm32l1xx_spi_dr      DR;
    volatile t_register_stm32l1xx_spi_crc     CRC_Polynom;
    volatile t_register_stm32l1xx_spi_rx_crc  RX_CRC;
    volatile t_register_stm32l1xx_spix_crc_t  TX_CRC;
    volatile t_register_stm32l1xx_spi_i2scfgr I2SCFGR;
    volatile t_register_stm32l1xx_spi_i2spr   I2SPR;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_spi;
typedef union { // t_register_stm32l1xx_adc_sr                ADC Status Register (->RM0038 p.247)
    volatile t_u32 All;
    struct {
        unsigned AWD        : 1;     // Analog Watchdog Flag
        unsigned EOC        : 1;     // End of conversion
        unsigned JEOC       : 1;     // Injected channel end of conversion
        unsigned JSTRT      : 1;     // Injected channel Start flag
        unsigned STRT       : 1;     // Regular channel Start flag
        unsigned OVR        : 1;     // Overrun
        unsigned ADONS      : 1;     // ADC ON status
        unsigned reserved1  : 1;     //
        unsigned RCNR       : 1;     // Regular channel not ready
        unsigned JCNR       : 1;     // Injected channel not ready
        unsigned reserved   : 22;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_sr;
typedef union { // t_register_stm32l1xx_adc_cr1                ADC Control Register 1 (->RM0038 p.249)
    volatile t_u32 All;
    struct {
        unsigned AWDCH      : 5;     // Analog Watchdog Flag
        unsigned EOCIE      : 1;     // Interrupt enable for EOC
        unsigned AWDIE      : 1;     // Analog watchdog interrupt enable
        unsigned JEOCIE     : 1;     // Interrupt enable for injected channels
        unsigned SCAN       : 1;     // Scan mode
        unsigned AWDSGL     : 1;     // Enable the watchdog on a single channel in scan mode
        unsigned JAUTO      : 1;     // Automatic Injected Group conversion
        unsigned DISC_EN     : 1;     // Discontinuous mode on regular channels
        unsigned JDISC_EN    : 1;     // Discontinuous mode on injected channels
        unsigned DISCNUM    : 3;     // Discontinuous mode channel count
        unsigned PDD        : 1;     // Power down during the delay phase
        unsigned PDI        : 1;     // Power down during the idle phase
        unsigned reserved1  : 4;     //
        unsigned JAWD_EN     : 1;     // Analog watchdog enable on injected channels
        unsigned AWD_EN      : 1;     // Analog watchdog enable on regular channels
        unsigned RES        : 2;     // Resolution
        unsigned OVRIE      : 1;     // Overrun interrupt enable
        unsigned reserved2  : 5;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_cr1;
typedef union { // t_register_stm32l1xx_adc_cr2                ADC Control Register 2 (->RM0038 p.251)
    volatile t_u32 All;
    struct {
        unsigned ADON       : 1;     // A/D converter ON/OFF
        unsigned CONT       : 1;     // Continuous conversion
        unsigned ADC_CFG    : 1;     // ADC configuration
        unsigned reserved0  : 1;     //
        unsigned DELS       : 3;     // Delay selection
        unsigned reserved1  : 1;     //
        unsigned DMA        : 1;     // Diect memory access mode
        unsigned DDS        : 1;     // DMA disable selection
        unsigned EOCS       : 1;     // End of conversion selection
        unsigned ALIGN      : 1;     // Data alignment
        unsigned reserved2  : 4;     //
        unsigned JEXTSEL    : 4;     // External event select for injected group
        unsigned JEXTEN     : 2;     // External tirgger enable for injected channels
        unsigned JSWSTART   : 1;     // Start conversion of injected channels
        unsigned reserved3  : 1;     //
        unsigned EXTSEL     : 4;     // External event select for regular group
        unsigned EXTEN      : 2;     // External trigger enable for regular channels
        unsigned SWSTART    : 1;     // Start conversion of regular channels
        unsigned reserved4  : 1;     //
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_cr2;
typedef union { // t_register_stm32l1xx_adc_smpr1                ADC sample time register 2 (->RM0038 p.255)
    volatile t_u32 All;
    struct {
        unsigned SMP20      : 3;     // Channel 20 Sample time selection
        unsigned SMP21      : 3;     // Channel 21 Sample time selection
        unsigned SMP22      : 3;     // Channel 22 Sample time selection
        unsigned SMP23      : 3;     // Channel 23 Sample time selection
        unsigned SMP24      : 3;     // Channel 24 Sample time selection
        unsigned SMP25      : 3;     // Channel 25 Sample time selection
        unsigned SMP26      : 3;     // Channel 26 Sample time selection
        unsigned SMP27      : 3;     // Channel 27 Sample time selection
        unsigned SMP28      : 3;     // Channel 28 Sample time selection
        unsigned SMP29      : 3;     // Channel 29 Sample time selection
        unsigned reserved   : 2;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_smpr1;
typedef union { // t_register_stm32l1xx_adc_smpr2                ADC sample time register 2 (->RM0038 p.255)
    volatile t_u32 All;
    struct {
        unsigned SMP10      : 3;     // Channel 10 Sample time selection
        unsigned SMP11      : 3;     // Channel 11 Sample time selection
        unsigned SMP12      : 3;     // Channel 12 Sample time selection
        unsigned SMP13      : 3;     // Channel 13 Sample time selection
        unsigned SMP14      : 3;     // Channel 14 Sample time selection
        unsigned SMP15      : 3;     // Channel 15 Sample time selection
        unsigned SMP16      : 3;     // Channel 16 Sample time selection
        unsigned SMP17      : 3;     // Channel 17 Sample time selection
        unsigned SMP18      : 3;     // Channel 18 Sample time selection
        unsigned SMP19      : 3;     // Channel 19 Sample time selection
        unsigned reserved   : 2;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_smpr2;
typedef union { // t_register_stm32l1xx_adc_smpr3                ADC sample time register 3 (->RM0038 p.256)
    volatile t_u32 All;
    struct {
        unsigned SMP0      : 3;     // Channel 0 Sample time selection
        unsigned SMP1      : 3;     // Channel 1 Sample time selection
        unsigned SMP2      : 3;     // Channel 2 Sample time selection
        unsigned SMP3      : 3;     // Channel 3 Sample time selection
        unsigned SMP4      : 3;     // Channel 4 Sample time selection
        unsigned SMP5      : 3;     // Channel 5 Sample time selection
        unsigned SMP6      : 3;     // Channel 6 Sample time selection
        unsigned SMP7      : 3;     // Channel 7 Sample time selection
        unsigned SMP8      : 3;     // Channel 8 Sample time selection
        unsigned SMP9      : 3;     // Channel 9 Sample time selection
        unsigned reserved  : 2;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_smpr3;
typedef union { // t_register_stm32l1xx_adc_jofr1                ADC injected channel data offset register x (->RM0038 p.257)
    volatile t_u32 All;
    struct {
        unsigned JOFFSET1  : 12;    // Data offset for injected channel 1
        unsigned reserved  : 20;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_jofr1;
typedef union { // t_register_stm32l1xx_adc_jofr2                ADC injected channel data offset register x (->RM0038 p.257)
    volatile t_u32 All;
    struct {
        unsigned JOFFSET2  : 12;    // Data offset for injected channel 2
        unsigned reserved  : 20;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_jofr2;
typedef union { // t_register_stm32l1xx_adc_jofr3                ADC injected channel data offset register x (->RM0038 p.257)
    volatile t_u32 All;
    struct {
        unsigned JOFFSET3  : 12;    // Data offset for injected channel 3
        unsigned reserved  : 20;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_jofr3;
typedef union { // t_register_stm32l1xx_adc_jofr4                ADC injected channel data offset register x (->RM0038 p.257)
    volatile t_u32 All;
    struct {
        unsigned JOFFSET4  : 12;    // Data offset for injected channel 4
        unsigned reserved  : 20;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_jofr4;
typedef union { // t_register_stm32l1xx_adc_htr                ADC watchdog high threshold register (->RM0038 p.257)
    volatile t_u32 All;
    struct {
        unsigned HT        : 12;     // Channel 0 Sample time selection
        unsigned reserved  : 20;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_htr;
typedef union { // t_register_stm32l1xx_adc_ltr                ADC watchdog low threshold register (->RM0038 p.257)
    volatile t_u32 All;
    struct {
        unsigned LT        : 12;     // Channel 0 Sample time selection
        unsigned reserved  : 20;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_ltr;
typedef union { // t_register_stm32l1xx_adc_sqr1                ADC regular sequence register 1 (->RM0038 p.258)
    volatile t_u32 All;
    struct {
        unsigned SQ25      : 5;     // 25th conversion in regular sequence
        unsigned SQ26      : 5;     // 26th conversion in regular sequence
        unsigned SQ27      : 5;     // 27th conversion in regular sequence
        unsigned SQ28      : 5;     // 28th conversion in regular sequence
        unsigned L         : 5;     // Regular channel sequence length
        unsigned reserved  : 7;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_sqr1;
typedef union { // t_register_stm32l1xx_adc_sqr2                ADC regular sequence register 2 (->RM0038 p.258)
    volatile t_u32 All;
    struct {
        unsigned SQ19       : 5;     // 19th conversion in regular sequence
        unsigned SQ20       : 5;     // 20th conversion in regular sequence
        unsigned SQ21       : 5;     // 21th conversion in regular sequence
        unsigned SQ22       : 5;     // 22th conversion in regular sequence
        unsigned SQ23       : 5;     // 23th conversion in regular sequence
        unsigned SQ24       : 5;     // 24th conversion in regular sequence
        unsigned reserved   : 2;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_sqr2;
typedef union { // t_register_stm32l1xx_adc_sqr3                ADC regular sequence register 3 (->RM0038 p.259)
    volatile t_u32 All;
    struct {
        unsigned SQ13       : 5;     // 13st conversion in regular sequence
        unsigned SQ14       : 5;     // 14nd conversion in regular sequence
        unsigned SQ15       : 5;     // 15rd conversion in regular sequence
        unsigned SQ16       : 5;     // 16th conversion in regular sequence
        unsigned SQ17       : 5;     // 17th conversion in regular sequence
        unsigned SQ18       : 5;     // 18th conversion in regular sequence
        unsigned reserved   : 2;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_sqr3;
typedef union { // t_register_stm32l1xx_adc_sqr4                ADC regular sequence register 4 (->RM0038 p.260)
    volatile t_u32 All;
    struct {
        unsigned SQ7       : 5;     // 7st conversion in regular sequence
        unsigned SQ8       : 5;     // 8nd conversion in regular sequence
        unsigned SQ9       : 5;     // 9rd conversion in regular sequence
        unsigned SQ10      : 5;     // 10th conversion in regular sequence
        unsigned SQ11      : 5;     // 11th conversion in regular sequence
        unsigned SQ12      : 5;     // 12th conversion in regular sequence
        unsigned reserved  : 2;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_sqr4;
typedef union { // t_register_stm32l1xx_adc_sqr5                ADC regular sequence register 5 (->RM0038 p.260)
    volatile t_u32 All;
    struct {
        unsigned SQ1       : 5;     // 1st conversion in regular sequence
        unsigned SQ2       : 5;     // 2nd conversion in regular sequence
        unsigned SQ3       : 5;     // 3rd conversion in regular sequence
        unsigned SQ4       : 5;     // 4th conversion in regular sequence
        unsigned SQ5       : 5;     // 5th conversion in regular sequence
        unsigned SQ6       : 5;     // 6th conversion in regular sequence
        unsigned reserved  : 2;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_sqr5;
typedef union { // t_register_stm32l1xx_adc_jsqr                ADC injected sequence register (->RM0038 p.261)
    volatile t_u32 All;
    struct {
        unsigned JSQ1       : 5;     // 1st conversion in injected sequence
        unsigned JSQ2       : 5;     // 2nd conversion in injected sequence
        unsigned JSQ3       : 5;     // 3rd conversion in injected sequence
        unsigned JSQ4       : 5;     // 4th conversion in injected sequence
        unsigned JL         : 2;     // Injected sequence length
        unsigned reserved   : 10;

    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_jsqr;
typedef union { // t_register_stm32l1xx_adc_jdr1                ADC injected data register 1 (->RM0038 p.261)
    volatile t_u32 All;
    struct {
        unsigned JDATA      : 16;     // Injected data
        unsigned reserved   : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_jdr1;
typedef union { // t_register_stm32l1xx_adc_jdr2                ADC injected data register 2 (->RM0038 p.261)
    volatile t_u32 All;
    struct {
        unsigned JDATA      : 16;     // Injected data
        unsigned reserved   : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_jdr2;
typedef union { // t_register_stm32l1xx_adc_jdr3                ADC injected data register 3 (->RM0038 p.261)
    volatile t_u32 All;
    struct {
        unsigned JDATA      : 16;     // Injected data
        unsigned reserved   : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_jdr3;
typedef union { // t_register_stm32l1xx_adc_jdr4                ADC injected data register 4 (->RM0038 p.261)
    volatile t_u32 All;
    struct {
        unsigned JDATA      : 16;     // Injected data
        unsigned reserved   : 16;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_jdr4;
typedef union { // t_register_stm32l1xx_adc_dr                ADC regular data register (->RM0038 p.262)
    volatile t_u32 All;
    struct {
        unsigned DATA       : 16;     // ADC2 data
        unsigned reserved   : 16;     //
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_dr;
typedef union { // t_register_stm32l1xx_adc_smpr0             ADC sample time register 0 (->RM0038 p.262)
    volatile t_u32 All;
    struct {
        unsigned SMP30      : 3;      // Channel 30 Sample time selection
        unsigned SMP31      : 3;      // Channel 31 Sample time selection
        unsigned reserved   : 26;     //
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc_smpr0;
typedef struct {
    volatile t_register_stm32l1xx_adc_sr    SR;
    volatile t_register_stm32l1xx_adc_cr1   CR1;
    volatile t_register_stm32l1xx_adc_cr2   CR2;
    volatile t_register_stm32l1xx_adc_smpr1 SMPR1;
    volatile t_register_stm32l1xx_adc_smpr2 SMPR2;
    volatile t_register_stm32l1xx_adc_smpr3 SMPR3;
    volatile t_register_stm32l1xx_adc_jofr1 JOFR1;
    volatile t_register_stm32l1xx_adc_jofr2 JOFR2;
    volatile t_register_stm32l1xx_adc_jofr3 JOFR3;
    volatile t_register_stm32l1xx_adc_jofr4 JOFR4;
    volatile t_register_stm32l1xx_adc_htr   HTR;
    volatile t_register_stm32l1xx_adc_ltr   LTR;
    volatile t_register_stm32l1xx_adc_sqr1  SQR1;
    volatile t_register_stm32l1xx_adc_sqr2  SQR2;
    volatile t_register_stm32l1xx_adc_sqr3  SQR3;
    volatile t_register_stm32l1xx_adc_sqr4  SQR4;
    volatile t_register_stm32l1xx_adc_sqr5  SQR5;
    volatile t_register_stm32l1xx_adc_jsqr  JSQR;
    volatile t_register_stm32l1xx_adc_jdr1  JDR1;
    volatile t_register_stm32l1xx_adc_jdr2  JDR2;
    volatile t_register_stm32l1xx_adc_jdr3  JDR3;
    volatile t_register_stm32l1xx_adc_jdr4  JDR4;
    volatile t_register_stm32l1xx_adc_dr    DR;
    volatile t_register_stm32l1xx_adc_smpr0 SMPR0;
}  __attribute__( ( __packed__ ) ) t_register_stm32l1xx_adc;
typedef struct {

    volatile t_u32 CCR;
    volatile t_u32 CNDTR;
    volatile t_u32 CPAR;
    volatile t_u32 CMAR;

}  __attribute__( ( __packed__ ) ) t_register_stm32l1xx_dma_channel;
typedef struct {

    volatile t_u32 ISR;
    volatile t_u32 IFCR;

}  __attribute__( ( __packed__ ) ) t_register_stm32l1xx_dma;
typedef struct {

    volatile t_register_stm32l1xx_timx_cr1                      CR1;    //
    volatile t_u16                           reserved1;
    volatile t_register_stm32l1xx_timx_cr2                      CR2;    //
    volatile t_u16                           reserved2;
    volatile t_register_stm32l1xx_timx_smcr                     SMCR;   //
    volatile t_u16                           reserved3;
    volatile t_register_stm32l1xx_timx_dier                     DIER;   //
    volatile t_u16                           reserved4;
    volatile t_register_stm32l1xx_timx_sr                       SR;     //
    volatile t_u16                           reserved5;
    volatile t_register_stm32l1xx_timx_egr                      EGR;     //
    volatile t_u16                           reserved6;
    volatile t_timx_ccmr1_input_capture_mode CCMR1;  //
    volatile t_u16                           reserved7;
    volatile t_timx_ccmr2_input_capture_mode CCMR2;  //
    volatile t_u16                           reserved8;
    volatile t_register_stm32l1xx_timx_ccer                     CCER;   //
    volatile t_u16                           reserved9;
    volatile t_register_stm32l1xx_timx_cnt                      CNT;    // counter
    volatile t_u16                           reserved10;
    volatile t_register_stm32l1xx_timx_psc                      PSC;    // prescalar
    volatile t_u16                           reserved11;
    volatile t_register_stm32l1xx_timx_arr                      ARR;    // auto-reload reg
    volatile t_u16                           reserved12;
    volatile t_reserved_reg                  XXX;    // reserved
    volatile t_u16                           reserved13;
    volatile t_register_stm32l1xx_timx_ccr1                     CCR1;   //
    volatile t_u16                           reserved14;
    volatile t_register_stm32l1xx_timx_ccr2                     CCR2;   //
    volatile t_u16                           reserved15;
    volatile t_register_stm32l1xx_timx_ccr3                     CCR3;   //
    volatile t_u16                           reserved16;
    volatile t_register_stm32l1xx_timx_ccr4                     CCR4;   //
    volatile t_u16                           reserved17;
    volatile t_reserved_reg                  YYY;    // reserved
    volatile t_u16                           reserved18;
    volatile t_register_stm32l1xx_timx_dcr                      DCR;    //
    volatile t_u16                           reserved19;
    volatile t_register_stm32l1xx_timx_dmar                     DMAR;   //
    volatile t_u16                           reserved20;
    volatile t_tim2_or                       TIMx_OR;
    volatile t_u16                           reserved21;

}  __attribute__( ( __packed__ ) ) t_register_stm32l1xx_timer;
typedef union { // t_register_stm32l1xx_ri_ascr1  - RI analog switches control register (RI_ASCR1)  (->RM0038 p.154)
    // The RI_ASCR1 register is used to configure the analog switches of the I/Os linked to the
    // ADC. These I/Os are pointed to by the ADC channel number.

    volatile t_u32 All;
    struct {
        unsigned C_H0_G_R1_1    : 1; // Bit 0    =0: Analog switch open; =1: Analog switch closed
        unsigned C_H1_G_R1_2    : 1; // Bit 1    =0: Analog switch open; =1: Analog switch closed
        unsigned C_H2_G_R1_3    : 1; // Bit 2    =0: Analog switch open; =1: Analog switch closed
        unsigned C_H3_G_R1_4    : 1; // Bit 3    =0: Analog switch open; =1: Analog switch closed
        unsigned C_H31_G_R11_51 : 1; // Bit 4   : Analog switch control
        // This bit is set and cleared by software to control the analog switches. 0: Analog switch open 1: Analog switch closed
        unsigned C_OM_P1_S_W1   : 1; // Bit 5   : Comparator 1 analog switch
        // This bit is set and cleared by software to control the core analog switch connecting the positive input of the COMP1 comparator. It can be used to route the ADC matrix or OPAMP3 output to the comparator1 positive input. See Figure 60 on page 274 . 0: Analog switch open 1: Analog switch closed
        unsigned C_H8_G_R2_1    : 1; // Bit 6    =0: Analog switch open; =1: Analog switch closed
        unsigned C_H9_G_R2_2    : 1; // Bit 7    =0: Analog switch open; =1: Analog switch closed
        unsigned C_H8_G_R3_1    : 1; // Bit 8    =0: Analog switch open; =1: Analog switch closed
        unsigned C_H9_G_R3_2    : 1; // Bit 9    =0: Analog switch open; =1: Analog switch closed
        unsigned C_H10_G_R8_1   : 1; // Bit 10   =0: Analog switch open; =1: Analog switch closed
        unsigned C_H11_G_R8_2   : 1; // Bit 11   =0: Analog switch open; =1: Analog switch closed
        unsigned C_H12_G_R8_3   : 1; // Bit 12   =0: Analog switch open; =1: Analog switch closed
        unsigned C_H13_G_R8_4   : 1; // Bit 13   =0: Analog switch open; =1: Analog switch closed
        unsigned C_H14_G_R9_1   : 1; // Bit 14   =0: Analog switch open; =1: Analog switch closed
        unsigned C_H15_G_R9_2   : 1; // Bit 15   =0: Analog switch open; =1: Analog switch closed
        unsigned C_H31_G_R11_52 : 1; // Bit 16   =0: Analog switch open; =1: Analog switch closed
        // Note: This bit is available in high density devices only
        unsigned reserved       : 1; // Bit 17
        unsigned C_H18_G_R7_1   : 1; // Bit 18   =0: Analog switch open; =1: Analog switch closed
        unsigned C_H19_G_R7_2   : 1; // Bit 19   =0: Analog switch open; =1: Analog switch closed
        unsigned C_H20_G_R7_3   : 1; // Bit 20   =0: Analog switch open; =1: Analog switch closed
        unsigned C_H21_G_R7_4   : 1; // Bit 21   =0: Analog switch open; =1: Analog switch closed
        unsigned C_H22          : 1; // Bit 22   =0: Analog switch open; =1: Analog switch closed
        unsigned C_H23          : 1; // Bit 23   =0: Analog switch open; =1: Analog switch closed
        unsigned C_H24          : 1; // Bit 24   =0: Analog switch open; =1: Analog switch closed
        unsigned C_H25          : 1; // Bit 25   =0: Analog switch open; =1: Analog switch closed
        unsigned VC_OM_P        : 1; // Bit 26  : ADC analog switch selection for internal node to comparator 1
        // This bit is set and cleared by software to control the VCOMP ADC analog switch. See Figure 59 on page 273 . 0: Analog switch open 1: Analog switch closed
        unsigned C_H27_G_R11_1  : 1; // Bit 27   =0: Analog switch open; =1: Analog switch closed
        // Note: This bit is available in high density devices only
        unsigned C_H28_G_R11_2  : 1; // Bit 28   =0: Analog switch open; =1: Analog switch closed
        // Note: This bit is available in high density devices only
        unsigned C_H29_G_R11_3  : 1; // Bit 29   =0: Analog switch open; =1: Analog switch closed
        // Note: This bit is available in high density devices only
        unsigned C_H30_G_R11_4  : 1; // Bit 30   =0: Analog switch open; =1: Analog switch closed
        // Note: This bit is available in high density devices only
        unsigned SC_M           : 1; // Bit 31  : Switch control mode
        // This bit is set and cleared by software to control the analog ADC switches according to the state of the analog I/O switches controlled by bits [25:18] and [15:0]. 0: ADC analog switches open or controlled by the ADC interface 1: ADC analog switches closed if the corresponding I/O switch is also closed
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_ascr1;
typedef union { // t_register_stm32l1xx_ri_ascr2  - RI analog switch control register 2 (RI_ASCR2)  (->RM0038 p.156)
    // The RI_ASCR2 register is used to configure the analog switches of groups of I/Os not linked
    // to the ADC. In this way, predefined groups of I/Os can be connected together.

    volatile t_u32 All;
    struct {
        unsigned G_R10_1   : 1; // Bit 0    =0: Analog Switch open; =1: closed
        unsigned G_R10_2   : 1; // Bit 1    =0: Analog Switch open; =1: closed
        unsigned G_R10_3   : 1; // Bit 2    =0: Analog Switch open; =1: closed
        unsigned G_R10_4   : 1; // Bit 3    =0: Analog Switch open; =1: closed
        unsigned G_R6_1    : 1; // Bit 4    =0: Analog Switch open; =1: closed
        unsigned G_R6_2    : 1; // Bit 5    =0: Analog Switch open; =1: closed
        unsigned G_R5_1    : 1; // Bit 6    =0: Analog Switch open; =1: closed
        unsigned G_R5_2    : 1; // Bit 7    =0: Analog Switch open; =1: closed
        unsigned G_R5_3    : 1; // Bit 8    =0: Analog Switch open; =1: closed
        unsigned G_R4_1    : 1; // Bit 9    =0: Analog Switch open; =1: closed
        unsigned G_R4_2    : 1; // Bit 10   =0: Analog Switch open; =1: closed
        unsigned G_R4_3    : 1; // Bit 11   =0: Analog Switch open; =1: closed
        unsigned reserved1 : 4; // Bit 12  , must be kept at reset value
        unsigned G_R3_3    : 1; // Bit 16   =0: Analog Switch open; =1: closed
        unsigned G_R3_4    : 1; // Bit 17   =0: Analog Switch open; =1: closed
        unsigned G_R3_5    : 1; // Bit 18   =0: Analog Switch open; =1: closed
        unsigned G_R9_3    : 1; // Bit 19   =0: Analog Switch open; =1: closed
        unsigned G_R9_4    : 1; // Bit 20   =0: Analog Switch open; =1: closed
        unsigned G_R2_3    : 1; // Bit 21   =0: Analog Switch open; =1: closed
        unsigned G_R2_4    : 1; // Bit 22   =0: Analog Switch open; =1: closed
        unsigned G_R2_5    : 1; // Bit 23   =0: Analog Switch open; =1: closed
        unsigned G_R7_5    : 1; // Bit 24   =0: Analog Switch open; =1: closed
        unsigned G_R7_6    : 1; // Bit 25   =0: Analog Switch open; =1: closed
        unsigned G_R7_7    : 1; // Bit 26   =0: Analog Switch open; =1: closed
        unsigned G_R6_3    : 1; // Bit 27   =0: Analog Switch open; =1: closed (Bit available in high density devices only!)
        unsigned G_R6_4    : 1; // Bit 28   =0: Analog Switch open; =1: closed (Bit available in high density devices only!)
        unsigned reserved2 : 3; // Bit 29  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_ascr2;
typedef union { // t_register_stm32l1xx_ri_asmr1  - Analog switch mode register (RI_ASMR1)  (->RM0038 p.159)
    // The RI_ASMR1 register is available in high density devices only and is used to select if
    // analog switches of port A are to be controlled by the timer OC or through the ADC interface
    // or RI_ASCRx registers.

    volatile t_u32 All;
    struct {
        unsigned PA       : 16; // Bit 0   [15:0]: Port A analog switch mode selection
        // These bits are set and cleared by software to select the mode of controlling the analog switches for Port A. 0: ADC interface or RI_ASCRx controlled 1: Timer controlled
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_asmr1;
typedef union { // t_register_stm32l1xx_ri_asmr2  - Analog switch mode register (RI_ASMR2)  (->RM0038 p.160)
    // The RI_ASMR2 register is available in high density devices only and is used to select if
    // analog switches of port B are to be controlled by the timer OC or through the ADC interface
    // or RI_ASCRx registers.

    volatile t_u32 All;
    struct {
        unsigned PB       : 16; // Bit 0   [15:0]: Port B analog switch mode selection
        // These bits are set and cleared by software to select the mode of controlling the analog switches for Port B. 0: ADC interface or RI_ASCRx controlled 1: Timer controlled
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_asmr2;
typedef union { // t_register_stm32l1xx_ri_asmr3  - Analog switch mode register (RI_ASMR3)  (->RM0038 p.162)
    // The RI_ASMR3 register is available in high density devices only and is used to select if
    // analog switches of port C are to be controlled by the timer OC or through the ADC interface
    // or RI_ASCRx registers.

    volatile t_u32 All;
    struct {
        unsigned PC       : 16; // Bit 0   [15:0]: Port C analog switch mode selection
        // These bits are set and cleared by software to select the mode of controlling the analog switches for Port C. 0: ADC interface or RI_ASCRx controlled 1: Timer controlled
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_asmr3;
typedef union { // t_register_stm32l1xx_ri_asmr4  - Analog switch mode register (RI_ASMR4)  (->RM0038 p.163)
    // The RI_ASMR4 register is available in high density devices only and is used to select if
    // analog switches of port F are to be controlled by the timer OC or through the ADC interface
    // or RI_ASCRx registers.

    volatile t_u32 All;
    struct {
        unsigned PF       : 16; // Bit 0   [15:0]: Port F analog switch mode selection
        // These bits are set and cleared by software to select the mode of controlling the analog switches for Port F. 0: ADC interface or RI_ASCRx controlled 1: Timer controlled Note: These bits are available in high density devices only.
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_asmr4;
typedef union { // t_register_stm32l1xx_ri_asmr5  - Analog switch mode register (RI_ASMR5) (->RM0038 p.165)
    // The RI_ASMR5 register is available in high density devices only and is used to select if
    // analog switches of port G are to be controlled by the timer OC or through the ADC interface
    // or RI_ASCRx registers.

    volatile t_u32 All;
    struct {
        unsigned PG       : 16; // Bit 0   [15:0]: Port G analog switch mode selection
        // These bits are set and cleared by software to select the mode of controlling the analog switches for Port G. 0: ADC interface or RI_ASCRx controlled 1: Timer controlled Note: These bits are available in high density devices only.
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_asmr5;
typedef union { // t_register_stm32l1xx_ri_cicr1  - Channel identification for capture register (RI_CICR1)  (->RM0038 p.160)
    // The RI_CICR1 register is available in high density devices only and is used when analog
    // switches are controlled by a timer OC. RI_CICR1 allows a channel to be identifed for timer
    // input capture.

    volatile t_u32 All;
    struct {
        unsigned PA       : 16; // Bit 0   [15:0]: Port A channel identification for capture
        // These bits are set and cleared by software to identify the sampling capacitor I/Os on Port A. 0: Channel I/O 1: Sampling capacitor I/O
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_cicr1;
typedef union { // t_register_stm32l1xx_ri_cicr2  - Channel identification for capture register (RI_CICR2)  (->RM0038 p.161)
    // The RI_CICR2 register is available in high density devices only and is used when analog
    // switches are controlled by a timer OC. RI_CICR2 allows a port B channel to be identifed for
    // timer input capture.

    volatile t_u32 All;
    struct {
        unsigned PB       : 16; // Bit 0   [15:0]: Port B channel identification for capture
        // These bits are set and cleared by software to identify the sampling capacitor I/Os on Port B. 0: Channel I/O 1: Sampling capacitor I/O
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_cicr2;
typedef union { // t_register_stm32l1xx_ri_cicr3  - Channel identification for capture register (RI_CICR3)  (->RM0038 p.163)
    // The RI_CICR3 register is available in high density devices only and is used when analog
    // switches are controlled by a timer OC. RI_CICR3 allows a port C channel to be identifed for
    // timer input capture.

    volatile t_u32 All;
    struct {
        unsigned PC       : 16; // Bit 0   [15:0]: Port C channel identification for capture
        // These bits are set and cleared by software to identify the sampling capacitor I/Os on Port C. 0: Channel I/O 1: Sampling capacitor I/O
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_cicr3;
typedef union { // t_register_stm32l1xx_ri_cicr4  - Channel identification for capture register (RI_CICR4) (->RM0038 p.164)
    // The RI_CICR4 register is available in high density devices only and is used when analog
    // switches are controlled by a timer OC. RI_CICR4 allows a port F channel to be identifed for
    // timer input capture.

    volatile t_u32 All;
    struct {
        unsigned PF       : 16; // Bit 0   [15:0]: Port F channel identification for capture
        // These bits are set and cleared by software to identify the sampling capacitor I/Os on Port F. 0: Channel I/O 1: Sampling capacitor I/O Note: These bits are available in high density devices only.
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_cicr4;
typedef union { // t_register_stm32l1xx_ri_cicr5  - Channel identification for capture register (RI_CICR5)
    // The RI_CICR5 register is available in high density devices only and is used when analog
    // switches are controlled by a timer OC. RI_CICR5 allows a port G channel to be identifed for
    // timer input capture.

    volatile t_u32 All;
    struct {
        unsigned PG       : 16; // Bit 0   [15:0]: Port G channel identification for capture
        // These bits are set and cleared by software to identify the sampling capacitor I/Os on Port G. 0: Channel I/O 1: Sampling capacitor I/O Note: These bits are available in high density devices only.
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_cicr5;
typedef union { // t_register_stm32l1xx_ri_cmr1   - Channel mask register (RI_CMR1)  (->RM0038 p.159)
    // RI_CMR1 is available in high density devices only and is used to mask a port A channel
    // designated as a timer input capture (after acquisition completion to avoid triggering multiple
    // detections).

    volatile t_u32 All;
    struct {
        unsigned PA       : 16; // Bit 0   [15:0]: Port A channel masking
        // These bits are set and cleared by software to mask the ZI input of Port A. 0: Masked 1: Not masked
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_cmr1;
typedef union { // t_register_stm32l1xx_ri_cmr2   - Channel mask register (RI_CMR2)  (->RM0038 p.161)
    // RI_CMR2 is available in high density devices only and is used to mask a por B channel
    // designated as a timer input capture (after acquisition completion to avoid triggering multiple
    // detections)

    volatile t_u32 All;
    struct {
        unsigned PB       : 16; // Bit 0   [15:0]: Port B channel masking
        // These bits are set and cleared by software to mask ZI input of Port B. 0: Masked 1: Not masked
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_cmr2;
typedef union { // t_register_stm32l1xx_ri_cmr3   - Channel mask register (RI_CMR3)  (->RM0038 p.162)
    // RI_CMR3 is available in high density devices only and is used to mask a port C channel
    // designated as a timer input capture (after acquisition completion to avoid triggering multiple
    // detections)

    volatile t_u32 All;
    struct {
        unsigned PC       : 16; // Bit 0   [15:0]: Port C channel masking
        // These bits are set and cleared by software to mask ZI input of Port C. 0: Masked 1: Not masked
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_cmr3;
typedef union { // t_register_stm32l1xx_ri_cmr4   - Channel mask register (RI_CMR4)  (->RM0038 p.164)
    // RI_CMR4 is available in high density devices only and is used to mask a port F channel
    // designated as a timer input capture (after acquisition completion to avoid triggering multiple
    // detections).

    volatile t_u32 All;
    struct {
        unsigned PF       : 16; // Bit 0   [15:0]: Port F channel masking
        // These bits are set and cleared by software to mask ZI input of Port F. 0: Masked 1: Not masked Note: These bits are available in high density devices only.
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_cmr4;
typedef union { // t_register_stm32l1xx_ri_cmr5   - Channel mask register (RI_CMR5) (->RM0038 p.165)
    // RI_CMR5 is available in high density devices only and is used to mask a port G channel
    // designated as a timer input capture (after acquisition completion to avoid triggering multiple
    // detections).

    volatile t_u32 All;
    struct {
        unsigned PG       : 16; // Bit 0   [15:0]: Port G channel masking
        // These bits are set and cleared by software to mask ZI input of Port G. 0: Not masked 1: Masked Note: These bits are available in high density devices only.
        unsigned reserved : 16; // Bit 16  , must be kept at reset value
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_cmr5;
typedef union { // t_register_stm32l1xx_ri_hyscr1 - RI hysteresis control register (RI_HYSCR1)  (->RM0038 p.157)
    // The RI_HYSCR1 register is used to enable/disable the hysteresis of the input Schmitt
    // trigger of ports A and B.

    volatile t_u32 All;
    struct {
        unsigned PA : 16; // Bit 0   [15:0]: Port A hysteresis control on/off
        // These bits are set and cleared by software to control the Schmitt trigger hysteresis of the Port A[15:0]. 0: Hysteresis on 1: Hysteresis off
        unsigned PB : 16; // Bit 16  [15:0]: Port B hysteresis control on/off
        // These bits are set and cleared by software to control the Schmitt trigger hysteresis of the Port B[15:0]. 0: Hysteresis on 1: Hysteresis off
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_hyscr1;
typedef union { // t_register_stm32l1xx_ri_hyscr2 - RI Hysteresis control register (RI_HYSCR2)  (->RM0038 p.157)
    // RI_HYSCR2 register allows to enable/disable hysteresis of input Schmitt trigger of ports C
    // and D.

    volatile t_u32 All;
    struct {
        unsigned PC : 16; // Bit 0   [15:0]: Port C hysteresis control on/off
        // These bits are set and cleared by software to control the Schmitt trigger hysteresis of the Port C[15:0]. 0: Hysteresis on 1: Hysteresis off
        unsigned PD : 16; // Bit 16  [15:0]: Port D hysteresis control on/off
        // These bits are set and cleared by software to control the Schmitt trigger hysteresis of the Port D[15:0]. 0: Hysteresis on 1: Hysteresis off
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_hyscr2;
typedef union { // t_register_stm32l1xx_ri_hyscr3 - RI Hysteresis control register (RI_HYSCR3)  (->RM0038 p.158)
    // The RI_HYSCR3 register is used to enable/disable the hysteresis of the input Schmitt
    // trigger of the entire port E and F.

    volatile t_u32 All;
    struct {
        unsigned PE : 16; // Bit 0   [15:0]: Port E hysteresis control on/off
        // These bits are set and cleared by software to control the Schmitt trigger hysteresis of the Port E[15:0]. 0: Hysteresis on 1: Hysteresis off
        unsigned PF : 16; // Bit 16  [15:0]: Port F hysteresis control on/off
        // These bits are set and cleared by software to control the Schmitt trigger hysteresis of the Port F[15:0]. 0: Hysteresis on 1: Hysteresis off Note: These bits are available in high density devices only.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_hyscr3;
typedef union { // t_register_stm32l1xx_ri_hyscr4 - RI Hysteresis control register (RI_HYSCR4)  (->RM0038 p.158)
    // The RI_HYSCR4 register is used to enable/disable the hysteresis of the input Schmitt
    // trigger of the entire port G.

    volatile t_u32 All;
    struct {
        unsigned PE : 16; // Bit 0   [15:0]: Port E hysteresis control on/off
        // These bits are set and cleared by software to control the Schmitt trigger hysteresis of the Port E[15:0]. 0: Hysteresis on 1: Hysteresis off
        unsigned PF : 16; // Bit 16  [15:0]: Port F hysteresis control on/off
        // These bits are set and cleared by software to control the Schmitt trigger hysteresis of the Port F[15:0]. 0: Hysteresis on 1: Hysteresis off Note: These bits are available in high density devices only.
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_ri_hyscr4;
// InsertStructureDefinitons  above (DO NOT DELETE THIS LINE!)

#define t_register_stm32l1xx_all void

// t_ttc_register_architecture is required by ttc_register_types.h
#define t_ttc_register_architecture volatile t_register_stm32l1xx_all

// } Structures/ Enums


#endif // REGISTER_STM32L1XX_TYPES_H
