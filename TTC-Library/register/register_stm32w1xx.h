#ifndef REGISTER_STM32W1XX_H
#define REGISTER_STM32W1XX_H

/** { register_stm32w1xx.h ***********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for register devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by high-level register and application.
 *
 *  Created from template device_architecture.h revision 21 at 20140223 10:54:39 UTC
 *
 *  Note: See ttc_register.h for description of stm32w1xx independent REGISTER implementation.
 *
 *  Authors: Greg Knoll, Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_REGISTER_STM32W1XX
//
// Implementation status of low-level driver support for register devices on stm32w1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_REGISTER_STM32W1XX 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_REGISTER_STM32W1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_REGISTER_STM32W1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_REGISTER_STM32W1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "../basic/basic_cm3.h"
#include "../register/register_cortexm3.h"
#include "register_stm32w1xx_types.h"
#include "../ttc_register_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_register_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_<device>_prepare
//
#define ttc_driver_register_get_configuration() register_stm32w1xx_get_configuration()
#define ttc_driver_register_prepare() register_stm32w1xx_prepare()
#define ttc_driver_register_reset(Register) register_stm32w1xx_reset(Register)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
// Register Variable Declarations ***************************************{

extern volatile t_u32                                    register_stm32w1xx_PERIPHERAL_DISABLE;
extern volatile t_u32                                    register_stm32w1xx_TIM1_INT;
extern volatile t_u32                                    register_stm32w1xx_TIM2_INT;
extern volatile t_u32                                    register_stm32w1xx_INT_ADCFLAG;
extern volatile t_u32                                    register_stm32w1xx_INT_TIM1MISS;
extern volatile t_u32                                    register_stm32w1xx_INT_TIM2MISS;
extern volatile t_u32                                    register_stm32w1xx_INT_TIM1CFG;
extern volatile t_u32                                    register_stm32w1xx_INT_TIM2CFG;
extern volatile t_u32                                    register_stm32w1xx_INT_ADCCFG;
extern volatile t_u32                                    register_stm32w1xx_ADC_DATA;
extern volatile t_u32                                    register_stm32w1xx_ADC_CFG;
extern volatile t_u32                                    register_stm32w1xx_ADC_OFFSET;
extern volatile t_u32                                    register_stm32w1xx_ADC_GAIN;
extern volatile t_u32                                    register_stm32w1xx_ADC_DMACFG;
extern volatile t_u32                                    register_stm32w1xx_ADC_DMASTAT;
extern volatile t_u32                                    register_stm32w1xx_ADC_DMABEG;
extern volatile t_u32                                    register_stm32w1xx_ADC_DMASIZE;
extern volatile t_u32                                    register_stm32w1xx_ADC_DMACUR;
extern volatile t_u32                                    register_stm32w1xx_ADC_DMACNT;
extern volatile t_u32                                    register_stm32w1xx_TIM1_CR1;
extern volatile t_u32                                    register_stm32w1xx_TIM1_CR2;
extern volatile t_u32                                    register_stm32w1xx_TIM1_SMCR;
extern volatile t_u32                                    register_stm32w1xx_TIM1_EGR;
extern volatile t_u32                                    register_stm32w1xx_TIM1_CCMR1;
extern volatile t_u32                                    register_stm32w1xx_TIM1_CCMR2;
extern volatile t_u32                                    register_stm32w1xx_TIM1_CCER;
extern volatile t_u32                                    register_stm32w1xx_TIM1_CNT;
extern volatile t_u32                                    register_stm32w1xx_TIM1_PSC;
extern volatile t_u32                                    register_stm32w1xx_TIM1_ARR;
extern volatile t_u32                                    register_stm32w1xx_TIM1_CCR1;
extern volatile t_u32                                    register_stm32w1xx_TIM1_CCR2;
extern volatile t_u32                                    register_stm32w1xx_TIM1_CCR3;
extern volatile t_u32                                    register_stm32w1xx_TIM1_CCR4;
extern volatile t_u32                                    register_stm32w1xx_TIM1_OR;
extern volatile t_u32                                    register_stm32w1xx_TIM2_CR1;
extern volatile t_u32                                    register_stm32w1xx_TIM2_CR2;
extern volatile t_u32                                    register_stm32w1xx_TIM2_SMCR;
extern volatile t_u32                                    register_stm32w1xx_TIM2_EGR;
extern volatile t_u32                                    register_stm32w1xx_TIM2_CCMR1;
extern volatile t_u32                                    register_stm32w1xx_TIM2_CCMR2;
extern volatile t_u32                                    register_stm32w1xx_TIM2_CCER;
extern volatile t_u32                                    register_stm32w1xx_TIM2_CNT;
extern volatile t_u32                                    register_stm32w1xx_TIM2_PSC;
extern volatile t_u32                                    register_stm32w1xx_TIM2_ARR;
extern volatile t_u32                                    register_stm32w1xx_TIM2_CCR1;
extern volatile t_u32                                    register_stm32w1xx_TIM2_CCR2;
extern volatile t_u32                                    register_stm32w1xx_TIM2_CCR3;
extern volatile t_u32                                    register_stm32w1xx_TIM2_CCR4;
extern volatile t_u32                                    register_stm32w1xx_TIM2_OR;
extern volatile t_register_stm32w1xx_gpiox               register_stm32w1xx_GPIOA;
extern   volatile t_register_stm32w1xx_gpio_pxcfgl       register_stm32w1xx_GPIOA_CFGL;
extern   volatile t_register_stm32w1xx_gpio_pxcfgh       register_stm32w1xx_GPIOA_CFGH;
extern   volatile t_register_stm32w1xx_gpio_pxin         register_stm32w1xx_GPIOA_IN;
extern   volatile t_register_stm32w1xx_gpio_pxout        register_stm32w1xx_GPIOA_OUT;
extern   volatile t_register_stm32w1xx_gpio_pxset        register_stm32w1xx_GPIOA_SET;
extern   volatile t_register_stm32w1xx_gpio_pxclr        register_stm32w1xx_GPIOA_CLR;
extern volatile t_register_stm32w1xx_gpiox               register_stm32w1xx_GPIOB;
extern   volatile t_register_stm32w1xx_gpio_pxcfgl       register_stm32w1xx_GPIOOB_CFGL;
extern   volatile t_register_stm32w1xx_gpio_pxcfgh       register_stm32w1xx_GPIOOB_CFGH;
extern   volatile t_register_stm32w1xx_gpio_pxin         register_stm32w1xx_GPIOOB_IN;
extern   volatile t_register_stm32w1xx_gpio_pxout        register_stm32w1xx_GPIOOB_OUT;
extern   volatile t_register_stm32w1xx_gpio_pxset        register_stm32w1xx_GPIOOB_SET;
extern   volatile t_register_stm32w1xx_gpio_pxclr        register_stm32w1xx_GPIOOB_CLR;
extern volatile t_register_stm32w1xx_gpiox               register_stm32w1xx_GPIOC;
extern   volatile t_register_stm32w1xx_gpio_pxcfgl       register_stm32w1xx_GPIOC_CFGL;
extern   volatile t_register_stm32w1xx_gpio_pxcfgh       register_stm32w1xx_GPIOC_CFGH;
extern   volatile t_register_stm32w1xx_gpio_pxin         register_stm32w1xx_GPIOC_IN;
extern   volatile t_register_stm32w1xx_gpio_pxout        register_stm32w1xx_GPIOC_OUT;
extern   volatile t_register_stm32w1xx_gpio_pxset        register_stm32w1xx_GPIOC_SET;
extern   volatile t_register_stm32w1xx_gpio_pxclr        register_stm32w1xx_GPIOC_CLR;
extern volatile t_register_stm32w1xx_gpio_debug_wake_irq register_stm32w1xx_GPIO_DB_WAKE_IRQ;
extern   volatile t_register_stm32w1xx_gpio_dbgcfg       register_stm32w1xx_GPIO_DBGCFG;
extern   volatile t_register_stm32w1xx_gpio_dbgstat      register_stm32w1xx_GPIO_DBGSTAT;
extern   volatile t_register_stm32w1xx_gpio_pxwake       register_stm32w1xx_GPIO_AWAKE;
extern   volatile t_register_stm32w1xx_gpio_pxwake       register_stm32w1xx_GPIO_BWAKE;
extern   volatile t_register_stm32w1xx_gpio_pxwake       register_stm32w1xx_GPIO_CWAKE;
extern   volatile t_register_stm32w1xx_gpio_irqcsel      register_stm32w1xx_GPIO_IRQCSEL;
extern   volatile t_register_stm32w1xx_gpio_irqdsel      register_stm32w1xx_GPIO_IRQDSEL;
extern   volatile t_register_stm32w1xx_gpio_wakefilt     register_stm32w1xx_GPIO_WAKEFILT;
extern volatile t_register_stm32w1xx_gpio_interruptcfg   register_stm32w1xx_GPIO_INT_CFG;
extern volatile t_register_stm32w1xx_gpio_interruptflag  register_stm32w1xx_INT_GPIOFLAG;
extern volatile t_register_stm32w1xx_int_miss            register_stm32w1xx_INT_MISS;
extern volatile t_register_stm32w1xx_scx                 register_stm32w1xx_SC1;
extern   volatile t_register_stm32w1xx_scx_rxbega        register_stm32w1xx_SC1_RXBEGA;
extern   volatile t_register_stm32w1xx_scx_rxenda        register_stm32w1xx_SC1_RXENDA;
extern   volatile t_register_stm32w1xx_scx_rxbegb        register_stm32w1xx_SC1_RXBEGB;
extern   volatile t_register_stm32w1xx_scx_rxendb        register_stm32w1xx_SC1_RXENDB;
extern   volatile t_register_stm32w1xx_scx_txbega        register_stm32w1xx_SC1_TXBEGA;
extern   volatile t_register_stm32w1xx_scx_txenda        register_stm32w1xx_SC1_TXENDA;
extern   volatile t_register_stm32w1xx_scx_txbegb        register_stm32w1xx_SC1_TXBEGB;
extern   volatile t_register_stm32w1xx_scx_txendb        register_stm32w1xx_SC1_TXENDB;
extern   volatile t_register_stm32w1xx_scx_rxcnta        register_stm32w1xx_SC1_RXCNTA;
extern   volatile t_register_stm32w1xx_scx_rxcntb        register_stm32w1xx_SC1_RXCNTB;
extern   volatile t_register_stm32w1xx_scx_txcnt         register_stm32w1xx_SC1_TXCNT;
extern   volatile t_register_stm32w1xx_scx_dmastat       register_stm32w1xx_SC1_DMASTAT;
extern   volatile t_register_stm32w1xx_scx_dmactrl       register_stm32w1xx_SC1_DMACTRL;
extern   volatile t_register_stm32w1xx_scx_rxerra        register_stm32w1xx_SC1_RXERRA;
extern   volatile t_register_stm32w1xx_scx_rxerrb        register_stm32w1xx_SC1_RXERRB;
extern   volatile t_register_stm32w1xx_scx_data          register_stm32w1xx_SC1_DATA;
extern   volatile t_register_stm32w1xx_scx_spistat       register_stm32w1xx_SC1_SPISTAT;
extern   volatile t_register_stm32w1xx_scx_twistat       register_stm32w1xx_SC1_TWISTAT;
extern   volatile t_register_stm32w1xx_sc1_uartstat      register_stm32w1xx_SC1_UARTSTAT;
extern   volatile t_register_stm32w1xx_scx_twictrl1      register_stm32w1xx_SC1_TWICTRL1;
extern   volatile t_register_stm32w1xx_scx_twictrl2      register_stm32w1xx_SC1_TWICTRL2;
extern   volatile t_register_stm32w1xx_scx_mode          register_stm32w1xx_SC1_MODE;
extern   volatile t_register_stm32w1xx_scx_spicfg        register_stm32w1xx_SC1_SPICFG;
extern   volatile t_register_stm32w1xx_sc1_uartcfg       register_stm32w1xx_SC1_UARTCFG;
extern   volatile t_register_stm32w1xx_scx_ratelin       register_stm32w1xx_SC1_RATELIN;
extern   volatile t_register_stm32w1xx_scx_rateexp       register_stm32w1xx_SC1_RATEEXP;
extern   volatile t_register_stm32w1xx_sc1_uartper       register_stm32w1xx_SC1_UARTPER;
extern   volatile t_register_stm32w1xx_sc1_uartfrac      register_stm32w1xx_SC1_UARTFRAC;
extern   volatile t_register_stm32w1xx_scx_rxcntsaved    register_stm32w1xx_SC1_RXCNTSAVED;
extern volatile t_register_stm32w1xx_scx                 register_stm32w1xx_SC2;
extern   volatile t_register_stm32w1xx_scx_rxbega        register_stm32w1xx_SC2_RXBEGA;
extern   volatile t_register_stm32w1xx_scx_rxenda        register_stm32w1xx_SC2_RXENDA;
extern   volatile t_register_stm32w1xx_scx_rxbegb        register_stm32w1xx_SC2_RXBEGB;
extern   volatile t_register_stm32w1xx_scx_rxendb        register_stm32w1xx_SC2_RXENDB;
extern   volatile t_register_stm32w1xx_scx_txbega        register_stm32w1xx_SC2_TXBEGA;
extern   volatile t_register_stm32w1xx_scx_txenda        register_stm32w1xx_SC2_TXENDA;
extern   volatile t_register_stm32w1xx_scx_txbegb        register_stm32w1xx_SC2_TXBEGB;
extern   volatile t_register_stm32w1xx_scx_txendb        register_stm32w1xx_SC2_TXENDB;
extern   volatile t_register_stm32w1xx_scx_rxcnta        register_stm32w1xx_SC2_RXCNTA;
extern   volatile t_register_stm32w1xx_scx_rxcntb        register_stm32w1xx_SC2_RXCNTB;
extern   volatile t_register_stm32w1xx_scx_txcnt         register_stm32w1xx_SC2_TXCNT;
extern   volatile t_register_stm32w1xx_scx_dmastat       register_stm32w1xx_SC2_DMASTAT;
extern   volatile t_register_stm32w1xx_scx_dmactrl       register_stm32w1xx_SC2_DMACTRL;
extern   volatile t_register_stm32w1xx_scx_rxerra        register_stm32w1xx_SC2_RXERRA;
extern   volatile t_register_stm32w1xx_scx_rxerrb        register_stm32w1xx_SC2_RXERRB;
extern   volatile t_register_stm32w1xx_scx_data          register_stm32w1xx_SC2_DATA;
extern   volatile t_register_stm32w1xx_scx_spistat       register_stm32w1xx_SC2_SPISTAT;
extern   volatile t_register_stm32w1xx_scx_twistat       register_stm32w1xx_SC2_TWISTAT;
extern   volatile t_register_stm32w1xx_sc1_uartstat      register_stm32w1xx_SC2_UARTSTAT;
extern   volatile t_register_stm32w1xx_scx_twictrl1      register_stm32w1xx_SC2_TWICTRL1;
extern   volatile t_register_stm32w1xx_scx_twictrl2      register_stm32w1xx_SC2_TWICTRL2;
extern   volatile t_register_stm32w1xx_scx_mode          register_stm32w1xx_SC2_MODE;
extern   volatile t_register_stm32w1xx_scx_spicfg        register_stm32w1xx_SC2_SPICFG;
extern   volatile t_register_stm32w1xx_sc1_uartcfg       register_stm32w1xx_SC2_UARTCFG;
extern   volatile t_register_stm32w1xx_scx_ratelin       register_stm32w1xx_SC2_RATELIN;
extern   volatile t_register_stm32w1xx_scx_rateexp       register_stm32w1xx_SC2_RATEEXP;
extern   volatile t_register_stm32w1xx_sc1_uartper       register_stm32w1xx_SC2_UARTPER;
extern   volatile t_register_stm32w1xx_sc1_uartfrac      register_stm32w1xx_SC2_UARTFRAC;
extern   volatile t_register_stm32w1xx_scx_rxcntsaved    register_stm32w1xx_SC2_RXCNTSAVED;
extern volatile t_register_stm32w1xx_int_scxflag         register_stm32w1xx_INT_SC1FLAG;
extern volatile t_register_stm32w1xx_int_scxflag         register_stm32w1xx_INT_SC2FLAG;
extern volatile t_register_stm32w1xx_int_scxcfg          register_stm32w1xx_INT_SC1CFG;
extern volatile t_register_stm32w1xx_int_scxcfg          register_stm32w1xx_INT_SC2CFG;
extern volatile t_register_stm32w1xx_scx_intmode         register_stm32w1xx_SC1_INTMODE;
extern volatile t_register_stm32w1xx_scx_intmode         register_stm32w1xx_SC2_INTMODE;
extern volatile t_register_stm32w1xx_osc24m_ctrl         register_stm32w1xx_OSC24M_CTRL;
extern volatile t_register_stm32w1xx_int_cfgset          register_stm32w1xx_INT_CFGSET;
extern volatile t_register_stm32w1xx_int_cfgclr          register_stm32w1xx_INT_CFGCLR;
extern volatile t_register_stm32w1xx_int_pendset         register_stm32w1xx_INT_PENDSET;
extern volatile t_register_stm32w1xx_int_pendclr         register_stm32w1xx_INT_PENDCLR;
extern volatile t_register_stm32w1xx_int_active          register_stm32w1xx_INT_ACTIVE;
extern volatile t_register_stm32w1xx_scs_afsr            register_stm32w1xx_BUS_AFSR;
extern volatile t_register_stm32w1xx_peripherals         register_stm32w1xx_Peripherals;
extern volatile t_register_stm32w1xx_private_peripherals register_stm32w1xx_Private_Peripherals;
extern volatile t_register_stm32w1xx_all                 register_stm32w1xx_ALL;

//}
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_register.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_register.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

/** returns current register configuration
 * @param Config        = pointer to struct t_ttc_register_config (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
volatile t_register_stm32w1xx_all* register_stm32w1xx_get_configuration();

/** Prepares register driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void register_stm32w1xx_prepare();

/** resets given register to default value
 *
 * @param Register    pointer to one entry of struct t_ttc_register_config as returned by ttc_register_get_configuration()
 * @return            ==0: register has been reset successfully
 */
e_ttc_register_errorcode register_stm32w1xx_reset( void* Register );

/** Debugs all manufacture tokens in current device as human readable text.
 *
 * @param reportFunction  pointer to function being called to print out strings
 *
 * Example Usage:

void myPrintText(const char* Text, t_base Size) {
    ttc_usart_send_string(1, Text, Size);
}

void dump_mfg_tokens() {
    register_stm32w1xx_report_all_tokens(myPrintText);
}

 */
void register_stm32w1xx_report_all_tokens( void ( *reportFunction )( const char* Text, t_base Size ) );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _register_stm32w1xx_foo(t_ttc_register_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //REGISTER_STM32W1XX_H