/*{ cortexm3_Registers.c ************************************************

 Structured pointers to registers of ARM CortexM3 microcontrollers.

 written by Gregor Rebel 2011-2011

}*/

#include "register_cortexm3.h"
#include "compile_options.h"

/** Global variables for hardware registers
 *
 * These variables are mapped onto hardware registers in linker script ttc-lib/_linker/memory_stm32w1xx.ld and do not occupy any RAM.
 */

/**  Note: Use of ttc_register_set32()/ ttc_register_get32() is mandatory for these registers.
 *   Bitfield implementation of GCC is broken. DO NOT ACCESS THEM DIRECTLY!
 *
 * Examples:
 * // register_cortexm3_SCB.VTOR.TableOffset = 23;
 * ttc_register_set32(&(register_cortexm3_SCB.VTOR),   TableOffset, 23);
 *
 * // register_cortexm3_SCB.VTOR.TableOffset = 23;
 * // register_cortexm3_SCB.VTOR.TableInSRAM = 1;
 * ttc_register_set32_2(&(register_cortexm3_SCB.VTOR), TableOffset, 23, TableInSRAM, 1);
 *
 * // Value = register_cortexm3_SCB.VTOR.TableOffset;
 * t_u8 Value;
 * ttc_register_get32(&(register_cortexm3_SCB.VTOR), TableOffset, Value);
 */

void register_cortexm3_prepare() {
    // Check register addresses against Cortex-M3 Devices Generic User Guide p.137 (4-11)

#ifdef EXTENSION_basic_extensions_optimize_1_debug

    // testing each register for correct address also ensures that their structs
    // are available during debugging with enabled optimizations
    ttc_assert_address_matches( &( register_cortexm3_ACTLR ), ( void* ) 0xe000e008 );
    ttc_assert_address_matches( &( register_cortexm3_NVIC ), ( void* ) 0xe000e100 );
    ttc_assert_address_matches( &( register_cortexm3_SCB ), ( void* ) 0xe000ed00 );
    ttc_assert_address_matches( &( register_cortexm3_SCB.AFSR ), ( void* ) 0xe000ed3C );
    ttc_assert_address_matches( &( register_cortexm3_SCB.AIRCR ), ( void* ) 0xe000ed0C );
    ttc_assert_address_matches( &( register_cortexm3_SCB.BFAR ), ( void* ) 0xe000ed38 );
    ttc_assert_address_matches( &( register_cortexm3_SCB.CCR ), ( void* ) 0xe000ed14 );
    ttc_assert_address_matches( &( register_cortexm3_SCB.CFSR ), ( void* ) 0xe000ed28 );
    ttc_assert_address_matches( &( register_cortexm3_SCB.CPUID ), ( void* ) 0xe000ed00 );
    ttc_assert_address_matches( &( register_cortexm3_SCB.HFSR ), ( void* ) 0xe000ed2C );
    ttc_assert_address_matches( &( register_cortexm3_SCB.ICSR ), ( void* ) 0xe000ed04 );
    ttc_assert_address_matches( &( register_cortexm3_SCB.MMFAR ), ( void* ) 0xe000ed34 );
    ttc_assert_address_matches( &( register_cortexm3_SCB.SCR ), ( void* ) 0xe000ed10 );
    ttc_assert_address_matches( &( register_cortexm3_SCB.SHCSR ), ( void* ) 0xe000ed24 );
    ttc_assert_address_matches( &( register_cortexm3_SCB.SHPR1 ), ( void* ) 0xe000ed18 );
    ttc_assert_address_matches( &( register_cortexm3_SCB.SHPR2 ), ( void* ) 0xe000ed1C );
    ttc_assert_address_matches( &( register_cortexm3_SCB.SHPR3 ), ( void* ) 0xe000ed20 );
    ttc_assert_address_matches( &( register_cortexm3_SCB.VTOR ), ( void* ) 0xe000ed08 );
    ttc_assert_address_matches( &( register_cortexm3_STK ), ( void* ) 0xe000e010 );
#endif

    // check for CortexM3 CPU
    if ( register_cortexm3_SCB.CPUID.Implementer != 0x41 )
    { ttc_assert_halt_origin( ttc_assert_origin_auto ); } // this is not a CortexM3 cpu (->PM0056 p.131)

    volatile t_u16 PartNo = register_cortexm3_SCB.CPUID.PartNo;
    if ( PartNo      != 0xc23 )
    { ttc_assert_halt_origin( ttc_assert_origin_auto ); } // this is not a CortexM3 cpu (->PM0056 p.131)
}
