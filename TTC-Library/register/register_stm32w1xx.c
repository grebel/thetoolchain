/** { register_stm32w1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for register devices on stm32w1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 20 at 20140223 10:54:39 UTC
 *
 *  Note: Use of ttc_register_set32()/ ttc_register_get32() is mandatory for these registers. Bitfield implementation of GCC is broken. DO NOT ACCESS THEM DIRECTLY!
 * 
 *  Authors: Gregor Rebel
}*/

#include "register_stm32w1xx.h"
#include "../ttc_string.h"

#define int8u  t_u8
#define int16u t_u16
#include "mfg-token.h"

/** Global variables for hardware registers
 *
 * These variables are mapped onto hardware registers in linker script ttc-lib/_linker/memory_stm32w1xx.ld and do not occupy any RAM.
 */

/**  Note: Use of ttc_register_set32()/ ttc_register_get32() is mandatory for these registers.
 *   Bitfield implementation of GCC is broken. DO NOT ACCESS THEM DIRECTLY!
 *
 * Examples:
 * // register_cortexm3_SCB.VTOR.TableOffset = 23;
 * ttc_register_set32(&(register_cortexm3_SCB.VTOR),   TableOffset, 23);
 *
 * // register_cortexm3_SCB.VTOR.TableOffset = 23;
 * // register_cortexm3_SCB.VTOR.TableInSRAM = 1;
 * ttc_register_set32_2(&(register_cortexm3_SCB.VTOR), TableOffset, 23, TableInSRAM, 1);
 *
 * // Value = register_cortexm3_SCB.VTOR.TableOffset;
 * t_u8 Value;
 * ttc_register_get32(&(register_cortexm3_SCB.VTOR), TableOffset, Value);
 */

volatile t_u32                                    register_stm32w1xx_PERIPHERAL_DISABLE   __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_INT             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_INT             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_INT_ADCFLAG          __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_INT_TIM1MISS         __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_INT_TIM2MISS         __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_INT_TIM1CFG          __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_INT_TIM2CFG          __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_INT_ADCCFG           __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_ADC_DATA             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_ADC_CFG              __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_ADC_OFFSET           __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_ADC_GAIN             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_ADC_DMACFG           __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_ADC_DMASTAT          __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_ADC_DMABEG           __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_ADC_DMASIZE          __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_ADC_DMACUR           __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_ADC_DMACNT           __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_CR1             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_CR2             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_SMCR            __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_EGR             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_CCMR1           __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_CCMR2           __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_CCER            __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_CNT             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_PSC             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_ARR             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_CCR1            __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_CCR2            __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_CCR3            __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_CCR4            __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM1_OR              __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_CR1             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_CR2             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_SMCR            __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_EGR             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_CCMR1           __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_CCMR2           __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_CCER            __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_CNT             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_PSC             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_ARR             __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_CCR1            __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_CCR2            __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_CCR3            __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_CCR4            __attribute__((section(".peripherals"))); /* ToDo define register struct */
volatile t_u32                                    register_stm32w1xx_TIM2_OR              __attribute__((section(".peripherals"))); /* ToDo define register struct */

volatile t_register_stm32w1xx_gpiox               register_stm32w1xx_GPIOA                __attribute__((section (".peripherals")));
// registers inside register_stm32w1xx_GPIOA
  volatile t_register_stm32w1xx_gpio_pxcfgl       register_stm32w1xx_GPIOA_CFGL           __attribute__((section (".peripherals")));    //Port configuration register low
  volatile t_register_stm32w1xx_gpio_pxcfgh       register_stm32w1xx_GPIOA_CFGH           __attribute__((section (".peripherals")));    //Port configuration register high
  volatile t_register_stm32w1xx_gpio_pxin         register_stm32w1xx_GPIOA_IN             __attribute__((section (".peripherals")));      //GPIO input data register
  volatile t_register_stm32w1xx_gpio_pxout        register_stm32w1xx_GPIOA_OUT            __attribute__((section (".peripherals")));     //GPIO output data register
  volatile t_register_stm32w1xx_gpio_pxset        register_stm32w1xx_GPIOA_SET            __attribute__((section (".peripherals")));     //GPIO set register
  volatile t_register_stm32w1xx_gpio_pxclr        register_stm32w1xx_GPIOA_CLR            __attribute__((section (".peripherals")));     //GPIO clear register

volatile t_register_stm32w1xx_gpiox               register_stm32w1xx_GPIOB                __attribute__((section (".peripherals")));
// registers inside register_stm32w1xx_GPIOB
  volatile t_register_stm32w1xx_gpio_pxcfgl       register_stm32w1xx_GPIOOB_CFGL          __attribute__((section (".peripherals")));    //Port configuration register low
  volatile t_register_stm32w1xx_gpio_pxcfgh       register_stm32w1xx_GPIOOB_CFGH          __attribute__((section (".peripherals")));    //Port configuration register high
  volatile t_register_stm32w1xx_gpio_pxin         register_stm32w1xx_GPIOOB_IN            __attribute__((section (".peripherals")));      //GPIO input data register
  volatile t_register_stm32w1xx_gpio_pxout        register_stm32w1xx_GPIOOB_OUT           __attribute__((section (".peripherals")));     //GPIO output data register
  volatile t_register_stm32w1xx_gpio_pxset        register_stm32w1xx_GPIOOB_SET           __attribute__((section (".peripherals")));     //GPIO set register
  volatile t_register_stm32w1xx_gpio_pxclr        register_stm32w1xx_GPIOOB_CLR           __attribute__((section (".peripherals")));     //GPIO clear register

volatile t_register_stm32w1xx_gpiox               register_stm32w1xx_GPIOC                __attribute__((section (".peripherals")));
// registers inside register_stm32w1xx_GPIOC
  volatile t_register_stm32w1xx_gpio_pxcfgl       register_stm32w1xx_GPIOC_CFGL           __attribute__((section (".peripherals")));    //Port configuration register low
  volatile t_register_stm32w1xx_gpio_pxcfgh       register_stm32w1xx_GPIOC_CFGH           __attribute__((section (".peripherals")));    //Port configuration register high
  volatile t_register_stm32w1xx_gpio_pxin         register_stm32w1xx_GPIOC_IN             __attribute__((section (".peripherals")));      //GPIO input data register
  volatile t_register_stm32w1xx_gpio_pxout        register_stm32w1xx_GPIOC_OUT            __attribute__((section (".peripherals")));     //GPIO output data register
  volatile t_register_stm32w1xx_gpio_pxset        register_stm32w1xx_GPIOC_SET            __attribute__((section (".peripherals")));     //GPIO set register
  volatile t_register_stm32w1xx_gpio_pxclr        register_stm32w1xx_GPIOC_CLR            __attribute__((section (".peripherals")));     //GPIO clear register

volatile t_register_stm32w1xx_gpio_debug_wake_irq register_stm32w1xx_GPIO_DB_WAKE_IRQ     __attribute__((section (".peripherals")));
// registers inside register_stm32w1xx_GPIO_DB_WAKE_IRQ
  volatile t_register_stm32w1xx_gpio_dbgcfg       register_stm32w1xx_GPIO_DBGCFG          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_gpio_dbgstat      register_stm32w1xx_GPIO_DBGSTAT         __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_gpio_pxwake       register_stm32w1xx_GPIO_AWAKE           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_gpio_pxwake       register_stm32w1xx_GPIO_BWAKE           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_gpio_pxwake       register_stm32w1xx_GPIO_CWAKE           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_gpio_irqcsel      register_stm32w1xx_GPIO_IRQCSEL         __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_gpio_irqdsel      register_stm32w1xx_GPIO_IRQDSEL         __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_gpio_wakefilt     register_stm32w1xx_GPIO_WAKEFILT        __attribute__((section (".peripherals")));

volatile t_register_stm32w1xx_gpio_interruptcfg   register_stm32w1xx_GPIO_INT_CFG         __attribute__((section (".peripherals")));
volatile t_register_stm32w1xx_gpio_interruptflag  register_stm32w1xx_INT_GPIOFLAG         __attribute__((section (".peripherals"))); // == EXTI_BASE in stm32w108xx.h
volatile t_register_stm32w1xx_int_miss            register_stm32w1xx_INT_MISS             __attribute__((section (".peripherals")));
volatile t_register_stm32w1xx_scx                 register_stm32w1xx_SC1                  __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxbega        register_stm32w1xx_SC1_RXBEGA           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxenda        register_stm32w1xx_SC1_RXENDA           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxbegb        register_stm32w1xx_SC1_RXBEGB           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxendb        register_stm32w1xx_SC1_RXENDB           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_txbega        register_stm32w1xx_SC1_TXBEGA           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_txenda        register_stm32w1xx_SC1_TXENDA           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_txbegb        register_stm32w1xx_SC1_TXBEGB           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_txendb        register_stm32w1xx_SC1_TXENDB           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxcnta        register_stm32w1xx_SC1_RXCNTA           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxcntb        register_stm32w1xx_SC1_RXCNTB           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_txcnt         register_stm32w1xx_SC1_TXCNT            __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_dmastat       register_stm32w1xx_SC1_DMASTAT          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_dmactrl       register_stm32w1xx_SC1_DMACTRL          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxerra        register_stm32w1xx_SC1_RXERRA           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxerrb        register_stm32w1xx_SC1_RXERRB           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_data          register_stm32w1xx_SC1_DATA             __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_spistat       register_stm32w1xx_SC1_SPISTAT          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_twistat       register_stm32w1xx_SC1_TWISTAT          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_sc1_uartstat      register_stm32w1xx_SC1_UARTSTAT         __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_twictrl1      register_stm32w1xx_SC1_TWICTRL1         __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_twictrl2      register_stm32w1xx_SC1_TWICTRL2         __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_mode          register_stm32w1xx_SC1_MODE             __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_spicfg        register_stm32w1xx_SC1_SPICFG           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_sc1_uartcfg       register_stm32w1xx_SC1_UARTCFG          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_ratelin       register_stm32w1xx_SC1_RATELIN          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rateexp       register_stm32w1xx_SC1_RATEEXP          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_sc1_uartper       register_stm32w1xx_SC1_UARTPER          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_sc1_uartfrac      register_stm32w1xx_SC1_UARTFRAC         __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxcntsaved    register_stm32w1xx_SC1_RXCNTSAVED       __attribute__((section (".peripherals")));

volatile t_register_stm32w1xx_scx                 register_stm32w1xx_SC2                  __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxbega        register_stm32w1xx_SC2_RXBEGA           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxenda        register_stm32w1xx_SC2_RXENDA           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxbegb        register_stm32w1xx_SC2_RXBEGB           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxendb        register_stm32w1xx_SC2_RXENDB           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_txbega        register_stm32w1xx_SC2_TXBEGA           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_txenda        register_stm32w1xx_SC2_TXENDA           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_txbegb        register_stm32w1xx_SC2_TXBEGB           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_txendb        register_stm32w1xx_SC2_TXENDB           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxcnta        register_stm32w1xx_SC2_RXCNTA           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxcntb        register_stm32w1xx_SC2_RXCNTB           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_txcnt         register_stm32w1xx_SC2_TXCNT            __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_dmastat       register_stm32w1xx_SC2_DMASTAT          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_dmactrl       register_stm32w1xx_SC2_DMACTRL          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxerra        register_stm32w1xx_SC2_RXERRA           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxerrb        register_stm32w1xx_SC2_RXERRB           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_data          register_stm32w1xx_SC2_DATA             __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_spistat       register_stm32w1xx_SC2_SPISTAT          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_twistat       register_stm32w1xx_SC2_TWISTAT          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_sc1_uartstat      register_stm32w1xx_SC2_UARTSTAT         __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_twictrl1      register_stm32w1xx_SC2_TWICTRL1         __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_twictrl2      register_stm32w1xx_SC2_TWICTRL2         __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_mode          register_stm32w1xx_SC2_MODE             __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_spicfg        register_stm32w1xx_SC2_SPICFG           __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_sc1_uartcfg       register_stm32w1xx_SC2_UARTCFG          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_ratelin       register_stm32w1xx_SC2_RATELIN          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rateexp       register_stm32w1xx_SC2_RATEEXP          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_sc1_uartper       register_stm32w1xx_SC2_UARTPER          __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_sc1_uartfrac      register_stm32w1xx_SC2_UARTFRAC         __attribute__((section (".peripherals")));
  volatile t_register_stm32w1xx_scx_rxcntsaved    register_stm32w1xx_SC2_RXCNTSAVED       __attribute__((section (".peripherals")));

volatile t_register_stm32w1xx_int_scxflag         register_stm32w1xx_INT_SC1FLAG          __attribute__((section (".peripherals")));
volatile t_register_stm32w1xx_int_scxflag         register_stm32w1xx_INT_SC2FLAG          __attribute__((section (".peripherals")));
volatile t_register_stm32w1xx_int_scxcfg          register_stm32w1xx_INT_SC1CFG           __attribute__((section (".peripherals")));
volatile t_register_stm32w1xx_int_scxcfg          register_stm32w1xx_INT_SC2CFG           __attribute__((section (".peripherals")));
volatile t_register_stm32w1xx_scx_intmode         register_stm32w1xx_SC1_INTMODE          __attribute__((section (".peripherals")));
volatile t_register_stm32w1xx_scx_intmode         register_stm32w1xx_SC2_INTMODE          __attribute__((section (".peripherals")));

// system clock
volatile t_register_stm32w1xx_osc24m_ctrl         register_stm32w1xx_OSC24M_CTRL         __attribute__((section (".peripherals")));

volatile t_register_stm32w1xx_int_cfgset          register_stm32w1xx_INT_CFGSET           __attribute__((section (".private_peripherals")));
volatile t_register_stm32w1xx_int_cfgclr          register_stm32w1xx_INT_CFGCLR           __attribute__((section (".private_peripherals")));
volatile t_register_stm32w1xx_int_pendset         register_stm32w1xx_INT_PENDSET          __attribute__((section (".private_peripherals")));
volatile t_register_stm32w1xx_int_pendclr         register_stm32w1xx_INT_PENDCLR          __attribute__((section (".private_peripherals")));
volatile t_register_stm32w1xx_int_active          register_stm32w1xx_INT_ACTIVE           __attribute__((section (".private_peripherals")));
volatile t_register_stm32w1xx_scs_afsr            register_stm32w1xx_BUS_AFSR             __attribute__((section (".private_peripherals")));

// collections of all registers collected by memory region
volatile t_register_stm32w1xx_peripherals         register_stm32w1xx_Peripherals         __attribute__((section (".peripherals")));
volatile t_register_stm32w1xx_private_peripherals register_stm32w1xx_Private_Peripherals __attribute__((section (".private_peripherals")));


// all registers in one single struct
volatile t_register_stm32w1xx_all register_stm32w1xx_ALL;

t_u8  register_stm32w1xx_TOKEN_MFG_CIB_OBS[16]           __attribute__((section (".peripherals"))); /* { 0x08040800
  AN710:
  Dedicated special flash storage called option bytes. Option
  bytes are special storage because they are directly linked to
  hardware functionality of the chip. There are 8 option bytes,
  each occupying 16 bits of flash as follows:
   Option Byte 0: Configures flash read protection
   Option Byte 1: Reserved
   Option Byte 2: Available for customer use
   Option Byte 3: Available for customer use
   Option Byte 4: Configures flash write protection
   Option Byte 5: Configures flash write protection
   Option Byte 6: Configures flash write protection
   Option Byte 7: Reserved
  Refer to the EM35x Datasheet (120-035X-000) for a
  detailed description of option byes, what they mean, how
  they work, and what values should be used.
  Usage: All option bytes must be set to a valid state.
}*/
t_u16 register_stm32w1xx_TOKEN_MFG_CUSTOM_VERSION        __attribute__((section (".peripherals"))); /* { 0x08040810
  AN710:
  Version number to signify which revision of CIB
  manufacturing tokens you are using. This value should
  match CURRENT_MFG_CUSTOM_VERSION which is
  currently set to 0x01FE.
  CURRENT_MFG_CUSTOM_VERSION is defined in
  \hal\micro\cortexm3\token-manufacturing.h.
  Usage: Recommended for all devices using CIB
  manufacturing tokens.
}*/
t_u8  register_stm32w1xx_TOKEN_MFG_CUSTOM_EUI_64[8]      __attribute__((section (".peripherals"))); /* { 0x08040812
  AN710:
  IEEE 64-bit address, unique for each radio. Entered and
  stored in little-endian. Setting a value here overrides the
  pre-programmed Ember EUI64 stored in the FIB
  (TOKEN_MFG_EMBER_EUI_64). This is for customers
  who have purchased their own address block from IEEE.
  Usage: Optionally set by device manufacturer if using
  custom EUI64 address block.
}*/
t_u8  register_stm32w1xx_TOKEN_MFG_STRING[16]            __attribute__((section (".peripherals"))); /* { 0x0804081A
  AN710:
  Optional device-specific string, for example, the serial
  number.
  Usage: Optionally set by device manufacturer to identify
  device.
}*/
t_u8  register_stm32w1xx_TOKEN_MFG_BOARD_NAME[16]        __attribute__((section (".peripherals"))); /* { 0x0804082A
  AN710:
  Optional string identifying the board name or hardware
  model.
  Usage: Optionally set by device manufacturer to identify
  device.
}*/
t_u16 register_stm32w1xx_TOKEN_MFG_MANUF_ID              __attribute__((section (".peripherals"))); /* { 0x0804083A
  AN710:
  16-bit ID denoting the manufacturer of this device. Silicon
  Labs recommends setting this value to match your ZigBee-
  assigned manufacturer code, such as in the stack’s
  emberSetManufacturerCode() API call.
  Usage: Recommended for devices utilizing the standalone
  bootloader.
}*/
t_u16 register_stm32w1xx_TOKEN_MFG_PHY_CONFIG            __attribute__((section (".peripherals"))); /* { 0x0804083C
  AN710:
  Default configuration of the radio for power mode
  (boost/normal), transmit path selection (bi-
  directional/alternate), and boost transmit power level:
  • Bit 0 (lsb): Set to 0 for boost mode, set to 1 for non-
    boost (normal) mode. Boost mode mainly increases rx
    sensitivity but also increases tx output power by a small
    amount. Larger increases in tx output power are
    available in bits 3-7 of this token.
  • Bit 1: Set to 0 if an external PA is connected to the
    alternate TX path (RF_TX_ALT_P and RF_TX_ALT_N
    pins), set to 1 otherwise.
  • Bit 2: Set to 0 if an external PA is connected to the bi-
    directional RF path (RF_P and RF_N pins), set to 1
    otherwise.
  • Bits 3-7: Set to the boost tx power offset from the first
    integer value above nominal maximum tx power (+3dBm).
    0 = +4 dBm
    1 = +5 dBm
    2 = +6 dBm
    3 = +7 dBm
    4 = +8 dBm

  Bit 7 is most significant, Bit 3 is least significant. All bits
  must be set to 1 if tx boost power level is not desired.
  • Bits 8–15: Reserved. Must be set to 1 (the erased state).

  Usage: Required for devices that utilize boost mode or an
  external PA circuit.
}*/
t_u8  register_stm32w1xx_TOKEN_MFG_BOOTLOAD_AES_KEY[16]  __attribute__((section (".peripherals"))); /* { 0x0804083E
  AN710:
  Sets the AES key used by the bootloader utility to
  authenticate bootloader launch requests.
  Usage: Required for devices that utilize the standalone
  bootloader.
}*/
t_u8  register_stm32w1xx_TOKEN_MFG_EZSP_STORAGE[8]       __attribute__((section (".peripherals"))); /* { 0x0804084E
  AN710:
  An 8-byte, general-purpose token that can be set at
  manufacturing time and read by the host microcontroller via
  EZSP’s getMfgToken command frame.
  Usage: Not required. Device manufacturer may populate or
  leave empty as desired.
}*/
t_u8  register_stm32w1xx_TOKEN_MFG_CBKE_DATA[92]         __attribute__((section (".peripherals"))); /* { 0x0804087E
  AN710:
  Defines the security data necessary for Smart Energy
  devices. It is used for Certificate Based Key Exchange to
  authenticate a device on a Smart Energy network. The first
  48 bytes are the device’s implicit certificate, the next 22
  bytes are the Root Certificate Authority’s Public Key, the
  next 21 bytes are the device’s private key (the other half of
  the public/private key pair stored in the certificate), and the
  last byte is a flags field. The flags field should be set to
  0x00 to indicate that the security data is initialized.
  Usage: Required by Smart Energy Profile certified devices.
}*/
t_u8  register_stm32w1xx_TOKEN_MFG_INSTALLATION_CODE[20] __attribute__((section (".peripherals"))); /* { 0x080408DA
  AN710:
  Defines the installation code for Smart Energy devices. The
  installation code is used to create a pre-configured link key
  for initially joining a Smart Energy network. The first 2 bytes
  are a flags field, the next 16 bytes are the installation code,
  and the last 2 bytes are a CRC.
  Valid installation code sizes are 6, 8, 12, or 16 bytes in
  length. All unused bytes should be 0xFF. The flags field
  should be set as follows depending on the size of the install
  code:
  • 6 bytes = 0x0000
  • 8 bytes = 0x0002
  • 12 bytes = 0x0004
  • 16 bytes = 0x0006
  Usage: Required by Smart Energy Profile certified devices.
}*/
t_u16 register_stm32w1xx_TOKEN_MFG_OSC24M_BIAS_TRIM      __attribute__((section (".peripherals"))); /* { 0x080408EE
  AN710:
  A relative amount (between 0x0000 and 0x000F) that trims
  the 24 MHz crystal bias drive. A lower value reduces drive
  and current consumption, but increases instability. Although
  a value can be set here manually, Silicon Labs
  recommends leaving this value unpopulated (0xFFFF) so
  that the stack can perform auto-calibration at runtime to
  determine the optimal value for lowest current consumption
  while maintaining stability.
  Usage: Not recommended (leave erased, 0xFFFF).
}*/
t_u16 register_stm32w1xx_TOKEN_MFG_SYNTH_FREQ_OFFSET     __attribute__((section (".peripherals"))); /* { 0x080408F0
  AN710:
  An offset applied to the radio synthesizer frequency. This
  offset can be used to compensate for variations in 24 MHz
  crystals to accurately center IEEE-802.15.4 channels.
  • Bits 0-7: Set to the signed offset to be applied to the
  synthesizer frequency. Each step provides
  approximately 11 kHz of adjustment.
  • Bit 8: Set to 0 if the offset is valid and should be used.
  Set to 1 (the erased state) if the token is invalid or has
  not been set.
  • Bits 9-15: Reserved. Must be set to 1 (the erased
  state).
}*/
t_u16 register_stm32w1xx_TOKEN_MFG_OSC24M_SETTLE_DELAY   __attribute__((section (".peripherals"))); /* { 0x080408F2
  AN710:
  The delay in microseconds to allow the 24 MHz crystal to
  settle after a bias change when waking up from deep sleep.
  Silicon Labs recommends leaving this value unpopulated
  (0xFFFF) so that the stack uses its default conservative
  delay value.
}*/
t_u16 register_stm32w1xx_TOKEN_MFG_SECURITY_CONFIG       __attribute__((section (".peripherals"))); /* { 0x080408F4
  AN710:
Def  ines the security policy for application calls into the stack
  to retrieve key values. The API calls emberGetKey() and
  emberGetKeyTableEntry() are affected by this setting.
  This prevents a running application from reading the actual
  encryption key values from the stack. This token may also
  be set at runtime with emberSetMfgSecurityConfig()
  (see that API for more information). The stack utilizes the
  emberGetMfgSecurityConfig() to determine the
  current security policy for encryption keys. The token values
  are mapped to the EmberKeySettings stack data type
  (defined in ember-types.h). See Table 3 below for the
  mapping of token values to the stack values.
}*/
t_u16 register_stm32w1xx_TOKEN_MFG_CCA_THRESHOLD         __attribute__((section (".peripherals"))); /* { 0x080408F6
  AN710:
  Threshold used for energy detection clear channel
  assessment (CCA).
  • Bits 0-7: Set to the two’s complement representation of
  the CCA threshold in dBm below which the channel will
  be considered clear. Valid values are -128 through
  +126, inclusive. +127 is NOT valid and must not be
  used.
  • Bit 8: Set to 0 if the threshold is valid and should be
  used. Set to 1 (the erased state) if the token is invalid
  or has not been set, in this case the default threshold
  will be used.
  • Bits 9-15: Reserved. Must be set to 1 (the erased
  state).
  The default CCA threshold for EM35x chips is -75 dBm. If
  bit 8 of this token is 1 then -75 dBm will be used.
  You may want to override the default CCA threshold by
  setting this token if your design uses an LNA. An LNA
  changes the gain on the radio input, which results in the
  radio “seeing” a different energy level than if no LNA was
  used.
  Example: A design uses an LNA that provides gain of
  +12 dBm. Add the default -75 dBm gain to the LNA’s gain to
  get the dBm value for the token: -75 + 12 = -63 dBm.
  The two’s complement signed representation of -63 dBm is
  0xC1, and so the complete token value to be programmed
  is 0xFEC1.
}*/


//}
//{ Function definitions *******************************************************

volatile t_register_stm32w1xx_all* register_stm32w1xx_get_configuration() {

    return &register_stm32w1xx_ALL;
}
void register_stm32w1xx_prepare() {

    register_cortexm3_prepare();

    register_stm32w1xx_ALL.Peripherals        = &register_stm32w1xx_Peripherals;
    register_stm32w1xx_ALL.PrivatePeripherals = &register_stm32w1xx_Private_Peripherals;

#if (TTC_ASSERT_REGISTER == 1) // check structure definitions and size
    // Note: Correct addresses should have been defined in Linkerscript (ttc-lib/_linker/memory_stm32w1xx.ld)
    ttc_assert_address_matches(&register_stm32w1xx_INT_SC1FLAG      , &(register_stm32w1xx_ALL.Peripherals->rSC1_FLAG)         ); // check struct definition and size
    ttc_assert_address_matches(&register_stm32w1xx_INT_SC2FLAG      , &(register_stm32w1xx_ALL.Peripherals->rSC2_FLAG)         ); // check struct definition and size
    ttc_assert_address_matches(&register_stm32w1xx_INT_GPIOFLAG    , &(register_stm32w1xx_ALL.Peripherals->rINT_GPIOFLAG)    ); // check struct definition and size
    ttc_assert_address_matches(&register_stm32w1xx_INT_MISS         , &(register_stm32w1xx_ALL.Peripherals->rINT_MISS)         ); // check struct definition and size
    ttc_assert_address_matches(&register_stm32w1xx_INT_SC1CFG       , &(register_stm32w1xx_ALL.Peripherals->rSC1_CFG)          ); // check struct definition and size
    ttc_assert_address_matches(&register_stm32w1xx_INT_SC2CFG       , &(register_stm32w1xx_ALL.Peripherals->rSC2_CFG)          ); // check struct definition and size
    ttc_assert_address_matches(&register_stm32w1xx_SC1_INTMODE      , &(register_stm32w1xx_ALL.Peripherals->rSC1_INTMODE)      ); // check struct definition and size
    ttc_assert_address_matches(&register_stm32w1xx_SC2_INTMODE      , &(register_stm32w1xx_ALL.Peripherals->rSC2_INTMODE)      ); // check struct definition and size

    ttc_assert_address_matches(&register_stm32w1xx_GPIO_INT_CFG     , &(register_stm32w1xx_ALL.Peripherals->rGPIO_INT_CFG)     ); // check struct definition and size
    // registers inside register_stm32w1xx_GPIOA
    ttc_assert_address_matches(&register_stm32w1xx_GPIOA_CFGL, &(register_stm32w1xx_GPIOA.CFGL));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOA_CFGH, &(register_stm32w1xx_GPIOA.CFGH));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOA_IN,   &(register_stm32w1xx_GPIOA.IN));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOA_OUT,  &(register_stm32w1xx_GPIOA.OUT));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOA_SET,  &(register_stm32w1xx_GPIOA.SET));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOA_CLR,  &(register_stm32w1xx_GPIOA.CLR));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOA            , &(register_stm32w1xx_ALL.Peripherals->rGPIOA)            ); // check struct definition and size

    ttc_assert_address_matches(&register_stm32w1xx_GPIOB            , &(register_stm32w1xx_ALL.Peripherals->rGPIOB)            ); // check struct definition and size
    // registers inside register_stm32w1xx_GPIOB
    ttc_assert_address_matches(&register_stm32w1xx_GPIOOB_CFGL, &(register_stm32w1xx_GPIOB.CFGL));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOOB_CFGH, &(register_stm32w1xx_GPIOB.CFGH));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOOB_IN,   &(register_stm32w1xx_GPIOB.IN));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOOB_OUT,  &(register_stm32w1xx_GPIOB.OUT));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOOB_SET,  &(register_stm32w1xx_GPIOB.SET));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOOB_CLR,  &(register_stm32w1xx_GPIOB.CLR));

    ttc_assert_address_matches(&register_stm32w1xx_GPIOC            , &(register_stm32w1xx_ALL.Peripherals->rGPIOC)            ); // check struct definition and size
    // registers inside register_stm32w1xx_GPIOC
    ttc_assert_address_matches(&register_stm32w1xx_GPIOC_CFGL, &(register_stm32w1xx_GPIOC.CFGL));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOC_CFGH, &(register_stm32w1xx_GPIOC.CFGH));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOC_IN,   &(register_stm32w1xx_GPIOC.IN));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOC_OUT,  &(register_stm32w1xx_GPIOC.OUT));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOC_SET,  &(register_stm32w1xx_GPIOC.SET));
    ttc_assert_address_matches(&register_stm32w1xx_GPIOC_CLR,  &(register_stm32w1xx_GPIOC.CLR));

    ttc_assert_address_matches(&register_stm32w1xx_GPIO_DB_WAKE_IRQ , &(register_stm32w1xx_ALL.Peripherals->rGPIO_DB_WAKE_IRQ) ); // check struct definition and size
    ttc_assert_address_matches( &register_stm32w1xx_GPIO_DBGCFG, &(register_stm32w1xx_GPIO_DB_WAKE_IRQ.DBGCFG ));
    ttc_assert_address_matches(&register_stm32w1xx_GPIO_DBGSTAT     , &(register_stm32w1xx_GPIO_DB_WAKE_IRQ.DBGSTAT) );
    ttc_assert_address_matches(&register_stm32w1xx_GPIO_AWAKE       , &(register_stm32w1xx_GPIO_DB_WAKE_IRQ.AWAKE) );
    ttc_assert_address_matches(&register_stm32w1xx_GPIO_BWAKE       , &(register_stm32w1xx_GPIO_DB_WAKE_IRQ.BWAKE) );
    ttc_assert_address_matches(&register_stm32w1xx_GPIO_CWAKE       , &(register_stm32w1xx_GPIO_DB_WAKE_IRQ.CWAKE) );
    ttc_assert_address_matches(&register_stm32w1xx_GPIO_IRQCSEL     , &(register_stm32w1xx_GPIO_DB_WAKE_IRQ.IRQCSEL) );
    ttc_assert_address_matches(&register_stm32w1xx_GPIO_IRQDSEL     , &(register_stm32w1xx_GPIO_DB_WAKE_IRQ.IRQDSEL) );
    ttc_assert_address_matches(&register_stm32w1xx_GPIO_WAKEFILT    , &(register_stm32w1xx_GPIO_DB_WAKE_IRQ.WAKEFILT) );

    ttc_assert_address_matches(&register_stm32w1xx_SC2              , &(register_stm32w1xx_ALL.Peripherals->rSC2)              ); // check struct definition and size
    ttc_assert_address_matches(&register_stm32w1xx_SC2_RXBEGA,       &(register_stm32w1xx_SC2.RXBEGA));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_RXENDA,       &(register_stm32w1xx_SC2.RXENDA));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_RXBEGB,       &(register_stm32w1xx_SC2.RXBEGB));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_RXENDB,       &(register_stm32w1xx_SC2.RXENDB));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_TXBEGA,       &(register_stm32w1xx_SC2.TXBEGA));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_TXENDA,       &(register_stm32w1xx_SC2.TXENDA));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_TXBEGB,       &(register_stm32w1xx_SC2.TXBEGB));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_TXENDB,       &(register_stm32w1xx_SC2.TXENDB));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_RXCNTA,       &(register_stm32w1xx_SC2.RXCNTA));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_RXCNTB,       &(register_stm32w1xx_SC2.RXCNTB));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_TXCNT,        &(register_stm32w1xx_SC2.TXCNT));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_DMASTAT,      &(register_stm32w1xx_SC2.DMASTAT));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_DMACTRL,      &(register_stm32w1xx_SC2.DMACTRL));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_RXERRA,       &(register_stm32w1xx_SC2.RXERRA));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_RXERRB,       &(register_stm32w1xx_SC2.RXERRB));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_DATA,         &(register_stm32w1xx_SC2.DATA));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_SPISTAT,      &(register_stm32w1xx_SC2.SPISTAT));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_TWISTAT,      &(register_stm32w1xx_SC2.TWISTAT));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_UARTSTAT,     &(register_stm32w1xx_SC2.UARTSTAT));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_TWICTRL1,     &(register_stm32w1xx_SC2.TWICTRL1));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_TWICTRL2,     &(register_stm32w1xx_SC2.TWICTRL2));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_MODE,         &(register_stm32w1xx_SC2.MODE));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_SPICFG,       &(register_stm32w1xx_SC2.SPICFG));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_UARTCFG,      &(register_stm32w1xx_SC2.UARTCFG));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_RATELIN,      &(register_stm32w1xx_SC2.RATELIN));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_RATEEXP,      &(register_stm32w1xx_SC2.RATEEXP));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_UARTPER,      &(register_stm32w1xx_SC2.UARTPER));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_UARTFRAC,     &(register_stm32w1xx_SC2.UARTFRAC));
    ttc_assert_address_matches(&register_stm32w1xx_SC2_RXCNTSAVED,   &(register_stm32w1xx_SC2.RXCNTSAVED));

    ttc_assert_address_matches(&register_stm32w1xx_SC1              , &(register_stm32w1xx_ALL.Peripherals->rSC1)              ); // check struct definition and size
    ttc_assert_address_matches(&register_stm32w1xx_SC1_RXBEGA,       &(register_stm32w1xx_SC1.RXBEGA));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_RXENDA,       &(register_stm32w1xx_SC1.RXENDA));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_RXBEGB,       &(register_stm32w1xx_SC1.RXBEGB));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_RXENDB,       &(register_stm32w1xx_SC1.RXENDB));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_TXBEGA,       &(register_stm32w1xx_SC1.TXBEGA));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_TXENDA,       &(register_stm32w1xx_SC1.TXENDA));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_TXBEGB,       &(register_stm32w1xx_SC1.TXBEGB));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_TXENDB,       &(register_stm32w1xx_SC1.TXENDB));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_RXCNTA,       &(register_stm32w1xx_SC1.RXCNTA));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_RXCNTB,       &(register_stm32w1xx_SC1.RXCNTB));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_TXCNT,        &(register_stm32w1xx_SC1.TXCNT));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_DMASTAT,      &(register_stm32w1xx_SC1.DMASTAT));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_DMACTRL,      &(register_stm32w1xx_SC1.DMACTRL));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_RXERRA,       &(register_stm32w1xx_SC1.RXERRA));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_RXERRB,       &(register_stm32w1xx_SC1.RXERRB));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_DATA,         &(register_stm32w1xx_SC1.DATA));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_SPISTAT,      &(register_stm32w1xx_SC1.SPISTAT));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_TWISTAT,      &(register_stm32w1xx_SC1.TWISTAT));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_UARTSTAT,     &(register_stm32w1xx_SC1.UARTSTAT));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_TWICTRL1,     &(register_stm32w1xx_SC1.TWICTRL1));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_TWICTRL2,     &(register_stm32w1xx_SC1.TWICTRL2));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_MODE,         &(register_stm32w1xx_SC1.MODE));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_SPICFG,       &(register_stm32w1xx_SC1.SPICFG));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_UARTCFG,      &(register_stm32w1xx_SC1.UARTCFG));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_RATELIN,      &(register_stm32w1xx_SC1.RATELIN));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_RATEEXP,      &(register_stm32w1xx_SC1.RATEEXP));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_UARTPER,      &(register_stm32w1xx_SC1.UARTPER));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_UARTFRAC,     &(register_stm32w1xx_SC1.UARTFRAC));
    ttc_assert_address_matches(&register_stm32w1xx_SC1_RXCNTSAVED,   &(register_stm32w1xx_SC1.RXCNTSAVED));


    ttc_assert_address_matches(&register_stm32w1xx_INT_CFGSET       , &(register_stm32w1xx_ALL.PrivatePeripherals-> rINT_CFGSET) ); // check struct definition and size
    ttc_assert_address_matches(&register_stm32w1xx_INT_CFGCLR       , &(register_stm32w1xx_ALL.PrivatePeripherals-> rINT_CFGCLR) ); // check struct definition and size
    ttc_assert_address_matches(&register_stm32w1xx_INT_PENDSET      , &(register_stm32w1xx_ALL.PrivatePeripherals-> rINT_PENDSET)); // check struct definition and size
    ttc_assert_address_matches(&register_stm32w1xx_INT_PENDCLR      , &(register_stm32w1xx_ALL.PrivatePeripherals-> rINT_PENDCLR)); // check struct definition and size
    ttc_assert_address_matches(&register_stm32w1xx_INT_ACTIVE       , &(register_stm32w1xx_ALL.PrivatePeripherals-> rINT_ACTIVE) ); // check struct definition and size
    ttc_assert_address_matches(&register_stm32w1xx_BUS_AFSR         , &(register_stm32w1xx_ALL.PrivatePeripherals-> rBUS_AFSR)   ); // check struct definition and size

    ttc_assert_address_matches((void*) 0xe000e100 , &register_cortexm3_NVIC.ISER0);
    ttc_assert_address_matches((void*) 0xe000e104 , &register_cortexm3_NVIC.ISER1);
    ttc_assert_address_matches((void*) 0xe000e108 , &register_cortexm3_NVIC.ISER2);
    ttc_assert_address_matches((void*) 0xe000e180 , &register_cortexm3_NVIC.ICER0);
    ttc_assert_address_matches((void*) 0xe000e184 , &register_cortexm3_NVIC.ICER1);
    ttc_assert_address_matches((void*) 0xe000e188 , &register_cortexm3_NVIC.ICER2);
    ttc_assert_address_matches((void*) 0xe000e200 , &register_cortexm3_NVIC.ISPR0);
    ttc_assert_address_matches((void*) 0xe000e204 , &register_cortexm3_NVIC.ISPR1);
    ttc_assert_address_matches((void*) 0xe000e208 , &register_cortexm3_NVIC.ISPR2);
    ttc_assert_address_matches((void*) 0xe000e280 , &register_cortexm3_NVIC.ICPR0);
    ttc_assert_address_matches((void*) 0xe000e284 , &register_cortexm3_NVIC.ICPR1);
    ttc_assert_address_matches((void*) 0xe000e288 , &register_cortexm3_NVIC.ICPR2);
    ttc_assert_address_matches((void*) 0xe000e300 , &register_cortexm3_NVIC.IABR0);
    ttc_assert_address_matches((void*) 0xe000e304 , &register_cortexm3_NVIC.IABR1);
    ttc_assert_address_matches((void*) 0xe000e308 , &register_cortexm3_NVIC.IABR2);
#endif
}
e_ttc_register_errorcode register_stm32w1xx_reset(void* Register) {
    Assert(Register != NULL); // pointers must not be NULL

    return (e_ttc_register_errorcode) 0;
}
t_u8 register_stm32w1xx_report_token_Index = 0;
void register_stm32w1xx_report_token(void (*reportFunction)(const char* Text, t_base Size), t_u8 DatatypeSize, const char* Name, void* Register, t_u8 AmountEntries, t_base Address) {
    Assert_REGISTER(reportFunction, ttc_assert_origin_auto);

#ifndef register_stm32w1xx_report_token_BUFFERSIZE
#  define register_stm32w1xx_report_token_BUFFERSIZE 70
#endif
    char Buffer[register_stm32w1xx_report_token_BUFFERSIZE];
    t_base BufferFree = register_stm32w1xx_report_token_BUFFERSIZE;
    ttc_string_appendf( (t_u8*) Buffer, &BufferFree, "0x%02x, ", 10); //D
    t_base BytesWritten;
    register_stm32w1xx_report_token_Index++;
    switch (DatatypeSize) {
    case 1: {
        BytesWritten = ttc_string_appendf( (t_u8*) Buffer, &BufferFree, "%02i) t_u8 %s[%i] = {", register_stm32w1xx_report_token_Index, Name, AmountEntries);
        reportFunction(Buffer, BytesWritten);

        t_u8* Reader = (t_u8*) Register;
        while (AmountEntries) {
            BufferFree = register_stm32w1xx_report_token_BUFFERSIZE;
            if (AmountEntries-- > 1) // still >=1 entry left
                BytesWritten = ttc_string_appendf( (t_u8*) Buffer, &BufferFree, "0x%02x, ", *Reader++);
            else
                BytesWritten = ttc_string_appendf( (t_u8*) Buffer, &BufferFree, "0x%02x};", *Reader++);
            reportFunction(Buffer, BytesWritten);
        }
        break;
    }
    case 2: {
        BytesWritten = ttc_string_appendf( (t_u8*) Buffer, &BufferFree, "%02i) t_u16 %s[%i] = {", register_stm32w1xx_report_token_Index, Name, AmountEntries);
        reportFunction(Buffer, BytesWritten);

        t_u16* Reader = (t_u16*) Register;
        while (AmountEntries) {
            BufferFree = register_stm32w1xx_report_token_BUFFERSIZE;
            if (AmountEntries-- > 1) // still >=1 entry left
                BytesWritten = ttc_string_appendf( (t_u8*) Buffer, &BufferFree, "0x%04x, ", *Reader++);
            else
                BytesWritten = ttc_string_appendf( (t_u8*) Buffer, &BufferFree, "0x%04x};", *Reader++);
            reportFunction(Buffer, BytesWritten);
        }
        break;
    }
    default: break;
    }

    BufferFree = register_stm32w1xx_report_token_BUFFERSIZE;
    BytesWritten = ttc_string_appendf( (t_u8*) Buffer, &BufferFree, " // 0x%08x\n", Address);
    reportFunction(Buffer, BytesWritten);
}
void register_stm32w1xx_report_all_tokens(void (*reportFunction)(const char* Text, t_base Size)) {
    Assert_REGISTER(reportFunction, ttc_assert_origin_auto);

        t_u8 Buffer[50];
#define BigInfoBase 0x8040000
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_1V8_REG_VOLTAGE);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_1V8_REG_VOLTAGE",     Buffer, TOKEN_MFG_1V8_REG_VOLTAGE_SIZE,     (BigInfoBase | TOKEN_MFG_1V8_REG_VOLTAGE) + (TOKEN_MFG_1V8_REG_VOLTAGE_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_ANALOG_TRIM_BOOST);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_ANALOG_TRIM_BOOST",   Buffer, TOKEN_MFG_ANALOG_TRIM_BOOST_SIZE,   (BigInfoBase | TOKEN_MFG_ANALOG_TRIM_BOOST) + (TOKEN_MFG_ANALOG_TRIM_BOOST_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_ANALOG_TRIM_BOTH);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_ANALOG_TRIM_BOTH",    Buffer, TOKEN_MFG_ANALOG_TRIM_BOTH_SIZE,    (BigInfoBase | TOKEN_MFG_ANALOG_TRIM_BOTH) + (TOKEN_MFG_ANALOG_TRIM_BOTH_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_ANALOG_TRIM_NORMAL);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_ANALOG_TRIM_NORMAL",  Buffer, TOKEN_MFG_ANALOG_TRIM_NORMAL_SIZE,  (BigInfoBase | TOKEN_MFG_ANALOG_TRIM_NORMAL) + (TOKEN_MFG_ANALOG_TRIM_NORMAL_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_ASH_CONFIG);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_ASH_CONFIG",          Buffer, TOKEN_MFG_ASH_CONFIG_SIZE,          (BigInfoBase | TOKEN_MFG_ASH_CONFIG) + (TOKEN_MFG_ASH_CONFIG_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_BOARD_NAME);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_BOARD_NAME",          Buffer, TOKEN_MFG_BOARD_NAME_SIZE,          (BigInfoBase | TOKEN_MFG_BOARD_NAME) + (TOKEN_MFG_BOARD_NAME_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_BOOTLOAD_AES_KEY);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_BOOTLOAD_AES_KEY",    Buffer, TOKEN_MFG_BOOTLOAD_AES_KEY_SIZE,    (BigInfoBase | TOKEN_MFG_BOOTLOAD_AES_KEY) + (TOKEN_MFG_BOOTLOAD_AES_KEY_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_CBKE_DATA);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_CBKE_DATA",           Buffer, TOKEN_MFG_CBKE_DATA_SIZE,           (BigInfoBase | TOKEN_MFG_CBKE_DATA) + (TOKEN_MFG_CBKE_DATA_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_CHIP_DATA);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_CHIP_DATA",           Buffer, TOKEN_MFG_CHIP_DATA_SIZE,           (BigInfoBase | TOKEN_MFG_CHIP_DATA) + (TOKEN_MFG_CHIP_DATA_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_CIB_OBS);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_CIB_OBS",             Buffer, TOKEN_MFG_CIB_OBS_SIZE,             (BigInfoBase | TOKEN_MFG_CIB_OBS) + (TOKEN_MFG_CIB_OBS_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_CUSTOM_EUI_64);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_CUSTOM_EUI_64",       Buffer, TOKEN_MFG_CUSTOM_EUI_64_SIZE,       (BigInfoBase | TOKEN_MFG_CUSTOM_EUI_64) + (TOKEN_MFG_CUSTOM_EUI_64_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_CUSTOM_VERSION);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_CUSTOM_VERSION",      Buffer, TOKEN_MFG_CUSTOM_VERSION_SIZE,      (BigInfoBase | TOKEN_MFG_CUSTOM_VERSION) + (TOKEN_MFG_CUSTOM_VERSION_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_EUI_64);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_EUI_64",              Buffer, TOKEN_MFG_EUI_64_SIZE,              (BigInfoBase | TOKEN_MFG_EUI_64) + (TOKEN_MFG_EUI_64_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_EZSP_STORAGE);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_EZSP_STORAGE",        Buffer, TOKEN_MFG_EZSP_STORAGE_SIZE,        (BigInfoBase | TOKEN_MFG_EZSP_STORAGE) + (TOKEN_MFG_EZSP_STORAGE_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_FIB_CHECKSUM);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_FIB_CHECKSUM",        Buffer, TOKEN_MFG_FIB_CHECKSUM_SIZE,        (BigInfoBase | TOKEN_MFG_FIB_CHECKSUM) + (TOKEN_MFG_FIB_CHECKSUM_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_FIB_OBS);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_FIB_OBS",             Buffer, TOKEN_MFG_FIB_OBS_SIZE,             (BigInfoBase | TOKEN_MFG_FIB_OBS) + (TOKEN_MFG_FIB_OBS_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_FIB_VERSION);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_FIB_VERSION",         Buffer, TOKEN_MFG_FIB_VERSION_SIZE,         (BigInfoBase | TOKEN_MFG_FIB_VERSION) + (TOKEN_MFG_FIB_VERSION_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_INSTALLATION_CODE);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_INSTALLATION_CODE",   Buffer, TOKEN_MFG_INSTALLATION_CODE_SIZE,   (BigInfoBase | TOKEN_MFG_INSTALLATION_CODE) + (TOKEN_MFG_INSTALLATION_CODE_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_MANUF_ID);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_MANUF_ID",            Buffer, TOKEN_MFG_MANUF_ID_SIZE,            (BigInfoBase | TOKEN_MFG_MANUF_ID) + (TOKEN_MFG_MANUF_ID_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_OSC24M_BIAS_TRIM);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_OSC24M_BIAS_TRIM",    Buffer, TOKEN_MFG_OSC24M_BIAS_TRIM_SIZE,    (BigInfoBase | TOKEN_MFG_OSC24M_BIAS_TRIM) + (TOKEN_MFG_OSC24M_BIAS_TRIM_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_PART_DATA);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_PART_DATA",           Buffer, TOKEN_MFG_PART_DATA_SIZE,           (BigInfoBase | TOKEN_MFG_PART_DATA) + (TOKEN_MFG_PART_DATA_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_PHY_CONFIG);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_PHY_CONFIG",          Buffer, TOKEN_MFG_PHY_CONFIG_SIZE,          (BigInfoBase | TOKEN_MFG_PHY_CONFIG) + (TOKEN_MFG_PHY_CONFIG_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_REG_TRIM);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_REG_TRIM",            Buffer, TOKEN_MFG_REG_TRIM_SIZE,            (BigInfoBase | TOKEN_MFG_REG_TRIM) + (TOKEN_MFG_REG_TRIM_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_ST_EUI_64);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_ST_EUI_64",           Buffer, TOKEN_MFG_ST_EUI_64_SIZE,           (BigInfoBase | TOKEN_MFG_ST_EUI_64) + (TOKEN_MFG_ST_EUI_64_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_STRING);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_STRING",              Buffer, TOKEN_MFG_STRING_SIZE,              (BigInfoBase | TOKEN_MFG_STRING) + (TOKEN_MFG_STRING_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_SYNTH_FREQ_OFFSET);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_SYNTH_FREQ_OFFSET",   Buffer, TOKEN_MFG_SYNTH_FREQ_OFFSET_SIZE,   (BigInfoBase | TOKEN_MFG_SYNTH_FREQ_OFFSET) + (TOKEN_MFG_SYNTH_FREQ_OFFSET_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_TEMP_CAL);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_TEMP_CAL",            Buffer, TOKEN_MFG_TEMP_CAL_SIZE,            (BigInfoBase | TOKEN_MFG_TEMP_CAL) + (TOKEN_MFG_TEMP_CAL_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_TESTER_DATA);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_TESTER_DATA",         Buffer, TOKEN_MFG_TESTER_DATA_SIZE,         (BigInfoBase | TOKEN_MFG_TESTER_DATA) + (TOKEN_MFG_TESTER_DATA_SIZE * 0x7F));
        halCommonGetMfgToken(&Buffer, TOKEN_MFG_VREF_VOLTAGE);
        register_stm32w1xx_report_token(reportFunction, 1, "TOKEN_MFG_VREF_VOLTAGE",        Buffer, TOKEN_MFG_VREF_VOLTAGE_SIZE,        (BigInfoBase | TOKEN_MFG_VREF_VOLTAGE) + (TOKEN_MFG_VREF_VOLTAGE_SIZE * 0x7F));

        /* List of all token values read from STM32W108 device

01) t_u8 TOKEN_MFG_1V8_REG_VOLTAGE[2] = {0xd8, 0x45}; // 0x080408dc
02) t_u8 TOKEN_MFG_ANALOG_TRIM_BOOST[20] = {0xf0, 0x01, 0x40, 0x2f, 0xf0, 0x01, 0xf0, 0x01, 0x28, 0x00, 0x35, 0x00, 0x35, 0x00, 0xf0, 0x01, 0x9a, 0x19, 0x35, 0x0a}; // 0x080411aa
03) t_u8 TOKEN_MFG_ANALOG_TRIM_BOTH[10] = {0x00, 0x04, 0xf2, 0x01, 0x35, 0x00, 0xf5, 0x01, 0xf2, 0x01}; // 0x08040cc8
04) t_u8 TOKEN_MFG_ANALOG_TRIM_NORMAL[20] = {0xf0, 0x01, 0x00, 0x01, 0xf0, 0x01, 0xf0, 0x11, 0x28, 0x00, 0x35, 0x00, 0x35, 0x00, 0xf0, 0x01, 0x35, 0x00, 0x35, 0x00}; // 0x08041196
05) t_u8 TOKEN_MFG_ASH_CONFIG[2] = {0xff, 0xff}; // 0x08040954
06) t_u8 TOKEN_MFG_BOARD_NAME[16] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x0804101a
07) t_u8 TOKEN_MFG_BOOTLOAD_AES_KEY[16] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x0804102e
08) t_u8 TOKEN_MFG_CBKE_DATA[92] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x08043622
09) t_u8 TOKEN_MFG_CHIP_DATA[24] = {0xaa, 0x55, 0xff, 0xff, 0x43, 0x32, 0x4b, 0x33, 0x36, 0x39, 0xff, 0xff, 0xff, 0xff, 0x25, 0x00, 0x39, 0x41, 0xff, 0xff, 0xaa, 0x55, 0xff, 0xff}; // 0x08041366
10) t_u8 TOKEN_MFG_CIB_OBS[16] = {0xa5, 0x5a, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0xff}; // 0x08040ff0
11) t_u8 TOKEN_MFG_CUSTOM_EUI_64[8] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x08040c0a
12) t_u8 TOKEN_MFG_CUSTOM_VERSION[2] = {0xff, 0xff}; // 0x0804090e
13) t_u8 TOKEN_MFG_EUI_64[8] = {0x55, 0x34, 0x1e, 0x00, 0x02, 0xe1, 0x80, 0x00}; // 0x080483f8
14) t_u8 TOKEN_MFG_EZSP_STORAGE[8] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x08040c46
15) t_u8 TOKEN_MFG_FIB_CHECKSUM[2] = {0xff, 0xff}; // 0x080408f4
16) t_u8 TOKEN_MFG_FIB_OBS[8] = {0x80, 0x7f, 0x02, 0xfd, 0x55, 0xaa, 0xff, 0xff}; // 0x08040bf0
17) t_u8 TOKEN_MFG_FIB_VERSION[2] = {0xfe, 0x01}; // 0x080408f2
18) t_u8 TOKEN_MFG_INSTALLATION_CODE[20] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x080412c6
19) t_u8 TOKEN_MFG_MANUF_ID[2] = {0xff, 0xff}; // 0x08040938
20) t_u8 TOKEN_MFG_OSC24M_BIAS_TRIM[2] = {0xff, 0xff}; // 0x080409ec
21) t_u8 TOKEN_MFG_PART_DATA[6] = {0x01, 0x00, 0x03, 0x00, 0x00, 0x01}; // 0x08040a90
22) t_u8 TOKEN_MFG_PHY_CONFIG[2] = {0xff, 0xff}; // 0x0804093a
23) t_u8 TOKEN_MFG_REG_TRIM[2] = {0x01, 0x04}; // 0x080408da
24) t_u8 TOKEN_MFG_ST_EUI_64[8] = {0x55, 0x34, 0x1e, 0x00, 0x02, 0xe1, 0x80, 0x00}; // 0x08040b9a
25) t_u8 TOKEN_MFG_STRING[16] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x0804100a
26) t_u8 TOKEN_MFG_SYNTH_FREQ_OFFSET[2] = {0xff, 0xff}; // 0x080409ee
27) t_u8 TOKEN_MFG_TEMP_CAL[2] = {0x80, 0x87}; // 0x080408e0
28) t_u8 TOKEN_MFG_TESTER_DATA[6] = {0xff, 0xff, 0x00, 0x00, 0x01, 0x00}; // 0x08040a96
29) t_u8 TOKEN_MFG_VREF_VOLTAGE[2] = {0xe7, 0x33}; // 0x080408de

*/
    /* List of all token values read from Dizic Module equipped with an STM32W108 device

01) t_u8 TOKEN_MFG_1V8_REG_VOLTAGE[2] = {0xc7, 0x46}; // 0x080408dc
02) t_u8 TOKEN_MFG_ANALOG_TRIM_BOOST[20] = {0x36, 0x01, 0x80, 0x2e, 0x36, 0x01, 0x36, 0x01, 0x2e, 0x00, 0x3b, 0x00, 0x3b, 0x00, 0x36, 0x01, 0x80, 0x19, 0x3b, 0x0a}; // 0x080411aa
03) t_u8 TOKEN_MFG_ANALOG_TRIM_BOTH[10] = {0x31, 0x06, 0xf4, 0x00, 0x3b, 0x00, 0x3b, 0x01, 0x38, 0x01}; // 0x08040cc8
04) t_u8 TOKEN_MFG_ANALOG_TRIM_NORMAL[20] = {0x36, 0x01, 0x40, 0x00, 0x36, 0x01, 0x36, 0x11, 0x2e, 0x00, 0x3b, 0x00, 0x3b, 0x00, 0x36, 0x01, 0x00, 0x00, 0x3b, 0x00}; // 0x08041196
05) t_u8 TOKEN_MFG_ASH_CONFIG[2] = {0xff, 0xff}; // 0x08040954
06) t_u8 TOKEN_MFG_BOARD_NAME[16] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x0804101a
07) t_u8 TOKEN_MFG_BOOTLOAD_AES_KEY[16] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x0804102e
08) t_u8 TOKEN_MFG_CBKE_DATA[92] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x08043622
09) t_u8 TOKEN_MFG_CHIP_DATA[24] = {0xaa, 0x55, 0xff, 0xff, 0x43, 0x32, 0x48, 0x32, 0x37, 0x37, 0xff, 0xff, 0xff, 0xff, 0x10, 0x00, 0x66, 0x36, 0xff, 0xff, 0xaa, 0x55, 0xff, 0xff}; // 0x08041366
10) t_u8 TOKEN_MFG_CIB_OBS[16] = {0xa5, 0x5a, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0xff}; // 0x08040ff0
11) t_u8 TOKEN_MFG_CUSTOM_EUI_64[8] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x08040c0a
12) t_u8 TOKEN_MFG_CUSTOM_VERSION[2] = {0xff, 0xff}; // 0x0804090e
13) t_u8 TOKEN_MFG_EUI_64[8] = {0x38, 0xd8, 0x1b, 0x00, 0x02, 0xe1, 0x80, 0x00}; // 0x080483f8
14) t_u8 TOKEN_MFG_EZSP_STORAGE[8] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x08040c46
15) t_u8 TOKEN_MFG_FIB_CHECKSUM[2] = {0xff, 0xff}; // 0x080408f4
16) t_u8 TOKEN_MFG_FIB_OBS[8] = {0x80, 0x7f, 0x02, 0xfd, 0x55, 0xaa, 0xff, 0xff}; // 0x08040bf0
17) t_u8 TOKEN_MFG_FIB_VERSION[2] = {0xfe, 0x01}; // 0x080408f2
18) t_u8 TOKEN_MFG_INSTALLATION_CODE[20] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x080412c6
19) t_u8 TOKEN_MFG_MANUF_ID[2] = {0xff, 0xff}; // 0x08040938
20) t_u8 TOKEN_MFG_OSC24M_BIAS_TRIM[2] = {0xff, 0xff}; // 0x080409ec
21) t_u8 TOKEN_MFG_PART_DATA[6] = {0x01, 0x00, 0x03, 0x00, 0x00, 0xff}; // 0x08040a90
22) t_u8 TOKEN_MFG_PHY_CONFIG[2] = {0xff, 0xff}; // 0x0804093a
23) t_u8 TOKEN_MFG_REG_TRIM[2] = {0x02, 0x05}; // 0x080408da
24) t_u8 TOKEN_MFG_ST_EUI_64[8] = {0x38, 0xd8, 0x1b, 0x00, 0x02, 0xe1, 0x80, 0x00}; // 0x08040b9a
25) t_u8 TOKEN_MFG_STRING[16] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x0804100a
26) t_u8 TOKEN_MFG_SYNTH_FREQ_OFFSET[2] = {0xff, 0xff}; // 0x080409ee
27) t_u8 TOKEN_MFG_TEMP_CAL[2] = {0x00, 0x88}; // 0x080408e0
28) t_u8 TOKEN_MFG_TESTER_DATA[6] = {0xff, 0xff, 0x00, 0x00, 0x01, 0x00}; // 0x08040a96
29) t_u8 TOKEN_MFG_VREF_VOLTAGE[2] = {0x5e, 0x31}; // 0x080408de

*/
    /* List of all token values read from ETRX357Module equipped with an EM357 device

01) t_u8 TOKEN_MFG_1V8_REG_VOLTAGE[2] = {0x74, 0x46}; // 0x080408dc
02) t_u8 TOKEN_MFG_ANALOG_TRIM_BOOST[20] = {0xef, 0x01, 0x5a, 0x3f, 0xef, 0x01, 0xef, 0x11, 0x27, 0x00, 0x34, 0x00, 0x34, 0x00, 0xef, 0x01, 0x99, 0x59, 0x34, 0x00}; // 0x080411aa
03) t_u8 TOKEN_MFG_ANALOG_TRIM_BOTH[10] = {0xef, 0x01, 0xf1, 0x01, 0xb4, 0x0d, 0x34, 0x01, 0xf1, 0x01}; // 0x08040cc8
04) t_u8 TOKEN_MFG_ANALOG_TRIM_NORMAL[20] = {0xef, 0x01, 0x1a, 0x01, 0xef, 0x01, 0xef, 0x11, 0x27, 0x00, 0x34, 0x00, 0x34, 0x00, 0xef, 0x01, 0x34, 0x40, 0x34, 0x00}; // 0x08041196
05) t_u8 TOKEN_MFG_ASH_CONFIG[2] = {0xff, 0xff}; // 0x08040954
06) t_u8 TOKEN_MFG_BOARD_NAME[16] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x0804101a
07) t_u8 TOKEN_MFG_BOOTLOAD_AES_KEY[16] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x0804102e
08) t_u8 TOKEN_MFG_CBKE_DATA[92] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x08043622
09) t_u8 TOKEN_MFG_CHIP_DATA[24] = {0xaa, 0x55, 0x1d, 0x3b, 0x30, 0x35, 0x38, 0x56, 0x32, 0x43, 0xff, 0xff, 0xff, 0xff, 0x04, 0x00, 0x39, 0x1d, 0x15, 0x07, 0xaa, 0x55, 0x23, 0x3b}; // 0x08041366
10) t_u8 TOKEN_MFG_CIB_OBS[16] = {0xa5, 0x5a, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0xff}; // 0x08040ff0
11) t_u8 TOKEN_MFG_CUSTOM_EUI_64[8] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x08040c0a
12) t_u8 TOKEN_MFG_CUSTOM_VERSION[2] = {0xff, 0xff}; // 0x0804090e
13) t_u8 TOKEN_MFG_EUI_64[8] = {0x92, 0x98, 0x96, 0x01, 0x00, 0x6f, 0x0d, 0x00}; // 0x080483f8
14) t_u8 TOKEN_MFG_EZSP_STORAGE[8] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x08040c46
15) t_u8 TOKEN_MFG_FIB_CHECKSUM[2] = {0xef, 0xc6}; // 0x080408f4
16) t_u8 TOKEN_MFG_FIB_OBS[8] = {0xff, 0xff, 0x03, 0xfc, 0x55, 0xaa, 0xff, 0xff}; // 0x08040bf0
17) t_u8 TOKEN_MFG_FIB_VERSION[2] = {0xfe, 0x01}; // 0x080408f2
18) t_u8 TOKEN_MFG_INSTALLATION_CODE[20] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x080412c6
19) t_u8 TOKEN_MFG_MANUF_ID[2] = {0xff, 0xff}; // 0x08040938
20) t_u8 TOKEN_MFG_OSC24M_BIAS_TRIM[2] = {0xff, 0xff}; // 0x080409ec
21) t_u8 TOKEN_MFG_PART_DATA[6] = {0x00, 0x01, 0x00, 0x02, 0x00, 0x80}; // 0x08040a90
22) t_u8 TOKEN_MFG_PHY_CONFIG[2] = {0xff, 0xff}; // 0x0804093a
23) t_u8 TOKEN_MFG_REG_TRIM[2] = {0x01, 0x04}; // 0x080408da
24) t_u8 TOKEN_MFG_ST_EUI_64[8] = {0x92, 0x98, 0x96, 0x01, 0x00, 0x6f, 0x0d, 0x00}; // 0x08040b9a
25) t_u8 TOKEN_MFG_STRING[16] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}; // 0x0804100a
26) t_u8 TOKEN_MFG_SYNTH_FREQ_OFFSET[2] = {0xff, 0xff}; // 0x080409ee
27) t_u8 TOKEN_MFG_TEMP_CAL[2] = {0x0a, 0x46}; // 0x080408e0
28) t_u8 TOKEN_MFG_TESTER_DATA[6] = {0x69, 0x3b, 0x03, 0x00, 0x0a, 0x05}; // 0x08040a96
29) t_u8 TOKEN_MFG_VREF_VOLTAGE[2] = {0x38, 0x2f}; // 0x080408de

*/
    /* List of token values being different among three different sample boards

TOKEN_MFG_1V8_REG_VOLTAGE
    etrx357: 01) t_u8 TOKEN_MFG_1V8_REG_VOLTAGE[2] = {0x74, 0x46}; // 0x080408dc
    stm32w1: 01) t_u8 TOKEN_MFG_1V8_REG_VOLTAGE[2] = {0xd8, 0x45}; // 0x080408dc
    dizic:   01) t_u8 TOKEN_MFG_1V8_REG_VOLTAGE[2] = {0xc7, 0x46}; // 0x080408dc
TOKEN_MFG_ANALOG_TRIM_BOOST
    etrx357: 02) t_u8 TOKEN_MFG_ANALOG_TRIM_BOOST[20] = {0xef, 0x01, 0x5a, 0x3f, 0xef, 0x01, 0xef, 0x11, 0x27, 0x00, 0x34, 0x00, 0x34, 0x00, 0xef, 0x01, 0x99, 0x59, 0x34, 0x00}; // 0x080411aa
    stm32w1: 02) t_u8 TOKEN_MFG_ANALOG_TRIM_BOOST[20] = {0xf0, 0x01, 0x40, 0x2f, 0xf0, 0x01, 0xf0, 0x01, 0x28, 0x00, 0x35, 0x00, 0x35, 0x00, 0xf0, 0x01, 0x9a, 0x19, 0x35, 0x0a}; // 0x080411aa
    dizic:   02) t_u8 TOKEN_MFG_ANALOG_TRIM_BOOST[20] = {0x36, 0x01, 0x80, 0x2e, 0x36, 0x01, 0x36, 0x01, 0x2e, 0x00, 0x3b, 0x00, 0x3b, 0x00, 0x36, 0x01, 0x80, 0x19, 0x3b, 0x0a}; // 0x080411aa
TOKEN_MFG_ANALOG_TRIM_BOTH
    etrx357: 03) t_u8 TOKEN_MFG_ANALOG_TRIM_BOTH[10] = {0xef, 0x01, 0xf1, 0x01, 0xb4, 0x0d, 0x34, 0x01, 0xf1, 0x01}; // 0x08040cc8
    stm32w1: 03) t_u8 TOKEN_MFG_ANALOG_TRIM_BOTH[10] = {0x00, 0x04, 0xf2, 0x01, 0x35, 0x00, 0xf5, 0x01, 0xf2, 0x01}; // 0x08040cc8
    dizic:   03) t_u8 TOKEN_MFG_ANALOG_TRIM_BOTH[10] = {0x31, 0x06, 0xf4, 0x00, 0x3b, 0x00, 0x3b, 0x01, 0x38, 0x01}; // 0x08040cc8
TOKEN_MFG_ANALOG_TRIM_NORMAL
    etrx357: 04) t_u8 TOKEN_MFG_ANALOG_TRIM_NORMAL[20] = {0xef, 0x01, 0x1a, 0x01, 0xef, 0x01, 0xef, 0x11, 0x27, 0x00, 0x34, 0x00, 0x34, 0x00, 0xef, 0x01, 0x34, 0x40, 0x34, 0x00}; // 0x08041196
    stm32w1: 04) t_u8 TOKEN_MFG_ANALOG_TRIM_NORMAL[20] = {0xf0, 0x01, 0x00, 0x01, 0xf0, 0x01, 0xf0, 0x11, 0x28, 0x00, 0x35, 0x00, 0x35, 0x00, 0xf0, 0x01, 0x35, 0x00, 0x35, 0x00}; // 0x08041196
    dizic:   04) t_u8 TOKEN_MFG_ANALOG_TRIM_NORMAL[20] = {0x36, 0x01, 0x40, 0x00, 0x36, 0x01, 0x36, 0x11, 0x2e, 0x00, 0x3b, 0x00, 0x3b, 0x00, 0x36, 0x01, 0x00, 0x00, 0x3b, 0x00}; // 0x08041196
TOKEN_MFG_CHIP_DATA
    etrx357: 09) t_u8 TOKEN_MFG_CHIP_DATA[24] = {0xaa, 0x55, 0x1d, 0x3b, 0x30, 0x35, 0x38, 0x56, 0x32, 0x43, 0xff, 0xff, 0xff, 0xff, 0x04, 0x00, 0x39, 0x1d, 0x15, 0x07, 0xaa, 0x55, 0x23, 0x3b}; // 0x08041366
    stm32w1: 09) t_u8 TOKEN_MFG_CHIP_DATA[24] = {0xaa, 0x55, 0xff, 0xff, 0x43, 0x32, 0x4b, 0x33, 0x36, 0x39, 0xff, 0xff, 0xff, 0xff, 0x25, 0x00, 0x39, 0x41, 0xff, 0xff, 0xaa, 0x55, 0xff, 0xff}; // 0x08041366
    dizic:   09) t_u8 TOKEN_MFG_CHIP_DATA[24] = {0xaa, 0x55, 0xff, 0xff, 0x43, 0x32, 0x48, 0x32, 0x37, 0x37, 0xff, 0xff, 0xff, 0xff, 0x10, 0x00, 0x66, 0x36, 0xff, 0xff, 0xaa, 0x55, 0xff, 0xff}; // 0x08041366
TOKEN_MFG_EUI_64
    etrx357: 13) t_u8 TOKEN_MFG_EUI_64[8] = {0x92, 0x98, 0x96, 0x01, 0x00, 0x6f, 0x0d, 0x00}; // 0x080483f8
    stm32w1: 13) t_u8 TOKEN_MFG_EUI_64[8] = {0x55, 0x34, 0x1e, 0x00, 0x02, 0xe1, 0x80, 0x00}; // 0x080483f8
    dizic:   13) t_u8 TOKEN_MFG_EUI_64[8] = {0x38, 0xd8, 0x1b, 0x00, 0x02, 0xe1, 0x80, 0x00}; // 0x080483f8
TOKEN_MFG_FIB_CHECKSUM
    etrx357: 15) t_u8 TOKEN_MFG_FIB_CHECKSUM[2] = {0xef, 0xc6}; // 0x080408f4
    stm32w1: 15) t_u8 TOKEN_MFG_FIB_CHECKSUM[2] = {0xff, 0xff}; // 0x080408f4
    dizic:   15) t_u8 TOKEN_MFG_FIB_CHECKSUM[2] = {0xff, 0xff}; // 0x080408f4
TOKEN_MFG_FIB_OBS
    etrx357: 16) t_u8 TOKEN_MFG_FIB_OBS[8] = {0xff, 0xff, 0x03, 0xfc, 0x55, 0xaa, 0xff, 0xff}; // 0x08040bf0
    stm32w1: 16) t_u8 TOKEN_MFG_FIB_OBS[8] = {0x80, 0x7f, 0x02, 0xfd, 0x55, 0xaa, 0xff, 0xff}; // 0x08040bf0
    dizic:   16) t_u8 TOKEN_MFG_FIB_OBS[8] = {0x80, 0x7f, 0x02, 0xfd, 0x55, 0xaa, 0xff, 0xff}; // 0x08040bf0
TOKEN_MFG_PART_DATA
    etrx357: 21) t_u8 TOKEN_MFG_PART_DATA[6] = {0x00, 0x01, 0x00, 0x02, 0x00, 0x80}; // 0x08040a90
    stm32w1: 21) t_u8 TOKEN_MFG_PART_DATA[6] = {0x01, 0x00, 0x03, 0x00, 0x00, 0x01}; // 0x08040a90
    dizic:   21) t_u8 TOKEN_MFG_PART_DATA[6] = {0x01, 0x00, 0x03, 0x00, 0x00, 0xff}; // 0x08040a90
TOKEN_MFG_REG_TRIM
    etrx357: 23) t_u8 TOKEN_MFG_REG_TRIM[2] = {0x01, 0x04}; // 0x080408da
    stm32w1: 23) t_u8 TOKEN_MFG_REG_TRIM[2] = {0x01, 0x04}; // 0x080408da
    dizic:   23) t_u8 TOKEN_MFG_REG_TRIM[2] = {0x02, 0x05}; // 0x080408da
TOKEN_MFG_ST_EUI_64
    etrx357: 24) t_u8 TOKEN_MFG_ST_EUI_64[8] = {0x92, 0x98, 0x96, 0x01, 0x00, 0x6f, 0x0d, 0x00}; // 0x08040b9a
    stm32w1: 24) t_u8 TOKEN_MFG_ST_EUI_64[8] = {0x55, 0x34, 0x1e, 0x00, 0x02, 0xe1, 0x80, 0x00}; // 0x08040b9a
    dizic:   24) t_u8 TOKEN_MFG_ST_EUI_64[8] = {0x38, 0xd8, 0x1b, 0x00, 0x02, 0xe1, 0x80, 0x00}; // 0x08040b9a
TOKEN_MFG_TEMP_CAL
    etrx357: 27) t_u8 TOKEN_MFG_TEMP_CAL[2] = {0x0a, 0x46}; // 0x080408e0
    stm32w1: 27) t_u8 TOKEN_MFG_TEMP_CAL[2] = {0x80, 0x87}; // 0x080408e0
    dizic:   27) t_u8 TOKEN_MFG_TEMP_CAL[2] = {0x00, 0x88}; // 0x080408e0
TOKEN_MFG_TESTER_DATA
    etrx357: 28) t_u8 TOKEN_MFG_TESTER_DATA[6] = {0x69, 0x3b, 0x03, 0x00, 0x0a, 0x05}; // 0x08040a96
    stm32w1: 28) t_u8 TOKEN_MFG_TESTER_DATA[6] = {0xff, 0xff, 0x00, 0x00, 0x01, 0x00}; // 0x08040a96
    dizic:   28) t_u8 TOKEN_MFG_TESTER_DATA[6] = {0xff, 0xff, 0x00, 0x00, 0x01, 0x00}; // 0x08040a96
TOKEN_MFG_VREF_VOLTAGE
    etrx357: 29) t_u8 TOKEN_MFG_VREF_VOLTAGE[2] = {0x38, 0x2f}; // 0x080408de
    stm32w1: 29) t_u8 TOKEN_MFG_VREF_VOLTAGE[2] = {0xe7, 0x33}; // 0x080408de
    dizic:   29) t_u8 TOKEN_MFG_VREF_VOLTAGE[2] = {0x5e, 0x31}; // 0x080408de
*/
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
