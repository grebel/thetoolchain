#ifndef CORTEXM3_REGISTERS_H
#define CORTEXM3_REGISTERS_H

/*{ register_cortexm3.h ******************************************************

 Structure definitions of basic CortexM3-registers.

 written by Gregor Rebel 2012

}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

// Basic set of helper functions
#include "../ttc_basic_types.h"
#include "../ttc_assert.h"
//X #include "../ttc_register_types.h"

//} Includes
//{ Default Definitions *****************************************************

//{ CortexM3 Registers ******************************************************

typedef struct { // t_register_cortexm3_actlr Auxiliary Control Register (->PM0056 p.130)
    unsigned DISMCYCINT  : 1;   // =0: enable interruption latency of processor (load/store and multiply/divide operations)
    unsigned DISDEFWBUF  : 1;   // =0: enable write buffer use:  stores to memory is competed before next instruction
    unsigned DISFOLD     : 1;   // =0: enable IT instructions folding
    unsigned reserved1   : 13;
    unsigned reserved2   : 16;
} __attribute__( ( __packed__ ) ) t_register_cortexm3_actlr;
/* DEPRECATED
typedef struct { // t_register_cortexm3_syst_csr     SysTick Control and Status Register
    unsigned ENABLE    :  1;    // ==1: enables systick counter
    unsigned TICKINT   :  1;    // ==1: counting down to zero asserts the SysTick exception request.
    unsigned CLKSOURCE :  1;    // ==1: processor clock; == 0: external clock
    unsigned reserved1 : 13;
    unsigned COUNTFLAG :  1;    //  Returns 1 if timer counted to 0 since last time this was read.
    unsigned reserved2 : 15;
} __attribute__((__packed__)) t_register_cortexm3_syst_csr;
typedef struct { // t_register_cortexm3_syst_rvr    SysTick Reload Value Register
    unsigned RELOAD    : 24;    //  Value to load into the SYST_CVR register when the counter is enabled and when it reaches 0,
    unsigned reserved1 : 8;
} __attribute__((__packed__)) t_register_cortexm3_syst_rvr;
typedef struct { // t_register_cortexm3_syst_cvr     SysTick Current Value Register
    unsigned CURRENT   : 24;    // Reads return the current value of the SysTick counter.
                                // A write of any value clears the field to 0, and also clears the SYST_CSR COUNTFLAG bit to 0.
    unsigned reserved1 : 8;
} __attribute__((__packed__)) t_register_cortexm3_syst_cvr;
typedef struct { // t_register_cortexm3_syst_calib   SysTick Calibration Value Register
    unsigned TENMS     : 24; //  Reload value for 10ms (100Hz) timing, subject to system clock skew errors.
                             // If the value reads as zero, the calibration value is not known.
    unsigned reserved1 :  6;
    unsigned SKEW      :  1; // =1: TENMS value is inexact, or not given.
    unsigned NOREF     :  1;//  = 0: reference clock provided to processor; =0: no reference clock provided

} __attribute__((__packed__)) t_register_cortexm3_syst_calib;
*/

typedef struct { // register_cortexm3_SCB_CPUID_t CPUID register -> PM0056 p.131
    unsigned Revision    : 4;  // Revision number (patch release) (0x0 = p0, 0x1 = p1)
    unsigned PartNo      : 12; // part number of the processor (0xc23 = Cortex-M3)
    unsigned Constant    : 4;  // reads as 0xf
    unsigned Variant     : 4;  // variant number (0x1 = r1, 0x2 = r2)
    unsigned Implementer : 8;  // Implementer code (0x41 = ARM)
} __attribute__( ( __packed__ ) )( register_cortexm3_SCB_CPUID_t );
typedef struct { // register_cortexm3_SCB_ICSR_t  Interrupt Control and State Register -> PM0056 p.132
    unsigned VECTACTIVE  : 9;  // Active vector; Contains the active exception number: 0: Thread mode/Other values: The exception number(1) of the currently active exception. Note: Subtract 16 from this value to obtain the IRQ number required to index into the Interrupt Clear-Enable, Set-Enable, Clear-Pending, Set-Pending, or Priority Registers, see Table 5 on page 19 in PM0056.
    unsigned reserved1   : 2;
    unsigned RETOBASE    : 1;  // Return to base level =0: There are preempted active exceptions to execute/ =1: There are no active exceptions, or the currently-executing exception is the only active exception.
    unsigned VECTPENDING : 10; // Pending vector; Indicates the exception number of the highest priority pending enabled exception. 0: No pending exceptions
    unsigned ISRPENDING  : 1;  // Interrupt pending flag, excluding NMI and Faults =0: Interrupt not pending/ =1: Interrupt pending
    unsigned DEBUG       : 1;  // This bit is reserved for Debug use and reads-as-zero when the processor is not in Debug.
    unsigned reserved2   : 1;
    unsigned PENDSTCLR   : 1;  // SysTick exception clear-pending bit. Write: 0: No effect/1: Removes the pending state from the SysTick exception.; This bit is write-only. On a register read its value is unknown.
    unsigned PENDSTSET   : 1;  // SysTick exception set-pending bit. Write: 0: No effect/1: Change SysTick exception state to pending; Read: 0: SysTick exception is not pending/1: SysTick exception is pending
    unsigned PENDSVCLR   : 1;  // PendSV clear-pending bit. Write: 0: No effect/1: Removes the pending state from the PendSV exception.
    unsigned PENDSVSET   : 1;  // PendSV set-pending bit. Write: 0: No effect/1: Change PendSV exception state to pending.; Read: 0: PendSV exception is not pending/ 1: PendSV exception is pending; Writing 1 to this bit is the only way to set the PendSV exception state to pending.
    unsigned reserved3   : 2;
    unsigned NMIPENDSET  : 1;  // NMI set-pending bit. Write: 0: No effect/1: Change NMI exception state to pending.; Read: 0: NMI exception is not pending/1: NMI exception is pending
} __attribute__( ( __packed__ ) )( register_cortexm3_SCB_ICSR_t );
typedef struct { // register_cortexm3_SCB_VTOR_t  Vector table offset register
    unsigned reserved1   : 2;
    unsigned TableOffset : 29; // offset address of table base from memory address 0x0 (bits 8:0 must be kept zero!)
    unsigned TableInSRAM : 1;  // =1: vector table is in SRAM; =0: VeectorTable is in Code
} __attribute__( ( __packed__ ) )( register_cortexm3_SCB_VTOR_t );

typedef enum { // valid values for register_cortexm3_SCB_AIRCR_t.PRIGROUP
    register_cortexm3_priority_group_none = 0,
    register_cortexm3_priority_group_4 = 0b011, // 16 group priorities and 1 sub priority
    register_cortexm3_priority_group_3 = 0b100, //  8 group priorities and 2 sub priorities
    register_cortexm3_priority_group_2 = 0b101, //  4 group priorities and 4 sub priorities
    register_cortexm3_priority_group_1 = 0b110, //  2 group priorities and 8 sub priorities
    register_cortexm3_priority_group_0 = 0b111, //  1 group priority   and 16 sub priorities
    register_cortexm3_priority_group_unknown
} e_register_cortexm3_aircr_prigroup;
typedef struct { // register_cortexm3_SCB_AIRCR_t Application interrupt and reset control register -> http://infocenter.arm.com/help/topic/com.arm.doc.dui0552a/Cihehdge.html#BABIJABJ
    unsigned VECTRESET      : 1;  // Reserved for Debug use. This bit reads as 0. When writing to the register you must write 0 to this bit, otherwise behavior is unpredictable.
    unsigned VECTCLRACTIVE  : 1;  // Reserved for Debug use. This bit reads as 0. When writing to the register you must write 0 to this bit, otherwise behavior is unpredictable.
    unsigned SYSRESETREQ    : 1;  // System reset request. This is intended to force a large system reset of all major components except for debug. write 1: Asserts a signal to the outer system that requests a reset.
    unsigned reserved1      : 5;
    unsigned PRIGROUP       : 3;  /* Interrupt priority grouping field. This field determines the split of group priority from subpriority, see Binary point on PM0056 p.135.
                                     Interrupt priority level value, PRI_N[7:0]             Number of
                                     PRIGROUP   Binary point [a]    Group priority bits Subpriority bits    Group priorities    Subpriorities
                                      0b000    bxxxxxxx.y             [7:1]                 [0]             128                2
                                      0b001    bxxxxxx.yy             [7:2]                 [1:0]            64                4
                                      0b010    bxxxxx.yyy             [7:3]                 [2:0]            32                8
                                      0b011    bxxxx.yyyy             [7:4]                 [3:0]            16               16
                                      0b100    bxxx.yyyyy             [7:5]                 [4:0]             8               32
                                      0b101    bxx.yyyyyy             [7:6]                 [5:0]             4               64
                                      0b110    bx.yyyyyyy             [7]                    [6:0]             2              128
                                      0b111    b.yyyyyyyy             None                   [7:0]             1              256
                                  */
    unsigned reserved2      : 4;
    unsigned ENDIANESS      : 1;  // Data endianness bit. Reads as 0 = Little-endian
    unsigned VECTKEYSTAT    : 16; // VECTKEY[15:0] Register key. Reads as 0xFA05. On writes, write 0x5FA to VECTKEY, otherwise the write is ignored.
} __attribute__( ( __packed__ ) )( register_cortexm3_SCB_AIRCR_t );
typedef struct { // t_register_cortexm3_scb_scr   System control register
    unsigned reserved1    : 1;
    unsigned SLEEPONEXIT  : 1;   // =1: Enter sleep, or deep sleep, on return from an interrupt service routine.
    unsigned SLEEPDEEP    : 1;   // =1: use deep sleep as low power mode
    unsigned reserved2    : 1;
    unsigned SEVEONPEND   : 1;   // =1: Enabled events and all interrupts, including disabled interrupts, can wakeup the processor; only enabled events or interrupts otherwise
    unsigned reserved3    : 27;
} __attribute__( ( __packed__ ) ) t_register_cortexm3_scb_scr;
typedef struct { // t_register_cortexm3_scb_ccr   Configuration and control register
    unsigned NONBASETHRDENA : 1;  // =1: Processor can enter Thread mode from any level under the control of an EXC_RETURN; =0: only when no exception is active
    unsigned USERSETMPEND   : 1;  // =1: Enables unprivileged software access to the STIR, see Software trigger interrupt register (register_cortexm3_NVIC_STIR) on page 127
    unsigned reserved1      : 1;
    unsigned UNALIGN_TRP    : 1;  // =1: Trap unaligned halfword and word accesses
    unsigned DIV_0_TRP      : 1;  // =1: Enable faulting or halting when the processor executes an SDIV or UDIV instruction with a divisor of 0
    unsigned reserved2      : 3;
    unsigned BFHFNMIGN      : 1;  // =1: Handlers running at priority -1 and -2 ignore data bus faults caused by load and store instructions
    unsigned STKALIGN       : 1;  // =1: stack is 4-byte aligned; =1: stack is 8-byte aligned
    unsigned reserved3      : 22;
} __attribute__( ( __packed__ ) ) t_register_cortexm3_scb_ccr;
typedef struct { // register_cortexm3_SCB_SHPR1_t System handler priority register 1
    unsigned PRI_4_MemoryManagementFault : 8; // Priority of system handler 4, memory management fault
    unsigned PRI_5_BusFault              : 8; // Priority of system handler 5, bus fault
    unsigned PRI_6_UsageFault            : 8; // Priority of system handler 6, usage fault
    unsigned reserved1                   : 8;
} __attribute__( ( __packed__ ) )( register_cortexm3_SCB_SHPR1_t );
typedef struct { // register_cortexm3_SCB_SHPR2_t System handler priority register 2
    unsigned reserved1                   : 24;
    unsigned PRI_11_SVCall               : 8; // Priority of system handler 11, SVCall
} __attribute__( ( __packed__ ) )( register_cortexm3_SCB_SHPR2_t );
typedef struct { // register_cortexm3_SCB_SHPR3_t System handler priority register 3
    unsigned reserved1                   : 16;
    unsigned PRI_15_SysTickException     : 8; // Priority of system handler 15, SysTick exception
    unsigned PRI_14_PendSV               : 8; // Priority of system handler 14, PendSV
} __attribute__( ( __packed__ ) )( register_cortexm3_SCB_SHPR3_t );
typedef struct { // t_register_cortexm3_scb_shcsr System handler control and state register (->PM0056 p.140)
    unsigned MEMFAULTACT    : 1; // Memory management fault exception active bit, reads as 1 if exception is active
    unsigned BUSFAULTACT    : 1; // Bus fault exception active bit, reads as 1 if exception is active
    unsigned reserved1      : 1;
    unsigned USGFAULTACT    : 1; // Usage fault exception active bit, reads as 1 if exception is active
    unsigned reserved2      : 3;
    unsigned SVCALLACT      : 1; // SVC call active bit, reads as 1 if SVC call is active
    unsigned MONITORACT     : 1; // Debug monitor active bit, reads as 1 if Debug monitor is active
    unsigned reserved3      : 1;
    unsigned PENDSVACT      : 1; // PendSV exception active bit, reads as 1 if exception is active
    unsigned SYSTICKACT     : 1; // SysTick exception active bit, reads as 1 if exception is active
    unsigned USGFAULTPENDED : 1; // Usage fault exception pending bit, reads as 1 if exception is pending
    unsigned MEMFAULTPENDED : 1; // Memory management fault exception pending bit, reads as 1 if exception is pending
    unsigned BUSFAULTPENDED : 1; // Bus fault exception pending bit, reads as 1 if exception is pending
    unsigned SVCALLPENDED   : 1; // SVC call pending bit, reads as 1 if exception is pending
    unsigned MEMFAULTENA    : 1; // Memory management fault enable bit, set to 1 to enable
    unsigned BUSFAULTENA    : 1; // Bus fault enable bit, set to 1 to enable
    unsigned USGFAULTENA    : 1; // Usage fault enable bit, set to 1 to enable
    unsigned reserved4      : 13;
} __attribute__( ( __packed__ ) ) t_register_cortexm3_scb_shcsr;
typedef struct { // t_register_cortexm3_scb_cfsr_mmfsr  Memory Management Fault Status Register (->PM0056 p.142)
    unsigned IACCVIOL  : 1;   // =1: Instruction access violation flag
    unsigned DACCVIOL  : 1;   // =1: Data access violation flag
    unsigned reserved1 : 1;
    unsigned MUNSTKERR : 1;   // =1: Memory manager fault on unstacking for a return from exception
    unsigned MSTKERR   : 1;   // =1: Memory manager fault on stacking for exception entry
    unsigned reserved2 : 2;
    unsigned MMARVALID : 1;   // =1: Memory Management Fault Address Register (MMAR) is valid
} __attribute__( ( __packed__ ) ) t_register_cortexm3_scb_cfsr_mmfsr;
typedef struct { // t_register_cortexm3_scb_cfsr_bfsr   Bus Fault Status Register (->PM0056 p.142)
    unsigned IBUSERR     : 1;  // =1: Instruction bus error
    unsigned PRECISEERR  : 1;  // =1: Precise data bus error (faulting address in BFAR)
    unsigned IMPRECISERR : 1;  // =1: Imprecise data bus error (no stack frame or fault address known)
    unsigned UNSTKERR    : 1;  // =1: Bus fault on unstacking for a return from exception (original stack stil present)
    unsigned STKERR      : 1;  // =1: Bus fault on stacking for exception entry
    unsigned reserved1   : 2;
    unsigned BFARVALID  : 1;   // =1: Bus Fault Address Register (BFAR) is valid
} __attribute__( ( __packed__ ) ) t_register_cortexm3_scb_cfsr_bfsr;
typedef struct { // t_register_cortexm3_scb_cfsr_ufsr   Usage Fault Status Register (->PM0056 p.143)
    unsigned UNDEFINSTR  : 1;   // =1: Undefined instruction usage fault (processor has attempted to execute an undefined instruction)
    unsigned INVSTATE    : 1;   // =1: Invalid state usage fault (processor has attempted to execute an instruction that makes illegal use of the EPSR)
    unsigned INVPC       : 1;   // =1: Invalid PC load usage fault, caused by an invalid PC load by EXC_RETURN
    unsigned NOCP        : 1;   // =1: No coprocessor usage fault
    unsigned reserved1   : 4;
    unsigned UNALIGNED   : 1;   // =1: processor has made an unaligned memory access
    unsigned DIVBYZERO   : 1;   // =1:  processor has executed an SDIV or UDIV instruction with a divisor of 0
    unsigned reserved2   : 6;
} __attribute__( ( __packed__ ) ) t_register_cortexm3_scb_cfsr_ufsr;
typedef struct { // t_register_cortexm3_scb_cfsr Configurable fault status register (->PM0056 p.142)
    t_register_cortexm3_scb_cfsr_mmfsr MMFSR;          // Memory Management Fault Status Register
    t_register_cortexm3_scb_cfsr_bfsr  BFSR;           // Bus Fault Status Register
    t_register_cortexm3_scb_cfsr_ufsr  UFSR;           // Usage Fault Status Register
} __attribute__( ( __packed__ ) ) t_register_cortexm3_scb_cfsr;
typedef struct { // t_register_cortexm3_scb_hfsr Hard fault status register (->PM0056 p.145)
    unsigned reserved1   : 1;
    unsigned VectorTable : 1;   // =1: hardfault was caused by vector table read
    unsigned reserved2   : 28;
    unsigned Forced      : 1;   // =1: hardfault was forced by escalation of fault that could not be handled
    unsigned DebugVT     : 1;   // =1: reserved for debug use
} __attribute__( ( __packed__ ) ) t_register_cortexm3_scb_hfsr;

typedef struct { // t_register_cortexm3_scb      System Control Block
    /* DEPRECATED (ACTLR is located far away from rest of SCB registers so it was moved into separate variable register_cortexm3_ACTLR)
     *
                                    t_u8 reserved1[8];
      volatile t_register_cortexm3_actlr  ACLTR;  // Offset: 0x008  Auxiliary Control Register
                                         t_u8 reserved2[4];
      volatile t_register_cortexm3_syst_csr   SYST_CSR;    //  SysTick Control and Status Register
      volatile t_register_cortexm3_syst_rvr   SYST_RVR;    //  SysTick Reload Value Register
      volatile t_register_cortexm3_syst_cvr   SYST_CVR;    //  SysTick Current Value Register
      volatile t_register_cortexm3_syst_calib SYST_CALIB;  //  SysTick Calibration Value Register
                                unsigned char reserved3[0xd00 - 32];
    */
    volatile register_cortexm3_SCB_CPUID_t  CPUID;  // Offset: 0x100  CPU ID Base Register
    volatile register_cortexm3_SCB_ICSR_t   ICSR;   // Offset: 0x104  Interrupt Control State Register
    volatile register_cortexm3_SCB_VTOR_t   VTOR;   // Offset: 0x108  Vector Table Offset Register
    volatile register_cortexm3_SCB_AIRCR_t  AIRCR;  // Offset: 0x10C  Application Interrupt / Reset Control Register
    volatile t_register_cortexm3_scb_scr    SCR;    // Offset: 0x110  System Control Register
    volatile t_register_cortexm3_scb_ccr    CCR;    // Offset: 0x114  Configuration Control Register
    volatile register_cortexm3_SCB_SHPR1_t  SHPR1;  // Offset: 0x118  System Handlers Priority Register 1
    volatile register_cortexm3_SCB_SHPR2_t  SHPR2;  // Offset: 0x11c  System Handlers Priority Register 2
    volatile register_cortexm3_SCB_SHPR3_t  SHPR3;  // Offset: 0x120  System Handlers Priority Register 3
    volatile t_register_cortexm3_scb_shcsr  SHCSR;  // Offset: 0x124  System Handler Control and State Register
    volatile t_register_cortexm3_scb_cfsr   CFSR;   // Offset: 0x128  Configurable Fault Status Register
    volatile t_register_cortexm3_scb_hfsr   HFSR;   // Offset: 0x12C  Hard Fault Status Register

    volatile t_u32 DFSR;                         // Offset: 0x30  Debug Fault Status Register
    volatile t_u32 MMFAR;                        // Offset: 0x34  Mem Manage Address Register
    volatile t_u32 BFAR;                         // Offset: 0x38  Bus Fault Address Register

    // no covered by PM0056.. (copied from CMSIS/.../core_cm3.h)
    volatile t_u32 AFSR;                         // Offset: 0x3C  Auxiliary Fault Status Register
    volatile t_u32 PFR[2];                       // Offset: 0x40  Processor Feature Register
    volatile t_u32 DFR;                          // Offset: 0x48  Debug Feature Register
    volatile t_u32 ADR;                          // Offset: 0x4C  Auxiliary Feature Register
    volatile t_u32 MMFR[4];                      // Offset: 0x50  Memory Model Feature Register
    volatile t_u32 ISAR[5];                      // Offset: 0x60  ISA Feature Register
} __attribute__( ( __packed__ ) ) t_register_cortexm3_scb;

typedef struct { // t_register_cortexm3_nvic_interrupts0   interrupt lines #1 (common to most NVIC-registers)
    union {
        t_u32 All;

        struct {
            unsigned IRQ00     : 1;
            unsigned IRQ01     : 1;
            unsigned IRQ02     : 1;
            unsigned IRQ03     : 1;
            unsigned IRQ04     : 1;
            unsigned IRQ05     : 1;
            unsigned IRQ06     : 1;
            unsigned IRQ07     : 1;
            unsigned IRQ08     : 1;
            unsigned IRQ09     : 1;
            unsigned IRQ10     : 1;
            unsigned IRQ11     : 1;
            unsigned IRQ12     : 1;
            unsigned IRQ13     : 1;
            unsigned IRQ14     : 1;
            unsigned IRQ15     : 1;
            unsigned IRQ16     : 1;
            unsigned IRQ17     : 1;
            unsigned IRQ18     : 1;
            unsigned IRQ19     : 1;
            unsigned IRQ20     : 1;
            unsigned IRQ21     : 1;
            unsigned IRQ22     : 1;
            unsigned IRQ23     : 1;
            unsigned IRQ24     : 1;
            unsigned IRQ25     : 1;
            unsigned IRQ26     : 1;
            unsigned IRQ27     : 1;
            unsigned IRQ28     : 1;
            unsigned IRQ29     : 1;
            unsigned IRQ30     : 1;
            unsigned IRQ31     : 1;
        } Bits;
    };
} __attribute__( ( __packed__ ) ) t_register_cortexm3_nvic_interrupts0;
typedef struct { // t_register_cortexm3_nvic_interrupts1   interrupt lines #1 (common to most NVIC-registers)
    union {
        t_u32 All;

        struct {
            unsigned IRQ32     : 1;
            unsigned IRQ33     : 1;
            unsigned IRQ34     : 1;
            unsigned IRQ35     : 1;
            unsigned IRQ36     : 1;
            unsigned IRQ37     : 1;
            unsigned IRQ38     : 1;
            unsigned IRQ39     : 1;
            unsigned IRQ40     : 1;
            unsigned IRQ41     : 1;
            unsigned IRQ42     : 1;
            unsigned IRQ43     : 1;
            unsigned IRQ44     : 1;
            unsigned IRQ45     : 1;
            unsigned IRQ46     : 1;
            unsigned IRQ47     : 1;
            unsigned IRQ48     : 1;
            unsigned IRQ49     : 1;
            unsigned IRQ50     : 1;
            unsigned IRQ51     : 1;
            unsigned IRQ52     : 1;
            unsigned IRQ53     : 1;
            unsigned IRQ54     : 1;
            unsigned IRQ55     : 1;
            unsigned IRQ56     : 1;
            unsigned IRQ57     : 1;
            unsigned IRQ58     : 1;
            unsigned IRQ59     : 1;
            unsigned IRQ60     : 1;
            unsigned IRQ61     : 1;
            unsigned IRQ62     : 1;
            unsigned IRQ63     : 1;
        } Bits;
    };
} __attribute__( ( __packed__ ) ) t_register_cortexm3_nvic_interrupts1;
typedef struct { // t_register_cortexm3_nvic_interrupts2   interrupt lines #2 (common to most NVIC-registers)
    union {
        t_u32 All;

        struct {
            unsigned IRQ64     : 1;
            unsigned IRQ65     : 1;
            unsigned IRQ66     : 1;
            unsigned IRQ67     : 1;
            unsigned reserved1 : 28;
        } Bits;
    };
} __attribute__( ( __packed__ ) ) t_register_cortexm3_nvic_interrupts2;
typedef struct { // t_register_cortexm3_nvic_ip     Interrupt Priority Register (8Bit wide)
    union { // bit partitioning of this field depends on value of register_cortexm3_SCB.AIRCR.PRIGROUP
        t_u8 Byte;

        struct  { // PRIGROUP==0b000
            unsigned SubPriority   : 1;
            unsigned GroupPriority : 7;
        } Priority000;
        struct  { // PRIGROUP==0b001
            unsigned SubPriority   : 2;
            unsigned GroupPriority : 6;
        } Priority001;
        struct  { // PRIGROUP==0b010
            unsigned SubPriority   : 3;
            unsigned GroupPriority : 5;
        } Priority010;
        struct  { // PRIGROUP==0b011
            unsigned SubPriority   : 4;
            unsigned GroupPriority : 4;
        } Priority011;
        struct  { // PRIGROUP==0b100
            unsigned SubPriority   : 5;
            unsigned GroupPriority : 3;
        } Priority100;
        struct  { // PRIGROUP==0b101
            unsigned SubPriority   : 6;
            unsigned GroupPriority : 2;
        } Priority101;
        struct  { // PRIGROUP==0b110
            unsigned SubPriority   : 7;
            unsigned GroupPriority : 1;
        } Priority110;
        struct  { // PRIGROUP==0b111
            unsigned SubPriority   : 8;
        } Priority111;
    };
} __attribute__( ( __packed__ ) ) t_register_cortexm3_nvic_ip;
typedef struct { // t_register_cortexm3_nvic_stir   Software Trigger Interrupt Register (->PM0056 p.127)
    unsigned InterruptID : 8;          // software generated interrupt ID
    unsigned reserved1   : 8;
    unsigned reserved2   : 16;
} __attribute__( ( __packed__ ) ) t_register_cortexm3_nvic_stir;
typedef struct { // t_register_cortexm3_nvic        Nested Vector Interrupt Controller (->ST PM0056 p.119)
    volatile t_register_cortexm3_nvic_interrupts0 ISER0;               // Offset: 0x000  Interrupt Set Enable Register
    volatile t_register_cortexm3_nvic_interrupts1 ISER1;               // Offset: 0x000  Interrupt Set Enable Register
    volatile t_register_cortexm3_nvic_interrupts2 ISER2;               // Offset: 0x000  Interrupt Set Enable Register
    t_u32 reserved1[29];
    volatile t_register_cortexm3_nvic_interrupts0 ICER0;               // Offset: 0x080  Interrupt Clear Enable Register
    volatile t_register_cortexm3_nvic_interrupts1 ICER1;               // Offset: 0x084  Interrupt Clear Enable Register
    volatile t_register_cortexm3_nvic_interrupts2 ICER2;               // Offset: 0x088  Interrupt Clear Enable Register
    t_u32 reserved2[29];
    volatile t_register_cortexm3_nvic_interrupts0 ISPR0;               // Offset: 0x100  Interrupt Set Pending Register
    volatile t_register_cortexm3_nvic_interrupts1 ISPR1;               // Offset: 0x104  Interrupt Set Pending Register
    volatile t_register_cortexm3_nvic_interrupts2 ISPR2;               // Offset: 0x108  Interrupt Set Pending Register
    t_u32 reserved3[29];
    volatile t_register_cortexm3_nvic_interrupts0 ICPR0;               // Offset: 0x180  Interrupt Clear Pending Register
    volatile t_register_cortexm3_nvic_interrupts1 ICPR1;               // Offset: 0x184  Interrupt Clear Pending Register
    volatile t_register_cortexm3_nvic_interrupts2 ICPR2;               // Offset: 0x188  Interrupt Clear Pending Register
    t_u32 reserved4[29];
    volatile t_register_cortexm3_nvic_interrupts0 IABR0;               // Offset: 0x200  Interrupt Active bit Register
    volatile t_register_cortexm3_nvic_interrupts1 IABR1;               // Offset: 0x204  Interrupt Active bit Register
    volatile t_register_cortexm3_nvic_interrupts2 IABR2;               // Offset: 0x208  Interrupt Active bit Register
    t_u32 reserved5[61];
    volatile t_register_cortexm3_nvic_ip   IP[240];                    // Offset: 0x300  Interrupt Priority Register (8Bit wide)
    t_u32 reserved6[644];
    volatile t_register_cortexm3_nvic_stir STIR;                       // Offset: 0xE00  Software Trigger Interrupt Register

} __attribute__( ( __packed__ ) ) t_register_cortexm3_nvic;
typedef struct {                                                       // SysTick control and status register (STK_CTRL) (->PM0056 p. 149)
    union {
        t_u32 All;

        struct {
            unsigned Enable    : 1;   /* Counter enable
                                 0: Counter disabled
                                 1: Counter enabled
    */
            unsigned TickInt   : 1;   /* SysTick exception request enable
                                 0: Counting down to zero does not assert the SysTick exception request
                                 1: Counting down to zero to asserts the SysTick exception request.
                                 Note: Software can use COUNTFLAG to determine if SysTick has ever counted to zero.
    */
            unsigned ClkSource : 1;   /* Clock Source Selection (=0: AHB/8; =1: AHB)
                                 Selects the clock source.
                                 0: AHB/8
                                 1: Processor clock (AHB)
    */
            unsigned reserved1 : 13;
            unsigned CountFlag : 1;   /* Returns 1 if timer counted to 0 since last time this was read.
                                 The LOAD register specifies the start value to load into the VAL register when the counter is
                                 enabled and when it reaches 0.
                                 Calculating the RELOAD value
                                 The RELOAD value can be any value in the range 0x00000001-0x00FFFFFF. A start value of
                                 0 is possible, but has no effect because the SysTick exception request and COUNTFLAG are
                                 activated when counting from 1 to 0.
                                 The RELOAD value is calculated according to its use:
                                 + To generate a multi-shot timer with a period of N processor clock cycles, use a RELOAD
                                   value of N-1. For example, if the SysTick interrupt is required every 100 clock pulses, set
                                   RELOAD to 99.
                                 + To deliver a single SysTick interrupt after a delay of N processor clock cycles, use a
                                   RELOAD of value N. For example, if a SysTick interrupt is required after 400 clock
                                   pulses, set RELOAD to 400.
    */
            unsigned reserved2 : 15;
        } Bits;
    };
} __attribute__( ( __packed__ ) ) t_register_cortexm3_ctrl;
typedef struct {                                                      // SysTick reload value register (STK_LOAD) (-> PM0056 p.150)
    union {
        t_u32 All;

        struct {
            unsigned Reload   : 24;  // reload value
            unsigned reserved :  8;
        } Bits;
    };
} __attribute__( ( __packed__ ) ) t_register_cortexm3_load;
typedef struct {                                                      // SysTick current value register (STK_VAL) (-> PM0056 p.152)
    union {
        t_u32 All;

        struct {
            unsigned Current : 24;  /* current counter value
                               The VAL register contains the current value of the SysTick counter.
                               Reads return the current value of the SysTick counter.
                               A write of any value clears the field to 0, and also clears the COUNTFLAG bit in the
                               STK_CTRL register to 0.
                             */
            unsigned reserved :  8;
        } Bits;
    };
} __attribute__( ( __packed__ ) ) t_register_cortexm3_val;
typedef struct {                                                      // SysTick calibration value register (STK_CALIB) (->PM0056 p.152)
    union {
        t_u32 All;

        struct {
            unsigned TenMS     : 24; /* Calibration value
                                        Indicates the calibration value when the SysTick counter runs on HCLK max/8 as external
                                        clock. The value is product dependent, please refer to the Product Reference Manual, SysTick
                                        Calibration Value section. When HCLK is programmed at the maximum frequency, the SysTick
                                        period is 1ms.
                                        If calibration information is not known, calculate the calibration value required from the
                                        frequency of the processor clock or external clock.
                                    */
            unsigned reserved1 : 6;
            unsigned Skew      : 1;  /* SKEW flag
                                        Reads as one. Calibration value for the 1 ms inexact timing is not known because TENMS is
                                        not known. This can affect the suitability of SysTick as a software real time clock.
                                     */
            unsigned NoRef     : 1;  /* NOREF flag
                                        Reads as zero. Indicates that a separate reference clock is provided. The frequency of this
                                        clock is HCLK/8.
                                     */
        } Bits;
    };
} __attribute__( ( __packed__ ) ) t_register_cortexm3_calib;
typedef struct {
    volatile t_register_cortexm3_ctrl  CTRL;                           // Offset: 0x00  SysTick Control and Status Register
    volatile t_register_cortexm3_load  LOAD;                           // Offset: 0x04  SysTick Reload Value Register
    volatile t_register_cortexm3_val   VAL;                            // Offset: 0x08  SysTick Current Value Register
    volatile t_register_cortexm3_calib CALIB;                          // Offset: 0x0C  SysTick Calibration Register
} __attribute__( ( __packed__ ) ) t_register_cortexm3_stk;
//} CortexM3 Registers

// provided by register_cortexm3.c
extern volatile t_register_cortexm3_scb      register_cortexm3_SCB;   // System Control Block (PM0056 p.130)
extern volatile t_register_cortexm3_nvic     register_cortexm3_NVIC;  // Nested Vectored Interrupt Controller (PM0056 p.119)
extern volatile t_register_cortexm3_actlr    register_cortexm3_ACTLR; // Auxiliary Control Register (PM0056 p.130)
extern volatile t_register_cortexm3_stk      register_cortexm3_STK;   // SysTick register

void register_cortexm3_prepare();

#endif //CORTEXM3_REGISTERS_H
