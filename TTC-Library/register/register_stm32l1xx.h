#ifndef REGISTER_STM32L1XX_H
#define REGISTER_STM32L1XX_H

/** { register_stm32l1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for register devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by high-level register and application.
 *
 *  Created from template device_architecture.h revision 22 at 20140423 11:05:10 UTC
 *
 *  Note: See ttc_register.h for description of stm32l1xx independent REGISTER implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_REGISTER_STM32L1XX
//
// Implementation status of low-level driver support for register devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_REGISTER_STM32L1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_REGISTER_STM32L1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_REGISTER_STM32L1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_REGISTER_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "stm32l1xx.h" // required to be included before register_cortexm3.h (defines IRQn_Type)
#include "../register/register_cortexm3.h"
#include "register_stm32l1xx_types.h"
#include "../ttc_register_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_register_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_register_foo
//
#define ttc_driver_register_get_configuration() register_stm32l1xx_get_configuration()
#define ttc_driver_register_prepare() register_stm32l1xx_prepare()
#define ttc_driver_register_reset(Register) register_stm32l1xx_reset(Register)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
// Register Variable Declarations ***************************************{

extern volatile t_register_stm32l1xx_rcc         register_stm32l1xx_RCC;
extern volatile t_register_stm32l1xx_pwr         register_stm32l1xx_PWR;
extern volatile t_register_stm32l1xx_rtc         register_stm32l1xx_RTC;
extern volatile t_register_stm32l1xx_dac         register_stm32l1xx_DAC;
extern volatile t_register_stm32l1xx_dma         register_stm32l1xx_DMA1;
extern volatile t_register_stm32l1xx_dma_channel register_stm32l1xx_DMA_CHANNEL1;
extern volatile t_register_stm32l1xx_dma_channel register_stm32l1xx_DMA_CHANNEL2;
extern volatile t_register_stm32l1xx_dma_channel register_stm32l1xx_DMA_CHANNEL3;
extern volatile t_register_stm32l1xx_dma_channel register_stm32l1xx_DMA_CHANNEL4;
extern volatile t_register_stm32l1xx_dma_channel register_stm32l1xx_DMA_CHANNEL5;
extern volatile t_register_stm32l1xx_dma_channel register_stm32l1xx_DMA_CHANNEL6;
extern volatile t_register_stm32l1xx_dma_channel register_stm32l1xx_DMA_CHANNEL7;
extern volatile t_register_stm32l1xx_timer       register_stm32l1xx_TIMER2;
extern volatile t_register_stm32l1xx_timer       register_stm32l1xx_TIMER3;
extern volatile t_register_stm32l1xx_timer       register_stm32l1xx_TIMER4;
extern volatile t_register_stm32l1xx_timer       register_stm32l1xx_TIMER6;
extern volatile t_register_stm32l1xx_timer       register_stm32l1xx_TIMER7;
extern volatile t_register_stm32l1xx_timer       register_stm32l1xx_TIMER9;
extern volatile t_register_stm32l1xx_timer       register_stm32l1xx_TIMER10;
extern volatile t_register_stm32l1xx_timer       register_stm32l1xx_TIMER11;
extern volatile t_register_stm32l1xx_usart       register_stm32l1xx_USART1;
extern volatile t_register_stm32l1xx_usart       register_stm32l1xx_USART2;
extern volatile t_register_stm32l1xx_usart       register_stm32l1xx_USART3;
extern volatile t_register_stm32l1xx_usart       register_stm32l1xx_UART4;
extern volatile t_register_stm32l1xx_usart       register_stm32l1xx_UART5;
extern volatile t_register_stm32l1xx_spi         register_stm32l1xx_SPI1;
extern volatile t_register_stm32l1xx_spi         register_stm32l1xx_SPI2;
//extern volatile t_register_stm32l1xx_spi        register_stm32l1xx_SPI3;
extern volatile t_register_stm32l1xx_i2c         register_stm32l1xx_I2C1;
extern volatile t_register_stm32l1xx_i2c         register_stm32l1xx_I2C2;
//extern volatile register_stm32l1xx_can_t        register_stm32l1xx_CAN1;
//extern volatile register_stm32l1xx_can_t        register_stm32l1xx_CAN2;
extern volatile t_register_stm32l1xx_adc         register_stm32l1xx_ADC1;
extern volatile t_register_stm32l1xx_gpio        register_stm32l1xx_GPIOA;
extern volatile t_register_stm32l1xx_gpio        register_stm32l1xx_GPIOB;
extern volatile t_register_stm32l1xx_gpio        register_stm32l1xx_GPIOC;
extern volatile t_register_stm32l1xx_gpio        register_stm32l1xx_GPIOD;
extern volatile t_register_stm32l1xx_gpio        register_stm32l1xx_GPIOH;
//extern volatile register_stm32l1xx_afio_t       register_stm32l1xx_AFIO;
extern volatile t_register_stm32l1xx_exti        register_stm32l1xx_EXTI;
extern volatile DBGMCU_TypeDef                   register_stm32l1xx_DGBMCU;

extern volatile t_register_stm32l1xx_ri_ascr1    register_stm32l1xx_RI_ASCR1;   // RI analog switches control register (RI_ASCR1)  (->RM0038 p.154)
extern volatile t_register_stm32l1xx_ri_ascr2    register_stm32l1xx_RI_ASCR2;   // RI analog switch control register 2 (RI_ASCR2)  (->RM0038 p.156)
extern volatile t_register_stm32l1xx_ri_asmr1    register_stm32l1xx_RI_ASMR1;   // Analog switch mode register (RI_ASMR1)  (->RM0038 p.159)
extern volatile t_register_stm32l1xx_ri_asmr2    register_stm32l1xx_RI_ASMR2;   // Analog switch mode register (RI_ASMR2)  (->RM0038 p.160)
extern volatile t_register_stm32l1xx_ri_asmr3    register_stm32l1xx_RI_ASMR3;   // Analog switch mode register (RI_ASMR3)  (->RM0038 p.162)
extern volatile t_register_stm32l1xx_ri_asmr4    register_stm32l1xx_RI_ASMR4;   // Analog switch mode register (RI_ASMR4)  (->RM0038 p.163)
extern volatile t_register_stm32l1xx_ri_asmr5    register_stm32l1xx_RI_ASMR5;   // Analog switch mode register (RI_ASMR5) (->RM0038 p.165)
extern volatile t_register_stm32l1xx_ri_cicr1    register_stm32l1xx_RI_CICR1;   // Channel identification for capture register (RI_CICR1)  (->RM0038 p.160)
extern volatile t_register_stm32l1xx_ri_cicr2    register_stm32l1xx_RI_CICR2;   // Channel identification for capture register (RI_CICR2)  (->RM0038 p.161)
extern volatile t_register_stm32l1xx_ri_cicr3    register_stm32l1xx_RI_CICR3;   // Channel identification for capture register (RI_CICR3)  (->RM0038 p.163)
extern volatile t_register_stm32l1xx_ri_cicr4    register_stm32l1xx_RI_CICR4;   // Channel identification for capture register (RI_CICR4) (->RM0038 p.164)
extern volatile t_register_stm32l1xx_ri_cicr5    register_stm32l1xx_RI_CICR5;   // Channel identification for capture register (RI_CICR5)
extern volatile t_register_stm32l1xx_ri_cmr1     register_stm32l1xx_RI_CMR1;    // Channel mask register (RI_CMR1)  (->RM0038 p.159)
extern volatile t_register_stm32l1xx_ri_cmr2     register_stm32l1xx_RI_CMR2;    // Channel mask register (RI_CMR2)  (->RM0038 p.161)
extern volatile t_register_stm32l1xx_ri_cmr3     register_stm32l1xx_RI_CMR3;    // Channel mask register (RI_CMR3)  (->RM0038 p.162)
extern volatile t_register_stm32l1xx_ri_cmr4     register_stm32l1xx_RI_CMR4;    // Channel mask register (RI_CMR4)  (->RM0038 p.164)
extern volatile t_register_stm32l1xx_ri_cmr5     register_stm32l1xx_RI_CMR5;    // Channel mask register (RI_CMR5) (->RM0038 p.165)
extern volatile t_register_stm32l1xx_ri_hyscr1   register_stm32l1xx_RI_HYSCR1;  // RI hysteresis control register (RI_HYSCR1)  (->RM0038 p.157)
extern volatile t_register_stm32l1xx_ri_hyscr2   register_stm32l1xx_RI_HYSCR2;  // RI Hysteresis control register (RI_HYSCR2)  (->RM0038 p.157)
extern volatile t_register_stm32l1xx_ri_hyscr3   register_stm32l1xx_RI_HYSCR3;  // RI Hysteresis control register (RI_HYSCR3)  (->RM0038 p.158)
extern volatile t_register_stm32l1xx_ri_hyscr4   register_stm32l1xx_RI_HYSCR4;  // RI Hysteresis control register (RI_HYSCR4)  (->RM0038 p.158)

//}

/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_register.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_register.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** fills out given Config with maximum valid values for indexed REGISTER
 * @param Config        = pointer to struct t_ttc_register_config
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
t_ttc_register_architecture* register_stm32l1xx_get_configuration();


/** Prepares register driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void register_stm32l1xx_prepare();


/** resets given register to default value
 *
 * @param Register    pointer to one entry of struct t_ttc_register_config as returned by ttc_register_get_configuration()
 * @return            ==0: register has been reset successfully
 */
e_ttc_register_errorcode register_stm32l1xx_reset( void* Register );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _register_stm32l1xx_foo(t_ttc_register_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //REGISTER_STM32L1XX_H
