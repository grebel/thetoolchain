/** { register_stm32l0xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for register devices on stm32l0xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150319 12:51:52 UTC
 *
 *  Note: See ttc_register.h for description of stm32l0xx independent REGISTER implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "register_stm32l0xx.h".
//
#include "register_stm32l0xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

volatile t_register_stm32l0xx_gpio          register_stm32l0xx_GPIOA  __attribute__((section (".peripherals")));
volatile t_register_stm32l0xx_gpio          register_stm32l0xx_GPIOB  __attribute__((section (".peripherals")));
volatile t_register_stm32l0xx_gpio          register_stm32l0xx_GPIOC  __attribute__((section (".peripherals")));
volatile t_register_stm32l0xx_gpio          register_stm32l0xx_GPIOD  __attribute__((section (".peripherals")));
volatile t_register_stm32l0xx_gpio          register_stm32l0xx_GPIOH  __attribute__((section (".peripherals")));

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************


//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

void register_stm32l0xx_prepare(){  //ToDo

}

e_ttc_register_errorcode register_stm32l0xx_reset(void* Register) { //ToDo
    Assert_REGISTER(Register, ttc_assert_origin_auto); // pointers must not be NULL
}

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
