/** { register_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for register devices on stm32l1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 21 at 20140423 11:05:10 UTC
 *
 *  Note: See ttc_register.h for description of stm32l1xx independent REGISTER implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "register_stm32l1xx.h"
#include "../ttc_assert.h"
//{ Global Variables ***********************************************************

//X extern void ttc_assert_test( BOOL TestOK );

//volatile t_register_stm32l1xx_ri_ascr1   register_stm32l1xx_RI_ASCR1;   // RI analog switches control register (RI_ASCR1)  (->RM0038 p.154)
//volatile t_register_stm32l1xx_ri_ascr2   register_stm32l1xx_RI_ASCR2;   // RI analog switch control register 2 (RI_ASCR2)  (->RM0038 p.156)
//volatile t_register_stm32l1xx_ri_asmr1   register_stm32l1xx_RI_ASMR1;   // Analog switch mode register (RI_ASMR1)  (->RM0038 p.159)
//volatile t_register_stm32l1xx_ri_asmr2   register_stm32l1xx_RI_ASMR2;   // Analog switch mode register (RI_ASMR2)  (->RM0038 p.160)
//volatile t_register_stm32l1xx_ri_asmr3   register_stm32l1xx_RI_ASMR3;   // Analog switch mode register (RI_ASMR3)  (->RM0038 p.162)
//volatile t_register_stm32l1xx_ri_asmr4   register_stm32l1xx_RI_ASMR4;   // Analog switch mode register (RI_ASMR4)  (->RM0038 p.163)
//volatile t_register_stm32l1xx_ri_asmr5   register_stm32l1xx_RI_ASMR5;   // Analog switch mode register (RI_ASMR5) (->RM0038 p.165)
//volatile t_register_stm32l1xx_ri_cicr1   register_stm32l1xx_RI_CICR1;   // Channel identification for capture register (RI_CICR1)  (->RM0038 p.160)
//volatile t_register_stm32l1xx_ri_cicr2   register_stm32l1xx_RI_CICR2;   // Channel identification for capture register (RI_CICR2)  (->RM0038 p.161)
//volatile t_register_stm32l1xx_ri_cicr3   register_stm32l1xx_RI_CICR3;   // Channel identification for capture register (RI_CICR3)  (->RM0038 p.163)
//volatile t_register_stm32l1xx_ri_cicr4   register_stm32l1xx_RI_CICR4;   // Channel identification for capture register (RI_CICR4) (->RM0038 p.164)
//volatile t_register_stm32l1xx_ri_cicr5   register_stm32l1xx_RI_CICR5;   // Channel identification for capture register (RI_CICR5)
//volatile t_register_stm32l1xx_ri_cmr1    register_stm32l1xx_RI_CMR1;    // Channel mask register (RI_CMR1)  (->RM0038 p.159)
//volatile t_register_stm32l1xx_ri_cmr2    register_stm32l1xx_RI_CMR2;    // Channel mask register (RI_CMR2)  (->RM0038 p.161)
//volatile t_register_stm32l1xx_ri_cmr3    register_stm32l1xx_RI_CMR3;    // Channel mask register (RI_CMR3)  (->RM0038 p.162)
//volatile t_register_stm32l1xx_ri_cmr4    register_stm32l1xx_RI_CMR4;    // Channel mask register (RI_CMR4)  (->RM0038 p.164)
//volatile t_register_stm32l1xx_ri_cmr5    register_stm32l1xx_RI_CMR5;    // Channel mask register (RI_CMR5) (->RM0038 p.165)
//volatile t_register_stm32l1xx_ri_hyscr1  register_stm32l1xx_RI_HYSCR1;  // RI hysteresis control register (RI_HYSCR1)  (->RM0038 p.157)
//volatile t_register_stm32l1xx_ri_hyscr2  register_stm32l1xx_RI_HYSCR2;  // RI Hysteresis control register (RI_HYSCR2)  (->RM0038 p.157)
//volatile t_register_stm32l1xx_ri_hyscr3  register_stm32l1xx_RI_HYSCR3;  // RI Hysteresis control register (RI_HYSCR3)  (->RM0038 p.158)
//volatile t_register_stm32l1xx_ri_hyscr4  register_stm32l1xx_RI_HYSCR4;  // RI Hysteresis control register (RI_HYSCR4)  (->RM0038 p.158)
//InsertGlobalVariables  above (DO NOT DELETE THIS LINE!)

//}Global Variables
//{ Function Definitions *******************************************************


//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions

volatile t_register_stm32l1xx_all* register_stm32l1xx_get_configuration() { // ToDo: implement
    return NULL;
}
void register_stm32l1xx_prepare() {

    register_cortexm3_prepare();
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_SPI1 )    == 0x40013000, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_SPI2 )    == 0x40003800, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RCC )     == 0x40023800, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RTC )     == 0x40002800, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_PWR )     == 0x40007000, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_ADC1 )    == 0x40012400, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_TIMER2 )  == 0x40000000, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_TIMER3 )  == 0x40000400, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_TIMER4 )  == 0x40000800, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_TIMER6 )  == 0x40001000, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_TIMER7 )  == 0x40001400, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_TIMER9 )  == 0x40010800, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_TIMER10 ) == 0x40010c00, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_TIMER11 ) == 0x40011000, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_USART1 )  == 0x40013800, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_USART2 )  == 0x40004400, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_USART3 )  == 0x40004800, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_GPIOA )   == 0x40020000, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_GPIOB )   == 0x40020400, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_GPIOC )   == 0x40020800, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_GPIOD )   == 0x40020c00, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_GPIOH )   == 0x40021400, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_EXTI )    == 0x40010400, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_EXTI.PR ) == 0x40010414, ttc_assert_origin_auto );

    const t_u32 RI_Base = 0x40007c00; ( void ) RI_Base;
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_ASCR1 )  == RI_Base + 0x04, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_ASCR2 )  == RI_Base + 0x0c, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_HYSCR1 ) == RI_Base + 0x10, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_HYSCR2 ) == RI_Base + 0x14, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_HYSCR3 ) == RI_Base + 0x18, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_HYSCR4 ) == RI_Base + 0x1c, ttc_assert_origin_auto );

    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_ASMR1 )  == RI_Base + 0x20, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_CMR1 )   == RI_Base + 0x24, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_CICR1 )  == RI_Base + 0x28, ttc_assert_origin_auto );

    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_ASMR2 )  == RI_Base + 0x2c, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_CMR2 )   == RI_Base + 0x30, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_CICR2 )  == RI_Base + 0x34, ttc_assert_origin_auto );

    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_ASMR3 )  == RI_Base + 0x38, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_CMR3 )   == RI_Base + 0x3c, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_CICR3 )  == RI_Base + 0x40, ttc_assert_origin_auto );

    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_ASMR4 )  == RI_Base + 0x44, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_CMR4 )   == RI_Base + 0x48, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_CICR4 )  == RI_Base + 0x4c, ttc_assert_origin_auto );

    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_ASMR5 )  == RI_Base + 0x50, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_CMR5 )   == RI_Base + 0x54, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_CICR5 )  == RI_Base + 0x58, ttc_assert_origin_auto );
    Assert_REGISTER( ( ( t_u32 ) &register_stm32l1xx_RI_CMR5 )   == RI_Base + 0x54, ttc_assert_origin_auto );
}
e_ttc_register_errorcode register_stm32l1xx_reset( void* Register ) {
    Assert_REGISTER_Writable( Register, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_register_errorcode ) 0;
}

//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
