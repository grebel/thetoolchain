/** { ttc_gyroscope_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for GYROSCOPE device.
 *  Structures, Enums and Defines being required by both, high- and low-level gyroscope.
 *  This file does not provide function declarations!
 *  
 *  Created from template ttc_device_types.h revision 27 at 20150121 10:09:22 UTC
 *
 *  Authors: Gregor Rebel
 * 
}*/

#ifndef TTC_GYROSCOPE_TYPES_H
#define TTC_GYROSCOPE_TYPES_H

//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_gyroscope.h" or "ttc_gyroscope.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h" // only required to help IDE to know constant definitions
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
#ifdef EXTENSION_gyroscope_mpu6050
#  include "gyroscope/gyroscope_mpu6050_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)
 
//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

//{  TTC_GYROSCOPEn has to be defined as constant by makefile.100_board_*
#ifdef TTC_GYROSCOPE5
  #ifndef TTC_GYROSCOPE4
    #error TTC_GYROSCOPE5 is defined, but not TTC_GYROSCOPE4 - all lower TTC_GYROSCOPEn must be defined!
  #endif
  #ifndef TTC_GYROSCOPE3
    #error TTC_GYROSCOPE5 is defined, but not TTC_GYROSCOPE3 - all lower TTC_GYROSCOPEn must be defined!
  #endif
  #ifndef TTC_GYROSCOPE2
    #error TTC_GYROSCOPE5 is defined, but not TTC_GYROSCOPE2 - all lower TTC_GYROSCOPEn must be defined!
  #endif
  #ifndef TTC_GYROSCOPE1
    #error TTC_GYROSCOPE5 is defined, but not TTC_GYROSCOPE1 - all lower TTC_GYROSCOPEn must be defined!
  #endif

  #define TTC_GYROSCOPE_AMOUNT 5
#else
  #ifdef TTC_GYROSCOPE4
    #define TTC_GYROSCOPE_AMOUNT 4

    #ifndef TTC_GYROSCOPE3
      #error TTC_GYROSCOPE5 is defined, but not TTC_GYROSCOPE3 - all lower TTC_GYROSCOPEn must be defined!
    #endif
    #ifndef TTC_GYROSCOPE2
      #error TTC_GYROSCOPE5 is defined, but not TTC_GYROSCOPE2 - all lower TTC_GYROSCOPEn must be defined!
    #endif
    #ifndef TTC_GYROSCOPE1
      #error TTC_GYROSCOPE5 is defined, but not TTC_GYROSCOPE1 - all lower TTC_GYROSCOPEn must be defined!
    #endif
  #else
    #ifdef TTC_GYROSCOPE3

      #ifndef TTC_GYROSCOPE2
        #error TTC_GYROSCOPE5 is defined, but not TTC_GYROSCOPE2 - all lower TTC_GYROSCOPEn must be defined!
      #endif
      #ifndef TTC_GYROSCOPE1
        #error TTC_GYROSCOPE5 is defined, but not TTC_GYROSCOPE1 - all lower TTC_GYROSCOPEn must be defined!
      #endif

      #define TTC_GYROSCOPE_AMOUNT 3
    #else
      #ifdef TTC_GYROSCOPE2

        #ifndef TTC_GYROSCOPE1
          #error TTC_GYROSCOPE5 is defined, but not TTC_GYROSCOPE1 - all lower TTC_GYROSCOPEn must be defined!
        #endif

        #define TTC_GYROSCOPE_AMOUNT 2
      #else
        #ifdef TTC_GYROSCOPE1
          #define TTC_GYROSCOPE_AMOUNT 1
        #else
          #define TTC_GYROSCOPE_AMOUNT 0
        #endif
      #endif
    #endif
  #endif
#endif
//}
/**{ Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop. 
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_GYROSCOPE 0  # disable assert handling for gyroscope devices
 *
 */
#ifndef TTC_ASSERT_GYROSCOPE    // any previous definition set (Makefile)?
#define TTC_ASSERT_GYROSCOPE 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_GYROSCOPE == 1)  // use Assert()s in GYROSCOPE code (somewhat slower but alot easier to debug)
  #define Assert_GYROSCOPE(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in GYROSCOPE code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_GYROSCOPE(Condition, ErrorCode)
#endif
//}
/**{ Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_gyroscope_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_gyroscope_architecture
#warning Missing low-level definition for t_ttc_gyroscope_architecture (using default)
#define t_ttc_gyroscope_architecture void
#endif
//}
/**{ Check if definitions of logical indices are taken from e_ttc_gyroscope_physical_index
 *
 * See definition of e_ttc_gyroscope_physical_index below for details.
 *
 */

//#ifdef TTC_GYROSCOPE1
//#  if TTC_GYROSCOPE1 >= TTC_INDEX_GYROSCOPE_MAX
//#    error Wrong definition of TTC_GYROSCOPE1, check your makefile (must be one from e_ttc_gyroscope_physical_index)!
//#  endif
//#  ifndef TTC_GYROSCOPE1_DRIVER
//#    warning Missing definition, check your makefile: TTC_GYROSCOPE1_DRIVER (choose one from e_ttc_gyroscope_architecture)
//#    define TTC_GYROSCOPE1_DRIVER ta_gyroscope_None
//#  endif
//#endif
//#ifdef TTC_GYROSCOPE2
//#  if TTC_GYROSCOPE2 >= TTC_INDEX_GYROSCOPE_MAX
//#    error Wrong definition of TTC_GYROSCOPE2, check your makefile (must be one from e_ttc_gyroscope_physical_index)!
//#  endif
//#  ifndef TTC_GYROSCOPE2_DRIVER
//#    warning Missing definition, check your makefile: TTC_GYROSCOPE2_DRIVER (choose one from e_ttc_gyroscope_architecture)
//#    define TTC_GYROSCOPE2_DRIVER ta_gyroscope_None
//#  endif
//#endif
//#ifdef TTC_GYROSCOPE3
//#  if TTC_GYROSCOPE3 >= TTC_INDEX_GYROSCOPE_MAX
//#    error Wrong definition of TTC_GYROSCOPE3, check your makefile (must be one from e_ttc_gyroscope_physical_index)!
//#  endif
//#  ifndef TTC_GYROSCOPE3_DRIVER
//#    warning Missing definition, check your makefile: TTC_GYROSCOPE3_DRIVER (choose one from e_ttc_gyroscope_architecture)
//#    define TTC_GYROSCOPE3_DRIVER ta_gyroscope_None
//#  endif
//#endif
//#ifdef TTC_GYROSCOPE4
//#  if TTC_GYROSCOPE4 >= TTC_INDEX_GYROSCOPE_MAX
//#    error Wrong definition of TTC_GYROSCOPE4, check your makefile (must be one from e_ttc_gyroscope_physical_index)!
//#  endif
//#  ifndef TTC_GYROSCOPE4_DRIVER
//#    warning Missing definition, check your makefile: TTC_GYROSCOPE4_DRIVER (choose one from e_ttc_gyroscope_architecture)
//#    define TTC_GYROSCOPE4_DRIVER ta_gyroscope_None
//#  endif
//#endif
//#ifdef TTC_GYROSCOPE5
//#  if TTC_GYROSCOPE5 >= TTC_INDEX_GYROSCOPE_MAX
//#    error Wrong definition of TTC_GYROSCOPE5, check your makefile (must be one from e_ttc_gyroscope_physical_index)!
//#  endif
//#  ifndef TTC_GYROSCOPE5_DRIVER
//#    warning Missing definition, check your makefile: TTC_GYROSCOPE5_DRIVER (choose one from e_ttc_gyroscope_architecture)
//#    define TTC_GYROSCOPE5_DRIVER ta_gyroscope_None
//#  endif
//#endif
//#ifdef TTC_GYROSCOPE6
//#  if TTC_GYROSCOPE6 >= TTC_INDEX_GYROSCOPE_MAX
//#    error Wrong definition of TTC_GYROSCOPE6, check your makefile (must be one from e_ttc_gyroscope_physical_index)!
//#  endif
//#  ifndef TTC_GYROSCOPE6_DRIVER
//#    warning Missing definition, check your makefile: TTC_GYROSCOPE6_DRIVER (choose one from e_ttc_gyroscope_architecture)
//#    define TTC_GYROSCOPE6_DRIVER ta_gyroscope_None
//#  endif
//#endif
//#ifdef TTC_GYROSCOPE7
//#  if TTC_GYROSCOPE7 >= TTC_INDEX_GYROSCOPE_MAX
//#    error Wrong definition of TTC_GYROSCOPE7, check your makefile (must be one from e_ttc_gyroscope_physical_index)!
//#  endif
//#  ifndef TTC_GYROSCOPE7_DRIVER
//#    warning Missing definition, check your makefile: TTC_GYROSCOPE7_DRIVER (choose one from e_ttc_gyroscope_architecture)
//#    define TTC_GYROSCOPE7_DRIVER ta_gyroscope_None
//#  endif
//#endif
//#ifdef TTC_GYROSCOPE8
//#  if TTC_GYROSCOPE8 >= TTC_INDEX_GYROSCOPE_MAX
//#    error Wrong definition of TTC_GYROSCOPE8, check your makefile (must be one from e_ttc_gyroscope_physical_index)!
//#  endif
//#  ifndef TTC_GYROSCOPE8_DRIVER
//#    warning Missing definition, check your makefile: TTC_GYROSCOPE8_DRIVER (choose one from e_ttc_gyroscope_architecture)
//#    define TTC_GYROSCOPE8_DRIVER ta_gyroscope_None
//#  endif
//#endif
//#ifdef TTC_GYROSCOPE9
//#  if TTC_GYROSCOPE9 >= TTC_INDEX_GYROSCOPE_MAX
//#    error Wrong definition of TTC_GYROSCOPE9, check your makefile (must be one from e_ttc_gyroscope_physical_index)!
//#  endif
//#  ifndef TTC_GYROSCOPE9_DRIVER
//#    warning Missing definition, check your makefile: TTC_GYROSCOPE9_DRIVER (choose one from e_ttc_gyroscope_architecture)
//#    define TTC_GYROSCOPE9_DRIVER ta_gyroscope_None
//#  endif
//#endif
//#ifdef TTC_GYROSCOPE10
//#  if TTC_GYROSCOPE10 >= TTC_INDEX_GYROSCOPE_MAX
//#    error Wrong definition of TTC_GYROSCOPE10, check your makefile (must be one from e_ttc_gyroscope_physical_index)!
//#  endif
//#  ifndef TTC_GYROSCOPE10_DRIVER
//#    warning Missing definition, check your makefile: TTC_GYROSCOPE10_DRIVER (choose one from e_ttc_gyroscope_architecture)
//#    define TTC_GYROSCOPE10_DRIVER ta_gyroscope_None
//#  endif
//#endif
//}

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef e_ttc_physical_index e_ttc_gyroscope_physical_index;
typedef enum {   // e_ttc_gyroscope_errorcode      return codes of GYROSCOPE devices
    ec_gyroscope_OK = 0,

    // other warnings go here..

    ec_gyroscope_ERROR,                  // general failure
    ec_gyroscope_NULL,                   // NULL pointer not accepted
    ec_gyroscope_DeviceNotFound,         // corresponding device could not be found
    ec_gyroscope_InvalidConfiguration,   // sanity check of device configuration failed
    ec_gyroscope_InvalidImplementation,  // your code does not behave as expected
    ec_gyroscope_CommunicationI2C,       // error occured while communicating via I2C

    // other failures go here..

    ec_gyroscope_unknown                // no valid errorcodes past this entry
} e_ttc_gyroscope_errorcode;
typedef enum {   // e_ttc_gyroscope_architecture  types of architectures supported by GYROSCOPE driver
  ta_gyroscope_None,           // no architecture selected
  

  ta_gyroscope_mpu6050, // automatically added by ./create_DeviceDriver.pl
//InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)
  
  ta_gyroscope_unknown        // architecture not supported
} e_ttc_gyroscope_architecture;
typedef struct s_ttc_gyroscope_measures { // measure values
    t_s16 GyroX; // G acceleration along X-axis (G/1000)
    t_s16 GyroY; // G acceleration along X-axis (G/1000)
    t_s16 GyroZ; // G acceleration along X-axis (G/1000)
} t_ttc_gyroscope_measures;


typedef struct s_ttc_gyroscope_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_gyroscope_init()
    //       and after ttc_gyroscope_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    e_ttc_gyroscope_architecture Architecture; // type of architecture used for current gyroscope device
    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_gyroscope1, ...)

    t_u8 LogicalIndex_Interface;

    // low-level configuration (structure defined by low-level driver)
    t_ttc_gyroscope_architecture LowLevelConfig;

    // latest measures from gyroscope
    t_ttc_gyroscope_measures Measures;

    union  { // generic configuration bits common for all low-level Drivers
        t_u16 All;
        struct {
          unsigned Initialized        : 1;  // ==1: device has been initialized successfully
          // ToDo: additional high-level flags go here..

            unsigned Reserved1        : 15; // pad to 16 bits
        } Bits;
    } Flags;

    // Additional high-level attributes go here..

} __attribute__((__packed__)) t_ttc_gyroscope_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_GYROSCOPE_TYPES_H
