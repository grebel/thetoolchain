/** { ttc_cpu_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for CPU device.
 *  Structures, Enums and Defines being required by both, high- and low-level cpu.
 *  This file does not provide function declarations!
 *
 *  Created from template ttc_device_types.h revision 28 at 20150318 12:33:43 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_CPU_VARIANTS_H
#define TTC_CPU_VARIANTS_H

/* Define CPU-Variant constants BEFORE including the low-level types header.
 * We use defines to be able to use #if () clauses. We also use enum e_ttc_cpu_variant
 * to provide a valid C datatype to identify current cpu variant.
 * Each TTC_CPU_VARIANT_* must be defined as a unique number.
 * Keep this list synchronous with e_ttc_cpu_variant below!
 */
#define TTC_CPU_VARIANT_stm32f051r8t6      101
#define TTC_CPU_VARIANT_stm32f100rbt6      201
#define TTC_CPU_VARIANT_stm32f103_start    300
#define TTC_CPU_VARIANT_stm32f103rbt6      301
#define TTC_CPU_VARIANT_stm32f103r8t6      302
#define TTC_CPU_VARIANT_stm32f103r6t6a     303
#define TTC_CPU_VARIANT_stm32f103zet6      304
#define TTC_CPU_VARIANT_stm32f103rdy6tr    305
#define TTC_CPU_VARIANT_stm32f103_end      399
#define TTC_CPU_VARIANT_stm32f105rct6      401
#define TTC_CPU_VARIANT_stm32f107vct6      501
#define TTC_CPU_VARIANT_stm32l053r8t6      601
#define TTC_CPU_VARIANT_stm32l100rbt6      701
#define TTC_CPU_VARIANT_stm32l100rct6      702
#define TTC_CPU_VARIANT_stm32l152rbt6      802
#define TTC_CPU_VARIANT_stm32w108ccu      1001
#define TTC_CPU_VARIANT_stm32f302r8t6     1101


//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_cpu.h" or "ttc_cpu.c"
//
#include "ttc_basic_types.h"
#include "ttc_sysclock_types.h"
#include "compile_options.h" // only required to help IDE to know constant definitions
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
#ifdef EXTENSION_cpu_stm32f1xx
    #include "cpu/cpu_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_cpu_stm32f4xx
    #include "cpu/cpu_stm32f4xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_cpu_stm32l1xx
    #include "cpu/cpu_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_cpu_stm32l0xx
    #include "cpu/cpu_stm32l0xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_cpu_cortexm3
    #include "cpu/cpu_cortexm3_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_cpu_stm32w1xx
    #include "cpu/cpu_stm32w1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_cpu_stm32f0xx
    #include "cpu/cpu_stm32f0xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_cpu_stm32f3xx
    #include "cpu/cpu_stm32f3xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_cpu_stm32f2xx
    #include "cpu/cpu_stm32f2xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_cpu_cortexm4
    #include "cpu/cpu_cortexm4_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_cpu_cortexm0
    #include "cpu/cpu_cortexm0_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

// only single cpus supported at the moment
#define TTC_CPU_AMOUNT 1

/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_CPU 0  # disable assert handling for cpu devices
 *
 */
#ifndef TTC_ASSERT_CPU    // any previous definition set (Makefile)?
    #define TTC_ASSERT_CPU 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_CPU == 1)  // use Assert()s in CPU code (somewhat slower but alot easier to debug)
    #define Assert_CPU(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_CPU_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_CPU_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in CPU code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_CPU(Condition, Origin)
    #define Assert_CPU_Writable(Address, Origin)
    #define Assert_CPU_Readable(Address, Origin)
#endif


#ifndef TTC_ASSERT_CPU_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_CPU_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_CPU_EXTRA == 1)  // use Assert()s in CPU code (somewhat slower but alot easier to debug)
    #define Assert_CPU_EXTRA(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_CPU_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_CPU_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in CPU code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_CPU_EXTRA(Condition, Origin)
    #define Assert_CPU_EXTRA_Writable(Address, Origin)
    #define Assert_CPU_EXTRA_Readable(Address, Origin)
#endif
//}

/** { Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_cpu_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_cpu_architecture
    #  warning Missing low-level definition for t_ttc_cpu_architecture (using default)
    #define t_ttc_cpu_architecture void
#endif

#ifndef TTC_CPU_VARIANT
    #  error Missing definition of TTC_CPU_VARIANT. Defines this as one from e_ttc_cpu_variant in your board makefile!
#endif
#if (TTC_CPU_VARIANT==1) || (TTC_CPU_VARIANT==0)
    #  error Wrong definition of TTC_CPU_VARIANT. Defines this as one from e_ttc_cpu_variant in your board makefile!
#endif

// See cpu_stm32f1xx_types.h for an example how to configure the cpu
#ifndef TTC_CPU_SIZE_RAM
    #  error Missing definition for TTC_CPU_SIZE_RAM! The cpu low-level driver must define this constant.
#endif
#ifndef TTC_CPU_SIZE_FLASH
    #  error Missing definition for TTC_CPU_SIZE_FLASH! The cpu low-level driver must define this constant.
#endif
#ifndef TTC_CPU_AMOUNT_PINS
    #  error Missing definition for TTC_CPU_AMOUNT_PINS! The cpu low-level driver must define this constant.
#endif
#ifndef TTC_CPU_AMOUNT_GPIOS
    #  error Missing definition for TTC_CPU_AMOUNT_GPIOS! The cpu low-level driver must define this constant.
#endif
#ifndef TTC_CPU_ARCHITECTURE
    #  error Missing definition for TTC_CPU_ARCHITECTURE! The cpu low-level driver must define this constant.
#endif
#ifndef TTC_SYSCLOCK_FREQUENCY_MAX
    #  error Missing definition of TTC_SYSCLOCK_FREQUENCY_MAX. Define it to the maxmimum allowed system clock frequency in your low-level cpu driver (->ttc_cpu_types.h)!
#endif
//}

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_cpu_errorcode      return codes of CPU devices
    ec_cpu_OK = 0,

    // other warnings go here..

    ec_cpu_ERROR,                  // general failure
    ec_cpu_NULL,                   // NULL pointer not accepted
    ec_cpu_DeviceNotFound,         // corresponding device could not be found
    ec_cpu_InvalidConfiguration,   // sanity check of device configuration failed
    ec_cpu_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..

    ec_cpu_unknown                // no valid errorcodes past this entry
} e_ttc_cpu_errorcode;

typedef enum {   // e_ttc_cpu_architecture  types of architectures supported by CPU driver
    ta_cpu_None,           // no architecture selected

    ta_cpu_stm32f0xx,
    ta_cpu_stm32f1xx,
    ta_cpu_stm32l0xx,
    ta_cpu_stm32l1xx,
    ta_cpu_stm32f3xx,
    ta_cpu_stm32w1xx,
    ta_cpu_stm32f4xx, // automatically added by ./create_DeviceDriver.pl
    ta_cpu_cortexm3, // automatically added by ./create_DeviceDriver.pl
    ta_cpu_stm32f2xx, // automatically added by ./create_DeviceDriver.pl
    ta_cpu_cortexm4, // automatically added by ./create_DeviceDriver.pl
    ta_cpu_cortexm0, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_cpu_unknown        // architecture not supported
} e_ttc_cpu_architecture;

typedef enum {   // e_ttc_cpu_architecture  types of individual chip variants supported by CPU driver
    ta_cpu_variant_None    = 0,  // no architecture selected
    ta_cpu_variant_Illegal = 1,  // allows to detect COMPILE_OPTS+=-DTTC_CPU_VARIANT lines

    // individual cpus
    // update t_ttc_cpu_info variable of corresponding low-level driver after adding new cpu here!

    ta_cpu_stm32f051r8t6   = TTC_CPU_VARIANT_stm32f051r8t6,

    ta_cpu_stm32f100rbt6   = TTC_CPU_VARIANT_stm32f100rbt6,

    ta_cpu_stm32f103rbt6   = TTC_CPU_VARIANT_stm32f103rbt6,
    ta_cpu_stm32f103r8t6   = TTC_CPU_VARIANT_stm32f103r8t6,
    ta_cpu_stm32f103r6t6a  = TTC_CPU_VARIANT_stm32f103r6t6a,
    ta_cpu_stm32f103zet6   = TTC_CPU_VARIANT_stm32f103zet6,
    ta_cpu_stm32f103rdy6tr = TTC_CPU_VARIANT_stm32f103rdy6tr,

    ta_cpu_stm32f105rct6   = TTC_CPU_VARIANT_stm32f105rct6,

    ta_cpu_stm32f107vct6   = TTC_CPU_VARIANT_stm32f107vct6,

    ta_cpu_stm32l053r8t6   = TTC_CPU_VARIANT_stm32l053r8t6,

    ta_cpu_stm32l100rct6   = TTC_CPU_VARIANT_stm32l100rct6,
    ta_cpu_stm32l100rbt6   = TTC_CPU_VARIANT_stm32l100rbt6,
    ta_cpu_stm32l152rbt6   = TTC_CPU_VARIANT_stm32l152rbt6,

    ta_cpu_stm32w108ccu    = TTC_CPU_VARIANT_stm32w108ccu,

    // add new variants above
    ta_cpu_variant_Error,           // no architecture selected
} e_ttc_cpu_variant;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef struct s_ttc_cpu_info { //           architecture independent, constant configuration data
    t_base Size_RAM;            // size of RAM (bytes)
    t_base Size_FLASH;          // size of FLASH memory (bytes)
    t_base Size_EEPROM;         // size of EEPROM (bytes)
    t_u8 Amount_Pins;           // total amount of package pins
    t_u8 Amount_GPIOs;          // amount of usable general purpose IO pins

    e_ttc_cpu_architecture Architecture; // type of architecture used for current cpu device
    e_ttc_cpu_variant      Variant;      // individual chip variant (type + FLASH-size + RAM-size + EEPROM-size + pin count)
} t_ttc_cpu_info;

typedef struct s_ttc_cpu_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_cpu_init()
    //       and after ttc_cpu_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.


    // low-level configuration (structure defined by low-level driver)
#ifdef t_ttc_cpu_architecture
    t_ttc_cpu_architecture* LowLevelConfig;
#endif

    union  { // generic configuration bits common for all low-level Drivers
        t_u8 All;
        struct {
            unsigned Initialized        : 1;  // ==1: device has been initialized successfully
            // ToDo: additional high-level flags go here..

        } Bits;
    } Flags;

    // Additional high-level attributes go here..

    // points to constant cpu information

    const t_ttc_cpu_info* Info;
} __attribute__( ( __packed__ ) ) t_ttc_cpu_config;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_CPU_VARIANTS_H
