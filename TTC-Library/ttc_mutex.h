#ifndef ttc_mutex_H
#define ttc_mutex_H

/** ttc_mutex.h ***********************************************{
 *
 * Written by Gregor Rebel 2010-2018
 *
 */
/** Description of ttc_mutex (Do not delete this line!)
 *
 * Architecture independent support for inter task communication & synchronization.
 *
 * Currently supported architecture/ schedulers: CortexM3 (cm3), FreeRTOS
 *
 * Why using ttc_mutex.c instead of cm3_mutex.c or FreeRTOS-functions directly?
 * (1) it is more portable
 *     You may change to another multitasking scheduler once it is supported by The ToolChain
 * (2) extra safety checks
 *     Deadlocks are a common issue in multitasking applications and sometimes hard to debug.
 *     + ttc_mutex allows to turn all endless locks into a timed one that will assert if expired.
 *       -> t_ttc_mutex_types.h/TTC_MUTEX_TIMEOUT_USECS
 *     + ttc_mutex provides smart mutexes that can record history of operations and
 *       run as fast as normal mutexs if disabled.
 *       -> t_ttc_mutex_types.h/TTC_SMART_MUTEXES
 *
 * Smart mutexes store the owner of a lock.
 * Whenever a mutex stays locked because of a programming error, then the last locking
 * function can be identified by taking a look into the smart lock structure.
}*/

//{ includes

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)

#ifdef EXTENSION_ttc_mutex_cortexm3
    #include "cpu/cm3_mutex.h"
#endif


#ifdef EXTENSION_ttc_mutex_cortexm0
    #include "cm0/cm0_mutex.h"
#endif

#ifdef EXTENSION_ttc_mutex_freertos
    #include "scheduler/freertos_mutex.h"
#endif

#include "ttc_mutex_types.h"
#ifdef EXTENSION_ttc_task
    #include "ttc_task.h"
#endif

//}includes
//{ Types & Structures ***************************************************

/** Types to be defined by low-level driver ******************************
 *
 * t_ttc_mutex  datatype storing a mutex
 * E.g.: typedef t_u8 t_ttc_mutex
 *
 *
 */


//}

//{ Function prototypes **************************************************
//
// For each function, a _driver_() pendant must be defined by low-level driver to provide an implementation.


/** Checks if given mutex is in locked state
 *
 * Note: Calling this function makes only sense from interrupt service routine
 *
 * @param Mutex  handle of already created mutex
 * @return         == 0: Mutex is not locked; !=0: Mutex is locked
 */
BOOL ttc_mutex_is_locked( t_ttc_mutex_smart* Mutex ); // just a prototype for easier understanding
#if TTC_SMART_MUTEXES == 1
    #define ttc_mutex_is_locked(Mutex) _ttc_mutex_smart_is_locked(Mutex)
#else
    #define ttc_mutex_is_locked(Mutex) _ttc_mutex_is_locked(Mutex)
#endif


/** creates a new non-recursive mutex lock
 *
 * @return                 =! NULL: mutex has been created successfully (use handle to operate on mutex)
 */
t_ttc_mutex_smart* ttc_mutex_create();  // just a prototype for easier understanding
#if TTC_SMART_MUTEXES == 1
    #define ttc_mutex_create()   _ttc_mutex_smart_create()
#else
    #define ttc_mutex_create() _ttc_mutex_create()
#endif

/** initializes an existing non-recursive mutex lock to the unlocked state
 *
 * @Mutex                 storage to be initialized as mutex
 */
void ttc_mutex_init( t_ttc_mutex_smart* Lock ); // just a prototype for easier understanding
#if TTC_SMART_MUTEXES == 1
    #define ttc_mutex_init(Mutex) _ttc_mutex_smart_init(Mutex)
#else
    #define ttc_mutex_init(Mutex) _ttc_mutex_init(Mutex)
#endif

/** Blocks until lock has been obtained on mutex
 *
 * @param Mutex  handle of already created mutex
 */
// void ttc_mutex_lock_endless(t_ttc_mutex_smart* Mutex);  // just a prototype for easier understanding
#if TTC_SMART_MUTEXES == 1
    #define ttc_mutex_lock_endless(Mutex) _ttc_mutex_smart_lock(Mutex, -1)
#else
    #ifdef _driver_mutex_lock_endless
        #define ttc_mutex_lock_endless(Mutex) _driver_mutex_lock_endless(Mutex)
    #else
        #define ttc_mutex_lock_endless(Mutex) _ttc_mutex_lock(Mutex, -1)
    #endif
#endif

/** Tries to directly obtain lock. Will return immediately.
 *
* @param Mutex  handle of already created mutex
* @return       == 0: Lock has been successfully obtained; != 0: mutex was already locked (lock failed)
 */
// e_ttc_mutex_error ttc_mutex_lock_try(t_ttc_mutex_smart* Mutex);  // just a prototype for easier understanding
#if TTC_SMART_MUTEXES == 1
    #define ttc_mutex_lock_try(Mutex) _ttc_mutex_smart_lock(Mutex, 0)
#else
    #ifdef _driver_mutex_lock_try
        #define ttc_mutex_lock_try(Mutex) _driver_mutex_lock_try(Mutex)
    #else
        #define ttc_mutex_lock_try(Mutex) _ttc_mutex_lock(Mutex, 0)
    #endif
#endif

/** Tries to obtain lock on mutex within timeout
 *
 * @param Mutex  handle of already created mutex
 * @param TimeOut  !=-1: amount of system ticks to wait for lock
 *                 ==-1: wait forever

 * @return         == 0: Lock has been successfully obtained within TimeOut; != 0: TimeOut occured before obtaining lock (No Success)
 */
//e_ttc_mutex_error ttc_mutex_lock(t_ttc_mutex_smart* Mutex, t_base TimeOut);  // just a prototype for easier understanding
#if TTC_SMART_MUTEXES == 1
    #define ttc_mutex_lock(Mutex, TimeOut) _ttc_mutex_smart_lock(Mutex, TimeOut)
#else
    #ifdef _driver_mutex_lock
        #define ttc_mutex_lock(Mutex, TimeOut) _driver_mutex_lock(Mutex, TimeOut)
    #else
        #define ttc_mutex_lock(Mutex, TimeOut) _ttc_mutex_lock(Mutex, TimeOut)
    #endif
#endif

/** Tries to obtain lock on mutex; this function will never block
 *
 * Note: This function must only be used with disabled interrupts (e.g. from interrupt service routine)!
 *
 * @param Mutex    handle of already created mutex

 * @return         == 0: Lock has been successfully obtained within TimeOut
 *                 == tme_WasLockedByTask: - a task of higher priority has been interrupted => call ttc_task_yield() at end of your interrupt service routine!
 *                 >= tme_Error: Error condition
 */
e_ttc_mutex_error ttc_mutex_lock_isr( t_ttc_mutex_smart* Mutex ); // just a prototype for easier understanding
#ifndef _driver_mutex_lock_isr
    #error Missing low-level implementation! // check description above for function to be implemented
#endif
#if TTC_SMART_MUTEXES == 1
    #define ttc_mutex_lock_isr(Mutex) _ttc_mutex_smart_lock_isr(Mutex)
#else
    #ifdef _driver_mutex_lock_isr
        #define ttc_mutex_lock_isr(Mutex) _driver_mutex_lock_isr(Mutex)
    #else
        #define ttc_mutex_lock_isr(Mutex) _ttc_mutex_lock_isr(Mutex)
    #endif
#endif

/** Releases an already obtained lock from mutex
 *
 * @param Mutex  handle of already created smart mutex
 */
void ttc_mutex_unlock( t_ttc_mutex_smart* Mutex ); // just a prototype for easier understanding
#if TTC_SMART_MUTEXES == 1
    #define ttc_mutex_unlock(Mutex) _ttc_mutex_smart_unlock(Mutex)
#else
    #ifdef _driver_mutex_unlock
        #define ttc_mutex_unlock(Mutex) _driver_mutex_unlock(Mutex)
    #else
        #define ttc_mutex_unlock(Mutex) _ttc_mutex_unlock(Mutex)
    #endif
#endif


/** Releases an already obtained lock from mutex (does never block)
 *
 * @param Mutex  handle of already created mutex
 */
void ttc_mutex_unlock_isr( t_ttc_mutex_smart* Mutex ); // just a prototype for easier understanding
#if TTC_SMART_MUTEXES == 1
    #define ttc_mutex_unlock_isr(Mutex) _ttc_mutex_smart_unlock_isr(Mutex)
#else
    #ifdef _driver_mutex_unlock_isr
        #define ttc_mutex_unlock_isr(Mutex) _driver_mutex_unlock_isr(Mutex)
    #else
        #define ttc_mutex_unlock_isr(Mutex) _ttc_mutex_unlock_isr(Mutex)
    #endif
#endif

//}Function prototypes
//{ low-level prototypes (call via macros above!)

t_ttc_mutex* _ttc_mutex_create();
t_ttc_mutex_smart* _ttc_mutex_smart_create();
#ifndef _driver_mutex_create
    #error Missing low-level implementation! // check description above for function to be implemented
#endif

void _ttc_mutex_init( t_ttc_mutex* Lock );
void _ttc_mutex_smart_init( t_ttc_mutex_smart* Mutex );
#ifndef _driver_mutex_init
    #error Missing low-level implementation! // check description above for function to be implemented
#endif

e_ttc_mutex_error _ttc_mutex_lock( t_ttc_mutex* Mutex, t_base TimeOut );
e_ttc_mutex_error _ttc_mutex_smart_lock( t_ttc_mutex_smart* SmartMutex, t_base TimeOut );
#ifndef _driver_mutex_lock
    #error Missing low-level implementation! // check description above for function to be implemented
#endif

e_ttc_mutex_error _ttc_mutex_lock_isr( t_ttc_mutex* Mutex );
e_ttc_mutex_error _ttc_mutex_smart_lock_isr( t_ttc_mutex_smart* Mutex );
#ifndef _driver_mutex_lock_isr
    #error Missing low-level implementation! // check description above for function to be implemented
#endif

BOOL _ttc_mutex_is_locked( t_ttc_mutex* Mutex );
BOOL _ttc_mutex_smart_is_locked( t_ttc_mutex_smart* SmartMutex );
#ifndef _driver_mutex_is_locked
    #error Missing low-level implementation! // check description above for function to be implemented
#endif

void _ttc_mutex_unlock( t_ttc_mutex* Mutex );
#if TTC_ASSERT_MUTEX != 1
    #define _ttc_mutex_unlock( Mutex ) _driver_mutex_unlock( Mutex )
#endif
void _ttc_mutex_smart_unlock( t_ttc_mutex_smart* SmartMutex );
#ifndef _driver_mutex_unlock
    #error Missing low-level implementation! // check description above for function to be implemented
#endif

void _ttc_mutex_unlock_isr( t_ttc_mutex* Mutex );
#if TTC_ASSERT_MUTEX != 1
    #define _ttc_mutex_unlock_isr( Mutex ) _driver_mutex_unlock_isr( Mutex )
#endif
void _ttc_mutex_smart_unlock_isr( t_ttc_mutex_smart* SmartMutex );
#ifndef _driver_mutex_unlock_isr
    #error Missing low-level implementation! // check description above for function to be implemented
#endif

//} low-level prototypes


#endif
