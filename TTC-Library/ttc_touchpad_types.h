/** { ttc_touchpad_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for TOUCHPAD device.
 *  Structures, Enums and Defines being required by both, high- and low-level touchpad.
 *  This file does not provide function declarations!
 *
 *  Created from template ttc_device_types.h revision 27 at 20150316 07:48:52 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_TOUCHPAD_TYPES_H
#define TTC_TOUCHPAD_TYPES_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_touchpad.h" or "ttc_touchpad.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h" // only required to help IDE to know constant definitions
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
#ifdef EXTENSION_touchpad_analog4
#  include "touchpad/touchpad_analog4_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

//{  TTC_TOUCHPADn has to be defined as constant by makefile.100_board_* or makefile.150_board_extension_*
#ifdef TTC_TOUCHPAD5
#ifndef TTC_TOUCHPAD4
#error TTC_TOUCHPAD5 is defined, but not TTC_TOUCHPAD4 - all lower TTC_TOUCHPADn must be defined!
#endif
#ifndef TTC_TOUCHPAD3
#error TTC_TOUCHPAD5 is defined, but not TTC_TOUCHPAD3 - all lower TTC_TOUCHPADn must be defined!
#endif
#ifndef TTC_TOUCHPAD2
#error TTC_TOUCHPAD5 is defined, but not TTC_TOUCHPAD2 - all lower TTC_TOUCHPADn must be defined!
#endif
#ifndef TTC_TOUCHPAD1
#error TTC_TOUCHPAD5 is defined, but not TTC_TOUCHPAD1 - all lower TTC_TOUCHPADn must be defined!
#endif

#define TTC_TOUCHPAD_AMOUNT 5
#else
#ifdef TTC_TOUCHPAD4
#define TTC_TOUCHPAD_AMOUNT 4

#ifndef TTC_TOUCHPAD3
#error TTC_TOUCHPAD5 is defined, but not TTC_TOUCHPAD3 - all lower TTC_TOUCHPADn must be defined!
#endif
#ifndef TTC_TOUCHPAD2
#error TTC_TOUCHPAD5 is defined, but not TTC_TOUCHPAD2 - all lower TTC_TOUCHPADn must be defined!
#endif
#ifndef TTC_TOUCHPAD1
#error TTC_TOUCHPAD5 is defined, but not TTC_TOUCHPAD1 - all lower TTC_TOUCHPADn must be defined!
#endif
#else
#ifdef TTC_TOUCHPAD3

#ifndef TTC_TOUCHPAD2
#error TTC_TOUCHPAD5 is defined, but not TTC_TOUCHPAD2 - all lower TTC_TOUCHPADn must be defined!
#endif
#ifndef TTC_TOUCHPAD1
#error TTC_TOUCHPAD5 is defined, but not TTC_TOUCHPAD1 - all lower TTC_TOUCHPADn must be defined!
#endif

#define TTC_TOUCHPAD_AMOUNT 3
#else
#ifdef TTC_TOUCHPAD2

#ifndef TTC_TOUCHPAD1
#error TTC_TOUCHPAD5 is defined, but not TTC_TOUCHPAD1 - all lower TTC_TOUCHPADn must be defined!
#endif

#define TTC_TOUCHPAD_AMOUNT 2
#else
#ifdef TTC_TOUCHPAD1
#define TTC_TOUCHPAD_AMOUNT 1
#else
#define TTC_TOUCHPAD_AMOUNT 0
#endif
#endif
#endif
#endif
#endif
//}
/**{ Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_TOUCHPAD 0        # disable default asserts for TOUCHPAD driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_TOUCHPAD_EXTRA 1  # enable extra asserts for TOUCHPAD driver
 *
 */
#ifndef TTC_ASSERT_TOUCHPAD    // any previous definition set (Makefile)?
#define TTC_ASSERT_TOUCHPAD 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_TOUCHPAD == 1)  // use Assert()s in TOUCHPAD code (somewhat slower but alot easier to debug)
#define Assert_TOUCHPAD(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // use no default Assert()s in TOUCHPAD code (somewhat smaller + faster, but crashes are hard to debug)
#define Assert_TOUCHPAD(Condition, ErrorCode)
#endif

#define TTC_ASSERT_TOUCHPAD_EXTRA 1  // <-- remove line after completing your driver!

#ifndef TTC_ASSERT_TOUCHPAD_EXTRA    // any previous definition set (Makefile)?
#define TTC_ASSERT_TOUCHPAD_EXTRA 0  // extra asserts are disabled by default
#endif
#if (TTC_ASSERT_TOUCHPAD_EXTRA == 1)  // use Assert()s in TOUCHPAD code (somewhat slower but alot easier to debug)
#define Assert_TOUCHPAD_EXTRA(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // use no extra Assert()s in TOUCHPAD code (somewhat smaller + faster, but crashes are hard to debug)
#define Assert_TOUCHPAD_EXTRA(Condition, ErrorCode)
#endif
//}
/**{ Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_touchpad_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
//X #ifndef t_ttc_touchpad_architecture
//X #  warning Missing low-level definition for t_ttc_touchpad_architecture (using default)
//X #  define t_ttc_touchpad_architecture void
//X #endif
//}
/**{ Check constant configuration of all touchpad devices
 *
 */

#define TTC_INDEX_TOUCHPAD_MAX 10  // must have same value as TTC_INDEX_TOUCHPAD_ERROR
#ifdef TTC_TOUCHPAD1

#  ifndef TTC_TOUCHPAD1_WIDTH
#    error Missing definition of TTC_TOUCHPAD1_WIDTH, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD1_HEIGHT
#    error Missing definition of TTC_TOUCHPAD1_HEIGHT, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD1_DEPTH
#    error Missing definition of TTC_TOUCHPAD1_DEPTH, add one to your board makefile!
#  endif

#endif
#ifdef TTC_TOUCHPAD2

#  ifndef TTC_TOUCHPAD2_WIDTH
#    error Missing definition of TTC_TOUCHPAD2_WIDTH, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD2_HEIGHT
#    error Missing definition of TTC_TOUCHPAD2_HEIGHT, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD2_DEPTH
#    error Missing definition of TTC_TOUCHPAD2_DEPTH, add one to your board makefile!
#  endif

#endif
#ifdef TTC_TOUCHPAD3

#  ifndef TTC_TOUCHPAD3_WIDTH
#    error Missing definition of TTC_TOUCHPAD3_WIDTH, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD3_HEIGHT
#    error Missing definition of TTC_TOUCHPAD3_HEIGHT, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD3_DEPTH
#    error Missing definition of TTC_TOUCHPAD3_DEPTH, add one to your board makefile!
#  endif

#endif
#ifdef TTC_TOUCHPAD4

#  ifndef TTC_TOUCHPAD4_WIDTH
#    error Missing definition of TTC_TOUCHPAD4_WIDTH, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD4_HEIGHT
#    error Missing definition of TTC_TOUCHPAD4_HEIGHT, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD4_DEPTH
#    error Missing definition of TTC_TOUCHPAD4_DEPTH, add one to your board makefile!
#  endif

#endif
#ifdef TTC_TOUCHPAD5

#  ifndef TTC_TOUCHPAD5_WIDTH
#    error Missing definition of TTC_TOUCHPAD5_WIDTH, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD5_HEIGHT
#    error Missing definition of TTC_TOUCHPAD5_HEIGHT, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD5_DEPTH
#    error Missing definition of TTC_TOUCHPAD5_DEPTH, add one to your board makefile!
#  endif

#endif
#ifdef TTC_TOUCHPAD6

#  ifndef TTC_TOUCHPAD6_WIDTH
#    error Missing definition of TTC_TOUCHPAD6_WIDTH, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD6_HEIGHT
#    error Missing definition of TTC_TOUCHPAD6_HEIGHT, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD6_DEPTH
#    error Missing definition of TTC_TOUCHPAD6_DEPTH, add one to your board makefile!
#  endif

#endif
#ifdef TTC_TOUCHPAD7

#  ifndef TTC_TOUCHPAD7_WIDTH
#    error Missing definition of TTC_TOUCHPAD7_WIDTH, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD7_HEIGHT
#    error Missing definition of TTC_TOUCHPAD7_HEIGHT, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD7_DEPTH
#    error Missing definition of TTC_TOUCHPAD7_DEPTH, add one to your board makefile!
#  endif

#endif
#ifdef TTC_TOUCHPAD8

#  ifndef TTC_TOUCHPAD8_WIDTH
#    error Missing definition of TTC_TOUCHPAD8_WIDTH, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD8_HEIGHT
#    error Missing definition of TTC_TOUCHPAD8_HEIGHT, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD8_DEPTH
#    error Missing definition of TTC_TOUCHPAD8_DEPTH, add one to your board makefile!
#  endif

#endif
#ifdef TTC_TOUCHPAD9

#  ifndef TTC_TOUCHPAD9_WIDTH
#    error Missing definition of TTC_TOUCHPAD9_WIDTH, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD9_HEIGHT
#    error Missing definition of TTC_TOUCHPAD9_HEIGHT, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD9_DEPTH
#    error Missing definition of TTC_TOUCHPAD9_DEPTH, add one to your board makefile!
#  endif

#endif
#ifdef TTC_TOUCHPAD10

#  ifndef TTC_TOUCHPAD10_WIDTH
#    error Missing definition of TTC_TOUCHPAD10_WIDTH, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD10_HEIGHT
#    error Missing definition of TTC_TOUCHPAD10_HEIGHT, add one to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD10_DEPTH
#    error Missing definition of TTC_TOUCHPAD10_DEPTH, add one to your board makefile!
#  endif

#endif

//}

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef enum {   // type of touchpad update
    ttu_None = 0,

    ttu_TouchMove,  // finger moved on touchpad
    ttu_TouchOut,   // finger left touchpad
    ttu_TouchIn,    // first finger contact

    ttu_Invalid
} e_ttc_touchpad_update;
typedef enum {   // e_ttc_touchpad_errorcode      return codes of TOUCHPAD devices
    ec_touchpad_OK = 0,

    // other warnings go here..

    ec_touchpad_ERROR,                  // general failure
    ec_touchpad_NULL,                   // NULL pointer not accepted
    ec_touchpad_DeviceNotFound,         // corresponding device could not be found
    ec_touchpad_InvalidConfiguration,   // sanity check of device configuration failed
    ec_touchpad_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..

    ec_touchpad_unknown                // no valid errorcodes past this entry
} e_ttc_touchpad_errorcode;
typedef enum {   // e_ttc_touchpad_architecture  types of architectures supported by TOUCHPAD driver
    ta_touchpad_None,           // no architecture selected

    ta_touchpad_analog4, // automatically added by ./create_DeviceDriver.pl
//InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_touchpad_unknown        // architecture not supported
} e_ttc_touchpad_architecture;
typedef union {  // architecture dependent configuration
    t_touchpad_analog4_config analog4;
//InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_touchpad_architecture;
typedef struct s_ttc_touchpad_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_touchpad_init()
    //       and after ttc_touchpad_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // low-level configuration (structure defined by low-level driver)
    u_ttc_touchpad_architecture* LowLevelConfig;

    // current physical position (without rotation)
    t_u16  Position_X;
    t_u16  Position_Y;

    // current logical position (respecting TTC_TOUCHPAD_ROTATION)
    t_u16  Rotated_X;
    t_u16  Rotated_Y;

    // touchpad configuration (allowed to be written before initialization only!)
    t_u16 Max_X;          // X-position is scaled to range 0..Max_X
    t_u16 Max_Y;          // Y-position is scaled to range 0..Max_Y
    t_s16 AdjustX;        // added to Position_X to adjust constant offset
    t_s16 AdjustY;        // added to Position_Y to adjust constant offset
    t_u8  Amount_Rereads; // Amount of values to read to smooth position

    // position readings from latest ttc_touchpad_check() run
    t_u8   Pressure;
    t_u8   Pressure_Previous;
    t_u8   Pressure_Counter;

    e_ttc_touchpad_architecture Architecture; // type of architecture used for current touchpad device
    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_TOUCHPAD1, ...)
    //? t_u8  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)

    union  { // generic configuration bits common for all low-level Drivers
        t_u8 All;
        struct {
            unsigned Initialized        : 1;  // ==1: device has been initialized successfully
            // ToDo: additional high-level flags go here..
            unsigned ContactLost        : 1;  // ==1: finger contact has been lost during previous ttc_touchpad_check()

            unsigned Reserved1          : 6; // pad to 8 bits
        } Bits;
    } Flags;

    // Additional high-level attributes go here..
} __attribute__( ( __packed__ ) ) t_ttc_touchpad_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_TOUCHPAD_TYPES_H
