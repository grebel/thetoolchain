/** { ttc_crc.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for crc devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Most functions of this driver are defined as macros and being implemented
 *  in interfaces/ttc_crc_interface.c or in low-level drivers crc/crc_*.c.
 *
 *  See corresponding ttc_crc.h for documentation of all high-level functions.
 *
 *  Created from template ttc_device.c revision 52 at 20180323 10:28:15 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_crc.h".
//
#include "ttc_crc.h"
#include "ttc_heap.h"       // dynamic memory allocationand safe arrays  
#include "ttc_task.h"       // disable/ enable multitasking scheduler
#ifdef EXTENSION_ttc_interrupt
    #include "ttc_interrupt.h"  // globally disable/ enable interrupts
#endif

//}Includes

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of crc devices.
 *
 */


// for each initialized device, a pointer to its generic and architecture specific definitions is stored
t_ttc_crc_config* ttc_crc_config = NULL;

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ********************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_crc(t_ttc_crc_config* Config)

void _ttc_crc_configuration_check( );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

t_ttc_crc_config*   ttc_crc_get_configuration( ) {
    t_ttc_crc_config* Config = ttc_crc_config; // will assert if outside array bounds

    if ( !Config ) {
#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_disable();                      // ensure that we are not interrupted while creating configuration
#endif

        Config = ttc_crc_config; // have to reload with disabled interrupts to avoid racing conditions
        if ( !Config ) {
            Config = ttc_crc_config = ttc_heap_alloc_zeroed( sizeof( t_ttc_crc_config ) );
            Config->Flags.Initialized = 0;             // make sure that deinit() is not called during load_defaults()
            ttc_crc_load_defaults( );
        }

#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_enable();
#endif
    }

    Assert_CRC_EXTRA_Writable( Config, ttc_assert_origin_auto ); // memory corrupted?
    TTC_TASK_RETURN( Config ); // will perform stack overflow check + return value
}
e_ttc_crc_errorcode ttc_crc_init( ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration
#if (TTC_ASSERT_CRC_EXTRA == 1)
    t_u8 InitialCriticalLevel = ttc_interrupt_critical_level();
#endif
    t_ttc_crc_config* Config = ttc_crc_get_configuration( );

    // check configuration to meet all limits of current architecture
    _ttc_crc_configuration_check( );

    e_ttc_crc_errorcode Error = _driver_crc_init( Config );
    if ( ! Error ) // == 0
    { Config->Flags.Initialized = 1; }

    Assert_CRC( Config->Architecture != 0, ttc_assert_origin_auto ); // Low-Level driver must set this value!
    Assert_CRC_EXTRA( InitialCriticalLevel == ttc_interrupt_critical_level(), ttc_assert_origin_auto ); // amount of  ttc_task_critical_end() calls does not match. Check driver implementation!
    ttc_task_critical_end(); // other tasks now may access this driver

    TTC_TASK_RETURN( Error ); // will perform stack overflow check + return value
}
e_ttc_crc_errorcode ttc_crc_load_defaults( ) {
    t_ttc_crc_config* Config = ttc_crc_get_configuration( );

    u_ttc_crc_architecture* LowLevelConfig = NULL;
    if ( Config->Flags.Initialized ) {
        LowLevelConfig = Config->LowLevelConfig; // rescue pointer to dynamic allocated memory
    }

    ttc_memory_set( Config, 0, sizeof( t_ttc_crc_config ) );

    // restore pointer to dynamic allocated memory
    Config->LowLevelConfig = LowLevelConfig;

    //Insert additional generic default values here ...

    // Let low-level driver initialize remaining fields
    e_ttc_crc_errorcode Error = _driver_crc_load_defaults( Config );

    // Check mandatory configuration items
    Assert_CRC_EXTRA( ( Config->Architecture > E_ttc_crc_architecture_None ) && ( Config->Architecture < E_ttc_crc_architecture_unknown ), ttc_assert_origin_auto ); // architecture not set properly! Low-Level driver must set this field!

    TTC_TASK_RETURN( Error ); // will perform stack overflow check + return value
}
void                ttc_crc_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)

    _driver_crc_prepare();
    ttc_crc_config = ttc_crc_get_configuration(); // will load default configuration
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_crc() {  }

void _ttc_crc_configuration_check( ) {
    t_ttc_crc_config* Config = ttc_crc_get_configuration( );

    // perform some generic plausibility checks
    const t_ttc_crc_features* Features = Features;

    /** Example plausibility check

    if (Config->BitRate > Features->MaxBitRate)   Config->BitRate = Features->MaxBitRate;
    if (Config->BitRate < Features->MinBitRate)   Config->BitRate = Features->MinBitRate;
    */

    // let low-level driver check this configuration too
    _driver_crc_configuration_check( Config );
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
