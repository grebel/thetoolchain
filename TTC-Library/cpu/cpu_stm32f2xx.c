/** cpu_stm32f2xx.c ***************************************{
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for cpu devices on stm32f2xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 27 at 20150819 17:55:07 UTC
 *
 *  Note: See ttc_cpu.h for description of stm32f2xx independent CPU implementation.
 * 
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "cpu_stm32f2xx.h".
 */

#include "cpu_stm32f2xx.h"
#include "../ttc_memory.h"            // basic memory checks
#include "cpu_common.h"          // generic functions shared by low-level drivers of all architectures 
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc(); 
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

t_ttc_cpu_config*  cpu_stm32f2xx_get_features(t_ttc_cpu_config* Config) {
    Assert_CPU_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // always check incoming pointers!

    static t_ttc_cpu_config Features;
    if (!Features.LogicalIndex) { // called first time: initialize data
        
      // Fill Features with maximum allowed values and enable all applicable Flags!
      
#     warning Function cpu_stm32f2xx_get_features() is empty.
  
    }
    Features.LogicalIndex = Config->LogicalIndex;
    
    return &Features;
}
e_ttc_cpu_errorcode  cpu_stm32f2xx_load_defaults(t_ttc_cpu_config* Config) {
    Assert_CPU_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // always check incoming pointers!

    // assumption: *Config has been zeroed
    Config->Architecture = ta_cpu_stm32f2xx;  // set type of architecture 

    if (0) { // enable for memory mapped devices (everything that has a base register)
      switch (Config->PhysicalIndex) { // load address of peripheral base register
      //case 0: Config->BaseRegister = & register_stm32f2xx_CPU1; break;
      //case 1: Config->BaseRegister = & register_stm32f2xx_CPU2; break;
      default: ttc_assert_halt_origin(ttc_assert_origin_auto); break; // invalid physical index: check your makefile and high-level driver implementation!
      }
    }
    
    // set individual feature flags    
    Config->Flags.All                        = 0;
  //Config->Flags.Bits.Foo                   = 1;

  
    return (e_ttc_cpu_errorcode) 0; // erverything OK
}
e_ttc_cpu_errorcode cpu_stm32f2xx_init(t_ttc_cpu_config* Config) {
    Assert_CPU_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function cpu_stm32f2xx_init() is empty.
    
    return (e_ttc_cpu_errorcode) 0;
}
void cpu_stm32f2xx_prepare() {


#warning Function cpu_stm32f2xx_prepare() is empty.
    

}
void cpu_stm32f2xx_reset(t_ttc_cpu_config* Config) {
    Assert_CPU_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function cpu_stm32f2xx_reset() is empty.
    

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by 
 * the maintainer of this file.
 */
 
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
