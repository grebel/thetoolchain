/** { cpu_stm32f1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for cpu devices on stm32f1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20150318 12:33:43 UTC
 *
 *  Note: See ttc_cpu.h for description of stm32f1xx independent CPU implementation.
 *
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "cpu_stm32f1xx.h".
//
#include "cpu_stm32f1xx.h"
#include "../ttc_memory.h"
#include "../ttc_register.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

e_ttc_cpu_errorcode cpu_stm32f1xx_init( t_ttc_cpu_config* Config ) {
    Assert_CPU_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_cpu_errorcode ) 0;
}
void cpu_stm32f1xx_prepare() {

    cpu_cortexm3_prepare();

    // clear reset flags (-> RM0008 p.118)
    register_stm32f1xx_RCC.CSR.RMVF = 1;
}
e_ttc_cpu_errorcode cpu_stm32f1xx_load_defaults( t_ttc_cpu_config* Config ) {
    Assert_CPU_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    return ( e_ttc_cpu_errorcode ) 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
