#ifndef CPU_CORTEXM3_TYPES_H
#define CPU_CORTEXM3_TYPES_H

/** { cpu_cortexm3.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for CPU devices on cortexm3 architectures.
 *  Structures, Enums and Defines being required by ttc_cpu_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150805 15:52:37 UTC
 *
 *  Note: See ttc_cpu.h for description of architecture independent CPU implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_cpu_types.h *************************

//} Structures/ Enums


#endif //CPU_CORTEXM3_TYPES_H
