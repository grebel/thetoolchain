/** { cm3_mutex.c **********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Architecture independent support for inter task communication & synchronization.
 *
 * Currently supported schedulers: FreeRTOS
}*/
#include "cm3_mutex.h"
#include "../ttc_heap.h"
#include "../ttc_systick.h"
#include "../ttc_interrupt.h"

//{ functions *************************************************

volatile cm3_Mutex* cm3_mutex_create() {

    volatile cm3_Mutex* NewMutex = ( volatile cm3_Mutex* ) ttc_heap_alloc( sizeof( cm3_Mutex ) );
    cm3_mutex_init( NewMutex );

    return NewMutex;
}
void                cm3_mutex_init( volatile cm3_Mutex* Lock ) {
    Assert_MUTEX_Writable( ( void* ) Lock, ttc_assert_origin_auto );

    // pretty simple
    *Lock = 0;
}
e_ttc_mutex_error   cm3_mutex_lock( volatile cm3_Mutex* Mutex, t_base TimeOut ) {
    Assert_MUTEX_Writable( ( void* ) Mutex, ttc_assert_origin_auto );
    Assert_MUTEX( ( ( ( t_u32 ) Mutex ) & 3 ) == 0, ttc_assert_origin_auto );  // 32-bit alignment required

    if ( cm3_mutex_lock_try( Mutex ) == tme_OK ) // general case first: try to obtain lock
    { return tme_OK; }                           // got lock: simple and fast

    if ( TimeOut == -1 ) {    // lock with endless timeout
#if (TTC_ASSERT_MUTEX == 1)
        if ( cm3_mutex_lock_try( Mutex ) != tme_OK ) { // could not obtain lock on first try: check if interrupts are enabled
            Assert_MUTEX( ttc_interrupt_critical_level() == 0, ttc_assert_origin_auto ); // interrupts disabled => dead lock. Check implementation of caller!
        }
        else
        { return tme_OK; }
#endif

        // wait until lock is free
        while ( cm3_mutex_lock_try( Mutex ) != tme_OK )
        { _ttc_mutex_wait(); }

        return tme_OK;
    }
    else if ( TimeOut ==  0 ) // no timeout: we already tried to obtain mutex without timeout
    { return tme_MutexAlreadyLocked; }
    else {                    // lock within timeout
        t_base CurrentTime = ttc_systick_get_elapsed_usecs();
        t_base EndTime     = CurrentTime + TimeOut;
        e_ttc_mutex_error LockFailed = tme_MutexAlreadyLocked;

        while ( ( EndTime < CurrentTime ) && // tick-timer can overflow during wait: wait until overflow
                LockFailed                 // or until successfull lock
              ) {
            LockFailed = cm3_mutex_lock_try( Mutex );
            if ( LockFailed ) {
                CurrentTime = ttc_systick_get_elapsed_usecs();
                _ttc_mutex_wait();
            }
        }

        while ( LockFailed &&
                ( CurrentTime < EndTime )
              ) {         // loop until successfull lock or timeout
            LockFailed = cm3_mutex_lock_try( Mutex );
            if ( LockFailed ) {
                CurrentTime = ttc_systick_get_elapsed_usecs();
                _ttc_mutex_wait();
            }
        };

        if ( !LockFailed )
        { return tme_OK; }

        return tme_TimeOut;
    }

    ttc_assert_halt_origin( ttc_assert_origin_auto );
    return tme_UNKNOWN; // this line should not be reachable
}
e_ttc_mutex_error   cm3_mutex_lock_try( volatile cm3_Mutex* Mutex ) {
    Assert_MUTEX_Writable( ( void* ) Mutex, ttc_assert_origin_auto );

    TODO( "speed up cm3_mutex_lock_try() by replacing function calls with single inline assembly" );
    // __ASM volatile ("ldrexb %0, [%1]" : "=r" (result) : "r" (addr) );
    // __ASM volatile ("strexb %0, %2, [%1]" : "=&r" (result) : "r" (addr), "r" (value) );
    // __ASM volatile ("clrex");

    if ( 1 ) { // C-Code
        volatile BOOL MutexAlreadyLocked;
        MUTEX_CM3_LDREX( Mutex, MutexAlreadyLocked );
        if ( MutexAlreadyLocked ) {
            return tme_MutexAlreadyLocked;
        }

        MUTEX_CM3_STREX( 1, Mutex, MutexAlreadyLocked );
        if ( MutexAlreadyLocked ) {
            __CLREX(); // remove lock created by __LDREXB()
            return tme_MutexAlreadyLocked;
        }
    }
    else { // Inline Assembly
        // ToDo
    }

    return tme_OK;
}
#if (TTC_ASSERT_MUTEX == 1)
e_ttc_mutex_error   cm3_mutex_lock_try_isr( volatile cm3_Mutex* Mutex ) {
    Assert_MUTEX( ttc_interrupt_check_inside_isr(), ttc_assert_origin_auto ); // must call this function from interrupt service routine or with disabled interrupts. Check implementation of caller!

    return cm3_mutex_lock_try( Mutex );
}
#endif

//}functions
//{ private functions ******************************************

void _cm3_mutex_lock_endless( volatile cm3_Mutex* Mutex ) {
    Assert_MUTEX_Writable( ( void* ) Mutex, ttc_assert_origin_auto );
    Assert_MUTEX( ( ( ( t_u32 ) Mutex ) & 3 ) == 0, ttc_assert_origin_auto );  // 32-bit alignment required

    if ( 0 ) {
        while ( cm3_mutex_lock_try( Mutex ) != tme_OK )
        { _ttc_mutex_wait(); }
    }
    else { // slightly faster implementation
        BOOL LockFailed;

#if (TTC_ASSERT_MUTEX == 1)
        MUTEX_CM3_LDREX( Mutex, LockFailed );
        if ( LockFailed ) { // could not obtain lock on first try: check if interrupts are enabled
            Assert_MUTEX( ttc_interrupt_critical_level() == 0, ttc_assert_origin_auto ); // interrupts disabled => dead lock. Check implementation of caller!
        }
        else
        { return; }  // successfully obtained lock on first try
#endif
        do {
            MUTEX_CM3_LDREX( Mutex, LockFailed );
            if ( !LockFailed ) {
                // try to obtain lock
                MUTEX_CM3_STREX( 1, Mutex, LockFailed );
                if ( LockFailed ) {
                    __CLREX();          // remove __LDREXB() lock
                }
            }
            else { _ttc_mutex_wait(); }     // locked by other task: give away cpu
        }
        while ( LockFailed );
    }
}
void _cm3_mutex_unlock( volatile cm3_Mutex* Mutex ) {
    Assert_MUTEX_Writable( ( void* ) Mutex, ttc_assert_origin_auto );
    //? Assert_MUTEX(*Mutex); // unlocking a mutex that is not locked!

    *Mutex = 0;
}

// private functions ********************************************
