/** { cpu_stm32l0xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for cpu devices on stm32l0xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150326 11:04:59 UTC
 *
 *  Note: See ttc_cpu.h for description of stm32l0xx independent CPU implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "cpu_stm32l0xx.h".
//
#include "cpu_stm32l0xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

t_ttc_cpu_config* cpu_stm32l0xx_get_features(t_ttc_cpu_config* Config) {
    Assert_CPU(Config, ttc_assert_origin_auto); // pointers must not be NULL
    
    return (t_ttc_cpu_config*) 0;
}
e_ttc_cpu_errorcode cpu_stm32l0xx_init(t_ttc_cpu_config* Config) {
    Assert_CPU(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function cpu_stm32l0xx_init() is empty.
    
    return (e_ttc_cpu_errorcode) 0;
}
e_ttc_cpu_errorcode cpu_stm32l0xx_load_defaults(t_ttc_cpu_config* Config) {
    Assert_CPU(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function cpu_stm32l0xx_load_defaults() is empty.
    
    return (e_ttc_cpu_errorcode) 0;
}
void cpu_stm32l0xx_prepare() {


#warning Function cpu_stm32l0xx_prepare() is empty.
    

}
void cpu_stm32l0xx_reset(t_ttc_cpu_config* Config) {
    Assert_CPU(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function cpu_stm32l0xx_reset() is empty.
    

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
