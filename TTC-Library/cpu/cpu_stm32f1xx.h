#ifndef CPU_STM32F1XX_H
#define CPU_STM32F1XX_H

/** { cpu_stm32f1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for cpu devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by low-level driver only.
 *
 *  Created from template device_architecture.h revision 22 at 20150318 12:33:43 UTC
 *
 *  Note: See ttc_cpu.h for description of stm32f1xx independent CPU implementation.
 *
 *  Authors: Gregor Rebel
 *
 *
 *  How to configure stm32f1xx cpu devices
 *
 *  This low-level driver requires configuration defines in addtion to the howto section
 *  of ttc_cpu.h.
 *
 *  PLACE YOUR DESCRIPTION HERE!
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_CPU_STM32F1XX
//
// Implementation status of low-level driver support for cpu devices on stm32f1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_CPU_STM32F1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_CPU_STM32F1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_CPU_STM32F1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_CPU_STM32F1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "cpu_stm32f1xx.c"
//
// DO NOT INCLUDE THIS HERE: #include "cpu_stm32f1xx_types.h"
#include "../ttc_cpu_types.h" // will include cpu_stm32f1xx_types.h at right place
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_cpu_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_cpu_foo
//
#define ttc_driver_cpu_deinit(Config)
#define ttc_driver_cpu_get_features(Config)  cpu_stm32f1xx_get_features(Config)
#define ttc_driver_cpu_init(Config)          cpu_stm32f1xx_init(Config)
#define ttc_driver_cpu_prepare()             cpu_stm32f1xx_prepare()
#define ttc_driver_cpu_reset(Config)         cpu_cortexm3_reset(Config)
#define ttc_driver_cpu_load_defaults(Config) cpu_stm32f1xx_load_defaults(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

#include "cpu_cortexm3.h" // must be included AFTER defining all ttc_driver_*() functions!

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_cpu.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_cpu.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

/** initializes single CPU unit for operation
 * @param Config        pointer to struct t_ttc_cpu_config
 * @return              == 0: CPU has been initialized successfully; != 0: error-code
 */
e_ttc_cpu_errorcode cpu_stm32f1xx_init( t_ttc_cpu_config* Config );


/** Prepares cpu Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void cpu_stm32f1xx_prepare();



/** loads configuration of indexed CPU unit with default values
 * @param Config        pointer to struct t_ttc_cpu_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_cpu_errorcode cpu_stm32f1xx_load_defaults( t_ttc_cpu_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _cpu_stm32f1xx_foo(t_ttc_cpu_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //CPU_STM32F1XX_H