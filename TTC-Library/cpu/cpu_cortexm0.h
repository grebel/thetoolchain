#ifndef CPU_CORTEXM0_H
#define CPU_CORTEXM0_H

/** { cpu_cortexm0.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for cpu devices on cortexm0 architectures.
 *  Structures, Enums and Defines being required by low-level driver only.
 *
 *  Created from template device_architecture.h revision 25 at 20150819 18:36:12 UTC
 *
 *  Note: See ttc_cpu.h for description of cortexm0 independent CPU implementation.
 *
 *  Authors: Gregor Rebel
 *
 *
 *  How to configure cortexm0 cpu devices
 *
 *  This low-level driver requires configuration defines in addtion to the howto section
 *  of ttc_cpu.h.
 *
 *  PLACE YOUR DESCRIPTION HERE!
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_CPU_CORTEXM0
//
// Implementation status of low-level driver support for cpu devices on cortexm0
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_CPU_CORTEXM0 '?'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_CPU_CORTEXM0 == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_CPU_CORTEXM0 to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_CPU_CORTEXM0

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "cpu_cortexm0.c"
//
#include "../ttc_cpu_types.h" // will include cpu_cortexm0_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_cpu_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_cpu_foo
//
#define ttc_driver_cpu_init(Config) cpu_cortexm0_init(Config)
#define ttc_driver_cpu_load_defaults(Config) cpu_cortexm0_load_defaults(Config)
#define ttc_driver_cpu_prepare() cpu_cortexm0_prepare()
#define ttc_driver_cpu_reset(Config) cpu_cortexm0_reset(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_cpu.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_cpu.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** initializes single CPU unit for operation
 * @param Config        pointer to struct t_ttc_cpu_config
 * @return              == 0: CPU has been initialized successfully; != 0: error-code
 */
e_ttc_cpu_errorcode cpu_cortexm0_init( t_ttc_cpu_config* Config );


/** loads configuration of indexed CPU unit with default values
 * @param Config        pointer to struct t_ttc_cpu_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_cpu_errorcode cpu_cortexm0_load_defaults( t_ttc_cpu_config* Config );


/** Prepares cpu Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void cpu_cortexm0_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_cpu_config
 */
void cpu_cortexm0_reset( t_ttc_cpu_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //CPU_CORTEXM0_H