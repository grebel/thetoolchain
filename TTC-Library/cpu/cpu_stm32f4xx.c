/** { cpu_stm32f4xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for cpu devices on stm32f4xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150319 08:08:35 UTC
 *
 *  Note: See ttc_cpu.h for description of stm32f4xx independent CPU implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "cpu_stm32f4xx.h".
//
#include "cpu_stm32f4xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

t_ttc_cpu_config* cpu_stm32f4xx_get_features(t_ttc_cpu_config* Config) {
    Assert_CPU(Config, ttc_assert_origin_auto); // pointers must not be NULL

    static t_ttc_cpu_config Features;
    if (!Features.LogicalIndex) { // called first time: initialize data
        
#     warning Function cpu_stm32f4xx_get_features() is empty.

    }
    Features.LogicalIndex = Config->LogicalIndex;
    
    return (t_ttc_cpu_config*) 0;
}
e_ttc_cpu_errorcode cpu_stm32f4xx_init(t_ttc_cpu_config* Config) {
    Assert_CPU(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function cpu_stm32f4xx_init() is empty.
    
    return (e_ttc_cpu_errorcode) 0;
}
e_ttc_cpu_errorcode cpu_stm32f4xx_load_defaults(t_ttc_cpu_config* Config) {
    Assert_CPU(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function cpu_stm32f4xx_load_defaults() is empty.
    
    return (e_ttc_cpu_errorcode) 0;
}
void cpu_stm32f4xx_prepare() {


#warning Function cpu_stm32f4xx_prepare() is empty.
    

}
void cpu_stm32f4xx_reset(t_ttc_cpu_config* Config) {
    Assert_CPU(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function cpu_stm32f4xx_reset() is empty.
    

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
