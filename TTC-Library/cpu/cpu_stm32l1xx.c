/** { cpu_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for cpu devices on stm32l1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20150322 21:39:33 UTC
 *
 *  Note: See ttc_cpu.h for description of stm32l1xx independent CPU implementation.
 *
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "cpu_stm32l1xx.h".
//
#include "cpu_stm32l1xx.h"
#include "../ttc_basic.h"
#include "../ttc_memory.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

t_ttc_cpu_config* cpu_stm32l1xx_get_features( t_ttc_cpu_config* Config ) {
    Assert_CPU_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    return ( t_ttc_cpu_config* ) 0;
}
e_ttc_cpu_errorcode cpu_stm32l1xx_init( t_ttc_cpu_config* Config ) {
    Assert_CPU_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    return ( e_ttc_cpu_errorcode ) 0;
}
e_ttc_cpu_errorcode cpu_stm32l1xx_load_defaults( t_ttc_cpu_config* Config ) {
    Assert_CPU_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_cpu_errorcode ) 0;
}
void cpu_stm32l1xx_prepare() {
    cpu_cortexm3_prepare();
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions