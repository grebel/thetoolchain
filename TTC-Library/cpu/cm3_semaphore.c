/*{ cm3_semaphore.c ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Architecture independent support for inter task communication & synchronization.
 *
 * Currently supported schedulers: FreeRTOS
}*/
#include "cm3_semaphore.h"
#include "../basic/basic_cm3.h"
#include "../ttc_systick.h"
#include "../ttc_heap.h"
#include "../ttc_task.h"

// wait function is provided by high-level driver
extern void ttc_semaphore_wait( t_ttc_semaphore* Semaphore, t_base TimeOut );

//{ functions *************************************************

// called when actively waiting for semaphore
void cm3_semaphore_wait() {
    ttc_task_yield();
}

t_u32 ttc__LDREXW( t_u32* Address ) { // for debugging purpose
    Assert_SEMAPHORE_Writable( Address, ttc_assert_origin_auto );
    return __LDREXW( Address );
}
t_cm3_semaphore* cm3_semaphore_create() {

    t_cm3_semaphore* NewSemaphore = ( t_cm3_semaphore* ) ttc_heap_alloc( sizeof( t_cm3_semaphore ) );

    return NewSemaphore;
}
void cm3_semaphore_init( t_cm3_semaphore* Semaphore ) {
    Assert_SEMAPHORE( ( ( ( t_u32 ) Semaphore ) & 3 ) == 0, ttc_assert_origin_auto );  // 4 byte alignment required for semaphores (e.g.: do not embedd into packed structures)!

    // initialization is pretty simple
    *Semaphore = 0;
}
e_ttc_semaphore_error cm3_semaphore_take( t_cm3_semaphore* Semaphore, t_base Amount, t_base TimeOut ) {
    Assert_SEMAPHORE_Writable( Semaphore, ttc_assert_origin_auto );

    if ( cm3_semaphore_take_try( Semaphore, Amount ) == tsme_OK ) // general case first: try to obtain Amount
    { return tsme_OK; }                        // got Amount: simple and fast

    if ( TimeOut == -1 ) {    // take with endless timeout
        // wait until Amount is free
        while ( cm3_semaphore_take_try( Semaphore, Amount ) != tsme_OK )
        { cm3_semaphore_wait(); }

        return tsme_OK;
    }
    else if ( TimeOut ==  0 ) // no timeout: we already tried to obtain semaphore without timeout
    { return tsme_SemaphoreExpired; }
    else {                    // take within timeout
        t_base CurrentTime = ttc_systick_get_elapsed_usecs();
        t_base EndTime     = CurrentTime + TimeOut;
        e_ttc_semaphore_error AmountFailed = tsme_SemaphoreExpired;

        while ( ( EndTime < CurrentTime ) && // tick-timer can overflow during wait: wait until overflow
                AmountFailed                 // or until successfull Amount
              ) {
            AmountFailed = cm3_semaphore_take_try( Semaphore, Amount );
            if ( AmountFailed ) {
                CurrentTime = ttc_systick_get_elapsed_usecs();
                cm3_semaphore_wait();
            }
        }

        while ( AmountFailed &&
                ( CurrentTime < EndTime )
              ) {         // loop until successfull Amount or timeout
            AmountFailed = cm3_semaphore_take_try( Semaphore, Amount );
            if ( AmountFailed ) {
                CurrentTime = ttc_systick_get_elapsed_usecs();
                cm3_semaphore_wait();
            }
        };

        if ( !AmountFailed )
        { return tsme_OK; }

        return tsme_TimeOut;
    }

    ttc_assert_halt_origin( ttc_assert_origin_auto );
    return tsme_UNKNOWN; // this line should not be reachable
}
e_ttc_semaphore_error cm3_semaphore_take_try( t_cm3_semaphore* Semaphore, t_base Amount ) {
    Assert_SEMAPHORE_Writable( Semaphore, ttc_assert_origin_auto );
    Assert_SEMAPHORE( ( ( ( t_u32 ) Semaphore ) & 3 ) == 0, ttc_assert_origin_auto );  // 4 byte alignment required for semaphores (e.g.: do not embedd into packed structures)!

    TODO( "speed up cm3_semaphore_take_try() by replacing function calls with single inline assembly" );
    // __ASM volatile ("ldrexb %0, [%1]" : "=r" (result) : "r" (addr) );
    // __ASM volatile ("strexb %0, %2, [%1]" : "=&r" (result) : "r" (addr), "r" (value) );
    // __ASM volatile ("clrex");

#if (CM3_SEMAPHORE_SIZE == 8) || (CM3_SEMAPHORE_SIZE == 16) || (CM3_SEMAPHORE_SIZE == 32)
    while ( 1 ) { // try to give back until read-write modification passed uninterrupted
#if CM3_SEMAPHORE_SIZE == 8 // byte sized semaphores
        t_u8 TokensAvailable = __LDREXB( Semaphore );
        if ( TokensAvailable < Amount )
        { return tsme_SemaphoreExpired; }
        TokensAvailable -= Amount;
        if ( ! __STREXB( TokensAvailable, Semaphore ) ) { // read-write modification was not interrupted
            __CLREX(); // remove lock created by __LDREXB()
            return tsme_OK;
        }
        __CLREX(); // remove lock created by __LDREXB()
#endif
#if CM3_SEMAPHORE_SIZE == 16 // half word sized semaphores
        t_u16 TokensAvailable = __LDREXH( Semaphore );
        if ( TokensAvailable < Amount )
        { return tsme_SemaphoreExpired; }
        TokensAvailable -= Amount;
        if ( ! __STREXH( TokensAvailable, Semaphore ) ) { // read-write modification was not interrupted
            __CLREX(); // remove lock created by __LDREXH()
            return tsme_OK;
        }
        __CLREX(); // remove lock created by __LDREXH()
#endif

#if CM3_SEMAPHORE_SIZE == 32 // word sized semaphores
        t_u32 TokensAvailable = __LDREXW( Semaphore );
        if ( TokensAvailable < Amount )
        { return tsme_SemaphoreExpired; }
        TokensAvailable -= Amount;
        if ( ! __STREXW( TokensAvailable, Semaphore ) ) { // read-write modification was not interrupted
            __CLREX(); // remove lock created by __LDREXW()
            return tsme_OK;
        }
        __CLREX(); // remove lock created by __LDREXW()
#endif
    }
#else
    ttc_assert_halt_origin( ttc_assert_origin_auto ); // missing implementation or wrong definition of CM3_SEMAPHORE_SIZE
    return tsme_NotImplemented;
#endif
}
void cm3_semaphore_give( t_cm3_semaphore* Semaphore, t_base Amount ) {
    Assert_SEMAPHORE_Writable( Semaphore, ttc_assert_origin_auto );

    // ToDo: speed up operation by replacing function calls with single inline assembly
    // __ASM volatile ("ldrexb %0, [%1]" : "=r" (result) : "r" (addr) );
    // __ASM volatile ("strexb %0, %2, [%1]" : "=&r" (result) : "r" (addr), "r" (value) );
    // __ASM volatile ("clrex");

#if (CM3_SEMAPHORE_SIZE == 8) || (CM3_SEMAPHORE_SIZE == 16) || (CM3_SEMAPHORE_SIZE == 32)
#if CM3_SEMAPHORE_SIZE == 8 // byte sized semaphores
    basic_cm3_08_atomic_add( Semaphore, ( t_u8 ) Amount );
#endif
#if CM3_SEMAPHORE_SIZE == 16 // byte sized semaphores
    basic_cm3_16_atomic_addSemaphore, ( t_u16 ) Amount );
#endif
#if CM3_SEMAPHORE_SIZE == 32 // byte sized semaphores
    basic_cm3_32_atomic_add( Semaphore, ( t_u32 ) Amount );
#endif
#else
#   warn Missing implementation or wrong definition of CM3_SEMAPHORE_SIZE
    ttc_assert_halt_origin( ttc_assert_origin_auto ); // missing implementation or wrong definition of CM3_SEMAPHORE_SIZE
#endif
}

// private functions ********************************************

//}functions
