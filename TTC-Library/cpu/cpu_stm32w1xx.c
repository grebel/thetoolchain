/** cpu_stm32w1xx.c ***************************************{
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for cpu devices on stm32w1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 27 at 20150806 10:29:19 UTC
 *
 *  Note: See ttc_cpu.h for description of stm32w1xx independent CPU implementation.
 * 
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "cpu_stm32w1xx.h".
 */

#include "cpu_stm32w1xx.h"
#include "../ttc_memory.h"            // basic memory checks
#include "cpu_common.h"          // generic functions shared by low-level drivers of all architectures 
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc(); 
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

t_ttc_cpu_config*  cpu_stm32w1xx_get_features(t_ttc_cpu_config* Config) {
    Assert_CPU_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // always check incoming pointers!

    // nothing to do here
    return NULL;
}
e_ttc_cpu_errorcode  cpu_stm32w1xx_load_defaults(t_ttc_cpu_config* Config) {
    Assert_CPU_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // always check incoming pointers!
    
    // set individual feature flags    
    Config->Flags.All                        = 0;
  //Config->Flags.Bits.Foo                   = 1;

  
    return (e_ttc_cpu_errorcode) 0; // erverything OK
}
e_ttc_cpu_errorcode cpu_stm32w1xx_init(t_ttc_cpu_config* Config) {
    Assert_CPU_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

    return (e_ttc_cpu_errorcode) 0;
}
void cpu_stm32w1xx_prepare() {


}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by 
 * the maintainer of this file.
 */
 
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
