#ifndef CPU_STM32W1XX_TYPES_H
#define CPU_STM32W1XX_TYPES_H

/** { cpu_stm32w1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for CPU devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by ttc_cpu_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150806 10:29:19 UTC
 *
 *  Note: See ttc_cpu.h for description of architecture independent CPU implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#include "../ttc_sysclock_types.h"

// these are defined in CPU makefile
#  define TTC_CPU_SIZE_RAM     TTC_MEMORY_REGION_RAM_SIZEK    * 1024
#  define TTC_CPU_SIZE_FLASH   TTC_MEMORY_REGION_ROM_SIZEK    * 1024
#  define TTC_CPU_SIZE_EEPROM  0

#if (TTC_CPU_VARIANT == TTC_CPU_VARIANT_stm32w108ccu)
    #define TTC_CPU_AMOUNT_PINS  48
    #define TTC_CPU_AMOUNT_GPIOS 23
    #define TTC_CPU_ARCHITECTURE ta_cpu_stm32w1xx
#endif

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_cpu_types.h *************************

typedef struct { // register description (adapt according to stm32w1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_cpu_register;

typedef struct {  // stm32w1xx specific configuration data of single cpu device
    t_cpu_register* BaseRegister;       // base address of cpu device registers
} __attribute__( ( __packed__ ) ) t_cpu_stm32w1xx_config;

// t_ttc_cpu_architecture is required by ttc_cpu_types.h
#define t_ttc_cpu_architecture t_cpu_stm32w1xx_config

//} Structures/ Enums


#endif //CPU_STM32W1XX_TYPES_H
