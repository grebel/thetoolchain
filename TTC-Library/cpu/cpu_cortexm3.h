#ifndef CPU_CORTEXM3_H
#define CPU_CORTEXM3_H

/** { cpu_cortexm3.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for cpu devices on cortexm3 architectures.
 *  Structures, Enums and Defines being required by low-level driver only.
 *
 *  Created from template device_architecture.h revision 25 at 20150805 15:52:37 UTC
 *
 *  Note: See ttc_cpu.h for description of cortexm3 independent CPU implementation.
 *
 *  Authors: Gregor Rebel
 *
 *
 *  How to configure cortexm3 cpu devices
 *
 *  This low-level driver requires configuration defines in addtion to the howto section
 *  of ttc_cpu.h.
 *
 *  PLACE YOUR DESCRIPTION HERE!
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_CPU_CORTEXM3
//
// Implementation status of low-level driver support for cpu devices on cortexm3
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_CPU_CORTEXM3 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_CPU_CORTEXM3 == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_CPU_CORTEXM3 to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_CPU_CORTEXM3

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "cpu_cortexm3.c"
//
#include "../ttc_cpu_types.h" // will include cpu_cortexm3_types.h (do not include it directly!)
#include "../register/register_cortexm3.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_cpu_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_cpu_foo
//
#ifndef ttc_driver_cpu_init
    #undef ttc_driver_cpu_init
#endif
#ifndef ttc_driver_cpu_load_defaults
    #undef ttc_driver_cpu_load_defaults
#endif
#ifndef ttc_driver_cpu_prepare
    #define ttc_driver_cpu_prepare()     cpu_cortexm3_prepare()
#endif
#ifndef ttc_driver_cpu_reset
    #define ttc_driver_cpu_reset(Config) cpu_cortexm3_reset(Config)
#endif
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_cpu.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_cpu.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Prepares cpu Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void cpu_cortexm3_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_cpu_config
 */
void cpu_cortexm3_reset( t_ttc_cpu_config* Config );


/** configure binary point of priority groups and sub groups
 *
 * BinaryPoint  new value to write into register_cortexm3_SCB.AIRCR.PRIGROUP
 */
void cpu_cortexm3_set_priority_group( e_register_cortexm3_aircr_prigroup BinaryPoint );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //CPU_CORTEXM3_H
