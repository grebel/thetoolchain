#ifndef CPU_STM32F1XX_TYPES_H
#define CPU_STM32F1XX_TYPES_H

/** { cpu_stm32f1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for CPU devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by ttc_cpu_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150318 12:33:43 UTC
 *
 *  Note: See ttc_cpu.h for description of architecture independent CPU implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Defines/ TypeDefs **********************************************************

// these are defined in CPU makefile
#  define TTC_CPU_SIZE_RAM     TTC_MEMORY_REGION_RAM_SIZEK    * 1024
#  define TTC_CPU_SIZE_FLASH   TTC_MEMORY_REGION_ROM_SIZEK    * 1024
#  define TTC_CPU_SIZE_EEPROM  0

// identify CPU architecture and its features from given variant
#if (TTC_CPU_VARIANT == TTC_CPU_VARIANT_stm32f051r8t6)
    #define TTC_CPU_AMOUNT_PINS  64
    #define TTC_CPU_AMOUNT_GPIOS 55
    #define TTC_CPU_ARCHITECTURE ta_cpu_stm32f0xx
#endif

#if (TTC_CPU_VARIANT == TTC_CPU_VARIANT_stm32f100rbt6)
    #define TTC_CPU_AMOUNT_PINS  64
    #define TTC_CPU_AMOUNT_GPIOS 51
    #define TTC_CPU_ARCHITECTURE ta_cpu_stm32f1xx
#endif

#if (TTC_CPU_VARIANT == TTC_CPU_VARIANT_stm32f103r8t6)
    #define TTC_CPU_AMOUNT_PINS  64
    #define TTC_CPU_AMOUNT_GPIOS 51
    #define TTC_CPU_ARCHITECTURE ta_cpu_stm32f0xx
#endif

#if (TTC_CPU_VARIANT == TTC_CPU_VARIANT_stm32f103zet6)
    #define TTC_CPU_AMOUNT_PINS  144
    #define TTC_CPU_AMOUNT_GPIOS 112
    #define TTC_CPU_ARCHITECTURE ta_cpu_stm32f0xx
#endif

#if (TTC_CPU_VARIANT == TTC_CPU_VARIANT_stm32f103rbt6)
    #define TTC_CPU_AMOUNT_PINS  64
    #define TTC_CPU_AMOUNT_GPIOS 51
    #define TTC_CPU_ARCHITECTURE ta_cpu_stm32f1xx
#endif

#if (TTC_CPU_VARIANT == TTC_CPU_VARIANT_stm32f103r6t6a)
    #define TTC_CPU_AMOUNT_PINS  64
    #define TTC_CPU_AMOUNT_GPIOS 51
    #define TTC_CPU_ARCHITECTURE ta_cpu_stm32f1xx
#endif

#if (TTC_CPU_VARIANT == TTC_CPU_VARIANT_stm32f103rdy6tr)
    #define TTC_CPU_AMOUNT_PINS  64
    #define TTC_CPU_AMOUNT_GPIOS 51
    #define TTC_CPU_ARCHITECTURE ta_cpu_stm32f0xx
#endif

#if (TTC_CPU_VARIANT == TTC_CPU_VARIANT_stm32f105rct6)
    #define TTC_CPU_AMOUNT_PINS  64
    #define TTC_CPU_AMOUNT_GPIOS 51
    #define TTC_CPU_ARCHITECTURE ta_cpu_stm32f1xx
#endif

#if (TTC_CPU_VARIANT == TTC_CPU_VARIANT_stm32f107vct6)
    #define TTC_CPU_AMOUNT_PINS  100
    #define TTC_CPU_AMOUNT_GPIOS 80
    #define TTC_CPU_ARCHITECTURE ta_cpu_stm32f1xx
#endif

#if (TTC_CPU_VARIANT == TTC_CPU_VARIANT_stm32f302r8t6)
    #define TTC_CPU_AMOUNT_PINS  64
    #define TTC_CPU_AMOUNT_GPIOS 51
    #define TTC_CPU_ARCHITECTURE ta_cpu_stm32f0xx
#endif

#if (TTC_CPU_VARIANT == TTC_CPU_VARIANT_stm32l053r8t6)
    #define TTC_CPU_AMOUNT_PINS  64
    #define TTC_CPU_AMOUNT_GPIOS 51
    #define TTC_CPU_ARCHITECTURE ta_cpu_stm32f0xx
#endif

#if (TTC_CPU_VARIANT == TTC_CPU_VARIANT_stm32w108ccu)
    #define TTC_CPU_AMOUNT_PINS  48
    #define TTC_CPU_AMOUNT_GPIOS 24
    #define TTC_CPU_ARCHITECTURE ta_cpu_stm32w1xx
#endif

#ifndef TTC_CPU_ARCHITECTURE
    #  warning Missing define TTC_CPU_ARCHITECTURE. Define it in your board makefile to one from the constants for ttc_cpu_types.h:e_ttc_cpu_architecture
    #define TTC_CPU_INFO_SET     1
    #define TTC_CPU_SIZE_RAM     16  * 1024
    #define TTC_CPU_SIZE_FLASH   256 * 1024
    #define TTC_CPU_SIZE_EEPROM  0
    #define TTC_CPU_AMOUNT_PINS  48
    #define TTC_CPU_AMOUNT_GPIOS 24
    #define TTC_CPU_ARCHITECTURE ta_cpu_None
#endif

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_cpu_types.h *************************


typedef struct {  // stm32f1xx specific configuration data of single CPU device
    t_u8 Unused;
} __attribute__( ( __packed__ ) ) t_cpu_stm32f1xx_config;

// t_ttc_cpu_architecture is required by ttc_cpu_types.h
#define t_ttc_cpu_architecture t_cpu_stm32f1xx_config

//} Structures/ Enums


#endif //CPU_STM32F1XX_TYPES_H
