#ifndef cm3_mutex_types_H
#define cm3_mutex_types_H

/*{ cm3_mutex.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Low-level datatypes for Mutual Exclusive Locks (Mutexes) on Cortex M3 architecture.
 *
}*/

//{ includes

#include "../ttc_basic_types.h"

//}includes

// datatype used to store individual mutex
#define CM3_MUTEX_SIZE 8

#if CM3_MUTEX_SIZE == 8
    typedef t_u8 cm3_Mutex; // smallest size but often creates misalignement of consecutive struct fields
#endif
#if CM3_MUTEX_SIZE == 16
    typedef t_u16 cm3_Mutex;
#endif
#if CM3_MUTEX_SIZE == 32
    typedef t_u32 cm3_Mutex; // does not change alignement; best debuggability (watchpoints always check 32 bits)
#endif

#define t_ttc_mutex volatile cm3_Mutex

//}

#endif
