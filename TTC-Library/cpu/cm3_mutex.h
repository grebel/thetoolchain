#ifndef cm3_mutex_H
#define cm3_mutex_H

/*{ cm3_mutex.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Low-level driver for Mutual Exclusive Locks (Mutexes) on Cortex M3 architecture.
 *
}*/

//{ includes


//}includes
//{ Default settings (change via makefile) *******************************


//}Default settings
//{ Types & Structures ***************************************************


//}
//{ Low-Level driver macros for ttc_mutex.h ******************************

#define _driver_mutex_create()               cm3_mutex_create()
#define _driver_mutex_init(Mutex)            cm3_mutex_init(Mutex)
#define _driver_mutex_lock(Mutex, TimeOut)   cm3_mutex_lock(Mutex, TimeOut)
#define _driver_mutex_lock_endless(Mutex)    cm3_mutex_lock_endless(Mutex)
#define _driver_mutex_lock_try(Mutex)        cm3_mutex_lock_try(Mutex)
#define _driver_mutex_lock_isr(Mutex)        cm3_mutex_lock_try_isr(Mutex)
#define _driver_mutex_is_locked(Mutex)       cm3_mutex_is_locked(Mutex)
#if TTC_ASSERT_MUTEXES == 1
    #define _driver_mutex_unlock(Mutex)          cm3_mutex_unlock(Mutex)
    #define _driver_mutex_unlock_isr(Mutex)      cm3_mutex_unlock(Mutex)

#else
    #define _driver_mutex_unlock(Mutex)          (*Mutex = 0)
    #define _driver_mutex_unlock_isr(Mutex)      (*Mutex = 0)

#endif

//}
//{ includes 2 ***********************************************************

// these includes require macros and types above
#include "cm3_mutex_types.h"
#include "../ttc_mutex_types.h"
#include "../ttc_memory_types.h"
#include "../ttc_basic_types.h"

//}includes 2
//{ macros ***************************************************************

// define exclusive access commands depending on configured mutex size
#if CM3_MUTEX_SIZE == 8 // byte sized mutexes
    #define MUTEX_CM3_LDREX(Mutex, Result)         __asm volatile ("ldrexb %0, [%1]" : "=r" (Result) : "r" (Mutex) )
    #define MUTEX_CM3_STREX(Value, Mutex, Result)  __asm volatile ("strexb %0, %2, [%1]" : "=&r" (Result) : "r" (Mutex), "r" (Value) )
#endif

#if CM3_MUTEX_SIZE == 16 // half word sized mutexes
    #define MUTEX_CM3_LDREX(Mutex, Result)         __asm volatile ("ldrexh %0, [%1]" : "=r" (Result) : "r" (Mutex) )
    #define MUTEX_CM3_STREX(Value, Mutex, Result)  __asm volatile ("strexh %0, %2, [%1]" : "=&r" (Result) : "r" (Mutex), "r" (Value) )
#endif

#if CM3_MUTEX_SIZE == 32 // word sized mutexes
    #define MUTEX_CM3_LDREX(Mutex, Result)         __asm volatile ("ldrexw %0, [%1]" : "=r" (Result) : "r" (Mutex) )
    #define MUTEX_CM3_STREX(Value, Mutex, Result)  __asm volatile ("strexw %0, %2, [%1]" : "=&r" (Result) : "r" (Mutex), "r" (Value) )
#endif

//}macros
//{ Function prototypes **************************************************


/** allocates + initializes a new non-recursive mutex lock
 * @return                 =! NULL: mutex has been created successfully (use handle to operate on mutex)
 */
volatile cm3_Mutex* cm3_mutex_create();

/** initializes an existing non-recursive mutex lock to the unlocked state
 * @Lock                 storage to be initialized as mutex
 */
void cm3_mutex_init( volatile cm3_Mutex* Lock );

/** Tries to obtain lock on mutex within timeout
 * @param Mutex  handle of already created mutex
 * @param TimeOut  !=-1: amount of system ticks to wait for lock
 *                 ==-1: wait forever
 * @return         == tme_OK: Lock has been successfully obtained within TimeOut
 */
e_ttc_mutex_error cm3_mutex_lock( volatile cm3_Mutex* Mutex, t_base TimeOut );

/** Blocks until lock has been obtained on mutex
 * @param Mutex  handle of already created mutex
 */
void cm3_mutex_lock_endless( volatile cm3_Mutex* Mutex );
#if TTC_ASSERT_MUTEXES == 1
    #define cm3_mutex_lock_endless(Mutex) _cm3_mutex_lock_endless(Mutex)
#else
    //#  define cm3_mutex_lock_endless(Mutex) _cm3_mutex_lock_endless(Mutex)
    #define cm3_mutex_lock_endless(Mutex) while (cm3_mutex_lock_try(Mutex) != tme_OK) _ttc_mutex_wait()
#endif
void _cm3_mutex_lock_endless( volatile cm3_Mutex* Mutex );


/** Tries to obtain lock on mutex; this function will never block
 * @param Mutex    handle of already created mutex
 * @return         == tme_OK: Lock has been successfully obtained within TimeOut
 *                 == tme_WasLockedByTask: - a task of higher priority has been interrupted => call cm3_task_yield() at end of your interrupt service routine!
 *                 >= tme_Error: Error condition
 */
e_ttc_mutex_error cm3_mutex_lock_try( volatile cm3_Mutex* Mutex );

/** Tries to obtain lock on mutex; this function will never block (only to be called by interrupt service routines!)
 * @param Mutex    handle of already created mutex
 * @return         == tme_OK: Lock has been successfully obtained within TimeOut
 *                 == tme_WasLockedByTask: - a task of higher priority has been interrupted => call cm3_task_yield() at end of your interrupt service routine!
 *                 >= tme_Error: Error condition
 */
e_ttc_mutex_error cm3_mutex_lock_try_isr( volatile cm3_Mutex* Mutex );
#if (TTC_ASSERT_MUTEX != 1) // no self tests: use macro instead of function
    #define cm3_mutex_lock_try_isr(Mutex) cm3_mutex_lock_try(Mutex)
#endif

/** Checks if given Mutex is currently locked
 * @param Mutex    handle of already created mutex
 * @return         == TRUE: given Mutex is currently in locked state; FALSE otherwise
 */
// BOOL cm3_mutex_is_locked(volatile cm3_Mutex* Mutex);
#define cm3_mutex_is_locked(Mutex) (Mutex && (*Mutex != 0))

/** Releases an already obtained lock from mutex
 * @param Mutex  handle of already created smart mutex
 * @return         == tme_OK: Lock has been released successfully
 */
void cm3_mutex_unlock( volatile cm3_Mutex* Mutex );
#if TTC_ASSERT_MUTEXES == 1
    #define cm3_mutex_unlock(Mutex) _cm3_mutex_unlock(Mutex)
#else
    #define cm3_mutex_unlock(Mutex) (*Mutex = 0)
#endif
void _cm3_mutex_unlock( volatile cm3_Mutex* Mutex );

//}Function prototypes

#endif
