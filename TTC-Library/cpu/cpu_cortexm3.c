/** cpu_cortexm3.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for cpu devices on cortexm3 architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 27 at 20150805 15:52:37 UTC
 *
 *  Note: See ttc_cpu.h for description of cortexm3 independent CPU implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "cpu_cortexm3.h".
 */

#include "cpu_cortexm3.h"
#include "../ttc_memory.h"            // basic memory checks
#include "cpu_common.h"          // generic functions shared by low-level drivers of all architectures 
#include "../register/register_cortexm3.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

void cpu_cortexm3_prepare() {

    // enable division by zero fault (aids debugging)
    register_cortexm3_SCB.CCR.DIV_0_TRP   = 1;

    if ( 0 ) // enable unaligned memory access trap (these accesses are slower than aligned ones)
    { register_cortexm3_SCB.CCR.UNALIGN_TRP = 1; }

    // enable bus fault
    register_cortexm3_SCB.SHCSR.BUSFAULTENA = 1;

    // enable MMU fault
    register_cortexm3_SCB.SHCSR.MEMFAULTENA = 1;

    // enable usage fault
    register_cortexm3_SCB.SHCSR.USGFAULTENA = 1;

    if ( 1 ) { // set default binary point for interrupt priorities
        cpu_cortexm3_set_priority_group( register_cortexm3_priority_group_4 );
    }
}
void cpu_cortexm3_reset( t_ttc_cpu_config* Config ) {
    ( void ) Config;

    // set reset request bit + access key to request a reset
    *( ( t_u32* ) &register_cortexm3_SCB.AIRCR ) = 0x05fa0004;

    // wait for reset to happen
    while ( 1 );
}
void cpu_cortexm3_set_priority_group( e_register_cortexm3_aircr_prigroup BinaryPoint ) {

    volatile register_cortexm3_SCB_AIRCR_t AIRCR;
    *( ( volatile t_u32* ) &AIRCR ) = *( ( volatile t_u32* ) &register_cortexm3_SCB.AIRCR );

    AIRCR.VECTKEYSTAT = 0x5fa; // set key to allow register write access
    AIRCR.PRIGROUP    = BinaryPoint; // no sub priorities (recommended by FreeRTOS documentation ->http://www.freertos.org/RTOS-Cortex-M3-M4.html)

    *( ( volatile t_u32* ) &register_cortexm3_SCB.AIRCR ) = *( ( volatile t_u32* ) &AIRCR );
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
