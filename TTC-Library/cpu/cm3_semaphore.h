#ifndef cm3_semaphore_H
#define cm3_semaphore_H

/*{ cm3_semaphore.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Low-level driver for Mutual Exclusive Amounts (Semaphores) on Cortex M3 architecture.
 *
}*/

//{ includes

#include "../ttc_basic.h"

//}includes
//{ Default settings (change via makefile) ******************************


//}Default settings
//{ Types & Structures ***************************************************


//}
//{ Low-Level driver macros for ttc_semaphore.h ******************************

#define _driver_semaphore_create()                             cm3_semaphore_create()
#define _driver_semaphore_init(Semaphore)                      cm3_semaphore_init(Semaphore)
#define _driver_semaphore_take(Semaphore, Amount, TimeOut)     cm3_semaphore_take(Semaphore, Amount, TimeOut)
#define _driver_semaphore_take_try(Semaphore, Amount)          cm3_semaphore_take_try(Semaphore, Amount)
#define _driver_semaphore_can_provide(Semaphore, Amount)       cm3_semaphore_can_provide(Semaphore, Amount)
#define _driver_semaphore_give(Semaphore, Amount)              cm3_semaphore_give(Semaphore, Amount)
#define _driver_semaphore_available(Semaphore)                 (*(Semaphore))

// using default implementation for these functions (no exclusive access required during interrupt service routine)
//X #define _driver_semaphore_take_isr(Semaphore, Amount)          cm3_semaphore_take_try_isr(Semaphore, Amount)
//X #define _driver_semaphore_give_isr(Semaphore, Amount)          cm3_semaphore_give(Semaphore, Amount)

//}
//{ includes 2 ***********************************************************

#include "cm3_semaphore_types.h"

// these includes require macros and types above
#include "../ttc_semaphore_types.h"
#include "../ttc_memory.h"

//}includes 2
//{ Function prototypes **************************************************


/** allocates + initializes a new non-recursive semaphore Amount
 * @return                 =! NULL: semaphore has been created successfully (use handle to operate on semaphore)
 */
t_cm3_semaphore* cm3_semaphore_create();

/** initializes an existing non-recursive semaphore to zero
 * @Amount                 storage to be initialized as semaphore
 */
void cm3_semaphore_init( t_cm3_semaphore* Semaphore );

/** Tries to take given amount from semaphore within timeout
 * @param Semaphore  handle of already created semaphore
 * @param TimeOut  !=-1: amount of system ticks to wait for Amount
 *                 ==-1: wait forever
 * @return         == tsme_OK: Amount has been successfully obtained within TimeOut
 */
e_ttc_semaphore_error cm3_semaphore_take( t_cm3_semaphore* Semaphore, t_base Amount, t_base TimeOut );

/** Tries to take given amount from semaphore; this function will never block and must only be called with disabled interrupts!
 * @param Semaphore                handle of already created semaphore
 * @return         == tsme_OK: Amount has been successfully obtained within TimeOut
 *                 >= tsme_Error: Error condition
 */
e_ttc_semaphore_error cm3_semaphore_take_try( t_cm3_semaphore* Semaphore, t_base Amount );

/** Tries to take given amount from semaphore; this function will never block and must only be called with disabled interrupts! (only to be called by interrupt service routines!)
 * @param Semaphore    handle of already created semaphore
 * @return         == tsme_OK: Amount has been successfully obtained within TimeOut
 *                 >= tsme_Error: Error condition
 */
e_ttc_semaphore_error cm3_semaphore_take_try_isr( t_cm3_semaphore* Semaphore, t_base Amount );
#define cm3_semaphore_take_try_isr(Semaphore, Amount) cm3_semaphore_take_try(Semaphore, Amount)

/** Checks if value of given Semaphore >= Amount
 * @param Semaphore    handle of already created semaphore
 * @return         == TRUE: given Semaphore can provide given amount; FALSE otherwise
 */
// BOOL cm3_semaphore_can_provide(cm3_Semaphore* Semaphore, t_base Amount);
#define cm3_semaphore_can_provide(Semaphore, Amount) ( ( *(Semaphore) >= Amount ) ? 1 : 0 )

/** Gives amount of tokens back to semaphore
 * @param Semaphore  handle of already created smart semaphore
 */
void cm3_semaphore_give( t_cm3_semaphore* Semaphore, t_base Amount );

//}Function prototypes

#endif
