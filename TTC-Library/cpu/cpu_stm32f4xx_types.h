#ifndef CPU_STM32F4XX_TYPES_H
#define CPU_STM32F4XX_TYPES_H

/** { cpu_stm32f4xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for CPU devices on stm32f4xx architectures.
 *  Structures, Enums and Defines being required by ttc_cpu_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150319 08:08:35 UTC
 *
 *  Note: See ttc_cpu.h for description of architecture independent CPU implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_cpu_types.h *************************

typedef struct { // register description (adapt according to stm32f4xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_cpu_register;

typedef struct {  // stm32f4xx specific configuration data of single CPU device
    t_cpu_register* BaseRegister;       // base address of CPU device registers
} __attribute__( ( __packed__ ) ) t_cpu_stm32f4xx_config;

// t_ttc_cpu_architecture is required by ttc_cpu_types.h
#define t_ttc_cpu_architecture t_cpu_stm32f4xx_config

//} Structures/ Enums


#endif //CPU_STM32F4XX_TYPES_H
