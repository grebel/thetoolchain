#ifndef cm3_semaphore_types_H
#define cm3_semaphore_types_H

/*{ cm3_semaphore.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Low-level driver for Mutual Exclusive Amounts (Semaphores) on Cortex M3 architecture.
 *
}*/

//{ includes

#include "../ttc_basic_types.h"

//}includes
//{ Types & Structures ***************************************************

// datatype used to store individual semaphore
#define CM3_SEMAPHORE_SIZE 32

#if CM3_SEMAPHORE_SIZE == 8
    typedef t_u8 t_cm3_semaphore;    // smallest memory usage but often requires pad bytes to align following values
#endif
#if CM3_SEMAPHORE_SIZE == 16
    typedef t_u16 t_cm3_semaphore;
#endif
#if CM3_SEMAPHORE_SIZE == 32
    typedef t_u32 t_cm3_semaphore; // biggest memory usage but does not alter memory alignement
#endif

#define t_driver_semaphore t_cm3_semaphore

//}

#endif
