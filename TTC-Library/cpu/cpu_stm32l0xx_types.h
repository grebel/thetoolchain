#ifndef CPU_STM32L0XX_TYPES_H
#define CPU_STM32L0XX_TYPES_H

/** { cpu_stm32l0xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for CPU devices on stm32l0xx architectures.
 *  Structures, Enums and Defines being required by ttc_cpu_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150326 11:04:59 UTC
 *
 *  Note: See ttc_cpu.h for description of architecture independent CPU implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#  define TTC_SYSCLOCK_FREQUENCY_MAX 32000000
#  define TTC_CPU_SIZE_RAM     8  * 1024
#  define TTC_CPU_SIZE_FLASH   64* 1024
#  define TTC_CPU_SIZE_EEPROM  2   * 1024
#  define TTC_CPU_AMOUNT_PINS  64
#  define TTC_CPU_AMOUNT_GPIOS 51
#  define TTC_CPU_ARCHITECTURE ta_cpu_stm32l0xx

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_cpu_types.h *************************

typedef struct { // register description (adapt according to stm32l0xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_cpu_register;

typedef struct {  // stm32l0xx specific configuration data of single CPU device
    t_cpu_register* BaseRegister;       // base address of CPU device registers
} __attribute__( ( __packed__ ) ) t_cpu_stm32l0xx_config;

// t_ttc_cpu_architecture is required by ttc_cpu_types.h
#define t_ttc_cpu_architecture t_cpu_stm32l0xx_config

//} Structures/ Enums


#endif //CPU_STM32L0XX_TYPES_H
