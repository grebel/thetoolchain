#ifndef SLAM_SIMPLE_2D_TYPES_H
#define SLAM_SIMPLE_2D_TYPES_H

/** { slam_simple_2d.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_slam.h providing
 *  Structures, Enums and Defines being required by ttc_slam_types.h
 *
 *  2 dimensional localization and mapping storing all n! distances between n nodes
 *
 *  Created from template device_architecture_types.h revision 24 at 20160607 10:19:25 UTC
 *
 *  Note: See ttc_slam.h for description of high-level slam implementation.
 *  Note: See slam_simple_2d.h for description of simple_2d slam implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_SLAM1   // device not defined in makefile
    #define TTC_SLAM1    ta_slam_simple_2d   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "../ttc_math_types.h"
#include "../ttc_list_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_slam_types.h *************************

typedef enum { // e_slam_simple_2d_node_index
    ss2_Undefined,
    simple_2d_index_southpole = 1, // index of special node southpole
    simple_2d_index_northpole = 2, // index of special node northpole
    ss2_unknown
} e_slam_simple_2d_node_index;

typedef struct  { // distance between two nodes A and B
    //X    t_u8              NodeA;       // index number of node A
    //X    t_u8              NodeB;       // index number of node B
    ttm_number        Average;     // averaged distance value (type of calculation depends on current low-level driver)

    // Array below is only available if Config->Init.AmountRawMeasures > 1
    // (Will not store single raw distance as it would be the same as average)
    ttm_number        RawValues[]; // last reported raw distance values (size TTC_SLAM<n>_AMOUNT_DISTANCE_MEASURES may differ for each ttc_slam instance)
} __attribute__( ( __packed__ ) ) t_sc2d_node_distance;

typedef struct { // configuration of single node
    t_sc2d_node_distance* Distance2North;  // distance from current node to NorthPole (#2)
    t_sc2d_node_distance* Distance2South;  // distance from current node to SouthPole (#1)
} __attribute__( ( __packed__ ) ) t_slam_simple_2d_node;

typedef struct { // configuration of low-level driver
    t_u8 AmountComplete; // amount of nodes with complete data (initial mapping possible when AmountComplete==t_ttc_slam_config.Amount_Nodes
} __attribute__( ( __packed__ ) ) t_slam_simple_2d_config;

//} Structures/ Enums


#endif //SLAM_SIMPLE_2D_TYPES_H
