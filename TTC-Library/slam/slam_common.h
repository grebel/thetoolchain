#ifndef slam_common_h
#define slam_common_h

/** slam_common.h *****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to slam low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.h revision 13 at 20160606 12:07:47 UTC
 *
 *  Authors: Gregor Rebel
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "slam_common.c"
 */

#include "../ttc_basic.h"
#include "../ttc_slam_types.h" // will include slam_common_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_slam_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_slam_foo
 */

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions
/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_slam.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_slam.h for details.
 *
 * You normally should not need to add functions here.
 * Global feature functions must be added in ttc_slam.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl slam UPDATE".
 */

/** low-level drivers should always use this function to update coordinates of single node!
 *
 * This high-level function also takes care of bounding box.
 */
extern void ttc_slam_update_coordinates( t_u8 LogicalIndex, t_u16 NodeIndex, ttm_number X, ttm_number Y, ttm_number Z );
TODO( "extract implementation from ttc_slam_update_coordinates() into slam_common_update_coordinates() to get rid of this extern declaration!" )
//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //slam_common_h
