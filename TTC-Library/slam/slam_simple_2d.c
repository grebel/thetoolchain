/** slam_simple_2d.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_slam.c.
 *
 *  2 dimensional localization and mapping storing only 2*n - 2 distances between n nodes
 *
 *  Created from template device_architecture.c revision 32 at 20160607 10:19:25 UTC
 *
 *  Note: See ttc_slam.h for description of high-level slam implementation.
 *  Note: See slam_simple_2d.h for description of simple_2d slam implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "slam_simple_2d.h".
 */

#include "slam_simple_2d.h"
#include "../ttc_basic_types.h"   // basic datatypes
#include "../ttc_memory.h"        // basic memory checks
#include "../ttc_task.h"          // stack checking (provides TTC_TASK_RETURN() macro)
#include "../ttc_math.h"          // integer and floating point mathematic calculations
#include "../ttc_heap.h"          // dynamic memory allocation and memory pool
#include "slam_common.h"          // generic functions shared by low-level drivers of all architectures
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_slam_features slam_simple_2d_Features = {
    /** example features
      .MinBitRate       = 4800,
      .MaxBitRate       = 230400,
    */
    // set more feature values...

    .Coordinates_2D     = 1,
    .Coordinates_3D     = 0,
    .Placement_Simple   = 1,
    .Placement_Random   = 0,
    .MaxAmountNodes     = 255
};

// size of single t_ttc_slam_node (size of each entry of t_ttc_slam_config.Nodes[]
#define SS2D_NODES_ALIGNED 0
const t_u16 ss2_Size_SlamNode = (
#if SS2D_NODES_ALIGNED == 1
                                    (
#endif
                                        sizeof( t_ttc_slam_node )        // size of total structure including u_ttc_slam_node_driver
                                        - sizeof( u_ttc_slam_node_driver ) // size of union of all architectures
                                        + sizeof( t_slam_simple_2d_node )  // size of one element from union
#if SS2D_NODES_ALIGNED == 1
#warning ss2_Size_SlamNode = multiple of 4 wastes 3 bytes per node. ToDo: Check if required!
                                        + 3 ) & ( 0xffff - 3 )            // make size a multiple of 4
#endif
                                );
//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

BOOL                  slam_simple_2d_calculate_mapping( t_ttc_slam_config* Config ) {

    if ( Config->Flags.Completed_Distances ) {   // have egnough data: calculate mapping
        if ( ! Config->Flags.Completed_Mapping ) { // first mapping: all Distance2North -> SouthPole, Distance2South -> NorthPole

            // all nodes will use south- and north pole as fixpoints
            ttm_number Fy = slam_simple_2d_get_node( Config, simple_2d_index_northpole )->Position.Y;
            ttm_number Nx, Ny;

            for ( t_u8 NodeIndex = Config->Init.Amount_Nodes; NodeIndex > simple_2d_index_northpole; NodeIndex-- ) {

                // Obtain pointers to indexed node
                t_ttc_slam_node* SLAM_Node = slam_simple_2d_get_node( Config, NodeIndex );
                t_slam_simple_2d_node* Simple2D_Node = ( t_slam_simple_2d_node* ) & ( SLAM_Node->Driver.simple_2d );

                Nx = SLAM_Node->Position.X;
                Ny = SLAM_Node->Position.Y;

                BOOL LeftSide = 1 - ( NodeIndex & 1 ); // east (right side) nodes are odd
                if ( Fy < 0 )
                { LeftSide = 1 - LeftSide; } // line directed downwards: east is now on other side

                if ( LeftSide ) {
                    ttc_math_lateration_2d(
                        Simple2D_Node->Distance2North->Average,  // distance south pole (0, 0)  to node
                        Simple2D_Node->Distance2South->Average,  // distance north pole (0, Fy) to node
                        0, Fy,                                   // using north pole as second fixpoint (first is origin)
                        & Nx,                                    // calculated x-coordinate
                        & Ny,                                    // calculated y-coordinate
                        NULL,
                        NULL
                    );
                }
                else {
                    ttc_math_lateration_2d(
                        Simple2D_Node->Distance2North->Average,  // distance south pole (0, 0)  to node
                        Simple2D_Node->Distance2South->Average,  // distance north pole (0, Fy) to node
                        0, Fy,                                   // using north pole as second fixpoint (first is origin)
                        NULL,
                        NULL,
                        & Nx,                                    // calculated x-coordinate
                        & Ny                                     // calculated y-coordinate
                    );
                }

                // Must use high-level function to correctly update node coordinates!
                ttc_slam_update_coordinates( Config->LogicalIndex, NodeIndex, Nx, Ny, SLAM_Node->Position.Z );
            }
            Config->Flags.Completed_Mapping = 1;
        }
        return TRUE;
    }

    return ( BOOL ) 0;
}
void                  slam_simple_2d_configuration_check( t_ttc_slam_config* Config ) {
    Assert_SLAM_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    Assert_SLAM( ( Config->Init.Amount_Nodes < 256 ) && ( Config->Init.Amount_Nodes > 0 ), ttc_assert_origin_auto );  // simple_2d only supports 1..255 nodes!
}
e_ttc_slam_errorcode  slam_simple_2d_deinit( t_ttc_slam_config* Config ) {
    Assert_SLAM_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    return ( e_ttc_slam_errorcode ) 0;
}
ttm_number            slam_simple_2d_get_distance( t_ttc_slam_config* Config, t_u16 NodeIndexA, t_u16 NodeIndexB ) {
    Assert_SLAM_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    if ( NodeIndexA > NodeIndexB ) { // ensure that NodeIndexA < NodeIndexB
        t_u16 Temp = NodeIndexA;
        NodeIndexA = NodeIndexB;
        NodeIndexB = Temp;
    }

    t_ttc_slam_node* Node = slam_simple_2d_get_node( Config, NodeIndexB );
    t_sc2d_node_distance* TTC_SLAM_VOLATILE Distance = NULL;
    if ( NodeIndexA == 1 ) { // get distance to south pole
        Distance = Node->Driver.simple_2d.Distance2South;
    }
    if ( NodeIndexA == 2 ) { // get distance to north pole
        Distance = Node->Driver.simple_2d.Distance2North;
    }

    if ( Distance == NULL )
    { return TTC_SLAM_UNDEFINED_VALUE; }

    // we store a distance: check for valid pointer and return it
    Assert_SLAM_Writable( Distance, ttc_assert_origin_auto );

    return Distance->Average;
}
t_ttc_slam_node*      slam_simple_2d_get_node( t_ttc_slam_config* Config, t_u16 NodeIndex ) {
    Assert_SLAM_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_SLAM( NodeIndex <= Config->Init.Amount_Nodes, ttc_assert_origin_auto );  // invalid node index given (allowed range: 1..Config->Init.Amount_Nodes)
    Assert_SLAM( NodeIndex  > 0, ttc_assert_origin_auto );                    // invalid node index given (allowed range: 1..Config->Init.Amount_Nodes)

    // calculate memory address of desired node
    NodeIndex--; // array starts at index zero
    return ( t_ttc_slam_node* )( ( ( ( t_base ) Config->Nodes ) + NodeIndex * ss2_Size_SlamNode ) );
}
e_ttc_slam_errorcode  slam_simple_2d_init( t_ttc_slam_config* Config ) {
    Assert_SLAM_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_slam_simple_2d_config* ConfigLL = ( t_slam_simple_2d_config* ) Config->LowLevelConfig;
    if ( !ConfigLL ) { // first call: allocate configuration
        ConfigLL = ttc_heap_alloc_zeroed( sizeof( t_slam_simple_2d_config ) );

        // we allocate egnough memory to store 2 distances for every node
        t_u16 Amount_Distances = 2 * Config->Init.Amount_Nodes;

        // size of pool elements depends on desired amount of raw measures to store for each distance
        t_sc2d_node_distance Dummy; ( void ) Dummy;
        t_u16 Size_NodeDistance = sizeof( Dummy.RawValues[0] ) * Config->Init.AmountRawMeasures
                                  + sizeof( t_sc2d_node_distance );

        // init memory pool for distance measures
        ttc_heap_pool_init( & Config->Pool_Distances, Size_NodeDistance, Amount_Distances );

        Assert_SLAM( sizeof( Config->LowLevelConfig->simple_2d ) == sizeof( *ConfigLL ), ttc_assert_origin_auto );  // These should be the same structures. Check implementation!
        Config->LowLevelConfig = ( u_ttc_slam_architecture* ) ConfigLL;

        // allocate array Config->Nodes[Amount_Nodes]
        Config->Nodes = ttc_heap_alloc_zeroed( Config->Init.Amount_Nodes * ss2_Size_SlamNode );
    }

    return ( e_ttc_slam_errorcode ) 0;
}
e_ttc_slam_errorcode  slam_simple_2d_load_defaults( t_ttc_slam_config* Config ) {
    Assert_SLAM_EXTRA_Writable( Config, ttc_assert_origin_auto );  // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = ta_slam_simple_2d;         // set type of architecture
    Config->Features     = &slam_simple_2d_Features;  // assign feature set

    if ( 0 ) { // enable for memory mapped devices (everything that has a base register)
        switch ( Config->PhysicalIndex ) { // load address of peripheral base register
            //case 0: Config->BaseRegister = & register_simple_2d_SLAM1; break;
            //case 1: Config->BaseRegister = & register_simple_2d_SLAM2; break;
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid physical index: check your makefile and high-level driver implementation!
        }
    }

    // set individual feature flags
    ttc_memory_set( &( Config->Flags ), 0, sizeof( Config->Flags ) );
    //Config->Flags.Foo                   = 1;
    // add more flags...


    TTC_TASK_RETURN( ec_slam_OK ); // will perform stack overflow check + return value
}
void                  slam_simple_2d_prepare() {

    // double check constant definitions to be sure that implementation works as expected
    Assert_SLAM_EXTRA( simple_2d_index_southpole == 1, ttc_assert_origin_auto );
    Assert_SLAM_EXTRA( simple_2d_index_northpole == 2, ttc_assert_origin_auto );

}
void                  slam_simple_2d_reset( t_ttc_slam_config* Config ) {
    Assert_SLAM_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    // set z-coordinate of all nodes to 0
    for ( t_u16 NodeIndex = Config->Init.Amount_Nodes; NodeIndex > 0; NodeIndex-- ) {
        t_ttc_slam_node* Node = slam_simple_2d_get_node( Config, NodeIndex );

        // release remaining distances to their pool
        if ( Node->Driver.simple_2d.Distance2North )
        { ttc_heap_pool_block_free( Node->Driver.simple_2d.Distance2North ); }
        if ( Node->Driver.simple_2d.Distance2South )
        { ttc_heap_pool_block_free( Node->Driver.simple_2d.Distance2South ); }

        // reset Z-coordinate as this driver supports only 2D slam
        Node->Position.Z = 0;

        // reset rest of driver specific node data
        ttc_memory_set( &( Node->Driver.simple_2d ), 0, sizeof( Node->Driver.simple_2d ) );
    }

    // reset low-level configuration
    ttc_memory_set( Config->LowLevelConfig, 0, sizeof( Config->LowLevelConfig->simple_2d ) );

    // fix SouthPole at (0,0,0)
    ttc_slam_update_coordinates( Config->LogicalIndex, 1, 0, 0, 0 );
    Config->LowLevelConfig->simple_2d.AmountComplete = 1;     // SouthPole is complete
}
void                  slam_simple_2d_update_distance( t_ttc_slam_config* Config, TTC_SLAM_VOLATILE t_u16 NodeIndexA, TTC_SLAM_VOLATILE t_u16 NodeIndexB, TTC_SLAM_VOLATILE ttm_number RawValue ) {
    Assert_SLAM_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    if ( NodeIndexA == NodeIndexB ) { return; } // distance within same node makes no sense
    if ( RawValue == 0 )            { return; } // not storing non-distances (will corrupt data structures as 0 means no measure!)
    if ( NodeIndexB  < NodeIndexA ) {           // swap indexes to ensure NodeIndexA < NodeIndexB
        t_u8 Temp = NodeIndexA;
        NodeIndexA = NodeIndexB;
        NodeIndexB = Temp;
    }

    if ( RawValue < 0 )
    { RawValue = -RawValue; } // distances are always positive

    t_sc2d_node_distance** DistancePtr  = NULL; // !=NULL: where to store new distance
    t_sc2d_node_distance** DistancePtr2 = NULL; // !=NULL: second distance pointer (just read)

    t_ttc_slam_node* SLAM_NodeIndexB = NULL;

    if ( NodeIndexB == 2 ) { // distance north- to southpole (NodeIndexA < NodeIndexB)

        // obtain pointer to northpole node (will only store NS-distance in northpole to save RAM)
        SLAM_NodeIndexB = slam_simple_2d_get_node( Config, simple_2d_index_northpole );
        t_slam_simple_2d_node* Simple2D_NodeIndexB = ( t_slam_simple_2d_node* ) & ( SLAM_NodeIndexB->Driver.simple_2d );

        // will store distance from Northpole to Southpole
        DistancePtr  = &( Simple2D_NodeIndexB->Distance2South );
        DistancePtr2 = &( Simple2D_NodeIndexB->Distance2North );
    }
    else {                 // distance from west or east node to one pole
        SLAM_NodeIndexB = slam_simple_2d_get_node( Config, NodeIndexB );
        t_slam_simple_2d_node* Simple2D_NodeIndexB = ( t_slam_simple_2d_node* ) & ( SLAM_NodeIndexB->Driver.simple_2d );


        switch ( NodeIndexA ) {
            case simple_2d_index_northpole:  // storing distance west/east node to north pole
                DistancePtr  = &( Simple2D_NodeIndexB->Distance2North );
                DistancePtr2 = &( Simple2D_NodeIndexB->Distance2South );
                break;
            case simple_2d_index_southpole:  // storing distance west/east node to south pole
                DistancePtr  = &( Simple2D_NodeIndexB->Distance2South );
                DistancePtr2 = &( Simple2D_NodeIndexB->Distance2North );
                break;
            default: break;
        }
    }

    if ( 0 ) { // DEPRECATED
        if ( !Config->Flags.Completed_Distances ) { // initial mapping phase: accept only certain distances

            if ( NodeIndexA == simple_2d_index_southpole ) { // accepting distance to south- or northpole only

                // obtain pointers to indexed node
                SLAM_NodeIndexB = slam_simple_2d_get_node( Config, NodeIndexB );
                t_slam_simple_2d_node* Simple2D_NodeIndexB = ( t_slam_simple_2d_node* ) & ( SLAM_NodeIndexB->Driver.simple_2d );

                if ( NodeIndexA == simple_2d_index_southpole ) {          // got distance to Southpole
                    if ( NodeIndexB == simple_2d_index_northpole ) {      // got length of SN-axis
                        if ( SLAM_NodeIndexB->Position.X == TTC_SLAM_UNDEFINED_VALUE ) { // NorthPole is complete now
                            SLAM_NodeIndexB->Flags.DistancesComplete = TRUE;
                            // ToDo: distances are complete only if N+S distance have been averaged!
                        }
                        ttc_slam_update_coordinates( Config->LogicalIndex, NodeIndexB, 0, RawValue, 0 );
                    }
                    else {                                     // got distance from SouthPole to West- or EastNode
                        if ( ( !Simple2D_NodeIndexB->Distance2North ) && // node had no Distance2North before
                                Simple2D_NodeIndexB->Distance2South ) {   // and it has Distance2South: node completed
                            SLAM_NodeIndexB->Flags.DistancesComplete = TRUE;
                        }
                    }

                    // always storing distance to Southpole (#1) in Distance2North
                    DistancePtr  = &( Simple2D_NodeIndexB->Distance2North );
                    DistancePtr2 = &( Simple2D_NodeIndexB->Distance2South );
                }
            }
            else {
                if ( NodeIndexA == simple_2d_index_northpole ) {      // got distance from West- or EastNode to NorthPole
                    // obtain pointers to indexed node
                    SLAM_NodeIndexB = slam_simple_2d_get_node( Config, NodeIndexB );
                    t_slam_simple_2d_node* Simple2D_NodeIndexB = ( t_slam_simple_2d_node* ) & ( SLAM_NodeIndexB->Driver.simple_2d );

                    if ( ( !Simple2D_NodeIndexB->Distance2South ) && // node had no distance to NorthPole before
                            Simple2D_NodeIndexB->Distance2North ) {  // and it has distance to SouthPole: node completed
                        SLAM_NodeIndexB->Flags.DistancesComplete = TRUE;
                        // ToDo: distances are complete only if N+S distance have been averaged!
                    }
                    // always storing distance to Northpole (#2) in Distance2South
                    DistancePtr  = &( Simple2D_NodeIndexB->Distance2South );
                    DistancePtr2 = &( Simple2D_NodeIndexB->Distance2North );
                }
            }
        }
        else {                                             // update phase: adjust node positions
            TODO( "Implement Update Phase!" )
        }
    } // DEPRECATED

    if ( DistancePtr ) { // we found a place where to store reported distance
        Assert_SLAM_EXTRA_Writable( DistancePtr, ttc_assert_origin_auto );     // check implementation above!
        Assert_SLAM_EXTRA_Writable( SLAM_NodeIndexB, ttc_assert_origin_auto );  // must have been loaded with pointer to single node (check implementation above!)
        t_u8 AmountRawMeasures = Config->Init.AmountRawMeasures;

        // Each distance and its raw measures is stored in a memory block obtained from a memory pool
        t_sc2d_node_distance* NewDistance;
        if ( !*DistancePtr ) { // Node has no distance block up to now: obtain memory block from pool
            t_ttc_heap_block_from_pool* NewBlock = ttc_heap_pool_block_get_try( & Config->Pool_Distances );
            Assert_SLAM_Writable( NewBlock, ttc_assert_origin_auto );  // Ran out of memory blocks, Check implementation!
            NewDistance = ( t_sc2d_node_distance* ) ttc_heap_pool_to_list_item( NewBlock );
            NewDistance->Average = 0;
            if ( AmountRawMeasures > 1 ) { // reset all raw values to 0
                ttc_memory_set( NewDistance->RawValues,
                                0,
                                sizeof( NewDistance->RawValues[0] * AmountRawMeasures )
                              );
            }
            // store pointer to distance block in node
            *DistancePtr = NewDistance;
        }
        else {                 // reuse previously obtained memory block
            Assert_SLAM_EXTRA_Writable( DistancePtr, ttc_assert_origin_auto );
            NewDistance = *DistancePtr;
        }

        // copy distance data into memory block
        //X NewDistance->NodeA = ( t_u8 ) 0xff & NodeIndexA;
        //X NewDistance->NodeB = ( t_u8 ) 0xff & NodeIndexB;

        BOOL AverageUpdated = FALSE;
        if ( AmountRawMeasures > 1 ) { // shift raw values and store newest one in RawValues[0]
            for ( t_u8 Index = AmountRawMeasures - 1; Index > 0; ) {
                NewDistance->RawValues[Index] = NewDistance->RawValues[Index - 1];
                Index--;
            }
            NewDistance->RawValues[0] = RawValue;

            if ( NewDistance->RawValues[AmountRawMeasures - 1] ) { // all raw-values set: calculate average of raw values
                // ToDo: move average calculation into ttc_math
                ttm_number Average1 = RawValue;
                for ( t_u8 Index = AmountRawMeasures - 1; Index > 1; ) { // first run: calculate average of all raw values
                    Average1 += NewDistance->RawValues[Index];
                }
                Average1 /= AmountRawMeasures;
                ttm_number LowCriteria  = Average1 / 2; // raw values below will not be accepted
                ttm_number HighCriteria = Average1 * 2; // raw values above will not be accepted

                ttm_number Average2;
                t_u8 AmountAccepted = 0;
                for ( t_u8 Index = AmountRawMeasures - 1; Index > 0; ) { // second run: calculate average of all raw values which meet the criteria
                    ttm_number ThisRawValue = NewDistance->RawValues[Index];
                    if ( ( ThisRawValue >= LowCriteria ) && ( ThisRawValue <= HighCriteria ) ) { // raw value is acceptable
                        if ( AmountAccepted == 0 )
                        { Average2 = ThisRawValue; }
                        else
                        { Average2 += ThisRawValue; }
                        AmountAccepted++;
                    }
                }
                Average2 /= AmountAccepted;

                NewDistance->Average = Average2;
                AverageUpdated = TRUE;
            }
        }
        else {                         // averaging disabled: just copy value
            NewDistance->Average = RawValue;
            AverageUpdated = TRUE;
        }
        if ( AverageUpdated ) { // averaged distance value updated: check if this node is complete now
            if ( !SLAM_NodeIndexB->Flags.DistancesComplete ) { // node was not complete before update: maybe it is now
                BOOL NodeCompleted = FALSE;

                if ( NodeIndexB == simple_2d_index_northpole ) { // north pole is complete with only one distance
                    NodeCompleted = TRUE;
                }
                else {                                    // check if west/east node is complete
                    if ( *DistancePtr2 ) { // node has two distances
                        if ( ( *DistancePtr2 )->Average != 0 ) { // node has two valid average distance values: it is complete now
                            NodeCompleted = TRUE;
                            SLAM_NodeIndexB->Flags.DistancesComplete = TRUE;
                        }
                    }
                }

                if ( NodeCompleted ) { // this node now has complete distance measures: check if all nodes are complete
                    Config->LowLevelConfig->simple_2d.AmountComplete++;
                    if ( Config->LowLevelConfig->simple_2d.AmountComplete == Config->Init.Amount_Nodes ) { // initial phase finished: update flag
                        // slam_simple_2d_calculate_mapping() now may calculate mapping
                        Config->Flags.Completed_Distances = 1;
                    }
                }
            }

            // slam_simple_2d_calculate_mapping() will have to recalculate mapping
            Config->Flags.Completed_Mapping = 0;
        }
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
