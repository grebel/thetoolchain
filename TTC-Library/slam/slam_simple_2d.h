#ifndef SLAM_SIMPLE_2D_H
#define SLAM_SIMPLE_2D_H

/** { slam_simple_2d.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_slam.h providing
 *  2 dimensional localization and mapping of n nodes being placed on west or
 *  east side of a south-north axis.
 *
 *  Memory usage: c * 2 * n - 2, c = sizeof(t_ttc_slam_node)
 *
 *  Complexities of supported functions:
 *    ttc_slam_update_distance()  O(1)
 *
 *
 *  This module simplifies the original SLAM problem by assuming that
 *  1) The Z-coordinate is the same for all nodes
 *     (all nodes are located on a flat horizontal plane)
 *  2) The nodes are enumerated in a special way
 *     a) nodes are numbered 1..n
 *     b) #1      is placed south of #2 and called Southpole
 *     c) #2      is placed north of #1 and called Northpole
 *     d) #i>2, i=2k+1, k=1..e are located east of axis Southpole Nortpole and called Eastnodes W1..We
 *     e) #i>2, i=2k+2, k=1..w are located west of axis Southpole Nortpole and called Westnodes E1..E1
 *  9) Foreach node #i, distance (i,i-1) and (i,i+1) are updated
 * 10) For node #1, distance (1, n/2+1) is updated (gives y-coordinate of Nortpole)
 *
 *
 *  Simplified Location Map
 *                           o #2 Northpole
 *                           !
 *                           !   o #5
 *                           !
 *                    #6 o   !     o #3
 *                           !
 *          Westnodes        !        Eastnodes
 *                           !
 *                    #4 o   !    o #7
 *                           !
 *                #8 o       !
 *                           !
 *                           o #1 Southpole
 *
 *  Once the distance between North- and Southpole is reported, the SN-axis is used
 *  to calculate the coordinates of all other nodes. The special enumeration
 *  guarantees that all nodes are placed on correct side of SN-axis:
 *  #3,5,7,... on right (east) side
 *  #4,6,8,... on left (west) side
 *
 *  Advantages of Implementation
 *  a) Low memory usage (<1kB RAM usage for <= 5 nodes)
 *  b) Low computational requirements (tested on STM32L100 @24MHz 4kB RAM)
 *  c) Constraints are applicable to many practical scenarios:
 *     - Only one node is configured and labeled as Northpole and one as Southpole
 *     - Nodes placed east of vector S->N are configured and labeled as E1, E2, ...
 *     - Nodes placed west of vector S->N are configured and labeled as W1, W2, ...
 *
 *  Initial Mapping Process
 *
 *  The initial mapping process takes place right after initialization to gather egnough
 *  data to calculate an initial location for all nodes.
 *  1) The Southpole is always located at (0,0,0).
 *  2) The first node to be located is the Nortpole at (0, Distance(Northpole, Southpole), 0).
 *  3) All other nodes #i are located by lateration using the triangle (Northpole, Southpole, Node #i)
 *
 *
 *  Implementation of Initial Mapping
 *
 *  a) Memory for 2 * n distance measures is allocated as a memory pool.
 *     Whenever egnough distance measures are available for a localization, the
 *     distance measure memory blocks are returned to the pool.
 *
 *  b) While locations are still unknown, node #i will store only certain distances:
 *     i = 3,5,7, ... (Eastnode):
 *     i = 4,6,8, ... (Westnode):
 *        Distance1 = Distance(i, Southpole)
 *        Distance2 = Distance(i, Northpole)
 *     i = 2 (Northpole):
 *     i = 1 (Southpole):
 *        No distances stored
 *
 *
 *   Graphical Representation of Stored Distance Measures during Initial Mapping
 *                           o #2 Northpole = (0,Ny)
 *                        _//!\\_
 *                      _/ / ! \ \_
 *                    _/  /  !  \  \_
 *              #4   o   /   !   \   o  #5
 *                   \  /    !    \  /
 *    West Nodes      \/     !     \/       East Nodes
 *                    /\     !     /\.
 *                   /  \    !    /  \.
 *              #6   o   \   !   /   o  #3
 *                    \_  \  !  /  _/
 *                      \_ \ ! / _/
 *                        \_\!/_/
 *                          \!/
 *                           o #1 Southpole = (0,0)
 *
 *
 *  Created from template device_architecture.h revision 28 at 20160607 10:19:25 UTC
 *
 *  Note: See ttc_slam.h for description of simple_2d independent SLAM implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_SLAM_DRIVER_AVAILABLE
#define EXTENSION_SLAM_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_slam_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "slam_simple_2d.c"
//
#include "../ttc_slam_types.h" // will include slam_simple_2d_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_slam_interface.h
//
// Some driver functions may be optional to implement.
// Check ttc_slam_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_slam_foo
//
#define ttc_driver_slam_configuration_check(Config)                     slam_simple_2d_configuration_check(Config)
#define ttc_driver_slam_deinit(Config)                                  slam_simple_2d_deinit(Config)
#define ttc_driver_slam_init(Config)                                    slam_simple_2d_init(Config)
#define ttc_driver_slam_load_defaults(Config)                           slam_simple_2d_load_defaults(Config)
#define ttc_driver_slam_prepare()                                       slam_simple_2d_prepare()
#define ttc_driver_slam_reset(Config)                                   slam_simple_2d_reset(Config)
#define ttc_driver_slam_update_distance(Config, NodeA, Node2, Distance) slam_simple_2d_update_distance(Config, NodeA, Node2, Distance)
#define ttc_driver_slam_get_node(Config, NodeIndex)                     slam_simple_2d_get_node(Config, NodeIndex)
#define ttc_driver_slam_calculate_mapping(Config)                       slam_simple_2d_calculate_mapping(Config)
#define ttc_driver_slam_get_distance                                    slam_simple_2d_get_distance
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

// undefine feature functions that we do not implement
#undef ttc_driver_slam_update_coordinates
#undef ttc_driver_slam_calculate_distance

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_slam.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_slam.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

/** Calculates new localization mapping of all nodes (if possible)
 *
 * @param LogicalIndex  (t_u8)   logical index of SLAM instance. Each logical device <n> is defined via TTC_SLAM<n>* constants in compile_options.h and extensions.active/makefile
 * @return              (BOOL)   ==TRUE: new mapping has been calculated; ==FALSE: not egnough data available: update more distances!
 */
BOOL slam_simple_2d_calculate_mapping( t_ttc_slam_config* Config );

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_slam_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_slam_features.
 *
 * @param Config        Configuration of slam device
 * @return              Fields of *Config have been adjusted if necessary
 */
void slam_simple_2d_configuration_check( t_ttc_slam_config* Config );


/** shutdown single SLAM unit device
 * @param Config        Configuration of slam device
 * @return              == 0: SLAM has been shutdown successfully; != 0: error-code
 */
e_ttc_slam_errorcode slam_simple_2d_deinit( t_ttc_slam_config* Config );


/** Returns pointer to current data of indexed node
 *
 * Nodes are stored in an array. The size of each entry is only known by current low-level driver.
 * Calculation of memory address of indexed node is done in low-level driver.
 *
 * @param LogicalIndex  (t_u8)               logical index of SLAM instance. Each logical device <n> is defined via TTC_SLAM<n>* constants in compile_options.h and extensions.active/makefile
 * @param NodeIndex (t_u16)
 * @return          (t_ttc_slam_node*)
 * @param Config   =
 */
t_ttc_slam_node* slam_simple_2d_get_node( t_ttc_slam_config* Config, t_u16 NodeIndex );


/** initializes single SLAM unit for operation
 * @param Config        Configuration of slam device
 * @return              == 0: SLAM has been initialized successfully; != 0: error-code
 */
e_ttc_slam_errorcode slam_simple_2d_init( t_ttc_slam_config* Config );


/** loads configuration of indexed SLAM unit with default values
 * @param Config        Configuration of slam device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_slam_errorcode slam_simple_2d_load_defaults( t_ttc_slam_config* Config );


/** Prepares slam Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void slam_simple_2d_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of slam device
 */
void slam_simple_2d_reset( t_ttc_slam_config* Config );


/** Report a new distance measure between two nodes A and B.
 *
 * Distances can be updated at any time. Low-level driver will take care of filtering.
 * The amount of distance measures that is required to compute all locations may depend
 * on current low-level driver.
 *
 * @param LogicalIndex  (t_u8)               logical index of SLAM instance. Each logical device <n> is defined via TTC_SLAM<n>* constants in compile_options.h and extensions.active/makefile
 * @param Config        (t_ttc_slam_config*) initialized slam configuration as returned by ttc_slam_get_configuration()
 * @param NodeIndexA    (t_u16)              index of node #1 (1..ttc_slam_get_configuration()->Init.Amount_Nodes)
 * @param NodeIndexB    (t_u16)              index of node #2 (1..ttc_slam_get_configuration()->Init.Amount_Nodes)
 * @param RawValue      (ttm_number)         Raw measured distance value.
 */
void slam_simple_2d_update_distance( t_ttc_slam_config* Config, t_u16 NodeIndexA, t_u16 NodeIndexB, ttm_number RawValue );


/** return stored distance between two nodes
 *
 * Note: Current low-level driver may not store all possible n! distances among n nodes.
 * Note: Even if no distance is stored, it might be calculatable from node positions.
 *
 * @param Config       (t_ttc_slam_config*)  Configuration of slam device as returned by ttc_slam_get_configuration()
 * @param LogicalIndex (t_u8)                Logical index of slam instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SLAM<n> lines in extensions.active/makefile
 * @param NodeIndexA   (t_u16)               index of node #1 (1..ttc_slam_get_configuration()->Init.Amount_Nodes)
 * @param NodeIndexB   (t_u16)               index of node #2 (1..ttc_slam_get_configuration()->Init.Amount_Nodes)
 * @return             (ttm_number)          ==TTC_SLAM_UNDEFINED_VALUE: no distance stored; previously stored value otherwise
 */
ttm_number slam_simple_2d_get_distance( t_ttc_slam_config* Config, t_u16 NodeIndexA, t_u16 NodeIndexB );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //SLAM_SIMPLE_2D_H
