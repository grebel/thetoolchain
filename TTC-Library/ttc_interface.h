#ifndef TTC_INTERFACE_H
#define TTC_INTERFACE_H
/** { ttc_interface.h *******************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for INTERFACE devices.
 *  The functions defined in this file provide a hardware independent interface 
 *  for your application.
 *                                          
 *  The basic usage scenario for devices:
 *  1) check:       Assert_INTERFACE(tc_interface_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_interface_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_interface_init(LogicalIndex);
 *  4) use:         ttc_interface_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level interface and application.
 *
 *  Created from template ttc_device.h revision 30 at 20150310 09:22:28 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

#ifndef EXTENSION_ttc_interface
#error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_interface.sh
#endif

//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_interface.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_interface_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level interface only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * interface devices on all supported architectures.
 * Check interface/interface_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your 
 * activate_project.sh file inside your project folder.
 *
 */
 
/** Prepares interface Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_interface_prepare();
void _driver_interface_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_interface_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_INTERFACE_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_interface_config* ttc_interface_get_configuration(t_u8 LogicalIndex);

/** fills out given Config with maximum valid values for indexed INTERFACE
 * @param LogicalIndex    index of device to init (1..ttc_INTERFACE_get_max_LogicalIndex())
 * @return                pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_interface_config* ttc_interface_get_features(t_u8 LogicalIndex);

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_INTERFACE_get_max_LogicalIndex())
 * @return                == 0: interface device has been initialized successfully; != 0: error-code
 */
e_ttc_interface_errorcode ttc_interface_init(t_u8 LogicalIndex);

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_INTERFACE_get_max_LogicalIndex())
 */
void ttc_interface_deinit(t_u8 LogicalIndex);

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_interface_deinit() if device has been initialized.  
 *
 * @param LogicalIndex  logical index of interface device (1..ttc_interface_get_max_index() )
 * @return                == 0: default values have been loaded successfully for indexed interface device; != 0: error-code
 */
e_ttc_interface_errorcode  ttc_interface_load_defaults(t_u8 LogicalIndex);

/** maps from logical to physical device index
 *
 * High-level interfaces (ttc_interface_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_INTERFACEn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be 
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any 
 * order.
 *
 * @param LogicalIndex  logical index of interface device (1..ttc_interface_get_max_index() )
 * @return              physical index of interface device (0 = first physical interface device, ...)
 */
t_u8 ttc_interface_logical_2_physical_index(t_u8 LogicalIndex);

/** maps from physical to logical device index
 *
 * This is a reverse function to ttc_interface_logical_2_physical_index()
 *
 * @param  PhysicalIndex  physical index of interface device (0 = first physical interface device, ...)
 * @return                logical index of interface device (1..ttc_interface_get_max_index() )
 */
t_u8 ttc_interface_physical_2_logical_index(t_u8 PhysicalIndex);

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_INTERFACE_get_max_LogicalIndex())
 */
void ttc_interface_reset(t_u8 LogicalIndex);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_interface(t_u8 LogicalIndex)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_interface_ are passed to interfaces/ttc_interface_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl interface UPDATE
 */


/** fills out given Config with maximum valid values for indexed INTERFACE
 * @param Config  = pointer to struct t_ttc_interface_config (must have valid value for PhysicalIndex)
 * @return        pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_interface_config* _driver_interface_get_features(t_ttc_interface_config* Config);

/** shutdown single INTERFACE unit device
 * @param Config        pointer to struct t_ttc_interface_config (must have valid value for PhysicalIndex)
 * @return              == 0: INTERFACE has been shutdown successfully; != 0: error-code
 */
e_ttc_interface_errorcode _driver_interface_deinit(t_ttc_interface_config* Config);

/** initializes single INTERFACE unit for operation
 * @param Config        pointer to struct t_ttc_interface_config (must have valid value for PhysicalIndex)
 * @return              == 0: INTERFACE has been initialized successfully; != 0: error-code
 */
e_ttc_interface_errorcode _driver_interface_init(t_ttc_interface_config* Config);

/** loads configuration of indexed INTERFACE unit with default values
 * @param Config        pointer to struct t_ttc_interface_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_interface_errorcode _driver_interface_load_defaults(t_ttc_interface_config* Config);

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_interface_config (must have valid value for PhysicalIndex)
 */
void _driver_interface_reset(t_ttc_interface_config* Config);
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_INTERFACE_H
