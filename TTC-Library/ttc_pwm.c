/** { ttc_pwm.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for pwm devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Most functions of this driver are defined as macros and being implemented
 *  in interfaces/ttc_pwm_interface.c or in low-level drivers pwm/pwm_*.c.
 *
 *  See corresponding ttc_pwm.h for documentation of all high-level functions.
 *
 *  Created from template ttc_device.c revision 46 at 20180502 11:57:47 UTC
 *
 *  Authors: <AUTHOR>
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_pwm.h".
//
#include "ttc_pwm.h"
#ifdef EXTENSION_ttc_interrupt
#include "ttc_interrupt.h"
#endif
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_PWM_AMOUNT == 0
#warning No PWM devices defined, cdid you forget to activate something? - Define at least TTC_PWM1 as one from e_ttc_pwm_architecture in your makefile!
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of pwm devices.
 *
 */


// for each initialized device, a pointer to its generic and architecture specific definitions is stored
A_define( t_ttc_pwm_config*, ttc_pwm_configs, TTC_PWM_AMOUNT );

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ********************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_pwm(t_ttc_pwm_config* Config)

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * @param LogicalIndex    1.. logical index of PWM device. Each logical device <n> is defined via TTC_PWM<n>* constants in compile_options.h and extensions.active/makefile
 */
void _ttc_pwm_configuration_check( t_u8 LogicalIndex );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

t_ttc_pwm_config*   ttc_pwm_create() {

    t_u8 LogicalIndex = ttc_pwm_get_max_index();
    t_ttc_pwm_config* Config = ttc_pwm_get_configuration( LogicalIndex );

    return Config;
}
t_u8                ttc_pwm_get_max_index() {
    return TTC_PWM_AMOUNT;
}
t_ttc_pwm_config*   ttc_pwm_get_configuration( t_u8 LogicalIndex ) {
    Assert_PWM( LogicalIndex > 0, ttc_assert_origin_auto ); // logical index starts at 1
    Assert_PWM( LogicalIndex <= TTC_PWM_AMOUNT, ttc_assert_origin_auto ); // invalid index given (did you configure engnough ttc_pwm devices in your makefile?)
    t_ttc_pwm_config* Config = A( ttc_pwm_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        #ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_disable();                      // ensure that we are not interrupted while creating configuration
        #endif

        Config = A( ttc_pwm_configs, LogicalIndex - 1 ); // have to reload with disabled interrupts to avoid racing conditions
        if ( !Config ) {
            Config = A( ttc_pwm_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_pwm_config ) );
            Config->Flags.Initialized = 0;             // make sure that deinit() is not called during load_defaults()
            ttc_pwm_load_defaults( LogicalIndex );
        }

        #ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_enable();
        #endif
    }

    Assert_PWM_EXTRA_Writable( Config, ttc_assert_origin_auto ); // memory corrupted?
    return Config;
}
void                ttc_pwm_deinit( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration

    t_ttc_pwm_config* Config = ttc_pwm_get_configuration( LogicalIndex );

    if ( Config->Flags.Initialized ) {
        e_ttc_pwm_errorcode Result = _driver_pwm_deinit( Config );
        if ( Result == ec_pwm_OK )
        { Config->Flags.Initialized = 0; }
    }
    ttc_task_critical_end(); // other tasks now may access this driver
}
e_ttc_pwm_errorcode ttc_pwm_init( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration
    #if (TTC_ASSERT_PWM_EXTRA == 1)
    t_u8 InitialCriticalLevel = ttc_interrupt_critical_level();
    #endif
    t_ttc_pwm_config* Config = ttc_pwm_get_configuration( LogicalIndex );

    // check configuration to meet all limits of current architecture
    _ttc_pwm_configuration_check( LogicalIndex );

    Config->LastError = _driver_pwm_init( Config );
    if ( ! Config->LastError ) // == 0
    { Config->Flags.Initialized = 1; }

    Assert_PWM( Config->Architecture != 0, ttc_assert_origin_auto ); // Low-Level driver must set this value!
    Assert_PWM_EXTRA( InitialCriticalLevel == ttc_interrupt_critical_level(), ttc_assert_origin_auto ); // amount of  ttc_task_critical_end() calls does not match. Check driver implementation!
    ttc_task_critical_end(); // other tasks now may access this driver

    return Config->LastError;
}
e_ttc_pwm_errorcode ttc_pwm_load_defaults( t_u8 LogicalIndex ) {
    t_ttc_pwm_config* Config = ttc_pwm_get_configuration( LogicalIndex );

    u_ttc_pwm_architecture* LowLevelConfig = NULL;
    if ( Config->Flags.Initialized ) {
        ttc_pwm_deinit( LogicalIndex );
        LowLevelConfig = Config->LowLevelConfig; // rescue pointer to dynamic allocated memory
    }

    ttc_memory_set( Config, 0, sizeof( t_ttc_pwm_config ) );

    // restore pointer to dynamic allocated memory
    Config->LowLevelConfig = LowLevelConfig;

    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    switch ( LogicalIndex ) { // physical index of pwm device from static configuration
            #ifdef TTC_PWM1
        case  1: Config->PhysicalIndex = TTC_PWM1; break;
            #endif
            #ifdef TTC_PWM2
        case  2: Config->PhysicalIndex = TTC_PWM2; break;
            #endif
            #ifdef TTC_PWM3
        case  3: Config->PhysicalIndex = TTC_PWM3; break;
            #endif
            #ifdef TTC_PWM4
        case  4: Config->PhysicalIndex = TTC_PWM4; break;
            #endif
            #ifdef TTC_PWM5
        case  5: Config->PhysicalIndex = TTC_PWM5; break;
            #endif
            #ifdef TTC_PWM6
        case  6: Config->PhysicalIndex = TTC_PWM6; break;
            #endif
            #ifdef TTC_PWM7
        case  7: Config->PhysicalIndex = TTC_PWM7; break;
            #endif
            #ifdef TTC_PWM8
        case  8: Config->PhysicalIndex = TTC_PWM8; break;
            #endif
            #ifdef TTC_PWM9
        case  9: Config->PhysicalIndex = TTC_PWM9; break;
            #endif
            #ifdef TTC_PWM10
        case 10: Config->PhysicalIndex = TTC_PWM10; break;
            #endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }

    //Insert additional generic default values here ...

    // Let low-level driver initialize remaining fields
    Config->LastError = _driver_pwm_load_defaults( Config );

    // Check mandatory configuration items
    Assert_PWM_EXTRA( ( Config->Architecture > ta_pwm_None ) && ( Config->Architecture < ta_pwm_unknown ), ttc_assert_origin_auto ); // architecture not set properly! Low-Level driver must set this field!

    return Config->LastError;
}
void                ttc_pwm_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)

    A_reset( ttc_pwm_configs, TTC_PWM_AMOUNT ); // safe arrays are placed in data section and must be initialized manually

    if ( 0 ) // optional: register this device for a sysclock change update
    { ttc_sysclock_register_for_update( ttc_pwm_sysclock_changed ); }

    _driver_pwm_prepare();
}
void                ttc_pwm_reset( t_u8 LogicalIndex ) {
    t_ttc_pwm_config* Config = ttc_pwm_get_configuration( LogicalIndex );

    _driver_pwm_reset( Config );
}
void                ttc_pwm_sysclock_changed() {

    // deinit + reinit all initialized PWM devices
    for ( t_u8 LogicalIndex = 1; LogicalIndex < ttc_pwm_get_max_index(); LogicalIndex++ ) {
        t_ttc_pwm_config* Config = ttc_pwm_get_configuration( LogicalIndex );
        if ( Config->Flags.Initialized ) {
            ttc_pwm_deinit( LogicalIndex );
            ttc_pwm_init( LogicalIndex );
        }
    }
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_pwm(t_u8 LogicalIndex) {  }

void _ttc_pwm_configuration_check( t_u8 LogicalIndex ) {
    t_ttc_pwm_config* Config = ttc_pwm_get_configuration( LogicalIndex );

    // perform some generic plausibility checks
    const t_ttc_pwm_features* Features = Features;

    /** Example plausibility check

    if (Config->BitRate > Features->MaxBitRate)   Config->BitRate = Features->MaxBitRate;
    if (Config->BitRate < Features->MinBitRate)   Config->BitRate = Features->MinBitRate;
    */
    // add more architecture independent checks...
    Assert_PWM( LogicalIndex == Config->LogicalIndex, ttc_assert_origin_auto ); // configuration must store corresponding logical index!

    // let low-level driver check this configuration too
    _driver_pwm_configuration_check( Config );
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
