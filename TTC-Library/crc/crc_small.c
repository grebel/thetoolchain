/** crc_small.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_crc.c.
 *
 *  cyclic redundancy checksum implementations optimized for smallest ram and flash usage
 *
 *  Created from template device_architecture.c revision 39 at 20180323 10:28:15 UTC
 *
 *  Note: See ttc_crc.h for description of high-level crc implementation.
 *  Note: See crc_small.h for description of small crc implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "crc_small.h".
 */

#include "crc_small.h"
#include "../ttc_heap.h"              // dynamic memory allocation (e.g. malloc() )
#include "../ttc_memory.h"            // basic memory checks
#include "../ttc_task.h"              // stack checking (provides TTC_TASK_RETURN() macro)
#include "crc_common.h"          // generic functions shared by low-level drivers of all architectures 
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_crc_features crc_small_Features = {
    /** example features
          .MinBitRate       = 4800,
          .MaxBitRate       = 230400,
        */
    // set more feature values...

    .unused = 0
};

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

e_ttc_crc_errorcode crc_small_load_defaults( t_ttc_crc_config* Config ) {
    Assert_CRC_EXTRA_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = E_ttc_crc_architecture_small;         // set type of architecture
    Config->Features     = &crc_small_Features;  // assign feature set

    if ( Config->LowLevelConfig == NULL ) { // allocate memory for spi specific configuration
        { Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_ttc_crc_architecture ) ); }

        // reset all flags
        ttc_memory_set( &( Config->Flags ), 0, sizeof( Config->Flags ) );

        // set individual feature flags
        //Config->Flags.Foo = 1;
        // add more flags...


    }
    TTC_TASK_RETURN( 0 ); // will perform stack overflow check and return given value
}
void                crc_small_configuration_check( t_ttc_crc_config* Config ) {
    Assert_CRC_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

}
e_ttc_crc_errorcode crc_small_deinit( t_ttc_crc_config* Config ) {
    Assert_CRC_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
e_ttc_crc_errorcode crc_small_init( t_ttc_crc_config* Config ) {
    Assert_CRC_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
void                crc_small_prepare() {

}
void                crc_small_reset( t_ttc_crc_config* Config ) {
    Assert_CRC_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
     *
     * Private functions are declared in this c-file. They can be called from outside
     * via an extern declaration (or by ignoring the corresponding compiler warning).
     * Though it is possible to call them from outside, it is not intended to do so!
     * Private functions may be changed, renamed or deleted without notification by
     * the maintainer of this file.
     */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
