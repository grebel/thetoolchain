#ifndef CRC_FAST_H
#define CRC_FAST_H

/** { crc_fast.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_crc.h providing
 *  function prototypes being required by ttc_crc.h
 *
 *  cyclic redundancy checksum implementations optimized for fastest execution without hardware acceleration
 *
 *  Created from template device_architecture.h revision 32 at 20180409 12:16:07 UTC
 *
 *  Note: See ttc_crc.h for description of fast independent CRC implementation.
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of crc_fast (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_CRC_DRIVER_AVAILABLE
#define EXTENSION_CRC_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_crc_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "crc_fast.c"
//
#include "../ttc_crc_types.h" // will include crc_fast_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_crc_interface.h
//
// Some driver functions may be optional to implement.
// Check ttc_crc_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_crc_foo
//
#define ttc_driver_crc_configuration_check crc_fast_configuration_check
#define ttc_driver_crc_crc16_calculate_0x1021 crc_fast_crc16_calculate_0x1021
#define ttc_driver_crc_crc7_calculate crc_fast_crc7_calculate
#define ttc_driver_crc_init crc_fast_init
#define ttc_driver_crc_load_defaults crc_fast_load_defaults
#define ttc_driver_crc_prepare crc_fast_prepare
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

// functions not implemented by this low-level driver (-> ttc_crc_interface.h)
#undef ttc_driver_crc_crc7_check
#undef ttc_driver_crc_crc16_check_0x1021
#undef ttc_driver_crc_crc16_check_ccitt
#undef ttc_driver_crc_crc16_calculate_ccitt


//} Macro definitions
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_crc.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_crc.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_crc_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_crc_features.
 *
 * @param Config        Configuration of crc device
 * @return              Fields of *Config have been adjusted if necessary
 */
void crc_fast_configuration_check( t_ttc_crc_config* Config );


/** Calculate 16-bit checksum for given raw data (SDCARD compliant)
 *
 * Polynomial: 0x1021
 * StartValue: 0x0000 (typical)
 *
 * This implementation is compatible to sdcards and most other applications.
 * Though, it seems to have a defect that makes it not compliant with CRC16-CCITT specification.
 * Example CRCs (StartValue=0x0000):
 * (1) 512 bytes of 0xff -> CRC16=0x7fa1 (as described in SDCARD technical specification)
 * (2) "123456789"       -> CRC16=0xe5cc (opposed to http://srecord.sourceforge.net/crc16-ccitt.html)
 *
 * @param Buffer     (t_u8*)  Buffer storing raw data to calculate checksum from
 * @param Amount     (t_u32)  amount of valid bytes in Buffer[] (except crc value)
 * @param StartValue (t_u16)  initial CRC16 value (=0 to calculate new crc16; return value from previous call when continuing calculation)
 * @return           (t_u16)  calculated CRC16 value of Buffer[0..Amount-1] based on given start value.
 */
t_u16 crc_fast_crc16_calculate_0x1021( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue );


/** Calculate 16-bit checksum for given raw data for polynomial 0x1021 (not SDCARD compliant)
 *
 * Polynomial: 0x1021
 * StartValue: 0xffff (typical)
 *
 * This implementation is compatible to sdcards and most other applications.
 * Though, it seems to have a defect that makes it not compliant with CRC16-CCITT specification.
 * Example CRCs (StartValue=0xffff):
 *
 * @param Buffer     (t_u8*)  Buffer storing raw data to calculate checksum from
 * @param Amount     (t_u32)  amount of valid bytes in Buffer[] (except crc value)
 * @param StartValue (t_u16)  initial CRC16 value (=0 to calculate new crc16; return value from previous call when continuing calculation)
 * @return           (t_u16)  calculated CRC16 value of Buffer[0..Amount-1] based on given start value.
 */
t_u16 crc_fast_crc16_calculate_ccitt( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue );

/** Calculate 7-bit checksum for given raw data
 *
 * Generic use
 *     // calculate new 7-bit checksum
 *     t_u8 CRC_SDCard = ttc_crc_crc7_calculate(Buffer, Amount, 0);
 *
 * SDCARD use
 *     This checksum can be used to calculate sdcard command sequences.
 *     For sdcard usage, a special pre- and postprocessing is required:
 *     // calculate new checksum for sdcard commands
 *     t_u8 CRC_SDCard = (ttc_crc_crc7_calculate(Buffer, Amount, 0) << 1) | 1;
 *
 *     // continue calculation for additional data
 *     CRC_SDCard = (ttc_crc_crc7_calculate(Buffer2, Amount2, CRC_SDCard >> 1) << 1) | 1;
 *
 * @param Buffer     (t_u8*) Data buffer to send or check
 * @param Amount     (t_u16) amount of valid bytes in Buffer[] (except crc byte)
 * @param StartValue (t_u8)  initial CRC7 value (=0 to calculate new crc7; return value from previous call when continuing calculation)
 * @return           (t_u8)  calculated 7-bit crc value of Buffer[0..Amount-1] (for SDCard usage: t_u8 CRC = ( ttc_crc_crc7_calculate(...) << 1 ) | 1; )
 */
t_u8 crc_fast_crc7_calculate( const t_u8* Buffer, t_u16 Amount, t_u8 StartValue );


/** initializes single CRC unit for operation
 * @param Config        Configuration of crc device
 * @return              == 0: CRC has been initialized successfully; != 0: error-code
 */
e_ttc_crc_errorcode crc_fast_init( t_ttc_crc_config* Config );


/** loads configuration of indexed CRC unit with default values
 * @param Config        Configuration of crc device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_crc_errorcode crc_fast_load_defaults( t_ttc_crc_config* Config );


/** Prepares crc Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void crc_fast_prepare();

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //CRC_FAST_H
