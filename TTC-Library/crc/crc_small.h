#ifndef CRC_SMALL_H
#define CRC_SMALL_H

/** { crc_small.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_crc.h providing
 *  function prototypes being required by ttc_crc.h
 *
 *  cyclic redundancy checksum implementations optimized for smallest ram and flash usage
 *
 *  Created from template device_architecture.h revision 32 at 20180323 10:28:15 UTC
 *
 *  Note: See ttc_crc.h for description of small independent CRC implementation.
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of crc_small (Do not delete this line!)
 *
 * Cyclic Redundancy Checksum (CRC) implementations with small code size and RAM usage.
 * This driver is practically empty. It's only purpose is to activate implementations inside
 * ttc_crc_interface.c. Check this file for real implementations!
 *
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_CRC_DRIVER_AVAILABLE
#define EXTENSION_CRC_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_crc_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "crc_small.c"
//
#include "../ttc_crc_types.h" // will include crc_small_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_crc_interface.h
//
// Some driver functions may be optional to implement.
// Check ttc_crc_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_crc_foo
//
#define ttc_driver_crc_configuration_check crc_small_configuration_check
#define ttc_driver_crc_deinit              crc_small_deinit
#define ttc_driver_crc_init                crc_small_init
#define ttc_driver_crc_load_defaults       crc_small_load_defaults
#define ttc_driver_crc_prepare             crc_small_prepare
#define ttc_driver_crc_reset               crc_small_reset
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

// functions not implemented by this low-level driver (-> ttc_crc_interface.h)
#undef ttc_driver_crc_crc7_check
#undef ttc_driver_crc_crc16_check_0x1021
#undef ttc_driver_crc_crc16_check_ccitt
#undef ttc_driver_crc_crc16_calculate_ccitt
#undef ttc_driver_crc_crc16_calculate_0x1021
#undef ttc_driver_crc_crc7_calculate

//} Macro definitions
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_crc.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_crc.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_crc_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_crc_features.
 *
 * @param Config        Configuration of crc device
 * @return              Fields of *Config have been adjusted if necessary
 */
void crc_small_configuration_check( t_ttc_crc_config* Config );


/** shutdown single CRC unit device
 * @param Config        Configuration of crc device
 * @return              == 0: CRC has been shutdown successfully; != 0: error-code
 */
e_ttc_crc_errorcode crc_small_deinit( t_ttc_crc_config* Config );


/** initializes single CRC unit for operation
 * @param Config        Configuration of crc device
 * @return              == 0: CRC has been initialized successfully; != 0: error-code
 */
e_ttc_crc_errorcode crc_small_init( t_ttc_crc_config* Config );


/** loads configuration of indexed CRC unit with default values
 * @param Config        Configuration of crc device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_crc_errorcode crc_small_load_defaults( t_ttc_crc_config* Config );


/** Prepares crc Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void crc_small_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of crc device
 */
void crc_small_reset( t_ttc_crc_config* Config );


//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //CRC_SMALL_H
