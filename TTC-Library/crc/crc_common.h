#ifndef crc_common_h
#define crc_common_h

/** crc_common.h ****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to crc low-level drivers of all architectures.
 *  The functions provided here are common in two ways:
 *  1) They can be called from high-level driver ttc_crc.c and from current low-level driver crc_*.c.
 *  2) All common_crc_*() functions can call low-level driver functions as _driver_crc_*().
 *
 *  Created from template ttc-lib/templates/device_common.h revision 15 at 20180323 10:28:15 UTC
 *
 *  Authors: Gregor Rebel
 *
 *  Description of crc_common (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "crc_small.c"
 */

#include "../ttc_basic.h"
#include "../ttc_crc_types.h" // will include crc_common_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_crc_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_crc_foo
 */

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions
/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_crc.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_crc.h for details.
 *
 * You normally should not need to add functions here.
 * Global feature functions must be added in ttc_crc.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl crc UPDATE".
 */

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //crc_common_h
