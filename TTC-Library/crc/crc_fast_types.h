#ifndef CRC_FAST_TYPES_H
#define CRC_FAST_TYPES_H

/** { crc_fast.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_crc.h providing
 *  Structures, Enums and Defines being required by ttc_crc_types.h
 *
 *  cyclic redundancy checksum implementations optimized for fastest execution without hardware acceleration
 *
 *  Created from template device_architecture_types.h revision 27 at 20180409 12:16:07 UTC
 *
 *  Note: See ttc_crc.h for description of high-level crc implementation!
 *  Note: See crc_fast.h for description of fast specific crc implementation!
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_CRC1   // device not defined in makefile
    #define TTC_CRC1    E_ttc_crc_architecture_fast   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#ifndef TTC_CRC_TYPES_H
    #  error Do not include crc_common_types.h directly. Include ttc_crc_types.h instead!
#endif

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_crc_types.h *************************

typedef struct {  // fast specific configuration data of single crc device
    t_u8 unused;         // remove me (C structs may not be empty)!
    // add architecture specific configuration fields here..
} __attribute__( ( __packed__ ) ) t_crc_fast_config;

// t_ttc_crc_architecture is required by ttc_crc_types.h
#define t_ttc_crc_architecture t_crc_fast_config

//} Structures/ Enums

#endif //CRC_FAST_TYPES_H
