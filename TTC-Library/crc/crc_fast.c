/** crc_fast.c ***************************************{
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_crc.c.
 *
 *  cyclic redundancy checksum implementations optimized for fastest execution without hardware acceleration
 *    
 *  Created from template device_architecture.c revision 39 at 20180409 12:16:07 UTC
 *
 *  Note: See ttc_crc.h for description of high-level crc implementation.
 *  Note: See crc_fast.h for description of fast crc implementation.
 * 
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "crc_fast.h".
 */

#include "crc_fast.h"
#include "../ttc_heap.h"              // dynamic memory allocation (e.g. malloc() )
#include "../ttc_memory.h"            // basic memory checks
#include "../ttc_task.h"              // stack checking (provides TTC_TASK_RETURN() macro)
#include "crc_common.h"          // generic functions shared by low-level drivers of all architectures 
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_crc_features crc_fast_Features = {
  /** example features
    .MinBitRate       = 4800,  
    .MaxBitRate       = 230400,    
  */
  // set more feature values...
  
  .unused = 0
};

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc(); 
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

e_ttc_crc_errorcode  crc_fast_load_defaults(t_ttc_crc_config* Config) {
    Assert_CRC_EXTRA_Writable(Config, ttc_assert_origin_auto); // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = E_ttc_crc_architecture_fast;         // set type of architecture
    Config->Features     = &crc_fast_Features;  // assign feature set

    if (0) { // enable for memory mapped devices (everything that has a base register)
      switch (Config->PhysicalIndex) { // load address of peripheral base register
      //case 0: Config->BaseRegister = & register_fast_CRC1; break;
      //case 1: Config->BaseRegister = & register_fast_CRC2; break;
      default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid physical index: check your makefile and high-level driver implementation!
      }
    }
    
    if (Config->LowLevelConfig == NULL) { // allocate memory for spi specific configuration
      { Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof(t_ttc_crc_architecture) ); }

    // reset all flags
    ttc_memory_set( &(Config->Flags), 0, sizeof(Config->Flags) );

    // set individual feature flags    
    //Config->Flags.Foo = 1;
    // add more flags...

    }

    TTC_TASK_RETURN( 0 ); // will perform stack overflow check and return given value
}
void                 crc_fast_configuration_check(t_ttc_crc_config* Config) {
    Assert_CRC_EXTRA_Writable(Config, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function crc_fast_configuration_check() is empty.
    

}
t_u16                crc_fast_crc16_calculate_0x1021(const t_u8* Buffer, t_u32 Amount, t_u16 StartValue) {
    Assert_CRC_EXTRA_Writable(Buffer, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

    VOLATILE_CRC t_u16 CRC16 = StartValue;

    // table based CRC-16 SDCARD for Polynom = 0x1021
    // adapted from http://automationwiki.com/index.php/CRC-16-CCITT
    // Note: This implementation calculates correct CRC16==0x7fa1 for 512 bytes of 0xff as required by sdcard specification.
    // Note: This implementation calculates incorrect CRC16!=0xe5cc for "123456789" as required
    static const t_u16 sccc_TableCRC_CCITT[256] = { // table for sdcard compatible CRC16-CCITT with polynom 0x1021

                                                    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5,
                                                    0x60c6, 0x70e7, 0x8108, 0x9129, 0xa14a, 0xb16b,
                                                    0xc18c, 0xd1ad, 0xe1ce, 0xf1ef, 0x1231, 0x0210,
                                                    0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
                                                    0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c,
                                                    0xf3ff, 0xe3de, 0x2462, 0x3443, 0x0420, 0x1401,
                                                    0x64e6, 0x74c7, 0x44a4, 0x5485, 0xa56a, 0xb54b,
                                                    0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
                                                    0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6,
                                                    0x5695, 0x46b4, 0xb75b, 0xa77a, 0x9719, 0x8738,
                                                    0xf7df, 0xe7fe, 0xd79d, 0xc7bc, 0x48c4, 0x58e5,
                                                    0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
                                                    0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969,
                                                    0xa90a, 0xb92b, 0x5af5, 0x4ad4, 0x7ab7, 0x6a96,
                                                    0x1a71, 0x0a50, 0x3a33, 0x2a12, 0xdbfd, 0xcbdc,
                                                    0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
                                                    0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03,
                                                    0x0c60, 0x1c41, 0xedae, 0xfd8f, 0xcdec, 0xddcd,
                                                    0xad2a, 0xbd0b, 0x8d68, 0x9d49, 0x7e97, 0x6eb6,
                                                    0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
                                                    0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a,
                                                    0x9f59, 0x8f78, 0x9188, 0x81a9, 0xb1ca, 0xa1eb,
                                                    0xd10c, 0xc12d, 0xf14e, 0xe16f, 0x1080, 0x00a1,
                                                    0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
                                                    0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c,
                                                    0xe37f, 0xf35e, 0x02b1, 0x1290, 0x22f3, 0x32d2,
                                                    0x4235, 0x5214, 0x6277, 0x7256, 0xb5ea, 0xa5cb,
                                                    0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
                                                    0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447,
                                                    0x5424, 0x4405, 0xa7db, 0xb7fa, 0x8799, 0x97b8,
                                                    0xe75f, 0xf77e, 0xc71d, 0xd73c, 0x26d3, 0x36f2,
                                                    0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
                                                    0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9,
                                                    0xb98a, 0xa9ab, 0x5844, 0x4865, 0x7806, 0x6827,
                                                    0x18c0, 0x08e1, 0x3882, 0x28a3, 0xcb7d, 0xdb5c,
                                                    0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
                                                    0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0,
                                                    0x2ab3, 0x3a92, 0xfd2e, 0xed0f, 0xdd6c, 0xcd4d,
                                                    0xbdaa, 0xad8b, 0x9de8, 0x8dc9, 0x7c26, 0x6c07,
                                                    0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
                                                    0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba,
                                                    0x8fd9, 0x9ff8, 0x6e17, 0x7e36, 0x4e55, 0x5e74,
                                                    0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
                                                  };

    while ( Amount-- ) {
        t_u8 Byte = *Buffer++;
        t_u8 temp = Byte ^ ( CRC16 >> 8 );
        CRC16 = sccc_TableCRC_CCITT[temp] ^ ( CRC16 << 8 );
    }
    TTC_TASK_RETURN ( 0 );  // will perform stack overflow check and return given value
}
t_u16                crc_fast_crc16_calculate_ccitt(const t_u8* Buffer, t_u32 Amount, t_u16 StartValue) {
    Assert_CRC_EXTRA_Writable(Buffer, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function crc_fast_crc16_calculate_ccitt() is empty.
    
    TTC_TASK_RETURN ( 0 );  // will perform stack overflow check and return given value
}
t_u8                 crc_fast_crc7_calculate(const t_u8* Buffer, t_u16 Amount, t_u8 StartValue) {
    Assert_CRC_EXTRA_Writable(Buffer, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function crc_fast_crc7_calculate() is empty.
    
    TTC_TASK_RETURN ( 0 );  // will perform stack overflow check and return given value
}
e_ttc_crc_errorcode  crc_fast_init(t_ttc_crc_config* Config) {
    Assert_CRC_EXTRA_Writable(Config, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function crc_fast_init() is empty.
    
    TTC_TASK_RETURN ( 0 );  // will perform stack overflow check and return given value
}
e_ttc_crc_errorcode  crc_fast_load_defaults(t_ttc_crc_config* Config) {
    Assert_CRC_EXTRA_Writable(Config, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function crc_fast_load_defaults() is empty.
    
    TTC_TASK_RETURN ( 0 );  // will perform stack overflow check and return given value
}
void                 crc_fast_prepare() {


#warning Function crc_fast_prepare() is empty.
    

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by 
 * the maintainer of this file.
 */
 
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
