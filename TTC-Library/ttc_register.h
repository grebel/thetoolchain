#ifndef TTC_REGISTER_H
#define TTC_REGISTER_H
/** { ttc_register.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for REGISTER devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_REGISTER(tc_register_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_register_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_register_init(LogicalIndex);
 *  4) use:         ttc_register_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Created from template ttc_device.h revision 25 at 20140223 10:54:39 UTC
 *
 *  Authors: Gregor Rebel, Greg Knoll, Patrick von Poblotzki
 *
 */
/** Description of ttc_register (Do not delete this line!)
 *
 *  ttc_register defines memory mapped io registers as C structs with named bitfields.
 *  These named bitfields allow to configure functional units inside current microcontroller
 *  exactly as described in official datasheets.
 *
 *  These structures are extremly handy when it comes to debugging via gdb.
 *  You can view any memory location through the eye of a struct:
 *  p *( (register_stm32w1xx_register_usart_t*) 0x40013800 )
 *
 *  which gives the same resulat as
 *  p register_stm32f1xx_USART1
 *
 *  Note: Use of these structures in your code can be problematic
 *        if peripheral registers require special sized memory accesses (8-/ 16-/ 32-Bits).
 *
 * Example:
 *   + Force 16-Bit memory access
 *     // Note: USART_TypeDef defined by StdPeripheral-library
 *     t_register_stm32f1xx_usart_cr1 CR1 = register_stm32f1xx_USART1.CR1; // 16 bit transfer into local variable
 *     CR1.PCE  = 1;
 *     CR1.TCIE = 0;
 *     register_stm32f1xx_USART1.CR1 = CR1; // 16 bit transfer into register
 *
 *   + alternative
 *     register_stm32f1xx_USART1.CR1.PCE = 1;
 *
 *   Despite of this compiler dependend problem, the structures defined here
 *   allow an easy, self checking, way to read and write registers in a safe manner.
 *
 *   You will not have to do any error prone bit-shifting like this:
 *   USART1.CR1 |= 1 << 10;             // PCE  = 1
 *   USART1.CR1 &= (0xffff - (1 << 6)); // TCIE = 0
}*/

#ifndef EXTENSION_ttc_register
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_register.sh
#endif

//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_register_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

/** 32bit bitfield access as described by zakharov (http://communities.mentor.com/community/cs/archives/arm-gnu/msg03675.html)
 *
 *  Some architectures like ARM-NONE-EABI does not allow non 32-bit accesses to peripheral memory regions.
 *  The direct use of bitfields for peripherals on these architectures will cause busfaults.
 *  The ttc_register_set32() and ttc_register_get32() macros below allow to use bitfields under these circumstances.
 *
 *  Examples
 *
 *  Direct Bitfield Access:
 *    register_stm32w1xx_GPIO_DB_WAKE_IRQ.IRQCSEL.bSEL = 23;
 *    t_u8 Value = register_stm32w1xx_GPIO_DB_WAKE_IRQ.IRQCSEL.bSEL;
 *
 *  Using macros:
 *    ttc_register_set32(register_stm32w1xx_GPIO_DB_WAKE_IRQ.IRQCSEL, bSEL, 23);
 *    t_u8 Value; ttc_register_get32(register_stm32w1xx_GPIO_DB_WAKE_IRQ.IRQCSEL, bSEL, Value);
 */

/** sets one field in register
 *
 * @param Register  pointer to 32 bit register being described by a struct type
 * @param Field     bitfield element in struct to set
 * @param Value     value to write into struct element
 */
#define ttc_register_set32(Register, Field, Value)                   do {                                                              t_u32 Temp = *( (t_u32*) (Register));                       ( (typeof(*Register)*) &Temp )->Field = (Value);              *( (t_u32*) (Register)) = Temp;                        } while(0)

/** sets two fields in register (all fields changed in a single 32 bit write)
 *
 * @param Register  pointer to 32 bit register being described by a struct type
 * @param Field1    bitfield element in struct to set
 * @param Value1    value to write into struct element
 * @param Field2    bitfield element in struct to set
 * @param Value2    value to write into struct element
 */
#define ttc_register_set32_2(Register, Field1, Value1, Field2, Value2)         do {                                                                        t_u32 Temp = *( (t_u32*) (Register));                                  ( (typeof(*Register)*) &Temp )->Field1 = (Value1);                     ( (typeof(*Register)*) &Temp )->Field2 = (Value2);                     *( (t_u32*) (Register)) = Temp;                                   } while(0)

/** reads one field from register
 *
 * @param Register  variable storing register content (no pointer)
 * @param Field     bitfield element in struct to read
 * @param Value     field value from struct is stored in this variable
 */
#define ttc_register_get32(Register, Field, Value)                   do {                                                           t_u32 Temp = *( (t_u32*) &(Register));                       Value = ( (typeof(Register)*) &Temp )->Field;              } while(0)

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level register only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * register devices on all supported architectures.
 * Check register/register_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares register Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_register_prepare();

/** returns configuration of all registers (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_REGISTER_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_register_config* ttc_register_get_configuration();
#define ttc_register_get_configuration() _driver_register_get_configuration()

/** resets given register to default value
 *
 * @param Register    pointer to one entry of struct t_ttc_register_config as returned by ttc_register_get_configuration()
 * @return            ==0: register has been reset successfully
 */
e_ttc_register_errorcode ttc_register_reset( void* Register );
#define ttc_register_reset(Register) _driver_register_reset(Register)
e_ttc_register_errorcode _driver_register_reset( void* Register );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_register(t_u8 LogicalIndex)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_heap_ are passed to interfaces/ttc_heap_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl heap UPDATE
 */

/** Prepares register driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void _driver_register_prepare();

/** fills out given Config with maximum valid values for indexed REGISTER
 * @param Config        = pointer to struct t_ttc_register_config
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
t_ttc_register_architecture* _driver_register_get_configuration();


//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_REGISTER_H
