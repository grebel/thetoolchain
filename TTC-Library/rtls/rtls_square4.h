#ifndef RTLS_SIMPLE4_H
#define RTLS_SIMPLE4_H

/** { rtls_square4.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_rtls.h providing
 *  function prototypes being required by ttc_rtls.h
 *
 *  simple coordinate system up for 2..4 anchor nodes requiring only one range measure.
 *
 *  Created from template device_architecture.h revision 29 at 20180328 16:29:02 UTC
 *
 *  Note: See ttc_rtls.h for description of square4 independent RTLS implementation.
 *
 *  Authors: <AUTHOR>
 *
 *
 *  Description of rtls_square4 (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_RTLS_DRIVER_AVAILABLE
#define EXTENSION_RTLS_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_rtls_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "rtls_square4.c"
//
#include "../ttc_rtls_types.h" // will include rtls_square4_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_rtls_interface.h
//
// Some driver functions may be optional to implement.
// Check ttc_rtls_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_rtls_foo
//
#define ttc_driver_rtls_anchor_buildup_start rtls_square4_anchor_buildup_start
#define ttc_driver_rtls_anchor_statemachine rtls_square4_anchor_statemachine
#define ttc_driver_rtls_configuration_check rtls_square4_configuration_check
#define ttc_driver_rtls_deinit rtls_square4_deinit
#define ttc_driver_rtls_init rtls_square4_init
#define ttc_driver_rtls_load_defaults rtls_square4_load_defaults
#define ttc_driver_rtls_mobile_statemachine rtls_square4_mobile_statemachine
#define ttc_driver_rtls_prepare rtls_square4_prepare
#define ttc_driver_rtls_reset rtls_square4_reset
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_rtls.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_rtls.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Start building up coordinate system.
 *
 * Will send range requests to all neighbouring anchor nodes and exchange data to build up a coordinate system.
 * Low-level driver sets Config->Flags.IsCoordinateSystemReady = 1 once coordinates of all anchor nodes are complete.
 * Amount of participating anchor nodes depends on current rtls low-level driver.
 *
 * Note: Requires Config->Flags.Bits.IsAnchor=1 before ttc_rtls_init() call!
 *
 * @param Config       (t_ttc_rtls_config*)  Configuration of rtls device as returned by ttc_rtls_get_configuration()
 * @param LogicalIndex (t_u8)                     Logical index of rtls instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RTLS<n> lines in extensions.active/makefile
 */
void rtls_square4_anchor_buildup_start( t_ttc_rtls_config* Config );


/** Statemachine of anchor_*() functions. Call regularly to process events.
 *
 * Note: Requires Config->Flags.Bits.IsAnchor==1 before ttc_rtls_init() call!
 *
 * @param Config       (t_ttc_rtls_config*)  Configuration of rtls device as returned by ttc_rtls_get_configuration()
 */
void rtls_square4_anchor_statemachine( t_ttc_rtls_config* Config );


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_rtls_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_rtls_features.
 *
 * @param Config        Configuration of rtls device
 * @return              Fields of *Config have been adjusted if necessary
 */
void rtls_square4_configuration_check( t_ttc_rtls_config* Config );


/** shutdown single RTLS unit device
 * @param Config        Configuration of rtls device
 * @return              == 0: RTLS has been shutdown successfully; != 0: error-code
 */
e_ttc_rtls_errorcode rtls_square4_deinit( t_ttc_rtls_config* Config );


/** initializes single RTLS unit for operation
 * @param Config        Configuration of rtls device
 * @return              == 0: RTLS has been initialized successfully; != 0: error-code
 */
e_ttc_rtls_errorcode rtls_square4_init( t_ttc_rtls_config* Config );


/** loads configuration of indexed RTLS unit with default values
 * @param Config        Configuration of rtls device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_rtls_errorcode rtls_square4_load_defaults( t_ttc_rtls_config* Config );


/** Statemachine of mobile_*() functions. Call regularly to process events.
 *
 * @param Config       (t_ttc_rtls_config*)  Configuration of rtls device as returned by ttc_rtls_get_configuration()
 */
void rtls_square4_mobile_statemachine( t_ttc_rtls_config* Config );


/** Prepares rtls Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void rtls_square4_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of rtls device
 */
void rtls_square4_reset( t_ttc_rtls_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //RTLS_SIMPLE4_H
