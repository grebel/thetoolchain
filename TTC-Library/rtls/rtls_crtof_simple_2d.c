/** rtls_crtof_simple_2d.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_rtls.c.
 *
 *  synchronous location and mapping radio protocol for a simple two dimensional anchor setup using the cascaded replies time of flight ranging protocol
 *
 *  Created from template device_architecture.c revision 34 at 20161208 10:46:38 UTC
 *
 *  Note: See ttc_rtls.h for description of high-level rtls implementation.
 *  Note: See rtls_crtof_simple_2d.h for description of crtof_simple_2d rtls implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "rtls_crtof_simple_2d.h".
 */

#include "../slam/slam_simple_2d_types.h"
#include "rtls_crtof_simple_2d.h"
#include "rtls_common.h"          // generic functions shared by low-level drivers of all architectures
#include "../ttc_memory.h"             // basic memory checks
#include "../ttc_task.h"               // stack checking (provides TTC_TASK_RETURN() macro)
#include "../ttc_slam.h"               // mathematic model to build up coordinate system and locate mobile nodes
#include "../ttc_radio.h"              // radio transceiver with ranging capabilities
#include "../ttc_packet.h"             // 802.15.4a compliant packet format
#include "../ttc_systick.h"            // precise delays
#include "../ttc_states.h"             // universal statemachine
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_rtls_features rtls_crtof_simple_2d_Features = {

    .MinimumAnchorNodes = 2,
    .MaximumAnchorNodes = 255,
    .AnchorNodes1D = 1,
    .AnchorNodes2D = 1,
    .AnchorNodes3D = 0
};

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables

#warning ToDo: Protocol rtls_crtof_simple_2d is in unfinished state and will not work!

/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

/** delivers distance report from any node to south or north pole
 *
 * Note: This function is intended to be called from rtls_common_statemachine_report_distances() only!
 *
 * @param Config    Configuration of rtls device as returned by ttc_rtls_get_configuration()
 * @param Index     =2 * NodeIndex. LSB is used to select between distance to south and nort pole
 *                  Index&1==0: copy distance to south pole; ==1: copy distance to north pole
 * @param Distance  points to storage to be filled by getDistance()
 */
void _scs2_copy_pole_distance( t_ttc_rtls_config* Config,  t_u16 Index, t_rtls_common_distance* Distance );

/** delivers latest distance report stored in Config->LowLevelConfig->crtof_simple_2d.LatestDistance
 *
 * Note: This function is to be called from rtls_common_statemachine_report_distances() only!
 *
 * @param Config    Configuration of rtls device as returned by ttc_rtls_get_configuration()
 * @param Index     =0: copy distance to south pole; =1: copy distance to north pole
 * @param Distance  points to storage to be filled by getDistance()
 */
void _scs2_copy_latest_distance( t_ttc_rtls_config* Config,  t_u16 Index, t_rtls_common_distance* Distance );

/** update mathematical model with newest distance from range measure between an anchor node and one of the poles
 *
 * @param Config        (t_ttc_rtls_config*)  Configuration of rtls device as returned by ttc_rtls_get_configuration()
 * @param NodeIndexA    (t_u16)                    protocol address of first node (1=south pole, 2=north pole, >2: west or east node)
 * @param NodeIndexB    (t_u16)                    protocol address of second node (1=south pole, 2=north pole, >2: west or east node)
 *
 */
void _scs2_update_distance_to_pole( t_ttc_rtls_config* Config,  t_u16 NodeIndexA, t_u16 NodeIndexB, t_base Distance_cm );

// State functions of southpole anchor node ----------------------------------------------------------------------------------------

/** statemachine state function for state init for a southpole anchor node
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_anchor_southpole_state_init( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );

/** statemachine state function that measures ranges to all other n-1 nodes and their range to north pole
 *
 * Note: This function is used on a southpole anchor node only.
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_anchor_southpole_state_measure_all_ranges( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );

/** statemachine state function that requests range to northpole node from all west- and east nodes
 *
 * Note: This function is used on a southpole anchor node only.
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_anchor_southpole_state_request_all_ranges_to_northpole( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );

/** statemachine state function that reports all gathered ranges via broadcast to all other anchor nodes
 *
 * Note: This function is used on a southpole anchor node only.
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_anchor_southpole_state_report_ranges( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );

/** statemachine state function that provides service to other anchor and mobile nodes after building up coordinate system
 *
 * Note: This function is used on a southpole anchor node only.
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_anchor_southpole_state_ready( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );

/** statemachine state function that requests a distance measure to northpole from a west- or east-node
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_anchor_southpole_state_request_single_range_to_northpole( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );


// State functions of all non-southpole anchor nodes --------------------------------------------------------------------------

/** statemachine state function for state that initializes a northpole, west or east anchor node
 *
 * Note: This state function is run by a northpole, a west or an east anchor node only!
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_anchor_other_state_init( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );

/** statemachine state function for state that waits for requests from southpole to be included in anchor node setup
 *
 * Note: This state function is run by a northpole, a west or an east anchor node only!
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_anchor_other_state_waiting( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );

/** statemachine state function for state that all current range data from southpole (if available)
 *
 * Note: This state function is run by a northpole, a west or an east anchor node only!
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_anchor_other_state_request_all_ranges( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );

/** statemachine state function for state that measures range from current node to northpole anchor node and sends it to southpole anchor node
 *
 * Note: This state function is run by a west or an east anchor node only!
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_anchor_other_state_measure_range_to_northpole( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );

/** statemachine state function for state that that provides service to mobile nodes after building up coordinate system
 *
 * Note: This state function is run by a northpole, a west or an east anchor node only!
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_anchor_other_state_ready( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );


// Universal state functions for all types of anchor nodes --------------------------------------------------------------------------

/** statemachine state function that computes coordinates of all anchor nodes from measured ranges.
 *
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_anchor_substate_build_coordinate_system( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );

/** statemachine state function that will prepare and send an answer for a ranging request
 *
 * A RangingReply message basically only contains data required to calculate the distance between two nodes.
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_anchor_substate_reply_ranging( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );

/** statemachine state function that will prepare and send an answer for a localization request
 *
 * A LocalizationReply message contains all data of a RangingReply plus the 3D coordinates of local node (if known).
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_anchor_substate_reply_localization( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );

/** statemachine state function for state measuring a distance to a single remote node
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _scs2_node_substate_measure_single_range( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );

/** monitor function being called for every state transition in anchor node statemachine
 *
 * Note: This function is only called if TTC_ASSERT_STATES == 1
 * Note: Rename all my_ keywords with your own prefix to avoid name collisions!
 * Usage: Start a debug session and place a breakpoint inside:
 *        gdb> b ttc_states_monitor
 *        gdb> c
 *
 * @param MyStatemachine       (volatile t_ttc_states_config*)  configuration of initialized ttc_states instance
 * @param State_Current        (volatile TTC_STATES_TYPE)       state being transitioned to
 * @param State_Previous       (volatile TTC_STATES_TYPE)       state being transitioned from
 * @param Transition           (e_ttc_states_transition)        cause of transition
 */
void _scs2_anchor_states_monitor( volatile t_ttc_states_config* MyStatemachine, t_ttc_rtls_config* MyStateData, volatile e_ttc_rtls_state State_Current, volatile e_ttc_rtls_state State_Previous, e_ttc_states_transition Transition );

/** monitor function being called for every state transition in mobile node statemachine
 *
 * Note: This function is only called if TTC_ASSERT_STATES == 1
 * Note: Rename all my_ keywords with your own prefix to avoid name collisions!
 * Usage: Start a debug session and place a breakpoint inside:
 *        gdb> b ttc_states_monitor
 *        gdb> c
 *
 * @param MyStatemachine       (volatile t_ttc_states_config*)  configuration of initialized ttc_states instance
 * @param State_Current        (volatile TTC_STATES_TYPE)       state being transitioned to
 * @param State_Previous       (volatile TTC_STATES_TYPE)       state being transitioned from
 * @param Transition           (e_ttc_states_transition)        cause of transition
 */
void _scs2_mobile_states_monitor( volatile t_ttc_states_config* MyStatemachine, t_ttc_rtls_config* MyStateData, volatile e_ttc_rtls_state State_Current, volatile e_ttc_rtls_state State_Previous, e_ttc_states_transition Transition );

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

/** Declare each individual state of our statemachine as static data.
 *  As everything is constant, it will be put in FLASH and does not occupy any RAM.
 *
 * Attributes of each state:
 * + StateFunction
 *   Pointer to a function that will be called while this state is active.
 *
 * + AmountReachable
 *   Amount of other states to which may be switched from this state.
 *   Any other state switch will cause an assert.
 *
 * + ReachableStates[]
 *   Array of state enums to which this state may switch to.
 *   The array must be terminated by a zero (0) for extra safety.
 *
 */

// Southpole Anchor Node States
const t_ttc_states_state _scs2_anchor_southpole_State_Init = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_southpole_state_init,

    // Unique number identifying this state
    .Number = state_s2d_southpole_Init,

    // amount of reachable states listed below
    .AmountReachable = 1,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_s2d_southpole_MeasureAllRanges,
        0
    }
};
const t_ttc_states_state _scs2_anchor_southpole_State_MeasureAllRanges = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_southpole_state_measure_all_ranges,

    // Unique number identifying this state
    .Number = state_s2d_southpole_MeasureAllRanges,

    // amount of reachable states listed below
    .AmountReachable = 2,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_s2d_southpole_MeasureSingleRange,
        state_s2d_southpole_RequestAllRanges2Northpole,
        0
    }
};
const t_ttc_states_state _scs2_anchor_southpole_State_MeasureSingleRange = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_node_substate_measure_single_range,

    // Unique number identifying this state
    .Number = state_s2d_southpole_MeasureSingleRange,

    // amount of reachable states listed below
    .AmountReachable = 1,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_s2d_southpole_MeasureAllRanges,
        0
    }
};
const t_ttc_states_state _scs2_anchor_southpole_State_RequestAllRanges2Northpole = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_southpole_state_request_all_ranges_to_northpole,

    // Unique number identifying this state
    .Number = state_s2d_southpole_RequestAllRanges2Northpole,

    // amount of reachable states listed below
    .AmountReachable = 2,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_s2d_southpole_RequestSingleRange2Northpole,
        state_s2d_southpole_BuildCoordinateSystem,
        0
    }
};
const t_ttc_states_state _scs2_anchor_southpole_State_RequestSingleRange2Northpole = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_southpole_state_request_single_range_to_northpole,

    // Unique number identifying this state
    .Number = state_s2d_southpole_RequestSingleRange2Northpole,

    // amount of reachable states listed below
    .AmountReachable = 1,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_s2d_southpole_RequestAllRanges2Northpole,
        0
    }
};
const t_ttc_states_state _scs2_anchor_southpole_State_BuildCoordinateSystem = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_substate_build_coordinate_system,

    // Unique number identifying this state
    .Number = state_s2d_southpole_BuildCoordinateSystem,

    // substates must be declared explicitely
    .Flags = { .IsSubstate = 1 },

    // amount of reachable states listed below
    .AmountReachable = 0,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        0
    }
};
const t_ttc_states_state _scs2_anchor_southpole_State_ReportRanges = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_southpole_state_report_ranges,

    // Unique number identifying this state
    .Number = state_s2d_southpole_ReportRanges,

    // amount of reachable states listed below
    .AmountReachable = 1,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_s2d_southpole_Ready,
        0
    }
};
const t_ttc_states_state _scs2_anchor_southpole_State_Ready = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_southpole_state_ready,

    // Unique number identifying this state
    .Number = state_s2d_southpole_Ready,

    // amount of reachable states listed below
    .AmountReachable = 3,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_s2d_southpole_ReportRanges,
        state_s2d_southpole_ReplyRanging,
        state_s2d_southpole_ReplyLocalization,
        0
    }
};
const t_ttc_states_state _scs2_anchor_southpole_State_ReplyRanging = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_substate_reply_ranging,

    // Unique number identifying this state
    .Number = state_s2d_southpole_ReplyRanging,

    // substates must be declared explicitely
    .Flags = { .IsSubstate = 1 },

    // amount of reachable states listed below
    .AmountReachable = 0,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        0
    }
};
const t_ttc_states_state _scs2_anchor_southpole_State_ReplyLocalization = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_substate_reply_localization,

    // Unique number identifying this state
    .Number = state_s2d_southpole_ReplyLocalization,

    // substates must be declared explicitely
    .Flags = { .IsSubstate = 1 },

    // amount of reachable states listed below
    .AmountReachable = 0,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        0
    }
};

const t_ttc_states_state* scs2_anchor_southpole_AllStates[] = { // list of all states in southpole anchor node statemachine
    &_scs2_anchor_southpole_State_Init,
    &_scs2_anchor_southpole_State_MeasureAllRanges,
    &_scs2_anchor_southpole_State_MeasureSingleRange,
    &_scs2_anchor_southpole_State_RequestAllRanges2Northpole,
    &_scs2_anchor_southpole_State_RequestSingleRange2Northpole,
    &_scs2_anchor_southpole_State_BuildCoordinateSystem,
    &_scs2_anchor_southpole_State_ReportRanges,
    &_scs2_anchor_southpole_State_Ready,
    &_scs2_anchor_southpole_State_ReplyRanging,
    &_scs2_anchor_southpole_State_ReplyLocalization,
    NULL // array must be zero terminated
};

// Non-Southpole Anchor Node States (Northpole, West- and East-Nodes)
const t_ttc_states_state _scs2_anchor_other_State_Init = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_other_state_init,

    // Unique number identifying this state
    .Number = state_s2d_anchor_Init,

    // amount of reachable states listed below
    .AmountReachable = 1,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_s2d_anchor_Waiting,
        0
    }
};
const t_ttc_states_state _scs2_anchor_other_State_Waiting = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_other_state_waiting,

    // Unique number identifying this state
    .Number = state_s2d_anchor_Waiting,

    // amount of reachable states listed below
    .AmountReachable = 4,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_s2d_anchor_RequestAllRanges,
        state_s2d_anchor_MeasureRange2Northpole,
        state_s2d_anchor_BuildCoordinateSystem,
        state_s2d_anchor_ReplyRanging,
        0
    }
};
const t_ttc_states_state _scs2_anchor_other_State_RequestAllRanges = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_other_state_request_all_ranges,

    // Unique number identifying this state
    .Number = state_s2d_anchor_RequestAllRanges,

    // amount of reachable states listed below
    .AmountReachable = 1,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_s2d_anchor_Waiting,
        0
    }
};
const t_ttc_states_state _scs2_anchor_other_State_BuildCoordinateSystem = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_substate_build_coordinate_system,

    // Unique number identifying this state
    .Number = state_s2d_anchor_BuildCoordinateSystem,

    // substates must be declared explicitely
    .Flags = { .IsSubstate = 1 },

    // amount of reachable states listed below
    .AmountReachable = 0,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        0
    }
};
const t_ttc_states_state _scs2_anchor_other_State_Ready = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_other_state_ready,

    // Unique number identifying this state
    .Number = state_s2d_anchor_Ready,

    // amount of reachable states listed below
    .AmountReachable = 2,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_s2d_anchor_ReplyRanging,
        state_s2d_anchor_ReplyLocalization,
        0
    }
};
const t_ttc_states_state _scs2_anchor_other_State_ReplyRanging = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_substate_reply_ranging,

    // Unique number identifying this state
    .Number = state_s2d_anchor_ReplyRanging,

    // substates must be declared explicitely
    .Flags = { .IsSubstate = 1 },

    // amount of reachable states listed below
    .AmountReachable = 0,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        0
    }
};
const t_ttc_states_state _scs2_anchor_other_State_ReplyLocalization = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_substate_reply_localization,

    // Unique number identifying this state
    .Number = state_s2d_anchor_ReplyLocalization,

    // substates must be declared explicitely
    .Flags = { .IsSubstate = 1 },

    // amount of reachable states listed below
    .AmountReachable = 0,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        0
    }
};
const t_ttc_states_state _scs2_anchor_other_State_MeasureRange2Northpole = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_anchor_other_state_measure_range_to_northpole,

    // Unique number identifying this state
    .Number = state_s2d_anchor_MeasureRange2Northpole,

    // amount of reachable states listed below
    .AmountReachable = 2,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_s2d_anchor_Waiting,
        state_s2d_anchor_MeasureSingleRange,
        0
    }
};
const t_ttc_states_state _scs2_anchor_other_State_MeasureSingleRange = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
    & _scs2_node_substate_measure_single_range,

    // Unique number identifying this state
    .Number = state_s2d_anchor_MeasureSingleRange,

    // substates must be declared explicitely
    .Flags = { .IsSubstate = 1 },

    // amount of reachable states listed below
    .AmountReachable = 0,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        0
    }
};

const t_ttc_states_state* scs2_anchor_other_AllStates[] = { // list of all states in non-southpole anchor node statemachine
    &_scs2_anchor_southpole_State_Init,
    &_scs2_anchor_other_State_Waiting,
    &_scs2_anchor_other_State_RequestAllRanges,
    &_scs2_anchor_other_State_BuildCoordinateSystem,
    &_scs2_anchor_other_State_Ready,
    &_scs2_anchor_other_State_ReplyRanging,
    &_scs2_anchor_other_State_ReplyLocalization,
    &_scs2_anchor_other_State_MeasureRange2Northpole,
    &_scs2_anchor_other_State_MeasureSingleRange,
    NULL // array must be zero terminated
};

/** Function Definitions ********************************************{
 */


e_ttc_rtls_errorcode rtls_crtof_simple_2d_load_defaults( t_ttc_rtls_config* Config ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = ta_rtls_crtof_simple_2d;         // set type of architecture
    Config->Features     = &rtls_crtof_simple_2d_Features;  // assign feature set

    // set individual feature flags
    ttc_memory_set( &( Config->Flags ), 0, sizeof( Config->Flags ) );

    // allocate memory for rtof_simple_2d specific configuration
    Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_rtls_crtof_simple_2d_config ) );

    Config->LowLevelConfig->crtof_simple_2d.Delay_RangingRequestRetry_us = 1000000;

    TTC_TASK_RETURN( ec_rtls_OK ); // will perform stack overflow check + return value
}
void                 rtls_crtof_simple_2d_configuration_check( t_ttc_rtls_config* volatile Config ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    Assert_RTLS( Config->ConfigSLAM->Init.Amount_Nodes > 1, ttc_assert_origin_auto ); // ttc_slam has no nodes. Call ttc_slam_init() before!
    Assert_RTLS( Config->LocalID16 > 0, ttc_assert_origin_auto ); // Node address must be >= 1. Check your configuration!

    Assert_RTLS( Config->Init.AnchorIDs[0].Address16 == simple_2d_index_southpole, ttc_assert_origin_auto ); // first entry of AnchorIDs[] must store south pole!
    Assert_RTLS( Config->Init.AnchorIDs[1].Address16 == simple_2d_index_northpole, ttc_assert_origin_auto ); // second entry of AnchorIDs[] must store north pole!

    if ( 1 ) { // ensure that AnchorIDs[] is sorted by Address16 ( runtime = O(n) )
        #if (TTC_ASSERT_RTLS == 1)
        volatile t_u16 Index = Config->ConfigSLAM->Init.Amount_Nodes;
        volatile t_u16 Address16a = 0;
        volatile t_u16 Address16b = 0;
        while ( ( Index > 0 ) && ( Address16a == 0 ) ) { // find first non zero address
            Index--;
            Address16a = Config->Init.AnchorIDs[Index].Address16;
        }
        while ( ( Index > 0 ) && ( Address16b == 0 ) ) { // find second non zero address
            Index--;
            Address16b = Config->Init.AnchorIDs[Index].Address16;
        }
        Assert_RTLS( Address16b != 0, ttc_assert_origin_auto ); // given array stores not even two non-zero Address16 entries. Check your configuration!
        while ( Index > 0 ) {
            Assert_RTLS( Address16a > Address16b, ttc_assert_origin_auto ); // non-zero entries of AnchorIDs[] must be sorted by their Address16 field. Sort array and come back!

            Index--;
            Address16a = Address16b;
            Address16b = Config->Init.AnchorIDs[Index].Address16;
        }
        #endif
    }
}
e_ttc_rtls_errorcode rtls_crtof_simple_2d_deinit( t_ttc_rtls_config* Config ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS_EXTRA_Writable( Config->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!

    // reset low-level configuration
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config->LowLevelConfig->crtof_simple_2d );
    ttc_memory_set( Config_Simple2D, 0, sizeof( *Config_Simple2D ) );

    return ( e_ttc_rtls_errorcode ) 0;
}
e_ttc_rtls_errorcode rtls_crtof_simple_2d_init( t_ttc_rtls_config* Config ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    // obtain pointer to low-level configuration
    if ( !Config->LowLevelConfig ) {
        Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_rtls_crtof_simple_2d_config ) );

        #if TTC_RTLS_IMPLEMENT_ANCHOR == 1
        t_scs2_anchor_config* Config_Anchor = &( Config->LowLevelConfig->Anchor );

        // create new statemachine instance for anchor node protocol
        Config_Anchor->StateMachine = ttc_states_create();
        if ( Config->LocalID16 == 1 ) { // use state list of southpole anchor node
            Config_Anchor->StateMachine->Init.AllStates = scs2_anchor_southpole_AllStates;
        }
        else {                          // use state list of non-southpole anchor node
            Config_Anchor->StateMachine->Init.AllStates = scs2_anchor_other_AllStates;
        }

        Config_Anchor->StateMachine->Init.Stack_Size        = 1;
        Config_Anchor->StateMachine->Init.StateData         = Config;

        #if (TTC_ASSERT_RTLS == 1)  // enable monitoring for debug builds only
        Config_Anchor->StateMachine->Init.MonitorTransition =
            ( void ( * )( volatile void*, void*, volatile TTC_STATES_TYPE, volatile TTC_STATES_TYPE, e_ttc_states_transition ) )
            &_scs2_anchor_states_monitor;
        #endif

        ttc_states_init( Config_Anchor->StateMachine );
        #endif

    }
    #if TTC_RTLS_IMPLEMENT_ANCHOR == 1
    #  if RTLS_CRTOF_SIMPLE_2D_CACHE_DISTANCES==1

    if ( Config->LowLevelConfig->crtof_simple_2d->Anchor.NodeDistances == NULL ) {  // first call: allocate memory
        Config->LowLevelConfig->crtof_simple_2d->Anchor.NodeDistances = ttc_heap_alloc_zeroed( Config->ConfigSLAM->Amount_Nodes_Initial * sizeof( *  Config->LowLevelConfig->crtof_simple_2d->Anchor.NodeDistances ) );
    }
    #  endif
    #endif


    return ( e_ttc_rtls_errorcode ) 0;
}
void                 rtls_crtof_simple_2d_prepare() {

}
void                 rtls_crtof_simple_2d_reset( t_ttc_rtls_config* Config ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

}
void                 rtls_crtof_simple_2d_anchor_buildup_start( t_ttc_rtls_config* Config ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS_EXTRA_Writable( Config->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!

    // obtain pointer to low-level configuration
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config->LowLevelConfig->crtof_simple_2d );

    // reset statemachine for anchor node protocol to initial state
    ttc_states_reset( Config_Simple2D->Anchor.StateMachine->LogicalIndex );
}
void                 rtls_crtof_simple_2d_anchor_statemachine( t_ttc_rtls_config* Config ) {
    #if TTC_RTLS_IMPLEMENT_ANCHOR == 1
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS_EXTRA_Writable( Config->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!

    // obtain pointer to low-level configuration
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config->LowLevelConfig->crtof_simple_2d );

    // run statemachine for anchor node protocol
    ttc_states_run( Config_Simple2D->Anchor.StateMachine );
    #endif
}
void                 rtls_crtof_simple_2d_mobile_statemachine( t_ttc_rtls_config* Config ) {
    #if TTC_RTLS_IMPLEMENT_MOBILE == 1
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning ToDo: implement rtls_crtof_simple_2d_mobile_statemachine()
    switch ( Config->StateMobile ) {

        default: Assert_RTLS( 0, ttc_assert_origin_auto ); break; // unknown/ unsupported state. Check implementation!
    }
    #endif
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 *
 * Note: function prefix _scs2_ is a short acronym for _rtls_crtof_simple_2d_
 */

void _scs2_copy_pole_distance( t_ttc_rtls_config* Config,  volatile t_u16 Index, t_rtls_common_distance* Distance ) { // ToDo: remove volatile!
    Assert_RTLS_EXTRA_Writable( Config,   ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS_EXTRA_Writable( Distance, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS( Index >= simple_2d_index_northpole, ttc_assert_origin_auto ); //

    // Each node stores two distances.
    // LSB of Index is used to indicate distance to deliver:
    // Index == 2k   => Distance to south pole
    // Index == 2k+1 => Distance to north pole
    BOOL Distance2NorthPole = ( Index & 1 );
    Index /= 2;
    Distance->NodeID_A = Index;

    /* Disabled (not used atm and corrupts source folding in QtCreator)
    #if ( RTLS_CRTOF_SIMPLE_2D_CACHE_DISTANCES==1 ) // copy distance from local cache
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config->LowLevelConfig->crtof_simple_2d );
    t_scs2_node_distances* PoleDistances = &( Config_Simple2D->Anchor.NodeDistances[Index] );

    if ( Distance2NorthPole ) { // copy distance to north pole
        Distance->Distance = PoleDistances->SouthPole;
        Distance->NodeID_B = simple_2d_index_southpole;
    }
    else {                      // copy distance to south pole
        Distance->Distance = PoleDistances->NorthPole;
        Distance->NodeID_B = simple_2d_index_northpole;
    }
    #else                                             // copy distance from ttc_slam instance
    */
    if ( Distance2NorthPole )
    { Distance->NodeID_B = simple_2d_index_southpole; }
    else
    { Distance->NodeID_B = simple_2d_index_northpole; }

    Distance->Distance = ttc_slam_get_distance( Config->Init.Index_SLAM, Distance->NodeID_B, Index );
    //#endif
}
void _scs2_copy_latest_distance( t_ttc_rtls_config* Config,  t_u16 Index, t_rtls_common_distance* Distance ) {
    Assert_RTLS_EXTRA_Writable( Config,   ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS_EXTRA_Writable( Distance, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS_EXTRA_Writable( Config->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config->LowLevelConfig->crtof_simple_2d );

    // copy distance data
    Distance->Distance = Config_Simple2D->LatestDistance.Distance_cm;
    Distance->NodeID_A = Config->LocalID16;
    Distance->NodeID_B = Config_Simple2D->LatestDistance.RemoteID.Address16;
}
void _scs2_update_distance_to_pole( t_ttc_rtls_config* Config,  t_u16 NodeIndexA, t_u16 NodeIndexB, t_base Distance_cm ) {
    Assert_RTLS_EXTRA_Writable( Config,   ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    if ( NodeIndexA > NodeIndexB ) { // swap values to ensure NodeIndexA < NodeIndexB (slam_simple_2d_update_distance() would do this anyway)
        t_u16 Temp = NodeIndexA;
        NodeIndexA = NodeIndexB;
        NodeIndexB = Temp;
    }
    Assert_RTLS( NodeIndexA < 3, ttc_assert_origin_auto );  // smaller index must be north or south pole. Check implementation of caller!

    #if (RTLS_CRTOF_SIMPLE_2D_CACHE_DISTANCES==1) // store distance in local cache
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config->LowLevelConfig->crtof_simple_2d );
    Assert_RTLS_EXTRA_Writable( Config_Simple2D->Anchor.NodeDistances, ttc_assert_origin_auto ); // it seems as if NodeDistances[] was not allocated (or overwritten). Check implementation of ttc_rtls_init() and everything else! ;-)

    t_scs2_node_distances* NodeDistances = & ( Config_Simple2D->Anchor.NodeDistances[NodeIndexB] );
    if ( NodeIndexA == 1 )
    { NodeDistances->SouthPole = Distance_cm; }
    else
    { NodeDistances->NorthPole = Distance_cm; }
    #endif

    ttc_slam_update_distance( Config->Init.Index_SLAM, NodeIndexA, NodeIndexB, Distance_cm );
}

/** Statemachine State Functions
 *
 * The private functions below implement individual states of statemachines for different type of anchor nodes.
 * + SouthPole Anchor Node
 * + Non-SouthPole Anchor Node (NorthPole, West Nodes and East Nodes)
 * + Mobile (non-Anchor) Nodes
 *
 * State functions are typically only called via ttc_states_run() according to current state value.
 * State functions itself may not call other state functions or ttc_states_run().
 *
 * They may manipulate the current state by calling
 * - ttc_states_switch() to switch to another state.
 * - ttc_states_call()   to push a return state onto stack
 * - ttc_states_return()   to pull previous state from stack (return part of a substate)
 */

#if TTC_RTLS_IMPLEMENT_ANCHOR == 1

// State functions of southpole anchor node ----------------------------------------------------------------------------------------
void _scs2_anchor_southpole_state_init( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    ( void ) Transition;

    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*             Config_RTLS = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    t_scs2_anchor_config*               Config_Anchor    = &( Config_Simple2D->Anchor );

    t_ttc_packet* PacketRX;
    do {  // empty receive queue (will ignore all previously received packets and free memory blocks for new packets)
        PacketRX = ( t_ttc_packet* ) ttc_radio_packet_received_tryget( Config_RTLS->ConfigRadio->LogicalIndex );
        if ( PacketRX )
        { ttc_radio_packet_release( PacketRX ); }
    }
    while ( PacketRX );

    ( void ) Config_Simple2D;
    ( void ) Config_Anchor;

    ttc_states_switch( MyStatemachine, state_s2d_southpole_MeasureAllRanges );
}
void _scs2_anchor_southpole_state_measure_all_ranges( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*             Config_RTLS        = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config* Config_Simple2D         = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    t_scs2_anchor_config*               Config_Anchor           = &( Config_Simple2D->Anchor );
    t_statedata_s2d_measure_all_ranges* Shared_MeasureAllRanges = &( Config_Anchor->Shared.MeasureAllRanges );

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state

            // reset data set of this state
            ttc_memory_set( Shared_MeasureAllRanges, 0, sizeof( *Shared_MeasureAllRanges ) );

            // initialize with maximum value
            Shared_MeasureAllRanges->Index_RemoteAnchor = -1;

            // will have to measure range to nodes 2..n
            Shared_MeasureAllRanges->Amount_Remaining   = Config_RTLS->ConfigSLAM->Init.Amount_Nodes - 1;

            // reset mathematical SLAM model
            ttc_slam_reset( Config_RTLS->Init.Index_SLAM );

            // action will start on next function call in default case
            break;
        }
        case states_transition_Return: { // came back from a sub-state via ttc_state_return()

            // check if slam model now stores a distance from local node to remote node
            if ( ttc_slam_get_distance( Config_RTLS->Init.Index_SLAM,
                                        Config_RTLS->LocalID16,
                                        Shared_MeasureAllRanges->Index_RemoteAnchor )
                    != TTC_SLAM_UNDEFINED_VALUE
               ) { // distance measure successfull: decrease amount of ranges remaining to measure
                Assert_RTLS_EXTRA( Shared_MeasureAllRanges->Amount_Remaining  > 0, ttc_assert_origin_auto ); // unexpected value: Check implementation of rtls_crtof_simple2d !
                Shared_MeasureAllRanges->Amount_Remaining--;
            }
            break;
        }
        default:                       { // state did not change since previous call

            // we will measure distances to nodes 2 (NorthPole), 3, ..., n
            if ( Shared_MeasureAllRanges->Amount_Remaining > 0 ) { // still range measures to do
                #if (TTC_ASSERT_RTLS_EXTRA == 1)
                t_u16 MaxIterations = Config_RTLS->ConfigSLAM->Init.Amount_Nodes;
                #endif

                do {  // proceed to next node for which range measure is still missing
                    if ( Shared_MeasureAllRanges->Index_RemoteAnchor >= Config_RTLS->ConfigSLAM->Init.Amount_Nodes ) {

                        // set index of first remote node to measure range to
                        Shared_MeasureAllRanges->Index_RemoteAnchor = simple_2d_index_northpole;
                    }
                    else
                    { Shared_MeasureAllRanges->Index_RemoteAnchor++; }

                    #if (TTC_ASSERT_RTLS_EXTRA == 1)
                    MaxIterations--;
                    Assert_RTLS_EXTRA( MaxIterations > 0, ttc_assert_origin_auto ); // internal error: cannot find a node index without valid distance. Check implementation of rtls_crtof_simple2d !
                    #endif

                }
                while ( ttc_slam_get_distance( Config_RTLS->Init.Index_SLAM,
                                               Config_RTLS->LocalID16,
                                               Shared_MeasureAllRanges->Index_RemoteAnchor )
                        != TTC_SLAM_UNDEFINED_VALUE );


                ttc_states_call( MyStatemachine,
                                 state_s2d_southpole_MeasureSingleRange,
                                 &( Shared_MeasureAllRanges->MeasureSingleRange )
                               );
            }
            else {                                                   // ranges to all other nodes complete: proceed to next state
                ttc_states_switch( MyStatemachine, state_s2d_southpole_RequestAllRanges2Northpole );
            }
            break;
        }
    }
}
void _scs2_anchor_southpole_state_request_all_ranges_to_northpole( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*                          Config_RTLS = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config*              Config_Simple2D  = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    t_scs2_anchor_config*                            Config_Anchor    = &( Config_Simple2D->Anchor );
    t_statedata_s2d_request_all_ranges_to_northpole* Shared_RequestAllRanges = &( Config_Anchor->Shared.RequestAllRanges );

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state

            // reset data set of this state
            ttc_memory_set( Shared_RequestAllRanges, 0, sizeof( *Shared_RequestAllRanges ) );

            // initialize with maximum value
            Shared_RequestAllRanges->Index_RemoteAnchor = -1;

            // will have to request range to northpole from nodes 3..n
            Shared_RequestAllRanges->Amount_Remaining   = Config_RTLS->ConfigSLAM->Init.Amount_Nodes - 2;

            break;
        }
        case states_transition_Return: { // came back from a sub-state via ttc_state_return()

            // check if slam model now stores a distance from remote node to northpole
            if ( ttc_slam_get_distance( Config_RTLS->Init.Index_SLAM,
                                        simple_2d_index_northpole,
                                        Shared_RequestAllRanges->Index_RemoteAnchor )
                    != TTC_SLAM_UNDEFINED_VALUE
               ) { // distance request successfull: decrease amount of ranges remaining to measure
                Assert_RTLS_EXTRA( Shared_RequestAllRanges->Amount_Remaining  > 0, ttc_assert_origin_auto ); // unexpected value: Check implementation of rtls_crtof_simple2d !
                Shared_RequestAllRanges->Amount_Remaining--;
            }
            break;
        }
        default:                       { // state did not change since previous call

            // we will measure distances to nodes 2 (NorthPole), 3, ..., n
            if ( Shared_RequestAllRanges->Amount_Remaining > 0 ) { // still range measures to do
                #if (TTC_ASSERT_RTLS_EXTRA == 1)
                t_u16 MaxIterations = Config_RTLS->ConfigSLAM->Init.Amount_Nodes;
                #endif

                do {  // proceed to next node for which range to northpole is still missing
                    if ( Shared_RequestAllRanges->Index_RemoteAnchor >= Config_RTLS->ConfigSLAM->Init.Amount_Nodes ) {

                        // set index of first remote node to measure range to
                        Shared_RequestAllRanges->Index_RemoteAnchor = simple_2d_index_northpole;
                    }
                    else
                    { Shared_RequestAllRanges->Index_RemoteAnchor++; }

                    #if (TTC_ASSERT_RTLS_EXTRA == 1)
                    MaxIterations--;
                    Assert_RTLS_EXTRA( MaxIterations > 0, ttc_assert_origin_auto ); // internal error: cannot find a node index without valid distance. Check implementation of rtls_crtof_simple2d !
                    #endif
                }
                while ( ttc_slam_get_distance( Config_RTLS->Init.Index_SLAM,
                                               simple_2d_index_northpole,
                                               Shared_RequestAllRanges->Index_RemoteAnchor )
                        != TTC_SLAM_UNDEFINED_VALUE );


                ttc_states_switch( MyStatemachine,
                                   state_s2d_southpole_RequestSingleRange2Northpole
                                 );
            }
            else {                                                   // ranges to all other nodes complete: proceed to next state
                ttc_states_switch( MyStatemachine, state_s2d_southpole_BuildCoordinateSystem );
            }
            break;
        }
    }
}
void _scs2_anchor_southpole_state_report_ranges( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*                   Config_RTLS       = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config*       Config_Simple2D        = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    t_scs2_anchor_config*                     Config_Anchor          = &( Config_Simple2D->Anchor );
    t_rtls_common_data_report_distances* Config_ReportDistances = &( Config_Anchor->Shared.ReportDistances );

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state

            // reset data set of this state
            ttc_memory_set( Config_ReportDistances, 0, sizeof( *Config_ReportDistances ) );

            // will report all 2*n distances (redundant distances reported too: 1->1, 2->1, 2->2)
            Config_ReportDistances->DistancesAmount = 2 * Config_RTLS->ConfigSLAM->Init.Amount_Nodes;

            Config_ReportDistances->DistancesOffset = 0;
            Config_ReportDistances->ReferenceTime   = NULL;
            Config_ReportDistances->TargetIDsAmount = 0;

            break;
        }
        case states_transition_Return: { // came back from a sub-state via ttc_state_return()
            Assert_RTLS( 0, ttc_assert_origin_auto ); // invalid/ unsupported transition: Check implementation!
            //..
            break;
        }
        default:                       { // state did not change since previous call: run statemachine to report distances

            if ( rtls_common_statemachine_report_distances( Config_RTLS, Config_ReportDistances ) ) {
                // distance report completed: proceed to next state
                ttc_states_switch( MyStatemachine, state_s2d_southpole_Ready );
            }
            break;
        }
    }
}
void _scs2_anchor_southpole_state_ready( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*             Config_RTLS = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    t_scs2_anchor_config*               Config_Anchor    = &( Config_Simple2D->Anchor );


    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state

            //.. do some initialization stuff
            break;
        }
        case states_transition_Return: { // came back from a sub-state via ttc_state_return()

            //..
            break;
        }
        default:                       { // state did not change since previous call

            t_ttc_rtls_packet* PacketRX_Payload;
            t_u16                   PacketRX_PayloadMaxSize;

            // casting away const! (ttc_packet does not support constant packets)
            const t_ttc_packet* PacketPeeked = ( t_ttc_packet* ) ttc_radio_packet_received_peek( Config_RTLS->ConfigRadio->LogicalIndex );
            if ( PacketPeeked ) { // just received a packet from someone
                ttc_packet_payload_get_pointer( ( t_ttc_packet* ) PacketPeeked, ( t_u8** ) &PacketRX_Payload, &PacketRX_PayloadMaxSize );
                t_ttc_packet* PacketRX = NULL;
                if ( ( PacketRX_Payload->Header.Type > ttc_rtls_packet_generic ) &&
                        ( PacketRX_Payload->Header.Type < ttc_rtls_packet_2mobile )
                   ) {
                    // seems to be a packet for us: pop it from ListRX
                    PacketRX = ttc_radio_packet_received_tryget( Config_RTLS->ConfigRadio->LogicalIndex );
                    Assert_RTLS( PacketRX == PacketPeeked, ttc_assert_origin_auto ); // somehow peeked packet got lost: You're sure that no other task is accessing ttc_radio in parallel?
                }

                if ( PacketRX ) { // Received packet is for us: remove from receive queue + process packet
                    switch ( PacketRX_Payload->Header.Type ) {
                        case ttc_rtls_packet_2anchor_request_all_ranges: { // sender is requesting a report of all ranges: -> ReportRanges
                            ttc_states_switch( MyStatemachine, state_s2d_southpole_ReportRanges );
                            break;
                        }
                        case ttc_rtls_packet_generic_request_ranging:
                            break; // ranging requests are handled by radio_common.c:radio_common_push_list_rx()
                        case ttc_rtls_packet_2anchor_request_localization:
#warning ToDo: implement localization reply!
                            break; // localization requests are handled by radio_common.c:radio_common_push_list_rx()
                        default: break;
                    }
                    // give back packet buffer for reuse
                    ttc_radio_packet_release( PacketRX );
                }
            }

            break;
        }
    }
}
void _scs2_anchor_southpole_state_request_single_range_to_northpole( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*             Config_RTLS = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    t_scs2_anchor_config*               Config_Anchor    = &( Config_Simple2D->Anchor );
    t_statedata_s2d_request_all_ranges_to_northpole* Shared_RequestAllRanges = &( Config_Anchor->Shared.RequestAllRanges );

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state
            ttc_systick_delay_init( &( Config_Anchor->TimeOut ), 500000 ); // 500ms

            t_ttc_rtls_packet* Payload;
            t_u16 PayloadSize;

            // find target address in list of all known anchor nodes
            Assert_RTLS( Shared_RequestAllRanges->Index_RemoteAnchor > 0, ttc_assert_origin_auto ); // valid index value = 1..n: check implementation!
            Assert_RTLS( Shared_RequestAllRanges->Index_RemoteAnchor <= Config_RTLS->Init.Amount_AnchorIDs, ttc_assert_origin_auto ); // valid index value = 1..n: check implementation!
            Shared_RequestAllRanges->RemoteAddress = &( Config_RTLS->Init.AnchorIDs[Shared_RequestAllRanges->Index_RemoteAnchor - 1] );

            t_ttc_packet* PacketTX = rtls_common_packet_get_empty_try( Config_RTLS,
                                                                       &Payload,
                                                                       &PayloadSize,
                                                                       Shared_RequestAllRanges->RemoteAddress,
                                                                       E_ttc_packet_type_802154_Data_001010
                                                                     );
            if ( PacketTX ) { // got an empty packet: create message request range to northpole
                Payload->Header.Type = ttc_rtls_packet_2anchor_request_range_to_northpole;
                Shared_RequestAllRanges->SequenceNumber = Config_RTLS->SequenceNumber++;
                Payload->Union.CRTOF_Simple_2D.RequestDistance2Northpole.SequenceNumber = Shared_RequestAllRanges->SequenceNumber;

                ttc_radio_transmit_packet_now( Config_RTLS->Init.Index_Radio,
                                               PacketTX,
                                               sizeof( Payload->Header ) + sizeof( Payload->Union.CRTOF_Simple_2D.RequestDistance2Northpole ),
                                               TRUE
                                             );
            }
            break;
        }
        case states_transition_Return: { // came back from a sub-state via ttc_state_return()
            Assert_RTLS_EXTRA( 0, ttc_assert_origin_auto ); // unsupported transition: check implementation!
            break;
        }
        default:                       { // state did not change since previous call
            BOOL ReceivedRange2Nortpole = FALSE;

            t_ttc_rtls_packet* PacketRX_Payload;
            t_u16                   PacketRX_PayloadMaxSize;

            // casting away const! (ttc_packet does not support constant packets)
            const t_ttc_packet* PacketPeeked = ( t_ttc_packet* ) ttc_radio_packet_received_peek( Config_RTLS->ConfigRadio->LogicalIndex );
            if ( PacketPeeked ) { // just received a packet from someone
                ttc_packet_payload_get_pointer( ( t_ttc_packet* ) PacketPeeked, ( t_u8** ) &PacketRX_Payload, &PacketRX_PayloadMaxSize );
                t_ttc_packet* PacketRX = NULL;
                if ( ( PacketRX_Payload->Header.Type > ttc_rtls_packet_generic ) &&
                        ( PacketRX_Payload->Header.Type < ttc_rtls_packet_2mobile )
                   ) { // seems to be a packet for us: pop it from ListRX
                    PacketRX = ttc_radio_packet_received_tryget( Config_RTLS->ConfigRadio->LogicalIndex );
                    Assert_RTLS( PacketRX == PacketPeeked, ttc_assert_origin_auto ); // somehow peeked packet got lost: You're sure that no other task is accessing ttc_radio in parallel?
                }

                if ( PacketRX ) { // Received packet is for us: remove from receive queue + process packet
                    switch ( PacketRX_Payload->Header.Type ) {
                        case ttc_rtls_packet_2anchor_report_range_to_northpole: { // sender is reporting ranges: read ranges from message

                            if ( ( E_ttc_packet_address_source_compare( PacketRX, Shared_RequestAllRanges->RemoteAddress ) ) && // report from node to which request was sent to before
                                    ( PacketRX_Payload->Union.CRTOF_Simple_2D.ReportDistance2Northpole.SequenceNumber == Shared_RequestAllRanges->SequenceNumber )
                               ) { // report matches our request: update reported distance

                                ReceivedRange2Nortpole = TRUE;
                                ttc_slam_update_distance( Config_RTLS->Init.Index_SLAM,
                                                          simple_2d_index_northpole,
                                                          Shared_RequestAllRanges->Index_RemoteAnchor,
                                                          PacketRX_Payload->Union.CRTOF_Simple_2D.ReportDistance2Northpole.Distance_CM
                                                        );
                            }
                            break;
                        }
                        default:

                            break; // ignoring packet
                    }

                    // give back packet buffer for reuse
                    ttc_radio_packet_release( PacketRX );
                }
            }

            if ( ReceivedRange2Nortpole || ttc_systick_delay_expired( &( Config_Anchor->TimeOut ) ) ) {
                ttc_states_switch( MyStatemachine, state_s2d_southpole_RequestAllRanges2Northpole );
            }
            break;
        }
    }

// ttc_states_switch( MyStatemachine, state_s2d_ );
}

// State functions of all non-southpole anchor nodes --------------------------------------------------------------------------
void _scs2_anchor_other_state_init( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    ( void ) Transition;

    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*             Config_RTLS = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    t_scs2_anchor_config*               Config_Anchor    = &( Config_Simple2D->Anchor );

    t_ttc_packet* PacketRX;
    do {  // empty receive queue (will ignore all previously received packets and free memory blocks for new packets)
        PacketRX = ( t_ttc_packet* ) ttc_radio_packet_received_tryget( Config_RTLS->ConfigRadio->LogicalIndex );
        if ( PacketRX )
        { ttc_radio_packet_release( PacketRX ); }
    }
    while ( PacketRX );

    ( void ) Config_Simple2D;
    ( void ) Config_Anchor;

    ttc_states_switch( MyStatemachine, state_s2d_anchor_Waiting );
}
void _scs2_anchor_other_state_waiting( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*             Config_RTLS = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    t_scs2_anchor_config*               Config_Anchor    = &( Config_Simple2D->Anchor );
    t_statedata_s2d_waiting*            Shared_Waiting   = &( Config_Anchor->Shared.Waiting );

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state
            // must initialize shared memory
            ttc_memory_set( Shared_Waiting, 0, sizeof( *Shared_Waiting ) );

            if ( MyStatemachine->State_Previous == state_s2d_anchor_Init ) { // came from init: first initialization
                ttc_systick_delay_init( &( Config_Anchor->TimeOut ), 10000000 ); // 10s timeout until requesting all ranges from southpole
                return;
            }
            if ( MyStatemachine->State_Previous == state_s2d_anchor_RequestAllRanges ) { // came from requesting all ranges: initialize timeout
                ttc_systick_delay_init( &( Config_Anchor->TimeOut ), 20000000 ); // 20s timeout until requesting all ranges from southpole again
                return;
            }
            break;
        }
        case states_transition_Return: { // came back from a sub-state via ttc_state_return()
            if ( Config_RTLS->Flags.IsCoordinateSystemReady ) {
                ttc_states_switch( MyStatemachine,  state_s2d_anchor_Ready );
                return;
            }
            //..
            break;
        }
        default:                       { // state did not change since previous call
            if ( ttc_systick_delay_expired( &( Config_Anchor->TimeOut ) ) ) {
                ttc_states_switch( MyStatemachine,  state_s2d_anchor_RequestAllRanges );
                return;
            }

            t_ttc_rtls_packet* PacketRX_Payload;
            t_u16                   PacketRX_PayloadMaxSize;

            // casting away const! (ttc_packet does not support constant packets)
            const t_ttc_packet* PacketPeeked = ( t_ttc_packet* ) ttc_radio_packet_received_peek( Config_RTLS->ConfigRadio->LogicalIndex );
            if ( PacketPeeked ) { // just received a packet from someone
                ttc_packet_payload_get_pointer( ( t_ttc_packet* ) PacketPeeked, ( t_u8** ) &PacketRX_Payload, &PacketRX_PayloadMaxSize );
                t_ttc_packet* PacketRX = NULL;
                if ( ( PacketRX_Payload->Header.Type > ttc_rtls_packet_generic ) &&
                        ( PacketRX_Payload->Header.Type < ttc_rtls_packet_2mobile )
                   ) {
                    // seems to be a packet for us: pop it from ListRX
                    PacketRX = ttc_radio_packet_received_tryget( Config_RTLS->ConfigRadio->LogicalIndex );
                    Assert_RTLS( PacketRX == PacketPeeked, ttc_assert_origin_auto ); // somehow peeked packet got lost: You're sure that no other task is accessing ttc_radio in parallel?
                }

                if ( PacketRX ) { // Received packet is for us: remove from receive queue + process packet
                    switch ( PacketRX_Payload->Header.Type ) {
                        case ttc_rtls_packet_2anchor_request_range_to_northpole: { // sender is requesting a range measure to northpole
                            ttc_states_switch( MyStatemachine, state_s2d_anchor_MeasureRange2Northpole );
                            break;
                        }
                        case ttc_rtls_packet_generic_report_distances: { // sender is reporting ranges among nodes: try to build up coordinate system
                            ttc_states_call( MyStatemachine, state_s2d_anchor_BuildCoordinateSystem, PacketRX );
                            return; // sub-state will free PacketRX
                            break;
                        }
                        case ttc_rtls_packet_generic_request_ranging:
                            break; // ranging requests are handled by radio_common.c:radio_common_push_list_rx()
                        default: break;
                    }
                    // give back packet buffer for reuse
                    ttc_radio_packet_release( PacketRX );
                }
            }
            break;
        }
    }

    // ttc_states_switch( MyStatemachine, state_s2d_ );
}
void _scs2_anchor_other_state_request_all_ranges( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*             Config_RTLS = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    t_scs2_anchor_config*               Config_Anchor    = &( Config_Simple2D->Anchor );

    ( void ) Transition;
    ( void ) Config_Anchor;

    t_ttc_rtls_packet* Payload;
    t_u16 PayloadSize;

    // set remote address of southpole (first entry in AnchorIDs[])
    Config_Simple2D->Anchor.Shared.RequestAllRanges.RemoteAddress = &( Config_RTLS->Init.AnchorIDs[0] );

    // obtain empty packet
    t_ttc_packet* PacketTX = rtls_common_packet_get_empty_try( Config_RTLS,
                                                               &Payload,
                                                               &PayloadSize,
                                                               Config_Simple2D->Anchor.Shared.RequestAllRanges.RemoteAddress,
                                                               E_ttc_packet_type_802154_Data_001010
                                                             );

    if ( PacketTX ) { // got empty packet: fill payload and transmit it
        Payload->Header.Type = ttc_rtls_packet_2anchor_request_all_ranges;

        // prepare distance request + report
        //? Config_Simple2D->Anchor.DistanceReportRX.SenderID16  = 0;

        ttc_radio_transmit_packet_now( Config_RTLS->Init.Index_Radio,
                                       PacketTX,
                                       sizeof( t_ttc_rtls_payload_header ), // sending just header
                                       TRUE
                                     );

    }

    ttc_states_switch( MyStatemachine, state_s2d_anchor_Waiting );
}
void _scs2_anchor_other_state_measure_range_to_northpole( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*                     Config_RTLS = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config*         Config_Simple2D  = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    t_scs2_anchor_config*                       Config_Anchor    = &( Config_Simple2D->Anchor );
    t_statedata_s2d_measure_range_to_northpole* Shared_MeasureRangeToNorthpole = &( Config_Anchor->Shared.MeasureRangeToNorthpole );

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state
            ttc_memory_set( Shared_MeasureRangeToNorthpole, 0, sizeof( *Shared_MeasureRangeToNorthpole ) );

            // configure single range measure
            Shared_MeasureRangeToNorthpole->Private.MeasureSingleRange.Input.Amount_RangeMeasures = Config_RTLS->Init.Amount_RawMeasures;
            Shared_MeasureRangeToNorthpole->Private.MeasureSingleRange.Input.Index_RemoteAnchor   = simple_2d_index_northpole - 1;

            // call substate to do the range measure to northpole
            ttc_states_call( MyStatemachine,
                             state_s2d_anchor_MeasureSingleRange,
                             &( Shared_MeasureRangeToNorthpole->Private.MeasureSingleRange )
                           );
            break;
        }
        case states_transition_Return: { // came back from a sub-state via ttc_state_return(): copy measured distance into return value + switch to Waiting
            Shared_MeasureRangeToNorthpole->Return.Distance_cm = Shared_MeasureRangeToNorthpole->Private.MeasureSingleRange.Return.Distance_cm;
            ttc_states_switch( MyStatemachine, state_s2d_anchor_Waiting );
            break;
        }
        default:                       { // state did not change since previous call
            Assert_RTLS_EXTRA( 0, ttc_assert_origin_auto ); // this state should not be run twice. Check implementation!
            break;
        }
    }
}
void _scs2_anchor_other_state_ready( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*             Config_RTLS = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    t_scs2_anchor_config*               Config_Anchor    = &( Config_Simple2D->Anchor );

#warning ToDo: implement _scs2_anchor_other_state_ready()!
    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state

            //.. do some initialization stuff
            break;
        }
        case states_transition_Return: { // came back from a sub-state via ttc_state_return()

            //..
            break;
        }
        default:                       { // state did not change since previous call

            //..
            break;
        }
    }
}

// Universal state functions for all types of anchor nodes --------------------------------------------------------------------------
void _scs2_anchor_substate_build_coordinate_system( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*             Config_RTLS = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    t_scs2_anchor_config*               Config_Anchor    = &( Config_Simple2D->Anchor );
    t_statedata_s2d_build_coordinate_system* MyStateData = ( t_statedata_s2d_build_coordinate_system* ) ttc_states_get_argument( MyStatemachine );

#warning ToDo: implement _scs2_anchor_substate_build_coordinate_system()!

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state

            //.. do some initialization stuff
            break;
        }
        case states_transition_Return: { // came back from a sub-state via ttc_state_return()

            //..
            break;
        }
        default:                       { // state did not change since previous call

            ttc_states_return( MyStatemachine, NULL );
            break;
        }
    }

    // ttc_states_switch( MyStatemachine, state_s2d_ );
}
void _scs2_anchor_substate_reply_ranging( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*             Config_RTLS = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    t_scs2_anchor_config*               Config_Anchor    = &( Config_Simple2D->Anchor );

#warning ToDo: implement _scs2_anchor_substate_reply_ranging()!

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state

            //.. do some initialization stuff
            break;
        }
        case states_transition_Return: { // came back from a sub-state via ttc_state_return()

            //..
            break;
        }
        default:                       { // state did not change since previous call

            ttc_states_return( MyStatemachine, NULL );
            break;
        }
    }

    // ttc_states_switch( MyStatemachine, state_s2d_ );
}
void _scs2_anchor_substate_reply_localization( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*             Config_RTLS = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    t_scs2_anchor_config*               Config_Anchor    = &( Config_Simple2D->Anchor );

#warning ToDo: implement _scs2_anchor_substate_reply_localization()!

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state

            //.. do some initialization stuff
            break;
        }
        case states_transition_Return: { // came back from a sub-state via ttc_state_return()

            //..
            break;
        }
        default:                       { // state did not change since previous call

            ttc_states_return( MyStatemachine, NULL );
            break;
        }
    }

    // ttc_states_switch( MyStatemachine, state_s2d_ );
}
void _scs2_anchor_states_monitor( volatile t_ttc_states_config* MyStatemachine, t_ttc_rtls_config* MyStateData, volatile e_ttc_rtls_state State_Current, volatile e_ttc_rtls_state State_Previous, e_ttc_states_transition Transition ) {
    // place breakpoint here!

    // avoid compiler warnings about unused variables
    ( void ) MyStatemachine;
    ( void ) MyStateData;
    ( void ) State_Current;
    ( void ) State_Previous;
    ( void ) Transition;

    volatile static t_u32 CallCounter = 0; // counts function calls
    const           t_u32 BreakAt     = 0; // set to value > 0 to enable breakpoint at certain function call
    CallCounter++;

    if ( BreakAt && ( CallCounter == BreakAt ) ) {
        ttc_assert_break_origin( ttc_assert_origin_auto ); // desired function call reached
    }
}

#endif //TTC_RTLS_IMPLEMENT_ANCHOR == 1
#if TTC_RTLS_IMPLEMENT_MOBILE == 1

void _scs2_mobile_states_monitor( volatile t_ttc_states_config* MyStatemachine, t_ttc_rtls_config* MyStateData, volatile e_ttc_rtls_state State_Current, volatile e_ttc_rtls_state State_Previous, e_ttc_states_transition Transition ) {
    // place breakpoint here!

    // avoid compiler warnings about unused variables
    ( void ) MyStatemachine;
    ( void ) MyStateData;
    ( void ) State_Current;
    ( void ) State_Previous;
    ( void ) Transition;

    volatile static t_u32 CallCounter = 0; // counts function calls
    const           t_u32 BreakAt     = 0; // set to value > 0 to enable breakpoint at certain function call
    CallCounter++;

    if ( BreakAt && ( CallCounter == BreakAt ) ) {
        ttc_assert_break_origin( ttc_assert_origin_auto ); // desired function call reached
    }
}

#endif //TTC_RTLS_IMPLEMENT_MOBILE == 1

// super universal substate functions for anchor and mobile nodes
void _scs2_node_substate_measure_single_range( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition )  {
    // only we know what type of pointer is stored because we set it before
    t_ttc_rtls_config*             Config_RTLS = ( t_ttc_rtls_config* ) StateData;
    Assert_RTLS_EXTRA_Writable( Config_RTLS->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    //?Used t_rtls_crtof_simple_2d_config* Config_Simple2D  = &( Config_RTLS->LowLevelConfig->crtof_simple_2d );
    //?Used t_scs2_anchor_config*               Config_Anchor    = &( Config_Simple2D->Anchor );

    // obtain argument passed to ttc_states_call()
    t_statedata_s2d_measure_single_range* Config_SingleRange = ( t_statedata_s2d_measure_single_range* ) ttc_states_get_argument( MyStatemachine );

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state
            Config_SingleRange->Return.Distance_cm = 0;
            ttc_memory_set( &( Config_SingleRange->Private ), 0, sizeof( Config_SingleRange->Private ) );
            break;
        }
        case states_transition_Return: { // came back from a sub-state via ttc_state_return()
            Assert_RTLS_EXTRA( 0, ttc_assert_origin_auto ); // this state does not call any substates. Check implementation!
            break;
        }
        default:                       { // state did not change since previous call: issue ranging request
            ttc_radio_ranging_request( Config_RTLS->Init.Index_Radio,
                                       rcrt_Request_Ranging_SSTOFCR,
                                       1,
                                       &( Config_RTLS->Init.AnchorIDs[1] ), // address of northpole
                                       Config_RTLS->Init.TimeOut_RangeMeasureMS,
                                       NULL,
                                       &( Config_SingleRange->Private.RawDistance ),
                                       NULL // not used for Type==rcrt_Request_Ranging_SSTOFCR
                                     );
#warning ToDo: Implement averaging of multiple distance measures!

            ttc_states_return( MyStatemachine, NULL );
            break;
        }
    }

    // ttc_states_switch( MyStatemachine, state_s2d_ );
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
