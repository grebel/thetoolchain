#ifndef RTLS_CRTOF_SIMPLE_2D_H
#define RTLS_CRTOF_SIMPLE_2D_H

// Note: Implementation state unfinished. - This code is not working. See rtls_square4 instead!

/** { rtls_crtof_simple_2d.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_rtls.h providing
 *  function prototypes being required by ttc_rtls.h
 *
 *  synchronous location and mapping radio protocol for a simple two dimensional anchor setup using
 *  the cascaded replies time of flight ranging protocol
 *
 *  Created from template device_architecture.h revision 28 at 20161208 10:46:38 UTC
 *
 *  Note: See ttc_rtls.h for description of crtof_simple_2d independent RTLS implementation.
 *
 *  Authors: Gregor Rebel
 *
 *
 *  Description of rtls_crtof_simple_2d
 *
 *  This low-level driver is specially optimized for a combination of
 *  #) ttc_slam using the slam_simple_2d low-level driver (see slam/slam_simple_2d.h for details)
 *  #) Time of Flight Cascading Replies (ToF-CR) ranging protocol as described here:
 *     https://www.researchgate.net/publication/306509328_A_Novel_Indoor_Localization_Scheme_for_Autonomous_Nodes_in_IEEE_802154a_Networks
 *
 *  For n anchor nodes, the slam_simple_2d driver stores only O(2 * n) bytes of ranging data
 *  instead of O(n!) for all possible distances. This low-level driver reduces the amount of
 *  radio packets by measuring only those ranges being used by slam_simple_2d.
 *
 *
 *  How to Configure rtls_crtof_simple_2d
 *
 *      t_ttc_rtls_config* Config = ttc_rtls_get_configuration( <N> );
 *
 *      Basic initialization as described in ttc_rtls.h
 *
 *      Config->Init.AnchorIDs = array storing pointers to packet addresses of all participating anchor nodes
 *
 *                          Special meaning of array indices as corresponding to slam_simple_2d.h:
 *                          Config->Init.AnchorIDs[0]  stores address of south pole (node #1)
 *                          Config->Init.AnchorIDs[1]  stores address of north pole (node #2)
 *                          Config->Init.AnchorIDs[i>1, i=2k,   k=1..e]  east anchor node #i
 *                          Config->Init.AnchorIDs[i>1, i=2k+1, k=1..w]  west anchor node #i
 *
 *                          In other words:
 *                          Config->Init.AnchorIDs[2,4,6,8, ...] store address of anchor nodes being physically placed on right side of vector south->north anchor node
 *                          Config->Init.AnchorIDs[3,5,7,9, ...] store address of anchor nodes being physically placed on left side of vector south->north anchor node
 *
 *      Config->AnchorIDs_Amount = maximum valid array index of Config->Init.AnchorIDs[] + 1
 *
 *      ttc_rtls_init( <N> );
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_RTLS_DRIVER_AVAILABLE
#define EXTENSION_RTLS_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_rtls_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)


//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "rtls_crtof_simple_2d.c"
//
#include "../ttc_rtls_types.h" // will include rtls_crtof_simple_2d_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_rtls_interface.h
//
// Some driver functions may be optional to implement.
// Check ttc_rtls_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_rtls_foo
//
#define ttc_driver_rtls_configuration_check  rtls_crtof_simple_2d_configuration_check
#define ttc_driver_rtls_deinit               rtls_crtof_simple_2d_deinit
#define ttc_driver_rtls_init                 rtls_crtof_simple_2d_init
#define ttc_driver_rtls_load_defaults        rtls_crtof_simple_2d_load_defaults
#define ttc_driver_rtls_prepare              rtls_crtof_simple_2d_prepare
#define ttc_driver_rtls_reset                rtls_crtof_simple_2d_reset
#define ttc_driver_rtls_anchor_buildup_start rtls_crtof_simple_2d_anchor_buildup_start
#define ttc_driver_rtls_anchor_statemachine  rtls_crtof_simple_2d_anchor_statemachine
#define ttc_driver_rtls_mobile_statemachine  rtls_crtof_simple_2d_mobile_statemachine
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_rtls.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_rtls.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_rtls_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_rtls_features.
 *
 * The special slam implementation slam_simple_2d puts some constraints to node configuration.
 * These constraints are checked if asserts are enabled.
 *
 * 1) Config->Init.AnchorIDs[] stores at least two non-zero entries
 *    At least two nodes (south and north pole) are required to build up coordinate system.
 *
 * 2) Config->Init.AnchorIDs[Config->Index_LocalID].Address16 > 0
 *    Zero is not a valid node address.
 *
 * 3) Config->Init.AnchorIDs[0]->Address16 == 1
 *    First entry of AnchorIDs[] must store address of south pole.
 *
 * 4) Config->Init.AnchorIDs[1]->Address16 == 2
 *    Second entry of AnchorIDs[] must store address of north pole.
 *
 * 5) Config->Init.AnchorIDs[>=2]->Address16 >= 3 || == 0
 *    All remaining entries store none, a west or an east node.
 *
 * 6) Non-zero entries of AnchorIDs[] are sorted by their 16 bit protocol address.
 *
 * 7) Config->Init.AnchorIDs[] stores no Address16 twice (except 0)
 *
 * @param Config        Configuration of rtls device
 * @return              Fields of *Config have been adjusted if necessary
 */
void rtls_crtof_simple_2d_configuration_check( t_ttc_rtls_config* volatile Config );


/** shutdown single RTLS unit device
 * @param Config        Configuration of rtls device
 * @return              == 0: RTLS has been shutdown successfully; != 0: error-code
 */
e_ttc_rtls_errorcode rtls_crtof_simple_2d_deinit( t_ttc_rtls_config* Config );


/** initializes single RTLS unit for operation
 * @param Config        Configuration of rtls device
 * @return              == 0: RTLS has been initialized successfully; != 0: error-code
 */
e_ttc_rtls_errorcode rtls_crtof_simple_2d_init( t_ttc_rtls_config* Config );


/** loads configuration of indexed RTLS unit with default values
 * @param Config        Configuration of rtls device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_rtls_errorcode rtls_crtof_simple_2d_load_defaults( t_ttc_rtls_config* Config );


/** Prepares rtls Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void rtls_crtof_simple_2d_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of rtls device
 */
void rtls_crtof_simple_2d_reset( t_ttc_rtls_config* Config );


/** Start building up coordinate system.
 *
 * Will send range requests to all neighbouring anchor nodes and exchange data to build up
 * a coordinate system.
 *
 *
 * @param Config       (t_ttc_rtls_config*)  Configuration of rtls device as returned by ttc_rtls_get_configuration()
 * @param LogicalIndex (t_u8)                     Logical index of rtls instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RTLS<n> lines in extensions.active/makefile
 */
void rtls_crtof_simple_2d_anchor_buildup_start( t_ttc_rtls_config* Config );


/** Statemachine of anchor_*() functions. Call regularly to process events.
 *
 * Note: Requires Config->Flags.Bits.IsAnchor==1 before ttc_rtls_init() call!
 *
 * According to slam_simple_2d.h, anchor nodes can be divided into two main groups:
 *
 * 1) North and South nodes
 *    These form the y-axis of the coordinate system with south anchor node at (0,0,0).
 *
 * 2) West and East nodes
 *    All west nodes are located left of vector south->north.
 *    All east nodes are located right of vector south->north.
 *    Each west and east node will measure its range to north and south node to laterate
 *    its own position.
 *
 * These two groups also have different statemachine implementations for building up
 * their coordinate system.
 *
 *
 * Pseudo code of coordinate system builtup in north and south anchor nodes
 *
 *     while (StateAnchor != 0) {
 *       switch (StateAnchor) {
 *         case tss_CRTOF_Anchor_BuildUp_Start:
 *             Index_RemoteAnchor = AnchorIDs_Amount
 *             StateAnchor = tss_CRTOF_Anchor_BuildUp_RequestRanges
 *             break;
 *
 *         case tss_CRTOF_Anchor_BuildUp_RequestRanges:
 *             if (Index_RemoteAnchor >= 2) { // AnchorIDs[2..] store addresses of east- and west nodes
 *               request range measure to remote anchor node AnchorIDs[Index_RemoteAnchor]
 *               report distances to all other anchor nodes using broadcast message
 *               Index_RemoteAnchor--;
 *             }
 *             else {
 *               StateAnchor = tss_CRTOF_Anchor_BuildUp_RequestDistances
 *             }
 *             break;
 *
 *         case tss_CRTOF_Anchor_BuildUp_RequestDistances:
 *             StateAnchor = tss_CRTOF_Anchor_BuildUp_Complete
 *             break;
 *
 *         case tss_CRTOF_Anchor_BuildUp_Complete:
 *             Let ttc_slam calculate initial mapping.
 *             StateAnchor = 0
 *             break;
 *       }
 *     }
 *
 *
 * Pseudo code of coordinate system builtup in east and west anchor nodes
 *
 *     while (StateAnchor != 0) {
 *       switch (StateAnchor) {
 *         case tss_CRTOF_Anchor_BuildUp_Start:
 *             StateAnchor = tss_CRTOF_Anchor_BuildUp_RequestRanges
 *             break;
 *
 *         case tss_CRTOF_Anchor_BuildUp_RequestRanges:
 *             request range measure from south pole anchor node
 *             request range measure from north pole anchor node
 *             StateAnchor = tss_CRTOF_Anchor_Distances_Report
 *             break;
 *
 *         case tss_CRTOF_Anchor_Distances_Report:
 *             report measured distances to north and south node as broadcast
 *             StateAnchor = tss_CRTOF_Anchor_BuildUp_RequestDistances
 *             break;
 *
 *         case tss_CRTOF_Anchor_BuildUp_RequestDistances:
 *             if (LocalNode is west node)
 *               request all known distances from north anchor node
 *             else
 *               request all known distances from south anchor node
 *             StateAnchor = tss_CRTOF_Anchor_BuildUp_Distances_Wait
 *             break;
 *
 *         case tss_CRTOF_Anchor_BuildUp_Distances_Wait:
 *             if (Distances Received)
 *               StateAnchor = tss_CRTOF_Anchor_BuildUp_Complete
 *             else {
 *               if (TimeOut)
 *                 StateAnchor = tss_CRTOF_Anchor_BuildUp_RequestDistances
 *             }
 *             break;
 *
 *         case tss_CRTOF_Anchor_BuildUp_Complete:
 *             Let ttc_slam calculate initial mapping.
 *
 *             // shut down statemachine
 *             StateAnchor = 0
 *             break;
 *        }
 *      }
 *
 *
 * @param Config       (t_ttc_rtls_config*)  Configuration of rtls device as returned by ttc_rtls_get_configuration()
 * @param LogicalIndex (t_u8)                     Logical index of rtls instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RTLS<n> lines in extensions.active/makefile
 */
void rtls_crtof_simple_2d_anchor_statemachine( t_ttc_rtls_config* Config );


/** Statemachine of mobile_*() functions. Call regularly to process events.
 *
 * @param Config       (t_ttc_rtls_config*)  Configuration of rtls device as returned by ttc_rtls_get_configuration()
 * @param LogicalIndex (t_u8)                     Logical index of rtls instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RTLS<n> lines in extensions.active/makefile
 */
void rtls_crtof_simple_2d_mobile_statemachine( t_ttc_rtls_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //RTLS_CRTOF_SIMPLE_2D_H
