#ifndef rtls_common_types_h
#define rtls_common_types_h

/** rtls_common_types.h ****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to rtls low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.h revision 14 at 20161208 10:11:47 UTC
 *
 *  Authors: Gregor Rebel
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "rtls_crtof_simple_2d.c"
 */

#include "../ttc_basic.h"
#include "../ttc_radio_types.h"
#include "../ttc_rtls_types.h" // will include rtls_common_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_rtls_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_rtls_foo
 */

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions

// packet payload formats used by rtls_common are available to all rtls_* drivers.

typedef struct {  // data of a single distance between two nodes
    t_u16 NodeID_A;  // protocol ID of node A
    t_u16 NodeID_B;  // protocol ID of node B
    t_u32 Distance;  // distance between node A and node B
} __attribute__( ( __packed__ ) ) t_rtls_common_distance;

typedef struct {  // packet reporting one or more distances
    t_u8  PacketNumber;                        // index number of this report packet (1..ReportAmount)
    t_u8  PacketAmount;                        // amount of report packets belonging together
    // (>1 if more than one packet is required to transport all distances to be reported)
    t_u8  DistancesAmount;                     // amount of distances stored in this packet
    t_rtls_common_distance Distances[0];  // array of individual distances being reported
} __attribute__( ( __packed__ ) ) t_rtls_common_packet_distances_report;

typedef struct {  // 3D location data of a single node
    t_u16                   NodeID;    // protocol identifier of corresponding node
    t_ttc_math_vector3d_xyz Position;  // 3D coordinates of node in local coordinate system
} __attribute__( ( __packed__ ) ) t_rtls_common_location;

typedef struct {  // packet reporting one or more locations
    t_u8  PacketNumber;                        // index number of this report packet (1..ReportAmount)
    t_u8  PacketAmount;                        // amount of report packets belonging together
    // (>1 if more than one packet is required to transport all distances to be reported)
    t_u8  LocationsAmount;                     // amount of distances stored in this packet
    t_rtls_common_location Locations[0];  // array of individual distances being reported
} __attribute__( ( __packed__ ) ) t_rtls_common_packet_locations_report;

typedef union {   // specific packet format
    t_rtls_common_packet_distances_report DistanceReport;
    t_rtls_common_packet_locations_report LocationsReport;
} __attribute__( ( __packed__ ) ) t_rtls_common_packet;

typedef struct s_rtls_common_data_report_distances { // data set required by rtls_common_statemachine_report_distances()

    // fields below must be set by application before calling rtls_common_statemachine_report_distances()


    struct { // fields below must be set by application before calling rtls_common_statemachine_report_locations()
        /** pointer to function that will deliver individual distance between two nodes
         *
         * @param Config    (t_ttc_rtls_config*)  Configuration of rtls device
         * @param Index     =(0..DistancesAmount-1)+DistancesOffset  index number of distance to report
         * @param Distance  points to storage to be filled by getDistance()
         * @return Fields of *Distance have been loaded with data.
         *
         * getDistance() will be called for Index = 0..DistancesAmount-1 to write distance to report into individual packet payload.
         * The use of a function pointer is more flexible and reduces memory overhead than requiring an array of certain type.
         * E.g. rtls_crtof_simple_2d uses a special, memory optimized structure to store node distances.
         */
        void ( *getDistance )( void* Config, t_u16 Index, t_rtls_common_distance* Distance );

        const t_ttc_packet_address*  TargetIDs;          // pointer to first entry of an array of target identifiers
        u_ttc_packetimestamp_40*   ReferenceTime;      // == NULL: start first transmission now; !=NULL: transmitter will be switched on at ReferenceTime + Config->ReferenceTimeTX
        e_ttc_rtls_state             State;              // current state value. Must be set to 0 before first call!
        t_u16                        TargetIDsAmount;    // ==0: send report packets as broadcast; >0: amount of node ids to send distances to (size of TargetIDs[])
        t_u16                        DistancesOffset;    // added to Index before calling getDistance(). => getDistance(Config, DistancesOffset..(DistancesOffset+DistancesAmount), ...)
        t_u16                        DistancesAmount;    // amount of distances to report (size of Distances[])
    } Init;

    struct { // fields below must not be written by application
        t_u8  DistancesPerPacket; // amount of distances that fit into each packet
        t_u16 IndexTargetIDs;     // current index of TargetIDs[]
        t_u16 IndexDistances;     // current index of Distances[]
        t_u16 PacketAmount;       // amount of packets being required to send all distances
        t_u16 PacketNumber;       // number of currently prepared packet (1..PacketsAmount)
    } priv;

} __attribute__( ( __packed__ ) ) t_rtls_common_data_report_distances;

typedef struct s_rtls_common_data_report_locations { // data set required by rtls_common_statemachine_report_locations()

    struct { // fields below must be set by application before calling rtls_common_statemachine_report_locations()
        /** pointer to function that will deliver individual location
         *
         * @param Config    (t_ttc_rtls_config*)  Configuration of rtls device
         * @param Index     =(0..LocationsAmount-1)+LocationsOffset index number of node to report location of
         * @param Location  points to storage to be filled by getLocation()
         * @return Fields of *Location have been loaded with data.
         *
         * getLocation() will be called for Index = LocationsOffset..LocationsAmount+LocationsOffset-1 to write location to report
         * into individual packet payload.
         * The use of a function pointer is more flexible and reduces memory overhead than requiring an array of certain type.
         */
        void ( *getLocation )( void* Config, t_u16 Index, t_rtls_common_location* Location );

        const t_ttc_packet_address*  TargetIDs;          // pointer to first entry of an array of target identifiers
        u_ttc_packetimestamp_40*   ReferenceTime;      // == NULL: start first transmission now; !=NULL: transmitter will be switched on at ReferenceTime + Config->ReferenceTimeTX
        e_ttc_rtls_state             State;              // current state value. Must be set to 0 before first call!
        t_u16                        TargetIDsAmount;    // ==0: send report packets as broadcast; >0: amount of node ids to send locations to (size of TargetIDs[])
        t_u16                        LocationsOffset;    // added to Index before calling getLocation(). => getLocation(Config, LocationsOffset..(LocationsOffset+LocationsAmount), ...)
        t_u16                        LocationsAmount;    // amount of locations to report (size of Locations[])
    } Init;

    struct { // return values from statemachine are stored in these fields
        BOOL TransmitSuccessful;  // ==TRUE: all locations have been transmitted successfully; ==FALSE: timeout or still transmitting
    } Status;

    struct { // fields below must not be written by application
        t_u8                 LocationsPerPacket;   // amount of locations that fit into each packet
        t_u8                 Amount_TxRetries;     // >0: if a transmission fails for some reason, it will be restart some times
        t_u16                PacketTX_PayloadSize; // size of payload area in PacketTX
        t_u16                IndexTargetIDs;       // current index of TargetIDs[]
        t_u16                IndexLocations;       // current index of Locations[]
        t_u16                PacketAmount;         // amount of packets being required to send all locations
        t_u16                PacketNumber;         // number of currently prepared packet (1..PacketsAmount)
        t_ttc_packet*        PacketTX;             // memory buffer storing packet to be transmitted next (in case of resending same packet)
        //Xt_ttc_systick_delay  TimeOut;              // timeout delay for current operation
    } priv;

} __attribute__( ( __packed__ ) ) t_rtls_common_data_report_locations;

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //rtls_common_types_h
