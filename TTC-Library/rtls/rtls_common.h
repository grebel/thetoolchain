#ifndef rtls_common_h
#define rtls_common_h

/** rtls_common.h ****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to rtls low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.h revision 14 at 20161208 10:11:47 UTC
 *
 *  Authors: Gregor Rebel
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "rtls_crtof_simple_2d.c"
 */

#include "../ttc_basic.h"
#include "../ttc_radio_types.h"
#include "../ttc_rtls_types.h" // will include rtls_common_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_rtls_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_rtls_foo
 */

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions

/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_rtls.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_rtls.h for details.
 *
 * You normally should not need to add functions here.
 * Global feature functions must be added in ttc_rtls.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl rtls UPDATE".
 */


/** statemachine function that will send out packets to report given distances to remote nodes
 *
 * Note: Statemachine functions will return immediately and have to be called again until they return zero.
 *
 * Basic usage:
 * t_rtls_common_data_report_distances Data;
 * Data.State = 0;
 * // fill all other fields of Data..
 *
 * // call statemachine function until it returns FALSE
 * while ( rtls_common_statemachine_report_distances( Config, &Data ) );
 *
 * @param Config        (t_ttc_rtls_config*)                    Configuration of rtls device
 * @param RadioUserID   (t_u8)                                  UserID passed to ttc_radio_lock(). ==0: no locking; >0: one from Config->Anchor.RadioUserID, Config->Mobile.RadioUserID
 * @param Data          (t_rtls_common_data_report_distances*)  pointer to data set being used to run statemachine
 * @return  (BOOL) == TRUE: further function calls are required; == FALSE: transmission of all distances to all remote nodes has been finished successfully;
 */
BOOL rtls_common_statemachine_report_distances( t_ttc_rtls_config* Config, t_u8 RadioUserID, t_rtls_common_data_report_distances* Data );

/** statemachine function that will send out packets to report given locations to remote nodes
 *
 * Note: Statemachine functions will return immediately and have to be called again until they return zero.
 *
 * Basic usage:
 * t_rtls_common_data_report_locations Data;
 * Data.State = 0;
 * // fill all other fields of Data..
 *
 * // call statemachine function until it returns FALSE
 * while ( rtls_common_statemachine_report_locations( Config, &Data ) );
 *
 * @param Config        (t_ttc_rtls_config*)                    Configuration of rtls device
 * @param RadioUserID   (t_u8)                                  UserID passed to ttc_radio_lock(). ==0: no locking; >0: one from Config->Anchor.RadioUserID, Config->Mobile.RadioUserID
 * @param Data          (t_rtls_common_data_report_locations*)  pointer to data set being used to run statemachine
 * @return  (BOOL) == TRUE: further function calls are required; == FALSE: transmission of all locations to all remote nodes has been finished successfully;
 */
BOOL rtls_common_statemachine_report_locations( t_ttc_rtls_config* Config, t_u8 RadioUserID, t_rtls_common_data_report_locations* Data );

/** Tries to obtain and initialize new empty packet from ttc_radio
 *
 * This function will always return immediately.
 *
 * @param Config                (t_ttc_rtls_config*)      Configuration of rtls device as returned by ttc_rtls_get_configuration()
 * @param Payload               (t_ttc_rtls_packet**)     !=NULL: will be loaded with pointer to packet payload area; ==NULL: No payload pointer required
 * @param PayloadMaxSize        (t_u16*)                       Payload!=NULL: *PayloadSize will be loaded with available amount of bytes in ( (t_u8*) *Payload)[]
 * @param TargetAddress         (const t_ttc_packet_address*)  !=NULL: value of Type will define if logical or physical address is copied into packet header; ==NULL: no target address
 * @param Type                  (e_ttc_packet_type)            Type of packet to create. Using optimal packet type will maximize available payload area.
 * @param Protocol              ==0: no special protocol: >0: write protocol type into first byte of payload (requires TTC_PACKET_SOCKET_BYTE==1)
 * @return                      (t_ttc_rtls_packet*)      !=NULL: initialized packet ready to be filled with payload and sent via ttc_radio_transmit_*()
 */
t_ttc_packet* rtls_common_packet_get_empty_try( t_ttc_rtls_config* Config, t_ttc_rtls_packet** Payload, t_u16* PayloadMaxSize, const t_ttc_packet_address* TargetAddress, e_ttc_packet_type Type, e_ttc_packet_pattern Protocol );

/** Measure distance to single remote node.
 *
 * This function will block until given amount of measures have been taken or timeout occurs.
 *
 * @param Config                (t_ttc_rtls_config*)   Configuration of rtls device as returned by ttc_rtls_get_configuration()
 * @param RadioUserID           (t_u8)                 UserID passed to ttc_radio_lock(). ==0: no locking; >0: one from Config->Anchor.RadioUserID, Config->Mobile.RadioUserID
 * @param NodeIndex             (t_u16)                protocol index number of remote node
 * @param AmountMeasures        (t_u16)                amount of distance measures to take (higher value increases precision and duration)
 * @param TimeOutMS             (t_u32)                function will abort if a single measure takes longer than timeout (milliseconds)
 * @param MaxRetries            (t_u8)                 maximum retries for each single measure. If one measure is not successfull for MaxRetries times, then whole range measure is aborted and 0 is returned.
 * @return  == 0: distance could not be measured; >0: averaged distance in centimeters
 */
t_u32 rtls_common_measure_range( t_ttc_rtls_config* Config, t_u8 RadioUserID, t_u16 NodeIndex, t_u16 AmountMeasures, t_u32 TimeOutMS, t_u8 MaxRetries );

/** Low-level driver calls this function to update current state of anchor node coordinate system
 *
 * All anchor nodes measure ranges between them and communicate these ranges to build up a coordinate system.
 * Whenever the current rtls low-level driver wants to change the current state of the coordinate system, it should call this
 * function instead of modifying Config->Flags.IsCoordinateSystemReady directly.
 * Calling this function makes it easier to debug coordinate system build up and to call
 *
 *
 * @param Config (t_ttc_rtls_config*)
 */
void rtls_common_anchor_coordinate_system_complete( t_ttc_rtls_config* Config, BOOL Complete );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //rtls_common_h
