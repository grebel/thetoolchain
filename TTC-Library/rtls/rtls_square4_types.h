#ifndef RTLS_SIMPLE4_TYPES_H
#define RTLS_SIMPLE4_TYPES_H

/** { rtls_square4.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_rtls.h providing
 *  Structures, Enums and Defines being required by ttc_rtls_types.h
 *
 *  simple coordinate system up for 2..4 anchor nodes requiring only one range measure.
 *
 *  Created from template device_architecture_types.h revision 25 at 20180328 16:29:02 UTC
 *
 *  Note: See ttc_rtls.h for description of high-level rtls implementation.
 *  Note: See rtls_square4.h for description of square4 rtls implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_RTLS1   // device not defined in makefile
    #define TTC_RTLS1    ta_rtls_square4   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "../ttc_systick_types.h"
#include "../ttc_packet_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_rtls_types.h *************************

typedef struct {  // square4 specific configuration data of single rtls device
    t_u8                       Amount_RangeMeasures;             // amount of measures to take for every range (distance will be averaged over all measures)
    t_u32                      Distance_Southpole2Northpole_cm;  // this distance is the base length to build up our coordinate system
    t_ttc_systick_delay        Delay;                            // precise timer delay usable in statemachines and multitasking
    u_ttc_packetimestamp_40  LastPeekedRxTime;                 // used to detect packets in receive list that are not removed by someone else
} __attribute__( ( __packed__ ) ) t_rtls_square4_config;

// t_ttc_rtls_architecture is required by ttc_rtls_types.h
#define t_ttc_rtls_architecture t_rtls_square4_config

//} Structures/ Enums

#endif //RTLS_SIMPLE4_TYPES_H
