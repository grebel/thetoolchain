/** rtls_square4.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_rtls.c.
 *
 *  simple coordinate system up for 2..4 anchor nodes requiring only one range measure.
 *
 *  Created from template device_architecture.c revision 35 at 20180328 16:29:02 UTC
 *
 *  Note: See ttc_rtls.h for description of high-level rtls implementation.
 *  Note: See rtls_square4.h for description of square4 rtls implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "rtls_square4.h".
 */

#include "rtls_square4.h"
#include "../ttc_memory.h"            // basic memory checks
#include "../ttc_heap.h"              // dynamic memory and safe arrays
#include "../ttc_packet.h"            // network packets conforming to IEEE 802.15.4a (2011)
#include "../ttc_random.h"            // pseudo random number generation
#include "../ttc_slam.h"              // mathematic model of anchor node positions + localization
#include "../ttc_systick.h"           // precise timer delays
#include "../ttc_task.h"              // stack checking (provides TTC_TASK_RETURN() macro)
#include "../ttc_radio.h"             // generic radio transmitter with ranging capability
#include "rtls_common.h"              // generic functions shared by low-level drivers of all architectures
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_rtls_features rtls_square4_Features = {

    .MinimumAnchorNodes = 2,
    .MaximumAnchorNodes = 4,
    .AnchorNodes1D = 1,
    .AnchorNodes2D = 1,
    .AnchorNodes3D = 0
};

// statically assign protocol type (unique type being registered in packet_common_types.h:e_ttc_packet_pattern)
const e_ttc_packet_pattern rtls_square4_SocketID = E_ttc_packet_pattern_socket_Radio_RTLS_Simple4;
//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

/** report stored locations of all anchor nodes to given remote node
 *
 * Note: This function will block until all locations have been transmitted successfully.
 *
 * @param Config    (t_ttc_rtls_config*)  Configuration of rtls device
 * @param NodeIndex (t_u16)                    >0: index number of node to report location of
 * @return  ==TRUE: locations have been reported successfully; == FALSE: timeout occured while trying to contact remote node
 */
BOOL _rtls_square4_anchor_report_locations( t_ttc_rtls_config* Config, t_u16 NodeIndex );

/** delivers individual location into given structure for rtls_common_statemachine_report_locations()
 *
 * @param Config    (t_ttc_rtls_config*)       Configuration of rtls device
 * @param NodeIndex (t_u16)                         >0: index number of node to report location of
 * @param Location  (t_rtls_common_location*)  points to storage to be filled by getLocation()
 * @return Fields of *Location have been loaded with data.
 *
 */
void _rtls_square4_getLocation( t_ttc_rtls_config* Config, t_u16 NodeIndex, t_rtls_common_location* Location );

/** stores node locations in given packet in local slam model
 *
 * @param Config           (t_ttc_rtls_config*)                      Configuration of rtls device
 * @param LocationsReport  (t_rtls_common_packet_locations_report*)  points to packet payload reporting one or more locations
 *
 */
void _rtls_square4_storeLocations( t_ttc_rtls_config* Config, t_rtls_common_packet_locations_report* LocationsReport );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

void                  rtls_square4_anchor_buildup_start( t_ttc_rtls_config* Config ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS_EXTRA_Writable( Config->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    Assert_RTLS( ttc_slam_get_configuration( Config->ConfigSLAM->LogicalIndex )->Init.Amount_Nodes > 1, ttc_assert_origin_auto ); // at least 2 anchor nodes required (set amount of anchor nodes to 2..4 before calling ttc_slam_init()!)
    t_rtls_square4_config* Config_Simple4 = &( Config->LowLevelConfig->square4 ); ( void ) Config_Simple4;

    // Building coordinate system using super simple setup supporting only 4 anchor nodes.
    //?Used t_u8 IndexRadio = Config->ConfigRadio->LogicalIndex;

    switch ( Config->LocalID16 ) {
        case simple_2d_index_southpole: { // this is southpole: initiate coordinate system build up

            rtls_square4_reset( Config ); // reset coordinate system data
            Config_Simple4->Distance_Southpole2Northpole_cm = rtls_common_measure_range( Config,
                                                                                         Config->Anchor.RadioUserID,
                                                                                         simple_2d_index_northpole,
                                                                                         Config_Simple4->Amount_RangeMeasures,
                                                                                         1000, // 1 second timeout
                                                                                         5
                                                                                       );

            if ( Config_Simple4->Distance_Southpole2Northpole_cm ) { // calculate coordinate system for 4 anchor nodes
                /* Super simple coordinate system setup supporting only 4 anchor nodes at certain positions.
                 *
                 * This super simple coordinate system requires no complex
                 * software protocol to calculate the positions of all nodes.
                 * Everything is based on an exact distance measure between
                 * Southpole and Northpole.
                 *
                 * Restrains:
                 * - Distance(#1, #2) must be same as Distance(#4,#5).
                 * - Nodes #4,#5 must be placed centered between #1,#2.
                 * - only 2..4 anchor nodes
                 *
                 *                     Northpole
                 *                       #2
                 *                        |
                 *                        |
                 *                        |
                 *                        |
                 *       West Node        |          East Node
                 *            #4----------+----------#5
                 *                        |
                 *                        |
                 *                        |
                 *                        |
                 *                        |
                 *                       #1
                 *                     Southpole
                 *
                 */
                ttc_slam_update_coordinates( Config->ConfigSLAM->LogicalIndex,
                                             simple_2d_index_southpole,
                                             0,  // X
                                             0,  // Y
                                             0   // Z
                                           );
                ttc_slam_update_coordinates( Config->ConfigSLAM->LogicalIndex,
                                             simple_2d_index_northpole,
                                             0,
                                             Config_Simple4->Distance_Southpole2Northpole_cm,
                                             0
                                           );
                t_u32 HalfDistance = Config_Simple4->Distance_Southpole2Northpole_cm / 2;
                if ( ttc_slam_get_configuration( Config->ConfigSLAM->LogicalIndex )->Init.Amount_Nodes > 2 ) { // define position of east node #3
                    ttc_slam_update_coordinates( Config->ConfigSLAM->LogicalIndex,
                                                 3,
                                                 HalfDistance,
                                                 HalfDistance,
                                                 0
                                               );
                }
                if ( ttc_slam_get_configuration( Config->ConfigSLAM->LogicalIndex )->Init.Amount_Nodes > 3 ) { // define position of west node #4
                    ttc_slam_update_coordinates( Config->ConfigSLAM->LogicalIndex,
                                                 4,
                                                 -HalfDistance,
                                                 HalfDistance,
                                                 0
                                               );
                }

                rtls_common_anchor_coordinate_system_complete( Config, 1 );
            }
            else { // could not measure distance to northpole: will have to retry
                rtls_common_anchor_coordinate_system_complete( Config, 0 );
            }
            if ( Config->Flags.IsCoordinateSystemReady ) { // send coordinate system to all other anchor nodes
                t_u16 AmountAnchorNodes = ttc_slam_get_configuration( Config->ConfigSLAM->LogicalIndex )->Init.Amount_Nodes;
                if ( AmountAnchorNodes > 4 )
                { AmountAnchorNodes = 4; }  // this driver only supports 2..4 anchor nodes

                for ( t_u16 NodeIndex = simple_2d_index_northpole; NodeIndex <= AmountAnchorNodes; ) {
                    volatile BOOL LocationReported = _rtls_square4_anchor_report_locations( Config, NodeIndex );

                    if ( LocationReported )
                    { NodeIndex++; } // locations reported successfully: proceed to next node
                }
            }
            break;
        }
        default: {                        // this is northpole or other anchor node: wait for southpole to contact us
            if ( Config_Simple4->Delay.TimeStart == 0 ) { // first call: initialize delay
                rtls_square4_reset( Config );                             // reset coordinate system data
                ttc_systick_delay_init( &( Config_Simple4->Delay ), 10000000 ); // 10 second delay until we request rebuild of coordinate system
            }
            if ( ttc_systick_delay_expired( &( Config_Simple4->Delay ) ) ) { // delay expired: request rebuild of coordinate system
                t_ttc_rtls_packet PacketTX_Payload;
                PacketTX_Payload.Header.Type = ttc_rtls_packet_2anchor_builtup_start;
                Assert_RTLS( Config->Init.AnchorIDs[0].Address16 == simple_2d_index_southpole, ttc_assert_origin_auto ); // following line requires that node address of southpole is stored in AnchorIDs[0]. Change order of AnchorIDs[] entries!

                // using this handy transmit function which will do memory allocation and initialization for us
                ttc_radio_transmit_buffer( Config->Init.Index_Radio,
                                           Config->Anchor.RadioUserID,
                                           Config->Anchor.SocketJob.Init.Pattern,
                                           //X E_ttc_packet_pattern_socket_Radio_RTLS_Simple4,
                                           E_ttc_packet_type_802154_Data_001010,
                                           ( t_u8* ) &PacketTX_Payload,
                                           sizeof( PacketTX_Payload.Header ),
                                           &( Config->Init.AnchorIDs[0] ), // sending to southpole (which must be stored in first entry!)
                                           0,                              // sending immediately
                                           TRUE                            // use 3-way handshakes to ensure safe delivery
                                         );

                // pseudo random delay until next rebuild request (5 - 21.4 second delay)
                t_u32 DelayUS = ( ttc_random_generate32() & ( ( 1 << 24 ) - 1 ) ) + 5000000;
                ttc_systick_delay_init( &( Config_Simple4->Delay ), DelayUS );
            }

            break;
        }
    }
}
void                  rtls_square4_anchor_statemachine( t_ttc_rtls_config* Config ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS_EXTRA_Writable( Config->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!

    t_rtls_square4_config* Config_Simple4 = &( Config->LowLevelConfig->square4 ); ( void ) Config_Simple4;

    Config->Anchor.State = state_s2d_anchor_Ready; // this simple protocol does not use the state variable. But we must set it to avoid assert failure in ttc_rtls_statemachine()

    t_u8 IndexRadio = Config->ConfigRadio->LogicalIndex;
    t_ttc_packet* PacketRX = ttc_radio_packet_received_tryget( IndexRadio, Config->Anchor.RadioUserID, &( Config->Anchor.SocketJob ) );

    if ( PacketRX ) { // found packet in receive queue: check if we can use it
        t_ttc_rtls_packet*    PacketRX_Payload;
        t_u16                 PacketRX_PayloadMaxSize;
        e_ttc_packet_pattern  PacketRX_Protocol;

        ttc_packet_payload_get_pointer( ( t_ttc_packet* ) PacketRX, ( t_u8** ) &PacketRX_Payload, &PacketRX_PayloadMaxSize, &PacketRX_Protocol );
        Assert_RTLS( PacketRX_Protocol == Config->Anchor.SocketJob.Init.Pattern, ttc_assert_origin_auto ); // we received a packet with different protocol. Check implementation of ttc_radio_packet_received_tryget()!

        if ( ( PacketRX_Payload->Header.Type > ttc_rtls_packet_generic ) &&
                ( PacketRX_Payload->Header.Type < ttc_rtls_packet_2mobile )
           ) { // Received packet is for us: process packet

            switch ( PacketRX_Payload->Header.Type ) {
                case ttc_rtls_packet_2anchor_builtup_start: {    // remote node requests to rebuild coordinate system
                    ttc_radio_packet_release( PacketRX ); // give back packet buffer for reuse now (may be required during buildup process)
                    PacketRX = NULL;
                    rtls_square4_anchor_buildup_start( Config );
                    break;
                }
                case ttc_rtls_packet_generic_report_locations: { // sender is sending anchor node positions: store positions
                    _rtls_square4_storeLocations( Config, &( PacketRX_Payload->Union.Common.LocationsReport ) );
                    break;
                }
                default: break;
            }
        }
        if ( PacketRX )
        { ttc_radio_packet_release( PacketRX ); } // give back packet buffer for reuse
    }
    else {            // nothing received: check other stuff
        if ( !Config->Flags.IsCoordinateSystemReady ) {           // got no coordinate system: start built up process
            rtls_square4_anchor_buildup_start( Config );
        }
    }
}
void                  rtls_square4_configuration_check( t_ttc_rtls_config* Config ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS_EXTRA_Writable( Config->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!

    t_rtls_square4_config* Config_Simple4 = &( Config->LowLevelConfig->square4 ); ( void ) Config_Simple4;


}
e_ttc_rtls_errorcode  rtls_square4_deinit( t_ttc_rtls_config* Config ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS_EXTRA_Writable( Config->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!

    t_rtls_square4_config* Config_Simple4 = &( Config->LowLevelConfig->square4 ); ( void ) Config_Simple4;

    rtls_square4_reset( Config );
    return ( e_ttc_rtls_errorcode ) 0;
}
e_ttc_rtls_errorcode  rtls_square4_init( t_ttc_rtls_config* Config ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS_EXTRA_Writable( Config->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!

    t_rtls_square4_config* Config_Simple4 = &( Config->LowLevelConfig->square4 ); ( void ) Config_Simple4;

    // configure protocol type to use
    // Important: Protocol must be same value as on all other nodes!
    //            This is only guaranteed, if implementation of other nodes calls ttc_radio_socket_new() in same order as we.
    //            If you get different Protocol values, consider to assign them statically.

#if TTC_RTLS_IMPLEMENT_ANCHOR == 1
    Config->Anchor.SocketJob.Init.Pattern    = rtls_square4_SocketID;
    Config->Anchor.SocketJob.Init.Initialized = FALSE;
#endif
#if TTC_RTLS_IMPLEMENT_MOBILE == 1
    Config->Mobile.SocketJob.Init.Pattern    = rtls_square4_SocketID;
    Config->Mobile.SocketJob.Init.Initialized = FALSE;
#endif

    return ( e_ttc_rtls_errorcode ) 0;
}
e_ttc_rtls_errorcode  rtls_square4_load_defaults( t_ttc_rtls_config* Config ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = ta_rtls_square4;         // set type of architecture
    Config->Features     = &rtls_square4_Features;  // assign feature set

    // allocate memory for square4 specific configuration
    Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_ttc_rtls_architecture ) );

    Config->LowLevelConfig->square4.Amount_RangeMeasures = 20;

    TTC_TASK_RETURN( ec_rtls_OK ); // will perform stack overflow check + return value
}
void                  rtls_square4_mobile_statemachine( t_ttc_rtls_config* Config ) {
    Assert_RTLS_EXTRA_Writable( Config,                 ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS_EXTRA_Writable( Config->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!
    Assert_RTLS_EXTRA_Writable( Config->ConfigRadio,    ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!

    Config->Anchor.State = state_s2d_anchor_Ready; // this simple protocol does not use the state variable. But we must set it to avoid assert failure in ttc_rtls_statemachine()
    t_rtls_square4_config* Config_Simple4 = &( Config->LowLevelConfig->square4 ); ( void ) Config_Simple4;

    t_u8 IndexRadio = Config->ConfigRadio->LogicalIndex;
    t_ttc_packet* PacketRX = ttc_radio_packet_received_tryget( IndexRadio, Config->Mobile.RadioUserID, &( Config->Mobile.SocketJob ) );
    if ( PacketRX ) { // found packet in receive queue: check if we can use it
        t_ttc_rtls_packet*     PacketRX_Payload;
        t_u16                  PacketRX_PayloadMaxSize;
        e_ttc_packet_pattern  PacketRX_Protocol;

        ttc_packet_payload_get_pointer( ( t_ttc_packet* ) PacketRX, ( t_u8** ) &PacketRX_Payload, &PacketRX_PayloadMaxSize, &PacketRX_Protocol );
        Assert_RTLS( PacketRX_Protocol == Config->Anchor.SocketJob.Init.Pattern, ttc_assert_origin_auto ); // we received a packet with different protocol. Check implementation of ttc_radio_packet_received_tryget()!
        if (
            ( ( PacketRX_Payload->Header.Type > ttc_rtls_packet_generic ) &&
              ( PacketRX_Payload->Header.Type < ttc_rtls_packet_2anchor )
            ) // it's a generic rtls packet
            ||
            (
                ( PacketRX_Payload->Header.Type > ttc_rtls_packet_2mobile ) &&
                ( PacketRX_Payload->Header.Type < ttc_rtls_packet_unknown )
            ) // it's a rtls packet to a mobile node
        ) { // seems to be a packet for us: process it

            switch ( PacketRX_Payload->Header.Type ) {
                case ttc_rtls_packet_generic_report_locations: { // store reported anchor node positions
                    _rtls_square4_storeLocations( Config, &( PacketRX_Payload->Union.Common.LocationsReport ) );
                    break;
                }
                default: break;
            }
        }
        ttc_radio_packet_release( PacketRX ); // give back packet buffer for reuse
    }
}
void                  rtls_square4_prepare() {
    if ( !ttc_random_is_seeded() ) // random generator not yet initialized (seeded): use current time as seed value (not the best seed)
    { ttc_random_seed( ttc_systick_get_elapsed_ticks() ); }
}
void                  rtls_square4_reset( t_ttc_rtls_config* Config ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS_EXTRA_Writable( Config->LowLevelConfig, ttc_assert_origin_auto ); // low-level configuration was not allocated (or pointer got corrupted). Check implementation!

    t_rtls_square4_config* Config_Simple4 = &( Config->LowLevelConfig->square4 ); ( void ) Config_Simple4;
    Config_Simple4->Distance_Southpole2Northpole_cm = 0;
    rtls_common_anchor_coordinate_system_complete( Config, 0 );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

void _rtls_square4_getLocation( t_ttc_rtls_config* Config, t_u16 ArrayIndex, t_rtls_common_location* Location ) {
    t_u16 NodeIndex = ArrayIndex + 1;
    t_ttc_math_vector3d_xyz* Position = ttc_slam_get_node_position( Config->ConfigSLAM->LogicalIndex, NodeIndex );
    Location->NodeID = NodeIndex;
    Location->Position.X = Position->X;
    Location->Position.Y = Position->Y;
    Location->Position.Z = Position->Z;
}
BOOL _rtls_square4_anchor_report_locations( t_ttc_rtls_config* Config, t_u16 NodeIndex ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS( NodeIndex > 0, ttc_assert_origin_auto ); // valid node index > 0. Check implementation of caller!
    TODO( "Turn this function into a statemachine to avoid blocking!" )

    BOOL ReportSuccessfull = FALSE;
    // use a single range measure to see if remote node is listening
    //X t_u32 Distance2Node = rtls_common_measure_range( Config, Config->Anchor.RadioUserID, NodeIndex, 1, 1000, 5 );
    //X ttc_radio_receiver( Config->Init.Index_Radio, Config->Anchor.RadioUserID, TRUE, NULL, 0 ); // reenable receiver

    //X if ( Distance2Node ) { // anchor node found: send locations
    if ( 1 ) { // use rtls_common_statemachine_report_locations() to transmit locations to this anchor node
        t_rtls_common_data_report_locations ReportData;
        ReportData.Init.getLocation     = ( void ( * )( void*, t_u16, t_rtls_common_location* ) ) _rtls_square4_getLocation;
        ReportData.Init.LocationsAmount = Config->Init.Amount_AnchorIDs; // reporting locations of all anchor nodes
        ReportData.Init.LocationsOffset = 0;                             // report from first node (node #i is stored in AnchorIDs[i-1])
        ReportData.Init.ReferenceTime   = NULL;
        ReportData.Init.State           = 0;
        ReportData.Init.TargetIDs       = &( Config->Init.AnchorIDs[NodeIndex - 1] ); // target address of indexed node
        ReportData.Init.TargetIDsAmount = 1;

        // run statemachine until all locations have been transmitted
        while ( rtls_common_statemachine_report_locations( Config, Config->Anchor.RadioUserID, &ReportData ) );

        ReportSuccessfull = ReportData.Status.TransmitSuccessful;
    }
    else {     // directly send out hard coded location report (DEPRECATED)

        // obtaining access to payload area of new packet
        t_u16                   PayloadSize = 0;    // start of payload depends on packet type
        t_ttc_rtls_packet* Payload     = NULL; // will be loaded with pointer to payload area

        Assert_RTLS( NodeIndex <= Config->Init.Amount_AnchorIDs, ttc_assert_origin_auto ); // given node index is outside of AnchorIDs[]. Check your configuration!
        t_ttc_packet* PacketTX = rtls_common_packet_get_empty_try( Config,
                                                                   &Payload,
                                                                   &PayloadSize,
                                                                   &( Config->Init.AnchorIDs[NodeIndex - 1] ),
                                                                   E_ttc_packet_type_802154_Data_001010,
                                                                   Config->Anchor.SocketJob.Init.Pattern
                                                                   //X E_ttc_packet_pattern_socket_Radio_RTLS_Simple4
                                                                 );
        if ( !PacketTX )
        { return FALSE; }

        Assert_RTLS( PayloadSize >= sizeof( t_rtls_common_packet_locations_report ) + 4 * sizeof( t_rtls_common_location ), ttc_assert_origin_auto ); // packet does not have egnough space for 4 locations. Check implementation and configuration!
        t_u16 AmountAnchorNodes = ttc_slam_get_configuration( Config->ConfigSLAM->LogicalIndex )->Init.Amount_Nodes;
        if ( AmountAnchorNodes > 4 )
        { AmountAnchorNodes = 4; }  // this driver only supports 2..4 anchor nodes

        // Fill Payload with locations report
        t_rtls_common_packet_locations_report* LocationsReport = &( Payload->Union.Common.LocationsReport );
        LocationsReport->LocationsAmount = AmountAnchorNodes;
        LocationsReport->PacketAmount    = 1;
        LocationsReport->PacketNumber    = 1;

        t_u8 IndexSLAM = Config->ConfigSLAM->LogicalIndex;
        t_ttc_slam_node* Node;

        Node = ttc_slam_get_node( IndexSLAM, simple_2d_index_southpole );
        LocationsReport->Locations[0].NodeID = simple_2d_index_southpole;
        LocationsReport->Locations[0].Position.X = Node->Position.X;
        LocationsReport->Locations[0].Position.Y = Node->Position.Y;
        LocationsReport->Locations[0].Position.Z = Node->Position.Z;

        Node = ttc_slam_get_node( IndexSLAM, simple_2d_index_northpole );
        LocationsReport->Locations[1].NodeID = simple_2d_index_northpole;
        LocationsReport->Locations[1].Position.X = Node->Position.X;
        LocationsReport->Locations[1].Position.Y = Node->Position.Y;
        LocationsReport->Locations[1].Position.Z = Node->Position.Z;
        if ( AmountAnchorNodes > 2 ) {
            Node = ttc_slam_get_node( IndexSLAM, 3 );
            LocationsReport->Locations[2].NodeID = 3;
            LocationsReport->Locations[2].Position.X = Node->Position.X;
            LocationsReport->Locations[2].Position.Y = Node->Position.Y;
            LocationsReport->Locations[2].Position.Z = Node->Position.Z;
        }
        if ( AmountAnchorNodes > 3 ) {
            Node = ttc_slam_get_node( IndexSLAM, 4 );
            LocationsReport->Locations[3].NodeID = 4;
            LocationsReport->Locations[3].Position.X = Node->Position.X;
            LocationsReport->Locations[3].Position.Y = Node->Position.Y;
            LocationsReport->Locations[3].Position.Z = Node->Position.Z;
        }
    }

    // ignoring received packets is important to ensure egnough memory buffers to operate!
    ttc_radio_packets_received_ignore( Config->Init.Index_Radio, Config->Anchor.RadioUserID, Config->Anchor.SocketJob.Init.Pattern );


    return ReportSuccessfull;
}
void _rtls_square4_storeLocations( t_ttc_rtls_config* Config, t_rtls_common_packet_locations_report* LocationsReport ) {
    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_RTLS_EXTRA_Writable( LocationsReport, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    t_u8 IndexSLAM = Config->ConfigSLAM->LogicalIndex;

    t_rtls_common_location* Location = LocationsReport->Locations;
    for ( t_u8 IndexLocation = 0; IndexLocation < LocationsReport->LocationsAmount; IndexLocation++ ) {

        ttc_slam_update_coordinates( IndexSLAM,
                                     Location->NodeID,
                                     Location->Position.X,
                                     Location->Position.Y,
                                     Location->Position.Z
                                   );
        Location++;
    }
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
