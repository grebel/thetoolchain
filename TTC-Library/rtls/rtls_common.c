/** rtls_common.c ***********************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to rtls low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.c revision 12 at 20161208 10:11:47 UTC
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * Put all includes here that are required to compile this source code.
 */

#include "rtls_common.h"
#include "../ttc_packet.h"
#include "../ttc_radio.h"
#include "../ttc_heap_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)
//}PrivateFunctions
/** Function Definitions ********************************************{
 */

t_ttc_packet* rtls_common_packet_get_empty_try( t_ttc_rtls_config* Config, t_ttc_rtls_packet** Payload, t_u16* PayloadMaxSize, const t_ttc_packet_address* TargetAddress, e_ttc_packet_type Type, e_ttc_packet_pattern Protocol ) {
    Assert_RTLS_Writable( Config,      ttc_assert_origin_auto ); // always check incoming pointer arguments

    // try to get new empty packet from radio memory pool (will block if pool is empty)
    t_ttc_packet* EmptyPacket = ttc_radio_packet_get_empty_try( Config->Init.Index_Radio, Type, Protocol );

    if ( EmptyPacket ) {

#if TTC_HEAP_POOL_STATISTICS == 1
        t_ttc_heap_block_from_pool* MemoryBlock = ( t_ttc_heap_block_from_pool* ) EmptyPacket;
        // storing address of function call in block header allows to find out who did allocate this memory block
        ( MemoryBlock - 1 )->MyOwner = __builtin_return_address( 0 );
#endif

        // address packet
        E_ttc_packet_address_source_set( EmptyPacket, Config->Init.LocalID );
        if ( TargetAddress )
        { ttc_packet_address_target_set( EmptyPacket, TargetAddress ); }

        if ( Payload ) { // obtain pointer to payload area
            Assert_RTLS_Writable( Payload,     ttc_assert_origin_auto ); // given address must be writable. Check provided argument!
            Assert_RTLS_Writable( PayloadMaxSize, ttc_assert_origin_auto ); // given address must be writable. Check provided argument!
            ttc_packet_payload_get_pointer( EmptyPacket, ( t_u8** ) Payload, PayloadMaxSize, NULL );
            //X Assert_RTLS(*PayloadSize >= sizeof(t_rtls_common_packet) + sizeof(PacketTX_RTLS->Header.Type), ttc_assert_origin_auto); // current packet size provided by current radio is too small. Check radio configuration!
        }
    }

    return EmptyPacket;
}
t_u32         rtls_common_measure_range( t_ttc_rtls_config* Config, t_u8 RadioUserID, VOLATILE_RADIO t_u16 NodeIndex, VOLATILE_RADIO t_u16 AmountMeasures, VOLATILE_RADIO t_u32 TimeOutMS, VOLATILE_RADIO t_u8 MaxRetries ) {
    Assert_RTLS_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments

    if ( ! ttc_radio_user_lock( Config->Init.Index_Radio, RadioUserID ) )
    { return 0; } // could not obtain exclusive lock on ttc_radio

    // construct remote node address from our own address
    t_ttc_packet_address NodeAddress;
    ttc_memory_copy( &NodeAddress, &( Config->ConfigRadio->LocalID ), sizeof( NodeAddress ) );
    NodeAddress.Address16 = NodeIndex;
    NodeAddress.Address64.Bytes[6] = ( t_u8 )( ( 0xff00 & NodeIndex ) >> 8 );
    NodeAddress.Address64.Bytes[7] = ( t_u8 )( 0x00ff & NodeIndex );
    Assert_RTLS_EXTRA( ( NodeAddress.Address64.Bytes[7] ) == ( 0xff & NodeIndex ), ttc_assert_origin_auto ); // implementation in previous line does not seem to work. Check it!

    t_ttc_radio_distance RangeMeasure;
    ttc_memory_set( ( void* ) &RangeMeasure, 0, sizeof( RangeMeasure ) );

    t_u32 Distance_cm = 0;
    VOLATILE_RADIO t_u8 Retries = MaxRetries;

    for ( t_u8 Times = AmountMeasures; Times > 0; ) { // issue range measure multiple times (only works if northpole is listening)
        VOLATILE_RADIO t_u8 AmountReplies = ttc_radio_ranging_request( 1,
                                                                       RadioUserID,
                                                                       rcrt_Request_Ranging_SSTOFCR,
                                                                       1,
                                                                       &NodeAddress,
                                                                       TimeOutMS,
                                                                       0,
                                                                       &RangeMeasure,
                                                                       NULL,
                                                                       FALSE
                                                                     );

        if ( AmountReplies && RangeMeasure.Distance_cm ) {
            if ( Distance_cm ) { // calculate average in a way that avoids overrun
                Distance_cm = ( Distance_cm / 2 ) +
                              ( RangeMeasure.Distance_cm  / 2 );
            }
            else {               // first measure: simply copy value
                Distance_cm = RangeMeasure.Distance_cm;
            }
            Times--;
            Retries = MaxRetries; // reset retries
        }
        else {     // ranging request was not successfull: retry
            if ( Retries )
            { Retries--; }
            else { // max retries exceeded: abort ranging measure
                Distance_cm = 0;
                break;
            }
        }
    }

    ttc_radio_user_unlock( Config->Init.Index_Radio, RadioUserID ); // re-allow accedd to ttc_radio for other users
    return Distance_cm;
}
BOOL          rtls_common_statemachine_report_distances( t_ttc_rtls_config* Config, t_u8 RadioUserID, t_rtls_common_data_report_distances* Data ) {
    TODO( "Replace implementation of rtls_common_statemachine_report_distances() by generic report function!  (doubled implementation)" )
    Assert_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_Writable( Data,   ttc_assert_origin_auto ); // always check incoming pointer arguments

    //scsrd_statemachine_switch:
    switch ( Data->Init.State ) {
        case 0:
            Data->Init.State = tss_Report_Distances_Start;
        case tss_Report_Distances_Start: {    // prepare statemachine to start transmission
            Assert_RTLS( Data->Init.DistancesAmount > 0, ttc_assert_origin_auto ); // caller must set this. Check description of struct field!
            Assert_RTLS_Readable( Data->Init.getDistance, ttc_assert_origin_auto ); // caller must set this. Check description of struct field!

            if ( ttc_radio_user_lock( Config->Init.Index_Radio, RadioUserID ) ) { // obtained exclusive lock on ttc_radio
                Data->priv.IndexDistances = 0;
                Data->priv.IndexTargetIDs = 0;
                Data->priv.PacketNumber   = 0;
                Data->priv.PacketAmount   = 0; // will be calculated after allocating first packet
                goto scsrd_state_distances_transmit;
            }
            break;
        }
    scsrd_state_distances_transmit:
        Data->Init.State          = tss_Report_Distances_Transmit;

        case tss_Report_Distances_Transmit: { // transmit one packet after another to all distances
            t_ttc_rtls_packet* Payload;
            t_u16                   PayloadMaxSize;

            t_ttc_packet* PacketTX;
            const t_ttc_packet_address* TargetAddress;
            e_ttc_packet_type PacketType;
            if ( Data->Init.TargetIDsAmount == 0 ) { // no target ids: obtain broadcast packet
                TargetAddress = NULL;
                PacketType = E_ttc_packet_type_802154_Data_000010; // data packet with 16 bit source- and no target-address
            }
            else {                                   // obtain unicast packet targeted at current TargetID
                TargetAddress = & ( Data->Init.TargetIDs[Data->priv.IndexTargetIDs] );
                PacketType = E_ttc_packet_type_802154_Data_001010; // data packet with 16 bit source- and 16 bit target-address
            }
            PacketTX = rtls_common_packet_get_empty_try( Config,
                                                         &Payload,
                                                         &PayloadMaxSize,
                                                         TargetAddress,
                                                         PacketType,
                                                         E_ttc_packet_pattern_socket_Radio_RTLS
                                                       );

            Assert_RTLS_Writable( PacketTX, ttc_assert_origin_auto ); // could not obtain empty packet: check implementation (maybe this assert is superfluos)!
            if ( PacketTX ) { // got a valid packet buffer: fill and transmit it

                if ( Data->priv.PacketAmount == 0 ) { // calculate total amount of packets to transmit
                    Data->priv.DistancesPerPacket = ( PayloadMaxSize + sizeof( t_rtls_common_distance ) - 1 )   /
                                                    sizeof( t_rtls_common_distance );
                    Data->priv.PacketAmount      = ( Data->Init.DistancesAmount + Data->priv.DistancesPerPacket - 1 ) /
                                                   Data->priv.DistancesPerPacket;
                }
                PacketTX->Meta.ReceiveAfterTransmit = 0;

                // fill protocol specific payload area
                Payload->Header.Type = ttc_rtls_packet_generic_report_distances;
                t_rtls_common_packet_distances_report* Report = & ( Payload->Union.Common.DistanceReport );

                // calculate how much distances to store in packet
                t_u8 DistancesInPacket = Data->priv.DistancesPerPacket;
                if ( Data->priv.IndexDistances + DistancesInPacket > Data->Init.DistancesAmount ) { // not egnough distances left to fill packet
                    DistancesInPacket = Data->Init.DistancesAmount - Data->priv.IndexDistances;
                }

                // calculate actual size of packet payload
                t_u16 PayloadSize = sizeof( *Report ) +
                                    ( DistancesInPacket * sizeof( t_rtls_common_distance ) );

                // given getDistance() will copy each distance into packet
                t_rtls_common_distance* Writer = &( Report->Distances[0] );
                for ( volatile t_u16 Index = Data->priv.IndexDistances; Index < Data->priv.IndexDistances + DistancesInPacket; Index++ ) { // ToDo: remove volatile!
                    Data->Init.getDistance( Config, Data->Init.DistancesOffset + Index, Writer );
                    Assert_RTLS( Writer->NodeID_A > 0, ttc_assert_origin_auto ); // invalid node index (1..Amount_Nodes). Check *getDistance() implementation!
                    Assert_RTLS( Writer->NodeID_B > 0, ttc_assert_origin_auto ); // invalid node index (1..Amount_Nodes). Check *getDistance() implementation!
                    Assert_RTLS( Writer->NodeID_A != Writer->NodeID_B, ttc_assert_origin_auto ); // It makes no sense to report distance within same node. Check *getDistance() implementation!
                    Writer++;
                }
                Data->priv.IndexDistances += DistancesInPacket;

                Report->PacketNumber    = ++Data->priv.PacketNumber;
                Report->PacketAmount    = Data->priv.PacketAmount;
                Report->DistancesAmount = DistancesInPacket;

                ttc_radio_transmit_packet_now( Config->Init.Index_Radio,
                                               RadioUserID,
                                               PacketTX,
                                               PayloadSize,
                                               FALSE
                                             );

                if ( Data->priv.PacketNumber == Data->priv.PacketAmount ) { // all packets sent for one target: proceed to next target
                    Data->priv.IndexTargetIDs++;
                    if ( Data->priv.IndexTargetIDs >= Data->Init.TargetIDsAmount ) { // all target ids have been processed: complete statemachine
                        goto scsrd_state_distances_complete;
                    }

                    // prepare for next target id
                    Data->priv.PacketNumber   = 0;
                    Data->priv.IndexDistances = 0;
                }
            }

            break;
        }
    scsrd_state_distances_complete:
        Data->Init.State = tss_Report_Distances_Complete;
#if (TTC_ASSERT_RTLS == 1)
            // reset arguments (allows argument check on next tss_Report_Distances_Start state)
        Data->Init.DistancesAmount = 0;
        Data->Init.getDistance     = NULL;
#endif

        case tss_Report_Distances_Complete: { // transmission of distance report completed
            // switch on receiver
            ttc_radio_receiver( Config->Init.Index_Radio, RadioUserID, TRUE, NULL, 0 );
            ttc_radio_user_unlock( Config->Init.Index_Radio, RadioUserID ); // re-allow accedd to ttc_radio for other users

            Data->Init.State = tss_Report_Distances_Idle;
            // no break here but fallthrough to idle state..
        }
        case tss_Report_Distances_Idle: {
            return FALSE; // caller must not call us again
        }
        default: Assert_RTLS( 0, ttc_assert_origin_auto ); break; // illegal/ unkown state: check implementation!
    }

    return TRUE; // statemachine function must be called again
}
BOOL          rtls_common_statemachine_report_locations( t_ttc_rtls_config* Config, t_u8 RadioUserID, t_rtls_common_data_report_locations* Data ) {
    TODO( "Replace implementation of rtls_common_statemachine_report_locations() by generic report function! (doubled implementation)" )
    Assert_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_Writable( Data,   ttc_assert_origin_auto ); // always check incoming pointer arguments

    switch ( Data->Init.State ) {
        case 0:
            Data->Init.State = tss_Report_Locations_Start;
        case tss_Report_Locations_Start: {    // prepare statemachine to start transmission
            Assert_RTLS( Data->Init.LocationsAmount > 0, ttc_assert_origin_auto ); // caller must set this. Check description of struct field!
            Assert_RTLS_Readable( Data->Init.getLocation, ttc_assert_origin_auto ); // caller must set this. Check description of struct field!

            if ( ttc_radio_user_lock( Config->Init.Index_Radio, RadioUserID ) ) { // obtained exclusive lock on ttc_radio
                ttc_memory_set( &Data->priv, 0, sizeof( Data->priv ) );
                Data->priv.Amount_TxRetries      = Config->Init.Amount_TxRetries;
                Data->Status.TransmitSuccessful = FALSE;
                goto scsrd_state_locations_transmit;
            }
            break;
        }

    scsrd_state_locations_transmit:
        Data->Init.State          = tss_Report_Locations_Transmit;

        case tss_Report_Locations_Transmit: { // transmit one packet after another to all target nodes
            t_ttc_rtls_packet* Payload;
            t_u16              PayloadMaxSize;

            if ( !Data->priv.PacketTX ) { // obtain + initialize memory block for new packet to be sent
                const t_ttc_packet_address* TargetAddress;
                e_ttc_packet_type PacketType;
                if ( Data->Init.TargetIDsAmount == 0 ) { // no target ids: obtain broadcast packet
                    TargetAddress = NULL;
                    PacketType = E_ttc_packet_type_802154_Data_000010; // data packet with 16 bit source- and no target-address
                }
                else {                              // obtain unicast packet targeted at current TargetID
                    TargetAddress = & ( Data->Init.TargetIDs[Data->priv.IndexTargetIDs] );
                    PacketType = E_ttc_packet_type_802154_Data_001010; // data packet with 16 bit source- and 16 bit target-address
                }
                Data->priv.PacketTX = rtls_common_packet_get_empty_try( Config,
                                                                        &Payload,
                                                                        &PayloadMaxSize,
                                                                        TargetAddress,
                                                                        PacketType,
                                                                        E_ttc_packet_pattern_socket_Radio_RTLS
                                                                      );

                Assert_RTLS_Writable( Data->priv.PacketTX, ttc_assert_origin_auto ); // could not obtain empty packet: check implementation (maybe this assert is superfluos)!
                if ( Data->priv.PacketTX ) { // got a valid packet buffer: fill and transmit it

                    if ( Data->priv.PacketAmount == 0 ) { // calculate total amount of packets to transmit
                        Data->priv.LocationsPerPacket = ( PayloadMaxSize + sizeof( t_rtls_common_location ) - 1 )   /
                                                        sizeof( t_rtls_common_location );
                        Data->priv.PacketAmount      = ( Data->Init.LocationsAmount + Data->priv.LocationsPerPacket - 1 ) /
                                                       Data->priv.LocationsPerPacket;
                    }
                    Data->priv.PacketTX->Meta.ReceiveAfterTransmit = 1; // immediately switch on receiver after transmission to receive ACK packet
                    Data->priv.PacketTX->Meta.DoNotReleaseBuffer   = 1; // we may want to reuse this packet

                    ttc_packet_acknowledge_request_set( Data->priv.PacketTX, TRUE ); // request 2-way handshake for this packet

                    // fill protocol specific payload area
                    Payload->Header.Type = ttc_rtls_packet_generic_report_locations;
                    t_rtls_common_packet_locations_report* Report = & ( Payload->Union.Common.LocationsReport );

                    // calculate how much locations to store in packet
                    t_u8 LocationsInPacket = Data->priv.LocationsPerPacket;
                    if ( Data->priv.IndexLocations + LocationsInPacket > Data->Init.LocationsAmount ) { // not egnough locations left to fill packet
                        LocationsInPacket = Data->Init.LocationsAmount - Data->priv.IndexLocations;
                    }

                    // calculate actual size of packet payload
                    Data->priv.PacketTX_PayloadSize = sizeof( *Report ) +
                                                      ( LocationsInPacket * sizeof( t_rtls_common_location ) );

                    // given getLocation() will copy each location into packet
                    t_rtls_common_location* Writer = &( Report->Locations[0] );
                    for ( volatile t_u16 Index = Data->priv.IndexLocations; Index < Data->priv.IndexLocations + LocationsInPacket; Index++ ) { // ToDo: remove volatile!
                        Data->Init.getLocation( Config, Data->Init.LocationsOffset + Index, Writer );
                        Assert_RTLS( Writer->NodeID > 0, ttc_assert_origin_auto ); // invalid node index (1..Amount_Nodes). Check *getLocation() implementation!
                        Writer++;
                    }
                    Data->priv.IndexLocations += LocationsInPacket;

                    Report->PacketNumber    = ++Data->priv.PacketNumber;
                    Report->PacketAmount    = Data->priv.PacketAmount;
                    Report->LocationsAmount = LocationsInPacket;
                }
            }
            if ( Data->priv.PacketTX ) {  // transmit new or retransmit previous packet
                e_ttc_radio_errorcode ReturnCode = ttc_radio_transmit_packet_now( Config->Init.Index_Radio,
                                                                                  RadioUserID,
                                                                                  Data->priv.PacketTX,
                                                                                  Data->priv.PacketTX_PayloadSize,
                                                                                  FALSE
                                                                                );

                Assert_RTLS( ReturnCode != ec_radio_IsLocked, ttc_assert_origin_auto ); // We have locked radio earlier, so this should not happen. Check general implementation!
                if ( ReturnCode == ec_radio_OK ) { // packet was sent successfully
                    if ( Data->priv.PacketNumber == Data->priv.PacketAmount ) { // all packets sent for one target: proceed to next target
                        Data->priv.IndexTargetIDs++;
                        if ( Data->priv.IndexTargetIDs >= Data->Init.TargetIDsAmount ) { // all target ids have been processed: complete statemachine
                            goto scsrd_state_locations_complete;
                        }

                        // prepare for next target id
                        Data->priv.PacketNumber   = 0;
                        Data->priv.IndexLocations = 0;
                    }

                    // allow packet buffer to be reused
                    Data->priv.PacketTX->Meta.DoNotReleaseBuffer = 0;
                    Data->priv.PacketTX = ttc_radio_packet_release( Data->priv.PacketTX );
                }
                else { goto scsrd_transmission_failed; }
            }
            else { goto scsrd_transmission_failed; }

            break;
        }

    scsrd_state_locations_complete:
        Data->Init.State = tss_Report_Locations_Complete;
#if (TTC_ASSERT_RTLS == 1)
            // reset arguments (allows argument check on next tss_Report_Locations_Start state)
        Data->Init.LocationsAmount = 0;
        Data->Init.getLocation     = NULL;
#endif

        case tss_Report_Locations_Complete: { // transmission of location report completed
            // switch on receiver
            ttc_radio_receiver( Config->Init.Index_Radio, RadioUserID, TRUE, NULL, 0 );
            ttc_radio_user_unlock( Config->Init.Index_Radio, RadioUserID ); // re-allow accedd to ttc_radio for other users
            Data->Status.TransmitSuccessful = TRUE;
            Data->Init.State = tss_Report_Locations_Idle;
            // no break here but fallthrough to idle state..
        }
        case tss_Report_Locations_Idle: {
            return FALSE; // caller must not call us again
        }
        default: Assert_RTLS( 0, ttc_assert_origin_auto ); break; // illegal/ unkown state: check implementation!
    }

    return TRUE; // statemachine function must be called again

scsrd_transmission_failed: // transmission has failed: end statemachine
    if ( Data->priv.Amount_TxRetries-- == 0 ) { // no more retries left: abort
        // allow packet buffer to be reused
        Data->priv.PacketTX->Meta.DoNotReleaseBuffer = 0;
        Data->priv.PacketTX = ttc_radio_packet_release( Data->priv.PacketTX );

        // stop statemachine
        Data->Init.State    = tss_Report_Locations_Idle;

        return FALSE; // inform caller that statemachine has ended
    }
    return TRUE;
}
void          rtls_common_anchor_coordinate_system_complete( t_ttc_rtls_config* Config, VOLATILE_RADIO BOOL Complete ) {
    Assert_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments

    if ( Complete )
    { Config->Flags.IsCoordinateSystemReady = 1; }
    else
    { Config->Flags.IsCoordinateSystemReady = 0; }

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
