#ifndef RTLS_CRTOF_SIMPLE_2D_TYPES_H
#define RTLS_CRTOF_SIMPLE_2D_TYPES_H

/** { rtls_crtof_simple_2d.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_rtls.h providing
 *  Structures, Enums and Defines being required by ttc_rtls_types.h
 *
 *  synchronous location and mapping radio protocol for a simple two dimensional anchor setup using the cascaded replies time of flight ranging protocol
 *
 *  Created from template device_architecture_types.h revision 24 at 20161208 10:46:38 UTC
 *
 *  Note: See ttc_rtls.h for description of high-level rtls implementation.
 *  Note: See rtls_crtof_simple_2d.h for description of crtof_simple_2d rtls implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_RTLS1   // device not defined in makefile
    #define TTC_RTLS1    ta_rtls_crtof_simple_2d   // example device definition for current architecture (Disable line if not desired!)
#endif
#define RTLS_CS2_IMPLEMENT_ANCHOR_NS 1  // ==1: activate source-code and data storage to operate as a north or south anchor node (see descroption of rtls_crtof_simple_2d_anchor_statemachine() for details!)
#define RTLS_CS2_IMPLEMENT_ANCHOR_WE 1  // ==1: activate source-code and data storage to operate as a west or east anchor node (see descroption of rtls_crtof_simple_2d_anchor_statemachine() for details!)
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

#define RTLS_CRTOF_SIMPLE_2D_CACHE_DISTANCES 0  // ==1: store a copy of latest integer value distances for faster replies to distances requests (increases memory usage by O(Amount Nodes)
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "../ttc_radio_types.h"
#include "../ttc_rtls_types.h"
#include "../ttc_systick_types.h"
#include "../ttc_states_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_rtls_types.h *************************

typedef struct {  // t_scs2_node_distances - distances from single node to south and north poles
    t_u32 NorthPole;  // distance to north pole
    t_u32 SouthPole;  // distance to south pole
} __attribute__( ( __packed__ ) ) t_scs2_node_distances;

/* Deprecated (protocol v1.1)
typedef struct {  // statedata_s2d_measure_ranges_t - data required by state function _scs2_anchor_substate_measure_single_range()
    t_u16  Index_RemoteAnchor;   // index of remote anchor node being processed currently
    t_u16  Amount_RangeMeasures; // amount of remaining range measures

    struct {                                              // data from received distance report
        t_u16                       SenderID16;           // 16 bit node identifier of sender being extracted from last received distance report
        //?Used e_ttc_rtls_state       ReturnState;  // state to return to after reporting distances
    } DistanceReportRX;
    struct {                                              // statemachine data for transmitting a distance request
        const t_ttc_packet_address* TargetID;             // node identifier of target node of last distance request
        t_ttc_slam_node*            RemoteNode;           // points to slam node that should represent remote node to request distance from
    } RangingRequest;
} __attribute__( ( __packed__ ) ) t_statedata_s2d_measure_single_range;
*/

typedef struct {  // statedata_s2d_measure_ranges_t - data required by state function _scs2_anchor_substate_measure_single_range()

    struct { // input params to this substate ( must be set before ttc_states_call() )
        t_u16  Index_RemoteAnchor;   // index of remote anchor node being processed currently
        t_u16  Amount_RangeMeasures; // amount of remaining range measures
    } Input;

    struct { // return values from this substate ( can be read after returning from substate)
        t_u32  Distance_cm;          // >0: averaged distance from multiple range measures
    } Return;

    struct { // private data of this substate
        const t_ttc_packet_address* TargetID;     // node identifier of target node of last distance request
        t_ttc_slam_node*            RemoteNode;   // points to slam node that should represent remote node to request distance from
        t_ttc_radio_distance        RawDistance;  // latest raw distance measure
        t_u16                       SenderID16;   // 16 bit node identifier of sender being extracted from last received distance report
    } Private;
} __attribute__( ( __packed__ ) ) t_statedata_s2d_measure_single_range;

typedef struct {  // t_statedata_s2d_build_coordinate_system - data required by state function _scs2_anchor_substate_build_coordinate_system()

    struct { // input params to this substate ( must be set before ttc_states_call() )
        t_u8 Unused;
    } Input;

    struct { // return values from this substate ( can be read after returning from substate)
        t_u8 Unused;
    } Return;

    struct { // private data of this substate
        t_u8 Unused;
    } Private;
} __attribute__( ( __packed__ ) ) t_statedata_s2d_build_coordinate_system;

typedef struct {  // t_statedata_s2d_measure_all_ranges - data required by state function _scs2_anchor_southpole_state_measure_all_ranges()
    t_u16                                Index_RemoteAnchor;   // index of remote anchor node being processed currently
    t_u16                                Amount_Remaining;     // amount of remaining range measures
    t_statedata_s2d_measure_single_range MeasureSingleRange;   // data required by state function _scs2_node_substate_measure_single_range()
} __attribute__( ( __packed__ ) ) t_statedata_s2d_measure_all_ranges;

typedef struct {  // t_statedata_s2d_request_all_ranges_to_northpole - data required by state function _scs2_anchor_southpole_state_request_all_ranges_to_northpole()
    const t_ttc_packet_address* RemoteAddress;      // packet address of remote node to which request has been sent
    t_u16                       Index_RemoteAnchor; // index of remote anchor node being processed currently
    t_u16                       Amount_Remaining;   // amount of remaining range requests
    t_u16                       SequenceNumber;     // sequence number being transmitted in latest request message

} __attribute__( ( __packed__ ) ) t_statedata_s2d_request_all_ranges_to_northpole;

typedef struct {  // t_statedata_s2d_waiting - data required by state function _scs2_anchor_other_state_waiting()
    t_u8 Unused;
} __attribute__( ( __packed__ ) ) t_statedata_s2d_waiting;

typedef struct {  // t_statedata_s2d_measure_range_to_northpole - data required by state function _scs2_anchor_other_state_measure_range_to_northpole()
    struct { // return values from this substate ( can be read after returning from substate)
        t_u32  Distance_cm;          // >0: averaged distance from multiple range measures
    } Return;
    struct {
        t_statedata_s2d_measure_single_range MeasureSingleRange;   // data required by state function _scs2_node_substate_measure_single_range()
    } Private;
} __attribute__( ( __packed__ ) ) t_statedata_s2d_measure_range_to_northpole;

typedef struct {  // t_scs2_anchor_config - low-level config variables used by rtls_crtof_simple_2d_anchor_statemachine() only

    t_ttc_states_config*                    StateMachine;     // statemachine implementing anchor node protocol
#if RTLS_CRTOF_SIMPLE_2D_CACHE_DISTANCES==1
    t_scs2_node_distances*                  NodeDistances;    // points to array of distances from each node to north and south anchor node
#endif
    union { // variables being shared by several states (saves RAM as only one state is active at a time)

        t_statedata_s2d_request_all_ranges_to_northpole RequestAllRanges;        // -> _scs2_anchor_southpole_state_request_all_ranges_to_northpole()
        t_statedata_s2d_measure_all_ranges              MeasureAllRanges;        // -> _scs2_anchor_southpole_state_measure_all_ranges()
        t_rtls_common_data_report_distances        ReportDistances;         // -> rtls_common_statemachine_report_distances()
        t_statedata_s2d_measure_range_to_northpole      MeasureRangeToNorthpole; // -> _scs2_anchor_other_state_measure_range_to_northpole()
        t_statedata_s2d_waiting                         Waiting;                 // -> _scs2_anchor_other_state_waiting()
    } Shared;
    t_ttc_systick_delay                     TimeOut;          // universal timeout delay used by anchor node statemachine
    t_u8                                    TimeOut_Retries;  // amount of remaining retries after timeout
} __attribute__( ( __packed__ ) ) t_scs2_anchor_config;

typedef struct {  // t_scs2_mobile_config - low-level config variables used by rtls_crtof_simple_2d_anchor_statemachine() only
    t_ttc_systick_delay    TimeOut;                     // universal timeout delay used by anchor node statemachine
} __attribute__( ( __packed__ ) ) t_scs2_mobile_config;

typedef struct {  // t_rtls_crtof_simple_2d_config - crtof_simple_2d specific configuration data of single rtls device

    // storage for single distance measures
    t_ttc_radio_distance LatestDistance;

#if TTC_RTLS_IMPLEMENT_ANCHOR == 1
    // configuration data of anchor node
    t_scs2_anchor_config Anchor;
#endif

#if TTC_RTLS_IMPLEMENT_MOBILE == 1
    // configuration data of mobile node
    t_scs2_mobile_config Mobile;
#endif

    // amount of received packets being ignored (corrupt or not expected)
    t_u16 Amount_PacketsRX_Ignored;

    // delay time after unsuccessfull ranging request (microseconds)
    t_u32  Delay_RangingRequestRetry_us;
} __attribute__( ( __packed__ ) ) t_rtls_crtof_simple_2d_config;

// t_ttc_rtls_architecture is required by ttc_rtls_types.h
#define t_ttc_rtls_architecture t_rtls_crtof_simple_2d_config


// special packet formats below --------------------------------------------

typedef struct { // message requesting distance to northpole from target node
    t_u16 SequenceNumber;  // distance report must copy this number
} __attribute__( ( __packed__ ) ) t_rtls_crtof_simple_2d_packet_request_distance_to_northpole;

typedef struct { // message requesting distance to northpole from target node
    t_u16 SequenceNumber;  // copied from request message
    t_u32 Distance_CM;     // distance to northpole in cm
} __attribute__( ( __packed__ ) ) t_rtls_crtof_simple_2d_packet_report_distance_to_northpole;

typedef union { // specific packet format
    t_u8 Unused;
    t_rtls_crtof_simple_2d_packet_request_distance_to_northpole RequestDistance2Northpole;
    t_rtls_crtof_simple_2d_packet_report_distance_to_northpole  ReportDistance2Northpole;

} __attribute__( ( __packed__ ) ) t_rtls_crtof_simple_2d_packet;

//} Structures/ Enums

#endif //RTLS_CRTOF_SIMPLE_2D_TYPES_H
