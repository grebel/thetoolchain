#ifndef ttc_queue_H
#define ttc_queue_H

/** { ttc_queue.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Architecture independent support for inter task communication & synchronization.
 *
 * Currently supported schedulers: FreeRTOS
 *
 */
/** Description of ttc_queue (Do not delete this line!)
 *
 *
 * ttc_queue provides two main types of queues:
 * 1) Generic Queues
 *    These are just a shallow interface to an external supplied queue implementation.
 *    The generic queue interface was developed on top of FreeRTOS queues. Other queue implementations may follow.
 *    Depending on their individual implementation, generic queues can consume lots of ram.
 *
 * 2) Simple Queues
 *    The most basic datatypes being placed in queues are single bytes and pointers. In ttc_queue, specialized
 *    implementations for queueing these simple datatypes are provided. The implementations fully support multitasking
 *    and interrupt service routines with small extra ram usage. The support for interrupt service routines is special.
 *    When an _isr() function should operate on a simple queue, it can happen that this queue is currently in use by a
 *    task. This is indicated by Lock==1. In such a case, the isr has a problem. It cannot wait until the task finishes.
 *    For this situation, an extra space is reserved in the queue buffer for data pending to be pushed.
}*/

//{ Includes

#include "ttc_queue_types.h"
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "ttc_mutex.h"
#include "ttc_task.h"
#include "ttc_semaphore.h"

#ifdef EXTENSION_300_scheduler_freertos
    //? #include <string.h>
    //? #include "stddef.h"
    #include "FreeRTOS.h"
    #include "queue.h"
    #include "semphr.h"
#endif

//}Includes
//{ Function prototypes **************************************************

/** creates a new generic queue  (Allocates required memory block from heap).
 * @param AmountOfEntries  maximum amount of entries that the new queue can store
 * @param SizeOfEntries    size of each entry in queue
 * @return                 =! NULL: queue has been created successfully (use handle to operate on queue)
 */
t_ttc_queue_generic ttc_queue_generic_create( t_base AmountOfEntries, t_base SizeOfEntries );

/** initializes given memory for use as simple byte queue.
 * Simple queues are faster and use less memory than general queues.
 * Simple queues are available even without a multitasking scheduler.
 *
 * Note: use of this function can be dangerous, because the valid size of *Storage[] cannot be verified!
 *
 * Restrictions are:
 * - only one task is alowed to push data into the queue
 * - only one task is alowed to pop data from the queue
 * - fixed item size
 *
 * @param Queue            address of static or dynamically allocated memory space to use for queue header
 * @param Storage          memory space to use for queue data (must store AmountOfEntries bytes!)
 * @param AmountOfEntries  Add 1 for queu-management! Queue is created with a fixed size. It cannot be resized later.
 * @return pointer to struct containing all necessary data of this queue
 */
void ttc_queue_byte_init( t_ttc_queue_bytes* Queue, t_u8* Storage, t_u16 AmountOfEntries );

/** adds given entry ad end of given queue
 *
 * Note: This function requires a running task scheduler!
 *
 * @param Queue    handle of already created queue
 * @param Entry    pointer to data to be copied into new queue entry
 * @param TimeOut  amount of system ticks to wait for queue to become available
 * @return         == tqe_OK: Entry has been successfully added to Queue within TimeOut
 */
e_ttc_queue_error ttc_queue_generic_push_back( t_ttc_queue_generic Queue, const void* Entry, t_base TimeOut );

/** adds given entry ad end of given queue (does never block)
 * Note: Must be called from ISR or with disabled scheduler! (-> ttc_task_critical_begin )
 *
 * @param Queue    handle of already created queue
 * @param Entry    pointer to data to be copied into new queue entry
 * @param TimeOut  amount of system ticks to wait for queue to become available
 * @return         == tqe_OK: Entry has been successfully added to Queue within TimeOut
 *                 == tqe_HigherPriorityTaskWoken: call task ttc_task_yield_isr() before exiting your ISR!
 */
e_ttc_queue_error ttc_queue_generic_push_back_isr( t_ttc_queue_generic Queue, const void* Entry );

/** removes first entry from given queue and copies its data into given buffer
 *
 * Note: This function requires a running task scheduler!
 *
 * @param Queue  handle of already created queue
 * @param Entry  pointer to buffer where content of first entry should be copied to
 * @param TimeOut  amount of system ticks to wait for queue to provide data
 * @return         == tqe_OK: Entry has been successfully added to Queue within TimeOut
 */
e_ttc_queue_error ttc_queue_generic_pull_front( t_ttc_queue_generic Queue, void* Entry, t_base TimeOut );

/** removes first entry from given queue and copies its data into given buffer (does never block)
 * @param Queue  handle of already created queue
 * @param Entry  pointer to buffer where content of first entry should be copied to
 * @return         == tqe_OK: Entry has been successfully added to Queue within TimeOut
 */
e_ttc_queue_error ttc_queue_generic_pull_front_isr( t_ttc_queue_generic Queue, void* Entry );

/** provides amount of entries currently waiting in given queue
 *
 * Note: This function requires a running task scheduler!
 *
 * @param Queue  handle of already created queue
 * @return       current amount of occupied entries
 */
t_base ttc_queue_amount_waiting( t_ttc_queue_generic Queue );

/** Creates a simple queue for 8-bit data  (Allocates required memory block from heap).
 * Simple queues are faster and use less memory than general queues.
 * Simple queues are available even without a multitasking scheduler.
 * Functions on simple queues will never block.
 *
 * Restrictions are:
 * - only one task is alowed to push data into the queue
 * - only one task is alowed to pop data from the queue
 * - fixed item size
 *
 * @param AmountOfEntries  Queue is created with a fixed size. It cannot be resized later.
 * @return pointer to struct containing all necessary data of this queue
 */
t_ttc_queue_bytes* ttc_queue_byte_create( t_u16 AmountOfEntries );

/** returns amount of entries currently stored in given queue
 *
 * Note: This function is not protected against concurrent calls from different tasks!
 * Note: Returned size can be corrupt if items are added/ removed by another task!
 *
 * @param QueueHandle  handle of already created queue
 * @param Data         Byte to append to queue
 * @return             == tqe_OK: Entry has been successfully added
 */
t_base ttc_queue_byte_get_amount( t_ttc_queue_bytes* Queue );

/** adds given byte at end of given queue
 *
 * Note: This function is may be called from different tasks on same queue.
 *
 * @param QueueHandle  handle of already created queue
 * @param Data         Byte to append to queue
 * @return             == tqe_OK: Entry has been successfully added
 */
void ttc_queue_byte_push_back( t_ttc_queue_bytes* Queue, t_u8 Data );

/** adds given byte at end of given queue (never blocks)
 *
 * Note: This function may only be called if interrupts are disabled (e.g. from an interrupt service routine)
 * Note: If queue is locked for pushing, data will be stored in a small extra buffer. If this buffer expires, data is not stored!
 *
 * @param QueueHandle  handle of already created queue
 * @param Data         Byte to append to queue
 * @return             == tqe_OK: Entry has been successfully added
 *                     == tqe_HigherPriorityTaskWoken: call task ttc_task_yield_isr() before exiting your ISR!
 *                     >= tqe_Error: Error condition
 */
e_ttc_queue_error ttc_queue_byte_push_back_isr( t_ttc_queue_bytes* Queue, t_u8 Data );

/** adds given byte at end of given queue
 *
 * Note: This function is not protected against concurrent calls from different tasks!
 * Note: It is safe to have one single task call push and another single task call pull on the same queue.
 * Note: It is not safe to have >1 task push/ pull on same queue!
 *
 * @param QueueHandle  handle of already created queue
 * @param Data         Byte to append to queue
 * @return             == tqe_OK: Entry has been successfully added
 */
e_ttc_queue_error _ttc_queue_byte_push_back_try( t_ttc_queue_bytes* Queue, t_u8 Data );

/** reads one byte from given queue
 *
 * Note: This function may be called from different tasks
 *
 * @param QueueHandle  handle of already created queue
 * @return             byte being read from Queue
 */
t_u8 ttc_queue_byte_pull_front( t_ttc_queue_bytes* Queue );

/** reads one byte from given queue (call from interrupt service routine)
 *
 * Note: This function may be called from different tasks
 *
 * @param QueueHandle  handle of already created queue
 * @param Data         byte will be written to this address (no write in case of error)
 * @return             == tqe_OK: Entry has been successfully pulled
 *                     == tqe_HigherPriorityTaskWoken: call task ttc_task_yield_isr() before exiting your ISR!
 *                     >= tqe_Error: Error condition
 */
e_ttc_queue_error ttc_queue_byte_pull_front_isr( t_ttc_queue_bytes* Queue, t_u8* Data );
/** reads one byte from given queue (call from interrupt service routine)
 *
 * Note: This function may be called from different tasks
 *
 * @param QueueHandle  handle of already created queue
 * @param Data         byte will be written to this address (no write in case of error)
 * @return             == tqe_OK: Entry has been successfully pulled
 *                     == tqe_HigherPriorityTaskWoken: call task ttc_task_yield_isr() before exiting your ISR!
 *                     >= tqe_Error: Error condition
 */
e_ttc_queue_error ttc_queue_byte_pull_front_try( t_ttc_queue_bytes* Queue, t_u8* Data );
/** processes all pending isr operations on given byte queue
 *
 * Note: This function is not protected against concurrent calls from different tasks!
 * Note: It is safe to have one single task call push and another single task call pull on the same queue.
 * Note: It is not safe to have >1 task push/ pull on same queue!
 * Note: This function is private and should not be called from outside!
 *
 * @param Queue        datastructure of already created queue
 */
void _ttc_queue_byte_process_pending( t_ttc_queue_bytes* Queue );

/** Creates a simple queue to store pointers (Allocates required memory block from heap).
 * Simple queues are faster and use less memory than general queues.
 * Simple queues are available even without a multitasking scheduler.
 *
 * Restrictions are:
 * - only one task is alowed to push data into the queue
 * - only one task is alowed to pop data from the queue
 * - fixed item size
 *
 * @param AmountOfEntries  Queue is created with a fixed size. It cannot be resized later.
 * @return pointer to struct containing all necessary data of this queue
 */
t_ttc_queue_pointers* ttc_queue_pointer_create( t_u16 AmountOfEntries );

/** initializes given memory for use as simple pointer queue.
 * Simple queues are faster and use less memory than general queues.
 * Simple queues are available even without a multitasking scheduler.
 *
 * Note: use of this function can be dangerous, because the valid size of *Storage[] cannot be verified!
 *
 * Restrictions are:
 * - only one task is alowed to push data into the queue
 * - only one task is alowed to pop data from the queue
 * - fixed item size
 *
 * @param Queue            address of static or dynamically allocated memory space to use
 * @param Storage          memory space to use for queue data (must store AmountOfEntries+1 * sizeof(t_base) bytes!)
 * @param AmountOfEntries  Add 1 for queu-management! Queue is created with a fixed size. It cannot be resized later.
 * @return pointer to struct containing all necessary data of this queue
 */
void ttc_queue_pointer_init( t_ttc_queue_pointers* Queue, void** Storage, t_u16 AmountOfEntries );

/** returns amount of entries currently stored in given queue
 *
 * Note: This function is not protected against concurrent calls from different tasks!
 * Note: Returned size can be corrupt if items are added/ removed by another task!
 *
 * @param QueueHandle  handle of already created queue
 * @param Data         Byte to append to queue
 * @return             == tqe_OK: Entry has been successfully added
 */
t_base ttc_queue_pointer_get_amount( t_ttc_queue_pointers* Queue );

/** adds given byte at end of given queue
 *
 * Note: It is safe to have one single task call push and another single task call pull on the same queue.
 * Note: It is not safe to have >1 task push/ pull on same queue!
 *
 * @param QueueHandle  handle of already created queue
 * @param Data         pointer address to append to queue
 * @return             == tqe_OK: Entry has been successfully added
 */
void ttc_queue_pointer_push_back( t_ttc_queue_pointers* Queue, void* Data );

/** adds given byte at end of given queue (never blocks)
 *
 * Note: This function may only be called if interrupts are disabled (e.g. from an interrupt service routine)
 * Note: If queue is locked for pushing, data will be stored in a small extra buffer. If this buffer expires, data is not stored!
 *
 * @param QueueHandle  handle of already created queue
 * @param Data         pointer address to append to queue
 * @return             == tqe_OK: Entry has been successfully added
 */
e_ttc_queue_error ttc_queue_pointer_push_back_isr( t_ttc_queue_pointers* Queue, void* Data );

/** reads one byte from given queue
 *
 * Note: This function can be used from different tasks
 * Note:
 *
 * @param QueueHandle  handle of already created queue
 * @return             pointer item from head of list
 */
void* ttc_queue_pointer_pull_front( t_ttc_queue_pointers* Queue );

/** tries to read one byte from given queue (will never block)
 *
 * Note: This function can be used from different tasks
 *
 * @param QueueHandle  handle of already created queue
 * @param Pointer      pointer address will be written to this address (NULL if queue is empty)
 * @return             == tqe_OK: Entry has been successfully pulled
 */
e_ttc_queue_error ttc_queue_pointer_pull_front_try( t_ttc_queue_pointers* Queue, void** Data );

/** reads one byte from given queue(never blocks)
 *
 * Note: This function may only be called if interrupts are disabled (e.g. from an interrupt service routine)
 *
 * @param QueueHandle  handle of already created queue
 * @param Pointer      pointer address will be written to this address (NULL if queue is empty)
 * @return             == tqe_OK: Entry has been successfully pulled
 */
e_ttc_queue_error ttc_queue_pointer_pull_front_isr( t_ttc_queue_pointers* Queue, void** Data );

//}Function prototypes

#endif
