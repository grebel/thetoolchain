/** { ttc_tcpip.c ****************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for tcpip devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported 
 *  architectures.
 *
 *  See corresponding header file for documentation.
 *
 *  Created from template ttc_device.c revision 27 at 20150309 09:09:54 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_tcpip.h".
//
#include "ttc_tcpip.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_TCPIP_AMOUNT == 0
  #warning No TCPIP devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of tcpip devices.
 *
 */
 

// for each initialized device, a pointer to its generic and uip definitions is stored
A_define(t_ttc_tcpip_config*, ttc_tcpip_configs, TTC_TCPIP_AMOUNT);

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8 ttc_tcpip_get_max_index() {
    return TTC_TCPIP_AMOUNT;
}
t_ttc_tcpip_config* ttc_tcpip_get_configuration(t_u8 LogicalIndex) {
    Assert_TCPIP(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_tcpip_config* Config = A(ttc_tcpip_configs, LogicalIndex-1); // will assert if outside array bounds

    if (!Config) {
        Config = A(ttc_tcpip_configs, LogicalIndex-1) = ttc_heap_alloc_zeroed(sizeof(t_ttc_tcpip_config));
        Config->Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_tcpip_load_defaults(LogicalIndex);
    }

    return Config;
}
t_ttc_tcpip_config* ttc_tcpip_get_features(t_u8 LogicalIndex) {
    t_ttc_tcpip_config* Config = ttc_tcpip_get_configuration(LogicalIndex);
    return _driver_tcpip_get_features(Config);
}
void ttc_tcpip_deinit(t_u8 LogicalIndex) {
    Assert_TCPIP(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_tcpip_config* Config = ttc_tcpip_get_configuration(LogicalIndex);
  
    e_ttc_tcpip_errorcode Result = _driver_tcpip_deinit(Config);
    if (Result == ec_tcpip_OK)
      Config->Flags.Bits.Initialized = 0;
}
e_ttc_tcpip_errorcode ttc_tcpip_init(t_u8 LogicalIndex) {
    Assert_TCPIP(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_tcpip_config* Config = ttc_tcpip_get_configuration(LogicalIndex);

    e_ttc_tcpip_errorcode Result = _driver_tcpip_init(Config);
    if (Result == ec_tcpip_OK)
      Config->Flags.Bits.Initialized = 1;
    
    return Result;
}
e_ttc_tcpip_errorcode ttc_tcpip_load_defaults(t_u8 LogicalIndex) {
    Assert_TCPIP(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_tcpip_config* Config = ttc_tcpip_get_configuration(LogicalIndex);

    if (Config->Flags.Bits.Initialized)
      ttc_tcpip_deinit(LogicalIndex);
    
    ttc_memory_set( Config, 0, sizeof(t_ttc_tcpip_config) );

    // load generic default values
    switch (LogicalIndex) { // load type of low-level driver for current architecture
#ifdef TTC_TCPIP1_DRIVER
    case  1: Config->Architecture = TTC_TCPIP1_DRIVER; break;
#endif
#ifdef TTC_TCPIP2_DRIVER
    case  2: Config->Architecture = TTC_TCPIP2_DRIVER; break;
#endif
#ifdef TTC_TCPIP3_DRIVER
    case  3: Config->Architecture = TTC_TCPIP3_DRIVER; break;
#endif
#ifdef TTC_TCPIP4_DRIVER
    case  4: Config->Architecture = TTC_TCPIP4_DRIVER; break;
#endif
#ifdef TTC_TCPIP5_DRIVER
    case  5: Config->Architecture = TTC_TCPIP5_DRIVER; break;
#endif
#ifdef TTC_TCPIP6_DRIVER
    case  6: Config->Architecture = TTC_TCPIP6_DRIVER; break;
#endif
#ifdef TTC_TCPIP7_DRIVER
    case  7: Config->Architecture = TTC_TCPIP7_DRIVER; break;
#endif
#ifdef TTC_TCPIP8_DRIVER
    case  8: Config->Architecture = TTC_TCPIP8_DRIVER; break;
#endif
#ifdef TTC_TCPIP9_DRIVER
    case  9: Config->Architecture = TTC_TCPIP9_DRIVER; break;
#endif
#ifdef TTC_TCPIP10_DRIVER
    case 10: Config->Architecture = TTC_TCPIP10_DRIVER; break;
#endif
    default: ttc_assert_halt_origin(ec_tcpip_InvalidImplementation); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }
    Assert_TCPIP( (Config->Architecture > ta_tcpip_None) && (Config->Architecture < ta_tcpip_unknown), ttc_assert_origin_auto); // architecture not set properly! Check makefile for TTC_TCPIP<n>_DRIVER and compare with e_ttc_tcpip_architecture 

    Config->LogicalIndex = LogicalIndex;
    Config->PhysicalIndex = ttc_tcpip_logical_2_physical_index(LogicalIndex);
    
    //Insert additional generic default values here ...

    return _driver_tcpip_load_defaults(Config);    
}
t_u8 ttc_tcpip_logical_2_physical_index(t_u8 LogicalIndex) {
  
  switch (LogicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_TCPIP1
           case 1: return TTC_TCPIP1;
#endif
#ifdef TTC_TCPIP2
           case 2: return TTC_TCPIP2;
#endif
#ifdef TTC_TCPIP3
           case 3: return TTC_TCPIP3;
#endif
#ifdef TTC_TCPIP4
           case 4: return TTC_TCPIP4;
#endif
#ifdef TTC_TCPIP5
           case 5: return TTC_TCPIP5;
#endif
#ifdef TTC_TCPIP6
           case 6: return TTC_TCPIP6;
#endif
#ifdef TTC_TCPIP7
           case 7: return TTC_TCPIP7;
#endif
#ifdef TTC_TCPIP8
           case 8: return TTC_TCPIP8;
#endif
#ifdef TTC_TCPIP9
           case 9: return TTC_TCPIP9;
#endif
#ifdef TTC_TCPIP10
           case 10: return TTC_TCPIP10;
#endif
      // extend as required
      
      default: break;
    }
    
  Assert_TCPIP(0, ttc_assert_origin_auto); // No TTC_TCPIPn defined! Check your makefile.100_board_* file!
  return -1; // program flow should never go here
}
t_u8 ttc_tcpip_physical_2_logical_index(t_u8 PhysicalIndex) {
  
  switch (PhysicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_TCPIP1
           case TTC_TCPIP1: return 1;
#endif
#ifdef TTC_TCPIP2
           case TTC_TCPIP2: return 2;
#endif
#ifdef TTC_TCPIP3
           case TTC_TCPIP3: return 3;
#endif
#ifdef TTC_TCPIP4
           case TTC_TCPIP4: return 4;
#endif
#ifdef TTC_TCPIP5
           case TTC_TCPIP5: return 5;
#endif
#ifdef TTC_TCPIP6
           case TTC_TCPIP6: return 6;
#endif
#ifdef TTC_TCPIP7
           case TTC_TCPIP7: return 7;
#endif
#ifdef TTC_TCPIP8
           case TTC_TCPIP8: return 8;
#endif
#ifdef TTC_TCPIP9
           case TTC_TCPIP9: return 9;
#endif
#ifdef TTC_TCPIP10
           case TTC_TCPIP10: return 10;
#endif
      // extend as required
      
      default: break;
    }
    
  Assert_TCPIP(0, ttc_assert_origin_auto); // No TTC_TCPIPn defined! Check your makefile.100_board_* file!
  return -1; // program flow should never go here
}
void ttc_tcpip_prepare() {
  // add your startup code here (Singletasking!)
  _driver_tcpip_prepare();
}
void ttc_tcpip_reset(t_u8 LogicalIndex) {
    Assert_TCPIP(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_tcpip_config* Config = ttc_tcpip_get_configuration(LogicalIndex);

    _driver_tcpip_reset(Config);    
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_tcpip(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
