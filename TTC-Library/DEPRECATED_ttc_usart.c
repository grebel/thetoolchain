/*{ ttc_usart::.c ************************************************

                      The ToolChain
                      
   Device independent support for
   Universal Synchronous Asynchronous Receiver and Transmitters (USARTs)
   
   written by Gregor Rebel 2012
   

}*/

#define TU_USE_LED1 1
#define TU_USE_LED2 1

#include "DEPRECATED_ttc_usart.h"
//{ Global variables *****************************************************

#ifdef TARGET_ARCHITECTURE_STM32F1xx
void (*ISR_RX)(physical_index_t, void*) = _stm32_usart_rx_isr; // function that will forward interrupt to _ttc_usart_rx_isr()
void (*ISR_TX)(physical_index_t, void*) = _stm32_usart_tx_isr; // function that will forward interrupt to _ttc_usart_tx_isr()
#else
void (*ISR_RX)(physical_index_t, void*) = NULL;
void (*ISR_TX)(physical_index_t, void*) = NULL;
#endif

// logical index of USART to use by ttc_usart_stdout_send_string()/ ttc_usart_stdout_send_block()
u8_t ttc_usart_stdout_index = 0;

// for each initialized USART, a pointer to its generic and architecture definitions is stored
ttc_array_define(ttc_usart_config_t*, ttc_USARTs, TTC_AMOUNT_USARTS);

u8_t Amount_BlocksAllocated;    // amount of transmit/receive buffers allocated by ttc_usart_get_tx_block() so far
u8_t Amount_RxBytesSkipped;     // amount of bytes received by _ttc_usart_rx_isr() which could not be pushed into receive queue

// stores released memory blocks for reuse as transmit or receive blocks
// Note: Pushes to this queue are allowed from outside interrupt service routine only!
ttc_queue_pointers_t tm_QueueEmptyBlocks;
void* tm_QueueEmptyBlocks_Data[TTC_USART_MAX_MEMORY_BUFFERS+1];

// protects queue from simultaneous access

//}Global variables
//{ Function definitions *************************************************

ttc_usart_config_t* ttc_usart_get_configuration(u8_t LogicalIndex) {
    Assert_USART(LogicalIndex > 0, ec_InvalidArgument);                  // logical index starts at 1
    Assert_USART(LogicalIndex <= TTC_AMOUNT_USARTS, ec_InvalidArgument); // use ttc_i2c_get_max_index() to get max allowed value
    ttc_usart_config_t* USART_Generic = A(ttc_USARTs, LogicalIndex-1);

    if (!USART_Generic) {
        USART_Generic = A(ttc_USARTs, LogicalIndex-1) = ttc_memory_alloc_zeroed(sizeof(ttc_usart_config_t));
        ttc_usart_reset(LogicalIndex);
    }

    return USART_Generic;
}

void ttc_usart_prepare() {
    ttc_queue_pointer_init(&tm_QueueEmptyBlocks, &(tm_QueueEmptyBlocks_Data[0]), TTC_USART_MAX_MEMORY_BUFFERS+1);
    Amount_BlocksAllocated = 0;
    Amount_RxBytesSkipped  = 0;

}
const ttc_channel_device_interface_t* ttc_usart_get_channel_interface() {
    const static ttc_channel_device_interface_t Interface = {

        ttc_usart_send_block,
        ttc_usart_send_raw,
        ttc_usart_send_raw_const,
        ttc_usart_send_string,
        ttc_usart_send_string_const,
        ttc_usart_get_empty_block
    };

    return &Interface;
}
u8_t ttc_usart_get_max_logicalindex() {

    return TTC_AMOUNT_USARTS; // -> ttc_usart_types.h
}
ttc_channel_errorcode_e ttc_usart_reset(u8_t LogicalIndex) {
    ttc_usart_config_t* USART_Generic = _ttc_usart_get_configuration(LogicalIndex);

    memset(USART_Generic, 0, sizeof(USART_Generic));
    USART_Generic->LogicalIndex = LogicalIndex;

    ttc_channel_errorcode_e Status = tue_NotImplemented;

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    Status = stm32_usart_get_defaults( ttc_usart_get_physicalindex(LogicalIndex),
                                       USART_Generic
                                       );
#endif

    if (Status == tce_OK) { // low-level driver returned OK: add platform independent features
        USART_Generic->Flags.Bits.DelayedTransmits = 1;
        USART_Generic->Size_Queue_Rx          = 20;
        USART_Generic->Size_Queue_Tx          = 20;
    }

    return Status;
}
ttc_channel_errorcode_e ttc_usart_get_features(u8_t LogicalIndex, ttc_usart_config_t* USART_Generic) {
    Assert_USART(LogicalIndex > 0, ec_InvalidArgument);                // logical index starts at 1
    Assert_USART(LogicalIndex <= TTC_AMOUNT_USARTS, ec_InvalidArgument); // use ttc_i2c_get_max_index() to get max allowed value
    Assert_USART(USART_Generic != NULL, ec_InvalidArgument);

    memset(USART_Generic, 0, sizeof(USART_Generic));
    USART_Generic->LogicalIndex = LogicalIndex;
    ttc_channel_errorcode_e Status = tue_NotImplemented;

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    Status = stm32_usart_get_features( ttc_usart_get_physicalindex(LogicalIndex),
                                       USART_Generic
                                       );
#endif

    if (Status == tce_OK) { // low-level driver returned OK: add platform independent features
        USART_Generic->Flags.Bits.DelayedTransmits = 1;

        if (1) { // disable features requiring unconfigured pins
            switch (LogicalIndex) {
            case 1: {
#ifndef TTC_USART1_TX
                USART_Generic->Flags.Bits.Transmit    = 0;
#endif
#ifndef TTC_USART1_RX
                USART_Generic->Flags.Bits.Receive     = 0;
#endif
#ifndef TTC_USART1_RTS
                USART_Generic->Flags.Bits.RtsCts      = 0;
                USART_Generic->Flags.Bits.Rts         = 0;
#endif
#ifndef TTC_USART1_CTS
                USART_Generic->Flags.Bits.RtsCts      = 0;
                USART_Generic->Flags.Bits.Cts         = 0;
#endif
#ifndef TTC_USART1_CK
                USART_Generic->Flags.Bits.Synchronous = 0;
#endif

                break;
            }
            case 2: {
#ifndef TTC_USART2_TX
                USART_Generic->Flags.Bits.Transmit    = 0;
#endif
#ifndef TTC_USART2_RX
                USART_Generic->Flags.Bits.Receive     = 0;
#endif
#ifndef TTC_USART2_RTS
                USART_Generic->Flags.Bits.RtsCts      = 0;
                USART_Generic->Flags.Bits.Rts         = 0;
#endif
#ifndef TTC_USART2_CTS
                USART_Generic->Flags.Bits.RtsCts      = 0;
                USART_Generic->Flags.Bits.Cts         = 0;
#endif
#ifndef TTC_USART2_CK
                USART_Generic->Flags.Bits.Synchronous = 0;
#endif

                break;
            }
            case 3: {
#ifndef TTC_USART3_TX
                USART_Generic->Flags.Bits.Transmit    = 0;
#endif
#ifndef TTC_USART3_RX
                USART_Generic->Flags.Bits.Receive     = 0;
#endif
#ifndef TTC_USART3_RTS
                USART_Generic->Flags.Bits.RtsCts      = 0;
                USART_Generic->Flags.Bits.Rts         = 0;
#endif
#ifndef TTC_USART3_CTS
                USART_Generic->Flags.Bits.RtsCts      = 0;
                USART_Generic->Flags.Bits.Cts         = 0;
#endif
#ifndef TTC_USART3_CK
                USART_Generic->Flags.Bits.Synchronous = 0;
#endif

                break;
            }
            case 4: {
#ifndef TTC_USART4_TX
                USART_Generic->Flags.Bits.Transmit    = 0;
#endif
#ifndef TTC_USART4_RX
                USART_Generic->Flags.Bits.Receive     = 0;
#endif

                // UART4, UART5 only provide TX+RX pins
                USART_Generic->Flags.Bits.RtsCts      = 0;
                USART_Generic->Flags.Bits.Rts         = 0;
                USART_Generic->Flags.Bits.Cts         = 0;
                USART_Generic->Flags.Bits.Synchronous = 0;

                break;
            }
            case 5: {
#ifndef TTC_USART5_TX
                USART_Generic->Flags.Bits.Transmit    = 0;
#endif
#ifndef TTC_USART5_RX
                USART_Generic->Flags.Bits.Receive     = 0;
#endif

                // UART4, UART5 only provide TX+RX pins
                USART_Generic->Flags.Bits.RtsCts      = 0;
                USART_Generic->Flags.Bits.Rts         = 0;
                USART_Generic->Flags.Bits.Cts         = 0;
                USART_Generic->Flags.Bits.Synchronous = 0;

                break;
            }
            default: return tue_DeviceNotFound;
            }
        }
    }

    return tue_NotImplemented;
}
ttc_memory_block_t* ttc_usart_get_empty_block() {

    ttc_memory_block_t* Block = NULL;
    while (Block == NULL) {
        ttc_queue_pointer_try_pull_front(&tm_QueueEmptyBlocks, (void**) &Block);
        if (!Block) { // no released memory block available: alloc new block
            if (Amount_BlocksAllocated < TTC_USART_MAX_MEMORY_BUFFERS) {
                Block = ttc_memory_alloc_block(TTC_USART_BLOCK_SIZE, 0, _ttc_usart_release_block);
                Amount_BlocksAllocated++;
            }
            else // no more memory blocks available: wait for next block to be released
                ttc_queue_pointer_pull_front(&tm_QueueEmptyBlocks, (void**) &Block);
        }
        if (Block) {
            Assert_USART(ttc_memory_is_writable(Block), ec_InvalidImplementation);
            Block->Buffer = ( (u8_t*) Block ) + sizeof(ttc_memory_block_t); // correct pointer to buffer in case this block has been used as a virtual block
        }

        if (Block == NULL)  // got no block: wait for some while
            ttc_task_yield();
    }
    //? ttc_memory_block_use(Block);

    return Block;
}
ttc_channel_errorcode_e ttc_usart_init(u8_t LogicalIndex) {

    if (LogicalIndex > ttc_usart_get_max_logicalindex() )
        return tue_DeviceNotFound;
    ttc_task_begin_criticalsection(ttc_usart_init);

    ttc_usart_config_t* USART_Generic = ttc_usart_get_configuration(LogicalIndex);
    ttc_channel_errorcode_e ReturnCode = tue_NotImplemented;
    ttc_usart_architecture_t* USART_Arch = &(USART_Generic->USART_Arch);

    memset(USART_Arch, 0, sizeof(USART_Arch));
    USART_Generic->LogicalIndex = LogicalIndex;
    if ((USART_Generic->receiveBlock != NULL)||(USART_Generic->receiveSingleByte != NULL)) { // blocking of data activated: spawn receive-task
        if (USART_Generic->Queue_Rx == NULL) { // first initialisation of this USART
            USART_Generic->Queue_Rx = ttc_queue_byte_create(USART_Generic->Size_Queue_Rx);
            ttc_task_create(_ttc_usart_task_receive, "ttc_usart_rx", 128, (void*) USART_Generic, 10, &(USART_Generic->Task_Rx) );
            Assert_USART(USART_Generic->Task_Rx, ec_Malloc); // could not spawn receive task (out of memory?)
        }
    }

    // issue low-level initialization
    ReturnCode = _driver_ttc_usart_init(USART_Generic);

    if (ReturnCode == tce_OK) { // low-level initialization successfull: init interrupts
        u8_t PhysicalIndex = ttc_usart_get_physicalindex(LogicalIndex); // interrupts require real hardware index

        if ( USART_Generic->Flags.Bits.IrqOnRxNE &&
             ( (USART_Generic->receiveBlock != NULL) || (USART_Generic->receiveSingleByte != NULL) )
             ) { // setup interrupt for tit_USART_RxNE
            ttc_interrupt_errorcode_e Error = ttc_interrupt_init(tit_USART_RxNE,
                                                                 PhysicalIndex,
                                                                 ISR_RX,
                                                                 (void*) USART_Generic,
                                                                 NULL,
                                                                 NULL
                                                                 );
            Assert_USART(Error == tine_OK, ec_UNKNOWN);
            ttc_interrupt_enable(tit_USART_RxNE, PhysicalIndex, 1);
        }
        if (USART_Generic->Flags.Bits.DelayedTransmits) { // setup interrupt for tit_USART_TransmitDataEmpty
            if (USART_Generic->Queue_Tx== NULL) {
                USART_Generic->Queue_Tx = ttc_queue_pointer_create(USART_Generic->Size_Queue_Tx);
                Assert_USART(USART_Generic->Queue_Tx, ec_Malloc);
            }

            // this interrupt will be enabled when there is data to send
            Assert_USART( ttc_interrupt_init(tit_USART_TransmitDataEmpty,
                                             PhysicalIndex,
                                             ISR_TX,
                                             (void*) USART_Generic,
                                             NULL,
                                             NULL
                                             ) == tine_OK, ec_UNKNOWN);

        }
        ttc_mutex_init( &(USART_Generic->Queue_TX_Lock) );

        A(ttc_USARTs, LogicalIndex-1) = USART_Generic;

        // from now on use this USART for default output
        ttc_usart_stdout_set(LogicalIndex);
    }
    else {
        Assert_USART(FALSE, ec_InvalidConfiguration);
    }

    ttc_task_end_criticalsection();
    return ReturnCode;
}
ttc_channel_errorcode_e ttc_usart_register_receive(u8_t LogicalIndex, ttc_memory_block_t* (*receiveBlock)(void* Argument, struct ttc_usart_generic_s* USART_Generic, ttc_memory_block_t* Block), void* Argument) {
    ttc_usart_config_t* USART_Generic = ttc_usart_get_configuration(LogicalIndex);

    USART_Generic->receiveBlock          = receiveBlock;
    USART_Generic->receiveBlock_Argument = Argument;

    return ttc_usart_init(LogicalIndex);
}
ttc_channel_errorcode_e ttc_usart_send_raw(u8_t LogicalIndex, u8_t* Buffer, Base_t Amount) {
    Assert_USART(Buffer != NULL, ec_InvalidArgument);
    ttc_usart_config_t* USART_Generic = _ttc_usart_get_configuration(LogicalIndex);

    if (USART_Generic->Flags.Bits.DelayedTransmits) { // push buffer to USART_Generic->Queue_Tx

        // data must be copied into one or more memory blocks
        u16_t RemainingBytes = Amount;
        u16_t Bytes_written  = 0;
        while (RemainingBytes > 0) {
            ttc_memory_block_t* TxBlock = ttc_usart_get_empty_block();

            u16_t Bytes2Copy = RemainingBytes;
            if (Bytes2Copy > TTC_USART_BLOCK_SIZE)
                Bytes2Copy = TTC_USART_BLOCK_SIZE;

            ttc_memory_copy(TxBlock->Buffer, &Buffer[Bytes_written], Bytes2Copy);
            TxBlock->Size = Bytes2Copy;
            ttc_usart_send_block(LogicalIndex, TxBlock);
            RemainingBytes -= Bytes2Copy;
            Bytes_written += Bytes2Copy;
        }

    }
    else
        _ttc_usart_send_raw_blocking(LogicalIndex, Buffer, Amount);

    return tue_OK;
}
ttc_channel_errorcode_e ttc_usart_send_raw_const(u8_t LogicalIndex, u8_t* Buffer, Base_t Amount) {
    Assert_USART(Buffer != NULL, ec_InvalidArgument);
    ttc_usart_config_t* USART_Generic = _ttc_usart_get_configuration(LogicalIndex);

    if (USART_Generic->Flags.Bits.DelayedTransmits) { // push buffer to USART_Generic->Queue_Tx
        // data from constant memory is not copied
        ttc_memory_block_t* TxBlock = ttc_usart_get_empty_block();
        TxBlock->Buffer = Buffer; // use memory block as a virtual memory block
        TxBlock->Size   = Amount;
        if (TxBlock->Size < 0)    // make sure its a positive value (negative size means zero terminated with max length)
            TxBlock->Size = -TxBlock->Size;
        ttc_usart_send_block(LogicalIndex, TxBlock);
    }
    else
        _ttc_usart_send_raw_blocking(LogicalIndex, Buffer, Amount);

    return tue_OK;
}
ttc_channel_errorcode_e ttc_usart_send_string(u8_t LogicalIndex, const char* Buffer, Base_t MaxLength) {
    Assert_USART(Buffer != NULL, ec_InvalidArgument);
    ttc_usart_config_t* USART_Generic = _ttc_usart_get_configuration(LogicalIndex);

    if (USART_Generic->Flags.Bits.DelayedTransmits) { // push buffer to USART_Generic->Queue_Tx
        // data must be copied into one or more memory blocks
        u16_t TotalBytesCopied = 0;
        const u8_t* Reader = (u8_t*) Buffer;
        while (TotalBytesCopied < MaxLength) {
            ttc_memory_block_t* TxBlock = ttc_usart_get_empty_block();

            u8_t* Writer = TxBlock->Buffer;

            u16_t MaxBytes2Copy = TTC_USART_BLOCK_SIZE;
            if (MaxBytes2Copy > (MaxLength - TotalBytesCopied)) {
                MaxBytes2Copy = MaxLength - TotalBytesCopied;
            }

            u16_t BytesCopied = 0;
            while (BytesCopied < MaxBytes2Copy) {
                u8_t C = *Reader++;
                if (C != 0)           // stopping at first zero
                    *Writer++ = C;
                else {
                    MaxLength = TotalBytesCopied; // will cause outer loop to leave
                    break;
                }

                BytesCopied++;
                TotalBytesCopied++;
            }

            TxBlock->Size = BytesCopied;
            ttc_usart_send_block(LogicalIndex, TxBlock);
        }
    }
    else
        _ttc_usart_send_string_blocking(LogicalIndex, Buffer, MaxLength);

    return tue_OK;
}
ttc_channel_errorcode_e ttc_usart_send_string_const(u8_t LogicalIndex, const char* Buffer, Base_t MaxLength) {
    Assert_USART(Buffer != NULL, ec_InvalidArgument);
    ttc_usart_config_t* USART_Generic = _ttc_usart_get_configuration(LogicalIndex);

    if (USART_Generic->Flags.Bits.DelayedTransmits) { // push buffer to USART_Generic->Queue_Tx
        // data from constant memory is not copied
        ttc_memory_block_t* TxBlock = ttc_usart_get_empty_block();
        char* Buffer2 = (char*) Buffer;                 // yes, we cast away the constness!
        TxBlock->Buffer = (u8_t*) Buffer2;              // use memory block as a virtual memory block
        TxBlock->Size   = - (Base_signed_t) MaxLength;  // marking string as zero terminated (see definition of ttc_memory_block_t)
        if (TxBlock->Size > 0)                          // make sure its a negative value
            TxBlock->Size = -TxBlock->Size;

        ttc_usart_send_block(LogicalIndex, TxBlock);
    }
    else
        _ttc_usart_send_string_blocking(LogicalIndex, Buffer, MaxLength);

    return tue_OK;
}
ttc_channel_errorcode_e ttc_usart_send_block(u8_t LogicalIndex, ttc_memory_block_t* Block) {
    Assert_USART(Block != NULL, ec_InvalidArgument);
    ttc_usart_config_t* USART_Generic = _ttc_usart_get_configuration(LogicalIndex);

    if (USART_Generic->Flags.Bits.DelayedTransmits) { // push buffer to USART_Generic->Queue_Tx
        while (ttc_mutex_lock(&(USART_Generic->Queue_TX_Lock), -1, ttc_usart_send_block) != tme_OK); // ToDo: required?
        ttc_queue_pointer_push_back(USART_Generic->Queue_Tx, Block);
#ifdef TTC_REGRESSION
        USART_Generic->Amount_Queue_TX += Block->Size;
#endif
        ttc_mutex_unlock(&(USART_Generic->Queue_TX_Lock));

        ttc_interrupt_enable(tit_USART_TransmitDataEmpty,
                             ttc_usart_get_physicalindex(LogicalIndex),
                             1);
    }
    else {
        _ttc_usart_send_raw_blocking(LogicalIndex, Block->Buffer, Block->Size);
        ttc_memory_block_release(Block);
    }

    return tue_OK;
}
ttc_channel_errorcode_e ttc_usart_read_word(u8_t LogicalIndex, u16_t* Word) {
    Assert_USART(Word != NULL, ec_InvalidArgument);
    ttc_usart_config_t* USART_Generic = _ttc_usart_get_configuration(LogicalIndex);

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_usart_read_word_blocking(USART_Generic, Word);
#endif

    return tue_NotImplemented;
}
ttc_channel_errorcode_e ttc_usart_read_byte(u8_t LogicalIndex, u8_t* Byte) {
    Assert_USART(Byte != NULL, ec_InvalidArgument);
    ttc_usart_config_t* USART_Generic = _ttc_usart_get_configuration(LogicalIndex);

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_usart_read_byte_blocking(USART_Generic, Byte);
#endif

    return tue_NotImplemented;
}
u8_t ttc_usart_get_physicalindex(u8_t LogicalIndex) {
    Assert_USART(LogicalIndex > 0, ec_InvalidArgument); // logical USART-indices start at 1 !
    s8_t PhysicalIndex = -1;

    switch (LogicalIndex) {                // find corresponding USART as defined by makefile.100_board_*
#ifdef TTC_USART1
    case 1: PhysicalIndex = TTC_USART1; break; // TTC_USART1 = 0..TTC_AMOUNT_USARTS-1
#endif
#ifdef TTC_USART2
    case 2: PhysicalIndex = TTC_USART2; break; // TTC_USART2 = 0..TTC_AMOUNT_USARTS-1
#endif
#ifdef TTC_USART3
    case 3: PhysicalIndex = TTC_USART3; break; // TTC_USART3 = 0..TTC_AMOUNT_USARTS-1
#endif
#ifdef TTC_USART4
    case 4: PhysicalIndex = TTC_USART4; break; // TTC_USART4 = 0..TTC_AMOUNT_USARTS-1
#endif
#ifdef TTC_USART5
    case 5: PhysicalIndex = TTC_USART5; break; // TTC_USART5 = 0..TTC_AMOUNT_USARTS-1
#endif
    default: Assert_USART(0, ec_UNKNOWN); break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
    }
    Assert_USART(PhysicalIndex < TTC_USART_MAX_AMOUNT, ec_InvalidConfiguration); // given logical USART index is out of range
    Assert_USART(PhysicalIndex != -1, ec_InvalidConfiguration);   // error: invalid configuration or no low-level implementation available
    return PhysicalIndex;
}
ttc_channel_errorcode_e ttc_usart_stdout_set(u8_t LogicalIndex) {
    Assert_USART(LogicalIndex <= TTC_AMOUNT_USARTS, ec_DeviceNotFound);    // not egnough TTC_USARTs defined!
    Assert_USART(A(ttc_USARTs, LogicalIndex-1), ec_InvalidConfiguration);  // USART not yet initialized!

    ttc_usart_stdout_index = LogicalIndex;
    return tue_OK;
}
void ttc_usart_stdout_send_block(ttc_memory_block_t* Block) {
    if (ttc_usart_stdout_index) {
        ttc_usart_send_block(ttc_usart_stdout_index, Block);
    }
}
void ttc_usart_stdout_send_string(const u8_t* String, Base_t MaxSize) {
    if (ttc_usart_stdout_index) {
        if (ttc_memory_is_constant(String))
            ttc_usart_send_string_const(ttc_usart_stdout_index, (const char*) String, MaxSize);
        else
            ttc_usart_send_string(ttc_usart_stdout_index, (const char*) String, MaxSize);
    }
}
void ttc_usart_flush_tx(u8_t LogicalIndex) {
    ttc_usart_config_t* USART_Generic = _ttc_usart_get_configuration(LogicalIndex);
    if (USART_Generic->Flags.Bits.DelayedTransmits) { // delayed transmits have been activated for this USART

        // wait for transmit queue to run empty
        while (ttc_queue_pointer_get_amount(USART_Generic->Queue_Tx) > 0)
            ttc_task_yield();
    }
}

//} Function definitions
//{ private functions (do not call from outside!)

// Note: Blocking transmit functions can be used from GDB like "p _ttc_usart_send_string_blocking(...)"
ttc_channel_errorcode_e _ttc_usart_send_word_blocking(u8_t LogicalIndex, const u16_t Word) {
    ttc_usart_config_t* USART_Generic = _ttc_usart_get_configuration(LogicalIndex);

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_usart_send_word_blocking(USART_Generic, Word);
#endif

    return tue_NotImplemented;
}
ttc_channel_errorcode_e _ttc_usart_send_raw_blocking(u8_t LogicalIndex, const u8_t* Buffer, u16_t Amount) {
    ttc_usart_config_t* USART_Generic = _ttc_usart_get_configuration(LogicalIndex);
    ttc_channel_errorcode_e Error = tue_OK;

    while (ttc_mutex_lock(&(USART_Generic->Queue_TX_Lock), -1, _ttc_usart_send_raw_blocking) != tme_OK);
    while (Amount-- > 0) {
#ifdef TARGET_ARCHITECTURE_STM32F1xx
        Error = stm32_usart_send_word_blocking(USART_Generic, (u16_t) *Buffer++);
#else
        Error = tue_NotImplemented;
#endif
    }
    ttc_mutex_unlock(&(USART_Generic->Queue_TX_Lock));

    return Error;
}
ttc_channel_errorcode_e _ttc_usart_send_string_blocking(u8_t LogicalIndex, const char* Buffer, u16_t MaxLength) {
    Assert_USART(Buffer != NULL, ec_InvalidArgument);
    ttc_usart_config_t* USART_Generic = _ttc_usart_get_configuration(LogicalIndex);

    char C=0;
    ttc_channel_errorcode_e Error = tue_OK;
    ttc_mutex_lock(&(USART_Generic->Queue_TX_Lock), -1, _ttc_usart_send_string_blocking);
    while ( (MaxLength-- > 0) && ((C=*Buffer++) != 0)  ) {
        if (USART_Generic->activity_tx_isr)
            USART_Generic->activity_tx_isr(USART_Generic, C);

#ifdef TARGET_ARCHITECTURE_STM32F1xx
        Error = stm32_usart_send_word_blocking(USART_Generic, (u16_t) C);
#else
        Error = tue_NotImplemented;
#endif
        if (Error != tce_OK)
            break;
    }
    ttc_mutex_unlock(&(USART_Generic->Queue_TX_Lock));

    return Error;
}
void _ttc_usart_task_receive(void* Args) {
    ttc_usart_config_t* USART_Generic = (ttc_usart_config_t*) Args;
    Assert_USART(USART_Generic, ec_InvalidArgument);

    // Note: A mutex would be safer here, but it is very unlikely, that two tasks initialize the same USART in parallel
    Assert_USART(USART_Generic->Flags.Bits.TaskRxRunning == 0, ec_InvalidConfiguration); // task has alrready been spawn for this USART!
    USART_Generic->Flags.Bits.TaskRxRunning = 1;

    ttc_memory_block_t* CurrentRxBuffer = NULL;
    u8_t* Writer = NULL;
    Base_t Size = 0;
    u8_t Byte = 0;
    u16_t Char_EndOfLine = USART_Generic->Char_EndOfLine;

    while (1) {
        if (ttc_queue_byte_pull_front(USART_Generic->Queue_Rx, &Byte) == tqe_OK) { // single byte received: process it
#ifdef TTC_REGRESSION
            USART_Generic->Amount_Queue_RX++;
#endif

            if (USART_Generic->receiveBlock != NULL) {

                if (Writer == NULL) { // no buffer available: fetch new memory block from Queue_EmptyRxBlocks
                    if (!CurrentRxBuffer) { // no receive buffer: fetch one from queue
                        CurrentRxBuffer = ttc_usart_get_empty_block();
                        Assert_USART(CurrentRxBuffer != NULL, ec_NULL); // got NULL-pointer as pointer to memory block!
                    }
                    Writer = CurrentRxBuffer->Buffer;
                    Size = 0;
                }

                if (Writer == NULL) { // no buffer available: deliver single byte
                    Assert_USART(USART_Generic->receiveSingleByte != NULL, ec_InvalidConfiguration); // no memory block and no single receive function available!
                    USART_Generic->receiveSingleByte(USART_Generic, Byte);
                }
                else {                // write byte into buffer
                    *Writer++ = Byte;
                    Size++;

                    if (
                            ( (Char_EndOfLine < 0x100) && (Char_EndOfLine == Byte)  ) ||
                            (Size >= TTC_USART_BLOCK_SIZE)
                            ) {
                        CurrentRxBuffer->Size = Size;
                        *Writer = 0;

                        // buffer will be reused if receiveBlock() returns pointer != NULL
                        CurrentRxBuffer = USART_Generic->receiveBlock(USART_Generic->receiveBlock_Argument, USART_Generic, CurrentRxBuffer);
                        Writer = NULL;
                    }
                }
            }
            else if (USART_Generic->receiveSingleByte != NULL) { // deliver byte directly
                USART_Generic->receiveSingleByte(USART_Generic, Byte);
            }
            else Assert_USART(0, ec_InvalidConfiguration); // no function registered to pass received data to
        }
        else {
            ttc_task_yield();  // give cpu to other tasks while waiting
            Char_EndOfLine = USART_Generic->Char_EndOfLine; // maybe configuration has changed
        }
    }
}
void _ttc_usart_rx_isr(ttc_usart_config_t* USART_Generic, u8_t Byte) {

    Assert_USART(USART_Generic != NULL, ec_NULL);
    Assert_USART(USART_Generic->Queue_Rx != NULL, ec_NULL);
    ttc_queue_error_e PushError;

    if (USART_Generic->activity_rx_isr)
        USART_Generic->activity_rx_isr(USART_Generic, Byte);

    // send byte to receive task
    PushError = ttc_queue_byte_push_back_isr(USART_Generic->Queue_Rx, Byte);
#ifdef TTC_REGRESSION
    USART_Generic->Amount_ISR_RX++;
    Assert_USART(PushError < tqe_Error, ec_InvalidImplementation); // no bytes should be skipped in a regression test!
#endif

    if (PushError >= tqe_Error) {
        if (USART_Generic->Amount_RxBytesSkipped < 255){ // limit to 255
            USART_Generic->Amount_RxBytesSkipped++;
        } else {
            //Assert_USART(FALSE, ec_Debug); // ERROR: Too much bytes lost! (Seems as if your application is too slow)
        }
    }
}
void _ttc_usart_tx_isr(ttc_usart_config_t* USART_Generic, void* Argument) {
    Assert_USART(USART_Generic != NULL, ec_InvalidArgument);

    static ttc_memory_block_t* CurrentTxBuffer;
    static u8_t*  TxBuffer_Reader = NULL;
    static Base_t TxBuffer_Index  = 0;
    static Base_t TxBuffer_Size   = 0;
    static BOOL   ZeroTerminated  = FALSE;  // ==TRUE: string is zero terminated

    if (TxBuffer_Reader == NULL) { // try to get new buffer from Queue_Tx
        //X do {
        ttc_queue_error_e Error = ttc_queue_pointer_pull_front_isr(USART_Generic->Queue_Tx, (void**) &CurrentTxBuffer);

        if (Error == tqe_OK) { // buffer received: use it
            TxBuffer_Reader = CurrentTxBuffer->Buffer;
            TxBuffer_Index  = 0;
            TxBuffer_Size   = 0x7fffffff & CurrentTxBuffer->Size; // make it positive
            ZeroTerminated  = (CurrentTxBuffer->Size < 0) ? TRUE : FALSE;
        }
        else
            TxBuffer_Reader = NULL;
        //X } while ( (TxBuffer_Reader != NULL) && (TxBuffer_Size == 0) );
    }
    if (TxBuffer_Reader != NULL) { // have valid, non-empty TxBuffer: send single byte
        u8_t Byte = *TxBuffer_Reader++;
        TxBuffer_Index++;

        if (ZeroTerminated && (Byte == 0))
            TxBuffer_Index = TxBuffer_Size; // all bytes send
        else {
            if (USART_Generic->activity_tx_isr)
                USART_Generic->activity_tx_isr(USART_Generic, Byte);

#ifdef TARGET_ARCHITECTURE_STM32F1xx // ToDo: move into _driver_()
            register_stm32f1xx_usart_t* USART_Hardware = (register_stm32f1xx_usart_t*) Argument;
            _stm32_usart_send_byte_isr(USART_Hardware, Byte);       // fast low-level transmit
#else
            _ttc_usart_send_word_blocking(USART_Generic->LogicalIndex, Byte); // slow but platform independent transmit
#endif
#ifdef TTC_REGRESSION
            USART_Generic->Amount_ISR_TX++;
#endif
        }
        if (TxBuffer_Index >= TxBuffer_Size) { // all bytes send: release buffer
            _ttc_usart_release_block_isr(CurrentTxBuffer);
            TxBuffer_Reader = NULL;
            CurrentTxBuffer = NULL;
        }
    }
    else // no data to send: disable interrupt
        ttc_interrupt_enable(tit_USART_TransmitDataEmpty,
                             ttc_usart_get_physicalindex(USART_Generic->LogicalIndex),
                             0
                             );
}
void _ttc_usart_release_block(ttc_memory_block_t* Block) {
    Assert_USART(Block != NULL, ec_NULL);

    if (Block->releaseBuffer == _ttc_usart_release_block) {
        ttc_queue_pointer_push_back(&tm_QueueEmptyBlocks, Block); // ToDo: Change Queue -> List
    }
    else { // release foreign block
        Assert(Block->releaseBuffer, ec_NULL);
        ttc_memory_block_release(Block);
    }
}
void _ttc_usart_release_block_isr(ttc_memory_block_t* Block) {
    Assert_USART(Block != NULL, ec_NULL);

    if (Block->releaseBuffer == _ttc_usart_release_block) {
        ttc_queue_error_e Error = ttc_queue_pointer_push_back_isr(&tm_QueueEmptyBlocks, Block);
        Assert_USART(Error < tqe_Error, ec_UNKNOWN); // could not release memory block (maybe queue is too small?)
    }
    else { // release foreign block
        Assert(Block->releaseBuffer, ec_NULL);
        ttc_memory_block_release(Block); // ToDo: release_isr()???
    }
}
ttc_usart_config_t* _ttc_usart_get_configuration(u8_t LogicalIndex) {
    Assert_USART(LogicalIndex > 0, ec_InvalidArgument); // logical USART-indices start at 1 !
    ttc_usart_config_t* USART_Generic = A(ttc_USARTs, LogicalIndex-1);
    Assert_USART(USART_Generic, ec_NULL); // USART not yet created; call ttc_usart_get_configuration() before!

    return USART_Generic;
}

//} private functions
