#ifndef ttc_queue_types_H
#define ttc_queue_types_H

/*{ ttc_queue.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Architecture independent support for inter task communication & synchronization.
 *
 * Currently supported schedulers: FreeRTOS
 *
}*/
/*{ Details of Queue-Implementation

  ttc_queue provides two main types of queues:
  1) Generic Queues
     These are just a shallow interface to an external supplied queue implementation.
     The generic queue interface was developed on top of FreeRTOS queues. Other queue implementations may follow.
     Depending on their individual implementation, generic queues can consume lots of ram.

  2) Simple Queues
     The most basic datatypes being placed in queues are single bytes and pointers. In ttc_queue, specialized
     implementations for queueing these simple datatypes are provided. The implementations fully support multitasking
     and interrupt service routines with small extra ram usage. The support for interrupt service routines is special.
     When an _isr() function should operate on a simple queue, it can happen that this queue is currently in use by a
     task. This is indicated by Lock==1. In such a case, the isr has a problem. It cannot wait until the task finishes.
     For this situation, an extra space is reserved in the queue buffer for data pending to be pushed.
  }*/

//{ includes

#include "compile_options.h"
#include "ttc_basic_types.h"
#include "ttc_memory_types.h"
#include "ttc_mutex_types.h"
#include "ttc_task_types.h"
#include "ttc_semaphore_types.h"

#ifdef EXTENSION_300_scheduler_freertos
    #ifdef DEPRECATED
        #include "stddef.h"
        #include "FreeRTOS.h"
        #include "queue.h"
        #include "semphr.h"
    #endif

    typedef void* t_ttc_queue_generic;
#else
    typedef void* t_ttc_queue_generic;
#endif

typedef enum ttc_queue_error_E {      // return type of most functions in ttc_queue
    tqe_OK,                           // no error
    tqe_HigherPriorityTaskWoken,      // an _isr()-call has unblocked a higher priority task to unblock. Call ttc_task_yield() before exiting your ISR!
    tqe_IsrOperationPending,          // operation is stored for later processing. Call again to complete _isr() operation

    tqe_Error,
    // Error Conditions below
    tqe_TimeOut,                      // an operation has timed out
    tqe_NotImplemented,               // required function not implemented for current architecture
    tqe_QueuePushFailed,              // pushing data to a queue has failed
    tqe_QueueIsEmpty,                 // pulling data from a queue has failed because its empty
    tqe_QueueIsLocked,                // data could not be pulled/pushed, because queue is locked by another task
    tqe_UNKNOWN
} e_ttc_queue_error;

//}includes
//{ Default settings (change via makefile) ******************************

#ifndef TTC_ASSERT_QUEUES
    #define TTC_ASSERT_QUEUES 1
#endif

#ifndef TTC_QUEUE_USE_WAKEUP_INTERLEAVE
    #define TTC_QUEUE_USE_WAKEUP_INTERLEAVE 0  // ==1: do not wakeup waiting task every time (experimental)
#endif

//}Default settings
//{ Types & Structures ***************************************************


#if TTC_ASSERT_QUEUES == 1 // use Assert()s in Queue code (somewhat slower but alot easier to debug)
    #define Assert_QUEUES(Condition, Origin) Assert( ((Condition) != 0), Origin)
    #define Assert_QUEUES_Writable(Address, Origin) Assert_Writable(Address, Origin)
#else  // us no Assert()s in USART code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_QUEUES(Condition, Origin)
    #define Assert_QUEUES_Writable(Address, Origin)
#endif

// extra memory space in queue reserved as buffer for ISR-operations
#define TTC_SQB_ISR_SPACE 7 // check t_ttc_queue_bytes to get proper aligned end of PendingPush[]!

typedef struct s_ttc_queue_waiting_task {
    TaskHandle_t Task;                        // handle of task waiting on queue
    struct s_ttc_queue_waiting_task* Next;
} t_ttc_queue_waiting_task;

typedef struct { // data of a simple queue for 8-bit values
    t_ttc_task_waiting_list TasksWaiting2Push; // list of sleeping tasks waiting to push to this queue
    t_ttc_task_waiting_list TasksWaiting2Pull; // list of sleeping tasks waiting to pull from this queue
    t_u8* First;                               // lowest storage address of of Data[]
    t_u8* Last;                                // highest storage address of of Data[]
    t_u8* FirstUsed;                           // address of first valid element in Data[]
    t_u8* FirstUnused;                         // address of last valid element in Data[]

#ifdef TTC_REGRESSION
    t_base Size;                               // size of Data[]
#endif

#if TTC_QUEUE_USE_WAKEUP_INTERLEAVE == 1
    // experimental stuff: reduce amount of ttc_tack_resmue_now() calls
    t_u8 Interleave_WakeUpPusher;
    t_u8 Interleave_WakeUpPuller;
    t_u8 Counter_WakeUpPusher;
    t_u8 Counter_WakeUpPuller;
#endif

#if TTC_SQB_ISR_SPACE > 0
    t_u8 AmountPendingPushes;                  // amount of ISR-pushs still pending to be processed
    t_u8 PendingPush[TTC_SQB_ISR_SPACE];       // data pending to be pushed to queue
    //
    // Note: t_u8 AmountPendingPushes requires proper value of TTC_SQB_ISR_SPACE to ensure correct alignment!
#  ifdef TTC_REGRESSION
    t_base AmountPendingTotal;                 // total amount of pending operations in this queue so far
    t_base AmountTaskWaiting2Push;             // total amount of tasks being put to sleep while waiting to push
    t_base AmountTaskWaiting2Pull;             // total amount of tasks being put to sleep while waiting to pull
#  endif
#endif
    t_u8* Data;                                // stores data in circular buffer
    t_ttc_mutex_smart Lock;                    // protects queue from concurrent accesses
} t_ttc_queue_bytes;

typedef struct { // data of a simple queue for pointer values
    t_ttc_mutex_smart Lock;                    // protects queue from concurrent accesses
    t_ttc_task_waiting_list TasksWaiting2Push; // list of sleeping tasks waiting to push to this queue
    t_ttc_task_waiting_list TasksWaiting2Pull; // list of sleeping tasks waiting to pull from this queue
    t_base FirstUsed;                          // index of first valid element in Data[]
    t_base FirstUnused;                        // index of last valid element in Data[]
    t_base Size;                               // size of Data[]

#if TTC_QUEUE_USE_WAKEUP_INTERLEAVE == 1
    // experimental stuff: reduce amount of ttc_tack_resmue_now() calls
    t_u8 Interleave_WakeUpPusher;
    t_u8 Interleave_WakeUpPuller;
    t_u8 Counter_WakeUpPusher;
    t_u8 Counter_WakeUpPuller;
#endif

#if TTC_SQB_ISR_SPACE > 0
    t_u8 AmountPendingPushes;                  // amount of ISR-pushs still pending to be processed
    void* PendingPush[TTC_SQB_ISR_SPACE];      // data pending to be pushed to queue
    //
    // Note: t_u8 AmountPendingPushes requires proper value of TTC_SQB_ISR_SPACE to ensure correct alignment!
#  ifdef TTC_REGRESSION
    t_base AmountPendingTotal;                 // total amount of pending operations in this queue so far
    t_base AmountTaskWaiting2Push;             // total amount of tasks being put to sleep while waiting to push
    t_base AmountTaskWaiting2Pull;             // total amount of tasks being put to sleep while waiting to pull
#  endif
#endif
    void** Data;                                // stores data in circular buffer
} t_ttc_queue_pointers;

//}


#endif
