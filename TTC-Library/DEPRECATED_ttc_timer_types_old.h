/** { ttc_timer_types.h ************************************************

                           The ToolChain

   High-Level interface for TIMER device.
   
   Structures, Enums and Defines being required by both, high- and low-level timer.
   

   Authors:
   
}*/

#ifndef TTC_TIMER_TYPES_H
#define TTC_TIMER_TYPES_H

#include "ttc_basic.h"
#include "ttc_task_types.h"
#ifdef TARGET_ARCHITECTURE_STM32F1xx
#include "stm32f1/stm32f1_timer_types.h"
#endif

#ifdef TARGET_ARCHITECTURE_STM32L1xx
#include "timer/timer_stm32l1_types.h"
#endif

//{ Defines ***************************************************
//  defines that may be overridden in makefile via "COMPILE_OPTS += -D..."
// TTC_TIMERn has to be defined as constant by makefile.100_board_*
#ifdef TTC_TIMER17
#ifndef TTC_TIMER16
    #error TTC_TIMER17 is defined, but not TTC_TIMER16 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER15
    #error TTC_TIMER17 is defined, but not TTC_TIMER15 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER14
    #error TTC_TIMER17 is defined, but not TTC_TIMER14 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER13
    #error TTC_TIMER17 is defined, but not TTC_TIMER13 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER12
    #error TTC_TIMER17 is defined, but not TTC_TIMER12 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER11
    #error TTC_TIMER17 is defined, but not TTC_TIMER11 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER10
    #error TTC_TIMER17 is defined, but not TTC_TIMER10 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER9
    #error TTC_TIMER17 is defined, but not TTC_TIMER9 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER8
    #error TTC_TIMER17 is defined, but not TTC_TIMER8 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER7
    #error TTC_TIMER17 is defined, but not TTC_TIMER7 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER6
    #error TTC_TIMER17 is defined, but not TTC_TIMER6 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER5
    #error TTC_TIMER17 is defined, but not TTC_TIMER5 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER4
    #error TTC_TIMER17 is defined, but not TTC_TIMER4 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER3
    #error TTC_TIMER17 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER2
    #error TTC_TIMER17 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER1
    #error TTC_TIMER17 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
  #endif

#define TTC_INTERRUPT_TIMER_AMOUNT 17
#elif TTC_TIMER16
  #ifndef TTC_TIMER15
    #error TTC_TIMER16 is defined, but not TTC_TIMER15 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER14
    #error TTC_TIMER16 is defined, but not TTC_TIMER14 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER13
    #error TTC_TIMER16 is defined, but not TTC_TIMER13 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER12
    #error TTC_TIMER16 is defined, but not TTC_TIMER12 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER11
    #error TTC_TIMER16 is defined, but not TTC_TIMER11 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER10
    #error TTC_TIMER16 is defined, but not TTC_TIMER10 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER9
    #error TTC_TIMER16 is defined, but not TTC_TIMER9 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER8
    #error TTC_TIMER16 is defined, but not TTC_TIMER8 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER7
    #error TTC_TIMER16 is defined, but not TTC_TIMER7 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER6
    #error TTC_TIMER16 is defined, but not TTC_TIMER6 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER5
    #error TTC_TIMER16 is defined, but not TTC_TIMER5 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER4
    #error TTC_TIMER16 is defined, but not TTC_TIMER4 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER3
    #error TTC_TIMER16 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER2
    #error TTC_TIMER16 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER1
    #error TTC_TIMER16 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
  #endif

#define TTC_INTERRUPT_TIMER_AMOUNT 16
#elif TTC_TIMER15
  #ifndef TTC_TIMER14
    #error TTC_TIMER15 is defined, but not TTC_TIMER14 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER13
    #error TTC_TIMER15 is defined, but not TTC_TIMER13 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER12
    #error TTC_TIMER15 is defined, but not TTC_TIMER12 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER11
    #error TTC_TIMER15 is defined, but not TTC_TIMER11 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER10
    #error TTC_TIMER15 is defined, but not TTC_TIMER10 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER9
    #error TTC_TIMER15 is defined, but not TTC_TIMER9 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER8
    #error TTC_TIMER15 is defined, but not TTC_TIMER8 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER7
    #error TTC_TIMER15 is defined, but not TTC_TIMER7 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER6
    #error TTC_TIMER15 is defined, but not TTC_TIMER6 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER5
    #error TTC_TIMER15 is defined, but not TTC_TIMER5 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER4
    #error TTC_TIMER15 is defined, but not TTC_TIMER4 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER3
    #error TTC_TIMER15 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER2
    #error TTC_TIMER15 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER1
    #error TTC_TIMER15 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
  #endif

#define TTC_INTERRUPT_TIMER_AMOUNT 15
#elif TTC_TIMER14
  #ifndef TTC_TIMER13
    #error TTC_TIMER14 is defined, but not TTC_TIMER13 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER12
    #error TTC_TIMER14 is defined, but not TTC_TIMER12 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER11
    #error TTC_TIMER14 is defined, but not TTC_TIMER11 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER10
    #error TTC_TIMER14 is defined, but not TTC_TIMER10 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER9
    #error TTC_TIMER14 is defined, but not TTC_TIMER9 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER8
    #error TTC_TIMER14 is defined, but not TTC_TIMER8 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER7
    #error TTC_TIMER14 is defined, but not TTC_TIMER7 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER6
    #error TTC_TIMER14 is defined, but not TTC_TIMER6 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER5
    #error TTC_TIMER14 is defined, but not TTC_TIMER5 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER4
    #error TTC_TIMER14 is defined, but not TTC_TIMER4 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER3
    #error TTC_TIMER14 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER2
    #error TTC_TIMER14 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER1
    #error TTC_TIMER14 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
  #endif

#define TTC_INTERRUPT_TIMER_AMOUNT 14
#elif TTC_TIMER13
  #ifndef TTC_TIMER12
    #error TTC_TIMER13 is defined, but not TTC_TIMER12 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER11
    #error TTC_TIMER13 is defined, but not TTC_TIMER11 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER10
    #error TTC_TIMER13 is defined, but not TTC_TIMER10 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER9
    #error TTC_TIMER13 is defined, but not TTC_TIMER9 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER8
    #error TTC_TIMER13 is defined, but not TTC_TIMER8 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER7
    #error TTC_TIMER13 is defined, but not TTC_TIMER7 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER6
    #error TTC_TIMER13 is defined, but not TTC_TIMER6 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER5
    #error TTC_TIMER13 is defined, but not TTC_TIMER5 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER4
    #error TTC_TIMER13 is defined, but not TTC_TIMER4 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER3
    #error TTC_TIMER13 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER2
    #error TTC_TIMER13 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER1
    #error TTC_TIMER13 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
  #endif

#define TTC_INTERRUPT_TIMER_AMOUNT 13
#elif TTC_TIMER12
  #ifndef TTC_TIMER11
    #error TTC_TIMER12 is defined, but not TTC_TIMER11 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER10
    #error TTC_TIMER12 is defined, but not TTC_TIMER10 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER9
    #error TTC_TIMER12 is defined, but not TTC_TIMER9 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER8
    #error TTC_TIMER12 is defined, but not TTC_TIMER8 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER7
    #error TTC_TIMER12 is defined, but not TTC_TIMER7 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER6
    #error TTC_TIMER12 is defined, but not TTC_TIMER6 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER5
    #error TTC_TIMER12 is defined, but not TTC_TIMER5 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER4
    #error TTC_TIMER12 is defined, but not TTC_TIMER4 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER3
    #error TTC_TIMER12 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER2
    #error TTC_TIMER12 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER1
    #error TTC_TIMER12 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
  #endif

#define TTC_INTERRUPT_TIMER_AMOUNT 12
#elif TTC_TIMER11
  #ifndef TTC_TIMER10
    #error TTC_TIMER11 is defined, but not TTC_TIMER10 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER9
    #error TTC_TIMER11 is defined, but not TTC_TIMER9 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER8
    #error TTC_TIMER11 is defined, but not TTC_TIMER8 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER7
    #error TTC_TIMER11 is defined, but not TTC_TIMER7 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER6
    #error TTC_TIMER11 is defined, but not TTC_TIMER6 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER5
    #error TTC_TIMER11 is defined, but not TTC_TIMER5 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER4
    #error TTC_TIMER11 is defined, but not TTC_TIMER4 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER3
    #error TTC_TIMER11 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER2
    #error TTC_TIMER11 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER1
    #error TTC_TIMER11 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
  #endif

#define TTC_INTERRUPT_TIMER_AMOUNT 11
#elif TTC_TIMER10
  #ifndef TTC_TIMER9
    #error TTC_TIMER10 is defined, but not TTC_TIMER9 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER8
    #error TTC_TIMER10 is defined, but not TTC_TIMER8 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER7
    #error TTC_TIMER10 is defined, but not TTC_TIMER7 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER6
    #error TTC_TIMER10 is defined, but not TTC_TIMER6 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER5
    #error TTC_TIMER10 is defined, but not TTC_TIMER5 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER4
    #error TTC_TIMER10 is defined, but not TTC_TIMER4 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER3
    #error TTC_TIMER10 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER2
    #error TTC_TIMER10 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER1
    #error TTC_TIMER10 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
  #endif

#define TTC_INTERRUPT_TIMER_AMOUNT 10
#elif TTC_TIMER9
  #ifndef TTC_TIMER8
    #error TTC_TIMER9 is defined, but not TTC_TIMER8 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER7
    #error TTC_TIMER9 is defined, but not TTC_TIMER7 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER6
    #error TTC_TIMER9 is defined, but not TTC_TIMER6 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER5
    #error TTC_TIMER9 is defined, but not TTC_TIMER5 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER4
    #error TTC_TIMER9 is defined, but not TTC_TIMER4 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER3
    #error TTC_TIMER9 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER2
    #error TTC_TIMER9 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER1
    #error TTC_TIMER9 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
  #endif

#define TTC_INTERRUPT_TIMER_AMOUNT 9
#elif TTC_TIMER8
  #ifndef TTC_TIMER7
    #error TTC_TIMER8 is defined, but not TTC_TIMER7 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER6
    #error TTC_TIMER8 is defined, but not TTC_TIMER6 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER5
    #error TTC_TIMER8 is defined, but not TTC_TIMER5 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER4
    #error TTC_TIMER8 is defined, but not TTC_TIMER4 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER3
    #error TTC_TIMER8 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER2
    #error TTC_TIMER8 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER1
    #error TTC_TIMER8 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
  #endif

#define TTC_INTERRUPT_TIMER_AMOUNT 8
#elif TTC_TIMER7
  #ifndef TTC_TIMER6
    #error TTC_TIMER7 is defined, but not TTC_TIMER6 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER5
    #error TTC_TIMER7 is defined, but not TTC_TIMER5 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER4
    #error TTC_TIMER7 is defined, but not TTC_TIMER4 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER3
    #error TTC_TIMER7 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER2
    #error TTC_TIMER7 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER1
    #error TTC_TIMER7 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
  #endif

#define TTC_INTERRUPT_TIMER_AMOUNT 7
#elif TTC_TIMER6
  #ifndef TTC_TIMER5
    #error TTC_TIMER6 is defined, but not TTC_TIMER5 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER4
    #error TTC_TIMER6 is defined, but not TTC_TIMER4 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER3
    #error TTC_TIMER6 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER2
    #error TTC_TIMER6 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER1
    #error TTC_TIMER6 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
  #endif

#define TTC_INTERRUPT_TIMER_AMOUNT 6
#elif TTC_TIMER5
#ifndef TTC_TIMER4
    #error TTC_TIMER5 is defined, but not TTC_TIMER4 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER3
    #error TTC_TIMER5 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER2
    #error TTC_TIMER5 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER1
    #error TTC_TIMER5 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
  #endif

  #define TTC_INTERRUPT_TIMER_AMOUNT 5
#else
  #ifdef TTC_TIMER4
    #define TTC_INTERRUPT_TIMER_AMOUNT 4

    #ifndef TTC_TIMER3
      #error TTC_TIMER4 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
    #endif
    #ifndef TTC_TIMER2
      #error TTC_TIMER4 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
    #endif
    #ifndef TTC_TIMER1
      #error TTC_TIMER4 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
    #endif
  #else
    #ifdef TTC_TIMER3

      #ifndef TTC_TIMER2
        #error TTC_TIMER3 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
      #endif
      #ifndef TTC_TIMER1
        #error TTC_TIMER3 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
      #endif

      #define TTC_INTERRUPT_TIMER_AMOUNT 3
    #else
      #ifdef TTC_TIMER2

        #ifndef TTC_TIMER1
          #error TTC_TIMER2 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
        #endif

        #define TTC_INTERRUPT_TIMER_AMOUNT 2
      #else
        #ifdef TTC_TIMER1
          #define TTC_INTERRUPT_TIMER_AMOUNT 1
        #else
          #define TTC_INTERRUPT_TIMER_AMOUNT 0
        #endif
      #endif
    #endif
  #endif
#endif

#define TTC_TIMER_MAX_CHANNELS 4

//} Defines
//{ Static Configuration **************************************
#ifndef TTC_ASSERT_TIMER    // any previous definition set (Makefile)?
#define TTC_ASSERT_TIMER 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_TIMER == 1)  // use Assert()s in TIMER code (somewhat slower but alot easier to debug)
  #define Assert_TIMER(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in TIMER code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_TIMER(Condition, ErrorCode)
#endif

//}Static Configuration
//{ Enums/ Structures *****************************************

typedef enum {   // ttc_timer_errorcode_e
    tte_OK,                // =0: no error
    tte_DeviceNotFound,    // adressed TIMER device not available in current uC
    tte_NotImplemented,    // function has no implementation for current architecture
    tte_InvalidArgument,   // general argument error
    tte_InvalidConfiguration,  // one or more pins have been configured incorrectly -> check makefile for TTC_USARTxxx defines
    tte_DeviceNotReady,    // choosen device has not been initialized properly

    tte_UnknownError
} ttc_timer_errorcode_e;
typedef enum{
    tts_Update,
    tts_CC1,
    tts_CC2,
    tts_CC3,
    tts_CC4,
    tts_COM,
    tts_Trigger,
    tts_Break,
    tts_CC1OF,
    tts_CC2OF,
    tts_CC3OF,
    tts_CC4OF,

    tts_Unknown
} ttc_timer_statuscode_e;
typedef struct ttc_time_event_s{
    void (*Function)();
    void* Argument;
    Base_t TimeStart;
    Base_t TimePeriod;
    bool Continuous;
    struct ttc_time_event_s* Next; //pointer on the next Time Event
}ttc_time_event_t;
typedef struct ttc_timer_config_s {
    // Note: Write-access to this structure is only allowed before first ttc_timer_init() call!
    //       After initialization, only read accesses are allowed for high-level code.
    ttc_timer_errorcode_e Architecture;
    u8_t  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_TIMER1, ...)
    void* LowLevelConfig;  // low-level configuration (structure known by low-level driver only)
    union  { // generic configuration bits common for all low-level Drivers
        u16_t All;
        struct {
          unsigned Initialized        : 1;  // ==1: device has been initialized successfully
          // ToDo: additional high-level flags go here..

            unsigned Reserved1        : 15; // pad to 16 bits
        } Bits;
    } Flags;
    // Additional high-level attributes go here.
    //This structure defines all the parameter for the timer and it is the structure used by Software Timers
    ttc_time_event_t TimerEvent;

    // architecture dependent TIMER configuration (each TIMER requires its own!)
    ttc_timer_arch_t* Timer_Arch;

    struct ttc_timer_config_s* Next; //pointer on the next Time Event

    u32_t Counter; //General Timer Counter

    u8_t TimeScale; //Time scale flag. 0: uSecs, 1: mSecs

    // Interrupt Chaining
    void (*eti_NextISR)();
    void* eti_NextISR_Argument;

} __attribute__((__packed__)) ttc_timer_config_t;

//}Structures

#endif // TTC_TIMER_TYPES_H
