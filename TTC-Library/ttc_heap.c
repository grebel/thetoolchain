/** { ttc_heap.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for heap devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 23 at 20140301 07:28:17 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_heap.h"
#include "ttc_interrupt.h"
#include "ttc_semaphore.h"
#include "ttc_mutex.h"
#include "ttc_list.h"

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of heap devices.
 *
 */


#if TTC_HEAP_ENUMERATE == 1
    t_base ttc_memory_Count_Alloc = 1;
    t_base ttc_memory_Count_PoolAlloc = 1;
#endif

#if TTC_HEAP_STATISTICS == 1

    // amount of blocks allocated on heap
    t_base ttc_heap_Amount_Blocks_Allocated = 0;

    // amount of bytes occupied on heap by dynamic allocations
    t_base ttc_heap_Bytes_Allocated_Total   = 0;

    // usable amount of allocated bytes
    t_base ttc_heap_Bytes_Allocated_Usable  = 0;

#endif


#if TTC_HEAP_RECORDS > 0

    // stores recorded data of latest memory allocations
    A_define( t_ttc_heap_record, ttc_heap_Records, TTC_HEAP_RECORDS );

    // index in ttc_heap_Records[] of first unused entry
    t_u16 ttc_memory_first_free_record = 0;

#endif

// start address of heap is calculated by calling ttc_heap_prepare()
t_base ttc_memory_HeapSize = 0;

// size of heap is calculated by calling ttc_heap_prepare()
volatile t_u8* ttc_memory_HeapStart = NULL;

// size of heap is calculated by calling ttc_heap_prepare()
volatile t_u8* ttc_memory_HeapEnd = NULL;

// >0: size of a temporary allocated chunk of memory
t_base ttc_heap_TemporarySize = 0;

extern BOOL ttc_task_SchedulerIsRunning; // will use critical sections if scheduler has been started
//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Private Functions ******************************************************

/** checks consistency of given memory pool and all currently free blocks
 *
 * @param Pool      memory pool created via ttc_heap_pool_create()
 */
void                        _ttc_heap_pool_check( t_ttc_heap_pool* Pool ) {
    ttc_interrupt_critical_begin();

#if TTC_HEAP_POOL_STATISTICS == 1
    volatile t_u8 Available = ttc_semaphore_available( & ( Pool->BlocksAvailable ) );

    volatile t_u8 ListSize  = ttc_list_count_items_safe( ( t_ttc_list_item* ) Pool->FirstFree, Pool->Statistics.AmountAllocated );
    if ( Pool->FirstFree_FromISR )
    { ListSize += ttc_list_count_items_safe( ( t_ttc_list_item* ) Pool->FirstFree_FromISR, Pool->Statistics.AmountAllocated ); }

    Assert_HEAP( Available == ListSize, ttc_assert_origin_auto );
    Assert_HEAP( Available                                   <= Pool->Statistics.AmountAllocated, ttc_assert_origin_auto );

    // Note: These checks will only work until counters in Statistics overrun.
    //       Do not call _ttc_heap_pool_check() when TTC_HEAP_POOL_STATISTICS == 1 in productive environments!
    //
    //Assert_HEAP( Pool->Statistics.AmountFreeCurrently        <= Pool->Statistics.AmountAllocated, ttc_assert_origin_auto );
    //Assert_HEAP( Pool->Statistics.AmountFreeCurrentlyFromISR <= Pool->Statistics.AmountAllocated, ttc_assert_origin_auto );
#endif

    t_ttc_heap_block_from_pool* Block = Pool->FirstFree;
    while ( Block ) {
        Assert_HEAP( Block->ListItem.Next != TTC_LIST_TERMINATOR, ttc_assert_origin_auto );  // block has already being used in a ttc_list_*() call!
        Block = ( t_ttc_heap_block_from_pool* ) Block->ListItem.Next;
    }
    ttc_interrupt_critical_end();
}

/** allocate a new/ reuses a released memory block from given memory pool
 *
 * Note: This function is private to ttc_heap and must not be called from outside!
 * Note: This function is to be used from tasks and not from interrupt service routines!
 * Note: Returned address points to allocated memory directly AFTER header struct and can be directly casted to something else
 * Note: Pool->Lock must be locked or interrupts being disabled before calling this function!
 *
 * @param Pool      memory pool created via ttc_heap_pool_create()
 * @return          =! NULL: address of newly allocated or reused  memory block without header
 *                  == NULL: memory pool could not provide another memory block (blocks limited/ out of memory)
 */
t_ttc_heap_block_from_pool* _ttc_heap_pool_block_get( t_ttc_heap_pool* Pool ) {

    t_ttc_heap_block_from_pool* NewBlock = Pool->FirstFree;
    if ( NewBlock ) { // reusing a released memory block
        Assert_HEAP_Writable( NewBlock, ttc_assert_origin_auto );
        Assert_HEAP( ( NewBlock->ListItem.Next == NULL ) || ( NewBlock->ListItem.Next == TTC_LIST_TERMINATOR ) || ttc_memory_is_writable( NewBlock->ListItem.Next ), ttc_assert_origin_auto );
        Pool->FirstFree = ( t_ttc_heap_block_from_pool* ) NewBlock->ListItem.Next;
        NewBlock->ListItem.Next = NULL;

        Assert_HEAP_Writable( NewBlock->Pool, ttc_assert_origin_auto );
#if TTC_HEAP_POOL_STATISTICS == 1
        ttc_interrupt_critical_begin(); // ensure that no interrupt is corrupting statistical counters
        Pool->Statistics.AmountFreeCurrently--;
        ttc_interrupt_critical_end();
#endif
#if (TTC_HEAP_MAGICKEY != 0)
        Assert_HEAP( NewBlock->MagicKey == ( void* ) TTC_HEAP_MAGICKEY, ttc_assert_origin_auto );  // this memory block has been corrupted!
#endif
    }

    return NewBlock;
}

/** allocate a new/ reuses a released memory block from isr part of given memory pool
 *
 * Note: This function is private to ttc_heap and must not be called from outside!
 * Note: This function is to be used from tasks and not from interrupt service routines!
 * Note: Returned address points to allocated memory directly AFTER header struct and can be directly casted to something else
 * Note: Function must be called with disabled interrupts. (Call ttc_interrupt_critical_begin() before and ttc_interrupt_critical_end() after!)
 *
 * @param Pool      memory pool created via ttc_heap_pool_create()
 * @return          =! NULL: address of newly allocated or reused  memory block without header
 *                  == NULL: memory pool could not provide another memory block (blocks limited/ out of memory)
 */
t_ttc_heap_block_from_pool* _ttc_heap_pool_block_get_from_isr( t_ttc_heap_pool* Pool ) {

    t_ttc_heap_block_from_pool* NewBlock = Pool->FirstFree_FromISR;
    if ( NewBlock ) {  // blocks have been freed from interrupt service routine: reuse them first
        Pool->FirstFree_FromISR  = ( t_ttc_heap_block_from_pool* ) NewBlock->ListItem.Next;
        NewBlock->ListItem.Next = NULL;
        Assert_HEAP_Writable( NewBlock->Pool, ttc_assert_origin_auto );
#if TTC_HEAP_POOL_STATISTICS == 1
        Pool->Statistics.AmountFreeCurrently--;
#endif
#if (TTC_HEAP_MAGICKEY != 0)
        Assert_HEAP( NewBlock->MagicKey == ( void* ) TTC_HEAP_MAGICKEY, ttc_assert_origin_auto );  // this memory block has been corrupted!
#endif
    }

    return NewBlock;
}

/** calculate size of memory chunk being allocated for every memory block in given pool
 *
 * A memory block for a pool consists of
 * - Payload
 * - Header
 * - 0..3 Pad Bytes (for 32-/ 16- bit alignment)
 *
 * @return  amount of bytes required to store each pool block
 */
t_base                      _ttc_heap_pool_calculate_block_size( t_ttc_heap_pool* Pool ) {

    t_base BlockSize = sizeof( t_ttc_heap_block_from_pool ) + Pool->BlockSize;
    while ( BlockSize & 0x3 ) // add pad bytes to allow 32 bit alignment of every block
    { BlockSize++; }

    return BlockSize;
}

/** initialize given memory chunk as an array of pool blocks
 *
 * Note: Access to pool must be locked before and unlocked after calling this function!
 *
 * @param Pool      memory pool created via ttc_heap_pool_create()
 * @param Blocks    single chunk of memory to initialize as an array of pool blocks (must store sizeof( t_ttc_heap_block_from_pool ) + Pool->BlockSize bytes!)
 * @param Amount    amount of blocks to initialize (== size of Blocks[])
 * @return (t_ttc_heap_block_from_pool*)  pointer to last initialized pool block
 */
t_ttc_heap_block_from_pool* _ttc_heap_pool_blocks_init( t_ttc_heap_pool* const Pool, t_ttc_heap_block_from_pool* Blocks, t_base Amount ) {
    Assert_HEAP_Writable( Pool,   ttc_assert_origin_auto ); // invalid pointer argument
    Assert_HEAP_Writable( Blocks, ttc_assert_origin_auto ); // invalid pointer argument
    Assert_HEAP_EXTRA( Amount > 0, ttc_assert_origin_auto ); // makes no sense to call this low-level function with Amount==0. Check implementation of caller!

    t_ttc_heap_block_from_pool* LastBlock = NULL;

    t_base BlockSize = _ttc_heap_pool_calculate_block_size( Pool );

#if TTC_HEAP_STATISTICS == 1
    ttc_heap_Bytes_Allocated_Usable -= sizeof( t_ttc_heap_block_from_pool ) * MaxAllocated; // correct statistics
    ttc_heap_Bytes_Allocated_Usable += sizeof( NewBlock->ListItem * MaxAllocated );
#endif

    t_ttc_heap_block_from_pool* NewBlock = Blocks;
    for ( t_base Remaining = Amount; Remaining > 0; Remaining-- ) { // initialize all new blocks
        NewBlock->Pool = Pool;
#if TTC_HEAP_ENUMERATE == 1
        NewBlock->Enum = ( t_u16 ) ttc_memory_Count_PoolAlloc++; // every memory block gets its own
#endif
#if (TTC_HEAP_MAGICKEY != 0)
        NewBlock->MagicKey = ( void* ) TTC_HEAP_MAGICKEY;
#endif
#if TTC_HEAP_POOL_STATISTICS == 1
        NewBlock->MyNextBlock = NULL;
        NewBlock->MyOwner     = ( void* ) 0xdeadaffe;
#endif
        if ( Remaining > 1 ) { // calculate address of next block except for the last one
            NewBlock->ListItem.Next = ( t_ttc_list_item* )( ( ( t_base ) NewBlock ) + BlockSize );
#if TTC_HEAP_POOL_STATISTICS == 1
            NewBlock->MyNextBlock   = ( struct s_ttc_heap_block_from_pool* ) NewBlock->ListItem.Next;
#endif
            NewBlock = ( t_ttc_heap_block_from_pool* ) NewBlock->ListItem.Next;
        }
    }
    NewBlock->ListItem.Next = NULL; // terminate last list item
#if TTC_HEAP_POOL_STATISTICS == 1
    NewBlock->MyNextBlock   = NULL;
#endif
    LastBlock = NewBlock;
    Assert_HEAP( ( t_base ) NewBlock + BlockSize == ( t_base ) Blocks + BlockSize * Amount, ttc_assert_origin_auto );  // ran out of allocated memory block!

    return LastBlock;
}

//}Private Function declarations
//{ Function definitions ***************************************************

void                        ttc_heap_prepare() {

    // Check basic configuration
    // Note: The compiler may think that these comparison are always true. The compiler is wrong. ;)
    //
#if TTC_HEAP_RECORDS > 0
    A_reset( ttc_heap_Records, TTC_HEAP_RECORDS ); // must reset data of safe array manually!
#endif

#if (TTC_ASSERT_HEAP == 1)
    volatile unsigned char* Address;
    Address = ( volatile unsigned char* ) &_ttc_heap_start;
    Assert_HEAP( Address != NULL, ttc_assert_origin_auto );  // must be set by linker script ttc-lib/_linker/memory_*.ld for current uC!
    Address = ( volatile unsigned char* ) &_ttc_heap_end;
    Assert_HEAP( Address != NULL, ttc_assert_origin_auto );  // must be set by linker script ttc-lib/_linker/memory_*.ld for current uC!

    // ensure that this function is not optimized away so that is available during gdb session
    ttc_heap_pool_debug( NULL );
#endif

    // Load address of symbols from ttc_lib/linker/memory_basic.ld into variables.

    ttc_memory_HeapStart = &_ttc_heap_start;
    ttc_memory_HeapEnd   = &_ttc_heap_end;
    ttc_memory_HeapSize  = ttc_memory_HeapEnd - ttc_memory_HeapStart;
    Assert_HEAP( ttc_memory_HeapSize <= 0x100000, ttc_assert_origin_auto );  // Assuming error if >1MB of heap is available

#if TTC_HEAP_STATISTICS == 1
    ttc_heap_Amount_Blocks_Allocated = 0;
    ttc_heap_Bytes_Allocated_Total   = 0;
    ttc_heap_Bytes_Allocated_Usable  = 0;
#endif

#if TTC_HEAP_RECORDS > 0
    ttc_memory_set( &( A( ttc_heap_Records, 0 ) ), 0, sizeof( t_ttc_heap_record ) * TTC_HEAP_RECORDS );
    ttc_memory_first_free_record = 0;
#endif

#ifdef _driver_heap_prepare
    _driver_heap_prepare( ttc_memory_HeapStart, ttc_memory_HeapSize );
#else
#  warning Missing low-level implementation for _driver_heap_prepare()
#endif

}
void*                       ttc_heap_alloc( t_base Size ) {
    Assert_HEAP( Size, ttc_assert_origin_auto );
    Assert_HEAP( ttc_memory_HeapSize, ttc_assert_origin_auto );  // ttc_heap is not yet prepared!
    Assert_HEAP( ttc_heap_TemporarySize == 0, ttc_assert_origin_auto ); // somebody else already issued a temporary allocation. No allocations allowed while temporary allocation is active!

    void* Address = _driver_heap_alloc( Size );
    if ( !Address )
    { ttc_assert_halt_origin( ec_heap_OutOfMemory ); } // block could not be allocated. Did you allocate more memory than available?

    return Address;
}
void*                       ttc_heap_alloc_zeroed( t_base Size ) {
    Assert_HEAP( Size, ttc_assert_origin_auto );
    Assert_HEAP( ttc_heap_TemporarySize == 0, ttc_assert_origin_auto ); // somebody else already issued a temporary allocation. No allocations allowed while temporary allocation is active!

#ifdef _driver_heap_alloc_zeroed
    return _driver_heap_alloc_zeroed( Size );
#else
    void* Address = ttc_heap_alloc( Size );
    ttc_memory_set( Address, 0, Size );

    return Address;
#endif
}
t_ttc_heap_block*           ttc_heap_alloc_block( t_base Size, t_u8 Hint, void ( *releaseBuffer )( t_ttc_heap_block* Block ) ) {
    t_ttc_heap_block* MemoryBlock = ttc_heap_alloc( sizeof( t_ttc_heap_block ) + Size );

    MemoryBlock->Size          = 0;
    MemoryBlock->MaxSize       = Size;
    MemoryBlock->UseCount      = 1;
    MemoryBlock->Hint          = Hint;
    MemoryBlock->releaseBuffer = releaseBuffer;
    MemoryBlock->Buffer        = ( ( t_u8* ) MemoryBlock ) + sizeof( t_ttc_heap_block );

    return MemoryBlock;
}
t_ttc_heap_block*           ttc_heap_alloc_virtual_block( t_u8* Buffer, t_u8 Hint, void ( *releaseBuffer )( t_ttc_heap_block* Block ) ) {
    Assert_HEAP( Buffer != NULL, ttc_assert_origin_auto );
    t_ttc_heap_block* MemoryBlock = ttc_heap_alloc( sizeof( t_ttc_heap_block ) );

    MemoryBlock->Size          = 0;
    MemoryBlock->Hint          = Hint;
    MemoryBlock->releaseBuffer = releaseBuffer;
    MemoryBlock->Buffer        = Buffer;

    return MemoryBlock;
}
void                        ttc_heap_block_use( t_ttc_heap_block* Block ) {
    Assert_HEAP( Block->UseCount < 255, ttc_assert_origin_auto );  // maybe endless loop calling ttc_heap_block_use() ?
    Block->UseCount++;
}
void                        ttc_heap_block_release( t_ttc_heap_block* Block ) {
    Assert_HEAP_Writable( Block, ttc_assert_origin_auto );

    Block->UseCount--;
    Assert_HEAP( Block->UseCount != 255, ttc_assert_origin_auto );  // maybe you forgot to call ttc_heap_block_use() before reusing a release memory block?
    if ( Block->UseCount == 0 ) {
        Assert_HEAP( Block->releaseBuffer != NULL, ttc_assert_origin_auto );  // no release function stored!
        Block->releaseBuffer( Block );
    }
}

void                        ttc_heap_pool_block_free( void* Block ) {
    Assert_HEAP_Writable( Block, ttc_assert_origin_auto );

    t_ttc_heap_block_from_pool* MemoryBlock = ( t_ttc_heap_block_from_pool* ) Block;

    // get reference to header before memory block
    MemoryBlock--;

#if (TTC_HEAP_MAGICKEY != 0)
    Assert_HEAP( MemoryBlock->MagicKey == ( void* ) TTC_HEAP_MAGICKEY, ttc_assert_origin_auto );  // given Block does not seem to point to a memory block from a pool!
#endif

    t_ttc_heap_pool* Pool = MemoryBlock->Pool;
    Assert_HEAP_Writable( Pool, ttc_assert_origin_auto );

    e_ttc_mutex_error Error = ttc_mutex_lock( &( Pool->Lock ), -1 );
#if (TTC_ASSERT_HEAP == 1)
    Assert_HEAP( Error == tme_OK, ttc_assert_origin_auto );
#else
    ( void ) Error;
#endif

#ifdef TTC_MEMORY_DEBUG_POOL_RELEASE_PIN
    if ( ttc_task_SchedulerIsRunning ) { ttc_task_critical_begin(); }
    ttc_gpio_set( TTC_MEMORY_DEBUG_POOL_RELEASE_PIN );
#endif

    // insert memory block at begin of list of free blocks
    Assert_HEAP( Pool->FirstFree != MemoryBlock, ttc_assert_origin_auto ); // simple check failed: block to be freed has already been freed. Caller has used a memory block after freeing it. Check implementation of caller!
    Assert_HEAP_Writable( MemoryBlock, ttc_assert_origin_auto );
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
    Assert_HEAP( MemoryBlock->ListItem.Owner == NULL, ttc_assert_origin_auto ); // Item still belongs to a list. Remove it from this list before freeing it!
#endif
    MemoryBlock->ListItem.Next = ( struct s_ttc_list_item* ) Pool->FirstFree;
    Pool->FirstFree = MemoryBlock;

#ifdef TTC_MEMORY_DEBUG_POOL_RELEASE_PIN
    ttc_gpio_clr( TTC_MEMORY_DEBUG_POOL_RELEASE_PIN );
    if ( ttc_task_SchedulerIsRunning ) { ttc_task_critical_end(); }
#endif

    ttc_semaphore_give( &( Pool->BlocksAvailable ), 1 );

#if TTC_HEAP_POOL_STATISTICS == 1
    ttc_interrupt_critical_begin(); // ensure that no interrupt is corrupting statistical counters
    Pool->Statistics.AmountBlockFree++;
    Pool->Statistics.AmountFreeCurrently++;
    Pool->LastFree = __builtin_return_address( 0 );
    ttc_interrupt_critical_end(); // ensure that no interrupt is corrupting statistical counters
#endif

    ttc_mutex_unlock( &( Pool->Lock ) );
}
void                        ttc_heap_pool_block_free_isr( void* Block ) {
    Assert_HEAP_Writable( Block, ttc_assert_origin_auto );

    t_ttc_heap_block_from_pool* MemoryBlock = ( t_ttc_heap_block_from_pool* ) Block;

    // get reference to header before memory block
    MemoryBlock--;
    t_ttc_heap_pool* Pool = MemoryBlock->Pool;

    Assert_HEAP_Writable( Pool, ttc_assert_origin_auto );
#if (TTC_HEAP_MAGICKEY != 0)
    Assert_HEAP( MemoryBlock->MagicKey == ( void* ) TTC_HEAP_MAGICKEY, ttc_assert_origin_auto );  // given Block does not seem to point to a memory block from a pool!
#endif

    if ( ttc_mutex_is_locked( &( Pool->Lock ) ) ) { // pool locked by a task: store in extra isr list
        MemoryBlock->ListItem.Next = ( struct s_ttc_list_item* ) Pool->FirstFree_FromISR;
        Pool->FirstFree_FromISR = MemoryBlock;
#if TTC_HEAP_POOL_STATISTICS == 1
        Pool->Statistics.AmountSpecialFreedFromISR++;
        Pool->Statistics.AmountFreeCurrentlyFromISR++;
#endif

        ttc_semaphore_give_isr( &( Pool->BlocksAvailable ), 1 );
        return;
    }

#ifdef TTC_MEMORY_DEBUG_POOL_RELEASE_PIN
    ttc_gpio_set( TTC_MEMORY_DEBUG_POOL_RELEASE_PIN );
#endif

    // insert memory block at begin of list of free blocks
#if (TTC_ASSERT_HEAP == 1)
    if ( Pool->FirstFree )
    { Assert_HEAP_Writable( Pool->FirstFree, ttc_assert_origin_auto ); }  // if pointer is set, it must point to writable memory
#endif
    MemoryBlock->ListItem.Next = ( struct s_ttc_list_item* ) Pool->FirstFree;
    Assert_HEAP_EXTRA( MemoryBlock->ListItem.Next != TTC_LIST_TERMINATOR, ttc_assert_origin_auto ); //DEBUG

    Pool->FirstFree = MemoryBlock;

#if TTC_HEAP_POOL_STATISTICS == 1
    Pool->Statistics.AmountBlockFreeISR++;
    Pool->Statistics.AmountFreeCurrently++;
    Assert_HEAP( Pool->Statistics.AmountFreeCurrently == ttc_semaphore_available( & Pool->BlocksAvailable ) + 1, ttc_assert_origin_auto );
    Pool->LastFree = __builtin_return_address( 0 );
#endif

#ifdef TTC_MEMORY_DEBUG_POOL_RELEASE_PIN
    ttc_gpio_clr( TTC_MEMORY_DEBUG_POOL_RELEASE_PIN );
#endif

    ttc_semaphore_give_isr( &( Pool->BlocksAvailable ), 1 );
}
t_ttc_heap_block_from_pool* ttc_heap_pool_block_get( t_ttc_heap_pool* Pool ) {
    Assert_HEAP_Writable( Pool, ttc_assert_origin_auto );

    if ( ttc_semaphore_take( &( Pool->BlocksAvailable ), 1, -1 ) != tsme_OK ) {
        return NULL;
    }

    e_ttc_mutex_error Error = ttc_mutex_lock( &( Pool->Lock ), -1 );

    ( void ) Error; Assert_HEAP( Error == tme_OK, ttc_assert_origin_auto );
#if TTC_HEAP_POOL_STATISTICS == 1
    Pool->Statistics.AmountBlockGet++;
#endif

    // got semaphore + mutex: real work is done in private function
    t_ttc_heap_block_from_pool* NewBlock = Pool->FirstFree_FromISR;
    if ( NewBlock ) { // blocks available in extra space being released from isr: try to use them first
        ttc_interrupt_critical_begin();
        NewBlock = _ttc_heap_pool_block_get_from_isr( Pool );
        ttc_interrupt_critical_end();
    }
    if ( !NewBlock )  // yet no block found: try to fetch from regular list
    { NewBlock = _ttc_heap_pool_block_get( Pool ); }

    Assert_HEAP( NewBlock != NULL, ttc_assert_origin_auto ); // we obtained 1 from semaphore but pool stores no blocks. Check implementation of ttc_heap_pool!

#if TTC_HEAP_POOL_STATISTICS == 1
    // store address of function call
    NewBlock->MyOwner = __builtin_return_address( 0 );
    Assert_HEAP_Readable( NewBlock->MyOwner, ttc_assert_origin_auto ); // could not obtain a valid caller address!
    Pool->LastGet = __builtin_return_address( 0 );
#endif

    ttc_mutex_unlock( &( Pool->Lock ) );

    // got a memory block: return pointer to its content right after header
    return NewBlock + 1;
}
t_ttc_heap_block_from_pool* ttc_heap_pool_block_get_try( t_ttc_heap_pool* Pool ) {

    Assert_HEAP_Writable( Pool, ttc_assert_origin_auto );

    if ( ttc_semaphore_take_try( &( Pool->BlocksAvailable ), 1 ) != tsme_OK )
    { return NULL; }
    if ( ttc_mutex_lock( &( Pool->Lock ), -1 ) ) { // Timeout while waiting for mutex
        // give token back to semaphore for next try
        ttc_semaphore_give( &( Pool->BlocksAvailable ), 1 );
        return NULL;
    }

    // got semaphore + mutex: real work is done in private function
    t_ttc_heap_block_from_pool* NewBlock = Pool->FirstFree_FromISR;
    if ( NewBlock ) { // blocks available in extra space being released from isr: try to use them first
        ttc_interrupt_critical_begin();
        NewBlock = _ttc_heap_pool_block_get_from_isr( Pool );
        ttc_interrupt_critical_end();
    }
    if ( !NewBlock )  // yet no block found: try to fetch from regular list
    { NewBlock = _ttc_heap_pool_block_get( Pool ); }

    Assert_HEAP( NewBlock != NULL, ttc_assert_origin_auto ); // we obtained 1 from semaphore but pool stores no blocks. Check implementation of ttc_heap_pool!

#if TTC_HEAP_POOL_STATISTICS == 1
    // store address of function call in block header
    NewBlock->MyOwner = __builtin_return_address( 0 );
    Assert_HEAP_Readable( NewBlock->MyOwner, ttc_assert_origin_auto ); // could not obtain a valid caller address!

    Pool->LastGet = __builtin_return_address( 0 );
#endif

    ttc_mutex_unlock( &( Pool->Lock ) );

    // got a memory block: return pointer to its content right after header
    return NewBlock + 1;
}
t_ttc_heap_block_from_pool* ttc_heap_pool_block_get_isr( t_ttc_heap_pool* Pool ) {
    Assert_HEAP_Writable( Pool, ttc_assert_origin_auto );

    t_ttc_heap_block_from_pool* NewBlock = NULL;
    if ( ttc_mutex_is_locked( &( Pool->Lock ) ) ) { // Pool locked by task: Maybe we can sneak around it
        // Idea: The task will only manipulate not more than the first item of Pool->FirstFree and Pool->FirstFree_FromISR.
        //       Let's try if we can grab the second entry from one of the lists

        if ( ttc_semaphore_available( &( Pool->BlocksAvailable ) ) > 1 ) { // egnough blocks available to sneak around task

            if ( Pool->FirstFree_FromISR && Pool->FirstFree_FromISR->ListItem.Next ) { // this list has > 1 items: grab second one
                NewBlock = ( t_ttc_heap_block_from_pool* ) Pool->FirstFree_FromISR->ListItem.Next;
                Pool->FirstFree_FromISR->ListItem.Next = NewBlock->ListItem.Next;
#if TTC_HEAP_POOL_STATISTICS == 1
                Pool->Statistics.AmountFreeCurrentlyFromISR--;
#endif
            }
            if ( ( !NewBlock ) &&                    // got no block from ISR list
                    ( Pool->FirstFree ) &&              // list has >= 1 entries
                    ( Pool->FirstFree->ListItem.Next )  // list has >= 2 entries
               ) { // grab second block from regular list
                NewBlock = ( t_ttc_heap_block_from_pool* ) Pool->FirstFree->ListItem.Next;
                Assert_HEAP_Writable( Pool->FirstFree, ttc_assert_origin_auto );
                Pool->FirstFree->ListItem.Next = NewBlock->ListItem.Next;
            }

            if ( NewBlock ) {
#if TTC_HEAP_POOL_STATISTICS == 1
                Pool->Statistics.AmountFreeCurrently--;
                Pool->Statistics.AmountGrabbedFromISR++;
#endif

                e_ttc_semaphore_error Error = ttc_semaphore_take_isr( &( Pool->BlocksAvailable ), 1 );
                ( void ) Error; Assert_HEAP( !Error, ttc_assert_origin_auto );  // isr must be able to take this semaphore!

#if (TTC_HEAP_MAGICKEY != 0)
                Assert( NewBlock->MagicKey == ( void* ) TTC_HEAP_MAGICKEY, ttc_assert_origin_auto ); // reused memory block is corrupt!
#endif

#if TTC_HEAP_POOL_STATISTICS == 1
                // store address of function call in block header
                NewBlock->MyOwner = __builtin_return_address( 0 );
                Assert_HEAP_Readable( NewBlock->MyOwner, ttc_assert_origin_auto ); // could not obtain a valid caller address!
#endif

                return NewBlock + 1; // got a memory block: return pointer to its content right after header
            }
        }

        // could not obtain memory block
#if TTC_HEAP_POOL_STATISTICS == 1
        Pool->Statistics.AmountBlockGetISR_Failed1++;
#endif

        return NULL;
    }


#if TTC_HEAP_POOL_STATISTICS == 1
    Assert_HEAP( Pool->Statistics.AmountFreeCurrently + Pool->Statistics.AmountFreeCurrentlyFromISR == ttc_semaphore_available( & Pool->BlocksAvailable ), ttc_assert_origin_auto ); // inconsistent Pool data. Check implementation!
#endif

    if ( ttc_semaphore_take_isr( &( Pool->BlocksAvailable ), 1 ) != tsme_OK ) {
        //X(not locking mutex in isr anymore => ensure no other isr with higher priority can access same mutex!)   ttc_mutex_unlock_isr( &( Pool->Lock ) );

#if TTC_HEAP_POOL_STATISTICS == 1
        Pool->Statistics.AmountBlockGetISR_Failed2++;
#endif

        return NULL; // cannot access semaphore or pool empty
    }

#if TTC_HEAP_POOL_STATISTICS == 1
    Pool->Statistics.AmountBlockGetISR++;
#endif

    /* DEPRECATED (duplicate code)
    if ( Pool->FirstFree_FromISR ) { // blocks have been freed from interrupt service routine: reuse them first
        NewBlock = Pool->FirstFree_FromISR;
        Pool->FirstFree_FromISR  = ( t_ttc_heap_block_from_pool* ) NewBlock->ListItem.Next;
        NewBlock->ListItem.Next = NULL;
        #if TTC_HEAP_POOL_STATISTICS == 1
        Pool->Statistics.AmountFreeCurrently--;
        #endif
        Assert_HEAP_Writable( NewBlock->Pool, ttc_assert_origin_auto );
    }
    else {
        NewBlock = Pool->FirstFree;
        if ( NewBlock ) { // reusing a released memory block
            Assert_HEAP( ( NewBlock->ListItem.Next == NULL ) || ( NewBlock->ListItem.Next == TTC_LIST_TERMINATOR ) || ttc_memory_is_writable( NewBlock->ListItem.Next ), ttc_assert_origin_auto );
            Pool->FirstFree = ( t_ttc_heap_block_from_pool* ) NewBlock->ListItem.Next;
            NewBlock->ListItem.Next = NULL;
            #if TTC_HEAP_POOL_STATISTICS == 1
            Pool->Statistics.AmountFreeCurrently--;
            #endif
            Assert_HEAP_Writable( NewBlock->Pool, ttc_assert_origin_auto );
        }
    }
    */
    NewBlock = Pool->FirstFree_FromISR;
    if ( NewBlock ) {  // blocks available in extra space being released from isr: try to use them first
        NewBlock = _ttc_heap_pool_block_get_from_isr( Pool );
    }
    if ( !NewBlock ) { // no block found yet: try to fetch from regular list
        NewBlock = _ttc_heap_pool_block_get( Pool );
    }

    if ( NewBlock ) {
#if (TTC_HEAP_MAGICKEY != 0)
        Assert( NewBlock->MagicKey == ( void* ) TTC_HEAP_MAGICKEY, ttc_assert_origin_auto ); // reused memory block is corrupt!
#endif

#if TTC_HEAP_POOL_STATISTICS == 1
        // store address of function call in block header
        NewBlock->MyOwner = __builtin_return_address( 0 );
        Assert_HEAP_Readable( NewBlock->MyOwner, ttc_assert_origin_auto ); // could not obtain a valid caller address!
        Assert_HEAP( Pool->Statistics.AmountFreeCurrently + Pool->Statistics.AmountFreeCurrentlyFromISR == ttc_semaphore_available( & Pool->BlocksAvailable ), ttc_assert_origin_auto ); // inconsistent Pool data. Check implementation!
#endif

        return NewBlock + 1; // got a memory block: return pointer to its content right after header
    } // return address of allocated buffer

#if TTC_HEAP_POOL_STATISTICS == 1
    Pool->Statistics.AmountBlockGetISR_Failed3++;
#endif

    return NULL;             // no more memory blocks available
}
t_ttc_heap_pool*            ttc_heap_pool_create( TTC_MEMORY_BLOCK_BASE Size, t_base MaxAllocated ) {

    t_ttc_heap_pool* NewPool = ( t_ttc_heap_pool* ) ttc_heap_alloc( sizeof( t_ttc_heap_pool ) );
    if ( NewPool )
    { ttc_heap_pool_init( NewPool, Size, MaxAllocated ); }

#if TTC_HEAP_STATISTICS == 1
    ttc_heap_Bytes_Allocated_Usable -= sizeof( t_ttc_heap_pool ); // correct statistics
#endif

    return NewPool;
}
void                        ttc_heap_pool_init( t_ttc_heap_pool* Pool, TTC_MEMORY_BLOCK_BASE Size, t_base AmountBlocks ) {
    Assert_HEAP_Writable( Pool, ttc_assert_origin_auto );
    Assert_HEAP( Size, ttc_assert_origin_auto );
    Assert_HEAP( AmountBlocks != -1, ttc_assert_origin_auto ); // -1 is a deprecated value. Size of pool may be increased dynamically now. Update your code!

    ttc_memory_set( Pool, 0, sizeof( *Pool ) );
    Pool->BlockSize = Size;
    ttc_mutex_init( &( Pool->Lock ) );
    ttc_semaphore_init( &( Pool->BlocksAvailable ) );

    if ( 1 ) { // increase pool to initial size
        ttc_heap_pool_increase( Pool, AmountBlocks );
    }
    else {  //DEPRECATED (doubled code) allocate memory for all blocks at once and initialize all blocks
        Pool->FirstFree = ttc_heap_alloc( _ttc_heap_pool_calculate_block_size( Pool ) * AmountBlocks );
        _ttc_heap_pool_blocks_init( Pool, Pool->FirstFree, AmountBlocks );

        ttc_semaphore_give( &( Pool->BlocksAvailable ), AmountBlocks );
    }

}
t_base                      ttc_heap_pool_increase( t_ttc_heap_pool* Pool, t_base AmountAdditional ) {
    Assert_HEAP_Writable( Pool, ttc_assert_origin_auto ); // always check incoming pointer arguments

    e_ttc_mutex_error Error = ttc_mutex_lock( &( Pool->Lock ), -1 );
#if (TTC_ASSERT_HEAP == 1)
    Assert_HEAP( Error == tme_OK, ttc_assert_origin_auto );
#else
    ( void ) Error;
#endif

    t_base BlockSize = _ttc_heap_pool_calculate_block_size( Pool );
    t_base MaxAdditional = ttc_heap_get_free_size() / BlockSize;
    if ( AmountAdditional > MaxAdditional )
    { AmountAdditional = MaxAdditional; }
    t_base ChunkSize = AmountAdditional * BlockSize; // size of memory chunk to allocate for all additional blocks

    if ( AmountAdditional ) { // free heap space sufficient: allocate + initialize additional pool blocks
        t_ttc_heap_block_from_pool* AdditionalBlocks = ttc_heap_alloc_zeroed( ChunkSize ); // must erase memory to avoid issues with uninitialized list item headers (required by ttc_list_*() )
        t_ttc_heap_block_from_pool* LastBlock = _ttc_heap_pool_blocks_init( Pool, AdditionalBlocks, AmountAdditional );
        ttc_semaphore_give( &( Pool->BlocksAvailable ), AmountAdditional );

#if TTC_HEAP_POOL_STATISTICS == 1
        ttc_interrupt_critical_begin(); // ensure that no interrupt is corrupting statistical counters

#if TTC_HEAP_POOL_AMOUNT_MYBLOCKS > 0
        t_ttc_heap_block_from_pool* CurrentBlock = AdditionalBlocks;
        for ( t_u16 Index = Pool->Statistics.AmountAllocated;
                ( Index < Pool->Statistics.AmountAllocated + AmountAdditional ) && ( Index < TTC_HEAP_POOL_AMOUNT_MYBLOCKS );
                Index++ ) {
            // store pointers to additional pool blocks to allow finding them later during debug session (MyBlocks has no other use)
            Assert_HEAP_EXTRA_Writable( CurrentBlock, ttc_assert_origin_auto ); // something went wrong during initialization of new blocks. Check implementation of _ttc_heap_pool_blocks_init()!

            Pool->Statistics.MyBlocks[Index] = CurrentBlock;
            CurrentBlock = ( t_ttc_heap_block_from_pool* ) CurrentBlock->ListItem.Next;
        }
#endif
        Pool->Statistics.AmountFreeCurrently += AmountAdditional;
        Pool->Statistics.AmountAllocated     += AmountAdditional;

        // append list of additional blocks in front of existing single linked list (faster than appending at end of existing list)
        LastBlock->ListItem.Next = &( Pool->FirstFree->ListItem );
        Pool->FirstFree          = AdditionalBlocks;
        ttc_interrupt_critical_end();
#else
        ( void ) LastBlock; // avoid compiler warning "Unused variable..."
#endif
    }

    ttc_mutex_unlock( &( Pool->Lock ) );

#if (TTC_ASSERT_HEAP == 1)
    _ttc_heap_pool_check( Pool );
#endif

    return AmountAdditional; // amount of memory blocks added to the pool
}
t_ttc_heap_pool_debug*      ttc_heap_pool_debug( t_ttc_heap_pool* Pool ) {

    if ( !Pool )
    { return NULL; }

    static t_ttc_heap_pool_debug* DebugInfo = NULL;

    if ( !DebugInfo ) { // allocate memory for debug info

        DebugInfo = ttc_heap_alloc( sizeof( t_ttc_heap_pool_debug ) );
        DebugInfo->FreeBlocks_Size = THPD_BLOCKS_SIZE; // set size of DebugInfo->FreeBlocks[]
    }

    ttc_memory_copy( & ( DebugInfo->Pool ), Pool, sizeof( DebugInfo->Pool ) );

    // reset all entries of DebugInfo->FreeBlocks[]
    ttc_memory_set( DebugInfo->FreeBlocks,
                    0,
                    DebugInfo->FreeBlocks_Size * sizeof( t_ttc_heap_pool_block_debug )
                  );

    t_ttc_heap_block_from_pool* Block = Pool->FirstFree;
    for ( t_u8 Index = 0; Index < DebugInfo->FreeBlocks_Size; Index++ ) { // collect all available blocks in pool
        if ( Block ) {
            // copy memory address of block
            DebugInfo->FreeBlocks[Index].Address = Block;

            if ( ttc_memory_is_readable( Block ) ) { // copy block data
                ttc_memory_copy( &( DebugInfo->FreeBlocks[Index].Data ),
                                 Block,
                                 sizeof( t_ttc_heap_pool_block_debug )
                               );

                // proceed to next entry in single linked list
                Block = ( t_ttc_heap_block_from_pool* ) Block->ListItem.Next;
            }
        }
    }

    // count amount of blocks in single linked list Pool->FirstFree
    DebugInfo->List_Size = ttc_list_count_items_safe( &( Pool ->FirstFree->ListItem ), THPD_BLOCKS_SIZE * 100 );

    return DebugInfo;
}
t_ttc_list_item*            ttc_heap_pool_to_list_item( t_ttc_heap_block_from_pool* Block ) {
    if ( Block == NULL )
    { return NULL; }

    Block--;
#if (TTC_HEAP_MAGICKEY != 0)
    Assert_HEAP( Block->MagicKey == ( void* ) TTC_HEAP_MAGICKEY, ttc_assert_origin_auto );  // wrong magic-key: this is not a valid memory block
#endif

    return &( Block->ListItem );
}
t_ttc_heap_block_from_pool* ttc_heap_pool_from_any( void* Block ) {
    Assert_HEAP_Writable( Block, ttc_assert_origin_auto );

    return ( t_ttc_heap_block_from_pool* ) Block;
}
t_ttc_heap_block_from_pool* ttc_heap_pool_from_list_item( t_ttc_list_item* Item ) {
    if ( Item == NULL )
    { return NULL; }

    t_ttc_heap_block_from_pool* Block = ( ( t_ttc_heap_block_from_pool* ) Item );

#if (TTC_HEAP_MAGICKEY != 0)
    Assert_HEAP_Writable( Block, ttc_assert_origin_auto );  // address outside RAM: this cannot be a valid memory block
    Assert_HEAP( Block->MagicKey == ( void* ) TTC_HEAP_MAGICKEY, ttc_assert_origin_auto );  // wrong magic-key: this is not a valid memory block
#endif

    return Block + 1;
}
void                        ttc_heap_register_block( void* Address, t_base Size ) {
    ( void ) Address;
    ( void ) Size;
    if ( ttc_task_SchedulerIsRunning )
    { ttc_task_critical_begin(); }

#if TTC_HEAP_STATISTICS == 1
    ttc_heap_Amount_Blocks_Allocated++;
    ttc_heap_Bytes_Allocated_Total += Size;
#endif
#if TTC_HEAP_RECORDS > 0

    static t_ttc_heap_record* PreviousRecord;
    static t_ttc_heap_record* CurrentRecord;

    if ( ttc_memory_first_free_record > 0 ) {
        CurrentRecord  = &( A( ttc_heap_Records, ttc_memory_first_free_record ) );
        PreviousRecord = CurrentRecord - 1;
        if ( ttc_memory_first_free_record < TTC_HEAP_RECORDS - 1 )
        { ttc_memory_first_free_record++; }
        else
        { ttc_memory_first_free_record = 0; }
    }
    else {
        CurrentRecord  = &( A( ttc_heap_Records, ttc_memory_first_free_record ) );
        PreviousRecord = &( A( ttc_heap_Records, TTC_HEAP_RECORDS - 1 ) );
        ttc_memory_first_free_record++;
    }
    CurrentRecord->Enum    = PreviousRecord->Enum + 1;
    CurrentRecord->Address = Address;
    CurrentRecord->Size    = Size;

#if TTC_HEAP_STATISTICS == 1
    Assert_HEAP( ttc_heap_Amount_Blocks_Allocated == CurrentRecord->Enum, ttc_assert_origin_auto );  // One counter increased somewhere else?
#endif
#endif
    if ( ttc_task_SchedulerIsRunning )
    { ttc_task_critical_end(); }
}
void                        ttc_heap_unregister_block( void* Address, t_base Size ) {
    ( void ) Address;
    ( void ) Size;

#if TTC_HEAP_STATISTICS == 1
    ttc_heap_Amount_Blocks_Allocated--;
    ttc_heap_Bytes_Allocated_Total  -= Size;
    ttc_heap_Bytes_Allocated_Usable -= Size;
#endif
}

void*                       ttc_heap_temporary_alloc( t_base Size ) {
    Assert_HEAP( Size, ttc_assert_origin_auto );
    Assert_HEAP( ttc_memory_HeapSize, ttc_assert_origin_auto );  // ttc_heap is not yet prepared!
    Assert_HEAP( ttc_heap_TemporarySize == 0, ttc_assert_origin_auto ); // somebody else already issued a temporary allocation. Only one temporary allocation allowed!

    void* Address = _driver_heap_temporary_alloc( Size );
    Assert( Address != NULL, ttc_assert_origin_auto ); // block could not be allocated. Did you allocate more memory than available?

    return Address;
}
void                        ttc_heap_temporary_release( t_base Size ) {
    Assert_HEAP( ttc_heap_TemporarySize == Size, ttc_assert_origin_auto ); // somebody else already issued a temporary allocation. Only one temporary allocation allowed!
    ttc_heap_TemporarySize = 0;
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_heap(t_u8 LogicalIndex) {  }

void*  _ttc_heap_alloc( t_base Size ) {

    t_base* Address = NULL;

#if TTC_HEAP_ENUMERATE == 1
    Size += sizeof( t_base );
#endif

    Address = ( t_base* ) _driver_heap_alloc( Size );
    Assert_HEAP( Address != NULL, ttc_assert_origin_auto );  // could not allocate memory!

#if TTC_HEAP_STATISTICS == 1
    ttc_heap_Bytes_Allocated_Usable += Size;
#endif
#if TTC_HEAP_ENUMERATE == 1
    *Address = ++ttc_memory_Count_Alloc; // each memory block gets its own number at Address - sizeof(t_base)
    Address++;
#endif

    return Address;
}
void*  _A_ref( t_base* Size, t_base Index ) {
    Assert_HEAP( Index < *Size, ttc_assert_origin_auto );
    return Size + 1; // return pointer to first array element
}
t_base _A_index( t_base* Size, t_base Index ) {
    Assert_HEAP( Index < *Size, ttc_assert_origin_auto );
    return Index;
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
