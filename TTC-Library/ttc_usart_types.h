/** { ttc_usart_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for USART device.
 *  Structures, Enums and Defines being required by both, high- and low-level usart.
 *
 *  Created from template ttc_device_types.h revision 21 at 20140217 09:36:59 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_USART_TYPES_H
#define TTC_USART_TYPES_H

//{ Includes *************************************************************

#include "compile_options.h"
#ifdef EXTENSION_usart_stm32f1xx
    #include "usart/usart_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_usart_stm32w1xx
    #include "usart/usart_stm32w1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_usart_stm32l1xx
    #include "usart/usart_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_usart_stm32l0xx
    #include "usart/usart_stm32l0xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

#include "ttc_basic_types.h"
#include "ttc_queue_types.h"
#include "ttc_memory_types.h"
#include "ttc_heap_types.h"

//} Includes
//{ Defines ***************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration **************************************

// maximum available amount of usart devices on current architecture
#ifndef TTC_USART_MAX_AMOUNT
    #warning Missing Low-Level definition of TTC_USART_MAX_AMOUNT!
    #define TTC_USART_MAX_AMOUNT 0
#endif

// Datatype of low-level usart register being passed to low-level send/receive functions
// The low-level driver may define a datatype for each to ease debugging.
#ifndef t_ttc_usart_register_tx
    #warning Missing Low-Level definition of t_ttc_usart_register_tx
    //#define t_ttc_usart_register_tx void
#endif
#ifndef t_ttc_usart_register_rx
    #warning Missing Low-Level definition of t_ttc_usart_register_rx
    //#define t_ttc_usart_register_rx void
#endif

#ifndef _driver_usart_rx_isr
    /** _driver_usart_rx_isr must be defined as a function of prototype void (*ISR_RX)(t_physical_index, void*)
    *
    * Example (Low-Level driver):
    * void MyArchitecture_RX_ISR(t_physical_index, void*);
    * #define _driver_usart_rx_isr MyArchitecture_RX_ISR
    *
    */
    #warning Missing Low-Level Driver implementation for _driver_usart_rx_isr!
    #define _driver_usart_rx_isr NULL
#endif
#ifndef _driver_usart_tx_isr
    /** _driver_usart_rx_isr must be defined as a function of prototype void (*ISR_TX)(t_physical_index, void*)
    *
    * Example (Low-Level driver):
    * void MyArchitecture_TX_ISR(t_physical_index, void*);
    * #define _driver_usart_rx_isr MyArchitecture_TX_ISR
    *
    */
    #warning Missing Low-Level Driver implementation for _driver_usart_tx_isr!
    #define _driver_usart_tx_isr NULL
#endif

// TTC_USARTn has to be defined as constant by makefile.100_board_*
#ifdef TTC_USART5
    #ifndef TTC_USART4
        #error TTC_USART5 is defined, but not TTC_USART4 - all lower TTC_USARTn must be defined!
    #endif
    #ifndef TTC_USART3
        #error TTC_USART5 is defined, but not TTC_USART3 - all lower TTC_USARTn must be defined!
    #endif
    #ifndef TTC_USART2
        #error TTC_USART5 is defined, but not TTC_USART2 - all lower TTC_USARTn must be defined!
    #endif
    #ifndef TTC_USART1
        #error TTC_USART5 is defined, but not TTC_USART1 - all lower TTC_USARTn must be defined!
    #endif

    #define TTC_USART_AMOUNT 5
#else
    #ifdef TTC_USART4
        #define TTC_USART_AMOUNT 4

        #ifndef TTC_USART3
            #error TTC_USART5 is defined, but not TTC_USART3 - all lower TTC_USARTn must be defined!
        #endif
        #ifndef TTC_USART2
            #error TTC_USART5 is defined, but not TTC_USART2 - all lower TTC_USARTn must be defined!
        #endif
        #ifndef TTC_USART1
            #error TTC_USART5 is defined, but not TTC_USART1 - all lower TTC_USARTn must be defined!
        #endif
    #else
        #ifdef TTC_USART3

            #ifndef TTC_USART2
                #error TTC_USART5 is defined, but not TTC_USART2 - all lower TTC_USARTn must be defined!
            #endif
            #ifndef TTC_USART1
                #error TTC_USART5 is defined, but not TTC_USART1 - all lower TTC_USARTn must be defined!
            #endif

            #define TTC_USART_AMOUNT 3
        #else
            #ifdef TTC_USART2

                #ifndef TTC_USART1
                    #error TTC_USART5 is defined, but not TTC_USART1 - all lower TTC_USARTn must be defined!
                #endif

                #define TTC_USART_AMOUNT 2
            #else
                #ifdef TTC_USART1
                    #define TTC_USART_AMOUNT 1
                #else
                    #define TTC_USART_AMOUNT 0
                #endif
            #endif
        #endif
    #endif
#endif

/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_USART 0        # disable default asserts for USART driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_USART_EXTRA 1  # enable extra asserts for USART driver
 *
 */
#ifndef TTC_ASSERT_USART    // any previous definition set (Makefile)?
    #define TTC_ASSERT_USART 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_USART == 1)  // use Assert()s in USART code (somewhat slower but alot easier to debug)
    #define Assert_USART(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_USART_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_USART_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in USART code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_USART(Condition, Origin)
    #define Assert_USART_Writable(Address, Origin)
    #define Assert_USART_Readable(Address, Origin)
#endif


#ifndef TTC_ASSERT_USART_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_USART_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_USART_EXTRA == 1)  // use Assert()s in USART code (somewhat slower but alot easier to debug)
    #define Assert_USART_EXTRA(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_USART_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_USART_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in USART code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_USART_EXTRA(Condition, Origin)
    #define Assert_USART_EXTRA_Writable(Address, Origin)
    #define Assert_USART_EXTRA_Readable(Address, Origin)
#endif
//}


/** Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_usart_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
//#ifndef t_ttc_usart_architecture
//#warning Missing low-level definition for t_ttc_usart_architecture (using default)
//#define t_ttc_usart_architecture void*
//#endif

// capacity of each memory block (bytes)
#ifndef TTC_USART_BLOCK_SIZE
    #define TTC_USART_BLOCK_SIZE 20
#endif

// maximum allocated amount of memory buffers for all usarts
#ifndef TTC_USART_MAX_MEMORY_BUFFERS
    #define TTC_USART_MAX_MEMORY_BUFFERS  TTC_USART_AMOUNT * 5
#endif

//}Static Configuration
//{ Enums/ Structures *****************************************

typedef enum {   // e_ttc_usart_physical_index (starts at zero!)
    //
    // You may use this enum to assign physical index to logical TTC_USARTn constants in your makefile
    //
    // E.g.: Defining physical device #3 of USART as first logical device TTC_USART1
    //       COMPILE_OPTS += -DTTC_USART1=INDEX_USART3
    //
    INDEX_USART1,
    INDEX_USART2,
    INDEX_USART3,
    INDEX_USART4,
    INDEX_USART5,
    INDEX_USART6,
    INDEX_USART7,
    INDEX_USART8,
    INDEX_USART9,
    // add more if needed

    INDEX_USART_ERROR
} e_ttc_usart_physical_index;


typedef enum {    // e_ttc_usart_errorcode     return codes of USART devices
    ec_usart_OK = 0,

    // other warnings go here..

    ec_usart_ERROR,                 // general failure
    ec_usart_RxBufferEmpty,         // no data in receive buffer
    ec_usart_NULL,                  // NULL pointer not accepted
    ec_usart_DeviceNotFound,        // corresponding device could not be found
    ec_usart_InvalidImplementation, // internal self-test failed
    ec_usart_InvalidConfiguration,  // sanity check of device configuration failed
    ec_usart_TimeOut,               // timeout occured in called function
    ec_usart_NotImplemented,        // function has no implementation for current architecture
    ec_usart_InvalidBaudRate,       // given baud rate not suitable for adressed USART
    ec_usart_InvalidWordSize,       // given word size not suitable for adressed USART
    ec_usart_InvalidStopBits,       // given amount of stop bits not suitable for adressed USART
    ec_usart_TxBufferFull,          // given data could not be stored
    ec_usart_RxQueueLimited,        // given rx-buffer could not be stored because receive queue cannot hold more entries
    ec_usart_No_ControlParity,      // feature not available: check incoming parity bit
    ec_usart_No_Receive,            // feature not available: activate receiver
    ec_usart_No_RtsCts,             // feature not available: Hardware Flow Control
    ec_usart_No_SingleWire,         // feature not available: single-wire half-duplex
    ec_usart_No_SmartCard,          // feature not available: SmartCard mode
    ec_usart_No_Synchronous,        // feature not available: Synchronous Communication
    ec_usart_No_Transmit,           // feature not available: activate transmitter
    ec_usart_No_TransmitParity,     // feature not available: transmit parity bit
    ec_usart_InvalidPinConfig,      // one or more pins have been configured incorrectly -> check makefile for TTC_USARTxxx defines
    ec_usart_DeviceNotReady,        // choosen device has not been initialized properly

    // other failures go here..

    ec_usart_unknown                // no valid errorcodes past this entry
} e_ttc_usart_errorcode;

typedef enum {    // e_ttc_usart_architecture  types of architectures supported by USART driver
    ta_usart_None,           // no architecture selected


    ta_usart_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
    ta_usart_stm32w1xx, // automatically added by ./create_DeviceDriver.pl
    ta_usart_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
    ta_usart_stm32l0xx, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_usart_unknown        // architecture not supported
} e_ttc_usart_architecture;

typedef struct s_ttc_usart_config { //         architecture independent configuration data

    struct { // Init: fields to be set by application before first calling ttc_<device>_init() --------------
        // Do not change these values after calling ttc_<device>_init()!

        unsigned WordLength         : 4; // size of each character (7..9 Bits)
        unsigned HalfStopBits       : 4; // amount of half stop bits (1..4 * 0.5 Bits -> 0.5 .. 2 StopBits)
        t_u32    BaudRate;               // symbol rate of connection

        // Defines character at end of packets in incoming data
        // Whenever a Char_EndOfLine character is detected, the receive task
        // will pass the received buffer to receiveBlock()
        t_u16    Char_EndOfLine;        // < 0x100: character that ends one line;

        // local address of USART node in LIN mode
        t_u8     LIN_Address;

        // activity functions for byte receive
        // != NULL: registered function is called from _ttc_usart_rx_isr() after single byte has been received
        // Note: Registered function is called directly from interrupt service routine!
        void ( *activity_rx_isr )( struct s_ttc_usart_config*, t_u8 Byte );

        // activity functions for byte transmit
        // != NULL: registered function is called from _ttc_usart_tx_isr() before single byte is send out
        // Note: Registered function is called directly from interrupt service routine!
        void ( *activity_tx_isr )( struct s_ttc_usart_config*, t_u8 Byte );

        // If none of the following receive() functions is given then ttc_usart_read_byte()
        // must be called periodically to check for incoming data
        //
        // If given, receiveBlock() will be called whenever this character is received or RX-Buffer full.
        // !=NULL: function that will be called to parse a block of received data
        //         receiveBlock() can return value of Block to allow to reuse it for new received data
        //         If receiveBlock() wants to use the memory block longer then has to return NULL
        //         and call ttc_heap_block_release(Block) to release it later.
        //
        t_ttc_heap_block* ( *receiveBlock )( void* Argument, struct s_ttc_usart_config* USART_Generic, t_ttc_heap_block* Block );

        // value being passed as first argument to receiveBlock()
        void* receiveBlock_Argument;

        // != NULL: function that will be called for every received byte
        //          Note: if receiveBlock!=NULL then receiveSingleByte() is called only if no
        //                memory blocks are available for incoming data !
        void ( *receiveSingleByte )( void* USART_Generic, t_u8 Byte );

        struct { // generic initial configuration bits common for all low-level Drivers
            unsigned ControlParity    : 1; // =1: check incoming parity bit
            unsigned IrDA             : 1; // =1: activate infrared mode
            unsigned IrDALowPower     : 1; // =1: activate infrared low power mode
            unsigned IrqOnCts         : 1; // =1: interrupt is generated when CTS=1
            unsigned IrqOnError       : 1; // =1: interrupt is generated in case of any error
            unsigned IrqOnIdle        : 1; // =1: interrupt is generated when USART is idle
            unsigned IrqOnTxComplete  : 1; // =1: interrupt is generated when transmission has completed
            unsigned IrqOnParityError : 1; // =1: interrupt is generated when parity error occurs
            unsigned IrqOnRxNE        : 1; // =1: interrupt is generated when byte has been received
            unsigned LIN              : 1; // =1: activate Local Interconnection Network mode (serial bus)
            unsigned LIN_IrqOnBreak   : 1; // =1: interrupt is generated when a LIN break has been detected
            unsigned LIN_Break11Bits  : 1; // =1: LIN break detected as 11 bit break; =1: as 10 bit break
            unsigned ParityEven       : 1; // =1: activate parity checking with even parity
            unsigned ParityOdd        : 1; // =1: activate parity checking with odd parity
            unsigned Receive          : 1; // =1: activate receiver via RxD
            unsigned Cts              : 1; // =1: data is only sent when Cts goes low
            unsigned Rts              : 1; // =1: data is only requested when receive buffer is empty
            unsigned RtsCts           : 1; // =1: activate hardware flow-control via RTS/CTS wires
            unsigned SendBreaks       : 1; // =1: activate software flow-control via break characters
            unsigned SingleWire       : 1; // =1: use single-wire half-duplex
            unsigned SmartCard        : 1; // =1: activate smartcard mode
            unsigned SmartCardNACK    : 1; // =1: transmission during parity error enabled
            unsigned Synchronous      : 1; // =1: activate synchronous mode (requires additional clock wire)
            unsigned ClockIdleLow     : 1; // =1: clock signal is low when idle; =0: high when idle
            unsigned ClockOnFirst     : 1; // =1: data is captured on first clock transition; =0: on second clock transition
            unsigned Transmit         : 1; // =1: activate transmitter via TxD
            unsigned TransmitParity   : 1; // =1: transmit parity bit
            unsigned HalfDuplex       : 1; // =1: enable single-wire half-duplex mode
            unsigned RxDMA            : 1; // =1: enable DMA for Receiver
            unsigned TxDMA            : 1; // =1: enable DMA for Transmitter
            unsigned DelayedTransmits : 1; // =1: enable _delayed_() transmit functions

        } Flags;
    } Init;


    //
    // fields below are set automatically by ttc_usart ----------------------------------
    //

    // low-level configuration (structure defined by low-level driver)
#ifdef t_ttc_usart_architecture
    t_ttc_usart_architecture* LowLevelConfig;
#endif

    // handlers of registered interrupts
    void* Interrupt_RxNE;       // triggered when receive buffer has bytes
    void* Interrupt_TxComplete; // triggered after successfull transmission
    void* Interrupt_Idle;       // triggered when USART is idling

    t_ttc_task_info* Task_Rx;   // task handle of receive task that is reading from Queue_Rx

    // transports received data:           _ttc_usart_rx_isr() -> _ttc_usart_task_receive()
    t_ttc_queue_bytes*    Queue_Rx;

    // transports data to be transmitted:  ttc_usart_send_block() -> _ttc_usart_tx_isr()
    t_ttc_queue_pointers* Queue_Tx;

    // if set, no data is allowed to be added to Queue_Tx
    t_ttc_mutex_smart     Queue_TX_Lock __attribute__( ( aligned( 4 ) ) );

    // pointers to low-level registers used for fast transmit/ receive
    // The pointer is passed to low-level driver functions.
    volatile t_ttc_usart_register* BaseRegister;

    t_ttc_usart_register_rx* RegisterRx;
    t_ttc_usart_register_tx* RegisterTx;

    // GPIO-pins used for this USART (initialized by low-level driver)
    e_ttc_gpio_pin PortTxD;  // port pin for TxD
    e_ttc_gpio_pin PortRxD;  // port pin for RxD
    e_ttc_gpio_pin PortRTS;  // port pin for RTS
    e_ttc_gpio_pin PortCTS;  // port pin for CTS
    e_ttc_gpio_pin PortCLK;  // port pin for CLK

#ifdef TTC_REGRESSION
    t_base Amount_ISR_RX;    // amount of bytes received by _ttc_usart_rx_isr()
    t_base Amount_ISR_TX;    // amount of bytes send out by _ttc_usart_tx_isr()
    t_base Amount_Queue_RX;  // amount of bytes pulled from Queue_Rx
    t_base Amount_Queue_TX;  // amount of bytes pushed to Queue_Tx
#endif

    t_base   Timeout;        // > 0: every transaction must finish within this amount of time (task-switches/ msecs)
    // = 0: return immediately if transaction cannot be done right now (Note: untested!)
    // =-1: no timeouts (wait indefinitely)

    struct { // generic initial configuration bits common for all low-level Drivers
        unsigned Initialized      : 1; // =1: This USART already has been initialized
        unsigned TaskRxRunning    : 1; // =1: task for delayed transmits has been spawned
    } Flags;

    t_u8 Amount_RxBytesSkipped; // amount of bytes received by _ttc_usart_rx_isr() which could not be pushed into receive queue
    t_u8 Amount_TxISRsSkipped;  // amount of _ttc_usart_tx_isr() runs which did not find any buffer to transmit (check buffer management if >0!)

    // queue management
    t_u8 Size_Queue_Rx;      // max amount of entries to store in receive queue
    t_u8 Size_Queue_Tx;      // max amount of memory blocks to manage for transmit
    t_u8 LogicalIndex;       // automatically set: logical index of device to use (1 = TTC_USART1, ...)
    t_u8 PhysicalIndex;      // automatically set: physical index of device to use (0 = first hardware device, ...)
    t_u8 Layout;             // select pin layout to use (some uC allow pin remapping)

    e_ttc_usart_architecture Architecture; // type of architecture used for current usart device
#ifdef t_ttc_usart_architecture
    t_ttc_usart_architecture USART_Arch;   // architecture dependend USART configuration (low-level driver might want to store architecture specific data!)
#endif

} __attribute__( ( __packed__ ) ) t_ttc_usart_config;

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_USART_TYPES_H
