/*{ font_type1_16x24.h ************************************************

  SOME TEXT

}*/
#ifndef TTC_GFX_FONT_H
#define TTC_GFX_FONT_H
//{ Includes *************************************************************

//} Includes

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_font_types.h"

//{ Defines/ TypeDefs ****************************************************
//} Defines

//{ Structures/ Enums ****************************************************
//} Structures/ Enums

// returns font stored in corresponding C-file
const t_ttc_font_data* gfx_font_type1_16x24();

//{ Function prototypes **************************************************

//} Function prototypes
#endif