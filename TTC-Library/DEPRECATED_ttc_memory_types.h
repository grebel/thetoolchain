#ifndef ttc_memory_types_H
#define ttc_memory_types_H

/*{ ttc_memory.h ***********************************************
 
 * Written by Gregor Rebel 2010-2012
 *
 * Architecture independent support for dynamic memory management.
 * 
 * Currently implemented architectures: stm32 + FreeRTOS
 * 
}*/

//{ includes

#include "ttc_basic.h"
#include "ttc_list_types.h"

#ifdef malloc
#error malloc from stdlib is not supported, use ttc_memory_alloc() instead!
#endif

#ifndef TTC_MEMORY_ENUMERATE
#define TTC_MEMORY_ENUMERATE 0  // == 1: each allocated memory block gets its own number at offset -sizeof(Base_t)
#endif

// base datatype for sizes of memory blocks (must be a signed datatype)
#ifndef TTC_MEMORY_BLOCK_BASE
#define TTC_MEMORY_BLOCK_BASE  s16_t
#endif

#ifndef TTC_MEMORY_MAGICKEY
//#define TTC_MEMORY_MAGICKEY 1234554321  // used to identify valid memory blocks
#endif

#ifndef TTC_MEMORY_STATISTICS
#define TTC_MEMORY_STATISTICS 1
#endif

#ifndef TTC_MEMORY_POOL_STATISTICS
#define TTC_MEMORY_POOL_STATISTICS 0 // == 1: add extra statistic data to each memory pool
#endif

#ifndef TTC_MEMORY_RECORDS
#define TTC_MEMORY_RECORDS 100 // >0: amount of memory allocations to record; ==0: no recording
#endif

//}includes
//{ enums and structures *******************************************

typedef struct ttc_memory_block_s { // ttc_memory_block_t - memory block that stores up to 2GB (on 32-bit architectures)
  // After usage, a memory block can be returned to its memory
  // pool by calling ttc_memory_block_release().

    struct ttc_memory_block_s* Next; // memory blocks can build a linked list of memory blocks
    void (*releaseBuffer)(struct ttc_memory_block_s* Block);  // function that will take back ownership of this block
    u8_t Hint;                       // general hint that can be used by application to assign this block to a certain class
    u8_t UseCount;                   // increased on every ttc_memory_block_use(); decreased on every ttc_memory_block_release()
    TTC_MEMORY_BLOCK_BASE MaxSize;   // maximum allowed value of Size (size of Buffer[])
    TTC_MEMORY_BLOCK_BASE Size;      // >0: current amount of bytes stored in Buffer[]
                                     // <0: max size of zero terminated string stored in Buffer[]
    u8_t* Buffer;                    // points to continous memory area containing data of this block
} __attribute__((__packed__)) ttc_memory_block_t;

/** Header of memory block being allocated from a memory pool
  */
typedef struct ttc_memory_from_pool_s {

    // memory blocks can build a linked list
    // This pointer can be used by the application
    // until a block has been released
    ttc_list_item_t ListItem;

    // reference of memory pool serving this block
    struct ttc_memory_pool_s* Pool;

#if TTC_MEMORY_ENUMERATE == 1
    // every memory block gets its own unique number
    u16_t Enum;

#endif
#ifdef TTC_MEMORY_MAGICKEY
    // loaded with special number to distinguish between valid and invalid memory block pointers
    u32_t MagicKey;
#endif
    // End of Header - Start of Data
} ttc_memory_from_pool_t;

typedef struct ttc_memory_pool_statistics_s {
    // statistic: current list-size of released, unused memory blocks
    Base_t AmountFreeCurrently;

    // statistic: maximum seen list-size of released, unused memory blocks
    Base_t AmountFreeMaximum;

    // statistic: sum of AmountFreeCurrently on every mallocBlock()-call
    Base_t AmountFreeTotal;

    // statistic: amount of mallocBlock()-calls
    Base_t AmountUseTotal;

    // amount of memory blocks allocated from Heap[]
    Base_t AmountAllocated;
} ttc_memory_pool_statistics_t;

/** Management data of a pool of equal sized memory blocks
  */
typedef struct ttc_memory_pool_s {

    // used by ttc_memory_pool_block_get() to wait for memory block to be released
    ttc_semaphore_smart_t BlocksAvailable;

#ifdef RT_SCHEDULER
    // Mutex that protects this pool from concurrent accesses
    // Required in Multitasking applications only
    ttc_mutex_smart_t Lock;
#endif  

    // size of each memory block belonging to this pool
    unsigned int BlockSize;

    // first entry in a linked list of unused memory blocks
    ttc_memory_from_pool_t* FirstFree;

#if TTC_MEMORY_POOL_STATISTICS == 1
    // statistic data
    ttc_memory_pool_statistics_t Statistics;
#endif
} ttc_memory_pool_t;

#if TTC_MEMORY_RECORDS > 0

typedef struct {
    u16_t Enum;     // each block gets a unique, consecutive number
    void* Address;  // start address of memory block
    Base_t Size;    // size of memory block
} ttc_memory_record_t;

#endif

/** registers given data for debugging purposes
  *
  * Note: low-level heap allocator must call this function to register each new allocated block!
  *
  * @param Address  start address of allocated block
  * @param Size     size of allocated block (bytes)
  */
void ttc_memory_register_block(void* Address, Base_t Size);
extern void _ttc_memory_register_block(void* Address, Base_t Size);
#if (TTC_MEMORY_RECORDS > 0) || (TTC_MEMORY_STATISTICS == 1)
#  define ttc_memory_register_block(Address, Size) _ttc_memory_register_block(Address, Size)
#else
// no debugging => no function call
#  define ttc_memory_register_block(Address, Size)
#endif

/** removes registration data of a memory block
  *
  * Note: low-level heap allocator must call this function for each new freed block!
  *
  * @param Address  start address of allocated block
  * @param Size     size of allocated block (bytes)
  */
void ttc_memory_unregister_block(void* Address, Base_t Size);
extern void _ttc_memory_unregister_block(void* Address, Base_t Size);
#if (TTC_MEMORY_RECORDS > 0) || (TTC_MEMORY_STATISTICS == 1)
#  define ttc_memory_unregister_block(Address, Size) _ttc_memory_unregister_block(Address, Size)
#else
// no debugging => no function call
#  define ttc_memory_unregister_block(Address, Size)
#endif

/** checks if given memory address is known as being readonly by a pointer.
 *
 * This function is designed to be used for Assert() checks.
 *
 * E.g.: Assert(ttc_memory_is_constant(Pointer), ec_InvalidArgument);
 *
 * @return  ==1: memory is constant (resides in ROM or FLASH);
 *          ==0: memory is known not to be readonly (E.g. resides in RAM or is invalid)
 *         ==-1: memory status not known for current architecture
 */
static inline s8_t ttc_memory_is_constant(const void* Address) {
    (void) Address; // avoids warning "unused variable"

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    if (  ( ((u32_t) Address) > 0x08000000 ) && ( ((u32_t) Address) < 0x08040000 )  )
        return 1;
    return 0;
#else
    return -1;
#endif
}

/** checks if given memory address is known as being writable by a pointer.
 *
 * This function is designed to be used for Assert() checks.
 *
 * E.g.: Assert(ttc_memory_is_writable(Pointer), ec_InvalidArgument);
 *       *Pointer = foo;
 *
 * @return  ==1: memory is writable (resides in RAM or Registers);
 *          ==0: memory is known not to be writable != memory is readonly!
 *         ==-1: memory status not known for current architecture
 */
static inline s8_t ttc_memory_is_writable(const void* Address) {
#ifdef TARGET_ARCHITECTURE_STM32F1xx
    if (  ( ((u32_t) Address) >= 0x20000000) && ( ((u32_t) Address) < 0x20010000)  )
        return 1;
    return 0;
#else
    return -1;
#endif
}

/** checks if given memory address is known as being readable by a pointer.
 *
 * This function is designed to be used for Assert() checks.
 *
 * E.g.: Assert(ttc_memory_is_readable(Pointer), ec_InvalidArgument);
 *       foo = *Pointer;
 *
 * @return  ==1: memory is writable (resides in RAM or Registers);
 *          ==0: memory is known not to be writable != memory is readonly!
 *         ==-1: memory status not known for current architecture
 */
static inline s8_t ttc_memory_is_readable(const void* Address) {
#ifdef TARGET_ARCHITECTURE_STM32F1xx
    if ( ((u32_t) Address)< 0x5fffffff)
        return 1;
    return 0;
#else
    return -1;
#endif
}

/** will assert if given pointer is 0 (NULL)
 * Note: This function is normally used via macro P()
 *
 * @param Pointer  memory pointer to check
 * @return         =0: pointer was not 0; Assert_Halt_NULL() is called otherwise
 */
static inline u8_t ttc_memory_checkpointer(void* Pointer) {
    if (!Pointer)
        Assert_Halt_NULL();

    return 0;
}

/** will assert if given pointer is outside [Min, .., Max]
 * Note: This function is normally used via macro P()
 *
 * @param Pointer  memory pointer to check
 * @param Min      lowest allowed pointer address
 * @param Max      highest allowed pointer address
 * @return         =0: pointer was not 0; Assert_Halt_NULL() is called otherwise
 */
static inline u8_t ttc_memory_checkpointer_within(void* Pointer, void* Min, void* Max) {
    Assert( (Pointer < Min) || (Pointer > Max), ec_IllegalPointer);
    return 0;
}

//}enums and structures

#endif

