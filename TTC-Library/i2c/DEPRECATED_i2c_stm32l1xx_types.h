#ifndef I2C_STM32L1XX_TYPES_H
#define I2C_STM32L1XX_TYPES_H

/** { DEPRECATED_i2c_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  Low-Level datatypes for I2C devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_i2c_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20141007 15:32:58 UTC
 *
 *  Note: See ttc_i2c.h for description of architecture independent I2C implementation.
 *
 *  Authors: <AUTHOR>
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_mutex.h"
#include "../gpio/gpio_stm32l1xx.h"
#include "../register/register_stm32l1xx.h"
#include "../ttc_basic.h"
//? #include "stm32l1xx_dma.h"
//? #include "stm32l1xx_i2c.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_i2c_types.h *************************

typedef enum {
    ise_None,
    i2c_event_MASTER_MODE_SELECT                       = 0x00030001,  // BUSY, MSL and SB flag
    i2c_event_MASTER_TRANSMITTER_MODE_SELECTED         = 0x00070082,  // BUSY, MSL, ADDR, TXE and TRA flags
    i2c_event_MASTER_RECEIVER_MODE_SELECTED            = 0x00030002,  // BUSY, MSL and ADDR flags
    i2c_event_MASTER_BYTE_TRANSMITTED                  = 0x00070084,  // TRA, BUSY, MSL, TXE and BTF flags
    i2c_event_MASTER_BYTE_RECEIVED                     = 0x00030040,  // BUSY, MSL and RXNE flags
    i2c_event_MASTER_BYTE_TRANSMITTING                 = 0x00070080,  // TRA, BUSY, MSL, TXE flags
    i2c_event_SLAVE_RECEIVER_ADDRESS_MATCHED           = 0x00020002,  // BUSY and ADDR flags
    i2c_event_SLAVE_TRANSMITTER_ADDRESS_MATCHED        = 0x00060082,  // TRA, BUSY, TXE and ADDR flags
    i2c_event_SLAVE_RECEIVER_SECONDADDRESS_MATCHED     = 0x00820000,  // DUALF and BUSY flags
    i2c_event_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED  = 0x00860080,  // DUALF, TRA, BUSY and TXE flags
    i2c_event_SLAVE_GENERALCALLADDRESS_MATCHED         = 0x00120000,  // GENCALL and BUSY flags
    i2c_event_SLAVE_BYTE_RECEIVED                      = 0x00020040,  // BUSY and RXNE flags
    i2c_event_SLAVE_STOP_DETECTED                      = 0x00000010,  // STOPF flag
    i2c_event_SLAVE_BYTE_TRANSMITTED                   = 0x00060084,  // TRA, BUSY, TXE and BTF flags
    i2c_event_SLAVE_BYTE_TRANSMITTING                  = 0x00060080,  // TRA, BUSY and TXE flags
    i2c_event_SLAVE_ACK_FAILURE                        = 0x00000400,  // AF flag
    ise_Unknown
} i2c_stm32l1xx_event_e;
typedef enum {
    isf_None,
    i2c_flag_DUALF                                     = 0x00800000,
    i2c_flag_SMBHOST                                   = 0x00400000,
    i2c_flag_SMBDEFAULT                                = 0x00200000,
    i2c_flag_GENCALL                                   = 0x00100000,
    i2c_flag_TRA                                       = 0x00040000,
    i2c_flag_BUSY                                      = 0x00020000,
    i2c_flag_MSL                                       = 0x00010000,
    isf_Unknown
} i2c_stm32l1xx_flag_e;

typedef struct
{
  u32_t dma_PeripheralBaseAddr; // Specifies the peripheral base address for DMAy Channelx.
  u32_t dma_MemoryBaseAddr;     // Specifies the memory base address for DMAy Channelx.
  u32_t dma_DIR;                // Specifies if the peripheral is the source or destination.
  u32_t dma_BufferSize;         // Specifies the buffer size, in data unit, of the specified Channel.
  u32_t dma_PeripheralInc;      // Specifies whether the Peripheral address register is incremented or not.
  u32_t dma_MemoryInc;          // Specifies whether the memory address register is incremented or not.
  u32_t dma_PeripheralDataSize; // Specifies the Peripheral data width.
  u32_t dma_MemoryDataSize;     // Specifies the Memory data width.
  u32_t dma_Mode;               // Specifies the operation mode of the DMAy Channelx.
  u32_t dma_Priority;           // Specifies the software priority for the DMAy Channelx.
  u32_t dma_M2M;                // Specifies if the DMAy Channelx will be used in memory-to-memory transfer.
}dma_init_structure;

typedef struct
{
  uint32_t i2c_ClockSpeed;          // Specifies the clock frequency.
  uint16_t i2c_Mode;                // Specifies the I2C mode.
  uint16_t i2c_DutyCycle;           // Specifies the I2C fast mode duty cycle.
  uint16_t i2c_OwnAddress1;         // Specifies the first device own address.
  uint16_t i2c_Ack;                 // Enables or disables the acknowledgement.
  uint16_t i2c_AcknowledgedAddress; // Specifies if 7-bit or 10-bit address is acknowledged.
}i2c_init_structure;

typedef struct { // register description (adapt according to stm32l1xx registers)
  unsigned Reserved1 : 16;
  unsigned Reserved2 : 16;
} I2C_Register_t;

typedef struct {  // stm32l1xx specific configuration data of single I2C device
  register_stm32l1xx_i2c_t*          Base;              // base TargetAddress of I2C registers
  register_stm32l1xx_dma_channel_t*  Dma_baseTx;
  register_stm32l1xx_dma_channel_t*  Dma_baseRx;

  ttc_gpio_pin_e PortSCL;           // port pin for SCL
  ttc_gpio_pin_e PortSDA;           // port pin for SDA
  ttc_gpio_pin_e PortSMBAL;         // port pin for SMBAL

  //std configuration
  GPIO_InitTypeDef *GPIO;
  I2C_InitTypeDef *I2C;
  DMA_InitTypeDef *DMA;

  u8_t *RX;
  u8_t *TX;
  u8_t NumBytes;

  u8_t SlaveAddress;
} __attribute__((__packed__)) i2c_stm32l1xx_config_t;

// ttc_i2c_architecture_t is required by ttc_i2c_types.h
#define ttc_i2c_architecture_t i2c_stm32l1xx_config_t
#define ttc_i2c_base_register_t register_stm32l1xx_i2c_t

//} Structures/ Enums


#endif //I2C_STM32L1XX_TYPES_H
