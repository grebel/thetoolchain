#ifndef I2C_STM32F1XX_TYPES_H
#define I2C_STM32F1XX_TYPES_H

/** { i2c_stm32f1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *                     
 *  Low-Level datatypes for I2C devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by DEPRECATED_ttc_i2c_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140424 05:06:47 UTC
 *
 *  Note: See DEPRECATED_ttc_i2c.h for description of architecture independent I2C implementation.
 * 
 *  Authors: <AUTHOR>
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************
#include "../ttc_mutex.h"
#include "../gpio/gpio_stm32f1xx.h"
#include "../register/register_stm32f1xx.h"
#include "../ttc_basic.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by DEPRECATED_ttc_i2c_types.h *************************

typedef struct { // register description (adapt according to stm32f1xx registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} I2C_Register_t;

typedef struct {  // stm32f1xx specific configuration data of single I2C device
    register_stm32f1xx_i2c_t*       Base;              // base TargetAddress of I2C registers
    ttc_gpio_pin_e PortSCL;           // port pin for SCL
    ttc_gpio_pin_e PortSDA;           // port pin for SDA
    ttc_gpio_pin_e PortSMBAL;         // port pin for SMBAL
    ttc_mutex_smart_t * I2C_HW_Lock;  //protect I2C interface against simultaneous access from multiple tasks.
} __attribute__((__packed__)) i2c_stm32f1xx_config_t;

// these defines are required by DEPRECATED_ttc_i2c_types.h
#define ttc_i2c_architecture_t  i2c_stm32f1xx_config_t
#define ttc_i2c_base_register_t register_stm32f1xx_i2c_t

//} Structures/ Enums


#endif //I2C_STM32F1XX_TYPES_H
