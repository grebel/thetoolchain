/** i2c_stm32f1xx.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for i2c devices on stm32f1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 27 at 20150625 13:50:38 UTC
 *
 *  Note: See ttc_i2c.h for description of stm32f1xx independent I2C implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "i2c_stm32f1xx.h".
 */

#include "i2c_stm32f1xx.h"
#include "../ttc_memory.h"            // basic memory checks
#include "i2c_common.h"          // generic functions shared by low-level drivers of all architectures 
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

t_ttc_i2c_config*   i2c_stm32f1xx_get_features( t_ttc_i2c_config* Config ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // always check incoming pointers!

    static t_ttc_i2c_config Features;
    if ( !Features.LogicalIndex ) { // called first time: initialize data

        // Fill Features with maximum allowed values and enable all applicable Flags!

#     warning Function i2c_stm32f1xx_get_features() is empty.

    }
    Features.LogicalIndex = Config->LogicalIndex;

    return &Features;
}
e_ttc_i2c_errorcode i2c_stm32f1xx_load_defaults( t_ttc_i2c_config* Config ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // always check incoming pointers!

    // assumption: *Config has been zeroed
    Config->Architecture = ta_i2c_stm32f1xx;  // set type of architecture

    if ( 0 ) { // enable for memory mapped devices (everything that has a base register)
        switch ( Config->PhysicalIndex ) { // load address of peripheral base register
            //case 0: Config->BaseRegister = & register_stm32f1xx_I2C1; break;
            //case 1: Config->BaseRegister = & register_stm32f1xx_I2C2; break;
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid physical index: check your makefile and high-level driver implementation!
        }
    }

    // set individual feature flags
    //Config->Init.Flags.Bits.Foo                   = 1;


    return ( e_ttc_i2c_errorcode ) 0; // erverything OK
}
BOOL                i2c_stm32f1xx_check_flag( volatile t_ttc_i2c_base_register* BaseRegister, e_ttc_i2c_flag_code FlagCode ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( BaseRegister ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_check_flag() is empty.

    return ( BOOL ) 0;
}
e_ttc_i2c_errorcode i2c_stm32f1xx_deinit( t_ttc_i2c_config* Config ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_deinit() is empty.

    return ( e_ttc_i2c_errorcode ) 0;
}
t_base              i2c_stm32f1xx_get_event_value( volatile t_ttc_i2c_base_register* BaseRegister ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( BaseRegister ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    // Read the I2Cx status register
    t_base Flag1 = *( ( volatile t_base* )( &BaseRegister->SR1 ) );
    t_base Flag2 = *( ( volatile t_base* )( &BaseRegister->SR2 ) );

    // Compile 32 bit event value from both 16 bit registers
    return ( Flag1 | ( Flag2 << 16 ) ) & 0x00FFFFFF;
}
e_ttc_i2c_errorcode i2c_stm32f1xx_init( t_ttc_i2c_config* Config ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_init() is empty.

    return ( e_ttc_i2c_errorcode ) 0;
}
e_ttc_i2c_errorcode i2c_stm32f1xx_master_condition_start( t_ttc_i2c_config* Config ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_master_condition_start() is empty.

    return ( e_ttc_i2c_errorcode ) 0;
}
e_ttc_i2c_errorcode i2c_stm32f1xx_master_condition_stop( t_ttc_i2c_config* Config ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_master_condition_stop() is empty.

    return ( e_ttc_i2c_errorcode ) 0;
}
e_ttc_i2c_errorcode i2c_stm32f1xx_master_read_bytes( t_ttc_i2c_config* Config, t_u8 Amount, t_u8* Buffer ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_I2C_EXTRA( ttc_memory_is_writable( Buffer ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_master_read_bytes() is empty.

    return ( e_ttc_i2c_errorcode ) 0;
}
e_ttc_i2c_errorcode i2c_stm32f1xx_master_send_bytes( t_ttc_i2c_config* Config, t_u8 Amount, const t_u8* Buffer ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_I2C_EXTRA( ttc_memory_is_writable( Buffer ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_master_send_bytes() is empty.

    return ( e_ttc_i2c_errorcode ) 0;
}
e_ttc_i2c_errorcode i2c_stm32f1xx_master_send_slave_address( t_ttc_i2c_config* Config, t_u16 SlaveAddress, BOOL ReadMode ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_master_send_slave_address() is empty.

    return ( e_ttc_i2c_errorcode ) 0;
}
void                i2c_stm32f1xx_prepare() {


#warning Function i2c_stm32f1xx_prepare() is empty.


}
void                i2c_stm32f1xx_reset( t_ttc_i2c_config* Config ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_reset() is empty.


}
BOOL                i2c_stm32f1xx_slave_check_own_address_received( volatile t_ttc_i2c_base_register* BaseRegister ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( BaseRegister ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_slave_check_own_address_received() is empty.

    return ( BOOL ) 0;
}
BOOL                i2c_stm32f1xx_slave_check_read_mode( volatile t_ttc_i2c_base_register* BaseRegister ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( BaseRegister ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_slave_check_read_mode() is empty.

    return ( BOOL ) 0;
}
t_u8                i2c_stm32f1xx_slave_read_bytes( t_ttc_i2c_config* Config, t_u8 BufferSize, t_u8* Buffer ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_I2C_EXTRA( ttc_memory_is_writable( Buffer ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_slave_read_bytes() is empty.

    return ( t_u8 ) 0;
}
e_ttc_i2c_errorcode i2c_stm32f1xx_slave_reset_bus( t_ttc_i2c_config* Config ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_slave_reset_bus() is empty.

    return ( e_ttc_i2c_errorcode ) 0;
}
t_u8                i2c_stm32f1xx_slave_send_bytes( t_ttc_i2c_config* Config, t_u8 BufferSize, const t_u8* Buffer ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_I2C_EXTRA( ttc_memory_is_writable( Buffer ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_slave_send_bytes() is empty.

    return ( t_u8 ) 0;
}
void i2c_stm32f1xx_enable_acknowledge( volatile t_ttc_i2c_base_register* BaseRegister, BOOL Enable ) {
    Assert_I2C_EXTRA( ttc_memory_is_writable( BaseRegister ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function i2c_stm32f1xx_enable_acknowledge() is empty.


}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
