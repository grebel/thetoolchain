/** i2c_stm32l1xx.c ***************************************{
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for i2c devices on stm32l1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 26 at 20150531 21:47:36 UTC
 *
 *  Note: See ttc_i2c.h for description of stm32l1xx independent I2C implementation.
 * 
 *  Authors: Gregor Rebel, Adrian Romero
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "i2c_stm32l1xx.h".
 */

#include "i2c_stm32l1xx.h"
#include "i2c_common.h"          // generic functions shared by low-level drivers of all architectures
#include "../ttc_memory.h"       // basic memory checks
#include "../ttc_sysclock.h"     // system clocks
#include "../ttc_gpio.h"         // gpio pin configuration
#include "../ttc_register.h"     // register definitions and contents

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc(); 
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

t_ttc_i2c_config*   i2c_stm32l1xx_get_features(t_ttc_i2c_config* Config) {
    Assert_I2C(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // always check incoming pointers!

    static t_ttc_i2c_config Features;
    if (!Features.LogicalIndex) { // called first time: initialize data
        Features.LogicalIndex = Config->LogicalIndex;

        // Fill Features with maximum allowed values and enable all applicable Flags!
        Features.Flags.All = 0;
        Features.Architecture                     = ta_i2c_stm32l1xx;
        Features.ClockSpeed                       = 400000; // datasheet says 400000 is limit, but experiments showed that it can run higher frequencies
        Features.Flags.Bits.Master                = 1;
        Features.Flags.Bits.Slave                 = 1;
        Features.Flags.Bits.ModeFast              = 1;
        Features.Flags.Bits.ModeFastDuty169       = 1;
        Features.Flags.Bits.Slave_Acknowledgement = 1;
        Features.Flags.Bits.PacketErrorCheck      = 1;

        // ToDo: Enable more features!
        Features.Flags.Bits.DMA_TX                = 0;
        Features.Flags.Bits.DMA_RX                = 0;
        Features.Flags.Bits.SMBus                 = 0;
        Features.Flags.Bits.SMBus_Alert           = 0;
        Features.Flags.Bits.SMBus_Host            = 0;
        Features.Flags.Bits.SMBus_ARP             = 0;
        Features.Flags.Bits.Slave_NoStretch       = 0;
        Features.Flags.Bits.Slave_GeneralCalls    = 0;
        Features.Flags.Bits.Interrupt_Events      = 0;
        Features.Flags.Bits.Interrupt_Errors      = 0;
        Features.Flags.Bits.Address_10Bit         = 0;
    }
    
    return &Features;
}
e_ttc_i2c_errorcode i2c_stm32l1xx_deinit(t_ttc_i2c_config* Config) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

    if (Config->Flags.Bits.Initialized) {
        i2c_stm32l1xx_reset(Config);

        if (Config->PhysicalIndex == 0) {
            register_stm32l1xx_RCC.APB1ENR.Bits.I2C1_EN   = 0; // disable clock for I2C device
        }
        else {
            register_stm32l1xx_RCC.APB1ENR.Bits.I2C2_EN   = 0; // disable clock for I2C device
        }

        Config->Flags.Bits.Initialized = 0;
    }
    
    return (e_ttc_i2c_errorcode) 0;
}
e_ttc_i2c_errorcode i2c_stm32l1xx_init(t_ttc_i2c_config* Config) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    t_ttc_i2c_architecture* LowLevelConfig = (t_i2c_stm32l1xx_config*) Config->LowLevelConfig;
    Assert_I2C_EXTRA(ttc_memory_is_writable(LowLevelConfig), ttc_assert_origin_auto); // Illegal pointer in structure (corrupted memory?)

    volatile t_ttc_i2c_base_register* BaseRegister = Config->BaseRegister;
    Assert_I2C_EXTRA(ttc_memory_is_writable(BaseRegister), ttc_assert_origin_auto); // Illegal pointer in structure (corrupted memory?)

    if (1) { // reset I2C device
        // release all GPIO pins to avoid flipping bus lines
        if (Config->Init.Port_SCL != E_ttc_gpio_pin_none) {
            ttc_gpio_init(Config->Init.Port_SCL, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min);
        }
        if (Config->Init.Port_SDA != E_ttc_gpio_pin_none) {
            ttc_gpio_init(Config->Init.Port_SDA, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min);
        }
        if (Config->Init.Port_SMBALERT != E_ttc_gpio_pin_none) {
            ttc_gpio_init(Config->Init.Port_SMBALERT, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min);
        }
        t_register_stm32l1xx_i2c_cr1 CR1;
        CR1.All = BaseRegister->CR1.All;
        CR1.Bits.SWRST = 1;
        BaseRegister->CR1.All = CR1.All;
    }

    Config->Layout = 0;
    switch (Config->LogicalIndex) { // Check pin configuration
#ifdef TTC_I2C1
    case 1: { // Check pin configuration of TTC_I2C1
        if (Config->PhysicalIndex == 0) { // check configuration of I2C1 as TTC_I2C1
            Assert( (TTC_I2C1_SCL   == E_ttc_gpio_pin_b6 ) || (TTC_I2C1_SCL   == E_ttc_gpio_pin_b8  ), ttc_assert_origin_auto );       // pin is not usable for I2C: Check your makefile!
            Assert( (TTC_I2C1_SDA   == E_ttc_gpio_pin_b7 ) || (TTC_I2C1_SCL   == E_ttc_gpio_pin_b9  ), ttc_assert_origin_auto );       // pin is not usable for I2C: Check your makefile!
            Assert( (TTC_I2C1_SMBAL == E_ttc_gpio_pin_b5 ) || (TTC_I2C1_SMBAL == E_ttc_gpio_pin_none), ttc_assert_origin_auto ); // pin is not usable for I2C: Check your makefile!
        }
        else {                            // check configuration of I2C2 as TTC_I2C1
            Assert(TTC_I2C1_SCL     == E_ttc_gpio_pin_b10, ttc_assert_origin_auto );       // pin is not usable for I2C: Check your makefile!
            Assert(TTC_I2C1_SDA     == E_ttc_gpio_pin_b11, ttc_assert_origin_auto );       // pin is not usable for I2C: Check your makefile!
            Assert( (TTC_I2C1_SMBAL == E_ttc_gpio_pin_b12 ) || (TTC_I2C1_SMBAL == E_ttc_gpio_pin_none), ttc_assert_origin_auto ); // pin is not usable for I2C: Check your makefile!
        }
        break;
    }
#endif
#ifdef TTC_I2C2
    case 2: { // Check pin configuration of TTC_I2C2
        if (Config->PhysicalIndex == 0) { // check configuration of I2C1 as TTC_I2C2
            Assert( (TTC_I2C2_SCL   == E_ttc_gpio_pin_b6 ) || (TTC_I2C2_SCL   == E_ttc_gpio_pin_b8  ), ttc_assert_origin_auto );       // pin is not usable for I2C: Check your makefile!
            Assert( (TTC_I2C2_SDA   == E_ttc_gpio_pin_b7 ) || (TTC_I2C2_SCL   == E_ttc_gpio_pin_b9  ), ttc_assert_origin_auto );       // pin is not usable for I2C: Check your makefile!
            Assert( (TTC_I2C2_SMBAL == E_ttc_gpio_pin_b5 ) || (TTC_I2C2_SMBAL == E_ttc_gpio_pin_none), ttc_assert_origin_auto ); // pin is not usable for I2C: Check your makefile!
        }
        else {                            // check configuration of I2C2 as TTC_I2C2
            Assert(TTC_I2C2_SCL     == E_ttc_gpio_pin_b10, ttc_assert_origin_auto );       // pin is not usable for I2C: Check your makefile!
            Assert(TTC_I2C2_SDA     == E_ttc_gpio_pin_b11, ttc_assert_origin_auto );       // pin is not usable for I2C: Check your makefile!
            Assert( (TTC_I2C2_SMBAL == E_ttc_gpio_pin_b12 ) || (TTC_I2C2_SMBAL == E_ttc_gpio_pin_none), ttc_assert_origin_auto ); // pin is not usable for I2C: Check your makefile!
        }
        break;
    }
#endif
    default: ttc_assert_halt_origin(ec_basic_InvalidImplementation); break; // Invalid logical index: check your makefile
    }

    switch (Config->PhysicalIndex) { // enable clocks to I2C device
    case 0: { // initialize I2C1
        Assert_I2C_EXTRA(Config->BaseRegister == & register_stm32l1xx_I2C1, ttc_assert_origin_auto); // configuration corrupt: check i2c_stm32l1xx_load_defaults() or assume memory corruption!
        LowLevelConfig->DMA_BaseRx = & register_stm32l1xx_DMA_CHANNEL6;
        LowLevelConfig->DMA_BaseTx = & register_stm32l1xx_DMA_CHANNEL7;

        register_stm32l1xx_RCC.APB1ENR.Bits.I2C1_EN   = 1; // enable clock to I2C device
        register_stm32l1xx_RCC.APB1RSTR.Bits.I2C1RST = 1; // activate device reset
        register_stm32l1xx_RCC.APB1RSTR.Bits.I2C1RST = 0; // deactivate device reset

        break;
    }
    case 1: { // initialize I2C2
        Assert_I2C_EXTRA(Config->BaseRegister == & register_stm32l1xx_I2C2, ttc_assert_origin_auto); // configuration corrupt: check i2c_stm32l1xx_load_defaults() or assume memory corruption!
        LowLevelConfig->DMA_BaseRx = & register_stm32l1xx_DMA_CHANNEL4;
        LowLevelConfig->DMA_BaseTx = & register_stm32l1xx_DMA_CHANNEL5;

        register_stm32l1xx_RCC.APB1ENR.Bits.I2C2_EN   = 1; // enable clock to I2C device
        register_stm32l1xx_RCC.APB1RSTR.Bits.I2C2RST = 1; // activate device reset
        register_stm32l1xx_RCC.APB1RSTR.Bits.I2C2RST = 0; // deactivate device reset

        break;
    }
    default: ttc_assert_halt_origin(ec_basic_InvalidImplementation); break; // check your high-level implementation!
    }
    if (Config->Init.Port_SCL != E_ttc_gpio_pin_none) {
        ttc_gpio_init(Config->Init.Port_SCL, E_ttc_gpio_mode_alternate_function_open_drain, E_ttc_gpio_speed_max);
        ttc_gpio_alternate_function(Config->Init.Port_SCL, E_ttc_gpio_alternate_function_I2C1); // both I2C devices have same AF mode
    }
    if (Config->Init.Port_SDA != E_ttc_gpio_pin_none) {
        ttc_gpio_init(Config->Init.Port_SDA, E_ttc_gpio_mode_alternate_function_open_drain, E_ttc_gpio_speed_max);
        ttc_gpio_alternate_function(Config->Init.Port_SDA, E_ttc_gpio_alternate_function_I2C1); // both I2C devices have same AF mode
    }
    if (Config->Init.Port_SMBALERT != E_ttc_gpio_pin_none) {
        ttc_gpio_init(Config->Init.Port_SMBALERT, E_ttc_gpio_mode_alternate_function_open_drain, E_ttc_gpio_speed_max);
        ttc_gpio_alternate_function(Config->Init.Port_SMBALERT, E_ttc_gpio_alternate_function_I2C1); // both I2C devices have same AF mode
    }
    if (Config->Flags.Bits.DMA_RX) {  // ToDo: Implement DMA_RX
    }
    if (Config->Flags.Bits.DMA_TX) {  // ToDo: Implement DMA_RX
    }

    BaseRegister->CR1.Bits.PE = 0; // disable peripheral prior to initialization

    t_ttc_sysclock_config* SysClock = ttc_sysclock_get_configuration();

    // configure I2C registers according to RM0038 p. 598 - 610
    t_register_stm32l1xx_i2c_cr1    CR1;   CR1.All   = 0;
    t_register_stm32l1xx_i2c_cr2    CR2;   CR2.All   = 0;
    t_register_stm32l1xx_i2c_oar1   OAR1;  OAR1.All  = 0;
    t_register_stm32l1xx_i2c_oar2   OAR2;  OAR2.All  = 0;
    t_register_stm32l1xx_i2c_ccr    CCR;   CCR.All   = 0;
    t_t_register_stm32l1xx_i2crise  TRISE; TRISE.All = 0;

    CR1.Bits.PE        = 1; // will enable peripheral
    CR1.Bits.ACK       = Config->Flags.Bits.Slave_Acknowledgement;
    CR1.Bits.SMBUS     = Config->Flags.Bits.SMBus;
    CR1.Bits.SMBTYPE   = Config->Flags.Bits.SMBus_Host;
    CR1.Bits.PEC       = Config->Flags.Bits.PacketErrorCheck;
    CR1.Bits.ENARP     = Config->Flags.Bits.SMBus_ARP;
    CR1.Bits.ENGC      = Config->Flags.Bits.Slave_GeneralCalls;
    CR1.Bits.NOSTRETCH = Config->Flags.Bits.Slave_NoStretch;

    CR2.Bits.FREQ  = SysClock->LowLevelConfig.frequency_apb1 / 1000000;
    if (Config->Flags.Bits.DMA_RX || Config->Flags.Bits.DMA_RX)
        CR2.Bits.DMA_EN = 1;
    CR2.Bits.ITEVT_EN = Config->Flags.Bits.Interrupt_Events;
    CR2.Bits.ITERR_EN = Config->Flags.Bits.Interrupt_Errors;
    CR2.Bits.ITBUF_EN = 0; // ToDo: Implement interrupts for TxE/RxNE

    if (Config->Flags.Bits.Address_10Bit) { // configure 10 bit own addresses
        OAR1.All          = 0b1111111111 & Config->OwnAddress1; // limit own address to 10 bits
        OAR1.Bits.ADDMODE = 1;

        // no dual addressing in 10 bit mode
    }
    else {                                  // configure 7 bit own addresses
        OAR1.Bits.ADD  = 0b1111111 & Config->OwnAddress1;
        OAR2.Bits.ADD2 = 0b1111111 & Config->OwnAddress2;
        OAR2.Bits.ENDUAL = Config->Flags.Bits.Slave_DualAddress; // enable/ disable dual addressing mode
    }

    if (Config->Flags.Bits.ModeFast) { // Fast Mode
        CCR.Bits.Fast = 1;
        CCR.Bits.DUTY = Config->Flags.Bits.ModeFastDuty169;
        if (CCR.Bits.DUTY) { // Fast Mode Duty Cycle t_low/t_high = 16/9
            CCR.Bits.CCR = 0b111111111111 & (SysClock->LowLevelConfig.frequency_apb1 / Config->Init.ClockSpeed / 25);
        }
        else {               // Fast Mode Duty Cycle t_low/t_high = 2
            CCR.Bits.CCR = 0b111111111111 & (SysClock->LowLevelConfig.frequency_apb1 / Config->Init.ClockSpeed / 3);
            Assert_I2C(CCR.Bits.CCR >= 4, ttc_assert_origin_auto); // exceeding allowed range (-> RM0038 p. 609)
        }
    }
    else {                             // Standard Mode
        CCR.Bits.Fast = 0;
        CCR.Bits.CCR = 0b111111111111 & (SysClock->LowLevelConfig.frequency_apb1 / Config->Init.ClockSpeed / 2);
        Assert_I2C(CCR.Bits.CCR >= 4, ttc_assert_origin_auto); // exceeding allowed range (-> RM0038 p. 609)
    }

    // calculate t_Rise to fullfill maximum allowed rise time
    t_u16 t_PCLK = 1000 / CR2.Bits.FREQ; // Time of one PCLK cycle (nanoseconds)
    TRISE.Bits.TRISE = 0b111111 & ( (Config->MaxRiseTime_ns / t_PCLK) + 1 );

    BaseRegister->CR1.All   = CR1.All;
    BaseRegister->CR2.All   = CR2.All;
    BaseRegister->OAR1.All  = OAR1.All;
    BaseRegister->OAR2.All  = OAR2.All;
    BaseRegister->CCR.All   = CCR.All;
    BaseRegister->TRISE.All = TRISE.All;

    return (e_ttc_i2c_errorcode) 0;
}
e_ttc_i2c_errorcode i2c_stm32l1xx_load_defaults(t_ttc_i2c_config* Config) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

    // assumption: *Config has been zeroed
    Config->Architecture = ta_i2c_stm32l1xx;  // set type of architecture

    switch (Config->PhysicalIndex) { // load address of peripheral base register
    case 0: Config->BaseRegister = & register_stm32l1xx_I2C1; break;
    case 1: Config->BaseRegister = & register_stm32l1xx_I2C2; break;
    default: ttc_assert_halt_origin(ttc_assert_origin_auto); break; // invalid physical index: check your makefile and high-level driver implementation!
    }

    Config->Flags.All                        = 0;
    Config->Flags.Bits.Master                = 1;
    Config->Flags.Bits.Slave                 = 1;
    Config->Flags.Bits.ModeFast              = 0; // some slaves do not like fast mode
    Config->Flags.Bits.ModeFastDuty169       = 0;
    Config->Flags.Bits.DMA_TX                = 0;
    Config->Flags.Bits.DMA_RX                = 0;
    Config->Flags.Bits.SMBus                 = 0;
    Config->Flags.Bits.SMBus_Alert           = 0;
    Config->Flags.Bits.SMBus_Host            = 0;
    Config->Flags.Bits.SMBus_ARP             = 0;
    Config->Flags.Bits.Address_10Bit         = 0;
    Config->Flags.Bits.PacketErrorCheck      = 0;
    Config->Flags.Bits.Slave_Acknowledgement = 1; // required for most slaves
    Config->Flags.Bits.Slave_DualAddress     = 0;
    Config->Flags.Bits.Slave_NoStretch       = 0;
    Config->Flags.Bits.Slave_GeneralCalls    = 0;
    Config->Flags.Bits.Interrupt_Events      = 0;
    Config->Flags.Bits.Interrupt_Errors      = 0;
    Config->Init.ClockSpeed                       = 400000;

    Config->OwnAddress1 = 0;
    Config->OwnAddress2 = 0;

    return (e_ttc_i2c_errorcode) 0;
}
void                i2c_stm32l1xx_prepare() {

    // reset registers of all I2C devices
    if (0) { // asserts in ttc_memory_set()
        ttc_memory_set( (void*) &register_stm32l1xx_I2C1, 0, sizeof(register_stm32l1xx_I2C1) );
        ttc_memory_set( (void*) &register_stm32l1xx_I2C2, 0, sizeof(register_stm32l1xx_I2C2) );
    }

    // reset + disable all I2C devices
    register_stm32l1xx_RCC.APB1RSTR.Bits.I2C1RST = 1; // reset I2C device
    register_stm32l1xx_RCC.APB1RSTR.Bits.I2C1RST = 0; // clear device reset
    register_stm32l1xx_RCC.APB1ENR.Bits.I2C1_EN   = 0; // disable clock for I2C device
    register_stm32l1xx_RCC.APB1RSTR.Bits.I2C2RST = 1; // reset I2C device
    register_stm32l1xx_RCC.APB1RSTR.Bits.I2C2RST = 0; // clear device reset
    register_stm32l1xx_RCC.APB1ENR.Bits.I2C2_EN   = 0; // disable clock for I2C device
}
void                i2c_stm32l1xx_reset(t_ttc_i2c_config* Config) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

    if (Config->PhysicalIndex == 0) {                         // reset I2C1
        register_stm32l1xx_I2C1.CR1.Bits.PE = 0;              // disable peripheral
        register_stm32l1xx_RCC.APB1RSTR.Bits.I2C1RST = 1; // reset I2C device
        register_stm32l1xx_RCC.APB1RSTR.Bits.I2C1RST = 0; // clear device reset
    }
    else {                                                    // reset I2C2
        register_stm32l1xx_I2C2.CR1.Bits.PE = 0;              // disable peripheral
        register_stm32l1xx_RCC.APB1RSTR.Bits.I2C2RST = 1; // reset I2C device
        register_stm32l1xx_RCC.APB1RSTR.Bits.I2C2RST = 0; // clear device reset
    }

    Config->Flags.Bits.Initialized = 0;
}
BOOL                i2c_stm32l1xx_check_event(t_u32 EventValue, t_u32 Event) {
// ToDo: Replace this function by a macro to speed it up
    // Check whether the last event contains given Event
    if ((EventValue & Event) == Event)
        return 1;
    else
        return 0;
}
e_ttc_i2c_errorcode i2c_stm32l1xx_master_condition_start(t_ttc_i2c_config* Config) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

    volatile t_register_stm32l1xx_i2c* BaseRegister = Config->BaseRegister;
    Assert_I2C_EXTRA(BaseRegister, ttc_assert_origin_auto);

    // have to set bit like this to avoid strange compiler optimizations on register access (GCC bug?)
    t_register_stm32l1xx_i2c_cr1 CR1; CR1.All = BaseRegister->CR1.All;
    CR1.Bits.START = 1;
    CR1.Bits.ACK   = 0; // slave has to acknowledge (avoid Acknowledge Failure in case we're called from ttc_i2c_master_read_register()
    BaseRegister->CR1.All = CR1.All;

    if (i2c_common_wait_until_event( TTC_I2C_EVENT_CODE_MASTER_MODE_SELECT,
                                    BaseRegister,
                                    Config->Init.TimeOut,
                                    Config->Init.AmountRetries
                                    )
        )
        return E_ttc_i2c_errorcode_OK;


    return E_ttc_i2c_errorcode_TimeOut_StartCondition;
}
e_ttc_i2c_errorcode i2c_stm32l1xx_master_condition_stop(t_ttc_i2c_config* Config) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

    volatile t_register_stm32l1xx_i2c* BaseRegister = Config->BaseRegister;
    Assert_I2C_EXTRA(BaseRegister, ttc_assert_origin_auto);

    // have to set bit like this to avoid strange compiler optimizations on register access (GCC bug?)
    t_register_stm32l1xx_i2c_cr1 CR1; CR1.All = BaseRegister->CR1.All;
    CR1.Bits.STOP = 1;
    BaseRegister->CR1.All = CR1.All;

    if (i2c_common_wait_until_event( E_ttc_i2c_event_code_bus_free,
                                    BaseRegister,
                                    Config->Init.TimeOut,
                                    Config->Init.AmountRetries
                                  )
            )
        return E_ttc_i2c_errorcode_OK;

    return E_ttc_i2c_errorcode_TimeOut_StopCondition;
}
e_ttc_i2c_errorcode i2c_stm32l1xx_master_send_slave_address(t_ttc_i2c_config* Config, t_u16 SlaveAddress, BOOL ReadMode) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

    volatile t_register_stm32l1xx_i2c* BaseRegister = Config->BaseRegister;
    Assert_I2C_EXTRA(BaseRegister, ttc_assert_origin_auto);
    volatile t_u16 Dummy; (void) Dummy;

    if (Config->Flags.Bits.Address_10Bit) { // sending 10 bit address
        ttc_assert_halt_origin(ttc_assert_origin_auto); // ToDo
    }
    else {                                  // sending 7 bit address
        if (ReadMode) { // sending 7 bit address for read mode
            SlaveAddress = (SlaveAddress << 1) | 1; // set read bit in LSB


            // transmit slave address (read SR1 + write DR according to RM0038 p.586)
            Dummy = *( (t_u8*) &(BaseRegister->SR1) );
            *( (t_u8*) &(BaseRegister->DR) ) = SlaveAddress;

            if (! i2c_common_wait_until_event( TTC_I2C_EVENT_CODE_MASTER_TRANSMITTER_MODE_SELECTED,
                                              BaseRegister,
                                              Config->Init.TimeOut,
                                              Config->Init.AmountRetries
                                            )
               )
            return E_ttc_i2c_errorcode_TimeOut_Master_ReceiverModeSelected;
        }
        else {          // sending 7 bit address for write mode
            SlaveAddress = (SlaveAddress << 1); // clear read bit in LSB

            // transmit slave address (read SR1 + write DR according to RM0038 p.586)
            Dummy = *( (t_u8*) &(BaseRegister->SR1) );
            *( (t_u8*) &(BaseRegister->DR) ) = SlaveAddress;

            if (! i2c_common_wait_until_event( TTC_I2C_EVENT_CODE_MASTER_TRANSMITTER_MODE_SELECTED,
                                              BaseRegister,
                                              Config->Init.TimeOut,
                                              Config->Init.AmountRetries
                                            )
               )
                return E_ttc_i2c_errorcode_TimeOut_Master_TransmitterModeSelected;
        }
    }

    return E_ttc_i2c_errorcode_OK;
}
e_ttc_i2c_errorcode i2c_stm32l1xx_master_read_bytes(t_ttc_i2c_config* Config, t_u8 Amount, t_u8* Buffer) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_I2C_EXTRA(ttc_memory_is_writable(Buffer), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

    volatile t_ttc_i2c_base_register* BaseRegister = Config->BaseRegister;
    e_ttc_i2c_errorcode Error = E_ttc_i2c_errorcode_OK;
    t_register_stm32l1xx_i2c_cr1 CR1_ACK1, CR1_ACK0;
    CR1_ACK0.All = BaseRegister->CR1.All; CR1_ACK0.Bits.ACK = 0;
    CR1_ACK1.All = BaseRegister->CR1.All; CR1_ACK1.Bits.ACK = 1;

    t_u8* DR = (t_u8*) &(BaseRegister->DR);

    while (Amount) {
        Amount--;

        if (Amount)
            BaseRegister->CR1.All = CR1_ACK1.All; // still more than one byte to receive: enable acknowledgement
        else
            BaseRegister->CR1.All = CR1_ACK0.All; // last byte to receive: disable acknowledgement

        if (! i2c_common_wait_until_event( TTC_I2C_EVENT_CODE_MASTER_BYTE_RECEIVED,
                                          BaseRegister,
                                          Config->Init.TimeOut,
                                          Config->Init.AmountRetries
                                        )
           )
            return E_ttc_i2c_errorcode_TimeOut_WaitingForByteReceivedFromSlave;

        *Buffer++ = *DR;
    }

    return Error;
}
e_ttc_i2c_errorcode i2c_stm32l1xx_master_send_bytes(t_ttc_i2c_config* Config, t_u8 Amount, const t_u8* Buffer) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_I2C_EXTRA(ttc_memory_is_readable(Buffer), ttc_assert_origin_auto); // check if it points to RAM, ROM or FLASH

    volatile t_ttc_i2c_base_register* BaseRegister = Config->BaseRegister;

    t_u32* DR = (t_u32*) & (BaseRegister->DR);
    while (Amount) {
        // ToDo: Wait for slave to become ready!
        *DR = *Buffer++;
        ttc_task_usleep(10); //slow down master as a workaround to get master-slave communication running - ToDo: FixMe!

        if (! i2c_common_wait_until_event( TTC_I2C_EVENT_CODE_MASTER_BYTE_TRANSMITTED,
                                          BaseRegister,
                                          Config->Init.TimeOut,
                                          Config->Init.AmountRetries
                                        )
           )
            return E_ttc_i2c_errorcode_TimeOut_WaitingForTransmitBuffer;

        Amount--;
    }

    return E_ttc_i2c_errorcode_OK;
}
t_u8                i2c_stm32l1xx_slave_read_bytes(t_ttc_i2c_config* Config, t_u8 BufferSize, t_u8* Buffer) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_I2C_EXTRA(ttc_memory_is_writable(Buffer), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

    volatile t_ttc_i2c_base_register* BaseRegister = Config->BaseRegister;
    t_u8* DR = (t_u8*) &(BaseRegister->DR);

    t_u8 AmountBytesReceived = 0;

    const e_ttc_i2c_event_code EventCodes[5] = {
        TTC_I2C_EVENT_CODE_SLAVE_BYTE_RECEIVED,
        TTC_I2C_EVENT_CODE_SLAVE_STOP_DETECTED,
        TTC_I2C_EVENT_CODE_SLAVE_TRANSMITTER_ADDRESS_MATCHED,
        TTC_I2C_EVENT_CODE_SLAVE_BUFFER_OVERRUN,
        0
    };

    while (1) {
        e_ttc_i2c_event_code Event = i2c_common_wait_until_events( EventCodes,
                                                                   BaseRegister,
                                                                   Config->Init.TimeOut,
                                                                   Config->Init.AmountRetries
                                                                   );

        switch (Event) {
        case TTC_I2C_EVENT_CODE_SLAVE_BYTE_RECEIVED:  {              // successfully received single byte from master
            if (BufferSize) { // still space in buffer: store byte in buffer
                *Buffer++ = *DR;
                BufferSize--;
                AmountBytesReceived++;
            }
            else {
                volatile t_u8 Dummy = *DR; (void) Dummy; // just read byte without storing it
            }
            break;
        }
        case TTC_I2C_EVENT_CODE_SLAVE_STOP_DETECTED: {               // master sent stop condition
            // EV4: reset STOPF bit by reading SR1 + writing CR1
            volatile t_u16 CR1 = *( (t_u16*) &(BaseRegister->CR1) );
            volatile t_u16 SR1 = *( (t_u16*) &(BaseRegister->SR1) ); (void) SR1;
            *( (t_u16*) &(BaseRegister->CR1) ) = CR1;

            goto ReadRemainingBytes;
        }
        case TTC_I2C_EVENT_CODE_SLAVE_TRANSMITTER_ADDRESS_MATCHED: { // master sent address again
            if (i2c_stm32l1xx_check_flag(BaseRegister, E_ttc_i2c_flag_code_read_mode_detected)) {
                i2c_stm32l1xx_enable_acknowledge(BaseRegister, 0); // disable acknowledgments

                goto ReadRemainingBytes;
            }
            break;
        }
        case TTC_I2C_EVENT_CODE_SLAVE_BUFFER_OVERRUN: {              // got too much data to process: skip everything
            i2c_stm32l1xx_slave_reset_bus(Config);
            return 0;
            break;
        }
        default: {                                                   // TimeOut occured while waiting for byte receival

            i2c_stm32l1xx_slave_reset_bus(Config);
            return 0;
            break;
        }
        };
    }

ReadRemainingBytes:
    while (i2c_stm32l1xx_check_flag(BaseRegister, E_ttc_i2c_flag_code_receive_buffer_not_empty)) { // read byte from input buffer
        if (BufferSize) {
            *Buffer++ = *DR;
            BufferSize--;
        }
        else {
            volatile t_u8 Dummy = *DR; (void) Dummy; // just read byte without storing it
        }
        AmountBytesReceived++;
    }

    return AmountBytesReceived;
}
t_u8                i2c_stm32l1xx_slave_send_bytes(t_ttc_i2c_config* Config, t_u8 BufferSize, const t_u8* Buffer) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_I2C_EXTRA(ttc_memory_is_readable(Buffer), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

    volatile t_ttc_i2c_base_register* BaseRegister = Config->BaseRegister;
    t_u8* DR = (t_u8*) &(BaseRegister->DR);

    t_u8 AmountBytesSent = 0;

    const e_ttc_i2c_event_code EventCodes[5] = {
        TTC_I2C_EVENT_CODE_SLAVE_BYTE_TRANSMITTED,
        TTC_I2C_EVENT_CODE_SLAVE_STOP_DETECTED,
        TTC_I2C_EVENT_CODE_SLAVE_ACK_FAILURE,
        TTC_I2C_EVENT_CODE_BUS_FREE,
        0
    };

    while (1) {
        ttc_task_usleep(10); //slow down master as a workaround to get master-slave communication running - ToDo: FixMe!
        if (BufferSize) { // send byte from buffer
            *DR = *Buffer++;
            BufferSize--;
        }
        else
            *DR = 0;     // buffer exceeded: send zeroes instead
        AmountBytesSent++;

        e_ttc_i2c_event_code Event = i2c_common_wait_until_events( EventCodes,
                                                                   BaseRegister,
                                                                   Config->Init.TimeOut,
                                                                   Config->Init.AmountRetries
                                                                   );

        switch (Event) {

        case TTC_I2C_EVENT_CODE_SLAVE_STOP_DETECTED: { // master sent stop condition

            // EV4: reset STOPF bit by reading SR1 + writing CR1
            volatile t_u16 CR1 = *( (t_u16*) &(BaseRegister->CR1) );
            volatile t_u16 SR1 = *( (t_u16*) &(BaseRegister->SR1) ); (void) SR1;
            *( (t_u16*) &(BaseRegister->CR1) ) = CR1;

            return AmountBytesSent;
        }
        case TTC_I2C_EVENT_CODE_SLAVE_ACK_FAILURE: // Acknowledge Failure detected
            BaseRegister->SR1.Bits.AF = 0;         // reset bit
        case TTC_I2C_EVENT_CODE_BUS_FREE:          // bus has been released (STOPF is not set after NACK ->RM0038 p.605)

            return AmountBytesSent;

        case TTC_I2C_EVENT_CODE_SLAVE_BYTE_TRANSMITTED:
            break;                                 // contine reveiving bytes

        default:
            // timeout while waiting for byte to be transmitted
            i2c_stm32l1xx_slave_reset_bus(Config);
            return AmountBytesSent;
        };
    }

    return AmountBytesSent;
}
e_ttc_i2c_errorcode i2c_stm32l1xx_slave_reset_bus(t_ttc_i2c_config* Config) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

    volatile t_register_stm32l1xx_i2c* BaseRegister = Config->BaseRegister;
    BaseRegister->CR1.Bits.SWRST = 1;
    BaseRegister->CR1.Bits.SWRST = 0;

    return (e_ttc_i2c_errorcode) 0;
}
BOOL                i2c_stm32l1xx_slave_check_own_address_received(volatile t_ttc_i2c_base_register* BaseRegister) {
    return i2c_stm32l1xx_check_flag(BaseRegister, TTC_I2C_FLAG_CODE_ADDRESS_SENT);
}
BOOL                i2c_stm32l1xx_slave_check_read_mode(volatile t_ttc_i2c_base_register* BaseRegister) {
    return i2c_stm32l1xx_check_flag(BaseRegister, TTC_I2C_FLAG_CODE_READ_MODE_DETECTED);
}
t_base              i2c_stm32l1xx_get_event_value(volatile t_ttc_i2c_base_register* BaseRegister) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(BaseRegister), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

    // Read the I2Cx status register
    t_u32 Flag1 = BaseRegister->SR1.All;
    t_u32 Flag2 = BaseRegister->SR2.All;

    // Compile 32 bit event value from both 16 bit registers
    return ( Flag1 | (Flag2 << 16) ) & 0x00FFFFFF;
}
BOOL                i2c_stm32l1xx_check_flag(volatile t_ttc_i2c_base_register* BaseRegister, e_ttc_i2c_flag_code FlagCode) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(BaseRegister), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

    if (FlagCode & 0x80000000) { // flag is in SR2
        return BaseRegister->SR2.All & FlagCode; // ignoring highest bit because its always zero in this register
    }
    return BaseRegister->SR1.All & FlagCode;
}
void                i2c_stm32l1xx_enable_acknowledge(volatile t_ttc_i2c_base_register* BaseRegister, BOOL Enable) {
    Assert_I2C_EXTRA(ttc_memory_is_writable(BaseRegister), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)


    // must use this way to set bit to avoid compiler bug (cleared PE bit instead of setting ACK)!

    t_register_stm32l1xx_i2c_cr1 CR1; CR1.All = BaseRegister->CR1.All;

    if (Enable)
        CR1.Bits.ACK = 1; // enable acknowledgments for incoming data
    else
        CR1.Bits.ACK = 0; // enable acknowledgments for incoming data

    BaseRegister->CR1.All = CR1.All;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by 
 * the maintainer of this file.
 */
 
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
