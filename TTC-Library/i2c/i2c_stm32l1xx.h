#ifndef I2C_STM32L1XX_H
#define I2C_STM32L1XX_H

/** { i2c_stm32l1xx.h **********************************************
 *
 *                               The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for i2c devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by low-level driver only.
 *
 *  Created from template device_architecture.h revision 25 at 20150531 21:47:36 UTC
 *
 *  Note: See ttc_i2c.h for description of stm32l1xx independent I2C implementation.
 *  
 *  Authors: Adrian Romero, Gregor Rebel
 *
 *
 *  How to configure stm32l1xx i2c devices
 * 
 *  This low-level driver requires configuration defines in addtion to the howto section 
 *  of ttc_i2c.h.
 *  
 *  PLACE YOUR DESCRIPTION HERE!
}*/
//{ Defines/ TypeDefs ****************************************************

#define EXTENSION_I2C_DRIVER_AVAILABLE
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_I2C_STM32L1XX
//
// Implementation status of low-level driver support for i2c devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_I2C_STM32L1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_I2C_STM32L1XX == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_I2C_STM32L1XX to one from '-', 'o', '+' 
#endif

//}EXTENSION_STATUS_TTC_I2C_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "i2c_stm32l1xx.c"
//
#include "../ttc_i2c_types.h" // will include i2c_stm32l1xx_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes

//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_i2c_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_i2c_foo
//
#define ttc_driver_i2c_deinit(Config) i2c_stm32l1xx_deinit(Config)
#define ttc_driver_i2c_get_features(Config) i2c_stm32l1xx_get_features(Config)
#define ttc_driver_i2c_init(Config) i2c_stm32l1xx_init(Config)
#define ttc_driver_i2c_load_defaults(Config) i2c_stm32l1xx_load_defaults(Config)
#define ttc_driver_i2c_prepare() i2c_stm32l1xx_prepare()
#define ttc_driver_i2c_reset(Config) i2c_stm32l1xx_reset(Config)

#define ttc_driver_i2c_check_event_bus_not_busy(EventValue)                               i2c_stm32l1xx_check_event_bus_not_busy(EventValue)
#define ttc_driver_i2c_check_event_master_byte_received(EventValue)                       i2c_stm32l1xx_check_event_master_byte_received(EventValue)
#define ttc_driver_i2c_check_event_master_byte_transmitted(EventValue)                    i2c_stm32l1xx_check_event_master_byte_transmitted(EventValue)
#define ttc_driver_i2c_check_event_master_byte_transmitting(EventValue)                   i2c_stm32l1xx_check_event_master_byte_transmitting(EventValue)
#define ttc_driver_i2c_check_event_master_receiver_mode_selected(EventValue)              i2c_stm32l1xx_check_event_master_receiver_mode_selected(EventValue)
#define ttc_driver_i2c_check_event_master_slave_has_acknowledged_start(EventValue)        i2c_stm32l1xx_check_event_master_slave_has_acknowledged_start(EventValue)
#define ttc_driver_i2c_check_event_master_transmitter_mode_selected(EventValue)           i2c_stm32l1xx_check_event_master_transmitter_mode_selected(EventValue)
#define ttc_driver_i2c_check_event_slave_ack_failure(EventValue)                          i2c_stm32l1xx_check_event_slave_ack_failure(EventValue)
#define ttc_driver_i2c_check_event_slave_byte_received(EventValue)                        i2c_stm32l1xx_check_event_slave_byte_received(EventValue)
#define ttc_driver_i2c_check_event_slave_byte_transmitted(EventValue)                     i2c_stm32l1xx_check_event_slave_byte_transmitted(EventValue)
#define ttc_driver_i2c_check_event_slave_byte_transmitting(EventValue)                    i2c_stm32l1xx_check_event_slave_byte_transmitting(EventValue)
#define ttc_driver_i2c_check_event_slave_general_call_address_matched(EventValue)         i2c_stm32l1xx_check_event_slave_general_call_address_matched(EventValue)
#define ttc_driver_i2c_check_event_slave_receiver_address_matched(EventValue)             i2c_stm32l1xx_check_event_slave_receiver_address_matched(EventValue)
#define ttc_driver_i2c_check_event_slave_receiver_second_address_matched(EventValue)      i2c_stm32l1xx_check_event_slave_receiver_second_address_matched(EventValue)
#define ttc_driver_i2c_check_event_slave_stop_detected(EventValue)                        i2c_stm32l1xx_check_event_slave_stop_detected(EventValue)
#define ttc_driver_i2c_check_event_slave_transmitter_address_matched(EventValue)          i2c_stm32l1xx_check_event_slave_transmitter_address_matched(EventValue)
#define ttc_driver_i2c_check_event_slave_transmitter_second_address_matched(EventValue)   i2c_stm32l1xx_check_event_slave_transmitter_second_address_matched(EventValue)

#define ttc_driver_i2c_check_flag_bus_is_busy(BaseRegister)                                 i2c_stm32l1xx_check_flag_bus_is_busy(BaseRegister)
#define ttc_driver_i2c_check_flag_bytes_received(BaseRegister)                              i2c_stm32l1xx_check_flag_bytes_received(BaseRegister)
#define ttc_driver_i2c_check_flag_bytes_transmitted(BaseRegister)                           i2c_stm32l1xx_check_flag_bytes_transmitted(BaseRegister)
#define ttc_driver_i2c_check_flag_error_arbitration_lost(BaseRegister)                      i2c_stm32l1xx_check_flag_error_arbitration_lost(BaseRegister)
#define ttc_driver_i2c_check_flag_error_misplaced_condition(BaseRegister)                   i2c_stm32l1xx_check_flag_error_misplaced_condition(BaseRegister)
#define ttc_driver_i2c_check_flag_error_no_acknowledge(BaseRegister)                        i2c_stm32l1xx_check_flag_error_no_acknowledge(BaseRegister)
#define ttc_driver_i2c_check_flag_error_parity(BaseRegister)                                i2c_stm32l1xx_check_flag_error_parity(BaseRegister)
#define ttc_driver_i2c_check_flag_error_slave_buffer_overrun(BaseRegister)                  i2c_stm32l1xx_check_flag_error_slave_buffer_overrun(BaseRegister)
#define ttc_driver_i2c_check_flag_general_call_received(BaseRegister)                       i2c_stm32l1xx_check_flag_general_call_received(BaseRegister)
#define ttc_driver_i2c_check_flag_master_mode(BaseRegister)                                 i2c_stm32l1xx_check_flag_master_mode(BaseRegister)
#define ttc_driver_i2c_check_flag_smb_default_address_received(BaseRegister)                i2c_stm32l1xx_check_flag_smb_default_address_received(BaseRegister)
#define ttc_driver_i2c_check_flag_smb_host_header_received(BaseRegister)                    i2c_stm32l1xx_check_flag_smb_host_header_received(BaseRegister)
#define ttc_driver_i2c_check_flag_smb_error_timeout(BaseRegister)                           i2c_stm32l1xx_check_flag_smb_error_timeout(BaseRegister)

/** checks if given 32-bit event has occured
 *
 * An event is a combination of different flags. Events cannot be checked by consecutive flag checks,
 * because many flags are cleared by hardware after reading their register.
 *
 * @param EventValue   (t_u32)  address of peripheral register base of I2C device
 * @param Event        (t_u32)  32 bit mask defining a certain I2C event
 * @return             (BOOL)   != 0: event has occured: == 0: not occured
 */
BOOL i2c_stm32l1xx_check_event(t_u32 EventValue, t_u32 Event);

/* DEPRECATED
// checks if given 1-bit status flag is set in StatusRegister 1 (SR1)
//
// @param BaseRegister (t_ttc_i2c_base_register*)  address of peripheral register base of I2C device
// @param Flag         (e_i2c_stm32l1xx_flag_sr1)  32 bit mask defining a single I2C flag in SR1 register
// @return             (BOOL)                      != 0: flag is active: == 0: not active
//
BOOL i2c_stm32l1xx_check_flag_sr1(volatile t_ttc_i2c_base_register* BaseRegister, e_i2c_stm32l1xx_flag_sr1 Flag);
#if (TTC_ASSERT_I2C_EXTRA == 0) // no extra asserts requested: use fast macro to check flag
#  define i2c_stm32l1xx_check_flag_sr1(BaseRegister, Flag) (BaseRegister->SR1.All & (t_u32) Flag)
#endif

// checks if given 1-bit status flag is set in StatusRegister 2 (SR2)
//
// @param BaseRegister (t_ttc_i2c_base_register*)  address of peripheral register base of I2C device
// @param Flag         (e_i2c_stm32l1xx_flag_sr2)  32 bit mask defining a single I2C flag in SR2 register
// @return             (BOOL)                      != 0: flag is active: == 0: not active
//
BOOL i2c_stm32l1xx_check_flag_sr2(volatile t_ttc_i2c_base_register* BaseRegister, e_i2c_stm32l1xx_flag_sr2 Flag);
#if (TTC_ASSERT_I2C_EXTRA == 0) // no extra asserts requested: use fast macro to check flag
#  define i2c_stm32l1xx_check_flag_sr2(BaseRegister, Flag) (BaseRegister->SR2.All & (t_u32) Flag)
#endif
*/

#define ttc_driver_i2c_master_condition_start(Config) i2c_stm32l1xx_master_condition_start(Config)
#define ttc_driver_i2c_master_send_slave_address(Config, SlaveAddress, ReadMode) i2c_stm32l1xx_master_send_slave_address(Config, SlaveAddress, ReadMode)
#define ttc_driver_i2c_master_condition_stop(Config) i2c_stm32l1xx_master_condition_stop(Config)
#define ttc_driver_i2c_check_flag_transmit_buffer_empty(BaseRegister) i2c_stm32l1xx_check_flag_transmit_buffer_empty(BaseRegister)
#define ttc_driver_i2c_master_read_bytes(Config, Amount, Buffer) i2c_stm32l1xx_master_read_bytes(Config, Amount, Buffer)
#define ttc_driver_i2c_master_send_bytes(Config, Amount, Buffer) i2c_stm32l1xx_master_send_bytes(Config, Amount, Buffer)
#define ttc_driver_i2c_slave_read_bytes(Config, MaxAmount, Buffer) i2c_stm32l1xx_slave_read_bytes(Config, MaxAmount, Buffer)
#define ttc_driver_i2c_slave_send_bytes(Config, MaxAmount, Buffer) i2c_stm32l1xx_slave_send_bytes(Config, MaxAmount, Buffer)
#define ttc_driver_i2c_slave_check_own_address_received(BaseRegister) i2c_stm32l1xx_slave_check_own_address_received(BaseRegister)
#define ttc_driver_i2c_slave_check_read_mode(BaseRegister) i2c_stm32l1xx_slave_check_read_mode(BaseRegister)
#define ttc_driver_i2c_slave_reset_bus(Config) i2c_stm32l1xx_slave_reset_bus(Config)
#define ttc_driver_i2c_check_flag(BaseRegister, FlagCode) i2c_stm32l1xx_check_flag(BaseRegister, FlagCode)
#define ttc_driver_i2c_get_event_value(BaseRegister) i2c_stm32l1xx_get_event_value(BaseRegister)
#define ttc_driver_i2c_enable_acknowledge(BaseRegister, Enable) i2c_stm32l1xx_enable_acknowledge(BaseRegister, Enable)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Check Functions ******************************************************{
 *
 * Inline functions are almost as fast as macros.
 * The advantage of inline functions is that they are typesafe. Their arguments types can be checked by the compiler.
 */

/* DEPRECATED
BOOL i2c_stm32l1xx_check_event_master_byte_received(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_master_byte_transmitted(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_master_byte_transmitting(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_master_receiver_mode_selected(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_master_slave_has_acknowledged_start(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_master_transmitter_mode_selected(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_bus_not_busy(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_slave_ack_failure(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_slave_byte_received(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_slave_byte_transmitted(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_slave_byte_transmitting(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_slave_general_call_address_matched(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_slave_receiver_address_matched(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_slave_receiver_second_address_matched(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_slave_stop_detected(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_slave_transmitter_address_matched(t_u32 EventValue);
BOOL i2c_stm32l1xx_check_event_slave_transmitter_second_address_matched(t_u32 EventValue);

BOOL i2c_stm32l1xx_check_flag_bus_is_busy(volatile t_ttc_i2c_base_register* BaseRegister);
BOOL i2c_stm32l1xx_check_flag_bytes_received(volatile t_ttc_i2c_base_register* BaseRegister);
BOOL i2c_stm32l1xx_check_flag_bytes_transmitted(volatile t_ttc_i2c_base_register* BaseRegister);
BOOL i2c_stm32l1xx_check_flag_error_arbitration_lost(volatile t_ttc_i2c_base_register* BaseRegister);
BOOL i2c_stm32l1xx_check_flag_error_misplaced_condition(volatile t_ttc_i2c_base_register* BaseRegister);
BOOL i2c_stm32l1xx_check_flag_error_no_acknowledge(volatile t_ttc_i2c_base_register* BaseRegister);
BOOL i2c_stm32l1xx_check_flag_error_parity(volatile t_ttc_i2c_base_register* BaseRegister);
BOOL i2c_stm32l1xx_check_flag_error_slave_buffer_overrun(volatile t_ttc_i2c_base_register* BaseRegister);
BOOL i2c_stm32l1xx_check_flag_general_call_received(volatile t_ttc_i2c_base_register* BaseRegister);
BOOL i2c_stm32l1xx_check_flag_master_mode(volatile t_ttc_i2c_base_register* BaseRegister);
BOOL i2c_stm32l1xx_check_flag_smb_default_address_received(volatile t_ttc_i2c_base_register* BaseRegister);
BOOL i2c_stm32l1xx_check_flag_smb_host_header_received(volatile t_ttc_i2c_base_register* BaseRegister);
BOOL i2c_stm32l1xx_check_flag_smb_error_timeout(volatile t_ttc_i2c_base_register* BaseRegister);

// checks in SMBus mode if a timeout has occured on SMBus
//
// @param LogicalIndex (t_u8)               device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
// @param Config       (t_ttc_i2c_config*)  configuration of I2C device
// @return             (BOOL)               != 0: flag is active; == 0: inactive
// @param BaseRegister   =
//
BOOL i2c_stm32l1xx_check_flag_error_smbus_timeout(volatile t_ttc_i2c_base_register* BaseRegister);

// checks if transmit buffer has space for another byte
//
// @param LogicalIndex (t_u8)                     device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
// @param BaseRegister (t_ttc_i2c_base_register*) adress of base of low-level peripheral register responsible for I2C device
// @return             (BOOL)                     != 0: flag is active; == 0: inactive
///
BOOL i2c_stm32l1xx_check_flag_transmit_buffer_empty(volatile t_ttc_i2c_base_register* BaseRegister);

*/

//}Inline Functions
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_i2c.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_i2c.h for details.
 *
 * You normally should not need to add functions here. 
 * Add your private functions in the private section below. 
 */


/** shutdown single I2C unit device
 * @param Config        Configuration of i2c device
 * @return              == 0: I2C has been shutdown successfully; != 0: error-code
 */
e_ttc_i2c_errorcode i2c_stm32l1xx_deinit(t_ttc_i2c_config* Config);


/** fills out given Config with maximum valid values for indexed I2C
 * @param Config  = Configuration of i2c device
 * @return        pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_i2c_config* i2c_stm32l1xx_get_features(t_ttc_i2c_config* Config);


/** initializes single I2C unit for operation
 * @param Config        Configuration of i2c device
 * @return              == 0: I2C has been initialized successfully; != 0: error-code
 */
e_ttc_i2c_errorcode i2c_stm32l1xx_init(t_ttc_i2c_config* Config);


/** loads configuration of indexed I2C unit with default values
 * @param Config        Configuration of i2c device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_i2c_errorcode i2c_stm32l1xx_load_defaults(t_ttc_i2c_config* Config);


/** Prepares i2c Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void i2c_stm32l1xx_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of i2c device
 */
void i2c_stm32l1xx_reset(t_ttc_i2c_config* Config);


/** Generic receiving of individual bytes from I2C bus
 *
 * Note: Reading of individual bytes only makes sense if someone on the bus is willing to send at the moment.
 *
 * @param Config       (t_ttc_i2c_config)    Configuration of i2c device
 * @param Amount       (t_u8)                amount of bytes to read
 * @param Buffer       (t_u8*)               bytes being read will be stored in this buffer
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
e_ttc_i2c_errorcode i2c_stm32l1xx_read_bytes(t_ttc_i2c_config* Config, t_u8 Amount, t_u8* Buffer);


/** Generic sending of individual bytes to I2C bus.
 *
 * Note: Sending of individual bytes only makes sense if someone on the bus is expecting this at the moment.
 *
 * @param Config       (t_ttc_i2c_config)    Configuration of i2c device
 * @param Amount       (t_u8)                amount of bytes to read
 * @param Buffer       (const t_u8*)         bytes to send will be read from this buffer
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
e_ttc_i2c_errorcode i2c_stm32l1xx_write_bytes(t_ttc_i2c_config* Config, t_u8 Amount, const t_u8* Buffer);

/** Generates a start condition on I2C bus.
 *
 * Note: This function may only be called if I2C device has been initialized as master!
 *
 * @param LogicalIndex (t_u8)                device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param Config       (t_ttc_i2c_config*)   configuration of I2C device
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
e_ttc_i2c_errorcode i2c_stm32l1xx_master_condition_start(t_ttc_i2c_config* Config);


/** Sends out given slave address
 *
 * Note: This function may only be called if I2C device has been initialized as master!
 *
 * @param LogicalIndex (t_u8)                device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param SlaveAddress (t_u16)               I2C address of desired slave to send (only lower 7- or 10-bits are used)
 * @param ReadMode     (BOOL)                ==TRUE: Switch interface into read mode after sending address; write mode otherwise
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 * @param Config   = 
 */
e_ttc_i2c_errorcode i2c_stm32l1xx_master_send_slave_address(t_ttc_i2c_config* Config, t_u16 SlaveAddress, BOOL ReadMode);


/** Generates a stop condition on I2C bus.
 *
 * Note: This function may only be called if I2C device has been initialized as master!
 *
 * @param LogicalIndex (t_u8)                device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param Config       (t_ttc_i2c_config*)   configuration of I2C device
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
e_ttc_i2c_errorcode i2c_stm32l1xx_master_condition_stop(t_ttc_i2c_config* Config);


/** Generic receiving of individual bytes from I2C bus in master mode
 *
 * Note: Reading of individual bytes only makes sense if someone on the bus is willing to send at the moment.
 *
 * @param Config       (t_ttc_i2c_config)    Configuration of i2c device
 * @param Amount       (t_u8)                amount of bytes to read
 * @param Buffer       (t_u8*)               bytes being read will be stored in this buffer
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
e_ttc_i2c_errorcode i2c_stm32l1xx_master_read_bytes(t_ttc_i2c_config* Config, t_u8 Amount, t_u8* Buffer);


/** Generic sending of individual bytes to I2C bus in master mode.
 *
 * Note: Sending of individual bytes only makes sense if someone on the bus is expecting this at the moment.
 *
 * @param Config       (t_ttc_i2c_config)    Configuration of i2c device
 * @param Amount       (t_u8)                amount of bytes to write
 * @param Buffer       (const t_u8*)         bytes to send will be read from this buffer
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
e_ttc_i2c_errorcode i2c_stm32l1xx_master_send_bytes(t_ttc_i2c_config* Config, t_u8 Amount, const t_u8* Buffer);


/** Generic receiving of individual bytes from I2C bus in slave mode
 *
 * Note: Reading of individual bytes only makes sense if someone on the bus is willing to send at the moment.
 *
 * @param Config       (t_ttc_i2c_config)    Configuration of i2c device
 * @param MaxAmount    (t_u8)                no more than this amount of bytes will be read (master will end operation)
 * @param Buffer       (t_u8*)               bytes being read will be stored in this buffer
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
t_u8 i2c_stm32l1xx_slave_read_bytes(t_ttc_i2c_config* Config, t_u8 MaxAmount, t_u8* Buffer);


/** Generic sending of individual bytes to I2C bus in slave mode.
 *
 * Note: Sending of individual bytes only makes sense if someone on the bus is expecting this at the moment.
 *
 * @param Config       (t_ttc_i2c_config)    Configuration of i2c device
 * @param MaxAmount    (t_u8)                no more than this amount of bytes will be written (master will end operation)
 * @param Buffer       (const t_u8*)         bytes to send will be read from this buffer
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
t_u8 i2c_stm32l1xx_slave_send_bytes(t_ttc_i2c_config* Config, t_u8 MaxAmount, const t_u8* Buffer);


/** Checks in slave mode if an address matching to OwnAddress has been received
 *
 * In slave mode, the I2C interface is constantly watching the lines for an incoming slave address.
 * If this address matches OwnAddress1 or OwnAddress2 (in dual addressing mode), then another
 * master is calling us.
 *
 * @param BaseRegister (t_ttc_i2c_base_register*)  address of base of peripheral I2C register
 * @return             (BOOL)  !=0: someone is calling us; ==0: no matching address received
 */
BOOL i2c_stm32l1xx_slave_check_own_address_received(volatile t_ttc_i2c_base_register* BaseRegister);


/** Checks in slave mode if last received address implied a read mode.
 *
 * Calling this function makes only sense after ttc_i2c_slave_check_own_address_received()
 * returned TRUE.
 *
 * @param BaseRegister (t_ttc_i2c_base_register*)  address of base of peripheral I2C register
 * @return             (t_u8)  ==TRUE: master wants to read data, master wants to write data
 */
BOOL i2c_stm32l1xx_slave_check_read_mode(volatile t_ttc_i2c_base_register* BaseRegister);

/** resets interface to release SDA and SCK lines in slave mode
 *
 * @param LogicalIndex (t_u8)  device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param Config       (t_ttc_i2c_config)    Configuration of i2c device
 * @return             (e_ttc_i2c_errorcode) ==0: reset went successfull; error-code otherwise
 */
e_ttc_i2c_errorcode i2c_stm32l1xx_slave_reset_bus(t_ttc_i2c_config* Config);


/** Reads all I2C flags for given register base
 *
 * This function is the base for all check functions below. Its return value is given to a _check_ function
 * to determine the current state of a flag or event.
 *
 * @param BaseRegister (t_ttc_i2c_base_register*)  address of base of peripheral I2C register
 * @return             (BOOL)  !=0: someone is calling us; ==0: no matching address received
 */
t_base i2c_stm32l1xx_get_event_value(volatile t_ttc_i2c_base_register* BaseRegister);


/** checks if transmit buffer has space for another byte
 *
 * @param LogicalIndex (t_u8)                     device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param BaseRegister (t_ttc_i2c_base_register*) adress of base of low-level peripheral register responsible for I2C device
 * @return             (BOOL)                     != 0: flag is active; == 0: inactive
 * @param FlagCode   = 
 */
BOOL i2c_stm32l1xx_check_flag(volatile t_ttc_i2c_base_register* BaseRegister, e_ttc_i2c_flag_code FlagCode);


/** Enables/ Disables acknowledgement of incoming bytes
 *
 * @param LogicalIndex (t_u8)  device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param BaseRegister (t_ttc_i2c_base_register*)  address of base of peripheral I2C register
 */
void i2c_stm32l1xx_enable_acknowledge(volatile t_ttc_i2c_base_register* BaseRegister, BOOL Enable);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //I2C_STM32L1XX_H
