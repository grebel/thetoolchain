#ifndef i2c_common_h
#define i2c_common_h

/** i2c_common.h *****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to i2c low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.h revision 12 at 20150531 21:41:38 UTC
 *
 *  Authors: Gregor Rebel
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_I2C_COMMON
//
// Implementation status of low-level driver support for i2c devices on common
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_I2C_COMMON '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_I2C_COMMON == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_I2C_COMMON to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_I2C_COMMON

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "i2c_stm32l1xx.c"
 */

#include "../ttc_i2c_types.h" // will include i2c_stm32l1xx_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_i2c_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_i2c_foo
 */

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions
/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_i2c.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_i2c.h for details.
 *
 * You normally should not need to add functions here.
 * Global feature functions must be added in ttc_i2c.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl i2c UPDATE".
 */

/** General delay function to be used by all low-level drivers when waiting for events/ flags
 *
 * @return  amount of microseconds being delayed
 */
t_u16 i2c_common_delay();


/** Waits until event for given event code takes place or timeout occurs
 *
 * Note: Each Low Level should use this function to wait for an event for correct timeout and retries!
 * Note: Calls _driver_i2c_get_event_value() periodically to obtain current event value.
 *
 * @param BaseRegister    (volatile t_ttc_i2c_base_register*) will be passed to checkFunction()
 * @param TimeOut         (t_u32)                             maximum time to wait (us)
 * @param AmountRetries   (t_u16)                             maximum amount of retries before putting task to sleep (busy polling can speed up waiting)
 * @return                (e_ttc_i2c_event_code)              ==0: no event from EventCodes[] did not occure before timeout; event code otherwise
 */
e_ttc_i2c_event_code i2c_common_wait_until_event( e_ttc_i2c_event_code EventCode,
                                                  volatile t_ttc_i2c_base_register* BaseRegister,
                                                  t_u32 TimeOut,
                                                  t_u16 AmountRetries
                                                );

/** Waits until one of severals events for given event codes takes place or timeout occurs
 *
 * Note: Each Low Level should use this function to wait for multiple events for correct timeout and retries!
 * Note: Calls _driver_i2c_get_event_value() periodically to obtain current event value.
 *
 * @param EventCodes      (const e_ttc_i2c_event_code [])     zero terminated array of event codes
 * @param BaseRegister    (volatile t_ttc_i2c_base_register*) will be passed to checkFunction()
 * @param TimeOut         (t_u32)                             maximum time to wait (us)
 * @param AmountRetries   (t_u16)                             maximum amount of retries before putting task to sleep (busy polling can speed up waiting)
 * @return                (e_ttc_i2c_event_code)              ==0: no event from EventCodes[] did not occure before timeout; event code otherwise
 */
e_ttc_i2c_event_code i2c_common_wait_until_events( const e_ttc_i2c_event_code EventCodes[],
                                                   volatile t_ttc_i2c_base_register* BaseRegister,
                                                   t_u32 TimeOut,
                                                   t_u16 AmountRetries
                                                 );

/** Checks if given event value contains event for given event code
 *
 * See ttc_i2c_get_event_value() for I2C event description
 *
 * @param EventValue (t_base)               return value from ttc_i2c_get_event_value()
 * @param EventCode  (e_ttc_i2c_event_code) assembly of bits defining a certain I2C event
 * @return           (e_ttc_i2c_event_code) ==0: no event from EventCodes[] did not occure before timeout; event code otherwise
 */
e_ttc_i2c_event_code i2c_common_check_event( t_base EventValue, e_ttc_i2c_event_code EventCode );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //i2c_common_h
