#ifndef I2C_STM32L1XX_TYPES_H
#define I2C_STM32L1XX_TYPES_H

/** { DEPRECATED_i2c_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for I2C devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_i2c_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20141007 15:32:58 UTC
 *
 *  Note: See ttc_i2c.h for description of architecture independent I2C implementation.
 *
 *  Authors: Gregor Rebel
 *
} */
//{ Defines/ TypeDefs **********************************************************

// defining drivers that are required by i2c_common.c
#define ttc_driver_i2c_get_event_value(BaseRegister) i2c_stm32l1xx_get_event_value(BaseRegister)
#define ttc_driver_i2c_check_flag(BaseRegister, FlagCode) i2c_stm32l1xx_check_flag(BaseRegister, FlagCode)

/* DEPRECATED
// These constants are defined to allow fast macros
#define I2C_EVENT_MASTER_MODE_SELECT                        0x00030001  // BUSY, MSL and SB flag
#define I2C_EVENT_BUS_BUSY                                  0x00020000  // BUSY flag

#define I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED             0x00070082  // BUSY, MSL, ADDR, TXE and TRA flags
#define I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED          0x00030002  // BUSY, MSL and ADDR flags

#define I2C_EVENT_MASTER_BYTE_TRANSMITTED                   0x00070084  // TRA, BUSY, MSL, TXE and BTF flags
#define I2C_EVENT_MASTER_BYTE_RECEIVED                      0x00030040  // BUSY, MSL and RXNE flags
#define I2C_EVENT_MASTER_BYTE_TRANSMITTING                  0x00070080  // TRA, BUSY, MSL, TXE flags
#define I2C_EVENT_SLAVE_RECEIVER_ADDRESS_MATCHED            0x00020002  // BUSY and ADDR flags
#define I2C_EVENT_SLAVE_TRANSMITTER_ADDRESS_MATCHED         0x00060082  // TRA, BUSY, TXE and ADDR flags
#define I2C_EVENT_SLAVE_RECEIVER_SECONDADDRESS_MATCHED      0x00820000  // DUALF and BUSY flags
#define I2C_EVENT_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED   0x00860080  // DUALF, TRA, BUSY and TXE flags
#define I2C_EVENT_SLAVE_GENERALCALLADDRESS_MATCHED          0x00120000  // GENCALL and BUSY flags
#define I2C_EVENT_SLAVE_BYTE_RECEIVED                       0x00020040  // BUSY and RXNE flags
#define I2C_EVENT_SLAVE_STOP_DETECTED                       0x00000010  // STOPF flag
#define I2C_EVENT_SLAVE_BYTE_TRANSMITTED                    0x00060084  // TRA, BUSY, TXE and BTF flags
#define I2C_EVENT_SLAVE_BYTE_TRANSMITTING                   0x00060080  // TRA, BUSY and TXE flags
#define I2C_EVENT_SLAVE_ACK_FAILURE                         0x00000400  // AF flag
#define I2C_FLAG_SR1_SMBALERT                               0x00008000 // SMBus Alert occured (SMBALERT pin triggered or response address header received)
#define I2C_FLAG_SR1_TIMEOUT                                0x00004000 // Timeout occured in SMBus Mode
#define I2C_FLAG_SR1_PECERR                                 0x00001000 // CRC Error for received byte                         (Slave mode)
#define I2C_FLAG_SR1_OVR                                    0x00000800 // Buffer Overrun                                      (Slave mode)
#define I2C_FLAG_SR1_AF                                     0x00000400 // Acknowledge Failure
#define I2C_FLAG_SR1_ARLO                                   0x00000200 // Arbitration Lost detected                           (Master mode)
#define I2C_FLAG_SR1_BERR                                   0x00000100 // Bus Error detected                                  (Master mode)
#define I2C_FLAG_SR1_TXE                                    0x00000080 // Transmit Data Register Empty     (not set during address phase)
#define I2C_FLAG_SR1_RXNE                                   0x00000040 // Receive Data Register not Empty  (not set during address phase)
#define I2C_FLAG_SR1_STOPF                                  0x00000010 // Stop Condition detected                             (Slave mode)
#define I2C_FLAG_SR1_ADD10                                  0x00000008 // 10 bit header sent                                  (Master mode)
#define I2C_FLAG_SR1_BTF                                    0x00000004 // Byte Transfer Finished
#define I2C_FLAG_SR1_ADDR                                   0x00000002 // Address Sent                                        (Master mode)
#define I2C_FLAG_SR1_SB                                     0x00000001 // Start Bit                                           (Master mode)
#define I2C_FLAG_SR2_DUALF                                  0x00000080 // ==1: received address matches OAR1; OAR2 otherwise  (Slave mode)
#define I2C_FLAG_SR2_SMBHOST                                0x00000040 // Matching SMBus Host Header received                 (Slave mode)
#define I2C_FLAG_SR2_SMBDEFAULT                             0x00000020 // Matching SMBus Default Address received             (Slave mode)
#define I2C_FLAG_SR2_GENCALL                                0x00000010 // Matching General Call Address received              (Slave mode)
#define I2C_FLAG_SR2_TRA                                    0x00000004 // read/write bit of last address byte
#define I2C_FLAG_SR2_BUSY                                   0x00000002 // I2C bus is busy (SDA or SCL is low)
#define I2C_FLAG_SR2_MSL                                    0x00000001 // ==1: Device operates in master mode; slave mode otherwise
*/

// Events required by ttc_i2c_types.h
/** FIX: Event values swapped because TRA==R/W-bit of address byte and 1 means master wants to READ data
 * original definitions:
   #define TTC_I2C_EVENT_CODE_MASTER_TRANSMITTER_MODE_SELECTED          0x00070082  // BUSY, MSL, ADDR, TXE and TRA flags
   #define TTC_I2C_EVENT_CODE_MASTER_RECEIVER_MODE_SELECTED             0x00030002  // BUSY, MSL and ADDR flags
*/
// Note: MSB of EVENT_CODE == 1 means event is negated!
#define TTC_I2C_EVENT_CODE_MASTER_RECEIVER_MODE_SELECTED             0x00070082  // BUSY, MSL, ADDR, TXE and TRA flags
#define TTC_I2C_EVENT_CODE_MASTER_TRANSMITTER_MODE_SELECTED          0x00030002  // BUSY, MSL and ADDR flags
#define TTC_I2C_EVENT_CODE_MASTER_MODE_SELECT                        0x00030001  // BUSY, MSL and SB flag
#define TTC_I2C_EVENT_CODE_BUS_BUSY                                  0x00020000  // BUSY flag
#define TTC_I2C_EVENT_CODE_BUS_FREE                                  0x80020000  // BUSY flag (MSB bit set negates bit state)
#define TTC_I2C_EVENT_CODE_MASTER_BYTE_TRANSMITTED                   0x00070084  // TRA, BUSY, MSL, TXE and BTF flags
#define TTC_I2C_EVENT_CODE_MASTER_BYTE_RECEIVED                      0x00030040  // BUSY, MSL and RXNE flags
#define TTC_I2C_EVENT_CODE_MASTER_BYTE_TRANSMITTING                  0x00070080  // TRA, BUSY, MSL, TXE flags
#define TTC_I2C_EVENT_CODE_SLAVE_RECEIVER_ADDRESS_MATCHED            0x00020002  // BUSY and ADDR flags
#define TTC_I2C_EVENT_CODE_SLAVE_TRANSMITTER_ADDRESS_MATCHED         0x00060082  // TRA, BUSY, TXE and ADDR flags
#define TTC_I2C_EVENT_CODE_SLAVE_RECEIVER_SECONDADDRESS_MATCHED      0x00820000  // DUALF and BUSY flags
#define TTC_I2C_EVENT_CODE_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED   0x00860080  // DUALF, TRA, BUSY and TXE flags
#define TTC_I2C_EVENT_CODE_SLAVE_GENERALCALLADDRESS_MATCHED          0x00120000  // GENCALL and BUSY flags
#define TTC_I2C_EVENT_CODE_SLAVE_BYTE_RECEIVED                       0x00020040  // BUSY and RXNE flags
#define TTC_I2C_EVENT_CODE_SLAVE_STOP_DETECTED                       0x00000010  // STOPF flag
#define TTC_I2C_EVENT_CODE_SLAVE_BYTE_TRANSMITTED                    0x00060084  // TRA, BUSY, TXE and BTF flags
#define TTC_I2C_EVENT_CODE_SLAVE_BYTE_TRANSMITTING                   0x00060080  // TRA, BUSY and TXE flags
#define TTC_I2C_EVENT_CODE_SLAVE_ACK_FAILURE                         0x00000400  // AF flag
#define TTC_I2C_EVENT_CODE_SLAVE_BUFFER_OVERRUN                      0x00000800  // OVR flag

// Note: MSB of flag code == 1 meas that flag is located in SR2 instead of SR1!
#define TTC_I2C_FLAG_CODE_SMBUS_ALERT                                0x00008000  // SR1: SMBus Alert occured (SMBALERT pin triggered or response address header received)
#define TTC_I2C_FLAG_CODE_SMBUS_TIMEOUT                              0x00004000  // SR1: Timeout occured in SMBus Mode
#define TTC_I2C_FLAG_CODE_CRC_ERROR                                  0x00001000  // SR1: CRC Error for received byte                         (Slave mode)
#define TTC_I2C_FLAG_CODE_BUFFER_OVERRUN                             0x00000800  // SR1: Buffer Overrun                                      (Slave mode)
#define TTC_I2C_FLAG_CODE_ACKNOWLEDGE_FAILURE                        0x00000400  // SR1: Acknowledge Failure
#define TTC_I2C_FLAG_CODE_ARBITRATION_LOST                           0x00000200  // SR1: Arbitration Lost detected                           (Master mode)
#define TTC_I2C_FLAG_CODE_BUS_ERROR                                  0x00000100  // SR1: Bus Error detected                                  (Master mode)
#define TTC_I2C_FLAG_CODE_TRANSMIT_BUFFER_EMPTY                      0x00000080  // SR1: Transmit Data Register Empty     (not set during address phase)
#define TTC_I2C_FLAG_CODE_RECEIVE_BUFFER_NOT_EMPTY                   0x00000040  // SR1: Receive Data Register not Empty  (not set during address phase)
#define TTC_I2C_FLAG_CODE_STOP_DETECTED                              0x00000010  // SR1: Stop Condition detected                             (Slave mode)
#define TTC_I2C_FLAG_CODE_10_BIT_ADDRESS                             0x00000008  // SR1: 10 bit header sent                                  (Master mode)
#define TTC_I2C_FLAG_CODE_BYTE_TRANSFER_FINISHED                     0x00000004  // SR1: Byte Transfer Finished
#define TTC_I2C_FLAG_CODE_ADDRESS_SENT                               0x00000002  // SR1: Address Sent                                        (Master mode)
#define TTC_I2C_FLAG_CODE_START_BIT_DETECTED                         0x00000001  // SR1: Start Bit                                           (Master mode)
#define TTC_I2C_FLAG_CODE_OWNADDRESS1_MATCHED                        0x80000080  // SR2: ==1: received address matches OAR1; OAR2 otherwise  (Slave mode)
#define TTC_I2C_FLAG_CODE_SMBUS_HOST_HEADER_RECEIVED                 0x80000040  // SR2: Matching SMBus Host Header received                 (Slave mode)
#define TTC_I2C_FLAG_CODE_SMBUS_DEFAULT_ADDRESS_RECEIVED             0x80000020  // SR2: Matching SMBus Default Address received             (Slave mode)
#define TTC_I2C_FLAG_CODE_SMBUS_GENERAL_CALL_ADDRESS_RECEIVED        0x80000010  // SR2: Matching General Call Address received              (Slave mode)
#define TTC_I2C_FLAG_CODE_READ_MODE_DETECTED                         0x80000004  // SR2: read/write bit of last address byte
#define TTC_I2C_FLAG_CODE_BUS_BUSY                                   0x80000002  // SR2: I2C bus is busy (SDA or SCL is low)
#define TTC_I2C_FLAG_CODE_MASTER_MODE                                0x80000001  // SR2: ==1: Device operates in master mode; slave mode otherwise

#ifndef TTC_I2C1   // device not defined in makefile
    #define TTC_I2C1    E_ttc_i2c_architecture_stm32l1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//{ Enums required by i2c_stm32l1xx.h *************************


/* DEPRECATED
// these enums are defined to allow typesafe function prototypes that allow compiler to check arguments at compile time
typedef enum { // bitmasks of combinations of multiple flags in SR1 and SR2
    ise_None,
    i2c_event_MASTER_MODE_SELECT                       = I2C_EVENT_MASTER_MODE_SELECT,                       // BUSY, MSL and SB flag
    i2c_event_BUS_BUSY                                 = I2C_EVENT_BUS_BUSY,                                 // BUSY flag
    i2c_event_MASTER_TRANSMITTER_MODE_SELECTED         = I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED,         // BUSY, MSL, ADDR, TXE and TRA flags
    i2c_event_MASTER_RECEIVER_MODE_SELECTED            = I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED,            // BUSY, MSL and ADDR flags
    i2c_event_MASTER_BYTE_TRANSMITTED                  = I2C_EVENT_MASTER_BYTE_TRANSMITTED,                  // TRA, BUSY, MSL, TXE and BTF flags
    i2c_event_MASTER_BYTE_RECEIVED                     = I2C_EVENT_MASTER_BYTE_RECEIVED,                     // BUSY, MSL and RXNE flags
    i2c_event_MASTER_BYTE_TRANSMITTING                 = I2C_EVENT_MASTER_BYTE_TRANSMITTING,                 // TRA, BUSY, MSL, TXE flags
    i2c_event_SLAVE_RECEIVER_ADDRESS_MATCHED           = I2C_EVENT_SLAVE_RECEIVER_ADDRESS_MATCHED,           // BUSY and ADDR flags
    i2c_event_SLAVE_TRANSMITTER_ADDRESS_MATCHED        = I2C_EVENT_SLAVE_TRANSMITTER_ADDRESS_MATCHED,        // TRA, BUSY, TXE and ADDR flags
    i2c_event_SLAVE_RECEIVER_SECONDADDRESS_MATCHED     = I2C_EVENT_SLAVE_RECEIVER_SECONDADDRESS_MATCHED,     // DUALF and BUSY flags
    i2c_event_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED  = I2C_EVENT_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED,  // DUALF, TRA, BUSY and TXE flags
    i2c_event_SLAVE_GENERALCALLADDRESS_MATCHED         = I2C_EVENT_SLAVE_GENERALCALLADDRESS_MATCHED,         // GENCALL and BUSY flags
    i2c_event_SLAVE_BYTE_RECEIVED                      = I2C_EVENT_SLAVE_BYTE_RECEIVED,                      // BUSY and RXNE flags
    i2c_event_SLAVE_STOP_DETECTED                      = I2C_EVENT_SLAVE_STOP_DETECTED,                      // STOPF flag
    i2c_event_SLAVE_BYTE_TRANSMITTED                   = I2C_EVENT_SLAVE_BYTE_TRANSMITTED,                   // TRA, BUSY, TXE and BTF flags
    i2c_event_SLAVE_BYTE_TRANSMITTING                  = I2C_EVENT_SLAVE_BYTE_TRANSMITTING,                  // TRA, BUSY and TXE flags
    i2c_event_SLAVE_ACK_FAILURE                        = I2C_EVENT_SLAVE_ACK_FAILURE,                        // AF flag
} e_i2c_stm32l1xx_event;
typedef enum { // bitmasks for individual flags in SR1 and SR2 registers (-> RM0038 p. 603)
    i2c_flag_sr1_None,

    i2c_flag_sr1_SMBALERT   = I2C_FLAG_SR1_SMBALERT, // SMBus Alert occured (SMBALERT pin triggered or response address header received)
    i2c_flag_sr1_TIMEOUT    = I2C_FLAG_SR1_TIMEOUT,  // Timeout occured in SMBus Mode
    i2c_flag_sr1_PECERR     = I2C_FLAG_SR1_PECERR,   // CRC Error for received byte                         (Slave mode)
    i2c_flag_sr1_OVR        = I2C_FLAG_SR1_OVR,      // Buffer Overrun                                      (Slave mode)
    i2c_flag_sr1_AF         = I2C_FLAG_SR1_AF,       // Acknowledge Failure
    i2c_flag_sr1_ARLO       = I2C_FLAG_SR1_ARLO,     // Arbitration Lost detected                           (Master mode)
    i2c_flag_sr1_BERR       = I2C_FLAG_SR1_BERR,     // Bus Error detected                                  (Master mode)
    i2c_flag_sr1_TXE        = I2C_FLAG_SR1_TXE,      // Transmit Data Register Empty     (not set during address phase)
    i2c_flag_sr1_RXNE       = I2C_FLAG_SR1_RXNE,     // Receive Data Register not Empty  (not set during address phase)
    i2c_flag_sr1_STOPF      = I2C_FLAG_SR1_STOPF,    // Stop Condition detected                             (Slave mode)
    i2c_flag_sr1_ADD10      = I2C_FLAG_SR1_ADD10,    // 10 bit header sent                                  (Master mode)
    i2c_flag_sr1_BTF        = I2C_FLAG_SR1_BTF,      // Byte Transfer Finished
    i2c_flag_sr1_ADDR       = I2C_FLAG_SR1_ADDR,     // Address Sent                                        (Master mode)
    i2c_flag_sr1_SB         = I2C_FLAG_SR1_SB,       // Start Bit                                           (Master mode)

    i2c_flag_sr1_unknown
} e_i2c_stm32l1xx_flag_sr1;
typedef enum { // bitmasks for individual flags in SR1 and SR2 registers (-> RM0038 p. 603)
    i2c_flag_sr2_None,

    i2c_flag_sr2_DUALF      = I2C_FLAG_SR2_DUALF,      // ==1: received address matches OAR1; OAR2 otherwise  (Slave mode)
    i2c_flag_sr2_SMBHOST    = I2C_FLAG_SR2_SMBHOST,    // Matching SMBus Host Header received                 (Slave mode)
    i2c_flag_sr2_SMBDEFAULT = I2C_FLAG_SR2_SMBDEFAULT, // Matching SMBus Default Address received             (Slave mode)
    i2c_flag_sr2_GENCALL    = I2C_FLAG_SR2_GENCALL,    // Matching General Call Address received              (Slave mode)
    i2c_flag_sr2_TRA        = I2C_FLAG_SR2_TRA,        // read/write bit of last address byte
    i2c_flag_sr2_BUSY       = I2C_FLAG_SR2_BUSY,       // I2C bus is busy (SDA or SCL is low)
    i2c_flag_sr2_MSL        = I2C_FLAG_SR2_MSL,        // ==1: Device operates in master mode; slave mode otherwise

    i2c_flag_sr2_unknown
} e_i2c_stm32l1xx_flag_sr2;
*/
//} Enums
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines

//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "../ttc_mutex_types.h"
#include "../gpio/gpio_stm32l1xx.h"
#include "../register/register_stm32l1xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_i2c_types.h *************************

#ifdef DEPRECATED
typedef struct {
    t_u32 dma_PeripheralBaseAddr; // Specifies the peripheral base address for DMAy Channelx.
    t_u32 dma_MemoryBaseAddr;     // Specifies the memory base address for DMAy Channelx.
    t_u32 dma_DIR;                // Specifies if the peripheral is the source or destination.
    t_u32 dma_BufferSize;         // Specifies the buffer size, in data unit, of the specified Channel.
    t_u32 dma_PeripheralInc;      // Specifies whether the Peripheral address register is incremented or not.
    t_u32 dma_MemoryInc;          // Specifies whether the memory address register is incremented or not.
    t_u32 dma_PeripheralDataSize; // Specifies the Peripheral data width.
    t_u32 dma_MemoryDataSize;     // Specifies the Memory data width.
    t_u32 dma_Mode;               // Specifies the operation mode of the DMAy Channelx.
    t_u32 dma_Priority;           // Specifies the software priority for the DMAy Channelx.
    t_u32 dma_M2M;                // Specifies if the DMAy Channelx will be used in memory-to-memory transfer.
} dma_init_structure;

typedef struct {
    t_u32 i2c_ClockSpeed;          // Specifies the clock frequency.
    ut_int16 i2c_Mode;                // Specifies the I2C mode.
    ut_int16 i2c_DutyCycle;           // Specifies the I2C fast mode duty cycle.
    ut_int16 i2c_OwnAddress1;         // Specifies the first device own address.
    ut_int16 i2c_Ack;                 // Enables or disables the acknowledgement.
    ut_int16 i2c_AcknowledgedAddress; // Specifies if 7-bit or 10-bit address is acknowledged.
} i2c_init_structure;
#endif

typedef struct {  // stm32l1xx specific configuration data of single I2C device
    volatile t_register_stm32l1xx_dma_channel*  DMA_BaseTx;
    volatile t_register_stm32l1xx_dma_channel*  DMA_BaseRx;

#ifdef DEPRECATED
    //std configuration
    GPIO_InitTypeDef* GPIO;
    I2C_InitTypeDef* I2C;
    DMA_InitTypeDef* DMA;

    t_u8* RX;
    t_u8* TX;
    t_u8 NumBytes;

    t_u8 SlaveAddress;
#endif

} __attribute__( ( __packed__ ) ) t_i2c_stm32l1xx_config;

// t_ttc_i2c_architecture is required by ttc_i2c_types.h
#define t_ttc_i2c_architecture  t_i2c_stm32l1xx_config
#define t_ttc_i2c_base_register t_register_stm32l1xx_i2c

//} Structures/ Enums


#endif //I2C_STM32L1XX_TYPES_H
