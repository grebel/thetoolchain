/** { i2c_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  Low-Level driver for i2c devices on stm32l1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20141007 15:32:58 UTC
 *
 *  Note: See ttc_i2c.h for description of stm32l1xx independent I2C implementation.
 *
 *  Authors: <AUTHOR>
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "i2c_stm32l1xx.h".
//

#include "DEPRECATED_i2c_stm32l1xx.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
#define I2C_STM32L1XX_USE_TIMEOUTS 1 // ==0: use endless wait loops (faster but may block endlessly), !=0: wait loops have timeouts

//{ Global Variables ***********************************************************

//}Global Variables
//{ Private Function Declarations **********************************************

/** Check current state of single I2C-event
 *
 * @param Config   filled out struct ttc_i2c_config_t from i2c_stm32f1xx_Configs[]
 * @param Event    name of event to check
 * @return  ==0: event is not active; !=0: event is active
 */
BOOL _i2c_check_event(ttc_i2c_base_register_t* I2C_Base, i2c_stm32l1xx_event_e Event);

/** Check current state of single I2C-event
 *
 * @param Config   filled out struct ttc_i2c_config_t from i2c_stm32f1xx_Configs[]
 * @param Event    name of event to check
 * @return  ==0: event is not active; !=0: event is active
 */
BOOL _i2c_check_flag(register_stm32l1xx_i2c_t* I2Cx, i2c_stm32l1xx_flag_e I2C_FLAG);

void _i2c_stm32l1xx_dma_init(ttc_i2c_config_t* Config, dma_init_structure* dma_init, u8_t rx_tx);

//{ Private Function Declarations
//{ Function Definitions *******************************************************

     ttc_i2c_errorcode_e i2c_stm32l1xx_deinit(ttc_i2c_config_t* Config) {
    Assert_I2C(Config, ec_NULL); // pointers must not be NULL

    _i2c_de_init(Config);

    return (ttc_i2c_errorcode_e) 0;
}
     ttc_i2c_errorcode_e i2c_stm32l1xx_get_features(ttc_i2c_config_t* Config) {
    Assert_I2C(Config, ec_NULL); // pointers must not be NULL

    return tie_OK;
}
     ttc_i2c_errorcode_e i2c_stm32l1xx_init(ttc_i2c_config_t* Config) {

    u8_t NumBytes = Config->LowLevelConfig->NumBytes;
    u8_t LogicalIndex = Config->LogicalIndex;

    u8_t TxBuffer[NumBytes]; // BUG: stack memory used for DMA transfers!
    u8_t RxBuffer[NumBytes]; // BUG: stack memory used for DMA transfers!

    dma_init_structure dma_init;
    i2c_init_structure i2c_init;

    ttc_i2c_architecture_t* ConfigArch = Config->LowLevelConfig;

    if (ConfigArch == NULL) {
        ConfigArch = (ttc_i2c_architecture_t*)ttc_heap_alloc_zeroed( sizeof(ttc_i2c_architecture_t) );
        Config->LowLevelConfig = ConfigArch;
    }

    if (LogicalIndex == 2) { // BUG: Must be PhysicalIndex!
        Config->LowLevelConfig->Base = i2c2_address;
        Config->LowLevelConfig->Dma_baseTx = (register_stm32l1xx_dma_channel_t*) dma1_Channel4;
        Config->LowLevelConfig->Dma_baseRx = (register_stm32l1xx_dma_channel_t*) dma1_Channel5;
    }
    else{
        Config->LowLevelConfig->Base = i2c1_address;
        Config->LowLevelConfig->Dma_baseTx = (register_stm32l1xx_dma_channel_t*) dma1_Channel6;
        Config->LowLevelConfig->Dma_baseRx = (register_stm32l1xx_dma_channel_t*) dma1_Channel7;
    }

    if (LogicalIndex==2) {// BUG: Must be PhysicalIndex!

        /* I2C Periph clock enable */
        sysclock_stm32l1xx_RCC_APB1PeriphClockCmd(rcc_APB1Periph_I2C2, ENABLE);
        /* Enable the DMA periph */
        sysclock_stm32l1xx_RCC_AHBPeriphClockCmd(rcc_AHBPeriph_DMA1, ENABLE);
        /*!< SDA GPIO clock enable */
        sysclock_stm32l1xx_RCC_AHBPeriphClockCmd(rcc_AHBPeriph_GPIOB, ENABLE);
        /*!< SCL GPIO clock enable */
        sysclock_stm32l1xx_RCC_AHBPeriphClockCmd(rcc_AHBPeriph_GPIOB, ENABLE);

        /* Connect PXx to I2C_SCL */
        ttc_gpio_pin_AF_config(tgp_b10, gpio_AF_I2C2);
        /* Connect PXx to I2C_SDA */
        ttc_gpio_pin_AF_config(tgp_b11, gpio_AF_I2C2);

        /*!< Configure I2C SCL pin */
        ttc_gpio_init(tgp_b10,tgm_alternate_function_open_drain, tgs_40MHz);
        ttc_gpio_init(tgp_b11,tgm_alternate_function_open_drain, tgs_40MHz);

        /* DMAx Channel I2Cx Rx channel Config */
        //_dma_de_init(Config, 1);                                // [1]->dmaRX || [0]->dmaTX
        dma_init.dma_PeripheralBaseAddr = (u32_t)0x40005810;
        dma_init.dma_MemoryBaseAddr = (u32_t)RxBuffer;
        dma_init.dma_DIR = dma_DIR_PeripheralSRC;
        dma_init.dma_BufferSize = NumBytes;
        dma_init.dma_PeripheralInc = dma_PeripheralInc_Disable;
        dma_init.dma_MemoryInc = dma_MemoryInc_Enable;
        dma_init.dma_PeripheralDataSize = dma_PeripheralDataSize_Byte;
        dma_init.dma_MemoryDataSize = dma_MemoryDataSize_Byte;
        dma_init.dma_Mode = dma_Mode_Normal;
        dma_init.dma_Priority = dma_Priority_VeryHigh;
        dma_init.dma_M2M = dma_M2M_Disable;
        _i2c_stm32l1xx_dma_init(Config, &dma_init, 1);                        // [1]->dmaRX || [0]->dmaTX


        /* DMAx Channel I2Cx TX Config */
        //_dma_de_init(Config,0);
        dma_init.dma_MemoryBaseAddr = (u32_t)TxBuffer;
        dma_init.dma_DIR = dma_DIR_PeripheralDST;
        dma_init.dma_BufferSize = NumBytes;
        dma_init.dma_Priority = dma_Priority_High;
        _i2c_stm32l1xx_dma_init(Config, &dma_init, 0);


        /* DMAx Channelx enable */
        _i2c_stm32l1xx_dma_cmd(Config, ENABLE, 0);
        /* DMAx Channelx enable */
        _i2c_stm32l1xx_dma_cmd(Config, ENABLE, 1);


        /* I2C Config and Init */
        _i2c_de_init(Config);
        i2c_init.i2c_Mode = i2c_Mode_I2C;
        i2c_init.i2c_DutyCycle = i2c_DutyCycle_2;
        i2c_init.i2c_OwnAddress1 = Config->OwnAddress;
        i2c_init.i2c_Ack = i2c_Ack_Enable;
        i2c_init.i2c_ClockSpeed = 100000;
        i2c_init.i2c_AcknowledgedAddress = i2c_AcknowledgedAddress_7bit;
        _i2c_stm32l1xx_init(Config, &i2c_init);

        _i2c_stm32l1xx_cmd(Config, ENABLE);


    } else {

        /*!< I2C Periph clock enable */
        sysclock_stm32l1xx_RCC_APB1PeriphClockCmd(rcc_APB1Periph_I2C1, ENABLE);
        /* Enable the DMA periph */
        sysclock_stm32l1xx_RCC_AHBPeriphClockCmd(rcc_AHBPeriph_DMA1, ENABLE);
        /*!< SDA GPIO clock enable */
        sysclock_stm32l1xx_RCC_AHBPeriphClockCmd(rcc_AHBPeriph_GPIOB, ENABLE);
        /*!< SCL GPIO clock enable */
        sysclock_stm32l1xx_RCC_AHBPeriphClockCmd(rcc_AHBPeriph_GPIOB, ENABLE);


        /* Connect PXx to I2C_SCL */
        ttc_gpio_pin_AF_config(tgp_b6, gpio_AF_I2C1);
        ttc_gpio_pin_AF_config(tgp_b7, gpio_AF_I2C1);

        /* Configure I2C SCL pin */
        ttc_gpio_init(tgp_b6,tgm_alternate_function_open_drain, tgs_40MHz);
        ttc_gpio_init(tgp_b7,tgm_alternate_function_open_drain, tgs_40MHz);


        /* DMAx Channel I2Cx Rx channel Config */
        dma_init.dma_PeripheralBaseAddr = (u32_t)0x40005410;
        dma_init.dma_MemoryBaseAddr = (u32_t)RxBuffer;
        dma_init.dma_DIR = dma_DIR_PeripheralSRC;
        dma_init.dma_BufferSize = NumBytes;
        dma_init.dma_PeripheralInc = dma_PeripheralInc_Disable;
        dma_init.dma_MemoryInc = dma_MemoryInc_Enable;
        dma_init.dma_PeripheralDataSize = dma_PeripheralDataSize_Byte;
        dma_init.dma_MemoryDataSize = dma_MemoryDataSize_Byte;
        dma_init.dma_Mode = dma_Mode_Normal;
        dma_init.dma_Priority = dma_Priority_VeryHigh;
        dma_init.dma_M2M = dma_M2M_Disable;
        _i2c_stm32l1xx_dma_init(Config, &dma_init, 1);                        // [1]->dmaRX || [0]->dmaTX


        /* DMAx Channel I2Cx TX Config */
        dma_init.dma_MemoryBaseAddr = (u32_t)TxBuffer;
        dma_init.dma_DIR = dma_DIR_PeripheralDST;
        dma_init.dma_BufferSize = NumBytes;
        dma_init.dma_Priority = dma_Priority_High;
        _i2c_stm32l1xx_dma_init(Config, &dma_init, 0);


        /* DMAx ChannelTx enable */
        _i2c_stm32l1xx_dma_cmd(Config, ENABLE, 0);
        /* DMAx ChannelRx enable */
        _i2c_stm32l1xx_dma_cmd(Config, ENABLE, 1);


        /* I2C ENABLE */
        _i2c_de_init(Config);
        i2c_init.i2c_Mode = i2c_Mode_I2C;
        i2c_init.i2c_DutyCycle = i2c_DutyCycle_2;
        i2c_init.i2c_OwnAddress1 = Config->OwnAddress;
        i2c_init.i2c_Ack = i2c_Ack_Enable;
        i2c_init.i2c_ClockSpeed = 100000;
        i2c_init.i2c_AcknowledgedAddress = i2c_AcknowledgedAddress_7bit;
        _i2c_stm32l1xx_init(Config, &i2c_init);

        _i2c_stm32l1xx_cmd(Config, ENABLE);

    }

    return 0;

}
     ttc_i2c_errorcode_e i2c_stm32l1xx_load_defaults(ttc_i2c_config_t* Config) {

    /*
    Assert_I2C(Config, ec_NULL); // pointers must not be NULL
    u8_t I2C_Index = Config->LogicalIndex;
    Assert_I2C(I2C_Index > 0,    ec_InvalidArgument);
    //if (I2C_Index > TTC_AMOUNT_I2CS) return tie_DeviceNotFound;

    // assumption: *Config has been zeroed
    Config->Flags.All                   = 0;
    Config->Flags.Bits.Master           = 1;
    Config->Flags.Bits.ModeFast         = 0;
    Config->Flags.Bits.Slave            = 0;
    Config->Flags.Bits.DMA_TX           = 0;
    Config->Flags.Bits.DMA_RX           = 0;
    Config->Flags.Bits.SMBus            = 0;
    Config->Flags.Bits.SMBus_Alert      = 0;
    Config->Flags.Bits.SMBus_Host       = 0;
    Config->Flags.Bits.SMBus_ARP        = 0;
    Config->Flags.Bits.DutyCycle_16_9   = 0;
    Config->Flags.Bits.Acknowledgement  = 1;
    Config->Flags.Bits.Adress_10Bit     = 0;
    Config->Flags.Bits.PacketErrorCheck = 0;
    Config->Flags.Bits.Slave_NoStretch  = 0;
    Config->Flags.Bits.GeneralCalls     = 0;
    Config->Flags.Bits.Interrupt_Events = 0;
    Config->Flags.Bits.Interrupt_Errors = 0;
    Config->ClockSpeed                  = 1000; // Standard Mode

    Config->OwnAddress = 1;
    Config->TimeOut    = 200000;

    */

    ttc_i2c_architecture_t* ConfigArch = Config->LowLevelConfig;

    if (ConfigArch == NULL) {
        ConfigArch = (ttc_i2c_architecture_t*)ttc_heap_alloc_zeroed( sizeof(ttc_i2c_architecture_t) );
        Config->LowLevelConfig = ConfigArch;
    }

    Config->Flags.Bits.Master = 1;
    Config->OwnAddress = 0x00;
    Config->LowLevelConfig->NumBytes=1;

    return tie_OK;
}
                    void i2c_stm32l1xx_prepare() {

}
     ttc_i2c_errorcode_e i2c_stm32l1xx_reset(ttc_i2c_config_t* Config) {
    Assert_I2C(Config, ec_NULL); // pointers must not be NULL

#warning i2c_stm32l1xx_reset() is not implemented!

    return (ttc_i2c_errorcode_e) 0;
}
     ttc_i2c_errorcode_e i2c_stm32l1xx_read_register(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, u8_t* Buffer) {
    return (i2c_stm32l1xx_read_registers(I2C_Index,TargetAddress,Register,RegisterType,Buffer,1));
}
     ttc_i2c_errorcode_e i2c_stm32l1xx_read_registers(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, u8_t* Buffer, u16_t NumByteToRead) {

    ttc_i2c_config_t*  Config = ttc_i2c_get_configuration(I2C_Index);
    ttc_i2c_base_register_t* I2C_Base = i2c_stm32l1xx_get_base_register(Config);

    if (Config->Flags.Bits.Master || !Config->Flags.Bits.Slave) {


            // ENTR_CRT_SECTION();

            /* While the bus is busy */
            while (I2C_Base->SR2.Bits.BUSY == 1) {}

            /* Send START condition */
            I2C_CR1_t CR1 = I2C_Base->CR1;
            CR1.All |= i2c_CR1_START;
            I2C_Base->CR1 = CR1;

            /* Test on EV5 and clear it */
            while (_i2c_check_event(I2C_Base, i2c_event_MASTER_MODE_SELECT) == 0) {}

            /* Send MPU6050 address for write */
            _i2c_send7bit_Address(Config, TargetAddress<<1, i2c_Direction_Transmitter);

            /* Test on EV6 and clear it */
            while (!_i2c_check_event(I2C_Base, i2c_event_MASTER_TRANSMITTER_MODE_SELECTED));

            /* Clear EV6 by setting again the PE bit */
            _i2c_stm32l1xx_cmd(Config, ENABLE);

            /* Send the MPU6050's internal address to write to */
            I2C_DR_t DR = I2C_Base->DR;
            DR.All = Register;
            I2C_Base->DR = DR;

            /* Test on EV8 and clear it */
            while (!_i2c_check_event(I2C_Base, i2c_event_MASTER_BYTE_TRANSMITTED));

            /* Send STRAT condition a second time */
            CR1 = I2C_Base->CR1;
            CR1.All |= i2c_CR1_START;
            I2C_Base->CR1 = CR1;

            /* Test on EV5 and clear it */
            while (!_i2c_check_event(I2C_Base, i2c_event_MASTER_MODE_SELECT));

            /* Send MPU6050 address for read */
            _i2c_send7bit_Address(Config, TargetAddress<<1, i2c_Direction_Receiver);

            /* Test on EV6 and clear it */
            while (!_i2c_check_event(I2C_Base, i2c_event_MASTER_RECEIVER_MODE_SELECTED));

            /* While there is data to be read */
            while (NumByteToRead)
            {
                if (NumByteToRead == 1)
                {
                    /* Disable Acknowledgement */
                    CR1 = I2C_Base->CR1;
                    CR1.All &= (u16_t)~((u16_t)i2c_CR1_ACK);
                    I2C_Base->CR1 = CR1;

                    /* Send STOP Condition */
                    CR1 = I2C_Base->CR1;
                    CR1.All |= i2c_CR1_STOP;
                    I2C_Base->CR1 = CR1;

                }
                /* Test on EV7 and clear it */
                if (_i2c_check_event(I2C_Base, i2c_event_MASTER_BYTE_RECEIVED))
                {
                    /* Read a byte from the MPU6050 */
                    DR = I2C_Base->DR;
                    *Buffer = DR.All;
                    /* Point to the next location where the byte read will be saved */
                    Buffer++;
                    /* Decrement the read bytes counter */
                    NumByteToRead--;
                }
            }
            /* Enable Acknowledgement to be ready for another reception */
            CR1 = I2C_Base->CR1;
            CR1.All |= i2c_CR1_ACK;
            I2C_Base->CR1 = CR1;

            // EXT_CRT_SECTION();

    }

    return (ttc_i2c_errorcode_e) 0;

}
     ttc_i2c_errorcode_e i2c_stm32l1xx_write_register(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, const char Byte) {


    ttc_i2c_config_t*  Config = ttc_i2c_get_configuration(I2C_Index);
    ttc_i2c_base_register_t* I2C_Base = i2c_stm32l1xx_get_base_register(Config);


    if (Config->Flags.Bits.Master || !Config->Flags.Bits.Slave) {

            // wait until I2C1 is not busy any more
            while (I2C_Base->SR2.Bits.BUSY == 1) {}

            /* Send START condition */
            I2C_CR1_t CR1 = I2C_Base->CR1;
            CR1.All |= i2c_CR1_START;
            I2C_Base->CR1 = CR1;

            /* Test on EV5 and clear it */
            while (_i2c_check_event(I2C_Base, i2c_event_MASTER_MODE_SELECT) == 0) {}

            /* Send address for write */
            _i2c_send7bit_Address(Config, TargetAddress<<1, i2c_Direction_Transmitter);

            /* Test on EV6 and clear it */
            while (!_i2c_check_event(I2C_Base, i2c_event_MASTER_TRANSMITTER_MODE_SELECTED));

            /* Send the MPU6050's internal address to write to */
            I2C_DR_t DR = I2C_Base->DR;
            DR.All = Register;
            I2C_Base->DR = DR;

            /* Test on EV8 and clear it */
            while (!_i2c_check_event(I2C_Base, i2c_event_MASTER_BYTE_TRANSMITTING)) {}

            /* Send the byte to be written */
            DR = I2C_Base->DR;
            DR.All = Byte;
            I2C_Base->DR = DR;

            /* Test on EV8_2 and clear it */
            while (!_i2c_check_event(I2C_Base, i2c_event_MASTER_BYTE_TRANSMITTED));

            /* Send STOP condition */
            CR1 = I2C_Base->CR1;
            CR1.All |= i2c_CR1_STOP;
            I2C_Base->CR1 = CR1;

        }

    return (ttc_i2c_errorcode_e)0;
}
     ttc_i2c_errorcode_e i2c_stm32l1xx_write_registers(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, const u8_t* Buffer, u16_t Amount) {
    Assert_I2C(Buffer, ec_NULL); // pointers must not be NULL
    ttc_i2c_config_t*  Config = ttc_i2c_get_configuration(I2C_Index);
    ttc_i2c_architecture_t* ConfigArch = Config->LowLevelConfig;
    Assert_I2C(ConfigArch, ec_NULL);
    register_stm32l1xx_i2c_t* I2C_Base = ConfigArch->Base;
    Assert_I2C(I2C_Base, ec_NULL);
    u32_t MyTimeOut = Config->TimeOut;
    ttc_i2c_errorcode_e Close_Status = tie_OK;
    ttc_i2c_errorcode_e Error_I2C    = tie_OK;
#if 1
    if (1) {
        ttc_mutex_error_e Error_Mutex = ttc_mutex_lock(ConfigArch->I2C_HW_Lock, Config->TimeOut*10);
        if (Error_Mutex) {
            Assert_I2C(FALSE, ec_Debug);// could not get Mutex Lock within specified Timeout
            return tie_BusError;
        }

        if (Close_Status == tie_OK) {
            if (tie_OK != _i2c_stm32l1xx_wait_for_flag(Config, i2c_flag_BUSY, RESET) ) {
                Close_Status = tie_TimeOut_Flag_Busy;
            }
        }
        if (Close_Status == tie_OK) {
            if (tie_OK != _i2c_stm32l1xx_condition_start(Config)  ) {
                Close_Status = tie_TimeOut_StartCondition;
            }
        }
        if (Close_Status == tie_OK) {
            if (tie_OK != _i2c_stm32l1xx_send_target_address(Config, TargetAddress, tid_WritingToSlave) ) {
                Close_Status = tie_TimeOut_Master_ModeSelect;
            }
        }
        if (Close_Status == tie_OK) {
            if (tie_OK != _i2c_stm32l1xx_send_register_address(Config, Register, RegisterType)) {
                Close_Status = tie_TimeOut_Master_RegisterSend;
            }
        }
        switch (RegisterType) {
        case (tiras_MSB_First_8Bit):
            for(;Amount>=1;Amount--,Buffer++) {
                if (Close_Status == tie_OK && Amount) {
                    if (tie_OK != _i2c_stm32l1xx_send_byte(Config, Buffer)) {
                        Close_Status = tie_TimeOut_Master_BufferSend;
                    }
                }
            }
            break;
        case (tiras_LSB_First_8Bit) :
            Buffer+=(Amount-1);
            for(;Amount>=1;Amount--,Buffer--) {
                if (Close_Status == tie_OK && Amount) {
                    if (tie_OK != _i2c_stm32l1xx_send_byte(Config, Buffer)) {
                        Close_Status = tie_TimeOut_Master_BufferSend;
                    }
                }
            }
            break;
        default: return tie_NotImplemented;
        }
            Error_I2C = _i2c_stm32l1xx_close_bus(Config, Close_Status);
    }

#else
    //this implementation does not use hardware locks to protect the interface from concurrent access
    if (tie_OK != _i2c_stm32l1xx_wait_for_flag(Config, i2c_flag_BUSY, RESET) )                      return _i2c_stm32l1xx_close_bus(Config, tie_TimeOut_Flag_Busy);
    if (tie_OK != _i2c_stm32l1xx_condition_start(ArchConfig,(I2C_TypeDef*)I2C_Base) )               return _i2c_stm32l1xx_close_bus(Config, tie_TimeOut_StartCondition);
    if (tie_OK != _i2c_stm32l1xx_send_target_address(Config, TargetAddress, tid_WritingToSlave) )   return _i2c_stm32l1xx_close_bus(Config, tie_TimeOut_Master_ModeSelect);
    if (tie_OK != _i2c_stm32l1xx_send_register_address(Config, Register, RegisterType) )            return _i2c_stm32l1xx_close_bus(Config, tie_TimeOut_Master_RegisterSend); //D
    if (tie_OK != _i2c_stm32l1xx_send_bytes(Config, Buffer, Amount) )                               return _i2c_stm32l1xx_close_bus(Config, tie_TimeOut_Master_BufferSend);

    return _i2c_stm32l1xx_close_bus(Config, tie_OK);
#endif

    else {
        while (I2C_GetFlagStatus((I2C_TypeDef*)I2C_Base,i2c_flag_BUSY)) {
            MyTimeOut--;
            ttc_task_yield();
            if (MyTimeOut == 0)
                return tie_TimeOut_Flag_Busy;
        }
        // Intiate Start Sequence

        I2C_GenerateSTART((I2C_TypeDef*)I2C_Base, ENABLE);

        while (!I2C_CheckEvent((I2C_TypeDef*)I2C_Base,i2c_event_MASTER_MODE_SELECT)) {
            MyTimeOut--;
            ttc_task_yield();
            if (MyTimeOut == 0)
                return tie_TimeOut_Flag_Busy;
        }
        // Send Address  EV5

        I2C_Send7bitAddress((I2C_TypeDef*)I2C_Base, TargetAddress, I2C_Direction_Transmitter);

        while (!I2C_CheckEvent((I2C_TypeDef*)I2C_Base,i2c_event_MASTER_TRANSMITTER_MODE_SELECTED)) {
            MyTimeOut--;
            ttc_task_yield();
            if (MyTimeOut == 0)
                return tie_TimeOut_Flag_Busy;
        }
        // EV6

        // Write first byte EV8_1

        I2C_SendData((I2C_TypeDef*)I2C_Base, Register);

        while (--Amount) {

            // wait on BTF

            while (!I2C_CheckEvent((I2C_TypeDef*)I2C_Base,i2c_event_MASTER_BYTE_TRANSMITTED)) {
                MyTimeOut--;
                ttc_task_yield();
                if (MyTimeOut == 0)
                    return tie_TimeOut_Flag_Busy;
            }

            I2C_SendData((I2C_TypeDef*)I2C_Base, *Buffer++);
        }

        while (!I2C_CheckEvent((I2C_TypeDef*)I2C_Base,i2c_event_MASTER_BYTE_TRANSMITTED)) {
            MyTimeOut--;
            ttc_task_yield();
            if (MyTimeOut == 0)
                return tie_TimeOut_Flag_Busy;
        }

        I2C_GenerateSTOP((I2C_TypeDef*)I2C_Base, ENABLE);
    }

    ttc_mutex_unlock(ConfigArch->I2C_HW_Lock);
    return Error_I2C;
    return (ttc_i2c_errorcode_e) 0;
}
ttc_i2c_base_register_t* i2c_stm32l1xx_get_base_register(ttc_i2c_config_t* Config) {
    Assert_I2C(Config, ec_NULL); // pointers must not be NULL

    ttc_i2c_base_register_t * BaseRegister;

    switch(Config->LogicalIndex)
    {
        case 1: BaseRegister = i2c1_address;
            break;
        case 2: BaseRegister = i2c2_address;
            break;
        default: Assert_I2C(0, ec_UNKNOWN);
            break;
    }

    return (ttc_i2c_base_register_t*) BaseRegister;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

               u8_t _i2c_stm32l1xx_get_pins(u8_t I2C_Index, ttc_i2c_architecture_t* ConfigArch) {
    u8_t Layout = 0;
    ttc_gpio_pin_e PortSDA = tgp_none;

    switch (I2C_Index) { // check if board has defined the SDA-pin of I2C to init
#ifdef TTC_I2C1_SDA
    case 1:
        PortSDA = TTC_I2C1_SDA;

        break;
#endif
#ifdef TTC_I2C2_SDA
    case 2:
        PortSDA = TTC_I2C2_SDA;
        break;
#endif
    default:
        Assert_I2C(0, ec_UNKNOWN);   // No TTC_I2Cn_SDA defined! Check your makefile.100_board_* file!
        break;
    }

    switch ( (u32_t) ConfigArch->Base ) { // determine pin layout (-> UM1079 p.29)
    case (u32_t) I2C1: { // I2C1
        if (PortSDA == tgp_b7) Layout = 1;
        else                            Layout = 0;

        if (Layout) { //no remapping
            ConfigArch->PortSDA = tgp_b7;
            ConfigArch->PortSCL = tgp_b6;
            //GPIO_PinRemapConfig(GPIO_Remap_I2C1, DISABLE);
        }
        else {       // alternate pin remapping (-> ST UM1079 STM32L1 discovery kits p.29)
            ConfigArch->PortSDA = tgp_b9;
            ConfigArch->PortSCL = tgp_b8;
            //GPIO_PinRemapConfig(GPIO_Remap_I2C1, ENABLE);

            //GPIO_PinAFConfig(GPIOB, 8, GPIO_AF_I2C1);
            //GPIO_PinAFConfig(GPIOB, 9, GPIO_AF_I2C1);

            ttc_gpio_pin_AF_config(tgp_b8, gpio_AF_I2C1);
            ttc_gpio_pin_AF_config(tgp_b9, gpio_AF_I2C1);
        }
        ConfigArch->PortSMBAL = tgp_b5;

        break;
    }
    case (u32_t) I2C2: { // I2C2
        ConfigArch->PortSDA   = tgp_b11;
        ConfigArch->PortSCL   = tgp_b10;
        ConfigArch->PortSMBAL = tgp_b12;
        //no remapping avaliable
        break;
    }
    default: { return tie_DeviceNotFound; }
    }

    return Layout;
}
ttc_i2c_errorcode_e _i2c_stm32l1xx_read_bytes(ttc_i2c_config_t* Config, u8_t* Buffer, u16_t Amount, u16_t* AmountRead) {
    Assert_I2C(Config, ec_NULL);

    ttc_i2c_architecture_t* ConfigArch = Config->LowLevelConfig; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32l1xx_i2c_t* I2C_Base = ConfigArch->Base;
    Assert_I2C(I2C_Base, ec_NULL);

    ttc_i2c_errorcode_e Error;

    _i2c_stm32l1xx_condition_stop(Config);
    if (0) ttc_task_begin_criticalsection();
    while (Amount-- > 0) {
        Error = _i2c_stm32l1xx_read_byte(Config);
        if (Error) return Error;
        (*AmountRead)++;
        Buffer++;
    }
    if (0) ttc_task_end_criticalsection();

    return tie_OK;
}
               u8_t _i2c_stm32l1xx_read_byte(ttc_i2c_config_t* Config) {

    ttc_i2c_base_register_t* I2C_Base = i2c_stm32l1xx_get_base_register(Config);

    if (Config->Flags.Bits.Slave) {

        /* Enable Acknowledge */
        I2C_CR1_t CR1 = I2C_Base->CR1;
        CR1.All |= i2c_CR1_ACK;
        I2C_Base->CR1 = CR1;

        while ( !_i2c_check_event(I2C_Base, i2c_event_SLAVE_RECEIVER_ADDRESS_MATCHED) );

        while ( !_i2c_check_event(I2C_Base, i2c_event_SLAVE_BYTE_RECEIVED) );

        // read data from I2C data register and return data byte
        I2C_DR_t DR = I2C_Base->DR;
        u8_t Byte = DR.All;

        while ( !_i2c_check_event(I2C_Base, i2c_event_SLAVE_STOP_DETECTED) );

        return Byte;

    } else{

        if (I2C_STM32L1XX_USE_TIMEOUTS) { // wait until I2C1 is not busy any more
            if ( _i2c_stm32l1xx_wait_for_flag(Config, i2c_flag_BUSY, 1) )
                return 0; // Timeout while waiting for bus to free!
        }
        else {
            while (_i2c_check_flag(I2C_Base, i2c_flag_BUSY));
        }

        I2C_CR1_t CR1 = I2C_Base->CR1;
        CR1.All |= i2c_CR1_ACK;
        I2C_Base->CR1 = CR1;

        // Send I2C1 START condition
        CR1 = I2C_Base->CR1;
        CR1.All |= i2c_CR1_START;
        I2C_Base->CR1 = CR1;


        // wait for I2C1 EV5 --> Slave has acknowledged start condition
        if ( _i2c_stm32l1xx_wait_for_event(Config, i2c_event_MASTER_MODE_SELECT, 1) )
            return 0; // Timeout while waiting for event!
        //X while (!_i2c_check_event(I2C_Base, i2c_event_MASTER_MODE_SELECT));

        // Send slave Address for write
        _i2c_send7bit_Address(Config, Config->LowLevelConfig->SlaveAddress, i2c_Direction_Receiver);

        /* wait for I2Cx EV6, check if either Slave has acknowledged Master transmitter or
         * Master receiver mode, depending on the transmission
         * direction*/
        if ( _i2c_stm32l1xx_wait_for_event(Config, i2c_event_MASTER_RECEIVER_MODE_SELECTED, 1) )
            return 0; // Timeout while waiting for event!
//X         while (!_i2c_check_event(I2C_Base, i2c_event_MASTER_RECEIVER_MODE_SELECTED));

        CR1 = I2C_Base->CR1;
        CR1.All &= (u16_t)~((u16_t)i2c_CR1_ACK);
        I2C_Base->CR1 = CR1;

        // Generate Stop
        CR1 = I2C_Base->CR1;
        CR1.All |= i2c_CR1_STOP;
        I2C_Base->CR1 = CR1;

        // wait until one byte has been received
        if ( _i2c_stm32l1xx_wait_for_event(Config, i2c_event_MASTER_BYTE_RECEIVED, 1) )
            return 0; // Timeout while waiting for event!
        //X while ( !_i2c_check_event(I2C_Base, i2c_event_MASTER_BYTE_RECEIVED) );

        // read data from I2C data register and return data byte
        return (u8_t) I2C_Base->DR.All;
    }
}
ttc_i2c_errorcode_e _i2c_stm32l1xx_send_register_address(ttc_i2c_config_t* Config, u32_t Register, ttc_i2c_register_type_e RegisterType) {
    Assert_I2C(Config, ec_NULL);

    ttc_i2c_architecture_t* ConfigArch = Config->LowLevelConfig; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32l1xx_i2c_t* I2C_Base = ConfigArch->Base;
    Assert_I2C(I2C_Base, ec_NULL);

    u8_t AmountBytes = 0;
    u8_t Buffer[4];

    switch (RegisterType) { // send register adress (MSB first)

    case tiras_MSB_First_32Bit:
        Buffer[0] = (u8_t) (Register & 0xff000000) >> 24;
        Buffer[1] = (u8_t) (Register & 0x00ff0000) >> 16;
        Buffer[2] = (u8_t) (Register & 0x0000ff00) >> 8;
        Buffer[3] = (u8_t) Register & 0x000000ff;
        AmountBytes = 4;
        break;
    case tiras_MSB_First_24Bit:
        Buffer[0] = (u8_t) (Register & 0x00ff0000) >> 16;
        Buffer[1] = (u8_t) (Register & 0x0000ff00) >> 8;
        Buffer[2] = (u8_t) Register & 0x000000ff;
        AmountBytes = 3;
        break;

    case tiras_MSB_First_16Bit:
        Buffer[0] = (u8_t) (Register & 0x0000ff00) >> 8;
        Buffer[1] = (u8_t) Register & 0x000000ff;
        AmountBytes = 2;
        break;

    case tiras_MSB_First_8Bit:
        Buffer[0] = (u8_t) Register & 0x000000ff;
        AmountBytes = 1;
        break;

    case tiras_LSB_First_32Bit:
        Buffer[0] = (u8_t) Register & 0x000000ff;
        Buffer[1] = (u8_t) (Register & 0x0000ff00) >> 8;
        Buffer[2] = (u8_t) (Register & 0x00ff0000) >> 16;
        Buffer[3] = (u8_t) (Register & 0xff000000) >> 24;
        AmountBytes = 4;
        break;
    case tiras_LSB_First_24Bit:
        Buffer[0] = (u8_t) Register & 0x000000ff;
        Buffer[1] = (u8_t) (Register & 0x0000ff00) >> 8;
        Buffer[2] = (u8_t) (Register & 0x00ff0000) >> 16;
        AmountBytes = 3;
        break;

    case tiras_LSB_First_16Bit:
        Buffer[0] = (u8_t) Register & 0x000000ff;
        Buffer[1] = (u8_t) (Register & 0x0000ff00) >> 8;
        AmountBytes = 2;
        break;

    case tiras_LSB_First_8Bit:
        Buffer[0] = (u8_t) Register & 0x000000ff;
        AmountBytes = 1;
        break;

    default: Assert_I2C(0, ec_InvalidArgument); // unknown register size
    }

    return _i2c_stm32l1xx_send_bytes(Config, Buffer, AmountBytes);
}
ttc_i2c_errorcode_e _i2c_stm32l1xx_send_bytes(ttc_i2c_config_t* Config, const u8_t* Buffer, u16_t Amount) {
    Assert_I2C(Config, ec_NULL);

    ttc_i2c_architecture_t* ConfigArch = Config->LowLevelConfig; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32l1xx_i2c_t* I2C_Base = ConfigArch->Base;
    Assert_I2C(I2C_Base, ec_NULL);

    ttc_i2c_errorcode_e Error;

    while (Amount-- > 0) {
        Error = _i2c_stm32l1xx_send_byte(Config, Buffer);
        if (Error)
            return Error;
    }

    return tie_OK;
}
ttc_i2c_errorcode_e _i2c_stm32l1xx_send_byte(ttc_i2c_config_t* Config, const u8_t* Byte) {
    Assert_I2C(Config, ec_NULL);

    ttc_i2c_base_register_t* I2C_Base = i2c_stm32l1xx_get_base_register(Config);

    if (Config->Flags.Bits.Master) {

        // wait until I2C is not busy any more
        while (_i2c_check_flag(I2C_Base, i2c_flag_BUSY));

        // Send I2C START condition
        I2C_CR1_t CR1 = I2C_Base->CR1;
        CR1.All |= i2c_CR1_START;
        I2C_Base->CR1 = CR1;

        // wait for I2C EV5 --> Slave has acknowledged start condition
        while (!_i2c_check_event(I2C_Base, i2c_event_MASTER_MODE_SELECT));

        // Send slave Address for write
        _i2c_send7bit_Address(Config, Config->LowLevelConfig->SlaveAddress, i2c_Direction_Transmitter);

        /* wait for I2Cx EV6, check if either Slave has acknowledged Master transmitter or
         * Master receiver mode, depending on the transmission
         * direction*/

        while (!_i2c_check_event(I2C_Base, i2c_event_MASTER_TRANSMITTER_MODE_SELECTED));

        // wait for I2C EV8 --> last byte is still being transmitted (last byte in SR, buffer empty), next byte can already be written
        while (!_i2c_check_event(I2C_Base, i2c_event_MASTER_BYTE_TRANSMITTING));

        // Send data
        I2C_DR_t DR = I2C_Base->DR;
        DR.All = (u32_t) Byte;
        I2C_Base->DR = DR;

        // wait for I2C EV8_2 --> byte has been transmitted
        while (!_i2c_check_event(I2C_Base, i2c_event_MASTER_BYTE_TRANSMITTED));

        // Send I2C STOP Condition after last byte has been transmitted
        CR1 = I2C_Base->CR1;
        CR1.All |= i2c_CR1_STOP;
        I2C_Base->CR1 = CR1;


    } else {

        I2C_CR1_t CR1 = I2C_Base->CR1;
        CR1.All |= i2c_CR1_ACK;
        I2C_Base->CR1 = CR1;

        /* wait for I2Cx EV6, check if either Slave has acknowledged Master transmitter or
         * Master receiver mode, depending on the transmission direction*/
        while (!_i2c_check_event(I2C_Base, i2c_event_SLAVE_TRANSMITTER_ADDRESS_MATCHED));

        // Send data
        I2C_DR_t DR = I2C_Base->DR;
        DR.All = (u32_t) Byte;
        I2C_Base->DR = DR;


        while (!_i2c_check_event(I2C_Base, i2c_event_SLAVE_ACK_FAILURE));
    }

    return 0;

}
ttc_i2c_errorcode_e _i2c_stm32l1xx_close_bus(ttc_i2c_config_t* Config, ttc_i2c_errorcode_e Status) {
    Assert_I2C(Config, ec_NULL);
    ttc_i2c_architecture_t* ConfigArch = Config->LowLevelConfig; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32l1xx_i2c_t* I2C_Base = ConfigArch->Base;
    Assert_I2C(I2C_Base, ec_NULL);

    if (0) {
        I2C_SR1_t SR1 = I2C_Base->SR1;
        if (SR1.Bits.TIMEOUT)  Status = tie_TimeOut;
        if (SR1.Bits.BERR)     Status = tie_BusError;
        if (SR1.Bits.ARLO)     Status = tie_Master_ArbitrationLost;
        if (SR1.Bits.AF)       Status = tie_AcknowledgeFailure;
        if (SR1.Bits.PECERR)   Status = tie_PEC_Error;
        if (SR1.Bits.SMBALERT) Status = tie_SMBusAlert;
    }

    if (Status == tie_OK) {
        // Generate Stop
        I2C_CR1_t CR1 = I2C_Base->CR1;
        CR1.All |= i2c_CR1_STOP;
        I2C_Base->CR1 = CR1;
    }
    else { // error-status: send out zero bits to reset all slaves
        _i2c_stm32l1xx_flush_slave(Config);
        _i2c_stm32l1xx_reinit_i2c(Config);
        //Assert_I2C(FALSE, ec_Debug);//D
        if (0) { // try to reset slaves
            // _i2c_stm32f1xx_condition_start(ArchConfig,I2C_Base);

            // send out zero byte to flush slaves shift register
            u8_t Zero = 0;

            _i2c_stm32l1xx_send_bytes(Config, &Zero, 1);
            // _i2c_stm32f1xx_condition_stop(Config);
        }
        Config->ErrorCount++;
        Assert_I2C(Config->ErrorCount < 50, ec_DeviceNotFound);
    }

    // Read from SR1 and SR2 register to clear all flags
    (void) I2C_Base->SR1;
    (void) I2C_Base->SR2;

    return Status;
}
ttc_i2c_errorcode_e _i2c_stm32l1xx_flush_slave(ttc_i2c_config_t* Config) {
    Assert_I2C(Config, ec_NULL);

    ttc_i2c_architecture_t* ConfigArch = Config->LowLevelConfig; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32l1xx_i2c_t* I2C_Base = ConfigArch->Base;
    Assert_I2C(I2C_Base, ec_NULL);

    if (ConfigArch->PortSDA)
        ttc_gpio_init(ConfigArch->PortSCL, tgm_output_push_pull, tgs_50MHz);

    for (int q = 0; q < 32; q++) {
        ttc_gpio_set(ConfigArch->PortSCL);
        uSleep(10);
        ttc_gpio_clr(ConfigArch->PortSCL);
        uSleep(10);
    }

    return tie_OK;
}
ttc_i2c_errorcode_e _i2c_stm32l1xx_reinit_i2c(ttc_i2c_config_t* Config) {
    Assert_I2C(Config, ec_NULL);

    ttc_i2c_architecture_t* ConfigArch = Config->LowLevelConfig; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32l1xx_i2c_t* I2C_Base = ConfigArch->Base;
    Assert_I2C(I2C_Base, ec_NULL);

    if (1) { // deconfigure GPIOs

        if (ConfigArch->PortSDA)
            ttc_gpio_init(ConfigArch->PortSDA,    tgm_input_floating, tgs_Max);

        if (ConfigArch->PortSCL)
            ttc_gpio_init(ConfigArch->PortSCL,    tgm_input_floating, tgs_Max);

        if (ConfigArch->PortSMBAL && Config->Flags.Bits.SMBus_Alert)
            ttc_gpio_init(ConfigArch->PortSMBAL,  tgm_input_floating,  tgs_Max);
    }
    I2C_Base->CR1.Bits.SWRST = 1;
    _i2c_de_init(Config);
    if (1) { // activate clocks
        switch ( (u32_t) I2C_Base ) {
        case (u32_t) I2C1: {
            sysclock_stm32l1xx_RCC_APB1PeriphClockCmd(rcc_APB1Periph_I2C1, ENABLE);
            if (Config->Layout) { // GPIOB + GPIOA
                sysclock_stm32l1xx_RCC_AHBPeriphClockCmd(rcc_AHBPeriph_GPIOA | rcc_AHBPeriph_GPIOB, ENABLE);
            }
            else {                     // GPIOB
                sysclock_stm32l1xx_RCC_AHBPeriphClockCmd(rcc_AHBPeriph_GPIOA | rcc_AHBPeriph_GPIOB, ENABLE);
            }
            break;
        }
        case (u32_t) I2C2: {
            sysclock_stm32l1xx_RCC_APB1PeriphClockCmd(rcc_APB1Periph_I2C2, ENABLE);
            sysclock_stm32l1xx_RCC_AHBPeriphClockCmd(rcc_AHBPeriph_GPIOB, ENABLE);
            break;
        }
        default: Assert_I2C(0, ec_UNKNOWN); break; // should never occur!
        }
    }
    if (1) { // reset I2C device
        switch ( (u32_t) I2C_Base ) {
        case (u32_t) I2C1: {
        default:
                sysclock_stm32l1xx_RCC_APB1PeriphResetCmd(rcc_APB1Periph_I2C1, ENABLE);
                sysclock_stm32l1xx_RCC_APB1PeriphResetCmd(rcc_APB1Periph_I2C1, DISABLE);
                break;
            }
        case (u32_t) I2C2: {
            sysclock_stm32l1xx_RCC_APB1PeriphResetCmd(rcc_APB1Periph_I2C2, ENABLE);
            sysclock_stm32l1xx_RCC_APB1PeriphResetCmd(rcc_APB1Periph_I2C2, DISABLE);
            break;
        }
        }

    }
    if (1) { // apply remap layout

        switch ( (u32_t) I2C_Base ) {
        case (u32_t) I2C1: { // I2C1
            if (Config->Layout) {
                ttc_gpio_init(ConfigArch->PortSCL,    tgm_alternate_function_open_drain, tgs_50MHz);
                ttc_gpio_pin_AF_config(tgp_b6, gpio_AF_I2C1);

                ttc_gpio_init(ConfigArch->PortSDA,    tgm_alternate_function_open_drain, tgs_50MHz);
                ttc_gpio_pin_AF_config(tgp_b7, gpio_AF_I2C1);
            }
            else{
                ttc_gpio_init(ConfigArch->PortSCL,    tgm_alternate_function_open_drain, tgs_50MHz);
                ttc_gpio_pin_AF_config(tgp_b8, gpio_AF_I2C1);

                ttc_gpio_init(ConfigArch->PortSDA,    tgm_alternate_function_open_drain, tgs_50MHz);
                ttc_gpio_pin_AF_config(tgp_b9, gpio_AF_I2C1);
            }
            break;
        }
        case (u32_t) I2C2: { // I2C2
            ttc_gpio_init(ConfigArch->PortSCL,    tgm_alternate_function_open_drain, tgs_50MHz);
            ttc_gpio_pin_AF_config(tgp_b10, gpio_AF_I2C2);

            ttc_gpio_init(ConfigArch->PortSDA,    tgm_alternate_function_open_drain, tgs_50MHz);
            ttc_gpio_pin_AF_config(tgp_b11, gpio_AF_I2C2);
        }
        default: { break; }
        }
    }
    if (1) { // configure GPIOs

        if (0) { // test pins
            ttc_gpio_init(ConfigArch->PortSDA, tgm_output_open_drain, tgs_50MHz);
            ttc_gpio_init(ConfigArch->PortSCL, tgm_output_open_drain, tgs_50MHz);
            while (1) {
                // ttc_gpio_set(ConfigArch->PortSDA);
                ttc_gpio_set(ConfigArch->PortSCL);
                ttc_gpio_set(PIN_PB8);
                ttc_gpio_set(PIN_PB9);
                uSleep(10);

                ttc_gpio_clr(PIN_PB8);
                ttc_gpio_clr(PIN_PB9);
                uSleep(10);
            }
            while (1) {

                ttc_gpio_set(ConfigArch->PortSDA);
                ttc_gpio_set(ConfigArch->PortSCL);
                uSleep(10);
                ttc_gpio_clr(ConfigArch->PortSCL);
                uSleep(10);
                ttc_gpio_clr(ConfigArch->PortSDA);
                ttc_gpio_set(ConfigArch->PortSCL);
                uSleep(10);
                ttc_gpio_clr(ConfigArch->PortSCL);
                uSleep(10);
            }
        }

        if (ConfigArch->PortSDA)
            ttc_gpio_init(ConfigArch->PortSDA, tgm_alternate_function_open_drain, tgs_50MHz);

        if (ConfigArch->PortSCL)
            ttc_gpio_init(ConfigArch->PortSCL, tgm_alternate_function_open_drain, tgs_50MHz);

        if (ConfigArch->PortSMBAL && Config->Flags.Bits.SMBus_Alert)
            ttc_gpio_init(ConfigArch->PortSMBAL,  tgm_input_pull_up,  tgs_50MHz);
    }

    _i2c_stm32l1xx_cmd(Config, ENABLE);

    if (1) { // init I2C after enabling it

        i2c_init_structure i2c_myinit;

        ttc_memory_set( &i2c_myinit, 0, sizeof(i2c_init_structure) );

        i2c_myinit.i2c_OwnAddress1 = Config->OwnAddress;
        i2c_myinit.i2c_ClockSpeed = Config->ClockSpeed;

        if (Config->Flags.Bits.SMBus) {
            if (Config->Flags.Bits.SMBus_Host) {
                 i2c_myinit.i2c_Mode = i2c_Mode_SMBusHost;
            }
            else{
                i2c_myinit.i2c_Mode = i2c_Mode_SMBusDevice;
            }
        }
        else{
             i2c_myinit.i2c_Mode = i2c_Mode_I2C;
        }

        if (Config->Flags.Bits.Acknowledgement) {
            i2c_myinit.i2c_Ack= i2c_Ack_Enable;
        }
        else{
            i2c_myinit.i2c_Ack= i2c_Ack_Disable;
        }

        if (Config->Flags.Bits.Adress_10Bit) {
            i2c_myinit.i2c_AcknowledgedAddress = i2c_AcknowledgedAddress_10bit;
        }
        else{
            i2c_myinit.i2c_AcknowledgedAddress = i2c_AcknowledgedAddress_7bit;
        }

        if (Config->Flags.Bits.DutyCycle_16_9) {
            i2c_myinit.i2c_DutyCycle = i2c_DutyCycle_16_9;
        }
        else{
            i2c_myinit.i2c_DutyCycle = i2c_DutyCycle_2;
        }

        _i2c_stm32l1xx_init(Config, &i2c_myinit);

        // do stuff that was not done by standard_peripherals_library
        I2C_Base->CCR.Bits.Fast = Config->Flags.Bits.ModeFast;
    }

    return tie_OK;
}
ttc_i2c_errorcode_e _i2c_stm32l1xx_condition_start(ttc_i2c_config_t *Config) {
    Assert_I2C(Config, ec_NULL);

    ttc_i2c_architecture_t* ConfigArch = Config->LowLevelConfig;
    Assert_I2C(ConfigArch, ec_NULL);

    register_stm32l1xx_i2c_t* Base = ConfigArch->Base;
    Assert_I2C(Base, ec_NULL);

    Base->CR1.Bits.START = 1;
    u16_t MyTimeOut = Config->TimeOut;
    while ( (MyTimeOut > 0) && Base->CR1.Bits.START) {
        MyTimeOut--;
        ttc_task_yield();
    }

    if (MyTimeOut > 0)  return tie_OK;

    return tie_TimeOut_StartCondition;
}
ttc_i2c_errorcode_e _i2c_stm32l1xx_condition_stop(ttc_i2c_config_t* Config) {
    Assert_I2C(Config, ec_NULL);

    ttc_i2c_architecture_t* ConfigArch = Config->LowLevelConfig;
    Assert_I2C(ConfigArch, ec_NULL);

    register_stm32l1xx_i2c_t* I2C_Base = ConfigArch->Base;
    Assert_I2C(I2C_Base, ec_NULL);

    I2C_Base->CR1.Bits.STOP = 1;
    u16_t MyTimeOut = Config->TimeOut;
    while ( (MyTimeOut > 0) && I2C_Base->CR1.Bits.STOP) {
        MyTimeOut--;
        ttc_task_yield();
    }

    if (MyTimeOut > 0)  return tie_OK;

    return tie_TimeOut_StopCondition;
}
ttc_i2c_errorcode_e _i2c_stm32l1xx_send_target_address(ttc_i2c_config_t* Config, u16_t TargetAddress, ttc_i2c_direction_e Direction) {
    Assert_I2C(Config, ec_NULL);

    ttc_i2c_architecture_t* ConfigArch = Config->LowLevelConfig;
    Assert_I2C(ConfigArch, ec_NULL);

    register_stm32l1xx_i2c_t* I2C_Base = ConfigArch->Base;
    Assert_I2C(I2C_Base, ec_NULL);

    ttc_i2c_Address_7Bit_u Data;
    if (Direction == tid_WritingToSlave)
        Data.Fields.ReadMode = 0;               // writing to slave
    else
        Data.Fields.ReadMode = 1;               // reading from slave

    Data.Fields.Address = TargetAddress;
    (void) I2C_Base->SR1;
    *( (u8_t*) &(I2C_Base->DR) ) = Data.Byte;
    u16_t MyTimeOut = Config->TimeOut;
    while ( (MyTimeOut-- > 0) && ((!_i2c_check_event(I2C_Base, i2c_event_MASTER_TRANSMITTER_MODE_SELECTED))))
        ttc_task_yield();
    if (MyTimeOut == 0)
        return tie_TimeOut_Master_TransmitterMode;

    return tie_OK;
}
               void _i2c_stm32l1xx_dma_init(ttc_i2c_config_t* Config, dma_init_structure* dma_init, u8_t rx_tx) {

    u32_t tmpreg = 0;

    //Set configuration of DMA RX
    if (rx_tx == 1) {

        // CCR Configuration
        tmpreg = Config->LowLevelConfig->Dma_baseRx->CCR;
        tmpreg &= ccr_CLEAR_MASK;

        tmpreg |= dma_init->dma_DIR | dma_init->dma_Mode |
                dma_init->dma_PeripheralInc | dma_init->dma_MemoryInc |
                dma_init->dma_PeripheralDataSize | dma_init->dma_MemoryDataSize|
                dma_init->dma_Priority | dma_init->dma_M2M;

        Config->LowLevelConfig->Dma_baseRx->CCR = tmpreg;

        // CNDTR Configuration
        tmpreg = Config->LowLevelConfig->Dma_baseRx->CNDTR;
        tmpreg = dma_init->dma_BufferSize;
        Config->LowLevelConfig->Dma_baseRx->CNDTR = tmpreg;


        // CPAR Configuration
        tmpreg = Config->LowLevelConfig->Dma_baseRx->CPAR;
        tmpreg = dma_init->dma_PeripheralBaseAddr;
        Config->LowLevelConfig->Dma_baseRx->CPAR = tmpreg;


        // CMAR Configuration
        tmpreg = Config->LowLevelConfig->Dma_baseRx->CMAR;
        tmpreg = dma_init->dma_MemoryBaseAddr;
        Config->LowLevelConfig->Dma_baseRx->CMAR = tmpreg;

    }
    else{

        // CCR Configuration
        tmpreg = Config->LowLevelConfig->Dma_baseTx->CCR;
        tmpreg &= ccr_CLEAR_MASK;

        tmpreg |= dma_init->dma_DIR | dma_init->dma_Mode |
                dma_init->dma_PeripheralInc | dma_init->dma_MemoryInc |
                dma_init->dma_PeripheralDataSize | dma_init->dma_MemoryDataSize|
                dma_init->dma_Priority | dma_init->dma_M2M;

        Config->LowLevelConfig->Dma_baseTx->CCR = tmpreg;

        // CNDTR Configuration
        tmpreg = Config->LowLevelConfig->Dma_baseTx->CNDTR;
        tmpreg = dma_init->dma_BufferSize;
        Config->LowLevelConfig->Dma_baseTx->CNDTR = tmpreg;


        // CPAR Configuration
        tmpreg = Config->LowLevelConfig->Dma_baseTx->CPAR;
        tmpreg = dma_init->dma_PeripheralBaseAddr;
        Config->LowLevelConfig->Dma_baseTx->CPAR = tmpreg;


        // CMAR Configuration
        tmpreg = Config->LowLevelConfig->Dma_baseTx->CMAR;
        tmpreg = dma_init->dma_MemoryBaseAddr;
        Config->LowLevelConfig->Dma_baseTx->CMAR = tmpreg;

    }
}
               void _i2c_stm32l1xx_dma_cmd(ttc_i2c_config_t* Config, FunctionalState NewState, u8_t rx_tx) {

    u32_t tmpreg;

    if (rx_tx == 1) {
        if (NewState != DISABLE)
        {
          /* Enable the selected DMAy Channelx */
          tmpreg = Config->LowLevelConfig->Dma_baseRx->CCR;
          tmpreg |= dma_CCR1_EN;
          Config->LowLevelConfig->Dma_baseRx->CCR = tmpreg;
        }
        else
        {
          /* Disable the selected DMAy Channelx */
          tmpreg = Config->LowLevelConfig->Dma_baseRx->CCR;
          tmpreg &= (u16_t)(~dma_CCR1_EN);
          Config->LowLevelConfig->Dma_baseRx->CCR = tmpreg;
        }
    }
    else{
        if (NewState != DISABLE)
        {
          /* Enable the selected DMAy Channelx */
          tmpreg = Config->LowLevelConfig->Dma_baseTx->CCR;
          tmpreg |= dma_CCR1_EN;
          Config->LowLevelConfig->Dma_baseTx->CCR = tmpreg;
        }
        else
        {
          /* Disable the selected DMAy Channelx */
          tmpreg = Config->LowLevelConfig->Dma_baseTx->CCR;
          tmpreg &= (u16_t)(~dma_CCR1_EN);
          Config->LowLevelConfig->Dma_baseTx->CCR = tmpreg;
        }

    }

}
               void _i2c_stm32l1xx_init(ttc_i2c_config_t* Config, i2c_init_structure* i2c_init) {
    ttc_i2c_base_register_t* I2C_Base = i2c_stm32l1xx_get_base_register(Config);

    u16_t tmpreg = 0, freqrange = 0;
    u16_t result = 0x04;
    u32_t pclk1 = 8000000;
    RCC_ClocksTypeDef  rcc_clocks;


  /*---------------------------- I2Cx CR2 Configuration ------------------------*/
    /* Get the I2Cx CR2 value */
    I2C_CR2_t CR2 = I2C_Base->CR2;

    /* Clear frequency FREQ[5:0] bits */
    CR2.All &= (u16_t)~((u16_t)i2c_CR2_FREQ);

    /* Get pclk1 frequency value */
    RCC_GetClocksFreq(&rcc_clocks);
    pclk1 = rcc_clocks.PCLK1_Frequency;

    /* Set frequency bits depending on pclk1 value */
    freqrange = (u16_t)(pclk1 / 1000000);
    CR2.All |= freqrange;

    /* Write to I2Cx CR2 */
    I2C_Base->CR2 = CR2;


  /*---------------------------- I2Cx CCR Configuration ------------------------*/
    /* Disable the selected I2C peripheral to configure TRISE */
    I2C_CR1_t CR1 = I2C_Base->CR1;
    CR1.All &= (u16_t)~((u16_t)i2c_CR1_PE);
    I2C_Base->CR1 = CR1;

    /* Reset tmpreg value */
    /* Clear F/S, DUTY and CCR[11:0] bits */
    tmpreg = 0;

    /* Configure speed in standard mode */
    if (i2c_init->i2c_ClockSpeed <= 100000)
    {
      /* Standard mode speed calculate */
      result = (u16_t)(pclk1 / (i2c_init->i2c_ClockSpeed << 1));
      /* Test if CCR value is under 0x4*/
      if (result < 0x04)
      {
        /* Set minimum allowed value */
        result = 0x04;
      }
      /* Set speed value for standard mode */
      tmpreg |= result;

      /* Set Maximum Rise Time for standard mode */
      I2C_TRISE_t TRISE = I2C_Base->TRISE;
      TRISE.All = freqrange + 1;
      I2C_Base->TRISE = TRISE;
    }
    /* Configure speed in fast mode */
    /* To use the I2C at 400 KHz (in fast mode), the PCLK1 frequency (I2C peripheral
       input clock) must be a multiple of 10 MHz */
    else
    {
      if (i2c_init->i2c_DutyCycle == i2c_DutyCycle_2)
      {
        /* Fast mode speed calculate: Tlow/Thigh = 2 */
        result = (u16_t)(pclk1 / (i2c_init->i2c_ClockSpeed * 3));
      }
      else
      {
        /* Fast mode speed calculate: Tlow/Thigh = 16/9 */
        result = (u16_t)(pclk1 / (i2c_init->i2c_ClockSpeed * 25));
        /* Set DUTY bit */
        result |= i2c_DutyCycle_16_9;
      }

      /* Test if CCR value is under 0x1*/
      if ((result & i2c_CCR_CCR) == 0)
      {
        /* Set minimum allowed value */
        result |= (u16_t)0x0001;
      }

      /* Set speed value and set F/S bit for fast mode */
      tmpreg |= (u16_t)(result | i2c_CCR_FS);

      /* Set Maximum Rise Time for fast mode */
      I2C_TRISE_t TRISE = I2C_Base->TRISE;
      TRISE.All = (u16_t)(((freqrange * (u16_t)300) / (u16_t)1000) + (u16_t)1);
      I2C_Base->TRISE = TRISE;
    }

    /* Write to I2Cx CCR */
    I2C_CCR_t CCR = I2C_Base->CCR;
    CCR.All = tmpreg;
    I2C_Base->CCR = CCR;

    /* Enable the selected I2C peripheral */
    CR1 = I2C_Base->CR1;
    CR1.All |= i2c_CR1_PE;
    I2C_Base->CR1 = CR1;

  /*---------------------------- I2Cx CR1 Configuration ------------------------*/
    /* Get the I2Cx CR1 value */
    CR1 = I2C_Base->CR1;

    /* Clear ACK, SMBTYPE and  SMBUS bits */
    CR1.All &= cr1_CLEAR_MASK;

    /* Configure I2Cx: mode and acknowledgement */
    /* Set SMBTYPE and SMBUS bits according to I2C_Mode value */
    /* Set ACK bit according to I2C_Ack value */
    CR1.All |= (u16_t)((u32_t)i2c_init->i2c_Mode | i2c_init->i2c_Ack);

    /* Write to I2Cx CR1 */
    I2C_Base->CR1 = CR1;

  /*---------------------------- I2Cx OAR1 Configuration -----------------------*/
    /* Set I2Cx Own Address1 and acknowledged address */
    I2C_OAR1_t OAR1 = I2C_Base->OAR1;
    OAR1.All = (i2c_init->i2c_AcknowledgedAddress | i2c_init->i2c_OwnAddress1);
    I2C_Base->OAR1 = OAR1;
}
               void _i2c_stm32l1xx_cmd(ttc_i2c_config_t* Config, FunctionalState NewState) {
    ttc_i2c_base_register_t* I2C_Base = i2c_stm32l1xx_get_base_register(Config);

    /* Check the parameters */
    assert_param(IS_I2C_ALL_PERIPH(I2C_Base));
    assert_param(IS_FUNCTIONAL_STATE(NewState));

    I2C_CR1_t CR1 = I2C_Base->CR1;

    if (NewState != DISABLE)
    {
      /* Enable the selected I2C peripheral */
      CR1.All |= i2c_CR1_PE;
    }
    else
    {
      /* Disable the selected I2C peripheral */
      CR1.All &= (u16_t)~((u16_t)i2c_CR1_PE);
    }

    I2C_Base->CR1 = CR1;
}
               void _i2c_send7bit_Address(ttc_i2c_config_t* Config, uint8_t Address, uint8_t Direction)
{
    ttc_i2c_base_register_t* I2C_Base = i2c_stm32l1xx_get_base_register(Config);

    /* Test on the direction to set/reset the read/write bit */
    if (Direction != i2c_Direction_Transmitter)
    {
        /* Set the address bit0 for read */
        Address |= i2c_OAR1_ADD0;
    }
    else
    {
        /* Reset the address bit0 for write */
        Address &= (u8_t)~((u8_t)i2c_OAR1_ADD0);
    }

    I2C_Base->DR.All = Address;
}
               void _i2c_de_init(ttc_i2c_config_t* Config) {

    if (Config->LogicalIndex == 1) // BUG: Must be PhysicalIndex!
    {
      /* Enable I2C1 reset state */
      sysclock_stm32l1xx_RCC_APB1PeriphResetCmd(rcc_APB1Periph_I2C1, ENABLE);
      /* Release I2C1 from reset state */
      sysclock_stm32l1xx_RCC_APB1PeriphResetCmd(rcc_APB1Periph_I2C1, DISABLE);
    }
    else
    {
      /* Enable I2C2 reset state */
      sysclock_stm32l1xx_RCC_APB1PeriphResetCmd(rcc_APB1Periph_I2C2, ENABLE);
      /* Release I2C2 from reset state */
      sysclock_stm32l1xx_RCC_APB1PeriphResetCmd(rcc_APB1Periph_I2C2, DISABLE);
    }
}
               BOOL _i2c_check_event(ttc_i2c_base_register_t* I2C_Base, i2c_stm32l1xx_event_e Event) {

    u32_t lastevent = 0;
    u32_t flag1;
    u32_t flag2;
    u8_t status = 0;

    /* Check the parameters */
    assert_param(IS_I2C_ALL_PERIPH(I2C_Base));
    assert_param(IS_I2C_EVENT(Event));

    /* Read the I2Cx status register */
    flag1 = I2C_Base->SR1.All;
    flag2 = I2C_Base->SR2.All;
    flag2 = flag2 << 16;

    /* Get the last event value from I2C status register */
    lastevent = (flag1 | flag2) & flag_MASK;

    /* Check whether the last event contains the I2C_EVENT */
    if ((lastevent & Event) == Event)
    {
      /* SUCCESS: last event is equal to I2C_EVENT */
      status = 1;
    }
    else
    {
      /* ERROR: last event is different from I2C_EVENT */
      status = 0;
    }
    /* Return status */
    return status;

}
               BOOL _i2c_check_flag(register_stm32l1xx_i2c_t* I2Cx, i2c_stm32l1xx_flag_e I2C_FLAG)
{
  u8_t bitstatus = 0;
  u32_t i2creg = 0, i2cxbase = 0;

  /* Check the parameters */
  assert_param(IS_I2C_ALL_PERIPH(I2Cx));
  assert_param(IS_I2C_GET_FLAG(I2C_FLAG));

  /* Get the I2Cx peripheral base address */
  i2cxbase = (u32_t)I2Cx;

  /* Read flag register index */
  i2creg = I2C_FLAG >> 28;

  /* Get bit[23:0] of the flag */
  I2C_FLAG &= flag_MASK;

  if (i2creg != 0)
  {
    /* Get the I2Cx SR1 register address */
    i2cxbase += 0x14;
  }
  else
  {
    /* Flag in I2Cx SR2 Register */
    I2C_FLAG = (u32_t)(I2C_FLAG >> 16);
    /* Get the I2Cx SR2 register address */
    i2cxbase += 0x18;
  }

  if (((*(u32_t *)i2cxbase) & I2C_FLAG) != (u32_t)0)
  {
    /* I2C_FLAG is set */
    bitstatus = 1;
  }
  else
  {
    /* I2C_FLAG is reset */
    bitstatus = 0;
  }

  /* Return the I2C_FLAG status */
  return  bitstatus;
}
ttc_i2c_errorcode_e _i2c_stm32l1xx_wait_for_flag(ttc_i2c_config_t* Config, i2c_stm32l1xx_flag_e Flag, BOOL WaitForState) {
    ttc_i2c_base_register_t* I2C_Base = i2c_stm32l1xx_get_base_register(Config);

    u16_t MyTimeOut = Config->TimeOut;

    if (MyTimeOut) { // wait not longer than TimeOut
        if (WaitForState) { // waiting for flag to activate
            while ( _i2c_check_flag(I2C_Base, Flag) && (MyTimeOut > 0) ) {
                MyTimeOut--;
                ttc_task_yield();
            }
        }
        else {             // // waiting for flag to deactivate
            while ( (! _i2c_check_flag(I2C_Base, Flag)) && (MyTimeOut > 0) ) {
                MyTimeOut--;
                ttc_task_yield();
            }
        }
        if (MyTimeOut == 0)
            return tie_TimeOut;
    }
    else {          // wait endless
        if (WaitForState) { // waiting for flag to activate
            while ( _i2c_check_flag(I2C_Base, Flag) ) {
                ttc_task_yield();
            }
        }
        else {             // // waiting for flag to deactivate
            while (! _i2c_check_flag(I2C_Base, Flag) ) {
                ttc_task_yield();
            }
        }
    }

    return tie_OK;
}
ttc_i2c_errorcode_e _i2c_stm32l1xx_wait_for_event(ttc_i2c_config_t* Config, i2c_stm32l1xx_event_e Event, BOOL WaitForState) {
    ttc_i2c_base_register_t* I2C_Base = i2c_stm32l1xx_get_base_register(Config);

    u16_t MyTimeOut = Config->TimeOut;

    if (MyTimeOut) { // wait not longer than TimeOut
        if (WaitForState) { // waiting for event to activate
            while ( _i2c_check_event(I2C_Base, Event) && (MyTimeOut > 0) ) {
                MyTimeOut--;
                ttc_task_yield();
            }
        }
        else {             // // waiting for event to deactivate
            while ( (! _i2c_check_event(I2C_Base, Event)) && (MyTimeOut > 0) ) {
                MyTimeOut--;
                ttc_task_yield();
            }
        }
        if (MyTimeOut == 0)
            return tie_TimeOut;
    }
    else {           // wait endless
        if (WaitForState) { // waiting for flag to activate
            while ( _i2c_check_event(I2C_Base, Event) ) {
                ttc_task_yield();
            }
        }
        else {             // // waiting for flag to deactivate
            while (! _i2c_check_event(I2C_Base, Event) ) {
                ttc_task_yield();
            }
        }
    }

    return tie_OK;

}

//} private functions
               /*?used?
                                void _i2c_stm32l1xx_dma_de_init(ttc_i2c_config_t* Config, u8_t rx_tx) {

                   u32_t tmpreg;
                   register_stm32l1xx_dma_t* dma1;

                   dma1 = (register_stm32l1xx_dma_t*)0x40206000;

                   // Disable the selected DMAy Channelx
                   tmpreg = Config->LowLevelConfig->Dma_baseRx->CCR;
                   tmpreg &= (u16_t)(~dma_CCR1_EN);
                   Config->LowLevelConfig->Dma_baseRx->CCR = tmpreg;

                   // Reset DMAy Channelx control register
                   Config->LowLevelConfig->Dma_baseRx->CCR = 0;

                   // Reset DMAy Channelx remaining bytes register
                   Config->LowLevelConfig->Dma_baseRx->CNDTR = 0;

                   // Reset DMAy Channelx peripheral address register
                   Config->LowLevelConfig->Dma_baseRx->CPAR = 0;

                   // Reset DMAy Channelx memory address register
                   Config->LowLevelConfig->Dma_baseRx->CMAR = 0;

                   if (Config->LowLevelConfig->Dma_baseRx == (register_stm32l1xx_dma_channel_t *)dma1_Channel1)
                   {
                     // Reset interrupt pending bits for DMA1 Channel1
                     dma1->IFCR |= ((u32_t)(dma_ISR_GIF1 | dma_ISR_TCIF1 | dma_ISR_HTIF1 | dma_ISR_TEIF1));
                   }
                   else if (Config->LowLevelConfig->Dma_baseRx == (register_stm32l1xx_dma_channel_t *)dma1_Channel2)
                   {
                     // Reset interrupt pending bits for DMA1 Channel2
                     dma1->IFCR |= ((u32_t)(dma_ISR_GIF2 | dma_ISR_TCIF2 | dma_ISR_HTIF2 | dma_ISR_TEIF2));
                   }
                   else if (Config->LowLevelConfig->Dma_baseRx == (register_stm32l1xx_dma_channel_t *)dma1_Channel3)
                   {
                     // Reset interrupt pending bits for DMA1 Channel3
                     dma1->IFCR |= ((u32_t)(dma_ISR_GIF3 | dma_ISR_TCIF3 | dma_ISR_HTIF3 | dma_ISR_TEIF3));
                   }
                   else if (Config->LowLevelConfig->Dma_baseRx == (register_stm32l1xx_dma_channel_t *)dma1_Channel4)
                   {
                     // Reset interrupt pending bits for DMA1 Channel4
                     dma1->IFCR |= ((u32_t)(dma_ISR_GIF4 | dma_ISR_TCIF4 | dma_ISR_HTIF4 | dma_ISR_TEIF4));
                   }
                   else if (Config->LowLevelConfig->Dma_baseRx == (register_stm32l1xx_dma_channel_t *)dma1_Channel5)
                   {
                     // Reset interrupt pending bits for DMA1 Channel5
                     dma1->IFCR |= ((u32_t)(dma_ISR_GIF5 | dma_ISR_TCIF5 | dma_ISR_HTIF5 | dma_ISR_TEIF5));
                   }
                   else if (Config->LowLevelConfig->Dma_baseRx == (register_stm32l1xx_dma_channel_t *) dma1_Channel6)
                   {
                     // Reset interrupt pending bits for DMA1 Channel6
                     dma1->IFCR |= ((u32_t)(dma_ISR_GIF6 | dma_ISR_TCIF6 | dma_ISR_HTIF6 | dma_ISR_TEIF6));
                   }
                   else
                   {
                     if (Config->LowLevelConfig->Dma_baseRx ==(register_stm32l1xx_dma_channel_t *) dma1_Channel7)
                     {
                       // Reset interrupt pending bits for DMA1 Channel7
                       dma1->IFCR |= ((u32_t)(dma_ISR_GIF7 | dma_ISR_TCIF7 | dma_ISR_HTIF7 | dma_ISR_TEIF7));
                     }
                   }

               }
               */
