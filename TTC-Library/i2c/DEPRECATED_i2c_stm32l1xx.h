#ifndef I2C_STM32L1XX_H
#define I2C_STM32L1XX_H

/** { i2c_stm32l1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  Low-Level driver for i2c devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by high-level i2c and application.
 *
 *  Created from template device_architecture.h revision 22 at 20141007 15:32:58 UTC
 *
 *  Note: See ttc_i2c.h for description of stm32l1xx independent I2C implementation.
 *
 *  Authors: <AUTHOR>
 *
}*/
//{ Defines/ TypeDefs ****************************************************
#define i2c1_address                    ((register_stm32l1xx_i2c_t*) 0x40005400)
#define i2c2_address                    ((register_stm32l1xx_i2c_t*) 0x40005800)

#define USE_OWN_I2C_CODE 1

#define gpio_AF_I2C1                    ((u8_t)0x04)  /*!< I2C1 Alternate Function mapping */
#define gpio_AF_I2C2                    ((u8_t)0x04)  /*!< I2C2 Alternate Function mapping */

#define dma_DIR_PeripheralDST           ((u32_t)0x00000010)
#define dma_DIR_PeripheralSRC           ((u32_t)0x00000000)

#define dma_PeripheralInc_Enable        ((u32_t)0x00000040)
#define dma_PeripheralInc_Disable       ((u32_t)0x00000000)

#define dma_MemoryInc_Enable            ((u32_t)0x00000080)
#define dma_MemoryInc_Disable           ((u32_t)0x00000000)

#define dma_PeripheralDataSize_Byte        ((u32_t)0x00000000)
#define dma_PeripheralDataSize_HalfWord    ((u32_t)0x00000100)
#define dma_PeripheralDataSize_Word        ((u32_t)0x00000200)

#define dma_MemoryDataSize_Byte            ((u32_t)0x00000000)
#define dma_MemoryDataSize_HalfWord        ((u32_t)0x00000400)
#define dma_MemoryDataSize_Word            ((u32_t)0x00000800)

#define dma_Mode_Circular                  ((u32_t)0x00000020)
#define dma_Mode_Normal                    ((u32_t)0x00000000)

#define dma_Priority_VeryHigh              ((u32_t)0x00003000)
#define dma_Priority_High                  ((u32_t)0x00002000)
#define dma_Priority_Medium                ((u32_t)0x00001000)
#define dma_Priority_Low                   ((u32_t)0x00000000)

#define dma_M2M_Enable                     ((u32_t)0x00004000)
#define dma_M2M_Disable                    ((u32_t)0x00000000)

#define ccr_CLEAR_MASK                     ((u32_t)0xFFFF800F)

#define dma_CCR1_EN                       ((u16_t)0x0001)

#define  dma_ISR_GIF1                        ((u32_t)0x00000001)        /*!< Channel 1 Global interrupt flag */
#define  dma_ISR_TCIF1                       ((u32_t)0x00000002)        /*!< Channel 1 Transfer Complete flag */
#define  dma_ISR_HTIF1                       ((u32_t)0x00000004)        /*!< Channel 1 Half Transfer flag */
#define  dma_ISR_TEIF1                       ((u32_t)0x00000008)        /*!< Channel 1 Transfer Error flag */
#define  dma_ISR_GIF2                        ((u32_t)0x00000010)        /*!< Channel 2 Global interrupt flag */
#define  dma_ISR_TCIF2                       ((u32_t)0x00000020)        /*!< Channel 2 Transfer Complete flag */
#define  dma_ISR_HTIF2                       ((u32_t)0x00000040)        /*!< Channel 2 Half Transfer flag */
#define  dma_ISR_TEIF2                       ((u32_t)0x00000080)        /*!< Channel 2 Transfer Error flag */
#define  dma_ISR_GIF3                        ((u32_t)0x00000100)        /*!< Channel 3 Global interrupt flag */
#define  dma_ISR_TCIF3                       ((u32_t)0x00000200)        /*!< Channel 3 Transfer Complete flag */
#define  dma_ISR_HTIF3                       ((u32_t)0x00000400)        /*!< Channel 3 Half Transfer flag */
#define  dma_ISR_TEIF3                       ((u32_t)0x00000800)        /*!< Channel 3 Transfer Error flag */
#define  dma_ISR_GIF4                        ((u32_t)0x00001000)        /*!< Channel 4 Global interrupt flag */
#define  dma_ISR_TCIF4                       ((u32_t)0x00002000)        /*!< Channel 4 Transfer Complete flag */
#define  dma_ISR_HTIF4                       ((u32_t)0x00004000)        /*!< Channel 4 Half Transfer flag */
#define  dma_ISR_TEIF4                       ((u32_t)0x00008000)        /*!< Channel 4 Transfer Error flag */
#define  dma_ISR_GIF5                        ((u32_t)0x00010000)        /*!< Channel 5 Global interrupt flag */
#define  dma_ISR_TCIF5                       ((u32_t)0x00020000)        /*!< Channel 5 Transfer Complete flag */
#define  dma_ISR_HTIF5                       ((u32_t)0x00040000)        /*!< Channel 5 Half Transfer flag */
#define  dma_ISR_TEIF5                       ((u32_t)0x00080000)        /*!< Channel 5 Transfer Error flag */
#define  dma_ISR_GIF6                        ((u32_t)0x00100000)        /*!< Channel 6 Global interrupt flag */
#define  dma_ISR_TCIF6                       ((u32_t)0x00200000)        /*!< Channel 6 Transfer Complete flag */
#define  dma_ISR_HTIF6                       ((u32_t)0x00400000)        /*!< Channel 6 Half Transfer flag */
#define  dma_ISR_TEIF6                       ((u32_t)0x00800000)        /*!< Channel 6 Transfer Error flag */
#define  dma_ISR_GIF7                        ((u32_t)0x01000000)        /*!< Channel 7 Global interrupt flag */
#define  dma_ISR_TCIF7                       ((u32_t)0x02000000)        /*!< Channel 7 Transfer Complete flag */
#define  dma_ISR_HTIF7                       ((u32_t)0x04000000)        /*!< Channel 7 Half Transfer flag */
#define  dma_ISR_TEIF7                       ((u32_t)0x08000000)        /*!< Channel 7 Transfer Error flag */

#define  dma_CCR1_EN                         ((u16_t)0x0001)            /*!< Channel enable*/

#define i2c_Mode_I2C                         ((u16_t)0x0000)
#define i2c_Mode_SMBusDevice                 ((u16_t)0x0002)
#define i2c_Mode_SMBusHost                   ((u16_t)0x000A)

#define i2c_DutyCycle_16_9                   ((u16_t)0x4000) /*!< I2C fast mode Tlow/Thigh = 16/9 */
#define i2c_DutyCycle_2                      ((u16_t)0xBFFF) /*!< I2C fast mode Tlow/Thigh = 2 */

#define i2c_Ack_Enable                       ((u16_t)0x0400)
#define i2c_Ack_Disable                      ((u16_t)0x0000)

#define i2c_AcknowledgedAddress_7bit         ((u16_t)0x4000)
#define i2c_AcknowledgedAddress_10bit        ((u16_t)0xC000)

#define i2c_CR2_FREQ                         ((u16_t)0x003F)            /*!< FREQ[5:0] bits (Peripheral Clock Frequency) */

#define i2c_CR1_PE                           ((u16_t)0x0001)            /*!< Peripheral Enable */

#define i2c_CCR_CCR                          ((u16_t)0x0FFF)            /*!< Clock Control Register in Fast/Standard mode (Master mode) */
#define i2c_CCR_DUTY                         ((u16_t)0x4000)            /*!< Fast Mode Duty Cycle */
#define i2c_CCR_FS                           ((u16_t)0x8000)            /*!< I2C Master Mode Selection */

#define cr1_CLEAR_MASK                       ((u16_t)0xFBF5)      /*<! I2C registers Masks */
#define flag_MASK                            ((u32_t)0x00FFFFFF)  /*<! I2C FLAG mask */
#define iten_MASK                            ((u32_t)0x07000000)  /*<! I2C Interrupt Enable mask */

#define dma1_Channel1                        ((u32_t)0x40026008)
#define dma1_Channel2                        ((u32_t)0x4002601C)
#define dma1_Channel3                        ((u32_t)0x40026030)
#define dma1_Channel4                        ((u32_t)0x40026044)
#define dma1_Channel5                        ((u32_t)0x40026058)
#define dma1_Channel6                        ((u32_t)0x4002606C)
#define dma1_Channel7                        ((u32_t)0x40026080)

#define i2c_CR1_START                        ((u16_t)0x0100)            /*!< Start Generation */


#define i2c_OAR1_ADD0                        ((u16_t)0x0001)            /*!< Bit 0 */

#define i2c_Direction_Transmitter            ((u8_t)0x00)
#define i2c_Direction_Receiver               ((u8_t)0x01)


#define  rcc_APB1Periph_I2C1                 ((u32_t)0x00200000)
#define  rcc_APB1Periph_I2C2                 ((u32_t)0x00400000)
#define  rcc_AHBPeriph_DMA1                  ((u32_t)0x01000000)
#define  rcc_AHBPeriph_GPIOB                 ((u32_t)0x00000002)
#define  rcc_AHBPeriph_GPIOA                 ((u32_t)0x00000001)


#define  i2c_CR1_ACK                         ((u16_t)0x0400)            /*!< Acknowledge Enable */
#define  i2c_CR1_STOP                        ((u16_t)0x0200)            /*!< Stop Generation */

/* DEPRECATED (moved into enums in i2c_stm32l1xx_types.h)

#define i2c_event_MASTER_BYTE_TRANSMITTING                 ((u32_t)0x00070080) // TRA, BUSY, MSL, TXE flags

#define  i2c_event_SLAVE_RECEIVER_ADDRESS_MATCHED          ((u32_t)0x00020002) // BUSY and ADDR flags
#define  i2c_event_SLAVE_TRANSMITTER_ADDRESS_MATCHED       ((u32_t)0x00060082) // TRA, BUSY, TXE and ADDR flags
#define  i2c_event_SLAVE_RECEIVER_SECONDADDRESS_MATCHED    ((u32_t)0x00820000)  // DUALF and BUSY flags
#define  i2c_event_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED ((u32_t)0x00860080)  // DUALF, TRA, BUSY and TXE flags
#define  i2c_event_SLAVE_GENERALCALLADDRESS_MATCHED        ((u32_t)0x00120000)  // GENCALL and BUSY flags

#define  i2c_event_SLAVE_BYTE_RECEIVED                     ((u32_t)0x00020040)  // BUSY and RXNE flags
#define  i2c_event_SLAVE_STOP_DETECTED                     ((u32_t)0x00000010)  // STOPF flag
#define  i2c_event_SLAVE_BYTE_TRANSMITTED                  ((u32_t)0x00060084)  // TRA, BUSY, TXE and BTF flags
#define  i2c_event_SLAVE_BYTE_TRANSMITTING                 ((u32_t)0x00060080)  // TRA, BUSY and TXE flags
#define  i2c_event_SLAVE_ACK_FAILURE                       ((u32_t)0x00000400)  // AF flag

#define i2c_flag_DUALF                  ((u32_t)0x00800000)
#define i2c_flag_SMBHOST                ((u32_t)0x00400000)
#define i2c_flag_SMBDEFAULT             ((u32_t)0x00200000)
#define i2c_flag_GENCALL                ((u32_t)0x00100000)
#define i2c_flag_TRA                    ((u32_t)0x00040000)
#define i2c_flag_BUSY                   ((u32_t)0x00020000)
#define i2c_flag_MSL                    ((u32_t)0x00010000)
*/



//typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;
//#define IS_FUNCTIONAL_STATE(STATE) (((STATE) == DISABLE) || ((STATE) == ENABLE))






//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_I2C_STM32L1XX
//
// Implementation status of low-level driver support for i2c devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_I2C_STM32L1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_I2C_STM32L1XX == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_I2C_STM32L1XX to one from '-', 'o', '+' 
#endif

//}EXTENSION_STATUS_TTC_I2C_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "DEPRECATED_i2c_stm32l1xx.c"
//
#include "DEPRECATED_i2c_stm32l1xx_types.h"
#include "../ttc_i2c.h"
#include "../ttc_i2c_types.h"
#include "../ttc_task.h"
#include "../ttc_gpio.h"
#include "../ttc_memory.h"
#include "../register/register_stm32l1xx.h"

//#define USE_STDPERIPH_DRIVER

#include "stm32l1xx.h"
//#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
//#include "stm32l1xx_conf.h"
//#include "stm32l1xx_i2c.h"
//#include "stm32l1xx_rcc.h"
//#include "stm32l1xx_gpio.h"
//#include "stm32l1xx_dma.h"
//#include "misc.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_i2c_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_i2c_foo
//
#define ttc_driver_i2c_deinit(Config) i2c_stm32l1xx_deinit(Config)
#define ttc_driver_i2c_get_features(Config) i2c_stm32l1xx_get_features(Config)
#define ttc_driver_i2c_init(Config) i2c_stm32l1xx_init(Config)
#define ttc_driver_i2c_load_defaults(Config) i2c_stm32l1xx_load_defaults(Config)
#define ttc_driver_i2c_prepare() i2c_stm32l1xx_prepare()
#define ttc_driver_i2c_read_register(I2C_Index, TargetAddress, Register, RegisterType, Buffer) i2c_stm32l1xx_read_register(I2C_Index, TargetAddress, Register, RegisterType, Buffer)
#define ttc_driver_i2c_read_registers(I2C_Index, TargetAddress, Register, RegisterType, Buffer, Amount) i2c_stm32l1xx_read_registers(I2C_Index, TargetAddress, Register, RegisterType, Buffer, Amount)
#define ttc_driver_i2c_reset(Config) i2c_stm32l1xx_reset(Config)
#define ttc_driver_i2c_write_register(I2C_Index, TargetAddress, Register, RegisterType, Byte) i2c_stm32l1xx_write_register(I2C_Index, TargetAddress, Register, RegisterType, Byte)
#define ttc_driver_i2c_write_registers(I2C_Index, TargetAddress, Register, RegisterType, Buffer, Amount) i2c_stm32l1xx_write_registers(I2C_Index, TargetAddress, Register, RegisterType, Buffer, Amount)
#define ttc_driver_i2c_get_base_register(Config) i2c_stm32l1xx_get_base_register(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_i2c.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_i2c.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** shutdown single I2C unit device
 * @param Config        pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return              == 0: I2C has been shutdown successfully; != 0: error-code
 */
ttc_i2c_errorcode_e i2c_stm32l1xx_deinit(ttc_i2c_config_t* Config);


/** fills out given Config with maximum valid values for indexed I2C
 * @param Config        = pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
ttc_i2c_errorcode_e i2c_stm32l1xx_get_features(ttc_i2c_config_t* Config);


/** initializes single I2C unit for operation
 * @param Config        pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return              == 0: I2C has been initialized successfully; != 0: error-code
 */
ttc_i2c_errorcode_e i2c_stm32l1xx_init(ttc_i2c_config_t* Config);


/** loads configuration of indexed I2C unit with default values
 * @param Config        pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
ttc_i2c_errorcode_e i2c_stm32l1xx_load_defaults(ttc_i2c_config_t* Config);


/** Prepares i2c Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void i2c_stm32l1xx_prepare();


/** Reads amount of data bytes from addressed target device.
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param I2C_Index      device index of I2C to init (1..ttc_i2c_get_max_index() )
 * @param TargetAddress  receivers own TargetAddress
 * @param Register       index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Byte           pointer to start of 8 bit buffer where to store received bytes
 * @param Amount         amount of bytes to be read from target device
 * @param AmountRead     will be loaded with amount of bytes being read
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: one or more bytes have been read successfully; != 0: error-code
 * @param Buffer   =
 */
ttc_i2c_errorcode_e i2c_stm32l1xx_read_register(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, u8_t* Buffer);


/** Reads amount of data bytes from addressed target device.
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param I2C_Index      device index of I2C to init (1..ttc_i2c_get_max_index() )
 * @param TargetAddress  receivers own TargetAddress
 * @param Register       index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Byte           pointer to start of 8 bit buffer where to store received bytes
 * @param Amount         amount of bytes to be read from target device
 * @param AmountRead     will be loaded with amount of bytes being read
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: one or more bytes have been read successfully; != 0: error-code
 * @param Buffer   =
 */
ttc_i2c_errorcode_e i2c_stm32l1xx_read_registers(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, u8_t* Buffer, u16_t Amount);


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return   =
 */
ttc_i2c_errorcode_e i2c_stm32l1xx_reset(ttc_i2c_config_t* Config);


/** Send out given amount of bytes to addressed target device.
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param I2C_Index    device index of I2C to init (1..ttc_i2c_get_max_index() )
 * @param TargetAddress      receivers own TargetAddress
 * @param Register     index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Buffer       memory location from which to read data
 * @param Amount       amount of bytes to send from Buffer[]
 * @return             == 0: Buffer has been sent successfully; != 0: error-code
 * @param Byte   =
 */
ttc_i2c_errorcode_e i2c_stm32l1xx_write_register(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, const char Byte);


/** Send out given amount of bytes to addressed target device.
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param I2C_Index    device index of I2C to init (1..ttc_i2c_get_max_index() )
 * @param TargetAddress      receivers own TargetAddress
 * @param Register     index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Buffer       memory location from which to read data
 * @param Amount       amount of bytes to send from Buffer[]
 * @return             == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_i2c_errorcode_e i2c_stm32l1xx_write_registers(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, const u8_t* Buffer, u16_t Amount);

/** returns base address of low-level registers used to controll indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_I2C_get_max_LogicalIndex())
 * @param Config        = pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return                configuration of indexed device
 */
ttc_i2c_base_register_t* i2c_stm32l1xx_get_base_register(ttc_i2c_config_t* Config);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _i2c_stm32f1xx_foo(ttc_i2c_config_t* Config)
/** load pin configuration into given structure
 * @return: index of pin layout being determined
 */
u8_t _i2c_stm32l1xx_get_pins(u8_t I2C_Index, ttc_i2c_architecture_t* I2C_Arch);

/** Wait until Byte has been received or timeout occured.
 * Note: This low-level function should not be called from outside!
 *
 * @param I2C_Arch       entry from i2c_stm32f1xx_Configs[] of preconfigured I2C unit to use
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: byte has been received within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32l1xx_wait_for_RXNE(ttc_i2c_config_t* Config);

/** Waits until given I2C flag is active.
 *
 * Note: This low-level function should not be called from outside!
 *
 * @param Config          filled out struct ttc_i2c_config_t from i2c_stm32f1xx_Configs[]
 * @param Flag            bitmask of flags to check
 * @param WaitForState    flag state to wait for (==0: wait for flag to become active; !=0: wait for flag to become inactive)
 * @param Config->TimeOut  > 0: max loop iterations to wait; ==0: no timeout
 * @return:               == 0: flag set/reset within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32l1xx_wait_for_flag(ttc_i2c_config_t* Config, i2c_stm32l1xx_flag_e Flag, BOOL WaitForState);

/** Generic wait for I2C event.
 *
 * Note: This low-level function should not be called from outside!
 *
 * @param Config          filled out struct ttc_i2c_config_t from i2c_stm32f1xx_Configs[]
 * @param Flag            bitmask of flags to check
 * @param WaitForState    flag state to wait for (==0: wait for flag to become active; !=0: wait for flag to become inactive)
 * @param Config->TimeOut  > 0: max loop iterations to wait; ==0: no timeout
 * @return:               == 0: event occured within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32l1xx_wait_for_event(ttc_i2c_config_t* Config, i2c_stm32l1xx_event_e Event, BOOL WaitForState);

/** Reads in given amount of bytes from slave (low level read; does not generate Start- or Stop-event)
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from i2c_stm32f1xx_Configs[]
 * @param Buffer         memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[]
 * @param AmountRead     increased on every byte being received successfully
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: all bytes successfully read  within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32l1xx_read_bytes(ttc_i2c_config_t* Config, u8_t* Buffer, u16_t Amount, u16_t* AmountRead);

/** Reads in single byte from slave (low level receive; does not generate Start- or Stop-event)
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from i2c_stm32f1xx_Configs[]
 * @param Buffer         8-bit storage for byte to read
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: byte successfully read within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32l1xx_read_byte(ttc_i2c_config_t* Config);

/** Sends out register address to slave device
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Register       index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @return:              == 0: register sent successfully within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32l1xx_send_register_address(ttc_i2c_config_t* Config, u32_t Register, ttc_i2c_register_type_e RegisterType);

/** Sends out given amount of bytes (low level send; does not generate Start- or Stop-event)
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Buffer         memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[]
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: event occured within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32l1xx_send_bytes(ttc_i2c_config_t* Config, const u8_t* Buffer, u16_t Amount);

/** Sends out single byte (low level send; does not generate Start- or Stop-event)
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Byte           8-bit value to send out
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: event occured within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32l1xx_send_byte(ttc_i2c_config_t* Config, const u8_t *Byte);

/** closes the I2C bus and brings it back to a defined state.
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Status         simply passed as return value
 * @return               Status
 */
ttc_i2c_errorcode_e _DEPRECATED_i2c_stm32l1xx.close_bus(ttc_i2c_config_t* Config, ttc_i2c_errorcode_e Status);
/** triese to push and pull to SCL line to flush the slave internal state machine and bring it back to normal operation
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @return               Status
 */
ttc_i2c_errorcode_e _i2c_stm32l1xx_flush_slave(ttc_i2c_config_t* Config);
/** initialize/ reinitialize an I2C unit
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @return               Status
 */
ttc_i2c_errorcode_e _i2c_stm32l1xx_reinit_i2c(ttc_i2c_config_t* Config);

/** generate start condition
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return               Status
 */
ttc_i2c_errorcode_e _DEPRECATED_i2c_stm32l1xx.condition_start(ttc_i2c_config_t* Config);
/** generate start condition
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return               Status
 */
ttc_i2c_errorcode_e _DEPRECATED_i2c_stm32l1xx.condition_stop(ttc_i2c_config_t* Config);

/** send target address to slave
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param TargetAddress  TargetAddress of device to read from
 * @param Direction      =1: next data will be read from slave; =0: next data will be written to slave
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return               Status
 */
ttc_i2c_errorcode_e _i2c_stm32l1xx_send_target_address(ttc_i2c_config_t* Config, u16_t TargetAddress, ttc_i2c_direction_e Direction);

//InsertPrivatePrototypes above (DO NOT DELETE THIS LINE!)

//}private functions

#endif //I2C_STM32L1XX_H
