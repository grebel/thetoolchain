/** i2c_common.c ***********************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to i2c low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.c revision 12 at 20150531 21:41:38 UTC
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * Put all includes here that are required to compile this source code.
 */

#include "i2c_common.h"
#include "../ttc_task.h"
#include "../ttc_gpio.h"
#include "../ttc_i2c.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables

extern t_base ttc_driver_i2c_get_event_value( volatile t_ttc_i2c_base_register* BaseRegister );

/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)
//}PrivateFunctions
/** Function Definitions ********************************************{
 */

t_u16 i2c_common_delay() {

    const t_u16 DelayUS = 100;
    ttc_task_usleep( DelayUS );
    return DelayUS;
}
e_ttc_i2c_event_code i2c_common_check_event( t_base EventValue, e_ttc_i2c_event_code EventCode ) {

    Assert_I2C_EXTRA( EventCode, ttc_assert_origin_auto ); // got no event code!

    if ( EventCode & 0x80000000 ) { // MSB set
        EventCode = EventCode & ~ 0x80000000;
        if ( ( EventValue & EventCode ) != EventCode ) // negated check
        { return EventCode; }
    }
    else {
        if ( ( EventValue & EventCode ) == EventCode ) // normal check
        { return EventCode; }
    }
    return E_ttc_i2c_event_code_None;
}
e_ttc_i2c_event_code i2c_common_wait_until_event( e_ttc_i2c_event_code EventCode,
                                                  volatile t_ttc_i2c_base_register* BaseRegister,
                                                  t_u32 TimeOut,
                                                  t_u16 AmountRetries
                                                ) {

    Assert_I2C_EXTRA( EventCode, ttc_assert_origin_auto ); // got no event code!
    Assert_I2C_EXTRA( ttc_memory_is_writable( BaseRegister ), ttc_assert_origin_auto );

    do {
        t_u16 Retries = AmountRetries;
        BOOL UnlimitedRetries = ( ++AmountRetries == 0 ); AmountRetries--; // AmountRetries ==-1 does not work

        while ( Retries ) { // try to obtain state in polling mode can be faster than sleeping in between
            t_u32 EventValue = ttc_driver_i2c_get_event_value( BaseRegister );

            if ( EventCode & 0x80000000 ) { // event negated
                if ( ! i2c_common_check_event( EventValue, EventCode & ~0x80000000 ) ) {
                    return EventCode; // negated event occured
                }
            }
            else {
                if ( i2c_common_check_event( EventValue, EventCode ) ) {
                    return EventCode; // event occured
                }
            }

            if ( !UnlimitedRetries )
            { Retries--; }
        };

        t_u32 TimePassed = i2c_common_delay();
        if ( TimeOut > TimePassed )
        { TimeOut -= TimePassed; }
        else
        { TimeOut = 0; }

    }
    while ( TimeOut );

    return E_ttc_i2c_event_code_None; // none of given events occured before timeout
}
e_ttc_i2c_event_code i2c_common_wait_until_events( const e_ttc_i2c_event_code EventCodes[],
                                                   volatile t_ttc_i2c_base_register* BaseRegister,
                                                   t_u32 TimeOut,
                                                   t_u16 AmountRetries
                                                 ) {

    Assert_I2C_EXTRA( ttc_memory_is_writable( BaseRegister ), ttc_assert_origin_auto );
    Assert_I2C_EXTRA( ttc_memory_is_readable( EventCodes ),  ttc_assert_origin_auto );
    Assert_I2C_EXTRA( *EventCodes, ttc_assert_origin_auto ); // got no event code!

    do {

        t_u16 Retries = AmountRetries;
        BOOL UnlimitedRetries = ( ++AmountRetries == 0 ); AmountRetries--; // AmountRetries ==-1 does not work

        while ( Retries ) { // try to obtain state in polling mode can be faster than sleeping in between
            t_u32 EventValue = ttc_driver_i2c_get_event_value( BaseRegister );

            const e_ttc_i2c_event_code* CurrentEventCode = EventCodes;
            while ( *CurrentEventCode ) { // test event value against all event codes
                e_ttc_i2c_event_code EventCode = *CurrentEventCode++;

                if ( EventCode & 0x80000000 ) { // event negated
                    if ( ! i2c_common_check_event( EventValue, EventCode & ~0x80000000 ) ) {
                        return EventCode; // negated event occured
                    }
                }
                else {
                    if ( i2c_common_check_event( EventValue, EventCode ) )
                    { return EventCode; } // event occured: return its array index
                }
            }

            if ( !UnlimitedRetries )
            { Retries--; }
        };

        t_u32 TimePassed = i2c_common_delay();
        if ( TimeOut > TimePassed )
        { TimeOut -= TimePassed; }
        else
        { TimeOut = 0; }

    }
    while ( TimeOut );

    return E_ttc_i2c_event_code_None; // none of given events occured before timeout
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
