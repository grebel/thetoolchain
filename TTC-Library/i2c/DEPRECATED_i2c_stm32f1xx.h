#ifndef I2C_STM32F1XX_H
#define I2C_STM32F1XX_H

/** { i2c_stm32f1xx.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  Low-Level driver for i2c devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by high-level i2c and application.
 *
 *  Created from template device_architecture.h revision 22 at 20140424 05:06:47 UTC
 *
 *  Note: See ttc_i2c.h for description of stm32f1xx independent I2C implementation.
 *  
 *  Authors: Sascha Poggemann, Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************
#define USE_OWN_I2C_CODE 1
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_I2C_STM32F1XX
//
// Implementation status of low-level driver support for i2c devices on stm32f1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_I2C_STM32F1XX '?'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_I2C_STM32F1XX == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_I2C_STM32F1XX to one from '-', 'o', '+' 
#endif

//}EXTENSION_STATUS_TTC_I2C_STM32F1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "i2c_stm32f1xx_types.h"
#include "../ttc_i2c.h"
#include "../ttc_i2c_types.h"
#include "../ttc_task.h"
#include "../ttc_gpio.h"
#include "../ttc_memory.h"
#include "../register/register_stm32f1xx.h"

#define USE_STDPERIPH_DRIVER
#include "stm32f10x.h"
#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
#include "stm32f10x_conf.h"
#include "stm32f10x_i2c.h"
#include "misc.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_i2c_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_i2c_foo
//
#define ttc_driver_i2c_deinit(Config) i2c_stm32f1xx_deinit(Config)
#define ttc_driver_i2c_get_base_register(Config) i2c_stm32f1xx_get_base_register(Config)
#define ttc_driver_i2c_get_features(Config) i2c_stm32f1xx_get_features(Config)
#define ttc_driver_i2c_init(Config) i2c_stm32f1xx_init(Config)
#define ttc_driver_i2c_load_defaults(Config) i2c_stm32f1xx_load_defaults(Config)
#define ttc_driver_i2c_prepare() i2c_stm32f1xx_prepare()
#define ttc_driver_i2c_reset(Config) i2c_stm32f1xx_reset(Config)
#define ttc_driver_i2c_read_register(I2C_Index, TargetAddress, Register, RegisterType, Buffer) i2c_stm32f1xx_read_register(I2C_Index, TargetAddress, Register, RegisterType, Buffer)
#define ttc_driver_i2c_read_registers(I2C_Index, TargetAddress, Register, RegisterType, Buffer, Amount) i2c_stm32f1xx_read_registers(I2C_Index, TargetAddress, Register, RegisterType, Buffer, Amount)
#define ttc_driver_i2c_write_register(I2C_Index, TargetAddress, Register, RegisterType, Byte) i2c_stm32f1xx_write_register(I2C_Index, TargetAddress, Register, RegisterType, Byte)
#define ttc_driver_i2c_write_registers(I2C_Index, TargetAddress, Register, RegisterType, Buffer, Amount) i2c_stm32f1xx_write_registers(I2C_Index, TargetAddress, Register, RegisterType, Buffer, Amount)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_i2c.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_i2c.h for details.
 *
 * You normally should not need to add functions here. 
 * Add your private functions in the private section below. 
 */


/** shutdown single I2C unit device
 * @param Config        pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return              == 0: I2C has been shutdown successfully; != 0: error-code
 */
ttc_i2c_errorcode_e i2c_stm32f1xx_deinit(ttc_i2c_config_t* Config);


/** fills out given Config with maximum valid values for indexed I2C
 * @param Config        = pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
ttc_i2c_errorcode_e i2c_stm32f1xx_get_features(ttc_i2c_config_t* Config);


/** initializes single I2C unit for operation
 * @param Config        pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return              == 0: I2C has been initialized successfully; != 0: error-code
 */
ttc_i2c_errorcode_e i2c_stm32f1xx_init(ttc_i2c_config_t* Config);


/** loads configuration of indexed I2C unit with default values
 * @param Config        pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
ttc_i2c_errorcode_e i2c_stm32f1xx_load_defaults(ttc_i2c_config_t* Config);


/** Prepares i2c Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void i2c_stm32f1xx_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct ttc_i2c_config_t (must have valid value for PhysicalIndex)
 * @return   = 
 */
ttc_i2c_errorcode_e i2c_stm32f1xx_reset(ttc_i2c_config_t* Config);


/** Reads amount of data bytes from addressed target device.
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param I2C_Index      device index of I2C to init (1..ttc_i2c_get_max_index() )
 * @param TargetAddress  receivers own TargetAddress
 * @param Register       index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Byte           pointer to start of 8 bit buffer where to store received bytes
 * @param Amount         amount of bytes to be read from target device
 * @param AmountRead     will be loaded with amount of bytes being read
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: one or more bytes have been read successfully; != 0: error-code
 * @param Buffer   = 
 */
ttc_i2c_errorcode_e i2c_stm32f1xx_read_register(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, u8_t* Buffer);


/** Reads amount of data bytes from addressed target device.
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param I2C_Index      device index of I2C to init (1..ttc_i2c_get_max_index() )
 * @param TargetAddress  receivers own TargetAddress
 * @param Register       index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Byte           pointer to start of 8 bit buffer where to store received bytes
 * @param Amount         amount of bytes to be read from target device
 * @param AmountRead     will be loaded with amount of bytes being read
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: one or more bytes have been read successfully; != 0: error-code
 * @param Buffer   = 
 */
ttc_i2c_errorcode_e i2c_stm32f1xx_read_registers(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, u8_t* Buffer, u16_t Amount);


/** Send out given amount of bytes to addressed target device.
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param I2C_Index    device index of I2C to init (1..ttc_i2c_get_max_index() )
 * @param TargetAddress      receivers own TargetAddress
 * @param Register     index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Buffer       memory location from which to read data
 * @param Amount       amount of bytes to send from Buffer[]
 * @return             == 0: Buffer has been sent successfully; != 0: error-code
 * @param Byte   = 
 */
ttc_i2c_errorcode_e i2c_stm32f1xx_write_register(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, const char Byte);


/** Send out given amount of bytes to addressed target device.
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param I2C_Index    device index of I2C to init (1..ttc_i2c_get_max_index() )
 * @param TargetAddress      receivers own TargetAddress
 * @param Register     index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Buffer       memory location from which to read data
 * @param Amount       amount of bytes to send from Buffer[]
 * @return             == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_i2c_errorcode_e i2c_stm32f1xx_write_registers(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, const u8_t* Buffer, u16_t Amount);


/** returns base address of low-level registers used to controll indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_I2C_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
register_stm32f1xx_i2c_t* i2c_stm32f1xx_get_base_register(ttc_i2c_config_t* Config);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _i2c_stm32f1xx_foo(ttc_i2c_config_t* Config)
/** load pin configuration into given structure
 * @return: index of pin layout being determined
 */
u8_t _i2c_stm32f1xx_get_pins(u8_t I2C_Index, ttc_i2c_architecture_t* I2C_Arch);

/** Wait until Byte has been received or timeout occured.
 * Note: This low-level function should not be called from outside!
 *
 * @param I2C_Arch       entry from i2c_stm32f1xx_Configs[] of preconfigured I2C unit to use
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: byte has been received within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32f1xx_wait_for_RXNE(ttc_i2c_config_t* Config);

/** Generic wait for I2C flag.
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from i2c_stm32f1xx_Configs[]
 * @param Flag           bitmask of flags to check
 * @param WaitForState   flag state to wait for
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: flag set/reset within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32f1xx_wait_for_flag(ttc_i2c_config_t* Config, u32_t Flag, FlagStatus WaitForState);

/** Generic wait for I2C event.
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from i2c_stm32f1xx_Configs[]
 * @param Flag           bitmask of flags to check
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: event occured within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32f1xx_wait_for_event(ttc_i2c_config_t* Config, u32_t Event);

/** Reads in given amount of bytes from slave (low level read; does not generate Start- or Stop-event)
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from i2c_stm32f1xx_Configs[]
 * @param Buffer         memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[]
 * @param AmountRead     increased on every byte being received successfully
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: all bytes successfully read  within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32f1xx_read_bytes(ttc_i2c_config_t* Config, u8_t* Buffer, u16_t Amount, u16_t* AmountRead);

/** Reads in single byte from slave (low level receive; does not generate Start- or Stop-event)
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from i2c_stm32f1xx_Configs[]
 * @param Buffer         8-bit storage for byte to read
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: byte successfully read within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32f1xx_read_byte(ttc_i2c_config_t* Config, u8_t* Buffer);

/** Sends out register address to slave device
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Register       index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @return:              == 0: register sent successfully within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32f1xx_send_register_address(ttc_i2c_config_t* Config, u32_t Register, ttc_i2c_register_type_e RegisterType);

/** Sends out given amount of bytes (low level send; does not generate Start- or Stop-event)
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Buffer         memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[]
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: event occured within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32f1xx_send_bytes(ttc_i2c_config_t* Config, const u8_t* Buffer, u16_t Amount);

/** Sends out single byte (low level send; does not generate Start- or Stop-event)
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Byte           8-bit value to send out
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: event occured within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _i2c_stm32f1xx_send_byte(ttc_i2c_config_t* Config, const u8_t *Byte);

/** closes the I2C bus and brings it back to a defined state.
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Status         simply passed as return value
 * @return               Status
 */
ttc_i2c_errorcode_e _i2c_stm32f1xx_close_bus(ttc_i2c_config_t* Config, ttc_i2c_errorcode_e Status);
/** triese to push and pull to SCL line to flush the slave internal state machine and bring it back to normal operation
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @return               Status
 */
ttc_i2c_errorcode_e _i2c_stm32f1xx_flush_slave(ttc_i2c_config_t* Config);
/** initialize/ reinitialize an I2C unit
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @return               Status
 */
ttc_i2c_errorcode_e _i2c_stm32f1xx_reinit_i2c(ttc_i2c_config_t* Config);

/** generate start condition
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return               Status
 */
ttc_i2c_errorcode_e _i2c_stm32f1xx_condition_start(ttc_i2c_config_t* Config);
/** generate start condition
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return               Status
 */
ttc_i2c_errorcode_e _i2c_stm32f1xx_condition_stop(ttc_i2c_config_t* Config);

/** send target address to slave
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param TargetAddress  TargetAddress of device to read from
 * @param Direction      =1: next data will be read from slave; =0: next data will be written to slave
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return               Status
 */
ttc_i2c_errorcode_e _i2c_stm32f1xx_send_target_address(ttc_i2c_config_t* Config, u16_t TargetAddress, ttc_i2c_direction_e Direction);

//InsertPrivatePrototypes above (DO NOT DELETE THIS LINE!)

//}private functions

#endif //I2C_STM32F1XX_H
