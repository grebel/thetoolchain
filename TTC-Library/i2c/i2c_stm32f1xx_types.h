#ifndef I2C_STM32F1XX_TYPES_H
#define I2C_STM32F1XX_TYPES_H

/** { i2c_stm32f1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for I2C devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by ttc_i2c_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150625 13:50:38 UTC
 *
 *  Note: See ttc_i2c.h for description of architecture independent I2C implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

// Events required by ttc_i2c_types.h
/** FIX: Event values swapped because TRA==R/W-bit of address byte and 1 means master wants to READ data
 * original definitions:
   #define TTC_I2C_EVENT_CODE_MASTER_TRANSMITTER_MODE_SELECTED          0x00070082  // BUSY, MSL, ADDR, TXE and TRA flags
   #define TTC_I2C_EVENT_CODE_MASTER_RECEIVER_MODE_SELECTED             0x00030002  // BUSY, MSL and ADDR flags
*/
// Note: MSB of EVENT_CODE == 1 means event is negated!
#define TTC_I2C_EVENT_CODE_MASTER_RECEIVER_MODE_SELECTED             0x00070082  // BUSY, MSL, ADDR, TXE and TRA flags
#define TTC_I2C_EVENT_CODE_MASTER_TRANSMITTER_MODE_SELECTED          0x00030002  // BUSY, MSL and ADDR flags
#define TTC_I2C_EVENT_CODE_MASTER_MODE_SELECT                        0x00030001  // BUSY, MSL and SB flag
#define TTC_I2C_EVENT_CODE_BUS_BUSY                                  0x00020000  // BUSY flag
#define TTC_I2C_EVENT_CODE_BUS_FREE                                  0x80020000  // BUSY flag (MSB bit set negates bit state)
#define TTC_I2C_EVENT_CODE_MASTER_BYTE_TRANSMITTED                   0x00070084  // TRA, BUSY, MSL, TXE and BTF flags
#define TTC_I2C_EVENT_CODE_MASTER_BYTE_RECEIVED                      0x00030040  // BUSY, MSL and RXNE flags
#define TTC_I2C_EVENT_CODE_MASTER_BYTE_TRANSMITTING                  0x00070080  // TRA, BUSY, MSL, TXE flags
#define TTC_I2C_EVENT_CODE_SLAVE_RECEIVER_ADDRESS_MATCHED            0x00020002  // BUSY and ADDR flags
#define TTC_I2C_EVENT_CODE_SLAVE_TRANSMITTER_ADDRESS_MATCHED         0x00060082  // TRA, BUSY, TXE and ADDR flags
#define TTC_I2C_EVENT_CODE_SLAVE_RECEIVER_SECONDADDRESS_MATCHED      0x00820000  // DUALF and BUSY flags
#define TTC_I2C_EVENT_CODE_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED   0x00860080  // DUALF, TRA, BUSY and TXE flags
#define TTC_I2C_EVENT_CODE_SLAVE_GENERALCALLADDRESS_MATCHED          0x00120000  // GENCALL and BUSY flags
#define TTC_I2C_EVENT_CODE_SLAVE_BYTE_RECEIVED                       0x00020040  // BUSY and RXNE flags
#define TTC_I2C_EVENT_CODE_SLAVE_STOP_DETECTED                       0x00000010  // STOPF flag
#define TTC_I2C_EVENT_CODE_SLAVE_BYTE_TRANSMITTED                    0x00060084  // TRA, BUSY, TXE and BTF flags
#define TTC_I2C_EVENT_CODE_SLAVE_BYTE_TRANSMITTING                   0x00060080  // TRA, BUSY and TXE flags
#define TTC_I2C_EVENT_CODE_SLAVE_ACK_FAILURE                         0x00000400  // AF flag
#define TTC_I2C_EVENT_CODE_SLAVE_BUFFER_OVERRUN                      0x00000800  // OVR flag

// Note: MSB of flag code == 1 meas that flag is located in SR2 instead of SR1!
#define TTC_I2C_FLAG_CODE_SMBUS_ALERT                                0x00008000  // SR1: SMBus Alert occured (SMBALERT pin triggered or response address header received)
#define TTC_I2C_FLAG_CODE_SMBUS_TIMEOUT                              0x00004000  // SR1: Timeout occured in SMBus Mode
#define TTC_I2C_FLAG_CODE_CRC_ERROR                                  0x00001000  // SR1: CRC Error for received byte                         (Slave mode)
#define TTC_I2C_FLAG_CODE_BUFFER_OVERRUN                             0x00000800  // SR1: Buffer Overrun                                      (Slave mode)
#define TTC_I2C_FLAG_CODE_ACKNOWLEDGE_FAILURE                        0x00000400  // SR1: Acknowledge Failure
#define TTC_I2C_FLAG_CODE_ARBITRATION_LOST                           0x00000200  // SR1: Arbitration Lost detected                           (Master mode)
#define TTC_I2C_FLAG_CODE_BUS_ERROR                                  0x00000100  // SR1: Bus Error detected                                  (Master mode)
#define TTC_I2C_FLAG_CODE_TRANSMIT_BUFFER_EMPTY                      0x00000080  // SR1: Transmit Data Register Empty     (not set during address phase)
#define TTC_I2C_FLAG_CODE_RECEIVE_BUFFER_NOT_EMPTY                   0x00000040  // SR1: Receive Data Register not Empty  (not set during address phase)
#define TTC_I2C_FLAG_CODE_STOP_DETECTED                              0x00000010  // SR1: Stop Condition detected                             (Slave mode)
#define TTC_I2C_FLAG_CODE_10_BIT_ADDRESS                             0x00000008  // SR1: 10 bit header sent                                  (Master mode)
#define TTC_I2C_FLAG_CODE_BYTE_TRANSFER_FINISHED                     0x00000004  // SR1: Byte Transfer Finished
#define TTC_I2C_FLAG_CODE_ADDRESS_SENT                               0x00000002  // SR1: Address Sent                                        (Master mode)
#define TTC_I2C_FLAG_CODE_START_BIT_DETECTED                         0x00000001  // SR1: Start Bit                                           (Master mode)
#define TTC_I2C_FLAG_CODE_OWNADDRESS1_MATCHED                        0x80000080  // SR2: ==1: received address matches OAR1; OAR2 otherwise  (Slave mode)
#define TTC_I2C_FLAG_CODE_SMBUS_HOST_HEADER_RECEIVED                 0x80000040  // SR2: Matching SMBus Host Header received                 (Slave mode)
#define TTC_I2C_FLAG_CODE_SMBUS_DEFAULT_ADDRESS_RECEIVED             0x80000020  // SR2: Matching SMBus Default Address received             (Slave mode)
#define TTC_I2C_FLAG_CODE_SMBUS_GENERAL_CALL_ADDRESS_RECEIVED        0x80000010  // SR2: Matching General Call Address received              (Slave mode)
#define TTC_I2C_FLAG_CODE_READ_MODE_DETECTED                         0x80000004  // SR2: read/write bit of last address byte
#define TTC_I2C_FLAG_CODE_BUS_BUSY                                   0x80000002  // SR2: I2C bus is busy (SDA or SCL is low)
#define TTC_I2C_FLAG_CODE_MASTER_MODE                                0x80000001  // SR2: ==1: Device operates in master mode; slave mode otherwise

#ifndef TTC_I2C1   // device not defined in makefile
    #define TTC_I2C1    E_ttc_i2c_architecture_stm32f1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "../register/register_stm32f1xx_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_i2c_types.h *************************

typedef struct {  // stm32f1xx specific configuration data of single i2c device
    t_register_stm32f1xx_i2c* BaseRegister;       // base address of i2c device registers
} __attribute__( ( __packed__ ) ) t_i2c_stm32f1xx_config;

// t_ttc_i2c_architecture is required by ttc_i2c_types.h
#define t_ttc_i2c_architecture t_i2c_stm32f1xx_config
#define t_ttc_i2c_base_register t_register_stm32f1xx_i2c

//} Structures/ Enums


#endif //I2C_STM32F1XX_TYPES_H
