/** { ttc_network_layer.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for network_layer devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 23 at 20140224 16:21:32 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_network_layer.h"

#if TTC_NETWORK_LAYER_AMOUNT == 0
#warning No NETWORK_LAYER devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of network_layer devices.
 *
 */


// for each initialized device, a pointer to its generic and usart definitions is stored
A_define( t_ttc_network_layer_config*, ttc_network_layer_configs, TTC_NETWORK_LAYER_AMOUNT );

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8 ttc_network_layer_get_max_index() {
    return TTC_NETWORK_LAYER_AMOUNT;
}
t_ttc_network_layer_config* ttc_network_layer_get_configuration( t_u8 LogicalIndex ) {
    Assert_NETWORK_LAYER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_network_layer_config* Config = A( ttc_network_layer_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        Config = A( ttc_network_layer_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_network_layer_config ) );
        Config->Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_network_layer_load_defaults( LogicalIndex );
    }

    return Config;
}
e_ttc_network_layer_errorcode ttc_network_layer_deinit( t_u8 LogicalIndex ) {
    Assert_NETWORK_LAYER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_network_layer_config* Config = ttc_network_layer_get_configuration( LogicalIndex );

    e_ttc_network_layer_errorcode Result = _driver_network_layer_deinit( Config );
    if ( Result == ec_network_layer_OK )
    { Config->Flags.Bits.Initialized = 0; }
}
e_ttc_network_layer_errorcode ttc_network_layer_init( t_u8 LogicalIndex ) {
    Assert_NETWORK_LAYER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_network_layer_config* Config = ttc_network_layer_get_configuration( LogicalIndex );

    e_ttc_network_layer_errorcode Result = _driver_network_layer_init( Config );
    if ( Result == ec_network_layer_OK )
    { Config->Flags.Bits.Initialized = 1; }
}
e_ttc_network_layer_errorcode ttc_network_layer_load_defaults( t_u8 LogicalIndex ) {
    Assert_NETWORK_LAYER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_network_layer_config* Config = ttc_network_layer_get_configuration( LogicalIndex );

    if ( Config->Flags.Bits.Initialized )
    { ttc_network_layer_deinit( LogicalIndex ); }

    ttc_memory_set( Config, 0, sizeof( t_ttc_network_layer_config ) );

    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    Config->PhysicalIndex = ttc_network_layer_logical_2_physical_index( LogicalIndex );

    //Insert additional generic default values here ...

    _driver_network_layer_load_defaults( Config );
}
t_u8 ttc_network_layer_logical_2_physical_index( t_u8 LogicalIndex ) {

    switch ( LogicalIndex ) {         // determine base register and other low-level configuration data
            #ifdef TTC_NETWORK_LAYER1
        case 1: return TTC_NETWORK_LAYER1;
            #endif
            #ifdef TTC_NETWORK_LAYER2
        case 2: return TTC_NETWORK_LAYER2;
            #endif
            #ifdef TTC_NETWORK_LAYER3
        case 3: return TTC_NETWORK_LAYER3;
            #endif
            #ifdef TTC_NETWORK_LAYER4
        case 4: return TTC_NETWORK_LAYER4;
            #endif
            #ifdef TTC_NETWORK_LAYER5
        case 5: return TTC_NETWORK_LAYER5;
            #endif
            #ifdef TTC_NETWORK_LAYER6
        case 6: return TTC_NETWORK_LAYER6;
            #endif
            #ifdef TTC_NETWORK_LAYER7
        case 7: return TTC_NETWORK_LAYER7;
            #endif
            #ifdef TTC_NETWORK_LAYER8
        case 8: return TTC_NETWORK_LAYER8;
            #endif
            #ifdef TTC_NETWORK_LAYER9
        case 9: return TTC_NETWORK_LAYER9;
            #endif
            #ifdef TTC_NETWORK_LAYER10
        case 10: return TTC_NETWORK_LAYER10;
            #endif
        // extend as required

        default: break;
    }

    Assert_NETWORK_LAYER( 0, ttc_assert_origin_auto ); // No TTC_NETWORK_LAYERn defined! Check your makefile.100_board_* file!
    return -1; // program flow should never go here
}
t_u8 ttc_network_layer_physical_2_logical_index( t_u8 PhysicalIndex ) {

    switch ( PhysicalIndex ) {         // determine base register and other low-level configuration data
            #ifdef TTC_NETWORK_LAYER1
        case TTC_NETWORK_LAYER1: return 1;
            #endif
            #ifdef TTC_NETWORK_LAYER2
        case TTC_NETWORK_LAYER2: return 2;
            #endif
            #ifdef TTC_NETWORK_LAYER3
        case TTC_NETWORK_LAYER3: return 3;
            #endif
            #ifdef TTC_NETWORK_LAYER4
        case TTC_NETWORK_LAYER4: return 4;
            #endif
            #ifdef TTC_NETWORK_LAYER5
        case TTC_NETWORK_LAYER5: return 5;
            #endif
            #ifdef TTC_NETWORK_LAYER6
        case TTC_NETWORK_LAYER6: return 6;
            #endif
            #ifdef TTC_NETWORK_LAYER7
        case TTC_NETWORK_LAYER7: return 7;
            #endif
            #ifdef TTC_NETWORK_LAYER8
        case TTC_NETWORK_LAYER8: return 8;
            #endif
            #ifdef TTC_NETWORK_LAYER9
        case TTC_NETWORK_LAYER9: return 9;
            #endif
            #ifdef TTC_NETWORK_LAYER10
        case TTC_NETWORK_LAYER10: return 10;
            #endif
        // extend as required

        default: break;
    }

    Assert_NETWORK_LAYER( 0, ttc_assert_origin_auto ); // No TTC_NETWORK_LAYERn defined! Check your makefile.100_board_* file!
    return -1; // program flow should never go here
}
void ttc_network_layer_prepare() {
    // add your startup code here (Singletasking!)
    _driver_network_layer_prepare();
}
e_ttc_network_layer_errorcode ttc_network_layer_reset( t_u8 LogicalIndex ) {
    Assert_NETWORK_LAYER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_network_layer_config* Config = ttc_network_layer_get_configuration( LogicalIndex );

    _driver_network_layer_reset( Config );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_network_layer(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
