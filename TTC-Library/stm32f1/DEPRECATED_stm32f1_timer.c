/** { stm32f1_timer.c ************************************************
 
                           The ToolChain
                      
   High-Level interface for TIMER device.
     
   Note: See ttc_timer.h for description of stm32f1 independent TIMER implementation.
  
   Authors: 
}*/


#include "DEPRECATED_stm32f1_timer.h"

//{ Function definitions ***************************************************

bool stm32f1_timer_check_initialized(ttc_timer_config_t* Config) {
    Assert_TIMER(Config, tte_InvalidArgument);
    switch(Config->LogicalIndex){
    case 1: if((TIM1->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
    case 2: if((TIM2->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
    case 3: if((TIM3->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#ifdef TTC_TIMER4
    case 4: if((TIM4->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER5
    case 5: if((TIM5->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER6
    case 6: if((TIM6->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER7
    case 7: if((TIM7->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER8
    case 8: if((TIM8->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER9
    case 9: if((TIM9->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER10
    case 10: if((TIM10->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER11
    case 11: if((TIM11->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER12
    case 12: if((TIM12->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER13
    case 13: if((TIM13->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER14
    case 14: if((TIM14->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER15
    case 15: if((TIM15->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER16
    case 16: if((TIM16->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER17
    case 17: if((TIM17->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
    default: Assert_TIMER(0, ec_UNKNOWN);
    }

    return FALSE;
}
ttc_timer_errorcode_e stm32f1_timer_load_defaults(ttc_timer_config_t* Config) {
    Assert_TIMER(Config != NULL, tte_NotImplemented);
    Assert_TIMER(Config->LogicalIndex > 0, tte_InvalidArgument);

/*{    Config->Flags.All=0;
    Config->Timer_Arch->BaseRegister->CR1.URS=0;       // This bit is set and cleared by software to select the UEV event sources
    Config->Timer_Arch->BaseRegister->CR1.CMS=0;       // Center-aligned mode selection (Deactivate, control by DIR flag)
    Config->Timer_Arch->BaseRegister->CR1.DIR=0;       // 0-Upcounting Mode Selection, 1-Downcounting Mode Selection
    Config->Timer_Arch->BaseRegister->CR1.OPM=0;       // One Pulse Mode deactivated
    Config->Timer_Arch->BaseRegister->CR1.URS=1;       // Update request source
    Config->Timer_Arch->BaseRegister->SMCR.SMS=0;      // Slave mode selection disabled
       Config->Timer_Arch->BaseRegister->PSC = 1;   }*/         // Prescaler configuration
    switch (Config->LogicalIndex) {           // determine base register and other low-level configuration data
       case 1: { // load low-level configuration of second timer device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM1, ENABLE);
         break;
       }
       case 2: { // load low-level configuration of third timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM2, ENABLE);
         break;
       }
       case 3: { // load low-level configuration of fourth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM3, ENABLE);
         break;
       }
       case 4: { // load low-level configuration of fifth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM4, ENABLE);
         break;
       }
       case 5: { // load low-level configuration of sixth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM5, ENABLE);
         break;
       }
       case 6: { // load low-level configuration of seveth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM6, ENABLE);
         break;
       }
       case 7: { // load low-level configuration of eighth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM7, ENABLE);
         break;
       }
       case 8: { // load low-level configuration of ninth timer device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM8, ENABLE);
         break;
       }
       case 9: { // load low-level configuration of tenth timer device
         //Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM10; // load adress of register base of TIMER device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM9, ENABLE);
         break;
       }
       case 10: { // load low-level configuration of eleventh timer device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM10, ENABLE);
         break;
       }
       case 11: { // load low-level configuration of twelfth timer device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM11, ENABLE);
         break;
       }
       case 12: { // load low-level configuration of thirteenth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM12, ENABLE);
         break;
       }
       case 13: { // load low-level configuration of fourteenth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM13, ENABLE);
         break;
       }
       case 14: { // load low-level configuration of fifteenth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM14, ENABLE);
         break;
       }
       case 15: { // load low-level configuration of sixteenth timer device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM15, ENABLE);
         break;
       }
       case 16: { // load low-level configuration of seventeenth timer device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM16, ENABLE);
         break;
       }
       case 17: { // load low-level configuration of seventeenth timer device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM17, ENABLE);
         break;
       }
       default: Assert_TIMER(0, tte_NotImplemented);
           break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
       }

    return tte_OK;
}
stm32f1_timer_config_t* stm32f1_timer_get_configuration(ttc_timer_config_t* Config) {
    Assert_TIMER(Config, tte_InvalidArgument);
    Assert_TIMER(Config->LogicalIndex > 0, tte_InvalidArgument);

    stm32f1_timer_config_t* Config_stm32f1 = Config->Timer_Arch;
    if (Config_stm32f1 == NULL)   // first call: allocate + init configuration
      Config_stm32f1 = ttc_heap_alloc_zeroed( sizeof(ttc_timer_arch_t) );


      switch (Config->LogicalIndex) {           // determine base register and other low-level configuration data
         case 1: { // load low-level configuration of second timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM1; // load adress of register base of TIMER device
           break;
         }
         case 2: { // load low-level configuration of third timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM2; // load adress of register base of TIMER device
           break;
         }
         case 3: { // load low-level configuration of fourth timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM3; // load adress of register base of TIMER device
           break;
         }
         case 4: { // load low-level configuration of fifth timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM4; // load adress of register base of TIMER device
           break;
         }
         case 5: { // load low-level configuration of sixth timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM5; // load adress of register base of TIMER device
           break;
         }
         case 6: { // load low-level configuration of seveth timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM6; // load adress of register base of TIMER device
           break;
         }
         case 7: { // load low-level configuration of eighth timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM7; // load adress of register base of TIMER device
           break;
         }
         case 8: { // load low-level configuration of ninth timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM8; // load adress of register base of TIMER device
           break;
         }
         case 9: { // load low-level configuration of tenth timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM9; // load adress of register base of TIMER device
           break;
         }
         case 10: { // load low-level configuration of eleventh timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM10; // load adress of register base of TIMER device
           break;
         }
         case 11: { // load low-level configuration of twelfth timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM11; // load adress of register base of TIMER device
           break;
         }
         case 12: { // load low-level configuration of thirteenth timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM12; // load adress of register base of TIMER device
           break;
         }
         case 13: { // load low-level configuration of fourteenth timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM13; // load adress of register base of TIMER device
           break;
         }
         case 14: { // load low-level configuration of fifteenth timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM14; // load adress of register base of TIMER device
           break;
         }
         case 15: { // load low-level configuration of sixteenth timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM15; // load adress of register base of TIMER device
           break;
         }
         case 16: { // load low-level configuration of seventeenth timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM16; // load adress of register base of TIMER device
           break;
         }
         case 17: { // load low-level configuration of seventeenth timer device
           Config_stm32f1->BaseRegister = (register_stm32f1xx_timer_t*)TIM17; // load adress of register base of TIMER device
           break;
         }
         default: Assert_TIMER(0, tte_NotImplemented);
             break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
         }
    
    Assert_TIMER(Config_stm32f1, tte_InvalidConfiguration);
    return Config_stm32f1;
}
ttc_timer_errorcode_e stm32f1_timer_get_features(ttc_timer_config_t* Config) {
    Assert_TIMER(Config,    tte_InvalidArgument);
    Assert_TIMER(Config->LogicalIndex > 0, tte_InvalidArgument);
    if (Config->LogicalIndex > TTC_INTERRUPT_TIMER_AMOUNT)
        return tte_DeviceNotFound;

    Config->Flags.All                        = 0;

    /*switch (Config->LogicalIndex) {           // determine features of indexed TIMER device
#ifdef TTC_TIMER1
      case 1: break;
#endif
      default: Assert_TIMER(0, tte_NotImplemented); break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
    }*/

    return tte_OK;
}
ttc_timer_errorcode_e stm32f1_timer_init() {
    return tte_OK;
}
ttc_timer_errorcode_e stm32f1_timer_set(ttc_timer_config_t* Config) {
    Assert_TIMER(Config->LogicalIndex > 0, tte_InvalidArgument);
    Assert_TIMER(Config->TimerEvent.TimePeriod > 0, tte_InvalidArgument);
    if (Config->LogicalIndex > TTC_INTERRUPT_TIMER_AMOUNT)
        return tte_DeviceNotFound;
    TIM_TimeBaseInitTypeDef TimerStd;
    stm32f1_timer_deinit(Config);
    stm32f1_timer_load_defaults(Config);

    if(Config->TimeScale == 0){
        //TimerStd.TIM_ClockDivision = TIM_CKD_DIV1;
        TIM_SetClockDivision(stm32f1_timer_physical_index_2_address(Config->LogicalIndex), TIM_CKD_DIV1);
        TimerStd.TIM_Prescaler = 71;
        TimerStd.TIM_Period = Config->TimerEvent.TimePeriod;
    }
    else{
        //TimerStd.TIM_ClockDivision = TIM_CKD_DIV1;
        TIM_SetClockDivision(stm32f1_timer_physical_index_2_address(Config->LogicalIndex), TIM_CKD_DIV2);
        TimerStd.TIM_Prescaler = 35999;
        if(Config->TimerEvent.TimePeriod <= 32767)
            TimerStd.TIM_Period = Config->TimerEvent.TimePeriod*2;
        else return tte_InvalidArgument;
    }
    TimerStd.TIM_CounterMode = TIM_CounterMode_Up;
    TimerStd.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(stm32f1_timer_physical_index_2_address(Config->LogicalIndex), &TimerStd);
    TIM_ARRPreloadConfig(stm32f1_timer_physical_index_2_address(Config->LogicalIndex), ENABLE);

    Config->Flags.Bits.Initialized = 1;
    return tte_OK;
}
ttc_timer_errorcode_e stm32f1_timer_deinit(ttc_timer_config_t* Config) {
    Assert_TIMER(Config, tte_NotImplemented);
    Assert_TIMER(Config->LogicalIndex > 0, tte_InvalidArgument);

    // ToDo: Deinitialize hardware device
    TIM_DeInit(stm32f1_timer_physical_index_2_address(Config->LogicalIndex));
    Config->Flags.Bits.Initialized = 0;

    return tte_OK;
}
TIM_TypeDef* stm32f1_timer_physical_index_2_address(u8_t LogicalIndex) {
  Assert_TIMER(LogicalIndex > 0, tte_InvalidArgument);
  Assert_TIMER(LogicalIndex < TTC_INTERRUPT_TIMER_AMOUNT, tte_InvalidArgument);

  switch (LogicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_TIMER1
           case TTC_TIMER1: return (TIM_TypeDef*)TIM1;
#endif
#ifdef TTC_TIMER2
           case TTC_TIMER2: return (TIM_TypeDef*)TIM2;
#endif
#ifdef TTC_TIMER3
           case TTC_TIMER3: return (TIM_TypeDef*)TIM3;
#endif
#ifdef TTC_TIMER4
           case TTC_TIMER4: return (TIM_TypeDef*)TIM4;
#endif
#ifdef TTC_TIMER5
           case TTC_TIMER5: return (TIM_TypeDef*)TIM5;
#endif
#ifdef TTC_TIMER6
           case TTC_TIMER6: return (TIM_TypeDef*)TIM6;
#endif
#ifdef TTC_TIMER7
           case TTC_TIMER7: return (TIM_TypeDef*)TIM7;
#endif
#ifdef TTC_TIMER8
           case TTC_TIMER8: return (TIM_TypeDef*)TIM8;
#endif
#ifdef TTC_TIMER9
           case TTC_TIMER9: return (TIM_TypeDef*)TIM9;
#endif
#ifdef TTC_TIMER10
           case TTC_TIMER10: return (TIM_TypeDef*)TIM10;
#endif
#ifdef TTC_TIMER11
           case TTC_TIMER11: return (TIM_TypeDef*)TIM11;
#endif
#ifdef TTC_TIMER12
           case TTC_TIMER12: return (TIM_TypeDef*)TIM12;
#endif
#ifdef TTC_TIMER13
           case TTC_TIMER13: return (TIM_TypeDef*)TIM13;
#endif
#ifdef TTC_TIMER14
           case TTC_TIMER14: return (TIM_TypeDef*)TIM14;
#endif
#ifdef TTC_TIMER15
           case TTC_TIMER15: return (TIM_TypeDef*)TIM15;
#endif
#ifdef TTC_TIMER16
           case TTC_TIMER16: return (TIM_TypeDef*)TIM16;
#endif
#ifdef TTC_TIMER17
           case TTC_TIMER17: return (TIM_TypeDef*)TIM17;
#endif
      default: Assert_TIMER(0, tte_NotImplemented); break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
    }

  return (TIM_TypeDef*)-1;
}
ttc_timer_errorcode_e stm32f1_timer_reset(ttc_timer_config_t* Config){
    Assert_TIMER(Config, tte_NotImplemented);
    stm32f1_timer_stop(Config->LogicalIndex);
    Config->Counter=0;
    return stm32f1_timer_set(Config);
}
ttc_timer_errorcode_e stm32f1_timer_stop(u8_t LogicalIndex){
    TIM_Cmd(stm32f1_timer_physical_index_2_address(LogicalIndex), DISABLE);
    return tte_OK;
}
ttc_timer_errorcode_e stm32f1_timer_start(u8_t LogicalIndex){
    TIM_Cmd(stm32f1_timer_physical_index_2_address(LogicalIndex), ENABLE);
    return tte_OK;
}
void stm32f1_timer_set_counter(u8_t LogicalIndex, u16_t Value, u8_t TimeScale){
    if(TimeScale == 0){
        TIM_SetClockDivision(stm32f1_timer_physical_index_2_address(LogicalIndex), TIM_CKD_DIV1);
        TIM_PrescalerConfig(stm32f1_timer_physical_index_2_address(LogicalIndex), 71,TIM_PSCReloadMode_Immediate);
    }
    else{
        TIM_SetClockDivision(stm32f1_timer_physical_index_2_address(LogicalIndex), TIM_CKD_DIV2);
        TIM_PrescalerConfig(stm32f1_timer_physical_index_2_address(LogicalIndex), 35999,TIM_PSCReloadMode_Immediate);
    }
    TIM_SetCounter(stm32f1_timer_physical_index_2_address(LogicalIndex), Value);
}
u16_t stm32f1_timer_get_counter(u8_t LogicalIndex){
    return (u16_t)TIM_GetCounter(stm32f1_timer_physical_index_2_address(LogicalIndex));
}
ttc_timer_statuscode_e stm32f1_timer_get_state(u8_t LogicalIndex){
    TIMx_SR_u StatusRegister;
    StatusRegister.U16 = (u16_t)stm32f1_timer_physical_index_2_address(LogicalIndex)->SR;
    if(StatusRegister.Fields.UIF)
        return tts_Update;
    else if(StatusRegister.Fields.CC1IF)
       return tts_CC1;
    else if(StatusRegister.Fields.CC2IF)
       return tts_CC2;
    else if(StatusRegister.Fields.CC3IF)
       return tts_CC3;
    else if(StatusRegister.Fields.CC4IF)
       return tts_CC4;
    else if(StatusRegister.Fields.COMIF)
       return tts_COM;
    else if(StatusRegister.Fields.TIF)
       return tts_Trigger;
    else if(StatusRegister.Fields.BIF)
       return tts_Break;
    else if(StatusRegister.Fields.CC1OF)
       return tts_CC1OF;
    else if(StatusRegister.Fields.CC2OF)
       return tts_CC2OF;
    else if(StatusRegister.Fields.CC3OF)
       return tts_CC3OF;
    else if(StatusRegister.Fields.CC4OF)
       return tts_CC4OF;
    else return tts_Unknown;
}

//} Function definitions
//{ private functions (ideally) ********************************************

//} private functions
