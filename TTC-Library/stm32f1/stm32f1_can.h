#ifndef STM32F1_CAN_H
#define STM32F1_CAN_H

/** { stm32f1_can.h **********************************************
 
                           The ToolChain
                      
   High-Level interface for CAN device.

   Structures, Enums and Defines being required by high-level can and application.

   Note: See ttc_can.h for description of stm32f1 independent CAN implementation.
  
   Authors: 

 
}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

#include "stm32f1_can_types.h"
#include "../ttc_can_types.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"

#include "stm32f10x.h"
#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
#include "stm32f10x_conf.h"
#include "stm32f10x_can.h"
#include "stm32f10x_gpio.h"
#include "misc.h"

//} Includes
//{ Macro definitions ****************************************************

#define Driver_can_load_defaults(Config) stm32f1_can_load_defaults(Config)
#define Driver_can_deinit(Config)        stm32f1_can_deinit(Config)
#define Driver_can_init(Config)          stm32f1_can_init(Config)


//} Includes
//{ Function prototypes **************************************************

/** checks if indexed CAN bus already has been initialized
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for LogicalIndex)
 * @return              == TRUE: indexed CAN unit is already initialized
 */
bool stm32f1_can_check_initialized(t_ttc_can_config* Config);

/** returns reference to low-level configuration struct of indexed CAN device
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for LogicalIndex)
 * @return              pointer to struct t_ttc_can_config (will assert if no configuration available)
 */
t_stm32f1_can_config* stm32f1_can_get_configuration(t_ttc_can_config* Config);

/** loads configuration of indexed CAN interface with default values
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_can_errorcode stm32f1_can_load_defaults(t_ttc_can_config* Config);

/** fills out given Config with maximum valid values for indexed CAN
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for LogicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_can_errorcode stm32f1_can_get_features(t_ttc_can_config* Config);

/** reset configuration of indexed CAN device into default state
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for LogicalIndex)
 * @return              == 0: CAN has been initialized successfully; != 0: error-code
 */
e_ttc_can_errorcode stm32f1_can_reset(t_ttc_can_config* Config);

/** initializes single CAN
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for LogicalIndex)
 * @return              == 0: CAN has been initialized successfully; != 0: error-code
 */
e_ttc_can_errorcode stm32f1_can_init(t_ttc_can_config* Config);

/** shutdown single CAN device
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for LogicalIndex)
 * @return              == 0: CAN has been shutdown successfully; != 0: error-code
 */
e_ttc_can_errorcode stm32f1_can_deinit(t_ttc_can_config* Config);

/** maps from logical to physical device index
 *
 * High-level cans (ttc_can_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_CANn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be 
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any 
 * order.
 *
 * @param LogicalIndex  logical index of can device (1..ttc_can_get_max_index() )
 * @return              physical index of can device (0 = first physical can device, ...)
 */
t_u8 stm32f1_can_logical_2_physical_index(t_u8 LogicalIndex);

//Additional Functions goes here...


e_ttc_can_errorcode stm32f1_can_send_raw(t_ttc_can_config* Config, const char* Buffer, t_u16 Amount);

/** Send out given bytes from buffer until first zero byte or MaxLength reached.
 *
 * Note: CAN must be initialized before!
 * Note: This function is partially thread safe (use it for each CAN from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for LogicalIndex)
 * @param Buffer         memory location from which to read data
 * @param MaxLength      no more than this amount of bytes will be send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_can_errorcode stm32f1_can_send_string(t_ttc_can_config* Config, const char* Buffer, t_u16 MaxLength);

/** Send out given data word (8 or 9 bits)
 *
 * Note: CAN must be initialized before!
 * Note: This function is partially thread safe (use it for each CAN from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for LogicalIndex)
 * @param Byte           8 bit value to send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_can_errorcode stm32f1_can_send_word(t_ttc_can_config* Config, const t_u16 Word);

/** Reads single data word from input buffer (8 bits)
 *
 * Note: CAN must be initialized before!
 * Note: This function is partially thread safe (use it for each CAN from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for LogicalIndex)
 * @param Word           pointer to 16 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_can_errorcode stm32f1_can_read_word(t_ttc_can_config* Config, t_u16* Word);

/** Reads single data byte from input buffer.
 *
 * Note: CAN must be initialized before!
 * Note: This function is partially thread safe (use it for each CAN from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for LogicalIndex)
 * @param Word           pointer to 16 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_can_errorcode stm32f1_can_read_byte(t_ttc_can_config* Config, t_u8* Byte);

/** loads default configuration for CAN filter
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for LogicalIndex)
 * @param FilterConfig  pointer to struct t_ttc_can_filter_config
 * @return              == 0: CAN has been shutdown successfully; != 0: error-code
 */
e_ttc_can_errorcode stm32f1_can_load_filter_defaults(t_ttc_can_filter_config* FilterConfig);

/** initialice a single CAN filter
 *
 * Note: CAN must be initialized before!
 *
 * @param FilterConfig  pointer to struct t_ttc_can_filter_config
 * @return              == 0: CAN has been shutdown successfully; != 0: error-code
 */
e_ttc_can_errorcode stm32f1_can_filter_init(t_ttc_can_filter_config* FilterConfig);

//Private Functions
/** resets library. Automatically called.
 * Note: This function is private to stm32_can.h and should not be called from outside!
 */
void _can_reset_all();

/** Sends out given byte.
 * Note: This low-level function should not be called from outside!
 */
void _stm32f1_can_send_single_word(t_ttc_can_config* Config, t_u16 Word);

//} Function prototypes

#endif //STM32F1_CAN_H
