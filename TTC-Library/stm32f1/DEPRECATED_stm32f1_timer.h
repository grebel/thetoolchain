#ifndef STM32F1_TIMER_H
#define STM32F1_TIMER_H

/** { stm32f1_timer.h **********************************************
 
                           The ToolChain
                      
   High-Level interface for TIMER device.

   Structures, Enums and Defines being required by high-level timer and application.

   Note: See ttc_timer.h for description of stm32f1 independent TIMER implementation.
  
   Authors: 

 
}*/
//{ Defines/ TypeDefs ****************************************************
//} Defines
//{ Includes *************************************************************

#include "stm32f1_timer_types.h"
#include "../ttc_timer_types.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"

//} Includes
//{ Macro definitions ****************************************************

#define Driver_timer_prepare()                  stm32f1_timer_prepare()
#define Driver_timer_load_defaults(Config)      stm32f1_timer_load_defaults(Config)
#define Driver_timer_deinit(Config)             stm32f1_timer_deinit(Config)
#define Driver_timer_init()                     stm32f1_timer_init()
#define Driver_timer_set(Config)                stm32f1_timer_set(Config)
#define Driver_timer_reset(Config)              stm32f1_timer_reset(Config)
#define Driver_timer_check_initialized(Config)  stm32f1_timer_check_initialized(Config)
#define Driver_timer_stop(LogicalIndex)         stm32f1_timer_stop(LogicalIndex)
#define Driver_timer_start(LogicalIndex)        stm32f1_timer_start(LogicalIndex)
#define Driver_timer_set_counter(LogicalIndex, Value, TimeScale)  stm32f1_timer_set_counter(LogicalIndex, Value, TimeScale)
#define Driver_timer_get_counter(LogicalIndex)  stm32f1_timer_get_counter(LogicalIndex)
#define Driver_timer_get_state(LogicalIndex)    stm32f1_timer_get_state(LogicalIndex)

//} Includes
//{ Function prototypes **************************************************

void stm32f1_timer_prepare(); //Just to avoid warnings

/** checks if indexed TIMER bus already has been initialized
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == TRUE: indexed TIMER unit is already initialized
 */

bool stm32f1_timer_check_initialized(ttc_timer_config_t* Config);

/** returns reference to low-level configuration struct of indexed TIMER device
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              pointer to struct ttc_timer_config_t (will assert if no configuration available)
 */
stm32f1_timer_config_t* stm32f1_timer_get_configuration(ttc_timer_config_t* Config);

/** loads configuration of indexed TIMER interface with default values
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
ttc_timer_errorcode_e stm32f1_timer_load_defaults(ttc_timer_config_t* Config);

/** fills out given Config with maximum valid values for indexed TIMER
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
ttc_timer_errorcode_e stm32f1_timer_get_features(ttc_timer_config_t* Config);

/** reset configuration of indexed TIMER device into default state
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
ttc_timer_errorcode_e stm32f1_timer_reset(ttc_timer_config_t* Config);

/** initializes single TIMER from 1 mSec
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
ttc_timer_errorcode_e stm32f1_timer_init();

/** initializes single TIMER up to 1 mSec
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
ttc_timer_errorcode_e stm32f1_timer_set(ttc_timer_config_t* Config);

/** shutdown single TIMER device
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been shutdown successfully; != 0: error-code
 */
ttc_timer_errorcode_e stm32f1_timer_deinit(ttc_timer_config_t* Config);

/** maps from logical to physical device index
 *
 * High-level timers (ttc_timer_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_TIMERn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be 
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any 
 * order.
 *
 * @param LogicalIndex  logical index of timer device (1..ttc_timer_get_max_index() )
 * @return              physical address of timer device (0 = first physical timer device, ...)
 */
TIM_TypeDef* stm32f1_timer_physical_index_2_address(u8_t LogicalIndex);

/** resets a single TIMER, remaining the information on the data structure
 *
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 *
 */
ttc_timer_errorcode_e stm32f1_timer_reset(ttc_timer_config_t* Config);

/** stops a single TIMER, remaining the information on the data structure
 *
 * @param LogicalIndex  Timer Index to stop
 *
 */
ttc_timer_errorcode_e stm32f1_timer_stop(u8_t LogicalIndex);

/** starts a single TIMER
 *
 * @param LogicalIndex  Timer Index to start
 *
 */
ttc_timer_errorcode_e stm32f1_timer_start(u8_t LogicalIndex);

/** set a counter value on a single TIMER
 *
 * @param LogicalIndex  Timer Index to set a time period
 * @param Value         Value to load on the counter
 * @param TimeScale     0: uSeconds time scale, 1: mSeconds time scale
 *
 */
void stm32f1_timer_set_counter(u8_t LogicalIndex, u16_t Value, u8_t TimeScale);

/** gets the counter value of a single TIMER
 *
 * @param LogicalIndex  Timer Index to get the counter value
 * @return              Counter value (TIMx->CNT)
 *
 */
u16_t stm32f1_timer_get_counter(u8_t LogicalIndex);

/** gets the state of a single TIMER
 *
 * @param LogicalIndex  Timer Index to get the status value
 * @return              Status value (TIMx->SR)
 *
 */
ttc_timer_statuscode_e stm32f1_timer_get_state(u8_t LogicalIndex);
//} Function prototypes

#endif //STM32F1_TIMER_H
