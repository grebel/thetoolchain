/** { stm32f1_can.c ************************************************
 
                           The ToolChain
                      
   High-Level interface for CAN device.
     
   Note: See ttc_can.h for description of stm32f1 independent CAN implementation.
  
   Authors: 
}*/

#include "stm32f1_can.h"

//{ Function definitions ***************************************************

bool stm32f1_can_check_initialized(t_ttc_can_config* Config) {

    if (Config->Architecture)
        return TRUE;

    return FALSE;
}

e_ttc_can_errorcode stm32f1_can_load_defaults(t_ttc_can_config* Config) {
    Assert_CAN(Config != NULL, ttc_assert_origin_auto);
    Assert_CAN(Config->LogicalIndex > 0, ttc_assert_origin_auto);

    Config->Flags.All                        = 0;
    // CAN_Cfg->Flags.Bits.Master                = 1;

    return ec_can_OK;
}

t_stm32f1_can_config* stm32f1_can_get_configuration(t_ttc_can_config* Config) {
    Assert_CAN(Config != NULL, ttc_assert_origin_auto);
    Assert_CAN(Config->LogicalIndex > 0, ttc_assert_origin_auto);

    t_stm32f1_can_config* Config_stm32f1 = Config->Architecture;
    if (Config_stm32f1 == NULL) {  // first call: allocate + init configuration
      Config->Architecture = Config_stm32f1 = ttc_heap_alloc_zeroed( sizeof(t_ttc_can_arch) );
      
      Config_stm32f1->PhysicalIndex = stm32f1_can_logical_2_physical_index(Config->LogicalIndex);
      
      switch (Config_stm32f1->PhysicalIndex) {           // determine base register and other low-level configuration data
         case 0: { // load low-level configuration of first can device 
           Config_stm32f1->BaseRegister = CAN1; // load adress of register base of CAN device
           break;
         }
         case 1: { // load low-level configuration of second can device
           Config_stm32f1->BaseRegister = CAN2; // load adress of register base of CAN device
           break;
         } 
      default: Assert_CAN(0, ttc_assert_origin_auto); break; // No TTC_CANn defined! Check your makefile.100_board_* file!
      }
    }
    
    Assert_CAN(Config_stm32f1, ttc_assert_origin_auto);
    return Config_stm32f1;
}

e_ttc_can_errorcode stm32f1_can_get_features(t_ttc_can_config* Config) {
    Assert_CAN(Config,    ttc_assert_origin_auto);
    Assert_CAN(Config->LogicalIndex > 0, ttc_assert_origin_auto);
    if (Config->LogicalIndex > TTC_CAN_AMOUNT) return ttc_assert_origin_auto;

    Config->Flags.All                        = 0;
    //Config->Flags.Bits.Master                = 1;

    Config->Layout = 1;
    Config->Bits.TimeTriggerComm = 0;
    Config->Bits.AutoBusOff = 0;
    Config->Bits.AutoWakeUp = 0;
    Config->Bits.NoAutoRetransmission = 0;
    Config->Bits.ReceivedFifoLock = 0;
    Config->Bits.TransmitFifoPriority = 0;
    Config->CAN_Synchro = CAN_Synchro_2q;
    Config->CAN_Bit1 = CAN_Bit1_11q;
    Config->CAN_Bit2 = CAN_Bit2_4q;
    Config->CAN_Prescaler = 2;
    Config->CAN_Mode = CAN_Normal_Mode;

    Config->CAN_TX_Message.CAN_Std_ID = 0x11;
    Config->CAN_TX_Message.CAN_Ext_ID = 0x11;
    Config->CAN_TX_Message.CAN_Transmit_Id = CAN_Standard;
    Config->CAN_TX_Message.CAN_Length = 8;
    Config->CAN_TX_Message.CAN_Transmit_Request = CAN_Data;

    Config->CAN_RX_Message.CAN_Std_ID = 0x00;
    Config->CAN_RX_Message.CAN_Ext_ID = 0x00;
    Config->CAN_RX_Message.CAN_Transmit_Id = CAN_Standard;
    Config->CAN_RX_Message.CAN_Length = 0;
    Config->CAN_RX_Message.CAN_Transmit_Request = CAN_Data;

    switch (Config->LogicalIndex) {           // determine features of indexed CAN device
#ifdef TTC_CAN1
      case 1: break;
#endif
      default: Assert_CAN(0, ttc_assert_origin_auto); break; // No TTC_CANn defined! Check your makefile.100_board_* file!
    }
    
    return ec_can_OK;
}

e_ttc_can_errorcode stm32f1_can_init(t_ttc_can_config* Config) {
    Assert_CAN(Config, ttc_assert_origin_auto);
    Assert_CAN(Config->LogicalIndex > 0, ttc_assert_origin_auto);
    t_stm32f1_can_config* Config_stm32f1 = stm32f1_can_get_configuration(Config);
    (void) Config_stm32f1; // avoid warning: "unused variable"
    CAN_InitTypeDef CAN_InitStructure;

    if (1) { // validate CAN_Features
        t_ttc_can_config CAN_Features;
        CAN_Features.LogicalIndex = Config->LogicalIndex;
        e_ttc_can_errorcode Error = stm32f1_can_get_features(&CAN_Features);
        if (Error) return Error;
        Config->Flags.All &= CAN_Features.Flags.All; // mask out unavailable flags

        CAN_InitStructure.CAN_TTCM = Config->Bits.TimeTriggerComm;
        CAN_InitStructure.CAN_ABOM = Config->Bits.AutoBusOff;
        CAN_InitStructure.CAN_AWUM = Config->Bits.AutoWakeUp;
        CAN_InitStructure.CAN_NART = Config->Bits.NoAutoRetransmission;
        CAN_InitStructure.CAN_RFLM = Config->Bits.ReceivedFifoLock;
        CAN_InitStructure.CAN_TXFP = Config->Bits.TransmitFifoPriority;
        CAN_InitStructure.CAN_Mode = Config->CAN_Mode;
        CAN_InitStructure.CAN_SJW = Config->CAN_Synchro;
        CAN_InitStructure.CAN_BS1 = Config->CAN_Bit1;
        CAN_InitStructure.CAN_BS2 = Config->CAN_Bit2;
        CAN_InitStructure.CAN_Prescaler = Config->CAN_Prescaler;
    }

    t_u8 PhysicalIndex = stm32f1_can_logical_2_physical_index(Config->LogicalIndex); 
    switch (PhysicalIndex) {                // find CAN corresponding to CAN_index as defined by makefile.100_board_*
#ifdef CAN1
    case 0:   
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);

        ttc_gpio_init(TTC_CAN1_PIN_RX, E_ttc_gpio_mode_input_pull_up);
        ttc_gpio_init(TTC_CAN1_PIN_TX, E_ttc_gpio_mode_alternate_function_push_pull);

        switch(Config->Layout) {
            case 0:
                GPIO_PinRemapConfig(GPIO_Remap1_CAN1, DISABLE);
                GPIO_PinRemapConfig(GPIO_Remap2_CAN1, DISABLE);
                break;
            case 1:
                GPIO_PinRemapConfig(GPIO_Remap1_CAN1, ENABLE);
                break;
            case 2:
                GPIO_PinRemapConfig(GPIO_Remap2_CAN1, ENABLE);
                break;
             default:
                Assert_CAN(0, ttc_assert_origin_auto);
                break;
            }

        CAN_Init(CAN1, &CAN_InitStructure);
        break;
#endif
#ifdef CAN2
    case 1:
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN2, ENABLE);
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);

        ttc_gpio_init(TTC_CAN2_PIN_RX, E_ttc_gpio_mode_input_pull_up);
        ttc_gpio_init(TTC_CAN2_PIN_TX, E_ttc_gpio_mode_alternate_function_push_pull);

        switch(Config->Layout) {
            case 0:
                GPIO_PinRemapConfig(GPIO_Remap_CAN2, DISABLE);
                break;
            case 1:
                GPIO_PinRemapConfig(GPIO_Remap_CAN2, ENABLE);
                break;
             default:
                Assert_CAN(0, ttc_assert_origin_auto);
                break;
            }

         CAN_Init(CAN2, &CAN_InitStructure);
        break;
#endif
      default: Assert_CAN(0, ttc_assert_origin_auto); break; // No TTC_CANn defined! Check your makefile.100_board_* file!
    }
    
    return ec_can_OK;
}

e_ttc_can_errorcode stm32f1_can_deinit(t_ttc_can_config* Config) {
    Assert_CAN(Config, ttc_assert_origin_auto);
    Assert_CAN(Config->LogicalIndex > 0, ttc_assert_origin_auto);
    t_stm32f1_can_config* Config_stm32f1 = stm32f1_can_get_configuration(Config);
    (void) Config_stm32f1; // avoid warning: "unused variable"

    // ToDo: Deinitialize hardware device
    t_u8 PhysicalIndex = stm32f1_can_logical_2_physical_index(Config->LogicalIndex);
    switch (PhysicalIndex) {                // find CAN corresponding to CAN_index as defined by makefile.100_board_*
#ifdef CAN1
    case 0:
         CAN_DeInit(CAN1);
         break;
#endif
#ifdef CAN2
    case 1:
         CAN_DeInit(CAN2);
        break;
#endif
      default: Assert_CAN(0, ttc_assert_origin_auto); break; // No TTC_CANn defined! Check your makefile.100_board_* file!
    }

    return ec_can_OK;
    return ec_can_OK;
}

t_u8 stm32f1_can_logical_2_physical_index(t_u8 LogicalIndex) {
  switch (LogicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_CAN1
           case 1: return TTC_CAN1;
#endif
#ifdef TTC_CAN2
           case 2: return TTC_CAN2;
#endif
      default: Assert_CAN(0, ttc_assert_origin_auto); break; // No TTC_CANn defined! Check your makefile.100_board_* file!
    }
    
    return -1;
}

//Additional functions goes here...

e_ttc_can_errorcode stm32f1_can_send_raw(t_ttc_can_config* Config, const char *Buffer, t_u16 Amount){
    Assert_CAN(Config, ttc_assert_origin_auto);

    while (Amount-- > 0) {
        _stm32f1_can_send_single_word(Config, (t_u16) *Buffer++);
    }

    return ec_can_OK;
}

e_ttc_can_errorcode stm32f1_can_send_string(t_ttc_can_config* Config, const char *Buffer, t_u16 MaxLength){
    Assert_CAN(Config, ttc_assert_origin_auto);

    char C=0;
    while ( (MaxLength-- > 0) && ((C=*Buffer++) != 0)  ) {
        _stm32f1_can_send_single_word(Config, (t_u16) C);
    }

    return ec_can_OK;
}

e_ttc_can_errorcode stm32f1_can_send_word(t_ttc_can_config* Config, const t_u16 Word){
    Assert_CAN(Config, ttc_assert_origin_auto);
    _stm32f1_can_send_single_word(Config, Word);
    return ec_can_OK;
}

e_ttc_can_errorcode stm32f1_can_read_word(t_ttc_can_config* Config, t_u16 *Word){
    Assert_CAN(Config, ttc_assert_origin_auto);
    CanRxMsg RxMessage;

    RxMessage.StdId = Config->CAN_RX_Message.CAN_Std_ID;
    RxMessage.ExtId = Config->CAN_RX_Message.CAN_Ext_ID;
    RxMessage.IDE = Config->CAN_RX_Message.CAN_Transmit_Id;
    RxMessage.DLC = 0;
    RxMessage.Data[0] = 0x00;
    RxMessage.Data[1] = 0x00;

    t_u8 PhysicalIndex = stm32f1_can_logical_2_physical_index(Config->LogicalIndex);

    switch(PhysicalIndex){
    case 0:
        CAN_Receive(CAN1, CAN_FIFO0, &RxMessage);
        break;
    case 1:
        CAN_Receive(CAN2, CAN_FIFO0, &RxMessage);
     break;
    default: Assert_CAN(0, ttc_assert_origin_auto);
        break;
    }

    *Word = RxMessage.Data[1]<<8;
    *Word = RxMessage.Data[0];

    return ec_can_OK;
}

e_ttc_can_errorcode stm32f1_can_read_byte(t_ttc_can_config* Config, t_u8 *Byte){
    Assert_CAN(Config, ttc_assert_origin_auto);
    CanRxMsg RxMessage;

    RxMessage.StdId = Config->CAN_RX_Message.CAN_Std_ID;
    RxMessage.ExtId = Config->CAN_RX_Message.CAN_Ext_ID;
    RxMessage.IDE = Config->CAN_RX_Message.CAN_Transmit_Id;
    RxMessage.DLC = 0;
    RxMessage.Data[0] = 0x00;
    RxMessage.Data[1] = 0x00;

    t_u8 PhysicalIndex = stm32f1_can_logical_2_physical_index(Config->LogicalIndex);

    switch(PhysicalIndex){
    case 0:
        CAN_Receive(CAN1, CAN_FIFO0, &RxMessage);
        break;
    case 1:
        CAN_Receive(CAN2, CAN_FIFO0, &RxMessage);
     break;
    default: Assert_CAN(0, ttc_assert_origin_auto);
        break;
    }

    *Byte = RxMessage.Data[0];

    return ec_can_OK;
}

e_ttc_can_errorcode stm32f1_can_load_filter_defaults(t_ttc_can_filter_config* FilterConfig){
      Assert_CAN(FilterConfig, ttc_assert_origin_auto);

      FilterConfig->CAN_FilterNumber = 0;
      FilterConfig->CAN_FilterIdHigh = 0x0000;
      FilterConfig->CAN_FilterIdLow = 0x0000;
      FilterConfig->CAN_FilterMaskHigh = 0x0000;
      FilterConfig->CAN_FilterMaskLow = 0x0000;
      FilterConfig->Bits.FilterMode = 0;
      FilterConfig->Bits.FilterScale = 1;
      FilterConfig->Bits.FIFO = 0;
      FilterConfig->Bits.Activation = 1;

      return ec_can_OK;
}

e_ttc_can_errorcode stm32f1_can_filter_init(t_ttc_can_filter_config* FilterConfig){
    Assert_CAN(FilterConfig, ttc_assert_origin_auto);
    Assert_CAN(FilterConfig->CAN_FilterNumber >= 0, ttc_assert_origin_auto);
    CAN_FilterInitTypeDef CAN_FilterInitStructure;

    CAN_FilterInitStructure.CAN_FilterNumber = FilterConfig->CAN_FilterNumber;
    CAN_FilterInitStructure.CAN_FilterMode = FilterConfig->Bits.FilterMode;
    CAN_FilterInitStructure.CAN_FilterScale = FilterConfig->Bits.FilterScale;
    CAN_FilterInitStructure.CAN_FilterIdHigh = FilterConfig->CAN_FilterIdHigh;
    CAN_FilterInitStructure.CAN_FilterIdLow = FilterConfig->CAN_FilterIdLow;
    CAN_FilterInitStructure.CAN_FilterMaskIdHigh = FilterConfig->CAN_FilterMaskHigh;
    CAN_FilterInitStructure.CAN_FilterMaskIdLow = FilterConfig->CAN_FilterMaskLow;
    CAN_FilterInitStructure.CAN_FilterFIFOAssignment = FilterConfig->Bits.FIFO;
    CAN_FilterInitStructure.CAN_FilterActivation = FilterConfig->Bits.Activation;
    CAN_FilterInit(&CAN_FilterInitStructure);

    return ec_can_OK;
}

//}FunctionDefinitions
//{ private functions (ideally) ********************************************

void _stm32f1_can_send_single_word(t_ttc_can_config* Config, t_u16 Word){
    Assert_CAN(Config, ttc_assert_origin_auto);
    CanTxMsg TxMessage;

    TxMessage.StdId = Config->CAN_TX_Message.CAN_Std_ID;
    TxMessage.ExtId = Config->CAN_TX_Message.CAN_Ext_ID;
    TxMessage.RTR = Config->CAN_TX_Message.CAN_Transmit_Request;
    TxMessage.IDE = Config->CAN_TX_Message.CAN_Transmit_Id;
    TxMessage.DLC =  2; //Config->CAN_TX_Message.CAN_Length;
    TxMessage.Data[0] = Word;
    TxMessage.Data[1] = Word>>8;

    t_u8 PhysicalIndex = stm32f1_can_logical_2_physical_index(Config->LogicalIndex);

    switch(PhysicalIndex){
    case 0:
        CAN_Transmit(CAN1, &TxMessage);
    break;
    case 1:
        CAN_Transmit(CAN2, &TxMessage);
     break;
    default: Assert_CAN(0, ttc_assert_origin_auto);
        break;
    }
}

//} private functions
