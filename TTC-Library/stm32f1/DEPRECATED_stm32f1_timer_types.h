#ifndef STM32F1_TIMER_TYPES_H
#define STM32F1_TIMER_TYPES_H

/** { DEPRECATED_stm32f1_timer.h ************************************************
 
                           The ToolChain
                      
   High-Level interface for TIMER device.

   Structures, Enums and Defines being required by ttc_timer_types.h

   Note: See ttc_timer.h for description of stm32f1 independent TIMER implementation.
  
   Authors: 

 
}*/
//{ Defines/ TypeDefs ******************************************************


//} Defines
//{ Includes ***************************************************************


#include "../ttc_basic.h"
#include "../stm32/../gpio/gpio_stm32f1xx.h"

//} Includes
//{ Structures/ Enums required by ttc_timer_types.h ***********************

typedef struct { // register description (adapt according to stm32f1 registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} TIMER_Register_t;

typedef struct {  // stm32f1 specific configuration data of single TIMER device
  register_stm32f1xx_timer_t* BaseRegister;       // base address of TIMER registers
               u8_t  PhysicalIndex;      // physical device index (1=first TIMER device, ...)
} __attribute__((__packed__)) DEPRECATED_stm32f1_timer.config_t;

// ttc_timer_arch_t is required by ttc_timer_types.h
typedef DEPRECATED_stm32f1_timer.config_t ttc_timer_arch_t;

//} Structures/ Enums


#endif //STM32F1_TIMER_TYPES_H

