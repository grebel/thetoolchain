#ifndef STM32F1_CAN_TYPES_H
#define STM32F1_CAN_TYPES_H

/** { stm32f1_can.h ************************************************
 
                           The ToolChain
                      
   High-Level interface for CAN device.

   Structures, Enums and Defines being required by ttc_can_types.h

   Note: See ttc_can.h for description of stm32f1 independent CAN implementation.
  
   Authors: 

 
}*/
//{ Defines/ TypeDefs ******************************************************


//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic.h"

//} Includes
//{ Structures/ Enums required by ttc_can_types.h ***********************

typedef struct { // register description (adapt according to stm32f1 registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} t_can_register;

typedef struct {  // stm32f1 specific configuration data of single CAN device
  CAN_TypeDef* BaseRegister;       // base address of CAN registers
               t_u8  PhysicalIndex;      // physical device index (1=first CAN device, ...)
} __attribute__((__packed__)) t_stm32f1_can_config;

// t_ttc_can_arch is required by ttc_can_types.h
typedef t_stm32f1_can_config t_ttc_can_arch;

//} Structures/ Enums


#endif //STM32F1_CAN_TYPES_H
