/** { ttc_math.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for math devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  See corresponding header file for documentation.
 *
 *  Created from template ttc_device.c revision 32 at 20150724 07:49:04 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_math.h".
//
#include "ttc_math.h"

/* gpio pins can be configured for runtime benchmarking of some functions
 *
 * Each gpio pin is set while corresponding function is running.
 * One may use an oscilloscope to measure runtime of individual functions.
 *
 * Example line which may be added to your extensions.local/makefile.700_extra_settings
 * Adjust value to match your board!:
 * COMPILE_OPTS += -DTTC_MATH_PIN_LATERATION_3D=E_ttc_gpio_pin_a1
 */
#ifdef TTC_MATH_PIN_LATERATION_2D // set during ttc_math_lateration_2d()
    #define TTC_MATH_GPIO_USED
#endif
#ifdef TTC_MATH_PIN_LATERATION_3D // set during ttc_math_lateration_3d()
    #define TTC_MATH_GPIO_USED
#endif
#ifdef TTC_MATH_PIN_ROTATE_2D
    #define TTC_MATH_GPIO_USED
#endif
#ifdef TTC_MATH_GPIO_USED
    #include "ttc_gpio.h"
#endif

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of math devices.
 *
 */

// precision setting for different approximation algorithms (e.g. ttc_math_arcsin() )
e_ttc_math_precision ttc_math_Precision      = 0;
ttm_number           ttc_math_PrecisionValue = 0;
BOOL ttc_math_lateration_2d_STOP = FALSE; // ==TRUE: stop at end of ttc_math_lateration_2d() (used by regression test to aid debugging)

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ********************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_math(t_ttc_math_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

#if (TTC_ASSERT_MATH_EXTRA == 1) // extra checks enabled: provide private function for debugger friendly implementation check

BOOL _t_ttc_math_lateration_3dest_within( ttm_number MaxDeviation, ttm_number NominalDistance, ttm_number ActualDistance ) {
    ttm_number Deviation = ttc_math_distance( NominalDistance, ActualDistance );
    if ( Deviation < MaxDeviation )
    { return TRUE; } // given actual distance matches nominal distance within given maximum deviation

    return FALSE;    // given actual distance is far away from nominal distance
}
BOOL _t_ttc_math_lateration_3dest_distance( ttm_number MaxDeviation, ttm_number NominalDistance, t_ttc_math_vector3d_xyz* A, t_ttc_math_vector3d_xyz* B ) {
    ttm_number ActualDistance = ttc_math_vector3d_distance( A, B );
    return _t_ttc_math_lateration_3dest_within( MaxDeviation, NominalDistance, ActualDistance );
}
e_ttc_math_errorcode _t_ttc_math_lateration_3dest_distances( ttm_number NominalDistanceON, ttm_number NominalDistanceF2N, ttm_number NominalDistanceF3N, ttm_number NominalDistanceF2F3, t_ttc_math_vector3d_xyz* F2, t_ttc_math_vector3d_xyz* F3, t_ttc_math_vector3d_xyz* N )  {

    struct { // placing calculated distances in a struct makes it easier to display them in GDB (one print command instead of three)
        ttm_number ON;   // distance Origin to point N
        ttm_number F2N;  // distance point F2 to point N
        ttm_number F3N;  // distance point F3 to point N
        ttm_number F2F3; // distance point F2 to point F3
    } Distances;


    // caclulate actual distances from laterated N1 to fixed points in rotated and translated coordinate system
    Distances.ON   = ttc_math_vector3d_length( N );
    Distances.F2N  = ttc_math_vector3d_distance( N, F2 );
    Distances.F3N  = ttc_math_vector3d_distance( N, F3 );
    Distances.F2F3 = ttc_math_vector3d_distance( F2, F3 );

    if ( !_t_ttc_math_lateration_3dest_within( NominalDistanceON   / 100, NominalDistanceON,   Distances.ON ) )
    { return E_ttc_math_error_lateration3d_distance_on; } // laterated position invalid (distance O to N not as required)
    if ( ! _t_ttc_math_lateration_3dest_within( NominalDistanceF2N  / 100, NominalDistanceF2N,  Distances.F2N ) )
    { return E_ttc_math_error_lateration3d_distance_f2n; } // laterated position invalid (distance N to F2 not as required)
    if ( !_t_ttc_math_lateration_3dest_within( NominalDistanceF3N  / 100, NominalDistanceF3N,  Distances.F3N ) )
    { return E_ttc_math_error_lateration3d_distance_f3n; } // laterated position invalid (distance N to F3 not as required)
    Assert_MATH( _t_ttc_math_lateration_3dest_within( NominalDistanceF2F3 / 100, NominalDistanceF2F3, Distances.F2F3 ), ttc_assert_origin_auto ); // internal error: distance F2 to F3 not as required

    return 0; // all positions seem to be valid
}

#endif

void                 ttc_math_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)

    ttc_math_set_precision( tmp_100m ); // fast precision

#ifdef TTC_MATH_PIN_LATERATION_2D
    ttc_gpio_init( TTC_MATH_PIN_LATERATION_2D, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
#endif
#ifdef TTC_MATH_PIN_LATERATION_3D
    ttc_gpio_init( TTC_MATH_PIN_LATERATION_3D, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
#endif
#ifdef TTC_MATH_PIN_ROTATE_2D
    ttc_gpio_init( TTC_MATH_PIN_ROTATE_2D, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
#endif

    _driver_math_prepare();
}
void                 ttc_math_int_multiply_32x32( t_u32 A, t_u32 B, t_u32* C32l, t_u32* C32h ) {
    /* Basic idea of this algorithm
     *
     * Multiplication of two 32 bit variables can be 64 bits wide: C64 = A32 * B32
     * We split 32 bit values into 16 bit high and low values and C64 into 32 bit high and low values:
     * A32 = A16h * 2^16 + A16l
     * B32 = B16h * 2^16 + B16l
     * C64 = C32h * 2^32 + C32l
     * This gives:
     * C32h * 2^32 + C32l = (A16h * 2^16 + A16l) * (B16h * 2^16 + B16l)
     *                    = A16h * B16h * 2^32 + (A16h*B16l + A16l*B16h) * 2^16 + A16l*B16l
     *                    = Mul32a      * 2^32 + ( Mul32b   +   Mul32c ) * 2^16 + Mul32d
     *
     * Mul32a must only be calculated if (A16h > 0) && (B16h > 0)
     * C32h = Mul32a;
     * Mul32bc = Mul32b + Mul32c;
     * if (Overflow) C32h++;
     * C32l = Mul32bc + Mul32d;
     * if (Overflow) C32h++;
     */

    t_u32 A16l = A & 0xffff;
    t_u32 A16h = ( A & 0xffff0000 ) >> 16;
    t_u32 B16l = B & 0xffff;
    t_u32 B16h = ( B & 0xffff0000 ) >> 16;

    if ( A16h && B16h ) { *C32h = A16h * B16h; } // calculate Mul32a
    else              { *C32h = 0; }

    t_u32 Mul32b = A16h * B16l;
    t_u32 Mul32c = A16l * B16h;
    t_u32 Mul32bc = Mul32b + Mul32c;
    if ( Mul32bc < Mul32b ) { ( *C32h )++; } // overflow occured

    t_u32 Mul32d = A16l * B16l;
    *C32l = Mul32bc + Mul32d;
    if ( *C32l < Mul32bc ) { ( *C32h )++; } // overflow occured
}
t_base               ttc_math_int_faculty( t_u8 Value ) {

    t_base Faculty = 1;

    while ( Value > 1 ) {
        Faculty *= Value;
        Value--;
    }

    return Faculty; // t_base
}
e_ttc_math_errorcode ttc_math_lateration_2d( ttm_number DistanceON, ttm_number DistanceFN, ttm_number Fx, ttm_number Fy, ttm_number* N1x, ttm_number* N1y, ttm_number* N2x, ttm_number* N2y ) {
    Assert_MATH( DistanceON >= 0, ttc_assert_origin_auto );  // distances are always positive! (must use signed variable for correct calculation)
    Assert_MATH( DistanceFN >= 0, ttc_assert_origin_auto );  // distances are always positive! (must use signed variable for correct calculation)

    const ttm_number Minimum_AcceptableDistance = 0.9; // abort lateration if DistanceON + DistanceFN < Minimum_AcceptableDistance * distance(O, F)
    e_ttc_math_errorcode Error = 0;

    if ( N1x ) {
        Assert_MATH_Writable( N1x, ttc_assert_origin_auto );
        Assert_MATH_Writable( N1y, ttc_assert_origin_auto );
    }
    if ( N2x ) {
        Assert_MATH_Writable( N2x, ttc_assert_origin_auto );
        Assert_MATH_Writable( N2y, ttc_assert_origin_auto );
    }

    if ( ttc_math_lateration_2d_STOP )
    { ttc_assert_halt_origin( ttc_assert_origin_auto ); }

#ifdef TTC_MATH_PIN_LATERATION_2D
    ttc_task_critical_begin();
    ttc_gpio_set( TTC_MATH_PIN_LATERATION_2D );
#endif

    if ( ( Fx == TTC_MATH_CONST_NAN ) || ( Fx == TTC_MATH_CONST_NAN ) ) { // invalid coordinates given: abort calculation
        Error = E_ttc_math_error_NotANumber;
        goto tml2_Failed;
    }

    ttm_number Nx, Ny, Alpha;
    if ( ( Fx != 0 ) && ( Fy != 0 ) ) {            // general case: non-orthogonal line O->F must be calculated
        /* Idea:
         * 1) Calculate angle alpha between positive y-axis and O->F.
         * 2) Rotate F=(Fx, Fy) -> F_r=(0, Fy_r) by angle -alpha (O->F becomes vertical)
         * 3) laterate simple case O->F is vertical          -> N_r=(*Nx,*Ny)
         * 4) Calculate angle beta between O->F and O->N
         * 5) Rotate N_r=(*Nx,*Ny) to angle alpha + beta     -> N=(*Nx, *Ny)
         *    *Nx = DistanceON * sin(alpha + beta)
         *    *Ny = DistanceON * cos(alpha + beta)
         * 6) Return N = (*Nx, *Ny)
         *
         *                                  A +Y
         *                    x F=(Fx,Fy)   |
         *                   _/\            |
         *       DistanceFN_/   \DistanceOF |
         *               _/      \          |
         *    (Nx,Ny)=N x         \         |
         *               \_        \        |
         *                 \_       \    ___|
         *                   \_    __\<--  +|
         *                     \_<-  +\    a|
         *            DistanceON \_   b\   l|
         *                         \_  e\  p|
         *                           \_ t\ h|
         *                             \_a\a|
         *                               \_\|
         *     <----------------------------O------>
         *     -X                         (0,0)   +X
         */

        // calculate rotation angle alpha between O->Y and O->F
        Alpha = ttc_math_vector2d_angle( Fx, Fy ) - PI / 2; // inverting angle because ttc_math_vector2d_angle() calculates angles relative to x-axis instead of y-axis

        // ensure that Alpha is from ]-PI, PI] for faster calculations (ttc_math functions otherwise have to reduce it on every call)
        Alpha = ttc_math_angle_reduce( Alpha );

        // rotate F to (0, DistanceOF)
        Fy = ttc_math_sqrt( Fx * Fx + Fy * Fy );
        Fx = 0;
    }
    else { Alpha = 0; }

    if ( Fx == 0 ) { // simple case:  line O->F is vertical
        Assert_MATH( Fy != 0, ttc_assert_origin_auto );  // fixpoint must not be the origin!
        ttm_number DistanceOF  = ttc_math_abs( Fy ); // simple calculation for simple case
        ttm_number DistanceONF = DistanceON + DistanceFN;
        ttm_number DistanceOFN = DistanceOF + DistanceFN;

        if ( ( DistanceONF <= DistanceOF ) ||  // N lies on line O->F between O and F
                ( DistanceOFN <= DistanceON )  // N lies on line O->F outside O and F
           ) { // simple special case: C lies on vertical line O->F

            Nx = 0;
            if ( ( DistanceONF == DistanceOF ) ||
                    ( DistanceOFN == DistanceON )
               ) { // exact case: calculate Ny exactly
                if ( Fy < 0 )
                { Ny = -DistanceON; }
                else
                { Ny =  DistanceON; }
            }
            else {                                         // approximate Ny
                if ( DistanceONF < DistanceOF * Minimum_AcceptableDistance ) { // given distances are way too small: abort lateration
                    Error = E_ttc_math_error_lateration2d_distances_small;
                    goto tml2_Failed;
                }
                ttm_number Nya; ttm_number Nyb;
                if ( DistanceONF <= DistanceOF ) {  // N lies on line O->F between O and F
                    if ( Fy < 0 ) {
                        Nya =  0 - DistanceON;   // Ny calculated from O
                        Nyb = Fy + DistanceFN;   // Ny calculated from F
                    }
                    else {
                        Nya =  0 + DistanceON;   // Ny calculated from O
                        Nyb = Fy - DistanceFN;   // Ny calculated from F
                    }
                }
                else {                              // N lies on line O->F outside O and F
                    if ( Fy < 0 ) {
                        Nya =  0 - DistanceON;   // Ny calculated from O
                        Nyb = Fy - DistanceFN;   // Ny calculated from F
                    }
                    else {
                        Nya =  0 + DistanceON;   // Ny calculated from O
                        Nyb = Fy + DistanceFN;   // Ny calculated from F
                    }
                }
                Ny = ( Nya + Nyb ) / 2;
            }
        }
        else {                                         // simple general case: N is outside vertical line O->F
            // Ny = ( Fy² - DistanceFN² + DistanceON² ) / ( 2 * Fy )
            // Nx = sqrt( DistanceON² + Ny² )

            ttm_number DistanceON2 = DistanceON * DistanceON;
            Ny = ( Fy * Fy - DistanceFN * DistanceFN + DistanceON2 ) / ( 2 * Fy );
            ttm_number N_y2 = Ny * Ny;
            Nx = ( DistanceON2 > N_y2 ) ? ttc_math_sqrt( DistanceON2 - N_y2 ) : 0;
            if ( Fy >= 0 ) { // vector O->F goes upwards: put Nx on left side of vector O->F
                Nx = -Nx;
            }
        }
        if ( N1x ) {  // (*N1x, *N1y) is placed on left side of vector O->F
            *N1x =  Nx;
            *N1y =  Ny;
        }
        if ( N2x ) {  // (*N1x, *N1y) is placed on right side of vector O->F
            *N2x = -Nx;
            *N2y =  Ny;
        }
    }
    else {           // simple case:  line O->F is horizontal
        Assert_MATH( Fx != 0, ttc_assert_origin_auto );  // fixpoint must not be the origin!
        ttm_number DistanceOF  = ttc_math_abs( Fx ); // simple calculation for simple case
        ttm_number DistanceONF = DistanceON + DistanceFN;
        ttm_number DistanceOFN = DistanceOF + DistanceFN;

        if ( ( DistanceONF <= DistanceOF ) ||  // N lies on line O->F between O and F
                ( DistanceOFN <= DistanceON )  // N lies on line O->F outside O and F
           ) { // simple special case: C lies on horizontal line O->F
            Ny = 0;

            if ( ( DistanceONF == DistanceOF ) ||
                    ( DistanceOFN == DistanceON )
               ) { // exact case: calculate Nx exactly
                if ( Fx < 0 )
                { Nx = -DistanceON; }
                else
                { Nx =  DistanceON; }
            }
            else {                                         // approximate Nx
                if ( DistanceONF < DistanceOF * Minimum_AcceptableDistance ) { // given distances are way too small: abort lateration
                    Error = E_ttc_math_error_lateration2d_distances_small;
                    goto tml2_Failed;
                }

                ttm_number Nxa; ttm_number Nxb;
                if ( DistanceONF <= DistanceOF ) {  // N lies on line O->F between O and F
                    if ( Fx < 0 ) {
                        Nxa =  0 - DistanceON;   // Nx calculated from O
                        Nxb = Fx + DistanceFN;   // Nx calculated from F
                    }
                    else {
                        Nxa =  0 + DistanceON;   // Nx calculated from O
                        Nxb = Fx - DistanceFN;   // Nx calculated from F
                    }
                }
                else {                              // N lies on line O->F outside O and F
                    if ( Fx < 0 ) {
                        Nxa =  0 - DistanceON;   // Nx calculated from O
                        Nxb = Fx - DistanceFN;   // Nx calculated from F
                    }
                    else {
                        Nxa =  0 + DistanceON;   // Nx calculated from O
                        Nxb = Fx + DistanceFN;   // Nx calculated from F
                    }
                }
                Nx = ( Nxa + Nxb ) / 2;
            }
        }
        else {                                 // simple general case: N is outside horizontal line O->F
            // Nx = ( Fx² - DistanceFN² + DistanceON² ) / ( 2 * Fx )
            // Ny = sqrt( DistanceON² + Nx² )

            ttm_number DistanceON2 = DistanceON * DistanceON;
            Nx = ( Fx * Fx - DistanceFN * DistanceFN + DistanceON2 ) / ( 2 * Fx );
            ttm_number N_x2 = Nx * Nx;
            Ny = ( DistanceON2 > N_x2 ) ? ttc_math_sqrt( DistanceON2 - N_x2 ) : 0;

            if ( Fx >= 0 ) { // vector O->F goes to the right: put Nx on left side of vector O->F
                Ny = -Ny; //???
            }
        }
        if ( N1x ) {  // (*N1x, *N1y) is placed on left side of vector O->F
            *N1x =  Nx;
            *N1y =  Ny;
        }
        if ( N2x ) {  // (*N1x, *N1y) is placed on right side of vector O->F
            *N2x =  Nx;
            *N2y = -Ny;
        }
    }

    if ( Alpha ) { // coordinate system has been rotated: rotate N=(Nx, Ny) to final position
        // subtracting 90° from angle because ttc_math_vector2d_angle() calculates angles relative to x-axis instead of y-axis
        ttm_number Beta = ttc_math_angle_reduce( ttc_math_vector2d_angle( Nx, Ny ) - PI / 2 );

        if ( N1x )  { // rotate N_r to leftside N
            // Final rotation angle in original coordinate system
            ttm_number Gamma = ttc_math_angle_reduce( ( Alpha + Beta ) );

            *N1x = DistanceON * ttc_math_sin( -Gamma );
            *N1y = DistanceON * ttc_math_cos( -Gamma );
        }
        if ( N2x ) { // rotate N_r to rightside N
            // Final rotation angle in original coordinate system
            ttm_number Gamma = ttc_math_angle_reduce( ( Alpha - Beta ) );

            *N2x = DistanceON * ttc_math_sin( -Gamma );
            *N2y = DistanceON * ttc_math_cos( -Gamma );
        }
    }

#ifdef TTC_MATH_PIN_LATERATION_2D
    ttc_gpio_clr( TTC_MATH_PIN_LATERATION_2D );
    ttc_task_critical_end();
#endif
    return 0;

tml2_Failed:
#ifdef TTC_MATH_PIN_LATERATION_2D
    ttc_gpio_clr( TTC_MATH_PIN_LATERATION_2D );
    ttc_task_critical_end();
#endif
    if ( Error )
    { return Error; }

    return E_ttc_math_error_lateration2d; // specific error code not known

}
e_ttc_math_errorcode ttc_math_lateration_3d( t_ttc_math_lateration_3d* Rotated, BOOL FixedNodesUnchanged, t_ttc_math_vector3d_xyz* F2, t_ttc_math_vector3d_xyz* F3, ttm_number DistanceON, ttm_number DistanceF2N, ttm_number DistanceF3N, t_ttc_math_vector3d_xyz* N1, t_ttc_math_vector3d_xyz* N2 ) {
    VOLATIL_MATH e_ttc_math_errorcode Error = 0;

    // configure algorithm
    const BOOL Use_DirectCalculation      = 1; // ==1: enable direct calculation of final rotation angle ROTy (fixed runtime but not precise egnough in some scenarios)
    const BOOL Use_QuadraticApproximation = 1; // ==1: enable quadratic approximation of final rotation angle ROTy (variable runtim but increased precision of calculated result)

#ifdef TTC_MATH_PIN_LATERATION_3D
    ttc_task_critical_begin();
    ttc_gpio_set( TTC_MATH_PIN_LATERATION_3D );
#endif

    Assert_MATH_Writable( Rotated, ttc_assert_origin_auto );
    Assert_MATH_Writable( N1, ttc_assert_origin_auto );  // always check incoming pointer arguments

#if (TTC_ASSERT_MATH_EXTRA == 1) // extra checks enabled: ensure that distance F2-F3 does not change during rotations
    ttm_number DistanceF2F3 = ttc_math_vector3d_distance( F2, F3 );
    ttm_number DistanceF2F3_MaxDeviation = DistanceF2F3 / 1000.0; // maximum allowed variation during rotations

    // reset N1,N2 to avoid reusing vector from previous run (allows to detect some special bugs)
    if ( N1 ) { ttc_memory_set( N1, 0, sizeof( *N1 ) ); }
    if ( N2 ) { ttc_memory_set( N2, 0, sizeof( *N2 ) ); }
#endif

    if ( ! FixedNodesUnchanged ) { // rotate coordinate system so that F2 = (0,F2.y,0) and F3 = (F3.x,F3.y, 0)
        Assert_MATH_Readable( F2, ttc_assert_origin_auto );  // always check incoming pointer arguments
        Assert_MATH_Readable( F3, ttc_assert_origin_auto );  // always check incoming pointer arguments

        // copy integer values into floating point variables before rotating coordinaste system
        ttc_memory_copy( &Rotated->F2, F2, sizeof( Rotated->F2 ) );
        ttc_memory_copy( &Rotated->F3, F3, sizeof( Rotated->F3 ) );

        // calculate distances from origin to fixed nodes
        Rotated->DistanceOF2 = ttc_math_vector3d_length( &( Rotated->F2 ) );
        Rotated->DistanceOF3 = ttc_math_vector3d_length( &( Rotated->F3 ) );

#if (TTC_ASSERT_MATH_EXTRA == 1) // extra checks enabled: check if RotationX has been calculated correctly
        t_ttc_math_vector3d_xyz TestF2, TestF3; // vectors are rotated using ttc_math_vector3d_rotate_*() which is slower than the tricky implementation to check if all tricks calculate correct coordinates
        ttc_memory_copy( &TestF2, F2, sizeof( TestF2 ) );
        ttc_memory_copy( &TestF3, F3, sizeof( TestF3 ) );
#endif
        if ( Rotated->F2.Z != 0 ) { // rotate F2,F3 around x-axis until F2 = (F2.x, F2.y, 0)
            // calculate rotation angle required so that F2 becomes (F2.x, F2.y, 0)
            Rotated->RotationX = - ttc_math_vector2d_angle( Rotated->F2.Y, Rotated->F2.Z );

            if ( 1 ) { // rotate F2 by RotationX around x-axis so that F2 = (F2.x, F2.y, 0)
                Rotated->F2.Y = ttc_math_length_3d( Rotated->F2.Y, Rotated->F2.Z, 0 ); // don't need cos() as target angle is 0°
                Rotated->F2.Z = 0;
            }

            // rotate F3 by RotationX around x-axis
            // calculate angle and length of 2D projection O->(F3.Z, F3.Y)
            ttm_number ProjectedAngle, ProjectedLength;
            ttc_math_cartesian2polar( Rotated->F3.Y, Rotated->F3.Z, &ProjectedAngle, &ProjectedLength );

            // calculate polar angle of F3 after rotation around x-axis
            ProjectedAngle += Rotated->RotationX;

            // calculate rotated coordinates
            ttc_math_polar2cartesian( ProjectedAngle, ProjectedLength, &Rotated->F3.Y, &Rotated->F3.Z );

#if (TTC_ASSERT_MATH_EXTRA == 1) // extra checks enabled: check if RotationX has been calculated correctly
            _t_ttc_math_lateration_3dest_distance( DistanceF2F3_MaxDeviation, DistanceF2F3, &Rotated->F2, &Rotated->F3 );

            ttc_math_vector3d_rotate_xzy( &TestF2, Rotated->RotationX, 0, 0 );
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF2.X, Rotated->F2.X ); // rotated vector does not match to calculated RoationX: Check implementation above!
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF2.Y, Rotated->F2.Y ); // rotated vector does not match to calculated RoationX: Check implementation above!
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF2.Z, Rotated->F2.Z ); // rotated vector does not match to calculated RoationX: Check implementation above!

            ttc_math_vector3d_rotate_xzy( &TestF3, Rotated->RotationX, 0, 0 );
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF3.X, Rotated->F3.X ); // rotated vector does not match to calculated RoationX: Check implementation above!
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF3.Y, Rotated->F3.Y ); // rotated vector does not match to calculated RoationX: Check implementation above!
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF3.Z, Rotated->F3.Z ); // rotated vector does not match to calculated RoationX: Check implementation above!
#endif
        }
        else {
            Rotated->RotationX = 0;
        }
        if ( Rotated->F2.X != 0 ) { // rotate F2,F3 around z-axis so that F2 lies on y-axis

            // calculate rotation angle around z-axis: F2=(F2.X, F2.Y,0) => F2=(0,F2.y,0)
            Rotated->RotationZ = - ttc_math_vector2d_angle( Rotated->F2.Y, Rotated->F2.X );

            // F2 now lies on y-axis: F2 = (0, F2.y, 0)
            Rotated->F2.X = 0;
            Rotated->F2.Y = Rotated->DistanceOF2; // already calculated above

            // rotate F3 by RotationZ around z-axis
            // calculate current polar angle of F3 projected onto XY-plane
            ttm_number ProjectedAngle, ProjectedLength;
            ttc_math_cartesian2polar( Rotated->F3.Y, Rotated->F3.X, &ProjectedAngle, &ProjectedLength );

            // calculate polar angle of F3 after rotation around z-axis
            ProjectedAngle += Rotated->RotationZ;

            // calculate rotated coordinates projected onto XY-plane
            ttc_math_polar2cartesian( ProjectedAngle, ProjectedLength, &Rotated->F3.Y, &Rotated->F3.X );

#if (TTC_ASSERT_MATH_EXTRA == 1) // extra checks enabled: check if RotationX has been calculated correctly
            _t_ttc_math_lateration_3dest_distance( DistanceF2F3_MaxDeviation, DistanceF2F3, &Rotated->F2, &Rotated->F3 );

            ttc_math_vector3d_rotate_xzy( &TestF2, 0, 0, Rotated->RotationZ );
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF2.X, Rotated->F2.X ); // rotated vector does not match to calculated RoationX: Check implementation above!
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF2.Y, Rotated->F2.Y ); // rotated vector does not match to calculated RoationX: Check implementation above!
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF2.Z, Rotated->F2.Z ); // rotated vector does not match to calculated RoationX: Check implementation above!

            ttc_math_vector3d_rotate_xzy( &TestF3, 0, 0, Rotated->RotationZ );
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF3.X, Rotated->F3.X ); // rotated vector does not match to calculated RoationX: Check implementation above!
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF3.Y, Rotated->F3.Y ); // rotated vector does not match to calculated RoationX: Check implementation above!
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF3.Z, Rotated->F3.Z ); // rotated vector does not match to calculated RoationX: Check implementation above!
#endif
        }
        else {
            Rotated->RotationZ   = 0;
        }
        if ( Rotated->F3.Z != 0 ) { // rotate F3 around y-axis so that F3 lies in XY-plane

            // calculate angle to rotate F3 around y-axis so that F3 = (F3.x, F3.y, 0)
            Rotated->RotationY = - ttc_math_vector2d_angle( Rotated->F3.X, Rotated->F3.Z );

            ttm_number ProjectedLength = _driver_math_length_2d( Rotated->F3.X, Rotated->F3.Z );

            // target rotation angle around y-axis is zero: no sinus required
            Rotated->F3.X = ProjectedLength;
            Rotated->F3.Z = 0;

#if (TTC_ASSERT_MATH_EXTRA == 1) // extra checks enabled: check if RotationX has been calculated correctly
            _t_ttc_math_lateration_3dest_distance( DistanceF2F3_MaxDeviation, DistanceF2F3, &Rotated->F2, &Rotated->F3 );

            ttc_math_vector3d_rotate_xzy( &TestF2, 0, Rotated->RotationY, 0 );
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF2.X, Rotated->F2.X ); // rotated vector does not match to calculated RoationX: Check implementation above!
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF2.Y, Rotated->F2.Y ); // rotated vector does not match to calculated RoationX: Check implementation above!
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF2.Z, Rotated->F2.Z ); // rotated vector does not match to calculated RoationX: Check implementation above!

            ttc_math_vector3d_rotate_xzy( &TestF3, 0, Rotated->RotationY, 0 );
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF3.X, Rotated->F3.X ); // rotated vector does not match to calculated RoationX: Check implementation above!
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF3.Y, Rotated->F3.Y ); // rotated vector does not match to calculated RoationX: Check implementation above!
            _t_ttc_math_lateration_3dest_within( ttc_math_Precision, TestF3.Z, Rotated->F3.Z ); // rotated vector does not match to calculated RoationX: Check implementation above!
#endif
        }
        else {
            Rotated->RotationY = 0;
        }
    }

#if (TTC_ASSERT_MATH_EXTRA == 1)
    static volatile struct { // statistic data of Approach 2

        // values from Approach 1
        ttm_number ROTy_1;
        ttm_number dF2N1;
        ttm_number dF3N1;
        ttm_number dON1;

        // values from Approach 2
        ttm_number ROTy_2;
        ttm_number dF2N2;
        ttm_number dF3N2;
        ttm_number dON2;

        // statistics of runtime and precision enhancements of Approach 2
        ttm_number Change_ROTy_Degree;  // change of rotation angle (degrees)
        ttm_number Change_DistanceF3N;  // improvement of distance(F3, N) during approach 2
        // == abs(dF3N1-DistanceF3N) - abs(dF3N2-DistanceF3N)
        t_u16      AmountSteps;         // amount of iteration steps required

        // maximum seen values
        ttm_number Max_Change_ROTy_Degree;
        ttm_number Max_Change_DistanceF3N;
    } tml3_Approach2Stats __attribute__( ( section( ".bss" ) ) ); // have struct being reset to 0 at startup
#endif

    if ( 1 )  { // calculate N1 in rotated and translated coordinate system
        /* Rotated and translated coordinate system
         *
         * All three fixed points now lie in XY-plane:
         * F1 = O = Origin = (0,0,0)
         * F2 = (0,F2.Y,0)
         * F3 = (F3.X,F3.Z,0)
         *
         * N1 and N2 will be laterated in this coordinate system.
         * N1 = (N1x,N1y,N1z)
         * N2 = (N1x,N1y,-N1z)
         * N1 and N2 are rotated back to original coordinate system afterwards.
         *
         *                +z A      +y
         *                   |     /
         *                   |    x F2
         *                   |   /  x N1
         *                   |  /
         *                   | /    x F3
         *                   |/
         *        <----------+---------->
         *                  /|O         +x
         *                 / |
         *                /  |
         *               /   |       x N2
         *              /    |
         *             /     |
         *                   V
         */
        // check if all fixed points lie within XY-plane
        Assert_MATH_EXTRA( Rotated->F2.X == 0, ttc_assert_origin_auto );  // F2 must lie on Y-axis. Check implementation of coordinate system rotation above!
        Assert_MATH_EXTRA( Rotated->F2.Z == 0, ttc_assert_origin_auto );  // F2 must lie on Y-axis. Check implementation of coordinate system rotation above!
        Assert_MATH_EXTRA( Rotated->F3.Z == 0, ttc_assert_origin_auto );  // F3 must lie inside XY-plane. Check implementation of coordinate system rotation above!
        if ( Rotated->F3.X == 0 ) { // F3 must not be colinear with F2! (Can only rotate (0, N1_y, N1_.z) around y-axis if does not F3 lie on y-axis.) Make sure that F3 != k * F2!
            Error = E_ttc_math_error_lateration3d_colinear_f2_f3;
            goto tml3_Failed;
        }
        /* step 1: laterate N1_ in YZ-plane with z > 0 (easy task)
         *
         *                +z A      +y
         *                   |     /
         *                   |N1_ x F2
         *                   | x /  x N1
         *                   |  /
         *                   | /    x F3
         *                   |/
         *        <----------+---------->
         *                  /|O         +x
         *                 / |
         *             N2 /x |
         *               /   |
         *              /    |
         *             /     |
         *                   V
         */
        Error = ttc_math_lateration_2d( DistanceON, DistanceF2N, Rotated->F2.Y, 0, & N1->Y, & N1->Z, NULL, NULL );
        if ( Error ) {
            goto tml3_Failed;
        }

        if ( N1->Z < 0 ) { // we're looking for N.Y >= 0: using second solution (which is trivial to find in our rotated coordinate system)
            N1->Z = -N1->Z;
        }
        N1->X = 0; // N1_ lies in YZ-plane

        if ( N1->Z != 0 ) { // step 2: vector has nonzero length: rotate to final position

            /* step 2: rotate N1_ by angle ROTy around Y-axis so that distance(N1, F3) == DistanceF3N
             *
             *       A              / Y
             *     Z |   N1_       /
             *       |  o-------_ / -ROTy
             *       | /|\       ---__
             *       |/ | \     /     ->o N1
             * N1_.z |  |  \   /   __-_/ \.
             *       |  |   \ / _-- _/    \   N1->F3
             *       |  |    o--  _/       \ |N1->F3| == Distance_F3N
             *       |  |   / F2_/          \.
             *       |  |  /  _/             \.
             *       |  | / _/                o F3
             *       |  |/_/
             *       |  /
             *       | / N1_.y
             *       |/
             *  <----o--------------------------->
             *      /|O=(0,0,0)                   X
             *     / |
             *    /  |
             *       V
             *
             * N1_    = (N1_x, N1_y, N1_.z)
             * ROTy   = rotation angle of N1_ around y
             * L      = N1_.z (vertical length of N1_ being projected onto XY-plane)
             * N1.x   = L * sin(-ROTy)
             * N1.z   = L * cos(-ROTy)
             * N1.y   = N1_.y (unchanged during rotation)
             * N1->F3 = vector from N1 to F3
             *        = O->N1 - O->F3
             *        = ( L * sin(ROTy) - F3.y, N1_.y - F3.y, L * cos(ROTy) - F3.z)
             *
             * Finding ROTy requires some trigonometry terms
             * |N1->F3| = 3D distance from N1 to F3
             *          = length of vector N1->F3
             *          = sqrt( (N1.x - F3.x)² + (N1.y - F3.y)² + (N1.z - F3.z)² )
             * <=>
             * |N1->F3|²= (L * sin(ROTy) - F3.x)² +
             *            (L * cos(ROTy) - F3.z)² +
             *            (N1_.y - F3.y)²
             *
             * |N1->F3| = DistanceF3N, F3.z = 0:
             *
             * Approach 1 (Direct Calculation):
             *    DistanceF3N² = L² * sin²(ROTy) - 2 * L * F3.x * sin(ROTy) + F3.x² +
             *                   L² * cos²(ROTy)                                    +
             *                   (N1_.y - F3.y)²
             *
             *    <=> DistanceF3N² - F3.x² - (N1_.y - F3.y)²
             *      = L² * sin²(ROTy) - 2 * L * F3.x * sin(ROTy) + L² * cos²(ROTy)
             *
             *    sin²(x) = 1-cos²(x):
             *      = L² * (1 - cos²(ROTy))  - 2 * L * F3.x * sin(ROTy) + L² * cos²(ROTy)
             *           __________________                             _________________
             *      = L² -  L² * cos²(ROTy)  - 2 * L * F3.x * sin(ROTy) + L² * cos²(ROTy)
             *      = L²                     - 2 * L * F3.x * sin(ROTy)
             *
             *    L = N1_.z:
             *    <=> ( DistanceF3N² - F3.x² - (N1_.y - F3.y)² - N1_.z² )
             *       = - 2 * N1_.z * F3.x * sin(ROTy)
             *
             *    <=> ( DistanceF3N² - F3.x² - (N1_.y - F3.y)² - N1_.z² ) / (- 2 * F3.x * N1_.z )
             *      = sin(ROTy)
             *
             *    C = ( F3.x² + (N1_.y - F3.y)² + N1_.z² - DistanceF3N²) / (2 * F3.x * N1_.z):
             *        sin(ROTy) = C
             *     => ROTy = arcsin( C )
             *
             *    Pro: Constant Runtime
             *    Con: Not yet precise egnough
             *
             * Approach 2 (Approximation):
             *    Idea: Try different rotation angles and compare their error square (avoids square root calculation)
             *
             *    Start with simple rotations +0°,+90°,-90° to decide which rotation is to be tried next
             *
             *    Pro: Adjustable Precision (stop when error square < square of maximum allowed distance error
             *    Con: Long, non-constant runtime
             *
             * Combined Approach:
             *    Start with Approach 1
             *    Use Approach 2 to improve precision
             */

            ttm_number ROTy;          // angle to rotate N1_=(0,N1_y, N1_z) to final position in rotated coordinate system
            ttm_number N1_z = N1->Z;  // N1->Z will change several times until final ROTy is found

            ttm_number DistanceF3N2 = DistanceF3N * DistanceF3N; // DistanceF2n² is required several times
            ttm_number AngleStepSize;
            if ( Use_DirectCalculation ) { // Approach 1: Directly calculate ROTy
                ttm_number C = N1->Y - Rotated->F3.Y;

                // C = (N1_.y - F3.y)²
                C = C * C;

                // C = F3.x² + (N1_.y - F3.y)² + N1_.z² - DistanceF3N²
                C = ( Rotated->F3.X * Rotated->F3.X ) + C + ( N1->Z * N1->Z ) - DistanceF3N2;

                // C = ( F3.x² + (N1_.y - F3.y)² + N1_.z² - DistanceF3N²) / (2 * F3.x * N1_.z)
                C /= ( 2 * Rotated->F3.X * N1->Z );

                /* Check if angle can be computed
                *
                * Two situations can make it impossible to calculate ROTy:
                * 1) distance(N1_max, F3) < DistanceF3N
                *    Distance of N1_ rotated to most far position is not long egnough.
                *    => C < -1
                *    => ROTy = PI / 2, N1 = (-N1_.z, N1_.y, 0) (farest position to F3)
                * 2) distance(N1_min, F3) > DistanceF3N
                *    Distance of N1_ rotated to nearest position is not short egnough
                *    => C > 1
                *    => ROTy = -PI / 2, N1 = (N1_.z, N1_.y, 0) (nearest position to F3)
                */
                if ( ( C <= -1 ) || ( C >= 1 ) ) { // rotate to most far away or nearest position
                    /* Idea: compare distance between F3 and two extreme rotated positions of N1
                    * N1r=(+N1_->Z, N1_->Y, 0)  == N1_ rotated by -90°
                    * N1l=(-N1_->Z, N1_->Y, 0)  == N1_ rotated by +90°
                    *
                    * Instead of calculating real distance, we calculate its square to avoid
                    * time consuming square root calculation:
                    *    Error1       = sqrt( abs( (F3.x-N1r.x)² + (F3.y-N1r.y)² + (F3.z-0)² - DistanceF3N² ) )
                    * => ErrorSquare1 = abs( (F3.x-N1r.x)² + (F3.y-N1r.y)² + (F3.z-0)² - DistanceF3N² )
                    *    Error2       = sqrt( abs( (F3.x-N1l.x)² + (F3.y-N1l.y)² + (F3.z-0)² - DistanceF3N² ) )
                    * => ErrorSquare2 = abs( (F3.x-N1l.x)² + (F3.y-N1l.y)² + (F3.z-0)² - DistanceF3N² )
                    */

                    // calculate distance sqaure from N1r to F3
                    ttm_number DX = Rotated->F3.X - N1->Z; // N1->X = +N1->Z (ROTy = -90°)
                    ttm_number DY = Rotated->F3.Y - N1->Y;
                    //ttm_number DZ = Rotated->F3.Z - N1->Z; // N1-Z == 0
                    ttm_number ErrorSquare1 = DX * DX + DY * DY + Rotated->F3.Z * Rotated->F3.Z - DistanceF3N2;

                    // calculate distance square from N1l to F3
                    DX = Rotated->F3.X + N1->Z;            // N1->X = -N1->Z (ROTy = +90°)
                    ttm_number ErrorSquare2 = DX * DX + DY * DY + Rotated->F3.Z * Rotated->F3.Z - DistanceF3N2;

                    // both values may get negative due to calculation errors
                    ErrorSquare1 = ttc_math_abs( ErrorSquare1 );
                    ErrorSquare2 = ttc_math_abs( ErrorSquare2 );

                    if ( ErrorSquare1 < ErrorSquare2 ) // select solution with smallest error
                    { N1->X = +N1->Z; } // ROTy = + 0°
                    else
                    { N1->X = -N1->Z; } // ROTy = + 180°
                    ROTy  = 0;          // disable further rotation via ttc_math_polar2cartesian() below
                    N1->Z = 0;          // this component is easy to calculate
                    AngleStepSize = 0;  // disable quadratic approximation (no precision enhancement possible)
                }
                else { // value within input range of arcsin(): calculate rotation angle
                    ROTy = PI / 2 - ttc_math_arcsin( C );
                    AngleStepSize = 2 * PI / 180; // enable quadratic approximation to enhance precision of calculated ROTy
                }
            }
            else {                         // Direct computation disabled: use center angle as starting point for quadratic approximation
                ROTy = 90 * PI / 180;          // start at +0° (coordinate system is rotated +90°)
                AngleStepSize = 90 * PI / 180; // current size of search sector will half in every turn
            }

            if ( Use_QuadraticApproximation &&
                    ( AngleStepSize > 0 )
               ) { // Approach 2: use quadratic approximation scheme to improve precision (can also be used if direct calculation is disabled)
                // Problem: ROTy calculated by Approach 1 was observed to be too imprecise to pass regression test in example_ttc_math

#if (TTC_ASSERT_MATH_EXTRA == 1)
                if ( 1 ) { // collect results from approach 1 and 2 for fine tuning
                    ttc_memory_copy( N2, N1, sizeof( *N2 ) );
                    tml3_Approach2Stats.ROTy_1 = ROTy;
                    ttc_math_polar2cartesian( ROTy, N1_z, &N2->X, &N2->Z );
                    tml3_Approach2Stats.dF2N1 = ttc_math_vector3d_distance( N2, &Rotated ->F2 );
                    tml3_Approach2Stats.dF3N1 = ttc_math_vector3d_distance( N2, &Rotated ->F3 );
                    tml3_Approach2Stats.dON1  = ttc_math_vector3d_length( N2 );
                }
#endif

                BOOL InvertSearch = ( Rotated->F3.X < 0 ) ? 1 : 0; // sign of sector search must be inverted if F3 lies in left side of coordinate system

                // using N2 as test vector
                N2->Y = N1->Y;                          // will stay constant during roations
                ttm_number DY2 = Rotated->F3.Y - N2->Y;
                DY2 = DY2 * DY2;                        // DY² = (F3.y - N1_.y)²  will stay constant during rotations

                // initialize loop variables
                ttm_number DeltaSquares;
                ttm_number CurrentDeltaSquareError;
                ttm_number DeltaSquareError        = TTC_MATH_CONST_PLUS_INFINITY;
                ttm_number CurrentROTy             = ROTy;

#if (TTC_ASSERT_MATH_EXTRA == 1)
                tml3_Approach2Stats.AmountSteps = 0;
#endif

                /* Calculate target value for DeltaSquareError (loop may exit if sum of squares - DistanceF3N² is <= target square error)
                 *
                 * Definition: N1r = N1 rotated around y-axis by angle ROTy to a position that meets all given distances DistanceON, DistanceF2N, DistanceF3N
                 *
                 * Basic idea: ROTy is approximated good egnough if
                 *             (1)  abs( distance(N1r, F3) - DistanceF3N) < DistanceF3N * ttc_math_PrecisionValue
                 *
                 *             Calculating distance(N1r, F3) in every loop iteration would be slow (sqrt() is approximated itself).
                 *             Instead we calculate
                 *             (2)  D = distance(N1r, F3)² = (N1r.x - F3.x)² + (N1r.y - F3->y)² + (N1r.z - F3.z)²
                 *
                 *             So we square equation (1):
                 *             ( distance(N1r, F3) - DistanceF3N)² < DistanceF3N² * ttc_math_PrecisionValue²
                 *         <=> distance(N1r, F3)² - 2*distance(N1r, F3)*DistanceF3N + DistanceF3N² < DistanceF3N² * ttc_math_PrecisionValue²
                 *             Trick: As we are optimizing ROTy to become the position where distance(N1r, F3) == DistanceF3N, we simplify to
                 *          => distance(N1r, F3)² - 2*DistanceF3N² + DistanceF3N² < DistanceF3N² * ttc_math_PrecisionValue²
                 *         <=> distance(N1r, F3)² - DistanceF3N² < DistanceF3N² * ttc_math_PrecisionValue²
                 *
                 *             Written in variables:
                 *             TargetDeltaSquareError = DistanceF3N² * ttc_math_PrecisionValue²
                 *             CurrentDeltaSquareError= abs( distance(N1r, F3)² - DistanceF3N² )
                 *
                 *             So we have a second loop exit criteria that is fast to check and will allow to avoid unnecessary iterations:
                 *             do { ... }
                 *             while ( () && ( CurrentDeltaSquareError > TargetDeltaSquareError ) );
                 *
                 */
                ttm_number TargetDeltaSquareError = DistanceF3N2 * ttc_math_PrecisionValue * ttc_math_PrecisionValue;

                do {
#if (TTC_ASSERT_MATH_EXTRA == 1)
                    tml3_Approach2Stats.AmountSteps++;
#endif

                    ttc_math_polar2cartesian( CurrentROTy, N1_z, & N2->X, & N2->Z );

                    if ( 1 ) { // calculate DeltaSquares = (Rotated->F3.X - N2->X)² + (Rotated->F3.X - N2->X)² + DY²
                        ttm_number D2;
                        DeltaSquares = DY2;
                        D2 = Rotated->F3.X - N2->X;
                        D2 = D2 * D2;
                        DeltaSquares += D2;  // = (Rotated->F3.X - N2->X)²
                        D2 = Rotated->F3.Z - N2->Z;
                        D2 = D2 * D2;
                        DeltaSquares += D2;  // = (Rotated->F3.X - N2->X)² + (Rotated->F3.Z - N2->Z)² + DY²
                    }
                    CurrentDeltaSquareError = ttc_math_distance( DeltaSquares, DistanceF3N2 );

                    if ( DeltaSquareError > CurrentDeltaSquareError ) { // found angle is better than existing one
                        DeltaSquareError = CurrentDeltaSquareError;
                        N1->X = N2->X;
                        //N1->Y = N2->Y; y-coordinate does not change during rotation
                        N1->Z = N2->Z;
                        ROTy = CurrentROTy;
                    }

                    if ( DeltaSquares < DistanceF3N2 ) { // distance too small: increase angle between N and F3
                        if ( InvertSearch ) {
                            CurrentROTy -= AngleStepSize;
                        }
                        else {
                            CurrentROTy += AngleStepSize;
                        }
                    }
                    else {                               // distance too big:   decrease angle between N and F3
                        if ( InvertSearch ) {
                            CurrentROTy += AngleStepSize;
                        }
                        else {
                            CurrentROTy -= AngleStepSize;
                        }
                    }

                    AngleStepSize /= 2; // decrease step size
                }
                while ( ( AngleStepSize > ttc_math_PrecisionValue ) && // rotation angle delta is above desired precision
                        ( CurrentDeltaSquareError > TargetDeltaSquareError )          // sum of squares is still above target value
                      );

                ROTy = ttc_math_angle_reduce( ROTy );
                //X ttc_math_polar2cartesian( ROTy, N1_z, & N1->X, & N1->Z );

#if (TTC_ASSERT_MATH_EXTRA == 1)
                if ( ROTy != tml3_Approach2Stats.ROTy_1 ) { // found a better ROTy: store in debug data
                    tml3_Approach2Stats.ROTy_2 = ROTy;
                    tml3_Approach2Stats.dF2N2 = ttc_math_vector3d_distance( N1, &Rotated ->F2 );
                    tml3_Approach2Stats.dF3N2 = ttc_math_vector3d_distance( N1, &Rotated ->F3 );
                    tml3_Approach2Stats.dON2  = ttc_math_vector3d_length( N1 );
                    tml3_Approach2Stats.Change_ROTy_Degree = ttc_math_abs( tml3_Approach2Stats.ROTy_2 - tml3_Approach2Stats.ROTy_1 ) * 180 / PI;
                    tml3_Approach2Stats.Change_DistanceF3N = ttc_math_abs( tml3_Approach2Stats.dF3N1 - DistanceF3N ) - ttc_math_abs( tml3_Approach2Stats.dF3N2 - DistanceF3N );
                    if ( tml3_Approach2Stats.Max_Change_ROTy_Degree < tml3_Approach2Stats.Change_ROTy_Degree )
                    { tml3_Approach2Stats.Max_Change_ROTy_Degree = tml3_Approach2Stats.Change_ROTy_Degree; }
                    if ( tml3_Approach2Stats.Max_Change_DistanceF3N < tml3_Approach2Stats.Change_DistanceF3N )
                    { tml3_Approach2Stats.Max_Change_DistanceF3N = tml3_Approach2Stats.Change_DistanceF3N; }

                    ROTy += 0; // <-- place breakpoint here!
                }
                else {
                    tml3_Approach2Stats.ROTy_2 = 0;
                    tml3_Approach2Stats.dF2N2  = 0;
                    tml3_Approach2Stats.dF3N2  = 0;
                    tml3_Approach2Stats.dON2   = 0;
                    tml3_Approach2Stats.Change_ROTy_Degree = 0;
                    tml3_Approach2Stats.Change_DistanceF3N = 0;
                }
#endif
            }
            else {                     // found ROTy is minimal/ maximal: no further optimization possible
                if ( ROTy > 0 ) { // rotate N1_ around y-axis to final position N1 (y-coordinate unchanged)
                    ttc_math_polar2cartesian( ROTy, N1_z, & N1->X, & N1->Z );
                }
            }

#if (TTC_ASSERT_MATH_EXTRA == 1)
            if ( 1 ) {
                Error = _t_ttc_math_lateration_3dest_distances( DistanceON, DistanceF2N, DistanceF3N, DistanceF2F3, &Rotated->F2, &Rotated->F3, N1 );
                //X Assert_MATH_EXTRA(Error == 0, ttc_assert_origin_auto);
                if ( Error )
                { goto tml3_Failed; } // laterated position invalid (does not match to given distances!)
            }
#endif
        }
    }

    if ( N2 && ( N1 != N2 ) ) { // N2 given: calculating second possible location is easy in rotated coordinate system
        Assert_MATH_Writable( N2, ttc_assert_origin_auto );  // must point to writable memory!

        N2->X =  N1->X;
        N2->Y =  N1->Y;
        N2->Z = -N1->Z; // in our rotated coordinate system, N2 is just mirrored on xy-plane

        // coordinate system has been rotated xzy: have to rotate point in exact reverse order
        ttc_math_vector3d_rotate_yzx( N2, -Rotated->RotationX, -Rotated->RotationY, -Rotated->RotationZ );
    }
    if ( N1 != N2 ) {           // rotate N1 to final position (must do this AFTER calculating N2)

        // coordinate system has been rotated xzy: have to rotate point in exact reverse order
        ttc_math_vector3d_rotate_yzx( N1, -Rotated->RotationX, -Rotated->RotationY, -Rotated->RotationZ );
    }

#if (TTC_ASSERT_MATH_EXTRA == 1)  // Check if calculated coordinates match to given distances
    if ( N1 ) {                 // compare distances to N1 with given (required) distances
        Error = _t_ttc_math_lateration_3dest_distances( DistanceON, DistanceF2N, DistanceF3N, DistanceF2F3, F2, F3, N1 );
        //X Assert_MATH_EXTRA(Error == 0, ttc_assert_origin_auto);
        if ( Error )
        { goto tml3_Failed; } // laterated position invalid (does not match to given distances!)
        //X _t_ttc_math_lateration_3dest_distances( DistanceON, DistanceF2N, DistanceF3N, DistanceF2F3, F2, F3, N1 );
    }
    if ( N2 && ( N1 != N2 ) ) { // compare distances to N2 with given (required) distances
        Error = _t_ttc_math_lateration_3dest_distances( DistanceON, DistanceF2N, DistanceF3N, DistanceF2F3, F2, F3, N2 );
        //X Assert_MATH_EXTRA(Error == 0, ttc_assert_origin_auto);
        if ( Error )
        { goto tml3_Failed; } // laterated position invalid (does not match to given distances!)
        //X _t_ttc_math_lateration_3dest_distances( DistanceON, DistanceF2N, DistanceF3N, DistanceF2F3, F2, F3, N2 );
    }
#endif

#ifdef TTC_MATH_PIN_LATERATION_3D // signal end of function
    ttc_task_critical_end();
    ttc_gpio_clr( TTC_MATH_PIN_LATERATION_3D );
#endif
    return E_ttc_math_error_OK; // successfully laterated valid coordinates for N1, N2

tml3_Failed:
#ifdef TTC_MATH_PIN_LATERATION_3D // signal end of function
    ttc_task_critical_end();
    ttc_gpio_clr( TTC_MATH_PIN_LATERATION_3D );
#endif
    if ( Error )
    { return Error; }

    return E_ttc_math_error_lateration3d; // specific error code not known
}
ttm_number           ttc_math_set_precision( e_ttc_math_precision Precision ) {

    Assert_MATH( Precision < tmp_unknown, ttc_assert_origin_auto );  // Invalid value given!

    switch ( Precision ) {
        case tmp_1m:   ttc_math_PrecisionValue = 0.001; break;
        case tmp_10m:  ttc_math_PrecisionValue = 0.01;  break;
        case tmp_100m: ttc_math_PrecisionValue = 0.1;   break;
        // Add more cases if required!
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unsuported precision - Add more cases above or check your code!
    }
    ttc_math_Precision = Precision;

    return ttc_math_PrecisionValue;
}

// Implementation of floating point math can be found in current low-level driver or in interfaces/ttc_math_interface.c

void                   ttc_math_vector2d_rotate( ttm_number* X, ttm_number* Y, ttm_number Angle ) {

    Assert_MATH_Writable( X, ttc_assert_origin_auto );  // always check incoming pointer arguments
    Assert_MATH_Writable( Y, ttc_assert_origin_auto );  // always check incoming pointer arguments

#ifdef TTC_MATH_PIN_ROTATE_2D
    ttc_task_critical_begin();
    ttc_gpio_set( TTC_MATH_PIN_ROTATE_2D );
#endif

    // calculate angle and length of given coordinates
    ttm_number AngleOld;
    ttm_number Length;
    ttc_math_cartesian2polar( *X, *Y, &AngleOld, &Length );

    // calculate rotated coordinates
    ttc_math_polar2cartesian( AngleOld + Angle, Length, X, Y );

#ifdef TTC_MATH_PIN_ROTATE_2D
    ttc_gpio_clr( TTC_MATH_PIN_ROTATE_2D );
    ttc_task_critical_end();
#endif
}
void                   ttc_math_vector3d_rotate_xyz( t_ttc_math_vector3d_xyz* Vector, ttm_number RotateX, ttm_number RotateY, ttm_number RotateZ ) {
    Assert_MATH_Writable( Vector, ttc_assert_origin_auto );

    if ( RotateX )
    { ttc_math_vector2d_rotate( & Vector->Y, & Vector->Z, RotateX ); }
    if ( RotateY )
    { ttc_math_vector2d_rotate( & Vector->X, & Vector->Z, RotateY ); }
    if ( RotateZ )
    { ttc_math_vector2d_rotate( & Vector->Y, & Vector->X, RotateZ ); }
}
void                   ttc_math_vector3d_rotate_xzy( t_ttc_math_vector3d_xyz* Vector, ttm_number RotateX, ttm_number RotateY, ttm_number RotateZ ) {
    Assert_MATH_Writable( Vector, ttc_assert_origin_auto );

    if ( RotateX )
    { ttc_math_vector2d_rotate( & Vector->Y, & Vector->Z, RotateX ); }
    if ( RotateZ )
    { ttc_math_vector2d_rotate( & Vector->Y, & Vector->X, RotateZ ); }
    if ( RotateY )
    { ttc_math_vector2d_rotate( & Vector->X, & Vector->Z, RotateY ); }
}
void                   ttc_math_vector3d_rotate_yzx( t_ttc_math_vector3d_xyz* Vector, ttm_number RotateX, ttm_number RotateY, ttm_number RotateZ ) {
    Assert_MATH_Writable( Vector, ttc_assert_origin_auto );

    if ( RotateY )
    { ttc_math_vector2d_rotate( & Vector->X, & Vector->Z, RotateY ); }
    if ( RotateZ )
    { ttc_math_vector2d_rotate( & Vector->Y, & Vector->X, RotateZ ); }
    if ( RotateX )
    { ttc_math_vector2d_rotate( & Vector->Y, & Vector->Z, RotateX ); }
}
void                   ttc_math_vector3d_rotate_yxz( t_ttc_math_vector3d_xyz* Vector, ttm_number RotateX, ttm_number RotateY, ttm_number RotateZ ) {
    Assert_MATH_Writable( Vector, ttc_assert_origin_auto );

    if ( RotateY )
    { ttc_math_vector2d_rotate( & Vector->X, & Vector->Z, RotateY ); }
    if ( RotateX )
    { ttc_math_vector2d_rotate( & Vector->Y, & Vector->Z, RotateX ); }
    if ( RotateZ )
    { ttc_math_vector2d_rotate( & Vector->Y, & Vector->X, RotateZ ); }
}
void                   ttc_math_vector3d_rotate_zxy( t_ttc_math_vector3d_xyz* Vector, ttm_number RotateX, ttm_number RotateY, ttm_number RotateZ ) {
    Assert_MATH_Writable( Vector, ttc_assert_origin_auto );

    if ( RotateZ )
    { ttc_math_vector2d_rotate( & Vector->Y, & Vector->X, RotateZ ); }
    if ( RotateX )
    { ttc_math_vector2d_rotate( & Vector->Y, & Vector->Z, RotateX ); }
    if ( RotateY )
    { ttc_math_vector2d_rotate( & Vector->X, & Vector->Z, RotateY ); }
}
void                   ttc_math_vector3d_rotate_zyx( t_ttc_math_vector3d_xyz* Vector, ttm_number RotateX, ttm_number RotateY, ttm_number RotateZ ) {
    Assert_MATH_Writable( Vector, ttc_assert_origin_auto );

    if ( RotateZ )
    { ttc_math_vector2d_rotate( & Vector->Y, & Vector->X, RotateZ ); }
    if ( RotateY )
    { ttc_math_vector2d_rotate( & Vector->X, & Vector->Z, RotateY ); }
    if ( RotateX )
    { ttc_math_vector2d_rotate( & Vector->Y, & Vector->Z, RotateX ); }
}
void                   ttc_math_vector3d_copy( t_ttc_math_vector3d_xyz* Target, t_ttc_math_vector3d_xyz* Source ) {

    Assert_MATH_Writable( Target, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_MATH_Writable( Source, ttc_assert_origin_auto ); // always check incoming pointer arguments

    Target->X = Source->X;
    Target->Y = Source->Y;
    Target->Z = Source->Z;
}
void                   ttc_math_vector3d_add( t_ttc_math_vector3d_xyz* Target, t_ttc_math_vector3d_xyz* Add ) {

    Assert_MATH_Writable( Target, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_MATH_Writable( Add, ttc_assert_origin_auto ); // always check incoming pointer arguments

    Target->X += Add->X;
    Target->Y += Add->Y;
    Target->Z += Add->Z;
}
void                   ttc_math_vector3d_sub( t_ttc_math_vector3d_xyz* Target, t_ttc_math_vector3d_xyz* Subtract ) {

    Assert_MATH_Writable( Target, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_MATH_Writable( Subtract, ttc_assert_origin_auto ); // always check incoming pointer arguments

    Target->X -= Subtract->X;
    Target->Y -= Subtract->Y;
    Target->Z -= Subtract->Z;
}
void                   ttc_math_vector3d_scale( t_ttc_math_vector3d_xyz* Target, ttm_number Scale ) {

    Assert_MATH_Writable( Target, ttc_assert_origin_auto ); // always check incoming pointer arguments

    Target->X *= Scale;
    Target->Y *= Scale;
    Target->Z *= Scale;
}
ttm_number             ttc_math_vector3d_length( t_ttc_math_vector3d_xyz* Vector ) {

    Assert_MATH_Readable( Vector, ttc_assert_origin_auto ); // always check incoming pointer arguments
    return ttc_math_length_3d( Vector->X, Vector->Y, Vector->Z );
}
ttm_number             ttc_math_vector2d_length( t_ttc_math_vector2d_xy* Vector ) {

    Assert_MATH_Readable( Vector, ttc_assert_origin_auto ); // always check incoming pointer arguments
    return ttc_math_length_2d( Vector->X, Vector->Y );
}
BOOL                   ttc_math_vector2d_valid( const t_ttc_math_vector2d_xy* Vector ) {

    Assert_MATH_Readable( Vector, ttc_assert_origin_auto ); // always check incoming pointer arguments

    return ( Vector->X != TTC_MATH_CONST_NAN ) &&
           ( Vector->Y != TTC_MATH_CONST_NAN );
}
void                   ttc_math_intersection_2d( t_ttc_math_vector2d_xy* Line1_Point, t_ttc_math_vector2d_xy* Line1_Incline, t_ttc_math_vector2d_xy* Line2_Point, t_ttc_math_vector2d_xy* Line2_Incline, t_ttc_math_vector2d_xy* Intersection ) {

    Assert_MATH_Writable( Line1_Point, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_MATH_Writable( Line1_Incline, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_MATH_Writable( Line2_Point, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_MATH_Writable( Line2_Incline, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_MATH_Writable( Intersection, ttc_assert_origin_auto ); // always check incoming pointer arguments

    //your_code_goes_here

    _driver_math_intersection_2d( Line1_Point, Line1_Incline, Line2_Point, Line2_Incline, Intersection );
}
t_ttc_math_vector2d_xy ttc_math_vector2d_incline( t_ttc_math_vector2d_xy* A, t_ttc_math_vector2d_xy* B ) {

    Assert_MATH_Writable( A, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_MATH_Writable( B, ttc_assert_origin_auto ); // always check incoming pointer arguments

    //your_code_goes_here

    return _driver_math_vector2d_incline( A, B );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_math(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
