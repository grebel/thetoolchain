#ifndef TTC_INTERRUPT_H
#define TTC_INTERRUPT_H

/** { ttc_interrupt.h **********************************************************

                      The ToolChain

   Device independent support for interrupt requests

   Currently implemented architectures: stm32

   written by Gregor Rebel 2012


   How it works

   ttc_interrupt.h defines and documents an architecture independent interface to
   interrupt handling. It also checks which low-level features are provided by
   current low-level driver.
   Each low-level driver must declare a set of _driver_*() macros to register
   all implemented features.

   Different architecture drivers for usart, spi and others may define certain
   constants to add entries to interrupt definition structures.
   See ttc_interrupt_types.h for a list of valid constants.

}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
/** Defines/ TypeDefs/ structures being set by architecture driver
 *
 * Constant defines
 *
 * Structures
 *
 */
//{ Includes *************************************************************

#include "ttc_basic.h"
#include "ttc_gpio.h"
#include "DEPRECATED_ttc_interrupt_types.h"

#ifndef EXTENSION_500_ttc_interrupt
#  error Extension must be activated: 500_ttc_interrupt
#endif

// supported architectures
#ifdef EXTENSION_450_interrupt_stm32f1
#  include "stm32/DEPRECATED_stm32_interrupt.h"
#endif
#ifdef TARGET_ARCHITECTURE_STM32W1xx
#  include "stm32w/DEPRECATED_stm32w_interrupt.h"
#endif
#ifdef TARGET_ARCHITECTURE_STM32L1xx
#  include "stm32l1/DEPRECATED_stm32l1_interrupt.h"
#endif

// define default types for types not defined by architecture driver
#ifndef physical_index_t
#  warning physical_index_t not defined!
#  define physical_index_t u8_t
#endif

//} Includes
//{ Structures/ Enums ****************************************************

/** Check definition that have to be set by low-level driver.. */

#ifndef _driver_AMOUNT_EXTERNAL_INTERRUPTS
#  warning Missing value for _driver_AMOUNT_EXTERNAL_INTERRUPTS
#  define _driver_AMOUNT_EXTERNAL_INTERRUPTS 16
#endif

//} Structures/ Enums
//{ Global Variables *****************************************************


//} Global Variables
//{ Function prototypes **************************************************

/** initializes interrupt management for first use
 *
 * Note: This function must be called with disabled scheduler!
 */
void ttc_interrupt_prepare();

/** initializes interrupt for given Type to call given ISR
 * Note: This will not activate your interrupt. Call ttc_interrupt_enable() afterwards to enable/ disable this interrupt.
 *
 * @param Type        interrupt Type to use
 * @param PhysicalIndex real physical index of functional unit (e.g. 0 = USART1, 1 = USART2, ...)
 * @param ISR           function pointer of interrupt service routine to call;
 *                      ISR() gets two params: physical_index_t Index, void* Reference
 *                      Index     index functional unit in microcontroller (e.g. 0=first USART)
 *                      Reference hardware dependent data used by low level driver (e.g. register base address)
 * @param Argument      will be passed as argument to ISR()
 * @param OldISR        != NULL: previous stored pointer will be saved into given address
 * @param OldArgument   != NULL: previous stored argument will be saved into given address
 */
ttc_interrupt_errorcode_e ttc_interrupt_init(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, void (*ISR)(physical_index_t, void*), void* Argument, void (**OldISR)(physical_index_t, void*), void** OldArgument);
#ifndef _driver_interrupt_init
//#  define ttc_interrupt_init(Type, PhysicalIndex)
#  warning Missing implementation for ttc_interrupt_init()
#endif

/** initializes interrupt for USART devices only
 *
 * Note: This function is called from ttc_interrupt_init(). You may call it directly to get a slightly slower runtime.
 * Note: This will not activate your interrupt. Call ttc_interrupt_enable() afterwards to enable/ disable this interrupt.
 *
 * @param Type        interrupt Type to use
 * @param PhysicalIndex real physical index of functional unit (e.g. 0 = USART1, 1 = USART2, ...)
 * @param ISR           function pointer of interrupt service routine to call;
 *                      ISR() gets two params: physical_index_t Index, void* Reference
 *                      Index     index functional unit in microcontroller (e.g. 0=first USART)
 *                      Reference hardware dependent data used by low level driver (e.g. register base address)
 * @param Argument      will be passed as argument to ISR()
 * @param OldISR        != NULL: previous stored pointer will be saved into given address
 * @param OldArgument   != NULL: previous stored argument will be saved into given address
 */
ttc_interrupt_errorcode_e ttc_interrupt_init_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, void (*ISR)(physical_index_t, void*), void* Argument, void (**OldISR)(physical_index_t, void*), void** OldArgument);

/** initializes interrupt for GPIO devices only
 *
 * Note: This function is called from ttc_interrupt_init(). You may call it directly to get a slightly slower runtime.
 * Note: This will not activate your interrupt. Call ttc_interrupt_enable() afterwards to enable/ disable this interrupt.
 *
 * @param Type        interrupt Type to use
 * @param PhysicalIndex real physical index of functional unit (e.g. 0 = USART1, 1 = USART2, ...)
 * @param ISR           function pointer of interrupt service routine to call;
 *                      ISR() gets two params: physical_index_t Index, void* Reference
 *                      Index     index functional unit in microcontroller (e.g. 0=first USART)
 *                      Reference hardware dependent data used by low level driver (e.g. register base address)
 * @param Argument      will be passed as argument to ISR()
 * @param OldISR        != NULL: previous stored pointer will be saved into given address
 * @param OldArgument   != NULL: previous stored argument will be saved into given address
 */
ttc_interrupt_errorcode_e ttc_interrupt_init_gpio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, void (*ISR)(physical_index_t, void*), void* Argument, void (**OldISR)(physical_index_t, void*), void** OldArgument);

/** initializes interrupt for TIMER only
 *
 * Note: This function is called from ttc_interrupt_init(). You may call it directly to get a slightly slower runtime.
 * Note: This will not activate your interrupt. Call ttc_interrupt_usart_enable() afterwards to enable/ disable this interrupt.
 *
 * @param Type        interrupt Type to use
 * @param PhysicalIndex real physical index of functional unit (e.g. 0 = TIMER1, 1 = TIMER2, ...)
 * @param ISR           function pointer of interrupt service routine to call;
 *                      ISR() gets two params: physical_index_t Index, void* Reference
 *                      Index     index functional unit in microcontroller (e.g. 0=first TIMER)
 *                      Reference hardware dependent data used by low level driver (e.g. register base address)
 * @param Argument      will be passed as argument to ISR()
 * @param OldISR        != NULL: previous stored pointer will be saved into given address
 * @param OldArgument   != NULL: previous stored argument will be saved into given address
 */
ttc_interrupt_errorcode_e ttc_interrupt_init_timer(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, void (*ISR)(physical_index_t, void*), void* Argument, void (**OldISR)(physical_index_t, void*), void** OldArgument);

/** un-initialize given interrupt Type on given functional unit
 * @param Type     interrupt Type to use
 * @param PhysicalIndex  real physical index of functional unit (e.g. 0 = USART1, 1 = USART2, ...)
 */
ttc_interrupt_errorcode_e ttc_interrupt_deinit(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);
#define ttc_interrupt_deinit(Type, PhysicalIndex) ttc_interrupt_init(Type, PhysicalIndex, NULL, NULL, NULL, NULL)

/** activates/ deactivates a previously initialized interrupt
 * @param Type        interrupt Type to use
 * @param PhysicalIndex real physical index of functional unit (e.g. 0 = USART1, 1 = USART2, ...)
 * @param EnableIRQ     =0: disable interrupt; !=0: enable interrupt
 */
void ttc_interrupt_enable(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);
#ifdef _driver_interrupt_enable
#else
#  define ttc_interrupt_enable(Type, PhysicalIndex, EnableIRQ)
#  warning Missing implementation for ttc_interrupt_enable()
#endif

/** globally enables all interrupts
  */
void ttc_interrupt_all_enable();
#ifdef _driver_interrupt_all_enable
#define ttc_interrupt_all_enable() _driver_interrupt_all_enable()
#else
#define ttc_interrupt_all_enable()
#warning Missing implementation for _driver_interrupt_all_enable()
#endif

/** globally disables all interrupts
  */
void ttc_interrupt_all_disable();
#ifdef _driver_interrupt_all_disable
#define ttc_interrupt_all_disable() _driver_interrupt_all_disable()
#else
#define ttc_interrupt_all_disable()
#warning Missing implementation for _driver_interrupt_all_disable()
#endif

//} Function prototypes
/** { Check availability of low-level driver functions
 *
 * Each function is given as a prototype and checked for a matching _driver_*() definition.
 * The more functions are implemented by low-level driver, the more features can be used on current architecture.
 */

/** { Interrupt-Initializers
  *
  * Individual interrupts have to be initialized before they can be enabled.
  * Each function here will initialize only interrupts for a specific functional unit (E.g. interrupts from GPIO)
  */
ttc_interrupt_errorcode_e _driver_interrupt_init_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);
#ifndef _driver_interrupt_init_usart
#  define _driver_interrupt_init_usart(Type, PhysicalIndex)
//ToDo: #  warning Missing implementation for _driver_interrupt_init_usart()
#else
ttc_interrupt_errorcode_e _driver_interrupt_init_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);
#endif

ttc_interrupt_errorcode_e _driver_interrupt_init_spi(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);
#ifndef _driver_interrupt_init_spi
//ToDo: #  warning Missing implementation for _driver_interrupt_init_spi()
#endif

ttc_interrupt_errorcode_e _driver_interrupt_init_radio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);
#ifndef _driver_interrupt_init_radio
//ToDo: #  warning Missing implementation for _driver_interrupt_init_radio()
#endif

ttc_interrupt_errorcode_e _driver_interrupt_init_timer(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);
#ifndef _driver_interrupt_init_timer
//ToDo: #  warning Missing implementation for _driver_interrupt_init_timer()
#endif

ttc_interrupt_errorcode_e _driver_interrupt_init_i2c(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);
#ifndef _driver_interrupt_init_i2c
//ToDo: #  warning Missing implementation for _driver_interrupt_init_i2c()
#endif

ttc_interrupt_errorcode_e _driver_interrupt_init_gpio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);
#ifndef _driver_interrupt_init_gpio
#  warning Missing implementation for _driver_interrupt_init_gpio()
#endif


//}Interrupt-Initializers
/** { Interrupt-Enablers
  *
  * Each function here will activate/ deactivate interrupts only for a specific functional unit (E.g. interrupts from GPIO)
  * After successfully initialization, each individual interrupt has to be enabled before it can fire.
  */

void _driver_interrupt_enable_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);
#ifndef _driver_interrupt_enable_usart
//ToDo: #  warning Missing implementation for _driver_interrupt_enable_usart()
#endif

void _driver_interrupt_enable_spi(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);
#ifndef _driver_interrupt_enable_spi
//ToDo: #  warning Missing implementation for _driver_interrupt_enable_spi()
#endif

void _driver_interrupt_enable_radio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);
#ifndef _driver_interrupt_enable_radio
//ToDo: #  warning Missing implementation for _driver_interrupt_enable_radio()
#endif

void stm32_interrupt_enable_timer(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);
#ifndef _driver_interrupt_enable_timer
//ToDo: #  warning Missing implementation for _driver_interrupt_enable_timer()
#endif

void _driver_interrupt_enable_i2c(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);
#ifndef _driver_interrupt_enable_i2c
//ToDo: #  warning Missing implementation for _driver_interrupt_enable_i2c()
#endif

void stm32_interrupt_enable_gpio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);
#ifndef _driver_interrupt_enable_gpio
#  warning Missing implementation for _driver_interrupt_enable_gpio()
#endif

//}Interrupt-Enablers

//}low-level driver functions

#endif //TTC_INTERRUPT_H
