#ifndef INTERFACE_STE101P_TYPES_H
#define INTERFACE_STE101P_TYPES_H

/** { interface_ste101p.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *                     
 *  Low-Level datatypes for INTERFACE devices on ste101p architectures.
 *  Structures, Enums and Defines being required by ttc_interface_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150310 09:22:28 UTC
 *
 *  Note: See ttc_interface.h for description of architecture independent INTERFACE implementation.
 * 
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_interface_types.h *************************

typedef struct { // register description (adapt according to ste101p registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} t_interface_register;

typedef struct {  // ste101p specific configuration data of single INTERFACE device
  t_interface_register* BaseRegister;       // base address of INTERFACE device registers
} __attribute__((__packed__)) t_interface_ste101p_config;

// t_ttc_interface_architecture is required by ttc_interface_types.h
#define t_ttc_interface_architecture t_interface_ste101p_config

//} Structures/ Enums


#endif //INTERFACE_STE101P_TYPES_H
