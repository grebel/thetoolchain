#ifndef INTERFACE_STE101P_H
#define INTERFACE_STE101P_H

/** { interface_ste101p.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for interface devices on ste101p architectures.
 *  Structures, Enums and Defines being required by high-level interface and application.
 *
 *  Created from template device_architecture.h revision 22 at 20150310 09:22:28 UTC
 *
 *  Note: See ttc_interface.h for description of ste101p independent INTERFACE implementation.
 *  
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_INTERFACE_STE101P
//
// Implementation status of low-level driver support for interface devices on ste101p
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_INTERFACE_STE101P '?'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_INTERFACE_STE101P == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_INTERFACE_STE101P to one from '-', 'o', '+' 
#endif

//}EXTENSION_STATUS_TTC_INTERFACE_STE101P

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "interface_ste101p.c"
//
#include "interface_ste101p_types.h"
#include "../ttc_interface_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_interface_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_interface_foo
//
#define ttc_driver_interface_deinit(Config) interface_ste101p_deinit(Config)
#define ttc_driver_interface_get_features(Config) interface_ste101p_get_features(Config)
#define ttc_driver_interface_init(Config) interface_ste101p_init(Config)
#define ttc_driver_interface_load_defaults(Config) interface_ste101p_load_defaults(Config)
#define ttc_driver_interface_prepare() interface_ste101p_prepare()
#define ttc_driver_interface_reset(Config) interface_ste101p_reset(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_interface.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_interface.h for details.
 *
 * You normally should not need to add functions here. 
 * Add your private functions in the private section below. 
 */


/** shutdown single INTERFACE unit device
 * @param Config        pointer to struct t_ttc_interface_config (must have valid value for PhysicalIndex)
 * @return              == 0: INTERFACE has been shutdown successfully; != 0: error-code
 */
e_ttc_interface_errorcode interface_ste101p_deinit(t_ttc_interface_config* Config);


/** fills out given Config with maximum valid values for indexed INTERFACE
 * @param Config  = pointer to struct t_ttc_interface_config (must have valid value for PhysicalIndex)
 * @return        pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_interface_config* interface_ste101p_get_features(t_ttc_interface_config* Config);


/** initializes single INTERFACE unit for operation
 * @param Config        pointer to struct t_ttc_interface_config (must have valid value for PhysicalIndex)
 * @return              == 0: INTERFACE has been initialized successfully; != 0: error-code
 */
e_ttc_interface_errorcode interface_ste101p_init(t_ttc_interface_config* Config);


/** loads configuration of indexed INTERFACE unit with default values
 * @param Config        pointer to struct t_ttc_interface_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_interface_errorcode interface_ste101p_load_defaults(t_ttc_interface_config* Config);


/** Prepares interface Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void interface_ste101p_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_interface_config (must have valid value for PhysicalIndex)
 */
void interface_ste101p_reset(t_ttc_interface_config* Config);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _interface_ste101p_foo(t_ttc_interface_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //INTERFACE_STE101P_H