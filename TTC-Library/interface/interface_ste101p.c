/** { interface_ste101p.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for interface devices on ste101p architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150310 09:22:28 UTC
 *
 *  Note: See ttc_interface.h for description of ste101p independent INTERFACE implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "interface_ste101p.h".
//
#include "interface_ste101p.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

t_ttc_interface_config* interface_ste101p_get_features(t_ttc_interface_config* Config) {
    Assert_INTERFACE(Config, ttc_assert_origin_auto); // pointers must not be NULL

    static t_ttc_interface_config Features;
    if (!Features.LogicalIndex) { // called first time: initialize data
        
#     warning Function interface_ste101p_get_features() is empty.

    }
    Features.LogicalIndex = Config->LogicalIndex;
    
    return (t_ttc_interface_config*) 0;
}
e_ttc_interface_errorcode interface_ste101p_deinit(t_ttc_interface_config* Config) {
    Assert_INTERFACE(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function interface_ste101p_deinit() is empty.
    
    return (e_ttc_interface_errorcode) 0;
}
e_ttc_interface_errorcode interface_ste101p_init(t_ttc_interface_config* Config) {
    Assert_INTERFACE(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function interface_ste101p_init() is empty.
    
    return (e_ttc_interface_errorcode) 0;
}
e_ttc_interface_errorcode interface_ste101p_load_defaults(t_ttc_interface_config* Config) {
    Assert_INTERFACE(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function interface_ste101p_load_defaults() is empty.
    
    return (e_ttc_interface_errorcode) 0;
}
void interface_ste101p_prepare() {


#warning Function interface_ste101p_prepare() is empty.
    

}
void interface_ste101p_reset(t_ttc_interface_config* Config) {
    Assert_INTERFACE(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function interface_ste101p_reset() is empty.
    

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
