/** { ttc_gpio.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level interface and documentation for GPIO devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The ToolChain handles GPIOs a little bit different than other devices.
 *  Instead of logical indices, a direct hardware reference of type e_ttc_gpio_pin is used.
 *  The actual low-level driver will define this type for maximum speed.
 *  For the application, gpio usage is quite simple. As shown below, only steps 1) and 2)
 *  are required to make full use of gpio pins.
 *  Step 3) provides a convenient interface to read the current hardware configuration of
 *  a gpio pin from register settings. ttc_gpio_get_configuration() is not speed optimized.
 *  Its main purpose is to aid debugging.
 *
 *  The basic usage scenario for gpios:
 *  1) initialize:  ttc_gpio_init()
 *  2) use:         ttc_gpio_set()/ ttc_gpio_clr()/ ttc_gpio_put()/ ttc_gpio_get()
 *  3) debug:       p *ttc_gpio_get_configuration()
 *
 *  Created from template ttc_device.h revision 21 at 20140204 13:46:55 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_GPIO_H
#define TTC_GPIO_H

#ifndef EXTENSION_ttc_gpio
    #  error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_gpio.sh
#endif
//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "interfaces/ttc_gpio_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level gpio only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 */
/** Description of ttc_gpio (Do not delete this line!)
 *
 *  Architecture independent General Purpose Input Output (GPIO).
 *  Software control of individual GPIO pins on current microcontroller.
 *
 *  Each GPIO pin typically can be configured as one of these basic modes
 *  - Input Digital             (voltage on pin is read as 0 or 1)
 *  - Input Analog              (voltage on pin is read as analog voltage 0..MAX_VALUE)
 *  - Input Alternate Function  (pin is routed to an internal functional unit, e.g. SPI-MOSI)
 *  - Output Digital            (voltage on pin is actively set to 0V or Vcc)
 *  - Output Analog             (voltage on pin is actively set to 0V ... Vadc)
 *  - Output Alternate Function (pin is routed to an internal functional unit, e.g. SPI-MOSI)
 *
 *  Many architectures also allow to configure GPIO speed.
 *  When configured for lower speeds, current consumption and electromagnetic interference noise can be reduced.
 *
 *  ttc_gpio allows to configure each GPIO pin to each mode and speed in a architecture independent
 *  fashion, if supported by currrent low-level driver.
 *
 *  This approach can lead to a situtation when an application wants to configure a GPIO pin in a mode or
 *  speed that is not supported by the current microcontroller. This typically means that the board makefile
 *  is misconfigured. Its a good idea no to use fixed pin names (like E_ttc_gpio_pin_a0) in your c-code.
 *  Instead use makefile defined constants like TTC_LED1 whenever a e_ttc_gpio_pin argument is expected!
 *
 *  ttc_gpio also provides parallel output and input of 8 or 16 consecutive gpio pins.
 *  If the current low-level driver does not provide parallel support, then a slow interface function will
 *  automatically provide this feature.
 *
 *
 *  GPIO Pins
 *
 *  GPIO pins are usually grouped in banks of 8, 16 or more bits. Each bank is named by a letter A..Z.
 *  Pins are numbered 0..n-1. Pin B6 would then be the 7th pin of bank B.
 *  The ToolChain provides the hardware independent datatype e_ttc_gpio_pin. According to TTC naming scheme,
 *  its elements are prefixed with E_ttc_gpio_pin_ (Ttc_Gpio_Pin_e -> E_ttc_gpio_pin_). This prefix avoids conflicts with other symbols.
 *  E_ttc_gpio_pin_a0 -> first pin of bank A
 *  E_ttc_gpio_pin_a1 -> second pin of bank A
 *  ...
 *
 *  Example 1 - Set and Clear gpio pin
 *  ttc_gpio_init(E_ttc_gpio_pin_c3, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max);
 *  ttc_gpio_set(E_ttc_gpio_pin_c3); // set output pin to logical high (e.g. 3.3V)
 *  ttc_gpio_clr(E_ttc_gpio_pin_c3); // set output pin to logical low (0V)
 *  ttc_gpio_put(E_ttc_gpio_pin_c3, V); // set output pin to high if V!=0 or low otherwise
 *
 *  Example 2 - Read digital value from gpio pin
 *  ttc_gpio_init(E_ttc_gpio_pin_d7, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min);
 *  BOOL Value = ttc_gpio_get(E_ttc_gpio_pin_c3); // Value != 0 if voltage on pin C3 > 0.5 * Supply Voltage or 0 otherwise
 *
 *
 *  Parallel Ports
 *
 *  Complete banks of gpio pins can be accessed in parallel. This requires that all pins of an 8- or 16-bit wide bank
 *  are available on your PCB. Acessing all pins of a bank in a single write- or read-access can give a great speed up.
 *  ttc_gpio provides 8- and 16-bit wide parallel input and output regardless if the current low-level driver supports it.
 *  ttc_gpio_interface will automatically provide (slow) default implementations.
 *  GPIO banks are defined as values of enum t_ttc_gpio_bank (-> E_ttc_gpio_bank_a, E_ttc_gpio_bank_b, ...) or
 *  as constant defines TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, ...
 *  The constant defines allow to compile parts of your code conditionally
 *
 *  Example 3 - write out 16 bit in parallel to first 16 bit port
 *    + makefile
 *      # configure 8 bit parallel port #2 as pins 0..15 of gpio bank A
 *      COMPILE_OPTS += -DTTC_GPIO_PARALLEL16_1_BANK=TTC_GPIO_BANK_A
 *      COMPILE_OPTS += -DTTC_GPIO_PARALLEL16_1_FIRSTPIN=0
 *
 *    + my.c
 *      ttc_gpio_parallel16_init(1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max); // initialize all 16 pins of bank C to same configuration
 *      ...
 *
 *      // send multiple data words to first configured 16 bit parallel bus
 *      for (t_u16 Value = 0xffff; Value > 0; Value--) // write out 65536 different numbers
 *      { ttc_gpio_parallel16_1_put(Value); }
 *
 *
 *  Example 4 - read in data byte from second 8 bit parallel port
 *    + makefile
 *      # configure 8 bit parallel port #2 as pins 0..7 of gpio bank A
 *      COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_BANK=TTC_GPIO_BANK_A
 *      COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_FIRSTPIN=0
 *
 *    + my.c
 *      // init logical 8 bit parallel port as output with push-pull driver for minimum speed
 *      ttc_gpio_parallel08_init(2, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min);
 *      ...
 *
 *      // read byte from second 8 bit parallel port
 *      t_u8 Data = ttc_gpio_parallel08_2_get();
 *
 *
 *  Often it is not possible to use gpio pins 0..7.
 *  We can also shift data byte inside a 16 bit bank without great speed penalty.
 *
 *  Example 5 - write out shifted 8 bits to bank A
 *    + makefile
 *      # configure 8 bit parallel port #1 as pins 4..11 of gpio bank B
 *      COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_BANK=TTC_GPIO_BANK_B
 *      COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_FIRSTPIN=4
 *
 *    + my.c
 *      // init logical 8 bit parallel port as output with push-pull driver for maximum speed
 *      ttc_gpio_parallel08_init(1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max);
 *      ...
 *
 *      // send 42 to first 8 bit parallel port
 *      ttc_gpio_parallel08_1_put(42);
 */

/** Prepares gpio Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_gpio_prepare();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * Note: This function is rather slow and not intended to be used frequently.
 *       An application may call it from time to time to check if the configuration of a gpio has been altered unwillingly.
 * Note: This function cannot be used from GDB directky, as it requires an external buffer.
 *       Use ttc_gpio_get_configuration_gdb() for GDB debugging!
 *
 * Example:
 *       ttc_gpio_init(E_ttc_gpio_pin_a1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min);
 *       ... lots of code ...
 *       t_ttc_gpio_config ConfigGPIO;
 *       ttc_gpio_get_configuration(E_ttc_gpio_pin_a1, &ConfigGPIO);
 *       Assert(ConfigGPIO.Mode == E_ttc_gpio_mode_output_push_pull, ttc_assert_origin_auto); // someone has changed port configuration!
 *
 * @param PortPin         Port bit definition
 * @param Configuration   will be loaded with configuration of given port
 */
void ttc_gpio_get_configuration( e_ttc_gpio_pin PortPin, t_ttc_gpio_config* Configuration );

/** initialize indexed device for use
 *
 * In order to be used, every GPIO pin (cell) has to be initialized prior to operation. Some
 * microcontrollers (E.g. STM32) require to power up the corresponding GPIO bank before initializing
 * one of its cells. These architecture specific details are taken care of by the low-level driver.
 * The application developer normally must not consider these details.

 * @param PortPin port bit definition
 *        This enum identifies each GPIO pin with a unique identifier of form "E_ttc_gpio_pin_<G><N>".
 *        <G> = (A..G)  identifies the GPIO bank
 *        <N> = (0..15) identifies the individual pin number inside GPIO bank <G>
 * @param Mode   Selects the operating mode of an individual GPIO cell.
 * @param Speed  GPIO cells may operate at different switching frequencies. If set to lower frequencies, additional low-pass filters
 *               are activated to reduce electromagnetic interference from fast rising/ falling slopes.
 */
void ttc_gpio_init( e_ttc_gpio_pin PortPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );

/** Changes mapping of specified pin to alternate function.
 *
 * Note: Configure PortPin as alternate function pin via ttc_gpio_init() before or after calling this function!
 *
 * @param PortPin            Port bit definition
 * @param AlternateFunction  Selects alternate function to use on given PortPin
 */
void ttc_gpio_alternate_function( e_ttc_gpio_pin PortPin, e_ttc_gpio_alternate_function AlternateFunction );
void _driver_gpio_alternate_function( e_ttc_gpio_pin PortPin, e_ttc_gpio_alternate_function AlternateFunction );
//#define ttc_gpio_alternate_function(PortPin,AlternateFunction) _driver_gpio_alternate_function(PortPin,AlternateFunction);


/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_GPIO_get_max_LogicalIndex())
 */
void ttc_gpio_deinit( e_ttc_gpio_pin PortPin );

/** Set single output port bit to logical high.
 *  Voltage on the corresponding output pin is forced to logical high
 *  (usually 5V, 3.3V or 1.8V depending on architecture).
 *  In open drain mode, the voltage can only be forced if upper transistor
 *  (push) is active.
 *
 * @param PortPin bit definition
 */
void ttc_gpio_set( e_ttc_gpio_pin PortPin );
#define ttc_gpio_set(Port) _driver_gpio_set(Port)

/** Set single output port bit to either logical high or Low.
 *
 * @param PortPin bit definition
 * @param Value   new logical state of output pin (==true: set to high; ==false: set to low)
 */
void ttc_gpio_put( e_ttc_gpio_pin PortPin, BOOL Value );
// void _driver_gpio_put(e_ttc_gpio_pin PortPin, BOOL Value);
#define ttc_gpio_put(Port, Value) _driver_gpio_put(Port, Value)


/** set single output port bit to logical low
 *  Voltage on the corresponding output pin is forced to logical low
 *  (usually 0V).
 *  In open drain mode, the voltage can only be forced if lower transistor
 *  (pull) is active.
 *
 * @param PortPin bit definition
 */
void ttc_gpio_clr( e_ttc_gpio_pin PortPin );
#define ttc_gpio_clr(Port) _driver_gpio_clr(Port)

/** Read digital value from single input pin.
 *  The current voltage at corresponding GPIO pin is compared to a reference voltage (typically
 *  half of the logical high voltage). If voltage is higher, the get operation returns a true (!=0).
 *  false (==0) is returned otherwise.
 *
 * @param  PortPin bit definition
 * @return true = high level detected on input pin; low level otherwise
 */
BOOL ttc_gpio_get( e_ttc_gpio_pin PortPin );
#define ttc_gpio_get(Port) _driver_gpio_get(Port)

/** { creates numeric index representing given GPIO-bank and pin number
 *
 * Interrupt sources are identified by their type plus a numerical index.
 * The type simply names a kind of interrupt source (e.g. receive buffer of an usart is not empty).
 * Often more than one identical physical devices of equal type are present. The physical index
 * identifies which device is meant (e.g. 0 = USART1, 1 = USART2, ...)
 * Conversion functions convert between physical index and extra information required to access hardware.
 *
 * @param GPIOx  GPIO bank to use (-> ttc_gpio_types.h:t_ttc_gpio_bank)
 *               Note: GPIOx, Pin can be given as one argument by use of Pin_Pxn macro (TTC_GPIO_BANK_A, 7) = Pin_PA7
 * @param Pin    0..15
 * @return       32-bit value representing given GPIO-pin in a scalar datatype
}*/
t_base ttc_gpio_create_index( t_ttc_gpio_bank GPIOx, t_u8 Pin );
t_base _driver_gpio_create_index( t_ttc_gpio_bank GPIOx, t_u8 Pin );

/** { restores GPIO-bank and pin number from index value
 *
 * @param PhysicalIndex  32-bit value representing GPIO-bank + pin
 * @param GPIOx          loaded with GPIO bank (-> ttc_gpio_types.h:t_ttc_gpio_bank)
 * @param Pin            loaded with pin number
}*/
void ttc_gpio_from_index( t_base PhysicalIndex, t_ttc_gpio_bank* GPIOx, t_u8* Pin );
void _driver_gpio_from_index( t_base PhysicalIndex, t_ttc_gpio_bank* GPIOx, t_u8* Pin );
#define ttc_gpio_from_index(PhysicalIndex, GPIOx, Pin) _driver_gpio_from_index(PhysicalIndex, GPIOx, Pin)

/** return amount of currently configured 8 bit parallel ports
 *
 * @return maximum valid logical index for ttc_gpio_parallel16_*() functions (1..ttc_gpio_parallel16_get_amount())
 */
t_u16 ttc_gpio_parallel16_amount();
#define ttc_gpio_parallel16_amount()  TTC_GPIO_PARALLEL16_AMOUNT

/** initialize all pins of 16-bit wide parallel port at once
 *
 * Note: Runtime measured on STM32F107 @72MHz as < 3us
 *
 * @param Bank     GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 */
void ttc_gpio_parallel16_init( t_ttc_gpio_bank GPIOx, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );
#define ttc_gpio_parallel16_init(GPIOx, Mode, Speed) _driver_gpio_parallel16_init(GPIOx, Mode, Speed)

/** read 16 bit value from a 16 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * See description at top of this file for more details!
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 ttc_gpio_parallel16_1_get();  // LogicalIndex == 1
t_u16 ttc_gpio_parallel16_2_get();  // LogicalIndex == 2
t_u16 ttc_gpio_parallel16_3_get();  // LogicalIndex == 3
t_u16 ttc_gpio_parallel16_4_get();  // LogicalIndex == 4
t_u16 ttc_gpio_parallel16_5_get();  // LogicalIndex == 5
t_u16 ttc_gpio_parallel16_6_get();  // LogicalIndex == 6
t_u16 ttc_gpio_parallel16_7_get();  // LogicalIndex == 7
t_u16 ttc_gpio_parallel16_8_get();  // LogicalIndex == 8
t_u16 ttc_gpio_parallel16_9_get();  // LogicalIndex == 9
t_u16 ttc_gpio_parallel16_10_get(); // LogicalIndex == 10

// map function prototypes to low-level driver implementations
#define ttc_gpio_parallel16_1_get  _driver_gpio_parallel16_1_get
#define ttc_gpio_parallel16_2_get  _driver_gpio_parallel16_2_get
#define ttc_gpio_parallel16_3_get  _driver_gpio_parallel16_3_get
#define ttc_gpio_parallel16_4_get  _driver_gpio_parallel16_4_get
#define ttc_gpio_parallel16_5_get  _driver_gpio_parallel16_5_get
#define ttc_gpio_parallel16_6_get  _driver_gpio_parallel16_6_get
#define ttc_gpio_parallel16_7_get  _driver_gpio_parallel16_7_get
#define ttc_gpio_parallel16_8_get  _driver_gpio_parallel16_8_get
#define ttc_gpio_parallel16_9_get  _driver_gpio_parallel16_9_get
#define ttc_gpio_parallel16_10_get _driver_gpio_parallel16_10_get

/** write 16 bit value to a 16 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * See description at top of this file for more details!
 *
 * @param         16 bit value to write into output data register of configured gpio bank
 */
void ttc_gpio_parallel16_1_put( t_u16 Value ); // LogicalIndex == 1
void ttc_gpio_parallel16_2_put( t_u16 Value ); // LogicalIndex == 2
void ttc_gpio_parallel16_3_put( t_u16 Value ); // LogicalIndex == 3
void ttc_gpio_parallel16_4_put( t_u16 Value ); // LogicalIndex == 4
void ttc_gpio_parallel16_5_put( t_u16 Value ); // LogicalIndex == 5
void ttc_gpio_parallel16_6_put( t_u16 Value ); // LogicalIndex == 6
void ttc_gpio_parallel16_7_put( t_u16 Value ); // LogicalIndex == 7
void ttc_gpio_parallel16_8_put( t_u16 Value ); // LogicalIndex == 8
void ttc_gpio_parallel16_9_put( t_u16 Value ); // LogicalIndex == 9
void ttc_gpio_parallel16_10_put( t_u16 Value ); // LogicalIndex == 10

#if (TTC_ASSERT_GPIO_EXTRA != 1)  // map function prototypes to low-level driver implementations
    #define ttc_gpio_parallel16_1_put(Value)  _driver_gpio_parallel16_1_put(Value)
    #define ttc_gpio_parallel16_2_put(Value)  _driver_gpio_parallel16_2_put(Value)
    #define ttc_gpio_parallel16_3_put(Value)  _driver_gpio_parallel16_3_put(Value)
    #define ttc_gpio_parallel16_4_put(Value)  _driver_gpio_parallel16_4_put(Value)
    #define ttc_gpio_parallel16_5_put(Value)  _driver_gpio_parallel16_5_put(Value)
    #define ttc_gpio_parallel16_6_put(Value)  _driver_gpio_parallel16_6_put(Value)
    #define ttc_gpio_parallel16_7_put(Value)  _driver_gpio_parallel16_7_put(Value)
    #define ttc_gpio_parallel16_8_put(Value)  _driver_gpio_parallel16_8_put(Value)
    #define ttc_gpio_parallel16_9_put(Value)  _driver_gpio_parallel16_9_put(Value)
    #define ttc_gpio_parallel16_10_put(Value) _driver_gpio_parallel16_10_put(Value)
#endif

/** return amount of currently configured 8 bit parallel ports
 *
 * @return maximum valid logical index for ttc_gpio_parallel08_*() functions (1..ttc_gpio_parallel08_get_amount())
 */
t_u8 ttc_gpio_parallel08_amount();
#define ttc_gpio_parallel08_amount()  TTC_GPIO_PARALLEL08_AMOUNT

/** initialize 8 bit wide parallel port from given logical index
 *
 * An 8 bit wide parallel port is a set of 8 consecutive gpio port pins.
 * All 8 pins can be set or read with one memory access in parallel.
 * The gpio pins ideally start at index 0 (e.g. E_ttc_gpio_pin_a0 ... E_ttc_gpio_pin_a7).
 *
 * All available parallel ports have to be defined in makefile by using constants of this format:
 * TTC_GPIO_PARALLEL08_<n>_BANK
 * TTC_GPIO_PARALLEL08_<n>_FIRSTPIN
 *
 * Example (E_ttc_gpio_pin_b0 ... E_ttc_gpio_pin_b7):
   COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_BANK=TTC_GPIO_BANK_B
   COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_FIRSTPIN=0
 *
 * See description at top of this file for more details!
 *
 * @param LogicalIndex   1..ttc_gpio_parallel8_amount() = index of 8 bit parallel port to initialize.
 *                       Parallel port must be configured via constants in makefile: ==1: TTC_GPIO_PARALLEL08
 * @param Mode           mode to use for all 8 pins  (-> ttc_gpio_types.h:e_ttc_gpio_mode)
 * @param Speed          speed to use for all 8 pins (-> ttc_gpio_types.h:e_ttc_gpio_speed)
 */
void ttc_gpio_parallel08_init( t_u8 LogicalIndex, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );

/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * See description at top of this file for more details!
 *
 * @return          8 bit value read from register of given gpio bank
 */
t_u8 ttc_gpio_parallel08_1_get();  // LogicalIndex == 1
t_u8 ttc_gpio_parallel08_2_get();  // LogicalIndex == 2
t_u8 ttc_gpio_parallel08_3_get();  // LogicalIndex == 3
t_u8 ttc_gpio_parallel08_4_get();  // LogicalIndex == 4
t_u8 ttc_gpio_parallel08_5_get();  // LogicalIndex == 5
t_u8 ttc_gpio_parallel08_6_get();  // LogicalIndex == 6
t_u8 ttc_gpio_parallel08_7_get();  // LogicalIndex == 7
t_u8 ttc_gpio_parallel08_8_get();  // LogicalIndex == 8
t_u8 ttc_gpio_parallel08_9_get();  // LogicalIndex == 9
t_u8 ttc_gpio_parallel08_10_get(); // LogicalIndex == 10

// map function prototypes to low-level driver implementations
#define ttc_gpio_parallel08_1_get  _driver_gpio_parallel08_1_get
#define ttc_gpio_parallel08_2_get  _driver_gpio_parallel08_2_get
#define ttc_gpio_parallel08_3_get  _driver_gpio_parallel08_3_get
#define ttc_gpio_parallel08_4_get  _driver_gpio_parallel08_4_get
#define ttc_gpio_parallel08_5_get  _driver_gpio_parallel08_5_get
#define ttc_gpio_parallel08_6_get  _driver_gpio_parallel08_6_get
#define ttc_gpio_parallel08_7_get  _driver_gpio_parallel08_7_get
#define ttc_gpio_parallel08_8_get  _driver_gpio_parallel08_8_get
#define ttc_gpio_parallel08_9_get  _driver_gpio_parallel08_9_get
#define ttc_gpio_parallel08_10_get _driver_gpio_parallel08_10_get

/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param         8 bit data to write intou output data register of configured gpio bank
 */
void ttc_gpio_parallel08_1_put( t_u8 Value ); // LogicalIndex == 1
void ttc_gpio_parallel08_2_put( t_u8 Value ); // LogicalIndex == 2
void ttc_gpio_parallel08_3_put( t_u8 Value ); // LogicalIndex == 3
void ttc_gpio_parallel08_4_put( t_u8 Value ); // LogicalIndex == 4
void ttc_gpio_parallel08_5_put( t_u8 Value ); // LogicalIndex == 5
void ttc_gpio_parallel08_6_put( t_u8 Value ); // LogicalIndex == 6
void ttc_gpio_parallel08_7_put( t_u8 Value ); // LogicalIndex == 7
void ttc_gpio_parallel08_8_put( t_u8 Value ); // LogicalIndex == 8
void ttc_gpio_parallel08_9_put( t_u8 Value ); // LogicalIndex == 9
void ttc_gpio_parallel08_10_put( t_u8 Value ); // LogicalIndex == 10

#if (TTC_ASSERT_GPIO_EXTRA != 1)  // map function prototypes to low-level driver implementations
    #define ttc_gpio_parallel08_1_put(Value)  _driver_gpio_parallel08_1_put(Value)
    #define ttc_gpio_parallel08_2_put(Value)  _driver_gpio_parallel08_2_put(Value)
    #define ttc_gpio_parallel08_3_put(Value)  _driver_gpio_parallel08_3_put(Value)
    #define ttc_gpio_parallel08_4_put(Value)  _driver_gpio_parallel08_4_put(Value)
    #define ttc_gpio_parallel08_5_put(Value)  _driver_gpio_parallel08_5_put(Value)
    #define ttc_gpio_parallel08_6_put(Value)  _driver_gpio_parallel08_6_put(Value)
    #define ttc_gpio_parallel08_7_put(Value)  _driver_gpio_parallel08_7_put(Value)
    #define ttc_gpio_parallel08_8_put(Value)  _driver_gpio_parallel08_8_put(Value)
    #define ttc_gpio_parallel08_9_put(Value)  _driver_gpio_parallel08_9_put(Value)
    #define ttc_gpio_parallel08_10_put(Value) _driver_gpio_parallel08_10_put(Value)
#endif

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
/** { GDB Function prototypes ******************************************
 *
 * GDB support functions for ttc_gpio
 *
 * Functions provided here allow simple debugging of GPIO ports.
 * They return a pointer to a static buffer.
 * It is highly advised, not to use these functions in multitasking environment!
 *
 * Example Usage:
 * GDB> p * ttc_gpio_get_configuration_gdb(E_ttc_gpio_pin_d8)
 */
#ifdef EXTENSION_basic_extensions_optimize_1_debug

    /** Shows current configuration of single GPIO cell during a GDB debugging session
    *
    * Current configuration of single GPIO cell is read from registers and parsed into human readable format.
    * This can be a great help to verify the configuration of GPIO cells during debugging.
    *
    * IMPORTANT: This function shall only be used inside gdb debug session!
    *
    * Example of intended use in gdb session:
    * gdb> p * ttc_gpio_get_configuration_gdb( E_ttc_gpio_pin_a0 )
    *
    * Note: This function is not reentrant. It shall be used from a single task only!
    *       Use ttc_gpio_get_configuration() in case you want to read GPIO configurations from multiple tasks!
    *
    * @param GPIOx     GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
    * @param Pin       0..8  lowest of 8 consecutive pins to use
    * @param Value     written into bits Pin..Pin+7 of output data register of given gpio bank
    * @return          Pointer to debug data of given GPIO port pin.
    */
    volatile t_ttc_gpio_config* ttc_gpio_get_configuration_gdb( e_ttc_gpio_pin PortPin );

#endif

//}GDB Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_gpio(e_ttc_gpio_pin PortPin)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_gpio_ are passed to interfaces/ttc_gpio_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl gpio UPDATE
 */

/** Prepares gpio driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void _driver_gpio_prepare();

/** shutdown single GPIO unit device
 * @param PortPin bit definition
 */
void _driver_gpio_deinit( e_ttc_gpio_pin PortPin );

/** initializes single GPIO unit for operation
 * @param PortPin bit definition
 * @param Speed  GPIOs support different driver speeds
 * @param Type   operation mode to use
 */
void _driver_gpio_init( e_ttc_gpio_pin PortPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );

/** initializes single GPIO unit for operation
 * @param PortPin         port bit definition
 * @param Configuration  loaded with current configuration of given PortPin
 */
void _driver_gpio_get_configuration( e_ttc_gpio_pin PortPin, t_ttc_gpio_config* Configuration );

/** set single output port bit to logical 1 (usually high level)
 *
 * @param PortPin bit definition
 */
#ifndef _driver_gpio_set
    void _driver_gpio_set( e_ttc_gpio_pin PortPin );
#endif

/** set single output port bit to logical 0 (usually high level)
 *
 * @param PortPin bit definition
 */
#ifndef _driver_gpio_set
    void _driver_gpio_clr( e_ttc_gpio_pin PortPin );
#endif

/** get value of single input port bit (voltage level at input pin)
 *
 * @param PortPin bit definition
 */
#ifndef _driver_gpio_get
    BOOL _driver_gpio_get( e_ttc_gpio_pin PortPin );
#endif

/** initialize all pins of 16-bit wide parallel port at once
 *
 * Note: Runtime measured on STM32F107 @72MHz as < 3us
 *
 * @param Bank     GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 */
void _driver_gpio_parallel16_init( t_ttc_gpio_bank GPIOx, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );

/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
#if 0 // must not compile these lines as they are only usefull for create_DeviceDriver.pl
    t_u16 _driver_gpio_parallel16_1_get();  // LogicalIndex == 1
    t_u16 _driver_gpio_parallel16_2_get();  // LogicalIndex == 2
    t_u16 _driver_gpio_parallel16_3_get();  // LogicalIndex == 3
    t_u16 _driver_gpio_parallel16_4_get();  // LogicalIndex == 4
    t_u16 _driver_gpio_parallel16_5_get();  // LogicalIndex == 5
    t_u16 _driver_gpio_parallel16_6_get();  // LogicalIndex == 6
    t_u16 _driver_gpio_parallel16_7_get();  // LogicalIndex == 7
    t_u16 _driver_gpio_parallel16_8_get();  // LogicalIndex == 8
    t_u16 _driver_gpio_parallel16_9_get();  // LogicalIndex == 9
    t_u16 _driver_gpio_parallel16_10_get(); // LogicalIndex == 10
#endif

/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
#if 0 // must not compile these lines as they are only usefull for create_DeviceDriver.pl
    void _driver_gpio_parallel16_1_put( t_u16 Value ); // LogicalIndex == 1
    void _driver_gpio_parallel16_2_put( t_u16 Value ); // LogicalIndex == 2
    void _driver_gpio_parallel16_3_put( t_u16 Value ); // LogicalIndex == 3
    void _driver_gpio_parallel16_4_put( t_u16 Value ); // LogicalIndex == 4
    void _driver_gpio_parallel16_5_put( t_u16 Value ); // LogicalIndex == 5
    void _driver_gpio_parallel16_6_put( t_u16 Value ); // LogicalIndex == 6
    void _driver_gpio_parallel16_7_put( t_u16 Value ); // LogicalIndex == 7
    void _driver_gpio_parallel16_8_put( t_u16 Value ); // LogicalIndex == 8
    void _driver_gpio_parallel16_9_put( t_u16 Value ); // LogicalIndex == 9
    void _driver_gpio_parallel16_10_put( t_u16 Value ); // LogicalIndex == 10
#endif

/** initialize 8 bit wide parallel port
 *
 * Note: Runtime measured on STM32F107 @72MHz as < 2.2us
 *
 * @param Bank+FirstPin  Bank and Pin given as one argument by use of Pin_Pxn macro: E.g. (GPIOA, 7) = Pin_PA7
 * @param Bank           GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 * @param FirstPin       0..8  lowest of 8 consecutive pins to use
 * @param Type           mode to use for all 8 pins  (-> ttc_gpio_types.h:e_ttc_gpio_mode)
 * @param Speed          speed to use for all 8 pins (-> ttc_gpio_types.h:e_ttc_gpio_speed)
 */
void _driver_gpio_parallel08_init( t_ttc_gpio_bank GPIOx, t_u8 FirstPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );

/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          8 bit value read from register of given gpio bank
 */
#if 0 // must not compile these lines as they are only usefull for create_DeviceDriver.pl
    t_u8 _driver_gpio_parallel08_1_get();  // LogicalIndex == 1
    t_u8 _driver_gpio_parallel08_2_get();  // LogicalIndex == 2
    t_u8 _driver_gpio_parallel08_3_get();  // LogicalIndex == 3
    t_u8 _driver_gpio_parallel08_4_get();  // LogicalIndex == 4
    t_u8 _driver_gpio_parallel08_5_get();  // LogicalIndex == 5
    t_u8 _driver_gpio_parallel08_6_get();  // LogicalIndex == 6
    t_u8 _driver_gpio_parallel08_7_get();  // LogicalIndex == 7
    t_u8 _driver_gpio_parallel08_8_get();  // LogicalIndex == 8
    t_u8 _driver_gpio_parallel08_9_get();  // LogicalIndex == 9
    t_u8 _driver_gpio_parallel08_10_get(); // LogicalIndex == 10
#endif

/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    8 bit data to write into output data register of configured gpio bank
 */
#if 0 // must not compile these lines as they are only usefull for create_DeviceDriver.pl
    void _driver_gpio_parallel08_1_put( t_u8 Value ); // LogicalIndex == 1
    void _driver_gpio_parallel08_2_put( t_u8 Value ); // LogicalIndex == 2
    void _driver_gpio_parallel08_3_put( t_u8 Value ); // LogicalIndex == 3
    void _driver_gpio_parallel08_4_put( t_u8 Value ); // LogicalIndex == 4
    void _driver_gpio_parallel08_5_put( t_u8 Value ); // LogicalIndex == 5
    void _driver_gpio_parallel08_6_put( t_u8 Value ); // LogicalIndex == 6
    void _driver_gpio_parallel08_7_put( t_u8 Value ); // LogicalIndex == 7
    void _driver_gpio_parallel08_8_put( t_u8 Value ); // LogicalIndex == 8
    void _driver_gpio_parallel08_9_put( t_u8 Value ); // LogicalIndex == 9
    void _driver_gpio_parallel08_10_put( t_u8 Value ); // LogicalIndex == 10
#endif

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_GPIO_H
