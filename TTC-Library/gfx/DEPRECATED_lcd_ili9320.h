/*{ lcd_ili9320.h *******************************

  Low-level driver for LCD Controller ILI9320.

}*/

#ifndef IL9320_H
#define IL9320_H

//{ Includes 1 ***********************************************************

#include "../ttc_basic.h"
#include "../ttc_gpio.h"
#include "../ttc_task.h"
#include "../ttc_interrupt.h"
#include "../ttc_memory.h"

//}Includes 1
//{ Defines 1 *************************************************************

/** DEPRECATED
typedef struct {
    e_ttc_gpio_pin LCD_ChipSelect;
    e_ttc_gpio_pin LCD_WriteMode;
    e_ttc_gpio_pin LCD_ReadMode;
    e_ttc_gpio_pin LCD_Reset;
} t_lcd_ili9320_config;
*/

//}Defines 1
//{ Includes *************************************************************

#include "../ttc_gfx_types.h"

//X #include "ttc_watchdog.h" // ToDo: required?
//X #include "pin_configuration.h"
//X #include "lcd_gfximage.h"
//X #include "lcd_color.h"
//X #include "ttc_ads7843.h"
//X #include <stdio.h>

//}Includes
//{ externals provided by ttc_gfx.c *************************************

extern t_ttc_gfx_generic* ttc_gfx_CurrentDisplay;
extern t_u16 ttc_gfx_CurrentColorFg;
extern t_u16 ttc_gfx_CurrentColorBg;

//}externals
//{ Defines *************************************************************

//LCD measurements

#ifndef TTC_GFX1_LI9320_WIDTH
#define TTC_GFX1_LI9320_WIDTH 240
#endif
#ifndef TTC_GFX1_LI9320_HEIGHT
#define TTC_GFX1_LI9320_HEIGHT 320
#endif
#ifndef TTC_GFX1_LI9320_DEPTH
#define TTC_GFX1_LI9320_DEPTH 16 //in the 16-bit 1-transfer mode, 16-bit 2-transfer mode initialisation possible
#endif

#ifndef TTC_GFX1_ILI9320_PIN_CHIPSELECT
#warning missing definition for TTC_GFX1_ILI9320_PIN_CHIPSELECT - using internal default
#define TTC_GFX1_ILI9320_PIN_CHIPSELECT  TTC_GPIO_E3 // pin on protoboard Olimex STM32-LCD
#endif
#ifndef TTC_GFX1_ILI9320_PIN_WRITEMODE
#warning missing definition for TTC_GFX1_ILI9320_PIN_WRITEMODE - using internal default
#define TTC_GFX1_ILI9320_PIN_WRITEMODE   TTC_GPIO_D5 // pin on protoboard Olimex STM32-LCD
#endif
#ifndef TTC_GFX1_ILI9320_PIN_RESET
#warning missing definition for TTC_GFX1_ILI9320_PIN_RESET - using internal default
#define TTC_GFX1_ILI9320_PIN_RESET       TTC_GPIO_D7 // pin on protoboard Olimex STM32-LCD
#endif

#define BUTTONHEIGHT 18
#define BUTTONWIDTH BUTTONHEIGHT*3
#define MEDIUMLENGHT 10
#ifdef EXTENSION_board_olimex_lcd
#define T_HCK	10
#define LCDBUS_RSLOW_ADDR	((volatile t_u16 *) 0x60000000)
#define LCDBUS_RSHIGH_ADDR	((volatile t_u16 *)(0x60000000 | (1<<(19+1))))
#define ili9320_write_register(Register, Value) ili9320_write_command(Register); *LCDBUS_RSHIGH_ADDR = (t_u16) Value
#define ili9320_write_command(Command)  *LCDBUS_RSLOW_ADDR = (t_u16) Command
//#define CLR_LCD_CS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOD)->BRR),  7)) = 1  //LCD Chip Select
//#define CLR_LCD_RS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOE)->BRR),  2)) = 1  //LCD Reset Command
//#define CLR_LCD_WR *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOD)->BRR),  5)) = 1 //LCD Write Command Bit
//#define SET_LCD_CS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOD)->BSRR), 7)) = 1  //LCD Chip Select
//#define SET_LCD_RS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOE)->BSRR), 2)) = 1  //LCD Reset Command
//#define SET_LCD_WR *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOD)->BSRR), 5)) = 1 //LCD Write Command Bit
//#define ili9320_write_data(Value)  *LCDBUS_RSHIGH_ADDR = (t_u16) Value
#else
#define ili9320_write_register(LCD_Reg,LCD_Dat) ili9320_write_command(LCD_Reg);ili9320_write_data(LCD_Dat)
void ili9320_write_command(t_u16 LCD_Reg);
#endif

#define ili9320_set_pixel()   ili9320_write_command(0x22);ili9320_write_data(ttc_gfx_CurrentColorFg)
#define ili9320_set_x(X)      ili9320_write_command(0x20);ili9320_write_data(X)
#define ili9320_set_y(Y)      ili9320_write_command(0x21);ili9320_write_data(Y)

#ifdef EXTENSION_100_board_mini_stm32_rev20
#ifdef TARGET_ARCHITECTURE_STM32F1xx
#define CLR_LCD_CS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BRR),  8))  = 1  //LCD Chip Select
#define CLR_LCD_RS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BRR),  9))  = 1  //LCD Reset Command
#define CLR_LCD_WR *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BRR),  10)) = 1 //LCD Write Command Bit
#define CLR_LCD_RD *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BRR),  11)) = 1 //LCD Read Command Bit
#define SET_LCD_CS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BSRR), 8))  = 1  //LCD Chip Select
#define SET_LCD_RS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BSRR), 9))  = 1  //LCD Reset Command
#define SET_LCD_WR *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BSRR), 10)) = 1 //LCD Write Command Bit
#define SET_LCD_RD *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BSRR), 11)) = 1 //LCD Read Command Bit
#else
#define CLR_LCD_CS ttc_gpio_clr(TTC_GPIO_C8)  //LCD Chip Select
#define CLR_LCD_RS ttc_gpio_clr(TTC_GPIO_C9)  //LCD Reset Command
#define CLR_LCD_WR ttc_gpio_clr(TTC_GPIO_C10) //LCD Write Command Bit
#define CLR_LCD_RD ttc_gpio_clr(TTC_GPIO_C11) //LCD Read Command Bit
#define SET_LCD_CS ttc_gpio_set(TTC_GPIO_C8)  //LCD Chip Select
#define SET_LCD_RS ttc_gpio_set(TTC_GPIO_C9)  //LCD Reset Command
#define SET_LCD_WR ttc_gpio_set(TTC_GPIO_C10) //LCD Write Command Bit
#define SET_LCD_RD ttc_gpio_set(TTC_GPIO_C11) //LCD Read Command Bit
#endif
#endif

#ifdef EXTENSION_100_board_mini_stm32_rev30
#ifdef TARGET_ARCHITECTURE_STM32F1xx
#define CLR_LCD_CS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BRR),  8)) = 1  //LCD Chip Select
#define CLR_LCD_RS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BRR),  9)) = 1  //LCD Reset Command
#define CLR_LCD_WR *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BRR),  10)) = 1 //LCD Write Command Bit
#define CLR_LCD_RD *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BRR),  11)) = 1 //LCD Read Command Bit
#define SET_LCD_CS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BSRR), 8)) = 1  //LCD Chip Select
#define SET_LCD_RS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BSRR), 9)) = 1  //LCD Reset Command
#define SET_LCD_WR *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BSRR), 10)) = 1 //LCD Write Command Bit
#define SET_LCD_RD *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BSRR), 11)) = 1 //LCD Read Command Bit
#else
#define CLR_LCD_CS ttc_gpio_clr(TTC_GPIO_C8)  //LCD Chip Select
#define CLR_LCD_RS ttc_gpio_clr(TTC_GPIO_C9)  //LCD Reset Command
#define CLR_LCD_WR ttc_gpio_clr(TTC_GPIO_C10) //LCD Write Command Bit
#define CLR_LCD_RD ttc_gpio_clr(TTC_GPIO_C11) //LCD Read Command Bit
#define SET_LCD_CS ttc_gpio_set(TTC_GPIO_C8)  //LCD Chip Select
#define SET_LCD_RS ttc_gpio_set(TTC_GPIO_C9)  //LCD Reset Command
#define SET_LCD_WR ttc_gpio_set(TTC_GPIO_C10) //LCD Write Command Bit
#define SET_LCD_RD ttc_gpio_set(TTC_GPIO_C11) //LCD Read Command Bit
#endif
#endif

#ifdef EXTENSION_100_board_mini_stm32_rev40
#ifdef TARGET_ARCHITECTURE_STM32F1xx
#define CLR_LCD_CS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BRR),  9)) = 1  //LCD Chip Select
#define CLR_LCD_RS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BRR),  8)) = 1  //LCD Reset Command
#define CLR_LCD_WR *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BRR),  10)) = 1 //LCD Write Command Bit
#define CLR_LCD_RD *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BRR),  11)) = 1 //LCD Read Command Bit
#define SET_LCD_CS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BSRR), 9)) = 1  //LCD Chip Select
#define SET_LCD_RS *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BSRR), 8)) = 1  //LCD Reset Command
#define SET_LCD_WR *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BSRR), 10)) = 1 //LCD Write Command Bit
#define SET_LCD_RD *(cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOC)->BSRR), 11)) = 1 //LCD Read Command Bit
#else
#define CLR_LCD_CS ttc_gpio_clr(TTC_GPIO_C9)  //LCD Chip Select
#define CLR_LCD_RS ttc_gpio_clr(TTC_GPIO_C8)  //LCD Reset Command
#define CLR_LCD_WR ttc_gpio_clr(TTC_GPIO_C10) //LCD Write Command Bit
#define CLR_LCD_RD ttc_gpio_clr(TTC_GPIO_C11) //LCD Read Command Bit
#define SET_LCD_CS ttc_gpio_set(TTC_GPIO_C9)  //LCD Chip Select
#define SET_LCD_RS ttc_gpio_set(TTC_GPIO_C8)  //LCD Reset Command
#define SET_LCD_WR ttc_gpio_set(TTC_GPIO_C10) //LCD Write Command Bit
#define SET_LCD_RD ttc_gpio_set(TTC_GPIO_C11) //LCD Read Command Bit
#endif
#endif

//}Defines
//{ Function prototypes **************************************************

// initialize the LCD , white Background
void ili9320_init();

/* Send the information to the LCD-driver which Reg is asked */
// Write command to the TFT-driver which paramter will be read


// Reads the Reg-ID of the TFT-driver
t_u16 ili9320_read_register(t_u16 LCD_Reg);

//void ili9320_write_register(t_u16 LCD_Reg,t_u16 LCD_Dat);

void ili9320_delay (t_u32 nCount);

void ili9320_clear();

void ili9320_write_data(ut_int16 LCD_Dat);

void ili9320_set_color_fg8(t_u8 Color);

void ili9320_set_color_fg24(t_u32 Color);

void ili9320_set_color_bg24(t_u32 Color);

/** sets single pixel at current cursor position in forgeground color */


/** sets single pixel at current cursor position in background color */
void ili9320_clr_pixel_at();


void ili9320_put_string_solid(const char *String);

/** sets level of backlight
  *
  * @param Config  configuration data of display to use
  * @param Level   Brightness level (0 = off, 255 = max)
  */
void ili9320_set_backlight(t_u8 Level);

void ili9320_set_cursor(t_u16 X,t_u16 Y);

void ili9320_set_display_window(t_u8 Xpos, ut_int16 Ypos, t_u8 Height, ut_int16 Width);

void ili9320_rect_fill(t_u16 X, t_u16 Y, t_s16 W, t_s16 H);

void ili9320_get_configuration();

void ili9320_line_horizontal(t_u16 X, t_u16 Y, t_s16 Lenght);

void ili9320_line_vertical(t_u16 X,t_u16 Y,t_s16 Lenght);

void ili9320_circle(t_u16 CenterX, t_u16 CenterY, t_u16 Radius);

void ili9320_line(t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2);

void ili9320_repeat_last_data();

void ili9320_init_repeat_mode();

void ili9320_deinit_repeat_mode();


#ifdef EXTENSION_ttc_font  // font support available

void ili9320_put_char(t_u8 Char);
void ili9320_put_char_solid(t_u8 Character);

#endif
void ili9320_fsmc_init();

//}Function prototypes
//{ Macro support for ttc_gfx.h

#define _ttc_gfx_driver_set_pixel_at(X,Y)  ili9320_set_cursor(X, Y); ili9320_set_pixel()
#define _ttc_gfx_driver_clr_pixel_at(X,Y)  ili9320_set_cursor(X, Y); ili9320_clr_pixel_at()
#define _ttc_gfx_driver_set_cursor(X,Y)    ili9320_set_cursor(X, Y)

//}Macros
#endif // IL9320
