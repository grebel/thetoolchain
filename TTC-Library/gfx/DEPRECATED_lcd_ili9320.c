/*{ lcd_ili9320.c *******************************

      Low-level driver for LCD Controller ILI9320.

      written by Patrick von Poblotzki, Gregor Rebel 2012 - 2014

      Benchmark v1.0.50(5)
      benchmark_Clear(5)        4975 ms
      benchmark_Circle(5)      34086 ms
      benchmark_Line_ortho(5)  34297 ms
      benchmark_Line(5)        35196 ms
      benchmark_Rect(5)        31492 ms
      benchmark_Text(5)        22577 ms
      benchmark_SetPixel(5)    33842 ms
      benchmark_RectFill(5)    31159 ms
      sum:                     227624 ms

      Benchmark v1.0.50(5)
      changes: vert and hori lines are drawn with only one adress command
               command pins are controled by adress
               debug compile
      benchmark_Clear(5)       1251  ms
      benchmark_Circle(5)      8852  ms
      benchmark_Line_ortho(5)  8258  ms
      benchmark_Line(5)        9426  ms
      benchmark_Rect(5)        8209  ms
      benchmark_Text(5)        2359  ms
      benchmark_SetPixel(5)    8880  ms
      benchmark_RectFill(5)    8140  ms
      sum:                     55375 ms

      Benchmark v1.0.50(5)
      changes smallest compile
      benchmark_Clear(5)       1181  ms
      benchmark_Circle(5)      8549  ms
      benchmark_Line_ortho(5)  1478  ms
      benchmark_Line(5)        9543  ms
      benchmark_Rect(5)        1497  ms
      benchmark_Text(5)        2040  ms
      benchmark_SetPixel(5)    9032  ms
      benchmark_RectFill(5)    1176  ms
      sum:                     34888 ms

      Benchmark v1.0.50(5)
      register inline
      benchmark_Clear(5)       1181  ms
      benchmark_Circle(5)      8594  ms
      benchmark_Line_ortho(5)  1506  ms
      benchmark_Line(5)        9173  ms
      benchmark_Rect(5)        1533  ms
      benchmark_Text(5)        2023  ms
      benchmark_SetPixel(5)    8666  ms
      benchmark_RectFill(5)    1173  ms
      sum:                     33849 ms

      Benchmark v1.0.53(5)
      - new GPIO driver
      - changed LCD_* to constants TTC_GFX1_ILI9320_PIN_*
      - activate.060_basic_extensions_optimize_1_debug.sh
      benchmark_Clear(5)         799 ms Run0
      benchmark_Circle(5)       7895 ms Run1
      benchmark_CircleFill(5)   6462 ms Run2
      benchmark_Line_ortho(5)   1175 ms Run3
      benchmark_Line(5)        10231 ms Run4
      benchmark_Rect(5)         1205 ms Run5
      benchmark_Text(5)         2723 ms Run6
      benchmark_SetPixel(5)    10691 ms Run7
      benchmark_RectFill(5)      875 ms Run8
      sum:                     42056 ms

    }*/

#include "lcd_ili9320.h"
#include "stm32f10x_fsmc.h"
// define parallel banks to use for parallel output in this example
#ifdef TTC_PARALLEL16_1_BANK_
#define PARALLEL16_BANK TTC_PARALLEL16_1_BANK
#else
// using 8-bit ports if no 16-bit port available
#ifdef TTC_PARALLEL08_1_BANK
#define PARALLEL08_1_BANK     TTC_PARALLEL08_1_BANK
#define PARALLEL08_1_FIRSTPIN TTC_PARALLEL08_1_FIRSTPIN
#endif
#ifdef TTC_PARALLEL08_2_BANK
#define PARALLEL08_2_BANK     TTC_PARALLEL08_2_BANK
#define PARALLEL08_2_FIRSTPIN TTC_PARALLEL08_2_FIRSTPIN
#endif
#endif


/** DEPRECATED

// pin configuration
e_ttc_gpio_pin LCD_ChipSelect = E_ttc_gpio_pin_unknown;
e_ttc_gpio_pin LCD_ReadMode   = E_ttc_gpio_pin_unknown;
e_ttc_gpio_pin LCD_WriteMode  = E_ttc_gpio_pin_unknown;
e_ttc_gpio_pin LCD_Reset      = E_ttc_gpio_pin_unknown;

#ifdef TARGET_ARCHITECTURE_STM32F1xx
volatile t_u32* LCD_ChipSelect_BRR  = NULL;
volatile t_u32* LCD_ReadMode_BRR    = NULL;
volatile t_u32* LCD_WriteMode_BRR   = NULL;
volatile t_u32* LCD_Reset_BRR       = NULL;
volatile t_u32* LCD_ChipSelect_BSRR = NULL;
volatile t_u32* LCD_ReadMode_BSRR   = NULL;
volatile t_u32* LCD_WriteMode_BSRR  = NULL;
volatile t_u32* LCD_Reset_BSRR      = NULL;
#endif
*/

// minimal interface for ttc_gfx.c (must be implemented)
void ili9320_get_configuration() {

    if (! ttc_gfx_CurrentDisplay->Width)  ttc_gfx_CurrentDisplay->Width  = 320;
    if (! ttc_gfx_CurrentDisplay->Height) ttc_gfx_CurrentDisplay->Height = 240;
    if (! ttc_gfx_CurrentDisplay->Depth)  ttc_gfx_CurrentDisplay->Depth  = 24;

    // fill in mandatory function pointers
    ttc_gfx_CurrentDisplay->function_set_color_fg24 = ili9320_set_color_fg24;
    ttc_gfx_CurrentDisplay->function_set_color_bg24 = ili9320_set_color_bg24;

    // fill in optional function pointers
    ttc_gfx_CurrentDisplay->function_set_backlight  = ili9320_set_backlight;
    ttc_gfx_CurrentDisplay->function_clear          = ili9320_clear;
    ttc_gfx_CurrentDisplay->function_line_horizontal= ili9320_line_horizontal;
    ttc_gfx_CurrentDisplay->function_line_vertical  = ili9320_line_vertical;
    ttc_gfx_CurrentDisplay->function_rect_fill      = ili9320_rect_fill;
    ttc_gfx_CurrentDisplay->function_circle         = ili9320_circle;
    ttc_gfx_CurrentDisplay->function_line           = ili9320_line;
#ifdef EXTENSION_ttc_font  // font attributes
    ttc_gfx_CurrentDisplay->function_put_char       = ili9320_put_char;
    ttc_gfx_CurrentDisplay->function_put_char_solid = ili9320_put_char_solid;
#endif
}
inline void ili9320_clr_pixel_at() {
    // set pixel
    ili9320_write_command(0x22);             // Command for write dot a activ position
    ili9320_write_data(ttc_gfx_CurrentColorBg);    // Color-data for the point
}
void ili9320_set_color_fg24(t_u32 Color) {
    ttc_gfx_CurrentDisplay->ColorFg = ttc_gfx_CurrentColorFg = ( (((Color>>3)&0x1F) |( (Color>>5)&0x7E0 )| ((Color>>8)&0xF800)) );
}
void ili9320_set_color_bg24(t_u32 Color) {
    ttc_gfx_CurrentDisplay->ColorBg = ttc_gfx_CurrentColorBg = ( (((Color>>3)&0x1F) |( (Color>>5)&0x7E0 )| ((Color>>8)&0xF800)) );
}
void ili9320_set_cursor(t_u16 X, t_u16 Y) {
    ili9320_set_x(X);
    ili9320_set_y(Y);
}

// extended interface for ttc_gfx.c (can be implemented)
void ili9320_clear() {
    GFX_STATIC t_u16 i9c_ForegroundColor; i9c_ForegroundColor = ttc_gfx_CurrentDisplay->ColorBg;

    ili9320_write_register(0x50,0);           // Set X - Window Start
    ili9320_write_register(0x51,ttc_gfx_CurrentDisplay->Width -1);         // Set X - Windows End
    ili9320_write_register(0x52,0);   	      // Set Y - Windows Start
    ili9320_write_register(0x53,ttc_gfx_CurrentDisplay->Height - 1);         // Set Y - Window End
    ili9320_write_command(0x22);              // prepare GRAM for writing

    //mass sending off data to the driver, runtime shorten by mass-orders of 120x (ili9320_write_data(Color);)
    t_base PixelCount = ttc_gfx_CurrentDisplay->Height * ttc_gfx_CurrentDisplay->Width;

    while (PixelCount & 0xf) { // make it dividable by 16
        ili9320_write_data(i9c_ForegroundColor);
        PixelCount--;
    }
    ili9320_write_data(i9c_ForegroundColor);
    ili9320_init_repeat_mode();
    while (PixelCount > 0) {
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        PixelCount -= 16;
    }
     ili9320_deinit_repeat_mode();
}
void ili9320_set_backlight(t_u8 Level) {
    (void) Level;
}
void ili9320_rect_fill(t_u16 X, t_u16 Y, t_s16 W, t_s16 H) {

    if (W < 0) {
        X += W;
        W = -W;
    }
    if (H < 0) {
        Y += H;
        H = -H;
    }
    ili9320_write_register(0x50,X);           //Set X - Window Start
    ili9320_write_register(0x51,X+W-1);         //Set X - Windows End
    ili9320_write_register(0x52,Y);   	 //Set Y - Windows Start
    ili9320_write_register(0x53,Y+H-1);         //Set Y - Window End
    ili9320_write_command(0x22);
    ili9320_set_cursor(X,Y);
    ili9320_write_command(0x22);
    t_u16 ForegroundColor = ttc_gfx_CurrentDisplay->ColorFg;

    t_base PixelCount = W*H;

    while (PixelCount & 0xf) { // make it dividable by 16
        ili9320_write_data(ForegroundColor);
        PixelCount--;
    }
    while (PixelCount > 0) {
        ili9320_write_data(ForegroundColor);
#ifdef EXTENSION_board_olimex_lcd
        ili9320_write_data(ForegroundColor);
        ili9320_write_data(ForegroundColor);
        ili9320_write_data(ForegroundColor);
        ili9320_write_data(ForegroundColor);
        ili9320_write_data(ForegroundColor);
        ili9320_write_data(ForegroundColor);
        ili9320_write_data(ForegroundColor);
        ili9320_write_data(ForegroundColor);
        ili9320_write_data(ForegroundColor);
        ili9320_write_data(ForegroundColor);
        ili9320_write_data(ForegroundColor);
        ili9320_write_data(ForegroundColor);
        ili9320_write_data(ForegroundColor);
        ili9320_write_data(ForegroundColor);
        ili9320_write_data(ForegroundColor);
#else
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
        ili9320_repeat_last_data();
#endif
        PixelCount -= 16;
    }

    ili9320_write_register(0x50,0);           //Set X - Window Start
    ili9320_write_register(0x51,239);         //Set X - Windows End
    ili9320_write_register(0x52,0);   	 //Set Y - Windows Start
    ili9320_write_register(0x53,359);         //Set Y - Window End
}

#ifdef EXTENSION_ttc_font  // font support available
void ili9320_put_char(t_u8 Char) {
    GFX_STATIC t_u16 i9pc_BitLine;
    GFX_STATIC  t_u8 i9pc_LineNo;
    GFX_STATIC t_u16 i9pc_X;
    GFX_STATIC t_u16 i9pc_Y;              i9pc_Y = ttc_gfx_CurrentDisplay->CursorY;
    GFX_STATIC const t_u16* i9pc_Font; i9pc_Font = ttc_gfx_CurrentDisplay->Font->Characters + (Char - 32) * 24;

    for (i9pc_LineNo = 0; i9pc_LineNo < 24; i9pc_LineNo++)
    {
        i9pc_BitLine = *i9pc_Font++;

        if (i9pc_BitLine) {
            i9pc_X = ttc_gfx_CurrentDisplay->CursorX;
            do {
                if (i9pc_BitLine & 1) {
                    ili9320_write_register(0x20, i9pc_X);
                    ili9320_write_register(0x21, i9pc_Y);           // Set cursor on the LCD
                    ili9320_set_pixel();
                }
                i9pc_X++;
                i9pc_BitLine /= 2;
            } while (i9pc_BitLine);
        }
        i9pc_Y++;
    }
}
void ili9320_put_char_solid(t_u8 Character) {
    t_u16 k,line;
    t_u16 CurrentX = ttc_gfx_CurrentDisplay->CursorX;
    t_u16 CurrentY = ttc_gfx_CurrentDisplay->CursorY;

    ili9320_set_cursor(CurrentX, CurrentY);
    ili9320_write_register(0x50, CurrentX);           // Set X - Window Start
    ili9320_write_register(0x51, CurrentX+15);        // Set X - Windows End
    ili9320_write_register(0x52, CurrentY);   	     // Set Y - Windows Start
    ili9320_write_register(0x53, CurrentY+23);        // Set Y - Window End

    ili9320_write_command(0x22); //
    t_u16 BackgroundColor    = ttc_gfx_CurrentDisplay->ColorBg;
    t_u16 ForegroundColor    = ttc_gfx_CurrentDisplay->ColorFg;
    const t_u16* ttc_gfx_Display.Font = ttc_gfx_CurrentDisplay->Font->Characters;

    for (k=0; k<21; k++) {
        line = ttc_gfx_Display.Font[k+((Character-32)*24)];

        for ( t_u32 BitMask = 1; BitMask < 0x10000; BitMask = BitMask << 1) {
            if (line & BitMask) {
                ili9320_write_data(ForegroundColor);
            }
            else {
                ili9320_write_data(BackgroundColor);
            }
        }
    }

    ili9320_write_register(0x50,0);           //Set X - Window Start
    ili9320_write_register(0x51,239);         //Set X - Window End
    ili9320_write_register(0x52,0);   	      //Set Y - Window Start
    ili9320_write_register(0x53,319);         //Set Y - Window End
}
#endif


// private support  functions
void ili9320_set_display_window(t_u8 Xpos, t_u16 Ypos, t_u8 Height, t_u16 LI9320_WIDTH) {
    ili9320_write_register(0x50, Xpos);	   	   	//Partial Image 1 Display Position to X-pos
    ili9320_write_register(0x51, Xpos+Height);                 //Partial Image 1 Area (Start Line) to Xpos+Height
    ili9320_write_register(0x52, Ypos);		  	//Partial Image 1 Area (End Line) to Ypos
    ili9320_write_register(0x53, Ypos+LI9320_WIDTH);                  //Partial Image 2 Display Position Ypos+LI9320_WIDTH

    ili9320_set_cursor(Xpos, Ypos);			//Set Cursor on xpos, ypos
}

void ili9320_delay(t_u32 nCount) {
    //Sleep function
    //? for(t_u16 i=0;i++;i<=nCount*10);
}

void ili9320_line_horizontal(t_u16 X, t_u16 Y, t_s16 Lenght)
{
    GFX_STATIC t_u32 i9lh_Count;

    ili9320_set_cursor(X,Y);
    ili9320_write_command(0x22);
    ili9320_write_data(ttc_gfx_CurrentDisplay->ColorFg);
    for(i9lh_Count=1;i9lh_Count<=Lenght;i9lh_Count++)
    {
#ifdef EXTENSION_board_olimex_lcd
        ili9320_write_data(ttc_gfx_CurrentDisplay->ColorFg);
#else
        ili9320_repeat_last_data();
#endif
    }
}

void ili9320_line_vertical(t_u16 X,t_u16 Y,t_s16 Lenght)
{
    GFX_STATIC t_u32 i9lv_Count;
    ili9320_write_register(0x03,0x1038);
    ili9320_set_cursor(X,Y);
    ili9320_write_command(0x22);
    ili9320_write_data(ttc_gfx_CurrentDisplay->ColorFg);
    for(i9lv_Count=1;i9lv_Count<=Lenght;i9lv_Count++)
    {
#ifdef EXTENSION_board_olimex_lcd
        ili9320_write_data(ttc_gfx_CurrentDisplay->ColorFg);
#else
        ili9320_repeat_last_data();
#endif
    }
    ili9320_write_register(0x03,0x1030);
}

void ili9320_circle(t_u16 CenterX, t_u16 CenterY, t_u16 Radius) {

    GFX_STATIC int i9c_F;     i9c_F = 1 - Radius;
    GFX_STATIC int i9c_ddF_x; i9c_ddF_x = 0;
    GFX_STATIC int i9c_ddF_y; i9c_ddF_y = -2 * Radius;
    GFX_STATIC int i9c_X;     i9c_X = 0;
    GFX_STATIC int i9c_Y;     i9c_Y = Radius;

    _ttc_gfx_driver_set_pixel_at(CenterX, CenterY + Radius);
    ili9320_set_y(CenterY - Radius);
    ili9320_set_pixel();
    _ttc_gfx_driver_set_pixel_at(CenterX + Radius, CenterY);
    ili9320_set_x(CenterX - Radius);
    ili9320_set_pixel();



    while (i9c_X < i9c_Y) {
        if (i9c_F >= 0) {
            i9c_Y--;
            i9c_ddF_y += 2;
            i9c_F += i9c_ddF_y;
        }
        i9c_X++;
        i9c_ddF_x += 2;
        i9c_F += i9c_ddF_x + 1;
        ili9320_set_cursor(CenterX + i9c_X,CenterY + i9c_Y);
        ili9320_set_pixel();
        ili9320_set_x(CenterX - i9c_X);
        ili9320_set_pixel();
        ili9320_set_y(CenterY - i9c_Y);
        ili9320_set_pixel();
        ili9320_set_x(CenterX + i9c_X);
        ili9320_set_pixel();

        ili9320_set_cursor(CenterX + i9c_Y,CenterY + i9c_X);
        ili9320_set_pixel();
        ili9320_set_x( CenterX - i9c_Y);
        ili9320_set_pixel();
        ili9320_set_y( CenterY - i9c_X);
        ili9320_set_pixel();
        ili9320_set_x( CenterX + i9c_Y);
        ili9320_set_pixel();


    }
}

void ili9320_line(t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2) {
    if (X2 == X1) {
        ili9320_line_vertical(X1, Y1, Y2);
        return;
    }
    if (Y2 == Y1) {
        ili9320_line_horizontal(X1, Y1, X2);
        return;
    }

    GFX_STATIC t_u16 i9l_T;
    GFX_STATIC t_s16 i9l_XError, i9l_YError, i9l_DeltaX, i9l_DeltaY, i9l_Distance;
    GFX_STATIC t_s16 i9l_IncrementX, i9l_IncrementY;

    i9l_XError = 0;
    i9l_YError = 0;
    i9l_DeltaX = X2 - X1;
    i9l_DeltaY = Y2 - Y1;

    if (i9l_DeltaX > 0)
        i9l_IncrementX = 1;
    else if (i9l_DeltaX == 0)
        i9l_IncrementX = 0;
    else
    { i9l_IncrementX = -1; i9l_DeltaX = -i9l_DeltaX; }

    if (i9l_DeltaY > 0)
        i9l_IncrementY = 1;
    else if (i9l_DeltaY == 0)
        i9l_IncrementY = 0;
    else
    { i9l_IncrementY = -1; i9l_DeltaY = -i9l_DeltaY; }

    if ( i9l_DeltaX > i9l_DeltaY )
        i9l_Distance = i9l_DeltaX;
    else
        i9l_Distance = i9l_DeltaY;


    /* if (delta_x > 0) {
            incx = 1;
            if (delta_y > 0) { // increment x and y
                incy = 1;
                for (t = 0; t <= distance + 1; t++) {
                    _ttc_gfx_driver_set_pixel_at(X1, Y1);
                    xerr += delta_x ;
                    yerr += delta_y ;
                    if (xerr > distance) {
                        xerr -= distance;
                        if (xerr > distance) {
                            xerr -= distance;
                            X1 += incx;
                        }
                        if (yerr > distance) {
                            yerr -= distance;
                            Y1 += incy;
                        }
                    }
                }
                else { // decrement y, increment x
                    incy = -1; delta_y = -delta_y;
                    for (t = 0; t <= distance + 1; t++) {
                        _ttc_gfx_driver_set_pixel_at(X1, Y1);
                        xerr += delta_x ;
                        yerr += delta_y ;
                        if (xerr > distance) {
                            xerr -= distance;
                            if (xerr > distance) {
                                xerr -= distance;
                                X1 += incx;
                            }
                            if (yerr > distance) {
                                yerr -= distance;
                                Y1 += incy;
                            }
                        }
                    }
                }
                else {
                    incx = -1; delta_x = -delta_x;
                    if (delta_y > 0) { // decrement x and increment y
                        incy = 1;
                        for (t = 0; t <= distance + 1; t++) {

                            xerr += delta_x ;
                            yerr += delta_y ;
                            if (xerr > distance) {
                                xerr -= distance;
                                X1 += incx;
                            }
                            if (yerr > distance) {
                                yerr -= distance;
                                Y1 += incy;
                            }
                        }
                    }
                }
                else { // decrement x, decrement y
                    incy = -1; delta_y = -delta_y;
                    for (t = 0; t <= distance + 1; t++) {
                        _ttc_gfx_driver_set_pixel_at(X1, Y1);
                        xerr += delta_x ;
                        yerr += delta_y ;
                        if (xerr > distance) {
                            xerr -= distance;
                            X1--;
                            if (yerr > distance) {
                                yerr -= distance;
                                Y1++;
                                _ttc_gfx_driver_set_pixel_at(X1, Y1);
                            }
                            else {
                                ili9320_set_x(X1);
                                ili9320_set_pixel();
                            }
                        }
                        else {
                            if (yerr > distance) {
                                yerr -= distance;
                                Y1++;
                                ili9320_set_y(Y1);
                                ili9320_set_pixel();
                            }
                            else {
                                ili9320_set_pixel();
                            }
                        }
                    }
                }
            }
           */
    _ttc_gfx_driver_set_pixel_at(X1, Y1);
    for (i9l_T = 0; i9l_T <= i9l_Distance + 1; i9l_T++) {

        i9l_XError += i9l_DeltaX ;
        i9l_YError += i9l_DeltaY ;
        if (i9l_XError > i9l_Distance) {
            i9l_XError -= i9l_Distance;
            X1 += i9l_IncrementX;
            ili9320_set_x(X1);
        }
        if (i9l_YError > i9l_Distance) {
            i9l_YError -= i9l_Distance;
            Y1 += i9l_IncrementY;
            ili9320_set_y(Y1);
        }
        ili9320_set_pixel();
    }

}

#ifdef EXTENSION_board_olimex_lcd

t_u16 ili9320_read_register(t_u16 LCD_Reg) {

    *LCDBUS_RSLOW_ADDR = LCD_Reg;

    return *LCDBUS_RSHIGH_ADDR;
}
/*
    void ili9320_write_command(t_u16 LCD_Dat){
        *LCDBUS_RSLOW_ADDR = LCD_Dat;
    }
    */
void ili9320_write_data(t_u16 LCD_Dat){
    *LCDBUS_RSHIGH_ADDR = LCD_Dat;
}



void ili9320_init(){
    //{ init_LCD Start ToDo: replace by ttc function calls



    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    GPIO_Init(GPIOE, &GPIO_InitStructure);
    GPIO_Init(GPIOF, &GPIO_InitStructure);
    GPIO_Init(GPIOG, &GPIO_InitStructure);

    /* Enable the FSMC Clock */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_FSMC, ENABLE);

    GFX_STATIC t_u16 i9i_DeviceCode=0x9325; //int for the Device Code of the TFT-drive
    ili9320_fsmc_init();
/** DEPRECATED
    LCD_ChipSelect = ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_ChipSelect = TTC_GFX1_ILI9320_PIN_CHIPSELECT;
    LCD_Reset      = ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_WriteMode  = TTC_GFX1_ILI9320_PIN_WRITEMODE;
    LCD_WriteMode  = ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_Reset      = TTC_GFX1_ILI9320_PIN_RESET;
*/
    ttc_gpio_init(TTC_GFX1_ILI9320_PIN_CHIPSELECT, tgm_output_push_pull, tgs_Max);
    ttc_gpio_init(TTC_GFX1_ILI9320_PIN_WRITEMODE,  tgm_output_push_pull, tgs_Max);
    ttc_gpio_init(TTC_GFX1_ILI9320_PIN_RESET,      tgm_output_push_pull, tgs_Max);

/** DEPRECATED
    ttc_gpio_init_variable(&(ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_ChipSelect), TTC_GPIO_E3, tgm_output_push_pull, tgs_Max);
    ttc_gpio_init_variable(&(ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_WriteMode),  TTC_GPIO_D5, tgm_output_push_pull, tgs_Max);
    ttc_gpio_init_variable(&(ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_Reset),      TTC_GPIO_D7, tgm_output_push_pull, tgs_Max);

    LCD_ChipSelect = ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_ChipSelect;
    LCD_Reset      = ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_Reset;
    LCD_WriteMode  = ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_WriteMode;

    LCD_ChipSelect_BRR = LCD_ChipSelect->BRR;
    LCD_Reset_BRR = LCD_Reset->BRR;
    LCD_WriteMode_BRR = LCD_WriteMode->BRR;
    LCD_ChipSelect_BSRR = LCD_ChipSelect->BSRR;
    LCD_Reset_BSRR = LCD_Reset->BSRR;
    LCD_WriteMode_BSRR = LCD_WriteMode->BSRR;
*/
    ili9320_deinit_repeat_mode();

    ttc_gpio_init(TTC_GPIO_E2, tgm_output_push_pull, tgs_Max);
    ttc_gpio_init(TTC_GPIO_D13,tgm_output_push_pull, tgs_Max);
    ttc_gpio_set(TTC_GPIO_D13);
    ili9320_delay(1);
    ttc_gpio_clr(TTC_GPIO_E2);
    ili9320_delay(1);
    ttc_gpio_set(TTC_GPIO_E2);
    ili9320_delay(1);
    ttc_gpio_clr(TTC_GPIO_D13);

    ili9320_write_register(0x0000,0x0001); //command for reading the register
    ili9320_delay(1);                 // LCD_Delay 5 ms
    i9i_DeviceCode = ili9320_read_register(0x0000); //save Readreg to var. DeviceCode
    ili9320_delay(1);
    ili9320_write_register(0x0000,0x0001);
    i9i_DeviceCode = ili9320_read_register(0x0000); //save Readreg to var. DeviceCode
    ili9320_delay(1);

    //power configuration, not optimized, for more information ->manual
    if (i9i_DeviceCode==0x9325 || i9i_DeviceCode==0x9328)//ILI9325
    {

        ili9320_write_register(0x0001,0x0100);       //Driver Output Contral.
        ili9320_write_register(0x0002,0x0700);       //LCD Driver Waveform Contral.
        ili9320_write_register(0x0003,0x1030);       //Entry Mode Set.
        ili9320_write_register(0x0004,0x0000);       //Scalling Control.
        ili9320_write_register(0x0007,0x0133);       //Display Control.
        ili9320_write_register(0x0008,0x0202);       //Display Contral 2.(0x0207)
        ili9320_write_register(0x0009,0x0000);       //Display Contral 3.(0x0000)
        ili9320_write_register(0x000a,0x0000);       //Frame Cycle Control.(0x0000)
        ili9320_write_register(0x000c,0x0001);       //Extern Display Interface Control 1.(0x0000) = 16 bit
        ili9320_write_register(0x000d,0x0000);       //Frame Maker Position.
        ili9320_write_register(0x000f,0x0000);       //Extern Display Interface Control 2.
        ili9320_write_register(0x0010,0x1690);       //Power Control 1.(0x16b0)
        ili9320_write_register(0x0011,0x0227);       //Power Control 2.(0x0001)
        ili9320_write_register(0x0012,0x009d);       //Power Control 3.(0x0138)
        ili9320_write_register(0x0013,0x1900);       //Power Control 4.
        ili9320_write_register(0x0020,0x0000);
        ili9320_write_register(0x0021,0x0000);
        ili9320_write_register(0x0029,0x0025);
        ili9320_write_register(0x002b,0x000d);
        ili9320_write_register(0x0030,0x0007);
        ili9320_write_register(0x0031,0x0303);
        ili9320_write_register(0x0032,0x0003);
        ili9320_write_register(0x0035,0x0206);
        ili9320_write_register(0x0036,0x0008);
        ili9320_write_register(0x0037,0x0406);
        ili9320_write_register(0x0038,0x0304);
        ili9320_write_register(0x0039,0x0007);
        ili9320_write_register(0x003c,0x0602);
        ili9320_write_register(0x003d,0x0008);
        ili9320_write_register(0x0050,0x0000);       //Set X - Window Start
        ili9320_write_register(0x0051,0x00ef);       //Set X - Window END
        ili9320_write_register(0x0052,0x0000);       //Set Y - Window Start
        ili9320_write_register(0x0053,0x013f);       //Set Y - Window END
        ili9320_write_register(0x0060,0xa700);
        ili9320_write_register(0x0061,0x0001);
        ili9320_write_register(0x006a,0x0000);
        ili9320_write_register(0x0080,0x0000);
        ili9320_write_register(0x0081,0x0000);
        ili9320_write_register(0x0082,0x0000);
        ili9320_write_register(0x0083,0x0000);
        ili9320_write_register(0x0084,0x0000);
        ili9320_write_register(0x0085,0x0000);
        ili9320_write_register(0x0090,0x0010);
        ili9320_write_register(0x0092,0x0600);
        ili9320_write_register(0x00e5,0x78F0);       //LCD_WriteReg(0x0000,0x0001);

    }
    else if (i9i_DeviceCode==0x9320 || i9i_DeviceCode==0x9300|| i9i_DeviceCode==0x9324||i9i_DeviceCode==0x1320||i9i_DeviceCode==0x9125)
    {
        ili9320_write_register(0x00,0x0000);
        ili9320_write_register(0x01,0x0100);	//Driver Output Contral.
        ili9320_write_register(0x02,0x0700);	//LCD Driver Waveform Contral.
        ili9320_write_register(0x03,0x1030);    //Entry Mode Set.
        ili9320_write_register(0x04,0x0000);	//Scalling Control.
        ili9320_write_register(0x08,0x0202);	//Display Contral 2.(0x0207)
        ili9320_write_register(0x09,0x0000);	//Display Contral 3.(0x0000)
        ili9320_write_register(0x0a,0x0000);	//Frame Cycle Contal.(0x0000)
        ili9320_write_register(0x0c,(1<<0));	//Extern Display Interface Control 1.(0x0000) = 16 bit
        ili9320_write_register(0x0d,0x0000);	//Frame Maker Position.
        ili9320_write_register(0x0f,0x0000);	//Extern Display Interface Control 2.
        //   ili9320_delay(10);
        ili9320_write_register(0x07,0x0101);	//Display Control.
        //   ili9320_delay(10);
        ili9320_write_register(0x10,(1<<12)|(0<<8)|(1<<7)|(1<<6)|(0<<4));                         	//Power Control 1.(0x16b0)
        ili9320_write_register(0x11,0x0007);								//Power Control 2.(0x0001)
        ili9320_write_register(0x12,(1<<8)|(1<<4)|(0<<0));                        			//Power Control 3.(0x0138)
        ili9320_write_register(0x13,0x0b00);								//Power Control 4.
        ili9320_write_register(0x29,0x0000);								//Power Control 7.
        ili9320_write_register(0x2b,(1<<14)|(1<<4));
        ili9320_write_register(0x50,0);         //Set X - Window Start
        ili9320_write_register(0x51,239);       //Set X - Windows End
        ili9320_write_register(0x52,0);   	    //Set Y - Windows Start
        ili9320_write_register(0x53,319);       //Set Y - Window End
        ili9320_write_register(0x60,0x2700);	//Driver Output Control.
        ili9320_write_register(0x61,0x0001);	//Driver Output Control.
        ili9320_write_register(0x6a,0x0000);	//Vertical Srcoll Control.
        ili9320_write_register(0x80,0x0000);	//Display Position? Partial Display 1.
        ili9320_write_register(0x81,0x0000);	//RAM Address Start? Partial Display 1.
        ili9320_write_register(0x82,0x0000);	//RAM Address End-Partial Display 1.
        ili9320_write_register(0x83,0x0000);	//Displsy Position? Partial Display 2.
        ili9320_write_register(0x84,0x0000);	//RAM Address Start? Partial Display 2.
        ili9320_write_register(0x85,0x0000);	//RAM Address End? Partial Display 2.
        ili9320_write_register(0x90,(0<<7)|(16<<0));	//Frame Cycle Contral.(0x0013)
        ili9320_write_register(0x92,0x0000);	//Panel Interface Contral 2.(0x0000)
        ili9320_write_register(0x93,0x0001);	//Panel Interface Contral 3.
        ili9320_write_register(0x95,0x0110);	//Frame Cycle Contral.(0x0110)
        ili9320_write_register(0x97,(0<<8));	//
        ili9320_write_register(0x98,0x0000);	//Frame Cycle Contral.
        ili9320_write_register(0x07,0x0173);	//(0x0173)
        //  ili9320_delay(10);
    }

    //first cleanup of the display
    ili9320_clear();

}

void ili9320_fsmc_init() {
    FSMC_NORSRAMInitTypeDef  FSMC_NORSRAMInitStructure;
    FSMC_NORSRAMTimingInitTypeDef  pw,pr;
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOG | RCC_APB2Periph_GPIOE |
                           RCC_APB2Periph_GPIOF, ENABLE);

    /*-- GPIO Configuration ------------------------------------------------------*/
    /* SRAM Data lines configuration */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_8 | GPIO_Pin_9 |
            GPIO_Pin_10 | GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = tgs_Max;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 |
            GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 |
            GPIO_Pin_15;
    GPIO_Init(GPIOE, &GPIO_InitStructure);

    //TODO - get rid of address lines!!!
    /* SRAM Address lines configuration */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 |
            GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_12 | GPIO_Pin_13 |
            GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_Init(GPIOF, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 |
            GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_Init(GPIOG, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
    GPIO_Init(GPIOE, &GPIO_InitStructure);

    /* NOE and NWE configuration */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 |GPIO_Pin_5;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

    /* NE1 configuration */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
    GPIO_Init(GPIOD, &GPIO_InitStructure);


    /* NBL0, NBL1 configuration */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
    GPIO_Init(GPIOE, &GPIO_InitStructure);

    /*-- FSMC Configuration ------------------------------------------------------*/
    pr.FSMC_AddressSetupTime = (1/T_HCK+1);
    pr.FSMC_AddressHoldTime = (1/T_HCK+1);
    pr.FSMC_DataSetupTime = (100/T_HCK+1);
    pr.FSMC_BusTurnAroundDuration = 0;
    pr.FSMC_CLKDivision = 0;
    pr.FSMC_DataLatency = 0;
    pr.FSMC_AccessMode = FSMC_AccessMode_A;

    pw.FSMC_AddressSetupTime = (1/T_HCK+1);
    pw.FSMC_AddressHoldTime = (1/T_HCK+1);
    pw.FSMC_DataSetupTime = ((35)/T_HCK+1);
    pw.FSMC_BusTurnAroundDuration = 0;
    pw.FSMC_CLKDivision = 0;
    pw.FSMC_DataLatency = 0;
    pw.FSMC_AccessMode = FSMC_AccessMode_A;

    FSMC_NORSRAMInitStructure.FSMC_Bank = FSMC_Bank1_NORSRAM2;
    FSMC_NORSRAMInitStructure.FSMC_DataAddressMux = FSMC_DataAddressMux_Disable;
    FSMC_NORSRAMInitStructure.FSMC_MemoryType = FSMC_MemoryType_SRAM;
    FSMC_NORSRAMInitStructure.FSMC_MemoryDataWidth = FSMC_MemoryDataWidth_16b;
    FSMC_NORSRAMInitStructure.FSMC_BurstAccessMode = FSMC_BurstAccessMode_Disable;
    FSMC_NORSRAMInitStructure.FSMC_WaitSignalPolarity = FSMC_WaitSignalPolarity_Low;
    FSMC_NORSRAMInitStructure.FSMC_WrapMode = FSMC_WrapMode_Disable;
    FSMC_NORSRAMInitStructure.FSMC_WaitSignalActive = FSMC_WaitSignalActive_BeforeWaitState;
    FSMC_NORSRAMInitStructure.FSMC_WriteOperation = FSMC_WriteOperation_Enable;
    FSMC_NORSRAMInitStructure.FSMC_WaitSignal = FSMC_WaitSignal_Disable;
    FSMC_NORSRAMInitStructure.FSMC_ExtendedMode = FSMC_ExtendedMode_Enable;
    FSMC_NORSRAMInitStructure.FSMC_WriteBurst = FSMC_WriteBurst_Enable;
    FSMC_NORSRAMInitStructure.FSMC_ReadWriteTimingStruct = &pr;
    FSMC_NORSRAMInitStructure.FSMC_WriteTimingStruct = &pw;

    FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure);

    /* Enable FSMC Bank1_SRAM Bank */
    FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM2, ENABLE);
}

void ili9320_repeat_last_data(){
        ttc_gpio_set(TTC_GPIO_E3);                                  // pull LCD_Register-Select to zero -> now the driver expect data which he will interprent as command
        ttc_gpio_clr(TTC_GPIO_D5);                              // pull LCD-Chip-Select to zero -> chip-select is activated
        ttc_gpio_set(TTC_GPIO_D5);                                  // pull LCD_Register-Select to zero -> now the driver expect data which he will interprent as command

}

void ili9320_init_repeat_mode(){
    ttc_gpio_init(TTC_GPIO_E3,tgm_output_push_pull, tgs_Max);
    ttc_gpio_init(TTC_GPIO_D7,tgm_output_push_pull, tgs_Max);
    ttc_gpio_init(TTC_GPIO_D5,tgm_output_push_pull, tgs_Max);
}

void ili9320_deinit_repeat_mode(){
    ttc_gpio_init(TTC_GPIO_E3,tgm_alternate_function_push_pull, tgs_Max);
    ttc_gpio_init(TTC_GPIO_D7,tgm_alternate_function_push_pull, tgs_Max);
    ttc_gpio_init(TTC_GPIO_D5,tgm_alternate_function_push_pull, tgs_Max);
}

#else

t_u16 ili9320_read_register(t_u16 LCD_Reg) {
    t_u16 temp; //initialize the variable temp which will be returned
    ili9320_write_command(LCD_Reg);  // order the driver to send data

    //init BANKS as input
    ttc_gpio_init_u8_shift(PARALLEL08_1_BANK,PARALLEL08_1_FIRSTPIN,tgm_input_floating,tgs_Max);
    ttc_gpio_init_u8_shift(PARALLEL08_2_BANK,PARALLEL08_2_FIRSTPIN,tgm_input_floating,tgs_Max);
//X #ifdef TARGET_ARCHITECTURE_STM32F1xx
    ttc_gpio_clr(TTC_GFX1_ILI9320_PIN_CHIPSELECT);                              //pull LCD_CS to zero -> beginning open the read mode
    ttc_gpio_set(TTC_GFX1_ILI9320_PIN_RESET);                                  //push LCD_RS to iovcc
    ttc_gpio_clr(TTC_GFX1_ILI9320_PIN_READMODE);
/** DEPRECATED
 #else
    ttc_gpio_clr(LCD_ChipSelect);                         //pull LCD_CS to zero -> beginning open the read mode
    ttc_gpio_set(LCD_Reset);                              //push LCD_RS to iovcc
    ttc_gpio_clr(LCD_ReadMode);
#endif
*/
    temp=(ttc_gpio_get_u8_shift(TTC_GPIO_BANK_C,0) )| (ttc_gpio_get_u8_shift(TTC_GPIO_BANK_B,8)<<8);//saves sended data to temp
//X #ifdef TARGET_ARCHITECTURE_STM32F1xx
    ttc_gpio_set(TTC_GFX1_ILI9320_PIN_READMODE);                                //closing of the read-mode of the TFT-driver
    ttc_gpio_set(TTC_GFX1_ILI9320_PIN_CHIPSELECT);
/** DEPRECATED
#else
    ttc_gpio_set_variable(LCD_ReadMode);                  //closing of the read-mode of the TFT-driver
    ttc_gpio_set_variable(LCD_ChipSelect);
#endif
*/

    //init BANKS as output
    ttc_gpio_init_u8_shift(PARALLEL08_1_BANK,PARALLEL08_1_FIRSTPIN,tgm_output_push_pull,tgs_Max);
    ttc_gpio_init_u8_shift(PARALLEL08_2_BANK,PARALLEL08_2_FIRSTPIN,tgm_output_push_pull,tgs_Max);


    return temp;   //return of the Reg-data
}

void ili9320_write_command(t_u16 LCD_Dat) {

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    ttc_gpio_clr(TTC_GFX1_ILI9320_PIN_CHIPSELECT);                              // pull LCD-Chip-Select to zero -> chip-select is activated
    ttc_gpio_clr(TTC_GFX1_ILI9320_PIN_RESET);                                   // pull LCD_Register-Select to zero -> now the driver expect data which he will interprent as command
#else
    ttc_gpio_clr_variable(LCD_ChipSelect);                // pull LCD-Chip-Select to zero -> chip-select is activated
    ttc_gpio_clr_variable(LCD_Reset);                     // pull LCD_Register-Select to zero -> now the driver expect data which he will interprent as command
#endif


    //sending the Command-Data to the driver
    ttc_gpio_put_u8_shift(PARALLEL08_2_BANK, PARALLEL08_2_FIRSTPIN, (0x00ff & LCD_Dat));
    ttc_gpio_put_u8_shift(PARALLEL08_1_BANK, PARALLEL08_1_FIRSTPIN, (0xff00 & LCD_Dat)>>8);

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    ttc_gpio_clr(TTC_GFX1_ILI9320_PIN_WRITEMODE);                              // pull LCD-Chip-Select to zero -> chip-select is activated
    ttc_gpio_set(TTC_GFX1_ILI9320_PIN_WRITEMODE);                                  // pull LCD_Register-Select to zero -> now the driver expect data which he will interprent as command
    ttc_gpio_set(TTC_GFX1_ILI9320_PIN_CHIPSELECT);
#else
    ttc_gpio_clr_variable(LCD_WriteMode);                // pull LCD-Chip-Select to zero -> chip-select is activated
    ttc_gpio_set_variable(LCD_WriteMode);                     // pull LCD_Register-Select to zero -> now the driver expect data which he will interprent as command
    ttc_gpio_set_variable(LCD_ChipSelect);
#endif


}

void ili9320_write_data(t_u16 LCD_Dat) {

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    ttc_gpio_clr(TTC_GFX1_ILI9320_PIN_CHIPSELECT);                              // pull LCD-Chip-Select to zero -> chip-select is activated
    ttc_gpio_set(TTC_GFX1_ILI9320_PIN_RESET);                                   // pull LCD_Register-Select to zero -> now the driver expect data which he will interprent as command
#else
    ttc_gpio_clr_variable(LCD_ChipSelect);                // pull LCD-Chip-Select to zero -> chip-select is activated
    ttc_gpio_set_variable(LCD_Reset);                     // pull LCD_Register-Select to zero -> now the driver expect data which he will interprent as command
#endif


    //sending the Data to the driver
    ttc_gpio_put_u8_shift(PARALLEL08_2_BANK, PARALLEL08_2_FIRSTPIN, (0x00ff & LCD_Dat));
    ttc_gpio_put_u8_shift(PARALLEL08_1_BANK, PARALLEL08_1_FIRSTPIN, (0xff00 & LCD_Dat)>>8);

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    ttc_gpio_clr(TTC_GFX1_ILI9320_PIN_WRITEMODE);                              // pull LCD-Chip-Select to zero -> chip-select is activated
    ttc_gpio_set(TTC_GFX1_ILI9320_PIN_WRITEMODE);                                  // pull LCD_Register-Select to zero -> now the driver expect data which he will interprent as command
    ttc_gpio_set(TTC_GFX1_ILI9320_PIN_CHIPSELECT);
#else
    ttc_gpio_clr_variable(LCD_WriteMode);                // pull LCD-Chip-Select to zero -> chip-select is activated
    ttc_gpio_set_variable(LCD_WriteMode);                     // pull LCD_Register-Select to zero -> now the driver expect data which he will interprent as command
    ttc_gpio_set_variable(LCD_ChipSelect);
#endif


}

void ili9320_repeat_last_data()
{
//X #ifdef TARGET_ARCHITECTURE_STM32F1xx
    ttc_gpio_clr(TTC_GFX1_ILI9320_PIN_CHIPSELECT);  // pull LCD-Chip-Select to zero -> chip-select is activated
    ttc_gpio_set(TTC_GFX1_ILI9320_PIN_RESET);       // pull LCD_Register-Select to zero -> now the driver expect data which he will interprent as command
    ttc_gpio_clr(TTC_GFX1_ILI9320_PIN_WRITEMODE);   // pull LCD-Chip-Select to zero -> chip-select is activated
    ttc_gpio_set(TTC_GFX1_ILI9320_PIN_WRITEMODE);    // pull LCD_Register-Select to zero -> now the driver expect data which he will interprent as command
    ttc_gpio_set(TTC_GFX1_ILI9320_PIN_CHIPSELECT);
/** DEPRECATED
#else
    ttc_gpio_clr_variable(LCD_ChipSelect);                // pull LCD-Chip-Select to zero -> chip-select is activated
    ttc_gpio_set_variable(LCD_Reset);                     // pull LCD_Register-Select to zero -> now the driver expect data which he will interprent as command
    ttc_gpio_clr_variable(LCD_WriteMode);                // pull LCD-Chip-Select to zero -> chip-select is activated
    ttc_gpio_set_variable(LCD_WriteMode);                     // pull LCD_Register-Select to zero -> now the driver expect data which he will interprent as command
    ttc_gpio_set_variable(LCD_ChipSelect);
#endif
*/
}

void ili9320_init_repeat_mode()
{

}

void ili9320_deinit_repeat_mode(){
}


void ili9320_init() {
    GFX_STATIC t_u16 i9i2_DeviceCode=0x9325; //int for the Device Code of the TFT-drive

    ttc_gpio_init_u8_shift(PARALLEL08_1_BANK,PARALLEL08_1_FIRSTPIN,tgm_output_push_pull,tgs_Max); // ToDo: Do not use private function!
    ttc_gpio_init_u8_shift(PARALLEL08_2_BANK,PARALLEL08_2_FIRSTPIN,tgm_output_push_pull,tgs_Max); // ToDo: Do not use private function!

    ttc_gpio_init(TTC_GFX1_ILI9320_PIN_CHIPSELECT, tgm_output_push_pull, tgs_Max);
    ttc_gpio_init(TTC_GFX1_ILI9320_PIN_READMODE,   tgm_output_push_pull, tgs_Max);
    ttc_gpio_init(TTC_GFX1_ILI9320_PIN_WRITEMODE,  tgm_output_push_pull, tgs_Max);
    ttc_gpio_init(TTC_GFX1_ILI9320_PIN_RESET,      tgm_output_push_pull, tgs_Max);

/** DEPRECATED
    ttc_gpio_init_variable(&(ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_ChipSelect), TTC_GFX1_ILI9320_PIN_CHIPSELECT, tgm_output_push_pull, tgs_Max);
    ttc_gpio_init_variable(&(ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_ReadMode),   TTC_GFX1_ILI9320_PIN_READMODE, tgm_output_push_pull, tgs_Max);
    ttc_gpio_init_variable(&(ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_WriteMode),  TTC_GFX1_ILI9320_PIN_WRITEMODE, tgm_output_push_pull, tgs_Max);
    ttc_gpio_init_variable(&(ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_Reset),      TTC_GFX1_ILI9320_PIN_RESET, tgm_output_push_pull, tgs_Max);

    LCD_ChipSelect = &(ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_ChipSelect);
    LCD_ReadMode = &(ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_ReadMode);
    LCD_Reset = &(ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_Reset);
    LCD_WriteMode= &(ttc_gfx_CurrentDisplay->GFX_Arch.ILI9320.LCD_WriteMode);
#ifdef TARGET_ARCHITECTURE_STM32F1xx
    LCD_ChipSelect_BRR = LCD_ChipSelect->BRR;
    LCD_ReadMode_BRR = LCD_ReadMode->BRR;
    LCD_Reset_BRR = LCD_Reset->BRR;
    LCD_WriteMode_BRR = LCD_WriteMode->BRR;
    LCD_ChipSelect_BSRR = LCD_ChipSelect->BSRR;
    LCD_ReadMode_BSRR = LCD_ReadMode->BSRR;
    LCD_Reset_BSRR = LCD_Reset->BSRR;
    LCD_WriteMode_BSRR = LCD_WriteMode->BSRR;
#endif
*/

    ttc_gpio_init(TTC_GPIO_C8,  tgm_output_push_pull, tgs_Max); //? ToDo: Name this pin!
    ttc_gpio_init(TTC_GPIO_C9,  tgm_output_push_pull, tgs_Max); //? ToDo: Name this pin!
    ttc_gpio_init(TTC_GPIO_C10, tgm_output_push_pull, tgs_Max); //? ToDo: Name this pin!
    ttc_gpio_init(TTC_GPIO_C11, tgm_output_push_pull, tgs_Max); //? ToDo: Name this pin!


#ifdef PARALLEL08_1_BANK
    // initialize 8-bit wide output port
    ttc_gpio_init_u8_shift(PARALLEL08_1_BANK, PARALLEL08_1_FIRSTPIN,  tgm_output_push_pull, tgs_Max);

#define USE_BANK8_1
#endif

#ifdef PARALLEL08_2_BANK
    // initialize 8-bit wide output port
    ttc_gpio_init_u8_shift(PARALLEL08_2_BANK, PARALLEL08_2_FIRSTPIN,  tgm_output_push_pull, tgs_Max);

#define USE_BANK8_2
#endif

    ili9320_write_register(0x0000,0x0001); //command for reading the register
    //   ili9320_delay(5);                 // LCD_Delay 5 ms
    i9i2_DeviceCode = ili9320_read_register(0x0000); //save Readreg to var. i9i2_DeviceCode
    //   ili9320_delay(5);
    ili9320_write_register(0x0000,0x0001);
    i9i2_DeviceCode = ili9320_read_register(0x0000); //save Readreg to var. i9i2_DeviceCode
    //   ili9320_delay(5);

    //power configuration, not optimized, for more information ->manual
    if (i9i2_DeviceCode==0x9325 || i9i2_DeviceCode==0x9328)//ILI9325
    {

        ili9320_write_register(0x0001,0x0100);       //Driver Output Contral.
        ili9320_write_register(0x0002,0x0700);       //LCD Driver Waveform Contral.
        ili9320_write_register(0x0003,0x1030);       //Entry Mode Set.
        ili9320_write_register(0x0004,0x0000);       //Scalling Control.
        ili9320_write_register(0x0007,0x0133);       //Display Control.
        ili9320_write_register(0x0008,0x0202);       //Display Contral 2.(0x0207)
        ili9320_write_register(0x0009,0x0000);       //Display Contral 3.(0x0000)
        ili9320_write_register(0x000a,0x0000);       //Frame Cycle Control.(0x0000)
        ili9320_write_register(0x000c,0x0001);       //Extern Display Interface Control 1.(0x0000) = 16 bit
        ili9320_write_register(0x000d,0x0000);       //Frame Maker Position.
        ili9320_write_register(0x000f,0x0000);       //Extern Display Interface Control 2.
        ili9320_write_register(0x0010,0x1690);       //Power Control 1.(0x16b0)
        ili9320_write_register(0x0011,0x0227);       //Power Control 2.(0x0001)
        ili9320_write_register(0x0012,0x009d);       //Power Control 3.(0x0138)
        ili9320_write_register(0x0013,0x1900);       //Power Control 4.
        ili9320_write_register(0x0020,0x0000);
        ili9320_write_register(0x0021,0x0000);
        ili9320_write_register(0x0029,0x0025);
        ili9320_write_register(0x002b,0x000d);
        ili9320_write_register(0x0030,0x0007);
        ili9320_write_register(0x0031,0x0303);
        ili9320_write_register(0x0032,0x0003);
        ili9320_write_register(0x0035,0x0206);
        ili9320_write_register(0x0036,0x0008);
        ili9320_write_register(0x0037,0x0406);
        ili9320_write_register(0x0038,0x0304);
        ili9320_write_register(0x0039,0x0007);
        ili9320_write_register(0x003c,0x0602);
        ili9320_write_register(0x003d,0x0008);
        ili9320_write_register(0x0050,0x0000);       //Set X - Window Start
        ili9320_write_register(0x0051,0x00ef);       //Set X - Window END
        ili9320_write_register(0x0052,0x0000);       //Set Y - Window Start
        ili9320_write_register(0x0053,0x013f);       //Set Y - Window END
        ili9320_write_register(0x0060,0xa700);
        ili9320_write_register(0x0061,0x0001);
        ili9320_write_register(0x006a,0x0000);
        ili9320_write_register(0x0080,0x0000);
        ili9320_write_register(0x0081,0x0000);
        ili9320_write_register(0x0082,0x0000);
        ili9320_write_register(0x0083,0x0000);
        ili9320_write_register(0x0084,0x0000);
        ili9320_write_register(0x0085,0x0000);
        ili9320_write_register(0x0090,0x0010);
        ili9320_write_register(0x0092,0x0600);
        ili9320_write_register(0x00e5,0x78F0);       //LCD_WriteReg(0x0000,0x0001);

    }
    else if (i9i2_DeviceCode==0x9320 || i9i2_DeviceCode==0x9300|| i9i2_DeviceCode==0x9324||i9i2_DeviceCode==0x1320||i9i2_DeviceCode==0x9125)
    {
        ili9320_write_register(0x00,0x0000);
        ili9320_write_register(0x01,0x0100);	//Driver Output Contral.
        ili9320_write_register(0x02,0x0700);	//LCD Driver Waveform Contral.
        ili9320_write_register(0x03,0x1030);    //Entry Mode Set.
        ili9320_write_register(0x04,0x0000);	//Scalling Control.
        ili9320_write_register(0x08,0x0202);	//Display Contral 2.(0x0207)
        ili9320_write_register(0x09,0x0000);	//Display Contral 3.(0x0000)
        ili9320_write_register(0x0a,0x0000);	//Frame Cycle Contal.(0x0000)
        ili9320_write_register(0x0c,(1<<0));	//Extern Display Interface Control 1.(0x0000) = 16 bit
        ili9320_write_register(0x0d,0x0000);	//Frame Maker Position.
        ili9320_write_register(0x0f,0x0000);	//Extern Display Interface Control 2.
        //   ili9320_delay(10);
        ili9320_write_register(0x07,0x0101);	//Display Control.
        //   ili9320_delay(10);
        ili9320_write_register(0x10,(1<<12)|(0<<8)|(1<<7)|(1<<6)|(0<<4));                         	//Power Control 1.(0x16b0)
        ili9320_write_register(0x11,0x0007);								//Power Control 2.(0x0001)
        ili9320_write_register(0x12,(1<<8)|(1<<4)|(0<<0));                        			//Power Control 3.(0x0138)
        ili9320_write_register(0x13,0x0b00);								//Power Control 4.
        ili9320_write_register(0x29,0x0000);								//Power Control 7.
        ili9320_write_register(0x2b,(1<<14)|(1<<4));
        ili9320_write_register(0x50,0);         //Set X - Window Start
        ili9320_write_register(0x51,239);       //Set X - Windows End
        ili9320_write_register(0x52,0);   	    //Set Y - Windows Start
        ili9320_write_register(0x53,319);       //Set Y - Window End
        ili9320_write_register(0x60,0x2700);	//Driver Output Control.
        ili9320_write_register(0x61,0x0001);	//Driver Output Control.
        ili9320_write_register(0x6a,0x0000);	//Vertical Srcoll Control.
        ili9320_write_register(0x80,0x0000);	//Display Position? Partial Display 1.
        ili9320_write_register(0x81,0x0000);	//RAM Address Start? Partial Display 1.
        ili9320_write_register(0x82,0x0000);	//RAM Address End-Partial Display 1.
        ili9320_write_register(0x83,0x0000);	//Displsy Position? Partial Display 2.
        ili9320_write_register(0x84,0x0000);	//RAM Address Start? Partial Display 2.
        ili9320_write_register(0x85,0x0000);	//RAM Address End? Partial Display 2.
        ili9320_write_register(0x90,(0<<7)|(16<<0));	//Frame Cycle Contral.(0x0013)
        ili9320_write_register(0x92,0x0000);	//Panel Interface Contral 2.(0x0000)
        ili9320_write_register(0x93,0x0001);	//Panel Interface Contral 3.
        ili9320_write_register(0x95,0x0110);	//Frame Cycle Contral.(0x0110)
        ili9320_write_register(0x97,(0<<8));	//
        ili9320_write_register(0x98,0x0000);	//Frame Cycle Contral.
        ili9320_write_register(0x07,0x0173);	//(0x0173)
        //  ili9320_delay(10);
    }

    //first cleanup of the display
    ili9320_clear();
}

#endif

// experimental stuff
#ifdef ZERO
void LCD_EnterSleep(void)
{
    LCDBUS_WriteReg(0x00007, 0x00000); /* display OFF */
    //************* Power OFF sequence **************//
    LCDBUS_WriteReg(0x00010, 0x00000); /* SAP, BT[3:0], APE, AP, DSTB, SLP */
    LCDBUS_WriteReg(0x00011, 0x00000); /* DC1[2:0], DC0[2:0], VC[2:0] */
    LCDBUS_WriteReg(0x00012, 0x00000); /* VREG1OUT voltage */
    LCDBUS_WriteReg(0x00013, 0x00000); /* VDV[4:0] for VCOM amplitude */
    Delay(2000); /* Dis-charge capacitor power voltage */
    LCDBUS_WriteReg(0x00010, 0x00002); /* SAP, BT[3:0], APE, AP, DSTB, SLP */
}


void LCD_EnterDeepStandby(void)
{
    LCDBUS_WriteReg(0x00007, 0x00000); /* display OFF */
    //************* Power OFF sequence **************//
    LCDBUS_WriteReg(0x00010, 0x00000); /* SAP, BT[3:0], APE, AP, DSTB, SLP */
    LCDBUS_WriteReg(0x00011, 0x00000); /* DC1[2:0], DC0[2:0], VC[2:0] */
    LCDBUS_WriteReg(0x00012, 0x00000); /* VREG1OUT voltage */
    LCDBUS_WriteReg(0x00013, 0x00000); /* VDV[4:0] for VCOM amplitude */
    Delay(2000); /* Dis-charge capacitor power voltage */
    LCDBUS_WriteReg(0x00010, 0x00004); /* SAP, BT[3:0], APE, AP, DSTB, SLP */
}
#endif
