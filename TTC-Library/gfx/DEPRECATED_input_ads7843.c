#include "input_ads7843.h"


// different chip revisions deliver different coordinates for same point

// updated by _ttc_input_update_configuration()
extern ttc_input_generic_t* ttc_input_CurrentInput;


void input_ads7843_ADS_Write_Byte(u8_t num)
{
    u8_t count=0;                   //declaration of the count variable
    for(count=0;count<8;count++)    //beginning of the for loop, every bit of num will be checked and transmitted
    {
        if(num&0x80)ttc_gpio_set(TTC_INPUT_MOSI);     //the bit on the very left is checked first, in the following the bits will be shifted
        else ttc_gpio_clr(TTC_INPUT_MOSI);            //and also transmitted
        num<<=1;                //shifting
        ttc_gpio_clr(TTC_INPUT_SCLK);                 //after every transmission the clock (TCLK) must be reseted
        ttc_gpio_set(TTC_INPUT_SCLK);
    }
}
u16_t input_ads7843_ADS_Read_AD(u8_t CMD)
{
    u8_t i;                         //declaration of the count variable i
    u8_t count=0;                   //declaration of the count variable count
    u16_t Num=0;                    //declaration of the memory variable Num
    ttc_gpio_clr(TTC_INPUT_SCLK);                         //set clock to 0
    ttc_gpio_clr(TTC_INPUT_CS);                          //chip conversation mode "on"
    input_ads7843_ADS_Write_Byte(CMD);            //sending of the command byte
    for(i=100;i>0;i--);

    ttc_gpio_set(TTC_INPUT_SCLK);                         //reset of the clock
    ttc_gpio_clr(TTC_INPUT_SCLK);
    for(count=0;count<16;count++)
    {
        Num<<=1;                    //Bitshift
        ttc_gpio_clr(TTC_INPUT_SCLK);                     //Clock-Reset
        ttc_gpio_set(TTC_INPUT_SCLK);
        if(ttc_gpio_get(TTC_INPUT_MISO))Num++; 		    //if TOUT =1, the first bit of Num will be set
    }
    Num>>=4;                        //the touch-driver sends 16 bit, but the last 4 are always empty, so bitshifting of 4
    ttc_gpio_set(TTC_INPUT_CS);                          //chip conversation mode "off"
    return(Num);
}
u16_t input_ads7843_ADS_Read_XY(u8_t xy)
{
    u16_t i, j;                      //declaration of the count variable i
    u16_t buf[READ_TIMES];           //declaration of the BUFFER-Array buf
    u16_t sum=0;                     //declaration of the variable sum, which is responsible for calculate the average value
    u16_t temp;                      //declaration of the variable temp, which is responsible for calculate the average value

    for(i=0;i<READ_TIMES;i++)
    {
        buf[i]=input_ads7843_ADS_Read_AD(xy);	    //filling the buffer with the read data of the driver
    }

    for(i=0;i<READ_TIMES-1; i++)
    {
        for(j=i+1;j<READ_TIMES;j++)
        {
            if(buf[i]>buf[j])
            {
                temp=buf[i];
                buf[i]=buf[j];          //bubble-sort for sort values of the array, who are empty to the very right
                buf[j]=temp;
            }
        }
    }
    sum=0;
    for(i=LOST_VAL;i<READ_TIMES-LOST_VAL;i++)sum+=buf[i];    //akkumulation of the data of the buffer in the variable sum
    temp=sum/(READ_TIMES-2*LOST_VAL);                        //caluculating of the average value
    return temp;                                             //return of the calculated value
}
BOOL input_ads7843_read_position(volatile u16_t *x,volatile u16_t *y)
{
  //  Assert(ttc_input_CurrentInput, ec_NULL);

    u16_t xtemp,ytemp;                              //declaration of the two temp. variables

    if (ttc_input_CurrentInput->ChipVersion==1)
    {
        xtemp=input_ads7843_ADS_Read_XY(TTC_INPUT_CMD_RDX);            //reading of the x,y position
        ytemp=input_ads7843_ADS_Read_XY(TTC_INPUT_CMD_RDY);
        if(xtemp<100||ytemp<100)return 0;                    //check, value in range, if out of range return 0
        xtemp=(xtemp*ttc_input_CurrentInput->xfac+ttc_input_CurrentInput->xoff);       //save the values to given adresses
        ytemp=(ytemp*ttc_input_CurrentInput->yfac+ttc_input_CurrentInput->yoff);
    }
    else
    {
        xtemp=input_ads7843_ADS_Read_XY(TTC_INPUT_CMD_RDY);            //reading of the x,y position
        ytemp=input_ads7843_ADS_Read_XY(TTC_INPUT_CMD_RDX);
        if(xtemp<100||ytemp<100)return 0;                    //check, value in range, if out of range return 0
        xtemp=240-(xtemp*ttc_input_CurrentInput->xfac+ttc_input_CurrentInput->xoff);   //save the values to given adresses
        ytemp=320-(ytemp*ttc_input_CurrentInput->yfac+ttc_input_CurrentInput->yoff);
    }
    if( (0<xtemp) && (xtemp<240) && (0<ytemp) && (ytemp<340) ) { // touch data valid
        *x = xtemp;
        *y = ytemp;

        return TRUE;
    }
    else // no touch data abailable
    {
        return FALSE;
    }
}
void input_ads7843_configuration()
{





}
void input_ads7843_init() {
    Assert(ttc_input_CurrentInput, ec_NULL);

    //outputs
    ttc_gpio_init(TTC_INPUT_SCLK, tgm_output_push_pull);
    ttc_gpio_init(TTC_INPUT_MOSI, tgm_output_push_pull);
    ttc_gpio_init(TTC_INPUT_CS, tgm_output_push_pull);
    //inputs
    ttc_gpio_init(TTC_INPUT_MISO, tgm_input_floating);
    ttc_gpio_init(TTC_INPUT_INT,tgm_input_floating);

    input_ads7843_get_configuration(ttc_input_CurrentInput);


  /*  if (1) { // ToDo: replace by ttc_interrupt-call

    }
    */

    //Values for calibrating the touch screen, optional, function for autoadjust
    ttc_input_CurrentInput->xoff = -10;
    ttc_input_CurrentInput->yoff = -10;
    ttc_input_CurrentInput->xfac = 0.13;
    ttc_input_CurrentInput->yfac = 0.18;

    ttc_input_CurrentInput->ChipVersion = 1; // ToDo: correct?

    ttc_input_CurrentInput = ttc_input_CurrentInput;
}

void input_ads7843_get_configuration(ttc_input_generic_t* ttc_input_NewInput){

    ttc_input_CurrentInput = ttc_input_NewInput;

    if (! ttc_input_CurrentInput->Width)  ttc_input_CurrentInput->Width  = 320;
    if (! ttc_input_CurrentInput->Height) ttc_input_CurrentInput->Height = 240;
    if (! ttc_input_CurrentInput->Depth)  ttc_input_CurrentInput->Depth  = 24;

    ttc_input_CurrentInput->function_read_position =input_ads7843_read_position;
}
