/*{ lcd_k320qvb.c *******************************

  Low-level driver for LCD Controller K320QVB as used in ARMSTM32-LCD
  prototype board form Olimex Corp.

  written by Gregor Rebel 2012
  
  test run #1 (all generic drivers)
  gcc -O0
  Benchmark v1.0.50
  benchmark_Clear(5)       19760 ms _ttc_gfx_generic_clear()
  benchmark_Circle(5)      21371 ms _ttc_gfx_generic_circle()
  benchmark_Line_ortho(5)  21408 ms _ttc_gfx_generic_line_horizontal(), _ttc_gfx_generic_line_vertical()
  benchmark_Line(5)        22057 ms _ttc_gfx_generic_line()
  benchmark_Rect(5)        19714 ms _ttc_gfx_generic_rect(), _ttc_gfx_generic_rect_fill()
  benchmark_Text(5)         7035 ms _ttc_gfx_generic_put_char(), _ttc_gfx_generic_put_char_solid()
  benchmark_SetPixel(5)    21153 ms
  benchmark_RectFill(5)    19520 ms _ttc_gfx_generic_rect_fill()
  sum:                     165441 ms

  test run #2 ()
  gcc -O0
  - replaced function calls by macros like
    #define ttc_gfx_put_char(Char) ttc_gfx_CurrentDisplay->function_put_char(Char)
  Benchmark v1.0.50
  benchmark_Clear(5)       19801 ms _ttc_gfx_generic_clear()
  benchmark_Circle(5)      21384 ms _ttc_gfx_generic_circle()
  benchmark_Line_ortho(5)  21487 ms _ttc_gfx_generic_line_horizontal(), _ttc_gfx_generic_line_vertical()
  benchmark_Line(5)        22122 ms _ttc_gfx_generic_line()
  benchmark_Rect(5)        19760 ms _ttc_gfx_generic_rect(), _ttc_gfx_generic_rect_fill()
  benchmark_Text(5)         7035 ms k320qvb_put_char(), k320qvb_put_char_solid()
  benchmark_SetPixel(5)    21200 ms
  benchmark_RectFill(5)    19567 ms _ttc_gfx_generic_rect_fill()
  sum:                     152356 ms

  test run #3 ()
  gcc -O9
  - replaced function calls by macros like
    #define ttc_gfx_put_char(Char) ttc_gfx_CurrentDisplay->function_put_char(Char)
  Benchmark v1.0.50
  benchmark_Clear(5)       18737 ms _ttc_gfx_generic_clear()
  benchmark_Circle(5)      20139 ms _ttc_gfx_generic_circle()
  benchmark_Line_ortho(5)  18956 ms _ttc_gfx_generic_line_horizontal(), _ttc_gfx_generic_line_vertical()
  benchmark_Line(5)        19299 ms _ttc_gfx_generic_line()
  benchmark_Rect(5)        18698 ms _ttc_gfx_generic_rect(), _ttc_gfx_generic_rect_fill()
  benchmark_Text(5)         6095 ms k320qvb_put_char(), k320qvb_put_char_solid()
  benchmark_SetPixel(5)    19600 ms
  benchmark_RectFill(5)    18474 ms _ttc_gfx_generic_rect_fill()
  sum:                     139998 ms

  test run #4 ()
  gcc -O0
  - copied function_XXX() pointers  ttc_gfx_CurrentDisplay -> global variables
    (removes ttc_gfx_CurrentDisplay-> from every ttc_gfx_XXX() macro)
  Benchmark v1.0.50
  benchmark_Clear(5)       19761 ms _ttc_gfx_generic_clear()
  benchmark_Circle(5)      21371 ms _ttc_gfx_generic_circle()
  benchmark_Line_ortho(5)  21405 ms _ttc_gfx_generic_line_horizontal(), _ttc_gfx_generic_line_vertical()
  benchmark_Line(5)        22083 ms _ttc_gfx_generic_line()
  benchmark_Rect(5)        19722 ms _ttc_gfx_generic_rect(), _ttc_gfx_generic_rect_fill()
  benchmark_Text(5)         7030 ms k320qvb_put_char(), k320qvb_put_char_solid()
  benchmark_SetPixel(5)    21299 ms
  benchmark_RectFill(5)    19530 ms _ttc_gfx_generic_rect_fill()
  sum:                     152201 ms

  test run #4 ()
  gcc -O9 -> binary size = 128KB
  - copied function_XXX() pointers  ttc_gfx_CurrentDisplay -> global variables
    (removes ttc_gfx_CurrentDisplay-> from every ttc_gfx_XXX() macro)
  Benchmark v1.0.50
  benchmark_Clear(5)       18737 ms _ttc_gfx_generic_clear()
  benchmark_Circle(5)      20139 ms _ttc_gfx_generic_circle()
  benchmark_Line_ortho(5)  18956 ms _ttc_gfx_generic_line_horizontal(), _ttc_gfx_generic_line_vertical()
  benchmark_Line(5)        19306 ms _ttc_gfx_generic_line()
  benchmark_Rect(5)        18698 ms _ttc_gfx_generic_rect(), _ttc_gfx_generic_rect_fill()
  benchmark_Text(5)         6093 ms k320qvb_put_char(), k320qvb_put_char_solid()
  benchmark_SetPixel(5)    19600 ms
  benchmark_RectFill(5)    18474 ms _ttc_gfx_generic_rect_fill()
  sum:                     140003 ms

  test run #5 ()
  gcc -Os -> binary size = 89Kb
  - copied function_XXX() pointers  ttc_gfx_CurrentDisplay -> global variables
    (removes ttc_gfx_CurrentDisplay-> from every ttc_gfx_XXX() macro)
  Benchmark v1.0.50
  benchmark_Clear(5)       18737 ms _ttc_gfx_generic_clear()
  benchmark_Circle(5)      20148 ms _ttc_gfx_generic_circle()
  benchmark_Line_ortho(5)  19117 ms _ttc_gfx_generic_line_horizontal(), _ttc_gfx_generic_line_vertical()
  benchmark_Line(5)        19482 ms _ttc_gfx_generic_line()
  benchmark_Rect(5)        18699 ms _ttc_gfx_generic_rect(), _ttc_gfx_generic_rect_fill()
  benchmark_Text(5)         6195 ms k320qvb_put_char(), k320qvb_put_char_solid()
  benchmark_SetPixel(5)    19600 ms
  benchmark_RectFill(5)    18484 ms _ttc_gfx_generic_rect_fill()
  sum:                     140462 ms

  test run #6 ()
  gcc -O0 -> binary size = 150Kb
  - implemented + optimized low-level functions
  Benchmark v1.0.50
  benchmark_Clear(5)        3138 ms k320qvb_clear()
  benchmark_Circle(5)      20216 ms _ttc_gfx_generic_circle()
  benchmark_Line_ortho(5)   7990 ms k320qvb_line_horizontal(), k320qvb_line_vertical()
  benchmark_Line(5)        20309 ms _ttc_gfx_generic_line()
  benchmark_Rect(5)         9157 ms _ttc_gfx_generic_rect(), k320qvb_rect_fill()
  benchmark_Text(5)         6066 ms k320qvb_put_char(), k320qvb_put_char_solid()
  benchmark_SetPixel(5)    19919 ms k320qvb_set_pixel()
  benchmark_RectFill(5)     3738 ms k320qvb_rect_fill()
  sum:                     90533 ms

  test run #7 ()
  gcc -Os -> binary size = 90Kb
  - implemented + optimized low-level functions
  Benchmark v1.0.50
  benchmark_Clear(5)        3136 ms k320qvb_clear()
  benchmark_Circle(5)      20139 ms _ttc_gfx_generic_circle()
  benchmark_Line_ortho(5)   7897 ms k320qvb_line_horizontal(), k320qvb_line_vertical()
  benchmark_Line(5)        19321 ms _ttc_gfx_generic_line()
  benchmark_Rect(5)         9065 ms _ttc_gfx_generic_rect(), k320qvb_rect_fill()
  benchmark_Text(5)         5680 ms k320qvb_put_char(), k320qvb_put_char_solid()
  benchmark_SetPixel(5)    19600 ms k320qvb_set_pixel()
  benchmark_RectFill(5)     3621 ms k320qvb_rect_fill()
  sum:                     88459 ms

}*/

#include "lcd_k320qvb.h"
#include "ttc_gfx.h"
//{ Board specific drivers ************************************

#ifdef EXTENSION_board_olimex_lcd

static void k320qvb_init_stm32_lcd() {
    //{ init_LCD Start ToDo: replace by ttc function calls

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    GPIO_Init(GPIOE, &GPIO_InitStructure);
    GPIO_Init(GPIOF, &GPIO_InitStructure);
    GPIO_Init(GPIOG, &GPIO_InitStructure);

    /* Enable the FSMC Clock */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_FSMC, ENABLE);

    LCD_Init();
}

#endif

//}Board specific drivers
// functions provided to use macros from inside debugger
void m_k320qvb_set_cursor(t_u16 X, t_u16 Y)  { k320qvb_set_cursor(X, Y); }
void m_k320qvb_set_cursor_y(t_u16 Y) { k320qvb_set_cursor_y(Y); }
void m_k320qvb_set_cursor_x(t_u16 Y) { k320qvb_set_cursor_x(Y); }
void m_k320qvb_write_register(t_u16 Register, t_u16 Value) { k320qvb_write_register(Register, Value); }
void m_k320qvb_set_pixel(t_u16 Color) { k320qvb_set_pixel(Color); }

// minimal interface for ttc_gfx.c  (must be implemented)
void k320qvb_init() {
#ifdef EXTENSION_board_olimex_lcd
    k320qvb_init_stm32_lcd(ttc_gfx_CurrentDisplay);
#else
#warning missing implementation
#endif
}
void k320qvb_get_configuration() {
    if(0){
        if (! ttc_gfx_CurrentDisplay->Width)  ttc_gfx_CurrentDisplay->Width  = 320;
        if (! ttc_gfx_CurrentDisplay->Height) ttc_gfx_CurrentDisplay->Height = 240;
    }
    else
    {
        if (! ttc_gfx_CurrentDisplay->Width)  ttc_gfx_CurrentDisplay->Width  = 240;
        if (! ttc_gfx_CurrentDisplay->Height) ttc_gfx_CurrentDisplay->Height = 320;
    }
    if (! ttc_gfx_CurrentDisplay->Depth)  ttc_gfx_CurrentDisplay->Depth  = 24;

    // fill in mandatory function pointers
    ttc_gfx_CurrentDisplay->function_set_color_fg24 = k320qvb_set_color_fg24;
    ttc_gfx_CurrentDisplay->function_set_color_bg24 = k320qvb_set_color_bg24;

    // fill in optional function pointers
    ttc_gfx_CurrentDisplay->function_set_backlight  = k320qvb_set_backlight;
    ttc_gfx_CurrentDisplay->function_line_horizontal= k320qvb_line_horizontal;
    ttc_gfx_CurrentDisplay->function_line_vertical  = k320qvb_line_vertical;
    ttc_gfx_CurrentDisplay->function_clear          = k320qvb_clear;
    ttc_gfx_CurrentDisplay->function_rect_fill      = k320qvb_rect_fill;

#ifdef EXTENSION_ttc_font  // font attributes
    ttc_gfx_CurrentDisplay->function_put_char       = k320qvb_put_char;
    ttc_gfx_CurrentDisplay->function_put_char_solid = k320qvb_put_char_solid;
#endif
}
void k320qvb_set_color_fg24(t_u32 Color) {
    ttc_gfx_CurrentDisplay->ColorFg = ttc_gfx_CurrentColorFg = LCD_RGB_TO_LCDCOLOR( (Color & 0xff0000) >> 16, (Color & 0x00ff00) >> 8, (Color & 0x0000ff) );
}
void k320qvb_set_color_bg24(t_u32 Color) {
    ttc_gfx_CurrentDisplay->ColorBg = ttc_gfx_CurrentColorBg = LCD_RGB_TO_LCDCOLOR( (Color & 0xff0000) >> 16, (Color & 0x00ff00) >> 8, (Color & 0x0000ff) );
}

// extended interface for ttc_gfx.c (can be implemented)
void k320qvb_clear() {
    GFX_STATIC t_u32 Count;

    k320qvb_write_register(0x20, 0);
    k320qvb_write_register(0x21, 0);
    k320qvb_write_command(0x22);

    Count = ttc_gfx_CurrentDisplay->Height * ttc_gfx_CurrentDisplay->Width / 32;
    for (; Count > 0; --Count) {
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
    }
}
void k320qvb_rect_fill(t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height) {

    if (Width < 0) {
        X += Width;
        Width = - Width;
    }
    if (Height < 0) {
        Y += Height;
        Height = - Height;
    }
    if (X >= ttc_gfx_CurrentDisplay->Width)
        X = ttc_gfx_CurrentDisplay->Width - 1;
    if (Y >= ttc_gfx_CurrentDisplay->Height)
        Y = ttc_gfx_CurrentDisplay->Height - 1;
    if (X + Width >= ttc_gfx_CurrentDisplay->Width)
        Width = ttc_gfx_CurrentDisplay->Width - X - 1;
    if (X + Height >= ttc_gfx_CurrentDisplay->Height)
        Height = ttc_gfx_CurrentDisplay->Height - Y - 1;

    while (Height--) {
        k320qvb_line_horizontal(X, Y++, Width);
    }

}
void k320qvb_line_horizontal(t_u16 X, t_u16 Y, t_s16 Length) {

    if (Length < 0) {
        X += Length;
        Length = - Length;
    }

    k320qvb_set_cursor(X,Y);
    while (Length & 0xf) { // draw pixels until length is dividable by 16
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        Length--;
    }

    while (Length > 0) {
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        Length -= 16;
    }
}
void k320qvb_line_vertical(t_u16 X, t_u16 Y, t_s16 Length) {

    if (Length < 0) {
        Y += Length;
        Length = - Length;
    }

    k320qvb_write_register(0x03, 0x1038);

    k320qvb_set_cursor(X,Y);

    while (Length & 0xf) { // draw pixels until length is dividable by 16
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        Length--;
    }

    while (Length > 0) {
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
        Length -= 16;
    }

    k320qvb_write_register(0x03, 0x1030);
}

void k320qvb_set_backlight(t_u8 Level) {
    LCD_SetBacklight( (int) Level); // ToDo: Direct implementation
}

#ifdef EXTENSION_ttc_font  // font attributes

void k320qvb_put_char(t_u8 Character) {
    GFX_STATIC t_u16 BitLine;
    GFX_STATIC  t_u8 LineNo;
    GFX_STATIC t_u16 X;
    GFX_STATIC t_u16 Y;              Y = ttc_gfx_CurrentDisplay->CursorY;
    GFX_STATIC const t_u16* Bitmap; Bitmap = ttc_gfx_CurrentDisplay->Font->Characters + (Character - 32) * 24;

    for (LineNo = 0; LineNo < 24; LineNo++) {
        BitLine = *Bitmap++;
        k320qvb_set_cursor(X,Y);

        if (BitLine) {
            X = ttc_gfx_CurrentDisplay->CursorX;
            do {
                if (BitLine & 1) {
                    k320qvb_set_cursor_x(X);
                    k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
                }
                X++;
                BitLine /= 2;
            } while (BitLine);
        }
        Y++;
    }
}
void k320qvb_put_char_solid(t_u8 Character) {
    GFX_STATIC t_u16 BitLine;
    GFX_STATIC  t_u8 LineNo;
    GFX_STATIC  t_u8 BitCount;
    GFX_STATIC t_u16 X;
    GFX_STATIC t_u16 Y;              Y = ttc_gfx_CurrentDisplay->CursorY;
    GFX_STATIC const t_u16* Bitmap; Bitmap = ttc_gfx_CurrentDisplay->Font->Characters + (Character - 32) * 24;

    for (LineNo = 0; LineNo < 24; LineNo++) {
        BitLine = *Bitmap++;
        X = ttc_gfx_CurrentDisplay->CursorX;

        k320qvb_set_cursor(X, Y);
        for (BitCount = 16; BitCount > 0; BitCount--) {
            if (BitLine & 1) {
                k320qvb_set_pixel(ttc_gfx_CurrentColorFg);
            }
            else {
                k320qvb_set_pixel(ttc_gfx_CurrentColorBg);
            }
            X++;
            BitLine /= 2;
        }
        Y++;
    }
}

#endif

// private support  functions
t_u16 k320qvb_read_register(t_u16 Register) {

    *LCDBUS_RSLOW_ADDR = Register;

    return *LCDBUS_RSHIGH_ADDR;
}
