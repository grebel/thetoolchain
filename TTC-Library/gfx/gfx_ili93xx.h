#ifndef GFX_ILI93xx_H
#define GFX_ILI93xx_H

/** { gfx_ili93xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for gfx devices on ili93xx architectures.
 *  Structures, Enums and Defines being required by high-level gfx and application.
 *
 *  Created from template device_architecture.h revision 22 at 20140313 10:58:25 UTC
 *
 *  Note: See ttc_gfx.h for description of ili93xx independent GFX implementation.
 *
 *  In order to access the ili93xx grafic chip, some macros must be defined before including this header.
 *  Find a list of required board drivers below.
 *
 *  Authors: Patrick von Poblotzki, Gregor Rebel
 *
}*/
/** example_gfx_benchmark results (5 iterations each)
 *
 * Note: Deactivate ASSERT_GFX_* in ttc_gfx_types.h for maximum speed!
 * Tested with: arm-none-eabi-gcc (GNU Tools for ARM Embedded Processors) 5.4.1 20160919
 * Tested on:   board_olimex_stm32_lcd
 *
 * Additional compile extensions:
 * 060_basic_extensions_optimize_3_remove_unused
 *
 * gcc:   (-O0)      (-Os)     (-O9)
 * Size:  50888 b    33516 b   45580 b
 * Run0:   1845 ms     585 ms    571 ms
 * Run1:  12826 ms   12758 ms  12724 ms
 * Run2:  12642 ms    7418 ms   6639 ms
 * Run3:   3740 ms    1830 ms   1685 ms
 * Run4:  16139 ms   16070 ms  16070 ms
 * Run5:   5097 ms    2734 ms   2505 ms
 * Run6:  11672 ms   11013 ms  10943 ms
 * Run7:  19581 ms   19580 ms  19580 ms
 * Run8:   3341 ms    1557 ms   1463 ms
 * sum:   86703 ms   73545 ms  72180 ms
 *
 **/
//{ Defines/ TypeDefs ****************************************************

#include "../ttc_task.h"
/**     --------------------------------------------------------------------
    Registers of ILI93xx
    ------------------------------------------------------------------*/


//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_GFX_ILI93xx
//
// Implementation status of low-level driver support for gfx devices on ili93xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_GFX_ILI93xx '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_GFX_ILI93xx == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_GFX_ILI93xx to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_GFX_ILI93xx

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
#include "../ttc_basic.h"
#include "../ttc_gpio.h"
#include "../ttc_task.h"
//? #include "../ttc_interrupt.h"
#include "../ttc_memory.h"
#include "gfx_ili93xx_types.h"
#include "../ttc_gfx_types.h"
#include "compile_options.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

/** Check if required low-level board macros are defined
 *
 * These macros must be provided by current ttc-lib/board/board_* low-level driver.
 *
 * See existing ttc-lib/board/board_* files example implementations:
 * + ttc-lib/board/board_olimex_stm32_lcd.*
 *   16 bit parallel bus using flexible static memory controller (FSMC) in stm32f103.
 *
 * + ttc-lib/board/board_dso_0138*
 *   8 bit parallel bus using software control.
 *
 *
 * Mandatory Macros
 * + void gfx_ili93xx_board_write_command(t_u8 Command)
 *   Sends given command value to ili93xx.
 *
 * + void gfx_ili93xx_board_write_data16(t_u16 Value)
 *   Sends given 16 bit value to ili93xx.
 *
 * + void gfx_ili93xx_board_write_register16(e_gfx_ili93xx_register Register, t_u16 Value)
 *   Write given value into given register number in ili93xx.
 *
 * + t_u16 gfx_ili93xx_board_read_register16(e_gfx_ili93xx_register Register)
 *   Read 2 byte value from given register number in ili93xx.
 *
 * + t_u16 gfx_ili93xx_board_read_register32(e_gfx_ili93xx_register Register)
 *   Read 4 byte value from given register number in ili93xx.
 *
 *
 * Optional Macros (may be defined as empty)
 * + void gfx_ili93xx_board_select(t_ttc_gfx_config* Config)
 *   Selects LCD display to listen to commands on shared bus.
 *   This shall control the select input line, if it is connected to a gpio.
 *   Note: The low-level driver must not call this function except during board_*_prepare() or board_*_reset().
 *
 * + void gfx_ili93xx_board_deselect(t_ttc_gfx_config* Config)
 *   Deselects LCD display to stop listening to shared bus.
 *   This shall control the select input line, if it is connected to a gpio.
 *   Note: The low-level driver must not call this function except during board_*_prepare() or board_*_reset().
 *
 * + void gfx_ili93xx_board_write_register08(e_gfx_ili93xx_register Register, t_u8 Value)
 *   Write given value into given register number in ili93xx.
 *
 * + t_u8 gfx_ili93xx_board_read_register08(e_gfx_ili93xx_register Register)
 *   Read 16 bit value from given register number in ili93xx.
 *

 * Example definitions (Olimex STM32 LCD using FSMC):
   #define gfx_ili93xx_board_write_command(Command)             *LCDBUS_RSLOW_ADDR  = (t_u16) Command
   #define gfx_ili93xx_board_write_data16(Data)                 *LCDBUS_RSHIGH_ADDR = (t_u16) Data;
   #define gfx_ili93xx_board_write_register16(Register, Value)  gfx_ili93xx_board_write_command(Register); gfx_ili93xx_board_write_data16(Value)
   #define gfx_ili93xx_board_read_register16(Register)          board_olimex_stm32_lcd_ili93xx_read_register16(Register)
   #define gfx_ili93xx_board_read_register32(Register)          board_olimex_stm32_lcd_ili93xx_read_register32(Register)
   #define gfx_ili93xx_board_select(Config)
   #define gfx_ili93xx_board_deselect(Config)
   #define gfx_ili93xx_board_write_register08(Register, Value)
   #define gfx_ili93xx_board_read_register08(Register)

 * Optional constant definitions for board makefiles:
   COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_LCDLED=E_ttc_gpio_pin_?  # gpio pin that switches on LCD backlight

 */

#include "../ttc_board.h"

#ifndef gfx_ili93xx_board_write_register16
    #error Missing definition for gfx_ili93xx_board_write_register16(t_u32 Register, t_u16 Value). Add a macro definition to write a value into certain register in ili93xx on current board!
#endif
#ifndef gfx_ili93xx_board_read_register16
    #error Missing definition for t_u16 gfx_ili93xx_board_read_register16(t_u32 Register). Add a macro definition to read a data word from an ili93xx register on current board!
#endif
#ifndef gfx_ili93xx_board_read_register32
    #error Missing definition for t_u32 gfx_ili93xx_board_read_register32(t_u32 Register). Add a macro definition to read a data word from an ili93xx register on current board!
#endif
#ifndef gfx_ili93xx_board_write_command
    #error Missing definition for gfx_ili93xx_board_write_command(t_u8 Command). Add a macro definition to send a command word to ili93xx on current board!
#endif
#ifndef gfx_ili93xx_board_write_data16
    #error Missing definition for gfx_ili93xx_board_write_data16(t_u16 Data). Add a macro definition to send a data word to ili93xx on current board!
#endif
#ifndef gfx_ili93xx_board_select
    #error Missing definition for void gfx_ili93xx_board_select( t_ttc_gfx_config* Config ). Add a macro definition to send a data word to ili93xx on current board (or define it as empty)!
#endif
#ifndef gfx_ili93xx_board_deselect
    #error Missing definition for void gfx_ili93xx_board_deselect( t_ttc_gfx_config* Config ). Add a macro definition to send a data word to ili93xx on current board (or define it as empty)!
#endif

// define all supported low-level functions for ttc_gfx_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_gfx_foo
//
#define gfx_ili93xx_set_x(X)      gfx_ili93xx_board_write_command(E_gfx_ili93xx_register_GRAMHorizontalAddressSet); gfx_ili93xx_board_write_data16(X)
#define gfx_ili93xx_set_y(Y)      gfx_ili93xx_board_write_command(0x21); gfx_ili93xx_board_write_data16(Y)

#define ttc_driver_gfx_deinit(Config)                             gfx_ili93xx_deinit(Config)
#define ttc_driver_gfx_get_features(Config)                       gfx_ili93xx_get_features(Config)
#define ttc_driver_gfx_init(Config)                               gfx_ili93xx_init(Config)
#define ttc_driver_gfx_load_defaults(Config)                      gfx_ili93xx_load_defaults(Config)
#define ttc_driver_gfx_prepare()                                  gfx_ili93xx_prepare()
#define ttc_driver_gfx_reset(Config)                              gfx_ili93xx_reset(Config)
#define ttc_driver_gfx_backlight(Level)                           gfx_ili93xx_backlight(Level)
#define ttc_driver_gfx_color_bg16(Color)                          gfx_ili93xx_color_bg16(Color)
#define ttc_driver_gfx_color_bg24(Color)                          gfx_ili93xx_color_bg24(Color)
#define ttc_driver_gfx_color_bg_palette(PaletteIndex)             gfx_ili93xx_color_bg_palette(PaletteIndex)
#define ttc_driver_gfx_color_fg16(Color)                          gfx_ili93xx_color_fg16(Color)
#define ttc_driver_gfx_color_fg24(Color)                          gfx_ili93xx_color_fg24(Color)
#define ttc_driver_gfx_color_fg_palette(PaletteIndex)             gfx_ili93xx_color_fg_palette(PaletteIndex)
#define ttc_driver_gfx_cursor_set(X, Y)                           gfx_ili93xx_cursor_set(X, Y)
#define ttc_driver_gfx_palette_set(Palette, Size)                 gfx_ili93xx_palette_set(Palette, Size)
#define ttc_driver_gfx_clear()                                    gfx_ili93xx_clear()
#define ttc_driver_gfx_line(X1, Y1, X2, Y2)                       gfx_ili93xx_line(X1, Y1, X2, Y2)
#define ttc_driver_gfx_circle(CenterX, CenterY, Radius)           gfx_ili93xx_circle(CenterX, CenterY, Radius)
#define ttc_driver_gfx_line_horizontal(X, Y, Length)              gfx_ili93xx_line_horizontal(X, Y, Length)
#define ttc_driver_gfx_line_vertical(X, Y, Length)                gfx_ili93xx_line_vertical(X, Y, Length)
#define ttc_driver_gfx_rect_fill(X, Y, Width, Height)             gfx_ili93xx_rect_fill(X, Y, Width, Height)
#define ttc_driver_gfx_print(String, MaxSize)                     gfx_ili93xx_print(String, MaxSize)
#define ttc_driver_gfx_printSolid(string)                         gfx_ili93xx_printSolid(string)
#define ttc_driver_gfx_print_at(X, Y, String, MaxSize)            gfx_ili93xx_print_at(X, Y, String, MaxSize)
#define ttc_driver_gfx_print_block(Block)                         gfx_ili93xx_print_block(Block)
#define ttc_driver_gfx_print_block_solid(Block)                   gfx_ili93xx_print_block_solid(Block)
#define ttc_driver_gfx_print_boxed(String, MaxSize, InnerSpacing) gfx_ili93xx_print_boxed(String, MaxSize, InnerSpacing)
#define ttc_driver_gfx_print_solid(String, MaxSize)               gfx_ili93xx_print_solid(String, MaxSize)
#define ttc_driver_gfx_print_solid_at(X, Y, String, MaxSize)      gfx_ili93xx_print_solid_at(X, Y, String, MaxSize)
#define ttc_driver_gfx_rect(X, Y, Width, Height)                  gfx_ili93xx_rect(X,Y,Width,Height)
#define ttc_driver_gfx_set_font(Font)                             gfx_ili93xx_set_font(Font)
#define ttc_driver_gfx_stdout_print_block(Block)                  gfx_ili93xx_stdout_print_block(Block)
#define ttc_driver_gfx_stdout_print_string(String, MaxSize)       gfx_ili93xx_stdout_print_string(String, MaxSize)
#define ttc_driver_gfx_stdout_set(DisplayIndex)                   gfx_ili93xx_stdout_set(DisplayIndex)
#define ttc_driver_gfx_text_cursor_down()                         gfx_ili93xx_text_cursor_down()
#define ttc_driver_gfx_text_cursor_left()                         gfx_ili93xx_text_cursor_left()
#define ttc_driver_gfx_text_cursor_right()                        gfx_ili93xx_text_cursor_right()
#define ttc_driver_gfx_text_cursor_set(TextX, TextY)              gfx_ili93xx_text_cursor_set(TextX, TextY)
#define ttc_driver_gfx_text_cursor_up()                           gfx_ili93xx_text_cursor_up()
#define ttc_driver_gfx_circle_fill(CenterX, CenterY, Radius)      gfx_ili93xx_circle_fill(CenterX, CenterY, Radius)
#define ttc_driver_gfx_pixel_clr()                                gfx_ili93xx_pixel_clr()
#define ttc_driver_gfx_pixel_clr_at(X, Y)                         gfx_ili93xx_pixel_clr_at(X, Y)
#define ttc_driver_gfx_pixel_set()                                gfx_ili93xx_pixel_set()
#define ttc_driver_gfx_pixel_set_at(X, Y)                         gfx_ili93xx_pixel_set_at(X, Y)
#define ttc_driver_gfx_char(Char)                                 gfx_ili93xx_char(Char)
#define ttc_driver_gfx_char_180(Char)                             gfx_ili93xx_char_180(Char)
#define ttc_driver_gfx_char_090(Char)                             gfx_ili93xx_char_090(Char)
#define ttc_driver_gfx_char_270(Char)                             gfx_ili93xx_char_270(Char)
#define ttc_driver_gfx_char_solid(Char)                           gfx_ili93xx_char_solid(Char)
#define ttc_driver_gfx_char_solid_090(Char)                       gfx_ili93xx_char_solid_090(Char)
#define ttc_driver_gfx_char_solid_180(Char)                       gfx_ili93xx_char_solid_180(Char)
#define ttc_driver_gfx_char_solid_270(Char)                       gfx_ili93xx_char_solid_270(Char)
#define ttc_driver_gfx_rect_090(X, Y, Width, Height)              gfx_ili93xx_rect_090(X, Y, Width, Height)
#define ttc_driver_gfx_rect_180(X, Y, Width, Height)              gfx_ili93xx_rect_180(X, Y, Width, Height)
#define ttc_driver_gfx_rect_270(X, Y, Width, Height)              gfx_ili93xx_rect_270(X, Y, Width, Height)
#define ttc_driver_gfx_line_horizontal_090(X, Y, Length)          gfx_ili93xx_line_horizontal_090(X, Y, Length)
#define ttc_driver_gfx_line_vertical_090(X, Y, Length)            gfx_ili93xx_line_vertical_090(X, Y, Length)
#define ttc_driver_gfx_select(Config)                             gfx_ili93xx_select(Config)
#define ttc_driver_gfx_deselect(Config)                           gfx_ili93xx_deselect(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

// not implemented driver macros must be undefined (interface will provide implementation)!
#undef ttc_driver_gfx_arrow
#undef ttc_driver_gfx_circle_segment
#undef ttc_driver_gfx_circle_segment_fill
#undef ttc_driver_gfx_char_solid
#undef ttc_driver_gfx_char_solid_090
#undef ttc_driver_gfx_char_solid_180
#undef ttc_driver_gfx_char_solid_270
#undef ttc_driver_gfx_char
#undef ttc_driver_gfx_char_090
#undef ttc_driver_gfx_char_180
#undef ttc_driver_gfx_char_270
#undef ttc_driver_arrow

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_gfx.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_gfx.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** shutdown single GFX unit device
 * @param Config        pointer to struct t_ttc_gfx_config
 * @return              == 0: GFX has been shutdown successfully; != 0: error-code
 */
e_ttc_gfx_errorcode gfx_ili93xx_deinit( t_ttc_gfx_config* Config );

/** fills out given Config with maximum valid values for indexed GFX
 * @param Config        = pointer to struct t_ttc_gfx_config
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_gfx_errorcode gfx_ili93xx_get_features( t_ttc_gfx_config* Config );

/** initializes single GFX unit for operation
 * @param Config        pointer to struct t_ttc_gfx_config
 * @return              == 0: GFX has been initialized successfully; != 0: error-code
 */
e_ttc_gfx_errorcode gfx_ili93xx_init( t_ttc_gfx_config* Config );

/** loads configuration of indexed GFX unit with default values
 * @param Config        pointer to struct t_ttc_gfx_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_gfx_errorcode gfx_ili93xx_load_defaults( t_ttc_gfx_config* Config );

/** Prepares gfx Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void gfx_ili93xx_prepare();

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_gfx_config
 * @return              ==0: reset successfull; !=0: error value
 */
e_ttc_gfx_errorcode gfx_ili93xx_reset( t_ttc_gfx_config* Config );

/** set background color to given 16-bit value
  *
  * @param Color  16-bit values are display specific use ttc_gfx_color_fg24() for conversion
 * @param Level   =
 * @return        =
 */
void gfx_ili93xx_backlight( t_u8 Level );

/** set background color to given 16-bit value
  *
  * @param Color  16-bit values are display specific use ttc_gfx_color_fg24() for conversion
 * @return   =
 */
void gfx_ili93xx_color_bg16( t_u16 Color );

/** Moves pixel cursor to a new location.
  *
  * Many drawing operations start at the current cursor position.
  * Some drawing operations automatically move the cursor.
  *
  * @param X   x-coordinate of new drawing position
  * @param Y   x-coordinate of new drawing position
 * @param Color   =
 * @return        =
 */
void gfx_ili93xx_color_bg24( t_u32 Color );

/** Moves pixel cursor to a new location.
  *
  * Many drawing operations start at the current cursor position.
  * Some drawing operations automatically move the cursor.
  *
  * @param X   x-coordinate of new drawing position
  * @param Y   x-coordinate of new drawing position
 * @param PaletteIndex   =
 * @return               =
 */
void gfx_ili93xx_color_bg_palette( t_u8 PaletteIndex );

/** set foreground color to given 16-bit value
  *
  * @param Color  16-bit values are display specific use ttc_gfx_color_fg24() for conversion
 * @return   =
 */
void gfx_ili93xx_color_fg16( t_u16 Color );

/** Moves pixel cursor to a new location.
  *
  * Many drawing operations start at the current cursor position.
  * Some drawing operations automatically move the cursor.
  *
  * @param X   x-coordinate of new drawing position
  * @param Y   x-coordinate of new drawing position
 * @param Color   =
 * @return        =
 */
void gfx_ili93xx_color_fg24( t_u32 Color );

/** Moves pixel cursor to a new location.
  *
  * Many drawing operations start at the current cursor position.
  * Some drawing operations automatically move the cursor.
  *
  * @param X   x-coordinate of new drawing position
  * @param Y   x-coordinate of new drawing position
 * @param PaletteIndex   =
 * @return               =
 */
void gfx_ili93xx_color_fg_palette( t_u8 PaletteIndex );

/** Moves pixel cursor to a new location.
  *
  * Many drawing operations start at the current cursor position.
  * Some drawing operations automatically move the cursor.
  *
  * @param X   x-coordinate of new drawing position
  * @param Y   x-coordinate of new drawing position
 * @return   =
 */
void gfx_ili93xx_cursor_set( t_u16 X, t_u16 Y );

/** Moves pixel cursor to a new location.
  *
  * Many drawing operations start at the current cursor position.
  * Some drawing operations automatically move the cursor.
  *
  * @param X   x-coordinate of new drawing position
  * @param Y   x-coordinate of new drawing position
 * @param Palette   =
 * @param Size      =
 * @return          =
 */
void gfx_ili93xx_palette_set( t_u16* Palette, t_u8 Size );

void  gfx_ili93xx_clear();

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_gfx_config
 * @param X1   =
 * @param Y1   =
 * @param X2   =
 * @param Y2   =
 * @return     =
 */
void gfx_ili93xx_line( t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2 );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_gfx_config
 * @param CenterX   =
 * @param CenterY   =
 * @param Radius    =
 * @return          =
 */
void gfx_ili93xx_circle( t_u16 CenterX, t_u16 CenterY, t_u16 Radius );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_gfx_config
 * @param X        =
 * @param Y        =
 * @param Length   =
 * @return         =
 */
void gfx_ili93xx_line_horizontal( t_u16 X, t_u16 Y, t_s16 Length );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_gfx_config
 * @param X        =
 * @param Y        =
 * @param Length   =
 * @return         =
 */
void gfx_ili93xx_line_vertical( t_u16 X, t_u16 Y, t_s16 Length );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_gfx_config
 * @param X        =
 * @param Y        =
 * @param Width    =
 * @param Height   =
 * @return         =
 */
void gfx_ili93xx_rect_fill( t_u16 X, t_u16 Y, t_s16 PhysicalWidth, t_s16 PhysicalHeight );

void gfx_ili93xx_set_font( const t_ttc_font_data* Font );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_gfx_config
 * @param CenterX   =
 * @param CenterY   =
 * @param Radius    =
 * @return          =
 */
void gfx_ili93xx_circle_fill( t_u16 CenterX, t_u16 CenterY, t_u16 Radius );

/** reset the actual window for ili93xx to maximum size
 *
 */
void gfx_ili93xx_window_reset( );

/** set the actual window for ili93xx
 *
 * @param Left   = x-coordinate of left border
 * @param Right  = x-coordinate of right border
 * @param Top    = y-coordinate of upper border
 * @param Bottom = y-coordinate of lower border
 */
void gfx_ili93xx_window_set( t_u16 Left, t_u16 Right, t_u16 Top, t_u16 Bottom );

/** set for portrait ram addresses
 *
 * @param Config        pointer to struct t_ttc_gfx_config
 * @param CenterX   =
 * @param CenterY   =
 * @param Radius    =
 * @return          =
 */
void gfx_ili93xx_setPortrait();

/** set for landscape ram addresses
 *
 * @return          =
 */
void gfx_ili93xx_setLandscape();
/** Enter sleep of ili93xx display
 *
 * @return          =
 */
void gfx_ili93xx_sleep_enter();

/** Exit sleep of ili93xx display
 *
 */
void gfx_ili93xx_sleep_exit();

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_gfx_config
 */
void gfx_ili93xx_pixel_clr();

/** Set single pixel using current background color at given coordinate
 *
 * @param X  x-coordinate (0 = left column of display)
 * @param Y  y-coordinate (0 = top row of display)
 */
void gfx_ili93xx_pixel_clr_at( t_u16 X, t_u16 Y );

/** Put char_solid for 180° turned portrait display
   * @param Char
   */
void gfx_ili93xx_char( t_u8 Char );

/** Put char for Displaymode 180 degrees mirrored
   * @param Char         character to print
   */
void gfx_ili93xx_char_180( t_u8 Char );

/** Put char for Displaymode clockwise turned
   * @param Char         character to print
   */
void gfx_ili93xx_char_090( t_u8 Char );

/** Put char for Displaymode counter clockwise turned
   * @param Char         character to print
   */
void gfx_ili93xx_char_270( t_u8 Char );

/** Put char_solid for 180° turned portrait display
   * @param Char         character to print
   */
void gfx_ili93xx_char_solid( t_u8 Char );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_gfx_config
 */
void gfx_ili93xx_pixel_set();

/** Set single pixel using current foreground color at given coordinate
 *
 * @param X  x-coordinate (0 = left column of display)
 * @param Y  y-coordinate (0 = top row of display)
 */
void gfx_ili93xx_pixel_set_at( t_u16 X, t_u16 Y );

/** draws line in current ColorFg from (X, Y) to (Left+Length-1, Y) with display landscape, 90° clockwise
 *
 * Note: Before calling this function, make sure that ttc_gfx_CurrentDisplay has correct value!
 *
 * @param Left            x-coordinate of starting point
 * @param Y               y-coordinate of starting point
 * @param Length          distance to end point of line
 * @param Length   =
 */
void gfx_ili93xx_line_horizontal_090( t_u16 X, t_u16 Y, t_s16 Length );

/** draws line in current ColorFg from (X, Y) to (Left+Length-1, Y) with display landscape, 90° clockwise
 *
 * Note: Before calling this function, make sure that ttc_gfx_CurrentDisplay has correct value!
 *
 * @param Left            x-coordinate of starting point
 * @param Y               y-coordinate of starting point
 * @param Length          distance to end point of line
 * @param Length   =
 */
void gfx_ili93xx_line_vertical_090( t_u16 X, t_u16 Y, t_s16 Length );

/** draws circle segment in current ColorFg around (CenterX, CenterY) of radius Radius
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param CenterX         x-coordinate of starting point
 * @param CenterY         y-coordinate of starting point
 * @param StartAngle      0..360 - circle segment is drawn counter clockwise
 * @param EndAngle        0..360 - circle segment is drawn counter clockwise
 * @param Radius          distance to corner opposing starting point in x-direction
 */
void gfx_ili93xx_circle_segment( t_u16 CenterX, t_u16 CenterY, t_u16 Radius, t_u16 StartAngle, t_u16 EndAngle );

/** draw graphical element of a single- or double-ended arrow
 *
 * Examples:
 *   _etsc2_gfx_arrow(1,1, 100,100, 0,0);    // draw arrow (1,1) --- (100,100) (no arrow but just simple line)
 *   _etsc2_gfx_arrow(1,1, 100,100, 0,4);    // draw arrow (1,1) --> (100,100) (anchor of size 4 pointing at (100,100)
 *   _etsc2_gfx_arrow(1,1, 100,100,10,0);    // draw arrow (1,1) <-- (100,100) (bigger anchor pointing at (1,1)
 *   _etsc2_gfx_arrow(1,1, 100,100, 6,6);    // draw arrow (1,1) <-> (100,100) (equal sized anchor pointing between (1,1) and (100,100)
 *
 * @param X1       x-coordinate of starting point
 * @param Y1       y-coordinate of starting point
 * @param X2       x-coordinate of end point
 * @param Y2       y-coordinate of end point
 * @param Length1  length of arrow anchor lines at (X1,Y1)
 * @param Length2  length of arrow anchor lines at (X2,Y2)
 */
void gfx_ili93xx_arrow( t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2, t_u8 Length1, t_u8 Length2 );

/** draws filled circle segment in current ColorFg around (CenterX, CenterY) of radius Radius
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param CenterX         x-coordinate of center point
 * @param CenterY         y-coordinate of center point
 * @param StartAngle      0..360 - circle segment is drawn counter clockwise
 * @param EndAngle        0..360 - circle segment is drawn counter clockwise
 * @param Radius          distance from center point to all points on boundary line
 */
void gfx_ili93xx_circle_segment_fill( t_u16 CenterX, t_u16 CenterY, t_u16 Radius, t_u16 StartAngle, t_u16 EndAngle );


/** Select display to be able to communicate with
 *
 * Many displays have a dedicated select input with makes them listen to a shared bus.
 *
 * @param Config       (t_ttc_gfx_config*)  Configuration of gfx device as returned by ttc_gfx_get_configuration()
 */
void gfx_ili93xx_select( t_ttc_gfx_config* Config );
#define gfx_ili93xx_select(Config) gfx_ili93xx_board_select(Config)

/** Deselect display to be able to communicate with
 *
 * Many displays have a dedicated select input with makes them listen to a shared bus.
 *
 * @param Config       (t_ttc_gfx_config*)  Configuration of gfx device as returned by ttc_gfx_get_configuration()
 */
void gfx_ili93xx_deselect( t_ttc_gfx_config* Config );
#define gfx_ili93xx_deselect(Config) gfx_ili93xx_board_deselect(Config)

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

/** Draws rectangle in ColorFg on display
 *
 * @param X              = left border of rectangle  (in rotated orientation)
 * @param Y              = upper border of rectangle (in rotated orientation)
 * @param PhysicalWidth  = width of rectangle        (in native, not rotated, display orientation)
 * @param PhysicalHeight = height of rectangle       (in native, not rotated, display orientation)
 */
void gfx_ili93xx_rect( t_u16 X, t_u16 Y, t_s16 PhysicalWidth, t_s16 PhysicalHeight );

/** Draws rectangle in ColorFg on 90° clockwise rotated display
 *
 * @param X              = left border of rectangle  (in rotated orientation)
 * @param Y              = upper border of rectangle (in rotated orientation)
 * @param PhysicalWidth  = width of rectangle        (in native, not rotated, display orientation)
 * @param PhysicalHeight = height of rectangle       (in native, not rotated, display orientation)
 */
void gfx_ili93xx_rect_090( t_u16 X, t_u16 Y, t_s16 PhysicalWidth, t_s16 PhysicalHeight );

/** Draws rectangle in ColorFg on 180° clockwise rotated display
 *
 * @param X              = left border of rectangle  (in rotated orientation)
 * @param Y              = upper border of rectangle (in rotated orientation)
 * @param PhysicalWidth  = width of rectangle        (in native, not rotated, display orientation)
 * @param PhysicalHeight = height of rectangle       (in native, not rotated, display orientation)
 */
void gfx_ili93xx_rect_180( t_u16 X, t_u16 Y, t_s16 PhysicalWidth, t_s16 PhysicalHeight );

/** Draws rectangle in ColorFg on 270° clockwise rotated display
 *
 * @param X              = left border of rectangle  (in rotated orientation)
 * @param Y              = upper border of rectangle (in rotated orientation)
 * @param PhysicalWidth  = width of rectangle        (in native, not rotated, display orientation)
 * @param PhysicalHeight = height of rectangle       (in native, not rotated, display orientation)
 */
void gfx_ili93xx_rect_270( t_u16 X, t_u16 Y, t_s16 PhysicalWidth, t_s16 PhysicalHeight );

//InsertDriverPrototypes
//}PrivateFunctions

#endif //GFX_ILI93xx_H
