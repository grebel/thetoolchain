#ifndef INPUT_ANALOG
#define INPUT_ANALOG
#define AUTO 0

#include "../ttc_basic.h"
#include "../ttc_gpio.h"
#include "../ttc_task.h"
#include "../DEPRECATED_ttc_input_types.h"
#include "../ttc_adc.h"
//{ externals provided by DEPRECATED_ttc_input.c *************************************

//X #include "pin_configuration.h"
//X #include "lcd_color.h"
#define READ_TIMES 5  //shows how often the positions will be quested (average build)
#define LOST_VAL 2
#define ERR_RANGE 50
#define KEY_DOWN 0x01
#define KEY_UP   0x00
#define TOUCHFACTOR 1/13
#define OFFSET 20
#define INPUT_ANALOG_SMART_COMPILE 0


void input_analog_init();

void input_analog_configuration(ttc_input_generic_t* ttc_input_CurrentInput);

BOOL input_analog_read_position(volatile u16_t *adc_x, volatile u16_t *adc_y);

void input_analog_get_configuration(ttc_input_generic_t* ttc_input_CurrentInput);

void input_analog_prepare_wait();

void my_qsort(u16_t *links, u16_t *rechts);
/***************************************************************************************************************************************
  input_analog_TouchCheckAll
  load the current position of the touchpanel an check whether the position belongs to a registered touchhandler function
****************************************************************************************************************************************/



#endif // INPUT_ANALOG
