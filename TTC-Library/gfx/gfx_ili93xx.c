/** { gfx_ili93xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for gfx devices on gfx_ili93xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 21 at 20140313 10:58:25 UTC
 *
 *  Note: See ttc_gfx.h for description of gfx_ili93xx independent GFX implementation.
 *
 *  Authors: Patrick von Poblotzki, Gregor Rebel
}*/

#include "gfx_ili93xx.h"
#include "../ttc_register.h"

/*?
// define parallel banks to use for parallel output in this example
#ifdef TTC_GPIO_PARALLEL16_1_BANK_
#define PARALLEL16_BANK TTC_GPIO_PARALLEL16_1_BANK
#else
// using 8-bit ports if no 16-bit port available
#ifdef TTC_GPIO_PARALLEL08_1_BANK
#define PARALLEL08_1_BANK     TTC_GPIO_PARALLEL08_1_BANK
#define PARALLEL08_1_FIRSTPIN TTC_GPIO_PARALLEL08_1_FIRSTPIN
#endif
#ifdef TTC_GPIO_PARALLEL08_2_BANK
#define PARALLEL08_2_BANK     TTC_GPIO_PARALLEL08_2_BANK
#define PARALLEL08_2_FIRSTPIN TTC_GPIO_PARALLEL08_2_FIRSTPIN
#endif
#endif
*/

// { Global Variables ***********************************************************

// Using global variable from ttc_gfx.c for quick access to current display reduces amount of function arguments and pointer derefencing
extern t_ttc_gfx_quick ttc_gfx_Quick;

/** Reusable variables for highspeed draw functions
 *
 * ttc_gfx allows only one task to use its functions.
 * This has several advantages:
 * + Faster
 *   Functions does not need to allocate local variables on stack for each function call.
 *   Instead they reuse global variables.
 * + Less RAM usage
 *   All gfx_ili93xx functions share a small set of global variables.
 *   A set of defines allows to use memorizable names of shared global variables.
 */
BOOL         gfx_ili93xx_WindowUnchanged; // ==FALSE: drawing window has been reconfigured
t_s16        s16a, s16b, s16c, s16d, s16e, s16f, s16g;
t_u16        u16a, u16b, u16c;
t_u8         u8a, u8b;

#ifdef EXTENSION_basic_cm3
    // Data for parallel 16-bit access to gpio port for super fast write strobe toggling (works on CortexM3 only)
    t_u16* gfx_ili93xx_WriteStrobe_Port;
    t_u16  gfx_ili93xx_WriteStrobe_Mask;
#endif

// schedule names used in gfx functions onto above variables
#define gfx_ili93xx_PhysicalX             u8a
#define gfx_ili93xx_PhysicalY             s16a
#define gfx_ili93xx_Circle_F              s16a
#define gfx_ili93xx_Circle_Length         s16b
#define gfx_ili93xx_Circle_LineX          u8a
#define gfx_ili93xx_Circle_LineY          u16a
#define gfx_ili93xx_Circle_X              s16c
#define gfx_ili93xx_Circle_Y              s16d
#define gfx_ili93xx_Circle_ddF_x          s16e
#define gfx_ili93xx_Circle_ddF_y          s16f
#define gfx_ili93xx_char_BitLine          u16a
#define gfx_ili93xx_char_CharWidth        u8a
#define gfx_ili93xx_char_LineNo           u8b
#define gfx_ili93xx_data_Strobe0          u16b
#define gfx_ili93xx_data_Strobe1          u16c
#define gfx_ili93xx_line_DeltaX           s16a
#define gfx_ili93xx_line_DeltaY           s16b
#define gfx_ili93xx_line_Distance         s16c
#define gfx_ili93xx_line_IncrementX       s16d
#define gfx_ili93xx_line_IncrementY       s16e
#define gfx_ili93xx_line_T                u16a
#define gfx_ili93xx_line_XError           s16f
#define gfx_ili93xx_line_YError           s16g

/*
t_s16        gfx_ili93xx_Circle_F        = 0;
t_s16        gfx_ili93xx_Circle_X        = 0;
t_s16        gfx_ili93xx_Circle_Y        = 0;
t_s16        gfx_ili93xx_Circle_ddF_x    = 0;
t_s16        gfx_ili93xx_Circle_ddF_y    = 0;

t_s16        gfx_ili93xx_line_DeltaX =0;
t_s16        gfx_ili93xx_line_DeltaY =0;
t_s16        gfx_ili93xx_line_Distance =0;
t_s16        gfx_ili93xx_line_IncrementX =0;
t_s16        gfx_ili93xx_line_IncrementY =0;
t_s16        gfx_ili93xx_line_XError =0;
t_s16        gfx_ili93xx_line_YError =0;

t_u8         gfx_ili93xx_char_CharWidth       = 0;
t_u8         gfx_ili93xx_Circle_Length   = 0;
t_u8         gfx_ili93xx_Circle_LineX    = 0;
t_u8         gfx_ili93xx_char_LineNo          = 0;
t_u8         gfx_ili93xx_PhysicalX       = 0;
t_u16        gfx_ili93xx_char_BitLine         = 0;
t_u16        gfx_ili93xx_BitNo           = 0;
t_u16        gfx_ili93xx_Circle_LineY    = 0;
t_u16        gfx_ili93xx_PhysicalY       = 0;
t_u16        gfx_ili93xx_line_T =0;
*/
const t_u16* gfx_ili93xx_char_BitMap;

// }Global Variables

// { Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _gfx_ili93xx_foo(t_ttc_gfx_config* Config)

// super fast versions with no clipping
void _gfx_ili93xx_pixel_clr_at( t_u16 X, t_u16 Y );
void _gfx_ili93xx_pixel_set_at( t_u16 X, t_u16 Y );

/** initialize data repeat mode
 *
 * Once initialized, each writemode line toggle will repeat the previous data word.
 * This special mode allows to fill areas with pixels of same color much quicker than by
 * writing the same dataword for each pixel.
 */
void _gfx_ili93xx_repeat_start();

/** ends repeat mode
 */
void _gfx_ili93xx_repeat_end();

/** toggles writemode line to repeat the latest written dataword given times
 *
 * This function allows to speed up area filling even more because it does not require to send dataword again.
 * The ili93xx controller allows to reuse previous dataword by just toggling the writemode line.
 *
 * Note: This function might not work on all boards!
 */
void _gfx_ili93xx_data_fill16( t_u16 Data, t_u32 Times );

// InsertPrivatePrototypes above (DO NOT DELETE THIS LINE!)

// InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

// { Function Definitions *******************************************************
void                gfx_ili93xx_arrow( t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2, t_u8 Length1, t_u8 Length2 ) {
    TODO( "Function gfx_ili93xx_arrow() is empty." );
    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }
}
void                gfx_ili93xx_backlight( t_u8 Level ) {
#ifdef TTC_GFX1_ILI93XX_PIN_LCDLED
    if ( Level ) // currently only on or off (ToDo: implement dimming via PWM signal)
    { ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_LCDLED ); }
    else
    { ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_LCDLED ); }
#endif
}
void                gfx_ili93xx_char( t_u8 Char ) {

    gfx_ili93xx_PhysicalY = ttc_gfx_Quick.Config->PhysicalY;
    Char -= ttc_gfx_Quick.Font->FirstASCII;
    gfx_ili93xx_char_LineNo = ttc_gfx_Quick.Font->CharHeight;
    gfx_ili93xx_char_BitMap = ttc_gfx_Quick.Font->Characters + ( Char ) * gfx_ili93xx_char_LineNo;

    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }
    for ( ; gfx_ili93xx_char_LineNo > 0; gfx_ili93xx_char_LineNo-- ) {
        gfx_ili93xx_char_BitLine = *gfx_ili93xx_char_BitMap++;

        if ( gfx_ili93xx_char_BitLine ) {
            gfx_ili93xx_PhysicalX = ttc_gfx_Quick.Config->PhysicalX;
            do {
                if ( gfx_ili93xx_char_BitLine & 1 ) {
                    gfx_ili93xx_pixel_set_at( gfx_ili93xx_PhysicalX, gfx_ili93xx_PhysicalY );
                }
                gfx_ili93xx_PhysicalX++;
                gfx_ili93xx_char_BitLine /= 2;
            }
            while ( gfx_ili93xx_char_BitLine );
        }
        gfx_ili93xx_PhysicalY++;
    }
}
void                gfx_ili93xx_char_090( t_u8 Char ) { // stuck here
    gfx_ili93xx_PhysicalX = ttc_gfx_Quick.Config->PhysicalX;
    Char -= ttc_gfx_Quick.Font->FirstASCII;
    gfx_ili93xx_char_LineNo = ttc_gfx_Quick.Font->CharHeight;
    gfx_ili93xx_char_BitMap = ttc_gfx_Quick.Font->Characters + ( Char ) * gfx_ili93xx_char_LineNo;

    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }
    for ( ; gfx_ili93xx_char_LineNo > 0; gfx_ili93xx_char_LineNo-- ) {
        gfx_ili93xx_char_BitLine = *gfx_ili93xx_char_BitMap++;

        if ( gfx_ili93xx_char_BitLine ) {
            gfx_ili93xx_PhysicalY = ttc_gfx_Quick.Config->PhysicalY;
            do {
                if ( gfx_ili93xx_char_BitLine & 1 ) {
                    gfx_ili93xx_pixel_set_at( gfx_ili93xx_PhysicalX, gfx_ili93xx_PhysicalY );
                }
                gfx_ili93xx_PhysicalY--;
                gfx_ili93xx_char_BitLine /= 2;
            }
            while ( gfx_ili93xx_char_BitLine );
        }
        gfx_ili93xx_PhysicalX++;
    }
}
void                gfx_ili93xx_char_270( t_u8 Char ) {
    gfx_ili93xx_PhysicalX = ttc_gfx_Quick.Config->PhysicalX;
    Char -= ttc_gfx_Quick.Font->FirstASCII;
    gfx_ili93xx_char_LineNo = ttc_gfx_Quick.Font->CharHeight;
    gfx_ili93xx_char_BitMap = ttc_gfx_Quick.Font->Characters + ( Char ) * gfx_ili93xx_char_LineNo;

    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }
    for ( ; gfx_ili93xx_char_LineNo > 0; gfx_ili93xx_char_LineNo-- ) {
        gfx_ili93xx_char_BitLine = *gfx_ili93xx_char_BitMap++;

        if ( gfx_ili93xx_char_BitLine ) {
            gfx_ili93xx_PhysicalY = ttc_gfx_Quick.Config->PhysicalY;
            do {
                if ( gfx_ili93xx_char_BitLine & 1 ) {
                    gfx_ili93xx_pixel_set_at( gfx_ili93xx_PhysicalX, gfx_ili93xx_PhysicalY );
                }
                gfx_ili93xx_PhysicalY++;
                gfx_ili93xx_char_BitLine /= 2;
            }
            while ( gfx_ili93xx_char_BitLine );
        }
        gfx_ili93xx_PhysicalX--;
    }
    ttc_gfx_Quick.Config->PhysicalY += ttc_gfx_Quick.Font->CharWidth;
}
void                gfx_ili93xx_char_180( t_u8 Char ) {
    gfx_ili93xx_PhysicalY = ttc_gfx_Quick.Config->PhysicalY;
    Char -= ttc_gfx_Quick.Font->FirstASCII;
    gfx_ili93xx_char_LineNo = ttc_gfx_Quick.Font->CharHeight;
    gfx_ili93xx_char_BitMap = ttc_gfx_Quick.Font->Characters + ( Char ) * gfx_ili93xx_char_LineNo;

    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }
    for ( ; gfx_ili93xx_char_LineNo > 0; gfx_ili93xx_char_LineNo-- ) {
        gfx_ili93xx_char_BitLine = *gfx_ili93xx_char_BitMap++;

        if ( gfx_ili93xx_char_BitLine ) {
            gfx_ili93xx_PhysicalX = ttc_gfx_Quick.Config->PhysicalX;
            do {
                if ( gfx_ili93xx_char_BitLine & 1 ) {
                    gfx_ili93xx_pixel_set_at( gfx_ili93xx_PhysicalX, gfx_ili93xx_PhysicalY );
                }
                gfx_ili93xx_PhysicalX--;
                gfx_ili93xx_char_BitLine /= 2;
            }
            while ( gfx_ili93xx_char_BitLine );
        }
        gfx_ili93xx_PhysicalY--;
    }
}
void                gfx_ili93xx_char_solid( t_u8 Char ) {
    gfx_ili93xx_PhysicalY = ttc_gfx_Quick.Config->PhysicalY;
    Char -= ttc_gfx_Quick.Font->FirstASCII;
    gfx_ili93xx_char_CharWidth = ttc_gfx_Quick.Font->CharWidth;
    t_u8 Width = 0;
    gfx_ili93xx_char_LineNo = ttc_gfx_Quick.Font->CharHeight;
    gfx_ili93xx_char_BitMap = ttc_gfx_Quick.Font->Characters + ( Char ) * gfx_ili93xx_char_LineNo;

    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }
    for ( ; gfx_ili93xx_char_LineNo > 0; gfx_ili93xx_char_LineNo-- ) {
        gfx_ili93xx_char_BitLine = *gfx_ili93xx_char_BitMap++;

        gfx_ili93xx_PhysicalX = ttc_gfx_Quick.Config->PhysicalX;
        do {
            if ( gfx_ili93xx_char_BitLine & 1 ) {
                gfx_ili93xx_pixel_set_at( gfx_ili93xx_PhysicalX, gfx_ili93xx_PhysicalY );
            }
            else {
                gfx_ili93xx_pixel_clr_at( gfx_ili93xx_PhysicalX, gfx_ili93xx_PhysicalY );
            }
            gfx_ili93xx_PhysicalX++;
            gfx_ili93xx_char_BitLine /= 2;
            Width++;
        }
        while ( Width != gfx_ili93xx_char_CharWidth );

        Width = 0;
        gfx_ili93xx_PhysicalY++;
    }
}
void                gfx_ili93xx_circle( t_u16 CenterX, t_u16 CenterY, t_u16 Radius ) {
    gfx_ili93xx_Circle_F     = 1 - Radius;
    gfx_ili93xx_Circle_ddF_x = 0;
    gfx_ili93xx_Circle_ddF_y = -2 * Radius;
    gfx_ili93xx_Circle_X     = 0;
    gfx_ili93xx_Circle_Y     = Radius;

    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }

    if ( ( Radius > CenterX )
            || ( Radius > CenterY )
            || ( CenterX + Radius >= ttc_gfx_Quick.PhysicalWidth )
            || ( CenterY + Radius >= ttc_gfx_Quick.PhysicalHeight )
       ) { // some pixels will fall outside boundaries: draw circle with clipping
        gfx_ili93xx_pixel_set_at( CenterX, CenterY + Radius );
        gfx_ili93xx_pixel_set_at( CenterX, CenterY - Radius );
        gfx_ili93xx_pixel_set_at( CenterX + Radius, CenterY );
        gfx_ili93xx_pixel_set_at( CenterX - Radius, CenterY );

        while ( gfx_ili93xx_Circle_X < gfx_ili93xx_Circle_Y ) {
            if ( gfx_ili93xx_Circle_F >= 0 ) {
                gfx_ili93xx_Circle_Y--;
                gfx_ili93xx_Circle_ddF_y += 2;
                gfx_ili93xx_Circle_F += gfx_ili93xx_Circle_ddF_y;
            }
            gfx_ili93xx_Circle_X++;
            gfx_ili93xx_Circle_ddF_x += 2;
            gfx_ili93xx_Circle_F += gfx_ili93xx_Circle_ddF_x + 1;

            gfx_ili93xx_pixel_set_at( CenterX + gfx_ili93xx_Circle_X, CenterY + gfx_ili93xx_Circle_Y );
            gfx_ili93xx_pixel_set_at( CenterX - gfx_ili93xx_Circle_X, CenterY + gfx_ili93xx_Circle_Y );
            gfx_ili93xx_pixel_set_at( CenterX - gfx_ili93xx_Circle_X, CenterY - gfx_ili93xx_Circle_Y );
            gfx_ili93xx_pixel_set_at( CenterX + gfx_ili93xx_Circle_X, CenterY - gfx_ili93xx_Circle_Y );

            gfx_ili93xx_pixel_set_at( CenterX + gfx_ili93xx_Circle_Y, CenterY + gfx_ili93xx_Circle_X );
            gfx_ili93xx_pixel_set_at( CenterX - gfx_ili93xx_Circle_Y, CenterY + gfx_ili93xx_Circle_X );
            gfx_ili93xx_pixel_set_at( CenterX - gfx_ili93xx_Circle_Y, CenterY - gfx_ili93xx_Circle_X );
            gfx_ili93xx_pixel_set_at( CenterX + gfx_ili93xx_Circle_Y, CenterY - gfx_ili93xx_Circle_X );
        }
    }
    else {   // all pixels will fall inside boundaries: draw circle without clipping
        _gfx_ili93xx_pixel_set_at( CenterX, CenterY + Radius );
        gfx_ili93xx_set_y( CenterY - Radius );
        gfx_ili93xx_pixel_set();
        _gfx_ili93xx_pixel_set_at( CenterX + Radius, CenterY );
        gfx_ili93xx_set_x( CenterX - Radius );
        gfx_ili93xx_pixel_set();

        while ( gfx_ili93xx_Circle_X < gfx_ili93xx_Circle_Y ) {
            if ( gfx_ili93xx_Circle_F >= 0 ) {
                gfx_ili93xx_Circle_Y--;
                gfx_ili93xx_Circle_ddF_y += 2;
                gfx_ili93xx_Circle_F += gfx_ili93xx_Circle_ddF_y;
            }
            gfx_ili93xx_Circle_X++;
            gfx_ili93xx_Circle_ddF_x += 2;
            gfx_ili93xx_Circle_F += gfx_ili93xx_Circle_ddF_x + 1;
            gfx_ili93xx_cursor_set( CenterX + gfx_ili93xx_Circle_X, CenterY + gfx_ili93xx_Circle_Y );
            gfx_ili93xx_pixel_set();
            gfx_ili93xx_set_x( CenterX - gfx_ili93xx_Circle_X );
            gfx_ili93xx_pixel_set();
            gfx_ili93xx_set_y( CenterY - gfx_ili93xx_Circle_Y );
            gfx_ili93xx_pixel_set();
            gfx_ili93xx_set_x( CenterX + gfx_ili93xx_Circle_X );
            gfx_ili93xx_pixel_set();

            gfx_ili93xx_cursor_set( CenterX + gfx_ili93xx_Circle_Y, CenterY + gfx_ili93xx_Circle_X );
            gfx_ili93xx_pixel_set();
            gfx_ili93xx_set_x( CenterX - gfx_ili93xx_Circle_Y );
            gfx_ili93xx_pixel_set();
            gfx_ili93xx_set_y( CenterY - gfx_ili93xx_Circle_X );
            gfx_ili93xx_pixel_set();
            gfx_ili93xx_set_x( CenterX + gfx_ili93xx_Circle_Y );
            gfx_ili93xx_pixel_set();
        }

    }
}
void                gfx_ili93xx_circle_fill( t_u16 CenterX, t_u16 CenterY, t_u16 Radius ) {
    if ( Radius == 0 ) { return; }

    gfx_ili93xx_Circle_F     = 1 - Radius;
    gfx_ili93xx_Circle_ddF_x = 0;
    gfx_ili93xx_Circle_ddF_y = -2 * Radius;
    gfx_ili93xx_Circle_X     = 0;
    gfx_ili93xx_Circle_Y     = Radius;

    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }

    if ( ( Radius > CenterX )
            || ( Radius > CenterY )
            || ( CenterX + Radius >= ttc_gfx_Quick.PhysicalWidth )
            || ( CenterY + Radius >= ttc_gfx_Quick.PhysicalHeight )
       ) { // some pixels will fall outside boundaries: draw circle with clipping
        if ( CenterY < ttc_gfx_Quick.PhysicalHeight ) { // line is inside vertical range
            gfx_ili93xx_Circle_Length = 2 * Radius;
            if ( CenterX > Radius ) { // will not cross left boundary
                gfx_ili93xx_Circle_LineX  = CenterX - Radius;
            }
            else {                    // crossing left boundary: cut horizontal line at left border
                gfx_ili93xx_Circle_Length -= ( Radius - CenterX );
                gfx_ili93xx_Circle_LineX = 0;
            }
            if ( gfx_ili93xx_Circle_LineX + gfx_ili93xx_Circle_Length > ttc_gfx_Quick.PhysicalWidth ) {
                gfx_ili93xx_Circle_Length = ttc_gfx_Quick.PhysicalWidth - gfx_ili93xx_Circle_LineX;
            }
            gfx_ili93xx_line_horizontal( gfx_ili93xx_Circle_LineX, CenterY, gfx_ili93xx_Circle_Length );
        }

        while ( gfx_ili93xx_Circle_X < gfx_ili93xx_Circle_Y ) {
            if ( gfx_ili93xx_Circle_F >= 0 ) {
                gfx_ili93xx_Circle_Y--;
                gfx_ili93xx_Circle_ddF_y += 2;
                gfx_ili93xx_Circle_F += gfx_ili93xx_Circle_ddF_y;
            }
            gfx_ili93xx_Circle_X++;
            gfx_ili93xx_Circle_ddF_x += 2;
            gfx_ili93xx_Circle_F += gfx_ili93xx_Circle_ddF_x + 1;

            gfx_ili93xx_Circle_Length = gfx_ili93xx_Circle_X * 2;
            if ( CenterX > gfx_ili93xx_Circle_X ) // will not cross left boundary
            { gfx_ili93xx_Circle_LineX  = CenterX - gfx_ili93xx_Circle_X; }
            else {                                // crossing left boundary: cut horizontal line at left border
                gfx_ili93xx_Circle_Length -= ( gfx_ili93xx_Circle_X - CenterX );
                gfx_ili93xx_Circle_LineX = 0;
            }
            if ( gfx_ili93xx_Circle_LineX + gfx_ili93xx_Circle_Length > ttc_gfx_Quick.PhysicalWidth ) // will cross right boundary: cut line at right border
            { gfx_ili93xx_Circle_Length = ttc_gfx_Quick.PhysicalWidth - gfx_ili93xx_Circle_LineX; }

            if ( gfx_ili93xx_Circle_Length ) {
                gfx_ili93xx_Circle_LineY = CenterY + gfx_ili93xx_Circle_Y;
                if ( gfx_ili93xx_Circle_LineY < ttc_gfx_Quick.PhysicalHeight )
                { gfx_ili93xx_line_horizontal( gfx_ili93xx_Circle_LineX, gfx_ili93xx_Circle_LineY, gfx_ili93xx_Circle_Length ); } // bottom up
            }

            if ( CenterY > gfx_ili93xx_Circle_Y ) { // line inside positive y-coordinate
                gfx_ili93xx_Circle_LineY = CenterY - gfx_ili93xx_Circle_Y;
                if ( gfx_ili93xx_Circle_LineY < ttc_gfx_Quick.PhysicalHeight )
                { gfx_ili93xx_line_horizontal( gfx_ili93xx_Circle_LineX, gfx_ili93xx_Circle_LineY, gfx_ili93xx_Circle_X * 2 ); } // top down
            }

            gfx_ili93xx_Circle_Length = gfx_ili93xx_Circle_Y * 2;
            if ( CenterX > gfx_ili93xx_Circle_Y ) // will not cross left boundary
            { gfx_ili93xx_Circle_LineX  = CenterX - gfx_ili93xx_Circle_Y; }
            else {                              // crossing left boundary: cut horizontal line at left border
                gfx_ili93xx_Circle_Length -= ( gfx_ili93xx_Circle_Y - CenterX );
                gfx_ili93xx_Circle_LineX = 0;
            }
            if ( gfx_ili93xx_Circle_LineX + gfx_ili93xx_Circle_Length > ttc_gfx_Quick.PhysicalWidth ) // will cross right boundary: cut line at right border
            { gfx_ili93xx_Circle_Length = ttc_gfx_Quick.PhysicalWidth - gfx_ili93xx_Circle_LineX; }

            if ( gfx_ili93xx_Circle_Length ) {
                gfx_ili93xx_Circle_LineY = CenterY + gfx_ili93xx_Circle_X;
                if ( gfx_ili93xx_Circle_LineY < ttc_gfx_Quick.PhysicalHeight )
                { gfx_ili93xx_line_horizontal( gfx_ili93xx_Circle_LineX, gfx_ili93xx_Circle_LineY, gfx_ili93xx_Circle_Length ); } // center down

                if ( CenterY > gfx_ili93xx_Circle_X ) { // line inside positive y-coordinate
                    gfx_ili93xx_Circle_LineY = CenterY - gfx_ili93xx_Circle_X;
                    if ( gfx_ili93xx_Circle_LineY < ttc_gfx_Quick.PhysicalHeight )
                    { gfx_ili93xx_line_horizontal( gfx_ili93xx_Circle_LineX, gfx_ili93xx_Circle_LineY, gfx_ili93xx_Circle_Length ); } // center up
                }
            }
        }
    }
    else { // all pixels will fall inside boundaries: draw circle without clipping
        gfx_ili93xx_line_horizontal( CenterX - Radius, CenterY, 2 * Radius );

        while ( gfx_ili93xx_Circle_X < gfx_ili93xx_Circle_Y ) {
            if ( gfx_ili93xx_Circle_F >= 0 ) {
                gfx_ili93xx_Circle_Y--;
                gfx_ili93xx_Circle_ddF_y += 2;
                gfx_ili93xx_Circle_F += gfx_ili93xx_Circle_ddF_y;
            }
            gfx_ili93xx_Circle_X++;
            gfx_ili93xx_Circle_ddF_x += 2;
            gfx_ili93xx_Circle_F += gfx_ili93xx_Circle_ddF_x + 1;

            if ( gfx_ili93xx_Circle_X > 0 ) {
                gfx_ili93xx_line_horizontal( CenterX - gfx_ili93xx_Circle_X, CenterY + gfx_ili93xx_Circle_Y, gfx_ili93xx_Circle_X * 2 ); // bottom up
                gfx_ili93xx_line_horizontal( CenterX - gfx_ili93xx_Circle_X, CenterY - gfx_ili93xx_Circle_Y, gfx_ili93xx_Circle_X * 2 ); // top down
            }
            if ( gfx_ili93xx_Circle_Y > 0 ) {
                gfx_ili93xx_line_horizontal( CenterX - gfx_ili93xx_Circle_Y, CenterY + gfx_ili93xx_Circle_X, gfx_ili93xx_Circle_Y * 2 ); // center down
                gfx_ili93xx_line_horizontal( CenterX - gfx_ili93xx_Circle_Y, CenterY - gfx_ili93xx_Circle_X, gfx_ili93xx_Circle_Y * 2 ); // center up
            }

        }
    }
}
void                gfx_ili93xx_circle_segment( t_u16 CenterX, t_u16 CenterY, t_u16 Radius, t_u16 StartAngle, t_u16 EndAngle ) {

    TODO( "Function gfx_ili93xx_circle_segment() is empty." );
    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }


}
void                gfx_ili93xx_circle_segment_fill( t_u16 CenterX, t_u16 CenterY, t_u16 Radius, t_u16 StartAngle, t_u16 EndAngle ) {


    TODO( "Function gfx_ili93xx_circle_segment_fill() is empty." );
    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }


}
void                gfx_ili93xx_clear() {
    t_base PixelCount = ttc_gfx_Quick.Config->PhysicalHeight * ttc_gfx_Quick.Config->PhysicalWidth;
    if ( 1 ) { // fill screen in one window
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_EntryMode, 0x1030 );                           // Set GRAM Access Direction Setting: AM=0, I/D=0b11, BGR=1 (ili9325ds p.54)
        if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
            gfx_ili93xx_window_reset();
        }
        gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );                                    // prepare GRAM for writing
        gfx_ili93xx_cursor_set( 0, 0 );
        gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );                                    // prepare GRAM for writing
        _gfx_ili93xx_data_fill16( ttc_gfx_Quick.Config->ColorBg, PixelCount );        // mass send background color
    }
    else {     // fill screen in two windows
        PixelCount /= 2;

        // fill upper half of screen
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_EntryMode, 0x1030 );                           // Set GRAM Access Direction Setting: AM=0, I/D=0b11, BGR=1 (ili9325ds p.54)
        gfx_ili93xx_window_set( 0, 239, 0, 159 );
        gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );                                    // prepare GRAM for writing
        gfx_ili93xx_cursor_set( 0, 0 );
        gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );                                    // prepare GRAM for writing
        _gfx_ili93xx_data_fill16( ttc_gfx_Quick.Config->ColorBg, PixelCount ); // mass send background color

        // fill lower half of screen
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_EntryMode, 0x1030 );                           // Set GRAM Access Direction Setting: AM=0, I/D=0b11, BGR=1 (ili9325ds p.54)
        gfx_ili93xx_window_set( 0, 239, 120, 319 );
        gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );                                    // prepare GRAM for writing
        gfx_ili93xx_cursor_set( 0, 160 );
        gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );                                    // prepare GRAM for writing
        _gfx_ili93xx_data_fill16( ttc_gfx_Quick.Config->ColorBg, PixelCount ); // mass send background color

        gfx_ili93xx_WindowUnchanged = FALSE;  // drawing window needs to be reset before next drawing operation
    }
}
void                gfx_ili93xx_color_bg16( t_u16 Color ) {
    if ( ttc_gfx_Quick.Config ) {
        ttc_gfx_Quick.Config->ColorBg = Color;
    }
}
void                gfx_ili93xx_color_bg24( t_u32 Color ) {
    if ( ttc_gfx_Quick.Config ) {
        ttc_gfx_Quick.Config->ColorBg = ( ( ( ( Color >> 3 ) & 0x1F ) | ( ( Color >> 5 ) & 0x7E0 ) | ( ( Color >> 8 ) & 0xF800 ) ) );
    }
}
void                gfx_ili93xx_color_bg_palette( t_u8 PaletteIndex ) {

}
void                gfx_ili93xx_color_fg16( t_u16 Color ) {
    if ( ttc_gfx_Quick.Config ) {
        ttc_gfx_Quick.Config->ColorFg = Color;
    }
}
void                gfx_ili93xx_color_fg24( t_u32 Color ) {
    if ( ttc_gfx_Quick.Config ) {
        ttc_gfx_Quick.Config->ColorFg = ( ( ( ( Color >> 3 ) & 0x1F ) | ( ( Color >> 5 ) & 0x7E0 ) | ( ( Color >> 8 ) & 0xF800 ) ) );
    }
}
void                gfx_ili93xx_color_fg_palette( t_u8 PaletteIndex ) {

}
void                gfx_ili93xx_cursor_set( t_u16 X, t_u16 Y ) {
    gfx_ili93xx_set_x( X );
    gfx_ili93xx_set_y( Y );
}
e_ttc_gfx_errorcode gfx_ili93xx_deinit( t_ttc_gfx_config* Config ) {
    Assert_GFX_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    return gfx_ili93xx_reset( Config );
}
e_ttc_gfx_errorcode gfx_ili93xx_get_features( t_ttc_gfx_config* Config ) {
    Assert_GFX_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Config->PhysicalHeight = TTC_GFX1_HEIGHT;
    Config->PhysicalWidth = TTC_GFX1_WIDTH;
    Config->ColorBg = 0;
    Config->ColorFg = 0xF0FF;
    Config->Depth = TTC_GFX1_DEPTH;
    Config->TextBorderLeft = 0;
    Config->TextBorderTop = 0;
    Config->TextColumns = 10;
    Config->TextRows = 0;

    return ( e_ttc_gfx_errorcode ) 0;
}
t_u32               gfx_ili93xx_identify( t_ttc_gfx_config* Config ) {
    volatile t_u32 DeviceCode = 0;

    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DriverCodeRead, 0x0001 );   // command for reading the register
    ttc_task_msleep( 50 );                           // LCD_Delay 5 ms
    DeviceCode = gfx_ili93xx_board_read_register16( E_gfx_ili93xx_register_DriverCodeRead );
    if ( !DeviceCode ) { // that did not work: maybe it's an ili9341?
        DeviceCode = gfx_ili93xx_board_read_register32( 0xd3 );
    }
    Config->LowLevelConfig->IdCode = DeviceCode;

    return DeviceCode;
}
e_ttc_gfx_errorcode gfx_ili93xx_init( t_ttc_gfx_config* Config ) {
    Assert_GFX_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    gfx_ili93xx_reset( Config );
    gfx_ili93xx_select( Config );
    gfx_ili93xx_backlight( 255 );
    ttc_task_msleep( 100 );

    volatile t_u16 i9i_DeviceCode = gfx_ili93xx_identify( Config );

    // power configuration, not optimized, for more information -> manual ili93xx
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DriverOutputControl,          0x0100 );    // Driver Output Contral.
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_LCDDrivingWaveControl,        0x0700 );    // LCD Driver Waveform Contral.
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_EntryMode,                    0x1030 );    // Entry Mode Set.
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_ResizingControlRegister,      0x0000 );    // Scalling Control.
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DisplayControl2,              0x0202 );    // Display Control 2.(0x0207)
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DisplayControl3,              0x0000 );    // Display Control 3.(0x0000)
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DisplayControl4,              0x0000 );    // Frame Cycle Control.(0x0000)
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_RGBDisplayInterfaceControl1,  0x0001 );    // Extern Display Interface Control 1.(0x0000) = 16 bit
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_FrameMarkerPosition,          0x0000 );    // Frame Maker Position.
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_RGBDisplayInterfaceControl2,  0x0000 );    // Extern Display Interface Control 2.
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_BaseImageDisplayControl,      0x0001 );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_VerticalScrollControl,        0x0000 );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PartialImage1DisplayPosition, 0x0000 );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PartialImage1RAMStartAddress, 0x0000 );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PartialImage1RAMEndAddress,   0x0000 );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PartialImage2DisplayPosition, 0x0000 );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PartialImage2RAMStartAddress, 0x0000 );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PartialImage2RAMEndAddress,   0x0000 );

    if ( i9i_DeviceCode == 0x9325 || i9i_DeviceCode == 0x9328 ) {
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DisplayControl1,          0x0133 );    // Display Control.
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl1,            0x1690 );    // Power Control 1.(0x16b0)
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl2,            0x0227 );    // Power Control 2.(0x0001)
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl3,            0x009d );    // Power Control 3.(0x0138)
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl4,            0x1900 );    // Power Control 4.
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_GRAMHorizontalAddressSet, 0x0000 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_GRAMVerticalAddressSet,   0x0000 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl7,            0x0025 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_FrameRateandColorControl, 0x000d );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_GammaControl1,            0x0007 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_GammaControl2,            0x0303 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_GammaControl3,            0x0003 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_GammaControl4,            0x0206 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_GammaControl5,            0x0008 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_GammaControl6,            0x0406 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_GammaControl7,            0x0304 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_GammaControl8,            0x0007 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_GammaControl9,            0x0602 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_GammaControl10,           0x0008 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DriverOutputControl2,     0xa700 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PanelInterfaceControl1,   0x0010 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PanelInterfaceControl2,   0x0600 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_Unknown1,                 0x78F0 );    // LCD_WriteReg(0x0000,0x0001);

    }
    if ( i9i_DeviceCode == 0x9320 || i9i_DeviceCode == 0x9300 || i9i_DeviceCode == 0x9324 || i9i_DeviceCode == 0x1320 || i9i_DeviceCode == 0x9125 ) {
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DriverCodeRead,           0x0000 );
        ttc_task_msleep( 50 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DisplayControl1,          0x0101 );    // Display Control.
        ttc_task_msleep( 50 );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl1, ( 1 << 12 ) | ( 0 << 8 ) | ( 1 << 7 ) | ( 1 << 6 ) | ( 0 << 4 ) );            // Power Control 1.(0x16b0)
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl2,            0x0007 );                                // Power Control 2.(0x0001)
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl3, ( 1 << 8 ) | ( 1 << 4 ) | ( 0 << 0 ) );            // Power Control 3.(0x0138)
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl4,            0x0b00 );                                // Power Control 4.
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl7,            0x0000 );                                // Power Control 7.
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_FrameRateandColorControl, ( 1 << 14 ) | ( 1 << 4 ) );
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DriverOutputControl2,     0x2700 );    // Driver Output Control.
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PanelInterfaceControl1, ( 0 << 7 ) | ( 16 << 0 ) );    // Frame Cycle Contral.(0x0013)
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PanelInterfaceControl2,   0x0000 );    // Panel Interface Contral 2.(0x0000)
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PanelInterfaceControl3,   0x0001 );    // Panel Interface Contral 3.
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PanelInterfaceControl4,   0x0110 );    // Frame Cycle Contral.(0x0110)
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PanelInterfaceControl5, ( 0 << 8 ) );      //
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PanelInterfaceControl6,   0x0000 );    // Frame Cycle Control
        gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DisplayControl1,          0x0173 );    // (0x0173)
        ttc_task_msleep( 50 );
    }

    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_HorizontalRAMStartAddressPosition, 0x0000 );                     // Set X - Window Start
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_HorizontalRAMEndAddressPosition,   Config->PhysicalWidth - 1 );  // Set X - Window END
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_VerticalRAMStartAddressPosition,   0x0000 );                     // Set Y - Window Start
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_VerticalRAMEndAddressPosition,     Config->PhysicalHeight - 1 ); // Set Y - Window END

    return ( e_ttc_gfx_errorcode ) 0;
}
void                gfx_ili93xx_line( t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2 ) {
    if ( X2 == X1 ) {
        gfx_ili93xx_line_vertical( X1, Y1, Y2 - Y1 );
        return;
    }
    if ( Y2 == Y1 ) {
        gfx_ili93xx_line_horizontal( X1, Y1, X2 - X1 );
        return;
    }

    gfx_ili93xx_line_XError = 0;
    gfx_ili93xx_line_YError = 0;
    gfx_ili93xx_line_DeltaX = X2 - X1;
    gfx_ili93xx_line_DeltaY = Y2 - Y1;

    if ( gfx_ili93xx_line_DeltaX > 0 )
    { gfx_ili93xx_line_IncrementX = 1; }
    else if ( gfx_ili93xx_line_DeltaX == 0 )
    { gfx_ili93xx_line_IncrementX = 0; }
    else
    { gfx_ili93xx_line_IncrementX = -1; gfx_ili93xx_line_DeltaX = -gfx_ili93xx_line_DeltaX; }

    if ( gfx_ili93xx_line_DeltaY > 0 )
    { gfx_ili93xx_line_IncrementY = 1; }
    else if ( gfx_ili93xx_line_DeltaY == 0 )
    { gfx_ili93xx_line_IncrementY = 0; }
    else
    { gfx_ili93xx_line_IncrementY = -1; gfx_ili93xx_line_DeltaY = -gfx_ili93xx_line_DeltaY; }

    if ( gfx_ili93xx_line_DeltaX > gfx_ili93xx_line_DeltaY )
    { gfx_ili93xx_line_Distance = gfx_ili93xx_line_DeltaX; }
    else
    { gfx_ili93xx_line_Distance = gfx_ili93xx_line_DeltaY; }

    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }
    gfx_ili93xx_pixel_set_at( X1, Y1 );
    for ( gfx_ili93xx_line_T = 0; gfx_ili93xx_line_T <= gfx_ili93xx_line_Distance + 1; gfx_ili93xx_line_T++ ) {

        gfx_ili93xx_line_XError += gfx_ili93xx_line_DeltaX ;
        gfx_ili93xx_line_YError += gfx_ili93xx_line_DeltaY ;
        if ( gfx_ili93xx_line_XError > gfx_ili93xx_line_Distance ) {
            gfx_ili93xx_line_XError -= gfx_ili93xx_line_Distance;
            X1 += gfx_ili93xx_line_IncrementX;
            gfx_ili93xx_set_x( X1 );
        }
        if ( gfx_ili93xx_line_YError > gfx_ili93xx_line_Distance ) {
            gfx_ili93xx_line_YError -= gfx_ili93xx_line_Distance;
            Y1 += gfx_ili93xx_line_IncrementY;
            gfx_ili93xx_set_y( Y1 );
        }
        gfx_ili93xx_pixel_set();
    }
}
void                gfx_ili93xx_line_horizontal( t_u16 X, t_u16 Y, t_s16 Length ) {
    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }
    Assert_GFX( Length > 0, ttc_assert_origin_auto ); // caller must ensure that this value is positive!
    if ( 0 && ( Length < 0 ) ) { // adjust negative length (not required as already checked by assert above)
        gfx_ili93xx_cursor_set( X + Length, Y );
        Length = -Length;
    }
    else
    { gfx_ili93xx_cursor_set( X, Y ); }

    gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );
    _gfx_ili93xx_data_fill16( ttc_gfx_Quick.Config->ColorFg, Length );
}
void                gfx_ili93xx_line_horizontal_090( t_u16 X, t_u16 Y, t_s16 Length ) {

    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }
    Assert_GFX( Length > 0, ttc_assert_origin_auto ); // caller must ensure that this value is positive!
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_EntryMode, 0x1038 );
    gfx_ili93xx_cursor_set( X, ttc_gfx_Quick.Config->PhysicalHeight - Y );
    gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );
    _gfx_ili93xx_data_fill16( ttc_gfx_Quick.Config->ColorFg, Length );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_EntryMode, 0x1030 );
}
void                gfx_ili93xx_line_vertical( t_u16 X, t_u16 Y, t_s16 Length ) {

    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_EntryMode, 0x1038 );

    Assert_GFX( Length > 0, ttc_assert_origin_auto ); // caller must ensure that this value is positive!
    if ( 0 && ( Length < 0 ) ) { // adjust negative length
        gfx_ili93xx_cursor_set( X, Y + Length );
        Length = -Length;
    }
    else
    { gfx_ili93xx_cursor_set( X, Y ); }

    gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );
    _gfx_ili93xx_data_fill16( ttc_gfx_Quick.Config->ColorFg, Length );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_EntryMode, 0x1030 ); // reset GRAM address increment back to horizontal
}
void                gfx_ili93xx_line_vertical_090( t_u16 X, t_u16 Y, t_s16 Length ) {

    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }
    Assert_GFX( Length > 0, ttc_assert_origin_auto ); // caller must ensure that this value is positive!
    gfx_ili93xx_cursor_set( X, Y );
    gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );
    t_u16  ColorFg = ttc_gfx_Quick.Config->ColorFg;
    gfx_ili93xx_board_write_data16( ColorFg );
    _gfx_ili93xx_data_fill16( ColorFg,  Length );
}
e_ttc_gfx_errorcode gfx_ili93xx_load_defaults( t_ttc_gfx_config* Config ) {
    Assert_GFX_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    Config->Architecture   = ta_gfx_ili93xx;
    Config->ColorBg        = 0xFFFF;
    Config->ColorFg        = 0x0;

    return ( e_ttc_gfx_errorcode ) 0;
}
void                gfx_ili93xx_pixel_clr() {
    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }
    gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );           // Command for write dot a activ position
    gfx_ili93xx_board_write_data16( ttc_gfx_Quick.Config->ColorBg );  // Color-data for the point
}
void                gfx_ili93xx_pixel_clr_at( t_u16 X, t_u16 Y ) {
    if ( ( X < ttc_gfx_Quick.PhysicalWidth ) && ( Y < ttc_gfx_Quick.PhysicalHeight ) ) {
        if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
            gfx_ili93xx_window_reset();
        }
        gfx_ili93xx_cursor_set( X, Y );
        gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );
        gfx_ili93xx_board_write_data16( ttc_gfx_Quick.Config->ColorBg );
    }
}
void                gfx_ili93xx_pixel_set() {
    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }
    gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );
    gfx_ili93xx_board_write_data16( ttc_gfx_Quick.Config->ColorFg );
}
void                gfx_ili93xx_pixel_set_at( t_u16 X, t_u16 Y ) {
    if ( ( X < ttc_gfx_Quick.PhysicalWidth ) && ( Y < ttc_gfx_Quick.PhysicalHeight ) ) {
        if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
            gfx_ili93xx_window_reset();
        }
        gfx_ili93xx_cursor_set( X, Y );
        gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );
        gfx_ili93xx_board_write_data16( ttc_gfx_Quick.Config->ColorFg );
    }
}
void                gfx_ili93xx_prepare() {
#ifdef EXTENSION_basic_cm3
    // Calculate data for parallel 16-bit access to gpio port for super fast write strobe toggling (works on CortexM3 only)
    gfx_ili93xx_WriteStrobe_Port = ( t_u16* ) cm3_calc_peripheral_Word( TTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE );
    t_u8   BitNo = cm3_calc_peripheral_BitNumber( TTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE );
    gfx_ili93xx_WriteStrobe_Mask = 1 << BitNo;
    gfx_ili93xx_WriteStrobe_Port = ( t_u16* ) 0x4001140c; // DEBUG

#endif
}
void                gfx_ili93xx_palette_set( t_u16* Palette, t_u8 Size ) {
    Assert_GFX_Writable( Palette, ttc_assert_origin_auto ); // pointers must not be NULL
    TODO( "Implement gfx_ili93xx_palette_set()" );
}
void                gfx_ili93xx_rect( t_u16 X, t_u16 Y, t_s16 PhysicalWidth, t_s16 PhysicalHeight ) {
    if ( ( PhysicalWidth == 0 ) || ( PhysicalHeight == 0 ) )
    { return; }

    if ( !gfx_ili93xx_WindowUnchanged ) { // reset drawing window
        gfx_ili93xx_window_reset();
    }
    gfx_ili93xx_line_horizontal( X, Y, PhysicalWidth );
    gfx_ili93xx_line_vertical( X, Y, PhysicalHeight );
    gfx_ili93xx_line_vertical( X + PhysicalWidth, Y, PhysicalHeight );
    gfx_ili93xx_line_horizontal( X, Y + PhysicalHeight, PhysicalWidth );
}
e_ttc_gfx_errorcode gfx_ili93xx_reset( t_ttc_gfx_config* Config ) {
    Assert_GFX_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    ttc_gpio_init( TTC_GFX1_ILI93XX_PIN_NOT_RESET,  E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_NOT_RESET );
    ttc_task_msleep( 100 );
    ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_RESET );
    ttc_task_msleep( 100 );

#ifdef TTC_GFX1_ILI93XX_PIN_LCDLED
    ttc_gpio_init( TTC_GFX1_ILI93XX_PIN_LCDLED, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    gfx_ili93xx_backlight( 0 );
#endif

    return ( e_ttc_gfx_errorcode ) 0;
}
void                gfx_ili93xx_rect_fill( t_u16 X, t_u16 Y, t_s16 PhysicalWidth, t_s16 PhysicalHeight ) {
    Assert_GFX( X < ttc_gfx_Quick.Width,  ttc_assert_origin_auto ); // calling function must ensure optimized values. Call ttc_gfx_rect_fill() or optimize your arguments!
    Assert_GFX( Y < ttc_gfx_Quick.Height, ttc_assert_origin_auto ); // calling function must ensure optimized values. Call ttc_gfx_rect_fill() or optimize your arguments!

    if ( PhysicalWidth <= 0 ) {
        if ( PhysicalWidth == 0 )
        { return; }

        PhysicalWidth = -PhysicalWidth;
        if ( PhysicalWidth >= X )
        { X = 0; }
        else
        { X -= PhysicalWidth; }
    }
    if ( PhysicalHeight <= 0 ) {
        if ( PhysicalHeight == 0 )
        { return; }

        PhysicalHeight = -PhysicalHeight;
        if ( PhysicalHeight >= Y )
        { Y = 0; }
        else
        { Y -= PhysicalHeight; }
    }
    gfx_ili93xx_window_set( X, X + PhysicalWidth - 1, Y, Y + PhysicalHeight - 1 );
    gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );
    gfx_ili93xx_cursor_set( X, Y );
    gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );
    _gfx_ili93xx_data_fill16( ttc_gfx_Quick.Config->ColorFg, PhysicalWidth * PhysicalHeight );

    gfx_ili93xx_WindowUnchanged = FALSE;
}
void                gfx_ili93xx_set_font( const t_ttc_font_data* Font ) {
    Assert_GFX_Readable( Font, ttc_assert_origin_auto ); // pointers must not be NULL
    ttc_gfx_Quick.Config->TextColumns  = ttc_gfx_Quick.Config->PhysicalWidth  / Font->CharWidth;
    ttc_gfx_Quick.Config->TextRows     = ttc_gfx_Quick.Config->PhysicalHeight / Font->CharHeight;
}
void                gfx_ili93xx_setPortrait() {
    // LowCS;
    // set GRAM write direction to PORTRAIT MODE
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_EntryMode, 0x1030 ); // 0x30 = 0b00110000;
    // shift dir. to the right (SM=0, SS=1)
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DriverOutputControl, 0x0100 );
    // HighCS;

    gfx_ili93xx_clear();
}
void                gfx_ili93xx_sleep_enter() {
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DisplayControl1, 0x0131 ); // setD1=0,D0=1
    ttc_task_msleep( 10 ); // Wait for 2 frames or more
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DisplayControl1, 0x0130 ); // setD1=0,D0=0
    ttc_task_msleep( 10 ); // Wait for 2 frames or more
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DisplayControl1, 0x0000 ); // displayoff
    // ---------- Power off sequence
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl1, 0x0000 ); // SAP,BT[3:0],AP,DSTB,SLP,STB
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl2, 0x0000 ); // DC1[2:0],DC0[2:0],VC[2:0]
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl3, 0x0000 ); // VREG1OUTvoltage
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl4, 0x0000 ); // VDV[4:0]forVCOMamplitude
    ttc_task_msleep( 200 ); // dis-chargecapacitorpowervoltage
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl1, 0x0002 ); // SAP,BT[3:0],APE,AP,DSTB,SLP
}
void                gfx_ili93xx_sleep_exit() {
    // ---------- Power On sequence
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl1, 0x0000 ); // SAP, BT[3:0], AP, DSTB, SLP, STB
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl2, 0x0007 ); // DC1[2:0], DC0[2:0], VC[2:0]
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl3, 0x0000 ); // VREG1OUT voltage
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl4, 0x0000 ); // VDV[4:0] for VCOM amplitude
    ttc_task_msleep( 200 );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl1, 0x1490 ); // SAP, BT[3:0], AP, DSTB, SLP, STB
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl2, 0x0227 ); // DC1[2:0], DC0[2:0], VC[2:0]
    ttc_task_msleep( 50 );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl3, 0x001C ); // External reference voltage=Vci
    ttc_task_msleep( 50 );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl4, 0x1800 ); // VDV[4:0] for VCOM amplitude
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_PowerControl7, 0x001C ); // VCM[4:0] for VCOMH
    ttc_task_msleep( 50 );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_DisplayControl1, 0x0133 ); //
}
void                gfx_ili93xx_window_reset( ) {

    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_HorizontalRAMStartAddressPosition, 0 );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_HorizontalRAMEndAddressPosition,   239 );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_VerticalRAMStartAddressPosition,   0 );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_VerticalRAMEndAddressPosition,     319 );
    gfx_ili93xx_WindowUnchanged = TRUE;
}
void                gfx_ili93xx_window_set( t_u16 Left, t_u16 Right, t_u16 Top, t_u16 Bottom ) {

    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_HorizontalRAMStartAddressPosition, Left );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_HorizontalRAMEndAddressPosition,   Right );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_VerticalRAMStartAddressPosition,   Top );
    gfx_ili93xx_board_write_register16( E_gfx_ili93xx_register_VerticalRAMEndAddressPosition,     Bottom );
}
// InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

// }Function Definitions
// { Private Functions (ideally) ************************************************

void  _gfx_ili93xx_data_fill16( t_u16 Data, t_u32 Times ) {
    if ( 1 ) { // fast filling by sending data strobe signal only
#ifdef EXTENSION_basic_cm3  // super fast strobing via direct 16-bit port writes (faster than using bitbanding on individual port bits)

        gfx_ili93xx_board_write_data16( Data );
        _gfx_ili93xx_repeat_start();

        // create 16-bit masks to set/clear strobe bit
        gfx_ili93xx_data_Strobe1 = *gfx_ili93xx_WriteStrobe_Port | gfx_ili93xx_WriteStrobe_Mask;
        gfx_ili93xx_data_Strobe0 = *gfx_ili93xx_WriteStrobe_Port & ( 0xffff ^ gfx_ili93xx_WriteStrobe_Mask );

        while ( Times & 15 ) { // repeat previous data given times without writing Data again
            Times--;
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
        }
        while ( Times ) { // repeat previous data given times without writing Data again
            Times -= 16;
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe0; // clr write-mode => data is transfered
            *gfx_ili93xx_WriteStrobe_Port = gfx_ili93xx_data_Strobe1; // set write-mode
        }
        _gfx_ili93xx_repeat_end();

#else   // pin strobing via individual ttc_gpio_clr()/ttc_gpio_set() (slower on some architectures)

        gfx_ili93xx_board_write_data16( Data );
        _gfx_ili93xx_repeat_start();
        while ( Times ) { // repeat previous data given times without writing Data again
            Times--;
            ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE ); // clr write-mode => data is transfered
            ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE ); // set write-mode
        }
        _gfx_ili93xx_repeat_end();
#endif
    }
    else {     // slow filling by sending Data multiple times
        while ( Times-- ) { // repeat previous data given times without writing Data again
            gfx_ili93xx_board_write_data16( Data );
        }
    }
}
void  _gfx_ili93xx_pixel_clr_at( t_u16 X, t_u16 Y ) {
    gfx_ili93xx_cursor_set( X, Y );
    gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );
    gfx_ili93xx_board_write_data16( ttc_gfx_Quick.Config->ColorBg );
}
void  _gfx_ili93xx_pixel_set_at( t_u16 X, t_u16 Y ) {
    gfx_ili93xx_cursor_set( X, Y );
    gfx_ili93xx_board_write_command( E_gfx_ili93xx_register_WriteDatatoGRAM );
    gfx_ili93xx_board_write_data16( ttc_gfx_Quick.Config->ColorFg );
}
void  _gfx_ili93xx_repeat_end() {
    ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT ); // release LCD-Chip-Select

    ttc_gpio_init( TTC_GFX1_ILI93XX_PIN_REGISTER_SELECT,     E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max );
    ttc_gpio_init( TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT,   E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max );
    ttc_gpio_init( TTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max );
    ttc_gpio_init( TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE,  E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max );
}
void  _gfx_ili93xx_repeat_start() {
    ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_REGISTER_SELECT );     // set Numberister-Select => now the driver expects data
    ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE );  // disable read mode

    ttc_gpio_init( TTC_GFX1_ILI93XX_PIN_REGISTER_SELECT,     E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    ttc_gpio_init( TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT,   E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    ttc_gpio_init( TTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    ttc_gpio_init( TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE,  E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );

    ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT );   // pull LCD-Chip-Select to zero -> chip-select is activated
}
// InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

// }Private Functions
