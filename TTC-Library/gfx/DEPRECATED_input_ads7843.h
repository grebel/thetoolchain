#ifndef INPUT_ADS7843
#define INPUT_ADS7843
#define AUTO 0

#include "ttc_basic.h"
#include "ttc_gpio.h"
#include "ttc_task.h"
#include "ttc_gpio_types.h"
//#include "ttc_gfx_types.h"
#include "DEPRECATED_ttc_input_types.h"
//{ externals provided by DEPRECATED_ttc_input.c *************************************

//}externals

//X #include "pin_configuration.h"
//X #include "lcd_color.h"

// ToDo: Papo: use long names for global constants (e.g. ADS7843_TTC_INPUT_CMD_RDY)
#define TTC_INPUT_CMD_RDY 0X90    //Bit Configuration 10010000    ---Read the position of Y
#define TTC_INPUT_CMD_RDX 0XD0    //Bit Configuration 11010000    ---Read the position of X
#define TTC_INPUT_INT  PIN_PC13  //PC13  INT ---Pen interrupt
#define TTC_INPUT_MISO PIN_PA6   //PA6  MISO  ---Serial Data OUT
#define TTC_INPUT_MOSI PIN_PA7 //PA7  MOSI  ---Serial Data IN
#define TTC_INPUT_SCLK PIN_PA5  //PA5  SCLK  ---Clock
#define TTC_INPUT_CS  PIN_PA4  //PA4  CS    ---Chip Select
#define READ_TIMES 8  //shows how often the positions will be quested (average build)
#define LOST_VAL 3
#define ERR_RANGE 50
#define KEY_DOWN 0x01
#define KEY_UP   0x00

void input_ads7843_init() ;

void DEPRECATED_input_ads7843.configuration(void);


/** this functions send one command byte to the driver, every bit of this byte has a special function
  for more details see the driver manual
*/
void  input_ads7843_ADS_Write_Byte(u8_t num);

/** this function read data from the touch-driver, the commands are defined in the header file,
  the function returns a u16_t number for example with the value of the x touch point   */

u16_t input_ads7843_ADS_Read_AD(u8_t CMD);

/** this function read the position-data of the touchdisplay, the values are written in the adresses which are given with the pointer
 , but it reads only one position at the same the */
u16_t input_ads7843_ADS_Read_XY(u8_t xy);


u8_t input_ads7843_Read_TP_Once(void);


//u8_t input_ads7843_read_position2(u16_t x,u16_t y);

/** this function calls the x and y-position with the ADS_Read_XY-function, it also check wheather the values are in the given range
you have the transmit two pointers for the positions
*/
BOOL input_ads7843_read_position(volatile u16_t *x,volatile u16_t *y);

//void Draw_input_ads7843_Point(u8_t x,u16_t y);

void input_ads7843_Pen_Int_Set(u8_t en);

void input_ads7843_Callibration();

void input_ads7843_get_configuration(ttc_input_generic_t* ttc_input_CurrentInput);



/***************************************************************************************************************************************
  input_ads7843_TouchCheckAll
  load the current position of the touchpanel an check wheater the position belongs to a registered touchhandler function
****************************************************************************************************************************************/

void input_ads7843_TouchCheckAll(void* TaskArgument);

bool input_ads7843_Single_TouchCheck(void* TaskArgument);

void following_circle();

#endif // INPUT_ADS7843
