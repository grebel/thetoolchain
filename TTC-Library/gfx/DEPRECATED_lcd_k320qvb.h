/*{ lcd_k320qvb.h *******************************

  Low-level driver for LCD Controller K320QVB as used in ARMSTM32-LCD 
  prototype board form Olimex Corp. 

  written by Gregor Rebel 2012  

}*/

#ifndef K320QVB
#define K320QVB

//{ Includes 1 ***********************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)

//}Includes 1
//{ Includes *************************************************************

#include "ttc_gpio.h"
#include "ttc_task.h"
#include "ttc_interrupt.h"
#include "ttc_memory.h"
#include "ttc_gfx_types.h"

#ifdef EXTENSION_board_olimex_lcd
  #include "lcd-320x240.h"
#endif

//}Includes
//{ externals provided by ttc_gfx.c *************************************

extern t_ttc_gfx_generic* ttc_gfx_CurrentDisplay;
extern t_u16 ttc_gfx_CurrentColorFg;
extern t_u16 ttc_gfx_CurrentColorBg;

//}externals
//{ Defines *************************************************************

//LCD measurements

//? #ifndef LCD_K320QVB_START_X
//? #define LCD_K320QVB_START_X 10 // ToDo: What's this for?
//? #endif
//? #ifndef LCD_K320QVB_START_Y
//? #define LCD_K320QVB_START_Y 10 // ToDo: What's this for?
//? #endif

//? #define BUTTONHEIGHT 18
//? #define BUTTONWIDTH BUTTONHEIGHT*3
//? #define MEDIUMLENGHT 10

#define LCD_CS TTC_GPIO_C8 		          //LCD Chip Select
#define LCD_RS TTC_GPIO_C9                //LCD Reset Command
#define LCD_WR TTC_GPIO_C10               //LCD Write Command Bit
#define LCD_RD TTC_GPIO_C11               //LCD Read Command Bit
#define LCD_DB00 TTC_GPIO_C0              //LCD Data Bit
#define LCD_DB01 TTC_GPIO_C1              //LCD Data Bit
#define LCD_DB02 TTC_GPIO_C2              //LCD Data Bit
#define LCD_DB03 TTC_GPIO_C3              //LCD Data Bit
#define LCD_DB04 TTC_GPIO_C4              //LCD Data Bit
#define LCD_DB05 TTC_GPIO_C5              //LCD Data Bit
#define LCD_DB06 TTC_GPIO_C6              //LCD Data Bit
#define LCD_DB07 TTC_GPIO_C7
#define LCD_DB08 TTC_GPIO_B8              //LCD Data Bit
#define LCD_DB09 TTC_GPIO_B9              //LCD Data Bit
#define LCD_DB10 TTC_GPIO_B10             //LCD Data Bit
#define LCD_DB11 TTC_GPIO_B11             //LCD Data Bit
#define LCD_DB12 TTC_GPIO_B12             //LCD Data Bit
#define LCD_DB13 TTC_GPIO_B13             //LCD Data Bit
#define LCD_DB14 TTC_GPIO_B14             //LCD Data Bit
#define LCD_DB15 TTC_GPIO_B15             //LCD Data Bit
#define LCDBUS_RSLOW_ADDR	((volatile t_u16 *) 0x60000000)
#define LCDBUS_RSHIGH_ADDR	((volatile t_u16 *)(0x60000000 | (1<<(19+1))))

//}Defines
//{ Function prototypes **************************************************

// initialize the LCD , white Background
void k320qvb_init();

// Reads the Reg-ID of the TFT-driver
t_u16 k320qvb_read_register(t_u16 Register);

/** write value into LCD-register
  *
  * @param Register   16-bit address inside K320QVB
  * @param Value      8-bit value to write into register
  */
// void k320qvb_write_register(t_u16 Register, t_u16 Value);
#define k320qvb_write_register(Register, Value) k320qvb_write_command(Register); *LCDBUS_RSHIGH_ADDR = (t_u16) Value

#define k320qvb_write_command(Command)  *LCDBUS_RSLOW_ADDR = (t_u16) Command


void k320qvb_clear();

/** define new color foreground/ background
  */
void k320qvb_set_color_fg8(t_u8 Color);
void k320qvb_set_color_fg24(t_u32 Color);
void k320qvb_set_color_bg24(t_u32 Color);
void k320qvb_set_color_fg16(t_u32 Color);
void k320qvb_set_color_bg16(t_u32 Color);

// draw horizontal/ vertical line
void k320qvb_line_horizontal(t_u16 X, t_u16 Y, t_s16 Length);
void k320qvb_line_vertical(t_u16 X, t_u16 Y, t_s16 Length);

/** sets single pixel at current cursor position in given color */
// void k320qvb_set_pixel(t_u16 Color);
#define k320qvb_set_pixel(Color) *LCDBUS_RSHIGH_ADDR = Color

#ifdef EXTENSION_ttc_font  // font attributes
void k320qvb_put_char(t_u8 Character);
void k320qvb_put_char_solid(t_u8 Character);
#endif

/** sets level of backlight
  *
  * @param Level   Brightness level (0 = off, 255 = max)
  */
void k320qvb_set_backlight(t_u8 Level);

/** move pixel cursor to given location
  *
  * @param X  x-coordinate
  * @param X  y-coordinate
  */
// void k320qvb_set_cursor(t_u16 X,t_u16 Y);
#define k320qvb_set_cursor(X, Y)  k320qvb_write_register(0x20, X); k320qvb_write_register(0x21, Y); k320qvb_write_command(0x22)
#define k320qvb_set_cursor_x(X)   k320qvb_write_register(0x20, X); k320qvb_write_command(0x22)
#define k320qvb_set_cursor_y(Y)   k320qvb_write_register(0x21, Y); k320qvb_write_command(0x22)
//#define k320qvb_set_cursor_x(X)   k320qvb_write_register(0x20, X)
//#define k320qvb_set_cursor_y(Y)   k320qvb_write_register(0x21, Y)

void k320qvb_set_display_window(t_u8 Xpos, ut_int16 Ypos, t_u8 Height, ut_int16 Width);

void k320qvb_rect_fill(t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height);

void k320qvb_get_configuration();

//}Function prototypes
//{ Macro support for ttc_gfx.h

//#define _ttc_gfx_driver_set_pixel_at(X,Y)  k320qvb_set_cursor(X, Y); k320qvb_set_pixel(ttc_gfx_CurrentColorFg)
//#define _ttc_gfx_driver_clr_pixel_at(X,Y)  k320qvb_set_cursor(X, Y); k320qvb_set_pixel(ttc_gfx_CurrentColorBg)
//#define _ttc_gfx_driver_set_cursor(X,Y)    k320qvb_set_cursor(X, Y)

//}Macros

#endif // K320QVB
