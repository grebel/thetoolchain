#ifndef GFX_IMAGE_H
#define GFX_IMAGE_H
/*{ gfx_image.h ************************************************

  Image manipulation

}*/

//{ Includes *************************************************************
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
//} Includes

//{ Defines/ TypeDefs ****************************************************

//} Defines

//{ Structures/ Enums ****************************************************

typedef enum enumcolorDepth { Monochrom = 1, Color8Bit = 8, RealColor = 16, TrueColor = 24} ColorDepth;

typedef struct struct_image {
    t_u16 Width;
    t_u16 Height;
    ColorDepth Depth;
    unsigned char* Data;
} gfxImage;

//} Structures/ Enums

//{ Global Variables *****************************************************
//} Global Variables

//{ Function prototypes **************************************************

/** delivers single pixel from monochrome image
  *
  * t_u32 gfx_image_get_pixel_24bit(t_u16 X, t_u16 Y, gfxImage *Image)
  *
  * @return  24-bit color value of single pixel
  */
#define gfx_image_get_pixel_24bit(X, Y, Image)  ( (t_u32) Image->Data[3*(Y*(Image->Width)+X)] << 16 ) + ( (t_u32) Image->Data[3*(Y*(Image->Width)+X)+1] << 8 ) + ( (t_u32) Image->Data[3*(Y*(Image->Width)+X)+2] )

/** delivers single pixel from monochrome image
  *
  * t_u16 gfx_image_get_pixel_16bit(t_u16 X, t_u16 Y, gfxImage *Image)
  *
  * @return  16-bit color value of single pixel
  */
#define gfx_image_get_pixel_16bit(X, Y, Image)  (t_u16) Image->Data[( Y*Image->Width+X) * sizeof(t_u16)]  // ToDo: correct?

/** delivers single pixel from monochrome image
  *
  * t_u8 gfx_image_get_pixel_8bit(t_u16 X, t_u16 Y, gfxImage *Image)
  *
  * @return  8-bit color value of single pixel
  */
#define gfx_image_get_pixel_8bit(X, Y, Image)  (t_u8) Image->Data[ (Y*Image->Width+X) * sizeof(t_u8)]  // ToDo: correct?

/** delivers single pixel from monochrome image
  *
  * t_u8 gfx_image_get_pixel_1bit(t_u16 X, t_u16 Y, gfxImage *Image)
  *
  * @return  == 0: pixel not set; != 0: pixel is set
  */
#define gfx_image_get_pixel_1bit(X, Y, Image)  (t_u8) Image->Data[(Y*Image->Width/8 + X/8)] & (X & 7) // ToDo: correct?

//} Function prototypes

#endif