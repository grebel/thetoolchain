#include "DEPRECATED_input_analog.h"

BOOL input_analog_read_position(volatile u16_t *adc_x,volatile u16_t *adc_y)
{
    INPUT_STATIC u16_t val1[READ_TIMES], val2[READ_TIMES];
    INPUT_STATIC u32_t temp1,temp2;
    INPUT_STATIC u8_t i=0;
    /*
     * YD=0 YU=1, measure XL and XR
     * YD=1 YU=0, measure XL and XR
     * The average of the previous four samples is the X value.
     *
     * XL=0 XR=1, measure YD and YU
     * XL=1 XR=0, measure YD and YU
     * The average of the previous four samples is the Y value.
     *
     * A total of 8 ADC measurements are needed!
     */
    ttc_task_msleep(1);

    ttc_gpio_init(TTC_INPUT_DIGITAL_YD, tgm_analog_in, tgs_Min);

    if(!ttc_gpio_get(TTC_INPUT_DIGITAL_YU)){
        temp1=0;
        temp2=0;
        ttc_task_msleep(50);
        return FALSE;
    }

    else
    {
        ttc_gpio_init(TTC_INPUT_DIGITAL_XR, tgm_output_push_pull, tgs_Min);
        ttc_gpio_init(TTC_INPUT_DIGITAL_XL, tgm_output_push_pull, tgs_Min);

        ttc_gpio_init(TTC_INPUT_DIGITAL_YD, tgm_analog_in, tgs_Min);
        ttc_gpio_init(TTC_INPUT_DIGITAL_YU, tgm_analog_in, tgs_Min);


        for (i=0;i<READ_TIMES;i++)
        {
            ttc_gpio_clr(TTC_INPUT_DIGITAL_XL);
            ttc_gpio_set(TTC_INPUT_DIGITAL_XR);
            uSleep(50);
            val2[i] = ttc_adc_get_adc_value(TTC_INPUT_ANALOG_YD,TTC_INPUT_ANALOG_ADC) + ttc_adc_get_adc_value(TTC_INPUT_ANALOG_YU,TTC_INPUT_ANALOG_ADC);
        }
        for (i=0;i<READ_TIMES;i++)
        {
            ttc_gpio_set(TTC_INPUT_DIGITAL_XL);
            ttc_gpio_clr(TTC_INPUT_DIGITAL_XR);
            uSleep(50);
            val1[i] = ttc_adc_get_adc_value(TTC_INPUT_ANALOG_YD,TTC_INPUT_ANALOG_ADC) + ttc_adc_get_adc_value(TTC_INPUT_ANALOG_YU,TTC_INPUT_ANALOG_ADC);
        }

        temp1=0;
        temp2=0;

        for (i=0;i<READ_TIMES;i++)
        {
            temp1+=((1<<12)-val1[i]);
            temp2+=val2[i];
        }


        *adc_x = (temp2+temp1)/(4*READ_TIMES);

        ttc_gpio_init(TTC_INPUT_DIGITAL_YD, tgm_output_push_pull, tgs_Min);
        ttc_gpio_init(TTC_INPUT_DIGITAL_YU, tgm_output_push_pull, tgs_Min);

        ttc_gpio_init(TTC_INPUT_DIGITAL_XL, tgm_analog_in, tgs_Min);
        ttc_gpio_init(TTC_INPUT_DIGITAL_XR, tgm_analog_in, tgs_Min); // Port-Einstellung

        for (i=0;i<READ_TIMES;i++)
        {
            ttc_gpio_clr(TTC_INPUT_DIGITAL_YD);
            ttc_gpio_set(TTC_INPUT_DIGITAL_YU);
            uSleep(50);
            val1[i] = ttc_adc_get_adc_value(TTC_INPUT_ANALOG_XL,TTC_INPUT_ANALOG_ADC) + ttc_adc_get_adc_value(TTC_INPUT_ANALOG_XR,TTC_INPUT_ANALOG_ADC);
        }
        for (i=0;i<READ_TIMES;i++)
        {
            ttc_gpio_set(TTC_INPUT_DIGITAL_YD);
            ttc_gpio_clr(TTC_INPUT_DIGITAL_YU);
            uSleep(50);
            val2[i] = ttc_adc_get_adc_value(TTC_INPUT_ANALOG_XL,TTC_INPUT_ANALOG_ADC) + ttc_adc_get_adc_value(TTC_INPUT_ANALOG_XR,TTC_INPUT_ANALOG_ADC);
        }

        temp1=0;
        temp2=0;

        for (i=0;i<READ_TIMES;i++)
        {
            temp1+=(1<<12)-val1[i];
            temp2+=val2[i];

        }

        *adc_y = (temp2+temp1)/(4*READ_TIMES);

        input_analog_prepare_wait();
        if (INPUT_ANALOG_SMART_COMPILE){
        *adc_x=((*adc_x *15/100) -*adc_y/1000-35);
        *adc_y=((*adc_y *22/100) -*adc_x/1000-45);
        }
        else
        {
            *adc_x=((*adc_x *15/100) -*adc_y/1000-35);
            *adc_y=((*adc_y *20/100) -*adc_x/1000-45);
        }
    }
    if( (0<*adc_x) && (*adc_x<240) && (0<*adc_y) && (*adc_y<340) )
        return TRUE;
    else
        return FALSE;


}
void input_analog_configuration(ttc_input_generic_t *ttc_input_CurrentInput)
{
    ttc_gpio_init(TTC_INPUT_DIGITAL_XL, tgm_output_push_pull, tgs_Max);
    ttc_gpio_init(TTC_INPUT_DIGITAL_XR, tgm_output_push_pull, tgs_Max);
    ttc_gpio_set(TTC_INPUT_DIGITAL_XL);
    ttc_gpio_set(TTC_INPUT_DIGITAL_XR);
    ttc_gpio_init(TTC_INPUT_DIGITAL_YD, tgm_input_floating, tgs_Min);
    ttc_gpio_init(TTC_INPUT_DIGITAL_YU, tgm_input_floating, tgs_Min);


}
void input_analog_init(ttc_input_generic_t* ttc_input_CurrentInput) {

    ttc_input_CurrentInput=ttc_input_CurrentInput;
    ttc_adc_init_single_mode(ADC1);
    ttc_gpio_init(PIN_PC0, tgm_output_push_pull, tgs_Max);
    ttc_gpio_init(PIN_PC1, tgm_output_push_pull, tgs_Max);
    ttc_gpio_init(PIN_PC2, tgm_output_push_pull, tgs_Max);
    ttc_gpio_init(PIN_PC3, tgm_output_push_pull, tgs_Max);
    input_analog_prepare_wait();
    for (volatile int i=0;i<100;i++);
}
void input_analog_get_configuration(ttc_input_generic_t* ttc_input_CurrentInput){

    if (! ttc_input_CurrentInput->Width)  ttc_input_CurrentInput->Width  = 320;
    if (! ttc_input_CurrentInput->Height) ttc_input_CurrentInput->Height = 240;
    if (! ttc_input_CurrentInput->Depth)  ttc_input_CurrentInput->Depth  = 24;

    ttc_input_CurrentInput->function_read_position =input_analog_read_position;
}
void input_analog_prepare_wait(){
    ttc_gpio_init(TTC_INPUT_DIGITAL_XL, tgm_output_push_pull, tgs_Min);
    ttc_gpio_init(TTC_INPUT_DIGITAL_XR, tgm_output_push_pull, tgs_Min);
    ttc_gpio_init(TTC_INPUT_DIGITAL_YD, tgm_output_push_pull, tgs_Min);
    ttc_gpio_init(TTC_INPUT_DIGITAL_YU, tgm_input_floating, tgs_Min);
    ttc_gpio_set(TTC_INPUT_DIGITAL_XL);
    ttc_gpio_set(TTC_INPUT_DIGITAL_XR);
    ttc_gpio_clr(TTC_INPUT_DIGITAL_YD);
}
void my_qsort(u16_t *links, u16_t *rechts) { //ToDo PaPo: Function still used?
    u16_t *ptr1 = links;
    u16_t *ptr2 = rechts;
    u16_t w, x;
    /* x bekommt die Anfangsadresse der
    * Mitte von links und rechts.
    * Anstatt der Bitverschiebung hätten Sie
    * auch einfach »geteilt durch 2« rechnen können.
    */
    x = *(links + ((rechts - links) >> 1));
    do {
        while(*ptr1 < x) ptr1++;
        while(*ptr2 > x) ptr2--;
        if(ptr1 > ptr2)
            break;
        w = *ptr1;
        *ptr1 = *ptr2;
        *ptr2 = w;
    } while(++ptr1 <= --ptr2);
    if(links < ptr2)  my_qsort(links, ptr2);
    if(ptr1 < rechts) my_qsort(ptr1, rechts);
}
