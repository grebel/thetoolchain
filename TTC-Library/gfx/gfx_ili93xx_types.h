#ifndef GFX_ILI93xx_TYPES_H
#define GFX_ILI93xx_TYPES_H

/** { gfx_ili93xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for GFX devices on ili93xx architectures.
 *  Structures, Enums and Defines being required by ttc_gfx_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140313 10:58:25 UTC
 *
 *  Note: See ttc_gfx.h for description of architecture independent GFX implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_GFX1   // device not defined in makefile
    #define TTC_GFX1    ta_gfx_ili93xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_gfx_types.h *************************

typedef enum { // e_gfx_ili93xx_register - ili93xx register adresses
    E_gfx_ili93xx_register_DriverCodeRead                    = 0x00,
    E_gfx_ili93xx_register_DriverOutputControl               = 0x01,
    E_gfx_ili93xx_register_LCDDrivingWaveControl             = 0x02,
    E_gfx_ili93xx_register_EntryMode                         = 0x03,
    E_gfx_ili93xx_register_ResizingControlRegister           = 0x04,
    E_gfx_ili93xx_register_DisplayControl1                   = 0x07,
    E_gfx_ili93xx_register_DisplayControl2                   = 0x08,
    E_gfx_ili93xx_register_DisplayControl3                   = 0x09,
    E_gfx_ili93xx_register_DisplayControl4                   = 0x0A,
    E_gfx_ili93xx_register_RGBDisplayInterfaceControl1       = 0x0C,
    E_gfx_ili93xx_register_FrameMarkerPosition               = 0x0D,
    E_gfx_ili93xx_register_RGBDisplayInterfaceControl2       = 0x0F,
    E_gfx_ili93xx_register_PowerControl1                     = 0x10,
    E_gfx_ili93xx_register_PowerControl2                     = 0x11,
    E_gfx_ili93xx_register_PowerControl3                     = 0x12,
    E_gfx_ili93xx_register_PowerControl4                     = 0x13,
    E_gfx_ili93xx_register_GRAMHorizontalAddressSet          = 0x20,
    E_gfx_ili93xx_register_GRAMVerticalAddressSet            = 0x21,
    E_gfx_ili93xx_register_WriteDatatoGRAM                   = 0x22,
    E_gfx_ili93xx_register_ReadDatatoGRAM                    = 0x22,
    E_gfx_ili93xx_register_PowerControl7                     = 0x29,
    E_gfx_ili93xx_register_FrameRateandColorControl          = 0x2B,
    E_gfx_ili93xx_register_GammaControl1                     = 0x30,
    E_gfx_ili93xx_register_GammaControl2                     = 0x31,
    E_gfx_ili93xx_register_GammaControl3                     = 0x32,
    E_gfx_ili93xx_register_GammaControl4                     = 0x35,
    E_gfx_ili93xx_register_GammaControl5                     = 0x36,
    E_gfx_ili93xx_register_GammaControl6                     = 0x37,
    E_gfx_ili93xx_register_GammaControl7                     = 0x38,
    E_gfx_ili93xx_register_GammaControl8                     = 0x39,
    E_gfx_ili93xx_register_GammaControl9                     = 0x3C,
    E_gfx_ili93xx_register_GammaControl10                    = 0x3D,
    E_gfx_ili93xx_register_HorizontalRAMStartAddressPosition = 0x50,
    E_gfx_ili93xx_register_HorizontalRAMEndAddressPosition   = 0x51,
    E_gfx_ili93xx_register_VerticalRAMStartAddressPosition   = 0x52,
    E_gfx_ili93xx_register_VerticalRAMEndAddressPosition     = 0x53,
    E_gfx_ili93xx_register_DriverOutputControl2              = 0x60,
    E_gfx_ili93xx_register_BaseImageDisplayControl           = 0x61,
    E_gfx_ili93xx_register_VerticalScrollControl             = 0x6A,
    E_gfx_ili93xx_register_PartialImage1DisplayPosition      = 0x80,
    E_gfx_ili93xx_register_PartialImage1RAMStartAddress      = 0x81,
    E_gfx_ili93xx_register_PartialImage1RAMEndAddress        = 0x82,
    E_gfx_ili93xx_register_PartialImage2DisplayPosition      = 0x83,
    E_gfx_ili93xx_register_PartialImage2RAMStartAddress      = 0x84,
    E_gfx_ili93xx_register_PartialImage2RAMEndAddress        = 0x85,
    E_gfx_ili93xx_register_PanelInterfaceControl1            = 0x90,
    E_gfx_ili93xx_register_PanelInterfaceControl2            = 0x92,
    E_gfx_ili93xx_register_PanelInterfaceControl3            = 0x93,
    E_gfx_ili93xx_register_PanelInterfaceControl4            = 0x95,
    E_gfx_ili93xx_register_PanelInterfaceControl5            = 0x97,
    E_gfx_ili93xx_register_PanelInterfaceControl6            = 0x98,
    E_gfx_ili93xx_register_OTPVCMProgrammingControl          = 0xA1,
    E_gfx_ili93xx_register_OTPVCMStatusandEnable             = 0xA2,
    E_gfx_ili93xx_register_OTPProgrammingIDKey               = 0xA5,
    E_gfx_ili93xx_register_Unknown1                          = 0xE5,
    E_gfx_ili93xx_register_unknown
} e_gfx_ili93xx_register;

typedef struct { // register description (adapt according to ili93xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_gfx_register;

typedef struct {  // ili93xx specific configuration data of single GFX device
    t_gfx_register* BaseRegister;       // base address of GFX device registers
    t_u16           IdCode;             // ID_CODE of connected ili93xx LCD driver
} __attribute__( ( __packed__ ) ) t_gfx_ili93xx_config;

// t_ttc_gfx_architecture is required by ttc_gfx_types.h
#define t_ttc_gfx_architecture t_gfx_ili93xx_config

#ifndef TTC_GFX1_ILI93XX_PIN_D00
    #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D00. Define gpio connected ti ILI93xx pin D00 as one from ttc_gpio_pine_e!
#endif
#ifndef TTC_GFX1_ILI93XX_PIN_D01
    #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D01. Define gpio connected ti ILI93xx pin D01 as one from ttc_gpio_pine_e!
#endif
#ifndef TTC_GFX1_ILI93XX_PIN_D02
    #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D02. Define gpio connected ti ILI93xx pin D02 as one from ttc_gpio_pine_e!
#endif
#ifndef TTC_GFX1_ILI93XX_PIN_D03
    #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D03. Define gpio connected ti ILI93xx pin D03 as one from ttc_gpio_pine_e!
#endif
#ifndef TTC_GFX1_ILI93XX_PIN_D04
    #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D04. Define gpio connected ti ILI93xx pin D04 as one from ttc_gpio_pine_e!
#endif
#ifndef TTC_GFX1_ILI93XX_PIN_D05
    #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D05. Define gpio connected ti ILI93xx pin D05 as one from ttc_gpio_pine_e!
#endif
#ifndef TTC_GFX1_ILI93XX_PIN_D06
    #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D06. Define gpio connected ti ILI93xx pin D06 as one from ttc_gpio_pine_e!
#endif
#ifndef TTC_GFX1_ILI93XX_PIN_D07
    #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D07. Define gpio connected ti ILI93xx pin D07 as one from ttc_gpio_pine_e!
#endif
#ifdef TTC_GFX1_ILI93XX_PIN_D08 // 16 bit wide bus
    #ifndef TTC_GFX1_ILI93XX_PIN_D09
        #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D09. Define gpio connected ti ILI93xx pin D09 as one from ttc_gpio_pine_e!
    #endif
    #ifndef TTC_GFX1_ILI93XX_PIN_D10
        #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D10. Define gpio connected ti ILI93xx pin D10 as one from ttc_gpio_pine_e!
    #endif
    #ifndef TTC_GFX1_ILI93XX_PIN_D11
        #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D11. Define gpio connected ti ILI93xx pin D11 as one from ttc_gpio_pine_e!
    #endif
    #ifndef TTC_GFX1_ILI93XX_PIN_D12
        #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D12. Define gpio connected ti ILI93xx pin D12 as one from ttc_gpio_pine_e!
    #endif
    #ifndef TTC_GFX1_ILI93XX_PIN_D13
        #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D13. Define gpio connected ti ILI93xx pin D13 as one from ttc_gpio_pine_e!
    #endif
    #ifndef TTC_GFX1_ILI93XX_PIN_D14
        #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D14. Define gpio connected ti ILI93xx pin D14 as one from ttc_gpio_pine_e!
    #endif
    #ifndef TTC_GFX1_ILI93XX_PIN_D15
        #  error Missing pin definition TTC_GFX1_ILI93XX_PIN_D15. Define gpio connected ti ILI93xx pin D15 as one from ttc_gpio_pine_e!
    #endif
#endif

//} Structures/ Enums


#endif //GFX_ILI93xx_TYPES_H
