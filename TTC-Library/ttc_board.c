/** { ttc_board.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for board devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Most functions of this driver are defined as macros and being implemented
 *  in interfaces/ttc_board_interface.c or in low-level drivers board/board_*.c.
 *
 *  See corresponding ttc_board.h for documentation of all high-level functions.
 *
 *  Created from template ttc_device.c revision 46 at 20180420 13:26:22 UTC
 *
 *  Authors: GREGOR REBEL
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_board.h".
//
#include "ttc_board.h"
#include "ttc_heap.h"       // dynamic memory allocation
#include "ttc_task.h"       // disable/ enable multitasking scheduler
#ifdef EXTENSION_ttc_interrupt
    #include "ttc_interrupt.h"  // globally disable/ enable interrupts
#endif
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_BOARD_AMOUNT == 0
    #warning No BOARD devices defined, cdid you forget to activate something? - Define at least TTC_BOARD1 as one from e_ttc_board_architecture in your makefile!
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of board devices.
 *
 */


// for each initialized device, a pointer to its generic and architecture specific definitions is stored
A_define( t_ttc_board_config*, ttc_board_configs, TTC_BOARD_AMOUNT );

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ********************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_board(t_ttc_board_config* Config)

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * @param LogicalIndex    1.. logical index of BOARD device. Each logical device <n> is defined via TTC_BOARD<n>* constants in compile_options.h and extensions.active/makefile
 */
void _ttc_board_configuration_check( t_u8 LogicalIndex );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

t_ttc_board_config*      ttc_board_create() {

    t_u8 LogicalIndex = ttc_board_get_max_index();
    t_ttc_board_config* Config = ttc_board_get_configuration( LogicalIndex );

    return Config;
}
t_u8                     ttc_board_get_max_index() {
    return TTC_BOARD_AMOUNT;
}
t_ttc_board_config*      ttc_board_get_configuration( t_u8 LogicalIndex ) {
    Assert_BOARD( LogicalIndex > 0, ttc_assert_origin_auto ); // logical index starts at 1
    Assert_BOARD( LogicalIndex <= TTC_BOARD_AMOUNT, ttc_assert_origin_auto ); // invalid index given (did you configure engnough ttc_board devices in your makefile?)
    t_ttc_board_config* Config = A( ttc_board_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_disable();                      // ensure that we are not interrupted while creating configuration
#endif

        Config = A( ttc_board_configs, LogicalIndex - 1 ); // have to reload with disabled interrupts to avoid racing conditions
        if ( !Config ) {
            Config = A( ttc_board_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_board_config ) );
            Config->Flags.Initialized = 0;             // make sure that deinit() is not called during load_defaults()
            ttc_board_load_defaults( LogicalIndex );
        }

#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_enable();
#endif
    }

    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // memory corrupted?
    return Config;
}
void                     ttc_board_deinit( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration

    t_ttc_board_config* Config = ttc_board_get_configuration( LogicalIndex );

    if ( Config->Flags.Initialized ) {
        e_ttc_board_errorcode Result = _driver_board_deinit( Config );
        if ( Result == E_ttc_board_errorcode_OK )
        { Config->Flags.Initialized = 0; }
    }
    ttc_task_critical_end(); // other tasks now may access this driver
}
e_ttc_board_errorcode    ttc_board_init( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration
#if (TTC_ASSERT_BOARD_EXTRA == 1)
    t_u8 InitialCriticalLevel = ttc_interrupt_critical_level();
#endif
    t_ttc_board_config* Config = ttc_board_get_configuration( LogicalIndex );

    // check configuration to meet all limits of current architecture
    _ttc_board_configuration_check( LogicalIndex );

    Config->LastError = _driver_board_init( Config );
    if ( ! Config->LastError ) // == 0
    { Config->Flags.Initialized = 1; }

    Assert_BOARD( Config->Architecture != 0, ttc_assert_origin_auto ); // Low-Level driver must set this value!
    Assert_BOARD_EXTRA( InitialCriticalLevel == ttc_interrupt_critical_level(), ttc_assert_origin_auto ); // amount of  ttc_task_critical_end() calls does not match. Check driver implementation!
    ttc_task_critical_end(); // other tasks now may access this driver

    return Config->LastError;
}
e_ttc_board_errorcode    ttc_board_load_defaults( t_u8 LogicalIndex ) {
    t_ttc_board_config* Config = ttc_board_get_configuration( LogicalIndex );

    u_ttc_board_architecture* LowLevelConfig = NULL;
    if ( Config->Flags.Initialized ) {
        ttc_board_deinit( LogicalIndex );
        LowLevelConfig = Config->LowLevelConfig; // rescue pointer to dynamic allocated memory
    }

    ttc_memory_set( Config, 0, sizeof( t_ttc_board_config ) );

    // restore pointer to dynamic allocated memory
    Config->LowLevelConfig = LowLevelConfig;

    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    switch ( LogicalIndex ) { // physical index of board device from static configuration
#ifdef TTC_BOARD1
        case  1: Config->PhysicalIndex = TTC_BOARD1; break;
#endif
#ifdef TTC_BOARD2
        case  2: Config->PhysicalIndex = TTC_BOARD2; break;
#endif
#ifdef TTC_BOARD3
        case  3: Config->PhysicalIndex = TTC_BOARD3; break;
#endif
#ifdef TTC_BOARD4
        case  4: Config->PhysicalIndex = TTC_BOARD4; break;
#endif
#ifdef TTC_BOARD5
        case  5: Config->PhysicalIndex = TTC_BOARD5; break;
#endif
#ifdef TTC_BOARD6
        case  6: Config->PhysicalIndex = TTC_BOARD6; break;
#endif
#ifdef TTC_BOARD7
        case  7: Config->PhysicalIndex = TTC_BOARD7; break;
#endif
#ifdef TTC_BOARD8
        case  8: Config->PhysicalIndex = TTC_BOARD8; break;
#endif
#ifdef TTC_BOARD9
        case  9: Config->PhysicalIndex = TTC_BOARD9; break;
#endif
#ifdef TTC_BOARD10
        case 10: Config->PhysicalIndex = TTC_BOARD10; break;
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }

    //Insert additional generic default values here ...

    // Let low-level driver initialize remaining fields
    Config->LastError = _driver_board_load_defaults( Config );

    // Check mandatory configuration items
    Assert_BOARD_EXTRA( ( Config->Architecture > E_ttc_board_architecture_none ) && ( Config->Architecture < E_ttc_board_architecture_unknown ), ttc_assert_origin_auto ); // architecture not set properly! Low-Level driver must set this field!

    return Config->LastError;
}
void                     ttc_board_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)

    A_reset( ttc_board_configs, TTC_BOARD_AMOUNT ); // safe arrays are placed in data section and must be initialized manually

    if ( 1 ) // optional: register this device for a sysclock change update
    { ttc_sysclock_register_for_update( ttc_board_sysclock_changed ); }

    _driver_board_prepare();
}
void                     ttc_board_reset( t_u8 LogicalIndex ) {
    t_ttc_board_config* Config = ttc_board_get_configuration( LogicalIndex );

    _driver_board_reset( Config );
}
void                     ttc_board_sysclock_changed() {

    // deinit + reinit all initialized BOARD devices
    for ( t_u8 LogicalIndex = 1; LogicalIndex < ttc_board_get_max_index(); LogicalIndex++ ) {
        t_ttc_board_config* Config = ttc_board_get_configuration( LogicalIndex );
        if ( Config->Flags.Initialized ) {
            ttc_board_deinit( LogicalIndex );
            ttc_board_init( LogicalIndex );
        }
    }
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_board(t_u8 LogicalIndex) {  }

void _ttc_board_configuration_check( t_u8 LogicalIndex ) {
    t_ttc_board_config* Config = ttc_board_get_configuration( LogicalIndex );

    // perform some generic plausibility checks
    const t_ttc_board_features* Features = Features;

    /** Example plausibility check

    if (Config->BitRate > Features->MaxBitRate)   Config->BitRate = Features->MaxBitRate;
    if (Config->BitRate < Features->MinBitRate)   Config->BitRate = Features->MinBitRate;
    */
    // add more architecture independent checks...
    Assert_BOARD( LogicalIndex == Config->LogicalIndex, ttc_assert_origin_auto ); // configuration must store corresponding logical index!

    // let low-level driver check this configuration too
    _driver_board_configuration_check( Config );
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
