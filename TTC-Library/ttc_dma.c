/** { ttc_dma.c ****************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for dma devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported 
 *  architectures.
 *
 *  See corresponding header file for documentation.
 *
 *  Created from template ttc_device.c revision 27 at 20150108 10:24:10 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_dma.h".
//
#include "ttc_dma.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_DMA_AMOUNT == 0
  #warning No DMA devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of dma devices.
 *
 */
 

// for each initialized device, a pointer to its generic and stm32l1xx definitions is stored
A_define(t_ttc_dma_config*, ttc_dma_configs, TTC_DMA_AMOUNT*TTC_NUMBER_OF_CHANNELS);

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8 ttc_dma_get_max_index() {
    return TTC_DMA_AMOUNT;
}
t_ttc_dma_config* ttc_dma_get_configuration(t_u8 LogicalIndex) {
    Assert_DMA(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_dma_config* Config = A(ttc_dma_configs, LogicalIndex-1); // will assert if outside array bounds

    if (!Config) {
        Config = A(ttc_dma_configs, LogicalIndex-1) = ttc_heap_alloc_zeroed(sizeof(t_ttc_dma_config));
        Config->Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_dma_load_defaults(LogicalIndex);
    }

    return Config;
}
void ttc_dma_deinit(t_u8 LogicalIndex) {
    Assert_DMA(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_dma_config* Config = ttc_dma_get_configuration(LogicalIndex);
  
    e_ttc_dma_errorcode Result = _driver_dma_deinit(Config);
    if (Result == ec_dma_OK)
      Config->Flags.Bits.Initialized = 0;
}
e_ttc_dma_errorcode ttc_dma_init(t_u8 LogicalIndex) {
    Assert_DMA(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_dma_config* Config = ttc_dma_get_configuration(LogicalIndex);

    e_ttc_dma_errorcode Result = _driver_dma_init(Config);
    if (Result == ec_dma_OK)
      Config->Flags.Bits.Initialized = 1;
    
    return Result;
}
e_ttc_dma_errorcode ttc_dma_load_defaults(t_u8 LogicalIndex) {
    Assert_DMA(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_dma_config* Config = ttc_dma_get_configuration(LogicalIndex);

    if (Config->Flags.Bits.Initialized)
      ttc_dma_deinit(LogicalIndex);
    
    ttc_memory_set( Config, 0, sizeof(t_ttc_dma_config) );

      Config->LogicalIndex = LogicalIndex;
    Config->PhysicalIndex = ttc_dma_logical_2_physical_index(LogicalIndex);
    
    //Insert additional generic default values here ...

    return _driver_dma_load_defaults(Config);    
}
t_u8 ttc_dma_logical_2_physical_index(t_u8 LogicalIndex) {
  
  switch (LogicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_DMA1
           case 1: return TTC_DMA1;
#endif
#ifdef TTC_DMA2
           case 2: return TTC_DMA2;
#endif
#ifdef TTC_DMA3
           case 3: return TTC_DMA3;
#endif
#ifdef TTC_DMA4
           case 4: return TTC_DMA4;
#endif
#ifdef TTC_DMA5
           case 5: return TTC_DMA5;
#endif
#ifdef TTC_DMA6
           case 6: return TTC_DMA6;
#endif
#ifdef TTC_DMA7
           case 7: return TTC_DMA7;
#endif
#ifdef TTC_DMA8
           case 8: return TTC_DMA8;
#endif
#ifdef TTC_DMA9
           case 9: return TTC_DMA9;
#endif
#ifdef TTC_DMA10
           case 10: return TTC_DMA10;
#endif
      // extend as required
      
      default: break;
    }
    
  Assert_DMA(0, ttc_assert_origin_auto); // No TTC_DMAn defined! Check your makefile.100_board_* file!
  return -1; // program flow should never go here
}
t_u8 ttc_dma_physical_2_logical_index(t_u8 PhysicalIndex) {
  
  switch (PhysicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_DMA1
           case TTC_DMA1: return 1;
#endif
#ifdef TTC_DMA2
           case TTC_DMA2: return 2;
#endif
#ifdef TTC_DMA3
           case TTC_DMA3: return 3;
#endif
#ifdef TTC_DMA4
           case TTC_DMA4: return 4;
#endif
#ifdef TTC_DMA5
           case TTC_DMA5: return 5;
#endif
#ifdef TTC_DMA6
           case TTC_DMA6: return 6;
#endif
#ifdef TTC_DMA7
           case TTC_DMA7: return 7;
#endif
#ifdef TTC_DMA8
           case TTC_DMA8: return 8;
#endif
#ifdef TTC_DMA9
           case TTC_DMA9: return 9;
#endif
#ifdef TTC_DMA10
           case TTC_DMA10: return 10;
#endif
      // extend as required
      
      default: break;
    }
    
  Assert_DMA(0, ttc_assert_origin_auto); // No TTC_DMAn defined! Check your makefile.100_board_* file!
  return -1; // program flow should never go here
}
void ttc_dma_prepare() {
  // add your startup code here (Singletasking!)
  _driver_dma_prepare();
}
void ttc_dma_reset(t_u8 LogicalIndex) {
    Assert_DMA(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_dma_config* Config = ttc_dma_get_configuration(LogicalIndex);

    _driver_dma_reset(Config);    
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_dma(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
