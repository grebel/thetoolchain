/** { ttc_network_layer_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for NETWORK_LAYER device.
 *  Structures, Enums and Defines being required by both, high- and low-level network_layer.
 *
 *  Created from template ttc_device_types.h revision 22 at 20140224 16:21:32 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_NETWORK_LAYER_TYPES_H
#define TTC_NETWORK_LAYER_TYPES_H

//{ Includes *************************************************************

#include "ttc_basic_types.h"
#ifdef EXTENSION_network_layer_usart
#  include "network_layer/network_layer_usart_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

// TTC_NETWORK_LAYERn has to be defined as constant by makefile.100_board_*
#ifdef TTC_NETWORK_LAYER5
#ifndef TTC_NETWORK_LAYER4
#error TTC_NETWORK_LAYER5 is defined, but not TTC_NETWORK_LAYER4 - all lower TTC_NETWORK_LAYERn must be defined!
#endif
#ifndef TTC_NETWORK_LAYER3
#error TTC_NETWORK_LAYER5 is defined, but not TTC_NETWORK_LAYER3 - all lower TTC_NETWORK_LAYERn must be defined!
#endif
#ifndef TTC_NETWORK_LAYER2
#error TTC_NETWORK_LAYER5 is defined, but not TTC_NETWORK_LAYER2 - all lower TTC_NETWORK_LAYERn must be defined!
#endif
#ifndef TTC_NETWORK_LAYER1
#error TTC_NETWORK_LAYER5 is defined, but not TTC_NETWORK_LAYER1 - all lower TTC_NETWORK_LAYERn must be defined!
#endif

#define TTC_NETWORK_LAYER_AMOUNT 5
#else
#ifdef TTC_NETWORK_LAYER4
#define TTC_NETWORK_LAYER_AMOUNT 4

#ifndef TTC_NETWORK_LAYER3
#error TTC_NETWORK_LAYER5 is defined, but not TTC_NETWORK_LAYER3 - all lower TTC_NETWORK_LAYERn must be defined!
#endif
#ifndef TTC_NETWORK_LAYER2
#error TTC_NETWORK_LAYER5 is defined, but not TTC_NETWORK_LAYER2 - all lower TTC_NETWORK_LAYERn must be defined!
#endif
#ifndef TTC_NETWORK_LAYER1
#error TTC_NETWORK_LAYER5 is defined, but not TTC_NETWORK_LAYER1 - all lower TTC_NETWORK_LAYERn must be defined!
#endif
#else
#ifdef TTC_NETWORK_LAYER3

#ifndef TTC_NETWORK_LAYER2
#error TTC_NETWORK_LAYER5 is defined, but not TTC_NETWORK_LAYER2 - all lower TTC_NETWORK_LAYERn must be defined!
#endif
#ifndef TTC_NETWORK_LAYER1
#error TTC_NETWORK_LAYER5 is defined, but not TTC_NETWORK_LAYER1 - all lower TTC_NETWORK_LAYERn must be defined!
#endif

#define TTC_NETWORK_LAYER_AMOUNT 3
#else
#ifdef TTC_NETWORK_LAYER2

#ifndef TTC_NETWORK_LAYER1
#error TTC_NETWORK_LAYER5 is defined, but not TTC_NETWORK_LAYER1 - all lower TTC_NETWORK_LAYERn must be defined!
#endif

#define TTC_NETWORK_LAYER_AMOUNT 2
#else
#ifdef TTC_NETWORK_LAYER1
#define TTC_NETWORK_LAYER_AMOUNT 1
#else
#define TTC_NETWORK_LAYER_AMOUNT 0
#endif
#endif
#endif
#endif
#endif

/* Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_NETWORK_LAYER 0  # disable assert handling for network_layer devices
 *
 */
#ifndef TTC_ASSERT_NETWORK_LAYER    // any previous definition set (Makefile)?
#define TTC_ASSERT_NETWORK_LAYER 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_NETWORK_LAYER == 1)  // use Assert()s in NETWORK_LAYER code (somewhat slower but alot easier to debug)
#define Assert_NETWORK_LAYER(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in NETWORK_LAYER code (somewhat smaller + faster, but crashes are hard to debug)
#define Assert_NETWORK_LAYER(Condition, ErrorCode)
#endif

/** Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_network_layer_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_network_layer_architecture
#warning Missing low-level definition for t_ttc_network_layer_architecture (using default)
#define t_ttc_network_layer_architecture void
#endif

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_network_layer_physical_index (starts at zero!)
    //
    // You may use this enum to assign physical index to logical TTC_NETWORK_LAYERn constants in your makefile
    //
    // E.g.: Defining physical device #3 of network_layer as first logical device TTC_NETWORK_LAYER1
    //       COMPILE_OPTS += -DTTC_NETWORK_LAYER1=INDEX_NETWORK_LAYER3
    //
    INDEX_NETWORK_LAYER1,
    INDEX_NETWORK_LAYER2,
    INDEX_NETWORK_LAYER3,
    INDEX_NETWORK_LAYER4,
    INDEX_NETWORK_LAYER5,
    INDEX_NETWORK_LAYER6,
    INDEX_NETWORK_LAYER7,
    INDEX_NETWORK_LAYER8,
    INDEX_NETWORK_LAYER9,
    // add more if needed

    INDEX_NETWORK_LAYER_ERROR
} e_ttc_network_layer_physical_index;
typedef enum {    // e_ttc_network_layer_errorcode     return codes of NETWORK_LAYER devices
    ec_network_layer_OK = 0,

    // other warnings go here..

    ec_network_layer_ERROR,                 // general failure
    ec_network_layer_NULL,                  // NULL pointer not accepted
    ec_network_layer_DeviceNotFound,        // corresponding device could not be found
    ec_network_layer_InvalidImplementation, // internal self-test failed
    ec_network_layer_InvalidConfiguration,  // sanity check of device configuration failed

    // other failures go here..

    ec_network_layer_unknown                // no valid errorcodes past this entry
} e_ttc_network_layer_errorcode;
typedef enum {    // e_ttc_network_layer_architecture  types of architectures supported by NETWORK_LAYER driver
    ta_network_layer_None,           // no architecture selected


    ta_network_layer_usart, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_network_layer_unknown        // architecture not supported
} e_ttc_network_layer_architecture;

#ifndef t_ttc_network_layer_architecture
#define t_ttc_network_layer_architecture void*
#endif

typedef struct s_ttc_network_layer_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_network_layer_init()
    //       and after ttc_network_layer_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    e_ttc_network_layer_architecture Architecture; // type of architecture used for current network_layer device
    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_NETWORK_LAYER1, ...)
    t_u8  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)

    // low-level configuration (structure defined by low-level driver)
    t_ttc_network_layer_architecture* LowLevelConfig;

    // these are implemented and set by low-level driver
    void ( *upper_rx )( t_ttc_heap_block* Packet );
    void ( *lower_rx )( t_ttc_heap_block* Packet );
    void ( *tick )( t_base ElapseMicroseconds );

    // these can be changed from outside
    void ( *upper_tx )( t_ttc_heap_block* Packet );
    void ( *lower_tx )( t_ttc_heap_block* Packet );

    union  { // generic configuration bits common for all low-level Drivers
        t_u16 All;
        struct {
            unsigned Initialized        : 1;  // ==1: device has been initialized successfully
            // ToDo: additional high-level flags go here..

            unsigned Reserved1        : 15; // pad to 16 bits
        } Bits;
    } Flags;

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_network_layer_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_NETWORK_LAYER_TYPES_H
