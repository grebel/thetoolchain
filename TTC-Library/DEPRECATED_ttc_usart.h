#ifndef TTC_USART_H
#define TTC_USART_H

/*{ ttc_usart.h **********************************************************
 
                      The ToolChain
                      
   Device independent support for 
   Universal Synchronous Asynchronous Receiver and Transmitters (USARTs)
   
   Currently implemented architectures: stm32

   
   written by Gregor Rebel 2012
   
   Reference:
   
   RM008 - STM32 Reference Manual p. 757
   -> http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/REFERENCE_MANUAL/CD00171190.pdf

   
   How it works
   
   Each makefile.100_board* can define one or more USARTs being routed to connectors.
   This is done via lines of this type:
     COMPILE_OPTS += -D<OPTION>=<VALUE>
   This sets compile option <OPTION> to value <VALUE>
   
   USARTn are enumerated starting at index 1 (a special enum provides handy names: INDEX_USART1, INDEX_USART2, ..).
   Each USARTn can be configured via this constants:
   TTC_USART1     <USART_INDEX>          1..MAX_USART  index number of internal USART device to use
   TTC_USART1_TX  <GPIO_BANK>,<PIN_NO>   sets port bit to use for TX pin
   TTC_USART1_RX  <GPIO_BANK>,<PIN_NO>   sets port bit to use for RX pin
   TTC_USART1_RTS <GPIO_BANK>,<PIN_NO>   sets port bit to use for RTS pin
   TTC_USART1_CTS <GPIO_BANK>,<PIN_NO>   sets port bit to use for CTS pin
   
   Note: <GPIO_BANK>,<PIN_NO> can be given as device independent PIN_P<x><n> macros.
   E.g. on stm32 architecture GPIOA,3 equals to PIN_PA3
   
   # Example excerpt from makefile.100_board_olimex_p107
   COMPILE_OPTS += -DTTC_USART1=INDEX_USART3 # USART connected to RS232 connector 9-pin female
   COMPILE_OPTS += -DTTC_USART1_TX=PIN_PD8   # RS232 connector 9-pin female TX-pin
   COMPILE_OPTS += -DTTC_USART1_RX=PIN_PD9   # RS232 connector 9-pin female RX-pin
   COMPILE_OPTS += -DTTC_USART1_RTS=PIN_PD12 # RS232 connector 9-pin female RTS-pin
   COMPILE_OPTS += -DTTC_USART1_CTS=PIN_PD11 # RS232 connector 9-pin female CTS-pin
    
   COMPILE_OPTS += -DTTC_USART2=INDEX_USART2 # USART connected to 10-pin male header connector
   COMPILE_OPTS += -DTTC_USART2_TX=PIN_PD4   # RS232 on 10-pin male header connector TX-Pin
   COMPILE_OPTS += -DTTC_USART2_RX=PIN_PD5   # RS232 on 10-pin male header connector RX-Pin

}*/
//{ Defines/ TypeDefs ****************************************************

//} Defines
/** Defines/ TypeDefs/ structures being set by architecture driver
 *
 * Constant defines
 *     TTC_USART_MAX_AMOUNT      maximum amount of available USART/ UART units in current architecture
 *
 * Structures
 *     ttc_usart_architecture_t  architecture specific configuration data of single USART
 *
 */
//{ Includes *************************************************************

#include "ttc_basic.h"
#include "ttc_queue.h"
#include "ttc_memory.h"
#include "ttc_usart_types.h"
#include "ttc_channel_types.h"

#ifdef TARGET_ARCHITECTURE_STM32F1xx
  #include "stm32/DEPRECATED_stm32_usart.h"
#endif

#ifdef TARGET_ARCHITECTURE_STM32L1xx
  #include "stm32l1/stm32l1_usart.h"
#endif

//} Includes
//{ Structures/ Enums ****************************************************

//} Structures/ Enums
//{ Global Variables *****************************************************



//} Global Variables
//{ Check low-level DRIVER implementations


//}
//{ Function prototypes **************************************************

const ttc_channel_device_interface_t* ttc_usart_get_channel_interface();

/** Prepares USART-interface for operation. Must be called before any other function!
 */
void ttc_usart_prepare();

/** returns amount of USART devices available on current uC
 * @return amount of available USART devices
 */
u8_t ttc_usart_get_max_logicalindex();

/** returns configuration of indexed usart (asserts if no valid configuration was read)
 *
 * @param LogicalIndex    device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @return                !=NULL: pointer to struct ttc_usart_config_t
 */
ttc_usart_config_t* ttc_usart_get_configuration(u8_t LogicalIndex) ;

/** fills out given USART_Generic with default values for indexed USART
 * @param LogicalIndex    device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param USART_Generic   pointer to struct ttc_usart_config_t
 * @return  == 0:         *USART_Generic has been initialized successfully; != 0: error-code
 */
ttc_channel_errorcode_e ttc_usart_reset(u8_t LogicalIndex);

/** fills out given USART_Generic with maximum valid values for indexed USART
 * @param LogicalIndex    device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param USART_Generic   pointer to struct ttc_usart_config_t
 * @return  == 0:         *USART_Generic has been initialized successfully; != 0: error-code
 */
ttc_channel_errorcode_e ttc_usart_get_features(u8_t LogicalIndex, ttc_usart_config_t* USART_Generic);

/** delivers a memory block usable as transmit or receive buffer
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @return               != NULL: pointer to new or reused memory block; == NULL: currently no tx-buffer available (-> increase USART_Generic->Size_Queue_Tx or wait until tx-buffer is released)
 */
ttc_memory_block_t* ttc_usart_get_empty_block();

/** initializes single USART with current configuration
 * @param LogicalIndex    device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @return                == 0: USART has been initialized successfully; != 0: error-code
 */
ttc_channel_errorcode_e ttc_usart_init(u8_t LogicalIndex);
#ifndef _driver_ttc_usart_init
#define _driver_ttc_usart_init()
#warning Missing implementation for _driver_ttc_usart_init()
#endif

/** registers given function as a receiver for incoming data
 *
 * Note: This function will automatically call ttc_usart_init() to init/reinit USART
 *
 * @param LogicalIndex    device index of USART to use (1..ttc_usart_get_max_logicalindex())
 * @param receiveBlock    function to call for each incoming data block (will contain one or more bytes)
 * @param Argument        will be passed as first argument to receiveBlock()
 * @return                == 0: USART has been initialized successfully; != 0: error-code
 */
ttc_channel_errorcode_e ttc_usart_register_receive(u8_t LogicalIndex, ttc_memory_block_t* (*receiveBlock)(void* Argument, struct ttc_usart_generic_s* USART_Generic, ttc_memory_block_t* Block), void* Argument);

/** Stores given raw buffer to be send in parallel. (Function copies data and returns immediately)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 *
 * @param LogicalIndex     device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Buffer           memory location from which to read data
 * @param Amount           amount of bytes to send from Buffer[] (even zero bytes are sent)
 * @return                 == 0: Buffer has been stored for delivery successfully; != 0: error-code
 */
ttc_channel_errorcode_e ttc_usart_send_raw(u8_t LogicalIndex, u8_t* Buffer, Base_t Amount);

/** Stores given raw buffer to be send in parallel. (Function stores pointer to data and returns immediately)
 *
 * This function is especially optimized for constant data (e.g. strings in ROM) and avoids extra copying into buffers.
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 *
 * @param LogicalIndex     device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Buffer           memory location from which to read data (must not change!)
 * @param Amount           amount of bytes to send from Buffer[] (even zero bytes are sent)
 * @return                 == 0: Buffer has been stored for delivery successfully; != 0: error-code
 */
ttc_channel_errorcode_e ttc_usart_send_raw_const(u8_t LogicalIndex, u8_t* Buffer, Base_t Amount);

/** Stores given string buffer to be send in parallel. (Function copies data and returns immediately)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 *
 * @param LogicalIndex     device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Buffer           memory location from which to read data
 * @param MaxLength        no more than this amount of bytes will be send
 * @return                 == 0: Buffer has been stored for delivery successfully; != 0: error-code
 */
ttc_channel_errorcode_e ttc_usart_send_string(u8_t LogicalIndex, const char* Buffer, Base_t MaxLength);

/** Stores given string buffer to be send in parallel. (Function stores pointer to data and returns immediately)
 *
 * This function is especially optimized for constant data (e.g. strings in ROM) and avoids extra copying into buffers.
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 *
 * @param LogicalIndex     device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Buffer           memory location from which to read data
 * @param MaxLength        no more than this amount of bytes will be send
 * @return                 == 0: Buffer has been stored for delivery successfully; != 0: error-code
 */
ttc_channel_errorcode_e ttc_usart_send_string_const(u8_t LogicalIndex, const char* Buffer, Base_t MaxLength);

/** Sends out data from given memory block in parallel. This function copies no data and has least overhead of all delayed functions.
 * After all data from Block has been transmitted, the Block is appended to an internal queue.
 * Use ttc_usart_get_tx_block() to reuse the block from this queue.
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Block          stores data to be send; It is mandatory to allocate Block with ttc_usart_get_tx_block()!
 * @return               == 0: Buffer has been stored for delivery successfully; != 0: error-code
 */
ttc_channel_errorcode_e ttc_usart_send_block(u8_t LogicalIndex, ttc_memory_block_t* Block);

/** Waits until transmit queue has run empty on given USART
 *
 * Note: USART must be initialized before!
 * Note: This function blocks until all bytes have been sent!
 * Note: You may want to call ttc_task_begin_criticalsection() before to be sure that no other task transmits in parallel.
 *
 * @param LogicalIndex      device index of USART to init (1..ttc_usart_get_max_logicalindex())
 */
void ttc_usart_flush_tx(u8_t LogicalIndex);

/** Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Word           pointer to 16 bit buffer where to store Word
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
ttc_channel_errorcode_e ttc_usart_read_word(u8_t LogicalIndex, u16_t* Word);

/** Reads single data byte from input buffer.
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Word           pointer to 16 bit buffer where to store Word
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
ttc_channel_errorcode_e ttc_usart_read_byte(u8_t LogicalIndex, u8_t* Byte);

/** Adds 1 byte to receive queue. Called from low level USART interrupt service routine
 * Note: This function is private and should not be called from outside.
 *
 * @param USART_Generic  structure containing configuration data of USART which has received this byte
 * @param Byte           received byte to add to receive buffer
 */
void _ttc_usart_rx_isr(ttc_usart_config_t* USART_Generic, u8_t Byte);

/** Will send out next byte from transmit queue.
 * Note: This function is private and should not be called from outside.
 *
 * @param USART_Generic  structure containing configuration data of USART which has received this byte
 * @param USART_Hardware low-level pointer required for fast low-level transmit
 */
void _ttc_usart_tx_isr(ttc_usart_config_t* USART_Generic, void* USART_Hardware);

/** translates from logical index of functional unit to real hardware index (0=USART1, 1=USART2, ...)
 * @param LogicalIndex    logical device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @return                real device index of USART to init (0..MAX)
 */
u8_t ttc_usart_get_physicalindex(u8_t LogicalIndex);

/** Defines which logical device shall be used by ttc_usart_stdout_send_string()/ ttc_usart_stdout_send_block()
 *
 * @param LogicalIndex   >0: logical device index of USART (must be initialized before!); ==0: disable default USART
 * @return               != 0: error message
 */
ttc_channel_errorcode_e ttc_usart_stdout_set(u8_t LogicalIndex);

/** will send given memory block to default USART being set by ttc_usart_stdout_set()
 *
 * @param Block         pointer to a ttc_memory_block_t (will be released after sending automatically)
 */
void ttc_usart_stdout_send_block(ttc_memory_block_t* Block);

/** will send given string to default USART being set by ttc_usart_stdout_set()
 *
 * @param Block         pointer to a ttc_memory_block_t (will be released after sending automatically)
 */
void ttc_usart_stdout_send_string(const u8_t* String, Base_t MaxSize);

/** Send out given amount of raw bytes from buffer.
 * Will not interpret bytes in buffer.
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Buffer         memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[]
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_channel_errorcode_e _ttc_usart_send_raw_blocking(u8_t LogicalIndex, const u8_t* Buffer, u16_t Amount);

/** Send out given bytes from buffer until first zero byte or MaxLength reached.
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Buffer         memory location from which to read data
 * @param MaxLength      no more than this amount of bytes will be send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_channel_errorcode_e _ttc_usart_send_string_blocking(u8_t LogicalIndex, const char* Buffer, u16_t MaxLength);

/** Send out given data word (8 or 9 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until bytes has been sent!
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Byte           8 or 9 bit value to send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_channel_errorcode_e _ttc_usart_send_word_blocking(u8_t LogicalIndex, const u16_t Word);

/** Implements asynchronous receiver. Reads data from one receive queue.
 * @param Args  ttc_usart_config_t* pointer to data of configured USART
 */
void _ttc_usart_task_receive(void* Args);

/** Implements asynchronous transmitter. Reads data from one transmit queue.
 * @param Args  ttc_usart_config_t* pointing to data of configured USART
 */
void _ttc_usart_task_transmit(void* Args);

/** releases a used Tx/ Rx memory blocks for later reuse
 * @param Block         pointer to a ttc_memory_block_t
 */
void _ttc_usart_release_block(ttc_memory_block_t* Block);

/** releases a used Tx/ Rx memory blocks for later reuse (will not block)
 * @param Block         pointer to a ttc_memory_block_t
 */
void _ttc_usart_release_block_isr(ttc_memory_block_t* Block);

/** returns configuration of indexed usart (allocates + resets new configuration if necessary)
 * for internal use only
 * @param LogicalIndex    device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @return                !=NULL: pointer to struct ttc_usart_config_t
 */
ttc_usart_config_t* _ttc_usart_get_configuration(u8_t LogicalIndex);

//} Function prototypes

#endif //TTC_USART_H
