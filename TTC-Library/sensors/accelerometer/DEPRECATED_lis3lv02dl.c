/*{ lis3lv02dl::.c ************************************************

 Program Name: lis3lv02dl


 Description: Basic defines and support functions for Accelerometer LIS3LV02DL.
              ->

 $Author: <Sascha Poggemann>, <Gregor Rebel> $

 $Date: 2012_06_19 $
 
}*/

#include "lis3lv02dl.h"

//{ Private Defines/ TypeDefs ****************************************************


//}  Private Defines
//{ Includes *************************************************************



//} Includes
//{ Global Variables *****************************************************

//} Global Variables
//{ Private Function prototypes **************************************************


//} Private Function prototypes

//{ Implemented Functions **************************************************

lis3lv02dl_errorcode_e i2c_lis3lv02dl_init(u8_t I2C_Index) {

    i2c_lis3lv02dl_CONFIG liscfg;
    ttc_memory_set(&liscfg, 0, sizeof(i2c_lis3lv02dl_CONFIG));

    liscfg.CTRL_REG1.DevicePowermode = 3;
    liscfg.CTRL_REG1.FrequencyRate = 0;
    liscfg.CTRL_REG1.Selftest = 0;
    liscfg.CTRL_REG1.XEnable = 1;
    liscfg.CTRL_REG1.YEnable = 1;
    liscfg.CTRL_REG1.ZEnable = 1;
    liscfg.CTRL_REG2.Fullscale = 0;
    liscfg.CTRL_REG2.BlockDataUpdate = 1;
    liscfg.CTRL_REG2.BigEndian = 0;

    u8_t StatusRegister = 0;
    ttc_i2c_errorcode_e Error = ttc_i2c_read_register(I2C_Index, I2C_ADDRESS_LIS3LV02DL, LIS_REG_WHO_AM_I, tiras_MSB_First_8Bit, &StatusRegister);
    if (Error != tie_OK)
        return Error;

    if (StatusRegister != 0x3A)
        return lie_DeviceNotFound;


    u8_t *r1 = (u8_t*) &liscfg.CTRL_REG1;
    u8_t *r2 = (u8_t*) &liscfg.CTRL_REG2;
    u8_t *r3 = (u8_t*) &liscfg.CTRL_REG3;

    ttc_i2c_write_register(I2C_Index, I2C_ADDRESS_LIS3LV02DL, LIS_CTRL_REG1, tiras_MSB_First_8Bit, *r1);
    ttc_i2c_write_register(I2C_Index, I2C_ADDRESS_LIS3LV02DL, LIS_CTRL_REG2, tiras_MSB_First_8Bit, *r2);
    ttc_i2c_write_register(I2C_Index, I2C_ADDRESS_LIS3LV02DL, LIS_CTRL_REG3, tiras_MSB_First_8Bit, *r3);

    if (1) { // compare written values
        u8_t Compare1 = 0;
        u8_t Compare2 = 0;
        u8_t Compare3 = 0;

        ttc_i2c_read_register(I2C_Index, I2C_ADDRESS_LIS3LV02DL, LIS_CTRL_REG1, tiras_MSB_First_8Bit, &Compare1);
        ttc_i2c_read_register(I2C_Index, I2C_ADDRESS_LIS3LV02DL, LIS_CTRL_REG2, tiras_MSB_First_8Bit, &Compare2);
        ttc_i2c_read_register(I2C_Index, I2C_ADDRESS_LIS3LV02DL, LIS_CTRL_REG3, tiras_MSB_First_8Bit, &Compare3);

        Assert(*r1 == Compare1, ec_UNKNOWN);
        Assert(*r2 == Compare2, ec_UNKNOWN);
        Assert(*r3 == Compare3, ec_UNKNOWN);
    }

    return lie_OK;
}

/*
void task_i2c_lis3lv02dl(void *TaskArgument) {
    TaskArgument = TaskArgument; // avoids warning: unused parameter 'TaskArgument'

    u8_t acc_buffer[6]={0,0,0,0,0,0};

    u8_t acc_currentvalue[1] ;
    u8_t status_buffer[1];

    int beat = 0 ;




    while (1) {
        //sendData("Hello world!\n\r", -1);
              if(0)readACC_Values(I2C_Index,I2C_ADDRESS_LIS3LV02DL,acc_buffer);
              if(0)ttc_i2c_write_register(I2C_Index, I2C_ADDRESS_LIS3LV02DL, 0x61, tiras_MSB_First_8Bit, 0xaa, 200);
              if(1)ttc_i2c_read_register(I2C_Index,I2C_ADDRESS_LIS3LV02DL, LIS_REG_STATUS, tiras_MSB_First_8Bit, status_buffer,200);

              //read sensor data
              if(1){


                  ttc_i2c_read_register(I2C_Index,I2C_ADDRESS_LIS3LV02DL, LIS_OUTX_H, tiras_MSB_First_8Bit, acc_currentvalue,200);
                  acc_buffer[0]= acc_currentvalue[0];
                  ttc_i2c_read_register(I2C_Index,I2C_ADDRESS_LIS3LV02DL, LIS_OUTX_L, tiras_MSB_First_8Bit, acc_currentvalue,200);
                  acc_buffer[1]= (acc_currentvalue[0]);

                  ttc_i2c_read_register(I2C_Index,I2C_ADDRESS_LIS3LV02DL, LIS_OUTY_H, tiras_MSB_First_8Bit, acc_currentvalue,200);
                  acc_buffer[2]= acc_currentvalue[0];
                  ttc_i2c_read_register(I2C_Index,I2C_ADDRESS_LIS3LV02DL, LIS_OUTY_L, tiras_MSB_First_8Bit, acc_currentvalue,200);
                  acc_buffer[3]= (acc_currentvalue[0]);

                  ttc_i2c_read_register(I2C_Index,I2C_ADDRESS_LIS3LV02DL, LIS_OUTZ_H, tiras_MSB_First_8Bit, acc_currentvalue,200);
                  acc_buffer[4]= acc_currentvalue[0];
                  ttc_i2c_read_register(I2C_Index,I2C_ADDRESS_LIS3LV02DL, LIS_OUTZ_L, tiras_MSB_First_8Bit, acc_currentvalue,200);
                  acc_buffer[5]= (acc_currentvalue[0]);



              }

              ttc_task_msleep(100);
              beat ++;
      #ifdef EXTENSION_400_lcd_320x240_olimex
              LCD_ConSetPos(11,0);
              LCD_Printf("Beat:");
              //LCD_ConSetPos(11,0);
              LCD_Printf("%i",beat);

              LCD_ConSetPos(12,0);
      //        acc_buffer[2]=6;
      //        acc_buffer[3]=15;
              short s=-5;
             // LCD_Printf("12bitvalue %i",get12BitValue(&acc_buffer[2]));
      #endif
}

}
*/

//} Implemented Functions
