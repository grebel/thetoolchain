#ifndef lis3lv02dl_H
#define lis3lv02dl_H

/*{ lis3lv02dl::.h ************************************************

 Basic defines and register structures for accelerometer device LIS3LV02DL.

 $Author: <Sascha Poggemann>, <Gregor Rebel> $

 $Date: 2012_06_12 $
 
}*/
//{ Defines/ TypeDefs ****************************************************

#define I2C_ADDRESS_LIS3LV02DL 29

#define LIS_REG_WHO_AM_I    0x0F
#define LIS_REG_STATUS      0x27
#define LIS_CTRL_REG1       0x20
#define LIS_CTRL_REG2       0x21
#define LIS_CTRL_REG3       0x22

#define LIS_OUTX_L          0x28
#define LIS_OUTX_H          0x29

#define LIS_OUTY_L          0x2A
#define LIS_OUTY_H          0x2B

#define LIS_OUTZ_L          0x2C
#define LIS_OUTZ_H          0x2D


//} Defines
//{ Includes *************************************************************

#include "../../ttc_basic.h"
#include "../../ttc_i2c.h"
#include "../../ttc_memory.h"

//} Includes
//{ Structures/ Enums ****************************************************

typedef enum {     // lis3lv02dl_errorcode_e
  lie_OK,                                  // =0: no error
  lie_TimeOut,                             // timeout occured in called function
  lie_NotImplemented,                      // function has no implementation for current architecture
  lie_DeviceNotFound,                      // adressed I2C device not available in current uC
  lie_InvalidArgument,                     // general argument error
  lie_DeviceNotReady,                      // choosen device has not been initialized properly

  lie_UnknownError
} lis3lv02dl_errorcode_e;

typedef struct {   // i2c_lis3lv02dl_REG_CTLREG1
    unsigned XEnable            : 1;
    unsigned YEnable            : 1;
    unsigned ZEnable            : 1;
    unsigned Selftest           : 1;
    unsigned FrequencyRate      : 2;
    unsigned DevicePowermode    : 2;
} __attribute__((__packed__)) i2c_lis3lv02dl_REG_CTLREG1;

typedef struct {   // i2c_lis3lv02dl_REG_CTLREG2
    unsigned DataAligment       : 1;
    unsigned SPIMode            : 1;
    unsigned DataReadyEnable    : 1;
    unsigned InterruptEnable    : 1;
    unsigned Boot               : 1;
    unsigned BigEndian          : 1;
    unsigned BlockDataUpdate    : 1;
    unsigned Fullscale          : 1;
} __attribute__((__packed__)) i2c_lis3lv02dl_REG_CTLREG2;

typedef struct {   // i2c_lis3lv02dl_REG_CTLREG3
    unsigned HPF_CutoffFreq     : 2;
    unsigned reserved0          : 2;
    unsigned FilterDataSelect   : 1;
    unsigned HPF_FreeFall       : 1;
    unsigned HPF_Direction      : 1;
    unsigned ExternalClock      : 1;
} __attribute__((__packed__)) i2c_lis3lv02dl_REG_CTLREG3;

typedef struct {   // i2c_lis3lv02dl_CONFIG
    i2c_lis3lv02dl_REG_CTLREG1  CTRL_REG1;
    i2c_lis3lv02dl_REG_CTLREG2  CTRL_REG2;
    i2c_lis3lv02dl_REG_CTLREG3  CTRL_REG3;
} __attribute__((__packed__)) i2c_lis3lv02dl_CONFIG;

//} Structures/ Enums
//{ Function prototypes **************************************************

/* Runs basic initialization of device via pre-initialized I2C bus.
 * Also checks if device can be accessed.
 * @param I2C_Index  index of already initialized I2C-bus (see ttc_i2c_init() )
 */
lis3lv02dl_errorcode_e i2c_lis3lv02dl_init(u8_t I2C_Index);

//} Function prototypes

#endif //lis3lv02dl_H
