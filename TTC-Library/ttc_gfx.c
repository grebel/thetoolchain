/** { ttc_gfx.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for gfx devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 23 at 20140313 10:58:25 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "compile_options.h"
#include "ttc_gfx.h"
#include "ttc_heap.h"

#if TTC_GFX_AMOUNT == 0
    #warning No GFX devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of gfx devices.
 *
 */


// for each initialized device, a pointer to its   and ili93xx definitions is stored
A_define( t_ttc_gfx_config*, ttc_gfx_configs, TTC_GFX_AMOUNT );

// logical index of display to use by ttc_gfx_stdout_send_string()/ ttc_gfx_stdout_send_block()
t_u8  ttc_gfx_stdout_index = 0;

/* Quick access variables for current display provide faster function calls and use less RAM than using function arguments.
 * Note: No write access allowed. Call ttc_gfx_switch_to() to update when using multiple displays!
 * Note: ttc_gfx allows no multithreading for maximum speed and minimum RAM usage!
 */
t_ttc_gfx_quick ttc_gfx_Quick;

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8                ttc_gfx_get_max_index() {
    return TTC_GFX_AMOUNT;
}
t_ttc_gfx_config*   ttc_gfx_get_configuration( t_u8 LogicalIndex ) {
    Assert_GFX( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_gfx_config* Config = A( ttc_gfx_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        Config = A( ttc_gfx_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_gfx_config ) );
        Config->Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_gfx_load_defaults( LogicalIndex );
    }

    return Config;
}
e_ttc_gfx_errorcode ttc_gfx_deinit( t_u8 LogicalIndex ) {
    Assert_GFX( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_gfx_config* Config = ttc_gfx_get_configuration( LogicalIndex );

    e_ttc_gfx_errorcode Result = _driver_gfx_deinit( Config );
    Assert_GFX_EXTRA( Result == ec_gfx_OK, ttc_assert_origin_auto );  // low-level driver could not deinit display. Check implementation of _driver_gfx_deinit()!

    Config->Flags.Bits.Initialized = 0;
    if ( ttc_gfx_Quick.Config == Config ) { // deinitializing current display: clear its data
        ttc_memory_set( & ttc_gfx_Quick, 0, sizeof( ttc_gfx_Quick ) );
    }

    return Result;
}
e_ttc_gfx_errorcode ttc_gfx_init( t_u8 LogicalIndex ) {
    ttc_gfx_switch_to( LogicalIndex );
    t_ttc_gfx_config* Config = ttc_gfx_get_configuration( LogicalIndex );

    ttc_gfx_select( ttc_gfx_Quick.Config );
    e_ttc_gfx_errorcode Result = _driver_gfx_init( Config );

    if ( Result == ec_gfx_OK ) {
        Config->Flags.Bits.Initialized = 1;
        ttc_gfx_switch_to( LogicalIndex );
        ttc_gfx_clear(); //first cleanup of the display

#ifdef EXTENSION_ttc_font
        // fonts available: load first font as default
        ttc_font_init();

        Config->TextBorderLeft = 0;
        Config->TextBorderTop = 0;
        ttc_gfx_set_font( ttc_font_get_configuration( 1 ) );
#endif

        ttc_gfx_switch_to( LogicalIndex );
    }
    return Result;
}
e_ttc_gfx_errorcode ttc_gfx_load_defaults( t_u8 LogicalIndex ) {
    Assert_GFX( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_gfx_config* Config = ttc_gfx_get_configuration( LogicalIndex );

    if ( Config->Flags.Bits.Initialized )
    { ttc_gfx_deinit( LogicalIndex ); }

    ttc_memory_set( Config, 0, sizeof( t_ttc_gfx_config ) );

    // load   default values
    Config->LogicalIndex = LogicalIndex;
    Config->PhysicalIndex = ttc_gfx_logical_2_physical_index( LogicalIndex );

    Config->PhysicalHeight = TTC_GFX1_HEIGHT;
    Config->PhysicalWidth  = TTC_GFX1_WIDTH;
    Config->Depth          = TTC_GFX1_DEPTH;
    Config->TextBorderLeft = 0;
    Config->TextBorderTop  = 0;

    //Insert additional   default values here ...
    e_ttc_gfx_errorcode Error = _driver_gfx_load_defaults( Config );
    Assert_GFX( Config->Architecture, ttc_assert_origin_auto ); // Low-Level driver must set this field t one from e_ttc_gfx_architecture!

#if (TTC_GFX_ROTATION_CLOCKWISE==0) || (TTC_GFX_ROTATION_CLOCKWISE==180)
    Config->Height = Config->PhysicalHeight;
    Config->Width  = Config->PhysicalWidth;
#endif
#if (TTC_GFX_ROTATION_CLOCKWISE==90) || (TTC_GFX_ROTATION_CLOCKWISE==270)
    Config->Height = Config->PhysicalWidth;
    Config->Width  = Config->PhysicalHeight;
#endif

    return Error;
}
t_u8                ttc_gfx_logical_2_physical_index( t_u8 LogicalIndex ) {

    switch ( LogicalIndex ) {         // determine base register and other low-level configuration data
#ifdef TTC_GFX1
        case 1: return TTC_GFX1;
#endif
#ifdef TTC_GFX2
        case 2: return TTC_GFX2;
#endif
#ifdef TTC_GFX3
        case 3: return TTC_GFX3;
#endif
#ifdef TTC_GFX4
        case 4: return TTC_GFX4;
#endif
#ifdef TTC_GFX5
        case 5: return TTC_GFX5;
#endif
#ifdef TTC_GFX6
        case 6: return TTC_GFX6;
#endif
#ifdef TTC_GFX7
        case 7: return TTC_GFX7;
#endif
#ifdef TTC_GFX8
        case 8: return TTC_GFX8;
#endif
#ifdef TTC_GFX9
        case 9: return TTC_GFX9;
#endif
#ifdef TTC_GFX10
        case 10: return TTC_GFX10;
#endif
        // extend as required

        default: break;
    }

    Assert_GFX( 0, ttc_assert_origin_auto ); // No TTC_GFXn defined! Check your makefile.100_board_* file!
    return -1; // program flow should never go here
}
t_u8                ttc_gfx_physical_2_logical_index( t_u8 PhysicalIndex ) {

    switch ( PhysicalIndex ) {         // determine base register and other low-level configuration data
#ifdef TTC_GFX1
        case TTC_GFX1: return 1;
#endif
#ifdef TTC_GFX2
        case TTC_GFX2: return 2;
#endif
#ifdef TTC_GFX3
        case TTC_GFX3: return 3;
#endif
#ifdef TTC_GFX4
        case TTC_GFX4: return 4;
#endif
#ifdef TTC_GFX5
        case TTC_GFX5: return 5;
#endif
#ifdef TTC_GFX6
        case TTC_GFX6: return 6;
#endif
#ifdef TTC_GFX7
        case TTC_GFX7: return 7;
#endif
#ifdef TTC_GFX8
        case TTC_GFX8: return 8;
#endif
#ifdef TTC_GFX9
        case TTC_GFX9: return 9;
#endif
#ifdef TTC_GFX10
        case TTC_GFX10: return 10;
#endif
        // extend as required

        default: break;
    }

    Assert_GFX( 0, ttc_assert_origin_auto ); // No TTC_GFXn defined! Check your makefile.100_board_* file!
    return -1; // program flow should never go here
}
void                ttc_gfx_prepare() {
    // add your startup code here (Singletasking!)
    ttc_memory_set( & ttc_gfx_Quick, 0, sizeof( ttc_gfx_Quick ) );

    //set needed gpio to input
    _driver_gfx_prepare();
}
e_ttc_gfx_errorcode ttc_gfx_reset( t_u8 LogicalIndex ) {
    Assert_GFX( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_gfx_config* Config = ttc_gfx_get_configuration( LogicalIndex );

    return _driver_gfx_reset( Config );
}
void                ttc_gfx_polygon_l( t_s32 ExternalX, t_s32 ExternalY, t_s32 Width, t_s32 Height, t_s32 InternalX, t_s32 InternalY ) {

    if ( ( ExternalX < InternalX ) && ( ExternalY < InternalY ) ) {
        ttc_gfx_rect_fill( ExternalX, ExternalY, InternalX - ExternalX, Height );
        ttc_gfx_rect_fill( InternalX, ExternalY, ExternalX + Width - InternalX, InternalY - ExternalY );
    }
    else if ( ( ExternalX < InternalX ) && ( ExternalY > InternalY ) ) {
        ttc_gfx_rect_fill( ExternalX, ExternalY - Height, InternalX - ExternalX, Height );
        ttc_gfx_rect_fill( InternalX, InternalY, ExternalX + Width - InternalX, ExternalY - InternalY );
    }
    else if ( ( ExternalX > InternalX ) && ( ExternalY < InternalY ) ) {
        ttc_gfx_rect_fill( ExternalX - Width, ExternalY, Width, InternalY - ExternalY );
        ttc_gfx_rect_fill( InternalX, InternalY, ExternalX - InternalX, ExternalY + Height - InternalY );
    }
    else {
        ttc_gfx_rect_fill( ExternalX - Width, InternalY, Width, ExternalY - InternalY );
        ttc_gfx_rect_fill( InternalX, ExternalY - Height, ExternalX - InternalX, InternalY + Height - ExternalY );
    }
}
void                ttc_gfx_rect_fill( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height ) {

    if ( Width <= 0 )  {                    // invert width for simpler math
        if ( Width == 0 )  { // nothing to draw
            return;
        }
        if ( X > Width ) {
            X += Width;
            Width = -Width;
        }
        else {      // leaving screen: reduce width
            Width = -Width - X;
            X = 0;
        }
    }
    if ( Height <= 0 ) {                    // invert Height for simpler math
        if ( Height == 0 )  { // nothing to draw
            return;
        }
        if ( Y > Height ) {
            Y += Height;
            Height = -Height;
        }
        else {      // leaving screen: reduce Height
            Height = -Height - Y;
            Y = 0;
        }
    }
    if ( X >= ttc_gfx_Quick.Width )        // outside of displayable area
    { return; }
    if ( Y >= ttc_gfx_Quick.Height )       // outside of displayable area
    { return; }

#if TTC_GFX_ROTATION_CLOCKWISE == 90
    _driver_gfx_rect_fill( ROTATED_X( X, Y ), ROTATED_Y( ( X + Width ), Y ), Height, Width );
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 180
    _driver_gfx_rect_fill( ROTATED_X( X, Y ), ROTATED_Y( X, Y ), -( Width ), -( Height ) );
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 270
    _driver_gfx_rect_fill( ROTATED_X( X, ( Y + Height ) ), ROTATED_Y( X, Y ), -( Height ), -( Width ) );
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 0
    _driver_gfx_rect_fill( X, Y, Width, Height );
#endif
}
void                ttc_gfx_color_bg_palette( t_u8 PaletteIndex ) {
    Assert_GFX( ttc_gfx_Quick.Config, ttc_assert_origin_auto ); // global variable not set: did you call ttc_gfx_init() before calling this function?

    if ( PaletteIndex > ttc_gfx_Quick.Config->PaletteSize )
    { PaletteIndex = 1; }    // map all unknown colors to default foreground color

    ttc_gfx_color_bg24( ttc_gfx_Quick.Config->Palette[PaletteIndex] );
}
void                ttc_gfx_color_fg_palette( t_u8 PaletteIndex ) {
    Assert_GFX( ttc_gfx_Quick.Config, ttc_assert_origin_auto ); // global variable not set: did you call ttc_gfx_init() before calling this function?

    if ( PaletteIndex > ttc_gfx_Quick.Config->PaletteSize )
    { PaletteIndex = 1; }    // map all unknown colors to default foreground color

    ttc_gfx_color_fg24( ttc_gfx_Quick.Config->Palette[PaletteIndex] );
}
void                ttc_gfx_palette_set( const t_u32* Palette24, t_u8 Size ) {
    Assert_GFX( ttc_memory_is_readable( Palette24 ), ttc_assert_origin_auto );
    Assert_GFX( Size >= 2, ttc_assert_origin_auto ); // need at least two colors
    Assert_GFX( ttc_gfx_Quick.Config, ttc_assert_origin_auto ); // global variable not set: did you call ttc_gfx_init() before calling this function?

    ttc_gfx_Quick.Config->Palette     = Palette24;
    ttc_gfx_Quick.Config->PaletteSize = Size;
}
void                ttc_gfx_backlight( t_u8 Level ) {}
void                ttc_gfx_switch_to( t_u8 LogicalIndex ) {

    t_ttc_gfx_config* Config = ttc_gfx_get_configuration( LogicalIndex );

    if ( ttc_gfx_Quick.Config != Config ) { // switching to different display
        ttc_gfx_deselect( ttc_gfx_Quick.Config ); // deselect previous display

        // update quick access data
        ttc_gfx_Quick.Config         = Config;
        ttc_gfx_Quick.Font           = Config->Font;
        ttc_gfx_Quick.Width          = Config->Width;
        ttc_gfx_Quick.Height         = Config->Height;
        ttc_gfx_Quick.PhysicalWidth  = Config->PhysicalWidth;
        ttc_gfx_Quick.PhysicalHeight = Config->PhysicalHeight;

        ttc_gfx_select( ttc_gfx_Quick.Config ); // select new display to accept commands
    }
}
//String operations

#ifdef EXTENSION_ttc_font  // font handling functions

#include "ttc_string.h"

void                ttc_gfx_text( const void* String, t_base MaxSize ) {
    Assert_GFX( ttc_gfx_Quick.Font, ttc_assert_origin_auto ); // no font has been registered/ activated!
    t_u8* Reader = ( t_u8* ) String;

    while ( *Reader && MaxSize-- ) {
        ttc_gfx_char( *Reader++ );
        ttc_gfx_text_cursor_right();
    }
}
void                ttc_gfx_text_solid_at( t_u16 X, t_u16 Y, const void* String, t_base MaxSize ) {
    ttc_gfx_text_cursor_set( X, Y );
    ttc_gfx_text_solid( String, MaxSize );
}
void                ttc_gfx_text_at( t_u16 X, t_u16 Y, const void* String, t_base MaxSize ) {
    ttc_gfx_text_cursor_set( X, Y );
    ttc_gfx_text( String, MaxSize );
}
void                ttc_gfx_text_solid( const void* String, t_base MaxSize ) {
    Assert_GFX( ttc_gfx_Quick.Font, ttc_assert_origin_auto ); // no font has been registered/ activated!
    t_u8* Reader = ( t_u8* ) String;

    while ( *Reader && MaxSize-- ) {

        ttc_gfx_char_solid( *Reader++ );
        ttc_gfx_text_cursor_right();
    }
}
void                ttc_gfx_text_boxed( const void* String, t_base MaxSize, t_u8 InnerSpacing ) {
    Assert_GFX( ttc_gfx_Quick.Font, ttc_assert_origin_auto ); // no font has been registered/ activated!
    Assert_GFX( ttc_gfx_Quick.Config, ttc_assert_origin_auto ); // global variable not set: did you call ttc_gfx_init() before calling this function?
    MaxSize = 0;
    static t_u16 tgpb_StartX; tgpb_StartX = ttc_gfx_Quick.Config->X;
    static t_u16 tgpb_StartY; tgpb_StartY = ttc_gfx_Quick.Config->Y;
    MaxSize = ttc_string_length8( String, ttc_gfx_Quick.Config->TextColumns );
    ttc_gfx_cursor_set( tgpb_StartX, tgpb_StartY );

    ttc_gfx_text( String, MaxSize );
    ttc_gfx_rect( tgpb_StartX - InnerSpacing,
                  tgpb_StartY - InnerSpacing,
                  2 * InnerSpacing + MaxSize * ttc_gfx_Quick.Font->CharWidth,
                  2 * InnerSpacing + ttc_gfx_Quick.Font->CharHeight
                );

    ttc_gfx_cursor_set( ttc_gfx_Quick.Config->X + 2 * InnerSpacing, tgpb_StartY );
}
void                ttc_gfx_text_block( t_ttc_heap_block* Block ) {
    Assert_GFX( Block,         ttc_assert_origin_auto );
    Assert_GFX( Block->Buffer, ttc_assert_origin_auto );
    Assert_GFX( ttc_gfx_Quick.Font, ttc_assert_origin_auto ); // no font has been registered/ activated!

    static TTC_MEMORY_BLOCK_BASE tgpb2_Amount; tgpb2_Amount = Block->Size;
    static t_u8* tgpb2_Reader;                 tgpb2_Reader = Block->Buffer;

    while ( tgpb2_Amount-- ) {
        ttc_gfx_char( *tgpb2_Reader++ );
        ttc_gfx_text_cursor_right();
    }
}
void                ttc_gfx_text_block_solid( t_ttc_heap_block* Block ) {
    Assert_GFX( Block,         ttc_assert_origin_auto );
    Assert_GFX( Block->Buffer, ttc_assert_origin_auto );
    Assert_GFX( ttc_gfx_Quick.Font, ttc_assert_origin_auto ); // no font has been registered/ activated!

    static TTC_MEMORY_BLOCK_BASE tgpbs_Amount; tgpbs_Amount = Block->Size;
    static t_u8* tgpbs_Reader;                 tgpbs_Reader = Block->Buffer;

    while ( tgpbs_Amount-- ) {
        ttc_gfx_char_solid( *tgpbs_Reader++ );
        ttc_gfx_text_cursor_right();
    }
}
void                ttc_gfx_set_font( const t_ttc_font_data* Font ) {
    Assert_GFX( ttc_gfx_Quick.Config, ttc_assert_origin_auto ); // global variable not set: did you call ttc_gfx_init() before calling this function?

    ttc_gfx_Quick.Font = ttc_gfx_Quick.Config->Font = Font;
    ttc_gfx_Quick.Config->TextColumns  = ( ttc_gfx_Quick.Config->Width  - ttc_gfx_Quick.Config->TextBorderLeft ) / Font->CharWidth;
    ttc_gfx_Quick.Config->TextRows     = ( ttc_gfx_Quick.Config->Height - ttc_gfx_Quick.Config->TextBorderTop ) / Font->CharHeight;

    // calculate borders of text display for current font
    ttc_gfx_Quick.Config->TextBorderRight = ttc_gfx_Quick.Config->TextBorderLeft + ttc_gfx_Quick.Config->TextColumns  * ttc_gfx_Quick.Config->Font->CharWidth;
    ttc_gfx_Quick.Config->TextBorderBottom = ttc_gfx_Quick.Config->TextBorderTop + ttc_gfx_Quick.Config->TextRows     * ttc_gfx_Quick.Config->Font->CharHeight;

    _driver_gfx_set_font( Font );

    // ensure that new font is updated in current display
    ttc_gfx_switch_to( ttc_gfx_Quick.Config->LogicalIndex );
}
void                ttc_gfx_text_clear( t_u16 AmountCharacters ) {
    Assert_GFX( ttc_gfx_Quick.Font,   ttc_assert_origin_auto ); // no font has been registered/ activated!
    Assert_GFX( ttc_gfx_Quick.Config, ttc_assert_origin_auto ); // global variable not set: did you call ttc_gfx_init() before calling this function?

    if ( AmountCharacters == ( t_u16 ) - 1 ) // calculate amount of characters until end of row
    { AmountCharacters = ttc_gfx_Quick.Config->TextColumns - ttc_gfx_Quick.Config->TextX; }

    t_u16 ColorFg = ttc_gfx_Quick.Config->ColorFg;

    ttc_gfx_color_fg16( ttc_gfx_Quick.Config->ColorBg );

    ttc_gfx_rect_fill( ttc_gfx_Quick.Config->X,
                       ttc_gfx_Quick.Config->Y,
                       ttc_gfx_Quick.Font->CharWidth * AmountCharacters,
                       ttc_gfx_Quick.Font->CharHeight
                     );

    // restore previous fg color
    ttc_gfx_color_fg16( ColorFg );

}
void                ttc_gfx_text_cursor_set( t_u16 TextX, t_u16 TextY ) {
    Assert_GFX( ttc_gfx_Quick.Config, ttc_assert_origin_auto ); // global variable not set: did you call ttc_gfx_init() before calling this function?

    // keep coordinate within screen
    if ( TextX >= ttc_gfx_Quick.Config->TextColumns )
    { TextX = ttc_gfx_Quick.Config->TextColumns - 1; }
    if ( TextY >= ttc_gfx_Quick.Config->TextRows )
    { TextY = ttc_gfx_Quick.Config->TextRows - 1; }

    // calculate pixel coordinate
    TextX *= ttc_gfx_Quick.Font->CharWidth;
    TextY *= ttc_gfx_Quick.Font->CharHeight;

    TextX += ttc_gfx_Quick.Config->TextBorderLeft;
    TextY += ttc_gfx_Quick.Config->TextBorderTop;

    ttc_gfx_text_cursor_set_precise( TextX, TextY );
}
void                ttc_gfx_text_cursor_set_precise( t_u16 PixelX, t_u16 PixelY ) {
    Assert_GFX( ttc_gfx_Quick.Config, ttc_assert_origin_auto ); // global variable not set: did you call ttc_gfx_init() before calling this function?

    // keep coordinate within screen
    if ( PixelX >= ttc_gfx_Quick.Config->Width )
    { PixelX = ttc_gfx_Quick.Config->Width - 1; }
    if ( PixelY >= ttc_gfx_Quick.Config->Height )
    { PixelY = ttc_gfx_Quick.Config->Height - 1; }

    ttc_gfx_Quick.Config->TextX = ( PixelX - ttc_gfx_Quick.Config->TextBorderLeft ) / ttc_gfx_Quick.Font->CharWidth;
    ttc_gfx_Quick.Config->TextY = ( PixelY - ttc_gfx_Quick.Config->TextBorderTop )  / ttc_gfx_Quick.Font->CharHeight;

    ttc_gfx_Quick.Config->X = PixelX;
    ttc_gfx_Quick.Config->Y = PixelY;

    ttc_gfx_Quick.Config->PhysicalX = ROTATED_X( PixelX, PixelY );
    ttc_gfx_Quick.Config->PhysicalY = ROTATED_Y( PixelX, PixelY );

    ttc_gfx_cursor_set( PixelX, PixelY );
}
void                ttc_gfx_text_cursor_right() {
    Assert_GFX( ttc_gfx_Quick.Config, ttc_assert_origin_auto ); // global variable not set: did you call ttc_gfx_init() before calling this function?

    if ( ttc_gfx_Quick.Config->X >= ttc_gfx_Quick.Config->TextBorderRight ) {
        if ( ttc_gfx_Quick.Config->Y < ttc_gfx_Quick.Config->TextBorderBottom ) { // line-break => begin of next line
            ttc_gfx_Quick.Config->X = ttc_gfx_Quick.Config->TextBorderLeft;
            ttc_gfx_Quick.Config->Y += ttc_gfx_Quick.Font->CharHeight;
        }
        else {                                            // page-break => upper left
            ttc_gfx_Quick.Config->X = ttc_gfx_Quick.Config->TextBorderLeft;
            ttc_gfx_Quick.Config->Y = ttc_gfx_Quick.Config->TextBorderTop;
        }
    }
    else
    { ttc_gfx_Quick.Config->X += ttc_gfx_Quick.Font->CharWidth; }

    ttc_gfx_text_cursor_set_precise( ttc_gfx_Quick.Config->X, ttc_gfx_Quick.Config->Y );
}
void                ttc_gfx_text_cursor_left() {
    Assert_GFX( ttc_gfx_Quick.Config, ttc_assert_origin_auto ); // global variable not set: did you call ttc_gfx_init() before calling this function?

    if ( ttc_gfx_Quick.Config->X <= ttc_gfx_Quick.Config->TextBorderLeft ) {
        if ( ttc_gfx_Quick.Config->Y > ttc_gfx_Quick.Config->TextBorderTop ) { // line-break: back to last character in previous line
            ttc_gfx_Quick.Config->Y -= ttc_gfx_Quick.Font->CharHeight;
        }
        else {                                            // page-break: jump to right bottom position
            ttc_gfx_Quick.Config->Y = ttc_gfx_Quick.Config->TextBorderBottom - ttc_gfx_Quick.Font->CharHeight;
        }
        ttc_gfx_Quick.Config->X = ttc_gfx_Quick.Config->TextBorderRight - ttc_gfx_Quick.Font->CharWidth;
    }
    else
    { ttc_gfx_Quick.Config->TextX -= ttc_gfx_Quick.Font->CharWidth; }

    ttc_gfx_text_cursor_set_precise( ttc_gfx_Quick.Config->X, ttc_gfx_Quick.Config->Y );
}
void                ttc_gfx_text_cursor_up() {
    Assert_GFX( ttc_gfx_Quick.Config, ttc_assert_origin_auto ); // global variable not set: did you call ttc_gfx_init() before calling this function?

    if ( ttc_gfx_Quick.Config->Y > ttc_gfx_Quick.Config->TextBorderTop ) // move one line up
    { ttc_gfx_Quick.Config->Y -= ttc_gfx_Quick.Font->CharHeight; }
    else                                                  // page-break: move to last line
    { ttc_gfx_Quick.Config->Y = ttc_gfx_Quick.Config->TextBorderBottom - ttc_gfx_Quick.Font->CharHeight; }

    ttc_gfx_text_cursor_set_precise( ttc_gfx_Quick.Config->X, ttc_gfx_Quick.Config->Y );
}
void                ttc_gfx_text_cursor_down() {
    Assert_GFX( ttc_gfx_Quick.Config, ttc_assert_origin_auto ); // global variable not set: did you call ttc_gfx_init() before calling this function?

    if ( ttc_gfx_Quick.Config->Y < ttc_gfx_Quick.Config->TextBorderBottom )
    { ttc_gfx_Quick.Config->Y += ttc_gfx_Quick.Font->CharHeight; }     // move one line down
    else
    { ttc_gfx_Quick.Config->Y = ttc_gfx_Quick.Config->TextBorderTop; } // page-break: move to first line

    ttc_gfx_text_cursor_set_precise( ttc_gfx_Quick.Config->X, ttc_gfx_Quick.Config->Y );
}
t_u16               ttc_gfx_text_center( const void* String, t_base MaxSize ) {

    t_s16 StartPos = ( ttc_gfx_Quick.Config->TextColumns - ttc_string_length16( String, MaxSize ) ) / 2;
    if ( StartPos < 0 ) { StartPos = 0; }

    return StartPos;
}
t_u16               ttc_gfx_text_width( const void* Text ) {
    Assert_GFX( ttc_gfx_Quick.Font,   ttc_assert_origin_auto ); // no font has been registered/ activated!
    Assert_GFX( ttc_gfx_Quick.Config, ttc_assert_origin_auto ); // global variable not set: did you call ttc_gfx_init() before calling this function?

    t_u16 AmountCharacters = ttc_string_length16( Text, -1 );

    return AmountCharacters * ttc_gfx_Quick.Font->CharWidth; // ToDo: Exact implementation for proportional fonts
}

// stdout implementation for ttc_string.c
void                ttc_gfx_stdout_set( t_u8 DisplayIndex ) {
    //ttc_gfx_Quick(DisplayIndex);

    ttc_gfx_stdout_index = DisplayIndex;
}
void                ttc_gfx_stdout_print_block( t_ttc_heap_block* Block ) {
    Assert_GFX( ttc_gfx_Quick.Config, ttc_assert_origin_auto ); // global variable not set: did you call ttc_gfx_init() before calling this function?

    if ( ttc_gfx_stdout_index ) {
        if ( ttc_gfx_Quick.Config->LogicalIndex != ttc_gfx_stdout_index )
            //       ttc_gfx_Quick(ttc_gfx_stdout_index); // switch to display registered for stdout

        { ttc_gfx_text_block( Block ); }
    }
}
void                ttc_gfx_stdout_print_string( const t_u8* String, t_base MaxSize ) {

    if ( ttc_gfx_stdout_index ) {
        ttc_gfx_text_solid( ( const void* ) String, MaxSize );
    }
}

#else
// declare dummy functions to avoid compilation issues
void                ttc_gfx_text( const void* String, t_base MaxSize ) {}
void                ttc_gfx_text_between( t_u16 X, t_u16 Y, const void* String, t_base MaxSize ) {}
void                ttc_gfx_text_solid_at( t_u16 X, t_u16 Y, const void* String, t_base MaxSize ) {}
void                ttc_gfx_text_at( t_u16 X, t_u16 Y, const void* String, t_base MaxSize ) {}
void                ttc_gfx_text_solid( const void* String, t_base MaxSize ) {}
void                ttc_gfx_text_boxed( const void* String, t_base MaxSize, t_u8 InnerSpacing ) {}
void                ttc_gfx_text_block( t_ttc_heap_block* Block ) {}
void                ttc_gfx_text_block_solid( t_ttc_heap_block* Block ) {}
void                ttc_gfx_set_font( const t_ttc_font_data* Font ) {}
void                ttc_gfx_text_cursor_set( t_u16 X, t_u16 Y ) {}
void                ttc_gfx_text_cursor_right() {}
void                ttc_gfx_text_cursor_left() {}
void                ttc_gfx_text_cursor_up() {}
void                ttc_gfx_text_cursor_down() {}
t_u16               ttc_gfx_text_center( const void* String, t_base MaxSize ) { return 0; }
void                ttc_gfx_stdout_set( t_u8 DisplayIndex ) {}
void                ttc_gfx_stdout_print_block( t_ttc_heap_block* Block ) {}
void                ttc_gfx_stdout_print_string( const t_u8* String, t_base MaxSize ) { }
void                ttc_gfx_text_clear( t_u16 AmountCharacters ) { }
t_u16               ttc_gfx_text_width( const void* Text ) {

    ( void ) Text;
    return 0; // t_u16
}
#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_gfx(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
