/** ttc_semaphore.c ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Architecture independent support for inter task communication & synchronization.
 *
 */
#include "ttc_semaphore.h"
#include "ttc_systick.h"
#include "ttc_heap.h"

//{ global variables ********************************************

#if TTC_SMART_SEMAPHORE_HISTORY_SIZE > 0

    // stores pointers to smart semaphores given for latest t_ttc_semaphore_smartake() calls
    t_ttc_semaphore_smart* tml_LastSmartSemaphore[TTC_SMART_SEMAPHORE_HISTORY_SIZE];

    // history of smart semaphores without doubles
    t_ttc_semaphore_smart* tml_UniqueSmartSemaphore[TTC_SMART_SEMAPHORE_HISTORY_SIZE];

    // index of next valid entry inside tml_LastSmartSemaphore[]
    t_u8 tml_Index_LastSmartSemaphore = 0;
    t_u8 tml_Index_UniqueSmartSemaphore = 0;

    t_u32 tml_AmountSmartAmounts = 0;
#endif
#ifdef REGRESSION_PIN_QUEUE_PUSH
    // support for regresssion_queue.c
    extern void regression_queue_setPin_QueuePush( BOOL Set );
#endif

//}global variables
//{ functions *************************************************

void ttc_semaphore_wait( t_ttc_semaphore* Semaphore, t_base TimeOut ) {

#ifdef TTC_MULTITASKING_SCHEDULER
    if ( 0 ) { ttc_task_yield(); } // active waiting for semaphore
    else {                   // put task to sleep (will be awaken by _give()/ _give_isr() )  ToDo: Activating this gives strange hardfault crashes and deadlocks!
        ttc_task_critical_begin();
        ttc_task_waitinglist_wait( &( Semaphore->WaitingTasks ), 0 );
        ttc_task_critical_end();
    }
#endif
}

// private functions ********************************************

t_ttc_semaphore* _ttc_semaphore_create() {

#ifdef _driver_semaphore_create
    t_ttc_semaphore* NewSemaphore = ttc_heap_alloc_zeroed( sizeof( t_ttc_semaphore ) );
    _ttc_semaphore_init( NewSemaphore );

    return NewSemaphore;
#else
    return NULL;
#endif
}
void _ttc_semaphore_init( t_ttc_semaphore* Semaphore ) {

    ttc_memory_set( Semaphore, 0, sizeof( t_ttc_semaphore ) );
#ifdef _driver_semaphore_init
    _driver_semaphore_init( &( Semaphore->Value ) );
#endif
}
void _ttc_semaphore_take_endless( t_ttc_semaphore* Semaphore, t_base Amount ) {
    Assert_SEMAPHORE_Writable( Semaphore, ttc_assert_origin_auto );

    ttc_task_critical_begin();
    while ( _driver_semaphore_take_try( &( Semaphore->Value ), Amount ) ) {
#ifdef TTC_MULTITASKING_SCHEDULER
        ttc_task_waitinglist_wait( &( Semaphore->WaitingTasks ), -1 );
#endif
    };
    ttc_task_critical_end();
}
e_ttc_semaphore_error _ttc_semaphore_take( t_ttc_semaphore* SEMAPHORE_VOLATILE Semaphore, t_base Amount, t_base TimeOut ) {
    Assert_SEMAPHORE_Writable( Semaphore, ttc_assert_origin_auto );

#if TTC_SEMAPHORE_TIMEOUT_USECS > 0
    if ( TimeOut == -1 )
    { TimeOut = TTC_SEMAPHORE_TIMEOUT_USECS; }
#endif

    e_ttc_semaphore_error Error = tsme_NotImplemented;
#ifdef _driver_semaphore_take_try
    if ( TimeOut == 0 )
    { return _driver_semaphore_take_try( &( Semaphore->Value ), Amount ); }

    if ( TimeOut == -1 ) { // endless take
        do {
            Error = _driver_semaphore_take_try( &( Semaphore->Value ), Amount );
            if ( Error ) {
#ifdef TTC_MULTITASKING_SCHEDULER
                if ( ttc_task_is_scheduler_started() ) { // sleep on semaphore
                    ttc_task_critical_begin();
                    ttc_task_waitinglist_wait( &( Semaphore->WaitingTasks ), -1 );
                    ttc_task_critical_end();
                }
#endif
            }
        }
        while ( Error );
    }
    else {              // take with timeout
        t_base_signed RemainingTimeOut = TimeOut;
        ttc_task_critical_begin();
        do {
            Error = _driver_semaphore_take_try( &( Semaphore->Value ), Amount );
            if ( Error ) {
#ifdef TTC_MULTITASKING_SCHEDULER
                t_base StartTime = ttc_systick_get_elapsed_usecs();
                ttc_task_waitinglist_wait( &( Semaphore->WaitingTasks ), RemainingTimeOut );
                t_base EndTime = ttc_systick_get_elapsed_usecs();
                RemainingTimeOut = RemainingTimeOut - ( EndTime - StartTime );
#endif
            }
        }
        while ( Error && ( RemainingTimeOut > 0 ) );
        ttc_task_critical_end();
    }
#else
#  warning Not implemented: _driver_semaphore_take_try()
#endif

#if TTC_SEMAPHORE_TIMEOUT_USECS > 0
    if ( Error == tsme_TimeOut ) {
        Assert( TimeOut != TTC_SEMAPHORE_TIMEOUT_USECS, ttc_assert_origin_auto ); // endless semaphore timed out!
    }
#endif

    return Error;
}
e_ttc_semaphore_error _ttc_semaphore_take_try( t_ttc_semaphore* Semaphore, t_base Amount ) {
    Assert_SEMAPHORE_Writable( Semaphore, ttc_assert_origin_auto );

#ifdef _driver_semaphore_take_try
    return _driver_semaphore_take_try( &( Semaphore->Value ), Amount );
#else
    return tsme_NotImplemented;
#endif
}
e_ttc_semaphore_error _ttc_semaphore_take_isr( t_ttc_semaphore* Semaphore, t_base Amount ) {
    Assert_SEMAPHORE_Writable( Semaphore, ttc_assert_origin_auto );
    //X static signed portBASE_TYPE HigherPrioTaskWoken; HigherPrioTaskWoken = FALSE;

#ifdef _driver_semaphore_take_isr
    return _driver_semaphore_take_isr( &( Semaphore->Value ), Amount );
#else
    if ( Semaphore->Value >= Amount ) {
        Semaphore->Value -= Amount; // safe to to simple calculation during isr on many architectures
        return tsme_OK;
    }
    return tsme_SemaphoreExpired;
#endif
}
BOOL _ttc_semaphore_can_provide( t_ttc_semaphore* Semaphore, t_base Amount ) {
    Assert_SEMAPHORE_Writable( Semaphore, ttc_assert_origin_auto );

#ifdef _driver_semaphore_can_provide
    return _driver_semaphore_can_provide( &( Semaphore->Value ), Amount );
#else
    return tsme_NotImplemented;
#endif
}
void _ttc_semaphore_give( t_ttc_semaphore* Semaphore, t_base Amount ) {
    Assert_SEMAPHORE_Writable( Semaphore, ttc_assert_origin_auto );

    ttc_task_critical_begin();

#ifdef _driver_semaphore_give
    _driver_semaphore_give( &( Semaphore->Value ), Amount );
#endif

#ifdef TTC_MULTITASKING_SCHEDULER
    t_ttc_task_waiting_list* WaitingTasks = &( Semaphore->WaitingTasks );
    if ( WaitingTasks->First ) {
        ttc_task_waitinglist_awake( WaitingTasks );
    }
#endif
#ifdef REGRESSION_PIN_QUEUE_PUSH
    regression_queue_setPin_QueuePush( FALSE ); // disable output pin before task switch takes place
#endif
    ttc_task_critical_end();
}
void _ttc_semaphore_give_isr( t_ttc_semaphore* Semaphore, t_base Amount ) {
    Assert_SEMAPHORE_Writable( Semaphore, ttc_assert_origin_auto );

#ifdef _driver_semaphore_give_isr
    _driver_semaphore_give_isr( &( Semaphore->Value ), Amount );
#else
    Semaphore->Value += Amount; // safe to to simple calculation during isr on many architectures
#endif

#ifdef TTC_MULTITASKING_SCHEDULER
    t_ttc_task_waiting_list* WaitingTasks = &( Semaphore->WaitingTasks );
    if ( WaitingTasks->First ) { // a task is waiting on this semaphore: wake it up
        ttc_task_waitinglist_awake( WaitingTasks );
    }
#endif
}

#if TTC_SMART_SEMAPHORE == 1
t_base _ttc_semaphore_smart_available( t_ttc_semaphore_smart* SmartSemaphore ) {
    Assert_SEMAPHORE_Writable( SmartSemaphore, ttc_assert_origin_auto );

    return _driver_semaphore_available( &( SmartSemaphore->Semaphore.Value ) );
}
t_ttc_semaphore_smart* _ttc_semaphore_smart_create() {

    t_ttc_semaphore_smart* NewSemaphore = ttc_heap_alloc( sizeof( t_ttc_semaphore_smart ) );
    ttc_semaphore_init( NewSemaphore );

    return NewSemaphore;
}
void _ttc_semaphore_smart_init( t_ttc_semaphore_smart* Semaphore ) {

    _ttc_semaphore_init( &( Semaphore->Semaphore ) );
    Semaphore->LastCaller = NULL;
}
#if TTC_SMART_SEMAPHORE_HISTORY_SIZE > 0
void _ttc_semaphore_record_history( t_ttc_semaphore_smart* SmartSemaphore, void ( *LastCaller )() ) {

    tml_AmountSmartAmounts++;
    tml_LastSmartSemaphore[tml_Index_LastSmartSemaphore++] = SmartSemaphore;
    if ( tml_Index_LastSmartSemaphore >= TTC_SMART_SEMAPHORE_HISTORY_SIZE )
    { tml_Index_LastSmartSemaphore = 0; }

    BOOL SemaphoreIsKnown = FALSE;
    for ( int I = 0; ( I < tml_Index_UniqueSmartSemaphore ) && ( I < TTC_SMART_SEMAPHORE_HISTORY_SIZE ); I++ ) {
        if ( tml_UniqueSmartSemaphore[I] == SmartSemaphore ) {
            SemaphoreIsKnown = TRUE;
            break;
        }
    }
    if ( ( !SemaphoreIsKnown ) && ( tml_Index_UniqueSmartSemaphore < TTC_SMART_SEMAPHORE_HISTORY_SIZE ) )
    { tml_UniqueSmartSemaphore[tml_Index_UniqueSmartSemaphore++] = SmartSemaphore; }

}
#endif
e_ttc_semaphore_error _t_ttc_semaphore_smartake( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount, t_base TimeOut ) {
    Assert_SEMAPHORE_Writable( SmartSemaphore, ttc_assert_origin_auto );

#if TTC_SMART_SEMAPHORE_HISTORY_SIZE > 0
    _ttc_semaphore_record_history( SmartSemaphore, LastCaller );
#endif
    e_ttc_semaphore_error Error = _ttc_semaphore_take( &( SmartSemaphore->Semaphore ), Amount, TimeOut );
    if ( Error == tsme_OK ) {
#ifdef TTC_REGRESSION
        // copy info about current task into smart semaphore
        SmartSemaphore->LastCallerTask = ttc_task_update_info( NULL );
#endif
        // use return address as reference (next line of code after lock() call )
        SmartSemaphore->LastCaller = __builtin_return_address( 0 );
    }

    return Error;
}
e_ttc_semaphore_error _t_ttc_semaphore_smartake_try( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount ) {
    Assert_SEMAPHORE_Writable( SmartSemaphore, ttc_assert_origin_auto );

#if TTC_SMART_SEMAPHORE_HISTORY_SIZE > 0
    _ttc_semaphore_record_history( SmartSemaphore, LastCaller );
#endif
    e_ttc_semaphore_error Error = _ttc_semaphore_take_try( &( SmartSemaphore->Semaphore ), Amount );
    if ( Error == tsme_OK ) {
#ifdef TTC_REGRESSION
        // copy info about current task into smart semaphore
        SmartSemaphore->LastCallerTask = ttc_task_update_info( NULL );
#endif
        SmartSemaphore->LastCaller = __builtin_return_address( 0 );
    }

    return Error;
}
e_ttc_semaphore_error _t_ttc_semaphore_smartake_isr( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount ) {
    Assert_SEMAPHORE_Writable( SmartSemaphore, ttc_assert_origin_auto );

#if TTC_SMART_SEMAPHORE_HISTORY_SIZE > 0
    _ttc_semaphore_record_history( SmartSemaphore, LastCaller );
#endif
    e_ttc_semaphore_error Error = _ttc_semaphore_take_isr( &( SmartSemaphore->Semaphore ), Amount );
    if ( Error == tsme_OK ) {
#ifdef TTC_REGRESSION
        // copy info about current task into smart semaphore
        SmartSemaphore->LastCallerTask = ttc_task_update_info( NULL );
#endif
        SmartSemaphore->LastCaller = __builtin_return_address( 0 );
    }

    return Error;
}
void _ttc_semaphore_smart_give( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount ) {
    Assert_SEMAPHORE_Writable( SmartSemaphore, ttc_assert_origin_auto );

    if ( 0 ) { SmartSemaphore->LastCaller = NULL; }

    _ttc_semaphore_give( &( SmartSemaphore->Semaphore ), Amount );
}
void _ttc_semaphore_smart_give_isr( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount ) {
    Assert_SEMAPHORE_Writable( SmartSemaphore, ttc_assert_origin_auto );

    if ( 0 ) { SmartSemaphore->LastCaller = NULL; }

    _ttc_semaphore_give_isr( &( SmartSemaphore->Semaphore ), Amount );
}
BOOL _ttc_semaphore_smart_can_provide( t_ttc_semaphore_smart* SmartSemaphore, t_base Amount ) {
    Assert_SEMAPHORE_Writable( SmartSemaphore, ttc_assert_origin_auto );

    return _ttc_semaphore_can_provide( &( SmartSemaphore->Semaphore ), Amount );
}
#endif

//}functions
