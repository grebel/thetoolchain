/** { ttc_layer_phy.c ****************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for layer_phy devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported 
 *  architectures.
 *
 *  See corresponding header file for documentation.
 *
 *  Created from template ttc_device.c revision 27 at 20150304 08:22:38 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_layer_phy.h".
//
#include "ttc_layer_phy.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_LAYER_PHY_AMOUNT == 0
  #warning No LAYER_PHY devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of layer_phy devices.
 *
 */
 

// for each initialized device, a pointer to its generic and spi definitions is stored
A_define(t_ttc_layer_phy_config*, ttc_layer_phy_configs, TTC_LAYER_PHY_AMOUNT);

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8 ttc_layer_phy_get_max_index() {
    return TTC_LAYER_PHY_AMOUNT;
}
t_ttc_layer_phy_config* ttc_layer_phy_get_configuration(t_u8 LogicalIndex) {
    Assert_LAYER_PHY(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_layer_phy_config* Config = A(ttc_layer_phy_configs, LogicalIndex-1); // will assert if outside array bounds

    if (!Config) {
        Config = A(ttc_layer_phy_configs, LogicalIndex-1) = ttc_heap_alloc_zeroed(sizeof(t_ttc_layer_phy_config));
        Config->Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_layer_phy_load_defaults(LogicalIndex);
    }

    return Config;
}
void ttc_layer_phy_deinit(t_u8 LogicalIndex) {
    Assert_LAYER_PHY(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_layer_phy_config* Config = ttc_layer_phy_get_configuration(LogicalIndex);
  
    e_ttc_layer_phy_errorcode Result = _driver_layer_phy_deinit(Config);
    if (Result == ec_layer_phy_OK)
      Config->Flags.Bits.Initialized = 0;
}
e_ttc_layer_phy_errorcode ttc_layer_phy_init(t_u8 LogicalIndex) {
    Assert_LAYER_PHY(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_layer_phy_config* Config = ttc_layer_phy_get_configuration(LogicalIndex);

    e_ttc_layer_phy_errorcode Result = _driver_layer_phy_init(Config);
    if (Result == ec_layer_phy_OK)
      Config->Flags.Bits.Initialized = 1;
    
    return Result;
}
e_ttc_layer_phy_errorcode ttc_layer_phy_load_defaults(t_u8 LogicalIndex) {
    Assert_LAYER_PHY(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_layer_phy_config* Config = ttc_layer_phy_get_configuration(LogicalIndex);

    if (Config->Flags.Bits.Initialized)
      ttc_layer_phy_deinit(LogicalIndex);
    
    ttc_memory_set( Config, 0, sizeof(t_ttc_layer_phy_config) );

    // load generic default values
    switch (LogicalIndex) { // load type of low-level driver for current architecture
#ifdef TTC_LAYER_PHY1_DRIVER
    case  1: Config->Architecture = TTC_LAYER_PHY1_DRIVER; break;
#endif
#ifdef TTC_LAYER_PHY2_DRIVER
    case  2: Config->Architecture = TTC_LAYER_PHY2_DRIVER; break;
#endif
#ifdef TTC_LAYER_PHY3_DRIVER
    case  3: Config->Architecture = TTC_LAYER_PHY3_DRIVER; break;
#endif
#ifdef TTC_LAYER_PHY4_DRIVER
    case  4: Config->Architecture = TTC_LAYER_PHY4_DRIVER; break;
#endif
#ifdef TTC_LAYER_PHY5_DRIVER
    case  5: Config->Architecture = TTC_LAYER_PHY5_DRIVER; break;
#endif
#ifdef TTC_LAYER_PHY6_DRIVER
    case  6: Config->Architecture = TTC_LAYER_PHY6_DRIVER; break;
#endif
#ifdef TTC_LAYER_PHY7_DRIVER
    case  7: Config->Architecture = TTC_LAYER_PHY7_DRIVER; break;
#endif
#ifdef TTC_LAYER_PHY8_DRIVER
    case  8: Config->Architecture = TTC_LAYER_PHY8_DRIVER; break;
#endif
#ifdef TTC_LAYER_PHY9_DRIVER
    case  9: Config->Architecture = TTC_LAYER_PHY9_DRIVER; break;
#endif
#ifdef TTC_LAYER_PHY10_DRIVER
    case 10: Config->Architecture = TTC_LAYER_PHY10_DRIVER; break;
#endif
    default: ttc_assert_halt_origin(ec_layer_phy_InvalidImplementation); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }
    Assert_LAYER_PHY( (Config->Architecture > ta_layer_phy_None) && (Config->Architecture < ta_layer_phy_unknown), ttc_assert_origin_auto); // architecture not set properly! Check makefile for TTC_LAYER_PHY<n>_DRIVER and compare with e_ttc_layer_phy_architecture 

    Config->LogicalIndex = LogicalIndex;
    Config->PhysicalIndex = ttc_layer_phy_logical_2_physical_index(LogicalIndex);
    
    //Insert additional generic default values here ...

    return _driver_layer_phy_load_defaults(Config);    
}
t_u8 ttc_layer_phy_logical_2_physical_index(t_u8 LogicalIndex) {
  
  switch (LogicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_LAYER_PHY1
           case 1: return TTC_LAYER_PHY1;
#endif
#ifdef TTC_LAYER_PHY2
           case 2: return TTC_LAYER_PHY2;
#endif
#ifdef TTC_LAYER_PHY3
           case 3: return TTC_LAYER_PHY3;
#endif
#ifdef TTC_LAYER_PHY4
           case 4: return TTC_LAYER_PHY4;
#endif
#ifdef TTC_LAYER_PHY5
           case 5: return TTC_LAYER_PHY5;
#endif
#ifdef TTC_LAYER_PHY6
           case 6: return TTC_LAYER_PHY6;
#endif
#ifdef TTC_LAYER_PHY7
           case 7: return TTC_LAYER_PHY7;
#endif
#ifdef TTC_LAYER_PHY8
           case 8: return TTC_LAYER_PHY8;
#endif
#ifdef TTC_LAYER_PHY9
           case 9: return TTC_LAYER_PHY9;
#endif
#ifdef TTC_LAYER_PHY10
           case 10: return TTC_LAYER_PHY10;
#endif
      // extend as required
      
      default: break;
    }
    
  Assert_LAYER_PHY(0, ttc_assert_origin_auto); // No TTC_LAYER_PHYn defined! Check your makefile.100_board_* file!
  return -1; // program flow should never go here
}
t_u8 ttc_layer_phy_physical_2_logical_index(t_u8 PhysicalIndex) {
  
  switch (PhysicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_LAYER_PHY1
           case TTC_LAYER_PHY1: return 1;
#endif
#ifdef TTC_LAYER_PHY2
           case TTC_LAYER_PHY2: return 2;
#endif
#ifdef TTC_LAYER_PHY3
           case TTC_LAYER_PHY3: return 3;
#endif
#ifdef TTC_LAYER_PHY4
           case TTC_LAYER_PHY4: return 4;
#endif
#ifdef TTC_LAYER_PHY5
           case TTC_LAYER_PHY5: return 5;
#endif
#ifdef TTC_LAYER_PHY6
           case TTC_LAYER_PHY6: return 6;
#endif
#ifdef TTC_LAYER_PHY7
           case TTC_LAYER_PHY7: return 7;
#endif
#ifdef TTC_LAYER_PHY8
           case TTC_LAYER_PHY8: return 8;
#endif
#ifdef TTC_LAYER_PHY9
           case TTC_LAYER_PHY9: return 9;
#endif
#ifdef TTC_LAYER_PHY10
           case TTC_LAYER_PHY10: return 10;
#endif
      // extend as required
      
      default: break;
    }
    
  Assert_LAYER_PHY(0, ttc_assert_origin_auto); // No TTC_LAYER_PHYn defined! Check your makefile.100_board_* file!
  return -1; // program flow should never go here
}
void ttc_layer_phy_prepare() {
  // add your startup code here (Singletasking!)
  _driver_layer_phy_prepare();
}
void ttc_layer_phy_reset(t_u8 LogicalIndex) {
    Assert_LAYER_PHY(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_layer_phy_config* Config = ttc_layer_phy_get_configuration(LogicalIndex);

    _driver_layer_phy_reset(Config);    
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_layer_phy(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
