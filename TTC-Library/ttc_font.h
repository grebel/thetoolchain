/*{ ttc_font.h ************************************************

   I'M A TEMPLATE PLEASE CHANGE ME!

   Structures, Enums and Defines being required by high-level driver and application.

}*/

#ifndef TTC_FONT_H
#define TTC_FONT_H

//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "ttc_font_types.h"

#ifdef EXTENSION_ttc_font__font_type1_16x24
    #include "font_type1_16x24.h"
#endif

//} Includes
//{ Defines/ TypeDefs ****************************************************

//} Defines
//{ Structures/ Enums ****************************************************

// Structures and Enums required by High-Level driver only go here...

//} Structures/ Enums
//{ Function prototypes **************************************************

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_font_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_usart_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
const t_ttc_font_data* ttc_font_get_configuration( t_u8 LogicalIndex );

/** initialize fonts for usage
 *
 */
void ttc_font_init();

// additional prototypes go here...

//} Function prototypes
//{ private Function prototypes ******************************************

//}PrivateFunctions
//{ Macros ***************************************************************

//}Macros

#endif //TTC_FONT_H
