/** { ttc_timer.h *******************************************************

                           The ToolChain
                           
   High-Level interface for TIMER device.

   Structures, Enums and Defines being required by high-level timer and application.

   Authors: Francisco Estevez
   
}*/

#ifndef TTC_TIMER_H
#define TTC_TIMER_H

//{ Includes *************************************************************

#include "ttc_basic.h"
#include "ttc_memory.h"
#include "ttc_task.h"
#include "ttc_interrupt.h"
#include "interfaces/ttc_timer_interface.h"

#include "ttc_timer_types_old.h"

//} Includes
//{ Check for missing low-level implementations **************************

// Note: functions declared below must be redefined to implementations provided by low-level Driver!

/** Prepares timer Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_timer_prepare();

//}
//{ Defines/ TypeDefs ****************************************************

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level timer only go here...

//} Structures/ Enums
//{ Function prototypes **************************************************

//  Basic timer operations ********************************************

/** initialize the user timer library
 *
 */
void ttc_timer_prepare();

//  Software timer operations ********************************************

/** Concept of software timer
 *
 * Software timers are relaxed-time condition timer. These timers are indicated
 * for general applications. These timers are based on the main clock of the
 * system and use time functions to calculate the time difference.
 *
 * These timers have a miliseconds accuracy, and values should be expressed in mSecs
 *
 */

/** initialize the software timer library
 *
 */
void ttc_software_timer_init();

/** remove the specified timer, releasing the allocated memory space
 *
 * @param EventToDelete         Event to remove from the timer list
 *
 */
void ttc_software_timer_deinit(ttc_time_event_t* EventToDelete);

/** reset the user timer library, deleting the task list and releasing all the allocated memory space
 *
 */
void ttc_software_timer_reset();

/** Create a new software timer. This timer is recommended for applications without real-time requirements
 *
 * @param Function          function
 * @param Argument          passed as argument to Function()
 * @param TimePeriod        time when the callback function is called expressed in miliseconds
 * @param Continuos         == TRUE: When is a periodically task; == FALSE: Task is executed just one time
 *
 * @return                  == NULL: event has not created, otherwise returns the event created
 */
ttc_time_event_t* ttc_software_timer_set(void TaskFunction(void*), void* Argument, Base_t TimePeriod, bool Continuous);

// Hardware timer operations ********************************************

/** Concept of hardware timer
 *
 * Hardware timers are high-precission timers. These timer run on dedicated hardware
 * components and they are not affected by program flow. There are two possibilities
 * to use these timers: using interruptions and controlling the counter by yourself.
 *
 * 1. The using of interruptions have a delay between a timer expires and the callback
 *    function is executed. This delay is minimum but should be considered.
 *
 * 2. Controlling the counter by yourself gives you another way to operate with timers.
 *    This method basically gives you the possibility to read the counter and decide
 *    what do you want to do when this counter reachs a certain value.
 *
 * Both ways need to choose a time-scale. The time-scale should be choosen between
 * miliseconds and microseconds.
 * Microseconds scale goes from 1uSec to 6 mSecs.
 * Miliseconds scale goes from 1mSec to 33 Secs.
 *
 * If you need higher values, please consider to use software timers.
 *
 */

/** initialize the hardware timer library
 *
 */
ttc_timer_errorcode_e ttc_hardware_timer_init();

/** Create a new hardware timer. This timer is recommended for applications with real-time requirements
 *  This function runs directly on the timer conunter register (TIMx->CNT).
 *  The control of this counter should be made it by the user, reading the status of the timer
 *
 * @param TimerIndex        The Index of the timer on our hardware (TIM1...TIM17)
 * @param Function          CallBack Function that will be executed
 * @param Argument          passed as argument to Function()
 * @param TimePeriod        time when the callback function is called (declared in mSecs)
 * @param Scale             time scale used on the timer. 0: uSecs scale, 1: mSecs scale
 *
 * @return                  == NULL: event has not created, otherwise returns the event created
 */
void ttc_hardware_timer_set(u8_t TimerIndex, void TaskFunction(void*), void* Argument, Base_t TimePeriod, u8_t Scale);

/** Create a new hardware timer. This timer is recommended for applications with real-time requirements
 *  This function runs directly on the timer conunter register (TIMx->CNT).
 *  This function executes a callback function via interrupt routine.
 *
 * @param TimerIndex        The Index of the timer on our hardware (TIM1...TIM17)
 * @param Function          CallBack Function that will be executed
 * @param Argument          passed as argument to Function()
 * @param TimePeriod        time when the callback function is called (declared in nano Seconds)
 * @param Scale             time scale used on the timer. 0: uSecs scale, 1: mSecs scale
 *
 * @return                  == NULL: event has not created, otherwise returns the event created
 */
void ttc_hardware_timer_set_isr(u8_t TimerIndex, void TaskFunction(void*), void* Argument, Base_t TimePeriod, u8_t Scale);

/** reset a hardware timer, reseting the information stored on the data structure
 *
 * @param Config        Pointer to the Configuration structure
 *
 */
void ttc_timer_hardware_reset(ttc_timer_config_t* Config);

/** stop the hardware timer, maintaining the information about the last execution
 *
 * @param TimerIndex        Index of the hardware timer
 *
 */
void ttc_timer_hardware_stop(u8_t TimerIndex);

/** start the hardware timer. It is necessary to initiliase before!
 *
 * @param TimerIndex        Index of the hardware timer
 *
 */
void ttc_timer_hardware_start(u8_t TimerIndex);

/** set the counter value on the hardware timer
 *
 * @param TimerIndex        Index of the hardware timer
 * @param Value             Time to load on the timer counter
 * @param TimeScale         0: uSecond scale, 1: mSecond scale
 *
 */
void ttc_timer_hardware_set_counter(u8_t TimerIndex, u16_t Value, u8_t TimeScale);

/** get the counter value of the hardware timer
 *
 * @param TimerIndex        Index of the hardware timer
 *
 */
u16_t ttc_timer_hardware_get_counter(u8_t TimerIndex);

/** get the status value of the hardware timer (TIMx->SR)
 *
 * @param TimerIndex        Index of the hardware timer
 *
 */
ttc_timer_statuscode_e ttc_hardware_timer_get_state(u8_t TimerIndex);

//} Function prototypes
//{ private Function prototypes ******************************************

//  Software timer operations ********************************************

/**
 *
 * Note: This function controls the timer list
 *
*/
void _ttc_software_timer_();

/**
 *
 * Note: This function executes an event and controls several timer options
 *
*/
void _ttc_software_timer_check_next_event_();

/**
 *
 * Note: This function inserts an event in the right position on pending timer list
 *
 * @param Timer         New event to insert to the pending timer list
 *
*/
void _ttc_software_timer_insert_event_(ttc_time_event_t* Timer);

// Hardware timer operations ********************************************

/**
 *
 * Note: This private function creates the timer and initializes the interruption for mSecs
 *
 * @param Config        Pointer to the Configuration structure
 * @param InterruptFlag Flag to declare and activate the interruptions
 *
*/
void _ttc_hardware_timer_create(ttc_timer_config_t* Config, bool InterruptFlag);

/**
 *
 * Note: This private function manages the hardware timer interruption
 *
 * @param Index           Index of the hardware timer
 * @param Argument        Pointer to the Configuration structure
 *
*/
void _ttc_hardware_timer_manage_(u8_t Index, void* Argument);
//}private functions
//{ Macros ***************************************************************

//}Macros

#endif //TTC_TIMER_H
