/*{ extensions.c *********************************************************
 *
 *               TheToolChain
 *
 * This code manages activated source-code extensions
 * written 2010-2018 by Gregor Rebel
 *
 * Basic framework template for
 *   - Olimex STM32-P107/ STM32-H107/ STM32-LCD eval board
 *   - StdPeripheralsLibrary CMSIS V3.4.0 (tested)
 *   - GNU toolchain from CodeSourcery G++
 *   - FreeRTOS v7.0.1
 *   - FixPointLib
 *   - uIP TCP/IP-Stack
 *   - Color LCD Panel 320x240
 *   - local/ remote debugging (JTAG-Adapter may be connected to different computer)
 *
 * Coding-style
 *  + gnu99
 *    It's year 2012, get a decent compiler if your's does not comply with this.
 *    arm-none-eabi-gcc-4.4.1 does support gnu99.
 *
 *  + Text Folding
 *    This source makes use of text folding to improve hierarchical readability
 *    It has been written with jEdit + plugin Configurable Fold Handler.
 *    Activate folding in your favourite text-editor for { and }
 *    (fold-start and -end) for best visual experience.
 *
}*/
//{ Includes *************************************************************

#include "ttc_extensions.h"
#include "ttc_assert_types.h"
//} Includes
//{ Global Variables *****************************************************

struct FirstTaskArguments TaskArguments;

extern t_base _sdata;       /* start address for the .data section. defined in linker script */
extern t_base _edata;       /* end address for the .data section. defined in linker script */

extern t_base _sbss;        /* start address for the .bss section. defined in linker script */
extern t_base _ebss;        /* end address for the .bss section. defined in linker script */

//} Global Variables
//{ Function definitions *************************************************

/** Last single task start function being called before multitasking scheduler is started.
 *
 * This function is typically implemented in main.c.
 * If your project lacks this function, simply copy it from a newer project or add it manually:
 *
 *
 * void main_prepare() {
 *    // If this function does not return, multitasking will not be available!
 * }
 */
extern void main_prepare();

void ttc_extensions_start() {

    // check some static settings
    Assert( ( ( ( t_base ) &_edata ) > ( ( t_base ) &_sdata ) ), ttc_assert_origin_auto ); // error in linker script (.data section seems to be empty!)
    Assert( ( ( ( t_base ) &_ebss )  > ( ( t_base ) &_sbss ) ),  ttc_assert_origin_auto ); // error in linker script (.bss  section seems to be empty!)

    /* Prepare all active extensions one after another in single tasking mode.
     * This sequential startup is fast, deterministic and debuggable (in contrast to multitasking mode).
     * Each _prepare() function can use the big global stack instead of the small task stacks.
     *
     * ttc_extensions_prepare() is to be found in file extensions.active/ttc_extensions_active.c.
     * This file is created dynamically by _/compile.sh.
     */
    ttc_extensions_prepare();

    // last function to run before multitasking is enabled
    main_prepare();

    // start multitasking scheduler
    ttc_task_start_scheduler();
}

//}FunctionDefinitions
