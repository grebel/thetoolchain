#ifndef TTC_USB_TYPES_H
#define TTC_USB_TYPES_H

/*{ TTC_USB_types.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Basic defines, enums and structures.
 *
 * Include this file from 
 * architecture independent header files (ttc_XXX.h) and 
 * architecture depend      header files (e.g. stm32_XXX.h)
 * 
}*/
#include "ttc_basic_types.h"
//? #include "ttc_queue.h"
//? #include "ttc_memory.h"

//{ Defines/ TypeDefs ****************************************************

#ifndef TTC_ASSERT_USB    // any previous definition set (Makefile)?
#define TTC_ASSERT_USB 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_USB == 1)  // use Assert()s in USB code (somewhat slower but alot easier to debug)
  #define Assert_USB(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in USB code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_USB(Condition, ErrorCode)
#endif

//  defines that may be overridden in makefile via "COMPILE_OPTS += -D..."
#ifndef TTC_USB_SIZE_QUEUE_RECEIVE
  #define TTC_USB_SIZE_QUEUE_RECEIVE 3  // max amount of memory blocks that can be stored in advance as receive buffers
#endif

#ifndef TTC_USB_SIZE_QUEUE_TRANSMIT
  #define TTC_USB_SIZE_QUEUE_TRANSMIT 5  // max amount of memory blocks that can be stored in advance for transmit
#endif

// TTC_USBn has to be defined as constant by makefile.100_board_*

#define TTC_AMOUNT_USBS 1


//}
// capacity of each memory block (bytes)
#ifndef TTC_USB_BLOCK_SIZE
#define TTC_USB_BLOCK_SIZE 20
#endif

// maximum allocated amount of memory buffers for all USBs
#ifndef TTC_USB_MAX_MEMORY_BUFFERS
#define TTC_USB_MAX_MEMORY_BUFFERS  TTC_AMOUNT_USBS * 5
#endif


//} Defines
//{ constants to be defined by architecture driver ***********************

//}constants
//{ Structures/ Enums  ***************************************************

typedef enum {   // ttc_usb_physical_index_e
    INDEX_USB1,
    INDEX_USB2,
    INDEX_USB3,
    INDEX_USB4,
    INDEX_USB5
} ttc_usb_physical_index_e;

typedef enum {   // ttc_usb_errorcode_e
    tusbe_OK,                // =0: no error
    tusbe_TimeOut,           // timeout occured in called function
    tusbe_DeviceNotFound,    // adressed USART device not available in current uC
    tusbe_NotImplemented,    // function has no implementation for current architecture
    tusbe_InvalidArgument,   // general argument error
    tusbe_InvalidBaudRate,   // given baud rate not suitable for adressed USART
    tusbe_InvalidWordSize,   // given word size not suitable for adressed USART
    tusbe_InvalidStopBits,   // given amount of stop bits not suitable for adressed USART
    tusbe_TxBufferFull,      // given data could not be stored
    tusbe_RxQueueLimited,    // given rx-buffer could not be stored because receive queue cannot hold more entries
    tusbe_No_ControlParity,  // feature not available: check incoming parity bit
    tusbe_No_Receive,        // feature not available: activate receiver
    tusbe_No_RtsCts,         // feature not available: Hardware Flow Control
    tusbe_No_SingleWire,     // feature not available: single-wire half-duplex
    tusbe_No_SmartCard,      // feature not available: SmartCard mode
    tusbe_No_Synchronous,    // feature not available: Synchronous Communication
    tusbe_No_Transmit,       // feature not available: activate transmitter
    tusbe_No_TransmitParity, // feature not available: transmit parity bit
    tusbe_InvalidPinConfig,  // one or more pins have been configured incorrectly -> check makefile for ttc_usbxxx defines
    tusbe_DeviceNotReady,    // choosen device has not been initialized properly

    tusbe_UnknownError
} ttc_usb_errorcode_e;

typedef struct { // ttc_usb_Error_t
    unsigned Overrun  : 1;
    unsigned Noise    : 1;
    unsigned Framing  : 1;
    unsigned Parity   : 1;
    unsigned reserved : 4;

} ttc_usb_Error_t;

typedef struct ttc_usb_generic_s { // architecture independent configuration data of single USART

    // Note: Write-access to this structure is only allowed before first ttc_usb_init() call!
    //       After initialization, only read accesses are allowed for high-level code.

    u8_t LogicalIndex;               // automatically set: logical index of USART to use (1 = ttc_usb1, ...)

    // function being called to parse incoming packet
    void (*RxFunction_UCB)(u8_t* data_buffer, u8_t Nb_bytes);

    // function being called to observe each outgoing packet
    void (*TxFunction_UCB)(u8_t *, u8_t);


} __attribute__((__packed__)) ttc_usb_generic_t;


//}

#endif // ttc_usb_TYPES_H
