#ifndef ttc_EEPROM_EMULATION_H
#define ttc_EEPROM_EMOULATION_H

/*{ ttc_eeprom_emulation.h **********************************************************
 
                      The ToolChain
                      
   Device independent support for 
   a virtual EEPROM
   
   Currently implemented architectures: stm32

   
   written by Sascha Poggemann 2013
   
*/
//{ Defines/ TypeDefs ****************************************************

//} Defines

//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#ifdef EXTENSION_400_stm32f1xx_eeprom_emulation
#include "stm32/stm32_eeprom_emulation.h"
#else
#warning No EEPROM emulation implementation for current architecture
#endif
//} Includes
//{ Structures/ Enums ****************************************************

//} Structures/ Enums
//{ Global Variables *****************************************************



//} Global Variables
//{ Function prototypes **************************************************

void ttc_eeprom_emulation_init(void);

//} Function prototypes

#endif //ttc_EEPROM_EMOULATION_H
