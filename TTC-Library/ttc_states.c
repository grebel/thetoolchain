/** { ttc_states.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for states devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Most functions of this driver are defined as macros and being implemented
 *  in interfaces/ttc_states_interface.c or in low-level drivers states/states_*.c.
 *
 *  See corresponding ttc_states.h for documentation of all high-level functions.
 *
 *  Created from template ttc_device.c revision 42 at 20180126 14:53:05 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_states.h".
//
#include "ttc_states.h"
#include "ttc_heap.h"
#ifdef EXTENSION_ttc_interrupt
    #include "ttc_interrupt.h"
#endif
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of states devices.
 *
 */


// for each initialized device, a pointer to its generic and architecture specific definitions is stored
// Single linked list of dynamically created statemachines
t_ttc_states_config* ttc_states_Configs = NULL;

// Amount of currently allocated statemachines
t_u8                 ttc_states_AmountConfigs = 0;

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ********************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_states(t_ttc_states_config* Config)

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * @param LogicalIndex    1.. logical index of STATES device. Each logical device <n> is defined via TTC_STATES<n>* constants in compile_options.h and extensions.active/makefile
 */
void _ttc_states_configuration_check( t_u8 LogicalIndex );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

t_ttc_states_config*   ttc_states_create() {

    return ttc_states_get_configuration( ttc_states_get_max_index() + 1 );
}
t_u8                   ttc_states_get_max_index() {
    return ttc_states_AmountConfigs;
}
t_ttc_states_config*   ttc_states_get_configuration( t_u8 LogicalIndex ) {
    Assert_STATES( LogicalIndex > 0, ttc_assert_origin_auto ); // logical index starts at 1

    t_u8 Count = LogicalIndex;
    t_ttc_states_config*  Config   = ttc_states_Configs;
    t_ttc_states_config** Previous = &ttc_states_Configs;
    while ( Count != 0 ) {
        if ( !Config ) {
#ifdef EXTENSION_ttc_interrupt
            ttc_interrupt_all_disable();                      // ensure that we are not interrupted while creating configuration
#endif
            Config = ttc_heap_alloc_zeroed( sizeof( t_ttc_states_config ) );
            *Previous = Config;  // store pointer to new configuration in previous element
            ttc_states_load_defaults( LogicalIndex );

#ifdef EXTENSION_ttc_interrupt
            ttc_interrupt_all_enable();
#endif
        }
        Count--;
    }

    Assert_STATES_EXTRA_Writable( Config, ttc_assert_origin_auto ); // memory corrupted?
    return Config;
}
void                   ttc_states_deinit( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration

    t_ttc_states_config* Config = ttc_states_get_configuration( LogicalIndex );

    //..
    ( void ) Config;

    ttc_task_critical_end(); // other tasks now may access this driver
}
e_ttc_states_errorcode ttc_states_init( t_ttc_states_config* Config ) {
    Assert_STATES_Writable( Config, ttc_assert_origin_auto ); // configuration must reside in RAM

    t_u8 LogicalIndex = Config->LogicalIndex;
    Assert_STATES( Config == ttc_states_get_configuration( LogicalIndex ), ttc_assert_origin_auto ); // given pointer seems to be invalid. Did you pass a pointer being returned by ttc_states_create() or ttc_states_get_configuration()?

    ttc_task_critical_begin(); // avoid concurrent access to driver configuration
#if (TTC_ASSERT_STATES_EXTRA == 1)
    t_u8 InitialCriticalLevel = ttc_interrupt_critical_level();
#endif

    // check configuration to meet all limits of current architecture
    _ttc_states_configuration_check( LogicalIndex );

    if ( ( Config->Init.Stack_Size > 0 ) &&  // stack size given
            ( Config->Stack_States == NULL ) // no stack allocated so far
       ) { // allocate memory block for state stack
        Config->Stack_States = ttc_heap_alloc( sizeof( TTC_STATES_TYPE ) * Config->Init.Stack_Size );
    }

    if ( ! Config->LastError ) // == 0
    { Config->Flags.Bits.Initialized = 1; }

    Assert_STATES_EXTRA( InitialCriticalLevel == ttc_interrupt_critical_level(), ttc_assert_origin_auto ); // amount of  ttc_task_critical_end() calls does not match. Check driver implementation!
    ttc_task_critical_end(); // other tasks now may access this driver

    return Config->LastError;
}
TTC_STATES_TYPE*       ttc_states_history( t_u8 LogicalIndex ) {
    t_ttc_states_config* Config = ttc_states_get_configuration( LogicalIndex );

#if TTC_STATES_AMOUNT_RECORDS > 0

    while ( Config->Index_State_History < TTC_STATES_AMOUNT_RECORDS - 1 ); { // rotate right buffer until oldest entry is at last index
        t_u8 Carry = Config->State_History[TTC_STATES_AMOUNT_RECORDS - 1];
        for ( t_u8 Index = TTC_STATES_AMOUNT_RECORDS - 1; Index > 0; Index-- )
        { Config->State_History[Index] = Config->State_History[Index - 1]; }
        Config->State_History[0] = Carry;
        Config->Index_State_History++;
    }

    // zero terminate array
    Config->State_History[TTC_STATES_AMOUNT_RECORDS - 1] = 0;
    return Config->State_History;
#endif

    return 0;
}
e_ttc_states_errorcode ttc_states_load_defaults( t_u8 LogicalIndex ) {
    t_ttc_states_config* Config = ttc_states_get_configuration( LogicalIndex );

    if ( Config->Flags.Bits.Initialized ) {
        ttc_states_deinit( LogicalIndex );
    }

    ttc_memory_set( Config, 0, sizeof( t_ttc_states_config ) );

    // load generic default values
    Config->LogicalIndex = LogicalIndex;

    //Insert additional generic default values here ...

    // Check mandatory configuration items

    return Config->LastError;
}
void                   ttc_states_monitor( volatile t_ttc_states_config* Config, volatile TTC_STATES_TYPE State_Current, volatile TTC_STATES_TYPE State_Previous, volatile e_ttc_states_transition Transition ) {
    ( void ) Config;
    ( void ) State_Current;
    ( void ) State_Previous;
    ( void ) Transition;

    State_Current = State_Current; // place breakpoint here to see every state transition!
}
void                   ttc_states_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)

}
void                   ttc_states_reset( t_u8 LogicalIndex ) {
    t_ttc_states_config* Config = ttc_states_get_configuration( LogicalIndex );

    Config->State_Current = 0;
    ttc_states_switch( Config, 0 );
}
void                   ttc_states_run( t_ttc_states_config* Config ) {
    Assert_STATES_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
#if (TTC_ASSERT_STATES == 1)
    Assert_STATES( Config->Flags.Bits.AlreadyRunning == 0, ttc_assert_origin_auto ); // Calling ttc_states_run() from ttc_states_run() is not allowed. Did you call ttc_states_run() from a state? Check your statemachin implementation!
    Config->Flags.Bits.AlreadyRunning = 1;
#endif
    TTC_STATES_TYPE  State_Current = Config->State_Current;
    if ( State_Current == 0 ) { // first run after reset: jump to initial state
        State_Current = Config->State_Current = Config->State_Initial;
        Config->Transition = states_transition_Switch;
    }

    const t_ttc_states_state* State = Config->Init.AllStates[State_Current - Config->State_Initial];
    Assert_STATES_Readable( State->StateFunction, ttc_assert_origin_auto ); // Invalid function pointer. Memory corruption?
    Assert_STATES( State_Current <= Config->State_LastValid, ttc_assert_origin_auto ); // Invalid state value. Did someone corrupt our configuration?

    e_ttc_states_transition Transition;
    if ( State_Current != Config->State_Previous ) {
        Transition = Config->Transition;
        Assert_STATES( Transition  !=  states_transition_None, ttc_assert_origin_auto ); // a transition happened but was not recorded. Maybe memory corrupted? Check implementation!
    }
    else
    { Transition = states_transition_None; }

    // run the current state
#if (TTC_ASSERT_STATES == 1)
    if ( Transition && Config->Init.MonitorTransition ) {
        Assert_STATES_Readable( Config->Init.MonitorTransition, ttc_assert_origin_auto ); // must point to a readable (and executable) memory. Memory corrupted? Check your Configuration!
        Config->Init.MonitorTransition( Config, Config->Init.StateData, State_Current, Config->State_Previous, Transition );
    }
#endif
    State->StateFunction( Config, Config->Init.StateData, Transition );
    Config->State_Previous = State_Current;
#if (TTC_ASSERT_STATES == 1)
    Config->Flags.Bits.AlreadyRunning = 0;
#endif
}
void                   ttc_states_switch( t_ttc_states_config* Config, TTC_STATES_TYPE NewState ) {
    Assert_STATES_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments

    Assert_STATES( NewState <= Config->State_LastValid,              ttc_assert_origin_auto ); // invalid state given. Check implementation!
    Assert_STATES( NewState > 0,                                     ttc_assert_origin_auto ); // cannot switch to state zero. Check implementation!
    Assert_STATES( Config->State_Current <= Config->State_LastValid, ttc_assert_origin_auto ); // invalid state. Check implementation!

#if (TTC_ASSERT_STATES == 1)
    const t_ttc_states_state* State = Config->Init.AllStates[Config->State_Current - Config->State_Initial];
    const TTC_STATES_TYPE* ReachableStatePtr = State->ReachableStates;
    TTC_STATES_TYPE ReachableState = 0;
    if ( State->AmountReachable < 8 ) { // small amount of reachable states: use linear search
        ReachableState = *ReachableStatePtr;
        do { // check if new state is reachable from current one
            if ( NewState == ReachableState )
            { break; }
            ReachableState = *( ++ReachableStatePtr );
        }
        while ( ReachableState );
    }
    else {                              // large amount of reachable states: use logarithmic search
        // Base: Entries  in State->ReachableStates[] are sorted by increasing values:
        //       State->ReachableStates[i] < State->ReachableStates[i+1])
        // Search starts in the middle of

        //ToDo: test implementation of logarithmic search
        t_u8 Index = ( State->AmountReachable + 1 ) / 2;
        t_u8 Distance = Index / 2;
        do {
            ReachableState = ReachableStatePtr[Index - 1];
            if ( NewState == ReachableState )
            { break; }                  // state found
            else {
                if ( NewState > ReachableState )
                { Index += Distance; }  // state located in higher indices
                else
                { Index -= Distance; }  // state located in lower indices
                Distance /= 2;
            }
        }
        while ( Distance );
        if ( ( NewState != ReachableState ) &&
                ( ( State->AmountReachable & 1 ) == 0 )
           ) { // even amount of entries: must check last one manually
            ReachableState = ReachableStatePtr[State->AmountReachable - 1];
        }
    }
    Assert_STATES( NewState == ReachableState, ttc_assert_origin_auto );
    Assert_STATES( State->Flags.IsSubstate == 0, ttc_assert_origin_auto ); // State to call has been declared as a sub-state. You may only switch to it via ttc_states_call(). Check your declaration and implementation! Compare with ets_State_Display in example_ttc_states.c!
#endif

#if TTC_STATES_AMOUNT_RECORDS > 0 // record current state
    Config->State_History[Config->Index_State_History] = Config->State_Current;
    Config->Index_State_History++;
    if ( Config->Index_State_History >= TTC_STATES_AMOUNT_RECORDS ) { // array overflow
        Config->Index_State_History = 0;
    }
#endif

    // do the switch
    Config->State_Current = NewState;
    Config->Transition = states_transition_Switch;
}
void                   ttc_states_call( t_ttc_states_config* Config, TTC_STATES_TYPE NewState, void* Argument ) {

    Assert_STATES_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_STATES_Writable( Config->Stack_States, ttc_assert_origin_auto ); // Config->Stack_States does not seem to point to allocated RAM. Did you set Config->Init.Stack_Size before calling ttc_states_init()?
    Assert_STATES( Config->Index_Stack < Config->Init.Stack_Size, ttc_assert_origin_auto ); // stack too small: increase Config->Init.Stack_Size value before first ttc_states_init() call or check your implementation!
    Assert_STATES( Config->Init.AllStates[NewState - Config->State_Initial]->Flags.IsSubstate, ttc_assert_origin_auto ); // state to call has not been declared as a sub-state. Check your declaration! Compare with ets_State_Display in example_ttc_states.c!

    // push data of sub-state call onto stack
    t_ttc_states_call* StackData = &( Config->Stack_States[Config->Index_Stack] );
    StackData->ReturnState = Config->State_Current;
    StackData->Value       = Argument;
    Config->Index_Stack++;

    // do the switch
    ttc_states_switch( Config, NewState );

    // adjust transition type
    Config->Transition = states_transition_Call;
}
void                   ttc_states_return( t_ttc_states_config* Config, void* Return ) {

    Assert_STATES_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_STATES( Config->Index_Stack > 0, ttc_assert_origin_auto ); // more pulls than pushes or ttc_states_call() was never called before or memory corrupted. Check your implementation!

    Config->Index_Stack--;
    t_ttc_states_call* StackData = &( Config->Stack_States[Config->Index_Stack] );
    Config->State_Current = StackData->ReturnState;
    Config->Transition = states_transition_Return;

    // storing return value in now unused argument (will be overwritten on next ttc_states_call() call)
    StackData->Value = Return;
}
void*                  ttc_states_get_argument( t_ttc_states_config* Config ) {

    Assert_STATES_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_STATES( Config->Index_Stack > 0, ttc_assert_origin_auto ); // more pulls than pushes or ttc_states_call() was never called before or memory corrupted. Check your implementation!
    Assert_STATES( Config->Index_Stack <= Config->Init.Stack_Size, ttc_assert_origin_auto ); // invalid stack index value. Check implementation!

    // argument was push onto stack by ttc_states_call()
    return Config->Stack_States[Config->Index_Stack - 1].Value;
}
void*                  ttc_states_get_return( t_ttc_states_config* Config ) {

    Assert_STATES_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_STATES( Config->Index_Stack < Config->Init.Stack_Size, ttc_assert_origin_auto ); // invalid stack index value. Check implementation!

    return Config->Stack_States[Config->Index_Stack].Value;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_states(t_u8 LogicalIndex) {  }

void _ttc_states_configuration_check( t_u8 LogicalIndex ) {
    t_ttc_states_config* Config = ttc_states_get_configuration( LogicalIndex );

    // add more architecture independent checks...
    Assert_STATES( LogicalIndex == Config->LogicalIndex, ttc_assert_origin_auto ); // configuration must store corresponding logical index!

    // check state configurations for plausibility
    Config->AmountStates = 0;
    const t_ttc_states_state** StatePtr = Config->Init.AllStates;
    Assert_STATES_Readable( StatePtr, ttc_assert_origin_auto );  // invalid pointer: check your data structure!
    Assert_STATES_Readable( *StatePtr, ttc_assert_origin_auto ); // invalid pointer: check your data structure!
    Config->State_Initial = StatePtr[0]->Number;

    while ( *StatePtr++ ) { // count amount of States[] entries
        Assert_STATES_Readable( StatePtr, ttc_assert_origin_auto ); // invalid pointer: check your data structure!
        Config->AmountStates++;
    }

    // calculate last valid state once to speed up plausibility checks during runtime
    Config->State_LastValid = Config->State_Initial + Config->AmountStates - 1;

    StatePtr = Config->Init.AllStates;
    t_u8 Index = 0;
    while ( *StatePtr ) { // check AllStates[] entries
        Assert_STATES_Readable( StatePtr, ttc_assert_origin_auto ); // invalid pointer: check your data structure!
        const t_ttc_states_state* State = *StatePtr++;
        Assert_STATES_Readable( State, ttc_assert_origin_auto );    // invalid pointer: check your data structure!
        Assert_STATES( State->Number == Config->State_Initial + Index, ttc_assert_origin_auto ); // following state numbers must increase by 1. Check your statemachine configuration!
        Index++;

        Assert_STATES_Readable( State->StateFunction, ttc_assert_origin_auto ); // invalid function pointer given. Provide a pointer to a function of matching signature!
        t_u8 AmountReachable = State->AmountReachable;
        const TTC_STATES_TYPE* ReachableStatePtr = State->ReachableStates;
        Assert_STATES_Readable( ReachableStatePtr, ttc_assert_origin_auto ); // invalid pointer found. Check your states configuration!

        TTC_STATES_TYPE PreviousReachableState = 0; ( void ) PreviousReachableState;
        while ( *ReachableStatePtr != 0 ) { // check entries of ReachableStates[]
            Assert_STATES( AmountReachable > 0, ttc_assert_origin_auto ); // State->ReachableStates[] seems to be longer than declared. Update State->AmountReachable or add a zero terminator to State->ReachableStates[]!
            AmountReachable--;
            volatile TTC_STATES_TYPE ReachableState = *ReachableStatePtr;
            Assert_STATES( ReachableState <= Config->State_LastValid, ttc_assert_origin_auto ); // invalid state number in State->ReachableStates[]. Check your states configuration!
            Assert_STATES( ReachableState > PreviousReachableState, ttc_assert_origin_auto );   // entries of State->ReachableStates[] must be sorted by increasing numbers. Reorder State->ReachableStates[]!
            PreviousReachableState = ReachableState;

            // jump to next reachable state
            ReachableStatePtr++;
        }
        Assert_STATES( AmountReachable == 0, ttc_assert_origin_auto ); // State->ReachableStates[] seems to be shorter than declared. Update State->AmountReachable to show amount of non-zero entries!
    }

    if ( Config->Init.MonitorTransition ) { // pointer to monitor function given: check if readable
        Assert_STATES_Readable( Config->Init.MonitorTransition, ttc_assert_origin_auto ); // must point to a readable (and executable) memory. Check your Configuration!
    }
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
