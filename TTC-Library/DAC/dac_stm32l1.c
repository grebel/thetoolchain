/** { DAC_stm32l1.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for DAC devices on stm32l1 architectures.
 *  Implementation of low-level driver. 
 *    
 *  Note: See ttc_DAC.h for description of stm32l1 independent DAC implementation.
 * 
 *  Authors: Gregor Rebel
}*/

#include "DAC_stm32l1.h"

//{ Function definitions ***************************************************

void DAC_stm32l1_prepare() {
  // add your startup code here (Singletasking!)
  
}
bool DAC_stm32l1_check_initialized(t_ttc_dac_config* Config) {

    if (Config->LowLevelConfig)
        return TRUE;

    return FALSE;
}
t_DAC_stm32l1_config* DAC_stm32l1_get_configuration(t_ttc_dac_config* Config) {
    Assert_DAC(Config != NULL, ttc_assert_origin_auto);
    Assert_DAC(Config->LogicalIndex > 0, ttc_assert_origin_auto);

    t_DAC_stm32l1_config* Config_stm32l1 = Config->LowLevelConfig;
    if (Config_stm32l1 == NULL) {  // first call: allocate + init configuration
      Config_stm32l1 = ttc_heap_alloc_zeroed( sizeof(t_ttc_dac_arch) );
      
      switch (Config->PhysicalIndex) {           // determine base register and other low-level configuration data
         case 0: { // load low-level configuration of first DAC device 
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of DAC device
           break;
         }
         case 1: { // load low-level configuration of second DAC device
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of DAC device
           break;
         } 
         case 2: { // load low-level configuration of third DAC device
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of DAC device
           break;             
         }
         case 3: { // load low-level configuration of fourth DAC device
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of DAC device
           break;
         }
         case 4: { // load low-level configuration of fifth DAC device
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of DAC device
           break;
         }
         case 5: { // load low-level configuration of sixth DAC device
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of DAC device
           break;
         }
         case 6: { // load low-level configuration of seveth DAC device
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of DAC device
           break;
         }
         case 7: { // load low-level configuration of eighth DAC device
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of DAC device
           break;
         }
      default: Assert_DAC(0, ttc_assert_origin_auto); break; // No TTC_DACn defined! Check your makefile.100_board_* file!
      }
    }
    
    Assert_DAC(Config_stm32l1, ttc_assert_origin_auto);
    return Config_stm32l1;
}
ttc_DAC_errorcode_e DAC_stm32l1_get_features(t_ttc_dac_config* Config) {
    Assert_DAC(Config,    ttc_assert_origin_auto);
    Assert_DAC(Config->LogicalIndex > 0, ttc_assert_origin_auto);
    if (Config->LogicalIndex > TTC_DAC_AMOUNT) return ec_DAC_DeviceNotFound;

    Config->Flags.All                        = 0;
    //Config->Flags.Bits.Master                = 1;

    switch (Config->LogicalIndex) {           // determine features of indexed DAC device
#ifdef TTC_DAC1
      case 1: break;
#endif
#ifdef TTC_DAC2
      case 2: break;
#endif
#ifdef TTC_DAC3
      case 3: break;
#endif
#ifdef TTC_DAC4
      case 4: break;
#endif
#ifdef TTC_DAC5
      case 5: break;
#endif
      default: Assert_DAC(0, ttc_assert_origin_auto); break; // No TTC_DACn defined! Check your makefile.100_board_* file!
    }
    
    return ec_DAC_OK;
}
ttc_DAC_errorcode_e DAC_stm32l1_init(t_ttc_dac_config* Config) {
    Assert_DAC(Config, ttc_assert_origin_auto);
    Assert_DAC(Config->LogicalIndex > 0, ttc_assert_origin_auto);
    t_DAC_stm32l1_config* Config_stm32l1 = DAC_stm32l1_get_configuration(Config);
    (void) Config_stm32l1; // avoid warning: "unused variable"

    if (1) { // validate DAC_Features
        t_ttc_dac_config DAC_Features;
        DAC_Features.LogicalIndex = Config->LogicalIndex;
        ttc_DAC_errorcode_e Error = DAC_stm32l1_get_features(&DAC_Features);
        if (Error) return Error;
        Config->Flags.All &= DAC_Features.Flags.All; // mask out unavailable flags
    }
    switch (Config->LogicalIndex) {                // find DAC corresponding to DAC_index as defined by makefile.100_board_*
#ifdef TTC_DAC1
      case 1: Config_stm32l1->Base = (DAC_t*) TTC_DAC1; break;
#endif
#ifdef TTC_DAC2
      case 2: Config_stm32l1->Base = (DAC_t*) TTC_DAC2; break;
#endif
#ifdef TTC_DAC3
      case 3: Config_stm32l1->Base = (DAC_t*) TTC_DAC3; break;
#endif
      default: Assert_DAC(0, ttc_assert_origin_auto); break; // No TTC_DACn defined! Check your makefile.100_board_* file!
    }
    
    return ec_DAC_OK;
}
ttc_DAC_errorcode_e DAC_stm32l1_deinit(t_ttc_dac_config* Config) {
    Assert_DAC(Config, ttc_assert_origin_auto);
    Assert_DAC(Config->LogicalIndex > 0, ttc_assert_origin_auto);
    t_DAC_stm32l1_config* Config_stm32l1 = DAC_stm32l1_get_configuration(Config);
    (void) Config_stm32l1; // avoid warning: "unused variable"

    // ToDo: Deinitialize hardware device
    
    return ec_DAC_OK;
}
ttc_DAC_errorcode_e DAC_stm32l1_load_defaults(t_ttc_dac_config* Config) {
    Assert_DAC(Config != NULL, ttc_assert_origin_auto);
    Assert_DAC(Config->LogicalIndex > 0, ttc_assert_origin_auto);

    Config->Flags.All                        = 0;
    // DAC_Cfg->Flags.Bits.Master                = 1;

    return ec_DAC_OK;
}
ttc_DAC_errorcode_e  DAC_stm32l1_reset(t_ttc_dac_config* Config) {
    Assert_DAC(Config != NULL, ttc_assert_origin_auto);
    Assert_DAC(Config->LogicalIndex > 0, ttc_assert_origin_auto);
  
    return ec_DAC_OK;
}

//}FunctionDefinitions
//{ private functions (ideally) *********************************************

//} private functions
