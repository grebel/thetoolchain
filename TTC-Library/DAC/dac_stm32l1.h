#ifndef ARCHITECTURE_DAC_H
#define ARCHITECTURE_DAC_H

/** { DAC_stm32l1.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for DAC devices on stm32l1 architectures.
 *  Structures, Enums and Defines being required by high-level DAC and application.
 *
 *  Note: See ttc_DAC.h for description of stm32l1 independent DAC implementation.
 *  
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

#include "DAC_stm32l1_types.h"
#include "../ttc_DAC_types.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_DAC_interface.h
#define ttc_driver_DAC_prepare()             DAC_stm32l1_prepare()
#define ttc_driver_DAC_reset(Config)         DAC_stm32l1_reset(Config)
#define ttc_driver_DAC_load_defaults(Config) DAC_stm32l1_load_defaults(Config)
#define ttc_driver_DAC_deinit(Config)        DAC_stm32l1_deinit(Config)
#define ttc_driver_DAC_init(Config)          DAC_stm32l1_init(Config)

//} Includes
//{ Function prototypes **************************************************

/** Prepares DAC Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void DAC_stm32l1_prepare();

/** checks if DAC driver already has been initialized
 * @param Config        pointer to struct t_ttc_dac_config (must have valid value for LogicalIndex)
 * @return              == TRUE: indexed DAC unit is already initialized
 */
bool DAC_stm32l1_check_initialized(t_ttc_dac_config* Config);

/** returns reference to low-level configuration struct of DAC driver
 * @param Config        pointer to struct t_ttc_dac_config (must have valid value for LogicalIndex)
 * @return              pointer to struct t_ttc_dac_config (will assert if no configuration available)
 */
t_DAC_stm32l1_config* DAC_stm32l1_get_configuration(t_ttc_dac_config* Config);

/** loads configuration of DAC driver with default values
 * @param Config        pointer to struct t_ttc_dac_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
ttc_DAC_errorcode_e DAC_stm32l1_load_defaults(t_ttc_dac_config* Config);

/** fills out given Config with maximum valid values for indexed DAC
 * @param Config        pointer to struct t_ttc_dac_config (must have valid value for LogicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
ttc_DAC_errorcode_e DAC_stm32l1_get_features(t_ttc_dac_config* Config);

/** initializes single DAC
 * @param Config        pointer to struct t_ttc_dac_config (must have valid value for LogicalIndex)
 * @return              == 0: DAC has been initialized successfully; != 0: error-code
 */
ttc_DAC_errorcode_e DAC_stm32l1_init(t_ttc_dac_config* Config);

/** shutdown single DAC device
 * @param Config        pointer to struct t_ttc_dac_config (must have valid value for LogicalIndex)
 * @return              == 0: DAC has been shutdown successfully; != 0: error-code
 */
ttc_DAC_errorcode_e DAC_stm32l1_deinit(t_ttc_dac_config* Config);

/** reset configuration of DAC driver and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_DAC_get_max_LogicalIndex())
 */
ttc_DAC_errorcode_e DAC_stm32l1_reset(t_ttc_dac_config* Config);

//} Function prototypes

#endif //ARCHITECTURE_DAC_H
