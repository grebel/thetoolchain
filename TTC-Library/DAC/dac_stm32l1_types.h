#ifndef ARCHITECTURE_DAC_TYPES_H
#define ARCHITECTURE_DAC_TYPES_H

/** { DAC_stm32l1.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *                     
 *  Low-Level datatypes for DAC devices on stm32l1 architectures.
 *  Structures, Enums and Defines being required by ttc_DAC_types.h
 *
 *  Note: See ttc_DAC.h for description of architecture independent DAC implementation.
 * 
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ******************************************************


//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic.h"

//} Includes
//{ Structures/ Enums required by ttc_DAC_types.h ***********************

typedef struct { // register description (adapt according to stm32l1 registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} t_dac_register;

typedef enum {
    dac_stm32l1_trigger_none = (t_u32)0x00000000,
    dac_stm32l1_trigger_T6_TRGO = (t_u32)0x00000004,
    dac_stm32l1_trigger_T7_TRGO =(t_u32)0x00000014,
    dac_stm32l1_trigger_T9_TRGO =(t_u32)0x0000001C,
    dac_stm32l1_trigger_T2_TRGO =(t_u32)0x00000024,
    dac_stm32l1_trigger_T4_TRGO =(t_u32)0x0000002C,
    dac_stm32l1_trigger_Ext_IT9 =(t_u32)0x00000034,
    dac_stm32l1_trigger_Software =(t_u32)0x0000003C

} e_dac_stm32l1_trigger_source;

typedef enum{ //is wave generated and what type
    dac_stm32l1_WaveGeneration_None =           (t_u32)0x00000000,
    dac_stm32l1_WaveGeneration_Noise =          (t_u32)0x00000040,
    dac_stm32l1_WaveGeneration_Triangle =       (t_u32)0x00000080

} e_dac_stm32l1_wave_gen;

typedef enum{ //LFSR mask for noise wave or amplitude of triangular wave
    dac_stm32l1_LFSRUnmask_Bit0             =   (t_u32)0x00000000, //*!< Unmask DAC channel LFSR bit0 for noise wave generation */
    dac_stm32l1_LFSRUnmask_Bits1_0          =  (t_u32)0x00000100, //*!< Unmask DAC channel LFSR bit[1:0] for noise wave generation */
    dac_stm32l1_LFSRUnmask_Bits2_0          =   (t_u32)0x00000200, //*!< Unmask DAC channel LFSR bit[2:0] for noise wave generation */
    dac_stm32l1_LFSRUnmask_Bits3_0          =   (t_u32)0x00000300, //*!< Unmask DAC channel LFSR bit[3:0] for noise wave generation */
    dac_stm32l1_LFSRUnmask_Bits4_0          =   (t_u32)0x00000400, //*!< Unmask DAC channel LFSR bit[4:0] for noise wave generation */
    dac_stm32l1_LFSRUnmask_Bits5_0          =   (t_u32)0x00000500, //*!< Unmask DAC channel LFSR bit[5:0] for noise wave generation */
    dac_stm32l1_LFSRUnmask_Bits6_0          =   (t_u32)0x00000600, //*!< Unmask DAC channel LFSR bit[6:0] for noise wave generation */
    dac_stm32l1_LFSRUnmask_Bits7_0          =   (t_u32)0x00000700, //*!< Unmask DAC channel LFSR bit[7:0] for noise wave generation */
    dac_stm32l1_LFSRUnmask_Bits8_0          =   (t_u32)0x00000800, //*!< Unmask DAC channel LFSR bit[8:0] for noise wave generation */
    dac_stm32l1_LFSRUnmask_Bits9_0          =   (t_u32)0x00000900, //*!< Unmask DAC channel LFSR bit[9:0] for noise wave generation */
    dac_stm32l1_LFSRUnmask_Bits10_0         =   (t_u32)0x00000A00, //*!< Unmask DAC channel LFSR bit[10:0] for noise wave generation */
    dac_stm32l1_LFSRUnmask_Bits11_0         =   (t_u32)0x00000B00, //*!< Unmask DAC channel LFSR bit[11:0] for noise wave generation */
    dac_stm32l1_TriangleAmplitude_1         =   (t_u32)0x00000000, //*!< Select max triangle amplitude of 1 */
    dac_stm32l1_TriangleAmplitude_3         =   (t_u32)0x00000100, //*!< Select max triangle amplitude of 3 */
    dac_stm32l1_TriangleAmplitude_7         =   (t_u32)0x00000200, //*!< Select max triangle amplitude of 7 */
    dac_stm32l1_TriangleAmplitude_15        =   (t_u32)0x00000300, //*!< Select max triangle amplitude of 15 */
    dac_stm32l1_TriangleAmplitude_31        =   (t_u32)0x00000400, //*!< Select max triangle amplitude of 31 */
    dac_stm32l1_TriangleAmplitude_63        =   (t_u32)0x00000500, //*!< Select max triangle amplitude of 63 */
    dac_stm32l1_TriangleAmplitude_127       =   (t_u32)0x00000600, //*!< Select max triangle amplitude of 127 */
    dac_stm32l1_TriangleAmplitude_255       =   (t_u32)0x00000700, //*!< Select max triangle amplitude of 255 */
    dac_stm32l1_TriangleAmplitude_511       =   (t_u32)0x00000800, //*!< Select max triangle amplitude of 511 */
    dac_stm32l1_TriangleAmplitude_1023      =   (t_u32)0x00000900, //*!< Select max triangle amplitude of 1023 */
    dac_stm32l1_TriangleAmplitude_2047      =   (t_u32)0x00000A00, //*!< Select max triangle amplitude of 2047 */
    dac_stm32l1_TriangleAmplitude_4095      =   (t_u32)0x00000B00 //*!< Select max triangle amplitude of 4095 */

} e_dac_stm32l1_mask_or_amplitude;

typedef enum{
    dac_stm32l1_output_buffer_enable = (t_u32)0x00000000,
    dac_stm32l1_output_buffer_disable = (t_u32)0x00000002

} e_dac_stm32l1_output_buffer;

typedef struct {  // stm32l1 specific configuration data of single DAC device
    e_dac_stm32l1_trigger_source    TriggerType;
    e_dac_stm32l1_wave_gen          WaveType;
    e_dac_stm32l1_mask_or_amplitude Mask_or_Amp;
    e_dac_stm32l1_output_buffer     Output_Buffer_status;
} __attribute__((__packed__)) t_DAC_stm32l1_config;

// t_ttc_dac_arch is required by ttc_DAC_types.h
typedef t_DAC_stm32l1_config t_ttc_dac_arch;

//} Structures/ Enums


#endif //ARCHITECTURE_DAC_TYPES_H
