/*{ ttc_spi::.c ************************************************
 
                      The ToolChain
                      
   Device independent support for 
   
   
   written by Gregor Rebel 2012
   
 
}*/

#include "DEPRECATED_ttc_spi.h"

//{ Function definitions *************************************************

bool ttc_spi_check_initialized(u8_t SPI_Index) {
#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_spi_check_initialized(SPI_Index);
#endif
#ifdef TARGET_ARCHITECTURE_STM32W1xx
    return stm32w_spi_check_initialized(SPI_Index);
#endif

  return tse_NotImplemented;
}
inline                u8_t ttc_spi_get_max_index() {

    return TTC_AMOUNT_SPIS; // -> ttc_spi_types.h
}
inline  ttc_spi_generic_t* ttc_spi_get_configuration(u8_t SPI_Index) {
    
    Assert(SPI_Index > 0, ec_InvalidArgument);                // logical index starts at 1
    Assert(SPI_Index <= TTC_AMOUNT_SPIS, ec_InvalidArgument); // use ttc_spi_get_max_index() to get max allowed value

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_spi_get_configuration(SPI_Index);
#endif
#ifdef TARGET_ARCHITECTURE_STM32W1xx
    return stm32w_spi_get_configuration(SPI_Index);
#endif

  return NULL;
}
inline ttc_spi_errorcode_e ttc_spi_get_features(u8_t SPI_Index, ttc_spi_generic_t* SPI_Generic) {
    
#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_spi_get_features(SPI_Index, SPI_Generic);
#endif
#ifdef TARGET_ARCHITECTURE_STM32W1xx
    return stm32w_spi_get_features(SPI_Index, SPI_Generic);
#endif

  return tse_NotImplemented;
}
inline ttc_spi_errorcode_e ttc_spi_load_defaults(u8_t SPI_Index) {

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_spi_load_defaults(SPI_Index);
#endif
#ifdef TARGET_ARCHITECTURE_STM32W1xx
    return stm32w_spi_load_defaults(SPI_Index);
#endif

  return tse_NotImplemented;
}
inline ttc_spi_errorcode_e ttc_spi_init(u8_t SPI_Index) {

#ifdef TARGET_ARCHITECTURE_STM32F1xx
  return stm32_spi_init(SPI_Index);
#endif
#ifdef TARGET_ARCHITECTURE_STM32W1xx
  return stm32w_spi_init(SPI_Index);
#endif

  return tse_NotImplemented;
}
inline ttc_spi_errorcode_e ttc_spi_send_raw(u8_t SPI_Index, const u8_t* Buffer, u16_t Amount) {
  
#ifdef TARGET_ARCHITECTURE_STM32F1xx
  return stm32_spi_send_raw(SPI_Index, (const char*) Buffer, Amount);
#endif
#ifdef TARGET_ARCHITECTURE_STM32W1xx
  return stm32w_spi_send_raw(SPI_Index, (const char*) Buffer, Amount);
#endif

  return tse_NotImplemented;
}
inline ttc_spi_errorcode_e ttc_spi_send_string(u8_t SPI_Index, const char* Buffer, u16_t MaxLength) {
  
#ifdef TARGET_ARCHITECTURE_STM32F1xx
  return stm32_spi_send_string(SPI_Index, Buffer, MaxLength);
#endif
#ifdef TARGET_ARCHITECTURE_STM32W1xx
  return stm32w_spi_send_string(SPI_Index, Buffer, MaxLength);
#endif

  return tse_NotImplemented;
}
inline ttc_spi_errorcode_e ttc_spi_send_word(u8_t SPI_Index, const u16_t Word) {

#ifdef TARGET_ARCHITECTURE_STM32F1xx
  return stm32_spi_send_word(SPI_Index, Word);
#endif
#ifdef TARGET_ARCHITECTURE_STM32W1xx
  return stm32w_spi_send_word(SPI_Index, Word);
#endif

  return tse_NotImplemented;
}
inline ttc_spi_errorcode_e ttc_spi_read_word(u8_t SPI_Index, u16_t* Word, Base_t TimeOut) {

#ifdef TARGET_ARCHITECTURE_STM32F1xx
  return stm32_spi_read_word(SPI_Index, Word, TimeOut);
#endif
#ifdef TARGET_ARCHITECTURE_STM32W1xx
  return stm32w_spi_read_word(SPI_Index, Word, TimeOut);
#endif

  return tse_NotImplemented;
}
inline ttc_spi_errorcode_e ttc_spi_read_byte(u8_t SPI_Index, u8_t* Byte, Base_t TimeOut) {

#ifdef TARGET_ARCHITECTURE_STM32F1xx
  return stm32_spi_read_byte(SPI_Index, Byte, TimeOut);
#endif
#ifdef TARGET_ARCHITECTURE_STM32W1xx
  return stm32w_spi_read_byte(SPI_Index, Byte, TimeOut);
#endif

  return tse_NotImplemented;
}
inline ttc_gpio_pin_e* ttc_spi_get_port(u8_t SPI_Index, ttc_spi_pins_e Pin) {

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return (ttc_gpio_pin_e*) stm32_spi_get_port(SPI_Index, Pin);
#endif
}

//} Function definitions
