/** { ttc_usb_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for USB device.
 *   
 *  *_interface_*() functions may be redirected to low-level implementations in 
 *  two ways:
 *   
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *      
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than 
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *      
 *  Adding a new low-level implementation requires changes in this file.
 *  
 *  Created from template ttc_device_interface.h revision 20 at 20140411 07:54:11 UTC
 *
 *  Authors: Gregor Rebel
 *   
}*/

#ifndef TTC_USB_INTERFACE_H
#define TTC_USB_INTERFACE_H

//{ Includes *************************************************************

#include "../ttc_basic.h"

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_usb_stm32f1xx
#  include "../usb/usb_stm32f1xx.h"
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_usb_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_usb_*() declaration in ../ttc_usb.h, a corresponding 
 * interface part is required here. The interface will forward the declaration either   
 * to a low-level implementation or a default implementation in ttc_usb_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_usb.h for prototypes of low-level driver functions!
 *
 */
 
e_ttc_usb_errorcode ttc_usb_interface_load_defaults(t_ttc_usb_config* Config); //{  macro _driver_usb_interface_load_defaults(Config)
#ifdef ttc_driver_usb_load_defaults
// enable following line to forward interface function as a macro definition
#  define _driver_usb_load_defaults(Config) ttc_driver_usb_load_defaults(Config)
#else
#  define _driver_usb_load_defaults(Config) ttc_usb_interface_load_defaults(Config)
#endif
//}
void ttc_usb_interface_prepare(); //{  macro _driver_usb_interface_prepare()
#ifdef ttc_driver_usb_prepare
// enable following line to forward interface function as a macro definition
#  define _driver_usb_prepare() ttc_driver_usb_prepare()
#else
#  define _driver_usb_prepare() ttc_usb_interface_prepare()
#endif
//}
e_ttc_usb_errorcode ttc_usb_interface_reset(t_ttc_usb_config* Config); //{  macro _driver_usb_interface_reset(Config)
#ifdef ttc_driver_usb_reset
// enable following line to forward interface function as a macro definition
#  define _driver_usb_reset(Config) ttc_driver_usb_reset(Config)
#else
#  define _driver_usb_reset(Config) ttc_usb_interface_reset(Config)
#endif
//}
e_ttc_usb_errorcode ttc_usb_interface_init(t_ttc_usb_config* Config); //{  macro _driver_usb_interface_init(Config)
#ifdef ttc_driver_usb_init
// enable following line to forward interface function as a macro definition
#  define _driver_usb_init(Config) ttc_driver_usb_init(Config)
#else
#  define _driver_usb_init(Config) ttc_usb_interface_init(Config)
#endif
//}
e_ttc_usb_errorcode ttc_usb_interface_deinit(t_ttc_usb_config* Config); //{  macro _driver_usb_interface_deinit(Config)
#ifdef ttc_driver_usb_deinit
// enable following line to forward interface function as a macro definition
#  define _driver_usb_deinit(Config) ttc_driver_usb_deinit(Config)
#else
#  define _driver_usb_deinit(Config) ttc_usb_interface_deinit(Config)
#endif
//}
e_ttc_usb_errorcode ttc_usb_interface_get_features(t_ttc_usb_config* Config); //{  macro _driver_usb_interface_get_features(Config)
#ifdef ttc_driver_usb_get_features
// enable following line to forward interface function as a macro definition
#  define _driver_usb_get_features(Config) ttc_driver_usb_get_features(Config)
#else
#  define _driver_usb_get_features(Config) ttc_usb_interface_get_features(Config)
#endif
//}
e_ttc_usb_errorcode ttc_usb_interface_register_rx_function(t_u8 LogicalIndex, void (*TxFunction)(t_u8* Data_Buffer, t_u8 Amount)); //{  macro _driver_usb_interface_register_rx_function(LogicalIndex, Data_Buffer, e_ttc_usb_errorcode)
#ifdef ttc_driver_usb_register_rx_function
// enable following line to forward interface function as a macro definition
#  define _driver_usb_register_rx_function(LogicalIndex, Data_Buffer, e_ttc_usb_errorcode) ttc_driver_usb_register_rx_function(LogicalIndex, Data_Buffer, e_ttc_usb_errorcode)
#else
#  define _driver_usb_register_rx_function(LogicalIndex, Data_Buffer, e_ttc_usb_errorcode) ttc_usb_interface_register_rx_function(LogicalIndex, Data_Buffer, e_ttc_usb_errorcode)
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_usb_interface_foo(t_ttc_usb_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_USB_INTERFACE_H
