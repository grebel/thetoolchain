/** { ttc_sysclock_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for SYSCLOCK device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_sysclock_*() functions.
 *  Instead they can call _driver_sysclock_*() pendants.
 *  E.g.: ttc_sysclock_reset() -> _driver_sysclock_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20140218 17:32:51 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_sysclock_interface.h"

/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_sysclock_prepare  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void ttc_sysclock_interface_prepare( t_ttc_sysclock_config* Config ) {




}

#endif
#ifndef ttc_driver_sysclock_reset  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void ttc_sysclock_interface_reset( t_ttc_sysclock_config* Config ) {



}

#endif
#ifndef ttc_driver_sysclock_profile_switch  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void ttc_sysclock_interface_profile_switch( t_ttc_sysclock_config* Config, e_ttc_sysclock_profile NewProfile ) {




}

#endif
#ifndef ttc_driver_sysclock_udelay  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sysclock_udelay() (using default implementation)!

void ttc_sysclock_interface_wait( t_ttc_sysclock_config* Config, t_base Microseconds ) {




}

#endif
#ifndef ttc_driver_sysclock_udelay  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sysclock_udelay() (using default implementation)!

void ttc_sysclock_interface_udelay( t_base Microseconds ) {




}

#endif
#ifndef ttc_driver_sysclock_update  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sysclock_update() (using default implementation)!

void ttc_sysclock_interface_update( t_ttc_sysclock_config* Config ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif
#ifndef ttc_driver_sysclock_enable_oscillator  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sysclock_enable_oscillator() (using default implementation)!

e_ttc_sysclock_errorcode ttc_sysclock_interface_enable_oscillator( e_ttc_sysclock_oscillator Oscillator, BOOL On ) {


#warning Function ttc_sysclock_interface_enable_oscillator() is empty.

    return ( e_ttc_sysclock_errorcode ) 0;
}

#endif
#ifndef ttc_driver_sysclock_frequency_get_all  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sysclock_frequency_get_all() (using default implementation)!

t_base* ttc_sysclock_interface_frequency_get_all( t_ttc_sysclock_config* Config ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL


#warning Function ttc_sysclock_interface_frequency_get_all() is empty.

    return ( t_base* ) 0;
}

#endif
#ifndef ttc_driver_sysclock_frequency_set  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sysclock_frequency_set() (using default implementation)!

void ttc_sysclock_interface_frequency_set( t_ttc_sysclock_config* Config, t_base NewFrequency ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_sysclock_interface_frequency_set() is empty.


}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_sysclock_interface_foo(t_ttc_sysclock_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions