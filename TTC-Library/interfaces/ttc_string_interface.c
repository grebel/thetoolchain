/** { ttc_string_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for STRING device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_string_*() functions.
 *  Instead they can call _driver_string_*() pendants.
 *  E.g.: ttc_string_reset() -> _driver_string_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20151120 13:30:25 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_string_interface.h".
//
#include "ttc_string_interface.h"
#include "../string/string_common.h"
#include "../ttc_memory.h"
#include "../ttc_mutex.h"
#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

typedef struct { // internal flags used by ttc_string_snprintf() only
    unsigned LeadingZeros : 1;
    unsigned InsideFormat : 1;
    signed MinLength      : 9;
    unsigned FloatPoint     : 9;
} __attribute__( ( __packed__ ) ) t_tss_flags;

//} Global Variables
//{ Private Function declarations ****************************************

t_u8* _ttc_driver_string_snprintf( t_u8* Buffer, t_base Size, const char* Format, va_list Arguments );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Private Function declarations
//{ Function definitions *************************************************

#ifndef ttc_driver_string_prepare  // no low-level implementation: use default implementation
void ttc_string_interface_prepare() {


}
#endif

#ifndef ttc_driver_string_print  // no low-level implementation: use default implementation
void ttc_string_interface_print( const t_u8* String, t_base MaxSize ) {
    Assert_STRING_Readable( String, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    if ( _string_common_stdout ) {
        _string_common_stdout( String, MaxSize );
    }
}
#endif
#ifndef ttc_driver_string_print_block  // no low-level implementation: use default implementation
void ttc_string_interface_print_block( t_ttc_heap_block* Block ) {
    Assert_STRING_Writable( Block, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    if ( _string_common_stdout_block ) {
        _string_common_stdout_block( Block );
    }
}
#endif
#ifndef ttc_driver_string_is_whitechar  // no low-level implementation: use default implementation
BOOL ttc_string_interface_is_whitechar( t_u8 ASCII ) {

    if ( ASCII < 32 ) { return TRUE; }

    return FALSE;
}
#endif
#ifndef ttc_driver_string_snprintf  // no low-level implementation: use default implementation
t_u8* ttc_string_interface_snprintf( t_u8* Buffer, t_base Size, const char* Format, ... ) {
    Assert_STRING_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_STRING_Readable( Format, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    va_list Arguments;
    va_start( Arguments, Format );
    Buffer = _ttc_driver_string_snprintf( Buffer, Size, Format, Arguments );
    va_end( Arguments );

    return Buffer;
}
t_u8* ttc_string_interface_snprintf4( t_u8* Buffer, t_base Size, const char* Format, va_list Arguments ) {
    Assert_STRING_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_STRING_Readable( Format, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    Buffer = _ttc_driver_string_snprintf( Buffer, Size, Format, Arguments );

    return Buffer;
}
#endif
#ifndef ttc_driver_string_log_hex  // no low-level implementation: use default implementation
t_u8 ttc_string_interface_log_hex( t_base Value ) {

    if ( Value > 0xfffffff ) { return 8; }
    if ( Value > 0x0ffffff ) { return 7; }
    if ( Value > 0x00fffff ) { return 6; }
    if ( Value > 0x000ffff ) { return 5; }
    if ( Value > 0x0000fff ) { return 4; }
    if ( Value > 0x00000ff ) { return 3; }
    if ( Value > 0x000000f ) { return 2; }
    return 1;
}
#endif
#ifndef ttc_driver_string_printf  // no low-level implementation: use default implementation
void ttc_string_interface_printf( const char* Format, ... ) {
    Assert_STRING_Readable( Format, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    string_common_lock();

    va_list Arguments;
    va_start( Arguments, Format );

    _ttc_driver_string_snprintf( string_common_get_buffer(), TTC_STRING_PRINTF_BUFFERSIZE, Format, Arguments );

    va_end( Arguments );

    _driver_string_print( string_common_get_buffer(), ( t_base ) TTC_STRING_PRINTF_BUFFERSIZE );

#ifdef EXTENSION_arm_semihosting
    perror( uss_Buffer );
    TODO( "get arm semihosting working!" )
#endif

    string_common_unlock();
}
#endif
#ifndef ttc_driver_string_length8  // no low-level implementation: use default implementation
t_u8 ttc_string_interface_length8( const char* String, t_u8 MaxLen ) {
    Assert_STRING_Readable( String, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_u8 Length = 0;

    while ( ( MaxLen > 0 ) && ( *String != 0 ) ) {
        MaxLen--;
        String++;
        Length++;
    }
    return Length;
}
#endif
#ifndef ttc_driver_string_length16  // no low-level implementation: use default implementation
t_u16 ttc_string_interface_length16( const char* String, t_u16 MaxLen ) {
    Assert_STRING_Readable( String, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_u16 Length = 0;

    while ( ( MaxLen > 0 ) && ( *String != 0 ) ) {
        MaxLen--;
        String++;
        Length++;
    }
    return Length;
}
#endif
#ifndef ttc_driver_string_length32  // no low-level implementation: use default implementation
t_u32 ttc_string_interface_length32( const char* String, t_u32 MaxLen ) {
    Assert_STRING_Readable( String, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_u32 Length = 0;

    while ( ( MaxLen > 0 ) && ( *String != 0 ) ) {
        MaxLen--;
        String++;
        Length++;
    }
    return Length;
}
#endif
#ifndef ttc_driver_string_copy  // no low-level implementation: use default implementation
t_base ttc_string_interface_copy( t_u8* Target, const t_u8* Source, t_base MaxSize ) {
    Assert_STRING_Writable( Target, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_STRING_Readable( Source, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_base BytesCopied = 0;
    t_u8 C = 0;

    MaxSize--; // reserve 1 byte for string terminator

    while ( ( BytesCopied < MaxSize ) && ( ( C = *Source++ ) != 0 ) ) {
        *Target++ = C;
        BytesCopied++;
    }
    *Target++ = 0;          // terminate string in Target[]

    return BytesCopied;
}
#endif
#ifndef ttc_driver_string_appendf  // no low-level implementation: use default implementation
t_base ttc_string_interface_appendf( t_u8* Buffer, t_base* Size, const char* Format, ... ) {
    Assert_STRING_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_STRING_Writable( Size, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_STRING_Readable( Format, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    va_list Arguments;
    va_start( Arguments, Format );
    t_u8* End =  _driver_string_snprintf4( Buffer, *Size, Format, Arguments );
    va_end( Arguments );

    t_base BytesAppended = End - Buffer;
    *Size -= BytesAppended;

    return BytesAppended;
}
#endif
#ifndef ttc_driver_string_strip  // no low-level implementation: use default implementation
void ttc_string_interface_strip( t_u8** Start, t_base* Size ) {
    Assert_STRING_Writable( Start, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_STRING_Writable( Size, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_u8* Reader   = *Start;
    t_base NewSize = *Size;

    // remove leading whitespaces
    while ( ( NewSize > 0 ) &&  _driver_string_is_whitechar( *Reader ) ) {
        Reader++;
        NewSize--;
    }
    *Start = Reader;

    // remove trailing whitespaces
    Reader = *Start + NewSize - 1;
    while ( ( NewSize > 0 ) &&  _driver_string_is_whitechar( *Reader ) ) {
        Reader--;
        NewSize--;
    }
    *Size = NewSize;
}
#endif
#ifndef ttc_driver_string_log_binary  // no low-level implementation: use default implementation
t_u8 ttc_string_interface_log_binary( t_base Value ) {

    if ( Value == 0 )
    { return 1; }

    if ( 1 ) { // using for-loop (slower but less code size)
        t_u8 Length = TARGET_DATA_WIDTH; // 32, 16 or 8

        while ( ( Value & ( 1 << ( TARGET_DATA_WIDTH - 1 ) ) ) == 0 ) {  // find first non zero bit
            Length--;
            Value = Value << 1;
        }

        return Length;
    }
    else { // using constant boolean operations (faster but larger code size)
#if TARGET_DATA_WIDTH >= 32
        if ( Value & ( 1 << 31 ) ) { return 32; }
        if ( Value & ( 1 << 30 ) ) { return 31; }
        if ( Value & ( 1 << 29 ) ) { return 30; }
        if ( Value & ( 1 << 28 ) ) { return 29; }
        if ( Value & ( 1 << 27 ) ) { return 28; }
        if ( Value & ( 1 << 26 ) ) { return 27; }
        if ( Value & ( 1 << 25 ) ) { return 26; }
        if ( Value & ( 1 << 24 ) ) { return 25; }
        if ( Value & ( 1 << 23 ) ) { return 24; }
        if ( Value & ( 1 << 22 ) ) { return 23; }
        if ( Value & ( 1 << 21 ) ) { return 22; }
        if ( Value & ( 1 << 20 ) ) { return 21; }
        if ( Value & ( 1 << 19 ) ) { return 20; }
        if ( Value & ( 1 << 18 ) ) { return 19; }
        if ( Value & ( 1 << 17 ) ) { return 18; }
        if ( Value & ( 1 << 16 ) ) { return 17; }
#endif
#if TARGET_DATA_WIDTH >= 16
        if ( Value & ( 1 << 15 ) ) { return 16; }
        if ( Value & ( 1 << 14 ) ) { return 15; }
        if ( Value & ( 1 << 13 ) ) { return 14; }
        if ( Value & ( 1 << 12 ) ) { return 13; }
        if ( Value & ( 1 << 11 ) ) { return 12; }
        if ( Value & ( 1 << 10 ) ) { return 11; }
        if ( Value & ( 1 <<  9 ) ) { return 10; }
        if ( Value & ( 1 <<  8 ) ) { return  9; }
#endif
        if ( Value & ( 1 <<  7 ) ) { return  8; }
        if ( Value & ( 1 <<  6 ) ) { return  7; }
        if ( Value & ( 1 <<  5 ) ) { return  6; }
        if ( Value & ( 1 <<  4 ) ) { return  5; }
        if ( Value & ( 1 <<  3 ) ) { return  4; }
        if ( Value & ( 1 <<  2 ) ) { return  3; }
        if ( Value & ( 1 <<  1 ) ) { return  2; }

        return ( t_u8 ) 1;
    }
}

#endif
#ifndef ttc_driver_string_log_decimal  // no low-level implementation: use default implementation
t_u8 ttc_string_interface_log_decimal( t_base_signed Value ) {
    if ( Value < 0 )
    { Value = -Value; }

    if ( Value > 999999999 ) { return 10; }
    if ( Value >  99999999 ) { return 9; }
    if ( Value >   9999999 ) { return 8; }
    if ( Value >    999999 ) { return 7; }
    if ( Value >     99999 ) { return 6; }
    if ( Value >      9999 ) { return 5; }
    if ( Value >       999 ) { return 4; }
    if ( Value >        99 ) { return 3; }
    if ( Value >         9 ) { return 2; }
    return 1;
}
#endif
#ifndef ttc_driver_string_xtoa  // no low-level implementation: use default implementation
t_u8 ttc_string_interface_xtoa( t_base Value, t_u8* Buffer, t_base MaxSize ) {
    Assert_STRING_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    t_u8 BytesWritten = 0;

    if ( Value == 0 ) { // Value == 0
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        *Buffer++ = '0';
        return 1;
    }

#if TARGET_DATA_WIDTH==32
    t_base Base = 0xf0000000;
    t_u8  Shift = 7 * 4;
#endif
#if TARGET_DATA_WIDTH==16
    t_base Base = 0xf000;
    t_u8  Shift = 3 * 4;
#endif
#if TARGET_DATA_WIDTH==8
    t_base Base = 0xf0;
    t_u8  Shift = 1 * 4;
#endif
    t_u8 Digit;

    while ( ( Base > 0 ) && ( ( Base & Value ) == 0 ) ) { // skip leading zeroes
        Base >>= 4;
        Shift -= 4;
    }

    while ( Base > 0 ) {
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        if ( Value & Base )
        { Digit = ( Value & Base ) >> Shift; }
        else
        { Digit = 0; }

        if ( Digit < 10 )
        { *Buffer++ = ( ( t_u8 ) '0' ) + Digit; }
        else
        { *Buffer++ = ( ( t_u8 ) 'a' ) + Digit - 10; }

        BytesWritten++;
        Base >>= 4;
        Shift -= 4;
    }
    return BytesWritten;
}

#endif
#ifndef ttc_driver_string_btoa  // no low-level implementation: use default implementation
t_u8 ttc_string_interface_btoa( t_base Value, t_u8* Buffer, t_base MaxSize ) {
    Assert_STRING_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    if ( MaxSize < 1 )
    { return 0; }  // no space left in buffer

    if ( Value == 0 ) { // Value == 0
        *Buffer++ = '0';
        return 1;
    }

    while ( ( Value & ( 1 << ( TARGET_DATA_WIDTH - 1 ) ) ) == 0 ) // find first non-zero bit
    { Value = Value << 1; }

    *Buffer++ = '1';
    Value = ( Value << 1 ) | 1; // shift value to the left and set sentinel bit
    t_u8 BytesWritten = 1;
    MaxSize--;

    while ( ( Value != ( 1 << ( TARGET_DATA_WIDTH - 1 ) ) )  // will stop when value contains only sentinel bit
            && MaxSize                                      // or if buffer full
          ) {
        if ( Value & ( 1 << ( TARGET_DATA_WIDTH - 1 ) ) )
        { *Buffer++ = '1'; }
        else
        { *Buffer++ = '0'; }

        Value = Value << 1;
        MaxSize--;
        BytesWritten++;
    }

    return BytesWritten;
}

#endif
#ifndef ttc_driver_string_itoa  // no low-level implementation: use default implementation
t_u8 ttc_string_interface_itoa( t_base_signed Value, t_u8* Buffer, t_base MaxSize ) {
    Assert_STRING_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    t_u8 BytesWritten = 0;

    if ( Value == 0 ) { // Value == 0
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        *Buffer++ = '0';
        return 1;
    }
    if ( Value < 0 )  { // print minus
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        *Buffer++ = '-';
        Value *= -1;
        BytesWritten++;
    }

#if TARGET_DATA_WIDTH==32
    t_base Base = 1000000000;
#endif
#if TARGET_DATA_WIDTH==16
    t_base Base = 10000;
#endif
#if TARGET_DATA_WIDTH==8
    t_base Base = 100;
#endif
    t_u8 Digit;

    while ( ( Base > 0 ) && ( Base > Value ) ) // skip leading zeroes
    { Base /= 10; }

    while ( Base > 0 ) {
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        if ( Value >= Base ) {
            Digit = Value / Base;
            Value -= Digit * Base;
        }
        else
        { Digit = 0; }

        *Buffer++ = '0' + Digit;
        BytesWritten++;

        Base /= 10;
    }
    return BytesWritten;
}

#endif
#ifndef ttc_driver_string_byte2int  // no low-level implementation: use default implementation
t_u8 ttc_string_interface_byte2int( t_s8 Value, t_u8* Buffer ) {
    Assert_STRING_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    if ( Value < 0 ) { // negative value
        *Buffer++ = '-';
        return 1 + _driver_string_byte2uint( ( t_u8 ) 0x7f & Value, Buffer );
    }
    return _driver_string_byte2uint( ( t_u8 ) Value, Buffer );
}

#endif
#ifndef ttc_driver_string_byte2hex  // no low-level implementation: use default implementation
void ttc_string_interface_byte2hex( t_u8* Buffer, t_u8 Byte ) {
    Assert_STRING_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    t_u8 Nibble = Byte & 0xf;

    if ( Nibble < 10 )
    { *Buffer++ = '0' + Nibble; }
    else
    { *Buffer++ = 'a' - 10 + Nibble; }

    Nibble = Byte >> 4;
    if ( Nibble < 10 )
    { *Buffer = '0' + Nibble; }
    else
    { *Buffer = 'a' - 10 + Nibble; }
}

#endif
#ifndef ttc_driver_string_itoa_unsigned  // no low-level implementation: use default implementation
t_u8 ttc_string_interface_itoa_unsigned( t_base Value, t_u8* Buffer, t_base MaxSize ) {
    Assert_STRING_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    t_u8 BytesWritten = 0;

    if ( Value == 0 ) { // Value == 0
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        *Buffer++ = '0';
        return 1;
    }

#if TARGET_DATA_WIDTH==32
    t_base Base = 1000000000;
#endif
#if TARGET_DATA_WIDTH==16
    t_base Base = 10000;
#endif
#if TARGET_DATA_WIDTH==8
    t_base Base = 100;
#endif
    t_u8 Digit;

    while ( ( Base > 0 ) && ( Base > Value ) ) // skip leading zeroes
    { Base /= 10; }

    while ( Base > 0 ) {
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        if ( Value >= Base ) {
            Digit = Value / Base;
            Value -= Digit * Base;
        }
        else
        { Digit = 0; }

        *Buffer++ = '0' + Digit;
        BytesWritten++;

        Base /= 10;
    }
    return BytesWritten;
}

#endif
#ifndef ttc_driver_string_byte2uint  // no low-level implementation: use default implementation
t_u8 ttc_string_interface_byte2uint( t_u8 Value, t_u8* Buffer ) {
    Assert_STRING_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    if ( Value >= 100 ) {
        t_u8 Digit = Value / 100;
        *Buffer++ = '0' + Digit;
        Value -= Digit * 100;

        Digit = Value / 10;
        *Buffer++ = '0' + Digit;
        Value -= Digit * 10;

        *Buffer++ = '0' + Value;
        return 3;
    }
    if ( Value >= 10 ) {
        t_u8 Digit = Value / 10;
        *Buffer++ = '0' + Digit;
        Value -= Digit * 10;

        *Buffer++ = '0' + Value;
        return 2;
    }

    *Buffer++ = '0' + Value;
    return 1;
}

#endif
#ifndef ttc_driver_string_compare  // no low-level implementation: use default implementation
t_base_signed ttc_string_interface_compare( const char* StringA, const char* StringB, t_base MaxLen ) {
    Assert_STRING_Readable( StringA, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_STRING_Readable( StringB, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_base_signed Position = 1;

    while ( MaxLen-- ) {
        t_u8 A = * ( ( t_u8* ) StringA++ );
        t_u8 B = * ( ( t_u8* ) StringB++ );
        if ( A == B ) {
            if ( A != 0 ) // proceed to next position
            { Position++; }
            else
            { return 0; } // End of string: StringA[] == StringB[]
        }
        else { // StringA[] != StringB[]
            if ( A < B )
            { return -Position; }
            else
            { return Position; }
        }
    }

    return 0; // StringA[0..MaxLen-1] == StringB[0..MaxLen-1]
}

#endif

#ifndef ttc_driver_string_ftoa  // no low-level implementation: use default implementation
t_u8 ttc_string_interface_ftoa( ttm_number Value, t_u8* Buffer, t_base MaxSize, t_s8 DecNumber ) {
    Assert_STRING_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    t_u8 BytesWritten = 0;

    if ( Value == 0 ) { // Value == 0
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        *Buffer++ = '0';
        return 1;
    }
    if ( Value < 0 )  { // print minus
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        *Buffer++ = '-';
        Value *= -1;
        BytesWritten++;
    }

#if TARGET_DATA_WIDTH==32
    t_base Base = 1000000000;
#endif
#if TARGET_DATA_WIDTH==16
    t_base Base = 10000;
#endif
#if TARGET_DATA_WIDTH==8
    t_base Base = 100;
#endif
    t_u8 Digit;

    while ( ( Base > 0 ) && ( Base > Value ) ) // Skip leading zeroes
    { Base /= 10; }
    // First, we are going to save the non-decimal part
    t_u32 Fraction = 0;    //Maximum of 23 (Non-decimal part)
    ttm_number Counter = Value;

    while ( Counter >= 1 ) {
        Fraction++;
        Counter--;
    }

    if ( ( Value > 0 ) && ( Value < 1 ) ) {
        *Buffer++ = '0';
        BytesWritten++;
    }

    while ( Base > 0 ) {
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        if ( Fraction >= Base ) {
            Digit = Fraction / Base;
            Fraction -= Digit * Base;
        }
        else
        { Digit = 0; }

        *Buffer++ = '0' + Digit;
        BytesWritten++;

        Base /= 10;
    }
    if ( Counter == 0 ) {    // Value was an integer
        return BytesWritten;
    }

    *Buffer++ = '.';   //We print the decimal point
    BytesWritten++;

    //t_u32 decimals = Value - counter;   //We extract the decimal part of the number

    // We have to reset the Base variable for the second part of the number
#if TARGET_DATA_WIDTH==32
    Base = 1000000000;
#endif
#if TARGET_DATA_WIDTH==16
    Base = 10000;
#endif
#if TARGET_DATA_WIDTH==8
    Base = 100;
#endif

    t_u32 decimals = Counter * Base;
    Base /= 10;    //First division to print the decimal part

    while ( ( Base > 0 ) && ( DecNumber ) ) {
        if ( BytesWritten >= MaxSize )
        { return BytesWritten; }  // Buffer is full

        if ( decimals >= Base ) {
            Digit = decimals / Base;
            decimals -= Digit * Base;
        }
        else  { Digit = 0; }

        *Buffer++ = '0' + Digit;
        BytesWritten++;

        Base /= 10;
        DecNumber--;
    }

    while ( *Buffer == '0' ) { //We delete the right zeros in decimal part
        BytesWritten--;
        Buffer--;
    }

    return BytesWritten;
}
#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_string_interface_foo(t_ttc_string_config* Config) { ... }

t_u8* _ttc_driver_string_snprintf( t_u8* Buffer, t_base Size, const char* Format, va_list Arguments ) {
    t_u8* Writer        = ( t_u8* ) Buffer;
    t_u8* BufferEnd     = ( t_u8* ) Buffer + Size;
    t_u8* Reader_Format = ( t_u8* ) Format;
    t_tss_flags Flags;

    t_u8 C = 0;
    t_base_signed Value;

    for ( ; ( ( C = *Reader_Format ) != 0 ) && ( Writer < BufferEnd ); Reader_Format++ ) {
        if ( C != '%' ) { // copy character
            *Writer++ = C;
        }
        else {            // interpret format string
            // Reset flags for new format
            Flags.MinLength    = 0;
            Flags.LeadingZeros = 0;
            Flags.InsideFormat = 1;
            Flags.FloatPoint   = 0;

            do {
                Reader_Format++;
                C = *Reader_Format;

                switch ( C ) {

                    case 'c': { // Single character
                        *Writer++ = ( t_u8 ) va_arg( Arguments, int );
                        Flags.InsideFormat = 0;
                        break;
                    }
                    case '0': { // Leading zeros
                        Flags.LeadingZeros = 1;
                        break;
                    }
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9': { // Fixed length
                        t_u8 MinLength = 0;
                        while ( ( C > '0' ) && ( C <= '9' ) ) {
                            MinLength = MinLength * 10 + C - '0';
                            C = *++Reader_Format;
                        }
                        Flags.MinLength = MinLength;
                        Reader_Format--;
                        break;
                    }
                    case 'b': { // Unsigned binary integer
                        Value = va_arg( Arguments, t_base );

                        if ( Flags.MinLength ) { // prefix with spaces/ zeroes
                            Flags.MinLength -=  _driver_string_log_binary( Value );
                            if ( Flags.LeadingZeros ) {
                                while ( ( Flags.MinLength-- > 0 )  && ( Writer < BufferEnd ) )
                                { *Writer++ = '0'; }
                            }
                            else {
                                while ( ( Flags.MinLength-- > 0 )  && ( Writer < BufferEnd ) )
                                { *Writer++ = ' '; }
                            }
                        }
                        Writer +=  _driver_string_btoa( Value, Writer, BufferEnd - Writer );

                        Flags.InsideFormat = 0;
                        break;
                    }
                    case 'd':
                    case 'u':
                    case 'i': { // Decimal integer
                        Value = va_arg( Arguments, t_base_signed );

                        if ( Flags.MinLength ) { // Prefix with spaces/ zeroes
                            Flags.MinLength -=  _driver_string_log_decimal( Value );
                            if ( Value < 0 )
                            { Flags.MinLength--; } // Take leading minus into account
                            if ( Flags.LeadingZeros ) {
                                while ( ( Flags.MinLength-- > 0 )  && ( Writer < BufferEnd ) )
                                { *Writer++ = '0'; }
                            }
                            else {
                                while ( ( Flags.MinLength-- > 0 )  && ( Writer < BufferEnd ) )
                                { *Writer++ = ' '; }
                            }
                        }

                        if ( C == 'u' ) // Unsigned value
                        { Writer +=  _driver_string_itoa_unsigned( ( t_base ) Value, Writer, BufferEnd - Writer );}
                        else
                        { Writer +=  _driver_string_itoa( Value, Writer, BufferEnd - Writer ); }

                        Flags.InsideFormat = 0;
                        break;
                    }
                    case 'x': { // Hexadecimal integer
                        Value = va_arg( Arguments, t_base );

                        if ( Flags.MinLength ) { // prefix with spaces/ zeroes
                            Flags.MinLength -=  _driver_string_log_hex( Value );
                            if ( Flags.LeadingZeros ) {
                                while ( ( Flags.MinLength-- > 0 )  && ( Writer < BufferEnd ) )
                                { *Writer++ = '0'; }
                            }
                            else {
                                while ( ( Flags.MinLength-- > 0 )  && ( Writer < BufferEnd ) )
                                { *Writer++ = ' '; }
                            }
                        }

                        Writer +=  _driver_string_xtoa( Value, Writer, BufferEnd - Writer );
                        Flags.InsideFormat = 0;
                        break;
                    }
                    case 's': { // String
                        Writer +=  _driver_string_copy( Writer, ( t_u8* ) va_arg( Arguments, char* ), BufferEnd - Writer );
                        Flags.InsideFormat = 0;
                        break;
                    }
                    case '.': {
                        t_u8 MinLengthFloat = 0;
                        C = *++Reader_Format;   //We skip the "dot" character and evaluate the amount of digits
                        while ( ( C > '0' ) && ( C <= '9' ) ) {
                            MinLengthFloat = MinLengthFloat * 10 + C - '0';
                            C = *++Reader_Format;
                        }
                        Flags.FloatPoint = MinLengthFloat;    //In this flag we introduce the amount of decimal digits we want to print
                        Reader_Format--;
                        break;
                    }
                    case 'f': { //Floating point number
                        ttm_number ValueF = va_arg( Arguments, double );
                        t_s8 DecNumber;

                        if ( Flags.FloatPoint ) {  //We have requested certain amount of decimal digits
                            DecNumber = Flags.FloatPoint;
                        }
                        else {                     //We have not requested an amount of decimal digits
                            DecNumber = 12;       //Random number to limit the amount of decimal digits
                        }

                        Writer += _driver_string_ftoa( ValueF, Writer, BufferEnd - Writer, DecNumber );
                        Flags.InsideFormat = 0;
                        break;
                    }
                    default: {
                        if ( C ) {
                            *Writer++ = C;
                        }
                        Flags.InsideFormat = 0;
                        break;
                    }
                }
            }
            while ( ( C != 0 ) && Flags.InsideFormat && ( Writer < BufferEnd ) );
        }
    }

    if ( Writer < BufferEnd )
    { *Writer = 0; }
    else
    { *( Writer - 1 ) = 0; }

    return Writer;
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
