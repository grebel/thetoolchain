/** { ttc_packet_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for PACKET device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_packet_*() functions.
 *  Instead they can call _driver_packet_*() pendants.
 *  E.g.: ttc_packet_reset() -> _driver_packet_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20150928 09:30:04 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_packet_interface.h".
//
#include "ttc_packet_interface.h"
#include "../ttc_basic_types.h"
#include "../ttc_memory.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_packet_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_prepare() (using default implementation)!

void ttc_packet_interface_prepare() {


#warning Function ttc_packet_interface_prepare() is empty.


}

#endif
#ifndef ttc_driver_packet_address_source_compare  // no low-level implementation: use default implementation

e_ttc_packet_address_format ttc_packet_interface_address_source_compare( t_ttc_packet* Packet, const t_ttc_packet_address* Address ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_PACKET_Readable( Address, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_base* Pointer_Address = NULL;
    t_u16*  Pointer_PanID   = NULL;
    e_ttc_packet_address_format AddressFormat = _driver_packet_address_source_get_pointer( Packet, ( void** ) &Pointer_Address, &Pointer_PanID );

    if ( Pointer_PanID ) {
        if ( *Pointer_PanID != Address->PanID )
        { return 0; } // wrong PanID
    }

    switch ( AddressFormat ) {
        case E_ttc_packet_address_Source16: { // packet stores 16 bit address
            if ( *( ( t_u16* ) Pointer_Address ) == Address->Address16 )
            { return AddressFormat; }
            break;
        }
        case E_ttc_packet_address_Source64: { // packet stores 64 bit address
#if TARGET_DATA_WIDTH == 32  // implementation for 32-bit wide data bus
            const t_u32* Address64 = Address->Address64.Words;
            if ( ( Pointer_Address[0] == Address64[0] ) &&
                    ( Pointer_Address[1] == Address64[1] )
               )
#else                         // implementation for 16-bit wide data bus
            const t_u16* Address64 = ( const t_u16* ) Address->Address64.Words;

            if ( ( ( ( t_u16* ) Pointer_Address )[0] == Address64[0] ) &&
                    ( ( ( t_u16* ) Pointer_Address )[1] == Address64[1] ) &&
                    ( ( ( t_u16* ) Pointer_Address )[2] == Address64[2] ) &&
                    ( ( ( t_u16* ) Pointer_Address )[3] == Address64[3] )
               )
#endif
                return AddressFormat; // 64-bit address has matched
            break;
        }
        default: ttc_assert_halt_origin( ec_packet_InvalidImplementation ); break; // unexpected address format (check implementation of E_ttc_packet_address_source_get_pointer()
    }

    return 0; // no source address found in packet
}

#endif
#ifndef ttc_driver_packet_address_target_compare  // no low-level implementation: use default implementation

e_ttc_packet_address_format ttc_packet_interface_address_target_compare( t_ttc_packet* Packet, t_ttc_packet_address* Address ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_PACKET_Writable( Address, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_base* Pointer_Address = NULL;
    t_u16*  Pointer_PanID   = NULL;
    e_ttc_packet_address_format AddressFormat = _driver_packet_address_target_get_pointer( Packet, ( void** ) &Pointer_Address, &Pointer_PanID );
    if ( Pointer_PanID ) {
        if ( *Pointer_PanID != Address->PanID )
        { return 0; } // wrong PanID
    }

    switch ( AddressFormat ) {
        case E_ttc_packet_address_Target16: // packet stores 16 bit address
            if ( *( ( t_u16* ) Pointer_Address ) == Address->Address16 )
            { return AddressFormat; }
            break;
        case E_ttc_packet_address_Target64: { // packet stores 64 bit address
#if TARGET_DATA_WIDTH == 32  // implementation for 32-bit wide data bus
            t_u32* Address64 = ( t_u32* ) Address->Address64.Words;
            if (
                ( Pointer_Address[0] == Address64[0] ) &&
                ( Pointer_Address[1] == Address64[1] )
            )
#else                         // implementation for 16-bit wide data bus
            t_u16* Address64 = ( t_u32* ) Address->Address64.Words;
            if (
                ( ( ( t_u16* ) Pointer_Address )[0] == Address64[0] ) &&
                ( ( ( t_u16* ) Pointer_Address )[1] == Address64[1] ) &&
                ( ( ( t_u16* ) Pointer_Address )[2] == Address64[2] ) &&
                ( ( ( t_u16* ) Pointer_Address )[3] == Address64[3] )
            )
#endif
                return AddressFormat; // 64-bit address has matched
            break;
        }
        default: ttc_assert_halt_origin( ec_packet_InvalidImplementation ); break; // unexpected address format (check implementation of E_ttc_packet_address_source_get_pointer()
    }

    return E_ttc_packet_address_None;
}

#endif
#ifndef ttc_driver_packet_address_source_get_pointer  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_address_source_get_pointer() (using default implementation)!

e_ttc_packet_address_format ttc_packet_interface_address_source_get_pointer( t_ttc_packet* Packet, void** Pointer2SourceID, t_u16** Pointer2SourcePanID ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_PACKET_Writable( AddressPtr, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_address_source_get_pointer() is empty.

    return E_ttc_packet_address_None;
}

#endif
#ifndef ttc_driver_packet_payload_get_size_max  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_payload_get_size_max() (using default implementation)!

t_u16 ttc_packet_interface_payload_get_size_max( t_ttc_packet* Packet ) {
    Assert_PACKET_Readable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_payload_get_size_max() is empty.

    return 0;
}

#endif
#ifndef ttc_driver_packet_payload_get_pointer  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_payload_get_pointer() (using default implementation)!

t_u8 ttc_packet_interface_payload_get_pointer( t_ttc_packet* Packet, t_u8** PayloadPtr, t_u8* SizePtr, e_ttc_packet_pattern* ProtocolPtr ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_PACKET_Writable( PayloadPtr, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    if ( ProtocolPtr ) {
        Assert_PACKET_Writable( ProtocolPtr, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    }
    if ( ProtocolPtr ) {
        Assert_PACKET_Writable( ProtocolPtr, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    }
#warning Function ttc_packet_interface_payload_get_pointer() is empty.

    return E_ttc_packet_address_None;
}

#endif
#ifndef ttc_driver_packet_address_target_get_pointer  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_address_target_get_pointer() (using default implementation)!

e_ttc_packet_address_format ttc_packet_interface_address_target_get_pointer( t_ttc_packet* Packet, void** Pointer2TargetID, t_u16** Pointer2TargetPanID ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_PACKET_Writable( AddressPtr, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_address_target_get_pointer() is empty.

    return E_ttc_packet_address_None;
}

#endif
#ifndef ttc_driver_packet_payload_get_size  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_payload_get_size() (using default implementation)!

t_u16 ttc_packet_interface_payload_get_size( t_ttc_packet* Packet ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_payload_get_size() is empty.

    return 0;
}

#endif
#ifndef ttc_driver_packet_address_source_set  // no low-level implementation: use default implementation

e_ttc_packet_address_format ttc_packet_interface_address_source_set( t_ttc_packet* Packet, const t_ttc_packet_address* Address ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_PACKET_Readable( Address, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_base* Pointer_Address = NULL;
    t_u16*  PacketPanIDPtr  = NULL;

    e_ttc_packet_address_format AddressFormat = _driver_packet_address_source_get_pointer( Packet, ( void** ) &Pointer_Address, &PacketPanIDPtr );

    if ( PacketPanIDPtr )
    { *PacketPanIDPtr = Address->PanID; } // packet stores a target PanID: update it

    switch ( AddressFormat ) {
        case E_ttc_packet_address_Source16: { // packet stores 16 bit address
            *( ( t_u16* ) Pointer_Address ) = Address->Address16;
            return 1; // 16-bit address stored as source address in packet
        }
        case E_ttc_packet_address_Source64: { // packet stores 64 bit address
#if TARGET_DATA_WIDTH == 32  // implementation for 32-bit wide data bus
            t_u32* Address64 = ( t_u32* ) Address->Address64.Words;
            Pointer_Address[0] = Address64[0];
            Pointer_Address[1] = Address64[1];
#else                         // implementation for 16-bit wide data bus
            t_u16* Address64 = ( t_u32* ) Address->Address64.Words;
            ( ( t_u16* ) Pointer_Address )[0] = Address64[0];
            ( ( t_u16* ) Pointer_Address )[1] = Address64[1];
            ( ( t_u16* ) Pointer_Address )[2] = Address64[2];
            ( ( t_u16* ) Pointer_Address )[3] = Address64[3];
#endif
            return 2; // 64-bit address stored as source address in packet
            break;
        }
        default: break; // packet has no source address field
    }

    return E_ttc_packet_address_None; // cannot store address in packet
}

#endif
#ifndef ttc_driver_packet_address_target_set  // no low-level implementation: use default implementation

e_ttc_packet_address_format ttc_packet_interface_address_target_set( t_ttc_packet* Packet, const t_ttc_packet_address* Address ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    if ( Address )
    { Assert_PACKET_Readable( Address, ttc_assert_origin_auto ); }  // check if it points to RAM or ROM

    t_base* Pointer_Address  = NULL;
    t_u16*  PacketPanIDPtr   = NULL;
    e_ttc_packet_address_format AddressFormat = _driver_packet_address_target_get_pointer( Packet, ( void** ) &Pointer_Address, &PacketPanIDPtr );

    if ( PacketPanIDPtr )
    { *PacketPanIDPtr = Address->PanID; } // packet stores a target PanID: update it

    switch ( AddressFormat ) {
        case E_ttc_packet_address_Target16: // packet stores 16 bit address
            if ( Address ) // set given address
            { *( ( t_u16* ) Pointer_Address ) = Address->Address16; }
            else           // set broadcast address
            { *( ( t_u16* ) Pointer_Address ) = 0xffff; }
            return 1; // 16-bit address stored as target address in packet

        case E_ttc_packet_address_Target64: { // packet stores 64 bit address
            if ( Address )  { // set given address
#if TARGET_DATA_WIDTH == 32  // implementation for 32-bit wide data bus
                t_u32* Address64 = ( t_u32* ) Address->Address64.Words;
                Pointer_Address[0] = Address64[0];
                Pointer_Address[1] = Address64[1];
#else                         // implementation for 16-bit wide data bus
                t_u16* Address64 = ( t_u32* ) Address->Address64.Words;
                ( ( t_u16* ) Pointer_Address )[0] = Address64[0];
                ( ( t_u16* ) Pointer_Address )[1] = Address64[1];
                ( ( t_u16* ) Pointer_Address )[2] = Address64[2];
                ( ( t_u16* ) Pointer_Address )[3] = Address64[3];
#endif
            }
            else {         // set broadcast address
#if TARGET_DATA_WIDTH == 32  // implementation for 32-bit wide data bus
                Pointer_Address[0] = 0xffffffff;
                Pointer_Address[1] = 0xffffffff;
#else                         // implementation for 16-bit wide data bus
                ( ( t_u16* ) Pointer_Address )[0] = 0xffff;
                ( ( t_u16* ) Pointer_Address )[1] = 0xffff;
                ( ( t_u16* ) Pointer_Address )[2] = 0xffff;
                ( ( t_u16* ) Pointer_Address )[3] = 0xffff;
#endif
            }
            return 2; // 64-bit address stored as target address in packet
            break;
        }
        default: break; // packet has no target address field
    }

    return E_ttc_packet_address_None; // cannot store address in packet
}

#endif
#ifndef ttc_driver_packet_identify  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_identify() (using default implementation)!

e_ttc_packet_category ttc_packet_interface_identify( t_ttc_packet* Packet ) {
    Assert_PACKET_Readable( PacketHeader, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_identify() is empty.

    return ( e_ttc_packet_category ) 0;
}

#endif
#ifndef ttc_driver_packet_initialize  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_initialize() (using default implementation)!

void ttc_packet_interface_initialize( t_ttc_packet* Packet, e_ttc_packet_type Type ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_initialize() is empty.


}

#endif
#ifndef ttc_driver_packet_mac_header_get_size  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_mac_header_get_size() (using default implementation)!

t_u8 ttc_packet_interface_mac_header_get_size( t_ttc_packet* Packet ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_mac_header_get_size() is empty.

    return 0;
}

#endif
#ifndef ttc_driver_packet_payload_set_size  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_payload_set_size() (using default implementation)!

void ttc_packet_interface_payload_set_size( t_ttc_packet* Packet, t_u16 PayloadSize ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    Packet->MAC.Length = ttc_driver_packet_mac_header_get_size( Packet ) + PayloadSize;
}

#endif
#ifndef ttc_driver_packet_acknowledge_request_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_acknowledge_request_get() (using default implementation)!

BOOL ttc_packet_interface_acknowledge_request_get( t_ttc_packet* Packet ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_acknowledge_request_get() is empty.

    return ( BOOL ) 0;
}

#endif
#ifndef ttc_driver_packet_acknowledge_request_set  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_acknowledge_request_set() (using default implementation)!

void ttc_packet_interface_acknowledge_request_set( t_ttc_packet* Packet, BOOL RequestACK ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_acknowledge_request_set() is empty.


}

#endif
#ifndef ttc_driver_packet_address_target_get  // no low-level implementation: use default implementation
e_ttc_packet_address_format ttc_packet_interface_address_target_get( t_ttc_packet* Packet, t_ttc_packet_address* Address ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_PACKET_Writable( Address, ttc_assert_origin_auto ); // check if it points to RAM

    t_base* Pointer_Address  = NULL;
    t_u16*  PacketPanIDPtr   = NULL;
    e_ttc_packet_address_format AddressFormat = _driver_packet_address_target_get_pointer( Packet, ( void** ) &Pointer_Address, &PacketPanIDPtr );

    if ( PacketPanIDPtr )
    { Address->PanID = *PacketPanIDPtr; } // packet stores a target PanID: update it
    else
    {Address->PanID =  0; }

    switch ( AddressFormat ) {
        case E_ttc_packet_address_Target16: { // packet stores 16 bit address
            Address->Address16 = *( ( t_u16* ) Pointer_Address ) ;
#if TARGET_DATA_WIDTH == 32  // implementation for 32-bit wide data bus
            t_u32* Address64 = ( t_u32* ) Address->Address64.Words;
            Address64[0] = 0;
            Address64[1] = 0;
#else                         // implementation for 8-/16-bit wide data bus
            t_u16* Address64 = ( t_u16* ) Address->Address64.Words;
            Address64[0] = 0;
            Address64[1] = 0;
            Address64[2] = 0;
            Address64[3] = 0;
#endif
            return 1; // 16-bit address stored
        }
        case E_ttc_packet_address_Target64: { // packet stores 64 bit address
#if TARGET_DATA_WIDTH == 32  // implementation for 32-bit wide data bus
            t_u32* Address64 = ( t_u32* ) Address->Address64.Words;
            Address64[0] = Pointer_Address[0];
            Address64[1] = Pointer_Address[1];
#else                         // implementation for 8-/16-bit wide data bus
            t_u16* Address64 = ( t_u16* ) Address->Address64.Words;
            Address64[0]( ( t_u16* ) Pointer_Address )[0];
            Address64[1]( ( t_u16* ) Pointer_Address )[1];
            Address64[2]( ( t_u16* ) Pointer_Address )[2];
            Address64[3]( ( t_u16* ) Pointer_Address )[3];
#endif
            Address->Address16 = 0;
            return 2; // 64-bit address stored
            break;
        }
        default: break; // packet has no target address field
    }

    return E_ttc_packet_address_None; // cannot store address in packet
}

#endif
#ifndef ttc_driver_packet_address_source_get  // no low-level implementation: use default implementation
e_ttc_packet_address_format ttc_packet_interface_address_source_get( t_ttc_packet* Packet, t_ttc_packet_address* Address ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_PACKET_Writable( Address, ttc_assert_origin_auto ); // check if it points to RAM

    t_base* Pointer_Address  = NULL;
    t_u16*  PacketPanIDPtr   = NULL;
    e_ttc_packet_address_format AddressFormat = _driver_packet_address_source_get_pointer( Packet, ( void** ) &Pointer_Address, &PacketPanIDPtr );

    if ( PacketPanIDPtr )
    { Address->PanID = *PacketPanIDPtr; } // packet stores a source PanID: update it
    else
    {Address->PanID =  0; }

    switch ( AddressFormat ) {
        case E_ttc_packet_address_Source16: { // packet stores 16 bit address
            Address->Address16 = *( ( t_u16* ) Pointer_Address ) ;
#if TARGET_DATA_WIDTH == 32  // implementation for 32-bit wide data bus
            t_u32* Address64 = ( t_u32* ) Address->Address64.Words;
            Address64[0] = 0;
            Address64[1] = 0;
#else                         // implementation for 8-/16-bit wide data bus
            t_u16* Address64 = ( t_u16* ) Address->Address64.Words;
            Address64[0] = 0;
            Address64[1] = 0;
            Address64[2] = 0;
            Address64[3] = 0;
#endif
            return 1; // 16-bit address stored
        }
        case E_ttc_packet_address_Source64: { // packet stores 64 bit address
#if TARGET_DATA_WIDTH == 32  // implementation for 32-bit wide data bus
            t_u32* Address64 = ( t_u32* ) Address->Address64.Words;
            Address64[0] = Pointer_Address[0];
            Address64[1] = Pointer_Address[1];
#else                         // implementation for 8-/16-bit wide data bus
            t_u16* Address64 = ( t_u16* ) Address->Address64.Words;
            Address64[0]( ( t_u16* ) Pointer_Address )[0];
            Address64[1]( ( t_u16* ) Pointer_Address )[1];
            Address64[2]( ( t_u16* ) Pointer_Address )[2];
            Address64[3]( ( t_u16* ) Pointer_Address )[3];
#endif
            Address->Address16 = 0;
            return 2; // 64-bit address stored
            break;
        }
        default: break; // packet has no source address field
    }

    return E_ttc_packet_address_None; // cannot store address in packet
}

#endif
#ifndef ttc_driver_packet_type_get  // no low-level implementation: use default implementation
e_ttc_packet_type ttc_packet_interface_type_get( t_ttc_packet* Packet ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    if ( !Packet->Meta.Type ) { // missing packet type in meta block: identify packet
        ttc_driver_packet_identify( Packet );
    }

    return ( e_ttc_packet_address_format ) Packet->Meta.Type;
}

#endif
#ifndef ttc_driver_packet_address_reply2  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_address_reply2() (using default implementation)!

void ttc_packet_interface_address_reply2( t_ttc_packet* Packet, t_ttc_packet* PacketReply ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_PACKET_Writable( PacketReply, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_address_reply2() is empty.


}

#endif
#ifndef ttc_driver_packet_is_acknowledge  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_is_acknowledge() (using default implementation)!

BOOL ttc_packet_interface_is_acknowledge( t_ttc_packet* Packet ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_is_acknowledge() is empty.

    return ( BOOL ) 0;
}

#endif
#ifndef ttc_driver_packet_is_command  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_is_command() (using default implementation)!

BOOL ttc_packet_interface_is_command( t_ttc_packet* Packet ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_is_command() is empty.

    return ( BOOL ) 0;
}

#endif
#ifndef ttc_driver_packet_is_data  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_is_data() (using default implementation)!

BOOL ttc_packet_interface_is_data( t_ttc_packet* Packet ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_is_data() is empty.

    return ( BOOL ) 0;
}

#endif
#ifndef ttc_driver_packet_is_beacon  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_is_beacon() (using default implementation)!

BOOL ttc_packet_interface_is_beacon( t_ttc_packet* Packet ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_is_beacon() is empty.

    return ( BOOL ) 0;
}

#endif
#ifndef ttc_driver_packet_acknowledge  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_acknowledge() (using default implementation)!

void ttc_packet_interface_acknowledge( t_ttc_packet* Packet, t_ttc_packet* PacketACK ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_PACKET_Writable( PacketACK, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_acknowledge() is empty.


}

#endif
#ifndef ttc_driver_packet_sequence_number_set  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_sequence_number_set() (using default implementation)!

void ttc_packet_interface_sequence_number_set( t_ttc_packet* Packet, t_u8 SequenceNumber ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_sequence_number_set() is empty.


}

#endif
#ifndef ttc_driver_packet_sequence_number_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_sequence_number_get() (using default implementation)!

t_u8 ttc_packet_interface_sequence_number_get( t_ttc_packet* Packet ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_packet_interface_sequence_number_get() is empty.

    return ( t_u8 ) 0;
}

#endif
#ifndef ttc_driver_packet_pattern_matches_type  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_pattern_matches_type() (using default implementation)!

BOOL ttc_packet_interface_pattern_matches_type( e_ttc_packet_type Type, e_ttc_packet_pattern Pattern ) {


#warning Function ttc_packet_interface_pattern_matches_type() is empty.

    return ( BOOL ) 0;
}

#endif
#ifndef ttc_driver_packet_mac_header_get_type_size  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_packet_mac_header_get_type_size() (using default implementation)!

t_u8 ttc_packet_interface_mac_header_get_type_size( e_ttc_packet_type Type ) {


#warning Function ttc_packet_interface_mac_header_get_type_size() is empty.

    return ( t_u8 ) 0;
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_packet_interface_foo(t_ttc_packet_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
