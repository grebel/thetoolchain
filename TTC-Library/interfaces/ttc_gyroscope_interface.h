/** { ttc_gyroscope_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for GYROSCOPE device.
 *   
 *  *_interface_*() functions may be redirected to low-level implementations in 
 *  two ways:
 *   
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *      
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than 
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *      
 *  Adding a new low-level implementation requires changes in this file.
 *  
 *  Created from template ttc_device_interface.h revision 21 at 20150121 10:09:22 UTC
 *
 *  Authors: Gregor Rebel
 *   
}*/

#ifndef TTC_GYROSCOPE_INTERFACE_H
#define TTC_GYROSCOPE_INTERFACE_H

//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_gyroscope_interface.c"
//

#include "../ttc_basic.h"

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_gyroscope_mpu6050
#  include "../gyroscope/gyroscope_mpu6050.h"
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_gyroscope_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_gyroscope_*() declaration in ../ttc_gyroscope.h, a corresponding 
 * interface part is required here. The interface will forward the declaration either   
 * to a low-level implementation or a default implementation in ttc_gyroscope_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_gyroscope.h for prototypes of low-level driver functions!
 *
 */
 
e_ttc_gyroscope_errorcode ttc_gyroscope_interface_init(t_ttc_gyroscope_config* Config); //{  macro _driver_gyroscope_interface_init(Config)
#ifdef ttc_driver_gyroscope_init
// enable following line to forward interface function as a macro definition
#  define _driver_gyroscope_init(Config) ttc_driver_gyroscope_init(Config)
#else
#  define _driver_gyroscope_init(Config) ttc_gyroscope_interface_init(Config)
#endif
//}
void ttc_gyroscope_interface_prepare(); //{  macro _driver_gyroscope_interface_prepare()
#ifdef ttc_driver_gyroscope_prepare
// enable following line to forward interface function as a macro definition
#  define _driver_gyroscope_prepare() ttc_driver_gyroscope_prepare()
#else
#  define _driver_gyroscope_prepare() ttc_gyroscope_interface_prepare()
#endif
//}
e_ttc_gyroscope_errorcode ttc_gyroscope_interface_deinit(t_ttc_gyroscope_config* Config); //{  macro _driver_gyroscope_interface_deinit(Config)
#ifdef ttc_driver_gyroscope_deinit
// enable following line to forward interface function as a macro definition
#  define _driver_gyroscope_deinit(Config) ttc_driver_gyroscope_deinit(Config)
#else
#  define _driver_gyroscope_deinit(Config) ttc_gyroscope_interface_deinit(Config)
#endif
//}
void ttc_gyroscope_interface_reset(t_ttc_gyroscope_config* Config); //{  macro _driver_gyroscope_interface_reset(Config)
#ifdef ttc_driver_gyroscope_reset
// enable following line to forward interface function as a macro definition
#  define _driver_gyroscope_reset(Config) ttc_driver_gyroscope_reset(Config)
#else
#  define _driver_gyroscope_reset(Config) ttc_gyroscope_interface_reset(Config)
#endif
//}
e_ttc_gyroscope_errorcode ttc_gyroscope_interface_get_features(t_ttc_gyroscope_config* Config); //{  macro _driver_gyroscope_interface_get_features(Config)
#ifdef ttc_driver_gyroscope_get_features
// enable following line to forward interface function as a macro definition
#  define _driver_gyroscope_get_features(Config) ttc_driver_gyroscope_get_features(Config)
#else
#  define _driver_gyroscope_get_features(Config) ttc_gyroscope_interface_get_features(Config)
#endif
//}
e_ttc_gyroscope_errorcode ttc_gyroscope_interface_load_defaults(t_ttc_gyroscope_config* Config); //{  macro _driver_gyroscope_interface_load_defaults(Config)
#ifdef ttc_driver_gyroscope_load_defaults
// enable following line to forward interface function as a macro definition
#  define _driver_gyroscope_load_defaults(Config) ttc_driver_gyroscope_load_defaults(Config)
#else
#  define _driver_gyroscope_load_defaults(Config) ttc_gyroscope_interface_load_defaults(Config)
#endif
//}
t_ttc_gyroscope_measures* ttc_gyroscope_interface_read_measures(t_ttc_gyroscope_config* Config); //{  macro _driver_gyroscope_interface_read_measures(Config)
#ifdef ttc_driver_gyroscope_read_measures
// enable following line to forward interface function as a macro definition
#  define _driver_gyroscope_read_measures(Config) ttc_driver_gyroscope_read_measures(Config)
#else
#  define _driver_gyroscope_read_measures(Config) ttc_gyroscope_interface_read_measures(Config)
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_gyroscope_interface_foo(t_ttc_gyroscope_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_GYROSCOPE_INTERFACE_H
