/** { ttc_adc_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for ADC device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 21 at 20141008 14:25:12 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_ADC_INTERFACE_H
#define TTC_ADC_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_adc_interface.c"
//

#include "../ttc_basic.h"

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_adc_stm32f1xx
    #include "../adc/adc_stm32f1xx.h"
#endif
#ifdef EXTENSION_adc_stm32l1xx
    #include "../adc/adc_stm32l1xx.h"  // low-level driver for adc devices on stm32l1xx architecture
#endif
#ifdef EXTENSION_adc_stm32l0xx
    #include "../adc/adc_stm32l0xx.h"  // low-level driver for adc devices on stm32l0xx architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_adc_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_adc_*() declaration in ../ttc_adc.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_adc_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_adc.h for prototypes of low-level driver functions!
 *
 */

e_ttc_adc_errorcode ttc_adc_interface_load_defaults( t_ttc_adc_config* Config ); //{  macro _driver_adc_interface_load_defaults(Config)
#ifdef ttc_driver_adc_load_defaults
    // enable following line to forward interface function as a macro definition
    #define _driver_adc_load_defaults(Config) ttc_driver_adc_load_defaults(Config)
#else
    #define _driver_adc_load_defaults(Config) ttc_adc_interface_load_defaults(Config)
#endif
//}
void ttc_adc_interface_prepare(); //{  macro _driver_adc_interface_prepare()
#ifdef ttc_driver_adc_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_adc_prepare() ttc_driver_adc_prepare()
#else
    #define _driver_adc_prepare() ttc_adc_interface_prepare()
#endif
//}
e_ttc_adc_errorcode ttc_adc_interface_reset( t_ttc_adc_config* Config ); //{  macro _driver_adc_interface_reset(Config)
#ifdef ttc_driver_adc_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_adc_reset(Config) ttc_driver_adc_reset(Config)
#else
    #define _driver_adc_reset(Config) ttc_adc_interface_reset(Config)
#endif
//}
e_ttc_adc_errorcode ttc_adc_interface_deinit( t_ttc_adc_config* Config ); //{  macro _driver_adc_interface_deinit(Config)
#ifdef ttc_driver_adc_deinit
    // enable following line to forward interface function as a macro definition
    #define _driver_adc_deinit(Config) ttc_driver_adc_deinit(Config)
#else
    #define _driver_adc_deinit(Config) ttc_adc_interface_deinit(Config)
#endif
//}
e_ttc_adc_errorcode ttc_adc_interface_init( t_ttc_adc_config* Config ); //{  macro _driver_adc_interface_init(Config)
#ifdef ttc_driver_adc_init
    // enable following line to forward interface function as a macro definition
    #define _driver_adc_init(Config) ttc_driver_adc_init(Config)
#else
    #define _driver_adc_init(Config) ttc_adc_interface_init(Config)
#endif
//}
e_ttc_adc_errorcode ttc_adc_interface_get_features( t_ttc_adc_config* Config ); //{  macro _driver_adc_interface_get_features(Config)
#ifdef ttc_driver_adc_get_features
    // enable following line to forward interface function as a macro definition
    #define _driver_adc_get_features(Config) ttc_driver_adc_get_features(Config)
#else
    #define _driver_adc_get_features(Config) ttc_adc_interface_get_features(Config)
#endif

e_ttc_adc_errorcode ttc_adc_interface_init_single( t_ttc_adc_config* Config ); //{  macro _driver_adc_interface_get_features(Config)
#ifdef ttc_driver_adc_init_single
    // enable following line to forward interface function as a macro definition
    #define _driver_adc_init_single(Config) ttc_driver_adc_init_single(Config)
#else
    #define _driver_adc_init_single(Config) ttc_adc_interface_init_single(Config)
#endif

e_ttc_adc_errorcode ttc_adc_interface_init_dma( t_ttc_adc_config* Config, t_u16* Value ); //{  macro _driver_adc_interface_get_features(Config)
#ifdef ttc_driver_adc_init_dma
    // enable following line to forward interface function as a macro definition
    #define _driver_adc_init_dma(Config, Value) ttc_driver_adc_init_dma(Config, Value)
#else
    #define _driver_adc_init_dma(Config, Value) ttc_adc_interface_init_dma(Config, Value)
#endif

t_u16 ttc_adc_interface_get_value( t_ttc_adc_config* Config ); //{  macro _driver_adc_interface_get_features(Config)
#ifdef ttc_driver_adc_get_value
    // enable following line to forward interface function as a macro definition
    #define _driver_adc_get_value(Config) ttc_driver_adc_get_value(Config)
#else
    #define _driver_adc_get_value(Config) ttc_adc_interface_get_value(Config)
#endif

//}
e_ttc_adc_errorcode ttc_adc_interface_find_input_pin( t_ttc_adc_config* Config ); //{  macro _driver_adc_interface_find_input_pin(Config)
#ifdef ttc_driver_adc_find_input_pin
    // enable following line to forward interface function as a macro definition
    #define _driver_adc_find_input_pin(Config) ttc_driver_adc_find_input_pin(Config)
#else
    #define _driver_adc_find_input_pin(Config) ttc_adc_interface_find_input_pin(Config)
#endif
//}
t_base_signed ttc_adc_interface_get_minimum( t_u8 LogicalIndex ); //{  macro _driver_adc_interface_get_minimum(LogicalIndex)
#ifdef ttc_driver_adc_get_minimum
    // enable following line to forward interface function as a macro definition
    #define _driver_adc_get_minimum(LogicalIndex) ttc_driver_adc_get_minimum(LogicalIndex)
#else
    #define _driver_adc_get_minimum(LogicalIndex) ttc_adc_interface_get_minimum(LogicalIndex)
#endif
//}
t_base_signed ttc_adc_interface_get_maximum( t_u8 LogicalIndex ); //{  macro _driver_adc_interface_get_maximum(LogicalIndex)
#ifdef ttc_driver_adc_get_maximum
    // enable following line to forward interface function as a macro definition
    #define _driver_adc_get_maximum(LogicalIndex) ttc_driver_adc_get_maximum(LogicalIndex)
#else
    #define _driver_adc_get_maximum(LogicalIndex) ttc_adc_interface_get_maximum(LogicalIndex)
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_adc_interface_foo(t_ttc_adc_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_ADC_INTERFACE_H
