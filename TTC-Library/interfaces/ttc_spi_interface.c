/** { ttc_spi_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for SPI device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_spi_*() functions.
 *  Instead they can call _driver_spi_*() pendants.
 *  E.g.: ttc_spi_reset() -> _driver_spi_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20140423 11:04:14 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_spi_interface.h"

/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************



#ifndef ttc_driver_spi_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_spi_prepare() (using default implementation)!

void ttc_spi_interface_prepare() {




}

#endif
#ifndef ttc_driver_spi_init  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_spi_init() (using default implementation)!

e_ttc_spi_errorcode ttc_spi_interface_init( t_ttc_spi_config* Config ) {
    Assert_SPI( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_spi_errorcode ) 0;
}

#endif
#ifndef ttc_driver_spi_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_spi_load_defaults() (using default implementation)!

e_ttc_spi_errorcode ttc_spi_interface_load_defaults( t_ttc_spi_config* Config ) {
    Assert_SPI( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_spi_errorcode ) 0;
}

#endif
#ifndef ttc_driver_spi_get_features  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_spi_get_features() (using default implementation)!

e_ttc_spi_errorcode ttc_spi_interface_get_features( t_ttc_spi_config* Config ) {
    Assert_SPI( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_spi_errorcode ) 0;
}

#endif
#ifndef ttc_driver_spi_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_spi_deinit() (using default implementation)!

e_ttc_spi_errorcode ttc_spi_interface_deinit( t_ttc_spi_config* Config ) {
    Assert_SPI( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_spi_errorcode ) 0;
}

#endif
#ifndef ttc_driver_spi_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_spi_reset() (using default implementation)!

e_ttc_spi_errorcode ttc_spi_interface_reset( t_ttc_spi_config* Config ) {
    Assert_SPI( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_spi_errorcode ) 0;
}

#endif
#ifndef ttc_driver_spi_read_word  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_spi_read_word() (using default implementation)!

e_ttc_spi_errorcode ttc_spi_interface_read_word( t_ttc_spi_config* Config, t_u16* Word ) {
    Assert_SPI( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_SPI( Word, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_spi_errorcode ) 0;
}

#endif
#ifndef ttc_driver_spi_send_raw  // no low-level implementation: use default implementation
//#  warning: missing low-level implementation for ttc_driver_spi_send_raw() (using default implementation)!

e_ttc_spi_errorcode ttc_spi_interface_send_raw( t_ttc_spi_config* Config, const t_u8* Buffer, t_u16 Amount ) {
    Assert_SPI( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_SPI( Buffer, ttc_assert_origin_auto );  // pointers must not be NULL

    while ( Amount ) {
        _driver_spi_send_word( Config, *Buffer++ );
        Amount--;
    }

    return ( e_ttc_spi_errorcode ) 0;
}

#endif
#ifndef ttc_driver_spi_send_word  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_spi_send_word() (using default implementation)!

e_ttc_spi_errorcode ttc_spi_interface_send_word( t_ttc_spi_config* Config, const t_u16 Word ) {
    Assert_SPI( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_spi_errorcode ) 0;
}

#endif
#ifndef ttc_driver_spi_read_byte  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_spi_read_byte() (using default implementation)!

e_ttc_spi_errorcode ttc_spi_interface_read_byte( t_ttc_spi_config* Config, t_u8* Byte ) {
    Assert_SPI( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_SPI( Byte, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_spi_errorcode ) 0;
}

#endif
#ifndef ttc_driver_spi_send_read  // no low-level implementation: use default implementation
e_ttc_spi_errorcode ttc_spi_interface_send_read( t_ttc_spi_config* Config, const t_u8* BufferTx, t_u16 AmountTx, t_u16 AmountZerosTx, t_u8* BufferRx, t_u16 AmountRx ) {
    Assert_SPI_Writable( Config, ttc_assert_origin_auto );        // check if it points to RAM (change for read only memory!)
    if ( AmountTx ) {
        Assert_SPI_Readable( BufferTx, ttc_assert_origin_auto );        // check if it points to RAM/FLASH/Peripheral
    }
    if ( AmountRx ) {
        Assert_SPI_Writable( BufferRx, ttc_assert_origin_auto );        // check if it points to RAM (change for read only memory!)
    }
    Assert_SPI( AmountTx + AmountZerosTx >= AmountRx, ttc_assert_origin_auto );  // must send at least one byte for every byte to receive!

    e_ttc_spi_errorcode Error = 0;

#ifdef ttc_driver_spi_send_read
    Error = ttc_driver_spi_send_read( Config, BufferTx, AmountTx, AmountZerosTx, BufferRx, AmountRx );
#else
    if ( AmountZerosTx >= AmountRx ) { // phase BufferRx is not overlapping with phase BufferTx: process phases BufferTx and BufferRx one after another
        ttc_driver_spi_send_raw( Config, BufferTx, AmountTx );
#ifdef ttc_driver_spi_read_raw
        ttc_driver_spi_read_raw( Config, BufferRx, AmountRx );
#else
        t_u16 SendAsZero = Config->Init.SendAsZero;
        if ( Config->Flags.Bits.WordSize16 ) { // 16 bit transfers
            while ( AmountZerosTx > AmountRx ) {
                ttc_driver_spi_send_word( Config, SendAsZero );
                AmountZerosTx -= 2;
            }
            while ( AmountRx > 1 ) {
                ttc_driver_spi_send_word( Config, SendAsZero );
                ttc_driver_spi_read_word( Config, ( t_u16* ) BufferRx );
                BufferRx += 2;
                AmountRx -= 2;
            }
        }
        else {                               // 8 bit transfers
            while ( AmountZerosTx > AmountRx ) {
                ttc_driver_spi_send_word( Config, SendAsZero );
                ttc_driver_spi_read_byte( Config, BufferRx );
                BufferRx ++;
                AmountZerosTx--;
            }
            while ( AmountRx > 0 ) {
                ttc_driver_spi_send_word( Config, SendAsZero );
                ttc_driver_spi_read_byte( Config, BufferRx );
                BufferRx++;
                AmountRx--;
            }
        }
#endif
    }
    else {                           // phase BufferRx does overlap with phase BufferTx
        Error = ttc_driver_spi_send_raw( Config, BufferTx, AmountTx );
    }
#endif

    return Error;
}

#endif
#ifndef ttc_driver_spi_read_raw  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_spi_read_raw() (using default implementation)!

t_u16 ttc_spi_interface_read_raw( t_ttc_spi_config* Config, t_u8* BufferRx, t_u16 AmountRx ) {
    Assert_SPI_Writable( ttc_Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_SPI_Writable( BufferRx ) ; // check if it points to RAM (change for read only memory!)

#warning Function ttc_spi_interface_read_raw() is empty.

    return 0;
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_spi_interface_foo(t_ttc_spi_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
