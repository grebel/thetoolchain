/** { ttc_gfx_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for GFX device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 20 at 20140313 10:58:25 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_GFX_INTERFACE_H
#define TTC_GFX_INTERFACE_H

//{ Includes *************************************************************

#include "../ttc_basic.h"

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_gfx_ili93xx
    #include "../gfx/gfx_ili93xx.h"
#endif
#ifdef EXTENSION_gfx_image
    #include "../gfx/gfx_image.h"  // low-level driver for gfx devices on image architecture
#endif
#ifdef EXTENSION_gfx_update
    #include "../gfx/gfx_update.h"  // low-level driver for gfx devices on update architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_gfx_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_gfx_*() declaration in ../ttc_gfx.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_gfx_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_gfx.h for prototypes of low-level driver functions!
 *
 */

void ttc_gfx_interface_init( t_ttc_gfx_config* Config ); //{  macro _driver_gfx_interface_init(Config)
#ifdef ttc_driver_gfx_init
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_init(Config) ttc_driver_gfx_init(Config)
#else
    #define _driver_gfx_init(Config) ttc_gfx_interface_init(Config)
#endif
//}
void ttc_gfx_interface_deinit( t_ttc_gfx_config* Config ); //{  macro _driver_gfx_interface_deinit(Config)
#ifdef ttc_driver_gfx_deinit
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_deinit(Config) ttc_driver_gfx_deinit(Config)
#else
    #define _driver_gfx_deinit(Config) ttc_gfx_interface_deinit(Config)
#endif
//}
e_ttc_gfx_errorcode ttc_gfx_interface_get_features( t_ttc_gfx_config* Config ); //{  macro _driver_gfx_interface_get_features(Config)
#ifdef ttc_driver_gfx_get_features
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_get_features(Config) ttc_driver_gfx_get_features(Config)
#else
    #define _driver_gfx_get_features(Config) ttc_gfx_interface_get_features(Config)
#endif
//}
void ttc_gfx_interface_reset( t_ttc_gfx_config* Config ); //{  macro _driver_gfx_interface_reset(Config)
#ifdef ttc_driver_gfx_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_reset(Config) ttc_driver_gfx_reset(Config)
#else
    #define _driver_gfx_reset(Config) ttc_gfx_interface_reset(Config)
#endif
//}
void ttc_gfx_interface_load_defaults( t_ttc_gfx_config* Config ); //{  macro _driver_gfx_interface_load_defaults(Config)
#ifdef ttc_driver_gfx_load_defaults
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_load_defaults(Config) ttc_driver_gfx_load_defaults(Config)
#else
    #define _driver_gfx_load_defaults(Config) ttc_gfx_interface_load_defaults(Config)
#endif
//}
void ttc_gfx_interface_prepare(); //{  macro _driver_gfx_interface_prepare()
#ifdef ttc_driver_gfx_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_prepare() ttc_driver_gfx_prepare()
#else
    #define _driver_gfx_prepare() ttc_gfx_interface_prepare()
#endif
//}
void ttc_gfx_interface_color_bg24( t_u32 Color ); //{  macro _driver_gfx_interface_color_bg24(Color)
#ifdef ttc_driver_gfx_color_bg24
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_color_bg24(Color) ttc_driver_gfx_color_bg24(Color)
#else
    #define _driver_gfx_color_bg24(Color) ttc_gfx_interface_color_bg24(Color)
#endif
//}
void ttc_gfx_interface_backlight( t_u8 Level ); //{  macro _driver_gfx_interface_backlight(Level)
#ifdef ttc_driver_gfx_backlight
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_backlight(Level) ttc_driver_gfx_backlight(Level)
#else
    #define _driver_gfx_backlight(Level) ttc_gfx_interface_backlight(Level)
#endif
//}
void ttc_gfx_interface_color_fg_palette( t_u8 PaletteIndex ); //{  macro _driver_gfx_interface_color_fg_palette(PaletteIndex)
#ifdef ttc_driver_gfx_color_fg_palette
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_color_fg_palette(PaletteIndex) ttc_driver_gfx_color_fg_palette(PaletteIndex)
#else
    #define _driver_gfx_color_fg_palette(PaletteIndex) ttc_gfx_interface_color_fg_palette(PaletteIndex)
#endif
//}
void ttc_gfx_interface_color_fg16( t_u16 Color ); //{  macro _driver_gfx_interface_color_fg16(Color)
#ifdef ttc_driver_gfx_color_fg16
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_color_fg16(Color) ttc_driver_gfx_color_fg16(Color)
#else
    #define _driver_gfx_color_fg16(Color) ttc_gfx_interface_color_fg16(Color)
#endif
//}
void ttc_gfx_interface_cursor_set( t_u16 X, t_u16 Y ); //{  macro _driver_gfx_interface_cursor_set(X, Y)
#ifdef ttc_driver_gfx_cursor_set
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_cursor_set(X, Y) ttc_driver_gfx_cursor_set(X, Y)
#else
    #define _driver_gfx_cursor_set(X, Y) ttc_gfx_interface_cursor_set(X, Y)
#endif
//}
void ttc_gfx_interface_color_bg16( t_u16 Color ); //{  macro _driver_gfx_interface_color_bg16(Color)
#ifdef ttc_driver_gfx_color_bg16
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_color_bg16(Color) ttc_driver_gfx_color_bg16(Color)
#else
    #define _driver_gfx_color_bg16(Color) ttc_gfx_interface_color_bg16(Color)
#endif
//}
void ttc_gfx_interface_color_bg_palette( t_u8 PaletteIndex ); //{  macro _driver_gfx_interface_color_bg_palette(PaletteIndex)
#ifdef ttc_driver_gfx_color_bg_palette
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_color_bg_palette(PaletteIndex) ttc_driver_gfx_color_bg_palette(PaletteIndex)
#else
    #define _driver_gfx_color_bg_palette(PaletteIndex) ttc_gfx_interface_color_bg_palette(PaletteIndex)
#endif
//}
void ttc_gfx_interface_palette_set( t_u16* Palette, t_u8 Size ); //{  macro _driver_gfx_interface_palette_set(Palette, Size)
#ifdef ttc_driver_gfx_palette_set
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_palette_set(Palette, Size) ttc_driver_gfx_palette_set(Palette, Size)
#else
    #define _driver_gfx_palette_set(Palette, Size) ttc_gfx_interface_palette_set(Palette, Size)
#endif
//}
void ttc_gfx_interface_color_fg24( t_u32 Color ); //{  macro _driver_gfx_interface_color_fg24(Color)
#ifdef ttc_driver_gfx_color_fg24
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_color_fg24(Color) ttc_driver_gfx_color_fg24(Color)
#else
    #define _driver_gfx_color_fg24(Color) ttc_gfx_interface_color_fg24(Color)
#endif
//}
void ttc_gfx_interface_pixel_set_at( t_u16 X, t_u16 Y ); //{  macro _driver_gfx_interface_pixel_set_at(X, Y)
#ifdef ttc_driver_gfx_pixel_set
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_pixel_set_at(X, Y) ttc_driver_gfx_pixel_set_at(X, Y)
#else
    #define _driver_gfx_pixel_set_at(X, Y) ttc_gfx_interface_pixel_set_at(X, Y)
#endif
//}
void ttc_gfx_interface_clear( t_u16 Width, t_u16 Height ); //{  macro _driver_gfx_interface_clear()
#ifdef ttc_driver_gfx_clear
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_clear() ttc_driver_gfx_clear()
#else
    #define _driver_gfx_clear( ) ttc_gfx_interface_clear()
#endif
//}
void ttc_gfx_interface_pixel_set(); //{  macro _driver_gfx_interface_pixel_set()
#ifdef ttc_driver_gfx_pixel_set
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_pixel_set() ttc_driver_gfx_pixel_set()
#else
    #define _driver_gfx_pixel_set() ttc_gfx_interface_pixel_set()
#endif

//}
void ttc_gfx_interface_line( t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2 ); //{  macro _driver_gfx_interface_line(X1, Y1, X2, Y2)
#ifdef ttc_driver_gfx_line
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_line(X1, Y1, X2, Y2) ttc_driver_gfx_line(X1, Y1, X2, Y2)
#else
    #define _driver_gfx_line(X1, Y1, X2, Y2) ttc_gfx_interface_line(X1, Y1, X2, Y2)
#endif
//}
void ttc_gfx_interface_rect( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height ); //{  macro _driver_gfx_interface_rect(X, Y, Width, Height)
#ifdef ttc_driver_gfx_rect
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_rect(X, Y, Width, Height) ttc_driver_gfx_rect(X, Y, Width, Height)
#else
    #define _driver_gfx_rect(X, Y, Width, Height) ttc_gfx_interface_rect(X, Y, Width, Height)
#endif
//}
void ttc_gfx_interface_line_vertical( t_u16 X, t_u16 Y, t_s16 Length ); //{  macro _driver_gfx_interface_line_vertical(X, Y, Length)
#ifdef ttc_driver_gfx_line_vertical
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_line_vertical(X, Y, Length) ttc_driver_gfx_line_vertical(X, Y, Length)
#else
    #define _driver_gfx_line_vertical(X, Y, Length) ttc_gfx_interface_line_vertical(X, Y, Length)
#endif
//}
void ttc_gfx_interface_circle( t_u16 CenterX, t_u16 CenterY, t_u16 Radius ); //{  macro _driver_gfx_interface_circle(CenterX, CenterY, Radius)
#ifdef ttc_driver_gfx_circle
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_circle(CenterX, CenterY, Radius) ttc_driver_gfx_circle(CenterX, CenterY, Radius)
#else
    #define _driver_gfx_circle(CenterX, CenterY, Radius) ttc_gfx_interface_circle(CenterX, CenterY, Radius)
#endif
//}
void ttc_gfx_interface_pixel_clr(); //{  macro _driver_gfx_interface_pixel_clr()
#ifdef ttc_driver_gfx_pixel_clr_at
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_pixel_clr() ttc_driver_gfx_pixel_clr()
#else
    #define _driver_gfx_pixel_clr() ttc_gfx_interface_pixel_clr()
#endif
//}
void ttc_gfx_interface_line_horizontal( t_u16 X, t_u16 Y, t_s16 Length ); //{  macro _driver_gfx_interface_line_horizontal(X, Y, Length)
#ifdef ttc_driver_gfx_line_horizontal
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_line_horizontal(X, Y, Length) ttc_driver_gfx_line_horizontal(X, Y, Length)
#else
    #define _driver_gfx_line_horizontal(X, Y, Length) ttc_gfx_interface_line_horizontal(X, Y, Length)
#endif
//}
void ttc_gfx_interface_rect_fill( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height ); //{  macro _driver_gfx_interface_rect_fill(X, Y, Width, Height)
#ifdef ttc_driver_gfx_rect_fill
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_rect_fill(X, Y, Width, Height) ttc_driver_gfx_rect_fill(X, Y, Width, Height)
#else
    #define _driver_gfx_rect_fill(X, Y, Width, Height) ttc_gfx_interface_rect_fill(X, Y, Width, Height)
#endif
//}
void ttc_gfx_interface_set_font( const t_ttc_font_data* Font ); //{  macro _driver_gfx_interface_set_font(Font)
#ifdef ttc_driver_gfx_set_font
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_set_font(Font) ttc_driver_gfx_set_font(Font)
#else
    #define _driver_gfx_set_font(Font) ttc_gfx_interface_set_font(Font)
#endif
//}
void ttc_gfx_interface_char( t_u8 Char ); //{  macro _driver_gfx_interface_char(Char)
#ifdef ttc_driver_gfx_char
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_char(Char) ttc_driver_gfx_char(Char)
#else
    #define _driver_gfx_char(Char) ttc_gfx_interface_char(Char)
#endif
//}
void ttc_gfx_interface_char_090( t_u8 Char ); //{  macro _driver_gfx_interface_char_090(Char)
#ifdef ttc_driver_gfx_char_090
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_char_090(Char) ttc_driver_gfx_char_090(Char)
#else
    #define _driver_gfx_char_090(Char) ttc_gfx_interface_char_090(Char)
#endif
//}
void ttc_gfx_interface_char_270( t_u8 Char ); //{  macro _driver_gfx_interface_char_270(Char)
#ifdef ttc_driver_gfx_char_270
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_char_270(Char) ttc_driver_gfx_char_270(Char)
#else
    #define _driver_gfx_char_270(Char) ttc_gfx_interface_char_270(Char)
#endif
//}
void ttc_gfx_interface_char_180( t_u8 Char ); //{  macro _driver_gfx_interface_char_180(Char)
#ifdef ttc_driver_gfx_char_180
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_char_180(Char) ttc_driver_gfx_char_180(Char)
#else
    #define _driver_gfx_char_180(Char) ttc_gfx_interface_char_180(Char)
#endif

void ttc_gfx_interface_char_solid( t_u8 Char ); //{  macro _driver_gfx_interface_char_solid(Char)
#ifdef ttc_driver_gfx_char_solid
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_char_solid(Char) ttc_driver_gfx_char_solid(Char)
#else
    #define _driver_gfx_char_solid(Char) ttc_gfx_interface_char_solid(Char)
#endif
//}
void ttc_gfx_interface_circle_fill( t_u16 CenterX, t_u16 CenterY, t_u16 Radius ); //{  macro _driver_gfx_interface_circle_fill(CenterX, CenterY, Radius)
#ifdef ttc_driver_gfx_circle_fill
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_circle_fill(CenterX, CenterY, Radius) ttc_driver_gfx_circle_fill(CenterX, CenterY, Radius)
#else
    #define _driver_gfx_circle_fill(CenterX, CenterY, Radius) ttc_gfx_interface_circle_fill(CenterX, CenterY, Radius)
#endif
//}

//}
void ttc_gfx_interface_char_solid_090( t_u8 Char ); //{  macro _driver_gfx_interface_char_solid_090(Char)
#ifdef ttc_driver_gfx_char_solid_090
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_char_solid_090(Char) ttc_driver_gfx_char_solid_090(Char)
#else
    #define _driver_gfx_char_solid_090(Char) ttc_gfx_interface_char_solid_090(Char)
#endif
//}
void ttc_gfx_interface_char_solid_270( t_u8 Char ); //{  macro _driver_gfx_interface_char_solid_270(Char)
#ifdef ttc_driver_gfx_char_solid_270
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_char_solid_270(Char) ttc_driver_gfx_char_solid_270(Char)
#else
    #define _driver_gfx_char_solid_270(Char) ttc_gfx_interface_char_solid_270(Char)
#endif
//}
void ttc_gfx_interface_char_solid_180( t_u8 Char ); //{  macro _driver_gfx_interface_char_solid_180(Char)
#ifdef ttc_driver_gfx_char_solid_180
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_char_solid_180(Char) ttc_driver_gfx_char_solid_180(Char)
#else
    #define _driver_gfx_char_solid_180(Char) ttc_gfx_interface_char_solid_180(Char)
#endif
//}
void ttc_gfx_interface_pixel_set_at( t_u16 X, t_u16 Y ); //{  macro _driver_gfx_interface_pixel_set_at(X, Y)
#ifdef ttc_driver_gfx_pixel_set_at
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_pixel_set_at(X, Y) ttc_driver_gfx_pixel_set_at(X, Y)
#else
    #define _driver_gfx_pixel_set_at(X, Y) ttc_gfx_interface_pixel_set_at(X, Y)
#endif
//}
void ttc_gfx_interface_pixel_clr_at( t_u16 X, t_u16 Y ); //{  macro _driver_gfx_interface_pixel_clr_at(X, Y)
#ifdef ttc_driver_gfx_pixel_clr_at
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_pixel_clr_at(X, Y) ttc_driver_gfx_pixel_clr_at(X, Y)
#else
    #define _driver_gfx_pixel_clr_at(X, Y) ttc_gfx_interface_pixel_clr_at(X, Y)
#endif
//}
void ttc_gfx_interface_rect_090( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height ); //{  macro _driver_gfx_interface_rect_090(X, Y, Width, Height)
#ifdef ttc_driver_gfx_rect_090
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_rect_090(X, Y, Width, Height) ttc_driver_gfx_rect_090(X, Y, Width, Height)
#else
    #define _driver_gfx_rect_090(X, Y, Width, Height) ttc_gfx_interface_rect_090(X, Y, Width, Height)
#endif
//}
void ttc_gfx_interface_rect_180( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height ); //{  macro _driver_gfx_interface_rect_180(X, Y, Width, Height)
#ifdef ttc_driver_gfx_rect_180
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_rect_180(X, Y, Width, Height) ttc_driver_gfx_rect_180(X, Y, Width, Height)
#else
    #define _driver_gfx_rect_180(X, Y, Width, Height) ttc_gfx_interface_rect_180(X, Y, Width, Height)
#endif
//}
void ttc_gfx_interface_rect_270( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height ); //{  macro _driver_gfx_interface_rect_270(X, Y, Width, Height)
#ifdef ttc_driver_gfx_rect_270
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_rect_270(X, Y, Width, Height) ttc_driver_gfx_rect_270(X, Y, Width, Height)
#else
    #define _driver_gfx_rect_270(X, Y, Width, Height) ttc_gfx_interface_rect_270(X, Y, Width, Height)
#endif
//}
void ttc_gfx_interface_line_horizontal_090( t_u16 X, t_u16 Y, t_s16 Length ); //{  macro _driver_gfx_interface_line_horizontal_090(X, Y, Length)
#ifdef ttc_driver_gfx_line_horizontal_090
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_line_horizontal_090(X, Y, Length) ttc_driver_gfx_line_horizontal_090(X, Y, Length)
#else
    #define _driver_gfx_line_horizontal_090(X, Y, Length) ttc_gfx_interface_line_horizontal_090(X, Y, Length)
#endif
//}
void ttc_gfx_interface_line_vertical_090( t_u16 X, t_u16 Y, t_s16 Length ); //{  macro _driver_gfx_interface_line_vertical_090(X, Y, Length)
#ifdef ttc_driver_gfx_line_vertical_090
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_line_vertical_090(X, Y, Length) ttc_driver_gfx_line_vertical_090(X, Y, Length)
#else
    #define _driver_gfx_line_vertical_090(X, Y, Length) ttc_gfx_interface_line_vertical_090(X, Y, Length)
#endif
//}
void ttc_gfx_interface_arrow( t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2, t_u8 Length1, t_u8 Length2 ); //{  macro _driver_gfx_interface_arrow(X1, Y1, X2, Y2, Length1, Length2)
#ifdef ttc_driver_gfx_arrow
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_arrow(X1, Y1, X2, Y2, Length1, Length2) ttc_driver_gfx_arrow(X1, Y1, X2, Y2, Length1, Length2)
#else
    #define _driver_gfx_arrow(X1, Y1, X2, Y2, Length1, Length2) ttc_gfx_interface_arrow(X1, Y1, X2, Y2, Length1, Length2)
#endif
//}
void ttc_gfx_interface_circle_segment( t_u16 CenterX, t_u16 CenterY, t_u16 Radius, t_u16 StartAngle, t_u16 EndAngle ); //{  macro _driver_gfx_interface_circle_segment(CenterX, CenterY, Radius, StartAngle, EndAngle)
#ifdef ttc_driver_gfx_circle_segment
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_circle_segment(CenterX, CenterY, Radius, StartAngle, EndAngle) ttc_driver_gfx_circle_segment(CenterX, CenterY, Radius, StartAngle, EndAngle)
#else
    #define _driver_gfx_circle_segment(CenterX, CenterY, Radius, StartAngle, EndAngle) ttc_gfx_interface_circle_segment(CenterX, CenterY, Radius, StartAngle, EndAngle)
#endif
//}
void ttc_gfx_interface_circle_segment_fill( t_u16 CenterX, t_u16 CenterY, t_u16 Radius, t_u16 StartAngle, t_u16 EndAngle ); //{  macro _driver_gfx_interface_circle_segment_fill(CenterX, CenterY, Radius, StartAngle, EndAngle)
#ifdef ttc_driver_gfx_circle_segment_fill
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_circle_segment_fill ttc_driver_gfx_circle_segment_fill
#else
    #define _driver_gfx_circle_segment_fill ttc_gfx_interface_circle_segment_fill
#endif
//}
void ttc_gfx_interface_select( t_ttc_gfx_config* Config ); //{  macro _driver_gfx_interface_select(Config)
#ifdef ttc_driver_gfx_select
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_select ttc_driver_gfx_select
#else
    #define _driver_gfx_select ttc_gfx_interface_select
#endif
//}
void ttc_gfx_interface_deselect( t_ttc_gfx_config* Config ); //{  macro _driver_gfx_interface_deselect(Config)
#ifdef ttc_driver_gfx_deselect
    // enable following line to forward interface function as a macro definition
    #define _driver_gfx_deselect ttc_driver_gfx_deselect
#else
    #define _driver_gfx_deselect ttc_gfx_interface_deselect
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_gfx_interface_foo(t_ttc_gfx_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_GFX_INTERFACE_H
