/** { ttc_memory_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for MEMORY device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_memory_*() functions.
 *  Instead they can call _driver_memory_*() pendants.
 *  E.g.: ttc_memory_reset() -> _driver_memory_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20140228 13:50:05 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_memory_interface.h"

/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_memory_is_executable  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_memory_is_executable() (using default implementation)!

t_s8 ttc_memory_interface_is_executable( volatile const void* Address ) {
    Assert_MEMORY( Address != NULL, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( t_s8 ) - 1;
}

#endif
#ifndef ttc_driver_memory_is_readable  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_memory_is_readable() (using default implementation)!

t_s8 ttc_memory_interface_is_readable( volatile const void* Address ) {
    Assert_MEMORY( Address != NULL, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( t_s8 ) - 1;
}

#endif
#ifndef ttc_driver_memory_is_writable  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_memory_is_writable() (using default implementation)!

t_s8 ttc_memory_interface_is_writable( volatile const void* Address ) {
    Assert_MEMORY( Address != NULL, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( t_s8 ) - 1;
}

#endif
#ifndef ttc_driver_memory_is_constant  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_memory_is_constant() (using default implementation)!

t_s8 ttc_memory_interface_is_constant( volatile const void* Address ) {
    Assert_MEMORY( Address != NULL, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( t_s8 ) - 1;
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_memory_interface_foo(t_ttc_memory_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
