/** { ttc_timer_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for TIMER device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_timer_*() functions.
 *  Instead they can call _driver_timer_*() pendants.
 *  E.g.: ttc_timer_reset() -> _driver_timer_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20140204 13:03:59 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_timer_interface.h"

/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_timer_reset  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void _driver_timer_reset( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL



}

#endif
#ifndef ttc_driver_timer_read_value  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

t_u32 _driver_timer_read_value( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( t_u32 ) 0;
}

#endif
#ifndef ttc_driver_timer_set  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

e_ttc_timer_errorcode _driver_timer_set( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TIMER( TaskFunction, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TIMER( Period > 0, ttc_assert_origin_auto ); // pointers must not be NULL



}

#endif
#ifndef ttc_driver_timer_prepare  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void _driver_timer_prepare() {




}

#endif
#ifndef ttc_driver_timer_deinit  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

e_ttc_timer_errorcode _driver_timer_deinit( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_timer_errorcode ) 0;
}

#endif
#ifndef ttc_driver_timer_init  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

e_ttc_timer_errorcode _driver_timer_init( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_timer_errorcode ) 0;
}

#endif
#ifndef ttc_driver_timer_get_features  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

e_ttc_timer_errorcode _driver_timer_get_features( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_timer_errorcode ) 0;
}

#endif
#ifndef ttc_driver_timer_change_period  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void _driver_timer_change_period( t_ttc_timer_config* Config, t_u32 Period ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL



}

#endif
#ifndef ttc_driver_timer_reload  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void _driver_timer_reload( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL

}

#endif
#ifndef ttc_driver_timer_load_defaults  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

e_ttc_timer_errorcode _driver_timer_load_defaults( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_timer_errorcode ) 0;
}

#endif

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void __driver_timerfoo(t_ttc_timer_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
