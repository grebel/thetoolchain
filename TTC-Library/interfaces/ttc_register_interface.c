/** { ttc_register_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for REGISTER device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_register_*() functions.
 *  Instead they can call _driver_register_*() pendants.
 *  E.g.: ttc_register_reset() -> _driver_register_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20140223 10:54:39 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_register_interface.h"

/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_register_get_configuration  // no low-level implementation: use default implementation

t_ttc_register_architecture* ttc_register_interface_get_configuration() {

    return NULL;
}

#endif
#ifndef ttc_driver_register_prepare  // no low-level implementation: use default implementation

void ttc_register_interface_prepare() {

}

#endif
#ifndef ttc_driver_register_reset  // no low-level implementation: use default implementation

e_ttc_register_errorcode ttc_register_interface_reset( void* Register ) {
    Assert_REGISTER_Writable( Register, ttc_assert_origin_auto );  // pointers must not be NULL

    return ( e_ttc_register_errorcode ) 0;
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_register_interface_foo(t_ttc_register_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions