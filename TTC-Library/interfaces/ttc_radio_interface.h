/** { ttc_radio_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for RADIO device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *  Functions in this file may not call ttc_radio_* () functions.
 *  Instead they can call _driver_radio_* () pendants.
 *  E.g.: ttc_radio_reset() -> _driver_radio_reset()
 *
 *  Created from template ttc_device_interface.h revision 20 at 20140514 03: 13: 59 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_RADIO_INTERFACE_H
#define TTC_RADIO_INTERFACE_H

//{ Includes *************************************************************

#include "../ttc_basic.h"

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_radio_stm32w1xx
    #include "../radio/radio_stm32w1xx.h"
#endif
#ifdef EXTENSION_radio_cc1101
    #include "../radio/radio_cc1101.h"  // low-level driver for radio devices on cc1101 architecture
#endif
#ifdef EXTENSION_radio_stm32l1xx
    #include "../radio/radio_stm32l1xx.h"  // low-level driver for radio devices on stm32l1xx architecture
#endif
#ifdef EXTENSION_radio_dw1000
    #include "../radio/radio_dw1000.h"  // low-level driver for radio devices on dw1000 architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_radio_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_radio_*() declaration in ../ttc_radio.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_radio_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_radio.h for prototypes of low-level driver functions!
 *
 */

e_ttc_radio_errorcode ttc_radio_interface_load_defaults( t_ttc_radio_config* Config ); //{  macro _driver_radio_interface_load_defaults(Config)
#ifdef ttc_driver_radio_load_defaults
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_load_defaults(Config) ttc_driver_radio_load_defaults(Config)
#else
    #define _driver_radio_load_defaults(Config) ttc_radio_interface_load_defaults(Config)
#endif
//}
e_ttc_radio_errorcode ttc_radio_interface_reset( t_ttc_radio_config* Config ); //{  macro _driver_radio_interface_reset(Config)
#ifdef ttc_driver_radio_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_reset(Config) ttc_driver_radio_reset(Config)
#else
    #define _driver_radio_reset(Config) ttc_radio_interface_reset(Config)
#endif
//}
void ttc_radio_interface_prepare(); //{  macro _driver_radio_interface_prepare()
#ifdef ttc_driver_radio_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_prepare() ttc_driver_radio_prepare()
#else
    #define _driver_radio_prepare() ttc_radio_interface_prepare()
#endif
//}
e_ttc_radio_errorcode ttc_radio_interface_configure_channel_rx( t_ttc_radio_config* Config, t_u8 NewChannel ); //{  macro _driver_radio_interface_configure_channel_rx(Config, NewChannel)
#ifdef ttc_driver_radio_configure_channel_rx
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_configure_channel_rx(Config, NewChannel) ttc_driver_radio_configure_channel_rx(Config, NewChannel)
#else
    #define _driver_radio_configure_channel_rx(Config, NewChannel) ttc_radio_interface_configure_channel_rx(Config, NewChannel)
#endif
//}
e_ttc_radio_errorcode ttc_radio_interface_configure_channel_tx( t_ttc_radio_config* Config, t_u8 NewChannel ); //{  macro _driver_radio_interface_configure_channel_tx(Config, NewChannel)
#ifdef ttc_driver_radio_configure_channel_tx
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_configure_channel_tx(Config, NewChannel) ttc_driver_radio_configure_channel_tx(Config, NewChannel)
#else
    #define _driver_radio_configure_channel_tx(Config, NewChannel) ttc_radio_interface_configure_channel_tx(Config, NewChannel)
#endif
//}
u_ttc_radio_frame_filter ttc_radio_interface_configure_frame_filter( t_ttc_radio_config* Config, u_ttc_radio_frame_filter Settings, BOOL ChangeFilter ); //{  macro _driver_radio_interface_configure_frame_filter(Config, Settings)
#ifdef ttc_driver_radio_configure_frame_filter
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_configure_frame_filter(Config, Settings, ChangeFilter) ttc_driver_radio_configure_frame_filter(Config, Settings, ChangeFilter)
#else
    #define _driver_radio_configure_frame_filter(Config, Settings, ChangeFilter) ttc_radio_interface_configure_frame_filter(Config, Settings, ChangeFilter)
#endif
//}
e_ttc_radio_errorcode ttc_radio_interface_configure_channel_rxtx( t_ttc_radio_config* Config, t_u8 NewChannel ); //{  macro _driver_radio_interface_configure_channel_rxtx(Config, NewChannel)
#ifdef ttc_driver_radio_configure_channel_rxtx
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_configure_channel_rxtx(Config, NewChannel) ttc_driver_radio_configure_channel_rxtx(Config, NewChannel)
#else
    #define _driver_radio_configure_channel_rxtx(Config, NewChannel) ttc_radio_interface_configure_channel_rxtx(Config, NewChannel)
#endif
//}
BOOL ttc_radio_interface_configure_auto_acknowledge( t_ttc_radio_config* Config, BOOL Enable, BOOL Change ); //{  macro _driver_radio_interface_configure_auto_acknowledge(Config, UserID, Enable, Change)
#ifdef ttc_driver_radio_configure_auto_acknowledge
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_configure_auto_acknowledge ttc_driver_radio_configure_auto_acknowledge
#else
    #define _driver_radio_configure_auto_acknowledge ttc_radio_interface_configure_auto_acknowledge
#endif
//}
void ttc_radio_interface_configuration_check( t_ttc_radio_config* Config ); //{  macro _driver_radio_configuration_check(Config)
#ifdef ttc_driver_radio_configuration_check
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_configuration_check(Config) ttc_driver_radio_configuration_check(Config)
#else
    #define _driver_radio_configuration_check(Config) ttc_radio_interface_get_features(Config)
#endif
//}
e_ttc_radio_errorcode ttc_radio_interface_init( t_ttc_radio_config* Config ); //{  macro _driver_radio_interface_init(Config)
#ifdef ttc_driver_radio_init
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_init(Config) ttc_driver_radio_init(Config)
#else
    #define _driver_radio_init(Config) ttc_radio_interface_init(Config)
#endif
//}
e_ttc_radio_errorcode ttc_radio_interface_deinit( t_ttc_radio_config* Config ); //{  macro _driver_radio_interface_deinit(Config)
#ifdef ttc_driver_radio_deinit
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_deinit(Config) ttc_driver_radio_deinit(Config)
#else
    #define _driver_radio_deinit(Config) ttc_radio_interface_deinit(Config)
#endif
//}
void ttc_radio_interface_transmit( t_ttc_radio_config* Config, u_ttc_packetimestamp_40* TransmitTime ); //{  macro _driver_radio_interface_transmit(Config, DelayNS)
#ifdef ttc_driver_radio_transmit
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_transmit(Config, TransmitTime) ttc_driver_radio_transmit(Config, TransmitTime)
#else
    #define _driver_radio_transmit(Config, TransmitTime) ttc_radio_interface_transmit(Config, TransmitTime)
#endif
//}
void ttc_radio_interface_maintenance( t_ttc_radio_config* Config ); //{  macro _driver_radio_interface_maintenance(Config)
#ifdef ttc_driver_radio_maintenance
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_maintenance(Config) ttc_driver_radio_maintenance(Config)
#else
    #define _driver_radio_maintenance(Config) ttc_radio_interface_maintenance(Config)
#endif
//}
void ttc_radio_interface_isr_receive( t_physical_index Index, t_ttc_radio_config* Config ); //{  macro _driver_radio_interface_isr_receive(Index, Config)
#ifdef ttc_driver_radio_isr_receive
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_isr_receive(Index, Config) ttc_driver_radio_isr_receive(Index, Config)
#else
    #define _driver_radio_isr_receive(Index, Config) ttc_radio_interface_isr_receive(Index, Config)
#endif
//}
void ttc_radio_interface_change_local_address( t_ttc_radio_config* Config, const t_ttc_packet_address* LocalAddress ); //{  macro _driver_radio_interface_change_local_address(Config, LocalAddress)
#ifdef ttc_driver_radio_change_local_address
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_change_local_address(Config, LocalAddress) ttc_driver_radio_change_local_address(Config, LocalAddress)
#else
    #define _driver_radio_change_local_address(Config, LocalAddress) ttc_radio_interface_change_local_address(Config, LocalAddress)
#endif
//}
void ttc_radio_interface_change_delay_time( t_ttc_radio_config* Config, t_u16 Delay_uSeconds ); //{  macro _driver_radio_interface_change_delay_time(Config, Delay_uSeconds)
#ifdef ttc_driver_radio_change_delay_time
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_change_delay_time(Config, Delay_uSeconds) ttc_driver_radio_change_delay_time(Config, Delay_uSeconds)
#else
    #define _driver_radio_change_delay_time(Config, Delay_uSeconds) ttc_radio_interface_change_delay_time(Config, Delay_uSeconds)
#endif
//}
BOOL ttc_radio_interface_update_meta( t_ttc_radio_config* Config, t_ttc_packet* Packet ); //{  macro _driver_radio_interface_update_meta(Config, Packet)
#ifdef ttc_driver_radio_update_meta
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_update_meta(Config, Packet) ttc_driver_radio_update_meta(Config, Packet)
#else
    #define _driver_radio_update_meta(Config, Packet) ttc_radio_interface_update_meta(Config, Packet)
#endif
//}
void ttc_radio_interface_isr_transmit( t_physical_index Index, t_ttc_radio_config* Config ); //{  macro _driver_radio_interface_isr_transmit(Index, Config)
#ifdef ttc_driver_radio_isr_transmit
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_isr_transmit(Index, Config) ttc_driver_radio_isr_transmit(Index, Config)
#else
    #define _driver_radio_isr_transmit(Index, Config) ttc_radio_interface_isr_transmit(Index, Config)
#endif
//}
BOOL ttc_radio_interface_receiver( t_ttc_radio_config* Config, BOOL Enable, u_ttc_packetimestamp_40* ReferenceTime, t_base TimeOut_us ); //{  macro _driver_radio_interface_receiver(Config, Enable, ReferenceTime)
#ifdef ttc_driver_radio_receiver
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_receiver(Config, Enable, ReferenceTime, TimeOut_us) ttc_driver_radio_receiver(Config, Enable, ReferenceTime, TimeOut_us)
#else
    #define _driver_radio_receiver(Config, Enable, ReferenceTime, TimeOut_us) ttc_radio_interface_receiver(Config, Enable, ReferenceTime, TimeOut_us)
#endif
//}

BOOL ttc_radio_interface_gpio_out( t_ttc_radio_config* Config, e_ttc_radio_gpio Pin, BOOL NewState ); //{  macro _driver_radio_interface_gpio_out(Config, Pin, NewState)
#ifdef ttc_driver_radio_gpio_out
    // enable following line to forward interface function as a macro definition
    #define _driver_radio_gpio_out ttc_driver_radio_gpio_out
#else
    #define _driver_radio_gpio_out ttc_radio_interface_gpio_out
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_radio_interface_foo(t_ttc_radio_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_RADIO_INTERFACE_H
