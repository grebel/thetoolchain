/** { ttc_packet_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for PACKET device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 22 at 20150928 09:30:04 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_PACKET_INTERFACE_H
#define TTC_PACKET_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_packet_interface.c"
//

#include "../ttc_basic.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_packet_802154
    #include "../packet/packet_802154.h"
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

// Check if at least one low-level driver could be activated
#ifndef EXTENSION_PACKET_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_PACKET! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _packet_
#endif

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_packet_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_packet_*() declaration in ../ttc_packet.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_packet_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_packet.h for prototypes of low-level driver functions!
 *
 */

void ttc_packet_interface_prepare(); //{  macro _driver_packet_interface_prepare()
#ifdef ttc_driver_packet_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_prepare() ttc_driver_packet_prepare()
#else
    #define _driver_packet_prepare() ttc_packet_interface_prepare()
#endif
//}
e_ttc_packet_address_format ttc_packet_interface_address_source_compare( t_ttc_packet* Packet, const t_ttc_packet_address* Address ); //{  macro _driver_packet_interface_address_source_compare(Packet, Address)
#ifdef ttc_driver_packet_address_source_compare
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_address_source_compare(Packet, Address) ttc_driver_packet_address_source_compare(Packet, Address)
#else
    #define _driver_packet_address_source_compare(Packet, Address) ttc_packet_interface_address_source_compare(Packet, Address)
#endif
//}
e_ttc_packet_address_format ttc_packet_interface_address_target_set( t_ttc_packet* Packet, const t_ttc_packet_address* Address ); //{  macro _driver_packet_interface_address_target_set(Packet, Address)
#ifdef ttc_driver_packet_address_target_set
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_address_target_set(Packet, Address) ttc_driver_packet_address_target_set(Packet, Address)
#else
    #define _driver_packet_address_target_set(Packet, Address) ttc_packet_interface_address_target_set(Packet, Address)
#endif
//}
e_ttc_packet_address_format ttc_packet_interface_address_source_get_pointer( t_ttc_packet* Packet, void** Pointer2SourceID, t_u16** Pointer2SourcePanID ); //{  macro _driver_packet_interface_address_source_get_pointer(Packet, Address)
#ifdef ttc_driver_packet_address_source_get_pointer
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_address_source_get_pointer(Packet, Pointer2SourceID, Pointer2SourcePanID) ttc_driver_packet_address_source_get_pointer(Packet, Pointer2SourceID, Pointer2SourcePanID)
#else
    #define _driver_packet_address_source_get_pointer(Packet, Pointer2SourceID, Pointer2SourcePanID) ttc_packet_interface_address_source_get_pointer(Packet, Pointer2SourceID, Pointer2SourcePanID)
#endif
//}
e_ttc_packet_address_format ttc_packet_interface_address_source_get( t_ttc_packet* Packet, t_ttc_packet_address* Address ); //{  macro _driver_packet_interface_address_source_get(Packet, Address)
#ifdef ttc_driver_packet_address_source_get
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_address_source_get ttc_driver_packet_address_source_get
#else
    #define _driver_packet_address_source_get ttc_packet_interface_address_source_get
#endif
//}
t_u16 ttc_packet_interface_payload_get_size_max( t_ttc_packet* Packet ); //{  macro _driver_packet_interface_payload_get_size_max(Packet)
#ifdef ttc_driver_packet_payload_get_size_max
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_payload_get_size_max(Packet) ttc_driver_packet_payload_get_size_max(Packet)
#else
    #define _driver_packet_payload_get_size_max(Packet) ttc_packet_interface_payload_get_size_max(Packet)
#endif
//}
e_ttc_packet_address_format ttc_packet_interface_address_target_compare( t_ttc_packet* Packet, t_ttc_packet_address* Address ); //{  macro _driver_packet_interface_address_target_compare(Packet, Address)
#ifdef ttc_driver_packet_address_target_compare
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_address_target_compare(Packet, Address) ttc_driver_packet_address_target_compare(Packet, Address)
#else
    #define _driver_packet_address_target_compare(Packet, Address) ttc_packet_interface_address_target_compare(Packet, Address)
#endif
//}
t_u8 ttc_packet_interface_payload_get_pointer( t_ttc_packet* Packet, t_u8** PayloadPtr, t_u16* SizePtr ); //{  macro _driver_packet_interface_payload_get_pointer(Packet, PayloadPtr, SizePtr)
#ifdef ttc_driver_packet_payload_get_pointer
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_payload_get_pointer(Packet, PayloadPtr, SizePtr, ProtocolPtr) ttc_driver_packet_payload_get_pointer(Packet, PayloadPtr, SizePtr, ProtocolPtr)
#else
    #define _driver_packet_payload_get_pointer(Packet, PayloadPtr, SizePtr, ProtocolPtr) ttc_packet_interface_payload_get_pointer(Packet, PayloadPtr, SizePtr, ProtocolPtr)
#endif
//}
e_ttc_packet_address_format ttc_packet_interface_address_target_get_pointer( t_ttc_packet* Packet, void** Pointer2TargetID, t_u16** Pointer2TargetPanID ); //{  macro _driver_packet_interface_address_target_get_pointer(Packet, Address)
#ifdef ttc_driver_packet_address_target_get_pointer
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_address_target_get_pointer(Packet, Pointer2TargetID, Pointer2TargetPanID) ttc_driver_packet_address_target_get_pointer(Packet, Pointer2TargetID, Pointer2TargetPanID)
#else
    #define _driver_packet_address_target_get_pointer(Packet, Pointer2TargetID, Pointer2TargetPanID) ttc_packet_interface_address_target_get_pointer(Packet, Pointer2TargetID, Pointer2TargetPanID)
#endif
//}
e_ttc_packet_address_format ttc_packet_interface_address_target_get( t_ttc_packet* Packet, t_ttc_packet_address* Address ); //{  macro _driver_packet_interface_address_target_get(Packet, Address)
#ifdef ttc_driver_packet_address_target_get
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_address_target_get ttc_driver_packet_address_target_get
#else
    #define _driver_packet_address_target_get ttc_packet_interface_address_target_get
#endif
//}
t_u16 ttc_packet_interface_payload_get_size( t_ttc_packet* Packet ); //{  macro _driver_packet_interface_payload_get_size(Packet)
#ifdef ttc_driver_packet_payload_get_size
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_payload_get_size(Packet) ttc_driver_packet_payload_get_size(Packet)
#else
    #define _driver_packet_payload_get_size(Packet) ttc_packet_interface_payload_get_size(Packet)
#endif
//}
e_ttc_packet_address_format ttc_packet_interface_address_source_set( t_ttc_packet* Packet, const t_ttc_packet_address* Address ); //{  macro _driver_packet_interface_address_source_set(Packet, Address)
#ifdef ttc_driver_packet_address_source_set
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_address_source_set(Packet, Address) ttc_driver_packet_address_source_set(Packet, Address)
#else
    #define _driver_packet_address_source_set(Packet, Address) ttc_packet_interface_address_source_set(Packet, Address)
#endif
//}
e_ttc_packet_category ttc_packet_interface_identify( t_ttc_packet* Packet ); //{  macro _driver_packet_interface_identify(PacketHeader)
#ifdef ttc_driver_packet_identify
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_identify(PacketHeader) ttc_driver_packet_identify(PacketHeader)
#else
    #define _driver_packet_identify(PacketHeader) ttc_packet_interface_identify(PacketHeader)
#endif
//}
void ttc_packet_interface_initialize( t_ttc_packet* Packet, e_ttc_packet_type Type ); //{  macro _driver_packet_interface_initialize(Packet, Type)
#ifdef ttc_driver_packet_initialize
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_initialize(Packet, Type) ttc_driver_packet_initialize(Packet, Type)
#else
    #define _driver_packet_initialize(Packet, Type) ttc_packet_interface_initialize(Packet, Type)
#endif
//}
t_u8 ttc_packet_interface_mac_header_get_size( t_ttc_packet* Packet ); //{  macro _driver_packet_interface_mac_header_get_size(Packet)
#ifdef ttc_driver_packet_mac_header_get_size
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_mac_header_get_size(Packet) ttc_driver_packet_mac_header_get_size(Packet)
#else
    #define _driver_packet_mac_header_get_size(Packet) ttc_packet_interface_mac_header_get_size(Packet)
#endif
//}
void ttc_packet_interface_payload_set_size( t_ttc_packet* Packet, t_u16 PayloadSize ); //{  macro _driver_packet_interface_payload_set_size(Packet, PayloadSize)
#ifdef ttc_driver_packet_payload_set_size
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_payload_set_size(Packet, PayloadSize) ttc_driver_packet_payload_set_size(Packet, PayloadSize)
#else
    #define _driver_packet_payload_set_size(Packet, PayloadSize) ttc_packet_interface_payload_set_size(Packet, PayloadSize)
#endif
//}
BOOL ttc_packet_interface_acknowledge_request_get( t_ttc_packet* Packet ); //{  macro _driver_packet_interface_acknowledge_request_get(Packet)
#ifdef ttc_driver_packet_acknowledge_request_get
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_acknowledge_request_get ttc_driver_packet_acknowledge_request_get
#else
    #define _driver_packet_acknowledge_request_get ttc_packet_interface_acknowledge_request_get
#endif
//}
void ttc_packet_interface_acknowledge_request_set( t_ttc_packet* Packet, BOOL RequestACK ); //{  macro _driver_packet_interface_acknowledge_request_set(Packet, RequestACK)
#ifdef ttc_driver_packet_acknowledge_request_set
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_acknowledge_request_set ttc_driver_packet_acknowledge_request_set
#else
    #define _driver_packet_acknowledge_request_set ttc_packet_interface_acknowledge_request_set
#endif
//}
e_ttc_packet_type ttc_packet_interface_type_get( t_ttc_packet* Packet ); //{  macro _driver_packet_interface_type_get(Packet)
#ifdef ttc_driver_packet_type_get
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_type_get ttc_driver_packet_type_get
#else
    #define _driver_packet_type_get ttc_packet_interface_type_get
#endif
//}
void ttc_packet_interface_address_reply2( t_ttc_packet* Packet, t_ttc_packet* PacketReply ); //{  macro _driver_packet_interface_address_reply2(Packet, PacketReply)
#ifdef ttc_driver_packet_address_reply2
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_address_reply2 ttc_driver_packet_address_reply2
#else
    #define _driver_packet_address_reply2 ttc_packet_interface_address_reply2
#endif
//}
BOOL ttc_packet_interface_is_acknowledge( t_ttc_packet* Packet ); //{  macro _driver_packet_interface_is_acknowledge(Packet)
#ifdef ttc_driver_packet_is_acknowledge
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_is_acknowledge ttc_driver_packet_is_acknowledge
#else
    #define _driver_packet_is_acknowledge ttc_packet_interface_is_acknowledge
#endif
//}
BOOL ttc_packet_interface_is_command( t_ttc_packet* Packet ); //{  macro _driver_packet_interface_is_command(Packet)
#ifdef ttc_driver_packet_is_command
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_is_command ttc_driver_packet_is_command
#else
    #define _driver_packet_is_command ttc_packet_interface_is_command
#endif
//}
BOOL ttc_packet_interface_is_data( t_ttc_packet* Packet ); //{  macro _driver_packet_interface_is_data(Packet)
#ifdef ttc_driver_packet_is_data
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_is_data ttc_driver_packet_is_data
#else
    #define _driver_packet_is_data ttc_packet_interface_is_data
#endif
//}
BOOL ttc_packet_interface_is_beacon( t_ttc_packet* Packet ); //{  macro _driver_packet_interface_is_beacon(Packet)
#ifdef ttc_driver_packet_is_beacon
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_is_beacon ttc_driver_packet_is_beacon
#else
    #define _driver_packet_is_beacon ttc_packet_interface_is_beacon
#endif
//}
void ttc_packet_interface_acknowledge( t_ttc_packet* Packet, t_ttc_packet* PacketACK ); //{  macro _driver_packet_interface_acknowledge(Packet, PacketACK)
#ifdef ttc_driver_packet_acknowledge
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_acknowledge ttc_driver_packet_acknowledge
#else
    #define _driver_packet_acknowledge ttc_packet_interface_acknowledge
#endif
//}
void ttc_packet_interface_sequence_number_set( t_ttc_packet* Packet, t_u8 SequenceNumber ); //{  macro _driver_packet_interface_sequence_number_set(Packet, SequenceNumber)
#ifdef ttc_driver_packet_sequence_number_set
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_sequence_number_set ttc_driver_packet_sequence_number_set
#else
    #define _driver_packet_sequence_number_set ttc_packet_interface_sequence_number_set
#endif
//}
t_u8 ttc_packet_interface_sequence_number_get( t_ttc_packet* Packet ); //{  macro _driver_packet_interface_sequence_number_get(Packet)
#ifdef ttc_driver_packet_sequence_number_get
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_sequence_number_get ttc_driver_packet_sequence_number_get
#else
    #define _driver_packet_sequence_number_get ttc_packet_interface_sequence_number_get
#endif
//}
BOOL ttc_packet_interface_pattern_matches_type( e_ttc_packet_type Type, e_ttc_packet_pattern Pattern ); //{  macro _driver_packet_interface_pattern_matches_type(Protocol, Pattern)
#ifdef ttc_driver_packet_pattern_matches_type
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_pattern_matches_type ttc_driver_packet_pattern_matches_type
#else
    #define _driver_packet_pattern_matches_type ttc_packet_interface_pattern_matches_type
#endif
//}
t_u8 ttc_packet_interface_mac_header_get_type_size( e_ttc_packet_type Type ); //{  macro _driver_packet_interface_mac_header_get_type_size(Type)
#ifdef ttc_driver_packet_mac_header_get_type_size
    // enable following line to forward interface function as a macro definition
    #define _driver_packet_mac_header_get_type_size ttc_driver_packet_mac_header_get_type_size
#else
    #define _driver_packet_mac_header_get_type_size ttc_packet_interface_mac_header_get_type_size
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_packet_interface_foo(t_ttc_packet_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_PACKET_INTERFACE_H
