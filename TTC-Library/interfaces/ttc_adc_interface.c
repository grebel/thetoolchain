/** { ttc_adc_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for ADC device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_adc_*() functions.
 *  Instead they can call _driver_adc_*() pendants.
 *  E.g.: ttc_adc_reset() -> _driver_adc_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20141008 14:25:12 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_adc_interface.h".
//
#include "ttc_adc_interface.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_adc_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_adc_load_defaults() (using default implementation)!

e_ttc_adc_errorcode ttc_adc_interface_load_defaults( t_ttc_adc_config* Config ) {
    Assert_ADC( Config, ttc_assert_origin_auto ); // pointers must not be NULL

#warning Function ttc_adc_interface_load_defaults() is empty.

    return ( e_ttc_adc_errorcode ) 0;
}

#endif
#ifndef ttc_driver_adc_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_adc_prepare() (using default implementation)!

void ttc_adc_interface_prepare() {


#warning Function ttc_adc_interface_prepare() is empty.


}

#endif
#ifndef ttc_driver_adc_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_adc_reset() (using default implementation)!

e_ttc_adc_errorcode ttc_adc_interface_reset( t_ttc_adc_config* Config ) {
    Assert_ADC( Config, ttc_assert_origin_auto ); // pointers must not be NULL

#warning Function ttc_adc_interface_reset() is empty.

    return ( e_ttc_adc_errorcode ) 0;
}

#endif
#ifndef ttc_driver_adc_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_adc_deinit() (using default implementation)!

e_ttc_adc_errorcode ttc_adc_interface_deinit( t_ttc_adc_config* Config ) {
    Assert_ADC( Config, ttc_assert_origin_auto ); // pointers must not be NULL

#warning Function ttc_adc_interface_deinit() is empty.

    return ( e_ttc_adc_errorcode ) 0;
}

#endif
#ifndef ttc_driver_adc_init  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_adc_init() (using default implementation)!

e_ttc_adc_errorcode ttc_adc_interface_init( t_ttc_adc_config* Config ) {
    Assert_ADC( Config, ttc_assert_origin_auto ); // pointers must not be NULL

#warning Function ttc_adc_interface_init() is empty.

    return ( e_ttc_adc_errorcode ) 0;
}

#endif
#ifndef ttc_driver_adc_get_features  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_adc_get_features() (using default implementation)!

e_ttc_adc_errorcode ttc_adc_interface_get_features( t_ttc_adc_config* Config ) {
    Assert_ADC( Config, ttc_assert_origin_auto ); // pointers must not be NULL

#warning Function ttc_adc_interface_get_features() is empty.

    return ( e_ttc_adc_errorcode ) 0;
}

#endif
#ifndef ttc_driver_adc_init_single  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_adc_init_single() (using default implementation)!

e_ttc_adc_errorcode ttc_adc_interface_init_single( t_ttc_adc_config* Config ) {
    Assert_ADC( Config, ttc_assert_origin_auto ); // pointers must not be NULL

#warning Function ttc_adc_interface_init_sinlge() is empty.

    return ( e_ttc_adc_errorcode ) 0;
}

#endif
#ifndef ttc_driver_adc_get_value  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_adc_get_value() (using default implementation)!

e_ttc_adc_errorcode ttc_adc_interface_get_value( t_u8 Channel, t_ttc_adc_config* Config ) {
    Assert_ADC( Config, ttc_assert_origin_auto ); // pointers must not be NULL

#warning Function ttc_adc_interface_get_value() is empty.

    return ( e_ttc_adc_errorcode ) 0;
}

#endif

#ifndef ttc_driver_adc_init_dma  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_adc_init_dma() (using default implementation)!

e_ttc_adc_errorcode ttc_adc_interface_init_dma( t_ttc_adc_config* Config, volatile t_u16* Value ) {
    Assert_ADC( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_ADC( Value, ttc_assert_origin_auto ); // pointers must not be NULL

#warning Function ttc_adc_interface_init_dma() is empty.

    return ( e_ttc_adc_errorcode ) 0;
}

#endif
#ifndef ttc_driver_adc_find_input_pin  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_adc_find_input_pin() (using default implementation)!

e_ttc_adc_errorcode ttc_adc_interface_find_input_pin( t_ttc_adc_config* Config ) {
    Assert_ADC( Config, ttc_assert_origin_auto ); // pointers must not be NULL

#warning Function ttc_adc_interface_find_input_pin() is empty.

    return ( e_ttc_adc_errorcode ) 0;
}

#endif
#ifndef ttc_driver_adc_get_minimum  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_adc_get_minimum() (using default implementation)!

t_base_signed ttc_adc_interface_get_minimum( t_u8 LogicalIndex ) {


#warning Function ttc_adc_interface_get_minimum() is empty.

    return ( t_base_signed ) 0;
}

#endif
#ifndef ttc_driver_adc_get_maximum  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_adc_get_maximum() (using default implementation)!

t_base_signed ttc_adc_interface_get_maximum( t_u8 LogicalIndex ) {


#warning Function ttc_adc_interface_get_maximum() is empty.

    return ( t_base_signed ) 0;
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_adc_interface_foo(t_ttc_adc_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
