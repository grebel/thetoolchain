/** { ttc_can_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for CAN device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 20 at 20140324 09:30:54 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_CAN_INTERFACE_H
#define TTC_CAN_INTERFACE_H

//{ Includes *************************************************************

#include "../ttc_basic.h"

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_can_stm32f1xx
#  include "../can/can_stm32f1xx.h"
#endif

//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)
#include "../ttc_can_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_can_*() declaration in ../ttc_can.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_can_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_can.h for prototypes of low-level driver functions!
 *
 */

t_u8 ttc_can_interface_send( t_ttc_can_config* Config ); //{  macro _driver_can_interface_send(Register, Word)
#ifdef ttc_driver_can_send
// enable following line to forward interface function as a macro definition
#  define _driver_can_send(Config) ttc_driver_can_send(Config)
#else
#  define _driver_can_send(Config) ttc_can_interface_send(Config)
#endif
//}
e_ttc_can_errorcode ttc_can_interface_deinit( t_ttc_can_config* Config ); //{  macro _driver_can_interface_deinit(Config)
#ifdef ttc_driver_can_deinit
// enable following line to forward interface function as a macro definition
#  define _driver_can_deinit(Config) ttc_driver_can_deinit(Config)
#else
#  define _driver_can_deinit(Config) ttc_can_interface_deinit(Config)
#endif
//}
void ttc_can_interface_prepare(); //{  macro _driver_can_interface_prepare()
#ifdef ttc_driver_can_prepare
// enable following line to forward interface function as a macro definition
#  define _driver_can_prepare() ttc_driver_can_prepare()
#else
#  define _driver_can_prepare() ttc_can_interface_prepare()
#endif
//}
e_ttc_can_errorcode ttc_can_interface_get_features( t_ttc_can_config* Config ); //{  macro _driver_can_interface_get_features(Config)
#ifdef ttc_driver_can_get_features
// enable following line to forward interface function as a macro definition
#  define _driver_can_get_features(Config) ttc_driver_can_get_features(Config)
#else
#  define _driver_can_get_features(Config) ttc_can_interface_get_features(Config)
#endif
//}
e_ttc_can_errorcode ttc_can_interface_load_defaults( t_ttc_can_config* Config ); //{  macro _driver_can_interface_load_defaults(Config)
#ifdef ttc_driver_can_load_defaults
// enable following line to forward interface function as a macro definition
#  define _driver_can_load_defaults(Config) ttc_driver_can_load_defaults(Config)
#else
#  define _driver_can_load_defaults(Config) ttc_can_interface_load_defaults(Config)
#endif
//}
e_ttc_can_errorcode ttc_can_interface_reset( t_ttc_can_architecture* Architecture ); //{  macro _driver_can_interface_reset(Config)
#ifdef ttc_driver_can_reset
// enable following line to forward interface function as a macro definition
#  define _driver_can_reset(Architecture) ttc_driver_can_reset(Architecture)
#else
#  define _driver_can_reset(Architecture) ttc_can_interface_reset(Architecture)
#endif
//}
t_ttc_can_config* ttc_can_interface_get_configuration( t_ttc_can_config* Config ); //{  macro _driver_can_interface_get_configuration(Config)
#ifdef ttc_driver_can_get_configuration
// enable following line to forward interface function as a macro definition
#  define _driver_can_get_configuration(Config) ttc_driver_can_get_configuration(Config)
#else
#  define _driver_can_get_configuration(Config) ttc_can_interface_get_configuration(Config)
#endif
//}
e_ttc_can_errorcode ttc_can_interface_init( t_ttc_can_config* Config ); //{  macro _driver_can_interface_init(Config)
#ifdef ttc_driver_can_init
// enable following line to forward interface function as a macro definition
#  define _driver_can_init(Config) ttc_driver_can_init(Config)
#else
#  define _driver_can_init(Config) ttc_can_interface_init(Config)
#endif
//}
e_ttc_can_errorcode ttc_can_interface_read( t_ttc_can_config* Config, t_u8* Byte ); //{  macro _driver_can_interface_read(Register, Word)
#ifdef ttc_driver_can_read
// enable following line to forward interface function as a macro definition
#  define _driver_can_read(Config, Byte) ttc_driver_can_read(Config, Byte)
#else
#  define _driver_can_read(Config, Byte) ttc_can_interface_read(Config, Byte)
#endif
//}
void ttc_can_interface_set_filter( t_u8 FilterNum, t_ttc_can_config* Config ); //{  macro _driver_can_interface_read(Register, Word)
#ifdef ttc_driver_can_set_filter
// enable following line to forward interface function as a macro definition
#  define _driver_can_set_filter(FilterNum, Config) ttc_driver_can_set_filter(FilterNum, Config)
#else
#  define _driver_can_set_filter(FilterNum, Config) ttc_can_interface_set_filter(FilterNum, Config)
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_can_interface_foo(t_ttc_can_config* Config)
t_u8 _ttc_can_interface_tx_status( t_ttc_can_config* Config, t_u8 MailBox ); //{  macro _driver_can_interface_read(Register, Word)
#ifdef _ttc_driver_can_tx_status
// enable following line to forward interface function as a macro definition
#  define _driver_can_tx_status(Config, MailBox) _ttc_driver_can_tx_status(Config, MailBox)
#else
#  define _driver_can_tx_status(Config, MailBox) _ttc_driver_can_tx_status(Config, MailBox)
#endif
//}

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_CAN_INTERFACE_H
