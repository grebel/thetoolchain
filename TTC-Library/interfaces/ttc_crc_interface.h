/** { ttc_crc_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for CRC device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 24 at 20180323 10:28:15 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_CRC_INTERFACE_H
#define TTC_CRC_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_crc_interface.c"
//

#include "../ttc_basic.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_crc_small
    #include "../crc/crc_small.h"  // low-level driver for crc devices on small architecture
#endif
#ifdef EXTENSION_crc_fast
    #include "../crc/crc_fast.h"  // low-level driver for crc devices on fast architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

// Check if at least one low-level driver could be activated
#ifndef EXTENSION_CRC_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_CRC! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _crc_
#endif

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_crc_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_crc_*() declaration in ../ttc_crc.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_crc_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_crc.h for prototypes of low-level driver functions!
 *
 */

void ttc_crc_interface_configuration_check( t_ttc_crc_config* Config ); //{  macro _driver_crc_interface_configuration_check(Config)
#ifdef ttc_driver_crc_configuration_check
    // enable following line to forward interface function as a macro definition
    #define _driver_crc_configuration_check ttc_driver_crc_configuration_check
#else
    #define _driver_crc_configuration_check ttc_crc_interface_configuration_check
#endif
//}
e_ttc_crc_errorcode ttc_crc_interface_deinit( t_ttc_crc_config* Config ); //{  macro _driver_crc_interface_deinit(Config)
#ifdef ttc_driver_crc_deinit
    // enable following line to forward interface function as a macro definition
    #define _driver_crc_deinit ttc_driver_crc_deinit
#else
    #define _driver_crc_deinit ttc_crc_interface_deinit
#endif
//}
e_ttc_crc_errorcode ttc_crc_interface_init( t_ttc_crc_config* Config ); //{  macro _driver_crc_interface_init(Config)
#ifdef ttc_driver_crc_init
    // enable following line to forward interface function as a macro definition
    #define _driver_crc_init ttc_driver_crc_init
#else
    #define _driver_crc_init ttc_crc_interface_init
#endif
//}
e_ttc_crc_errorcode ttc_crc_interface_load_defaults( t_ttc_crc_config* Config ); //{  macro _driver_crc_interface_load_defaults(Config)
#ifdef ttc_driver_crc_load_defaults
    // enable following line to forward interface function as a macro definition
    #define _driver_crc_load_defaults ttc_driver_crc_load_defaults
#else
    #define _driver_crc_load_defaults ttc_crc_interface_load_defaults
#endif
//}
void ttc_crc_interface_prepare(); //{  macro _driver_crc_interface_prepare()
#ifdef ttc_driver_crc_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_crc_prepare ttc_driver_crc_prepare
#else
    #define _driver_crc_prepare ttc_crc_interface_prepare
#endif
//}
void ttc_crc_interface_reset( t_ttc_crc_config* Config ); //{  macro _driver_crc_interface_reset(Config)
#ifdef ttc_driver_crc_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_crc_reset ttc_driver_crc_reset
#else
    #define _driver_crc_reset ttc_crc_interface_reset
#endif
//}
t_u16 ttc_crc_interface_crc16_calculate_0x1021( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue ); //{  macro _driver_crc_interface_crc16_calculate_0x1021(Buffer, Amount, StartValue)
#ifdef ttc_driver_crc_crc16_calculate_0x1021
    // enable following line to forward interface function as a macro definition
    #define _driver_crc_crc16_calculate_0x1021 ttc_driver_crc_crc16_calculate_0x1021
#else
    #define _driver_crc_crc16_calculate_0x1021 ttc_crc_interface_crc16_calculate_0x1021
#endif
//}
t_u16 ttc_crc_interface_crc16_calculate_ccitt( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue ); //{  macro _driver_crc_interface_crc16_calculate_ccitt(Buffer, Amount, StartValue)
#ifdef ttc_driver_crc_crc16_calculate_ccitt
    // enable following line to forward interface function as a macro definition
    #define _driver_crc_crc16_calculate_ccitt ttc_driver_crc_crc16_calculate_ccitt
#else
    #define _driver_crc_crc16_calculate_ccitt ttc_crc_interface_crc16_calculate_ccitt
#endif
//}
BOOL ttc_crc_interface_crc16_check_0x1021( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue ); //{  macro _driver_crc_interface_crc16_check_0x1021(Buffer, Amount, StartValue)
#ifdef ttc_driver_crc_crc16_check_0x1021
    // enable following line to forward interface function as a macro definition
    #define _driver_crc_crc16_check_0x1021 ttc_driver_crc_crc16_check_0x1021
#else
    #define _driver_crc_crc16_check_0x1021 ttc_crc_interface_crc16_check_0x1021
#endif
//}
BOOL ttc_crc_interface_crc16_check_ccitt( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue ); //{  macro _driver_crc_interface_crc16_check_ccitt(Buffer, Amount, StartValue)
#ifdef ttc_driver_crc_crc16_check_ccitt
    // enable following line to forward interface function as a macro definition
    #define _driver_crc_crc16_check_ccitt ttc_driver_crc_crc16_check_ccitt
#else
    #define _driver_crc_crc16_check_ccitt ttc_crc_interface_crc16_check_ccitt
#endif
//}
t_u8 ttc_crc_interface_crc7_calculate( const t_u8* Buffer, t_u16 Amount, t_u8 StartValue ); //{  macro _driver_crc_interface_crc7_calculate(Buffer, Amount, StartValue)
#ifdef ttc_driver_crc_crc7_calculate
    // enable following line to forward interface function as a macro definition
    #define _driver_crc_crc7_calculate ttc_driver_crc_crc7_calculate
#else
    #define _driver_crc_crc7_calculate ttc_crc_interface_crc7_calculate
#endif
//}
BOOL ttc_crc_interface_crc7_check( const t_u8* Buffer, t_u16 Amount, t_u8 StartValue ); //{  macro _driver_crc_interface_crc7_check(Buffer, Amount, StartValue)
#ifdef ttc_driver_crc_crc7_check
    // enable following line to forward interface function as a macro definition
    #define _driver_crc_crc7_check ttc_driver_crc_crc7_check
#else
    #define _driver_crc_crc7_check ttc_crc_interface_crc7_check
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_crc_interface_foo(t_ttc_crc_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_CRC_INTERFACE_H
