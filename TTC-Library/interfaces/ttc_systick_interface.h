/** { ttc_systick_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for SYSTICK device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 23 at 20160926 13:55:21 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_SYSTICK_INTERFACE_H
#define TTC_SYSTICK_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_systick_interface.c"
//

#include "../ttc_basic.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_systick_cortexm3
    #include "../systick/systick_cortexm3.h"
#endif
#ifdef EXTENSION_systick_freertos
    #include "../systick/systick_freertos.h"  // low-level driver for systick devices on freertos architecture
#endif
#ifdef EXTENSION_systick_
    #include "../systick/systick_.h"  // low-level driver for systick devices on  architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

// Check if at least one low-level driver could be activated
#ifndef EXTENSION_SYSTICK_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_SYSTICK! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _systick_
#endif

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_systick_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_systick_*() declaration in ../ttc_systick.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_systick_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_systick.h for prototypes of low-level driver functions!
 *
 */

e_ttc_systick_errorcode ttc_systick_interface_deinit( t_ttc_systick_config* Config ); //{  macro _driver_systick_interface_deinit(Config)
#ifdef ttc_driver_systick_deinit
    // enable following line to forward interface function as a macro definition
    #define _driver_systick_deinit(Config) ttc_driver_systick_deinit(Config)
#else
    #define _driver_systick_deinit(Config) ttc_systick_interface_deinit(Config)
#endif
//}
e_ttc_systick_errorcode ttc_systick_interface_load_defaults( t_ttc_systick_config* Config ); //{  macro _driver_systick_interface_load_defaults(Config)
#ifdef ttc_driver_systick_load_defaults
    // enable following line to forward interface function as a macro definition
    #define _driver_systick_load_defaults(Config) ttc_driver_systick_load_defaults(Config)
#else
    #define _driver_systick_load_defaults(Config) ttc_systick_interface_load_defaults(Config)
#endif
//}
e_ttc_systick_errorcode ttc_systick_interface_init( t_ttc_systick_config* Config ); //{  macro _driver_systick_interface_init(Config)
#ifdef ttc_driver_systick_init
    // enable following line to forward interface function as a macro definition
    #define _driver_systick_init(Config) ttc_driver_systick_init(Config)
#else
    #define _driver_systick_init(Config) ttc_systick_interface_init(Config)
#endif
//}
void ttc_systick_interface_configuration_check( t_ttc_systick_config* Config ); //{  macro _driver_systick_interface_configuration_check(Config)
#ifdef ttc_driver_systick_configuration_check
    // enable following line to forward interface function as a macro definition
    #define _driver_systick_configuration_check(Config) ttc_driver_systick_configuration_check(Config)
#else
    #define _driver_systick_configuration_check(Config) ttc_systick_interface_configuration_check(Config)
#endif
//}
void ttc_systick_interface_prepare(); //{  macro _driver_systick_interface_prepare()
#ifdef ttc_driver_systick_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_systick_prepare() ttc_driver_systick_prepare()
#else
    #define _driver_systick_prepare() ttc_systick_interface_prepare()
#endif
//}
void ttc_systick_interface_reset( t_ttc_systick_config* Config ); //{  macro _driver_systick_interface_reset(Config)
#ifdef ttc_driver_systick_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_systick_reset(Config) ttc_driver_systick_reset(Config)
#else
    #define _driver_systick_reset(Config) ttc_systick_interface_reset(Config)
#endif
//}
t_base ttc_systick_interface_get_elapsed_ticks(); //{  macro _driver_systick_interface_get_elapsed_ticks()
#ifdef ttc_driver_systick_get_elapsed_ticks
    // enable following line to forward interface function as a macro definition
    #define _driver_systick_get_elapsed_ticks ttc_driver_systick_get_elapsed_ticks
#else
    #define _driver_systick_get_elapsed_ticks ttc_systick_interface_get_elapsed_ticks
#endif
//}
t_base ttc_systick_interface_get_elapsed_usecs(); //{  macro _driver_systick_interface_get_elapsed_usecs()
#ifdef ttc_driver_systick_get_elapsed_usecs
    // enable following line to forward interface function as a macro definition
    #define _driver_systick_get_elapsed_usecs ttc_driver_systick_get_elapsed_usecs
#else
    #define _driver_systick_get_elapsed_usecs ttc_systick_interface_get_elapsed_usecs
#endif
//}
t_base ttc_systick_interface_get_us_ticks( t_base Microseconds ); //{  macro _driver_systick_interface_get_us_ticks(Microseconds)
#ifdef ttc_driver_systick_get_us_ticks
    // enable following line to forward interface function as a macro definition
    #define _driver_systick_get_us_ticks ttc_driver_systick_get_us_ticks
#else
    #define _driver_systick_get_us_ticks ttc_systick_interface_get_us_ticks
#endif
//}
t_base ttc_systick_interface_get_elapsed_ticks_isr(); //{  macro _driver_systick_interface_get_elapsed_ticks_isr()
#ifdef ttc_driver_systick_get_elapsed_ticks_isr
    // enable following line to forward interface function as a macro definition
    #define _driver_systick_get_elapsed_ticks_isr ttc_driver_systick_get_elapsed_ticks_isr
#else
    #define _driver_systick_get_elapsed_ticks_isr ttc_systick_interface_get_elapsed_ticks_isr
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_systick_interface_foo(t_ttc_systick_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_SYSTICK_INTERFACE_H
