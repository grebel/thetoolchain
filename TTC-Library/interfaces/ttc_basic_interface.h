/** { ttc_basic_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for BASIC device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 21 at 20140603 04:04:24 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_BASIC_INTERFACE_H
#define TTC_BASIC_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_basic_interface.c"
//

#include "../ttc_basic.h"
#include "../ttc_basic_types.h"

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_basic_cm3
    #include "../basic/basic_cm3.h"
#endif
#ifdef EXTENSION_basic_stm32f30x
    #include "../basic/basic_stm32f30x.h"  // low-level driver for basic devices on stm32f30x architecture
#endif
#ifdef EXTENSION_basic_stm32l1xx
    #include "../basic/basic_stm32l1xx.h"  // low-level driver for basic devices on stm32l1xx architecture
#endif
#ifdef EXTENSION_basic_stm32l0xx
    #include "../basic/basic_stm32l0xx.h"  // low-level driver for basic devices on stm32l0xx architecture
#endif
#ifdef EXTENSION_basic_stm32f1xx
    #include "../basic/basic_stm32f1xx.h"  // low-level driver for basic devices on stm32f1xx architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)


//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_basic_*() declaration in ../ttc_basic.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_basic_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_basic.h for prototypes of low-level driver functions!
 *
 */

void ttc_basic_interface_prepare(); //{  macro _driver_basic_interface_prepare()
#ifdef ttc_driver_basic_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_prepare() ttc_driver_basic_prepare()
#else
    #define _driver_basic_prepare() ttc_basic_interface_prepare()
#endif
//}

t_u32 ttc_basic_interface_32_atomic_read_write( t_u32* Address, t_u32 NewValue ); //{  macro _driver_basic_interface_32_atomic_read_write(Address, NewValue)
#ifdef ttc_driver_basic_32_atomic_read_write
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_32_atomic_read_write(Address, NewValue) ttc_driver_basic_32_atomic_read_write(Address, NewValue)
#else
    #define _driver_basic_32_atomic_read_write(Address, NewValue) ttc_basic_interface_32_atomic_read_write(Address, NewValue)
#endif
//}
t_u16 ttc_basic_interface_16_atomic_read_write( t_u16* Address, t_u16 NewValue ); //{  macro _driver_basic_interface_16_atomic_read_write(Address, NewValue)
#ifdef ttc_driver_basic_16_atomic_read_write
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_16_atomic_read_write(Address, NewValue) ttc_driver_basic_16_atomic_read_write(Address, NewValue)
#else
    #define _driver_basic_16_atomic_read_write(Address, NewValue) ttc_basic_interface_16_atomic_read_write(Address, NewValue)
#endif
//}
t_u8 ttc_basic_interface_08_atomic_read_write( t_u8* Address, t_u8 NewValue ); //{  macro _driver_basic_interface_08_atomic_read_write(Address, NewValue)
#ifdef ttc_driver_basic_08_atomic_read_write
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_08_atomic_read_write(Address, NewValue) ttc_driver_basic_08_atomic_read_write(Address, NewValue)
#else
    #define _driver_basic_08_atomic_read_write(Address, NewValue) ttc_basic_interface_08_atomic_read_write(Address, NewValue)
#endif
//}
t_u32 ttc_basic_interface_32_atomic_add( t_u32* Address, t_s32 Amount ); //{  macro _driver_basic_interface_32_atomic_add(Address, NewValue)
#ifdef ttc_driver_basic_32_atomic_add
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_32_atomic_add(Address, NewValue) ttc_driver_basic_32_atomic_add(Address, NewValue)
#else
    #define _driver_basic_32_atomic_add(Address, NewValue) ttc_basic_interface_32_atomic_add(Address, NewValue)
#endif
//}
t_u16 ttc_basic_interface_16_atomic_add( t_u16* Address, t_s16 NewValue ); //{  macro _driver_basic_interface_16_atomic_add(Address, NewValue)
#ifdef ttc_driver_basic_16_atomic_add
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_16_atomic_add(Address, NewValue) ttc_driver_basic_16_atomic_add(Address, NewValue)
#else
    #define _driver_basic_16_atomic_add(Address, NewValue) ttc_basic_interface_16_atomic_add(Address, NewValue)
#endif
//}
t_u8 ttc_basic_interface_08_atomic_add( t_u8* Address, t_s8 NewValue ); //{  macro _driver_basic_interface_08_atomic_add(Address, NewValue)
#ifdef ttc_driver_basic_08_atomic_add
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_08_atomic_add(Address, NewValue) ttc_driver_basic_08_atomic_add(Address, NewValue)
#else
    #define _driver_basic_08_atomic_add(Address, NewValue) ttc_basic_interface_08_atomic_add(Address, NewValue)
#endif
//}
void ttc_basic_interface_16_write_little_endian( t_u8* Buffer, t_u16 Data ); //{  macro _driver_basic_interface_16_write_little_endian(Buffer, Data)
#ifdef ttc_driver_basic_16_write_little_endian
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_16_write_little_endian(Buffer, Data) ttc_driver_basic_16_write_little_endian(Buffer, Data)
#else
    #define _driver_basic_16_write_little_endian(Buffer, Data) ttc_basic_interface_16_write_little_endian(Buffer, Data)
#endif
//}
void ttc_basic_interface_16_write_big_endian( t_u8* Buffer, t_u16 Data ); //{  macro _driver_basic_interface_16_write_big_endian(Buffer, Data)
#ifdef ttc_driver_basic_16_write_big_endian
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_16_write_big_endian(Buffer, Data) ttc_driver_basic_16_write_big_endian(Buffer, Data)
#else
    #define _driver_basic_16_write_big_endian(Buffer, Data) ttc_basic_interface_16_write_big_endian(Buffer, Data)
#endif
//}
void ttc_basic_interface_32_write_little_endian( t_u8* Buffer, t_u32 Data ); //{  macro _driver_basic_interface_32_write_little_endian(Buffer, Data)
#ifdef ttc_driver_basic_32_write_little_endian
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_32_write_little_endian(Buffer, Data) ttc_driver_basic_32_write_little_endian(Buffer, Data)
#else
    #define _driver_basic_32_write_little_endian(Buffer, Data) ttc_basic_interface_32_write_little_endian(Buffer, Data)
#endif
//}
void ttc_basic_interface_32_write_big_endian( t_u8* Buffer, t_u32 Data ); //{  macro _driver_basic_interface_32_write_big_endian(Buffer, Data)
#ifdef ttc_driver_basic_32_write_big_endian
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_32_write_big_endian(Buffer, Data) ttc_driver_basic_32_write_big_endian(Buffer, Data)
#else
    #define _driver_basic_32_write_big_endian(Buffer, Data) ttc_basic_interface_32_write_big_endian(Buffer, Data)
#endif
//}
t_u32 ttc_basic_interface_32_read_little_endian( t_u8* Buffer ); //{  macro _driver_basic_interface_32_read_little_endian(Buffer)
#ifdef ttc_driver_basic_32_read_little_endian
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_32_read_little_endian(Buffer) ttc_driver_basic_32_read_little_endian(Buffer)
#else
    #define _driver_basic_32_read_little_endian(Buffer) ttc_basic_interface_32_read_little_endian(Buffer)
#endif
//}
t_u16 ttc_basic_interface_16_read_big_endian( t_u8* Buffer ); //{  macro _driver_basic_interface_16_read_big_endian(Buffer)
#ifdef ttc_driver_basic_16_read_big_endian
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_16_read_big_endian(Buffer) ttc_driver_basic_16_read_big_endian(Buffer)
#else
    #define _driver_basic_16_read_big_endian(Buffer) ttc_basic_interface_16_read_big_endian(Buffer)
#endif
//}
t_u16 ttc_basic_interface_16_read_little_endian( t_u8* Buffer ); //{  macro _driver_basic_interface_16_read_little_endian(Buffer)
#ifdef ttc_driver_basic_16_read_little_endian
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_16_read_little_endian(Buffer) ttc_driver_basic_16_read_little_endian(Buffer)
#else
    #define _driver_basic_16_read_little_endian(Buffer) ttc_basic_interface_16_read_little_endian(Buffer)
#endif
//}
t_u32 ttc_basic_interface_32_read_big_endian( t_u8* Buffer ); //{  macro _driver_basic_interface_32_read_big_endian(Buffer)
#ifdef ttc_driver_basic_32_read_big_endian
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_32_read_big_endian(Buffer) ttc_driver_basic_32_read_big_endian(Buffer)
#else
    #define _driver_basic_32_read_big_endian(Buffer) ttc_basic_interface_32_read_big_endian(Buffer)
#endif
//}
void ttc_basic_interface_32_swap_endian( t_u32* Variable ); //{  macro _driver_basic_interface_32_swap_endian(Variable)
#ifdef ttc_driver_basic_32_swap_endian
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_32_swap_endian ttc_driver_basic_32_swap_endian
#else
    #define _driver_basic_32_swap_endian ttc_basic_interface_32_swap_endian
#endif
//}
void ttc_basic_interface_16_swap_endian( t_u16* Variable ); //{  macro _driver_basic_interface_16_swap_endian(Variable)
#ifdef ttc_driver_basic_16_swap_endian
    // enable following line to forward interface function as a macro definition
    #define _driver_basic_16_swap_endian ttc_driver_basic_16_swap_endian
#else
    #define _driver_basic_16_swap_endian ttc_basic_interface_16_swap_endian
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_basic_interface_foo(t_ttc_basic_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_BASIC_INTERFACE_H
