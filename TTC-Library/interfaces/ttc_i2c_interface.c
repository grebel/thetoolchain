/** { ttc_i2c_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for I2C device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_i2c_*() functions.
 *  Instead they can call _driver_i2c_*() pendants.
 *  E.g.: ttc_i2c_reset() -> _driver_i2c_reset()
 *
 *  Created from template ttc_device_interface.c revision 24 at 20180131 15:13:24 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_i2c_interface.h".
//
#include "ttc_i2c_interface.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

// interface will include low-driver for us. Check if at least one low-level driver could be activated
#ifndef EXTENSION_I2C_DRIVER_AVAILABLE
#  error Missing low-level driver for TTC_<DRIVER>! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _i2c_
#endif

//}Includes
/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Private Function declarations ****************************************

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Private Function declarations
//{ Function definitions *************************************************

#ifndef ttc_driver_i2c_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_load_defaults() (using default implementation)!

e_ttc_i2c_errorcode ttc_i2c_interface_load_defaults( t_ttc_i2c_config* Config ) {
    Assert_I2C_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_load_defaults() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_check_flag  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_check_flag() (using default implementation)!

BOOL ttc_i2c_interface_check_flag( volatile t_ttc_i2c_base_register* BaseRegister, e_ttc_i2c_flag_code FlagCode ) {
    Assert_I2C_Writable( BaseRegister, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_check_flag() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_get_event_value  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_get_event_value() (using default implementation)!

t_base ttc_i2c_interface_get_event_value( volatile t_ttc_i2c_base_register* BaseRegister ) {
    Assert_I2C_Writable( BaseRegister, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_get_event_value() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_slave_read_bytes  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_slave_read_bytes() (using default implementation)!

t_u8 ttc_i2c_interface_slave_read_bytes( t_ttc_i2c_config* Config, t_u8 BufferSize, t_u8* Buffer ) {
    Assert_I2C_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_I2C_Writable( Buffer, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_slave_read_bytes() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_get_features  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_get_features() (using default implementation)!

t_ttc_i2c_config* ttc_i2c_interface_get_features( t_ttc_i2c_config* Config ) {
    Assert_I2C_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_get_features() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_slave_send_bytes  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_slave_send_bytes() (using default implementation)!

t_u8 ttc_i2c_interface_slave_send_bytes( t_ttc_i2c_config* Config, t_u8 BufferSize, const t_u8* Buffer ) {
    Assert_I2C_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_I2C_Writable( Buffer, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_slave_send_bytes() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_master_send_slave_address  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_master_send_slave_address() (using default implementation)!

e_ttc_i2c_errorcode ttc_i2c_interface_master_send_slave_address( t_ttc_i2c_config* Config, t_u16 SlaveAddress, BOOL ReadMode ) {
    Assert_I2C_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_master_send_slave_address() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_master_condition_stop  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_master_condition_stop() (using default implementation)!

e_ttc_i2c_errorcode ttc_i2c_interface_master_condition_stop( t_ttc_i2c_config* Config ) {
    Assert_I2C_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_master_condition_stop() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_master_condition_start  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_master_condition_start() (using default implementation)!

e_ttc_i2c_errorcode ttc_i2c_interface_master_condition_start( t_ttc_i2c_config* Config ) {
    Assert_I2C_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_master_condition_start() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_deinit() (using default implementation)!

e_ttc_i2c_errorcode ttc_i2c_interface_deinit( t_ttc_i2c_config* Config ) {
    Assert_I2C_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_deinit() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_slave_check_own_address_received  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_slave_check_own_address_received() (using default implementation)!

BOOL ttc_i2c_interface_slave_check_own_address_received( volatile t_ttc_i2c_base_register* BaseRegister ) {
    Assert_I2C_Writable( BaseRegister, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_slave_check_own_address_received() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_slave_reset_bus  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_slave_reset_bus() (using default implementation)!

e_ttc_i2c_errorcode ttc_i2c_interface_slave_reset_bus( t_ttc_i2c_config* Config ) {
    Assert_I2C_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_slave_reset_bus() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_prepare() (using default implementation)!

void ttc_i2c_interface_prepare() {


#warning Function ttc_i2c_interface_prepare() is empty.


}

#endif
#ifndef ttc_driver_i2c_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_reset() (using default implementation)!

void ttc_i2c_interface_reset( t_ttc_i2c_config* Config ) {
    Assert_I2C_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_reset() is empty.


}

#endif
#ifndef ttc_driver_i2c_init  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_init() (using default implementation)!

e_ttc_i2c_errorcode ttc_i2c_interface_init( t_ttc_i2c_config* Config ) {
    Assert_I2C_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_init() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_master_send_bytes  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_master_send_bytes() (using default implementation)!

e_ttc_i2c_errorcode ttc_i2c_interface_master_send_bytes( t_ttc_i2c_config* Config, t_u8 Amount, const t_u8* Buffer ) {
    Assert_I2C_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_I2C_Writable( Buffer, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_master_send_bytes() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_slave_check_read_mode  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_slave_check_read_mode() (using default implementation)!

BOOL ttc_i2c_interface_slave_check_read_mode( volatile t_ttc_i2c_base_register* BaseRegister ) {
    Assert_I2C_Writable( BaseRegister, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_slave_check_read_mode() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_i2c_enable_acknowledge  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_enable_acknowledge() (using default implementation)!

void ttc_i2c_interface_enable_acknowledge( volatile t_ttc_i2c_base_register* BaseRegister, BOOL Enable ) {
    Assert_I2C_Writable( BaseRegister, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_enable_acknowledge() is empty.


}

#endif
#ifndef ttc_driver_i2c_master_read_bytes  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_i2c_master_read_bytes() (using default implementation)!

e_ttc_i2c_errorcode ttc_i2c_interface_master_read_bytes( t_ttc_i2c_config* Config, t_u8 Amount, t_u8* Buffer ) {
    Assert_I2C_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_I2C_Writable( Buffer, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_i2c_interface_master_read_bytes() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_i2c_interface_foo(t_ttc_i2c_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
