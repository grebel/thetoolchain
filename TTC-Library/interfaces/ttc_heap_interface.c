/** { ttc_heap_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for HEAP device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_heap_*() functions.
 *  Instead they can call _driver_heap_*() pendants.
 *  E.g.: ttc_heap_reset() -> _driver_heap_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20140301 07:28:17 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_heap_interface.h"

/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_heap_get_free_size  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_heap_get_free_size() (using default implementation)!

t_base ttc_heap_interface_get_free_size() {



    return ( t_base ) 0;
}

#endif
#ifndef ttc_driver_heap_alloc  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_heap_alloc() (using default implementation)!

void* ttc_heap_interface_alloc( t_base Size ) {



    return ( void* ) 0;
}

#endif
#ifndef ttc_driver_heap_free  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_heap_free() (using default implementation)!

void* ttc_heap_interface_free( void* Block ) {
    Assert_HEAP_Writable( Block, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( void* ) 0;
}

#endif
#ifndef ttc_driver_heap_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_heap_prepare() (using default implementation)!

void ttc_heap_interface_prepare( t_u8* HeapStart, t_base HeapSize ) {
    Assert_HEAP_Writable( HeapStart, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif
#ifndef ttc_driver_heap_alloc_zeroed  // no low-level implementation: use default implementation

void* ttc_heap_interface_alloc_zeroed( t_base Size ) {

    return ( void* ) 0;
}

#endif
#ifndef ttc_driver_heap_temporary_alloc // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_heap_interface_temporary_alloc() (using default implementation)!

void* ttc_heap_interface_temporary_alloc( t_base Size ) {
    ( void ) Size;

    return ( void* ) 0; // function not implemented. Add implementation to current low-level driver!
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_heap_interface_foo(t_ttc_heap_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
