/** { ttc_sdcard_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for SDCARD device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_sdcard_*() functions.
 *  Instead they can call _driver_sdcard_*() pendants.
 *  E.g.: ttc_sdcard_reset() -> _driver_sdcard_reset()
 *
 *  Created from template ttc_device_interface.c revision 24 at 20180130 09:42:26 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_sdcard_interface.h".
//
#include "ttc_sdcard_interface.h"
#include "../ttc_task.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

// interface will include low-driver for us. Check if at least one low-level driver could be activated
#ifndef EXTENSION_SDCARD_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_<DRIVER>! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _sdcard_
#endif

//}Includes
/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Private Function declarations ****************************************

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Private Function declarations
//{ Function definitions *************************************************

#ifndef ttc_driver_sdcard_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_reset() (using default implementation)!

void ttc_sdcard_interface_reset( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_reset() is empty.


}

#endif
#ifndef ttc_driver_sdcard_configuration_check  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_configuration_check() (using default implementation)!

void ttc_sdcard_interface_configuration_check( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_configuration_check() is empty.


}

#endif
#ifndef ttc_driver_sdcard_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_load_defaults() (using default implementation)!

e_ttc_sdcard_errorcode ttc_sdcard_interface_load_defaults( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_load_defaults() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_sdcard_init  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_init() (using default implementation)!

e_ttc_sdcard_errorcode ttc_sdcard_interface_init( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_init() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_sdcard_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_deinit() (using default implementation)!

e_ttc_sdcard_errorcode ttc_sdcard_interface_deinit( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_deinit() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_sdcard_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_prepare() (using default implementation)!

void ttc_sdcard_interface_prepare() {


#warning Function ttc_sdcard_interface_prepare() is empty.


}

#endif
#ifndef ttc_driver_sdcard_medium_detect  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_medium_detect() (using default implementation)!

e_ttc_storage_event ttc_sdcard_interface_detect( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_detect() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_sdcard_command_r1  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_command_r1() (using default implementation)!

u_ttc_sdcard_response ttc_sdcard_interface_command_r1( t_ttc_sdcard_config* Config, const t_ttc_sdcard_command_argument* CommandArgument ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_command_r1() is empty.

    u_ttc_sdcard_response Response;
    Response.byte = 0xff; // 0xff == no response

    return Response;
}

#endif
#ifndef ttc_driver_sdcard_medium_go_idle  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_medium_go_idle() (using default implementation)!

e_ttc_sdcard_errorcode ttc_sdcard_interface_medium_go_idle( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_medium_go_idle() is empty.

    TTC_TASK_RETURN( 1 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_sdcard_medium_unmount  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_medium_unmount() (using default implementation)!

void ttc_sdcard_interface_medium_unmount( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_medium_unmount() is empty.


}

#endif
#ifndef ttc_driver_sdcard_medium_detect  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_medium_detect() (using default implementation)!

e_ttc_storage_event ttc_sdcard_interface_medium_detect( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_medium_detect() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_sdcard_command_rn  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_command_rn() (using default implementation)!

t_u16 ttc_sdcard_interface_command_rn( t_ttc_sdcard_config* Config, const t_ttc_sdcard_command_argument* CommandArgument, t_u8* Buffer, t_u16 Amount ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_Writable( CommandArgument, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_Writable( Buffer, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_command_rn() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_sdcard_set_speed  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_set_speed() (using default implementation)!

e_ttc_sdcard_errorcode ttc_sdcard_interface_set_speed( t_ttc_sdcard_config* Config, e_ttc_sdcard_speed InterfaceSpeed ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_set_speed() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_sdcard_block_read  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_block_read() (using default implementation)!

e_ttc_sdcard_errorcode ttc_sdcard_interface_block_read( t_ttc_sdcard_config* Config, t_u32 BlockAddress, t_u8* Buffer, t_u16 BufferSize ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_Writable( Buffer, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_block_read() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_sdcard_block_write  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_block_write() (using default implementation)!

e_ttc_sdcard_errorcode ttc_sdcard_interface_block_write( t_ttc_sdcard_config* Config, t_u32 BlockAddress, const t_u8* Buffer, t_u16 BufferSize ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_Readable( Buffer, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_block_write() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_sdcard_wait_busy  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_sdcard_wait_busy() (using default implementation)!

BOOL ttc_sdcard_interface_wait_busy( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_sdcard_interface_wait_busy() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_sdcard_interface_foo(t_ttc_sdcard_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
