/** { ttc_rtc_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for RTC device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_rtc_*() functions.
 *  Instead they can call _driver_rtc_*() pendants.
 *  E.g.: ttc_rtc_reset() -> _driver_rtc_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20150317 10:07:45 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_rtc_interface.h".
//
#include "ttc_rtc_interface.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_rtc_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_rtc_load_defaults() (using default implementation)!

e_ttc_rtc_errorcode ttc_rtc_interface_load_defaults( t_ttc_rtc_config* Config ) {
    Assert_RTC_Writable( Config , ttc_assert_origin_auto); // pointers must not be NULL

#warning Function ttc_rtc_interface_load_defaults() is empty.

    return ( e_ttc_rtc_errorcode ) 0;
}

#endif
#ifndef ttc_driver_rtc_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_rtc_prepare() (using default implementation)!

void ttc_rtc_interface_prepare() {


#warning Function ttc_rtc_interface_prepare() is empty.


}

#endif
#ifndef ttc_driver_rtc_init  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_rtc_init() (using default implementation)!

e_ttc_rtc_errorcode ttc_rtc_interface_init( t_ttc_rtc_config* Config ) {
    Assert_RTC_Writable( Config , ttc_assert_origin_auto); // pointers must not be NULL

#warning Function ttc_rtc_interface_init() is empty.

    return ( e_ttc_rtc_errorcode ) 0;
}

#endif
#ifndef ttc_driver_rtc_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_rtc_reset() (using default implementation)!

void ttc_rtc_interface_reset( t_ttc_rtc_config* Config ) {
    Assert_RTC_Writable( Config , ttc_assert_origin_auto); // pointers must not be NULL

#warning Function ttc_rtc_interface_reset() is empty.


}

#endif
#ifndef ttc_driver_rtc_get_features  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_rtc_get_features() (using default implementation)!

t_ttc_rtc_config* ttc_rtc_interface_get_features( t_ttc_rtc_config* Config ) {
    Assert_RTC_Writable( Config , ttc_assert_origin_auto); // pointers must not be NULL

#warning Function ttc_rtc_interface_get_features() is empty.

    return ( t_ttc_rtc_config* ) 0;
}

#endif
#ifndef ttc_driver_rtc_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_rtc_deinit() (using default implementation)!

e_ttc_rtc_errorcode ttc_rtc_interface_deinit( t_ttc_rtc_config* Config ) {
    Assert_RTC_Writable( Config , ttc_assert_origin_auto); // pointers must not be NULL

#warning Function ttc_rtc_interface_deinit() is empty.

    return ( e_ttc_rtc_errorcode ) 0;
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_rtc_interface_foo(t_ttc_rtc_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions