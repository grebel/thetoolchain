/** { ttc_network_layer_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for NETWORK_LAYER device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_network_layer_*() functions.
 *  Instead they can call _driver_network_layer_*() pendants.
 *  E.g.: ttc_network_layer_reset() -> _driver_network_layer_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20140224 16:21:32 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_network_layer_interface.h"

/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_network_layer_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_network_layer_prepare() (using default implementation)!

void ttc_network_layer_interface_prepare() {




}

#endif
#ifndef ttc_driver_network_layer_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_network_layer_deinit() (using default implementation)!

e_ttc_network_layer_errorcode ttc_network_layer_interface_deinit( t_ttc_network_layer_config* Config ) {
    Assert_NETWORK_LAYER( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_network_layer_errorcode ) 0;
}

#endif
e_ttc_network_layer_errorcode ttc_network_layer_interface_init( t_ttc_network_layer_config* Config ) {
    Assert_NETWORK_LAYER( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    switch ( Config->Architecture ) {
        case network_layer_usart: return network_layer_usart_init( Config ); break;
        // ...
        default: Assert( 0, ttc_assert_origin_auto ); break;
    }

    return ( e_ttc_network_layer_errorcode ) 0;
}
#ifndef ttc_driver_network_layer_get_features  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_network_layer_get_features() (using default implementation)!

e_ttc_network_layer_errorcode ttc_network_layer_interface_get_features( t_ttc_network_layer_config* Config ) {
    Assert_NETWORK_LAYER( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_network_layer_errorcode ) 0;
}

#endif
#ifndef ttc_driver_network_layer_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_network_layer_reset() (using default implementation)!

e_ttc_network_layer_errorcode ttc_network_layer_interface_reset( t_ttc_network_layer_config* Config ) {
    Assert_NETWORK_LAYER( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_network_layer_errorcode ) 0;
}

#endif
#ifndef ttc_driver_network_layer_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_network_layer_load_defaults() (using default implementation)!

e_ttc_network_layer_errorcode ttc_network_layer_interface_load_defaults( t_ttc_network_layer_config* Config ) {
    Assert_NETWORK_LAYER( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_network_layer_errorcode ) 0;
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_network_layer_interface_foo(t_ttc_network_layer_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
