/** { ttc_real_time_clock_interface.c ************************************************
 *
 *                           The ToolChain
 *                           
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *   Interface layer between high- and low-level driver for REAL_TIME_CLOCK device.
 *
 *   The functions implemented here are used when additional runtime  
 *   computations arerequired to call the corresponding low-level driver.
 *   These computations should be held very simple and fast. Every additional
 *
 *   Adding a new low-level implementation may require changes in this file.
 *   
 *   Authors: Gregor Rebel
 *   
}*/
//{ Defines/ TypDefs *****************************************************
//} Defines

#include "ttc_real_time_clock_interface.h"

//{ Global Variables *****************************************************

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_real_time_clock_interface_prepare
void ttc_real_time_clock_interface_prepare() {

  // fill in logic to prepare all activated low-level drivers

}
#endif
#ifndef ttc_real_time_clock_interface_load_defaults
e_ttc_real_time_clock_errorcode ttc_real_time_clock_interface_load_defaults(t_ttc_real_time_clock_config* Config) {
  
  // fill in logic to access low-level driver suitable for given Config

  return ec_real_time_clock_OK;
}
#endif
#ifndef ttc_real_time_clock_interface_deinit
e_ttc_real_time_clock_errorcode ttc_real_time_clock_interface_deinit(t_ttc_real_time_clock_config* Config) {
  
  // fill in logic to access low-level driver suitable for given Config

  return ec_real_time_clock_OK;
}
#endif
#ifndef ttc_real_time_clock_interface_init
e_ttc_real_time_clock_errorcode ttc_real_time_clock_interface_init(t_ttc_real_time_clock_config* Config) {
  
  // fill in logic to access low-level driver suitable for given Config

  return ec_real_time_clock_OK;
}
#endif
#ifndef ttc_real_time_clock_interface_reset
e_ttc_real_time_clock_errorcode ttc_real_time_clock_interface_reset(t_ttc_real_time_clock_config* Config) {
  
  // fill in logic to access low-level driver suitable for given Config
  
  return ec_real_time_clock_OK;
}
#endif
#ifndef ttc_real_time_clock_interface_get_time
e_ttc_real_time_clock_errorcode ttc_real_time_clock_interface_get_time(t_u8 LogicalIndex,ttc_real_time_t_clock_time* Time) {

  // fill in logic to access low-level driver suitable for given Config

  return ec_real_time_clock_OK;
}
#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
