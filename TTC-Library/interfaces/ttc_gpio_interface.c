/** { ttc_gpio_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for GPIO device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_gpio_*() functions.
 *  Instead they can call _driver_gpio_*() pendants.
 *  E.g.: ttc_gpio_reset() -> _driver_gpio_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20140205 09:01:30 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_gpio_interface.h"

/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_gpio_get  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

BOOL ttc_gpio_interface_get( e_ttc_gpio_pin PortPin ) {



    return ( BOOL ) 0;
}

#endif
#ifndef ttc_driver_gpio_alternate_function  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)
void ttc_gpio_interface_alternate_function( e_ttc_gpio_pin PortPin, t_u8 AlternateFunction ) {




}
#endif

#ifndef ttc_driver_gpio_prepare  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void ttc_gpio_interface_prepare() {




}

#endif
#ifndef ttc_driver_gpio_set  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void ttc_gpio_interface_set( e_ttc_gpio_pin PortPin ) {




}

#endif
#ifndef ttc_driver_gpio_deinit  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void ttc_gpio_interface_deinit( e_ttc_gpio_pin PortPin ) {


}

#endif
#ifndef ttc_driver_gpio_init  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void ttc_gpio_interface_init( e_ttc_gpio_pin PortPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ) {




}

#endif
#ifndef ttc_driver_gpio_clr  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void ttc_gpio_interface_clr( e_ttc_gpio_pin PortPin ) {




}

#endif
#ifndef ttc_driver_gpio_get_configuration  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void ttc_gpio_interface_get_configuration( e_ttc_gpio_pin PortPin, t_ttc_gpio_config* Configuration ) {
    Assert_GPIO_Writable( Configuration, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif
#ifndef ttc_driver_gpio_parallel08_put_shift  // no low-level implementation: use default implementation
//#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_put_shift() (using default implementation)!

void ttc_gpio_interface_set_u8_shift( t_ttc_gpio_bank GPIOx, t_u8 Pin, t_u16 Value ) {

    // slow but safe implementation
    if ( Value & 0b00000001 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 0 ) ); }
    else                    { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 0 ) ); }
    if ( Value & 0b00000010 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 1 ) ); }
    else                    { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 1 ) ); }
    if ( Value & 0b00000100 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 2 ) ); }
    else                    { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 2 ) ); }
    if ( Value & 0b00001000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 3 ) ); }
    else                    { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 3 ) ); }
    if ( Value & 0b00010000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 4 ) ); }
    else                    { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 4 ) ); }
    if ( Value & 0b00100000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 5 ) ); }
    else                    { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 5 ) ); }
    if ( Value & 0b01000000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 6 ) ); }
    else                    { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 6 ) ); }
    if ( Value & 0b10000000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 7 ) ); }
    else                    { _driver_gpio_set( _driver_gpio_create_index( GPIOx, Pin + 7 ) ); }

}

#endif
#ifndef ttc_driver_gpio_parallel08_get_shift  // no low-level implementation: use default implementation
//#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_get_shift() (using default implementation)!

t_u8 ttc_gpio_interface_get_u8_shift( t_ttc_gpio_bank GPIOx, t_u8 FirstPin ) {

    // slow but safe implementation
    t_u8 Value = 0;
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx, FirstPin + 0 ) ) )  { Value |= 0b00000001; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx, FirstPin + 1 ) ) )  { Value |= 0b00000010; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx, FirstPin + 2 ) ) )  { Value |= 0b00000100; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx, FirstPin + 3 ) ) )  { Value |= 0b00001000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx, FirstPin + 4 ) ) )  { Value |= 0b00010000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx, FirstPin + 5 ) ) )  { Value |= 0b00100000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx, FirstPin + 6 ) ) )  { Value |= 0b01000000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx, FirstPin + 7 ) ) )  { Value |= 0b10000000; }

    return Value;
}

#endif
#ifndef ttc_driver_gpio_parallel16_put  // no low-level implementation: use default implementation
//#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_put() (using default implementation)!

void ttc_gpio_interface_set_u16( t_ttc_gpio_bank GPIOx, t_u16 Value ) {

    // slow but safe implementation
    if ( Value & 0b0000000000000001 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  0 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  0 ) ); }
    if ( Value & 0b0000000000000010 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  1 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  1 ) ); }
    if ( Value & 0b0000000000000100 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  2 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  2 ) ); }
    if ( Value & 0b0000000000001000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  3 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  3 ) ); }
    if ( Value & 0b0000000000010000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  4 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  4 ) ); }
    if ( Value & 0b0000000000100000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  5 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  5 ) ); }
    if ( Value & 0b0000000001000000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  6 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  6 ) ); }
    if ( Value & 0b0000000010000000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  7 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  7 ) ); }
    if ( Value & 0b0000000100000000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  8 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  8 ) ); }
    if ( Value & 0b0000001000000000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  9 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx,  9 ) ); }
    if ( Value & 0b0000010000000000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx, 10 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx, 10 ) ); }
    if ( Value & 0b0000100000000000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx, 11 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx, 11 ) ); }
    if ( Value & 0b0001000000000000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx, 12 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx, 12 ) ); }
    if ( Value & 0b0010000000000000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx, 13 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx, 13 ) ); }
    if ( Value & 0b0100000000000000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx, 14 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx, 14 ) ); }
    if ( Value & 0b1000000000000000 ) { _driver_gpio_set( _driver_gpio_create_index( GPIOx, 15 ) ); }
    else                            { _driver_gpio_set( _driver_gpio_create_index( GPIOx, 15 ) ); }
}

#endif
#ifndef ttc_driver_gpio_parallel16_get  // no low-level implementation: use default implementation
//#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_get() (using default implementation)!

t_u16 ttc_gpio_interface_get_u16( t_ttc_gpio_bank GPIOx ) {

    // slow but safe implementation
    t_u8 Value = 0;
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx,  0 ) ) )  { Value |= 0b0000000000000001; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx,  1 ) ) )  { Value |= 0b0000000000000010; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx,  2 ) ) )  { Value |= 0b0000000000000100; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx,  3 ) ) )  { Value |= 0b0000000000001000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx,  4 ) ) )  { Value |= 0b0000000000010000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx,  5 ) ) )  { Value |= 0b0000000000100000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx,  6 ) ) )  { Value |= 0b0000000001000000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx,  7 ) ) )  { Value |= 0b0000000010000000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx,  8 ) ) )  { Value |= 0b0000000100000000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx,  9 ) ) )  { Value |= 0b0000001000000000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx, 10 ) ) )  { Value |= 0b0000010000000000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx, 11 ) ) )  { Value |= 0b0000100000000000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx, 12 ) ) )  { Value |= 0b0001000000000000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx, 13 ) ) )  { Value |= 0b0010000000000000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx, 14 ) ) )  { Value |= 0b0100000000000000; }
    if ( _driver_gpio_get( _driver_gpio_create_index( GPIOx, 15 ) ) )  { Value |= 0b1000000000000000; }

    return Value;
}

#endif
#ifndef ttc_driver_gpio_parallel16_init  // no low-level implementation: use default implementation
//#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_init() (using default implementation)!

void ttc_gpio_interface_parallel16_init( t_ttc_gpio_bank GPIOx, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ) {

    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  0 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  1 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  2 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  3 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  4 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  5 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  6 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  7 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  8 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  9 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx, 10 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx, 11 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx, 12 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx, 13 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx, 14 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx, 15 ), Mode, Speed );

}

#endif
#ifndef ttc_driver_gpio_parallel08_init  // no low-level implementation: use default implementation
//#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_init() (using default implementation)!

void ttc_gpio_interface_parallel08_init( t_ttc_gpio_bank GPIOx, t_u8 FirstPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ) {

    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  0 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  1 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  2 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  3 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  4 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  5 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  6 ), Mode, Speed );
    _driver_gpio_init( _driver_gpio_create_index( GPIOx,  7 ), Mode, Speed );
}

#endif
#ifndef ttc_driver_gpio_from_index  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_from_index() (using default implementation)!

void ttc_gpio_interface_from_index( t_base PhysicalIndex, t_ttc_gpio_bank* GPIOx, t_u8* Pin ) {
    Assert_GPIO_Writable( GPIOx, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_GPIO_Writable( Pin, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif
#ifndef ttc_driver_gpio_create_index  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_create_index() (using default implementation)!

t_base ttc_gpio_interface_create_index( t_ttc_gpio_bank GPIOx, t_u8 Pin ) {



    return ( t_base ) 0;
}

#endif
#ifndef ttc_driver_gpio_put  // no low-level implementation: use default implementation
void ttc_gpio_interface_put( e_ttc_gpio_pin PortPin, BOOL Value ) {

    if ( Value ) {
        ttc_driver_gpio_set( PortPin );
    }
    else {
        ttc_driver_gpio_clr( PortPin );
    }
}
#endif
#ifndef ttc_driver_gpio_parallel08_10_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_10_get() (using default implementation)!

t_u8 ttc_gpio_interface_parallel08_10_get() {


#warning Function ttc_gpio_interface_parallel08_10_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel08_10_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_10_put() (using default implementation)!

void ttc_gpio_interface_parallel08_10_put( t_u8 Value ) {


#warning Function ttc_gpio_interface_parallel08_10_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel08_1_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_1_get() (using default implementation)!

t_u8 ttc_gpio_interface_parallel08_1_get() {


#warning Function ttc_gpio_interface_parallel08_1_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel08_1_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_1_put() (using default implementation)!

void ttc_gpio_interface_parallel08_1_put( t_u8 Value ) {


#warning Function ttc_gpio_interface_parallel08_1_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel08_2_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_2_get() (using default implementation)!

t_u8 ttc_gpio_interface_parallel08_2_get() {


#warning Function ttc_gpio_interface_parallel08_2_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel08_2_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_2_put() (using default implementation)!

void ttc_gpio_interface_parallel08_2_put( t_u8 Value ) {


#warning Function ttc_gpio_interface_parallel08_2_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel08_3_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_3_get() (using default implementation)!

t_u8 ttc_gpio_interface_parallel08_3_get() {


#warning Function ttc_gpio_interface_parallel08_3_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel08_3_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_3_put() (using default implementation)!

void ttc_gpio_interface_parallel08_3_put( t_u8 Value ) {


#warning Function ttc_gpio_interface_parallel08_3_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel08_4_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_4_get() (using default implementation)!

t_u8 ttc_gpio_interface_parallel08_4_get() {


#warning Function ttc_gpio_interface_parallel08_4_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel08_4_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_4_put() (using default implementation)!

void ttc_gpio_interface_parallel08_4_put( t_u8 Value ) {


#warning Function ttc_gpio_interface_parallel08_4_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel08_5_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_5_get() (using default implementation)!

t_u8 ttc_gpio_interface_parallel08_5_get() {


#warning Function ttc_gpio_interface_parallel08_5_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel08_5_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_5_put() (using default implementation)!

void ttc_gpio_interface_parallel08_5_put( t_u8 Value ) {


#warning Function ttc_gpio_interface_parallel08_5_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel08_6_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_6_get() (using default implementation)!

t_u8 ttc_gpio_interface_parallel08_6_get() {


#warning Function ttc_gpio_interface_parallel08_6_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel08_6_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_6_put() (using default implementation)!

void ttc_gpio_interface_parallel08_6_put( t_u8 Value ) {


#warning Function ttc_gpio_interface_parallel08_6_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel08_7_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_7_get() (using default implementation)!

t_u8 ttc_gpio_interface_parallel08_7_get() {


#warning Function ttc_gpio_interface_parallel08_7_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel08_7_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_7_put() (using default implementation)!

void ttc_gpio_interface_parallel08_7_put( t_u8 Value ) {


#warning Function ttc_gpio_interface_parallel08_7_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel08_8_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_8_get() (using default implementation)!

t_u8 ttc_gpio_interface_parallel08_8_get() {


#warning Function ttc_gpio_interface_parallel08_8_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel08_8_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_8_put() (using default implementation)!

void ttc_gpio_interface_parallel08_8_put( t_u8 Value ) {


#warning Function ttc_gpio_interface_parallel08_8_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel08_9_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_9_get() (using default implementation)!

t_u8 ttc_gpio_interface_parallel08_9_get() {


#warning Function ttc_gpio_interface_parallel08_9_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel08_9_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel08_9_put() (using default implementation)!

void ttc_gpio_interface_parallel08_9_put( t_u8 Value ) {


#warning Function ttc_gpio_interface_parallel08_9_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel16_10_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_10_get() (using default implementation)!

t_u16 ttc_gpio_interface_parallel16_10_get() {


#warning Function ttc_gpio_interface_parallel16_10_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel16_10_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_10_put() (using default implementation)!

void ttc_gpio_interface_parallel16_10_put( t_u16 Value ) {


#warning Function ttc_gpio_interface_parallel16_10_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel16_1_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_1_get() (using default implementation)!

t_u16 ttc_gpio_interface_parallel16_1_get() {


#warning Function ttc_gpio_interface_parallel16_1_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel16_1_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_1_put() (using default implementation)!

void ttc_gpio_interface_parallel16_1_put( t_u16 Value ) {


#warning Function ttc_gpio_interface_parallel16_1_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel16_2_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_2_get() (using default implementation)!

t_u16 ttc_gpio_interface_parallel16_2_get() {


#warning Function ttc_gpio_interface_parallel16_2_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel16_2_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_2_put() (using default implementation)!

void ttc_gpio_interface_parallel16_2_put( t_u16 Value ) {


#warning Function ttc_gpio_interface_parallel16_2_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel16_3_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_3_get() (using default implementation)!

t_u16 ttc_gpio_interface_parallel16_3_get() {


#warning Function ttc_gpio_interface_parallel16_3_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel16_3_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_3_put() (using default implementation)!

void ttc_gpio_interface_parallel16_3_put( t_u16 Value ) {


#warning Function ttc_gpio_interface_parallel16_3_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel16_4_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_4_get() (using default implementation)!

t_u16 ttc_gpio_interface_parallel16_4_get() {


#warning Function ttc_gpio_interface_parallel16_4_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel16_4_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_4_put() (using default implementation)!

void ttc_gpio_interface_parallel16_4_put( t_u16 Value ) {


#warning Function ttc_gpio_interface_parallel16_4_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel16_5_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_5_get() (using default implementation)!

t_u16 ttc_gpio_interface_parallel16_5_get() {


#warning Function ttc_gpio_interface_parallel16_5_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel16_5_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_5_put() (using default implementation)!

void ttc_gpio_interface_parallel16_5_put( t_u16 Value ) {


#warning Function ttc_gpio_interface_parallel16_5_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel16_6_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_6_get() (using default implementation)!

t_u16 ttc_gpio_interface_parallel16_6_get() {


#warning Function ttc_gpio_interface_parallel16_6_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel16_6_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_6_put() (using default implementation)!

void ttc_gpio_interface_parallel16_6_put( t_u16 Value ) {


#warning Function ttc_gpio_interface_parallel16_6_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel16_7_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_7_get() (using default implementation)!

t_u16 ttc_gpio_interface_parallel16_7_get() {


#warning Function ttc_gpio_interface_parallel16_7_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel16_7_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_7_put() (using default implementation)!

void ttc_gpio_interface_parallel16_7_put( t_u16 Value ) {


#warning Function ttc_gpio_interface_parallel16_7_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel16_8_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_8_get() (using default implementation)!

t_u16 ttc_gpio_interface_parallel16_8_get() {


#warning Function ttc_gpio_interface_parallel16_8_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel16_8_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_8_put() (using default implementation)!

void ttc_gpio_interface_parallel16_8_put( t_u16 Value ) {


#warning Function ttc_gpio_interface_parallel16_8_put() is empty.


}

#endif
#ifndef ttc_driver_gpio_parallel16_9_get  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_9_get() (using default implementation)!

t_u16 ttc_gpio_interface_parallel16_9_get() {


#warning Function ttc_gpio_interface_parallel16_9_get() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_gpio_parallel16_9_put  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gpio_parallel16_9_put() (using default implementation)!

void ttc_gpio_interface_parallel16_9_put( t_u16 Value ) {


#warning Function ttc_gpio_interface_parallel16_9_put() is empty.


}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_gpio_interface_foo(t_ttc_gpio_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
