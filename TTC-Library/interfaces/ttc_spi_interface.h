/** { ttc_spi_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for SPI device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 20 at 20140423 11:04:14 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_SPI_INTERFACE_H
#define TTC_SPI_INTERFACE_H

//{ Includes *************************************************************

#include "../ttc_basic.h"
#include "../ttc_gpio.h"

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_spi_stm32f1xx
    #include "../spi/spi_stm32f1xx.h"
#endif
#ifdef EXTENSION_spi_stm32l1xx
    #include "../spi/spi_stm32l1xx.h"  // low-level driver for spi devices on stm32l1xx architecture
#endif
#ifdef EXTENSION_spi_stm32w1xx
    #include "../spi/spi_stm32w1xx.h"  // low-level driver for spi devices on stm32w1xx architecture
#endif
#ifdef EXTENSION_spi_stm32f30x
    #include "../spi/spi_stm32f30x.h"  // low-level driver for spi devices on stm32f30x architecture
#endif
#ifdef EXTENSION_spi_stm32l0xx
    #include "../spi/spi_stm32l0xx.h"  // low-level driver for spi devices on stm32l0xx architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_spi_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_spi_*() declaration in ../ttc_spi.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_spi_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_spi.h for prototypes of low-level driver functions!
 *
 */

void ttc_spi_interface_prepare(); //{  macro _driver_spi_interface_prepare()
#ifdef ttc_driver_spi_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_spi_prepare() ttc_driver_spi_prepare()
#else
    #define _driver_spi_prepare() ttc_spi_interface_prepare()
#endif
//}
e_ttc_spi_errorcode ttc_spi_interface_init( t_ttc_spi_config* Config ); //{  macro _driver_spi_interface_init(Config)
#ifdef ttc_driver_spi_init
    // enable following line to forward interface function as a macro definition
    #define _driver_spi_init(Config) ttc_driver_spi_init(Config)
#else
    #define _driver_spi_init(Config) ttc_spi_interface_init(Config)
#endif

//}
e_ttc_spi_errorcode ttc_spi_interface_load_defaults( t_ttc_spi_config* Config ); //{  macro _driver_spi_interface_load_defaults(Config)
#ifdef ttc_driver_spi_load_defaults
    // enable following line to forward interface function as a macro definition
    #define _driver_spi_load_defaults(Config) ttc_driver_spi_load_defaults(Config)
#else
    #define _driver_spi_load_defaults(Config) ttc_spi_interface_load_defaults(Config)
#endif
//}
e_ttc_spi_errorcode ttc_spi_interface_get_features( t_ttc_spi_config* Config ); //{  macro _driver_spi_interface_get_features(Config)
#ifdef ttc_driver_spi_get_features
    // enable following line to forward interface function as a macro definition
    #define _driver_spi_get_features(Config) ttc_driver_spi_get_features(Config)
#else
    #define _driver_spi_get_features(Config) ttc_spi_interface_get_features(Config)
#endif
//}
void ttc_spi_interface_deinit( t_ttc_spi_config* Config ); //{  macro _driver_spi_interface_deinit(Config)
#ifdef ttc_driver_spi_deinit
    // enable following line to forward interface function as a macro definition
    #define _driver_spi_deinit(Config) ttc_driver_spi_deinit(Config)
#else
    #define _driver_spi_deinit(Config) ttc_spi_interface_deinit(Config)
#endif
//}
e_ttc_spi_errorcode ttc_spi_interface_reset( t_ttc_spi_config* Config ); //{  macro _driver_spi_interface_reset(Config)
#ifdef ttc_driver_spi_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_spi_reset(Config) ttc_driver_spi_reset(Config)
#else
    #define _driver_spi_reset(Config) ttc_spi_interface_reset(Config)
#endif
//}
//}
void ttc_spi_interface_read_word( t_ttc_spi_config* Config, t_u16* Word ); //{  macro _driver_spi_interface_read_word(Config, Word)
#ifdef ttc_driver_spi_read_word
    // enable following line to forward interface function as a macro definition
    #define _driver_spi_read_word(Config, Word) ttc_driver_spi_read_word(Config, Word)
#else
    #define _driver_spi_read_word(Config, Word) ttc_spi_interface_read_word(Config, Word)
#endif
//}
e_ttc_spi_errorcode ttc_spi_interface_send_word( t_ttc_spi_config* Config, const t_u16 Word ); //{  macro _driver_spi_interface_send_word(Config, Word)
#ifdef ttc_driver_spi_send_word
    // enable following line to forward interface function as a macro definition
    #define _driver_spi_send_word(Config, Word) ttc_driver_spi_send_word(Config, Word)
#else
    #define _driver_spi_send_word(Config, Word) ttc_spi_interface_send_word(Config, Word)
#endif
//}
void ttc_spi_interface_read_byte( t_ttc_spi_config* Config, t_u8* Byte ); //{  macro _driver_spi_interface_read_byte(Config, Byte)
#ifdef ttc_driver_spi_read_byte
    // enable following line to forward interface function as a macro definition
    #define _driver_spi_read_byte(Config, Byte) ttc_driver_spi_read_byte(Config, Byte)
#else
    #define _driver_spi_read_byte(Config, Byte) ttc_spi_interface_read_byte(Config, Byte)
#endif
//}
e_ttc_spi_errorcode ttc_spi_interface_send_raw( t_ttc_spi_config* Config, const t_u8* Buffer, t_u16 Amount ); //{  macro _driver_spi_interface_send_raw(Config, Buffer, Amount)
#ifdef ttc_driver_spi_send_raw
    // enable following line to forward interface function as a macro definition
    #define _driver_spi_send_raw(Config, Buffer, Amount) ttc_driver_spi_send_raw(Config, Buffer, Amount)
#else
    #define _driver_spi_send_raw(Config, Buffer, Amount) ttc_spi_interface_send_raw(Config, Buffer, Amount)
#endif
//}
e_ttc_spi_errorcode ttc_spi_interface_send_read( t_ttc_spi_config* Config, const t_u8* BufferTx, t_u16 AmountTx, t_u16 AmountZerosTx, t_u8* BufferRx, t_u16 AmountRx ); //{  macro _driver_spi_interface_send_read(Config, BufferTx, AmountTx, AmountZerosTx, BufferRx, AmountRx)
#ifdef ttc_driver_spi_send_read
    // enable following line to forward interface function as a macro definition
    #define _driver_spi_send_read(Config, BufferTx, AmountTx, AmountZerosTx, BufferRx, AmountRx) ttc_driver_spi_send_read(Config, BufferTx, AmountTx, AmountZerosTx, BufferRx, AmountRx)
#else
    #define _driver_spi_send_read(Config, BufferTx, AmountTx, AmountZerosTx, BufferRx, AmountRx) ttc_spi_interface_send_read(Config, BufferTx, AmountTx, AmountZerosTx, BufferRx, AmountRx)
#endif
//}
t_u16 ttc_spi_interface_read_raw( t_ttc_spi_config* Config, t_u8* BufferRx, t_u16 AmountRx ); //{  macro _driver_spi_interface_read_raw(Config, BufferRx, AmountRx)
#ifdef ttc_driver_spi_read_raw
    // enable following line to forward interface function as a macro definition
    #define _driver_spi_read_raw(Config, BufferRx, AmountRx) ttc_driver_spi_read_raw(Config, BufferRx, AmountRx)
#else
    #define _driver_spi_read_raw(Config, BufferRx, AmountRx) ttc_spi_interface_read_raw(Config, BufferRx, AmountRx)
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_spi_interface_foo(t_ttc_spi_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_SPI_INTERFACE_H
