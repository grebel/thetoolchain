/** { ttc_math_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for MATH device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 22 at 20150724 12:18:05 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_MATH_INTERFACE_H
#define TTC_MATH_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_math_interface.c"
//

#include "../ttc_basic.h"
#include "compile_options.h"       // only required to help IDE to know constant definitions
#  include "../math/math_common.h" // basic macros and defines used by all math implementations

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_math_software_float
    #include "../math/math_software_float.h"
#endif

#ifdef EXTENSION_math_software_double
    #include "../math/math_software_double.h"
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

// Check if at least one low-level driver could be activated
#ifndef EXTENSION_MATH_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_<DRIVER>! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _math_
#endif

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_math_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_math_*() declaration in ../ttc_math.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_math_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_math.h for prototypes of low-level driver functions!
 *
 */

void ttc_math_interface_prepare(); //{  macro _driver_math_interface_prepare()
#ifdef ttc_driver_math_prepare
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_prepare() ttc_driver_math_prepare()
    #define _driver_math_prepare ttc_driver_math_prepare
#else
    #define _driver_math_prepare() ttc_math_interface_prepare()
#endif
//}
ttm_number ttc_math_interface_cos( ttm_number X ); //{  macro _driver_math_interface_cos(X)
#ifdef ttc_driver_math_cos
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_cos(X) ttc_driver_math_cos(X)
    #define _driver_math_cos ttc_driver_math_cos
#else
    //X //X #  define _driver_math_cos(X) ttc_math_interface_cos(X)
    #define _driver_math_cos ttc_math_interface_cos
#endif
//}
void ttc_math_interface_polar2cartesian( ttm_number Angle, ttm_number Length, ttm_number* X, ttm_number* Y ); //{  macro _driver_math_interface_polar2cartesian(Angle, Length, X, Y)
#ifdef ttc_driver_math_polar2cartesian
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_polar2cartesian(Angle, Length, X, Y) ttc_driver_math_polar2cartesian(Angle, Length, X, Y)
    #define _driver_math_polar2cartesian ttc_driver_math_polar2cartesian
#else
    //X #  define _driver_math_polar2cartesian(Angle, Length, X, Y) ttc_math_interface_polar2cartesian(Angle, Length, X, Y)
    #define _driver_math_polar2cartesian ttc_math_interface_polar2cartesian
#endif
//}
ttm_number ttc_math_interface_sin( ttm_number X ); //{  macro _driver_math_interface_sin(X)

// Different implementations of sinus. Check source code for details.
ttm_number ttc_math_interface_sin_quadratic_curve_low_precision( ttm_number X );
ttm_number ttc_math_interface_sin_quadratic_curve_high_precision( ttm_number X );
ttm_number ttc_math_interface_sin_taylor4( ttm_number X );
ttm_number ttc_math_interface_sin_taylor( ttm_number X, t_u8 Grade );

#ifdef ttc_driver_math_sin
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_sin(X) ttc_driver_math_sin(X)
    #define _driver_math_sin ttc_driver_math_sin
#else

    // choose which implementation to use
    #ifdef MATH_SOFTWARE_DOUBLE_H
        // highest precision of all implementations and faster using double than float!
        //X #  define _driver_math_sin(X) ttc_math_interface_sin_quadratic_curve_high_precision(X) // STM31F107@72MHz - float: 22.03us, RelError<=0.000998037402; double: 21.77us, RelError<=0.00099803087014098055
        #define _driver_math_sin    ttc_math_interface_sin
    #else
        // fast with good precision
        //X #  define _driver_math_sin(X) ttc_math_interface_sin_taylor4(X)                        // STM31F107@72MHz - float: 19.80us, RelError<=0.00559643563;  double: 21.70us, RelError<=0.0055964102552663588
        //#  define _driver_math_sin ttc_math_interface_sin_taylor4
        //#  define _driver_math_sin ttc_math_interface_sin_quadratic_curve_high_precision
        #define _driver_math_sin ttc_math_interface_sin
    #endif

    // all available implementations
    //#  define _driver_math_sin(X) ttc_math_interface_sin_quadratic_curve_low_precision(X)  // STM31F107@72MHz - float: 15.23us, RelError<=0.125213683;    double: 14.03us, RelError<=0.12521364055908088
    //#  define _driver_math_sin(X) ttc_math_interface_sin_quadratic_curve_high_precision(X) // STM31F107@72MHz - float: 22.03us, RelError<=0.000998037402; double: 21.77us, RelError<=0.00099803087014098055
    //#  define _driver_math_sin(X) ttc_math_interface_sin_taylor4(X)                        // STM31F107@72MHz - float: 19.80us, RelError<=0.00559643563;  double: 21.70us, RelError<=0.0055964102552663588
    //#  define _driver_math_sin(X) ttc_math_interface_sin_taylor(X ,3)                      // STM31F107@72MHz - float: 12.80us, RelError<=0.0751677155;   double: 21.50us, RelError<=0.075167770711349613
    //#  define _driver_math_sin(X) ttc_math_interface_sin_taylor(X, 4)                      // STM31F107@72MHz - float: 18.30us, RelError<=0.00452494621;  double: 35.37us, RelError<=0.0045248555348174069
#endif
//}
ttm_number ttc_math_interface_tan( ttm_number X ); //{  macro _driver_math_interface_tan(X)
#ifdef ttc_driver_math_tan
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_tan(X) ttc_driver_math_tan(X)
    #define _driver_math_tan ttc_driver_math_tan
#else
    //X #  define _driver_math_tan(X) ttc_math_interface_tan(X)
    #define _driver_math_tan ttc_math_interface_tan
#endif
//}
void ttc_math_interface_cartesian2polar( ttm_number X, ttm_number Y, ttm_number* Angle, ttm_number* Length ); //{  macro _driver_math_interface_cartesian2polar(X, Y, Angle, Length)
#ifdef ttc_driver_math_cartesian2polar
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_cartesian2polar(X, Y, Angle, Length) ttc_driver_math_cartesian2polar(X, Y, Angle, Length)
    #define _driver_math_cartesian2polar ttc_driver_math_cartesian2polar
#else
    //X #  define _driver_math_cartesian2polar(X, Y, Angle, Length) ttc_math_interface_cartesian2polar(X, Y, Angle, Length)
    #define _driver_math_cartesian2polar ttc_math_interface_cartesian2polar
#endif
//}
ttm_number ttc_math_interface_angle_reduce( ttm_number Angle ); //{  macro _driver_math_interface_reduce_angle(Angle)
#ifdef ttc_driver_math_angle_reduce
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_angle_reduce(Angle) ttc_driver_math_angle_reduce(Angle)
    #define _driver_math_angle_reduce ttc_driver_math_angle_reduce
#else
    //X #  define _driver_math_angle_reduce(Angle) ttc_math_interface_angle_reduce(Angle)
    #define _driver_math_angle_reduce ttc_math_interface_angle_reduce
#endif
//}
ttm_number ttc_math_interface_pow( ttm_number Base, t_u8 Exponent ); //{  macro _driver_math_interface_pow(Base, Exponent)
#ifdef ttc_driver_math_pow
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_pow(Base, Exponent) ttc_driver_math_pow(Base, Exponent)
    #define _driver_math_pow ttc_driver_math_pow
#else
    //X #  define _driver_math_pow(Base, Exponent) ttc_math_interface_pow(Base, Exponent)
    #define _driver_math_pow ttc_math_interface_pow
#endif
//}
ttm_number ttc_math_interface_modulo( ttm_number A, ttm_number B ); //{  macro _driver_math_interface_modulo(A, B)
#ifdef ttc_driver_math_modulo
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_modulo(A, B) ttc_driver_math_modulo(A, B)
    #define _driver_math_modulo ttc_driver_math_modulo
#else
    //X #  define _driver_math_modulo(A, B) ttc_math_interface_modulo(A, B)
    #define _driver_math_modulo ttc_math_interface_modulo
#endif
//}
t_s8 ttc_math_interface_log_int( ttm_number A, t_u8 Base ); //{  macro _driver_math_interface_log_int(A, Base)
#ifdef ttc_driver_math_log_int
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_log_int(A, Base) ttc_driver_math_log_int(A, Base)
    #define _driver_math_log_int ttc_driver_math_log_int
#else
    //X #  define _driver_math_log_int(A, Base) ttc_math_interface_log_int(A, Base)
    #define _driver_math_log_int ttc_math_interface_log_int
#endif
//}
t_u8 ttc_math_interface_int_rankN( t_u32 A, t_u8 Base ); //{  macro _driver_math_interface_int_rankN(A, Base)
#ifdef ttc_driver_math_int_rankN
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_int_rankN(A, Base) ttc_driver_math_int_rankN(A, Base)
    #define _driver_math_int_rankN ttc_driver_math_int_rankN
#else
    //X #  define _driver_math_int_rankN(A, Base) ttc_math_interface_int_rankN(A, Base)
    #define _driver_math_int_rankN ttc_math_interface_int_rankN
#endif
//}
t_u8 ttc_math_interface_int_rank2( t_u32 A ); //{  macro _driver_math_interface_int_rank2(A)
#ifdef ttc_driver_math_int_rank2
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_int_rank2(A) ttc_driver_math_int_rank2(A)
    #define _driver_math_int_rank2 ttc_driver_math_int_rank2
#else
    //X #  define _driver_math_int_rank2(A) ttc_math_interface_int_rank2(A)
    #define _driver_math_int_rank2 ttc_math_interface_int_rank2
#endif
//}
ttm_number ttc_math_interface_arccos( ttm_number X ); //{  macro _driver_math_interface_arccos(X)
#ifdef ttc_driver_math_arccos
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_arccos(X) ttc_driver_math_arccos(X)
    #define _driver_math_arccos ttc_driver_math_arccos
#else
    //X #  define _driver_math_arccos(X) ( (PI / 2) - ttc_math_interface_arcsin(X) )
    #define _driver_math_arccos ttc_math_interface_arccos
#endif
//}
ttm_number ttc_math_interface_arcsin( ttm_number X ); //{  macro _driver_math_interface_arcsin(X)
#ifdef ttc_driver_math_arcsin
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_arcsin(X) ttc_driver_math_arcsin(X)
    #define _driver_math_arcsin ttc_driver_math_arcsin
#else
    //X #  define _driver_math_arcsin(X) ttc_math_interface_arcsin(X)
    #define _driver_math_arcsin ttc_math_interface_arcsin
#endif
//}
ttm_number ttc_math_interface_arctan( ttm_number X ); //{  macro _driver_math_interface_arctan(X)
#ifdef ttc_driver_math_arctan
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_arctan(X) ttc_driver_math_arctan(X)
    #define _driver_math_arctan ttc_driver_math_arctan
#else
    //X #  define _driver_math_arctan(X) ttc_math_interface_arctan(X)
    #define _driver_math_arctan ttc_math_interface_arctan
#endif
//}
ttm_number ttc_math_interface_inv_sqrt( ttm_number X ); //{  macro _driver_math_interface_inv_sqrt(X)
#ifdef ttc_driver_math_inv_sqrt
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_inv_sqrt(X) ttc_driver_math_inv_sqrt(X)
    #define _driver_math_inv_sqrt ttc_driver_math_inv_sqrt
#else
    //X #  define _driver_math_inv_sqrt(X) (1 / ttc_driver_math_sqrt( X ))
    #define _driver_math_inv_sqrt  ttc_math_interface_inv_sqrt
#endif
//}
ttm_number ttc_math_interface_sqrt( ttm_number X ); //{  macro _driver_math_interface_sqrt(X)
#ifdef ttc_driver_math_sqrt
    // enable following line to forward interface function as a macro definition
    //X #  define _driver_math_sqrt(X) ttc_driver_math_sqrt(X)
    #define _driver_math_sqrt ttc_driver_math_sqrt
#else
    //X #  define _driver_math_sqrt(X) ttc_math_interface_sqrt(X)
    #define _driver_math_sqrt ttc_math_interface_sqrt
#endif
//}
ttm_number ttc_math_interface_vector2d_angle( ttm_number DX, ttm_number DY ); //{  macro _driver_math_interface_vector2d_angle(DX, DY)
#ifdef ttc_driver_math_vector2d_angle
    // enable following line to forward interface function as a macro definition
    #define _driver_math_vector2d_angle ttc_driver_math_vector2d_angle
#else
    #define _driver_math_vector2d_angle ttc_math_interface_vector2d_angle
#endif
//}
ttm_number ttc_math_interface_length_3d( ttm_number DistanceX, ttm_number DistanceY, ttm_number DistanceZ ); //{  macro _driver_math_interface_length_3d(X, Y, Z)
#ifdef ttc_driver_math_length_3d
    // enable following line to forward interface function as a macro definition
    #define _driver_math_length_3d ttc_driver_math_length_3d
#else
    #define _driver_math_length_3d ttc_math_interface_length_3d
#endif
//}
ttm_number ttc_math_interface_from_int( t_base_signed Value ); //{  macro _driver_math_interface_from_int(Value)
#ifdef ttc_driver_math_from_int
    // enable following line to forward interface function as a macro definition
    #define _driver_math_from_int(Value) ttc_driver_math_from_int(Value)
#else
    #define _driver_math_from_int(Value) (Value)
#endif
//}
ttm_number ttc_math_interface_from_double( double Value ); //{  macro _driver_math_interface_from_double(Value)
#ifdef ttc_driver_math_from_double
    // enable following line to forward interface function as a macro definition
    #define _driver_math_from_double(Value) ttc_driver_math_from_double(Value)
#else
    #define _driver_math_from_double(Value) ( (double) (Value) )
#endif
//}
ttm_number ttc_math_interface_from_float( float Value ); //{  macro _driver_math_interface_from_float(Value)
#ifdef ttc_driver_math_from_float
    // enable following line to forward interface function as a macro definition
    #define _driver_math_from_float(Value) ttc_driver_math_from_float(Value)
#else
    #define _driver_math_from_float(Value) ( (float) (Value) )
#endif
//}
t_base_signed ttc_math_interface_to_int( ttm_number Value ); //{  macro _driver_math_interface_to_int(Value)
#ifdef ttc_driver_math_to_int
    // enable following line to forward interface function as a macro definition
    #define _driver_math_to_int(Value) ttc_driver_math_to_int(Value)
#else
    #define _driver_math_to_int(Value) (Value)
#endif
//}
double ttc_math_interface_to_double( ttm_number Value ); //{  macro _driver_math_interface_to_double(Value)
#ifdef ttc_driver_math_to_double
    // enable following line to forward interface function as a macro definition
    #define _driver_math_to_double(Value) ttc_driver_math_to_double(Value)
#else
    #define _driver_math_to_double(Value) (Value)
#endif
//}
float ttc_math_interface_to_float( ttm_number Value ); //{  macro _driver_math_interface_to_float(Value)
#ifdef ttc_driver_math_to_float
    // enable following line to forward interface function as a macro definition
    #define _driver_math_to_float(Value) ttc_driver_math_to_float(Value)
#else
    #define _driver_math_to_float(Value) (Value)
#endif
//}
ttm_number ttc_math_interface_distance_2d( ttm_number Ax, ttm_number Ay, ttm_number Bx, ttm_number By ); //{  macro _driver_math_interface_distance_2d(Ax, Ay, Bx, By)
#ifdef ttc_driver_math_distance_2d
    // enable following line to forward interface function as a macro definition
    #define _driver_math_distance_2d(Ax, Ay, Bx, By) ttc_driver_math_distance_2d(Ax, Ay, Bx, By)
#else
    #define _driver_math_distance_2d(Ax, Ay, Bx, By) ttc_math_interface_distance_2d(Ax, Ay, Bx, By)
#endif
//}
ttm_number ttc_math_interface_vector3d_distance( t_ttc_math_vector3d_xyz* A, t_ttc_math_vector3d_xyz* B ); //{  macro _driver_math_interface_vector3d_distance(A, B)
#ifdef ttc_driver_math_vector3d_distance
    // enable following line to forward interface function as a macro definition
    #define _driver_math_vector3d_distance(A, B) ttc_driver_math_vector3d_distance(A, B)
#else
    #define _driver_math_vector3d_distance(A, B) ttc_math_interface_vector3d_distance(A, B)
#endif
//}
#define _driver_math_distance( A, B ) ((A) > (B)) ? ((A) - (B)) : ((B) - (A))
ttm_number ttc_math_interface_length_2d( ttm_number DistanceX, ttm_number DistanceY ); //{  macro _driver_math_interface_length_2d(DistanceX, DistanceY)
#ifdef ttc_driver_math_length_2d
    // enable following line to forward interface function as a macro definition
    #define _driver_math_length_2d(DistanceX, DistanceY) ttc_driver_math_length_2d(DistanceX, DistanceY)
#else
    #define _driver_math_length_2d(DistanceX, DistanceY) ttc_math_interface_length_2d(DistanceX, DistanceY)
#endif
//}
ttm_number ttc_math_interface_abs( ttm_number X ); //{  macro _driver_math_interface_abs(X)
#ifdef ttc_driver_math_abs
    // enable following line to forward interface function as a macro definition
    #define _driver_math_abs(X) ttc_driver_math_abs(X)
#else
    #define _driver_math_abs(X) ttc_math_interface_abs(X)
#endif
//}
void ttc_math_interface_intersection_2d( t_ttc_math_vector2d_xy* Line1_Point, t_ttc_math_vector2d_xy* Line1_Incline, t_ttc_math_vector2d_xy* Line2_Point, t_ttc_math_vector2d_xy* Line2_Incline, t_ttc_math_vector2d_xy* Intersection ); //{  macro _driver_math_interface_intersection_2d(Line1_Point, Line1_Incline, Line2_Point, Line2_Incline, Intersection)
#ifdef ttc_driver_math_intersection_2d
    // enable following line to forward interface function as a macro definition
    #define _driver_math_intersection_2d ttc_driver_math_intersection_2d
#else
    #define _driver_math_intersection_2d ttc_math_interface_intersection_2d
#endif
//}
t_ttc_math_vector2d_xy  ttc_math_interface_vector2d_incline( t_ttc_math_vector2d_xy* A, t_ttc_math_vector2d_xy* B ); //{  macro _driver_math_interface_vector2d_incline(A, B)
#ifdef ttc_driver_math_vector2d_incline
    // enable following line to forward interface function as a macro definition
    #define _driver_math_vector2d_incline ttc_driver_math_vector2d_incline
#else
    #define _driver_math_vector2d_incline ttc_math_interface_vector2d_incline
#endif
//}
BOOL ttc_math_interface_vector3d_valid( const t_ttc_math_vector3d_xyz* Vector ); //{  macro _driver_math_interface_vector3d_valid(Vector)
#ifdef ttc_driver_math_vector3d_valid
    // enable following line to forward interface function as a macro definition
    #define _driver_math_vector3d_valid ttc_driver_math_vector3d_valid
#else
    #define _driver_math_vector3d_valid ttc_math_interface_vector3d_valid
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_math_interface_foo(t_ttc_math_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_MATH_INTERFACE_H
