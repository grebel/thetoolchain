/** { ttc_network_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *   Interface layer between high- and low-level driver for NETWORK device.
 *
 *   The functions implemented here are used when additional runtime
 *   computations arerequired to call the corresponding low-level driver.
 *   These computations should be held very simple and fast. Every additional
 *
 *   Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_network_*() functions.
 *  Instead they can call _driver_network_*() pendants.
 *  E.g.: ttc_network_reset() -> _driver_network_reset()
 *
 *   Authors:
 *
}*/
//{ Defines/ TypDefs *****************************************************
//} Defines

#include "ttc_network_interface.h"

//{ Global Variables *****************************************************

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_network_interface_prepare
void ttc_network_interface_prepare() {
    #ifdef EXTENSION_ttc_network_6lowpan
    network_6lowpan_prepare();
    #else
    network_mac_prepare();
    #endif
}
#endif
#ifndef ttc_network_interface_load_defaults
e_ttc_network_errorcode ttc_network_interface_load_defaults( t_ttc_network_config* Config ) {
    Assert_NETWORK( Config, ttc_assert_origin_auto );

    return Driver_network_load_defaults( Config );
}
#endif
#ifndef ttc_network_interface_deinit
e_ttc_network_errorcode ttc_network_interface_deinit( t_ttc_network_config* Config ) {
    Assert_NETWORK( Config, ttc_assert_origin_auto );

    Driver_network_deinit( Config );
    return ec_network_OK;
}
#endif
#ifndef ttc_network_interface_init
e_ttc_network_errorcode ttc_network_interface_init( t_ttc_network_config* Config ) {
    Assert_NETWORK( Config, ttc_assert_origin_auto );

    Driver_network_init( Config );
    return ec_network_OK;
}
#endif
#ifndef ttc_network_interface_reset
e_ttc_network_errorcode ttc_network_interface_reset( t_ttc_network_config* Config ) {
    Assert_NETWORK( Config, ttc_assert_origin_auto );

    Driver_network_reset( Config );
    return ec_network_OK;
}
#endif

// ToDo: additional functions go here...
#ifndef ttc_network_interface_send_direct_message
e_ttc_network_errorcode ttc_network_interface_send_direct_message( t_ttc_network_config* Config, ttc_network_address_t Target, t_u8* Buffer, t_base Amount ) {
    Assert_NETWORK( Config, ttc_assert_origin_auto );

    return Driver_network_send_unicast( Config, Target, Buffer, Amount );
}
#endif
#ifndef ttc_network_interface_send_broadcast_message
e_ttc_network_errorcode ttc_network_interface_send_broadcast_message( t_ttc_network_config* Config, t_u8* Buffer, t_base Amount ) {
    Assert_NETWORK( Config, ttc_assert_origin_auto );

    return Driver_network_send_broadcast( Config, Buffer, Amount );
}
#endif
#ifndef ttc_network_interface_register_receive
void ttc_network_interface_register_receive( t_ttc_network_config* Config ) {
    Assert_NETWORK( Config, ttc_assert_origin_auto );

    Driver_network_receive( Config );
}
#endif
#ifndef ttc_network_interface_get_neighbours
t_ttc_list* ttc_network_interface_get_neighbours( t_ttc_network_config* Config ) {
    Assert_NETWORK( Config, ttc_assert_origin_auto );

    return Driver_network_get_neighbours( Config );
}
#endif

//}FunctionDefinitions
