/** { ttc_slam_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for SLAM device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 23 at 20160606 11:59:52 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_SLAM_INTERFACE_H
#define TTC_SLAM_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_slam_interface.c"
//

#include "../ttc_basic.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_slam_simple_2d
    #include "../slam/slam_simple_2d.h"  // low-level driver for slam devices on simple_2d architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

// Check if at least one low-level driver could be activated
#ifndef EXTENSION_SLAM_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_SLAM! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _slam_
#endif

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_slam_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_slam_*() declaration in ../ttc_slam.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_slam_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_slam.h for prototypes of low-level driver functions!
 *
 */

e_ttc_slam_errorcode ttc_slam_interface_deinit( t_ttc_slam_config* Config ); //{  macro _driver_slam_interface_deinit(Config)
#ifdef ttc_driver_slam_deinit
    // enable following line to forward interface function as a macro definition
    #define _driver_slam_deinit(Config) ttc_driver_slam_deinit(Config)
#else
    #define _driver_slam_deinit(Config) ttc_slam_interface_deinit(Config)
#endif
//}
void ttc_slam_interface_reset( t_ttc_slam_config* Config ); //{  macro _driver_slam_interface_reset(Config)
#ifdef ttc_driver_slam_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_slam_reset(Config) ttc_driver_slam_reset(Config)
#else
    #define _driver_slam_reset(Config) ttc_slam_interface_reset(Config)
#endif
//}
void ttc_slam_interface_prepare(); //{  macro _driver_slam_interface_prepare()
#ifdef ttc_driver_slam_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_slam_prepare() ttc_driver_slam_prepare()
#else
    #define _driver_slam_prepare() ttc_slam_interface_prepare()
#endif
//}
void ttc_slam_interface_configuration_check( t_ttc_slam_config* Config ); //{  macro _driver_slam_interface_configuration_check(Config)
#ifdef ttc_driver_slam_configuration_check
    // enable following line to forward interface function as a macro definition
    #define _driver_slam_configuration_check(Config) ttc_driver_slam_configuration_check(Config)
#else
    #define _driver_slam_configuration_check(Config) ttc_slam_interface_configuration_check(Config)
#endif
//}
e_ttc_slam_errorcode ttc_slam_interface_load_defaults( t_ttc_slam_config* Config ); //{  macro _driver_slam_interface_load_defaults(Config)
#ifdef ttc_driver_slam_load_defaults
    // enable following line to forward interface function as a macro definition
    #define _driver_slam_load_defaults(Config) ttc_driver_slam_load_defaults(Config)
#else
    #define _driver_slam_load_defaults(Config) ttc_slam_interface_load_defaults(Config)
#endif
//}
e_ttc_slam_errorcode ttc_slam_interface_init( t_ttc_slam_config* Config ); //{  macro _driver_slam_interface_init(Config)
#ifdef ttc_driver_slam_init
    // enable following line to forward interface function as a macro definition
    #define _driver_slam_init(Config) ttc_driver_slam_init(Config)
#else
    #define _driver_slam_init(Config) ttc_slam_interface_init(Config)
#endif
//}
void ttc_slam_interface_update_distance( t_ttc_slam_config* Config, t_u16 NodeIndexA, t_u16 NodeIndexB, ttm_number RawValue ); //{  macro _driver_slam_interface_update_distance(Config, NodeIndexA, NodeIndexB, RawValue)
#ifdef ttc_driver_slam_update_distance
    // enable following line to forward interface function as a macro definition
    #define _driver_slam_update_distance(Config, NodeIndexA, NodeIndexB, RawValue) ttc_driver_slam_update_distance(Config, NodeIndexA, NodeIndexB, RawValue)
#else
    #define _driver_slam_update_distance(Config, NodeIndexA, NodeIndexB, RawValue) ttc_slam_interface_update_distance(Config, NodeIndexA, NodeIndexB, RawValue)
#endif
//}
t_ttc_slam_node* ttc_slam_interface_get_node( t_ttc_slam_config* Config, t_u16 NodeIndex ); //{  macro _driver_slam_interface_get_node(Config, NodeIndex)
#ifdef ttc_driver_slam_get_node
    // enable following line to forward interface function as a macro definition
    #define _driver_slam_get_node(Config, NodeIndex) ttc_driver_slam_get_node(Config, NodeIndex)
#else
    #define _driver_slam_get_node(Config, NodeIndex) ttc_slam_interface_get_node(Config, NodeIndex)
#endif
//}
void ttc_slam_interface_update_coordinates( t_ttc_slam_config* Config, t_u16 NodeIndex, ttm_number X, ttm_number Y, ttm_number Z ); //{  macro _driver_slam_interface_update_coordinates(LogicalIndex, NodeIndex, X, Y, Z)
#ifdef ttc_driver_slam_update_coordinates
    // enable following line to forward interface function as a macro definition
    #define _driver_slam_update_coordinates(Config, NodeIndex, X, Y, Z) ttc_driver_slam_update_coordinates(Config, NodeIndex, X, Y, Z)
#else
    #define _driver_slam_update_coordinates(Config, NodeIndex, X, Y, Z) ttc_slam_interface_update_coordinates(Config, NodeIndex, X, Y, Z)
#endif
//}
BOOL ttc_slam_interface_calculate_mapping( t_ttc_slam_config* Config ); //{  macro _driver_slam_interface_calculate_mapping(Config)
#ifdef ttc_driver_slam_calculate_mapping
    // enable following line to forward interface function as a macro definition
    #define _driver_slam_calculate_mapping(Config) ttc_driver_slam_calculate_mapping(Config)
#else
    #define _driver_slam_calculate_mapping(Config) ttc_slam_interface_calculate_mapping(Config)
#endif
//}
ttm_number ttc_slam_interface_calculate_distance( t_ttc_slam_config* Config, t_u16 NodeIndexA, t_u16 NodeIndexB ); //{  macro _driver_slam_interface_calculate_distance(Config, NodeIndexA, NodeIndexB)
#ifdef ttc_driver_slam_calculate_distance
    // enable following line to forward interface function as a macro definition
    #define _driver_slam_calculate_distance(Config, NodeIndexA, NodeIndexB) ttc_driver_slam_calculate_distance(Config, NodeIndexA, NodeIndexB)
#else
    #define _driver_slam_calculate_distance(Config, NodeIndexA, NodeIndexB) ttc_slam_interface_calculate_distance(Config, NodeIndexA, NodeIndexB)
#endif
//}
ttm_number ttc_slam_interface_get_distance( t_ttc_slam_config* Config, t_u16 NodeIndexA, t_u16 NodeIndexB ); //{  macro _driver_slam_interface_get_distance(Config, NodeIndex)
#ifdef ttc_driver_slam_get_distance
    // enable following line to forward interface function as a macro definition
    #define _driver_slam_get_distance ttc_driver_slam_get_distance
#else
    #define _driver_slam_get_distance ttc_slam_interface_get_distance
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_slam_interface_foo(t_ttc_slam_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_SLAM_INTERFACE_H
