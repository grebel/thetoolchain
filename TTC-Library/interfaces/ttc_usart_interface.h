/** { ttc_usart_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for USART device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 20 at 20140217 09:36:59 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_USART_INTERFACE_H
#define TTC_USART_INTERFACE_H

//{ Includes *************************************************************

#include "../ttc_basic.h"

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_usart_stm32f1xx
    #include "../usart/usart_stm32f1xx.h"
#endif
#ifdef EXTENSION_usart_stm32w1xx
    #include "../usart/usart_stm32w1xx.h"  // low-level driver for usart devices on stm32w1xx architecture
#endif
#ifdef EXTENSION_usart_stm32l1xx
    #include "../usart/usart_stm32l1xx.h"
#endif
#ifdef EXTENSION_usart_stm32l0xx
    #include "../usart/usart_stm32l0xx.h"  // low-level driver for usart devices on stm32l0xx architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_usart_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_usart_*() declaration in ../ttc_usart.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_usart_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_usart.h for prototypes of low-level driver functions!
 *
 */

e_ttc_usart_errorcode ttc_usart_interface_load_defaults( t_ttc_usart_config* Config ); //{  macro _driver_usart_interface_load_defaults(Config)
#ifdef ttc_driver_usart_load_defaults
    // enable following line to forward interface function as a macro definition
    #define _driver_usart_load_defaults(Config) ttc_driver_usart_load_defaults(Config)
#else
    #define _driver_usart_load_defaults(Config) ttc_usart_interface_load_defaults(Config)
    #  warning: missing low-level implementation for ttc_driver_usart_load_defaults()!
#endif
//}
void ttc_usart_interface_reset( t_ttc_usart_config* Config ); //{  macro _driver_usart_interface_reset(Config)
#ifdef ttc_driver_usart_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_usart_reset(Config) ttc_driver_usart_reset(Config)
#else
    #define _driver_usart_reset(Config) ttc_usart_interface_reset(Config)
    #  warning: missing low-level implementation for ttc_driver_usart_reset()!
#endif
//}
e_ttc_usart_errorcode ttc_usart_interface_deinit( t_ttc_usart_config* Config ); //{  macro _driver_usart_interface_deinit(Config)
#ifdef ttc_driver_usart_deinit
    // enable following line to forward interface function as a macro definition
    #define _driver_usart_deinit(Config) ttc_driver_usart_deinit(Config)
#else
    #define _driver_usart_deinit(Config) ttc_usart_interface_deinit(Config)
    #  warning: missing low-level implementation for ttc_driver_usart_deinit()!
#endif
//}
void ttc_usart_interface_prepare(); //{  macro _driver_usart_interface_prepare()
#ifdef ttc_driver_usart_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_usart_prepare() ttc_driver_usart_prepare()
#else
    #define _driver_usart_prepare() ttc_usart_interface_prepare()
    #  warning: missing low-level implementation for ttc_driver_usart_prepare()!
#endif
//}
e_ttc_usart_errorcode ttc_usart_interface_get_features( t_ttc_usart_config* Config ); //{  macro _driver_usart_interface_get_features(Config)
#ifdef ttc_driver_usart_get_features
    // enable following line to forward interface function as a macro definition
    #define _driver_usart_get_features(Config) ttc_driver_usart_get_features(Config)
#else
    #define _driver_usart_get_features(Config) ttc_usart_interface_get_features(Config)
    #  warning: missing low-level implementation for ttc_driver_usart_get_features()!
#endif
//}
e_ttc_usart_errorcode ttc_usart_interface_init( t_ttc_usart_config* Config ); //{  macro _driver_usart_interface_init(Config)
#ifdef ttc_driver_usart_init
    // enable following line to forward interface function as a macro definition
    #define _driver_usart_init(Config) ttc_driver_usart_init(Config)
#else
    #define _driver_usart_init(Config) ttc_usart_interface_init(Config)
    #  warning: missing low-level implementation for ttc_driver_usart_init()!
#endif
//}
t_ttc_usart_config* ttc_usart_interface_get_configuration( t_ttc_usart_config* Config ); //{  macro _driver_usart_interface_get_configuration(Config)
#ifdef ttc_driver_usart_get_configuration
    // enable following line to forward interface function as a macro definition
    #define _driver_usart_get_configuration(Config) ttc_driver_usart_get_configuration(Config)
#else
    #define _driver_usart_get_configuration(Config) ttc_usart_interface_get_configuration(Config)
#endif
//}
e_ttc_usart_errorcode ttc_usart_interface_read_word_blocking( volatile t_ttc_usart_register_rx* Register, t_u16* Word ); //{  macro _driver_usart_interface_read_word_blocking(Register, Word)
#ifdef ttc_driver_usart_read_word_blocking
    // enable following line to forward interface function as a macro definition
    #define _driver_usart_read_word_blocking(Register, Word) ttc_driver_usart_read_word_blocking(Register, Word)
#else
    #define _driver_usart_read_word_blocking(Register, Word) ttc_usart_interface_read_word_blocking(Register, Word)
#endif
//}
e_ttc_usart_errorcode ttc_usart_interface_send_word_blocking( t_ttc_usart_register_tx* Register, const t_u16 Word ); //{  macro _driver_usart_interface_send_word_blocking(Register, Word)
#ifdef ttc_driver_usart_send_word_blocking
    // enable following line to forward interface function as a macro definition
    #define _driver_usart_send_word_blocking(Register, Word) ttc_driver_usart_send_word_blocking(Register, Word)
#else
    #define _driver_usart_send_word_blocking(Register, Word) ttc_usart_interface_send_word_blocking(Register, Word)
#endif
//}
void ttc_usart_interface_send_byte_isr( volatile t_ttc_usart_register* Register, const t_u16 Word ); //{  macro _driver_usart_interface_send_byte_isr(Register, Word)
#ifdef ttc_driver_usart_send_byte_isr
    // enable following line to forward interface function as a macro definition
    #define _driver_usart_send_byte_isr(Register, Word) ttc_driver_usart_send_byte_isr(Register, Word)
#else
    #define _driver_usart_send_byte_isr(Register, Word) ttc_usart_interface_send_byte_isr(Register, Word)
#endif
//}
e_ttc_usart_errorcode ttc_usart_interface_read_byte( volatile t_ttc_usart_register_rx* Register, t_u8* Byte ); //{  macro _driver_usart_interface_read_byte(Register, Byte)
#ifdef ttc_driver_usart_read_byte
    // enable following line to forward interface function as a macro definition
    #define _driver_usart_read_byte(Register, Byte) ttc_driver_usart_read_byte(Register, Byte)
#else
    #define _driver_usart_read_byte(Register, Byte) ttc_usart_interface_read_byte(Register, Byte)
#endif
//}
t_u8 ttc_usart_interface_check_bytes_available( t_ttc_usart_config* Config ); //{  macro _driver_usart_interface_check_bytes_available(Config)
#ifdef ttc_driver_usart_check_bytes_available
    // enable following line to forward interface function as a macro definition
    #define _driver_usart_check_bytes_available(Config) ttc_driver_usart_check_bytes_available(Config)
#else
    #define _driver_usart_check_bytes_available(Config) ttc_usart_interface_check_bytes_available(Config)
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_usart_interface_foo(t_ttc_usart_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_USART_INTERFACE_H
