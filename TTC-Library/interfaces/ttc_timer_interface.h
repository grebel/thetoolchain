/** { ttc_timer_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for TIMER device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *  
 *  Created from template ttc_device_interface.h revision 20 at 20140204 13:03:59 UTC
 *
 *  Authors: Francisco Estevez 2014
 *
}*/

#ifndef TIMER_INTERFACE_H
#define TIMER_INTERFACE_H

//{ Includes *************************************************************

#include "../ttc_basic.h"

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_timer_stm32f1xx
#  include "../timer/timer_stm32f1xx.h"
#endif
#ifdef EXTENSION_timer_stm32w1xx
#  include "../timer/timer_stm32w1xx.h"  // low-level driver for timer devices on stm32w1xx architecture
#endif
#ifdef EXTENSION_timer_stm32l1xx
#  include "../timer/timer_stm32l1xx.h"  // low-level driver for timer devices on stm32l1xx architecture
#endif
#ifdef EXTENSION_timer_stm32f0xx
#  include "../timer/timer_stm32f0xx.h"  // low-level driver for timer devices on stm32f0xx architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_timer_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_timer_*() declaration in ../ttc_timer.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_timer_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_timer.h for prototypes of low-level driver functions!
 *
 */

void _driver_timer_reset( t_ttc_timer_config* Config ); //{  macro _driver_timer_interface_reset(Config)
#ifdef ttc_driver_timer_reset
// enable following line to forward interface function as a macro definition
#  define _driver_timer_reset(Config) ttc_driver_timer_reset(Config)
#else
#  define _driver_timer_reset(Config) _driver_timer_reset(Config)
#  warning: missing low-level implementation for ttc_driver_timer_reset()!
#endif
//}
t_u32 _driver_timer_read_value( t_ttc_timer_config* Config ); //{  macro _driver_timer_interface_read_value(Config)
#ifdef ttc_driver_timer_read_value
// enable following line to forward interface function as a macro definition
#  define _driver_timer_read_value(Config) ttc_driver_timer_read_value(Config)
#else
#  define _driver_timer_read_value(Config) _driver_timer_read_value(Config)
#  warning: missing low-level implementation for ttc_driver_timer_read_value()!
#endif
//}
e_ttc_timer_errorcode _driver_timer_set( t_ttc_timer_config* Config ); //{  macro _driver_timer_interface_set(Config, t_u32, Argument, Period)
#ifdef ttc_driver_timer_set
// enable following line to forward interface function as a macro definition
#  define _driver_timer_set(Config) ttc_driver_timer_set(Config)
#else
#  define _driver_timer_set(Config) _driver_timer_set(Config)
#  warning: missing low-level implementation for ttc_driver_timer_set()!
#endif
//}
void _driver_timer_prepare(); //{  macro _driver_timer_interface_prepare()
#ifdef ttc_driver_timer_prepare
// enable following line to forward interface function as a macro definition
#  define _driver_timer_prepare() ttc_driver_timer_prepare()
#else
#  define _driver_timer_prepare() _driver_timer_prepare()
#  warning: missing low-level implementation for ttc_driver_timer_prepare()!
#endif
//}
e_ttc_timer_errorcode _driver_timer_deinit( t_ttc_timer_config* Config ); //{  macro _driver_timer_interface_deinit(Config)
#ifdef ttc_driver_timer_deinit
// enable following line to forward interface function as a macro definition
#  define _driver_timer_deinit(Config) ttc_driver_timer_deinit(Config)
#else
#  define _driver_timer_deinit(Config) _driver_timer_deinit(Config)
#  warning: missing low-level implementation for ttc_driver_timer_deinit()!
#endif
//}
e_ttc_timer_errorcode _driver_timer_init( t_ttc_timer_config* Config ); //{  macro _driver_timer_interface_init(Config)
#ifdef ttc_driver_timer_init
// enable following line to forward interface function as a macro definition
#  define _driver_timer_init(Config) ttc_driver_timer_init(Config)
#else
#  define _driver_timer_init(Config) _driver_timer_init(Config)
#  warning: missing low-level implementation for ttc_driver_timer_init()!
#endif
//}
e_ttc_timer_errorcode _driver_timer_get_features( t_ttc_timer_config* Config ); //{  macro _driver_timer_interface_get_features(Config)
#ifdef ttc_driver_timer_get_features
// enable following line to forward interface function as a macro definition
#  define _driver_timer_get_features(Config) ttc_driver_timer_get_features(Config)
#else
#  define _driver_timer_get_features(Config) _driver_timer_get_features(Config)
#  warning: missing low-level implementation for ttc_driver_timer_get_features()!
#endif
//}
void _driver_timer_change_period( t_ttc_timer_config* Config, t_u32 Period ); //{  macro _driver_timer_interface_change_period(Config, Period)
#ifdef ttc_driver_timer_change_period
// enable following line to forward interface function as a macro definition
#  define _driver_timer_change_period(Config, Period) ttc_driver_timer_change_period(Config, Period)
#else
#  define _driver_timer_change_period(Config, Period) _driver_timer_change_period(Config, Period)
#  warning: missing low-level implementation for ttc_driver_timer_change_period()!
#endif
//}
void _driver_timer_reload( t_ttc_timer_config* Config ); //{  macro _driver_timer_interface_change_period(Config, Period)
#ifdef ttc_driver_timer_change_period
// enable following line to forward interface function as a macro definition
#  define _driver_timer_reload(Config) ttc_driver_timer_reload(Config)
#else
#  define _driver_timer_reload(Config) _driver_timer_reload(Config)
#  warning: missing low-level implementation for ttc_driver_timer_change_period()!
#endif
//}
e_ttc_timer_errorcode _driver_timer_load_defaults( t_ttc_timer_config* Config ); //{  macro _driver_timer_interface_load_defaults(Config)
#ifdef ttc_driver_timer_load_defaults
// enable following line to forward interface function as a macro definition
#  define _driver_timer_load_defaults(Config) ttc_driver_timer_load_defaults(Config)
#else
#  define _driver_timer_load_defaults(Config) _driver_timer_load_defaults(Config)
#  warning: missing low-level implementation for ttc_driver_timer_load_defaults()!
#endif
//}

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void __driver_timerfoo(t_ttc_timer_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TIMER_INTERFACE_H
