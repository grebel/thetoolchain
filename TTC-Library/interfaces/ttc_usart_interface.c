/** { ttc_usart_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for USART device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_usart_*() functions.
 *  Instead they can call _driver_usart_*() pendants.
 *  E.g.: ttc_usart_reset() -> _driver_usart_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20140217 09:36:59 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_usart_interface.h"

/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_usart_load_defaults  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

e_ttc_usart_errorcode ttc_usart_interface_load_defaults( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_usart_errorcode ) 0;
}

#endif
#ifndef ttc_driver_usart_reset  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void ttc_usart_interface_reset( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif
#ifndef ttc_driver_usart_deinit  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

e_ttc_usart_errorcode ttc_usart_interface_deinit( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_usart_errorcode ) 0;
}

#endif
#ifndef ttc_driver_usart_prepare  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

void ttc_usart_interface_prepare() {




}

#endif
#ifndef ttc_driver_usart_get_features  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

e_ttc_usart_errorcode ttc_usart_interface_get_features( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_usart_errorcode ) 0;
}

#endif
#ifndef ttc_driver_usart_init  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

e_ttc_usart_errorcode ttc_usart_interface_init( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_usart_errorcode ) 0;
}

#endif
#ifndef ttc_driver_usart_read_word_blocking  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

e_ttc_usart_errorcode ttc_usart_interface_read_word_blocking( t_ttc_usart_config* Config, t_u16* Word ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_USART_Writable( Word, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_usart_errorcode ) 0;
}

#endif
#ifndef ttc_driver_usart_get_configuration  // no low-level implementation: use default implementation
// using default implementation (low-level driver does not provide this implementation)

t_ttc_usart_config* ttc_usart_interface_get_configuration( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( t_ttc_usart_config* ) 0;
}

#endif
#ifndef ttc_driver_usart_send_word_blocking  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_usart_send_word_blocking() (using default implementation)!

e_ttc_usart_errorcode ttc_usart_interface_send_word_blocking( volatile t_ttc_usart_register* Register, const t_u16 Word ) {
    Assert_USART_Writable( Register, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_usart_errorcode ) 0;
}

#endif
#ifndef ttc_driver_usart_send_byte_isr  // no low-level implementation: use default implementation

void ttc_usart_interface_send_byte_isr( volatile t_ttc_usart_register* Register, const t_u16 Word ) {
    Assert_USART_Writable( Register, ttc_assert_origin_auto );  // pointers must not be NULL

#  warning: missing low-level implementation for ttc_usart_interface_send_byte_isr() (using default implementation)!

}

#endif
#ifndef ttc_driver_usart_read_byte  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_usart_read_byte() (using default implementation)!

e_ttc_usart_errorcode ttc_usart_interface_read_byte( t_ttc_usart_register_rx* Register, t_u8* Byte ) {
    Assert_USART_Writable( Register, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_USART_Writable( Byte, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_usart_errorcode ) 0;
}

#endif
#ifndef ttc_driver_usart_check_bytes_available  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_usart_check_bytes_available() (using default implementation)!

t_u8 ttc_usart_interface_check_bytes_available( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

#warning Function ttc_usart_interface_check_bytes_available() is empty.

    return ( t_u8 ) 0;
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_usart_interface_foo(t_ttc_usart_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions