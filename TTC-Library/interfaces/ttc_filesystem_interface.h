/** { ttc_filesystem_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for FILESYSTEM device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 24 at 20180413 11:21:12 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_FILESYSTEM_INTERFACE_H
#define TTC_FILESYSTEM_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_filesystem_interface.c"
//

#include "../ttc_basic.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_filesystem_dosfs
    #include "../filesystem/filesystem_dosfs.h"  // low-level driver for filesystem devices on dosfs architecture
#endif
#ifdef EXTENSION_filesystem_dosfs
    #include "../filesystem/filesystem_dosfs.h"  // low-level driver for filesystem devices on dosfs architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

// Check if at least one low-level driver could be activated
#ifndef EXTENSION_FILESYSTEM_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_FILESYSTEM! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _filesystem_
#endif

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_filesystem_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_filesystem_*() declaration in ../ttc_filesystem.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_filesystem_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_filesystem.h for prototypes of low-level driver functions!
 *
 */

void ttc_filesystem_interface_configuration_check( t_ttc_filesystem_config* Config ); //{  macro _driver_filesystem_interface_configuration_check(Config)
#ifdef ttc_driver_filesystem_configuration_check
    // enable following line to forward interface function as a macro definition
    #define _driver_filesystem_configuration_check ttc_driver_filesystem_configuration_check
#else
    #define _driver_filesystem_configuration_check ttc_filesystem_interface_configuration_check
#endif
//}
e_ttc_filesystem_errorcode ttc_filesystem_interface_deinit( t_ttc_filesystem_config* Config ); //{  macro _driver_filesystem_interface_deinit(Config)
#ifdef ttc_driver_filesystem_deinit
    // enable following line to forward interface function as a macro definition
    #define _driver_filesystem_deinit ttc_driver_filesystem_deinit
#else
    #define _driver_filesystem_deinit ttc_filesystem_interface_deinit
#endif
//}
e_ttc_filesystem_errorcode ttc_filesystem_interface_init( t_ttc_filesystem_config* Config ); //{  macro _driver_filesystem_interface_init(Config)
#ifdef ttc_driver_filesystem_init
    // enable following line to forward interface function as a macro definition
    #define _driver_filesystem_init ttc_driver_filesystem_init
#else
    #define _driver_filesystem_init ttc_filesystem_interface_init
#endif
//}
e_ttc_filesystem_errorcode ttc_filesystem_interface_load_defaults( t_ttc_filesystem_config* Config ); //{  macro _driver_filesystem_interface_load_defaults(Config)
#ifdef ttc_driver_filesystem_load_defaults
    // enable following line to forward interface function as a macro definition
    #define _driver_filesystem_load_defaults ttc_driver_filesystem_load_defaults
#else
    #define _driver_filesystem_load_defaults ttc_filesystem_interface_load_defaults
#endif
//}
void ttc_filesystem_interface_prepare(); //{  macro _driver_filesystem_interface_prepare()
#ifdef ttc_driver_filesystem_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_filesystem_prepare ttc_driver_filesystem_prepare
#else
    #define _driver_filesystem_prepare ttc_filesystem_interface_prepare
#endif
//}
void ttc_filesystem_interface_reset( t_ttc_filesystem_config* Config ); //{  macro _driver_filesystem_interface_reset(Config)
#ifdef ttc_driver_filesystem_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_filesystem_reset ttc_driver_filesystem_reset
#else
    #define _driver_filesystem_reset ttc_filesystem_interface_reset
#endif
//}
e_ttc_filesystem_errorcode ttc_filesystem_interface_medium_mount( t_ttc_filesystem_config* Config ); //{  macro _driver_filesystem_interface_medium_mount(LogicalIndex)
#ifdef ttc_driver_filesystem_medium_mount
    // enable following line to forward interface function as a macro definition
    #define _driver_filesystem_medium_mount ttc_driver_filesystem_medium_mount
#else
    #define _driver_filesystem_medium_mount ttc_filesystem_interface_medium_mount
#endif
//}
e_ttc_filesystem_errorcode ttc_filesystem_interface_medium_unmount( t_ttc_filesystem_config* Config ); //{  macro _driver_filesystem_interface_medium_unmount(LogicalIndex)
#ifdef ttc_driver_filesystem_medium_unmount
    // enable following line to forward interface function as a macro definition
    #define _driver_filesystem_medium_unmount ttc_driver_filesystem_medium_unmount
#else
    #define _driver_filesystem_medium_unmount ttc_filesystem_interface_medium_unmount
#endif
//}
e_ttc_filesystem_errorcode ttc_filesystem_interface_open_directory( t_ttc_filesystem_config* Config, const t_u8* Directory, t_u8 DirectoryHandle ); //{  macro _driver_filesystem_interface_open_directory(Config, Directory)
#ifdef ttc_driver_filesystem_open_directory
    // enable following line to forward interface function as a macro definition
    #define _driver_filesystem_open_directory ttc_driver_filesystem_open_directory
#else
    #define _driver_filesystem_open_directory ttc_filesystem_interface_open_directory
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_filesystem_interface_foo(t_ttc_filesystem_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_FILESYSTEM_INTERFACE_H
