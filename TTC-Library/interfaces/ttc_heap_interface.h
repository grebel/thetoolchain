/** { ttc_heap_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for HEAP device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 20 at 20140301 07:28:17 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_HEAP_INTERFACE_H
#define TTC_HEAP_INTERFACE_H

//{ Includes *************************************************************

#include "../ttc_basic.h"

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_heap_zdefault
    #include "../heap/heap_zdefault.h"  // low-level driver for heap devices on zdefault architecture
#endif
#ifdef EXTENSION_heap_freertos
    #include "../heap/heap_freertos.h"  // low-level driver for heap devices on freertos architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_heap_types.h"

//} Includes
t_base ttc_heap_interface_get_free_size(); //{  macro _driver_heap_interface_get_free_size()
#ifdef ttc_driver_heap_get_free_size
    // enable following line to forward interface function as a macro definition
    #define _driver_heap_get_free_size() ttc_driver_heap_get_free_size()
#else
    #define _driver_heap_get_free_size() ttc_heap_interface_get_free_size()
#endif
//}
void* ttc_heap_interface_alloc( t_base Size ); //{  macro _driver_heap_interface_alloc(Size)
#ifdef ttc_driver_heap_alloc
    // enable following line to forward interface function as a macro definition
    #define _driver_heap_alloc(Size) ttc_driver_heap_alloc(Size)
#else
    #define _driver_heap_alloc(Size) ttc_heap_interface_alloc(Size)
#endif
//}
void* ttc_heap_interface_temporary_alloc( t_base Size ); //{  macro _driver_heap_interface_temporary_alloc(Size)
#ifdef ttc_driver_heap_temporary_alloc
    // enable following line to forward interface function as a macro definition
    #define _driver_heap_temporary_alloc(Size) ttc_driver_heap_temporary_alloc(Size)
#else
    #define _driver_heap_temporary_alloc(Size) ttc_heap_interface_temporary_alloc(Size)
#endif
//}
void* ttc_heap_interface_alloc_zeroed( t_base Size ); //{  macro _driver_heap_interface_alloc_zeroed(Size)
#ifdef ttc_driver_heap_alloc_zeroed
    // enable following line to forward interface function as a macro definition
    #define _driver_heap_alloc_zeroed(Size) ttc_driver_heap_alloc_zeroed(Size)
#else
    #undef _driver_heap_alloc_zeroed
#endif
//}
void* ttc_heap_interface_free( void* Block ); //{  macro _driver_heap_interface_free(Block)
#ifdef ttc_driver_heap_free
    // enable following line to forward interface function as a macro definition
    #define _driver_heap_free(Block) ttc_driver_heap_free(Block)
#else
    #define _driver_heap_free(Block) ttc_heap_interface_free(Block)
#endif
//}
void ttc_heap_interface_prepare( volatile t_u8* HeapStart, t_base HeapSize ); //{  macro _driver_heap_interface_prepare(HeapStart, HeapSize)
#ifdef ttc_driver_heap_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_heap_prepare(HeapStart, HeapSize) ttc_driver_heap_prepare(HeapStart, HeapSize)
#else
    #define _driver_heap_prepare(HeapStart, HeapSize) ttc_heap_interface_prepare(HeapStart, HeapSize)
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_heap_interface_foo(t_ttc_heap_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_HEAP_INTERFACE_H
