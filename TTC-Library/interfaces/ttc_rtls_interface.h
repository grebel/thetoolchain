/** { ttc_rtls_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for RTLS device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 23 at 20161208 10:11:47 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_RTLS_INTERFACE_H
#define TTC_RTLS_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_rtls_interface.c"
//

#include "../ttc_basic.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_rtls_crtof_simple_2d
    #include "../rtls/rtls_crtof_simple_2d.h"  // low-level driver for rtls devices on crtof_simple_2d architecture
#endif
#ifdef EXTENSION_rtls_square4
    #include "../rtls/rtls_square4.h"  // low-level driver for rtls devices on square4 architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

// Check if at least one low-level driver could be activated
#ifndef EXTENSION_RTLS_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_RTLS! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _rtls_
#endif

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_rtls_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_rtls_*() declaration in ../ttc_rtls.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_rtls_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_rtls.h for prototypes of low-level driver functions!
 *
 */

e_ttc_rtls_errorcode ttc_rtls_interface_init( t_ttc_rtls_config* Config ); //{  macro _driver_rtls_interface_init(Config)
#ifdef ttc_driver_rtls_init
    // enable following line to forward interface function as a macro definition
    #define _driver_rtls_init ttc_driver_rtls_init
#else
    #define _driver_rtls_init ttc_rtls_interface_init
#endif
//}
e_ttc_rtls_errorcode ttc_rtls_interface_load_defaults( t_ttc_rtls_config* Config ); //{  macro _driver_rtls_interface_load_defaults(Config)
#ifdef ttc_driver_rtls_load_defaults
    // enable following line to forward interface function as a macro definition
    #define _driver_rtls_load_defaults ttc_driver_rtls_load_defaults
#else
    #define _driver_rtls_load_defaults ttc_rtls_interface_load_defaults
#endif
//}
void ttc_rtls_interface_configuration_check( t_ttc_rtls_config* Config ); //{  macro _driver_rtls_interface_configuration_check(Config)
#ifdef ttc_driver_rtls_configuration_check
    // enable following line to forward interface function as a macro definition
    #define _driver_rtls_configuration_check ttc_driver_rtls_configuration_check
#else
    #define _driver_rtls_configuration_check ttc_rtls_interface_configuration_check
#endif
//}
void ttc_rtls_interface_reset( t_ttc_rtls_config* Config ); //{  macro _driver_rtls_interface_reset(Config)
#ifdef ttc_driver_rtls_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_rtls_reset ttc_driver_rtls_reset
#else
    #define _driver_rtls_reset ttc_rtls_interface_reset
#endif
//}
e_ttc_rtls_errorcode ttc_rtls_interface_deinit( t_ttc_rtls_config* Config ); //{  macro _driver_rtls_interface_deinit(Config)
#ifdef ttc_driver_rtls_deinit
    // enable following line to forward interface function as a macro definition
    #define _driver_rtls_deinit ttc_driver_rtls_deinit
#else
    #define _driver_rtls_deinit ttc_rtls_interface_deinit
#endif
//}
void ttc_rtls_interface_prepare(); //{  macro _driver_rtls_interface_prepare()
#ifdef ttc_driver_rtls_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_rtls_prepare ttc_driver_rtls_prepare
#else
    #define _driver_rtls_prepare ttc_rtls_interface_prepare
#endif
//}
void ttc_rtls_interface_anchor_buildup_start( t_ttc_rtls_config* Config ); //{  macro _driver_rtls_interface_anchor_buildup_start(Config)
#ifdef ttc_driver_rtls_anchor_buildup_start
    // enable following line to forward interface function as a macro definition
    #define _driver_rtls_anchor_buildup_start ttc_driver_rtls_anchor_buildup_start
#else
    #define _driver_rtls_anchor_buildup_start ttc_rtls_interface_anchor_buildup_start
#endif
//}
void ttc_rtls_interface_mobile_statemachine( t_ttc_rtls_config* Config ); //{  macro _driver_rtls_interface_mobile_statemachine(Config)
#ifdef ttc_driver_rtls_mobile_statemachine
    // enable following line to forward interface function as a macro definition
    #define _driver_rtls_mobile_statemachine ttc_driver_rtls_mobile_statemachine
#else
    #define _driver_rtls_mobile_statemachine ttc_rtls_interface_mobile_statemachine
#endif
//}
void ttc_rtls_interface_anchor_statemachine( t_ttc_rtls_config* Config ); //{  macro _driver_rtls_interface_anchor_statemachine(Config)
#ifdef ttc_driver_rtls_anchor_statemachine
    // enable following line to forward interface function as a macro definition
    #define _driver_rtls_anchor_statemachine ttc_driver_rtls_anchor_statemachine
#else
    #define _driver_rtls_anchor_statemachine ttc_rtls_interface_anchor_statemachine
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_rtls_interface_foo(t_ttc_rtls_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_RTLS_INTERFACE_H
