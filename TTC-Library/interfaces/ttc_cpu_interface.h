/** { ttc_cpu_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for CPU device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 21 at 20150318 12:33:43 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_CPU_INTERFACE_H
#define TTC_CPU_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_cpu_interface.c"
//

#include "../ttc_basic.h"

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_cpu_stm32f1xx
    #include "../cpu/cpu_stm32f1xx.h"
#endif
#ifdef EXTENSION_cpu_stm32f4xx
    #include "../cpu/cpu_stm32f4xx.h"  // low-level driver for cpu devices on stm32f4xx architecture
#endif
#ifdef EXTENSION_cpu_stm32l1xx
    #include "../cpu/cpu_stm32l1xx.h"  // low-level driver for cpu devices on stm32l1xx architecture
#endif
#ifdef EXTENSION_cpu_stm32l0xx
    #include "../cpu/cpu_stm32l0xx.h"  // low-level driver for cpu devices on stm32l0xx architecture
#endif
#ifdef EXTENSION_cpu_stm32w1xx
    #include "../cpu/cpu_stm32w1xx.h"  // low-level driver for cpu devices on stm32w1xx architecture
#endif
#ifdef EXTENSION_cpu_stm32f0xx
    #include "../cpu/cpu_stm32f0xx.h"  // low-level driver for cpu devices on stm32f0xx architecture
#endif
#ifdef EXTENSION_cpu_stm32f3xx
    #include "../cpu/cpu_stm32f3xx.h"  // low-level driver for cpu devices on stm32f3xx architecture
#endif
#ifdef EXTENSION_cpu_stm32f2xx
    #include "../cpu/cpu_stm32f2xx.h"  // low-level driver for cpu devices on stm32f2xx architecture
#endif
#ifdef EXTENSION_cpu_cortexm4
    #include "../cpu/cpu_cortexm4.h"  // low-level driver for cpu devices on cortexm4 architecture
#endif
#ifdef EXTENSION_cpu_cortexm0
    #include "../cpu/cpu_cortexm0.h"  // low-level driver for cpu devices on cortexm0 architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_cpu_cortexm3
    #include "../cpu/cpu_cortexm3.h"  // low-level driver for cpu devices on cortexm3 architecture
#endif

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_cpu_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_cpu_*() declaration in ../ttc_cpu.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_cpu_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_cpu.h for prototypes of low-level driver functions!
 *
 */

void ttc_cpu_interface_prepare(); //{  macro _driver_cpu_interface_prepare()
#ifdef ttc_driver_cpu_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_cpu_prepare() ttc_driver_cpu_prepare()
#else
    #define _driver_cpu_prepare() ttc_cpu_interface_prepare()
#endif
//}
e_ttc_cpu_errorcode ttc_cpu_interface_init( t_ttc_cpu_config* Config ); //{  macro _driver_cpu_interface_init(Config)
#ifdef ttc_driver_cpu_init
    // enable following line to forward interface function as a macro definition
    #define _driver_cpu_init(Config) ttc_driver_cpu_init(Config)
#else
    #define _driver_cpu_init(Config) ttc_cpu_interface_init(Config)
#endif
//}
void ttc_cpu_interface_reset( t_ttc_cpu_config* Config ); //{  macro _driver_cpu_interface_reset(Config)
#ifdef ttc_driver_cpu_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_cpu_reset(Config) ttc_driver_cpu_reset(Config)
#else
    #define _driver_cpu_reset(Config) ttc_cpu_interface_reset(Config)
#endif
//}
e_ttc_cpu_errorcode ttc_cpu_interface_load_defaults( t_ttc_cpu_config* Config ); //{  macro _driver_cpu_interface_load_defaults(Config)
#ifdef ttc_driver_cpu_load_defaults
    // enable following line to forward interface function as a macro definition
    #define _driver_cpu_load_defaults(Config) ttc_driver_cpu_load_defaults(Config)
#else
    #define _driver_cpu_load_defaults(Config) ttc_cpu_interface_load_defaults(Config)
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_cpu_interface_foo(t_ttc_cpu_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_CPU_INTERFACE_H
