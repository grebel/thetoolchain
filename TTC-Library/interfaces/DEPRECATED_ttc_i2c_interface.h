/** { ttc_i2c_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for I2C device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 21 at 20150531 21:48:47 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_I2C_INTERFACE_H
#define TTC_I2C_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_i2c_interface.c"
//

#include "../ttc_basic.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_i2c_stm32f1xx
#  include "../i2c/i2c_stm32f1xx.h"
#endif
#ifdef EXTENSION_i2c_stm32l1xx
#  include "../i2c/i2c_stm32l1xx.h"  // low-level driver for i2c devices on stm32l1xx architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_i2c_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_i2c_*() declaration in ../ttc_i2c.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_i2c_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_i2c.h for prototypes of low-level driver functions!
 *
 */

e_ttc_i2c_errorcode ttc_i2c_interface_init( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_init(Config)
#ifdef ttc_driver_i2c_init
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_init(Config) ttc_driver_i2c_init(Config)
#else
#  define _driver_i2c_init(Config) ttc_i2c_interface_init(Config)
#endif
//}
void ttc_i2c_interface_reset( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_reset(Config)
#ifdef ttc_driver_i2c_reset
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_reset(Config) ttc_driver_i2c_reset(Config)
#else
#  define _driver_i2c_reset(Config) ttc_i2c_interface_reset(Config)
#endif
//}
void ttc_i2c_interface_prepare(); //{  macro _driver_i2c_interface_prepare()
#ifdef ttc_driver_i2c_prepare
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_prepare() ttc_driver_i2c_prepare()
#else
#  define _driver_i2c_prepare() ttc_i2c_interface_prepare()
#endif
//}
t_ttc_i2c_config* ttc_i2c_interface_get_features( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_get_features(Config)
#ifdef ttc_driver_i2c_get_features
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_get_features(Config) ttc_driver_i2c_get_features(Config)
#else
#  define _driver_i2c_get_features(Config) ttc_i2c_interface_get_features(Config)
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_deinit( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_deinit(Config)
#ifdef ttc_driver_i2c_deinit
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_deinit(Config) ttc_driver_i2c_deinit(Config)
#else
#  define _driver_i2c_deinit(Config) ttc_i2c_interface_deinit(Config)
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_load_defaults( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_load_defaults(Config)
#ifdef ttc_driver_i2c_load_defaults
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_load_defaults(Config) ttc_driver_i2c_load_defaults(Config)
#else
#  define _driver_i2c_load_defaults(Config) ttc_i2c_interface_load_defaults(Config)
#endif
//}
BOOL ttc_i2c_interface_check_event_slave_receiver_second_address_matched( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_slave_receiver_second_address_matched(EventValue)
#ifdef ttc_driver_i2c_check_event_slave_receiver_second_address_matched
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_slave_receiver_second_address_matched(EventValue) ttc_driver_i2c_check_event_slave_receiver_second_address_matched(EventValue)
#else
#  define _driver_i2c_check_event_slave_receiver_second_address_matched(EventValue) ttc_i2c_interface_check_event_slave_receiver_second_address_matched(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_flag_error_no_acknowledge( t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_check_flag_error_no_acknowledge(BaseRegister)
#ifdef ttc_driver_i2c_check_flag_error_no_acknowledge
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag_error_no_acknowledge(BaseRegister) ttc_driver_i2c_check_flag_error_no_acknowledge(BaseRegister)
#else
#  define _driver_i2c_check_flag_error_no_acknowledge(BaseRegister) ttc_i2c_interface_check_flag_error_no_acknowledge(BaseRegister)
#endif
//}
BOOL ttc_i2c_interface_check_event_master_receiver_mode_selected( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_master_receiver_mode_selected(EventValue)
#ifdef ttc_driver_i2c_check_event_master_receiver_mode_selected
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_master_receiver_mode_selected(EventValue) ttc_driver_i2c_check_event_master_receiver_mode_selected(EventValue)
#else
#  define _driver_i2c_check_event_master_receiver_mode_selected(EventValue) ttc_i2c_interface_check_event_master_receiver_mode_selected(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_event_master_byte_transmitted( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_master_byte_transmitted(EventValue)
#ifdef ttc_driver_i2c_check_event_master_byte_transmitted
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_master_byte_transmitted(EventValue) ttc_driver_i2c_check_event_master_byte_transmitted(EventValue)
#else
#  define _driver_i2c_check_event_master_byte_transmitted(EventValue) ttc_i2c_interface_check_event_master_byte_transmitted(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_event_slave_stop_detected( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_slave_stop_detected(EventValue)
#ifdef ttc_driver_i2c_check_event_slave_stop_detected
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_slave_stop_detected(EventValue) ttc_driver_i2c_check_event_slave_stop_detected(EventValue)
#else
#  define _driver_i2c_check_event_slave_stop_detected(EventValue) ttc_i2c_interface_check_event_slave_stop_detected(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_flag_bus_is_busy( t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_check_flag_bus_is_busy(BaseRegister)
#ifdef ttc_driver_i2c_check_flag_bus_is_busy
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag_bus_is_busy(BaseRegister) ttc_driver_i2c_check_flag_bus_is_busy(BaseRegister)
#else
#  define _driver_i2c_check_flag_bus_is_busy(BaseRegister) ttc_i2c_interface_check_flag_bus_is_busy(BaseRegister)
#endif
//}
BOOL ttc_i2c_interface_check_event_master_byte_transmitting( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_master_byte_transmitting(EventValue)
#ifdef ttc_driver_i2c_check_event_master_byte_transmitting
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_master_byte_transmitting(EventValue) ttc_driver_i2c_check_event_master_byte_transmitting(EventValue)
#else
#  define _driver_i2c_check_event_master_byte_transmitting(EventValue) ttc_i2c_interface_check_event_master_byte_transmitting(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_flag_error_misplaced_condition( t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_check_flag_error_misplaced_condition(BaseRegister)
#ifdef ttc_driver_i2c_check_flag_error_misplaced_condition
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag_error_misplaced_condition(BaseRegister) ttc_driver_i2c_check_flag_error_misplaced_condition(BaseRegister)
#else
#  define _driver_i2c_check_flag_error_misplaced_condition(BaseRegister) ttc_i2c_interface_check_flag_error_misplaced_condition(BaseRegister)
#endif
//}
BOOL ttc_i2c_interface_check_event_slave_byte_transmitted( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_slave_byte_transmitted(EventValue)
#ifdef ttc_driver_i2c_check_event_slave_byte_transmitted
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_slave_byte_transmitted(EventValue) ttc_driver_i2c_check_event_slave_byte_transmitted(EventValue)
#else
#  define _driver_i2c_check_event_slave_byte_transmitted(EventValue) ttc_i2c_interface_check_event_slave_byte_transmitted(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_event_slave_byte_received( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_slave_byte_received(EventValue)
#ifdef ttc_driver_i2c_check_event_slave_byte_received
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_slave_byte_received(EventValue) ttc_driver_i2c_check_event_slave_byte_received(EventValue)
#else
#  define _driver_i2c_check_event_slave_byte_received(EventValue) ttc_i2c_interface_check_event_slave_byte_received(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_flag_master_mode( t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_check_flag_master_mode(BaseRegister)
#ifdef ttc_driver_i2c_check_flag_master_mode
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag_master_mode(BaseRegister) ttc_driver_i2c_check_flag_master_mode(BaseRegister)
#else
#  define _driver_i2c_check_flag_master_mode(BaseRegister) ttc_i2c_interface_check_flag_master_mode(BaseRegister)
#endif
//}
BOOL ttc_i2c_interface_check_event_slave_byte_transmitting( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_slave_byte_transmitting(EventValue)
#ifdef ttc_driver_i2c_check_event_slave_byte_transmitting
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_slave_byte_transmitting(EventValue) ttc_driver_i2c_check_event_slave_byte_transmitting(EventValue)
#else
#  define _driver_i2c_check_event_slave_byte_transmitting(EventValue) ttc_i2c_interface_check_event_slave_byte_transmitting(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_event_master_transmitter_mode_selected( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_master_transmitter_mode_selected(EventValue)
#ifdef ttc_driver_i2c_check_event_master_transmitter_mode_selected
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_master_transmitter_mode_selected(EventValue) ttc_driver_i2c_check_event_master_transmitter_mode_selected(EventValue)
#else
#  define _driver_i2c_check_event_master_transmitter_mode_selected(EventValue) ttc_i2c_interface_check_event_master_transmitter_mode_selected(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_event_slave_general_call_address_matched( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_slave_general_call_address_matched(EventValue)
#ifdef ttc_driver_i2c_check_event_slave_general_call_address_matched
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_slave_general_call_address_matched(EventValue) ttc_driver_i2c_check_event_slave_general_call_address_matched(EventValue)
#else
#  define _driver_i2c_check_event_slave_general_call_address_matched(EventValue) ttc_i2c_interface_check_event_slave_general_call_address_matched(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_event_slave_transmitter_address_matched( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_slave_transmitter_address_matched(EventValue)
#ifdef ttc_driver_i2c_check_event_slave_transmitter_address_matched
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_slave_transmitter_address_matched(EventValue) ttc_driver_i2c_check_event_slave_transmitter_address_matched(EventValue)
#else
#  define _driver_i2c_check_event_slave_transmitter_address_matched(EventValue) ttc_i2c_interface_check_event_slave_transmitter_address_matched(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_flag_smb_default_address_received( t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_check_flag_smb_default_address_received(BaseRegister)
#ifdef ttc_driver_i2c_check_flag_smb_default_address_received
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag_smb_default_address_received(BaseRegister) ttc_driver_i2c_check_flag_smb_default_address_received(BaseRegister)
#else
#  define _driver_i2c_check_flag_smb_default_address_received(BaseRegister) ttc_i2c_interface_check_flag_smb_default_address_received(BaseRegister)
#endif
//}
BOOL ttc_i2c_interface_check_flag_bytes_received( t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_check_flag_bytes_received(BaseRegister)
#ifdef ttc_driver_i2c_check_flag_bytes_received
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag_bytes_received(BaseRegister) ttc_driver_i2c_check_flag_bytes_received(BaseRegister)
#else
#  define _driver_i2c_check_flag_bytes_received(BaseRegister) ttc_i2c_interface_check_flag_bytes_received(BaseRegister)
#endif
//}
BOOL ttc_i2c_interface_check_flag_smb_error_timeout( t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_check_flag_smb_error_timeout(BaseRegister)
#ifdef ttc_driver_i2c_check_flag_smb_error_timeout
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag_smb_error_timeout(BaseRegister) ttc_driver_i2c_check_flag_smb_error_timeout(BaseRegister)
#else
#  define _driver_i2c_check_flag_smb_error_timeout(BaseRegister) ttc_i2c_interface_check_flag_smb_error_timeout(BaseRegister)
#endif
//}
BOOL ttc_i2c_interface_check_event_slave_ack_failure( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_slave_ack_failure(EventValue)
#ifdef ttc_driver_i2c_check_event_slave_ack_failure
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_slave_ack_failure(EventValue) ttc_driver_i2c_check_event_slave_ack_failure(EventValue)
#else
#  define _driver_i2c_check_event_slave_ack_failure(EventValue) ttc_i2c_interface_check_event_slave_ack_failure(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_flag_error_slave_buffer_overrun( t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_check_flag_error_slave_buffer_overrun(BaseRegister)
#ifdef ttc_driver_i2c_check_flag_error_slave_buffer_overrun
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag_error_slave_buffer_overrun(BaseRegister) ttc_driver_i2c_check_flag_error_slave_buffer_overrun(BaseRegister)
#else
#  define _driver_i2c_check_flag_error_slave_buffer_overrun(BaseRegister) ttc_i2c_interface_check_flag_error_slave_buffer_overrun(BaseRegister)
#endif
//}
BOOL ttc_i2c_interface_check_event_master_byte_received( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_master_byte_received(EventValue)
#ifdef ttc_driver_i2c_check_event_master_byte_received
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_master_byte_received(EventValue) ttc_driver_i2c_check_event_master_byte_received(EventValue)
#else
#  define _driver_i2c_check_event_master_byte_received(EventValue) ttc_i2c_interface_check_event_master_byte_received(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_flag_error_arbitration_lost( t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_check_flag_error_arbitration_lost(BaseRegister)
#ifdef ttc_driver_i2c_check_flag_error_arbitration_lost
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag_error_arbitration_lost(BaseRegister) ttc_driver_i2c_check_flag_error_arbitration_lost(BaseRegister)
#else
#  define _driver_i2c_check_flag_error_arbitration_lost(BaseRegister) ttc_i2c_interface_check_flag_error_arbitration_lost(BaseRegister)
#endif
//}
BOOL ttc_i2c_interface_check_event_slave_transmitter_second_address_matched( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_slave_transmitter_second_address_matched(EventValue)
#ifdef ttc_driver_i2c_check_event_slave_transmitter_second_address_matched
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_slave_transmitter_second_address_matched(EventValue) ttc_driver_i2c_check_event_slave_transmitter_second_address_matched(EventValue)
#else
#  define _driver_i2c_check_event_slave_transmitter_second_address_matched(EventValue) ttc_i2c_interface_check_event_slave_transmitter_second_address_matched(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_event_bus_not_busy( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_bus_not_busy(EventValue)
#ifdef ttc_driver_i2c_check_event_bus_not_busy
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_bus_not_busy(EventValue) ttc_driver_i2c_check_event_bus_not_busy(EventValue)
#else
#  define _driver_i2c_check_event_bus_not_busy(EventValue) ttc_i2c_interface_check_event_bus_not_busy(EventValue)
#endif
//}

BOOL ttc_i2c_interface_check_event_master_slave_has_acknowledged_start( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_master_slave_has_acknowledged_start(EventValue)
#ifdef ttc_driver_i2c_check_event_master_slave_has_acknowledged_start
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_master_slave_has_acknowledged_start(EventValue) ttc_driver_i2c_check_event_master_slave_has_acknowledged_start(EventValue)
#else
#  define _driver_i2c_check_event_master_slave_has_acknowledged_start(EventValue) ttc_i2c_interface_check_event_master_slave_has_acknowledged_start(EventValue)
#endif
//}

BOOL ttc_i2c_interface_check_flag_bytes_transmitted( t_base EventValue ); //{  macro _driver_i2c_interface_check_flag_bytes_transmitted(EventValue)
#ifdef ttc_driver_i2c_check_flag_bytes_transmitted
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag_bytes_transmitted(EventValue) ttc_driver_i2c_check_flag_bytes_transmitted(EventValue)
#else
#  define _driver_i2c_check_flag_bytes_transmitted(EventValue) ttc_i2c_interface_check_flag_bytes_transmitted(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_event_slave_receiver_address_matched( t_base EventValue ); //{  macro _driver_i2c_interface_check_event_slave_receiver_address_matched(EventValue)
#ifdef ttc_driver_i2c_check_event_slave_receiver_address_matched
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_event_slave_receiver_address_matched(EventValue) ttc_driver_i2c_check_event_slave_receiver_address_matched(EventValue)
#else
#  define _driver_i2c_check_event_slave_receiver_address_matched(EventValue) ttc_i2c_interface_check_event_slave_receiver_address_matched(EventValue)
#endif
//}
BOOL ttc_i2c_interface_check_flag_smb_host_header_received( t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_check_flag_smb_host_header_received(BaseRegister)
#ifdef ttc_driver_i2c_check_flag_smb_host_header_received
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag_smb_host_header_received(BaseRegister) ttc_driver_i2c_check_flag_smb_host_header_received(BaseRegister)
#else
#  define _driver_i2c_check_flag_smb_host_header_received(BaseRegister) ttc_i2c_interface_check_flag_smb_host_header_received(BaseRegister)
#endif
//}
BOOL ttc_i2c_interface_check_flag_error_parity( t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_check_flag_error_parity(BaseRegister)
#ifdef ttc_driver_i2c_check_flag_error_parity
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag_error_parity(BaseRegister) ttc_driver_i2c_check_flag_error_parity(BaseRegister)
#else
#  define _driver_i2c_check_flag_error_parity(BaseRegister) ttc_i2c_interface_check_flag_error_parity(BaseRegister)
#endif
//}
BOOL ttc_i2c_interface_check_flag_general_call_received( t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_check_flag_general_call_received(BaseRegister)
#ifdef ttc_driver_i2c_check_flag_general_call_received
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag_general_call_received(BaseRegister) ttc_driver_i2c_check_flag_general_call_received(BaseRegister)
#else
#  define _driver_i2c_check_flag_general_call_received(BaseRegister) ttc_i2c_interface_check_flag_general_call_received(BaseRegister)
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_master_send_slave_address( t_ttc_i2c_config* Config, t_u16 SlaveAddress, BOOL ReadMode ); //{  macro _driver_i2c_interface_master_send_slave_address(Config, SlaveAddress, ReadMode)
#ifdef ttc_driver_i2c_master_send_slave_address
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_master_send_slave_address(Config, SlaveAddress, ReadMode) ttc_driver_i2c_master_send_slave_address(Config, SlaveAddress, ReadMode)
#else
#  define _driver_i2c_master_send_slave_address(Config, SlaveAddress, ReadMode) ttc_i2c_interface_master_send_slave_address(Config, SlaveAddress, ReadMode)
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_master_condition_start( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_master_condition_start(Config)
#ifdef ttc_driver_i2c_master_condition_start
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_master_condition_start(Config) ttc_driver_i2c_master_condition_start(Config)
#else
#  define _driver_i2c_master_condition_start(Config) ttc_i2c_interface_master_condition_start(Config)
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_master_condition_stop( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_master_condition_stop(Config)
#ifdef ttc_driver_i2c_master_condition_stop
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_master_condition_stop(Config) ttc_driver_i2c_master_condition_stop(Config)
#else
#  define _driver_i2c_master_condition_stop(Config) ttc_i2c_interface_master_condition_stop(Config)
#endif
//}
BOOL ttc_i2c_interface_check_flag_transmit_buffer_empty( volatile t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_check_flag_transmit_buffer_empty(BaseRegister)
#ifdef ttc_driver_i2c_check_flag_transmit_buffer_empty
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag_transmit_buffer_empty(BaseRegister) ttc_driver_i2c_check_flag_transmit_buffer_empty(BaseRegister)
#else
#  define _driver_i2c_check_flag_transmit_buffer_empty(BaseRegister) ttc_i2c_interface_check_flag_transmit_buffer_empty(BaseRegister)
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_slave_read_bytes( t_ttc_i2c_config* Config, t_u8 BufferSize, t_u8* Buffer ); //{  macro _driver_i2c_interface_slave_read_bytes(Config, BufferSize, Buffer)
#ifdef ttc_driver_i2c_slave_read_bytes
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_slave_read_bytes(Config, BufferSize, Buffer) ttc_driver_i2c_slave_read_bytes(Config, BufferSize, Buffer)
#else
#  define _driver_i2c_slave_read_bytes(Config, BufferSize, Buffer) ttc_i2c_interface_slave_read_bytes(Config, BufferSize, Buffer)
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_master_read_bytes( t_ttc_i2c_config* Config, t_u8 Amount, t_u8* Buffer ); //{  macro _driver_i2c_interface_master_read_bytes(Config, Amount, Buffer)
#ifdef ttc_driver_i2c_master_read_bytes
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_master_read_bytes(Config, Amount, Buffer) ttc_driver_i2c_master_read_bytes(Config, Amount, Buffer)
#else
#  define _driver_i2c_master_read_bytes(Config, Amount, Buffer) ttc_i2c_interface_master_read_bytes(Config, Amount, Buffer)
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_master_send_bytes( t_ttc_i2c_config* Config, t_u8 Amount, const t_u8* Buffer ); //{  macro _driver_i2c_interface_master_send_bytes(Config, Amount, Buffer)
#ifdef ttc_driver_i2c_master_send_bytes
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_master_send_bytes(Config, Amount, Buffer) ttc_driver_i2c_master_send_bytes(Config, Amount, Buffer)
#else
#  define _driver_i2c_master_send_bytes(Config, Amount, Buffer) ttc_i2c_interface_master_send_bytes(Config, Amount, Buffer)
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_slave_send_bytes( t_ttc_i2c_config* Config, t_u8 BufferSize, const t_u8* Buffer ); //{  macro _driver_i2c_interface_slave_send_bytes(Config, BufferSize, Buffer)
#ifdef ttc_driver_i2c_slave_send_bytes
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_slave_send_bytes(Config, BufferSize, Buffer) ttc_driver_i2c_slave_send_bytes(Config, BufferSize, Buffer)
#else
#  define _driver_i2c_slave_send_bytes(Config, BufferSize, Buffer) ttc_i2c_interface_slave_send_bytes(Config, BufferSize, Buffer)
#endif
//}
BOOL ttc_i2c_interface_slave_check_own_address_received( volatile t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_slave_check_own_address_received(BaseRegister)
#ifdef ttc_driver_i2c_slave_check_own_address_received
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_slave_check_own_address_received(BaseRegister) ttc_driver_i2c_slave_check_own_address_received(BaseRegister)
#else
#  define _driver_i2c_slave_check_own_address_received(BaseRegister) ttc_i2c_interface_slave_check_own_address_received(BaseRegister)
#endif
//}
BOOL ttc_i2c_interface_slave_check_read_mode( volatile t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_slave_check_read_mode(BaseRegister)
#ifdef ttc_driver_i2c_slave_check_read_mode
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_slave_check_read_mode(BaseRegister) ttc_driver_i2c_slave_check_read_mode(BaseRegister)
#else
#  define _driver_i2c_slave_check_read_mode(BaseRegister) ttc_i2c_interface_slave_check_read_mode(BaseRegister)
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_slave_reset_bus( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_slave_reset_bus(Config)
#ifdef ttc_driver_i2c_slave_reset_bus
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_slave_reset_bus(Config) ttc_driver_i2c_slave_reset_bus(Config)
#else
#  define _driver_i2c_slave_reset_bus(Config) ttc_i2c_interface_slave_reset_bus(Config)
#endif
//}
t_base ttc_i2c_interface_get_event_value( volatile t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_get_event_value(BaseRegister)
#ifdef ttc_driver_i2c_get_event_value
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_get_event_value(BaseRegister) ttc_driver_i2c_get_event_value(BaseRegister)
#else
#  define _driver_i2c_get_event_value(BaseRegister) ttc_i2c_interface_get_event_value(BaseRegister)
#endif
//}
BOOL ttc_i2c_interface_check_flag( volatile t_ttc_i2c_base_register* BaseRegister, e_ttc_i2c_flag_code FlagCode ); //{  macro _driver_i2c_interface_check_flag(BaseRegister, FlagCode)
#ifdef ttc_driver_i2c_check_flag
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag(BaseRegister, FlagCode) ttc_driver_i2c_check_flag(BaseRegister, FlagCode)
#else
#  define _driver_i2c_check_flag(BaseRegister, FlagCode) ttc_i2c_interface_check_flag(BaseRegister, FlagCode)
#endif
//}
void ttc_i2c_interface_enable_acknowledge( volatile t_ttc_i2c_base_register* BaseRegister, BOOL Enable ); //{  macro _driver_i2c_interface_enable_acknowledge(BaseRegister, Enable)
#ifdef ttc_driver_i2c_enable_acknowledge
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_enable_acknowledge(BaseRegister, Enable) ttc_driver_i2c_enable_acknowledge(BaseRegister, Enable)
#else
#  define _driver_i2c_enable_acknowledge(BaseRegister, Enable) ttc_i2c_interface_enable_acknowledge(BaseRegister, Enable)
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_i2c_interface_foo(t_ttc_i2c_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_I2C_INTERFACE_H
