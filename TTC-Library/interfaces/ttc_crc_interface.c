/** { ttc_crc_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for CRC device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_crc_*() functions.
 *  Instead they can call _driver_crc_*() pendants.
 *  E.g.: ttc_crc_reset() -> _driver_crc_reset()
 *
 *  Created from template ttc_device_interface.c revision 25 at 20180323 10:28:15 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_crc_interface.h".
//
#include "ttc_crc_interface.h"
#include "../ttc_task.h"             // provides macro TTC_TASK_RETURN()
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

// interface will include low-driver for us. Check if at least one low-level driver could be activated
#ifndef EXTENSION_CRC_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_<DRIVER>! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _crc_
#endif

//}Includes
/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Private Function declarations ****************************************

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Private Function declarations
//{ Function definitions *************************************************

#ifndef ttc_driver_crc_configuration_check  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_crc_configuration_check() (using default implementation)!

void ttc_crc_interface_configuration_check( t_ttc_crc_config* Config ) {
    Assert_CRC_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_crc_interface_configuration_check() is empty.


}

#endif
#ifndef ttc_driver_crc_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_crc_deinit() (using default implementation)!

e_ttc_crc_errorcode ttc_crc_interface_deinit( t_ttc_crc_config* Config ) {
    Assert_CRC_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_crc_interface_deinit() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_crc_init  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_crc_init() (using default implementation)!

e_ttc_crc_errorcode ttc_crc_interface_init( t_ttc_crc_config* Config ) {
    Assert_CRC_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_crc_interface_init() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_crc_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_crc_load_defaults() (using default implementation)!

e_ttc_crc_errorcode ttc_crc_interface_load_defaults( t_ttc_crc_config* Config ) {
    Assert_CRC_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_crc_interface_load_defaults() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_crc_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_crc_prepare() (using default implementation)!

void ttc_crc_interface_prepare() {


#warning Function ttc_crc_interface_prepare() is empty.


}

#endif
#ifndef ttc_driver_crc_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_crc_reset() (using default implementation)!

void ttc_crc_interface_reset( t_ttc_crc_config* Config ) {
    Assert_CRC_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_crc_interface_reset() is empty.


}

#endif
#ifndef ttc_driver_crc_crc16_calculate_0x1021  // no low-level implementation: use default implementation
t_u16 ttc_crc_interface_crc16_calculate_0x1021( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue ) {
    Assert_CRC_EXTRA_Writable( ( void* ) Buffer, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    const t_u16 Polynom = 0x1021;
    VOLATILE_CRC t_u16 CRC16 = StartValue;

    // implementation adapted from here: http://srecord.sourceforge.net/crc16-ccitt.html (look for bad_crc)
    while ( Amount-- ) {
        t_u16 Byte = *Buffer++;

        Byte <<= 8;

        for ( t_u8 Iteration = 8; Iteration > 0; Iteration-- ) {
            if ( ( CRC16 ^ Byte ) & 0x8000 ) {
                CRC16 = ( CRC16 << 1 ) ^ Polynom;
            }
            else {
                CRC16 <<= 1;
            }
            Byte <<= 1;
        }
    }

    TTC_TASK_RETURN CRC16;
}

#endif
#ifndef ttc_driver_crc_crc16_calculate_ccitt  // no low-level implementation: use default implementation
t_u16 ttc_crc_interface_crc16_calculate_ccitt( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue ) {
    Assert_CRC_EXTRA_Readable( ( void* ) Buffer, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    const t_u16 Polynom = 0x1021;
    VOLATILE_CRC t_u16 CRC16 = StartValue;

    // implementation adapted from here: http://srecord.sourceforge.net/crc16-ccitt.html
    t_u8 xor_flag;
    while ( Amount-- ) {
        t_u8 Byte = *Buffer++;

        for ( t_u8 Iteration = 8; Iteration > 0; Iteration-- ) {
            xor_flag = ( CRC16 & 0x8000 ) ? 1 : 0;
            CRC16 <<= 1;

            if ( Byte & 0x80 ) {
                // Append next bit of message to end of CRC if it is not zero.
                // The zero bit placed there by the shift above need not be
                // changed if the next bit of the message is zero.
                CRC16 |= 1;
            }

            if ( xor_flag )
            { CRC16 ^= Polynom; }

            Byte <<= 1;
        }
    }
    for ( t_u8 Iteration = 16; Iteration > 0; Iteration-- ) { // shift out CRC16
        if ( CRC16 & 0x8000 ) {
            CRC16 = ( CRC16 << 1 ) ^ Polynom;
        }
        else {
            CRC16 <<= 1;
        }
    }

    TTC_TASK_RETURN CRC16;
}

#endif
#ifndef ttc_driver_crc_crc16_check_0x1021  // no low-level implementation: use default implementation

BOOL ttc_crc_interface_crc16_check_0x1021( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue ) {
    VOLATILE_CRC t_u16 CRC16 = _driver_crc_crc16_calculate_0x1021( Buffer, Amount, StartValue );

    return ( CRC16 == * ( ( t_u16* )( &Buffer[Amount] ) ) ) ? TRUE : FALSE;
}

#endif
#ifndef ttc_driver_crc_crc16_check_ccitt  // no low-level implementation: use default implementation

BOOL ttc_crc_interface_crc16_check_ccitt( const t_u8* Buffer, t_u32 Amount, t_u16 StartValue ) {
    VOLATILE_CRC t_u16 CRC16 = _driver_crc_crc16_calculate_ccitt( Buffer, Amount, StartValue );

    return ( CRC16 == * ( ( t_u16* )( &Buffer[Amount] ) ) ) ? TRUE : FALSE;
}

#endif
#ifndef ttc_driver_crc_crc7_calculate  // no low-level implementation: use default implementation
t_u8 ttc_crc_interface_crc7_calculate( const t_u8* Buffer, t_u16 Amount, t_u8 StartValue ) {
    Assert_CRC_EXTRA_Readable( ( void* ) Buffer, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    t_u8 CRC7 = StartValue;

    static t_u8 crc_small_crc7_Implementation = 2;
    if ( crc_small_crc7_Implementation == 1 ) { // runtime for Amount=1024 on STM32L100 @45MHz 14160us
        // implementation adapted from here: http://nerdclub-uk.blogspot.de/2012/11/how-spi-works-with-sd-card.html
        while ( Amount-- ) {
            t_u8 Byte = *Buffer++;
            for ( t_u8 Iteration = 8; Iteration > 0; Iteration-- ) {
                CRC7 <<= 1;
                if ( ( Byte & 0x80 ) ^ ( CRC7 & 0x80 ) )
                { CRC7 ^= 0x09; }
                Byte <<= 1;
            }
        }
    }
    if ( crc_small_crc7_Implementation == 2 ) { // runtime for Amount=1024 on STM32L100 @45MHz  8200us
        // implementation adapted from here: http://nerdclub-uk.blogspot.de/2012/11/how-spi-works-with-sd-card.html
        while ( Amount-- ) {
            t_u8 Byte = *Buffer++;

            // iteration 1
            CRC7 <<= 1;
            if ( ( Byte & 0x80 ) ^ ( CRC7 & 0x80 ) )
            { CRC7 ^= 0x09; }
            Byte <<= 1;

            // iteration 2
            CRC7 <<= 1;
            if ( ( Byte & 0x80 ) ^ ( CRC7 & 0x80 ) )
            { CRC7 ^= 0x09; }
            Byte <<= 1;

            // iteration 3
            CRC7 <<= 1;
            if ( ( Byte & 0x80 ) ^ ( CRC7 & 0x80 ) )
            { CRC7 ^= 0x09; }
            Byte <<= 1;

            // iteration 4
            CRC7 <<= 1;
            if ( ( Byte & 0x80 ) ^ ( CRC7 & 0x80 ) )
            { CRC7 ^= 0x09; }
            Byte <<= 1;

            // iteration 5
            CRC7 <<= 1;
            if ( ( Byte & 0x80 ) ^ ( CRC7 & 0x80 ) )
            { CRC7 ^= 0x09; }
            Byte <<= 1;

            // iteration 6
            CRC7 <<= 1;
            if ( ( Byte & 0x80 ) ^ ( CRC7 & 0x80 ) )
            { CRC7 ^= 0x09; }
            Byte <<= 1;

            // iteration 7
            CRC7 <<= 1;
            if ( ( Byte & 0x80 ) ^ ( CRC7 & 0x80 ) )
            { CRC7 ^= 0x09; }
            Byte <<= 1;

            // iteration 8
            CRC7 <<= 1;
            if ( ( Byte & 0x80 ) ^ ( CRC7 & 0x80 ) )
            { CRC7 ^= 0x09; }
            //Byte <<= 1;
        }
    }
    if ( crc_small_crc7_Implementation == 3 ) { // BROKEN (does not pass tests ttc_sdcard_prepare() ): runtime for Amount=1024 on STM32L100 @45MHz  5512us
        // implementation adapted from here: https://github.com/LonelyWolf/stm32/blob/master/stm32l-dosfs/sdcard.c
        // 7 bit CRC with polynomial x^7 + x^3 + 1
        TODO( "Fix or remove this implementation!" );

        while ( Amount-- ) {
            CRC7 ^= *Buffer++;

            if ( CRC7 & 0x80 )
            { CRC7 ^= 0x89; }
            CRC7 <<= 1;

            if ( CRC7 & 0x80 )
            { CRC7 ^= 0x89; }
            CRC7 <<= 1;

            if ( CRC7 & 0x80 )
            { CRC7 ^= 0x89; }
            CRC7 <<= 1;

            if ( CRC7 & 0x80 )
            { CRC7 ^= 0x89; }
            CRC7 <<= 1;

            if ( CRC7 & 0x80 )
            { CRC7 ^= 0x89; }
            CRC7 <<= 1;

            if ( CRC7 & 0x80 )
            { CRC7 ^= 0x89; }
            CRC7 <<= 1;

            if ( CRC7 & 0x80 )
            { CRC7 ^= 0x89; }
            CRC7 <<= 1;

            if ( CRC7 & 0x80 )
            { CRC7 ^= 0x89; }
            CRC7 <<= 1;
        }
        CRC7 >>= 1;
    }

    TTC_TASK_RETURN( CRC7 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_crc_crc7_check  // no low-level implementation: use default implementation

BOOL ttc_crc_interface_crc7_check( const t_u8* Buffer, t_u16 Amount, t_u8 StartValue ) {
    VOLATILE_CRC t_u8 CRC7 = _driver_crc_crc7_calculate( Buffer, Amount, StartValue );

    return ( CRC7 == Buffer[Amount] ) ? TRUE : FALSE;
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_crc_interface_foo(t_ttc_crc_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
