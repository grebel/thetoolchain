/** { ttc_ethernet_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for ETHERNET device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_ethernet_*() functions.
 *  Instead they can call _driver_ethernet_*() pendants.
 *  E.g.: ttc_ethernet_reset() -> _driver_ethernet_reset()
 *
 *  Created from template ttc_device_interface.c revision 24 at 20180130 11:09:47 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_ethernet_interface.h".
//
#include "ttc_ethernet_interface.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

// interface will include low-driver for us. Check if at least one low-level driver could be activated
#ifndef EXTENSION_ETHERNET_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_<DRIVER>! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _ethernet_
#endif

//}Includes
/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Private Function declarations ****************************************

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Private Function declarations
//{ Function definitions *************************************************

#ifndef ttc_driver_ethernet_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_ethernet_reset() (using default implementation)!

void ttc_ethernet_interface_reset( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_ethernet_interface_reset() is empty.


}

#endif
#ifndef ttc_driver_ethernet_configuration_check  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_ethernet_configuration_check() (using default implementation)!

void ttc_ethernet_interface_configuration_check( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_ethernet_interface_configuration_check() is empty.


}

#endif
#ifndef ttc_driver_ethernet_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_ethernet_load_defaults() (using default implementation)!

e_ttc_ethernet_errorcode ttc_ethernet_interface_load_defaults( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_ethernet_interface_load_defaults() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_ethernet_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_ethernet_prepare() (using default implementation)!

void ttc_ethernet_interface_prepare() {


#warning Function ttc_ethernet_interface_prepare() is empty.


}

#endif
#ifndef ttc_driver_ethernet_init  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_ethernet_init() (using default implementation)!

e_ttc_ethernet_errorcode ttc_ethernet_interface_init( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_ethernet_interface_init() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_ethernet_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_ethernet_deinit() (using default implementation)!

e_ttc_ethernet_errorcode ttc_ethernet_interface_deinit( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_ethernet_interface_deinit() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_ethernet_interface_foo(t_ttc_ethernet_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
