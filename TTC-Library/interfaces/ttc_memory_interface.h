/** { ttc_memory_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for MEMORY device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 20 at 20140228 13:50:05 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_MEMORY_INTERFACE_H
#define TTC_MEMORY_INTERFACE_H

//{ Includes *************************************************************

#include "../ttc_basic.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_memory_stm32w1xx
    #include "../memory/memory_stm32w1xx.h"
#endif
#ifdef EXTENSION_memory_stm32f1xx
    #include "../memory/memory_stm32f1xx.h"  // low-level driver for memory devices on stm32f1xx architecture
#endif
#ifdef EXTENSION_memory_stm32l1xx
    #include "../memory/memory_stm32l1xx.h"  // low-level driver for memory devices on stm32l1xx architecture
#endif
#ifdef EXTENSION_memory_stm32l0xx
    #include "../memory/memory_stm32l0xx.h"  // low-level driver for memory devices on stm32l0xx architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_memory_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_memory_*() declaration in ../ttc_memory.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_memory_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_memory.h for prototypes of low-level driver functions!
 *
 */

t_s8 ttc_memory_interface_is_executable( volatile const void* Address ); //{  macro _driver_memory_interface_is_executable(Address)
#ifdef ttc_driver_memory_is_executable
    // enable following line to forward interface function as a macro definition
    #define _driver_memory_is_executable(Address) ttc_driver_memory_is_executable(Address)
#else
    #define _driver_memory_is_executable(Address) ttc_memory_interface_is_executable(Address)
#endif
//}
t_s8 ttc_memory_interface_is_readable( volatile const void* Address ); //{  macro _driver_memory_interface_is_readable(Address)
#ifdef ttc_driver_memory_is_readable
    // enable following line to forward interface function as a macro definition
    #define _driver_memory_is_readable(Address) ttc_driver_memory_is_readable(Address)
#else
    #define _driver_memory_is_readable(Address) ttc_memory_interface_is_readable(Address)
#endif
//}
t_s8 ttc_memory_interface_is_writable( volatile const void* Address ); //{  macro _driver_memory_interface_is_writable(Address)
#ifdef ttc_driver_memory_is_writable
    // enable following line to forward interface function as a macro definition
    #define _driver_memory_is_writable(Address) ttc_driver_memory_is_writable(Address)
#else
    #define _driver_memory_is_writable(Address) ttc_memory_interface_is_writable(Address)
#endif
//}
t_s8 ttc_memory_interface_is_constant( volatile const void* Address ); //{  macro _driver_memory_interface_is_constant(Address)
#ifdef ttc_driver_memory_is_constant
    // enable following line to forward interface function as a macro definition
    #define _driver_memory_is_constant(Address) ttc_driver_memory_is_constant(Address)
#else
    #define _driver_memory_is_constant(Address) ttc_memory_interface_is_constant(Address)
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_memory_interface_foo(t_ttc_memory_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_MEMORY_INTERFACE_H
