/** { ttc_board_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for BOARD device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 23 at 20180420 13:26:22 UTC
 *
 *  Define ranks of high-level and low-level drivers (required by create_DeviceDriver.pl to generate correct #ifdefs)
 *  TTC_RANK_HIGHLEVEL=490
 *  TTC_RANK_LOWLEVEL=110
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_BOARD_INTERFACE_H
#define TTC_BOARD_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_board_interface.c"
//

#include "../ttc_basic.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_board_stm32l100c_discovery
    #include "../board/board_stm32l100c_discovery.h"
#endif
#ifdef EXTENSION_board_stm32l152_discovery
    #include "../board/board_stm32l152_discovery.h"  // low-level driver for board devices on stm32l152_discovery architecture
#endif
#ifdef EXTENSION_board_stm32l053_discovery
    #include "../board/board_stm32l053_discovery.h"  // low-level driver for board devices on stm32l053_discovery architecture
#endif
#ifdef EXTENSION_board_sensor_dwm1000
    #include "../board/board_sensor_dwm1000.h"  // low-level driver for board devices on sensor_dwm1000 architecture
#endif
#ifdef EXTENSION_board_sensor_dwm10001000
    #include "../board/board_sensor_dwm1000.h"  // low-level driver for board devices on sensor_dwm1000 architecture
#endif
#ifdef EXTENSION_board_olimex_stm32_lcd
    #include "../board/board_olimex_stm32_lcd.h"  // low-level driver for board devices on olimex_stm32_lcd architecture
#endif
#ifdef EXTENSION_board_dso_0138
    #include "../board/board_dso_0138.h"  // low-level driver for board devices on dso_0138 architecture
#endif
#ifdef EXTENSION_board_olimex_stm32_p107
    #include "../board/board_olimex_stm32_p107.h"  // low-level driver for board devices on olimex_stm32_p107 architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

// Check if at least one low-level driver could be activated
#ifndef EXTENSION_BOARD_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_BOARD! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _board_
#endif

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_board_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_board_*() declaration in ../ttc_board.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_board_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_board.h for prototypes of low-level driver functions!
 *
 */

void ttc_board_interface_prepare(); //{  macro _driver_board_interface_prepare()
#ifdef ttc_driver_board_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_board_prepare ttc_driver_board_prepare
#else
    #define _driver_board_prepare ttc_board_interface_prepare
#endif
//}
void ttc_board_interface_configuration_check( t_ttc_board_config* Config ); //{  macro _driver_board_interface_configuration_check(Config)
#ifdef ttc_driver_board_configuration_check
    // enable following line to forward interface function as a macro definition
    #define _driver_board_configuration_check ttc_driver_board_configuration_check
#else
    #define _driver_board_configuration_check ttc_board_interface_configuration_check
#endif
//}
e_ttc_board_errorcode ttc_board_interface_load_defaults( t_ttc_board_config* Config ); //{  macro _driver_board_interface_load_defaults(Config)
#ifdef ttc_driver_board_load_defaults
    // enable following line to forward interface function as a macro definition
    #define _driver_board_load_defaults ttc_driver_board_load_defaults
#else
    #define _driver_board_load_defaults ttc_board_interface_load_defaults
#endif
//}
void ttc_board_interface_reset( t_ttc_board_config* Config ); //{  macro _driver_board_interface_reset(Config)
#ifdef ttc_driver_board_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_board_reset ttc_driver_board_reset
#else
    #define _driver_board_reset ttc_board_interface_reset
#endif
//}
e_ttc_board_errorcode ttc_board_interface_init( t_ttc_board_config* Config ); //{  macro _driver_board_interface_init(Config)
#ifdef ttc_driver_board_init
    // enable following line to forward interface function as a macro definition
    #define _driver_board_init ttc_driver_board_init
#else
    #define _driver_board_init ttc_board_interface_init
#endif
//}
e_ttc_board_errorcode ttc_board_interface_deinit( t_ttc_board_config* Config ); //{  macro _driver_board_interface_deinit(Config)
#ifdef ttc_driver_board_deinit
    // enable following line to forward interface function as a macro definition
    #define _driver_board_deinit ttc_driver_board_deinit
#else
    #define _driver_board_deinit ttc_board_interface_deinit
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_board_interface_foo(t_ttc_board_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_BOARD_INTERFACE_H
