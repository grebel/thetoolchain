/** { ttc_pwm_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for PWM device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_pwm_*() functions.
 *  Instead they can call _driver_pwm_*() pendants.
 *  E.g.: ttc_pwm_reset() -> _driver_pwm_reset()
 *
 *  Created from template ttc_device_interface.c revision 24 at 20180502 11:57:47 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_pwm_interface.h".
//
#include "ttc_pwm_interface.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

// interface will include low-driver for us. Check if at least one low-level driver could be activated
#ifndef EXTENSION_PWM_DRIVER_AVAILABLE
#  error Missing low-level driver for TTC_<DRIVER>! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _pwm_
#endif

//}Includes
/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Private Function declarations ****************************************

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Private Function declarations
//{ Function definitions *************************************************

#ifndef ttc_driver_pwm_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_pwm_deinit() (using default implementation)!

e_ttc_pwm_errorcode ttc_pwm_interface_deinit( t_ttc_pwm_config* Config ) {
    Assert_PWM_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_pwm_interface_deinit() is empty.

    return ( e_ttc_pwm_errorcode ) 0;
}

#endif
#ifndef ttc_driver_pwm_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_pwm_prepare() (using default implementation)!

void ttc_pwm_interface_prepare() {


#warning Function ttc_pwm_interface_prepare() is empty.


}

#endif
#ifndef ttc_driver_pwm_init  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_pwm_init() (using default implementation)!

e_ttc_pwm_errorcode ttc_pwm_interface_init( t_ttc_pwm_config* Config ) {
    Assert_PWM_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_pwm_interface_init() is empty.

    return ( e_ttc_pwm_errorcode ) 0;
}

#endif
#ifndef ttc_driver_pwm_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_pwm_load_defaults() (using default implementation)!

e_ttc_pwm_errorcode ttc_pwm_interface_load_defaults( t_ttc_pwm_config* Config ) {
    Assert_PWM_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_pwm_interface_load_defaults() is empty.

    return ( e_ttc_pwm_errorcode ) 0;
}

#endif
#ifndef ttc_driver_pwm_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_pwm_reset() (using default implementation)!

void ttc_pwm_interface_reset( t_ttc_pwm_config* Config ) {
    Assert_PWM_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_pwm_interface_reset() is empty.


}

#endif
#ifndef ttc_driver_pwm_configuration_check  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_pwm_configuration_check() (using default implementation)!

void ttc_pwm_interface_configuration_check( t_ttc_pwm_config* Config ) {
    Assert_PWM_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_pwm_interface_configuration_check() is empty.


}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_pwm_interface_foo(t_ttc_pwm_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
