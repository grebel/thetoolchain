/** { ttc_interrupt_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for INTERRUPT device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_interrupt_*() functions.
 *  Instead they can call _driver_interrupt_*() pendants.
 *  E.g.: ttc_interrupt_reset() -> _driver_interrupt_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20140224 21:37:20 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_interrupt_interface.h"
#include "ttc_heap.h"

/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */


#ifdef EXTENSION_ttc_usart
    A_dynamic_extern( t_ttc_interrupt_config_usart*, ttc_interrupt_config_usart );
#endif
#ifdef EXTENSION_ttc_gpio
    A_dynamic_extern( t_ttc_interrupt_config_gpio*,  ttc_interrupt_config_gpio );
#endif
#ifdef EXTENSION_ttc_rtc
    A_dynamic_extern( t_ttc_interrupt_config_rtc*,  ttc_interrupt_config_rtc );
#endif
#ifdef EXTENSION_ttc_timer
    A_dynamic_extern( t_ttc_interrupt_configimer_t*,  t_ttc_interrupt_configimer );
#endif
#ifdef EXTENSION_ttc_i2c
    A_dynamic_extern( t_ttc_interrupt_config_i2c*, ttc_interrupt_config_i2c );
#endif
#ifdef EXTENSION_ttc_can
    A_dynamic_extern( t_ttc_interrupt_config_can*, ttc_interrupt_config_can );
#endif
//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

void ttc_interrupt_interface_prepare() {

#ifdef ttc_driver_interrupt_prepare
    ttc_driver_interrupt_prepare();
#endif

}

#ifndef ttc_driver_interrupt_all_disable
void ttc_interrupt_interface_all_disable() {
#  warning Missing implementation for _driver_interrupt_all_disable()
}
#endif

#ifndef ttc_driver_interrupt_all_enable
void ttc_interrupt_interface_all_enable() {
#  warning Missing implementation for _driver_interrupt_all_enable()
}
#endif

#ifndef ttc_driver_interrupt_generate_gpio  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_generate_gpio() (using default implementation)!

void ttc_interrupt_interface_generate_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {


#warning Function ttc_interrupt_interface_generate_gpio() is empty.


}

#endif
#ifndef ttc_driver_interrupt_check_inside_isr  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_check_inside_isr() (using default implementation)!

BOOL ttc_interrupt_interface_check_inside_isr() {

    return ( BOOL ) 0;
}

#endif
#ifndef ttc_driver_interrupt_check_all_disabled  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_check_all_disabled() (using default implementation)!

BOOL ttc_interrupt_interface_check_all_disabled() {


#warning Function ttc_interrupt_interface_check_all_disabled() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

#ifdef EXTENSION_ttc_gpio //{

#ifndef ttc_driver_interrupt_enable_gpio
void ttc_interrupt_interface_enable_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {

#  warning Missing implementation for _driver_interrupt_enable_gpio()
}
#endif

#ifndef ttc_driver_interrupt_init_gpio
e_ttc_interrupt_errorcode ttc_interrupt_interface_init_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {

#  warning Missing implementation for _driver_interrupt_init_gpio()
    return ec_interrupt_NotImplemented;
}

#endif

#ifndef ttc_driver_interrupt_deinit_gpio

void ttc_interrupt_interface_deinit_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {

#  warning Missing implementation for _driver_interrupt_deinit_gpio()
}

#endif

#ifndef ttc_driver_interrupt_gpio_calc_config_index
t_u8 ttc_interrupt_interface_gpio_calc_config_index( t_base PhysicalIndex ) {

#  warning Missing implementation for _driver_interrupt_gpio_calc_config_index()
    return 0;
}
#endif
#ifndef ttc_driver_interrupt_deinit_all_gpio  // no low-level implementation: use default implementation
void ttc_interrupt_interface_deinit_all_gpio() {

    t_ttc_interrupt_config_gpio* Config_ISRs;

    for ( t_u8 Index = ttc_interrupt_config_gpio.Size; Index > 0; Index-- ) { // stm32l1xx has up to three gpios
        Config_ISRs = A( ttc_interrupt_config_gpio, Index );
        if ( Config_ISRs ) { // configuration exists: deinit all interrupts for this gpio
            ttc_driver_interrupt_deinit_gpio( Config_ISRs );
        }
    }
}

#endif
#ifndef ttc_driver_interrupt_disable_gpio  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_disable_gpio() (using default implementation)!

void ttc_interrupt_interface_disable_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif

#endif //}

#ifdef EXTENSION_ttc_timer

#ifndef ttc_driver_interrupt_init_timer
e_ttc_interrupt_errorcode ttc_interrupt_interface_init_timer( t_ttc_interrupt_configimer_t* Config_ISRs ) {

#  warning Missing implementation for _driver_interrupt_init_timer()
    return ec_interrupt_NotImplemented;
}
#endif

#ifndef ttc_driver_interrupt_enable_timer
void ttc_interrupt_interface_enable_timer( t_ttc_interrupt_configimer_t* Config_ISRs ) {
#  warning Missing implementation for _driver_interrupt_enable_timer()
}
#endif
#ifndef ttc_driver_interrupt_disable_timer  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_disable_timer() (using default implementation)!

void ttc_interrupt_interface_disable_timer( t_ttc_interrupt_configimer_t* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif
#ifndef ttc_driver_interrupt_deinit_timer  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_deinit_timer() (using default implementation)!

void ttc_interrupt_interface_deinit_timer( t_ttc_interrupt_configimer_t* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif
#ifndef ttc_driver_interrupt_deinit_all_timer  // no low-level implementation: use default implementation
void ttc_interrupt_interface_deinit_all_timer() {

    t_ttc_interrupt_configimer_t* Config_ISRs;

    for ( t_u8 Index = t_ttc_interrupt_configimer.Size; Index > 0; Index-- ) { // stm32l1xx has up to three timers
        Config_ISRs = A( t_ttc_interrupt_configimer, Index );
        if ( Config_ISRs ) { // configuration exists: deinit all interrupts for this timer
            ttc_driver_interrupt_deinit_timer( Config_ISRs );
        }
    }
}

#endif

#endif
#ifdef EXTENSION_ttc_usart

#ifndef ttc_driver_interrupt_init_usart
e_ttc_interrupt_errorcode ttc_interrupt_interface_init_usart( t_ttc_interrupt_config_usart* Config_ISRs ) {
#  warning Missing implementation for _driver_interrupt_init_usart()
    return ec_interrupt_NotImplemented;
}
#endif

#ifndef ttc_driver_interrupt_enable_usart
void ttc_interrupt_interface_enable_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type ) {
#  warning Missing implementation for _driver_interrupt_enable_usart()
}
#endif
#ifndef ttc_driver_interrupt_deinit_usart  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_deinit_usart() (using default implementation)!

void ttc_interrupt_interface_deinit_usart( t_ttc_interrupt_config_usart* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

}

#endif
#ifndef ttc_driver_interrupt_deinit_all_usart  // no low-level implementation: use default implementation
void ttc_interrupt_interface_deinit_all_usart() {

    t_ttc_interrupt_config_usart* Config_ISRs;

    for ( t_u8 Index = ttc_interrupt_config_usart.Size; Index > 0; Index-- ) { // stm32l1xx has up to three USARTs
        Config_ISRs = A( ttc_interrupt_config_usart, Index );
        if ( Config_ISRs ) { // configuration exists: deinit all interrupts for this USART
            ttc_driver_interrupt_deinit_usart( Config_ISRs );
        }
    }
}

#endif
#ifndef ttc_driver_interrupt_disable_usart  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_disable_usart() (using default implementation)!

void ttc_interrupt_interface_disable_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif

#endif
#ifdef EXTENSION_ttc_spi
#ifndef ttc_driver_interrupt_enable_spi
void ttc_interrupt_interface_enable_spi( t_ttc_interrupt_config_spi* Config_ISRs ) {
#  warning Missing implementation for _driver_interrupt_enable_spi()
}
#endif

#ifndef ttc_driver_interrupt_init_spi
e_ttc_interrupt_errorcode ttc_interrupt_interface_init_spi( t_ttc_interrupt_config_spi* Config_ISRs ) {
#  warning Missing implementation for _driver_interrupt_init_spi()
    return ec_interrupt_NotImplemented;
}
#endif
#ifndef ttc_driver_interrupt_deinit_all_spi  // no low-level implementation: use default implementation
void ttc_interrupt_interface_deinit_all_spi() {

    t_ttc_interrupt_config_spi* Config_ISRs;

    for ( t_u8 Index = ttc_interrupt_config_spi.Size; Index > 0; Index-- ) { // stm32l1xx has up to three spis
        Config_ISRs = A( ttc_interrupt_config_spi, Index );
        if ( Config_ISRs ) { // configuration exists: deinit all interrupts for this spi
            ttc_driver_interrupt_deinit_spi( Config_ISRs );
        }
    }
}

#endif
#ifndef ttc_driver_interrupt_deinit_spi  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_deinit_spi() (using default implementation)!

void ttc_interrupt_interface_deinit_spi( t_ttc_interrupt_config_spi* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif
#ifndef ttc_driver_interrupt_disable_spi  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_disable_spi() (using default implementation)!

void ttc_interrupt_interface_disable_spi( t_ttc_interrupt_config_spi* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif

#endif
#ifdef EXTENSION_ttc_i2c

#ifndef ttc_driver_interrupt_init_i2c
e_ttc_interrupt_errorcode ttc_interrupt_interface_init_i2c( t_ttc_interrupt_config_i2c* Config_ISRs ) {
#  warning Missing implementation for _driver_interrupt_init_i2c()
    return ec_interrupt_NotImplemented;
}
#endif

#ifndef ttc_driver_interrupt_enable_i2c
void ttc_interrupt_interface_enable_i2c( t_ttc_interrupt_config_i2c* Config_ISRs ) {
#  warning Missing implementation for _driver_interrupt_enable_i2c()
}
#endif
#ifndef ttc_driver_interrupt_deinit_i2c  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_deinit_i2c() (using default implementation)!

void ttc_interrupt_interface_deinit_i2c( t_ttc_interrupt_config_i2c* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif
#ifndef ttc_driver_interrupt_disable_i2c  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_disable_i2c() (using default implementation)!

void ttc_interrupt_interface_disable_i2c( t_ttc_interrupt_config_i2c* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif
#ifndef ttc_driver_interrupt_deinit_all_i2c  // no low-level implementation: use default implementation
void ttc_interrupt_interface_deinit_all_i2c() {

    t_ttc_interrupt_config_i2c* Config_ISRs;

    for ( t_u8 Index = ttc_interrupt_config_i2c.Size; Index > 0; Index-- ) { // stm32l1xx has up to three i2cs
        Config_ISRs = A( ttc_interrupt_config_i2c, Index );
        if ( Config_ISRs ) { // configuration exists: deinit all interrupts for this i2c
            ttc_driver_interrupt_deinit_i2c( Config_ISRs );
        }
    }
}

#endif

#endif
#ifdef EXTENSION_ttc_can

#ifndef ttc_driver_interrupt_enable_can
void ttc_interrupt_interface_enable_can( t_ttc_interrupt_config_can* Config_ISRs ) {
#  warning Missing implementation for _driver_interrupt_enable_can()
}
#endif

#ifndef ttc_driver_interrupt_init_can
e_ttc_interrupt_errorcode ttc_interrupt_interface_init_can( t_ttc_interrupt_config_can* Config_ISRs ) {
#  warning Missing implementation for _driver_interrupt_init_can()
    return ec_interrupt_NotImplemented;
}
#endif
#ifndef ttc_driver_interrupt_deinit_all_can  // no low-level implementation: use default implementation
void ttc_interrupt_interface_deinit_all_can() {

    t_ttc_interrupt_config_can* Config_ISRs;

    for ( t_u8 Index = ttc_interrupt_config_can.Size; Index > 0; Index-- ) { // stm32l1xx has up to three cans
        Config_ISRs = A( ttc_interrupt_config_can, Index );
        if ( Config_ISRs ) { // configuration exists: deinit all interrupts for this can
            ttc_driver_interrupt_deinit_can( Config_ISRs );
        }
    }
}

#endif
#ifndef ttc_driver_interrupt_deinit_can  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_deinit_can() (using default implementation)!

void ttc_interrupt_interface_deinit_can( t_ttc_interrupt_config_can* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

}

#endif
#ifndef ttc_driver_interrupt_disable_can  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_disable_can() (using default implementation)!

void ttc_interrupt_interface_disable_can( t_ttc_interrupt_config_can* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif

#endif



#ifdef EXTENSION_ttc_rtc

#ifndef ttc_driver_interrupt_init_rtc
e_ttc_interrupt_errorcode ttc_interrupt_interface_init_rtc( t_ttc_interrupt_config_rtc* Config_ISRs ) {
#  warning Missing implementation for _driver_interrupt_init_rtc()
    return ec_interrupt_NotImplemented;
}
#endif

#ifndef ttc_driver_interrupt_enable_rtc
void ttc_interrupt_interface_enable_rtc( t_ttc_interrupt_config_rtc* Config_ISRs ) {
#  warning Missing implementation for _driver_interrupt_enable_rtc()
}
#endif
#ifndef ttc_driver_interrupt_deinit_rtc  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_deinit_rtc() (using default implementation)!

void ttc_interrupt_interface_deinit_rtc( t_ttc_interrupt_config_rtc* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL



}

#endif
#ifndef ttc_driver_interrupt_deinit_all_rtc  // no low-level implementation: use default implementation
void ttc_interrupt_interface_deinit_all_rtc() {

    t_ttc_interrupt_config_rtc* Config_ISRs;

    for ( t_u8 Index = ttc_interrupt_config_rtc.Size; Index > 0; Index-- ) { // stm32l1xx has up to three rtcs
        Config_ISRs = A( ttc_interrupt_config_rtc, Index );
        if ( Config_ISRs ) { // configuration exists: deinit all interrupts for this rtc
            ttc_driver_interrupt_deinit_rtc( Config_ISRs );
        }
    }
}

#endif
#ifndef ttc_driver_interrupt_disable_rtc  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_interrupt_disable_rtc() (using default implementation)!

void ttc_interrupt_interface_disable_rtc( t_ttc_interrupt_config_rtc* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

}
#endif


#endif

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_interrupt_interface_foo(e_ttc_interrupt_type Type, t_physical_index PhysicalIndex) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
