/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

#include "ttc_radio_interface.h"

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_radio_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_load_defaults() (using default implementation)!

e_ttc_radio_errorcode ttc_radio_interface_load_defaults( t_ttc_radio_config* Config ) {
    Assert_RADIO( Config, ttc_assert_origin_auto );  // pointers must not be NULL

#warning Function ttc_radio_interface_load_defaults() is empty.

    return ( e_ttc_radio_errorcode ) 0;
}

#endif
#ifndef ttc_driver_radio_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_reset() (using default implementation)!

e_ttc_radio_errorcode ttc_radio_interface_reset( t_ttc_radio_config* Config ) {
    Assert_RADIO( Config, ttc_assert_origin_auto );  // pointers must not be NULL

#warning Function ttc_radio_interface_reset() is empty.

    return ( e_ttc_radio_errorcode ) 0;
}

#endif
#ifndef ttc_driver_radio_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_prepare() (using default implementation)!

void ttc_radio_interface_prepare() {


#warning Function ttc_radio_interface_prepare() is empty.


}

#endif
#ifndef ttc_driver_radio_configuration_check  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_configuration_check() (using default implementation)!

e_ttc_radio_errorcode ttc_radio_interface_configuration_check( t_ttc_radio_config* Config ) {
    Assert_RADIO( Config, ttc_assert_origin_auto );  // pointers must not be NULL

#warning Function ttc_radio_interface_configuration_check() is empty.

    return ( e_ttc_radio_errorcode ) 0;
}

#endif
#ifndef ttc_driver_radio_init  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_init() (using default implementation)!

e_ttc_radio_errorcode ttc_radio_interface_init( t_ttc_radio_config* Config ) {
    Assert_RADIO( Config, ttc_assert_origin_auto );  // pointers must not be NULL

#warning Function ttc_radio_interface_init() is empty.

    return ( e_ttc_radio_errorcode ) 0;
}

#endif
#ifndef ttc_driver_radio_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_deinit() (using default implementation)!

e_ttc_radio_errorcode ttc_radio_interface_deinit( t_ttc_radio_config* Config ) {
    Assert_RADIO( Config, ttc_assert_origin_auto );  // pointers must not be NULL

#warning Function ttc_radio_interface_deinit() is empty.

    return ( e_ttc_radio_errorcode ) 0;
}

#endif
#ifndef ttc_driver_radio_transmit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_transmit() (using default implementation)!

e_ttc_radio_errorcode ttc_radio_interface_transmit( t_ttc_radio_config* Config, u_ttc_packetimestamp_40* TransmitTime ) {
    Assert_RADIO( Config, ttc_assert_origin_auto );  // pointers must not be NULL

#warning Function ttc_radio_interface_transmit() is empty.

    return ( e_ttc_radio_errorcode ) 0;
}

#endif
#ifndef ttc_driver_radio_maintenance  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_maintenance() (using default implementation)!

void ttc_radio_interface_maintenance( t_ttc_radio_config* Config ) {
    Assert_RADIO( Config, ttc_assert_origin_auto );  // pointers must not be NULL

#warning Function ttc_radio_interface_maintenance() is empty.


}

#endif
#ifndef ttc_driver_radio_isr_receive  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_isr_receive() (using default implementation)!

void ttc_radio_interface_isr_receive( t_physical_index Index, void* Config ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_radio_interface_isr_receive() is empty.


}

#endif
#ifndef ttc_driver_radio_change_local_address  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_change_local_address() (using default implementation)!

void ttc_radio_interface_change_local_address( t_ttc_radio_config* Config, const t_ttc_packet_address* LocalAddress ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_RADIO_Writable( LocalAddress, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_radio_interface_change_local_address() is empty.


}

#endif
#ifndef ttc_driver_radio_configure_frame_filter  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_configure_frame_filter() (using default implementation)!

u_ttc_radio_frame_filter ttc_radio_interface_configure_frame_filter( t_ttc_radio_config* Config, u_ttc_radio_frame_filter Settings, BOOL ChangeFilter ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_radio_interface_configure_frame_filter() is empty.

    return ( u_ttc_radio_frame_filter ) 0;
}

#endif
#ifndef ttc_driver_radio_configure_channel_rx  // no low-level implementation: use default implementation

e_ttc_radio_errorcode ttc_radio_interface_configure_channel_rx( t_ttc_radio_config* Config, t_u8 NewChannel ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    return _driver_radio_configure_channel_rxtx( Config, NewChannel );
}

#endif
#ifndef ttc_driver_radio_configure_channel_tx  // no low-level implementation: use default implementation

e_ttc_radio_errorcode ttc_radio_interface_configure_channel_tx( t_ttc_radio_config* Config, t_u8 NewChannel ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    return _driver_radio_configure_channel_rxtx( Config, NewChannel );
}

#endif
#ifndef ttc_driver_radio_configure_channel_rxtx  // no low-level implementation: use default implementation

e_ttc_radio_errorcode ttc_radio_interface_configure_channel_rxtx( t_ttc_radio_config* Config, t_u8 NewChannel ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    e_ttc_radio_errorcode Error;

#ifdef ttc_driver_radio_configure_channel_rx
    Error = _driver_radio_configure_channel_rx( Config, NewChannel );

#  ifdef ttc_driver_radio_configure_channel_tx
    if ( !Error )
    { Error = _driver_radio_configure_channel_tx( Config, NewChannel ); }
#  endif
#else
#  ifdef ttc_driver_radio_configure_channel_tx
    Error = _driver_radio_configure_channel_tx( Config, NewChannel );
#  endif
#endif

    return Error;
}

#endif

#ifndef ttc_driver_radio_change_delay_time  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_change_delay_time() (using default implementation)!

void ttc_radio_interface_change_delay_time( t_ttc_radio_config* Config, t_base Delay_uSeconds ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_radio_interface_change_delay_time() is empty.


}

#endif
#ifndef ttc_driver_radio_update_meta  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_update_meta() (using default implementation)!

BOOL ttc_radio_interface_update_meta( t_ttc_radio_config* Config, t_ttc_packet* Packet ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_RADIO_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_radio_interface_update_meta() is empty.

    return ( BOOL ) 0;
}

#endif
#ifndef ttc_driver_radio_isr_transmit  // no low-level implementation: use default implementation

void ttc_radio_interface_isr_transmit( t_physical_index Index, t_ttc_radio_config* Config ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)


}

#endif
#ifndef ttc_driver_radio_receiver  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_receiver() (using default implementation)!

BOOL ttc_radio_interface_receiver( t_ttc_radio_config* Config, BOOL Enable, u_ttc_packetimestamp_40* ReferenceTime, t_base TimeOut_us ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_RADIO_Writable( ReferenceTime, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_radio_interface_receiver() is empty.

    return FALSE;
}

#endif
#ifndef ttc_driver_radio_configure_auto_acknowledge  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_configure_auto_acknowledge() (using default implementation)!

BOOL ttc_radio_interface_configure_auto_acknowledge( t_ttc_radio_config* Config, BOOL Enable, BOOL Change ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_radio_interface_configure_auto_acknowledge() is empty.

    return ( BOOL ) 0;
}

#endif
#ifndef ttc_driver_radio_gpio_out  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_radio_gpio_out() (using default implementation)!

BOOL ttc_radio_interface_gpio_out( t_ttc_radio_config* Config, e_ttc_radio_gpio Pin, BOOL NewState ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_radio_interface_gpio_out() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_radio_interface_foo(DEPRECATED_ttc_radio.t_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
