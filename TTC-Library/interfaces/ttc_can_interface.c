/** { ttc_can_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for CAN device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_basic_*() functions.
 *  Instead they can call _driver_basic_*() pendants.
 *  E.g.: ttc_basic_reset() -> _driver_basic_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20140324 09:30:54 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_can_interface.h"

/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_can_send  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_can_send() (using default implementation)!

t_u8 ttc_can_interface_send( t_ttc_can_config* Config ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_can_errorcode ) 0;
}

#endif
#ifndef ttc_driver_can_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_can_deinit() (using default implementation)!

e_ttc_can_errorcode ttc_can_interface_deinit( t_ttc_can_config* Config ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_can_errorcode ) 0;
}

#endif
#ifndef ttc_driver_can_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_can_prepare() (using default implementation)!

void ttc_can_interface_prepare() {




}

#endif
#ifndef ttc_driver_can_get_features  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_can_get_features() (using default implementation)!

e_ttc_can_errorcode ttc_can_interface_get_features( t_ttc_can_config* Config ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_can_errorcode ) 0;
}

#endif
#ifndef ttc_driver_can_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_can_load_defaults() (using default implementation)!

e_ttc_can_errorcode ttc_can_interface_load_defaults( t_ttc_can_config* Config ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_can_errorcode ) 0;
}

#endif
#ifndef ttc_driver_can_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_can_reset() (using default implementation)!

e_ttc_can_errorcode ttc_can_interface_reset( t_ttc_can_architecture* Architecture ) {
    Assert_CAN( Architecture, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_can_errorcode ) 0;
}

#endif
#ifndef ttc_driver_can_get_configuration  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_can_get_configuration() (using default implementation)!

t_ttc_can_config* ttc_can_interface_get_configuration( t_ttc_can_config* Config ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( t_ttc_can_config* ) 0;
}

#endif
#ifndef ttc_driver_can_init  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_can_init() (using default implementation)!

e_ttc_can_errorcode ttc_can_interface_init( t_ttc_can_config* Config ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_can_errorcode ) 0;
}

#endif
#ifndef ttc_driver_can_read  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_can_read() (using default implementation)!

e_ttc_can_errorcode ttc_can_interface_read( t_ttc_can_config* Config, t_u8* Byte ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_CAN( Byte, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_can_errorcode ) 0;
}

#endif
#ifndef ttc_driver_can_set_filter  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_can_set_filter() (using default implementation)!

void ttc_can_interface_set_filter( t_u8 FilterNum, t_ttc_can_config* Config ) {
    Assert_CAN( Filter, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_can_errorcode ) 0;
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_can_interface_foo(t_ttc_can_config* Config) { ... }

#ifndef _ttc_driver_can_tx_status  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for _ttc_can_interface_tx_status() (using default implementation)!

t_u8 _ttc_can_interface_tx_status( t_ttc_can_config* Config, t_u8 MailBox );

return ( e_ttc_can_errorcode ) 0;
}

#endif

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
