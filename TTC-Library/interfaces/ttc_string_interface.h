/** { ttc_string_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for STRING device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 22 at 20151120 13:30:25 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_STRING_INTERFACE_H
#define TTC_STRING_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_string_interface.c"
//

#include "../ttc_basic.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_string_ascii
    #include "../string/string_ascii.h"  // low-level driver for string devices on ascii architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_string_types.h"
#include "../ttc_heap_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_string_*() declaration in ../ttc_string.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_string_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_string.h for prototypes of low-level driver functions!
 *
 */

void ttc_string_interface_prepare(); //{  macro _driver_string_interface_prepare()
#ifdef ttc_driver_string_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_string_prepare() ttc_driver_string_prepare()
#else
    #define _driver_string_prepare() ttc_string_interface_prepare()
#endif
//}

//}
void ttc_string_interface_print( const t_u8* String, t_base MaxSize ); //{  macro _driver_string_interface_print(String, MaxSize)
#ifdef ttc_driver_string_print
    // enable following line to forward interface function as a macro definition
    #define _driver_string_print(String, MaxSize) ttc_driver_string_print(String, MaxSize)
#else
    #define _driver_string_print(String, MaxSize) ttc_string_interface_print(String, MaxSize)
#endif
//}
void ttc_string_interface_print_block( t_ttc_heap_block* Block ); //{  macro _driver_string_interface_print_block(Block)
#ifdef ttc_driver_string_print_block
    // enable following line to forward interface function as a macro definition
    #define _driver_string_print_block(Block) ttc_driver_string_print_block(Block)
#else
    #define _driver_string_print_block(Block) ttc_string_interface_print_block(Block)
#endif
//}
BOOL ttc_string_interface_is_whitechar( t_u8 ASCII ); //{  macro _driver_string_interface_is_whitechar(ASCII)
#ifdef ttc_driver_string_is_whitechar
    // enable following line to forward interface function as a macro definition
    #define _driver_string_is_whitechar(ASCII) ttc_driver_string_is_whitechar(ASCII)
#else
    #define _driver_string_is_whitechar(ASCII) ttc_string_interface_is_whitechar(ASCII)
#endif
//}
t_u16 ttc_string_interface_length16( const char* String, t_u16 MaxLen ); //{  macro _driver_string_interface_length16(String, MaxLen)
#ifdef ttc_driver_string_length16
    // enable following line to forward interface function as a macro definition
    #define _driver_string_length16(String, MaxLen) ttc_driver_string_length16(String, MaxLen)
#else
    #define _driver_string_length16(String, MaxLen) ttc_string_interface_length16(String, MaxLen)
#endif
//}
t_u8* ttc_string_interface_snprintf( t_u8* Buffer, t_base Size, const char* Format, ... ); //{  macro _driver_string_interface_snprintf(...)
#ifdef ttc_driver_string_snprintf
    // enable following line to forward interface function as a macro definition
    #define _driver_string_snprintf(...) ttc_driver_string_snprintf(__VA_ARGS__)
#else
    #define _driver_string_snprintf(...) ttc_string_interface_snprintf(__VA_ARGS__)
#endif
//}
t_u8* ttc_string_interface_snprintf4( t_u8* Buffer, t_base Size, const char* Format, va_list Arguments ); //{  macro _driver_string_interface_snprintf4(Buffer, Size, Format, Arguments)
#ifdef ttc_driver_string_snprintf4
    // enable following line to forward interface function as a macro definition
    #define _driver_string_snprintf4(Buffer, Size, Format, Arguments) ttc_driver_string_snprintf4(Buffer, Size, Format, Arguments)
#else
    #define _driver_string_snprintf4(Buffer, Size, Format, Arguments) ttc_string_interface_snprintf4(Buffer, Size, Format, Arguments)
#endif
//}
t_u8 ttc_string_interface_log_binary( t_base Value ); //{  macro _driver_string_interface_log_binary(Value)
#ifdef ttc_driver_string_log_binary
    // enable following line to forward interface function as a macro definition
    #define _driver_string_log_binary(Value) ttc_driver_string_log_binary(Value)
#else
    #define _driver_string_log_binary(Value) ttc_string_interface_log_binary(Value)
#endif
//}
t_u8 ttc_string_interface_log_hex( t_base Value ); //{  macro _driver_string_interface_log_hex(Value)
#ifdef ttc_driver_string_log_hex
    // enable following line to forward interface function as a macro definition
    #define _driver_string_log_hex(Value) ttc_driver_string_log_hex(Value)
#else
    #define _driver_string_log_hex(Value) ttc_string_interface_log_hex(Value)
#endif
//}
void ttc_string_interface_printf( const char* Format, ... ); //{  macro _driver_string_interface_printf(...)
#ifdef ttc_driver_string_printf
    // enable following line to forward interface function as a macro definition
    #define _driver_string_printf(...) ttc_driver_string_printf(__VA_ARGS__)
#else
    #define _driver_string_printf(...) ttc_string_interface_printf(__VA_ARGS__)
#endif
//}
t_u8 ttc_string_interface_length8( const char* String, t_u8 MaxLen ); //{  macro _driver_string_interface_length8(String, MaxLen)
#ifdef ttc_driver_string_length8
    // enable following line to forward interface function as a macro definition
    #define _driver_string_length8(String, MaxLen) ttc_driver_string_length8(String, MaxLen)
#else
    #define _driver_string_length8(String, MaxLen) ttc_string_interface_length8(String, MaxLen)
#endif
//}
t_u32 ttc_string_interface_length32( const char* String, t_u32 MaxLen ); //{  macro _driver_string_interface_length32(String, MaxLen)
#ifdef ttc_driver_string_length32
    // enable following line to forward interface function as a macro definition
    #define _driver_string_length32(String, MaxLen) ttc_driver_string_length32(String, MaxLen)
#else
    #define _driver_string_length32(String, MaxLen) ttc_string_interface_length32(String, MaxLen)
#endif
//}
t_base ttc_string_interface_copy( t_u8* Target, const t_u8* Source, t_base MaxSize ); //{  macro _driver_string_interface_copy(Target, Source, MaxSize)
#ifdef ttc_driver_string_copy
    // enable following line to forward interface function as a macro definition
    #define _driver_string_copy(Target, Source, MaxSize) ttc_driver_string_copy(Target, Source, MaxSize)
#else
    #define _driver_string_copy(Target, Source, MaxSize) ttc_string_interface_copy(Target, Source, MaxSize)
#endif
//}
t_base ttc_string_interface_appendf( t_u8* Buffer, t_base* Size, const char* Format, ... ); //{  macro _driver_string_interface_appendf(...)
#ifdef ttc_driver_string_appendf
    // enable following line to forward interface function as a macro definition
    #define _driver_string_appendf(...) ttc_driver_string_appendf(__VA_ARGS__)
#else
    #define _driver_string_appendf(...) ttc_string_interface_appendf(__VA_ARGS__)
#endif
//}
void ttc_string_interface_strip( t_u8** Start, t_base* Size ); //{  macro _driver_string_interface_strip(Start, Size)
#ifdef ttc_driver_string_strip
    // enable following line to forward interface function as a macro definition
    #define _driver_string_strip(Start, Size) ttc_driver_string_strip(Start, Size)
#else
    #define _driver_string_strip(Start, Size) ttc_string_interface_strip(Start, Size)
#endif
//}
t_u8 ttc_string_interface_log_decimal( t_base_signed Value ); //{  macro _driver_string_interface_log_decimal(Value)
#ifdef ttc_driver_string_log_decimal
    // enable following line to forward interface function as a macro definition
    #define _driver_string_log_decimal(Value) ttc_driver_string_log_decimal(Value)
#else
    #define _driver_string_log_decimal(Value) ttc_string_interface_log_decimal(Value)
#endif
//}
t_u8 ttc_string_interface_xtoa( t_base Value, t_u8* Buffer, t_base MaxSize ); //{  macro _driver_string_interface_xtoa(Value, Buffer, MaxSize)
#ifdef ttc_driver_string_xtoa
    // enable following line to forward interface function as a macro definition
    #define _driver_string_xtoa(Value, Buffer, MaxSize) ttc_driver_string_xtoa(Value, Buffer, MaxSize)
#else
    #define _driver_string_xtoa(Value, Buffer, MaxSize) ttc_string_interface_xtoa(Value, Buffer, MaxSize)
#endif
//}
t_u8 ttc_string_interface_itoa( t_base_signed Value, t_u8* Buffer, t_base MaxSize ); //{  macro _driver_string_interface_itoa(Value, Buffer, MaxSize)
#ifdef ttc_driver_string_itoa
    // enable following line to forward interface function as a macro definition
    #define _driver_string_itoa(Value, Buffer, MaxSize) ttc_driver_string_itoa(Value, Buffer, MaxSize)
#else
    #define _driver_string_itoa(Value, Buffer, MaxSize) ttc_string_interface_itoa(Value, Buffer, MaxSize)
#endif
//}
t_u8 ttc_string_interface_byte2int( t_s8 Value, t_u8* Buffer ); //{  macro _driver_string_interface_byte2int(Value, Buffer)
#ifdef ttc_driver_string_byte2int
    // enable following line to forward interface function as a macro definition
    #define _driver_string_byte2int(Value, Buffer) ttc_driver_string_byte2int(Value, Buffer)
#else
    #define _driver_string_byte2int(Value, Buffer) ttc_string_interface_byte2int(Value, Buffer)
#endif
//}
void ttc_string_interface_byte2hex( t_u8* Buffer, t_u8 Byte ); //{  macro _driver_string_interface_byte2hex(Buffer, Byte)
#ifdef ttc_driver_string_byte2hex
    // enable following line to forward interface function as a macro definition
    #define _driver_string_byte2hex(Buffer, Byte) ttc_driver_string_byte2hex(Buffer, Byte)
#else
    #define _driver_string_byte2hex(Buffer, Byte) ttc_string_interface_byte2hex(Buffer, Byte)
#endif
//}
t_u8 ttc_string_interface_itoa_unsigned( t_base Value, t_u8* Buffer, t_base MaxSize ); //{  macro _driver_string_interface_itoa_unsigned(Value, Buffer, MaxSize)
#ifdef ttc_driver_string_itoa_unsigned
    // enable following line to forward interface function as a macro definition
    #define _driver_string_itoa_unsigned(Value, Buffer, MaxSize) ttc_driver_string_itoa_unsigned(Value, Buffer, MaxSize)
#else
    #define _driver_string_itoa_unsigned(Value, Buffer, MaxSize) ttc_string_interface_itoa_unsigned(Value, Buffer, MaxSize)
#endif
//}
t_u8 ttc_string_interface_byte2uint( t_u8 Value, t_u8* Buffer ); //{  macro _driver_string_interface_byte2uint(Value, Buffer)
#ifdef ttc_driver_string_byte2uint
    // enable following line to forward interface function as a macro definition
    #define _driver_string_byte2uint(Value, Buffer) ttc_driver_string_byte2uint(Value, Buffer)
#else
    #define _driver_string_byte2uint(Value, Buffer) ttc_string_interface_byte2uint(Value, Buffer)
#endif
//}
t_base_signed ttc_string_interface_compare( const char* StringA, const char* StringB, t_base MaxLen ); //{  macro _driver_string_interface_compare(StringA, StringB, MaxLen)
#ifdef ttc_driver_string_compare
    // enable following line to forward interface function as a macro definition
    #define _driver_string_compare(StringA, StringB, MaxLen) ttc_driver_string_compare(StringA, StringB, MaxLen)
#else
    #define _driver_string_compare(StringA, StringB, MaxLen) ttc_string_interface_compare(StringA, StringB, MaxLen)
#endif
//}

t_u8 ttc_string_interface_btoa( t_base Value, t_u8* Buffer, t_base MaxSize ); //{  macro _driver_string_interface_btoa(Value, Buffer, MaxSize)
#ifdef ttc_driver_string_btoa
    // enable following line to forward interface function as a macro definition
    #define _driver_string_btoa(Value, Buffer, MaxSize) ttc_driver_string_btoa(Value, Buffer, MaxSize)
#else
    #define _driver_string_btoa(Value, Buffer, MaxSize) ttc_string_interface_btoa(Value, Buffer, MaxSize)
#endif
//}
t_u8 ttc_string_interface_ftoa( ttm_number Value, t_u8* Buffer, t_base MaxSize, t_s8 DecNumber ); //{  macro _driver_string_interface_ftoa(Value, Buffer, MaxSize)
#ifdef ttc_driver_string_ftoa
    // enable following line to forward interface function as a macro definition
    #define _driver_string_ftoa(Value, Buffer, MaxSize) ttc_driver_string_ftoa(Value, Buffer, MaxSize)
#else
    #define _driver_string_ftoa(Value, Buffer, MaxSize, DecNumber) ttc_string_interface_ftoa(Value, Buffer, MaxSize, DecNumber)
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_string_interface_foo(t_ttc_string_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_STRING_INTERFACE_H
