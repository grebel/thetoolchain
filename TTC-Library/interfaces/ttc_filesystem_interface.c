/** { ttc_filesystem_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for FILESYSTEM device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_filesystem_*() functions.
 *  Instead they can call _driver_filesystem_*() pendants.
 *  E.g.: ttc_filesystem_reset() -> _driver_filesystem_reset()
 *
 *  Created from template ttc_device_interface.c revision 25 at 20180413 11:21:12 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_filesystem_interface.h".
//
#include "ttc_filesystem_interface.h"
#include "../ttc_task.h"             // provides macro TTC_TASK_RETURN()
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

// interface will include low-driver for us. Check if at least one low-level driver could be activated
#ifndef EXTENSION_FILESYSTEM_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_<DRIVER>! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _filesystem_
#endif

//}Includes
/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Private Function declarations ****************************************

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Private Function declarations
//{ Function definitions *************************************************

#ifndef ttc_driver_filesystem_configuration_check  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_filesystem_configuration_check() (using default implementation)!

void ttc_filesystem_interface_configuration_check( t_ttc_filesystem_config* Config ) {
    Assert_FILESYSTEM_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_filesystem_interface_configuration_check() is empty.


}

#endif
#ifndef ttc_driver_filesystem_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_filesystem_deinit() (using default implementation)!

e_ttc_filesystem_errorcode ttc_filesystem_interface_deinit( t_ttc_filesystem_config* Config ) {
    Assert_FILESYSTEM_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_filesystem_interface_deinit() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_filesystem_init  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_filesystem_init() (using default implementation)!

e_ttc_filesystem_errorcode ttc_filesystem_interface_init( t_ttc_filesystem_config* Config ) {
    Assert_FILESYSTEM_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_filesystem_interface_init() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_filesystem_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_filesystem_load_defaults() (using default implementation)!

e_ttc_filesystem_errorcode ttc_filesystem_interface_load_defaults( t_ttc_filesystem_config* Config ) {
    Assert_FILESYSTEM_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_filesystem_interface_load_defaults() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_filesystem_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_filesystem_prepare() (using default implementation)!

void ttc_filesystem_interface_prepare() {


#warning Function ttc_filesystem_interface_prepare() is empty.


}

#endif
#ifndef ttc_driver_filesystem_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_filesystem_reset() (using default implementation)!

void ttc_filesystem_interface_reset( t_ttc_filesystem_config* Config ) {
    Assert_FILESYSTEM_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_filesystem_interface_reset() is empty.


}

#endif
#ifndef ttc_driver_filesystem_medium_mount  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_filesystem_medium_mount() (using default implementation)!

e_ttc_filesystem_errorcode ttc_filesystem_interface_medium_mount( t_ttc_filesystem_config* Config ) {


#warning Function ttc_filesystem_interface_medium_mount() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_filesystem_medium_unmount  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_filesystem_medium_unmount() (using default implementation)!

e_ttc_filesystem_errorcode ttc_filesystem_interface_medium_unmount( t_ttc_filesystem_config* Config ) {


#warning Function ttc_filesystem_interface_medium_unmount() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
#ifndef ttc_driver_filesystem_open_directory  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_filesystem_open_directory() (using default implementation)!

e_ttc_filesystem_errorcode ttc_filesystem_interface_open_directory( t_ttc_filesystem_config* Config, const t_u8* Directory, t_u8 DirectoryHandle ) {
    Assert_FILESYSTEM_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_FILESYSTEM_Writable( Directory, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_filesystem_interface_open_directory() is empty.

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_filesystem_interface_foo(t_ttc_filesystem_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
