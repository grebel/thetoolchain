/** { ttc_dac_interface.c ************************************************
 *
 *                           The ToolChain
 *                           
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for DAC device.
 *
 *  The functions implemented here are used when additional runtime  
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_<device>_*() functions.
 *  Instead they can call _driver_<device>_*() pendants.
 *  E.g.: ttc_<device>_reset() -> _driver_<device>_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20141215 11:13:24 UTC
 *
 *  Authors: Gregor Rebel
 *   
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_dac_interface.h".
//
#include "ttc_dac_interface.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */
 
//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_dac_init  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_dac_init() (using default implementation)!

e_ttc_dac_errorcode ttc_dac_interface_init(t_ttc_dac_config* Config) {
    Assert_DAC(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function ttc_dac_interface_init() is empty.
    
    return (e_ttc_dac_errorcode) 0;
}

#endif
#ifndef ttc_driver_dac_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_dac_prepare() (using default implementation)!

void ttc_dac_interface_prepare() {


#warning Function ttc_dac_interface_prepare() is empty.
    

}

#endif
#ifndef ttc_driver_dac_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_dac_deinit() (using default implementation)!

e_ttc_dac_errorcode ttc_dac_interface_deinit(t_ttc_dac_config* Config) {
    Assert_DAC(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function ttc_dac_interface_deinit() is empty.
    
    return (e_ttc_dac_errorcode) 0;
}

#endif
#ifndef ttc_driver_dac_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_dac_load_defaults() (using default implementation)!

e_ttc_dac_errorcode ttc_dac_interface_load_defaults(t_ttc_dac_config* Config) {
    Assert_DAC(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function ttc_dac_interface_load_defaults() is empty.
    
    return (e_ttc_dac_errorcode) 0;
}

#endif
#ifndef ttc_driver_dac_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_dac_reset() (using default implementation)!

void ttc_dac_interface_reset(t_ttc_dac_config* Config) {
    Assert_DAC(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function ttc_dac_interface_reset() is empty.
    

}

#endif
#ifndef ttc_driver_dac_get_features  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_dac_get_features() (using default implementation)!

e_ttc_dac_errorcode ttc_dac_interface_get_features(t_ttc_dac_config* Config) {
    Assert_DAC(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function ttc_dac_interface_get_features() is empty.
    
    return (e_ttc_dac_errorcode) 0;
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_dac_interface_foo(t_ttc_dac_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
