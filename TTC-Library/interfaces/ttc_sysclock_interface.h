/** { ttc_sysclock_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for SYSCLOCK device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 20 at 20140218 17:32:51 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_SYSCLOCK_INTERFACE_H
#define TTC_SYSCLOCK_INTERFACE_H

//{ Includes *************************************************************

#include "../ttc_basic.h"

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_sysclock_stm32f1xx
    #include "../sysclock/sysclock_stm32f1xx.h"
#endif
#ifdef EXTENSION_sysclock_stm32w1xx
    #include "../sysclock/sysclock_stm32w1xx.h"  // low-level driver for sysclock devices on stm32w1xx architecture
#endif
#ifdef EXTENSION_sysclock_stm32l1xx
    #include "../sysclock/sysclock_stm32l1xx.h"  // low-level driver for sysclock devices on stm32l1xx architecture
#endif
#ifdef EXTENSION_sysclock_stm32l0xx
    #include "../sysclock/sysclock_stm32l0xx.h"  // low-level driver for sysclock devices on stm32l0xx architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_sysclock_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_sysclock_*() declaration in ../ttc_sysclock.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_sysclock_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_sysclock.h for prototypes of low-level driver functions!
 *
 */

void ttc_sysclock_interface_prepare( t_ttc_sysclock_config* Config ); //{  macro _driver_sysclock_interface_prepare()
#ifdef ttc_driver_sysclock_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_sysclock_prepare(Config) ttc_driver_sysclock_prepare(Config)
#else
    #define _driver_sysclock_prepare(Config) ttc_sysclock_interface_prepare(Config)
    #  warning: missing low-level implementation for ttc_driver_sysclock_prepare()!
#endif
//}
void ttc_sysclock_interface_reset( t_ttc_sysclock_config* Config ); //{  macro _driver_sysclock_interface_reset()
#ifdef ttc_driver_sysclock_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_sysclock_reset(Config) ttc_driver_sysclock_reset(Config)
#else
    #define _driver_sysclock_reset(Config) ttc_sysclock_interface_reset(Config)
    #  warning: missing low-level implementation for ttc_driver_sysclock_reset()!
#endif
//}
void ttc_sysclock_interface_profile_switch( t_ttc_sysclock_config* Config ); //{  macro _driver_sysclock_interface_profile_switch(Config)
#ifdef ttc_driver_sysclock_profile_switch
    // enable following line to forward interface function as a macro definition
    #define _driver_sysclock_profile_switch(Config, NewProfile) ttc_driver_sysclock_profile_switch(Config, NewProfile)
#else
    #define _driver_sysclock_profile_switch(Config) ttc_sysclock_interface_profile_switch(Config)
    #  warning: missing low-level implementation for ttc_driver_sysclock_profile_switch()!
#endif
//}
void ttc_sysclock_interface_udelay( t_ttc_sysclock_config* Config, t_base Microseconds ); //{  macro _driver_sysclock_interface_udelay(Microseconds)
#ifdef ttc_driver_sysclock_udelay
    // enable following line to forward interface function as a macro definition
    #define _driver_sysclock_udelay(Config, Microseconds) ttc_driver_sysclock_udelay(Config, Microseconds)
#else
    #define _driver_sysclock_udelay(Config, Microseconds) ttc_sysclock_interface_udelay(Config, Microseconds)
#endif
//}
void ttc_sysclock_interface_update( t_ttc_sysclock_config* Config ); //{  macro _driver_sysclock_interface_update(Config)
#ifdef ttc_driver_sysclock_update
    // enable following line to forward interface function as a macro definition
    #define _driver_sysclock_update(Config) ttc_driver_sysclock_update(Config)
#else
    #define _driver_sysclock_update(Config) ttc_sysclock_interface_update(Config)
#endif
//}
e_ttc_sysclock_errorcode ttc_sysclock_interface_enable_oscillator( e_ttc_sysclock_oscillator Oscillator, BOOL On ); //{  macro _driver_sysclock_interface_enable_oscillator(Oscillator, On)
#ifdef ttc_driver_sysclock_enable_oscillator
    // enable following line to forward interface function as a macro definition
    #define _driver_sysclock_enable_oscillator(Oscillator, On) ttc_driver_sysclock_enable_oscillator(Oscillator, On)
#else
    #define _driver_sysclock_enable_oscillator(Oscillator, On) ttc_sysclock_interface_enable_oscillator(Oscillator, On)
#endif
//}
t_base* ttc_sysclock_interface_frequency_get_all(); //{  macro _driver_sysclock_interface_frequency_get_all()
#ifdef ttc_driver_sysclock_frequency_get_all
    // enable following line to forward interface function as a macro definition
    #define _driver_sysclock_frequency_get_all ttc_driver_sysclock_frequency_get_all
#else
    #define _driver_sysclock_frequency_get_all ttc_sysclock_interface_frequency_get_all
#endif
//}
void ttc_sysclock_interface_frequency_set( t_ttc_sysclock_config* Config, t_base NewFrequency ); //{  macro _driver_sysclock_interface_frequency_set(Config, NewFrequency)
#ifdef ttc_driver_sysclock_frequency_set
    // enable following line to forward interface function as a macro definition
    #define _driver_sysclock_frequency_set ttc_driver_sysclock_frequency_set
#else
    #define _driver_sysclock_frequency_set ttc_sysclock_interface_frequency_set
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_sysclock_interface_foo(t_ttc_sysclock_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_SYSCLOCK_INTERFACE_H
