/** { ttc_sdcard_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for SDCARD device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 24 at 20180130 09:42:26 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_SDCARD_INTERFACE_H
#define TTC_SDCARD_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_sdcard_interface.c"
//

#include "../ttc_basic.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_sdcard_spi
    #include "../sdcard/sdcard_spi.h"  // low-level driver for sdcard devices on spi architecture
#endif
#ifdef EXTENSION_sdcard_spi
    #include "../sdcard/sdcard_spi.h"  // low-level driver for sdcard devices on spi architecture
#endif
#ifdef EXTENSION_sdcard_spi
    #include "../sdcard/sdcard_spi.h"  // low-level driver for sdcard devices on spi architecture
#endif
#ifdef EXTENSION_sdcard_spi
    #include "../sdcard/sdcard_spi.h"  // low-level driver for sdcard devices on spi architecture
#endif
#ifdef EXTENSION_sdcard_spi
    #include "../sdcard/sdcard_spi.h"  // low-level driver for sdcard devices on spi architecture
#endif
#ifdef EXTENSION_sdcard_spi
    #include "../sdcard/sdcard_spi.h"  // low-level driver for sdcard devices on spi architecture
#endif
#ifdef EXTENSION_sdcard_spi
    #include "../sdcard/sdcard_spi.h"  // low-level driver for sdcard devices on spi architecture
#endif
#ifdef EXTENSION_sdcard_spi
    #include "../sdcard/sdcard_spi.h"  // low-level driver for sdcard devices on spi architecture
#endif
#ifdef EXTENSION_sdcard_spi
    #include "../sdcard/sdcard_spi.h"  // low-level driver for sdcard devices on spi architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

// Check if at least one low-level driver could be activated
#ifndef EXTENSION_SDCARD_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_SDCARD! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _sdcard_
#endif

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_sdcard_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_sdcard_*() declaration in ../ttc_sdcard.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_sdcard_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_sdcard.h for prototypes of low-level driver functions!
 *
 */

void ttc_sdcard_interface_reset( t_ttc_sdcard_config* Config ); //{  macro _driver_sdcard_interface_reset(Config)
#ifdef ttc_driver_sdcard_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_reset ttc_driver_sdcard_reset
#else
    #define _driver_sdcard_reset ttc_sdcard_interface_reset
#endif
//}
void ttc_sdcard_interface_configuration_check( t_ttc_sdcard_config* Config ); //{  macro _driver_sdcard_interface_configuration_check(Config)
#ifdef ttc_driver_sdcard_configuration_check
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_configuration_check ttc_driver_sdcard_configuration_check
#else
    #define _driver_sdcard_configuration_check ttc_sdcard_interface_configuration_check
#endif
//}
e_ttc_sdcard_errorcode ttc_sdcard_interface_load_defaults( t_ttc_sdcard_config* Config ); //{  macro _driver_sdcard_interface_load_defaults(Config)
#ifdef ttc_driver_sdcard_load_defaults
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_load_defaults ttc_driver_sdcard_load_defaults
#else
    #define _driver_sdcard_load_defaults ttc_sdcard_interface_load_defaults
#endif
//}
e_ttc_sdcard_errorcode ttc_sdcard_interface_init( t_ttc_sdcard_config* Config ); //{  macro _driver_sdcard_interface_init(Config)
#ifdef ttc_driver_sdcard_init
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_init ttc_driver_sdcard_init
#else
    #define _driver_sdcard_init ttc_sdcard_interface_init
#endif
//}
e_ttc_sdcard_errorcode ttc_sdcard_interface_deinit( t_ttc_sdcard_config* Config ); //{  macro _driver_sdcard_interface_deinit(Config)
#ifdef ttc_driver_sdcard_deinit
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_deinit ttc_driver_sdcard_deinit
#else
    #define _driver_sdcard_deinit ttc_sdcard_interface_deinit
#endif
//}
void ttc_sdcard_interface_prepare(); //{  macro _driver_sdcard_interface_prepare()
#ifdef ttc_driver_sdcard_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_prepare ttc_driver_sdcard_prepare
#else
    #define _driver_sdcard_prepare ttc_sdcard_interface_prepare
#endif
//}
e_ttc_storage_event ttc_sdcard_interface_detect( t_ttc_sdcard_config* Config ); //{  macro _driver_sdcard_interface_detect(Config)
#ifdef ttc_driver_sdcard_medium_detect
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_medium_detect ttc_driver_sdcard_medium_detect
#else
    #define _driver_sdcard_medium_detect ttc_sdcard_interface_detect
#endif
//}
u_ttc_sdcard_response ttc_sdcard_interface_command_r1( t_ttc_sdcard_config* Config, const t_ttc_sdcard_command_argument* CommandArgument ); //{  macro _driver_sdcard_interface_command_r1(Config, Command, Argument)
#ifdef ttc_driver_sdcard_command_r1
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_command_r1 ttc_driver_sdcard_command_r1
#else
    #define _driver_sdcard_command_r1 ttc_sdcard_interface_command_r1
#endif
//}
e_ttc_sdcard_errorcode ttc_sdcard_interface_medium_go_idle( t_ttc_sdcard_config* Config ); //{  macro _driver_sdcard_interface_medium_go_idle(Config)
#ifdef ttc_driver_sdcard_medium_go_idle
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_medium_go_idle ttc_driver_sdcard_medium_go_idle
#else
    #define _driver_sdcard_medium_go_idle ttc_sdcard_interface_medium_go_idle
#endif
//}
void ttc_sdcard_interface_medium_unmount( t_ttc_sdcard_config* Config ); //{  macro _driver_sdcard_interface_medium_unmount(Config)
#ifdef ttc_driver_sdcard_medium_unmount
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_medium_unmount ttc_driver_sdcard_medium_unmount
#else
    #define _driver_sdcard_medium_unmount ttc_sdcard_interface_medium_unmount
#endif
//}
e_ttc_storage_event ttc_sdcard_interface_medium_detect( t_ttc_sdcard_config* Config ); //{  macro _driver_sdcard_interface_medium_detect(Config)
#ifdef ttc_driver_sdcard_medium_detect
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_medium_detect ttc_driver_sdcard_medium_detect
#else
    #define _driver_sdcard_medium_detect ttc_sdcard_interface_medium_detect
#endif
//}
t_u16 ttc_sdcard_interface_command_rn( t_ttc_sdcard_config* Config, const t_ttc_sdcard_command_argument* CommandArgument, t_u8* Buffer, t_u16 Amount ); //{  macro _driver_sdcard_interface_command_rn(Config, CommandArgument, Buffer, Amount)
#ifdef ttc_driver_sdcard_command_rn
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_command_rn ttc_driver_sdcard_command_rn
#else
    #define _driver_sdcard_command_rn ttc_sdcard_interface_command_rn
#endif
//}
e_ttc_sdcard_errorcode ttc_sdcard_interface_set_speed( t_ttc_sdcard_config* Config, e_ttc_sdcard_speed InterfaceSpeed ); //{  macro _driver_sdcard_interface_set_speed(Config, InterfaceSpeed)
#ifdef ttc_driver_sdcard_set_speed
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_set_speed ttc_driver_sdcard_set_speed
#else
    #define _driver_sdcard_set_speed ttc_sdcard_interface_set_speed
#endif
//}
e_ttc_sdcard_errorcode ttc_sdcard_interface_block_read( t_ttc_sdcard_config* Config, t_u32 BlockAddress, t_u8* Buffer, t_u16 BufferSize ); //{  macro _driver_sdcard_interface_block_read(Config, Address, Buffer, BufferSize)
#ifdef ttc_driver_sdcard_block_read
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_block_read ttc_driver_sdcard_block_read
#else
    #define _driver_sdcard_block_read ttc_sdcard_interface_block_read
#endif
//}
e_ttc_sdcard_errorcode ttc_sdcard_interface_block_write( t_ttc_sdcard_config* Config, t_u32 BlockAddress, const t_u8* Buffer, t_u16 BufferSize ); //{  macro _driver_sdcard_interface_block_write(Config, Address, Buffer, BufferSize)
#ifdef ttc_driver_sdcard_block_write
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_block_write ttc_driver_sdcard_block_write
#else
    #define _driver_sdcard_block_write ttc_sdcard_interface_block_write
#endif
//}
BOOL ttc_sdcard_interface_wait_busy( t_ttc_sdcard_config* Config ); //{  macro _driver_sdcard_interface_wait_busy(Config)
#ifdef ttc_driver_sdcard_wait_busy
    // enable following line to forward interface function as a macro definition
    #define _driver_sdcard_wait_busy ttc_driver_sdcard_wait_busy
#else
    #define _driver_sdcard_wait_busy ttc_sdcard_interface_wait_busy
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_sdcard_interface_foo(t_ttc_sdcard_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_SDCARD_INTERFACE_H
