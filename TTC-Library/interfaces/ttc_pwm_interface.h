/** { ttc_pwm_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for PWM device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 24 at 20180502 11:57:47 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_PWM_INTERFACE_H
#define TTC_PWM_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_pwm_interface.c"
//

#include "../ttc_basic.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_pwm_stm32l1xx
#  include "../pwm/pwm_stm32l1xx.h"  // low-level driver for pwm devices on stm32l1xx architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

// Check if at least one low-level driver could be activated
#ifndef EXTENSION_PWM_DRIVER_AVAILABLE
#  error Missing low-level driver for TTC_PWM! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _pwm_
#endif

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_pwm_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_pwm_*() declaration in ../ttc_pwm.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_pwm_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_pwm.h for prototypes of low-level driver functions!
 *
 */

e_ttc_pwm_errorcode ttc_pwm_interface_deinit( t_ttc_pwm_config* Config ); //{  macro _driver_pwm_interface_deinit(Config)
#ifdef ttc_driver_pwm_deinit
// enable following line to forward interface function as a macro definition
#  define _driver_pwm_deinit ttc_driver_pwm_deinit
#else
#  define _driver_pwm_deinit ttc_pwm_interface_deinit
#endif
//}
void ttc_pwm_interface_prepare(); //{  macro _driver_pwm_interface_prepare()
#ifdef ttc_driver_pwm_prepare
// enable following line to forward interface function as a macro definition
#  define _driver_pwm_prepare ttc_driver_pwm_prepare
#else
#  define _driver_pwm_prepare ttc_pwm_interface_prepare
#endif
//}
e_ttc_pwm_errorcode ttc_pwm_interface_init( t_ttc_pwm_config* Config ); //{  macro _driver_pwm_interface_init(Config)
#ifdef ttc_driver_pwm_init
// enable following line to forward interface function as a macro definition
#  define _driver_pwm_init ttc_driver_pwm_init
#else
#  define _driver_pwm_init ttc_pwm_interface_init
#endif
//}
e_ttc_pwm_errorcode ttc_pwm_interface_load_defaults( t_ttc_pwm_config* Config ); //{  macro _driver_pwm_interface_load_defaults(Config)
#ifdef ttc_driver_pwm_load_defaults
// enable following line to forward interface function as a macro definition
#  define _driver_pwm_load_defaults ttc_driver_pwm_load_defaults
#else
#  define _driver_pwm_load_defaults ttc_pwm_interface_load_defaults
#endif
//}
void ttc_pwm_interface_reset( t_ttc_pwm_config* Config ); //{  macro _driver_pwm_interface_reset(Config)
#ifdef ttc_driver_pwm_reset
// enable following line to forward interface function as a macro definition
#  define _driver_pwm_reset ttc_driver_pwm_reset
#else
#  define _driver_pwm_reset ttc_pwm_interface_reset
#endif
//}
void ttc_pwm_interface_configuration_check( t_ttc_pwm_config* Config ); //{  macro _driver_pwm_interface_configuration_check(Config)
#ifdef ttc_driver_pwm_configuration_check
// enable following line to forward interface function as a macro definition
#  define _driver_pwm_configuration_check ttc_driver_pwm_configuration_check
#else
#  define _driver_pwm_configuration_check ttc_pwm_interface_configuration_check
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_pwm_interface_foo(t_ttc_pwm_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_PWM_INTERFACE_H
