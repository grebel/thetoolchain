/** { ttc_gfx_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for GFX device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_gfx_*() functions.
 *  Instead they can call _driver_gfx_*() pendants.
 *  E.g.: ttc_gfx_reset() -> _driver_gfx_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20140313 10:58:25 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_gfx_interface.h"
#include "../ttc_math.h"

/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */
// Using global variable from ttc_gfx.c for quick access to current display reduces amount of function arguments and pointer derefencing
extern t_ttc_gfx_quick ttc_gfx_Quick;

typedef struct {
    t_u16  BitLine;
    t_u8   LineNo;
    t_u8   RowNo;
    t_u8   CharWidth;
    t_u16  PhysicalX;
    t_u16  PhysicalY;
    const  t_u16* Bitmap;
} t_ttc_gfx_interface_data;

static t_ttc_gfx_interface_data ttc_gfx_interface_Data;

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_gfx_init  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_init() (using default implementation)!

e_ttc_gfx_errorcode ttc_gfx_interface_init( t_ttc_gfx_config* Config ) {
    Assert_GFX( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_deinit() (using default implementation)!

e_ttc_gfx_errorcode ttc_gfx_interface_deinit( t_ttc_gfx_config* Config ) {
    Assert_GFX( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_get_features  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_get_features() (using default implementation)!

e_ttc_gfx_errorcode ttc_gfx_interface_get_features( t_ttc_gfx_config* Config ) {
    Assert_GFX( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_reset() (using default implementation)!

e_ttc_gfx_errorcode ttc_gfx_interface_reset( t_ttc_gfx_config* Config ) {
    Assert_GFX( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_load_defaults() (using default implementation)!

e_ttc_gfx_errorcode ttc_gfx_interface_load_defaults( t_ttc_gfx_config* Config ) {
    Assert_GFX( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_prepare() (using default implementation)!

void ttc_gfx_interface_prepare() {




}

#endif
#ifndef ttc_driver_gfx_color_bg24  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_color_bg24() (using default implementation)!

void ttc_gfx_interface_color_bg24( t_u32 Color ) {



    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_backlight  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_backlight() (using default implementation)!

void ttc_gfx_interface_backlight( t_u8 Level ) {



    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_color_fg_palette  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_color_fg_palette() (using default implementation)!

void ttc_gfx_interface_color_fg_palette( t_u8 PaletteIndex ) {



    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_color_fg16  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_color_fg16() (using default implementation)!

void ttc_gfx_interface_color_fg16( t_u16 Color ) {



    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_cursor_set  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_cursor_set() (using default implementation)!

void ttc_gfx_interface_cursor_set( t_u16 X, t_u16 Y ) {



    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_color_bg16  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_color_bg16() (using default implementation)!

void ttc_gfx_interface_color_bg16( t_u16 Color ) {



    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_color_bg_palette  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_color_bg_palette() (using default implementation)!

void ttc_gfx_interface_color_bg_palette( t_u8 PaletteIndex ) {



    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_palette_set  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_palette_set() (using default implementation)!

void ttc_gfx_interface_palette_set( t_u16* Palette, t_u8 Size ) {
    Assert_GFX( Palette, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_color_fg24  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_color_fg24() (using default implementation)!

void ttc_gfx_interface_color_fg24( t_u32 Color ) {



    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_pixel_set  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_pixel_set() (using default implementation)!
1
void ttc_gfx_interface_pixel_set_at( t_u16 X, t_u16 Y ) {



    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_clear  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_clear() (using default implementation)!

void ttc_gfx_interface_clear( t_u16 Width, t_u16 Height ) {



    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_pixel_set  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_pixel_set() (using default implementation)!

void ttc_gfx_interface_pixel_set() {



    return ( e_ttc_gfx_errorcode ) 0;
}

#endif

#ifndef ttc_driver_gfx_line  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_line() (using default implementation)!

void ttc_gfx_interface_line( t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2 ) {



    return ( e_ttc_gfx_errorcode ) 0; 1
}

#endif
#ifndef ttc_driver_gfx_rect  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_rect() (using default implementation)!

void ttc_gfx_interface_rect( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height ) {

    ttc_driver_gfx_line_horizontal( X, Y, Width );
    ttc_driver_gfx_line_vertical( X, Y, Height );
    ttc_driver_gfx_line_vertical( X + Width, Y, Height );
    ttc_driver_gfx_line_horizontal( X, Y + Height, Width );

    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_line_vertical  // no low-level implementation: use default implementation
void ttc_gfx_interface_line_vertical( t_u16 X, t_u16 Y, t_s16 Length ) {

    Assert_GFX_EXTRA( Length  > 0, ttc_assert_origin_auto ); // calling function must ensure optimized values. Call ttc_gfx_rect_fill() or optimize your arguments!
    Assert_GFX_EXTRA( X          < ttc_gfx_Quick.Width, ttc_assert_origin_auto ); // calling function must ensure optimized values. Call ttc_gfx_rect_fill() or optimize your arguments!
    Assert_GFX_EXTRA( Y + Length < ttc_gfx_Quick.Height, ttc_assert_origin_auto ); // calling function must ensure optimized values. Call ttc_gfx_rect_fill() or optimize your arguments!

    while ( Length-- ) {
        _driver_gfx_pixel_set_at( X, Y++ );
    }
}

#endif
#ifndef ttc_driver_gfx_circle  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_circle() (using default implementation)!

void ttc_gfx_interface_circle( t_u16 CenterX, t_u16 CenterY, t_u16 Radius ) {



    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_pixel_clr_at  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_pixel_clr() (using default implementation)!

void ttc_gfx_interface_pixel_clr() {



    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_line_horizontal  // no low-level implementation: use default implementation
void ttc_gfx_interface_line_horizontal( t_u16 X, t_u16 Y, t_s16 Length ) {
    Assert_GFX_EXTRA( Length  > 0, ttc_assert_origin_auto ); // calling function must ensure optimized values. Call ttc_gfx_rect_fill() or optimize your arguments!
    Assert_GFX_EXTRA( X + Length < ttc_gfx_Quick.Width, ttc_assert_origin_auto ); // calling function must ensure optimized values. Call ttc_gfx_rect_fill() or optimize your arguments!
    Assert_GFX_EXTRA( Y < ttc_gfx_Quick.Height, ttc_assert_origin_auto ); // calling function must ensure optimized values. Call ttc_gfx_rect_fill() or optimize your arguments!

    while ( Length-- ) {
        _driver_gfx_pixel_set_at( X++, Y );
    }
}

#endif
#ifndef ttc_driver_gfx_rect_fill  // no low-level implementation: use default implementation
void ttc_gfx_interface_rect_fill( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height ) {
    Assert_GFX_EXTRA( Width  > 0, ttc_assert_origin_auto ); // calling function must ensure optimized values. Call ttc_gfx_rect_fill() or optimize your arguments!
    Assert_GFX_EXTRA( Height > 0, ttc_assert_origin_auto ); // calling function must ensure optimized values. Call ttc_gfx_rect_fill() or optimize your arguments!
    Assert_GFX_EXTRA( X < ttc_gfx_Quick.Width, ttc_assert_origin_auto ); // calling function must ensure optimized values. Call ttc_gfx_rect_fill() or optimize your arguments!
    Assert_GFX_EXTRA( Y < ttc_gfx_Quick.Height, ttc_assert_origin_auto ); // calling function must ensure optimized values. Call ttc_gfx_rect_fill() or optimize your arguments!

    if ( Height < Width ) {  // use horizontal lines to reduce loop count
        ttc_gfx_interface_Data.PhysicalY = Y + Height;
        while ( ttc_gfx_interface_Data.PhysicalY > Y ) {
            ttc_gfx_interface_Data.PhysicalY--;
            _driver_gfx_line_horizontal( X, ttc_gfx_interface_Data.PhysicalY, Width );
        }
    }
    else {                  // use vertical lines to reduce loop count
        ttc_gfx_interface_Data.PhysicalX = X + Width;
        while ( ttc_gfx_interface_Data.PhysicalX > X ) {
            ttc_gfx_interface_Data.PhysicalX--;
            _driver_gfx_line_vertical( ttc_gfx_interface_Data.PhysicalX, Y, Height );
        }
    }
}
#endif
#ifndef ttc_driver_gfx_set_font  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_set_font() (using default implementation)!

void ttc_gfx_interface_set_font( const t_ttc_font_data* Font ) {
    Assert_GFX( Font, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_circle_fill  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_circle_fill() (using default implementation)!

void ttc_gfx_interface_circle_fill( t_u16 CenterX, t_u16 CenterY, t_u16 Radius ) {



    return ( e_ttc_gfx_errorcode ) 0;
}

#endif
#ifndef ttc_driver_gfx_char  // no low-level implementation: use default implementation
void ttc_gfx_interface_char( t_u8 Char ) {
    Char -= ttc_gfx_Quick.Font->FirstASCII;
    ttc_gfx_interface_Data.PhysicalY = ttc_gfx_Quick.Config->PhysicalY;
    ttc_gfx_interface_Data.CharWidth = ttc_gfx_Quick.Font->CharWidth;
    ttc_gfx_interface_Data.LineNo    = ttc_gfx_Quick.Font->CharHeight;
    ttc_gfx_interface_Data.Bitmap    = ttc_gfx_Quick.Font->Characters + ( Char ) * ttc_gfx_interface_Data.LineNo;

    for ( ; ttc_gfx_interface_Data.LineNo > 0; ttc_gfx_interface_Data.LineNo-- ) {
        ttc_gfx_interface_Data.BitLine = *ttc_gfx_interface_Data.Bitmap++;

        if ( ttc_gfx_interface_Data.BitLine ) {
            ttc_gfx_interface_Data.PhysicalX = ttc_gfx_Quick.Config->PhysicalX;
            do {
                if ( ttc_gfx_interface_Data.BitLine & 1 ) {
                    ttc_driver_gfx_pixel_set_at( ttc_gfx_interface_Data.PhysicalX, ttc_gfx_interface_Data.PhysicalY );
                }
                ttc_gfx_interface_Data.PhysicalX++;
                ttc_gfx_interface_Data.BitLine /= 2;
            }
            while ( ttc_gfx_interface_Data.BitLine );
        }
        ttc_gfx_interface_Data.PhysicalY++;
    }
}

#endif
#ifndef ttc_driver_gfx_char_090  // no low-level implementation: use default implementation
void ttc_gfx_interface_char_090( t_u8 Char ) {
    Char -= ttc_gfx_Quick.Font->FirstASCII;
    ttc_gfx_interface_Data.PhysicalX = ttc_gfx_Quick.Config->PhysicalX;
    ttc_gfx_interface_Data.CharWidth = ttc_gfx_Quick.Font->CharWidth;
    ttc_gfx_interface_Data.LineNo    = ttc_gfx_Quick.Font->CharHeight;
    ttc_gfx_interface_Data.Bitmap    = ttc_gfx_Quick.Font->Characters + ( Char ) * ttc_gfx_interface_Data.LineNo;

    for ( ; ttc_gfx_interface_Data.LineNo > 0; ttc_gfx_interface_Data.LineNo-- ) {
        ttc_gfx_interface_Data.BitLine = *ttc_gfx_interface_Data.Bitmap++;

        if ( ttc_gfx_interface_Data.BitLine ) {
            ttc_gfx_interface_Data.PhysicalY = ttc_gfx_Quick.Config->PhysicalY;
            do {
                if ( ttc_gfx_interface_Data.BitLine & 1 ) {
                    ttc_driver_gfx_pixel_set_at( ttc_gfx_interface_Data.PhysicalX, ttc_gfx_interface_Data.PhysicalY );
                }
                ttc_gfx_interface_Data.PhysicalY--;
                ttc_gfx_interface_Data.BitLine /= 2;
            }
            while ( ttc_gfx_interface_Data.BitLine );
        }
        ttc_gfx_interface_Data.PhysicalX++;
    }
}

#endif
#ifndef ttc_driver_gfx_char_270  // no low-level implementation: use default implementation
void ttc_gfx_interface_char_270( t_u8 Char ) {
    Char -= ttc_gfx_Quick.Font->FirstASCII;
    ttc_gfx_interface_Data.PhysicalX = ttc_gfx_Quick.Config->PhysicalX;
    ttc_gfx_interface_Data.CharWidth = ttc_gfx_Quick.Font->CharWidth;
    ttc_gfx_interface_Data.LineNo    = ttc_gfx_Quick.Font->CharHeight;
    ttc_gfx_interface_Data.Bitmap    = ttc_gfx_Quick.Font->Characters + ( Char ) * ttc_gfx_interface_Data.LineNo;

    for ( ; ttc_gfx_interface_Data.LineNo > 0; ttc_gfx_interface_Data.LineNo-- ) {
        ttc_gfx_interface_Data.BitLine = *ttc_gfx_interface_Data.Bitmap++;

        if ( ttc_gfx_interface_Data.BitLine ) {
            ttc_gfx_interface_Data.PhysicalY = ttc_gfx_Quick.Config->PhysicalY;
            do {
                if ( ttc_gfx_interface_Data.BitLine & 1 ) {
                    ttc_driver_gfx_pixel_set_at( ttc_gfx_interface_Data.PhysicalX, ttc_gfx_interface_Data.PhysicalY );
                }
                ttc_gfx_interface_Data.PhysicalY++;
                ttc_gfx_interface_Data.BitLine /= 2;
            }
            while ( ttc_gfx_interface_Data.BitLine );
        }
        ttc_gfx_interface_Data.PhysicalX--;
    }
}

#endif
#ifndef ttc_driver_gfx_char_180  // no low-level implementation: use default implementation
void ttc_gfx_interface_char_180( t_u8 Char ) {
    Char -= ttc_gfx_Quick.Font->FirstASCII;
    ttc_gfx_interface_Data.PhysicalY = ttc_gfx_Quick.Config->PhysicalY;
    ttc_gfx_interface_Data.CharWidth = ttc_gfx_Quick.Font->CharWidth;
    ttc_gfx_interface_Data.LineNo    = ttc_gfx_Quick.Font->CharHeight;
    ttc_gfx_interface_Data.Bitmap    = ttc_gfx_Quick.Font->Characters + ( Char ) * ttc_gfx_interface_Data.LineNo;

    for ( ; ttc_gfx_interface_Data.LineNo > 0; ttc_gfx_interface_Data.LineNo-- ) {
        ttc_gfx_interface_Data.BitLine = *ttc_gfx_interface_Data.Bitmap++;

        if ( ttc_gfx_interface_Data.BitLine ) {
            ttc_gfx_interface_Data.PhysicalX = ttc_gfx_Quick.Config->PhysicalX;
            do {
                if ( ttc_gfx_interface_Data.BitLine & 1 ) {
                    ttc_driver_gfx_pixel_set_at( ttc_gfx_interface_Data.PhysicalX, ttc_gfx_interface_Data.PhysicalY );
                }
                ttc_gfx_interface_Data.PhysicalX--;
                ttc_gfx_interface_Data.BitLine /= 2;
            }
            while ( ttc_gfx_interface_Data.BitLine );
        }
        ttc_gfx_interface_Data.PhysicalY--;
    }
}

#endif
#ifndef ttc_driver_gfx_char_solid  // no low-level implementation: use default implementation
void ttc_gfx_interface_char_solid( t_u8 Char ) {
    Char -= ttc_gfx_Quick.Font->FirstASCII;
    ttc_gfx_interface_Data.PhysicalY = ttc_gfx_Quick.Config->PhysicalY;
    ttc_gfx_interface_Data.CharWidth = ttc_gfx_Quick.Font->CharWidth;
    ttc_gfx_interface_Data.LineNo    = ttc_gfx_Quick.Font->CharHeight;
    ttc_gfx_interface_Data.Bitmap    = ttc_gfx_Quick.Font->Characters + ( Char ) * ttc_gfx_interface_Data.LineNo;

    for ( ; ttc_gfx_interface_Data.LineNo > 0; ttc_gfx_interface_Data.LineNo-- ) {
        ttc_gfx_interface_Data.BitLine = *ttc_gfx_interface_Data.Bitmap++;

        ttc_gfx_interface_Data.PhysicalX = ttc_gfx_Quick.Config->PhysicalX;
        for ( ttc_gfx_interface_Data.RowNo = ttc_gfx_interface_Data.CharWidth; ttc_gfx_interface_Data.RowNo > 0; ttc_gfx_interface_Data.RowNo-- ) {
            if ( ttc_gfx_interface_Data.BitLine & 1 ) {
                ttc_driver_gfx_pixel_set_at( ttc_gfx_interface_Data.PhysicalX, ttc_gfx_interface_Data.PhysicalY );
            }
            else {
                ttc_driver_gfx_pixel_clr_at( ttc_gfx_interface_Data.PhysicalX, ttc_gfx_interface_Data.PhysicalY );
            }
            ttc_gfx_interface_Data.PhysicalX++;
            ttc_gfx_interface_Data.BitLine /= 2;
        };
        ttc_gfx_interface_Data.PhysicalY++;
    }
}

#endif
#ifndef ttc_driver_gfx_char_solid_090  // no low-level implementation: use default implementation
void ttc_gfx_interface_char_solid_090( t_u8 Char ) {
    Char -= ttc_gfx_Quick.Font->FirstASCII;
    ttc_gfx_interface_Data.PhysicalX = ttc_gfx_Quick.Config->PhysicalX;
    ttc_gfx_interface_Data.CharWidth = ttc_gfx_Quick.Font->CharWidth;
    ttc_gfx_interface_Data.LineNo    = ttc_gfx_Quick.Font->CharHeight;
    ttc_gfx_interface_Data.Bitmap    = ttc_gfx_Quick.Font->Characters + ( Char ) * ttc_gfx_interface_Data.LineNo;

    for ( ; ttc_gfx_interface_Data.LineNo > 0; ttc_gfx_interface_Data.LineNo-- ) {
        ttc_gfx_interface_Data.BitLine = *ttc_gfx_interface_Data.Bitmap++;

        ttc_gfx_interface_Data.PhysicalY = ttc_gfx_Quick.Config->PhysicalY;
        for ( ttc_gfx_interface_Data.RowNo = ttc_gfx_interface_Data.CharWidth; ttc_gfx_interface_Data.RowNo > 0; ttc_gfx_interface_Data.RowNo-- ) {
            if ( ttc_gfx_interface_Data.BitLine & 1 ) {
                ttc_driver_gfx_pixel_set_at( ttc_gfx_interface_Data.PhysicalX, ttc_gfx_interface_Data.PhysicalY );
            }
            else {
                ttc_driver_gfx_pixel_clr_at( ttc_gfx_interface_Data.PhysicalX, ttc_gfx_interface_Data.PhysicalY );
            }
            ttc_gfx_interface_Data.PhysicalY--;
            ttc_gfx_interface_Data.BitLine /= 2;
        };
        ttc_gfx_interface_Data.PhysicalX++;
    }
}

#endif
#ifndef ttc_driver_gfx_char_solid_180  // no low-level implementation: use default implementation
void ttc_gfx_interface_char_solid_180( t_u8 Char ) {
    Char -= ttc_gfx_Quick.Font->FirstASCII;
    ttc_gfx_interface_Data.PhysicalY = ttc_gfx_Quick.Config->PhysicalY;
    ttc_gfx_interface_Data.CharWidth = ttc_gfx_Quick.Font->CharWidth;
    ttc_gfx_interface_Data.LineNo    = ttc_gfx_Quick.Font->CharHeight;
    ttc_gfx_interface_Data.Bitmap    = ttc_gfx_Quick.Font->Characters + ( Char ) * ttc_gfx_interface_Data.LineNo;

    for ( ; ttc_gfx_interface_Data.LineNo > 0; ttc_gfx_interface_Data.LineNo-- ) {
        ttc_gfx_interface_Data.BitLine = *ttc_gfx_interface_Data.Bitmap++;

        ttc_gfx_interface_Data.PhysicalX = ttc_gfx_Quick.Config->PhysicalX;
        for ( ttc_gfx_interface_Data.RowNo = ttc_gfx_interface_Data.CharWidth; ttc_gfx_interface_Data.RowNo > 0; ttc_gfx_interface_Data.RowNo-- ) {
            if ( ttc_gfx_interface_Data.BitLine & 1 ) {
                ttc_driver_gfx_pixel_set_at( ttc_gfx_interface_Data.PhysicalX, ttc_gfx_interface_Data.PhysicalY );
            }
            else {
                ttc_driver_gfx_pixel_clr_at( ttc_gfx_interface_Data.PhysicalX, ttc_gfx_interface_Data.PhysicalY );
            }
            ttc_gfx_interface_Data.PhysicalX--;
            ttc_gfx_interface_Data.BitLine /= 2;
        };
        ttc_gfx_interface_Data.PhysicalY--;
    }
}

#endif
#ifndef ttc_driver_gfx_char_solid_270  // no low-level implementation: use default implementation
void ttc_gfx_interface_char_solid_270( t_u8 Char ) {
    Char -= ttc_gfx_Quick.Font->FirstASCII;
    ttc_gfx_interface_Data.PhysicalX = ttc_gfx_Quick.Config->PhysicalX;
    ttc_gfx_interface_Data.CharWidth = ttc_gfx_Quick.Font->CharWidth;
    ttc_gfx_interface_Data.LineNo    = ttc_gfx_Quick.Font->CharHeight;
    ttc_gfx_interface_Data.Bitmap    = ttc_gfx_Quick.Font->Characters + ( Char ) * ttc_gfx_interface_Data.LineNo;

    for ( ; ttc_gfx_interface_Data.LineNo > 0; ttc_gfx_interface_Data.LineNo-- ) {
        ttc_gfx_interface_Data.BitLine = *ttc_gfx_interface_Data.Bitmap++;

        ttc_gfx_interface_Data.PhysicalY = ttc_gfx_Quick.Config->PhysicalY;
        for ( ttc_gfx_interface_Data.RowNo = ttc_gfx_interface_Data.CharWidth; ttc_gfx_interface_Data.RowNo > 0; ttc_gfx_interface_Data.RowNo-- ) {
            if ( ttc_gfx_interface_Data.BitLine & 1 ) {
                ttc_driver_gfx_pixel_set_at( ttc_gfx_interface_Data.PhysicalX, ttc_gfx_interface_Data.PhysicalY );
            }
            else {
                ttc_driver_gfx_pixel_clr_at( ttc_gfx_interface_Data.PhysicalX, ttc_gfx_interface_Data.PhysicalY );
            }
            ttc_gfx_interface_Data.PhysicalY++;
            ttc_gfx_interface_Data.BitLine /= 2;
        };
        ttc_gfx_interface_Data.PhysicalX--;
    }
}

#endif
#ifndef ttc_driver_gfx_pixel_set_at  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_pixel_set() (using default implementation)!

void ttc_gfx_interface_pixel_set_at( t_u16 X, t_u16 Y ) {


#warning Function ttc_gfx_interface_pixel_set() is empty.


}

#endif
#ifndef ttc_driver_gfx_pixel_clr_at  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_pixel_clr() (using default implementation)!

void ttc_gfx_interface_pixel_clr_at( t_u16 X, t_u16 Y ) {


#warning Function ttc_gfx_interface_pixel_clr() is empty.


}

#endif
#ifndef ttc_driver_gfx_rect_090  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_rect_090() (using default implementation)!

void ttc_gfx_interface_rect_090( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height ) {


#warning Function ttc_gfx_interface_rect_090() is empty.


}

#endif
#ifndef ttc_driver_gfx_rect_180  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_rect_180() (using default implementation)!

void ttc_gfx_interface_rect_180( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height ) {


#warning Function ttc_gfx_interface_rect_180() is empty.


}

#endif
#ifndef ttc_driver_gfx_rect_270  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_rect_270() (using default implementation)!

void ttc_gfx_interface_rect_270( t_u16 X, t_u16 Y, t_s16 Width, t_s16 Height ) {


#warning Function ttc_gfx_interface_rect_270() is empty.


}

#endif
#ifndef ttc_driver_gfx_line_horizontal_090  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_line_horizontal_090() (using default implementation)!

void ttc_gfx_interface_line_horizontal_090( t_u16 X, t_u16 Y, t_s16 Length ) {


#warning Function ttc_gfx_interface_line_horizontal_090() is empty.


}

#endif
#ifndef ttc_driver_gfx_line_vertical_090  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_line_vertical_090() (using default implementation)!

void ttc_gfx_interface_line_vertical_090( t_u16 X, t_u16 Y, t_s16 Length ) {


#warning Function ttc_gfx_interface_line_vertical_090() is empty.


}

#endif
#ifndef ttc_driver_gfx_arrow  // no low-level implementation: use default implementation
void ttc_gfx_interface_arrow( t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2, t_u8 Length1, t_u8 Length2 ) {

    ttc_driver_gfx_line( X1, Y1, X2, Y2 );

    // angle between line and each arrow line
    const ttm_number HalfArrowAngle = 15.0 * PI / 180; // 15° in radiant

    // absolute line direction
    static ttm_number tga_Direction = 0;
    tga_Direction = ttc_math_vector2d_angle( X2 - X1, Y2 - Y1 );

    // x- and y-distance of arrow line
    static ttm_number tga_dX, tga_dY;

    if ( Length1 ) { // draw arrow at X1,Y1
        ttc_math_polar2cartesian( tga_Direction + HalfArrowAngle, Length1, &tga_dX, &tga_dY );
        ttc_driver_gfx_line( X1, Y1, X1 + tga_dX, Y1 + tga_dY );
        ttc_math_polar2cartesian( tga_Direction - HalfArrowAngle, Length1, &tga_dX, &tga_dY );
        ttc_driver_gfx_line( X1, Y1, X1 + tga_dX, Y1 + tga_dY );
    }
    if ( Length2 ) { // draw arrow at X2,Y2
        tga_Direction = ttc_math_angle_reduce( tga_Direction + PI ); // rotate 180° to reverse direction
        ttc_math_polar2cartesian( tga_Direction + HalfArrowAngle, Length2, &tga_dX, &tga_dY );
        ttc_driver_gfx_line( X2, Y2, X2 + tga_dX, Y2 + tga_dY );
        ttc_math_polar2cartesian( tga_Direction - HalfArrowAngle, Length2, &tga_dX, &tga_dY );
        ttc_driver_gfx_line( X2, Y2, X2 + tga_dX, Y2 + tga_dY );
    }
}

#endif
#ifndef ttc_driver_gfx_circle_segment  // no low-level implementation: use default implementation

void ttc_gfx_interface_circle_segment( t_u16 CenterX, t_u16 CenterY, t_u16 Radius, t_u16 StartAngle, t_u16 EndAngle ) {

    TODO( "Function ttc_gfx_interface_circle_segment() is empty." );
}

#endif
#ifndef ttc_driver_gfx_circle_segment_fill  // no low-level implementation: use default implementation
void ttc_gfx_interface_circle_segment_fill( t_u16 CenterX, t_u16 CenterY, t_u16 Radius, t_u16 StartAngle, t_u16 EndAngle ) {

    TODO( "Function ttc_gfx_interface_circle_segment_fill() is empty." );
}

#endif
#ifndef ttc_driver_gfx_select  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_select() (using default implementation)!

void ttc_gfx_interface_select( t_ttc_gfx_config* Config ) {
    Assert_GFX_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_gfx_interface_select() is empty.


}

#endif
#ifndef ttc_driver_gfx_deselect  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_gfx_deselect() (using default implementation)!

void ttc_gfx_interface_deselect( t_ttc_gfx_config* Config ) {
    Assert_GFX_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function ttc_gfx_interface_deselect() is empty.


}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_gfx_interface_foo(t_ttc_gfx_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
