/** { ttc_i2c_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for I2C device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 24 at 20180131 15:13:24 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_I2C_INTERFACE_H
#define TTC_I2C_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_i2c_interface.c"
//

#include "../ttc_basic.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_i2c_stm32l1xx
#  include "../i2c/i2c_stm32l1xx.h"  // low-level driver for i2c devices on stm32l1xx architecture
#endif
#ifdef EXTENSION_i2c_stm32f1xx
#  include "../i2c/i2c_stm32f1xx.h"  // low-level driver for i2c devices on stm32f1xx architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

// Check if at least one low-level driver could be activated
#ifndef EXTENSION_I2C_DRIVER_AVAILABLE
#  error Missing low-level driver for TTC_I2C! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _i2c_
#endif

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_i2c_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_i2c_*() declaration in ../ttc_i2c.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_i2c_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_i2c.h for prototypes of low-level driver functions!
 *
 */

e_ttc_i2c_errorcode ttc_i2c_interface_load_defaults( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_load_defaults(Config)
#ifdef ttc_driver_i2c_load_defaults
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_load_defaults ttc_driver_i2c_load_defaults
#else
#  define _driver_i2c_load_defaults ttc_i2c_interface_load_defaults
#endif
//}
BOOL ttc_i2c_interface_check_flag( volatile t_ttc_i2c_base_register* BaseRegister, e_ttc_i2c_flag_code FlagCode ); //{  macro _driver_i2c_interface_check_flag(BaseRegister, FlagCode)
#ifdef ttc_driver_i2c_check_flag
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_check_flag ttc_driver_i2c_check_flag
#else
#  define _driver_i2c_check_flag ttc_i2c_interface_check_flag
#endif
//}
t_base ttc_i2c_interface_get_event_value( volatile t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_get_event_value(BaseRegister)
#ifdef ttc_driver_i2c_get_event_value
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_get_event_value ttc_driver_i2c_get_event_value
#else
#  define _driver_i2c_get_event_value ttc_i2c_interface_get_event_value
#endif
//}
t_u8 ttc_i2c_interface_slave_read_bytes( t_ttc_i2c_config* Config, t_u8 BufferSize, t_u8* Buffer ); //{  macro _driver_i2c_interface_slave_read_bytes(Config, BufferSize, Buffer)
#ifdef ttc_driver_i2c_slave_read_bytes
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_slave_read_bytes ttc_driver_i2c_slave_read_bytes
#else
#  define _driver_i2c_slave_read_bytes ttc_i2c_interface_slave_read_bytes
#endif
//}
t_ttc_i2c_config* ttc_i2c_interface_get_features( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_get_features(Config)
#ifdef ttc_driver_i2c_get_features
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_get_features ttc_driver_i2c_get_features
#else
#  define _driver_i2c_get_features ttc_i2c_interface_get_features
#endif
//}
t_u8 ttc_i2c_interface_slave_send_bytes( t_ttc_i2c_config* Config, t_u8 BufferSize, const t_u8* Buffer ); //{  macro _driver_i2c_interface_slave_send_bytes(Config, BufferSize, Buffer)
#ifdef ttc_driver_i2c_slave_send_bytes
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_slave_send_bytes ttc_driver_i2c_slave_send_bytes
#else
#  define _driver_i2c_slave_send_bytes ttc_i2c_interface_slave_send_bytes
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_master_send_slave_address( t_ttc_i2c_config* Config, t_u16 SlaveAddress, BOOL ReadMode ); //{  macro _driver_i2c_interface_master_send_slave_address(Config, SlaveAddress, ReadMode)
#ifdef ttc_driver_i2c_master_send_slave_address
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_master_send_slave_address ttc_driver_i2c_master_send_slave_address
#else
#  define _driver_i2c_master_send_slave_address ttc_i2c_interface_master_send_slave_address
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_master_condition_stop( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_master_condition_stop(Config)
#ifdef ttc_driver_i2c_master_condition_stop
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_master_condition_stop ttc_driver_i2c_master_condition_stop
#else
#  define _driver_i2c_master_condition_stop ttc_i2c_interface_master_condition_stop
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_master_condition_start( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_master_condition_start(Config)
#ifdef ttc_driver_i2c_master_condition_start
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_master_condition_start ttc_driver_i2c_master_condition_start
#else
#  define _driver_i2c_master_condition_start ttc_i2c_interface_master_condition_start
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_deinit( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_deinit(Config)
#ifdef ttc_driver_i2c_deinit
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_deinit ttc_driver_i2c_deinit
#else
#  define _driver_i2c_deinit ttc_i2c_interface_deinit
#endif
//}
BOOL ttc_i2c_interface_slave_check_own_address_received( volatile t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_slave_check_own_address_received(BaseRegister)
#ifdef ttc_driver_i2c_slave_check_own_address_received
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_slave_check_own_address_received ttc_driver_i2c_slave_check_own_address_received
#else
#  define _driver_i2c_slave_check_own_address_received ttc_i2c_interface_slave_check_own_address_received
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_slave_reset_bus( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_slave_reset_bus(Config)
#ifdef ttc_driver_i2c_slave_reset_bus
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_slave_reset_bus ttc_driver_i2c_slave_reset_bus
#else
#  define _driver_i2c_slave_reset_bus ttc_i2c_interface_slave_reset_bus
#endif
//}
void ttc_i2c_interface_prepare(); //{  macro _driver_i2c_interface_prepare()
#ifdef ttc_driver_i2c_prepare
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_prepare ttc_driver_i2c_prepare
#else
#  define _driver_i2c_prepare ttc_i2c_interface_prepare
#endif
//}
void ttc_i2c_interface_reset( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_reset(Config)
#ifdef ttc_driver_i2c_reset
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_reset ttc_driver_i2c_reset
#else
#  define _driver_i2c_reset ttc_i2c_interface_reset
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_init( t_ttc_i2c_config* Config ); //{  macro _driver_i2c_interface_init(Config)
#ifdef ttc_driver_i2c_init
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_init ttc_driver_i2c_init
#else
#  define _driver_i2c_init ttc_i2c_interface_init
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_master_send_bytes( t_ttc_i2c_config* Config, t_u8 Amount, const t_u8* Buffer ); //{  macro _driver_i2c_interface_master_send_bytes(Config, Amount, Buffer)
#ifdef ttc_driver_i2c_master_send_bytes
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_master_send_bytes ttc_driver_i2c_master_send_bytes
#else
#  define _driver_i2c_master_send_bytes ttc_i2c_interface_master_send_bytes
#endif
//}
BOOL ttc_i2c_interface_slave_check_read_mode( volatile t_ttc_i2c_base_register* BaseRegister ); //{  macro _driver_i2c_interface_slave_check_read_mode(BaseRegister)
#ifdef ttc_driver_i2c_slave_check_read_mode
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_slave_check_read_mode ttc_driver_i2c_slave_check_read_mode
#else
#  define _driver_i2c_slave_check_read_mode ttc_i2c_interface_slave_check_read_mode
#endif
//}
void ttc_i2c_interface_enable_acknowledge( volatile t_ttc_i2c_base_register* BaseRegister, BOOL Enable ); //{  macro _driver_i2c_interface_enable_acknowledge(BaseRegister, Enable)
#ifdef ttc_driver_i2c_enable_acknowledge
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_enable_acknowledge ttc_driver_i2c_enable_acknowledge
#else
#  define _driver_i2c_enable_acknowledge ttc_i2c_interface_enable_acknowledge
#endif
//}
e_ttc_i2c_errorcode ttc_i2c_interface_master_read_bytes( t_ttc_i2c_config* Config, t_u8 Amount, t_u8* Buffer ); //{  macro _driver_i2c_interface_master_read_bytes(Config, Amount, Buffer)
#ifdef ttc_driver_i2c_master_read_bytes
// enable following line to forward interface function as a macro definition
#  define _driver_i2c_master_read_bytes ttc_driver_i2c_master_read_bytes
#else
#  define _driver_i2c_master_read_bytes ttc_i2c_interface_master_read_bytes
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_i2c_interface_foo(t_ttc_i2c_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_I2C_INTERFACE_H
