/** { ttc_ethernet_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for ETHERNET device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 24 at 20180130 11:09:47 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_ETHERNET_INTERFACE_H
#define TTC_ETHERNET_INTERFACE_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_ethernet_interface.c"
//

#include "../ttc_basic.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_ethernet_ste101p
    #include "../ethernet/ethernet_ste101p.h"  // low-level driver for ethernet devices on ste101p architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

// Check if at least one low-level driver could be activated
#ifndef EXTENSION_ETHERNET_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_ETHERNET! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _ethernet_
#endif

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_ethernet_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_ethernet_*() declaration in ../ttc_ethernet.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_ethernet_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_ethernet.h for prototypes of low-level driver functions!
 *
 */

void ttc_ethernet_interface_reset( t_ttc_ethernet_config* Config ); //{  macro _driver_ethernet_interface_reset(Config)
#ifdef ttc_driver_ethernet_reset
    // enable following line to forward interface function as a macro definition
    #define _driver_ethernet_reset ttc_driver_ethernet_reset
#else
    #define _driver_ethernet_reset ttc_ethernet_interface_reset
#endif
//}
void ttc_ethernet_interface_configuration_check( t_ttc_ethernet_config* Config ); //{  macro _driver_ethernet_interface_configuration_check(Config)
#ifdef ttc_driver_ethernet_configuration_check
    // enable following line to forward interface function as a macro definition
    #define _driver_ethernet_configuration_check ttc_driver_ethernet_configuration_check
#else
    #define _driver_ethernet_configuration_check ttc_ethernet_interface_configuration_check
#endif
//}
e_ttc_ethernet_errorcode ttc_ethernet_interface_load_defaults( t_ttc_ethernet_config* Config ); //{  macro _driver_ethernet_interface_load_defaults(Config)
#ifdef ttc_driver_ethernet_load_defaults
    // enable following line to forward interface function as a macro definition
    #define _driver_ethernet_load_defaults ttc_driver_ethernet_load_defaults
#else
    #define _driver_ethernet_load_defaults ttc_ethernet_interface_load_defaults
#endif
//}
void ttc_ethernet_interface_prepare(); //{  macro _driver_ethernet_interface_prepare()
#ifdef ttc_driver_ethernet_prepare
    // enable following line to forward interface function as a macro definition
    #define _driver_ethernet_prepare ttc_driver_ethernet_prepare
#else
    #define _driver_ethernet_prepare ttc_ethernet_interface_prepare
#endif
//}
e_ttc_ethernet_errorcode ttc_ethernet_interface_init( t_ttc_ethernet_config* Config ); //{  macro _driver_ethernet_interface_init(Config)
#ifdef ttc_driver_ethernet_init
    // enable following line to forward interface function as a macro definition
    #define _driver_ethernet_init ttc_driver_ethernet_init
#else
    #define _driver_ethernet_init ttc_ethernet_interface_init
#endif
//}
e_ttc_ethernet_errorcode ttc_ethernet_interface_deinit( t_ttc_ethernet_config* Config ); //{  macro _driver_ethernet_interface_deinit(Config)
#ifdef ttc_driver_ethernet_deinit
    // enable following line to forward interface function as a macro definition
    #define _driver_ethernet_deinit ttc_driver_ethernet_deinit
#else
    #define _driver_ethernet_deinit ttc_ethernet_interface_deinit
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_ethernet_interface_foo(t_ttc_ethernet_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_ETHERNET_INTERFACE_H
