/** { ttc_slam_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for SLAM device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_slam_*() functions.
 *  Instead they can call _driver_slam_*() pendants.
 *  E.g.: ttc_slam_reset() -> _driver_slam_reset()
 *
 *  Created from template ttc_device_interface.c revision 23 at 20160606 11:59:52 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_slam_interface.h".
//
#include "ttc_slam_interface.h"
#include "../ttc_math.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

// interface will include low-driver for us. Check if at least one low-level driver could be activated
#ifndef EXTENSION_SLAM_DRIVER_AVAILABLE
    #  error Missing low-level driver for TTC_<DRIVER>! Did you forget to provide a feature? Check your activation stage: ./clean.sh ; ./activate_project.sh 2>&1 | grep _slam_
#endif

//}Includes
/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Private Function declarations ****************************************

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Private Function declarations
//{ Function definitions *************************************************

#ifndef ttc_driver_slam_deinit  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_slam_deinit() (using default implementation)!

e_ttc_slam_errorcode ttc_slam_interface_deinit( t_ttc_slam_config* Config ) {
    Assert_SLAM_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_slam_interface_deinit() is empty.

    return ( e_ttc_slam_errorcode ) 0;
}

#endif
#ifndef ttc_driver_slam_reset  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_slam_reset() (using default implementation)!

void ttc_slam_interface_reset( t_ttc_slam_config* Config ) {
    Assert_SLAM_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_slam_interface_reset() is empty.


}

#endif
#ifndef ttc_driver_slam_prepare  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_slam_prepare() (using default implementation)!

void ttc_slam_interface_prepare() {


#warning Function ttc_slam_interface_prepare() is empty.


}

#endif
#ifndef ttc_driver_slam_configuration_check  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_slam_configuration_check() (using default implementation)!

void ttc_slam_interface_configuration_check( t_ttc_slam_config* Config ) {
    Assert_SLAM_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_slam_interface_configuration_check() is empty.


}

#endif
#ifndef ttc_driver_slam_load_defaults  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_slam_load_defaults() (using default implementation)!

e_ttc_slam_errorcode ttc_slam_interface_load_defaults( t_ttc_slam_config* Config ) {
    Assert_SLAM_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_slam_interface_load_defaults() is empty.

    return ( e_ttc_slam_errorcode ) 0;
}

#endif
#ifndef ttc_driver_slam_init  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_slam_init() (using default implementation)!

e_ttc_slam_errorcode ttc_slam_interface_init( t_ttc_slam_config* Config ) {
    Assert_SLAM_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_slam_interface_init() is empty.

    return ( e_ttc_slam_errorcode ) 0;
}

#endif
#ifndef ttc_driver_slam_update_distance  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_slam_update_distance() (using default implementation)!

void ttc_slam_interface_update_distance( t_ttc_slam_config* Config, t_u16 NodeIndexA, t_u16 NodeIndexB, ttm_number RawValue ) {
    Assert_SLAM_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_slam_interface_update_distance() is empty.


}

#endif
#ifndef ttc_driver_slam_get_node  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_slam_get_node() (using default implementation)!

t_ttc_slam_node* ttc_slam_interface_get_node( t_ttc_slam_config* Config, t_u16 NodeIndex ) {
    Assert_SLAM_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_slam_interface_get_node() is empty.

    return ( t_ttc_slam_node* ) 0;
}

#endif
#ifndef ttc_driver_slam_update_coordinates  // no low-level implementation: use default implementation

void ttc_slam_interface_update_coordinates( t_ttc_slam_config* Config, t_u16 NodeIndex, ttm_number X, ttm_number Y, ttm_number Z ) {
    Assert_SLAM_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_ttc_slam_node* SlamNode = slam_simple_2d_get_node( Config, NodeIndex ); // will check if NodeIndex is valid

    if ( 1 ) { // simple implementation without filtering: just store given values
        t_u8 PositionValid = 1;
        if ( X != TTC_SLAM_UNDEFINED_VALUE ) {
            SlamNode->Position.X = X;
        }
        else
        { PositionValid = 0; }
        if ( Y != TTC_SLAM_UNDEFINED_VALUE )
        { SlamNode->Position.Y = Y; }
        else
        { PositionValid = 0; }
        if ( Z != TTC_SLAM_UNDEFINED_VALUE )
        { SlamNode->Position.Z = Z; }
        else
        { PositionValid = 0; }

        SlamNode->Flags.PositionValid = PositionValid;
        if ( PositionValid ) {
            Assert_SLAM( *( ( t_u32* )( &SlamNode->Position.X ) ) != 0x400000, ttc_assert_origin_auto ); // different type of not a number (check how to handle it!)
            Assert_SLAM( *( ( t_u32* )( &SlamNode->Position.Y ) ) != 0x400000, ttc_assert_origin_auto ); // different type of not a number (check how to handle it!)
            Assert_SLAM( *( ( t_u32* )( &SlamNode->Position.Z ) ) != 0x400000, ttc_assert_origin_auto ); // different type of not a number (check how to handle it!)
        }
        /* DEPRECATED
        if ( !SlamNode->Flags.PositionValid ) { // check if position is valid now
            if (
                ( SlamNode->Position.X != TTC_SLAM_UNDEFINED_VALUE ) &&
                ( SlamNode->Position.Y != TTC_SLAM_UNDEFINED_VALUE ) &&
                ( SlamNode->Position.Z != TTC_SLAM_UNDEFINED_VALUE )
            ) {
                Assert_SLAM( *( ( t_u32* ) &X ) != 0x400000, ttc_assert_origin_auto ); // different type of not a number (check how to handle it!)
                Assert_SLAM( *( ( t_u32* ) &Y ) != 0x400000, ttc_assert_origin_auto ); // different type of not a number (check how to handle it!)
                Assert_SLAM( *( ( t_u32* ) &Z ) != 0x400000, ttc_assert_origin_auto ); // different type of not a number (check how to handle it!)

                SlamNode->Flags.PositionValid = 1;
            }
            else
                SlamNode->Flags.PositionValid = 0;
        }
        */
    }
}


#endif
#ifndef ttc_driver_slam_calculate_mapping  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_slam_calculate_mapping() (using default implementation)!

BOOL ttc_slam_interface_calculate_mapping( t_ttc_slam_config* Config ) {
    Assert_SLAM_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function ttc_slam_interface_calculate_mapping() is empty.

    return ( BOOL ) 0;
}

#endif
#ifndef ttc_driver_slam_calculate_distance  // no low-level implementation: use default implementation

ttm_number ttc_slam_interface_calculate_distance( t_ttc_slam_config* Config, t_u16 NodeIndexA, t_u16 NodeIndexB ) {
    Assert_SLAM_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    if ( NodeIndexA == NodeIndexB )
    { return 0; }

    t_ttc_slam_node* NodeA = ttc_driver_slam_get_node( Config, NodeIndexA ); // will check if NodeIndex is valid
    t_ttc_slam_node* NodeB = ttc_driver_slam_get_node( Config, NodeIndexB ); // will check if NodeIndex is valid

    ttm_number Distance;
    if ( NodeA->Flags.PositionValid && NodeB->Flags.PositionValid ) {

        Distance = ttc_math_length_3d( ttc_math_distance( NodeA->Position.X, NodeB->Position.X ),
                                       ttc_math_distance( NodeA->Position.Y, NodeB->Position.Y ),
                                       ttc_math_distance( NodeA->Position.Z, NodeB->Position.Z )
                                     );
    }
    else
    { Distance = TTC_SLAM_UNDEFINED_VALUE; } // either A or B has incomplete position

    return Distance; // coordinates of one node not yet calculated!
}

#endif
#ifndef ttc_driver_slam_get_distance  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_slam_get_distance() (using default implementation)!

ttm_number ttc_slam_interface_get_distance( t_ttc_slam_config* Config, t_u16 NodeIndexA, t_u16 NodeIndexB ) {
    Assert_SLAM_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)


    return ( ttm_number ) TTC_SLAM_UNDEFINED_VALUE;
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_slam_interface_foo(t_ttc_slam_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
