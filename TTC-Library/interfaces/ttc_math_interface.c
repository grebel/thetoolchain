/** { ttc_math_interface.c ************************************************
*
*                           The ToolChain
*
*  The ToolChain has been originally developed by Gregor Rebel 2010 - 2015.
*
*  Interface layer between high -  and low - level driver for MATH device.
*
*  The functions implemented here are used when additional runtime
*  computations arerequired to call the corresponding low - level driver.
*  These computations should be held very simple and fast. Every additional
*
*  Adding a new low - level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_math_*() functions.
 *  Instead they can call _driver_math_*() pendants.
 *  E.g.: ttc_math_reset() -> _driver_math_reset()
 *
*  Created from template ttc_device_interface.c revision 20 at 20150724 12:18:05 UTC
*
*  Authors: Gregor Rebel
*
}*/

// { Includes *************************************************************
//
// C - Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_math_interface.h".
//
#include "ttc_math_interface.h"
#include "../ttc_memory.h"
#include "../ttc_heap.h"
// InsertIncludes above (DO NOT REMOVE THIS LINE!)


// }Includes
/** Global Variables ****************************************************{
*
* Interfaces ideally do not use any extra memory.
* Try to use macros and definitions instead.
*
*/

extern e_ttc_math_precision ttc_math_Precision;
extern ttm_number           ttc_math_PrecisionValue;

// InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

// } Global Variables
// { Function definitions *************************************************

#ifndef ttc_driver_math_prepare  //  no low - level implementation: use default implementation

void ttc_math_interface_prepare() {

    // nothing to prepare
}

#endif
#ifndef ttc_driver_math_arcsin  // no low-level implementation: use default implementation

ttm_number ttc_math_interface_arcsin( ttm_number X ) {
    Assert_MATH( abs( X ) <= 1, ttc_assert_origin_auto );  // invalid argument given. Check implementation of caller!
    ttm_number ArcSin;

    if ( 0 ) { // calculate via Taylor Series (->https://de.wikipedia.org/wiki/Arkussinus_und_Arkuskosinus)
        /* Benchmark results (source code: example_ttc_math.c)
         *
         * Compiler: arm-none-eabi-gcc 5.3.1 20160307 (release)
         *
         * Reported time was measured for 1000 function calls including for-loop and divided by 1000.
         *
         * + STM32F107 @72MHz (Protoboard Olimex STM32-H107)
         *   + Low-Level Driver: math_software_float
         *     - Precision = +-0.1  => Loops =   12:   240us  (gcc -O0)
         *     - Precision = +-0.1  => Loops =   12:   236us  (gcc -Os)
         *     - Precision = +-0.01 => Loops =  128:  2760us  (gcc -O0)
         *   + Low-Level Driver: math_software_double
         *     - Precision = +-0.1  => Loops =   12:  3300us  (gcc -O0)
         *     - Precision = +-0.1  => Loops =   12:  3200us  (gcc -Os)
         *     - Precision = +-0.01 => Loops =  128:  3720us  (gcc -O0)
         *     - Precision = +-0.01 => Loops =  128:  3600us  (gcc -Os)
         */

        t_u16 Loops; // amount of loop runs for different
        switch ( ttc_math_Precision ) {
            case tmp_10m:   Loops = 128;  break;
            case tmp_100m:  Loops = 12;   break;
            default:        Loops = 0;    break;
        }

        ArcSin = X;
        ttm_number X2 = X * X; // X² is used several times

        // load parameters for first loop iteration
        ttm_number A = 0.5;
        ttm_number X_K = X2 * X; // X³
        t_u8 K = 3;

        while ( Loops-- > 0 ) {
            ArcSin += A * X_K / K;

            A *= 1.0 * K / ( K + 1 );
            X_K *= X2;
            K += 2;
        }
    }
    else {     // calculate via Olli's approximation (-> http://www.olliw.eu/2014/fast-functions/#asin)
        /* Benchmark results (source code: example_ttc_math.c 'benchmark arcus sinus')
         *
         * Compiler: arm-none-eabi-gcc 5.3.1 20160307 (release)
         *
         * Reported time was measured for 1000 function calls including for-loop and divided by 1000.
         *
         * + STM32F107 @72MHz (Protoboard Olimex STM32-H107)
         *   + Low-Level Driver: math_software_float
         *     - Precision = +-0.001:  32.7us (gcc -O0)
         *     - Precision = +-0.001:  31.8us (gcc -Os)
         *     - Precision = +-0.001:  31.8us (gcc -O9)
         *   + Low-Level Driver: math_software_double
         *     - Precision = +-0.001:  39.6us (gcc -O0)
         *     - Precision = +-0.001:  38.8us (gcc -Os)
         *     - Precision = +-0.001:  39.0us (gcc -O9)
         */

        // Approximation Constants
        const ttm_number A0 = 0.318309886;
        const ttm_number A2 = -0.5182875;
        const ttm_number A4 = 0.222375;
        const ttm_number A6 = -0.016850156;
        const ttm_number B0 = 0.5;
        const ttm_number B2 = -0.89745875;
        const ttm_number B4 = 0.46138125;
        const ttm_number B6 = -0.058377188;

        ttm_number X2 = X * X;
        ttm_number X4 = X2 * X2;
        ttm_number X6 = X4 * X2;

        ArcSin = ( PI / 2 )
                 * X
                 * ( A0 + A2 * X2 + A4 * X4 + A6 * X6 )
                 / ( B0 + B2 * X2 + B4 * X4 + B6 * X6 );
    }

    return ArcSin;
}

#endif
#ifndef ttc_driver_math_cartesian2polar  //  no low - level implementation: use default implementation

void ttc_math_interface_cartesian2polar( ttm_number X, ttm_number Y, ttm_number* Angle, ttm_number* Length ) {
    Assert_MATH_Writable( Angle, ttc_assert_origin_auto );  //  check if it points to RAM (change for read only memory!)
    Assert_MATH_Writable( Length, ttc_assert_origin_auto );  //  check if it points to RAM (change for read only memory!)

    if ( ( X == 0 ) && ( Y == 0 ) ) { // it's a point!
        *Angle  = 0;
        *Length = 0;
        return;
    }

    *Angle  = _driver_math_vector2d_angle( X, Y );
    *Length = _driver_math_sqrt( X * X + Y * Y );
}

#endif
#ifndef ttc_driver_math_polar2cartesian  //  no low - level implementation: use default implementation

void ttc_math_interface_polar2cartesian( ttm_number Angle, ttm_number Length, ttm_number* X, ttm_number* Y ) {
    Assert_MATH_Writable( X, ttc_assert_origin_auto );  //  check if it points to RAM (change for read only memory!)
    Assert_MATH_Writable( Y, ttc_assert_origin_auto );  //  check if it points to RAM (change for read only memory!)

    // reduce angle so that sin() and cos() does not have to
    Angle = ttc_math_interface_angle_reduce( Angle );

    *X = Length * _driver_math_cos( Angle );
    *Y = Length * _driver_math_sin( Angle );
}

#endif
#ifndef ttc_driver_math_cos  //  no low - level implementation: use default implementation

ttm_number ttc_math_interface_cos( ttm_number X ) {

    // Note: Runtime measures have been taken by running benchmark ttc_math_cos(x)
    //       in example_ttc_math.c with Runs = 100 on an Olimx STM32-H107 prototype board.
    //       No gcc optimization (-O0 -g)
    //       On-time of TTC_LED1 gpio has been measured with digital oscilloscope.
    //       20151202 Gregor Rebel
    ttm_number absX = abs( X );
    if ( absX > PI ) {
        X = ttc_math_interface_angle_reduce( X );
        absX = abs( X );
    }
    if ( absX >  PI / 4.0 ) { //  cos(x) = -sin( x - (PI / 2) ) (implementation of sinus is more precise for |X| > PI/4)
        return _driver_math_sin( X + PI / 2.0 );
    }
    else { // 4th Taylor Polynom of cosinus (https://de.wikipedia.org/wiki/Taylorpolynom#N.C3.A4herungsformeln_f.C3.BCr_Sinus_und_Kosinus)

        // + using low-level driver math_software_float
        //   - MaxRelativeError = 0.000455944624
        //   - STM32F107@72MHz (-O0 -g): Runtime of 600 sin(X) : 9.78ms (stm32f107 @72MHz) => us/sin(X)
        //
        // + using low-level driver math_software_double
        //   - MaxRelativeError = 0.00045597856979831297
        //   - STM32F107@72MHz (-O0 -g): Runtime of 600 sin(X): 15.44ms (stm32f107 @72MHz) => us/sin(X)

        X *= X;
        return ( ( ( X / 12 )  - 1 ) * X / 2 ) + 1;
    }
}

#endif
#ifndef ttc_driver_math_sin  //  no low - level implementation: use default implementation

// Note: Runtime measures have been taken by running
//       - benchmark ttc_math_sin(x) in example_ttc_math.c
//       - Runs = 100
//       - Low-Level Drivers_ math_software_float, math_software_double
//       - Olimex STM32-H107 prototype board (@72MHz)
//       - No gcc optimization (-O0 -g)
//       - On-time of TTC_LED1 gpio has been measured with digital oscilloscope.
//       - Date 20151202
//       - done by Gregor Rebel

ttm_number ttc_math_interface_sin( ttm_number X ) {
    // reduce angle to stay inside optimal range for approximation
    X = _driver_math_angle_reduce( X );

    if ( ttc_math_Precision <= tmp_100m )
    { return ttc_math_interface_sin_quadratic_curve_low_precision( X ); }

    if ( ttc_math_Precision <= tmp_10m )
    { return ttc_math_interface_sin_quadratic_curve_high_precision( X ); }

    if ( X < 0 ) { // for smaller angles, mirroring taylor polynom at (0,0) provides better approximation
        return -ttc_math_interface_sin_taylor( -X, 5 );
        //X return -ttc_math_interface_sin_taylor( -X, 4 ); // slower but higher precision
    }
    if ( 0 ) { return ttc_math_interface_sin_quadratic_curve_high_precision( X ); }
    else   { return ttc_math_interface_sin_taylor( X, 5 ); } // slower but higher precision
}

ttm_number ttc_math_interface_sin_quadratic_curve_low_precision( ttm_number X ) {
    Assert_MATH_EXTRA( ( X >= -PI ) && ( X <= PI ), ttc_assert_origin_auto );  // optimal range exceeded: reduce angle before calling this function

    //  calculate sin(x) using low precision quadratic curve

    // + using low-level driver math_software_float
    //   - MaxRelativeError = 0.125213683
    //   - STM32F107@72MHz (-O0 -g): Runtime of 600 sin(X): 10.76ms (stm32f107 @72MHz) => 17.93us/sin(X)
    //   - STM32F107@72MHz (-O9):    Runtime of 600 sin(X):  9.14ms (stm32f107 @72MHz) => 15.23us/sin(X)
    //
    // + using low-level driver math_software_double
    //   - MaxRelativeError = 0.12521364055908088
    //   - STM32F107@72MHz (-O0 -g): Runtime of 600 sin(X) : 9.00ms (stm32f107 @72MHz) => 15.00us/sin(X)
    //   - STM32F107@72MHz (-O9):    Runtime of 600 sin(X):  8.42ms (stm32f107 @72MHz) => 14.03us/sin(X)

    if ( X < 0 ) {
        if ( X < -PI / 2 )
        { X = -PI - X; } // mirror curve at vertical line through (-PI/2, 0)
        return 1.27323954 * X + .405284735 * X * X;
    }
    else {
        if ( X > PI / 2 )
        { X = PI - X; } // mirror curve at vertical line through (PI/2, 0)
        return 1.27323954 * X - 0.405284735 * X * X;
    }
}
ttm_number ttc_math_interface_sin_quadratic_curve_high_precision( ttm_number X ) {
    Assert_MATH_EXTRA( ( X >= -PI ) && ( X <= PI ), ttc_assert_origin_auto );  // optimal range exceeded: reduce angle before calling this function

    // + using low-level driver math_software_float
    //   - MaxRelativeError = 0.000998037402
    //   - STM32F107@72MHz (-O0 -g): Runtime of 600 sin(X): 15.28ms (stm32f107 @72MHz) => 25.47us/sin(X)
    //   - STM32F107@72MHz (-O9):    Runtime of 600 sin(X): 13.22ms (stm32f107 @72MHz) => 22.03us/sin(X)
    //
    // + using low-level driver math_software_double
    //   - MaxRelativeError = 0.00099803087014098055
    //   - STM32F107@72MHz (-O0 -g): Runtime of 600 sin(X): 13.92ms (stm32f107 @72MHz) => 23.2us/sin(X)
    //   - STM32F107@72MHz (-O9):    Runtime of 600 sin(X): 13.06ms (stm32f107 @72MHz) => 21.77us/sin(X)
    ttm_number Y = 0;

    if ( X < 0 ) {
        if ( X < -PI / 2 )
        { X = -PI - X; } // mirror curve at vertical line through (-PI/2, 0)

        Y = 1.27323954 * X + .405284735 * X * X;

        if ( Y < 0 )
        { Y = .225 * ( Y * -Y - Y ) + Y; }
        else
        { Y = .225 * ( Y * Y - Y ) + Y; }
    }
    else {
        if ( X > PI / 2 )
        { X = PI - X; } // mirror curve at vertical line through (PI/2, 0)

        Y = 1.27323954 * X - 0.405284735 * X * X;

        if ( Y < 0 )
        { Y = .225 * ( Y * -Y - Y ) + Y; }
        else
        { Y = .225 * ( Y * Y - Y ) + Y; }
    }

    return Y;
}
ttm_number ttc_math_interface_sin_taylor4( ttm_number X ) {
    Assert_MATH_EXTRA( ( X >= -PI ) && ( X <= PI ), ttc_assert_origin_auto );  // optimal range exceeded: reduce angle before calling this function
    if ( X < -PI / 2 )
    { X = -PI - X; } // mirror curve at vertical line through (-PI/2, 0)
    else {
        if ( X > PI / 2 )
        { X = PI - X; } // mirror curve at vertical line through (PI/2, 0)
    }

    // 4th grade Taylor polynom for cosinus with shifted input

    // + using low-level driver math_software_float
    //   - MaxRelativeError = 0.00559643563
    //   - STM32F107@72MHz (-O0 -g): Runtime of 600 sin(X) : 12.64ms (stm32f107 @72MHz) => 21.1us/sin(X)
    //   - STM32F107@72MHz (-O9):    Runtime of 600 sin(X) : 11.90ms (stm32f107 @72MHz) => 19.8us/sin(X)
    //
    // + using low-level driver math_software_double
    //   - MaxRelativeError = 0.0055964102552663588
    //   - STM32F107@72MHz (-O0 -g): Runtime of 600 sin(X): 15.24ms (stm32f107 @72MHz) => 25.4us/sin(X)
    //   - STM32F107@72MHz (-O9):    Runtime of 600 sin(X): 13.02ms (stm32f107 @72MHz) => 21.7us/sin(X)


    //? X = _driver_math_angle_reduce( X - ( PI / 2.0 ) );  // reduce angle to stay inside optimal range for approximation
    X *= X;
    return ( ( ( X / 12 )  - 1 ) * X / 2 ) + 1;
}
ttm_number ttc_math_interface_sin_taylor( ttm_number X, t_u8 Grade ) {
    Assert_MATH_EXTRA( ( X >= -PI ) && ( X <= PI ), ttc_assert_origin_auto );  // optimal range exceeded: reduce angle before calling this function
    if ( X < -PI / 2 )
    { X = -PI - X; } // mirror curve at vertical line through (-PI/2, 0)
    else {
        if ( X > PI / 2 )
        { X = PI - X; } // mirror curve at vertical line through (PI/2, 0)
    }

    ttm_number Y = 0;

    // Taylor Polynom of adjustable grade (Grade=3..4)
    // More grades for higher precision can be added easily

    ttm_number X3 = X * X * X;

    if ( 1 ) { // 2th taylor polyonom of sinus

        /* Benchmark Results
         *
         * Runtime of 600 sin(X): 12.2ms (stm32f107 @72MHz)
         * + using low-level driver math_software_float
         *   - MaxRelativeError = 0.0751677155
         *   - STM32F107@72MHz (-O0 -g): Runtime of 600 sin(X) : 8.82ms (stm32f107 @72MHz) => 14.70us/sin(X)
         *   - STM32F107@72MHz (-O9):    Runtime of 600 sin(X) : 7.70ms (stm32f107 @72MHz) => 12.83us/sin(X)
         *
         * + using low-level driver math_software_double
         *   - MaxRelativeError = 0.075167770711349613
         *   - STM32F107@72MHz (-O0 -g): Runtime of 600 sin(X): 13.56ms (stm32f107 @72MHz) => 22.60us/sin(X)
         *   - STM32F107@72MHz (-O9):    Runtime of 600 sin(X): 12.90ms (stm32f107 @72MHz) => 21.50us/sin(X)
         */

        Y = X - ( X3 / 6 );
    }
    if ( Grade > 2 ) { // 3th taylor polyonom of sinus

        if ( X > 0.5 ) { // higher grades only provide more accuracy for greater arguments
            /* Benchmark Results
            *
            * Runtime of 600 sin(X): 12.2ms (stm32f107 @72MHz) => us/sin(X)
            * + using low-level driver math_software_float
            *   - MaxRelativeError = 0.00452494621
            *   - STM32F107@72MHz (-O0 -g): Runtime of 600 sin(X) : 12.20ms (stm32f107 @72MHz) => 20.33us/sin(X)
            *   - STM32F107@72MHz (-O9):    Runtime of 600 sin(X) : 10.98ms (stm32f107 @72MHz) => 18.30us/sin(X)
            *
            * + using low-level driver math_software_double
            *   - MaxRelativeError = 0.0045248555348174069
            *   - STM32F107@72MHz (-O0 -g): Runtime of 600 sin(X): 22.12ms (stm32f107 @72MHz) => 36.87us/sin(X)
            *   - STM32F107@72MHz (-O9):    Runtime of 600 sin(X): 21.22ms (stm32f107 @72MHz) => 35.37us/sin(X)
            */
            Y += X3 * X * X / 120;

            if ( Grade > 3 ) { // 4th taylor polyonom of sinus
                Y -= X3 * X3 * X / 5040;

                if ( Grade > 4 ) { // 5th taylor polyonom of sinus
                    Y += X3 * X3 * X3 / 362880;
                }
            }
            // more grades for more precision..
        }
    }

    return Y;
}

#endif
#ifndef ttc_driver_math_tan  //  no low - level implementation: use default implementation

ttm_number ttc_math_interface_tan( ttm_number X ) {

    ttm_number Enumerator  = _driver_math_sin( X );
    ttm_number Denominator = _driver_math_cos( X );

    if ( Denominator == 0 ) { // handle infinity by returning maximum or minium value for current ttm_number
        if ( Enumerator > 0 )
        { return TTC_MATH_CONST_MAX_VALUE; }
        else
        { return TTC_MATH_CONST_MIN_VALUE; }
    }

    return Enumerator / Denominator;
}

#endif
#ifndef ttc_driver_math_arctan  //  no low - level implementation: use default implementation

ttm_number ttc_math_interface_arctan( ttm_number X ) {

    // https://de.wikipedia.org/wiki/Arkustangens_und_Arkuskotangens#N.C3.A4herungsweise_Berechnung
    // https://web.archive.org/web/20090416044652/http://lightsoft.co.uk/PD/stu/stuchat37.html


    ttm_number X2 = X * X;
    ttm_number ArcTan = 1;

    while ( abs( X ) >= 2 ) { // reduce big argument to increase precision
        X = X / ( 1 + ttc_driver_math_sqrt( 1 + X2 ) );
        ArcTan *= 2;
        X2 = X * X;
    }

    if ( abs( X ) <= 1 ) {
        if ( ttc_math_Precision <= tmp_1m ) { // more precise approximation
            ArcTan *= ( X + 0.43157974 * X2 * X ) / ( 1 + 0.76443945 * X2 + 0.05831938 * X2 * X2 );
        }

        else { // fast approximation better than 0.005 radiants
            ArcTan *= X / ( 1 + ( 0.28 * X2 ) );
        }
    }
    else {
        if ( X > 1 ) {
            ArcTan *= PI / 2.0 - ( X / ( X2 + 0.28 ) );
        }
        else { // X < -1
            ArcTan *= -PI / 2.0 - ( X / ( X2 + 0.28 ) );
        }
    }

    return ArcTan;
}

#endif
#ifndef ttc_driver_math_angle_reduce  //  no low - level implementation: use default implementation

ttm_number ttc_math_interface_angle_reduce( ttm_number Angle ) {

    if ( Angle > PI ) {
        Angle = _driver_math_modulo( Angle, 2.0 * PI ); // ensure it's not bigger than 2*PI
        if ( Angle > PI )
        { Angle  -= ( 2.0 * PI ); }
    }
    else if ( Angle <  -  PI ) {
        if ( Angle <  -  2.0 * PI )
        { Angle = _driver_math_modulo( Angle, -2.0 * PI ); } // ensure it's not bigger than 2*PI
        if ( Angle <  -  PI )
        { Angle  += ( 2.0 * PI ); }
    }

    /* ???
    if ( Angle > ( PI / 2 ) )
    { Angle = PI - Angle; }
    else if ( Angle <  - ( PI / 2 ) )
    { Angle =  - PI - Angle; }
    */

    return Angle;
}

#endif
#ifndef ttc_driver_math_pow  //  no low - level implementation: use default implementation

ttm_number ttc_math_interface_pow( ttm_number Base, t_u8 Exponent ) {
    ttm_number Power = 1;

    if ( Exponent == 0 ) { return 0; }
    if ( Base == 0 )     { return 0; }

    while ( Exponent ) {
        if ( Exponent & 1 )
        { Power *= Base; }
        Exponent >>= 1;
        Base *= Base;
    }
    return Power;
}

#endif
#ifndef ttc_driver_math_modulo  //  no low - level implementation: use default implementation

ttm_number ttc_math_interface_modulo( ttm_number A, ttm_number B ) {

    t_base Factor = ( ( A / B ) + 0.0001 );

    return A - Factor * B;
}

#endif
#ifndef ttc_driver_math_log_int  // no low-level implementation: use default implementation

t_s8 ttc_math_interface_log_int( ttm_number A, t_u8 Base ) {

    ttm_number Log = 0;
    ttm_number Compare = 1;

    if ( A >= 1.0 ) { // Log > 0
        while ( Compare < A ) {
            Log++;
            Compare *= Base;
        }
    }
    else {        // Log < 0
        if ( !A )
        { return ( t_s8 ) TTC_MATH_CONST_MIN_VALUE; } // cheap -infinity
        Assert_MATH( A > 0, ttc_assert_origin_auto );  // ERROR: got negative value for A

        while ( Compare > A ) {
            Log--;
            Compare /= Base;
        }
    }
    return ( t_s8 ) Log;
}

#endif
#if 0  // alternative implementations available (if removed, this function will be recreated by create_DeviceDriver.pl on next UPDATE)
ttm_number ttc_math_interface_sin( ttm_number X ) {
    return ( ttm_number ) 0;
}
#endif
#ifndef ttc_driver_math_int_rankN  // no low-level implementation: use default implementation

t_u8 ttc_math_interface_int_rankN( t_u32 A, t_u8 Base ) {
    if ( !A )
    { return 0; }

    t_u8  Rank = 0;
    t_u32 Compare = 1;

    while ( Compare && ( Compare <= A ) ) {
        Rank++;
        Compare *= Base; // multiplication is faster than division
    }

    return Rank;
}

#endif
#ifndef ttc_driver_math_int_rank2  // no low-level implementation: use default implementation

t_u8 ttc_math_interface_int_rank2( t_u32 A ) {
    t_u8 Rank = 0;

    while ( A > 512 ) { // speed up for larger numbers
        Rank += 9;
        A = A >> 9;
    }
    while ( A ) {
        Rank++;
        A = A >> 1;
    }
    return Rank;
}

#endif
#ifndef ttc_driver_math_inv_sqrt  // no low-level implementation: use default implementation

ttm_number ttc_math_interface_inv_sqrt( ttm_number X ) {
    Assert_MATH( X >= 0, ttc_assert_origin_auto );  // must be a positive value

    ttm_number InvSqrt = 0;

    if ( 0 ) { // DOES COMPUTE INCORRECT VALUES - ToDo: Find better implementation to directly compute result
        if ( ( 0.6 >= X ) && ( X <= 1.4 ) ) { // calculate using Olli's approximation (->http://www.olliw.eu/2014/fast-functions/#invsqrt)
            InvSqrt = 1.5 - ( X / 2 );

            InvSqrt = ( 1.5 - X * InvSqrt * ( InvSqrt / 2 ) ) * InvSqrt; // precision < 0.0001 for 0.6 < X < 1.4
            if ( ttc_math_Precision >= tmp_1m )
            { InvSqrt = ( 1.5 - X * InvSqrt * ( InvSqrt / 2 ) ) * InvSqrt; } // repeat second Newton step for improved accuracy
        }
        else { // range not optimal for above implementations: use default implementation
            return 1.0 / ttc_driver_math_sqrt( X );
        }
    }
    else { InvSqrt = 1 / ttc_driver_math_sqrt( X ); } // always works

    return InvSqrt;
}

#endif
ttm_number ttc_math_interface_arccos( ttm_number X ) {
    return ( PI / 2 ) - ttc_math_interface_arcsin( X );
}
#ifndef ttc_driver_math_sqrt  // no low-level implementation: use default implementation

ttm_number ttc_math_interface_sqrt( ttm_number X ) {
    Assert_MATH( X >= 0, ttc_assert_origin_auto );  // must be a positive value

    ttm_number SQRT;

    if ( 1 ) { // initial guess
        if ( X >= 1 ) {        // [1,inf]
            SQRT = ( X + 1 ) / 2;
        }
        else  {                // [0,1[
            if ( X == 0 )
            { return 0; }

            SQRT = 2 * X;
        }
    }

#warning Default ttc_driver_math_sqrt() implementation is untested and may be buggy!

    if ( 1 ) {   // Newton Method (->https://de.wikipedia.org/wiki/Newton-Verfahren)
        // Drawbacks:
        // - amount of iterations is not constant
        // - difficult to iterate until desired precision is reached

        t_u16 Loops = 0;
        ttm_number SQRTprev = 0;
        do { // run newton iteration until single step brings less change than desired precision
            SQRTprev = SQRT;
            //SQRT = SQRTprev - ( SQRTprev * SQRTprev - X ) / ( 2 * X ); // Newton Step
            SQRT = 0.5 * ( SQRTprev + ( X / SQRTprev ) );
            if ( SQRT < 0 ) {
                ttc_assert_halt_origin( ttc_assert_origin_auto );
            }
            Loops++;
            if ( SQRTprev > SQRT ) { // exit loop if last iteration gave less change than required precision
                if ( ( SQRTprev - SQRT ) < ttc_math_PrecisionValue )
                { break; }
            }
            else {
                if ( ( SQRT - SQRTprev ) < ttc_math_PrecisionValue )
                { break; }
            }
        }
        while ( 1 );
    }

    return SQRT;
}

#endif
#ifndef ttc_driver_math_vector2d_angle  // no low-level implementation: use default implementation

ttm_number ttc_math_interface_vector2d_angle( ttm_number DX, ttm_number DY ) {
    ttm_number Ratio;  // argument to atan()
    ttm_number Offset; // added to result of atan()

    /* Using complete decision tree for optimal calculation with highest precision
     *
     * Calculating increment ratio dX/dY is imprecise when dY is almost zero.
     * Basic idea: arctan(dX / dY) = PI / 2 - arctan(dY / dX)
     *
     * Ratio  = optimal calculation increment (divident is always larger absolute value)
     * Offset = rotates calculated angle to correct position
     *
     * Angle = arctan(Ratio) + Offset
     */

    if ( DX > 0 ) {                    // DX>0: quadrants 1 & 4
        if ( DY > 0 ) {                // DX>0, DY>0
            if ( DX < DY )             // DX>0, DY>0, DX<DY
            { Ratio = -DX / DY; Offset = PI / 2; }
            else {
                if ( DX == DY )        // DX>0, DY>0, DX==DY: +45°
                { return PI / 4; }
                else {                 // DX>0, DY>0, DX>DY
                    Ratio = DY / DX; Offset = 0;
                }
            }
        }
        else {                         // DX>0, DY<=0
            if ( DY == 0 )             // DX>0, DY==0: 0°
            { return 0; }
            else {                     // DX>0, DY<0
                if ( DX < -DY ) {      // DX>0, DY<0, |DX|<|DY|
                    Ratio = -DX / DY; Offset = -PI / 2;
                }
                else {                 // DX>0, DY<0, |DX|>=|DY|
                    if ( DX == -DY )   // DX>0, DY<0, |DX|==|-DY|: -45°
                    { return -PI / 4; }
                    else {             // DX>0, DY<0, |DX|>|DY|
                        //X Ratio = -DY / DX; Offset = -PI / 2;
                        Ratio =  DY / DX; Offset = 0;
                    }
                }
            }
        }
    }
    else {                             // DX<=0
        if ( DX == 0 ) {               // DX==0: vertical line
            if ( DY < 0 )
            { return -PI / 2; }            // DX==0,DY <0: -90°
            else
            { return  PI / 2; }            // DX==0,DY>=0: +90°
        }
        else {                         // DX<0: quadrants 2 & 3
            if ( DY > 0 ) {            // DX<0, DY>0: quadrant 2
                if ( DY > -DX )
                { Ratio =  -DX / DY; Offset = PI / 2; }  // DX<0, abs(DY)>abs(DX)
                else {
                    if ( DY == -DX )       // DX<0, DX==-DY: 135°
                    { return PI * 3 / 4; }
                    else
                    { Ratio = DY / DX; Offset = PI; }  //
                }
            }
            else {                     // DX<0, DY<=0
                if ( DY == 0 )         // DX<0, DY==0: horizontal line
                { return PI ; }        //   +180°
                else {                 // DX<0, DY<0: quadrant 3
                    if ( DX < DY )     // DX<0, DY<0, |DY|<|DX| because DY and DX negative!
                    { Ratio = DY / DX; Offset = -PI; }
                    else {             // DX<0, DY<=DX
                        if ( DX == DY )// DX<0,DX==DY: -135°
                        { return -PI * 3 / 4; }
                        else           // DX<0, |DX|<|DY|
                        { Ratio =  -DX / DY; Offset = -PI / 2; }
                    }
                }
            }
        }
    }

    volatile ttm_number Angle = _driver_math_arctan( Ratio ) ;

    if ( Offset )
    { Angle += Offset; }

    return Angle;
}

#endif
#ifndef ttc_driver_math_length_2d  // no low-level implementation: use default implementation
ttm_number ttc_math_interface_length_2d( ttm_number DistanceX, ttm_number DistanceY ) {


    if ( DistanceY == 0 )
    { return abs( DistanceX ); }
    else {
        if ( DistanceX == 0 )
        { return abs( DistanceY ); }
        else
        { return _driver_math_sqrt( DistanceX * DistanceX + DistanceY * DistanceY ); }
    }
}

#endif
#ifndef ttc_driver_math_length_3d  // no low-level implementation: use default implementation

ttm_number ttc_math_interface_length_3d( ttm_number DistanceX, ttm_number DistanceY, ttm_number DistanceZ ) {

    if ( DistanceZ ) {
        if ( DistanceY ) {
            if ( DistanceX ) { // calculate 3D-distance
                return _driver_math_sqrt( DistanceX * DistanceX + DistanceY * DistanceY + DistanceZ * DistanceZ );
            }
            else {           // A.X == B.X: calculate 2D-distance in YZ
                return _driver_math_sqrt( DistanceZ * DistanceZ + DistanceY * DistanceY );
            }
        }
        else {               // A.Y == B.Y
            if ( DistanceX ) { // calculate 2D-distance in XZ
                return _driver_math_sqrt( DistanceZ * DistanceZ + DistanceX * DistanceX );
            }
            else {           // A.Y == B.Y, A.X == B.X: calculate 2D-distance along Z
                return abs( DistanceZ );
            }
        }
    }
    else {
        if ( DistanceY ) { // A.Z == B.Z
            if ( DistanceX ) { // calculate 2D-distance in XY
                return _driver_math_sqrt( DistanceY * DistanceY + DistanceX * DistanceX );
            }
            else {           // A.Z == B.Z, A.X == B.X: calculate 2D-distance along Y
                return abs( DistanceY );
            }
        }
        else {               // A.Z == B.Z, A.Y == B.Y: calculate 2D-distance along X
            return abs( DistanceX );
        }
    }

    ttc_assert_halt_origin( ttc_assert_origin_auto ); // Must not get here. Check implementation above!
}

#endif
#ifndef ttc_driver_math_distance_2d  // no low-level implementation: use default implementation
ttm_number ttc_math_interface_distance_2d( ttm_number Ax, ttm_number Ay, ttm_number Bx, ttm_number By ) {

    return _driver_math_length_2d(
               _driver_math_distance( Ax, Bx ),
               _driver_math_distance( Ay, By )
           );
}

#endif
#ifndef ttc_driver_math_vector3d_distance  // no low-level implementation: use default implementation
ttm_number ttc_math_interface_vector3d_distance( t_ttc_math_vector3d_xyz* A, t_ttc_math_vector3d_xyz* B ) {
    Assert_MATH_Readable( A, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_MATH_Readable( B, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    return _driver_math_length_3d(
               _driver_math_distance( A->X, B->X ),
               _driver_math_distance( A->Y, B->Y ),
               _driver_math_distance( A->Z, B->Z )
           );
}

#endif
#ifndef ttc_driver_math_abs  // no low-level implementation: use default implementation
ttm_number ttc_math_interface_abs( ttm_number X ) {
    if ( X < 0 )
    { return -X; }
    return X;
}

#endif
#ifndef ttc_driver_math_intersection_2d  // no low-level implementation: use default implementation

void ttc_math_interface_intersection_2d( t_ttc_math_vector2d_xy* Line1_Point, t_ttc_math_vector2d_xy* Line1_Incline, t_ttc_math_vector2d_xy* Line2_Point, t_ttc_math_vector2d_xy* Line2_Incline, t_ttc_math_vector2d_xy* Intersection ) {
    Assert_MATH_Writable( Line1_Point, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_MATH_Writable( Line1_Incline, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_MATH_Writable( Line2_Point, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_MATH_Writable( Line2_Incline, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_MATH_Writable( Intersection, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    if ( ( Line1_Incline->X / Line1_Incline->Y ) != ( Line2_Incline->X / Line2_Incline->Y ) ) { // straights are not parallel
        ttm_number Line1_b = 0; // y axis section
        ttm_number Line1_m = 0; // Incline

        ttm_number Line2_b = 0; // y axis section
        ttm_number Line2_m = 0; // Incline

        Line1_m = Line1_Incline->X / Line1_Incline->Y;
        Line2_m = Line2_Incline->X / Line2_Incline->Y;

        Line1_b = Line1_Point->Y - ( Line1_m * Line1_Point->X );
        Line2_b = Line2_Point->Y - ( Line2_m * Line2_Point->X );

        Intersection->X = ( Line1_b - Line2_b ) / ( Line2_m - Line1_m );
        Intersection->Y = Line2_m * Intersection->X + Line2_b;

    }
    else { // straights are parallel, no intersection
        Intersection->X = -10000;
        Intersection->Y = -10000;
    }


}

#endif
#ifndef ttc_driver_math_vector2d_incline  // no low-level implementation: use default implementation

t_ttc_math_vector2d_xy ttc_math_interface_vector2d_incline( t_ttc_math_vector2d_xy* A, t_ttc_math_vector2d_xy* B ) {
    Assert_MATH_Writable( A, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_MATH_Writable( B, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    t_ttc_math_vector2d_xy incline;

    incline.X = A->X - B->X;
    incline.Y = A->Y - B->Y;

    return ( t_ttc_math_vector2d_xy ) incline;
}

#endif
#ifndef ttc_driver_math_vector3d_valid  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_math_vector3d_valid() (using default implementation)!

BOOL ttc_math_interface_vector3d_valid( const t_ttc_math_vector3d_xyz* Vector ) {
    Assert_MATH_Readable( Vector, ttc_assert_origin_auto ); // always check incoming pointer arguments

    return ( Vector->X != TTC_MATH_CONST_NAN ) &&
           ( Vector->Y != TTC_MATH_CONST_NAN ) &&
           ( Vector->Z != TTC_MATH_CONST_NAN );
}

#endif
//InsertFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

#if 0 // keeping this empty implementations to avoid that create_DeviceDriver.pl will recreate them on next UPDATE run

float         ttc_math_interface_to_float( ttm_number Value ) { }
ttm_number    ttc_math_interface_from_double( double Value ) { }
double        ttc_math_interface_to_double( ttm_number Value ) { }
ttm_number    ttc_math_interface_from_float( float Value ) { }
t_base_signed ttc_math_interface_to_int( ttm_number Value ) { }
ttm_number    ttc_math_interface_from_int( t_base_signed Value ) { }

#endif

// }FunctionDefinitions
// { private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_math_interface_foo(t_ttc_math_config* Config) { ... }

// InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

// }PrivateFunctions
