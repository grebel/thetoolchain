/** { ttc_gpio_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for GPIO device.
 *
 *  *_interface_*() functions may be redirected to low-level implementations in
 *  two ways:
 *
 *  1) Macro Definition
 *     This applies if only one architecture can be available during runtime.
 *     One example is a GPIO driver for current microcontroller architecture.
 *     Only one type of microcontroller can be available during runtime.
 *
 *  2) Selector Function
 *     Applies if more than one architecture can be availabe during runtime.
 *     A function will select the corresponding low-level driver via a switch-,
 *     if-else construct of via a vector table.
 *     An example can be a display driver. It is possible to connect more than
 *     one different displays to a microcontroller. So every interface function has
 *     to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation requires changes in this file.
 *
 *  Created from template ttc_device_interface.h revision 20 at 20140205 09:01:30 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_GPIO_INTERFACE_H
#define TTC_GPIO_INTERFACE_H

//{ Includes *************************************************************

#include "../ttc_basic.h"

//{ BEGIN_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_gpio_stm32f1xx
    #include "../gpio/gpio_stm32f1xx.h"
#endif
#ifdef EXTENSION_gpio_stm32l1xx
    #include "../gpio/gpio_stm32l1xx.h"  // low-level driver for gpio devices on stm32l1xx architecture
#endif
#ifdef EXTENSION_gpio_stm32w1xx
    #include "../gpio/gpio_stm32w1xx.h"  // low-level driver for gpio devices on stm32w1xx architecture
#endif
#ifdef EXTENSION_gpio_stm32f30x
    #include "../gpio/gpio_stm32f30x.h"  // low-level driver for gpio devices on stm32f30x architecture
#endif
#ifdef EXTENSION_gpio_stm32l0xx
    #include "../gpio/gpio_stm32l0xx.h"  // low-level driver for gpio devices on stm32l0xx architecture
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

//} END_ARCHITECTURE_INCLUDES (DO NOT REMOVE THIS LINE!)

#include "../ttc_gpio_types.h"

//} Includes
/** static declaration of interface between high- and low-level implementation ********{
 *
 * For each _driver_gpio_*() declaration in ../ttc_gpio.h, a corresponding
 * interface part is required here. The interface will forward the declaration either
 * to a low-level implementation or a default implementation in ttc_gpio_interface.c .
 * A warning is generated for each missing driver implementation.
 *
 * Note: This code is generated automatically. Check ../ttc_gpio.h for prototypes of low-level driver functions!
 *
 */

BOOL   ttc_gpio_interface_get( e_ttc_gpio_pin PortPin ); //{  macro _driver_gpio_interface_get(Port)
#ifdef ttc_driver_gpio_get

    #define _driver_gpio_get(Port) ttc_driver_gpio_get(Port)
#else
    #define _driver_gpio_get(Port) ttc_gpio_interface_get(Port)
    #  warning: missing low-level implementation for ttc_driver_gpio_get()!
#endif
//}
void   ttc_gpio_interface_prepare(); //{  macro _driver_gpio_interface_prepare()
#ifdef ttc_driver_gpio_prepare

    #define _driver_gpio_prepare() ttc_driver_gpio_prepare()
#else
    #define _driver_gpio_prepare() ttc_gpio_interface_prepare()
    #  warning: missing low-level implementation for ttc_driver_gpio_prepare()!
#endif
//}
void   ttc_gpio_interface_set( e_ttc_gpio_pin PortPin ); //{  macro _driver_gpio_interface_set(Port)
#ifdef ttc_driver_gpio_set

    #define _driver_gpio_set(Port) ttc_driver_gpio_set(Port)
#else
    #define _driver_gpio_set(Port) ttc_gpio_interface_set(Port)
    #  warning: missing low-level implementation for ttc_driver_gpio_set()!
#endif
//}
void   ttc_gpio_interface_deinit( e_ttc_gpio_pin PortPin ); //{  macro _driver_gpio_interface_deinit(Port)
#ifdef ttc_driver_gpio_deinit

    #define _driver_gpio_deinit(Port) ttc_driver_gpio_deinit(Port)
#else
    #define _driver_gpio_deinit(Port) ttc_gpio_interface_deinit(Port)
    #  warning: missing low-level implementation for ttc_driver_gpio_deinit()!
#endif
//}
void   ttc_gpio_interface_init( e_ttc_gpio_pin PortPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ); //{  macro _driver_gpio_interface_init(Port, Type, Speed)
#ifdef ttc_driver_gpio_init

    #define _driver_gpio_init(Port, Type, Speed) ttc_driver_gpio_init(Port, Type, Speed)
#else
    #define _driver_gpio_init(Port, Type, Speed) ttc_gpio_interface_init(Port, Type, Speed)
    #  warning: missing low-level implementation for ttc_driver_gpio_init()!
#endif
//}
void   ttc_gpio_interface_clr( e_ttc_gpio_pin PortPin ); //{  macro _driver_gpio_interface_clr(Port)
#ifdef ttc_driver_gpio_clr

    #define _driver_gpio_clr(Port) ttc_driver_gpio_clr(Port)
#else
    #define _driver_gpio_clr(Port) ttc_gpio_interface_clr(Port)
    #  warning: missing low-level implementation for ttc_driver_gpio_clr()!
#endif
//}
void   ttc_gpio_interface_get_configuration( e_ttc_gpio_pin PortPin, t_ttc_gpio_config* Configuration ); //{  macro _driver_gpio_interface_get_configuration(PortPin, Configuration)
#ifdef ttc_driver_gpio_get_configuration

    #define _driver_gpio_get_configuration(PortPin, Configuration) ttc_driver_gpio_get_configuration(PortPin, Configuration)
#else
    #define _driver_gpio_get_configuration(PortPin, Configuration) ttc_gpio_interface_get_configuration(PortPin, Configuration)
    #  warning: missing low-level implementation for ttc_driver_gpio_get_configuration()!
#endif
//}
void   ttc_gpio_interface_parallel16_init( t_ttc_gpio_bank GPIOx, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ); //{  macro _driver_gpio_interface_parallel16_init(GPIOx, Type, Speed)
#ifdef ttc_driver_gpio_parallel16_init

    #define _driver_gpio_parallel16_init(GPIOx, Mode, Speed) ttc_driver_gpio_parallel16_init(GPIOx, Mode, Speed)
#else
    #define _driver_gpio_parallel16_init(GPIOx, Mode, Speed) ttc_gpio_interface_parallel16_init(GPIOx, Mode, Speed)
#endif
//}
void   ttc_gpio_interface_parallel08_init( t_ttc_gpio_bank GPIOx, t_u8 FirstPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ); //{  macro _driver_gpio_interface_parallel08_init(GPIOx, FirstPin, Type, Speed)
#ifdef ttc_driver_gpio_parallel08_init

    #define _driver_gpio_parallel08_init(GPIOx, FirstPin, Mode, Speed) ttc_driver_gpio_parallel08_init(GPIOx, FirstPin, Mode, Speed)
#else
    #define _driver_gpio_parallel08_init(GPIOx, FirstPin, Mode, Speed) ttc_gpio_interface_parallel08_init(GPIOx, FirstPin, Mode, Speed)
#endif
//}
void   ttc_gpio_interface_from_index( t_base PhysicalIndex, t_ttc_gpio_bank* GPIOx, t_u8* Pin ); //{  macro _driver_gpio_interface_from_index(PhysicalIndex, GPIOx, Pin)
#ifdef ttc_driver_gpio_from_index

    #define _driver_gpio_from_index(PhysicalIndex, GPIOx, Pin) ttc_driver_gpio_from_index(PhysicalIndex, GPIOx, Pin)
#else
    #define _driver_gpio_from_index(PhysicalIndex, GPIOx, Pin) ttc_gpio_interface_from_index(PhysicalIndex, GPIOx, Pin)
#endif
//}
t_base ttc_gpio_interface_create_index( t_ttc_gpio_bank GPIOx, t_u8 Pin ); //{  macro _driver_gpio_interface_create_index(GPIOx, Pin)
#ifdef ttc_driver_gpio_create_index

    #define _driver_gpio_create_index(GPIOx, Pin) ttc_driver_gpio_create_index(GPIOx, Pin)
#else
    #define _driver_gpio_create_index(GPIOx, Pin) ttc_gpio_interface_create_index(GPIOx, Pin)
#endif
//}
void   ttc_gpio_interface_alternate_function( e_ttc_gpio_pin PortPin, t_u8 AlternateFunction ); //{  macro _driver_gpio_interface_prepare()
#ifdef ttc_driver_gpio_alternate_function

    #define _driver_gpio_alternate_function(PortPin, AlternateFunction) ttc_driver_gpio_alternate_function(PortPin, AlternateFunction)
#else
    #define _driver_gpio_alternate_function(PortPin, AlternateFunction) ttc_gpio_interface_alternate_function(PortPin, AlternateFunction)
#endif
//}
void   ttc_gpio_interface_put( e_ttc_gpio_pin PortPin, BOOL Value ); //{  macro _driver_gpio_interface_put(PortPin, Value)
#ifdef ttc_driver_gpio_put

    #define _driver_gpio_put(PortPin, Value) ttc_driver_gpio_put(PortPin, Value)
#else
    // #  define _driver_gpio_put(PortPin, Value) ( (Value) ? ttc_driver_gpio_set(PortPin) : ttc_driver_gpio_clr(PortPin) )
    #define _driver_gpio_put(PortPin, Value) ttc_gpio_interface_put(PortPin, Value)
#endif
//}

void   ttc_gpio_interface_parallel08_1_put( BOOL Value ); //{  macro _driver_gpio_interface_parallel08_1_put(Value)
#ifdef ttc_driver_gpio_parallel08_1_put
    #define _driver_gpio_parallel08_1_put(Value) ttc_driver_gpio_parallel08_1_put(Value)
#else
    #define _driver_gpio_parallel08_1_put(Value) ttc_gpio_interface_parallel08_1_put(Value)
#endif
//}
void   ttc_gpio_interface_parallel08_2_put( BOOL Value ); //{  macro _driver_gpio_interface_parallel08_2_put(Value)
#ifdef ttc_driver_gpio_parallel08_2_put
    #define _driver_gpio_parallel08_2_put(Value) ttc_driver_gpio_parallel08_2_put(Value)
#else
    #define _driver_gpio_parallel08_2_put(Value) ttc_gpio_interface_parallel08_2_put(Value)
#endif
//}
void   ttc_gpio_interface_parallel08_3_put( BOOL Value ); //{  macro _driver_gpio_interface_parallel08_3_put(Value)
#ifdef ttc_driver_gpio_parallel08_3_put
    #define _driver_gpio_parallel08_3_put(Value) ttc_driver_gpio_parallel08_3_put(Value)
#else
    #define _driver_gpio_parallel08_3_put(Value) ttc_gpio_interface_parallel08_3_put(Value)
#endif
//}
void   ttc_gpio_interface_parallel08_4_put( BOOL Value ); //{  macro _driver_gpio_interface_parallel08_4_put(Value)
#ifdef ttc_driver_gpio_parallel08_4_put
    #define _driver_gpio_parallel08_4_put(Value) ttc_driver_gpio_parallel08_4_put(Value)
#else
    #define _driver_gpio_parallel08_4_put(Value) ttc_gpio_interface_parallel08_4_put(Value)
#endif
//}
void   ttc_gpio_interface_parallel08_5_put( BOOL Value ); //{  macro _driver_gpio_interface_parallel08_5_put(Value)
#ifdef ttc_driver_gpio_parallel08_5_put
    #define _driver_gpio_parallel08_5_put(Value) ttc_driver_gpio_parallel08_5_put(Value)
#else
    #define _driver_gpio_parallel08_5_put(Value) ttc_gpio_interface_parallel08_5_put(Value)
#endif
//}
void   ttc_gpio_interface_parallel08_6_put( BOOL Value ); //{  macro _driver_gpio_interface_parallel08_6_put(Value)
#ifdef ttc_driver_gpio_parallel08_6_put
    #define _driver_gpio_parallel08_6_put(Value) ttc_driver_gpio_parallel08_6_put(Value)
#else
    #define _driver_gpio_parallel08_6_put(Value) ttc_gpio_interface_parallel08_6_put(Value)
#endif
//}
void   ttc_gpio_interface_parallel08_7_put( BOOL Value ); //{  macro _driver_gpio_interface_parallel08_7_put(Value)
#ifdef ttc_driver_gpio_parallel08_7_put
    #define _driver_gpio_parallel08_7_put(Value) ttc_driver_gpio_parallel08_7_put(Value)
#else
    #define _driver_gpio_parallel08_7_put(Value) ttc_gpio_interface_parallel08_7_put(Value)
#endif
//}
void   ttc_gpio_interface_parallel08_8_put( BOOL Value ); //{  macro _driver_gpio_interface_parallel08_8_put(Value)
#ifdef ttc_driver_gpio_parallel08_8_put
    #define _driver_gpio_parallel08_8_put(Value) ttc_driver_gpio_parallel08_8_put(Value)
#else
    #define _driver_gpio_parallel08_8_put(Value) ttc_gpio_interface_parallel08_8_put(Value)
#endif
//}
void   ttc_gpio_interface_parallel08_9_put( BOOL Value ); //{  macro _driver_gpio_interface_parallel08_9_put(Value)
#ifdef ttc_driver_gpio_parallel08_9_put
    #define _driver_gpio_parallel08_9_put(Value) ttc_driver_gpio_parallel08_9_put(Value)
#else
    #define _driver_gpio_parallel08_9_put(Value) ttc_gpio_interface_parallel08_9_put(Value)
#endif
//}
void   ttc_gpio_interface_parallel08_10_put( BOOL Value ); //{  macro _driver_gpio_interface_parallel08_10_put(Value)
#ifdef ttc_driver_gpio_parallel08_10_put
    #define _driver_gpio_parallel08_10_put(Value) ttc_driver_gpio_parallel08_10_put(Value)
#else
    #define _driver_gpio_parallel08_10_put(Value) ttc_gpio_interface_parallel08_10_put(Value)
#endif
//}

t_u8   ttc_gpio_interface_parallel08_1_get( ); //{  macro _driver_gpio_interface_parallel08_1_get(PortPin)
#ifdef ttc_driver_gpio_parallel08_1_get
    #define _driver_gpio_parallel08_1_get() ttc_driver_gpio_parallel08_1_get()
#else
    #define _driver_gpio_parallel08_1_get() ttc_gpio_interface_parallel08_1_get()
#endif
//}
t_u8   ttc_gpio_interface_parallel08_2_get( ); //{  macro _driver_gpio_interface_parallel08_2_get(PortPin)
#ifdef ttc_driver_gpio_parallel08_2_get
    #define _driver_gpio_parallel08_2_get() ttc_driver_gpio_parallel08_2_get()
#else
    #define _driver_gpio_parallel08_2_get() ttc_gpio_interface_parallel08_2_get()
#endif
//}
t_u8   ttc_gpio_interface_parallel08_3_get( ); //{  macro _driver_gpio_interface_parallel08_3_get(PortPin)
#ifdef ttc_driver_gpio_parallel08_3_get
    #define _driver_gpio_parallel08_3_get() ttc_driver_gpio_parallel08_3_get()
#else
    #define _driver_gpio_parallel08_3_get() ttc_gpio_interface_parallel08_3_get()
#endif
//}
t_u8   ttc_gpio_interface_parallel08_4_get( ); //{  macro _driver_gpio_interface_parallel08_4_get(PortPin)
#ifdef ttc_driver_gpio_parallel08_4_get
    #define _driver_gpio_parallel08_4_get() ttc_driver_gpio_parallel08_4_get()
#else
    #define _driver_gpio_parallel08_4_get() ttc_gpio_interface_parallel08_4_get()
#endif
//}
t_u8   ttc_gpio_interface_parallel08_5_get( ); //{  macro _driver_gpio_interface_parallel08_5_get(PortPin)
#ifdef ttc_driver_gpio_parallel08_5_get
    #define _driver_gpio_parallel08_5_get() ttc_driver_gpio_parallel08_5_get()
#else
    #define _driver_gpio_parallel08_5_get() ttc_gpio_interface_parallel08_5_get()
#endif
//}
t_u8   ttc_gpio_interface_parallel08_6_get( ); //{  macro _driver_gpio_interface_parallel08_6_get(PortPin)
#ifdef ttc_driver_gpio_parallel08_6_get
    #define _driver_gpio_parallel08_6_get() ttc_driver_gpio_parallel08_6_get()
#else
    #define _driver_gpio_parallel08_6_get() ttc_gpio_interface_parallel08_6_get()
#endif
//}
t_u8   ttc_gpio_interface_parallel08_7_get( ); //{  macro _driver_gpio_interface_parallel08_7_get(PortPin)
#ifdef ttc_driver_gpio_parallel08_7_get
    #define _driver_gpio_parallel08_7_get() ttc_driver_gpio_parallel08_7_get()
#else
    #define _driver_gpio_parallel08_7_get() ttc_gpio_interface_parallel08_7_get()
#endif
//}
t_u8   ttc_gpio_interface_parallel08_8_get( ); //{  macro _driver_gpio_interface_parallel08_8_get(PortPin)
#ifdef ttc_driver_gpio_parallel08_8_get
    #define _driver_gpio_parallel08_8_get() ttc_driver_gpio_parallel08_8_get()
#else
    #define _driver_gpio_parallel08_8_get() ttc_gpio_interface_parallel08_8_get()
#endif
//}
t_u8   ttc_gpio_interface_parallel08_9_get( ); //{  macro _driver_gpio_interface_parallel08_9_get(PortPin)
#ifdef ttc_driver_gpio_parallel08_9_get
    #define _driver_gpio_parallel08_9_get() ttc_driver_gpio_parallel08_9_get()
#else
    #define _driver_gpio_parallel08_9_get() ttc_gpio_interface_parallel08_9_get()
#endif
//}
t_u8   ttc_gpio_interface_parallel08_10_get( ); //{  macro _driver_gpio_interface_parallel08_10_get(PortPin)
#ifdef ttc_driver_gpio_parallel08_10_get
    #define _driver_gpio_parallel08_10_get() ttc_driver_gpio_parallel08_10_get()
#else
    #define _driver_gpio_parallel08_10_get() ttc_gpio_interface_parallel08_10_get()
#endif
//}
t_u16 ttc_gpio_interface_parallel16_10_get(); //{  macro _driver_gpio_interface_parallel16_10_get()
#ifdef ttc_driver_gpio_parallel16_10_get
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_10_get ttc_driver_gpio_parallel16_10_get
#else
    #define _driver_gpio_parallel16_10_get ttc_gpio_interface_parallel16_10_get
#endif
//}
void ttc_gpio_interface_parallel16_10_put( t_u16 Value ); //{  macro _driver_gpio_interface_parallel16_10_put(Value)
#ifdef ttc_driver_gpio_parallel16_10_put
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_10_put ttc_driver_gpio_parallel16_10_put
#else
    #define _driver_gpio_parallel16_10_put ttc_gpio_interface_parallel16_10_put
#endif
//}
t_u16 ttc_gpio_interface_parallel16_1_get(); //{  macro _driver_gpio_interface_parallel16_1_get()
#ifdef ttc_driver_gpio_parallel16_1_get
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_1_get ttc_driver_gpio_parallel16_1_get
#else
    #define _driver_gpio_parallel16_1_get ttc_gpio_interface_parallel16_1_get
#endif
//}
void ttc_gpio_interface_parallel16_1_put( t_u16 Value ); //{  macro _driver_gpio_interface_parallel16_1_put(Value)
#ifdef ttc_driver_gpio_parallel16_1_put
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_1_put ttc_driver_gpio_parallel16_1_put
#else
    #define _driver_gpio_parallel16_1_put ttc_gpio_interface_parallel16_1_put
#endif
//}
t_u16 ttc_gpio_interface_parallel16_2_get(); //{  macro _driver_gpio_interface_parallel16_2_get()
#ifdef ttc_driver_gpio_parallel16_2_get
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_2_get ttc_driver_gpio_parallel16_2_get
#else
    #define _driver_gpio_parallel16_2_get ttc_gpio_interface_parallel16_2_get
#endif
//}
void ttc_gpio_interface_parallel16_2_put( t_u16 Value ); //{  macro _driver_gpio_interface_parallel16_2_put(Value)
#ifdef ttc_driver_gpio_parallel16_2_put
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_2_put ttc_driver_gpio_parallel16_2_put
#else
    #define _driver_gpio_parallel16_2_put ttc_gpio_interface_parallel16_2_put
#endif
//}
t_u16 ttc_gpio_interface_parallel16_3_get(); //{  macro _driver_gpio_interface_parallel16_3_get()
#ifdef ttc_driver_gpio_parallel16_3_get
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_3_get ttc_driver_gpio_parallel16_3_get
#else
    #define _driver_gpio_parallel16_3_get ttc_gpio_interface_parallel16_3_get
#endif
//}
void ttc_gpio_interface_parallel16_3_put( t_u16 Value ); //{  macro _driver_gpio_interface_parallel16_3_put(Value)
#ifdef ttc_driver_gpio_parallel16_3_put
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_3_put ttc_driver_gpio_parallel16_3_put
#else
    #define _driver_gpio_parallel16_3_put ttc_gpio_interface_parallel16_3_put
#endif
//}
t_u16 ttc_gpio_interface_parallel16_4_get(); //{  macro _driver_gpio_interface_parallel16_4_get()
#ifdef ttc_driver_gpio_parallel16_4_get
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_4_get ttc_driver_gpio_parallel16_4_get
#else
    #define _driver_gpio_parallel16_4_get ttc_gpio_interface_parallel16_4_get
#endif
//}
void ttc_gpio_interface_parallel16_4_put( t_u16 Value ); //{  macro _driver_gpio_interface_parallel16_4_put(Value)
#ifdef ttc_driver_gpio_parallel16_4_put
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_4_put ttc_driver_gpio_parallel16_4_put
#else
    #define _driver_gpio_parallel16_4_put ttc_gpio_interface_parallel16_4_put
#endif
//}
t_u16 ttc_gpio_interface_parallel16_5_get(); //{  macro _driver_gpio_interface_parallel16_5_get()
#ifdef ttc_driver_gpio_parallel16_5_get
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_5_get ttc_driver_gpio_parallel16_5_get
#else
    #define _driver_gpio_parallel16_5_get ttc_gpio_interface_parallel16_5_get
#endif
//}
void ttc_gpio_interface_parallel16_5_put( t_u16 Value ); //{  macro _driver_gpio_interface_parallel16_5_put(Value)
#ifdef ttc_driver_gpio_parallel16_5_put
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_5_put ttc_driver_gpio_parallel16_5_put
#else
    #define _driver_gpio_parallel16_5_put ttc_gpio_interface_parallel16_5_put
#endif
//}
t_u16 ttc_gpio_interface_parallel16_6_get(); //{  macro _driver_gpio_interface_parallel16_6_get()
#ifdef ttc_driver_gpio_parallel16_6_get
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_6_get ttc_driver_gpio_parallel16_6_get
#else
    #define _driver_gpio_parallel16_6_get ttc_gpio_interface_parallel16_6_get
#endif
//}
void ttc_gpio_interface_parallel16_6_put( t_u16 Value ); //{  macro _driver_gpio_interface_parallel16_6_put(Value)
#ifdef ttc_driver_gpio_parallel16_6_put
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_6_put ttc_driver_gpio_parallel16_6_put
#else
    #define _driver_gpio_parallel16_6_put ttc_gpio_interface_parallel16_6_put
#endif
//}
t_u16 ttc_gpio_interface_parallel16_7_get(); //{  macro _driver_gpio_interface_parallel16_7_get()
#ifdef ttc_driver_gpio_parallel16_7_get
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_7_get ttc_driver_gpio_parallel16_7_get
#else
    #define _driver_gpio_parallel16_7_get ttc_gpio_interface_parallel16_7_get
#endif
//}
void ttc_gpio_interface_parallel16_7_put( t_u16 Value ); //{  macro _driver_gpio_interface_parallel16_7_put(Value)
#ifdef ttc_driver_gpio_parallel16_7_put
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_7_put ttc_driver_gpio_parallel16_7_put
#else
    #define _driver_gpio_parallel16_7_put ttc_gpio_interface_parallel16_7_put
#endif
//}
t_u16 ttc_gpio_interface_parallel16_8_get(); //{  macro _driver_gpio_interface_parallel16_8_get()
#ifdef ttc_driver_gpio_parallel16_8_get
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_8_get ttc_driver_gpio_parallel16_8_get
#else
    #define _driver_gpio_parallel16_8_get ttc_gpio_interface_parallel16_8_get
#endif
//}
void ttc_gpio_interface_parallel16_8_put( t_u16 Value ); //{  macro _driver_gpio_interface_parallel16_8_put(Value)
#ifdef ttc_driver_gpio_parallel16_8_put
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_8_put ttc_driver_gpio_parallel16_8_put
#else
    #define _driver_gpio_parallel16_8_put ttc_gpio_interface_parallel16_8_put
#endif
//}
t_u16 ttc_gpio_interface_parallel16_9_get(); //{  macro _driver_gpio_interface_parallel16_9_get()
#ifdef ttc_driver_gpio_parallel16_9_get
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_9_get ttc_driver_gpio_parallel16_9_get
#else
    #define _driver_gpio_parallel16_9_get ttc_gpio_interface_parallel16_9_get
#endif
//}
void ttc_gpio_interface_parallel16_9_put( t_u16 Value ); //{  macro _driver_gpio_interface_parallel16_9_put(Value)
#ifdef ttc_driver_gpio_parallel16_9_put
    // enable following line to forward interface function as a macro definition
    #define _driver_gpio_parallel16_9_put ttc_driver_gpio_parallel16_9_put
#else
    #define _driver_gpio_parallel16_9_put ttc_gpio_interface_parallel16_9_put
#endif
//}
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_gpio_interface_foo(t_ttc_gpio_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TTC_GPIO_INTERFACE_H
