/** { ttc_network_interface.h *************************************************
 *
 *                           The ToolChain
 *
 *   Interface layer between high- and low-level driver for NETWORK device.
 *
 *   *_interface_*() functions may be redirected to low-level implementations in
 *   two ways:
 *
 *   1) Macro Definition
 *      This applies if only one architecture can be available during runtime.
 *      One example is a GPIO driver for current microcontroller architecture.
 *      Only one type of microcontroller can be available during runtime.
 *
 *   2) Selector Function
 *      Applies if more than one architecture can be availabe during runtime.
 *      A function will select the corresponding low-level driver via a switch-,
 *      if-else construct of via a vector table.
 *      An example can be a display driver. It is possible to connect more than
 *      one different displays to a microcontroller. So every interface function has
 *      to choose, which low-level driver should be used.
 *
 *  Adding a new low-level implementation may require changes in this file.
 *  
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_NETWORK_INTERFACE_H
#define TTC_NETWORK_INTERFACE_H

//{ Includes *************************************************************

#include "../ttc_basic.h"
#include "../ttc_list_types.h"

#ifdef EXTENSION_network_6lowpan_uip
#  include "../network/network_6lowpan.h"
#else
#  include "../network/mac_ieee_802_15_4/network_mac.h"
#endif
//InsertArchitectureIncludes above (DO NOT REMOVE THIS LINE!)

#include "../ttc_network_types.h"

//} Includes
//{ declare interface between high- and low-level implementation *********

e_ttc_network_errorcode  ttc_network_interface_init( t_ttc_network_config* Config ); //{
#ifdef Driver_network_init
// enable following line to forward interface function as a macro definition
#  define ttc_network_interface_init(Config) Driver_network_init(Config)
#else
#  warning: missing low-level implementation for Driver_network_init()!
#endif
//}
void ttc_network_interface_prepare(); //{
#ifdef Driver_network_prepare
// enable following line to forward interface function as a macro definition
#  define ttc_network_interface_prepare(Config) Driver_network_prepare(Config)
#else
#  warning: missing low-level implementation for Driver_network_prepare()!
#endif
//}
e_ttc_network_errorcode ttc_network_interface_load_defaults( t_ttc_network_config* Config ); //{
#ifdef Driver_network_load_defaults
// enable following line to forward interface function as a macro definition
#  define ttc_network_interface_load_defaults(Config) Driver_network_load_defaults(Config)
#else
#  warning: missing low-level implementation for Driver_network_interface_load_defaults()!
#endif
//}
e_ttc_network_errorcode ttc_network_interface_deinit( t_ttc_network_config* Config ); //{
#ifdef Driver_network_deinit
// enable following line to forward interface function as a macro definition
#  define ttc_network_interface_deinit(Config) Driver_network_deinit(Config)
#else
#  warning: missing low-level implementation for Driver_network_deinit()!
#endif
//}
e_ttc_network_errorcode ttc_network_interface_reset( t_ttc_network_config* Config ); //{
#ifdef Driver_network_reset
// enable following line to forward interface function as a macro definition
#  define ttc_network_interface_reset(Config) Driver_network_reset(Config)
#else
#  warning: missing low-level implementation for Driver_network_reset()!
#endif
//}

// additional prototypes go here...

e_ttc_network_errorcode ttc_network_interface_send_direct_message( t_ttc_network_config* Config, ttc_network_address_t Target, t_u8* Buffer, t_base Amount );
#ifdef Driver_network_send_unicast
# define ttc_network_interface_send_direct_message(Config, Target, Buffer, Amount) Driver_network_send_unicast(Config, Target, Buffer, Amount)
#else
# warning Missing implementation for Driver_network_send_unicast()!
#endif

e_ttc_network_errorcode ttc_network_interface_send_broadcast_message( t_ttc_network_config* Config, t_u8* Buffer, t_base Amount );
#ifdef Driver_network_send_broadcast
# define ttc_network_interface_send_broadcast_message(Config, Buffer, Amount) Driver_network_send_broadcast(Config, Buffer, Amount)
#else
# warning Missing implementation for Driver_network_send_broadcast()!
#endif

void ttc_network_interface_receive( t_ttc_network_config* Config );
#ifdef Driver_network_receive
# define ttc_network_interface_receive(Config) Driver_network_receive(Config)
#else
# warning Missing implementation for Driver_network_receive()!
#endif

t_ttc_list* ttc_network_interface_get_neighbours( t_ttc_network_config* Config );
#ifdef Driver_network_get_neighbours
# define ttc_network_interface_get_neighbours(Config) Driver_network_get_neighbours(Config)
#else
# warning Missing implementation for Driver_network_get_neighbours()!
#endif

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _driver_network_interface_foo(t_ttc_network_config* Config)


//}PrivateFunctions

#endif //TTC_NETWORK_INTERFACE_H
