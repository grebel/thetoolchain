/** { ttc_basic_interface.c ************************************************
 *
 *                           The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Interface layer between high- and low-level driver for BASIC device.
 *
 *  The functions implemented here are used when additional runtime
 *  computations arerequired to call the corresponding low-level driver.
 *  These computations should be held very simple and fast. Every additional
 *
 *  Adding a new low-level implementation may require changes in this file.
 *
 *  Functions in this file may not call ttc_basic_*() functions.
 *  Instead they can call _driver_basic_*() pendants.
 *  E.g.: ttc_basic_reset() -> _driver_basic_reset()
 *
 *  Created from template ttc_device_interface.c revision 20 at 20140603 04:04:24 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_basic_interface.h".
//
#include "ttc_basic_interface.h"
#include "../ttc_interrupt.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ****************************************************{
 *
 * Interfaces ideally do not use any extra memory.
 * Try to use macros and definitions instead.
 *
 */

//InsertGlobalVariables above (DO NOT REMOVE THIS LINE!)

//} Global Variables
//{ Function definitions *************************************************

#ifndef ttc_driver_basic_prepare  // no low-level implementation: use default implementation

void ttc_basic_interface_prepare() {
}

#endif
#ifndef ttc_driver_basic_32_atomic_read_write  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_32_atomic_read_write() (using default implementation)!

t_u32 ttc_basic_interface_32_atomic_read_write( t_u32* Address, t_u32 NewValue ) {
    Assert_BASIC_Writable( Address, ttc_assert_origin_auto );  // pointers must not be NULL

    ttc_interrupt_all_disable();
    t_u32 OldValue = *Address;
    *Address = NewValue;
    ttc_interrupt_all_enable();

    return OldValue;
}

#endif
#ifndef ttc_driver_basic_16_atomic_read_write  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_16_atomic_read_write() (using default implementation)!

t_u16 ttc_basic_interface_16_atomic_read_write( t_u16* Address, t_u16 NewValue ) {
    Assert_BASIC_Writable( Address, ttc_assert_origin_auto );  // pointers must not be NULL

    ttc_interrupt_all_disable();
    t_u16 OldValue = *Address;
    *Address = NewValue;
    ttc_interrupt_all_enable();

    return OldValue;
}

#endif
#ifndef ttc_driver_basic_08_atomic_read_write  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_08_atomic_read_write() (using default implementation)!

t_u8 ttc_basic_interface_08_atomic_read_write( t_u8* Address, t_u8 NewValue ) {
    Assert_BASIC_Writable( Address, ttc_assert_origin_auto );  // pointers must not be NULL

    ttc_interrupt_all_disable();
    t_u8 OldValue = *Address;
    *Address = NewValue;
    ttc_interrupt_all_enable();

    return OldValue;
}

#endif
#ifndef ttc_driver_basic_32_atomic_add  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_32_atomic_add() (using default implementation)!

t_u32 ttc_basic_interface_32_atomic_add( t_u32* Address, t_s32 Amount ) {
    Assert_BASIC_Writable( Address, ttc_assert_origin_auto );  // pointers must not be NULL

    ttc_interrupt_all_disable();
    t_s32 Value = *Address;
    Value = Value  + Amount;
    *Address = Value;
    ttc_interrupt_all_enable();

    return Value;
}

#endif
#ifndef ttc_driver_basic_16_atomic_add  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_16_atomic_add() (using default implementation)!

t_u16 ttc_basic_interface_16_atomic_add( t_u16* Address, t_s16 Amount ) {
    Assert_BASIC_Writable( Address, ttc_assert_origin_auto );  // pointers must not be NULL

    ttc_interrupt_all_disable();
    t_s16 Value = *Address;
    Value = Value  + Amount;
    *Address = Value;
    ttc_interrupt_all_enable();

    return Value;
}

#endif
#ifndef ttc_driver_basic_08_atomic_add  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_08_atomic_add() (using default implementation)!

t_u8 ttc_basic_interface_08_atomic_add( t_u8* Address, t_s8 Amount ) {
    Assert_BASIC_Writable( Address, ttc_assert_origin_auto );  // pointers must not be NULL

    ttc_interrupt_all_disable();
    t_s8 Value = *Address;
    Value = Value  + Amount;
    *Address = Value;
    ttc_interrupt_all_enable();

    return Value;
}

#endif
#ifndef ttc_driver_basic_16_write_little_endian  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_16_write_little_endian() (using default implementation)!

void ttc_basic_interface_16_write_little_endian( t_u8* Buffer, t_u16 Data ) {
    Assert_BASIC_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    Buffer[0] = ( Data & 0x00ff ) >>  0;
    Buffer[1] = ( Data & 0xff00 ) >>  8;
}

#endif
#ifndef ttc_driver_basic_16_write_big_endian  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_16_write_big_endian() (using default implementation)!

void ttc_basic_interface_16_write_big_endian( t_u8* Buffer, t_u16 Data ) {
    Assert_BASIC_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    Buffer[0] = ( Data & 0xff00 ) >>  8;
    Buffer[1] = ( Data & 0x00ff ) >>  0;
}

#endif
#ifndef ttc_driver_basic_32_write_little_endian  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_32_write_little_endian() (using default implementation)!

void ttc_basic_interface_32_write_little_endian( t_u8* Buffer, t_u32 Data ) {
    Assert_BASIC_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    Buffer[0] = ( Data & 0x000000ff ) >>  0;
    Buffer[1] = ( Data & 0x0000ff00 ) >>  8;
    Buffer[2] = ( Data & 0x00ff0000 ) >> 16;
    Buffer[3] = ( Data & 0xff000000 ) >> 24;
}

#endif
#ifndef ttc_driver_basic_32_write_big_endian  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_32_write_big_endian() (using default implementation)!

void ttc_basic_interface_32_write_big_endian( t_u8* Buffer, t_u32 Data ) {
    Assert_BASIC_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    Buffer[0] = ( Data & 0xff000000 ) >> 24;
    Buffer[1] = ( Data & 0x00ff0000 ) >> 16;
    Buffer[2] = ( Data & 0x0000ff00 ) >>  8;
    Buffer[3] = ( Data & 0x000000ff ) >>  0;
}

#endif
#ifndef ttc_driver_basic_32_read_big_endian  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_32_read_big_endian() (using default implementation)!

t_u32 ttc_basic_interface_32_read_big_endian( t_u8* Buffer ) {
    Assert_BASIC_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_u32 Value = ( Buffer[0] <<  0 ) |
                  ( Buffer[1] <<  8 ) |
                  ( Buffer[2] << 16 ) |
                  ( Buffer[3] << 24 );

    return Value;
}
#ifndef ttc_driver_basic_32_read_little_endian  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_32_read_little_endian() (using default implementation)!

t_u32 ttc_basic_interface_32_read_little_endian( t_u8* Buffer ) {
    Assert_BASIC_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_u32 Value = ( Buffer[0] << 24 ) |
                  ( Buffer[1] << 16 ) |
                  ( Buffer[2] <<  8 ) |
                  ( Buffer[3] <<  0 );

    return Value;
}

#endif
#ifndef ttc_driver_basic_16_read_big_endian  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_16_read_big_endian() (using default implementation)!

t_u16 ttc_basic_interface_16_read_big_endian( t_u8* Buffer ) {
    Assert_BASIC_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_u16 Value = ( Buffer[0] << 0 ) |
                  ( Buffer[1] << 8 );

    return Value;
}

#endif
#ifndef ttc_driver_basic_16_read_little_endian  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_16_read_little_endian() (using default implementation)!

t_u16 ttc_basic_interface_16_read_little_endian( t_u8* Buffer ) {
    Assert_BASIC_Writable( Buffer, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_u16 Value = ( Buffer[0] << 8 ) |
                  ( Buffer[1] << 0 );

    return Value;
}

#endif

#endif
#ifndef ttc_driver_basic_32_swap_endian  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_32_swap_endian() (using default implementation)!

void ttc_basic_interface_32_swap_endian( t_u32* Variable ) {
    Assert_BASIC_Writable( Variable, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    t_u32 Temp = *Variable;
    *Variable = ( ( Temp & 0x000000ff ) << 24 ) |
                ( ( Temp & 0x0000ff00 ) <<  8 ) |
                ( ( Temp & 0x00ff0000 ) >>  8 ) |
                ( ( Temp & 0xff000000 ) >> 24 );
}

#endif
#ifndef ttc_driver_basic_16_swap_endian  // no low-level implementation: use default implementation
#  warning: missing low-level implementation for ttc_driver_basic_16_swap_endian() (using default implementation)!

void ttc_basic_interface_16_swap_endian( t_u16* Variable ) {
    Assert_BASIC_Writable( Variable, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    t_u16 Temp = *Variable;
    *Variable = ( ( Temp & 0x00ff ) << 8 ) |
                ( ( Temp & 0xff00 ) >> 8 );
}

#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *****************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_basic_interface_foo(t_ttc_basic_config* Config) { ... }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
