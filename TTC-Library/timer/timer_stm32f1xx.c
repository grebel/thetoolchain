/** { timer_stm32f1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for timer devices on stm32f1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 20 at 20140204 09:25:54 UTC
 *
 *  Note: See ttc_timer.h for description of stm32f1xx independent TIMER implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "timer_stm32f1xx.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"
#include "../ttc_interrupt.h"

//{ Global Variables *****************************************************
t_ttc_heap_pool* TimerPool = NULL;
t_ttc_timer_config* HeadEvent = NULL;

// provided by ttc_timer.c
extern void _ttc_timer_isr( t_ttc_timer_config* Config );

//} Global Variables
//{ Function definitions *******************************************************

e_ttc_timer_errorcode timer_stm32f1xx_deinit( t_ttc_timer_config* Config ) {
    Assert_TIMER( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TIMER( Config->LogicalIndex > 0, ttc_assert_origin_auto );
    Config->TIMER_Arch.BaseRegister = stm32f1_timer_physical_index_2_address( Config->PhysicalIndex );

    if ( Config->TimePeriod <= 30000000 ) {
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );

        switch ( Config->LogicalIndex ) {
            case 1: { // load low-level configuration of third timer device
                RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM2, DISABLE );
                break;
            }
            case 2: { // load low-level configuration of fourth timer device
                RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM3, DISABLE );
                break;
            }
            case 3: { // load low-level configuration of fifth timer device
                RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM4, DISABLE );
                break;
            }
            case 4: { // load low-level configuration of sixth timer device
                RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM5, DISABLE );
                break;
            }
            case 5: { // load low-level configuration of tenth timer device
                //Config_stm32f1->BaseRegister = (t_register_stm32f1xx_timer*)TIM10; // load adress of register base of TIMER device
                RCC_APB1PeriphClockCmd( RCC_APB2Periph_TIM9, DISABLE );
                break;
            }
            case 6: { // load low-level configuration of eleventh timer device
                RCC_APB1PeriphClockCmd( RCC_APB2Periph_TIM10, DISABLE );
                break;
            }
            case 7: { // load low-level configuration of twelfth timer device
                RCC_APB1PeriphClockCmd( RCC_APB2Periph_TIM11, DISABLE );
                break;
            }
            case 8: { // load low-level configuration of thirteenth timer device
                RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM12, DISABLE );
                break;
            }
            case 9: { // load low-level configuration of fourteenth timer device
                RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM13, DISABLE );
                break;
            }
            case 10: { // load low-level configuration of fifteenth timer device
                RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM14, DISABLE );
                break;
            }
            default: Assert_TIMER( 0, ttc_assert_origin_auto );
                break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
        }
    }
//    TIM_DeInit(stm32f1_timer_physical_index_2_address(Config->PhysicalIndex));
    Config->Flags.Bits.Initialized = 0;

    return ( e_ttc_timer_errorcode ) 0; //ec_timer_OK
}
e_ttc_timer_errorcode timer_stm32f1xx_get_features( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TIMER( Config->LogicalIndex > 0, ttc_assert_origin_auto );

    Config->Architecture = ta_timer_stm32f1xx;
    Config->Flags.All = 0;

    return ( e_ttc_timer_errorcode ) 0;
}
e_ttc_timer_errorcode timer_stm32f1xx_init( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Config->TIMER_Arch.BaseRegister = stm32f1_timer_physical_index_2_address( Config->PhysicalIndex );

    if ( Config->TimePeriod <= 30000000 )
    { Config->TIMER_Arch.BaseRegister->CR1.All |= ( t_u16 ) 0x0001; }

    Config->Flags.Bits.Initialized = 1;

    return ( e_ttc_timer_errorcode ) 0;
}
e_ttc_timer_errorcode timer_stm32f1xx_load_defaults( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TIMER( Config->LogicalIndex > 0, ttc_assert_origin_auto );

    switch ( Config->LogicalIndex ) {         // determine base register and other low-level configuration data
        case 1: { // load low-level configuration of third timer device
            RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM2, ENABLE );
            break;
        }
        case 2: { // load low-level configuration of fourth timer device
            RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM3, ENABLE );
            break;
        }
        case 3: { // load low-level configuration of fifth timer device
            RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM4, ENABLE );
            break;
        }
        case 4: { // load low-level configuration of sixth timer device
            RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM5, ENABLE );
            break;
        }
        case 5: { // load low-level configuration of tenth timer device
            //Config_stm32f1->BaseRegister = (t_register_stm32f1xx_timer*)TIM10; // load adress of register base of TIMER device
            RCC_APB1PeriphClockCmd( RCC_APB2Periph_TIM9, ENABLE );
            break;
        }
        case 6: { // load low-level configuration of eleventh timer device
            RCC_APB1PeriphClockCmd( RCC_APB2Periph_TIM10, ENABLE );
            break;
        }
        case 7: { // load low-level configuration of twelfth timer device
            RCC_APB1PeriphClockCmd( RCC_APB2Periph_TIM11, ENABLE );
            break;
        }
        case 8: { // load low-level configuration of thirteenth timer device
            RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM12, ENABLE );
            break;
        }
        case 9: { // load low-level configuration of fourteenth timer device
            RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM13, ENABLE );
            break;
        }
        case 10: { // load low-level configuration of fifteenth timer device
            RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM14, ENABLE );
            break;
        }
        default: Assert_TIMER( 0, ttc_assert_origin_auto );
            break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
    }

    return ( e_ttc_timer_errorcode ) 0;
}
void timer_stm32f1xx_prepare() {
    TimerPool = ttc_heap_pool_create( sizeof( t_ttc_timer_config ), 3 ); //Create the memory pool for timer events
    if ( TimerPool != NULL )
    { ttc_task_create( _timer_stm32f1xx_, "checkNextEvent", 64, NULL, 1, NULL ); }
}
void timer_stm32f1xx_reset( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    t_register_stm32f1xx_timer* TIMER = stm32f1_timer_physical_index_2_address( Config->PhysicalIndex );

    if ( Config->TimePeriod <= 30000000 ) {
        TIMER->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );
        TIMER->CNT = 0;
        TIMER->CR1.All |= ( t_u16 )0x0001;
    }
    else {
        ttc_task_critical_begin();
        t_ttc_timer_config* CurrentEvent = HeadEvent; //Take the first element of the event list
        ttc_task_critical_end();

        if ( CurrentEvent->LogicalIndex == Config->LogicalIndex ) {
            HeadEvent = CurrentEvent->Next;
            _timer_stm32f1xx_insert_( CurrentEvent );
        }
        else {
            t_ttc_timer_config* LastEvent = HeadEvent;
            CurrentEvent = CurrentEvent->Next;
            while ( CurrentEvent->Next ) {
                if ( CurrentEvent->LogicalIndex == Config->LogicalIndex ) {
                    LastEvent->Next = CurrentEvent->Next;
                    _timer_stm32f1xx_insert_( CurrentEvent );
                }
                else {
                    LastEvent = LastEvent->Next;
                    CurrentEvent = CurrentEvent->Next;
                }
            }
        }
    }
}
void timer_stm32f1xx_change_period( t_ttc_timer_config* Config, t_u32 Period ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TIMER( Period > 0, ttc_assert_origin_auto );
    Config->TIMER_Arch.BaseRegister = stm32f1_timer_physical_index_2_address( Config->PhysicalIndex );

    if ( ( Config->TimePeriod > 100 ) && ( Config->TimePeriod <= 1000 ) ) {
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );
        Config->TIMER_Arch.BaseRegister->CNT = ( t_u16 )( Config->TimePeriod / 100 ) * 3;
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( t_u16 ) 0x0001;
    }
    else if ( ( Config->TimePeriod > 1000 ) && ( Config->TimePeriod <= 5000 ) ) {
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );
        Config->TIMER_Arch.BaseRegister->CNT = ( t_u16 )( ( Config->TimePeriod / 1000 ) * 7 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( t_u16 ) 0x0001;
    }
    else if ( ( Config->TimePeriod > 5000 ) && ( Config->TimePeriod <= 30000000 ) ) {
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );
        Config->TIMER_Arch.BaseRegister->CNT = ( t_u16 )( ( Config->TimePeriod / 1000 ) * 2 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( t_u16 ) 0x0001;
    }
    else { Config->TimePeriod = Config->TimePeriod; }
}
t_u32 timer_stm32f1xx_read_value( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TIMER( Config->TimePeriod < 0, ttc_assert_origin_auto );
    Config->TIMER_Arch.BaseRegister = stm32f1_timer_physical_index_2_address( Config->PhysicalIndex );

    if ( Config->TimePeriod <= 5000 )
    { return ( t_u32 )Config->TIMER_Arch.BaseRegister->CNT; }
//        return (t_u32)TIM_GetCounter(stm32f1_timer_physical_index_2_address(Config->PhysicalIndex));
    else if ( ( Config->TimePeriod > 1000 ) && ( Config->TimePeriod <= 30000000 ) )
    { return ( t_u32 )Config->TIMER_Arch.BaseRegister->CNT; }
//        return (t_u32)((TIM_GetCounter(stm32f1_timer_physical_index_2_address(Config->PhysicalIndex))/2)*1000);
    else { return ( t_u32 )( ttc_systick_get_elapsed_usecs() - Config->TimeStart ); }
}
e_ttc_timer_errorcode timer_stm32f1xx_set( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TIMER( Config->TimePeriod > 0, ttc_assert_origin_auto ); // pointers must not be NULL

    Config->TIMER_Arch.BaseRegister = stm32f1_timer_physical_index_2_address( Config->PhysicalIndex );
    if ( timer_stm32f1xx_check_initialized( Config ) == 1 )
    { return ec_timer_DeviceInUse; }
    else { timer_stm32f1xx_deinit( Config ); }

    timer_stm32f1xx_load_defaults( Config );

    /* UNPREDICTABLE RESULT */
    /*if(Config->TimePeriod <= 100){
        Config->TIMER_Arch.BaseRegister->PSC = 576;
        Config->TIMER_Arch.BaseRegister->ARR = (Config->TimePeriod)/8;
        Config->TIMER_Arch.BaseRegister->RCR_ = 0;
        Config->TIMER_Arch.BaseRegister->CR1.All &= (t_u16)~((t_u16)0x0300);
        Config->TIMER_Arch.BaseRegister->CR1.All |= ((t_u16)0x0000);
        Config->TIMER_Arch.BaseRegister->CR1.All |= ((t_u16)0x0080);
        Config->TIMER_Arch.BaseRegister->EGR.All |= ((t_u16)0x0001);
        Config->TIMER_Arch.BaseRegister->SR.All = (t_u16)0x0;

        //Interruption Enabling
        if(Config->Function != NULL){
            Config->Interrupt_Updating = ttc_interrupt_init(tit_TIMER_Updating,
                                   Config->PhysicalIndex,
                                   _driver_timer_isr,  // will be passed as argument to isr_Switch()
                                   (void *)Config,
                                   NULL,
                                   NULL);
                Assert_TIMER(Config->Interrupt_Updating, ttc_assert_origin_auto);
                ttc_interrupt_enable_timer(Config->Interrupt_Updating);
        }

    }
    else */
    if ( ( Config->TimePeriod > 100 ) && ( Config->TimePeriod <= 1000 ) ) {
        Config->TIMER_Arch.BaseRegister->PSC = 2304;
        Config->TIMER_Arch.BaseRegister->ARR = Config->TimePeriod / 100 * 3;
        Config->TIMER_Arch.BaseRegister->RCR_ = 0;
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )~( ( t_u16 )0x0300 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( ( t_u16 )0x0100 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( ( t_u16 )0x0080 );
        Config->TIMER_Arch.BaseRegister->EGR.All |= ( ( t_u16 )0x0001 );
        Config->TIMER_Arch.BaseRegister->SR.All = ( t_u16 )0x0;

        //Interruption Enabling
        if ( Config->Function != NULL ) {
            Config->Interrupt_Updating = ttc_interrupt_init( tit_TIMER_Updating,
                                                             Config->PhysicalIndex,
                                                             _driver_timer_isr,  // will be passed as argument to isr_Switch()
                                                             ( void* )Config,
                                                             NULL,
                                                             NULL );
            Assert_TIMER( Config->Interrupt_Updating, ttc_assert_origin_auto );
            ttc_interrupt_enable_timer( Config->Interrupt_Updating );
        }
    }
    else if ( ( Config->TimePeriod > 1000 ) && ( Config->TimePeriod <= 5000 ) ) {
        Config->TIMER_Arch.BaseRegister->PSC = 8999;
        Config->TIMER_Arch.BaseRegister->ARR = ( ( Config->TimePeriod ) / 1000 ) * 7;
        Config->TIMER_Arch.BaseRegister->RCR_ = 0;
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )~( ( t_u16 )0x0300 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( ( t_u16 )0x0100 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( ( t_u16 )0x0080 );
        Config->TIMER_Arch.BaseRegister->EGR.All |= ( ( t_u16 )0x0001 );
        Config->TIMER_Arch.BaseRegister->SR.All = ( t_u16 )0x0;

        //Interruption Enabling
        if ( Config->Function != NULL ) {
            Config->Interrupt_Updating = ttc_interrupt_init( tit_TIMER_Updating,
                                                             Config->PhysicalIndex,
                                                             _driver_timer_isr,  // will be passed as argument to isr_Switch()
                                                             ( void* )Config,
                                                             NULL,
                                                             NULL );
            Assert_TIMER( Config->Interrupt_Updating, ttc_assert_origin_auto );
            ttc_interrupt_enable_timer( Config->Interrupt_Updating );
        }
    }
    else if ( ( Config->TimePeriod > 5000 ) && ( Config->TimePeriod <= 30000000 ) ) {
        Config->TIMER_Arch.BaseRegister->PSC = 35999;
        Config->TIMER_Arch.BaseRegister->ARR = ( ( Config->TimePeriod ) / 1000 ) * 2;
        Config->TIMER_Arch.BaseRegister->RCR_ = 0;
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )~( ( t_u16 )0x0300 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( ( t_u16 )0x0100 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( ( t_u16 )0x0080 );
        Config->TIMER_Arch.BaseRegister->EGR.All |= ( ( t_u16 )0x0001 );
        Config->TIMER_Arch.BaseRegister->SR.All = ( t_u16 )0x0;

        //Interruption Enabling
        if ( Config->Function != NULL ) {
            Config->Interrupt_Updating = ttc_interrupt_init( tit_TIMER_Updating,
                                                             Config->PhysicalIndex,
                                                             _driver_timer_isr,  // will be passed as argument to isr_Switch()
                                                             ( void* )Config,
                                                             NULL,
                                                             NULL );
            Assert_TIMER( Config->Interrupt_Updating, ttc_assert_origin_auto );
            ttc_interrupt_enable_timer( Config->Interrupt_Updating );
        }
    }
    else {
        //Software Timer comes here
        Config->TimeStart = ttc_systick_get_elapsed_usecs();
        _timer_stm32f1xx_insert_( Config );
    }

    timer_stm32f1xx_init( Config );
    return ec_timer_OK;
}
bool timer_stm32f1xx_check_initialized( t_ttc_timer_config* Config ) {
    return Config->Flags.Bits.Initialized;
}
t_register_stm32f1xx_timer* stm32f1_timer_physical_index_2_address( t_u8 Index ) {
    Assert_TIMER( Index > 0, ttc_assert_origin_auto );
    Assert_TIMER( Index < TTC_INTERRUPT_TIMER_AMOUNT, ttc_assert_origin_auto );

    switch ( Index ) {         // determine base register and other low-level configuration data
            #ifdef TTC_TIMER1
        case TTC_TIMER1: return ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER2;
            #endif
            #ifdef TTC_TIMER2
        case TTC_TIMER2: return ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER3;
            #endif
            #ifdef TTC_TIMER3
        case TTC_TIMER3: return ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER4;
            #endif
            #ifdef TTC_TIMER4
        case TTC_TIMER4: return ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER5;
            #endif
            #ifdef TTC_TIMER5
        case TTC_TIMER5: return ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER9;
            #endif
            #ifdef TTC_TIMER6
        case TTC_TIMER9: return ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER10;
            #endif
            #ifdef TTC_TIMER7
        case TTC_TIMER10: return ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER11;
            #endif
            #ifdef TTC_TIMER8
        case TTC_TIMER11: return ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER12;
            #endif
            #ifdef TTC_TIMER9
        case TTC_TIMER12: return ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER13;
            #endif
            #ifdef TTC_TIMER10
        case TTC_TIMER13: return ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER14;
            #endif

        default: Assert_TIMER( 0, ttc_assert_origin_auto ); break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
    }

    return ( t_register_stm32f1xx_timer* ) - 1;
}
void timer_stm32f1xx_reload( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    Config->TIMER_Arch.BaseRegister = stm32f1_timer_physical_index_2_address( Config->PhysicalIndex );
    if ( ( Config->TimePeriod > 100 ) && ( Config->TimePeriod <= 1000 ) ) {
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );
        Config->TIMER_Arch.BaseRegister->CNT = ( t_u16 )( Config->TimePeriod / 100 ) * 3;
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( t_u16 ) 0x0001;
    }
    else if ( ( Config->TimePeriod > 1000 ) && ( Config->TimePeriod <= 5000 ) ) {
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );
        Config->TIMER_Arch.BaseRegister->CNT = ( t_u16 )( ( Config->TimePeriod / 1000 ) * 7 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( t_u16 ) 0x0001;
    }
    else if ( ( Config->TimePeriod > 5000 ) && ( Config->TimePeriod <= 30000000 ) ) {
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );
        Config->TIMER_Arch.BaseRegister->CNT = ( t_u16 )( ( Config->TimePeriod / 1000 ) * 2 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( t_u16 ) 0x0001;
    }
    else { Config->TimePeriod = Config->TimePeriod; }
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)
void _timer_stm32f1xx_() {
    while ( 1 ) {
        if ( HeadEvent ) {
            if ( HeadEvent->TimePeriod <= ( ttc_systick_get_elapsed_usecs() - HeadEvent->TimeStart ) ) {
                ttc_task_critical_begin();
                t_ttc_timer_config* CurrentEvent = HeadEvent; //Take the first element of the event list
                ttc_task_critical_end();

                if ( HeadEvent->Next ) {
                    ttc_task_critical_begin();
                    HeadEvent = HeadEvent->Next;
                }
                else {
                    ttc_task_critical_begin();
                    HeadEvent = NULL;
                }
                ttc_task_critical_end();

                CurrentEvent->Flags.Bits.Initialized = 0;
                CurrentEvent->Function( CurrentEvent->Argument );
            }
            else { uSleep( HeadEvent->TimePeriod - ( ttc_systick_get_elapsed_usecs() - HeadEvent->TimeStart ) ); }
        }
        else { uSleep( 1000 ); }
    }
}
void _timer_stm32f1xx_manage_( t_base PhysicalIndex, void* Argument ) {
    Assert_TIMER( PhysicalIndex <= TTC_TIMER_AMOUNT, ttc_assert_origin_auto );

    _ttc_timer_isr( ( t_ttc_timer_config* ) Argument );
}
void _timer_stm32f1xx_insert_( t_ttc_timer_config* Config ) {
    // set to TRUE once Event has been found
    bool found = 0;

    if ( !HeadEvent ) {
        ttc_task_critical_begin();
        HeadEvent = Config;
        ttc_task_critical_end();
    }
    else {
        ttc_task_critical_begin();
        t_ttc_timer_config* CurrentEvent = HeadEvent;
        ttc_task_critical_end();
        t_base ActualTime = 0;

        if ( CurrentEvent->TimePeriod <= ( ttc_systick_get_elapsed_usecs() - CurrentEvent->TimeStart ) )
        { ActualTime = 0; }
        else { ActualTime = ( ( CurrentEvent->TimePeriod - ( ttc_systick_get_elapsed_usecs() - CurrentEvent->TimeStart ) ) ); }

        if ( Config->TimePeriod >= ActualTime ) {
            while ( !found ) {
                if ( CurrentEvent->Next ) {
                    ttc_task_critical_begin();
                    t_ttc_timer_config* LastEvent = CurrentEvent;
                    CurrentEvent = CurrentEvent->Next;
                    ttc_task_critical_end();

                    if ( CurrentEvent->TimePeriod < ( ttc_systick_get_elapsed_usecs() - CurrentEvent->TimeStart ) )
                    { ActualTime = 0; }
                    else { ActualTime = ( ( CurrentEvent->TimePeriod - ( ttc_systick_get_elapsed_usecs() - CurrentEvent->TimeStart ) ) ); }

                    if ( Config->TimePeriod <= ActualTime ) {
                        ttc_task_critical_begin();
                        Config->TimeStart = ttc_systick_get_elapsed_usecs();
                        Config->Next = CurrentEvent;
                        LastEvent->Next = Config;
                        ttc_task_critical_end();
                        found = 1;
                    }
                }
                else {
                    ttc_task_critical_begin();
                    Config->TimeStart = ttc_systick_get_elapsed_usecs();
                    Config->Next = NULL;
                    CurrentEvent->Next = Config;
                    ttc_task_critical_end();
                    found = 1;
                }
            }
        }
        else {
            ttc_task_critical_begin();
            Config->TimeStart = ttc_systick_get_elapsed_usecs();
            Config->Next = HeadEvent;
            HeadEvent = Config;
            ttc_task_critical_end();
        }
    }
}

//} private functions
