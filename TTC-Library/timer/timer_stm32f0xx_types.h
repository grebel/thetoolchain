#ifndef ARCHITECTURE_TIMER_TYPES_H
#define ARCHITECTURE_TIMER_TYPES_H

/** { timer_stm32f0xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for TIMER devices on stm32f0xx architectures.
 *  Structures, Enums and Defines being required by ttc_timer_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140203 16:39:49 UTC
 *
 *  Note: See ttc_timer.h for description of architecture independent TIMER implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ******************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_timer_types.h ***********************

typedef struct { // register description (adapt according to stm32f0xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_timer_register;

typedef struct {  // stm32f0xx specific configuration data of single TIMER device
    t_timer_register* BaseRegister;       // base address of TIMER device registers
} __attribute__( ( __packed__ ) ) t_timer_stm32f0xx_config;

// t_ttc_timer_arch is required by ttc_timer_types.h
typedef t_timer_stm32f0xx_config t_ttc_timer_arch;

//} Structures/ Enums


#endif //ARCHITECTURE_TIMER_TYPES_H
