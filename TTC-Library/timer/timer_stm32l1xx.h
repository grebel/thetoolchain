#ifndef TIMER_STM32L1XX_H
#define TIMER_STM32L1XX_H

/** { timer_stm32l1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for timer devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by high-level timer and application.
 *
 *  Note: See ttc_timer.h for description of stm32l1xx independent TIMER implementation.
 *
 *  Authors: Adrián Romero
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_TIMER_STM32L1XX
//
// Implementation status of low-level driver support for timer devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_TIMER_STM32L1XX 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_TIMER_STM32L1XX == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_TIMER_STM32L1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_TIMER_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "timer_stm32l1xx_types.h"
#include "../ttc_interrupt_types.h"
#include "../ttc_interrupt.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_timer_interface.h

/* ???
#define Driver_timer_init()                                         timer_stm32l1xx_init()
#define Driver_timer_load_defaults(Config)                          timer_stm32l1xx_load_defaults(Config)
#define Driver_timer_check_initialized(Config)                      timer_stm32l1xx_check_initialized(Config)
#define Driver_timer_deinit(Config)                                 timer_stm32l1xx_deinit(Config)

#define Driver_timer_set(Config)                                    timer_stm32l1xx_set(Config)
#define Driver_timer_reset(Config)                                  timer_stm32l1xx_reset(Config)
#define Driver_timer_prepare()                                      timer_stm32l1xx_prepare()

#define Driver_timer_start(Register)                            timer_stm32l1xx_start(Register)
#define Driver_timer_stop(Register)                             timer_stm32l1xx_stop(Register)

#define Driver_timer_set_counter(Register, Value)               timer_stm32l1xx_set_counter(Register, Value)
#define Driver_timer_get_counter(Register)                      timer_stm32l1xx_get_counter(Register)
#define Driver_timer_get_state(LogicalIndex)                        timer_stm32l1xx_get_state(LogicalIndex)
*/
#define ttc_driver_timer_change_period(Config, Period) timer_stm32l1xx_change_period(Config, Period)
#define ttc_driver_timer_deinit(Config)                timer_stm32l1xx_deinit(Config)
#define ttc_driver_timer_get_features(Config)          timer_stm32l1xx_get_features(Config)
#define ttc_driver_timer_init(Config)                  timer_stm32l1xx_init(Config)
#define ttc_driver_timer_load_defaults(Config)         timer_stm32l1xx_load_defaults(Config)
#define ttc_driver_timer_prepare()                     timer_stm32l1xx_prepare()
#define ttc_driver_timer_read_value(Config)            timer_stm32l1xx_read_value(Config)
#define ttc_driver_timer_reset(Config)                 timer_stm32l1xx_reset(Config)
#define ttc_driver_timer_set(Config)                   timer_stm32l1xx_set(Config)
#define ttc_driver_timer_reload(Config)                timer_stm32l1xx_reload(Config)


//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_timer.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_timer.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

/** initializes single TIMER from 1 mSec
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
e_ttc_timer_errorcode timer_stm32l1xx_init();

/** loads configuration of indexed TIMER interface with default values
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_timer_errorcode timer_stm32l1xx_load_defaults( t_ttc_timer_config* Config );

/** checks if indexed TIMER bus already has been initialized
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return              == TRUE: indexed TIMER unit is already initialized
 */
bool timer_stm32l1xx_check_initialized( t_ttc_timer_config* Config );

/** shutdown single TIMER device
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been shutdown successfully; != 0: error-code
 */
e_ttc_timer_errorcode timer_stm32l1xx_deinit( t_ttc_timer_config* Config );

t_register_stm32l1xx_timer* timer_stm32l1xx_physical_index_2_address( t_u8 Index );

/** initializes single TIMER up to 1 mSec
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
e_ttc_timer_errorcode timer_stm32l1xx_set( t_ttc_timer_config* Config );

/** reset configuration of indexed TIMER device into default state
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
e_ttc_timer_errorcode stm32l1xx_timer_reset( t_ttc_timer_config* Config );

void timer_stm32l1xx_prepare(); //Just to avoid warnings



/** starts a single TIMER
 *
 * @param LogicalIndex  Timer Index to start
 *
 */
e_ttc_timer_errorcode timer_stm32l1xx_start( t_register_stm32l1xx_timer* Register );

/** stops a single TIMER, remaining the information on the data structure
 *
 * @param LogicalIndex  Timer Index to stop
 *
 */
e_ttc_timer_errorcode timer_stm32l1xx_stop( t_register_stm32l1xx_timer* Register );



/** set a counter value on a single TIMER
 *
 * @param LogicalIndex  Timer Index to set a time period
 * @param Value         Value to load on the counter
 * @param TimeScale     0: uSeconds time scale, 1: mSeconds time scale
 *
 */
void timer_stm32l1xx_set_counter( t_register_stm32l1xx_timer* Register, t_u16 Value );

/** gets the counter value of a single TIMER
 *
 * @param LogicalIndex  Timer Index to get the counter value
 * @return              Counter value (TIMx->CNT)
 *
 */
t_u16 timer_stm32l1xx_get_counter( t_register_stm32l1xx_timer* Register );

/** gets the state of a single TIMER
 *
 * @param LogicalIndex  Timer Index to get the status value
 * @return              Status value (TIMx->SR)
 *
 */
//e_ttc_timer_statuscode timer_stm32l1xx_get_state(t_u8 LogicalIndex);



/** set a new reload value for given timer
 *
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @param Period        reload value (us)
 */
void timer_stm32l1xx_change_period( t_ttc_timer_config* Config, t_u32 Period );


/** shutdown single TIMER unit device
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been shutdown successfully; != 0: error-code
 */
e_ttc_timer_errorcode timer_stm32l1xx_deinit( t_ttc_timer_config* Config );


/** fills out given Config with maximum valid values for indexed TIMER
 * @param Config        = pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_timer_errorcode timer_stm32l1xx_get_features( t_ttc_timer_config* Config );


/** initializes single TIMER unit for operation
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
e_ttc_timer_errorcode timer_stm32l1xx_init( t_ttc_timer_config* Config );


/** loads configuration of indexed TIMER unit with default values
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_timer_errorcode timer_stm32l1xx_load_defaults( t_ttc_timer_config* Config );


/** Prepares timer driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void timer_stm32l1xx_prepare();


/** read current timer value
 *
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return   current elapsed time in given timer (us)
 */
t_u32 timer_stm32l1xx_read_value( t_ttc_timer_config* Config );


/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 * @param Config   =
 */
void timer_stm32l1xx_reset( t_ttc_timer_config* Config );


/** set a timer with a given value
 *
 * @param Config          pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @param TaskFunction    CallBack Function that will be executed
 * @param Argument        passed as argument to Function()
 * @param Period          reload value (us)
 * @return   =
 */
e_ttc_timer_errorcode timer_stm32l1xx_set( t_ttc_timer_config* Config );


/** Reload value for given timer
 *
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 *
 */
void timer_stm32l1xx_reload( t_ttc_timer_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _timer_stm32l1xx_foo(t_ttc_timer_config* Config)




//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

void _timer_stm32l1xx_select_output_trigger( t_register_stm32l1xx_timer* Register, t_u16 Trigger );
void _timer_stm32l1xx_update_event( t_register_stm32l1xx_timer* Register, t_u32 NewState );
void _timer_stm32l1xx_generate_update_event( t_register_stm32l1xx_timer* Register );
void _timer_stm32l1xx_one_pulse_mode( t_register_stm32l1xx_timer* Register, t_u16 OPMode );
e_ttc_timer_errorcode _timer_stm32l1xx_clock_selection( t_register_stm32l1xx_timer* Register, e_timer_stm32l1xx_clock_source Source );
void _timer_stm32l1xx_output_compare_channel1_init( t_ttc_timer_config* Config );
void _timer_stm32l1xx_output_compare_channel1_preload_config( t_register_stm32l1xx_timer* Register, t_u16 oc_Preload );
void _timer_stm32l1xx_output_compare_channel2_preload_config( t_register_stm32l1xx_timer* Register, t_u16 oc_Preload );
void _timer_stm32l1xx_output_compare_channel3_preload_config( t_register_stm32l1xx_timer* Register, t_u16 oc_Preload );
void _timer_stm32l1xx_output_compare_channel4_preload_config( t_register_stm32l1xx_timer* Register, t_u16 oc_Preload );
void _timer_stm32l1xx_arr_preload_config( t_register_stm32l1xx_timer* Register, FunctionalState NewState );


void _timer_stm32l1xx_output_compare_channel1_init( t_ttc_timer_config* Config );
void _timer_stm32l1xx_output_compare_channel2_init( t_ttc_timer_config* Config );
void _timer_stm32l1xx_output_compare_channel3_init( t_ttc_timer_config* Config );
void _timer_stm32l1xx_output_compare_channel4_init( t_ttc_timer_config* Config );


void _timer_stm32l1xx_insert_( t_ttc_timer_config* Config );
void _timer_stm32l1xx_();
void _timer_stm32l1xx_manage_( t_physical_index PhysicalIndex, void* Argument );

//}PrivateFunctions

#endif //TIMER_STM32L1XX_H