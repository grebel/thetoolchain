/** { timer_stm32l1.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  Low-Level driver for timer devices on stm32l1 architectures.
 *  Implementation of low-level driver. 
 *    
 *  Note: See ttc_timer.h for description of stm32l1 independent TIMER implementation.
 * 
 *  Authors: <AUTHOR>
}*/

#include "timer_stm32l1.h"

//{ Function definitions ***************************************************


ttc_timer_errorcode_e timer_stm32l1_init(ttc_timer_config_t* Config) {
    Assert_TIMER(Config, ec_NULL);
    Assert_TIMER(Config->LogicalIndex > 0, ec_InvalidArgument);
    timer_stm32l1_config_t* Config_stm32l1=Config ->Timer_Arch;
    //timer_stm32l1_config_t* Config_stm32l1 = timer_stm32l1_get_configuration(Config);
    (void) Config_stm32l1; // avoid warning: "unused variable"

    if (1) { // validate TIMER_Features
        ttc_timer_config_t TIMER_Features;
        TIMER_Features.LogicalIndex = Config->LogicalIndex;
        ttc_timer_errorcode_e Error = timer_stm32l1_get_features(&TIMER_Features);
        if (Error) return Error;
        Config->Flags.All &= TIMER_Features.Flags.All; // mask out unavailable flags
    }
    switch (Config->LogicalIndex) {                // find TIMER corresponding to TIMER_index as defined by makefile.100_board_*
#ifdef TTC_TIMER1
      case 1: Config_stm32l1->Base = (TIMER_t*) TTC_TIMER1; break;
#endif
#ifdef TTC_TIMER2
      case 2: Config_stm32l1->Base = (TIMER_t*) TTC_TIMER2; break;
#endif
#ifdef TTC_TIMER3
      case 3: Config_stm32l1->Base = (TIMER_t*) TTC_TIMER3; break;
#endif
      default: Assert_TIMER(0, ec_UNKNOWN); break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
    }

    return tte_NotImplemented;
}

ttc_timer_errorcode_e timer_stm32l1_load_defaults(ttc_timer_config_t* Config) {
    Assert_TIMER(Config != NULL, ec_NULL);
    Assert_TIMER(Config->LogicalIndex > 0, ec_InvalidArgument);

    Config->Flags.All                        = 0;
    // TIMER_Cfg->Flags.Bits.Master                = 1;

    return tte_NotImplemented;
}

//done
bool stm32l1_timer_check_initialized(ttc_timer_config_t* Config) {
    Assert_TIMER(Config, tte_InvalidArgument);
    switch(Config->LogicalIndex){
    case 1: if((TIM1->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
    case 2: if((TIM2->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
    case 3: if((TIM3->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#ifdef TTC_TIMER4
    case 4: if((TIM4->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER5
    case 5: if((TIM5->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER6
    case 6: if((TIM6->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER7
    case 7: if((TIM7->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER8
    case 8: if((TIM8->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER9
    case 9: if((TIM9->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER10
    case 10: if((TIM10->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER11
    case 11: if((TIM11->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER12
    case 12: if((TIM12->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER13
    case 13: if((TIM13->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER14
    case 14: if((TIM14->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER15
    case 15: if((TIM15->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER16
    case 16: if((TIM16->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER17
    case 17: if((TIM17->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
    default: Assert_TIMER(0, ec_UNKNOWN);
    }

    return FALSE;
}


ttc_timer_errorcode_e timer_stm32l1_deinit(ttc_timer_config_t* Config) {
    Assert_TIMER(Config, ec_NULL);
    Assert_TIMER(Config->LogicalIndex > 0, ec_InvalidArgument);
    timer_stm32l1_config_t* Config_stm32l1 = timer_stm32l1_get_configuration(Config);
    (void) Config_stm32l1; // avoid warning: "unused variable"

    // ToDo: Deinitialize hardware device

    return tte_NotImplemented;
}


//done
ttc_timer_errorcode_e timer_stm32l1_set(ttc_timer_config_t* Config){
    //check that there is a device number set (i.e. 2 in TIM2)
    Assert_TIMER(Config->LogicalIndex > 0, tte_InvalidArgument);
    //check validity of time period
    Assert_TIMER(Config->TimerEvent.TimePeriod > 0, tte_InvalidArgument);
    //check that device is defined (i.e. no TIM100)
    if (Config->LogicalIndex > TTC_INTERRUPT_TIMER_AMOUNT)
        return tte_DeviceNotFound;

    //define standard peripheral type pointer
    TIM_TimeBaseInitTypeDef TimerStd;

    //call timer_stm32l1_deinit() to clear any existing settings
    //call timer_stm32l1_load_defaults() to start with standard settings

    if(Config->TimeScale == 0){
        //TimerStd.TIM_ClockDivision = TIM_CKD_DIV1;
        TIM_SetClockDivision(stm32l1_timer_logical_2_physical_address(Config->LogicalIndex), TIM_CKD_DIV1);
        TimerStd.TIM_Prescaler = 71;
        TimerStd.TIM_Period = Config->TimerEvent.TimePeriod;
    }
    else{
        //TimerStd.TIM_ClockDivision = TIM_CKD_DIV1;
        TIM_SetClockDivision(stm32l1_timer_logical_2_physical_address(Config->LogicalIndex), TIM_CKD_DIV2);
        TimerStd.TIM_Prescaler = 35999;
        if(Config->TimerEvent.TimePeriod <= 32767)
            TimerStd.TIM_Period = Config->TimerEvent.TimePeriod*2;
        else return tte_InvalidArgument;
    }
    TimerStd.TIM_CounterMode = TIM_CounterMode_Up;
    TimerStd.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(stm32l1_timer_logical_2_physical_address(Config->LogicalIndex), &TimerStd);
    TIM_ARRPreloadConfig(stm32l1_timer_logical_2_physical_address(Config->LogicalIndex), ENABLE);

    Config->Flags.Bits.Initialized = 1;
    return tte_OK;
}


//done
ttc_timer_errorcode_e  timer_stm32l1_reset(ttc_timer_config_t* Config) {
    Assert_TIMER(Config, tte_NotImplemented);
    timer_stm32l1_stop(Config->LogicalIndex);
    Config->Counter=0;
    return timer_stm32l1_set(Config);
}
//done
void timer_stm32l1_prepare() {
  // add your startup code here (Singletasking!)

}


//done
ttc_timer_errorcode_e timer_stm32l1_start(u8_t LogicalIndex){
    TIM_Cmd(stm32l1_timer_logical_2_physical_address(LogicalIndex), ENABLE);
    return tte_OK;
}
//done
ttc_timer_errorcode_e timer_stm32l1_stop(u8_t LogicalIndex){
    TIM_Cmd(stm32l1_timer_logical_2_physical_address(LogicalIndex), DISABLE);
    return tte_OK;
}



void timer_stm32l1_set_counter(u8_t LogicalIndex, u16_t Value, u8_t TimeScale){
    //EMPTY
}

//done
u16_t timer_stm32l1_get_counter(u8_t LogicalIndex){
    return (u16_t)TIM_GetCounter(stm32l1_timer_logical_2_physical_address(LogicalIndex));
}

ttc_timer_statuscode_e timer_stm32l1_get_state(u8_t LogicalIndex){
    return tts_Unknown;
}









//} Function definitions
//{ private functions (ideally) *********************************************
TIM_TypeDef* stm32l1_timer_logical_2_physical_address(u8_t LogicalIndex) {
  Assert_TIMER(LogicalIndex > 0, tte_InvalidArgument);
  Assert_TIMER(LogicalIndex < TTC_INTERRUPT_TIMER_AMOUNT, tte_InvalidArgument);

  switch (LogicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_TIMER1
           case TTC_TIMER1: return (TIM_TypeDef*)TIM1;
#endif
#ifdef TTC_TIMER2
           case TTC_TIMER2: return (TIM_TypeDef*)TIM2;
#endif
#ifdef TTC_TIMER3
           case TTC_TIMER3: return (TIM_TypeDef*)TIM3;
#endif
#ifdef TTC_TIMER4
           case TTC_TIMER4: return (TIM_TypeDef*)TIM4;
#endif
#ifdef TTC_TIMER5
           case TTC_TIMER5: return (TIM_TypeDef*)TIM5;
#endif
#ifdef TTC_TIMER6
           case TTC_TIMER6: return (TIM_TypeDef*)TIM6;
#endif
#ifdef TTC_TIMER7
           case TTC_TIMER7: return (TIM_TypeDef*)TIM7;
#endif
#ifdef TTC_TIMER8
           case TTC_TIMER8: return (TIM_TypeDef*)TIM8;
#endif
#ifdef TTC_TIMER9
           case TTC_TIMER9: return (TIM_TypeDef*)TIM9;
#endif
#ifdef TTC_TIMER10
           case TTC_TIMER10: return (TIM_TypeDef*)TIM10;
#endif
#ifdef TTC_TIMER11
           case TTC_TIMER11: return (TIM_TypeDef*)TIM11;
#endif
#ifdef TTC_TIMER12
           case TTC_TIMER12: return (TIM_TypeDef*)TIM12;
#endif
#ifdef TTC_TIMER13
           case TTC_TIMER13: return (TIM_TypeDef*)TIM13;
#endif
#ifdef TTC_TIMER14
           case TTC_TIMER14: return (TIM_TypeDef*)TIM14;
#endif
#ifdef TTC_TIMER15
           case TTC_TIMER15: return (TIM_TypeDef*)TIM15;
#endif
#ifdef TTC_TIMER16
           case TTC_TIMER16: return (TIM_TypeDef*)TIM16;
#endif
#ifdef TTC_TIMER17
           case TTC_TIMER17: return (TIM_TypeDef*)TIM17;
#endif
      default: Assert_TIMER(0, tte_NotImplemented); break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
    }

  return (TIM_TypeDef*)-1;
}
/*timer_stm32l1_config_t* timer_stm32l1_get_configuration(ttc_timer_config_t* Config){

}*/


//} private functions
