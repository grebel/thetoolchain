#ifndef TIMER_STM32F1XX_H
#define TIMER_STM32F1XX_H

/** { timer_stm32f1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for timer devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by high-level timer and application.
 *
 *  Created from template device_architecture.h revision 20 at 20140204 09:25:54 UTC
 *
 *  Note: See ttc_timer.h for description of stm32f1xx independent TIMER implementation.
 *
 *  Authors: Francisco Estevez 2014
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_TIMER_STM32F1XX
//
// Implementation status of low-level driver support for timer devices on stm32f1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_TIMER_STM32F1XX 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_TIMER_STM32F1XX == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_TIMER_STM32F1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_TIMER_STM32F1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "../ttc_timer_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_timer_interface.h
#define ttc_driver_timer_prepare()                     timer_stm32f1xx_prepare()
#define ttc_driver_timer_reset(Config)                 timer_stm32f1xx_reset(Config)
#define ttc_driver_timer_load_defaults(Config)         timer_stm32f1xx_load_defaults(Config)
#define ttc_driver_timer_deinit(Config)                timer_stm32f1xx_deinit(Config)
#define ttc_driver_timer_init(Config)                  timer_stm32f1xx_init(Config)
#define ttc_driver_timer_get_features(Config)          timer_stm32f1xx_get_features(Config)
#define ttc_driver_timer_change_period(Config, Period) timer_stm32f1xx_change_period(Config, Period)
#define ttc_driver_timer_read_value(Config)            timer_stm32f1xx_read_value(Config)
#define ttc_driver_timer_set(Config)                   timer_stm32f1xx_set(Config)
#define ttc_driver_timer_reload(Config)                timer_stm32f1xx_reload(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_timer.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_timer.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** shutdown single TIMER unit device
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been shutdown successfully; != 0: error-code
 */
e_ttc_timer_errorcode timer_stm32f1xx_deinit( t_ttc_timer_config* Config );


/** fills out given Config with maximum valid values for indexed TIMER
 * @param Config        = pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_timer_errorcode timer_stm32f1xx_get_features( t_ttc_timer_config* Config );


/** initializes single TIMER unit for operation
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
e_ttc_timer_errorcode timer_stm32f1xx_init( t_ttc_timer_config* Config );


/** loads configuration of indexed TIMER unit with default values
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_timer_errorcode timer_stm32f1xx_load_defaults( t_ttc_timer_config* Config );


/** Prepares timer driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void timer_stm32f1xx_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 * @param Config   =
 */
void timer_stm32f1xx_reset( t_ttc_timer_config* Config );


/** set a new reload value for given timer
 *
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @param Period        reload value (us)
 */
void timer_stm32f1xx_change_period( t_ttc_timer_config* Config, t_u32 Period );


/** read current timer value
 *
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return   current elapsed time in given timer (us)
 */
t_u32 timer_stm32f1xx_read_value( t_ttc_timer_config* Config );


/** set a timer with a given value
 *
 * @param Config          pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 */
e_ttc_timer_errorcode timer_stm32f1xx_set( t_ttc_timer_config* Config );

/** checks if indexed TIMER bus already has been initialized
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return              == TRUE: indexed TIMER unit is already initialized
 */
bool timer_stm32f1xx_check_initialized( t_ttc_timer_config* Config );


/** maps from logical to physical device index
 *
 * High-level timers (ttc_timer_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_TIMERn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any
 * order.
 *
 * @param Index         logical index of timer device (1..ttc_timer_get_max_index() )
 * @return              physical address of timer device (0 = first physical timer device, ...)
 */
t_register_stm32f1xx_timer* stm32f1_timer_physical_index_2_address( t_u8 Index );



/** Reload value for given timer
 *
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 *
 */
void timer_stm32f1xx_reload( t_ttc_timer_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _timer_stm32f1xx_foo(t_ttc_timer_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

/** Note: This function controls the timer list
*/
void _timer_stm32f1xx_();


/** Note: This private function manages the hardware timer interruption
 *
 * @param PhysicalIndex    0..TTC_TIMER_AMOUNT-1 - TIMER device to use
 * @param Argument        Pointer to the Configuration structure
*/
void _timer_stm32f1xx_manage_( t_base PhysicalIndex, void* Argument );


/** Note: This function inserts an event in the right position on pending timer list
 *
 * @param Config          pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
*/
void _timer_stm32f1xx_insert_( t_ttc_timer_config* Config );
//}PrivateFunctions

#endif //TIMER_STM32F1XX_H