#ifndef ARCHITECTURE_TIMER_H
#define ARCHITECTURE_TIMER_H

/** { timer_stm32l1.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  Low-Level driver for timer devices on stm32l1 architectures.
 *  Structures, Enums and Defines being required by high-level timer and application.
 *
 *  Note: See ttc_timer.h for description of stm32l1 independent TIMER implementation.
 *  
 *  Authors: Greg Knoll 2014
 *
}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

#include "timer_stm32l1_types.h"
#include "../ttc_timer_types.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_timer_interface.h
#define Driver_timer_init()                                         timer_stm32l1_init()
#define Driver_timer_load_defaults(Config)                          timer_stm32l1_load_defaults(Config)
#define Driver_timer_check_initialized(Config)                      timer_stm32l1_check_initialized(Config)
#define Driver_timer_deinit(Config)                                 timer_stm32l1_deinit(Config)

#define Driver_timer_set(Config)                                    timer_stm32l1_set(Config)
#define Driver_timer_reset(Config)                                  timer_stm32l1_reset(Config)
#define Driver_timer_prepare()                                      timer_stm32l1_prepare()

#define Driver_timer_start(LogicalIndex)                            timer_stm32l1_start(LogicalIndex
#define Driver_timer_stop(LogicalIndex)                             timer_stm32l1_stop(LogicalIndex)

#define Driver_timer_set_counter(LogicalIndex, Value, TimeScale)    timer_stm32l1_set_counter(LogicalIndex, Value, TimeScale)
#define Driver_timer_get_counter(LogicalIndex)                      timer_stm32l1_get_counter(LogicalIndex)
#define Driver_timer_get_state(LogicalIndex)                        timer_stm32l1_get_state(LogicalIndex)




//} Includes
//{ Function prototypes **************************************************

/** initializes single TIMER from 1 mSec
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
ttc_timer_errorcode_e timer_stm32l1_init();

/** loads configuration of indexed TIMER interface with default values
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
ttc_timer_errorcode_e timer_stm32l1_load_defaults(ttc_timer_config_t* Config);

/** checks if indexed TIMER bus already has been initialized
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == TRUE: indexed TIMER unit is already initialized
 */
bool timer_stm32l1_check_initialized(ttc_timer_config_t* Config);

/** shutdown single TIMER device
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been shutdown successfully; != 0: error-code
 */
ttc_timer_errorcode_e timer_stm32l1_deinit(ttc_timer_config_t* Config);



/** initializes single TIMER up to 1 mSec
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
ttc_timer_errorcode_e timer_stm32l1_set(ttc_timer_config_t* Config);

/** reset configuration of indexed TIMER device into default state
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
ttc_timer_errorcode_e stm32f1_timer_reset(ttc_timer_config_t* Config);

void timer_stm32l1_prepare(); //Just to avoid warnings



/** starts a single TIMER
 *
 * @param LogicalIndex  Timer Index to start
 *
 */
ttc_timer_errorcode_e timer_stm32l1_start(u8_t LogicalIndex);

/** stops a single TIMER, remaining the information on the data structure
 *
 * @param LogicalIndex  Timer Index to stop
 *
 */
ttc_timer_errorcode_e timer_stm32l1_stop(u8_t LogicalIndex);



/** set a counter value on a single TIMER
 *
 * @param LogicalIndex  Timer Index to set a time period
 * @param Value         Value to load on the counter
 * @param TimeScale     0: uSeconds time scale, 1: mSeconds time scale
 *
 */
void timer_stm32l1_set_counter(u8_t LogicalIndex, u16_t Value, u8_t TimeScale);

/** gets the counter value of a single TIMER
 *
 * @param LogicalIndex  Timer Index to get the counter value
 * @return              Counter value (TIMx->CNT)
 *
 */
u16_t timer_stm32l1_get_counter(u8_t LogicalIndex);

/** gets the state of a single TIMER
 *
 * @param LogicalIndex  Timer Index to get the status value
 * @return              Status value (TIMx->SR)
 *
 */
ttc_timer_statuscode_e timer_stm32l1_get_state(u8_t LogicalIndex);





//----pricvate functions----//
/** maps from logical to physical device index
 *
 * High-level timers (ttc_timer_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_TIMERn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any
 * order.
 *
 * @param LogicalIndex  logical index of timer device (1..ttc_timer_get_max_index() )
 * @return              physical address of timer device (0 = first physical timer device, ...)
 */
TIM_TypeDef* stm32l1_timer_logical_2_physical_address(u8_t LogicalIndex);

/**
 * @brief timer_stm32l1_get_configuration
 * @param Config
 * @return stm32l1_base_config
 */
//timer_stm32l1_config_t* timer_stm32l1_get_configuration(ttc_timer_config_t* Config);

//} Function prototypes

#endif //ARCHITECTURE_TIMER_H
