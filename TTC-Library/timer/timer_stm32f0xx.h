#ifndef ARCHITECTURE_TIMER_H
#define ARCHITECTURE_TIMER_H

/** { timer_stm32f0xx.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for timer devices on stm32f0xx architectures.
 *  Structures, Enums and Defines being required by high-level timer and application.
 *
 *  Created from template device_architecture.h revision 20 at 20140203 16:39:49 UTC
 *
 *  Note: See ttc_timer.h for description of stm32f0xx independent TIMER implementation.
 *  
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_TIMER_STM32F0XX
//
// Implementation status of low-level driver support for timer devices on stm32f0xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_TIMER_STM32F0XX '?'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_TIMER_STM32F0XX == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_TIMER_STM32F0XX to one from '-', 'o', '+' 
#endif

//}EXTENSION_STATUS_TTC_TIMER_STM32F0XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "timer_stm32f0xx_types.h"
#include "../ttc_timer_types.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_timer_interface.h
#define ttc_driver_timer_prepare()             timer_stm32f0xx_prepare()
#define ttc_driver_timer_reset(Config)         timer_stm32f0xx_reset(Config)
#define ttc_driver_timer_load_defaults(Config) timer_stm32f0xx_load_defaults(Config)
#define ttc_driver_timer_deinit(Config)        timer_stm32f0xx_deinit(Config)
#define ttc_driver_timer_init(Config)          timer_stm32f0xx_init(Config)
#define ttc_driver_timer_change_period(Config, Period) timer_stm32f0xx_change_period(Config, Period)
#define ttc_driver_timer_get_features(Config) timer_stm32f0xx_get_features(Config)
#define ttc_driver_timer_read_value(Config) timer_stm32f0xx_read_value(Config)
#define ttc_driver_timer_set(Config) timer_stm32f0xx_set(Config)
#define ttc_driver_timer_reload(Config) timer_stm32f0xx_reload(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_timer.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_timer.h for details.
 *
 * You normally should not need to add functions here. 
 * Add your private functions in the private section below. 
 */


/** set a new reload value for given timer
 *
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @param Period        reload value (us)
 */
void timer_stm32f0xx_change_period(t_ttc_timer_config* Config, t_u32 Period);


/** shutdown single TIMER unit device
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been shutdown successfully; != 0: error-code
 */
e_ttc_timer_errorcode timer_stm32f0xx_deinit(t_ttc_timer_config* Config);


/** fills out given Config with maximum valid values for indexed TIMER
 * @param Config        = pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_timer_errorcode timer_stm32f0xx_get_features(t_ttc_timer_config* Config);


/** initializes single TIMER unit for operation
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
e_ttc_timer_errorcode timer_stm32f0xx_init(t_ttc_timer_config* Config);


/** loads configuration of indexed TIMER unit with default values
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_timer_errorcode timer_stm32f0xx_load_defaults(t_ttc_timer_config* Config);


/** Prepares timer driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void timer_stm32f0xx_prepare();


/** read current timer value
 *
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return   current elapsed time in given timer (us)
 */
t_u32 timer_stm32f0xx_read_value(t_ttc_timer_config* Config);


/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 * @param Config   = 
 */
void timer_stm32f0xx_reset(t_ttc_timer_config* Config);


/** set a timer with a given value
 *
 * @param Config          pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @param TaskFunction    CallBack Function that will be executed
 * @param Argument        passed as argument to Function()
 * @param Period          reload value (us)
 * @return   = 
 */
e_ttc_timer_errorcode timer_stm32f0xx_set(t_ttc_timer_config* Config);


/** Reload value for given timer
 *
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 *
 */
void timer_stm32f0xx_reload(t_ttc_timer_config* Config);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _timer_stm32f0xx_foo(t_ttc_timer_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //ARCHITECTURE_TIMER_H