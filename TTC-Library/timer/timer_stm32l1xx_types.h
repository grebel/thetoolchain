#ifndef ARCHITECTURE_TIMER_TYPES_H
#define ARCHITECTURE_TIMER_TYPES_H

/** { timer_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for TIMER devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_timer_types.h
 *
 *  Note: See ttc_timer.h for description of architecture independent TIMER implementation.
 *
 *  Authors: Adrián Romero
 *
}*/
//{ Defines/ TypeDefs ******************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic.h"
#include "../ttc_register.h"
#include "../ttc_gpio.h"


//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_timer_types.h ***********************

#define _driver_timer_isr    _timer_stm32l1xx_manage_ // define function pointer for receive interrupt service routine


typedef struct { // register description (adapt according to stm32l1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_timer_register;

typedef enum {
    timer_stm32l1xx_clock_source_none,
    timer_stm32l1xx_clock_source_internal_clock_source,
    e_timer_stm32l1xx_clock_sourcexternl_clock_source_mode_1,
    e_timer_stm32l1xx_clock_sourcexternl_clock_source_mode_2
} e_timer_stm32l1xx_clock_source;

typedef struct {  // stm32l1xx specific configuration data of single TIMER device
    t_register_stm32l1xx_timer* BaseRegister;       // base address of TIMER device registers
    t_register_stm32l1xx_tim2_to_tim4_input_capture_mode* BaseRegister_input_capture_mode;
    t_register_stm32l1xx_tim2_to_tim4_output_compare_mode* t_baseregister_output_compare_mode;

    t_u16 Prescaler;
    t_u16 CounterMode;
    t_u16 Period;
    t_u16 ClockDivision;

    /* Output Compare mode variables */

    /* Specifies the timer mode */
    t_u16 oc_Mode;

    /*Specifies the TIM Output Compare state*/
    t_u16 oc_OutputState;

    /*Specifies the pulse value to be loaded into the Capture Compare Register*/
    t_u16 oc_Pulse;

    /* Specifies the output polarity */
    t_u16 oc_Polarity;

} __attribute__( ( __packed__ ) ) t_timer_stm32l1xx_config;

// t_ttc_timer_arch is required by ttc_timer_types.h
#define t_ttc_timer_architecture t_timer_stm32l1xx_config

//} Structures/ Enums


#endif //ARCHITECTURE_TIMER_TYPES_H
