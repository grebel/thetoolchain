/** { timer_stm32w1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for timer devices on stm32w1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 20 at 20140203 16:39:51 UTC
 *
 *  Note: See ttc_timer.h for description of stm32w1xx independent TIMER implementation.
 * 
 *  Authors: Gregor Rebel
}*/

#include "timer_stm32w1xx.h"

//{ Function definitions *******************************************************

void timer_stm32w1xx_change_period(t_ttc_timer_config* Config, t_u32 Period) {
    Assert_TIMER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    

}
e_ttc_timer_errorcode timer_stm32w1xx_deinit(t_ttc_timer_config* Config) {
    Assert_TIMER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (e_ttc_timer_errorcode) 0;
}
e_ttc_timer_errorcode timer_stm32w1xx_get_features(t_ttc_timer_config* Config) {
    Assert_TIMER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (e_ttc_timer_errorcode) 0;
}
e_ttc_timer_errorcode timer_stm32w1xx_init(t_ttc_timer_config* Config) {
    Assert_TIMER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (e_ttc_timer_errorcode) 0;
}
e_ttc_timer_errorcode timer_stm32w1xx_load_defaults(t_ttc_timer_config* Config) {
    Assert_TIMER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (e_ttc_timer_errorcode) 0;
}
void timer_stm32w1xx_prepare() {


    

}
t_u32 timer_stm32w1xx_read_value(t_ttc_timer_config* Config) {
    Assert_TIMER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (t_u32) 0;
}
void timer_stm32w1xx_reset(t_ttc_timer_config* Config) {
    Assert_TIMER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    

}
e_ttc_timer_errorcode timer_stm32w1xx_set(t_ttc_timer_config* Config) {
    Assert_TIMER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (e_ttc_timer_errorcode) 0;
}
void timer_stm32w1xx_reload(t_ttc_timer_config* Config) {
    Assert_TIMER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
