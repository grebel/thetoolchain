/** { timer_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for timer devices on stm32l1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Note: See ttc_timer.h for description of stm32l1xx independent TIMER implementation.
 *
 *  Authors: Adrian Romero
}*/

#include "timer_stm32l1xx.h"

extern void _ttc_timer_isr( t_ttc_timer_config* Config );

//{ Function definitions *******************************************************
t_ttc_heap_pool* TimerPool = NULL;
t_ttc_timer_config* HeadEvent = NULL;

e_ttc_timer_errorcode timer_stm32l1xx_start( t_register_stm32l1xx_timer* Register ) {
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL

    Register->CR1.All |= ( t_u16 ) 0x0001;

    return ec_timer_OK;
}
e_ttc_timer_errorcode timer_stm32l1xx_stop( t_register_stm32l1xx_timer* Register ) {
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL

    Register->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );

    return ec_timer_OK;
}
void                  timer_stm32l1xx_set_counter( t_register_stm32l1xx_timer* Register, t_u16 Value ) {

    /* Check the parameters */
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL

    /* Set the Counter Register value */
    Register->CNT.All = Value;
}
t_u16                 timer_stm32l1xx_get_counter( t_register_stm32l1xx_timer* Register ) {

    /* Check the parameters */
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL

    /* Get the Counter Register value */
    return Register->CNT.All;
}
void                  timer_stm32l1xx_change_period( t_ttc_timer_config* Config, t_u32 Period ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TIMER( Period > 0, ttc_assert_origin_auto );
    Config->TIMER_Arch.BaseRegister = timer_stm32l1xx_physical_index_2_address( Config->PhysicalIndex );

    if ( ( Config->TimePeriod > 100 ) && ( Config->TimePeriod <= 1000 ) ) {
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );
        Config->TIMER_Arch.BaseRegister->CNT.All = ( t_u16 )( Config->TimePeriod / 100 ) * 3;
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( t_u16 ) 0x0001;
    }
    else if ( ( Config->TimePeriod > 1000 ) && ( Config->TimePeriod <= 5000 ) ) {
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );
        Config->TIMER_Arch.BaseRegister->CNT.All = ( t_u16 )( ( Config->TimePeriod / 1000 ) * 7 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( t_u16 ) 0x0001;
    }
    else if ( ( Config->TimePeriod > 5000 ) && ( Config->TimePeriod <= 30000000 ) ) {
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );
        Config->TIMER_Arch.BaseRegister->CNT.All = ( t_u16 )( ( Config->TimePeriod / 1000 ) * 2 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( t_u16 ) 0x0001;
    }
    else { Config->TimePeriod = Config->TimePeriod; }



}
e_ttc_timer_errorcode timer_stm32l1xx_deinit( t_ttc_timer_config* Config ) {


    Assert_TIMER( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TIMER( Config->LogicalIndex > 0, ttc_assert_origin_auto );
    Config->TIMER_Arch.BaseRegister = timer_stm32l1xx_physical_index_2_address( Config->PhysicalIndex );

//        if(Config->TimePeriod <= 30000000){
//            Config->TIMER_Arch.BaseRegister->CR1.All &= (t_u16)(~((t_u16)0x0001));

//            switch (Config->LogicalIndex) {
//               case 1: { // load low-level configuration of third timer device
//                 RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM2, DISABLE);
//                 break;
//               }
//               case 2: { // load low-level configuration of fourth timer device
//                 RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM3, DISABLE);
//                 break;
//               }
//               case 3: { // load low-level configuration of fifth timer device
//                 RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM4, DISABLE);
//                 break;
//               }
//               case 5: { // load low-level configuration of tenth timer device
//                 RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM6, DISABLE);
//                 break;
//               }
//               case 6: { // load low-level configuration of eleventh timer device
//                 RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM7, DISABLE);
//                 break;
//               }
//               case 8: { // load low-level configuration of twelfth timer device
//                 RCC_APB2PeriphClockCmd (RCC_APB2Periph_TIM9, DISABLE);
//                 break;
//               }

//               case 9: { // load low-level configuration of fourteenth timer device
//                 RCC_APB2PeriphClockCmd (RCC_APB2Periph_TIM10, DISABLE);
//                 break;
//               }
//               case 10: { // load low-level configuration of fifteenth timer device
//                 RCC_APB2PeriphClockCmd (RCC_APB2Periph_TIM11, DISABLE);
//                 break;
//               }
//               default: Assert_TIMER(0, ttc_assert_origin_auto);
//                   break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
//               }
//        }
//        Config->Flags.Bits.Initialized = 0;

    return ( e_ttc_timer_errorcode ) 0; //ec_timer_OK

}
e_ttc_timer_errorcode timer_stm32l1xx_get_features( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TIMER( Config->LogicalIndex > 0, ttc_assert_origin_auto );

    //Config->Architecture = ta_timer_stm32l1xx;
    //Config->Flags.All = 0;

    return ( e_ttc_timer_errorcode ) 0;
}
e_ttc_timer_errorcode timer_stm32l1xx_init( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    t_ttc_timer_architecture* Config_timer = ( Config->LowLevelConfig );

    if ( 1 ) {                                 // find indexed TIMER on current board
        switch ( Config->PhysicalIndex ) {              // find corresponding TIMER as defined by makefile.100_board_*

            case 1:

                Config_timer->BaseRegister = ( t_register_stm32l1xx_timer* ) TIM2;
                sysclock_stm32l1xx_RCC_APB1PeriphClockCmd( 0x00000001, ENABLE );
                break;

            case 2:

                Config_timer->BaseRegister = ( t_register_stm32l1xx_timer* ) TIM3;
                sysclock_stm32l1xx_RCC_APB1PeriphClockCmd( 0x00000002, ENABLE );
                break;

            case 3:

                Config_timer->BaseRegister = ( t_register_stm32l1xx_timer* ) TIM4;
                sysclock_stm32l1xx_RCC_APB1PeriphClockCmd( 0x00000004, ENABLE );
                break;

            case 5:
                Config_timer->BaseRegister = ( t_register_stm32l1xx_timer* ) TIM6;
                sysclock_stm32l1xx_RCC_APB1PeriphClockCmd( 0x00000010, ENABLE );

                break;
            case 6:

                Config_timer->BaseRegister = ( t_register_stm32l1xx_timer* ) TIM7;
                sysclock_stm32l1xx_RCC_APB1PeriphClockCmd( 0x00000020, ENABLE );

            case 8:

                Config_timer->BaseRegister = ( t_register_stm32l1xx_timer* ) TIM9;
                sysclock_stm32l1xx_RCC_APB2PeriphClockCmd( 0x00000004, ENABLE );
                break;

            case 9:
                Config_timer->BaseRegister = ( t_register_stm32l1xx_timer* ) TIM10;
                sysclock_stm32l1xx_RCC_APB2PeriphClockCmd( 0x00000008, ENABLE );

                break;
            case 10:

                Config_timer->BaseRegister = ( t_register_stm32l1xx_timer* ) TIM11;
                sysclock_stm32l1xx_RCC_APB2PeriphClockCmd( 0x00000010, ENABLE );

                break;
            default: Assert_TIMER( 0, ttc_assert_origin_auto ); break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
        }

    }

    Config_timer->BaseRegister_input_capture_mode = ( t_register_stm32l1xx_tim2_to_tim4_input_capture_mode* )Config_timer->BaseRegister;
    Config_timer->t_baseregister_output_compare_mode = ( t_register_stm32l1xx_tim2_to_tim4_output_compare_mode* ) Config_timer->BaseRegister;

    switch ( ( t_u32 ) Config_timer->BaseRegister ) { // determine pin layout

        case ( t_u32 ) TIM2: // TIMER 2

            #ifdef TTC_TIMER1_PWM1_PIN
            Assert_TIMER( ( TTC_TIMER1_PWM1_PIN == E_ttc_gpio_pin_a0 ) || ( TTC_TIMER1_PWM1_PIN == E_ttc_gpio_pin_a15 ), ttc_assert_origin_auto );
            Config->PWM1 = TTC_TIMER1_PWM1_PIN;
            #endif
            #ifdef TTC_TIMER1_PWM2_PIN
            Assert_TIMER( ( TTC_TIMER1_PWM2_PIN == E_ttc_gpio_pin_b3 ) || ( TTC_TIMER1_PWM2_PIN == E_ttc_gpio_pin_a1 ), ttc_assert_origin_auto );
            Config->PWM2 = TTC_TIMER1_PWM2_PIN;
            #endif

            #ifdef TTC_TIMER1_PWM3_PIN
            Assert_TIMER( ( TTC_TIMER1_PWM3_PIN == E_ttc_gpio_pin_b10 ) || ( TTC_TIMER1_PWM3_PIN == E_ttc_gpio_pin_a2 ), ttc_assert_origin_auto );
            Config->PWM3 = TTC_TIMER1_PWM3_PIN;
            #endif

            #ifdef TTC_TIMER1_PWM4_PIN
            Assert_TIMER( ( TTC_TIMER1_PWM4_PIN == E_ttc_gpio_pin_b11 ) || ( TTC_TIMER1_PWM4_PIN == E_ttc_gpio_pin_a3 ), ttc_assert_origin_auto );
            Config->PWM4 = TTC_TIMER1_PWM4_PIN;
            #endif

            #ifdef DTTC_TIMER1_CAPTURE1_PIN
            Assert_TIMER( ( DTTC_TIMER1_CAPTURE1_PIN == E_ttc_gpio_pin_a0 ) || ( DTTC_TIMER1_CAPTURE1_PIN == E_ttc_gpio_pin_a15 ), ttc_assert_origin_auto );
            Config->CH1 = DTTC_TIMER1_CAPTURE1_PIN;
            #endif

            #ifdef DTTC_TIMER1_CAPTURE2_PIN
            Assert_TIMER( ( DTTC_TIMER1_CAPTURE2_PIN == E_ttc_gpio_pin_b3 ) || ( DTTC_TIMER1_CAPTURE2_PIN == E_ttc_gpio_pin_a1 ), ttc_assert_origin_auto );
            Config->CH2 = DTTC_TIMER1_CAPTURE2_PIN;
            #endif

            #ifdef DTTC_TIMER1_CAPTURE3_PIN
            Assert_TIMER( ( DTTC_TIMER1_CAPTURE3_PIN == E_ttc_gpio_pin_b10 ) || ( DTTC_TIMER1_CAPTURE3_PIN == E_ttc_gpio_pin_a2 ), ttc_assert_origin_auto );
            Config->CH3 = DTTC_TIMER1_CAPTURE3_PIN;
            #endif

            #ifdef DTTC_TIMER1_CAPTURE4_PIN
            Assert_TIMER( ( DTTC_TIMER1_CAPTURE4_PIN == E_ttc_gpio_pin_b11 ) || ( DTTC_TIMER1_CAPTURE4_PIN == E_ttc_gpio_pin_a3 ), ttc_assert_origin_auto );
            Config->CH4 = DTTC_TIMER1_CAPTURE4_PIN;
            #endif
//            Config->CH1 = E_ttc_gpio_pin_a0;
//            Config->CH2 = E_ttc_gpio_pin_a1;
//            Config->CH3 = E_ttc_gpio_pin_a2;
//            Config->CH4 = E_ttc_gpio_pin_a3;
//            break;

        case ( t_u32 ) TIM3: // TIMER 3


            #ifdef TTC_TIMER2_PWM1_PIN
            Assert_TIMER( ( TTC_TIMER2_PWM1_PIN == E_ttc_gpio_pin_a6 ) || ( TTC_TIMER2_PWM1_PIN == E_ttc_gpio_pin_c6 ), ttc_assert_origin_auto );
            Config->PWM1 = TTC_TIMER2_PWM1_PIN;
            #endif
            #ifdef TTC_TIMER2_PWM2_PIN
            Assert_TIMER( ( TTC_TIMER2_PWM2_PIN == E_ttc_gpio_pin_a7 ) || ( TTC_TIMER2_PWM2_PIN == E_ttc_gpio_pin_c7 ), ttc_assert_origin_auto );
            Config->PWM2 = TTC_TIMER2_PWM2_PIN;
            #endif

            #ifdef TTC_TIMER2_PWM3_PIN
            Assert_TIMER( ( TTC_TIMER2_PWM3_PIN == E_ttc_gpio_pin_b0 ) || ( TTC_TIMER2_PWM3_PIN == E_ttc_gpio_pin_c8 ), ttc_assert_origin_auto );
            Config->PWM3 = TTC_TIMER2_PWM3_PIN;
            #endif

            #ifdef TTC_TIMER2_PWM4_PIN
            Assert_TIMER( ( TTC_TIMER2_PWM4_PIN == E_ttc_gpio_pin_b1 ) || ( TTC_TIMER2_PWM4_PIN == E_ttc_gpio_pin_c9 ), ttc_assert_origin_auto );
            Config->PWM4 = TTC_TIMER2_PWM4_PIN;
            #endif

            #ifdef DTTC_TIMER2_CAPTURE1_PIN
            Assert_TIMER( ( DTTC_TIMER2_CAPTURE1_PIN == E_ttc_gpio_pin_a6 ) || ( DTTC_TIMER2_CAPTURE1_PIN == E_ttc_gpio_pin_c6 ), ttc_assert_origin_auto );
            Config->CH1 = DTTC_TIMER2_CAPTURE1_PIN;
            #endif

            #ifdef DTTC_TIMER2_CAPTURE2_PIN
            Assert_TIMER( ( DTTC_TIMER2_CAPTURE2_PIN == E_ttc_gpio_pin_a7 ) || ( DTTC_TIMER2_CAPTURE2_PIN == E_ttc_gpio_pin_c7 ), ttc_assert_origin_auto );
            Config->CH2 = DTTC_TIMER2_CAPTURE2_PIN;
            #endif

            #ifdef DTTC_TIMER2_CAPTURE3_PIN
            Assert_TIMER( ( DTTC_TIMER2_CAPTURE3_PIN == E_ttc_gpio_pin_b0 ) || ( DTTC_TIMER2_CAPTURE3_PIN == E_ttc_gpio_pin_c8 ), ttc_assert_origin_auto );
            Config->CH3 = DTTC_TIMER2_CAPTURE3_PIN;
            #endif

            #ifdef DTTC_TIMER2_CAPTURE4_PIN
            Assert_TIMER( ( DTTC_TIMER2_CAPTURE4_PIN == E_ttc_gpio_pin_b1 ) || ( DTTC_TIMER2_CAPTURE4_PIN == E_ttc_gpio_pin_c9 ), ttc_assert_origin_auto );
            Config->CH4 = DTTC_TIMER2_CAPTURE4_PIN;
            #endif

//            Config->CH1 = E_ttc_gpio_pin_a6;
//            Config->CH2 = E_ttc_gpio_pin_a7;
//            Config->CH3 = E_ttc_gpio_pin_b0;
//            Config->CH4 = E_ttc_gpio_pin_b1;
            break;

        case ( t_u32 ) TIM4: // TIMER 4


            #ifdef TTC_TIMER3_PWM1_PIN
            Assert_TIMER( ( TTC_TIMER3_PWM1_PIN == E_ttc_gpio_pin_a6 ) || ( TTC_TIMER3_PWM1_PIN == E_ttc_gpio_pin_c6 ), ttc_assert_origin_auto );
            Config->PWM1 = TTC_TIMER3_PWM1_PIN;
            #endif
            #ifdef TTC_TIMER3_PWM2_PIN
            Assert_TIMER( ( TTC_TIMER3_PWM2_PIN == E_ttc_gpio_pin_a7 ) || ( TTC_TIMER3_PWM2_PIN == E_ttc_gpio_pin_c7 ), ttc_assert_origin_auto );
            Config->PWM2 = TTC_TIMER3_PWM2_PIN;
            #endif

            #ifdef TTC_TIMER3_PWM3_PIN
            Assert_TIMER( ( TTC_TIMER3_PWM3_PIN == E_ttc_gpio_pin_b0 ) || ( TTC_TIMER3_PWM3_PIN == E_ttc_gpio_pin_c8 ), ttc_assert_origin_auto );
            Config->PWM3 = TTC_TIMER3_PWM3_PIN;
            #endif

            #ifdef TTC_TIMER3_PWM4_PIN
            Assert_TIMER( ( TTC_TIMER3_PWM4_PIN == E_ttc_gpio_pin_b1 ) || ( TTC_TIMER3_PWM4_PIN == E_ttc_gpio_pin_c9 ), ttc_assert_origin_auto );
            Config->PWM4 = TTC_TIMER3_PWM4_PIN;
            #endif

            #ifdef DTTC_TIMER3_CAPTURE1_PIN
            Assert_TIMER( ( DTTC_TIMER3_CAPTURE1_PIN == E_ttc_gpio_pin_a6 ) || ( DTTC_TIMER3_CAPTURE1_PIN == E_ttc_gpio_pin_c6 ), ttc_assert_origin_auto );
            Config->CH1 = DTTC_TIMER3_CAPTURE1_PIN;
            #endif

            #ifdef DTTC_TIME3_CAPTURE2_PIN
            Assert_TIMER( ( DTTC_TIME3_CAPTURE2_PIN == E_ttc_gpio_pin_a7 ) || ( DTTC_TIME3_CAPTURE2_PIN == E_ttc_gpio_pin_c7 ), ttc_assert_origin_auto );
            Config->CH2 = DTTC_TIME3_CAPTURE2_PIN;
            #endif

            #ifdef DTTC_TIMER3_CAPTURE3_PIN
            Assert_TIMER( ( DTTC_TIMER3_CAPTURE3_PIN == E_ttc_gpio_pin_b0 ) || ( DTTC_TIMER3_CAPTURE3_PIN == E_ttc_gpio_pin_c8 ), ttc_assert_origin_auto );
            Config->CH3 = DTTC_TIMER3_CAPTURE3_PIN;
            #endif

            #ifdef DTTC_TIMER3_CAPTURE4_PIN
            Assert_TIMER( ( DTTC_TIMER3_CAPTURE4_PIN == E_ttc_gpio_pin_b1 ) || ( DTTC_TIMER3_CAPTURE4_PIN == E_ttc_gpio_pin_c9 ), ttc_assert_origin_auto );
            Config->CH4 = DTTC_TIMER3_CAPTURE4_PIN;
            #endif

//            Config->CH1 = E_ttc_gpio_pin_b6;
//            Config->CH2 = E_ttc_gpio_pin_b7;
//            Config->CH3 = E_ttc_gpio_pin_b8;
//            Config->CH4 = E_ttc_gpio_pin_b9;
            break;

        case ( t_u32 ) TIM6: // TIMER 6

//            Config_timer->CH1 = E_ttc_gpio_pin_a0;
//            Config_timer->CH2 = E_ttc_gpio_pin_a1;
//            Config_timer->CH3 = E_ttc_gpio_pin_a2;
//            Config_timer->CH4 = E_ttc_gpio_pin_a3;
            break;

        case ( t_u32 ) TIM7: // TIMER 7


//            Config_timer->CH1 = E_ttc_gpio_pin_a6;
//            Config_timer->CH2 = E_ttc_gpio_pin_a7;
//            Config_timer->CH3 = E_ttc_gpio_pin_b0;
//            Config_timer->CH4 = E_ttc_gpio_pin_b1;
            break;

        case ( t_u32 ) TIM9: // TIMER 9

            #ifdef TTC_TIMER6_PWM1_PIN
            Assert_TIMER( ( TTC_TIMER6_PWM1_PIN == E_ttc_gpio_pin_b13 ) || ( TTC_TIMER6_PWM1_PIN == E_ttc_gpio_pin_a2 ), ttc_assert_origin_auto );
            Config->PWM1 = TTC_TIMER6_PWM1_PIN;
            #endif

            #ifdef TTC_TIMER6_PWM2_PIN
            Assert_TIMER( ( TTC_TIMER6_PWM2_PIN == E_ttc_gpio_pin_b14 ) || ( TTC_TIMER6_PWM2_PIN == E_ttc_gpio_pin_a3 ), ttc_assert_origin_auto );
            Config->PWM2 = TTC_TIMER6_PWM2_PIN;
            #endif

            #ifdef DTTC_TIMER6_CAPTURE1_PIN
            Assert_TIMER( ( DTTC_TIMER8_CAPTURE1_PIN == E_ttc_gpio_pin_b13 ) || ( DTTC_TIMER8_CAPTURE1_PIN == E_ttc_gpio_pin_a2 ), ttc_assert_origin_auto );
            Config->CH1 = DTTC_TIMER8_CAPTURE1_PIN;
            #endif

            #ifdef DTTC_TIMER6_CAPTURE2_PIN
            Assert_TIMER( ( DTTC_TIMER6_CAPTURE2_PIN == E_ttc_gpio_pin_b14 ) || ( DTTC_TIMER6_CAPTURE2_PIN == E_ttc_gpio_pin_a3 ), ttc_assert_origin_auto );
            Config->CH2 = DTTC_TIMER6_CAPTURE2_PIN;
            #endif

//            Config->CH1 = E_ttc_gpio_pin_b13;
//            Config->CH2 = E_ttc_gpio_pin_b14;
            break;


        case ( t_u32 ) TIM10: // TIMER 10

            #ifdef TTC_TIMER7_PWM1_PIN
            Assert_TIMER( ( TTC_TIMER7_PWM1_PIN == E_ttc_gpio_pin_b12 ) || ( TTC_TIMER7_PWM1_PIN == E_ttc_gpio_pin_b8 ), ttc_assert_origin_auto );
            Config->PWM1 = TTC_TIMER7_PWM1_PIN;
            #endif

            #ifdef DTTC_TIMER7_CAPTURE1_PIN
            Assert_TIMER( ( DTTC_TIMER7_CAPTURE1_PIN == E_ttc_gpio_pin_b12 ) || ( DTTC_TIMER7_CAPTURE1_PIN == E_ttc_gpio_pin_b8 ), ttc_assert_origin_auto );
            Config->CH1 = DTTC_TIMER7_CAPTURE1_PIN;
            #endif

//            Config->CH1 = E_ttc_gpio_pin_b12;
            break;

        case ( t_u32 ) TIM11: // TIMER 11


            #ifdef TTC_TIMER8_PWM1_PIN
            Assert_TIMER( ( TTC_TIMER8_PWM1_PIN == E_ttc_gpio_pin_b15 ) || ( TTC_TIMER8_PWM1_PIN == E_ttc_gpio_pin_b9 ), ttc_assert_origin_auto );
            Config->PWM1 = TTC_TIMER8_PWM1_PIN;
            #endif

            #ifdef DTTC_TIMER8_CAPTURE1_PIN
            Assert_TIMER( ( DTTC_TIMER8_CAPTURE1_PIN == E_ttc_gpio_pin_b15 ) || ( DTTC_TIMER8_CAPTURE1_PIN == E_ttc_gpio_pin_b9 ), ttc_assert_origin_auto );
            Config->CH1 = DTTC_TIMER8_CAPTURE1_PIN;
            #endif
//            Config->CH1 = E_ttc_gpio_pin_b15;
            break;

        default: {

            Assert_TIMER( 0, ttc_assert_origin_auto );

        }
    }

    t_u32 tmpcr1;

    tmpcr1 = ( Config_timer->BaseRegister )->CR1.All;

    if ( ( ( Config->LowLevelConfig->BaseRegister ) == ( t_register_stm32l1xx_timer* )TIM2 ) || ( ( Config->LowLevelConfig->BaseRegister ) == ( t_register_stm32l1xx_timer* )TIM3 ) || ( ( Config->LowLevelConfig->BaseRegister ) == ( t_register_stm32l1xx_timer* )TIM4 ) ) {

        tmpcr1 &= ( t_u16 )( ~( ( t_u16 )( 0x0010 | 0x0060 ) ) );
        tmpcr1 |= ( t_u16 )Config->LowLevelConfig->CounterMode;

    }

    if ( ( ( Config->LowLevelConfig->BaseRegister ) != ( t_register_stm32l1xx_timer* )TIM6 ) && ( ( Config->LowLevelConfig->BaseRegister ) != ( t_register_stm32l1xx_timer* )TIM7 ) ) {

        /* Set the clock division */
        tmpcr1 &= ( t_u16 )( ~( ( t_u16 )0x0300 ) );
        tmpcr1 |= ( t_u32 )Config->LowLevelConfig->ClockDivision;

    }

    /* Set Timer Counter */
    //(Config_timer->BaseRegister)->CNT = (t_u32)tmpcr1;

    /* Set Timer System Control Register */
    ( Config_timer->BaseRegister )->CR1.All = ( t_u32 )tmpcr1;

    /* Set the Autoreload value */
    ( Config_timer->BaseRegister )->ARR.All = ( t_u32 )Config->LowLevelConfig->Period ;

    /* Set the Prescaler value */
    ( Config_timer->BaseRegister )->PSC.All = ( t_u32 )Config->LowLevelConfig->Prescaler;

    /* Generate an update event to reload the Prescaler value immediatly */
    _timer_stm32l1xx_generate_update_event( Config_timer->BaseRegister );


    if ( Config->Flags.Bits.OutputPWM ) {

        if ( Config->Flags.Bits.PWM_Channel == 0b00 ) {


            _timer_stm32l1xx_output_compare_channel1_init( Config );

            _timer_stm32l1xx_output_compare_channel1_preload_config( Config->LowLevelConfig->BaseRegister, ( t_u16 )0x0008 );


        }
        else if ( Config->Flags.Bits.PWM_Channel == 0b01 ) {


            _timer_stm32l1xx_output_compare_channel2_init( Config );

            _timer_stm32l1xx_output_compare_channel2_preload_config( Config->LowLevelConfig->BaseRegister, ( t_u16 )0x0008 );


        }
        else if ( Config->Flags.Bits.PWM_Channel == 0b10 ) {


            _timer_stm32l1xx_output_compare_channel3_init( Config );

            _timer_stm32l1xx_output_compare_channel3_preload_config( Config->LowLevelConfig->BaseRegister, ( t_u16 )0x0008 );

        }
        else {


            _timer_stm32l1xx_output_compare_channel4_init( Config );

            _timer_stm32l1xx_output_compare_channel4_preload_config( Config->LowLevelConfig->BaseRegister, ( t_u16 )0x0008 );

        }

        _timer_stm32l1xx_arr_preload_config( Config->LowLevelConfig->BaseRegister, ENABLE );


    }

    timer_stm32l1xx_start( Config_timer->BaseRegister );



    return ec_timer_OK;
}
e_ttc_timer_errorcode timer_stm32l1xx_load_defaults( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    t_ttc_timer_architecture* ConfigArch = Config->LowLevelConfig;

    if ( ConfigArch == NULL ) {
        ConfigArch = ( t_ttc_timer_architecture* )ttc_heap_alloc_zeroed( sizeof( t_ttc_timer_architecture ) );
        Config->LowLevelConfig = ConfigArch;
    }

    /* Set the default configuration */
    Config->LowLevelConfig->Period = 0xFFFF;
    Config->LowLevelConfig->Prescaler = 0x0000;
    Config->LowLevelConfig->ClockDivision = 0x0000;
    Config->LowLevelConfig->CounterMode = 0x0000;

    Config->LowLevelConfig->oc_Mode = 0x0000;
    Config->LowLevelConfig->oc_OutputState = 0x0000;
    Config->LowLevelConfig->oc_Pulse = 0x0000;
    Config->LowLevelConfig->oc_Polarity = 0x0000;

    return ec_timer_OK;
}
void                  timer_stm32l1xx_prepare() {

    TimerPool = ttc_heap_pool_create( sizeof( t_ttc_timer_config ), 3 ); //Create the memory pool for timer events
    if ( TimerPool != NULL )
    { ttc_task_create( _timer_stm32l1xx_, "checkNextEvent", 64, NULL, 1, NULL ); }


}
t_u32                 timer_stm32l1xx_read_value( t_ttc_timer_config* Config ) {

    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TIMER( Config->TimePeriod < 0, ttc_assert_origin_auto );
    Config->TIMER_Arch.BaseRegister = timer_stm32l1xx_physical_index_2_address( Config->PhysicalIndex );

    if ( Config->TimePeriod <= 5000 )
    { return ( t_u32 )Config->TIMER_Arch.BaseRegister->CNT.All; }
    //        return (t_u32)TIM_GetCounter(stm32f1_timer_physical_index_2_address(Config->PhysicalIndex));
    else if ( ( Config->TimePeriod > 1000 ) && ( Config->TimePeriod <= 30000000 ) )
    { return ( t_u32 )Config->TIMER_Arch.BaseRegister->CNT.All; }
    //        return (t_u32)((TIM_GetCounter(stm32f1_timer_physical_index_2_address(Config->PhysicalIndex))/2)*1000);
    else { return ( t_u32 )( ttc_systick_get_elapsed_usecs() - Config->TimeStart ); }

}
void                  timer_stm32l1xx_reset( t_ttc_timer_config* Config ) {

    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    timer_stm32l1xx_load_defaults( Config );

    t_register_stm32l1xx_timer* TIMER = timer_stm32l1xx_physical_index_2_address( Config->PhysicalIndex );

    if ( Config->TimePeriod <= 30000000 ) {
        TIMER->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );
        TIMER->CNT.All = 0;
        TIMER->CR1.All |= ( t_u16 )0x0001;
    }
    else {
        ttc_task_critical_begin();
        t_ttc_timer_config* CurrentEvent = HeadEvent; //Take the first element of the event list
        ttc_task_critical_end();

        if ( CurrentEvent->LogicalIndex == Config->LogicalIndex ) {
            HeadEvent = CurrentEvent->Next;
            _timer_stm32l1xx_insert_( CurrentEvent );
        }
        else {
            t_ttc_timer_config* LastEvent = HeadEvent;
            CurrentEvent = CurrentEvent->Next;
            while ( CurrentEvent->Next ) {
                if ( CurrentEvent->LogicalIndex == Config->LogicalIndex ) {
                    LastEvent->Next = CurrentEvent->Next;
                    _timer_stm32l1xx_insert_( CurrentEvent );
                }
                else {
                    LastEvent = LastEvent->Next;
                    CurrentEvent = CurrentEvent->Next;
                }
            }
        }
    }



}
e_ttc_timer_errorcode timer_stm32l1xx_set( t_ttc_timer_config* Config ) {

    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TIMER( Config->TimePeriod > 0, ttc_assert_origin_auto ); // pointers must not be NULL

    Config->TIMER_Arch.BaseRegister = timer_stm32l1xx_physical_index_2_address( Config->PhysicalIndex );

    timer_stm32l1xx_stop( Config->TIMER_Arch.BaseRegister );

    //timer_stm32l1xx_load_defaults(Config);


    if ( ( Config->TimePeriod > 100 ) && ( Config->TimePeriod <= 1000 ) ) {

        Config->TIMER_Arch.BaseRegister->PSC.All = ( t_u16 )2304;
        Config->TIMER_Arch.BaseRegister->ARR.All = ( t_u16 )( Config->TimePeriod / 100 * 3 );
        //Config->TIMER_Arch.BaseRegister->RCR = 0;
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )~( ( t_u16 )0x0300 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( ( t_u16 )0x0100 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( ( t_u16 )0x0080 );
        Config->TIMER_Arch.BaseRegister->EGR.All |= ( ( t_u16 )0x0001 );
        Config->TIMER_Arch.BaseRegister->SR.All = ( t_u16 )0x0;

        //Interruption Enabling
        if ( Config->Function != NULL ) {
            Config->Interrupt_Updating = ttc_interrupt_init( tit_TIMER_Updating,
                                                             Config->PhysicalIndex,
                                                             _driver_timer_isr,  // will be passed as argument to isr_Switch()
                                                             ( void* )Config,
                                                             NULL,
                                                             NULL );
            Assert_TIMER( Config->Interrupt_Updating, ttc_assert_origin_auto );
            ttc_interrupt_enable_timer( Config->Interrupt_Updating );
            Config->TIMER_Arch.BaseRegister->EGR.All |= ( ( t_u16 )0x0001 );

        }

    }
    else if ( ( Config->TimePeriod > 1000 ) && ( Config->TimePeriod <= 5000 ) ) {

        Config->TIMER_Arch.BaseRegister->PSC.All = ( t_u16 )8999;
        Config->TIMER_Arch.BaseRegister->ARR.All = ( t_u16 )( ( ( Config->TimePeriod ) / 1000 ) * 7 );
        //Config->TIMER_Arch.BaseRegister->RCR_ = 0;
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )~( ( t_u16 )0x0300 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( ( t_u16 )0x0100 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( ( t_u16 )0x0080 );
        Config->TIMER_Arch.BaseRegister->EGR.All |= ( ( t_u16 )0x0001 );
        Config->TIMER_Arch.BaseRegister->SR.All = ( t_u16 )0x0;

        //Interruption Enabling
        if ( Config->Function != NULL ) {

            Config->Interrupt_Updating = ttc_interrupt_init( tit_TIMER_Updating,
                                                             Config->PhysicalIndex,
                                                             _driver_timer_isr,  // will be passed as argument to isr_Switch()
                                                             ( void* )Config,
                                                             NULL,
                                                             NULL );
            Assert_TIMER( Config->Interrupt_Updating, ttc_assert_origin_auto );
            ttc_interrupt_enable_timer( Config->Interrupt_Updating );
            Config->TIMER_Arch.BaseRegister->EGR.All |= ( ( t_u16 )0x0001 );

        }


    }
    else if ( ( Config->TimePeriod > 5000 ) && ( Config->TimePeriod <= 30000000 ) ) {

        Config->TIMER_Arch.BaseRegister->PSC.All = ( t_u16 )35999;
        Config->TIMER_Arch.BaseRegister->ARR.All = ( t_u16 )( ( ( Config->TimePeriod ) / 1000 ) * 2 );
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )~( ( t_u16 )0x0300 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( ( t_u16 )0x0100 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( ( t_u16 )0x0080 );
        Config->TIMER_Arch.BaseRegister->EGR.All |= ( ( t_u16 )0x0001 );
        Config->TIMER_Arch.BaseRegister->SR.All = ( t_u16 )0x0;

        //Interruption Enabling
        if ( Config->Function != NULL ) {

            Config->Interrupt_Updating = ttc_interrupt_init( tit_TIMER_Updating,
                                                             Config->PhysicalIndex,
                                                             _driver_timer_isr,  // will be passed as argument to isr_Switch()
                                                             ( void* )Config,
                                                             NULL,
                                                             NULL );

            Assert_TIMER( Config->Interrupt_Updating, ttc_assert_origin_auto );
            ttc_interrupt_enable_timer( Config->Interrupt_Updating );
            Config->TIMER_Arch.BaseRegister->EGR.All |= ( ( t_u16 )0x0001 );

        }
    }
    else {
        //Software Timer comes here
        Config->TimeStart = ttc_systick_get_elapsed_usecs();
        _timer_stm32l1xx_insert_( Config );
    }


    /* Disable the Interrupt sources */
    timer_stm32l1xx_start( Config->TIMER_Arch.BaseRegister );

    /* Disable the Interrupt sources */
    Config->TIMER_Arch.BaseRegister->DIER.All &= ( t_u16 )~TIM_IT_Update;

    /* Clear the flags */
    Config->TIMER_Arch.BaseRegister->SR.All = ( t_u16 )~TIM_FLAG_Update;
    Config->TIMER_Arch.BaseRegister->SR.All = ( t_u16 )~TIM_IT_Update;

    /* Enable the Interrupt sources */
    Config->TIMER_Arch.BaseRegister->DIER.All |= TIM_IT_Update;


    /* Clean all interruption flags */
    return ec_timer_OK;
}



void                  timer_stm32l1xx_reload( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    Config->TIMER_Arch.BaseRegister = timer_stm32l1xx_physical_index_2_address( Config->PhysicalIndex );
    if ( ( Config->TimePeriod > 100 ) && ( Config->TimePeriod <= 1000 ) ) {
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );
        Config->TIMER_Arch.BaseRegister->CNT.All = ( t_u16 )( Config->TimePeriod / 100 ) * 3;
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( t_u16 ) 0x0001;
    }
    else if ( ( Config->TimePeriod > 1000 ) && ( Config->TimePeriod <= 5000 ) ) {
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );
        Config->TIMER_Arch.BaseRegister->CNT.All = ( t_u16 )( ( Config->TimePeriod / 1000 ) * 7 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( t_u16 ) 0x0001;
    }
    else if ( ( Config->TimePeriod > 5000 ) && ( Config->TimePeriod <= 30000000 ) ) {
        Config->TIMER_Arch.BaseRegister->CR1.All &= ( t_u16 )( ~( ( t_u16 )0x0001 ) );
        Config->TIMER_Arch.BaseRegister->CNT.All = ( t_u16 )( ( Config->TimePeriod / 1000 ) * 2 );
        Config->TIMER_Arch.BaseRegister->CR1.All |= ( t_u16 ) 0x0001;
    }
    else { Config->TimePeriod = Config->TimePeriod; }
}

/**
  * @brief  Configures the (Config_timer->BaseRegister) Prescaler.
  * @param  (Config_timer->BaseRegister): where x can be 2 to 11 to select the TIM peripheral.
  * @param  Prescaler: specifies the Prescaler Register value
  * @param  TIM_PSCReloadMode: specifies the TIM Prescaler Reload mode
  *   This parameter can be one of the following values:
  *     @arg TIM_PSCReloadMode_Update: The Prescaler is loaded at the update event.
  *     @arg TIM_PSCReloadMode_Immediate: The Prescaler is loaded immediatly.
  * @retval None
  */
void timer_stm32l1xx_prescaler_config( t_register_stm32l1xx_timer* Register, t_u16 Prescaler, t_u16 PSCReloadMode ) {
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL

    /* Set the Prescaler value */
    Register->PSC.All = Prescaler;

    /* Set or reset the UG Bit */
    Register->EGR.All = PSCReloadMode;
}

/**
  * @brief  Specifies the (Config_timer->BaseRegister) Counter Mode to be used.
  * @param  (Config_timer->BaseRegister): where x can be 2, 3 or 4 to select the TIM peripheral.
  * @param  TIM_CounterMode: specifies the Counter Mode to be used
  *   This parameter can be one of the following values:
  *     @arg TIM_CounterMode_Up: TIM Up Counting Mode
  *     @arg TIM_CounterMode_Down: TIM Down Counting Mode
  *     @arg TIM_CounterMode_CenterAligned1: TIM Center Aligned Mode1
  *     @arg TIM_CounterMode_CenterAligned2: TIM Center Aligned Mode2
  *     @arg TIM_CounterMode_CenterAligned3: TIM Center Aligned Mode3
  * @retval None
  */
void timer_stm32l1xx_counter_mode_config( t_register_stm32l1xx_timer* Register, t_u16 CounterMode ) {
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL

    t_u16 tmpcr1 = 0;

    tmpcr1 = Register->CR1.All ;

    /* Reset the CMS and DIR Bits */
    tmpcr1 &= ( t_u16 )( ~( ( t_u16 )( TIM_CR1_DIR | TIM_CR1_CMS ) ) );

    /* Set the Counter Mode */
    tmpcr1 |= CounterMode;

    /* Write to CR1 register */
    Register->CR1.All  = tmpcr1;

}


/**
  * @brief  Sets the (Config_timer->BaseRegister) Autoreload Register value
  * @param  (Config_timer->BaseRegister): where x can be 2 to 11 to select the TIM peripheral.
  * @param  Autoreload: specifies the Autoreload register new value.
  * @retval None
  */
void timer_stm32l1xx_set_autoreload( t_register_stm32l1xx_timer* Register, t_u32 Autoreload ) {
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL

    /* Set the Autoreload Register value */
    Register->ARR.All  = Autoreload;
}


/**
  * @brief  Gets the (Config_timer->BaseRegister) Prescaler value.
  * @param  (Config_timer->BaseRegister): where x can be 2 to 11 to select the TIM peripheral.
  * @retval Prescaler Register value.
  */
t_u16 timer_stm32l1xx_get_prescaler( t_register_stm32l1xx_timer* Register ) {
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL

    /* Get the Prescaler Register value */
    return Register->PSC.All ;

}



/**
  * @brief  Selects the (Config_timer->BaseRegister)�s One Pulse Mode.
  * @param  (Config_timer->BaseRegister): where x can be 2 to 11 to select the TIM peripheral.
  * @param  TIM_OPMode: specifies the OPM Mode to be used.
  *   This parameter can be one of the following values:
  *     @arg TIM_OPMode_Single
  *     @arg TIM_OPMode_Repetitive
  * @retval None
  */
void _timer_stm32l1xx_one_pulse_mode( t_register_stm32l1xx_timer* Register, t_u16 OPMode ) {
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL

    /* Reset the OPM Bit */
    Register->CR1.All  &= ( t_u16 )~( ( t_u16 )TIM_CR1_OPM );

    /* Configure the OPM Mode */
    Register->CR1.All  |= OPMode;
}

/**
  * @brief  Sets the (Config_timer->BaseRegister) Clock Division value.
  * @param  (Config_timer->BaseRegister): where x can be  2, 3, 4, 9, 10 or 11 to select the TIM peripheral.
  * @param  TIM_CKD: specifies the clock division value.
  *   This parameter can be one of the following value:
  *     @arg TIM_CKD_DIV1: TDTS = Tck_tim
  *     @arg TIM_CKD_DIV2: TDTS = 2*Tck_tim
  *     @arg TIM_CKD_DIV4: TDTS = 4*Tck_tim
  * @retval None
  */
void timer_stm32l1xx_set_clock_division( t_register_stm32l1xx_timer* Register, t_u16 CKD ) {
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL

    /* Reset the CKD Bits */
    Register->CR1.All  &= ( t_u16 )~( ( t_u16 )TIM_CR1_CKD );

    /* Set the CKD value */
    Register->CR1.All  |= CKD;
}


t_register_stm32l1xx_timer* timer_stm32l1xx_physical_index_2_address( t_u8 Index ) {

    Assert_TIMER( Index > 0, ttc_assert_origin_auto );
    Assert_TIMER( Index < TTC_INTERRUPT_TIMER_AMOUNT, ttc_assert_origin_auto );

    switch ( Index ) {         // determine base register and other low-level configuration data

        case 1:

            return ( t_register_stm32l1xx_timer* ) TIM2;
            break;

        case 2:

            return ( t_register_stm32l1xx_timer* ) TIM3;
            break;

        case 3:

            return ( t_register_stm32l1xx_timer* )TIM4;
            break;

        case 5:
            return ( t_register_stm32l1xx_timer* ) TIM6;
            break;
        case 6:

            return ( t_register_stm32l1xx_timer* ) TIM7;
            break;
        case 8:

            return ( t_register_stm32l1xx_timer* ) TIM9;
            break;

        case 9:
            return ( t_register_stm32l1xx_timer* ) TIM10;
            break;
        case 10:

            return ( t_register_stm32l1xx_timer* )TIM11;
            break;

        default: Assert_TIMER( 0, ttc_assert_origin_auto );
            break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
    }

    return ( t_register_stm32l1xx_timer* ) - 1;

}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************


void _timer_stm32l1xx_select_output_trigger( t_register_stm32l1xx_timer* Register, t_u16 Trigger ) {
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL

    /* Reset the MMS Bits */
    Register->CR2.All  &= ( t_u16 )~( ( t_u16 )0x0070 );

    /* Select the TRGO source */
    Register->CR2.All  |=  Trigger;

}

/* With this function you can enable o disable de update event */
void _timer_stm32l1xx_update_event( t_register_stm32l1xx_timer* Register, t_u32 NewState ) {
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL


    if ( NewState ) {

        /* enabled update event */
        Register->CR1.All  |= ( ( t_u16 )0x0002 );

    }
    else {

        /* enabled update event */
        Register->CR1.All  &= ( t_u16 )~( ( t_u16 )0x0002 );
    }

}

/* This function generates a update event */
void _timer_stm32l1xx_generate_update_event( t_register_stm32l1xx_timer* Register ) {
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL

    /* Generate an update event to reload the Prescaler value immediatly */
    Register->EGR.All  = ( t_u16 ) 0x0001;
}


/**
  * @brief  Sets the (Config_timer->BaseRegister) Clock Division value.
  * @param  (Config_timer->BaseRegister): where x can be  2, 3, 4, 9, 10 or 11 to select the TIM peripheral.
  * @param  TIM_CKD: specifies the clock division value.
  *   This parameter can be one of the following value:
  *     @arg TIM_CKD_DIV1: TDTS = Tck_tim
  *     @arg TIM_CKD_DIV2: TDTS = 2*Tck_tim
  *     @arg TIM_CKD_DIV4: TDTS = 4*Tck_tim
  * @retval None
  */
void _timer_stm32l1xx_set_clock_division( t_register_stm32l1xx_timer* Register, t_u16 CKD ) {
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL

    /* Reset the CKD Bits */
    Register->CR1.All  &= ( t_u16 )~( ( t_u16 )TIM_CR1_CKD );

    /* Set the CKD value */
    Register->CR1.All  |= CKD;
}

e_ttc_timer_errorcode _timer_stm32l1xx_clock_selection( t_register_stm32l1xx_timer* Register, e_timer_stm32l1xx_clock_source Source ) {
    Assert_TIMER( Register, ttc_assert_origin_auto ); // pointers must not be NULL

    /* Si Source is Internal Clock Source */
    if ( Source == timer_stm32l1xx_clock_source_internal_clock_source ) {

        if ( !Register->SMCR.All ) {

            /* C_EN bit activated */
            Register->CR1.All  |= ( t_u16 )0x0001;

        }
        else {

            return ec_timer_ERROR;
        }

    }
    else if ( ( Source = e_timer_stm32l1xx_clock_sourcexternl_clock_source_mode_1 ) ) {

        Register->SMCR.All  = 0b111;

    }
    else if ( ( Source = e_timer_stm32l1xx_clock_sourcexternl_clock_source_mode_2 ) ) {

        Register->SMCR.All  |= 0x000;

    }
    else {

        return ec_timer_ERROR;
    }

    return ec_timer_OK;

}


/**
  * @}
  */

/** @defgroup TIM_Group2 Output Compare management functions
 *  @brief    Output Compare management functions
 *
@verbatim
 ===============================================================================
                        Output Compare management functions
 ===============================================================================

       ===================================================================
              TIM Driver: how to use it in Output Compare Mode
       ===================================================================
       To use the Timer in Output Compare mode, the following steps are mandatory:

       1. Enable TIM clock using RCC_APBxPeriphClockCmd(RCC_APBxPeriph_TIMx, ENABLE) function

       2. Configure the TIM pins by configuring the corresponding GPIO pins

       2. Configure the Time base unit as described in the first part of this driver, if needed,
          else the Timer will run with the default configuration:
          - Autoreload value = 0xFFFF
          - Prescaler value = 0x0000
          - Counter mode = Up counting
          - Clock Division = TIM_CKD_DIV1

       3. Fill the TIM_OCInitStruct with the desired parameters including:
          - The TIM Output Compare mode: TIM_OCMode
          - TIM Output State: TIM_OutputState
          - TIM Pulse value: TIM_Pulse
          - TIM Output Compare Polarity : TIM_OCPolarity

       4. Call TIM_OCxInit(TIMx, &TIM_OCInitStruct) to configure the desired channel with the
          corresponding configuration

       5. Call the TIM_Cmd(ENABLE) function to enable the TIM counter.

       Note1: All other functions can be used separately to modify, if needed,
          a specific feature of the Timer.

       Note2: In case of PWM mode, this function is mandatory:
              TIM_OCxPreloadConfig(TIMx, TIM_OCPreload_ENABLE);

       Note3: If the corresponding interrupt or DMA request are needed, the user should:
              1. Enable the NVIC (or the DMA) to use the TIM interrupts (or DMA requests).
              2. Enable the corresponding interrupt (or DMA request) using the function
              TIM_ITConfig(TIMx, TIM_IT_CCx) (or TIM_DMA_Cmd(TIMx, TIM_DMA_CCx))

@endverbatim
  * @{
  */

/**
  * @brief  Initializes the TIMx Channel1 according to the specified
  *         parameters in the TIM_OCInitStruct.
  * @param  TIMx: where x can be 2, 3, 4, 9, 10 or 11 to select the TIM peripheral.
  * @param  TIM_OCInitStruct: pointer to a TIM_OCInitTypeDef structure
  *         that contains the configuration information for the specified TIM
  *         peripheral.
  * @retval None
  */

void _timer_stm32l1xx_output_compare_channel1_init( t_ttc_timer_config* Config ) {

    t_register_stm32l1xx_timer* Register;
    Register = Config->LowLevelConfig->BaseRegister;

    t_u16 tmpccmrx = 0, tmpccer = 0;

    /* Disable the Channel 1: Reset the CC1E Bit */
    Register->CCER.All  &= ( t_u16 )( ~( t_u16 )TIM_CCER_CC1E );

    /* Get the TIMx CCER register value */
    tmpccer = Register->CCER.All ;

    /* Get the TIMx CCMR1 register value */
    tmpccmrx = Register->CCMR1.All ;

    /* Reset the Output Compare Mode Bits */
    tmpccmrx &= ( t_u16 )( ~( ( t_u16 )TIM_CCMR1_OC1M ) );
    tmpccmrx &= ( t_u16 )( ~( ( t_u16 )TIM_CCMR1_CC1S ) );

    /* Select the Output Compare Mode */
    tmpccmrx |= Config->LowLevelConfig->oc_Mode;

    /* Reset the Output Polarity level */
    tmpccer &= ( t_u16 )( ~( ( t_u16 )TIM_CCER_CC1P ) );

    /* Set the Output Compare Polarity */
    tmpccer |= Config->LowLevelConfig->oc_Polarity;

    /* Set the Output State */
    tmpccer |= Config->LowLevelConfig->oc_OutputState;

    /* Set the Capture Compare Register value */
    Register->CCR1.All  = Config->LowLevelConfig->oc_Pulse;

    /* Write to TIMx CCMR1 */
    Register->CCMR1.All  = tmpccmrx;

    /* Write to TIMx CCER */
    Register->CCER.All  = tmpccer;
}

/**
  * @brief  Initializes the TIMx Channel2 according to the specified
  *         parameters in the TIM_OCInitStruct.
  * @param  TIMx: where x can be 2, 3, 4 or 9 to select the TIM peripheral.
  * @param  TIM_OCInitStruct: pointer to a TIM_OCInitTypeDef structure
  *         that contains the configuration information for the specified TIM
  *         peripheral.
  * @retval None
  */
void _timer_stm32l1xx_output_compare_channel2_init( t_ttc_timer_config* Config ) {

    t_register_stm32l1xx_timer* Register;
    Register = Config->LowLevelConfig->BaseRegister;

    t_u16 tmpccmrx = 0, tmpccer = 0;

    /* Disable the Channel 2: Reset the CC2E Bit */
    Register->CCER.All  &= ( t_u16 )( ~( ( t_u16 )TIM_CCER_CC2E ) );

    /* Get the TIMx CCER register value */
    tmpccer = Register->CCER.All ;

    /* Get the TIMx CCMR1 register value */
    tmpccmrx = Register->CCMR1.All ;

    /* Reset the Output Compare Mode Bits */
    tmpccmrx &= ( t_u16 )( ~( ( t_u16 )TIM_CCMR1_OC2M ) );

    /* Select the Output Compare Mode */
    tmpccmrx |= ( t_u16 )( Config->LowLevelConfig->oc_Mode << 8 );

    /* Reset the Output Polarity level */
    tmpccer &= ( t_u16 )( ~( ( t_u16 )TIM_CCER_CC2P ) );
    /* Set the Output Compare Polarity */
    tmpccer |= ( t_u16 )( Config->LowLevelConfig->oc_Polarity << 4 );

    /* Set the Output State */
    tmpccer |= ( t_u16 )( Config->LowLevelConfig->oc_OutputState << 4 );

    /* Set the Capture Compare Register value */
    Register->CCR2.All  = Config->LowLevelConfig->oc_Pulse;

    /* Write to TIMx CCMR1 */
    Register->CCMR1.All  = tmpccmrx;

    /* Write to TIMx CCER */
    Register->CCER.All  = tmpccer;
}

/**
  * @brief  Initializes the TIMx Channel3 according to the specified
  *         parameters in the TIM_OCInitStruct.
  * @param  TIMx: where x can be  2, 3 or 4 to select the TIM peripheral.
  * @param  TIM_OCInitStruct: pointer to a TIM_OCInitTypeDef structure
  *         that contains the configuration information for the specified TIM
  *         peripheral.
  * @retval None
  */
void _timer_stm32l1xx_output_compare_channel3_init( t_ttc_timer_config* Config ) {

    t_register_stm32l1xx_timer* Register;
    Register = Config->LowLevelConfig->BaseRegister;

    t_u16 tmpccmrx = 0, tmpccer = 0;

    /* Disable the Channel 2: Reset the CC2E Bit */
    Register->CCER.All  &= ( t_u16 )( ~( ( t_u16 )TIM_CCER_CC3E ) );

    /* Get the TIMx CCER register value */
    tmpccer = Register->CCER.All ;

    /* Get the TIMx CCMR2 register value */
    tmpccmrx = Register->CCMR2.All ;

    /* Reset the Output Compare Mode Bits */
    tmpccmrx &= ( t_u16 )( ~( ( t_u16 )TIM_CCMR2_OC3M ) );

    /* Select the Output Compare Mode */
    tmpccmrx |= Config->LowLevelConfig->oc_Mode;

    /* Reset the Output Polarity level */
    tmpccer &= ( t_u16 )( ~( ( t_u16 )TIM_CCER_CC3P ) );
    /* Set the Output Compare Polarity */
    tmpccer |= ( t_u16 )( Config->LowLevelConfig->oc_Polarity << 8 );

    /* Set the Output State */
    tmpccer |= ( t_u16 )( Config->LowLevelConfig->oc_OutputState << 8 );

    /* Set the Capture Compare Register value */
    Register->CCR3.All  = Config->LowLevelConfig->oc_Pulse;

    /* Write to TIMx CCMR2 */
    Register->CCMR2.All  = tmpccmrx;

    /* Write to TIMx CCER */
    Register->CCER.All  = tmpccer;
}

/**
  * @brief  Initializes the TIMx Channel4 according to the specified
  *         parameters in the TIM_OCInitStruct.
  * @param  TIMx: where x can be 2, 3 or 4 to select the TIM peripheral.
  * @param  TIM_OCInitStruct: pointer to a TIM_OCInitTypeDef structure
  *         that contains the configuration information for the specified TIM
  *         peripheral.
  * @retval None
  */
void _timer_stm32l1xx_output_compare_channel4_init( t_ttc_timer_config* Config ) {

    t_register_stm32l1xx_timer* Register;
    Register = Config->LowLevelConfig->BaseRegister;

    t_u16 tmpccmrx = 0, tmpccer = 0;

    /* Disable the Channel 2: Reset the CC4E Bit */
    Register->CCER.All  &= ( t_u16 )( ~( ( t_u16 )TIM_CCER_CC4E ) );

    /* Get the TIMx CCER register value */
    tmpccer = Register->CCER.All ;

    /* Get the TIMx CCMR2 register value */
    tmpccmrx = Register->CCMR2.All ;

    /* Reset the Output Compare Mode Bits */
    tmpccmrx &= ( t_u16 )( ~( ( t_u16 )TIM_CCMR2_OC4M ) );

    /* Select the Output Compare Mode */
    tmpccmrx |= ( t_u16 )( Config->LowLevelConfig->oc_Mode << 8 );

    /* Reset the Output Polarity level */
    tmpccer &= ( t_u16 )( ~( ( t_u16 )TIM_CCER_CC4P ) );
    /* Set the Output Compare Polarity */
    tmpccer |= ( t_u16 )( Config->LowLevelConfig->oc_Polarity << 12 );

    /* Set the Output State */
    tmpccer |= ( t_u16 )( Config->LowLevelConfig->oc_OutputState << 12 );

    /* Set the Capture Compare Register value */
    Register->CCR4.All  = Config->LowLevelConfig->oc_Pulse;

    /* Write to TIMx CCMR2 */
    Register->CCMR2.All  = tmpccmrx;

    /* Write to TIMx CCER */
    Register->CCER.All  = tmpccer;
}



/**
  * @brief  Enables or disables the TIMx peripheral Preload register on CCR1.
  * @param  TIMx: where x can be 2, 3, 4, 9, 10 or 11 to select the TIM peripheral.
  * @param  TIM_OCPreload: new state of the TIMx peripheral Preload register
  *   This parameter can be one of the following values:
  *     @arg TIM_OCPreload_Enable
  *     @arg TIM_OCPreload_Disable
  * @retval None
  */
void _timer_stm32l1xx_output_compare_channel1_preload_config( t_register_stm32l1xx_timer* Register, t_u16 oc_Preload ) {

    t_u16 tmpccmr1 = 0;

    tmpccmr1 = Register->CCMR1.All ;

    /* Reset the OC1PE Bit */
    tmpccmr1 &= ( t_u16 )~( ( t_u16 )TIM_CCMR1_OC1PE );

    /* Enable or Disable the Output Compare Preload feature */
    tmpccmr1 |= oc_Preload;

    /* Write to TIMx CCMR1 register */
    Register->CCMR1.All  = tmpccmr1;
}

/**
  * @brief  Enables or disables the TIMx peripheral Preload register on CCR2.
  * @param  TIMx: where x can be 2, 3, 4 or 9 to select the TIM peripheral.
  * @param  TIM_OCPreload: new state of the TIMx peripheral Preload register
  *   This parameter can be one of the following values:
  *     @arg TIM_OCPreload_Enable
  *     @arg TIM_OCPreload_Disable
  * @retval None
  */
void _timer_stm32l1xx_output_compare_channel2_preload_config( t_register_stm32l1xx_timer* Register, t_u16 oc_Preload ) {

    t_u16 tmpccmr1 = 0;

    tmpccmr1 = Register->CCMR1.All ;
    /* Reset the OC2PE Bit */
    tmpccmr1 &= ( t_u16 )~( ( t_u16 )TIM_CCMR1_OC2PE );
    /* Enable or Disable the Output Compare Preload feature */
    tmpccmr1 |= ( t_u16 )( oc_Preload << 8 );
    /* Write to TIMx CCMR1 register */
    Register->CCMR1.All  = tmpccmr1;
}

/**
  * @brief  Enables or disables the TIMx peripheral Preload register on CCR3.
  * @param  TIMx: where x can be 2, 3 or 4 to select the TIM peripheral.
  * @param  TIM_OCPreload: new state of the TIMx peripheral Preload register
  *   This parameter can be one of the following values:
  *     @arg TIM_OCPreload_Enable
  *     @arg TIM_OCPreload_Disable
  * @retval None
  */
void _timer_stm32l1xx_output_compare_channel3_preload_config( t_register_stm32l1xx_timer* Register, t_u16 oc_Preload ) {

    t_u16 tmpccmr2 = 0;

    tmpccmr2 = Register->CCMR2.All ;
    /* Reset the OC3PE Bit */
    tmpccmr2 &= ( t_u16 )~( ( t_u16 )TIM_CCMR2_OC3PE );
    /* Enable or Disable the Output Compare Preload feature */
    tmpccmr2 |= oc_Preload;
    /* Write to TIMx CCMR2 register */
    Register->CCMR2.All  = tmpccmr2;
}

/**
  * @brief  Enables or disables the TIMx peripheral Preload register on CCR4.
  * @param  TIMx: where x can be 2, 3 or 4 to select the TIM peripheral.
  * @param  TIM_OCPreload: new state of the TIMx peripheral Preload register
  *   This parameter can be one of the following values:
  *     @arg TIM_OCPreload_Enable
  *     @arg TIM_OCPreload_Disable
  * @retval None
  */
void _timer_stm32l1xx_output_compare_channel4_preload_config( t_register_stm32l1xx_timer* Register, t_u16 oc_Preload ) {

    t_u16 tmpccmr2 = 0;

    tmpccmr2 = Register->CCMR2.All;

    /* Reset the OC4PE Bit */
    tmpccmr2 &= ( t_u16 )~( ( t_u16 )TIM_CCMR2_OC4PE );

    /* Enable or Disable the Output Compare Preload feature */
    tmpccmr2 |= ( t_u16 )( oc_Preload << 8 );

    /* Write to TIMx CCMR2 register */
    Register->CCMR2.All  = tmpccmr2;
}


/**
  * @brief  Enables or disables TIMx peripheral Preload register on ARR.
  * @param  TIMx: where x can be  2 to 11 to select the TIM peripheral.
  * @param  NewState: new state of the TIMx peripheral Preload register
  *   This parameter can be: ENABLE or DISABLE.
  * @retval None
  */
void _timer_stm32l1xx_arr_preload_config( t_register_stm32l1xx_timer* Register, FunctionalState NewState ) {

    if ( NewState != DISABLE ) {
        /* Set the ARR Preload Bit */
        Register->CR1.All  |= TIM_CR1_ARPE;
    }
    else {
        /* Reset the ARR Preload Bit */
        Register->CR1.All  &= ( t_u16 )~( ( t_u16 )TIM_CR1_ARPE );
    }
}

void _timer_stm32l1xx_insert_( t_ttc_timer_config* Config ) {
    // set to TRUE once Event has been found
    bool found = 0;

    if ( !HeadEvent ) {
        ttc_task_critical_begin();
        HeadEvent = Config;
        ttc_task_critical_end();
    }
    else {
        ttc_task_critical_begin();
        t_ttc_timer_config* CurrentEvent = HeadEvent;
        ttc_task_critical_end();
        t_base ActualTime = 0;

        if ( CurrentEvent->TimePeriod <= ( ttc_systick_get_elapsed_usecs() - CurrentEvent->TimeStart ) )
        { ActualTime = 0; }
        else { ActualTime = ( ( CurrentEvent->TimePeriod - ( ttc_systick_get_elapsed_usecs() - CurrentEvent->TimeStart ) ) ); }

        if ( Config->TimePeriod >= ActualTime ) {
            while ( !found ) {
                if ( CurrentEvent->Next ) {
                    ttc_task_critical_begin();
                    t_ttc_timer_config* LastEvent = CurrentEvent;
                    CurrentEvent = CurrentEvent->Next;
                    ttc_task_critical_end();

                    if ( CurrentEvent->TimePeriod < ( ttc_systick_get_elapsed_usecs() - CurrentEvent->TimeStart ) )
                    { ActualTime = 0; }
                    else { ActualTime = ( ( CurrentEvent->TimePeriod - ( ttc_systick_get_elapsed_usecs() - CurrentEvent->TimeStart ) ) ); }

                    if ( Config->TimePeriod <= ActualTime ) {
                        ttc_task_critical_begin();
                        Config->TimeStart = ttc_systick_get_elapsed_usecs();
                        Config->Next = CurrentEvent;
                        LastEvent->Next = Config;
                        ttc_task_critical_end();
                        found = 1;
                    }
                }
                else {
                    ttc_task_critical_begin();
                    Config->TimeStart = ttc_systick_get_elapsed_usecs();
                    Config->Next = NULL;
                    CurrentEvent->Next = Config;
                    ttc_task_critical_end();
                    found = 1;
                }
            }
        }
        else {
            ttc_task_critical_begin();
            Config->TimeStart = ttc_systick_get_elapsed_usecs();
            Config->Next = HeadEvent;
            HeadEvent = Config;
            ttc_task_critical_end();
        }
    }
}
void _timer_stm32l1xx_() {
    while ( 1 ) {
        if ( HeadEvent ) {
            if ( HeadEvent->TimePeriod <= ( ttc_systick_get_elapsed_usecs() - HeadEvent->TimeStart ) ) {
                ttc_task_critical_begin();
                t_ttc_timer_config* CurrentEvent = HeadEvent; //Take the first element of the event list
                ttc_task_critical_end();

                if ( HeadEvent->Next ) {
                    ttc_task_critical_begin();
                    HeadEvent = HeadEvent->Next;
                }
                else {
                    ttc_task_critical_begin();
                    HeadEvent = NULL;
                }
                ttc_task_critical_end();

                CurrentEvent->Flags.Bits.Initialized = 0;
                CurrentEvent->Function( CurrentEvent->Argument );
            }
            else { uSleep( HeadEvent->TimePeriod - ( ttc_systick_get_elapsed_usecs() - HeadEvent->TimeStart ) ); }
        }
        else { uSleep( 1000 ); }
    }
}
void _timer_stm32l1xx_manage_( t_physical_index PhysicalIndex, void* Argument ) {
    Assert_TIMER( PhysicalIndex <= TTC_TIMER_AMOUNT, ttc_assert_origin_auto );

    _ttc_timer_isr( ( t_ttc_timer_config* ) Argument );
}


//} private functions
