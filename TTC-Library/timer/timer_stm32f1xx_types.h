#ifndef TIMER_STM32F1XX_TYPES_H
#define TIMER_STM32F1XX_TYPES_H

/** { timer_stm32f1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for TIMER devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by ttc_timer_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140204 09:25:54 UTC
 *
 *  Note: See ttc_timer.h for description of architecture independent TIMER implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ******************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

#define _driver_timer_isr    _timer_stm32f1xx_manage_ // define function pointer for receive interrupt service routine

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic.h"
#include "stm32f10x.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_timer_types.h ***********************

typedef struct { // register description (adapt according to stm32f1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_timer_register;

typedef struct {  // stm32f1xx specific configuration data of single TIMER device
    t_register_stm32f1xx_timer* BaseRegister;       // base address of TIMER device registers
} __attribute__( ( __packed__ ) ) t_timer_stm32f1xx_config;

// t_ttc_timer_architecture is required by ttc_timer_types.h
#define t_ttc_timer_architecture t_timer_stm32f1xx_config

//} Structures/ Enums


#endif //TIMER_STM32F1XX_TYPES_H
