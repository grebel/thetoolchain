#ifndef ARCHITECTURE_TIMER_TYPES_H
#define ARCHITECTURE_TIMER_TYPES_H

/** { timer_stm32l1.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *                     
 *  Low-Level datatypes for TIMER devices on stm32l1 architectures.
 *  Structures, Enums and Defines being required by ttc_timer_types.h
 *
 *  Note: See ttc_timer.h for description of architecture independent TIMER implementation.
 * 
 *  Authors: <AUTHOR>
 *
}*/
//{ Defines/ TypeDefs ******************************************************


//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic.h"

//} Includes
//{ Structures/ Enums required by ttc_timer_types.h ***********************

typedef enum { // register description (adapt according to stm32l1 registers)
    stm32l1_TIMER_None,
    stm32l1_TIM2 = (u32_t) TIM2,
    stm32l1_TIM3 = (u32_t) TIM3,
    stm32l1_TIM4 = (u32_t) TIM4,
    stm32l1_TIMER_NotImplemented
} TIMER_Register_e;

typedef struct {  // stm32l1 specific configuration data of single TIMER device
  TIMER_Register_e* TIMx;       // base address of TIMER device registers
  //TIM_TimeBaseInitTypeDef
  bool              OutputCompare;
  bool              InputCapture;
} __attribute__((__packed__)) timer_stm32l1_config_t;

// ttc_timer_arch_t is required by ttc_timer_types.h
typedef timer_stm32l1_config_t ttc_timer_arch_t;

//} Structures/ Enums


#endif //ARCHITECTURE_TIMER_TYPES_H
