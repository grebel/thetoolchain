/** { ttc_memory_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for MEMORY device.
 *  Structures, Enums and Defines being required by both, high- and low-level memory.
 *
 *  Created from template ttc_device_types.h revision 22 at 20140228 13:50:05 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_MEMORY_TYPES_H
#define TTC_MEMORY_TYPES_H

//{ Includes *************************************************************

#include "ttc_basic_types.h"
#ifdef EXTENSION_memory_stm32w1xx
    #include "memory/memory_stm32w1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_memory_stm32f1xx
    #include "memory/memory_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_memory_stm32l1xx
    #include "memory/memory_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_memory_stm32l0xx
    #include "memory/memory_stm32l0xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************


/* Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_MEMORY 0  # disable assert handling for memory devices
 *
 */
#ifndef TTC_ASSERT_MEMORY    // any previous definition set (Makefile)?
    #define TTC_ASSERT_MEMORY 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_MEMORY == 1)  // use Assert()s in MEMORY code (somewhat slower but alot easier to debug)
    #define Assert_MEMORY(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_MEMORY_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_MEMORY_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in MEMORY code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_MEMORY(Condition, Origin)
    #define Assert_MEMORY_Writable(Address, Origin)
    #define Assert_MEMORY_Readable(Address, Origin)
#endif


#ifndef TTC_ASSERT_MEMORY_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_MEMORY_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_MEMORY_EXTRA == 1)  // use Assert()s in MEMORY code (somewhat slower but alot easier to debug)
    #define Assert_MEMORY_EXTRA(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_MEMORY_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_MEMORY_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in MEMORY code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_MEMORY_EXTRA(Condition, Origin)
    #define Assert_MEMORY_EXTRA_Writable(Address, Origin)
    #define Assert_MEMORY_EXTRA_Readable(Address, Origin)
#endif

#ifdef malloc
    #error malloc from stdlib is not supported, use ttc_heap_alloc() instead!
#endif


// base datatype for sizes of memory blocks (must be a signed datatype)
#ifndef TTC_MEMORY_BLOCK_BASE
    #define TTC_MEMORY_BLOCK_BASE  t_s16
#endif



//}Static Configuration
//{ Enums/ Structures ****************************************************

//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef enum {    // e_ttc_switch_architecture  types of architectures supported by SWITCH driver
    ta_switch_None,           // no architecture selected


    ta_memory_stm32w1xx, // automatically added by ./create_DeviceDriver.pl
    ta_memory_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
    ta_memory_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
    ta_memory_stm32l0xx, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_switch_unknown        // architecture not supported
} e_ttc_switch_architecture;

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_MEMORY_TYPES_H
