#ifndef TTC_BOARD_H
#define TTC_BOARD_H
/** { ttc_board.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for board devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_BOARD(tc_board_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_board_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_board_init(LogicalIndex);
 *  4) use:         ttc_board_XXX(LogicalIndex);
 *
 *  Device Index
 *  Devices are addressed by a logical index with no correlation to the order of physical devices.
 *  Logical indices start at 1 to simply detect uninitialized values.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level board and application.
 *
 *  Created from template ttc_device.h revision 37 at 20180420 13:26:22 UTC
 *
 *  Define ranks of high-level and low-level drivers (required by create_DeviceDriver.pl to generate correct #ifdefs)
 *  TTC_RANK_HIGHLEVEL=490
 *  TTC_RANK_LOWLEVEL=110
 *
 *  Authors: GREGOR REBEL
 *
 *
 *  Description of ttc_board (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/

#ifndef EXTENSION_ttc_board
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_board.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_board.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_board_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level board only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function Declarations ************************************************{
 *
 * The functions declared below provide the main interface for
 * board devices on all supported architectures.
 * Check board/board_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** allocates a new board instance and returns pointer to its configuration
 *
 * @return (t_ttc_board_config*)  pointer to new configuration. Set all Init fields before calling ttc_board_init() on it.
 */
t_ttc_board_config* ttc_board_create();

/** Prepares board Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_board_prepare();
void _driver_board_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_board_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    logical index of BOARD device. Each logical device <n> is defined via TTC_BOARD<n>* constants in compile_options.h and extensions.active/makefile
 * @return                configuration of indexed device
 */
t_ttc_board_config* ttc_board_get_configuration( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    logical index of BOARD device. Each logical device <n> is defined via TTC_BOARD<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: board device has been initialized successfully; != 0: error-code
 */
e_ttc_board_errorcode ttc_board_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    logical index of BOARD device. Each logical device <n> is defined via TTC_BOARD<n>* constants in compile_options.h and extensions.active/makefile
 */
void ttc_board_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_board_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of BOARD device. Each logical device <n> is defined via TTC_BOARD<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: default values have been loaded successfully for indexed board device; != 0: error-code
 */
e_ttc_board_errorcode  ttc_board_load_defaults( t_u8 LogicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    logical index of BOARD device. Each logical device <n> is defined via TTC_BOARD<n>* constants in compile_options.h and extensions.active/makefile

 */
void ttc_board_reset( t_u8 LogicalIndex );

/** Reinitializes all initialized devices after changed systemcloc profile.
 *
 * Note: This function is called automatically from _ttc_sysclock_call_update_functions()
 */
void ttc_board_sysclock_changed();

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_board_ are passed to interfaces/ttc_board_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl board UPDATE
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_board_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_board_features.
 *
 * @param Config        Configuration of board device
 * @return              Fields of *Config have been adjusted if necessary
 */
void _driver_board_configuration_check( t_ttc_board_config* Config );

/** shutdown single BOARD unit device
 * @param Config        Configuration of board device
 * @return              == 0: BOARD has been shutdown successfully; != 0: error-code
 */
e_ttc_board_errorcode _driver_board_deinit( t_ttc_board_config* Config );

/** initializes single BOARD unit for operation
 * @param Config        Configuration of board device
 * @return              == 0: BOARD has been initialized successfully; != 0: error-code
 */
e_ttc_board_errorcode _driver_board_init( t_ttc_board_config* Config );

/** loads configuration of indexed BOARD unit with default values
 * @param Config        Configuration of board device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_board_errorcode _driver_board_load_defaults( t_ttc_board_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of board device
 */
void _driver_board_reset( t_ttc_board_config* Config );
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_BOARD_H
