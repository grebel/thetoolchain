/** { ttc_timer.c ****************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  High-Level interface for timer device.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported 
 *  architectures.
 *
 *  Created from template ttc_device.c revision 20 at 20140204 09:25:54 UTC
 *
 *  Authors: <AUTHOR>
 *  
}*/

#include "ttc_timer.h"

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of timer devices.
 *
 */
 
#if TTC_TIMER_AMOUNT == 0
  #warning No TIMER devices defined, check your makefile! (did you forget to activate something?)
#endif

// for each initialized device, a pointer to its generic and stm32f1xx definitions is stored
ttc_heap_array_define(ttc_timer_config_t*, ttc_timer_configs, TTC_TIMER_AMOUNT);

// Set to TRUE on first call of ttc_timer_init()
BOOL driver_Initialized = 0;

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

u8_t ttc_timer_get_max_index() {
    return TTC_TIMER_AMOUNT;
}
ttc_timer_config_t* ttc_timer_get_configuration(u8_t LogicalIndex) {
    Assert_TIMER(LogicalIndex, ec_InvalidArgument); // logical index starts at 1
    u8_t PhysicalIndex = ttc_timer_logical_2_physical_index(LogicalIndex);
    ttc_timer_config_t* Config = A(ttc_timer_configs, PhysicalIndex); // will assert if outside array bounds

    if (!Config) {
        Config = A(ttc_timer_configs, PhysicalIndex) = ttc_heap_alloc_zeroed(sizeof(ttc_timer_config_t));
//        ttc_timer_load_defaults(LogicalIndex);
        Config->LogicalIndex         = LogicalIndex;
        Config->PhysicalIndex        = PhysicalIndex;
        Config->eti_NextISR          = NULL;
        Config->eti_NextISR_Argument = NULL;
        Config->Next                 = NULL;
    }

    return Config;
}
void ttc_timer_deinit(u8_t LogicalIndex) {
    Assert_TIMER(LogicalIndex, ec_InvalidArgument); // logical index starts at 1
    ttc_timer_config_t* Config = ttc_timer_get_configuration(LogicalIndex);
  
    ttc_timer_errorcode_e Result = _driver_timer_deinit(Config);
    if (Result == ec_timer_OK)
      Config->Flags.Bits.Initialized = 0;
}
void ttc_timer_init(u8_t LogicalIndex) {
    Assert_TIMER(LogicalIndex, ec_InvalidArgument); // logical index starts at 1
    ttc_timer_config_t* Config = ttc_timer_get_configuration(LogicalIndex);

    ttc_timer_errorcode_e Result = _driver_timer_init(Config);
    if (Result == ec_timer_OK)
      Config->Flags.Bits.Initialized = 1;
}
void ttc_timer_load_defaults(u8_t LogicalIndex) {
    Assert_TIMER(LogicalIndex, ec_InvalidArgument); // logical index starts at 1
    ttc_timer_config_t* Config = ttc_timer_get_configuration(LogicalIndex);

    ttc_memory_set( Config, 0, sizeof(ttc_timer_config_t) );

    // load generic default values
    Config->LogicalIndex = LogicalIndex;

    _driver_timer_load_defaults(Config);
}
u8_t ttc_timer_logical_2_physical_index(u8_t LogicalIndex) {
  switch (LogicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_TIMER1
           case 1: return TTC_TIMER1;
#endif
#ifdef TTC_TIMER2
           case 2: return TTC_TIMER2;
#endif
#ifdef TTC_TIMER3
           case 3: return TTC_TIMER3;
#endif
#ifdef TTC_TIMER4
           case 4: return TTC_TIMER4;
#endif
#ifdef TTC_TIMER5
           case 5: return TTC_TIMER5;
#endif
#ifdef TTC_TIMER6
           case 6: return TTC_TIMER6;
#endif
#ifdef TTC_TIMER7
           case 7: return TTC_TIMER7;
#endif
#ifdef TTC_TIMER8
           case 8: return TTC_TIMER8;
#endif
#ifdef TTC_TIMER9
           case 9: return TTC_TIMER9;
#endif
#ifdef TTC_TIMER10
           case 10: return TTC_TIMER10;
#endif
      // extend as required
      
      default: Assert_TIMER(0, ec_UNKNOWN); break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
    }
    
    return -1;
}
void ttc_timer_prepare() {
  // add your startup code here (Singletasking!)
  _driver_timer_prepare();
}
void ttc_timer_reset(u8_t LogicalIndex) {
    Assert_TIMER(LogicalIndex, ec_InvalidArgument); // logical index starts at 1
    ttc_timer_config_t* Config = ttc_timer_get_configuration(LogicalIndex);

    _driver_timer_reset(Config);
}
ttc_timer_errorcode_e ttc_timer_set(u8_t LogicalIndex, void TaskFunction(void*), void* Argument, u32_t Period){
    Assert_TIMER(LogicalIndex, ec_InvalidArgument); // logical index starts at 1
    ttc_timer_config_t* Config = ttc_timer_get_configuration(LogicalIndex);

    Config->Function = TaskFunction;
    Config->Argument = Argument;
    Config->TimePeriod = Period;

    return _driver_timer_set(Config);
}
void ttc_timer_change_period(u8_t LogicalIndex, u32_t Period){
    Assert_TIMER(LogicalIndex, ec_InvalidArgument); // logical index starts at 1
    ttc_timer_config_t* Config = ttc_timer_get_configuration(LogicalIndex);

    _driver_timer_change_period(Config, Period);
}
u32_t ttc_timer_read_value(u8_t LogicalIndex){
    Assert_TIMER(LogicalIndex, ec_InvalidArgument); // logical index starts at 1
    ttc_timer_config_t* Config = ttc_timer_get_configuration(LogicalIndex);

    return ttc_driver_timer_read_value(Config);
}
//InsertFunctions above (DO NOT DELETE THIS LINE!)

//} Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_timer(u8_t LogicalIndex) {  }

//InsertPrivateDefinitions above (DO NOT DELETE THIS LINE!)

//}private functions
