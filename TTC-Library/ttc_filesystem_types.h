/** { ttc_filesystem_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for FILESYSTEM device.
 *  Structures, Enums and Defines being required by both, high- and low-level filesystem.
 *  This file does not provide function declarations.
 *
 *  Note: See ttc_filesystem.h for description of high-level filesystem implementation!
 *
 *  Created from template ttc_device_types.h revision 45 at 20180413 11:21:12 UTC
 *
 *  Authors: <AUTHOR>
 *
}*/

#ifndef TTC_FILESYSTEM_TYPES_H
#define TTC_FILESYSTEM_TYPES_H

//{ Includes1 ************************************************************
//
// Includes being required for static configuration.
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_filesystem.h" or "ttc_filesystem.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h"
#include "ttc_storage_types.h"

//} Includes
/** { Static Configuration ***********************************************
 *
 * Most ttc drivers are statically configured by constant definitions in makefiles.
 * A static configuration allows to hide certain code fragments and variables for the
 * compilation process. If used wisely, your code will be lean and fast and do only
 * what is required for the current configuration.
 */

// maximum amount of parallel opened directories
#ifndef TTC_FILESYSTEM_MAX_DIRECTORIES
    #define TTC_FILESYSTEM_MAX_DIRECTORIES 2
#endif

/** {  TTC_FILESYSTEMn has to be defined as constant in one of your makefiles
 *
 * Here we count how much devices have been defined and if all lower index devices have been defined too.
 */
#ifndef TTC_FILESYSTEM_AMOUNT //{ 10
    #ifdef TTC_FILESYSTEM10
        #ifndef TTC_FILESYSTEM9
            #      error TTC_FILESYSTEM10 is defined, but not TTC_FILESYSTEM9 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM8
            #      error TTC_FILESYSTEM10 is defined, but not TTC_FILESYSTEM8 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM7
            #      error TTC_FILESYSTEM10 is defined, but not TTC_FILESYSTEM7 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM6
            #      error TTC_FILESYSTEM10 is defined, but not TTC_FILESYSTEM6 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM5
            #      error TTC_FILESYSTEM10 is defined, but not TTC_FILESYSTEM5 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM4
            #      error TTC_FILESYSTEM10 is defined, but not TTC_FILESYSTEM4 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM3
            #      error TTC_FILESYSTEM10 is defined, but not TTC_FILESYSTEM3 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM2
            #      error TTC_FILESYSTEM10 is defined, but not TTC_FILESYSTEM2 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM1
            #      error TTC_FILESYSTEM10 is defined, but not TTC_FILESYSTEM1 - all lower TTC_FILESYSTEMn must be defined!
        #endif

        #define TTC_FILESYSTEM_AMOUNT 10
    #endif
#endif //}
#ifndef TTC_FILESYSTEM_AMOUNT //{ 9
    #ifdef TTC_FILESYSTEM9
        #ifndef TTC_FILESYSTEM8
            #      error TTC_FILESYSTEM9 is defined, but not TTC_FILESYSTEM8 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM7
            #      error TTC_FILESYSTEM9 is defined, but not TTC_FILESYSTEM7 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM6
            #      error TTC_FILESYSTEM9 is defined, but not TTC_FILESYSTEM6 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM5
            #      error TTC_FILESYSTEM9 is defined, but not TTC_FILESYSTEM5 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM4
            #      error TTC_FILESYSTEM9 is defined, but not TTC_FILESYSTEM4 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM3
            #      error TTC_FILESYSTEM9 is defined, but not TTC_FILESYSTEM3 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM2
            #      error TTC_FILESYSTEM9 is defined, but not TTC_FILESYSTEM2 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM1
            #      error TTC_FILESYSTEM9 is defined, but not TTC_FILESYSTEM1 - all lower TTC_FILESYSTEMn must be defined!
        #endif

        #define TTC_FILESYSTEM_AMOUNT 9
    #endif
#endif //}
#ifndef TTC_FILESYSTEM_AMOUNT //{ 8
    #ifdef TTC_FILESYSTEM8
        #ifndef TTC_FILESYSTEM7
            #      error TTC_FILESYSTEM8 is defined, but not TTC_FILESYSTEM7 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM6
            #      error TTC_FILESYSTEM8 is defined, but not TTC_FILESYSTEM6 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM5
            #      error TTC_FILESYSTEM8 is defined, but not TTC_FILESYSTEM5 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM4
            #      error TTC_FILESYSTEM8 is defined, but not TTC_FILESYSTEM4 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM3
            #      error TTC_FILESYSTEM8 is defined, but not TTC_FILESYSTEM3 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM2
            #      error TTC_FILESYSTEM8 is defined, but not TTC_FILESYSTEM2 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM1
            #      error TTC_FILESYSTEM8 is defined, but not TTC_FILESYSTEM1 - all lower TTC_FILESYSTEMn must be defined!
        #endif

        #define TTC_FILESYSTEM_AMOUNT 8
    #endif
#endif //}
#ifndef TTC_FILESYSTEM_AMOUNT //{ 7
    #ifdef TTC_FILESYSTEM7
        #ifndef TTC_FILESYSTEM6
            #      error TTC_FILESYSTEM7 is defined, but not TTC_FILESYSTEM6 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM5
            #      error TTC_FILESYSTEM7 is defined, but not TTC_FILESYSTEM5 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM4
            #      error TTC_FILESYSTEM7 is defined, but not TTC_FILESYSTEM4 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM3
            #      error TTC_FILESYSTEM7 is defined, but not TTC_FILESYSTEM3 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM2
            #      error TTC_FILESYSTEM7 is defined, but not TTC_FILESYSTEM2 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM1
            #      error TTC_FILESYSTEM7 is defined, but not TTC_FILESYSTEM1 - all lower TTC_FILESYSTEMn must be defined!
        #endif

        #define TTC_FILESYSTEM_AMOUNT 7
    #endif
#endif //}
#ifndef TTC_FILESYSTEM_AMOUNT //{ 6
    #ifdef TTC_FILESYSTEM6
        #ifndef TTC_FILESYSTEM5
            #      error TTC_FILESYSTEM6 is defined, but not TTC_FILESYSTEM5 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM4
            #      error TTC_FILESYSTEM6 is defined, but not TTC_FILESYSTEM4 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM3
            #      error TTC_FILESYSTEM6 is defined, but not TTC_FILESYSTEM3 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM2
            #      error TTC_FILESYSTEM6 is defined, but not TTC_FILESYSTEM2 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM1
            #      error TTC_FILESYSTEM6 is defined, but not TTC_FILESYSTEM1 - all lower TTC_FILESYSTEMn must be defined!
        #endif

        #define TTC_FILESYSTEM_AMOUNT 6
    #endif
#endif //}
#ifndef TTC_FILESYSTEM_AMOUNT //{ 5
    #ifdef TTC_FILESYSTEM5
        #ifndef TTC_FILESYSTEM4
            #      error TTC_FILESYSTEM5 is defined, but not TTC_FILESYSTEM4 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM3
            #      error TTC_FILESYSTEM5 is defined, but not TTC_FILESYSTEM3 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM2
            #      error TTC_FILESYSTEM5 is defined, but not TTC_FILESYSTEM2 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM1
            #      error TTC_FILESYSTEM5 is defined, but not TTC_FILESYSTEM1 - all lower TTC_FILESYSTEMn must be defined!
        #endif

        #define TTC_FILESYSTEM_AMOUNT 5
    #endif
#endif //}
#ifndef TTC_FILESYSTEM_AMOUNT //{ 4
    #ifdef TTC_FILESYSTEM4
        #ifndef TTC_FILESYSTEM3
            #      error TTC_FILESYSTEM4 is defined, but not TTC_FILESYSTEM3 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM2
            #      error TTC_FILESYSTEM4 is defined, but not TTC_FILESYSTEM2 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM1
            #      error TTC_FILESYSTEM4 is defined, but not TTC_FILESYSTEM1 - all lower TTC_FILESYSTEMn must be defined!
        #endif

        #define TTC_FILESYSTEM_AMOUNT 4
    #endif
#endif //}
#ifndef TTC_FILESYSTEM_AMOUNT //{ 3
    #ifdef TTC_FILESYSTEM3

        #ifndef TTC_FILESYSTEM2
            #      error TTC_FILESYSTEM3 is defined, but not TTC_FILESYSTEM2 - all lower TTC_FILESYSTEMn must be defined!
        #endif
        #ifndef TTC_FILESYSTEM1
            #      error TTC_FILESYSTEM3 is defined, but not TTC_FILESYSTEM1 - all lower TTC_FILESYSTEMn must be defined!
        #endif

        #define TTC_FILESYSTEM_AMOUNT 3
    #endif
#endif //}
#ifndef TTC_FILESYSTEM_AMOUNT //{ 2
    #ifdef TTC_FILESYSTEM2

        #ifndef TTC_FILESYSTEM1
            #      error TTC_FILESYSTEM2 is defined, but not TTC_FILESYSTEM1 - all lower TTC_FILESYSTEMn must be defined!
        #endif

        #define TTC_FILESYSTEM_AMOUNT 2
    #endif
#endif //}
#ifndef TTC_FILESYSTEM_AMOUNT //{ 1
    #ifdef TTC_FILESYSTEM1
        #define TTC_FILESYSTEM_AMOUNT 1
    #endif
#endif //}
#ifndef TTC_FILESYSTEM_AMOUNT //{ no devices defined at all
    INFO( "Missing definition for TTC_FILESYSTEM1. Using default type. Add valid definition to your makefile to get rid if this message!" );
    #define TTC_FILESYSTEM_AMOUNT 1
    #define TTC_FILESYSTEM1 E_ttc_filesystem_architecture_dosfs
#endif

//}
//}
/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_FILESYSTEM 0#         disable default asserts for filesystem driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_FILESYSTEM_EXTRA 1#   enable extra asserts for filesystem driver
 *
 * When debugging code that has been compiled with optimization (gcc option -O) then
 * certain variables may be optimized away. Declaring variables as VOLATILE_FILESYSTEM makes them
 * visible in debug session when asserts are enabled. You have no overhead at all if asserts are disabled.
 + foot_t* VOLATILE_FILESYSTEM VisiblePointer;
 *
 */
#ifndef TTC_ASSERT_FILESYSTEM    // any previous definition set (Makefile)?
    #define TTC_ASSERT_FILESYSTEM 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_FILESYSTEM == 1)  // use Assert()s in FILESYSTEM code (somewhat slower but alot easier to debug)
    #define Assert_FILESYSTEM(Condition, Origin)        Assert(Condition, Origin)
    #define Assert_FILESYSTEM_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_FILESYSTEM_Readable(Address, Origin) Assert_Readable(Address, Origin)
    #define VOLATILE_FILESYSTEM                         volatile
#else  // use no default Assert()s in FILESYSTEM code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_FILESYSTEM(Condition, Origin)
    #define Assert_FILESYSTEM_Writable(Address, Origin)
    #define Assert_FILESYSTEM_Readable(Address, Origin)
    #define VOLATILE_FILESYSTEM
#endif

#ifndef TTC_ASSERT_FILESYSTEM_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_FILESYSTEM_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_FILESYSTEM_EXTRA == 1)  // use Assert()s in FILESYSTEM code (somewhat slower but alot easier to debug)
    #define Assert_FILESYSTEM_EXTRA(Condition, Origin) Assert(Condition, Origin)
    #define Assert_FILESYSTEM_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_FILESYSTEM_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in FILESYSTEM code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_FILESYSTEM_EXTRA(Condition, Origin)
    #define Assert_FILESYSTEM_EXTRA_Writable(Address, Origin)
    #define Assert_FILESYSTEM_EXTRA_Readable(Address, Origin)
#endif
//}
/** { Check static configuration of filesystem devices
 *
 * Each TTC_FILESYSTEMn must be defined as one from e_ttc_filesystem_architecture.
 * Additional defines of type TTC_FILESYSTEMn_* may be added.
 *
 * The constants being checked here must be defined in your makefile.
 * Most constants are defined by EXTENSION_100_board_* or EXTENSION_150_board_extension_* files.
 */

#ifdef TTC_FILESYSTEM1

    // check extra defines for filesystem device #1 here

#endif
#ifdef TTC_FILESYSTEM2

    // check extra defines for filesystem device #2 here

#endif
#ifdef TTC_FILESYSTEM3

    // check extra defines for filesystem device #3 here

#endif
#ifdef TTC_FILESYSTEM4

    // check extra defines for filesystem device #4 here

#endif
#ifdef TTC_FILESYSTEM5

    // check extra defines for filesystem device #5 here

#endif
#ifdef TTC_FILESYSTEM6

    // check extra defines for filesystem device #6 here

#endif
#ifdef TTC_FILESYSTEM7

    // check extra defines for filesystem device #7 here

#endif
#ifdef TTC_FILESYSTEM8

    // check extra defines for filesystem device #8 here

#endif
#ifdef TTC_FILESYSTEM9

    // check extra defines for filesystem device #9 here

#endif
#ifdef TTC_FILESYSTEM10

    // check extra defines for filesystem device #10 here

#endif
//}

//}Static Configuration

//{ Includes2 ************************************************************
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_filesystem.h" or "ttc_filesystem.c"
//

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_filesystem_dosfs
    #include "filesystem/filesystem_dosfs_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_filesystem_dosfs
    #include "filesystem/filesystem_dosfs_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

/** { Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_filesystem_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_filesystem_architecture
    #  warning Missing low-level definition for t_ttc_filesystem_architecture (using default)
    #define t_ttc_filesystem_architecture void
#endif
//}

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_filesystem_errorcode       return codes of FILESYSTEM devices
    E_ttc_filesystem_errorcode_OK = 0,

    // other warnings go here..

    E_ttc_filesystem_errorcode_ERROR,                  // general failure
    E_ttc_filesystem_errorcode_NULL,                   // NULL pointer not accepted
    E_ttc_filesystem_errorcode_DeviceNotFound,         // corresponding device could not be found
    E_ttc_filesystem_errorcode_InvalidConfiguration,   // sanity check of device configuration failed
    E_ttc_filesystem_errorcode_InvalidImplementation,  // your code does not behave as expected
    E_ttc_filesystem_errorcode_InvalidPartionTable,    // device cannot be mounted because its partition table is not supported or defective
    E_ttc_filesystem_errorcode_CannotReadVolumeInfo,   // volume information of current partition cannot be read
    E_ttc_filesystem_errorcode_CannotReadDirectory,    // desired directory cannot be read

    // other failures go here..

    E_ttc_filesystem_errorcode_unknown                // no valid errorcodes past this entry
} e_ttc_filesystem_errorcode;
typedef enum {   // e_ttc_filesystem_architecture    types of architectures supported by FILESYSTEM driver
    E_ttc_filesystem_architecture_None = 0,       // no architecture selected


    E_ttc_filesystem_architecture_dosfs, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    E_ttc_filesystem_architecture_unknown        // architecture not supported
} e_ttc_filesystem_architecture;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef union {  // architecture dependent configuration

#ifdef EXTENSION_filesystem_dosfs
    t_filesystem_dosfs_config dosfs;  // automatically added by ./create_DeviceDriver.pl
#endif
    //InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_filesystem_architecture;
typedef struct s_ttc_filesystem_features { // static minimum, maximum and default values of features of single filesystem

    t_u16 Scratch_AmountBlocks_Minimum; // minimum required  size of scratch buffers (divided by 512)
    t_u16 Scratch_AmountBlocks_Maximum; // maximum supported size of scratch buffers (divided by 512)
    t_u16 Cache_AmountBlocks_Minimum;   // minimum required  size of scratch buffers (divided by 512)
    t_u16 Cache_AmountBlocks_Maximum;   // maximum supported size of scratch buffers (divided by 512)
    t_u16 BlockSize_Minimum;            // minimum supported size of individual data blocks
    t_u16 BlockSize_Maximum;            // maximum supported size of individual data blocks
    unsigned Supports_FAT12;            // ==1: filesystem can read/write FAT12 filesystem
    unsigned Supports_FAT16;            // ==1: filesystem can read/write FAT16 filesystem
    unsigned Supports_FAT32;            // ==1: filesystem can read/write FAT32 filesystem
    unsigned Supports_EXFAT;            // ==1: filesystem can read/write EXFAT filesystem
    unsigned Supports_NTFS;             // ==1: filesystem can read/write NTFS filesystem
    unsigned Supports_EXT2;             // ==1: filesystem can read/write EXT2 filesystem
    unsigned Supports_EXT3;             // ==1: filesystem can read/write EXT3 filesystem
    unsigned Supports_EXT4;             // ==1: filesystem can read/write EXT4 filesystem

} __attribute__( ( __packed__ ) ) t_ttc_filesystem_features;
typedef struct s_ttc_filesystem_config { //          architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_filesystem_init()
    //       and after ttc_filesystem_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // Hint: Order structure elements for optimal alignment.
    //       You should place smaller datatypes like t_u8 and t_u16 after all t_u32 and pointer types
    //       to prevent insertion of pad-bytes. Most architectures don't support placing 32-bit datatypes at
    //       unaligned address.

    struct { // Init: fields to be set by application before first calling ttc_filesystem_init() --------------
        // Do not change these values after calling ttc_filesystem_init()!

        t_u8* Scratch;              // buffer memory for temporary data during filesystem operations
        t_u8  Scratch_AmountBlocks; // >=1: size of Scratch[] divided by 512 (see Features->Scratch_AmountBlocks_* for sensefull values!)

        t_u8* Cache;                /* buffer memory used to speed up filesystem operations. Size must be multiple of 512.
                                  * (Cache[] can be used for other purposes when filesystem is unmounted). */
        t_u8  Cache_AmountBlocks;   // >=1: size of Cache[] divided by 512 (see Features->Cache_AmountBlocks_* for sensefull values!)

        struct { // Set of storage depended configuration
            // Note: User must set these fields to provide access to actual storage device!

            // Configuration pointer passed as first argument to all functions below.
            void* Config;

            /** Function Pointer: Read one or more data blocks from storage device into given buffer
             *
             * @param Config       (t_ttc_filesystem_config*) Configuration of filesystem device
             * @param Buffer       (t_u8*)  Storage in RAM big egnough for Config->Init.BlockSize * AmountBlocks
             * @param Address      (t_u32)  Address of first block to read (will read addresses Address..Address+AmountBlocks-1
             * @param AmountBlocks (t_u8)   Amount of consecutive data blocks to read
             * @return             (e_ttc_filesystem_errorcode)   ==0: desired amount of data blocks was read successfully; >0: error code
             */
            t_u8( *blocks_read )( void* Config, t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks );
            //X t_u8( *blocks_read )( void*, t_u8*, t_u32, t_u8 );

            /** Function Pointer: Write one or more data blocks from given buffer onto storage device
             *
             * @param Config       (t_ttc_filesystem_config*) Configuration of filesystem device
             * @param Buffer       (const t_u8*)  Storage in RAM big egnough for Config->Init.BlockSize * AmountBlocks
             * @param Address      (t_u32)  Address of first block to read (will read addresses Address..Address+AmountBlocks-1
             * @param AmountBlocks (t_u8)   Amount of consecutive data blocks to read
             * @return             (e_ttc_filesystem_errorcode)   ==0: desired amount of data blocks was read successfully; >0: error code
             */
            t_u8( *blocks_write )( void* Config, const t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks );

            /** Fast check to see if n storage media has been inserted or removed in indexed device
             *
             * This check should be implemented as fast as possible as it will probably
             * be called regular at a high frequency.
             *
             * @param Config       Configuration of storage media device
             * @return             ==0: no storage media available; >0: storage media detected or error code
             */
            e_ttc_storage_event( *medium_detect )( void* Config );

            /** Powers up storage media in slot and prepares it for usage
             *
             * Will do nothing, if no storage media is detected.
             *
             * @param Config       Configuration of storage media device
             * @param BlockSize    pointer to 16 bit storage. medium_mount() must store current block size in this storage
             * @return             == 0: storage media device has been mounted successfully and can be used; != 0: error-code
             */
            t_u8( *medium_mount )( void* Config, t_u16* BlockSize );

            /** Powers down storage media to be removed safely
             *
             * Will do nothing if storage media is currently not mounted.
             *
             * @param Config       Configuration of storage media device
             * @return             == 0: storage media device has been unmounted successfully; != 0: error-code (inform user not to remove card before unmounting it)
             */
            t_u8( *medium_unmount )( void* Config );

        } Storage;

        struct { // generic initial configuration bits common for all low-level Drivers
            unsigned unused : 1;  // ==1:
            // ToDo: additional high-level flags go here..
        } Flags;
    } Init;

    // Fields below are read-only for application! ----------------------------------------------------------

    u_ttc_filesystem_architecture* LowLevelConfig; // low-level configuration (structure defined by low-level driver)

    const t_ttc_filesystem_features* Features;     // constant features of this filesystem driver

    struct { // status flags common for all architectures
        unsigned Initialized        : 1;  // ==1: device has been initialized successfully
        // ToDo: additional high-level flags go here..
    } Flags;

    // Last returned error code
    // IMPORTANT: Every ttc_filesystem_*() function that returns e_ttc_filesystem_errorcode has to update this value if it returns an error!
    e_ttc_filesystem_errorcode LastError;

    t_u8                          LogicalIndex;  // automatically set: logical index of device to use (1 = TTC_FILESYSTEM1, ...)
    t_u8                          PhysicalIndex; // automatically set: index of physical device to use (0 = filesystem #1, ...)
    e_ttc_filesystem_architecture Architecture;  // type of architecture used for current filesystem device
    t_u16                         BlockSize;     // size of individual data blocks in current mounted filesystem
    e_ttc_storage_event           LastEvent;     // last reported medium event

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_filesystem_config;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_FILESYSTEM_TYPES_H
