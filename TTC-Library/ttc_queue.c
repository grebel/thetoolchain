/*{ ttc_queue.c ***********************************************
     *
     * Written by Gregor Rebel 2010-2018
     *
     * Architecture independent support for inter task communication & synchronization.
     *
     * Currently supported schedulers: FreeRTOS
    }*/
#include "ttc_queue.h"
#include "ttc_heap.h"
#include "ttc_interrupt.h"// required here to avoid endless include loop in header files

// global variables ********************************************

// Variables used by different interrupt service routines (isr).
// Declaring their local variables as global saves stack memory and speeds up isr.
static t_u8              ttc_queue_AmountRemaining;
static e_ttc_queue_error ttc_queue_Error;


//}global variables
//{ functions *************************************************

// generic queue (external implementation based on scheduler)
t_ttc_queue_generic ttc_queue_generic_create( t_base AmountOfEntries, t_base SizeOfEntries ) {
    t_ttc_queue_generic NewQueue = NULL;

#ifdef EXTENSION_300_scheduler_freertos
    NewQueue = ( t_ttc_queue_generic ) xQueueCreate( AmountOfEntries, SizeOfEntries );
#endif

    Assert_QUEUES( NewQueue != NULL, ttc_assert_origin_auto );  // could not create queue (no implementation/ not egnough memory)

    return NewQueue;
}
e_ttc_queue_error   ttc_queue_generic_push_back( t_ttc_queue_generic Queue, const void* Entry, t_base TimeOut ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );
    Assert_QUEUES( Entry != 0, ttc_assert_origin_auto );
    Assert_QUEUES( ttc_task_is_scheduler_started(), ttc_assert_origin_auto );  // start scheduler before using this function!

#ifdef EXTENSION_300_scheduler_freertos
    if ( xQueueSend( ( xQueueHandle ) Queue, Entry, TimeOut ) == pdTRUE )
    { return tqe_OK; }
    else
    { return tqe_TimeOut; }
#endif

    return tqe_NotImplemented;
}
e_ttc_queue_error   ttc_queue_generic_push_back_isr( t_ttc_queue_generic Queue, const void* Entry ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );
    Assert_QUEUES( Entry != 0, ttc_assert_origin_auto );

#ifdef EXTENSION_300_scheduler_freertos
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    static signed portBASE_TYPE Status;
    Status = xQueueSendToBackFromISR( ( xQueueHandle ) Queue, Entry, &xHigherPriorityTaskWoken );

    if ( xHigherPriorityTaskWoken == pdFALSE ) {
        if ( Status == pdPASS )
        { return tqe_OK; }
        else
        { return tqe_QueuePushFailed; }
    }
    else
    { return tqe_HigherPriorityTaskWoken; }
#endif

    return tqe_NotImplemented;
}
e_ttc_queue_error   ttc_queue_generic_pull_front( t_ttc_queue_generic Queue, void* Entry, t_base TimeOut ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );
    Assert_QUEUES( Entry != 0, ttc_assert_origin_auto );
    Assert_QUEUES( ttc_task_is_scheduler_started(), ttc_assert_origin_auto );  // start scheduler before using this function!

#ifdef EXTENSION_300_scheduler_freertos
    if ( xQueueReceive( ( xQueueHandle ) Queue, Entry, TimeOut ) )
    { return tqe_OK; }
    else
    { return tqe_TimeOut; }
#endif

    return tqe_NotImplemented;
}
e_ttc_queue_error   ttc_queue_generic_pull_front_isr( t_ttc_queue_generic Queue, void* Entry ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );
    Assert_QUEUES( Entry != 0, ttc_assert_origin_auto );

#ifdef EXTENSION_300_scheduler_freertos
    static signed portBASE_TYPE pxTaskWoken;
    pxTaskWoken = 0;
    if ( xQueueReceiveFromISR( ( xQueueHandle ) Queue, Entry, &pxTaskWoken ) ) {
        if ( pxTaskWoken )
        { return tqe_HigherPriorityTaskWoken; }
        return tqe_OK;
    }
    else
    { return tqe_TimeOut; }
#endif

    return tqe_NotImplemented;
}
t_base              ttc_queue_amount_waiting( t_ttc_queue_generic Queue ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );
    Assert_QUEUES( ttc_task_is_scheduler_started(), ttc_assert_origin_auto );  // start scheduler before using this function!

#ifdef EXTENSION_300_scheduler_freertos
    return uxQueueMessagesWaiting( ( xQueueHandle ) Queue );
#endif

    return 0;
}

#ifdef REGRESSION_PIN_QUEUE_PUSH
    // output pins for speed tests via external oscilloscope (-> regression_queue.c)
    extern void regression_queue_togglePin_QueuePushActivity();
    extern void regression_queue_setPin_QueuePush( BOOL Set );
    extern void regression_queue_setPin_QueuePull( BOOL Set );
    extern void regression_queue_setPin_QueuePushISR( BOOL Set );
    extern void regression_queue_mark_test( t_u16 Time );
#endif

// simple byte queue (fast + low memory usage + independent from scheduler)
e_ttc_queue_error    _ttc_queue_byte_push_back_try( t_ttc_queue_bytes* Queue, t_u8 Data ) {

    t_u8* FirstUnused = Queue->FirstUnused;
    t_u8* NewEnd = FirstUnused + 1;
    if ( NewEnd > Queue->Last )
    { NewEnd = Queue->First; }

    if ( Queue->FirstUsed == NewEnd ) {
        return tqe_QueuePushFailed; // queue is full
    }

    *FirstUnused = Data; // storing at old end index
    Queue->FirstUnused = NewEnd;

    return tqe_OK;
}
e_ttc_queue_error    _ttc_queue_byte_pull_front( t_ttc_queue_bytes* Queue, t_u8* Data ) {
    t_u8* FirstUsed = Queue->FirstUsed;
    Assert_QUEUES( FirstUsed <= Queue->Last, ttc_assert_origin_auto );

    if ( FirstUsed == Queue->FirstUnused ) {    // queue is empty
        return tqe_QueueIsEmpty;
    }

    *Data = *FirstUsed++;

    if ( FirstUsed > Queue->Last )
    { FirstUsed = Queue->First; }                 // wrapping around at end of array
    Queue->FirstUsed = FirstUsed;

    return tqe_OK;
}
void                 _ttc_queue_byte_process_pending( t_ttc_queue_bytes* Queue ) {

    // function gets called from interrupt service routine only: no need to waist stack memory
    static t_u8* tqbpp_Data;
    static t_u8* tqbpp_Writer;

    tqbpp_Data = Queue->PendingPush;

    while ( Queue->AmountPendingPushes ) { // process pending pushes
        if ( _ttc_queue_byte_push_back_try( Queue, *tqbpp_Data++ ) > tqe_Error ) { // cannot process all pending: cleanup
            ttc_queue_AmountRemaining = Queue->AmountPendingPushes;
            tqbpp_Writer = Queue->PendingPush;
            tqbpp_Data--; // back to last pending Data
            while ( ttc_queue_AmountRemaining-- ) {
                *tqbpp_Writer++ = *tqbpp_Data++;
            }
            break;
        }
        Queue->AmountPendingPushes--;
    }
}

t_ttc_queue_bytes*    ttc_queue_byte_create( t_u16 AmountOfEntries ) {
    Assert_QUEUES( AmountOfEntries, ttc_assert_origin_auto );  // need at least size 1

    // need to allocate one additional entry for queue-management
    AmountOfEntries++;

    t_ttc_queue_bytes* Queue = ttc_heap_alloc( AmountOfEntries + sizeof( t_ttc_queue_bytes ) );
    ttc_queue_byte_init( Queue, ( t_u8* )( Queue + 1 ), AmountOfEntries );

    return Queue;
}
void                  ttc_queue_byte_init( t_ttc_queue_bytes* Queue, t_u8* Storage, t_u16 AmountOfEntries ) {
    Assert_QUEUES( AmountOfEntries, ttc_assert_origin_auto );  // need at least size 1

    ttc_memory_set( Queue, 0, sizeof( *Queue ) );
    Queue->First = Storage;
    Queue->Last  = Storage + AmountOfEntries - 1 ;
    Queue->FirstUsed = Queue->FirstUnused = Queue->First;
#ifdef TTC_REGRESSION
    Queue->Size = AmountOfEntries;
#endif

    ttc_mutex_init( &( Queue->Lock ) );
    Queue->Data  = Storage;
}
t_base                ttc_queue_byte_get_amount( t_ttc_queue_bytes* Queue ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );

    t_base_signed Amount = Queue->FirstUnused - Queue->FirstUsed;
    if ( Amount < 0 )
    { Amount += Queue->Last - Queue->First + 1; }

#ifdef TTC_REGRESSION
    if ( Queue->FirstUnused < Queue->FirstUsed ) {
        Assert_QUEUES( Amount == Queue->FirstUnused + Queue->Size - Queue->FirstUsed, ttc_assert_origin_auto );
    }
    else {
        Assert_QUEUES( Amount == Queue->FirstUnused - Queue->FirstUsed, ttc_assert_origin_auto );
    }
#endif

    return Amount + Queue->AmountPendingPushes;
}
void                  ttc_queue_byte_push_back( t_ttc_queue_bytes* Queue, t_u8 Data ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );
    e_ttc_queue_error Error = tqe_UNKNOWN;

    ttc_mutex_lock_endless( &( Queue->Lock ) );
#if TTC_TASK_SCHEDULER_AVAILABLE==1
    ttc_task_critical_begin();
#endif
    do {
        if ( Queue->AmountPendingPushes ) { // data from isr is pending to be pushed: complete push
            ttc_interrupt_all_disable();
            _ttc_queue_byte_process_pending( Queue );
            ttc_interrupt_all_enable();
        }
        Error = _ttc_queue_byte_push_back_try( Queue, Data );
        if ( Error != tqe_OK ) {  // Queue full: append ourself to list of waiting tasks
#ifdef TTC_REGRESSION
            Queue->AmountTaskWaiting2Push++;
#endif
            ttc_mutex_unlock( &( Queue->Lock ) );
#if TTC_QUEUE_USE_WAKEUP_INTERLEAVE == 1
            if ( Queue->TasksWaiting2Pull.First ) { // at least one task is waiting to pull: awake it
                if ( Queue->Counter_WakeUpPuller == 0 ) {
                    Queue->Counter_WakeUpPuller = Queue->Interleave_WakeUpPuller;
                    ttc_task_waitinglist_awake( &( Queue->TasksWaiting2Pull ) );
                }
                else
                { Queue->Counter_WakeUpPuller--; }
            }
#else
            ttc_task_waitinglist_awake( &( Queue->TasksWaiting2Pull ) );
#endif
#ifdef REGRESSION_PIN_QUEUE_PUSH
            regression_queue_setPin_QueuePush( 0 );
#endif
            ttc_task_waitinglist_wait( &( Queue->TasksWaiting2Push ), -1 ); // will end + restore critical section
            Error = tqe_QueuePushFailed;
            if ( !ttc_mutex_lock_try( &( Queue->Lock ) ) ) { // could not obtain lock directly: wait for it
#if TTC_TASK_SCHEDULER_AVAILABLE==1
                ttc_task_critical_end();
#endif
                ttc_mutex_lock_endless( &( Queue->Lock ) );
#if TTC_TASK_SCHEDULER_AVAILABLE==1
                ttc_task_critical_begin();
#endif
            }
        }
        else {                    // push successfull
            ttc_mutex_unlock( &( Queue->Lock ) ); // byte pushed: free lock
            if ( Queue->TasksWaiting2Pull.First ) { // at least one task is waiting to pull: awake it
                ttc_task_waitinglist_awake( &( Queue->TasksWaiting2Pull ) ); // ToDo: implement delayed wake up
            }
#ifdef REGRESSION_PIN_QUEUE_PUSH
            regression_queue_setPin_QueuePush( 0 );
#endif
        }
    }
    while ( Error != tqe_OK );
#if TTC_TASK_SCHEDULER_AVAILABLE==1
    ttc_task_critical_end();
#endif
}
e_ttc_queue_error     ttc_queue_byte_push_back_isr( t_ttc_queue_bytes* Queue, t_u8 Data ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );

    if ( ! ttc_mutex_is_locked( &( Queue->Lock ) ) ) {
        if ( Queue->AmountPendingPushes )
        { _ttc_queue_byte_process_pending( Queue ); }
        ttc_queue_Error = _ttc_queue_byte_push_back_try( Queue, Data );
    }
    else { // queue is currently locked by a task: store pending byte in extra isr-storage
        if ( Queue->AmountPendingPushes < TTC_SQB_ISR_SPACE ) {
            Queue->PendingPush[Queue->AmountPendingPushes++] = Data;
            ttc_queue_Error = tqe_IsrOperationPending;
        }
        else // no more space left for pending pushes
        { ttc_queue_Error = tqe_QueueIsLocked; }
#ifdef TTC_REGRESSION
        Queue->AmountPendingTotal++;
#endif
    }
    if ( Queue->TasksWaiting2Pull.First ) { // at least one task is waiting to pull: awake it
        ttc_task_waitinglist_awake_isr( &( Queue->TasksWaiting2Pull ) );
    }

    return ttc_queue_Error;
}
t_u8                  ttc_queue_byte_pull_front( t_ttc_queue_bytes* Queue ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );

    ttc_mutex_lock_endless( &( Queue->Lock ) );
#ifdef TTC_REGRESSION
    Queue->AmountTaskWaiting2Pull++;
#endif
    t_u8 Data;
    e_ttc_queue_error Error;

    do {
#if TTC_TASK_SCHEDULER_AVAILABLE==1
        if ( ! ttc_task_is_inside_critical_section() ) { // make sure that we are inside critical section
            ttc_task_critical_begin();
        }
#endif
        if ( Queue->AmountPendingPushes )                              { // data from isr is pending to be pushed: complete push
            ttc_interrupt_all_disable();
            _ttc_queue_byte_process_pending( Queue );
            ttc_interrupt_all_enable();
        }
        Error = _ttc_queue_byte_pull_front( Queue, &Data );
        if ( Error == tqe_OK ) {
            ttc_mutex_unlock( &( Queue->Lock ) ); // received byte: free lock
            if ( Queue->TasksWaiting2Push.First ) { // at least one task is waiting to push: awake it
#if TTC_QUEUE_USE_WAKEUP_INTERLEAVE == 1
                if ( Queue->Counter_WakeUpPusher == 0 ) {
                    Queue->Counter_WakeUpPusher = Queue->Interleave_WakeUpPusher;
                    ttc_task_waitinglist_awake( &( Queue->TasksWaiting2Push ) );
                }
                else
                { Queue->Counter_WakeUpPusher--; }
#else
                ttc_task_waitinglist_awake( &( Queue->TasksWaiting2Push ) );
#endif
#ifdef REGRESSION_PIN_QUEUE_PULL
                regression_queue_setPin_QueuePull( 0 );
#endif
#if TTC_TASK_SCHEDULER_AVAILABLE==1
                ttc_task_critical_end();
#endif
            }
        }
        else {     // Queue empty: append ourself to list of waiting tasks
            ttc_mutex_unlock( &( Queue->Lock ) );
#ifdef TTC_REGRESSION
            Queue->AmountTaskWaiting2Pull++;
#endif
#ifdef REGRESSION_PIN_QUEUE_PULL
            regression_queue_setPin_QueuePull( 0 );
#endif
            ttc_task_waitinglist_wait( &( Queue->TasksWaiting2Pull ), -1 ); // will end critical section
        }
    }
    while ( Error != tqe_OK );

    return Data;
}
e_ttc_queue_error     ttc_queue_byte_pull_front_isr( t_ttc_queue_bytes* Queue, t_u8* Data ) {
    // variables may be static because we don't expect concurrency inside ISR
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );
    Assert_QUEUES_Writable( Data, ttc_assert_origin_auto );

    if ( Queue->FirstUsed != Queue->FirstUnused ) { // queue is not empty
        if ( ! ttc_mutex_is_locked( &( Queue->Lock ) ) ) { // queue is not locked:
            if ( Queue->AmountPendingPushes )
            { _ttc_queue_byte_process_pending( Queue ); }
            ttc_queue_Error = _ttc_queue_byte_pull_front( Queue, Data );
        }
        else { // queue is currently locked by a task
            ttc_queue_Error = tqe_QueueIsLocked;
        }
        return ttc_queue_Error;
    }
    return tqe_QueueIsEmpty;
}
e_ttc_queue_error     ttc_queue_byte_pull_front_try( t_ttc_queue_bytes* Queue, t_u8* Data ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );
    Assert_QUEUES_Writable( Data, ttc_assert_origin_auto );

    ttc_mutex_lock_endless( &( Queue->Lock ) );
    e_ttc_queue_error Error = _ttc_queue_byte_pull_front( Queue, Data );
    ttc_mutex_unlock( &( Queue->Lock ) );

    return Error;
}

// simple pointer queue (fast + low memory usage + independent from scheduler)
e_ttc_queue_error    _ttc_queue_pointer_push_back_try( t_ttc_queue_pointers* Queue, void* Data ) {

    t_base NewEnd = Queue->FirstUnused + 1;
    if ( NewEnd >= Queue->Size )
    { NewEnd = 0; }

    if ( Queue->FirstUsed == NewEnd ) {
        return tqe_QueuePushFailed; // queue is full
    }

    Queue->Data[Queue->FirstUnused] = Data; // storing at old end index
    Queue->FirstUnused = NewEnd;

    return tqe_OK;
}
void                 _ttc_queue_pointer_process_pending( t_ttc_queue_pointers* Queue ) {

    // function gets called from interrupt service routine only: no need to waist stack memory
    static void** tqppf_Data;
    static void** tqppf_Writer;

    tqppf_Data = Queue->PendingPush;

    while ( Queue->AmountPendingPushes ) { // process pending pushes
        if ( _ttc_queue_pointer_push_back_try( Queue, *tqppf_Data++ ) > tqe_Error ) { // cannot process all pending: cleanup
            ttc_queue_AmountRemaining = Queue->AmountPendingPushes;
            tqppf_Writer = Queue->PendingPush;
            tqppf_Data--; // back to last pending Data
            while ( ttc_queue_AmountRemaining-- ) {
                *tqppf_Writer++ = *tqppf_Data++;
            }
            break;
        }
        Queue->AmountPendingPushes--;
    }
}
e_ttc_queue_error    _ttc_queue_pointer_pull_front( t_ttc_queue_pointers* Queue, void** Data ) {
    t_base Start = Queue->FirstUsed;
    Assert_QUEUES( Start < Queue->Size, ttc_assert_origin_auto );

    if ( Start == Queue->FirstUnused ) {    // queue is empty
        if ( Queue->AmountPendingPushes ) { // entries are waiting to be pushed to queue: process them to user pending items
            ttc_interrupt_critical_begin();
            _ttc_queue_pointer_process_pending( Queue );
            Start = Queue->FirstUsed;
            ttc_interrupt_critical_end();
            Assert_QUEUES( Start != Queue->FirstUnused, ttc_assert_origin_auto ); // something went wrong while processing pending pushes. Check queue implementation!
        }
        else {
            return tqe_QueueIsEmpty;
        }
    }

    void* Pointer = Queue->Data[Start++];
    *Data = Pointer;

    if ( Start == Queue->Size )
    { Start = 0; }                 // wrapping around at end of array
    Queue->FirstUsed = Start;

    return tqe_OK;
}

t_ttc_queue_pointers* ttc_queue_pointer_create( t_u16 AmountOfEntries ) {
    Assert_QUEUES( AmountOfEntries, ttc_assert_origin_auto );  // need at least size 1

    // need to allocate one additional entry for queue-management
    AmountOfEntries++;

    t_ttc_queue_pointers* Queue = ttc_heap_alloc( AmountOfEntries * sizeof( void* ) + sizeof( t_ttc_queue_pointers ) );
    ttc_queue_pointer_init( Queue, ( void** )( Queue + 1 ), AmountOfEntries );

    return Queue;
}
void                  ttc_queue_pointer_init( t_ttc_queue_pointers* Queue, void** Storage, t_u16 AmountOfEntries ) {
    Assert_QUEUES( AmountOfEntries, ttc_assert_origin_auto );  // need at least size 1

    ttc_memory_set( Queue, 0, sizeof( *Queue ) );
    Queue->Size  = AmountOfEntries;
    ttc_mutex_init( &( Queue->Lock ) );
    Queue->Data  = Storage;
}
t_base                ttc_queue_pointer_get_amount( t_ttc_queue_pointers* Queue ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );

    t_base_signed Amount = Queue->FirstUnused - Queue->FirstUsed;
    if ( Amount < 0 )
    { Amount += Queue->Size; }

#ifdef TTC_REGRESSION
    if ( Queue->FirstUnused < Queue->FirstUsed ) {
        Assert_QUEUES( Amount == Queue->FirstUnused + Queue->Size - Queue->FirstUsed, ttc_assert_origin_auto );
    }
    else {
        Assert_QUEUES( Amount == Queue->FirstUnused - Queue->FirstUsed, ttc_assert_origin_auto );
    }
#endif

    return Amount + Queue->AmountPendingPushes;
}
void                  ttc_queue_pointer_push_back( t_ttc_queue_pointers* Queue, void* Data ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );
    e_ttc_queue_error Error = tqe_UNKNOWN;

    ttc_mutex_lock_endless( &( Queue->Lock ) );
#if TTC_TASK_SCHEDULER_AVAILABLE==1
    ttc_task_critical_begin();
#endif
    do {
        if ( Queue->AmountPendingPushes ) { // data from isr is pending to be pushed: complete push
            ttc_interrupt_all_disable();
            _ttc_queue_pointer_process_pending( Queue );
            ttc_interrupt_all_enable();
        }
        Error = _ttc_queue_pointer_push_back_try( Queue, Data );
        if ( Error != tqe_OK ) {  // Queue full: append ourself to list of waiting tasks
#ifdef TTC_REGRESSION
            Queue->AmountTaskWaiting2Push++;
#endif
            ttc_mutex_unlock( &( Queue->Lock ) );
            if ( Queue->TasksWaiting2Pull.First ) { // at least one task is waiting to pull: awake it
#if TTC_QUEUE_USE_WAKEUP_INTERLEAVE == 1
                if ( Queue->Counter_WakeUpPuller == 0 ) {
                    Queue->Counter_WakeUpPuller = Queue->Interleave_WakeUpPuller;
                    ttc_task_waitinglist_awake( &( Queue->TasksWaiting2Pull ) );
                }
                else
                { Queue->Counter_WakeUpPuller--; }
#else
                ttc_task_waitinglist_awake( &( Queue->TasksWaiting2Pull ) );
#endif
            }
#ifdef REGRESSION_PIN_QUEUE_PUSH
            regression_queue_setPin_QueuePush( 0 );
#endif
            ttc_task_waitinglist_wait( &( Queue->TasksWaiting2Push ), -1 ); // will end + restore critical section
            Error = tqe_QueuePushFailed;
            if ( !ttc_mutex_lock_try( &( Queue->Lock ) ) ) { // could not obtain lock directly: wait for it
#if TTC_TASK_SCHEDULER_AVAILABLE==1
                ttc_task_critical_end();
#endif
                ttc_mutex_lock_endless( &( Queue->Lock ) );
#if TTC_TASK_SCHEDULER_AVAILABLE==1
                ttc_task_critical_begin();
#endif
            }
        }
        else {                    // push successfull
            ttc_mutex_unlock( &( Queue->Lock ) ); // pointer pushed: free lock
            if ( Queue->TasksWaiting2Pull.First ) { // at least one task is waiting to pull: awake it
                ttc_task_waitinglist_awake( &( Queue->TasksWaiting2Pull ) ); // ToDo: implement delayed wake up
            }
#ifdef REGRESSION_PIN_QUEUE_PUSH
            regression_queue_setPin_QueuePush( 0 );
#endif
        }
    }
    while ( Error != tqe_OK );
#if TTC_TASK_SCHEDULER_AVAILABLE==1
    ttc_task_critical_end();
#endif
}
e_ttc_queue_error     ttc_queue_pointer_push_back_isr( t_ttc_queue_pointers* Queue, void* Data ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );

    if ( ! ttc_mutex_is_locked( &( Queue->Lock ) ) ) {
        if ( Queue->AmountPendingPushes )
        { _ttc_queue_pointer_process_pending( Queue ); }
        ttc_queue_Error = _ttc_queue_pointer_push_back_try( Queue, Data );
    }
    else { // queue is currently locked by a task: store pending pointer in extra isr-storage
        if ( Queue->AmountPendingPushes < TTC_SQB_ISR_SPACE ) {
            Queue->PendingPush[Queue->AmountPendingPushes++] = Data;
            ttc_queue_Error = tqe_IsrOperationPending;
        }
        else // no more space left for pending pushes
        { ttc_queue_Error = tqe_QueueIsLocked; }
#ifdef TTC_REGRESSION
        Queue->AmountPendingTotal++;
#endif
    }
    if ( Queue->TasksWaiting2Pull.First ) { // at least one task is waiting to pull: awake it
        ttc_task_waitinglist_awake_isr( &( Queue->TasksWaiting2Pull ) );
    }

    return ttc_queue_Error;
}
void*                 ttc_queue_pointer_pull_front( t_ttc_queue_pointers* Queue ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );

    ttc_mutex_lock_endless( &( Queue->Lock ) );
#ifdef TTC_REGRESSION
    Queue->AmountTaskWaiting2Pull++;
#endif
    void* Data;
    e_ttc_queue_error Error;

    do {
#if TTC_TASK_SCHEDULER_AVAILABLE==1
        if ( ! ttc_task_is_inside_critical_section() ) { // make sure that we are inside critical section
            ttc_task_critical_begin();
        }
#endif
        if ( Queue->AmountPendingPushes )                              { // data from isr is pending to be pushed: complete push
            ttc_interrupt_all_disable();
            _ttc_queue_pointer_process_pending( Queue );
            ttc_interrupt_all_enable();
        }
        Error = _ttc_queue_pointer_pull_front( Queue, &Data );
        if ( Error == tqe_OK ) {
            ttc_mutex_unlock( &( Queue->Lock ) ); // received pointer: free lock
            if ( Queue->TasksWaiting2Push.First ) { // at least one task is waiting to push: awake it
#if TTC_QUEUE_USE_WAKEUP_INTERLEAVE == 1
                if ( Queue->Counter_WakeUpPusher == 0 ) {
                    Queue->Counter_WakeUpPusher = Queue->Interleave_WakeUpPusher;
                    ttc_task_waitinglist_awake( &( Queue->TasksWaiting2Push ) );
                }
                else
                { Queue->Counter_WakeUpPusher--; }
#else
                ttc_task_waitinglist_awake( &( Queue->TasksWaiting2Push ) );
#endif
#ifdef REGRESSION_PIN_QUEUE_PULL
                regression_queue_setPin_QueuePull( 0 );
#endif
#if TTC_TASK_SCHEDULER_AVAILABLE==1
                ttc_task_critical_end();
#endif
            }
        }
        else {     // Queue empty: append ourself to list of waiting tasks
            ttc_mutex_unlock( &( Queue->Lock ) );
#ifdef TTC_REGRESSION
            Queue->AmountTaskWaiting2Pull++;
#endif
#ifdef REGRESSION_PIN_QUEUE_PULL
            regression_queue_setPin_QueuePull( 0 );
#endif
            ttc_task_waitinglist_wait( &( Queue->TasksWaiting2Pull ), -1 ); // will end critical section
        }
    }
    while ( Error != tqe_OK );

    return Data;
}
e_ttc_queue_error     ttc_queue_pointer_pull_front_isr( t_ttc_queue_pointers* Queue, void** Data ) {
    // variables may be static because we don't expect concurrency inside ISR
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );
    Assert_QUEUES_Writable( Data, ttc_assert_origin_auto );

    if ( Queue->FirstUsed != Queue->FirstUnused ) { // queue is not empty
        if ( ! ttc_mutex_is_locked( &( Queue->Lock ) ) ) { // queue is not locked:
            if ( Queue->AmountPendingPushes )
            { _ttc_queue_pointer_process_pending( Queue ); }
            ttc_queue_Error = _ttc_queue_pointer_pull_front( Queue, Data );
        }
        else { // queue is currently locked by a task
            ttc_queue_Error = tqe_QueueIsLocked;
        }
        return ttc_queue_Error;
    }
    return tqe_QueueIsEmpty;
}
e_ttc_queue_error     ttc_queue_pointer_pull_front_try( t_ttc_queue_pointers* Queue, void** Data ) {
    Assert_QUEUES_Writable( Queue, ttc_assert_origin_auto );
    Assert_QUEUES_Writable( Data, ttc_assert_origin_auto );

    ttc_mutex_lock_endless( &( Queue->Lock ) );
    e_ttc_queue_error Error = _ttc_queue_pointer_pull_front( Queue, Data );
    ttc_mutex_unlock( &( Queue->Lock ) );

    return Error;
}

