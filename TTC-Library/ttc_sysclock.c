/** { ttc_sysclock.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for sysclock devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 23 at 20140218 17:52:55 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_sysclock.h"
#include "ttc_heap.h"
#include "ttc_list.h"
#include "ttc_cpu_types.h"

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of sysclock devices.
 *
 * See corresponding header file for documentation.
 */

t_u8                  ttc_sysclock_Prepared = 0; // =0: not prepared; =1: ttc_sysclock_prepare() has finished; =2: ttc_sysclock_UpdateList has been initialized
t_ttc_sysclock_config ttc_sysclock_Config;
t_ttc_list            ttc_sysclock_UpdateList;

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)
/** Will call all update functions stored in list ttc_sysclock_UpdateList
 */
void _ttc_sysclock_call_update_functions();

/** Calls given update function
 *
 * Note: This function is called automatically from within _ttc_sysclock_call_update_functions(). Do not use it from outside!
 *
 * @param  Item  (t_ttc_list_item)  one item from ttc_sysclock_UpdateList storing a pointer to update function to be called
 * @return ==NULL: always returns NULL
 */
void* _ttc_sysclock_call_update_function( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item );

//} Global Variables
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_sysclock(t_u8 LogicalIndex)

/** generic linear calculated delay scale for current system clock frequency
 *
 * @return ttc_sysclock_Config.DelayScale is updated accordingly
 */
void _ttc_sysclock_calculate_delay_scale();
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

t_ttc_sysclock_config* ttc_sysclock_get_configuration() {
    Assert_SYSCLOCK( ttc_sysclock_Config.Flags.Bits.Initialized, ttc_assert_origin_auto );  // did you call ttc_sysclock_prepare() before?

    return &ttc_sysclock_Config;
}
t_u32                  ttc_sysclock_frequency_get() {
    Assert_SYSCLOCK( ttc_sysclock_Config.Flags.Bits.Initialized, ttc_assert_origin_auto );  // did you call ttc_sysclock_prepare() before?

    return ttc_sysclock_Config.SystemClockFrequency;
}
const t_base*          ttc_sysclock_frequency_get_all() {

    Assert_SYSCLOCK( ttc_sysclock_Config.Flags.Bits.Initialized, ttc_assert_origin_auto );  // did you call ttc_sysclock_prepare() before?

#ifdef TTC_SYSCLOCK_AVAILABLE_FREQUENCIES
    // someone has defined a static list of available frequencies: return it instead of low-level driver
    return TTC_SYSCLOCK_AVAILABLE_FREQUENCIES;
#else
    return _driver_sysclock_frequency_get_all( &ttc_sysclock_Config );
#endif
}
void                   ttc_sysclock_frequency_set( t_base NewFrequency ) {

    _driver_sysclock_frequency_set( &ttc_sysclock_Config, NewFrequency );
}
t_base                 ttc_sysclock_get_delay_scale() {
    return ttc_sysclock_Config.DelayScale;
}
void                   ttc_sysclock_prepare() {
    if ( ttc_sysclock_Prepared )
    { return; }  // already prepared

    Assert_SYSCLOCK( ( ttc_task_is_scheduler_started() == 0 ), ttc_assert_origin_auto ); // must be run befor starting scheduler!
    //X Assert_SYSCLOCK( ( ( ( t_base ) &ttc_sysclock_Config ) >= &_sbss ) && ( ( ( t_base ) &ttc_sysclock_Config ) <= &_ebss ), ttc_assert_origin_auto ); // variable must lie within section of global variables being initialized as zero!

    // add your startup code here (Singletasking!)
    ttc_sysclock_Config.Flags.Bits.Initialized = 1;
    ttc_sysclock_reset();
    _driver_sysclock_update( &ttc_sysclock_Config );
    _ttc_sysclock_calculate_delay_scale();

    _driver_sysclock_prepare( &ttc_sysclock_Config );

#if (TTC_ASSERT_SYSCLOCK == 1)   // check if we have a correct defined list of available frequencies
    const t_base* AllFrequencies = ttc_sysclock_frequency_get_all();
    Assert_SYSCLOCK_Readable( AllFrequencies, ttc_assert_origin_auto );
    t_u8 AmountFrequencies = 0;

    const t_base* Frequency = AllFrequencies;
    while ( *Frequency && ( AmountFrequencies < 255 ) ) {
        t_base PreviousFrequency = *Frequency;
        AmountFrequencies++;
        Frequency++;
        if ( *Frequency ) {
            Assert_SYSCLOCK( PreviousFrequency < *Frequency, ttc_assert_origin_auto );  // array entries must increase until zero terminator. (Reorder entries or add missing zero-terminator!)
        }
    }
    Assert_SYSCLOCK( AmountFrequencies < 255, ttc_assert_origin_auto );  // Do you really want to register so many frequency levels or did you just forgot the zero terminator?
#endif

    ttc_sysclock_profile_switch( TTC_SYSCLOCK_PROFILE_START );

    ttc_sysclock_Prepared = 1;
}
void                   ttc_sysclock_profile_switch( e_ttc_sysclock_profile NewProfile ) {
    ttc_task_critical_begin();

    t_ttc_sysclock_config* Config = ttc_sysclock_get_configuration();

    if ( NewProfile != Config->Profile ) {
#if (TTC_ASSERT_SYSCLOCK == 1)
        e_ttc_sysclock_profile OriginalProfile = Config->Profile;
        t_base OriginalFrequency  = Config->SystemClockFrequency;
#endif
        _driver_sysclock_profile_switch( Config, NewProfile );
        ttc_sysclock_update();

#if (TTC_ASSERT_SYSCLOCK == 1)
        BOOL OK = ( OriginalProfile != Config->Profile )  ||
                  ( !OriginalFrequency ||
                    ( OriginalFrequency == Config->SystemClockFrequency )
                  );
        Assert_SYSCLOCK( OK, ttc_assert_origin_auto );  // Frequency changed but profile did not. Check implementation of _driver_sysclock_profile_switch()!
#endif
    }
    ttc_task_critical_end();
}
void                   ttc_sysclock_register_for_update( void( *functionUpdateToSysclock )() ) {
    Assert_SYSCLOCK( functionUpdateToSysclock != NULL, ttc_assert_origin_auto );
    Assert_SYSCLOCK( ttc_sysclock_Prepared, ttc_assert_origin_auto );  // must not call this function before ttc_sysclock has been prepared successfully

    t_sysclock_update_item* NewItem = ttc_heap_alloc_zeroed( sizeof( t_sysclock_update_item ) );
    NewItem->UpdateFunction = functionUpdateToSysclock;

    if ( ttc_sysclock_Prepared < 2 ) { // first call: initialize list
        // using address of this function as list name (less FLASH usage than using a string as list name)
        ttc_list_init( &ttc_sysclock_UpdateList, ( const char* ) &ttc_sysclock_prepare );
        ttc_sysclock_Prepared = 2;
    }
    ttc_list_push_back_single( & ttc_sysclock_UpdateList, ( t_ttc_list_item* ) & ( NewItem->ListItem ) );
}
void                   ttc_sysclock_reset() {
    ttc_task_critical_begin();

    if ( !ttc_sysclock_Config.Flags.Bits.Initialized )
    { ttc_sysclock_get_configuration(); }

    ttc_memory_set( &ttc_sysclock_Config, 0, sizeof( ttc_sysclock_Config ) );
    ttc_sysclock_Config.Flags.Bits.Initialized = 1;

    _driver_sysclock_reset( &ttc_sysclock_Config );

    ttc_task_critical_end();
}
void                   ttc_sysclock_udelay( t_base Microseconds ) {
    _driver_sysclock_udelay( &ttc_sysclock_Config, Microseconds );
}
void                   ttc_sysclock_update() {
    t_base OriginalFrequency  = ttc_sysclock_Config.SystemClockFrequency;
    _driver_sysclock_update( &ttc_sysclock_Config );

    if ( OriginalFrequency != ttc_sysclock_Config.SystemClockFrequency ) {
        _ttc_sysclock_calculate_delay_scale();

        if ( ttc_sysclock_Prepared ) // will not call update functions before ttc_syclock has been prepared
        { _ttc_sysclock_call_update_functions(); }
    }
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_sysclock(t_u8 LogicalIndex) {  }

void _ttc_sysclock_calculate_delay_scale() {

    ttc_sysclock_Config.DelayScale = ( 1
                                       * ( ttc_sysclock_Config.SystemClockFrequency >> 10 ) // make calculation fit into 32 bit integer variable
                                       * TTC_SYSCLOCK_SCALE_UNIT
                                       / ( TTC_SYSCLOCK_FREQUENCY_MAX >> 10 )                // make calculation fit into 32 bit integer variable
                                     );
}
void _ttc_sysclock_call_update_functions() {

    if ( ttc_sysclock_Prepared >= 2 )
    { ttc_list_iterate( &ttc_sysclock_UpdateList, _ttc_sysclock_call_update_function, NULL, 1, 0, -1 ); }
}
void* _ttc_sysclock_call_update_function( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item ) {
    ( void ) Argument;
    ( void ) PreviousItem;

    Assert_SYSCLOCK_Writable( Item, ttc_assert_origin_auto );  // Item should point to RAM: check your implementation (maybe memory corruption?)

    // extract function pointer from generic list item
    t_sysclock_update_item* UpdateItem = ( t_sysclock_update_item* ) Item;
    void( *functionUpdateToSysclock )() = UpdateItem->UpdateFunction;

    // call registered function to update to new system clock profile
    Assert_SYSCLOCK( ttc_memory_is_constant( functionUpdateToSysclock ), ttc_assert_origin_auto );  // given function pointer does not point to FLASH memory!
    functionUpdateToSysclock();

    return NULL; // not removing anything from list
}

//{InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
