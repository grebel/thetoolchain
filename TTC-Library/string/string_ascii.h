#ifndef STRING_ASCII_H
#define STRING_ASCII_H

/** { string_ascii.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for string devices on ascii architectures.
 *  Structures, Enums and Defines being required by low-level driver only.
 *
 *  Created from template device_architecture.h revision 25 at 20151120 13:35:51 UTC
 *
 *  Note: See ttc_string.h for description of ascii independent STRING implementation.
 *
 *  Authors: Gregor Rebel
 *
 *
 *  How to configure ascii string devices
 *
 *  This low-level driver requires configuration defines in addtion to the howto section
 *  of ttc_string.h.
 *
 *  String conversion implementation for ASCII type strings.
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_STRING_ASCII
//
// Implementation status of low-level driver support for string devices on ascii
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_STRING_ASCII '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_STRING_ASCII == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_STRING_ASCII to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_STRING_ASCII

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "string_ascii.c"
//
#include "../ttc_string_types.h" // will include string_ascii_types.h (do not include it directly!)
#include "stdarg.h"  // required for va_list
#include "../ttc_math_types.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_string_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_string_foo
//
#undef ttc_driver_string_prepare
#undef ttc_driver_string_appendf
#undef ttc_driver_string_copy
#undef ttc_driver_string_is_whitechar
#undef ttc_driver_string_length16
#undef ttc_driver_string_length32
#undef ttc_driver_string_length8
#undef ttc_driver_string_log_decimal
#undef ttc_driver_string_log_hex
#undef ttc_driver_string_print
#undef ttc_driver_string_print_block
#undef ttc_driver_string_printf
#undef ttc_driver_string_snprintf
#undef ttc_driver_string_strip
#undef ttc_driver_string_byte2hex
#undef ttc_driver_string_byte2int
#undef ttc_driver_string_byte2uint
#undef ttc_driver_string_itoa
#undef ttc_driver_string_itoa_unsigned
#undef ttc_driver_string_xtoa
#undef ttc_driver_string_compare
#undef ttc_driver_string_snprintf4
#undef ttc_driver_string_log_binary
#undef ttc_driver_string_btoa
#undef ttc_driver_string_ftoa

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_string.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_string.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */



/** writes ascii representation of given floating point value into given buffer
 *
 * Note: The precision currently used is defined by ttc_math.
 *
 * @param   Value   floating point value of single or duble precision to convert
 * @param   Buffer  string buffer in which to write ascii representation
 * @param   MaxSize no more than this amount of bytes will be written into Buffer[]
 * @return          amount of bytes written to Buffer[]
 */
t_u8 string_ascii_ftoa( ttm_number Value, t_u8* Buffer, t_base MaxSize );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //STRING_ASCII_H
