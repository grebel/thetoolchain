#ifndef STRING_ASCII_TYPES_H
#define STRING_ASCII_TYPES_H

/** { string_ascii.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for STRING devices on ascii architectures.
 *  Structures, Enums and Defines being required by ttc_string_types.h
 *
 *  Created from template device_architecture_types.h revision 22 at 20151120 13:35:51 UTC
 *
 *  Note: See ttc_string.h for description of high-level string implementation.
 *  Note: See string_ascii.h for description of ascii string implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_string_types.h *************************

typedef struct { // register description (adapt according to ascii registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_string_register;

typedef struct {  // ascii specific configuration data of single string device
    t_string_register* BaseRegister;       // base address of string device registers
} __attribute__( ( __packed__ ) ) t_string_ascii_config;

// t_ttc_string_architecture is required by ttc_string_types.h
#define t_ttc_string_architecture t_string_ascii_config

//} Structures/ Enums


#endif //STRING_ASCII_TYPES_H
