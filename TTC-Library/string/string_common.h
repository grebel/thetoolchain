#ifndef string_common_h
#define string_common_h

/** string_common.h *****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to string low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.h revision 12 at 20151120 13:30:25 UTC
 *
 *  Authors: Gregor Rebel
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_STRING_COMMON
//
// Implementation status of low-level driver support for string devices on common
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_STRING_COMMON '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_STRING_COMMON == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_STRING_COMMON to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_STRING_COMMON

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines

/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "string_generic.c"
 */
#include "../ttc_basic.h"
#include "../ttc_string_types.h" // will include string_generic_types.h (do not include it directly!)
#include "../ttc_memory_types.h"
#include "../ttc_heap_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
// central function providing STDOUT like output of memory blocks
// Note: do not call this function directly, use ttc_string_block()/string_common_lock() instead!
// Note: do not write to this variable directly, use ttc_string_register_stdout() instead!
//
extern void ( *_string_common_stdout_block )( t_ttc_heap_block* Block );

// central function providing STDOUT like output of strings
// Note: do not call this function directly, use
//       ttc_string_print()/ ttc_string_printf() instead!
// Note: do not write to this variable directly, use ttc_string_register_stdout() instead!
//
extern void ( *_string_common_stdout )( const t_u8* String, t_base MaxSize );


/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_string_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_string_foo
 */

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions
/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_string.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_string.h for details.
 *
 * You normally should not need to add functions here.
 * Global feature functions must be added in ttc_string.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl string UPDATE".
 */


/** delivers pointer to temporary string buffer used for printf() like functions
 *
 * Note: String buffer is allocated on first call
 *
 * @return         pointer to first character in Buffer[] after written string
 */
t_u8* string_common_get_buffer();

/** obtain access to global static datafields used by string_common.c
  *
 * Note: Mutex is allocated and initialized on first call
 *
 */
#ifdef TTC_MULTITASKING_SCHEDULER
    void string_common_lock();
#else
    #define string_common_lock() // macro is a noop if no multitasking is availabe
#endif

/** Registers given function pointers for stdout output via ttc_string_print_block()/ ttc_string_print()/ ttc_string_printf().
 * @param StdoutBlock  function that will send out content of given memory block
 * @param StdoutString function that will send out content of given string buffer
 */
void string_common_register_stdout( void ( *StdoutBlock )( t_ttc_heap_block* ), void ( *StdoutString )( const t_u8*, t_base ) );

/** Release access to global static datafields provided by string_common.c
 *
 */
void string_common_unlock();
#ifdef TTC_MULTITASKING_SCHEDULER
    void string_common_unlock();
#else
    #define string_common_unlock() // macro is a noop if no multitasking is availabe
#endif

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //string_common_h