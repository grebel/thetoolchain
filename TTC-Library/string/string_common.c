/** string_common.c ***********************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to string low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.c revision 12 at 20151120 13:30:25 UTC
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * Put all includes here that are required to compile this source code.
 */

#include "string_common.h"
#include "../ttc_heap.h"
#include "../ttc_mutex.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

// central function providing STDOUT like output of memory blocks
// Note: do not call this function directly, use ttc_string_block()/string_common_lock() instead!
// Note: do not write to this variable directly, use ttc_string_register_stdout() instead!
//
void ( *_string_common_stdout_block )( t_ttc_heap_block* Block ) = NULL;

// central function providing STDOUT like output of strings
// Note: do not call this function directly, use
//       ttc_string_print()/ ttc_string_printf() instead!
// Note: do not write to this variable directly, use ttc_string_register_stdout() instead!
//
void ( *_string_common_stdout )( const t_u8* String, t_base MaxSize ) = NULL;

// buffer for temporary string (allocated on first use)
// _string_common_Buffer[TTC_STRING_PRINTF_BUFFERSIZE]
static t_u8* _string_common_Buffer = NULL;

// points to last valid field of ttc_string_Buffer[]
static t_u8* _string_common_BufferEnd = NULL;

#ifdef TTC_MULTITASKING_SCHEDULER
    // Protect single _string_common_Buffer[] from concurrent calls
    static t_ttc_mutex_smart* _string_common_Lock = NULL;
#endif

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)
//}PrivateFunctions
/** Function Definitions ********************************************{
 */

t_u8* string_common_get_buffer() {
    if ( _string_common_Buffer == NULL ) { // first call: allocate buffer
        _string_common_Buffer = ttc_heap_alloc( TTC_STRING_PRINTF_BUFFERSIZE );
        Assert_STRING_Writable( _string_common_Buffer, ttc_assert_origin_auto );

        // reserve 1 byte for string terminator
        _string_common_BufferEnd = _string_common_Buffer + TTC_STRING_PRINTF_BUFFERSIZE - 2;
    }

    return _string_common_Buffer;
}
void string_common_register_stdout( void ( *StdoutBlock )( t_ttc_heap_block* ), void ( *StdoutString )( const t_u8*, t_base ) ) {
    _string_common_stdout_block = StdoutBlock;
    _string_common_stdout       = StdoutString;
}

#ifdef TTC_MULTITASKING_SCHEDULER
void string_common_lock() {
    if ( _string_common_Lock == NULL ) { // first call: allocate mutex
        _string_common_Lock = ttc_mutex_create();
    }
    while ( ttc_mutex_lock( _string_common_Lock, -1 ) != tme_OK );
}
void string_common_unlock() {
    Assert_STRING_Writable( _string_common_Lock, ec_basic_InvalidImplementation ); // calling unlock before ever calling lock() !!!

    ttc_mutex_unlock( _string_common_Lock );
}
#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
