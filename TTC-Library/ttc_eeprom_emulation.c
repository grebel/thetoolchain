
#include "ttc_eeprom_emulation.h"
//{ Global variables *****************************************************



//// stores released memory blocks for reuse as transmit or receive blocks
//t_ttc_queue_pointers* ttc_usb_queue_empty_blocks = NULL;   // stores empty memory blocks for reuse

//// protects queues from simultaneous access
//t_ttc_mutex*            ttc_usb_mutex_empty_blocks = NULL;

//}Global variables
//{ Function definitions *************************************************

void ttc_eeprom_emulation_init(void){

#ifdef _driver_ttc_eeprom_emulation_init
  _driver_ttc_eeprom_emulation_init();
#else
#  warning Missing low-level implementation _driver_ttc_eeprom_emulation_init()
#endif

}

//} private functions

