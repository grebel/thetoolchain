/** { ttc_usart.c **********************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for usart devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 23 at 20140217 09:36:59 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_usart.h"
#include "ttc_queue.h"     // efficient queues for individual bytes and pointers
#include "ttc_heap.h"      // dynamic memory and safe arrays

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of usart devices.
 *
 */

#if TTC_USART_AMOUNT == 0
    #warning No USART devices defined, check your makefile! (did you forget to activate something?)
#endif

// for each initialized device, a pointer to its generic and stm32f1xx definitions is stored
A_define( t_ttc_usart_config*, ttc_usart_configs, TTC_USART_MAX_AMOUNT );

t_u8 ttc_usart_Amount_BlocksAllocated;    // amount of transmit/receive buffers allocated by ttc_usart_get_tx_block() so far
t_u8 ttc_usart_Amount_RxBytesSkipped;     // amount of bytes received by _ttc_usart_rx_isr() which could not be pushed into receive queue

// logical index of USART to use by ttc_usart_stdout_send_string()/ ttc_usart_stdout_send_block()
t_u8   ttc_usart_stdout_index = 0;
t_base ttc_usart_Timeout      = 200;     // Copy of Config->Init.TimeOut (some low-level drivers use it instead for faster operations)

// stores released memory blocks for reuse as transmit or receive blocks
// Note: Pushes to this queue are allowed from outside interrupt service routine only!
t_ttc_queue_pointers ttc_usart_QueueEmptyBlocks;
void* ttc_usart_QueueEmptyBlocks_Data[TTC_USART_MAX_MEMORY_BUFFERS + 1];

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
/** { Private Function Declarations ***********************************
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//}PrivateFunctions
//{ Function definitions ***************************************************

t_u8                  ttc_usart_check_bytes_available( t_u8 LogicalIndex ) {
    t_ttc_usart_config* Config = _ttc_usart_get_configuration( LogicalIndex );

    return _driver_usart_check_bytes_available( Config );
}
t_u8                  ttc_usart_get_max_index() {
    return TTC_USART_AMOUNT;
}
void                  ttc_usart_deinit( t_u8 LogicalIndex ) {
    Assert_USART( LogicalIndex, ttc_assert_origin_auto );  // logical index starts at 1
    t_ttc_usart_config* Config = ttc_usart_get_configuration( LogicalIndex );

    e_ttc_usart_errorcode Result = _driver_usart_deinit( Config );
    if ( Result == ec_usart_OK )
    { Config->Flags.Initialized = 0; }
}
void                  ttc_usart_flush_tx( t_u8 LogicalIndex ) {
    t_ttc_usart_config* Config = _ttc_usart_get_configuration( LogicalIndex );
    if ( Config->Init.Flags.DelayedTransmits ) { // delayed transmits have been activated for this USART

        // wait for transmit queue to run empty
        while ( ttc_queue_pointer_get_amount( Config->Queue_Tx ) > 0 )
        { ttc_task_yield(); }
    }
}
t_ttc_usart_config*   ttc_usart_get_configuration( t_u8 LogicalIndex ) {
    Assert_USART( LogicalIndex > 0, ttc_assert_origin_auto );                // logical index starts at 1
    Assert_USART( LogicalIndex <= TTC_USART_AMOUNT, ttc_assert_origin_auto );  // use ttc_i2c_get_max_index() to get max allowed value
    t_ttc_usart_config* Config = A( ttc_usart_configs, LogicalIndex - 1 );

    if ( !Config ) {
        Config = A( ttc_usart_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_usart_config ) );
        ttc_usart_reset( LogicalIndex );
        ttc_usart_load_defaults( LogicalIndex );
    }

    return Config;
}
t_u8                  ttc_usart_get_max_logicalindex() {

    return TTC_USART_AMOUNT; // -> ttc_usart_types.h
}
e_ttc_usart_errorcode ttc_usart_get_features( t_u8 LogicalIndex, t_ttc_usart_config* Config ) {
    Assert_USART( LogicalIndex > 0, ttc_assert_origin_auto );              // logical index starts at 1
    Assert_USART( LogicalIndex <= TTC_USART_AMOUNT, ttc_assert_origin_auto );  // use ttc_i2c_get_max_index() to get max allowed value
    Assert_USART( Config != NULL, ttc_assert_origin_auto );

    ttc_memory_set( Config, 0, sizeof( Config ) );
    Config->LogicalIndex = LogicalIndex;
    e_ttc_usart_errorcode Status = _driver_usart_get_features( Config );

    if ( Status == ec_usart_OK ) { // low-level driver returned OK: add platform independent features
        Config->Init.Flags.DelayedTransmits = 1;

        if ( 1 ) { // disable features requiring unconfigured pins
            switch ( LogicalIndex ) {
                case 1: {
#ifndef TTC_USART1_TX
                    Config->Init.Flags.Transmit    = 0;
#endif
#ifndef TTC_USART1_RX
                    Config->Init.Flags.Receive     = 0;
#endif
#ifndef TTC_USART1_RTS
                    Config->Init.Flags.RtsCts      = 0;
                    Config->Init.Flags.Rts         = 0;
#endif
#ifndef TTC_USART1_CTS
                    Config->Init.Flags.RtsCts      = 0;
                    Config->Init.Flags.Cts         = 0;
#endif
#ifndef TTC_USART1_CK
                    Config->Init.Flags.Synchronous = 0;
#endif

                    break;
                }
                case 2: {
#ifndef TTC_USART2_TX
                    Config->Init.Flags.Transmit    = 0;
#endif
#ifndef TTC_USART2_RX
                    Config->Init.Flags.Receive     = 0;
#endif
#ifndef TTC_USART2_RTS
                    Config->Init.Flags.RtsCts      = 0;
                    Config->Init.Flags.Rts         = 0;
#endif
#ifndef TTC_USART2_CTS
                    Config->Init.Flags.RtsCts      = 0;
                    Config->Init.Flags.Cts         = 0;
#endif
#ifndef TTC_USART2_CK
                    Config->Init.Flags.Synchronous = 0;
#endif

                    break;
                }
                case 3: {
#ifndef TTC_USART3_TX
                    Config->Init.Flags.Transmit    = 0;
#endif
#ifndef TTC_USART3_RX
                    Config->Init.Flags.Receive     = 0;
#endif
#ifndef TTC_USART3_RTS
                    Config->Init.Flags.RtsCts      = 0;
                    Config->Init.Flags.Rts         = 0;
#endif
#ifndef TTC_USART3_CTS
                    Config->Init.Flags.RtsCts      = 0;
                    Config->Init.Flags.Cts         = 0;
#endif
#ifndef TTC_USART3_CK
                    Config->Init.Flags.Synchronous = 0;
#endif

                    break;
                }
                case 4: {
#ifndef TTC_USART4_TX
                    Config->Init.Flags.Transmit    = 0;
#endif
#ifndef TTC_USART4_RX
                    Config->Init.Flags.Receive     = 0;
#endif

                    // UART4, UART5 only provide TX+RX pins
                    Config->Init.Flags.RtsCts      = 0;
                    Config->Init.Flags.Rts         = 0;
                    Config->Init.Flags.Cts         = 0;
                    Config->Init.Flags.Synchronous = 0;

                    break;
                }
                case 5: {
#ifndef TTC_USART5_TX
                    Config->Init.Flags.Transmit    = 0;
#endif
#ifndef TTC_USART5_RX
                    Config->Init.Flags.Receive     = 0;
#endif

                    // UART4, UART5 only provide TX+RX pins
                    Config->Init.Flags.RtsCts      = 0;
                    Config->Init.Flags.Rts         = 0;
                    Config->Init.Flags.Cts         = 0;
                    Config->Init.Flags.Synchronous = 0;

                    break;
                }
                default: return ec_usart_DeviceNotFound;
            }
        }
    }

    return Status;
}
t_ttc_heap_block*     ttc_usart_get_empty_block() {
    Assert_USART( ttc_interrupt_critical_level() == 0, ttc_assert_origin_auto ); // This function may block endlessly if called with disabled interrupts. Change code of caller!
    t_ttc_heap_block* Block = NULL;
    while ( Block == NULL ) {
        ttc_queue_pointer_pull_front_try( &ttc_usart_QueueEmptyBlocks, ( void** ) &Block );
        if ( !Block ) { // no released memory block available: alloc new block
            if ( ttc_usart_Amount_BlocksAllocated < TTC_USART_MAX_MEMORY_BUFFERS ) {
                Block = ttc_heap_alloc_block( TTC_USART_BLOCK_SIZE, 0, _ttc_usart_release_block );
                ttc_usart_Amount_BlocksAllocated++;
            }
            else // no more memory blocks available: wait for next block to be released
            { ttc_queue_pointer_pull_front_try( &ttc_usart_QueueEmptyBlocks, ( void** ) &Block ); }
        }
        if ( Block ) {
            Assert_USART_Writable( Block, ttc_assert_origin_auto );
            Block->Buffer = ( ( t_u8* ) Block ) + sizeof( t_ttc_heap_block ); // correct pointer to buffer in case this block has been used as a virtual block
        }

        if ( Block == NULL ) // got no block: wait for some while
        { ttc_task_yield(); }
    }
    //? ttc_heap_block_use(Block);

    return Block;
}
t_u8                  ttc_usart_get_physicalindex( t_u8 LogicalIndex ) {
    Assert_USART( LogicalIndex > 0, ttc_assert_origin_auto );  // logical USART-indices start at 1 !
    t_s8 PhysicalIndex = -1;

    switch ( LogicalIndex ) {              // find corresponding USART as defined by makefile.100_board_*
#ifdef TTC_USART1
        case 1: PhysicalIndex = TTC_USART1; break; // TTC_USART1 = 0..TTC_USART_AMOUNT-1
#endif
#ifdef TTC_USART2
        case 2: PhysicalIndex = TTC_USART2; break; // TTC_USART2 = 0..TTC_USART_AMOUNT-1
#endif
#ifdef TTC_USART3
        case 3: PhysicalIndex = TTC_USART3; break; // TTC_USART3 = 0..TTC_USART_AMOUNT-1
#endif
#ifdef TTC_USART4
        case 4: PhysicalIndex = TTC_USART4; break; // TTC_USART4 = 0..TTC_USART_AMOUNT-1
#endif
#ifdef TTC_USART5
        case 5: PhysicalIndex = TTC_USART5; break; // TTC_USART5 = 0..TTC_USART_AMOUNT-1
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
    }
    Assert_USART( PhysicalIndex < TTC_USART_MAX_AMOUNT, ttc_assert_origin_auto );  // given logical USART index is out of range
    Assert_USART( PhysicalIndex != -1, ttc_assert_origin_auto );  // error: invalid configuration or no low-level implementation available
    return PhysicalIndex;
}
e_ttc_usart_errorcode ttc_usart_init( t_u8 LogicalIndex ) {

    if ( LogicalIndex > ttc_usart_get_max_logicalindex() )
    { return ec_usart_DeviceNotFound; }
#if (TTC_ASSERT_USART_EXTRA == 1)
    t_u8 InitialCriticalLevel = ttc_interrupt_critical_level();
#endif
    ttc_task_critical_begin();

    t_ttc_usart_config* Config = ttc_usart_get_configuration( LogicalIndex );
    e_ttc_usart_errorcode ReturnCode = ec_usart_NotImplemented;
#ifdef t_ttc_usart_architecture
    ttc_memory_set( &( Config->USART_Arch ), 0, sizeof( t_ttc_usart_architecture ) );
#endif
    Config->LogicalIndex = LogicalIndex;
#ifdef TTC_MULTITASKING_SCHEDULER
    if ( ( Config->Init.receiveBlock != NULL ) || ( Config->Init.receiveSingleByte != NULL ) ) { // blocking of data activated: spawn receive-task
        if ( Config->Queue_Rx == NULL ) { // first initialisation of this USART
            Config->Queue_Rx = ttc_queue_byte_create( Config->Size_Queue_Rx );
            ttc_task_create( _ttc_usart_task_receive, "ttc_usart_rx", 128, ( void* ) Config, 10, &( Config->Task_Rx ) );
            Assert_USART_Writable( Config->Task_Rx, ttc_assert_origin_auto );  // could not spawn receive task (out of memory?)
        }
    }
#endif

    if ( Config->Init.Flags.ParityEven + Config->Init.Flags.ParityOdd == 0 )
    { Config->Init.Flags.ControlParity = 0; } // no parity at all

    // issue low-level initialization
    ttc_usart_Timeout = Config->Timeout; // some low-level drivers may use this global variable as it's faster than reading Config->Timeout
    ReturnCode = _driver_usart_init( Config );

    Assert_USART( ReturnCode == ec_usart_OK, ttc_assert_origin_auto );

    // Low-Level driver stores addresses of USART-Registers for fast
    // Rx/Tx operation (E.g. Config->BaseRegister->DR.All = Data;)
    Assert_USART_Writable( ( void* ) Config->BaseRegister, ttc_assert_origin_auto );  // low-level driver must set this field!

    // low-level initialization successfull: init interrupts

    if ( Config->Init.Flags.IrqOnRxNE &&
            ( ( Config->Init.receiveBlock != NULL ) || ( Config->Init.receiveSingleByte != NULL ) )
       ) { // setup interrupt for tit_USART_RxNE

        Config->Interrupt_RxNE = ttc_interrupt_init( tit_USART_RxNE,
                                                     Config->PhysicalIndex,
                                                     _driver_usart_rx_isr,
                                                     ( void* ) Config,
                                                     NULL,
                                                     NULL
                                                   );
        Assert_USART( Config->Interrupt_RxNE != NULL, ttc_assert_origin_auto );
        ttc_interrupt_enable_usart( Config->Interrupt_RxNE );
    }
    if ( Config->Init.Flags.DelayedTransmits ) { // setup interrupt for tit_USART_TransmitDataEmpty
        if ( Config->Queue_Tx == NULL ) {
            Config->Queue_Tx = ttc_queue_pointer_create( Config->Size_Queue_Tx );
            Assert_USART( Config->Queue_Tx != NULL, ttc_assert_origin_auto );
        }

        // this interrupt will be enabled when there is data to send
        Config->Interrupt_TxComplete = ttc_interrupt_init( tit_USART_TransmitDataEmpty,
                                                           Config->PhysicalIndex,
                                                           _driver_usart_tx_isr,
                                                           ( void* ) Config,
                                                           NULL,
                                                           NULL
                                                         );
        Assert_USART( Config->Interrupt_TxComplete != NULL, ttc_assert_origin_auto );  // interrupt could not be initialized
    }
    //ttc_mutex_init( &(Config->Queue_TX_Lock) );

    A( ttc_usart_configs, LogicalIndex - 1 ) = Config;
    Config->Flags.Initialized = 1;

    // from now on use this USART for default output
    ttc_usart_stdout_set( LogicalIndex );

    /*  ttc_sysclock should now inform us whenever the systemclock changes */
    //ttc_sysclock_register_for_update(_ttc_usart_update_to_sysclock);

    ttc_task_critical_end();
    Assert_USART_EXTRA( InitialCriticalLevel == ttc_interrupt_critical_level(), ttc_assert_origin_auto ); // amount of  ttc_task_critical_end() calls does not match. Check driver implementation!

    return ReturnCode;
}
void                  ttc_usart_load_defaults( t_u8 LogicalIndex ) {
    Assert_USART( LogicalIndex, ttc_assert_origin_auto );  // logical index starts at 1
    t_ttc_usart_config* Config = ttc_usart_get_configuration( LogicalIndex );

    if ( Config->Flags.Initialized )
    { ttc_usart_deinit( LogicalIndex ); }

    ttc_memory_set( Config, 0, sizeof( t_ttc_usart_config ) );

    // load generic default values
    Config->LogicalIndex                = LogicalIndex;
    Config->PhysicalIndex               = ttc_usart_logical_2_physical_index( LogicalIndex );
    Config->Timeout                     = 200;
    Config->Init.Flags.ParityEven       = 1;
    Config->Init.Flags.DelayedTransmits = 1;
    Config->Size_Queue_Rx               = 20;
    Config->Size_Queue_Tx               = 20;

    switch ( LogicalIndex ) { // load static pin configuration

#ifdef TTC_USART1_TX
        case  1: { // check configuration of USART #1
#ifdef TTC_USART1_TX
            Config->PortTxD = TTC_USART1_TX;
#else
            Config->PortTxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART1_RX
            Config->PortRxD = TTC_USART1_RX;
#else
            Config->PortRxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART1_RTS
            Config->PortRTS = TTC_USART1_RTS;
#else
            Config->PortRTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART1_CTS
            Config->PortCTS = TTC_USART1_CTS;
#else
            Config->PortCTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART1_CK
            Config->PortCLK = TTC_USART1_CK;
#else
            Config->PortCLK = E_ttc_gpio_pin_none;
#endif
            break;
        }
#endif
#ifdef TTC_USART2
        case  2: { // check configuration of USART #2
#ifdef TTC_USART2_TX
            Config->PortTxD = TTC_USART2_TX;
#else
            Config->PortTxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART2_RX
            Config->PortRxD = TTC_USART2_RX;
#else
            Config->PortRxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART2_RTS
            Config->PortRTS = TTC_USART2_RTS;
#else
            Config->PortRTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART2_CTS
            Config->PortCTS = TTC_USART2_CTS;
#else
            Config->PortCTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART2_CK
            Config->PortCLK = TTC_USART2_CK;
#else
            Config->PortCLK = E_ttc_gpio_pin_none;
#endif
            break;
        }
#endif
#ifdef TTC_USART3
        case  3: { // check configuration of USART #3
#ifdef TTC_USART3_TX
            Config->PortTxD = TTC_USART3_TX;
#else
            Config->PortTxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART3_RX
            Config->PortRxD = TTC_USART3_RX;
#else
            Config->PortRxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART3_RTS
            Config->PortRTS = TTC_USART3_RTS;
#else
            Config->PortRTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART3_CTS
            Config->PortCTS = TTC_USART3_CTS;
#else
            Config->PortCTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART3_CK
            Config->PortCLK = TTC_USART3_CK;
#else
            Config->PortCLK = E_ttc_gpio_pin_none;
#endif
            break;
        }
#endif
#ifdef TTC_USART4
        case  4: { // check configuration of USART #4
#ifdef TTC_USART4_TX
            Config->PortTxD = TTC_USART4_TX;
#else
            Config->PortTxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART4_RX
            Config->PortRxD = TTC_USART4_RX;
#else
            Config->PortRxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART4_RTS
            Config->PortRTS = TTC_USART4_RTS;
#else
            Config->PortRTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART4_CTS
            Config->PortCTS = TTC_USART4_CTS;
#else
            Config->PortCTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART4_CK
            Config->PortCLK = TTC_USART4_CK;
#else
            Config->PortCLK = E_ttc_gpio_pin_none;
#endif
            break;
        }
#endif
#ifdef TTC_USART5
        case  5: { // check configuration of USART #5
#ifdef TTC_USART5_TX
            Config->PortTxD = TTC_USART5_TX;
#else
            Config->PortTxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART5_RX
            Config->PortRxD = TTC_USART5_RX;
#else
            Config->PortRxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART5_RTS
            Config->PortRTS = TTC_USART5_RTS;
#else
            Config->PortRTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART5_CTS
            Config->PortCTS = TTC_USART5_CTS;
#else
            Config->PortCTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART5_CK
            Config->PortCLK = TTC_USART5_CK;
#else
            Config->PortCLK = E_ttc_gpio_pin_none;
#endif
            break;
        }
#endif
#ifdef TTC_USART6
        case  6: { // check configuration of USART #6
#ifdef TTC_USART6_TX
            Config->PortTxD = TTC_USART6_TX;
#else
            Config->PortTxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART6_RX
            Config->PortRxD = TTC_USART6_RX;
#else
            Config->PortRxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART6_RTS
            Config->PortRTS = TTC_USART6_RTS;
#else
            Config->PortRTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART6_CTS
            Config->PortCTS = TTC_USART6_CTS;
#else
            Config->PortCTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART6_CK
            Config->PortCLK = TTC_USART6_CK;
#else
            Config->PortCLK = E_ttc_gpio_pin_none;
#endif
            break;
        }
#endif
#ifdef TTC_USART7
        case  7: { // check configuration of USART #7
#ifdef TTC_USART7_TX
            Config->PortTxD = TTC_USART7_TX;
#else
            Config->PortTxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART7_RX
            Config->PortRxD = TTC_USART7_RX;
#else
            Config->PortRxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART7_RTS
            Config->PortRTS = TTC_USART7_RTS;
#else
            Config->PortRTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART7_CTS
            Config->PortCTS = TTC_USART7_CTS;
#else
            Config->PortCTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART7_CK
            Config->PortCLK = TTC_USART7_CK;
#else
            Config->PortCLK = E_ttc_gpio_pin_none;
#endif
            break;
        }
#endif
#ifdef TTC_USART8
        case  8: { // check configuration of USART #8
#ifdef TTC_USART8_TX
            Config->PortTxD = TTC_USART8_TX;
#else
            Config->PortTxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART8_RX
            Config->PortRxD = TTC_USART8_RX;
#else
            Config->PortRxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART8_RTS
            Config->PortRTS = TTC_USART8_RTS;
#else
            Config->PortRTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART8_CTS
            Config->PortCTS = TTC_USART8_CTS;
#else
            Config->PortCTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART8_CK
            Config->PortCLK = TTC_USART8_CK;
#else
            Config->PortCLK = E_ttc_gpio_pin_none;
#endif
            break;
        }
#endif
#ifdef TTC_USART9
        case  9: { // check configuration of USART #9
#ifdef TTC_USART9_TX
            Config->PortTxD = TTC_USART9_TX;
#else
            Config->PortTxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART9_RX
            Config->PortRxD = TTC_USART9_RX;
#else
            Config->PortRxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART9_RTS
            Config->PortRTS = TTC_USART9_RTS;
#else
            Config->PortRTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART9_CTS
            Config->PortCTS = TTC_USART9_CTS;
#else
            Config->PortCTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART9_CK
            Config->PortCLK = TTC_USART9_CK;
#else
            Config->PortCLK = E_ttc_gpio_pin_none;
#endif
            break;
        }
#endif
#ifdef TTC_USART10
        case 10: { // check configuration of USART #10
#ifdef TTC_USART10_TX
            Config->PortTxD = TTC_USART10_TX;
#else
            Config->PortTxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART10_RX
            Config->PortRxD = TTC_USART10_RX;
#else
            Config->PortRxD = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART10_RTS
            Config->PortRTS = TTC_USART10_RTS;
#else
            Config->PortRTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART10_CTS
            Config->PortCTS = TTC_USART10_CTS;
#else
            Config->PortCTS = E_ttc_gpio_pin_none;
#endif
#ifdef TTC_USART10_CK
            Config->PortCLK = TTC_USART10_CK;
#else
            Config->PortCLK = E_ttc_gpio_pin_none;
#endif
            break;
        }
#endif

        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
    }

    //Insert additional generic default values here ...

    _driver_usart_load_defaults( Config );

    // check certain required config fields that have to be filled by low-level driver
    Assert_USART_Writable( ( void* ) Config->BaseRegister, ttc_assert_origin_auto );  // low-level driver must assign base address of peripheral register!

}
t_u8                  ttc_usart_logical_2_physical_index( t_u8 LogicalIndex ) {

    switch ( LogicalIndex ) {         // determine base register and other low-level configuration data
#ifdef TTC_USART1
        case 1: return TTC_USART1;
#endif
#ifdef TTC_USART2
        case 2: return TTC_USART2;
#endif
#ifdef TTC_USART3
        case 3: return TTC_USART3;
#endif
#ifdef TTC_USART4
        case 4: return TTC_USART4;
#endif
#ifdef TTC_USART5
        case 5: return TTC_USART5;
#endif
#ifdef TTC_USART6
        case 6: return TTC_USART6;
#endif
#ifdef TTC_USART7
        case 7: return TTC_USART7;
#endif
#ifdef TTC_USART8
        case 8: return TTC_USART8;
#endif
#ifdef TTC_USART9
        case 9: return TTC_USART9;
#endif
#ifdef TTC_USART10
        case 10: return TTC_USART10;
#endif
        // extend as required

        default: break;
    }

    ttc_assert_halt_origin( ttc_assert_origin_auto ); // No TTC_USARTn defined! Check your makefile.100_board_* file!
    return -1; // program flow should never go here
}
void                  ttc_usart_prepare() {
    ttc_queue_pointer_init( &ttc_usart_QueueEmptyBlocks, &( ttc_usart_QueueEmptyBlocks_Data[0] ), TTC_USART_MAX_MEMORY_BUFFERS + 1 );
    ttc_usart_Amount_BlocksAllocated = 0;
    ttc_usart_Amount_RxBytesSkipped  = 0;

    // reset all data entries of safe array
    A_reset( ttc_usart_configs, TTC_USART_MAX_AMOUNT ); // must reset data of safe array manually!

    _driver_usart_prepare();
}
t_u8                  ttc_usart_physical_2_logical_index( t_u8 PhysicalIndex ) {

    switch ( PhysicalIndex ) {         // determine base register and other low-level configuration data
#ifdef TTC_USART1
        case TTC_USART1: return 1;
#endif
#ifdef TTC_USART2
        case TTC_USART2: return 2;
#endif
#ifdef TTC_USART3
        case TTC_USART3: return 3;
#endif
#ifdef TTC_USART4
        case TTC_USART4: return 4;
#endif
#ifdef TTC_USART5
        case TTC_USART5: return 5;
#endif
#ifdef TTC_USART6
        case TTC_USART6: return 6;
#endif
#ifdef TTC_USART7
        case TTC_USART7: return 7;
#endif
#ifdef TTC_USART8
        case TTC_USART8: return 8;
#endif
#ifdef TTC_USART9
        case TTC_USART9: return 9;
#endif
#ifdef TTC_USART10
        case TTC_USART10: return 10;
#endif
        // extend as required

        default: break;
    }

    ttc_assert_halt_origin( ttc_assert_origin_auto ); // No TTC_USARTn defined! Check your makefile.100_board_* file!
    return -1; // program flow should never go here
}
e_ttc_usart_errorcode ttc_usart_register_receive( t_u8 LogicalIndex, t_ttc_heap_block * ( *receiveBlock )( void* Argument, t_ttc_usart_config* Config, t_ttc_heap_block* Block ), void* Argument ) {
    t_ttc_usart_config* Config = ttc_usart_get_configuration( LogicalIndex );

    if ( Config->Init.Flags.Receive ) { // USART driver implements receive functionality
        Config->Init.receiveBlock          = receiveBlock;
        Config->Init.receiveBlock_Argument = Argument;

        return ttc_usart_init( LogicalIndex );
    }
    return ec_usart_NotImplemented;
}
e_ttc_usart_errorcode ttc_usart_reset( t_u8 LogicalIndex ) {
    Assert_USART( LogicalIndex, ttc_assert_origin_auto );  // logical index starts at 1
    t_ttc_usart_config* Config = _ttc_usart_get_configuration( LogicalIndex );

    ttc_memory_set( Config, 0, sizeof( Config ) );
    Config->LogicalIndex  = LogicalIndex;
    Config->PhysicalIndex = ttc_usart_get_physicalindex( LogicalIndex );


    e_ttc_usart_errorcode Status = _driver_usart_reset( Config );

    return Status;
}
e_ttc_usart_errorcode ttc_usart_send_raw( t_u8 LogicalIndex, t_u8* Buffer, t_base Amount ) {
    Assert_USART( Buffer != NULL, ttc_assert_origin_auto );
    t_ttc_usart_config* Config = _ttc_usart_get_configuration( LogicalIndex );

    if ( Config->Init.Flags.DelayedTransmits ) { // push buffer to Config->Queue_Tx

        // data must be copied into one or more memory blocks
        t_u16 RemainingBytes = Amount;
        t_u16 Bytes_written  = 0;
        while ( RemainingBytes > 0 ) {
            t_ttc_heap_block* TxBlock = ttc_usart_get_empty_block();

            t_u16 Bytes2Copy = RemainingBytes;
            if ( Bytes2Copy > TTC_USART_BLOCK_SIZE )
            { Bytes2Copy = TTC_USART_BLOCK_SIZE; }

            ttc_memory_copy( TxBlock->Buffer, &Buffer[Bytes_written], Bytes2Copy );
            TxBlock->Size = Bytes2Copy;
            ttc_usart_send_block( LogicalIndex, TxBlock );
            RemainingBytes -= Bytes2Copy;
            Bytes_written += Bytes2Copy;
        }

    }
    else
    { _ttc_usart_send_raw_blocking( LogicalIndex, Buffer, Amount ); }

    return ec_usart_OK;
}
e_ttc_usart_errorcode ttc_usart_send_raw_const( t_u8 LogicalIndex, t_u8* Buffer, t_base Amount ) {
    Assert_USART( Buffer != NULL, ttc_assert_origin_auto );
    t_ttc_usart_config* Config = _ttc_usart_get_configuration( LogicalIndex );

    if ( Config->Init.Flags.DelayedTransmits ) { // push buffer to Config->Queue_Tx
        // data from constant memory is not copied
        t_ttc_heap_block* TxBlock = ttc_usart_get_empty_block();
        TxBlock->Buffer = Buffer; // use memory block as a virtual memory block
        TxBlock->Size   = Amount;
        if ( TxBlock->Size < 0 )  // make sure its a positive value (negative size means zero terminated with max length)
        { TxBlock->Size = -TxBlock->Size; }
        ttc_usart_send_block( LogicalIndex, TxBlock );
    }
    else
    { _ttc_usart_send_raw_blocking( LogicalIndex, Buffer, Amount ); }

    return ec_usart_OK;
}
e_ttc_usart_errorcode ttc_usart_send_string( t_u8 LogicalIndex, const void* Buffer, t_base MaxLength ) {
    Assert_USART( Buffer != NULL, ttc_assert_origin_auto );
    t_ttc_usart_config* Config = _ttc_usart_get_configuration( LogicalIndex );

    if ( Config->Init.Flags.DelayedTransmits ) { // push buffer to Config->Queue_Tx
        // data must be copied into one or more memory blocks
        t_u16 TotalBytesCopied = 0;
        const t_u8* Reader = ( t_u8* ) Buffer;
        while ( TotalBytesCopied < MaxLength ) {
            t_ttc_heap_block* TxBlock = ttc_usart_get_empty_block();

            t_u8* Writer = TxBlock->Buffer;

            t_u16 MaxBytes2Copy = TTC_USART_BLOCK_SIZE;
            if ( MaxBytes2Copy > ( MaxLength - TotalBytesCopied ) ) {
                MaxBytes2Copy = MaxLength - TotalBytesCopied;
            }

            t_u16 BytesCopied = 0;
            while ( BytesCopied < MaxBytes2Copy ) {
                t_u8 C = *Reader++;
                if ( C != 0 )         // stopping at first zero
                { *Writer++ = C; }
                else {
                    MaxLength = TotalBytesCopied; // will cause outer loop to leave
                    break;
                }

                BytesCopied++;
                TotalBytesCopied++;
            }

            TxBlock->Size = BytesCopied;
            ttc_usart_send_block( LogicalIndex, TxBlock );
        }
    }
    else
    { _ttc_usart_send_string_blocking( LogicalIndex, ( const char* ) Buffer, MaxLength ); }

    return ec_usart_OK;
}
e_ttc_usart_errorcode ttc_usart_send_string_const( t_u8 LogicalIndex, const char* Buffer, t_base MaxLength ) {
    Assert_USART( Buffer != NULL, ttc_assert_origin_auto );
    t_ttc_usart_config* Config = _ttc_usart_get_configuration( LogicalIndex );

    if ( Config->Init.Flags.DelayedTransmits ) { // push buffer to Config->Queue_Tx
        // data from constant memory is not copied
        t_ttc_heap_block* TxBlock = ttc_usart_get_empty_block();
        char* Buffer2 = ( char* ) Buffer;               // yes, we cast away the constness!
        TxBlock->Buffer = ( t_u8* ) Buffer2;            // use memory block as a virtual memory block
        TxBlock->Size   = - ( t_base_signed ) MaxLength; // marking string as zero terminated (see definition of t_ttc_heap_block)
        if ( TxBlock->Size > 0 )                        // make sure its a negative value
        { TxBlock->Size = -TxBlock->Size; }

        ttc_usart_send_block( LogicalIndex, TxBlock );
    }
    else
    { _ttc_usart_send_string_blocking( LogicalIndex, Buffer, MaxLength ); }

    return ec_usart_OK;
}
e_ttc_usart_errorcode ttc_usart_send_block( t_u8 LogicalIndex, t_ttc_heap_block* Block ) {
    Assert_USART( Block != NULL, ttc_assert_origin_auto );
    t_ttc_usart_config* Config = _ttc_usart_get_configuration( LogicalIndex );

    if ( Config->Init.Flags.DelayedTransmits ) { // push buffer to Config->Queue_Tx
        while ( ttc_mutex_lock( &( Config->Queue_TX_Lock ), -1 ) != tme_OK ); // ToDo: required?
        ttc_queue_pointer_push_back( Config->Queue_Tx, Block );
#ifdef TTC_REGRESSION
        Config->Amount_Queue_TX += Block->Size;
#endif
        ttc_mutex_unlock( &( Config->Queue_TX_Lock ) );

        ttc_interrupt_enable_usart( Config->Interrupt_TxComplete );
    }
    else {
        _ttc_usart_send_raw_blocking( LogicalIndex, Block->Buffer, Block->Size );
        ttc_heap_block_release( Block );
    }

    return ec_usart_OK;
}
e_ttc_usart_errorcode ttc_usart_read_word( t_u8 LogicalIndex, t_u16* Word ) {
    Assert_USART( Word != NULL, ttc_assert_origin_auto );
    t_ttc_usart_config* Config = _ttc_usart_get_configuration( LogicalIndex );
    Assert_USART_Writable( ( void* ) Config->BaseRegister, ttc_assert_origin_auto );

    return _driver_usart_read_word_blocking( Config->BaseRegister, Word );
}
e_ttc_usart_errorcode ttc_usart_read_byte( t_u8 LogicalIndex, t_u8* Byte ) {
    Assert_USART( Byte != NULL, ttc_assert_origin_auto );
    t_ttc_usart_config* Config = _ttc_usart_get_configuration( LogicalIndex );
    Assert_USART_Writable( ( void* ) Config->BaseRegister, ttc_assert_origin_auto );

    return _driver_usart_read_byte( Config->BaseRegister, Byte );
}
e_ttc_usart_errorcode ttc_usart_stdout_set( t_u8 LogicalIndex ) {
    Assert_USART( LogicalIndex <= TTC_USART_AMOUNT, ttc_assert_origin_auto );  // not egnough ttc_usart_configs defined!
    Assert_USART( A( ttc_usart_configs, LogicalIndex - 1 ) != NULL, ttc_assert_origin_auto );  // USART not yet initialized!

    ttc_usart_stdout_index = LogicalIndex;
    return ec_usart_OK;
}
void                  ttc_usart_stdout_send_block( t_ttc_heap_block* Block ) {
    if ( ttc_usart_stdout_index ) {
        ttc_usart_send_block( ttc_usart_stdout_index, Block );
    }
}
void                  ttc_usart_stdout_send_string( const t_u8* String, t_base MaxSize ) {
    if ( ttc_usart_stdout_index ) {
        if ( ttc_memory_is_constant( String ) )
        { ttc_usart_send_string_const( ttc_usart_stdout_index, ( const char* ) String, MaxSize ); }
        else
        { ttc_usart_send_string( ttc_usart_stdout_index, ( const char* ) String, MaxSize ); }
    }
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_usart(t_u8 LogicalIndex) {  }

// Note: Blocking transmit functions can be used from GDB like "p _ttc_usart_send_string_blocking(...)"

t_ttc_usart_config*   _ttc_usart_get_configuration( t_u8 LogicalIndex ) {
    Assert_USART( LogicalIndex > 0, ttc_assert_origin_auto );  // logical USART-indices start at 1 !
    t_ttc_usart_config* Config = A( ttc_usart_configs, LogicalIndex - 1 );
    Assert_USART_Writable( Config, ttc_assert_origin_auto );  // USART not yet created; call ttc_usart_get_configuration() before!

    return Config;
}
void                  _ttc_usart_release_block( t_ttc_heap_block* Block ) {
    Assert_USART_Writable( Block, ttc_assert_origin_auto );

    if ( Block->releaseBuffer == _ttc_usart_release_block ) {
        ttc_queue_pointer_push_back( &ttc_usart_QueueEmptyBlocks, Block ); // ToDo: Change Queue -> List
    }
    else { // release foreign block
        Assert_USART_Writable( Block->releaseBuffer, ttc_assert_origin_auto );
        ttc_heap_block_release( Block );
    }
}
void                  _ttc_usart_release_block_isr( t_ttc_heap_block* Block ) {
    Assert_USART_Writable( Block, ttc_assert_origin_auto );

    if ( Block->releaseBuffer == _ttc_usart_release_block ) {
        e_ttc_queue_error Error = ttc_queue_pointer_push_back_isr( &ttc_usart_QueueEmptyBlocks, Block );
        ( void ) Error; // avoid warning unused variable if Assert_USART() is disabled
        Assert_USART( Error < tqe_Error, ttc_assert_origin_auto );  // could not release memory block (maybe queue is too small?)
    }
    else { // release foreign block
        Assert_USART_Writable( Block->releaseBuffer, ttc_assert_origin_auto );
        ttc_heap_block_release( Block ); // ToDo: release_isr()???
    }
}
void                  _ttc_usart_rx_isr( t_ttc_usart_config* Config, t_u8 Byte ) {

    Assert_USART_Writable( Config, ttc_assert_origin_auto );
    Assert_USART( Config->Queue_Rx != NULL, ttc_assert_origin_auto );
    e_ttc_queue_error PushError;

    if ( Config->Init.activity_rx_isr )
    { Config->Init.activity_rx_isr( Config, Byte ); }

    // send byte to receive task
    PushError = ttc_queue_byte_push_back_isr( Config->Queue_Rx, Byte );
#ifdef TTC_REGRESSION
    Config->Amount_ISR_RX++;
    Assert_USART( PushError < tqe_Error, ttc_assert_origin_auto );  // no bytes should be skipped in a regression test!
#endif

    if ( PushError >= tqe_Error ) {
        if ( Config->Amount_RxBytesSkipped < 255 ) { // limit to 255
            Config->Amount_RxBytesSkipped++;
        }
        else {
            //Assert_USART(FALSE, ttc_assert_origin_auto); // ERROR: Too much bytes lost! (Seems as if your application is too slow)
        }
    }
}
e_ttc_usart_errorcode _ttc_usart_send_word_blocking( t_u8 LogicalIndex, const t_u16 Word ) {
    t_ttc_usart_config* Config = _ttc_usart_get_configuration( LogicalIndex );
    Assert_USART_Writable( ( void* ) Config->BaseRegister, ttc_assert_origin_auto );

    return _driver_usart_send_word_blocking( Config->BaseRegister, Word );
}
e_ttc_usart_errorcode _ttc_usart_send_raw_blocking( t_u8 LogicalIndex, const t_u8* Buffer, t_u16 Amount ) {
    t_ttc_usart_config* Config = _ttc_usart_get_configuration( LogicalIndex );
    Assert_USART_Writable( ( void* ) Config->BaseRegister, ttc_assert_origin_auto );
    e_ttc_usart_errorcode Error = ec_usart_OK;

    while ( ttc_mutex_lock( &( Config->Queue_TX_Lock ), -1 ) != tme_OK );
    while ( Amount-- > 0 ) {
        // Error = _usart_stm32f1xx_send_word_blocking(BaseRegister, (t_u16) *Buffer++, -1);
        Error = _driver_usart_send_word_blocking( Config->BaseRegister, ( t_u16 ) * Buffer++ );
    }
    ttc_mutex_unlock( &( Config->Queue_TX_Lock ) );

    return Error;
}
e_ttc_usart_errorcode _ttc_usart_send_string_blocking( t_u8 LogicalIndex, const char* Buffer, t_u16 MaxLength ) {
    Assert_USART( Buffer != NULL, ttc_assert_origin_auto );
    t_ttc_usart_config* Config = _ttc_usart_get_configuration( LogicalIndex );
    Assert_USART_Writable( ( void* ) Config->BaseRegister, ttc_assert_origin_auto );

    char C = 0;
    e_ttc_usart_errorcode Error = ec_usart_OK;
    ttc_mutex_lock( &( Config->Queue_TX_Lock ), -1 );

    while ( ( MaxLength-- > 0 ) && ( ( C = *Buffer++ ) != 0 ) ) {
        if ( Config->Init.activity_tx_isr )
        { Config->Init.activity_tx_isr( Config, C ); }

        Error = _driver_usart_send_word_blocking( Config->BaseRegister, ( t_u16 ) C );
        if ( Error != ec_usart_OK )
        { break; }
    }
    ttc_mutex_unlock( &( Config->Queue_TX_Lock ) );

    return Error;
}
void                  _ttc_usart_task_receive( void* Args ) {
    t_ttc_usart_config* Config = ( t_ttc_usart_config* ) Args;
    Assert_USART_Writable( Config, ttc_assert_origin_auto );

    // Note: A mutex would be safer here, but it is very unlikely, that two tasks initialize the same USART in parallel
    Assert_USART( Config->Flags.TaskRxRunning == 0, ttc_assert_origin_auto );  // task has alrready been spawn for this USART!
    Config->Flags.TaskRxRunning = 1;

    t_ttc_heap_block* CurrentRxBuffer = NULL;
    t_u8* Writer = NULL;
    t_base Size = 0;
    t_u8 Byte = 0;
    t_u16 Char_EndOfLine = Config->Init.Char_EndOfLine;

    while ( 1 ) {
        Byte = ttc_queue_byte_pull_front( Config->Queue_Rx );
        // single byte received: process it
#ifdef TTC_REGRESSION
        Config->Amount_Queue_RX++;
#endif

        if ( Config->Init.receiveBlock != NULL ) {

            if ( Writer == NULL ) { // no buffer available: fetch new memory block from Queue_EmptyRxBlocks
                if ( !CurrentRxBuffer ) { // no receive buffer: fetch one from queue
                    CurrentRxBuffer = ttc_usart_get_empty_block();
                    Assert_USART_Writable( CurrentRxBuffer, ttc_assert_origin_auto );  // got NULL-pointer as pointer to memory block!
                }
                Writer = CurrentRxBuffer->Buffer;
                Size = 0;
            }

            if ( Writer == NULL ) { // no buffer available: deliver single byte
                Assert_USART( Config->Init.receiveSingleByte != NULL, ttc_assert_origin_auto );  // no memory block and no single receive function available!
                Config->Init.receiveSingleByte( Config, Byte );
            }
            else {                // write byte into buffer
                *Writer++ = Byte;
                Size++;

                if (
                    ( ( Char_EndOfLine < 0x100 ) && ( Char_EndOfLine == Byte ) ) ||
                    ( Size >= TTC_USART_BLOCK_SIZE )
                ) {
                    CurrentRxBuffer->Size = Size;
                    *Writer = 0;

                    // buffer will be reused if receiveBlock() returns pointer != NULL
                    CurrentRxBuffer = Config->Init.receiveBlock( Config->Init.receiveBlock_Argument, Config, CurrentRxBuffer );
                    Writer = NULL;
                }
            }
        }
        else if ( Config->Init.receiveSingleByte != NULL ) { // deliver byte directly
            Config->Init.receiveSingleByte( Config, Byte );
        }
        else { Assert_USART( 0, ttc_assert_origin_auto ); }  // no function registered to pass received data to

    }
}
BOOL                  _ttc_usart_tx_isr( t_ttc_usart_config* Config, volatile void* Argument ) {
    Assert_USART( Config != NULL, ttc_assert_origin_auto );

    static t_ttc_heap_block* CurrentTxBuffer = NULL;
    static t_u8*  tuti_TxBuffer_Reader = NULL;
    static t_base tuti_TxBuffer_Index  = 0;
    static t_base tuti_TxBuffer_Size   = 0;
    static BOOL   tuti_ZeroTerminated  = FALSE;  // ==TRUE: string is zero terminated
    static t_u8   tuti_Byte;
    BOOL BytesRemaining = TRUE;

    if ( tuti_TxBuffer_Reader == NULL ) { // try to get new buffer from Queue_Tx
        e_ttc_queue_error Error = ttc_queue_pointer_pull_front_isr( Config->Queue_Tx, ( void** ) &CurrentTxBuffer );

        if ( Error == tqe_OK ) { // buffer received: use it
            tuti_TxBuffer_Reader = CurrentTxBuffer->Buffer;
            tuti_TxBuffer_Index  = 0;
            tuti_TxBuffer_Size   = 0x7fffffff & CurrentTxBuffer->Size; // make it positive
            tuti_ZeroTerminated  = ( CurrentTxBuffer->Size < 0 ) ? TRUE : FALSE;
        }
        else { // no success in obtaining buffer
            tuti_TxBuffer_Reader = NULL;
            CurrentTxBuffer      = NULL;
            if ( Config->Amount_TxISRsSkipped != -1 ) // increase until max positive value
            { Config->Amount_TxISRsSkipped++; }
        }
    }
    if ( tuti_TxBuffer_Reader != NULL ) { // have valid, non-empty TxBuffer: send single byte
        tuti_Byte = *tuti_TxBuffer_Reader++;
        tuti_TxBuffer_Index++;

        if ( tuti_ZeroTerminated && ( tuti_Byte == 0 ) )
        { tuti_TxBuffer_Index = tuti_TxBuffer_Size; } // all bytes send
        else {
            if ( Config->Init.activity_tx_isr )
            { Config->Init.activity_tx_isr( Config, tuti_Byte ); }

            _driver_usart_send_byte_isr( Argument, tuti_Byte );
#ifdef TTC_REGRESSION
            Config->Amount_ISR_TX++;
#endif
        }
        if ( tuti_TxBuffer_Index >= tuti_TxBuffer_Size ) { // all bytes send: release buffer
            _ttc_usart_release_block_isr( CurrentTxBuffer );
            tuti_TxBuffer_Reader = NULL;
            CurrentTxBuffer      = NULL;
        }
    }
    else
    { BytesRemaining = FALSE; } // no more bytes to send (isr should end now)

    if ( !BytesRemaining ) // no data to send: disable interrupt
    { ttc_interrupt_disable_usart( Config->Interrupt_TxComplete ); }

    return BytesRemaining;
}
void                  _ttc_usart_update_to_sysclock() {

    for ( t_u8 Index = ttc_usart_get_max_index(); Index >= 1; Index-- ) {


        t_ttc_usart_config* Config = ttc_usart_get_configuration( Index );

        if ( Config->Flags.Initialized ) {
            ttc_usart_init( Index );
        }
    }

}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
