/** { ttc_spi_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for SPI device.
 *  Structures, Enums and Defines being required by both, high- and low-level spi.
 *
 *  Created from template ttc_device_types.h revision 22 at 20140423 11:04:14 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_SPI_TYPES_H
#define TTC_SPI_TYPES_H

//{ Includes *************************************************************

#include "ttc_basic_types.h"
#include "ttc_gpio_types.h"
#ifdef EXTENSION_spi_stm32f1xx
    #include "spi/spi_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_spi_stm32l1xx
    #include "spi/spi_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_spi_stm32w1xx
    #include "spi/spi_stm32w1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_spi_stm32f30x
    #include "spi/spi_stm32f30x_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_spi_stm32l0xx
    #include "spi/spi_stm32l0xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

// TTC_SPIn has to be defined as constant by makefile.100_board_*
#ifndef TTC_SPI_AMOUNT
    #ifdef TTC_SPI10
        #ifndef TTC_SPI9
            #      error TTC_SPI10 is defined, but not TTC_SPI9 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI8
            #      error TTC_SPI10 is defined, but not TTC_SPI8 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI7
            #      error TTC_SPI10 is defined, but not TTC_SPI7 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI6
            #      error TTC_SPI10 is defined, but not TTC_SPI6 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI5
            #      error TTC_SPI10 is defined, but not TTC_SPI5 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI4
            #      error TTC_SPI10 is defined, but not TTC_SPI4 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI3
            #      error TTC_SPI10 is defined, but not TTC_SPI3 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI2
            #      error TTC_SPI10 is defined, but not TTC_SPI2 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI1
            #      error TTC_SPI10 is defined, but not TTC_SPI1 - all lower TTC_SPIn must be defined!
        #endif
        #define TTC_SPI_AMOUNT 10
    #endif
#endif

#ifndef TTC_SPI_AMOUNT
    #ifdef TTC_SPI9
        #ifndef TTC_SPI8
            #      error TTC_SPI9 is defined, but not TTC_SPI8 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI7
            #      error TTC_SPI9 is defined, but not TTC_SPI7 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI6
            #      error TTC_SPI9 is defined, but not TTC_SPI6 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI5
            #      error TTC_SPI9 is defined, but not TTC_SPI5 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI4
            #      error TTC_SPI9 is defined, but not TTC_SPI4 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI3
            #      error TTC_SPI9 is defined, but not TTC_SPI3 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI2
            #      error TTC_SPI9 is defined, but not TTC_SPI2 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI1
            #      error TTC_SPI9 is defined, but not TTC_SPI1 - all lower TTC_SPIn must be defined!
        #endif
        #define TTC_SPI_AMOUNT 9
    #endif
#endif

#ifndef TTC_SPI_AMOUNT
    #ifdef TTC_SPI8
        #ifndef TTC_SPI7
            #      error TTC_SPI8 is defined, but not TTC_SPI7 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI6
            #      error TTC_SPI8 is defined, but not TTC_SPI6 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI5
            #      error TTC_SPI8 is defined, but not TTC_SPI5 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI4
            #      error TTC_SPI8 is defined, but not TTC_SPI4 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI3
            #      error TTC_SPI8 is defined, but not TTC_SPI3 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI2
            #      error TTC_SPI8 is defined, but not TTC_SPI2 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI1
            #      error TTC_SPI8 is defined, but not TTC_SPI1 - all lower TTC_SPIn must be defined!
        #endif
        #define TTC_SPI_AMOUNT 8
    #endif
#endif

#ifndef TTC_SPI_AMOUNT
    #ifdef TTC_SPI7
        #ifndef TTC_SPI6
            #      error TTC_SPI7 is defined, but not TTC_SPI6 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI5
            #      error TTC_SPI7 is defined, but not TTC_SPI5 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI4
            #      error TTC_SPI7 is defined, but not TTC_SPI4 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI3
            #      error TTC_SPI7 is defined, but not TTC_SPI3 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI2
            #      error TTC_SPI7 is defined, but not TTC_SPI2 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI1
            #      error TTC_SPI7 is defined, but not TTC_SPI1 - all lower TTC_SPIn must be defined!
        #endif
        #define TTC_SPI_AMOUNT 7
    #endif
#endif

#ifndef TTC_SPI_AMOUNT
    #ifdef TTC_SPI6
        #ifndef TTC_SPI5
            #      error TTC_SPI6 is defined, but not TTC_SPI5 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI4
            #      error TTC_SPI6 is defined, but not TTC_SPI4 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI3
            #      error TTC_SPI6 is defined, but not TTC_SPI3 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI2
            #      error TTC_SPI6 is defined, but not TTC_SPI2 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI1
            #      error TTC_SPI6 is defined, but not TTC_SPI1 - all lower TTC_SPIn must be defined!
        #endif
        #define TTC_SPI_AMOUNT 6
    #endif
#endif

#ifndef TTC_SPI_AMOUNT
    #ifdef TTC_SPI5
        #ifndef TTC_SPI4
            #      error TTC_SPI5 is defined, but not TTC_SPI4 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI3
            #      error TTC_SPI5 is defined, but not TTC_SPI3 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI2
            #      error TTC_SPI5 is defined, but not TTC_SPI2 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI1
            #      error TTC_SPI5 is defined, but not TTC_SPI1 - all lower TTC_SPIn must be defined!
        #endif
        #define TTC_SPI_AMOUNT 5
    #endif
#endif

#ifndef TTC_SPI_AMOUNT
    #ifdef TTC_SPI4
        #ifndef TTC_SPI3
            #      error TTC_SPI4 is defined, but not TTC_SPI3 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI2
            #      error TTC_SPI4 is defined, but not TTC_SPI2 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI1
            #      error TTC_SPI4 is defined, but not TTC_SPI1 - all lower TTC_SPIn must be defined!
        #endif
        #define TTC_SPI_AMOUNT 4
    #endif
#endif

#ifndef TTC_SPI_AMOUNT
    #ifdef TTC_SPI3
        #ifndef TTC_SPI2
            #      error TTC_SPI3 is defined, but not TTC_SPI2 - all lower TTC_SPIn must be defined!
        #endif
        #ifndef TTC_SPI1
            #      error TTC_SPI3 is defined, but not TTC_SPI1 - all lower TTC_SPIn must be defined!
        #endif
        #define TTC_SPI_AMOUNT 3
    #endif
#endif

#ifndef TTC_SPI_AMOUNT
    #ifdef TTC_SPI2
        #ifndef TTC_SPI1
            #      error TTC_SPI2 is defined, but not TTC_SPI1 - all lower TTC_SPIn must be defined!
        #endif
        #define TTC_SPI_AMOUNT 2
    #endif
#endif

#ifndef TTC_SPI_AMOUNT
    #ifdef TTC_SPI1
        #define TTC_SPI_AMOUNT 1
    #endif
#endif

/* Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_SPI 0  # disable assert handling for spi devices
 *
 */
#ifndef TTC_ASSERT_SPI    // any previous definition set (Makefile)?
    #define TTC_ASSERT_SPI 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_SPI == 1)  // use Assert()s in SPI code (somewhat slower but alot easier to debug)
    #define Assert_SPI(Condition, Origin) Assert( ((Condition) != 0), Origin)
    #define Assert_SPI_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_SPI_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in SPI code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_SPI(Condition, Origin)
    #define Assert_SPI_Writable(Address, Origin)
    #define Assert_SPI_Readable(Address, Origin)
#endif


#ifndef TTC_ASSERT_SPI_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_SPI_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_SPI_EXTRA == 1)  // use Assert()s in SPI code (somewhat slower but alot easier to debug)
    #define Assert_SPI_EXTRA(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_SPI_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_SPI_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in SPI code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_SPI_EXTRA(Condition, Origin)
    #define Assert_SPI_EXTRA_Writable(Address, Origin)
    #define Assert_SPI_EXTRA_Readable(Address, Origin)
#endif

/** Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_spi_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_spi_architecture
    #warning Missing low-level definition for t_ttc_spi_architecture (using default)
    #define t_ttc_spi_architecture void
#endif

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef e_ttc_physical_index e_ttc_spi_physical_index;
#define TTC_INDEX_MAX 10

/** Check Static Configuration of all SPI devices
 *
 */
#ifdef TTC_SPI1 //{
    #if TTC_SPI1 >= TTC_INDEX_MAX
        #    error Wrong definition of TTC_SPI1, check your makefile (must be one from e_ttc_spi_physical_index)!
    #endif
    #ifndef TTC_SPI1_NSS_AMOUNT
        #ifdef TTC_SPI1_NSS10
            #define TTC_SPI1_NSS_AMOUNT 10
        #endif
    #endif
    #ifndef TTC_SPI1_NSS_AMOUNT
        #ifdef TTC_SPI1_NSS9
            #define TTC_SPI1_NSS_AMOUNT 9
        #endif
    #endif
    #ifndef TTC_SPI1_NSS_AMOUNT
        #ifdef TTC_SPI1_NSS8
            #define TTC_SPI1_NSS_AMOUNT 8
        #endif
    #endif
    #ifndef TTC_SPI1_NSS_AMOUNT
        #ifdef TTC_SPI1_NSS7
            #define TTC_SPI1_NSS_AMOUNT 7
        #endif
    #endif
    #ifndef TTC_SPI1_NSS_AMOUNT
        #ifdef TTC_SPI1_NSS6
            #define TTC_SPI1_NSS_AMOUNT 6
        #endif
    #endif
    #ifndef TTC_SPI1_NSS_AMOUNT
        #ifdef TTC_SPI1_NSS5
            #define TTC_SPI1_NSS_AMOUNT 5
        #endif
    #endif
    #ifndef TTC_SPI1_NSS_AMOUNT
        #ifdef TTC_SPI1_NSS4
            #define TTC_SPI1_NSS_AMOUNT 4
        #endif
    #endif
    #ifndef TTC_SPI1_NSS_AMOUNT
        #ifdef TTC_SPI1_NSS3
            #define TTC_SPI1_NSS_AMOUNT 3
        #endif
    #endif
    #ifndef TTC_SPI1_NSS_AMOUNT
        #ifdef TTC_SPI1_NSS2
            #define TTC_SPI1_NSS_AMOUNT 2
        #endif
    #endif
    #ifndef TTC_SPI1_NSS_AMOUNT
        #ifdef TTC_SPI1_NSS1
            #define TTC_SPI1_NSS_AMOUNT 1
        #endif
    #endif
    #ifndef TTC_SPI1_NSS_AMOUNT
        #define TTC_SPI1_NSS_AMOUNT 0
    #endif
#endif //}
#ifdef TTC_SPI2 //{
    #if TTC_SPI2 >= TTC_INDEX_MAX
        #    error Wrong definition of TTC_SPI2, check your makefile (must be one from e_ttc_spi_physical_index)!
    #endif
    #ifndef TTC_SPI2_NSS_AMOUNT
        #ifdef TTC_SPI2_NSS10
            #define TTC_SPI2_NSS_AMOUNT 10
        #endif
    #endif
    #ifndef TTC_SPI2_NSS_AMOUNT
        #ifdef TTC_SPI2_NSS9
            #define TTC_SPI2_NSS_AMOUNT 9
        #endif
    #endif
    #ifndef TTC_SPI2_NSS_AMOUNT
        #ifdef TTC_SPI2_NSS8
            #define TTC_SPI2_NSS_AMOUNT 8
        #endif
    #endif
    #ifndef TTC_SPI2_NSS_AMOUNT
        #ifdef TTC_SPI2_NSS7
            #define TTC_SPI2_NSS_AMOUNT 7
        #endif
    #endif
    #ifndef TTC_SPI2_NSS_AMOUNT
        #ifdef TTC_SPI2_NSS6
            #define TTC_SPI2_NSS_AMOUNT 6
        #endif
    #endif
    #ifndef TTC_SPI2_NSS_AMOUNT
        #ifdef TTC_SPI2_NSS5
            #define TTC_SPI2_NSS_AMOUNT 5
        #endif
    #endif
    #ifndef TTC_SPI2_NSS_AMOUNT
        #ifdef TTC_SPI2_NSS4
            #define TTC_SPI2_NSS_AMOUNT 4
        #endif
    #endif
    #ifndef TTC_SPI2_NSS_AMOUNT
        #ifdef TTC_SPI2_NSS3
            #define TTC_SPI2_NSS_AMOUNT 3
        #endif
    #endif
    #ifndef TTC_SPI2_NSS_AMOUNT
        #ifdef TTC_SPI2_NSS2
            #define TTC_SPI2_NSS_AMOUNT 2
        #endif
    #endif
    #ifndef TTC_SPI2_NSS_AMOUNT
        #ifdef TTC_SPI2_NSS1
            #define TTC_SPI2_NSS_AMOUNT 1
        #endif
    #endif
    #ifndef TTC_SPI2_NSS_AMOUNT
        #define TTC_SPI2_NSS_AMOUNT 0
    #endif
#endif //}
#ifdef TTC_SPI3 //{
    #if TTC_SPI3 >= TTC_INDEX_MAX
        #    error Wrong definition of TTC_SPI3, check your makefile (must be one from e_ttc_spi_physical_index)!
    #endif
    #ifndef TTC_SPI3_NSS_AMOUNT
        #ifdef TTC_SPI3_NSS10
            #define TTC_SPI3_NSS_AMOUNT 10
        #endif
    #endif
    #ifndef TTC_SPI3_NSS_AMOUNT
        #ifdef TTC_SPI3_NSS9
            #define TTC_SPI3_NSS_AMOUNT 9
        #endif
    #endif
    #ifndef TTC_SPI3_NSS_AMOUNT
        #ifdef TTC_SPI3_NSS8
            #define TTC_SPI3_NSS_AMOUNT 8
        #endif
    #endif
    #ifndef TTC_SPI3_NSS_AMOUNT
        #ifdef TTC_SPI3_NSS7
            #define TTC_SPI3_NSS_AMOUNT 7
        #endif
    #endif
    #ifndef TTC_SPI3_NSS_AMOUNT
        #ifdef TTC_SPI3_NSS6
            #define TTC_SPI3_NSS_AMOUNT 6
        #endif
    #endif
    #ifndef TTC_SPI3_NSS_AMOUNT
        #ifdef TTC_SPI3_NSS5
            #define TTC_SPI3_NSS_AMOUNT 5
        #endif
    #endif
    #ifndef TTC_SPI3_NSS_AMOUNT
        #ifdef TTC_SPI3_NSS4
            #define TTC_SPI3_NSS_AMOUNT 4
        #endif
    #endif
    #ifndef TTC_SPI3_NSS_AMOUNT
        #ifdef TTC_SPI3_NSS3
            #define TTC_SPI3_NSS_AMOUNT 3
        #endif
    #endif
    #ifndef TTC_SPI3_NSS_AMOUNT
        #ifdef TTC_SPI3_NSS2
            #define TTC_SPI3_NSS_AMOUNT 2
        #endif
    #endif
    #ifndef TTC_SPI3_NSS_AMOUNT
        #ifdef TTC_SPI3_NSS1
            #define TTC_SPI3_NSS_AMOUNT 1
        #endif
    #endif
    #ifndef TTC_SPI3_NSS_AMOUNT
        #define TTC_SPI3_NSS_AMOUNT 0
    #endif
#endif //}
#ifdef TTC_SPI4 //{
    #if TTC_SPI4 >= TTC_INDEX_MAX
        #    error Wrong definition of TTC_SPI4, check your makefile (must be one from e_ttc_spi_physical_index)!
    #endif
    #ifndef TTC_SPI4_NSS_AMOUNT
        #ifdef TTC_SPI4_NSS10
            #define TTC_SPI4_NSS_AMOUNT 10
        #endif
    #endif
    #ifndef TTC_SPI4_NSS_AMOUNT
        #ifdef TTC_SPI4_NSS9
            #define TTC_SPI4_NSS_AMOUNT 9
        #endif
    #endif
    #ifndef TTC_SPI4_NSS_AMOUNT
        #ifdef TTC_SPI4_NSS8
            #define TTC_SPI4_NSS_AMOUNT 8
        #endif
    #endif
    #ifndef TTC_SPI4_NSS_AMOUNT
        #ifdef TTC_SPI4_NSS7
            #define TTC_SPI4_NSS_AMOUNT 7
        #endif
    #endif
    #ifndef TTC_SPI4_NSS_AMOUNT
        #ifdef TTC_SPI4_NSS6
            #define TTC_SPI4_NSS_AMOUNT 6
        #endif
    #endif
    #ifndef TTC_SPI4_NSS_AMOUNT
        #ifdef TTC_SPI4_NSS5
            #define TTC_SPI4_NSS_AMOUNT 5
        #endif
    #endif
    #ifndef TTC_SPI4_NSS_AMOUNT
        #ifdef TTC_SPI4_NSS4
            #define TTC_SPI4_NSS_AMOUNT 4
        #endif
    #endif
    #ifndef TTC_SPI4_NSS_AMOUNT
        #ifdef TTC_SPI4_NSS3
            #define TTC_SPI4_NSS_AMOUNT 3
        #endif
    #endif
    #ifndef TTC_SPI4_NSS_AMOUNT
        #ifdef TTC_SPI4_NSS2
            #define TTC_SPI4_NSS_AMOUNT 2
        #endif
    #endif
    #ifndef TTC_SPI4_NSS_AMOUNT
        #ifdef TTC_SPI4_NSS1
            #define TTC_SPI4_NSS_AMOUNT 1
        #endif
    #endif
    #ifndef TTC_SPI4_NSS_AMOUNT
        #define TTC_SPI4_NSS_AMOUNT 0
    #endif
#endif //}
#ifdef TTC_SPI5 //{
    #if TTC_SPI5 >= TTC_INDEX_MAX
        #    error Wrong definition of TTC_SPI5, check your makefile (must be one from e_ttc_spi_physical_index)!
    #endif
    #ifndef TTC_SPI5_NSS_AMOUNT
        #ifdef TTC_SPI5_NSS10
            #define TTC_SPI5_NSS_AMOUNT 10
        #endif
    #endif
    #ifndef TTC_SPI5_NSS_AMOUNT
        #ifdef TTC_SPI5_NSS9
            #define TTC_SPI5_NSS_AMOUNT 9
        #endif
    #endif
    #ifndef TTC_SPI5_NSS_AMOUNT
        #ifdef TTC_SPI5_NSS8
            #define TTC_SPI5_NSS_AMOUNT 8
        #endif
    #endif
    #ifndef TTC_SPI5_NSS_AMOUNT
        #ifdef TTC_SPI5_NSS7
            #define TTC_SPI5_NSS_AMOUNT 7
        #endif
    #endif
    #ifndef TTC_SPI5_NSS_AMOUNT
        #ifdef TTC_SPI5_NSS6
            #define TTC_SPI5_NSS_AMOUNT 6
        #endif
    #endif
    #ifndef TTC_SPI5_NSS_AMOUNT
        #ifdef TTC_SPI5_NSS5
            #define TTC_SPI5_NSS_AMOUNT 5
        #endif
    #endif
    #ifndef TTC_SPI5_NSS_AMOUNT
        #ifdef TTC_SPI5_NSS4
            #define TTC_SPI5_NSS_AMOUNT 4
        #endif
    #endif
    #ifndef TTC_SPI5_NSS_AMOUNT
        #ifdef TTC_SPI5_NSS3
            #define TTC_SPI5_NSS_AMOUNT 3
        #endif
    #endif
    #ifndef TTC_SPI5_NSS_AMOUNT
        #ifdef TTC_SPI5_NSS2
            #define TTC_SPI5_NSS_AMOUNT 2
        #endif
    #endif
    #ifndef TTC_SPI5_NSS_AMOUNT
        #ifdef TTC_SPI5_NSS1
            #define TTC_SPI5_NSS_AMOUNT 1
        #endif
    #endif
    #ifndef TTC_SPI5_NSS_AMOUNT
        #define TTC_SPI5_NSS_AMOUNT 0
    #endif
#endif //}
#ifdef TTC_SPI6 //{
    #if TTC_SPI6 >= TTC_INDEX_MAX
        #    error Wrong definition of TTC_SPI6, check your makefile (must be one from e_ttc_spi_physical_index)!
    #endif
    #ifndef TTC_SPI6_NSS_AMOUNT
        #ifdef TTC_SPI6_NSS10
            #define TTC_SPI6_NSS_AMOUNT 10
        #endif
    #endif
    #ifndef TTC_SPI6_NSS_AMOUNT
        #ifdef TTC_SPI6_NSS9
            #define TTC_SPI6_NSS_AMOUNT 9
        #endif
    #endif
    #ifndef TTC_SPI6_NSS_AMOUNT
        #ifdef TTC_SPI6_NSS8
            #define TTC_SPI6_NSS_AMOUNT 8
        #endif
    #endif
    #ifndef TTC_SPI6_NSS_AMOUNT
        #ifdef TTC_SPI6_NSS7
            #define TTC_SPI6_NSS_AMOUNT 7
        #endif
    #endif
    #ifndef TTC_SPI6_NSS_AMOUNT
        #ifdef TTC_SPI6_NSS6
            #define TTC_SPI6_NSS_AMOUNT 6
        #endif
    #endif
    #ifndef TTC_SPI6_NSS_AMOUNT
        #ifdef TTC_SPI6_NSS5
            #define TTC_SPI6_NSS_AMOUNT 5
        #endif
    #endif
    #ifndef TTC_SPI6_NSS_AMOUNT
        #ifdef TTC_SPI6_NSS4
            #define TTC_SPI6_NSS_AMOUNT 4
        #endif
    #endif
    #ifndef TTC_SPI6_NSS_AMOUNT
        #ifdef TTC_SPI6_NSS3
            #define TTC_SPI6_NSS_AMOUNT 3
        #endif
    #endif
    #ifndef TTC_SPI6_NSS_AMOUNT
        #ifdef TTC_SPI6_NSS2
            #define TTC_SPI6_NSS_AMOUNT 2
        #endif
    #endif
    #ifndef TTC_SPI6_NSS_AMOUNT
        #ifdef TTC_SPI6_NSS1
            #define TTC_SPI6_NSS_AMOUNT 1
        #endif
    #endif
    #ifndef TTC_SPI6_NSS_AMOUNT
        #define TTC_SPI6_NSS_AMOUNT 0
    #endif
#endif //}
#ifdef TTC_SPI7 //{
    #if TTC_SPI7 >= TTC_INDEX_MAX
        #    error Wrong definition of TTC_SPI7, check your makefile (must be one from e_ttc_spi_physical_index)!
    #endif
    #ifndef TTC_SPI7_NSS_AMOUNT
        #ifdef TTC_SPI7_NSS10
            #define TTC_SPI7_NSS_AMOUNT 10
        #endif
    #endif
    #ifndef TTC_SPI7_NSS_AMOUNT
        #ifdef TTC_SPI7_NSS9
            #define TTC_SPI7_NSS_AMOUNT 9
        #endif
    #endif
    #ifndef TTC_SPI7_NSS_AMOUNT
        #ifdef TTC_SPI7_NSS8
            #define TTC_SPI7_NSS_AMOUNT 8
        #endif
    #endif
    #ifndef TTC_SPI7_NSS_AMOUNT
        #ifdef TTC_SPI7_NSS7
            #define TTC_SPI7_NSS_AMOUNT 7
        #endif
    #endif
    #ifndef TTC_SPI7_NSS_AMOUNT
        #ifdef TTC_SPI7_NSS6
            #define TTC_SPI7_NSS_AMOUNT 6
        #endif
    #endif
    #ifndef TTC_SPI7_NSS_AMOUNT
        #ifdef TTC_SPI7_NSS5
            #define TTC_SPI7_NSS_AMOUNT 5
        #endif
    #endif
    #ifndef TTC_SPI7_NSS_AMOUNT
        #ifdef TTC_SPI7_NSS4
            #define TTC_SPI7_NSS_AMOUNT 4
        #endif
    #endif
    #ifndef TTC_SPI7_NSS_AMOUNT
        #ifdef TTC_SPI7_NSS3
            #define TTC_SPI7_NSS_AMOUNT 3
        #endif
    #endif
    #ifndef TTC_SPI7_NSS_AMOUNT
        #ifdef TTC_SPI7_NSS2
            #define TTC_SPI7_NSS_AMOUNT 2
        #endif
    #endif
    #ifndef TTC_SPI7_NSS_AMOUNT
        #ifdef TTC_SPI7_NSS1
            #define TTC_SPI7_NSS_AMOUNT 1
        #endif
    #endif
    #ifndef TTC_SPI7_NSS_AMOUNT
        #define TTC_SPI7_NSS_AMOUNT 0
    #endif
#endif //}
#ifdef TTC_SPI8 //{
    #if TTC_SPI8 >= TTC_INDEX_MAX
        #    error Wrong definition of TTC_SPI8, check your makefile (must be one from e_ttc_spi_physical_index)!
    #endif
    #ifndef TTC_SPI8_NSS_AMOUNT
        #ifdef TTC_SPI8_NSS10
            #define TTC_SPI8_NSS_AMOUNT 10
        #endif
    #endif
    #ifndef TTC_SPI8_NSS_AMOUNT
        #ifdef TTC_SPI8_NSS9
            #define TTC_SPI8_NSS_AMOUNT 9
        #endif
    #endif
    #ifndef TTC_SPI8_NSS_AMOUNT
        #ifdef TTC_SPI8_NSS8
            #define TTC_SPI8_NSS_AMOUNT 8
        #endif
    #endif
    #ifndef TTC_SPI8_NSS_AMOUNT
        #ifdef TTC_SPI8_NSS7
            #define TTC_SPI8_NSS_AMOUNT 7
        #endif
    #endif
    #ifndef TTC_SPI8_NSS_AMOUNT
        #ifdef TTC_SPI8_NSS6
            #define TTC_SPI8_NSS_AMOUNT 6
        #endif
    #endif
    #ifndef TTC_SPI8_NSS_AMOUNT
        #ifdef TTC_SPI8_NSS5
            #define TTC_SPI8_NSS_AMOUNT 5
        #endif
    #endif
    #ifndef TTC_SPI8_NSS_AMOUNT
        #ifdef TTC_SPI8_NSS4
            #define TTC_SPI8_NSS_AMOUNT 4
        #endif
    #endif
    #ifndef TTC_SPI8_NSS_AMOUNT
        #ifdef TTC_SPI8_NSS3
            #define TTC_SPI8_NSS_AMOUNT 3
        #endif
    #endif
    #ifndef TTC_SPI8_NSS_AMOUNT
        #ifdef TTC_SPI8_NSS2
            #define TTC_SPI8_NSS_AMOUNT 2
        #endif
    #endif
    #ifndef TTC_SPI8_NSS_AMOUNT
        #ifdef TTC_SPI8_NSS1
            #define TTC_SPI8_NSS_AMOUNT 1
        #endif
    #endif
    #ifndef TTC_SPI8_NSS_AMOUNT
        #define TTC_SPI8_NSS_AMOUNT 0
    #endif
#endif //}
#ifdef TTC_SPI9 //{
    #if TTC_SPI9 >= TTC_INDEX_MAX
        #    error Wrong definition of TTC_SPI9, check your makefile (must be one from e_ttc_spi_physical_index)!
    #endif
    #ifndef TTC_SPI9_NSS_AMOUNT
        #ifdef TTC_SPI9_NSS10
            #define TTC_SPI9_NSS_AMOUNT 10
        #endif
    #endif
    #ifndef TTC_SPI9_NSS_AMOUNT
        #ifdef TTC_SPI9_NSS9
            #define TTC_SPI9_NSS_AMOUNT 9
        #endif
    #endif
    #ifndef TTC_SPI9_NSS_AMOUNT
        #ifdef TTC_SPI9_NSS8
            #define TTC_SPI9_NSS_AMOUNT 8
        #endif
    #endif
    #ifndef TTC_SPI9_NSS_AMOUNT
        #ifdef TTC_SPI9_NSS7
            #define TTC_SPI9_NSS_AMOUNT 7
        #endif
    #endif
    #ifndef TTC_SPI9_NSS_AMOUNT
        #ifdef TTC_SPI9_NSS6
            #define TTC_SPI9_NSS_AMOUNT 6
        #endif
    #endif
    #ifndef TTC_SPI9_NSS_AMOUNT
        #ifdef TTC_SPI9_NSS5
            #define TTC_SPI9_NSS_AMOUNT 5
        #endif
    #endif
    #ifndef TTC_SPI9_NSS_AMOUNT
        #ifdef TTC_SPI9_NSS4
            #define TTC_SPI9_NSS_AMOUNT 4
        #endif
    #endif
    #ifndef TTC_SPI9_NSS_AMOUNT
        #ifdef TTC_SPI9_NSS3
            #define TTC_SPI9_NSS_AMOUNT 3
        #endif
    #endif
    #ifndef TTC_SPI9_NSS_AMOUNT
        #ifdef TTC_SPI9_NSS2
            #define TTC_SPI9_NSS_AMOUNT 2
        #endif
    #endif
    #ifndef TTC_SPI9_NSS_AMOUNT
        #ifdef TTC_SPI9_NSS1
            #define TTC_SPI9_NSS_AMOUNT 1
        #endif
    #endif
    #ifndef TTC_SPI9_NSS_AMOUNT
        #define TTC_SPI9_NSS_AMOUNT 0
    #endif
#endif //}
#ifdef TTC_SPI10 //{
    #if TTC_SPI10 >= TTC_INDEX_MAX
        #    error Wrong definition of TTC_SPI10, check your makefile (must be one from e_ttc_spi_physical_index)!
    #endif
    #ifndef TTC_SPI10_NSS_AMOUNT
        #ifdef TTC_SPI10_NS10
            #define TTC_SPI10_NSS_AMOUNT 10
        #endif
    #endif
    #ifndef TTC_SPI10_NSS_AMOUNT
        #ifdef TTC_SPI10_NS9
            #define TTC_SPI10_NSS_AMOUNT 9
        #endif
    #endif
    #ifndef TTC_SPI10_NSS_AMOUNT
        #ifdef TTC_SPI10_NS8
            #define TTC_SPI10_NSS_AMOUNT 8
        #endif
    #endif
    #ifndef TTC_SPI10_NSS_AMOUNT
        #ifdef TTC_SPI10_NS7
            #define TTC_SPI10_NSS_AMOUNT 7
        #endif
    #endif
    #ifndef TTC_SPI10_NSS_AMOUNT
        #ifdef TTC_SPI10_NS6
            #define TTC_SPI10_NSS_AMOUNT 6
        #endif
    #endif
    #ifndef TTC_SPI10_NSS_AMOUNT
        #ifdef TTC_SPI10_NS5
            #define TTC_SPI10_NSS_AMOUNT 5
        #endif
    #endif
    #ifndef TTC_SPI10_NSS_AMOUNT
        #ifdef TTC_SPI10_NS4
            #define TTC_SPI10_NSS_AMOUNT 4
        #endif
    #endif
    #ifndef TTC_SPI10_NSS_AMOUNT
        #ifdef TTC_SPI10_NS3
            #define TTC_SPI10_NSS_AMOUNT 3
        #endif
    #endif
    #ifndef TTC_SPI10_NSS_AMOUNT
        #ifdef TTC_SPI10_NS2
            #define TTC_SPI10_NSS_AMOUNT 2
        #endif
    #endif
    #ifndef TTC_SPI10_NSS_AMOUNT
        #ifdef TTC_SPI10_NS1
            #define TTC_SPI10_NSS_AMOUNT 1
        #endif
    #endif
    #ifndef TTC_SPI10_NSS_AMOUNT
        #define TTC_SPI10_NSS_AMOUNT 0
    #endif
#endif //}
//}

typedef enum {    // e_ttc_spi_errorcode     return codes of SPI devices
    ec_spi_OK = 0,

    // other warnings go here..
    ec_spi_TimeOut,                // given timeout has exceeded

    ec_spi_ERROR,                  // general failure
    ec_spi_NULL,                   // NULL pointer not accepted
    ec_spi_DeviceNotFound,         // corresponding device could not be found
    ec_spi_InvalidConfiguration,   // sanity check of device configuration failed
    ec_spi_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..

    ec_spi_unknown                // no valid errorcodes past this entry
} e_ttc_spi_errorcode;
typedef enum {    // e_ttc_spi_architecture  types of architectures supported by SPI driver
    ta_spi_None,           // no architecture selected


    ta_spi_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
    ta_spi_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
    ta_spi_stm32w1xx, // automatically added by ./create_DeviceDriver.pl
    ta_spi_stm32f30x, // automatically added by ./create_DeviceDriver.pl
    ta_spi_stm32l0xx, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_spi_unknown        // architecture not supported
} e_ttc_spi_architecture;

typedef struct s_ttc_spi_config { //         architecture independent configuration data

    struct { // Init: fields to be set by application before first calling ttc_<device>_init() --------------
        // Do not change these values after calling ttc_<device>_init()!

        t_u32                 BaudRate;        // transfer speed (RM0008 p.715)
        t_u16                 TimeOut;         // >0: amount of retries to wait for individual SPI commands; ==0: do not wait; ==-1: wait forever
        t_u16                 CRC_Polynom;     // 8-/16-bit polynom used for CRC-calculation
        e_ttc_gpio_pin        Pin_MOSI;        // Pin to use for Master Out Slave In
        e_ttc_gpio_pin        Pin_MISO;        // Pin to use for Master In Slave Out
        e_ttc_gpio_pin        Pin_SCLK;        // Pin to use for Serial Clock
        const e_ttc_gpio_pin* Pins_NSS;        // Array of pins to use for Slave Select (Pins_NSS[0] == 0, Pins_NSS[1] -> Slave #1 ,...)
        t_u8                  Pins_NSS_Amount; // Amount of valid entries in Pins_NSS[]
        t_u16                 SendAsZero;        /* Data to send out to slave when waiting for return data on MISO. (default = 0)
                                                * Some slaves require different data to be sent (e.g. sdcards require 0xff)
                                                * In 8 bit mode, only lower byte is used.
                                                */


        struct { // generic configuration bits common for all low-level Drivers

            unsigned Master            : 1; // =1: enable master mode                       ; =0: mode not set - Note: Only Slave or Master may be 1 at same time!
            unsigned Slave             : 1; // =1: enable slave mode                        ; =0: mode not set - Note: Only Slave or Master may be 1 at same time!
            unsigned MultiMaster       : 1; // =1: enable multi master mode                 ; =0: single master-mode
            unsigned ClockIdleHigh     : 1; // =1: clock line is high during inactivity     ; =0: low
            unsigned ClockPhase2ndEdge : 1; // =1: data are latched on second edge of clock ; =0: first edge
            unsigned Simplex           : 1; // =1: use only two lines (MISO+MOSI on 1 line) ; =0: use three lines (separate MISO, MOSI)
            unsigned Bidirectional     : 1; // =1: bidirectional data wire                  ; =0: receive-/ transmit-only
            unsigned Receive           : 1; // =1: receive data                             ; =0: no receive
            unsigned Transmit          : 1; // =1: transmit data                            ; =0: no transmit
            unsigned WordSize16        : 1; // =1: transport 16-bits in each word           ; =0: 8-bits in each word
            unsigned HardwareNSS       : 1; // =0: NSS-pin is configured as GPIO and must be controlled by software (set to 0 by _init() if Pin_NSS cannot be controlled by hardware)
            unsigned SSOutput          : 1; // =1: SSOutput enabled                         ; =0; SSOutput disabled
            unsigned CRC8              : 1; // =1: activate 8-Bit CRC calculator            ; =0: no CRC-checking
            unsigned CRC16             : 1; // =1: activate 16-Bit CRC calculator           ; =0: use 8-Bit CRC
            unsigned TxDMA             : 1; // =1: use DMA engine to load transmit data     ; =0: transmit data is loaded by software
            unsigned RxDMA             : 1; // =1: use DMA engine to store received data    ; =0: received data is stored by software
            unsigned FirstBitMSB       : 1; // =1: send Most Significant Bit first          ; =0: send Least Significant Bit first
            unsigned Irq_Error         : 1; // =1: generate interrupt when error occurs     ; =0: ignore errors
            unsigned Irq_Received      : 1; // =1: generate interrupt when data is received ; =0: ignore condition
            unsigned Irq_Transmitted   : 1; // =1: generate interrupt when data is sent     ; =0: ignore condition
            unsigned Reserved1        : 11; // pad to 32 bits
        } Flags;
    } Init;

    // fields below are not to be set by application
    e_ttc_spi_architecture  Architecture;   // type of architecture used for current spi device
    t_ttc_spi_architecture* LowLevelConfig; // low-level configuration (structure defined by low-level driver)

    struct { // generic configuration bits common for all low-level Drivers

        unsigned Initialized       : 1; // =1: interface has been initialized and is usable
    } Flags;

    t_u32 TimeOutRxNe_us; // Maximum time to wait for data from slave after sending SendAsZero (microseconds)
    // This value is calculated and used by low-level driver according to Init.BaudRate value during spi_*_init()
    t_u8  LogicalIndex;   // Automatically set: logical index of device to use (1 = TTC_SPI1, ...)
    t_u8  PhysicalIndex;  // Automatically set: physical index of device to use (0 = first hardware device, ...)
    t_u8  Layout;         // Select pin layout to use (some uC allow pin remapping)

} __attribute__( ( __packed__ ) ) t_ttc_spi_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_SPI_TYPES_H
