/** { ttc_assert.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Architecture independent self testing and error reporting.
 *
 *  See corresponding ttc_assert.h for documentation of all high-level functions.
 *
 *  Created from template ttc_device.c revision 40 at 20161107 16:02:14 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_assert.h".
//
#include "ttc_assert.h"
#ifdef EXTENSION_ttc_interrupt
    #include "ttc_interrupt.h"
#endif
#include "ttc_gpio.h"
#include "ttc_task.h"
#include "ttc_memory.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of assert devices.
 *
 */


//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ********************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_assert(t_ttc_assert_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

void ttc_assert_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)

}

void ttc_assert_address_matches( volatile void* Address1, volatile void* Address2 ) {

    if ( Address1 != Address2 )
    { ttc_assert_halt_origin( ttc_assert_origin_auto ); }
}
void ttc_assert_address_matches_origin( volatile void* Address1, volatile void* Address2, volatile e_ttc_assert_origin Origin ) {

    if ( Address1 != Address2 )
    { ttc_assert_halt_origin( Origin ); }
}
void ttc_assert_address_not_null( const void* Pointer ) {

    if ( Pointer == NULL )
    { ttc_assert_halt_origin( ttc_assert_origin_auto ); }
}
void ttc_assert_address_not_null_origin( const void* Pointer, volatile e_ttc_assert_origin Origin ) {

    if ( Pointer == NULL )
    { ttc_assert_halt_origin( Origin ); }
}
void ttc_assert_address_readable( const void* Address ) {
    if ( ! ttc_memory_is_readable( Address ) )
    { ttc_assert_halt_origin( ttc_assert_origin_auto ); }
}
void ttc_assert_address_readable_origin( const void* Address, volatile e_ttc_assert_origin Origin ) {
    if ( ! ttc_memory_is_readable( Address ) )
    { ttc_assert_halt_origin( Origin ); }
}
void ttc_assert_address_writable( void* Address ) {
    if ( ! ttc_memory_is_writable( Address ) )
    { ttc_assert_halt_origin( ttc_assert_origin_auto ); }
}
void ttc_assert_address_writable_origin( void* Address, volatile e_ttc_assert_origin Origin ) {
    if ( ! ttc_memory_is_writable( Address ) )
    { ttc_assert_halt_origin( Origin ); }
}
void ttc_assert_address_executable( void* Address ) {
    if ( ! ttc_memory_is_executable( Address ) )
    { ttc_assert_halt_origin( ttc_assert_origin_auto ); }
}
void ttc_assert_address_executable_origin( void* Address, volatile e_ttc_assert_origin Origin ) {
    if ( ! ttc_memory_is_executable( Address ) )
    { ttc_assert_halt_origin( Origin ); }
}
void __attribute__( ( noinline ) ) ttc_assert_break_origin( volatile e_ttc_assert_origin Origin ) {
    Origin = Origin; // <-- breakpoint here!
}
void __attribute__( ( noinline ) ) ttc_assert_halt_origin( volatile e_ttc_assert_origin Origin ) { // place breakpoint inside!
    ttc_assert_break_origin( Origin );

    volatile static bool HoldOnAssert = TRUE; // reset to 0 if you want to continue after assert!

#ifdef TTC_ASSERT_OUTPUT_PIN
    ttc_gpio_init( TTC_ASSERT_OUTPUT_PIN, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
#endif

    while ( HoldOnAssert ) {

#ifdef TTC_ASSERT_OUTPUT_PIN
#ifdef EXTENSION_ttc_task
        ttc_task_critical_begin(); // avoid task switch during serial output
#endif
        t_u16 Word = ( 0b111111111 & Origin ) << 3;
        Word = Word | 0b1010000000000010; // add start and end bits as described below

        /* Send error code to gpio pin
         *
         * Start  Origin    End
         * 1010   xxxxxxxxx   010
         *
         */
        for ( t_u16 Bit = 1 << 15; Bit != 0; Bit = Bit >> 1 ) {
            if ( Word & Bit )
            { ttc_gpio_set( TTC_ASSERT_OUTPUT_PIN ); }
            else
            { ttc_gpio_clr( TTC_ASSERT_OUTPUT_PIN ); }

            for ( volatile t_u8 Wait = -1; Wait != 0; )
            { Wait--; }
        }

#ifdef EXTENSION_ttc_task
        ttc_task_critical_end();
#endif

        // slow down output
        volatile t_u16 Wait = -1;
        while ( Wait )
        { Wait--; }

#endif
#ifdef EXTENSION_ttc_task
        ttc_task_yield(); // can make trouble if scheduler is not enabled because we cannot
#endif
    }
    // Only returns if HoldOnAssert==FALSE
}
void ttc_assert_halt( ) {
    ttc_assert_halt_origin( ttc_assert_origin_auto );
}
void ttc_assert_test_origin( BOOL TestOK, volatile e_ttc_assert_origin Origin ) {
    if ( !TestOK )
    { ttc_assert_halt_origin( Origin ); }
}
void ttc_assert_test( BOOL TestOK ) {
    if ( !TestOK )
    { ttc_assert_halt_origin( ttc_assert_origin_auto ); }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_assert(t_u8 LogicalIndex) {  }

void assert_failed( t_u8* File, t_u32 Line ) {
    ( void ) File;
    ( void ) Line;
    ttc_assert_halt_origin( ttc_assert_origin_auto );
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
