/** { ttc_network.c ****************************************************

                           The ToolChain

   High-Level interface for network device.

   Implementation of high-level interface.
   This file implements all functionality that is common to all supported
   architectures.

   Authors:

}*/
//{ Defines/ TypDefs *****************************************************
//} Defines

#include "ttc_network.h"

//{ Global Variables *****************************************************

#if TTC_NETWORK_AMOUNT == 0
#warning No devices defined, check your makefile!
#endif

// for each initialized device, a pointer to its generic and 6lowpan definitions is stored
A_define( t_ttc_network_config*, ttc_network_configs, TTC_NETWORK_AMOUNT );

// Set to TRUE on first call of ttc_network_init()
BOOL driver_Initialized = 0;

//} Global Variables
//{ Function definitions *************************************************

t_u8 ttc_network_get_max_index() {
    return TTC_NETWORK_AMOUNT;
}
t_ttc_network_config* ttc_network_get_configuration( t_u8 LogicalIndex ) {
    Assert_NETWORK( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_network_config* Config = A( ttc_network_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        Config = A( ttc_network_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_network_config ) );
        Config->LogicalIndex = LogicalIndex;
        ttc_network_load_defaults( Config );
    }

    return Config;
}
void ttc_network_deinit( t_u8 LogicalIndex ) {
    Assert_NETWORK( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_network_config* Config = ttc_network_get_configuration( LogicalIndex );

    e_ttc_network_errorcode Result = ttc_network_interface_deinit( Config );
    if ( Result == ec_network_OK )
    { Config->Flags.Bits.Initialized = 0; }
}
void ttc_network_init( t_u8 LogicalIndex ) {
    Assert_NETWORK( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_network_config* Config = ttc_network_get_configuration( LogicalIndex );

    e_ttc_network_errorcode Result = ttc_network_interface_init( Config );
    if ( Result == ec_network_OK )
    { Config->Flags.Bits.Initialized = 1; }
}
e_ttc_network_errorcode ttc_network_load_defaults( t_ttc_network_config* Config ) {
    Assert_NETWORK( Config, ttc_assert_origin_auto ); // logical index starts at 1
    //t_ttc_network_config* Config = ttc_network_get_configuration(LogicalIndex);
    t_u8 LogicalIndex = Config->LogicalIndex;
    ttc_memory_set( Config, 0, sizeof( t_ttc_network_config ) );

    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    //Config->PhysicalIndex = ttc_network_logical_2_physical_index(LogicalIndex);

    return ttc_network_interface_load_defaults( Config );
}
t_u8 ttc_network_logical_2_physical_index( t_u8 LogicalIndex ) {
    switch ( LogicalIndex ) {         // determine base register and other low-level configuration data
            #ifdef TTC_NETWORK1
        case 1: return TTC_NETWORK1;
            #endif
            #ifdef TTC_NETWORK2
        case 2: return TTC_NETWORK2;
            #endif
            #ifdef TTC_NETWORK3
        case 3: return TTC_NETWORK3;
            #endif
            #ifdef TTC_NETWORK4
        case 4: return TTC_NETWORK4;
            #endif
            #ifdef TTC_NETWORK5
        case 5: return TTC_NETWORK5;
            #endif
            #ifdef TTC_NETWORK6
        case 6: return TTC_NETWORK6;
            #endif
            #ifdef TTC_NETWORK7
        case 7: return TTC_NETWORK7;
            #endif
            #ifdef TTC_NETWORK8
        case 8: return TTC_NETWORK8;
            #endif
            #ifdef TTC_NETWORK9
        case 9: return TTC_NETWORK9;
            #endif
            #ifdef TTC_NETWORK10
        case 10: return TTC_NETWORK10;
            #endif
        // extend as required

        default: Assert_NETWORK( 0, ttc_assert_origin_auto ); break; // No TTC_NETWORKn defined! Check your makefile.100_board_* file!
    }

    return -1;
}
void ttc_network_prepare() {
    // add your startup code here (Singletasking!)
    ttc_network_interface_prepare();
}
void ttc_network_reset( t_u8 LogicalIndex ) {
    Assert_NETWORK( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_network_config* Config = ttc_network_get_configuration( LogicalIndex );

    ttc_network_interface_reset( Config );
}

// ToDo: additional functions go here...
e_ttc_network_errorcode ttc_network_send_unicast( t_u8 LogicalIndex, ttc_network_address_t Target, const t_u8* Buffer, t_base Amount ) {
    Assert_NETWORK( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_network_config* Config = ttc_network_get_configuration( LogicalIndex );

    return ttc_network_interface_send_direct_message( Config, Target, Buffer, Amount );
}
e_ttc_network_errorcode ttc_network_send_broadcast( t_ttc_network_config* Config, const t_u8* Buffer, t_base Amount ) {
    Assert_NETWORK( Config, ttc_assert_origin_auto ); // logical index starts at 1
//    t_ttc_network_config* Config = ttc_network_get_configuration(LogicalIndex);

    return ttc_network_interface_send_broadcast_message( Config, Buffer, Amount );
}
void ttc_network_receive( t_u8 LogicalIndex ) {
    Assert_NETWORK( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_network_config* Config = ttc_network_get_configuration( LogicalIndex );

    ttc_network_interface_receive( Config );
}
t_ttc_list* ttc_network_get_neighbours( t_u8 LogicalIndex ) {
    Assert_NETWORK( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_network_config* Config = ttc_network_get_configuration( LogicalIndex );

    return ttc_network_interface_get_neighbours( Config );
}
t_rimeaddr ttc_network_address_to_rimeaddr( ttc_network_address_t* Address ) {
    Assert_NETWORK( Address, ttc_assert_origin_auto ); // logical index starts at 1
    ttc_network_address_t TempAddress = *Address;
    t_rimeaddr Dest;
    int i;

    if ( TTC_NETWORK_ADDRESS_SIZE != RIMEADDR_SIZE ) {
        Assert_NETWORK( TTC_NETWORK_ADDRESS_SIZE, ttc_assert_origin_auto );
    }
    else {
        switch ( RIMEADDR_SIZE ) {
            case 1: Dest.u8[0] = TempAddress;
                break;
            case 2: Dest.u8[0] = TempAddress;
                Dest.u8[1] = TempAddress >> 8;
                break;
            case 8: for ( i = 0; i < RIMEADDR_SIZE; i++ ) {
                    Dest.u8[i] = TempAddress;
                    TempAddress = TempAddress >> 8;
                }
                break;
            default: Dest.u8[0] = TempAddress;
                Dest.u8[1] = TempAddress >> 8;
                break;
        }
    }
    return Dest;
}
//}FunctionDefinitions
