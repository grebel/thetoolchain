#ifndef ttc_extensions_H
#define ttc_extensions_H

/** { extensions.h *********************************************************
 *
 *               TheToolChain
 *
 * written 2010-2018 by Gregor Rebel
 *
 * Basic framework template for
 *   - Olimex STM32-P107/ STM32-H107/ STM32-LCD eval board
 *   - StdPeripheralsLibrary CMSIS V3.4.0 (tested)
 *   - GNU toolchain from CodeSourcery G++
 *   - FreeRTOS v7.0.1
 *   - FixPointLib
 *   - uIP TCP/IP-Stack
 *   - Color LCD Panel 320x240
 *   - local/ remote debugging (JTAG-Adapter may be connected to different computer)
 *
 * Coding-style
 *  + gnu99
 *    It's year 2012, get a decent compiler if your's does not comply with this.
 *    arm-none-eabi-gcc-4.4.1 does support gnu99.
 *
 *  + Text Folding
 *    This source makes use of text folding to improve hierarchical readability
 *    It has been written with jEdit + plugin Configurable Fold Handler.
 *    Activate folding in your favourite text-editor for { and }
 *    (fold-start and -end) for best visual experience.
 *
 */
/** Description of ttc_extensions (Do not delete this line!)
 *
 * This driver manages all activated source-code extensions and runs their *_prepare() functions.
 *
}*/
//{ check build environment **********************************************

#ifndef SOURCE_COMPILER
    #  error SOURCE_COMPILER not defined -> activate.050_
#endif
#ifndef TTC_BOARD
    #ifdef TTC_BOARD1
        #define TTC_BOARD TTC_BOARD1
    #else
        #    error TTC_BOARD not defined. Enable exactly one board! (E.g.: enableFeature 110_board_stm32l100c_discovery; activate.490_ttc_board.sh)
    #endif
#endif

//}
//{ Defines/ TypDefs *****************************************************

struct FirstTaskArguments {
    void ( *MainTask )( void* ); // function that will be called as main task
    void* MainArgument;
};

//} Defines
//{ Includes *************************************************************

/**
 * IO definitions
 *
 * define access restrictions to peripheral registers
 *
 * (Copied from core_cm3.h because its required to include stm32f10x.h which is required t include core_cm3.h!)
 */

// Basic set of helper functions
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)

#ifdef EXTENSION_ttc_task
    #include "ttc_task.h"
#endif
#include "ttc_extensions_active.h"

#ifdef EXTENSION_ttc_debug_registers
    #ifdef TARGET_ARCHITECTURE_STM32F1xx
        #include "../register/register_stm32f1xx.h"
    #endif
#endif

//} Includes
//{ Global Variables *****************************************************



//} Global Variables
//{ Function prototypes **************************************************

/** Start up system
 *
 * 1) call start functions of all activated extensions
 * 2) call main_prepare()
 * 3) start multitasking scheduler (if activated)
 *
 */
void ttc_extensions_start( );

//} Function prototypes

#endif //ttc_extensions_H
