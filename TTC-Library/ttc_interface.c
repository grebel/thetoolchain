/** { ttc_interface.c ****************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for interface devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported 
 *  architectures.
 *
 *  See corresponding header file for documentation.
 *
 *  Created from template ttc_device.c revision 27 at 20150310 09:22:28 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_interface.h".
//
#include "ttc_interface.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_INTERFACE_AMOUNT == 0
  #warning No INTERFACE devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of interface devices.
 *
 */
 

// for each initialized device, a pointer to its generic and ste101p definitions is stored
A_define(t_ttc_interface_config*, ttc_interface_configs, TTC_INTERFACE_AMOUNT);

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8 ttc_interface_get_max_index() {
    return TTC_INTERFACE_AMOUNT;
}
t_ttc_interface_config* ttc_interface_get_configuration(t_u8 LogicalIndex) {
    Assert_INTERFACE(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_interface_config* Config = A(ttc_interface_configs, LogicalIndex-1); // will assert if outside array bounds

    if (!Config) {
        Config = A(ttc_interface_configs, LogicalIndex-1) = ttc_heap_alloc_zeroed(sizeof(t_ttc_interface_config));
        Config->Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_interface_load_defaults(LogicalIndex);
    }

    return Config;
}
t_ttc_interface_config* ttc_interface_get_features(t_u8 LogicalIndex) {
    t_ttc_interface_config* Config = ttc_interface_get_configuration(LogicalIndex);
    return _driver_interface_get_features(Config);
}
void ttc_interface_deinit(t_u8 LogicalIndex) {
    Assert_INTERFACE(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_interface_config* Config = ttc_interface_get_configuration(LogicalIndex);
  
    e_ttc_interface_errorcode Result = _driver_interface_deinit(Config);
    if (Result == ec_interface_OK)
      Config->Flags.Bits.Initialized = 0;
}
e_ttc_interface_errorcode ttc_interface_init(t_u8 LogicalIndex) {
    Assert_INTERFACE(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_interface_config* Config = ttc_interface_get_configuration(LogicalIndex);

    e_ttc_interface_errorcode Result = _driver_interface_init(Config);
    if (Result == ec_interface_OK)
      Config->Flags.Bits.Initialized = 1;
    
    return Result;
}
e_ttc_interface_errorcode ttc_interface_load_defaults(t_u8 LogicalIndex) {
    Assert_INTERFACE(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_interface_config* Config = ttc_interface_get_configuration(LogicalIndex);

    if (Config->Flags.Bits.Initialized)
      ttc_interface_deinit(LogicalIndex);
    
    ttc_memory_set( Config, 0, sizeof(t_ttc_interface_config) );

    // load generic default values
    switch (LogicalIndex) { // load type of low-level driver for current architecture
#ifdef TTC_INTERFACE1_DRIVER
    case  1: Config->Architecture = TTC_INTERFACE1_DRIVER; break;
#endif
#ifdef TTC_INTERFACE2_DRIVER
    case  2: Config->Architecture = TTC_INTERFACE2_DRIVER; break;
#endif
#ifdef TTC_INTERFACE3_DRIVER
    case  3: Config->Architecture = TTC_INTERFACE3_DRIVER; break;
#endif
#ifdef TTC_INTERFACE4_DRIVER
    case  4: Config->Architecture = TTC_INTERFACE4_DRIVER; break;
#endif
#ifdef TTC_INTERFACE5_DRIVER
    case  5: Config->Architecture = TTC_INTERFACE5_DRIVER; break;
#endif
#ifdef TTC_INTERFACE6_DRIVER
    case  6: Config->Architecture = TTC_INTERFACE6_DRIVER; break;
#endif
#ifdef TTC_INTERFACE7_DRIVER
    case  7: Config->Architecture = TTC_INTERFACE7_DRIVER; break;
#endif
#ifdef TTC_INTERFACE8_DRIVER
    case  8: Config->Architecture = TTC_INTERFACE8_DRIVER; break;
#endif
#ifdef TTC_INTERFACE9_DRIVER
    case  9: Config->Architecture = TTC_INTERFACE9_DRIVER; break;
#endif
#ifdef TTC_INTERFACE10_DRIVER
    case 10: Config->Architecture = TTC_INTERFACE10_DRIVER; break;
#endif
    default: ttc_assert_halt_origin(ec_interface_InvalidImplementation); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }
    Assert_INTERFACE( (Config->Architecture > ta_interface_None) && (Config->Architecture < ta_interface_unknown), ttc_assert_origin_auto); // architecture not set properly! Check makefile for TTC_INTERFACE<n>_DRIVER and compare with e_ttc_interface_architecture 

    Config->LogicalIndex = LogicalIndex;
    Config->PhysicalIndex = ttc_interface_logical_2_physical_index(LogicalIndex);
    
    //Insert additional generic default values here ...

    return _driver_interface_load_defaults(Config);    
}
t_u8 ttc_interface_logical_2_physical_index(t_u8 LogicalIndex) {
  
  switch (LogicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_INTERFACE1
           case 1: return TTC_INTERFACE1;
#endif
#ifdef TTC_INTERFACE2
           case 2: return TTC_INTERFACE2;
#endif
#ifdef TTC_INTERFACE3
           case 3: return TTC_INTERFACE3;
#endif
#ifdef TTC_INTERFACE4
           case 4: return TTC_INTERFACE4;
#endif
#ifdef TTC_INTERFACE5
           case 5: return TTC_INTERFACE5;
#endif
#ifdef TTC_INTERFACE6
           case 6: return TTC_INTERFACE6;
#endif
#ifdef TTC_INTERFACE7
           case 7: return TTC_INTERFACE7;
#endif
#ifdef TTC_INTERFACE8
           case 8: return TTC_INTERFACE8;
#endif
#ifdef TTC_INTERFACE9
           case 9: return TTC_INTERFACE9;
#endif
#ifdef TTC_INTERFACE10
           case 10: return TTC_INTERFACE10;
#endif
      // extend as required
      
      default: break;
    }
    
  Assert_INTERFACE(0, ttc_assert_origin_auto); // No TTC_INTERFACEn defined! Check your makefile.100_board_* file!
  return -1; // program flow should never go here
}
t_u8 ttc_interface_physical_2_logical_index(t_u8 PhysicalIndex) {
  
  switch (PhysicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_INTERFACE1
           case TTC_INTERFACE1: return 1;
#endif
#ifdef TTC_INTERFACE2
           case TTC_INTERFACE2: return 2;
#endif
#ifdef TTC_INTERFACE3
           case TTC_INTERFACE3: return 3;
#endif
#ifdef TTC_INTERFACE4
           case TTC_INTERFACE4: return 4;
#endif
#ifdef TTC_INTERFACE5
           case TTC_INTERFACE5: return 5;
#endif
#ifdef TTC_INTERFACE6
           case TTC_INTERFACE6: return 6;
#endif
#ifdef TTC_INTERFACE7
           case TTC_INTERFACE7: return 7;
#endif
#ifdef TTC_INTERFACE8
           case TTC_INTERFACE8: return 8;
#endif
#ifdef TTC_INTERFACE9
           case TTC_INTERFACE9: return 9;
#endif
#ifdef TTC_INTERFACE10
           case TTC_INTERFACE10: return 10;
#endif
      // extend as required
      
      default: break;
    }
    
  Assert_INTERFACE(0, ttc_assert_origin_auto); // No TTC_INTERFACEn defined! Check your makefile.100_board_* file!
  return -1; // program flow should never go here
}
void ttc_interface_prepare() {
  // add your startup code here (Singletasking!)
  _driver_interface_prepare();
}
void ttc_interface_reset(t_u8 LogicalIndex) {
    Assert_INTERFACE(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_interface_config* Config = ttc_interface_get_configuration(LogicalIndex);

    _driver_interface_reset(Config);    
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_interface(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
