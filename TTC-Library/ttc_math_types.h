/** { ttc_math_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for MATH device.
 *  Structures, Enums and Defines being required by both, high- and low-level math.
 *  This file does not provide function declarations!
 *
 *  Created from template ttc_device_types.h revision 34 at 20150724 07:49:04 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_MATH_TYPES_H
#define TTC_MATH_TYPES_H

//{ Includes *************************************************************
//
// _types.h Header files mayshould include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_math.h" or "ttc_math.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_math_software_float
    #include "math/math_software_float_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_math_software_double
    #include "math/math_software_double_types.h" // automatically added by ./create_DeviceDriver.pl
#endif

//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

/* Note: Must typecast constants to ttm_number to avoid strange effects when using float type
 *       as these long constants are treated as double by default.
 *       Example:
 *       float X = PI;
 *       if (X == PI) // will not be true if PI was not typecasted to float!
 *       { ... }
 */
#define PI     ( (ttm_number) 3.141592653589793 )  // circle diameter constant (l = PI * d)
#define EULER  ( (ttm_number) 2.71828182846     )  // Euler number
#define GOLDEN ( (ttm_number) 1.61803398875     )  // golden cut

// these are easier to find using autocompletion (in qtcreator just type tmcp and tab-key for TTC_MATH_CONST_PI)
#define TTC_MATH_CONST_PI     PI
#define TTC_MATH_CONST_EULER  EULER
#define TTC_MATH_CONST_GOLDEN GOLDEN
#ifndef TTC_MATH_CONST_NAN
    #define TTC_MATH_CONST_NAN -1  // using best possible value
#endif

//} Defines
//{ Static Configuration *************************************************

//}
/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_MATH 0        # disable default asserts for math driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_MATH_EXTRA 1  # enable extra asserts for math driver
 *
 */
#ifndef TTC_ASSERT_MATH    // any previous definition set (Makefile)?
    #define TTC_ASSERT_MATH 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_MATH == 1)  // use Assert()s in MATH code (somewhat slower but alot easier to debug)
    #define Assert_MATH(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_MATH_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_MATH_Readable(Address, Origin) Assert_Readable(Address, Origin)
    #define VOLATIL_MATH                          volatile
#else  // use no default Assert()s in MATH code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_MATH(Condition, Origin)
    #define Assert_MATH_Writable(Address, Origin)
    #define Assert_MATH_Readable(Address, Origin)
    #define VOLATIL_MATH
#endif


#ifndef TTC_ASSERT_MATH_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_MATH_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_MATH_EXTRA == 1)  // use Assert()s in MATH code (somewhat slower but alot easier to debug)
    #define Assert_MATH_EXTRA(Condition, Origin) Assert( ((Condition) != 0), Origin)
#else  // use no extra Assert()s in MATH code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_MATH_EXTRA(Condition, Origin)
#endif
//}
/** { Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_math_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef ttm_number
//#  warning Missing low-level definition for ttm_number (using default). Low-level driver should define it as

typedef struct { // msf_number_t structure of single precision float according to IEEE754 binary32
    unsigned Fraction : 23;
    unsigned Exponent :  8;
    unsigned Sign     :  1;
} t_math_float;

// ttm_number is base data format and required by ttc_math_types.h
#  define ttm_number                    float                                        // general type to use for variables
#  define ttm_number_t                  t_math_float                                  // special type allowing manual composition of special numbers
#  define TTC_MATH_CONST_MAX_VALUE      0x7fffffff                                   // maximum positive value
#  define TTC_MATH_CONST_MIN_VALUE      0xffffffff                                   // maximum negative value
#  define TTC_MATH_CONST_PLUS_INFINITY  TTC_MATH_CONST_MAX_VALUE                     // cheap approximation for +infinity
#  define TTC_MATH_CONST_MINUS_INFINITY TTC_MATH_CONST_MIN_VALUE                     // cheap approximation for -infinity
#  define TTC_MATH_CONST_SMALLEST       0b01111111100000000000000000000001 // smallest positive value

#endif

// Check existence of required low-level defines
#ifndef TTC_MATH_CONST_MAX_VALUE
    #  error Missing definition of TTC_MATH_CONST_MAX_VALUE. Low-Level driver must define it as maximum positive number representable in current ttm_number!
#endif
#ifndef TTC_MATH_CONST_MIN_VALUE
    #  error Missing definition of TTC_MATH_CONST_MIN_VALUE. Low-Level driver must define it as maximum negative number representable in current ttm_number!
#endif
#ifndef TTC_MATH_CONST_PLUS_INFINITY
    #  error Missing definition of TTC_MATH_CONST_PLUS_INFINITY. Low-Level driver must define it as a symbolic value for +infinity (e.g. maximum positive value)!
#endif
#ifndef TTC_MATH_CONST_MINUS_INFINITY
    #  error Missing definition of TTC_MATH_CONST_MINUS_INFINITY. Low-Level driver must define it as a symbolic value for -infinity (e.g. maximum negative value)!
#endif
#ifndef TTC_MATH_CONST_SMALLEST
    #  error Missing definition of TTC_MATH_CONST_SMALLEST. Low-Level driver must define it as smallest supported value!
#endif
#ifndef ttm_number_t
    #  error Missing definition of ttm_number_t. Low-Level driver must define it as structure of used number format (see example above)!
#endif

//}

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_math_precision        precision setting for different approximation algorithms (e.g. ttc_math_arcsin() )
    tmp_None = 0,   // precision not set
    tmp_100m,       // calculations approximated +-0.1   = 100 milli
    tmp_10m,        // calculations approximated +-0.01  =  10 milli
    tmp_1m,         // calculations approximated +-0.001 =   1 milli
    tmp_unknown     // invalid values below

} e_ttc_math_precision;
typedef enum {   // e_ttc_math_errorcode       return codes of MATH devices
    E_ttc_math_error_OK = 0,

    // other warnings go here..

    E_ttc_math_error_ERROR,                  // general failure
    E_ttc_math_error_NULL,                   // NULL pointer not accepted
    E_ttc_math_error_DeviceNotFound,         // corresponding device could not be found
    E_ttc_math_error_InvalidConfiguration,   // sanity check of device configuration failed
    E_ttc_math_error_InvalidImplementation,  // your code does not behave as expected
    E_ttc_math_error_DivisionByZero,         // a value should be divided by zero
    E_ttc_math_error_NotANumber,             // result cannot be computed (invalid arguments?)
    E_ttc_math_error_Overflow,               // overflow occured during calculation

    // error codes from ttc_math_lateration_2d()
    E_ttc_math_error_lateration2d,                 // unspecific error during ttc_math_lateration_2d()
    E_ttc_math_error_lateration2d_distances_small, // given distances are way too small


    // error codes from ttc_math_lateration_3d()
    E_ttc_math_error_lateration3d,                // unspecific error during ttc_math_lateration_3d()
    E_ttc_math_error_lateration3d_insufficient,   // insufficient input data
    E_ttc_math_error_lateration3d_colinear_f2_f3, // insufficient input data: F3 must not be colinear with F2!
    E_ttc_math_error_lateration3d_distance_on,    // distance from laterated position to origin (0,0,0) does not match given DistanceON value
    E_ttc_math_error_lateration3d_distance_f2n,   // distance from laterated position to fixed point F2 does not match given DistanceF2N value
    E_ttc_math_error_lateration3d_distance_f3n,   // distance from laterated position to fixed point F3 does not match given DistanceF3N value

    // other failures go here..

    E_ttc_math_error_unknown                // no valid errorcodes past this entry
} e_ttc_math_errorcode;
typedef enum {   // e_ttc_math_architecture    types of architectures supported by MATH driver
    ta_math_None = 0,       // no architecture selected


    ta_math_software_float, // automatically added by ./create_DeviceDriver.pl
    ta_math_software_double, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_math_unknown        // architecture not supported
} e_ttc_math_architecture;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

// integer type vectors -------------------------------------------------------------

typedef struct s_ttc_math_int_vector2d_xy { // two dimensional vector using cartesian coordinates
    t_base_signed X;
    t_base_signed Y;
} t_ttc_math_int_vector2d_xy;

typedef struct s_ttc_math_int_vector2d_rz { // two dimensional vector using polar coordinates
    t_base_signed R;   // radius from (0,0,0)
    t_base_signed Az;  // rotation angle around Z-axis (radians)
} t_ttc_math_int_vector2d_rz;

typedef struct s_ttc_math_int_vector3d_xyz { // three dimensional vector using cartesian coordinates
    t_base_signed X;
    t_base_signed Y;
    t_base_signed Z;
} t_ttc_math_int_vector3d_xyz;

typedef struct s_ttc_math_int_vector3d_ryz { // three dimensional vector using polar coordinates
    t_base_signed R;   // radius from (0,0,0)
    t_base_signed Ay;  // rotation angle around Y-axis (radians)
    t_base_signed Az;  // rotation angle around Z-axis (radians)
} t_ttc_math_int_vector3d_ryz;


// floating / fixed point type vectors ----------------------------------------------

typedef struct s_ttc_math_vector2d_xy { // two dimensional vector using cartesian coordinates
    ttm_number X;
    ttm_number Y;
} t_ttc_math_vector2d_xy;

typedef struct s_ttc_math_vector2d_rz { // two dimensional vector using polar coordinates
    ttm_number R;   // radius from (0,0,0)
    ttm_number Az;  // rotation angle around Z-axis (radians)
} t_ttc_math_vector2d_rz;

typedef struct s_ttc_math_vector3d_xyz { // three dimensional vector using cartesian coordinates
    ttm_number X;
    ttm_number Y;
    ttm_number Z;
} t_ttc_math_vector3d_xyz;

typedef struct s_ttc_math_vector3d_ryz { // three dimensional vector using polar coordinates
    ttm_number R;   // radius from (0,0,0)
    ttm_number Ay;  // rotation angle around Y-axis (radians)
    ttm_number Az;  // rotation angle around Z-axis (radians)
} t_ttc_math_vector3d_ryz;

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_MATH_TYPES_H
