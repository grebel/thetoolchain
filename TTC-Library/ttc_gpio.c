/** { ttc_gpio.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level interface for gpio device.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 21 at 20140204 13:46:55 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_gpio.h"
#include "ttc_heap.h"
#include "ttc_memory.h"
#include "ttc_interrupt.h"

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of gpio devices.
 *
 */


//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

void ttc_gpio_get_configuration( e_ttc_gpio_pin PortPin, t_ttc_gpio_config* Configuration ) {
    Assert_GPIO( PortPin, ttc_assert_origin_auto );     // Port must not be NULL

    ttc_interrupt_critical_begin();
    ttc_memory_set( Configuration, 0, sizeof( t_ttc_gpio_config ) );
    if ( ( PortPin == 0 ) || ( PortPin >= E_ttc_gpio_pin_unknown ) ) // invalid PortPin
    { return; }

    _driver_gpio_get_configuration( PortPin, Configuration );
    Configuration->State = ttc_gpio_get( PortPin );

    ttc_interrupt_critical_end();
}
void ttc_gpio_deinit( e_ttc_gpio_pin PortPin ) {
    Assert_GPIO( PortPin, ttc_assert_origin_auto );  // Port must not be NULL
    ttc_interrupt_critical_begin();
    _driver_gpio_deinit( PortPin );
    ttc_interrupt_critical_end();
}
void ttc_gpio_init( e_ttc_gpio_pin PortPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ) {
    Assert_GPIO( PortPin && ( PortPin < E_ttc_gpio_pin_unknown ), ttc_assert_origin_auto );  // invalid PortPin
    Assert_GPIO( Mode  && ( Mode  < E_ttc_gpio_mode_unknown ), ttc_assert_origin_auto );
    Assert_GPIO( Speed && ( Speed < E_ttc_gpio_speed_unknown ), ttc_assert_origin_auto );

    ttc_interrupt_critical_begin();
    _driver_gpio_init( PortPin, Mode, Speed );
    ttc_interrupt_critical_end();
}
void ttc_gpio_parallel08_init( t_u8 LogicalIndex, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ) {
    switch ( LogicalIndex ) {
#ifdef TTC_GPIO_PARALLEL08_1_BANK
        case 1: _driver_gpio_parallel08_init( TTC_GPIO_PARALLEL08_1_BANK, TTC_GPIO_PARALLEL08_1_FIRSTPIN, Mode, Speed ); break;
#endif
#ifdef TTC_GPIO_PARALLEL08_2_BANK
        case 2: _driver_gpio_parallel08_init( TTC_GPIO_PARALLEL08_2_BANK, TTC_GPIO_PARALLEL08_2_FIRSTPIN, Mode, Speed ); break;
#endif
#ifdef TTC_GPIO_PARALLEL08_3_BANK
        case 3: _driver_gpio_parallel08_init( TTC_GPIO_PARALLEL08_3_BANK, TTC_GPIO_PARALLEL08_3_FIRSTPIN, Mode, Speed ); break;
#endif
#ifdef TTC_GPIO_PARALLEL08_4_BANK
        case 4: _driver_gpio_parallel08_init( TTC_GPIO_PARALLEL08_4_BANK, TTC_GPIO_PARALLEL08_4_FIRSTPIN, Mode, Speed ); break;
#endif
#ifdef TTC_GPIO_PARALLEL08_5_BANK
        case 5: _driver_gpio_parallel08_init( TTC_GPIO_PARALLEL08_5_BANK, TTC_GPIO_PARALLEL08_5_FIRSTPIN, Mode, Speed ); break;
#endif
#ifdef TTC_GPIO_PARALLEL08_6_BANK
        case 6: _driver_gpio_parallel08_init( TTC_GPIO_PARALLEL08_6_BANK, TTC_GPIO_PARALLEL08_6_FIRSTPIN, Mode, Speed ); break;
#endif
#ifdef TTC_GPIO_PARALLEL08_7_BANK
        case 7: _driver_gpio_parallel08_init( TTC_GPIO_PARALLEL08_7_BANK, TTC_GPIO_PARALLEL08_7_FIRSTPIN, Mode, Speed ); break;
#endif
#ifdef TTC_GPIO_PARALLEL08_8_BANK
        case 8: _driver_gpio_parallel08_init( TTC_GPIO_PARALLEL08_8_BANK, TTC_GPIO_PARALLEL08_8_FIRSTPIN, Mode, Speed ); break;
#endif
#ifdef TTC_GPIO_PARALLEL08_9_BANK
        case 9: _driver_gpio_parallel08_init( TTC_GPIO_PARALLEL08_9_BANK, TTC_GPIO_PARALLEL08_9_FIRSTPIN, Mode, Speed ); break;
#endif
#ifdef TTC_GPIO_PARALLEL08_10_BANK
        case 10: _driver_gpio_parallel08_init( TTC_GPIO_PARALLEL08_10_BANK, TTC_GPIO_PARALLEL08_10_FIRSTPIN, Mode, Speed ); break;
#endif
        default: Assert_GPIO( 0, ttc_assert_origin_auto ); break; // invalid or unconfigured parallel port selected. Check implementation of caller and current board makefile!
    }
}
void ttc_gpio_prepare() {
    // add your startup code here (Singletasking!)
    _driver_gpio_prepare();
}
void ttc_gpio_alternate_function( e_ttc_gpio_pin PortPin, t_u8 AlternateFunction ) {
    Assert_GPIO( PortPin, ttc_assert_origin_auto );
    ttc_interrupt_critical_begin();
    _driver_gpio_alternate_function( PortPin, AlternateFunction );
    ttc_interrupt_critical_end();
}

#if (TTC_ASSERT_GPIO_EXTRA == 1) // extra safe parallel port *_put() implementations (verify value being written to port)

/*{ safe ttc_gpio_parallel08_*_put() functions
 *
 * Each function will become active when it's corresponding parallel port has
 * been configured in makefile via two constants:
 * - TTC_GPIO_PARALLEL08_*_BANK
 * - TTC_GPIO_PARALLEL08_*_FIRSTPIN
 *
 * See description of parallel ports at top of ttc_gpio.h for more details!
 */

#  ifdef TTC_GPIO_PARALLEL08_1_BANK
void ttc_gpio_parallel08_1_put( t_u8 Value ) {
    _driver_gpio_parallel08_1_put( Value );
    t_u8 Actual = _driver_gpio_parallel08_1_get();

    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel08_init( 1, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 8-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL08_2_BANK
void ttc_gpio_parallel08_2_put( t_u8 Value ) {
    _driver_gpio_parallel08_2_put( Value );
    t_u8 Actual = _driver_gpio_parallel08_2_get();

    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel08_init( 2, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 8-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL08_3_BANK
void ttc_gpio_parallel08_3_put( t_u8 Value ) {
    _driver_gpio_parallel08_3_put( Value );
    t_u8 Actual = _driver_gpio_parallel08_3_get();

    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel08_init( 3, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 8-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL08_4_BANK
void ttc_gpio_parallel08_4_put( t_u8 Value ) {
    _driver_gpio_parallel08_4_put( Value );
    t_u8 Actual = _driver_gpio_parallel08_4_get();

    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel08_init( 4, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 8-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL08_5_BANK
void ttc_gpio_parallel08_5_put( t_u8 Value ) {
    _driver_gpio_parallel08_5_put( Value );
    t_u8 Actual = _driver_gpio_parallel08_5_get();

    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel08_init( 5, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 8-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL08_6_BANK
void ttc_gpio_parallel08_6_put( t_u8 Value ) {
    _driver_gpio_parallel08_6_put( Value );
    t_u8 Actual = _driver_gpio_parallel08_6_get();

    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel08_init( 6, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 8-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL08_7_BANK
void ttc_gpio_parallel08_7_put( t_u8 Value ) {
    _driver_gpio_parallel08_7_put( Value );
    t_u8 Actual = _driver_gpio_parallel08_7_get();

    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel08_init( 7, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 8-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL08_8_BANK
void ttc_gpio_parallel08_8_put( t_u8 Value ) {
    _driver_gpio_parallel08_8_put( Value );
    t_u8 Actual = _driver_gpio_parallel08_8_get();

    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel08_init( 8, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 8-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL08_9_BANK
void ttc_gpio_parallel08_9_put( t_u8 Value ) {
    _driver_gpio_parallel08_9_put( Value );
    t_u8 Actual = _driver_gpio_parallel08_9_get();

    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel08_init( 9, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 8-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL08_10_BANK
void ttc_gpio_parallel08_10_put( t_u8 Value ) {
    _driver_gpio_parallel08_10_put( Value );
    t_u8 Actual = _driver_gpio_parallel08_10_get();

    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel08_init( 10, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 8-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
//}
/*{ safe ttc_gpio_parallel16_*_put() functions
 *
 * Each function will become active when it's corresponding parallel port has
 * been configured in makefile via one constant:
 * - TTC_GPIO_PARALLEL16_*_BANK
 *
 * See description of parallel ports at top of ttc_gpio.h for more details!
 */

#  ifdef TTC_GPIO_PARALLEL16_1_BANK
void ttc_gpio_parallel16_1_put( t_u16 Value ) {
    _driver_gpio_parallel16_1_put( Value );
    t_u16 Actual = _driver_gpio_parallel16_1_get();
    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel16_init( 1, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 8-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL16_2_BANK
void ttc_gpio_parallel16_2_put( t_u16 Value ) {
    _driver_gpio_parallel16_2_put( Value );
    t_u16 Actual = _driver_gpio_parallel16_2_get();
    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel16_init( 2, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 16-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL16_3_BANK
void ttc_gpio_parallel16_3_put( t_u16 Value ) {
    _driver_gpio_parallel16_3_put( Value );
    t_u16 Actual = _driver_gpio_parallel16_3_get();
    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel16_init( 3, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 16-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL16_4_BANK
void ttc_gpio_parallel16_4_put( t_u16 Value ) {
    _driver_gpio_parallel16_4_put( Value );
    t_u16 Actual = _driver_gpio_parallel16_4_get();
    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel16_init( 4, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 16-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL16_5_BANK
void ttc_gpio_parallel16_5_put( t_u16 Value ) {
    _driver_gpio_parallel16_5_put( Value );
    t_u16 Actual = _driver_gpio_parallel16_5_get();
    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel16_init( 5, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 16-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL16_6_BANK
void ttc_gpio_parallel16_6_put( t_u16 Value ) {
    _driver_gpio_parallel16_6_put( Value );
    t_u16 Actual = _driver_gpio_parallel16_6_get();
    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel16_init( 6, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 16-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL16_7_BANK
void ttc_gpio_parallel16_7_put( t_u16 Value ) {
    _driver_gpio_parallel16_7_put( Value );
    t_u16 Actual = _driver_gpio_parallel16_7_get();
    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel16_init( 7, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 16-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL16_8_BANK
void ttc_gpio_parallel16_8_put( t_u16 Value ) {
    _driver_gpio_parallel16_8_put( Value );
    t_u16 Actual = _driver_gpio_parallel16_8_get();
    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel16_init( 8, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 16-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL16_9_BANK
void ttc_gpio_parallel16_9_put( t_u16 Value ) {
    _driver_gpio_parallel16_9_put( Value );
    t_u16 Actual = _driver_gpio_parallel16_9_get();
    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel16_init( 9, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 16-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif
#  ifdef TTC_GPIO_PARALLEL16_10_BANK
void ttc_gpio_parallel16_10_put( t_u16 Value ) {
    _driver_gpio_parallel16_10_put( Value );
    t_u16 Actual = _driver_gpio_parallel16_10_get();
    if ( Value != Actual ) { // written data differs to voltage levels: Set safe state + Assert
        ttc_gpio_parallel16_init( 10, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // 16-bit value being written to port differs from actual voltage levels. Check if there is an electrical issue or disable TTC_ASSERT_GPIO_EXTRA!
    }
}
#  endif

#endif

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
//{ GDB Function prototypes ******************************************

#ifdef EXTENSION_basic_extensions_optimize_1_debug

volatile t_ttc_gpio_config* ttc_gpio_get_configuration_gdb( e_ttc_gpio_pin PortPin ) {
    Assert_GPIO( PortPin, ttc_assert_origin_auto );     // Port must not be NULL

    static volatile t_ttc_gpio_config* Configuration = NULL;
    if ( !Configuration )
    { Configuration = ttc_heap_alloc( sizeof( *Configuration ) ); }

    ttc_gpio_get_configuration( PortPin, ( t_ttc_gpio_config* ) Configuration );

    // Returning pointer to local variable.
    // Use with caution (e.g. only as directed in function description)!
    return Configuration;
}

#endif

//}GDB Function prototypes
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_gpio(e_ttc_gpio_pin PortPin) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
