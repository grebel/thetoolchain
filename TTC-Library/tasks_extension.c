/*{ tasks_extension.c ************************************************
 
 This file contains additional code to extend tasks management of FreeRTOS.
 It is intended to be included from tasks.c 
 
 How to use (already done by TheToolChain):
   Append a "#include tasks_extension.c" at the end of 
     additionals/300_scheduler_freertos/Source/tasks.c
     
   Append a "#include tasks_extension.h" at the end of
     additionals/300_scheduler_freertos/Source/tasks.h
}*/

#include "tasks_extension.h"

//{ Function definitions *************************************************

#if ( ( configUSE_TRACE_FACILITY == 1 ) || ( INCLUDE_uxTaskGetStackHighWaterMark == 1 ) )
xTaskHandle getTaskHandle(unsigned portBASE_TYPE TaskIndex) {
    xTaskHandle FoundTaskHandle=0;
    
    unsigned portBASE_TYPE uxQueue=uxTopUsedPriority + ( unsigned portBASE_TYPE ) 1U;

    // algorithm is derived from tasks.c from FreeRTOS 7.1.0
    do
    {
        if (TaskIndex < 1) break;
        uxQueue--;
        if( listLIST_IS_EMPTY( &( pxReadyTasksLists[ uxQueue ] ) ) == pdFALSE )
        {
            //prvListTaskWithinSingleList( pcWriteBuffer, ( xList * ) &( pxReadyTasksLists[ uxQueue ] ), tskREADY_CHAR );
            xList* pxList=( xList * ) &( pxReadyTasksLists[ uxQueue ] );

            volatile tskTCB *pxNextTCB, *pxFirstTCB;
            unsigned short usStackRemaining;

            listGET_OWNER_OF_NEXT_ENTRY( pxFirstTCB, pxList );
            do
            {
                listGET_OWNER_OF_NEXT_ENTRY( pxNextTCB, pxList );
//X #if ( portSTACK_GROWTH > 0 )
//X                 {
//X                     usStackRemaining=usTaskCheckFreeStackSpace( ( unsigned char * ) pxNextTCB->pxEndOfStack );
//X                 }
//X #else
//X                 {
//X                     usStackRemaining=usTaskCheckFreeStackSpace( ( unsigned char * ) pxNextTCB->pxStack );
//X                 }
//X #endif

                //X sprintf( pcStatusString, ( char * ) "%s\t\t%c\t%u\t%u\t%u\r\n", pxNextTCB->pcTaskName, cStatus, ( unsigned int ) pxNextTCB->uxPriority, usStackRemaining, ( unsigned int ) pxNextTCB->uxTCBNumber );
                //X strcat( ( char * ) pcWriteBuffer, ( char * ) pcStatusString );
                TaskIndex--;
                if (TaskIndex < 1) break;
            } while( pxNextTCB != pxFirstTCB );
        }
    } while( uxQueue > ( unsigned short ) tskIDLE_PRIORITY );

    return FoundTaskHandle;
}
#endif

//}FunctionDefinitions
