/** { ttc_gfx_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for GFX device.
 *  Structures, Enums and Defines being required by both, high- and low-level gfx.
 *
 *  Created from template ttc_device_types.h revision 22 at 20140313 10:58:25 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_GFX_TYPES_H
#define TTC_GFX_TYPES_H

//{ Includes *************************************************************

#include "ttc_basic_types.h"
#ifdef EXTENSION_gfx_ili93xx
    #include "gfx/gfx_ili93xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_gfx_image
    #include "gfx/gfx_image_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#include "ttc_font_types.h"
#ifdef EXTENSION_gfx_update
    #include "gfx/gfx_update_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************



//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

// TTC_GFXn has to be defined as constant by makefile.100_board_*
#ifdef TTC_GFX5
    #ifndef TTC_GFX4
        #error TTC_GFX5 is defined, but not TTC_GFX4 - all lower TTC_GFXn must be defined!
    #endif
    #ifndef TTC_GFX3
        #error TTC_GFX5 is defined, but not TTC_GFX3 - all lower TTC_GFXn must be defined!
    #endif
    #ifndef TTC_GFX2
        #error TTC_GFX5 is defined, but not TTC_GFX2 - all lower TTC_GFXn must be defined!
    #endif
    #ifndef TTC_GFX1
        #error TTC_GFX5 is defined, but not TTC_GFX1 - all lower TTC_GFXn must be defined!
    #endif

    #define TTC_GFX_AMOUNT 5
#else
    #ifdef TTC_GFX4
        #define TTC_GFX_AMOUNT 4

        #ifndef TTC_GFX3
            #error TTC_GFX5 is defined, but not TTC_GFX3 - all lower TTC_GFXn must be defined!
        #endif
        #ifndef TTC_GFX2
            #error TTC_GFX5 is defined, but not TTC_GFX2 - all lower TTC_GFXn must be defined!
        #endif
        #ifndef TTC_GFX1
            #error TTC_GFX5 is defined, but not TTC_GFX1 - all lower TTC_GFXn must be defined!
        #endif
    #else
        #ifdef TTC_GFX3

            #ifndef TTC_GFX2
                #error TTC_GFX5 is definttc_gfx_cursor_seted, but not TTC_GFX2 - all lower TTC_GFXn must be defined!
            #endif
            #ifndef TTC_GFX1
                #error TTC_GFX5 is defined, but not TTC_GFX1 - all lower TTC_GFXn must be defined!
            #endif

            #define TTC_GFX_AMOUNT 3
        #else
            #ifdef TTC_GFX2

                #ifndef TTC_GFX1
                    #error TTC_GFX5 is defined, but not TTC_GFX1 - all lower TTC_GFXn must be defined!
                #endif

                #define TTC_GFX_AMOUNT 2
            #else
                #ifdef TTC_GFX1
                    #define TTC_GFX_AMOUNT 1
                #else
                    #define TTC_GFX_AMOUNT 0
                #endif
            #endif
        #endif
    #endif
#endif

// check static configuration for mandatory defines
#ifndef TTC_GFX_ROTATION_CLOCKWISE
    #define TTC_GFX_ROTATION_CLOCKWISE 0 // (define as 0,90,180 or 270)
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 0
    #define TTC_GFX_ROTATION_CLOCKWISE_OK 1
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 90
    #define TTC_GFX_ROTATION_CLOCKWISE_OK 1
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 180
    #define TTC_GFX_ROTATION_CLOCKWISE_OK 1
#endif
#if TTC_GFX_ROTATION_CLOCKWISE == 270
    #define TTC_GFX_ROTATION_CLOCKWISE_OK 1
#endif
#ifndef TTC_GFX_ROTATION_CLOCKWISE_OK
    #  error Wrong definition for TTC_GFX_ROTATION_CLOCKWISE (allowed values: 0,90,180,270)
#endif

/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_GFX 0        # disable default asserts for GFX driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_GFX_EXTRA 1  # enable extra asserts for GFX driver
 *
 */
#ifndef TTC_ASSERT_GFX    // any previous definition set (Makefile)?
    #define TTC_ASSERT_GFX 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_GFX == 1)  // use Assert()s in GFX code (somewhat slower but alot easier to debug)
    #define Assert_GFX(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_GFX_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_GFX_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in GFX code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_GFX(Condition, Origin)
    #define Assert_GFX_Writable(Address, Origin)
    #define Assert_GFX_Readable(Address, Origin)
#endif


#ifndef TTC_ASSERT_GFX_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_GFX_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_GFX_EXTRA == 1)  // use Assert()s in GFX code (somewhat slower but alot easier to debug)
    #define Assert_GFX_EXTRA(Condition, Origin)         Assert( ((Condition) != 0), Origin)
    #define Assert_GFX_EXTRA_Writable(Address, Origin)  Assert_Writable(Address, Origin)
    #define Assert_GFX_EXTRA_Readable(Address, Origin)  Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in GFX code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_GFX_EXTRA(Condition, Origin)
    #define Assert_GFX_EXTRA_Writable(Address, Origin)
    #define Assert_GFX_EXTRA_Readable(Address, Origin)
#endif
//}

/** Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_gfx_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_gfx_architecture
    #warning Missing low-level definition for t_ttc_gfx_architecture (using default)
    #define t_ttc_gfx_architecture void
#endif

//}Static Configuration

// Color defines *********************************************

#define TTC_GFX_COLOR24_BLACK   0x000000
#define TTC_GFX_COLOR24_SILVER  0xC0C0C0
#define TTC_GFX_COLOR24_GRAY    0x808080
#define TTC_GFX_COLOR24_WHITE   0xFFFFFF
#define TTC_GFX_COLOR24_MAROON  0x800000
#define TTC_GFX_COLOR24_RED     0xFF0000
#define TTC_GFX_COLOR24_PURPLE  0x800080
#define TTC_GFX_COLOR24_FUCHSIA 0xFF00FF
#define TTC_GFX_COLOR24_GREEN   0x008000
#define TTC_GFX_COLOR24_LIME    0x00FF00
#define TTC_GFX_COLOR24_OLIVE   0x808000
#define TTC_GFX_COLOR24_YELLOW  0xFFFF00
#define TTC_GFX_COLOR24_NAVY    0x000080
#define TTC_GFX_COLOR24_BLUE    0x0000FF
#define TTC_GFX_COLOR24_TEAL    0x008080
#define TTC_GFX_COLOR24_AQUA    0x00FFFF

// Structures ************************************************

//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_gfx_physical_index (starts at zero!)
    //
    // You may use this enum to assign physical index to logical TTC_GFXn constants in your makefile
    //
    // E.g.: Defining physical device #3 of gfx as first logical device TTC_GFX1
    //       COMPILE_OPTS += -DTTC_GFX1=INDEX_GFX3
    //
    INDEX_GFX1,
    INDEX_GFX2,
    INDEX_GFX3,
    INDEX_GFX4,
    INDEX_GFX5,
    INDEX_GFX6,
    INDEX_GFX7,
    INDEX_GFX8,
    INDEX_GFX9,
    // add more if needed

    INDEX_GFX_ERROR
} e_ttc_gfx_physical_index;


//hier eine enum mit den
typedef enum {    // e_ttc_gfx_errorcode     return codes of GFX devices
    ec_gfx_OK = 0,

    // other warnings go here..

    ec_gfx_ERROR,                  // general failure
    ec_gfx_NULL,                   // NULL pointer not accepted
    ec_gfx_DeviceNotFound,         // corresponding device could not be found
    ec_gfx_InvalidArgument,        // invalid function argument given
    ec_gfx_InvalidImplementation,  // internal self-test failed
    ec_gfx_InvalidConfiguration,   // sanity check of device configuration failed
    ec_gfx_NotImplemented,         // functionality not yet implemented

    // other failures go here..

    ec_gfx_unknown                // no valid errorcodes past this entry
} e_ttc_gfx_errorcode;
typedef enum {    // e_ttc_gfx_architecture  types of architectures supported by GFX driver
    ta_gfx_None,           // no architecture selected
    ta_gfx_ili93xx, // automatically added by ./create_DeviceDriver.pl
    ta_gfx_image, // automatically added by ./create_DeviceDriver.pl
    ta_gfx_update, // automatically added by ./create_DeviceDriver.pl
    E_ttc_gfx_architecture_ili93xx, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_gfx_unknown        // architecture not supported
} e_ttc_gfx_architecture;





typedef struct s_ttc_gfx_config { //         architecture independent configuration data
    // Fields in this struc are ordered by size to prevent misaligned 16- or 32-bit datatypes.

    // Note: Write-access to this structure is only allowed before ttc_gfx_init()
    //       and after ttc_gfx_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // curent font to use for print operations
    const t_ttc_font_data* Font;

    // low-level configuration (structure defined by low-level driver)
    t_ttc_gfx_architecture* LowLevelConfig;

    const t_u32* Palette;   // current color palette

    t_u16 PhysicalWidth;    // amount of pixels in horizontal direction (rotation == 0)
    t_u16 PhysicalHeight;   // amount of pixels in vertical direction   (rotation == 0)
    t_u16 PhysicalX;        // physical X-coordinate of current drawing position for pixel operations (always portrait oriented!)
    t_u16 PhysicalY;        // physical Y-coordinate of current drawing position for pixel operations (always portrait oriented!)
    t_u16 Depth;            // amount of bits used to encode pixel color

    t_u16 Width;            // amount of pixels in horizontal direction                              (rotated according to TTC_GFX_ROTATION_CLOCKWISE)
    t_u16 Height;           // amount of pixels in vertical direction                                (rotated according to TTC_GFX_ROTATION_CLOCKWISE)
    t_u16 X;                // logical X-coordinate of current drawing position for pixel operations (rotated according to TTC_GFX_ROTATION_CLOCKWISE)
    t_u16 Y;                // logical Y-coordinate of current drawing position for pixel operations (rotated according to TTC_GFX_ROTATION_CLOCKWISE)

    t_u16 ColorFg;          // current 16-bit color
    t_u16 ColorBg;          // current 16-bit background color

    t_u16 TextX;            // X-coordinate of current drawing position for text operations
    t_u16 TextY;            // Y-coordinate of current drawing position for text operations
    t_s16 TextBorderLeft;   // pixel x-coordinate where TextX == 0
    t_s16 TextBorderTop;    // pixel y-coordinate where TextY == 0
    t_s16 TextBorderRight;  // pixel x-coordinate of right border of text display
    t_s16 TextBorderBottom; // pixel y-coordinate of lower border of text display
    t_u16 TextColumns;      // amount of characters of current font that fit in one row
    t_u16 TextRows;         // amount of characters of current font that fit in one column

    t_u8  LogicalIndex;     // automatically set: logical index of device to use (1 = TTC_GFX1, ...)
    t_u8  PhysicalIndex;    // automatically set: physical index of device to use (0 = first hardware device, ...)
    t_u8  PaletteSize;      // size of current palette
    e_ttc_gfx_architecture Architecture; // type of architecture used for current gfx device

    union  { // generic configuration bits common for all low-level Drivers
        t_u8 All;
        struct {
            unsigned Initialized        : 1;  // ==1: device has been initialized successfully
            // ToDo: additional high-level flags go here..

            unsigned Reserved1        : 7; // pad to 8 bits
        } Bits;
    } Flags;

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_gfx_config;

typedef struct s_ttc_gfx_quick { //         set of quick access variables for current display
    t_ttc_gfx_config*      Config;
    const t_ttc_font_data* Font;
    t_u16                  Width;
    t_u16                  Height;
    t_u16                  PhysicalWidth;
    t_u16                  PhysicalHeight;
} __attribute__( ( __packed__ ) ) t_ttc_gfx_quick;

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_GFX_TYPES_H
