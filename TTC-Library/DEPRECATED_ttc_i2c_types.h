/** { ttc_i2c_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  High-Level datatype definitions for I2C device.
 *  Structures, Enums and Defines being required by both, high- and low-level i2c.
 *  
 *  Created from template ttc_device_types.h revision 22 at 20140424 05:11:32 UTC
 *
 *  Authors: <AUTHOR>
 * 
}*/

#ifndef TTC_I2C_TYPES_H
#define TTC_I2C_TYPES_H

//{ Includes *************************************************************

#include "ttc_basic_types.h"
#include "compile_options.h"
#ifdef EXTENSION_450_i2c_stm32f1xx
#  include "i2c/DEPRECATED_i2c_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_450_i2c_stm32l1xx
#  include "i2c/DEPRECATED_i2c_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

// TTC_I2Cn has to be defined as constant by makefile.100_board_*
#ifdef TTC_I2C5
  #ifndef TTC_I2C4
    #error TTC_I2C5 is defined, but not TTC_I2C4 - all lower TTC_I2Cn must be defined!
  #endif
  #ifndef TTC_I2C3
    #error TTC_I2C5 is defined, but not TTC_I2C3 - all lower TTC_I2Cn must be defined!
  #endif
  #ifndef TTC_I2C2
    #error TTC_I2C5 is defined, but not TTC_I2C2 - all lower TTC_I2Cn must be defined!
  #endif
  #ifndef TTC_I2C1
    #error TTC_I2C5 is defined, but not TTC_I2C1 - all lower TTC_I2Cn must be defined!
  #endif

  #define TTC_I2C_AMOUNT 5
#else
  #ifdef TTC_I2C4
    #define TTC_I2C_AMOUNT 4

    #ifndef TTC_I2C3
      #error TTC_I2C5 is defined, but not TTC_I2C3 - all lower TTC_I2Cn must be defined!
    #endif
    #ifndef TTC_I2C2
      #error TTC_I2C5 is defined, but not TTC_I2C2 - all lower TTC_I2Cn must be defined!
    #endif
    #ifndef TTC_I2C1
      #error TTC_I2C5 is defined, but not TTC_I2C1 - all lower TTC_I2Cn must be defined!
    #endif
  #else
    #ifdef TTC_I2C3

      #ifndef TTC_I2C2
        #error TTC_I2C5 is defined, but not TTC_I2C2 - all lower TTC_I2Cn must be defined!
      #endif
      #ifndef TTC_I2C1
        #error TTC_I2C5 is defined, but not TTC_I2C1 - all lower TTC_I2Cn must be defined!
      #endif

      #define TTC_I2C_AMOUNT 3
    #else
      #ifdef TTC_I2C2

        #ifndef TTC_I2C1
          #error TTC_I2C5 is defined, but not TTC_I2C1 - all lower TTC_I2Cn must be defined!
        #endif

        #define TTC_I2C_AMOUNT 2
      #else
        #ifdef TTC_I2C1
          #define TTC_I2C_AMOUNT 1
        #else
          #define TTC_I2C_AMOUNT 0
        #endif
      #endif
    #endif
  #endif
#endif

/* Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop. 
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_I2C 0  # disable assert handling for i2c devices
 *
 */
#ifndef TTC_ASSERT_I2C    // any previous definition set (Makefile)?
#define TTC_ASSERT_I2C 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_I2C == 1)  // use Assert()s in I2C code (somewhat slower but alot easier to debug)
  #define Assert_I2C(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in I2C code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_I2C(Condition, ErrorCode)
#endif

/** Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of ttc_i2c_config_t during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef ttc_i2c_architecture_t
#warning Missing low-level definition for ttc_i2c_architecture_t (using default)
#define ttc_i2c_architecture_t void
#endif

/** Architecture dependent base register
 *
 * Each low-level driver may define this structure to increase of return value of ttc_i2c_get_base_register().
 * Simply copy the define line below into your _types.h and change void to a structure according to your architecture.
 */
#ifndef ttc_i2c_base_register_t
#warning Missing low-level definition for ttc_i2c_base_register_t (using default)
#define ttc_i2c_base_register_t void
#endif

/** Architecture dependent event list
 *
 * Each low-level driver has to define a list of events that can be checked via ttc_i2c.check_event().
 * These can be defined as register bitmasks for fast check operations.
 * (See i2c_stm32l1xx_event_e for an example).
 */
#ifndef ttc_i2c_event_e
#warning Missing low-level definition for ttc_i2c_event_e (using default)
    typedef enum {
      i2c_event_None,

      // additional events go here...

      i2c_event_Unknown
    } ttc_i2c_event_generic_e;

#   define ttc_i2c_event_e ttc_i2c_event_generic_e
#endif

/** Architecture dependent flag list
 *
 * Each low-level driver has to define a list of flags that can be checked via ttc_i2c.check_flag().
 * These can be defined as register bitmasks for fast check operations.
 * (See i2c_stm32l1xx_flag_e for an example).
 */
#ifndef ttc_i2c_flag_e
#warning Missing low-level definition for ttc_i2c_event_e (using default)

    typedef enum {
    i2c_flag_None,

        // additional events go here...

        i2c_flag_Unknown
    } ttc_i2c_flag_generic_e;

#   define ttc_i2c_flag_e ttc_i2c_flag_generic_e
#endif

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef enum {   // ttc_i2c_physical_index_e (starts at zero!)
// 
// You may use this enum to assign physical index to logical TTC_I2Cn constants in your makefile
//
// E.g.: Defining physical device #3 of i2c as first logical device TTC_I2C1
//       COMPILE_OPTS += -DTTC_I2C1=INDEX_I2C3
//
    INDEX_I2C1,
    INDEX_I2C2,
    INDEX_I2C3,
    INDEX_I2C4,
    INDEX_I2C5,
    INDEX_I2C6,
    INDEX_I2C7,
    INDEX_I2C8,
    INDEX_I2C9,
    // add more if needed
    
    INDEX_I2C_ERROR
} ttc_i2c_physical_index_e;
typedef enum {    // ttc_i2c_errorcode_e     return codes of I2C devices
    tie_OK,                                  // =0: no error
    tie_TimeOut,                             // timeout occured in called function
    tie_TimeOut_StartCondition,              // timeout occured while waiting for start-condition to take place
    tie_TimeOut_StopCondition,               // timeout occured while waiting for stop-condition to take place
    tie_TimeOut_Flag_Busy,                   // timeout occured while waiting for busy flag to reset
    tie_TimeOut_Master_ModeSelect,           // timeout occured while waiting for event master mode select
    tie_TimeOut_Master_TransmitterMode,      // timeout occured while waiting for event master transmitter mode select
    tie_TimeOut_Master_ByteTransmitted,      // timeout occured while waiting for event master bytes transmitted
    tie_TimeOut_Master_RegisterSend,         // timeout occured while sending register address to slave
    tie_TimeOut_Master_BufferSend,           // timeout occured while sending data buffer to slave
    tie_TimeOut_Master_ByteRead,             // timeout occured while waiting for incoming byte from slave
    tie_TimeOut_Master_Receiver_Mode_Selected,
    tie_TimeOut_Flag_Txe,
    tie_BusError,                            // bus error occured
    tie_Master_ArbitrationLost,              // master has lost bus arbitration
    tie_AcknowledgeFailure,                  // Acknowledgement failed
    tie_PEC_Error,                           // packet error correction failed
    tie_SMBusAlert,                          // SMBus alert occured
    tie_NotImplemented,                      // function has no implementation for current architecture
    tie_DeviceNotFound,                      // adressed I2C device not available in current uC
    tie_InvalidArgument,                     // general argument error
    tie_DeviceNotReady,                      // choosen device has not been initialized properly
    tie_AddressNotAcknowledgedBySlave,       // protocol error: The slave device did not acknowledge its address

    tie_UnknownError               // no valid errorcodes past this entry
} ttc_i2c_errorcode_e;
typedef enum {    // ttc_i2c_architecture_e  types of architectures supported by I2C driver
  ta_i2c_None,           // no architecture selected
  ta_i2c_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
  ta_i2c_stm32w1xx, // automatically added by ./create_DeviceDriver.pl
  ta_i2c_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
  ta_i2c_stm32l1, // automatically added by ./create_DeviceDriver.pl
  ta_i2c_stm32f4xx, // automatically added by ./create_DeviceDriver.pl
//InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)
  
  ta_i2c_Unknown        // architecture not supported
} ttc_i2c_architecture_e;

typedef enum {     // ttc_i2c_register_type_e
    tiras_MSB_First_8Bit,    // 1 byte  used to transmit a register adress (Most significant byte transferred first)
    tiras_MSB_First_16Bit,   // 2 bytes used to transmit a register adress (Most significant byte transferred first)
    tiras_MSB_First_24Bit,   // 3 bytes used to transmit a register adress (Most significant byte transferred first)
    tiras_MSB_First_32Bit,   // 4 bytes used to transmit a register adress (Most significant byte transferred first)
    tiras_LSB_First_8Bit,    // 1 byte  used to transmit a register adress (Least significant byte transferred first)
    tiras_LSB_First_16Bit,   // 2 bytes used to transmit a register adress (Least significant byte transferred first)
    tiras_LSB_First_24Bit,   // 3 bytes used to transmit a register adress (Least significant byte transferred first)
    tiras_LSB_First_32Bit    // 4 bytes used to transmit a register adress (Least significant byte transferred first)
} ttc_i2c_register_type_e;

typedef struct {   // ttc_i2c_event_t  triggered event interrupts
  unsigned StartBitSend    : 1;     // =1: start bit send (SB)
  unsigned Address         : 1;     // =1: address send/ matched (ADDR)
  unsigned Address10       : 1;     // =1: 10-but header sent (ADD10)
  unsigned StopReceived    : 1;     // =1: Stop received (STOPF)
  unsigned ByteTransfered  : 1;     // =1: Data byte transfer finished (BTF)
  unsigned TxE             : 1;     // =1: Transmit buffer empty (TxE)
  unsigned RxNE            : 1;     // =1: Receive buffer not empty (RxNE)
  unsigned Reserved        : 1;
} __attribute__((__packed__)) ttc_i2c_event_t;
typedef struct {   // ttc_i2c_error_t  triggered error interrupts
  unsigned Bus             : 1;     // =1: Bus Error (BERR)
  unsigned ArbitrationLoss : 1;     // =1: Arbitration Loss (Master) (ARLO)
  unsigned AcknowledgeFail : 1;     // =1: AcknowledgeFailuire (AF)
  unsigned Overrun         : 1;     // =1: OverRun/ UnderRun (OVR)
  unsigned Checksum        : 1;     // =1: Packet received with invalid Checksum (PECERR)
  unsigned Timeout         : 1;     // =1: Timeout/ Tlow error (TIMEOUT)
  unsigned SMBusAlert      : 1;     // =1: SMBus Alert (SMBALERT)
  unsigned Reserved        : 1;
} __attribute__((__packed__)) ttc_i2c_error_t;

typedef enum { // ttc_i2c_direction_e
    tid_ReadingFromSlave,
    tid_WritingToSlave
} ttc_i2c_direction_e;

typedef union {  // ttc_i2c_Address_7Bit_u
    struct {
        unsigned ReadMode : 1;  // =1: master is reading from slave
        unsigned Address  : 7;  // 7 bit slave address
    } Fields;
    u8_t Byte;
} ttc_i2c_Address_7Bit_u;

typedef struct ttc_i2c_config_s { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_i2c_init()
    //       and after ttc_i2c_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.
    u16_t OwnAddress;                     // adress of this device
    u32_t ClockSpeed;                     // clock frequency to use (< 400000 on stm32f10x)
    u32_t ErrorCount;                     // increased on every error condition
    u8_t  Layout;                         // select pin layout to use (some uC allow pin remapping)
    u32_t TimeOut;                        // maximum allowed time for individual transactions (usecs)
    void*(*ISR_Events)(ttc_i2c_event_t);  // function pointer to interrupt service routine for events
    void*(*ISR_Errors)(ttc_i2c_error_t);  // function pointer to interrupt service routine for errors
    ttc_i2c_architecture_e Architecture; // type of architecture used for current i2c device
    u8_t  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_I2C1, ...)
    u8_t  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)
    
    // low-level configuration (structure defined by low-level driver)
    ttc_i2c_architecture_t* LowLevelConfig;      
    
    union  { // generic configuration bits common for all low-level Drivers
        u32_t All;
        struct {
            unsigned Master           : 1;  // =1: enable master mode (overrides Slave = 1); =0: not master
            unsigned Slave            : 1;  // =1: enable slave  mode; =0: not slave (automatically activates master mode)
            unsigned ModeFast         : 1;  // =1: enable fast mode; =0: standard mode
            unsigned DMA_TX           : 1;  // =1: enable direct memory access for transmitting data; =0: no DMA
            unsigned DMA_RX           : 1;  // =1: enable direct memory access for receiving data; =0: no DMA
            unsigned SMBus            : 1;  // =1: enable SMBus mode; =0: normal I2C mode
            unsigned SMBus_Alert      : 1;  // =1: use extra pin SMB_Alert; =0: only use SCL+SDA
            unsigned SMBus_Host       : 1;  // =1: enable Host configuration for SMBus mode; =0: Device configuration
            unsigned SMBus_ARP        : 1;  // =1: enable Address Resolution Protocol for SMBus mode; =0: no ARP
            unsigned DutyCycle_16_9   : 1;  // =1: enable Tlow/Thigh = 16/9 in fast mode; = 0: Tlow/Thigh = 2/1
            unsigned Acknowledgement  : 1;  // =1: enable acknowledgement mode; =0: no acknowledgements
            unsigned Adress_10Bit     : 1;  // =1: enable 10 bit wide adresses; =0: adresses are 7 bit wide
            unsigned PacketErrorCheck : 1;  // =1: enable packet error checking; =0: no error checking
            unsigned Slave_NoStretch  : 1;  // =1: enable clock stretching in slave mode; =0: no clock stretching
            unsigned GeneralCalls     : 1;  // =1: enable ACK of general address 0x00; =0: general address is not acknowledged
            unsigned Interrupt_Events : 1;  // =1: enable interrupts for events: =0: no interrupt generation for events
            unsigned Interrupt_Errors : 1;  // =1: enable interrupts for errors: =0: no interrupt generation for errors
            unsigned Initialized      : 1;  // =1: if I2C is initialized
            unsigned Reserved1        : 13;  // pad to 32 bits

        } Bits;
    } Flags;

    // Additional high-level attributes go here..
    
} __attribute__((__packed__)) ttc_i2c_config_t;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_I2C_TYPES_H
