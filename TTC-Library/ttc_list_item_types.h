#ifndef ttc_list_item_types_h
#define ttc_list_item_types_h

/** { ttc_list_item_types.h *********************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.h revision 11 at 20181203 21:16:10 UTC
 *
 *  This file must be included by every header that defines structures being able to be pushed to a ttc_list.
 *  Simply add one t_ttc_list_item type entry in every structure that shall be usable as a list item.
 *
 *  Example:
 *
 *  #include "ttc-lib/ttc_list_item_types.h"  // structures of individual list items
 *  typedef struct {
 *    t_ttc_list_item Item; // makes this structure appendable to a ttc_list (MUST BE FIRST ENTRY IN STRUCT!)
 *
 *    // any other fields come below
 *    t_u8 Foo;
 *    ...
 *  } t_my_universal_item;
 *
 *
 *  Authors: Gregor Rebel
 *
}*/
/** { Includes *********************************************************
 *
 * Put all includes here that are required to include this header file.
 * Do NOT add includes required to compile ttc_list_item_types.c here!
 */

#include "ttc_basic_types.h" // basic datatypes
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Constant Defines / Macros ****************************************
 *
 * As a convention, constant definitions are all UPPERCASED to distinguish
 * them from variables and functions.
 */

/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_LIST 0#         disable default asserts for LIST driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_LIST_EXTRA 1#   enable extra asserts for LIST driver
 *
 */
#ifndef TTC_ASSERT_LIST    // any previous definition set (Makefile)?
    #define TTC_ASSERT_LIST 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_LIST == 1)  // use Assert()s in LIST code (somewhat slower but alot easier to debug)
    #define Assert_LIST(Condition, Origin)        if (! (Condition) ) { ttc_assert_halt_origin(Origin); }
    #define Assert_LIST_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_LIST_Readable(Address, Origin) Assert_Readable(Address, Origin)
    #define Assert_LIST_VALID(Pointer, Origin) if (Pointer) (Assert_LIST_Writable(Pointer, Origin))
#else                      // use no default Assert()s in LIST code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_LIST(Condition, Origin)
    #define Assert_LIST_Writable(Address, Origin)
    #define Assert_LIST_Readable(Address, Origin)
    #define Assert_LIST_VALID(Pointer, Origin)
#endif

#ifndef TTC_ASSERT_LIST_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_LIST_EXTRA 1  // Define as 1 whenever you work on ttc_list.c! (ttc_list is quite complex and you really need extra checks when you add functionality.)
#endif
#if (TTC_ASSERT_LIST_EXTRA == 1)  // use Assert()s in LIST code (somewhat slower but alot easier to debug)
    #define Assert_LIST_EXTRA(Condition, Origin) if (! (Condition) ) { ttc_assert_halt_origin(Origin); }
    #define Assert_LIST_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_LIST_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
    #define Assert_LIST_VALID_EXTRA(Pointer, Origin) if (Pointer) (Assert_LIST_Writable(Pointer, Origin))
#else  // use no extra Assert()s in LIST code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_LIST_EXTRA(Condition, Origin)
    #define Assert_LIST_EXTRA_Writable(Address, Origin)
    #define Assert_LIST_EXTRA_Readable(Address, Origin)
    #define Assert_LIST_VALID_EXTRA(Pointer, Origin)
#endif
//}

#ifndef TTC_LIST_ITEMS_IN_ONE_LIST_ONLY  // ==1: each list item has a pointer to its list to ensure that each item is in one list only (easier debugging + extra safety checks in ttc_heap_pool_block_free() )
    #if (TTC_ASSERT_LIST_EXTRA == 1)
        #define TTC_LIST_ITEMS_IN_ONE_LIST_ONLY 1
    #else
        #define TTC_LIST_ITEMS_IN_ONE_LIST_ONLY 0
    #endif
#endif

//}ConstantDefines
/** { Structure Declarations *******************************************
 *
 * This can be typedef struct or typedef enum declarations.
 * Always prefix structures and enums with filename to avoid double declarations!
 *
 */

/** Header of all kinds of data being able to be queued in a ttc_list */
typedef struct s_ttc_list_item {
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
    void*                   Owner;  // !=NULL: list to which this item belongs to (can be casted to (t_ttc_list*) ); ==NULL: item belongs to no list
#endif
#if (TTC_ASSERT_LIST_EXTRA == 1)
    void*                   Caller; // pointer to function that last called a ttc_list_*() function for this item
#endif
    struct s_ttc_list_item* Next;   // !=NULL: pointer to next item in single linked list, ==TTC_LIST_TERMINATOR: last item in single linked list
} t_ttc_list_item;

//}StructureDeclarations
/** { Function Declarations ********************************************
 *
 * Declare all functions here that may be called from outside.
 * Rules of Good Code:
 * - document every function declaration
 * - prefix public function names with their filename to avoid double definitions
 */
// { Example declaration
/** calculates the sum of A and B
 *
 * @param A  unsigned integer
 * @param B  unsigned integer
 * @return   A + B
 */
// t_u8 ttc_list_item_types_calculate(t_u16 A, t_u16 B);
//}


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //ttc_list_item_types_h
