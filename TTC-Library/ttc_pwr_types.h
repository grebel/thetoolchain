/** { ttc_pwr_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for PWR device.
 *  Structures, Enums and Defines being required by both, high- and low-level pwr.
 *  This file does not provide function declarations!
 *  
 *  Created from template ttc_device_types.h revision 27 at 20150130 10:43:12 UTC
 *
 *  Authors: Gregor Rebel
 * 
}*/

#ifndef TTC_PWR_TYPES_H
#define TTC_PWR_TYPES_H

//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_pwr.h" or "ttc_pwr.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h" // only required to help IDE to know constant definitions
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
#ifdef EXTENSION_pwr_stm32l1xx
#  include "pwr/pwr_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)
 
//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

//{  TTC_PWRn has to be defined as constant by makefile.100_board_*
#ifdef TTC_PWR5
  #ifndef TTC_PWR4
    #error TTC_PWR5 is defined, but not TTC_PWR4 - all lower TTC_PWRn must be defined!
  #endif
  #ifndef TTC_PWR3
    #error TTC_PWR5 is defined, but not TTC_PWR3 - all lower TTC_PWRn must be defined!
  #endif
  #ifndef TTC_PWR2
    #error TTC_PWR5 is defined, but not TTC_PWR2 - all lower TTC_PWRn must be defined!
  #endif
  #ifndef TTC_PWR1
    #error TTC_PWR5 is defined, but not TTC_PWR1 - all lower TTC_PWRn must be defined!
  #endif

  #define TTC_PWR_AMOUNT 5
#else
  #ifdef TTC_PWR4
    #define TTC_PWR_AMOUNT 4

    #ifndef TTC_PWR3
      #error TTC_PWR5 is defined, but not TTC_PWR3 - all lower TTC_PWRn must be defined!
    #endif
    #ifndef TTC_PWR2
      #error TTC_PWR5 is defined, but not TTC_PWR2 - all lower TTC_PWRn must be defined!
    #endif
    #ifndef TTC_PWR1
      #error TTC_PWR5 is defined, but not TTC_PWR1 - all lower TTC_PWRn must be defined!
    #endif
  #else
    #ifdef TTC_PWR3

      #ifndef TTC_PWR2
        #error TTC_PWR5 is defined, but not TTC_PWR2 - all lower TTC_PWRn must be defined!
      #endif
      #ifndef TTC_PWR1
        #error TTC_PWR5 is defined, but not TTC_PWR1 - all lower TTC_PWRn must be defined!
      #endif

      #define TTC_PWR_AMOUNT 3
    #else
      #ifdef TTC_PWR2

        #ifndef TTC_PWR1
          #error TTC_PWR5 is defined, but not TTC_PWR1 - all lower TTC_PWRn must be defined!
        #endif

        #define TTC_PWR_AMOUNT 2
      #else
        #ifdef TTC_PWR1
          #define TTC_PWR_AMOUNT 1
        #else
          #define TTC_PWR_AMOUNT 0
        #endif
      #endif
    #endif
  #endif
#endif
//}
/**{ Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop. 
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_PWR 0  # disable assert handling for pwr devices
 *
 */
#ifndef TTC_ASSERT_PWR    // any previous definition set (Makefile)?
#define TTC_ASSERT_PWR 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_PWR == 1)  // use Assert()s in PWR code (somewhat slower but alot easier to debug)
  #define Assert_PWR(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in PWR code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_PWR(Condition, ErrorCode)
#endif
//}
/**{ Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_pwr_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_pwr_architecture
#warning Missing low-level definition for t_ttc_pwr_architecture (using default)
#define t_ttc_pwr_architecture void
#endif
//}
/**{ Check if definitions of logical indices are taken from e_ttc_pwr_physical_index
 *
 * See definition of e_ttc_pwr_physical_index below for details.
 *
 */

#define TTC_INDEX_PWR_MAX 10  // must have same value as TTC_INDEX_PWR_ERROR
#ifdef TTC_PWR1
#  if TTC_PWR1 >= TTC_INDEX_PWR_MAX
#    error Wrong definition of TTC_PWR1, check your makefile (must be one from e_ttc_pwr_physical_index)!
#  endif
#  ifndef TTC_PWR1_DRIVER
#    warning Missing definition, check your makefile: TTC_PWR1_DRIVER (choose one from e_ttc_pwr_architecture)
#    define TTC_PWR1_DRIVER ta_pwr_None
#  endif
#endif
#ifdef TTC_PWR2
#  if TTC_PWR2 >= TTC_INDEX_PWR_MAX
#    error Wrong definition of TTC_PWR2, check your makefile (must be one from e_ttc_pwr_physical_index)!
#  endif
#  ifndef TTC_PWR2_DRIVER
#    warning Missing definition, check your makefile: TTC_PWR2_DRIVER (choose one from e_ttc_pwr_architecture)
#    define TTC_PWR2_DRIVER ta_pwr_None
#  endif
#endif
#ifdef TTC_PWR3
#  if TTC_PWR3 >= TTC_INDEX_PWR_MAX
#    error Wrong definition of TTC_PWR3, check your makefile (must be one from e_ttc_pwr_physical_index)!
#  endif
#  ifndef TTC_PWR3_DRIVER
#    warning Missing definition, check your makefile: TTC_PWR3_DRIVER (choose one from e_ttc_pwr_architecture)
#    define TTC_PWR3_DRIVER ta_pwr_None
#  endif
#endif
#ifdef TTC_PWR4
#  if TTC_PWR4 >= TTC_INDEX_PWR_MAX
#    error Wrong definition of TTC_PWR4, check your makefile (must be one from e_ttc_pwr_physical_index)!
#  endif
#  ifndef TTC_PWR4_DRIVER
#    warning Missing definition, check your makefile: TTC_PWR4_DRIVER (choose one from e_ttc_pwr_architecture)
#    define TTC_PWR4_DRIVER ta_pwr_None
#  endif
#endif
#ifdef TTC_PWR5
#  if TTC_PWR5 >= TTC_INDEX_PWR_MAX
#    error Wrong definition of TTC_PWR5, check your makefile (must be one from e_ttc_pwr_physical_index)!
#  endif
#  ifndef TTC_PWR5_DRIVER
#    warning Missing definition, check your makefile: TTC_PWR5_DRIVER (choose one from e_ttc_pwr_architecture)
#    define TTC_PWR5_DRIVER ta_pwr_None
#  endif
#endif
#ifdef TTC_PWR6
#  if TTC_PWR6 >= TTC_INDEX_PWR_MAX
#    error Wrong definition of TTC_PWR6, check your makefile (must be one from e_ttc_pwr_physical_index)!
#  endif
#  ifndef TTC_PWR6_DRIVER
#    warning Missing definition, check your makefile: TTC_PWR6_DRIVER (choose one from e_ttc_pwr_architecture)
#    define TTC_PWR6_DRIVER ta_pwr_None
#  endif
#endif
#ifdef TTC_PWR7
#  if TTC_PWR7 >= TTC_INDEX_PWR_MAX
#    error Wrong definition of TTC_PWR7, check your makefile (must be one from e_ttc_pwr_physical_index)!
#  endif
#  ifndef TTC_PWR7_DRIVER
#    warning Missing definition, check your makefile: TTC_PWR7_DRIVER (choose one from e_ttc_pwr_architecture)
#    define TTC_PWR7_DRIVER ta_pwr_None
#  endif
#endif
#ifdef TTC_PWR8
#  if TTC_PWR8 >= TTC_INDEX_PWR_MAX
#    error Wrong definition of TTC_PWR8, check your makefile (must be one from e_ttc_pwr_physical_index)!
#  endif
#  ifndef TTC_PWR8_DRIVER
#    warning Missing definition, check your makefile: TTC_PWR8_DRIVER (choose one from e_ttc_pwr_architecture)
#    define TTC_PWR8_DRIVER ta_pwr_None
#  endif
#endif
#ifdef TTC_PWR9
#  if TTC_PWR9 >= TTC_INDEX_PWR_MAX
#    error Wrong definition of TTC_PWR9, check your makefile (must be one from e_ttc_pwr_physical_index)!
#  endif
#  ifndef TTC_PWR9_DRIVER
#    warning Missing definition, check your makefile: TTC_PWR9_DRIVER (choose one from e_ttc_pwr_architecture)
#    define TTC_PWR9_DRIVER ta_pwr_None
#  endif
#endif
#ifdef TTC_PWR10
#  if TTC_PWR10 >= TTC_INDEX_PWR_MAX
#    error Wrong definition of TTC_PWR10, check your makefile (must be one from e_ttc_pwr_physical_index)!
#  endif
#  ifndef TTC_PWR10_DRIVER
#    warning Missing definition, check your makefile: TTC_PWR10_DRIVER (choose one from e_ttc_pwr_architecture)
#    define TTC_PWR10_DRIVER ta_pwr_None
#  endif
#endif
//}

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef e_ttc_physical_index e_ttc_pwr_physical_index;
typedef enum {   // e_ttc_pwr_errorcode      return codes of PWR devices
  ec_pwr_OK = 0, 
  
  // other warnings go here..

  ec_pwr_ERROR,                  // general failure
  ec_pwr_NULL,                   // NULL pointer not accepted
  ec_pwr_DeviceNotFound,         // corresponding device could not be found
  ec_pwr_InvalidConfiguration,   // sanity check of device configuration failed
  ec_pwr_InvalidImplementation,  // your code does not behave as expected

  // other failures go here..
  
  ec_pwr_unknown                // no valid errorcodes past this entry 
} e_ttc_pwr_errorcode;
typedef enum {   // e_ttc_pwr_architecture  types of architectures supported by PWR driver
  ta_pwr_None,           // no architecture selected
  

  ta_pwr_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
//InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)
  
  ta_pwr_unknown        // architecture not supported
} e_ttc_pwr_architecture;
typedef struct s_ttc_pwr_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_pwr_init()
    //       and after ttc_pwr_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    e_ttc_pwr_architecture Architecture; // type of architecture used for current pwr device
    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_PWR1, ...)
    t_u8  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)
    
    // low-level configuration (structure defined by low-level driver)
    t_ttc_pwr_architecture* LowLevelConfig;      
    
    union  { // generic configuration bits common for all low-level Drivers
        t_u16 All;
        struct {
          unsigned Initialized        : 1;  // ==1: device has been initialized successfully
          // ToDo: additional high-level flags go here..
            
            unsigned Reserved1        : 15; // pad to 16 bits
        } Bits;
    } Flags;

    // Additional high-level attributes go here..
    
} __attribute__((__packed__)) t_ttc_pwr_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_PWR_TYPES_H
