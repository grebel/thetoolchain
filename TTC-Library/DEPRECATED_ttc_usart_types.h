#ifndef TTC_USART_TYPES_H
#define TTC_USART_TYPES_H

/*{ ttc_usart_types.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Basic defines, enums and structures.
 *
 * Include this file from 
 * architecture independent header files (ttc_XXX.h) and 
 * architecture depend      header files (e.g. stm32_XXX.h)
 * 
}*/
#include "ttc_basic.h"
#include "ttc_queue.h"
#include "ttc_memory.h"


#ifdef TARGET_ARCHITECTURE_STM32F1xx
#include "stm32/stm32_usart_types.h" // will define ttc_usart_architecture_t
#endif

#ifdef TARGET_ARCHITECTURE_STM32W1xx
#include "stm32w/stm32w_usart_types.h" // will define ttc_usart_architecture_t
#endif

#ifdef TARGET_ARCHITECTURE_STM32L1xx
#include "stm32l1//stm32l1_usart_types.h" // will define ttc_usart_architecture_t
#endif

//{ Defines/ TypeDefs ****************************************************

#ifndef TTC_ASSERT_USART    // any previous definition set (Makefile)?
#define TTC_ASSERT_USART 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_USART == 1)  // use Assert()s in USART code (somewhat slower but alot easier to debug)
  #define Assert_USART(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in USART code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_USART(Condition, ErrorCode)
#endif

//  defines that may be overridden in makefile via "COMPILE_OPTS += -D..."
#ifndef TTC_USART_SIZE_QUEUE_RECEIVE
  #define TTC_USART_SIZE_QUEUE_RECEIVE 3  // max amount of memory blocks that can be stored in advance as receive buffers
#endif

#ifndef TTC_USART_SIZE_QUEUE_TRANSMIT
  #define TTC_USART_SIZE_QUEUE_TRANSMIT 5  // max amount of memory blocks that can be stored in advance for transmit
#endif

// TTC_USARTn has to be defined as constant by makefile.100_board_*
#ifdef TTC_USART5
  #ifndef TTC_USART4
    #error TTC_USART5 is defined, but not TTC_USART4 - all lower TTC_USARTn must be defined!
  #endif
  #ifndef TTC_USART3
    #error TTC_USART5 is defined, but not TTC_USART3 - all lower TTC_USARTn must be defined!
  #endif
  #ifndef TTC_USART2
    #error TTC_USART5 is defined, but not TTC_USART2 - all lower TTC_USARTn must be defined!
  #endif
  #ifndef TTC_USART1
    #error TTC_USART5 is defined, but not TTC_USART1 - all lower TTC_USARTn must be defined!
  #endif

  #define TTC_AMOUNT_USARTS 5
#else
  #ifdef TTC_USART4
    #define TTC_AMOUNT_USARTS 4

    #ifndef TTC_USART3
      #error TTC_USART4 is defined, but not TTC_USART3 - all lower TTC_USARTn must be defined!
    #endif
    #ifndef TTC_USART2
      #error TTC_USART4 is defined, but not TTC_USART2 - all lower TTC_USARTn must be defined!
    #endif
    #ifndef TTC_USART1
      #error TTC_USART4 is defined, but not TTC_USART1 - all lower TTC_USARTn must be defined!
    #endif
  #else
    #ifdef TTC_USART3
    
      #ifndef TTC_USART2
        #error TTC_USART3 is defined, but not TTC_USART2 - all lower TTC_USARTn must be defined!
      #endif
      #ifndef TTC_USART1
        #error TTC_USART3 is defined, but not TTC_USART1 - all lower TTC_USARTn must be defined!
      #endif
    
      #define TTC_AMOUNT_USARTS 3
    #else
      #ifdef TTC_USART2

        #ifndef TTC_USART1
          #error TTC_USART2 is defined, but not TTC_USART1 - all lower TTC_USARTn must be defined!
        #endif

        #define TTC_AMOUNT_USARTS 2
      #else
        #ifdef TTC_USART1
          #define TTC_AMOUNT_USARTS 1
        #else
          #define TTC_AMOUNT_USARTS 0
        #endif
      #endif
    #endif
  #endif
#endif

//}
// capacity of each memory block (bytes)
#ifndef TTC_USART_BLOCK_SIZE
#define TTC_USART_BLOCK_SIZE 20
#endif

// maximum allocated amount of memory buffers for all usarts
#ifndef TTC_USART_MAX_MEMORY_BUFFERS
#define TTC_USART_MAX_MEMORY_BUFFERS  TTC_AMOUNT_USARTS * 5
#endif


//} Defines
//{ constants to be defined by architecture driver ***********************

//}constants
//{ Structures/ Enums  ***************************************************

typedef enum {   // ttc_usart_physical_index_e (starts at zero!)
    INDEX_USART1,
    INDEX_USART2,
    INDEX_USART3,
    INDEX_USART4,
    INDEX_USART5,
    tupi_NotImplemented
} ttc_usart_physical_index_e;

typedef enum {   // ttc_usart_errorcode_e
    ec_usart_OK,                // =0: no error
    tue_TimeOut,           // timeout occured in called function
    tue_DeviceNotFound,    // adressed USART device not available in current uC
    tue_NotImplemented,    // function has no implementation for current architecture
    tue_InvalidArgument,   // general argument error
    tue_InvalidBaudRate,   // given baud rate not suitable for adressed USART
    tue_InvalidWordSize,   // given word size not suitable for adressed USART
    tue_InvalidStopBits,   // given amount of stop bits not suitable for adressed USART
    tue_TxBufferFull,      // given data could not be stored
    tue_RxQueueLimited,    // given rx-buffer could not be stored because receive queue cannot hold more entries
    tue_No_ControlParity,  // feature not available: check incoming parity bit
    tue_No_Receive,        // feature not available: activate receiver
    tue_No_RtsCts,         // feature not available: Hardware Flow Control
    tue_No_SingleWire,     // feature not available: single-wire half-duplex
    tue_No_SmartCard,      // feature not available: SmartCard mode
    tue_No_Synchronous,    // feature not available: Synchronous Communication
    tue_No_Transmit,       // feature not available: activate transmitter
    tue_No_TransmitParity, // feature not available: transmit parity bit
    tue_InvalidPinConfig,  // one or more pins have been configured incorrectly -> check makefile for TTC_USARTxxx defines
    tue_DeviceNotReady,    // choosen device has not been initialized properly

    tue_UnknownError
} ttc_usart_errorcode_e;

typedef struct { // ttc_usart_errorcode_t
    unsigned Overrun  : 1;
    unsigned Noise    : 1;
    unsigned Framing  : 1;
    unsigned Parity   : 1;
    unsigned reserved : 4;

} ttc_usart_errorcode_t;

typedef struct ttc_usart_generic_s { // architecture independent configuration data of single USART

    // Note: Write-access to this structure is only allowed before first ttc_usart_init() call!
    //       After initialization, only read accesses are allowed for high-level code.

    u8_t LogicalIndex;               // automatically set: logical index of USART to use (1 = TTC_USART1, ...)

    union  {
        u32_t All;
        struct {
            // Bits
            unsigned ControlParity    : 1; // =1: check incoming parity bit
            unsigned IrDA             : 1; // =1: activate infrared mode
            unsigned IrDALowPower     : 1; // =1: activate infrared low power mode
            unsigned IrqOnCts         : 1; // =1: interrupt is generated when CTS=1
            unsigned IrqOnError       : 1; // =1: interrupt is generated in case of any error
            unsigned IrqOnIdle        : 1; // =1: interrupt is generated when USART is idle
            unsigned IrqOnTxComplete  : 1; // =1: interrupt is generated when transmission has completed
            unsigned IrqOnParityError : 1; // =1: interrupt is generated when parity error occurs
            unsigned IrqOnRxNE        : 1; // =1: interrupt is generated when byte has been received
            unsigned LIN              : 1; // =1: activate Local Interconnection Network mode (serial bus)
            unsigned ParityEven       : 1; // =1: activate parity checking with even parity
            unsigned ParityOdd        : 1; // =1: activate parity checking with odd parity
            unsigned Receive          : 1; // =1: activate receiver via RxD
            unsigned Cts              : 1; // =1: data is only sent when Cts goes low
            unsigned Rts              : 1; // =1: data is only requested when receive buffer is empty
            unsigned RtsCts           : 1; // =1: activate hardware flow-control via RTS/CTS wires
            unsigned RxDMA            : 1; // =1: enable DMA for Receiver
            unsigned SendBreaks       : 1; // =1: activate software flow-control via break characters
            unsigned SingleWire       : 1; // =1: use single-wire half-duplex
            unsigned SmartCard        : 1; // =1: activate smartcard mode
            unsigned SmartCardNACK    : 1; // =1: transmission during parity error enabled
            unsigned Synchronous      : 1; // =1: activate synchronous mode (requires additional clock wire)
            unsigned Transmit         : 1; // =1: activate transmitter via TxD
            unsigned TransmitParity   : 1; // =1: transmit parity bit
            unsigned TxDMA            : 1; // =1: enable DMA for Transmitter
            unsigned DelayedTransmits : 1; // =1: enable _delayed_() transmit functions
            unsigned TaskRxRunning    : 1; // =1: task for delayed transmits has been spawned

            unsigned Reserved1        : 5; // pad to 32 bits
        } Bits;
    } Flags;

    unsigned WordLength         : 4; // size of each character (7..9 Bits)
    unsigned HalfStopBits       : 3; // amount of half stop bits (1..4 * 0.5 Bits -> 0.5 .. 2 StopBits)
    unsigned Reserved2          : 1; // pad to 1 Byte
    u8_t     Layout;                 // select pin layout to use (some uC allow pin remapping)

    u32_t    BaudRate;               // symbol rate of connection
    u32_t    TimeOut;                // > 0: every transaction must finish within this amount of time (task-switches/ usecs)
                                     // = 0: return immediately if transaction cannot be done right now (Note: untested!)
                                     // =-1: no timeouts (wait indefinitely)

    // Defines character at end of packets in incoming data
    // Whenever a Char_EndOfLine character is detected, the receive task
    // will pass the received buffer to receiveBlock()
    u16_t    Char_EndOfLine;        // < 0x100: character that ends one line;

    // activity functions for byte receive
    // != NULL: registered function is called from _ttc_usart_rx_isr() after single byte has been received
    // Note: Registered function is called directly from interrupt service routine!
    void (*activity_rx_isr)(struct ttc_usart_generic_s*, u8_t Byte);

    // activity functions for byte receive
    // != NULL: registered function is called from _ttc_usart_tx_isr() before single byte is send out
    // Note: Registered function is called directly from interrupt service routine!
    void (*activity_tx_isr)(struct ttc_usart_generic_s*, u8_t Byte);

    // If none of the following receive() functions is given then ttc_usart_read_byte()
    // must be called periodically to check for incoming data
    //
    // If given, receiveBlock() will be called whenever this character is received or RX-Buffer full.
    // !=NULL: function that will be called to parse a block of received data
    //         receiveBlock() can return value of Block to allow to reuse it for new received data
    //         If receiveBlock() wants to use the memory block longer then has to return NULL
    //         and call ttc_memory_block_release(Block) to release it later.
    //
    ttc_memory_block_t* (*receiveBlock)(void* Argument, struct ttc_usart_generic_s* USART_Generic, ttc_memory_block_t* Block);

    // value being passed as first argument to receiveBlock()
    void* receiveBlock_Argument;

    // != NULL: function that will be called for every received byte
    //          Note: if receiveBlock!=NULL then receiveSingleByte() is called only if no
    //                memory blocks are available for incoming data !
    void (*receiveSingleByte)(void* USART_Generic, u8_t Byte);

    // queue management
    u8_t Size_Queue_Rx;        // max amount of entries to store in receive queue
    u8_t Size_Queue_Tx;        // max amount of memory blocks to manage for transmit

    ttc_task_info_t* Task_Rx;  // task handle of receive task that is reading from Queue_Rx

    //
    // fields below are set automatically by ttc_usart ----------------------------------
    //

    // transports received data:           _ttc_usart_rx_isr() -> _ttc_usart_task_receive()
    ttc_queue_bytes_t* Queue_Rx;

    // transports data to be transmitted:  ttc_usart_send_block() -> _ttc_usart_tx_isr()
    ttc_queue_pointers_t* Queue_Tx;

    // if set, no data is allowed to be added to Queue_Tx
    ttc_mutex_smart_t Queue_TX_Lock __attribute__ ((aligned (4)));


    u8_t Amount_RxBytesSkipped;     // amount of bytes received by _ttc_usart_rx_isr() which could not be pushed into receive queue

    // architecture dependend USART configuration (each USART requires its own!)
    ttc_usart_architecture_t USART_Arch;

#ifdef TTC_REGRESSION
    Base_t Amount_ISR_RX;   // amount of bytes received by _ttc_usart_rx_isr()
    Base_t Amount_ISR_TX;   // amount of bytes send out by _ttc_usart_tx_isr()
    Base_t Amount_Queue_RX; // amount of bytes pulled from Queue_Rx
    Base_t Amount_Queue_TX; // amount of bytes pushed to Queue_Tx
#endif
} ttc_usart_config_t;

//}

#endif // TTC_USART_TYPES_H
