/** { ttc_timer_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  High-Level datatype definitions for TIMER device.
 *
 *  Structures, Enums and Defines being required by both, high- and low-level timer.
 *  
 *  Created from template ttc_device_types.h revision 20 at 20140204 09:25:54 UTC
 *
 *  Authors: <AUTHOR>
 * 
}*/

#ifndef TTC_TIMER_TYPES_H
#define TTC_TIMER_TYPES_H

//{ Includes ***********************************************************

#include "ttc_basic_types.h"

#ifdef EXTENSION_450_timer_stm32w1xx
#  include "timer/timer_stm32w1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_450_timer_stm32f1xx
#  include "timer/timer_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_450_timer_stm32f0xx
#  include "timer/timer_stm32f0xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines ************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration ***********************************************

// TTC_TIMERn has to be defined as constant by makefile.100_board_*
#ifdef TTC_TIMER5
  #ifndef TTC_TIMER4
    #error TTC_TIMER5 is defined, but not TTC_TIMER4 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER3
    #error TTC_TIMER5 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER2
    #error TTC_TIMER5 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
  #endif
  #ifndef TTC_TIMER1
    #error TTC_TIMER5 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
  #endif

  #define TTC_TIMER_AMOUNT 5
#else
  #ifdef TTC_TIMER4
    #define TTC_TIMER_AMOUNT 4

    #ifndef TTC_TIMER3
      #error TTC_TIMER5 is defined, but not TTC_TIMER3 - all lower TTC_TIMERn must be defined!
    #endif
    #ifndef TTC_TIMER2
      #error TTC_TIMER5 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
    #endif
    #ifndef TTC_TIMER1
      #error TTC_TIMER5 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
    #endif
  #else
    #ifdef TTC_TIMER3

      #ifndef TTC_TIMER2
        #error TTC_TIMER5 is defined, but not TTC_TIMER2 - all lower TTC_TIMERn must be defined!
      #endif
      #ifndef TTC_TIMER1
        #error TTC_TIMER5 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
      #endif

      #define TTC_TIMER_AMOUNT 3
    #else
      #ifdef TTC_TIMER2

        #ifndef TTC_TIMER1
          #error TTC_TIMER5 is defined, but not TTC_TIMER1 - all lower TTC_TIMERn must be defined!
        #endif

        #define TTC_TIMER_AMOUNT 2
      #else
        #ifdef TTC_TIMER1
          #define TTC_TIMER_AMOUNT 1
        #else
          #define TTC_TIMER_AMOUNT 0
        #endif
      #endif
    #endif
  #endif
#endif

/* Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop. 
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_TIMER 0  # disable assert handling for timer devices
 *
 */
#ifndef TTC_ASSERT_TIMER    // any previous definition set (Makefile)?
#define TTC_ASSERT_TIMER 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_TIMER == 1)  // use Assert()s in TIMER code (somewhat slower but alot easier to debug)
  #define Assert_TIMER(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in TIMER code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_TIMER(Condition, ErrorCode)
#endif

//}Static Configuration
//{ Enums/ Structures **************************************************

typedef enum {    // ttc_timer_errorcode_e     return codes of TIMER devices
  ec_timer_OK = 0,
  
  // other warnings go here..

  ec_timer_ERROR,           // general failure
  ec_timer_DeviceNotFound,
  ec_timer_InvalidImplementation,
  
  // other failures go here..
  ec_timer_InvalidArgument,
  ec_timer_DeviceInUse

} ttc_timer_errorcode_e;
typedef enum {    // ttc_timer_architecture_e  types of architectures supported by TIMER driver
  ta_timer_None,           // no architecture selected
  
  ta_timer_stm32w1xx, // automatically added by ./create_DeviceDriver.pl
  ta_timer_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
  ta_timer_stm32f0xx, // automatically added by ./create_DeviceDriver.pl

    ta_timer_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
  ta_timer_stm32f10x, // automatically added by ./create_DeviceDriver.pl
//InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

  
  ta_timer_ERROR           // architecture not supported
} ttc_timer_architecture_e;
typedef struct ttc_timer_config_s { //         architecture independent configuration data


    // Note: Write-access to this structure is only allowed before first ttc_timer_init() call!
    //       After initialization, only read accesses are allowed for high-level code.

    ttc_timer_architecture_e Architecture; // type of architecture used for current timer device
    u8_t  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_TIMER1, ...)
    u8_t  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)

    void* LowLevelConfig;  // low-level configuration (structure known by low-level driver only)

    
    union  { // generic configuration bits common for all low-level Drivers
        u16_t All;
        struct {
          unsigned Initialized        : 1;  // ==1: device has been initialized successfully
          // ToDo: additional high-level flags go here..
            
            unsigned Reserved1        : 15; // pad to 16 bits
        } Bits;
    } Flags;

    // Additional high-level attributes go here..

    void (*Function)();
    void* Argument;
    Base_t TimeStart;
    Base_t TimePeriod;

    struct ttc_timer_config_s* Next; //pointer on the next Timer structure

    // Interrupt Chaining
    void (*eti_NextISR)(u32_t, void*);
    void* eti_NextISR_Argument;
    
} __attribute__((__packed__)) ttc_timer_config_t;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_TIMER_TYPES_H
