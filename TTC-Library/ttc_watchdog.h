#ifndef TTC_WATCHDOG_H
#define TTC_WATCHDOG_H

/*{ ttc_watchdog.h ************************************************

   Initialisation and usage of watchdog timers.

  This file provides a hardware independent interface to watchdog timers.

  written by Gregor Rebel 2012

}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#ifdef EXTENSION_ttc_watchdog
#ifdef TARGET_ARCHITECTURE_STM32F1xx
#include "stm32/stm32_watchdog.h"
#endif
#endif

//} Includes
//{ Structures/ Enums ****************************************************

//} Structures/ Enums
//{ Global Variables *****************************************************



//} Global Variables
//{ Function prototypes **************************************************

/** initialize first watchdog timer
 * @param Reload    != 0: sets reload value; == 0: use default reload value
 * @param Prescaler != 0: sets frequency prescaler;  == 0: use default prescaler value
 * @return ==0: watchdog has been successfully initialized (>0: error-code)
 */
int ttc_watchdog_init1( unsigned int Reload, int Prescaler );

/** reset first watchdog timer (must be called periodically to avoid reset from initialized watchdog)
 */
void ttc_watchdog_reload1();

/** deactivate first watchdog
 */
void ttc_watchdog_deinit1();

//} Function prototypes

#endif //TTC_WATCHDOG_H
