/*{ ttc_memory.c ***********************************************

 * Written by Gregor Rebel 2010-2012
 *
 * Scheduler and architecture independent multimemorying support.
 *
}*/
#include "DEPRECATED_ttc_memory.h"

#if TTC_MEMORY_ENUMERATE == 1
Base_t ttc_memory_Count_Alloc = 1;
Base_t ttc_memory_Count_PoolAlloc = 1;
#endif

#if TTC_MEMORY_STATISTICS == 1

// amount of blocks allocated on heap
Base_t ttc_memory_Amount_Blocks_Allocated = 0;

// amount of bytes occupied on heap by dynamic allocations
Base_t ttc_memory_Bytes_Allocated_Total   = 0;

// usable amount of allocated bytes
Base_t ttc_memory_Bytes_Allocated_Usable  = 0;

#endif


#if TTC_MEMORY_RECORDS > 0

// stores recorded data of latest memory allocations
ttc_array_define(ttc_memory_record_t, ttc_memory_Records, TTC_MEMORY_RECORDS);

// index in ttc_memory_Records[] of first unused entry
u16_t ttc_memory_first_free_record = 0;

#endif

// start address of heap is calculated by calling ttc_memory_init_heap()
Base_t ttc_memory_HeapSize = 0;

// size of heap is calculated by calling ttc_memory_init_heap()
u8_t* ttc_memory_HeapStart = NULL;

// size of heap is calculated by calling ttc_memory_init_heap()
u8_t* ttc_memory_HeapEnd = NULL;

// constants defined by linker script (configs/stm32f1xx.ld, configs/stm32w1xx.ld, ...)
extern unsigned char* _ttc_memory_heap_start; //TTC heap start address defined in linker script (e.g. memory_stm32f1xx.ld)
extern unsigned char* _ttc_memory_heap_end;   //TTC heap start address defined in linker script (e.g. memory_stm32f1xx.ld)

void ttc_memory_init_heap() {
    ttc_memory_HeapStart = (u8_t*) &_ttc_memory_heap_start;
    ttc_memory_HeapEnd   = (u8_t*) &_ttc_memory_heap_end;
    ttc_memory_HeapSize  = ttc_memory_HeapEnd - ttc_memory_HeapStart;
    Assert(ttc_memory_HeapSize <= 0x100000, ec_InvalidConfiguration); // Assuming error if >1MB of heap is available

#if TTC_MEMORY_STATISTICS == 1
    ttc_memory_Amount_Blocks_Allocated = 0;
    ttc_memory_Bytes_Allocated_Total   = 0;
    ttc_memory_Bytes_Allocated_Usable  = 0;
#endif

#if TTC_MEMORY_RECORDS > 0
    ttc_memory_set( &(A(ttc_memory_Records, 0)), 0, sizeof(ttc_memory_record_t) * TTC_MEMORY_RECORDS );
    ttc_memory_first_free_record = 0;
#endif

#ifdef _driver_memory_init
    _driver_memory_init(ttc_memory_HeapStart, ttc_memory_HeapSize);
#else
#  warning Missing low-level implementation for _driver_memory_init()
#endif
}
void* _ttc_memory_alloc(Base_t Size) {

    Base_t* Address = NULL;

#if TTC_MEMORY_ENUMERATE == 1
    Size += sizeof(Base_t);
#endif

    Address = (Base_t*) _driver_memory_alloc(Size);
    Assert(Address != NULL, ec_OutOfMemory); // could not allocate memory!

#if TTC_MEMORY_STATISTICS == 1
    ttc_memory_Bytes_Allocated_Usable += Size;
#endif
#if TTC_MEMORY_ENUMERATE == 1
    *Address = ++ttc_memory_Count_Alloc; // each memory block gets its own number at Address - sizeof(Base_t)
    Address++;
#endif

    return Address;
}
void* ttc_memory_alloc_zeroed(Base_t Size) {

    void* Address = ttc_memory_alloc(Size);
    ttc_memory_set(Address, 0, Size);

    return Address;
}
void ttc_memory_copy(void* Destination, const void* Source, Base_t AmountBytes) {
    Assert(Destination, ec_NULL);
    Assert(Source,      ec_NULL);

#if (TARGET_DATA_WIDTH == 32) // make use of 32-bit registers to speed up copying
    while (AmountBytes > 3) {
        *((u32_t*) Destination) = *((u32_t*) Source);
        Destination = (void*) (((u32_t) Destination) + 4);
        Source      = (void*) (((u32_t) Source)      + 4);
        AmountBytes -= 4;
    }
#else
#if (TARGET_DATA_WIDTH == 16) // make use of 16-bit registers to speed up copying
    while (AmountBytes > 1) {
        *((u16_t*) Destination) = *((u16_t*) Source);
        Destination = (void*) (((u32_t) Destination) + 2);
        Source      = (void*) (((u32_t) Source)      + 2);
        AmountBytes -= 2;
    }
#endif
#endif

    while (AmountBytes-- > 0) {
        *((u8_t*) Destination) = *((u8_t*) Source);
        Destination = (void*) (((u32_t) Destination) + 1);
        Source      = (void*) (((u32_t) Source)      + 1);
    }
}
void ttc_memory_set(void* Destination, Base_t Fill, Base_t AmountBytes) {
    Assert(Destination, ec_NULL);

#if (TARGET_DATA_WIDTH == 32) // make use of 32-bit registers to speed up copying
    if (AmountBytes > 11) {
        while ( ((Base_t) Destination) & 0x3) { // move to next 4-byte aligned address
            *((u8_t*) Destination) = (u8_t) (Fill & 0xff);
            Destination = (void*) ( ((u8_t*) Destination) + 1 );
            AmountBytes--;
        }
        while (AmountBytes > 15) { // fill 16 bytes in each iteration
            *((u32_t*) Destination) = Fill;
            Destination = (void*) (((u32_t) Destination) + 4);
            *((u32_t*) Destination) = Fill;
            Destination = (void*) (((u32_t) Destination) + 4);
            *((u32_t*) Destination) = Fill;
            Destination = (void*) (((u32_t) Destination) + 4);
            *((u32_t*) Destination) = Fill;
            Destination = (void*) (((u32_t) Destination) + 4);
            AmountBytes -= 16;
        }
        while (AmountBytes > 3) { // fill 4 bytes in each iteration
            *((u32_t*) Destination) = Fill;
            Destination = (void*) (((u32_t) Destination) + 4);
            AmountBytes -= 4;
        }
    }
#else
#if (TARGET_DATA_WIDTH == 16) // make use of 16-bit registers to speed up copying

    if (AmountBytes > 5) {
        while (AmountBytes > 1) {
            *((u16_t*) Destination) = (u16_t) Fill);
            Destination = (void*) (((u32_t) Destination) + 2);
            AmountBytes -= 2;

        }
    }

#endif
#endif

    while (AmountBytes-- > 0) {
        *((u8_t*) Destination) = (u8_t) Fill;
        Destination = (void*) (((u32_t) Destination) + 1);
    }
}
Base_t ttc_memory_compare(void* A, void* B, Base_t AmountBytes) {
    Base_t Index = 0;

#if (TARGET_DATA_WIDTH == 32) // make use of 32-bit registers to speed up copying
    if (AmountBytes > 11) {
        while (AmountBytes > 3) {
            if (*((u32_t*) A) != *((u32_t*) B) )  // words differ: will determine exact position later
                break;
            Index += 4;
            A += 4;
            B += 4;
            AmountBytes -= 4;
        }
    }
#endif
#if (TARGET_DATA_WIDTH == 16) // make use of 16-bit registers to speed up copying
    if (AmountBytes > 11) {
        while (AmountBytes > 3) {
            if (*((u32_t*) A) != *((u32_t*) B) ) // words differ: will determine exact position later
                break;
            Index += 2;
            A += 2;
            B += 2;
            AmountBytes -= 2;
        }
    }
#endif

    while (AmountBytes-- > 0) {
        if (*((u8_t*) A) != *((u8_t*) B) )
            return Index;
        A++;
        B++;
        Index++;
    }

    return 0;  // all checked bytes are the same
}
ttc_memory_block_t* ttc_memory_alloc_block(Base_t Size, u8_t Hint, void (*releaseBuffer)(ttc_memory_block_t* Block)) {
    ttc_memory_block_t* MemoryBlock = ttc_memory_alloc(sizeof(ttc_memory_block_t) + Size);

    MemoryBlock->Size          = 0;
    MemoryBlock->MaxSize       = Size;
    MemoryBlock->UseCount      = 1;
    MemoryBlock->Hint          = Hint;
    MemoryBlock->releaseBuffer = releaseBuffer;
    MemoryBlock->Buffer        = ( (u8_t*) MemoryBlock ) + sizeof(ttc_memory_block_t);

    return MemoryBlock;
}
ttc_memory_block_t* ttc_memory_alloc_virtual_block(u8_t* Buffer, u8_t Hint, void (*releaseBuffer)(ttc_memory_block_t* Block)) {
    Assert(Buffer != NULL, ec_InvalidArgument);
    ttc_memory_block_t* MemoryBlock = ttc_memory_alloc(sizeof(ttc_memory_block_t));

    MemoryBlock->Size          = 0;
    MemoryBlock->Hint          = Hint;
    MemoryBlock->releaseBuffer = releaseBuffer;
    MemoryBlock->Buffer        = Buffer;

    return MemoryBlock;
}
void ttc_memory_block_use(ttc_memory_block_t* Block) {
    Assert(Block->UseCount < 255, ec_UNKNOWN); // maybe endless loop calling ttc_memory_block_use() ?
    Block->UseCount++;
}
void ttc_memory_block_release(ttc_memory_block_t* Block) {
    Assert(ttc_memory_is_writable(Block), ec_InvalidArgument);

    Block->UseCount--;
    Assert(Block->UseCount != 255, ec_InvalidImplementation); // maybe you forgot to call ttc_memory_block_use() before reusing a release memory block?
    if (Block->UseCount == 0) {
        Assert(Block->releaseBuffer, ec_UNKNOWN);  // no release function stored!
        Block->releaseBuffer(Block);
    }
}
void ttc_memory_pool_block_free(ttc_memory_from_pool_t* Block) {
    Assert(Block, ec_NULL);
    Assert(ttc_memory_is_writable(Block), ec_UNKNOWN);

    // get reference to header before memory block
    Block--;
    ttc_memory_pool_t* Pool = Block->Pool;

    Assert(Pool, ec_NULL);
    Assert(ttc_memory_is_writable(Pool), ec_InvalidArgument);

#ifdef RT_SCHEDULER
    Assert(ttc_mutex_lock( &(Pool->Lock) , -1, ttc_memory_pool_block_free) == tme_OK, ec_UNKNOWN);
#endif
#ifdef TTC_MEMORY_DEBUG_POOL_RELEASE_PIN
    if (1) ttc_task_begin_criticalsection(ttc_memory_pool_block_free);
    ttc_gpio_set(TTC_MEMORY_DEBUG_POOL_RELEASE_PIN);
#endif

    // insert memory block at begin of list of free blocks
    Block->ListItem.Next = (struct ttc_list_item_s*) Pool->FirstFree;
    Pool->FirstFree = Block;

#if TTC_MEMORY_POOL_STATISTICS == 1
    Pool->Statistics.AmountFreeCurrently++;  // statistics
    if (Pool->Statistics.AmountFreeMaximum < Pool->Statistics.AmountFreeCurrently) // statistics
        Pool->Statistics.AmountFreeMaximum = Pool->Statistics.AmountFreeCurrently; // statistics
#endif

#ifdef TTC_MEMORY_DEBUG_POOL_RELEASE_PIN
    ttc_gpio_clr(TTC_MEMORY_DEBUG_POOL_RELEASE_PIN);
    if (1) ttc_task_end_criticalsection();
#endif

    ttc_semaphore_give( &(Pool->BlocksAvailable), 1);
#ifdef RT_SCHEDULER
    ttc_mutex_unlock( &(Pool->Lock) );
#endif
}
ttc_memory_from_pool_t* ttc_memory_pool_block_get(ttc_memory_pool_t* Pool) {

    Assert(Pool, ec_NULL);
    Assert(ttc_memory_is_writable(Pool), ec_UNKNOWN);

    ttc_semaphore_take(&(Pool->BlocksAvailable), 1, -1, ttc_memory_pool_block_get);

#ifdef RT_SCHEDULER
    Assert(ttc_mutex_lock(&(Pool->Lock), -1, ttc_memory_pool_block_get) == tme_OK, ec_UNKNOWN);
#endif
    ttc_memory_from_pool_t* NewBlock = Pool->FirstFree;
#ifdef TTC_MEMORY_DEBUG_POOL_ALLOC_PIN
    if (1) ttc_task_begin_criticalsection(ttc_memory_pool_block_get);
    ttc_gpio_set(TTC_MEMORY_DEBUG_POOL_ALLOC_PIN);
#endif

#if TTC_MEMORY_POOL_STATISTICS == 1
    Pool->Statistics.AmountFreeTotal += Pool->Statistics.AmountFreeCurrently;
    Pool->Statistics.AmountUseTotal++;
#endif

    if (NewBlock) {   // reusing a released memory block
        Pool->FirstFree = (ttc_memory_from_pool_t*) NewBlock->ListItem.Next;
        NewBlock->ListItem.Next = NULL;
#if TTC_MEMORY_POOL_STATISTICS == 1
        Pool->Statistics.AmountFreeCurrently--;  // statistics
#endif

        Assert(NewBlock->Pool, ec_NULL);
        Assert(ttc_memory_is_writable(NewBlock->Pool), ec_UNKNOWN);
    }
    if (! NewBlock) { // allocate new memory block
        NewBlock = ttc_memory_alloc(sizeof(ttc_memory_from_pool_t) + Pool->BlockSize);
        if (NewBlock) {
#if TTC_MEMORY_STATISTICS == 1
            ttc_memory_Bytes_Allocated_Usable -= sizeof(ttc_memory_from_pool_t); // correct statistics
            ttc_memory_Bytes_Allocated_Usable += sizeof(NewBlock->ListItem);
#endif
            NewBlock->Pool = Pool;
            NewBlock->ListItem.Next = NULL;
#if TTC_MEMORY_ENUMERATE == 1
            NewBlock->Enum = (u16_t) ttc_memory_Count_PoolAlloc++; // every memory block gets its own
#endif
#ifdef TTC_MEMORY_MAGICKEY
            NewBlock->MagicKey = TTC_MEMORY_MAGICKEY;
#endif
#if TTC_MEMORY_POOL_STATISTICS == 1
            Pool->Statistics.AmountAllocated++;  // statistics
#endif
        }
    }

#ifdef TTC_MEMORY_DEBUG_POOL_ALLOC_PIN
    ttc_gpio_clr(TTC_MEMORY_DEBUG_POOL_ALLOC_PIN);
    if (1) ttc_task_end_criticalsection();
#endif
#ifdef RT_SCHEDULER
    ttc_mutex_unlock( &(Pool->Lock) );
#endif

    if (NewBlock)
        return NewBlock + 1; // return address of allocated buffer
    return NULL;             // no more memory blocks available
}
ttc_memory_pool_t* ttc_memory_pool_create(TTC_MEMORY_BLOCK_BASE Size, Base_t MaxAllocated) {

    ttc_memory_pool_t* NewPool = (ttc_memory_pool_t*) ttc_memory_alloc(sizeof(ttc_memory_pool_t) + Size);
    if (NewPool)
        ttc_memory_pool_init(NewPool, Size, MaxAllocated);

#if TTC_MEMORY_STATISTICS == 1
    ttc_memory_Bytes_Allocated_Usable -= sizeof(ttc_memory_pool_t); // correct statistics
#endif

    return NewPool;
}
void ttc_memory_pool_init(ttc_memory_pool_t* Pool, TTC_MEMORY_BLOCK_BASE Size, Base_t MaxAllocated) {
    Assert(Pool, ec_InvalidArgument);
    Assert(Size, ec_InvalidArgument);

    ttc_memory_set(Pool, 0, sizeof(*Pool));
    Pool->BlockSize = Size;
    Pool->FirstFree = NULL;
#ifdef RT_SCHEDULER
    ttc_mutex_init( &(Pool->Lock) );
#endif
    ttc_semaphore_init( &(Pool->BlocksAvailable) );
    ttc_semaphore_give(&(Pool->BlocksAvailable), MaxAllocated);
}
ttc_list_item_t* ttc_memory_pool_to_list_item(ttc_memory_from_pool_t* Block) {
    if (Block == NULL)
        return NULL;

    Block--;
#ifdef TTC_MEMORY_MAGICKEY
    Assert(Block->MagicKey == TTC_MEMORY_MAGICKEY, ec_InvalidImplementation); // wrong magic-key: this is not a valid memory block
#endif

    return &(Block->ListItem);
}
ttc_memory_from_pool_t* ttc_memory_pool_from_list_item(ttc_list_item_t* Item) {
    if (Item == NULL)
        return NULL;

    ttc_memory_from_pool_t* Block = ( (ttc_memory_from_pool_t*) Item );

#ifdef TTC_MEMORY_MAGICKEY
    Assert(ttc_memory_is_writable(Block), ec_InvalidImplementation); // address outside RAM: this cannot be a valid memory block
    Assert(Block->MagicKey == TTC_MEMORY_MAGICKEY, ec_InvalidImplementation); // wrong magic-key: this is not a valid memory block
#endif

    return Block + 1;
}

void* _ttc_array_ref(Base_t* Size, Base_t Index) {
    Assert(Index < *Size, ec_ArrayIndexOutOfBound);
    return Size+1; // return pointer to first array element
}
Base_t _ttc_array_index(Base_t* Size, Base_t Index) {
    Assert(Index < *Size, ec_ArrayIndexOutOfBound);
    return Index;
}
void _ttc_memory_register_block(void* Address, Base_t Size) {
    (void) Address;
    (void) Size;
    ttc_task_begin_criticalsection(_ttc_memory_register_block);

#if TTC_MEMORY_STATISTICS == 1
    ttc_memory_Amount_Blocks_Allocated++;
    ttc_memory_Bytes_Allocated_Total += Size;
#endif
#if TTC_MEMORY_RECORDS > 0

    static ttc_memory_record_t* PreviousRecord;
    static ttc_memory_record_t* CurrentRecord;

    if (ttc_memory_first_free_record > 0) {
        CurrentRecord  = &( A(ttc_memory_Records, ttc_memory_first_free_record) );
        PreviousRecord = CurrentRecord - 1;
        if (ttc_memory_first_free_record < TTC_MEMORY_RECORDS - 1)
            ttc_memory_first_free_record++;
        else
            ttc_memory_first_free_record = 0;
    }
    else {
        CurrentRecord  = &( A(ttc_memory_Records, ttc_memory_first_free_record) );
        PreviousRecord = &( A(ttc_memory_Records, TTC_MEMORY_RECORDS - 1) );
        ttc_memory_first_free_record++;
    }
    CurrentRecord->Enum    = PreviousRecord->Enum + 1;
    CurrentRecord->Address = Address;
    CurrentRecord->Size    = Size;

#if TTC_MEMORY_STATISTICS == 1
    Assert(ttc_memory_Amount_Blocks_Allocated == CurrentRecord->Enum, ec_InvalidArgument); // One counter increased somewhere else?
#endif
#endif
    ttc_task_end_criticalsection();
}
void _ttc_memory_unregister_block(void* Address, Base_t Size) {
    (void) Address;
    (void) Size;

#if TTC_MEMORY_STATISTICS == 1
    ttc_memory_Amount_Blocks_Allocated--;
    ttc_memory_Bytes_Allocated_Total  -= Size;
    ttc_memory_Bytes_Allocated_Usable -= Size;
#endif
}
