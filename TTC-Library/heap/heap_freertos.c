/** { heap_freertos.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for heap devices on freertos architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 21 at 20140303 09:14:17 UTC
 *
 *  Note: See ttc_heap.h for description of freertos independent HEAP implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "heap_freertos.h"

//{ Global Variables ***********************************************************

#if TTC_MEMORY_STATISTICS == 1

    // amount of blocks allocated on heap
    extern t_base ttc_memory_Amount_Blocks_Allocated;

    // amount of bytes occupied on heap by dynamic allocations
    extern t_base ttc_memory_Bytes_Allocated_Total;

    // usable amount of allocated bytes
    extern t_base ttc_memory_Bytes_Allocated_Usable;

#endif

//}Global Variables
//{ Function Definitions *******************************************************

void*  heap_freertos_alloc( t_base Size ) {
    return pvPortMalloc( Size );
}
void*  heap_freertos_temporary_alloc( t_base Size ) {
    TODO( "get address of next free memory block of given size!" )
    return NULL;
}
void*  heap_freertos_free( void* Block ) {
    Assert_HEAP_Writable( Block, ttc_assert_origin_auto );  // pointers must not be NULL

    vPortFree( Block );
    return ( void* ) 0;
}
void   heap_freertos_prepare( volatile t_u8* HeapStart, t_base HeapSize ) {
    ( void ) HeapStart;
    ( void ) HeapSize;

    // initialization is done by FreeRTOS
}
t_base heap_freertos_get_free_size() {
    return ( t_base ) xPortGetFreeHeapSize();
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
