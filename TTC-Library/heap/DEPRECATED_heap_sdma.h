#ifndef heap_sdma_H
#define heap_sdma_H

/** { heap_sdma.h ************************************************
 
 Dynamic memory allocator "Smart Dynamic Memory Allocator" +
 as described by Ramakrishna M, Jisung Kim, Woohyong Lee and Youngki Chung. Smart Dynamic Memory Allocator for Embedded Systems, 2008

 written by Gregor Rebel 2013

}*/
//{ Defines/ TypeDefs ****************************************************
// MsGizma

//} Defines
//{ Includes *************************************************************

#include "../ttc_basic.h"

#ifdef EXTENSION_300_scheduler_freertos
#  include "FreeRTOS.h"
#  include "task.h"
#  include "portable.h"
#endif

//} Includes
//{ Structures/ Enums ****************************************************

extern Base_t heap_sdma_FreeSize;

typedef struct heap_sdma_free_block_s {
       unsigned Size    : 30;         // block size (multiple of 4)
       unsigned IsShort : 1;          // ==1: short lived block/ ==0: long lived block
       unsigned IsFree  : 1;          // ==1: block is free/ ==0: block is used
struct heap_sdma_free_block_s* Prev_Physical_BlkPtr;
struct heap_sdma_free_block_s* Next_Physical_BlkPtr;
          void* Prev_FreeListPtr;
          void* Next_FreeListPtr;
} heap_sdma_free_block_t;

typedef struct heap_sdma_used_block_s {
       unsigned Size    : 30;         // block size (multiple of 4)
       unsigned IsShort : 1;          // ==1: short lived block/ ==0: long lived block
       unsigned IsFree  : 1;          // ==1: block is free/ ==0: block is used
struct heap_sdma_used_block_s* Prev_Physical_BlkPtr;
struct heap_sdma_used_block_s* Next_Physical_BlkPtr;
          u32_t BlkAllocStat;
} heap_sdma_used_block_t;

typedef struct {
    heap_sdma_free_block_t* ShortLivedBlocks;
    heap_sdma_free_block_t* LongLivedBlocks;
} heap_sdma_free_list_set_t;

// amount of heap_sdma_free_list_set_t entries in each list set
#define HEAP_SDMA_SIZE_LIST_SET 8

// log2(HEAP_SDMA_SIZE_LIST_SET)
#define HEAP_SDMA_BITS_LIST_SET 3

//} Structures/ Enums
//{ driver interface for ttc_memory.h ************************************

#define _driver_memory_alloc(Size)      heap_sdma_alloc(Size)
#define _driver_memory_free(Block)      heap_sdma_free(Block)
#define _driver_memory_get_free_size()  heap_sdma_FreeSize
#define _driver_memory_init(HeapStart, HeapSize) heap_sdma_init(HeapStart, HeapSize);

//} Global Variables
//{ Includes2 ************************************************************

#include "../ttc_mutex.h"
#include "../ttc_memory.h"

//} Includes2
//{ Function prototypes **************************************************

/** Initializes memory allocator - Call before using any other function from this source
  *
  * Gets called automatically by extension initializer code
  */
void heap_sdma_init(u8_t* HeapStart, Base_t HeapSize);

/** tries to allocate a memory area of given size
 *
 * @param Size   size of memory block to allocate
 * @return       =! NULL: address of allocated memory block; == NULL: memory could not be allocated (out of memory)
 */
void* heap_sdma_alloc(Base_t Size);

/** returns given memory area to allocator for later reallocation
 *
 * @param Block  address of memory block to free (must be allocated by heap_sdma_alloc() before!)
 */
void heap_sdma_free(void* Block);

//} Function prototypes

#endif //heap_sdma_H
