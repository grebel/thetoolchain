#ifndef HEAP_ZDEFAULT_H
#define HEAP_ZDEFAULT_H

/** { heap_zdefault.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for heap devices on zdefault architectures.
 *  Structures, Enums and Defines being required by high-level heap and application.
 *
 *  Created from template device_architecture.h revision 21 at 20140301 07:31:49 UTC
 *
 *  Note: See ttc_heap.h for description of zdefault independent HEAP implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_HEAP_ZDEFAULT
//
// Implementation status of low-level driver support for heap devices on zdefault
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_HEAP_ZDEFAULT '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_HEAP_ZDEFAULT == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_HEAP_ZDEFAULT to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_HEAP_ZDEFAULT

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "heap_zdefault_types.h"
#include "../ttc_heap_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_heap_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_<device>_prepare
//
#undef  ttc_driver_heap_alloc_zeroed
#define ttc_driver_heap_alloc(Size)                  heap_zdefault_alloc(Size)
#define ttc_driver_heap_temporary_alloc(Size)        heap_zdefault_temporary_alloc(Size)
#define ttc_driver_heap_free(Block)                  heap_zdefault_free(Block)
#define ttc_driver_heap_get_free_size()              heap_zdefault_get_free_size()
#define ttc_driver_heap_prepare(HeapStart, HeapSize) heap_zdefault_prepare(HeapStart, HeapSize)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_heap.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_heap.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */



/** tries to allocate a memory area of given size and fills it with zero-bytes.
 * Note: For many setups, the allocated memory block cannot be freed!
 * Note: All ttc_malloc() functions will call ttc_assert_halt_origin(ttc_assert_origin_auto) if allocation fails.
 *
 * @param Size   size of memory block to allocate
 * @return       =! NULL: address of allocated memory block; == NULL: memory could not be allocated (out of memory)
 */
void* heap_zdefault_alloc( t_base Size );

/** Provides releasable dynamic memory for a short time frame.
 *
 * This is a very lightweight function that only calculates start of next memory chunk.
 * The idea is that the caller "knows" that no one else wants to allocate memory before the
 * temporary chunk is given back.
 *
 * Note: Allocated memory must be given back befor any other ttc_heap_alloc*() call using ttc_heap_release_temporary()!
 *
 * @param Size   size of memory block to allocate
 * @return       =! NULL: address of allocated memory block; == NULL: memory could not be allocated (out of memory)
 */
void* heap_zdefault_temporary_alloc( t_base Size );


/** frees given memory area
 * Note: For many setups, ttc_free() does not provide an implementation!
 *
 * @param Block  address of memory block to free (must be allocated by ttc_heap_alloc() before!)
 * @return       ==NULL: block has been freed; ==Block: block has not been freed
 */
void* heap_zdefault_free( void* Block );


/** returns amount of available dynamic memory
 *
 * @return       amount of bytes available for allocation
 */
t_base heap_zdefault_get_free_size();



/** Prepares heap driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 *
 * @param HeapStart  points to lowest address of memory block to be used as heap
 * @param HeapSize   amount of bytes to be used for heap
 */
void heap_zdefault_prepare( volatile t_u8* HeapStart, t_base HeapSize );


//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _heap_zdefault_foo(t_ttc_heap_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //HEAP_ZDEFAULT_H
