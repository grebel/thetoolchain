/** { heap_sdma.c ************************************************
 
 Dynamic memory allocator "Smart Dynamic Memory Allocator" +
 as described by Ramakrishna M, Jisung Kim, Woohyong Lee and Youngki Chung. Smart Dynamic Memory Allocator for Embedded Systems, 2008
 
 written by Gregor Rebel 2013

}*/

#include "heap_sdma.h"

//{ Global variables *****************************************************

// variables defined in ttc_memory.c
extern u8_t* ttc_memory_HeapStart;
extern Base_t ttc_memory_HeapSize;

// amount of bytes currently free to allocate
Base_t heap_sdma_FreeSize = 0;

// points to start of big memory block used for dynamic memory allocations
static u8_t* hs_Heap = NULL;

// size of hs_Heap[] (bytes)
static Base_t hs_Heap_Size = 0;

// begin of unused heap memory
static u8_t* hs_Heap_FirstFree = NULL;

// end of unused heap memory
static u8_t* hs_Heap_FirstNonFree = NULL;

// mask used to decide wether a block size belongs to a short-/ long-lived block
static u16_t hs_BlockPredictorMask = 0;

// array storing pointers to free list sets
static heap_sdma_free_list_set_t** hs_FreeListClasses = NULL;

// size of hs_FreeListClasses
static Base_t hs_FreeListClasses_Amount = 0;

// array storing free list sets
static heap_sdma_free_list_set_t* hs_FreeListSets = NULL;

// size of hs_FreeListSets (8x hs_FreeListClasses_Amount)
static Base_t hs_FreeListSets_Amount = 0;

// first level bit masks
static Base_t hs_Mask1_LongLivedBlocks;
static Base_t hs_Mask1_ShortLivedBlocks;

// arrays of second level bit masks (allocated dynamically according to heap size)
static u8_t* hs_Mask2_LongLivedBlocks;
static u8_t* hs_Mask2_ShortLivedBlocks;

// converts size byte -> first-level-index (index of Most Significant Bit)
static const u8_t hs_LookupTableByte1[256] = {
 0xff, 0, 1, 1, 2, 2, 2, 2,
    3, 3, 3, 3, 3, 3, 3, 3,
    4, 4, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 4, 4, 4, 4, 4,
    5, 5, 5, 5, 5, 5, 5, 5,
    5, 5, 5, 5, 5, 5, 5, 5,
    5, 5, 5, 5, 5, 5, 5, 5,
    5, 5, 5, 5, 5, 5, 5, 5,
    6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7
};

// converts size byte -> second-level-index (index of Least Significant Bit)
static const u8_t hs_LookupTableByte2[256] = {
    0xff, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       4, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       5, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       4, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       6, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       4, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       5, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       4, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       0, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       4, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       5, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       4, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       6, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       4, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       5, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0,
       4, 0, 1, 0, 2, 0, 1, 0,
       3, 0, 1, 0, 2, 0, 1, 0
};

// increased on every block allocation
static Base_t hs_Global_Alloc_Number = 0;

// increased on every block allocation
static Base_t hs_Global_Free_Number = 0;

// increased on every block allocation
static Base_t hs_Global_Reused_Number = 0;

// maximum seen age of a block (amount of allocations since its allocation)
static Base_t hs_Global_Max_Lifetime = 16;

// reference of latest memory block allocated by _heap_sdma_alloc_bottom()
static heap_sdma_used_block_t* hs_Last_Bottom_Alloc_Block = NULL;

// reference of latest memory block allocated by _heap_sdma_alloc_top()
static heap_sdma_used_block_t* hs_Last_Top_Alloc_Block = NULL;

// protects memory allocator from concurrent accesses
static ttc_mutex_smart_t hs_Lock;

#ifdef TTC_REGRESSION

// amount of blocks allocated on heap
extern Base_t ttc_memory_Amount_Blocks_Allocated;

// amount of bytes occupied on heap by dynamic allocations
extern Base_t ttc_memory_Bytes_Allocated_Total;

// usable amount of allocated bytes
extern Base_t ttc_memory_Bytes_Allocated_Usable;

#endif

//} Global variables
//{ Private Function prototypes *****************************************

/** reserves given amount of memory from bottom of heap
  *
  * @param ListHeadPtr reference of pointer to head of free block list (set to NULL if list is empty)
  * @return !=NULL: reference of removed item
  */
heap_sdma_free_block_t* _heap_sdma_remove_from_list(heap_sdma_free_block_t** ListHeadPtr);

/** reserves given amount of memory from bottom of heap
  *
  * @param ListHead       pointer to head of free block list
  * @param Item           memory block to add to given list
  * @return               values of ListHead (updated if required)
  */
heap_sdma_free_block_t* _heap_sdma_add_to_list(heap_sdma_free_block_t* ListHead, heap_sdma_free_block_t* Item);

/** allocates + initializes a new memory block of given size from bottom of heap
  *
  * @param Size   size of memory block to reserve
  * @return != NULL: address of allocated memory block
  */
heap_sdma_free_block_t* _heap_sdma_alloc_bottom(Base_t Size);

/** allocates + initializes a new memory block of given size from top of heap
  *
  * @param Size   size of memory block to reserve
  * @return != NULL: address of allocated memory block
  */
heap_sdma_free_block_t* _heap_sdma_alloc_top(Base_t Size);

/** reserves given amount of memory from bottom of heap
  *
  * @param Size   size of memory block to reserve
  * @return != NULL: address of allocated memory block
  */
void* _heap_sdma_reserve_bottom(Base_t Size);

/** reserves given amount of memory from top of heap
  *
  * @param Size   size of memory block to reserve
  * @return != NULL: address of allocated memory block
  */
void* _heap_sdma_reserve_top(Base_t Size);

/** calculates index of most significant, non zero bit in given size
  *
  * @param Size   size of memory block to reserve
  * @return       1..31 bit index
  */
u8_t _heap_sdma_calculate_first_level_index(Base_t Size);

/** calculates second level index for given size
  *
  * @param Size            size of memory block to reserve
  * @param FirstLevelIndex return value from _heap_sdma_calculate_first_level_index()
  * @param ShortLived      == TRUE: calculate index of a short-lived block; long-lived otherwise
  * @return                0..31 second level index
  */
u8_t _heap_sdma_calculate_second_level_index(Base_t Size, u8_t FirstLevelIndex, BOOL ShortLived);

/** counts size of given double linked list from given item backwards to start of list
  *
  * @param Item      end of double linked list of memory blocks
  * @return          amount of non NULL blocks in given reverse list
  */
Base_t _heap_sdma_count_backwards(heap_sdma_used_block_t* Item) {
    Base_t Amount = 0;

    while (Item) {
        Item = Item->Prev_Physical_BlkPtr;
        Amount++;
    }

    return Amount;
}

//} Private Function prototypes
//{ Function definitions *************************************************

 void heap_sdma_init(u8_t* HeapStart, Base_t HeapSize) {
    hs_Heap_Size = HeapSize;
    Assert(hs_Heap_Size < ttc_memory_HeapSize, ec_OutOfResources); // not egnough memory available!
#ifdef EXTENSION_300_scheduler_freertos
    hs_Heap = HeapStart; // ToDo: let FreeRTOS use heap managed by heap_sdma
    Assert(hs_Heap, ec_UNKNOWN);
#endif
    while (hs_Heap_Size & 3) // make size a factor of 4
        hs_Heap_Size--;
    hs_Heap_FirstFree    = hs_Heap;
    hs_Heap_FirstNonFree = hs_Heap + hs_Heap_Size;
    heap_sdma_FreeSize   = hs_Heap_Size;

    // create one free list class for each bit of amount of available memory
    Base_t N = hs_Heap_Size - 1;
    while (N) { // hs_FreeListClasses_Amount = log2(hs_Heap_Size-1) )
        hs_FreeListClasses_Amount++;
        N /= 2;
    }
    hs_FreeListClasses = (heap_sdma_free_list_set_t**) _heap_sdma_reserve_bottom( sizeof(void*) * hs_FreeListClasses_Amount );
    hs_Mask2_LongLivedBlocks  = (u8_t*) _heap_sdma_reserve_bottom( sizeof(u8_t) * hs_FreeListClasses_Amount );
    ttc_memory_set(hs_Mask2_LongLivedBlocks, 0, hs_FreeListClasses_Amount);
    hs_Mask2_ShortLivedBlocks = (u8_t*) _heap_sdma_reserve_bottom( sizeof(u8_t) * hs_FreeListClasses_Amount );
    ttc_memory_set(hs_Mask2_ShortLivedBlocks, 0, hs_FreeListClasses_Amount);

    hs_BlockPredictorMask = (1 << (hs_FreeListClasses_Amount/2)); // set lower half of bits required to represent largest possible memory block

    // create free list sets (HEAP_SDMA_SIZE_LIST_SET sets for each class)
    hs_FreeListSets_Amount = hs_FreeListClasses_Amount * HEAP_SDMA_SIZE_LIST_SET;
    hs_FreeListSets = (heap_sdma_free_list_set_t*) _heap_sdma_reserve_bottom( sizeof(heap_sdma_free_list_set_t) * hs_FreeListSets_Amount );
    ttc_memory_set(hs_FreeListSets, 0, sizeof(heap_sdma_free_list_set_t) * hs_FreeListSets_Amount);
    for (u8_t Class = 0; Class < hs_FreeListClasses_Amount; Class++) { // fill hs_FreeListClasses[] with pointers to first entry in corresponding set
        hs_FreeListClasses[Class] = hs_FreeListSets + (Class * HEAP_SDMA_SIZE_LIST_SET);
        hs_FreeListSets = hs_FreeListSets; //D
    }

    ttc_mutex_init(&hs_Lock);
}
void* heap_sdma_alloc(Base_t Size) {

    Assert(hs_Heap_FirstFree, ec_InvalidImplementation); // call heap_sdma_init() before using this memory allocator!
    ttc_mutex_lock(&hs_Lock, -1, heap_sdma_alloc);
    ttc_task_begin_criticalsection(heap_sdma_alloc);

#ifdef TTC_MEMORY_DEBUG_POOL_ALLOC_PIN
    ttc_gpio_set(TTC_MEMORY_DEBUG_POOL_ALLOC_PIN);
#endif

    hs_Global_Alloc_Number++;
    while (Size & 0x3) // pad size to be a multiple of 4
        Size++;
    u8_t FirstLevelIndex   = _heap_sdma_calculate_first_level_index(Size);
    BOOL IsShortLivedBlock = (hs_BlockPredictorMask & 1 << FirstLevelIndex);
    u8_t SecondLevelIndex  = _heap_sdma_calculate_second_level_index(Size, FirstLevelIndex, IsShortLivedBlock);

    heap_sdma_used_block_t* NewBlock = NULL;
    if (IsShortLivedBlock) {
        if (SecondLevelIndex != 0xff) {
            if ( hs_Mask1_ShortLivedBlocks & (1 << FirstLevelIndex) ) {
                u8_t Mask2 = hs_Mask2_ShortLivedBlocks[FirstLevelIndex];
                if (Mask2 & (1 << SecondLevelIndex)) { // block is available: reuse it
                    heap_sdma_free_list_set_t* FreeListSet = hs_FreeListClasses[FirstLevelIndex];
                    heap_sdma_free_block_t** ListHeadPtr = &(FreeListSet[SecondLevelIndex].ShortLivedBlocks);
                    NewBlock = (heap_sdma_used_block_t*) _heap_sdma_remove_from_list(ListHeadPtr);
                    if (*ListHeadPtr == NULL) { // list is empty: clear bitmask
                        hs_Mask1_ShortLivedBlocks &= 0xffffffff ^ (1 << FirstLevelIndex);
                    }
                }
            }
        }
        if (NewBlock == NULL) { // no reusable block found: allocate new one
            NewBlock = (heap_sdma_used_block_t*) _heap_sdma_alloc_bottom(Size);
            Assert(NewBlock, ec_OutOfMemory); // no more memory available
            NewBlock->IsShort = 1; // short lived block
        }
        else
            hs_Global_Reused_Number++;
    }
    else {
        if (SecondLevelIndex != 0xff) {
            if ( hs_Mask1_LongLivedBlocks & (1 << FirstLevelIndex) ) {
                u8_t Mask2 = hs_Mask2_LongLivedBlocks[FirstLevelIndex];
                if (Mask2 & (1 << SecondLevelIndex)) { // block is available: reuse it
                    heap_sdma_free_list_set_t* FreeListSet = hs_FreeListClasses[FirstLevelIndex];
                    heap_sdma_free_block_t** ListHeadPtr = &(FreeListSet[SecondLevelIndex].LongLivedBlocks);
                    NewBlock = (heap_sdma_used_block_t*) _heap_sdma_remove_from_list(ListHeadPtr);
                    if (*ListHeadPtr == NULL) { // list is empty: clear bitmask
                        hs_Mask1_LongLivedBlocks &= 0xffffffff ^ (1 << FirstLevelIndex);
                    }
                }
            }
        }
        if (NewBlock == NULL) { // no reusable block found: allocate new one
            NewBlock = (heap_sdma_used_block_t*) _heap_sdma_alloc_bottom(Size);
            Assert(NewBlock, ec_OutOfMemory);
            NewBlock->IsShort = 0; // short lived block
        }
        else
            hs_Global_Reused_Number++;
    }

    NewBlock->BlkAllocStat = hs_Global_Alloc_Number;
    NewBlock->Size         = Size;
    NewBlock->IsFree       = 0;

#ifdef TTC_MEMORY_DEBUG_POOL_ALLOC_PIN
    ttc_gpio_clr(TTC_MEMORY_DEBUG_POOL_ALLOC_PIN);
#endif
    ttc_task_end_criticalsection();
    ttc_mutex_unlock(&hs_Lock);

    return (void*) (NewBlock + 1);
}
 void heap_sdma_free(void* Block) {
     ttc_mutex_lock(&hs_Lock, -1, heap_sdma_alloc);
     ttc_task_begin_criticalsection(heap_sdma_alloc);

#ifdef TTC_MEMORY_DEBUG_POOL_RELEASE_PIN
  ttc_gpio_set(TTC_MEMORY_DEBUG_POOL_RELEASE_PIN);
#endif

     heap_sdma_used_block_t* UsedBlock = (heap_sdma_used_block_t*) Block;
     UsedBlock--; // point to header of memory block

     Base_t Age = hs_Global_Alloc_Number - UsedBlock->BlkAllocStat;
     if (hs_Global_Max_Lifetime < Age)
         hs_Global_Max_Lifetime = Age;
//? #warning ToDo: update hs_BlockPredictorMask

     heap_sdma_free_block_t* FreeBlock = (heap_sdma_free_block_t*) UsedBlock;
     FreeBlock->IsFree = 1;

     u8_t FirstLevelIndex = _heap_sdma_calculate_first_level_index(FreeBlock->Size);

     if (UsedBlock->IsShort) {
         u8_t SecondLevelIndex  = _heap_sdma_calculate_second_level_index(FreeBlock->Size, FirstLevelIndex, TRUE);
         if (SecondLevelIndex == 0xff)
             SecondLevelIndex = 0; //?
         heap_sdma_free_list_set_t* FreeListSet = hs_FreeListClasses[FirstLevelIndex];
         heap_sdma_free_block_t* ListHead = FreeListSet[SecondLevelIndex].ShortLivedBlocks;
         ListHead = _heap_sdma_add_to_list(ListHead, FreeBlock);
         FreeListSet[SecondLevelIndex].ShortLivedBlocks = ListHead;

         // update bitmasks
         hs_Mask1_ShortLivedBlocks |= (1 << FirstLevelIndex);
         hs_Mask2_ShortLivedBlocks[FirstLevelIndex] |= (1 << SecondLevelIndex);
     }
     else {
         u8_t SecondLevelIndex  = _heap_sdma_calculate_second_level_index(FreeBlock->Size, FirstLevelIndex, FALSE);
         (void) SecondLevelIndex;
         Assert_Halt_EC(ec_NotImplemented); // ToDo
//? #warning ToDo: implement freeing long lived objects
//? #warning ToDo: merge neighboured blocks
     }
     hs_Global_Free_Number++;

#ifdef TTC_MEMORY_DEBUG_POOL_RELEASE_PIN
  ttc_gpio_clr(TTC_MEMORY_DEBUG_POOL_RELEASE_PIN);
#endif
     ttc_task_end_criticalsection();
     ttc_mutex_unlock(&hs_Lock);
}
heap_sdma_free_block_t* _heap_sdma_remove_from_list(heap_sdma_free_block_t** ListHeadPtr) {
     Assert(ListHeadPtr, ec_InvalidArgument);
     heap_sdma_free_block_t* Item = NULL;

     if (*ListHeadPtr) {
#ifdef TARGET_ARCHITECTURE_STM32F1xx
         Assert(( ((u32_t) *ListHeadPtr) > 0x20000000) && ( ((u32_t) *ListHeadPtr) < 0x20010000), ec_InvalidImplementation);
#endif
         Item = *ListHeadPtr;
         heap_sdma_free_block_t* NewHeadItem = Item->Next_FreeListPtr;
         if (NewHeadItem) {
#ifdef TARGET_ARCHITECTURE_STM32F1xx
             Assert(( ((u32_t) NewHeadItem) > 0x20000000) && ( ((u32_t) NewHeadItem) < 0x20010000), ec_InvalidImplementation);
#endif
             NewHeadItem->Prev_FreeListPtr = NULL;
         }
         *ListHeadPtr = NewHeadItem;
     }

     return Item;
 }
 heap_sdma_free_block_t* _heap_sdma_add_to_list(heap_sdma_free_block_t* ListHead, heap_sdma_free_block_t* Item) {

     if (ListHead) {
#ifdef TARGET_ARCHITECTURE_STM32F1xx
         Assert(( ((u32_t) ListHead) > 0x20000000) && ( ((u32_t) ListHead) < 0x20010000), ec_InvalidImplementation);
#endif
         heap_sdma_free_block_t* HeadItem = ListHead;
         ListHead = Item;
         HeadItem->Prev_FreeListPtr = Item;
         Item->Next_FreeListPtr = HeadItem;
     }
     else {
         ListHead = Item;
         Item->Next_FreeListPtr = NULL;
         Item->Prev_FreeListPtr = NULL;
     }
#ifdef TARGET_ARCHITECTURE_STM32F1xx
     Assert(( ((u32_t) ListHead) > 0x20000000) && ( ((u32_t) ListHead) < 0x20010000), ec_InvalidImplementation);
#endif

     return ListHead;
 }
 u8_t _heap_sdma_calculate_first_level_index(Base_t Size) {
    Assert(Size, ec_InvalidArgument);

    u8_t BitShift = 24;
    u8_t Byte = 0xff & (Size >> BitShift);
    u8_t FirstLevelIndex = hs_LookupTableByte1[Byte];

    while (FirstLevelIndex == 0xff) {
        BitShift -= 8;
        Byte = 0xff & (Size >> BitShift);
        FirstLevelIndex = hs_LookupTableByte1[Byte];
    }

    FirstLevelIndex += BitShift;

    Assert(FirstLevelIndex < hs_FreeListClasses_Amount, ec_InvalidImplementation);
    return FirstLevelIndex;
}
 u8_t _heap_sdma_calculate_second_level_index(Base_t Size, u8_t FirstLevelIndex, BOOL ShortLived) {
    Assert(Size, ec_InvalidArgument);
    u8_t SecondLevelIndex = 0;

    if (0) { // implemented from source code example in paper (does not work)
        FirstLevelIndex++;
        Base_t Mask;
        if (ShortLived)
            Mask = hs_Mask1_ShortLivedBlocks >> FirstLevelIndex;
        else
            Mask = hs_Mask1_LongLivedBlocks  >> FirstLevelIndex;

        u8_t Temp = hs_LookupTableByte2[0xff & Mask];
        while (Temp == 0xff) {
            Mask = Mask >> 8;
            if (Mask == 0)
                return 0xff; // out of memory: get new memory block from OS
            Temp = hs_LookupTableByte2[0xff & Mask];
            FirstLevelIndex += 8;
        }

        Assert(FirstLevelIndex < hs_FreeListClasses_Amount, ec_InvalidImplementation);
        if (ShortLived) {
            SecondLevelIndex = hs_LookupTableByte2[ hs_Mask2_ShortLivedBlocks[FirstLevelIndex] ];
        }
        else {
            SecondLevelIndex = hs_LookupTableByte2[ hs_Mask2_LongLivedBlocks[FirstLevelIndex] ];
        }
    }
    else { // implemented according to textual description in paper (text below figure 5)

        // SecondLevelIndex = next HEAP_SDMA_SIZE_LIST_SET number of bits of Size after first MSB
        if (FirstLevelIndex < HEAP_SDMA_BITS_LIST_SET)
            SecondLevelIndex = Size ^ (1 << FirstLevelIndex);
        else {
            u8_t BitShift = FirstLevelIndex - HEAP_SDMA_BITS_LIST_SET;
            SecondLevelIndex = (Size ^ (1 << FirstLevelIndex)) >> BitShift;
        }
    }
    Assert(SecondLevelIndex < hs_FreeListClasses_Amount, ec_InvalidImplementation);
    return SecondLevelIndex;
}
heap_sdma_free_block_t* _heap_sdma_alloc_bottom(Base_t Size) {
    heap_sdma_free_block_t* NewBlock = (heap_sdma_free_block_t*) _heap_sdma_reserve_bottom(Size + sizeof(heap_sdma_used_block_t));

    if (NewBlock) { // initialize block header
        memset(NewBlock, 0, sizeof(heap_sdma_free_block_t));
        NewBlock->Prev_Physical_BlkPtr = (heap_sdma_free_block_t*) hs_Last_Bottom_Alloc_Block;
        if (hs_Last_Bottom_Alloc_Block)
            hs_Last_Bottom_Alloc_Block->Next_Physical_BlkPtr = (heap_sdma_used_block_t*) NewBlock;
        hs_Last_Bottom_Alloc_Block = (heap_sdma_used_block_t*) NewBlock;
#ifdef TTC_REGRESSION
        ttc_memory_Bytes_Allocated_Usable += Size;
#endif
    }

    return NewBlock;
}
heap_sdma_free_block_t* _heap_sdma_alloc_top(Base_t Size) {
    heap_sdma_free_block_t* NewBlock = (heap_sdma_free_block_t*) _heap_sdma_reserve_top(Size + sizeof(heap_sdma_used_block_t));

    if (NewBlock) { // initialize block header
        memset(NewBlock, 0, sizeof(heap_sdma_free_block_t));
        NewBlock->Next_Physical_BlkPtr = (heap_sdma_free_block_t*) hs_Last_Top_Alloc_Block;
        if (hs_Last_Top_Alloc_Block)
            hs_Last_Top_Alloc_Block->Prev_Physical_BlkPtr = (heap_sdma_used_block_t*) NewBlock;
        hs_Last_Top_Alloc_Block = (heap_sdma_used_block_t*) NewBlock;
#ifdef TTC_REGRESSION
        ttc_memory_Bytes_Allocated_Usable += Size;
#endif
    }

    return NewBlock;
}
void* _heap_sdma_reserve_bottom(Base_t Size) {
    while (Size & 3) // make size a factor of 4
        Size++;

    if (heap_sdma_FreeSize < Size)
        return NULL;

    void* Address = hs_Heap_FirstFree;
    hs_Heap_FirstFree += Size;
    heap_sdma_FreeSize -= Size;
#ifdef TTC_REGRESSION
    ttc_memory_Bytes_Allocated_Total += Size;
    ttc_memory_Amount_Blocks_Allocated++;
#endif


    return Address;
}
void* _heap_sdma_reserve_top(Base_t Size) {
    while (Size & 3) // make size a factor of 4
        Size++;

    if (heap_sdma_FreeSize < Size)
        return NULL;

    hs_Heap_FirstNonFree -= Size;
    void* Address = hs_Heap_FirstNonFree;
    heap_sdma_FreeSize -= Size;
#ifdef TTC_REGRESSION
    ttc_memory_Bytes_Allocated_Total += Size;
    ttc_memory_Amount_Blocks_Allocated++;
#endif

    return Address;
}

//} Function definitions
