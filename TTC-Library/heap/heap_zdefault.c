/** { heap_zdefault.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for heap devices on zdefault architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 20 at 20140301 07:31:49 UTC
 *
 *  Note: See ttc_heap.h for description of zdefault independent HEAP implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "heap_zdefault.h"
#include "compile_options.h"
#include "../ttc_task.h"

//{ Global Variables ***********************************************************

#ifdef TTC_MULTITASKING_SCHEDULER
    #  error heap_simple cannot work with activated scheduler. Activate a different heap implementation!
#endif

#if TTC_MEMORY_STATISTICS == 1

    // amount of blocks allocated on heap
    extern t_base ttc_heap_Amount_Blocks_Allocated;

    // amount of bytes occupied on heap by dynamic allocations
    extern t_base ttc_heap_Bytes_Allocated_Total;

    // usable amount of allocated bytes
    extern t_base ttc_heap_Bytes_Allocated_Usable;

#endif

volatile t_u8* hs_HeapStart     = NULL;
volatile t_u8* hs_NextFree      = NULL;
t_base_signed hs_BytesRemaining = 0;

//}Global Variables
//{ Function definitions *******************************************************

void* heap_zdefault_alloc( t_base Size ) {

    ttc_task_critical_begin();
    hs_BytesRemaining -= Size;
    if ( Size >= 4 ) { // align start address to 32 bit
        while ( ( ( t_base ) hs_NextFree ) & 3 ) {
            hs_NextFree++;
            hs_BytesRemaining--;
        }
    }
    else if ( Size > 1 ) { // align start address to 16 bit
        while ( ( ( t_base ) hs_NextFree ) & 1 ) {
            hs_NextFree++;
            hs_BytesRemaining--;
        }
    }
    Assert( hs_BytesRemaining > 3, ttc_assert_origin_auto );

    void* Address = ( void* ) hs_NextFree;
    hs_NextFree += Size;

#if TTC_MEMORY_STATISTICS == 1
    ttc_heap_Amount_Blocks_Allocated += Size;
    ttc_heap_Bytes_Allocated_Usable  += Size;
    ttc_heap_Bytes_Allocated_Total   += Size;
#endif

    ttc_task_critical_end();
    return Address;
}
void* heap_zdefault_temporary_alloc( t_base Size ) {
    if ( hs_BytesRemaining - 4 < Size )
    { return NULL; }  // not egnough heap memory left

    // return next 4 byte aligned address
    return ( void* )( ( ( ( t_base ) hs_NextFree ) + Size + 3 ) & ( ( ( t_base ) - 1 ) - 0b11 ) );
}
void* heap_zdefault_free( void* Block ) {
    Assert_HEAP_Writable( Block, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( void* ) 0;
}
t_base heap_zdefault_get_free_size() {

    return hs_BytesRemaining;
}
void heap_zdefault_prepare( volatile t_u8* HeapStart, t_base HeapSize ) {
    Assert_HEAP_Writable( ( void* ) HeapStart, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_HEAP( HeapSize, ttc_assert_origin_auto );

    hs_HeapStart = hs_NextFree = HeapStart;
    hs_BytesRemaining = HeapSize;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
