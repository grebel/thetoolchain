/*{ heap_freertos.c ************************************************
 
 Thin compatibility layer providing memory allocator of FreeRTOS to ttc_memory

}*/

#include "heap_freertos.h"

#if TTC_MEMORY_STATISTICS == 1

// amount of blocks allocated on heap
extern Base_t ttc_memory_Amount_Blocks_Allocated;

// amount of bytes occupied on heap by dynamic allocations
extern Base_t ttc_memory_Bytes_Allocated_Total;

// usable amount of allocated bytes
extern Base_t ttc_memory_Bytes_Allocated_Usable;

#endif

//{ Function definitions *************************************************


void heap_freertos_init(u8_t* HeapStart, Base_t HeapSize) {
  (void) HeapStart;
  (void) HeapSize;

  // initialization is done by FreeRTOS
}
void* heap_freertos_alloc(Base_t Size) {
    void* Address = pvPortMalloc(Size);
    return Address;
}
void heap_freertos_free(void* Block) {

    vPortFree(Block);
}

//} Function definitions
