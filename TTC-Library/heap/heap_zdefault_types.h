#ifndef HEAP_ZDEFAULT_TYPES_H
#define HEAP_ZDEFAULT_TYPES_H

/** { heap_zdefault.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for HEAP devices on zdefault architectures.
 *  Structures, Enums and Defines being required by ttc_heap_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140301 07:31:49 UTC
 *
 *  Note: See ttc_heap.h for description of architecture independent HEAP implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

//? #include "../ttc_basic.h" DO NOT INCLUDE HERE! (this file is included from ttc_basic.h)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_heap_types.h *************************

typedef struct { // register description (adapt according to zdefault registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_heap_register;

typedef struct {  // zdefault specific configuration data of single HEAP device
    t_heap_register* BaseRegister;       // base address of HEAP device registers
} __attribute__( ( __packed__ ) ) t_heap_zdefault_config;

// t_ttc_heap_architecture is required by ttc_heap_types.h
#define t_ttc_heap_architecture t_heap_zdefault_config

//} Structures/ Enums


#endif //HEAP_ZDEFAULT_TYPES_H
