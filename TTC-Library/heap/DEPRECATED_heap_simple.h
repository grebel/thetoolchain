#ifndef heap_simple_H
#define heap_simple_H

/** heap_simple.h ************************************************
 *
 * This memory allocator provides a lightweight implementation for
 * single task environments. It is not usable with a multitasking scheduler.
 *
 * Block Alignment
 *   Allocated blocks of sizes 2,3 get 16 bit aligned.
 *   Larger blocks get 32 bit aligned.
 *   Single bytes do not get aligned.
 *
}*/
//{ Defines/ TypeDefs ****************************************************

#ifndef EXTENSION_500_ttc_memory_heap_simple
#error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_memory_heap_simple.sh
#endif
//} Defines
//{ Includes *************************************************************

#include "../ttc_basic.h"
#include "../ttc_memory_types.h"

//} Includes
//{ Structures/ Enums ****************************************************

//} Structures/ Enums
//{ driver interface for ttc_memory.h ************************************

#define _driver_memory_alloc(Size)               heap_simple_alloc(Size)
#define _driver_memory_free(Block)               heap_simple_free(Block)
#define _driver_memory_get_free_size()           heap_simple_get_free()
#define _driver_memory_init(HeapStart, HeapSize) heap_simple_init(HeapStart, HeapSize);

//} Global Variables
//{ Function prototypes **************************************************

/** Initializes memory allocator - Call before using any other function from this source
  *
  * Gets called automatically by extension initializer code
  */
void heap_simple_init(u8_t* HeapStart, Base_t HeapSize);

/** Allocates memory block of given size.
  *
  */
void* heap_simple_alloc(Base_t Size);

/** Returns amount of bytes available for allocation.
  *
  * @return  amount of bytes free
  */
Base_t heap_simple_get_free();

/** Not implemented.
 *
 */
void heap_simple_free(void* Block);


//} Function prototypes

#endif //heap_simple_H
