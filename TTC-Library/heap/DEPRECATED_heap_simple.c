/** heap_simple.c ************************************************{
 *
 * Basic memory allocator implementation.
 * Provides
 * - standalone memory allocation
 * - no deallocation
 * - very fast
 * - minimal overhead
 *
 * Written by Gregor Rebel 2014
 *
}*/

#include "heap_simple.h"

#ifdef RT_SCHEDULER
#  error heap_simple cannot work with activated scheduler. Activate a different heap implementation!
#endif

#if TTC_MEMORY_STATISTICS == 1

// amount of blocks allocated on heap
extern Base_t ttc_memory_Amount_Blocks_Allocated;

// amount of bytes occupied on heap by dynamic allocations
extern Base_t ttc_memory_Bytes_Allocated_Total;

// usable amount of allocated bytes
extern Base_t ttc_memory_Bytes_Allocated_Usable;

#endif

u8_t* hs_HeapStart       = NULL;
u8_t* hs_NextFree        = NULL;
Base_signed_t hs_BytesRemaining = 0;

//{ Function definitions *************************************************

void heap_simple_init(u8_t* HeapStart, Base_t HeapSize) {

    hs_HeapStart = hs_NextFree = HeapStart;
    hs_BytesRemaining = HeapSize;
}
void* heap_simple_alloc(Base_t Size) {

    hs_BytesRemaining -= Size;
    if (Size >= 4) { // align start address to 32 bit
        while ( ((Base_t) hs_NextFree) & 3) {
            hs_NextFree++;
            hs_BytesRemaining--;
        }
    }
    else if (Size > 1) { // align start address to 16 bit
        while ( ((Base_t) hs_NextFree) & 1) {
            hs_NextFree++;
            hs_BytesRemaining--;
        }
    }
    Assert(hs_BytesRemaining > 3, ec_OutOfMemory);

    void* Address = hs_NextFree;
    hs_NextFree += Size;

    return Address;
}
Base_t heap_simple_get_free() {
    return hs_BytesRemaining;
}
void heap_simple_free(void* Block) {
    (void) Block;
}

//} Function definitions
