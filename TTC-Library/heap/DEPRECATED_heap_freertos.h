#ifndef heap_freertos_H
#define heap_freertos_H

/*{ heap_freertos.h ************************************************
 
 Thin compatibility layer providing memory allocator of FreeRTOS to ttc_memory
 
}*/
//{ Defines/ TypeDefs ****************************************************

#ifndef EXTENSION_300_scheduler_freertos
#error Missing extension! Please add this to your activate_project.sh script: activate.300_scheduler_freertos.sh
#endif
//} Defines
//{ Includes *************************************************************

#include "../ttc_basic.h"
#include "../ttc_memory_types.h"
#include "FreeRTOS.h"
#include "task.h"
#include "portable.h"

//} Includes
//{ Structures/ Enums ****************************************************

//} Structures/ Enums
//{ driver interface for ttc_memory.h ************************************

#if TTC_MEMORY_STATISTICS == 1
#define _driver_memory_alloc(Size)      heap_freertos_alloc(Size)
#define _driver_memory_free(Block)      heap_freertos_free(Block)
#else
#define _driver_memory_alloc(Size)      pvPortMalloc(Size)
#define _driver_memory_free(Block)      vPortFree(Block) // provided by heap_1.c, heap_2.c, heap_3.c, ...
#endif
#define _driver_memory_get_free_size()  xPortGetFreeHeapSize()
#define _driver_memory_init(HeapStart, HeapSize) heap_freertos_init(HeapStart, HeapSize);

//} Global Variables
//{ Function prototypes **************************************************

/** Initializes memory allocator - Call before using any other function from this source
  *
  * Gets called automatically by extension initializer code
  */
void heap_freertos_init(u8_t* HeapStart, Base_t HeapSize);
void* heap_freertos_alloc(Base_t Size);
void heap_freertos_free(void* Block);

//} Function prototypes

#endif //heap_freertos_H
