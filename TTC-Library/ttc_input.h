#ifndef TTC_INPUT_H
#define TTC_INPUT_H
/** { ttc_input.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for INPUT devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_INPUT(tc_input_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_input_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_input_init(LogicalIndex);
 *  4) use:         ttc_input_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level input and application.
 *
 *  Created from template ttc_device.h revision 31 at 20150317 17:32:16 UTC
 *
 *  Authors: Gregor Rebel
 *
 *
 *  How to configure input devices
 *
 *  TTC_INPUT is a generic concept of input sensitive areas on a graphic display.
 *  Touching an area with a mouse pointer or a finger on a touchpad causes an action.
 *  TTC_INPUT requires an extra driver that can deliver mouse movements like ttc_touchpad.
 *  The maximum amount of sensitive areas must be defined at initialization. See definition
 *  of t_ttc_input_config for details.
 *
}*/

#ifndef EXTENSION_ttc_input
#error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_input.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_input.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_input_interface.h" // multi architecture support
#include "ttc_gfx_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************
//FK
//#if TTC_TOUCH_ROTATION_CLOCKWISE == 90
//#    define ROTATED_INPUT_X(X, Y) (Y)
//#endif
//#if TTC_TOUCH_ROTATION_CLOCKWISE == 180
//#    define ROTATED_INPUT_X(X, Y) (TTC_GFX1_WIDTH - 1 - X)
//#endif
//#if TTC_TOUCH_ROTATION_CLOCKWISE == 270
//#    define ROTATED_INPUT_X(X, Y) (TTC_GFX1_WIDTH - 1 - Y)
//#endif
//#ifndef ROTATED_X
//#    define ROTATED_INPUT_X(X, Y) (X)
//#endif

//#if TTC_TOUCH_ROTATION_CLOCKWISE == 90
//#    define ROTATED_INPUT_Y(X, Y) (TTC_GFX1_HEIGHT - 1 - X)
//#endif
//#if TTC_TOUCH_ROTATION_CLOCKWISE == 180
//#    define ROTATED_INPUT_Y(X, Y) (TTC_GFX1_HEIGHT - 1 - Y)
//#endif
//#if TTC_TOUCH_ROTATION_CLOCKWISE == 270
//#    define ROTATED_INPUT_Y(X, Y) (X)
//#endif
//#ifndef ROTATED_Y
//#    define ROTATED_INPUT_Y(X, Y) (Y)
//#endif
//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level input only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * input devices on all supported architectures.
 * Check input/input_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares input Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_input_prepare();
void _driver_input_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_input_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_INPUT_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_input_config* ttc_input_get_configuration( t_u8 LogicalIndex );

/** fills out given Config with maximum valid values for indexed INPUT
 * @param LogicalIndex    index of device to init (1..ttc_INPUT_get_max_LogicalIndex())
 * @return                pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_input_config* ttc_input_get_features( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    logical index of input device (1..ttc_INPUT_get_max_LogicalIndex())
 * @return                == 0: input device has been initialized successfully; != 0: error-code
 */
e_ttc_input_errorcode ttc_input_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_INPUT_get_max_LogicalIndex())
 */
void ttc_input_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_input_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of input device (1..ttc_input_get_max_index() )
 * @return                == 0: default values have been loaded successfully for indexed input device; != 0: error-code
 */
e_ttc_input_errorcode  ttc_input_load_defaults( t_u8 LogicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    logical index of input device (1..ttc_INPUT_get_max_LogicalIndex())
 */
void ttc_input_reset( t_u8 LogicalIndex );

/** deletes all input areas (call when leaving a menu dialog)
 *
 * @param LogicalIndex    logical index of input device (1..ttc_INPUT_get_max_LogicalIndex())
 */
void ttc_input_clear( t_u8 LogicalIndex );

/** checks current input device for events and handles all affected input areas
 *
 * @param LogicalIndex    logical index of input device (1..ttc_INPUT_get_max_LogicalIndex())
 * @return == tie_None: no event; != tie_None: input event
 */
e_ttc_input_event ttc_input_check( t_u8 LogicalIndex );

/** configure a new input area
 *
 * ttc_input itself does not know anything about buttons.
 * Input areas are invisible rectangles that can be registered for a handler function.
 * When an input event occurs inside the rectangle (e.g. user clicks inside) then the handler function is called.
 * The handler function then can investigate the input event by its arguments:
 * - ConfigInput   configuration of input device that caused the event (e.g. ConfigInput->Event stores type of event)
 * - AffectedArea  registered input area (e.g. AffectedArea->Argument stores the value given as Argument to ttc_input_area_new() )
 * - RelativeX     x-coordinate  of event relative to top-left corner of input area
 * - RelativeY     y-coordinate  of event relative to top-left corner of input area

 * @param LogicalIndex    logical index of input device (1..ttc_INPUT_get_max_LogicalIndex())
 * @param Left            x-coordinate of left  border of input area to create
 * @param Top             y-coordinate of upper border of input area to create
 * @param Right           x-coordinate of left  border of input area to create
 * @param Bottom          y-coordinate of lower border of input area to create
 * @param Handler         pointer to function to call for each input event inside this area
 * @param Argument        stored in input area (Handler() may access it as AffectedArea->Argument)
 */
t_ttc_input_area* ttc_input_area_new( t_u8 LogicalIndex, t_s16 Left, t_s16 Top, t_s16 Right, t_s16 Bottom, void ( *Handler )( struct s_ttc_input_config* ConfigInput, struct s_ttc_input_area* AffectedArea, t_u16 RelativeX, t_u16 RelativeY ), void* Argument );

/** delete given input area from input
 *
 * @param LogicalIndex    device index of initialized touchpad device
 * @param InputArea       input area to delete
 * @return                ==TRUE: input area has been removedM ==FALSE: input area was not found in actice or inactive lists
 */
BOOL ttc_input_area_del( t_u8 LogicalIndex, t_ttc_input_area* InputArea );

/** move given input area from list of active to inactive input areas
 *
 * Note: Inactive input areas do not consume CPU time during input filtering.
 *
 * @param LogicalIndex    device index of initialized touchpad device
 * @param InputArea       input area to disable
 * @return                ==TRUE: input area has been removedM ==FALSE: input area was not found in actice or inactive lists
 */
BOOL ttc_input_area_disable( t_u8 LogicalIndex, t_ttc_input_area* InputArea );

/** move given input area from list of inactive to active input areas
 *
 * @param LogicalIndex    device index of initialized touchpad device
 * @param InputArea       input area to enable
 * @return                ==TRUE: input area has been removedM ==FALSE: input area was not found in actice or inactive lists
 */
BOOL ttc_input_area_enable( t_u8 LogicalIndex, t_ttc_input_area* InputArea );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_input_ are passed to interfaces/ttc_input_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl input UPDATE
 */


/** fills out given Config with maximum valid values for indexed INPUT
 * @param Config  = pointer to struct t_ttc_input_config
 * @return        pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_input_config* _driver_input_get_features( t_ttc_input_config* Config );

/** checks connected device for input events
 *
 * @param Config           pointer to struct t_ttc_input_config
 * @return Config->EventX  x-coordinate of input event
 * @return Config->EventY  y-coordinate of input event
 * @return == tie_None: no event; != tie_None: input event
 */
e_ttc_input_event _driver_input_check( t_ttc_input_config* Config );

/** shutdown single INPUT unit device
 *
 * @param Config        pointer to struct t_ttc_input_config
 * @return              == 0: INPUT has been shutdown successfully; != 0: error-code
 */
e_ttc_input_errorcode _driver_input_deinit( t_ttc_input_config* Config );

/** initializes single INPUT unit for operation
 * @param Config        pointer to struct t_ttc_input_config
 * @return              == 0: INPUT has been initialized successfully; != 0: error-code
 */
e_ttc_input_errorcode _driver_input_init( t_ttc_input_config* Config );

/** loads configuration of indexed INPUT unit with default values
 * @param Config        pointer to struct t_ttc_input_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_input_errorcode _driver_input_load_defaults( t_ttc_input_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_input_config
 */
void _driver_input_reset( t_ttc_input_config* Config );
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_INPUT_H
