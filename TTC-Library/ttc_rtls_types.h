/** { ttc_rtls_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for RTLS device.
 *  Structures, Enums and Defines being required by both, high- and low-level rtls.
 *  This file does not provide function declarations!
 *
 *  Created from template ttc_device_types.h revision 38 at 20161208 10:11:47 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_RTLS_TYPES_H
#define TTC_RTLS_TYPES_H

typedef enum {  // e_ttc_rtls_state - current statemachine state (combination of statemachines of all low-level drivers)
    tss_None,         // device not initialized (configure Config->Init.Flags and call ttc_rtls_init()!)
    tss_Init,         // statemachine is called from ttc_rtls_init() to initialize itself (after this, State must be tss_Anchor_First..tss_Anchor_Last or tss_Anchor_Mobile..tss_Mobile_Last

    // rtls - General
    tss_Report_Distances_Start,                // start sending a number of distances to one or more remote nodes
    tss_Report_Distances_Transmit,             // transmit single packet to single target node (or to broadcast address)
    tss_Report_Distances_Complete,             // transmission of all distances to all targets complete
    tss_Report_Distances_Idle,                 // statemachine has nothing to do

    tss_Report_Locations_Start,                // start sending a number of Locations to one or more remote nodes
    tss_Report_Locations_Transmit,             // transmit single packet to single target node (or to broadcast address)
    tss_Report_Locations_Complete,             // transmission of all Locations to all targets complete
    tss_Report_Locations_Idle,                 // statemachine has nothing to do

    // rtls_crtof_simple_2d - Anchor Node

    tss_Anchor_First, // ------------------------------ all valid anchor node states below! -----------------------

    // states for rtls_crtof_simple_2d southpole anchor node statemachine
    state_s2d_southpole_Init,                        // statemachine initialization (run as first state)
    state_s2d_southpole_MeasureAllRanges,            // measure range to all non-southpole nodes (NodeIndex > 1)
    state_s2d_southpole_MeasureSingleRange,          // measure range to a single node
    state_s2d_southpole_RequestAllRanges2Northpole,  // request range to northpole from all west- and east-nodes
    state_s2d_southpole_RequestSingleRange2Northpole,// request range to northpole from a single west- or east-node
    state_s2d_southpole_BuildCoordinateSystem,       // calculate coordinates of all anchor nodes relative to southpole
    state_s2d_southpole_ReportRanges,                // send all measured ranges as broadcast to all other nodes
    state_s2d_southpole_Ready,                       // ready to serve mobile nodes
    state_s2d_southpole_ReplyRanging,                // reply to a received ranging request (sends just ranging data)
    state_s2d_southpole_ReplyLocalization,           // reply to a received localization request (sends ranging data + own coordinates)


    // states for rtls_crtof_simple_2d non-soutpole anchor node statemachine
    state_s2d_anchor_Init,                            // statemachine initialization (run as first state)
    state_s2d_anchor_Waiting,                         // waiting for southpole to finish initialization
    state_s2d_anchor_RequestAllRanges,                // requesting data of all ranges from southpole
    state_s2d_anchor_MeasureRange2Northpole,          // measure range to northpole anchor node
    state_s2d_anchor_BuildCoordinateSystem,           // calculate coordinates of all anchor nodes relative to southpole
    state_s2d_anchor_MeasureSingleRange,              // measure range to another node
    state_s2d_anchor_Ready,                           // ready to serve mobile nodes
    state_s2d_anchor_ReplyRanging,                    // reply to a received ranging request (sends just ranging data)
    state_s2d_anchor_ReplyLocalization,               // reply to a received localization request (sends ranging data + own coordinates)

    tss_Anchor_Last, // -------------------------------- all valid anchor node states above! -----------------------

    tss_Mobile_First, // ------------------------------ all valid mobile node states below! -----------------------
    // states for rtls_crtof_simple_2d - mobile nodes
    tss_crtof_simple2d_mobile_state_number_Init,       // // statemachine initialization (run as first state)
    // ToDo: Add mobile node statemachine states!

    tss_Mobile_Last, // -------------------------------- all valid mobile node states above! -----------------------

    tss_unknown
} e_ttc_rtls_state; // -> tss_ prefix

//{ Includes *************************************************************
//
// _types.h Header files mayshould include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_rtls.h" or "ttc_rtls.c"
//
#include "compile_options.h"
#include "rtls/rtls_common_types.h"
#include "ttc_basic_types.h"
#include "ttc_math_types.h"
#include "ttc_packet_types.h"
#include "ttc_radio_types.h"
#include "ttc_slam_types.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)


//} Includes
//{ Defines **************************************************************
#ifndef TTC_RTLS1
    #  warning Missing definition for TTC_RTLS1. Using a default value.
    #define TTC_RTLS1 ta_rtls_crtof_simple_2d
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

/** {  TTC_RTLSn has to be defined as constant by makefile.100_board_*
 *
 * Here we count how much devices have been defined and if all lower index devices have been defined too.
 */
#ifndef TTC_RTLS_AMOUNT //{ 10
    #ifdef TTC_RTLS10
        #ifndef TTC_RTLS9
            #      error TTC_RTLS10 is defined, but not TTC_RTLS9 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS8
            #      error TTC_RTLS10 is defined, but not TTC_RTLS8 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS7
            #      error TTC_RTLS10 is defined, but not TTC_RTLS7 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS6
            #      error TTC_RTLS10 is defined, but not TTC_RTLS6 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS5
            #      error TTC_RTLS10 is defined, but not TTC_RTLS5 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS4
            #      error TTC_RTLS10 is defined, but not TTC_RTLS4 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS3
            #      error TTC_RTLS10 is defined, but not TTC_RTLS3 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS2
            #      error TTC_RTLS10 is defined, but not TTC_RTLS2 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS1
            #      error TTC_RTLS10 is defined, but not TTC_RTLS1 - all lower TTC_RTLSn must be defined!
        #endif

        #define TTC_RTLS_AMOUNT 10
    #endif
#endif //}
#ifndef TTC_RTLS_AMOUNT //{ 9
    #ifdef TTC_RTLS9
        #ifndef TTC_RTLS8
            #      error TTC_RTLS9 is defined, but not TTC_RTLS8 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS7
            #      error TTC_RTLS9 is defined, but not TTC_RTLS7 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS6
            #      error TTC_RTLS9 is defined, but not TTC_RTLS6 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS5
            #      error TTC_RTLS9 is defined, but not TTC_RTLS5 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS4
            #      error TTC_RTLS9 is defined, but not TTC_RTLS4 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS3
            #      error TTC_RTLS9 is defined, but not TTC_RTLS3 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS2
            #      error TTC_RTLS9 is defined, but not TTC_RTLS2 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS1
            #      error TTC_RTLS9 is defined, but not TTC_RTLS1 - all lower TTC_RTLSn must be defined!
        #endif

        #define TTC_RTLS_AMOUNT 9
    #endif
#endif //}
#ifndef TTC_RTLS_AMOUNT //{ 8
    #ifdef TTC_RTLS8
        #ifndef TTC_RTLS7
            #      error TTC_RTLS8 is defined, but not TTC_RTLS7 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS6
            #      error TTC_RTLS8 is defined, but not TTC_RTLS6 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS5
            #      error TTC_RTLS8 is defined, but not TTC_RTLS5 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS4
            #      error TTC_RTLS8 is defined, but not TTC_RTLS4 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS3
            #      error TTC_RTLS8 is defined, but not TTC_RTLS3 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS2
            #      error TTC_RTLS8 is defined, but not TTC_RTLS2 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS1
            #      error TTC_RTLS8 is defined, but not TTC_RTLS1 - all lower TTC_RTLSn must be defined!
        #endif

        #define TTC_RTLS_AMOUNT 8
    #endif
#endif //}
#ifndef TTC_RTLS_AMOUNT //{ 7
    #ifdef TTC_RTLS7
        #ifndef TTC_RTLS6
            #      error TTC_RTLS7 is defined, but not TTC_RTLS6 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS5
            #      error TTC_RTLS7 is defined, but not TTC_RTLS5 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS4
            #      error TTC_RTLS7 is defined, but not TTC_RTLS4 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS3
            #      error TTC_RTLS7 is defined, but not TTC_RTLS3 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS2
            #      error TTC_RTLS7 is defined, but not TTC_RTLS2 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS1
            #      error TTC_RTLS7 is defined, but not TTC_RTLS1 - all lower TTC_RTLSn must be defined!
        #endif

        #define TTC_RTLS_AMOUNT 7
    #endif
#endif //}
#ifndef TTC_RTLS_AMOUNT //{ 6
    #ifdef TTC_RTLS6
        #ifndef TTC_RTLS5
            #      error TTC_RTLS6 is defined, but not TTC_RTLS5 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS4
            #      error TTC_RTLS6 is defined, but not TTC_RTLS4 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS3
            #      error TTC_RTLS6 is defined, but not TTC_RTLS3 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS2
            #      error TTC_RTLS6 is defined, but not TTC_RTLS2 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS1
            #      error TTC_RTLS6 is defined, but not TTC_RTLS1 - all lower TTC_RTLSn must be defined!
        #endif

        #define TTC_RTLS_AMOUNT 6
    #endif
#endif //}
#ifndef TTC_RTLS_AMOUNT //{ 5
    #ifdef TTC_RTLS5
        #ifndef TTC_RTLS4
            #      error TTC_RTLS5 is defined, but not TTC_RTLS4 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS3
            #      error TTC_RTLS5 is defined, but not TTC_RTLS3 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS2
            #      error TTC_RTLS5 is defined, but not TTC_RTLS2 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS1
            #      error TTC_RTLS5 is defined, but not TTC_RTLS1 - all lower TTC_RTLSn must be defined!
        #endif

        #define TTC_RTLS_AMOUNT 5
    #endif
#endif //}
#ifndef TTC_RTLS_AMOUNT //{ 4
    #ifdef TTC_RTLS4
        #ifndef TTC_RTLS3
            #      error TTC_RTLS4 is defined, but not TTC_RTLS3 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS2
            #      error TTC_RTLS4 is defined, but not TTC_RTLS2 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS1
            #      error TTC_RTLS4 is defined, but not TTC_RTLS1 - all lower TTC_RTLSn must be defined!
        #endif

        #define TTC_RTLS_AMOUNT 4
    #endif
#endif //}
#ifndef TTC_RTLS_AMOUNT //{ 3
    #ifdef TTC_RTLS3

        #ifndef TTC_RTLS2
            #      error TTC_RTLS3 is defined, but not TTC_RTLS2 - all lower TTC_RTLSn must be defined!
        #endif
        #ifndef TTC_RTLS1
            #      error TTC_RTLS3 is defined, but not TTC_RTLS1 - all lower TTC_RTLSn must be defined!
        #endif

        #define TTC_RTLS_AMOUNT 3
    #endif
#endif //}
#ifndef TTC_RTLS_AMOUNT //{ 2
    #ifdef TTC_RTLS2

        #ifndef TTC_RTLS1
            #      error TTC_RTLS2 is defined, but not TTC_RTLS1 - all lower TTC_RTLSn must be defined!
        #endif

        #define TTC_RTLS_AMOUNT 2
    #endif
#endif //}
#ifndef TTC_RTLS_AMOUNT //{ 1
    #ifdef TTC_RTLS1
        #define TTC_RTLS_AMOUNT 1
    #endif
#endif //}
#ifndef TTC_RTLS_AMOUNT //{ no devices defined at all
    #  warning Missing definition for TTC_RTLS1. Using default type. Add valid definition to your makefile to get rid if this message!
    // option 1: no devices available
    #define TTC_RTLS_AMOUNT 0

    // option 2: define a default device (enable lines below)
    // #define TTC_RTLS_AMOUNT 1
    // define TTC_RTLS1 ??? // <- place entry from e_ttc_rtls_architecture here!!!
#endif

//}

/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_RTLS 0#         disable default asserts for rtls driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_RTLS_EXTRA 1#   enable extra asserts for rtls driver
 *
 */
#ifndef TTC_ASSERT_RTLS    // any previous definition set (Makefile)?
    #define TTC_ASSERT_RTLS 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_RTLS == 1)  // use Assert()s in RTLS code (somewhat slower but alot easier to debug)
    #define Assert_RTLS(Condition, Origin)        if (! (Condition) ) { ttc_assert_halt_origin(Origin); }
    #define Assert_RTLS_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_RTLS_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in RTLS code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_RTLS(Condition, Origin)
    #define Assert_RTLS_Writable(Address, Origin)
    #define Assert_RTLS_Readable(Address, Origin)
#endif

#ifndef TTC_ASSERT_RTLS_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_RTLS_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_RTLS_EXTRA == 1)  // use Assert()s in RTLS code (somewhat slower but alot easier to debug)
    #define Assert_RTLS_EXTRA(Condition, Origin) if (! (Condition) ) { ttc_assert_halt_origin(Origin); }
    #define Assert_RTLS_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_RTLS_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in RTLS code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_RTLS_EXTRA(Condition, Origin)
    #define Assert_RTLS_EXTRA_Writable(Address, Origin)
    #define Assert_RTLS_EXTRA_Readable(Address, Origin)
#endif
//}

/** { Check static configuration of rtls devices
 *
 * Each TTC_RTLSn must be defined as one from e_ttc_rtls_architecture.
 * Additional defines of type TTC_RTLSn_* may be added.
 *
 * The constants being checked here must be defined in your makefile.
 * Most constants are defined by EXTENSION_100_board_* or EXTENSION_150_board_extension_* files.
 */

#ifdef TTC_RTLS1

    // check if this node should provide anchor functionality (anchor nodes built up coordinate system and provide localization service for mobile nodes)
    #ifndef TTC_RTLS1_IMPLEMENT_ANCHOR
        #    warning Missing definition for TTC_RTLS1_IMPLEMENT_ANCHOR using default value
        #define TTC_RTLS1_IMPLEMENT_ANCHOR 1
    #endif
    #if TTC_RTLS1_IMPLEMENT_ANCHOR == 1
        //   enable anchor node source code and data sets
        #define TTC_RTLS_IMPLEMENT_ANCHOR 1
    #endif

    // check if this node should provide mobile functionality (mobile nodes can use localization service of anchor nodes)
    #ifndef TTC_RTLS1_IMPLEMENT_MOBILE
        #    warning Missing definition for TTC_RTLS1_IMPLEMENT_MOBILE using default value
        #define TTC_RTLS1_IMPLEMENT_MOBILE 1
    #endif
    #if TTC_RTLS1_IMPLEMENT_MOBILE == 1
        //   enable mobile node source code and data sets
        #define TTC_RTLS_IMPLEMENT_MOBILE 1
    #endif

#endif
#ifdef TTC_RTLS2

    // check if this node should provide anchor functionality (anchor nodes built up coordinate system and provide localization service for mobile nodes)
    #ifndef TTC_RTLS2_IMPLEMENT_ANCHOR
        #    warning Missing definition for TTC_RTLS2_IMPLEMENT_ANCHOR using default value
        #define TTC_RTLS2_IMPLEMENT_ANCHOR 1
    #endif
    #if TTC_RTLS2_IMPLEMENT_ANCHOR == 1
        //   enable anchor node source code and data sets
        #define TTC_RTLS_IMPLEMENT_ANCHOR 1
    #endif

    // check if this node should provide mobile functionality (mobile nodes can use localization service of anchor nodes)
    #ifndef TTC_RTLS2_IMPLEMENT_MOBILE
        #    warning Missing definition for TTC_RTLS2_IMPLEMENT_MOBILE using default value
        #define TTC_RTLS2_IMPLEMENT_MOBILE 1
    #endif
    #if TTC_RTLS2_IMPLEMENT_MOBILE == 1
        //   enable mobile node source code and data sets
        #define TTC_RTLS_IMPLEMENT_MOBILE 1
    #endif

#endif
#ifdef TTC_RTLS3

    // check if this node should provide anchor functionality (anchor nodes built up coordinate system and provide localization service for mobile nodes)
    #ifndef TTC_RTLS3_IMPLEMENT_ANCHOR
        #    warning Missing definition for TTC_RTLS3_IMPLEMENT_ANCHOR using default value
        #define TTC_RTLS3_IMPLEMENT_ANCHOR 1
    #endif
    #if TTC_RTLS3_IMPLEMENT_ANCHOR == 1
        //   enable anchor node source code and data sets
        #define TTC_RTLS_IMPLEMENT_ANCHOR 1
    #endif

    // check if this node should provide mobile functionality (mobile nodes can use localization service of anchor nodes)
    #ifndef TTC_RTLS3_IMPLEMENT_MOBILE
        #    warning Missing definition for TTC_RTLS3_IMPLEMENT_MOBILE using default value
        #define TTC_RTLS3_IMPLEMENT_MOBILE 1
    #endif
    #if TTC_RTLS3_IMPLEMENT_MOBILE == 1
        //   enable mobile node source code and data sets
        #define TTC_RTLS_IMPLEMENT_MOBILE 1
    #endif

#endif
#ifdef TTC_RTLS4

    // check if this node should provide anchor functionality (anchor nodes built up coordinate system and provide localization service for mobile nodes)
    #ifndef TTC_RTLS4_IMPLEMENT_ANCHOR
        #    warning Missing definition for TTC_RTLS4_IMPLEMENT_ANCHOR using default value
        #define TTC_RTLS4_IMPLEMENT_ANCHOR 1
    #endif
    #if TTC_RTLS4_IMPLEMENT_ANCHOR == 1
        //   enable anchor node source code and data sets
        #define TTC_RTLS_IMPLEMENT_ANCHOR 1
    #endif

    // check if this node should provide mobile functionality (mobile nodes can use localization service of anchor nodes)
    #ifndef TTC_RTLS4_IMPLEMENT_MOBILE
        #    warning Missing definition for TTC_RTLS4_IMPLEMENT_MOBILE using default value
        #define TTC_RTLS4_IMPLEMENT_MOBILE 1
    #endif
    #if TTC_RTLS4_IMPLEMENT_MOBILE == 1
        //   enable mobile node source code and data sets
        #define TTC_RTLS_IMPLEMENT_MOBILE 1
    #endif

#endif
#ifdef TTC_RTLS5

    // check if this node should provide anchor functionality (anchor nodes built up coordinate system and provide localization service for mobile nodes)
    #ifndef TTC_RTLS5_IMPLEMENT_ANCHOR
        #    warning Missing definition for TTC_RTLS5_IMPLEMENT_ANCHOR using default value
        #define TTC_RTLS5_IMPLEMENT_ANCHOR 1
    #endif
    #if TTC_RTLS5_IMPLEMENT_ANCHOR == 1
        //   enable anchor node source code and data sets
        #define TTC_RTLS_IMPLEMENT_ANCHOR 1
    #endif

    // check if this node should provide mobile functionality (mobile nodes can use localization service of anchor nodes)
    #ifndef TTC_RTLS5_IMPLEMENT_MOBILE
        #    warning Missing definition for TTC_RTLS5_IMPLEMENT_MOBILE using default value
        #define TTC_RTLS5_IMPLEMENT_MOBILE 1
    #endif
    #if TTC_RTLS5_IMPLEMENT_MOBILE == 1
        //   enable mobile node source code and data sets
        #define TTC_RTLS_IMPLEMENT_MOBILE 1
    #endif

#endif
#ifdef TTC_RTLS6

    // check if this node should provide anchor functionality (anchor nodes built up coordinate system and provide localization service for mobile nodes)
    #ifndef TTC_RTLS6_IMPLEMENT_ANCHOR
        #    warning Missing definition for TTC_RTLS6_IMPLEMENT_ANCHOR using default value
        #define TTC_RTLS6_IMPLEMENT_ANCHOR 1
    #endif
    #if TTC_RTLS6_IMPLEMENT_ANCHOR == 1
        //   enable anchor node source code and data sets
        #define TTC_RTLS_IMPLEMENT_ANCHOR 1
    #endif

    // check if this node should provide mobile functionality (mobile nodes can use localization service of anchor nodes)
    #ifndef TTC_RTLS6_IMPLEMENT_MOBILE
        #    warning Missing definition for TTC_RTLS6_IMPLEMENT_MOBILE using default value
        #define TTC_RTLS6_IMPLEMENT_MOBILE 1
    #endif
    #if TTC_RTLS6_IMPLEMENT_MOBILE == 1
        //   enable mobile node source code and data sets
        #define TTC_RTLS_IMPLEMENT_MOBILE 1
    #endif

#endif
#ifdef TTC_RTLS7

    // check if this node should provide anchor functionality (anchor nodes built up coordinate system and provide localization service for mobile nodes)
    #ifndef TTC_RTLS7_IMPLEMENT_ANCHOR
        #    warning Missing definition for TTC_RTLS7_IMPLEMENT_ANCHOR using default value
        #define TTC_RTLS7_IMPLEMENT_ANCHOR 1
    #endif
    #if TTC_RTLS7_IMPLEMENT_ANCHOR == 1
        //   enable anchor node source code and data sets
        #define TTC_RTLS_IMPLEMENT_ANCHOR 1
    #endif

    // check if this node should provide mobile functionality (mobile nodes can use localization service of anchor nodes)
    #ifndef TTC_RTLS7_IMPLEMENT_MOBILE
        #    warning Missing definition for TTC_RTLS7_IMPLEMENT_MOBILE using default value
        #define TTC_RTLS7_IMPLEMENT_MOBILE 1
    #endif
    #if TTC_RTLS7_IMPLEMENT_MOBILE == 1
        //   enable mobile node source code and data sets
        #define TTC_RTLS_IMPLEMENT_MOBILE 1
    #endif

#endif
#ifdef TTC_RTLS8

    // check if this node should provide anchor functionality (anchor nodes built up coordinate system and provide localization service for mobile nodes)
    #ifndef TTC_RTLS8_IMPLEMENT_ANCHOR
        #    warning Missing definition for TTC_RTLS8_IMPLEMENT_ANCHOR using default value
        #define TTC_RTLS8_IMPLEMENT_ANCHOR 1
    #endif
    #if TTC_RTLS8_IMPLEMENT_ANCHOR == 1
        //   enable anchor node source code and data sets
        #define TTC_RTLS_IMPLEMENT_ANCHOR 1
    #endif

    // check if this node should provide mobile functionality (mobile nodes can use localization service of anchor nodes)
    #ifndef TTC_RTLS8_IMPLEMENT_MOBILE
        #    warning Missing definition for TTC_RTLS8_IMPLEMENT_MOBILE using default value
        #define TTC_RTLS8_IMPLEMENT_MOBILE 1
    #endif
    #if TTC_RTLS8_IMPLEMENT_MOBILE == 1
        //   enable mobile node source code and data sets
        #define TTC_RTLS_IMPLEMENT_MOBILE 1
    #endif

#endif
#ifdef TTC_RTLS9

    // check if this node should provide anchor functionality (anchor nodes built up coordinate system and provide localization service for mobile nodes)
    #ifndef TTC_RTLS9_IMPLEMENT_ANCHOR
        #    warning Missing definition for TTC_RTLS9_IMPLEMENT_ANCHOR using default value
        #define TTC_RTLS9_IMPLEMENT_ANCHOR 1
    #endif
    #if TTC_RTLS9_IMPLEMENT_ANCHOR == 1
        //   enable anchor node source code and data sets
        #define TTC_RTLS_IMPLEMENT_ANCHOR 1
    #endif

    // check if this node should provide mobile functionality (mobile nodes can use localization service of anchor nodes)
    #ifndef TTC_RTLS9_IMPLEMENT_MOBILE
        #    warning Missing definition for TTC_RTLS9_IMPLEMENT_MOBILE using default value
        #define TTC_RTLS9_IMPLEMENT_MOBILE 1
    #endif
    #if TTC_RTLS9_IMPLEMENT_MOBILE == 1
        //   enable mobile node source code and data sets
        #define TTC_RTLS_IMPLEMENT_MOBILE 1
    #endif

#endif
#ifdef TTC_RTLS10

    // check if this node should provide anchor functionality (anchor nodes built up coordinate system and provide localization service for mobile nodes)
    #ifndef TTC_RTLS10_IMPLEMENT_ANCHOR
        #    warning Missing definition for TTC_RTLS10_IMPLEMENT_ANCHOR using default value
        #define TTC_RTLS10_IMPLEMENT_ANCHOR 1
    #endif
    #if TTC_RTLS10_IMPLEMENT_ANCHOR == 1
        //   enable anchor node source code and data sets
        #define TTC_RTLS_IMPLEMENT_ANCHOR 1
    #endif

    // check if this node should provide mobile functionality (mobile nodes can use localization service of anchor nodes)
    #ifndef TTC_RTLS10_IMPLEMENT_MOBILE
        #    warning Missing definition for TTC_RTLS10_IMPLEMENT_MOBILE using default value
        #define TTC_RTLS10_IMPLEMENT_MOBILE 1
    #endif
    #if TTC_RTLS10_IMPLEMENT_MOBILE == 1
        //   enable mobile node source code and data sets
        #define TTC_RTLS_IMPLEMENT_MOBILE 1
    #endif

#endif
//}

//}Static Configuration
#ifdef EXTENSION_rtls_crtof_simple_2d
    #include "rtls/rtls_crtof_simple_2d_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_rtls_square4
    #include "rtls/rtls_square4_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

/** { Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_rtls_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_rtls_architecture
    #  warning Missing low-level definition for t_ttc_rtls_architecture (using default)
    #define t_ttc_rtls_architecture void
#endif
//}

//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_rtls_errorcode       return codes of RTLS devices
    ec_rtls_OK = 0,

    // other warnings go here..

    ec_rtls_ERROR,                  // general failure
    ec_rtls_NULL,                   // NULL pointer not accepted
    ec_rtls_RadioLocked,            // ttc_radio is locked by another UserID (try again later)
    ec_rtls_CannotLocalize,         // a localiuation request was not successfull
    ec_rtls_NotAvailable,           // requested feature not available in current configuration
    ec_rtls_DeviceNotFound,         // corresponding device could not be found
    ec_rtls_InvalidConfiguration,   // sanity check of device configuration failed
    ec_rtls_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..

    ec_rtls_unknown                // no valid errorcodes past this entry
} e_ttc_rtls_errorcode;
typedef enum {   // e_ttc_rtls_architecture    types of architectures supported by RTLS driver
    ta_rtls_None = 0,       // no architecture selected


    ta_rtls_crtof_simple_2d, // automatically added by ./create_DeviceDriver.pl
    ta_rtls_square4, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_rtls_unknown        // architecture not supported
} e_ttc_rtls_architecture;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef union {  // architecture dependent configuration

#ifdef EXTENSION_rtls_crtof_simple_2d
    t_rtls_crtof_simple_2d_config crtof_simple_2d;  // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_rtls_square4
    t_rtls_square4_config square4;  // automatically added by ./create_DeviceDriver.pl
#endif
    //InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_rtls_architecture;

typedef enum {  // e_ttc_rtls_packet - type of t_ttc_rtls_packet.Union == type(Union)
    ttc_rtls_packet_None,

    // Note: DO NOT CHANGE ORDER OF ENTRIES BELOW!

    // -----------------------------------------------------------------------------------------------------
    ttc_rtls_packet_generic,                            // packet types handled by anchor and mobile nodes below

    ttc_rtls_packet_generic_report_distances,           // packet transports distances being requested before => type(Union) = t_rtls_common_packet_distances_report
    ttc_rtls_packet_generic_report_locations,           // packet transports locations of anchor nodes        => type(Union) = rtls_common_packet_positions_report_t

    ttc_rtls_packet_generic_request_ranging,            // implemented in radio_common_ranging_reply_isr()
    ttc_rtls_packet_generic_reply_ranging,              // implemented in radio_common_ranging_reply_isr()
    ttc_rtls_packet_generic_reply_localization,         // implemented in radio_common_ranging_reply_isr()


    // -----------------------------------------------------------------------------------------------------
    ttc_rtls_packet_2anchor,                            // packet types handled by anchor nodes below

    ttc_rtls_packet_2anchor_builtup_start,              // sender is requesting coordinate system rebuilding        => type(Union) = t_u8
    ttc_rtls_packet_2anchor_request_all_ranges,         // sender is requesting all known distances to other nodes  => type(Union) = t_u8
    ttc_rtls_packet_2anchor_request_range_to_northpole, // sender is requesting a ranging measure to northpole      => type(Union) = ToDo
    ttc_rtls_packet_2anchor_report_range_to_northpole,  // packet transports distance from sender to northpole      => type(Union) = t_rtls_common_packet_distances_report

    ttc_rtls_packet_2anchor_request_localization,       // sender is requesting a ranging measure (implemented in radio_common_ranging_reply_isr())


    // -----------------------------------------------------------------------------------------------------
    ttc_rtls_packet_2mobile,                            // packet types handled by mobile nodes below
    //...


    ttc_rtls_packet_unknown
} e_ttc_rtls_packet;

typedef struct { // format of packet payload for rtls communication
    e_ttc_rtls_packet   Type;   // packet type (also defines structure of next bytes in packet payload
} __attribute__( ( __packed__ ) ) t_ttc_rtls_payload_header;

typedef struct { // format of packet payload for rtls communication
    t_ttc_rtls_payload_header Header; // header of all rtls packets
    union {                                // specialized payload formats being provided by rtls low-level drivers
        t_u8                           Unused;
        t_rtls_common_packet           Common;
#ifdef EXTENSION_rtls_crtof_simple_2d
        t_rtls_crtof_simple_2d_packet  CRTOF_Simple_2D;
#endif
        // Each low-level driver may define specialized payload formats
        // Be sure to add one entry to e_ttc_rtls_packet for each special format!
    } Union;
} __attribute__( ( __packed__ ) ) t_ttc_rtls_packet;

typedef struct { // static minimum, maximum and default values of features of single rtls

    // Add any amount of architecture independent values describing rtls devices here.
    // You may also want to add a plausibility check for each value to _ttc_rtls_configuration_check().

    /** Example Features

    t_u16 MinBitRate;   // minimum allowed transfer speed
    t_u16 MaxBitRate;   // maximum allowed transfer speed
    */

    t_u16    MinimumAnchorNodes; // minimum required amount of anchor nodes
    t_u16    MaximumAnchorNodes; // maximum supported amount of anchor nodes
    unsigned AnchorNodes1D : 1;  // == 1: anchor nodes may be located in a 1 dimensional space (on a line)
    unsigned AnchorNodes2D : 1;  // == 1: anchor nodes may be located in a 2 dimensional space (on a plane)
    unsigned AnchorNodes3D : 1;  // == 1: anchor nodes may be located freely in a 3 dimensional space

} __attribute__( ( __packed__ ) ) t_ttc_rtls_features;

typedef struct { // t_ttc_rtls_config_anchor - anchor node specific configuration

    t_u16                     Index_AnchorID;   /* !=-1: index of AnchorIDs[] where local node address is stored
                                              *       LocalID = AnchorIDs[AnchorIndex_Local]
                                              * ==-1: LocalID does not point into AnchorIDs[]
                                              */
    t_u8                      RadioUserID;      // used to lock ttc_radio for exclusive access
    t_u8                      Amount_DroppedRx; // amount of packets being dropped from ListRX (>0: internal error: received packets are not processed and block ListRX. Check your implementation!)
    e_ttc_rtls_state          State;            // current statemachine state for anchor node operation
    t_ttc_radio_job_socket  SocketJob;      // statemachine data for ttc_radio_packet_received_tryget()
} __attribute__( ( __packed__ ) ) t_ttc_rtls_config_anchor;

typedef struct { // ttc_rtls_measure_t - generic measure statistic
    t_u32 Lower;    // lowest seen value
    t_u32 Upper;    // highest seen value
    t_u32 Median;   // (Lower + Upper) / 2
    t_u32 Average;  // cummulative average of all measures
} t_ttc_rtls_statistics;

typedef struct { // data used for localization
    union { // using same memory block for different types of data to reduce memory usage
        t_ttc_radio_distance* Radio;              // array of distances to anchor nodes as returned from ttc_radio_ranging_request() (memory is overwritten by Distances.SLAM while processing localization replies)
        t_ttc_slam_distance*  SLAM;               // array of distances prepared for ttc_slam_localize_foreigner() call from Distances.Radio
    } Distances;
    t_ttc_math_vector3d_xyz* AnchorLocations;  // array of locations of anchor nodes (as received from ranging request
    t_ttc_rtls_statistics*   Rangings;         // array of statistics about all previously taken range measures for a single localization
    t_u8                     ArraySizes;       // size of Distances.Radio[], Distances.SLAM[], Locations[], Rangings[]
    t_u8                     AmountValid;      // amount of valid entries Distances.SLAM[0..AmountValid-1], Locations[0..AmountValid-1], Statistics[0..AmountValid-1]
} t_ttc_rtls_localization;

typedef struct { // t_ttc_rtls_config_anchor - anchor node specific configuration

    t_ttc_rtls_localization   Localization;    // results from last localization run
    t_u8                      RadioUserID;     // used to lock ttc_radio for exclusive access
    e_ttc_rtls_state          State;           // current statemachine state for mobile node operation
    t_ttc_radio_job_socket  SocketJob;     // statemachine data for ttc_radio_packet_received_tryget()

} __attribute__( ( __packed__ ) ) t_ttc_rtls_config_mobile;

typedef struct { //          architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_rtls_init()
    //       and after ttc_rtls_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // Hint: Order structure elements for optimal alignment.
    //       You should place smaller datatypes like t_u8 and t_u16 after all t_u32 and pointer types
    //       to prevent insertion of pad-bytes. Most architectures don't support placing 32-bit datatypes at
    //       unaligned address.


    struct { // Fields to be set by application before first calling ttc_rtls_init() --------------
        // Do not change these values after calling ttc_rtls_init()!

        const t_ttc_packet_address* AnchorIDs;              // pointer to first entry of Array of anchor addresses (check description of current low-level driver for constraints!)
        const t_ttc_packet_address* LocalID;                // pointer to address of local node (may point into AnchorIDs[] if local node is an anchor node)
        t_u32                       TimeOut_RangeMeasureMS; // timeout for a single range measure (milliseconds)
        t_u8                        Amount_AnchorIDs;       // amount of valid entries in AnchorIDs[0..Amount_AnchorIDs-1]
        t_u8                        Index_Radio;            // logical index of ttc_radio instance to use (1..ttc_radio_get_max_index())
        t_u8                        Index_SLAM;             // logical index of ttc_slam instance to use (1..ttc_slam_get_max_index())
        t_u8                        Amount_RawMeasures;     // amount of raw range measures for each distance (greater value gives better average value)
        t_u8                        Amount_TxRetries;       // >0: if a transmission fails for some reason, it will be restart some times; ==0: no retries (abort on first failed transmission)

        struct { // enable certain features
            unsigned IsAnchorNode            : 1; // ==1: run anchor statemachine to built up coordinate system and provide localization service (requires TTC_RTLS_IMPLEMENT_ANCHOR==1)
            unsigned IsMobileNode            : 1; // ==1: run mobile statemachine to be able to localize ourself (requires TTC_RTLS_IMPLEMENT_MOBILE==1)
        } __attribute__( ( __packed__ ) ) Flags;
    } Init;

    // (see low-level rtls_*_configuration_check() function for restricitions!)

    // Some low-level rtls drivers may be able to detect available anchor nodes dynamically.
    // Check description of current low-level rtls driver for details!

    // Fields below are read-only for application! ---------------------------------------

    // fields are ordered for optimal packing (32 bit fields first)
    u_ttc_rtls_architecture*   LowLevelConfig;   // low-level configuration (structure defined by low-level driver)
    const t_ttc_rtls_features* Features;         // constant features of this rtls
    t_ttc_radio_config*        ConfigRadio;      // configuration of radio device to use
    t_ttc_slam_config*         ConfigSLAM;       // configuration of slam device to use
    t_u16                      LocalID16;        // copy of AnchorIDs[Index_LocalID].Address16 (copied in ttc_rtls_init() )
    t_u16                      SequenceNumber;   // free running counter to generate unique sequence numbers (can be used by low-level driver)
    t_u8                       LogicalIndex;     // automatically set: logical index of device to use (1 = TTC_RTLS1, ...)
    e_ttc_rtls_architecture    Architecture;     // type of architecture used for current rtls device

#if TTC_RTLS_IMPLEMENT_ANCHOR == 1
    t_ttc_rtls_config_anchor   Anchor;           // anchor node specific configuration data
#endif
#if TTC_RTLS_IMPLEMENT_MOBILE == 1
    t_ttc_rtls_config_mobile   Mobile;           // mobile node specific configuration data
#endif

    struct { // generic configuration bits common for all low-level Drivers
        unsigned Initialized             : 1; // ==1: device has been initialized successfully
        unsigned IsCoordinateSystemReady : 1; // ==1: coordinate system has been built up (localization service is available). Note: RTLS low-level driver must update this flag via calling rtls_common_anchor_coordinate_system_complete()!
    } __attribute__( ( __packed__ ) ) Flags;

    // Last returned error code
    // IMPORTANT: Every ttc_rtls_*() function that returns e_ttc_rtls_errorcode has to update this value if it returns an error!
    e_ttc_rtls_errorcode LastError;
    // Additional high-level attributes go here..

} t_ttc_rtls_config;


//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_RTLS_TYPES_H
