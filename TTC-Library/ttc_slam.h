#ifndef TTC_SLAM_H
#define TTC_SLAM_H
/** { ttc_slam.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of ttc_slam (Do not delete this line!)
 *
 *  Simultaenous Localization And Mapping (SLAM) is a universal problem that can be
 *  found in any autonomous navigation application.
 *  The SLAM problem itself consists of finding the 3D coordinates of n nodes by measuring
 *  distances and/ or directions between individual nodes without prior information.
 *  The basic mathematical mechanisms are lateration and triangulation.
 *
 *  Difficulties of SLAM are
 *  - Inaccuracy of Measures (e.g. Noise)
 *  - Complexity (Amount of distances between n Nodes = n * (n-1) * (n-2) * ... * 1 = n!)
 *
 *  Different SLAM algorithms can be optimized for different scenarios.
 *  ttc_slam provides a generic high-level interface and different low-level drivers to
 *  implement the actual algorithm. Multiple low-level drivers can be active for different
 *  problems at the same time. See header files of low-level drivers in ttc-lib/slam/ for a
 *  description of individual algorithms.
 *
 *  Usage of ttc_slam
 *    All ttc-modules share the same usage paradigm:
 *    1) t_ttc_slam_config* Config = ttc_slam_create( );
 *       or
 *       t_ttc_slam_config* Config = ttc_slam_get_configuration( LogicalIndex );
 *
 *    2) Change *Config to configure module
 *
 *    3) t_u8 LogicalIndex = Config->LogicalIndex;
 *
 *    3) ttc_slam_init( LogicalIndex );
 *
 *    5) Provide as many distance updates as possible
 *       ttc_driver_slam_update_distance(NodeIndexA, NodeIndexB, DistanceAB);
 *
 *    6) Try to calculate mapping
 *       BOOL MappingOK = ttc_slam_calculate_mapping(LogicalIndex)
 *
 *    7) Read out Node Positions
 *       if (MappingOK) {
 *         ...
 *       }
 *
 *  Device Index
 *  Devices are addressed by a logical index with no correlation to the order of physical devices.
 *  Logical indices start at 1 to simply detect uninitialized values.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
}*/

#ifndef EXTENSION_ttc_slam
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_slam.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_slam.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_slam_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level slam only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function Declarations ************************************************{
 *
 * The functions declared below provide the main interface for
 * slam devices on all supported architectures.
 * Check slam/slam_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Calculates new localization mapping of all nodes (if possible)
 *
 * @param LogicalIndex  (t_u8)   logical index of SLAM instance. Each logical device <n> is defined via TTC_SLAM<n>* constants in compile_options.h and extensions.active/makefile
 * @return              (BOOL)   ==TRUE: new mapping has been calculated; ==FALSE: not egnough data available: update more distances!
 */
BOOL ttc_slam_calculate_mapping( t_u8 LogicalIndex );
BOOL _driver_slam_calculate_mapping( t_ttc_slam_config* Config );

/** allocates a new SLAM instance and returns pointer to its configuration
 *
 * @return (t_ttc_slam_config*)  pointer to new configuration. Set all Init fields before calling ttc_slam_init() on it.
 */
t_ttc_slam_config* ttc_slam_create();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_slam_get_max_index();

/** Returns pointer to current data of indexed node
 *
 * Nodes are stored in an array. The size of each entry is only known by current low-level driver.
 * Calculation of memory address of indexed node is done in low-level driver.
 *
 * @param LogicalIndex  (t_u8)               logical index of SLAM instance. Each logical device <n> is defined via TTC_SLAM<n>* constants in compile_options.h and extensions.active/makefile
 * @param NodeIndex     (t_u16)              index of desired node (1..ttc_slam_get_configuration()->Init.Amount_Nodes)
 * @return              (t_ttc_slam_node*)   pointer to node data
 */
t_ttc_slam_node* ttc_slam_get_node( t_u8 LogicalIndex, t_u16 NodeIndex );
t_ttc_slam_node* _driver_slam_get_node( t_ttc_slam_config* Config, t_u16 NodeIndex );

/** delivers pointer to 3D coordinates of indexed node
 *
 * @param LogicalIndex (t_u8)          Logical index of slam instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SLAM<n> lines in extensions.active/makefile
 * @param NodeIndex    (t_u16)         index of desired node (1..ttc_slam_get_configuration()->Init.Amount_Nodes)
 * @return (t_ttc_math_vector3d_xyz*)  pointer to calculated 3D coordinates (undefined components read as TTC_SLAM_UNDEFINED_VALUE)
 */
t_ttc_math_vector3d_xyz* ttc_slam_get_node_position( t_u8 LogicalIndex, t_u16 NodeIndex );

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    logical index of SLAM instance. Each logical device <n> is defined via TTC_SLAM<n>* constants in compile_options.h and extensions.active/makefile
 * @return                configuration of indexed device
 */
t_ttc_slam_config* ttc_slam_get_configuration( t_u8 LogicalIndex );

/** return stored distance between two nodes
 *
 * Note: Current low-level driver may not store all possible n! distances among n nodes.
 * Note: Even if no distance is stored, it might be calculatable using ttc_slam_calculate_distance()
 *
 * @param Config       (t_ttc_slam_config*)  Configuration of slam device as returned by ttc_slam_get_configuration()
 * @param LogicalIndex (t_u8)                Logical index of slam instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SLAM<n> lines in extensions.active/makefile
 * @param NodeIndexA   (t_u16)               index of node #1 (1..ttc_slam_get_configuration()->Init.Amount_Nodes)
 * @param NodeIndexB   (t_u16)               index of node #2 (1..ttc_slam_get_configuration()->Init.Amount_Nodes)
 * @return             (ttm_number)          ==TTC_SLAM_UNDEFINED_VALUE: no distance stored; previously stored value otherwise
 */
ttm_number ttc_slam_get_distance( t_u8 LogicalIndex, t_u16 NodeIndexA, t_u16 NodeIndexB );
ttm_number _driver_slam_get_distance( t_ttc_slam_config* Config, t_u16 NodeIndexA, t_u16 NodeIndexB );


/** initialize indexed device for use
 *
 * @param LogicalIndex    logical index of SLAM instance. Each logical device <n> is defined via TTC_SLAM<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: slam device has been initialized successfully; != 0: error-code
 */
e_ttc_slam_errorcode ttc_slam_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    logical index of SLAM instance. Each logical device <n> is defined via TTC_SLAM<n>* constants in compile_options.h and extensions.active/makefile
 */
void ttc_slam_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_slam_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of SLAM instance. Each logical device <n> is defined via TTC_SLAM<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: default values have been loaded successfully for indexed slam device; != 0: error-code
 */
e_ttc_slam_errorcode  ttc_slam_load_defaults( t_u8 LogicalIndex );

/** Prepares slam Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_slam_prepare();
void _driver_slam_prepare();

/** Reset all nodes back to uninitialized state.
 *
 * Resetting a SLAM instance will keep all dynamic allocated memories.
 * Depending on the low-level driver, the amount of nodes may have to stay unchanged.
 * Read documentation of current low-level driver if it allows to change AmountNodes
 * before calling ttc_slam_reset()!
 *
 * @param LogicalIndex    logical index of SLAM instance. Each logical device <n> is defined via TTC_SLAM<n>* constants in compile_options.h and extensions.active/makefile

 */
void ttc_slam_reset( t_u8 LogicalIndex );

/** Report a new distance measure between two nodes A and B.
 *
 * Distances can be updated at any time. Low-level driver will take care of filtering.
 * The amount of distance measures that is required to compute all locations may depend
 * on current low-level driver.
 *
 * Note: Distance update might be a little bit faster is NodeIndexA < NodeIndexB.
 *
 * @param LogicalIndex  (t_u8)               logical index of SLAM instance. Each logical device <n> is defined via TTC_SLAM<n>* constants in compile_options.h and extensions.active/makefile
 * @param Config        (t_ttc_slam_config*) initialized slam configuration as returned by ttc_slam_get_configuration()
 * @param NodeIndexA    (t_u16)              index of node #1 (1..ttc_slam_get_configuration()->Init.Amount_Nodes)
 * @param NodeIndexB    (t_u16)              index of node #2 (1..ttc_slam_get_configuration()->Init.Amount_Nodes)
 * @param RawValue      (ttm_number)         Raw measured distance value.
 */
void ttc_slam_update_distance( t_u8 LogicalIndex, t_u16 NodeIndexA, t_u16 NodeIndexB, ttm_number RawValue );
void _driver_slam_update_distance( t_ttc_slam_config* Config, t_u16 NodeIndexA, t_u16 NodeIndexB, ttm_number RawValue );

/** Report a new calculated coordinate.
 *
 * Depending on current low-level driver, coordinate updates may just be stored or filtered.
 *
 * @param LogicalIndex  (t_u8)   logical index of SLAM instance. Each logical device <n> is defined via TTC_SLAM<n>* constants in compile_options.h and extensions.active/makefile
 * @param Config        (t_ttc_slam_config*) initialized slam configuration as returned by ttc_slam_get_configuration()
 * @param NodeIndex     (t_u16)  index of node to update (1..ttc_slam_get_configuration()->Init.Amount_Nodes)
 * @param X             (ttm_number)  !=TTC_SLAM_UNDEFINED_VALUE: new X-coordinate (scale must match to reported distances!)
 * @param Y             (ttm_number)  !=TTC_SLAM_UNDEFINED_VALUE: new Y-coordinate (scale must match to reported distances!)
 * @param Z             (ttm_number)  !=TTC_SLAM_UNDEFINED_VALUE: new Z-coordinate (scale must match to reported distances!)
 */
void ttc_slam_update_coordinates( t_u8 LogicalIndex, t_u16 NodeIndex, ttm_number X, ttm_number Y, ttm_number Z );
void _driver_slam_update_coordinates( t_ttc_slam_config* Config, t_u16 NodeIndex, ttm_number X, ttm_number Y, ttm_number Z );

/** Calculate distance between two nodes from ttc_slam instance in 3D-space
 *
 * Note: Calculation is faster when both nodes lie on a orthogonal plane or line (e.g. NodeA.Position.Z==NodeB.Position.Z).
 * Note: To calculate distance to foreign node, you may use ttc_math_calculate_distance()
 *
 * @param Config       (t_ttc_slam_config*)  Configuration of slam device as returned by ttc_slam_get_configuration()
 * @param LogicalIndex (t_u8)                Logical index of slam instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SLAM<n> lines in extensions.active/makefile
 * @param NodeIndexA   (t_u16)               index of node #1 (1..ttc_slam_get_configuration()->Init.Amount_Nodes)
 * @param NodeIndexB   (t_u16)               index of node #2 (1..ttc_slam_get_configuration()->Init.Amount_Nodes)
 * @return             (ttm_number)          calculated distance between nodes A and B
 */
ttm_number ttc_slam_calculate_distance( t_u8 LogicalIndex, t_u16 NodeIndexA, t_u16 NodeIndexB );
ttm_number _driver_slam_calculate_distance( t_ttc_slam_config* Config, t_u16 NodeIndexA, t_u16 NodeIndexB );

/** Calculate position of a foreign node relative to nodes in slam instance
 *
 * Once a slam instance has completed it's mapping process, its nodes can be used as reference (anchor) nodes
 * to localize foreign (mobile) nodes.
 *
 * A foreign node is a node that was not known during initial mapping process. In typical setups, a fixed set of
 * anchor nodes is installed in a location. Mobile (foreign) nodes can enter and leave the location. The anchor nodes
 * build up their own coordinate system by completing the mapping process. The anchor nodes then provide a localization
 * service to all visiting foreign nodes.
 *
 * To localize a foreign node, distance measures from foreigner to three or more different nodes of a ttc_slam
 * instance have to be taken. These distances are then used to laterate the position of the foreigner.
 *
 * Constraints on Reference Nodes
 * 1) All reference nodes are equal (or AmountReferences==1)
 *    => No localization possible
 *
 * 2) Two different references
 *    => Localization has 2 solutions in 2D space.
 *       Both reference nodes must have exactly one identical coordinate (must lie on an orthogonal line)
 *       Localized_Left  = matching position on left side of vector References[0]->References[1].
 *       Localized_Right = matching position on right side of vector References[0]->References[1].
 *
 *       if (References[0].X == References[1].X)
 *          Localized_Left.X = References[0].X;
 *          Localized_Right.X = References[0].X;
 *        }
 *       if (References[0].Y == References[1].Y)
 *          Localized_Left.Y = References[0].Y;
 *          Localized_Right.Y = References[0].Y;
 *        }
 *       if (References[0].Z == References[1].Z) {
 *          Localized_Left.Z = References[0].Z;
 *          Localized_Right.Z = References[0].Z;
 *        }
 *
 * 3) Three different references or all references lie on a plane
 *    => Localization has 2 solutions in 3D space:
 *       Localized_Left  = matching position above plane being defined by References[0..2]
 *       Localized_Right = matching position below plane being defined by References[0..2]
 *
 * 4) Four different references given
 *    a) Only first three references are used to laterate two possible positions:
 *       Localized_Left, Localized_Right
 *    b) Distances from Localized_Left and Localized_Right to fourth reference are calculated.
 *    c) Positions are sorted so that
 *       distance(Localized_Left, References[3]) < distance(Localized_Right, References[3])
 *
 *
 * Note: Each slam instance builds up it's own coordinate system. Positions from localizations in different
 *       ttc_slam instances normally can not be correlated.
 *
 * Note: Mapping must be completed to use this function (Config->Flags.Completed_Mapping==1)
 *
 * @param Config           (t_ttc_slam_config*)        Configuration of slam device as returned by ttc_slam_get_configuration()
 * @param LogicalIndex     (t_u8)                      Logical index of slam instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SLAM<n> lines in extensions.active/makefile
 * @param Localized_Left   (t_ttc_math_vector3d_xyz*)  !=NULL: where to store 3D node position relative to coordinate system of slam instance (first mathematical solution in under defined situation)
 * @param Localized_Right  (t_ttc_math_vector3d_xyz*)  !=NULL: where to store 3D node position relative to coordinate system of slam instance (second mathematical solution in under defined situation)
 * @param References       (t_ttc_slam_distance*)      array of distance measures to different reference nodes (must not contain doubled node indices!)
 * @param AmountReferences (t_u8)                      size of References[] (will read References[0]..References[AmountReferences-1])
 * @return                 (e_ttc_math_errorcode)      ==0: *Position has been updated; >0: could not localize node (check return code for details!)
 */
e_ttc_math_errorcode ttc_slam_localize_foreigner( t_u8 LogicalIndex, t_ttc_math_vector3d_xyz* Localized_Left, t_ttc_math_vector3d_xyz* Localized_Right, t_ttc_slam_distance* References, t_u8 AmountReferences );


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_slam_ are passed to interfaces/ttc_slam_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl slam UPDATE
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_slam_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_slam_features.
 *
 * @param Config        Configuration of slam device
 * @return              Fields of *Config have been adjusted if necessary
 */
void _driver_slam_configuration_check( t_ttc_slam_config* Config );

/** shutdown single SLAM unit device
 * @param Config        Configuration of slam device
 * @return              == 0: SLAM has been shutdown successfully; != 0: error-code
 */
e_ttc_slam_errorcode _driver_slam_deinit( t_ttc_slam_config* Config );

/** initializes single SLAM unit for operation
 * @param Config        Configuration of slam device
 * @return              == 0: SLAM has been initialized successfully; != 0: error-code
 */
e_ttc_slam_errorcode _driver_slam_init( t_ttc_slam_config* Config );

/** loads configuration of indexed SLAM unit with default values
 * @param Config        Configuration of slam device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_slam_errorcode _driver_slam_load_defaults( t_ttc_slam_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of slam device
 */
void _driver_slam_reset( t_ttc_slam_config* Config );
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_SLAM_H
