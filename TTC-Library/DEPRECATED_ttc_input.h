#ifndef TTC_INPUT
#define TTC_INPUT
/*{ ttc_input.h ************************************************
}*/


//{ Includes *************************************************************
#include "ttc_basic.h"
#include "ttc_memory.h"
#include "DEPRECATED_ttc_input_types.h"
#include "ttc_gfx.h"
#ifdef EXTENSION_400_input_ads7843
  #include "DEPRECATED_input_ads7843.h"
#endif
#ifdef EXTENSION_400_input_analog
  #include "DEPRECATED_input_analog.h"
#endif


//} Includes
//{ Defines/ TypeDefs ****************************************************
#define ABS(a) ((a)<0 ? -(a) : (a))
#define MEDIUMLENGHT 10 // ToDo: What's this?

//} Defines
//{ Structures/ Enums ****************************************************
//} Structures/ Enums

//{ Global Variables *****************************************************


//} Global Variables

//{ Function prototypes **************************************************




extern BOOL (*function_read_position)(volatile u16_t *x,volatile u16_t *y);

/* initializes touch screen for use as input device
 */
void ttc_input_init(u8_t DisplayIndex);


/** create a new thread named _ttc_input_task_TouchCheckAll which registers the funcition input_ads7843_TouchCheckAll in the scheduler
 */
void ttc_input_start(u8_t DisplayIndex);

void DEPRECATED_ttc_input.callibrate();

void ttc_input_maintask(void* TaskArgument);

void ttc_input_single_check_all(void* pvParamters);

bool DEPRECATED_ttc_input.check_touch(tit_InputArea_t* InputData);

void ttc_input_delete_all_input_areas();

/** register the function which is given by the pointer (TouchHandler)
  * the Argument of the function is void* Argument
  * the positions of the window which is responsible for the execution of the given function is framed by the values of LEFT,TOP,Right and Bottom
  */
void ttc_input_register_input_area(void (*TouchHandler)(void* Argument), void* Argument, u16_t Left, u16_t Top, u16_t Right, u16_t Bottom, const char* Text, tit_InputAreaShape_e Shape, tit_MouseEvent_e Event);

void ttc_input_delete_input_area(tit_InputArea_t* DeleteArea);

tit_InputArea_t* ttc_input_get_first_button();

BOOL ttc_input_get_touch_position(u16_t *X,u16_t *Y);

tit_InputArea_t* ttc_input_get_empty_input_area();

void _ttc_input_update_configuration(u8_t DisplayIndex);
//} Function prototypes
void ttc_input_reset(u8_t DisplayIndex);
#endif
