#ifndef TASKS_EXTENSION_H
#define TASKS_EXTENSION_H
/*{ tasks_extension.h ************************************************
 
 Empty template for new header files.
 Copy and adapt to your needs.
 
}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

 // Basic set of helper functions
#include "stm32_io.h"

#define USE_STDPERIPH_DRIVER
#include "stm32f10x.h"
#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
#include "stm32f10x_conf.h"

/*{ optional
// ADC
#include "stm32f10x_adc.h"
#include "stm32f10x_dma.h"

// Timers
#include "stm32f10x_tim.h"

#include "stm32f10x_it.h"
#include "system_stm32f10x.h"
}*/

#include "bits.h"

#include <string.h>
#include "FreeRTOS.h"
#include "task.h"

//} Includes
//{ Structures/ Enums ****************************************************

//} Structures/ Enums
//{ Global Variables *****************************************************



//} Global Variables
//{ Function prototypes **************************************************

/* returns handle of task corresponding to given index
 * This function scans lists of all ready tasks and returns handle of n-th found task (n == TaskIndex).
 * The intention of this function is to provide an easy method to traverse through all current tasks for debugging purposes only!
 *
 *@param TaskIndex  0..uxTaskGetNumberOfTasks()
 */
#if ( ( configUSE_TRACE_FACILITY == 1 ) || ( INCLUDE_uxTaskGetStackHighWaterMark == 1 ) )
xTaskHandle getTaskHandle(unsigned portBASE_TYPE TaskIndex);
#endif

//} Function prototypes
#endif
