#ifndef TTC_RADIO_TYPES_H
#define TTC_RADIO_TYPES_H

/*{ ttc_radio_types.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Basic defines, enums and structures.
 *
 * Include this file from
 * architecture independent header files (ttc_XXX.h) and
 * architecture depend      header files (e.g. radio_stm32w.h)
 *
}*/

#include "ttc_basic_types.h"
#include "ttc_memory_types.h"

//{ Defines/ TypeDefs ****************************************************

// assert macro for radio code
#ifndef TTC_Assert_Radio    // any previous definition set (Makefile)?
#define TTC_Assert_Radio 1  // string asserts are enabled by default
#endif
#if (TTC_Assert_Radio == 1)  // use Assert()s in RADIO code (somewhat slower but alot easier to debug)
  #define Assert_Radio(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in RADIO code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_Radio(Condition, ErrorCode)
#endif

//will collect debug information of the radio into Radio_Generic->Amount_Error_***
#ifndef TTC_DEBUG_RADIO
#define TTC_DEBUG_RADIO 0
#endif
//#undef TTC_DEBUG_RADIO



// maximum allocated amount of memory buffers for all radios
#ifndef TTC_RADIO_MAX_MEMORY_BUFFERS
#define TTC_RADIO_MAX_MEMORY_BUFFERS  TTC_AMOUNT_RADIOS * 10
#endif

// maximum header-, footer- and payload-sizes required to create packets that are usable for all radios
#ifndef TTC_RADIO_MAX_Header
#define TTC_RADIO_MAX_Header 0
#endif
#ifndef TTC_RADIO_MAX_Footer
#define TTC_RADIO_MAX_Footer 0
#endif
#ifndef TTC_RADIO_MAX_Payload
#define TTC_RADIO_MAX_Payload 0
#endif

//{ TTC_RADIOn has to be defined as constant by makefile.100_board_*
#ifdef TTC_RADIO5
  #ifndef TTC_RADIO4
    #error TTC_RADIO5 is defined, but not TTC_RADIO4 - all lower TTC_RADIOn must be defined!
  #endif
  #ifndef TTC_RADIO3
    #error TTC_RADIO5 is defined, but not TTC_RADIO3 - all lower TTC_RADIOn must be defined!
  #endif
  #ifndef TTC_RADIO2
    #error TTC_RADIO5 is defined, but not TTC_RADIO2 - all lower TTC_RADIOn must be defined!
  #endif
  #ifndef TTC_RADIO1
    #error TTC_RADIO5 is defined, but not TTC_RADIO1 - all lower TTC_RADIOn must be defined!
  #endif

  #define TTC_AMOUNT_RADIOS 5
#else
  #ifdef TTC_RADIO4
    #define TTC_AMOUNT_RADIOS 4

    #ifndef TTC_RADIO3
      #error TTC_RADIO5 is defined, but not TTC_RADIO3 - all lower TTC_RADIOn must be defined!
    #endif
    #ifndef TTC_RADIO2
      #error TTC_RADIO5 is defined, but not TTC_RADIO2 - all lower TTC_RADIOn must be defined!
    #endif
    #ifndef TTC_RADIO1
      #error TTC_RADIO5 is defined, but not TTC_RADIO1 - all lower TTC_RADIOn must be defined!
    #endif
  #else
    #ifdef TTC_RADIO3

      #ifndef TTC_RADIO2
        #error TTC_RADIO5 is defined, but not TTC_RADIO2 - all lower TTC_RADIOn must be defined!
      #endif
      #ifndef TTC_RADIO1
        #error TTC_RADIO5 is defined, but not TTC_RADIO1 - all lower TTC_RADIOn must be defined!
      #endif

      #define TTC_AMOUNT_RADIOS 3
    #else
      #ifdef TTC_RADIO2

        #ifndef TTC_RADIO1
          #error TTC_RADIO5 is defined, but not TTC_RADIO1 - all lower TTC_RADIOn must be defined!
        #endif

        #define TTC_AMOUNT_RADIOS 2
      #else
        #ifdef TTC_RADIO1
          #define TTC_AMOUNT_RADIOS 1
        #else
          #define TTC_AMOUNT_RADIOS 0
        #endif
      #endif
    #endif
  #endif
#endif
//}

//} Defines
//{ Includes *************************************************************

// include structures & enums of activated low-level drivers
#ifdef EXTENSION_400_radio_stm32w        // defined by extension makefile
  #include "radio_cc1101_types.h"
#endif
#ifdef EXTENSION_400_radio_cc1101_spi   // defined by extension makefile
  #include "radio_cc1101_types.h"
#endif
#ifdef EXTENSION_400_radio_cc1120_spi   // defined by extension makefile
  #include "radio_cc1120_types.h"
#endif
#ifdef EXTENSION_400_radio_cc1190       // defined by extension makefile
  #include "radio_cc1190_types.h"
#endif

//} Includes
//{ Structures/ Enums 1 **************************************************

typedef enum {   // ttc_radio_errorcode_e
    tre_OK,                 // =0: no error
    tre_TimeOut,            // timeout occured in called function
    tre_DeviceNotFound,     // adressed radio device not available in current uC
    tre_NotImplemented,     // function has no implementation for current architecture
    tre_InvalidArgument,    // general argument error
    tre_DeviceNotReady,     // choosen device has not been initialized properly
    // tre_TxFIFO_Overrun,  // too many bytes written to hardware fifo
    tre_TxFIFO_Underrun,    // not egnough data in TX-FIFO
    tre_RxFIFO_Overrun,     // too many bytes in receive fifo
    tre_UnexpectedState,    // radio hardware reported an unexpected system state
    tre_RxCorruptPacket,    // corrupt packet received
    tre_TX_Timeout,         // radio spend to much time waiting for TX Mode
    tre_Radio_Reset_Request,//a request for the error handler to reset the radio
    tre_TX_Packet_Not_Send, //a packed was not send properly
    tre_UnknownError
} ttc_radio_errorcode_e;
typedef enum { // ttc_radio_driver_e - type of driver to use
    trd_None,
    trd_cc1101_spi, // external transceiver cc1101 connected via SPI
    trd_cc1120_spi, // external transceiver cc1120 connected via SPI
    trd_stm32w,      // internal transceiver embedded into EM357/ STM32W108
    trd_serial      // external transceiver connected to an USART Port (Serial Interface)

} ttc_radio_driver_e;
typedef enum {           // radio_cc1101_mode_e operating modes
    trm_Idle,            // radio is doing nothing
    trm_Reset,           // radio is being reset (configuration gets lost)
    trm_Sleep,           // radio is doing even less than nothing (may loose configuration)
    trm_PowerOn,         // radio shall power up after sleep state (automatically reloads configuration)
    trm_PowerOff,        // radio shall be completely shut down
    trm_Transmit,        // radio will transmit current packet
    trm_Receive,         // radio is active listening
    trm_TransmitReceive  // radio is receiving + transmitting in parallel (requires trt_Bits_t.IndepentendTransmitterReceiver==1)
} ttc_radio_mode_e;
typedef enum { // radio_amplifier_mode_e  - operating mode of amplifier
    tram_None,
    tram_Init,             // first initialization of radio amplifier
    tram_Transmit ,        // transmitter active
    tram_TransmitHighGain, // transmitter active (higher output)
    tram_Receive,          // receiver active
    tram_ReceiveHighGain,  // receiver active (higher receive amplification)
    tram_PowerDown   // Amp in Power Down Mode
} ttc_radio_amplifier_mode_e;
typedef enum { // ttc_radio_amplifier_e - type of amplifier driver to use
    tra_None,
    tra_cc1190      // external radio amplifier cc1190

} ttc_radio_amplifier_e;
typedef enum { // ttc_radio_packet_type_e - internal buffers can have different types
    trpt_None,
    trpt_Buffer,   // packet stores payload data
    trpt_Constant, // packet points to constant buffer that stores payload
    trpt_Block     // packet stores pointer to memory block that stores payload
} ttc_radio_packet_type_e;

typedef struct { // Bits
  unsigned ChannelSwitchable              : 1; // =1: current channel can be switched by software
  unsigned IndepentendTransmitterReceiver : 1; // =1: ChannelTX may be different from ChannelRX
  unsigned RxDMA                          : 1; // =1: enable DMA for Receiver
  unsigned TxDMA                          : 1; // =1: enable DMA for Transmitter
  unsigned DelayedTransmits               : 1; // =1: enable _delayed_() transmit functions
  unsigned LBT                            : 1; // =1: radio supports LBT (listen before talk)
  // bits below are written by low-level driver and readonly for everybody else
  unsigned Task_RX_Started                : 1; // =1: _ttc_radio_task_rx() has been started for this radio device
  unsigned Task_TX_Started                : 1; // =1: _ttc_radio_task_tx() has been started for this radio device
  unsigned SleepMode_IDLE                 : 1; // =1: radio enters sleep-mode IDLE (low energy consumption)
  unsigned SleepMode_OFF                  : 1; // =1: radio enters sleep-mode Off  (nearly no energy consumption)
  unsigned Duty_Cycle_Check               : 1; // =1: radio will monitor the duty cycle
  unsigned Reserved1        : 5; // pad to 16 bits
} __attribute__((__packed__)) trt_Bits_t;
typedef union { // ttc_radio_driver_t - used to store all kinds of radio dependend configurations
    u8_t Byte0;  // avoids warning "union has no members" in case no radio has been activated

#ifdef RADIO_EM357_TYPES_H
    radio_stm32w_architecture_t stm32w;
#endif

#ifdef RADIO_CC1101_TYPES_H
    radio_cc1101_driver_t cc1101_spi;
#endif
#ifdef RADIO_CC1120_TYPES_H
    radio_cc1120_driver_t cc1120_spi;
#endif

} ttc_radio_driver_t;
typedef enum { // specifies the duty cycle limit for the current radio settings
    duty_cycle_limit_not_configured, //duty cycle limit not configured
    duty_cycle_limit_none,          //equals no limit (100% allowed)
    duty_cycle_limit_10_percent,    //10.00% Duty Cycle
    duty_cycle_limit_1_percent,    //01.00% Duty Cycle
    duty_cycle_limit_0_1_percent,  //00.10% Duty Cycle
    duty_cycle_limit_0_0_1_percent //00.01% Duty Cycle
} ttc_duty_cycle_limit_e;
typedef enum {
    duty_cycle_Timeslot_none,    //no Timeslot / Disabled
    duty_cycle_Timeslot_1_sek,   // use a 1 Second Timeslot to count transmitted bytes
    duty_cycle_Timeslot_60_sek,  // use a 1 Minute Timeslot to count transmitted bytes
    duty_cycle_Timeslot_3600_sek // use a 1 Hour   Timeslot to count transmitted bytes
}ttc_duty_cycle_Timeslot_e;
typedef struct {
    ttc_duty_cycle_limit_e Duty_Cycle_Limit;
    ttc_duty_cycle_Timeslot_e Timeslot;
    u32_t Datarate;          //The transmitter Datarate in Bit/sek
    u32_t Bytes_Transmitted; //Amount Bytes transfered within the last Timeslot
    u32_t Time_elapsed;      //stores the amount of time elapsed for the current timeframe in seconds
    u32_t MaxBytes_within_timeframe; //stores the maximum amount of bytes to be transmitted within a timeframe
    u16_t Transmitted_Packets_Total; //for debug purpose only
    void (*UCB_dc_25_used)(u8_t); //user callback - duty cycle has been 25% used
    void (*UCB_dc_50_used)(u8_t); //user callback - duty cycle has been 50% used
    void (*UCB_dc_75_used)(u8_t); //user callback - duty cycle has been 75% used
    void (*UCB_dc_exceed)(u8_t); //user callback - duty cycle has exceed its limit (completely used)
    void (*UCB_dc_timeframe_expired)(u8_t); //user callback - duty cycle timeframe has expired / new slot is going to be started
}ttc_duty_cycle_t;
typedef struct ttc_radio_generic_s { // ttc_radio_generic_t - architecture independent configuration of single radio

    // Note: Write-access to this structure is only allowed before first ttc_radio_init() call!
    //       After initialization, only read accesses are allowed for high-level code.

    union { // bit-field of flags
        u16_t All;
        trt_Bits_t Bit;
    } Flags;

    ttc_radio_mode_e Mode;    // current operating mode of this transceiver

    u8_t DeviceAddress;       // protocol address that identifies this radio (802.15.4 supports only 8-bit adrresses)
    u8_t TargetAddress;       // default target address for all following packets

    u8_t LevelRx;             // transmit power
    u8_t LevelTx;             // receive power

    u8_t ChannelTx;           // current channel index to be used for transmit
    u8_t ChannelRx;           // current channel index to be used for receive

    u8_t LBT_Enabled;
    // queues for transmit/ receive
    u8_t Size_QueueRx;        // max amount of entries to store in receive queue
    u8_t Size_QueueTx;        // max amount of memory blocks to manage for transmit

    //
    //{ fields below belong to low-level drivers and are not to be set by application -------------------
    //

    u8_t LogicalIndex;        // automatically set: logical index of radio to use (1 = TTC_RADIO1, ...)

    // automatically unlocked whenever a packet has been received by low-level driver
    ttc_mutex_smart_t* MutexPacketReceived;

    // transports received data
    ttc_queue_pointers_t* Queue_Rx;

    // transports data to be transmitted
    ttc_queue_pointers_t* Queue_Tx;

    u8_t MaxLevelTx;          // maximum allowed power level for transmit
    u8_t MaxLevelRx;          // maximum allowed power level for receive

    u8_t MaxChannelTx;        // maximum allowed channel index for transmit
    u8_t MaxChannelRx;        // maximum allowed channel index for receive

   u16_t Max_Payload;        // maximum allowed payload size of each raw packet
    u8_t Max_Header;         // max size of all packet headers added for this radio
    u8_t Max_Footer;         // max size of all packet footers added for this radio

    // architecture dependend radio transceiver configuration
    // (structure allocated and managed by low-level driver)
    ttc_radio_driver_e      Driver;       // type of low-level driver to use for this radio
    ttc_radio_driver_t*     DriverCfg;    // configuration data of low-level driver

    // function being called to parse incoming packet
    ttc_heap_block_t* (*RxFunction_UCB)(struct ttc_radio_generic_s*, ttc_heap_block_t*, u8_t RSSI, u8_t TargetAddress);

    // function being called to observe each outgoing packet
    void (*TxFunction_UCB)(struct ttc_radio_generic_s*, u8_t, const u8_t*);

    //settings required to meet the Duty cycle limitation within the 868MHz SRD Band
    ttc_duty_cycle_t Duty_Cycle;


#ifdef TTC_DEBUG_RADIO
    // statistic fields
    Base_t Max_TimeTxPreparation;  // maximum seen time required to preparate a transmission
    Base_t Max_TimeTxMode;         // maximum seen time required to transmit a packet
    u16_t  Amount_Error_Handler_Calls;    //
    u16_t  Amount_Error_Radio_Resets;     // amount of radio hardware resets due to error conditions
    u16_t  Amount_Error_TX_Timeout;       //
    u16_t  Amount_Error_RxCorruptPacket;  //
    u16_t  Amount_Error_UnexpectedState;  //
    u16_t  Amount_Error_Unknown;          //
    u16_t  Amount_Error_TXFIFO_UNDERFLOW; //
    u16_t  Amount_Error_RXFIFO_OVERFLOW;  //
    u16_t  Amount_Error_Missed_TXStates;  //
    u32_t  Amount_Packets_Send;           //
    u32_t  Amount_Packets_Received;       //
#endif
    BOOL   AMP_Initialized;        // will be set by the amplifier init function
    void (*TaskYield)();           // function used by low-level driver to give away cpu while waiting
    //}
} __attribute__((__packed__)) ttc_radio_generic_t;

//}

#endif // TTC_RADIO_TYPES_H
