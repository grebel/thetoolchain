/** { ttc_input.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for input devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  See corresponding header file for documentation.
 *
 *  Created from template ttc_device.c revision 27 at 20150317 17:32:16 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_input.h".
//
#include "ttc_input.h"
#include "ttc_heap.h"  // dynamic memory + memory pools
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_INPUT_AMOUNT == 0
#warning No INPUT devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of input devices.
 *
 */
//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_input(t_u8 LogicalIndex)

/** append given input area to given list of input areas
 *
 * @param ListStart  first item of single linked list of input area items
 * @param InputArea  memory block returned from ttc_heap_pool_block_get(ttc_input_AreaPool) before
 */
void _ttc_input_list_append( t_ttc_list_item** ListStart, t_ttc_input_area* InputArea );

/** remove given input area from given list of input areas
 *
 * @param ListStart  first item of single linked list of input area items
 * @param InputArea  memory block returned from ttc_heap_pool_block_get(ttc_input_AreaPool) before
 * @return ==TRUE: input area has been removed from list; ==FALSE: input area was not found in list
 */
BOOL _ttc_input_list_remove( t_ttc_list_item** ListStart, t_ttc_input_area* InputArea );

/** checks if given coordinates are within given input area
 *
  * @param X         (t_u16)              X-coordinate
  * @param Y         (t_u16)              Y-coordinate
  * @param InputArea (t_ttc_input_area*)  input area to check
  * @return          (t_u8)               ==0: (X,Y) is outside of input area; !=0: (X,Y) is inside
 */
t_u8 _ttc_input_is_within( t_u16 X, t_u16 Y, t_ttc_input_area* InputArea );

/** finds first input area in given list that contains given (X,Y) coordinate
 *
  * @param X         (t_u16)              X-coordinate
  * @param Y         (t_u16)              Y-coordinate
  * @param ListHead (t_ttc_list_item*)    Start item in single linked list of input area items (allowed to be NULL)
  * @return         (t_ttc_input_area*) =!NULL: first input area that contains (X,Y)
 */
t_ttc_input_area* _ttc_input_find_affected_area( t_u16 X, t_u16 Y, t_ttc_list_item* ListHead );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

// for each initialized device, a pointer to its generic and touch definitions is stored
A_define( t_ttc_input_config*, ttc_input_configs, TTC_INPUT_AMOUNT );

// collection of equal sized, reusable memory blocks used for input areas
// This pool is shared among all input devices.
t_ttc_heap_pool* ttc_input_AreaPool;

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_ttc_input_area*     ttc_input_area_new( t_u8 LogicalIndex, t_s16 Left, t_s16 Top, t_s16 Right, t_s16 Bottom, void ( *Handler )( struct s_ttc_input_config* ConfigInput, struct s_ttc_input_area* AffectedArea, t_u16 RelativeX, t_u16 RelativeY ), void* Argument ) {
    t_ttc_input_config* Config = ttc_input_get_configuration( LogicalIndex );
    // TODO_FK hier abfragen in welcher richtung das Display steht -  Positionen müssen rotiert werden...
    // via ifdef

    t_ttc_input_area* NewInputArea = ( t_ttc_input_area* ) ttc_heap_pool_block_get( ttc_input_AreaPool );

    if ( Left < 0 ) { Left = 0; }
    if ( Top  < 0 ) { Top  = 0; }
    Assert( Bottom > Top,  ec_basic_ERROR );
    Assert( Right  > Left, ec_basic_ERROR );

    NewInputArea->Left     = Left;
    NewInputArea->Top      = Top;
    NewInputArea->Right    = Right;
    NewInputArea->Bottom   = Bottom;
    NewInputArea->Handler  = ( void ( * )( void*, struct s_ttc_input_area*, t_u16, t_u16 ) ) Handler;
    NewInputArea->Argument = Argument;

    _ttc_input_list_append( &Config->InputAreas_Active, NewInputArea );

    return NewInputArea;
}
BOOL                  ttc_input_area_del( t_u8 LogicalIndex, t_ttc_input_area* InputArea ) {
    t_ttc_input_config* Config = ttc_input_get_configuration( LogicalIndex );

    if ( !_ttc_input_list_remove( &Config->InputAreas_InActive, InputArea ) )  // try to remove from list of inactive input areas
    { return _ttc_input_list_remove( &Config->InputAreas_Active, InputArea ); } // try to remove from list of active input areas

    return TRUE; // successfully removed input area from list of inactive areas
}
BOOL                  ttc_input_area_disable( t_u8 LogicalIndex, t_ttc_input_area* InputArea ) {
    t_ttc_input_config* Config = ttc_input_get_configuration( LogicalIndex );

    if ( _ttc_input_list_remove( &Config->InputAreas_Active, InputArea ) ) { // remove from list of active areas
        _ttc_input_list_append( &Config->InputAreas_InActive, InputArea ); // append to list of inactive areas
        return TRUE; // successfully deactivated input area
    }

    return FALSE; // given input area was not found in list of active areas
}
BOOL                  ttc_input_area_enable( t_u8 LogicalIndex, t_ttc_input_area* InputArea ) {
    t_ttc_input_config* Config = ttc_input_get_configuration( LogicalIndex );

    if ( _ttc_input_list_remove( &Config->InputAreas_InActive, InputArea ) ) { // remove from list of inactive areas
        _ttc_input_list_append( &Config->InputAreas_Active, InputArea );     // append to list of active areas
        return TRUE; // successfully activated input area
    }

    return FALSE; // given input area was not found in list of inactive areas
}
void                  ttc_input_clear( t_u8 LogicalIndex ) {
    t_ttc_input_config* Config = ttc_input_get_configuration( LogicalIndex );

    while ( Config->InputAreas_Active ) { // release all active input-areas
        // obtain input area being stored inside list item
        t_ttc_input_area* InputArea = ( t_ttc_input_area* ) ttc_heap_pool_from_list_item( Config->InputAreas_Active );

        // remove first item from list (constant time operation for this list)
        _ttc_input_list_remove( &Config->InputAreas_Active, InputArea );

        // release memory block to its pool
        ttc_heap_pool_block_free( ( t_ttc_heap_block_from_pool* ) InputArea );
    }
    while ( Config->InputAreas_InActive ) { // release all inactive input-areas
        // obtain input area being stored inside list item
        t_ttc_input_area* InputArea = ( t_ttc_input_area* ) ttc_heap_pool_from_list_item( Config->InputAreas_InActive );

        // remove first item from list (constant time operation for this list)
        _ttc_input_list_remove( &Config->InputAreas_InActive, InputArea );

        // release memory block to its pool
        ttc_heap_pool_block_free( ( t_ttc_heap_block_from_pool* ) InputArea );
    }
}
e_ttc_input_event     ttc_input_check( t_u8 LogicalIndex ) {
    t_ttc_input_config* Config = ttc_input_get_configuration( LogicalIndex );

    t_ttc_list_item* CurrentAreaItem = Config->InputAreas_Active;
    if ( CurrentAreaItem ) { // at least one active input area: check touchpad for updates
        e_ttc_input_event InputEvent = _driver_input_check( Config );
        if ( InputEvent ) {
            do { // process all affected areas

                #ifdef TTC_TOUCH_ROTATION_CLOCKWISE
                t_u16 EventX = Config->LowLevelConfig->TouchPad->Rotated_X;
                t_u16 EventY = Config->LowLevelConfig->TouchPad->Rotated_Y;
                #else
                t_u16 EventX = Config->LowLevelConfig->TouchPad->Position_X;
                t_u16 EventY = Config->LowLevelConfig->TouchPad->Position_Y;
                #endif
                t_ttc_input_area* AffectedArea = _ttc_input_find_affected_area( EventX,
                                                                                EventY,
                                                                                CurrentAreaItem
                                                                              );
                if ( AffectedArea ) { // found an affected area: call handler
                    if ( AffectedArea->Handler )
                        AffectedArea->Handler( Config,
                                               AffectedArea,
                                               EventX - AffectedArea->Left,
                                               EventY - AffectedArea->Top
                                             );

                    // continue processing at next input area item
                    CurrentAreaItem = ttc_heap_pool_to_list_item( ( t_ttc_heap_block_from_pool* ) AffectedArea )->Next;
                }
                else
                { CurrentAreaItem = NULL; } // no input areas affected

            }
            while ( CurrentAreaItem );

            return InputEvent;
        }
    }
    return tie_None; // nothing happened
}
void                  ttc_input_deinit( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration
    t_ttc_input_config* Config = ttc_input_get_configuration( LogicalIndex );

    if ( Config->Flags.Bits.Initialized ) {
        ttc_input_reset( LogicalIndex );
        e_ttc_input_errorcode Result = _driver_input_deinit( Config );
        if ( Result == ec_input_OK )
        { Config->Flags.Bits.Initialized = 0; }
    }
    ttc_task_critical_end(); // other tasks now may access this driver
}
t_u8                  ttc_input_get_max_index() {
    return TTC_INPUT_AMOUNT;
}
t_ttc_input_config*   ttc_input_get_configuration( t_u8 LogicalIndex ) {
    Assert_INPUT( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_input_config* Config = A( ttc_input_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        Config = A( ttc_input_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_input_config ) );
        Config->Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()

        ttc_input_load_defaults( LogicalIndex );
    }

    return Config;
}
t_ttc_input_config*   ttc_input_get_features( t_u8 LogicalIndex ) {
    t_ttc_input_config* Config = ttc_input_get_configuration( LogicalIndex );
    return _driver_input_get_features( Config );
}
e_ttc_input_errorcode ttc_input_init( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration
    t_ttc_input_config* Config = ttc_input_get_configuration( LogicalIndex );

    e_ttc_input_errorcode Result = _driver_input_init( Config );


    if ( Result == ec_input_OK )
    { Config->Flags.Bits.Initialized = 1; }

    ttc_task_critical_end(); // other tasks now may access this driver

    return Result;
}
e_ttc_input_errorcode ttc_input_load_defaults( t_u8 LogicalIndex ) {
    t_ttc_input_config* Config = ttc_input_get_configuration( LogicalIndex );

    if ( Config->Flags.Bits.Initialized )
    { ttc_input_deinit( LogicalIndex ); }

    ttc_memory_set( Config, 0, sizeof( t_ttc_input_config ) );

    // load generic default values
    switch ( LogicalIndex ) { // load type of low-level driver for current architecture
            #ifdef TTC_INPUT1
        case  1: Config->Architecture = TTC_INPUT1; break;
            #endif
            #ifdef TTC_INPUT2
        case  2: Config->Architecture = TTC_INPUT2; break;
            #endif
            #ifdef TTC_INPUT3
        case  3: Config->Architecture = TTC_INPUT3; break;
            #endif
            #ifdef TTC_INPUT4
        case  4: Config->Architecture = TTC_INPUT4; break;
            #endif
            #ifdef TTC_INPUT5
        case  5: Config->Architecture = TTC_INPUT5; break;
            #endif
            #ifdef TTC_INPUT6
        case  6: Config->Architecture = TTC_INPUT6; break;
            #endif
            #ifdef TTC_INPUT7
        case  7: Config->Architecture = TTC_INPUT7; break;
            #endif
            #ifdef TTC_INPUT8
        case  8: Config->Architecture = TTC_INPUT8; break;
            #endif
            #ifdef TTC_INPUT9
        case  9: Config->Architecture = TTC_INPUT9; break;
            #endif
            #ifdef TTC_INPUT10
        case 10: Config->Architecture = TTC_INPUT10; break;
            #endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }
    Assert_INPUT( ( Config->Architecture > ta_input_None ) && ( Config->Architecture < ta_input_unknown ), ttc_assert_origin_auto ); // architecture not set properly! Check makefile for TTC_INPUT<n> and compare with e_ttc_input_architecture

    Config->LogicalIndex = LogicalIndex;

    //Insert additional generic default values here ...

    return _driver_input_load_defaults( Config );
}
void                  ttc_input_prepare() {
    // add your startup code here (Singletasking!)

    // we reserve a fixed amount of input areas all input devices
    ttc_input_AreaPool = ttc_heap_pool_create( sizeof( t_ttc_input_area ), TTC_INPUT_MAX_AREAS );
    _driver_input_prepare();
}
void                  ttc_input_reset( t_u8 LogicalIndex ) {
    t_ttc_input_config* Config = ttc_input_get_configuration( LogicalIndex );

    _driver_input_reset( Config );
    ttc_input_clear( LogicalIndex );
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_input(t_u8 LogicalIndex) {  }

t_ttc_input_area* _ttc_input_find_affected_area( t_u16 X, t_u16 Y, t_ttc_list_item* ListHead ) {
    while ( ListHead ) {
        t_ttc_input_area* CurrentArea = ( t_ttc_input_area* ) ttc_heap_pool_from_list_item( ListHead );
        if ( CurrentArea && _ttc_input_is_within( X, Y, CurrentArea ) )
        { return CurrentArea; }

        ListHead = ListHead->Next;
    }

    return NULL; // no area affected
}
t_u8              _ttc_input_is_within( t_u16 X, t_u16 Y, t_ttc_input_area* InputArea ) {
    Assert_INPUT_EXTRA( ttc_memory_is_writable( InputArea ), ttc_assert_origin_auto ); // always check incoming pointer arguments

    return ( ( X >= InputArea->Left ) && ( X <= InputArea->Right ) &&
             ( Y >= InputArea->Top )  && ( Y <= InputArea->Bottom )
           );
}
void              _ttc_input_list_append( t_ttc_list_item** ListStart, t_ttc_input_area* InputArea ) {
    Assert_INPUT_EXTRA( ttc_memory_is_writable( ListStart ),    ttc_assert_origin_auto );
    Assert_INPUT_EXTRA( ttc_memory_is_writable( InputArea ), ttc_assert_origin_auto );

    if ( *ListStart ) { // list exists: InputArea.next = first list item

        // every pool memory block is also a list item
        ttc_heap_pool_to_list_item( ( t_ttc_heap_block_from_pool* ) InputArea )->Next = *ListStart;
    }
    *ListStart = ttc_heap_pool_to_list_item( ( t_ttc_heap_block_from_pool* ) InputArea ); // InputArea is now head of list
}
BOOL              _ttc_input_list_remove( t_ttc_list_item** ListStart, t_ttc_input_area* InputArea ) {
    Assert_INPUT_EXTRA( ttc_memory_is_writable( ListStart ), ttc_assert_origin_auto );
    Assert_INPUT_EXTRA( ttc_memory_is_writable( InputArea ), ttc_assert_origin_auto );

    // every pool memory block is also a list item
    t_ttc_list_item* SearchItem   = ttc_heap_pool_to_list_item( ( t_ttc_heap_block_from_pool* ) InputArea );
    t_ttc_list_item* CurrentItem  = *ListStart;
    t_ttc_list_item* PreviousItem = NULL;

    while ( CurrentItem && ( CurrentItem != SearchItem ) ) { // linear search (sufficient for small amount of input areas!)
        PreviousItem = CurrentItem;
        CurrentItem  = CurrentItem->Next;
    }

    if ( !CurrentItem )
    { return FALSE; } // could not find input area in given list

    if ( PreviousItem ) { // search item has a predecessor: redirect link of predecessor to successor of InputArea
        PreviousItem->Next = CurrentItem->Next;
    }
    else {              // search item has no predecessor: make its successor new head of list
        *ListStart = CurrentItem->Next;
    }

    return TRUE;
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
