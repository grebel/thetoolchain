/** { ttc_real_time_clock.h *******************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level interface and documentation for REAL_TIME_CLOCK devices.
 *  The functions defined in this file provide a hardware independent interface 
 *  for your application.
 *                                          
 *  The basic usage scenario for devices:
 *  1) check:       Assert_REAL_TIME_CLOCK(tc_real_time_clock_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_real_time_clock_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_real_time_clock_init(LogicalIndex);
 *  4) use:         ttc_real_time_clock_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level real_time_clock and application.
 *
 *  Authors: Gregor Rebel
 *  
}*/

#ifndef TTC_REAL_TIME_CLOCK_H
#define TTC_REAL_TIME_CLOCK_H

//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "interfaces/ttc_real_time_clock_interface.h"

//} Includes
//{ Check for missing low-level implementations **************************

// Note: functions declared below must be redefined to implementations provided by low-level Driver!

/** Prepares real_time_clock Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_real_time_clock_prepare();

/** loads configuration of indexed REAL_TIME_CLOCK interface with default values
 * @param Config        pointer to struct t_ttc_real_time_clock_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_real_time_clock_errorcode _driver_real_time_clock_load_defaults(t_ttc_real_time_clock_config* Config);

/** shutdown single REAL_TIME_CLOCK device
 * @param Config        pointer to struct t_ttc_real_time_clock_config (must have valid value for LogicalIndex)
 * @return              == 0: REAL_TIME_CLOCK has been shutdown successfully; != 0: error-code
 */
e_ttc_real_time_clock_errorcode _driver_real_time_clock_deinit(t_ttc_real_time_clock_config* Config);

/** initializes single REAL_TIME_CLOCK
 * @param Config        pointer to struct t_ttc_real_time_clock_config (must have valid value for LogicalIndex)
 * @return              == 0: REAL_TIME_CLOCK has been initialized successfully; != 0: error-code
 */
e_ttc_real_time_clock_errorcode _driver_real_time_clock_init(t_ttc_real_time_clock_config* Config);

//}
//{ Defines/ TypeDefs ****************************************************

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level real_time_clock only go here...

//} Structures/ Enums
//{ Function prototypes **************************************************

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_real_time_clock_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_REAL_TIME_CLOCK_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_real_time_clock_config* ttc_real_time_clock_get_configuration(t_u8 LogicalIndex);

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_REAL_TIME_CLOCK_get_max_LogicalIndex())
 */
void ttc_real_time_clock_init(t_u8 LogicalIndex);

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_REAL_TIME_CLOCK_get_max_LogicalIndex())
 */

void ttc_real_time_clock_load_defaults(t_u8 LogicalIndex);

void ttc_real_time_clock_deinit(t_u8 LogicalIndex);

/** maps from logical to physical device index
 *
 * High-level real_time_clocks (ttc_real_time_clock_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_REAL_TIME_CLOCKn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be 
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any 
 * order.
 *
 * @param LogicalIndex  logical index of real_time_clock device (1..ttc_real_time_clock_get_max_index() )
 * @return              physical index of real_time_clock device (0 = first physical real_time_clock device, ...)
 */
t_u8 ttc_real_time_clock_logical_2_physical_index(t_u8 LogicalIndex);

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_REAL_TIME_CLOCK_get_max_LogicalIndex())
 */
void ttc_real_time_clock_reset(t_u8 LogicalIndex);

void ttc_real_time_clock_get_time(t_u8 LogicalIndex,ttc_real_time_t_clock_time* Time);
// additional prototypes go here...

//} Function prototypes
//{ private Function prototypes ******************************************

/** 
 *
 * Note: This function is marked private and should be called from outside!
 *
 * @param 
 * @return
 */
void _ttc_real_time_clock_();


//}PrivateFunctions
//{ Macros ***************************************************************

//}Macros

#endif //TTC_REAL_TIME_CLOCK_H
