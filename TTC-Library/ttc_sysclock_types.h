/** { ttc_sysclock_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for SYSCLOCK device.
 *  Structures, Enums and Defines being required by both, high- and low-level sysclock.
 *
 *  Created from template ttc_device_types.h revision 22 at 20140218 17:45:54 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_SYSCLOCK_TYPES_H
#define TTC_SYSCLOCK_TYPES_H

//{ Includes *************************************************************

#include "ttc_basic_types.h"
#include "compile_options.h" // only required to help IDE to know constant definitions

#ifdef EXTENSION_sysclock_stm32f1xx
    #include "sysclock/sysclock_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_sysclock_stm32w1xx
    #include "sysclock/sysclock_stm32w1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_sysclock_stm32l1xx
    #include "sysclock/sysclock_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_sysclock_stm32l0xx
    #include "sysclock/sysclock_stm32l0xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

#include "ttc_list_item_types.h"

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************


/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_PACKET 0        # disable default asserts for packet driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_PACKET_EXTRA 1  # enable extra asserts for packet driver
 *
 */
#ifndef TTC_ASSERT_SYSCLOCK    // any previous definition set (Makefile)?
    #define TTC_ASSERT_SYSCLOCK 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_SYSCLOCK == 1)  // use Assert()s in SYSCLOCK code (somewhat slower but alot easier to debug)
    #define Assert_SYSCLOCK(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_SYSCLOCK_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_SYSCLOCK_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in SYSCLOCK code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_SYSCLOCK(Condition, Origin)
    #define Assert_SYSCLOCK_Writable(Address, Origin)
    #define Assert_SYSCLOCK_Readable(Address, Origin)
#endif


#ifndef TTC_ASSERT_SYSCLOCK_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_SYSCLOCK_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_SYSCLOCK_EXTRA == 1)  // use Assert()s in SYSCLOCK code (somewhat slower but alot easier to debug)
    #define Assert_SYSCLOCK_EXTRA(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_SYSCLOCK_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_SYSCLOCK_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in SYSCLOCK code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_SYSCLOCK_EXTRA(Condition, Origin)
    #define Assert_SYSCLOCK_EXTRA_Writable(Address, Origin)
    #define Assert_SYSCLOCK_EXTRA_Readable(Address, Origin)
#endif

/** Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_sysclock_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_sysclock_architecture
    #warning Missing low-level definition for t_ttc_sysclock_architecture (using default)
    #define t_ttc_sysclock_architecture void*
#endif

/** Describes the hardware clocking configuration being implemented on current board.
 *
 * Value must be one from ttc_sysclock_setup_e below.
 */
#ifdef TTC_SYSCLOCK_SETUP
    #warning Deprecated definition for TTC_SYSCLOCK_SETUP define TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL and TTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL instead!
    #undef TTC_SYSCLOCK_SETUP
#endif

/** Default system clock profile to use at system start
 *
 * Define this in your board makefile to be one from e_ttc_sysclock_profile.
 */
#ifndef TTC_SYSCLOCK_PROFILE_START
    #define TTC_SYSCLOCK_PROFILE_START E_ttc_sysclock_profile_MaxSpeed
#endif

/** If defined, states frequency of externally connected high speed crystal or oscillator
 *
 * >0: value of external high-speed oscillator/ crystal (e.g. 8000000)
 */
#ifndef TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL
    #define TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL 0
#endif

/** If defined, states frequency of externally connected low speed crystal or oscillator
 *
 * >0: value of external low-speed oscillator/ crystal  (e.g. 32768)
 */
#ifndef TTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL
    #define TTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL 0
#endif

/** Check for definitions to be made by low-level driver
 *
 * The low-level driver has to define these constants for current architecture:
 * TTC_SYSCLOCK_FREQUENCY_MIN  the lowest stable system clock frequency
 * TTC_SYSCLOCK_FREQUENCY_MAX       the highest stable system clock frequency
 */
#ifndef TTC_SYSCLOCK_FREQUENCY_MIN
    #  error Missing definition for TTC_SYSCLOCK_FREQUENCY_MIN - Add this definition to your sysclock_XXX_types.h! (compare with sysclock_stm32f1xx_types.h)
#endif
#ifndef TTC_SYSCLOCK_FREQUENCY_MAX
    #  error Missing definition for TTC_SYSCLOCK_FREQUENCY_MAX - Add this definition to your sysclock_XXX_types.h! (compare with sysclock_stm32f1xx_types.h)
#endif

/** Accuracy for dynamic delay scaling over current system clock.
 *
 * If not defined outside, the accuracy is choosen according to size of t_base in current architecture.
 */
#ifndef TTC_SYSCLOCK_SCALE_UNIT
    #ifndef TTC_BASIC_TYPES_DATAWIDTH
        //#    error Missing definition of TTC_BASIC_TYPES_DATAWIDTH for current architecure. See basic_cm3_types.h for an example!
    #endif

    #if TTC_BASIC_TYPES_DATAWIDTH == 32
        #define TTC_SYSCLOCK_SCALE_UNIT 10000  // t_base is large egnough for this accuracy
    #endif
    #if TTC_BASIC_TYPES_DATAWIDTH == 16
        #define TTC_SYSCLOCK_SCALE_UNIT 100    // t_base is large egnough for this accuracy
    #endif
    #if TTC_BASIC_TYPES_DATAWIDTH == 8
        #define TTC_SYSCLOCK_SCALE_UNIT 1      // t_base is large egnough for this accuracy
    #endif

    #ifndef TTC_SYSCLOCK_SCALE_UNIT
        //#    error Correct setting could not be determined for TTC_SYSCLOCK_SCALE_UNIT. Check your architecture driver!
    #endif
#endif

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef struct { // list item of list of all registered update functions
    t_ttc_list_item ListItem; // required as first element: make this usable by ttc_list
    void ( *UpdateFunction )(); // Function to call
} t_sysclock_update_item;

typedef enum {    // e_ttc_sysclock_errorcode     return codes of SYSCLOCK devices
    E_ttc_sysclock_errorcode_OK = 0,

    // other warnings go here..

    E_ttc_sysclock_errorcode_ERROR,                 // general failure
    E_ttc_sysclock_errorcode_NULL,                  // NULL pointer not accepted
    E_ttc_sysclock_errorcode_DeviceNotFound,        // corresponding device could not be found
    E_ttc_sysclock_errorcode_DeviceNotInitialized,  // calling a ttc_spi_*() function for an interface that has not beet initialized before.
    E_ttc_sysclock_errorcode_InvalidImplementation, // internal self-test failed
    E_ttc_sysclock_errorcode_InvalidArgument,       // invalid value given as argument to function
    E_ttc_sysclock_errorcode_InvalidConfiguration,  // sanity check of device configuration failed
    E_ttc_sysclock_errorcode_OscillatorError,       // cannot activate/ deactivate an oscillator
    E_ttc_sysclock_errorcode_CannotSwitchClock,     // cannot switch a clock multiplexer

    // other failures go here..

    E_ttc_sysclock_errorcode_unknown                // no valid errorcodes past this entry
} e_ttc_sysclock_errorcode;
typedef enum {    // e_ttc_sysclock_architecture  types of architectures supported by SYSCLOCK driver
    E_ttc_sysclock_architecture_None,           // no architecture selected

    E_ttc_sysclock_architecture_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
    E_ttc_sysclock_architecture_stm32w1xx, // automatically added by ./create_DeviceDriver.pl
    E_ttc_sysclock_architecture_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
    E_ttc_sysclock_architecture_stm32l0xx, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    E_ttc_sysclock_architecture_unknown        // architecture not supported
} e_ttc_sysclock_architecture;
typedef enum {   // e_ttc_sysclock_profile        system clock profiles
    E_ttc_sysclock_profile_None,
    E_ttc_sysclock_profile_MaxSpeed,          // maximum stable clock frequency
    E_ttc_sysclock_profile_MinSpeed,          // minimum stable clock frequency with lowest possible energy consumption
    E_ttc_sysclock_profile_Efficient,         // clock frequency providing highest computing performance per joule

    // insert new profiles above
    E_ttc_sysclock_profile_unknown

} e_ttc_sysclock_profile;
typedef enum {   // e_ttc_sysclock_oscillator     list of oscillators from all architectures
    E_ttc_sysclock_oscillator_None,
    E_ttc_sysclock_oscillator_LowSpeedInternal,
    E_ttc_sysclock_oscillator_LowSpeedExternal,
    E_ttc_sysclock_oscillator_HighSpeedInternal,
    E_ttc_sysclock_oscillator_HighSpeedExternal,
    E_ttc_sysclock_oscillator_MultiSpeedInternal,
    E_ttc_sysclock_oscillator_PLL1,
    E_ttc_sysclock_oscillator_PLL2,
    E_ttc_sysclock_oscillator_PLL3,

    // add more here..

    E_ttc_sysclock_oscillator_unknown
} e_ttc_sysclock_oscillator;

typedef struct s_ttc_sysclock_config { //         architecture independent configuration data

    // Note: Write-access to this structure is not allowed outside ttc_sysclock and its low-level drivers!

    t_base SystemClockFrequency;           // current system clock frequency (Hz)
    t_base SystemClockFrequencyMax;        // maximum supported system clock frequency

    t_base DelayScale;                     // time scale for current frequency according to maximum frequency
    // If uC runs at lower frequency, manual delays can be adjusted by this formula:
    // AdjustedDelay = OriginalDelay * DelayScale / TTC_SYSCLOCK_SCALE_UNIT;

    // low-level configuration (structure defined by low-level driver)
    t_ttc_sysclock_architecture LowLevelConfig;

    union  { // generic configuration bits common for all low-level Drivers
        t_u8 All;
        struct {
            unsigned Initialized        : 1;  // ==1: device has been initialized successfully
            unsigned ConsiderUSB        : 1;  // ==1: consider USB interface operation (may disable certain system clock frequencies)

            // ToDo: additional high-level flags go here..
        } Bits;
    } Flags;

    e_ttc_sysclock_architecture Architecture;         // type of architecture used for current sysclock device
    e_ttc_sysclock_profile      Profile;              // current clocking profile
} __attribute__( ( __packed__ ) ) t_ttc_sysclock_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_SYSCLOCK_TYPES_H
