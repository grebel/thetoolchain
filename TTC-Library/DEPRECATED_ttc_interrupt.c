/*{ ttc_interrupt::.c ************************************************

                      The ToolChain
                      
   Device independent support for
   Universal Synchronous Asynchronous Receiver and Transmitters (INTERRUPTs)
   
   written by Gregor Rebel 2012
   

}*/

#include "DEPRECATED_ttc_interrupt.h"

//{ Global variables *****************************************************

// stores pointers to structs of interrupt service routines for each functional unit
#ifdef EXTENSION_500_ttc_usart
ttc_array_dynamic(ttc_usart_isrs_t*, ttc_usart_isrs);
#endif
#ifdef EXTENSION_500_ttc_gpio
ttc_array_dynamic(ttc_gpio_isrs_t*,  ttc_gpio_isrs);
#endif
#ifdef EXTENSION_500_ttc_timer
ttc_array_dynamic(ttc_timer_isrs_t*,  ttc_timer_isrs);
#endif

//}Global variables
//{ Function definitions *************************************************

void ttc_interrupt_prepare() {

    // Note: We do not deallocate memory. Make sure, that this function is called after a reset of the memory management!

#ifdef EXTENSION_500_ttc_usart
    ttc_array_dynamic_reset(ttc_usart_isrs);
#endif
#ifdef EXTENSION_500_ttc_gpio
    ttc_array_dynamic_reset(ttc_gpio_isrs);
#endif
#ifdef EXTENSION_500_ttc_timer
    ttc_array_dynamic_reset(ttc_timer_isrs_t);
#endif
}
ttc_interrupt_errorcode_e ttc_interrupt_init(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, void (*ISR)(physical_index_t, void*), void* Argument, void (**OldISR)(physical_index_t, void*), void** OldArgument) {

    // Important: enums must checked in order from high to low!

    // Highest enum
#ifdef _driver_interrupt_init_usart
    if (Type > tit_First_USART)
        return ttc_interrupt_init_usart(Type, PhysicalIndex, ISR, Argument, OldISR, OldArgument);
#endif
#ifdef _driver_interrupt_init_spi
    if (Type > tit_First_SPI)
        return tine_NotImplemented;
#endif
#ifdef _driver_interrupt_init_radio
    if (Type > tit_First_RADIO)
        return tine_NotImplemented;
#endif
#ifdef _driver_interrupt_init_timer
    if (Type > tit_First_TIMER)
        return ttc_interrupt_init_timer(Type, PhysicalIndex, ISR, Argument, OldISR, OldArgument);
#endif
#ifdef _driver_interrupt_init_i2c
    if (Type > tit_First_I2C)
        return tine_NotImplemented;
#endif
#ifdef _driver_interrupt_init_gpio
    if (Type > tit_First_GPIO)
        return ttc_interrupt_init_gpio(Type, PhysicalIndex, ISR, Argument, OldISR, OldArgument);
#endif
    // Lowest enum

    Assert_Interrupt(0, ec_InvalidArgument); // unknown Type given!
    return tine_NotImplemented;    // just to make compiler happy
}
ttc_interrupt_errorcode_e ttc_interrupt_init_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, void (*ISR)(physical_index_t, void*), void* Argument, void (**OldISR)(physical_index_t, void*), void** OldArgument) {
#ifdef EXTENSION_500_ttc_usart
    if ( ttc_array_is_null(ttc_usart_isrs) ) { // first init of an USART-irq: create list of pointers
        ttc_array_allocate(ttc_usart_isrs, TTC_USART_MAX_AMOUNT);
    }
    if (A(ttc_usart_isrs, PhysicalIndex) == NULL) { // first init of irq for USART of this index: create emptx pointer table
        A(ttc_usart_isrs, PhysicalIndex) = ttc_memory_alloc_zeroed( sizeof(ttc_usart_isrs_t) );
    }


    void (**Entry_ISR)(physical_index_t, void*) = NULL;
    ttc_usart_config_t** Entry_Argument = NULL;
    ttc_usart_isrs_t* Entry = A(ttc_usart_isrs, PhysicalIndex);
    switch (Type) { // find memory location where to store function pointer + argument

    case tit_USART_Cts:
        Entry_ISR = (void(**)(physical_index_t, void*)) &(Entry->isr_ClearToSend);
        Entry_Argument = &(Entry->Argument_ClearToSend);
        break;
    case tit_USART_Idle:
        Entry_ISR = (void(**)(physical_index_t, void*)) &(Entry->isr_IdleLine);
        Entry_Argument = &(Entry->Argument_IdleLine);
        break;
    case tit_USART_TxComplete:
        Entry_ISR = (void(**)(physical_index_t, void*)) &(Entry->isr_TransmissionComplete);
        Entry_Argument = &(Entry->Argument_TransmissionComplete);
        break;
    case tit_USART_TransmitDataEmpty:
        Entry_ISR = (void(**)(physical_index_t, void*)) &(Entry->isr_TransmitDataEmpty);
        Entry_Argument = &(Entry->Argument_TransmitDataEmpty);
        break;
    case tit_USART_RxNE:
        Entry_ISR = (void(**)(physical_index_t, void*)) &(Entry->isr_ReceiveDataNotEmpty);
        Entry_Argument = &(Entry->Argument_ReceiveDataNotEmpty);
        break;
    case tit_USART_LinBreak:
        Entry_ISR = (void(**)(physical_index_t, void*)) &(Entry->isr_LinBreakDetected);
        Entry_Argument = &(Entry->Argument_LinBreakDetected);
        break;
    case tit_USART_Error:
        Entry_ISR = (void(**)(physical_index_t, void*)) &(Entry->isr_Error);
        Entry_Argument = &(Entry->Argument_Error);
        break;
    default: break;
    }

    if (OldISR != NULL)        // save old value of function ptr
        *OldISR = *Entry_ISR;
    if (OldArgument != NULL)   // save old argument
        *OldArgument = (void*) *Entry_Argument;

    *Entry_ISR = ISR;  // establish new isr
    Assert_Interrupt(Entry_Argument, ec_NULL);
    *Entry_Argument = (ttc_usart_config_t*) Argument;

    return _driver_interrupt_init_usart(Type, PhysicalIndex);
#else
    return 0;
#endif
}
ttc_interrupt_errorcode_e ttc_interrupt_init_gpio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, void (*ISR)(physical_index_t, void*), void* Argument, void (**OldISR)(physical_index_t, void*), void** OldArgument) {
#ifdef EXTENSION_500_ttc_gpio
    GPIO_TypeDef* GPIOx;
    u8_t Pin;
    ttc_gpio_from_index(PhysicalIndex, (ttc_gpio_bank_t*) &GPIOx, &Pin);
    Assert_Interrupt(GPIOx,    ec_InvalidArgument);
    Assert_Interrupt(Pin < TTC_GPIO_MAX_PINS, ec_InvalidArgument);

    if ( ttc_array_is_null(ttc_gpio_isrs) ) { // first init of an gpio-irq: create list of pointers
        ttc_array_allocate(ttc_gpio_isrs, _driver_AMOUNT_EXTERNAL_INTERRUPTS); // CortexM3 provides 16 lines for external gpio-interrupts
    }
    if (A(ttc_gpio_isrs, Pin) == NULL) {      // first init of irq for gpio of this index: create emptx pointer table
        A(ttc_gpio_isrs, Pin) = ttc_memory_alloc_zeroed( sizeof(ttc_gpio_isrs_t) );
    }

    ttc_gpio_isrs_t* GPIO_ISR_Entry = A(ttc_gpio_isrs, Pin);

    // save previous entries
    if (OldISR)      *OldISR      = GPIO_ISR_Entry->isr;
    if (OldArgument) *OldArgument = GPIO_ISR_Entry->Argument;

    // load new entries
    GPIO_ISR_Entry->isr           = ISR;
    GPIO_ISR_Entry->Argument      = Argument;
    GPIO_ISR_Entry->PhysicalIndex = PhysicalIndex;

    return _driver_interrupt_init_gpio(Type, PhysicalIndex);
#else
    return 0;
#endif
}
ttc_interrupt_errorcode_e ttc_interrupt_init_timer(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, void (*ISR)(physical_index_t, void*), void* Argument, void (**OldISR)(physical_index_t, void*), void** OldArgument) {
#ifdef EXTENSION_500_ttc_timer

    if ( ttc_array_is_null(ttc_timer_isrs) ) { // first init of a timer-irq: create list of pointers
        ttc_array_allocate(ttc_timer_isrs, TTC_INTERRUPT_TIMER_AMOUNT); // CortexM3 provides 16 lines for external gpio-interrupts
    }
    if (A(ttc_timer_isrs, PhysicalIndex) == NULL) {      // first init of irq for timer of this index: create emptx pointer table
        A(ttc_timer_isrs, PhysicalIndex) = ttc_memory_alloc_zeroed( sizeof(ttc_timer_isrs_t) );
    }

    ttc_timer_isrs_t* TIMER_ISR_Entry = A(ttc_timer_isrs, PhysicalIndex);

    // save previous entries
    if (OldISR)      *OldISR      = TIMER_ISR_Entry->isr;
    if (OldArgument) *OldArgument = TIMER_ISR_Entry->Argument;

    // load new entries
    TIMER_ISR_Entry->isr           = ISR;
    TIMER_ISR_Entry->Argument      = Argument;
    TIMER_ISR_Entry->PhysicalIndex = PhysicalIndex;

    return _driver_interrupt_init_timer(Type, PhysicalIndex);
#else
    return 0;
#endif
}
void ttc_interrupt_enable(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ) {

    // Important: enums must be checked in order from high to low!

    // Highest enum
#ifdef _driver_interrupt_enable_usart
    if (Type > tit_First_USART) {
        _driver_interrupt_enable_usart(Type, PhysicalIndex, EnableIRQ);
        return;
    }
#endif
#ifdef _driver_interrupt_enable_spi
    if (Type > tit_First_SPI)
        return;
#endif
#ifdef _driver_interrupt_enable_radio
    if (Type > tit_First_RADIO)
        return;
#endif
#ifdef _driver_interrupt_enable_timer
    if (Type > tit_First_TIMER){
        _driver_interrupt_enable_timer(Type, PhysicalIndex, EnableIRQ);
        return;
    }
#endif
#ifdef _driver_interrupt_enable_i2c
    if (Type > tit_First_I2C)
        return;
#endif
#ifdef EXTENSION_500_ttc_gpio
#ifdef _driver_interrupt_enable_gpio
    if (Type > tit_First_GPIO) {
        _driver_interrupt_enable_gpio(Type, PhysicalIndex, EnableIRQ);
        return;
    }
#endif
#endif
    // Lowest enum

    Assert_Interrupt(0, ec_InvalidConfiguration);  // Type not known
}
//} Function definitions
