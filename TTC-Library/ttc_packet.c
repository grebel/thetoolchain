/** { ttc_packet.c *********************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for packet devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Almost all functions in this driver are defined as macros and being implemented
 *  in interfaces/ttc_packet_interface.c or in low-level drivers packet/packet_*.c.
 *
 *  See corresponding ttc_packet.h for documentation of all high-level functions.
 *
 *  Created from template ttc_device.c revision 33 at 20150928 09:30:04 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes ***************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_packet.h".
//
#include "ttc_packet.h"
#include "ttc_string.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of packet devices.
 *
 */


//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ********************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_packet(t_ttc_packet_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

void                  ttc_packet_address_reply( t_ttc_packet* Packet, t_ttc_packet_address* NewSourceID ) {
    Assert_PACKET_Writable( Packet,      ttc_assert_origin_auto );  // always check incoming pointer arguments
    Assert_PACKET_Writable( NewSourceID, ttc_assert_origin_auto );  // always check incoming pointer arguments

    void*  Pointer2SourceID;
    t_u16* Pointer2SourcePanID;
    void*  Pointer2TargetID;
    t_u16* Pointer2TargetPanID;
    e_ttc_packet_address_format SourceAddressType = E_ttc_packet_address_source_get_pointer( Packet, &Pointer2SourceID, &Pointer2SourcePanID );
    e_ttc_packet_address_format TargetAddressType = t_ttc_packet_addressarget_get_pointer( Packet, &Pointer2TargetID, &Pointer2TargetPanID ); ( void ) TargetAddressType;

    Assert_PACKET_EXTRA(
        ( ( SourceAddressType == E_ttc_packet_address_Source16 )  && ( TargetAddressType == E_ttc_packet_address_Target16 ) ) ||
        ( ( SourceAddressType == E_ttc_packet_address_Source64 )  && ( TargetAddressType == E_ttc_packet_address_Target64 ) ),
        ttc_assert_origin_auto
    ); // this function currently only supports packets with same source- and target-address type

    switch ( SourceAddressType ) {
        case E_ttc_packet_address_Source16: { // 16-bit source address
            if ( NewSourceID ) { // new source identifier given: use it for Packet.SourceID
                *( ( t_u16* ) Pointer2TargetID ) = *( ( t_u16* ) Pointer2SourceID );
                *( ( t_u16* ) Pointer2SourceID ) = NewSourceID->Address16;
            }
            else {            // new source identifier NOT given: use Packet.TargetID for Packet.SourceID
                t_u16 Memo = *( ( t_u16* ) Pointer2TargetID );
                *( ( t_u16* ) Pointer2TargetID ) = *( ( t_u16* ) Pointer2SourceID );
                *( ( t_u16* ) Pointer2SourceID ) = Memo;
            }
            break;
        }
        case E_ttc_packet_address_Source64: { // 64-bit source address
            if ( NewSourceID ) { // new source identifier given: use it for Packet.SourceID
                // 64 bit source -> target
#if TARGET_DATA_WIDTH == 32  // implementation for 32-bit wide data bus
                ( ( t_u32* ) Pointer2TargetID )[0] = ( ( t_u32* ) Pointer2SourceID )[0];
                ( ( t_u32* ) Pointer2TargetID )[1] = ( ( t_u32* ) Pointer2SourceID )[1];

                // 64 bit NewSourceID -> source
                ( ( t_u32* ) Pointer2SourceID )[0] = NewSourceID->Address64.Words[0];
                ( ( t_u32* ) Pointer2SourceID )[1] = NewSourceID->Address64.Words[1];
#else
                ttc_memory_copy( Pointer2TargetID, Pointer2SourceID, 8 );
                ttc_memory_copy( Pointer2SourceID, NewSourceID->Address64.Bytes, 8 );
#endif
            }
            else {            // new source identifier NOT given: use Packet.TargetID for Packet.SourceID

                // 64 bit source -> target
#if TARGET_DATA_WIDTH == 32  // implementation for 32-bit wide data bus
                t_u32 Memo[2];
                Memo[0] = ( ( t_u32* ) Pointer2TargetID )[0];
                Memo[1] = ( ( t_u32* ) Pointer2TargetID )[1];

                ( ( t_u32* ) Pointer2TargetID )[0] = ( ( t_u32* ) Pointer2SourceID )[0];
                ( ( t_u32* ) Pointer2TargetID )[1] = ( ( t_u32* ) Pointer2SourceID )[1];

                // 64 bit NewSourceID -> source
                ( ( t_u32* ) Pointer2SourceID )[0] = Memo[0];
                ( ( t_u32* ) Pointer2SourceID )[1] = Memo[1];
#else
                t_u8 Memo[8];
                ttc_memory_copy( Memo, Pointer2SourceID, 8 );
                ttc_memory_copy( Pointer2TargetID, Pointer2SourceID, 8 );
                ttc_memory_copy( Pointer2SourceID, Memo, 8 );
#endif

            }
            break;
        }
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown source address type: maybe packet is invalid
    }
}
void                  ttc_packet_address_reply2( t_ttc_packet* PacketRX, t_ttc_packet* PacketReply, t_ttc_packet_address* NewSourceID ) {
    Assert_PACKET_Writable( PacketRX,    ttc_assert_origin_auto );  // always check incoming pointer arguments
    Assert_PACKET_Writable( PacketReply, ttc_assert_origin_auto );  // always check incoming pointer arguments
    if ( NewSourceID ) {
        Assert_PACKET_Writable( NewSourceID, ttc_assert_origin_auto );  // always check incoming pointer arguments
    }

    // obtain pointers to addresses in received packet
    void*  PacketRX_Pointer2SourceID;
    t_u16* PacketRX_Pointer2SourcePanID;
    void*  PacketRX_Pointer2TargetID;
    t_u16* PacketRX_Pointer2TargetPanID;
    e_ttc_packet_address_format SourceAddressType = E_ttc_packet_address_source_get_pointer( PacketRX, &PacketRX_Pointer2SourceID, &PacketRX_Pointer2SourcePanID );
    e_ttc_packet_address_format TargetAddressType = t_ttc_packet_addressarget_get_pointer( PacketRX, &PacketRX_Pointer2TargetID, &PacketRX_Pointer2TargetPanID );

    Assert_PACKET_EXTRA(
        ( ( SourceAddressType == E_ttc_packet_address_Source16 )  && ( TargetAddressType == E_ttc_packet_address_Target16 ) ) ||
        ( ( SourceAddressType == E_ttc_packet_address_Source64 )  && ( TargetAddressType == E_ttc_packet_address_Target64 ) ),
        ttc_assert_origin_auto
    ); // this function currently only supports packets with same source- and target-address type (ToDo: extend implementation!)
    ( void ) TargetAddressType; // avoid compiler warning (unused variable...)

    // obtain pointers to addresses in reply packet
    void*  PacketReply_Pointer2SourceID;
    t_u16* PacketReply_Pointer2SourcePanID;
    void*  PacketReply_Pointer2TargetID;
    t_u16* PacketReply_Pointer2TargetPanID;
    E_ttc_packet_address_source_get_pointer( PacketReply, &PacketReply_Pointer2SourceID, &PacketReply_Pointer2SourcePanID );
    t_ttc_packet_addressarget_get_pointer( PacketReply, &PacketReply_Pointer2TargetID, &PacketReply_Pointer2TargetPanID );

    // initialize reply packet being same type and protocol as received one
    e_ttc_packet_type     PacketType     = E_ttc_packet_type_get( PacketRX );
    e_ttc_packet_pattern PacketProtocol = s_ttc_packetocket_get( PacketRX );
    ttc_packet_initialize( ( t_ttc_heap_block_from_pool* ) PacketReply, PacketType, PacketProtocol );


    if ( PacketRX_Pointer2TargetPanID ) { // Copy Target and Source PanID (if stored in packet)
        if ( PacketRX_Pointer2SourcePanID )
        { *PacketReply_Pointer2SourcePanID = *PacketRX_Pointer2TargetPanID; }
        *PacketReply_Pointer2TargetPanID = *PacketRX_Pointer2TargetPanID;
    }

    switch ( SourceAddressType ) {
        case E_ttc_packet_address_Source16: { // 16-bit source address
            *( ( t_u16* ) PacketReply_Pointer2TargetID ) = *( ( t_u16* ) PacketRX_Pointer2SourceID );
            if ( NewSourceID ) { // PacketReply.SourceID = NewSourceID
                *( ( t_u16* ) PacketReply_Pointer2SourceID ) = NewSourceID->Address16;
            }
            else {               // PacketReply.SourceID = PacketRX.TargetID
                *( ( t_u16* ) PacketReply_Pointer2SourceID ) = *( ( t_u16* ) PacketRX_Pointer2TargetID ) ;
            }

            break;
        }
        case E_ttc_packet_address_Source64: { // 64-bit source address
#if TARGET_DATA_WIDTH == 32  // implementation for 32-bit wide data bus
            ( ( t_u32* ) PacketReply_Pointer2TargetID )[0] = ( ( t_u32* ) PacketRX_Pointer2SourceID )[0];
            ( ( t_u32* ) PacketReply_Pointer2TargetID )[1] = ( ( t_u32* ) PacketRX_Pointer2SourceID )[1];

            if ( NewSourceID ) {// PacketReply.SourceID = NewSourceID
                ( ( t_u32* ) PacketReply_Pointer2SourceID )[0] = NewSourceID->Address64.Words[0];
                ( ( t_u32* ) PacketReply_Pointer2SourceID )[1] = NewSourceID->Address64.Words[1];
            }
            else {              // PacketReply.SourceID = PacketRX.TargetID
                ( ( t_u32* ) PacketReply_Pointer2SourceID )[0] = ( ( t_u32* ) PacketRX_Pointer2TargetID )[0];
                ( ( t_u32* ) PacketReply_Pointer2SourceID )[1] = ( ( t_u32* ) PacketRX_Pointer2TargetID )[1];
            }
#else
            ttc_memory_copy( PacketReply_Pointer2TargetID, PacketRX_Pointer2SourceID, 8 );
            if ( NewSourceID ) { // PacketReply.SourceID = NewSourceID
                ttc_memory_copy( PacketReply_Pointer2SourceID, NewSourceID->Address64.Bytes, 8 );
            }
            else {               // PacketReply.SourceID = PacketRX.TargetID
                ttc_memory_copy( PacketReply_Pointer2SourceID, PacketRX_Pointer2TargetID, 8 );
            }
#endif
            break;
        }
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown source address type: maybe packet is invalid
    }

    // low-level driver may want to copy extra data
    _driver_packet_address_reply2( PacketRX, PacketReply );
}
t_u16                 ttc_packet_calculate_buffer_size( t_u16 BinarySize ) {

    t_u16 BufferSize = BinarySize
                       + sizeof( t_ttc_packet_meta ); // in memory, each packet stores an extra meta header

    return BufferSize;
}
t_u16                 ttc_packet_debug( t_ttc_packet* Packet, t_u8* Buffer, t_base* BufferSize, t_ttc_packet_debug Debug ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // always check incoming pointer arguments
    Assert_PACKET_Writable( Buffer, ttc_assert_origin_auto );  // always check incoming pointer arguments
    Assert_PACKET_Writable( BufferSize, ttc_assert_origin_auto );  // always check incoming pointer arguments

    //your_code_goes_here
    t_u8* BufferStart = Buffer;
    t_u16* PanID = NULL;
    void* NodeID = NULL;

    if ( Debug.SourceID ) { // compile source ID
        switch ( E_ttc_packet_address_source_get_pointer( Packet, &NodeID, &PanID ) ) {
            case E_ttc_packet_address_Source16: { // packet stores a 16-bit source ID
                Assert_PACKET_Readable( NodeID, ttc_assert_origin_auto );
                t_u16* SourceID16 = ( t_u16* ) NodeID;
                if ( SourceID16 ) {
                    if ( PanID ) {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "0x%04x/0x%04x", *SourceID16, *PanID );
                    }
                    else {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "0x%04x", *SourceID16 );
                    }
                }
                else {
                    if ( PanID ) {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "unknown/0x%04x", *PanID );
                    }
                    else {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "unknown" );
                    }
                }
                break;
            }
            case E_ttc_packet_address_Source64: { // packet stores a 64-bit source ID
                Assert_PACKET_Readable( NodeID, ttc_assert_origin_auto );
                t_u32* SourceID64 = ( t_u32* ) NodeID;
                if ( SourceID64 ) {
                    if ( PanID ) {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "0x%08x%08x/0x%04x", *SourceID64, *( SourceID64 + 1 ), *PanID );
                    }
                    else {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "0x%08x%08x", *SourceID64, *( SourceID64 + 1 ) );
                    }
                }
                else {
                    if ( PanID ) {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "unknown/0x%04x", *PanID );
                    }
                    else {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "unknown" );
                    }
                }
                break;
            }
            default: {
                Buffer += ttc_string_appendf( Buffer, BufferSize, "unknown" );
                break;
            }
        }
    }
    if ( Debug.TargetID ) { // compile destination ID
        Buffer += ttc_string_appendf( Buffer, BufferSize, "->" );

        switch ( t_ttc_packet_addressarget_get_pointer( Packet, &NodeID, &PanID ) ) {
            case E_ttc_packet_address_Target16: { // packet stores a 16-bit source ID
                Assert_PACKET_Readable( NodeID, ttc_assert_origin_auto );
                t_u16* SourceID16 = ( t_u16* ) NodeID;
                if ( SourceID16 ) {
                    if ( PanID ) {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "0x%04x/0x%04x", *SourceID16, *PanID );
                    }
                    else {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "0x%04x", *SourceID16 );
                    }
                }
                else {
                    if ( PanID ) {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "unknown/0x%04x", *PanID );
                    }
                    else {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "unknown" );
                    }
                }
                break;
            }
            case E_ttc_packet_address_Target64: { // packet stores a 64-bit source ID
                Assert_PACKET_Readable( NodeID, ttc_assert_origin_auto );
                t_u32* SourceID64 = ( t_u32* ) NodeID;
                if ( SourceID64 ) {
                    if ( PanID ) {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "0x%08x%08x/0x%04x", *SourceID64, *( SourceID64 + 1 ), *PanID );
                    }
                    else {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "0x%08x%08x", *SourceID64, *( SourceID64 + 1 ) );
                    }
                }
                else {
                    if ( PanID ) {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "unknown/0x%04x", *PanID );
                    }
                    else {
                        Buffer += ttc_string_appendf( Buffer, BufferSize, "unknown" );
                    }
                }
                break;
            }
            default: {
                Buffer += ttc_string_appendf( Buffer, BufferSize, "unknown" );
                break;
            }
        }
    }
    volatile t_u16                  PayloadSize = 0;
    volatile t_u8*                  Payload; // start of payload depends on packet type
    volatile e_ttc_packet_pattern  Protocol;

    ttc_packet_payload_get_pointer( Packet, ( t_u8** ) & Payload, ( t_u16* ) & PayloadSize, ( e_ttc_packet_pattern* ) &Protocol );
    if ( Debug.Size ) {     // compile size of payload and total size
        Buffer += ttc_string_appendf( Buffer, BufferSize, " [Payload=%i/%i]", PayloadSize, Packet->MAC.Length );
    }
    if ( Debug.Payload ) {  // get access to payload area
        Buffer += ttc_string_appendf( Buffer, BufferSize, "={" );
        if ( PayloadSize > *BufferSize )
        { PayloadSize = *BufferSize; }
        for ( t_u16 Index = 0; Index < PayloadSize; Index++ ) {
            Buffer += ttc_string_appendf( Buffer, BufferSize, "%02x", Payload[Index] );
            if ( Index + 1 < PayloadSize )
            { Buffer += ttc_string_appendf( Buffer, BufferSize, " " ); }
        }
        Buffer += ttc_string_appendf( Buffer, BufferSize, "}" );
    }

    return ( t_u16 )( Buffer - BufferStart );
}
t_u8                  ttc_packet_initialize( t_ttc_heap_block_from_pool* MemoryBlock, e_ttc_packet_type Type, e_ttc_packet_pattern SocketID ) {
    Assert_PACKET_Writable( MemoryBlock, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_ttc_packet* Packet = ( t_ttc_packet* ) MemoryBlock; // memory blocks from a pool can be casted to anything
    MemoryBlock--; // header is stored at negative offset
    Assert_PACKET_Writable( MemoryBlock->Pool, ttc_assert_origin_auto );  // this block does not seem to come from a valid memory pool!

    // reset meta data
    ttc_memory_set( &( Packet->Meta ), 0, sizeof( Packet->Meta ) );

    // Calculate maximum size of MAC part as distance between buffer end-adress and MAC start-address (MAC is 4 byte aligned)
    Packet->Meta.MaxSizeMAC = ( ( t_base ) Packet ) + MemoryBlock->Pool->BlockSize - ( ( t_base ) & ( Packet->MAC ) ) - 1;

    if ( Type ) { // extract size of memory block from its pool an let low-level driver initialize it
        Packet->Meta.Type = Type;
        t_u8 HeaderSize = _driver_packet_initialize( Packet, Type );

        s_ttc_packetocket_set( Packet, SocketID );
        return HeaderSize;
    }

    return 0;
}
t_u8                  ttc_packet_mac_header_get_size( t_ttc_packet* Packet ) {

    t_u8 Size = Packet->Meta.SizeOfMacHeader;
    if ( !Size )
    { Size = Packet->Meta.SizeOfMacHeader = _driver_packet_mac_header_get_size( Packet ); }

    return Size;
}
BOOL                  ttc_packet_pattern_matches( t_ttc_packet* Packet, e_ttc_packet_pattern Pattern ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto ); // always check incoming pointer arguments

    if ( Pattern < E_ttc_packet_pattern_type_End ) { // checking packet type instead of SocketID
        e_ttc_packet_type Type = E_ttc_packet_type_get( Packet );
        return ttc_packet_pattern_matches_type( Type, Pattern );
    }

    // extract first byte from payload and check if it matches given pattern
    e_ttc_packet_pattern PayloadProtocol = s_ttc_packetocket_get( Packet );
    return ttc_packet_pattern_matches_socket( PayloadProtocol, Pattern );
}
BOOL                  ttc_packet_pattern_matches_socket( e_ttc_packet_pattern Protocol, e_ttc_packet_pattern Pattern ) {
    Assert_PACKET( Protocol < E_ttc_packet_pattern_socket_Begin_AnyOf, ttc_assert_origin_auto ); // invalid argument given: a protocol type cannot use full range of this enum!

    if ( Pattern == 0 )
    { goto tppm_True; } // 0 always matches

    Assert_PACKET( Pattern > E_ttc_packet_pattern_socket_Begin,     ttc_assert_origin_auto ); // this function can only check E_ttc_packet_pattern_socket_* type values. See documentation in header file!
    Assert_PACKET( Pattern < E_ttc_packet_pattern_socket_End_AnyOf, ttc_assert_origin_auto ); // this function can only check E_ttc_packet_pattern_socket_* type values. See documentation in header file!
    switch ( Pattern ) {
        case E_ttc_packet_pattern_socket_AnyOf_Application: {    // Protocol must be inside  E_ttc_packet_pattern_socket_Begin_Application..ttc_packet_pattern_protocol_End_Application
            if ( ( Protocol >  E_ttc_packet_pattern_socket_Begin_Application ) && ( Protocol <  E_ttc_packet_pattern_socket_End_Application ) ) {
                goto tppm_True;
            }
            break;
        }
        case E_ttc_packet_pattern_socket_AnyOf_NonApplication: { // Protocol must be outside E_ttc_packet_pattern_socket_Begin_Application..ttc_packet_pattern_protocol_End_Application
            if ( ( Protocol <= E_ttc_packet_pattern_socket_Begin_Application ) || ( Protocol >= E_ttc_packet_pattern_socket_End_Application ) ) {
                goto tppm_True;
            }
            break;
        }
        case E_ttc_packet_pattern_socket_AnyOf_Radio: {          // Protocol must be inside  E_ttc_packet_pattern_socket_Begin_Radio..ttc_packet_pattern_protocol_End_Radio
            if ( ( Protocol >  E_ttc_packet_pattern_socket_Begin_Radio ) && ( Protocol <  E_ttc_packet_pattern_socket_End_Radio ) ) {
                goto tppm_True;
            }
            break;
        }
        case E_ttc_packet_pattern_socket_AnyOf_NonRadio: {       // Protocol must be outside E_ttc_packet_pattern_socket_Begin_Radio..ttc_packet_pattern_protocol_End_Radio
            if ( ( Protocol <= E_ttc_packet_pattern_socket_Begin_Radio ) || ( Protocol >= E_ttc_packet_pattern_socket_End_Radio ) ) {
                goto tppm_True;
            }
            break;
        }
        default: {                 // individual protocol type must exactly match
            if ( Pattern == Protocol )
            { goto tppm_True; }
            break;
        }
    }

    return FALSE;

tppm_True:
    return TRUE;
}
void                  ttc_packet_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)

    // sizes of these structures must be a multiple of 4 for correct alignment of following structures
#ifdef t_ttc_packet_meta_lowlevel
    Assert_PACKET( ( sizeof( t_ttc_packet_meta_lowlevel ) & 0x3 ) == 0, ttc_assert_origin_auto );  // size of this datatype must be a multiple of 4 for correct memory alignment. Check definition in low-level driver!
#endif

    if ( 1 ) { // check size of packet meta header and its sub fields
        t_ttc_packet_meta PacketMeta;

        // set max unsigned values to detect field sizes
        ttc_memory_set( &PacketMeta, 0xffffffff, sizeof( PacketMeta ) );
        Assert_PACKET( PacketMeta.MaxSizeMAC      > sizeof( t_ttc_packet_mac ) + 128, ttc_assert_origin_auto );  // field is too small to store maximum value: Increase field size in struct definition in ttc_packet_types.h!
        Assert_PACKET( PacketMeta.SizeOfMacHeader > sizeof( t_ttc_packet_mac ),       ttc_assert_origin_auto );  // field is too small to store maximum value: Increase field size in struct definition in ttc_packet_types.h!
        Assert_PACKET( PacketMeta.Category        > E_ttc_packet_category_unknown,    ttc_assert_origin_auto );  // field is too small to store maximum value: Increase field size in struct definition in ttc_packet_types.h!
        Assert_PACKET( PacketMeta.AddressFormat   > E_ttc_packet_address_unknown,     ttc_assert_origin_auto );  // field is too small to store maximum value: Increase field size in struct definition in ttc_packet_types.h!
        Assert_PACKET( PacketMeta.StatusTX        > tpst_unknown,                     ttc_assert_origin_auto );  // field is too small to store maximum value: Increase field size in struct definition in ttc_packet_types.h!
    }

    _driver_packet_prepare();
}
e_ttc_packet_pattern  s_ttc_packetocket_get( t_ttc_packet* Packet ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto ); // always check incoming pointer arguments

#if TTC_PACKET_SOCKET_BYTE==1
    e_ttc_packet_pattern  SocketID = 0;
    t_u8*                  PayloadStart;
    ttc_packet_payload_get_pointer( Packet, &PayloadStart, NULL, &SocketID );
    return SocketID; // e_ttc_packet_pattern

#else
    return 0; // e_ttc_packet_pattern
#endif

}
void                  s_ttc_packetocket_set( t_ttc_packet* Packet, e_ttc_packet_pattern SocketID ) {

#if TTC_PACKET_SOCKET_BYTE==1
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_PACKET( SocketID > E_ttc_packet_pattern_socket_Begin,       ttc_assert_origin_auto ); // given value is out of supported range. Choose one E_ttc_packet_pattern_socket_* value from e_ttc_packet_pattern!
    Assert_PACKET( SocketID < E_ttc_packet_pattern_socket_Begin_AnyOf, ttc_assert_origin_auto ); // given value is out of supported range. Choose one E_ttc_packet_pattern_socket_* value from e_ttc_packet_pattern!

    // store protocol type as first byte of payload
    t_u8 HeaderSize = ttc_packet_mac_header_get_size( Packet );
    *( ( ( t_u8* ) & ( Packet->MAC ) ) + HeaderSize ) = ( t_u8 ) SocketID;
#else
    ( void ) SocketID; // avoid compiler warning "unused..."
#endif
}

#if (TTC_ASSERT_PACKET > 0) // functions below only implement extra safety checks
void                  s_ttc_packet_payloadet_size( t_ttc_packet* Packet, t_u16 PayloadSize ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // always check incoming pointer arguments

    _driver_packet_payload_set_size( Packet, PayloadSize );

    Assert_PACKET( Packet->Meta.MaxSizeMAC - Packet->Meta.SizeOfMacHeader >= PayloadSize, ttc_assert_origin_auto );  // given value is too small for struct field
}
#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_packet(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
