#ifndef PWR_STM32L1XX_H
#define PWR_STM32L1XX_H

/** { pwr_stm32l1xx.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for pwr devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by high-level pwr and application.
 *
 *  Created from template device_architecture.h revision 22 at 20150130 10:43:12 UTC
 *
 *  Note: See ttc_pwr.h for description of stm32l1xx independent PWR implementation.
 *  
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_PWR_STM32L1XX
//
// Implementation status of low-level driver support for pwr devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_PWR_STM32L1XX 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_PWR_STM32L1XX == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_PWR_STM32L1XX to one from '-', 'o', '+' 
#endif

//}EXTENSION_STATUS_TTC_PWR_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "pwr_stm32l1xx.c"
//
#include "pwr_stm32l1xx_types.h"
#include "../ttc_pwr_types.h"
#include "../ttc_sysclock.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_pwr_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_pwr_foo
//
#define ttc_driver_pwr_deinit(Config) pwr_stm32l1xx_deinit(Config)
#define ttc_driver_pwr_get_features(Config) pwr_stm32l1xx_get_features(Config)
#define ttc_driver_pwr_init(Config) pwr_stm32l1xx_init(Config)
#define ttc_driver_pwr_load_defaults(Config) pwr_stm32l1xx_load_defaults(Config)
#define ttc_driver_pwr_prepare() pwr_stm32l1xx_prepare()
#define ttc_driver_pwr_reset(Config) pwr_stm32l1xx_reset(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_pwr.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_pwr.h for details.
 *
 * You normally should not need to add functions here. 
 * Add your private functions in the private section below. 
 */


/** shutdown single PWR unit device
 * @param Config        pointer to struct t_ttc_pwr_config (must have valid value for PhysicalIndex)
 * @return              == 0: PWR has been shutdown successfully; != 0: error-code
 */
e_ttc_pwr_errorcode pwr_stm32l1xx_deinit(t_ttc_pwr_config* Config);


/** fills out given Config with maximum valid values for indexed PWR
 * @param Config        = pointer to struct t_ttc_pwr_config (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_pwr_errorcode pwr_stm32l1xx_get_features(t_ttc_pwr_config* Config);


/** initializes single PWR unit for operation
 * @param Config        pointer to struct t_ttc_pwr_config (must have valid value for PhysicalIndex)
 * @return              == 0: PWR has been initialized successfully; != 0: error-code
 */
e_ttc_pwr_errorcode pwr_stm32l1xx_init(t_ttc_pwr_config* Config);


/** loads configuration of indexed PWR unit with default values
 * @param Config        pointer to struct t_ttc_pwr_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_pwr_errorcode pwr_stm32l1xx_load_defaults(t_ttc_pwr_config* Config);


/** Prepares pwr Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void pwr_stm32l1xx_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_pwr_config (must have valid value for PhysicalIndex)
 */
void pwr_stm32l1xx_reset(t_ttc_pwr_config* Config);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _pwr_stm32l1xx_foo(t_ttc_pwr_config* Config)

void pwr_stm32l1xx_voltage_scaling_config(t_u32 VoltageScaling);

void pwr_stm32l1xx_configure_sleep_mode();
void pwr_stm32l1xx_start_sleep_mode(t_u32 PWR_Regulator, t_u8 PWR_SLEEPEntry);
void pwr_stm32l1xx_configure_low_power_sleep_mode();

void pwr_stm32l1xx_start_low_power_run_mode();
void pwr_stm32l1xx_stop_low_power_run_mode();
void pwr_stm32l1xx_configure_low_power_run_mode();


void pwr_stm32l1xx_start_stop_mode(t_u32 PWR_Regulator, t_u8 PWR_STOPEntry);
void pwr_stm32l1xx_configure_stop_mode();

void pwr_stm32l1xx_start_standby_mode();
void pwr_stm32l1xx_configure_standby_mode();

void pwr_stm32l1xx_start_ultra_low_power_mode();
void pwr_stm32l1xx_stop_ultra_low_power_mode();



void flash_stm32l1xx_set_latency(t_u32 Latency);
void flash_stm32l1xx_start_prefetch_fuffer();
void flash_stm32l1xx_stop_prefetch_fuffer();
void flash_stm32l1xx_start_read_acceses_64();
void flash_stm32l1xx_stop_read_acceses_64();
void flash_stm32l1xx_start_sleep_power_down();
void flash_stm32l1xx_stop_sleep_power_down();

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //PWR_STM32L1XX_H