#ifndef PWR_STM32L1XX_TYPES_H
#define PWR_STM32L1XX_TYPES_H

/** { pwr_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *                     
 *  Low-Level datatypes for PWR devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_pwr_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150130 10:43:12 UTC
 *
 *  Note: See ttc_pwr.h for description of architecture independent PWR implementation.
 * 
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_pwr_types.h *************************


typedef enum {    // e_ttc_sysclock_errorcode     return codes of SYSCLOCK devices

    sleep_mode=0,
    low_power_run_mode,
    low_power_sleep_mode,
    stop_mode,
    standby_mode

} e_pwr_stm32l1xx_mode;



typedef struct { // register description (adapt according to stm32l1xx registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} t_pwr_register;

typedef struct {  // stm32l1xx specific configuration data of single PWR device
  t_pwr_register* BaseRegister;       // base address of PWR device registers
  e_pwr_stm32l1xx_mode Mode;
} __attribute__((__packed__)) t_pwr_stm32l1xx_config;

// t_ttc_pwr_architecture is required by ttc_pwr_types.h
#define t_ttc_pwr_architecture t_pwr_stm32l1xx_config

//} Structures/ Enums


#endif //PWR_STM32L1XX_TYPES_H
