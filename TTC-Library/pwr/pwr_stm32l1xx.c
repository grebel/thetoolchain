/** { pwr_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for pwr devices on stm32l1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150130 10:43:12 UTC
 *
 *  Note: See ttc_pwr.h for description of stm32l1xx independent PWR implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "pwr_stm32l1xx.h".
//
#include "pwr_stm32l1xx.h"
#include "stm32l1xx_pwr.h"
#include "stm32l1xx_rcc.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* --------- PWR registers bit address in the alias region ---------- */
#define PWR_OFFSET               (PWR_BASE - PERIPH_BASE)

/* --- CR Register ---*/

/* Alias word address of DBP bit */
#define CR_OFFSET                (PWR_OFFSET + 0x00)
#define DBP_BitNumber            0x08
#define CR_DBP_BB                (PERIPH_BB_BASE + (CR_OFFSET * 32) + (DBP_BitNumber * 4))

/* Alias word address of PVDE bit */
#define PVDE_BitNumber           0x04
#define CR_PVDE_BB               (PERIPH_BB_BASE + (CR_OFFSET * 32) + (PVDE_BitNumber * 4))

/* Alias word address of ULP bit */
#define ULP_BitNumber           0x09
#define CR_ULP_BB               (PERIPH_BB_BASE + (CR_OFFSET * 32) + (ULP_BitNumber * 4))

/* Alias word address of FWU bit */
#define FWU_BitNumber           0x0A
#define CR_FWU_BB               (PERIPH_BB_BASE + (CR_OFFSET * 32) + (FWU_BitNumber * 4))

/* --- CSR Register ---*/

/* Alias word address of EWUP bit */
#define CSR_OFFSET               (PWR_OFFSET + 0x04)
#define EWUP_BitNumber           0x08
#define CSR_EWUP_BB              (PERIPH_BB_BASE + (CSR_OFFSET * 32) + (EWUP_BitNumber * 4))

/* ------------------ PWR registers bit mask ------------------------ */

/* CR register bit mask */
#define CR_DS_MASK               ((t_u32)0xFFFFFFFC)
#define CR_PLS_MASK              ((t_u32)0xFFFFFF1F)
#define CR_VOS_MASK              ((t_u32)0xFFFFE7FF)

#define FLASH_Latency_0                ((t_u8)0x00)  /*!< FLASH Zero Latency cycle */
#define FLASH_Latency_1                ((t_u8)0x01)  /*!< FLASH One Latency cycle */

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

e_ttc_pwr_errorcode pwr_stm32l1xx_deinit(t_ttc_pwr_config* Config) {
    Assert_PWR(Config, ttc_assert_origin_auto); // pointers must not be NULL

    PWR_DeInit();
    
    return (e_ttc_pwr_errorcode) 0;
}
e_ttc_pwr_errorcode pwr_stm32l1xx_get_features(t_ttc_pwr_config* Config) {
    Assert_PWR(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (e_ttc_pwr_errorcode) 0;
}
e_ttc_pwr_errorcode pwr_stm32l1xx_init(t_ttc_pwr_config* Config) {
    Assert_PWR(Config, ttc_assert_origin_auto); // pointers must not be NULL

    e_pwr_stm32l1xx_mode mode = Config->LowLevelConfig->Mode;

    if(mode == sleep_mode){

        /*
         * The Sleep mode is entered by using the PWR_EnterSleepMode(PWR_Regulator_ON,) function with regulator ON.
         *
         */
        pwr_stm32l1xx_configure_sleep_mode();

    }else if(mode == low_power_run_mode){

        /*
         * Decrease the system frequency.
         * The regulator is forced in low power mode using the PWR_EnterLowPowerRunMode()function.
         */
        pwr_stm32l1xx_configure_low_power_run_mode();


    }else if(mode == low_power_sleep_mode){

        /*
         * The Flash memory must be switched off by using the FLASH_SLEEPPowerDownCmd()function.
         * Decrease the system frequency.
         * The regulator is forced in low power mode and the WFI or WFE instructions are executed using the PWR_EnterSleepMode(PWR_Regulator_LowPower,) function
         * with regulator in LowPower.
         */

        pwr_stm32l1xx_configure_low_power_sleep_mode();

    }else if(mode == stop_mode){

        /*
         * The Stop mode is entered using the PWR_EnterSTOPMode(PWR_Regulator_LowPower,) unction with regulator in LowPower or with Regulator ON
         */

        pwr_stm32l1xx_configure_stop_mode();


    }else if(mode == standby_mode){


        pwr_stm32l1xx_configure_standby_mode();

    } else {

        Assert_PWR(0,ttc_assert_origin_auto);
    }
    return (e_ttc_pwr_errorcode) 0;
}
e_ttc_pwr_errorcode pwr_stm32l1xx_load_defaults(t_ttc_pwr_config* Config) {
    Assert_PWR(Config, ttc_assert_origin_auto); // pointers must not be NULL

    t_ttc_pwr_architecture* ConfigArch = Config->LowLevelConfig;

    if (ConfigArch == NULL) {
        ConfigArch = (t_ttc_pwr_architecture*)ttc_heap_alloc_zeroed( sizeof(t_ttc_pwr_architecture) );
        Config->LowLevelConfig = ConfigArch;
    }
    ttc_memory_set(ConfigArch, 0, sizeof(t_ttc_pwr_architecture) );
    
    return (e_ttc_pwr_errorcode) 0;
}
void pwr_stm32l1xx_prepare() {

    

}
void pwr_stm32l1xx_reset(t_ttc_pwr_config* Config) {
    Assert_PWR(Config, ttc_assert_origin_auto); // pointers must not be NULL
    

}

/**
  * @brief  Configures the voltage scaling range.
  * @note   During voltage scaling configuration, the system clock is stopped
  *         until the regulator is stabilized (VOSF = 0). This must be taken
  *         into account during application developement, in case a critical
  *         reaction time to interrupt is needed, and depending on peripheral
  *         used (timer, communication,...).
  *
  * @param  PWR_VoltageScaling: specifies the voltage scaling range.
  *   This parameter can be:
  *     @arg PWR_VoltageScaling_Range1: Voltage Scaling Range 1 (VCORE = 1.8V)
  *     @arg PWR_VoltageScaling_Range2: Voltage Scaling Range 2 (VCORE = 1.5V)
  *     @arg PWR_VoltageScaling_Range3: Voltage Scaling Range 3 (VCORE = 1.2V)
  * @retval None
  */
void pwr_stm32l1xx_voltage_scaling_config(t_u32 VoltageScaling){

    t_u32 tmp = 0;

    /* Check the parameters */
    assert_param(IS_PWR_VOLTAGE_SCALING_RANGE(VoltageScaling));

    tmp = register_stm32l1xx_PWR.CR.All;

    tmp &= CR_VOS_MASK;
    tmp |= VoltageScaling;

    register_stm32l1xx_PWR.CR.All = tmp & 0xFFFFFFF3;

}


void pwr_stm32l1xx_configure_low_power_run_mode(){

    /* RCC system reset */
    RCC_DeInit();

    /* Disable Prefetch Buffer */
    flash_stm32l1xx_stop_prefetch_fuffer();

    /* Disable 64-bit access */
    flash_stm32l1xx_stop_read_acceses_64();

    /* Enable the PWR APB1 Clock */
    sysclock_stm32l1xx_RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

    /* Select the Voltage Range 2 (1.5V) */
    pwr_stm32l1xx_voltage_scaling_config(PWR_VoltageScaling_Range2);

    /* Wait Until the Voltage Regulator is ready */
    while(PWR_GetFlagStatus(PWR_FLAG_VOS) != RESET)
    {
    }

    /* HCLK = SYSCLK/2 = ~32KHz */
    sysclock_stm32l1xx_RCC_HCLKConfig(RCC_SYSCLK_Div2);

    /* PCLK2 = HCLK */
    sysclock_stm32l1xx_RCC_PCLK2Config(RCC_HCLK_Div1);

    /* PCLK1 = HCLK */
    sysclock_stm32l1xx_RCC_PCLK1Config(RCC_HCLK_Div1);

    /* Set MSI clock range to 65.536KHz */
    sysclock_stm32l1xx_RCC_MSIRangeConfig(RCC_MSIRange_0);

    /* Select MSI as system clock source */
    sysclock_stm32l1xx_RCC_SYSCLKConfig(RCC_SYSCLKSource_MSI);

    /* Wait till PLL is used as system clock source */
    while (RCC_GetSYSCLKSource() != 0x00)
    {}

    /* Enter RUN LP Mode */
    pwr_stm32l1xx_start_low_power_run_mode();

}


void pwr_stm32l1xx_configure_sleep_mode(){


    t_u32 StartUpCounter = 0, HSIStatus = 0;

    /* SYSCLK, HCLK, PCLK2 and PCLK1 configuration -----------------------------*/
    /* RCC system reset(for debug purpose) */
    sysclock_stm32l1xx_RCC_DeInit();

    /* Enable HSI */
    //X sysclock_stm32l1xx_RCC_HSICmd(ENABLE);
    ttc_sysclock_enable_oscillator(E_ttc_sysclock_oscillator_HighSpeedInternal, 1);

    /* Wait till HSI is ready and if Time out is reached exit */
    do
    {
      HSIStatus = sysclock_stm32l1xx_RCC_GetFlagStatus(ttc_sysclock_RCC_FLAG_HSIRDY);
      StartUpCounter++;
    } while((HSIStatus == 0) && (StartUpCounter != HSI_STARTUP_TIMEOUT));


    if (sysclock_stm32l1xx_RCC_GetFlagStatus(ttc_sysclock_RCC_FLAG_HSIRDY) != RESET)
    {
      HSIStatus = (t_u32)0x01;
    }
    else
    {
      HSIStatus = (t_u32)0x00;
    }

    if (HSIStatus == 0x01)
    {
      /* Enable 64-bit access */
      flash_stm32l1xx_start_read_acceses_64();

      /* Enable Prefetch Buffer */
      flash_stm32l1xx_start_prefetch_fuffer();

      /* Flash 1 wait state */
      flash_stm32l1xx_set_latency(FLASH_Latency_1);

      /* Enable the PWR APB1 Clock */
      sysclock_stm32l1xx_RCC_APB1PeriphClockCmd(ttc_sysclock_RCC_APB1Periph_PWR, ENABLE);

      /* Select the Voltage Range 2 (1.5V) */
      pwr_stm32l1xx_voltage_scaling_config(PWR_VoltageScaling_Range2);

      /* Wait Until the Voltage Regulator is ready */
      while(PWR_GetFlagStatus(PWR_FLAG_VOS) != RESET)
      {
      }

      /* HCLK = SYSCLK */
      sysclock_stm32l1xx_RCC_HCLKConfig(ttc_sysclock_RCC_SYSCLK_Div1);

      /* PCLK2 = HCLK */
      sysclock_stm32l1xx_RCC_PCLK2Config(ttc_sysclock_RCC_HCLK_Div1);

      /* PCLK1 = HCLK */
      sysclock_stm32l1xx_RCC_PCLK1Config(ttc_sysclock_RCC_HCLK_Div1);

      /* Select HSI as system clock source */
      sysclock_stm32l1xx_RCC_SYSCLKConfig(ttc_sysclock_RCC_SYSCLKSource_HSI);

      /* Wait till HSI is used as system clock source */
      while (RCC_GetSYSCLKSource() != 0x04)
      {}
    }
    else
    {
      /* If HSI fails to start-up, the application will have wrong clock configuration.
      User can add here some code to deal with this error */

      /* Go to infinite loop */
      while (1)
      {}
    }

    /* Request to enter SLEEP mode with regulator ON */
    pwr_stm32l1xx_start_sleep_mode(PWR_Regulator_ON, PWR_SLEEPEntry_WFI);

}

void pwr_stm32l1xx_configure_stop_mode(){

      /* Enable Ultra low power mode */
      pwr_stm32l1xx_start_ultra_low_power_mode();

      PWR_WakeUpPinCmd(PWR_WakeUpPin_1,ENABLE);

      /* Enter Stop Mode */
      pwr_stm32l1xx_start_stop_mode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);

}

void pwr_stm32l1xx_configure_standby_mode(){

    /* Enable Ultra low power mode */
    pwr_stm32l1xx_start_ultra_low_power_mode();

    PWR_WakeUpPinCmd(PWR_WakeUpPin_1,ENABLE);

    /* Request to enter STANDBY mode (Wake Up flag is cleared in PWR_EnterSTANDBYMode function) */
    pwr_stm32l1xx_start_standby_mode();

}

void pwr_stm32l1xx_configure_low_power_sleep_mode(){

    /* RCC system reset */
    sysclock_stm32l1xx_RCC_DeInit();

    /* Disable Prefetch Buffer */
    flash_stm32l1xx_stop_prefetch_fuffer();

    /* Disable 64-bit access */
    flash_stm32l1xx_stop_read_acceses_64();

    /* Enable the PWR APB1 Clock */
    sysclock_stm32l1xx_RCC_APB1PeriphClockCmd(ttc_sysclock_RCC_APB1Periph_PWR, ENABLE);

    /* Select the Voltage Range 2 (1.5V) */
    pwr_stm32l1xx_voltage_scaling_config(PWR_VoltageScaling_Range2);

    /* Wait Until the Voltage Regulator is ready */
    while(PWR_GetFlagStatus(PWR_FLAG_VOS) != RESET)
    {
    }

    /* HCLK = SYSCLK/2 = ~32KHz */
    sysclock_stm32l1xx_RCC_HCLKConfig(ttc_sysclock_RCC_SYSCLK_Div2);

    /* PCLK2 = HCLK */
    sysclock_stm32l1xx_RCC_PCLK2Config(ttc_sysclock_RCC_HCLK_Div1);

    /* PCLK1 = HCLK */
    sysclock_stm32l1xx_RCC_PCLK1Config(ttc_sysclock_RCC_HCLK_Div1);

    /* Set MSI clock range to 65.536KHz */
    sysclock_stm32l1xx_RCC_MSIRangeConfig(ttc_sysclock_RCC_MSIRange_0);

    /* Select MSI as system clock source */
    sysclock_stm32l1xx_RCC_SYSCLKConfig(ttc_sysclock_RCC_SYSCLKSource_MSI);

    /* Wait till PLL is used as system clock source */
    while (RCC_GetSYSCLKSource() != 0x00)
    {}

    /* Enable The ultra Low Power Mode */
    pwr_stm32l1xx_start_ultra_low_power_mode();

    /* Enable the power down mode during Sleep mode */
    flash_stm32l1xx_start_sleep_power_down();

    /* Request to enter SLEEP mode with regulator in low power mode */
    pwr_stm32l1xx_start_sleep_mode(PWR_Regulator_LowPower, PWR_SLEEPEntry_WFI);

}

/**
  * @brief  Enables or disables the Ultra Low Power mode.
  * @param  NewState: new state of the Ultra Low Power mode.
  *   This parameter can be: ENABLE or DISABLE.
  * @retval None
  */
void pwr_stm32l1xx_start_ultra_low_power_mode(){

  *(volatile t_u32 *) CR_ULP_BB = (t_u32)ENABLE;
}

void pwr_stm32l1xx_stop_ultra_low_power_mode(){

  *(volatile t_u32 *) CR_ULP_BB = (t_u32)DISABLE;
}


/*
* @brief  Enters/Exits the Low Power Run mode.
* @note   Low power run mode can only be entered when VCORE is in range 2.
*         In addition, the dynamic voltage scaling must not be used when Low
*         power run mode is selected. Only Stop and Sleep modes with regulator
*         configured in Low power mode is allowed when Low power run mode is
*         selected.
* @note   In Low power run mode, all I/O pins keep the same state as in Run mode.
* @param  NewState: new state of the Low Power Run mode.
*   This parameter can be: ENABLE or DISABLE.
* @retval None
*/
void pwr_stm32l1xx_start_low_power_run_mode(){


        register_stm32l1xx_PWR.CR.All |= CR_LPSDSR;
        register_stm32l1xx_PWR.CR.All |= CR_LPRUN;

}

void pwr_stm32l1xx_stop_low_power_run_mode(){

    register_stm32l1xx_PWR.CR.All &= (t_u32)~((t_u32)CR_LPRUN);
    register_stm32l1xx_PWR.CR.All &= (t_u32)~((t_u32)CR_LPSDSR);

}

/**
* @brief  Enters Sleep mode.
* @note   In Sleep mode, all I/O pins keep the same state as in Run mode.
* @param  PWR_Regulator: specifies the regulator state in Sleep mode.
*   This parameter can be one of the following values:
*     @arg PWR_Regulator_ON: Sleep mode with regulator ON
*     @arg PWR_Regulator_LowPower: Sleep mode with regulator in low power mode
* @note   Low power sleep mode can only be entered when VCORE is in range 2.
* @note   When the voltage regulator operates in low power mode, an additional
*         startup delay is incurred when waking up from Low power sleep mode.
*
* @param  PWR_SLEEPEntry: specifies if SLEEP mode in entered with WFI or WFE instruction.
*   This parameter can be one of the following values:
*     @arg PWR_SLEEPEntry_WFI: enter SLEEP mode with WFI instruction
*     @arg PWR_SLEEPEntry_WFE: enter SLEEP mode with WFE instruction
* @retval None
*/
void pwr_stm32l1xx_start_sleep_mode(t_u32 PWR_Regulator, t_u8 PWR_SLEEPEntry){

    t_u32 tmpreg = 0;

    /* Select the regulator state in Sleep mode ---------------------------------*/
    tmpreg = register_stm32l1xx_PWR.CR.All;

    /* Clear PDDS and LPDSR bits */
    tmpreg &= 0xFFFFFFFC;

    /* Set LPDSR bit according to PWR_Regulator value */
    tmpreg |= PWR_Regulator;

    /* Store the new value */
    register_stm32l1xx_PWR.CR.All = tmpreg;

    /* Clear SLEEPDEEP bit of Cortex System Control Register */
    SCB->SCR &= (t_u32)~((t_u32)SCB_SCR_SLEEPDEEP);

    /* Select SLEEP mode entry -------------------------------------------------*/
    if(PWR_SLEEPEntry == PWR_SLEEPEntry_WFI)
    {
        /* Request Wait For Interrupt */
        __WFI();
    }
    else
    {
        /* Request Wait For Event */
        __WFE();
    }
}

/**
* @brief  Enters STOP mode.
* @note   In Stop mode, all I/O pins keep the same state as in Run mode.
* @note   When exiting Stop mode by issuing an interrupt or a wakeup event,
*         the MSI RC oscillator is selected as system clock.
* @note   When the voltage regulator operates in low power mode, an additional
*         startup delay is incurred when waking up from Stop mode.
*         By keeping the internal regulator ON during Stop mode, the consumption
*         is higher although the startup time is reduced.
* @param  PWR_Regulator: specifies the regulator state in STOP mode.
*   This parameter can be one of the following values:
*     @arg PWR_Regulator_ON: STOP mode with regulator ON
*     @arg PWR_Regulator_LowPower: STOP mode with regulator in low power mode
* @param  PWR_STOPEntry: specifies if STOP mode in entered with WFI or WFE instruction.
*   This parameter can be one of the following values:
*     @arg PWR_STOPEntry_WFI: enter STOP mode with WFI instruction
*     @arg PWR_STOPEntry_WFE: enter STOP mode with WFE instruction
* @retval None
*/
void pwr_stm32l1xx_start_stop_mode(t_u32 PWR_Regulator, t_u8 PWR_STOPEntry){

    t_u32 tmpreg = 0;

    /* Check the parameters */
    assert_param(IS_PWR_REGULATOR(PWR_Regulator));
    assert_param(IS_PWR_STOP_ENTRY(PWR_STOPEntry));

    /* Select the regulator state in STOP mode ---------------------------------*/
    tmpreg = register_stm32l1xx_PWR.CR.All;
    /* Clear PDDS and LPDSR bits */
    tmpreg &= CR_DS_MASK;

    /* Set LPDSR bit according to PWR_Regulator value */
    tmpreg |= PWR_Regulator;

    /* Store the new value */
    register_stm32l1xx_PWR.CR.All = tmpreg;

    /* Set SLEEPDEEP bit of Cortex System Control Register */
    SCB->SCR |= SCB_SCR_SLEEPDEEP;

    /* Select STOP mode entry --------------------------------------------------*/
    if(PWR_STOPEntry == PWR_STOPEntry_WFI){

        /* Request Wait For Interrupt */
        __WFI();
    }
    else{

        /* Request Wait For Event */
        __WFE();
    }

    /* Reset SLEEPDEEP bit of Cortex System Control Register */
    SCB->SCR &= (t_u32)~((t_u32)SCB_SCR_SLEEPDEEP);
}

/**
* @brief  Enters STANDBY mode.
* @note   In Standby mode, all I/O pins are high impedance except for:
*          - Reset pad (still available)
*          - RTC_AF1 pin (PC13) if configured for Wakeup pin 2 (WKUP2), tamper,
*            time-stamp, RTC Alarm out, or RTC clock calibration out.
*          - WKUP pin 1 (PA0) and WKUP pin 3 (PE6), if enabled.
* @param  None
* @retval None
*/
void pwr_stm32l1xx_start_standby_mode(){

    /* Clear Wakeup flag */
    register_stm32l1xx_PWR.CR.All |= CR_CWUF;

    /* Select STANDBY mode */
    register_stm32l1xx_PWR.CR.All |= CR_PDDS;

    /* Set SLEEPDEEP bit of Cortex System Control Register */
    SCB->SCR |= SCB_SCR_SLEEPDEEP;

    /* This option is used to ensure that store operations are completed */
#if defined ( __CC_ARM   )
    __force_stores();
#endif
    /* Request Wait For Interrupt */
    __WFI();

}

/**
  * @brief  Sets the code latency value.
  * @param  FLASH_Latency: specifies the FLASH Latency value.
  *   This parameter can be one of the following values:
  *     @arg FLASH_Latency_0: FLASH Zero Latency cycle
  *     @arg FLASH_Latency_1: FLASH One Latency cycle
  * @retval None
  */
void flash_stm32l1xx_set_latency(t_u32 Latency){

   t_u32 tmpreg = 0;

   /* Read the ACR register */
   tmpreg = FLASH->ACR;

   /* Sets the Latency value */
   tmpreg &= (t_u32) (~((t_u32)FLASH_ACR_LATENCY));
   tmpreg |= Latency;

   /* Write the ACR register */
   FLASH->ACR = tmpreg;
}

/**
  * @brief  Enables or disables the Prefetch Buffer.
  * @param  NewState: new state of the FLASH prefetch buffer.
  *              This parameter can be: ENABLE or DISABLE.
  * @retval None
  */
void flash_stm32l1xx_start_prefetch_fuffer(){

    FLASH->ACR |= FLASH_ACR_PRFTEN;

}

void flash_stm32l1xx_stop_prefetch_fuffer(){

    FLASH->ACR &= (t_u32)(~((t_u32)FLASH_ACR_PRFTEN));

}

/**
  * @brief  Enables or disables read access to flash by 64 bits.
  * @param  NewState: new state of the FLASH read access mode.
  *              This parameter can be: ENABLE or DISABLE.
  * @note   - If this bit is set, the Read access 64 bit is used.
  *         - If this bit is reset, the Read access 32 bit is used.
  * @note   - This bit cannot be written at the same time as the LATENCY and
  *           PRFTEN bits.
  *         - To reset this bit, the LATENCY should be zero wait state and the
  *           prefetch off.
  * @retval None
  */
void flash_stm32l1xx_start_read_acceses_64(){


    FLASH->ACR |= FLASH_ACR_ACC64;

}


void flash_stm32l1xx_stop_read_acceses_64(){

     FLASH->ACR &= (t_u32)(~((t_u32)FLASH_ACR_ACC64));

}


/**
  * @brief  Enable or disable the power down mode during Sleep mode.
  * @note   This function is used to power down the FLASH when the system is in SLEEP LP mode.
  * @param  NewState: new state of the power down mode during sleep mode.
  *   This parameter can be: ENABLE or DISABLE.
  * @retval None
  */
void flash_stm32l1xx_start_sleep_power_down(){

    /* Set the SLEEP_PD bit to put Flash in power down mode during sleep mode */
    FLASH->ACR |= FLASH_ACR_SLEEP_PD;

}

void flash_stm32l1xx_stop_sleep_power_down(){

    /* Clear the SLEEP_PD bit in to put Flash in idle mode during sleep mode */
    FLASH->ACR &= (t_u32)(~((t_u32)FLASH_ACR_SLEEP_PD));
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
