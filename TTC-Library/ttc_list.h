#ifndef ttc_list_H
#define ttc_list_H

/** ttc_list.h ***********************************************{
 *
 * Written by Gregor Rebel 2013-2018
 *
 * Architecture independent support for single linked lists.
 *
 *
 */
/** Description of ttc_list (Do not delete this line!)
 *
 * Why to use lists instead of queues?
 * - Lists have no size limit (every item brings its own next-pointer).
 * - Lists require only as much memory as required (no oversized pre-allocation like queues).
 * - Lists have constant access times too.
 *
 * When not to prefer lists over queues?
 * - If you want to store many small blocks like single bytes
 *   (every item requires at least one next-pointer).
 *
 * Lists can be used as
 * - Input/ Output Queues of dynamic Length
 * - Storage for Memory Blocks
 * - Communication Channel Task<->Task and Task<->Interrupt Service Routines
 * - Task Synchronization
 *
 *
 * Features of ttc_list
 *
 * - Each item can carry a pointer to its list.
 *   Extra self tests then can check if an item should be stored in two lists at the same time.
 *   This typically indicates an error condition that is otherwise difficult to detect.
 *   See TTC_LIST_ITEMS_IN_ONE_LIST_ONLY in ttc_lity_types.h for details!
 *
 * - If a task waits to pull from an empty list, it is put to sleep and awaken after next push.
 *   The same applies for user application in single tasking. An interrupt service routine can
 *   use a *_push_*_isr() function to wake up the user application.
 *
 * - Special _isr() functions are available for interrupt service routines.
 *   An interrupt service routine cannot use the generic *_push_*()/ *_pull_*() functions.
 *   If a user space application currently is accessing a ttc_list, then dara corruption would occur.
 *   The special *_isr() functions will never block and avoid data corruption.
 *
 * - Every memory block starting with a t_ttc_list_item field can be added to a ttc_list.
 *   Simply add this as the first entry to your struct definition:
 *       #include "ttc_list_types.h"
 *       struct {
 *          t_ttc_list_item Item; // required as first entry!
 *          ...
 *       } t_foo;
 *
 * See example_ttc_list for details how to use ttc__list!
}*/

//{ includes

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_list_types.h"
#include "ttc_mutex_types.h"
#include "ttc_memory_types.h"
#include "ttc_heap_types.h"
#include "ttc_semaphore.h"

//}includes
//{ Default settings (change via makefile) ******************************

//}Default settings
//{ Types & Structures ***************************************************

//}
//{ Function prototypes **************************************************

/** counts amount of items linked together starting at given item
 *
 * Note: This functions terminates within O(n) with n = amount of items
 * Note: This functions will not terminate if list contains a loop!
 *
 * @param FirstItem   list item to start counting at
 * @return            amount of items linked together until first NULL-item
 */
t_base ttc_list_count_items( t_ttc_list_item* FirstItem );

/** counts amount of items linked together starting at given item (always returns)
 *
 * Note: This functions terminates within O(n) with n = amount of items
 *
 * @param FirstItem   list item to start counting at
 * @param MaxSize     counting will be stopped when list is longer
 * @return            amount of items linked together until first NULL-item
 */
t_base ttc_list_count_items_safe( t_ttc_list_item* FirstItem, t_base MaxSize );

/** allocates + initializes a new list
 *
 * @param  Name  used for debugging purposes only
 * @return =! NULL: list has been created successfully (use handle to operate on list)
 */
t_ttc_list* ttc_list_create( const char* Name );

/** initializes a new list in given storage place
 *
 * @param List   pointer to memory space to be used to manage list
 * @param Name   used for debugging purposes only
 * @return =! NULL: list has been created successfully (use handle to operate on list)
 */
void ttc_list_init( t_ttc_list* List, const char* Name );

/** Run iterator function beginning with n-th item in given list
 *
 * Note: List will be locked for push/pop operations during iteration.
 *
 * IterateFunction() can return TRUE to have Item being removed from list.
 * In such a case, it is safe for IterateFunction() to call ttc_heap_pool_block_free() to return
 * memory buffer to its memory pool (if it came from one).

 // Example:

 typedef struct { // a list item storing data
   t_ttc_list_item Item;  // header of all list items
   t_base Value;
   ...
 } t_my_type;

 void release_item(void* Argument, t_ttc_list_item* Item) {
   (void) Argument;                // not used in this example
   ttc_heap_pool_block_free(Item); // send memory buffer back to its pool
 }

 BOOL filter( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item ) {
    t_base Search = (t_base) Argument; // we simply expect this type

   // only works if this item came from a memory pool
   t_ttc_heap_block_from_pool* MemoryBlock = ttc_heap_pool_from_list_item( Item );

   // memory blocks from pool may be casted to anything
   t_my_type* Data = ( t_my_type* ) MemoryBlock;

   if (Data->Value == Search) { // found item with matching value
     return release_item; // Item will be removed from list and returned to its memory pool by calling ttc_heap_pool_block_free( (void*) Item) automatically.
   }

   return FALSE; // Item will remainf in list
 }

 ...
 t_base Search = 42;

 // Call filter(42, PrevItem, Item) for all items from number (1, 2, .., last)
 ttc_list_iterate(List, filter, 1, 0, -1);

 // Call filter(42, PrevItem, Item) for for up to 5 of every second item starting at fourth item (4, 6, 8, 10, 12)
 ttc_list_iterate(List, filter, 2, 3, 5);

 * @param List             pointer to memory space to be used to manage list
 * @param IterateFunction  function pointer: will be called for each list item.
 *                         Note: IterateFunction() may not append item to other lists or release it. It may return a function pointer that can process the item later (see @return below)!
 *                         @return ==0: do not remove item; ==1: current item shall be removed from list
 *                                 >1: function to call with item as argument AFTER removing it from current list.
 *                                     Signature of function being called: void RemoveItem(void* Argument, t_ttc_list_item* Item)
 * @param Argument         argument passed to IterateFunction() and RemoveItem()
 * @param Step             Allows to skip items:
 *                         == 1: iterate all items 1, 2, ..., last
 *                         == 2: iterate every second item 1, 3, ... last
 * @param SkipFirst        == 0: start at first item; >0: skip first items and start at item number SkipFirst + 1
 * @param Amount           maximum amount of items to iterate (==-1: not limited)
 */
void ttc_list_iterate( t_ttc_list* List, void* ( *IterateFunction )( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item ), void* Argument, t_s16 Step, t_u16 SkipFirst, t_u16 Amount );

/** Try to tun iterator function beginning with n-th item in given list
 *
 * Functionality is basically the same as ttc_list_iterate(). See its description above for details!
 * The only difference is that it will not block if List->Mutex is locked but return FALSE instead.
 * @return         ==TRUE: list was iterated through successfully; ==FALSE: list was locked and no iteration took place.
 */
BOOL ttc_list_iterate_try( t_ttc_list* List, void* ( *IterateFunction )( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item ), void* Argument, t_s16 Steps, t_u16 SkipFirst, t_u16 Amount );

/** Run iterator function beginning from start item in given list
 *
 * Usage is similar to ttc_list_iterate(). See its description above for an usage example.
 *
 * @param List             pointer to memory space to be used to manage list
 * @param IterateFunction  function pointer: will be called for each list item. Returns TRUE if current item shall be removed from list.
 *                         Note: IterateFunction() may not append item to other lists or release it. It may return a function pointer that can process the item later (see @return below)!
 *                         @return ==0: do not remove item; ==1: current item shall be removed from list
 *                                 >1: function to call with item as argument AFTER removing it from current list.
 *                                     Signature of function being called: void RemoveItem(void* Argument, t_ttc_list_item* Item)
 * @param Argument         argument passed to IterateFunction() and RemoveItem()
 * @param Step             Allows to skip items:
 *                         == 1: iterate all items 1, 2, ..., last
 *                         == 2: iterate every second item 1, 3, ... last
 * @param Start            pointer to list item for which IterateFunction is called first
 * @param End              pointer to list item for which IterateFunction is called last (==TTC_LIST_TERMINATOR or ==NULL: run to end of list)
 * @param Amount           maximum amount of items to iterate (==-1: not limited)
 */
void ttc_list_iterate_from( t_ttc_list* List, void* ( *IterateFunction )( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item ), void* Argument, t_s16 Step, t_ttc_list_item* Start, t_ttc_list_item* End, t_u16 Amount );

/** Try to run iterator function beginning from start item in given list
 *
 * Functionality is basically the same as ttc_list_iterate_from(). See its description above for details!
 * The only difference is that it will not block if List->Mutex is locked but return FALSE instead.
 * @return         ==TRUE: list was iterated through successfully; ==FALSE: list was locked and no iteration took place.
 */
BOOL ttc_list_iterate_from_try( t_ttc_list* List, void* ( *IterateFunction )( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item ), void* Argument, t_s16 Steps, t_ttc_list_item* Start, t_ttc_list_item* End, t_u16 Amount );

/** returns amount of items stored in List
 *
 * Note: This function completes in constant time O(1)
 *
 * @param List     pointer to already created list
 * @return         Amount of items currently stored in List
 */
t_base s_ttc_listize( t_ttc_list* List );
#define s_ttc_listize(LIST)  ( ttc_semaphore_available( &((LIST)->AmountItems) ) )

/** completes all pending operations on given list
 *
 * Note: This function may only be called from an interrupt service routine only!
 * Note: This function completes in constant time O(1)
 *
 * @param List     pointer to already created list
 * @return         == tle_OK: all pending operations have been completed
 *                 == tle_ListInUseByTask: list is locked by a task (no operations completed)
 */
e_ttc_list_error ttc_list_complete_all_pending_isr( t_ttc_list* List );

/** adds given single Item at end of given list
 * @param List     pointer to already created list
 * @param Item     pointer to item to be added to list
 */
void ttc_list_push_back_single( t_ttc_list* List, t_ttc_list_item* Item );

/** adds given linked list of items at end of given list
 * @param List       pointer to already created list
 * @param FirstItem  pointer to first item in list of items to be added to list
 * @param LastItem   pointer to last item in list of items to be added to list
 * @param Amount     Amount of items to be added to list
 */
void ttc_list_push_back_multiple( t_ttc_list* List, t_ttc_list_item* FirstItem, t_ttc_list_item* LastItem, t_base Amount );

/** adds given single Item at end of given list (does never block)
 *
 * Note: Must be called from interrupt service routine only!
 * Note: function terminates in O(1)
 *
 * @param List     pointer to already created list
 * @param Item     pointer to item to be added to list
 * @return         == tle_OK: Item has been successfully added to List
 *                 == tle_ListInUseByTask: items have been added to pending list and will be later on later calls
 *                                         (you may call ttc_list_push_back_isr(List, NULL) to retry push later
 */
e_ttc_list_error ttc_list_push_back_single_isr( t_ttc_list* List, t_ttc_list_item* Item );

/** adds given linked list of Items at end of given list (does never block)
 *
 * Note: Must be called from interrupt service routine only!
 * Note: function terminates in O(1)
 *
 * @param List       pointer to already created list
 * @param FirstItem  pointer to first item in list of items to be added to list
 * @param LastItem   pointer to last item in list of items to be added to list
 * @param Amount     Amount of items to be added to list
 * @return         == tle_OK: Item has been successfully added to List
 *                 == tle_ListInUseByTask: items have been added to pending list and will be completed on later calls
 *                                         (you may call ttc_list_push_back_isr(List, NULL) to retry push
 */
e_ttc_list_error ttc_list_push_back_multiple_isr( t_ttc_list* List, t_ttc_list_item* FirstItem, t_ttc_list_item* LastItem, t_base Amount );

/** adds given single Item at head of given list
 *
 * @param List     pointer to already created list
 * @param Item     pointer to item to be added to list
 */
void ttc_list_push_front_single( t_ttc_list* List, t_ttc_list_item* Item );

/** adds given single Item at head of given list (does never block)
 *
 * Note: Must be called from interrupt service routine only!
 * Note: function terminates in O(1)
 *
 * @param List     pointer to already created list
 * @param Item     pointer to item to be added to list
 * @return         == tle_OK: Item has been successfully added to List
 *                 == tle_ListInUseByTask: items have been added to pending list and will be completed on later calls
 *                                         (you may call ttc_list_push_back_isr(List, NULL) to retry push
 */
e_ttc_list_error ttc_list_push_front_single_isr( t_ttc_list* List, t_ttc_list_item* Item );


/** delivers pointer to first list entry without removing it from list
 *
 * Note: This function provides very fast access to inspect first list item.
 * Note: Returned pointer must be handled with special care to avoid data corruption.
 *       Caller must take care of race conditions with other tasks and interrupt service routines!
 * Note: This function can safely be called from an interrupt service routine.
 *
 * @param List     pointer to already created list
 * @return         !=NULL: pointer to first item; ==NULL: list is empty
 */
t_ttc_list_item* ttc_list_peek_front( t_ttc_list* List );

/** removes first Item from given list and delivers its reference
 *
 * Note: This function will do endless wait if list is empty!
 *
 * @param List     pointer to already created list
 * @return         pointer to item removed from given list
 */
t_ttc_list_item* ttc_list_pop_front_single_wait( t_ttc_list* List );

/** tries to remove first Item from given list and delivers its reference
 *
 * Note: Function returns immediately if list is empty.
 *
 * @param List     pointer to already created list
 * @return         != NULL: pointer to item removed from given list; == NULL: list is empty
 */
t_ttc_list_item* ttc_list_pop_front_single_try( t_ttc_list* List );

/** removes linked list of first items from front of given list and delivers reference to first and last one
 *
 * Note: This function completes in constant time O(Amount)
 *
 * @param List      pointer to already created list
 * @param Amount    amount of items to remove from list (==-1: means all items in list)
 * @param FirstItem loaded with pointer to first item of items removed from given list or NULL if list is empty
 * @param LastItem  loaded with pointer to last item of items removed from given list or NULL if list is empty
 * @return          actual amount of items being removed from list
 */
t_base ttc_list_pop_front_multiple( t_ttc_list* List, t_base Amount, t_ttc_list_item** FirstItem, t_ttc_list_item** LastItem );

/** removes linked list of first items from front of given list and delivers reference to first and last one (does never block)
 *
 * Note: Must be called from interrupt service routine only!
 * Note: This function completes in constant time O(Amount)
 *
 * @param List      pointer to already created list
 * @param Amount    amount of items to remove from list (==-1: means all items in list)
 * @param FirstItem loaded with pointer to first item of items removed from given list or NULL if list is empty
 * @param LastItem  loaded with pointer to last item of items removed from given list or NULL if list is empty
 * @return         == tle_OK: Items have been successfully removed from list
 *                 == tle_ListInUseByTask: list is currently in use by task; try again later
 */
e_ttc_list_error ttc_list_pop_front_multiple_isr( t_ttc_list* List, t_base Amount, t_ttc_list_item** FirstItem, t_ttc_list_item** LastItem );

/** removes first Items from given list and delivers reference to first item (does never block)
 *
 * Note: Must be called from interrupt service routine only!
 *
 * @param List     pointer to already created list
 * @return         != NULL: pointer to item removed from given list;
 *                 == NULL: list is empty
 *                 == 1:    list is locked by task
 */
t_ttc_list_item* ttc_list_pop_front_single_isr( t_ttc_list* List );

//}Function prototypes

#endif
