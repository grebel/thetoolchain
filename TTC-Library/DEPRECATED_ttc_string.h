#ifndef ttc_string_H
#define ttc_string_H

/*{ ttc_string.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Architecture independent support for operations on zero terminated character sequences (strings).
 * Here you can find support for string conversion and output. You will find an implementation
 * of printf() and snprintf() and several single byte conversions (faster than printf).
 *
}*/

//{ includes

#include "ttc_basic.h"
#include "ttc_memory.h"
#include "ttc_queue.h"
#include "stdarg.h"  // required for va_list

//}includes
//{ Constants & Defines ***************************************************

#ifndef TTC_ASSERT_STRING    // any previous definition set (Makefile)?
#define TTC_ASSERT_STRING 1  // string asserts are enaböed by default
#endif
#if (TTC_ASSERT_STRING == 1)  // use Assert()s in string functions (somewhat slower but alot easier to debug)
#define Assert_String(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in string functions (somewhat smaller + faster, but crashes are hard to debug)
#define Assert_String(Condition, ErrorCode)
#endif

// ttc_string provides complete, architecture optimized string support.
// Do not include other string libraries in your project!
//
/** If you include external libraries, add these lines to your install script to replace unwanted includes:

cat >replaceIncludes.sh <<END_OF_REPLACE
#!/bin/bash

source \$HOME/Source/TheToolChain/InstallData/scripts/installFuncs.sh

echo "\$0 patching includes in file \$1 ..."
replaceInFile "\$1" "#include <stdio.h>"  "// #include <stdio.h> // TTC provides optimized version of this library"
replaceInFile "\$1" "#include <stdlib.h>" "// #include <stdlib.h> // TTC provides optimized version of this library"
replaceInFile "\$1" "#include <string.h>"  '#include "ttc_memory.h" // #include <string.h>  // TTC provides optimized version of this library'

END_OF_REPLACE
    find ./ -name "*.c" -exec bash ./replaceIncludes.sh {} \;
    find ./ -name "*.h" -exec bash ./replaceIncludes.sh {} \;
*/

#ifdef snprintf
#  error snprintf() already defined outside ttc_string! Include ttc_string.h instead of <string.h>!
#endif
#ifdef printf
#  error printf() already defined outside ttc_string! Include ttc_string.h instead of <string.h>!
#endif
#ifdef print
#  error print() already defined outside ttc_string! Include ttc_string.h instead of <string.h>!
#endif
#ifdef strlen
#  error strlen() already defined outside ttc_string! Include ttc_string.h instead of <string.h>!
#endif
#ifdef stnrlen
#  error stnrlen() already defined outside ttc_string! Include ttc_string.h instead of <string.h>!
#endif
#ifdef strcpy
#  error strcpy() already defined outside ttc_string! Include ttc_string.h instead of <string.h>!
#endif
#ifdef strncpy
#  error strncpy() already defined outside ttc_string! Include ttc_string.h instead of <string.h>!
#endif

// Compatibility macros for <string.h> library (add more if needed)
#define snprintf(...)                         ttc_string_snprintf(__VA_ARGS__)
#define printf(...)                           ttc_string_printf(__VA_ARGS__)
#define print(String, MaxSize)                ttc_string_print(String, MaxSize)
#define strcpy(Destination, Source)           ttc_string_copy(Destination, Source, -1)
#define strncpy(Destination, Source, MaxSize) ttc_string_copy(Destination, Source, MaxSize)

#if TARGET_DATA_WIDTH == 8
#  ifndef strlen
#    define strlen(String) ttc_string_length8(String, -1)
#  endif
#  ifndef stnrlen
#    define stnrlen(String, MaxLength) ttc_string_length8(String, MaxLength)
#  endif
#endif
#if TARGET_DATA_WIDTH == 16
#  ifndef strlen
#    define strlen(String) ttc_string_length16(String, -1)
#  endif
#  ifndef stnrlen
#    define stnrlen(String, MaxLength) ttc_string_length16(String, MaxLength)
#  endif
#endif
#if TARGET_DATA_WIDTH == 32
#  ifndef strlen
#    define strlen(String) ttc_string_length32(String, -1)
#  endif
#  ifndef stnrlen
#    define stnrlen(String, MaxLength) ttc_string_length32(String, MaxLength)
#  endif
#endif

// size of temporary buffer used by ttc_string_printf()
#ifndef TTC_STRING_PRINTF_BUFFERSIZE
#define TTC_STRING_PRINTF_BUFFERSIZE 100
#endif

//}
//{ Function prototypes **************************************************


/** Removes leading and trailing invisible characters.
 *
 * @param Start     pointer storing address of first byte of string (will be moved to first non whitespace character)
 * @param Size      pointer storing size of string= (amount of valid bytes starting at *Start (will be adjusted)
 * @return  *Start, *Size get adjusted; String in Start[] is not altered
 */
void ttc_string_strip( u8_t** Start, Base_t* Size );

/** Checks if given character is an invisible character
 *
 * @param ASCII   character to check (ASCII code)
 * @return        == TRUE: character is an invisible character
 */
BOOL ttc_string_is_whitechar( u8_t ASCII );

/** Copies string from Source to Target address until first zero byte or MaxSize reached.
 *
 * Note: String in target will always be zero terminated, even if Target[] is smaller than Source[].
 *
 * @param Target  start address of buffer to write to
 * @param Source  start address of buffer to read from
 * @param MaxSize no more than this amount of bytes will be copied (safety check)
 * @return        amount of bytes copied (+ zero byte)
 */
Base_t ttc_string_copy( u8_t* Target, const u8_t* Source, Base_t MaxSize );

/** Prints given string on stdout device.
 * Note: A stdout device has to be registered for an interface driver (e.g. ttc_usart) before!
 *       If no interface driver has been registered, then given string is silently ignored.
 *
 * @param String  constant text to output
 * @param MaxSize no more than this amount of bytes will be read from String[]
 */
void ttc_string_print( const u8_t* String, Base_t MaxSize );

/** printf compatible function which outputs to standard output device
 *
 * Note: Will allocate TTC_STRING_PRINTF_BUFFERSIZE bytes at first call.
 * Note: This function is very slow and will lock complete multitasking.
 * Note: You may use ttc_string_snprintf() on memory-blocks and pass these to your output device instead.
 *
 * @param Format   formatting string as described in manpage of standard printf
 * @param varargs  variable amount of arguments as defined in Format
 */
void ttc_string_printf( const char* Format, ... );

/** snprintf compatible function which writes a formatted string into given buffer
 *
 * Note: This function is reentrant and thread-safe.
 *
 * @param Buffer   string buffer where to write to
 * @param Size     maximum allowed amount of bytes to write into Buffer[]
 * @param Format   formatting string mostly compatible to standard printf() (-> man 3 printf)
 * @param varargs  variable amount of arguments as defined in Format
 * @return         pointer to first character in Buffer[] after written string
 */
u8_t* ttc_string_snprintf( u8_t* Buffer, Base_t Size, const char* Format, ... );
u8_t* _ttc_string_snprintf( u8_t* Buffer, Base_t Size, const char* Format, va_list Arguments );

/** function similar to snprintf() which appends formatted string to given buffer
 *
 * Note: This function is reentrant and thread-safe.
 *
 * @param Buffer   string buffer where to write to
 * @param Size     pointer to maximum allowed amount of bytes to write into Buffer[] (will be decreased by amount bytes appended)
 * @param Format   formatting string as described in manpage of standard printf
 * @param varargs  variable amount of arguments as defined in Format
 * @return         amount of bytes being appended to Buffer[] (not more than *Size)
 */
Base_t ttc_string_appendf( u8_t* Buffer, Base_t* Size, const char* Format, ... );

/** Prints string in given memory block on stdout device (zero-copy strategy).
 * Note: A stdout device has to be registered for an interface driver (e.g. ttc_usart) before!
 *       If no interface driver has been registered, then given string is silently ignored.
 * Note: Ownership of Block goes to ttc_string_print_block(). Do NOT access it anymore!
 *       After successfull transmission, block will be released to Block->releaseBuffer().
 *
 * @param Block   memory block containing string to print
 * @param MaxSize no more than this amount of bytes will be read from String[]
 */
void ttc_string_print_block( ttc_heap_block_t* Block );

/** Registers given function pointers for stdout output via ttc_string_print_block()/ ttc_string_print()/ ttc_string_printf().
 * @param StdoutBlock  function that will send out content of given memory block
 * @param StdoutString function that will send out content of given string buffer
 */
void ttc_string_register_stdout( void ( *StdoutBlock )( ttc_heap_block_t* ), void ( *StdoutString )( const u8_t*, Base_t ) );

/** writes ascii representation of given integer value into given buffer
 * @param   Value   signed integer value to convert
 * @param   Buffer  string buffer in which to write ascii representation
 * @param   MaxSize no more than this amount of bytes will be written into Buffer[]
 * @return          amount of bytes written to Buffer[]
 */
u8_t ttc_itoa( Base_signed_t Value, u8_t* Buffer, Base_t MaxSize );

/** writes ascii representation of given unsigned integer value into given buffer
 * @param   Value   signed integer value to convert
 * @param   Buffer  string buffer in which to write ascii representation
 * @param   MaxSize no more than this amount of bytes will be written into Buffer[]
 * @return          amount of bytes written to Buffer[]
 */
u8_t ttc_itoa_unsigned( Base_t Value, u8_t* Buffer, Base_t MaxSize );

/** writes hexadecimal representation of given integer value into given buffer
 * @param   Value   unsigned integer value to convert
 * @param   Buffer  string buffer in which to write ascii representation
 * @param   MaxSize no more than this amount of bytes will be written into Buffer[]
 * @return          amount of bytes written to Buffer[]
 */
u8_t ttc_xtoa( Base_t Value, u8_t* Buffer, Base_t MaxSize );

/** Fast converter for 8-bit unsigned values.
 * Writes two character hexadecimal representation of given byte value into given buffer
 */
void ttc_byte2hex( u8_t* Buffer, u8_t Byte );

/** Fast converter for 8-bit unsigned values
 * Writes up to three characters as decimal representation of given byte value into given buffer
 *
 * @param   Value   unsigned integer value to convert
 * @param   Buffer  string buffer in which to write ascii representation
 * @return          amount of bytes written to Buffer[]
 */
u8_t ttc_byte2uint( u8_t Value, u8_t* Buffer );

/** Fast converter for 8-bit signed values
 * Writes up to four characters as decimal representation of given byte value into given buffer
 *
 * @param   Value   signed integer value to convert
 * @param   Buffer  string buffer in which to write ascii representation
 * @return          amount of bytes written to Buffer[]
 */
u8_t ttc_byte2int( s8_t Value, u8_t* Buffer );

/** Calculate logarithmus of given value to base 10 or 16.
 *
 * This function is handy if you wanna know how much digits are required to code a given value
 * in hex or decimal representation.
 *
 * @param   Value   signed integer value to convert
 * @return          amount of digits required in decimal or hexadecimal representation
 */
u8_t ttc_string_log_decimal( Base_signed_t Value );
u8_t ttc_string_log_hex( Base_t Value );

/** Calculate length of given string as amount of characters until first zero byte or MaxLen reached.
 * This function can be used for strings of size up to 255 bytes.
 * Note: You may use the architecture aware macro strnlen() instead!
 *
 * @param String  points to buffer storing string to be read
 * @param MaxLen  safety value used to avoid running out of allocated memory in case of missing zero byte
 * @return length of string <= MaxLen
 */
u8_t ttc_string_length8( const char* String, u8_t MaxLen );

/** Calculate length of given string as amount of characters until first zero byte or MaxLen reached.
 * This function can be used for strings of size up to 65535 bytes.
 * Note: You may use the architecture aware macro strnlen() instead!
 *
 * @param String  points to buffer storing string to be read
 * @param MaxLen  safety value used to avoid running out of allocated memory in case of missing zero byte
 * @return length of string <= MaxLen
 */
u16_t ttc_string_length16( const char* String, u16_t MaxLen );

/** Calculate length of given string as amount of characters until first zero byte or MaxLen reached.
 * This function can be used for strings of size up to 4 GB (theoretically)
 * Note: You may use the architecture aware macro strnlen() instead!
 *
 * @param String  points to buffer storing string to be read
 * @param MaxLen  safety value used to avoid running out of allocated memory in case of missing zero byte
 * @return length of string <= MaxLen
 */
u32_t ttc_string_length32( const char* String, u32_t MaxLen );

//}Function prototypes
//{ private functions

/** delivers pointer to temporary string buffer used for printf() like functions
 *
 * Note: This function is private and should not be called from outside
 * Note: String buffer is allocated on first call
 *
 * @return         pointer to first character in Buffer[] after written string
 */
u8_t* _ttc_string_get_buffer();

/** obtain access to global static datafields used by ttc_string.c
  *
 * Note: This function is private and should not be called from outside
 * Note: Mutex is allocated and initialized on first call
 *
 */
void _ttc_string_lock();

//}PrivateFunctions
#endif
