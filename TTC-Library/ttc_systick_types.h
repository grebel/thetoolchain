/** { ttc_systick_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for SYSTICK device.
 *  Structures, Enums and Defines being required by both, high- and low-level systick.
 *  This file does not provide function declarations!
 *
 *  Created from template ttc_device_types.h revision 38 at 20160926 13:55:21 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_SYSTICK_TYPES_H
#define TTC_SYSTICK_TYPES_H

//{ Includes *************************************************************
//
// _types.h Header files mayshould include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_systick.h" or "ttc_systick.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_systick_cortexm3
    #include "systick/systick_cortexm3_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_systick_freertos
    #include "systick/systick_freertos_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

/** {  TTC_SYSTICKn has to be defined as constant by makefile.100_board_*
 *
 * Here we count how much devices have been defined and if all lower index devices have been defined too.
 */
#ifndef TTC_SYSTICK_AMOUNT //{ 10
    #ifdef TTC_SYSTICK10
        #ifndef TTC_SYSTICK9
            #      error TTC_SYSTICK10 is defined, but not TTC_SYSTICK9 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK8
            #      error TTC_SYSTICK10 is defined, but not TTC_SYSTICK8 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK7
            #      error TTC_SYSTICK10 is defined, but not TTC_SYSTICK7 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK6
            #      error TTC_SYSTICK10 is defined, but not TTC_SYSTICK6 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK5
            #      error TTC_SYSTICK10 is defined, but not TTC_SYSTICK5 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK4
            #      error TTC_SYSTICK10 is defined, but not TTC_SYSTICK4 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK3
            #      error TTC_SYSTICK10 is defined, but not TTC_SYSTICK3 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK2
            #      error TTC_SYSTICK10 is defined, but not TTC_SYSTICK2 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK1
            #      error TTC_SYSTICK10 is defined, but not TTC_SYSTICK1 - all lower TTC_SYSTICKn must be defined!
        #endif

        #define TTC_SYSTICK_AMOUNT 10
    #endif
#endif //}
#ifndef TTC_SYSTICK_AMOUNT //{ 9
    #ifdef TTC_SYSTICK9
        #ifndef TTC_SYSTICK8
            #      error TTC_SYSTICK9 is defined, but not TTC_SYSTICK8 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK7
            #      error TTC_SYSTICK9 is defined, but not TTC_SYSTICK7 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK6
            #      error TTC_SYSTICK9 is defined, but not TTC_SYSTICK6 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK5
            #      error TTC_SYSTICK9 is defined, but not TTC_SYSTICK5 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK4
            #      error TTC_SYSTICK9 is defined, but not TTC_SYSTICK4 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK3
            #      error TTC_SYSTICK9 is defined, but not TTC_SYSTICK3 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK2
            #      error TTC_SYSTICK9 is defined, but not TTC_SYSTICK2 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK1
            #      error TTC_SYSTICK9 is defined, but not TTC_SYSTICK1 - all lower TTC_SYSTICKn must be defined!
        #endif

        #define TTC_SYSTICK_AMOUNT 9
    #endif
#endif //}
#ifndef TTC_SYSTICK_AMOUNT //{ 8
    #ifdef TTC_SYSTICK8
        #ifndef TTC_SYSTICK7
            #      error TTC_SYSTICK8 is defined, but not TTC_SYSTICK7 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK6
            #      error TTC_SYSTICK8 is defined, but not TTC_SYSTICK6 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK5
            #      error TTC_SYSTICK8 is defined, but not TTC_SYSTICK5 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK4
            #      error TTC_SYSTICK8 is defined, but not TTC_SYSTICK4 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK3
            #      error TTC_SYSTICK8 is defined, but not TTC_SYSTICK3 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK2
            #      error TTC_SYSTICK8 is defined, but not TTC_SYSTICK2 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK1
            #      error TTC_SYSTICK8 is defined, but not TTC_SYSTICK1 - all lower TTC_SYSTICKn must be defined!
        #endif

        #define TTC_SYSTICK_AMOUNT 8
    #endif
#endif //}
#ifndef TTC_SYSTICK_AMOUNT //{ 7
    #ifdef TTC_SYSTICK7
        #ifndef TTC_SYSTICK6
            #      error TTC_SYSTICK7 is defined, but not TTC_SYSTICK6 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK5
            #      error TTC_SYSTICK7 is defined, but not TTC_SYSTICK5 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK4
            #      error TTC_SYSTICK7 is defined, but not TTC_SYSTICK4 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK3
            #      error TTC_SYSTICK7 is defined, but not TTC_SYSTICK3 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK2
            #      error TTC_SYSTICK7 is defined, but not TTC_SYSTICK2 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK1
            #      error TTC_SYSTICK7 is defined, but not TTC_SYSTICK1 - all lower TTC_SYSTICKn must be defined!
        #endif

        #define TTC_SYSTICK_AMOUNT 7
    #endif
#endif //}
#ifndef TTC_SYSTICK_AMOUNT //{ 6
    #ifdef TTC_SYSTICK6
        #ifndef TTC_SYSTICK5
            #      error TTC_SYSTICK6 is defined, but not TTC_SYSTICK5 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK4
            #      error TTC_SYSTICK6 is defined, but not TTC_SYSTICK4 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK3
            #      error TTC_SYSTICK6 is defined, but not TTC_SYSTICK3 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK2
            #      error TTC_SYSTICK6 is defined, but not TTC_SYSTICK2 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK1
            #      error TTC_SYSTICK6 is defined, but not TTC_SYSTICK1 - all lower TTC_SYSTICKn must be defined!
        #endif

        #define TTC_SYSTICK_AMOUNT 6
    #endif
#endif //}
#ifndef TTC_SYSTICK_AMOUNT //{ 5
    #ifdef TTC_SYSTICK5
        #ifndef TTC_SYSTICK4
            #      error TTC_SYSTICK5 is defined, but not TTC_SYSTICK4 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK3
            #      error TTC_SYSTICK5 is defined, but not TTC_SYSTICK3 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK2
            #      error TTC_SYSTICK5 is defined, but not TTC_SYSTICK2 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK1
            #      error TTC_SYSTICK5 is defined, but not TTC_SYSTICK1 - all lower TTC_SYSTICKn must be defined!
        #endif

        #define TTC_SYSTICK_AMOUNT 5
    #endif
#endif //}
#ifndef TTC_SYSTICK_AMOUNT //{ 4
    #ifdef TTC_SYSTICK4
        #ifndef TTC_SYSTICK3
            #      error TTC_SYSTICK4 is defined, but not TTC_SYSTICK3 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK2
            #      error TTC_SYSTICK4 is defined, but not TTC_SYSTICK2 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK1
            #      error TTC_SYSTICK4 is defined, but not TTC_SYSTICK1 - all lower TTC_SYSTICKn must be defined!
        #endif

        #define TTC_SYSTICK_AMOUNT 4
    #endif
#endif //}
#ifndef TTC_SYSTICK_AMOUNT //{ 3
    #ifdef TTC_SYSTICK3

        #ifndef TTC_SYSTICK2
            #      error TTC_SYSTICK3 is defined, but not TTC_SYSTICK2 - all lower TTC_SYSTICKn must be defined!
        #endif
        #ifndef TTC_SYSTICK1
            #      error TTC_SYSTICK3 is defined, but not TTC_SYSTICK1 - all lower TTC_SYSTICKn must be defined!
        #endif

        #define TTC_SYSTICK_AMOUNT 3
    #endif
#endif //}
#ifndef TTC_SYSTICK_AMOUNT //{ 2
    #ifdef TTC_SYSTICK2

        #ifndef TTC_SYSTICK1
            #      error TTC_SYSTICK2 is defined, but not TTC_SYSTICK1 - all lower TTC_SYSTICKn must be defined!
        #endif

        #define TTC_SYSTICK_AMOUNT 2
    #endif
#endif //}
#ifndef TTC_SYSTICK_AMOUNT //{ 1
    #ifdef TTC_SYSTICK1
        #define TTC_SYSTICK_AMOUNT 1
    #endif
#endif //}

//}
/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_SYSTICK 0#         disable default asserts for systick driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_SYSTICK_EXTRA 1#   enable extra asserts for systick driver
 *
 */
#ifndef TTC_ASSERT_SYSTICK    // any previous definition set (Makefile)?
    #define TTC_ASSERT_SYSTICK 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_SYSTICK == 1)  // use Assert()s in SYSTICK code (somewhat slower but alot easier to debug)
    #define Assert_SYSTICK(Condition, Origin)        if (! (Condition) ) { ttc_assert_halt_origin(Origin); }
    #define Assert_SYSTICK_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_SYSTICK_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in SYSTICK code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_SYSTICK(Condition, Origin)
    #define Assert_SYSTICK_Writable(Address, Origin)
    #define Assert_SYSTICK_Readable(Address, Origin)
#endif

#ifndef TTC_ASSERT_SYSTICK_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_SYSTICK_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_SYSTICK_EXTRA == 1)  // use Assert()s in SYSTICK code (somewhat slower but alot easier to debug)
    #define Assert_SYSTICK_EXTRA(Condition, Origin)        if (! (Condition) ) { ttc_assert_halt_origin(Origin); }
    #define Assert_SYSTICK_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_SYSTICK_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in SYSTICK code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_SYSTICK_EXTRA(Condition, Origin)
    #define Assert_SYSTICK_EXTRA_Writable(Address, Origin)
    #define Assert_SYSTICK_EXTRA_Readable(Address, Origin)
#endif
//}
/** { Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_systick_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_systick_architecture
    #  warning Missing low-level definition for t_ttc_systick_architecture (using default)
    #define t_ttc_systick_architecture void
#endif
//}
/** { Check static configuration of systick devices
 *
 * Each TTC_SYSTICKn must be defined as one from e_ttc_systick_architecture.
 * Additional defines of type TTC_SYSTICKn_* may be added.
 *
 * The constants being checked here must be defined in your makefile.
 * Most constants are defined by EXTENSION_100_board_* or EXTENSION_150_board_extension_* files.
 */

#ifdef TTC_SYSTICK1

    // check extra defines for systick device #1 here

#endif
#ifdef TTC_SYSTICK2

    // check extra defines for systick device #2 here

#endif
#ifdef TTC_SYSTICK3

    // check extra defines for systick device #3 here

#endif
#ifdef TTC_SYSTICK4

    // check extra defines for systick device #4 here

#endif
#ifdef TTC_SYSTICK5

    // check extra defines for systick device #5 here

#endif
#ifdef TTC_SYSTICK6

    // check extra defines for systick device #6 here

#endif
#ifdef TTC_SYSTICK7

    // check extra defines for systick device #7 here

#endif
#ifdef TTC_SYSTICK8

    // check extra defines for systick device #8 here

#endif
#ifdef TTC_SYSTICK9

    // check extra defines for systick device #9 here

#endif
#ifdef TTC_SYSTICK10

    // check extra defines for systick device #10 here

#endif
//}

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_systick_errorcode       return codes of SYSTICK devices
    ec_systick_OK = 0,

    // other warnings go here..

    ec_systick_ERROR,                  // general failure
    ec_systick_NULL,                   // NULL pointer not accepted
    ec_systick_DeviceNotFound,         // corresponding device could not be found
    ec_systick_InvalidConfiguration,   // sanity check of device configuration failed
    ec_systick_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..

    ec_systick_unknown                // no valid errorcodes past this entry
} e_ttc_systick_errorcode;
typedef enum {   // e_ttc_systick_architecture    types of architectures supported by SYSTICK driver
    ta_systick_None = 0,       // no architecture selected


    ta_systick_cortexm3, // automatically added by ./create_DeviceDriver.pl
    ta_systick_freertos, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_systick_unknown        // architecture not supported
} e_ttc_systick_architecture;
//InsertEnums above (DO NOT REMOVE THIS LINE!)
typedef enum {   //
    profile_systick_None,
    profile_systick_Default,  // compromise between interrupt overhead and timing precision
    profile_systick_Precise,  // high systick frequency for small MinDelayTime (increased interrupt load will decrease processing performance)
    profile_systick_Relaxed,  // low systick frequency for maximum processing performance (increases MinDelayTime)
    profile_systick_unknown
} e_ttc_systick_profile;
typedef struct { // data of delay to be used with ttc_systick_delay_() functions
    t_base TimeStart;  // ticks of time when delay has been started
    t_base TimeEnd;    // ticks of time when delay will expire
} t_ttc_systick_delay;

typedef union {  // architecture dependent configuration

#ifdef EXTENSION_systick_cortexm3
    t_systick_cortexm3_config cortexm3;  // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_systick_freertos
    t_systick_freertos_config freertos;  // automatically added by ./create_DeviceDriver.pl
#endif
    //InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_systick_architecture;
typedef struct s_ttc_systick_features { // static minimum, maximum and default values of features of single systick

    // Add any amount of architecture independent values describing systick devices here.
    // You may also want to add a plausibility check for each value to _ttc_systick_configuration_check().

    /** Example Features

    t_u16 MinBitRate;   // minimum allowed transfer speed
    t_u16 MaxBitRate;   // maximum allowed transfer speed
    */

    const t_u8 unused; // remove me!

} __attribute__( ( __packed__ ) ) t_ttc_systick_features;

typedef struct s_ttc_systick_config { //          architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_systick_init()
    //       and after ttc_systick_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // Hint: Order structure elements for optimal alignment.
    //       You should place smaller datatypes like t_u8 and t_u16 after all t_u32 and pointer types
    //       to prevent insertion of pad-bytes. Most architectures don't support placing 32-bit datatypes at
    //       unaligned address.

    // Fields to be set by application before first calling ttc_systick_init() --------------
    // Do not change these values after calling ttc_systick_init()!

    // Application configurable fields
    // Call ttc_systick_init() again after changing!
    e_ttc_systick_profile       Profile;        // Select basic systick configuration profile

    // Fields below are read-only for application! ---------------------------------------

    t_u32 NanoSecondsPerSysTick;                // amount of nanoseconds of each system counter tick
    t_u32 MinimumDelayUS;                       // minimum available delay time (microseconds)   when using ttc_systick_delay_init*() functions
    t_u32 RecommendedDelayUS;                   // recommended minimum delay time (microseconds) when using ttc_systick_delay_init*() functions

    u_ttc_systick_architecture* LowLevelConfig; // low-level configuration (structure defined by low-level driver)

    const t_ttc_systick_features* Features;     // constant features of this systick

    union  { // generic configuration bits common for all low-level Drivers
        t_u8 All;
        struct {
            unsigned Initialized      : 1;  // ==1: device has been initialized successfully
            // ToDo: additional high-level flags go here..

            unsigned Reserved1        : 7; // pad to 8 bits
        } Bits;
    } Flags;

    // Last returned error code
    // IMPORTANT: Every ttc_systick_*() function that returns e_ttc_systick_errorcode has to update this value if it returns an error!
    e_ttc_systick_errorcode LastError;

    t_u8  LogicalIndex;                          // automatically set: logical index of device to use (1 = TTC_SYSTICK1, ...)
    t_u8  PhysicalIndex;                         // automatically set: index of physical device to use (0 = systick #1, ...)
    e_ttc_systick_architecture  Architecture;   // type of architecture used for current systick device

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_systick_config;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_SYSTICK_TYPES_H
