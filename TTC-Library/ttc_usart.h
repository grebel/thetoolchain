/** { ttc_usart.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for USART devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_USART(tc_usart_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_usart_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_usart_init(LogicalIndex);
 *  4) use:         ttc_usart_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level usart and application.
 *
 *  Created from template ttc_device.h revision 23 at 20140217 09:36:59 UTC
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of ttc_usart (Do not delete this line!)
 *
 * A Universal Synchronous and Asynchronous Receiver and Transmitter (USART) is the
 * swiss army knife in microcontroller communication. It is the most flexible communication
 * interface of modern microcontrollers. With disabled synchronous feature, a UART can send data
 * at low data rates (<= 115300 bit/s) over a single pin. In synchronous mode with enabled flow-control
 * pins a USART can transfer data at 2 Mbit/s.
 *
 * H2: High-Level Features
 *
 * ttc_usart provides a convenient serial communication interface with several high-level features
 * that keeps low-level drivers small and easy to implement:
 * - Send fixed amount of raw bytes.
 * - Send zero terminated ASCII strings.
 * - Send data from constant memory areas with minimal copying.
 * - Send memory blocks of raw bytes with automatic buffer release.
 * - Interrupt driven send and receive.
 * - Automatic transmit and receive queues.
 * - Usable as standard output for ttc_string_printf() (also available as printf() function)
 *
 * H2: Configure USART Devices
 *
 * ttc_usart devices are enumerated by their logical index 1..ttc_usart_get_max_index().
 * Each logical index <n> is configured by static defines named TTC_USART<n>_* in the currently activated
 * board makefile. When writing your own board makefile, it should be easy to understand the syntax from
 * existing board makefiles. Also take a look at ttc_usart_types.h which checks all
 * TTC_USART* defines and generates compiler errors if required.
 *
}*/

#ifndef TTC_USART_H
#define TTC_USART_H

#ifndef EXTENSION_ttc_usart
    #  error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_usart.sh
#endif

//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "ttc_interrupt.h"
#include "interfaces/ttc_usart_interface.h"
#include "interfaces/ttc_usart_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level usart only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * usart devices on all supported architectures.
 * Check usart/usart_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares usart Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_usart_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_usart_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_USART_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_usart_config* ttc_usart_get_configuration( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_USART_get_max_LogicalIndex())
 * @return              == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_usart_errorcode ttc_usart_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_USART_get_max_LogicalIndex())
 */
void ttc_usart_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Seetings of indexed device are being reset to their default values.
 * Will automatically call ttc_usart_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of usart device (1..ttc_usart_get_max_index() )
 */
void ttc_usart_load_defaults( t_u8 LogicalIndex );

/** maps from logical to physical device index
 *
 * High-level usarts (ttc_usart_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_USARTn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any
 * order.
 *
 * @param LogicalIndex  logical index of usart device (1..ttc_usart_get_max_index() )
 * @return              physical index of usart device (0 = first physical usart device, ...)
 */
t_u8 ttc_usart_logical_2_physical_index( t_u8 LogicalIndex );

/** maps from physical to logical device index
 *
 * This is a reverse function to ttc_usart_logical_2_physical_index()
 *
 * @param  PhysicalIndex  physical index of usart device (0 = first physical usart device, ...)
 * @return                logical index of usart device (1..ttc_usart_get_max_index() )
 */
t_u8 ttc_usart_physical_2_logical_index( t_u8 PhysicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_USART_get_max_LogicalIndex())
 * @return              == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_usart_errorcode ttc_usart_reset( t_u8 LogicalIndex );

/** Prepares USART-interface for operation. Must be called before any other function!
 */
void ttc_usart_prepare();

/** returns amount of USART devices available on current uC
 * @return amount of available USART devices
 */
t_u8 ttc_usart_get_max_logicalindex();

/** returns configuration of indexed usart (asserts if no valid configuration was read)
 *
 * @param LogicalIndex    device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @return                !=NULL: pointer to struct t_ttc_usart_config
 */
t_ttc_usart_config* ttc_usart_get_configuration( t_u8 LogicalIndex ) ;

/** fills out given USART_Generic with default values for indexed USART
 * @param LogicalIndex    device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @return  == 0:         *USART_Generic has been initialized successfully; != 0: error-code
 */
e_ttc_usart_errorcode ttc_usart_reset( t_u8 LogicalIndex );

/** fills out given USART_Generic with maximum valid values for indexed USART
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Config         USART configuration as returned by ttc_usart_get_configuration()
 * @return  == 0:        *USART_Generic has been initialized successfully; != 0: error-code
 */
e_ttc_usart_errorcode ttc_usart_get_features( t_u8 LogicalIndex, t_ttc_usart_config* Config );

/** delivers a memory block usable as transmit or receive buffer
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @return               != NULL: pointer to new or reused memory block; == NULL: currently no tx-buffer available (-> increase USART_Generic->Size_Queue_Tx or wait until tx-buffer is released)
 */
t_ttc_heap_block* ttc_usart_get_empty_block();

/** initializes single USART with current configuration
 * @param LogicalIndex    device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @return                == 0: USART has been initialized successfully; != 0: error-code
 */
e_ttc_usart_errorcode ttc_usart_init( t_u8 LogicalIndex );

/** registers given function as a receiver for incoming data
 *
 * Note: This function will automatically call ttc_usart_init() to init/reinit USART
 *
 * @param LogicalIndex    device index of USART to use (1..ttc_usart_get_max_logicalindex())
 * @param receiveBlock    function to call for each incoming data block (will contain one or more bytes)
 * @param Argument        will be passed as first argument to receiveBlock()
 * @return                == 0: USART has been initialized successfully; != 0: error-code
 */
e_ttc_usart_errorcode ttc_usart_register_receive( t_u8 LogicalIndex, t_ttc_heap_block * ( *receiveBlock )( void* Argument, t_ttc_usart_config* Config, t_ttc_heap_block* Block ), void* Argument );

/** Stores given raw buffer to be send in parallel. (Function copies data and returns immediately)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 *
 * @param LogicalIndex     device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Buffer           memory location from which to read data
 * @param Amount           amount of bytes to send from Buffer[] (even zero bytes are sent)
 * @return                 == 0: Buffer has been stored for delivery successfully; != 0: error-code
 */
e_ttc_usart_errorcode ttc_usart_send_raw( t_u8 LogicalIndex, t_u8* Buffer, t_base Amount );

/** Stores given raw buffer to be send in parallel. (Function stores pointer to data and returns immediately)
 *
 * This function is especially optimized for constant data (e.g. strings in ROM) and avoids extra copying into buffers.
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 *
 * @param LogicalIndex     device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Buffer           memory location from which to read data (must not change!)
 * @param Amount           amount of bytes to send from Buffer[] (even zero bytes are sent)
 * @return                 == 0: Buffer has been stored for delivery successfully; != 0: error-code
 */
e_ttc_usart_errorcode ttc_usart_send_raw_const( t_u8 LogicalIndex, t_u8* Buffer, t_base Amount );

/** Stores given string buffer to be send in parallel. (Function copies data and returns immediately)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 *
 * @param LogicalIndex     device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Buffer           memory location from which to read data
 * @param MaxLength        no more than this amount of bytes will be send
 * @return                 == 0: Buffer has been stored for delivery successfully; != 0: error-code
 */
e_ttc_usart_errorcode ttc_usart_send_string( t_u8 LogicalIndex, const void* Buffer, t_base MaxLength );

/** Stores given string buffer to be send in parallel. (Function stores pointer to data and returns immediately)
 *
 * This function is especially optimized for constant data (e.g. strings in ROM) and avoids extra copying into buffers.
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 *
 * @param LogicalIndex     device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Buffer           memory location from which to read data
 * @param MaxLength        no more than this amount of bytes will be send
 * @return                 == 0: Buffer has been stored for delivery successfully; != 0: error-code
 */
e_ttc_usart_errorcode ttc_usart_send_string_const( t_u8 LogicalIndex, const char* Buffer, t_base MaxLength );

/** Sends out data from given memory block in parallel. This function copies no data and has least overhead of all delayed functions.
 * After all data from Block has been transmitted, the Block is appended to an internal queue.
 * Use ttc_usart_get_tx_block() to reuse the block from this queue.
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Block          stores data to be send; It is mandatory to allocate Block with ttc_usart_get_tx_block()!
 * @return               == 0: Buffer has been stored for delivery successfully; != 0: error-code
 */
e_ttc_usart_errorcode ttc_usart_send_block( t_u8 LogicalIndex, t_ttc_heap_block* Block );

/** Waits until transmit queue has run empty on given USART
 *
 * Note: USART must be initialized before!
 * Note: This function blocks until all bytes have been sent!
 * Note: You may want to call ttc_task_critical_begin() before to be sure that no other task transmits in parallel.
 *
 * @param LogicalIndex      device index of USART to init (1..ttc_usart_get_max_logicalindex())
 */
void ttc_usart_flush_tx( t_u8 LogicalIndex );

/** Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Word           pointer to 16 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_usart_errorcode ttc_usart_read_word( t_u8 LogicalIndex, t_u16* Word );

/** returns amount of bytes being held in receive buffer
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Config         USART configuration as returned by ttc_usart_get_configuration()
 * @return               == 0: no bytes available; >0: amount of bytes available
 */
t_u8 ttc_usart_check_bytes_available( t_u8 LogicalIndex );
t_u8 _driver_usart_check_bytes_available( t_ttc_usart_config* Config );

/** Reads single data byte from input buffer.
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Word           pointer to 16 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_usart_errorcode ttc_usart_read_byte( t_u8 LogicalIndex, t_u8* Byte );

/** Adds 1 byte to receive queue. Called from low level USART interrupt service routine
 * Note: This function is private and should not be called from outside.
 *
 * @param Config         USART configuration as returned by ttc_usart_get_configuration()
 * @param Byte           received byte to add to receive buffer
 */
void _ttc_usart_rx_isr( t_ttc_usart_config* Config, t_u8 Byte );

/** Will send out next byte from transmit queue.
 * Note: This function is private and should not be called from outside.
 *
 * @param Config         USART configuration as returned by ttc_usart_get_configuration()
 * @param USART_Hardware low-level pointer required for fast low-level transmit (passed to _driver_usart_send_byte_isr() )
 * @return               !=0: still bytes in transmit buffer (low-level driver may stay in interrupt service routine for high-data rates=
 */
BOOL _ttc_usart_tx_isr( t_ttc_usart_config* Config, volatile void* USART_Hardware );

/** translates from logical index of functional unit to real hardware index (0=USART1, 1=USART2, ...)
 * @param LogicalIndex    logical device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @return                real device index of USART to init (0..MAX)
 */
t_u8 ttc_usart_get_physicalindex( t_u8 LogicalIndex );

/** Defines which logical device shall be used by ttc_usart_stdout_send_string()/ ttc_usart_stdout_send_block()
 *
 * @param LogicalIndex   >0: logical device index of USART (must be initialized before!); ==0: disable default USART
 * @return               != 0: error message
 */
e_ttc_usart_errorcode ttc_usart_stdout_set( t_u8 LogicalIndex );

/** will send given memory block to default USART being set by ttc_usart_stdout_set()
 *
 * @param Block         pointer to a t_ttc_heap_block (will be released after sending automatically)
 */
void ttc_usart_stdout_send_block( t_ttc_heap_block* Block );

/** will send given string to default USART being set by ttc_usart_stdout_set()
 *
 * @param Block         pointer to a t_ttc_heap_block (will be released after sending automatically)
 */
void ttc_usart_stdout_send_string( const t_u8* String, t_base MaxSize );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_usart(t_u8 LogicalIndex)

/** Send out given amount of raw bytes from buffer.
 * Will not interpret bytes in buffer.
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Buffer         memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[]
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_usart_errorcode _ttc_usart_send_raw_blocking( t_u8 LogicalIndex, const t_u8* Buffer, t_u16 Amount );

/** Send out given bytes from buffer until first zero byte or MaxLength reached.
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Buffer         memory location from which to read data
 * @param MaxLength      no more than this amount of bytes will be send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_usart_errorcode _ttc_usart_send_string_blocking( t_u8 LogicalIndex, const char* Buffer, t_u16 MaxLength );

/** Send out given data word (8 or 9 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until bytes has been sent!
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @param Byte           8 or 9 bit value to send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_usart_errorcode _ttc_usart_send_word_blocking( t_u8 LogicalIndex, const t_u16 Word );

/** Implements asynchronous receiver. Reads data from one receive queue.
 * @param Args  t_ttc_usart_config* pointer to data of configured USART
 */
void _ttc_usart_task_receive( void* Args );

/** Implements asynchronous transmitter. Reads data from one transmit queue.
 * @param Args  t_ttc_usart_config* pointing to data of configured USART
 */
void _ttc_usart_task_transmit( void* Args );

/** releases a used Tx/ Rx memory blocks for later reuse
 * @param Block         pointer to a t_ttc_heap_block
 */
void _ttc_usart_release_block( t_ttc_heap_block* Block );

/** releases a used Tx/ Rx memory blocks for later reuse (will not block)
 * @param Block         pointer to a t_ttc_heap_block
 */
void _ttc_usart_release_block_isr( t_ttc_heap_block* Block );

/** returns configuration of indexed usart (allocates + resets new configuration if necessary)
 * for internal use only
 * @param LogicalIndex    device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @return                !=NULL: pointer to struct t_ttc_usart_config
 */
t_ttc_usart_config* _ttc_usart_get_configuration( t_u8 LogicalIndex );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_heap_ are passed to interfaces/ttc_heap_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl heap UPDATE
 */

/** Prepares usart driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void _driver_usart_prepare();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param Config         USART configuration as returned by ttc_usart_get_configuration()
 * @return               configuration of indexed device
 */
t_ttc_usart_config* _driver_usart_get_configuration( t_ttc_usart_config* Config );

/** fills out given Config with maximum valid values for indexed USART
 * @param Config         USART configuration as returned by ttc_usart_get_configuration()
 * @return  == 0:        *Config has been initialized successfully; != 0: error-code
 */
e_ttc_usart_errorcode _driver_usart_get_features( t_ttc_usart_config* Config );

/** shutdown single USART unit device
 * @param Config         USART configuration as returned by ttc_usart_get_configuration()
 * @return               == 0: USART has been shutdown successfully; != 0: error-code
 */
e_ttc_usart_errorcode _driver_usart_deinit( t_ttc_usart_config* Config );

/** initializes single USART unit for operation
 * @param Config         USART configuration as returned by ttc_usart_get_configuration()
 * @return               == 0: USART has been initialized successfully; != 0: error-code
 */
e_ttc_usart_errorcode _driver_usart_init( t_ttc_usart_config* Config );

/** loads configuration of indexed USART unit with default values
 * @param Config         USART configuration as returned by ttc_usart_get_configuration()
 * @return  == 0:        configuration was loaded successfully
 */
e_ttc_usart_errorcode _driver_usart_load_defaults( t_ttc_usart_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config         USART configuration as returned by ttc_usart_get_configuration()
 * @return               == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_usart_errorcode _driver_usart_reset( t_ttc_usart_config* Config );

/** Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param Register      pointer to low-level register for fast hardware operation
 * @param Word           pointer to 16 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_usart_errorcode _driver_usart_read_word_blocking( volatile t_ttc_usart_register* Register, t_u16* Word );

/** Reads single data byte from input buffer (8 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param Register      pointer to low-level register for fast hardware operation
 * @param Byte          pointer to 8 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_usart_errorcode _driver_usart_read_byte( volatile t_ttc_usart_register* Register, t_u8* Byte );

/** Send single data word to output buffer (8 or 9 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until word has been sent or timeout occurs
 *
 * @param Register      pointer to low-level register for fast hardware operation
 * @param Word          pointer to 16 bit buffer where to store Word
 * @return              == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_usart_errorcode _driver_usart_send_word_blocking( volatile t_ttc_usart_register* Register, const t_u16 Word );

/** low level send routine especially to be called from _ttc_usart_tx_isr()
 *
 * @param Register       pointer to hardware register for fast access
 * @param Word           data word to send (8 or 9 bits)
 */
//void _driver_usart_send_byte_isr( volatile t_ttc_usart_register_tx* Register, const t_u16 Word );

/** Reinitializes all already initialized USARTs to reflect a changed system clock.
 *
 * Note: This function is normally only called from ttc_sysclock facility when system clock has changed.
 *
 */
void _ttc_usart_update_to_sysclock();

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)


//}

#endif //TTC_USART_H
