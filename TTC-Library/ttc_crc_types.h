/** { ttc_crc_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for CRC device.
 *  Structures, Enums and Defines being required by both, high- and low-level crc.
 *  This file does not provide function declarations.
 *
 *  Note: See ttc_crc.h for description of high-level crc implementation!
 *
 *  Created from template ttc_device_types.h revision 45 at 20180323 10:28:15 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_CRC_TYPES_H
#define TTC_CRC_TYPES_H

//{ Includes1 ************************************************************
//
// Includes being required for static configuration.
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_crc.h" or "ttc_crc.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h"

//} Includes
/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_CRC 0#         disable default asserts for crc driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_CRC_EXTRA 1#   enable extra asserts for crc driver
 *
 * When debugging code that has been compiled with optimization (gcc option -O) then
 * certain variables may be optimized away. Declaring variables as VOLATILE_CRC makes them
 * visible in debug session when asserts are enabled. You have no overhead at all if asserts are disabled.
 + foot_t* VOLATILE_CRC VisiblePointer;
 *
 */
#ifndef TTC_ASSERT_CRC    // any previous definition set (Makefile)?
    #define TTC_ASSERT_CRC 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_CRC == 1)  // use Assert()s in CRC code (somewhat slower but alot easier to debug)
    #define Assert_CRC(Condition, Origin)        Assert(Condition, Origin)
    #define Assert_CRC_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_CRC_Readable(Address, Origin) Assert_Readable(Address, Origin)
    #define VOLATILE_CRC                         volatile
#else  // use no default Assert()s in CRC code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_CRC(Condition, Origin)
    #define Assert_CRC_Writable(Address, Origin)
    #define Assert_CRC_Readable(Address, Origin)
    #define VOLATILE_CRC
#endif

#ifndef TTC_ASSERT_CRC_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_CRC_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_CRC_EXTRA == 1)  // use Assert()s in CRC code (somewhat slower but alot easier to debug)
    #define Assert_CRC_EXTRA(Condition, Origin) Assert(Condition, Origin)
    #define Assert_CRC_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_CRC_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in CRC code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_CRC_EXTRA(Condition, Origin)
    #define Assert_CRC_EXTRA_Writable(Address, Origin)
    #define Assert_CRC_EXTRA_Readable(Address, Origin)
#endif
//}

//}Static Configuration

//{ Includes2 ************************************************************
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_crc.h" or "ttc_crc.c"
//

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_crc_small
    #include "crc/crc_small_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_crc_small
    #include "crc/crc_small_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_crc_fast
    #include "crc/crc_fast_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_crc_small
    #include "crc/crc_small_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

/** { Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_crc_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_crc_architecture
    #  warning Missing low-level definition for t_ttc_crc_architecture (using default)
    #define t_ttc_crc_architecture void
#endif
//}

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_crc_errorcode       return codes of CRC devices
    E_ttc_crc_errorcode_OK = 0,

    // other warnings go here..

    E_ttc_crc_errorcode_ERROR,                  // general failure
    E_ttc_crc_errorcode_NULL,                   // NULL pointer not accepted
    E_ttc_crc_errorcode_DeviceNotFound,         // corresponding device could not be found
    E_ttc_crc_errorcode_InvalidConfiguration,   // sanity check of device configuration failed
    E_ttc_crc_errorcode_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..

    E_ttc_crc_errorcode_unknown                // no valid errorcodes past this entry
} e_ttc_crc_errorcode;
typedef enum {   // e_ttc_crc_architecture    types of architectures supported by CRC driver
    E_ttc_crc_architecture_None = 0,       // no architecture selected


    E_ttc_crc_architecture_small, // automatically added by ./create_DeviceDriver.pl
    E_ttc_crc_architecture_fast, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    E_ttc_crc_architecture_unknown        // architecture not supported
} e_ttc_crc_architecture;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef union {  // architecture dependent configuration

#ifdef EXTENSION_crc_small
    t_crc_small_config small;  // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_crc_fast
    t_crc_fast_config fast;  // automatically added by ./create_DeviceDriver.pl
#endif
    //InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_crc_architecture;
typedef struct s_ttc_crc_features { // static minimum, maximum and default values of features of single crc

    // Add any amount of architecture independent values describing crc devices here.
    // You may also want to add a plausibility check for each value to _ttc_crc_configuration_check().

    /** Example Features

    t_u16 MinBitRate;   // minimum allowed transfer speed
    t_u16 MaxBitRate;   // maximum allowed transfer speed
    */

    const t_u8 unused; // remove me!

} __attribute__( ( __packed__ ) ) t_ttc_crc_features;
typedef struct s_ttc_crc_config { //          architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_crc_init()
    //       and after ttc_crc_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // Hint: Order structure elements for optimal alignment.
    //       You should place smaller datatypes like t_u8 and t_u16 after all t_u32 and pointer types
    //       to prevent insertion of pad-bytes. Most architectures don't support placing 32-bit datatypes at
    //       unaligned address.

    struct { // Init: fields to be set by application before first calling ttc_crc_init() --------------
        // Do not change these values after calling ttc_crc_init()!

        //...

        struct { // generic initial configuration bits common for all low-level Drivers
            unsigned unused : 1;  // ==1:
            // ToDo: additional high-level flags go here..
        } Flags;
    } Init;

    // Fields below are read-only for application! ----------------------------------------------------------

    u_ttc_crc_architecture* LowLevelConfig; // low-level configuration (structure defined by low-level driver)

    const t_ttc_crc_features* Features;     // constant features of this crc

    struct { // status flags common for all architectures
        unsigned Initialized        : 1;  // ==1: device has been initialized successfully
        // ToDo: additional high-level flags go here..
    } Flags;

    e_ttc_crc_architecture  Architecture;   // type of architecture used for current crc device

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_crc_config;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_CRC_TYPES_H
