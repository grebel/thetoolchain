/** { ttc_interrupt_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for INTERRUPT device.
 *  Structures, Enums and Defines being required by both, high- and low-level interrupt.
 *
 *  Created from template ttc_device_types.h revision 22 at 20140224 21:37:20 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_INTERRUPT_TYPES_H
#define TTC_INTERRUPT_TYPES_H

// datatype used to enumerate physical interrupt devices (E.g. USART1, USART2, ...)
#define t_physical_index t_base

// handle used to identify an initialized interrupt
typedef void* t_ttc_interrupt_handle;

//{ Enums ****************************************************************
typedef enum {   // e_ttc_interrupt_errorcode
    ec_interrupt_OK,                    // =0: no error
    ec_interrupt_NotEgnoughResources,   // out of interrupt lines for requested source
    ec_interrupt_TimeOut,               // timeout occured in called function
    ec_interrupt_IrqSourceNotFound,     // adressed interrupt source not available in current uC
    ec_interrupt_NotInitialized,        // an interrupt has occured that was not initialized before
    ec_interrupt_NotImplemented,        // function has no implementation for current architecture
    ec_interrupt_InvalidArgument,       // general argument error
    ec_interrupt_InvalidImplementation, // Your code does not behave as expected

    ec_interrupt_unknownError
} e_ttc_interrupt_errorcode;
typedef enum { // e_ttc_interrupt_type (not all sources listed here may be supported by current architecture driver)
    tit_None,

    // Note: do not change order of following lines!

    tit_First_GPIO,   //{ interrupt type for general purpose IO

    // interrupt types supported for GPIO pins
    // Note: PhysicalIndex = ttc_interrupt_gpio_create_index8(GPIOx, Pin)
    // Note: Don't forget to configure your port pin as input!
    tit_GPIO_Falling,            // voltage on input pin falls from logical 1 to 0
    tit_GPIO_Rising,             // voltage on input pin rises from logical 0 to 1
    tit_GPIO_Rising_Falling,     // voltage on input pin falls or rises
    tit_GPIO_Active_High,        // voltage on input pin is high
    tit_GPIO_Active_Low,         // voltage on input pin is low

    tit_Last_GPIO, //}
    tit_First_I2C,    //{ interrupt types for I2C-bus

    tit_I2C_StartBit,
    tit_I2C_AddrSent,
    tit_I2C_10_HDR,
    tit_I2C_Stop,
    tit_I2C_DB_TF,
    tit_I2C_RxNE,
    tit_I2C_TxE,
    tit_I2C_BusError,
    tit_I2C_ArbitrationLoss,
    tit_I2C_AckFail,
    tit_I2C_Overrun,
    tit_I2C_PECError,
    tit_I2C_Timeout,
    tit_I2C_SMBUSAlert,

    tit_Last_I2C, //}
    tit_First_TIMER,  //{ interrupt types for timers

    tit_TIMER_Updating,         // Hardware Timer expired, update needed
    tit_TIMER_CC1,              // Channel 1 Capture/Compare Flag
    tit_TIMER_CC2,              // Channel 2 Capture/Compare Flag
    tit_TIMER_CC3,              // Channel 3 Capture/Compare Flag
    tit_TIMER_CC4,              // Channel 4 Capture/Compare Flag
    tit_TIMER_Triggering,       // Trigger Interrupt Flag
    tit_TIMER_Break,            // Hardware break required

    tit_Last_TIMER, //}
    tit_First_RADIO,  //{ interrupt types for external or internal radio transceivers

    tit_Last_RADIO, //}
    tit_First_SPI,   //{ interrupt types for SPI bus

    tit_Last_SPI, //}
    tit_First_USART, //{ interrupt types for universal serial asynchronous receiver transmitter

    tit_USART_All,               // apply to all USART interrupts (only for disabling!)
    tit_USART_Cts,               // Cts signal has been received
    tit_USART_Error,             // error condition has arised (detailed data is handed to error handler)
    tit_USART_Idle,              // deivce has gone idle
    tit_USART_TxComplete,        // transfer completed
    tit_USART_TransmitDataEmpty, // transmit buffer ist empty ( bytes sent)
    tit_USART_RxNE,              // receive buffer not empty  ( >= 1 byte received)
    tit_USART_LinBreak,          // break condition detected on LIN bus

    tit_Last_USART, //}

    tit_First_CAN,    //{ interrupt types for CAN-bus
    tit_CAN_Transmit_Empty,
    tit_CAN_FIFO0_Pending,
    tit_CAN_FIFO0_Full,
    tit_CAN_FIFO0_Overrun,
    tit_CAN_FIFO1_Pending,
    tit_CAN_FIFO1_Full,
    tit_CAN_FIFO1_Overrun,
    tit_CAN_Error_Warning,
    tit_CAN_Error_Passive,
    tit_CAN_Bus_Off,
    tit_CAN_WakeUp_State,
    tit_CAN_Sleep_State,
    tit_Last_CAN, //}


    tit_First_USB, //[interrupt types from USB-bus
    tit_USB_OTG__FS_global,
    tit_Last_USB,//}

    tit_First_RTC, //{ interrupt types for universal serial asynchronous receiver transmitter

    tit_RTC_All,           // apply to all USART interrupts (only for disabling!)
    tit_RTC_TimeStamp,     // Cts signal has been received
    tit_RTC_WakeUP,        // error condition has arised (detailed data is handed to error handler)
    tit_RTC_AlarmA,        // deivce has gone idle
    tit_RTC_AlarmB,        // transfer completed
    tit_RTC_Tamper,        // transmit buffer ist empty ( bytes sent)
    tit_RTC_Pin_line_wakeup,
    tit_Last_RTC, //}

    // additional types of sources go here...

    tit_Error
} e_ttc_interrupt_type;
typedef enum {    // e_ttc_interrupt_architecture  types of architectures supported by INTERRUPT driver
    E_ttc_interrupt_architecture_none,      // no architecture selected
    E_ttc_interrupt_architecture_cortexm3,  // automatically added by ./create_DeviceDriver.pl
    E_ttc_interrupt_architecture_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
    E_ttc_interrupt_architecture_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
    E_ttc_interrupt_architecture_stm32w1xx, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    E_ttc_interrupt_architecture_unknown        // architecture not supported
} e_ttc_interrupt_architecture;

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//}Enums
//{ Includes *************************************************************

#include "ttc_basic_types.h"
#ifndef TTC_BASIC_H
    #include "compile_options.h"
#endif

// Low-Level Architecture Drivers for Interrupt Datatypes
#ifdef EXTENSION_interrupt_stm32w1xx
    #include "interrupt/interrupt_stm32w1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_interrupt_stm32f1xx
    #include "interrupt/interrupt_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_interrupt_stm32l1xx
    #include "interrupt/interrupt_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_interrupt_cortexm3
    #include "interrupt/interrupt_cortexm3_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_interrupt_stm32f30x
    #include "interrupt/interrupt_stm32f30x_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

// High-Level Datatypes for individual interrupt enabled devices
#ifdef EXTENSION_ttc_usart
    #include "ttc_usart_types.h"
    #ifndef TTC_INTERRUPT_USART_AMOUNT // amount of interrupt lines from USART devices
        #warning Missing value for TTC_INTERRUPT_USART_AMOUNT
        #define TTC_INTERRUPT_USART_AMOUNT 0 // TC_USART_MAX_AMOUNT
    #endif
#endif
#ifdef EXTENSION_ttc_gpio
    #include "ttc_gpio_types.h"
    #ifndef TTC_INTERRUPT_GPIO_AMOUNT // amount of interrupt lines from GPIO devices
        #warning Missing value for TTC_INTERRUPT_GPIO_AMOUNT
        #define TTC_INTERRUPT_GPIO_AMOUNT 0
    #endif
#endif
#ifdef EXTENSION_ttc_timer //{
    #include "ttc_timer_types.h"
    #ifndef TTC_INTERRUPT_TIMER_AMOUNT // amount of interrupt lines from TIMER devices
        #warning Missing value for TTC_INTERRUPT_TIMER_AMOUNT
        #define TTC_INTERRUPT_TIMER_AMOUNT 0
    #endif
#endif //}
#ifdef EXTENSION_ttc_can //{
    #include "ttc_can_types.h"
    #ifndef TTC_INTERRUPT_CAN_AMOUNT // amount of interrupt lines from CAN devices
        #warning Missing value for TTC_INTERRUPT_CAN_AMOUNT
        #define TTC_INTERRUPT_CAN_AMOUNT 0
    #endif
#endif //}
#ifdef EXTENSION_ttc_i2c
    #include "ttc_i2c_types.h"
    #ifndef TTC_INTERRUPT_I2C_AMOUNT // amount of interrupt lines from I2C devices
        #warning Missing value for TTC_INTERRUPT_I2C_AMOUNT
        #define TTC_INTERRUPT_I2C_AMOUNT 0 // TC_I2C_MAX_AMOUNT
    #endif
#endif
#ifdef EXTENSION_ttc_spi
    #include "ttc_spi_types.h"
    #ifndef TTC_INTERRUPT_SPI_AMOUNT // amount of interrupt lines from SPI devices
        #warning Missing value for TTC_INTERRUPT_SPI_AMOUNT
        #define TTC_INTERRUPT_SPI_AMOUNT 0 // TC_SPI_MAX_AMOUNT
    #endif
#endif
#ifdef EXTENSION_ttc_rtc
    #include "ttc_rtc_types.h"
    #ifndef TTC_INTERRUPT_RTC_AMOUNT // amount of interrupt lines from RADIO devices
        #warning Missing value for TTC_INTERRUPT_RTC_AMOUNT
        #define TTC_INTERRUPT_RTC_AMOUNT 0 // TC_RTC_MAX_AMOUNT
    #endif
#endif

#include "ttc_list_types.h"

//} Includes
//{ Defines **************************************************************


//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

/** Check definition that have to be set by low-level driver.. */

/* Device specific Assert function
     *
     * The use of Assert() is the basic key for stable code in embedded software.
     * Every function should check all given arguments for plausibility.
     * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
     * This prevents any possibly dangerous operations.
     *
     * Every device driver provides its own Assert_{DEVICE}() function.
     * This allows to switch off the use of assert code for each device driver from your makefile.
     * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
     *   COMPILE_OPTS += -DTTC_ASSERT_INTERRUPT 0  # disable assert handling for interrupt devices
     *
     */
#ifndef TTC_ASSERT_INTERRUPT    // any previous definition set (Makefile)?
    #define TTC_ASSERT_INTERRUPT 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_INTERRUPT == 1)  // use Assert()s in INTERRUPT code (somewhat slower but alot easier to debug)
    #define Assert_INTERRUPT(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_INTERRUPT_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_INTERRUPT_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in INTERRUPT code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_INTERRUPT(Condition, Origin)
    #define Assert_INTERRUPT_Writable(Address, Origin)
    #define Assert_INTERRUPT_Readable(Address, Origin)
#endif


#ifndef TTC_ASSERT_INTERRUPT_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_INTERRUPT_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_INTERRUPT_EXTRA == 1)  // use Assert()s in INTERRUPT code (somewhat slower but alot easier to debug)
    #define Assert_INTERRUPT_EXTRA(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_INTERRUPT_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_INTERRUPT_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in INTERRUPT code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_INTERRUPT_EXTRA(Condition, Origin)
    #define Assert_INTERRUPT_EXTRA_Writable(Address, Origin)
    #define Assert_INTERRUPT_EXTRA_Readable(Address, Origin)
#endif

/** Architecture dependent device configuration
     *
     * Each low-level driver may define this structure to increase visibility of t_ttc_interrupt_config during debugging.
     * Simply copy the define line below into your _types.h and change void* to any structure you like.
     */
#ifndef t_ttc_interrupt_architecture
    #warning Missing low-level definition for t_ttc_interrupt_architecture (using default)
    #define t_ttc_interrupt_architecture void
#endif

// list of function pointers to initialization functions of this type:
// e_ttc_interrupt_errorcode (*interrupt_init)(e_ttc_interrupt_type Source, void* ISR);
// each architecture driver (GPIO, I2C, ...) may define one init() and multiple enums (-> e_ttc_interrupt_type).
#ifndef TTC_INTERRUPT_INIT_GPIO
    #define TTC_INTERRUPT_INIT_GPIO NULL
#endif
#ifndef TTC_INTERRUPT_INIT_I2C
    #define TTC_INTERRUPT_INIT_I2C NULL
#endif
#ifndef TTC_INTERRUPT_INIT_TIMER
    #define TTC_INTERRUPT_INIT_TIMER NULL
#endif
#ifndef TTC_INTERRUPT_INIT_RADIO
    #define TTC_INTERRUPT_INIT_RADIO NULL
#endif
#ifndef TTC_INTERRUPT_INIT_SPI
    #define TTC_INTERRUPT_INIT_SPI NULL
#endif
#ifndef TTC_INTERRUPT_INIT_interrupt
    #define TTC_INTERRUPT_INIT_interrupt NULL
#endif
#ifndef TTC_INTERRUPT_INIT_CAN
    #define TTC_INTERRUPT_INIT_CAN NULL
#endif

//}Static Configuration
//{ Structures ***********************************************************

typedef union { // t_ttc_interrupt_gpio_index
    t_u8 U8;
    struct {
        unsigned Bank : 4; // index of GPIO-bank to use (0 = GPIOA, ...)
        unsigned Pin  : 4; // index of GPIO-pin to use (0 = Pin1, ...)
    } B;
} __attribute__( ( __packed__ ) ) t_ttc_interrupt_gpio_index;

typedef struct { // t_ttc_interrupt_config
    e_ttc_interrupt_type Type;             // interrupt source type as given to _init()
    t_u8                 ConfigIndex;      // index of this entry in ttc_interrupt_config_usart[]
} t_ttc_interrupt_config;


#ifdef EXTENSION_ttc_usart

typedef struct s_ttc_interrupt_source_usart { // description of a single interrupt source
    struct s_ttc_interrupt_source_usart* Next;    // interrupt sources can be linked into a list
    t_ttc_interrupt_config               Config;  // interrupt source configuration
} t_ttc_interrupt_source_usart;

typedef struct { // t_ttc_interrupt_config_usart - pointers of interrupt service routines (-> STM32_RM0008 p. 788)

    //X t_ttc_interrupt_config Common;         // this part is common to all types of interrupt configs
    t_physical_index     PhysicalIndex;    // physical index of USART unit

    // interrupt service routines being called
    void ( *isr_ClearToSend )( t_physical_index,              t_ttc_usart_config* );
    void ( *isr_Error )( t_physical_index, t_ttc_interrupt_usart_errors, t_ttc_usart_config* );
    void ( *isr_IdleLine )( t_physical_index,                 t_ttc_usart_config* );
    void ( *isr_TransmitDataEmpty )( t_physical_index,        t_ttc_usart_config* );
    void ( *isr_ReceiveDataNotEmpty )( t_physical_index,      t_ttc_usart_config* );
    void ( *isr_TransmissionComplete )( t_physical_index,     t_ttc_usart_config* );
    void ( *isr_LinBreakDetected )( t_physical_index,         t_ttc_usart_config* );

    // arguments passed to isr_XXX()
    t_ttc_usart_config* Argument_ClearToSend;
    t_ttc_usart_config* Argument_Error;
    t_ttc_usart_config* Argument_IdleLine;
    t_ttc_usart_config* Argument_TransmitDataEmpty;
    t_ttc_usart_config* Argument_ReceiveDataNotEmpty;
    t_ttc_usart_config* Argument_TransmissionComplete;
    t_ttc_usart_config* Argument_LinBreakDetected;

    /* head of single linked list of interrupt sources
     * Each _ttc_interrupt_init_usart() call with a new e_ttc_interrupt_type will add a new entry to this list.
     * _ttc_interrupt_init_usart() will return a pointer to one item of this list. This allows to enable/disable
     * individual USART interrupts.
     */
    t_ttc_interrupt_source_usart* Sources;

#ifdef interrupt_driver_config_usart_t
    // low-level architecture dependent interrupt configuration
    interrupt_driver_config_usart_t LowLevel;
#endif
} t_ttc_interrupt_config_usart;
#endif

#ifdef EXTENSION_ttc_i2c
typedef struct { // t_ttc_interrupt_config_i2c - pointers of interrupt service routines (-> STM32_RM0008 p. 788)

    t_ttc_interrupt_config Common;         // this part is common to all types of interrupt configs
    t_physical_index     PhysicalIndex;    // physical index of I2C unit

    // interrupt service routines being called
    void ( *isr_StartBitSent )( t_physical_index,              t_ttc_i2c_config* );
    void ( *isr_AddressSent )( t_physical_index,              t_ttc_i2c_config* );
    void ( *isr_10BitHeader )( t_physical_index,              t_ttc_i2c_config* );
    void ( *isr_StopReceived )( t_physical_index,              t_ttc_i2c_config* );
    void ( *isr_DataByteTransferFinished )( t_physical_index,              t_ttc_i2c_config* );
    void ( *isr_ReceiveBufferNotEmpty )( t_physical_index,              t_ttc_i2c_config* );
    void ( *isr_TransmitBufferEmpty )( t_physical_index,              t_ttc_i2c_config* );
    void ( *isr_BusError )( t_physical_index,              t_ttc_i2c_config* );
    void ( *isr_ArbitrationLoss )( t_physical_index,              t_ttc_i2c_config* );
    void ( *isr_AckFail )( t_physical_index,              t_ttc_i2c_config* );
    void ( *isr_Overrun )( t_physical_index,              t_ttc_i2c_config* );
    void ( *isr_PECError )( t_physical_index,              t_ttc_i2c_config* );
    void ( *isr_Timeout )( t_physical_index,              t_ttc_i2c_config* );
    void ( *isr_SMBusAlert )( t_physical_index,              t_ttc_i2c_config* );

    // arguments passed to isr_XXX()
    t_ttc_i2c_config* Argument_StartBitSent;
    t_ttc_i2c_config* Argument_AddressSent;
    t_ttc_i2c_config* Argument_10BitHeader;
    t_ttc_i2c_config* Argument_StopReceived;
    t_ttc_i2c_config* Argument_DataByteTransferFinished;
    t_ttc_i2c_config* Argument_ReceiveBufferNotEmpty;
    t_ttc_i2c_config* Argument_TransmitBufferEmpty;
    t_ttc_i2c_config* Argument_BusError;
    t_ttc_i2c_config* Argument_ArbitrationLoss;
    t_ttc_i2c_config* Argument_AckFail;
    t_ttc_i2c_config* Argument_Overrun;
    t_ttc_i2c_config* Argument_PECError;
    t_ttc_i2c_config* Argument_Timeout;
    t_ttc_i2c_config* Argument_SMBusAlert;

#ifdef interrupt_driver_config_i2c_t
    // low-level architecture dependent interrupt configuration
    interrupt_driver_config_i2c_t LowLevel;
#endif
} t_ttc_interrupt_config_i2c;
#endif

#ifdef EXTENSION_ttc_spi
typedef struct { // t_ttc_interrupt_config_spi - pointers of interrupt service routines (-> STM32_RM0008 p. 788)

    t_ttc_interrupt_config Common;         // this part is common to all types of interrupt configs
    t_physical_index     PhysicalIndex;    // physical index of SPI unit

    // interrupt service routines being called
    void ( *isr_ChangeMe )( t_physical_index,              t_ttc_spi_config* );

    // arguments passed to isr_XXX()
    t_ttc_spi_config* Argument_ChangeMe;

#ifdef interrupt_driver_config_spi_t
    // low-level architecture dependent interrupt configuration
    interrupt_driver_config_spi_t LowLevel;
#endif
} t_ttc_interrupt_config_spi;
#endif
#ifdef EXTENSION_ttc_gpio
typedef struct { // t_ttc_interrupt_config_gpio - configuration data of single external interrupt line (-> STM32_RM0008 p. 201)
    t_ttc_interrupt_config Common;         // this part is common to all types of interrupt configs
    e_ttc_gpio_pin PhysicalIndex;          // gpio pin index
    void ( *isr )( t_physical_index, void* ); // interrupt service routine being called
    void* Argument;                        // argument passed to isr()

#ifdef interrupt_driver_config_gpio_t  // defined by low-level driver to store additional data for each gpio pin
    interrupt_driver_config_gpio_t LowLevel;      // low-level architecture dependent interrupt configuration
#endif
} t_ttc_interrupt_config_gpio;
#endif
#ifdef EXTENSION_ttc_timer
typedef struct { // t_ttc_interrupt_configimer_t - configuration data of single external interrupt line (-> STM32_RM0008 p. 201)

    t_ttc_interrupt_config Common;         // this part is common to all types of interrupt configs
    t_physical_index     PhysicalIndex;    // physical index of USART unit

    // interrupt service routines being called
    void ( *isr_Update )( t_physical_index, t_ttc_timer_config* );

    // arguments passed to isr_XXX()
    t_ttc_timer_config* Argument_Update;

#ifdef t_interrupt_driver_config_timer
    t_interrupt_driver_config_timer LowLevel;    // low-level architecture dependent interrupt configuration
#endif

} t_ttc_interrupt_configimer_t;
#endif
#ifdef EXTENSION_ttc_can
typedef struct { // t_ttc_interrupt_config_can - configuration data of single external interrupt line (-> STM32_RM0008 p. 201)

    t_ttc_interrupt_config Common;         // this part is common to all types of interrupt configs
    t_physical_index     PhysicalIndex;    // physical index of CAN unit

    // interrupt service routine being called
    void ( *isr_Transmit )( t_physical_index, t_ttc_can_config* );
    void ( *isr_FIFO0_Pending )( t_physical_index, t_ttc_can_config* );
    void ( *isr_FIFO0_Full )( t_physical_index, t_ttc_can_config* );
    void ( *isr_FIFO0_Overrun )( t_physical_index, t_ttc_can_config* );
    void ( *isr_FIFO1_Pending )( t_physical_index, t_ttc_can_config* );
    void ( *isr_FIFO1_Full )( t_physical_index, t_ttc_can_config* );
    void ( *isr_FIFO1_Overrun )( t_physical_index, t_ttc_can_config* );
    void ( *isr_Error_Warning )( t_physical_index, t_ttc_can_config* );
    void ( *isr_Error_Passive )( t_physical_index, t_ttc_can_config* );
    void ( *isr_Bus_Off )( t_physical_index, t_ttc_can_config* );
    void ( *isr_WakeUp_State )( t_physical_index, t_ttc_can_config* );
    void ( *isr_Sleep_State )( t_physical_index, t_ttc_can_config* );

    // argument passed to isr()
    t_ttc_can_config* Argument_Transmit;
    t_ttc_can_config* Argument_FIFO0_Pending;
    t_ttc_can_config* Argument_FIFO0_Full;
    t_ttc_can_config* Argument_FIFO0_Overrun;
    t_ttc_can_config* Argument_FIFO1_Pending;
    t_ttc_can_config* Argument_FIFO1_Full;
    t_ttc_can_config* Argument_FIFO1_Overrun;
    t_ttc_can_config* Argument_Error_Warning;
    t_ttc_can_config* Argument_Error_Passive;
    t_ttc_can_config* Argument_Bus_Off;
    t_ttc_can_config* Argument_WakeUp_State;
    t_ttc_can_config* Argument_Sleep_State;


#ifdef interrupt_driver_config_can_t
    interrupt_driver_config_can_t LowLevel; // low-level architecture dependent interrupt configuration
#endif
} t_ttc_interrupt_config_can;
#endif


#ifdef EXTENSION_ttc_rtc
typedef struct { // t_ttc_interrupt_config_rtc - configuration data of single external interrupt line (-> STM32_RM0008 p. 201)

    t_ttc_interrupt_config Common;         // this part is common to all types of interrupt configs
    t_physical_index     PhysicalIndex;    // physical index of rtc unit

    // interrupt service routine being called
    void ( *isr_TimeStamp )( t_physical_index, t_ttc_rtc_config* );
    void ( *isr_Tamper )( t_physical_index, t_ttc_rtc_config* );
    void ( *isr_Wakeup )( t_physical_index, t_ttc_rtc_config* );
    void ( *isr_AlarmA )( t_physical_index, t_ttc_rtc_config* );
    void ( *isr_AlarmB )( t_physical_index, t_ttc_rtc_config* );


    // argument passed to isr()
    t_ttc_rtc_config* Argument_TimeStamp;
    t_ttc_rtc_config* Argument_Tamper;
    t_ttc_rtc_config* Argument_Wakeup;
    t_ttc_rtc_config* Argument_AlarmA;
    t_ttc_rtc_config* Argument_AlarmB;



#ifdef interrupt_driver_config_rtc_t
    interrupt_driver_config_rtc_t LowLevel; // low-level architecture dependent interrupt configuration
#endif
} t_ttc_interrupt_config_rtc;
#endif
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_INTERRUPT_TYPES_H
