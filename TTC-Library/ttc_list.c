/** ttc_list.c ***********************************************{
 *
 * Written by Gregor Rebel 2013
 *
 * Architecture independent support for single linked lists.
 *
 * Why to use lists instead of queues?
 * - lists have no size limit (every item brings its own next-pointer)
 * - lists require only as much memory as required (no oversized pre-allocation like queues)
 * - lists can be used from interrupt service routines too
 * - every pool-allocated memory block can be put into a list (-> ttc_memory.h)
 *
 * When not to prefer lists over queues?
 * - if you want to store big amount of very small blocks like single bytes
 *   (every item requires a next-pointer)
 *
}*/
#include "ttc_list.h"
#include "ttc_semaphore.h"
#include "ttc_mutex.h"
#include "ttc_heap.h"

//{ declare private functions (no locking/ argument checking) *

// Define TTC_LIST_DEBUG_HISTORY in your makefile or right
// here to memorize pointers to lists being operated on.
#ifdef TTC_LIST_DEBUG_HISTORY
    #define SIZE_LIST_HISTORY 70
#else
    #define SIZE_LIST_HISTORY 10
#endif
#if SIZE_LIST_HISTORY > 0

/** rotating array storing last processed lists
 *
 * This history of list content allows to see what happened to your list during
 * last SIZE_LIST_HISTORY amount of operations in a debug session.
 *
 * Display number of current index in ListHistory[]
 * (gdb) p LatestListHistory - ListHistory
 *
 * Display latest recorded list values
 * (gdb) p *(LatestListHistory-0)
 *
 * Display second latest recorded list values
 * (gdb) p *(LatestListHistory-1)
 *
 */
t_ttc_list ListHistory[SIZE_LIST_HISTORY];
t_ttc_list* LatestListHistory = ListHistory - 1;  // always points to latest valid entry in ListHistory ( E.g.: p *(LatestListHistory-0) , p *(LatestListHistory-1) , ...)

void         _TTC_LIST_ADD_HISTORY( t_ttc_list* List ) {

    LatestListHistory++;
    if ( LatestListHistory >= ListHistory + SIZE_LIST_HISTORY )
    { LatestListHistory = ListHistory; }
    ttc_memory_copy( LatestListHistory, List, sizeof( t_ttc_list ) );

}
#define TTC_LIST_ADD_HISTORY(LIST) _TTC_LIST_ADD_HISTORY(LIST)

#else
#define TTC_LIST_ADD_HISTORY(LIST)
#endif

/** completes all pending  on given list
 *
 * Note: This function is private and must not be called from outside!
 * Note: This function completes in constant time O(1)
 *
 * @param List     pointer to already created list
 */
void _ttc_list_complete_all_pending_isr( t_ttc_list* List );

/** Common implementation for ttc_list_iterate(), ttc_list_iterate_from()
 *
 * Runs through given list items and calls given iterate function for each one.
 * Can remove individual items from list and call a remove function.
 *
 * @param List             pointer to memory space to be used to manage list
 * @param IterateFunction  function pointer: will be called for each list item. Returns TRUE if current item shall be removed from list.
 *                         Note: IterateFunction() may not append item to other lists or release it. It may return a function pointer that can process the item later (see @return below)!
 *                         @return ==0: do not remove item; ==1: current item shall be removed from list;
 *                                 >1: function to call with item as argument AFTER removing it from current list.
 *                                     Signature of function being called: void RemoveItem(t_ttc_list_item*)
 * @param Argument         argument passed to IterateFunction()
 * @param Step             Allows to skip items:
 *                         == 1: iterate all items 1, 2, ..., last
 *                         == 2: iterate every second item 1, 3, ... last
 * @param Start            pointer to list item for which IterateFunction is called first
 * @param End              pointer to list item for which IterateFunction is called last (==TTC_LIST_TERMINATOR or ==NULL: run to end of list)
 * @param Amount           maximum amount of items to iterate (==-1: not limited)
 */

void _ttc_list_iterate_from( t_ttc_list* List, void* ( *IterateFunction )( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item ), void* Argument, t_u16 SkipFirst, t_s16 Steps, t_ttc_list_item* Start, t_ttc_list_item* End, t_u16 Amount );

/** adds given Item or linked list of items at end of given list
 *
 * Note: This function is private and must not be called from outside!
 * Note: This function completes in constant time O(1)
 * Note: This function will not update List->AmountItems.
 *
 * @param List     pointer to already created list
 * @param Item     pointer to item to be added to list
 * @return         == tle_OK: Item has been successfully added to List
 */
void _ttc_list_push_back_single( t_ttc_list* List, t_ttc_list_item* Item );

/** adds given Item or linked list of items at head of given list
 *
 * Note: This function is private and must not be called from outside!
 * Note: This function completes in constant time O(1)
 * Note: This function will not update List->AmountItems.
 *
 * @param List     pointer to already created list
 * @param Item     pointer to item to be added to list
 * @return         == tle_OK: Item has been successfully added to List
 */
void _ttc_list_push_front_single( t_ttc_list* List, t_ttc_list_item* Item );

/** adds given linked list of items at end of given list
 *
 * Note: This function is private and must not be called from outside!
 * Note: This function completes in constant time O(1)
 * Note: This function will not update List->AmountItems.
 *
 * @param List       pointer to already created list
 * @param FirstItem  pointer to first item in list of items to be added to list
 * @param LastItem   pointer to last item in list of items to be added to list
 */
void _ttc_list_push_back_multiple( t_ttc_list* List, t_ttc_list_item* FirstItem, t_ttc_list_item* LastItem );

/** adds given linked list of items before head of given list
 *
 * Note: This function is private and must not be called from outside!
 * Note: This function completes in constant time O(1)
 * Note: This function will not update List->AmountItems.
 *
 * @param List       pointer to already created list
 * @param FirstItem  pointer to first item in list of items to be added to list
 * @param LastItem   pointer to last item in list of items to be added to list
 */
void _ttc_list_push_front_multiple( t_ttc_list* List, t_ttc_list_item* FirstItem, t_ttc_list_item* LastItem );

/** removes first Item from given list and delivers its reference
 *
 * Note: This function is private and must not be called from outside!
 * Note: This function completes in constant time O(1)
 * Note: This function will not update List->AmountItems.
 *
 * @param List     pointer to already created list
 * @param Item     !=NULL: loaded with pointer to item removed from given list or NULL if list is empty; ==NULL: removed item is not stored
 */
void _ttc_list_pop_front_single( t_ttc_list* List, t_ttc_list_item** Item );

/** removes linked list of first items from front of given list and delivers reference to first and last one
 *
 * Note: This function is private and must not be called from outside!
 * Note: This function completes in constant time O(Amount)
 * Note: This function will not update List->AmountItems.
 *
 * @param List      pointer to already created list
 * @param ListSize  amount of items currently stored in list
 * @param Amount    amount of items to remove from list (==-1: means all items in list)
 * @param FirstItem loaded with pointer to first item of items removed from given list or NULL if list is empty
 * @param LastItem  loaded with pointer to last item of items removed from given list or NULL if list is empty
 * @return          actual amount of items being removed from list
 */
t_base _ttc_list_pop_front_multiple( t_ttc_list* List, t_base ListSize, t_base Amount, t_ttc_list_item** FirstItem, t_ttc_list_item** LastItem );

/** removes single Item from given list
 *
 * Note: This function is private and must not be called from outside!
 * Note: This function completes in constant time O(1)
 * Note: This function will not update List->AmountItems.
 *
 * @param List          pointer to already created list
 * @param PreviousItem  !=NULL: pointer to item previous to item to remove; ==NULL: Item to remove is first item
 * @param Item          pointer to item to be removed from given list
 * @param NextItem      !=TTC_LIST_TERMINATOR: pointer to item following item to remove (==Item->Next); ==TTC_LIST_TERMINATOR: Item to remove is last item; ==NULL: not allowed!
 */
void _ttc_list_remove_item( t_ttc_list* List, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item, t_ttc_list_item* NextItem );

//} private functions
//{ functions *************************************************

t_base           ttc_list_count_items( t_ttc_list_item* FirstItem ) {
    if ( ( FirstItem == NULL ) || ( FirstItem == TTC_LIST_TERMINATOR ) )
    { return 0; }

    t_u16 Counter = 0; // Once your lists grow longer than 65535 items, you may increase size of this variable
    t_ttc_list_item* volatile CurrentItem = FirstItem;

#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
    t_ttc_list* List = FirstItem->Owner;
    if ( List ) {
        Assert_LIST_Writable( List, ttc_assert_origin_auto ); // Item claims to belong to a list, but its not located in RAM. Check if someone has modified this item!
    }
#endif

    while ( ( CurrentItem != NULL ) && ( CurrentItem != TTC_LIST_TERMINATOR ) ) {
        Assert_LIST_Writable( CurrentItem, ttc_assert_origin_auto );
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
        Assert_LIST( CurrentItem->Owner == List, ttc_assert_origin_auto ); // Item says that it does not belong to same list as first item. Check if someone has modified this item!
#endif
        CurrentItem = CurrentItem->Next;
        Counter++;
        Assert_LIST( Counter != 0, ttc_assert_origin_auto ); // endless loop detected in single linked list of list items. Check implementation of ttc_list and your application!
    }

    return Counter;
}
t_base           ttc_list_count_items_safe( t_ttc_list_item* FirstItem, t_base MaxSize ) {
    if ( ( FirstItem == NULL ) || ( FirstItem == TTC_LIST_TERMINATOR ) )
    { return 0; }

    t_base Counter = 0;
    t_ttc_list_item* volatile CurrentItem = FirstItem;

#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
    t_ttc_list* List = FirstItem->Owner;
    if ( List ) {
        Assert_LIST_Writable( List, ttc_assert_origin_auto ); // Item claims to belong to a list, but its not located in RAM. Check if someone has modified this item!
    }
#endif

    while ( CurrentItem && ( CurrentItem != TTC_LIST_TERMINATOR ) && MaxSize ) {
        Assert_LIST_Writable( CurrentItem, ttc_assert_origin_auto );
        CurrentItem = CurrentItem->Next;
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
        Assert_LIST( ( List == NULL ), ttc_assert_origin_auto ); // Item says that it does not belong to same list as first item. Check if someone has modified this item!
#endif
        Counter++;
        MaxSize--;
    }

    return Counter;
}
t_ttc_list*      ttc_list_create( const char* Name ) {
    t_ttc_list* NewList = ttc_heap_alloc( sizeof( t_ttc_list ) );
    Assert_LIST( NewList != NULL, ttc_assert_origin_auto );  // could not create list (no implementation/ not egnough memory)

    ttc_list_init( NewList, Name );

    return NewList;
}
void             ttc_list_init( t_ttc_list* List, const char* Name ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!

    ttc_memory_set( ( void* ) List, 0, sizeof( *List ) );
    ttc_mutex_init( &( List->Lock ) );
    ttc_semaphore_init( &( List->AmountItems ) );
#if (TTC_ASSERT_LIST_EXTRA == 1)
    List->Stats.Name = Name;
#else
    ( void ) Name;
#endif
}
e_ttc_list_error ttc_list_complete_all_pending_isr( t_ttc_list* List ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto );  // invalid list (did you initialize it?)

    if ( ttc_mutex_is_locked( &( List->Lock ) ) ) // list is locked by task: abort completion
    { return tle_ListInUseByTask; }

    _ttc_list_complete_all_pending_isr( List );

    return tle_OK;
}
void             ttc_list_push_back_single( t_ttc_list* List, t_ttc_list_item* Item ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );         // invalid pointer given!
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto );  // invalid list (did you initialize it?)
    Assert_LIST_VALID( Item, ttc_assert_origin_auto );         // invalid pointer given!

    ttc_mutex_lock( &( List->Lock ), -1 );

    _ttc_list_push_back_single( List, Item );
    TTC_LIST_STATS_INCREASE( List, AmountPushBack );
#if (TTC_ASSERT_LIST_EXTRA == 1)
    if ( ( ( Item->Next ) != NULL ) && ( ( Item->Next ) != TTC_LIST_TERMINATOR ) )  {
        Assert_LIST_VALID( Item->Next, ttc_assert_origin_auto );
    }
#endif
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
    Item->Caller = __builtin_return_address( 0 );  // store address of calling function for debugging
#endif

    //? ttc_task_critical_begin();
    ttc_mutex_unlock( &( List->Lock ) );
    ttc_semaphore_give( &( List->AmountItems ), 1 ); // must increase semaphore AFTER unlocking mutex (waiting task will try to pull right now)
    //? ttc_task_critical_end();
}
e_ttc_list_error ttc_list_push_back_single_isr( t_ttc_list* List, t_ttc_list_item* Item ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
    Assert_LIST( ( Item->Owner == NULL ) || ( ttc_memory_is_constant( Item->Owner ) ), ttc_assert_origin_auto ); // Item still belongs to a list. Remove it from this list before freeing it!
#endif

    if ( ! ttc_mutex_is_locked( &( List->Lock ) ) ) { // not locked by task: add items to list
        if ( List->Pending_Push_Amount )         // still data pending from last isr-push
        { _ttc_list_complete_all_pending_isr( List ); }
        _ttc_list_push_back_single( List, Item );
#if (TTC_ASSERT_LIST_EXTRA == 1)
        Item->Caller = __builtin_return_address( 0 );  // store address of calling function for debugging
#endif
        TTC_LIST_STATS_INCREASE( List, AmountPushBackISR );
        ttc_semaphore_give_isr( &( List->AmountItems ), 1 );
    }
    else {                                            // already locked by task: add to list of pending items
#if (TTC_ASSERT_LIST_EXTRA == 1)
        TTC_LIST_STATS_INCREASE( List, AmountPending );
#endif
        if ( List->Pending_Push_Back_Last ) { // append at end of list
            List->Pending_Push_Back_Last->Next = Item;
        }
        else {                                // append as first item
            List->Pending_Push_Back_First = Item;
        }
        //? while ( Item->Next )
        //? { Item = Item->Next; }
        List->Pending_Push_Back_Last = Item;
        List->Pending_Push_Amount++;
        Item->Next = TTC_LIST_TERMINATOR;
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
        Item->Owner = List;  // item now belongs to a list
#endif
#if (TTC_ASSERT_LIST_EXTRA == 1)
        Item->Caller = __builtin_return_address( 0 );
#endif

        return tle_ListInUseByTask;
    }

    return tle_OK;
}
void             ttc_list_push_back_multiple( t_ttc_list* List, t_ttc_list_item* FirstItem, t_ttc_list_item* LastItem, t_base Amount ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto ); // invalid list (did you initialize it?)
    Assert_LIST_VALID( FirstItem, ttc_assert_origin_auto );   // invalid pointer given!
    Assert_LIST_VALID( LastItem, ttc_assert_origin_auto );    // invalid pointer given!

    ttc_mutex_lock( &( List->Lock ), -1 );

    _ttc_list_push_back_multiple( List, FirstItem, LastItem );
#if (TTC_ASSERT_LIST_EXTRA == 1)
    FirstItem->Caller = __builtin_return_address( 0 );  // store address of calling function for debugging
#endif
    ttc_semaphore_give( &( List->AmountItems ), Amount );

    Assert_LIST_VALID( List->First, ttc_assert_origin_auto );  // invalid list (did you initialize it?)
    Assert_LIST_VALID( List->Last, ttc_assert_origin_auto );
    Assert_LIST_EXTRA( ttc_semaphore_available( &( List->AmountItems ) ) == ttc_list_count_items( List->First ), ttc_assert_origin_auto );  // bug in size counter implementation

    ttc_mutex_unlock( &( List->Lock ) );
}
e_ttc_list_error ttc_list_push_back_multiple_isr( t_ttc_list* List, t_ttc_list_item* FirstItem, t_ttc_list_item* LastItem, t_base Amount ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto ); // invalid list (did you initialize it?)
    Assert_LIST_VALID( FirstItem, ttc_assert_origin_auto );   // invalid pointer given!
    Assert_LIST_VALID( LastItem, ttc_assert_origin_auto );    // invalid pointer given!

    if ( ! ttc_mutex_is_locked( &( List->Lock ) ) ) { // not locked by task: add items to list
        if ( List->Pending_Push_Amount )         // still data pending from last isr-push
        { _ttc_list_complete_all_pending_isr( List ); }
        _ttc_list_push_back_multiple( List, FirstItem, LastItem );
#if (TTC_ASSERT_LIST_EXTRA == 1)
        FirstItem->Caller = __builtin_return_address( 0 );  // store address of calling function for debugging
#endif
        ttc_semaphore_give( &( List->AmountItems ), Amount );
        Assert_LIST_EXTRA( ttc_semaphore_available( &( List->AmountItems ) ) == ttc_list_count_items( List->First ), ttc_assert_origin_auto );  // bug in size counter implementation
    }
    else {                                            // already locked by task: add to list of pending items
#if (TTC_ASSERT_LIST_EXTRA == 1)
        t_ttc_list_item* Item = FirstItem;
        do { // check all items to be added
            Assert_LIST_VALID_EXTRA( Item, ttc_assert_origin_auto );
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
            Assert_LIST( FirstItem->Owner == NULL, ttc_assert_origin_auto ); // Item still belongs to a list. Remove it from this list before freeing it!
#endif

            if ( ( Item != TTC_LIST_TERMINATOR ) && ( Item != NULL ) ) {
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
                Item->Owner = List;  // item now belongs to a list
#endif
#if (TTC_ASSERT_LIST_EXTRA == 1)
                Item->Caller = __builtin_return_address( 0 );
#endif
            }
            Item = Item->Next;
        }
        while ( ( Item != TTC_LIST_TERMINATOR ) && ( Item != NULL ) && ( Item != LastItem ) );
        TTC_LIST_STATS_INCREASE( List, AmountPending );
#endif

        if ( List->Pending_Push_Back_Last ) { // append at end of list
            List->Pending_Push_Back_Last->Next = FirstItem;
        }
        else {                                // append as first item
            List->Pending_Push_Back_First = FirstItem;
        }
        List->Pending_Push_Back_Last = LastItem;
        List->Pending_Push_Amount    += Amount;

        return tle_ListInUseByTask;
    }

    return tle_OK;
}
void             ttc_list_push_front_single( t_ttc_list* List, t_ttc_list_item* Item ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto );  // invalid list (did you initialize it?)
    Assert_LIST_VALID( Item, ttc_assert_origin_auto );        // invalid pointer given!

    ttc_mutex_lock( &( List->Lock ), -1 );

    _ttc_list_push_front_single( List, Item );
#if (TTC_ASSERT_LIST_EXTRA == 1)
    Item->Caller = __builtin_return_address( 0 );  // store address of calling function for debugging
#endif
    TTC_LIST_STATS_INCREASE( List, AmountPushFront );
#if (TTC_ASSERT_LIST_EXTRA == 1)
    if ( ( Item->Next ) != TTC_LIST_TERMINATOR ) {
        Assert_LIST_VALID( Item->Next, ttc_assert_origin_auto );
    }
#endif

    ttc_mutex_unlock( &( List->Lock ) );
    ttc_semaphore_give( &( List->AmountItems ), 1 );
}
e_ttc_list_error ttc_list_push_front_single_isr( t_ttc_list* List, t_ttc_list_item* Item ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!

    if ( ! ttc_mutex_is_locked( &( List->Lock ) ) ) { // not locked by task: add items to list
        if ( List->Pending_Push_Amount )              // still data pending from last isr-push
        { _ttc_list_complete_all_pending_isr( List ); }
        _ttc_list_push_front_single( List, Item );
#if (TTC_ASSERT_LIST_EXTRA == 1)
        Item->Caller = __builtin_return_address( 0 );  // store address of calling function for debugging
#endif
        TTC_LIST_STATS_INCREASE( List, AmountPushFrontISR );
        ttc_semaphore_give( &( List->AmountItems ), 1 );
    }
    else {                                            // already locked by task: add to list of pending items
#if (TTC_ASSERT_LIST_EXTRA == 1)
        TTC_LIST_STATS_INCREASE( List, AmountPending );
#endif
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
        Assert_LIST( ( Item->Owner == NULL ) || ( ttc_memory_is_constant( Item->Owner ) ), ttc_assert_origin_auto ); // Item still belongs to a list. Remove it from this list before freeing it!
#endif

        if ( List->Pending_Push_Front_Last ) { // append at end of list
            List->Pending_Push_Front_Last->Next = Item;
        }
        else {                         // append as first item
            List->Pending_Push_Front_First = Item;
        }
        while ( Item->Next )
        { Item = Item->Next; }
        List->Pending_Push_Front_Last = Item;
        List->Pending_Push_Amount++;
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
        Item->Owner = List;  // item now belongs to a list
#endif
#if (TTC_ASSERT_LIST_EXTRA == 1)
        Item->Caller = __builtin_return_address( 0 );
#endif

        return tle_ListInUseByTask;
    }

    return tle_OK;
}
t_ttc_list_item* ttc_list_peek_front( t_ttc_list* List ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!

    if ( ttc_semaphore_available( &( List->AmountItems ) ) ) {  // list stores at least one item

        t_ttc_list_item* Item = List->First;
#if (TTC_ASSERT_LIST_EXTRA == 1)
        Assert_LIST_VALID( List->First, ttc_assert_origin_auto );  // invalid list (did you initialize it?)
        Assert_LIST_VALID( Item, ttc_assert_origin_auto );
#endif
        if ( !Item ) { // no item in regular list: check if we have items from interrupt service
            Item = List->Pending_Push_Front_First;

            if ( !Item ) {
                Item = List->Pending_Push_Back_First;
            }
        }

        return Item;
    }
    return NULL;
}
t_base           ttc_list_pop_front_multiple( t_ttc_list* List, t_base Amount, t_ttc_list_item** FirstItem, t_ttc_list_item** LastItem ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );
    Assert_LIST_VALID( FirstItem, ttc_assert_origin_auto );
    Assert_LIST_VALID( LastItem, ttc_assert_origin_auto );

    t_base ListSize = s_ttc_listize( List );
    t_base AmountRemoved;
    if ( ( ( Amount == -1 ) && ( List->Pending_Push_Amount ) ) ||                       // about to pop all items + we have pending ones
            ( ( ListSize < Amount ) && ( ListSize + List->Pending_Push_Amount >= Amount ) ) // list too short but pending items would fullfill our needs
       ) { // complete all pending items now (only done rarely as it locks interrupts)
        ttc_interrupt_critical_begin();             // must lock interrupts before running an isr
        AmountRemoved = ttc_list_pop_front_multiple_isr( List, Amount, FirstItem, LastItem );
        ttc_interrupt_critical_end();
    }

    while ( s_ttc_listize( List ) == 0 ) { // list is empty: wait for data
        ttc_semaphore_take( &( List->AmountItems ), Amount, -1 );
    }

    ttc_mutex_lock( &( List->Lock ), -1 );
    ListSize = ( Amount == -1 ) ? // must only provide list size if popping all items
               ttc_semaphore_available( &( List->AmountItems ) + Amount ) :
               -1;

    AmountRemoved = _ttc_list_pop_front_multiple( List, ListSize, Amount, FirstItem, LastItem );
#if (TTC_ASSERT_LIST_EXTRA == 1)
    if ( *FirstItem ) {
        t_ttc_list_item* CurrentItem = *FirstItem;
        do {
            CurrentItem->Caller = __builtin_return_address( 0 ); // store address of calling function for debugging
            CurrentItem = CurrentItem->Next;
        }
        while ( ( CurrentItem != TTC_LIST_TERMINATOR ) && ( CurrentItem != *LastItem ) );
    }
#endif
    ttc_mutex_unlock( &( List->Lock ) );

    return AmountRemoved;
}
e_ttc_list_error ttc_list_pop_front_multiple_isr( t_ttc_list* List, t_base Amount, t_ttc_list_item** FirstItem, t_ttc_list_item** LastItem ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );
    Assert_LIST_VALID( FirstItem, ttc_assert_origin_auto );
    Assert_LIST_VALID( LastItem, ttc_assert_origin_auto );

    if ( ttc_mutex_is_locked( &( List->Lock ) ) )
    { return tle_ListInUseByTask; }          // list is locked by a task

    if ( List->Pending_Push_Amount )         // still data pending from last isr-push
    { _ttc_list_complete_all_pending_isr( List ); }

    if ( ttc_semaphore_take_isr( &( List->AmountItems ), Amount ) >= tsme_Error )
    { return tle_ListIsEmpty; }

    t_base ListSize = ( Amount == -1 ) ? // must only provide list size if popping all items
                      ListSize = ttc_semaphore_available( &( List->AmountItems ) + Amount ) :
                                 -1;

    _ttc_list_pop_front_multiple( List,
                                  ListSize,
                                  Amount,
                                  FirstItem,
                                  LastItem
                                );

#if (TTC_ASSERT_LIST_EXTRA == 1)
    if ( *FirstItem ) {
        t_ttc_list_item* CurrentItem = *FirstItem;
        do {
            CurrentItem->Caller = __builtin_return_address( 0 ); // store address of calling function for debugging
            CurrentItem = CurrentItem->Next;
        }
        while ( ( CurrentItem != TTC_LIST_TERMINATOR ) && ( CurrentItem != *LastItem ) );
    }
#endif
    return tle_OK;
}
t_ttc_list_item* ttc_list_pop_front_single_isr( t_ttc_list* List ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!

    // we may use static variables as interrupts are never interrupted!
    static t_ttc_list_item* Item = NULL;

    if ( ttc_mutex_is_locked( &( List->Lock ) ) )
    { return ( t_ttc_list_item* ) 1; }       // list is locked by a task

    if ( List->Pending_Push_Amount )         // still data pending from last isr-push
    { _ttc_list_complete_all_pending_isr( List ); }

    if ( ttc_semaphore_take_isr( &( List->AmountItems ), 1 ) >= tsme_Error )
    { return NULL; }

    _ttc_list_pop_front_single( List, &Item );
#if (TTC_ASSERT_LIST_EXTRA == 1)
    Item->Caller = __builtin_return_address( 0 );  // store address of calling function for debugging
#endif
    TTC_LIST_STATS_INCREASE( List, AmountPopFrontISR );

    return Item;
}
t_ttc_list_item* ttc_list_pop_front_single_try( t_ttc_list* List ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!
    t_ttc_list_item* Item;

    e_ttc_semaphore_error Error = ttc_semaphore_take_try( &( List->AmountItems ), 1 );
    if ( Error ) { // no items in regular list. Maybe we have some pending items?
        if ( List->Pending_Push_Amount ) { // complete all pending items now (only done rarely as it locks interrupts)
            ttc_interrupt_critical_begin();             // must lock interrupts before running an isr
            Item = ttc_list_pop_front_single_isr( List ); // isr will complete all pending items
            ttc_interrupt_critical_end();
            return Item;
        }
        return NULL;
    }

    ttc_mutex_lock( &( List->Lock ), -1 );

    _ttc_list_pop_front_single( List, &Item );
#if (TTC_ASSERT_LIST_EXTRA == 1)
    Item->Caller = __builtin_return_address( 0 );  // store address of calling function for debugging
#endif
    TTC_LIST_STATS_INCREASE( List, AmountPopFront );

    ttc_mutex_unlock( &( List->Lock ) );
#if (TTC_ASSERT_LIST_EXTRA == 1)
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto );  // invalid list (did you initialize it?)
    Assert_LIST_VALID( Item, ttc_assert_origin_auto );
#endif

    return Item;
}
t_ttc_list_item* ttc_list_pop_front_single_wait( t_ttc_list* List ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!

    t_ttc_list_item* Item;
    if ( ( s_ttc_listize( List ) == 0 ) && List->Pending_Push_Amount ) { // list is empty but items are pending: complete all pending items now (only done rarely as it locks interrupts)
        ttc_interrupt_critical_begin();           // must lock interrupts before running an isr
        Item = ttc_list_pop_front_single_isr( List ); // complete all pending items
        ttc_interrupt_critical_end();
        if ( Item ) // got item from pending items: return it now
        { return Item; }
    }

    e_ttc_semaphore_error Error = ttc_semaphore_take( &( List->AmountItems ), 1, -1 ); ( void ) Error;
    Assert_LIST( Error == tsme_OK, ttc_assert_origin_auto );

    ttc_mutex_lock( &( List->Lock ), -1 );
    _ttc_list_pop_front_single( List, &Item );
#if (TTC_ASSERT_LIST_EXTRA == 1)
    Item->Caller = __builtin_return_address( 0 );  // store address of calling function for debugging
#endif
    TTC_LIST_STATS_INCREASE( List, AmountPopFront );
    ttc_mutex_unlock( &( List->Lock ) );
#if (TTC_ASSERT_LIST_EXTRA == 1)
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto );  // invalid list (did you initialize it?)
    Assert_LIST_VALID( Item, ttc_assert_origin_auto );
#endif

    return Item;
}
void             ttc_list_iterate( t_ttc_list* List, void* ( *IterateFunction )( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item ), void* Argument, t_s16 Steps, t_u16 SkipFirst, t_u16 Amount ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );             // invalid pointer given!

    ttc_mutex_lock( &( List->Lock ), -1 );
    _ttc_list_iterate_from( List, IterateFunction, Argument, SkipFirst, Steps, List->First, List->Last, Amount );
    ttc_mutex_unlock( &( List->Lock ) );
}
BOOL             ttc_list_iterate_try( t_ttc_list* List, void* ( *IterateFunction )( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item ), void* Argument, t_s16 Steps, t_u16 SkipFirst, t_u16 Amount ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );             // invalid pointer given!

    if ( ! ttc_mutex_lock_try( &( List->Lock ) ) ) { // obtained lock successfully
        _ttc_list_iterate_from( List, IterateFunction, Argument, SkipFirst, Steps, List->First, List->Last, Amount );
        ttc_mutex_unlock( &( List->Lock ) );
        return TRUE;
    }

    return FALSE; // could not obtain lock
}
void             ttc_list_iterate_from( t_ttc_list* List, void* ( *IterateFunction )( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item ), void* Argument, t_s16 Steps, t_ttc_list_item* Start, t_ttc_list_item* End, t_u16 Amount ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );             // invalid pointer given!

    ttc_mutex_lock( &( List->Lock ), -1 );
    _ttc_list_iterate_from( List, IterateFunction, Argument, 0, Steps, Start, End, Amount );
    ttc_mutex_unlock( &( List->Lock ) );
}
BOOL             ttc_list_iterate_from_try( t_ttc_list* List, void* ( *IterateFunction )( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item ), void* Argument, t_s16 Steps, t_ttc_list_item* Start, t_ttc_list_item* End, t_u16 Amount ) {
    Assert_LIST_VALID( List, ttc_assert_origin_auto );             // invalid pointer given!

    if ( ! ttc_mutex_lock_try( &( List->Lock ) ) ) { // obtained lock successfully
        _ttc_list_iterate_from( List, IterateFunction, Argument, 0, Steps, Start, End, Amount );
        ttc_mutex_unlock( &( List->Lock ) );
        return TRUE;
    }

    return FALSE; // could not obtain lock
}

//}functions
//{ private functions *****************************************

void         _ttc_list_complete_all_pending_isr( t_ttc_list* List ) {
    Assert_LIST( ttc_interrupt_check_inside_isr(), ttc_assert_origin_auto ); // this function must be called with disable interrupts or from interrupt service routine only. Check implementation of caller!
    if ( List->Pending_Push_Front_First ) { // complete pending pushes to list head
        _ttc_list_push_front_multiple( List, List->Pending_Push_Front_First, List->Pending_Push_Front_Last );
        List->Pending_Push_Front_First  = NULL;
        List->Pending_Push_Front_Last   = NULL;
    }
    if ( List->Pending_Push_Back_First ) {  // complete pending pushes to list end
        _ttc_list_push_back_multiple( List,
                                      List->Pending_Push_Back_First,
                                      List->Pending_Push_Back_Last
                                    );
        List->Pending_Push_Back_First  = NULL;
        List->Pending_Push_Back_Last   = NULL;
    }

    // adjust list size
    ttc_semaphore_give_isr( &( List->AmountItems ), List->Pending_Push_Amount );
    Assert_LIST_EXTRA( ttc_semaphore_available( &( List->AmountItems ) ) == ttc_list_count_items( List->First ), ttc_assert_origin_auto );  // bug in size counter implementation
    List->Pending_Push_Amount = 0;

    TTC_LIST_STATS_INCREASE( List, AmountPendingCompletion );
}
void         _ttc_list_iterate_from( t_ttc_list* List, void* ( *IterateFunction )( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item ), void* Argument, t_u16 SkipFirst, t_s16 Steps, t_ttc_list_item* Start, t_ttc_list_item* End, t_u16 Amount ) {
    Assert_LIST_VALID_EXTRA( List, ttc_assert_origin_auto );             // invalid pointer given!
    Assert_LIST_Readable( IterateFunction, ttc_assert_origin_auto ); // invalid pointer given!
    Assert_LIST_EXTRA( Steps > 0, ttc_assert_origin_auto ); // invalid argument (backward stepping might be added in the future). Check caller code!
    if ( !Start )
    { return; }                    // nothing to do (ttc_list does not use NULL as item pointers)
    if ( !End )
    { End = TTC_LIST_TERMINATOR; } // ttc_list uses a special value to terminate last item in list

    t_ttc_list_item* Item = Start;

    t_ttc_list_item* PreviousItem = NULL;
    while ( SkipFirst && ( Item != TTC_LIST_TERMINATOR ) ) { // skip first items until start item
        Assert_LIST_EXTRA_Writable( Item, ttc_assert_origin_auto ); // invalid item pointer. Check list implementation! (Memory corrupted?)
        t_ttc_list_item* NextItem = Item->Next;

        SkipFirst--;
        if ( 1 ) { // proceed to next item
            PreviousItem = Item;
            Item         = NextItem; // this is correct next pointer even if IterateFunction() changed Item.Next

            t_s16 Count = Steps;
            while ( ( Item != TTC_LIST_TERMINATOR ) && ( --Count > 0 ) ) {  // skip additional items
                Assert_LIST_Writable( Item->Next, ttc_assert_origin_auto ); // next pointer must point to RAM. Memory corrupted? Check implementation of ttc_list and your application!
                PreviousItem = Item;
                Item = Item->Next;
            }
        }
    }

    // iterate forward First -> Last
    while ( Amount && ( Item != TTC_LIST_TERMINATOR ) ) {
        Assert_LIST_Writable( Item, ttc_assert_origin_auto ); // invalid item pointer. Check list implementation! (Memory corrupted?)
        t_ttc_list_item* NextItem = Item->Next;
        Assert_LIST( NextItem != NULL,   ttc_assert_origin_auto ); // invalid next pointer (ttc_list uses TTC_LIST_TERMINATOR to terminate lists)
#if (TTC_ASSERT_LIST_EXTRA == 1)
        Assert_LIST( Item->Owner == List, ttc_assert_origin_auto ); // someone outside has modified this item. Check implementation of ttc_list and your application!
#endif

        void( *RemoveItem )( void* Argument, t_ttc_list_item * Item ) = ( void( * )( void*, t_ttc_list_item* ) ) IterateFunction( Argument, PreviousItem, Item ); // call iterate function
        Assert_LIST( Item->Next == NextItem, ttc_assert_origin_auto ); // IterateFunction() is not allowed to modify Item. Check implementation of IterateFunction()!
#if (TTC_ASSERT_LIST_EXTRA == 1)
        Assert_LIST( Item->Owner == List,     ttc_assert_origin_auto ); // IterateFunction() is not allowed to modify Item. Check implementation of IterateFunction()!
#endif
        if ( RemoveItem ) { // removing current item
            e_ttc_semaphore_error Error = ttc_semaphore_take_try( &( List->AmountItems ), 1 ); ( void ) Error;
            Assert_LIST_EXTRA( !Error, ttc_assert_origin_auto ); // list should be locked before calling this function. Check list implementation!

            // remove item from its list (allows to add it to other lists or to release it to its pool)
            _ttc_list_remove_item( List, PreviousItem, Item, NextItem );

            if ( 1 != ( t_base ) RemoveItem ) { // function pointer given: execute given function
                Assert_LIST_Readable( RemoveItem, ttc_assert_origin_auto ); // returned pointer does not seem to point to executable code. Check implementation of IterateFunction()!
                RemoveItem( Argument, Item );
            }
            Item = PreviousItem;
        }
        if ( Amount != -1 )
        { Amount--; }

        if ( 1 ) { // proceed to next item
            PreviousItem = Item;
            Item         = NextItem; // this is correct next pointer even if IterateFunction() changed Item.Next

            t_s16 Count = Steps;
            while ( ( Item != TTC_LIST_TERMINATOR ) && ( --Count > 0 ) ) {  // skip additional items
                Assert_LIST_Writable( Item->Next, ttc_assert_origin_auto ); // next pointer must point to RAM. Memory corrupted? Check implementation of ttc_list and your application!
                PreviousItem = Item;
                Item = Item->Next;
            }
        }
    }
}
void         _ttc_list_push_back_single( t_ttc_list* List, t_ttc_list_item* Item ) {
#if (TTC_ASSERT_LIST_EXTRA == 1)
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!
    Assert_LIST_VALID( Item, ttc_assert_origin_auto );
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto );  // invalid list (did you initialize it?)
    Assert_LIST_EXTRA( ttc_semaphore_available( &( List->AmountItems ) ) == ttc_list_count_items( List->First ), ttc_assert_origin_auto );  // bug in size counter implementation. Caller must add 1 to List->AmountItem after calling this function!
    Assert_LIST( List->LockedBy == tlo_None, ttc_assert_origin_auto );  // outer lock has been broken
    List->LockedBy = tlo_PushBackSingle;
    TTC_LIST_ADD_HISTORY( List );
#endif

#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
    Assert_LIST( ( Item->Owner == NULL ) || ( ttc_memory_is_constant( Item->Owner ) ), ttc_assert_origin_auto ); // Item still belongs to a list. Remove it from this list before freeing it!
#endif

    // push of multiple items would not allow to terminate within constant time!
    Item->Next = TTC_LIST_TERMINATOR;
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
    Item->Owner = List;  // item now belongs to a list
#endif

    if ( List->Last ) { // list is not empty: append item at end of list
        List->Last->Next = Item;
        List->Last       = Item;
    }
    else {              // list is empty: set list of new items as new list
        List->First = Item;
        List->Last  = Item;
    }

#if (TTC_ASSERT_LIST_EXTRA == 1)
    if ( ( Item->Next ) != TTC_LIST_TERMINATOR ) {
        Assert_LIST_VALID( Item->Next, ttc_assert_origin_auto );
    }
    if ( ( List->Last->Next ) != TTC_LIST_TERMINATOR ) {
        Assert_LIST_VALID( List->Last->Next, ttc_assert_origin_auto );
    }
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto );  // invalid list (did you initialize it?)
    Assert_LIST_VALID( List->Last, ttc_assert_origin_auto );
    Assert_LIST( ( ttc_semaphore_available( &( List->AmountItems ) ) + 1 ) == ttc_list_count_items( List->First ), ttc_assert_origin_auto );  // bug in size counter implementation
    List->LockedBy        = tlo_None;
    TTC_LIST_ADD_HISTORY( List );
#endif
}
void         _ttc_list_push_back_multiple( t_ttc_list* List, t_ttc_list_item* FirstItem, t_ttc_list_item* LastItem ) {
#if (TTC_ASSERT_LIST_EXTRA == 1)
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!
    Assert_LIST_VALID( FirstItem, ttc_assert_origin_auto );
    Assert_LIST_VALID( LastItem, ttc_assert_origin_auto );
    if ( FirstItem ) {
        Assert_LIST_EXTRA_Writable( FirstItem, ttc_assert_origin_auto ); // If first item is set, last item must be set too. Check implementation of ttc_list!
    }
    Assert_LIST_EXTRA( ttc_semaphore_available( &( List->AmountItems ) ) == ttc_list_count_items( List->First ), ttc_assert_origin_auto );  // bug in size counter implementation. Caller must add Amount to List->AmountItem after calling this function!
    Assert_LIST( List->LockedBy == tlo_None, ttc_assert_origin_auto );  // outer lock has been broken
    List->LockedBy = tlo_PushBackMultiple;
    TTC_LIST_STATS_INCREASE( List, AmountPushBackMultiple );
    TTC_LIST_ADD_HISTORY( List );
#endif

    if ( List->Last ) { // list is not empty: append item at end of list
        List->Last->Next = FirstItem;
        List->Last       = LastItem;
    }
    else {              // list is empty: set list of new items as new list
        List->First = FirstItem;
        List->Last  = LastItem;
    }

#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
    do { // set ->Owner field of all items
        Assert_LIST_VALID_EXTRA( FirstItem, ttc_assert_origin_auto );
        Assert_LIST( ( FirstItem->Owner == NULL ) || ( FirstItem->Owner == List ), ttc_assert_origin_auto ); // Item still belongs to a list. Remove it from this list before freeing it!
        FirstItem->Owner = List;
        FirstItem = FirstItem->Next;
    }
    while ( ( FirstItem != LastItem ) && ( FirstItem != TTC_LIST_TERMINATOR ) );
#endif

#if (TTC_ASSERT_LIST_EXTRA == 1)
    List->LockedBy        = tlo_None;
    TTC_LIST_ADD_HISTORY( List );
#endif
}
void         _ttc_list_push_front_single( t_ttc_list* List, t_ttc_list_item* Item ) {
#if (TTC_ASSERT_LIST_EXTRA == 1)
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!
    Assert_LIST_VALID( Item, ttc_assert_origin_auto );
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto );  // invalid list (did you initialize it?)
    Assert_LIST_EXTRA( ttc_semaphore_available( &( List->AmountItems ) ) == ttc_list_count_items( List->First ), ttc_assert_origin_auto );  // bug in size counter implementation. Caller must add 1 to List->AmountItem after calling this function!
    Assert_LIST( List->LockedBy == tlo_None, ttc_assert_origin_auto );  // outer lock has been broken
    List->LockedBy = tlo_PushFrontSingle;
    TTC_LIST_ADD_HISTORY( List );
#endif

#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
    Assert_LIST( ( Item->Owner == NULL ) || ( ttc_memory_is_constant( Item->Owner ) ), ttc_assert_origin_auto ); // Item still belongs to a list. Remove it from this list before freeing it!
#endif

    if ( List->First ) { // list is not empty: append item at head of list
        Item->Next  = List->First;
        List->First = Item;
    }
    else {               // list is empty: set list of new items as new list
        List->First = Item;
        List->Last  = Item;

        // push of multiple items would not allow to terminate within constant time!
        Item->Next = TTC_LIST_TERMINATOR;
    }
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
    Item->Owner = List;  // item now belongs to a list
#endif

#if (TTC_ASSERT_LIST_EXTRA == 1)
    if ( ( Item->Next ) != TTC_LIST_TERMINATOR ) {
        Assert_LIST_VALID( Item->Next, ttc_assert_origin_auto );
    }
    if ( ( List->Last->Next ) != TTC_LIST_TERMINATOR ) {
        Assert_LIST_VALID( List->Last->Next, ttc_assert_origin_auto );
    }
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto );  // invalid list (did you initialize it?)
    Assert_LIST_VALID( List->Last, ttc_assert_origin_auto );
    Assert_LIST( ( ttc_semaphore_available( &( List->AmountItems ) ) + 1 ) == ttc_list_count_items( List->First ), ttc_assert_origin_auto );  // bug in size counter implementation
    List->LockedBy        = tlo_None;
    TTC_LIST_ADD_HISTORY( List );
#endif
}
void         _ttc_list_push_front_multiple( t_ttc_list* List, t_ttc_list_item* FirstItem, t_ttc_list_item* LastItem ) {
#if (TTC_ASSERT_LIST_EXTRA == 1)
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!
    Assert_LIST_VALID( FirstItem, ttc_assert_origin_auto );
    Assert_LIST_VALID( LastItem, ttc_assert_origin_auto );
    if ( FirstItem ) {
        Assert_LIST_EXTRA_Writable( FirstItem, ttc_assert_origin_auto ); // If first item is set, last item must be set too. Check implementation of ttc_list!
    }
    Assert_LIST_EXTRA( ttc_semaphore_available( &( List->AmountItems ) ) == ttc_list_count_items( List->First ), ttc_assert_origin_auto );  // bug in size counter implementation. Caller must add 1 to List->AmountItem after calling this function!
    Assert_LIST( List->LockedBy == tlo_None, ttc_assert_origin_auto );  // outer lock has been broken
    List->LockedBy = tlo_PushFrontMultiple;
    TTC_LIST_STATS_INCREASE( List, AmountPushFront );
    TTC_LIST_ADD_HISTORY( List );
#endif

    if ( List->Last ) { // list is not empty: append item before first item in list
        LastItem->Next = List->First;
        List->First    = FirstItem;
    }
    else {              // list is empty: set list of new items as new list
        List->First = FirstItem;
        List->Last  = LastItem;
    }

#if (TTC_ASSERT_LIST_EXTRA == 1)
    do { // check all items to be added
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
        Assert_LIST( FirstItem->Owner == NULL, ttc_assert_origin_auto ); // Item still belongs to a list. Remove it from this list before freeing it!
        FirstItem->Owner = List; // item belongs to this list now
#endif
        FirstItem = FirstItem->Next;
        Assert_LIST_VALID_EXTRA( FirstItem, ttc_assert_origin_auto );
    }
    while ( ( FirstItem != TTC_LIST_TERMINATOR ) && ( FirstItem != NULL ) && ( FirstItem != LastItem ) );

    List->LockedBy        = tlo_None;
    TTC_LIST_ADD_HISTORY( List );
#endif
}
void         _ttc_list_pop_front_single( t_ttc_list* List, t_ttc_list_item** Item ) {
    t_ttc_list_item* FirstItem = List->First;

#if (TTC_ASSERT_LIST_EXTRA == 1)
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!
    Assert_LIST_VALID( Item, ttc_assert_origin_auto );
    Assert_LIST( ( ttc_semaphore_available( &( List->AmountItems ) ) + 1 ) == ttc_list_count_items( FirstItem ), ttc_assert_origin_auto );  // bug in size counter implementation. Caller must take 1 from List->AmountItem before calling this function!
    Assert_LIST( List->LockedBy == tlo_None, ttc_assert_origin_auto );  // outer lock has been broken
    List->LockedBy = tlo_PopFrontSingle;
    TTC_LIST_ADD_HISTORY( List );
#endif

#if (TTC_ASSERT_LIST_EXTRA == 1)
    Assert_LIST_VALID( FirstItem, ttc_assert_origin_auto );
#endif
    if ( FirstItem ) {   // list stores items: remove first one
#if (TTC_ASSERT_LIST_EXTRA == 1)
        if ( ( List->First->Next ) != TTC_LIST_TERMINATOR ) {
            Assert_LIST_VALID( List->First->Next, ttc_assert_origin_auto );
        }
#endif
        if ( List->First->Next == TTC_LIST_TERMINATOR ) { // list is empty now
            List->First = NULL;
            List->Last  = NULL;
        }
        else
        { List->First = List->First->Next; }
    }

    if ( Item ) {
        *Item = FirstItem; // store item in given pointer

#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
        FirstItem->Owner = NULL; // item is free now
#endif
#if (TTC_ASSERT_LIST_EXTRA == 1)
        FirstItem->Caller = __builtin_return_address( 0 );  // store address of calling function for debugging
#endif
    }

#if (TTC_ASSERT_LIST_EXTRA == 1)
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto );  // invalid list (did you initialize it?)
    Assert_LIST_EXTRA( ttc_semaphore_available( &( List->AmountItems ) ) == ttc_list_count_items( List->First ), ttc_assert_origin_auto );  // bug in size counter implementation
    List->LockedBy        = tlo_None;
    TTC_LIST_ADD_HISTORY( List );
#endif
}
t_base       _ttc_list_pop_front_multiple( t_ttc_list* List, t_base ListSize, t_base Amount, t_ttc_list_item** FirstItem, t_ttc_list_item** LastItem ) {
#if (TTC_ASSERT_LIST_EXTRA == 1)
    Assert_LIST( List, ttc_assert_origin_auto );
    Assert_LIST( FirstItem, ttc_assert_origin_auto );
    Assert_LIST( LastItem, ttc_assert_origin_auto );
    Assert_LIST_EXTRA( ttc_semaphore_available( &( List->AmountItems ) ) + Amount == ttc_list_count_items( List->First ), ttc_assert_origin_auto ); // bug in size counter implementation. Caller must take Amount from List->AmountItem before calling this function!
    Assert_LIST( List->LockedBy == tlo_None, ttc_assert_origin_auto );  // outer lock has been broken
    List->LockedBy = tlo_PopFrontMultiple;
    TTC_LIST_STATS_INCREASE( List, AmountPopFront );
    TTC_LIST_ADD_HISTORY( List );
#endif

    if ( List->First == NULL ) { // list is empty
        *FirstItem = NULL;
        *LastItem  = NULL;
        return 0;
    }
    if ( Amount == -1 ) {      // request for all items in list
        Amount      = ListSize;
        *FirstItem  = List->First;
        *LastItem   = List->Last;
        List->First = NULL;
        List->Last  = NULL;

#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
        t_ttc_list_item* CurrentItem = List->First;
        if ( CurrentItem ) { // free all items from this list
            do {
                CurrentItem->Owner = NULL; // Item is free now
                CurrentItem = CurrentItem->Next;
            }
            while ( CurrentItem  && ( CurrentItem != TTC_LIST_TERMINATOR ) );
        }
#endif
    }
    else {                     // request for limited amount of items
        if ( Amount > ListSize ) { // list has less items than requested
            Amount = ListSize;
        }
        t_ttc_list_item* CurrentItem = List->First;
        *FirstItem = CurrentItem;

        t_base Count = Amount;
        while ( Count-- > 0 ) { // collect requested amount of items (find last one)
            Assert_LIST_VALID_EXTRA( CurrentItem, ttc_assert_origin_auto );
            t_ttc_list_item* NextItem = CurrentItem->Next;
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
            CurrentItem->Owner = NULL; // Item is free now
#endif
            CurrentItem = NextItem;
        }
        *LastItem   = CurrentItem;
        List->First = CurrentItem;
        if ( CurrentItem == TTC_LIST_TERMINATOR ) // list is empty now
        { List->First = NULL; List->Last = NULL; }
    }

#if (TTC_ASSERT_LIST_EXTRA == 1)
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto );  // invalid list (did you initialize it?)
    Assert_LIST_EXTRA( ttc_semaphore_available( &( List->AmountItems ) ) == ttc_list_count_items( List->First ), ttc_assert_origin_auto );  // bug in size counter implementation
    List->LockedBy        = tlo_None;
    TTC_LIST_ADD_HISTORY( List );
#endif

    return Amount;
}
void         _ttc_list_remove_item( t_ttc_list* List, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item, t_ttc_list_item* NextItem ) {
    Assert_LIST_Writable( List, ttc_assert_origin_auto );    // invalid argument: Check implementation of caller!
    Assert_LIST_Writable( Item, ttc_assert_origin_auto );    // invalid argument: Check implementation of caller!
    Assert_LIST( NextItem != NULL, ttc_assert_origin_auto ); // invalid argument: Use TTC_LIST_TERMINATOR instead of NULL!

#if (TTC_ASSERT_LIST_EXTRA == 1)
    Assert_LIST_VALID( List, ttc_assert_origin_auto );        // invalid pointer given!
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto );
    Assert_LIST_VALID( PreviousItem, ttc_assert_origin_auto );
    Assert_LIST_Writable( Item, ttc_assert_origin_auto );
    Assert_LIST_EXTRA( ( ttc_semaphore_available( &( List->AmountItems ) ) + 1 ) == ttc_list_count_items( List->First ), ttc_assert_origin_auto ); // bug in size counter implementation. Caller must take 1 from List->AmountItem before calling this function!
    Assert_LIST( List->LockedBy == tlo_None, ttc_assert_origin_auto );  // outer lock has been broken
    List->LockedBy = tlo_RemoveItem;
    TTC_LIST_STATS_INCREASE( List, AmountRemoveItem );
    TTC_LIST_ADD_HISTORY( List );
#endif

    if ( List->First ) {   // list stores >=1 items: remove given one
        if ( PreviousItem ) {       // redirect link of previous item
            PreviousItem->Next = NextItem;
            if ( Item == List->Last ) { // removing at list tail
                List->Last = PreviousItem;
            }
        }
        else {                      // removing at list head
            if ( NextItem != TTC_LIST_TERMINATOR )
            { List->First = NextItem; }
            else { // list is empty now
                List->First = NULL;
                List->Last  = NULL;
            }
        }

#if (TTC_ASSERT_LIST_EXTRA == 1)
        Item->Caller = __builtin_return_address( 0 );  // store address of calling function for debugging
#endif
#if (TTC_LIST_ITEMS_IN_ONE_LIST_ONLY == 1)
        Item->Owner = NULL; // Item is free now
#endif

#if (TTC_ASSERT_LIST_EXTRA == 1)
        if ( List->First && ( ( List->First->Next ) != TTC_LIST_TERMINATOR ) ) {
            Assert_LIST_VALID( List->First->Next, ttc_assert_origin_auto );
        }
#endif
    }

#if (TTC_ASSERT_LIST_EXTRA == 1)
    Assert_LIST_VALID( List->First, ttc_assert_origin_auto );  // invalid list (did you initialize it?)
    Assert_LIST_EXTRA( ttc_semaphore_available( &( List->AmountItems ) ) == ttc_list_count_items( List->First ), ttc_assert_origin_auto );  // bug in size counter implementation
    List->LockedBy        = tlo_None;
    TTC_LIST_ADD_HISTORY( List );
#endif
}

//} private functions
