#ifndef TTC_GFX
#define TTC_GFX
/*{ Template_FreeRTOS::.h ************************************************
}*/

//{ Includes *************************************************************
#include "ttc_basic.h"
#include "ttc_memory.h"
#include "ttc_string.h"
#include "gfx/gfx_image.h"
#include "math/math_trigonometry.h"

#ifdef EXTENSION_510_ttc_font
  #include "ttc_font.h"
#endif

#ifdef EXTENSION_400_lcd_320x240_ili9320 // used on Mini-STM32
  #include "gfx/lcd_ili9320.h"
#endif
#ifdef EXTENSION_400_lcd_320x240_K320QVB // used on Olimex STM32-LCD
 #include "lcd_ili9320.h"
 // #include "lcd_k320qvb.h"
#include "lcd_ili9320.h"
#endif
#ifdef EXTENSION_500_ttc_input
  #include "ttc_input.h"
#endif

#include "ttc_gfx_types.h"

//} Includes

//{ Defines/ TypeDefs **************************************************** //if = 0 orientation is vertical, otherwise it is horizontal

//} Defines

//{ Structures/ Enums ****************************************************
//} Structures/ Enums

//{ Global Variables *****************************************************

///* DO NOT USE THE FUNCTION POINTERS FROM OUTSIDE!
extern void (*function_put_char)(u8_t Char);
extern void (*function_put_char_solid)(u8_t Char);
extern void (*function_clear)();
extern void (*function_line)(u16_t X1, u16_t Y1, u16_t X2, u16_t Y2);
extern void (*function_line_horizontal)(u16_t X, u16_t Y, s16_t Length);
extern void (*function_line_vertical)(u16_t X, u16_t Y, s16_t Length);
extern void (*function_rect)(u16_t X,u16_t Y, s16_t Width, s16_t Height);
extern void (*function_rect_fill)(u16_t X, u16_t Y, s16_t Width, s16_t Height);
extern void (*function_circle)(u16_t CenterX, u16_t CenterY, u16_t Radius);
extern void (*function_circle_segment)(u16_t CenterX, u16_t CenterY, u16_t Radius, u16_t StartAngle, u16_t EndAngle);
extern void (*function_circle_fill)(u16_t CenterX, u16_t CenterY, u16_t Radius);
extern void (*function_set_backlight)(u8_t Level);
//*/

//} Global Variables

//{ Function prototypes **************************************************

/** returns amount of display devices available in current configuration
 * @return amount of available graphic displays
 */
u8_t ttc_gfx_get_max_display_index();

/** returns configuration of indexed display (asserts if no valid configuration was found)
 *
 * @param DisplayIndex    device index of display to init (1..ttc_display_get_max_DisplayIndex())
 * @return                configuration of indexed Display
 */
ttc_gfx_generic_t* ttc_gfx_get_configuration(u8_t DisplayIndex);

/** switch current display (if >1 displays have been configured)
 *
 * @param DisplayIndex    device index of display to init (1..ttc_display_get_max_DisplayIndex())
 */
void ttc_gfx_display(u8_t DisplayIndex);

/** draws line in current ColorFg from (X,Y) to (X+DX-1, Y+DY-1)
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param X1              x-coordinate of starting point
 * @param Y1              y-coordinate of starting point
 * @param X2              x-coordinate of end point
 * @param Y2              x-coordinate of end point
 */
#define ttc_gfx_line(X1, Y1, X2, Y2) function_line(X1, Y1, X2, Y2)
// inline void ttc_gfx_line(u16_t X1, u16_t Y1, u16_t X2, u16_t Y2) { function_line(X1, Y1, X2, Y2); }
void _ttc_gfx_generic_line(u16_t X1, u16_t Y1, u16_t X2, u16_t Y2);

/** draws line in current ColorFg from (X, Y) to (Left+Length-1, Y)
 *
 * Note: Before calling this function, make sure that ttc_gfx_CurrentDisplay has correct value!
 *
 * @param Left            x-coordinate of starting point
 * @param Y               y-coordinate of starting point
 * @param Length          distance to end point of line
 * @return                !=NULL: pointer to struct ttc_display_generic_t
 */
#define ttc_gfx_line_horizontal(X1, Y1, Length) function_line_horizontal(X1, Y1, Length)
// inline void ttc_gfx_line_horizontal(u16_t X1, u16_t Y1, s16_t Length) { ttc_gfx_mt_line(X1, Y1, Length); }
void _ttc_gfx_generic_line_horizontal(u16_t X, u16_t Y, s16_t Length);

/** draws line in current ColorFg from (X, Y) to (X, Y + Length-1)
 *
 * Note: Before calling this function, make sure that ttc_gfx_CurrentDisplay has correct value!
 *
 * @param Left            x-coordinate of starting point
 * @param Y               y-coordinate of starting point
 * @param Length          distance to end point of line
 * @return                !=NULL: pointer to struct ttc_display_generic_t
 */
#define ttc_gfx_line_vertical(X, Y, Length) function_line_vertical(X, Y, Length)
// inline void ttc_gfx_line_vertical(u16_t X, u16_t Y, s16_t Length) { function_line_vertical(X1, Y1, X2, Y2); }
void _ttc_gfx_generic_line_vertical(u16_t X, u16_t Y, s16_t Length);

/** draws rectangle in current ColorFg from (X,Y) to (X+Width-1, Y+Height-1)
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param X               x-coordinate of starting point
 * @param Y               y-coordinate of starting point
 * @param Width           distance to corner opposing starting point in x-direction
 * @param Height          distance to corner opposing starting point in y-direction
 */
#define ttc_gfx_rect(X, Y, Width, Height)  function_rect(X, Y, Width, Height)
// inline void ttc_gfx_rect(u16_t X, u16_t Y, s16_t Width, s16_t Height) { function_rect(X, Y, Width, Height); }
void _ttc_gfx_generic_rect(u16_t X, u16_t Y, s16_t Width, s16_t Height);

/** draws filled rectangle in current ColorFg from (X,Y) to (X+Width-1, Y+Height-1)
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param X               x-coordinate of starting point
 * @param Y               y-coordinate of starting point
 * @param Width           distance to corner opposing starting point in x-direction
 * @param Height          distance to corner opposing starting point in y-direction
 */
#define ttc_gfx_rect_fill(X, Y, Width, Height)  function_rect_fill(X, Y, Width, Height)
// inline void ttc_gfx_rect_fill(u16_t X,u16_t Y, s16_t Width,s16_t Height) { function_rect_fill(X, Y, Width, Height); }
void _ttc_gfx_generic_rect_fill(u16_t X,u16_t Y, s16_t Width,s16_t Height);

/** draws circle in current ColorFg around (CenterX, CenterY) of radius Radius
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param CenterX         x-coordinate of starting point
 * @param CenterY         y-coordinate of starting point
 * @param Radius          distance to corner opposing starting point in x-direction
 */
#define ttc_gfx_circle(CenterX, CenterY, Radius) function_circle(CenterX, CenterY, Radius)
// inline void ttc_gfx_circle(u16_t CenterX, u16_t CenterY, u16_t Radius) { function_circle(CenterX, CenterY, Radius); }
void _ttc_gfx_generic_circle(u16_t CenterX, u16_t CenterY, u16_t Radius);

/** draws filled circle in current ColorFg around (CenterX, CenterY) of radius Radius
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param CenterX         x-coordinate of starting point
 * @param CenterY         y-coordinate of starting point
 * @param Radius          distance to corner opposing starting point in x-direction
 */
#define ttc_gfx_circle_fill(CenterX, CenterY, Radius) function_circle_fill(CenterX, CenterY, Radius)
// void _ttc_gfx_circle_fill(u16_t CenterX, u16_t CenterY, u16_t Radius) { function_circle_fill(CenterX, CenterY, Radius); }
void _ttc_gfx_generic_circle_fill(u16_t CenterX, u16_t CenterY, u16_t Radius);

/** draws circle segment in current ColorFg around (CenterX, CenterY) of radius Radius
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param CenterX         x-coordinate of starting point
 * @param CenterY         y-coordinate of starting point
 * @param StartAngle      0..360 - circle segment is drawn counter clockwise
 * @param EndAngle        0..360 - circle segment is drawn counter clockwise
 * @param Radius          distance to corner opposing starting point in x-direction
 */
#define ttc_gfx_circle_segment(CenterX, CenterY, Radius, StartAngle, EndAngle) function_circle_segment(CenterX, CenterY, Radius, StartAngle, EndAngle)
// inline void ttc_gfx_circle_segment(u16_t CenterX, u16_t CenterY, u16_t Radius, u16_t StartAngle, u16_t EndAngle) { function_circle_segment(CenterX, CenterY, Radius, StartAngle, EndAngle); }
void _ttc_gfx_generic_circle_segment(u16_t CenterX, u16_t CenterY, u16_t Radius, u16_t StartAngle, u16_t EndAngle);

void ttc_gfx_image(u16_t X, u16_t Y, gfxImage *img);

/** initialize indexed display for use
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 */
void ttc_gfx_init(u8_t DisplayIndex);

/** clear display with current background color
  *
  * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
  */
void ttc_gfx_clear();
#define ttc_gfx_clear() function_clear()
// inline void ttc_gfx_clear() { function_clear(); }
void _ttc_gfx_generic_clear();


/** reset display
  *
  * @param DisplayIndex  1..ttc_gfx_get_max_DisplayIndex() index of display to use
  */
void ttc_gfx_reset(u8_t DisplayIndex);

/** sets level of backlight (if available)
  *
  * @param Level         Brightness level (0 = off, 255 = max)
  */
#define ttc_gfx_set_backlight(Level) function_set_backlight(Level)
// inline void ttc_gfx_set_backlight(u8_t Level) { function_set_backlight(Level); }
void _ttc_gfx_generic_set_backlight(u8_t Level);

/** Moves pixel cursor to a new location.
  *
  * Many drawing operations start at the current cursor position.
  * Some drawing operations automatically move the cursor.
  *
  * @param X   x-coordinate of new drawing position
  * @param Y   x-coordinate of new drawing position
  */
void ttc_gfx_set_cursor(u16_t X,u16_t Y);
/** little bit faster than ttc_gfx_set_cursor(); does not check if coordinates are within screen */
#define _ttc_gfx_set_cursor(X, Y)  ttc_gfx_CurrentDisplay->CursorX=X; ttc_gfx_CurrentDisplay->CursorY=Y; _ttc_gfx_driver_set_cursor(X, Y)

void ttc_gfx_set_color_bg24(u32_t Color);
void ttc_gfx_set_color_bg_palette(u8_t PaletteIndex);
void ttc_gfx_set_color_fg24(u32_t Color);
void ttc_gfx_set_color_fg_palette(u8_t PaletteIndex);
void ttc_gfx_set_palette(u16_t* Palette, u8_t Size);
void ttc_gfx_set_pixel(u16_t X, u16_t Y);

/** set foreground color to given 16-bit value
  *
  * @param Color  16-bit values are display specific use ttc_gfx_set_color_fg24() for conversion
  */
void ttc_gfx_set_color_fg16(u16_t Color);

/** set background color to given 16-bit value
  *
  * @param Color  16-bit values are display specific use ttc_gfx_set_color_fg24() for conversion
  */
void ttc_gfx_set_color_bg16(u16_t Color);

#ifdef TTC_FONT1  // font handling functions requires activated font

/** set font to be used for text operations
 *
 * Note: call ttc_gfx_display() before to change to desired display (multi-displays only)
 * @param Font            font to use for all further text operations
 */
c
void _ttc_gfx_update_configuration(u8_t DisplayIndex);

/** function which writes on every place on the lcd, indepedent from rows and columns
 * @param X start position on the left side of the text
 * @param Y start position on the top side of the text
 * @param String you want to print
 *  @param MaxSize maximum size of the string which will be printed
 */
void ttc_gfx_print_between(u16_t X, u16_t Y, const char* String, Base_t MaxSize);

//}private functions
//{ Macros

//}Macros
//{ Check availability of low-level driver macros

/** paint pixel at given position in foreground color + move cursor to the right */
#ifndef _ttc_gfx_driver_set_pixel_at // no implementation available
  #warning Missing low-level implementation! (Did you forget to activate a display? Did you activate correct board?)
  #define _ttc_gfx_driver_set_pixel_at(X,Y) ttc_gfx_missing_implementation(X, Y)
#endif

/** paint pixel at given position in background color + move cursor to the right */
#ifndef _ttc_gfx_driver_clr_pixel_at // no implementation available
  #warning Missing low-level implementation!
  #define _ttc_gfx_driver_clr_pixel_at(X,Y) ttc_gfx_missing_implementation(X, Y)
#endif

/** move pixel cursor to given position */
#ifndef _ttc_gfx_driver_set_cursor // no implementation available
  #warning Missing low-level implementation!
  #define _ttc_gfx_driver_set_cursor(X,Y) ttc_gfx_missing_implementation(X, Y)
#endif

//}Macros



#endif
