/** { ttc_rtc.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level interface for rtc device.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_rtc.h"

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of rtc devices.
 *
 */

#if TTC_RTC_AMOUNT == 0
#warning No RTC devices defined, check your makefile! (did you forget to activate something?)
#endif

// for each initialized device, a pointer to its generic and stm32l1xx definitions is stored
A_define( t_ttc_rtc_config*, ttc_rtc_configs, TTC_RTC_MAX_AMOUNT );


// Set to TRUE on first call of ttc_rtc_init()
BOOL driver_Initialized = 0;

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8              ttc_rtc_get_max_index() {
    return TTC_RTC_AMOUNT;
}
t_ttc_rtc_config* ttc_rtc_get_configuration( t_u8 LogicalIndex ) {
    Assert_RTC( LogicalIndex , ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_rtc_config* Config = A( ttc_rtc_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    #ifdef EXTENSION_cpu_stm32l1xx
    if ( !Config ) {
        Config = A( ttc_rtc_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_rtc_config ) );
        Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_ttc_rtc_architecture ) );
        ttc_rtc_load_defaults( LogicalIndex );
    }
    #endif

    return Config;
}
void              ttc_rtc_deinit( t_u8 LogicalIndex ) {
    Assert_RTC( LogicalIndex , ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_rtc_config* Config = ttc_rtc_get_configuration( LogicalIndex );

    e_ttc_rtc_errorcode Result = _driver_rtc_deinit( Config );
    if ( Result == ec_rtc_OK )
    { Config->Flags.Bits.Initialized = 0; }
}
void              ttc_rtc_init( t_u8 LogicalIndex ) {
    Assert_RTC( LogicalIndex , ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_rtc_config* Config = ttc_rtc_get_configuration( LogicalIndex );


    e_ttc_rtc_errorcode Result = _driver_rtc_init( Config );
    if ( Result == ec_rtc_OK )
    { Config->Flags.Bits.Initialized = 1; }



}
void              ttc_rtc_load_defaults( t_u8 LogicalIndex ) {
    Assert_RTC( LogicalIndex , ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_rtc_config* Config = ttc_rtc_get_configuration( LogicalIndex );

    ttc_memory_set( Config, 0, sizeof( t_ttc_rtc_config ) );

    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    Config->PhysicalIndex = ttc_rtc_logical_2_physical_index( LogicalIndex );

    _driver_rtc_load_defaults( Config );
}
t_u8              ttc_rtc_logical_2_physical_index( t_u8 LogicalIndex ) {

    switch ( LogicalIndex ) {         // determine base register and other low-level configuration data
            #ifdef TTC_RTC1
        case 1: return TTC_RTC1;
            #endif
            #ifdef TTC_RTC2
        case 2: return TTC_RTC2;
            #endif
            #ifdef TTC_RTC3
        case 3: return TTC_RTC3;
            #endif
            #ifdef TTC_RTC4
        case 4: return TTC_RTC4;
            #endif
            #ifdef TTC_RTC5
        case 5: return TTC_RTC5;
            #endif
            #ifdef TTC_RTC6
        case 6: return TTC_RTC6;
            #endif
            #ifdef TTC_RTC7
        case 7: return TTC_RTC7;
            #endif
            #ifdef TTC_RTC8
        case 8: return TTC_RTC8;
            #endif
            #ifdef TTC_RTC9
        case 9: return TTC_RTC9;
            #endif
            #ifdef TTC_RTC10
        case 10: return TTC_RTC10;
            #endif
        // extend as required

        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // No TTC_RTCn defined! Check your makefile.100_board_* file!
    }

    return -1;
}
void              ttc_rtc_prepare() {

    A_reset( ttc_rtc_configs, TTC_RTC_MAX_AMOUNT ); // must reset data of safe array manually!
    _driver_rtc_prepare();

}
void              ttc_rtc_reset( t_u8 LogicalIndex ) {

    Assert_RTC( LogicalIndex , ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_rtc_config* Config = ttc_rtc_get_configuration( LogicalIndex );

    _driver_rtc_reset( Config );
}
void              ttc_rtc_get_time( t_u32 Format, t_ttc_rtc_config* Config ) {

    Assert_RTC( Format , ttc_assert_origin_auto); // logical index starts at 1
    Assert_RTC_Writable( Config , ttc_assert_origin_auto); //Time isn't initialized

    _driver_rtc_get_time( Format, Config );

}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_rtc(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions