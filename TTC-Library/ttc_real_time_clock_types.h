/** { ttc_real_time_clock_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for REAL_TIME_CLOCK device.
 *  
 *  Structures, Enums and Defines being required by both, high- and low-level real_time_clock.
 *  
 * Authors: Gregor Rebel
 * 
}*/

#ifndef TTC_REAL_TIME_CLOCK_TYPES_H
#define TTC_REAL_TIME_CLOCK_TYPES_H

#include "ttc_basic_types.h"
#include "real_time_clock/real_time_clock_stm32l1_types.h"

//{ Defines ***************************************************


//} Defines
//{ Static Configuration **************************************

// TTC_REAL_TIME_CLOCKn has to be defined as constant by makefile.100_board_*
#ifdef TTC_REAL_TIME_CLOCK5
  #ifndef TTC_REAL_TIME_CLOCK4
    #error TTC_REAL_TIME_CLOCK5 is defined, but not TTC_REAL_TIME_CLOCK4 - all lower TTC_REAL_TIME_CLOCKn must be defined!
  #endif
  #ifndef TTC_REAL_TIME_CLOCK3
    #error TTC_REAL_TIME_CLOCK5 is defined, but not TTC_REAL_TIME_CLOCK3 - all lower TTC_REAL_TIME_CLOCKn must be defined!
  #endif
  #ifndef TTC_REAL_TIME_CLOCK2
    #error TTC_REAL_TIME_CLOCK5 is defined, but not TTC_REAL_TIME_CLOCK2 - all lower TTC_REAL_TIME_CLOCKn must be defined!
  #endif
  #ifndef TTC_REAL_TIME_CLOCK1
    #error TTC_REAL_TIME_CLOCK5 is defined, but not TTC_REAL_TIME_CLOCK1 - all lower TTC_REAL_TIME_CLOCKn must be defined!
  #endif

  #define TTC_REAL_TIME_CLOCK_AMOUNT 5
#else
  #ifdef TTC_REAL_TIME_CLOCK4
    #define TTC_REAL_TIME_CLOCK_AMOUNT 4

    #ifndef TTC_REAL_TIME_CLOCK3
      #error TTC_REAL_TIME_CLOCK5 is defined, but not TTC_REAL_TIME_CLOCK3 - all lower TTC_REAL_TIME_CLOCKn must be defined!
    #endif
    #ifndef TTC_REAL_TIME_CLOCK2
      #error TTC_REAL_TIME_CLOCK5 is defined, but not TTC_REAL_TIME_CLOCK2 - all lower TTC_REAL_TIME_CLOCKn must be defined!
    #endif
    #ifndef TTC_REAL_TIME_CLOCK1
      #error TTC_REAL_TIME_CLOCK5 is defined, but not TTC_REAL_TIME_CLOCK1 - all lower TTC_REAL_TIME_CLOCKn must be defined!
    #endif
  #else
    #ifdef TTC_REAL_TIME_CLOCK3

      #ifndef TTC_REAL_TIME_CLOCK2
        #error TTC_REAL_TIME_CLOCK5 is defined, but not TTC_REAL_TIME_CLOCK2 - all lower TTC_REAL_TIME_CLOCKn must be defined!
      #endif
      #ifndef TTC_REAL_TIME_CLOCK1
        #error TTC_REAL_TIME_CLOCK5 is defined, but not TTC_REAL_TIME_CLOCK1 - all lower TTC_REAL_TIME_CLOCKn must be defined!
      #endif

      #define TTC_REAL_TIME_CLOCK_AMOUNT 3
    #else
      #ifdef TTC_REAL_TIME_CLOCK2

        #ifndef TTC_REAL_TIME_CLOCK1
          #error TTC_REAL_TIME_CLOCK5 is defined, but not TTC_REAL_TIME_CLOCK1 - all lower TTC_REAL_TIME_CLOCKn must be defined!
        #endif

        #define TTC_REAL_TIME_CLOCK_AMOUNT 2
      #else
        #ifdef TTC_REAL_TIME_CLOCK1
          #define TTC_REAL_TIME_CLOCK_AMOUNT 1
        #else
          #define TTC_REAL_TIME_CLOCK_AMOUNT 0
        #endif
      #endif
    #endif
  #endif
#endif

/* Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop. 
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_REAL_TIME_CLOCK 0  # disable assert handling for real_time_clock devices
 *
 */
#ifndef TTC_ASSERT_REAL_TIME_CLOCK    // any previous definition set (Makefile)?
#define TTC_ASSERT_REAL_TIME_CLOCK 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_REAL_TIME_CLOCK == 1)  // use Assert()s in REAL_TIME_CLOCK code (somewhat slower but alot easier to debug)
  #define Assert_REAL_TIME_CLOCK(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in REAL_TIME_CLOCK code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_REAL_TIME_CLOCK(Condition, ErrorCode)
#endif

//}Static Configuration
//{ Enums/ Structures *****************************************

typedef enum {    // return codes of REAL_TIME_CLOCK devices
  ec_real_time_clock_OK = 0,
  
  // other warnings go here..

  ec_real_time_clock_ERROR,           // general failure
  ec_real_time_clock_DeviceNotFound,
  ec_real_time_clock_InvalidImplementation
  
  // other failures go here..
} e_ttc_real_time_clock_errorcode;
typedef enum {    // types of architectures supported by REAL_TIME_CLOCK driver
  ta_real_time_clock_None,           // no architecture selected 
  ta_real_time_clock_stm32l1,  // first supported architecture
  //<MORE_ARCHITECTURES>
  
  ta_real_time_clock_ERROR           // architecture not supported
} e_ttc_real_time_clock_architecture;

typedef struct s_ttc_real_time_clock_time {
    t_u16 mil_sec;
    t_u8 sec;
    t_u8 dec_sec;
    t_u8 min;
    t_u8 dec_min;
    t_u8 hour;
    t_u8 dec_hour;
}ttc_real_time_t_clock_time;


typedef struct s_ttc_real_time_clock_config { // stm32l1 independent configuration data

    // Note: Write-access to this structure is only allowed before first ttc_real_time_clock_init() call!
    //       After initialization, only read accesses are allowed for high-level code.

    e_ttc_real_time_clock_architecture Architecture; // type of architecture used for current real_time_clock device
    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_REAL_TIME_CLOCK1, ...)
    t_u8  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)
 //   void* LowLevelConfig;  // low-level configuration (structure known by low-level driver only)
    t_u8 SynchPrediv;
    t_u8 AsynchPrediv;
    union  { // generic configuration bits common for all low-level Drivers
        t_u16 All;
        struct {
          unsigned Initialized        : 1;  // ==1: device has been initialized successfully
          unsigned HourFormat_12      : 1;  // ==1: device use AM/PM hour format :else device use 24 hour/day format
          unsigned Timestamp_ISR      : 1;
          unsigned Wakeup_ISR         : 1;
          unsigned Alarm_B_ISR        : 1;
          unsigned Alarm_A_ISR        : 1;
          unsigned Timestap           : 1;
          unsigned Alarm_A            : 1;
          unsigned Alarm_B            : 1;
          unsigned Reserved1          : 1;
          unsigned Reserved2          : 1;
          unsigned Reserved3          : 1;
          unsigned Reserved4          : 1;
          unsigned Reserved5          : 3;
          } Bits;
    } Flags;

    // Additional high-level attributes go here..
    
} __attribute__((__packed__)) t_ttc_real_time_clock_config;

//}Structures

#endif // TTC_REAL_TIME_CLOCK_TYPES_H
