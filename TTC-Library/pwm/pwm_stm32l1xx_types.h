#ifndef PWM_STM32L1XX_TYPES_H
#define PWM_STM32L1XX_TYPES_H

/** { pwm_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *                     
 *  Low-level driver for ttc_pwm.h providing
 *  Structures, Enums and Defines being required by ttc_pwm_types.h
 *
 *  pulse width modulation output
 *
 *  Created from template device_architecture_types.h revision 25 at 20180502 11:57:47 UTC
 *
 *  Note: See ttc_pwm.h for description of high-level pwm implementation.
 *  Note: See pwm_stm32l1xx.h for description of stm32l1xx pwm implementation.
 * 
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_PWM1   // device not defined in makefile
#  define TTC_PWM1    ta_pwm_stm32l1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_pwm_types.h *************************

typedef struct {  // stm32l1xx specific configuration data of single pwm device
  t_u8 unused;         // remove me (C structs may not be empty)!
  // add architecture specific configuration fields here..
} __attribute__((__packed__)) t_pwm_stm32l1xx_config;

// t_ttc_pwm_architecture is required by ttc_pwm_types.h
#define t_ttc_pwm_architecture t_pwm_stm32l1xx_config

//} Structures/ Enums

#endif //PWM_STM32L1XX_TYPES_H
