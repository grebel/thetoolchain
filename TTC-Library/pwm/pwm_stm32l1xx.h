#ifndef PWM_STM32L1XX_H
#define PWM_STM32L1XX_H

/** { pwm_stm32l1xx.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_pwm.h providing
 *  function prototypes being required by ttc_pwm.h
 *
 *  pulse width modulation output
 *
 *  Created from template device_architecture.h revision 30 at 20180502 11:57:47 UTC
 *
 *  Note: See ttc_pwm.h for description of stm32l1xx independent PWM implementation.
 *
 *  Authors: <AUTHOR>
 *
 *
 *  Description of pwm_stm32l1xx (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_PWM_DRIVER_AVAILABLE
#define EXTENSION_PWM_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_pwm_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "pwm_stm32l1xx.c"
//
#include "../ttc_pwm_types.h" // will include pwm_stm32l1xx_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_pwm_interface.h
//
// Some driver functions may be optional to implement.
// Check ttc_pwm_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_pwm_foo
//
#define ttc_driver_pwm_configuration_check pwm_stm32l1xx_configuration_check
#define ttc_driver_pwm_deinit pwm_stm32l1xx_deinit
#define ttc_driver_pwm_init pwm_stm32l1xx_init
#define ttc_driver_pwm_load_defaults pwm_stm32l1xx_load_defaults
#define ttc_driver_pwm_prepare pwm_stm32l1xx_prepare
#define ttc_driver_pwm_reset pwm_stm32l1xx_reset
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_pwm.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_pwm.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_pwm_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_pwm_features.
 *
 * @param Config        Configuration of pwm device
 * @return              Fields of *Config have been adjusted if necessary
 */
void pwm_stm32l1xx_configuration_check( t_ttc_pwm_config* Config );


/** shutdown single PWM unit device
 * @param Config        Configuration of pwm device
 * @return              == 0: PWM has been shutdown successfully; != 0: error-code
 */
e_ttc_pwm_errorcode pwm_stm32l1xx_deinit( t_ttc_pwm_config* Config );


/** initializes single PWM unit for operation
 * @param Config        Configuration of pwm device
 * @return              == 0: PWM has been initialized successfully; != 0: error-code
 */
e_ttc_pwm_errorcode pwm_stm32l1xx_init( t_ttc_pwm_config* Config );


/** loads configuration of indexed PWM unit with default values
 * @param Config        Configuration of pwm device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_pwm_errorcode pwm_stm32l1xx_load_defaults( t_ttc_pwm_config* Config );


/** Prepares pwm Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void pwm_stm32l1xx_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of pwm device
 */
void pwm_stm32l1xx_reset( t_ttc_pwm_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //PWM_STM32L1XX_H
