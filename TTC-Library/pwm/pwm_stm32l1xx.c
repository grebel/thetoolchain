/** pwm_stm32l1xx.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_pwm.c.
 *
 *  pulse width modulation output
 *
 *  Created from template device_architecture.c revision 36 at 20180502 11:57:47 UTC
 *
 *  Note: See ttc_pwm.h for description of high-level pwm implementation.
 *  Note: See pwm_stm32l1xx.h for description of stm32l1xx pwm implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "pwm_stm32l1xx.h".
 */

#include "pwm_stm32l1xx.h"
#include "../ttc_memory.h"            // basic memory checks
#include "../ttc_task.h"              // stack checking (provides TTC_TASK_RETURN() macro)
#include "pwm_common.h"          // generic functions shared by low-level drivers of all architectures 
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_pwm_features pwm_stm32l1xx_Features = {
    /** example features
      .MinBitRate       = 4800,
      .MaxBitRate       = 230400,
    */
    // set more feature values...

    .unused = 0
};

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

e_ttc_pwm_errorcode  pwm_stm32l1xx_load_defaults( t_ttc_pwm_config* Config ) {
    Assert_PWM_EXTRA_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = ta_pwm_stm32l1xx;         // set type of architecture
    Config->Features     = &pwm_stm32l1xx_Features;  // assign feature set

    if ( 0 ) { // enable for memory mapped devices (everything that has a base register)
        switch ( Config->PhysicalIndex ) { // load address of peripheral base register
            //case 0: Config->BaseRegister = & register_stm32l1xx_PWM1; break;
            //case 1: Config->BaseRegister = & register_stm32l1xx_PWM2; break;
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid physical index: check your makefile and high-level driver implementation!
        }
    }

    // allocate memory for stm32l1xx specific configuration
    Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_ttc_pwm_architecture ) );

    // reset all flags
    ttc_memory_set( &( Config->Flags ), 0, sizeof( Config->Flags ) );

    // set individual feature flags
    //Config->Flags.Foo = 1;
    // add more flags...


    TTC_TASK_RETURN( ec_pwm_OK ); // will perform stack overflow check + return value
}
void pwm_stm32l1xx_configuration_check( t_ttc_pwm_config* Config ) {
    Assert_PWM_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function pwm_stm32l1xx_configuration_check() is empty.


}
e_ttc_pwm_errorcode pwm_stm32l1xx_deinit( t_ttc_pwm_config* Config ) {
    Assert_PWM_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function pwm_stm32l1xx_deinit() is empty.

    return ( e_ttc_pwm_errorcode ) 0;
}
e_ttc_pwm_errorcode pwm_stm32l1xx_init( t_ttc_pwm_config* Config ) {
    Assert_PWM_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function pwm_stm32l1xx_init() is empty.

    return ( e_ttc_pwm_errorcode ) 0;
}
void pwm_stm32l1xx_prepare() {


#warning Function pwm_stm32l1xx_prepare() is empty.


}
void pwm_stm32l1xx_reset( t_ttc_pwm_config* Config ) {
    Assert_PWM_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

#warning Function pwm_stm32l1xx_reset() is empty.


}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
