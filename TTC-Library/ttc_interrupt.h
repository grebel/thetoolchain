#ifndef TTC_INTERRUPT_H
#define TTC_INTERRUPT_H
/** { { ttc_interrupt.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level interrupt configuration.
 *
 *  The basic usage scenario for interrupts:
 *  1) initialize:  ttc_interrupt_init_XXX();
 *  2) enable:      ttc_interrupt_enable_XXX();
 *
 *
 *  Structures, Enums and Defines being required by high-level interrupt and application.
 *
 *  Created from template ttc_device.h revision 25 at 20140224 21:37:20 UTC
 *
 *  Authors: Gregor Rebel, Francisco Estevez, Adrián Romero
 *
 */
/** Description of ttc_interrupt (Do not delete this line!)
 *
 * TODO: Add description
}*/

#ifndef EXTENSION_ttc_interrupt
    // #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_interrupt.sh
#endif

//{ Includes *************************************************************

#include "interfaces/ttc_interrupt_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level interrupt only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** { Function prototypes ***********************************************{
 *
 * The functions declared below provide the main interface for
 * interrupt devices on all supported architectures.
 * Check interrupt/interrupt_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
}*/

/** { initializes interrupt management for first use
 *
 * Note: This function must be called with disabled scheduler!
}*/
void ttc_interrupt_prepare();

/** { initializes interrupt for given Type to call given ISR
 * Note: This will not activate your interrupt. Call ttc_interrupt_enable_XXX() afterwards to enable/ disable this interrupt.
 *
 * @param Type        interrupt Type to use
 * @param PhysicalIndex real physical index of functional unit (e.g. 0 = USART1, 1 = USART2, ...)
 * @param ISR           function pointer of interrupt service routine to call;
 *                      ISR() gets two params: t_physical_index Index, void* Reference
 *                      Index     index functional unit in microcontroller (e.g. 0=first USART)
 *                      Reference hardware dependent data used by low level driver (e.g. register base address)
 * @param Argument      will be passed as argument to ISR()
 * @param OldISR        != NULL: previous stored pointer will be saved into given address
 * @param OldArgument   != NULL: previous stored argument will be saved into given address
 * @return              ==0: interrupt could not be initialized; >0: handle to be used when enabling/ disabling interrupt
}*/
t_ttc_interrupt_handle ttc_interrupt_init( e_ttc_interrupt_type Type, t_physical_index PhysicalIndex, void ( *ISR )( t_physical_index, void* ), void* Argument, void ( **OldISR )( t_physical_index, void* ), void** OldArgument );

/** { un-initialize given interrupt Type on given functional unit
 * @param Type     interrupt Type to use
 * @param PhysicalIndex  real physical index of functional unit (e.g. 0 = USART1, 1 = USART2, ...)
}*/
e_ttc_interrupt_errorcode ttc_interrupt_deinit( e_ttc_interrupt_type Type, t_physical_index PhysicalIndex );
#define ttc_interrupt_deinit(Type, PhysicalIndex) ttc_interrupt_init(Type, PhysicalIndex, NULL, NULL, NULL, NULL)

/** { Globally disables all interrupts
 *
 * Disabling all interrupts is very fast (one assembly instruction) and will not change configurations of individual interrupts.
 * For maximum performance, this function is typically implemented as a macro and is not available in a gdb session.
 *
}*/
void ttc_interrupt_all_disable();
#if 0 // fast: low-level driver may use a macro definition
    #define ttc_interrupt_all_disable() _driver_interrupt_all_disable()
#else // force use of a function call (slow but allows to place a breakpoint)
    #define ttc_interrupt_all_disable() ttc_interrupt_all_disable_gdb()
#endif

/** Globally disables all interrupts (implemented as function to allow usage in gdb session)
 *
 * Low-level driver shall implement this as a function to allow use inside a gdb session.
 */
void ttc_interrupt_all_disable_gdb();

/** { Globally enables all interrupts
 *
 * Enabling all interrupts is very fast (one assembly instruction) and will not change configurations of individual interrupts.
 *
}*/
void ttc_interrupt_all_enable();
#if 0 // fast: low-level driver may use a macro definition
    #define ttc_interrupt_all_enable() _driver_interrupt_all_enable()
#else // force use of a function call (slow but allows to place a breakpoint)
    #define ttc_interrupt_all_enable() ttc_interrupt_all_enable_gdb()
#endif

/** Globally disables all interrupts (implemented as function to allow usage in gdb session)
 *
 * Low-level driver shall implement this as a function to allow use inside a gdb session.
 */
void ttc_interrupt_all_enable_gdb();

/** Starts one critical section in which all interrupts are disabled.
 *
 * Note: Supports nested critical sections. Interrupts are reenabled at end of last critical section.
 */
void ttc_interrupt_critical_begin();

/** Ends one critical section.
 *
 * Note: Interrupts are reenabled when all started critical sections have been ended.
 */
void ttc_interrupt_critical_end();

/** Disables interrupts and restores a previous critical level
 *
 * Note: This function is typically only called after calling ttc_interrupt_critical_end_all()
 *
 * @param Level  >0: previous critical level as returned by ttc_interrupt_critical_end_all()
 */
void ttc_interrupt_critical_restore( t_u8 Level );

/** Ends all critical sections and re-enables interrupts
 *
 * @return  previous critical level (can be passed to ttc_interrupt_critical_restore() to restore previous level)
 */
t_u8 ttc_interrupt_critical_end_all();

/** Returns current value of critical section counter
 *
 * @return (t_u8)  ==0: interrupts are enabled: >0: interrupts are disabled
 */
t_u8 ttc_interrupt_critical_level();

/** Checks if current function is running inside an interrupt service routine.
 *
 * @return (BOOL)  ==TRUE: current function has been called from an interrupt service routine; ==FALSE: called from main() or a task.
 */
BOOL ttc_interrupt_check_inside_isr();
BOOL _driver_interrupt_check_inside_isr();
#define ttc_interrupt_check_inside_isr  _driver_interrupt_check_inside_isr  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Checks if interrupts are globally disabled.
 *
 * Most microcontrollers allow to globally disabled interrrupts.
 * Some operations may depend on interrupts being disabled or enabled.
 * This check is typically very fast.
 *
 * @return (BOOL)  == 0: interrupts not known to be disabled (does not check individual interrupts); !=0: all interrupts are disabled
 */
BOOL ttc_interrupt_check_all_disabled();
BOOL _driver_interrupt_check_all_disabled();
#define ttc_interrupt_check_all_disabled  _driver_interrupt_check_all_disabled  // enable to directly forward function call to low-level driver (remove function definition before!)

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

/** Device specific Interrupt Support
 *
 * In order to keep memory- and flash-usage as low as possible, interrupt support functions are only
 * available for activated device extensions.
 */

#ifdef EXTENSION_ttc_gpio  //{

    /** { resets interrupt settings of all GPIO units to their reset value
    *
    }*/
    void _driver_interrupt_deinit_all_gpio();

    /** { enables/ disables an already initialized interrupt
    *
    * After initialization, interrupts must be enabled to allow their trigger.
    * Enabling/ Disabling is faster than initialization.
    *
    * @param Handle  interrupt handle as returned by ttc_interrupt_init_*()
    }*/
    void ttc_interrupt_enable_gpio( t_ttc_interrupt_handle Handle );
    void ttc_interrupt_disable_gpio( t_ttc_interrupt_handle Handle );

    /** { Calculates index of ttc_interrupt_config_gpio[] corresponding for given pin
    *
    * As each entry in ttc_interrupt_config_gpio[] occupies RAM, the goal is to minimize TTC_INTERRUPT_GPIO_AMOUNT.
    * In most architectures the amount of interrupt lines is lower than the amount of GPIO pins.
    * The low-level driver has to compute this index from selected input pin.
    *
    * @param   PhysicalIndex  32-bit value representing GPIO-bank + pin
    * @return  Index of internal interrupt line serving given pin (0..TTC_INTERRUPT_GPIO_AMOUNT-1)
    }*/
    //#ifdef _driver_interrupt_gpio_calc_config_index
    //t_u8 ttc_interrupt_gpio_calc_config_index(t_base PhysicalIndex);
    ////t_u8 _driver_interrupt_gpio_calc_config_index(t_base PhysicalIndex);
    //#define ttc_interrupt_gpio_calc_config_index(PhysicalIndex) _driver_interrupt_gpio_calc_config_index(PhysicalIndex)
    //#else
    //#define _driver_interrupt_gpio_calc_config_index(PhysicalIndex) 0
    //#endif

    // Includes part 2  (ttc_basic_interface.c includes ttc_interrupt.h to use some of its functions)
    #include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
    #include "ttc_memory.h"
    #include "ttc_heap_types.h"

    // private functions below ------------------------------------------------------------

    /** { initializes interrupt for GPIO devices only
    *
    * Note: This function is called from ttc_interrupt_init(). You may call it directly to get a slightly slower runtime.
    * Note: This will not activate your interrupt. Call ttc_interrupt_enable_XXX() afterwards to enable/ disable this interrupt.
    *
    * @param Type        interrupt Type to use
    * @param PhysicalIndex real physical index of functional unit (e.g. 0 = USART1, 1 = USART2, ...)
    * @param ISR           function pointer of interrupt service routine to call;
    *                      ISR() gets two params: t_physical_index Index, void* Reference
    *                      Index     index functional unit in microcontroller (e.g. 0=first USART)
    *                      Reference hardware dependent data used by low level driver (e.g. register base address)
    * @param Argument      will be passed as argument to ISR()
    * @param OldISR        != NULL: previous stored pointer will be saved into given address
    * @param OldArgument   != NULL: previous stored argument will be saved into given address
    * @return              ==0: Interrrupt could not be initialized; !=0: Handle to be used when calling _enable()/ _disable()
    }*/
    t_ttc_interrupt_handle _ttc_interrupt_init_gpio( e_ttc_interrupt_type Type, t_physical_index PhysicalIndex, void ( *ISR )( t_physical_index, void* ), void* Argument, void ( **OldISR )( t_physical_index, void* ), void** OldArgument );

    /** { initializes single interrupt from gpio device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    e_ttc_interrupt_errorcode _driver_interrupt_init_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );

    /** { deinitializes single interrupt from gpio device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void _driver_interrupt_deinit_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );

    /** { enables/ disables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void _driver_interrupt_enable_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );
    void _driver_interrupt_disable_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );

    /** Generates a software interrupt for an external GPIO interrupt.
    *
    * @param Handle (t_ttc_interrupt_handle)
    */
    void ttc_interrupt_generate_gpio( t_ttc_interrupt_handle Handle );
    void _driver_interrupt_generate_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );


#endif //}EXTENSION_ttc_gpio
#ifdef EXTENSION_ttc_usart //{

    /** { resets interrupt settings of all USART units to their reset value
    *
    }*/
    void _driver_interrupt_deinit_all_usart();

    /** { enables/ disables an already initialized interrupt
    *
    * After initialization, interrupts must be enabled to allow their trigger.
    * Enabling/ Disabling is faster than initialization.
    *
    * @param Handle  interrupt handle as returned by ttc_interrupt_init_*()
    }*/
    void ttc_interrupt_enable_usart( t_ttc_interrupt_handle Handle );
    void ttc_interrupt_disable_usart( t_ttc_interrupt_handle Handle );

    // private functions below ------------------------------------------------------------


    /** { initializes single interrupt from usart device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @param Type           type of interrupt source to initialize (USART typically supports different interrupts)
    }*/
    e_ttc_interrupt_errorcode _driver_interrupt_init_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type );

    /** { initializes interrupt for USART devices only
    *
    * Note: This function is called from ttc_interrupt_init(). You may call it directly to get a slightly slower runtime.
    * Note: This will not activate your interrupt. Call ttc_interrupt_enable_XXX() afterwards to enable/ disable this interrupt.
    *
    * @param Type        interrupt Type to use
    * @param PhysicalIndex real physical index of functional unit (e.g. 0 = USART1, 1 = USART2, ...)
    * @param ISR           function pointer of interrupt service routine to call;
    *                      ISR() gets two params: t_physical_index Index, void* Reference
    *                      Index     index functional unit in microcontroller (e.g. 0=first USART)
    *                      Reference hardware dependent data used by low level driver (e.g. register base address)
    * @param Argument      will be passed as argument to ISR()
    * @param OldISR        != NULL: previous stored pointer will be saved into given address
    * @param OldArgument   != NULL: previous stored argument will be saved into given address
    * @return              ==0: Interrrupt could not be initialized; !=0: Handle to be used when calling _enable()/ _disable()
    }*/
    t_ttc_interrupt_handle _ttc_interrupt_init_usart( e_ttc_interrupt_type Type, t_physical_index PhysicalIndex, void ( *ISR )( t_physical_index, void* ), void* Argument, void ( **OldISR )( t_physical_index, void* ), void** OldArgument );

    /** { deinitializes all interrupts from usart device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void _driver_interrupt_deinit_usart( t_ttc_interrupt_config_usart* Config_ISRs );

    /** { enables/disables single interrupt from usart device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @param Type               interrupt source type to enable/ disable in USART device
    }*/
    void _driver_interrupt_enable_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type );
    void _driver_interrupt_disable_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type );

#endif //}EXTENSION_ttc_usart
#ifdef EXTENSION_ttc_spi   //{

    /** { resets interrupt settings of all SPI units to their reset value
    *
    }*/
    void _driver_interrupt_deinit_all_spi();

    /** { enables/ disables an already initialized interrupt
    *
    * After initialization, interrupts must be enabled to allow their trigger.
    * Enabling/ Disabling is faster than initialization.
    *
    * @param Handle  interrupt handle as returned by ttc_interrupt_init_*()
    }*/
    void ttc_interrupt_enable_spi( t_ttc_interrupt_handle Handle );
    void ttc_interrupt_disable_spi( t_ttc_interrupt_handle Handle );

    // private functions below ------------------------------------------------------------

    /** { initializes single interrupt from spi device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    e_ttc_interrupt_errorcode _driver_interrupt_init_spi( t_ttc_interrupt_config_spi* Config_ISRs );

    /** { deinitializes single interrupt from spi device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void _driver_interrupt_deinit_spi( t_ttc_interrupt_config_spi* Config_ISRs );

    /** { enables/disables single interrupt from spi device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void _driver_interrupt_enable_spi( t_ttc_interrupt_config_spi* Config_ISRs );
    void _driver_interrupt_disable_spi( t_ttc_interrupt_config_spi* Config_ISRs );

#endif //}EXTENSION_ttc_spi

// private functions below ------------------------------------------------------------

#ifdef EXTENSION_ttc_timer //{

    /** { resets interrupt settings of all TIMER units to their reset value
    *
    }*/
    void _driver_interrupt_deinit_all_timer();

    /** { enables/ disables an already initialized interrupt
    *
    * After initialization, interrupts must be enabled to allow their trigger.
    * Enabling/ Disabling is faster than initialization.
    *
    * @param Handle  interrupt handle as returned by ttc_interrupt_init_*()
    }*/
    void ttc_interrupt_enable_timer( t_ttc_interrupt_handle Handle );
    void ttc_interrupt_disable_timer( t_ttc_interrupt_handle Handle );

    // private functions below ------------------------------------------------------------

    /** { initializes interrupt for TIMER only
    *
    * Note: This function is called from ttc_interrupt_init(). You may call it directly to get a slightly slower runtime.
    * Note: This will not activate your interrupt. Call ttc_interrupt_usart_enable() afterwards to enable/ disable this interrupt.
    *
    * @param Type        interrupt Type to use
    * @param PhysicalIndex real physical index of functional unit (e.g. 0 = TIMER1, 1 = TIMER2, ...)
    * @param ISR           function pointer of interrupt service routine to call;
    *                      ISR() gets two params: t_physical_index Index, void* Reference
    *                      Index     index functional unit in microcontroller (e.g. 0=first TIMER)
    *                      Reference hardware dependent data used by low level driver (e.g. register base address)
    * @param Argument      will be passed as argument to ISR()
    * @param OldISR        != NULL: previous stored pointer will be saved into given address
    * @param OldArgument   != NULL: previous stored argument will be saved into given address
    * @return              ==0: Interrrupt could not be initialized; !=0: Handle to be used when calling _enable()/ _disable()
    }*/
    t_ttc_interrupt_handle _ttc_interrupt_init_timer( e_ttc_interrupt_type Type, t_physical_index PhysicalIndex, void ( *ISR )( t_physical_index, void* ), void* Argument, void ( **OldISR )( t_physical_index, void* ), void** OldArgument );

    /** { initializes single interrupt from timer device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    e_ttc_interrupt_errorcode _driver_interrupt_init_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

    /** { initializes single interrupt from timer device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void _driver_interrupt_deinit_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

    /** { enables/disables single interrupt from timer device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void _driver_interrupt_enable_timer( t_ttc_interrupt_configimer_t* Config_ISRs );
    void _driver_interrupt_disable_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

#endif //}EXTENSION_ttc_timer
#ifdef EXTENSION_ttc_i2c   //{

    /** { resets interrupt settings of all I2C units to their reset value
    *
    }*/
    void _driver_interrupt_deinit_all_i2c();

    /** { enables/ disables an already initialized interrupt
    *
    * After initialization, interrupts must be enabled to allow their trigger.
    * Enabling/ Disabling is faster than initialization.
    *
    * @param Handle  interrupt handle as returned by ttc_interrupt_init_*()
    }*/
    void ttc_interrupt_enable_i2c( t_ttc_interrupt_handle Handle );
    void ttc_interrupt_disable_i2c( t_ttc_interrupt_handle Handle );

    // private functions below ------------------------------------------------------------

    /** { initializes single interrupt from i2c device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    e_ttc_interrupt_errorcode _driver_interrupt_init_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

    /** { initializes single interrupt from i2c device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void _driver_interrupt_deinit_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

    /** { enables/disables single interrupt from i2c device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void _driver_interrupt_enable_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );
    void _driver_interrupt_disable_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

#endif //}EXTENSION_ttc_i2c
#ifdef EXTENSION_ttc_can   //{

    /** { resets interrupt settings of all CAN units to their reset value
    *
    }*/
    void _driver_interrupt_deinit_all_can();

    /** { enables/ disables an already initialized interrupt
    *
    * After initialization, interrupts must be enabled to allow their trigger.
    * Enabling/ Disabling is faster than initialization.
    *
    * @param Handle  interrupt handle as returned by ttc_interrupt_init_*()
    }*/
    void ttc_interrupt_enable_can( t_ttc_interrupt_handle Handle );
    void ttc_interrupt_disable_can( t_ttc_interrupt_handle Handle );

    // private functions below ------------------------------------------------------------

    /** { initializes interrupt for CAN only
    *
    * Note: This function is called from ttc_interrupt_init(). You may call it directly to get a slightly slower runtime.
    * Note: This will not activate your interrupt. Call ttc_interrupt_usart_enable() afterwards to enable/ disable this interrupt.
    *
    * @param Type          interrupt Type to use
    * @param PhysicalIndex real physical index of functional unit (e.g. 0 = CAN1, 1 = CAN2)
    * @param ISR           function pointer of interrupt service routine to call;
    *                      ISR() gets two params: t_physical_index Index, void* Reference
    *                      Index     index functional unit in microcontroller (e.g. 0=first CAN)
    *                      Reference hardware dependent data used by low level driver (e.g. register base address)
    * @param Argument      will be passed as argument to ISR()
    * @param OldISR        != NULL: previous stored pointer will be saved into given address
    * @param OldArgument   != NULL: previous stored argument will be saved into given address
    * @return              ==0: Interrrupt could not be initialized; !=0: Handle to be used when calling _enable()/ _disable()
    }*/
    t_ttc_interrupt_handle _ttc_interrupt_init_can( e_ttc_interrupt_type Type, t_physical_index PhysicalIndex, void ( *ISR )( t_physical_index, void* ), void* Argument, void ( **OldISR )( t_physical_index, void* ), void** OldArgument );

    /** { enables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void _driver_interrupt_enable_can( t_ttc_interrupt_config_can* Config_ISRs );
    void _driver_interrupt_disable_can( t_ttc_interrupt_config_can* Config_ISRs );

    /** { initializes single interrupt from can device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    e_ttc_interrupt_errorcode _driver_interrupt_init_can( t_ttc_interrupt_config_can* Config_ISRs );

    /** { deinitializes single interrupt from can device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void _driver_interrupt_deinit_can( t_ttc_interrupt_config_can* Config_ISRs );

#endif //}EXTENSION_ttc_can

#ifdef EXTENSION_ttc_rtc   //{

    /** { resets interrupt settings of all CAN units to their reset value
    *
    }*/
    void _driver_interrupt_deinit_all_rtc();

    /** { enables/ disables an already initialized interrupt
    *
    * After initialization, interrupts must be enabled to allow their trigger.
    * Enabling/ Disabling is faster than initialization.
    *
    * @param Handle  interrupt handle as returned by ttc_interrupt_init_*()
    }*/
    void ttc_interrupt_enable_rtc( t_ttc_interrupt_handle Handle );
    void ttc_interrupt_disable_rtc( t_ttc_interrupt_handle Handle );

    // private functions below ------------------------------------------------------------

    /** { initializes interrupt for rtc only
    *
    * Note: This function is called from ttc_interrupt_init(). You may call it directly to get a slightly slower runtime.
    * Note: This will not activate your interrupt. Call ttc_interrupt_usart_enable() afterwards to enable/ disable this interrupt.
    *
    * @param Type          interrupt Type to use
    * @param PhysicalIndex real physical index of functional unit (e.g. 0 = rtc1, 1 = rtc2)
    * @param ISR           function pointer of interrupt service routine to call;
    *                      ISR() gets two params: t_physical_index Index, void* Reference
    *                      Index     index functional unit in microcontroller (e.g. 0=first rtc)
    *                      Reference hardware dependent data used by low level driver (e.g. register base address)
    * @param Argument      will be passed as argument to ISR()
    * @param OldISR        != NULL: previous stored pointer will be saved into given address
    * @param OldArgument   != NULL: previous stored argument will be saved into given address
    * @return              ==0: Interrrupt could not be initialized; !=0: Handle to be used when calling _enable()/ _disable()
    }*/
    t_ttc_interrupt_handle _ttc_interrupt_init_rtc( e_ttc_interrupt_type Type, t_physical_index PhysicalIndex, void ( *ISR )( t_physical_index, void* ), void* Argument, void ( **OldISR )( t_physical_index, void* ), void** OldArgument );

    /** { enables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void _driver_interrupt_enable_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );
    void _driver_interrupt_disable_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );

    /** { initializes single interrupt from rtc device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    e_ttc_interrupt_errorcode _driver_interrupt_init_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );

    /** { deinitializes single interrupt from rtc device on current architecture
    *
    * Note: After deinitialization, the interrupt rtcnot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void _driver_interrupt_deinit_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );

#endif //}EXTENSION_ttc_rtc

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_interrupt(t_u8 LogicalIndex)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

/** { Prepares interrupt driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
}*/
void _driver_interrupt_prepare();


//}Interrupt-Initializers

//}

#endif //TTC_INTERRUPT_H
