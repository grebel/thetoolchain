/** { ttc_input_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for INPUT device.
 *  Structures, Enums and Defines being required by both, high- and low-level input.
 *  This file does not provide function declarations!
 *
 *  Created from template ttc_device_types.h revision 28 at 20150317 17:32:16 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_INPUT_TYPES_H
#define TTC_INPUT_TYPES_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_input.h" or "ttc_input.c"
//
#include "ttc_basic_types.h"
#include "ttc_heap_types.h"
#include "compile_options.h" // only required to help IDE to know constant definitions
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
#ifdef EXTENSION_input_touchpad
#  include "input/input_touchpad_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

//{  TTC_INPUTn has to be defined as constant by makefile.100_board_*
#ifdef TTC_INPUT5
#ifndef TTC_INPUT4
#error TTC_INPUT5 is defined, but not TTC_INPUT4 - all lower TTC_INPUTn must be defined!
#endif
#ifndef TTC_INPUT3
#error TTC_INPUT5 is defined, but not TTC_INPUT3 - all lower TTC_INPUTn must be defined!
#endif
#ifndef TTC_INPUT2
#error TTC_INPUT5 is defined, but not TTC_INPUT2 - all lower TTC_INPUTn must be defined!
#endif
#ifndef TTC_INPUT1
#error TTC_INPUT5 is defined, but not TTC_INPUT1 - all lower TTC_INPUTn must be defined!
#endif

#define TTC_INPUT_AMOUNT 5
#else
#ifdef TTC_INPUT4
#define TTC_INPUT_AMOUNT 4

#ifndef TTC_INPUT3
#error TTC_INPUT5 is defined, but not TTC_INPUT3 - all lower TTC_INPUTn must be defined!
#endif
#ifndef TTC_INPUT2
#error TTC_INPUT5 is defined, but not TTC_INPUT2 - all lower TTC_INPUTn must be defined!
#endif
#ifndef TTC_INPUT1
#error TTC_INPUT5 is defined, but not TTC_INPUT1 - all lower TTC_INPUTn must be defined!
#endif
#else
#ifdef TTC_INPUT3

#ifndef TTC_INPUT2
#error TTC_INPUT5 is defined, but not TTC_INPUT2 - all lower TTC_INPUTn must be defined!
#endif
#ifndef TTC_INPUT1
#error TTC_INPUT5 is defined, but not TTC_INPUT1 - all lower TTC_INPUTn must be defined!
#endif

#define TTC_INPUT_AMOUNT 3
#else
#ifdef TTC_INPUT2

#ifndef TTC_INPUT1
#error TTC_INPUT5 is defined, but not TTC_INPUT1 - all lower TTC_INPUTn must be defined!
#endif

#define TTC_INPUT_AMOUNT 2
#else
#ifdef TTC_INPUT1
#define TTC_INPUT_AMOUNT 1
#else
#define TTC_INPUT_AMOUNT 0
#endif
#endif
#endif
#endif
#endif
//}
/**{ Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_INPUT 0  # disable assert handling for input devices
 *
 */
#ifndef TTC_ASSERT_INPUT    // any previous definition set (Makefile)?
#define TTC_ASSERT_INPUT 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_INPUT == 1)  // use Assert()s in INPUT code (somewhat slower but alot easier to debug)
#define Assert_INPUT(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in INPUT code (somewhat smaller + faster, but crashes are hard to debug)
#define Assert_INPUT(Condition, ErrorCode)
#endif

#define TTC_ASSERT_INPUT_EXTRA 1  // <-- remove this line!
#ifndef TTC_ASSERT_INPUT_EXTRA    // any previous definition set (Makefile)?
#define TTC_ASSERT_INPUT_EXTRA 0  // extra asserts are disabled by default
#endif
#if (TTC_ASSERT_INPUT_EXTRA == 1)  // use Assert()s in INPUT code (somewhat slower but alot easier to debug)
#define Assert_INPUT_EXTRA(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in INPUT code (somewhat smaller + faster, but crashes are hard to debug)
#define Assert_INPUT_EXTRA(Condition, ErrorCode)
#endif
//}
/**{ Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_input_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_input_architecture
#  warning Missing low-level definition for t_ttc_input_architecture (using default)
#  define t_ttc_input_architecture void
#endif
//}
/**{ Check constant configuration of all touchpad devices
 *
 * The constants being checked here must be defined in your makefile.
 * Most constants are defined by EXTENSION_100_board_* or EXTENSION_150_board_extension_* files.
 */

#ifdef TTC_INPUT1

#  ifndef TTC_INPUT1_MAX_AREAS // maximum allowed amount of input areas for this input device
#    define TTC_INPUT1_MAX_AREAS 3
#  endif

#else
#  define TTC_INPUT1_MAX_AREAS 0
#endif
#ifdef TTC_INPUT2

#  ifndef TTC_INPUT2_MAX_AREAS // maximum allowed amount of input areas for this input device
#    define TTC_INPUT2_MAX_AREAS 5
#  endif

#else
#  define TTC_INPUT2_MAX_AREAS 0
#endif
#ifdef TTC_INPUT3

#  ifndef TTC_INPUT3_MAX_AREAS // maximum allowed amount of input areas for this input device
#    define TTC_INPUT3_MAX_AREAS 5
#  endif

#else
#  define TTC_INPUT3_MAX_AREAS 0
#endif
#ifdef TTC_INPUT4

#  ifndef TTC_INPUT4_MAX_AREAS // maximum allowed amount of input areas for this input device
#    define TTC_INPUT4_MAX_AREAS 5
#  endif

#else
#  define TTC_INPUT4_MAX_AREAS 0
#endif
#ifdef TTC_INPUT5

#  ifndef TTC_INPUT5_MAX_AREAS // maximum allowed amount of input areas for this input device
#    define TTC_INPUT5_MAX_AREAS 5
#  endif

#else
#  define TTC_INPUT5_MAX_AREAS 0
#endif
#ifdef TTC_INPUT6

#  ifndef TTC_INPUT6_MAX_AREAS // maximum allowed amount of input areas for this input device
#    define TTC_INPUT6_MAX_AREAS 5
#  endif

#else
#  define TTC_INPUT6_MAX_AREAS 0
#endif
#ifdef TTC_INPUT7

#  ifndef TTC_INPUT7_MAX_AREAS // maximum allowed amount of input areas for this input device
#    define TTC_INPUT7_MAX_AREAS 5
#  endif

#else
#  define TTC_INPUT7_MAX_AREAS 0
#endif
#ifdef TTC_INPUT8

#  ifndef TTC_INPUT8_MAX_AREAS // maximum allowed amount of input areas for this input device
#    define TTC_INPUT8_MAX_AREAS 5
#  endif

#else
#  define TTC_INPUT8_MAX_AREAS 0
#endif
#ifdef TTC_INPUT9

#  ifndef TTC_INPUT9_MAX_AREAS // maximum allowed amount of input areas for this input device
#    define TTC_INPUT9_MAX_AREAS 5
#  endif

#else
#  define TTC_INPUT9_MAX_AREAS 0
#endif
#ifdef TTC_INPUT10

#  ifndef TTC_INPUT10_MAX_AREAS // maximum allowed amount of input areas for this input device
#    define TTC_INPUT10_MAX_AREAS 5
#  endif

#else
#  define TTC_INPUT10_MAX_AREAS 0
#endif

#define TTC_INPUT_MAX_AREAS (TTC_INPUT1_MAX_AREAS + TTC_INPUT2_MAX_AREAS + TTC_INPUT3_MAX_AREAS + TTC_INPUT4_MAX_AREAS + TTC_INPUT5_MAX_AREAS + TTC_INPUT6_MAX_AREAS + TTC_INPUT7_MAX_AREAS + TTC_INPUT8_MAX_AREAS + TTC_INPUT9_MAX_AREAS + TTC_INPUT10_MAX_AREAS)
//}

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_input_errorcode      return codes of INPUT devices
    ec_input_OK = 0,

    // other warnings go here..

    ec_input_ERROR,                    // general failure
    ec_input_NULL,                     // NULL pointer not accepted
    ec_input_DeviceNotFound,           // corresponding device could not be found
    ec_input_InvalidConfiguration,     // sanity check of device configuration failed
    ec_input_InvalidImplementation,    // your code does not behave as expected
    ec_input_CannotInitializeTouchPad, // error during initialization of touchpad device

    // other failures go here..

    ec_input_unknown                // no valid errorcodes past this entry
} e_ttc_input_errorcode;
typedef enum {   // e_ttc_input_architecture  types of architectures supported by INPUT driver
    ta_input_None,           // no architecture selected

    ta_input_touchpad, // automatically added by ./create_DeviceDriver.pl
//InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_input_unknown        // architecture not supported
} e_ttc_input_architecture;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef enum {   // type of touchpad update
    tie_None = 0,

    tie_MouseDown, // pointer button pressed within input area
    tie_MouseUp,   // pointer button released within input area
    tie_Drag,      // pointer moved within input area while button is pressed
    tie_Move,      // pointer moved within input area while button is released
    tie_Leave,     // pointer left input area
    tie_Enter,     // pointer entered input area

    tie_Invalid
} e_ttc_input_event;
typedef struct s_ttc_input_area { //         configuration of single input area
    // implicit Next pointer provided by surrounding t_ttc_heap_block_from_pool

    t_u16 Left;           // left end of the botton
    t_u16 Top;            // top of the botton
    t_u16 Right;          // right end of the botton
    t_u16 Bottom;         // buttom of the button

    /** function handling input events
     *
     * @param ConfigInput   configuration data providing additional information about current event
     * @param AffectedArea  input area data in which input event has occured
     * @param RelativeX     x-coordinate of event relative to (Top,Left) of input area
     * @param RelativeY     y-coordinate of event relative to (Top,Left) of input area
     */
    // void (*Handler)(struct s_ttc_input_config* ConfigInput, struct s_ttc_input_area* AffectedArea, t_u16 RelativeX, t_u16 RelativeY);
    void ( *Handler )( void* ConfigInput, struct s_ttc_input_area* AffectedArea, t_u16 RelativeX, t_u16 RelativeY );
    void* Argument;  // argument given at creation of input area
} __attribute__( ( __packed__ ) ) t_ttc_input_area;
typedef struct s_ttc_input_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_input_init()
    //       and after ttc_input_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // low-level configuration (structure defined by low-level driver)
    t_ttc_input_architecture* LowLevelConfig;

    // start of single linked list of active input areas
    t_ttc_list_item* InputAreas_Active;

    // start of single linked list of inactive input areas
    t_ttc_list_item* InputAreas_InActive;

    t_u16 EventX;                    // X-coordinate of pointer event
    t_u16 EventY;                    // Y-coordinate of pointer event
    t_u16 EventPreviousX;            // previous X-coordinate of pointer event
    t_u16 EventPreviousY;            // previous Y-coordinate of pointer event
    e_ttc_input_event Event;         // current input event
    e_ttc_input_event EventPrevious; // previous input event

    e_ttc_input_architecture Architecture; // type of architecture used for current input device


    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_INPUT1, ...)
    union  { // generic configuration bits common for all low-level Drivers
        t_u8 All;
        struct {
            unsigned Initialized        : 1;  // ==1: device has been initialized successfully
            bool FlagBack                : 1;
            // ToDo: additional high-level flags go here..

            unsigned Reserved1        : 6; // pad to 8 bits
        } Bits;
    } Flags;

} __attribute__( ( __packed__ ) ) t_ttc_input_config;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_INPUT_TYPES_H
