/** { ttc_rtls.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for real time location service.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Most functions of this driver are defined as macros and being implemented
 *  in interfaces/ttc_rtls_interface.c or in low-level drivers rtls/rtls_*.c.
 *
 *  See corresponding ttc_rtls.h for documentation of all high-level functions.
 *
 *  Created from template ttc_device.c revision 41 at 20161208 10:11:47 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_rtls.h".
//
#include "ttc_rtls.h"
#include "ttc_radio.h"
#include "ttc_slam.h"
#include "ttc_math.h"
#include "ttc_heap.h"
#ifdef EXTENSION_ttc_interrupt
    #include "ttc_interrupt.h"
#endif
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_RTLS_AMOUNT == 0
    #warning No RTLS devices defined, cdid you forget to activate something? - Define at least TTC_RTLS1 as one from e_ttc_rtls_architecture in your makefile!
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of rtls devices.
 *
 */


// for each initialized device, a pointer to its generic and architecture specific definitions is stored
A_define( t_ttc_rtls_config*, ttc_rtls_configs, TTC_RTLS_AMOUNT );

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ********************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_rtls(t_ttc_rtls_config* Config)

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * @param LogicalIndex    1.. logical index of RTLS device. Each logical device <n> is defined via TTC_RTLS<n>* constants in compile_options.h and extensions.active/makefile
 */
void _ttc_rtls_configuration_check( t_u8 LogicalIndex );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

t_ttc_rtls_config*       ttc_rtls_create() {

    t_u8 LogicalIndex = ttc_rtls_get_max_index();
    t_ttc_rtls_config* Config = ttc_rtls_get_configuration( LogicalIndex );

    return Config;
}
void                     ttc_rtls_deinit( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration

    t_ttc_rtls_config* Config = ttc_rtls_get_configuration( LogicalIndex );

    if ( Config->Flags.Initialized ) {
        e_ttc_rtls_errorcode Result = _driver_rtls_deinit( Config );
        if ( Result == ec_rtls_OK )
        { Config->Flags.Initialized = 0; }
    }
    ttc_task_critical_end(); // other tasks now may access this driver
}
t_u8                     ttc_rtls_get_max_index() {
    return TTC_RTLS_AMOUNT;
}
t_ttc_rtls_config*       ttc_rtls_get_configuration( t_u8 LogicalIndex ) {
    Assert_RTLS( LogicalIndex > 0, ttc_assert_origin_auto ); // logical index starts at 1
    Assert_RTLS( LogicalIndex <= TTC_RTLS_AMOUNT, ttc_assert_origin_auto ); // invalid index given (did you configure engnough ttc_rtls devices in your makefile?)
    t_ttc_rtls_config* Config = A( ttc_rtls_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_disable();                      // ensure that we are not interrupted while creating configuration
#endif

        Config = A( ttc_rtls_configs, LogicalIndex - 1 ); // have to reload with disabled interrupts to avoid racing conditions
        if ( !Config ) {
            Config = A( ttc_rtls_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_rtls_config ) );
            Config->Flags.Initialized = 0;             // make sure that deinit() is not called during load_defaults()
            ttc_rtls_load_defaults( LogicalIndex );
        }

#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_enable();
#endif
    }

    Assert_RTLS_EXTRA_Writable( Config, ttc_assert_origin_auto ); // memory corrupted?
    return Config;
}
e_ttc_rtls_errorcode     ttc_rtls_init( t_u8 LogicalIndex ) {
#if (TTC_ASSERT_RTLS_EXTRA == 1)
    t_u8 InitialCriticalLevel = ttc_interrupt_critical_level();
#endif
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration
    t_ttc_rtls_config* Config = ttc_rtls_get_configuration( LogicalIndex );

    // load configuration pointers of radio and slam instances to use for faster access
    Config->ConfigRadio = ttc_radio_get_configuration( Config->Init.Index_Radio );
    Config->ConfigSLAM  = ttc_slam_get_configuration( Config->Init.Index_SLAM );

    // prepare local identifier
    ttc_rtls_set_local_id( LogicalIndex, Config->Init.LocalID );

#if TTC_RTLS_IMPLEMENT_ANCHOR == 1
    if ( Config->Init.Flags.IsAnchorNode ) { // prepare anchor node fields

        // calculate Config->Anchor.Index_AnchorID
        if ( ( Config->Init.LocalID >= &( Config->Init.AnchorIDs[0] ) ) &&
                ( Config->Init.LocalID <  & ( Config->Init.AnchorIDs[Config->ConfigSLAM->Init.Amount_Nodes] ) )
           )
        { Config->Anchor.Index_AnchorID = Config->Init.LocalID - &( Config->Init.AnchorIDs[0] ); } // LocalID points into AnchorIDs[]
        else
        { Config->Anchor.Index_AnchorID = -1; }                                         // LocalID is stored outside of AnchorIDs[]
    }
#else
    Assert_RTLS( Config->Init.Flags.IsAnchorNode == 0, ttc_assert_origin_auto ); // May not enable anchor node implementation if source code is disabled. Define TTC_RTLS<n>_IMPLEMENT_ANCHOR as checked in ttc_rtls_types.h!
#endif

    // check configuration to meet all limits of current architecture
    _ttc_rtls_configuration_check( LogicalIndex );

    ttc_rtls_reset( LogicalIndex );

    // low-level driver might want to initialize too
    Config->LastError = _driver_rtls_init( Config );
    if ( ! Config->LastError ) // == 0
    { Config->Flags.Initialized = 1; }

    Assert_RTLS( Config->Architecture != 0, ttc_assert_origin_auto ); // Low-Level driver must set this value!

#if TTC_RTLS_IMPLEMENT_ANCHOR == 1
    if ( Config->Init.Flags.IsAnchorNode ) {
        if ( !Config->Anchor.RadioUserID ) // get us a new radio user id + reserve some extra packet buffers for us
        { Config->Anchor.RadioUserID = ttc_radio_user_new( Config->Init.Index_Radio, 2 ); }

        Config->Anchor.State = tss_Init;
    }
    else
    { Config->Anchor.State = tss_None; }

#endif
#if TTC_RTLS_IMPLEMENT_MOBILE == 1
    if ( Config->Init.Flags.IsMobileNode ) { // init mobile node statemachine
        if ( !Config->Mobile.RadioUserID ) // get us a new radio user id + reserve some extra packet buffers for us
        { Config->Mobile.RadioUserID = ttc_radio_user_new( Config->Init.Index_Radio, 2 ); }

        Config->Mobile.State = tss_Init;
    }
    else
    { Config->Mobile.State = tss_None; }

#endif

    ttc_task_critical_end(); // other tasks now may access this driver
    Assert_RTLS_EXTRA( InitialCriticalLevel == ttc_interrupt_critical_level(), ttc_assert_origin_auto ); // amount of  ttc_task_critical_end() calls does not match. Check driver implementation!

    return Config->LastError;
}
e_ttc_rtls_errorcode     ttc_rtls_load_defaults( t_u8 LogicalIndex ) {
    t_ttc_rtls_config* Config = ttc_rtls_get_configuration( LogicalIndex );

    u_ttc_rtls_architecture* LowLevelConfig = NULL;
    if ( Config->Flags.Initialized ) {
        ttc_rtls_deinit( LogicalIndex );
        LowLevelConfig = Config->LowLevelConfig; // don't want to delete pointer to dynamic memory chunk
    }

    // clear high-level configuration
    ttc_memory_set( Config, 0, sizeof( t_ttc_rtls_config ) );

    // restore pointer to low-level config (if been allocated before)
    Config->LowLevelConfig = LowLevelConfig;

    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    Config->Init.Amount_RawMeasures     = 1;
    Config->Init.Amount_TxRetries       = 4;

    // timeouts
    Config->Init.TimeOut_RangeMeasureMS = 500;

    //Insert additional generic default values here ...

    // Let low-level driver initialize remaining fields
    Config->LastError = _driver_rtls_load_defaults( Config );

    // Check mandatory configuration items
    Assert_RTLS_EXTRA( ( LowLevelConfig == NULL ) || ( LowLevelConfig == Config->LowLevelConfig ), ttc_assert_origin_auto ); // LowLevelConfig already allocated, low-level driver must not reallocate it!
    Assert_RTLS_EXTRA( ( Config->Architecture > ta_rtls_None ) && ( Config->Architecture < ta_rtls_unknown ), ttc_assert_origin_auto ); // architecture not set properly! Low-Level driver must set this field!

    return Config->LastError;
}
void                     ttc_rtls_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)

    A_reset( ttc_rtls_configs, TTC_RTLS_AMOUNT ); // safe arrays are placed in data section and must be initialized manually

    if ( 0 ) // optional: register this device for a sysclock change update
    { ttc_sysclock_register_for_update( ttc_rtls_sysclock_changed ); }

    _driver_rtls_prepare();
}
void                     ttc_rtls_reset( t_u8 LogicalIndex ) {
    t_ttc_rtls_config* Config = ttc_rtls_get_configuration( LogicalIndex );

    _driver_rtls_reset( Config );
    ttc_slam_reset( Config->Init.Index_SLAM );

    // disable statemachines
    Config->Anchor.State = tss_None;
    Config->Mobile.State = tss_None;

    // start value for sequence number
    Config->SequenceNumber = 1;
}
void                     ttc_rtls_set_local_id( t_u8 LogicalIndex, const t_ttc_packet_address* LocalID ) {
    t_ttc_rtls_config* Config = ttc_rtls_get_configuration( LogicalIndex );

    Assert_RTLS_Readable( LocalID, ttc_assert_origin_auto ); // LocalID must point to readable memory. Set it to initialized identifier of local node!
    Config->Init.LocalID = LocalID;
    Config->LocalID16    = Config->Init.LocalID->Address16;
}
void                     ttc_rtls_statemachine( t_ttc_rtls_config* Config ) {
    Assert_RTLS_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_RTLS( Config->Anchor.State >= tss_Init, ttc_assert_origin_auto ); // requires ttc_rtls to be initialized before!

    // run all activated statemachines ( enable via Config->Init.Flags befor calling ttc_rtls_init() )
#if TTC_RTLS_IMPLEMENT_ANCHOR == 1
    if ( Config->Anchor.State ) {
        _driver_rtls_anchor_statemachine( Config );
        Assert_RTLS( ( Config->Anchor.State > tss_Anchor_First ) && ( Config->Anchor.State < tss_Anchor_Last ), ttc_assert_origin_auto ); // Low-level driver returned invalid statemachine state. Valid state numbers for anchor nodes are tss_Anchor_First..tss_Anchor_Last. Check implementation of low-level driver!
    }
#endif

#if TTC_RTLS_IMPLEMENT_MOBILE == 1
    if ( Config->Mobile.State ) {
        _driver_rtls_mobile_statemachine( Config );
        Assert_RTLS( ( Config->Mobile.State > tss_Mobile_First ) && ( Config->Mobile.State < tss_Anchor_Last ), ttc_assert_origin_auto ); // Low-level driver returned invalid statemachine state. Valid state numbers for anchor nodes are tss_Anchor_First..tss_Anchor_Last. Check implementation of low-level driver!
    }
#endif
}
e_ttc_rtls_errorcode     ttc_rtls_anchor_buildup_start( t_u8 LogicalIndex ) {
    t_ttc_rtls_config* Config = ttc_rtls_get_configuration( LogicalIndex );

#if TTC_RTLS_IMPLEMENT_ANCHOR == 1
    if ( Config->Anchor.State ) {
        Assert_RTLS( ( Config->Anchor.State > tss_Anchor_First ) && ( Config->Anchor.State < tss_Anchor_Last ), ttc_assert_origin_auto ); // Invalid anchor node state. Valid state numbers for anchor nodes are tss_Anchor_First..tss_Anchor_Last. Check implementation of low-level driver!
        if ( ! ttc_radio_user_locked( Config->Init.Index_Radio ) ) {
            _driver_rtls_anchor_buildup_start( Config );
            return ec_rtls_OK;
        }
        else
        { return ec_rtls_RadioLocked; }
    }
#endif
    return ec_rtls_NotAvailable;
}
t_ttc_rtls_localization* ttc_rtls_mobile_localize( t_u8 LogicalIndex, e_radio_common_ranging_type Type, t_ttc_math_vector3d_xyz* Location1, t_ttc_math_vector3d_xyz* Location2 ) {
    Assert_RTLS_Writable( Location1, ttc_assert_origin_auto ); // must point to RAM. Check implementation of caller!
    Assert_RTLS_Writable( Location2, ttc_assert_origin_auto ); // must point to RAM. Check implementation of caller!
    t_ttc_rtls_config* Config = ttc_rtls_get_configuration( LogicalIndex );
    t_ttc_rtls_localization* Result = NULL;

#if TTC_RTLS_IMPLEMENT_MOBILE == 1

    if ( ! ttc_radio_user_lock( Config->Init.Index_Radio, Config->Mobile.RadioUserID ) )
    { return NULL; }

    t_u8 volatile AmountReferences = 0; // amount of valid distances to reference nodes in Config->Mobile.Localization.Distances.SLAM[]

    // create broadcast address of current network from radio configuration
    t_ttc_packet_address BroadcastAddress;
    BroadcastAddress.PanID     = Config->ConfigRadio->LocalID.PanID;
    BroadcastAddress.Address16 = 0xffff;

    if ( Type == rcrt_Request_Localization_SSTOFCR ) {
        if ( !Config->Mobile.Localization.ArraySizes ) { // allocating memory for localization data
            t_u8 Amount_AnchorIDs = Config->Mobile.Localization.ArraySizes = Config->Init.Amount_AnchorIDs;

            Config->Mobile.Localization.AnchorLocations = ttc_heap_alloc( sizeof( *( Config->Mobile.Localization.AnchorLocations ) ) * Amount_AnchorIDs );
            Config->Mobile.Localization.Rangings        = ttc_heap_alloc( sizeof( *( Config->Mobile.Localization.Rangings ) ) * Amount_AnchorIDs );

            if ( 1 ) { // create Distances.Radio and Distances.SLAM in same memory chunk to reduce memory usage
                t_base SizeOfDistance = sizeof( *( Config->Mobile.Localization.Distances.Radio ) );
                if ( SizeOfDistance < sizeof( *( Config->Mobile.Localization.Distances.SLAM ) ) )
                { SizeOfDistance = sizeof( *( Config->Mobile.Localization.Distances.SLAM ) ); }

                void* MemoryChunk = ttc_heap_alloc( SizeOfDistance * Amount_AnchorIDs );
                Config->Mobile.Localization.Distances.Radio = MemoryChunk;
                Assert_RTLS( ( void* ) Config->Mobile.Localization.Distances.SLAM == MemoryChunk, ttc_assert_origin_auto ); // these should be placed in a union. Was t_ttc_rtls_config_mobile altered?
            }
        }
        VOLATILE_RADIO t_u8 AmountRepliesRequired = Config->Init.Amount_AnchorIDs;
        if ( AmountRepliesRequired > Config->Mobile.Localization.ArraySizes ) { // cannot increase buffers: reduce amount of required replies
            AmountRepliesRequired = Config->Mobile.Localization.ArraySizes;
        }

        if ( 1 ) { // reset statistics
            ttc_memory_set( Config->Mobile.Localization.Rangings, 0, sizeof( *Config->Mobile.Localization.Rangings ) * AmountRepliesRequired );
            for ( t_u8 AnchorIndex = 0; AnchorIndex < AmountRepliesRequired; AnchorIndex++ ) { // reset statistics
                t_ttc_rtls_statistics* Statistic = &( Config->Mobile.Localization.Rangings[AnchorIndex] );
                Statistic->Lower = -1;
            }
        }

        for ( t_u16 Retries = Config->Init.Amount_RawMeasures; Retries > 0; Retries-- ) {

            // Single Sided Time Of Flight (SSTOF) is the most simple ranging method
            // We send a broadcast ranging request and expect every anchor to answer it
            t_u8 VOLATILE_RADIO AmountReplies = ttc_radio_ranging_request( 1,
                                                                           Config->Mobile.RadioUserID,
                                                                           Type,
                                                                           AmountRepliesRequired,
                                                                           &BroadcastAddress,
                                                                           1000,
                                                                           0,
                                                                           Config->Mobile.Localization.Distances.Radio,
                                                                           Config->Mobile.Localization.AnchorLocations,  // required for Type==rcrt_Request_Localization_SSTOFCR
                                                                           FALSE
                                                                         );

            // obtain pointers to first entry in each array
            t_ttc_rtls_statistics*    VOLATILE_RADIO Statistic      = Config->Mobile.Localization.Rangings;
            t_ttc_radio_distance*     VOLATILE_RADIO DistanceRadio  = Config->Mobile.Localization.Distances.Radio;
            t_ttc_math_vector3d_xyz*  VOLATILE_RADIO AnchorLocation = Config->Mobile.Localization.AnchorLocations;

            for ( t_u8 Index = 0; Index < AmountReplies; Index++ ) { // update statistics for latest distance measures
                t_u16 NodeIndex = DistanceRadio->RemoteID.Address16;

                if ( ( NodeIndex > 0 ) && ( NodeIndex <= Config->Init.Amount_AnchorIDs ) ) { // remote id valid: update Config->Mobile.Localization.Rangings[]
                    Statistic = &( Config->Mobile.Localization.Rangings[NodeIndex - 1] );

                    if ( !Statistic->Average ) { // first measure: just store value
                        Statistic->Average = DistanceRadio->Distance_cm;
                        Statistic->Lower   = DistanceRadio->Distance_cm;
                        Statistic->Upper   = DistanceRadio->Distance_cm;
                    }
                    else {                       // >1 measures: calculate average
                        if ( Statistic->Lower > DistanceRadio->Distance_cm )
                        { Statistic->Lower = DistanceRadio->Distance_cm; }
                        if ( Statistic->Upper < DistanceRadio->Distance_cm )
                        { Statistic->Upper = DistanceRadio->Distance_cm; }

                        Statistic->Median  = Statistic->Lower   / 2 + Statistic->Upper           / 2;
                        Statistic->Average = Statistic->Average / 2 + DistanceRadio->Distance_cm / 2;
                    }

                    if ( ttc_math_vector3d_valid( AnchorLocation ) ) { // all coordinates valid: store location of anchor node in slam model (required for localization)
                        ttc_slam_update_coordinates( Config->Init.Index_SLAM,
                                                     NodeIndex,
                                                     AnchorLocation->X,
                                                     AnchorLocation->Y,
                                                     AnchorLocation->Z
                                                   );
                    }
                }
                DistanceRadio++;
                AnchorLocation++;
            }

            t_ttc_slam_distance* VOLATILE_RADIO DistanceSLAM = Config->Mobile.Localization.Distances.SLAM;
            DistanceRadio  = Config->Mobile.Localization.Distances.Radio;
            AnchorLocation = Config->Mobile.Localization.AnchorLocations;

            Assert_SLAM( sizeof( DistanceRadio ) >= sizeof( DistanceSLAM ), ttc_assert_origin_auto ); // the following loop is translating DistanceRadio[] to DistanceSLAM[] in same memory block. This requires DistanceRadio[] entries not to be smaller than DistanceSLAM[] entries. adjust structures and datatypes!
            AmountReferences = 0;
            for ( t_u8 Index = 0; Index < AmountReplies; Index++ ) { // build up DistanceSLAM[] as valid distances to reference nodes as required by ttc_slam_localize_foreigner()
                t_u16 NodeIndex = DistanceRadio->RemoteID.Address16;

                if ( ttc_slam_get_node( Config->Init.Index_SLAM, NodeIndex )->Flags.PositionValid ) { // copy data into References[] for ttc_slam_localize_foreigner() call
                    DistanceSLAM->NodeIndex = NodeIndex;
                    DistanceSLAM->Distance  = Statistic->Average;
                    DistanceSLAM++;
                    AmountReferences++;
                }
                DistanceRadio++;
                AnchorLocation++;
            }
        }

        if ( AmountReferences > 1 ) { // got egnough valid distances to reference nodes: localize ourself

            VOLATILE_RADIO e_ttc_math_errorcode Error = ttc_slam_localize_foreigner( Config->Init.Index_SLAM,
                                                                                     Location1,
                                                                                     Location2,
                                                                                     Config->Mobile.Localization.Distances.SLAM,
                                                                                     AmountReferences
                                                                                   );

            if ( 1 ) { // create debug info: positions of all anchor nodes
                VOLATILE_RADIO t_ttc_math_vector3d_xyz AnchorPositions[AmountRepliesRequired];
                ( void ) AnchorPositions;
                for ( t_u8 Index = 0; Index < AmountRepliesRequired; Index++ ) {
                    t_ttc_math_vector3d_xyz* Position = ttc_slam_get_node_position( Config->Init.Index_SLAM, Index + 1 );
                    AnchorPositions[Index].X = Position->X;
                    AnchorPositions[Index].Y = Position->Y;
                    AnchorPositions[Index].Z = Position->Z;
                }
            }

            if ( !Error ) {
                Result = &( Config->Mobile.Localization );
            }
        }
    }

    ttc_radio_user_unlock( Config->Init.Index_Radio, Config->Mobile.RadioUserID );
#endif


    return Result;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_rtls(t_u8 LogicalIndex) {  }

void _ttc_rtls_configuration_check( t_u8 LogicalIndex ) {
    t_ttc_rtls_config* Config = ttc_rtls_get_configuration( LogicalIndex );

    // perform some generic plausibility checks
    const t_ttc_rtls_features* Features = Features;

    // add more architecture independent checks...
    Assert_RTLS_Writable( Config->ConfigSLAM,  ttc_assert_origin_auto );
    Assert_RTLS_Writable( Config->ConfigRadio, ttc_assert_origin_auto );
    Assert_RTLS( Config->ConfigSLAM->Flags.Initialized,  ttc_assert_origin_auto ); // ttc_slam instance to use must be initialized!
    Assert_RTLS( Config->ConfigRadio->Flags.Initialized, ttc_assert_origin_auto ); // ttc_radio_instance to use must be initialized!
    Assert_RTLS( Config->Init.Amount_AnchorIDs >= Config->Features->MinimumAnchorNodes, ttc_assert_origin_auto ); // application must set this value. Check implementation of calling function!
    Assert_RTLS( Config->Init.Amount_AnchorIDs <= Config->Features->MaximumAnchorNodes, ttc_assert_origin_auto ); // application must set this value. Check implementation of calling function!

    // let low-level driver check this configuration too
    _driver_rtls_configuration_check( Config );
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)


//}PrivateFunctions
