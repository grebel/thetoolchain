#ifndef TTC_SPI_H
#define TTC_SPI_H

/*{ ttc_spi.h **********************************************************
 
                      The ToolChain
                      
   Device independent support for 
   Serial Peripheral Interface (SPI)
   
   Currently implemented architectures: stm32

   
   written by Gregor Rebel 2012
   
   Reference:
   
   RM008 - STM32 Reference Manual p. 672
   -> http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/REFERENCE_MANUAL/CD00171190.pdf
   
   
   How it works
   
   Each makefile.100_board* can define one or more SPIs being routed to connectors.
   This is done via lines of this type:
     COMPILE_OPTS += -D<OPTION>=<VALUE>
   This sets compile option <OPTION> to value <VALUE>
   
   SPIn are enumerated starting at index 1 (e.g. SPI1, SPI2, ..).
   Each SPIn can be configured via these constants:
   TTC_SPI1       <SPI>                  sets internal SPI device to use
   TTC_SPI1_MOSI  <GPIO_BANK>,<PIN_NO>   sets port to use for Master Out Slave In pin
   TTC_SPI1_MISO  <GPIO_BANK>,<PIN_NO>   sets port to use for Master In Slave Out pin
   TTC_SPI1_SCK   <GPIO_BANK>,<PIN_NO>   sets port to use for Serial Clock pin
   TTC_SPI1_NSS   <GPIO_BANK>,<PIN_NO>   sets port to use for Slave Select pin
   
   # Example excerpt from makefile.100_board_olimex_p107
   COMPILE_OPTS += -DTTC_SPI1=SPI3          # SPI connected to RS232 10-pin male header UEXT and SDCARD slot
   COMPILE_OPTS += -DTTC_SPI1_MOSI=GPIOC,12 # UEXT pin 8
   COMPILE_OPTS += -DTTC_SPI1_MISO=GPIOC,11 # UEXT pin 7
   COMPILE_OPTS += -DTTC_SPI1_SCK=GPIOC,10  # UEXT pin 9
   #COMPILE_OPTS += -DTTC_SPI1_NSS=         # NSS not connected 
    

}*/
//{ Defines/ TypeDefs ****************************************************

//} Defines
//{ Includes *************************************************************

#include "ttc_basic.h"
#include "ttc_gpio_types.h"
#include "ttc_spi_types.h"

#ifdef TARGET_ARCHITECTURE_STM32F1xx
  #include "stm32_spi.h"
#endif

#ifdef TARGET_ARCHITECTURE_STM32W1xx
  #include "stm32w_spi.h"
#endif


//} Includes
//{ Function prototypes **************************************************

/** checks if indexed SPI bus already has been initialized
 * @param SPI_Index     device index of SPI to check (1..stm32_getMax_SPI_Index())
 * @return              == TRUE: indexed SPI unit is already initialized
 */
bool ttc_spi_check_initialized(u8_t SPI_Index);

/** returns amount of SPI devices available on current uC
 * @return amount of available SPI devices
 */
u8_t ttc_spi_get_max_index();

/** returns reference to configuration struct of indexed SPI device
 * @param SPI_Index     device index of SPI to init (1..stm32_getMax_SPI_Index())
 * @return              != NULL: pointer to struct ttc_spi_generic_t
 */
ttc_spi_generic_t* ttc_spi_get_configuration(u8_t SPI_Index);

/** fills out given SPI_Generic with maximum valid values for indexed SPI
 * @param SPI_Index     device index of SPI to init (1..ttc_spi_get_max_index())
 * @param SPI_Generic   will be loaded with pointer to features of spi
 * @return  == 0:       *SPI_Generic has been initialized successfully; != 0: error-code
 */
ttc_spi_errorcode_e ttc_spi_get_features(u8_t SPI_Index, ttc_spi_generic_t* SPI_Generic);

/** loads configuration of indexed SPI interface with default values
 * @param SPI_Index     device index of SPI to init (1..stm32_getMax_SPI_Index())
 * @return  == 0:       configuration was loaded successfully
 */
ttc_spi_errorcode_e ttc_spi_load_defaults(u8_t SPI_Index);

/** initializes single SPI with its current configuration
 * @param SPI_Index     device index of SPI to init (1..ttc_spi_get_max_index() )
 * @return              == 0: SPI has been initialized successfully; != 0: error-code
 */
ttc_spi_errorcode_e ttc_spi_init(u8_t SPI_Index);

/** Send out given amount of raw bytes from buffer.
 * Will not interpret bytes in buffer.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * 
 * @param SPI_Index    device index of SPI to init (1..ttc_spi_get_max_index() )
 * @param Buffer         memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[]
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_spi_errorcode_e ttc_spi_send_raw(u8_t SPI_Index, const u8_t* Buffer, u16_t Amount);

/** Send out given bytes from buffer until first zero byte or MaxLength reached.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * 
 * @param SPI_Index    device index of SPI to init (1..ttc_spi_get_max_index() )
 * @param Buffer         memory location from which to read data
 * @param MaxLength      no more than this amount of bytes will be send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_spi_errorcode_e ttc_spi_send_string(u8_t SPI_Index, const char* Buffer, u16_t MaxLength);

/** Send out given data word (8 or 9 bits)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param SPI_Index    device index of SPI to init (1..ttc_spi_get_max_index() )
 * @param Byte           8 or 9 bit value to send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_spi_errorcode_e ttc_spi_send_word(u8_t SPI_Index, const u16_t Word);

/** Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param SPI_Index    device index of SPI to init (1..ttc_spi_get_max_index() )
 * @param Word           pointer to 16 bit buffer where to store Word
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
ttc_spi_errorcode_e ttc_spi_read_word(u8_t SPI_Index, u16_t* Word);

/** Reads single data byte from input buffer.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param SPI_Index    device index of SPI to init (1..ttc_spi_get_max_index() )
 * @param Word           pointer to 16 bit buffer where to store Word
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
ttc_spi_errorcode_e ttc_spi_read_byte(u8_t SPI_Index, u8_t* Byte);

/** returns single port pin used for indexed SPI
 *
 * @param SPI_Index    device index of SPI to init (0..ttc_spi_get_max_index()-1)
 * @param Pin          name of pin to return
 * @return             !=NULL: pointer to requested port pin data
 *                     ==NULL: requested pin is not configured
 */
ttc_gpio_pin_e* ttc_spi_get_port(u8_t SPI_Index, ttc_spi_pins_e Pin);

//} Function prototypes

#endif //TTC_SPI_H
