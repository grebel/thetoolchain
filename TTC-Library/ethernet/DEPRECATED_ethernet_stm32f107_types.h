#ifndef ETHERNET_STM32F107_TYPES_H
#define ETHERNET_STM32F107_TYPES_H

/** { ethernet_stm32f107.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for ETHERNET devices on stm32f107 architectures.
 *  Structures, Enums and Defines being required by ttc_ethernet_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150308 11:51:04 UTC
 *
 *  Note: See ttc_ethernet.h for description of architecture independent ETHERNET implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_ethernet_types.h *************************

typedef struct { // register description (adapt according to stm32f107 registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_ethernet_register;

typedef struct {  // stm32f107 specific configuration data of single ETHERNET device
    t_ethernet_register* BaseRegister;       // base address of ETHERNET device registers
} __attribute__( ( __packed__ ) ) t_ethernet_stm32f107_config;

// t_ttc_ethernet_architecture is required by ttc_ethernet_types.h
#define t_ttc_ethernet_architecture t_ethernet_stm32f107_config

typedef union _TranDesc0_t {
    t_u32 Data;
    struct {
        unsigned  DB            : 1;
        unsigned  UF            : 1;
        unsigned  ED            : 1;
        unsigned  CC            : 4;
        unsigned  VF            : 1;
        unsigned  EC            : 1;
        unsigned  LC            : 1;
        unsigned  NC            : 1;
        unsigned  LSC           : 1;
        unsigned  IPE           : 1;
        unsigned  FF            : 1;
        unsigned  JT            : 1;
        unsigned  ES            : 1;
        unsigned  IHE           : 1;
        unsigned                : 3;
        unsigned  TCH           : 1;
        unsigned  TER           : 1;
        unsigned  CIC           : 2;
        unsigned                : 2;
        unsigned  DP            : 1;
        unsigned  DC            : 1;
        unsigned  FS            : 1;
        unsigned  LSEG          : 1;
        unsigned  IC            : 1;
        unsigned  OWN           : 1;
    };
} TranDesc0_t, * t_ptrandesc0;

typedef union _TranDesc1_t {
    t_u32 Data;
    struct {
        unsigned  TBS1          : 13;
        unsigned                : 3;
        unsigned  TBS2          : 12;
        unsigned                : 3;
    };
} TranDesc1_t, * t_ptrandesc1;

typedef union _RecDesc0_t {
    t_u32 Data;
    struct {
        unsigned  RMAM_PCE      : 1;
        unsigned  CE            : 1;
        unsigned  DE            : 1;
        unsigned  RE            : 1;
        unsigned  RWT           : 1;
        unsigned  FT            : 1;
        unsigned  LC            : 1;
        unsigned  IPHCE         : 1;
        unsigned  LS            : 1;
        unsigned  FS            : 1;
        unsigned  VLAN          : 1;
        unsigned  OE            : 1;
        unsigned  LE            : 1;
        unsigned  SAF           : 1;
        unsigned  DERR          : 1;
        unsigned  ES            : 1;
        unsigned  FL            : 14;
        unsigned  AFM           : 1;
        unsigned  OWN           : 1;
    };
} RecDesc0_t, * t_precdesc0;

typedef union _recDesc1_t {
    t_u32 Data;
    struct {
        unsigned  RBS1          : 13;
        unsigned                : 1;
        unsigned  RCH           : 1;
        unsigned  RER           : 1;
        unsigned  RBS2          : 14;
        unsigned  DIC           : 1;
    };
} RecDesc1_t, * t_precdesc1;

typedef union _EnetDmaDesc_t {
    t_u32 Data[4];
    // Rx DMA descriptor
    struct {
        RecDesc0_t                RxDesc0;
        RecDesc1_t                RxDesc1;
        t_u32*                    pBuffer;
        union {
            t_u32*                  pBuffer2;
            union _EnetDmaDesc_t*   pEnetDmaNextDesc;
        };
    } Rx;
    // Tx DMA descriptor
    struct {
        TranDesc0_t               TxDesc0;
        TranDesc1_t               TxDesc1;
        t_u32*                    pBuffer1;
        union {
            t_u32*                  pBuffer2;
            union _EnetDmaDesc_t*   pEnetDmaNextDesc;
        };
    } Tx;
} EnetDmaDesc_t, * t_penetdmadesc;

//} Structures/ Enums


#endif //ETHERNET_STM32F107_TYPES_H
