#ifndef ETHERNET_STE101P_TYPES_H
#define ETHERNET_STE101P_TYPES_H

/** { ethernet_ste101p.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_ethernet.h providing
 *  Structures, Enums and Defines being required by ttc_ethernet_types.h
 *
 *  ethernet interface using deprecated ste101p chip from st microelectronics
 *
 *  Created from template device_architecture_types.h revision 27 at 20180130 11:09:47 UTC
 *
 *  Note: See ttc_ethernet.h for description of high-level ethernet implementation!
 *  Note: See ethernet_ste101p.h for description of ste101p specific ethernet implementation!
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_ETHERNET1   // device not defined in makefile
    #define TTC_ETHERNET1    E_ttc_ethernet_architecture_ste101p   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#ifndef TTC_ETHERNET_TYPES_H
    #  error Do not include ethernet_common_types.h directly. Include ttc_ethernet_types.h instead!
#endif

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_ethernet_types.h *************************

typedef struct {  // ste101p specific configuration data of single ethernet device
    t_u8 unused;         // remove me (C structs may not be empty)!
    // add architecture specific configuration fields here..
} __attribute__( ( __packed__ ) ) t_ethernet_ste101p_config;

// t_ttc_ethernet_architecture is required by ttc_ethernet_types.h
#define t_ttc_ethernet_architecture t_ethernet_ste101p_config

//} Structures/ Enums

#endif //ETHERNET_STE101P_TYPES_H
