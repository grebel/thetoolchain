#ifndef ETHERNET_STM32F107_H
#define ETHERNET_STM32F107_H

/** { ethernet_stm32f107.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for ethernet devices on stm32f107 architectures.
 *  Structures, Enums and Defines being required by high-level ethernet and application.
 *
 *  Created from template device_architecture.h revision 22 at 20150308 11:51:04 UTC
 *
 *  Note: See ttc_ethernet.h for description of stm32f107 independent ETHERNET implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//#define PHY_ADDR  0x15

// Maximum packet size which can be transmitted
#define EMAC_MAX_PACKET_SIZE 1520

// EMAC return codes
#define ENET_OK  (1)
#define ENET_NOK (0)

// Timeout for periodic network tasks [s]
#define PERIODIC_TIMER 0.5

// Timeout for ARP tasks [s]
#define ARP_TIMER      10

// ENET_OperatingMode
//#define PHY_OPR_MODE        0x2100      //  Set the full-duplex mode at 100 Mb/s

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_ETHERNET_STM32F107
//
// Implementation status of low-level driver support for ethernet devices on stm32f107
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_ETHERNET_STM32F107 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_ETHERNET_STM32F107 == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_ETHERNET_STM32F107 to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_ETHERNET_STM32F107

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ethernet_stm32f107.c"
//
#include "ethernet_stm32f107_types.h"
#include "../ttc_ethernet_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_ethernet_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_ethernet_foo
//
#define ttc_driver_ethernet_deinit(Config) ethernet_stm32f107_deinit(Config)
#define ttc_driver_ethernet_get_features(Config) ethernet_stm32f107_get_features(Config)
#define ttc_driver_ethernet_init(Config) ethernet_stm32f107_init(Config)
#define ttc_driver_ethernet_load_defaults(Config) ethernet_stm32f107_load_defaults(Config)
#define ttc_driver_ethernet_prepare() ethernet_stm32f107_prepare()
#define ttc_driver_ethernet_reset(Config) ethernet_stm32f107_reset(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_ethernet.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_ethernet.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** shutdown single ETHERNET unit device
 * @param Config        pointer to struct t_ttc_ethernet_config (must have valid value for PhysicalIndex)
 * @return              == 0: ETHERNET has been shutdown successfully; != 0: error-code
 */
e_ttc_ethernet_errorcode ethernet_stm32f107_deinit( t_ttc_ethernet_config* Config );


/** fills out given Config with maximum valid values for indexed ETHERNET
 * @param Config        = pointer to struct t_ttc_ethernet_config (must have valid value for PhysicalIndex)
 * @return        pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_ethernet_config*  ethernet_stm32f107_get_features( t_ttc_ethernet_config* Config );


/** initializes single ETHERNET unit for operation
 * @param Config        pointer to struct t_ttc_ethernet_config (must have valid value for PhysicalIndex)
 * @return              == 0: ETHERNET has been initialized successfully; != 0: error-code
 */
e_ttc_ethernet_errorcode ethernet_stm32f107_init( t_ttc_ethernet_config* Config );


/** loads configuration of indexed ETHERNET unit with default values
 * @param Config        pointer to struct t_ttc_ethernet_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_ethernet_errorcode ethernet_stm32f107_load_defaults( t_ttc_ethernet_config* Config );


/** Prepares ethernet Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ethernet_stm32f107_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_ethernet_config (must have valid value for PhysicalIndex)
 */
void ethernet_stm32f107_reset( t_ttc_ethernet_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ethernet_stm32f107_foo(t_ttc_ethernet_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //ETHERNET_STM32F107_H
