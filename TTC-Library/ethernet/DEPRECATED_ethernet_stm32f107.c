/** { ethernet_stm32f107.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for ethernet devices on stm32f107 architectures.
 *  Implementation of low-level driver.
 *  This implementation is based on ethernet demo for Olimex P107 board.
 *
 *  Created from template device_architecture.c revision 22 at 20150309 09:08:51 UTC
 *
 *  Note: See ttc_ethernet.h for description of stm32f107 independent ETHERNET implementation.
 *
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ethernet_stm32f107.h".
//
#include "ethernet_stm32f107.h"
#include "../../additionals/400_network_uip_ste101p//Libraries/STM32_ETH_Driver/inc/stm32_eth.h"
#include "../../additionals/400_network_uip_ste101p//Ethernet/uip/uip_arp.h"
#include "../ttc_gpio.h"
#include "../ttc_memory.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************


t_u8 ttc_ethernet_stm32f107_RxBuff[EMAC_MAX_PACKET_SIZE] __attribute__( ( aligned( 4 ) ) );
t_u8 ttc_ethernet_stm32f107_TxBuff[EMAC_MAX_PACKET_SIZE] __attribute__( ( aligned( 4 ) ) );

#define BUF ((struct uip_eth_hdr *)&uip_buf[0])

EnetDmaDesc_t ttc_ethernet_stm32f107_EnetDmaRx __attribute__( ( aligned( 128 ) ) );
EnetDmaDesc_t ttc_ethernet_stm32f107_EnetDmaTx __attribute__( ( aligned( 128 ) ) );

//}Global Variables
//{ Private Function Declarations **********************************************
t_u32 _ethernet_stm32f107_PhyInit( void );
t_u16 _ethernet_stm32f107_PhyRead( t_u8 PhyAddr, t_u8 Reg );
void _ethernet_stm32f107_PhyWri( t_u8 PhyAddr, t_u8 Reg, t_u16 Data );
void _ethernet_stm32f107_TapDevInit( void );

/** Read data to MAC/DMA Controller */
t_u32 _ethernet_stm32f107_TapDevRead( void* pPacket );

/** Send data to MAC/DMA Controller */
void _ethernet_stm32f107_TapDevSend( void* pPacket, t_u32 size );

/** Initializes the Rx ENET descriptor chain. Single Descriptor */
void _ethernet_stm32f107_RxDscrInit( void );

/** Initializes the Tx ENET descriptor chain. Single Descriptor */
void _ethernet_stm32f107_TxDscrInit( void );

//}Private Function Declarations
//{ Function Definitions *******************************************************

t_ttc_ethernet_config* ethernet_stm32f107_get_features( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    static t_ttc_ethernet_config Features;
    if ( !Features.LogicalIndex ) { // called first time: initialize data

    }
    Features.LogicalIndex = Config->LogicalIndex;

    return ( t_ttc_ethernet_config* ) 0;
}
e_ttc_ethernet_errorcode ethernet_stm32f107_deinit( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET( Config, ttc_assert_origin_auto ); // pointers must not be NULL

#warning Function ethernet_stm32f107_deinit() is empty.

    return ( e_ttc_ethernet_errorcode ) 0;
}
e_ttc_ethernet_errorcode ethernet_stm32f107_init( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    ethernet_stm32f107_reset( Config );

    switch ( Config->Interface ) {
        case ethernet_interface_rmii: { // Reduced Media Interchange Interface
            //ttc_gpio_init();

            break;
        }
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // -> implement configuration of new interface or select a supported interface!
    }

    return ( e_ttc_ethernet_errorcode ) 0;
}
e_ttc_ethernet_errorcode ethernet_stm32f107_load_defaults( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET( Config, ttc_assert_origin_auto ); // pointers must not be NULL

#warning Function ethernet_stm32f107_load_defaults() is empty.

    return ( e_ttc_ethernet_errorcode ) 0;
}
void ethernet_stm32f107_prepare() {

}
void ethernet_stm32f107_reset( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    ttc_memory_set( ttc_ethernet_stm32f107_RxBuff, 0, EMAC_MAX_PACKET_SIZE );
    ttc_memory_set( ttc_ethernet_stm32f107_TxBuff, 0, EMAC_MAX_PACKET_SIZE );
    ttc_memory_set( &ttc_ethernet_stm32f107_EnetDmaRx, 0, sizeof( ttc_ethernet_stm32f107_EnetDmaRx ) );
    ttc_memory_set( &ttc_ethernet_stm32f107_EnetDmaTx, 0, sizeof( ttc_ethernet_stm32f107_EnetDmaTx ) );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

void modbusInit() {
    ETH_InitTypeDef ETH_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Enable ETHERNET clock  */
    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_ETH_MAC | RCC_AHBPeriph_ETH_MAC_Tx |
                           RCC_AHBPeriph_ETH_MAC_Rx, ENABLE );

    /* Enable GPIOs clocks */
    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA |   RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC |
                            RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE | RCC_APB2Periph_AFIO, ENABLE );

    GPIO_ETH_MediaInterfaceConfig( GPIO_ETH_MediaInterface_RMII );

    /* Get HSE clock = 25MHz on PA8 pin(MCO) */
    /* set PLL3 clock output to 50MHz (25MHz /5 *10 =50MHz) */
    RCC_PLL3Config( RCC_PLL3Mul_10 );
    /* Enable PLL3 */
    RCC_PLL3Cmd( ENABLE );
    /* Wait till PLL3 is ready */
    while ( RCC_GetFlagStatus( RCC_FLAG_PLL3RDY ) == RESET );

    /* Get clock PLL3 clock on PA8 pin */
    RCC_MCOConfig( RCC_MCO_PLL3CLK );

    /* Get clock PLL3 clock on PA8 pin */
    RCC_MCOConfig( RCC_MCO_PLL3CLK );

    /* ETHERNET pins configuration */
    /* AF Output Push Pull:
    - ETH_MII_MDIO / ETH_RMII_MDIO: PA2
    - ETH_MII_MDC / ETH_RMII_MDC: PC1
    - ETH_MII_TXD2: PC2
    - ETH_MII_TX_EN / ETH_RMII_TX_EN: PB11
    - ETH_MII_TXD0 / ETH_RMII_TXD0: PB12
    - ETH_MII_TXD1 / ETH_RMII_TXD1: PB13
    - ETH_MII_PPS_OUT / ETH_RMII_PPS_OUT: PB5
    - ETH_MII_TXD3: PB8 */

    /* Configure PA2 as alternate function push-pull */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOA, &GPIO_InitStructure );

    /* Configure PC1, PC2 and PC3 as alternate function push-pull */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOC, &GPIO_InitStructure );

    /* Configure PB5, PB8, PB11, PB12 and PB13 as alternate function push-pull */
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_11 |
                                   GPIO_Pin_12 | GPIO_Pin_13;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOB, &GPIO_InitStructure );

    /**************************************************************/
    /*               For Remapped Ethernet pins                   */
    /*************************************************************/
    /* Input (Reset Value):
    - ETH_MII_CRS CRS: PA0
    - ETH_MII_RX_CLK / ETH_RMII_REF_CLK: PA1
    - ETH_MII_COL: PA3
    - ETH_MII_RX_DV / ETH_RMII_CRS_DV: PD8
    - ETH_MII_TX_CLK: PC3
    - ETH_MII_RXD0 / ETH_RMII_RXD0: PD9
    - ETH_MII_RXD1 / ETH_RMII_RXD1: PD10
    - ETH_MII_RXD2: PD11
    - ETH_MII_RXD3: PD12
    - ETH_MII_RX_ER: PB10 */

    /* ETHERNET pins remapp in STM3210C-EVAL board: RX_DV and RxD[3:0] */
    GPIO_PinRemapConfig( GPIO_Remap_ETH, DISABLE );

    /* Configure PA0, PA1 and PA3 as input */
    GPIO_InitStructure.GPIO_Pin = /*GPIO_Pin_0 |*/ GPIO_Pin_1 | GPIO_Pin_7 ;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init( GPIOA, &GPIO_InitStructure );

    /* Configure PB10 as input */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init( GPIOB, &GPIO_InitStructure );

    /* Configure PC3 as input */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init( GPIOC, &GPIO_InitStructure );

    /* Configure PD8, PD9, PD10, PD11 and PD12 as input */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init( GPIOC, &GPIO_InitStructure ); /**/

    /* MCO pin configuration------------------------------------------------- */
    /* Configure MCO (PA8) as alternate function push-pull */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init( GPIOA, &GPIO_InitStructure );

    /* Reset ETHERNET on AHB Bus */
    ETH_DeInit();

    /* Software reset */
    ETH_SoftwareReset();

    /* Wait for software reset */
    while ( ETH_GetSoftwareResetStatus() == SET );

    /* ETHERNET Configuration ------------------------------------------------------*/
    /* Call ETH_StructInit if you don't like to configure all ETH_InitStructure parameter */
    ETH_StructInit( &ETH_InitStructure );

    /* Fill ETH_InitStructure parametrs */
    /*------------------------   MAC   -----------------------------------*/
    ETH_InitStructure.ETH_AutoNegotiation = ETH_AutoNegotiation_Disable  ;
    //ETH_InitStructure.ETH_Speed = ETH_Speed_100M;
    ETH_InitStructure.ETH_LoopbackMode = ETH_LoopbackMode_Disable;
    //ETH_InitStructure.ETH_Mode = ETH_Mode_FullDuplex;
    ETH_InitStructure.ETH_RetryTransmission = ETH_RetryTransmission_Disable;
    ETH_InitStructure.ETH_AutomaticPadCRCStrip = ETH_AutomaticPadCRCStrip_Disable;
    ETH_InitStructure.ETH_ReceiveAll = ETH_ReceiveAll_Enable;
    ETH_InitStructure.ETH_BroadcastFramesReception = ETH_BroadcastFramesReception_Disable;
    ETH_InitStructure.ETH_PromiscuousMode = ETH_PromiscuousMode_Disable;
    ETH_InitStructure.ETH_MulticastFramesFilter = ETH_MulticastFramesFilter_Perfect;
    ETH_InitStructure.ETH_UnicastFramesFilter = ETH_UnicastFramesFilter_Perfect;
    ETH_InitStructure.ETH_Mode = ETH_Mode_FullDuplex;
    ETH_InitStructure.ETH_Speed = ETH_Speed_100M;

    unsigned int PhyAddr;
    for ( PhyAddr = 1; 32 >= PhyAddr; PhyAddr++ ) {
        if ( ( 0x0006 == ETH_ReadPHYRegister( PhyAddr, 2 ) ) && ( 0x1c50 == ( ETH_ReadPHYRegister( PhyAddr, 3 ) & 0xFFF0 ) ) ) { break; }
    }

    if ( 32 < PhyAddr ) {
        printf( "Ethernet Phy Not Found\n\r" );
        return;
    }
    /* Configure Ethernet */
    if ( 0 == ETH_Init( &ETH_InitStructure, PhyAddr ) ) {
        printf( "Ethernet Initialization Failed\n\r" );
        return;
    }
}
void modbusTask( void* pvParameters ) {
    uip_ipaddr_t ipaddr;
    portTickType periodic_tick, arp_tick;
    ( void )pvParameters;

    // Schedule periodic tasks
    periodic_tick = xTaskGetTickCount() + ( portTickType )( PERIODIC_TIMER * portTICK_RATE_MS );
    arp_tick = xTaskGetTickCount() + ( portTickType )( ARP_TIMER * portTICK_RATE_MS );

    // Initialize the ethernet device driver
    // Init MAC
    // Phy network negotiation
    _ethernet_stm32f107_TapDevInit();

    // uIP web server
    // Initialize the uIP TCP/IP stack.
    uip_init();

    // Init WEB server
    uip_ipaddr( ipaddr, 192, 168, 0, 114 );
    uip_sethostaddr( ipaddr );
    uip_ipaddr( ipaddr, 192, 168, 0, 1 );
    uip_setdraddr( ipaddr );
    uip_ipaddr( ipaddr, 255, 255, 255, 0 );
    uip_setnetmask( ipaddr );

    // Run the main loop
    while ( 1 ) {

        // handle incoming data from the network interface
        uip_len = _ethernet_stm32f107_TapDevRead( uip_buf );
        if ( uip_len > 0 ) {

            if ( BUF->type == htons( UIP_ETHTYPE_IP ) ) {
                uip_arp_ipin();
                uip_input();
                if ( uip_len > 0 ) {
                    uip_arp_out();
                    _ethernet_stm32f107_TapDevSend( uip_buf, uip_len );
                }
            }
            else if ( BUF->type == htons( UIP_ETHTYPE_ARP ) ) {
                uip_arp_arpin();
                if ( uip_len > 0 ) {
                    _ethernet_stm32f107_TapDevSend( uip_buf, uip_len );
                }
            }

        }
        else if ( xTaskGetTickCount() > periodic_tick ) {
            periodic_tick += ( portTickType )( PERIODIC_TIMER * portTICK_RATE_MS );

            for ( int i = 0; i < UIP_CONNS; i++ ) {
                uip_periodic( i );
                if ( uip_len > 0 ) {
                    uip_arp_out();
                    _ethernet_stm32f107_TapDevSend( uip_buf, uip_len );
                }
            }

            /* Call the ARP timer function every 10 seconds. */
            if ( xTaskGetTickCount() > arp_tick ) {
                arp_tick += ( portTickType )( ARP_TIMER * portTICK_RATE_MS );
                uip_arp_timer();
            }
        }
    }
}
void _ethernet_stm32f107_RxDscrInit( void ) {
    /* Initialization */
    /* Assign temp Rx array to the ENET buffer */
    ttc_ethernet_stm32f107_EnetDmaRx.Rx.pBuffer = ( t_u32* ) ttc_ethernet_stm32f107_RxBuff;

    /* Initialize RX ENET Status and control */
    ttc_ethernet_stm32f107_EnetDmaRx.Rx.RxDesc0.Data = 0;

    /* Initialize the next descriptor- In our case its single descriptor */
    ttc_ethernet_stm32f107_EnetDmaRx.Rx.pEnetDmaNextDesc = &ttc_ethernet_stm32f107_EnetDmaRx;

    ttc_ethernet_stm32f107_EnetDmaRx.Rx.RxDesc1.Data = 0;
    ttc_ethernet_stm32f107_EnetDmaRx.Rx.RxDesc1.RER  = 0; // end of ring
    ttc_ethernet_stm32f107_EnetDmaRx.Rx.RxDesc1.RCH  = 1; // end of ring

    /* Set the max packet size  */
    ttc_ethernet_stm32f107_EnetDmaRx.Rx.RxDesc1.RBS1 = EMAC_MAX_PACKET_SIZE;

    /* Setting the VALID bit */
    ttc_ethernet_stm32f107_EnetDmaRx.Rx.RxDesc0.OWN = 1;
    /* Setting the RX NEXT Descriptor Register inside the ENET */
    ETH->DMARDLAR = ( t_u32 ) &ttc_ethernet_stm32f107_EnetDmaRx;
    /* Setting the RX NEXT Descriptor Register inside the ENET */
    //ETH_DMARDLAR = (t_u32)&EnetDmaRx;
}
void _ethernet_stm32f107_TxDscrInit( void ) {
    /* ENET Start Address */
    ttc_ethernet_stm32f107_EnetDmaTx.Tx.pBuffer1 = ( t_u32* ) ttc_ethernet_stm32f107_TxBuff;

    /* Next Descriptor Address */
    ttc_ethernet_stm32f107_EnetDmaTx.Tx.pEnetDmaNextDesc = &ttc_ethernet_stm32f107_EnetDmaRx;

    /* Initialize ENET status and control */
    ttc_ethernet_stm32f107_EnetDmaTx.Tx.TxDesc0.TCH  = 1;
    ttc_ethernet_stm32f107_EnetDmaTx.Tx.TxDesc0.Data = 0;
    ttc_ethernet_stm32f107_EnetDmaTx.Tx.TxDesc1.Data = 0;
    /* Tx next set to Tx descriptor base */
    ETH->DMATDLAR = ( t_u32 ) &ttc_ethernet_stm32f107_EnetDmaRx;

}

void _ethernet_stm32f107_TapDevInit( void ) {
    _ethernet_stm32f107_TxDscrInit();
    _ethernet_stm32f107_RxDscrInit();
    ETH_Start();
}
t_u32 _ethernet_stm32f107_TapDevRead( void* pPacket ) {
    t_u32 size;
    /*check for validity*/
    if ( 0 == ttc_ethernet_stm32f107_EnetDmaRx.Rx.RxDesc0.OWN ) {
        /*Get the size of the packet*/
        size = ttc_ethernet_stm32f107_EnetDmaRx.Rx.RxDesc0.FL; // CRC
        //MEMCOPY_L2S_BY4((u8*)ppkt, ttc_ethernet_stm32f107_RxBuff, size); /*optimized memcopy function*/
        memcpy( pPacket, ttc_ethernet_stm32f107_RxBuff, size ); //string.h library*/
    }
    else {
        return ( ENET_NOK );
    }
    /* Give the buffer back to ENET */
    ttc_ethernet_stm32f107_EnetDmaRx.Rx.RxDesc0.OWN = 1;
    /* Start the receive operation */
    ETH->DMARPDR = 1;
    /* Return no error */
    return size;
}

void _ethernet_stm32f107_TapDevSend( void* pPacket, t_u32 size ) {
    while ( ttc_ethernet_stm32f107_EnetDmaTx.Tx.TxDesc0.OWN );

    /* Copy the  application buffer to the driver buffer
       Using this MEMCOPY_L2L_BY4 makes the copy routine faster
       than memcpy */
    //MEMCOPY_L2S_BY4((u8*)ttc_ethernet_stm32f107_TxBuff, (u8*)ppkt, size);
    memcpy( ttc_ethernet_stm32f107_TxBuff, pPacket, size );

    /* Assign ENET address to Temp Tx Array */
    ttc_ethernet_stm32f107_EnetDmaTx.Tx.pBuffer1 = ( t_u32* ) ttc_ethernet_stm32f107_TxBuff;

    /* Setting the Frame Length*/
    ttc_ethernet_stm32f107_EnetDmaTx.Tx.TxDesc0.Data = 0;
    ttc_ethernet_stm32f107_EnetDmaTx.Tx.TxDesc0.TCH  = 1;
    ttc_ethernet_stm32f107_EnetDmaTx.Tx.TxDesc0.LSEG = 1;
    ttc_ethernet_stm32f107_EnetDmaTx.Tx.TxDesc0.FS   = 1;
    ttc_ethernet_stm32f107_EnetDmaTx.Tx.TxDesc0.DC   = 0;
    ttc_ethernet_stm32f107_EnetDmaTx.Tx.TxDesc0.DP   = 0;

    ttc_ethernet_stm32f107_EnetDmaTx.Tx.TxDesc1.Data = 0;
    ttc_ethernet_stm32f107_EnetDmaTx.Tx.TxDesc1.TBS1 = ( size & 0xFFF );

    /* Start the ENET by setting the VALID bit in dmaPackStatus of current descr*/
    ttc_ethernet_stm32f107_EnetDmaTx.Tx.TxDesc0.OWN = 1;

    /* Start the transmit operation */
    ETH->DMATPDR = 1;
}


//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
