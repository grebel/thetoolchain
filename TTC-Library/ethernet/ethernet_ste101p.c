/** ethernet_ste101p.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_ethernet.c.
 *
 *  ethernet interface using deprecated ste101p chip from st microelectronics
 *
 *  Created from template device_architecture.c revision 38 at 20180130 11:09:47 UTC
 *
 *  Note: See ttc_ethernet.h for description of high-level ethernet implementation.
 *  Note: See ethernet_ste101p.h for description of ste101p ethernet implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "ethernet_ste101p.h".
 */

#include "ethernet_ste101p.h"
#include "../ttc_heap.h"              // dynamic memory allocation (e.g. malloc() )
#include "../ttc_memory.h"            // basic memory checks
#include "../ttc_task.h"              // stack checking (provides TTC_TASK_RETURN() macro)
#include "ethernet_common.h"          // generic functions shared by low-level drivers of all architectures 
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_ethernet_features ethernet_ste101p_Features = {
    /** example features
      .MinBitRate       = 4800,
      .MaxBitRate       = 230400,
    */
    // set more feature values...

    .unused = 0
};

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

e_ttc_ethernet_errorcode ethernet_ste101p_load_defaults( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET_EXTRA_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = E_ttc_ethernet_architecture_ste101p;         // set type of architecture
    Config->Features     = &ethernet_ste101p_Features;  // assign feature set

    if ( 0 ) { // enable for memory mapped devices (everything that has a base register)
        switch ( Config->PhysicalIndex ) { // load address of peripheral base register
            //case 0: Config->BaseRegister = & register_ste101p_ETHERNET1; break;
            //case 1: Config->BaseRegister = & register_ste101p_ETHERNET2; break;
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid physical index: check your makefile and high-level driver implementation!
        }
    }

    // allocate memory for ste101p specific configuration
    Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_ttc_ethernet_architecture ) );

    // reset all flags
    ttc_memory_set( &( Config->Flags ), 0, sizeof( Config->Flags ) );

    // set individual feature flags
    //Config->Flags.Foo = 1;
    // add more flags...


    TTC_TASK_RETURN( 0 ); // will perform stack overflow check and return given value
}
void                     ethernet_ste101p_configuration_check( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    TODO( "Function ethernet_ste101p_configuration_check() is empty." )


}
e_ttc_ethernet_errorcode ethernet_ste101p_deinit( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    TODO( "Function ethernet_ste101p_deinit() is empty." )

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
e_ttc_ethernet_errorcode ethernet_ste101p_init( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    TODO( "Function ethernet_ste101p_init() is empty." )

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
void                     ethernet_ste101p_prepare() {


    TODO( "Function ethernet_ste101p_prepare() is empty." )


}
void                     ethernet_ste101p_reset( t_ttc_ethernet_config* Config ) {
    Assert_ETHERNET_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    TODO( "Function ethernet_ste101p_reset() is empty." )


}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
