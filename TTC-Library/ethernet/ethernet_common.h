#ifndef ethernet_common_h
#define ethernet_common_h

/** ethernet_common.h ****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to ethernet low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.h revision 14 at 20180124 15:56:06 UTC
 *
 *  Authors: Gregor Rebel
 *
 *  Description of ethernet_common (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "ethernet_ste101p.c"
 */

#include "../ttc_basic.h"
#include "../ttc_ethernet_types.h" // will include ethernet_common_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_ethernet_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_ethernet_foo
 */

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions
/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_ethernet.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_ethernet.h for details.
 *
 * You normally should not need to add functions here.
 * Global feature functions must be added in ttc_ethernet.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl ethernet UPDATE".
 */

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //ethernet_common_h
