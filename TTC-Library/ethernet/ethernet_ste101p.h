#ifndef ETHERNET_STE101P_H
#define ETHERNET_STE101P_H

/** { ethernet_ste101p.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_ethernet.h providing
 *  function prototypes being required by ttc_ethernet.h
 *
 *  ethernet interface using deprecated ste101p chip from st microelectronics
 *
 *  Created from template device_architecture.h revision 32 at 20180130 11:09:47 UTC
 *
 *  Note: See ttc_ethernet.h for description of ste101p independent ETHERNET implementation.
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of ethernet_ste101p (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_ETHERNET_DRIVER_AVAILABLE
#define EXTENSION_ETHERNET_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_ethernet_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ethernet_ste101p.c"
//
#include "../ttc_ethernet_types.h" // will include ethernet_ste101p_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_ethernet_interface.h
//
// Some driver functions may be optional to implement.
// Check ttc_ethernet_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_ethernet_foo
//
#define ttc_driver_ethernet_configuration_check ethernet_ste101p_configuration_check
#define ttc_driver_ethernet_deinit ethernet_ste101p_deinit
#define ttc_driver_ethernet_init ethernet_ste101p_init
#define ttc_driver_ethernet_load_defaults ethernet_ste101p_load_defaults
#define ttc_driver_ethernet_prepare ethernet_ste101p_prepare
#define ttc_driver_ethernet_reset ethernet_ste101p_reset
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Macro definitions
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_ethernet.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_ethernet.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_ethernet_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_ethernet_features.
 *
 * @param Config        Configuration of ethernet device
 * @return              Fields of *Config have been adjusted if necessary
 */
void ethernet_ste101p_configuration_check( t_ttc_ethernet_config* Config );


/** shutdown single ETHERNET unit device
 * @param Config        Configuration of ethernet device
 * @return              == 0: ETHERNET has been shutdown successfully; != 0: error-code
 */
e_ttc_ethernet_errorcode ethernet_ste101p_deinit( t_ttc_ethernet_config* Config );


/** initializes single ETHERNET unit for operation
 * @param Config        Configuration of ethernet device
 * @return              == 0: ETHERNET has been initialized successfully; != 0: error-code
 */
e_ttc_ethernet_errorcode ethernet_ste101p_init( t_ttc_ethernet_config* Config );


/** loads configuration of indexed ETHERNET unit with default values
 * @param Config        Configuration of ethernet device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_ethernet_errorcode ethernet_ste101p_load_defaults( t_ttc_ethernet_config* Config );


/** Prepares ethernet Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ethernet_ste101p_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of ethernet device
 */
void ethernet_ste101p_reset( t_ttc_ethernet_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //ETHERNET_STE101P_H
