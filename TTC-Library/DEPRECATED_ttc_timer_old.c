/** { ttc_timer.c *******************************************************

                           The ToolChain
                           
   High-Level interface for TIMER device.

   Implementation of high-level interface.

   Authors:
   
}*/
//{ Defines/ TypDefs *****************************************************
//} Defines

#include "ttc_timer_old.h"

//{ Global Variables *****************************************************
ttc_heap_pool_t* MemoryPool = NULL;
ttc_heap_pool_t* HardTimerPool = NULL;
ttc_time_event_t* HeadEvent = NULL;
ttc_timer_config_t* HeadHardwareTimerEvent = NULL;

//} Global Variables
//{ Function definitions *************************************************

//  Basic timer operations ********************************************
void ttc_timer_prepare(){
    ttc_software_timer_init();
    ttc_hardware_timer_init();
}

//  Software timer operations ********************************************

void ttc_software_timer_init(){
    MemoryPool = ttc_heap_pool_create(sizeof(ttc_time_event_t), 10); //Create the memory pool for timer events
    if(MemoryPool != NULL)
        ttc_task_create(_ttc_software_timer_, "checkNextEvent", 128, NULL, 1, NULL);
}

void ttc_software_timer_deinit(ttc_time_event_t* EventToDelete){
    ttc_task_begin_criticalsection(ttc_software_timer_deinit);
    ttc_time_event_t* CurrentEvent = HeadEvent; //Take the first element of the event list
    ttc_task_end_criticalsection();
    ttc_time_event_t* LastEvent = NULL;
    // set to TRUE once Event has been found
    bool found = 0;

    if(EventToDelete == HeadEvent){
        ttc_task_begin_criticalsection(ttc_software_timer_deinit);
        HeadEvent = HeadEvent->Next;
        ttc_task_end_criticalsection();

        ttc_heap_pool_block_free((ttc_memory_from_pool_t*) CurrentEvent);
        found = 1;
    }
    else{
        ttc_task_begin_criticalsection(ttc_software_timer_deinit);
        LastEvent = CurrentEvent;
        CurrentEvent = CurrentEvent->Next;
        ttc_task_end_criticalsection();
    }

    while(!found && CurrentEvent->Next){
        if(EventToDelete == CurrentEvent){
            ttc_task_begin_criticalsection(ttc_software_timer_deinit);
            LastEvent->Next = CurrentEvent->Next;
            ttc_task_end_criticalsection();

            ttc_heap_pool_block_free((ttc_memory_from_pool_t*) CurrentEvent);
            found = 1;
         }
        else{
            ttc_task_begin_criticalsection(ttc_software_timer_deinit);
            LastEvent = CurrentEvent;
            CurrentEvent = CurrentEvent->Next;
            ttc_task_end_criticalsection();
        }
    }
}

void ttc_software_timer_reset(){
    ttc_task_begin_criticalsection(ttc_software_timer_reset);
    ttc_time_event_t* CurrentEvent = HeadEvent; //Take the first element of the event list
    HeadEvent = NULL;
    ttc_task_end_criticalsection();

    while(CurrentEvent->Next){
        ttc_task_begin_criticalsection(ttc_software_timer_reset);
        ttc_time_event_t* EventToRelease = CurrentEvent; //Take the first element of the event list
        CurrentEvent = CurrentEvent->Next;
        ttc_task_end_criticalsection();

        ttc_heap_pool_block_free((ttc_memory_from_pool_t*) EventToRelease);
    }
}

ttc_time_event_t* ttc_software_timer_set(void TaskFunction(void*), void* Argument, Base_t TimePeriod, bool Continuous){
   Assert_TIMER(TaskFunction, tte_InvalidArgument);
   Assert_TIMER(TimePeriod > 0, tte_InvalidArgument);

   ttc_time_event_t* NewEvent = (ttc_time_event_t*) ttc_heap_pool_block_get(MemoryPool);

   if(NewEvent != NULL){
       ttc_task_begin_criticalsection(ttc_software_timer_set);
       NewEvent->Function = TaskFunction;
       NewEvent->Argument = Argument;
       NewEvent->TimeStart = ttc_task_get_elapsed_usecs();
       NewEvent->TimePeriod = TimePeriod;
       NewEvent->Continuous = Continuous;
       NewEvent->Next = NULL;
       ttc_task_end_criticalsection();

       _ttc_software_timer_insert_event_(NewEvent);
       return NewEvent;
    }
   else return NULL;
}

// Hardware timer operations ********************************************
ttc_timer_errorcode_e ttc_hardware_timer_init(){
    HardTimerPool = ttc_heap_pool_create(sizeof(ttc_timer_config_t), TTC_INTERRUPT_TIMER_AMOUNT); //Create the memory pool for timer events
    if(HardTimerPool != NULL)
        return ttc_timer_interface_init();
    else return tte_UnknownError;
}

void ttc_hardware_timer_set(u8_t TimerIndex, void TaskFunction(void*), void* Argument, Base_t TimePeriod, u8_t Scale){
    Assert_TIMER(TimerIndex > 0, tte_InvalidArgument);
    Assert_TIMER(TaskFunction, tte_InvalidArgument);
    Assert_TIMER(TimePeriod > 0, tte_InvalidArgument);
    ttc_timer_config_t* NewConfig = HeadHardwareTimerEvent;
    bool stop = 0;

    if(NewConfig == NULL){
        NewConfig = (ttc_timer_config_t*) ttc_heap_pool_block_get(HardTimerPool);
        NewConfig->TimerEvent.Function = TaskFunction;
        NewConfig->TimerEvent.Argument = Argument;
        NewConfig->TimerEvent.TimePeriod = TimePeriod;
        NewConfig->TimerEvent.Next = NULL;
        NewConfig->Counter = 0;
        NewConfig->LogicalIndex = TimerIndex;
        NewConfig->TimeScale = Scale;
        NewConfig->eti_NextISR = 0;
        NewConfig->eti_NextISR_Argument = 0;
        NewConfig->Next = NULL;
        HeadHardwareTimerEvent = NewConfig;
    }
    else if(NewConfig->LogicalIndex == TimerIndex){
        NewConfig->TimerEvent.Function = TaskFunction;
        NewConfig->TimerEvent.Argument = Argument;
        NewConfig->TimerEvent.TimePeriod = TimePeriod;
        NewConfig->Counter = 0;
        NewConfig->TimeScale = Scale;
        NewConfig->eti_NextISR = 0;
        NewConfig->eti_NextISR_Argument = 0;
    }
    else{
        while((NewConfig->Next != NULL) && !stop){
            if(NewConfig->LogicalIndex != TimerIndex)
                NewConfig = NewConfig->Next;
            else stop=1;
        }

        if(!stop){
            NewConfig->Next = (ttc_timer_config_t*) ttc_heap_pool_block_get(HardTimerPool);
            NewConfig = NewConfig->Next;
            NewConfig->TimerEvent.Next = NULL;
            NewConfig->LogicalIndex = TimerIndex;
            NewConfig->Next = NULL;
        }
        NewConfig->TimerEvent.Function = TaskFunction;
        NewConfig->TimerEvent.Argument = Argument;
        NewConfig->TimerEvent.TimePeriod = TimePeriod;
        NewConfig->Counter = 0;
        NewConfig->TimeScale = Scale;
        NewConfig->eti_NextISR = 0;
        NewConfig->eti_NextISR_Argument = 0;
    }

    _ttc_hardware_timer_create(NewConfig, 0);
}

void ttc_hardware_timer_set_isr(u8_t TimerIndex, void TaskFunction(void*), void* Argument, Base_t TimePeriod, u8_t Scale){
    Assert_TIMER(TimerIndex > 0, tte_InvalidArgument);
    Assert_TIMER(TaskFunction, tte_InvalidArgument);
    Assert_TIMER(TimePeriod > 0, tte_InvalidArgument);
    ttc_timer_config_t* NewConfig = HeadHardwareTimerEvent;
    bool stop = 0;

    if(NewConfig == NULL){
        NewConfig = (ttc_timer_config_t*) ttc_heap_pool_block_get(HardTimerPool);
        NewConfig->TimerEvent.Function = TaskFunction;
        NewConfig->TimerEvent.Argument = Argument;
        NewConfig->TimerEvent.TimePeriod = TimePeriod;
        NewConfig->TimerEvent.Next = NULL;
        NewConfig->Counter = 0;
        NewConfig->LogicalIndex = TimerIndex;
        NewConfig->TimeScale = Scale;
        NewConfig->eti_NextISR = 0;
        NewConfig->eti_NextISR_Argument = 0;
        NewConfig->Next = NULL;
        HeadHardwareTimerEvent = NewConfig;
    }
    else if(NewConfig->LogicalIndex == TimerIndex){
        NewConfig->TimerEvent.Function = TaskFunction;
        NewConfig->TimerEvent.Argument = Argument;
        NewConfig->TimerEvent.TimePeriod = TimePeriod;
        NewConfig->Counter = 0;
        NewConfig->TimeScale = Scale;
        NewConfig->eti_NextISR = 0;
        NewConfig->eti_NextISR_Argument = 0;
    }
    else{
        while((NewConfig->Next != NULL) && !stop){
            if(NewConfig->LogicalIndex != TimerIndex)
                NewConfig = NewConfig->Next;
            else stop=1;
        }

        if(!stop){
            NewConfig->Next = (ttc_timer_config_t*) ttc_heap_pool_block_get(HardTimerPool);
            NewConfig = NewConfig->Next;
            NewConfig->TimerEvent.Next = NULL;
            NewConfig->LogicalIndex = TimerIndex;
            NewConfig->Next = NULL;
        }
        NewConfig->TimerEvent.Function = TaskFunction;
        NewConfig->TimerEvent.Argument = Argument;
        NewConfig->TimerEvent.TimePeriod = TimePeriod;
        NewConfig->Counter = 0;
        NewConfig->TimeScale = Scale;
        NewConfig->eti_NextISR = 0;
        NewConfig->eti_NextISR_Argument = 0;
    }

    _ttc_hardware_timer_create(NewConfig, 1);
}

ttc_timer_errorcode_e ttc_hardware_timer_reset(ttc_timer_config_t* Config){
    Assert_TIMER(Config, tte_InvalidArgument);
    return ttc_timer_interface_reset(Config);
}

ttc_timer_errorcode_e ttc_hardware_timer_stop(u8_t TimerIndex){
    Assert_TIMER(TimerIndex, tte_InvalidArgument);
    return ttc_timer_interface_stop(TimerIndex);
}

ttc_timer_errorcode_e ttc_hardware_timer_start(u8_t TimerIndex){
    Assert_TIMER(TimerIndex, tte_InvalidArgument);
    return ttc_timer_interface_start(TimerIndex);
}

void ttc_hardware_timer_set_counter(u8_t TimerIndex, u16_t Value, u8_t TimeScale){
    Assert_TIMER(TimerIndex, tte_InvalidArgument);
    Assert_TIMER(Value, tte_InvalidArgument);
    ttc_timer_interface_set_counter(TimerIndex, Value, TimeScale);
}

u16_t ttc_hardware_timer_get_counter(u8_t TimerIndex){
    Assert_TIMER(TimerIndex, tte_InvalidArgument);
    return ttc_timer_interface_get_counter(TimerIndex);
}

ttc_timer_statuscode_e ttc_hardware_timer_get_state(u8_t TimerIndex){
    Assert_TIMER(TimerIndex, tte_InvalidArgument);
    return ttc_timer_interface_get_state(TimerIndex);
}

//} Function definitions
//{ Private functions *************************************************

//  Software timer operations ********************************************

void _ttc_software_timer_(){
    while(1){        
        if(HeadEvent)
        {
            //if(HeadEvent->TimePeriod <= (ttc_task_get_elapsed_usecs() - HeadEvent->TimeStart))
            if(HeadEvent->TimePeriod <= (ttc_task_get_elapsed_usecs() - HeadEvent->TimeStart))
                _ttc_software_timer_check_next_event_();
            else uSleep(HeadEvent->TimePeriod - (ttc_task_get_elapsed_usecs() - HeadEvent->TimeStart));
        }
        else uSleep(1000);
    }
}

void _ttc_software_timer_check_next_event_(){
    if(HeadEvent) {
        ttc_task_begin_criticalsection(_ttc_software_timer_check_next_event_);
        ttc_time_event_t* CurrentEvent = HeadEvent; //Take the first element of the event list
        ttc_task_end_criticalsection();

        if(HeadEvent->Next){
            ttc_task_begin_criticalsection(_ttc_software_timer_check_next_event_);
            HeadEvent = HeadEvent->Next;
        }
        else {
            ttc_task_begin_criticalsection(_ttc_software_timer_check_next_event_);
            HeadEvent = NULL;
        }
        ttc_task_end_criticalsection();

        CurrentEvent->Function(CurrentEvent->Argument);
        if(CurrentEvent->Continuous)
            _ttc_software_timer_insert_event_(CurrentEvent);
         else ttc_heap_pool_block_free((ttc_memory_from_pool_t*) CurrentEvent);
    }
}

void _ttc_software_timer_insert_event_(ttc_time_event_t* NewEvent){
    // set to TRUE once Event has been found
    bool found = 0;

    if(!HeadEvent){
        ttc_task_begin_criticalsection(_ttc_software_timer_insert_event_);
        HeadEvent = NewEvent;
        ttc_task_end_criticalsection();
    }
    else {
        ttc_task_begin_criticalsection(_ttc_software_timer_insert_event_);
        ttc_time_event_t* CurrentEvent = HeadEvent;
        ttc_task_end_criticalsection();
        Base_t ActualTime = 0;

        if(CurrentEvent->TimePeriod <= (ttc_task_get_elapsed_usecs() - CurrentEvent->TimeStart))
            ActualTime = 0;
        else ActualTime = ((CurrentEvent->TimePeriod - (ttc_task_get_elapsed_usecs() - CurrentEvent->TimeStart)));

        if(NewEvent->TimePeriod >= ActualTime) {
            while(!found) {
                if(CurrentEvent->Next) {
                    ttc_task_begin_criticalsection(_ttc_software_timer_insert_event_);
                    ttc_time_event_t* LastEvent = CurrentEvent;
                    CurrentEvent = CurrentEvent->Next;
                    ttc_task_end_criticalsection();

                    if(CurrentEvent->TimePeriod < (ttc_task_get_elapsed_usecs() - CurrentEvent->TimeStart))
                        ActualTime = 0;
                    else ActualTime = ((CurrentEvent->TimePeriod - (ttc_task_get_elapsed_usecs() - CurrentEvent->TimeStart)));

                    if(NewEvent->TimePeriod <= ActualTime) {
                        ttc_task_begin_criticalsection(_ttc_software_timer_insert_event_);
                        NewEvent->TimeStart = ttc_task_get_elapsed_usecs();
                        NewEvent->Next = CurrentEvent;
                        LastEvent->Next = NewEvent;
                        ttc_task_end_criticalsection();
                        found = 1;
                    }
                }
                else {
                    ttc_task_begin_criticalsection(_ttc_software_timer_insert_event_);
                    NewEvent->TimeStart = ttc_task_get_elapsed_usecs();
                    NewEvent->Next = NULL;
                    CurrentEvent->Next = NewEvent;
                    ttc_task_end_criticalsection();
                    found = 1;
                }
            }
        }
        else{
            ttc_task_begin_criticalsection(_ttc_software_timer_insert_event_);
            NewEvent->TimeStart = ttc_task_get_elapsed_usecs();
            NewEvent->Next = HeadEvent;
            HeadEvent = NewEvent;
            ttc_task_end_criticalsection();
        }
    }
}

// Hardware timer operations ********************************************

void _ttc_hardware_timer_create(ttc_timer_config_t* Config, bool InterruptFlag){
    Assert_TIMER(Config, tte_InvalidArgument);

    if(ttc_timer_interface_check_initialized(Config) == FALSE){
        ttc_timer_interface_set(Config);
        if(InterruptFlag){
            if (tine_OK == ttc_interrupt_init(tit_TIMER_Updating,
                Config->LogicalIndex,
                _ttc_hardware_timer_manage_,  // will be passed as argument to isr_Switch()
                (void *)Config,
                // will be loaded with data of previously registered ISR (if any)
                Config->eti_NextISR,
                Config->eti_NextISR_Argument)
            )
            ttc_interrupt_enable(tit_TIMER_Updating, Config->LogicalIndex, TRUE);
        }
        ttc_hardware_timer_start(Config->LogicalIndex);
    }
    else Assert_TIMER(Config, tte_DeviceNotReady);
}

void _ttc_hardware_timer_manage_(u8_t Index, void* Argument){
    Assert_TIMER(Argument, tte_InvalidArgument);
    (void)Index;
    ttc_timer_config_t* Config = (ttc_timer_config_t*) Argument;
    Config->TimerEvent.Function(Config->TimerEvent.Argument);
}

//} Private functions
