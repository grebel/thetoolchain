/** { ttc_timer.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for timer devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 23 at 20140415 05:28:31 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_timer.h"

#if TTC_TIMER_AMOUNT == 0
#warning No TIMER devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of timer devices.
 *
 */

#warning ttc_timer is deprecated and will be recreated soon!

// for each initialized device, a pointer to its generic and stm32f1xx definitions is stored
A_define( t_ttc_timer_config*, ttc_timer_configs, TTC_TIMER_AMOUNT );

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8 ttc_timer_get_max_index() {
    return TTC_TIMER_AMOUNT;
}
t_ttc_timer_config* ttc_timer_get_configuration( t_u8 LogicalIndex ) {
    Assert_TIMER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_timer_config* Config = A( ttc_timer_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        Config = A( ttc_timer_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_timer_config ) );
        Config->Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_timer_load_defaults( LogicalIndex );
    }

    return Config;
}
e_ttc_timer_errorcode ttc_timer_deinit( t_u8 LogicalIndex ) {
    Assert_TIMER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_timer_config* Config = ttc_timer_get_configuration( LogicalIndex );

    e_ttc_timer_errorcode Result = _driver_timer_deinit( Config );
    if ( Result == ec_timer_OK )
    { Config->Flags.Bits.Initialized = 0; }

    return Result;
}
e_ttc_timer_errorcode ttc_timer_init( t_u8 LogicalIndex ) {
    Assert_TIMER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_timer_config* Config = ttc_timer_get_configuration( LogicalIndex );

    e_ttc_timer_errorcode Result = _driver_timer_init( Config );
    if ( Result == ec_timer_OK )
    { Config->Flags.Bits.Initialized = 1; }

    return Result;
}
e_ttc_timer_errorcode ttc_timer_load_defaults( t_u8 LogicalIndex ) {
    Assert_TIMER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_timer_config* Config = ttc_timer_get_configuration( LogicalIndex );

    if ( Config->Flags.Bits.Initialized )
    { ttc_timer_deinit( LogicalIndex ); }

    ttc_memory_set( Config, 0, sizeof( t_ttc_timer_config ) );

    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    Config->PhysicalIndex = ttc_timer_logical_2_physical_index( LogicalIndex );
    Config->LowLevelConfig = &Config->TIMER_Arch;

    //Insert additional generic default values here ...

    return _driver_timer_load_defaults( Config );
}
t_u8 ttc_timer_logical_2_physical_index( t_u8 LogicalIndex ) {

    switch ( LogicalIndex ) {         // determine base register and other low-level configuration data

            #ifdef EXTENSION_cpu_stm32l1xx

            #ifdef TTC_TIMER1
        case 2: return TTC_TIMER1;
            #endif
            #ifdef TTC_TIMER2
        case 3: return TTC_TIMER2;
            #endif
            #ifdef TTC_TIMER3
        case 5: return TTC_TIMER3;
            #endif
            #ifdef TTC_TIMER4
        case 6: return TTC_TIMER4;
            #endif
            #ifdef TTC_TIMER5
        case 8: return TTC_TIMER5;
            #endif
            #ifdef TTC_TIMER6
        case 9: return TTC_TIMER6;
            #endif
            #ifdef TTC_TIMER7
        case 10: return TTC_TIMER7;
            #endif
            #ifdef TTC_TIMER8
        case 11: return TTC_TIMER8;
            #endif

            #endif

            #ifdef EXTENSION_cpu_stm32f10x


            #ifdef TTC_TIMER1
        case 2: return TTC_TIMER1;
            #endif
            #ifdef TTC_TIMER2
        case 3: return TTC_TIMER2;
            #endif
            #ifdef TTC_TIMER3
        case 4: return TTC_TIMER3;
            #endif
            #ifdef TTC_TIMER4
        case 6: return TTC_TIMER4;
            #endif
            #ifdef TTC_TIMER5
        case 7: return TTC_TIMER5;
            #endif
            #ifdef TTC_TIMER6
        case 9: return TTC_TIMER6;
            #endif
            #ifdef TTC_TIMER7
        case 10: return TTC_TIMER7;
            #endif
            #ifdef TTC_TIMER8
        case 11: return TTC_TIMER8;
            #endif

            #endif
        // extend as required

        default: break;
    }

    Assert_TIMER( 0, ttc_assert_origin_auto ); // No TTC_TIMERn defined! Check your makefile.100_board_* file!
    return -1; // program flow should never go here
}
t_u8 ttc_timer_physical_2_logical_index( t_u8 PhysicalIndex ) {

    switch ( PhysicalIndex ) {         // determine base register and other low-level configuration data
            #ifdef TTC_TIMER2
        case TTC_TIMER2: return 1;
            #endif
            #ifdef TTC_TIMER3
        case TTC_TIMER3: return 2;
            #endif
            #ifdef TTC_TIMER4
        case TTC_TIMER4: return 3;
            #endif
            #ifdef TTC_TIMER5
        case TTC_TIMER5: return 4;
            #endif
            #ifdef TTC_TIMER9
        case TTC_TIMER9: return 5;
            #endif
            #ifdef TTC_TIMER10
        case TTC_TIMER10: return 6;
            #endif
            #ifdef TTC_TIMER11
        case TTC_TIMER11: return 7;
            #endif
            #ifdef TTC_TIMER12
        case TTC_TIMER12: return 8;
            #endif
            #ifdef TTC_TIMER13
        case TTC_TIMER13: return 9;
            #endif
            #ifdef TTC_TIMER14
        case TTC_TIMER14: return 10;
            #endif
        // extend as required

        default: break;
    }

    Assert_TIMER( 0, ttc_assert_origin_auto ); // No TTC_TIMERn defined! Check your makefile.100_board_* file!
    return -1; // program flow should never go here
}
void ttc_timer_prepare() {
    // add your startup code here (Singletasking!)
    _driver_timer_prepare();
}
void ttc_timer_reset( t_u8 LogicalIndex ) {
    Assert_TIMER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_timer_config* Config = ttc_timer_get_configuration( LogicalIndex );

    _driver_timer_reset( Config );
}
e_ttc_timer_errorcode ttc_timer_set( t_u8 LogicalIndex, void TaskFunction( void* ), void* Argument, t_u32 Period ) {
    Assert_TIMER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_timer_config* Config = ttc_timer_get_configuration( LogicalIndex );

    Config->Function = TaskFunction;
    Config->Argument = Argument;
    Config->TimePeriod = Period;

    return _driver_timer_set( Config );
}
void ttc_timer_change_period( t_u8 LogicalIndex, t_u32 Period ) {
    Assert_TIMER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_timer_config* Config = ttc_timer_get_configuration( LogicalIndex );

    _driver_timer_change_period( Config, Period );
}
void ttc_timer_reload( t_u8 LogicalIndex ) {
    Assert_TIMER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_timer_config* Config = ttc_timer_get_configuration( LogicalIndex );

    _driver_timer_reload( Config );
}
t_u32 ttc_timer_read_value( t_u8 LogicalIndex ) {
    Assert_TIMER( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_timer_config* Config = ttc_timer_get_configuration( LogicalIndex );

    return ttc_driver_timer_read_value( Config );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_timer(t_u8 LogicalIndex) {  }

void _ttc_timer_isr( t_ttc_timer_config* Config ) {
    Assert_TIMER( Config != NULL, ttc_assert_origin_auto );

    if ( Config->Function )
    { Config->Function( Config->Argument ); }
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
