/*{ cm3_basic.h ***********************************************
 *
 * TheToolChain.com
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Basic datatypes, enums and functions
 *
}*/

#include "cm3_basic.h"

extern void Assert_Halt_NULL();

inline u32_t cm3_atomic_read_write_32(volatile u32_t* Address, u32_t NewValue) {
    Assert_CM3(Address, ec_InvalidArgument); // invalid argument
    Assert_CM3( ( ((u32_t) Address) & 0x3 ) == 0, ec_InvalidImplementation); // Address not 4-byte aligned!

    u32_t OldValue;
    do {
        OldValue = __LDREXW( (u32_t*) Address);
    } while ( __STREXW( NewValue, (u32_t*) Address) != 0 );

    return OldValue;
}
inline u16_t cm3_atomic_read_write_16(volatile u16_t* Address, u16_t NewValue) {
    Assert_CM3(Address, ec_InvalidArgument); // invalid argument
    Assert_CM3( ( ((u32_t) Address) & 0x3 ) == 0, ec_InvalidImplementation); // Address not 4-byte aligned!

    u16_t OldValue;
    do {
        OldValue = __LDREXH( (u16_t*) Address);
    } while ( __STREXH( NewValue, (u16_t*) Address) != 0 );

    return OldValue;
}
inline u8_t cm3_atomic_read_write_8(volatile u8_t* Address, u8_t NewValue) {
    Assert_CM3(Address, ec_InvalidArgument); // invalid argument
    Assert_CM3( ( ((u32_t) Address) & 0x3 ) == 0, ec_InvalidImplementation); // Address not 4-byte aligned!

    u8_t OldValue;
    do {
        OldValue = __LDREXB( (u8_t*) Address);
    } while ( __STREXB( NewValue, (u8_t*) Address) != 0 );

    return OldValue;
}
inline u32_t cm3_atomic_add_32(volatile u32_t* Address, s32_t Amount) {
    Assert_CM3(Address, ec_InvalidArgument); // invalid argument
    Assert_CM3( ( ((u32_t) Address) & 0x3 ) == 0, ec_InvalidImplementation); // Address not 4-byte aligned!

    u32_t Value;
    do {
        Value = __LDREXW( (u32_t*) Address);
        Value += Amount;
    } while(__STREXW(Value, (u32_t*) Address) == 1);

    return Value;
}
inline u16_t cm3_atomic_add_16(volatile u16_t* Address, s16_t Amount) {
    Assert_CM3(Address, ec_InvalidArgument); // invalid argument
    Assert_CM3( ( ((u32_t) Address) & 0x3 ) == 0, ec_InvalidImplementation); // Address not 4-byte aligned!

    u16_t Value;
    do {
        Value = __LDREXH( (u16_t*) Address);
        Value += Amount;
    } while(__STREXH(Value, (u16_t*) Address) == 1);

    return Value;
}
inline u8_t cm3_atomic_add_8(volatile u8_t* Address, s8_t Amount) {
    Assert_CM3(Address, ec_InvalidArgument); // invalid argument
    Assert_CM3( ( ((u32_t) Address) & 0x3 ) == 0, ec_InvalidImplementation); // Address not 4-byte aligned!

    u8_t Value;
    do {
        Value = __LDREXB( (u8_t*) Address);
        Value += Amount;
    } while(__STREXB(Value, (u8_t*) Address) == 1);

    return Value;
}
