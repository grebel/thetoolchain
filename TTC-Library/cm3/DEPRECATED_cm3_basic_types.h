/*{ cm3_basic_types.h ***********************************************
 *
 * TheToolChain.com
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Basic datatypes for architecture CortexM3
 *
}*/

#ifndef CM3_BASIC_TYPES_H
#define CM3_BASIC_TYPES_H

typedef unsigned char        u8_t;
typedef unsigned short int  u16_t;
typedef unsigned long       u32_t;
typedef signed char          s8_t;
typedef signed short int    s16_t;
typedef signed int          s32_t;
#ifdef FALSE
#undef FALSE
#endif
#ifdef TRUE
#undef TRUE
#endif
typedef enum { FALSE=0, TRUE } BOOL;
typedef BOOL bool;

#define  Base_t        u32_t  // basic (native) datatype of STM32 architecture
#define  Base_signed_t s32_t  // signed basic (native) datatype

#endif
