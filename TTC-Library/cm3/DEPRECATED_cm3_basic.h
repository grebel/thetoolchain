/*{ cm3_basic.h ***********************************************
 *
 * TheToolChain.com
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Basic datatypes, enums and functions
 *
}*/

#ifndef CM3_BASIC_H
#define CM3_BASIC_H

#include "cm3_basic_types.h"
#include "../register/register_cortexm3.h"

#define _driver_atomic_read_write_32(Address, NewValue) cm3_atomic_read_write_32(Address, NewValue)
#define _driver_atomic_read_write_16(Address, NewValue) cm3_atomic_read_write_16(Address, NewValue)
#define _driver_atomic_read_write_8(Address, NewValue)  cm3_atomic_read_write_8(Address, NewValue)
#define _driver_atomic_add_32(Address, NewValue)        cm3_atomic_add_32(Address, NewValue)
#define _driver_atomic_add_16(Address, NewValue)        cm3_atomic_add_16(Address, NewValue)
#define _driver_atomic_add_8(Address, NewValue)         cm3_atomic_add_8(Address, NewValue)

#include "../compile_options.h"

#ifdef EXTENSION_200_cpu_stm32f0xx
  #include "stm32f0xx.h"
#endif
#ifdef EXTENSION_200_cpu_stm32f10x
  #include "stm32f10x.h"
#endif
#ifdef EXTENSION_200_cpu_stm32f2xx
  #include "stm32f2xx.h"
#endif
#ifdef EXTENSION_200_cpu_stm32l1xx
  #include "stm32l1xx.h"
#endif
#ifdef EXTENSION_200_cpu_stm32w1xx
#  ifdef EXTENSION_255_stm32w1xx_standard_peripherals
#    include "stm32w108xx.h"
#  endif
#endif

/* driver definitions must occure BEFORE including register_cortexm3.h */
#include "../register/register_cortexm3.h"

/* Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_HEAP 0  # disable assert handling for heap devices
 *
 */
#ifndef TTC_ASSERT_CM3    // any previous definition set (Makefile)?
#define TTC_ASSERT_CM3 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_CM3 == 1)  // use Assert()s in CM3 code (somewhat slower but alot easier to debug)
  #define Assert_CM3(Condition, ErrorCode) if (! (Condition) ) Assert_Halt_EC(ErrorCode)
#else  // us no Assert()s in CM3 code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_CM3(Condition, ErrorCode)
#endif

/* NOTE!! IF THIS VALUE IS CHANGED, NVIC-CONFIG.H MUST ALSO BE UPDATED */
#define INTERRUPTS_DISABLED_PRIORITY (12 << 3)

/** Globally disables all interrupts
 *
 * Disabling all interrupts is very fast (one assembly instruction) and will not change configurations of individual interrupts.
 *
 */
void basic_cm3_interrupt_all_disable();
//? #define basic_cm3_interrupt_all_disable() __ASM volatile ("cpsid i")
#define basic_cm3_interrupt_all_disable() do { __set_BASEPRI(INTERRUPTS_DISABLED_PRIORITY); } while(0)
/** Globally enables all interrupts
 *
 * Enabling all interrupts is very fast (one assembly instruction) and will not change configurations of individual interrupts.
 *
 */
void basic_cm3_interrupt_all_enable();
//? #define basic_cm3_interrupt_all_enable() __ASM volatile ("cpsie i")
#define basic_cm3_interrupt_all_enable() do { __set_BASEPRI(0); } while(0)

/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
u32_t cm3_atomic_read_write_32(volatile u32_t* Address, u32_t NewValue);
u16_t cm3_atomic_read_write_16(volatile u16_t* Address, u16_t NewValue);
u8_t cm3_atomic_read_write_8(volatile u8_t* Address, u8_t NewValue);

/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * @param Address  memory location to read and write
  * @param Value    *Address += Value
  * @return         current value of *Address
  */
u32_t cm3_atomic_add_32(volatile u32_t* Address, s32_t Amount);
u16_t cm3_atomic_add_16(volatile u16_t* Address, s16_t Amount);
 u8_t cm3_atomic_add_8(volatile u8_t* Address, s8_t Amount);

/** Calculate bit-banding address that corresponds to given bit in given 32-bit word
  *
  * @param   PeripheralWord   address of 32-bit word in which desired bit is located
  * @param   BitNumber        0..31
  * @return  address in peripheral bit-banding region that corresponds to given word and bit-number
  */
// volatile u32_t* cm3_calc_peripheral_BitBandAddress(void* PeripheralWord, u8_t BitNumber);
#define cm3_calc_peripheral_BitBandAddress(PeripheralWord, BitNumber) (u32_t*) (0x42000000 + 32 * (((u32_t) PeripheralWord)  - 0x40000000) + BitNumber * 4)

 /** Calculate peripheral word that corresponds to given bit-banding address
   *
   * This function is a reverse of cm3_calc_peripheral_BitBandAddress()
   *
   * @param   BitBandAddress address in peripheral bit-banding region that corresponds to given word and bit-number
   * @return  address of peripheral 32-bit word
   */
// volatile u32_t* cm3_calc_peripheral_Word(void* BitBandAddress);
#define cm3_calc_peripheral_Word(BitBandAddress) (    (   (  ( ((u32_t) BitBandAddress) & 0xffffff80 ) - 0x42000000  ) / 32   ) + 0x40000000    )


 /** Calculate bit number that corresponds to given bit-banding address
   *
   * This function is a reverse of cm3_calc_peripheral_BitBandAddress()
   *
   * @param   BitBandAddress address in peripheral bit-banding region that corresponds to given word and bit-number
   * @return  BitNumber     0..31
   */
// volatile u32_t* cm3_calc_peripheral_Word(void* BitBandAddress);
#define cm3_calc_peripheral_BitNumber(BitBandAddress) ( ((u32_t) BitBandAddress) & 0x7f ) / 4;


 /** Handler for Hardfault interrupt that tries to give you a clue what has happened
   *
   * Note: Will call Assert_Halt_EC()
   *       Place a breakpoint inside Assert_Halt_EC() to get informed when this handler is executed!
   *
   */
 void interrupt_cortexm3_HardFault_Handler(unsigned int * hardfault_args);

#endif
