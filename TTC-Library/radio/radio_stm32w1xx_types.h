#ifndef RADIO_STM32W1XX_TYPES_H
#define RADIO_STM32W1XX_TYPES_H

/** { radio_stm32w1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for RADIO devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by ttc_radio_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140514 04:32:30 UTC
 *
 *  Note: See ttc_radio.h for description of architecture independent RADIO implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_RADIO1   // device not defined in makefile
    #define TTC_RADIO1    ta_radio_stm32w1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_radio_types.h *************************

typedef struct {  // stm32w1xx specific configuration data of single RADIO device
    t_base Amount_Recalibrations; // amount of radio recalibrations done since initialisation
} __attribute__( ( __packed__ ) ) t_radio_stm32w1xx_config;

// t_ttc_radio_architecture is required by ttc_radio_types.h
#define t_ttc_radio_architecture t_radio_stm32w1xx_config

// radio is built in
#define TTC_INTERRUPT_RADIO_AMOUNT 1

//} Structures/ Enums


#endif //RADIO_STM32W1XX_TYPES_H
