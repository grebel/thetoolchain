/** { radio_common.c ************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.c revision 11 at 20150928 08:58:42 UTC
 *
 *  Authors: gregor
}*/

/** { Includes ********************************************************
 *
 * Put all includes here that are required to compile this source code.
 */

#include "radio_common.h"
#include "../ttc_radio_types.h"                 // this source may use all datatypes of ttc_radio
#include "../interfaces/ttc_radio_interface.h"  // this source may call all interface functions of ttc_radio
#include "../ttc_math_types.h"                  // 3D coordinates (ranging only)
#include "../ttc_packet.h"                      // generic network packets conforming to IEEE 802.15.4a (2011)
#include "../ttc_heap.h"                        // radio packets are stored in memory pools being managed here
#include "../ttc_list_types.h"                  // operations on single linked lists
#include "../ttc_semaphore.h"                   // read semaphore values
#include "../ttc_task.h"                        // single- and multitasking support
#include "../ttc_systick.h"                     // precise, hardware assisted timer delays

//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Global Variables ************************************************
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

/** Flag indicating if current function has been called from an interrupt service routine
 *
 * This flag must be set at begin and cleared at end of interrupt service routine of low-level driver!
 * Some functions are called from isr and application and require this information to work properly.
 *
 * !=0: interrupt service routine is running (only seen in functions being called from interrupt service routine)
 */
BOOL volatile radio_common_InsideISR = FALSE;

t_ttc_semaphore_smart radio_common_RangingNo __attribute__( ( aligned( 4 ) ) ); // each new ranging request gets a new number

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** { Private Function Declarations ***********************************
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

/** decides if given list item shall be ignored or not
 *
 * @param Argument      (t_radio_common_socket_iterator*) socket identifier to ignore
 * @param PreviousItem  ==NULL: item is first in list; !=NULL: pointer to previous list item
 * @param Item          pointer to current list item
 * @return ==TRUE: item shall be removed from list; ==FALSE: keep item in list
 */
void* _radio_common_iterator_packet_ignore( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item );

/** decides if given list item shall be ignored or not
 *
 * @param Argument      (t_radio_common_socket_iterator*) socket identifier to ignore
 * @param PreviousItem  ==NULL: item is first in list; !=NULL: pointer to previous list item
 * @param Item          pointer to current list item
 * @return ==TRUE: item shall be removed from list; ==FALSE: keep item in list
 */
void* _radio_common_iterator_packet_remove( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item );

/** stores single item in given socket job
 *
 * Note: This helper function is only intended to be called from a ttc_list_iterate*() function!
 *
 * @param Argument      (t_radio_common_socket_iterator*) pointer to a socket job
 * @param Item          pointer to list item to be stored
 */
void _radio_common_item_store( void* Argument, t_ttc_list_item* Item );

/** releases memory buffer of packet stored in single list item
 *
 * Note: This helper function is only intended to be called from a ttc_list_iterate*() function!
 *
 * @param Argument      (t_radio_common_socket_iterator*) pointer to a socket job
 * @param Item          pointer to list item to be released back to its memory pool
 */
void _radio_common_item_release( void* Argument, t_ttc_list_item* Item );

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** { Function Definitions ********************************************
 */

t_ttc_packet*          radio_common_packet_get_empty( t_ttc_radio_config* Config, e_ttc_packet_type Type, e_ttc_packet_pattern SocketID ) {
    t_ttc_heap_block_from_pool* MemoryBlock = ttc_heap_pool_block_get( &( Config->Pool_Packets ) );
    Assert_RADIO_Writable( MemoryBlock, ttc_assert_origin_auto );  // went out of dynamic memory!

#if TTC_HEAP_POOL_STATISTICS == 1
    // store address of function caller
    ( MemoryBlock - 1 )->MyOwner = __builtin_return_address( 0 );
#endif

    t_ttc_packet* EmptyPacket = ( t_ttc_packet* ) MemoryBlock;
    Assert_RADIO( 0 == ( ( ( t_base ) & ( EmptyPacket->MAC ) ) & 0x3 ), ttc_assert_origin_auto );  // adress must be 4 byte aligned

    // initialize packet to given type
    ttc_packet_initialize( MemoryBlock, Type, SocketID );

    return EmptyPacket;
}
t_ttc_packet*          radio_common_packet_get_empty_try( t_ttc_radio_config* Config, e_ttc_packet_type Type, e_ttc_packet_pattern SocketID ) {
    t_ttc_heap_block_from_pool* MemoryBlock = ttc_heap_pool_block_get_try( &( Config->Pool_Packets ) );

    if ( MemoryBlock ) {
#if TTC_HEAP_POOL_STATISTICS == 1
        // store address of function call in block header
        ( MemoryBlock - 1 )->MyOwner = __builtin_return_address( 0 );
        Config->Pool_Packets.LastGet = __builtin_return_address( 0 );
#endif

        t_ttc_packet* EmptyPacket = ( t_ttc_packet* ) MemoryBlock;

        Assert_RADIO_Writable( MemoryBlock, ttc_assert_origin_auto );  // went out of dynamic memory!
        Assert_RADIO( 0 == ( ( ( t_base ) & ( EmptyPacket->MAC ) ) & 0x3 ), ttc_assert_origin_auto );  // adress must be 4 byte aligned

        // initialize packet to given type
        ttc_packet_initialize( MemoryBlock, Type, SocketID );
        return EmptyPacket;
    }
    else {
        Assert_RADIO_EXTRA( Config->List_PacketsRx.AmountItems.Semaphore.Value < Config->Init.MaxAmountPackets, ttc_assert_origin_auto ); // All available packets are still in receive list. Do you call ttc_radio_packet_received_tryget() for all registerd socket identifiers regularly? Hint: You might call ttc_radio_packets_received_ignore(,,0) to return all packets in ListRX to their memory pool (last resort). Otherwise check registerd socket identifier of each packet in ListRX to see which types of packets are not processed!
    }
    return NULL;
}
t_ttc_packet*          radio_common_packet_get_empty_isr( t_ttc_radio_config* Config, e_ttc_packet_type Type, e_ttc_packet_pattern SocketID ) {
    t_ttc_heap_block_from_pool* MemoryBlock = ttc_heap_pool_block_get_isr( &( Config->Pool_Packets ) );
    if ( !MemoryBlock )
    { return NULL; }

#if TTC_HEAP_POOL_STATISTICS == 1
    // store address of function call in block header
    ( MemoryBlock - 1 )->MyOwner = __builtin_return_address( 0 );
    Config->Pool_Packets.LastGet = __builtin_return_address( 0 );
#endif

    t_ttc_packet* EmptyPacket = ( t_ttc_packet* ) MemoryBlock;
    Assert_RADIO_EXTRA( 0 == ( ( ( t_base ) & ( EmptyPacket->MAC ) ) & 0x3 ), ttc_assert_origin_auto );  // adress must be 4 byte aligned

    // initialize packet to given type
    ttc_packet_initialize( MemoryBlock, Type, SocketID );

    return EmptyPacket;
}
t_ttc_packet*          radio_common_pop_list_rx_try( t_ttc_radio_config* Config ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // must point to writable memory!

    if ( ! s_ttc_listize( & Config->List_PacketsRx ) ) // fast check
    { return NULL; } // list is empty

    t_ttc_list_item* ListItem = ttc_list_pop_front_single_try( & Config->List_PacketsRx );
    if ( ListItem ) { // successfully popped an item from list

        // we know that this item came from a memory pool
        t_ttc_heap_block_from_pool* MemoryBlock = ttc_heap_pool_from_list_item( ListItem );

#if TTC_HEAP_POOL_STATISTICS == 1
        // set owner info to aid debugging
        ( MemoryBlock - 1 )->MyOwner = __builtin_return_address( 0 );
#endif

        // memory blocks from pool may be casted to anything
        t_ttc_packet* Packet = ( t_ttc_packet* ) MemoryBlock;

#if (TTC_RADIO_STATISTICS == 1)
        RADIO_COMMON_COUNT( Amount_ListRX_Pop );

#endif

        return Packet;
    }

    return NULL; // list is empty
}
t_ttc_packet*          radio_common_pop_list_tx( t_ttc_radio_config* Config ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // must point to writable memory!

    if ( ! s_ttc_listize( & Config->List_PacketsTx ) ) // fast check
    { return NULL; } // list is empty

    t_ttc_list_item* ListItem = ttc_list_pop_front_single_try( & Config->List_PacketsTx );
    if ( ListItem ) { // successfully popped an item from list

        // we know that this item came from a memory pool
        t_ttc_heap_block_from_pool* MemoryBlock = ttc_heap_pool_from_list_item( ListItem );

        // memory blocks from pool may be casted to anything
        t_ttc_packet* Packet = ( t_ttc_packet* ) MemoryBlock;

#if (TTC_RADIO_STATISTICS == 1)
        RADIO_COMMON_COUNT( Amount_ListTX_Pop );
#endif

        return Packet;
    }

    return NULL; // list is empty
}
t_ttc_packet*          radio_common_pop_list_tx_isr( t_ttc_radio_config* Config ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // must point to writable memory!

    if ( ! s_ttc_listize( & Config->List_PacketsTx ) ) // fast check
    { return NULL; } // list is empty

    t_ttc_list_item* ListItem = ttc_list_pop_front_single_isr( & Config->List_PacketsTx );
    if ( ListItem && ( ListItem != ( t_ttc_list_item* ) 0x1 ) ) { // successfully popped an item from list

        // we know that this item came from a memory pool
        t_ttc_heap_block_from_pool* MemoryBlock = ttc_heap_pool_from_list_item( ListItem );

        // memory blocks from pool may be casted to anything
        t_ttc_packet* Packet = ( t_ttc_packet* ) MemoryBlock;

#if (TTC_RADIO_STATISTICS == 1)
        RADIO_COMMON_COUNT( Amount_ListTX_PopISR );
#endif

        return Packet;
    }

    return NULL; // list is empty
}
void                   radio_common_push_list_rx( t_ttc_radio_config* Config, t_ttc_packet* Packet ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // must point to writable memory!
    Assert_RADIO_Writable( Packet, ttc_assert_origin_auto );  // must point to writable memory!

    // we know that this packet came from a memory pool
    t_ttc_heap_block_from_pool* MemoryBlock = ( t_ttc_heap_block_from_pool* ) Packet;

#if TTC_HEAP_POOL_STATISTICS == 1
    // set owner info to aid debugging
    ( MemoryBlock - 1 )->MyOwner = ( void ( * )() ) __builtin_return_address( 0 );
#endif

    // every memory block from pool is also a list item
    t_ttc_list_item* ListItem = ttc_heap_pool_to_list_item( MemoryBlock );

    ttc_list_push_back_single( & Config->List_PacketsRx, ListItem );

#if (TTC_RADIO_STATISTICS == 1)
    RADIO_COMMON_COUNT( Amount_ListRX_Push );
#endif
}
void                   radio_common_push_list_rx_isr( t_ttc_radio_config* Config, t_ttc_packet* PacketRX ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // must point to writable memory!
    Assert_RADIO_EXTRA_Writable( PacketRX, ttc_assert_origin_auto );  // must point to writable memory!

#if TTC_PACKET_RANGING == 1
    if ( Config->Init.Flags.EnableRangingReplies ) { // ranging is available: check if we just received a ranging request
        if ( PacketRX->Meta.RangingMessage ) {       // seems to be a ranging request: better check if we need to reply immediately
            if ( radio_common_ranging_reply_isr( Config, PacketRX ) ) { // it was a ranging request: we will not pass it to the application
                return; // packet has already been released to its memory pool
            }
        }
    }
#endif

    if ( ! ttc_driver_radio_configure_auto_acknowledge( Config, 0, 0 ) ) { // auto acknowledgement disabled in transceiver: handle it in software (software auto acknowledge)
        if ( ttc_packet_acknowledge_request_get( PacketRX ) ) { // sender is requesting an acknowledge: queue it for transmission
            t_ttc_packet* PacketACK = radio_common_packet_get_empty_isr( Config, 0, 0 );
            if ( PacketACK ) {
                ttc_packet_acknowledge( PacketRX, PacketACK );

                // queue acknowledge packet for transmission
                radio_common_prepend_list_tx_isr( Config, PacketACK );
            }
        }
    }

    if ( 1 ) { // append received packet to List_PacketsRx
        // we know that this packet came from a memory pool
        t_ttc_heap_block_from_pool* MemoryBlock = ( t_ttc_heap_block_from_pool* ) PacketRX;

#if TTC_HEAP_POOL_STATISTICS == 1
        // set owner info to aid debugging
        ( MemoryBlock - 1 )->MyOwner = ( void ( * )() ) __builtin_return_address( 0 );
#endif

        // every memory block from pool is also a list item
        t_ttc_list_item* ListItem = ttc_heap_pool_to_list_item( MemoryBlock );

        ttc_list_push_back_single_isr( & Config->List_PacketsRx, ListItem );

#if (TTC_RADIO_STATISTICS == 1)
        RADIO_COMMON_COUNT( Amount_ListRX_PushISR );
#endif
    }
}
void                   radio_common_push_list_tx( t_ttc_radio_config* Config, t_ttc_packet* Packet ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // must point to writable memory!
    Assert_RADIO_EXTRA_Writable( Packet, ttc_assert_origin_auto );  // must point to writable memory!

    // we know that packet came from a memory pool
    t_ttc_heap_block_from_pool* MemoryBlock = ( t_ttc_heap_block_from_pool* ) Packet;

    // every memory block from pool is also a list-item
    t_ttc_list_item* ListItem = ttc_heap_pool_to_list_item( MemoryBlock );

    // queue packet for transmission
    ttc_list_push_back_single( & Config->List_PacketsTx, ListItem );

#if (TTC_RADIO_STATISTICS == 1)
    RADIO_COMMON_COUNT( Amount_ListTX_Push );
#endif
}
void                   radio_common_push_list_tx_isr( t_ttc_radio_config* Config, t_ttc_packet* Packet ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // must point to writable memory!
    Assert_RADIO_EXTRA_Writable( Packet, ttc_assert_origin_auto );  // must point to writable memory!

    // we know that this packet came from a memory pool
    t_ttc_heap_block_from_pool* MemoryBlock = ( t_ttc_heap_block_from_pool* ) Packet;

    // every memory block from pool is also a list item
    t_ttc_list_item* ListItem = ttc_heap_pool_to_list_item( MemoryBlock );

    ttc_list_push_back_single_isr( & Config->List_PacketsTx, ListItem );

#if (TTC_RADIO_STATISTICS == 1)
    RADIO_COMMON_COUNT( Amount_ListTX_PushISR );
#endif

}
void                   radio_common_prepend_list_tx( t_ttc_radio_config* Config, t_ttc_packet* Packet ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // must point to writable memory!
    Assert_RADIO_EXTRA_Writable( Packet, ttc_assert_origin_auto );  // must point to writable memory!

    // we know that packet came from a memory pool
    t_ttc_heap_block_from_pool* MemoryBlock = ( t_ttc_heap_block_from_pool* ) Packet;

    // every memory block from pool is also a list-item
    t_ttc_list_item* ListItem = ttc_heap_pool_to_list_item( MemoryBlock );

    // queue packet for transmission
    ttc_list_push_front_single( & Config->List_PacketsTx, ListItem );

#if (TTC_RADIO_STATISTICS == 1)
    RADIO_COMMON_COUNT( Amount_ListTX_Prepend );
#endif
}
void                   radio_common_prepend_list_tx_isr( t_ttc_radio_config* Config, t_ttc_packet* Packet ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // must point to writable memory!
    Assert_RADIO_EXTRA_Writable( Packet, ttc_assert_origin_auto );  // must point to writable memory!


    // we know that this packet came from a memory pool
    t_ttc_heap_block_from_pool* MemoryBlock = ( t_ttc_heap_block_from_pool* ) Packet;

    // every memory block from pool is also a list item
    t_ttc_list_item* ListItem = ttc_heap_pool_to_list_item( MemoryBlock );

    e_ttc_list_error  Error = ttc_list_push_front_single_isr( & Config->List_PacketsTx, ListItem );
    if ( Error ) {
        RADIO_COMMON_COUNT( Amount_DroppedTx ); // cannot push to list!

        // return memory block to its pool
        Packet = radio_common_packet_release( Packet );
    }
#if (TTC_RADIO_STATISTICS == 1)
    else {
        RADIO_COMMON_COUNT( Amount_ListTX_PrependISR );
    }
#endif

}
const t_ttc_packet*    radio_common_packet_received_peek( t_ttc_radio_config* Config ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );

    TODO( "use same statemachine data SocketJob as radio_common_packet_received_tryget()!" )
    t_ttc_list_item* Item = ttc_list_peek_front( &( Config->List_PacketsRx ) );
    if ( !Item )
    { return NULL; }

    // convert from list item back to block from memory pool
    t_ttc_heap_block_from_pool* BlockRX = ttc_heap_pool_from_list_item( Item );

    // convert memory pool block back to radio packet
    t_ttc_packet* PacketRX = ( t_ttc_packet* ) BlockRX;

    // set PacketRX->Meta.Type according to identified packet type
    if ( ! ttc_packet_identify( PacketRX ) ) { // packet could not be identified: remove from RX-list and skip it

        // let ttc_radio_packet_received_tryget() handle packet removal (avoids double implementation)
        t_ttc_packet* Packet2Remove = radio_common_packet_received_tryget( Config, NULL );
        Assert_RADIO( Packet2Remove == PacketRX, ttc_assert_origin_auto ); // someone stole the peeked packet from list before we were able to pop it. What shall we do with this new packet now?
        if ( PacketRX ) {
            PacketRX = radio_common_packet_release( Packet2Remove );
            RADIO_COMMON_COUNT( Amount_UnidentifiedRx );
        }
    }

    return PacketRX;
}
t_ttc_packet*          radio_common_packet_received_tryget( t_ttc_radio_config* Config, t_ttc_radio_job_socket* SocketJob ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );

    t_ttc_packet* PacketRX = NULL;

    if ( SocketJob ) { // look for received packet of given SocketID
        Assert_RADIO( SocketJob->Init.Pattern != 0, ttc_assert_origin_auto ); // if you don't want to receive packets of specific SocketID, just give SocketJob=NULL. Did you forget to set a value for SocketJob->Init.Pattern?

        if ( ! SocketJob->Init.Initialized ) { // new search job: initialize search job for list iterator

            SocketJob->Private.SearchJob.Input.Patterns_Amount   = 1;
            SocketJob->Private.SearchJob.Input.Flags.OnlyFirst   = 1;
            SocketJob->Private.SearchJob.Input.Flags.RemoveFirst = 1;
            SocketJob->Private.SearchJob.Input.Patterns[0]       = SocketJob->Init.Pattern;
            SocketJob->Init.Initialized                          = TRUE;
        }
        SocketJob->Private.SearchJob.Return.Amount = 0;

        // Runs through all items in List_PacketsRx and calls _radio_common_iterator_packet_remove() for each of them
        // Will remove first found item from list and store its reference in SocketJob->Private.SearchJob.Return.PacketFound_First.
        ttc_list_iterate_from( &( Config->List_PacketsRx ),
                               _radio_common_iterator_packet_remove,
                               &( SocketJob->Private.SearchJob ),
                               1,
                               Config->List_PacketsRx.First,
                               TTC_LIST_TERMINATOR,
                               -1
                             );

        if ( SocketJob->Private.SearchJob.Return.Amount ) { // we found a matching packet: store it
            Assert_RADIO( SocketJob->Private.SearchJob.Return.Amount == 1, ttc_assert_origin_auto ); // we only expect 0..1 items. Check iterator implementation!
            t_ttc_list_item* Item = SocketJob->Private.SearchJob.Return.PacketFound_First;
            Assert_RADIO_Writable( Item, ttc_assert_origin_auto ); // invalid return value. Check implementation of iterator function and everything else (memory leak?)

            // we know that this item is stored in a block from a pool
            t_ttc_heap_block_from_pool* Buffer = ttc_heap_pool_from_list_item( Item );

            // memory blocks from pool may be casted to anything
            PacketRX = ( t_ttc_packet* ) Buffer;

            //X // Job will have to be reinitialized next time
            //X SocketJob->Init.Initialized = FALSE;
        }
    }
    else {             // look for any type of packet
        PacketRX = radio_common_pop_list_rx_try( Config );
    }
    if ( PacketRX ) {  // set PacketRX->Meta.Type according to identified packet type
        if ( ! ttc_packet_identify( PacketRX ) ) { // packet could not be identified: skip it
            RADIO_COMMON_COUNT( Amount_DroppedRx );
            radio_common_packet_release( PacketRX );
            PacketRX = NULL;
        }
    }

#if TTC_HEAP_POOL_STATISTICS == 1
    if ( PacketRX ) {  // set owner info to aid debugging
        ( ( ( t_ttc_heap_block_from_pool* ) PacketRX ) - 1 )->MyOwner = ( void ( * )() ) __builtin_return_address( 0 );
    }
#endif

    return PacketRX;
}
t_ttc_packet*          radio_common_packet_received_waitfor( t_ttc_radio_config* Config, e_ttc_packet_pattern SocketID ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );

    t_ttc_packet* PacketRX = NULL;
    if ( SocketID ) { // look for new received packets that match to given SocketID type or pattern
        t_ttc_radio_job_socket SocketJob;
        SocketJob.Init.Initialized = 0;
        SocketJob.Init.Pattern    = SocketID;

        PacketRX = radio_common_packet_received_tryget( Config, &SocketJob );

        if ( !PacketRX ) { // no packet matching given socket job found: wait for new packets
            t_ttc_list_item* Last = Config->List_PacketsRx.Last;
            while ( !PacketRX ) {
                while ( Config->List_PacketsRx.Last == Last ) { // wait for new items to be pushed to list (simple check)
                    ttc_task_yield();
                }
                PacketRX = radio_common_packet_received_tryget( Config, &SocketJob );
                if ( !PacketRX ) {
                    Last = Config->List_PacketsRx.Last;
                    ttc_task_yield();
                }
            }
        }
    }
    else {            // looking for any type of packet is much faster
        // use list to sleep until packet is added (will not update Config->Statistics)
        t_ttc_list_item* Item = ttc_list_pop_front_single_wait( &( Config->List_PacketsRx ) );
        if ( !Item )
        { return NULL; }

        // convert from list item back to block from memory pool
        t_ttc_heap_block_from_pool* BlockRX = ttc_heap_pool_from_list_item( Item );

        // convert memory pool block back to radio packet
        PacketRX = ( t_ttc_packet* ) BlockRX;
    }

    if ( PacketRX ) { // packet found: set PacketRX->Meta.Type according to identified packet type
        ttc_packet_identify( PacketRX );
    }

    return PacketRX;
}
t_u16                  radio_common_packets_received_ignore( t_ttc_radio_config* Config, e_ttc_packet_pattern SocketID ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );
    t_u16 AmountReleased = 0;

#if TTC_PACKET_SOCKET_BYTE!=1
    SocketID = 0; // t_ttc_packet does not store socket identifier: will ignore all packet types
#endif

    if ( SocketID ) { // ignore packets of given socket identifier

        ttc_list_iterate( &( Config->List_PacketsRx ),           // filter all items in this list
                          _radio_common_iterator_packet_ignore,  // iterator function to call for every list item
                          ( void* ) SocketID,                    // first argument to iterator function
                          1, 0, -1                               // iterate all items first to last
                        );
    }
    else {            // ignore packets of all socket identifiers
        t_ttc_packet* PacketRX = radio_common_pop_list_rx_try( Config );
        while ( PacketRX ) {
            AmountReleased++;
            radio_common_packet_release( PacketRX );
            PacketRX = radio_common_pop_list_rx_try( Config );
        }
    }

    return AmountReleased;
}
void*                  radio_common_packet_release( t_ttc_packet* Packet ) {
    Assert_RADIO_EXTRA_Writable( Packet, ttc_assert_origin_auto );  // must point to writable memory!
    Assert_RADIO_EXTRA( !Packet->Meta.DoNotReleaseBuffer, ttc_assert_origin_auto );  // someone has marked this buffer as not releasable. Reset this flag or rethink if you should release this packet!

    // we know that packet came from a memory pool
    t_ttc_heap_block_from_pool* MemoryBlock = ( t_ttc_heap_block_from_pool* ) Packet;

    if ( radio_common_InsideISR ) // must use special function during isr!
    { ttc_heap_pool_block_free_isr( MemoryBlock ); }
    else
    { ttc_heap_pool_block_free( MemoryBlock ); }

    return NULL;
}
void                   radio_common_prepare() {
    ttc_semaphore_init( &radio_common_RangingNo );
}
void                   radio_common_set_ranging_delay( t_ttc_radio_config* Config, t_u32 Delay_us ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // must point to writable memory!
    Assert_RADIO_EXTRA( Config->Features->DelayTimerPeriod_fs != 0, ttc_assert_origin_auto );  // value has to be set by low-level driver!

    if ( !Delay_us )
    { Delay_us = 10; } // some value is better than none value

    // calculate maximum possible microsecond delay for current architecture
    t_u32 volatile Delay_us_max = ttc_basic_multiply_divide_u32x3( ( t_u32 ) - 1,
                                                                   Config->Features->DelayTimerPeriod_fs,
                                                                   1000 * 1000 * 1000
                                                                 );

    if ( Delay_us > Delay_us_max )
    { Delay_us = Delay_us_max; }  // must limit value to avoid overrun during following calculation

    // Precalculating value to be added to receive timestamp to allow faster ranging replies.
    Config->Delay_RangingReply_clocks = ttc_basic_multiply_divide_u32x3( Delay_us,
                                                                         1000 * 1000 * 1000,
                                                                         Config->Features->DelayTimerPeriod_fs
                                                                       );
    Config->Delay_RangingReply_us = Delay_us;
}
e_ttc_radio_errorcode  radio_common_transmit_packet( t_ttc_radio_config* Config, t_ttc_packet* VOLATILE_RADIO Packet, t_u16 PayloadSize, BOOL ReceiveAfterTransmit, u_ttc_packetimestamp_40* TransmitTime ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );                                     // must point to writable memory!
    Assert_RADIO_Writable( Packet, ttc_assert_origin_auto );                                           // packet must point to readable memory!
    Assert_RADIO( PayloadSize <= ttc_packet_payload_get_size_max( Packet ), ttc_assert_origin_auto );  // payload will not fit into memory block
    Assert_RADIO( radio_common_InsideISR == 0, ttc_assert_origin_auto );                               // this function must not be called from interrupt service routine!

    // a socket job that searches for a received acknowledge packet
    static t_ttc_radio_job_socket SocketJob_ACK = { .Init = { E_ttc_packet_pattern_type_Acknowledge, 0 } };

    BOOL VOLATILE_RADIO AcknowlegdeRequested = ttc_packet_acknowledge_request_get( Packet );
    if ( AcknowlegdeRequested ) {
        AcknowlegdeRequested = AcknowlegdeRequested;  // debug 2-way handshaking: place breakpoint #1/3 here! (about to send packet with acknowledge request bit set)
        Packet->Meta.ReceiveAfterTransmit = TRUE;     // switch on receiver right after transmit to receive acknowledge packet
    }

    if ( 1 ) { // prepare packet before queuing it
        s_ttc_packet_payloadet_size( Packet, PayloadSize );

        if ( ( Packet->Meta.Type > E_ttc_packet_type_802154 )               &&
                ( Packet->Meta.Type < E_ttc_packet_type_802154_unknown )    &&
                ( Packet->MAC.packet_802154_generic.SequenceNo == 0 )
           ) { // 802.15.4 type packet: set sequence number field
            if ( Packet->Meta.Category == E_ttc_packet_category_Beacon ) {
                Packet->MAC.packet_802154_generic.SequenceNo = Config->SequenceNo_Beacons++;
            }
            else {
                Packet->MAC.packet_802154_generic.SequenceNo = Config->SequenceNo_Generic++;
            }
        }
        Packet->Meta.ReceiveAfterTransmit = ( ReceiveAfterTransmit ) ? 1 : 0;

        // reset transmission status
        Packet->Meta.StatusTX = 0;
    }

    if ( TransmitTime == RCT_QUEUE_ONLY ) {  // append packet to List_PacketsTx
        radio_common_push_list_tx( Config, Packet );
        AcknowlegdeRequested = FALSE; // not available for queued packets
    } // only queuing packet
    else {                                   // transmit packet before all queued packets
        radio_common_prepend_list_tx( Config, Packet );
        _driver_radio_transmit( Config, TransmitTime );
        Assert_RADIO_EXTRA( ( t_ttc_packet* ) ttc_heap_pool_from_list_item( Config->List_PacketsTx.First ) != Packet, ttc_assert_origin_auto ); // Packet to be send still sits in ListTX. Check implementation of _driver_radio_transmit()!
    }

    if ( Packet->Meta.DoNotReleaseBuffer ) { // packet not being released by transmit isr: wait until low-level driver updates its status
        if ( !Packet->Meta.StatusTX ) {         // no status update yet: wait until update or timeout
            if ( Config->Init.Transceiver_Timeout_usecs ) { // timeout set: wait until low-level driver updates status or timeout occurs
                ttc_systick_delay_init( & ( Config->TimeOut ), Config->Init.Transceiver_Timeout_usecs );
                while ( ( !Packet->Meta.StatusTX ) &&
                        ( ! ttc_systick_delay_expired( & ( Config->TimeOut ) ) )
                      )
                { ttc_task_yield(); }  // wait until status update (StatusTX has been reset automatically)
                if ( !Packet->Meta.StatusTX ) // still no status update: let radio driver check this
                { _driver_radio_maintenance( Config ); }

                Assert_RADIO( Packet->Meta.StatusTX != 0, ttc_assert_origin_auto ); // timeout occured while waiting for packet to be transmitted. Increase Config->Init.Transceiver_Timeout_usecs or find out why packet was not sent within timeout!
            }
            else {                                          // no timeout set: wait for status update endlessly (may block!) before we reuse or free packet buffer
                while ( Packet->Meta.StatusTX == 0 )
                { ttc_task_yield(); }  // wait until status update (StatusTX has been reset automatically)
            }
        }
        if ( Packet->Meta.StatusTX > tpst_Error ) { // error occured while sending packet
            if ( Packet->Meta.StatusTX == tpst_Error_DelayedTxTooLate )
            { return ec_radio_DelayedTransmitFailed; }
            return ec_radio_TransmissionFailed;
        }
    }

    if ( AcknowlegdeRequested && Config->Init.Acknowledge_Timeout_usecs ) { // packet is to be acknowledged: wait for acknowledge
        t_ttc_systick_delay TimeOut;
        ttc_systick_delay_init( &TimeOut, Config->Init.Acknowledge_Timeout_usecs );
        t_ttc_packet* PacketRX = NULL;

        radio_common_packets_received_ignore( Config, 0 ); // empty rx list to be sure that we can receive ACK
        while ( !PacketRX ) { // wait for acknowledgement packet
            PacketRX = radio_common_packet_received_tryget( Config, &SocketJob_ACK );
            if ( PacketRX ) { // we received an ACK packet
                if ( s_ttc_packetequence_number_get( PacketRX ) == s_ttc_packetequence_number_get( Packet ) ) {
                    PacketRX = radio_common_packet_release( PacketRX ); // debug 2-way handshaking: place breakpoint #2/3 here! (ACK with matching sequence number received)
                    goto rctp_OK;
                }

                PacketRX = radio_common_packet_release( PacketRX );
            }
            if ( ttc_systick_delay_expired( &TimeOut ) ) {

#if (TTC_RADIO_STATISTICS == 1)
                Config->Statistics.Amount_FailedACKs++;
#endif
                return ec_radio_TimeOut_ACK; // debug 2-way handshaking: place breakpoint #3/3 here! (no ack packet received within timeout)
            }
            ttc_task_yield();  // give CPU to other tasks while waiting
        }
    }

rctp_OK:
    return ec_radio_OK;
}
void                   radio_common_add_40_32_40( u_ttc_packetimestamp_40* Value40, t_u32 Add, u_ttc_packetimestamp_40* Sum40 ) {
    t_u32 Value32 = Value40->Words.Low;

    t_u32 Sum32 = Value32 + Add; // simple 32-bit addition which may overflow
    Sum40->Words.Low = Sum32;

    if ( Sum32 < Value32 ) {     // overflow happened: increase high byte
        Sum40->Words.High = Value40->Words.High + 1;
    }
    else {
        Sum40->Words.High = Value40->Words.High;
    }
}
t_u32                  radio_common_sub_40_40_32( u_ttc_packetimestamp_40* Value40a, u_ttc_packetimestamp_40* Value40b ) {
    Assert_RADIO_EXTRA_Readable( Value40a, ttc_assert_origin_auto );
    Assert_RADIO_EXTRA_Readable( Value40b, ttc_assert_origin_auto );

    t_u32 Value40a_low = Value40a->Words.Low;
    t_u32 Value40b_low = Value40b->Words.Low;

    if ( Value40a_low < Value40b_low ) {
        if ( Value40a->Words.High != Value40b->Words.High + 1 )
        { return 0; } // Value40a < Value40b or difference does not fit into 32 bit!
    }

    return Value40a_low - Value40b_low;
}
void                   radio_common_time_add_ns( t_ttc_radio_config* Config, u_ttc_packetimestamp_40* Time, t_u32 Delay_ns, u_ttc_packetimestamp_40* TimeDelayed ) {
    Assert_RADIO_EXTRA_Readable( Time, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_RADIO_EXTRA_Writable( TimeDelayed, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    if ( Delay_ns ) { // calculate correct clock value in a smart way to avoid overflows
        Delay_ns = ttc_basic_multiply_divide_u32x3( Delay_ns,
                                                    1000 * 1000,
                                                    Config->Features->DelayTimerPeriod_fs
                                                  );

        radio_common_add_40_32_40( Time,
                                   Delay_ns,
                                   TimeDelayed
                                 );
    }
    else {
        if ( Time != TimeDelayed ) { // copy timestamp value
            TimeDelayed->Words.Low  = Time->Words.Low;
            TimeDelayed->Words.High = Time->Words.High;
        }
    }
}
void                   radio_common_breakpoint() {
    VOLATILE_RADIO static t_u32 radio_common_testpoint_Counter = 0;
    radio_common_testpoint_Counter++;
    ttc_assert_break_origin( ttc_assert_origin_auto ); //DEBUG
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

#if TTC_PACKET_RANGING == 1
t_u8                   radio_common_ranging_measure( t_ttc_radio_config* Config, e_radio_common_ranging_type RangingType, t_u8 AmountRepliesRequired, const t_ttc_packet_address* RemoteID, t_u16 TimeOutMS, u_ttc_packetimestamp_40* ReferenceTime, t_ttc_radio_distance* VOLATILE_RADIO Distances, t_ttc_math_vector3d_xyz* VOLATILE_RADIO Locations, BOOL ReenableReceiver ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // must point to writable memory!
    Assert_RADIO_EXTRA_Readable( RemoteID, ttc_assert_origin_auto );  // must point to writable memory!
    Assert_RADIO( ( RangingType  > 0 ) && ( RangingType < rcrt_Requests_End ), ttc_assert_origin_auto );  // only first rcrt_Request_* entries of e_radio_common_ranging_type are allowed here!
    Assert_RADIO( TimeOutMS > 0, ttc_assert_origin_auto ); // timeout value required. Check your function call!

#if TTC_PACKET_RANGING==1
    if ( !Config->Init.Flags.EnableRanging )
    { return 0; }

    ttc_semaphore_give( &radio_common_RangingNo, 1 ); // every ranging request gets a new number to identify correct ranging answer
    t_u8*               Payload;          // will be loaded with pointer to payload area
    t_ttc_systick_delay TimeOut;          // used to avoid waiting for external events endlessly
    t_u16               CurrentRangingNo = ttc_semaphore_available( &radio_common_RangingNo );

    if ( 1 ) { // create + send ranging request to remote node

        // switch off receiver to prepare immediate transmission
        _driver_radio_receiver( Config, FALSE, NULL, 0 );

        // empty receive list
        VOLATILE_RADIO t_u16 AmountIgnored = radio_common_packets_received_ignore( Config, E_ttc_packet_pattern_socket_None );
        ( void ) AmountIgnored; // value just used for debugging
        Assert_RADIO( ttc_semaphore_available( &( Config->Pool_Packets.BlocksAvailable ) ) > AmountRepliesRequired, ttc_assert_origin_auto ); // cannot store as much packets as required. Increase pool size!

        t_ttc_packet* PacketRangingRequest;

        e_ttc_packet_type VOLATILE_RADIO PacketType;
        if ( RemoteID->Address16 ) // 16-bit address given: create packet for 16 bit address (shorter)
        { PacketType = E_ttc_packet_type_802154_Data_001010; }
        else                       // no 16-bit address given: create packet for 64 bit address
        { PacketType = E_ttc_packet_type_802154_Data_011111; }
        PacketRangingRequest = radio_common_packet_get_empty( Config, PacketType, E_ttc_packet_pattern_socket_Radio_Ranging );

        // address packet
        E_ttc_packet_address_source_set( PacketRangingRequest, &( Config->LocalID ) );
        ttc_packet_address_target_set( PacketRangingRequest, RemoteID );

        Assert( ttc_packet_payload_get_pointer( PacketRangingRequest, &Payload, NULL, NULL ) == ec_packet_OK, ttc_assert_origin_auto ); // could not obtain pointer to payload area of new packet

        // load payload with ranging request
        u_ttc_radio_ranging_message* NewRequest = ( u_ttc_radio_ranging_message* ) Payload;

        switch ( RangingType ) { // fill Payload[]==NewRequest with new ranging measure request according to given type
            case rcrt_Request_Ranging_SSTOF:
            case rcrt_Request_Ranging_SSTOFCR:
            case rcrt_Request_Localization_SSTOFCR: {
                // request type supported: create packet
                NewRequest->Ranging.Type         = RangingType;
                NewRequest->Ranging.RangingNo    = CurrentRangingNo;

                // this special ranging type carries an extra field
                if ( RangingType == rcrt_Request_Localization_SSTOFCR )
                { NewRequest->Request_Ranging_SSTOFCR.AmountRequestedAnswers = AmountRepliesRequired; }

                /* Request to send this packet as a ranging request.
                 * The low-level driver has to
                 * 1) store exact transmission time of packet
                 * 2) set RNG bit in PHY Header (PHR)
                 */
                PacketRangingRequest->Meta.RangingMessage = 1;

                // switch on receiver after transmission to receive ranging reply
                PacketRangingRequest->Meta.ReceiveAfterTransmit = 1;

                // disable automatic release of packet to be able to monitor it's transmission status
                PacketRangingRequest->Meta.DoNotReleaseBuffer = 1;

                break;
            }
            case rcrt_Report_Localization_SSTOFCR: ttc_assert_halt_origin( ttc_assert_origin_auto ); break;
                TODO( "Implement sending of rcrt_Report_Localization_SSTOFCR packets!" )
            case rcrt_Report_Ranging_SSTOF: ttc_assert_halt_origin( ttc_assert_origin_auto ); break;
                TODO( "Implement sending of rcrt_Report_Ranging_SSTOF packets!" )
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unsupported request type (add case + implementation above!)
        }

        ttc_systick_delay_init( &TimeOut, 100000 ); // 100ms timeout
        while ( s_ttc_listize( & Config->List_PacketsTx ) ) { // wait until transmit queue has run dry
            ttc_task_yield();
            if ( ttc_systick_delay_expired( &TimeOut ) ) { // Got no timeout: check radio state
                _driver_radio_maintenance( Config ); // will process List_PacketsTx
            }
        }

        // Note: Remote node implementation can be found in radio_common_ranging_reply_isr()
        e_ttc_radio_errorcode Error = radio_common_transmit_packet( Config,
                                                                    PacketRangingRequest,
                                                                    sizeof( t_ttc_radio_packet_request_ranging_sstof ),
                                                                    TRUE,  // immediately activate receiver after TX
                                                                    ReferenceTime
                                                                  );

        if ( 1 ) {     // we now have to release packet on our own
            PacketRangingRequest->Meta.DoNotReleaseBuffer = 0;
            PacketRangingRequest = radio_common_packet_release( PacketRangingRequest );
        }
        if ( Error ) { // could not send delayed packet: abort transmission
            return 0;
        }
    }

    t_ttc_list_item* VOLATILE_RADIO List_RangingReplies     = NULL; // single linked list of all received ranging replies
    t_ttc_list_item* VOLATILE_RADIO List_RangingRepliesLast = NULL; // last item in single linked list

    ttc_systick_delay_init( &TimeOut, TimeOutMS * 1000 );
    if ( AmountRepliesRequired == 0 )
    { AmountRepliesRequired = -1; } // set to maximum positive value

    VOLATILE_RADIO t_u8 AmountReplies = 0;
    while ( AmountReplies < AmountRepliesRequired ) { // collect all ranging answers as fast as possible
        t_ttc_packet* VOLATILE_RADIO PacketRX = radio_common_packet_received_tryget( Config, NULL );
        if ( PacketRX ) { // received a packet: check if it is the awaited ranging answer

            if ( PacketRX->Meta.RangingMessage ) {
                //X ttc_radio_update_meta( 1, PacketRX ); // fill packet Meta-header with ranging information

                if ( PacketRX->Meta.ReceiveTime.Fields.TIMESTAMP_LOW ) { // packet stores timestamp: calculate round trip time
                    e_ttc_packet_errorcode Error = ttc_packet_payload_get_pointer( ( t_ttc_packet* ) PacketRX, &Payload, NULL, NULL );
                    ( void ) Error; Assert_RADIO_EXTRA( Error == ec_packet_OK, ttc_assert_origin_auto ); // could not obtain pointer to payload area of new packet

                    // load payload with ranging request
                    u_ttc_radio_ranging_message* RangingAnswer = ( u_ttc_radio_ranging_message* ) Payload;

                    if ( ( RangingAnswer->Type == rcrt_Reply_Ranging_SSTOFCR ) ||
                            ( RangingAnswer->Type == rcrt_Reply_Localization_SSTOFCR )
                       ) { // this seems to be a reply to a ranging request
                        if ( RangingAnswer->Ranging.RangingNo == CurrentRangingNo ) { // it fits to our request

                            // store received packet in single linked list for later processing
                            // (packets are stored in memory pool list items which allows to store them in a single linked list)
                            t_ttc_list_item* ItemRX = ttc_heap_pool_to_list_item( ( t_ttc_heap_block_from_pool* ) PacketRX );
                            if ( List_RangingRepliesLast == NULL ) { // list empty: store as first item in list
                                List_RangingReplies = ItemRX;
                            }
                            else {                                   // list not empty: append received packet after last item
                                List_RangingRepliesLast->Next = ItemRX;
                            }
                            List_RangingRepliesLast = ItemRX;
                            ItemRX->Next = NULL; // just to be safe (line may be omitted)

                            // store time of corresponding transmission in packet
                            PacketRX->Meta.TransmitTime.Words.High = Config->LastTransmitTime.Words.High;
                            PacketRX->Meta.TransmitTime.Words.Low  = Config->LastTransmitTime.Words.Low;

                            PacketRX = NULL; // prevent releasing this buffer
                            AmountReplies++;
                        }
                    }
                }
            }
            if ( PacketRX ) // packet ignored: release its buffer
            { PacketRX = radio_common_packet_release( PacketRX ); }
        }
        else {
            if ( ttc_systick_delay_expired( &TimeOut ) )
            { break; } // timeout expired: stop waiting for replies
        }
    }
    if ( !ReenableReceiver ) { // no more replies expected: switch off receiver + empty receive list
        _driver_radio_receiver( Config, 0, 0, 0 );
        radio_common_packets_received_ignore( Config, E_ttc_packet_pattern_socket_None ); // maybe E_ttc_packet_pattern_socket_Radio_Ranging is egnough?
    }

    t_ttc_math_vector3d_xyz* VOLATILE_RADIO Location = Locations;
    t_ttc_radio_distance*    VOLATILE_RADIO Distance = Distances;
    t_ttc_list_item*         VOLATILE_RADIO ItemRX   = List_RangingReplies;

    if ( AmountReplies > 0 ) { // received ranging replies: process them
        for ( t_u8 ReplyIndex = 0; ReplyIndex < AmountReplies; ReplyIndex++ ) { // process all received ranging replies
            // converting list item back to stored content
            t_ttc_packet* VOLATILE_RADIO PacketRX = ( t_ttc_packet* ) ttc_heap_pool_from_list_item( ItemRX );

            VOLATILE_RADIO t_u32 RoundTripTime_ClockCycles = radio_common_sub_40_40_32( & ( ( t_ttc_packet* ) PacketRX )->Meta.ReceiveTime,
                                                                                        & ( ( t_ttc_packet* ) PacketRX )->Meta.TransmitTime // timestamp of ranging request transmission
                                                                                      );

            // get access to ranging reply data inside received packet
            e_ttc_packet_errorcode Error = ttc_packet_payload_get_pointer( ( ( t_ttc_packet* ) PacketRX ), &Payload, NULL, NULL );
            ( void ) Error; Assert_RADIO_EXTRA( Error == ec_packet_OK, ttc_assert_origin_auto ); // could not obtain pointer to payload area of new packet
            u_ttc_radio_ranging_message* RangingAnswer = ( u_ttc_radio_ranging_message* ) Payload;

            if ( RoundTripTime_ClockCycles > RangingAnswer->Reply_Ranging_SSTOF.Ranging.ResponseDelay_ClockCycles )
            { RoundTripTime_ClockCycles -= RangingAnswer->Reply_Ranging_SSTOF.Ranging.ResponseDelay_ClockCycles; }
            else
            { RoundTripTime_ClockCycles = 0; }  // avoid negative values for too short ranges

            TODO( "implement exchange of clock cycle period and other details between both radios!" )
            t_u32 Foreign_DelayTimerPeriod_fs = Config->Features->DelayTimerPeriod_fs; // ToDo: obtain foreign timer period!

            if ( Foreign_DelayTimerPeriod_fs != Config->Features->DelayTimerPeriod_fs ) { // foreign node uses different clock frequency: adjust to our clock-frequency
                // PacketRX->Meta.ReceiveTime.Words.Low = ttc_basic_multiply_divide_u32x3( RoundTripTime_ClockCycles,
                //                                                                         Config->Features->DelayTimerPeriod_fs,
                //                                                                         RangingAnswer->..->DelayTimerPeriod_fs
                //                                                                       );
            }

            if ( 1 ) { // store ranging measure to source node in Distances[]
                if ( RoundTripTime_ClockCycles > Config->RoundTripTime2Distance_Subtract ) {
                    RoundTripTime_ClockCycles -= Config->RoundTripTime2Distance_Subtract;
                }
                else { // avoid negative distances
                    RoundTripTime_ClockCycles = 0;
                }

                if ( RoundTripTime_ClockCycles < Config->Features->MinRoundTripTimeClockCycles ) { // below minimum usefull distance: distance = 0
                    Distance->Distance_cm = 0;
                }
                else { // use constant provided by low-level driver to calculate distance in centimeter
                    Distance->Distance_cm = ttc_basic_multiply_divide_u32x3( RoundTripTime_ClockCycles,
                                                                             Config->RoundTripTime2Distance_Multiplier,
                                                                             Config->RoundTripTime2Distance_Divider
                                                                           );
                }

                // copy source id of remote node from received packet
                //X s_ttc_packetource_extract( ( ( t_ttc_packet* ) PacketRX ), & ( Distance->RemoteID ) );
                E_ttc_packet_address_source_get( ( ( t_ttc_packet* ) PacketRX ), & ( Distance->RemoteID ) );
                Distance++;
            }
            if ( Location ) { // location array given: copy location data

                if ( RangingAnswer->Type == rcrt_Reply_Localization_SSTOFCR ) { // store location of remote node
                    Location->X = RangingAnswer->Reply_Localization_SSTOFCR.X_mm;
                    Location->Y = RangingAnswer->Reply_Localization_SSTOFCR.Y_mm;
                    Location->Z = RangingAnswer->Reply_Localization_SSTOFCR.Z_mm;
                }
                else {                                                          // no localization reply: reset location data
                    Location->X = TTC_MATH_CONST_NAN;
                    Location->Y = TTC_MATH_CONST_NAN;
                    Location->Z = TTC_MATH_CONST_NAN;
                }
                Location++;
            }

            // Find next packet item in single linked list of received packets.
            // Each packet is stored in a memory block from a pool. Each pool block has a next pointer.
            t_ttc_list_item* NextItemRX = ItemRX->Next;
            PacketRX = radio_common_packet_release( ( ( t_ttc_packet* ) PacketRX ) );
            ItemRX = NextItemRX;
        }
    }
    else {                     // no replies at all: check radio
        _driver_radio_receiver( Config, 0, 0, 0 ); // it has been observed that DW1000 requires to switch off+on RX when nothing is received
        _driver_radio_maintenance( Config );
    }

    return AmountReplies;
#else
    return 0;
#endif
}
BOOL                   radio_common_ranging_reply_isr( t_ttc_radio_config* Config, t_ttc_packet* Packet ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // must point to writable memory!
    Assert_RADIO_EXTRA_Writable( Packet, ttc_assert_origin_auto );  // must point to writable memory!

    VOLATILE_RADIO u_ttc_radio_ranging_message* RangingPayload;  // will be loaded with pointer to payload area

    if ( ttc_packet_payload_get_pointer( Packet, ( t_u8** ) &RangingPayload, NULL, NULL ) != ec_packet_OK )
    { goto rcrri_failed; } // something is wrong with this packet!

    if ( !RangingPayload )
    { goto rcrri_failed; } // if it has no payload, it cannot be a ranging request

    // we now assume it is a ranging request
    static u_ttc_packetimestamp_40 rcrri_TransmitTime;

    switch ( RangingPayload->Type ) { // set common fields
        case rcrt_Request_Ranging_SSTOF:
        case rcrt_Request_Ranging_SSTOFCR:
        case rcrt_Request_Localization_SSTOFCR: {  // ranging request supported
            // address packet back to its originator
            ttc_packet_address_reply( Packet, &Config->LocalID );

            // request to re-enable receiver after sending this packet
            Packet->Meta.ReceiveAfterTransmit = 1;

            if ( Config->Delay_RangingReply_clocks ) { // calculate exact time of transmission relative to receive time
                radio_common_add_40_32_40( & Packet->Meta.ReceiveTime,
                                           Config->Delay_RangingReply_clocks,
                                           & rcrri_TransmitTime
                                         );

                // transmission time ignores lowest 9 bits (at least for DW1000)
                rcrri_TransmitTime.Words.Low &= Config->Features->DelayedTimeMask;

                // calculate real delay + store it in reply message
                RangingPayload->Reply_Ranging_SSTOF.Ranging.ResponseDelay_ClockCycles =
                    radio_common_sub_40_40_32( & rcrri_TransmitTime, & Packet->Meta.ReceiveTime )
                    + Config->TransmitTime_AntennaDelay;

                // Note: RangingPayload->Ranging.RangingNo stays unchanged (reusing packet buffer)
            }
            break;
        }
        default: goto rcrri_failed; break;         // type of ranging not supported
    }
    VOLATILE_RADIO t_u16 PayloadSize;
    switch ( RangingPayload->Type ) { // set special fields
        case rcrt_Request_Ranging_SSTOF: {         // set fields special for ranging replies

            // make packet a ranging reply
            RangingPayload->Type = rcrt_Reply_Ranging_SSTOF;

            PayloadSize = sizeof( t_ttc_radio_packet_reply_ranging_sstof );

            break;
        }
        case rcrt_Request_Ranging_SSTOFCR: {       // set fields special for ranging replies with cascaded replies
            TODO( "Limit amount of replies according to  RangingPayload->Request_Ranging_SSTOFCR.AmountRequestedAnswers" )

            // make packet a ranging reply
            RangingPayload->Type = rcrt_Reply_Ranging_SSTOFCR;

            // set size of payload in ranging reply packet
            PayloadSize = sizeof( t_ttc_radio_packet_reply_ranging_sstof );

            break;
        }
        case rcrt_Request_Localization_SSTOFCR: {  // set fields special for localization replies
            TODO( "Limit amount of replies according to  RangingPayload->Request_Localization_SSTOFCR.AmountRequestedAnswers" )

            // make packet a localization reply
            RangingPayload->Type = rcrt_Reply_Localization_SSTOFCR;
            t_ttc_math_vector3d_xyz* MyLocation = Config->Init.MyLocation;
            if ( MyLocation ) { // fill in our current coordinates
                RangingPayload->Reply_Localization_SSTOFCR.X_mm = MyLocation->X;
                RangingPayload->Reply_Localization_SSTOFCR.Y_mm = MyLocation->Y;
                RangingPayload->Reply_Localization_SSTOFCR.Z_mm = MyLocation->Z;
            }
            else {              // set all coordinates to undefined
                RangingPayload->Reply_Localization_SSTOFCR.X_mm = TTC_RADIO_COORDINATE_UNDEFINED;
                RangingPayload->Reply_Localization_SSTOFCR.Y_mm = TTC_RADIO_COORDINATE_UNDEFINED;
                RangingPayload->Reply_Localization_SSTOFCR.Z_mm = TTC_RADIO_COORDINATE_UNDEFINED;
            }

            // set size of payload in ranging reply packet
            PayloadSize = sizeof( t_ttc_radio_packet_reply_localization_sstofcr );

            Assert_RADIO_EXTRA( RangingPayload->Reply_Ranging_SSTOF.Ranging.ResponseDelay_ClockCycles == RangingPayload->Reply_Localization_SSTOFCR.Ranging.ResponseDelay_ClockCycles, ttc_assert_origin_auto );  // These structures should have same header. Check structure definitions!

            break;
        }
        default: goto rcrri_failed; break;         // type of ranging not supported
    }
    if ( 1 ) {                        // send ranging reply
        s_ttc_packet_payloadet_size( Packet, PayloadSize );

        // append packet as first in list of transmissions
        radio_common_prepend_list_tx_isr( Config, Packet );

        if ( Config->Delay_RangingReply_clocks )
        { _driver_radio_transmit( Config, &rcrri_TransmitTime ); } // send ranging reply after exact delay (will be monitored by low-level interrupt service routine)
        else
        { _driver_radio_transmit( Config, NULL ); }                // send reply immediately (unusable for ranging but may help to optimize settings using an oscilloscope)

        Config->Amount_RangingReplies++;
        Config->StatusTX = 0;
        Assert_RADIO_EXTRA( E_ttc_radio_status_tx_started < E_ttc_radio_status_tx_too_late, ttc_assert_origin_auto ); // configuration error found in e_ttc_radio_status. Reorder enum to fullfill this constraint and allow this code to work!
        if ( Config->Init.Transceiver_Timeout_usecs ) { // timeout set: wait until low-level driver updates status or timeout occurs
            ttc_systick_delay_init_isr( & ( Config->TimeOut ), Config->Init.Transceiver_Timeout_usecs );
            while ( ( Config->StatusTX < E_ttc_radio_status_tx_started ) &&
                    ( ! ttc_systick_delay_expired_isr( & ( Config->TimeOut ) ) )
                  ) {
                _driver_radio_isr_transmit( 0, Config );
            }  // run interrupt service routine to get status update from radio
        }
        else {                                          // no timeout set: wait for status update endlessly (may block!) before we reuse or free packet buffer
            while ( Config->StatusTX < E_ttc_radio_status_tx_started ) {
                _driver_radio_isr_transmit( 0, Config );
            }  // run interrupt service routine to get status update from radio
        }
        if ( Config->StatusTX == E_ttc_radio_status_tx_too_late ) { // preparation of ranging reply took too long: increase our delay time
            radio_common_set_ranging_delay( Config, Config->Delay_RangingReply_us * 2 );
#if (TTC_RADIO_STATISTICS == 1)
            Config->Statistics.Amount_IncreasedDelayTX++;
#endif
        }
        else {
            if ( Config->StatusTX == 0 ) { // still no status update: let radio driver check this
                /* Note: Low-level driver interrupt service routine must update Config->StatusTX during transmission.
                 *       Check enum declaration e_ttc_radio_status for details!
                 */
                Assert_RADIO_EXTRA( 0, ttc_assert_origin_auto ); // low-level driver did not update Config->StatusTX. Check implementation of _driver_radio_isr_transmit()!
                _driver_radio_maintenance( Config ); // we may let low-level driver handle this
            }
        }

        return TRUE; // packet has been released after transmission
    }

rcrri_failed:
    return FALSE; // packet was not processed or released
}
#endif

void*                 _radio_common_iterator_packet_ignore( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item ) {
    e_ttc_packet_pattern Pattern = ( e_ttc_packet_pattern ) Argument;
    Assert_RADIO_EXTRA( Pattern != 0, ttc_assert_origin_auto ); // makes no sense to use list iteration to ignore all socket identifiers. Call radio_common_packets_received_ignore() which shall do this in a simple while loop!
    Assert_RADIO_Writable( Item, ttc_assert_origin_auto ); // invalid pointer: check list implementation!
    ( void ) PreviousItem; // not used

    // we know that this item came from a memory pool
    t_ttc_heap_block_from_pool* MemoryBlock = ttc_heap_pool_from_list_item( Item );

    // memory blocks from pool may be casted to anything
    t_ttc_packet* VOLATILE_RADIO Packet = ( t_ttc_packet* ) MemoryBlock;

    // ttc_packet will do the check for us
    if ( ttc_packet_pattern_matches( Packet, Pattern ) ) { // packet is to be removed from list
        return _radio_common_item_release; // ttc_list_iterate will call this function to release memory buffer after removing item from list
    }

    return NULL;
}
void*                 _radio_common_iterator_packet_remove( void* Argument, t_ttc_list_item* PreviousItem, t_ttc_list_item* Item ) {
    Assert_RADIO_Writable( Argument, ttc_assert_origin_auto );
    t_radio_common_socket_iterator* Job = ( t_radio_common_socket_iterator* ) Argument;
    ( void ) PreviousItem;

    if ( ! Job->Return.Amount || ( Job->Input.Flags.OnlyFirst == 0 ) ) { // packet not yet found: check this one against all activate patterns
        BOOL PatternMatches = FALSE;

        // obtain memory block that stores this list item (we know that it came from a ttc_heap_pool)
        t_ttc_heap_block_from_pool* Block = ttc_heap_pool_from_list_item( Item );

        // memory blocks from pool may be casted to anything
        t_ttc_packet* VOLATILE_RADIO Packet = ( t_ttc_packet* ) Block;

        Assert_RADIO( Job->Input.Patterns_Amount < TTC_RADIO_MAX_PATTERNS, ttc_assert_origin_auto ); // will run out of Patterns.Input.Patterns[] array. Check implementation of caller!
        e_ttc_packet_pattern* Pattern = Job->Input.Patterns;
        e_ttc_packet_pattern SocketID = 0;
        e_ttc_packet_type    Type     = 0;
        for ( t_u8 Count = Job->Input.Patterns_Amount; Count > 0; Count-- ) { // check all given patterns
            if ( *Pattern < E_ttc_packet_pattern_type_End ) { // looking for a paket type: we have to check packet header instead of payload
                if ( !Type )
                { Type = E_ttc_packet_type_get( Packet ); }   // first occurence: extract type from packet
                PatternMatches = ttc_packet_pattern_matches_type( Type, *Pattern );
            }
            else {                                          // looking for a socket identifier: check first byte of payload
                if ( !SocketID )                            // first occurence: extract socket identifier from packet
                { SocketID = s_ttc_packetocket_get( Packet ); }

                PatternMatches = ttc_packet_pattern_matches_socket( SocketID, *Pattern );
            }
            if ( PatternMatches ) // found a pattern that matches this packet
            { break; }
            Pattern++;
        }
        if ( PatternMatches ) { // store pointer to packet being found before it is removed from list

            if ( Job->Input.Flags.RemoveAll ||                            // shall remove all matching items
                    ( Job->Input.Flags.RemoveFirst && ( Job->Return.Amount == 0 ) ) // shall remove first matching item
               )
            { return _radio_common_item_store; } // ttc_list_iterator*() will remove Item from its list and then call this function to store it in our socket job
        }
    }

    return NULL;
}
void                  _radio_common_item_store( void* Argument, t_ttc_list_item* Item ) {
    Assert_RADIO_Writable( Argument, ttc_assert_origin_auto );
    t_radio_common_socket_iterator* Job = ( t_radio_common_socket_iterator* ) Argument;

    if ( !Job->Return.Amount ) { // first entry
        Job->Return.PacketFound_First = Item;
        Job->Return.PacketFound_Last  = Item;
        Job->Return.Amount = 1;
    }
    else {                       // append at end of list
        Job->Return.PacketFound_Last->Next = Item;
        Job->Return.PacketFound_Last       = Item;
        Job->Return.Amount++;
    }

    // terminate list
    Item->Next = NULL;
}
void                  _radio_common_item_release( void* Argument, t_ttc_list_item* Item ) {
    ( void ) Argument;

    // we know that this item came from a memory pool
    t_ttc_heap_block_from_pool* MemoryBlock = ttc_heap_pool_from_list_item( Item );

    // memory blocks from pool may be casted to anything
    t_ttc_packet* Packet = ( t_ttc_packet* ) MemoryBlock;

    radio_common_packet_release( Packet );
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
