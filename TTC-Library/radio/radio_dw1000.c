/** { radio_dw1000.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for radio devices on dw1000 architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20150309 14:18:02 UTC
 *
 *  Note: See ttc_radio.h for description of dw1000 independent RADIO implementation.
 *
 *  Authors: Gregor Rebel, Adrián Romero Cáceres
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "radio_dw1000.h".
//
#include "../ttc_spi.h"                     // microcontroller independent serial peripheral bus communication to radio transceiver
#include "../ttc_list.h"                    // multitasking and interrupt aware serial linked lists
#include "../ttc_interrupt.h"               // microcontroller independent interrupt configuration and generation
#include "../ttc_memory.h"                  // memory set, clear and check functions
//? #include "../ttc_math.h"                    // floating point trigonometric mathematic support
#include "../ttc_systick.h"                 // exact delays
#include "../ttc_gpio.h"                    // direct access to individual gpio pins
#include "../ttc_task.h"                    // basic single- and multitasking support
#include "../ttc_mutex.h"                   // synchronization between tasks and interrupt service routines
#include "../ttc_radio_types.h"
#include "radio_dw1000.h"
#include "radio_common.h"
#include "radio_dw1000_registers.h"         // definitions of and macros to read/write dw1000-registers
#include "radio_dw1000_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
#if 1 // manually set TX_LED/RX_LED (ToDo: find out how to enable DW1000 automatic LED feature
    #define _RADIO_DW1000_LED_TX(STATE) if ( Config->Init.Flags.Enable_LED_TX ) { radio_dw1000_gpio_out( Config, E_ttc_radio_gpio_status_tx, STATE ); }
    #define _RADIO_DW1000_LED_RX(STATE) if ( Config->Init.Flags.Enable_LED_RX ) { radio_dw1000_gpio_out( Config, E_ttc_radio_gpio_status_rx, STATE ); }
#else
    #define _RADIO_DW1000_LED_TX(STATE)
    #define _RADIO_DW1000_LED_RX(STATE)
#endif

#if (TTC_ASSERT_RADIO_EXTRA == 1)
    #define RADIO_DW1000_STATUS_ISR_UPDATE(STATUS) if (radio_dw1000_isr_LowLevelConfig->StatusISR < STATUS) radio_dw1000_isr_LowLevelConfig->StatusISR = STATUS
#else
    #define RADIO_DW1000_STATUS_ISR_UPDATE(STATUS)
#endif

//{ Constant radio configuration

const BOOL radio_dw1000_InsertSleeps = 0;

#ifdef EXTENSION_300_scheduler_freertos
    #  warning BUG: radio_dw1000 is known to have issues with freertos scheduler - Fix it or disable FreeRTOS in your activate_project.sh!
#endif

//X BOOL DebugMode = 0; //DEBUG

const BOOL radio_dw1000_UseDoubleBuffering = FALSE; // ToDo: use of double buffering is untested!

const t_ttc_radio_features radio_dw1000_Features = {
    .MaxTxLevel       = 335,       // 33.5 dB
    .MinTxLevel       = 0,         //  0 dB
    .DefaultTxLevel   = 200,       // 20 dB
    .MinRxLevel       = 0,         // not supported
    .MaxRxLevel       = 0,         // not supported
    .DefaultRxLevel   = 0,         // not supported
    .MinChannel       = 0,
    .MaxChannel       = rdvc_virtual_unknown - 1, // -> e_radio_dw1000_virtual_channel
    .MaxPacketSize    = 1024,
    .MinBitRate       = 110000,    // -> e_radio_dw1000_datarates
    .MaxBitRate       = 6800000,   // -> e_radio_dw1000_datarates

    // default channel + bitrate -> DW1000 UserManual p.20
    .DefaultBitRate   = 6800000,   // -> e_radio_dw1000_datarates
    .DefaultRxChannel = rdvc_virtual26_freq6489_bw499_PRC4,
    .DefaultTxChannel = rdvc_virtual26_freq6489_bw499_PRC4,

    .DelayedTimeMask = 0xffffffff - 0b111111111, // lowest 9 bits of DX_TIME register are ignored (-> DW1000 UserManual p.77)

    .MinRoundTripTimeClockCycles = 270,          // as seen in practical measures (approximately 1 meter)
    .DelayTimerFrequency_kHz     = 63897600,     // system time counter is running at nominal 63.8976 GHz
    .DelayTimerPeriod_fs         = 15650         // amount of femtoseconds (1e-15s) of single sytem time counter step
};

// Replica Avoidance Coefficient for different preamble sequences -> DW1000 UserManual p.169
const t_u16 RADIO_DW1000_LDE_REPLICA_COEFFICIENT[RADIO_DW1000_AMOUNT_PREAMBLE_CODES] = {

    // 0
    ( t_u16 )( 0.0 * 65536 ),
    // 1
    ( t_u16 )( 0.35 * 65536 ),
    // 2
    ( t_u16 )( 0.35 * 65536 ),
    // 3
    ( t_u16 )( 0.32 * 65536 ),
    // 4
    ( t_u16 )( 0.26 * 65536 ),
    // 5
    ( t_u16 )( 0.27 * 65536 ),
    // 6
    ( t_u16 )( 0.18 * 65536 ),
    // 7
    ( t_u16 )( 0.50 * 65536 ),
    // 8
    ( t_u16 )( 0.32 * 65536 ),
    // 9
    ( t_u16 )( 0.16 * 65536 ),
    // 10
    ( t_u16 )( 0.20 * 65536 ),
    // 11
    ( t_u16 )( 0.23 * 65536 ),
    // 12
    ( t_u16 )( 0.24 * 65536 ),
    // 13
    ( t_u16 )( 0.23 * 65536 ),
    // 14
    ( t_u16 )( 0.21 * 65536 ),
    // 15
    ( t_u16 )( 0.17 * 65536 ),
    // 16
    ( t_u16 )( 0.21 * 65536 ),
    // 17
    ( t_u16 )( 0.20 * 65536 ),
    // 18
    ( t_u16 )( 0.21 * 65536 ),
    // 19
    ( t_u16 )( 0.21 * 65536 ),
    // 20
    ( t_u16 )( 0.28 * 65536 ),
    // 21
    ( t_u16 )( 0.23 * 65536 ),
    // 22
    ( t_u16 )( 0.22 * 65536 ),
    // 23
    ( t_u16 )( 0.19 * 65536 ),
    // 24
    ( t_u16 )( 0.22 * 65536 )
};

//}Constant radio configuration
//{ Global Variables ***********************************************************

// Set of global variables available during radio_dw1000_isr_generic()
// These variables are updated by radio_dw1000_isr_generic() and only valid during its run!


extern BOOL radio_common_InsideISR;  // !=0: interrupt service routine is running (only seen in functions being called from radio_dw1000_isr_generic())

// Received Frames Meta Data
// When receiving a frame, extra statistical data is stored for later use in a ring buffer.
// This allows to keep the interrupt service routine fast. Time consuming calculation of RSSI and
// ranging information is done by the application.
// For each packet, Packet->Meta.IndexPhysical is set as index of corresponding entry in radio_dw1000_RxMetaIndex[].
//
#ifndef RADIO_DW1000_AMOUNT_RX_META
    #define RADIO_DW1000_AMOUNT_RX_META 2 // entries of radio_dw1000_RxMetaIndex[] are reused after a while. Increase this constant to store more entries.
#endif
static t_radio_dw1000_frame_meta           radio_dw1000_RxFrameMeta[RADIO_DW1000_AMOUNT_RX_META];
static t_u8                                radio_dw1000_RxMetaIndex            = 0;    // index of next entry of radio_dw1000_RxMetaIndex[]

// Global Variables below are active only inside radio_dw1000_isr_generic() run
static t_radio_dw1000_frame_meta*          radio_dw1000_isr_CurrentRxFrameMeta  = NULL;  // pointer to current entry of rdir_SignalQualityRaw[]
static u_radio_dw1000_register_sys_status* radio_dw1000_isr_SysStatus           = NULL;  // pointer to current SYS_STATUS value (valid inside interrupt service routine only)
t_ttc_radio_architecture*                  radio_dw1000_isr_LowLevelConfig      = NULL;  // pointer to current Config->LowLevelConfig
t_radio_dw1000_registers*                  radio_dw1000_isr_Registers           = NULL;  // pointer to current Config->LowLevelConfig.Registers
BOOL volatile                              radio_dw1000_isr_TransmissionStarted = FALSE; // ==TRUE: a new packet transmission has started

#if TTC_ASSERT_RADIO_EXTRA == 1 // allocate ring buffer to record consecutive, nonzero SysStatus values to aid debugging
    #define RADIO_DW1000_AMOUNT_SYS_STATUS 10
    static t_u8 VOLATILE_RADIO radio_dw1000_isr_Index_Memo_SysStatus = -1; // index of latest stored entry in radio_dw1000_isr_Memo_SysStatus[]
    static u_radio_dw1000_register_sys_status VOLATILE_RADIO radio_dw1000_isr_Memo_SysStatus[RADIO_DW1000_AMOUNT_SYS_STATUS];
#else
    #define RADIO_DW1000_AMOUNT_SYS_STATUS 0
#endif
//}Global Variables

/** { Private Function Declarations *******************************************************
 *
 * Driver radio_dw1000 is meant to be operated via ttc_radio only.
 * The private functions below shall be called from radio_dw1000.c only to keep it universal and compatible.
 * If you need additional functionality, add more driver functions to ttc_radio and call them from ttc_radio.c!
 * This of course means to add functionality to all radio_* drivers.
 */

/** upload current always on configuration to AON memory
 *
 * @param Config  configuration structure retrieved from ttc_radio_get_configuration()
 *
 */
void _radio_dw1000_aon_upload( t_ttc_radio_config* Config );

/** Read and overrun condition
 *
 * Note: Will automatically reset receiver if error has occured
 *
 * @param Config  configuration structure retrieved from ttc_radio_get_configuration()
 * @return ==0: no overrun occured; error code otherwise
 */
BOOL _radio_dw1000_check_receiver_overrun( t_ttc_radio_config*   Config );

/** switch to different clock scheme type
 *
 * @param Config      configuration structure retrieved from ttc_radio_get_configuration()
 * @param ClockSource -> e_radio_dw1000_clocking_scheme
 */
void _radio_dw1000_configure_clocking( t_ttc_radio_config* Config, e_radio_dw1000_clocking_scheme ClockSource );

/** Configure SPI bus device used to communicate with DW1000
 *
 * @param Config    configuration structure retrieved from ttc_radio_get_configuration()
 * @param Speed     DW1000 can only communicate at minimum speed during wakeup state
 */
void _radio_dw1000_configure_spi( t_ttc_radio_config* Config, e_radio_dw1000_spi_speed Speed );

/** configure frequency synthesizer for given channel number
 *
 * Note: Call this when changing current RX- or TX-channel!
 *
 * @param Config    configuration structure retrieved from ttc_radio_get_configuration()
 * @param ChannelNo  1,2,3,4,5,7
 */
void _radio_dw1000_configure_synthesizer( t_ttc_radio_config* Config, t_u8 ChannelNo );

/** derive channel parameters from given virtual channel
 *
 * @param VirtualChannel  one from e_radio_dw1000_virtual_channel
 * @param ChannelNo        !=NULL: pointer to where to store channel number (1,2,3,4,5,7)
 * @param Frequency_100KHz !=NULL: pointer to where to store channel frequency (unit is 100kHz)
 * @param Bandwidth_100KHz !=NULL: pointer to where to store channel bandwitdh (unit is 100kHz)
 * @param PreambleCode     !=NULL: pointer to where to store channel Preamble Code (1..20)
 */
void _radio_dw1000_decode_channel( e_radio_dw1000_virtual_channel VirtualChannel, t_u8* ChannelNo, t_u32* Frequency_100KHz, t_u32* Bandwidth_100KHz, t_u8* PreambleCode );

/** Complete reinitialization of DW1000 after powerup
 *
 * Note: SPI communication to DW1000 bus must be available before calling this function!
 *
 * @param Config               radio configuration as returned by ttc_radio_get_configuration()
 */
void _radio_dw1000_init_dw1000( t_ttc_radio_config* Config );

/** read packet being received from radio receive queue an append it to List_PacketsRx
 *
 * @param Config                                radio configuration as returned by ttc_radio_get_configuration()
 */
void _radio_dw1000_isr_read_received_packet( t_ttc_radio_config* Config );

/** transmit next packet from List_PacketsTx
 *
 * @param Config         radio configuration as returned by ttc_radio_get_configuration()
 * @param TransmitTime   !=NULL: time when to start transmission; ==NULL: start transmission now
 */
void _radio_dw1000_isr_transmit_next( t_ttc_radio_config* Config, u_ttc_packetimestamp_40* TransmitTime );

/** generate software interrupt to run interrupt service routine
 *
 * Note: You should always call this function instead of generating the interrupt
 *       manually as it can monitor correctness of isr.
 *
 * @param Config         radio configuration as returned by ttc_radio_get_configuration()
 */
void _radio_dw1000_isr_raise( t_ttc_radio_config* Config );

typedef enum { // Addresses in OTP memory
    rdoa_LDOTUNE = 0x04,
    rdoa_PARTID  = 0x06,
    rdoa_LOTID   = 0x07,
    rdoa_VBAT    = 0x08,
    rdoa_VTEMP   = 0x09,
    rdoa_TXCFG   = 0x10,
    rdoa_ANTDLY  = 0x1C,
    rdoa_XTRIM   = 0x1E
} e_radio_dw1000_otp_address;

t_u32 _radio_dw1000_read_otp( t_ttc_radio_config* Config, e_radio_dw1000_otp_address Address );

/** perform software reset of receiver after error condition
 *
 * Due to an issue in the re-initialisation of the receiver, it is necessary to apply a receiver reset after
 * an error or timeout event (i.e. RXPTO (Preamble detection Timeout), RXSFDTO (SFD timeout), RXPHE (PHY
 * Header Error), RXRFSL (Reed Solomon error), RXRFTO (Frame wait timeout), etc.). This ensures that the
 * next good frame will have correctly calculated timestamp. For details on how to apply a receiver-only
 * reset see SOFTRESET field of Sub-Register 0x36:00 – PMSC_CTRL0.
 *
 * -> DW1000 User Manual p. 34
 *
 * @param Config   radio configuration as returned by ttc_radio_get_configuration()
 */
void _radio_dw1000_reset_receiver( t_ttc_radio_config* Config );

/** read data via SPI from any DW1000 device register
 *
 * Notes:
 *   1. Firstly we create a header (the first byte is a header byte)
 *   a. check if sub index is used, if subindexing is used - set bit-6 to 1 to signify that the sub-index address follows the register index byte
 *   b. set bit-7 (or with 0x80) for write operation
 *   c. if extended sub address index is used (i.e. if index > 127) set bit-7 of the first sub-index byte following the first header byte
 *
 *   2. Write the header followed by the data bytes to the DW1000 device
 *
 *
 * @param Config        configuration structure retrieved from ttc_radio_get_configuration()
 * @param Register      24 bit value of format 0xAAOOOO (AA = register address, OOOO = register offset)
 * @param length        number of bytes being written
 * @param Buffer        pointer to buffer containing the 'length' bytes to be written
 * @return DWT_SUCCESS for success, or DWT_ERROR for error
 */
void _radio_dw1000_spi_register_read( t_ttc_radio_config* Config, e_radio_dw1000_register Register, t_u32 Length, t_u8* Buffer );

/** write generic data via SPI into DW1000 device registers (no endianess reordering)
 *
 * Notes:
 *        1. First we create a header (the first byte is a header byte)
 *        a. check if sub index is used, if subindexing is used - set bit-6 to 1 to signify that the sub-index address follows the register index byte
 *        b. set bit-7 (or with 0x80) for write operation
 *        c. if extended sub address index is used (i.e. if index > 127) set bit-7 of the first sub-index byte following the first header byte
 *
 *        2. Write the header followed by the data bytes to the DW1000 device
 *
 * @param Config        configuration structure retrieved from ttc_radio_get_configuration()
 * @param Register      24 bit value of format 0xAAOOOO (AA = register address, OOOO = register offset)
 * @param Buffer        pointer to buffer containing the 'length' bytes to be written
 */
void _radio_dw1000_spi_register_write( t_ttc_radio_config* Config, e_radio_dw1000_register Register, t_u32 Length, const t_u8* Buffer );

/** read + reset SYS_STATUS register of DW1000
 *
 * @param Config  radio configuration as returned by ttc_radio_get_configuration()
 * @return        pointer to local copy of register content (-> & Config->LowLevelConfig.Registers.SYS_STATUS)
 */
u_radio_dw1000_register_sys_status* _radio_dw1000_read_sys_status( t_ttc_radio_config* Config );

/** perform a hard reset of DW1000 by pulling its reset line low
 *
 * @param Config                                radio configuration as returned by ttc_radio_get_configuration()
 */
void _radio_dw1000_reset_hard( t_ttc_radio_config* Config );

/** Perform a software reset of TX, RX, Host Interface and PMSC
 *
 * Note: This will not reset the AON block
 * -> DW1000 User Manual p. 182
 *
 * @param Config                                radio configuration as returned by ttc_radio_get_configuration()
 */
void _radio_dw1000_reset_soft( t_ttc_radio_config* Config );

/** synchronize rx buffer pointers to ensure that the host/IC buffer pointers are aligned before starting RX
 *
 * @param Config        configuration structure retrieved from ttc_radio_get_configuration()
 */
void _radio_dw1000_synchronize_receive_buffer( t_ttc_radio_config* Config );

/** tune receiver according to latest received frame
 *
 * Note: Must read register RX_FCTRL before calling this function!
 */
void _radio_dw1000_tune_receiver( t_ttc_radio_config* Config, e_radio_dw1000_datarates Rate );

/** Universal delay to be used from interrupt service routine and user space.
 *
 * @param Delay_us  time to wait (microseconds)
 */
void _radio_dw1000_udelay( t_base Delay_us );

/** interrupt service routine aware wrapper to ttc_systick_delay_init() or ttc_systick_delay_init_isr()
 */
void _radio_dw1000_delay_init( t_ttc_systick_delay* Delay, t_base Time_us );

/** interrupt service routine aware wrapper to ttc_systick_delay_expired() or ttc_systick_delay_expired_isr()
 */
BOOL _radio_dw1000_delay_check( t_ttc_systick_delay* Delay );

/** read 40-bit register value from DW1000 device (reorders bystes for correct endianess)
 *
 * @param Config     configuration structure retrieved from ttc_radio_get_configuration()
 * @param Register   24 bit value of format 0xAAOOOO (AA = register address, OOOO = register offset)
 * @param Buffer     5 byte buffer where to store register content
 * @return  register content has been written into Buffer[0..4]
 */
void  _radio_dw1000_register_read_40bit( t_ttc_radio_config* Config, e_radio_dw1000_register Register, t_u8* Buffer );

/** read 32-bit register value from DW1000 device (reorders bystes for correct endianess)
 *
 * @param Config     configuration structure retrieved from ttc_radio_get_configuration()
 * @param Register   24 bit value of format 0xAAOOOO (AA = register address, OOOO = register offset)
 * @return register value
 */
t_u32 _radio_dw1000_register_read_32bit( t_ttc_radio_config*   Config, e_radio_dw1000_register Register );

/** read 16-bit register value from DW1000 device (reorders bystes for correct endianess)
 *
 * @param Config      configuration structure retrieved from ttc_radio_get_configuration()
 * @param Register    24 bit value of format 0xAAOOOO (AA = register address, OOOO = register offset)
 * @return register value
 */
t_u16 _radio_dw1000_register_read_16bit( t_ttc_radio_config*   Config, e_radio_dw1000_register Register );

/** read 8-bit register value from DW1000 device (reorders bystes for correct endianess)
 *
 * @param Config      configuration structure retrieved from ttc_radio_get_configuration()
 * @param Register    24 bit value of format 0xAAOOOO (AA = register address, OOOO = register offset)
 * @return register value
 */
t_u8  _radio_dw1000_register_read_8bit( t_ttc_radio_config* Config, e_radio_dw1000_register Register );

/** read all important registers from DW1000 into local variables
 *
 * Note: This function reads only registers stored in Config->LowLevelConfig.Registers.
 *       Special registers like OTP_*, those only relevant after frame receive and some other
 *       rarey used once are not read. See implementation for details.
 *
 * @param Config      configuration structure retrieved from ttc_radio_get_configuration()
 * @return            pointer to Config->LowLevelConfig.Registers where register contents have been stored
 */
t_radio_dw1000_registers* _radio_dw1000_register_read_all( t_ttc_radio_config* Config );

/** write given 40-bit value into the DW1000 device register (reorders bystes for correct endianess)
 *
 * @param Config      configuration structure retrieved from ttc_radio_get_configuration()
 * @param Register    24 bit value of format 0xAAOOOO (AA = register address, OOOO = register offset)
 * @param Value       5 bytes buffer storing 40 bit value to write into register
 */
void  _radio_dw1000_register_write_40bit( t_ttc_radio_config* Config, e_radio_dw1000_register Register, t_u8* Value );

/** write given 32-bit value into the DW1000 device register (reorders bystes for correct endianess)
 *
 * @param Config      configuration structure retrieved from ttc_radio_get_configuration()
 * @param Register    24 bit value of format 0xAAOOOO (AA = register address, OOOO = register offset)
 * @param Value       new value to write into register
 */
void _radio_dw1000_register_write_32bit( t_ttc_radio_config* Config, e_radio_dw1000_register Register, t_u32 Value );

/** write 16-bit value into DW1000 device register (reorders bystes for correct endianess)
 *
 * @param Register      24 bit value of format 0xAAOOOO (AA = register address, OOOO = register offset)
 * @param Value         new value to write into register
 */
void _radio_dw1000_register_write_16bit( t_ttc_radio_config* Config, e_radio_dw1000_register Register, t_u16 Value );

/** write 8-bit value into DW1000 device register (reorders bystes for correct endianess)
 *
 * @param Register      24 bit value of format 0xAAOOOO (AA = register address, OOOO = register offset)
 * @param Value         new value to write into register
 */
void _radio_dw1000_register_write_8bit( t_ttc_radio_config* Config, e_radio_dw1000_register Register, t_u8 Value );

/** Immediately abort current transmission
 *
 * @param Config         radio configuration as returned by ttc_radio_get_configuration()
 */
void _radio_dw1000_tx_abort( t_ttc_radio_config* Config );

/** prepare transmission of given packet
 *
 * Will load goven Packet into transmission buffer and configure transmitter.
 * Call _radio_dw1000_tx_start() afterwards to start prepared transmission.
 *
 * Note: This function may be called from application or interrupt service routine.
 *
 * @param Config         radio configuration as returned by ttc_radio_get_configuration()
 * @param Packet         network packet to transmit
 */
void _radio_dw1000_tx_prepare( t_ttc_radio_config* Config, t_ttc_packet* Packet );

/** start a previously prepared transmission
 *
 * @param Config                radio configuration as returned by ttc_radio_get_configuration()
 * @param TransmitTime          !=NULL: time when to start transmission; ==NULL: start transmission now
 * @param PacketMeta            pointer to packet meta header where to store transmission status (PacketMeta->StatusTX will be updated)
 * @return                      ==0: transmission started successfully; error code otherwise
 */
void _radio_dw1000_tx_start( t_ttc_radio_config* Config, u_ttc_packetimestamp_40* TransmitTime, t_ttc_packet_meta* PacketMeta );

/** helper function that will compile a 32 bit value usable as DW1000 spi transaction header
 *
 * @param RegisterIndex     0..63 register index       (-> DW1000 UserManual)
 * @param RegisterSubIndex  15 bit register sub index  (-> DW1000 UserManual)
 * @param WriteOperation    ==TRUE: SPI write operation; read operation otherwise
 * @return compiled 32 bit value
 */
t_radio_dw1000_transaction _radio_dw1000_compile_transaction_header( t_u8 RegisterIndex, t_u16 RegisterSubIndex, BOOL WriteOperation );

/** write all registers to DW1000 that are marked as changed
 *
 * @param Config         radio configuration as returned by ttc_radio_get_configuration()
 * @param Config->LowLevelConfig.Changed  will be cleared automatically
 */
void _radio_dw1000_register_write_changed( t_ttc_radio_config* Config );

//InsertPrivateFunctionDeclarations

//}Private Function Declarations
//{ Public Function Definitions *******************************************************

void                     radio_dw1000_change_local_address( t_ttc_radio_config* Config, const t_ttc_packet_address* LocalAddress ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_RADIO_EXTRA_Readable( LocalAddress, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    if ( LocalAddress ) { // send new local address to dw1000
        ttc_memory_copy( & Config->LocalID, LocalAddress, sizeof( * LocalAddress ) );

        _radio_dw1000_register_write_SHORT_ADDR( Config );
        _radio_dw1000_register_write_EUI64( Config );
        _radio_dw1000_register_write_PAN_ID( Config );
    }
}
void                     radio_dw1000_change_delay_time( t_ttc_radio_config* Config, t_u16 Delay_uSeconds ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    // using smart multiply-divde function to avoid overflow
    t_u32 Delay = ttc_basic_multiply_divide_u32x3( Delay_uSeconds,
                                                   1000000,
                                                   Config->Features->DelayTimerPeriod_fs
                                                 );

    Config->LowLevelConfig.DelayAdd_RX2TX = Delay;
    Config->LowLevelConfig.DelayAdd_TX2RX = Delay;
}
void                     radio_dw1000_configuration_check( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    // disabling all unsupported features
    Config->Init.Flags.UseAlternateTxPath    = 0;
    Config->Init.Flags.EnableDoubleBuffering = 0; // ToDo: implement

}
BOOL                     radio_dw1000_configure_auto_acknowledge( t_ttc_radio_config* Config, BOOL VOLATILE_RADIO Enable, BOOL VOLATILE_RADIO Change ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    // SYS_CFG points to last value being sent to DW1000
    u_radio_dw1000_register_sys_cfg* SYS_CFG = &( Config->LowLevelConfig.Registers.SYS_CFG );

    if ( !SYS_CFG->Fields.FFEN )
    { Enable = 0; } // no auto acknowledge if frame filter is disabled (->DW1000 UserManual p.50)

    if ( Change ) { // send new auto acknowledge setting to transceiver
        SYS_CFG->Fields.AUTOACK = Enable;
        _radio_dw1000_register_write_SYS_CFG( Config );
    }
    else {          // read auto acknowledge setting from local cache
        Enable = SYS_CFG->Fields.AUTOACK;
    }

    return Enable;
}
e_ttc_radio_errorcode    radio_dw1000_configure_channel_rxtx( t_ttc_radio_config* Config, t_u8 NewChannel ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    // Using this pointer shortens code lines
    t_radio_dw1000_registers* Registers = &( Config->LowLevelConfig.Registers );
    t_radio_dw1000_changed*   Changed   = &( Config->LowLevelConfig.Changed );

    // Original implementation:
    // -> radio_dw1000_configure_parameters( Config, DWT_LOADUCODE | DWT_LOADXTALTRIM ); //| DWT_LOADANTDLY*/);

    t_u8  ChannelNo;
    t_u32 Bandwidth_100KHz;
    t_u8  PreambleCode;

    Config->ChannelTX = Config->ChannelRX = NewChannel;
    _radio_dw1000_decode_channel( Config->ChannelRX,
                                  &ChannelNo,
                                  NULL,
                                  &Bandwidth_100KHz,
                                  &PreambleCode
                                );

    Assert_RADIO_EXTRA( PreambleCode < RADIO_DW1000_AMOUNT_PREAMBLE_CODES, ttc_assert_origin_auto );
    Assert_RADIO_EXTRA( ChannelNo    < 8, ttc_assert_origin_auto );
    Assert_RADIO_EXTRA( ChannelNo   != 6, ttc_assert_origin_auto );

    if ( 1 ) { // configure Receiver for new channel
        if ( 1 ) { // configure Leading Edge Detection for new channel (-> DW1000 UserManual p.169)
            if ( Config->LowLevelConfig.DataRate == radio_dw1000_datarate_110k ) {
                changeRegister( LDE_REPC, RADIO_DW1000_LDE_REPLICA_COEFFICIENT[PreambleCode] / 8 );
            }
            else {
                changeRegister( LDE_REPC, RADIO_DW1000_LDE_REPLICA_COEFFICIENT[PreambleCode] );
            }

            // configure LDE according to current Receiver PRF Frequency (-> DW1000 UserManual p.169)
            if ( Registers->CHAN_CTRL.Fields.RXPRF == rd_chan_ctrl_prf_16MHz ) {
                changeRegister( LDE_CFG2, 0x1607 ); // configure LDE for 16MHz PRF
                changeRegister( LDE_RXANTD, Config->LowLevelConfig.OTP_Data.AntennaDelay_PRF16 );
            }
            else {
                changeRegister( LDE_CFG2, 0x0607 ); // configure LDE for 64MHz PRF
                changeRegister( LDE_RXANTD, Config->LowLevelConfig.OTP_Data.AntennaDelay_PRF64 );
            }
        }
        if ( 1 ) { // Analog RX Control
            switch ( ChannelNo ) { // set RF_RXCTRLH (-> DW1000 UserManual p141
                case 1:
                case 2:
                case 3:
                case 5:
                    changeRegister( RF_RXCTRLH, 0xd8 );
                    break;
                default:
                    changeRegister( RF_RXCTRLH, 0xbc );
                    break;
            }
        }
        if ( 1 ) { // switch channel
            changeRegisterField( CHAN_CTRL, RX_CHAN, ChannelNo );
            changeRegisterField( CHAN_CTRL, RX_PCODE, PreambleCode );
        }
    }
    if ( 1 ) { // configure Transmitter for new TX-channel
        changeRegisterField( CHAN_CTRL, TX_CHAN,  ChannelNo );
        changeRegisterField( CHAN_CTRL, TX_PCODE, PreambleCode );

        const static volatile t_u32 radio_dw1000_RF_TXCTRL[] = { // values for register RX_TXCTRL -> DW1000 UserManual p.141
            0x00005c40, // TX channel 1
            0x00045ca0, // TX channel 2
            0x00086cc0, // TX channel 3
            0x00045c80, // TX channel 4
            0x001e3fe0, // TX channel 5
            0x0,
            0x001e7de0  // TX channel 7
        };

        // load antenna delay foir current pulse repetition frequency
        if ( Registers->TX_FCTRL.Fields.TXPRF == rd_chan_ctrl_prf_16MHz ) {
            changeRegister( TX_ANTD, Config->LowLevelConfig.OTP_Data.AntennaDelay_PRF16 );
        }
        else {
            changeRegister( TX_ANTD, Config->LowLevelConfig.OTP_Data.AntennaDelay_PRF64 );
        }
        Config->TransmitTime_AntennaDelay = Registers->TX_ANTD;


        changeRegister( RF_TXCTRL, radio_dw1000_RF_TXCTRL[ChannelNo - 1] );
    }
    _radio_dw1000_register_write_changed( Config );
    _radio_dw1000_configure_synthesizer( Config, ChannelNo );

    return ec_radio_OK;
}
e_ttc_radio_errorcode    radio_dw1000_deinit( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    if ( Config->Flags.Initialized ) {
        _radio_dw1000_reset_hard( Config );
    }

    return ( e_ttc_radio_errorcode ) 0;
}
u_ttc_radio_frame_filter radio_dw1000_configure_frame_filter( t_ttc_radio_config* Config, u_ttc_radio_frame_filter Settings, t_u8 ChangeFilter ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    // SYS_CFG points to last value being sent to DW1000
    u_radio_dw1000_register_sys_cfg* SYS_CFG = &( Config->LowLevelConfig.Registers.SYS_CFG );

    if ( ChangeFilter ) {
        SYS_CFG->Fields.FFBC = Settings.Allowed.WithoutDestinationID;
        SYS_CFG->Fields.FFAB = Settings.Allowed.Beacon;
        SYS_CFG->Fields.FFAD = Settings.Allowed.Data;
        SYS_CFG->Fields.FFAA = Settings.Allowed.Acknowledge;
        SYS_CFG->Fields.FFAM = Settings.Allowed.MAC_Control;
        SYS_CFG->Fields.FFAR = Settings.Allowed.ReservedTypes;
        SYS_CFG->Fields.FFA4 = 0; // ToDo: support type 4 frames
        SYS_CFG->Fields.FFA5 = 0; // ToDo: support type 5 frames

        // enable filter if any setting is set
        if ( Settings.All ) { // enable filter
            SYS_CFG->Fields.FFEN = 1;
            SYS_CFG->Fields.AUTOACK = Config->Init.Flags.AutoAcknowledge; // bit may have been reset
        }
        else {                // disable frame filter
            SYS_CFG->Fields.FFEN    = 0;
            SYS_CFG->Fields.AUTOACK = 0; // no auto ack with disabled filter
        }

        _radio_dw1000_register_write_SYS_CFG( Config );
    }
    if ( 1 ) {              // read last frame filter settings
        Settings.All = 0;

        if ( SYS_CFG->Fields.FFEN ) {
            Settings.Allowed.WithoutDestinationID = SYS_CFG->Fields.FFBC;
            Settings.Allowed.Beacon               = SYS_CFG->Fields.FFAB;
            Settings.Allowed.Data                 = SYS_CFG->Fields.FFAD;
            Settings.Allowed.Acknowledge          = SYS_CFG->Fields.FFAA;
            Settings.Allowed.MAC_Control          = SYS_CFG->Fields.FFAM;
            Settings.Allowed.ReservedTypes        = SYS_CFG->Fields.FFAR;
        }
    }
    ttc_task_check_stack(); // a good place to check for a stack overrun

    return Settings;
}
BOOL                     radio_dw1000_gpio_out( t_ttc_radio_config* Config, e_ttc_radio_gpio Pin, BOOL State ) {
    t_u8 PinNo = 0;
    switch ( Pin ) { // check if gpio pin is supported
        case E_ttc_radio_gpio_status_tx: PinNo = 3;    break;
        case E_ttc_radio_gpio_status_rx: PinNo = 2;    break;
        default:                         return FALSE; break; // given gpio pin not supported
    }

    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    t_radio_dw1000_registers* Registers = &( Config->LowLevelConfig.Registers );

    t_u32 DOUT = ( State ) ? 0b10001 : 0b10000;
    static const t_u8 rdgo_Shift[] = { 0, 1, 2, 3, 8, 9, 10, 11, 16 };
    Assert_RADIO( PinNo < sizeof( rdgo_Shift ), ttc_assert_origin_auto );  // Pin must be 0..8 !
    t_u8 Shift = rdgo_Shift[PinNo];

    Registers->GPIO_DOUT.Word = DOUT << Shift;

    if ( Registers->GPIO_MODE.Word & ( 0b11 << ( 6 + ( 2 * PinNo ) ) ) ) {
        Registers->GPIO_MODE.Word &=  ~( 0b11 << ( 6 + ( 2 * PinNo ) ) );
        _radio_dw1000_register_write_GPIO_MODE( Config );
    }
    if ( Registers->GPIO_DIR.Word  & ( 0b1  << ( Shift ) ) ) {
        Registers->GPIO_DIR.Word &=  ~( 0b1  << ( Shift ) );
        _radio_dw1000_register_write_GPIO_DIR( Config );
    }

    Assert_RADIO_EXTRA( ( Registers->GPIO_MODE.Word & ( 0b11 << ( 6 + ( 2 * PinNo ) ) ) ) == 0, ttc_assert_origin_auto );  // selected GPIO pin is not configured as GPIO!
    Assert_RADIO_EXTRA( ( Registers->GPIO_DIR.Word  & ( 0b1  << ( Shift ) ) ) == 0, ttc_assert_origin_auto );  // selected GPIO pin is not configured as output!

    _radio_dw1000_register_write_GPIO_DOUT( Config );

    return TRUE;
}
e_ttc_radio_errorcode    radio_dw1000_init( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    t_radio_dw1000_registers* Registers = &( Config->LowLevelConfig.Registers );
    t_radio_dw1000_changed*   Changed   = &( Config->LowLevelConfig.Changed );

    // clear local copy of register set
    ttc_memory_set( Registers, 0, sizeof( *Registers ) );
    ttc_mutex_init( & ( Config->FlagRunningTX ) );

    if ( Config->LowLevelConfig.Pin_RESET ) { // reset pin available: configure corresponding GPIO
        ttc_gpio_init( Config->LowLevelConfig.Pin_RESET, E_ttc_gpio_mode_output_open_drain, E_ttc_gpio_speed_min );
        ttc_gpio_set( Config->LowLevelConfig.Pin_RESET );
    }

    // Configure SPI bus + establish connection to DW1000 (will reset DW1000 on first call)
    Config->LowLevelConfig.SpeedSPI = 0;
    _radio_dw1000_configure_spi( Config, radio_dw1000_spi_speed_Min );

    // adjust transceiver bitrate to valid value
    if ( Config->Init.RawBitrate <= 110000 ) {
        Config->Init.RawBitrate = 110000;
        Config->LowLevelConfig.DataRate = radio_dw1000_datarate_110k;
    }
    else if ( Config->Init.RawBitrate <= 850000 ) {
        Config->Init.RawBitrate = 850000;
        Config->LowLevelConfig.DataRate = radio_dw1000_datarate_850k;
    }
    else {
        Config->Init.RawBitrate = 6800000;
        Config->LowLevelConfig.DataRate = radio_dw1000_datarate_6M8;
    }
    changeRegisterField( TX_FCTRL, TXBR, Config->LowLevelConfig.DataRate );

    if ( 1 ) { // static configuration of certain radio specific settings not configurable via ttc_radio

        // Changing default configuration according to DW1000 UserManual p.22

        // transmitter/ receiver pulse repetion frequency
        changeRegisterField( CHAN_CTRL, RXPRF, rd_chan_ctrl_prf_64MHz );
        changeRegisterField( TX_FCTRL,  TXPRF, rd_chan_ctrl_prf_64MHz );

        // optimized LDE settings
        changeRegisterField( LDE_CFG1, NTM, 0xd );

        // optimized transmitter power setting
        changeRegister( TX_POWER, 0x0e082848 );

        if ( Config->Init.MaxPacketSize > 127 ) {
            if ( Config->Init.MaxPacketSize > 1023 )
            { Config->Init.MaxPacketSize = 1023; }
            changeRegisterField( SYS_CFG, PHR_MODE, radio_dw1000_mps_1023_bytes ); // using Non-standard headers for larger frames
        }
        else {
            changeRegisterField( SYS_CFG, PHR_MODE, radio_dw1000_mps_127_bytes ); // using IEEE 802.15.4 compliant headers
        }  // using IEEE 802.15.4 compliant headers

        TODO( "test individual preamble lengths in real setup!" )
        if ( Config->Init.LevelTX == Config->Features->MaxTxLevel ) { // select preamble length for max range
            switch ( Config->Init.RawBitrate ) { // choosing preamble length (-> DW1000 UserManual p.196)
                case 110000:
                    changeRegisterField( TX_FCTRL, TXPREAMBLE, radio_dw1000_pl_4096 ); // longer preamble increases range
                    break;
                case 850000:
                    changeRegisterField( TX_FCTRL, TXPREAMBLE, radio_dw1000_pl_1024 ); // longer preamble increases range
                    break;
                case 6800000:
                    changeRegisterField( TX_FCTRL, TXPREAMBLE, radio_dw1000_pl_256 );  // longer preamble increases range
                    break;
                default: ttc_assert_halt_origin( ec_radio_InvalidImplementation ); break; // RawBitrate should already been adjusted to a supported value
            }
        }
        else {                                                        // select preamble length for intermediate range (less energy, higher data throughput)
            switch ( Config->Init.RawBitrate ) { // choosing preamble length (-> DW1000 UserManual p.196)
                case 110000:
                    changeRegisterField( TX_FCTRL, TXPREAMBLE, radio_dw1000_pl_2048 ); // longer preamble increases range
                    break;
                case 850000:
                    changeRegisterField( TX_FCTRL, TXPREAMBLE, radio_dw1000_pl_256 ); // longer preamble increases range
                    break;
                case 6800000:
                    changeRegisterField( TX_FCTRL, TXPREAMBLE, radio_dw1000_pl_64 ); // longer preamble increases range
                    break;
                default: ttc_assert_halt_origin( ec_radio_InvalidImplementation ); break; // RawBitrate should already been adjusted to a supported value
            }
        }

        // transmit new configuration to DW1000
        _radio_dw1000_register_write_changed( Config );

        // configure which configuration parts to load from One Time Programmable memory
        Config->LowLevelConfig.LoadFromOTP.Bits.LDE_Config   = 1;
        Config->LowLevelConfig.LoadFromOTP.Bits.XtalTrim     = 1;
        Config->LowLevelConfig.LoadFromOTP.Bits.TX_Config    = 1;
        Config->LowLevelConfig.LoadFromOTP.Bits.AntennaDelay = 1;
        Config->LowLevelConfig.LoadFromOTP.Bits.LDO_TUNE     = 1;
        Config->LowLevelConfig.LoadFromOTP.Bits.IsValid      = 1; // must be set to mark configuration as valid
    }
    _radio_dw1000_init_dw1000( Config );

    if ( 1 ) { // load configured settings from Config (ChannelRX, ChannelTX, LocalID, LocalPanID)
        radio_dw1000_configure_channel_rxtx( Config, Config->ChannelRX );
        radio_dw1000_change_local_address( Config, & Config->LocalID );
    }
    if ( ( Config->LowLevelConfig.Pin_IRQ ) && ( Config->Init.Flags.Enable_Output_IRQ ) ) { // configure IRQ pin as external interrupt input
        ttc_gpio_init( Config->LowLevelConfig.Pin_IRQ, E_ttc_gpio_mode_input_pull_down, E_ttc_gpio_speed_min );

        Config->RadioInterrupt = ttc_interrupt_init( tit_GPIO_Rising,
                                                     Config->LowLevelConfig.Pin_IRQ,
                                                     ( void ( * )( t_physical_index, void* ) ) radio_dw1000_isr_generic,
                                                     Config,
                                                     0, 0
                                                   );
        Assert_RADIO( Config->RadioInterrupt != NULL, ttc_assert_origin_auto );  // could not initialize interrupt (configured GPIO pin not usable for external interrupts on current uC architecture?)

        // we have to re-enable interrupts to ensure that interrupt service routine is run now
        t_u8 PreviousCriticalLevel = ttc_interrupt_critical_end_all();
        while ( ttc_interrupt_critical_level() ) { // leave all critical levels to re-enable interrupts
            ttc_interrupt_critical_end();
            PreviousCriticalLevel++;
        }

        // enable external interrupt
        ttc_interrupt_enable_gpio( Config->RadioInterrupt );

        // generate software interrupt to run interrupt service routine at least once
        _radio_dw1000_isr_raise( Config );

        if ( PreviousCriticalLevel )
        { ttc_interrupt_critical_restore( PreviousCriticalLevel ); }
    }

    return ec_radio_OK;
}
void                     radio_dw1000_isr_generic( t_physical_index Index, t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM
    Assert_RADIO_EXTRA( !Config->PacketTX_Meta || ttc_memory_is_writable( Config->PacketTX_Meta ), ttc_assert_origin_auto );  // invalid pointer found (must point to RAM)
    ( void ) Index; // not used

    radio_common_InsideISR = TRUE;
    radio_dw1000_isr_CurrentRxFrameMeta  = &( radio_dw1000_RxFrameMeta[radio_dw1000_RxMetaIndex] );
    radio_dw1000_isr_LowLevelConfig      = &( Config->LowLevelConfig );
    radio_dw1000_isr_Registers           = &( Config->LowLevelConfig.Registers );
    radio_dw1000_isr_TransmissionStarted = FALSE;
    BOOL VOLATILE_RADIO InterruptHandled = FALSE;
    RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_isr_started );

    static t_ttc_systick_delay TimeOut_Transmission; // detects transmission update timeout
    TimeOut_Transmission.TimeStart = 0;
    do {
        radio_dw1000_isr_SysStatus = _radio_dw1000_read_sys_status( Config );
        InterruptHandled = FALSE;
        if ( radio_dw1000_isr_SysStatus->Fields.IRQS ) {
#if RADIO_DW1000_AMOUNT_SYS_STATUS  > 0
            if ( 1 ) { // store current SysStatus for later analysis
                if ( radio_dw1000_isr_Index_Memo_SysStatus < RADIO_DW1000_AMOUNT_SYS_STATUS )
                { radio_dw1000_isr_Index_Memo_SysStatus++; }
                else
                { radio_dw1000_isr_Index_Memo_SysStatus = 0; }

                radio_dw1000_isr_Memo_SysStatus[radio_dw1000_isr_Index_Memo_SysStatus].LowWord  = radio_dw1000_isr_SysStatus->LowWord;
                radio_dw1000_isr_Memo_SysStatus[radio_dw1000_isr_Index_Memo_SysStatus].Bytes[4] = radio_dw1000_isr_SysStatus->Bytes[4];
            }
#endif
            if ( radio_dw1000_isr_SysStatus->Fields.TXFRB )  { // transmitter has begun to send preamble
                _RADIO_DW1000_LED_TX( 1 );
                InterruptHandled = 1;
                Config->PacketTX_Meta->StatusTX = tpst_Info_TxPreamble;
                Config->StatusTX = E_ttc_radio_status_tx_started;
                RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_transmit_running );
            }
            if ( radio_dw1000_isr_SysStatus->Fields.TXPRS )  { // preamble sent
                if ( Config->PacketTX_Meta ) {
                    Config->PacketTX_Meta->StatusTX = tpst_Info_TxPreamble;
                }

                InterruptHandled = 1;
            }
            if ( radio_dw1000_isr_SysStatus->Fields.TXPHS )  { // MAC header sent
                if ( Config->PacketTX_Meta ) {
                    Config->PacketTX_Meta->StatusTX = tpst_Info_TxHeaderMAC;
                }

                InterruptHandled = 1;
            }
            if ( radio_dw1000_isr_SysStatus->Fields.TXFRS )  { // frame sent successfully
                if ( radio_dw1000_isr_Registers->TX_FCTRL.Fields.TR ) { // packet had ranging bit set: store its transmit timestamp
                    _radio_dw1000_register_read_40bit( Config,
                                                       rdr_TX_STAMP,
                                                       Config->LastTransmitTime.Bytes
                                                     );
                }
                _RADIO_DW1000_LED_TX( 0 );
                RADIO_COMMON_COUNT( Amount_Tx );
                if ( Config->PacketTX_Meta ) {
                    Config->PacketTX_Meta->StatusTX = tpst_OK_TxComplete;
                    Config->PacketTX_Meta = NULL;  // prevent further accesses to this address (buffer may now be reused by application)
                }
                Config->StatusTX = E_ttc_radio_status_tx_complete;

                ttc_mutex_unlock_isr( &( Config->FlagRunningTX ) ); // unlock mutex (required by _radio_dw1000_isr_transmit_next() )

                ttc_mutex_unlock_isr( &( Config->Flag_ReceiverOn ) );
                InterruptHandled = 1;
                RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_transmit_complete );
            }
            if ( radio_dw1000_isr_SysStatus->Fields.RXPRD )  { // Receiver Preamble Detected
                if ( Config->Init.function_preamble_detected_isr )
                { Config->Init.function_preamble_detected_isr( Config ); } // call activity function
                InterruptHandled = 1;
                RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_receive_preamble );
            }
            if ( radio_dw1000_isr_SysStatus->Fields.RXDFR )  { // received data frame decoded successfully
                InterruptHandled = 1;
                RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_receive_frame );
            }
            if ( radio_dw1000_isr_SysStatus->Fields.RXFCG )  { // checksum was OK: read received frame
                _RADIO_DW1000_LED_RX( 1 );
                ttc_mutex_unlock_isr( &( Config->Flag_ReceiverOn ) ); // receiver has been switched off automatically
                if ( Config->Init.function_start_rx_isr )
                { Config->Init.function_start_rx_isr( Config ); } // call activity function
                _radio_dw1000_isr_read_received_packet( Config );
                RADIO_COMMON_COUNT( Amount_Rx );
                InterruptHandled = 1;
                RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_receive_ok );
            }
            if ( radio_dw1000_isr_SysStatus->Fields.CPLOCK ) { // PLL has locked in
                // we may increase SPI-speed now
                InterruptHandled = 1;
                RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_pll_locked );
            }
            if ( radio_dw1000_isr_SysStatus->Fields.AAT )    { // Automatic Acknowledge has triggerted
                InterruptHandled = 1;
            }

            const u_radio_dw1000_register_sys_status radio_dw1000_MaskErrorCondition = {
                .Fields.RXPTO    = 1, // Receiver Preamble Detection Error
                .Fields.RXSFDTO  = 1, // Receiver Start of Frame Detection Timeout
                .Fields.RXPHE    = 1, // Receiver MAC Header Error
                .Fields.RXRFSL   = 1, // Receiver Reed Solomon Frame Sync Loss
                .Fields.RXRFTO   = 1, // Receiver Frame Wait Timeout
                .Fields.RXFCE    = 1, // Receiver Frame Checksum Error
                .Fields.RXOVRR   = 1, // Receiver Buffer Overrun
                .Fields.LDEERR   = 1, // Receiver Leading Edge Detector Error
                .Fields.HPDWARN  = 1, // Half Period Delay Warning
                .Fields.AFFREJ   = 1, // Receiver MAC Header Error
                .Fields.TXBERR   = 1, // data was sent to TX buffer too late (transmitter already consumed bytes from buffer)
                .Fields.HPDWARN  = 1, // configured period until delayed TX/RX is too short: application should issue TRXOFF to avoid waiting 8 secs until TX/RX !!!
                .Fields.TXPUTE   = 1, // transmitter has not egnough time to power up until next delayed TX
                .Fields.SLP2INIT = 1, // Device transitioned from SLEEP to INIT state
                .Fields.CLKPLL   = 1, // System Clock PLL Lock Loosing
                .Fields.RFPLL    = 1  // RF Clock PLL has locking issues
            };
            if ( radio_dw1000_isr_SysStatus->LowWord & radio_dw1000_MaskErrorCondition.LowWord ) { // error occured: have to reset receiver
                InterruptHandled = 1;
                RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_error_unknown );
                _RADIO_DW1000_LED_RX( 0 );
                _RADIO_DW1000_LED_TX( 0 );
                ttc_mutex_unlock_isr( &( Config->FlagRunningTX ) );
                radio_dw1000_isr_TransmissionStarted = FALSE;

                radio_dw1000_isr_Registers->SYS_CTRL.Word = 0;

                if ( radio_dw1000_isr_SysStatus->Fields.HPDWARN ) { // delayed transmit was too late: abort delayed TX
                    // switch off transmitter
                    radio_dw1000_isr_Registers->SYS_CTRL.Fields.TRXOFF = 1;
                    _radio_dw1000_register_write_SYS_CTRL( Config );
                    RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_transmit_too_late );
                }
                if ( radio_dw1000_isr_SysStatus->Fields.CLKPLL ) { // System Clock PLL has locking issues
                    RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_pll_error );
                }
                if ( Config->PacketTX_Meta && // report tx status
                        ( radio_dw1000_isr_SysStatus->Fields.TXBERR  ||
                          radio_dw1000_isr_SysStatus->Fields.HPDWARN ||
                          radio_dw1000_isr_SysStatus->Fields.TXPUTE
                        )
                   ) {
                    Config->StatusTX = E_ttc_radio_status_tx_too_late;
                    Config->PacketTX_Meta->StatusTX = tpst_Error_DelayedTxTooLate;
                    Config->PacketTX_Meta = NULL;  // prevent further accesses to this address (buffer may now be reused by application)
                }

                const u_radio_dw1000_register_sys_status radio_dw1000_MaskRxError = {
                    .Fields.RXPTO    = 1, // Receiver Preamble Detection Error
                    .Fields.RXSFDTO  = 1, // Receiver Start of Frame Detection Timeout
                    .Fields.RXPHE    = 1, // Receiver MAC Header Error
                    .Fields.RXRFSL   = 1, // Receiver Reed Solomon Frame Sync Loss
                    .Fields.RXRFTO   = 1, // Receiver Frame Wait Timeout
                    .Fields.RXFCE    = 1, // Receiver Frame Checksum Error
                    .Fields.RXOVRR   = 1, // Receiver Buffer Overrun
                    .Fields.LDEERR   = 1, // Receiver Leading Edge Detector Error
                };

                if ( radio_dw1000_isr_SysStatus->LowWord & radio_dw1000_MaskRxError.LowWord ) {
                    // receiver error: ignore packet
                    _radio_dw1000_reset_receiver( Config );
                    RADIO_COMMON_COUNT( Amount_FailedCRC );
                }
                TODO( "Handle RXOVRR (requires double buffering)" )

                if ( Config->Init.Flags.RxAutoReenable &&
                        ( ! ttc_mutex_is_locked( &( Config->FlagRunningTX ) ) )
                   ) { // no transmission ongoing: re-enable receiver
                    radio_dw1000_isr_Registers->SYS_CTRL.Fields.RXENAB = 1;
                    _radio_dw1000_register_write_SYS_CTRL( Config );
                    ttc_mutex_lock_isr( &( Config->Flag_ReceiverOn ) );
                }
                else
                { ttc_mutex_unlock_isr( &( Config->Flag_ReceiverOn ) ); } // receiver has been switched off automatically
            }
            if ( !InterruptHandled ) {
                radio_dw1000_isr_SysStatus->Fields.IRQS = 0; // software generated interrupts will have this bit set only
                Assert_RADIO( ( radio_dw1000_isr_SysStatus->LowWord == 0 ) && ( radio_dw1000_isr_SysStatus->Bytes[4] == 0 ), ttc_assert_origin_auto ); // interrupt was not handled. Add more cases to this isr!
            }
        }
        else { // no interrupt flags set: check if we may exit now
            radio_dw1000_isr_LowLevelConfig->Amount_EmptyISRs++;

            if ( ttc_mutex_is_locked( &( Config->FlagRunningTX ) ) ) { // transmission ongoing: stay in isr for update
                if ( Config->Init.Transceiver_Timeout_usecs ) { // transceiver timeout set: init or check timeout
                    if ( !TimeOut_Transmission.TimeStart ) { // init timeout
                        ttc_systick_delay_init_isr( &TimeOut_Transmission, Config->Init.Transceiver_Timeout_usecs );
                    }
                    else {
                        if ( ttc_systick_delay_expired_isr( &TimeOut_Transmission ) ) { // timeout while waiting for transmission update: abort transmission
                            TODO( "Find out why DW1000 sometimes does not generate an interrupt after starting a transmission!" )
                            TODO( "#warning ToDo: Instead of aborting transmission, we might try to restart it!" )
                            if ( Config->PacketTX_Meta ) {
                                Config->PacketTX_Meta->StatusTX = tpst_Error_TxNotStarted;
                                Config->PacketTX_Meta = NULL;  // prevent further accesses to this address (buffer may now be reused by application)
                            }
                            RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_transmit_not_started );
                            ttc_mutex_unlock_isr( &( Config->FlagRunningTX ) );
#if (TTC_RADIO_STATISTICS == 1)
                            Config->Statistics.Amount_FailedTx++;
#endif
                        }
                    }
                }
            }
        }
        if ( ! ttc_mutex_is_locked( &( Config->FlagRunningTX ) ) ) { // no transmission ongoing: check if we have packets to transmit
            if ( s_ttc_listize( & ( Config->List_PacketsTx ) ) ) { // still packets waiting to be transmitted: transmit next packet
                //?radio_dw1000_isr_LowLevelConfig->StatusISR = 0; //DEBUG
                _radio_dw1000_isr_transmit_next( Config, NULL );
                Assert_RADIO_EXTRA( radio_dw1000_isr_TransmissionStarted, ttc_assert_origin_auto ); // transmission was not started. Check why!
                if ( radio_dw1000_isr_TransmissionStarted ) {
                    TimeOut_Transmission.TimeStart = 0;
                    //? EmptyLoopsDuringTransmission = 0;
                }
            }
            else {                                                 // transmit queue empty: end transmission
                radio_dw1000_isr_TransmissionStarted = FALSE;
            }
        }
    }
    while ( ( ttc_gpio_get( radio_dw1000_isr_LowLevelConfig->Pin_IRQ ) ) // pending interrupt request from DW1000: handle it now

            TODO( "Staying inside isr loop while Config->FlagRunningTX is locked may slow down packet transmission, but tests showed unstable behaviour when following line is commented out." )
            || ( ttc_mutex_is_locked( &( Config->FlagRunningTX ) ) )    // transmission still ongoing
          );

    if ( Config->Init.Flags.RxAutoReenable && !ttc_mutex_is_locked( &( Config->Flag_ReceiverOn ) ) ) { // receiver is off: reenable it
        radio_dw1000_receiver( Config, TRUE, 0, 0 );
    }

    radio_common_InsideISR = FALSE;

    //X #warning implement handling of special error conditions as described below!
    /* original implementation from DecaWave Example (signatures of some functions have changed so it will not compile any more

    t_u32 status     = 0;
    t_u32 clear      = 0; // will clear any events seen
    t_u32 lde_loaded = 0; // We have to check if the lde is activated

    status = _radio_dw1000_register_read_32bit( Config, rdr_SYS_STATUS ) ;           // read status register low 32bits
    if ( radio_dw1000_InsertSleeps ) { ttc_task_msleep( 50 ); }

    t_u32 mask = _radio_dw1000_register_read_32bit( Config, 0x0E, 0 ) ;                // read status register low 32bits
    ( void ) mask; //? Used
    if ( radio_dw1000_InsertSleeps ) { ttc_task_msleep( 50 ); }

    lde_loaded = ( _radio_dw1000_register_read_32bit( Config, 0x36, 0x04 ) & 0x00020000 ); // check if LDE_RUN is activated
    if ( radio_dw1000_InsertSleeps ) { ttc_task_msleep( 50 ); }

    if ( status & SYS_STATUS_LDEERR ) {

    clear |= SYS_STATUS_LDEERR;
    _radio_dw1000_register_write_32bit( Config, rdr_SYS_STATUS, clear );        // write status register to clear event bits we have seen
    }
    //    if (status & SYS_STATUS_CPLOCK) {

    //        //radio_dw1000_FastSpi(0x0008);

    //        t_u32 ID;
    //        _radio_dw1000_spi_register_read(Radio, 0x0, 0, 4, &ID);
    //        Assert_RADIO(ID==(0xDECA0130), ttc_assert_origin_auto);

    //        clear |= SYS_STATUS_CPLOCK;
    //        _radio_dw1000_register_write_32bit(Radio, rdr_SYS_STATUS, &clear);         // write status register to clear event bits we have seen

    //    }

    //    if (status & SYS_STATUS_SLP2INIT) {

    //        clear |= SYS_STATUS_SLP2INIT;
    //        _radio_dw1000_register_write_32bit(Radio, rdr_SYS_STATUS, &clear);         // write status register to clear event bits we have seen

    //    }

    //NOTES:
    //1. TX Event - if DWT_INT_TFRS is enabled, then when the frame has completed transmission the interrupt will be triggered.
    //   The status register will have the TXFRS bit set. This function will clear the tx event and call the dwt_txcallback function.
    //
    //2. RX Event - if DWT_INT_RFCG is enabled, then when a frame with good CRC has been received the interrupt will be triggered.
    //   The status register will have the RXFCG bit set. This function will clear the rx event and call the dwt_rxcallback function.
    //
    //2.a. RX Event - This is same as 2. above except this time the received frame has ACK request bit set in the header (AAT bit will be set).
    //     This function will clear the rx event and call the dwt_rxcallback function, notifying the application that ACK req is set.
    //     If using auto-ACK, the AAT indicates that ACK frame transmission is in progress. Once the ACK has been sent the TXFRS bit will be set and TX event triggered.
    //     If the auto-ACK is not enabled, the application can format/configure and start transmission of its own ACK frame.
    //

    //fix for bug 622 - LDE done flag gets latched on a bad frame
    if ( ( status & SYS_STATUS_LDEDONE ) && ( radio_dw1000_UseDoubleBuffering == 0 ) )
    if ( ( status & ( SYS_STATUS_LDEDONE | SYS_STATUS_RXPHD | SYS_STATUS_RXSFDD ) ) != ( SYS_STATUS_LDEDONE | SYS_STATUS_RXPHD | SYS_STATUS_RXSFDD ) ) {

        //got LDE done but other flags SFD and PHR are clear - this is a bad frame - reset the transceiver
        //this will clear all events
        radio_dw1000_turn_off( Config );

        _radio_dw1000_reset_receiver( Config );

        //leave any TX events for processing (e.g. if we TX a frame, and then enable rx,
        //we can get into here before the TX frame done has been processed, when we are polling (i.e. slow to process the TX)
        status &= CLEAR_ALLTX_EVENTS;

        //re-enable the receiver - if auto rx re-enable set
        if ( Config->Init.Flags.RxAutoReenable ) {
            _radio_dw1000_register_write_16bit( Config, 0x0D, 0, ( t_u16 )0x00000100 ) ;
        }
    }


    // 1st check for RX frame received or RX timeout and if so call the rx callback function
    if ( status & SYS_STATUS_RXFCG ) { // Receiver FCS Good

    if ( ( status & SYS_STATUS_LDEDONE ) || !lde_loaded ) { // LDE done/finished or not activated
        // bug 634 - overrun overwrites the frame info data... so both frames should be discarded
        // read frame info and other registers and check for overflow again
        // if overflow set then discard both frames...

        t_u8  len = 0;

        if ( status & SYS_STATUS_RXOVRR ) { //NOTE when overrun both HS and RS pointers pot_u8 to the same buffer

            //when the overrun happens the frame info data of the buffer A (which contains the older frame e.g. seq. num = x)
            //will be corrupted with the latest frame (seq. num = x + 2) data, both the host and IC are pointing to buffer A
            //we are going to discard this frame - turn off transceiver and reset receiver
            radio_dw1000_turn_off( Config );

            // Reset Receiver
            _radio_dw1000_reset_receiver( Config );

            if ( Config->Init.Flags.RxAutoReenable ) { //re-enable of RX is ON, then re-enable here (ignore error)

                _radio_dw1000_register_write_16bit( Config, 0x0D, 0, ( t_u16 )0x00000100 ) ;
            }
            return;
        }
        else { //no overrun condition - proceed to process the frame

            len = _radio_dw1000_register_read_16bit( Config, 0x10, 0 ) & 0x3FF;
            if ( radio_dw1000_InsertSleeps ) { ttc_task_msleep( 50 ); }

            Config->LowLevelConfig.LocalParams.TxFCTRL = 0;
            _radio_dw1000_spi_register_read( Config,
                                             rdr_RX_BUFFER,
                                             2,
                                             ( t_u8* ) & ( Config->LowLevelConfig.LocalParams.TxFCTRL )
                                           );
            if ( Config->LowLevelConfig.LocalParams.LongFrames == 0 ) {
                len &= 0x7F ;
            }

            // Standard frame length up to 127, extended frame length up to 1023 bytes
            //dw1000local.cdata.datalength = len ;

            //bug 627 workaround - clear the AAT bit if the ACK request bit in the FC is not set
            if ( ( status & SYS_STATUS_AAT ) ) //AAT bit is set (ACK has been requested)
                //                && (((dw1000local.cdata.fctrl[0] & 0x20) == 0) || (dw1000local.cdata.fctrl[0] == 0x02)) //but the data frame has it clear or it is an ACK frame
                //                )
            {
                clear |= SYS_STATUS_AAT ;

            }

            if (! radio_dw1000_UseDoubleBuffering ) { // no double buffering

                //clear all receive status bits (as we are finished with this receive event)
                clear |= status & CLEAR_ALLRXGOOD_EVENTS ;

                _radio_dw1000_register_write_32bit( Config, rdr_SYS_STATUS, clear ) ;       // write status register to clear event bits we have seen

                //NOTE: clear the event which caused interrupt means once the rx is enabled or tx is started
                //new events can trigger and give rise to new interrupts
                //call the RX call-back function to process the RX event
                _radio_dw1000_isr_read_received_packet( Config, len );
            }
            else {                                    // double buffered receiving

                t_u8 buff;
                t_u8 hsrb = 0x01 ;

                //need to make sure that the host/IC buffer pointers are aligned before starting RX
                //read again because the status could have changed since the interrupt was triggered

                _radio_dw1000_spi_register_read( Config, SYS_STATUS_ID, 3, 1, &buff );

                //if ICRBP is equal to HSRBP this means we've received another frame (both buffers have frames)
                if ( ( buff & ( SYS_STATUS_ICRBP >> 24 ) ) ==        // IC side Receive Buffer Pointer
                        ( ( buff & ( SYS_STATUS_HSRBP >> 24 ) ) << 1 ) ) { // Host Side Receive Buffer Pointer

                    //clear all receive status bits (as we are finished with this receive event)
                    clear |= status & CLEAR_DBLBUFF_EVENTS  ;
                    _radio_dw1000_register_write_32bit( Config, rdr_SYS_STATUS, clear ) ;       // write status register to clear event bits we have seen
                }
                //if they are not aligned then there is a new frame in the other buffer, so we just need to toggle...

                if ( Config->Init.Flags.RxAutoReenable == 0 ) { //t_u32 buffer is on but no auto rx re-enable RX

                    _radio_dw1000_register_write_16bit( Config, 0x0D, 0, ( t_u16 )0x00000100 ) ;
                }

                //                //call the RX call-back function to process the RX event
                //                if (dw1000local.dwt_rxcallback != NULL)
                //                        dw1000local.dwt_rxcallback(&dw1000local.cdata);

                //if overrun, then reset the reciver - RX bug, when overruns cannot guarantee the last frame's data was not corrupted
                //if no overrun all is good, toggle the pointer
                if ( _radio_dw1000_check_receiver_overrun( Config ) == 0 ) {

                    //toggle the host side Receive Buffer Pointer by writing one to the register
                    _radio_dw1000_spi_register_write( Config, 0x0D, 3, 1, &hsrb ) ;     // we need to swap rx buffer status reg (write one to toggle internally)
                }
                else {
                    //the calback has completed, but the overrun has been set, before we toggled, this means two new frames have arrived (one in the other buffer) and the 2nd's PHR good set the overrun flag
                    //due to a receiver bug, which cannot guarantee the last frame's data was not corrupted need to reset receiver and discard any new data
                    radio_dw1000_turn_off( Config );

                    _radio_dw1000_reset_receiver( Config );


                    if ( Config->LowLevelConfig.LocalParams.SysCFGreg & 0x20000000 ) { //re-enable of RX is ON, then re-enable here
                        _radio_dw1000_register_write_16bit( Config, 0x0D, 0, ( t_u16 )0x00000100 ) ;
                    }
                }
            } //end of else t_u32 buffer

        }//end of no overrun
    } //if LDE_DONE is set (this means we have both SYS_STATUS_RXFCG and SYS_STATUS_LDEDONE)
    else { //no LDE_DONE ?
        //printf("NO LDE done or LDE error\n");
    }

    //        // start receiving now
    //        _radio_dw1000_enable_receiver(Radio, FALSE);

    } // end if CRC is good
    else
    //
    // Check for TX frame sent event and signal to upper layer.
    //
    if ( status & SYS_STATUS_TXFRS ) { // Transmit Frame Sent

        clear |= CLEAR_ALLTX_EVENTS; //clear TX event bits
        _radio_dw1000_register_write_32bit( Config, rdr_SYS_STATUS, clear );       // write status register to clear event bits we have seen

    }
    else if ( status & SYS_STATUS_RXRFTO ) { // Receiver Frame Wait timeout:

        clear |= status & SYS_STATUS_RXRFTO ;
        _radio_dw1000_register_write_32bit( Config, rdr_SYS_STATUS, clear ) ;       // write status register to clear event bits we have seen

    }
    else if ( status & CLEAR_ALLRXERROR_EVENTS ) { //catches all other error events

        clear |= status & CLEAR_ALLRXERROR_EVENTS;

        // write status register to clear event bits we have seen
        _radio_dw1000_register_write_32bit( Config, rdr_SYS_STATUS, clear );

        // Reset Receiver
        _radio_dw1000_reset_receiver( Config );
    }
    }
    */
}
e_ttc_radio_errorcode    radio_dw1000_load_defaults( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    // ToDo: revise this
    Config->Init.Flags.RxAutoReenable       = 1;
    Config->Init.Flags.EnableRanging        = 1;
    Config->Init.Flags.EnableRangingReplies = 1;
    Config->Init.Flags.EnableRX             = 1;
    Config->Init.Flags.EnableTX             = 1;
    Config->Init.Flags.Enable_LED_RX        = 1;
    Config->Init.Flags.Enable_LED_TX        = 1;
    Config->Init.Flags.Enable_Output_PA     = 0;
    TODO( "AutoACK feature currently does not seem to work with radio_common_push_list_rx_isr() and radio_common_transmit_packet(). Fix it to avoid using software AutoACK!" )
    Config->Init.Flags.AutoAcknowledge      = 0;
#ifndef EXTENSION_300_scheduler_freertos
    Config->Init.Flags.Enable_Output_IRQ    = 1;
    TODO( "move packet reception from ISR into task to allow interrupts even with enabled FreeRTOS" )
#endif
    Config->Init.MaxPacketSize               = 127;    // use IEEE 802.15.4 compliant packet size as default
    TODO( "Acknowledge_Timeout_usecs may require adjustment" )
    Config->Init.Acknowledge_Timeout_usecs   = 100000;

    Config->Features = &radio_dw1000_Features;

    t_ttc_radio_architecture* ConfigDW1000 = &( Config->LowLevelConfig );
    ttc_memory_set( ConfigDW1000, 0, sizeof( *ConfigDW1000 ) );

    // loading default delays for minimal Clock induced error in SS-TWR  time of flight estimation (DW1000 UserManual p.212)
    Config->Delay_RX_us           = 100;
    Config->Delay_TX_us           = 100;
    Config->Delay_RangingReply_us = 1600; // experimental value
    Config->Init.RawBitrate       = 6800000;
    Config->ChannelTX             = Config->ChannelRX = rdvc_virtual34_freq6489_bw1081_PRC18;

    // setting power levels for a balanced power level (some power hungry features are switched of)
    Config->Init.LevelTX          = Config->Features->MaxTxLevel - 1;
    Config->Init.LevelRX          = Config->Features->MaxRxLevel - 1;

    // load architecture specific ratio
    Config->RoundTripTime2Distance_Subtract   = 0;
    Config->RoundTripTime2Distance_Multiplier = 100;
    Config->RoundTripTime2Distance_Divider    = 264;

    switch ( Config->LogicalIndex ) { // SPI configuration (logical index of SPI device and Slave Select pin to use
#ifdef TTC_RADIO1_INDEX_SPI
        case 1: {
            ConfigDW1000->LogicalIndex_SPI     = TTC_RADIO1_INDEX_SPI;
            ConfigDW1000->LogicalIndex_SPI_NSS = TTC_RADIO1_INDEX_SPI_NSS;
#ifdef TTC_RADIO1_DW1000_PIN_IRQ
            ConfigDW1000->Pin_IRQ = TTC_RADIO1_DW1000_PIN_IRQ;
#else
#  warning Missing gpio pin definition for TTC_RADIO1_DW1000_PIN_RESET (Cannot hard reset device). Define it as gpio pin being connected to reset input pin of DW1000!
#endif
#ifdef TTC_RADIO1_DW1000_PIN_RESET
            ConfigDW1000->Pin_RESET = TTC_RADIO1_DW1000_PIN_RESET;
#else
#  warning Missing gpio pin definition for TTC_RADIO1_DW1000_PIN_RESET (Cannot hard reset device). Define it as gpio pin being connected to reset input pin of DW1000!
#endif
#ifdef TTC_RADIO1_DW1000_PIN_SPIPOL
            ConfigDW1000->Pin_SPIPOL = TTC_RADIO1_DW1000_PIN_SPIPOL;
#else
#  warning Missing gpio pin definition for TTC_RADIO1_DW1000_PIN_SPIPOL (Cannot configure SPI bus polarity). Define it as gpio pin being connected to input pin SPIPOL (GPIO3) of DW1000!
#endif
#ifdef TTC_RADIO1_DW1000_PIN_SPIPHA
            ConfigDW1000->Pin_SPIPHA = TTC_RADIO1_DW1000_PIN_SPIPHA;
#else
#  warning Missing gpio pin definition for TTC_RADIO1_DW1000_PIN_SPIPHA (Cannot configure SPI bus phase). Define it as gpio pin being connected to input pin SPIPHA (GPIO6) of DW1000!
#endif
            break;
        }
#endif
#ifdef TTC_RADIO2_INDEX_SPI
        case 2: {
            ConfigDW1000->LogicalIndex_SPI     = TTC_RADIO2_INDEX_SPI;
            ConfigDW1000->LogicalIndex_SPI_NSS = TTC_RADIO2_INDEX_SPI_NSS;
#ifdef TTC_RADIO2_DW1000_PIN_RESET
            ConfigDW1000->Pin_RESET = TTC_RADIO1_DW1000_PIN_RESET;
#else
#  warning Missing gpio pin definition for TTC_RADIO2_DW1000_PIN_RESET (Cannot hard reset device). Define it as gpio pin being connected to reset input pin of DW1000!
#endif
#ifdef TTC_RADIO2_DW1000_PIN_SPIPOL
            ConfigDW1000->Pin_SPIPOL = TTC_RADIO2_DW1000_PIN_SPIPOL;
#else
#  warning Missing gpio pin definition for TTC_RADIO2_DW1000_PIN_SPIPOL (Cannot configure SPI bus polarity). Define it as gpio pin being connected to input pin SPIPOL (GPIO3) of DW1000!
#endif
#ifdef TTC_RADIO2_DW1000_PIN_SPIPHA
            ConfigDW1000->Pin_SPIPHA = TTC_RADIO2_DW1000_PIN_SPIPHA;
#else
#  warning Missing gpio pin definition for TTC_RADIO2_DW1000_PIN_SPIPHA (Cannot configure SPI bus phase). Define it as gpio pin being connected to input pin SPIPHA (GPIO6) of DW1000!
#endif
            break;
        }
#endif
#ifdef TTC_RADIO3_INDEX_SPI
        case 3: {
            ConfigDW1000->LogicalIndex_SPI     = TTC_RADIO3_INDEX_SPI;
            ConfigDW1000->LogicalIndex_SPI_NSS = TTC_RADIO3_INDEX_SPI_NSS;
#ifdef TTC_RADIO3_DW1000_PIN_RESET
            ConfigDW1000->Pin_RESET = TTC_RADIO1_DW1000_PIN_RESET;
#else
#  warning Missing gpio pin definition for TTC_RADIO3_DW1000_PIN_RESET (Cannot hard reset device). Define it as gpio pin being connected to reset input pin of DW1000!
#endif
#ifdef TTC_RADIO3_DW1000_PIN_SPIPOL
            ConfigDW1000->Pin_SPIPOL = TTC_RADIO3_DW1000_PIN_SPIPOL;
#else
#  warning Missing gpio pin definition for TTC_RADIO3_DW1000_PIN_SPIPOL (Cannot configure SPI bus polarity). Define it as gpio pin being connected to input pin SPIPOL (GPIO3) of DW1000!
#endif
#ifdef TTC_RADIO3_DW1000_PIN_SPIPHA
            ConfigDW1000->Pin_SPIPHA = TTC_RADIO3_DW1000_PIN_SPIPHA;
#else
#  warning Missing gpio pin definition for TTC_RADIO3_DW1000_PIN_SPIPHA (Cannot configure SPI bus phase). Define it as gpio pin being connected to input pin SPIPHA (GPIO6) of DW1000!
#endif
            break;
        }
#endif
#ifdef TTC_RADIO4_INDEX_SPI
        case 4: {
            ConfigDW1000->LogicalIndex_SPI     = TTC_RADIO4_INDEX_SPI;
            ConfigDW1000->LogicalIndex_SPI_NSS = TTC_RADIO4_INDEX_SPI_NSS;
#ifdef TTC_RADIO4_DW1000_PIN_RESET
            ConfigDW1000->Pin_RESET = TTC_RADIO1_DW1000_PIN_RESET;
#else
#  warning Missing gpio pin definition for TTC_RADIO4_DW1000_PIN_RESET (Cannot hard reset device). Define it as gpio pin being connected to reset input pin of DW1000!
#endif
#ifdef TTC_RADIO4_DW1000_PIN_SPIPOL
            ConfigDW1000->Pin_SPIPOL = TTC_RADIO4_DW1000_PIN_SPIPOL;
#else
#  warning Missing gpio pin definition for TTC_RADIO4_DW1000_PIN_SPIPOL (Cannot configure SPI bus polarity). Define it as gpio pin being connected to input pin SPIPOL (GPIO3) of DW1000!
#endif
#ifdef TTC_RADIO4_DW1000_PIN_SPIPHA
            ConfigDW1000->Pin_SPIPHA = TTC_RADIO4_DW1000_PIN_SPIPHA;
#else
#  warning Missing gpio pin definition for TTC_RADIO4_DW1000_PIN_SPIPHA (Cannot configure SPI bus phase). Define it as gpio pin being connected to input pin SPIPHA (GPIO6) of DW1000!
#endif
            break;
        }
#endif
#ifdef TTC_RADIO5_INDEX_SPI
        case 5: {
            ConfigDW1000->LogicalIndex_SPI     = TTC_RADIO5_INDEX_SPI;
            ConfigDW1000->LogicalIndex_SPI_NSS = TTC_RADIO5_INDEX_SPI_NSS;
#ifdef TTC_RADIO5_DW1000_PIN_RESET
            ConfigDW1000->Pin_RESET = TTC_RADIO1_DW1000_PIN_RESET;
#else
#  warning Missing gpio pin definition for TTC_RADIO5_DW1000_PIN_RESET (Cannot hard reset device). Define it as gpio pin being connected to reset input pin of DW1000!
#endif
#ifdef TTC_RADIO5_DW1000_PIN_SPIPOL
            ConfigDW1000->Pin_SPIPOL = TTC_RADIO5_DW1000_PIN_SPIPOL;
#else
#  warning Missing gpio pin definition for TTC_RADIO5_DW1000_PIN_SPIPOL (Cannot configure SPI bus polarity). Define it as gpio pin being connected to input pin SPIPOL (GPIO3) of DW1000!
#endif
#ifdef TTC_RADIO5_DW1000_PIN_SPIPHA
            ConfigDW1000->Pin_SPIPHA = TTC_RADIO5_DW1000_PIN_SPIPHA;
#else
#  warning Missing gpio pin definition for TTC_RADIO5_DW1000_PIN_SPIPHA (Cannot configure SPI bus phase). Define it as gpio pin being connected to input pin SPIPHA (GPIO6) of DW1000!
#endif
            break;
        }
#endif
#ifdef TTC_RADIO6_INDEX_SPI
        case 6: {
            ConfigDW1000->LogicalIndex_SPI     = TTC_RADIO6_INDEX_SPI;
            ConfigDW1000->LogicalIndex_SPI_NSS = TTC_RADIO6_INDEX_SPI_NSS;
#ifdef TTC_RADIO6_DW1000_PIN_RESET
            ConfigDW1000->Pin_RESET = TTC_RADIO1_DW1000_PIN_RESET;
#else
#  warning Missing gpio pin definition for TTC_RADIO6_DW1000_PIN_RESET (Cannot hard reset device). Define it as gpio pin being connected to reset input pin of DW1000!
#endif
#ifdef TTC_RADIO6_DW1000_PIN_SPIPOL
            ConfigDW1000->Pin_SPIPOL = TTC_RADIO6_DW1000_PIN_SPIPOL;
#else
#  warning Missing gpio pin definition for TTC_RADIO6_DW1000_PIN_SPIPOL (Cannot configure SPI bus polarity). Define it as gpio pin being connected to input pin SPIPOL (GPIO3) of DW1000!
#endif
#ifdef TTC_RADIO6_DW1000_PIN_SPIPHA
            ConfigDW1000->Pin_SPIPHA = TTC_RADIO6_DW1000_PIN_SPIPHA;
#else
#  warning Missing gpio pin definition for TTC_RADIO6_DW1000_PIN_SPIPHA (Cannot configure SPI bus phase). Define it as gpio pin being connected to input pin SPIPHA (GPIO6) of DW1000!
#endif
            break;
        }
#endif
#ifdef TTC_RADIO7_INDEX_SPI
        case 7: {
            ConfigDW1000->LogicalIndex_SPI     = TTC_RADIO7_INDEX_SPI;
            ConfigDW1000->LogicalIndex_SPI_NSS = TTC_RADIO7_INDEX_SPI_NSS;
#ifdef TTC_RADIO7_DW1000_PIN_RESET
            ConfigDW1000->Pin_RESET = TTC_RADIO1_DW1000_PIN_RESET;
#else
#  warning Missing gpio pin definition for TTC_RADIO7_DW1000_PIN_RESET (Cannot hard reset device). Define it as gpio pin being connected to reset input pin of DW1000!
#endif
#ifdef TTC_RADIO7_DW1000_PIN_SPIPOL
            ConfigDW1000->Pin_SPIPOL = TTC_RADIO7_DW1000_PIN_SPIPOL;
#else
#  warning Missing gpio pin definition for TTC_RADIO7_DW1000_PIN_SPIPOL (Cannot configure SPI bus polarity). Define it as gpio pin being connected to input pin SPIPOL (GPIO3) of DW1000!
#endif
#ifdef TTC_RADIO7_DW1000_PIN_SPIPHA
            ConfigDW1000->Pin_SPIPHA = TTC_RADIO7_DW1000_PIN_SPIPHA;
#else
#  warning Missing gpio pin definition for TTC_RADIO7_DW1000_PIN_SPIPHA (Cannot configure SPI bus phase). Define it as gpio pin being connected to input pin SPIPHA (GPIO6) of DW1000!
#endif
            break;
        }
#endif
#ifdef TTC_RADIO8_INDEX_SPI
        case 8: {
            ConfigDW1000->LogicalIndex_SPI     = TTC_RADIO8_INDEX_SPI;
            ConfigDW1000->LogicalIndex_SPI_NSS = TTC_RADIO8_INDEX_SPI_NSS;
#ifdef TTC_RADIO8_DW1000_PIN_RESET
            ConfigDW1000->Pin_RESET = TTC_RADIO1_DW1000_PIN_RESET;
#else
#  warning Missing gpio pin definition for TTC_RADIO8_DW1000_PIN_RESET (Cannot hard reset device). Define it as gpio pin being connected to reset input pin of DW1000!
#endif
#ifdef TTC_RADIO8_DW1000_PIN_SPIPOL
            ConfigDW1000->Pin_SPIPOL = TTC_RADIO8_DW1000_PIN_SPIPOL;
#else
#  warning Missing gpio pin definition for TTC_RADIO8_DW1000_PIN_SPIPOL (Cannot configure SPI bus polarity). Define it as gpio pin being connected to input pin SPIPOL (GPIO3) of DW1000!
#endif
#ifdef TTC_RADIO8_DW1000_PIN_SPIPHA
            ConfigDW1000->Pin_SPIPHA = TTC_RADIO8_DW1000_PIN_SPIPHA;
#else
#  warning Missing gpio pin definition for TTC_RADIO8_DW1000_PIN_SPIPHA (Cannot configure SPI bus phase). Define it as gpio pin being connected to input pin SPIPHA (GPIO6) of DW1000!
#endif
            break;
        }
#endif
#ifdef TTC_RADIO9_INDEX_SPI
        case 9: {
            ConfigDW1000->LogicalIndex_SPI     = TTC_RADIO9_INDEX_SPI;
            ConfigDW1000->LogicalIndex_SPI_NSS = TTC_RADIO9_INDEX_SPI_NSS;
#ifdef TTC_RADIO9_DW1000_PIN_RESET
            ConfigDW1000->Pin_RESET = TTC_RADIO1_DW1000_PIN_RESET;
#else
#  warning Missing gpio pin definition for TTC_RADIO9_DW1000_PIN_RESET (Cannot hard reset device). Define it as gpio pin being connected to reset input pin of DW1000!
#endif
#ifdef TTC_RADIO9_DW1000_PIN_SPIPOL
            ConfigDW1000->Pin_SPIPOL = TTC_RADIO9_DW1000_PIN_SPIPOL;
#else
#  warning Missing gpio pin definition for TTC_RADIO9_DW1000_PIN_SPIPOL (Cannot configure SPI bus polarity). Define it as gpio pin being connected to input pin SPIPOL (GPIO3) of DW1000!
#endif
#ifdef TTC_RADIO9_DW1000_PIN_SPIPHA
            ConfigDW1000->Pin_SPIPHA = TTC_RADIO9_DW1000_PIN_SPIPHA;
#else
#  warning Missing gpio pin definition for TTC_RADIO9_DW1000_PIN_SPIPHA (Cannot configure SPI bus phase). Define it as gpio pin being connected to input pin SPIPHA (GPIO6) of DW1000!
#endif
            break;
        }
#endif
#ifdef TTC_RADIO10_INDEX_SPI
        case 10: {
            ConfigDW1000->LogicalIndex_SPI     = TTC_RADIO10_INDEX_SPI;
            ConfigDW1000->LogicalIndex_SPI_NSS = TTC_RADIO10_INDEX_SPI_NSS;
#ifdef TTC_RADIO10_DW1000_PIN_RESET
            ConfigDW1000->Pin_RESET = TTC_RADIO1_DW1000_PIN_RESET;
#else
#  warning Missing gpio pin definition for TTC_RADIO10_DW1000_PIN_RESET (Cannot hard reset device). Define it as gpio pin being connected to reset input pin of DW1000!
#endif
#ifdef TTC_RADIO10_DW1000_PIN_SPIPOL
            ConfigDW1000->Pin_SPIPOL = TTC_RADIO10_DW1000_PIN_SPIPOL;
#else
#  warning Missing gpio pin definition for TTC_RADIO10_DW1000_PIN_SPIPOL (Cannot configure SPI bus polarity). Define it as gpio pin being connected to input pin SPIPOL (GPIO3) of DW1000!
#endif
#ifdef TTC_RADIO10_DW1000_PIN_SPIPHA
            ConfigDW1000->Pin_SPIPHA = TTC_RADIO10_DW1000_PIN_SPIPHA;
#else
#  warning Missing gpio pin definition for TTC_RADIO10_DW1000_PIN_SPIPHA (Cannot configure SPI bus phase). Define it as gpio pin being connected to input pin SPIPHA (GPIO6) of DW1000!
#endif
            break;
        }
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // missing spi defines for given logical index. Check your board makefile and your application!
    }

    // find out which slave select pin has been configured for DW1000 on spi bus
    ConfigDW1000->Pin_SPI_NSS = ttc_spi_get_pin_slave_select( ConfigDW1000->LogicalIndex_SPI,
                                                              ConfigDW1000->LogicalIndex_SPI_NSS
                                                            );

    return ( e_ttc_radio_errorcode ) 0;
}
void                     radio_dw1000_maintenance( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    ttc_interrupt_critical_begin();
    volatile u_radio_dw1000_register_sys_state SYS_STATE;
    SYS_STATE.Word = _radio_dw1000_register_read_SYS_STATE( Config );
    /*
    volatile e_radio_dw1000_tx_state   StateTX   = SYS_STATE.Fields.TX_STATE;
    volatile e_radio_dw1000_rx_state   StateRX   = SYS_STATE.Fields.RX_STATE;
    volatile e_radio_dw1000_pmsc_state StatePMSC = SYS_STATE.Fields.PMSC_STATE;

    ( void ) StateTX; ( void ) StateRX; ( void ) StatePMSC; // avoid compiler warnings: unused variable...
    */

    if ( ttc_mutex_is_locked( &( Config->Flag_ReceiverOn ) ) &&
            ( SYS_STATE.Fields.RX_STATE == rdrs_IDLE )
       ) { // receiver is off but local flags says that it should be switched on
        Assert_RADIO_EXTRA( 0, ttc_assert_origin_auto ); // Somehow our local flag was not updated when receiver switched off. Check implementation of this low-level driver!
        ttc_mutex_lock_try( &( Config->Flag_ReceiverOn ) ); // correct flag
        radio_dw1000_receiver( Config, TRUE, 0, 0 );     // switch on receiver (will set flag if successfull)
        Config->LowLevelConfig.Amount_RX_Restart++;
    }
    if ( ttc_gpio_get( Config->LowLevelConfig.Pin_IRQ ) &&
            Config->Init.Flags.Enable_Output_IRQ
       ) { // interrupt line is active: it seems as if we have missed an interrupt
        Assert_RADIO_EXTRA( !radio_common_InsideISR, ttc_assert_origin_auto );  // this function should not be called from interrupt service routine!
        //radio_common_breakpoint(); //DEBUG
        Config->LowLevelConfig.Amount_PendingIRQs++;
        ttc_interrupt_enable_gpio( Config->RadioInterrupt );   // ensure that gpio pin can raise interrupts
        ttc_interrupt_critical_end();
        _radio_dw1000_isr_raise( Config );                     // generate software interrupt to run radio_dw1000_isr_generic()
        return;
    }
    if ( Config->Init.Flags.RxAutoReenable ) { // check if receiver has to be re-enabled
        if ( SYS_STATE.Fields.PMSC_STATE == rdps_IDLE ) {
            RADIO_COMMON_COUNT( Amount_ReceiverRestart );
            radio_dw1000_receiver( Config, TRUE, 0, 0 ); // re-enable receiver
        }
    }
    if ( Config->LowLevelConfig.TuneReceiverBitRate ) { // bitrate has changed: retune receiver
        _radio_dw1000_tune_receiver( Config, Config->LowLevelConfig.TuneReceiverBitRate );
        Config->LowLevelConfig.TuneReceiverBitRate = 0;
    }
    if ( ttc_mutex_is_locked( &( Config->FlagRunningTX ) ) && // it seems as if we are transmitting
            SYS_STATE.Fields.TX_STATE == rdts_IDLE         && // but transmitter is switched off!
            Config->Init.Flags.Enable_Output_IRQ              // interrupt line is enabled
       ) {
        ttc_interrupt_critical_end();
        _radio_dw1000_isr_raise( Config ); // generate software interrupt to run radio_dw1000_isr_generic()
        return;
    }

    static volatile BOOL rdm_UseExtraCheck = 0; // SEEMS TO HINDER FURTHER RECEIVING (may be enabled during debug session)
    if ( rdm_UseExtraCheck ) {
        if ( Config->Init.Flags.RxAutoReenable ) { // check if receiver has to be re-enabled
            if ( !ttc_mutex_is_locked( &( Config->FlagRunningTX ) ) ) { // no transmission ongoing
                _radio_dw1000_register_read_SYS_CTRL( Config );
                if ( ! Config->LowLevelConfig.Registers.SYS_CTRL.Fields.RXENAB ) {
                    _radio_dw1000_reset_soft( Config );
                    radio_dw1000_receiver( Config, TRUE, 0, 0 );
                    SYS_STATE.Word = _radio_dw1000_register_read_SYS_STATE( Config );

                    ttc_interrupt_critical_end();
                    _radio_dw1000_isr_raise( Config ); // generate software interrupt to run radio_dw1000_isr_generic()
                    return;
                } // re-enable receiver
            }
        }
    }

    ttc_interrupt_critical_end();
    TTC_TASK_RETURN; // check for stack overflow + return
}
void                     radio_dw1000_prepare() {

#if (TTC_ASSERT_RADIO_EXTRA == 1)
    t_ttc_packet Packet;
    Packet.Meta.IndexPhysical = -1;
    Assert_RADIO_EXTRA( Packet.Meta.IndexPhysical > RADIO_DW1000_AMOUNT_RX_META, ttc_assert_origin_auto );  // field is too small to store maximum value: Increase field size in struct definition in ttc_packet_types.h!

    /* check all register address codes
     *
     * How to add new register defines
     * 1) add new entry to e_radio_dw1000_register with value 0x0
     * 2) copy any of the following Assert_RADIO_EXTRA() lines
     * 3) modify rdr_* and function arguments of _radio_dw1000_compile_transaction_header()
     * 4) compile + flash + debug on microcontroller
     * 5) program should assert when run inside Assert_RADIO_EXTRA()
     * 6) use "up" command in gdb to get up to your new Assert_RADIO_EXTRA() line
     * 7) print correct code using a _radio_dw1000_compile_transaction_header()
     *    e.g.: _radio_dw1000_compile_transaction_header( 0x26, 0x0000, 0 ).Word
     * 8) replace 0x0 of new e_radio_dw1000_register entry by printed, correct code
     * 9) from now on you should be able to use your new register code
     */
    Assert_RADIO_EXTRA( rdr_GPIO_MODE    == _radio_dw1000_compile_transaction_header( 0x26, 0x0000, 0 ).Word, ttc_assert_origin_auto );
    Assert_RADIO_EXTRA( rdr_DEV_ID       == _radio_dw1000_compile_transaction_header( 0x00, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Device ID
    Assert_RADIO_EXTRA( rdr_EUI64        == _radio_dw1000_compile_transaction_header( 0x01, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Extended Unique Identifier (64 bit physical address)
    Assert_RADIO_EXTRA( rdr_SHORT_ADDR   == _radio_dw1000_compile_transaction_header( 0x03, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // 16 bit Local ID
    Assert_RADIO_EXTRA( rdr_PAN_ID       == _radio_dw1000_compile_transaction_header( 0x03, 0x0002, 0 ).Word, ttc_assert_origin_auto );  // 16 bit Local Pan ID
    Assert_RADIO_EXTRA( rdr_SYS_CFG      == _radio_dw1000_compile_transaction_header( 0x04, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // System Configuration
    Assert_RADIO_EXTRA( rdr_SYS_TIME     == _radio_dw1000_compile_transaction_header( 0x06, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // System Time Counter
    Assert_RADIO_EXTRA( rdr_TX_FCTRL     == _radio_dw1000_compile_transaction_header( 0x08, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Transmitter Frame Control
    Assert_RADIO_EXTRA( rdr_TX_BUFFER    == _radio_dw1000_compile_transaction_header( 0x09, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Transmit Data Buffer (1024 bytes)
    Assert_RADIO_EXTRA( rdr_DX_TIME      == _radio_dw1000_compile_transaction_header( 0x0a, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Delayed Send or Receive Time (40 bits)
    Assert_RADIO_EXTRA( rdr_RX_FWTO      == _radio_dw1000_compile_transaction_header( 0x0c, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Receiver Frame Wait Timeout Period
    Assert_RADIO_EXTRA( rdr_SYS_CTRL     == _radio_dw1000_compile_transaction_header( 0x0d, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // System Control
    Assert_RADIO_EXTRA( rdr_SYS_MASK     == _radio_dw1000_compile_transaction_header( 0x0e, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // System Event Masks
    Assert_RADIO_EXTRA( rdr_SYS_STATUS   == _radio_dw1000_compile_transaction_header( 0x0f, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // System Event Status
    Assert_RADIO_EXTRA( rdr_RX_FINFO     == _radio_dw1000_compile_transaction_header( 0x10, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Receiver Frame Information (double buffered operation only)
    Assert_RADIO_EXTRA( rdr_RX_BUFFER    == _radio_dw1000_compile_transaction_header( 0x11, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Receiver Frame Buffer (1024 bytes)
    Assert_RADIO_EXTRA( rdr_RX_FQUAL     == _radio_dw1000_compile_transaction_header( 0x12, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Receiver Frame Quality Information
    Assert_RADIO_EXTRA( rdr_RX_TTCKI     == _radio_dw1000_compile_transaction_header( 0x13, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Receiver Time Tracking Interval
    Assert_RADIO_EXTRA( rdr_RX_TTCKO     == _radio_dw1000_compile_transaction_header( 0x14, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Receiver Time Tracking Offset
    Assert_RADIO_EXTRA( rdr_RX_TIME      == _radio_dw1000_compile_transaction_header( 0x15, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // adjusted time stamp of last received frame
    Assert_RADIO_EXTRA( rdr_RX_STAMP     == _radio_dw1000_compile_transaction_header( 0x15, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // adjusted time stamp of last received frame
    Assert_RADIO_EXTRA( rdr_RX_RAWST     == _radio_dw1000_compile_transaction_header( 0x15, 0x0009, 0 ).Word, ttc_assert_origin_auto );  // raw time stamp of last received frame
    Assert_RADIO_EXTRA( rdr_FIRST_PATH   == _radio_dw1000_compile_transaction_header( 0x15, 0x0005, 0 ).Word, ttc_assert_origin_auto );  // First Path Data of received frame
    Assert_RADIO_EXTRA( rdr_RX_FPAMPL1   == _radio_dw1000_compile_transaction_header( 0x15, 0x0007, 0 ).Word, ttc_assert_origin_auto );  // Receive Time Stamp First Path Amplitude Point 1
    Assert_RADIO_EXTRA( rdr_TX_STAMP     == _radio_dw1000_compile_transaction_header( 0x17, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // time stamp of last transmission
    Assert_RADIO_EXTRA( rdr_TX_RAWST     == _radio_dw1000_compile_transaction_header( 0x17, 0x0005, 0 ).Word, ttc_assert_origin_auto );  // time stamp of last transmission
    Assert_RADIO_EXTRA( rdr_TX_ANTD      == _radio_dw1000_compile_transaction_header( 0x18, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Transmitter Antenna Delay
    Assert_RADIO_EXTRA( rdr_SYS_STATE    == _radio_dw1000_compile_transaction_header( 0x19, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // System State Information
    Assert_RADIO_EXTRA( rdr_ACK_RESP_T   == _radio_dw1000_compile_transaction_header( 0x1a, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Acknowledgement Time and Response Time
    Assert_RADIO_EXTRA( rdr_RX_SNIFF     == _radio_dw1000_compile_transaction_header( 0x1d, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Pulsed Preamble Reception Configuration
    Assert_RADIO_EXTRA( rdr_TX_POWER     == _radio_dw1000_compile_transaction_header( 0x1e, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Transmitter Power Control
    Assert_RADIO_EXTRA( rdr_CHAN_CTRL    == _radio_dw1000_compile_transaction_header( 0x1f, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Channel Control
    Assert_RADIO_EXTRA( rdr_USR_SFD      == _radio_dw1000_compile_transaction_header( 0x21, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // User specified short/long TX/RX Start of Frame Delimiter sequences
    Assert_RADIO_EXTRA( rdr_AGC_CTRL1    == _radio_dw1000_compile_transaction_header( 0x23, 0x0002, 0 ).Word, ttc_assert_origin_auto );  // Automatic Gain Control 1
    Assert_RADIO_EXTRA( rdr_AGC_TUNE1    == _radio_dw1000_compile_transaction_header( 0x23, 0x0004, 0 ).Word, ttc_assert_origin_auto );  // Automatic Gain Control Tuning 1
    Assert_RADIO_EXTRA( rdr_AGC_TUNE2    == _radio_dw1000_compile_transaction_header( 0x23, 0x000c, 0 ).Word, ttc_assert_origin_auto );  // Automatic Gain Control Tuning 2
    Assert_RADIO_EXTRA( rdr_AGC_TUNE3    == _radio_dw1000_compile_transaction_header( 0x23, 0x0012, 0 ).Word, ttc_assert_origin_auto );  // Automatic Gain Control Tuning 3
    Assert_RADIO_EXTRA( rdr_AGC_STAT1    == _radio_dw1000_compile_transaction_header( 0x23, 0x001e, 0 ).Word, ttc_assert_origin_auto );  // Automatic Gain Control Status
    Assert_RADIO_EXTRA( rdr_EC_CTRL      == _radio_dw1000_compile_transaction_header( 0x24, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // External Synchronization Control
    Assert_RADIO_EXTRA( rdr_ACC_MEM      == _radio_dw1000_compile_transaction_header( 0x25, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // read access to accumulator data
    Assert_RADIO_EXTRA( rdr_GPIO_MODE    == _radio_dw1000_compile_transaction_header( 0x26, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // GPIO Mode Control Register -> DW1000 UserManual v2.03 p.121
    Assert_RADIO_EXTRA( rdr_GPIO_DIR     == _radio_dw1000_compile_transaction_header( 0x26, 0x0008, 0 ).Word, ttc_assert_origin_auto );  // GPIO Direction Control Register -> DW1000 UserManual v2.03 p.123
    Assert_RADIO_EXTRA( rdr_GPIO_DOUT    == _radio_dw1000_compile_transaction_header( 0x26, 0x000c, 0 ).Word, ttc_assert_origin_auto );  // GPIO Data Output register -> DW1000 User Manual p.125
    Assert_RADIO_EXTRA( rdr_GPIO_IRQE    == _radio_dw1000_compile_transaction_header( 0x26, 0x0010, 0 ).Word, ttc_assert_origin_auto );  // GPIO Interrupt Enable -> DW1000 User Manual p.127
    Assert_RADIO_EXTRA( rdr_GPIO_ISEN    == _radio_dw1000_compile_transaction_header( 0x26, 0x0014, 0 ).Word, ttc_assert_origin_auto );  // GPIO Interrupt Sense Selection -> DW1000 User Manual p.128
    Assert_RADIO_EXTRA( rdr_GPIO_IMODE   == _radio_dw1000_compile_transaction_header( 0x26, 0x0018, 0 ).Word, ttc_assert_origin_auto );  // GPIO Interrupt Mode (Level / Edge) -> DW1000 User Manual p.129
    Assert_RADIO_EXTRA( rdr_GPIO_IBES    == _radio_dw1000_compile_transaction_header( 0x26, 0x001c, 0 ).Word, ttc_assert_origin_auto );  // GPIO Interrupt “Both Edge” Select -> DW1000 User Manual p.130
    Assert_RADIO_EXTRA( rdr_GPIO_ICLR    == _radio_dw1000_compile_transaction_header( 0x26, 0x0020, 0 ).Word, ttc_assert_origin_auto );  // GPIO Interrupt Latch Clear -> DW1000 User Manual p.131
    Assert_RADIO_EXTRA( rdr_GPIO_IDBE    == _radio_dw1000_compile_transaction_header( 0x26, 0x0024, 0 ).Word, ttc_assert_origin_auto );  // GPIO Interrupt De-bounce Enable -> DW1000 User Manual p.132
    Assert_RADIO_EXTRA( rdr_GPIO_RAW     == _radio_dw1000_compile_transaction_header( 0x26, 0x0028, 0 ).Word, ttc_assert_origin_auto );  // GPIO raw state -> DW1000 User Manual p.133
    Assert_RADIO_EXTRA( rdr_DRX_TUNE0b   == _radio_dw1000_compile_transaction_header( 0x27, 0x0002, 0 ).Word, ttc_assert_origin_auto );  // Digital Receiver Tune 0b
    Assert_RADIO_EXTRA( rdr_DRX_TUNE1a   == _radio_dw1000_compile_transaction_header( 0x27, 0x0004, 0 ).Word, ttc_assert_origin_auto );  // Digital Receiver Tune 1a
    Assert_RADIO_EXTRA( rdr_DRX_TUNE1b   == _radio_dw1000_compile_transaction_header( 0x27, 0x0006, 0 ).Word, ttc_assert_origin_auto );  // Digital Receiver Tune 1b
    Assert_RADIO_EXTRA( rdr_DRX_TUNE2    == _radio_dw1000_compile_transaction_header( 0x27, 0x0008, 0 ).Word, ttc_assert_origin_auto );  // Digital Receiver Tune 2
    Assert_RADIO_EXTRA( rdr_DRX_SFDTOC   == _radio_dw1000_compile_transaction_header( 0x27, 0x0020, 0 ).Word, ttc_assert_origin_auto );  // Digital Receiver Start Frame Delimiter Timeout
    Assert_RADIO_EXTRA( rdr_DRX_PRETOC   == _radio_dw1000_compile_transaction_header( 0x27, 0x0024, 0 ).Word, ttc_assert_origin_auto );  // Digital Receiver Preamble Detection Timeout
    Assert_RADIO_EXTRA( rdr_DRX_TUNE4H   == _radio_dw1000_compile_transaction_header( 0x27, 0x0026, 0 ).Word, ttc_assert_origin_auto );  // Digital Receiver Tuning Register 4H
    Assert_RADIO_EXTRA( rdr_RF_RXCTRLH   == _radio_dw1000_compile_transaction_header( 0x28, 0x000b, 0 ).Word, ttc_assert_origin_auto );  // Analog RX Control Register -> DW1000 User Manual p.141
    Assert_RADIO_EXTRA( rdr_RF_CONF      == _radio_dw1000_compile_transaction_header( 0x28, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // RF Configuration Register -> DW1000 User Manual p.140
    Assert_RADIO_EXTRA( rdr_RF_TXCTRL    == _radio_dw1000_compile_transaction_header( 0x28, 0x000c, 0 ).Word, ttc_assert_origin_auto );  // Analog Transmitter Control
    Assert_RADIO_EXTRA( rdr_RF_STATUS    == _radio_dw1000_compile_transaction_header( 0x28, 0x002c, 0 ).Word, ttc_assert_origin_auto );  // PLL-Lock Status Register-> DW1000 User Manual p.143
    Assert_RADIO_EXTRA( rdr_TX_CAL       == _radio_dw1000_compile_transaction_header( 0x2a, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Transmitter Calibration Block
    Assert_RADIO_EXTRA( rdr_TC_PGDELAY   == _radio_dw1000_compile_transaction_header( 0x2a, 0x000b, 0 ).Word, ttc_assert_origin_auto );  // Transmitter Pulse Generator Delay
    Assert_RADIO_EXTRA( rdr_FS_CTRL      == _radio_dw1000_compile_transaction_header( 0x2b, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Frequency Synthesizer Control
    Assert_RADIO_EXTRA( rdr_FS_PLLCFG    == _radio_dw1000_compile_transaction_header( 0x2b, 0x0007, 0 ).Word, ttc_assert_origin_auto );  // Frequency Synthesizer PLL Configuration
    Assert_RADIO_EXTRA( rdr_FS_PLLTUNE   == _radio_dw1000_compile_transaction_header( 0x2b, 0x000b, 0 ).Word, ttc_assert_origin_auto );  // Frequency Synthesizer PLL Tuning
    Assert_RADIO_EXTRA( rdr_FS_XTALT     == _radio_dw1000_compile_transaction_header( 0x2b, 0x000e, 0 ).Word, ttc_assert_origin_auto );  // Frequency Synthesizer Crystal trim
    Assert_RADIO_EXTRA( rdr_AON          == _radio_dw1000_compile_transaction_header( 0x2c, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Always On Configuration (??? DecaWave implementation used 0x28:00!)
    Assert_RADIO_EXTRA( rdr_AON_WCFG     == _radio_dw1000_compile_transaction_header( 0x2c, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Always On Configuration (??? DecaWave implementation used 0x28:00!)
    Assert_RADIO_EXTRA( rdr_AON_CTRL     == _radio_dw1000_compile_transaction_header( 0x2c, 0x0002, 0 ).Word, ttc_assert_origin_auto );  // Always-On Control Register -> DW1000 User Manual p.153
    Assert_RADIO_EXTRA( rdr_AON_RDAT     == _radio_dw1000_compile_transaction_header( 0x2c, 0x0003, 0 ).Word, ttc_assert_origin_auto );  // Always On Direct Access Read Data Result -> DW1000 User Manual p.156
    Assert_RADIO_EXTRA( rdr_AON_ADDR     == _radio_dw1000_compile_transaction_header( 0x2c, 0x0004, 0 ).Word, ttc_assert_origin_auto );  // Always On Direct Access Address -> DW1000 User Manual p.157
    Assert_RADIO_EXTRA( rdr_AON_CFG0     == _radio_dw1000_compile_transaction_header( 0x2c, 0x0006, 0 ).Word, ttc_assert_origin_auto );  // Always On Configuration Register 0 -> DW1000 User Manual p.157
    Assert_RADIO_EXTRA( rdr_AON_CFG1     == _radio_dw1000_compile_transaction_header( 0x2c, 0x000a, 0 ).Word, ttc_assert_origin_auto );  // Always On Configuration Register 1 -> DW1000 User Manual p.159
    Assert_RADIO_EXTRA( rdr_OTP_IF       == _radio_dw1000_compile_transaction_header( 0x2d, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // One Time Programmable Memory Interface
    Assert_RADIO_EXTRA( rdr_OTP_ADDR     == _radio_dw1000_compile_transaction_header( 0x2d, 0x0004, 0 ).Word, ttc_assert_origin_auto );  // One Time Programmable Memory Address
    Assert_RADIO_EXTRA( rdr_OTP_CTRL     == _radio_dw1000_compile_transaction_header( 0x2d, 0x0006, 0 ).Word, ttc_assert_origin_auto );  // One Time Programmable Memory Control
    Assert_RADIO_EXTRA( rdr_OTP_STAT     == _radio_dw1000_compile_transaction_header( 0x2d, 0x0008, 0 ).Word, ttc_assert_origin_auto );  // One Time Programmable Memory Control
    Assert_RADIO_EXTRA( rdr_OTP_WDAT     == _radio_dw1000_compile_transaction_header( 0x2d, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // One Time Programmable Memory Write Data
    Assert_RADIO_EXTRA( rdr_OTP_RDAT     == _radio_dw1000_compile_transaction_header( 0x2d, 0x000a, 0 ).Word, ttc_assert_origin_auto );  // One Time Programmable Memory Read Data
    Assert_RADIO_EXTRA( rdr_OTP_SF       == _radio_dw1000_compile_transaction_header( 0x2d, 0x0012, 0 ).Word, ttc_assert_origin_auto );  // One Time Programmable Memory Special Function
    Assert_RADIO_EXTRA( rdr_LDE_THRESH   == _radio_dw1000_compile_transaction_header( 0x2e, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Leading Edge Detection Threshold Report
    Assert_RADIO_EXTRA( rdr_LDE_CFG1     == _radio_dw1000_compile_transaction_header( 0x2e, 0x0806, 0 ).Word, ttc_assert_origin_auto );  // Leading Edge Detection Configuration 1
    Assert_RADIO_EXTRA( rdr_LDE_PPINDX   == _radio_dw1000_compile_transaction_header( 0x2e, 0x1000, 0 ).Word, ttc_assert_origin_auto );  // Leading Edge Detection Peak Path Index
    Assert_RADIO_EXTRA( rdr_LDE_PPAMPL   == _radio_dw1000_compile_transaction_header( 0x2e, 0x1002, 0 ).Word, ttc_assert_origin_auto );  // Leading Edge Detection Peak Path Amplitude
    Assert_RADIO_EXTRA( rdr_LDE_RXANTD   == _radio_dw1000_compile_transaction_header( 0x2e, 0x1804, 0 ).Word, ttc_assert_origin_auto );  // Leading Edge Detection Receive Antenna Delay
    Assert_RADIO_EXTRA( rdr_LDE_CFG2     == _radio_dw1000_compile_transaction_header( 0x2e, 0x1806, 0 ).Word, ttc_assert_origin_auto );  // Leading Edge Detection Configuration 2
    Assert_RADIO_EXTRA( rdr_LDE_REPC     == _radio_dw1000_compile_transaction_header( 0x2e, 0x2804, 0 ).Word, ttc_assert_origin_auto );  // Leading Edge Detection Control
    Assert_RADIO_EXTRA( rdr_DIG_DIAG     == _radio_dw1000_compile_transaction_header( 0x2f, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Digital Diagnostics Interface
    Assert_RADIO_EXTRA( rdr_PMSC_CTRL0   == _radio_dw1000_compile_transaction_header( 0x36, 0x0000, 0 ).Word, ttc_assert_origin_auto );  // Power Management and System Control 0
    Assert_RADIO_EXTRA( rdr_PMSC_CTRL1   == _radio_dw1000_compile_transaction_header( 0x36, 0x0004, 0 ).Word, ttc_assert_origin_auto );  // Power Management and System Control 1
    Assert_RADIO_EXTRA( rdr_PMSC_SNOZT   == _radio_dw1000_compile_transaction_header( 0x36, 0x000c, 0 ).Word, ttc_assert_origin_auto );  // PMSC Snooze Time
    Assert_RADIO_EXTRA( rdr_PMSC_TXFSEQ  == _radio_dw1000_compile_transaction_header( 0x36, 0x0026, 0 ).Word, ttc_assert_origin_auto );  // PMSC fine grain TX sequencinfg control
    Assert_RADIO_EXTRA( rdr_PMSC_LEDC    == _radio_dw1000_compile_transaction_header( 0x36, 0x0028, 0 ).Word, ttc_assert_origin_auto );  // PMSC LED Control

    // check values required by _radio_dw1000_ACK_TIM[]
    Assert_RADIO_EXTRA( radio_dw1000_datarate_110k == 0, ttc_assert_origin_auto );
    Assert_RADIO_EXTRA( radio_dw1000_datarate_850k == 1, ttc_assert_origin_auto );
    Assert_RADIO_EXTRA( radio_dw1000_datarate_6M8  == 2, ttc_assert_origin_auto );

#endif
}
BOOL                     radio_dw1000_receiver( t_ttc_radio_config* Config, BOOL Enable, u_ttc_packetimestamp_40* ReferenceTime, t_base TimeOut_us ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    t_radio_dw1000_registers* Registers = &( Config->LowLevelConfig.Registers );

    TODO( "implement SNIFF mode (DW1000 UserManual p.32)" )
    TODO( "implement Receive Frame Wait TimeOut" )

    Registers->SYS_CTRL.Word = 0;
    if ( Enable ) {
        if ( ReferenceTime ) { // switch on receiver after delay
            // Calculating delayed transmit time
            radio_common_add_40_32_40( ReferenceTime,
                                       Config->LowLevelConfig.DelayAdd_RX2TX,
                                       & Config->LowLevelConfig.Registers.DX_TIME
                                     );
            _radio_dw1000_register_write_DX_TIME( Config );

            Registers->SYS_CTRL.Fields.RXDLYE = 1; // enable receiver delay
        }
        else {                 // switch on receiver now
            Registers->SYS_CFG.Fields.RXAUTR    = 1; // enable receiver auto re-enable to keep receiver on until switched off manually
            Registers->PMSC_CTRL1.Fields.ARXSLP = 0; // disable receiver auto sleep after receiving a frame
            _radio_dw1000_register_write_SYS_CFG( Config );
        }
        Registers->SYS_CTRL.Fields.RXENAB = 1; // enable receiver
        ttc_mutex_lock_try( &( Config->Flag_ReceiverOn ) );
        _radio_dw1000_register_write_SYS_CTRL( Config );
        if ( ReferenceTime ) {
            _radio_dw1000_register_read_SYS_STATUS( Config );
            Assert_RADIO( Registers->SYS_STATUS.Fields.HPDWARN == 0, ttc_assert_origin_auto );  // delayed start is >8secs in the future (->DW1000 UserManual p.90, p.35)

            TODO( "check if delayed receive could be started" )
        }
        _radio_dw1000_udelay( 20 ); // allow receiver to power up
    }
    else {                     // disable receiver
        Registers->SYS_CTRL.Fields.TRXOFF = 1; // disable transmitter/receiver and return to idle
        ttc_mutex_unlock( &( Config->Flag_ReceiverOn ) );
        _radio_dw1000_register_write_SYS_CTRL( Config );
        while ( Registers->SYS_CTRL.Fields.TRXOFF ) {
            _radio_dw1000_register_read_SYS_CTRL( Config );
        }
    }

    return TRUE;
}
e_ttc_radio_errorcode    radio_dw1000_reset( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    if ( Config->Flags.Initialized ) {
        radio_dw1000_deinit( Config );
        Config->Flags.Initialized = 0;
        radio_dw1000_init( Config );
    }

    return ( e_ttc_radio_errorcode ) 0;
}
void                     radio_dw1000_transmit( t_ttc_radio_config* Config, u_ttc_packetimestamp_40* TransmitTime ) {
    /** This function will only start transmission if no transmission is currently running.
     *  If a transmission is already running, radio_dw1000_isr_generic() will transmit all packets in List_PacketsTx automatically.
     *
     *  Runtime of radio_dw1000_transmit() depends of availability of interrupt line from DW1000:
     *  Enable_Output_IRQ==1: return after starting transmission
     *  Enable_Output_IRQ==0: wait until packet has been transmitted
     */

    if ( radio_common_InsideISR ) { // got called while inside interrupt service routine (special case for radio_common_ranging_reply_isr() )
        _radio_dw1000_isr_transmit_next( Config, TransmitTime );
        return;
    }

    if ( Config->RadioInterrupt ) { // transmission handled by interrupt service routine: initiate interrupt to spawn it
        //?required ttc_interrupt_enable_gpio( Config->RadioInterrupt );
        // we have to re-enable interrupts to ensure that interrupt service routine is run now
        t_u8 PreviousCriticalLevel = ttc_interrupt_critical_end_all(); // ensure that interrupts are enabled

#if (TTC_ASSERT_RADIO_EXTRA == 1)
        if ( 0 ) {
            TODO( "Reactivate transmit check and find out why it hangs sometimes!" )
            if ( TransmitTime == NULL ) {
                if ( Config->Init.Transceiver_Timeout_usecs ) { // check if packet is processed by interrupt service routine
                    radio_dw1000_isr_LowLevelConfig->StatusISR = 0; // reset status to allow updates
                    _radio_dw1000_isr_raise( Config ); // generate software interrupt to run radio_dw1000_isr_generic()
                    ttc_systick_delay_init( & Config->TimeOut, Config->Init.Transceiver_Timeout_usecs );
                    while ( ( Config->LowLevelConfig.StatusISR < rdsi_transmit_complete ) &&
                            !ttc_systick_delay_expired( & Config->TimeOut )
                          ) // actively wait for isr to run
                    { ttc_task_yield(); } // ToDo: Maybe giving CPU to other tasks makes sending multiple packets too slow
                    Assert_RADIO_EXTRA( Config->LowLevelConfig.StatusISR >= rdsi_transmit_complete, ttc_assert_origin_auto ); // transmission did not start!
                    goto rdt_RestoreCriticalLevel; // have to jump over _radio_dw1000_isr_raise() call as we already called it
                }
            }
        }
#endif
        _radio_dw1000_isr_raise( Config ); // generate software interrupt to run radio_dw1000_isr_generic()

#if (TTC_ASSERT_RADIO_EXTRA == 1)
    rdt_RestoreCriticalLevel:
#endif
        if ( PreviousCriticalLevel )
        { ttc_interrupt_critical_restore( PreviousCriticalLevel ); }
    }
    else {                          // No interrupts available: issue some SPI communications outside interrupt service routine can interfere with incoming interrupts from DW1000)
        if ( ttc_mutex_lock_try( & ( Config->FlagRunningTX ) ) ) // transmission already ongoing
        { return; }   // packet transmission will automatically start later (controlled by radio_dw1000_isr_generic() )

        t_ttc_packet* Packet = radio_common_pop_list_tx( Config );
        if ( Packet ) {
            if ( Config->Init.Flags.Enable_Output_IRQ ) { // interrupts available: start tx of first packet (rest is controlled by radio_dw1000_isr_generic())
                volatile BOOL DoNotReleaseBuffer = Packet->Meta.DoNotReleaseBuffer;

                if ( Config->Init.function_start_tx_isr )
                {  DoNotReleaseBuffer |= Config->Init.function_start_tx_isr( Config, Packet ); }

                _radio_dw1000_tx_prepare( Config, Packet );

                // start transmission
                _radio_dw1000_tx_start( Config, TransmitTime, &( Packet->Meta ) );

                if ( Config->Init.function_end_tx_isr ) { // activity function defined: call it
                    DoNotReleaseBuffer |= Config->Init.function_end_tx_isr( Config, Packet );
                }

                if ( !DoNotReleaseBuffer )
                { radio_common_packet_release( Packet ); } // return packet to its memory pool
            }
            else {                                        // no interrupts available: send out all queued packets from here
                ttc_task_critical_begin();
                do {
                    volatile BOOL DoNotReleaseBuffer = Packet->Meta.DoNotReleaseBuffer;

                    if ( Config->Init.function_start_tx_isr )
                    {  DoNotReleaseBuffer |= Config->Init.function_start_tx_isr( Config, Packet ); }

                    _radio_dw1000_tx_prepare( Config, Packet );

                    // start transmission
                    _radio_dw1000_tx_start( Config, TransmitTime, &( Packet->Meta ) );
                    RADIO_COMMON_COUNT( Amount_Tx );

                    if ( Config->Init.function_end_tx_isr ) { // activity function defined: call it
                        DoNotReleaseBuffer |= Config->Init.function_end_tx_isr( Config, Packet );
                    }

                    if ( !DoNotReleaseBuffer )
                    { radio_common_packet_release( Packet ); } // return packet to its memory pool

                    // check for more packets to be transmitted
                    Packet = radio_common_pop_list_tx( Config );
                }
                while ( Packet );
                ttc_task_critical_end();
            }
        }

        if ( !Config->Init.Flags.Enable_Output_IRQ ) { // no interrupts available: we end this transmission
            ttc_mutex_unlock( & ( Config->FlagRunningTX ) );
        }
    }
}
e_ttc_radio_errorcode    radio_dw1000_update_meta( t_ttc_radio_config* Config, t_ttc_packet* Packet ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_RADIO_EXTRA_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    // Processing received frame meta data being stored by _radio_dw1000_isr_read_received_packet()

    t_u8 RxMetaIndex = Packet->Meta.IndexPhysical;
    if ( !RxMetaIndex )
    { return ec_radio_NoMetaData; }

    t_radio_dw1000_frame_meta* volatile CurrentFrameMeta = &( radio_dw1000_RxFrameMeta[RxMetaIndex - 1] ); // index was increased by 1

    if ( Config->Init.Flags.EnableRSSI ) { // calculate RSSI value for this packet
        t_base C = CurrentFrameMeta->RX_FQUAL.Fields.CIR_PWR;
        t_base N = CurrentFrameMeta->RX_FINFO.Fields.RXPACC;
        t_base A = ( CurrentFrameMeta->RX_FINFO.Fields.RXPRF == rd_chan_ctrl_prf_16MHz ) ? 115720 : 121740;
        Packet->Meta.RSSI_mdBm = ( 10000 * ttc_basic_int_rankN( ( C << 17 ) / ( N * N ), 10 ) ) - A;

        if ( 1 ) { // adjust RSSI_mdBm according to DW1000 UserManual p.47 fig.22)
            if ( Packet->Meta.RSSI_mdBm >= -89000 ) { // assuming linearity below -89 dBm
                const static ttm_number radio_dw1000_RX_PowerCorrection_PRF16MHz[] = { // taken from Fig.22 curve Extimated RX LEVEL (16MHz PRF Free Space)
                    ( ttm_number ) - 79.0 / -64.2,
                    ( ttm_number ) - 80.0 / -65.0,
                    ( ttm_number ) - 81.0 / -66.5,
                    ( ttm_number ) - 82.0 / -67.0,
                    ( ttm_number ) - 83.0 / -71.0,
                    ( ttm_number ) - 84.0 / -75.0,
                    ( ttm_number ) - 85.0 / -78.0,
                    ( ttm_number ) - 86.0 / -82.0,
                    ( ttm_number ) - 87.0 / -84.0,
                    ( ttm_number ) - 88.0 / -86.5,
                    ( ttm_number ) - 89.0 / -88.0,
                };
                const static ttm_number radio_dw1000_RX_PowerCorrection_PRF64MHz[] = { // taken from Fig.22 curve Extimated RX LEVEL (64MHz PRF Free Space)
                    ( ttm_number ) - 79.0 / -66.0,
                    ( ttm_number ) - 80.0 / -72.0,
                    ( ttm_number ) - 81.0 / -77.0,
                    ( ttm_number ) - 82.0 / -79.0,
                    ( ttm_number ) - 83.0 / -81.0,
                    ( ttm_number ) - 84.0 / -82.5,
                    ( ttm_number ) - 85.0 / -83.5,
                    ( ttm_number ) - 86.0 / -85.0,
                    ( ttm_number ) - 87.0 / -86.0,
                    ( ttm_number ) - 88.0 / -87.0,
                    ( ttm_number ) - 89.0 / -88.0,
                };
                t_u8 Index = ( ( abs( Packet->Meta.RSSI_mdBm ) + 500 ) / 1000 ) - 79; // first array value is -79dBm
                Packet->Meta.RSSI_mdBm *= ( CurrentFrameMeta->RX_FINFO.Fields.RXPRF == rd_chan_ctrl_prf_16MHz ) ?
                                          radio_dw1000_RX_PowerCorrection_PRF16MHz[Index] : // factor for 16MHz PRF
                                          radio_dw1000_RX_PowerCorrection_PRF64MHz[Index];  // factor for 64MHz PRF
            }
        }
    }

    return ec_radio_OK;
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Public Function Definitions
//{ Private Functions (ideally) ************************************************

void                                _radio_dw1000_udelay( t_base Delay_us ) {

    // have to switch to proper delay function
    if ( ttc_interrupt_check_inside_isr() )
    { ttc_task_udelay_isr( Delay_us ); }
    else
    { ttc_task_udelay( Delay_us ); }
}
void                                _radio_dw1000_delay_init( t_ttc_systick_delay* Delay, t_base Time_us ) {

    // have to switch to proper delay function
    if ( ttc_interrupt_check_inside_isr() || // inside interrupt service routine
            ttc_interrupt_critical_level()      // interrupts disabled
       )
    { ttc_systick_delay_init_isr( Delay, Time_us ); }
    else
    { ttc_systick_delay_init( Delay, Time_us ); }
}
BOOL                                _radio_dw1000_delay_check( t_ttc_systick_delay* Delay ) {

    // have to switch to proper delay function
    if ( ttc_interrupt_check_inside_isr() || // inside interrupt service routine
            ttc_interrupt_critical_level()      // interrupts disabled
       )
    { return ttc_systick_delay_expired_isr( Delay ); }
    else
    { return ttc_systick_delay_expired( Delay ); }
}
void                                _radio_dw1000_aon_upload( t_ttc_radio_config* Config ) {

    u_radio_dw1000_register_aon_ctrl* AON_CTRL = &( Config->LowLevelConfig.Registers.AON_CTRL );

    // clear all AON auto download bits
    Config->LowLevelConfig.Registers.AON_WCFG.Word = 0;
    _radio_dw1000_register_write_AON_WCFG( Config );

    // clear control
    AON_CTRL->Byte = 0;
    _radio_dw1000_register_write_AON_CTRL( Config );
    _radio_dw1000_udelay( 10 );

    // activate upload process
    AON_CTRL->Fields.SAVE = 1;
    _radio_dw1000_register_write_AON_CTRL( Config );

    if ( 0 ) { // wait for SAVE bit to clear automatically as described in DW1000 datasheet p. 154 (DOES NOT WORK!)
        t_u8 TimeOut = 100;
        while ( AON_CTRL->Fields.SAVE ) {
            _radio_dw1000_udelay( 10 );
            _radio_dw1000_register_read_AON_CTRL( Config );
            TimeOut--;
            Assert_RADIO( TimeOut, ttc_assert_origin_auto );
        }
    }
    else {   // manually clear save bit after 10 usecs similar to of DecaWave ranging example
        _radio_dw1000_udelay( 10 );
        AON_CTRL->Fields.SAVE = 0;
        _radio_dw1000_register_write_AON_CTRL( Config );
    }
}
BOOL                                _radio_dw1000_check_receiver_overrun( t_ttc_radio_config* Config ) {

    return _radio_dw1000_read_sys_status( Config )->Fields.RXOVRR;
}
t_radio_dw1000_transaction          _radio_dw1000_compile_transaction_header( t_u8 RegisterIndex, t_u16 RegisterSubIndex, BOOL WriteOperation ) {
    t_radio_dw1000_transaction TransactionHeader;

    TransactionHeader.Word = 0;
    TransactionHeader.Fields.Index = RegisterIndex & 0x3f;
    if ( RegisterSubIndex ) {
        TransactionHeader.Fields.SubIndexPresent = 1;
        TransactionHeader.Fields.SubIndex = RegisterSubIndex & 0x7f;
        if ( RegisterSubIndex > 0x7f ) {
            TransactionHeader.Fields.ExtendedPresent = 1;
            TransactionHeader.Fields.SubIndexHigh = ( RegisterSubIndex >> 7 ) & 0xff;
            TransactionHeader.Fields.HeaderLength = 3;
        }
        else
        { TransactionHeader.Fields.HeaderLength = 2; }
    }
    else
    { TransactionHeader.Fields.HeaderLength = 1; }

    if ( WriteOperation )
    { TransactionHeader.Fields.WriteOperation = 1; }

    return TransactionHeader;
}
void                                _radio_dw1000_configure_clocking( t_ttc_radio_config* Config, e_radio_dw1000_clocking_scheme ClockingScheme ) {

    _radio_dw1000_register_read_PMSC_CTRL0( Config );
    u_radio_dw1000_register_pmsc_ctrl0* PMSC_CTRL0 = & Config->LowLevelConfig.Registers.PMSC_CTRL0;

    // ensure that we can communicate with DW1000 even after switching clock
    _radio_dw1000_configure_spi( Config, radio_dw1000_spi_speed_Min );

    switch ( ClockingScheme ) {
        case rdcs_force_sysclk_xti: {
            PMSC_CTRL0->Fields.SYSCLKS = rdci_19_2MHz_XTI;
            break;
        }
        case rdcs_enable_all_seq: {
            PMSC_CTRL0->Fields.SYSCLKS = rdci_auto;
            PMSC_CTRL0->Fields.RXCLKS  = rdci_auto;
            PMSC_CTRL0->Fields.TXCLKS  = rdci_auto;
            PMSC_CTRL0->Fields.FACE    = 0;
            PMSC_CTRL0->Fields.ADCCE   = 0;
            PMSC_CTRL0->Fields.AMCE    = 0;
            break;
        }
        case rdcs_force_sysclk_pll: {
            PMSC_CTRL0->Fields.SYSCLKS = rdci_125MHz_PLL;
            break;
        }
        case rdcs_read_acc_on: {
            PMSC_CTRL0->Fields.RXCLKS = rdci_125MHz_PLL;
            PMSC_CTRL0->Fields.FACE   = 1;
            PMSC_CTRL0->Fields.AMCE   = 1;
            break;
        }
        case rdcs_read_acc_off: {
            PMSC_CTRL0->Fields.RXCLKS = rdci_auto;
            PMSC_CTRL0->Fields.FACE   = 0;
            PMSC_CTRL0->Fields.AMCE   = 0;
            break;
        }
        case rdcs_force_otp_on: {
            PMSC_CTRL0->Fields.OTP = 1; // info extracted from deprecated demo code case rdcs_force_otp_on below
            break;
        }
        case rdcs_force_otp_off: {
            PMSC_CTRL0->Fields.OTP = 0;
            break;
        }
        case rdcs_force_tx_pll: {
            PMSC_CTRL0->Fields.TXCLKS  = rdci_125MHz_PLL;
            break;
        }
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown clocking scheme!
    };

    /* DEPRECATED (unreadable Hex Stuff)
        t_u16 clk;
        t_u8 clk_H;
        t_u8 clk_L;

        //Low byte
        clk_L = (t_u8)(0x00FF&clk);
        //High byte
        clk_H = (t_u8)((0xFF00&clk)>>8);

        t_u32 clocks;
        _radio_dw1000_spi_register_read(Config, rdr_PMSC_CTRL0, sizeof(clocks), (t_u8*) &clocks);
        if (radio_dw1000_InsertSleeps) ttc_task_msleep(50);


        switch (Clocks) {
        case rdcs_enable_all_seq:
        {
            clk_L = 0x00;
            clk_H = clk_H & 0xfe;
            break;
        }
        case rdcs_force_sysclk_xti:
        {
            //system and rx
            clk_L = 0x01 | (clk_L & 0xfc);
        }
            break;
        case rdcs_force_sysclk_pll:
        {
            //system
            clk_L = 0x02 | (clk_L & 0xfc);
        }
            break;
        case rdcs_read_acc_on:
        {
            clk_L = 0x48 | (clk_L & 0xb3);
            clk_H = 0x80 | clk_H;
        }
            break;
        case rdcs_read_acc_off:
        {
            clk_L = clk_L & 0xb3;
            clk_H = 0x7f & clk_H;

        }
            break;
        case rdcs_force_otp_on:
        {
            clk_H = 0x02 | clk_H;
        }
            break;
        case rdcs_force_otp_off:
        {
            clk_H = clk_H & 0xfd;
        }
            break;
        case rdcs_force_tx_pll:
        {
            clk_L = 0x20| (clk_L & 0xcf);
        }
        default:
            break;
        }
    */

    if ( ( PMSC_CTRL0->Fields.SYSCLKS == rdci_125MHz_PLL ) ||
            ( PMSC_CTRL0->Fields.TXCLKS  == rdci_125MHz_PLL ) ||
            ( PMSC_CTRL0->Fields.RXCLKS  == rdci_125MHz_PLL ) ||
            ( PMSC_CTRL0->Fields.SYSCLKS == rdci_auto )       ||
            ( PMSC_CTRL0->Fields.TXCLKS  == rdci_auto )       ||
            ( PMSC_CTRL0->Fields.RXCLKS  == rdci_auto )
       ) {

        _radio_dw1000_register_read_EC_CTRL( Config );
        u_radio_dw1000_register_ec_ctrl* EC_CTRL = & Config->LowLevelConfig.Registers.EC_CTRL;

        // At least one PLL is active: Activate PLL lock detect tuning for stable high speed operation -> DW1000 UserManual p 85, 119
        if ( !EC_CTRL->Fields.PLLLDT ) { // PLL lock detect tune is inactive: activate it
            EC_CTRL->Fields.PLLLDT = 1;
            _radio_dw1000_register_write_EC_CTRL( Config );

            u_radio_dw1000_register_sys_status* SysStatus = _radio_dw1000_read_sys_status( Config );
            if ( ! SysStatus->Fields.CPLOCK ) {
                t_u8 TimeOut = -1;
                do { // wait until PLL has locked in
                    _radio_dw1000_register_read_SYS_STATUS( Config );
                    Assert_RADIO( *( ( t_u32* ) SysStatus->Bytes ) != 0xffffffff, ttc_assert_origin_auto );  // DW1000 seems to have stopped SPI communication (received only ones!)
                    Assert_RADIO( TimeOut, ttc_assert_origin_auto );  // radio PLL did not lock within timeout
                    TimeOut--;
                }
                while ( ! SysStatus->Fields.CPLOCK );
                _radio_dw1000_register_write_SYS_STATUS( Config );
            }
        }
    }
    _radio_dw1000_register_write_PMSC_CTRL0( Config );

    if ( ( PMSC_CTRL0->Fields.SYSCLKS == rdci_125MHz_PLL ) || // switch back to highspeed SPI
            ( PMSC_CTRL0->Fields.SYSCLKS == rdci_auto )
       ) {
        _radio_dw1000_configure_spi( Config, radio_dw1000_spi_speed_Max );
    }

    Config->LowLevelConfig.ClockingScheme = ClockingScheme;
    ttc_task_check_stack(); // a good place to check for a stack overrun
}
void                                _radio_dw1000_configure_synthesizer( t_ttc_radio_config* Config, t_u8 ChannelNo ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    t_radio_dw1000_registers* Registers = &( Config->LowLevelConfig.Registers );

    switch ( ChannelNo ) { // configure synthesizer according to DW1000 UserManual p.148
        case 1: {
            Registers->TC_PGDELAY = 0xc9;
            Registers->FS_PLLCFG  = 0x09000407;
            Registers->FS_PLLTUNE = 0x1e;
            break;
        }
        case 2: {
            Registers->TC_PGDELAY = 0xc2;
            Registers->FS_PLLCFG  = 0x08400508;
            Registers->FS_PLLTUNE = 0x26;
            break;
        }
        case 3: {
            Registers->TC_PGDELAY = 0xc5;
            Registers->FS_PLLCFG  = 0x08401009;
            Registers->FS_PLLTUNE = 0x5e;
            break;
        }
        case 4: {
            Registers->TC_PGDELAY = 0x95;
            Registers->FS_PLLCFG  = 0x08400508;
            Registers->FS_PLLTUNE = 0x26;
            break;
        }
        case 5: {
            Registers->TC_PGDELAY = 0xc0;
            Registers->FS_PLLCFG  = 0x0800041D;
            Registers->FS_PLLTUNE = 0xbe;
            break;
        }
        case 7: {
            Registers->TC_PGDELAY = 0x93;
            Registers->FS_PLLCFG  = 0x0800041D;
            Registers->FS_PLLTUNE = 0xbe;
            break;
        }
        default: {
            ttc_assert_halt_origin( ec_radio_InvalidConfiguration ); // illegal channel no!
            break;
        }
    };
    Registers->FS_XTALT.Fields.reserved = 0b011; // must set this value (->DW1000 UserManual p.150)

    _radio_dw1000_register_write_FS_PLLTUNE( Config );
    _radio_dw1000_register_write_TC_PGDELAY( Config );
    _radio_dw1000_register_write_FS_PLLCFG( Config );
    _radio_dw1000_register_write_FS_XTALT( Config );
}
void                                _radio_dw1000_configure_spi( t_ttc_radio_config* Config, e_radio_dw1000_spi_speed Speed ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );
    Assert_RADIO_EXTRA( Speed < radio_dw1000_spi_speed_unknown, ttc_assert_origin_auto );

    if ( Config->LowLevelConfig.SpeedSPI == Speed )
    { return; } // no change: do nothing
    Config->LowLevelConfig.SpeedSPI = Speed;

    t_u8 LogicalIndex_SPI = Config->LowLevelConfig.LogicalIndex_SPI;
    Assert_RADIO( LogicalIndex_SPI, ttc_assert_origin_auto );                            // must be set as logical index of SPI device to use to communicate with DW1000!
    Assert_RADIO( Config->LowLevelConfig.LogicalIndex_SPI_NSS, ttc_assert_origin_auto );  // must be set as logical index of Slave Select pin to use for DW1000!

    BOOL ResetDevice = FALSE;
    t_ttc_spi_config* ConfigSPI = ttc_spi_get_configuration( LogicalIndex_SPI );
    if ( !ConfigSPI->Flags.Initialized ) { // configured SPI not yet initialized: initialize it now
        ConfigSPI->Init.Flags.Receive           = 1;
        ConfigSPI->Init.Flags.Transmit          = 1;

        ConfigSPI->Init.Flags.Master            = 1;
        ConfigSPI->Init.Flags.Slave             = 0;
        ConfigSPI->Init.Flags.WordSize16        = 0;
        ConfigSPI->Init.Flags.FirstBitMSB       = 1;
        ConfigSPI->Init.Flags.HardwareNSS       = 0;
        ConfigSPI->Init.Flags.ClockIdleHigh     = 0;
        ConfigSPI->Init.Flags.ClockPhase2ndEdge = 0;
        ConfigSPI->Init.Flags.Bidirectional     = 0;
        ConfigSPI->Init.Flags.SSOutput          = 0;
        ConfigSPI->Init.Flags.CRC8              = 0;


        // Configure SPI settings of DW1000 according to current SPI bus settings (if gpio pins are defined)
        // Note: The DW1000 allows to configure it's SPI bus mode according to the levels of pins SPIPOL, SPIMODE during reset.
        //       For this to work, the DW1000 has to be wired to the uC as follows:
        //
        //        DW1000 #1 |  Microcontroller
        //       -----------+--------------------
        //          SPIPOL  |  gpio pin TTC_RADIO1_DW1000_PIN_SPIPOL
        //          SPIPHA  |  gpio pin TTC_RADIO1_DW1000_PIN_SPIPHA
        //
        //        DW1000 #2 |  Microcontroller
        //       -----------+--------------------
        //          SPIPOL  |  gpio pin TTC_RADIO2_DW1000_PIN_SPIPOL
        //          SPIPHA  |  gpio pin TTC_RADIO2_DW1000_PIN_SPIPHA
        //       ...

        if ( Config->LowLevelConfig.Pin_SPIPOL ) {
            ttc_gpio_init( Config->LowLevelConfig.Pin_SPIPOL, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min );
            ttc_gpio_put( Config->LowLevelConfig.Pin_SPIPOL, ConfigSPI->Init.Flags.ClockIdleHigh );
        }
        if ( Config->LowLevelConfig.Pin_SPIPOL ) {
            ttc_gpio_init( Config->LowLevelConfig.Pin_SPIPHA, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min );
            ttc_gpio_put( Config->LowLevelConfig.Pin_SPIPHA, ConfigSPI->Init.Flags.ClockPhase2ndEdge );
        }

        // DW1000 requires hard reset after changing SPIPOL, SPIPHA levels to reconfigure its SPI bus!
        ResetDevice = TRUE;
    }

    // DW1000 allows 3MHz SPI speed during WAKEUP and INIT. In IDLE it allows 20MHz SPI speed.
    //Speed = radio_dw1000_spi_speed_Min; //DEBUG  having issues with higher SPI-speeds on uwb_sensor_10 board

    ConfigSPI->Init.BaudRate = ( Speed == radio_dw1000_spi_speed_Min ) ? 3000000 : 20000000;

    t_ttc_gpio_config GPIO;
    e_ttc_gpio_speed  NewSpeed;
    if ( Speed == radio_dw1000_spi_speed_Min ) { NewSpeed = E_ttc_gpio_speed_min; } // configure SPI pins for minimum speed
    else                                       { NewSpeed = E_ttc_gpio_speed_max; } // configure SPI pins for maximum speed

    // copy gpio configuration from current settings except new speed
    ttc_gpio_get_configuration( ConfigSPI->Init.Pin_MOSI, &GPIO );
    ttc_gpio_init( ConfigSPI->Init.Pin_MOSI, GPIO.Mode, NewSpeed );
    ttc_gpio_get_configuration( ConfigSPI->Init.Pin_SCLK, &GPIO );
    ttc_gpio_init( ConfigSPI->Init.Pin_SCLK, GPIO.Mode, NewSpeed );

#if (TTC_ASSERT_RADIO == 1)
    e_ttc_spi_errorcode Error = ttc_spi_init( ConfigSPI->LogicalIndex );
    Assert_RADIO( !Error, ttc_assert_origin_auto );
#else
    ttc_spi_init( ConfigSPI->LogicalIndex );
#endif

    if ( ResetDevice ) // DW1000 reset required to accept new SPI configuration
    { _radio_dw1000_reset_hard( Config ); }
    if ( 1 ) {         // check if spi communication is stable
        t_radio_dw1000_registers* Registers = &( Config->LowLevelConfig.Registers );
        t_base PreviousBaudRate = 0;
        t_u8 RetryAll = 2;
        do {
            do {
                BOOL TestFailed = FALSE;
                for ( t_u8 Retries = 4; Retries > 0; Retries-- ) { // value must be read several times to trust in SPI communication
                    _radio_dw1000_register_read_DEV_ID( Config );
                    if ( Registers->DEV_ID != 0xDECA0130 )
                    { TestFailed = TRUE; }
                }

                if ( TestFailed ) { // cannot communicate with DW1000: reduce SPI baudrate
                    PreviousBaudRate = ConfigSPI->Init.BaudRate;
                    ConfigSPI->Init.BaudRate --; // low-level driver will switch to next lower, available bitrate
#if (TTC_ASSERT_RADIO == 1)
                    Error = ttc_spi_init( ConfigSPI->LogicalIndex ); // configure SPI for reduced baudrate
                    Assert_RADIO( !Error, ttc_assert_origin_auto );
#else
                    ttc_spi_init( ConfigSPI->LogicalIndex ); // configure SPI for reduced baudrate
#endif
                }
                else
                { break; }
            }
            while ( PreviousBaudRate != ConfigSPI->Init.BaudRate ); // retry while spi-bus allows to reduce bitrate
            if ( Registers->DEV_ID != 0xDECA0130 ) { // cannot establish communication with DW1000: reset and try again
                _radio_dw1000_reset_hard( Config );
            }
        }
        while ( RetryAll-- && ( Registers->DEV_ID != 0xDECA0130 ) );

        Assert_RADIO( Registers->DEV_ID == 0xDECA0130, ttc_assert_origin_auto );  // cannot establish communication with DW1000!
    }

    return;
}
void                                _radio_dw1000_decode_channel( e_radio_dw1000_virtual_channel VirtualChannel, t_u8* ChannelNo, t_u32* Frequency_100KHz, t_u32* Bandwidth_100KHz, t_u8* PreambleCode ) {
    t_u8  _ChannelNo = 0;
    t_u32 _Frequency_100KHz = 0;
    t_u32 _Bandwidth_100KHz = 0;
    t_u8  _PreambleCode     = 0;

    // This approach is not the fastest but very readable
    if ( VirtualChannel <= rdvc_virtual7_freq3993_bw499_PRC3 )   {      // frequency channel 1
        _ChannelNo = 1;
        _Frequency_100KHz = 34944;
        _Bandwidth_100KHz = 4992;

        switch ( VirtualChannel ) {
            case rdvc_virtual1_freq3494_bw499_PRC1:    _PreambleCode = 1;   break;
            case rdvc_virtual2_freq3494_bw499_PRC2:    _PreambleCode = 2;   break;
            case rdvc_virtual3_freq3494_bw499_PRC9:    _PreambleCode = 9;   break;
            case rdvc_virtual4_freq3494_bw499_PRC10:   _PreambleCode = 10;  break;
            case rdvc_virtual5_freq3494_bw499_PRC11:   _PreambleCode = 11;  break;
            case rdvc_virtual6_freq3494_bw499_PRC12:   _PreambleCode = 12;  break;
            default: break;
        }
    }
    else if ( VirtualChannel <= rdvc_virtual13_freq4492_bw499_PRC5 )  { // frequency channel 2
        _ChannelNo = 2;
        _Frequency_100KHz = 39936;
        _Bandwidth_100KHz = 4992;

        switch ( VirtualChannel ) {
            case rdvc_virtual7_freq3993_bw499_PRC3:    _PreambleCode = 3;   break;
            case rdvc_virtual8_freq3993_bw499_PRC4:    _PreambleCode = 4;   break;
            case rdvc_virtual9_freq3993_bw499_PRC9:    _PreambleCode = 9;   break;
            case rdvc_virtual10_freq3993_bw499_PRC10:  _PreambleCode = 10;  break;
            case rdvc_virtual11_freq3993_bw499_PRC11:  _PreambleCode = 11;  break;
            case rdvc_virtual12_freq3993_bw499_PRC12:  _PreambleCode = 12;  break;
            default: break;
        }
    }
    else if ( VirtualChannel <= rdvc_virtual19_freq3993_bw1331_PRC7 ) { // frequency channel 3
        _ChannelNo = 3;
        _Frequency_100KHz = 44928;
        _Bandwidth_100KHz = 4992;

        switch ( VirtualChannel ) {
            case rdvc_virtual13_freq4492_bw499_PRC5:   _PreambleCode = 5;   break;
            case rdvc_virtual14_freq4492_bw499_PRC6:   _PreambleCode = 6;   break;
            case rdvc_virtual15_freq4492_bw499_PRC9:   _PreambleCode = 9;   break;
            case rdvc_virtual16_freq4492_bw499_PRC10:  _PreambleCode = 10;  break;
            case rdvc_virtual17_freq4492_bw499_PRC11:  _PreambleCode = 11;  break;
            case rdvc_virtual18_freq4492_bw499_PRC12:  _PreambleCode = 12;  break;
            default: break;
        }
    }
    else if ( VirtualChannel <= rdvc_virtual25_freq6489_bw499_PRC3 )  { // frequency channel 4
        _ChannelNo = 4;
        _Frequency_100KHz = 34944;
        _Bandwidth_100KHz = 4992;

        switch ( VirtualChannel ) {
            case rdvc_virtual19_freq3993_bw1331_PRC7:  _PreambleCode = 7;   break;
            case rdvc_virtual20_freq3993_bw1331_PRC8:  _PreambleCode = 8;   break;
            case rdvc_virtual21_freq3993_bw1331_PRC17: _PreambleCode = 17;  break;
            case rdvc_virtual22_freq3993_bw1331_PRC18: _PreambleCode = 18;  break;
            case rdvc_virtual23_freq3993_bw1331_PRC19: _PreambleCode = 19;  break;
            case rdvc_virtual24_freq3993_bw1331_PRC20: _PreambleCode = 20;  break;
            default: break;
        }
    }
    else if ( VirtualChannel <= rdvc_virtual31_freq6489_bw1081_PRC7 ) { // frequency channel 5
        _ChannelNo = 5;
        _Frequency_100KHz = 34944;
        _Bandwidth_100KHz = 4992;

        switch ( VirtualChannel ) {
            case rdvc_virtual25_freq6489_bw499_PRC3:   _PreambleCode = 3;   break;
            case rdvc_virtual26_freq6489_bw499_PRC4:   _PreambleCode = 4;   break;
            case rdvc_virtual27_freq6489_bw499_PRC9:   _PreambleCode = 9;   break;
            case rdvc_virtual28_freq6489_bw499_PRC10:  _PreambleCode = 10;  break;
            case rdvc_virtual29_freq6489_bw499_PRC11:  _PreambleCode = 11;  break;
            case rdvc_virtual30_freq6489_bw499_PRC12:  _PreambleCode = 12;  break;
            default: break;
        }
    }
    else if ( VirtualChannel <= rdvc_virtual_unknown )                { // frequency channel 7 (channel 6 is not supported by DW1000!)
        _ChannelNo = 7;
        _Frequency_100KHz = 34944;
        _Bandwidth_100KHz = 4992;

        switch ( VirtualChannel ) {
            case rdvc_virtual31_freq6489_bw1081_PRC7:  _PreambleCode = 7;   break;
            case rdvc_virtual32_freq6489_bw1081_PRC8:  _PreambleCode = 8;   break;
            case rdvc_virtual33_freq6489_bw1081_PRC17: _PreambleCode = 17;  break;
            case rdvc_virtual34_freq6489_bw1081_PRC18: _PreambleCode = 18;  break;
            case rdvc_virtual35_freq6489_bw1081_PRC19: _PreambleCode = 19;  break;
            case rdvc_virtual36_freq6489_bw1081_PRC20: _PreambleCode = 20;  break;
            default: break;
        }
    }
    else {
        ttc_assert_halt_origin( ec_radio_InvalidConfiguration ); // invalid channel number!
    }
    Assert_RADIO( _ChannelNo > 0, ttc_assert_origin_auto );  // could not decode given virtual Channel!

    // store decoded channel parameters
    if ( ChannelNo )        {
        Assert_RADIO_EXTRA_Writable( ChannelNo, ttc_assert_origin_auto );  // got invalid pointer!
        *ChannelNo        = _ChannelNo;
    }
    if ( Frequency_100KHz ) {
        Assert_RADIO_EXTRA_Writable( Frequency_100KHz, ttc_assert_origin_auto );  // got invalid pointer!
        *Frequency_100KHz = _Frequency_100KHz;
    }
    if ( Bandwidth_100KHz ) {
        Assert_RADIO_EXTRA_Writable( Bandwidth_100KHz, ttc_assert_origin_auto );  // got invalid pointer!
        *Bandwidth_100KHz = _Bandwidth_100KHz;
    }
    if ( PreambleCode )     {
        Assert_RADIO_EXTRA_Writable( PreambleCode, ttc_assert_origin_auto );  // got invalid pointer!
        *PreambleCode     = _PreambleCode;
    }
}
void                                _radio_dw1000_init_dw1000( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    // Note: Content of *Registers has to be zeroed before to reset radio configuration!
    t_radio_dw1000_registers* Registers = &( Config->LowLevelConfig.Registers );
    t_radio_dw1000_changed*   Changed   = &( Config->LowLevelConfig.Changed );

    if ( 1 ) { // initialize DW1000 GPIO pins

        if ( !Config->LowLevelConfig.Registers.PMSC_CTRL0.Fields.GPCE ) { // enable clock to GPIO ports
            Config->LowLevelConfig.Registers.PMSC_CTRL0.Fields.GPCE = 1;
            Config->LowLevelConfig.Registers.PMSC_CTRL0.Fields.GPRN = 1;
            _radio_dw1000_register_write_PMSC_CTRL0( Config );
        }
        BOOL EnableLEDs = 0;

        // configure GPIOs according to DW1000 UserManual p.122
        if ( Config->Init.Flags.Enable_LED_RXOK ) {
            changeRegisterField( GPIO_MODE, MSGP0, 0b01 );

            // configure GPIO0 as output
            changeRegisterField( GPIO_DIR, GPIO_DM0, 1 ); // change configuration of GPIO
            changeRegisterField( GPIO_DIR, GPIO_DP0, 0 ); // configure GPIO as output
            EnableLEDs = 1;
        }
        if ( Config->Init.Flags.Enable_LED_RX ) {
            if ( 0 ) { // configure GPIO3 as RXLED
                changeRegisterField( GPIO_MODE, MSGP2, 0b01 );
            }
            else {
                changeRegisterField( GPIO_MODE, MSGP2, 0b00 );
            }
            changeRegisterField( GPIO_DIR,  GPIO_DM2, 1 ); // change configuration of GPIO
            changeRegisterField( GPIO_DIR,  GPIO_DP2, 0 ); // configure GPIO as output
            changeRegisterField( GPIO_DOUT, GPIO_OP2, 0 ); // switch off output
            changeRegisterField( GPIO_DOUT, GPIO_OM2, 1 ); // change this pin
            EnableLEDs = 1;
        }
        if ( Config->Init.Flags.Enable_LED_TX ) { // enable DW1000-GPIO as LED_TX

            if ( 0 ) { // configure GPIO3 as TXLED
                changeRegisterField( GPIO_MODE, MSGP3, 0b01 );
            }
            else {     // configure GPIO3 as GPIO
                changeRegisterField( GPIO_MODE, MSGP3, 0b00 );
            }

            // configure GPIO3 as output
            changeRegisterField( GPIO_DIR,  GPIO_DM3, 1 ); // change configuration of GPIO
            changeRegisterField( GPIO_DIR,  GPIO_DP3, 0 ); // configure GPIO as output
            changeRegisterField( GPIO_DOUT, GPIO_OP3, 0 ); // switch off output
            changeRegisterField( GPIO_DOUT, GPIO_OM3, 1 ); // change this pin
            EnableLEDs = 1;
        }
        if ( EnableLEDs ) { // enable LED blink feature
            changeRegisterField( PMSC_LEDC, BLINK_TIM, 10 );
            changeRegisterField( PMSC_LEDC, BLINKEN, 1 );
        }
        if ( Config->Init.Flags.Enable_Output_PA ) {
            changeRegisterField( GPIO_MODE, MSGP4, 0b01 ); // EXTPA
            changeRegisterField( GPIO_DIR, GPIO_DM4, 1 ); // change configuration of GPIO
            changeRegisterField( GPIO_DIR, GPIO_DP4, 0 ); // configure GPIO as output

            changeRegisterField( GPIO_MODE, MSGP5, 0b01 ); // EXTTXE
            changeRegisterField( GPIO_DIR, GPIO_DM5, 1 ); // change configuration of GPIO
            changeRegisterField( GPIO_DIR, GPIO_DP5, 0 ); // configure GPIO as output

            changeRegisterField( GPIO_MODE, MSGP6, 0b01 ); // EXTRXE
            changeRegisterField( GPIO_DIR, GPIO_DM6, 1 ); // change configuration of GPIO
            changeRegisterField( GPIO_DIR, GPIO_DP6, 0 ); // configure GPIO as output
        }

        //Initialise Interrupt Lines
        if ( ( Config->LowLevelConfig.Pin_IRQ ) && ( Config->Init.Flags.Enable_Output_IRQ ) ) { // configure IRQ pin as external interrupt input

            changeRegisterField( SYS_CFG, HIRQ_POL, 1 );  // configure DW1000 IRQ output line as active high
            changeRegisterField( GPIO_MODE, MSGP8, 0b00 ); // configure MSGP8 as IRQ output

            // configure MSGP8 as output
            changeRegisterField( GPIO_DIR, GPIO_DM8, 1 ); // change configuration of GPIO
            changeRegisterField( GPIO_DIR, GPIO_DP8, 0 ); // configure GPIO as output

        }
        else {
            changeRegisterField( GPIO_MODE, MSGP8, 0b01 ); // use IRQ output as GPIO (no IRQ output)
            changeRegisterField( GPIO_DIR, GPIO_DM8, 1 );
            changeRegisterField( GPIO_DIR, GPIO_DP8, 1 ); // configure pin as input
        }
    }
    if ( 1 ) { // configure Interrupts

        // Note: Every interrupt mask bit set here requires a handler inside radio_dw1000_isr_generic()!

        u_radio_dw1000_register_sys_mask SYS_MASK;
        SYS_MASK.Word = 0;
        SYS_MASK.Fields.MTXFRS    = 1; // Transmitter Frame Sent
        SYS_MASK.Fields.MRXDFR    = 1; // Receiver Data Frame Ready (frame can now be downloaded from RX buffer)
        SYS_MASK.Fields.MRXSFDTO  = 1; // Receiver Start of Frame Detection Timeout
        SYS_MASK.Fields.MRXPHE    = 1; // Receiver MAC Header Error
        SYS_MASK.Fields.MRXRFSL   = 1; // Receiver Reed Solomon Frame Sync Loss (could not decode frame data)
        SYS_MASK.Fields.MRXRFTO   = 1; // Receiver Frame Wait Timeout
        SYS_MASK.Fields.MRXOVRR   = 1; // Receiver Buffer Overrun
        SYS_MASK.Fields.MAFFREJ   = 1; // Receiver MAC Header Error
        SYS_MASK.Fields.MRXFCE    = 1; // Receiver Frame Checksum Error
        SYS_MASK.Fields.MLDEERR   = 1; // Receiver Leading Edge Detector Error
        SYS_MASK.Fields.MHPDWARN  = 1; // Half Period Delay Warning
        SYS_MASK.Fields.MTXBERR   = 1; // Transmitter Buffer Error
        SYS_MASK.Fields.MTXFRB    = 1; // Transmitter Frame Begins
        SYS_MASK.Fields.MRXPTO    = 1; // Receiver Preamble Detection Error (if enabled, it causes periodic empty interrupt events. If disabled, an RXPTO event can occur and DW1000 will stop generating further interrupts)
        if ( 1 ) { // extra bits (for testing)
            SYS_MASK.Fields.MCPLOCK   = 1; // PLL lock
            SYS_MASK.Fields.MCPLLLL   = 1; // System Clock PLL Lock Loosing
            SYS_MASK.Fields.MTXPRS    = 1; // Transmitter Preamble Sent
            SYS_MASK.Fields.MTXPHS    = 1; // Transmitter MAC Header Sent
            SYS_MASK.Fields.MRFPLLLL  = 1; // Receiver PLL Lock Loosing
            SYS_MASK.Fields.MSLP2INIT = 1; // Device transitioned from SLEEP to INIT state
        }

        // enable certain interrupts only if activity functions are set
        if ( Config->Init.function_preamble_detected_isr )
        { SYS_MASK.Fields.MRXPRD    = 1; } // Receiver Preamble Detected

        if ( 0 ) { // unused bits
            SYS_MASK.Fields.MAAT      = 1; // Automatic Acknowledge Trigger
            SYS_MASK.Fields.MRXFCG    = 1; // Receiver Frame Checksum Good
            SYS_MASK.Fields.MRXSFDD   = 1; // Receiver Start of Frame Delimiter Detected
            SYS_MASK.Fields.MESSYNCR  = 1; // external sync clock reset
            SYS_MASK.Fields.MLDEDONE  = 1; // Receiver Leading Edge Detection Done
            SYS_MASK.Fields.MRXPHD    = 1; // Receiver MAC Header Detected
            SYS_MASK.Fields.MGPIOIRQ  = 1; // Interrupt from GPIO-block
        }
        changeRegisterWord( SYS_MASK, SYS_MASK.Word );
    }
    if ( 1 ) { // load configuration from OTP memory.

        _radio_dw1000_register_write_changed( Config ); // send all changed registers to DW1000 before accessing OTP memory

        //set system clock to XTI to reliably use _dwt_otpread
        _radio_dw1000_configure_clocking( Config, rdcs_force_sysclk_xti );
        u_radio_dw1000_load_from_otp LoadFromOTP = Config->LowLevelConfig.LoadFromOTP;

        Config->LowLevelConfig.OTP_Data.PartID = _radio_dw1000_read_otp( Config, rdoa_PARTID );
        Config->LowLevelConfig.OTP_Data.LotID  = _radio_dw1000_read_otp( Config, rdoa_LOTID );

        if ( ! LoadFromOTP.Bits.IsValid ) { // no valid configuration: set all bits
            ttc_memory_set( & LoadFromOTP.Byte, 0xffffff, sizeof( LoadFromOTP ) ); // set all bits
        }
        t_u32 LDOTUNE = ( LoadFromOTP.Bits.LDO_TUNE ) ? _radio_dw1000_read_otp( Config, rdoa_LDOTUNE ) : 0;
        if ( LDOTUNE ) { // LDO tune values are stored in the OTP

            // reset special functions
            Registers->OTP_SF.Byte = 0;
            _radio_dw1000_register_write_OTP_SF( Config );

            // force loading LDO tuning data from OTP
            Registers->OTP_SF.Fields.LDO_KICK = 1;
            _radio_dw1000_register_write_OTP_SF( Config );
        }
        if ( LoadFromOTP.Bits.AntennaDelay ) {
            t_u32 Value = _radio_dw1000_read_otp( Config, rdoa_ANTDLY );
            Config->LowLevelConfig.OTP_Data.AntennaDelay_PRF16 = Value & 0xffff;
            Config->LowLevelConfig.OTP_Data.AntennaDelay_PRF64 = ( Value >> 16 ) & 0xffff;

            // load default values for DWM1000 if no values are stored (found them on DecaWave mailinglist post from mik@lamming.com)
            if ( !Config->LowLevelConfig.OTP_Data.AntennaDelay_PRF16 )
            { Config->LowLevelConfig.OTP_Data.AntennaDelay_PRF16 = 16640; }
            if ( !Config->LowLevelConfig.OTP_Data.AntennaDelay_PRF64 )
            { Config->LowLevelConfig.OTP_Data.AntennaDelay_PRF64 = 16640; }
        }
        if ( LoadFromOTP.Bits.XtalTrim ) {
            Config->LowLevelConfig.OTP_Data.XtalTrim = _radio_dw1000_read_otp( Config, rdoa_XTRIM ) & 0x1F;
        }
        if ( LoadFromOTP.Bits.TX_Config ) {
            t_u32* Writer = ( t_u32* ) Config->LowLevelConfig.OTP_Data.TransmitPower;
            for ( t_u8 Index = 0; Index < sizeof( Config->LowLevelConfig.OTP_Data.TransmitPower ) / 4; Index++ ) {
                *Writer++ = _radio_dw1000_read_otp( Config, rdoa_TXCFG + Index );
            }
        }
        else {
            ttc_memory_set( Config->LowLevelConfig.OTP_Data.TransmitPower, 0, sizeof( Config->LowLevelConfig.OTP_Data.TransmitPower ) );
        }
        if ( LoadFromOTP.Bits.LDE_Config ) { // loading Leading Edge Detection microcode from OTP
            Registers->OTP_CTRL.Word = 0;
            Registers->OTP_CTRL.Fields.LDELOAD = 1; // force loading LDE uCode from OTP
            _radio_dw1000_register_write_OTP_CTRL( Config );

            _radio_dw1000_udelay( 300 ); // LDE_LOAD takes 150us (-> DW1000 UserManual p.23)

            // You need to do some step before. Read DW1000 User Manual pag. 80
        }
        else { // no LDE uCode loaded: disable LDE algorithm
            _radio_dw1000_register_read_PMSC_CTRL1( Config );
            Registers->PMSC_CTRL1.Fields.LDERUNE = 0;
            _radio_dw1000_register_write_PMSC_CTRL1( Config );
        }

        // switch back to default clock source
        _radio_dw1000_configure_clocking( Config, rdcs_enable_all_seq );
    }
    if ( 1 ) { // configure Crystal Oscillator
        _radio_dw1000_register_read_FS_XTALT( Config );

        if ( Config->LowLevelConfig.OTP_Data.XtalTrim ) {
            changeRegisterField( FS_XTALT, XTALT, Config->LowLevelConfig.OTP_Data.XtalTrim ); // value from OTP
        }
        else {
            changeRegisterField( FS_XTALT, XTALT, 0x10 ); // medium value as default
        }
        Registers->FS_XTALT.Fields.reserved = 0b011; // must be kept at 0b011 for correct operation (-> DW1000 UserManual p.150)
    }
    if ( 1 ) { // configure Transmitter
        if ( Config->Init.LevelTX >= Config->Features->MaxTxLevel ) {
            changeRegisterField( SYS_CFG, DIS_STXP, 0 ); // increase output power at max tx-level
        }
        else {
            changeRegisterField( SYS_CFG, DIS_STXP, 1 ); // disable smart power
        }
        //?_radio_dw1000_register_write_TX_FCTRL( Config );

        // configure RX-TX turn around time for auto acknowledgements according to current datarate
        Assert_RADIO_EXTRA( Config->LowLevelConfig.DataRate < 3, ttc_assert_origin_auto );  // invalid bitrate value!
        const static t_u8 _radio_dw1000_ACK_TIM[3] = { 0, 2, 3 }; // ACK_TIM values for 110, 850, 6800 kBps (-> DW1000 UserManual p.102)
        changeRegisterField( ACK_RESP_T, ACK_TIM, _radio_dw1000_ACK_TIM[Config->LowLevelConfig.DataRate] );

        t_u32 TX_POWER = Config->Features->DefaultTxLevel;
        switch ( Config->PhysicalIndex ) { // load TX_POWER value
#ifdef TTC_RADIO1_DW1000_TX_POWER
            case 0: TX_POWER = TTC_RADIO1_DW1000_TX_POWER; break;
#endif
#ifdef TTC_RADIO2_DW1000_TX_POWER
            case 1: TX_POWER = TTC_RADIO2_DW1000_TX_POWER; break;
#endif
#ifdef TTC_RADIO3_DW1000_TX_POWER
            case 2: TX_POWER = TTC_RADIO3_DW1000_TX_POWER; break;
#endif
#ifdef TTC_RADIO4_DW1000_TX_POWER
            case 3: TX_POWER = TTC_RADIO4_DW1000_TX_POWER; break;
#endif
#ifdef TTC_RADIO5_DW1000_TX_POWER
            case 4: TX_POWER = TTC_RADIO5_DW1000_TX_POWER; break;
#endif
#ifdef TTC_RADIO6_DW1000_TX_POWER
            case 5: TX_POWER = TTC_RADIO6_DW1000_TX_POWER; break;
#endif
#ifdef TTC_RADIO7_DW1000_TX_POWER
            case 6: TX_POWER = TTC_RADIO7_DW1000_TX_POWER; break;
#endif
#ifdef TTC_RADIO8_DW1000_TX_POWER
            case 7: TX_POWER = TTC_RADIO8_DW1000_TX_POWER; break;
#endif
#ifdef TTC_RADIO9_DW1000_TX_POWER
            case 8: TX_POWER = TTC_RADIO9_DW1000_TX_POWER; break;
#endif
#ifdef TTC_RADIO10_DW1000_TX_POWER
            case 9: TX_POWER = TTC_RADIO10_DW1000_TX_POWER; break;
#endif
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // no TX_POWER defined for this physical index! (invalid index?)
        }
        changeRegister( TX_POWER, TX_POWER );
    }
    if ( 1 ) { // configure Receiver
        if ( 1 ) { // configure Receiver Automatic Gain Control (DW1000 UserManual p.115)
            changeRegisterField( AGC_CTRL1, DIS_AM, 1 ); // disabled by default to save power

            if ( Registers->CHAN_CTRL.Fields.RXPRF == rd_chan_ctrl_prf_16MHz ) {
                Registers->AGC_TUNE1 = 0x8870; // optimal setting for 16MHz PRF
            }
            else {
                changeRegister( AGC_TUNE1, 0x889b ); // optimal setting for 64MHz PRF
            }

            changeRegister( AGC_TUNE2, 0x2502a907 );
            changeRegister( AGC_TUNE3, 0x0035 );
        }
        if ( 1 ) { // tune receiver for current TX settings (assuming that other nodes use same bitrate)
            _radio_dw1000_tune_receiver( Config, Config->LowLevelConfig.DataRate );
        }
        if ( 1 ) { // configure frame filter to allow all type of frames
            u_ttc_radio_frame_filter Settings;
            Settings.All = 0;                              // will disable frame filtering (allows to receive all frame types)
            radio_dw1000_configure_frame_filter( Config, Settings, TRUE );
        }
        if ( 1 ) { // configure auto acknowledgement in DW1000
            radio_dw1000_configure_auto_acknowledge( Config, Config->Init.Flags.AutoAcknowledge, TRUE );
        }
    }
    if ( 1 ) { // configure basic stuff of Leading Edge Detection
        changeRegisterField( LDE_CFG1, NTM, 13 );
        changeRegisterField( LDE_CFG1, PMULT, 3 );

    }
    if ( 1 ) { // configure double/ single buffering
        Changed->SYS_CFG = 1;
        if ( Config->Init.Flags.EnableDoubleBuffering ) { // configure double buffered operation
            changeRegisterField( SYS_CFG, DIS_DRXB, 0 ); // enable double buffering

            TODO( "implement double buffering!" )
        }
        else {                                            // configure single buffered operation
            changeRegisterField( SYS_CFG, DIS_DRXB, 1 ); // disable double buffering
        }
    }

    _radio_dw1000_register_write_changed( Config ); // send all changed registers to DW1000
    _radio_dw1000_configure_clocking( Config, rdcs_enable_all_seq );
}
void                                _radio_dw1000_isr_read_received_packet( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    static t_ttc_packet* rdrrpi_Packet; // all local variables in isr-functions can be static (nobody can call us in parallel)
    rdrrpi_Packet = radio_common_packet_get_empty_isr( Config, 0, 0 );

    if ( !rdrrpi_Packet ) { // got no packet buffer: return immediately
        RADIO_COMMON_COUNT( Amount_DroppedRx );
        radio_common_breakpoint();  // cannot obtain buffer from memory pool to read received packet. Do you call ttc_radio_packet_received_tryget() regulary and pass received buffers to ttc_radio_packet_release() for reuse?
        return;
    }

    t_u8 Previous_RXBR = radio_dw1000_isr_CurrentRxFrameMeta->RX_FINFO.Fields.RXBR;

    rdrrpi_Packet->Meta.IndexPhysical = radio_dw1000_RxMetaIndex + 1;

#if RADIO_DW1000_AMOUNT_RX_META > 1 // calculate next meta index
    radio_dw1000_RxMetaIndex++;
    if ( radio_dw1000_RxMetaIndex >= RADIO_DW1000_AMOUNT_RX_META )
    { radio_dw1000_RxMetaIndex = 0; }
#endif

    radio_dw1000_isr_CurrentRxFrameMeta->RX_FINFO.Word = _radio_dw1000_register_read_32bit( Config, rdr_RX_FINFO );

    // read in frame meta-data
    if ( Config->Init.Flags.EnableRanging ) {  // ranging is enabled
        if ( radio_dw1000_isr_SysStatus->Fields.LDEDONE ) { // leading edge detection has determined first ray
            _radio_dw1000_register_read_40bit( Config, rdr_RX_STAMP,  rdrrpi_Packet->Meta.ReceiveTime.Bytes );
        }
        else {                                              // no leading edge found: reset time stamp
            rdrrpi_Packet->Meta.ReceiveTime.Fields.TIMESTAMP_LOW  = 0;
            rdrrpi_Packet->Meta.ReceiveTime.Fields.TIMESTAMP_HIGH = 0;
        }
        if ( radio_dw1000_isr_CurrentRxFrameMeta->RX_FINFO.Fields.RNG ) {
            rdrrpi_Packet->Meta.RangingMessage = 1;
            Config->LastReceiveTime.Words.Low  = rdrrpi_Packet->Meta.ReceiveTime.Words.Low;
            Config->LastReceiveTime.Words.High = rdrrpi_Packet->Meta.ReceiveTime.Words.High;
        }
    }
    if ( Config->Init.Flags.EnableRSSI ) {     // read frame quality data from DW1000
        _radio_dw1000_spi_register_read( Config, rdr_RX_FQUAL, 8, radio_dw1000_isr_CurrentRxFrameMeta->RX_FQUAL.Bytes );
        /* fields below are not used at the moment
        _radio_dw1000_register_read_32bit( Config, rdr_FIRST_PATH, &(radio_dw1000_isr_CurrentRxFrameMeta->FIRST_PATH) );
        _radio_dw1000_register_read_32bit( Config, rdr_RX_TTCKI,   &(radio_dw1000_isr_CurrentRxFrameMeta->RX_TTCKI) );
        _radio_dw1000_register_read_32bit( Config, rdr_RX_TTCKO,   &(radio_dw1000_isr_CurrentRxFrameMeta->RX_TTCKO) );
        _radio_dw1000_register_read_32bit( Config, rdr_FIRST_PATH, &(radio_dw1000_isr_CurrentRxFrameMeta->FIRST_PATH) );
        */
        radio_dw1000_isr_CurrentRxFrameMeta->Flags.Bits.Valid_RX_FQUAL = 1;
    }

    t_u16 FrameLength = radio_dw1000_isr_CurrentRxFrameMeta->RX_FINFO.Fields.RXFLEN;
    if ( FrameLength && ( FrameLength <= Config->Init.MaxPacketSize ) ) { // valid packet size: read received frame data into new packet
        _radio_dw1000_spi_register_read( Config,
                                         rdr_RX_BUFFER,
                                         FrameLength,
                                         ( t_u8* ) & ( rdrrpi_Packet->MAC.packet_802154_generic.FCF )
                                       );
        rdrrpi_Packet->MAC.packet_802154_generic.Length = FrameLength;

        if ( !_radio_dw1000_check_receiver_overrun( Config ) ) { // no overrun occured: process packet and pass it to ttc_radio
            _RADIO_DW1000_LED_RX( 0 );

            if ( Config->Init.function_end_rx_isr )
            { Config->Init.function_end_rx_isr( Config, rdrrpi_Packet ); } // call activity function

            if ( Previous_RXBR != radio_dw1000_isr_CurrentRxFrameMeta->RX_FINFO.Fields.RXBR ) {
                Config->LowLevelConfig.TuneReceiverBitRate = radio_dw1000_isr_CurrentRxFrameMeta->RX_FINFO.Fields.RXBR;
            }

            // pass received packet to high-level driver
            radio_common_push_list_rx_isr( Config, rdrrpi_Packet );

            return;
        }
    }

    // Error: could not read received packet
    RADIO_COMMON_COUNT( Amount_DroppedRx );
    _radio_dw1000_reset_receiver( Config );
    radio_common_packet_release( rdrrpi_Packet );
}
void                                _radio_dw1000_isr_transmit_next( t_ttc_radio_config* Config, u_ttc_packetimestamp_40* TransmitTime ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // given address des not point to writable memory!
    Assert_RADIO_EXTRA( radio_common_InsideISR, ttc_assert_origin_auto );  // this function must only be called from interrupt service routine!

    radio_dw1000_isr_TransmissionStarted = FALSE;
    t_ttc_list_item* Item = ttc_list_peek_front( &( Config->List_PacketsTx ) );
    if ( Item ) { // list has a packet to transmit: try to obtain lockl
        if ( ttc_mutex_lock_isr( & ( Config->FlagRunningTX ) ) == 0 ) { // successfully obtained lock: transmit packet

            t_ttc_packet* Packet = radio_common_pop_list_tx_isr( Config );
            Assert_RADIO_EXTRA_Writable( Packet, ttc_assert_origin_auto ); // we have successfully peeked for a packet but could not obtain it. Check implementation of radio_dw1000, radio_common and ttc_list!

            volatile BOOL DoNotReleaseBuffer = Packet->Meta.DoNotReleaseBuffer;
            if ( Config->Init.function_start_tx_isr )
            {  DoNotReleaseBuffer |= Config->Init.function_start_tx_isr( Config, Packet ); }

            // transfer packet into DW1000 transmit buffer
            _radio_dw1000_tx_prepare( Config, Packet );

            if ( DoNotReleaseBuffer ) // user wants to track packet transmission: initialize status variable
            { Packet->Meta.StatusTX = tpst_Info_TxStarted; }

            // start transmission
            _radio_dw1000_tx_start( Config, TransmitTime, &( Packet->Meta ) );

            if ( Config->Init.function_end_tx_isr ) { // activity function defined: call it
                DoNotReleaseBuffer |= Config->Init.function_end_tx_isr( Config, Packet );
            }

            if ( !DoNotReleaseBuffer ) // user allowed to release buffer
            { radio_common_packet_release( Packet ); } // return packet to its memory pool

            RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_transmit_started );
            radio_dw1000_isr_TransmissionStarted = TRUE;
        }
        else {
            RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_transmit_locked );
        }
    }
    else {        // no packets left to transmit:   clear transmission flag
        ttc_mutex_unlock_isr( & ( Config->FlagRunningTX ) );
        RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_transmit_empty );
    }
}
void                                _radio_dw1000_isr_raise( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_RADIO( Config->RadioInterrupt != NULL, ttc_assert_origin_auto ); // no interrupt source defined for this radio!
    Assert_RADIO_EXTRA( ttc_interrupt_critical_level() == 0, ttc_assert_origin_auto ); // this function must not be called with disabled interrupts. Call ttc_interrupt_critical_end_all() before!
    RADIO_DW1000_STATUS_ISR_UPDATE( rdsi_software_interrupt );

    ttc_interrupt_generate_gpio( Config->RadioInterrupt ); // generate software interrupt to run radio_dw1000_isr_generic()

    Assert_RADIO_EXTRA( Config->LowLevelConfig.StatusISR > rdsi_software_interrupt, ttc_assert_origin_auto ); // interrupt service routine did not update status. Check implementation!
}
u_radio_dw1000_register_sys_status* _radio_dw1000_read_sys_status( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    u_radio_dw1000_register_sys_status* SysStatus = &( Config->LowLevelConfig.Registers.SYS_STATUS );

    _radio_dw1000_register_read_SYS_STATUS( Config );
    _radio_dw1000_register_write_SYS_STATUS( Config ); // reset latched bits

    return SysStatus;
}
t_u32                               _radio_dw1000_read_otp( t_ttc_radio_config* Config, e_radio_dw1000_otp_address Address ) {

    _radio_dw1000_register_write_16bit( Config, rdr_OTP_ADDR, Address );

    // driving OTP_READ manually
    _radio_dw1000_register_write_8bit( Config, rdr_OTP_CTRL, 0x03 );

    // clear bit 0 (does not clear automatically)
    _radio_dw1000_register_write_8bit( Config, rdr_OTP_CTRL, 0x00 );

    return _radio_dw1000_register_read_32bit( Config, rdr_OTP_RDAT );
}
t_radio_dw1000_registers*           _radio_dw1000_register_read_all( t_ttc_radio_config* Config ) {

    if ( TTC_ASSERT_RADIO_EXTRA ) // fill local register storage with fill byte to see fields not being updated
    { ttc_memory_set( & Config->LowLevelConfig.Registers, 0xcc, sizeof( Config->LowLevelConfig.Registers ) ); }

    _radio_dw1000_register_read_ACK_RESP_T( Config );
    _radio_dw1000_register_read_AGC_CTRL1( Config );
    _radio_dw1000_register_read_AGC_TUNE1( Config );
    _radio_dw1000_register_read_AGC_TUNE2( Config );
    _radio_dw1000_register_read_AGC_TUNE3( Config );
    _radio_dw1000_register_read_AGC_STAT1( Config );
    _radio_dw1000_register_read_AON_CFG0( Config );
    _radio_dw1000_register_read_AON_CFG1( Config );
    _radio_dw1000_register_read_AON_CTRL( Config );
    _radio_dw1000_register_read_AON_WCFG( Config );
    _radio_dw1000_register_read_CHAN_CTRL( Config );
    _radio_dw1000_register_read_DEV_ID( Config );
    _radio_dw1000_register_read_DRX_PRETOC( Config );
    _radio_dw1000_register_read_DRX_SFDTOC( Config );
    _radio_dw1000_register_read_DRX_TUNE0b( Config );
    _radio_dw1000_register_read_DRX_TUNE1a( Config );
    _radio_dw1000_register_read_DRX_TUNE1b( Config );
    _radio_dw1000_register_read_DRX_TUNE2( Config );
    _radio_dw1000_register_read_DRX_TUNE4H( Config );
    _radio_dw1000_register_read_DX_TIME( Config );
    _radio_dw1000_register_read_EC_CTRL( Config );
    _radio_dw1000_register_read_FS_PLLCFG( Config );
    _radio_dw1000_register_read_FS_PLLTUNE( Config );
    _radio_dw1000_register_read_FS_XTALT( Config );
    _radio_dw1000_register_read_GPIO_DIR( Config );
    _radio_dw1000_register_read_GPIO_DOUT( Config );
    _radio_dw1000_register_read_GPIO_IBES( Config );
    _radio_dw1000_register_read_GPIO_ICLR( Config );
    _radio_dw1000_register_read_GPIO_IDBE( Config );
    _radio_dw1000_register_read_GPIO_IMODE( Config );
    _radio_dw1000_register_read_GPIO_IRQE( Config );
    _radio_dw1000_register_read_GPIO_ISEN( Config );
    _radio_dw1000_register_read_GPIO_MODE( Config );
    _radio_dw1000_register_read_GPIO_RAW( Config );
    _radio_dw1000_register_read_LDE_CFG1( Config );
    _radio_dw1000_register_read_LDE_CFG2( Config );
    _radio_dw1000_register_read_LDE_PPAMPL( Config );
    _radio_dw1000_register_read_LDE_PPINDX( Config );
    _radio_dw1000_register_read_LDE_REPC( Config );
    _radio_dw1000_register_read_LDE_RXANTD( Config );
    _radio_dw1000_register_read_LDE_THRESH( Config );
    _radio_dw1000_register_read_PMSC_CTRL0( Config );
    _radio_dw1000_register_read_PMSC_CTRL1( Config );
    _radio_dw1000_register_read_PMSC_LEDC( Config );
    _radio_dw1000_register_read_PMSC_SNOZT( Config );
    _radio_dw1000_register_read_PMSC_TXFSEQ( Config );
    _radio_dw1000_register_read_RF_CONF( Config );
    _radio_dw1000_register_read_RF_STATUS( Config );
    _radio_dw1000_register_read_RF_RXCTRLH( Config );
    _radio_dw1000_register_read_RF_TXCTRL( Config );
    _radio_dw1000_register_read_RX_FWTO( Config );
    _radio_dw1000_register_read_RX_SNIFF( Config );
    _radio_dw1000_register_read_SYS_CFG( Config );
    _radio_dw1000_register_read_SYS_CTRL( Config );
    _radio_dw1000_register_read_SYS_MASK( Config );
    _radio_dw1000_register_read_SYS_STATUS( Config );
    _radio_dw1000_register_read_SYS_TIME( Config );
    _radio_dw1000_register_read_TC_PGDELAY( Config );
    _radio_dw1000_register_read_TX_ANTD( Config );
    _radio_dw1000_register_read_TX_FCTRL( Config );
    _radio_dw1000_register_read_TX_POWER( Config );
    _radio_dw1000_register_read_TX_RAWST( Config );
    _radio_dw1000_register_read_TX_STAMP( Config );

    return & Config->LowLevelConfig.Registers;
}
t_u8                                _radio_dw1000_register_read_8bit( t_ttc_radio_config* Config, e_radio_dw1000_register Register ) {

    t_u8 Value;

    _radio_dw1000_spi_register_read( Config, Register, 1, ( t_u8* ) &Value ); // read 2 bytes (16-bits) register into buffer

    return Value;
}
t_u16                               _radio_dw1000_register_read_16bit( t_ttc_radio_config* Config, e_radio_dw1000_register Register ) {

    t_u16 Value;
    _radio_dw1000_spi_register_read( Config, Register, 2, ( t_u8* ) &Value ); // read 2 bytes (16-bits) register into buffer

#if TTC_BASIC_BIG_ENDIAN == 1
    Value = ttc_basic_16_read_little_endian( ( t_u8* ) &Value );
#endif

    return Value;
}
t_u32                               _radio_dw1000_register_read_32bit( t_ttc_radio_config* Config, e_radio_dw1000_register Register ) {

    t_u32 Value;

    // read 4 bytes (32-bits) from register into buffer
    _radio_dw1000_spi_register_read( Config,
                                     Register,
                                     4,
                                     ( t_u8* ) &Value
                                   );

#if TTC_BASIC_BIG_ENDIAN == 1
    Value = ttc_basic_32_read_little_endian( ( t_u8* ) &Value );
#endif

    return Value;
}
void                                _radio_dw1000_register_read_40bit( t_ttc_radio_config* Config, e_radio_dw1000_register Register, t_u8* Buffer ) {

    // read 5 bytes (40-bits) from register into buffer
    _radio_dw1000_spi_register_read( Config,
                                     Register,
                                     5,
                                     Buffer
                                   );

#if TTC_BASIC_BIG_ENDIAN == 1
    // endian conversion of lower 32 bits
    *( ( t_u32* ) Buffer ) = ttc_basic_32_read_little_endian( ( t_u8* ) Buffer );
    // upper 8 bits stay unchanged
#endif
}
void                                _radio_dw1000_register_write_8bit( t_ttc_radio_config* Config, e_radio_dw1000_register Register, t_u8 Value ) {

    _radio_dw1000_spi_register_write( Config, Register, 1, ( t_u8* ) &Value );
}
void                                _radio_dw1000_register_write_16bit( t_ttc_radio_config* Config, e_radio_dw1000_register Register, t_u16 Value ) {

#if TTC_BASIC_BIG_ENDIAN == 1
    Value = ttc_basic_16_read_little_endian( ( t_u8* ) &Value );
#endif
    _radio_dw1000_spi_register_write( Config, Register, 2, ( t_u8* ) &Value );
}
void                                _radio_dw1000_register_write_32bit( t_ttc_radio_config* Config, e_radio_dw1000_register Register, t_u32 Value ) {

#if TTC_BASIC_BIG_ENDIAN == 1
    Value = ttc_basic_32_read_little_endian( ( t_u8* ) &Value );
#endif
    _radio_dw1000_spi_register_write( Config, Register, 4, ( t_u8* ) &Value );
}
void                                _radio_dw1000_register_write_40bit( t_ttc_radio_config* Config, e_radio_dw1000_register Register, t_u8* Value ) {

#if TTC_BASIC_LITTLE_ENDIAN == 1
    // no conversion required
    _radio_dw1000_spi_register_write( Config, Register, 5, Value );
#else
    // convert endianess of lower 32 bits
    static t_u8 rdrw_Temp[5];
    *( ( t_u32* )  rdrw_Temp ) = ttc_basic_32_read_little_endian( Value );
    rdrw_Temp[4] = Value[4]; // upper 8 bits stay unchanged

    _radio_dw1000_spi_register_write( Config, Register, 5, ( t_u8* ) Temp );
#endif
}
void                                _radio_dw1000_register_write_changed( t_ttc_radio_config* Config ) {

    t_radio_dw1000_changed rdrwc_Changed;

    // copy content of struct into local variable an reset source atomically
    ttc_task_critical_begin();
    if ( sizeof( rdrwc_Changed ) == 8 ) { // can use fast 32-bit access
        *( ( t_u32* ) &rdrwc_Changed ) = *( ( t_u32* ) & ( Config->LowLevelConfig.Changed ) );
        *( ( ( t_u32* ) &rdrwc_Changed ) + 1 ) = *( ( ( t_u32* ) & ( Config->LowLevelConfig.Changed ) ) + 1 );
    }
    else                              // size of variable has changed: must use universal copy function
    { ttc_memory_copy( &rdrwc_Changed, &( Config->LowLevelConfig.Changed ), sizeof( rdrwc_Changed ) ); }

    ttc_memory_set( &( Config->LowLevelConfig.Changed ), 0, sizeof( rdrwc_Changed ) );
    ttc_task_critical_end();

    /* The RX Meta Data registers are only read during radio_dw1000_isr_generic() run and are stored in a special ringbuffer.
     * They have been removed from Config->LowLevelConfig.Registers to save RAM.

    if ( Changed.RX_FINFO )     { _radio_dw1000_register_write_RX_FINFO( Config ); }
    if ( Changed.RX_FQUAL )     { _radio_dw1000_register_write_RX_FQUAL( Config ); }
    if ( Changed.RX_TTCKI )     { _radio_dw1000_register_write_RX_TTCKI( Config ); }
    if ( Changed.RX_TTCKO )     { _radio_dw1000_register_write_RX_TTCKO( Config ); }
    if ( Changed.RX_STAMP )     { _radio_dw1000_register_write_RX_STAMP( Config ); }
    if ( Changed.FIRST_PATH )   { _radio_dw1000_register_write_FIRST_PATH( Config ); }
    */

    if ( rdrwc_Changed.ACK_RESP_T )   { _radio_dw1000_register_write_ACK_RESP_T( Config ); }
    if ( rdrwc_Changed.AGC_CTRL1 )    { _radio_dw1000_register_write_AGC_CTRL1( Config ); }
    if ( rdrwc_Changed.AGC_TUNE1 )    { _radio_dw1000_register_write_AGC_TUNE1( Config ); }
    if ( rdrwc_Changed.AGC_TUNE2 )    { _radio_dw1000_register_write_AGC_TUNE2( Config ); }
    if ( rdrwc_Changed.AGC_TUNE3 )    { _radio_dw1000_register_write_AGC_TUNE3( Config ); }
    if ( rdrwc_Changed.AON_CFG0 )     { _radio_dw1000_register_write_AON_CFG0( Config ); }
    if ( rdrwc_Changed.AON_CFG1 )     { _radio_dw1000_register_write_AON_CFG1( Config ); }
    if ( rdrwc_Changed.AON_CTRL )     { _radio_dw1000_register_write_AON_CTRL( Config ); }
    if ( rdrwc_Changed.AON_WCFG )     { _radio_dw1000_register_write_AON_WCFG( Config ); }
    if ( rdrwc_Changed.CHAN_CTRL )    { _radio_dw1000_register_write_CHAN_CTRL( Config ); }
    if ( rdrwc_Changed.DRX_PRETOC )   { _radio_dw1000_register_write_DRX_PRETOC( Config ); }
    if ( rdrwc_Changed.DRX_SFDTOC )   { _radio_dw1000_register_write_DRX_SFDTOC( Config ); }
    if ( rdrwc_Changed.DRX_TUNE0b )   { _radio_dw1000_register_write_DRX_TUNE0b( Config ); }
    if ( rdrwc_Changed.DRX_TUNE1a )   { _radio_dw1000_register_write_DRX_TUNE1a( Config ); }
    if ( rdrwc_Changed.DRX_TUNE1b )   { _radio_dw1000_register_write_DRX_TUNE1b( Config ); }
    if ( rdrwc_Changed.DRX_TUNE2 )    { _radio_dw1000_register_write_DRX_TUNE2( Config ); }
    if ( rdrwc_Changed.DRX_TUNE4H )   { _radio_dw1000_register_write_DRX_TUNE4H( Config ); }
    if ( rdrwc_Changed.DX_TIME )      { _radio_dw1000_register_write_DX_TIME( Config ); }
    if ( rdrwc_Changed.EC_CTRL )      { _radio_dw1000_register_write_EC_CTRL( Config ); }
    if ( rdrwc_Changed.FS_PLLCFG )    { _radio_dw1000_register_write_FS_PLLCFG( Config ); }
    if ( rdrwc_Changed.FS_PLLTUNE )   { _radio_dw1000_register_write_FS_PLLTUNE( Config ); }
    if ( rdrwc_Changed.FS_XTALT )     { _radio_dw1000_register_write_FS_XTALT( Config ); }
    if ( rdrwc_Changed.GPIO_DIR )     { _radio_dw1000_register_write_GPIO_DIR( Config ); }
    if ( rdrwc_Changed.GPIO_DOUT )    { _radio_dw1000_register_write_GPIO_DOUT( Config ); }
    if ( rdrwc_Changed.GPIO_IBES )    { _radio_dw1000_register_write_GPIO_IBES( Config ); }
    if ( rdrwc_Changed.GPIO_ICLR )    { _radio_dw1000_register_write_GPIO_ICLR( Config ); }
    if ( rdrwc_Changed.GPIO_IDBE )    { _radio_dw1000_register_write_GPIO_IDBE( Config ); }
    if ( rdrwc_Changed.GPIO_IMODE )   { _radio_dw1000_register_write_GPIO_IMODE( Config ); }
    if ( rdrwc_Changed.GPIO_IRQE )    { _radio_dw1000_register_write_GPIO_IRQE( Config ); }
    if ( rdrwc_Changed.GPIO_ISEN )    { _radio_dw1000_register_write_GPIO_ISEN( Config ); }
    if ( rdrwc_Changed.GPIO_MODE )    { _radio_dw1000_register_write_GPIO_MODE( Config ); }
    if ( rdrwc_Changed.GPIO_RAW )     { _radio_dw1000_register_write_GPIO_RAW( Config ); }
    if ( rdrwc_Changed.LDE_CFG1 )     { _radio_dw1000_register_write_LDE_CFG1( Config ); }
    if ( rdrwc_Changed.LDE_CFG2 )     { _radio_dw1000_register_write_LDE_CFG2( Config ); }
    if ( rdrwc_Changed.LDE_PPAMPL )   { _radio_dw1000_register_write_LDE_PPAMPL( Config ); }
    if ( rdrwc_Changed.LDE_PPINDX )   { _radio_dw1000_register_write_LDE_PPINDX( Config ); }
    if ( rdrwc_Changed.LDE_REPC )     { _radio_dw1000_register_write_LDE_REPC( Config ); }
    if ( rdrwc_Changed.LDE_RXANTD )   { _radio_dw1000_register_write_LDE_RXANTD( Config ); }
    if ( rdrwc_Changed.LDE_THRESH )   { _radio_dw1000_register_write_LDE_THRESH( Config ); }
    if ( rdrwc_Changed.PMSC_CTRL0 )   { _radio_dw1000_register_write_PMSC_CTRL0( Config ); }
    if ( rdrwc_Changed.PMSC_CTRL1 )   { _radio_dw1000_register_write_PMSC_CTRL1( Config ); }
    if ( rdrwc_Changed.PMSC_LEDC )    { _radio_dw1000_register_write_PMSC_LEDC( Config ); }
    if ( rdrwc_Changed.PMSC_SNOZT )   { _radio_dw1000_register_write_PMSC_SNOZT( Config ); }
    if ( rdrwc_Changed.PMSC_TXFSEQ )  { _radio_dw1000_register_write_PMSC_TXFSEQ( Config ); }
    if ( rdrwc_Changed.RF_CONF )      { _radio_dw1000_register_write_RF_CONF( Config ); }
    if ( rdrwc_Changed.RF_RXCTRLH )   { _radio_dw1000_register_write_RF_RXCTRLH( Config ); }
    if ( rdrwc_Changed.RF_TXCTRL )    { _radio_dw1000_register_write_RF_TXCTRL( Config ); }
    if ( rdrwc_Changed.RX_FWTO )      { _radio_dw1000_register_write_RX_FWTO( Config ); }
    if ( rdrwc_Changed.RX_SNIFF )     { _radio_dw1000_register_write_RX_SNIFF( Config ); }
    if ( rdrwc_Changed.SYS_CFG )      { _radio_dw1000_register_write_SYS_CFG( Config ); }
    if ( rdrwc_Changed.SYS_CTRL )     { _radio_dw1000_register_write_SYS_CTRL( Config ); }
    if ( rdrwc_Changed.SYS_MASK )     { _radio_dw1000_register_write_SYS_MASK( Config ); }
    if ( rdrwc_Changed.SYS_STATUS )   { _radio_dw1000_register_write_SYS_STATUS( Config ); }
    if ( rdrwc_Changed.SYS_TIME )     { _radio_dw1000_register_write_SYS_TIME( Config ); }
    if ( rdrwc_Changed.TC_PGDELAY )   { _radio_dw1000_register_write_TC_PGDELAY( Config ); }
    if ( rdrwc_Changed.TX_ANTD )      { _radio_dw1000_register_write_TX_ANTD( Config ); }
    if ( rdrwc_Changed.TX_FCTRL )     { _radio_dw1000_register_write_TX_FCTRL( Config ); }
    if ( rdrwc_Changed.TX_POWER )     { _radio_dw1000_register_write_TX_POWER( Config ); }
    if ( rdrwc_Changed.TX_RAWST )     { _radio_dw1000_register_write_TX_RAWST( Config ); }
    if ( rdrwc_Changed.TX_STAMP )     { _radio_dw1000_register_write_TX_STAMP( Config ); }

    ttc_task_check_stack(); // a good place to check for a stack overrun
}
void                                _radio_dw1000_reset_receiver( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    u_radio_dw1000_register_pmsc_ctrl0* PMSC_CTRL0 = &( Config->LowLevelConfig.Registers ).PMSC_CTRL0;

    // reset receiver only (-> DW1000 UserManual p.182)
    PMSC_CTRL0->Fields.SOFTRESET = 0b1110;
    _radio_dw1000_register_write_PMSC_CTRL0( Config );

    _radio_dw1000_udelay( 50 );

    // take back reset condition
    PMSC_CTRL0->Fields.SOFTRESET = 0b1111;
    _radio_dw1000_register_write_PMSC_CTRL0( Config );

    _radio_dw1000_udelay( 50 );

    if ( Config->Init.Flags.RxAutoReenable ) { // re-enable receiver
        radio_dw1000_receiver( Config, TRUE, 0, 0 );
    }
    else {                                     // receiver is off now
        ttc_mutex_unlock( &( Config->Flag_ReceiverOn ) );
    }
}
void                                _radio_dw1000_reset_hard( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );

    e_ttc_gpio_pin ResetPin = Config->LowLevelConfig.Pin_RESET;
    if ( ResetPin ) {
        ttc_gpio_clr( ResetPin );
        _radio_dw1000_udelay( 100 ); // DW1000 requires at least 10ns to react to reset
        ttc_gpio_set( ResetPin );
        _radio_dw1000_udelay( 100 ); // allow DW1000 to set reset line again
        Assert_RADIO( ttc_gpio_get( ResetPin ), ttc_assert_origin_auto ); // reset pin did not go back to high state. Check if reset pin is configured correctly and connected to DWM1000 reset pin (which should drive it high now)!
    }
    if ( ttc_spi_get_configuration( Config->LowLevelConfig.LogicalIndex_SPI )->Flags.Initialized )
    { _radio_dw1000_reset_soft( Config ); } // spi bus is available: issue soft reset of dw1000

    // switch of receiver
    radio_dw1000_receiver( Config, FALSE, 0, 0 );

    // Clear status flags
    ttc_mutex_unlock( & ( Config->FlagRunningTX ) );
}
void                                _radio_dw1000_reset_soft( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );
    t_radio_dw1000_registers* Registers = &( Config->LowLevelConfig.Registers );
    //X #warning ToDo: issue _radio_dw1000_reset_soft() call in case of receiver error according to DW1000 UserManual p.34!

    if ( 1 ) { // disable PMSC control of transmitter (TX sequencing) and switch to XTAL clock
        _radio_dw1000_configure_clocking( Config, rdcs_force_sysclk_xti ); //set system clock to XTI
        Registers->PMSC_CTRL1.Fields.PKTSEQ = rd_pmsc_disable;
        _radio_dw1000_register_write_PMSC_CTRL1( Config );
    }

    // clear any AON auto download bits (as reset will trigger AON download)
    Registers->AON_WCFG.Word = 0;
    _radio_dw1000_register_write_AON_WCFG( Config );

    // clear the wakeup configuration
    Registers->AON_CFG0.Word = 0;
    _radio_dw1000_register_write_AON_CFG0( Config );
    Registers->AON_CFG1.Word = 0;
    _radio_dw1000_register_write_AON_CFG1( Config );

    // upload the new configuration
    _radio_dw1000_aon_upload( Config );

    //reset Host Interface, TX, RX and PMSC according to DW1000 User Manual p.180
    _radio_dw1000_register_read_PMSC_CTRL0( Config );
    Registers->PMSC_CTRL0.Fields.SOFTRESET = 0b0000;
    _radio_dw1000_register_write_PMSC_CTRL0( Config );

    //DW1000 needs a 10us sleep to let clk PLL lock after reset - the PLL will automatically lock after the reset
    _radio_dw1000_udelay( 50 );

    // back to normal state
    Registers->PMSC_CTRL0.Fields.SOFTRESET = 0b1111;
    _radio_dw1000_register_write_PMSC_CTRL0( Config );

    _radio_dw1000_udelay( 50 );

    ttc_mutex_unlock( &( Config->Flag_ReceiverOn ) ); // receiver has been switched off
}
void                                _radio_dw1000_spi_register_read( t_ttc_radio_config* Config, e_radio_dw1000_register Register, t_u32 Length, t_u8* Buffer ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // must point to writable memory!
    Assert_RADIO_EXTRA_Writable( Buffer, ttc_assert_origin_auto );  // must point to writable memory!

    t_radio_dw1000_transaction TransactionHeader;
    TransactionHeader.Word = Register;

#if (TTC_ASSERT_RADIO_EXTRA == 1) // check correct configuration of operational bits

    Assert_RADIO_EXTRA( TransactionHeader.Fields.WriteOperation  == 0, ttc_assert_origin_auto );  // must not be set!
    if ( TransactionHeader.Fields.SubIndex ) {
        Assert_RADIO_EXTRA( TransactionHeader.Fields.SubIndexPresent  == 1, ttc_assert_origin_auto );

        if ( TransactionHeader.Fields.SubIndexHigh ) {
            Assert_RADIO_EXTRA( TransactionHeader.Fields.ExtendedPresent == 1, ttc_assert_origin_auto );
            Assert_RADIO_EXTRA( TransactionHeader.Fields.HeaderLength    == 3, ttc_assert_origin_auto );
        }
        else {
            Assert_RADIO_EXTRA( TransactionHeader.Fields.ExtendedPresent == 0, ttc_assert_origin_auto );
            Assert_RADIO_EXTRA( TransactionHeader.Fields.HeaderLength    == 2, ttc_assert_origin_auto );
        }
    }
    else {
        if ( TransactionHeader.Fields.SubIndexHigh == 0 ) {
            Assert_RADIO_EXTRA( TransactionHeader.Fields.ExtendedPresent == 0, ttc_assert_origin_auto );
            Assert_RADIO_EXTRA( TransactionHeader.Fields.HeaderLength    == 1, ttc_assert_origin_auto );
        }
    }
#endif

    if ( !radio_common_InsideISR )
    { ttc_interrupt_critical_begin(); }

    // use combined send+read function to send header and receive data
    ttc_spi_master_send_read( Config->LowLevelConfig.LogicalIndex_SPI,
                              Config->LowLevelConfig.LogicalIndex_SPI_NSS,
                              ( t_u8* ) & TransactionHeader.Word,
                              TransactionHeader.Fields.HeaderLength,
                              Length,        // sending one zero byte for each byte to receive
                              Buffer,
                              Length         // amount of bytes to read from register
                            );

    if ( !radio_common_InsideISR )
    { ttc_interrupt_critical_end(); }
}
void                                _radio_dw1000_spi_register_write( t_ttc_radio_config* Config, e_radio_dw1000_register Register, t_u32 Length, const t_u8* Buffer ) {

    t_radio_dw1000_transaction TransactionHeader;
    TransactionHeader.Word = Register;

#if (TTC_ASSERT_RADIO_EXTRA == 1) // check correct configuration of operational bits

    Assert_RADIO_EXTRA( TransactionHeader.Fields.WriteOperation  == 0, ttc_assert_origin_auto );  // must not be set!
    if ( TransactionHeader.Fields.SubIndex ) {
        Assert_RADIO_EXTRA( TransactionHeader.Fields.SubIndexPresent  == 1, ttc_assert_origin_auto );

        if ( TransactionHeader.Fields.SubIndexHigh ) {
            Assert_RADIO_EXTRA( TransactionHeader.Fields.ExtendedPresent == 1, ttc_assert_origin_auto );
            Assert_RADIO_EXTRA( TransactionHeader.Fields.HeaderLength    == 3, ttc_assert_origin_auto );
        }
        else {
            Assert_RADIO_EXTRA( TransactionHeader.Fields.ExtendedPresent == 0, ttc_assert_origin_auto );
            Assert_RADIO_EXTRA( TransactionHeader.Fields.HeaderLength    == 2, ttc_assert_origin_auto );
        }
    }
    else {
        Assert_RADIO_EXTRA( TransactionHeader.Fields.ExtendedPresent == 0, ttc_assert_origin_auto );
        Assert_RADIO_EXTRA( TransactionHeader.Fields.HeaderLength    == 1, ttc_assert_origin_auto );
    }
#endif
    TransactionHeader.Fields.WriteOperation = 1;

    t_u8 LogicalIndex_SPI = Config->LowLevelConfig.LogicalIndex_SPI;

    if ( !radio_common_InsideISR )
    { ttc_interrupt_critical_begin(); }

    // select slave manually (ensures NSS is active during several ttc_spi_*() calls
    ttc_gpio_clr( Config->LowLevelConfig.Pin_SPI_NSS );

    // sending data to SPI-bus without controlling slave select of DW1000 (IndexSlave=0)
    ttc_spi_master_send( LogicalIndex_SPI,
                         0,
                         ( t_u8* ) & TransactionHeader.Word,
                         TransactionHeader.Fields.HeaderLength
                       );
    ttc_spi_master_send( LogicalIndex_SPI,
                         0,
                         Buffer,
                         Length
                       );

    // deselect slave
    ttc_gpio_set( Config->LowLevelConfig.Pin_SPI_NSS );

    if ( !radio_common_InsideISR )
    { ttc_interrupt_critical_end(); }
}
void                                _radio_dw1000_synchronize_receive_buffer( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );

    u_radio_dw1000_register_sys_status* SysStatus = _radio_dw1000_read_sys_status( Config );
    if ( SysStatus->Fields.ICRB != SysStatus->Fields.HSRB ) {

        // swapping receiver buffer pointer so that host- and IC-side pointer match again (write 1 to toggle)
        _radio_dw1000_register_read_SYS_CTRL( Config );
        Config->LowLevelConfig.Registers.SYS_CTRL.Fields.HRBPT = 1;
        _radio_dw1000_register_write_SYS_CTRL( Config );
    }
    /* DEPRECATED
    t_u8  buff;
    //need to make sure that the host/IC buffer pointers are aligned before starting RX
    _radio_dw1000_spi_register_read( Config, SYS_STATUS_ID, 3, sizeof( buff ), &buff );

    if ( ( buff & ( SYS_STATUS_ICRBP >> 24 ) ) !=        // IC side Receive Buffer Pointer
            ( ( buff & ( SYS_STATUS_HSRBP >> 24 ) ) << 1 ) ) { // Host Side Receive Buffer Pointer
        t_u8 hsrb = 0x01;
        // we need to swap rx buffer status reg (write one to toggle internally)
        _radio_dw1000_spi_register_write( Config, SYS_CTRL_ID, SYS_CTRL_HRBT_OFFSET, 1, &hsrb );
    }
    */
}
void                                _radio_dw1000_tune_receiver( t_ttc_radio_config* Config, volatile e_radio_dw1000_datarates Rate ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );  // given address des not point to writable memory!
    t_radio_dw1000_registers* Registers = &( Config->LowLevelConfig.Registers );

    // configure Digital Receiver (DW1000 UserManual p.135)
    BOOL StandardSFD = 1 - Registers->CHAN_CTRL.Fields.RNSSFD;

    switch ( Rate ) { // optimize digital receiver for detected bitrate
        case radio_dw1000_datarate_110k: {
            Registers->DRX_TUNE0b = ( StandardSFD ) ? 0x000a : 0x0016;
            Registers->DRX_TUNE1b = 0x0064;
            break;
        }
        case radio_dw1000_datarate_850k: {
            Registers->DRX_TUNE0b = ( StandardSFD ) ? 0x0001 : 0x0006;
            Registers->DRX_TUNE1b = 0x0020;
            break;
        }
        case radio_dw1000_datarate_6M8: {
            Registers->DRX_TUNE0b = ( StandardSFD ) ? 0x0001 : 0x0002;
            Registers->DRX_TUNE1b = 0x0010;
            break;
        }
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown bitrate in received frame!
    }

    t_u8 PAC_Size  = 0;
    t_u8 PAC_Index = 0; // array index for radio_dw1000_DRX_TUNE2_16MHz[], radio_dw1000_DRX_TUNE2_64MHz[]

    switch ( Registers->TX_FCTRL.Fields.TXPREAMBLE ) { // select PAC-size for preamble length (->DW1000 UserManual p.32)

        case radio_dw1000_pl_64:
        case radio_dw1000_pl_128:
        case radio_dw1000_pl_256: // DW1000 UserManual says: for PreambleLength=256 => PAC_Size=16, but forum reported to better use PAC_Size=8 (https://groups.google.com/d/msgid/decawave_group/8ac7cd9d-8e88-4d2a-8580-72d341ff09ca%40googlegroups.com?utm_medium=email&utm_source=footer)
            PAC_Size  = 8;
            PAC_Index = 0;
            break;
        case radio_dw1000_pl_512:
            PAC_Size  = 16;
            PAC_Index = 1;
            break;
        case radio_dw1000_pl_1024:
            PAC_Size  = 32;
            PAC_Index = 2;
            break;
        default:
            PAC_Size  = 64;
            PAC_Index = 3;
            break;
    }
    const static t_u32 radio_dw1000_DRX_TUNE2_16MHz[4] = { 0x311a002d, 0x331a0052, 0x351a009a, 0x371a011d };
    const static t_u32 radio_dw1000_DRX_TUNE2_64MHz[4] = { 0x313b006b, 0x333b00be, 0x353b015e, 0x373b0296 };

    if ( Registers->CHAN_CTRL.Fields.RXPRF == rd_chan_ctrl_prf_16MHz ) {
        Registers->DRX_TUNE1a = 0x0087;
        Registers->DRX_TUNE2 = radio_dw1000_DRX_TUNE2_16MHz[PAC_Index];
    }
    else { // 64MHz PRF
        Registers->DRX_TUNE1a = 0x008d;
        Registers->DRX_TUNE2 = radio_dw1000_DRX_TUNE2_64MHz[PAC_Index];
    }

    // adapt Start Frame Delimiter Detection TimeOut to preamble size
    Registers->DRX_TUNE4H = 0x0028; // value for Preamble Length >= 128 (-> DW1000 UserManual p. 139)

    switch ( Registers->TX_FCTRL.Fields.TXPREAMBLE ) { // -> DW1000 UserManual p. 138
        case radio_dw1000_pl_64:
            Registers->DRX_SFDTOC = 64 + 64 + 1;   // length of preamble + SFD sequence
            Registers->DRX_PRETOC = 64 / PAC_Size; // preamble detection timeout count -> DW1000 UserManual p.138
            Registers->DRX_TUNE4H = 0x0010;
            break;
        case radio_dw1000_pl_128:
            Registers->DRX_SFDTOC = 128 + 64 + 1;  // length of preamble + SFD sequence
            Registers->DRX_PRETOC = 128 / PAC_Size; // preamble detection timeout count -> DW1000 UserManual p.138
            break;
        case radio_dw1000_pl_256:
            Registers->DRX_SFDTOC = 256 + 64 + 1;  // length of preamble + SFD sequence
            Registers->DRX_PRETOC = 256 / PAC_Size; // preamble detection timeout count -> DW1000 UserManual p.138
            break;
        case radio_dw1000_pl_512:
            Registers->DRX_SFDTOC = 512 + 64 + 1;  // length of preamble + SFD sequence
            Registers->DRX_PRETOC = 512 / PAC_Size; // preamble detection timeout count -> DW1000 UserManual p.138
            break;
        case radio_dw1000_pl_1024:
            Registers->DRX_SFDTOC = 1024 + 64 + 1; // length of preamble + SFD sequence
            Registers->DRX_PRETOC = 1024 / PAC_Size; // preamble detection timeout count -> DW1000 UserManual p.138
            break;
        case radio_dw1000_pl_2048:
            Registers->DRX_SFDTOC = 2048 + 64 + 1; // length of preamble + SFD sequence
            Registers->DRX_PRETOC = 2048 / PAC_Size; // preamble detection timeout count -> DW1000 UserManual p.138
            break;
        case radio_dw1000_pl_4096:
            Registers->DRX_SFDTOC = 4096 + 64 + 1; // length of preamble + SFD sequence
            Registers->DRX_PRETOC = 4096 / PAC_Size; // preamble detection timeout count -> DW1000 UserManual p.138
            break;
        default:
            ttc_assert_halt_origin( ec_radio_InvalidImplementation );
            break;
    }

    _radio_dw1000_register_write_DRX_TUNE0b( Config );
    _radio_dw1000_register_write_DRX_TUNE1a( Config );
    _radio_dw1000_register_write_DRX_TUNE1b( Config );
    _radio_dw1000_register_write_DRX_TUNE2( Config );
    _radio_dw1000_register_write_DRX_TUNE4H( Config );
    _radio_dw1000_register_write_DRX_SFDTOC( Config );
    _radio_dw1000_register_write_DRX_PRETOC( Config );
}
void                                _radio_dw1000_tx_prepare( t_ttc_radio_config* Config, t_ttc_packet* Packet ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );

    // Send frame data to transmit buffer
    _radio_dw1000_spi_register_write( Config,
                                      rdr_TX_BUFFER,
                                      Packet->MAC.packet_802154_generic.Length,
                                      ( t_u8* ) & ( Packet->MAC.packet_802154_generic.FCF ) // skipping Length field as this is encoded in PHY Header (-> DW1000 UserManual p.204)
                                    );

    // configure transmission
    u_radio_dw1000_t_registerx_fctrl* TX_FCTRL = & ( Config->LowLevelConfig.Registers.TX_FCTRL );
    Assert_RADIO_EXTRA( TX_FCTRL->Fields.TXBR == Config->LowLevelConfig.DataRate, ttc_assert_origin_auto );  // someone has changed this setting since init()!
    TX_FCTRL->Fields.TFLEN   = Packet->MAC.Length;
    TX_FCTRL->Fields.TXBOFFS = 0;
    TODO( "implement double buffered TX" )

    // reset meta flags
    radio_dw1000_isr_CurrentRxFrameMeta->Flags.Byte = 0;

    if ( Packet->Meta.RangingMessage ) { // packet should be send as ranging request
        TX_FCTRL->Fields.TR = 1; // radio will set RNG bit in PHY Header
        Config->LowLevelConfig.Registers.SYS_CTRL.Fields.WAIT4RESP = 1;
    }
    else {
        TX_FCTRL->Fields.TR = 0;
        Config->LowLevelConfig.Registers.SYS_CTRL.Fields.WAIT4RESP = 0;
    }

    _radio_dw1000_register_write_TX_FCTRL( Config );
}
void                                _radio_dw1000_tx_start( t_ttc_radio_config* Config, u_ttc_packetimestamp_40* TransmitTime, t_ttc_packet_meta* PacketMeta ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );
    Assert_RADIO_EXTRA_Writable( PacketMeta, ttc_assert_origin_auto );  // must point to RAM. Check implementation of caller!
    t_radio_dw1000_registers* Registers = &( Config->LowLevelConfig.Registers );

    if ( ttc_mutex_is_locked( &( Config->Flag_ReceiverOn ) ) ) { // receiver active: switch it off
        radio_dw1000_receiver( Config, 0, NULL, 0 );
    }

    // assemble transmit configuration + send to SYS_CTRL register
    Registers->SYS_CTRL.Word = 0;
    Registers->SYS_CTRL.Fields.TXSTRT = 1;

    if ( PacketMeta->ReceiveAfterTransmit ) { // switch on receiver after transmit
        Registers->SYS_CTRL.Fields.WAIT4RESP = 1;
        TODO( "Implement delayed receive via W4R_TIM to reduce energy consumption" )
    }
    if ( TransmitTime ) {                     // delay transmit
        Registers->SYS_CTRL.Fields.TXDLYS = 1;
        _radio_dw1000_register_write_40bit( Config, rdr_DX_TIME, ( t_u8* ) TransmitTime );
    }

    if ( PacketMeta->DoNotReleaseBuffer ) { // caller wants to track TX updates: set where interrupt service routine shall report transmission status
        Config->PacketTX_Meta = PacketMeta;
    }
    else {                                  // caller is not interested in TX updates: clear meta pointer
        Config->PacketTX_Meta = NULL;
    }

    _radio_dw1000_register_write_SYS_CTRL( Config ); // will immediately generate interrupt and run radio_dw1000_isr_generic() if interrupt from radio is enabled
    if ( !radio_common_InsideISR ) {                 // called from high-level driver
        if ( !Config->Init.Flags.Enable_Output_IRQ )  { // no interrupts available: actively poll DW1000 registers for status updates

            t_u32 TimeOut = -1;

            while ( Registers->SYS_CTRL.Fields.TXSTRT ) { // actively wait until transmission starts
                _radio_dw1000_register_read_SYS_CTRL( Config );
                _radio_dw1000_register_read_SYS_STATUS( Config );

                if ( Registers->SYS_STATUS.Fields.TXBERR ||
                        Registers->SYS_STATUS.Fields.TXPUTE ||
                        Registers->SYS_STATUS.Fields.HPDWARN
                   ) {
                    _radio_dw1000_register_write_SYS_STATUS( Config ); // reset latched flags
                    PacketMeta->StatusTX = tpst_Error_DelayedTxTooLate;
                }
                if ( TimeOut-- == 0 ) {
                    PacketMeta->StatusTX = tpst_Error_TxNotStarted;
                    return;
                }
            }

            // wait until frame has been transmitted
            while ( ! Registers->SYS_STATUS.Fields.TXFRB ) {
                _radio_dw1000_register_read_SYS_STATUS( Config );
            }
            while ( ! Registers->SYS_STATUS.Fields.TXFRS ) {
                _radio_dw1000_register_read_SYS_STATUS( Config );
            }
            _radio_dw1000_register_write_SYS_STATUS( Config ); // reset latched flags

            PacketMeta->StatusTX = tpst_OK_TxComplete;
            _RADIO_DW1000_LED_TX( 0 );
        }
        else {                                          // interrupts available: check if isr has been called
            if ( Config->PacketTX_Meta && ( Config->PacketTX_Meta->StatusTX == 0 ) ) { // no status update yet: activate timeout
                t_ttc_systick_delay TimeOut;
                ttc_systick_delay_init( &TimeOut, 1000 );
                while ( Config->PacketTX_Meta->StatusTX == 0 ) {
                    if ( ttc_systick_delay_expired( &TimeOut ) ) {
                        _radio_dw1000_register_read_all( Config );
                        Assert_RADIO_EXTRA( 0, ttc_assert_origin_auto ); //DEBUG
                        break;
                    }
                }
            }
        }
    }
    else {                                           // inside isr: check if radio raises interrupt line within timeout
        if ( TTC_ASSERT_RADIO_EXTRA ) { // this check is an extra feature
            if ( ! ttc_gpio_get( radio_dw1000_isr_LowLevelConfig->Pin_IRQ ) ) {
                t_ttc_systick_delay TimeOut;
                volatile t_u32 Counter = 0;
                volatile BOOL Overflow = 0; ( void ) Overflow;
                ttc_systick_delay_init_isr( &TimeOut, 100000 );
                while ( ! ttc_gpio_get( radio_dw1000_isr_LowLevelConfig->Pin_IRQ ) ) {
                    if ( ttc_systick_delay_expired_isr( &TimeOut ) ) { // transmission was not started within timeout: increase ranging delay
                        _radio_dw1000_tx_abort( Config );
#if (TTC_RADIO_STATISTICS == 1)
                        Config->Statistics.Amount_FailedTx++;
#endif
                        if ( Config->PacketTX_Meta ) { // inform caller of too late transmit

                            if ( Config->PacketTX_Meta->TransmitTime.Words.Low ) // delayed transmission did not start
                            { Config->PacketTX_Meta->StatusTX = tpst_Error_DelayedTxTooLate; }
                            else                                               // immediate transmission did not start
                            { Config->PacketTX_Meta->StatusTX = tpst_Error_TxNotStarted; }
                        }
                        break;
                    }
                    if ( ++Counter == 0 )
                    { Overflow = TRUE; }
                }
            }
        }
    }
}
void                                _radio_dw1000_tx_abort( t_ttc_radio_config* Config ) {
    Assert_RADIO_EXTRA_Writable( Config, ttc_assert_origin_auto );

    Config->LowLevelConfig.Registers.SYS_CTRL.Word          = 0;
    Config->LowLevelConfig.Registers.SYS_CTRL.Fields.TRXOFF = 1;
    _radio_dw1000_register_write_SYS_CTRL( Config );
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
