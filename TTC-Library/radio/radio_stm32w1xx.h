#ifndef RADIO_STM32W1XX_H
#define RADIO_STM32W1XX_H

/** { radio_stm32w1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for radio devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by high-level radio and application.
 *
 *  Created from template device_architecture.h revision 22 at 20140514 04:32:30 UTC
 *
 *  Note: See ttc_radio.h for description of stm32w1xx independent RADIO implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_RADIO_STM32W1XX
//
// Implementation status of low-level driver support for radio devices on stm32w1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_RADIO_STM32W1XX 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_RADIO_STM32W1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_RADIO_STM32W1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_RADIO_STM32W1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "radio_stm32w1xx_types.h"
#include "../ttc_radio_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_radio_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_radio_foo
//
#define ttc_driver_radio_deinit(Config) radio_stm32w1xx_deinit(Config)
#define ttc_driver_radio_configuration_check(Config) radio_stm32w1xx_configuration_check(Config)
#define ttc_driver_radio_init(Config) radio_stm32w1xx_init(Config)
#define ttc_driver_radio_load_defaults(Config) radio_stm32w1xx_load_defaults(Config)
#define ttc_driver_radio_prepare() radio_stm32w1xx_prepare()
#define ttc_driver_radio_reset(Config) radio_stm32w1xx_reset(Config)
#define ttc_driver_radio_transmit(Config, Packet) radio_stm32w1xx_transmit(Config, Packet)
#define ttc_driver_radio_configure_channel_rx(Config, NewChannel) radio_stm32w1xx_configure_channel(Config, NewChannel)
#define ttc_driver_radio_configure_channel_tx(Config, NewChannel) radio_stm32w1xx_configure_channel(Config, NewChannel)
#define ttc_driver_radio_maintenance(Config) radio_stm32w1xx_maintenance(Config)
#define ttc_driver_radio_isr_receive(Index, Config) radio_stm32w1xx_isr_receive(Index, Config)
#define ttc_driver_radio_change_local_address(Config, LocalAddress, LocalPanID) radio_stm32w1xx_change_local_address(Config, LocalAddress, LocalPanID)
#define ttc_driver_radio_configure_frame_filter(Config, Settings) radio_stm32w1xx_configure_frame_filter(Config, Settings)
#define ttc_driver_radio_configure_channel_rxtx(Config, NewChannel) radio_stm32w1xx_configure_channel_rxtx(Config, NewChannel)
#define ttc_driver_radio_change_delay_rx2tx(Config, Delay_uSeconds) radio_stm32w1xx_change_delay_rx2tx(Config, Delay_uSeconds)
#define ttc_driver_radio_change_delay_tx2rx(Config, Delay_uSeconds) radio_stm32w1xx_change_delay_tx2rx(Config, Delay_uSeconds)
#define ttc_driver_radio_update_meta(Config, Packet) radio_stm32w1xx_update_meta(Config, Packet)
#define ttc_driver_radio_change_delay_time(Config, Delay_uSeconds) radio_stm32w1xx_change_delay_time(Config, Delay_uSeconds)
#define ttc_driver_radio_isr_transmit(Index, Config) radio_stm32w1xx_isr_transmit(Index, Config)
#define ttc_driver_radio_receiver(Config, Enable, ReferenceTime) radio_stm32w1xx_receiver(Config, Enable, ReferenceTime)
#define ttc_driver_radio_time_add_ns(Time, Delay_ns, TimeDelayed) radio_stm32w1xx_time_add_ns(Time, Delay_ns, TimeDelayed)
#define ttc_driver_radio_configure_auto_acknowledge radio_stm32w1xx_configure_auto_acknowledge
#define ttc_driver_radio_gpio_out radio_stm32w1xx_gpio_out
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_radio.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_radio.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** shutdown single RADIO unit device
 * @param Config        pointer to struct t_ttc_radio_config (must have valid value for PhysicalIndex)
 * @return              == 0: RADIO has been shutdown successfully; != 0: error-code
 */
e_ttc_radio_errorcode radio_stm32w1xx_deinit( t_ttc_radio_config* Config );


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_radio_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_radio_features.
 *
 * @param Config = pointer to struct t_ttc_radio_config
 */
void radio_stm32w1xx_configuration_check( t_ttc_radio_config* Config );


/** initializes single RADIO unit for operation
 * @param Config        pointer to struct t_ttc_radio_config (must have valid value for PhysicalIndex)
 * @return              == 0: RADIO has been initialized successfully; != 0: error-code
 */
e_ttc_radio_errorcode radio_stm32w1xx_init( t_ttc_radio_config* Config );


/** loads configuration of indexed RADIO unit with default values
 * @param Config        pointer to struct t_ttc_radio_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_radio_errorcode radio_stm32w1xx_load_defaults( t_ttc_radio_config* Config );


/** Prepares radio Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void radio_stm32w1xx_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_radio_config (must have valid value for PhysicalIndex)
 * @return   =
 */
e_ttc_radio_errorcode radio_stm32w1xx_reset( t_ttc_radio_config* Config );


/** transmits given amount of binary data from given buffer
 *
 * The operation mode of this function depends on the config setting DelayedTransmits.
 * DelayedTransmits==TRUE:  given data is appended to output queue for transmission and function returns immediately
 * DelayedTransmits==FALSE: given data is sent immediately and function returns after successfull transmission
 *
 * @param Config        = pointer to struct t_ttc_radio_config (must have valid value for PhysicalIndex)
 * @param Data            buffer storing binary data to transmit
 * @param Amount          amount of bytes to transmit from Data[]
 * @return                == 0: data has been queued for transmission/ has been sent successfully; != 0: error-code
 */
e_ttc_radio_errorcode radio_stm32w1xx_transmit( t_ttc_radio_config* Config, ttc_radio_packet_t* Packet );


/** Switches channel used to receive/ transmit data.
 *
 * Note: This will also set the transmit channel.
 *
 * @param Config        = pointer to struct t_ttc_radio_config (must have valid value for PhysicalIndex)
 * @param NewChannel      Config->Features->MinChannel .. Config->Features->MaxChannel
 * @return                == 0: data has been queued for transmission/ has been sent successfully; != 0: error-code
 */
e_ttc_radio_errorcode radio_stm32w1xx_configure_channel( t_ttc_radio_config* Config, t_u8 NewChannel );


/** Issues regular maintenance on indexed radio.
 *
 * Call this function regularly to compensate temperature changes
 *
 * @param Config        pointer to struct t_ttc_radio_config (must have valid value for PhysicalIndex)
 */
void radio_stm32w1xx_maintenance( t_ttc_radio_config* Config );


/** Switches channel used to receive data.
 *
 * Note: On most architectures, this will also set the transmit channel.
 *       It is safe to call ttc_radio_configure_channel_tx() and ttc_radio_configure_channel_rx() for same channel.
 *
 * @param Config        = pointer to struct t_ttc_radio_config (must have valid value for PhysicalIndex)
 * @param NewChannel      Config->Features->MinChannel .. Config->Features->MaxChannel
 * @return                == 0: data has been queued for transmission/ has been sent successfully; != 0: error-code
 */
e_ttc_radio_errorcode radio_stm32w1xx_configure_channel_rx( t_ttc_radio_config* Config, t_u8 NewChannel );


/** Switches channel used to transmit data.
 *
 * Note: On most architectures, this will also set the receive channel.
 *       It is safe to call ttc_radio_configure_channel_tx() and ttc_radio_configure_channel_rx() for same channel.
 *
 * @param Config        = pointer to struct t_ttc_radio_config (must have valid value for PhysicalIndex)
 * @param NewChannel      Config->Features->MinChannel .. Config->Features->MaxChannel
 * @return                == 0: data has been queued for transmission/ has been sent successfully; != 0: error-code
 */
e_ttc_radio_errorcode radio_stm32w1xx_configure_channel_tx( t_ttc_radio_config* Config, t_u8 NewChannel );



/** low-level interrupt service routine for incoming packets
 *
 * This function must be registered as interrupt service routine for incoming packets.
 * Example for interrupts via GPIO pin A7:
 *    t_ttc_interrupt_handle RadioInterrupt = ttc_interrupt_init(tit_GPIO_Rising, E_ttc_gpio_pin_a7, _driver_radio_isr_receive, Config, 0, 0);
 *    Assert(RadioInterrupt, ttc_assert_origin_auto); // could not initialize interrupt (configured GPIO pin not usable for external interrupts on current uC architecture?)
 *    ttc_interrupt_enable_gpio(RadioInterrupt);
 *
 * This function has to be implemented by low-level radio driver:
 * 1) get an empty packet buffer via ttc_radio_packet_get_empty_isr()
 * 2) copy received raw data bytes from transceiver into empty packet
 * 3) call ttc_radio_isr_receive() to deliver filled packet to application
 *    The delivered packet will automatically been released to its memory pool after being processed.
 *
 * The low-level radio driver may declare these functions as extern to be able to call
 * @param Index   index of triggered interrupt (for gpio interrupts, this can be casted to e_ttc_gpio_pin)
 * @param Config  radio configuration as returned by ttc_radio_get_configuration()
 * @param Packet  network packet being delivers by ttc_radio_packet_get_empty_isr() and filled by low-level radio driver
 */
void radio_stm32w1xx_isr_receive( t_physical_index Index, void* Config );


/** Sets new protocol source address for outgoing and incoming packets
 *
 * After changing the local address, all further packets given to ttc_radio_transmit()
 * are will use it as source address. Also the address filter in the radio device will be updated to
 * accept packets for the new address.
 *
 * @param LogicalIndex   index of device to init (1..ttc_RADIO_get_max_LogicalIndex())
 * @param Config
 * @param SourceAddress  pointer to struct containing logical 16-bit and physical 64-bit address
 * @param SourcePanID    !=NULL: change LocalPanID too; ==NULL: LocalPanID is not changed
 * @param LocalAddress   =
 */
void radio_stm32w1xx_change_local_address( t_ttc_radio_config* Config, t_ttc_packet_address* LocalAddress, t_u16* LocalPanID );


/** Configure frame filter in connected radio
 *
 * Most radio transceivers provide an intergrated filter to automatically ignore received packets
 * that are not to be processed by this node.
 *
 * @param Config     pointer to struct t_ttc_radio_config
 * @param Settings   (u_ttc_radio_frame_filter)  configuration to use
 * @return           (u_ttc_radio_frame_filter)  updated configuration (unsupported filters may be reset)
 */
u_ttc_radio_frame_filter radio_stm32w1xx_configure_frame_filter( t_ttc_radio_config* Config, u_ttc_radio_frame_filter Settings );


/** Switches channel used to transmit + receive data.
 *
 * @param Config        = pointer to struct t_ttc_radio_config
 * @param NewChannel      Config->Features->MinChannel .. Config->Features->MaxChannel
 * @return              == 0: channel switched successfully; != 0: error-code
 */
e_ttc_radio_errorcode radio_stm32w1xx_configure_channel_rxtx( t_ttc_radio_config* Config, t_u8 NewChannel );


/** Set new delay to exactly enable transmitter after receiving next message
 *
 * Exact time delays allow to reduce energy by switching off receiver while remote node is preparing an answer packet.
 * Scenario:
 * A:TX @T1a ---------------> B:RX @T1b
 *                            B:prepare answer
 * A:wait until T1a + Delay   B:wait until T1b + Delay
 * A:RX <-------------------- B:TX @T2a
 *
 * @param Config          pointer to struct t_ttc_radio_config
 * @param Delay_uSeconds  >0: time between receival of next message and transmission start of next message; ==0: transmissions are sent immediately
 */
void radio_stm32w1xx_change_delay_rx2tx( t_ttc_radio_config* Config, t_base Delay_uSeconds );


/** Set new delay to exactly enable receiver after next transmission
 *
 * See ttc_radio_change_delay_rx2tx() for power saving scenario using delayed transmissions.
 *
 * Note: Delay must be long egnough to allow software to prepare and transmit a new packet frame into transceiver!
 *
 * @param Config          pointer to struct t_ttc_radio_config
 * @param Delay_uSeconds  >0: time between start of next transmission and automatic reenabling receiver (microseconds); ==0: receiver must be switched on manually
 */
void radio_stm32w1xx_change_delay_tx2rx( t_ttc_radio_config* Config, t_base Delay_uSeconds );


/** Updates meta header of given packet.
 *
 * During reception of a packet frame, an update of Meta header often requires complex calculations.
 * Running these calculations in the interrupt service routine would increase the response time.
 * This function shall be called from application to run the calculations required to fill all fields
 * in Packet->Meta.
 *
 * @param Config        pointer to struct t_ttc_radio_config
 * @param Packet       (t_ttc_packet*)  packet which Meta header is to be updated
 * @return             ==TRUE: Meta header has been updated; ==FALSE: Update of Meta header not possible.
 */
BOOL radio_stm32w1xx_update_meta( t_ttc_radio_config* Config, t_ttc_packet* Packet );


/** Set new delay to exactly enable receiver after next transmission
 *
 * See ttc_radio_change_delay_time() for power saving scenario using delayed transmissions.
 *
 * Note: Delay must be long egnough to allow software to prepare and transmit a new packet frame into transceiver!
 *
 * @param Config          pointer to struct t_ttc_radio_config
 * @param Delay_uSeconds  >0: time between start of next transmission and automatic reenabling receiver (microseconds); ==0: receiver must be switched on manually
 */
void radio_stm32w1xx_change_delay_time( t_ttc_radio_config* Config, t_u16 Delay_uSeconds );


/** low-level interrupt service routine for outgoing packets
 *
 * This function must be registered as interrupt service routine for outgoing packets.
 * Example for interrupts via GPIO pin A4:
 *    t_ttc_interrupt_handle RadioInterrupt = ttc_interrupt_init(tit_GPIO_Rising, E_ttc_gpio_pin_a4, _driver_radio_isr_transmit, Config, 0, 0);
 *    Assert(RadioInterrupt, ttc_assert_origin_auto); // could not initialize interrupt (configured GPIO pin not usable for external interrupts on current uC architecture?)
 *    ttc_interrupt_enable_gpio(RadioInterrupt);
 *
 * This function has to be implemented by low-level radio driver:
 * 1) t_ttc_packet* Packet = radio_common_pop_list_tx( Config )
 * 2) call Config->Init.function_start_tx_isr() (if set)
 * 3) send packet data to transceiver transmit buffer
 * 4) observe transmission
 * 5) call Config->Init.function_end_tx_isr() (if set)
 *
 * The low-level radio driver may declare these functions as extern to be able to call
 * @param Index   index of triggered interrupt (for gpio interrupts, this can be casted to e_ttc_gpio_pin)
 * @param Config  radio configuration as returned by ttc_radio_get_configuration()
 */
void radio_stm32w1xx_isr_transmit( t_physical_index Index, t_ttc_radio_config* Config );


/** Switches radio receiver on or off
 *
 * The receiver can be switched on now at a specified time.
 * Delayed switch times allow to reduce energy usage.
 * This can be done by answering an incoming message after a fixed, predefined delay.
 * Scenario:
 * A sends a message to B. B processes this message and sends an answer to A.
 * 1) Messages are sent immediately
 *    A does not know how long B will take to prepare its answer as runtime of software is difficult to
 *    forecast. So A has to switch on it's receiver after sending to B.
 * 2) Messages are answered after fixed delay
 *    A goes to sleep after sending to B for a predefined delay time.
 *    B has egnough time to prepare its answer.
 *    A switches on its receiver at SendTime + DelayTime
 *    B switches on its transmitter at ReceiveTime + DelayTime
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param Config          configuration of radio device as returned by ttc_radio_get_configuration()
 * @param Enable          !=0: switch on receiver to listen for incoming messages; ==0: switch of receiver (reduces power)
 * @param ReferenceTime   ==NULL: enable/disable receiver immediately; !=NULL: switch state at ReferenceTime + Config->Delay_RX
 */
void radio_stm32w1xx_receiver( t_ttc_radio_config* Config, BOOL Enable, ttc_radio_timestamp_u* ReferenceTime );


/** Add a nanosecond delay to given time stamp
 *
 * Timestamps are transceiver specific. Only current low-level knows its timebase.
 *
 * @param Time        (u_ttc_packetimestamp_40*)  time stamp of a received or transmitted packet
 * @param Delay_ns    (t_u32)                       delay time to add to Time (nanoseconds)
 * @param TimeDelayed (u_ttc_packetimestamp_40*)  will be loaded with Time + Delay_ns; may be same as Time (TimeDelayed==Time)
 * @return  delayed timestamp has been written to *TimeDelayed
 */
void radio_stm32w1xx_time_add_ns( u_ttc_packetimestamp_40* Time, t_u32 Delay_ns, u_ttc_packetimestamp_40* TimeDelayed );


/** Change or read current setting of auto acknowledge feature
 *
 * Auto acknowledgement is a feature being provided by several transceivers. If a packet is received
 * with header flag acknowledge set, the transceiver can immediately generate and sent the acknowledge packet
 * without interaction of the connected microcontroller.
 *
 * The basic auto acknowledgment setting is configured via setting Config->Init.Flags.AutoAcknowledge before calling ttc_radio_init().
 * Even if enabled, the radio may not be able to generate auto acknowledgements because of its current state.
 * This function allows to set and read the current state from low-level driver.
 * The return value may differ from Enable if transceiver is currently not able to support this feature.
 *
 * See datasheet of your transceiver and current low-level driver for details!
 *
 * @param Config       (t_ttc_radio_config*)  Configuration of radio device as returned by ttc_radio_get_configuration()
 * @param LogicalIndex (t_u8)                 Logical index of radio instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RADIO<n> lines in extensions.active/makefile
 * @param UserID       (t_u8)                 ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param Enable       (BOOL)                 ==TRUE: enable auto acknowledge; ==FALSE: disable auto acknowledge
 * @param Change       (BOOL)                 ==TRUE: write value of Enable to radio; ==FALSE: only read current setting
 * @return             (BOOL)                 current auto acknowledge setting; ==TRUE: radio will automatically reply to incoming packets with set acknowledge flag
 */
BOOL radio_stm32w1xx_configure_auto_acknowledge( t_ttc_radio_config* Config, t_u8 UserID, BOOL Enable, BOOL Change );


/** Sets new output state of a GPIO line connected to radio transceiver
 *
 * Some external radio transceivers have configurable gpio lines that can be controlled via software commands.
 * The current implementation must be provided by the low level radio_* driver.
 *
 * @param Config       (t_ttc_radio_config*)  Configuration of radio device as returned by ttc_radio_get_configuration()
 * @param LogicalIndex (t_u8)                 Logical index of radio instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RADIO<n> lines in extensions.active/makefile
 * @param Pin          (e_ttc_radio_gpio)     gpio pin to control (current radio may not provide all possible pins)
 * @param NewState     (BOOL)                 ==TRUE: set GPIO to logical 1; ==FALSE: set GPIO to logical 0
 * @return             (BOOL)                 ==TRUE: desired GPIO pin could be configured to new output state
 */
BOOL radio_stm32w1xx_gpio_out( t_ttc_radio_config* Config, e_ttc_radio_gpio Pin, BOOL NewState );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _radio_stm32w1xx_foo(t_ttc_radio_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //RADIO_STM32W1XX_H