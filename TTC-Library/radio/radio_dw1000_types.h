#ifndef RADIO_DW1000_TYPES_H
#define RADIO_DW1000_TYPES_H

/** { radio_dw1000.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for RADIO devices on dw1000 architectures.
 *  Structures, Enums and Defines being required by ttc_radio_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150309 14:18:02 UTC
 *
 *  Note: See ttc_radio.h for description of architecture independent RADIO implementation.
 *
 *  Authors: Gregor Rebel, Adrián Romero Cáceres
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines

//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "../ttc_register.h"
#include "../ttc_packet_types.h"
#include "../ttc_radio_types.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes

/** { Check static configuration of SPI device
 *
 * DecaWave 1000 is always connected via an SPI bus.
 * The current board makefile must define
 * - TTC_RADIO<n>_SPI                logical index <s> of TTC_SPI<s> device to use to communicate with DW1000
 * - TTC_RADIO<n>_SPI_NSS            gpio pin to use as slave select for connected DW1000 (one from e_ttc_gpio_pin)
 * - TTC_RADIO<n>_DW1000_PIN_SPIPHA  gpio pin connected to SPIPHA pin of DW1000 (one from e_ttc_gpio_pin)
 * - TTC_RADIO<n>_DW1000_PIN_SPIPOL  gpio pin connected to SPIPOL pin of DW1000 (one from e_ttc_gpio_pin)
 * - TTC_RADIO<n>_DW1000_PIN_IRQ     gpio pin connected to IRQ pin of DW1000 (one from e_ttc_gpio_pin)
 * - TTC_SPI<s>_*                    SPI device configuration (as described in ttc_spi.h)
 *
 * Example excerpt from a board makefile:
   COMPILE_OPTS += -DTTC_RADIO1_INDEX_SPI=1                     # logical index of SPI device to use to communicate with radio #1
   COMPILE_OPTS += -DTTC_RADIO1_INDEX_SPI_NSS=1                 # logical index of slave select pin to use
   COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_SPIPHA=E_ttc_gpio_pin_c9  # GPIO6
   COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_SPIPOL=E_ttc_gpio_pin_c6  # GPIO3
   COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_IRQ=E_ttc_gpio_pin_c10    # IRQ1
   COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_IRQ2=E_ttc_gpio_pin_c13   # WakeUp (connected to TTC_RADIO1_DW1000_PIN_IRQ on this board)
   COMPILE_OPTS += -DTTC_SPI1=ttc_device_1                # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
   COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a7                 # GPIO-Pin used for Master Out Slave In
   COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a6                 # GPIO-Pin used for Master In Slave Out
   COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_a5                  # GPIO-Pin used for Serial Clock
   COMPILE_OPTS += -DTTC_SPI1_NSS1=E_ttc_gpio_pin_b1                 # GPIO-Pin used for Slave Select #1
 */

// default value for missing TTC_RADIO<n>_DW1000_TX_POWER defines
#define RADIO_DW1000_TX_POWER_DEFAULT 0x15355575

#ifdef TTC_RADIO1_INDEX_SPI
    #ifndef TTC_RADIO1_INDEX_SPI_NSS
        #    error Missing definition for TTC_RADIO1_INDEX_SPI_NSS. Define it as logical index of slave select pin to use for DW1000!
    #endif
    #ifndef TTC_RADIO1_DW1000_PIN_SPIPHA
        #    warning Missing definition of TTC_RADIO1_DW1000_PIN_SPIPHA. Define it as GPIO pin connected to DW1000 pin SPIPHA or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO1_DW1000_PIN_SPIPHA E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO1_DW1000_PIN_SPIPOL
        #    warning Missing definition of TTC_RADIO1_DW1000_PIN_SPIPOL. Define it as GPIO pin connected to DW1000 pin SPIPOL or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO1_DW1000_PIN_SPIPOL E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO1_DW1000_PIN_IRQ
        #    warning Missing definition of TTC_RADIO1_DW1000_PIN_IRQ. Define it as GPIO pin connected to DW1000 pin IRQ or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO1_DW1000_PIN_IRQ E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO1_DW1000_TX_POWER
        #    warning Missing definition of TTC_RADIO1_DW1000_TX_POWER. Define it as a valid setting for register TX_POWER according to your actual hardware setup to meet your countries FCC regulations! (using internal default).
        #define TTC_RADIO1_DW1000_TX_POWER RADIO_DW1000_TX_POWER_DEFAULT
    #endif
#endif
#ifdef TTC_RADIO2_INDEX_SPI
    #ifndef TTC_RADIO2_INDEX_SPI_NSS
        #    error Missing definition for TTC_RADIO2_INDEX_SPI_NSS. Define it as logical index of slave select pin to use for DW1000!
    #endif
    #ifndef TTC_RADIO2_DW1000_PIN_SPIPHA
        #    warning Missing definition of TTC_RADIO2_DW1000_PIN_SPIPHA. Define it as GPIO pin connected to DW1000 pin SPIPHA or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO2_DW1000_PIN_SPIPHA E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO2_DW1000_PIN_SPIPOL
        #    warning Missing definition of TTC_RADIO2_DW1000_PIN_SPIPOL. Define it as GPIO pin connected to DW1000 pin SPIPOL or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO2_DW1000_PIN_SPIPOL E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO2_DW1000_PIN_IRQ
        #    warning Missing definition of TTC_RADIO2_DW1000_PIN_IRQ. Define it as GPIO pin connected to DW1000 pin IRQ or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO2_DW1000_PIN_IRQ E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO2_DW1000_TX_POWER
        #    warning Missing definition of TTC_RADIO2_DW1000_TX_POWER. Define it as a valid setting for register TX_POWER according to your actual hardware setup to meet your countries FCC regulations! (using internal default).
        #define TTC_RADIO2_DW1000_TX_POWER RADIO_DW1000_TX_POWER_DEFAULT
    #endif
#endif
#ifdef TTC_RADIO3_INDEX_SPI
    #ifndef TTC_RADIO3_INDEX_SPI_NSS
        #    error Missing definition for TTC_RADIO3_INDEX_SPI_NSS. Define it as logical index of slave select pin to use for DW1000!
    #endif
    #ifndef TTC_RADIO3_DW1000_PIN_SPIPHA
        #    warning Missing definition of TTC_RADIO3_DW1000_PIN_SPIPHA. Define it as GPIO pin connected to DW1000 pin SPIPHA or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO3_DW1000_PIN_SPIPHA E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO3_DW1000_PIN_SPIPOL
        #    warning Missing definition of TTC_RADIO3_DW1000_PIN_SPIPOL. Define it as GPIO pin connected to DW1000 pin SPIPOL or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO3_DW1000_PIN_SPIPOL E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO3_DW1000_PIN_IRQ
        #    warning Missing definition of TTC_RADIO3_DW1000_PIN_IRQ. Define it as GPIO pin connected to DW1000 pin IRQ or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO3_DW1000_PIN_IRQ E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO3_DW1000_TX_POWER
        #    warning Missing definition of TTC_RADIO3_DW1000_TX_POWER. Define it as a valid setting for register TX_POWER according to your actual hardware setup to meet your countries FCC regulations! (using internal default).
        #define TTC_RADIO3_DW1000_TX_POWER RADIO_DW1000_TX_POWER_DEFAULT
    #endif
#endif
#ifdef TTC_RADIO4_INDEX_SPI
    #ifndef TTC_RADIO4_INDEX_SPI_NSS
        #    error Missing definition for TTC_RADIO4_INDEX_SPI_NSS. Define it as logical index of slave select pin to use for DW1000!
    #endif
    #ifndef TTC_RADIO4_DW1000_PIN_SPIPHA
        #    warning Missing definition of TTC_RADIO4_DW1000_PIN_SPIPHA. Define it as GPIO pin connected to DW1000 pin SPIPHA or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO4_DW1000_PIN_SPIPHA E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO4_DW1000_PIN_SPIPOL
        #    warning Missing definition of TTC_RADIO4_DW1000_PIN_SPIPOL. Define it as GPIO pin connected to DW1000 pin SPIPOL or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO4_DW1000_PIN_SPIPOL E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO4_DW1000_PIN_IRQ
        #    warning Missing definition of TTC_RADIO4_DW1000_PIN_IRQ. Define it as GPIO pin connected to DW1000 pin IRQ or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO4_DW1000_PIN_IRQ E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO4_DW1000_TX_POWER
        #    warning Missing definition of TTC_RADIO4_DW1000_TX_POWER. Define it as a valid setting for register TX_POWER according to your actual hardware setup to meet your countries FCC regulations! (using internal default).
        #define TTC_RADIO4_DW1000_TX_POWER RADIO_DW1000_TX_POWER_DEFAULT
    #endif
#endif
#ifdef TTC_RADIO5_INDEX_SPI
    #ifndef TTC_RADIO5_INDEX_SPI_NSS
        #    error Missing definition for TTC_RADIO5_INDEX_SPI_NSS. Define it as logical index of slave select pin to use for DW1000!
    #endif
    #ifndef TTC_RADIO5_DW1000_PIN_SPIPHA
        #    warning Missing definition of TTC_RADIO5_DW1000_PIN_SPIPHA. Define it as GPIO pin connected to DW1000 pin SPIPHA or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO5_DW1000_PIN_SPIPHA E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO5_DW1000_PIN_SPIPOL
        #    warning Missing definition of TTC_RADIO5_DW1000_PIN_SPIPOL. Define it as GPIO pin connected to DW1000 pin SPIPOL or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO5_DW1000_PIN_SPIPOL E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO5_DW1000_PIN_IRQ
        #    warning Missing definition of TTC_RADIO5_DW1000_PIN_IRQ. Define it as GPIO pin connected to DW1000 pin IRQ or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO5_DW1000_PIN_IRQ E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO5_DW1000_TX_POWER
        #    warning Missing definition of TTC_RADIO5_DW1000_TX_POWER. Define it as a valid setting for register TX_POWER according to your actual hardware setup to meet your countries FCC regulations! (using internal default).
        #define TTC_RADIO5_DW1000_TX_POWER RADIO_DW1000_TX_POWER_DEFAULT
    #endif
#endif
#ifdef TTC_RADIO6_INDEX_SPI
    #ifndef TTC_RADIO6_INDEX_SPI_NSS
        #    error Missing definition for TTC_RADIO6_INDEX_SPI_NSS. Define it as logical index of slave select pin to use for DW1000!
    #endif
    #ifndef TTC_RADIO6_DW1000_PIN_SPIPHA
        #    warning Missing definition of TTC_RADIO6_DW1000_PIN_SPIPHA. Define it as GPIO pin connected to DW1000 pin SPIPHA or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO6_DW1000_PIN_SPIPHA E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO6_DW1000_PIN_SPIPOL
        #    warning Missing definition of TTC_RADIO6_DW1000_PIN_SPIPOL. Define it as GPIO pin connected to DW1000 pin SPIPOL or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO6_DW1000_PIN_SPIPOL E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO6_DW1000_PIN_IRQ
        #    warning Missing definition of TTC_RADIO6_DW1000_PIN_IRQ. Define it as GPIO pin connected to DW1000 pin IRQ or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO6_DW1000_PIN_IRQ E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO6_DW1000_TX_POWER
        #    warning Missing definition of TTC_RADIO6_DW1000_TX_POWER. Define it as a valid setting for register TX_POWER according to your actual hardware setup to meet your countries FCC regulations! (using internal default).
        #define TTC_RADIO6_DW1000_TX_POWER RADIO_DW1000_TX_POWER_DEFAULT
    #endif
#endif
#ifdef TTC_RADIO7_INDEX_SPI
    #ifndef TTC_RADIO7_INDEX_SPI_NSS
        #    error Missing definition for TTC_RADIO7_INDEX_SPI_NSS. Define it as logical index of slave select pin to use for DW1000!
    #endif
    #ifndef TTC_RADIO7_DW1000_PIN_SPIPHA
        #    warning Missing definition of TTC_RADIO7_DW1000_PIN_SPIPHA. Define it as GPIO pin connected to DW1000 pin SPIPHA or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO7_DW1000_PIN_SPIPHA E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO7_DW1000_PIN_SPIPOL
        #    warning Missing definition of TTC_RADIO7_DW1000_PIN_SPIPOL. Define it as GPIO pin connected to DW1000 pin SPIPOL or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO7_DW1000_PIN_SPIPOL E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO7_DW1000_PIN_IRQ
        #    warning Missing definition of TTC_RADIO7_DW1000_PIN_IRQ. Define it as GPIO pin connected to DW1000 pin IRQ or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO7_DW1000_PIN_IRQ E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO7_DW1000_TX_POWER
        #    warning Missing definition of TTC_RADIO7_DW1000_TX_POWER. Define it as a valid seRADIO_DW1000_TX_POWER_DEFAULTtting for register TX_POWER according to your actual hardware setup to meet your countries FCC regulations! (using internal default).
        #define TTC_RADIO7_DW1000_TX_POWER RADIO_DW1000_TX_POWER_DEFAULT
    #endif
#endif
#ifdef TTC_RADIO8_INDEX_SPI
    #ifndef TTC_RADIO8_INDEX_SPI_NSS
        #    error Missing definition for TTC_RADIO8_INDEX_SPI_NSS. Define it as logical index of slave select pin to use for DW1000!
    #endif
    #ifndef TTC_RADIO8_DW1000_PIN_SPIPHA
        #    warning Missing definition of TTC_RADIO8_DW1000_PIN_SPIPHA. Define it as GPIO pin connected to DW1000 pin SPIPHA or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO8_DW1000_PIN_SPIPHA E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO8_DW1000_PIN_SPIPOL
        #    warning Missing definition of TTC_RADIO8_DW1000_PIN_SPIPOL. Define it as GPIO pin connected to DW1000 pin SPIPOL or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO8_DW1000_PIN_SPIPOL E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO8_DW1000_PIN_IRQ
        #    warning Missing definition of TTC_RADIO8_DW1000_PIN_IRQ. Define it as GPIO pin connected to DW1000 pin IRQ or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO8_DW1000_PIN_IRQ E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO8_DW1000_TX_POWER
        #    warning Missing definition of TTC_RADIO8_DW1000_TX_POWER. Define it as a valid setting for register TX_POWER according to your actual hardware setup to meet your countries FCC regulations! (using internal default).
        #define TTC_RADIO8_DW1000_TX_POWER RADIO_DW1000_TX_POWER_DEFAULT
    #endif
#endif
#ifdef TTC_RADIO9_INDEX_SPI
    #ifndef TTC_RADIO9_INDEX_SPI_NSS
        #    error Missing definition for TTC_RADIO9_INDEX_SPI_NSS. Define it as logical index of slave select pin to use for DW1000!
    #endif
    #ifndef TTC_RADIO9_DW1000_PIN_SPIPHA
        #    warning Missing definition of TTC_RADIO9_DW1000_PIN_SPIPHA. Define it as GPIO pin connected to DW1000 pin SPIPHA or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO9_DW1000_PIN_SPIPHA E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO9_DW1000_PIN_SPIPOL
        #    warning Missing definition of TTC_RADIO9_DW1000_PIN_SPIPOL. Define it as GPIO pin connected to DW1000 pin SPIPOL or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO9_DW1000_PIN_SPIPOL E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO9_DW1000_PIN_IRQ
        #    warning Missing definition of TTC_RADIO9_DW1000_PIN_IRQ. Define it as GPIO pin connected to DW1000 pin IRQ or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO9_DW1000_PIN_IRQ E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO9_DW1000_TX_POWER
        #    warning Missing definition of TTC_RADIO9_DW1000_TX_POWER. Define it as a valid setting for register TX_POWER according to your actual hardware setup to meet your countries FCC regulations! (using internal default).
        #define TTC_RADIO9_DW1000_TX_POWER RADIO_DW1000_TX_POWER_DEFAULT
    #endif
#endif
#ifdef TTC_RADIO10_INDEX_SPI
    #ifndef TTC_RADIO10_INDEX_SPI_NSS
        #    error Missing definition for TTC_RADIO10_INDEX_SPI_NSS. Define it as logical index of slave select pin to use for DW1000!
    #endif
    #ifndef TTC_RADIO10_DW1000_PIN_SPIPHA
        #    warning Missing definition of TTC_RADIO10_DW1000_PIN_SPIPHA. Define it as GPIO pin connected to DW1000 pin SPIPHA or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO10_DW1000_PIN_SPIPHA E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO10_DW1000_PIN_SPIPOL
        #    warning Missing definition of TTC_RADIO10_DW1000_PIN_SPIPOL. Define it as GPIO pin connected to DW1000 pin SPIPOL or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO10_DW1000_PIN_SPIPOL E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO10_DW1000_PIN_IRQ
        #    warning Missing definition of TTC_RADIO10_DW1000_PIN_IRQ. Define it as GPIO pin connected to DW1000 pin IRQ or as E_ttc_gpio_pin_none if not connected!
        #define TTC_RADIO10_DW1000_PIN_IRQ E_ttc_gpio_pin_none
    #endif
    #ifndef TTC_RADIO10_DW1000_TX_POWER
        #    warning Missing definition of 1TTC_RADIO10_DW1000_TX_POWER. Define it as a valid setting for register TX_POWER according to your actual hardware setup to meet your countries FCC regulations! (using internal default).
        #define TTC_RADIO10_DW1000_TX_POWER RADIO_DW1000_TX_POWER_DEFAULT
    #endif
#endif

//} Check static configuration of SPI device
//{ Structures/ Enums required by ttc_radio_types.h *************************

// define array sizes
#define RADIO_DW1000_AMOUNT_PREAMBLE_CODES 25 // supported preamble codes
#define RADIO_DW1000_SIZE_POWER            2

/* DEPRECATED (old stuff from DecawaveExample)
#define RADIO_DW1000_SIZE_TX_POW_CFG 12
#define RADIO_DW1000_SIZE_STATES     3

// define characteristics of DW1000
#define RADIO_DW1000_NUM_BR 3
#define RADIO_DW1000_NUM_PRF 2
#define RADIO_DW1000_NUM_PACS 4
#define RADIO_DW1000_NUM_BW 2                 // 2 bandwidths are supported
#define RADIO_DW1000_NUM_SFD 2                // supported number of SFDs - standard = 0, non-standard = 1
#define RADIO_DW1000_NUM_CH 6                 // supported channels are 1, 2, 3, 4, 5, 7
#define RADIO_DW1000_NUM_CH_SUPPORTED 8       // supported channels are '0', 1, 2, 3, 4, 5, '6', 7

*/

#include "../ttc_basic_types.h"
#include "../ttc_gpio_types.h"
//?required #include "register_and_variables_dw1000.h" // ToDo: move content into radio_dw1000.c, .h, _types-h

typedef uint64_t        u64_t ;

typedef enum { /* list of all virtual channels provided by DW1000
    *
    * A virtual channel is a combination of
    * - frequency (freq)
    * - bandwidth (bw)
    * - Preamble Code (PRC)
    *
    * radio_dw1000 combines all of these into one virtual channel.
    * Different virtual channels should not interfere with each other, even on same frequency.
    * This increases the amount of usable channels.
    * A high-level application just sees a channel number 0..rdvc_virtual_unknown-1.
    */
    // channel 1
    rdvc_virtual1_freq3494_bw499_PRC1,
    rdvc_virtual2_freq3494_bw499_PRC2,
    rdvc_virtual3_freq3494_bw499_PRC9,
    rdvc_virtual4_freq3494_bw499_PRC10,
    rdvc_virtual5_freq3494_bw499_PRC11,
    rdvc_virtual6_freq3494_bw499_PRC12,

    // channel 2
    rdvc_virtual7_freq3993_bw499_PRC3,
    rdvc_virtual8_freq3993_bw499_PRC4,
    rdvc_virtual9_freq3993_bw499_PRC9,
    rdvc_virtual10_freq3993_bw499_PRC10,
    rdvc_virtual11_freq3993_bw499_PRC11,
    rdvc_virtual12_freq3993_bw499_PRC12,

    // channel 3
    rdvc_virtual13_freq4492_bw499_PRC5,
    rdvc_virtual14_freq4492_bw499_PRC6,
    rdvc_virtual15_freq4492_bw499_PRC9,
    rdvc_virtual16_freq4492_bw499_PRC10,
    rdvc_virtual17_freq4492_bw499_PRC11,
    rdvc_virtual18_freq4492_bw499_PRC12,

    // channel 4
    rdvc_virtual19_freq3993_bw1331_PRC7,
    rdvc_virtual20_freq3993_bw1331_PRC8,
    rdvc_virtual21_freq3993_bw1331_PRC17,
    rdvc_virtual22_freq3993_bw1331_PRC18,
    rdvc_virtual23_freq3993_bw1331_PRC19,
    rdvc_virtual24_freq3993_bw1331_PRC20,

    // channel 5
    rdvc_virtual25_freq6489_bw499_PRC3,
    rdvc_virtual26_freq6489_bw499_PRC4,
    rdvc_virtual27_freq6489_bw499_PRC9,
    rdvc_virtual28_freq6489_bw499_PRC10,
    rdvc_virtual29_freq6489_bw499_PRC11,
    rdvc_virtual30_freq6489_bw499_PRC12,

    // channel 7
    rdvc_virtual31_freq6489_bw1081_PRC7,
    rdvc_virtual32_freq6489_bw1081_PRC8,
    rdvc_virtual33_freq6489_bw1081_PRC17,
    rdvc_virtual34_freq6489_bw1081_PRC18,
    rdvc_virtual35_freq6489_bw1081_PRC19,
    rdvc_virtual36_freq6489_bw1081_PRC20,

    rdvc_virtual_unknown
} e_radio_dw1000_virtual_channel;

typedef enum {
    radio_dw1000_datarate_110k = 0b00,
    radio_dw1000_datarate_850k = 0b01,
    radio_dw1000_datarate_6M8  = 0b10,
    radio_dw1000_datarate_unknown
} e_radio_dw1000_datarates;

//{ register descriptions according to DW1000 User Manual ----------------------------------------------

typedef union { // DW1000 3 byte register address format (UserManual p.12)
    t_u32 Word;
    struct {
        unsigned Index           : 6; // register index number (0..63)
        unsigned SubIndexPresent : 1; // ==1: sub-index is present; ==0: single octet header only
        unsigned WriteOperation  : 1; // ==1: Write-Operation; ==0: Read-Operation

        // optional fields below (only available if SubIndexPresent==1)
        unsigned SubIndex        : 7; // sub-index to register ID
        unsigned ExtendedPresent : 1; // ==1: ID_High is present; ==0: two octet header only

        // optional fields below (only available if ExtendedPresent==1)
        t_u8     SubIndexHigh;        // bits 7..14 of 15-bit sub index (contains Most Significant Bit from Low-Byte of SubIndex! -> DW1000 UserManual p.13)

        t_u8     HeaderLength;        // length of transaction header (1..3)
    } __attribute__( ( packed ) ) Fields;
} t_radio_dw1000_transaction;

typedef enum { // register base (format: t_radio_dw1000_transaction)

    // -> DW1000 User Manual v2.05 p. 62
    //
    // Note: These values are precompiled for maximum speed and minimal RAM usage.
    //       When adding entries to this enum, ensure correct field values!
    //       You may want to assemble the value by use of _radio_dw1000_compile_transaction_header() during debugging.
    //       E.g. (gdb session): x _radio_dw1000_compile_transaction_header(0x15, 0x0007, 0).Word
    //       Also add a corresponding check into radio_dw1000_prepare()!

    rdr_DEV_ID       = 0x1000000,  // 00:0000 Device ID
    rdr_EUI64        = 0x1000001,  // 01:0000 Extended Unique Identifier (64 bit physical address)
    rdr_SHORT_ADDR   = 0x1000003,  // 03:0000 16 bit Local ID
    rdr_PAN_ID       = 0x2000243,  // 03:0002 16 bit Local Pan ID
    rdr_SYS_CFG      = 0x1000004,  // 04:0000 System Configuration
    rdr_SYS_TIME     = 0x1000006,  // 06:0000 System Time Counter
    rdr_TX_FCTRL     = 0x1000008,  // 08:0000 Transmitter Frame Control
    rdr_TX_BUFFER    = 0x1000009,  // 09:0000 Transmit Data Buffer (1024 bytes)
    rdr_DX_TIME      = 0x100000a,  // 0a:0000 Delayed Send or Receive Time (40 bits)
    rdr_RX_FWTO      = 0x100000c,  // 0c:0000 Receiver Frame Wait Timeout Period
    rdr_SYS_CTRL     = 0x100000d,  // 0d:0000 System Control
    rdr_SYS_MASK     = 0x100000e,  // 0e:0000 System Event Masks
    rdr_SYS_STATUS   = 0x100000f,  // 0f:0000 System Event Status
    rdr_RX_FINFO     = 0x1000010,  // 10:0000 Receiver Frame Information (double buffered operation only)
    rdr_RX_BUFFER    = 0x1000011,  // 11:0000 Receiver Frame Buffer (1024 bytes)
    rdr_RX_FQUAL     = 0x1000012,  // 12:0000 Receiver Frame Quality Information
    rdr_RX_TTCKI     = 0x1000013,  // 13:0000 Receiver Time Tracking Interval
    rdr_RX_TTCKO     = 0x1000014,  // 14:0000 Receiver Time Tracking Offset
    rdr_RX_TIME      = 0x1000015,  // 15:0000 adjusted time stamp of last received frame
    rdr_RX_STAMP     = rdr_RX_TIME,// Adjusted Receive Time of first ray (Leading Edge)
    rdr_FIRST_PATH   = 0x2000555,  // 15:0005 First Path Data of received frame
    rdr_RX_FPAMPL1   = 0x2000755,  // 15:0007 Receive Time Stamp First Path Amplitude Point 1
    rdr_RX_RAWST     = 0x2000955,  // 15:0009 raw time stamp of last received frame
    rdr_TX_STAMP     = 0x1000017,  // 17:0000 time stamp of last transmission
    rdr_TX_RAWST     = 0x2000557,  // 17:0005 raw time stamp of last transmission
    rdr_TX_ANTD      = 0x1000018,  // 18:0000 Transmitter Antenna Delay
    rdr_SYS_STATE    = 0x1000019,  // 19:0000 System State Information
    rdr_ACK_RESP_T   = 0x100001a,  // 1a:0000 Acknowledgement Time and Response Time
    rdr_RX_SNIFF     = 0x100001d,  // 1d:0000 Pulsed Preamble Reception Configuration
    rdr_TX_POWER     = 0x100001e,  // 1e:0000 Transmitter Power Control
    rdr_CHAN_CTRL    = 0x100001f,  // 1f:0000 Channel Control
    rdr_USR_SFD      = 0x1000021,  // 21:0000 User specified short/long TX/RX Start of Frame Delimiter sequences
    //rdr_AGC_CTRL     = 0x1000023,  // 23:0000 Automatic Gain Control
    rdr_AGC_CTRL1    = 0x2000263,  // 23:0002 Automatic Gain Control 1
    rdr_AGC_TUNE1    = 0x2000463,  // 23:0004 Automatic Gain Control Tuning 1
    rdr_AGC_TUNE2    = 0x2000c63,  // 23:000c Automatic Gain Control Tuning 2
    rdr_AGC_TUNE3    = 0x2001263,  // 23:0012 Automatic Gain Control Tuning 3
    rdr_AGC_STAT1    = 0x2001e63,  // 23:001e Automatic Gain Control Status
    rdr_EC_CTRL      = 0x1000024,  // 24:0000 External Synchronization Control
    rdr_ACC_MEM      = 0x1000025,  // 25:0000 read access to accumulator data
    rdr_GPIO_MODE    = 0x1000026,  // 26:0000 GPIO Mode Control Register -> DW1000 UserManual v2.03 p.121
    rdr_GPIO_DIR     = 0x2000866,  // 26:0008 GPIO Direction Control Register -> DW1000 UserManual v2.03 p.123
    rdr_GPIO_DOUT    = 0x2000c66,  // 26:000c GPIO Data Output register -> DW1000 User Manual p.125
    rdr_GPIO_IRQE    = 0x2001066,  // 26:0010 GPIO Interrupt Enable -> DW1000 User Manual p.127
    rdr_GPIO_ISEN    = 0x2001466,  // 26:0014 GPIO Interrupt Sense Selection -> DW1000 User Manual p.128
    rdr_GPIO_IMODE   = 0x2001866,  // 26:0018 GPIO Interrupt Mode (Level / Edge) -> DW1000 User Manual p.129
    rdr_GPIO_IBES    = 0x2001c66,  // 26:001c GPIO Interrupt “Both Edge” Select -> DW1000 User Manual p.130
    rdr_GPIO_ICLR    = 0x2002066,  // 26:0020 GPIO Interrupt Latch Clear -> DW1000 User Manual p.131
    rdr_GPIO_IDBE    = 0x2002466,  // 26:0024 GPIO Interrupt De-bounce Enable -> DW1000 User Manual p.132
    rdr_GPIO_RAW     = 0x2002866,  // 26:0028 GPIO raw state -> DW1000 User Manual p.133
    rdr_DRX_TUNE0b   = 0x2000267,  // 27:0002 Digital Receiver Tune 0b
    rdr_DRX_TUNE1a   = 0x2000467,  // 27:0004 Digital Receiver Tune 1a
    rdr_DRX_TUNE1b   = 0x2000667,  // 27:0006 Digital Receiver Tune 1b
    rdr_DRX_TUNE2    = 0x2000867,  // 27:0008 Digital Receiver Tune 2
    rdr_DRX_SFDTOC   = 0x2002067,  // 27:0020 Digital Receiver Start Frame Delimiter Timeout
    rdr_DRX_PRETOC   = 0x2002467,  // 27:0024 Digital Receiver Preamble Detection Timeout
    rdr_DRX_TUNE4H   = 0x2002667,  // 27:0026 Digital Receiver Tuning Register 4H
    rdr_RF_CONF      = 0x1000028,  // 28:0000 RF Configuration Register (manual test modes) -> DW1000 user manual p.140
    rdr_RF_RXCTRLH   = 0x2000b68,  // 28:000b Analog RX Control Register
    rdr_RF_TXCTRL    = 0x2000c68,  // 28:000c Analog Transmitter Control
    rdr_RF_STATUS    = 0x2002c68,  // 28:002c PLL-Lock Status Register-> DW1000 User Manual p.143
    rdr_TX_CAL       = 0x100002a,  // 2a:0000 Transmitter Calibration Block
    rdr_TC_PGDELAY   = 0x2000b6a,  // 2a:000b Transmitter Pulse Generator Delay
    rdr_FS_CTRL      = 0x100002b,  // 2b:0000 Frequency Synthesizer Control
    rdr_FS_PLLCFG    = 0x200076b,  // 2b:0007 Frequency Synthesizer PLL Configuration
    rdr_FS_PLLTUNE   = 0x2000b6b,  // 2b:000b Frequency Synthesizer PLL Tuning
    rdr_FS_XTALT     = 0x2000e6b,  // 2b:000e Frequency Synthesizer Crystal trim
    rdr_AON          = 0x100002c,  // 2c:0000 Always On Configuration (??? DecaWave implementation used 0x28:00!)
    rdr_AON_WCFG     = rdr_AON,    // 2c:0000 Always On Configuration WCFG
    rdr_AON_CTRL     = 0x200026c,  // 2c:0002 Always-On Control Register -> DW1000 User Manual p.153
    rdr_AON_RDAT     = 0x200036c,  // 2c:0003 Always On Direct Access Read Data Result -> DW1000 User Manual p.156
    rdr_AON_ADDR     = 0x200046c,  // 2c:0004 Always On Direct Access Address -> DW1000 User Manual p.157
    rdr_AON_CFG0     = 0x200066c,  // 2c:0006 Always On Configuration Register 0 -> DW1000 User Manual p.157
    rdr_AON_CFG1     = 0x2000a6c,  // 2c:000a Always On Configuration Register 1 -> DW1000 User Manual p.159
    rdr_OTP_IF       = 0x100002d,  // 2d:0000 One Time Programmable Memory Interface
    rdr_OTP_ADDR     = 0x200046d,  // 2d:0004 One Time Programmable Memory Address
    rdr_OTP_CTRL     = 0x200066d,  // 2d:0006 One Time Programmable Memory Control
    rdr_OTP_STAT     = 0x200086d,  // 2d:0008 One Time Programmable Status
    rdr_OTP_WDAT     = 0x100002d,  // 2d:0000 One Time Programmable Memory Write Data
    rdr_OTP_RDAT     = 0x2000a6d,  // 2d:000a One Time Programmable Memory Read Data
    rdr_OTP_SF       = 0x200126d,  // 2d:0012 One Time Programmable Memory Special Function
    rdr_LDE_THRESH   = 0x100002e,  // 2e:0000 Leading Edge Detection Threshold Report
    rdr_LDE_CFG1     = 0x310866e,  // 2e:0806 Leading Edge Detection Configuration 1
    rdr_LDE_PPINDX   = 0x320806e,  // 2e:1000 Leading Edge Detection Peak Path Index
    rdr_LDE_PPAMPL   = 0x320826e,  // 2e:1002 Leading Edge Detection Peak Path Amplitude
    rdr_LDE_RXANTD   = 0x330846e,  // 2e:1804 Leading Edge Detection Receive Antenna Delay
    rdr_LDE_CFG2     = 0x330866e,  // 2e:1806 Leading Edge Detection Configuration 2
    rdr_LDE_REPC     = 0x350846e,  // 2e:2804 Leading Edge Detection Control
    rdr_DIG_DIAG     = 0x100002f,  // 2f:0000 Digital Diagnostics Interface
    rdr_PMSC_CTRL0   = 0x1000036,  // 36:0000 Power Management and System Control 0
    rdr_PMSC_CTRL1   = 0x2000476,  // 36:0004 Power Management and System Control 1
    rdr_PMSC_SNOZT   = 0x2000c76,  // 36:000c PMSC Snooze Time
    rdr_PMSC_TXFSEQ  = 0x2002676,  // 36:0026 PMSC fine grain TX sequencinfg control
    rdr_PMSC_LEDC    = 0x2002876,  // 36:0028 PMSC LED Control

} e_radio_dw1000_register;

typedef enum { // Transmit Pulse Repetition Frequency (-> DW1000 Manual p. 75)
    radio_dw1000_prf_4MHz  = 0b00, // not supported by DW1000
    radio_dw1000_prf_16MHz = 0b01,
    radio_dw1000_prf_64MHz = 0b10,
    radio_dw1000_prf_unknown
} e_radio_dw1000_pulse_repetition_frequency;

typedef enum { // Maximum supported frame length (-> DW1000 Manual p. 27)
    radio_dw1000_mps_127_bytes  = 0b00, // 802.15.4 compliant payload 0..127 bytes
    radio_dw1000_mps_1023_bytes = 0b11, // proprietary payload 0-1023 bytes
    radio_dw1000_mps_unknown
} e_radio_dw1000_max_payload_size;

typedef union { // register SYS_CFG 0x04:00 -> DW1000 User Manual p.68
    t_u32 Word;

    struct {
        unsigned FFEN      : 1; // 1: Enable Frame Filtering
        unsigned FFBC      : 1; // 1: Frame Filtering behave as Coordinator
        unsigned FFAB      : 1; // 1: Frame Filter allows Beacon Reception
        unsigned FFAD      : 1; // 1: Frame Filter allows Data Reception
        unsigned FFAA      : 1; // 1: Frame Filter allows Acknowledgement Reception
        unsigned FFAM      : 1; // 1: Frame Filter allows MAC Cmmand Reception
        unsigned FFAR      : 1; // 1: Frame Filter allows Reserved Types Reception
        unsigned FFA4      : 1; // 1: Frame Filter allows Type 4 Reception
        unsigned FFA5      : 1; // 1: Frame Filter allows Type 5 Reception
        unsigned HIRQ_POL  : 1; // 1: IRQ output line is high-active; low-active otherwise
        unsigned SPI_EDGE  : 1; // 1: SPI samples MISO on opposing edge (more robust communication); 0: on sampling edge (highest data rate)
        unsigned DIS_FCE   : 1; // 1: Disable Frame Check Error Handling for non 802.15.4-2011 conform protocols
        unsigned DIS_DRXB  : 1; // 1: Disable Receiver Double Buffering
        unsigned DIS_PHE   : 1; // 1: Receive Frame even with corrupted MAC Header
        unsigned DIS_RSDE  : 1; // 1: Receive Frame even with corrupted Red Solomon Decoded Data
        unsigned FCS_INIT2F: 1; // 1: use 0xffff as Initial Seed for Frame Check Sum calculation; 0: use 0x0000
        unsigned PHR_MODE  : 2; // -> e_radio_dw1000_max_payload_size
        unsigned DIS_STXP  : 1; // 1: Disable Smart TX Power Control; 0: Enable (only for 6.8 Mb/s rate and for < 1 frame/msec)
        unsigned reserved1 : 3;
        unsigned RXM110K   : 1; // 1: Receiver Mode 110 kbps only; 0: Receiver accepts 850 kbps and 6.8 Mbps headers
        unsigned reserved2 : 5;
        unsigned RXWTOE    : 1; // 1: Enable Receiver Wait Timeout as configured in RX_FWTO register
        unsigned RXAUTR    : 1; // 1: Receiver Auto Re-Enable for high data rate mode
        unsigned AUTOACK   : 1; // 1: Enable Automatic Acknowledgement
        unsigned AACKPEND  : 1; // 1: Value is copied into FCF.Fields.FramePending of automatic generated ACK frames

    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_sys_cfg;

typedef enum { // Combination of TXPSR and PE field value giving all valid preamble configurations (-> DW1000 Manual p. 76)
    radio_dw1000_pl_64   = 0b0001, // = 1
    radio_dw1000_pl_128  = 0b0101, // = 5
    radio_dw1000_pl_256  = 0b1001, // = 9
    radio_dw1000_pl_512  = 0b1101, // = 13
    radio_dw1000_pl_1024 = 0b0010, // = 2
    radio_dw1000_pl_1536 = 0b0110, // = 6
    radio_dw1000_pl_2048 = 0b1010, // = 10
    radio_dw1000_pl_4096 = 0b0011, // = 3
    radio_dw1000_pl_unknown
} e_radio_dw1000_preamble_configuration;

typedef union { // Transmit Frame Control 0x08:00 -> DW1000 User Manual p.73
    t_u8 Bytes[5];

    struct {
        // Note: Each status bit is cleared by writing a 1 to it

        unsigned TFLEN     : 10; // length of frame to transmit including 2 byte FCS (only 7 bits available if SYS_CTRL.SFCST==1)
        unsigned reserved1 :  3; //
        unsigned TXBR      :  2; // 1: transmit datarate ->e_radio_dw1000_datarates
        unsigned TR        :  1; // 1: transmit ranging info (copied into MAC header only)
        unsigned TXPRF     :  2; // Transmit Pulse Repetition Frequency  -> e_radio_dw1000_pulse_repetition_frequency
        unsigned TXPREAMBLE:  4; // Combination of TXPSR and PE fields -> e_radio_dw1000_preamble_configuration
        unsigned TXBOFFS   : 10; // Transmit Buffer Offset of first byte to transmit
        unsigned IFSDELAY  :  8; // Inter Frame Spacing (unit is amount of preamble symbols)
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_t_registerx_fctrl;

typedef union { // register SYS_CTRL 0x0d:00 -> DW1000 User Manual p.78
    t_u32 Word;

    struct {
        unsigned SFCST     : 1; // 1: suppress auto transmission of Frame Checksum on next frame but take FCS from TX buffer
        unsigned TXSTRT    : 1; // 1: start transmission immediately if TXDLYS==0
        unsigned TXDLYS    : 1; // 1: delay transmissions according to DX_TIME value (set TXDLYS and TXSTRT together for every TX)
        unsigned CANSFCS   : 1; // 1: cancel suppression of auto Frame Checksum transmission for current frame for fast transmit cancellation
        unsigned reserved1 : 2;
        unsigned TRXOFF    : 1; // 1: switch off transmitter and return to IDLE state immediately
        unsigned WAIT4RESP : 1; // 1: switch on receiver after transmission to receive response frame (use TXDLYS to wakeup Receiver after exact delay)
        unsigned RXENAB    : 1; // 1: switch on receiver immediately and scan for preamble (configure Receiver before)
        unsigned RXDLYE    : 1; // 1: enable receiver enable delay according to DX_TIME value
        unsigned reserved2 : 14;
        unsigned HRBPT     : 1; // 1: host wants to read from receive buffer #1; 0: .. buffer #0 (double buffered receive only)
        unsigned reserved3 : 7;

    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_sys_ctrl;

typedef union { // Systen Event Mask Register 0x0e:00 -> DW1000 User Manual p.82
    t_u32 Word;

    struct {
        unsigned reserved1 : 1;
        unsigned MCPLOCK   : 1; // 1: enable event PLL lock
        unsigned MESSYNCR  : 1; // 1: enable event external sync clock reset
        unsigned MAAT      : 1; // 1: enable event Automatic Acknowledge Trigger
        unsigned MTXFRB    : 1; // 1: enable event Transmitter Frame Begins
        unsigned MTXPRS    : 1; // 1: enable event Transmitter Preamble Sent
        unsigned MTXPHS    : 1; // 1: enable event Transmitter MAC Header Sent
        unsigned MTXFRS    : 1; // 1: enable event Transmitter Frame Sent
        unsigned MRXPRD    : 1; // 1: enable event Receiver Preamble Detected
        unsigned MRXSFDD   : 1; // 1: enable event Receiver Start of Frame Delimiter Detected
        unsigned MLDEDONE  : 1; // 1: enable event Receiver Leading Edge Detection Done
        unsigned MRXPHD    : 1; // 1: enable event Receiver MAC Header Detected
        unsigned MRXPHE    : 1; // 1: enable event Receiver MAC Header Error (could not decode header)
        unsigned MRXDFR    : 1; // 1: enable event Receiver Data Frame Ready (frame can now be downloaded from RX buffer)
        unsigned MRXFCG    : 1; // 1: enable event Receiver Frame Checksum Good
        unsigned MRXFCE    : 1; // 1: enable event Receiver Frame Checksum Error
        unsigned MRXRFSL   : 1; // 1: enable event Receiver Reed Solomon Frame Sync Loss (could not decode frame data)
        unsigned MRXRFTO   : 1; // 1: enable event Receiver Frame Wait Timeout
        unsigned MLDEERR   : 1; // 1: enable event Receiver Leading Edge Detector Error
        unsigned reserved2 : 1;
        unsigned MRXOVRR   : 1; // 1: enable event Receiver Buffer Overrun
        unsigned MRXPTO    : 1; // 1: enable event Receiver Preamble Detection Error
        unsigned MGPIOIRQ  : 1; // 1: enable event Interrupt from GPIO-block
        unsigned MSLP2INIT : 1; // 1: enable event Device transitioned from SLEEP to INIT state
        unsigned MRFPLLLL  : 1; // 1: enable event Receiver PLL Lock Loosing
        unsigned MCPLLLL   : 1; // 1: enable event System Clock PLL Lock Loosing
        unsigned MRXSFDTO  : 1; // 1: enable event Receiver Start of Frame Detection Timeout
        unsigned MHPDWARN  : 1; // 1: enable event Half Period Delay Warning
        unsigned MTXBERR   : 1; // 1: enable event Transmitter Buffer Error
        unsigned MAFFREJ   : 1; // 1: enable event Automatic Frame Filtering Rejection
        unsigned reserved3 : 2;
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_sys_mask;

typedef union { // System Event Status Register 0x0f:00 -> DW1000 User Manual p.85
    t_u8 Bytes[5];
    t_u32 LowWord;  // low 32-bits are most interesting and faster to process than all 40 bits

    struct {
        // Note: Each status bit is cleared by writing a 1 to it

        // Bytes[0]
        unsigned IRQS      : 1; // 1: Interrupt Request has been triggered
        unsigned CPLOCK    : 1; // 1: digital clock PLL has locked (DW1000 is operating at full speed)
        unsigned ESYNCR    : 1; // 1: System Counter has been reset after receiving an external synchronization clock reset signal on SYNC pin (cleared by writing 1)
        unsigned AAT       : 1; // 1: frame filtering is enabled and a data or command frame has been received with FCF.Fields.ACK==1
        unsigned TXFRB     : 1; // 1: transmitter has begun to send preamble (automatically cleared at next transmitter enable)
        unsigned TXPRS     : 1; // 1: transmitter has completed sending preamble and is now sending Start of Frame Delimiter + MAX header
        unsigned TXPHS     : 1; // 1: transmitter has completed sending MAC header and is now sending payload data
        unsigned TXFRS     : 1; // 1: transmitter has completed sending payload data (transmission is done)

        // Bytes[1]
        unsigned RXPRD     : 1; // 1: receiver has detected and confirmed presence of a preamble sequence
        unsigned RXSFD     : 1; // 1: receiver has detected a Start of Frame Delimiter and is now decoding MAC header
        unsigned LDEDONE   : 1; // 1: Adjusted RX timestamp is now available in RX_STAMP register
        unsigned RXPHD     : 1; // 1: receiver has completed decoding of MAC header
        unsigned RXPHE     : 1; // 1: error occured while decoding MAC header
        unsigned RXDFR     : 1; // 1: data frame has been decoded successfully and is ready for download
        unsigned RXFCG     : 1; // 1: frame CRC checking was successfull
        unsigned RXFCE     : 1; // 1: Received frame has failed CRC check

        // Bytes[2]
        unsigned RXRFSL    : 1; // 1: Error occured during reed solomon decoding of frame => frame data is invalid
        unsigned RXRFTO    : 1; // 1: No valid frame was received within configured timeout since receiver was enabled
        unsigned LDEERR    : 1; // 1: Timeout occured while searching for first arriving ray
        unsigned reserved1 : 1; //
        unsigned RXOVRR    : 1; // 1: receive buffer was emptied too slowly (only possible with enabled double buffering)
        unsigned RXPTO     : 1; // 1: receiver could not complete preamble detection within timeout
        unsigned GPIOIRQ   : 1; // 1: interrupt condition has occured in GPIO block
        unsigned SLP2INIT  : 1; // 1: DW1000 has completed wakeup from sleep to init mode

        // Bytes[3]
        unsigned RFPLL     : 1; // 1: RF Clock PLL has locking issues (faulty hardware, power supply or wrong configuration)
        unsigned CLKPLL    : 1; // 1: System Clock PLL has locking issues (faulty hardware, power supply or wrong configuration)
        unsigned RXSFDTO   : 1; // 1: receiver could not decode a start frame after preamble within timeout
        unsigned HPDWARN   : 1; // 1: configured period until delayed TX/RX is too short: application should issue TRXOFF to avoid waiting 8 secs until TX/RX !!!
        unsigned TXBERR    : 1; // 1: data was sent to TX buffer too late (transmitter already consumed bytes from buffer)
        unsigned AFFREJ    : 1; // 1: automatic frame filter has rejected a frame (->Frame Filter Rejection Counter)
        unsigned HSRB      : 1; // index of receive buffer currently accessible from host (double buffering)
        unsigned ICRB      : 1; // index of receive buffer currently accessible from IC (double buffering)

        // Bytes[4]
        unsigned RXRSCS    : 1; // 1: reed solomon decoding has corrected at least one bit
        unsigned RXPREJ    : 1; // 1: receiver has seen a preamble but did not confirm it
        unsigned TXPUTE    : 1; // 1: transmitter has not egnough time to power up until next delayed TX
        unsigned reserved2 : 5; //

    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_sys_status;

typedef union { // -> DW1000 User Manual p.119
    t_u32 Word;

    struct {
        unsigned OSTSM     : 1; // External Transmit Synchronization Mode Enable
        unsigned OSRSM     : 1; // External Receive Synchronization Mode Enable
        unsigned PLLLDT    : 1; // Clock PLL Lock Detect Tune. Set to 1 to ensure reliable operation of the clock PLL lock detect flags
        unsigned WAIT      : 8; // Wait counter used for external transmit synchronisation and external timebase reset
        unsigned OSTRM     : 1; // External Time Base Reset Mode Enable
        unsigned reserved1 : 4;
        unsigned reserved2 : 16;
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_ec_ctrl;

typedef union { // register FIRSTPATH 0x15:05 -> DW1000 User Manual p.99
    t_u32 Word;

    struct {
        unsigned FP_INDEX_FRACTION :  6; // Fractional part of First Path Index (sub 1 nanosecond position of Leading Edge)
        unsigned FP_INDEX_INTEGER:   10; // Integer part of First Path Index (sub 1 nanosecond position of Leading Edge)
        t_u16    FP_AMPL1;               // First Path Amplitute Point 1
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_first_path;

typedef enum { // e_radio_dw1000_pmsc_state - Current PMSC State Machine value
    rdps_INIT,     // DW1000 is in INIT state
    rdps_IDLE,     // DW1000 is in IDLE state
    rdps_TX_WAIT,  // DW1000 is waiting for delayed transmit
    rdps_RX_WAIT,  // DW1000 is waiting for delayed receive
    rdps_TX,       // DW1000 is transmitting
    rdps_RX,       // DW1000 is receiving
    rdps_unknown   // unknown/ illegal values below
} e_radio_dw1000_pmsc_state;

typedef enum { // e_radio_dw1000_rx_state - Current Receive State Machine value
    rdrs_IDLE,           // receiver is idle
    rdrs_START_ANALOG,   //
    rdrs_RX_RDY,         //
    rdrs_PREAMBLE_FOUND, //
    rdrs_PRMBL_TIMEOUT,  //
    rdrs_SFD_FOUND,      //
    rdrs_CNFG_PHR_RX,    //
    rdrs_PHR_RX_STRT,    //
    rdrs_DATA_RATE_RDY,  //
    rdrs_DATA_RX_SEQ,    //
    rdrs_CNFG_DATA_RX,   //
    rdrs_PHR_NOT_OK,     //
    rdrs_LAST_SYMBOL,    //
    rdrs_WAIT_RSD_DONE,  //
    rdrs_RSD_OK,         //
    rdrs_RSD_NOT_OK,     //
    rdrs_RECONFIG_110,   //
    rdrs_WAIT_110_PHR,   //
    rdrs_unknown         // unknown/ illegal values below
} e_radio_dw1000_rx_state;

typedef enum { // e_radio_dw1000_rx_state - Current Receive State Machine value
    rdts_IDLE,      // transmitter is idle
    rdts_PREAMBLE,  // transmitting preamble
    rdts_SFD,       // transmitting SFD
    rdts_PHR,       // transmitting PHY Header
    rdts_SDE,       // transmitting PHY Header parity SECDED bits
    rdts_DATA,      // transmitting data block
    rdts_RSP_DATA,  // transmitting Reed-Solomon parity block
    rdts_TAIL,      // transmitting tail bits
    rdts_unknown    // unknown/ illegal values below
} e_radio_dw1000_tx_state;

typedef union { // register SYS_STATE 0x19:00 System State Information (not described in DW1000 User Manual)
    t_u32 Word;

    struct {
        e_radio_dw1000_tx_state   TX_STATE;   // Current Transmitter State Machine value -> e_radio_dw1000_tx_state
        e_radio_dw1000_rx_state   RX_STATE;   // Current Receiver State Machine value    -> e_radio_dw1000_rx_state
        e_radio_dw1000_pmsc_state PMSC_STATE; // Current PMSC State Machine value        -> e_radio_dw1000_pmsc_state
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_sys_state;

typedef enum { // -> DW1000 User Manual p.180
    rdci_auto        = 0b00, // system clock will run off the 19.2 MHz XTI clock until the PLL is calibrated and locked
    rdci_19_2MHz_XTI = 0b01, // Force system clock to be the 19.2 MHz XTI clock
    rdci_125MHz_PLL  = 0b10, // Force system clock to the 125 MHz PLL clock (DW1000 may lock up if 125 MHz PLL is not locked in!)
    rdci_none        = 0b11  // reserved

} e_radio_dw1000_clock_input;

typedef union { // register PMSC_CTRL0 0x36:00 -> DW1000 User Manual p.180
    t_u32 Word;

    struct {
        unsigned SYSCLKS   : 2; // system clock source      -> e_radio_dw1000_clock_input
        unsigned RXCLKS    : 2; // receiver clock source    -> e_radio_dw1000_clock_input
        unsigned TXCLKS    : 2; // transmitter clock source -> e_radio_dw1000_clock_input
        unsigned FACE      : 1; // Force Accumulator Clock Enable
        unsigned reserved1 : 2;
        unsigned OTP       : 1; //?
        unsigned ADCCE     : 1; // Analog-to-Digital Converter Clock Enable (IC-temperature and input battery voltage)
        unsigned reserved2 : 4;
        unsigned AMCE      : 1; // Accumulator Memory Clock Enable
        unsigned GPCE      : 1; // =1: enable GPIO CLocks
        unsigned GPRN      : 1; // =0: activate GPIO Reset
        unsigned GPDCE     : 1; // =1: enable GPIO interrupt input debounce filter
        unsigned GPDRN     : 1; // =0: activate GPIO debounce reset
        unsigned reserved3 : 3;
        unsigned KHZCLKEN  : 1; // =1: enable divider to provide kilohertz clock
        unsigned reserved4 : 4;
        unsigned SOFTRESET : 4; // reset sequence: set SOFTRESET to 0b0000, 0b1111 (one after another)
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_pmsc_ctrl0;

typedef enum { // valid values for u_radio_dw1000_register_pmsc_ctrl1.Fields.PKTSEQ -> DW1000 User Manual p.183
    rd_pmsc_disable  = 0x00, // disable PMSC control of analog RF subsystems
    rd_pmsc_enable   = 0xe7  // enable PMSC control of analog RF subsystems
} e_radio_dw1000_pmsc_control;

typedef union { // register PMSC_CTRL1 0x36:04 -> DW1000 User Manual p.183
    t_u32 Word;

    struct {
        unsigned reserved1 : 1;
        unsigned ARX2INIT  : 1; // 1: automatic transition from RX to INIT state (-> low duty-cycle SNIFF mode)
        unsigned PKTSEQ    : 8; // -> e_radio_dw1000_pmsc_control
        unsigned ATXSLP    : 1; // 1: automatically return to sleep after TX (requires valid AON configuration)
        unsigned ARXSLP    : 1; // 1: automatically return to sleep after RX (requires valid AON configuration)
        unsigned SNOZE     : 1; // 1: enable SNOOZE mode
        unsigned SNOZR     : 1; // 1: enable SNOOZE repeat mode (-> low-power listening)
        unsigned PLLSYN    : 1; // 1: enable special 1GHz clock form some external SYNC modes
        unsigned LDERUNE   : 1; // 1: enable Leading Edge Detection algorithm (required to update Receive Time Stamp register)
        unsigned KHZCLKDIV : 6; // top 6 bits of 10-bit kilohertz clock divider (input is 19.2MHz XTI clock)

    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_pmsc_ctrl1;

typedef union { // register PMSC_LEDC 0x36:04 -> DW1000 User Manual p.183
    t_u32 Word;

    struct {
        unsigned BLINK_TIM : 8; // LED blink time (unit 14ms)
        unsigned BLINKEN   : 1; // =1: enable LED blink feature
        unsigned BLINKNOW  : 4; // each bit enables blinking of corresponding LED output
        unsigned reserved1 : 12;

    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_pmsc_ledc;

typedef enum { // valid values for u_radio_dw1000_register_chan_ctrl.Fields.RXPRF -> DW1000 User Manual p.108
    rd_chan_ctrl_prf_16MHz = 0b01, //
    rd_chan_ctrl_prf_64MHz = 0b10, //
} e_radio_dw1000_chan_ctrl_rxprf;

typedef union { // register CHAN_CTRL 0x1f:00 -> DW1000 User Manual p.108
    t_u32 Word;

    struct {
        unsigned TX_CHAN   : 4; // Transmit Channel (valid values: 1,2,3,4,5,7)
        unsigned RX_CHAN   : 4; // Receive Channel (valid values: 1,2,3,4,5,7)
        unsigned reserved1 : 9;
        unsigned DWSFD     : 1; // 1: Enable non-standard Start of Frame Delimiter sequence
        unsigned RXPRF     : 2; // Receiver Pulse Repetition Frequency -> e_radio_dw1000_chan_ctrl_rxprf
        unsigned TNSSFD    : 1; // 1: Transmitter uses non-standard Start of Frame Delimiter as configured via USR_SFD[]
        unsigned RNSSFD    : 1; // 1: Receiver uses non-standard Start of Frame Delimiter as configured via USR_SFD[]
        unsigned TX_PCODE  : 5; // Transmitter Preamble Code
        unsigned RX_PCODE  : 5; // Receiver Preamble Code

    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_chan_ctrl;

typedef union { // register FS_XTALT 0x2b:0e -> DW1000 User Manual p.150
    t_u8 Byte;

    struct {
        unsigned XTALT    : 5;
        unsigned reserved : 3;  // must be set to 0b011 (according to User Manual)
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_fs_xtalt;

typedef union { // register RF_STATUS  0x28:2c PLL-Lock Status Register-> DW1000 User Manual p.143
    t_u32 Word;

    struct {
        unsigned CPLLLOCK  : 1; // 1: digital clock PLL is locked (set EC_CTRL.PLLDT to enable)
        unsigned CPLLLOW   : 1; // 1: PLL is running a little lower  than its target frequency
        unsigned CPLLHIGH  : 1; // 1: PLL is running a little higher than its target frequency
        unsigned RFPLLLOCK : 1; // 1: RF PLL is locked
        unsigned reserved1 : 4;
        t_u16    reserved2;
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_rf_status;

typedef union { // register LDE_CFG1 0x2e:0806 -> DW1000 User Manual p.167
    t_u32 Word;

    struct {
        unsigned NTM      : 5;  // Noise Threshold Multiplier
        unsigned PMULT    : 3;  // Peak Multiplier (default value = 3)
        t_u8     reserved1;     //
        t_u16    reserved2;     //
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_lde_cfg1;

typedef union { // register RX_FINFO 0x10:00 -> DW1000 User Manual p.92
    t_u32 Word;

    struct {
        unsigned RXFLEN   : 10; // Receive Frame Length copied from the PHR of the received frame (only 7 bits if 802.15.4 compliant)
        unsigned reserved1: 1;  //
        unsigned RXNSPL   : 2;  // Receive non-standard preamble length (works in combination with RXPSR)
        unsigned RXBR     : 2;  // Received Bit Rate -> e_radio_dw1000_datarates
        unsigned RNG      : 1;  // Ranging Bit from received PHY header
        unsigned RXPRF    : 2;  // -> e_radio_dw1000_chan_ctrl_rxprf
        unsigned RXPSR    : 2;  // Length of received preamble length (00 = 16, 01 = 64, 10 = 1024, 11 = 4096 symbols)
        unsigned RXPACC   : 12; // Number of accumulated preamble symbols
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_rx_finfo;

typedef enum { // valid values for u_radio_dw1000_register_otp_sf.Fields.OPS_SEL -> DW1000 User Manual p.165
    rd_ops_length64 = 0b00, // parameter set 0
    rd_ops_tight    = 0b01, // parameter set 1
} e_radio_dw1000_otp_parameter_set;

typedef union { // register OTP_CTRL 0x2d:06 -> DW1000 User Manual p.162
    t_u16 Word;

    struct {
        unsigned OTPRDEN  : 1;  // 1: forces the OTP into manual read mode (required before OTPREAD=1)
        unsigned OTPREAD  : 1;  // 1: commands a read operation from the address specified in the OTP_ADDR register,
        unsigned reserved1: 1;  //
        unsigned OTPMRWR  : 1;  // 1: reset current OTP mode         (bit must be manually reset afterwards!)
        unsigned reserved2: 2;  //
        unsigned OTPPROG  : 1;  // 1: start programming WDAT content (bit must be manually reset afterwards!)
        unsigned OTPMR    : 4;  // OTP Mode Register
        unsigned reserved3: 4;  //
        unsigned LDELOAD  : 1;  // 1: Force loading LDE microcode (required to use Leading Edge Detection)
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_otp_ctrl;

typedef union { // register OTP_STAT 0x2d:08 -> DW1000 User Manual p.163
    t_u16 Word;

    struct {
        unsigned OTPPRGD  : 1;  // 1: OTP Programming of 32 bit word from OTP_WDAT at address OTP_ADDR has completed
        unsigned OTPVPOK  : 1;  // 1: OTP Programming Voltage is OK
        unsigned reserved1: 6;  //
        t_u8     reserved2;
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_otp_stat;

typedef union { // register OTP_SF 0x2d:12 -> DW1000 User Manual p.164
    t_u8 Byte;

    struct {
        unsigned OPS_KICK : 1;  // 1: load operating parameter set according to OPS_SEL
        unsigned LDO_KICK : 1;  // 1: load LDOTUNE_CAL parameter into LDOTUNE register
        unsigned reserved1: 3;  //
        unsigned OPS_SEL  : 2;  // parameter set to load (->e_radio_dw1000_otp_parameter_set)
        unsigned reserved2: 1;  //
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_otp_sf;

typedef union { // register AGC_CTRL1; 0x23:0x02   Automatic Gain Control measurement -> DW1000 User Manual p.114
    t_u16 Word;

    struct {
        unsigned DIS_AM     : 1;  // 1: disables AGC measurement (set by default)
        unsigned reserved1  : 15;
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_agc_ctrl1;

typedef union { // register GPIO_MODE 0x26:00 GPIO Mode Control Register -> DW1000 UserManual v2.03 p.121
    t_u32 Word;
    struct {
        // reset value 0x00000000
        unsigned reserved1 : 6;
        unsigned MSGP0     : 2; // 00: GPIO0, 01: RXOKLED, 10: reserved, 11: reserved
        unsigned MSGP1     : 2; // 00: GPIO1, 01: SFDLED,  10: reserved, 11: reserved
        unsigned MSGP2     : 2; // 00: GPIO2, 01: RXLED,   10: reserved, 11: reserved
        unsigned MSGP3     : 2; // 00: GPIO3, 01: TXLED,   10: reserved, 11: reserved
        unsigned MSGP4     : 2; // 00: GPIO4, 01: EXTPA,   10: reserved, 11: reserved
        unsigned MSGP5     : 2; // 00: GPIO5, 01: EXTTXE,  10: reserved, 11: reserved
        unsigned MSGP6     : 2; // 00: GPIO6, 01: EXTRXE,  10: reserved, 11: reserved
        unsigned MSGP7     : 2; // 00: SYNC input, 01: GPIO7, 10: reserved, 11: reserved
        unsigned MSGP8     : 2; // 00: IRQ output, 01: GPIO8, 10: reserved, 11: reserved
        unsigned reserved2 : 8;
    } Fields;
} __attribute__( ( __packed__ ) ) u_radio_dw1000_register_gpio_mode;

typedef union { // register GPIO_DIR 0x26:08  GPIO Direction Control Register -> DW1000 UserManual v2.03 p.123
    t_u32 Word;
    struct {
        // reset value 0x00000000
        unsigned GPIO_DP0      : 1; // Direction selection: 1=input, 0=output
        unsigned GPIO_DP1      : 1; // Direction selection: 1=input, 0=output
        unsigned GPIO_DP2      : 1; // Direction selection: 1=input, 0=output
        unsigned GPIO_DP3      : 1; // Direction selection: 1=input, 0=output
        unsigned GPIO_DM0      : 1; // Direction mask: 1=change direction of corresponding GPIO
        unsigned GPIO_DM1      : 1; // Direction mask: 1=change direction of corresponding GPIO
        unsigned GPIO_DM2      : 1; // Direction mask: 1=change direction of corresponding GPIO
        unsigned GPIO_DM3      : 1; // Direction mask: 1=change direction of corresponding GPIO
        unsigned GPIO_DP4      : 1; // Direction selection: 1=input, 0=output
        unsigned GPIO_DP5      : 1; // Direction selection: 1=input, 0=output
        unsigned GPIO_DP6      : 1; // Direction selection: 1=input, 0=output
        unsigned GPIO_DP7      : 1; // Direction selection: 1=input, 0=output
        unsigned GPIO_DM4      : 1; // Direction mask: 1=change direction of corresponding GPIO
        unsigned GPIO_DM5      : 1; // Direction mask: 1=change direction of corresponding GPIO
        unsigned GPIO_DM6      : 1; // Direction mask: 1=change direction of corresponding GPIO
        unsigned GPIO_DM7      : 1; // Direction mask: 1=change direction of corresponding GPIO
        unsigned GPIO_DP8      : 1; // Direction selection: 1=input, 0=output
        unsigned reserved1     : 3;
        unsigned GPIO_DM8      : 1; // Direction mask: 1=change direction of corresponding GPIO
        unsigned reserved2     : 11;
    } Fields;
} __attribute__( ( __packed__ ) ) u_radio_dw1000_register_gpio_dir;

typedef union { // register GPIO_DOUT 0x26:0c  GPIO Data Output register -> DW1000 User Manual p.125
    t_u32 Word;
    struct {
        // reset value 0x00000000
        unsigned GPIO_OP0      : 1; // Output State Setting. Value is used if GPIO_OM0==1
        unsigned GPIO_OP1      : 1; // Output State Setting. Value is used if GPIO_OM1==1
        unsigned GPIO_OP2      : 1; // Output State Setting. Value is used if GPIO_OM2==1
        unsigned GPIO_OP3      : 1; // Output State Setting. Value is used if GPIO_OM3==1
        unsigned GPIO_OM0      : 1; // 1: Forward Value of GPIO_OP0 to output pin
        unsigned GPIO_OM1      : 1; // 1: Forward Value of GPIO_OP1 to output pin
        unsigned GPIO_OM2      : 1; // 1: Forward Value of GPIO_OP2 to output pin
        unsigned GPIO_OM3      : 1; // 1: Forward Value of GPIO_OP3 to output pin
        unsigned GPIO_OP4      : 1; // Output State Setting. Value is used if GPIO_OM4==1
        unsigned GPIO_OP5      : 1; // Output State Setting. Value is used if GPIO_OM5==1
        unsigned GPIO_OP6      : 1; // Output State Setting. Value is used if GPIO_OM6==1
        unsigned GPIO_OP7      : 1; // Output State Setting. Value is used if GPIO_OM7==1
        unsigned GPIO_OM4      : 1; // 1: Forward Value of GPIO_OP4 to output pin
        unsigned GPIO_OM5      : 1; // 1: Forward Value of GPIO_OP5 to output pin
        unsigned GPIO_OM6      : 1; // 1: Forward Value of GPIO_OP6 to output pin
        unsigned GPIO_OM7      : 1; // 1: Forward Value of GPIO_OP7 to output pin
        unsigned GPIO_OP8      : 1; // Output State Setting. Value is used if GPIO_OM8==1
        unsigned reserved1     : 3;
        unsigned GPIO_OM8      : 1; // 1: Forward Value of GPIO_OP8 to output pin
        unsigned reserved2     : 11;
    } Fields;
} __attribute__( ( __packed__ ) ) u_radio_dw1000_register_gpio_dout;

typedef union { // register GPIO_IRQE 0x26:10  GPIO Interrupt Enable -> DW1000 User Manual p.
    t_u32 Word;

    struct {
        unsigned GPIO_IRQE0 : 1; // 1: enable this GPIO input as an interrupt source
        unsigned GPIO_IRQE1 : 1; // 1: enable this GPIO input as an interrupt source
        unsigned GPIO_IRQE2 : 1; // 1: enable this GPIO input as an interrupt source
        unsigned GPIO_IRQE3 : 1; // 1: enable this GPIO input as an interrupt source
        unsigned GPIO_IRQE4 : 1; // 1: enable this GPIO input as an interrupt source
        unsigned GPIO_IRQE5 : 1; // 1: enable this GPIO input as an interrupt source
        unsigned GPIO_IRQE6 : 1; // 1: enable this GPIO input as an interrupt source
        unsigned GPIO_IRQE7 : 1; // 1: enable this GPIO input as an interrupt source
        unsigned GPIO_IRQE8 : 1; // 1: enable this GPIO input as an interrupt source

        unsigned reserved1  : 7;
        t_u16    reserved2;

    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_gpio_irqe;

typedef union { // register GPIO_ISEN 0x26:14  GPIO Interrupt Sense Selection -> DW1000 User Manual p.
    t_u32 Word;

    struct {
        unsigned GPIO_ISEN0 : 1; // 1: if configured as IRQ input, this GPIO pin will generate an interrupt on falling-edge/low-level (0: rising-edge/high-level)
        unsigned GPIO_ISEN1 : 1; // 1: if configured as IRQ input, this GPIO pin will generate an interrupt on falling-edge/low-level (0: rising-edge/high-level)
        unsigned GPIO_ISEN2 : 1; // 1: if configured as IRQ input, this GPIO pin will generate an interrupt on falling-edge/low-level (0: rising-edge/high-level)
        unsigned GPIO_ISEN3 : 1; // 1: if configured as IRQ input, this GPIO pin will generate an interrupt on falling-edge/low-level (0: rising-edge/high-level)
        unsigned GPIO_ISEN4 : 1; // 1: if configured as IRQ input, this GPIO pin will generate an interrupt on falling-edge/low-level (0: rising-edge/high-level)
        unsigned GPIO_ISEN5 : 1; // 1: if configured as IRQ input, this GPIO pin will generate an interrupt on falling-edge/low-level (0: rising-edge/high-level)
        unsigned GPIO_ISEN6 : 1; // 1: if configured as IRQ input, this GPIO pin will generate an interrupt on falling-edge/low-level (0: rising-edge/high-level)
        unsigned GPIO_ISEN7 : 1; // 1: if configured as IRQ input, this GPIO pin will generate an interrupt on falling-edge/low-level (0: rising-edge/high-level)
        unsigned GPIO_ISEN8 : 1; // 1: if configured as IRQ input, this GPIO pin will generate an interrupt on falling-edge/low-level (0: rising-edge/high-level)

        unsigned reserved1  : 7;
        t_u16    reserved2;
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_gpio_isen;

typedef union { // register GPIO_IMODE 0x26:18  GPIO Interrupt Mode (Level / Edge) -> DW1000 User Manual p.
    t_u32 Word;

    struct {
        unsigned GPIO_IMODE0 : 1; // 1: Configure interrupt mode as edge-sensitive (0: level-sensitive)
        unsigned GPIO_IMODE1 : 1; // 1: Configure interrupt mode as edge-sensitive (0: level-sensitive)
        unsigned GPIO_IMODE2 : 1; // 1: Configure interrupt mode as edge-sensitive (0: level-sensitive)
        unsigned GPIO_IMODE3 : 1; // 1: Configure interrupt mode as edge-sensitive (0: level-sensitive)
        unsigned GPIO_IMODE4 : 1; // 1: Configure interrupt mode as edge-sensitive (0: level-sensitive)
        unsigned GPIO_IMODE5 : 1; // 1: Configure interrupt mode as edge-sensitive (0: level-sensitive)
        unsigned GPIO_IMODE6 : 1; // 1: Configure interrupt mode as edge-sensitive (0: level-sensitive)
        unsigned GPIO_IMODE7 : 1; // 1: Configure interrupt mode as edge-sensitive (0: level-sensitive)
        unsigned GPIO_IMODE8 : 1; // 1: Configure interrupt mode as edge-sensitive (0: level-sensitive)

        unsigned reserved1  : 7;
        t_u16    reserved2;
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_gpio_imode;

typedef union { // register GPIO_IBES 0x26:1c  GPIO Interrupt “Both Edge” Select -> DW1000 User Manual p.
    t_u32 Word;

    struct {
        unsigned GPIO_IBES0 : 1; // 1: trigger interrupt on rising- and falling-edge (0: use edge configured by GPIO_IMODE0)
        unsigned GPIO_IBES1 : 1; // 1: trigger interrupt on rising- and falling-edge (0: use edge configured by GPIO_IMODE1)
        unsigned GPIO_IBES2 : 1; // 1: trigger interrupt on rising- and falling-edge (0: use edge configured by GPIO_IMODE2)
        unsigned GPIO_IBES3 : 1; // 1: trigger interrupt on rising- and falling-edge (0: use edge configured by GPIO_IMODE3)
        unsigned GPIO_IBES4 : 1; // 1: trigger interrupt on rising- and falling-edge (0: use edge configured by GPIO_IMODE4)
        unsigned GPIO_IBES5 : 1; // 1: trigger interrupt on rising- and falling-edge (0: use edge configured by GPIO_IMODE5)
        unsigned GPIO_IBES6 : 1; // 1: trigger interrupt on rising- and falling-edge (0: use edge configured by GPIO_IMODE6)
        unsigned GPIO_IBES7 : 1; // 1: trigger interrupt on rising- and falling-edge (0: use edge configured by GPIO_IMODE7)
        unsigned GPIO_IBES8 : 1; // 1: trigger interrupt on rising- and falling-edge (0: use edge configured by GPIO_IMODE8)

        unsigned reserved1  : 7;
        t_u16    reserved2;
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_gpio_ibes;

typedef union { // register GPIO_ICLR 0x26:20  GPIO Interrupt Latch Clear -> DW1000 User Manual p.
    t_u32 Word;

    struct {
        unsigned GPIO_ICLR0 : 1; // 1: clear interrupt latch for this GPIO pin (0: no effect)
        unsigned GPIO_ICLR1 : 1; // 1: clear interrupt latch for this GPIO pin (0: no effect)
        unsigned GPIO_ICLR2 : 1; // 1: clear interrupt latch for this GPIO pin (0: no effect)
        unsigned GPIO_ICLR3 : 1; // 1: clear interrupt latch for this GPIO pin (0: no effect)
        unsigned GPIO_ICLR4 : 1; // 1: clear interrupt latch for this GPIO pin (0: no effect)
        unsigned GPIO_ICLR5 : 1; // 1: clear interrupt latch for this GPIO pin (0: no effect)
        unsigned GPIO_ICLR6 : 1; // 1: clear interrupt latch for this GPIO pin (0: no effect)
        unsigned GPIO_ICLR7 : 1; // 1: clear interrupt latch for this GPIO pin (0: no effect)
        unsigned GPIO_ICLR8 : 1; // 1: clear interrupt latch for this GPIO pin (0: no effect)

        unsigned reserved1  : 7;
        t_u16    reserved2;
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_gpio_iclr;

typedef union { // register GPIO_IDBE 0x26:24  GPIO Interrupt De-bounce Enable -> DW1000 User Manual p.
    t_u32 Word;

    struct {
        unsigned GPIO_IDBE0 : 1; // 1: enable debouncing circuit for incoming interrupt requests on this GPIO pin
        unsigned GPIO_IDBE1 : 1; // 1: enable debouncing circuit for incoming interrupt requests on this GPIO pin
        unsigned GPIO_IDBE2 : 1; // 1: enable debouncing circuit for incoming interrupt requests on this GPIO pin
        unsigned GPIO_IDBE3 : 1; // 1: enable debouncing circuit for incoming interrupt requests on this GPIO pin
        unsigned GPIO_IDBE4 : 1; // 1: enable debouncing circuit for incoming interrupt requests on this GPIO pin
        unsigned GPIO_IDBE5 : 1; // 1: enable debouncing circuit for incoming interrupt requests on this GPIO pin
        unsigned GPIO_IDBE6 : 1; // 1: enable debouncing circuit for incoming interrupt requests on this GPIO pin
        unsigned GPIO_IDBE7 : 1; // 1: enable debouncing circuit for incoming interrupt requests on this GPIO pin
        unsigned GPIO_IDBE8 : 1; // 1: enable debouncing circuit for incoming interrupt requests on this GPIO pin

        unsigned reserved1  : 7;
        t_u16    reserved2;
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_gpio_idbe;

typedef union { // register GPIO_RAW 0x26:28  GPIO raw state -> DW1000 User Manual p.
    t_u32 Word;

    struct {
        unsigned GPIO_RAWP0 : 1; // current state of this GPIO pin
        unsigned GPIO_RAWP1 : 1; // current state of this GPIO pin
        unsigned GPIO_RAWP2 : 1; // current state of this GPIO pin
        unsigned GPIO_RAWP3 : 1; // current state of this GPIO pin
        unsigned GPIO_RAWP4 : 1; // current state of this GPIO pin
        unsigned GPIO_RAWP5 : 1; // current state of this GPIO pin
        unsigned GPIO_RAWP6 : 1; // current state of this GPIO pin
        unsigned GPIO_RAWP7 : 1; // current state of this GPIO pin
        unsigned GPIO_RAWP8 : 1; // current state of this GPIO pin

        unsigned reserved1  : 7;
        t_u16    reserved2;
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_gpio_raw;

typedef union { // register AON_WCFG 0x2c:00 Always On Wake-up Configuration register -> DW1000 User Manual p.151
    t_u16 Word;

    struct {
        unsigned ONW_RADC   : 1; // 1:  automatic initiation of temperature and input battery sampling at system wakeup (required to measure ambient temperature before IC heats up)
        unsigned ONW_RX     : 1; // 1:  On Wake-up turn on the Receiver
        unsigned reserved1  : 1;
        unsigned ONW_LEUI   : 1; // 1: On Wake-up load the EUI from OTP memory
        unsigned reserved2  : 2;
        unsigned ONW_LDC    : 1; // 1: On Wake-upload configurations from the AON memory into the host interface register set
        unsigned ONW_L64P   : 1; // 1: On Wake-up load the Length64 receiver operating parameter set
        unsigned PRES_SLEEP : 1; // 1: Allow DW1000 to automatically return to sleep after a failed reception
        unsigned reserved3  : 2;
        unsigned ONW_LLDE   : 1; // 1: On Wake-up load the LDE microcode.
        unsigned ONW_LLDO   : 1; // 1: On Wake-up load the LDOTUNE value from OTP
        unsigned reserved4  : 3;
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_aon_wcfg;

typedef union { // register AON_CTRL 0x2c:02 Always-On Control Register -> DW1000 User Manual p.153
    t_u8 Byte;
    struct {
        unsigned RESTORE   : 1; // 1: copy user configurations from AON memory to host interface register (bit is auto reset afterwards)
        unsigned SAVE      : 1; // 1: copy user configurations from host interface register to AON memory (bit is auto reset afterwards)
        unsigned UPL_CFG   : 1; // 1: copy AON_CFG0 + AON_CFG1 to AON memory
        unsigned DCA_READ  : 1; // 1: command direct read from AON_ADDR address into AON_RDAT register
        unsigned reserved1 : 3;
        unsigned DCA_ENAB  : 1; // 1: enable DCA_READ operations
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_aon_ctrl;

typedef union { // register AON_CFG0; 0x2c:0x06   Always On Configuration Register 0 -> DW1000 User Manual p.157
    t_u32 Word;

    struct {
        unsigned SLEEP_EN  : 1; // 1: start entering sleep state (upload configuration to AON afterwards to enter sleep mode)
        unsigned WAKE_PIN  : 1; // 1: enable wakeup from WakeUp pin
        unsigned WAKE_SPI  : 1; // 1: enable wakeup from SPI interface
        unsigned WAKE_CNT  : 1; // 1: enable wakeup from sleep counter
        unsigned LPDIV_EN  : 1; // 1: enable Low power divider as alternative clock source for sleep time counter (7..13kHz)
        unsigned LPCLKDIVA : 11; // sleep timer clock = raw DW1000 XTAL oscillator frequency / LPCLKDIVA
        t_u16 SLEEP_TIM;  // sleep time
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_aon_cfg0;

typedef union { // register AON_CFG1; 0x2c:0x0a   Always On Configuration Register 1 -> DW1000 User Manual p.159
    t_u16 Word;

    struct {
        unsigned SLEEP_C_EN  : 1;  // 1: enable sleep counter
        unsigned SMXX       : 1;  // set to zero while entering sleep mode for correct operation
        unsigned LPOSC_CAL  : 1;  // 1: enable calibration function to measure period of internal low powered oscillator
        unsigned reserved1  : 13; //
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_aon_cfg1;

typedef union { // register RX_FQUAL; 0x12:0x00   Received Frame Quality Information -> DW1000 User Manual p.95
    t_u8 Bytes[8];

    struct {
        t_u16    STD_NOISE;       // Standard Deviation of Noise as seen by LDE algorithm
        t_u16    FP_AMPL2;        // First Path Amplitude Point 2. (Part of magnitude of leading edge signal)
        t_u16    FP_AMPL3;        // First Path Amplitude Point 3. (Part of magnitude of leading edge signal)
        t_u16    CIR_PWR;         // Channel Impulse Response Power (Sum of squares of magnitudes of accumulator from estimated highest power portion of channel)
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_rx_fqual;

typedef union { // register RX_TTCKO; 0x14:0x00   Receiver Time Tracking Offset -> DW1000 User Manual p.97
    t_u8 Bytes[5];

    struct {
        unsigned  RXTOFS    : 19;  // 19 bit signed RX time tracking offset. => Clock Offset = RXTTCKI / RXTOFS
        unsigned  reserved1 :  5;  // should be written as zero
        unsigned  RSMPDEL   :  8;  // internal re-sampler delay value (only used during DW1000 development)
        unsigned  RCPHASE   :  7;  // Receive carrier phase adjustment at ranging timestamp creation (may be used as diagnose)
        unsigned  reserved2 :  1;  // should be written as zero
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_rx_ttcko;

typedef struct { // raw meta data for a single received frame (copies of DW1000 registers at time of receival)
    u_radio_dw1000_register_rx_finfo    RX_FINFO;    // 10:00   RX Frame Information         (updated after frame received)
    u_radio_dw1000_register_rx_fqual    RX_FQUAL;    // 12:00   Rx Frame Quality Information (updated after frame received)
    t_u32                               RX_TTCKI;    // 13:00   Receiver Time Tracking Interval
    u_radio_dw1000_register_first_path  FIRST_PATH;  // 15:05   First Path Data of received frame
    u_radio_dw1000_register_rx_ttcko    RX_TTCKO;    // 14:00   Receiver Time Tracking Offset

    union {
        t_u8 Byte;
        struct {
            unsigned Valid_RX_FQUAL : 1; // ==1: field RX_FQUAL stores a valid value
        } Bits;
    } Flags;
} __attribute__( ( packed ) ) t_radio_dw1000_frame_meta;

typedef union { // register ACK_RESP_T; 0x1a:0x00 Acknowledgement Time and Response Time -> DW1000 User Manual p.101
    t_u32 Word;

    struct {
        unsigned W4R_TIM    : 20; // Wait-for-Response turn-around time (time between TX complete and RX enable
        unsigned reserved1  : 4;
        unsigned ACK_TIM    : 8;  // Auto-Acknowledgement turn-around time (time between correct data-frame receipt and transmission of ACK)
    } __attribute__( ( packed ) ) Fields;
} radio_dw1000_register_ack_resp_t_u;

typedef union { // register RF_CONF_T; 0x28:0x00 RF Configuration Register (manual test modes) -> DW1000 user manual p.140
    t_u32 Word;

    struct {
        unsigned reserved1  : 8;
        unsigned TXFEN      : 5;  // =0xf: force enable all TX blocks
        unsigned PLLFEN     : 3;  // =0xa: force enable CLK_PLL and RF_PLL
        unsigned LDOFEN     : 5;  // =0x1f: force enable all LDOs
        unsigned TXRXSW     : 2;  // =0x2: force enable TX; =0x1: force enable RX
        unsigned reserved2  : 9;
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_rf_conf;

typedef union { // register RX_SNIFF; 0x1d:0x00 Sniff Mode Configuration -> DW1000 User Manual p.102
    t_u16 Word;

    struct {
        unsigned SNIFF_ONT   : 4;  // SNIFF-mode on-time (units of PAC)
        unsigned reserved1   : 4;
        unsigned SNIFF_OFFT  : 8;  // SNIFF-mode off-time (units of PAC)
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_rx_sniff;

typedef union { // register AGC_STAT1; 0x23:0x1e AGC Status Register -> DW1000 User Manual p.117
    t_u8 Bytes[3];

    struct {
        unsigned reserved1 : 6;
        unsigned EDG1      : 5;  // gain value related to input noise measurement
        unsigned EDV2      : 9;  // NoiseEnergyLevel = (EV2 - 40) * 10^EDG1 * SF
        // SF = (Channel < 5) ? 1.3335 : 1.0000;
        unsigned reserved2 : 4;
    } __attribute__( ( packed ) ) Fields;
} u_radio_dw1000_register_agc_stat1;

typedef struct { // set of all supported 8-/16-/32- and 40-bit registers
    u_radio_dw1000_register_sys_cfg     SYS_CFG;     // 04:00   System Configuration
    u_ttc_packetimestamp_40           SYS_TIME;    // 06:00   System Time Counter
    u_radio_dw1000_t_registerx_fctrl    TX_FCTRL;    // 08:00   Transmit Frame Control
    u_ttc_packetimestamp_40           DX_TIME;     // 0a:00   Delayed Send or Receive Time
    u_radio_dw1000_register_sys_ctrl    SYS_CTRL;    // 0d:00   System Control
    u_radio_dw1000_register_sys_mask    SYS_MASK;    // 0e:00   System Event Mask
    u_radio_dw1000_register_sys_status  SYS_STATUS;  // 0f:00   System Event Status
    /* The RX Meta Data registers are only read during radio_dw1000_isr_receive() run and are stored in a special ringbuffer.
     * They have been removed from Config->LowLevelConfig.Registers to save RAM.

       u_radio_dw1000_register_rx_finfo    RX_FINFO;    // 10:00   RX Frame Information         (updated after frame received)
       u_radio_dw1000_register_rx_fqual    RX_FQUAL;    // 12:00   Rx Frame Quality Information (updated after frame received)
       t_u32                               RX_TTCKI;    // 13:00   Receiver Time Tracking Interval
       u_radio_dw1000_register_rx_ttcko    RX_TTCKO;    // 14:00   Receiver Time Tracking Offset
       u_ttc_packetimestamp_40   RX_STAMP;    // 15:00   Adjusted Receive Time of first ray (Leading Edge)
       u_radio_dw1000_register_first_path  FIRST_PATH;  // 15:05   First Path Data of received frame
       */
    u_ttc_packetimestamp_40           TX_STAMP;    // 17:00   Adjusted Time of Transmission (unit of LSB is 15.65 picoseconds)
    u_ttc_packetimestamp_40           TX_RAWST;    // 17:05   Raw Timestamp of Transmission (9 LSBs ignored => precision 125MHz)
    u_radio_dw1000_register_sys_state   SYS_STATE;   // 19:00   System State Information
    u_radio_dw1000_register_chan_ctrl   CHAN_CTRL;   // 1f:00   Channel Control
    //t_u8                                AGC_CTRL[32];// 23:00   Automatic Gain Control configuration
    u_radio_dw1000_register_agc_ctrl1   AGC_CTRL1;   // 23:02   AGC Control #1
    t_u16                               AGC_TUNE1;   // 23:04   AGC Tuning register 1
    t_u32                               AGC_TUNE2;   // 23:0c   AGC Tuning register 2
    t_u16                               AGC_TUNE3;   // 23:12   AGC Tuning register 3
    u_radio_dw1000_register_agc_stat1   AGC_STAT1;   // 23:1e   AGC Status
    u_radio_dw1000_register_ec_ctrl     EC_CTRL;     // 24:00   External Clock Synchronization Counter Configuration
    //t_u8                                GPIO_CTRL[44];//26:00   GPIO control and status
    u_radio_dw1000_register_gpio_mode   GPIO_MODE;   // 26:00   GPIO Mode Control Register
    u_radio_dw1000_register_gpio_dir    GPIO_DIR;    // 26:08   GPIO Direction Control Register
    u_radio_dw1000_register_gpio_dout   GPIO_DOUT;   // 26:0c   GPIO Data Output register
    u_radio_dw1000_register_gpio_irqe   GPIO_IRQE;   // 26:10   GPIO Interrupt Enable
    u_radio_dw1000_register_gpio_isen   GPIO_ISEN;   // 26:14   GPIO Interrupt Sense Selection
    u_radio_dw1000_register_gpio_imode  GPIO_IMODE;  // 26:18   GPIO Interrupt Mode (Level / Edge)
    u_radio_dw1000_register_gpio_ibes   GPIO_IBES;   // 26:1c   GPIO Interrupt “Both Edge” Select
    u_radio_dw1000_register_gpio_iclr   GPIO_ICLR;   // 26:20   GPIO Interrupt Latch Clear
    u_radio_dw1000_register_gpio_idbe   GPIO_IDBE;   // 26:24   GPIO Interrupt De-bounce Enable
    u_radio_dw1000_register_gpio_raw    GPIO_RAW;    // 26:28   GPIO raw state

    u_radio_dw1000_register_rf_status   RF_STATUS;   // 28:2C   RF Status Register
    t_u32                               FS_PLLCFG;   // 2b:07   Frequency synthesiser – PLL configuration
    t_u8                                FS_PLLTUNE;  // 2b:0b   Frequency synthesiser – PLL Tuning
    u_radio_dw1000_register_fs_xtalt    FS_XTALT;    // 2b:0e   Frequency synthesiser – Crystal trim
    //t_u8                                AON[12];     // 2c:00   Always-On register set
    u_radio_dw1000_register_aon_wcfg    AON_WCFG;    // 2c:00   Always-On Wakeup Configuration Register
    u_radio_dw1000_register_aon_ctrl    AON_CTRL;    // 2c:02   Always-On Control Register
    //t_u8                                AON_RDAT;    // 2c:03   Always-On Read Data
    //t_u8                                AON_ADDR;    // 2c:04   Always-On Direct Access Address
    u_radio_dw1000_register_aon_cfg0    AON_CFG0;    // 2c:06   Always-On Configuration Register 0
    u_radio_dw1000_register_aon_cfg1    AON_CFG1;    // 2c:0a   Always-On Configuration Register 1
    /* OTP-Registers have special interface. It makes no sense to cache all of them like normal registers
     * OTP-Access is handled by _radio_dw1000_read_otp() as

    t_u8                                OTP_IF[18];  // 2d:00   One Time Programmable Memory Interface
    t_u32                               OTP_WDAT;    // 2d:00   OTP Write Data
    t_u16                               OTP_ADDR;    // 2d:04   OTP Address
    u_radio_dw1000_register_otp_stat    OTP_STAT;    // 2d:08   OTP Status
    t_u32                               OTP_RDAT;    // 2D:0A   OTP Read Data
    t_u32                               OTP_SRDAT;   // 2d:0E   OTP SR Read Data
    */
    u_radio_dw1000_register_otp_ctrl    OTP_CTRL;    // 2D:06   OTP Control
    u_radio_dw1000_register_otp_sf      OTP_SF;      // 2d:12   OTP Special Function
    t_u16                               LDE_THRESH;  // 2e:0000 LDE Threshold report
    u_radio_dw1000_register_lde_cfg1    LDE_CFG1;    // 2e:0806 LDE Configuration Register 1
    t_u16                               LDE_PPINDX;  // 2e:1000 LDE Peak Path Index
    t_u16                               LDE_PPAMPL;  // 2e:1002 LDE Peak Path Amplitude
    t_u16                               LDE_RXANTD;  // 2e:1804 LDE Receive Antenna Delay configuration
    t_u16                               LDE_CFG2;    // 2e:1806 LDE Configuration Register 2
    t_u16                               LDE_REPC;    // 2e:2804 LDE Replica Coefficient configuration
    u_radio_dw1000_register_pmsc_ctrl0  PMSC_CTRL0;  // 36:00   Power Management and System Control 0
    u_radio_dw1000_register_pmsc_ctrl1  PMSC_CTRL1;  // 36:04   Power Management and System Control 1
    u_radio_dw1000_register_pmsc_ledc   PMSC_LEDC;   // 36:28   Power Management and System Control LED Configuration

    t_u32                               DEV_ID;      // 00:00   Device Identifier – includes device type and revision info
    //t_u8                                EUI64[8];    // 01:00   Extended Unique Identifier              (stored in Config->LocalID.Physical)
    //t_u16                               SHORT_ADDR;  // 03:00   16-bit local identifier                 (stored in Config->LocalID.Logical)
    //t_u16                               PAN_ID;      // 03:02   16-bit Personal Area Network Identifier (stored in Config->LocalID.PanID)
    t_u16                               RX_FWTO;     // 0c:00   System Event Mask Register
    t_u16                               TX_ANTD;     // 18:00   Delay from Transmit to Antenna
    //t_u8                                SYS_STATE[5];// 19:00   System State Information
    radio_dw1000_register_ack_resp_t_u  ACK_RESP_T;  // 1a:00   Acknowledgement Time and Response Time
    u_radio_dw1000_register_rx_sniff    RX_SNIFF;    // 1d:00   Pulsed Preamble Reception Configuration (Sniff-Mode)
    t_u32                               TX_POWER;    // 1e:00   TX Power Control
    //t_u8                                USR_SFD[41]; // 21:00   User-specified short/long TX/RX SFD sequences
    t_u16                               DRX_TUNE0b;  // 27:02   Digital Tuning Register 0b
    t_u16                               DRX_TUNE1a;  // 27:04   Digital Tuning Register 1a
    t_u16                               DRX_TUNE1b;  // 27:06   Digital Tuning Register 1b
    t_u32                               DRX_TUNE2;   // 27:08   Digital Tuning Register 2
    t_u16                               DRX_SFDTOC;  // 27:20   SFD timeout
    t_u16                               DRX_PRETOC;  // 27:24   Preamble detection timeout
    t_u16                               DRX_TUNE4H;  // 27:26   Digital Tuning Register 4H
    t_u32                               RF_TXCTRL;   // 28:0C   Analog TX Control Register
    //t_u8                                TX_CAL[52];  // 2a:00   Transmitter calibration block
    t_u8                                TC_PGDELAY;  // 2a:0b   Transmitter Pulse Generator Delay (-> DW1000 UserManual p.147)
    //t_u8                                FS_CTRL[21]; // 2b:00   Frequency synthesiser control block

    //t_u8                                DIG_DIAG[41];// 2f:00   Digital Diagnostics Interface
    t_u8                                PMSC_SNOZT;  // 36:0c   PMSC Snooze Time Register
    t_u16                               PMSC_TXFSEQ; // 36:26   PMSC fine grain TX sequencing control

    // additional (no read-/write- macro defined yet)
    u_radio_dw1000_register_rf_conf     RF_CONF;     // 28:00   RF Configuration Register
    //t_u8                                RF_RES1[7];  // 28:04   Reserved area 1
    t_u8                                RF_RXCTRLH;  // 28:0B   Analog RX Control Register
    //t_u16                               RF_RES2;     // 28:10   Reserved area 2
    //t_u8                                LDOTUNE[5];  // 28:30   LDO voltage tuning

} __attribute__( ( __packed__ ) ) t_radio_dw1000_registers;

//} register descriptions according to DW1000 User Manual ----------------------------------------------

typedef struct {
    t_u8    PGdly;
    //TX POWER
    //31:24     BOOST_0.125ms_PWR
    //23:16     BOOST_0.25ms_PWR-TX_SHR_PWR
    //15:8      BOOST_0.5ms_PWR-TX_PHR_PWR
    //7:0       DEFAULT_PWR-TX_DATA_PWR
    t_u32   Power[RADIO_DW1000_SIZE_POWER];
} __attribute__( ( __packed__ ) ) t_radio_dw1000_transmission_config;

typedef union { // which parts should be loaded from One Time Programmable (OTP) memory by radio_dw1000_init_radio()
    t_u8 Byte;

    struct {
        unsigned LDO_TUNE     : 1;  // Voltage Regulator Tuning data
        unsigned AntennaDelay : 1;  // Calibrated Antenna Delay
        unsigned XtalTrim     : 1;  // Crystal Fine Tuning
        unsigned TX_Config    : 1;  // Transmitter Configuration
        unsigned LDE_Config   : 1;  // Leading Edge Detection Code (required to use LDE!)
        unsigned reserved     : 1;  //
        unsigned IsValid      : 1;  // set to 1 to enable loading from OTP!
    } __attribute__( ( packed ) ) Bits;
} u_radio_dw1000_load_from_otp;

typedef struct { // transmit power settings of one channel
    t_u32 PRF16; // power setting for Pulse Repetition Frequency 16MHz
    t_u32 PRF64; // power setting for Pulse Repetition Frequency 64MHz
} t_radio_dw1000_transmit_power;

typedef struct { // data loaded from (OTP) memory by radio_dw1000_init_radio()
    t_radio_dw1000_transmit_power TransmitPower[6]; // transmit power configuration for all six valid channels (1,2,3,4,5,7)

    t_u32 PartID;             // unique hardware identification number loaded from One Time Programming memory
    t_u32 LotID;              // identification number loaded from One Time Programming memory
    t_u16 AntennaDelay_PRF16; // Antenna Delay for 16MHz Pulse Repetion Frequency (PRF)
    t_u16 AntennaDelay_PRF64; // Antenna Delay for 64MHz Pulse Repetion Frequency (PRF)
    t_u8  XtalTrim;           // Crystal Fine Tuning
} t_radio_dw1000_otp_data;

typedef enum { // available SPI bus speed settings (DW1000 SPI bus has different speed limits according to it's current configuration)
    radio_dw1000_spi_speed_NotSet = 0, // speed not yet configured
    radio_dw1000_spi_speed_Min,        // use minimum speed
    radio_dw1000_spi_speed_Max,        // use maximum speed
    radio_dw1000_spi_speed_unknown     // do not use
} e_radio_dw1000_spi_speed;

typedef enum { // available clocking schemes for DW1000 device (-> _radio_dw1000_switch_clocking() )
    rdcs_force_sysclk_xti  = 0,  // system clock from XTI (19,2 MHz)
    rdcs_enable_all_seq    = 1,  // all clocks will automatically switch to 125 MHz if possible (preferred mode)
    rdcs_force_sysclk_pll  = 2,  // system clock from PLL (125 MHz) Note: DW1000 may lock up if 125MHz PLL is not locked yet!)
    rdcs_read_acc_on       = 7,  // Receiver Clock from PLL (125 MHz)
    rdcs_read_acc_off      = 8,
    rdcs_force_otp_on      = 11,
    rdcs_force_otp_off     = 12,
    rdcs_force_tx_pll      = 13,
    rdcs_unknown
} e_radio_dw1000_clocking_scheme;

typedef enum { // e_radio_dw1000_status_isr - Status level allows to monitor correctness of interrupt service routine
    rdsi_none,
    rdsi_software_interrupt,   // set before user space raises an interrupt request by software (e.g. _radio_dw1000_isr_raise() )
    rdsi_isr_started,          // interrupt service routine has started (overwritten by later actions inside isr)
    rdsi_transmit_locked,      // interrupt service routine could not send a packet from ListTX, because list mutex was locked
    rdsi_transmit_empty,       // interrupt service routine could not send a packet from ListTX, list was empty
    rdsi_transmit_started,     // interrupt service routine has has started transmission of latest packet
    rdsi_transmit_running,     // radio reports transmission of latest packet is ongoing
    rdsi_transmit_complete,    // radio tx has completed transmission of latest packet
    rdsi_transmit_not_started, // radio tx did not succeed for some reason (radio did not process packet)
    rdsi_transmit_too_late,    // delayed radio tx was aborted because preparation time is too short (this normally means to abort a transaction and wait for a second chance with increased delay time)
    rdsi_receive_preamble,     // radio rx has found a preamble
    rdsi_receive_frame,        // radio rx has received and successfully decoded a complete data frame
    rdsi_receive_ok,           // radio rx has successfully received a complete packet
    rdsi_pll_locked,           // transceiver PLL has locked in (after enabling PLL)
    rdsi_pll_error,            // system clock error (maybe hardware fault)
    rdsi_error_unknown,        // radio shows error condition

    rdsi_unknown
} e_radio_dw1000_status_isr;

typedef struct { // t_radio_dw1000_changed - change status of local register copies (-> _radio_dw1000_write_registers_changed() )
    unsigned SYS_CFG     : 1; // 04:00   System Configuration
    unsigned SYS_TIME    : 1; // 06:00   System Time Counter
    unsigned TX_FCTRL    : 1; // 08:00   Transmit Frame Control
    unsigned DX_TIME     : 1; // 0a:00   Delayed Send or Receive Time
    unsigned SYS_CTRL    : 1; // 0d:00   System Control
    unsigned SYS_MASK    : 1; // 0e:00   System Event Mask
    unsigned SYS_STATUS  : 1; // 0f:00   System Event Status
    /* These Registers are only read during radio_dw1000_isr_receive() run and are stored in a special ringbuffer.
     * They have been removed from Config->LowLevelConfig.Registers to save RAM.
    unsigned RX_FINFO    : 1; // 10:00   RX Frame Information         (updated after frame received)
    unsigned RX_FQUAL    : 1; // 12:00   Rx Frame Quality Information (updated after frame received)
    unsigned RX_TTCKI    : 1; // 13:00   Receiver Time Tracking Interval
    unsigned RX_TTCKO    : 1; // 14:00   Receiver Time Tracking Offset
    unsigned RX_STAMP    : 1; // 15:00   Adjusted Receive Time of first ray (Leading Edge)
    unsigned FIRST_PATH  : 1; // 15:05   First Path Data of received frame
    */
    unsigned TX_STAMP    : 1; // 17:00   Adjusted Time of Transmission (unit of LSB is 15.65 picoseconds)
    unsigned TX_RAWST    : 1; // 17:05   Raw Timestamp of Transmission (9 LSBs ignored => precision 125MHz)
    unsigned CHAN_CTRL   : 1; // 1f:00   Channel Control
    unsigned AGC_CTRL1   : 1; // 23:02   AGC Control #1
    unsigned AGC_TUNE1   : 1; // 23:04   AGC Tuning register 1
    unsigned AGC_TUNE2   : 1; // 23:0c   AGC Tuning register 2
    unsigned AGC_TUNE3   : 1; // 23:12   AGC Tuning register 3
    unsigned EC_CTRL     : 1; // 24:00   External Clock Synchronization Counter Configuration
    unsigned GPIO_MODE   : 1; // 26:00   GPIO Mode Control Register
    unsigned GPIO_DIR    : 1; // 26:08   GPIO Direction Control Register
    unsigned GPIO_DOUT   : 1; // 26:0c   GPIO Data Output register
    unsigned GPIO_IRQE   : 1; // 26:10   GPIO Interrupt Enable
    unsigned GPIO_ISEN   : 1; // 26:14   GPIO Interrupt Sense Selection
    unsigned GPIO_IMODE  : 1; // 26:18   GPIO Interrupt Mode (Level / Edge)
    unsigned GPIO_IBES   : 1; // 26:1c   GPIO Interrupt “Both Edge” Select
    unsigned GPIO_ICLR   : 1; // 26:20   GPIO Interrupt Latch Clear
    unsigned GPIO_IDBE   : 1; // 26:24   GPIO Interrupt De-bounce Enable
    unsigned GPIO_RAW    : 1; // 26:28   GPIO raw state
    //unsigned RF_STATUS   : 1; // 28:2C   RF Status Register (read only)
    unsigned FS_PLLCFG   : 1; // 2b:07   Frequency synthesiser – PLL configuration
    unsigned FS_PLLTUNE  : 1; // 2b:0b   Frequency synthesiser – PLL Tuning
    unsigned FS_XTALT    : 1; // 2b:0e   Frequency synthesiser – Crystal trim
    unsigned AON_WCFG    : 1; // 2c:00   Always-On Wakeup Configuration Register
    unsigned AON_CTRL    : 1; // 2c:02   Always-On Control Register
    unsigned AON_RDAT    : 1; // 2c:03   Always-On Read Data
    unsigned AON_ADDR    : 1; // 2c:04   Always-On Direct Access Address
    unsigned AON_CFG0    : 1; // 2c:06   Always-On Configuration Register 0
    unsigned AON_CFG1    : 1; // 2c:0a   Always-On Configuration Register 1
    /* OTP Registers require a special access protocol. It makes no sense to create local copies.

    unsigned OTP_WDAT    : 1; // 2d:00   OTP Write Data
    unsigned OTP_ADDR    : 1; // 2d:04   OTP Address
    unsigned OTP_CTRL    : 1; // 2D:06   OTP Control
    unsigned OTP_STAT    : 1; // 2d:08   OTP Status
    unsigned OTP_RDAT    : 1; // 2D:0A   OTP Read Data
    unsigned OTP_SRDAT   : 1; // 2d:0E   OTP SR Read Data
    unsigned OTP_SF      : 1; // 2d:12   OTP Special Function
    */
    unsigned LDE_THRESH  : 1; // 2e:0000 LDE Threshold report
    unsigned LDE_CFG1    : 1; // 2e:0806 LDE Configuration Register 1
    unsigned LDE_PPINDX  : 1; // 2e:1000 LDE Peak Path Index
    unsigned LDE_PPAMPL  : 1; // 2e:1002 LDE Peak Path Amplitude
    unsigned LDE_RXANTD  : 1; // 2e:1804 LDE Receive Antenna Delay configuration
    unsigned LDE_CFG2    : 1; // 2e:1806 LDE Configuration Register 2
    unsigned LDE_REPC    : 1; // 2e:2804 LDE Replica Coefficient configuration
    unsigned PMSC_CTRL0  : 1; // 36:00   Power Management and System Control 0
    unsigned PMSC_CTRL1  : 1; // 36:04   Power Management and System Control 1
    unsigned PMSC_LEDC   : 1; // 36:28   Power Management and System Control LED Configuration
    unsigned PMSC_SNOZT  : 1; // 36:0c   PMSC Snooze Time Register
    unsigned PMSC_TXFSEQ : 1; // 36:26   PMSC fine grain TX sequencing control
    unsigned RX_FWTO     : 1; // 0c:00   System Event Mask Register
    unsigned TX_ANTD     : 1; // 18:00   Delay from Transmit to Antenna
    unsigned ACK_RESP_T  : 1; // 1a:00   Acknowledgement Time and Response Time
    unsigned RX_SNIFF    : 1; // 1d:00   Pulsed Preamble Reception Configuration
    unsigned TX_POWER    : 1; // 1e:00   TX Power Control
    unsigned DRX_TUNE0b  : 1; // 27:02   Digital Tuning Register 0b
    unsigned DRX_TUNE1a  : 1; // 27:04   Digital Tuning Register 1a
    unsigned DRX_TUNE1b  : 1; // 27:06   Digital Tuning Register 1b
    unsigned DRX_TUNE2   : 1; // 27:08   Digital Tuning Register 2
    unsigned DRX_SFDTOC  : 1; // 27:20   SFD timeout
    unsigned DRX_PRETOC  : 1; // 27:24   Preamble detection timeout
    unsigned DRX_TUNE4H  : 1; // 27:26   Digital Tuning Register 4H
    unsigned RF_CONF     : 1; // 28:00   RF Configuration Register
    unsigned RF_RXCTRLH  : 1; // 28:0b   Analog RX Control Register
    unsigned RF_TXCTRL   : 1; // 28:0C   Analog TX Control Register
    unsigned TC_PGDELAY  : 1; // 2a:0b   Transmitter Pulse Generator Delay (-> DW1000 UserManual p.147)
} t_radio_dw1000_changed;

typedef struct s_radio_dw1000_config {  // dw1000 specific configuration data of single RADIO device

    t_radio_dw1000_otp_data            OTP_Data;              // read-only data loaded from OTP memory of DW1000
    t_radio_dw1000_registers           Registers;             // local copies of registers being read by radio_dw1000_register_*() macros
    t_radio_dw1000_changed             Changed;               // setting a bit here will cause _radio_dw1000_write_registers_changed() to write corresponding value into DW1000 register
    t_u8                               LogicalIndex_SPI;      // logical index of SPI device to use to communicate with DW1000
    t_u8                               LogicalIndex_SPI_NSS;  // logical index of nSS pin on SPI device to use for DW1000
    t_u8                               TuneReceiverBitRate;   // != 0: radio_dw1000_maintenance() will tune receiver for this bitrate  (set from interrupt service routine)

    e_ttc_gpio_pin                     Pin_SPI_NSS;           // GPIO pin to use as SlaveSelect
    e_ttc_gpio_pin                     Pin_RESET;             // GPIO pin connected to Reset (if any)
    e_ttc_gpio_pin                     Pin_IRQ;               // GPIO pin connected to Interrupt output of DW1000 (if any)
    e_ttc_gpio_pin                     Pin_SPIPOL;            // GPIO pin connected to SPIPOL input (SPI bus polarity) (if any)
    e_ttc_gpio_pin                     Pin_SPIPHA;            // GPIO pin connected to SPIPHA input (SPI bus phase) (if any)

    e_radio_dw1000_datarates           DataRate;              // data rate to use
    u_radio_dw1000_load_from_otp       LoadFromOTP;           // parts of radio configuration to load from One Time Programmable memory
    e_radio_dw1000_spi_speed           SpeedSPI;              // During WakeUp, DW1000 only supports minimum SPI speed (->_radio_dw1000_configure_spi())
    e_radio_dw1000_clocking_scheme     ClockingScheme;        // latest clocking scheme of DW1000 being set via _radio_dw1000_switch_clocking()

    t_u32                              DelayAdd_RX2TX;        // value to add to RX_STAMP for delayed transmit (calculated from Config->Delay_RX2TX)
    t_u32                              DelayAdd_TX2RX;        // value to add to TX-Time for delayed receive   (calculated from Config->Delay_TX2RX)

    t_u16                              Amount_EmptyISRs;      // amount of interrupt requests from DW1000 with empty SYS_STATUS (no flags set)
    t_u16                              Amount_PendingIRQs;    // amount of interrupt requests from DW1000 seen inside radio_dw1000_maintenance()
    t_u16                              Amount_RX_Restart;     // amount of times that the receiver was restarted because it was found idling unexpectedly

    e_radio_dw1000_status_isr          StatusISR;             // used to monitor correctnes of interrupt service routine
} __attribute__( ( __packed__ ) ) t_radio_dw1000_config;


// define types required by ttc_radio_types.h
#define t_ttc_radio_architecture     t_radio_dw1000_config
#define ttc_radio_virtual_channel_e  e_radio_dw1000_virtual_channel


/* DEPRECATED

typedef struct {
    t_u8 Channel;                           //!< channel number {1, 2, 3, 4, 5, 7 }
    t_u8 PulseRepetitionFrequency;          //!< Pulse Repetition Frequency {DWT_PRF_16M or DWT_PRF_64M}
    t_u8 TxPreambleLength;                  //!< DWT_PLEN_64..DWT_PLEN_4096
    t_u8 RxPAC ;                            //!< Acquisition Chunk Size (Relates to RX preamble length)
    t_u8 TxPreambleCode ;                   //!< TX preamble code
    t_u8 RxPreambleCode ;                   //!< RX preamble code
    t_u8 StarOfFrameDelimiter_Standard;     //!< Boolean should we use non-standard SFD for better performance
    e_radio_dw1000_datarates DataRate;      //!< Data Rate {DWT_BR_110K, DWT_BR_850K or DWT_BR_6M8}
    t_u8 PHR_Mode;                          //!< PHR mode {0x0 - standard DWT_PHRMODE_STD, 0x3 - extended frames DWT_PHRMODE_EXT}
    t_u8 SmartPower;                        //!< Smart Power enable / disable
    t_u16 TimeOut_StartOfFrame ;            //!< SFD timeout value (in symbols)
} __attribute__( ( packed ) )  t_radio_dw1000_channel;

typedef struct {
    t_u32 DeviceID ;
    t_u32 PartID ;
    t_u32 LotID ;
    t_u8  Channel;            // added chan here - used in the reading of acc
    t_u8  LongFrames ;        // flag in non-standard long frame mode
    t_u32 TxFCTRL ;           // keep TX_FCTRL register config
    t_u16 RF_RxDly;           // rf delay (delay though the RF blocks before the signal comes out of the antenna i.e. "antenna delay")
    t_u16 RF_TxDly;           // rf delay (delay though the RF blocks before the signal comes out of the antenna i.e. "antenna delay")
    t_u32 AntennaDly;         // antenna delay read from OTP 64 PRF value is in high 16 bits and 16M PRF in low 16 bits
    t_u8  Xtrim;              // xtrim value read from OTP
    t_u8  DoubleBuffer;       // t_u32 rx buffer mode flag
    t_u32 SysCFGreg ;         // local copy of system config register
    t_u32 TxPowCfg[RADIO_DW1000_SIZE_TX_POW_CFG];       // stores the Tx power configuration read from OTP (6 channels consecutively with PRF16 then 64, e.g. Ch 1 PRF16 is index 0 and 64 index 1)
    t_u32 States[RADIO_DW1000_SIZE_STATES] ;         // MP workaround debug states register
    t_u8  Statescount ;
    //t_u8  Wait4Resp ;         //wait 4 response was set with last TX start command
    t_u8  Pulse_Repetition_Freqency_Index ;
    t_u32 LDO_Tune ;            //low 32 bits of LDO tune value

} __attribute__( ( __packed__ ) ) radio_dw1000_local_parameters ;

typedef struct {
    t_u16 maxNoise ;            // LDE max value of noise
    t_u16 firstPathAmp1 ;       // Amplitude at floor(index FP) + 1
    t_u16 stdNoise ;            // Standard deviation of noise
    t_u16 firstPathAmp2 ;       // Amplitude at floor(index FP) + 2
    t_u16 firstPathAmp3 ;       // Amplitude at floor(index FP) + 3
    t_u16 maxGrowthCIR ;        // Channel Impulse Response max growth CIR
    t_u16 rxPreamCount ;        // Count of preamble symbols accumulated
    //t_u32        debug1;
    //t_u32        debug2;
    t_u32 firstPath ;           // First path index
} __attribute__( ( __packed__ ) ) radio_dw1000_Rx_Diagnosis_t ;

*/

//} Structures/ Enums


#endif //RADIO_DW1000_TYPES_H
