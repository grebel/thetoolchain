#ifndef radio_common_h
#define radio_common_h

/** { radio_common.h *********************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.h revision 11 at 20150928 08:58:42 UTC
 *
 *  Common implementation to be used by high- and low-level radio drivers.
 *
 *  Authors: Gregor Rebel
 *
}*/
/** { Includes *********************************************************
 *
 * Put all includes here that are required to include this header file.
 * Do NOT add includes required to compile radio_common.c here!
 */

#include "../ttc_basic.h"        // basic datatypes
#include "../ttc_memory.h"       // memory checks and safe pointers
#include "../ttc_packet_types.h" // network packet datatypes
#include "../ttc_radio_types.h"  // radio datatypes
#include "../ttc_math_types.h"   // vectors and special numbers

//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Constant Defines / Macros ****************************************
 */

// increase a statistic counter by one until maximum positive value is reached
#if (TTC_RADIO_STATISTICS == 1)
    #define RADIO_COMMON_COUNT(COUNTER) if (Config->Statistics.COUNTER != -1) { Config->Statistics.COUNTER++; }
#else
    #define RADIO_COMMON_COUNT(COUNTER)
#endif

/* Datatype to use for X,Y,Z coordinates in localization replies and reports
 *
 * Note: All participating nodes must be compiled to use the same datatype!
 */
#ifndef TTC_RADIO_COORDINATE_FORMAT
    #define TTC_RADIO_COORDINATE_FORMAT     float          // use default datatype
    #define TTC_RADIO_COORDINATE_UNDEFINED  TTC_MATH_CONST_NAN // maximum positive value of a signed 32 bit data type
#else
    #ifndef TTC_RADIO_COORDINATE_UNDEFINED
        #    error Missing definition for TTC_RADIO_COORDINATE_UNDEFINED. Define it as a constant to use for non initialized coordinates!
    #endif
#endif


//}ConstantDefines
/** { Structure Declarations *******************************************
 *
 * This can be typedef struct or typedef enum declarations.
 * Always prefix structures and enums with filename to avoid double declarations!
 */


/** Ranging messages
 *
 * radio_common implements different architecture independent ranging algorithms for Ultra Wide Bande
 * IEEE 802.15.4a comliant radios (e.g. the DecaWave DW1000).
 *
 * Basic message exchange of implemented algorithms.
 * c = speed of light (approx. 3e08 m/s)
 *
 * 1) Single Sided Time Of Flight (SS-TOF-RNG) ranging
 *    This is the most simple ranging measure from node M to another node A.
 *    This ranging method does not compensate clock frequency offset (CFO) between the nodes.
 *
 *    Node M                                       Node A
 *    @TS_M,1 --- rcrt_Request_Ranging_SSTOFCR ----> @TS_A,1
 *                                                 Delay_A,1
 *    @TS_M,2 <-- rcrt_Reply_Ranging_SSTOFCR ------- @TS_A,2
 *    Distance_M-A = (TS_M,2 - TS_M,1 - Delay_A,1) / 2 * c;
 *
 * 2) Single Sided Time Of Flight (SS-TOF-RNG 4x) ranging with n=4 replies
 *    This is the most simple localization method. Node M sends one ranging
 *    request as broadcast message and waits for n ranging replies.
 *    This ranging method does not compensate clock frequency offset (CFO) between the nodes.
 *    The application has to implement the localization protocol on its own.
 *
 *    a) M sends ranging request as broadcast
 *       Node M                                       Node A1     Node A2     Node A3     Node A4
 *       @TS_M,1 --- rcrt_Request_Ranging_SSTOFCR ----> @TS_A1,1
 *                                              ----------------> @TS_A2,1
 *                                              ----------------------------> @TS_A3,1
 *                                              ----------------------------------------> @TS_A4,1
 *                                                    Delay_A1,1  Delay_A2,1  Delay_A3,1  Delay_A4,1
 *
 *    b) M receives 4 ranging reply messages
 *       @TS_M,2 <-- rcrt_Reply_Ranging_SSTOFCR ------- @TS_A1,2
 *       @TS_M,3 <-- rcrt_Reply_Ranging_SSTOFCR ------------------- @TS_A2,2
 *       @TS_M,4 <-- rcrt_Reply_Ranging_SSTOFCR ------------------------------- @TS_A2,2
 *       @TS_M,5 <-- rcrt_Reply_Ranging_SSTOFCR ------------------------------------------- @TS_A2,2
 *
 *    c) Calculate distances from Node M to Nodes A1, ..., A4
 *       Distance_M-A1 = (TS_M,2 - TS_M,1 - Delay_A1,1) / 2 * c;
 *       Distance_M-A2 = (TS_M,2 - TS_M,1 - Delay_A2,1) / 2 * c;
 *       Distance_M-A3 = (TS_M,2 - TS_M,1 - Delay_A3,1) / 2 * c;
 *       Distance_M-A4 = (TS_M,2 - TS_M,1 - Delay_A4,1) / 2 * c;
 *
 *
 * 3) Single Sided Time Of Flight (SS-TOF-LOC 4x) localization with n=4 replies
 *    This protocol works similar to SS-TOF-RNG 4x. It's localization messages additionally carry
 *    world coordinates (X,Y,Z) to provide egnough data to calculate the world coordinate of the
 *    localization initiator.
 *
 *    a) M sends localization request as broadcast
 *       Node M                                            Node A1     Node A2     Node A3     Node A4
 *       @TS_M,1 --- rcrt_Request_Localization_SSTOFCR ----> @TS_A1,1
 *                                                   ----------------> @TS_A2,1
 *                                                   ----------------------------> @TS_A3,1
 *                                                   ----------------------------------------> @TS_A4,1
 *                                                         Delay_A1,1  Delay_A2,1  Delay_A3,1  Delay_A4,1
 *
 *    b) M receives 4 localization reply messages
 *       @TS_M,2 <-- rcrt_Reply_Localization_SSTOFCR ------- @TS_A1,2
 *       @TS_M,3 <-- rcrt_Reply_Localization_SSTOFCR ------------------- @TS_A2,2
 *       @TS_M,4 <-- rcrt_Reply_Localization_SSTOFCR ------------------------------- @TS_A2,2
 *       @TS_M,5 <-- rcrt_Reply_Localization_SSTOFCR ------------------------------------------- @TS_A2,2
 *
 *    c) Calculate distances from Node M to Nodes A1, ..., A4
 *       Distance_M-A1 = (TS_M,2 - TS_M,1 - Delay_A1,1) / 2 * c;
 *       Distance_M-A2 = (TS_M,2 - TS_M,1 - Delay_A2,1) / 2 * c;
 *       Distance_M-A3 = (TS_M,2 - TS_M,1 - Delay_A3,1) / 2 * c;
 *       Distance_M-A4 = (TS_M,2 - TS_M,1 - Delay_A4,1) / 2 * c;
 *
 *    d) Calculate world coordinates of local node (if egnough coordinates are available)
 *
 *    e) Send out localization report as broadcast/ unicast (optional)
 *       @TS_M,1 --- rcrt_Request_Localization_SSTOFCR ----> @TS_A1,1
 *                                                   ----------------> @TS_A2,1
 *                                                   ----------------------------> @TS_A3,1
 *                                                   ----------------------------------------> @TS_A4,1
 *                                                         Delay_A1,1  Delay_A2,1  Delay_A3,1  Delay_A4,1
 */
typedef enum { // e_radio_common_ranging_type  - magic keys used to identify ranging messages
    rcrt_None = 0,
    rcrt_Request_None = 0x10,           // BOOL IsRequestMessage = (Type > rcrt_Request_None) && (Type < rcrt_Requests_End);
    rcrt_Request_Ranging_SSTOF,         /* Ranging request for Single Sided Time Of Flight ranging to a single remote node.
                                         * This type of request is the most simple one. The request message must be sent directly
                                         * to a single remote node. Only one answer is expected.
                                         * struct(Packet): t_ttc_radio_packet_request_ranging_sstof
                                         */
    rcrt_Request_Ranging_SSTOFCR,       /* ranging request for Single Sided Time Of Flight ranging with Cascading Replies
                                         * This type of request makes only sense if sent as broadcast to all anchor nodes in the vicinity.
                                         * The message carries a numeric value that states how many reply messages are desired.
                                         * Depending on the anchor node implementation, more replies may be sent.
                                         * struct(Packet): t_ttc_radio_packet_request_ranging_sstofcr
                                         */
    rcrt_Request_Localization_SSTOFCR,  /* ranging request for Single Sided Time Of Flight ranging with Cascading Replies
                                         * This type of request must be sent as broadcast to multiple anchor nodes.
                                         * Multiple remote nodes will reply with t_ttc_radio_packet_reply_localization_sstofcr.
                                         * struct(Packet): t_ttc_radio_packet_request_localization_sstofcr
                                         */
    // add additional requests here...
    rcrt_Requests_End,                  // all higher values are no ranging requests

    rcrt_Reply_None = 0x20,             // BOOL IsReplyMessage = (Type > rcrt_Reply_None) && (Type < rcrt_Reply_End);
    rcrt_Reply_Ranging_SSTOF,           /* reply message to a SS-TOF ranging request
                                         * Only a single node will reply to a SS-ToF request.
                                         * struct(Packet): t_ttc_radio_packet_reply_ranging_sstof
                                         */
    rcrt_Reply_Ranging_SSTOFCR,         /* reply message to a SS-TOF Cascading Replies Ranging request
                                         * Multiple node will reply to a single SSToF-CR request, one after another.
                                         * struct(Packet): t_ttc_radio_packet_reply_ranging_sstof
                                         */
    rcrt_Reply_Localization_SSTOFCR,    // reply message to a SS-TOF Cascading Replies Localization request (t_ttc_radio_packet_reply_localization_sstofcr)
    // add additional replies here...
    rcrt_Reply_End,                     // all higher values are no ranging replies

    rcrt_Report_None = 0x30,            // BOOL IsReportMessage = (Type > rcrt_Report_None) && (Type < rcrt_Report_End);
    rcrt_Report_Ranging_SSTOF,          /* report correction data to compensate clock induced error to improve ranging
                                         * There is no difference in packet format between reporting for a SSTOF or a SSTOFCR ranging.
                                         * A report for SSTOF is send as unicast while a report for SSTOFCR is send as broadcast.
                                         * struct(Packet): t_ttc_radio_packet_report_ranging_sstof
                                         */
    rcrt_Report_Localization_SSTOFCR,   /* report correction data to compensate clock induced error to improve localization
                                         * This type of report can only be sent via broadcast to provide multiple anchor nodes with correction data
                                         * struct(Packet): t_ttc_radio_packet_report_localization_sstofcr
                                         */
    // add additional reports here...
    rcrt_Report_End,                    // all higher values are no report messages

    //ToDo: rcrt_Report_TRADSTOF_Ranging,  // magic-key identifying message of type t_ttc_radio_packet_report_ranging_sstof (Traffic Reduced Asymmetric Double Sided Time Of Flight ranging)
    rcrt_unknown
} e_radio_common_ranging_type;

typedef struct { // common header of all ranging request or reply packets
    e_radio_common_ranging_type  Type;      // magic-key identifying type of ranging payload
    t_u16                        RangingNo; // every ranging request gets a new number. The remote node must return this number. (ToDo: Check if t_u8 is egnough!)
} __attribute__( ( packed ) ) t_ttc_radio_packet_ranging;

typedef struct { // sent by any node as broadcast or directed packet to initiate a ranging measure
    t_ttc_radio_packet_ranging Header;
    t_u8                       Payload[0];                // extra payload (begins directly after this struct)
} __attribute__( ( packed ) ) t_ttc_radio_packet_request_ranging_sstof;

typedef struct { // sent by any node as broadcast or directed packet to initiate a ranging measure with multiple cascaded replies (SSToF-CR)
    t_ttc_radio_packet_ranging Header;
    t_u8                       AmountRequestedAnswers;    // amount of answers being expected by sender
    t_u8                       Payload[0];                // extra payload (begins directly after this struct)
} __attribute__( ( packed ) ) t_ttc_radio_packet_request_ranging_sstofcr;

typedef struct { // sent by any node as broadcast or directed packet to initiate a localization process
    t_ttc_radio_packet_ranging Header;
    t_u8                       AmountRequestedAnswers;    // amount of answers being expected by sender
    t_u8                       Payload[0];                // extra payload (begins directly after this struct)
} __attribute__( ( packed ) ) t_ttc_radio_packet_request_localization_sstofcr;

typedef struct { // basic header of all types of ranging request replies
    t_ttc_radio_packet_ranging Header;
    t_u32                      ResponseDelay_ClockCycles; // response delay added by remote node before sending back ranging packet (clock cycles)
    t_u32                      ClockPeriod_fs;            // duration of one clock cycle on sending node (femtoseconds)
} __attribute__( ( packed ) ) t_ttc_radio_packet_reply_ranging;

typedef struct { // sent back after receiving SSToF or SSToF-CR ranging request
    t_ttc_radio_packet_reply_ranging  Ranging;                   // general header common to all types of ranging replies
    t_u8                              Payload[0];                // extra payload (begins directly after this struct)
} __attribute__( ( packed ) ) t_ttc_radio_packet_reply_ranging_sstof;

typedef struct { // sent back after receiving localization request
    t_ttc_radio_packet_reply_ranging  Ranging;                  // general header common to all types of ranging replies

    // Note: Fields above must be the same (and in same order) as t_ttc_radio_packet_reply_ranging_sstof!
    // Extra fields for localization below:
    TTC_RADIO_COORDINATE_FORMAT X_mm;                     // world X-coordinate of replying node (==TTC_RADIO_COORDINATE_UNDEFINED: undefined)
    TTC_RADIO_COORDINATE_FORMAT Y_mm;                     // world Y-coordinate of replying node (==TTC_RADIO_COORDINATE_UNDEFINED: undefined)
    TTC_RADIO_COORDINATE_FORMAT Z_mm;                     // world Z-coordinate of replying node (==TTC_RADIO_COORDINATE_UNDEFINED: undefined)
    t_u8                        Payload[0];               // extra payload (begins directly after this struct)
} __attribute__( ( packed ) ) t_ttc_radio_packet_reply_localization_sstofcr;

typedef struct { // sent by ranging initiator after successfully receiving reply to a ranging request to provide remote node with data to compensate clock induced error (CIE).
    t_ttc_radio_packet_ranging Header;
    t_u32                      ReportDelay_ClockCycles;   // delay time between ranging-request and -report packet
    t_u32                      ClockPeriod_fs;            // duration of one clock cycle on sending node (femtoseconds)
    t_u8                       Payload[0];                // extra payload (begins directly after this struct)
} __attribute__( ( packed ) ) t_ttc_radio_packet_report_ranging_sstof;

typedef struct { // sent by localization initiator after successfully receiving reply to a ranging request to provide remote nodes with data to compensate clock induced error (CIE).
    t_ttc_radio_packet_ranging Header;
    t_u32                             ReportDelay_ClockCycles;  // delay time between ranging-request and -report packet
    t_u32                             ClockPeriod_fs;           // duration of one clock cycle on sending node (femtoseconds)
    // Note: Fields above must be the same (and in same order) as t_ttc_radio_packet_report_ranging_sstof !

    // Extra fields for localization below:
    TTC_RADIO_COORDINATE_FORMAT X_mm;                     // world X-coordinate of replying node (==TTC_RADIO_COORDINATE_UNDEFINED: undefined)
    TTC_RADIO_COORDINATE_FORMAT Y_mm;                     // world Y-coordinate of replying node (==TTC_RADIO_COORDINATE_UNDEFINED: undefined)
    TTC_RADIO_COORDINATE_FORMAT Z_mm;                     // world Z-coordinate of replying node (==TTC_RADIO_COORDINATE_UNDEFINED: undefined)
    t_u8                        Payload[0];               // extra payload (begins directly after this struct)
} __attribute__( ( packed ) ) t_ttc_radio_packet_report_localization_sstofcr;

typedef union {
    e_radio_common_ranging_type                      Type;                         // magic-key identifying type of ranging message
    t_ttc_radio_packet_ranging                       Ranging;                      // common header of all ranging messages
    t_ttc_radio_packet_request_ranging_sstof         Request_Ranging_SSTOF;        // basic ranging request
    t_ttc_radio_packet_request_ranging_sstofcr       Request_Ranging_SSTOFCR;      // ranging request to multiple remote nodes
    t_ttc_radio_packet_reply_ranging_sstof           Reply_Ranging_SSTOF;          // basic ranging reply
    t_ttc_radio_packet_report_ranging_sstof          ReportRanging_SSTOF;          // not yet used
    t_ttc_radio_packet_request_localization_sstofcr  Request_Localization_SSTOFCR; // basic localization request using cartesian coordinates
    t_ttc_radio_packet_reply_localization_sstofcr    Reply_Localization_SSTOFCR;   // basic localization reply   using cartesian coordinates
    t_ttc_radio_packet_report_localization_sstofcr   Report_Localization_SSTOFCR;  // basic localization report  using cartesian coordinates

} u_ttc_radio_ranging_message;

//}StructureDeclarations
/** { Function Declarations ********************************************
 *
 * Declare all functions here that may be called from outside.
 * Rules of Good Code:
 * - document every function declaration
 * - prefix public function names with their filename to avoid double definitions
 */

/** Provides empty, pre-initialized packet to be addressed and filled with data
 *
 * Packet buffers are stored in memory pools of equal sized memory blocks allocated from heap.
 * Whenever a packet shall be created for transmit or for incoming data, it is delivered by this
 * function. The function will block and go to sleep when the pool is empty. The sleeping task
 * is automatically awaken when a packet buffer is returned by calling ttc_radio_packet_release().
 *
 * @param Config  pointer to struct t_ttc_radio_config
 * @param Type    !=0: returned packet will be pre-initialized via ttc_radio_packet_initialize() to this type
 * @param SocketID will be copied into first payload byte (only if Type>0)
 * @return        !=NULL: empty, uninitialized packet
 */
t_ttc_packet*  radio_common_packet_get_empty( t_ttc_radio_config* Config, e_ttc_packet_type Type, e_ttc_packet_pattern SocketID );

/** Tries to provide empty, pre-initialized packet to be addressed and filled with data
 *
 * This function does nearly the same as radio_common_packet_get_empty().
 * The only difference is that it will always return immediately.
 *
 * @param Config   pointer to struct t_ttc_radio_config
 * @param Type     !=0: returned packet will be pre-initialized via ttc_radio_packet_initialize() to this type
 * @param SocketID will be copied into first payload byte (only if Type>0)
 * @return         !=NULL: empty, pre-initialized packet of given type; ==NULL: packet pool is empty (try again later)
 */
t_ttc_packet*  radio_common_packet_get_empty_try( t_ttc_radio_config* Config, e_ttc_packet_type Type, e_ttc_packet_pattern SocketID );

/** Provides empty buffer to create outgoing/ incoming data packets
 *
 * Note: This function may only be called from interrupt service routine or with disabled interrupts.
 * Note: This function will never block.
 *
 * Packet buffers are stored in memory pools of equal sized memory blocks allocated from heap.
 * Whenever a packet shall be created for transmit or for incoming data, it is delivered by this
 * function.
 *
 * @param Config   pointer to struct t_ttc_radio_config
 * @param Type     !=0: returned packet will be pre-initialized via ttc_radio_packet_initialize() to this type
 * @param SocketID will be copied into first payload byte (only if Type>0)
 * @return         !=NULL: empty, uninitialized packet
 */
t_ttc_packet*  radio_common_packet_get_empty_isr( t_ttc_radio_config* Config, e_ttc_packet_type Type, e_ttc_packet_pattern SocketID );

/** tries to remove a packet from list of received packets and returns its pointer
 *
 * Note: This function must NOT be called from an interrupt service routine!
 * Note: Caller is responsible to release packet on his own!
 * Note: Function will always return immediately even if ListRX is empty.
 *
 * @param Config  pointer to struct t_ttc_radio_config
 * @return        ==NULL: no packets queued for transmission; !=NULL: packet to transmit
 */
t_ttc_packet* radio_common_pop_list_rx_try( t_ttc_radio_config* Config );

/** Low-level driver calls this function to obtain next packet to transmit
 *
 * Note: This function may be called from an interrupt service routine ONLY!
 * Note: Function will always return immediately even if ListRX is empty.
 *
 * @param Config  pointer to struct t_ttc_radio_config
 * @return        ==NULL: no packets queued for transmission; !=NULL: packet to transmit
 */
t_ttc_packet* radio_common_pop_list_tx_isr( t_ttc_radio_config* Config );

/** Low-level driver calls this function to obtain next packet to be transmitted
 *
 * Note: This function must NOT be called from an interrupt service routine!
 *
 * @param Config  pointer to struct t_ttc_radio_config
 * @return        ==NULL: no packets queued for transmission; !=NULL: packet to transmit
 */
t_ttc_packet* radio_common_pop_list_tx( t_ttc_radio_config* Config );

/** appends given packet to list of received packets
 *
 * Note: This function must NOT be called from an interrupt service routine!
 *
 * @param Config  pointer to struct t_ttc_radio_config
 * @param Packet  received packet to be  delivered to application
 */
void radio_common_push_list_rx( t_ttc_radio_config* Config, t_ttc_packet* Packet );

/** appends given packet to list of received packets
 *
 * Note: This function may be called from an interrupt service routine ONLY!
 *
 * @param Config  pointer to struct t_ttc_radio_config
 * @param Packet  received packet to be  delivered to application
 */
void radio_common_push_list_rx_isr( t_ttc_radio_config* Config, t_ttc_packet* Packet );

/** appends given packet at end of list of packets to be transmitted
 *
 * @param Config  pointer to struct t_ttc_radio_config
 * @param Packet  received packet to be  delivered to application
 */
void radio_common_push_list_tx( t_ttc_radio_config* Config, t_ttc_packet* Packet );

/** appends given packet at end of list of packets to be transmitted
 *
 * Note: This function may be called from an interrupt service routine ONLY!
 *
 * @param Config  pointer to struct t_ttc_radio_config
 * @param Packet  received packet to be  delivered to application
 */
void radio_common_push_list_tx_isr( t_ttc_radio_config* Config, t_ttc_packet* Packet );

/** prepare common part of ttc_radio for operation
 *
 * Note: This function must be called once before any ttc_radio_init() call!
 */
void radio_common_prepare();

/** sets given packet as first in list of packets to be transmitted
 *
 * @param Config  pointer to struct t_ttc_radio_config
 * @param Packet  received packet to be  delivered to application
 */
void radio_common_prepend_list_tx( t_ttc_radio_config* Config, t_ttc_packet* Packet );

/** sets given packet as first in list of packets to be transmitted
 *
 * Note: This function may be called from an interrupt service routine ONLY!
 *
 * @param Config  pointer to struct t_ttc_radio_config
 * @param Packet  received packet to be  delivered to application
 */
void radio_common_prepend_list_tx_isr( t_ttc_radio_config* Config, t_ttc_packet* Packet );

/** Generic implementation for all kinds of ranging measures.
 *
 * This function implements different kinds of range measures. See source code for details!
 *
 * Note: If the current radio device does not support ranging, this function will simply return 0.
 * Note: This function may block until amount of required answer packets have been received or timeout occured
 *
 * @param Config                 pointer to struct t_ttc_radio_config
 * @param Type                   different types of ranging requests may be implemented (one request entry from e_radio_common_ranging_type)
 * @param AmountRepliesRequired  >0: maximum amount of reply messages to store; ==0: store all replies being received within timeout (as long as packet buffers are available)
 * @param RemoteID               Identifier of remote node (RemoteID->Address16 != 0: will use packet with 16-bit addresses, 64-bit adresses otherwise)
 * @param TimeOutMS              function will abort to wait for answer after Timeout milliseconds
 * @param ReferenceTime          == NULL: start transmission now; !=NULL: transmitter will be switched on at ReferenceTime + Config->ReferenceTimeTX
 * @param Distances              array where to write computed distances (must be large egnough to store AmountRepliesRequired entries)
 * @param Locations              != NULL: array where to store reported locations (must be large egnough to store AmountRepliesRequired entries); ==NULL: do not store location data
 * @param ReenableReceiver       ==TRUE: receiver will be switched on after last transmission (allows to receive further packets but uses more energy)
 * @return                       amount of valid ranging measures being stored in Distances[] (0..AmountRepliesRequired)
 */
t_u8 radio_common_ranging_measure( t_ttc_radio_config* Config, e_radio_common_ranging_type Type, t_u8 AmountRepliesRequired, const t_ttc_packet_address* RemoteID, t_u16 TimeOutMS, u_ttc_packetimestamp_40* ReferenceTime, t_ttc_radio_distance* VOLATILE_RADIO Distances, t_ttc_math_vector3d_xyz* VOLATILE_RADIO Locations, BOOL ReenableReceiver );

/** answers incoming ranging and localization request
 *
 * Supported message types
 * switch (u_ttc_radio_ranging_message.Type)
 *   case rcrt_Request_Ranging_SSTOFCR:
 *
 *   case rcrt_Request_Localization_SSTOFCR:
 *
 * Note: This function may only be called from interrupt service routine or with disabled interrupts!
 *
 * @param Config  pointer to struct t_ttc_radio_config
 * @param Packet  received packet containing a ranging request
 * @return        TRUE: Packet stored a valid ranging request; FALSE: Packet stored something else (no ranging reply was sent)
 */
BOOL radio_common_ranging_reply_isr( t_ttc_radio_config* Config, t_ttc_packet* Packet );

/** returns oldest received data packet (if any)
 *
 * This function allows application to poll for received data from radio and other event sources.
 * This function will always return immediately.
 * If looking for packets of a certain socket identifier, then a statemachine data vector SocketJob must be preallocated and can be
 * reused on all subsequent function calls.
 *
 * ttc_radio automatically appends all received packets in a single linked list.
 * ttc_radio_packet_received_tryget() can be called any time to obtain all packets in their arrival order
 * until NULL is returned. No packets can get lost until memory pool is exhausted.
 * See implementation of radio_common_packet_received_waitfor() for an example!
 *
 * @param Config      Configuration of radio device as returned by ttc_radio_get_configuration()
 * @param SocketJob ==NULL: look for any type of packet: !=NULL: preallocated search job with filled out SocketJob->Init part
 * @param StartAfter  == NULL: start at list head; !=NULL: pointer to list item after which to start searching for new packets
 * @return            == NULL: no packet received; != NULL: packet as received from radio
 */
t_ttc_packet* radio_common_packet_received_tryget( t_ttc_radio_config* Config, t_ttc_radio_job_socket* SocketJob );

/** Releases all packets from list of received packets for reuse.
 *
 * Call this function to simply ignore received packets.
 *
 * @param Config       Configuration of radio device as returned by ttc_radio_get_configuration()
 * @param SocketID     ==0: ignore all types of packet: >0: ignore only packets of certain socket identifier (use E_ttc_packet_pattern_socket_AnyOf_* identifiers to select a group of socket identifiers)
 * @return             amount of ignored packets
 */
t_u16 radio_common_packets_received_ignore( t_ttc_radio_config* Config, e_ttc_packet_pattern SocketID );

/** returns pointer to oldest packet in list of received packets
 *
 * Note: This function allows a fast way to inspect first packet in ListRX without removing it from list.
 *       Returned pointer must be handled with care and not written to.
 *
 * @param Config       Configuration of radio device as returned by ttc_radio_get_configuration()
 * @return             == NULL: no packet received; != NULL: oldest packet being received from radio
 */
const t_ttc_packet* radio_common_packet_received_peek( t_ttc_radio_config* Config );

/** returns oldest received data packet or waits for packet to be received
 *
 * Use of this function is recommended if multitasking is enabled and an extra task is used to
 * process received packets. This task will sleep when no packets are available for processing.
 * The drawback is, that this task can only handle incoming data from this radio.
 *
 * ttc_radio automatically appends all received packets in a single linked list.
 * ttc_radio_packet_received_tryget() can be called any time to obtain all packets in their arrival order
 * until NULL is returned.
 * This function will put current task to sleep if receive list is empty until a packet is received.
 *
 * Two other options are available to retrieve received packets:
 * 1) ttc_radio_packet_received_tryget()
 * 2) register your own interrupt service routine via ttc_radio_register_isr_receive()
 *
 * @param Config       Configuration of radio device as returned by ttc_radio_get_configuration()
 * @param UserID       ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param SocketID     ==0: look for any type of packet: >0: look for received packet of certain socket identifier (use E_ttc_packet_pattern_socket_AnyOf_* identifiers to select a group of socket identifiers)
 * @return             == NULL: no packet received (possible if ttc_radio is locked by someone else); != NULL: packet as received from radio
 */
t_ttc_packet* radio_common_packet_received_waitfor( t_ttc_radio_config* Config, e_ttc_packet_pattern SocketID );

/** Returns given packet buffer to memory pool for reuse
 *
 * Example Usage:
 *   MyPacket = radio_common_packet_release( MyPacket );
 *
 * @param Packet   packet buffer as being returned from ttc_radio_packet_get_empty()
 * @return         NULL (always returns NULL)
 */
void* radio_common_packet_release( t_ttc_packet* Packet );

/** Set new delay time for ranging replies
 *
 * If ranging is enabled and when this node receives a ranging request, it will send the
 * ranging reply message at receive time + delay time.
 * The delay time must be long egnough to prepare the reply message.
 *
 * @param LogicalIndex          device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param Delay_us     (t_u32)  delay time between ranging request receival and ranging reply transmission (microseconds)
 */
void radio_common_set_ranging_delay( t_ttc_radio_config* Config, t_u32 Delay_us );

/** central breakpoint function for radio debugging
 */
void radio_common_breakpoint();

#define RCT_QUEUE_ONLY (u_ttc_packetimestamp_40*) -1

/** immediately send  or queue single radio packet for transmission
 *
 * Note: The state of each packet transmission can be monitored if its Meta.StatusTX field has been set.
 *
 *
 * Packet Transmission Schemes
 *
 * 1) Queue packet for later transmission (TransmitTime==RCT_QUEUE_ONLY)
 *    Given Packet is added at end of packet transmission queue but no transmission is initiated.
 *    Transmission of all queued packets is initiated by calling _driver_radio_transmit() manually.
 *    Example:
 *      radio_common_transmit_packet( Config, Packet1, 23, FALSE, RCT_QUEUE_ONLY);
 *      radio_common_transmit_packet( Config, Packet2, 46, FALSE, RCT_QUEUE_ONLY);
 *      radio_common_transmit_packet( Config, Packet3, 12, FALSE, RCT_QUEUE_ONLY);
 *      _driver_radio_transmit();
 *
 * 2) Transmit Packet now (TransmitTime==NULL)
 *    Transmitter is switched on and given Packet is send as soon as possible before all queued packets.
 *    Example:
 *      radio_common_transmit_packet( Config, Packet, 42, FALSE, NULL);
 *
 * 3) Transmit Packet at given time (TransmitTime!=NULL) && (TransmitTime!=RCT_QUEUE_ONLY)
 *    Transmitter is switched on and given Packet is send at exact time (if possible) before all queued packets.
 *    Example:
 *      radio_common_transmit_packet( Config, Packet, 42, FALSE, TimeTX);
 *
 *
 * Packet Release Strategy
 *
 * Packet->Meta.DoNotReleaseBuffer
 * ==0: Given Packet will be released back to its memory pool automatically after processing.
 * ==1: Given Packet will not be released automatically.
 *      Calling function has to observe Packet->Meta.StatusTX variable to wait until it is processed.
 *      Example #1 - Send packet and observe its transmission from application:
 *         t_ttc_packet* Packet = ttc_radio_packet_get_empty( LogicalIndex, PacketType, SocketID );
 *
 *         // address Packet
 *         // fill Packet Payload
 *
 *         Packet->Meta.DoNotReleaseBuffer = 1;
 *         Packet->Meta.StatusTX = 0;
 *         radio_common_transmit_packet( Config, Packet, 42, FALSE, NULL);
 *         while ( Packet->Meta.StatusTX == 0 ) ttc_task_yield();
 *
 *         // fill Packet Payload (reuse packet buffer and its header)
 *         Packet->Meta.DoNotReleaseBuffer = 1;
 *         Packet->Meta.StatusTX = 0;
 *         radio_common_transmit_packet( Config, Packet, 42, FALSE, NULL);
 *         while ( Packet->Meta.StatusTX == 0 ) ttc_task_yield();
 *         ...
 *
 *         radio_common_packet_release(Packet);
 *
 *
 * Sending packet with 2-way handshake to ensure delivery
 *      Handshake is enabled by setting the appropriate acknowledge request bit in the packet header.
 *      radio_common_transmit_packet() will wait for an acknowledge packet until Config->Init.Acknowledge_Timeout_usecs microseconds have passed.
 *      If acknowledge is received with enabled ACK field, an acknowledge-acknowledge (ACK-ACK) is send.
 *      Transmitting with handshakes allows to reliably check if a packet has been received by target node successfully.
 *
 *      Example #2 - Send a packet with 2-way handshake:
 *         t_ttc_packet* Packet = ttc_radio_packet_get_empty( LogicalIndex, PacketType, SocketID );
 *
 *         // address Packet
 *         // fill Packet Payload
 *
 *         // set acknowledge bit in packet header
 *         ttc_packet_acknowledge_request_set(Packet, TRUE);
 *
 *         e_ttc_radio_errorcode Error = radio_common_transmit_packet( Config, Packet, 42, FALSE, NULL);
 *         if (Error) { // remote did not send acknowledge before timeout
 *            ...we may now resend packet...
 *         }
 *
 *
 * @param LogicalIndex           device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param Packet                 data packet to transmit ( must have been obtained from ttc_radio_packet_get_empty() )
 * @param PayloadSize            amount of bytes stored in Payload[] (calculated by ttc_packet_payload_get_pointer() )
 * @param ReceiveAfterTransmit   == TRUE: turn on receiver after transmission to receive immediate answer
 * @param TransmitTime           == RCT_QUEUE_ONLY: do not transmit but add packet at end of transmit queue
 *                               == NULL:           start transmission now (before all queued packets)
 *                                  ELSE:           transmitter will be switched on at this time (packet is sent before all queue packets)
 * @return Transmission status of Packet can be monitored by checking Packet->Meta.StatusTX (see comments above!).
 * @return ==0: packet was queued for transmission or send successfully
 */
e_ttc_radio_errorcode radio_common_transmit_packet( t_ttc_radio_config* Config, t_ttc_packet* Packet, t_u16 PayloadSize, BOOL ReceiveAfterTransmit, u_ttc_packetimestamp_40* TransmitTime );

/** adds 32-bit value to a 40-bit value in-situ
 *
 * @param Value40  pointer to 40-bit value to add to
 * @param Add      32-bit value to add to Value40
 * @param Sum40    pointer to 40-bit value where to store sum (may be == Value40)
 * @return         result of addition is stored in Sum40[5]
 */
void radio_common_add_40_32_40( u_ttc_packetimestamp_40* Value40, t_u32 Add, u_ttc_packetimestamp_40* Sum40 );

/** Computes 32-bit difference between two near 40-bit values
 *
 * Assumptions:
 *   Value40a > Value40b
 *   Value40a - Value40b fits into unsigned 32 bit value
 *
 * @param Value40a  40-bit value to subtract from
 * @param Value40b  40-bit value to subtract
 * @return          Value40a > Value40b: Value40a - Value40b; 0 otherwise
 */
t_u32 radio_common_sub_40_40_32( u_ttc_packetimestamp_40* Value40a, u_ttc_packetimestamp_40* Value40b );

/** Add a nanosecond delay to given time stamp
 *
 * Timestamps are transceiver specific. Only current low-level knows its timebase.
 *
 * @param Time        (u_ttc_packetimestamp_40*)  time stamp of a received or transmitted packet
 * @param Delay_ns    (t_u32)                       delay time to add to Time (nanoseconds)
 * @param TimeDelayed (u_ttc_packetimestamp_40*)  will be loaded with Time + Delay_ns; may be same as Time (TimeDelayed==Time)
 * @return  delayed timestamp has been written to *TimeDelayed
 */
void radio_common_time_add_ns( t_ttc_radio_config* Config, u_ttc_packetimestamp_40* Time, t_u32 Delay_ns, u_ttc_packetimestamp_40* TimeDelayed );

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //radio_common_h
