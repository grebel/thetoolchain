#ifndef RADIO_DW1000_H
#define RADIO_DW1000_H

/** { radio_dw1000.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for radio devices on dw1000 architectures.
 *  Structures, Enums and Defines being required by high-level radio and application.
 *
 *  Created from template device_architecture.h revision 22 at 20150309 14:18:02 UTC
 *
 *  Note: See ttc_radio.h for description of dw1000 independent RADIO implementation.
 *
 *  Authors: Adrián Romero Cáceres
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_RADIO_DW1000
//
// Implementation status of low-level driver support for radio devices on dw1000
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_RADIO_DW1000 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_RADIO_DW1000 == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_RADIO_DW1000 to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_RADIO_DW1000

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "radio_dw1000.c"
//
#include "../ttc_packet_types.h"
#include "../ttc_radio_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_radio_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_radio_foo
//
#define ttc_driver_radio_deinit(Config) radio_dw1000_deinit(Config)
#define ttc_driver_radio_configuration_check(Config) radio_dw1000_configuration_check(Config)
#define ttc_driver_radio_init(Config) radio_dw1000_init(Config)
#define ttc_driver_radio_load_defaults(Config) radio_dw1000_load_defaults(Config)
#define ttc_driver_radio_maintenance(Config) radio_dw1000_maintenance(Config)
#define ttc_driver_radio_prepare() radio_dw1000_prepare()
#define ttc_driver_radio_reset(Config) radio_dw1000_reset(Config)
#define ttc_driver_radio_transmit(Config, TransmitTime) radio_dw1000_transmit(Config, TransmitTime)
#define ttc_driver_radio_isr_receive(Index, Config) radio_dw1000_isr_generic(Index, Config)
#define ttc_driver_radio_isr_transmit(Index, Config) radio_dw1000_isr_generic(Index, Config)
#define ttc_driver_radio_change_local_address(Config, LocalAddress) radio_dw1000_change_local_address(Config, LocalAddress)
#define ttc_driver_radio_configure_frame_filter(Config, Settings, ChangeFilter) radio_dw1000_configure_frame_filter(Config, Settings, ChangeFilter)
#define ttc_driver_radio_change_delay_time(Config, Delay_uSeconds) radio_dw1000_change_delay_time(Config, Delay_uSeconds)
#define ttc_driver_radio_update_meta(Config, Packet) radio_dw1000_update_meta(Config, Packet)
#define ttc_driver_radio_receiver(Config, Enable, ReferenceTime, TimeOut_us) radio_dw1000_receiver(Config, Enable, ReferenceTime, TimeOut_us)
#define ttc_driver_radio_configure_auto_acknowledge radio_dw1000_configure_auto_acknowledge
#define ttc_driver_radio_gpio_out radio_dw1000_gpio_out
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

// dw1000 does not allow to switch rx- and tx-channels independently
#define ttc_driver_radio_configure_channel_rxtx(Config, NewChannel) radio_dw1000_configure_channel_rxtx(Config, NewChannel)
#undef ttc_driver_radio_configure_channel_tx
#undef ttc_driver_radio_configure_channel_rx


//} Macro Definitions
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_radio.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_radio.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

/** shutdown single RADIO unit device
 * @param Config        pointer to struct t_ttc_radio_config
 * @return              == 0: RADIO has been shutdown successfully; != 0: error-code
 */
e_ttc_radio_errorcode radio_dw1000_deinit( t_ttc_radio_config* Config );

/** Configure frame filter in connected radio
 *
 * Most radio transceivers provide an intergrated filter to automatically ignore received packets
 * that are not to be processed by this node.
 *
 * @param Config       pointer to struct t_ttc_radio_config
 * @param Settings     (u_ttc_radio_frame_filter)  configuration to use
 * @param ChangeFilter (t_u8)                      == 1: load new filter setting into radio; ==0: only read current filter setting; ==-1: store changes in cache for later update
 * @return             (u_ttc_radio_frame_filter)  updated configuration (unsupported filters may be reset)
 */
u_ttc_radio_frame_filter radio_dw1000_configure_frame_filter( t_ttc_radio_config* Config, u_ttc_radio_frame_filter Settings, t_u8 ChangeFilter );

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_radio_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_radio_features.
 *
 * @param Config = pointer to struct t_ttc_radio_config
 */
void radio_dw1000_configuration_check( t_ttc_radio_config* Config );

/** change output state of single gpio pin on DW1000
 *
 * Note: DW1000 GPIO must be configured as output before!
 *
 * @param Config       (t_ttc_radio_config*)  Configuration of radio device as returned by ttc_radio_get_configuration()
 * @param LogicalIndex (t_u8)                 Logical index of radio instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RADIO<n> lines in extensions.active/makefile
 * @param Pin          (e_ttc_radio_gpio)     gpio pin to control (current radio may not provide all possible pins)
 * @param NewState     (BOOL)                 ==TRUE: set GPIO to logical 1; ==FALSE: set GPIO to logical 0
 * @return             (BOOL)                 ==TRUE: desired GPIO pin could be configured to new output state
 */
BOOL radio_dw1000_gpio_out( t_ttc_radio_config* Config, e_ttc_radio_gpio Pin, BOOL State );

/** initializes single RADIO unit for operation
 * @param Config        pointer to struct t_ttc_radio_config
 * @return              == 0: RADIO has been initialized successfully; != 0: error-code
 */
e_ttc_radio_errorcode radio_dw1000_init( t_ttc_radio_config* Config );

/** loads configuration of indexed RADIO unit with default values
 * @param Config        pointer to struct t_ttc_radio_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_radio_errorcode radio_dw1000_load_defaults( t_ttc_radio_config* Config );

/** Issues regular maintenance on indexed radio.
 *
 * Call this function regularly to compensate temperature changes
 *
 * @param Config        pointer to struct t_ttc_radio_config
 */
void radio_dw1000_maintenance( t_ttc_radio_config* Config );

/** Prepares radio driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void radio_dw1000_prepare();

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_radio_config
 * @return   =
 */
e_ttc_radio_errorcode radio_dw1000_reset( t_ttc_radio_config* Config );

/** Switches channel used to receive + transmit data.
 *
 * @param Config         radio configuration as returned by ttc_radio_get_configuration()
 * @param NewChannel      Config->Features->MinChannel .. Config->Features->MaxChannel
 * @return                == 0: data has been queued for transmission/ has been sent successfully; != 0: error-code
 */
e_ttc_radio_errorcode radio_dw1000_configure_channel_rxtx( t_ttc_radio_config* Config, t_u8 NewChannel );

/** transmit single packet now or at specified time
 *
 * Duties of low-level driver:
 * do {
 *   (1) pop oldest packet from transmission list via radio_common_pop_list_tx()
 *   (2) calculate and configure exact time of transmission (if DelayNS!=0)
 *   (3) configure according to Packet->Meta.Flags
 *   (4) call Config->Init.function_start_tx_isr(Packet) (if !=NULL)
 *   (5) send packet to radio transmit buffer
 *   (6) start packet transmission
 *   (7) call ttc_heap_pool_block_free(Packet) after successfull transmission to return packet it to its memory pool
 *   (8) call Config->Init.function_end_tx_isr(Packet) (if !=NULL)
 * } while ( s_ttc_listize(Config->List_PacketsTX) );
 *
 * Note: ttc_radio_transmit_packet() takes care that this function is not called from multiple tasks in parallel.
 *
 * @param Config                pointer to struct t_ttc_radio_config
 * @param ReceiveAfterTransmit  == TRUE: switch on receiver after transmission to receive response
 * @param TransmitTime          == 0: start transmission now; >0: time when to switch on transmitter
 */
void radio_dw1000_transmit( t_ttc_radio_config* Config, u_ttc_packetimestamp_40* TransmitTime );

/** low-level interrupt service routine for incoming packets
 *
 * This function must be registered as interrupt service routine for incoming packets.
 * Example for interrupts via GPIO pin A4:
 *    t_ttc_interrupt_handle RadioInterrupt = ttc_interrupt_init(tit_GPIO_Rising, E_ttc_gpio_pin_a4, _driver_radio_isr_transmit, Config, 0, 0);
 *    Assert(RadioInterrupt, ttc_assert_origin_auto); // could not initialize interrupt (configured GPIO pin not usable for external interrupts on current uC architecture?)
 *    ttc_interrupt_enable_gpio(RadioInterrupt);
 *
 * This function has to be implemented by low-level radio driver:
 * 1) get an empty packet buffer via radio_common_packet_get_empty_isr()
 * 2) copy received raw data bytes from transceiver into empty packet
 * 3) call radio_common_push_list_rx_isr() to deliver filled packet to application
 *    The delivered packet will automatically been released to its memory pool after being processed.
 *
 * The low-level radio driver may declare these functions as extern to be able to call
 * @param Index   index of triggered interrupt (for gpio interrupts, this can be casted to e_ttc_gpio_pin)
 * @param Config  radio configuration as returned by ttc_radio_get_configuration()
 * @return        latest error condition reported from radio (if any)
 */
void radio_dw1000_isr_generic( t_physical_index Index, t_ttc_radio_config* Config );

/** Sets new protocol source address for outgoing and incoming packets
 *
 * After changing the local address, all further packets given to ttc_radio_transmit()
 * are will use it as source address. Also the address filter in the radio device will be updated to
 * accept packets for the new address.
 *
 * @param Config         radio configuration as returned by ttc_radio_get_configuration()
 * @param LocalAddress   !=NULL: new local logical and physical address; == NULL: don't change local address
 */
void radio_dw1000_change_local_address( t_ttc_radio_config* Config, const t_ttc_packet_address* LocalAddress );

/* DEPRECATED code from DecaWave example
* radio_dw1000_initialise_radio()
 *
 * Description: This function initiates communications with the DW1000 transceiver
 * and reads its DEV_ID register (address 0x00) to verify the IC is one supported
 * by this software (e.g. DW1000 32-bit device ID value is 0xDECA0130).  Then it
 * does any initial once only device configurations needed for use and initialises
 * as necessary any static data items belonging to this low-level driver.
 *
 * NOTES:
 * 1.this function needs to be run before dwt_configuresleep, also the SPI freq has to be < 3MHz
 * 2.it also reads any calibration data from OTP memory as specified by the input paramter
 *
 *
 * input parameters
 * @param config    -   specifies what configuration to load form OTP memory
 *                  DWT_LOADUCODE     0x800 - load the LDE micro-code from ROM - enabled accurate RX timestamp
 *                  DWT_LOADTXCONFIG  0x4 - load the TX power values
 *                  DWT_LOADANTDLY    0x2 - load the antenna delay values
 *                  DWT_LOADXTALTRIM  0x1 - load XTAL trim values
 *                  DWT_LOADNONE      0x0 - do not load any values from OTP memory
 *
 * output parameters
 *
 * returns DWT_SUCCESS for success, or DWT_ERROR for error

void radio_dw1000_initialise_radio( t_ttc_radio_config* Config, t_u32 InitialConfiguration );

//reset the DW1000 by driving the RSTn line low
void radio_dw1000_resetby_RSTN_line_low( t_ttc_radio_config* Config );

//Function to check if the dw1000 radio is sleeping
void radio_dw1000_check_if_asleep( t_ttc_radio_config* Config );

! ------------------------------------------------------------------------------------------------------------------
 * @fn void radio_dw1000_set_interrupt_dw1000( t_u32 bitmask, t_u8 enable)
 *
 * @brief This function enables the specified events to trigger an interrupt.
 * The following events can be enabled:
 * DWT_INT_TFRS         0x00000080          // frame sent
 * DWT_INT_RFCG         0x00004000          // frame received with good CRC
 * DWT_INT_RPHE         0x00001000          // receiver PHY header error
 * DWT_INT_RFCE         0x00008000          // receiver CRC error
 * DWT_INT_RFSL         0x00010000          // receiver sync loss error
 * DWT_INT_RFTO         0x00020000          // frame wait timeout
 * DWT_INT_RXPTO        0x00200000          // preamble detect timeout
 * DWT_INT_SFDT         0x04000000          // SFD timeout
 * DWT_INT_ARFE         0x20000000          // frame rejected (due to frame filtering configuration)
 *
 *
 * input parameters:
 * @param bitmask - sets the events which will generate interrupt
 * @param enable - if set the interrupts are enabled else they are cleared
 *
 * output parameters
 *
 * no return value

void radio_dw1000_set_interrupt_dw1000( t_ttc_radio_config* Config, t_u32 BitMask, t_u8 Enable );
void radio_dw1000_config_features( t_ttc_radio_config* Config );

! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_write_TxData()
 *
 * @brief This API function writes the supplied TX data into the DW1000's
 * TX buffer.  The input parameters are the data length in bytes and a pointer
 * to those data bytes.
 *
 * input parameters
 * @param txFrameLength  - This is the total frame length, including the two byte CRC.
 *                         Note: this is the length of TX message (including the 2 byte CRC) - max is 1023
 *                         standard PHR mode allows up to 127 bytes
 *                         if > 127 is programmed, DWT_PHRMODE_EXT needs to be set in the phrMode configuration
 *                         see dwt_configure function
 * @param txFrameBytes   - Pointer to the user\92s buffer containing the data to send.
 * @param txBufferOffset - This specifies an offset in the DW1000\92s TX Buffer at which to start writing data.
 *
 * output parameters
 *
 * returns DWT_SUCCESS for success, or DWT_ERROR for error

t_u8 radio_dw1000_write_TxData( t_ttc_radio_config* Config, t_u16  txFrameLength, t_u8* txFrameBytes, t_u16  txBufferOffset );
! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_write_TxFctrl()
 *
 * @brief This API function configures the TX frame control register before the transmission of a frame
 *
 * input parameters:
 * @param txFrameLength - this is the length of TX message (including the 2 byte CRC) - max is 1023
 *                              NOTE: standard PHR mode allows up to 127 bytes
 *                              if > 127 is programmed, DWT_PHRMODE_EXT needs to be set in the phrMode configuration
 *                              see dwt_configure function
 * @param txBufferOffset - the offset in the tx buffer to start writing the data
 *
 * output parameters
 *
 * returns DWT_SUCCESS for success, or DWT_ERROR for error
 *

void radio_dw1000_write_TxFctrl( t_ttc_radio_config* Config, t_u16  txFrameLength, t_u16  txBufferOffset );
! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_Enter_Sleep_after_Tx()
 *
 * @brief This API function returns the tx power value read from OTP memory as part of initialisation
 *
 * input parameters
 * @param prf   -   this is the PRF e.g. DWT_PRF_16M or DWT_PRF_64M
 * @param chan  -   this is the channel e.g. 1 to 7
 *
 * output parameters
 *
 * returns tx power value for a given PRF and channel

t_u32 radio_dw1000_GetOTPtxpower( t_ttc_radio_config* Config, t_u8 prf, t_u8 chan );
void radio_dw1000_Enter_Sleep_after_Tx( t_ttc_radio_config* Config, t_u8 enable );
! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_configure_parameters()
 *
 * @brief This function provides the main API for the configuration of the
 * DW1000 and this low-level driver.  The input is a pointer to the data structure
 * of type t_dwt_config that holds all the configurable items.
 * The t_dwt_config structure shows which ones are supported
 *
 * input parameters
 * @param config    -   pointer to the configuration structure, which contains the device configuration data.
 * @param useotp    -   specifies what configuration (read form OTP memory as part of initialisation) to configure
 *                  DWT_LOADANTDLY    0x2 - load the antenna delay values
 *                  DWT_LOADXTALTRIM  0x1 - load XTAL trim values
 *                  DWT_LOADNONE      0x0 - do not load any values from OTP memory
 *
 * output parameters
 *
 * returns DWT_SUCCESS for success, or DWT_ERROR for error

void radio_dw1000_configure_parameters( t_ttc_radio_config* Config, t_u16 InitialConfiguration );
! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_XtalTrim()
 *
 * @brief This is used adjust the crystal frequency
 *
 * input parameters:
 * @param   value - crystal trim value (in range 0x0 to 0x1F) 31 steps (~1.5ppm per step)
 *
 * @output
 *
 * no return value

void radio_dw1000_XtalTrim( t_ttc_radio_config* Config, t_u8 value );
! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_Configure_LDE()
 *
 * @brief configure LDE algorithm parameters
 *
 * input parameters
 * @param prf   -   this is the PRF index (0 or 1) 0 correcpods to 16 and 1 to 64 PRF
 *
 * output parameters
 *
 * no return value

void radio_dw1000_Configure_LDE( t_ttc_radio_config* Config, t_u8 prfIndex );
! ------------------------------------------------------------------------------------------------------------------
 _dwt_otpread - function to read the OTP memory. Ensure that MR,MRa,MRb are reset to 0.
//

t_u32 radio_dw1000_read_otp( t_ttc_radio_config* Config, t_u32 Address );
! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_Load_UCode()
 *
 * @brief  load ucode from OTP MEMORY or ROM
 *
 * input parameters
 *
 * output parameters
 *
 * no return value

void radio_dw1000_Load_UCode( t_ttc_radio_config* Config );
! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_config_event_counters()
 *
 *  @brief This is used to enable/disable the event counter in the IC
 *
 * input parameters
 * @param - enable - 1 enables (and reset), 0 disables the event counters
 * output parameters
 *
 * no return value

void radio_dw1000_config_event_counters( t_ttc_radio_config* Config, t_u8 State );
! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_configure_frame_filter()
 *
 *  @brief This is used to enable the frame filtering - (the default option is to
 *  accept any data and ack frames with correct destination address
 *
 * input parameters
 * @param - bitmask - enables/disables the frame filtering options according to
 *      DWT_FF_NOTYPE_EN        0x000   no frame types allowed
 *      DWT_FF_COORD_EN         0x002   behave as coordinator (can receive frames with no destination address (PAN ID has to match))
 *      DWT_FF_BEACON_EN        0x004   beacon frames allowed
 *      DWT_FF_DATA_EN          0x008   data frames allowed
 *      DWT_FF_ACK_EN           0x010   ack frames allowed
 *      DWT_FF_MAC_EN           0x020   mac control frames allowed
 *      DWT_FF_RSVD_EN          0x040   reserved frame types allowed
 *
 * output parameters
 *
 * no return value

void radio_dw1000_configure_frame_filter( t_ttc_radio_config* Config, t_u16 Types );

! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_AutoACK()
 *
 *  @brief This call enables the auto-ACK feature. If the responseReferenceTime (paramter) is 0, the ACK will be sent a.s.a.p.
 *  otherwise it will be sent with a programmed delay (in symbols), max is 255.
 *
 * input parameters
 * @param responseReferenceTime - if non-zero the ACK is sent after this delay, max is 255.
 *
 * output parameters
 *
 * no return value

void radio_dw1000_AutoACK( t_ttc_radio_config* Config );
! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_Antenna_Delay()
 *
 * @brief This API function writes the antenna delay (in time units) to TX registers
 *
 * input parameters:
 * @param antennaDly - this is the total (TX) antenna delay value, which
 *                          will be programmed into the TX delay register
 *
 * output parameters
 *
 * no return value
 *

void radio_dw1000_Antenna_Delay( t_ttc_radio_config* Config );
! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_disable_sequencing()
 *
 * @brief  This function disables the TX blocks sequencing, it disables PMSC control of RF blocks, system clock is also set to XTAL
 *
 * input parameters none
 *
 * output parameters none
 *
 * no return value

void radio_dw1000_disable_sequencing( t_ttc_radio_config* Config );
// upload always on array configuration and go to sleep
void radio_dw1000_AON_Upload( t_ttc_radio_config* Config );


// -------------------------------------------------------------------------------------------------------------------
//
// Force Transceiver OFF
//
void radio_dw1000_turn_off( t_ttc_radio_config* Config );

! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_set_Rx_after_Tx_delay()
 *
 *  @brief This sets the receiver turn on delay time after a transmission of a frame
 *
 * input parameters
 * @param rxReferenceTime - (20 bits) - the delay is in micro seconds
 *
 * output parameters
 *
 * no return value

void radio_dw1000_set_Rx_after_Tx_delay( t_ttc_radio_config* Config, t_u32 rxReferenceTime );

! ------------------------------------------------------------------------------------------------------------------
 * @fn  radio_dw1000_set_RxTimeOut()
 *
 * @brief This call enables RX timeout (SYS_STAT_RFTO event)
 *
 * input parameters
 * @param time - how long the receiver remains on from the RX enable command
 *               The time parameter used here is in 1.012 us (128/499.2MHz) units
 *               If set to 0 the timeout is disabled.
 *
 * output parameters
 *
 * no return value

void radio_dw1000_set_RxTimeOut( t_ttc_radio_config* Config, t_u16  Time );
! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_set_Preamble_detection_TimeOut()
 *
 *  @brief This call enables preamble timeout (SY_STAT_RXPTO event)
 *
 * input parameters
 * @param  timeout - in PACs
 *
 * output parameters
 *
 * no return value

void radio_dw1000_set_Preamble_detection_TimeOut( t_ttc_radio_config* Config, t_u16  Timeout );


! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_read_diagnostics()
 *
 *  @brief @brief this function reads the rx signal quality diagnostic data
 *
 * input parameters
 * @param diagnostics - diagnostic structure pointer, this will contain the diagnostic data read from the DW1000
 *
 * output parameters
 *
 * no return value

void radio_dw1000_read_diagnostics( t_ttc_radio_config* Config );
! ------------------------------------------------------------------------------------------------------------------
 * Function: radio_dw1000_RangeBiasCorrection()
 *
 * Description: This function is used to return the range bias correction need for TWR with DW1000 units.
 *
 * input parameters:
 * @param chan  - specifies the operating channel (e.g. 1, 2, 3, 4, 5, 6 or 7)
 * @param range - the calculated distance before correction
 * @param prf   - this is the PRF e.g. DWT_PRF_16M or DWT_PRF_64M
 *
 * output parameters
 *
 * returns correction needed in meters

t_u32 radio_dw1000_RangeBiasCorrection( t_u8 chan, t_u32 range, t_u8 prf );

! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_SetLeds()
 *
 *  @brief This is used to set up Tx/Rx GPIOs which could be used to control LEDs
 *  Note: not completely IC dependent, also needs board with LEDS fitted on right I/O lines
 *        this function enables GPIOs 2 and 3 which are connected to LED3 and LED4 on EVB1000
 *
 * input parameters
 * @param test - if 1 the LEDs will be enabled, if 0 the LED control is disabled.
 *             - if value is 2 the LEDs will flash once after enable.
 *
 * output parameters
 *
 * no return value

void radio_dw1000_SetLeds( t_ttc_radio_config* Config, t_u8 test );


! ------------------------------------------------------------------------------------------------------------------
 * @fn radio_dw1000_ConfigureSleep()
 *
 *  @brief configures the device for both DEEP_SLEEP and SLEEP modes, and on-wake mode
 *  I.e. before entering the sleep, the device should be programmed for TX or RX, then upon "waking up" the TX/RX settings
 *  will be preserved and the device can immediately perform the desired action TX/RX
 *
 * NOTE: e.g. MP :- Tag operation - after deep sleep, the device needs to just load the TX buffer and send the frame
 *
 *
 *      mode: the array and LDE code (OTP/ROM) and LDO tune, and set sleep persist
 *      DWT_LOADLDO      0x1000 - load LDO tune value from OTP
 *      DWT_LOADUCODE    0x0800 - load ucode from OTP
 *      DWT_PRESRV_SLEEP 0x0100 - preserve sleep
 *      DWT_LOADOPSET    0x0080 - load operating parameter set on wakeup
 *      DWT_CONFIG       0x0040 - download the AON array into the HIF (configuration download)
 *      DWT_LOADEUI      0x0008
 *      DWT_GOTORX       0x0002
 *      DWT_TANDV        0x0001
 *
 *      wake: wake up parameters
 *      DWT_XTAL_EN      0x10 - keep XTAL running during sleep
 *      DWT_WAKE_SLPCNT  0x8 - wake up after sleep count
 *      DWT_WAKE_CS      0x4 - wake up on chip select
 *      DWT_WAKE_WK      0x2 - wake up on WAKEUP PIN
 *      DWT_SLP_EN       0x1 - enable sleep/deep sleep functionality
 *
 * input parameters
 * @param mode - config on-wake parameters
 * @param wake - config wake up parameters
 *
 * output parameters
 *
 * no return value

void radio_dw1000_ConfigureSleep( t_ttc_radio_config* Config, t_u16 mode, t_u8 wake );

//DEPRECATED */


/** Set new delay to exactly enable transmitter or receiver after given reference time
 *
 * Exact time delays allow to reduce energy by switching off receiver while remote node is preparing an answer packet.
 * Scenario:
 * A:TX @T1a ---------------> B:RX @T1b
 *                            B:prepare answer
 * A:wait until T1a + Delay   B:wait until T1b + Delay
 * A:RX <-------------------- B:TX @T2a
 *
 * @param Config          pointer to struct t_ttc_radio_config
 * @param Delay_uSeconds  >0: time between receival of next message and transmission start of next message; ==0: transmissions are sent immediately
 */
void radio_dw1000_change_delay_time( t_ttc_radio_config* Config, t_u16 Delay_uSeconds );


/** Updates meta header of given packet.
 *
 * During reception of a packet frame, an update of Meta header often requires complex calculations.
 * Running these calculations in the interrupt service routine would increase the response time.
 * This function shall be called from application to run the calculations required to fill all fields
 * in Packet->Meta.
 *
 * @param Config        pointer to struct t_ttc_radio_config
 * @param Packet       (t_ttc_packet*)  packet which Meta header is to be updated
 * @return             ==0: Meta header has been updated; >0: error code
 */
e_ttc_radio_errorcode radio_dw1000_update_meta( t_ttc_radio_config* Config, t_ttc_packet* Packet );


/** Switches radio receiver on or off
 *
 * The receiver can be switched on now at a specified time.
 * Delayed switch times allow to reduce energy usage.
 * This can be done by answering an incoming message after a fixed, predefined delay.
 * Scenario:
 * A sends a message to B. B processes this message and sends an answer to A.
 * 1) Messages are sent immediately
 *    A does not know how long B will take to prepare its answer as runtime of software is difficult to
 *    forecast. So A has to switch on it's receiver after sending to B.
 * 2) Messages are answered after fixed delay
 *    A goes to sleep after sending to B for a predefined delay time.
 *    B has egnough time to prepare its answer.
 *    A switches on its receiver at SendTime + DelayTime
 *    B switches on its transmitter at ReceiveTime + DelayTime
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param Config          configuration of radio device as returned by ttc_radio_get_configuration()
 * @param Enable          !=0: switch on receiver to listen for incoming messages; ==0: switch of receiver (reduces power)
 * @param ReferenceTime   ==NULL: enable/disable receiver immediately; !=NULL: switch state at ReferenceTime + Config->Delay_RX
 * @return                !=NULL: receiver has been activated successfully; ==NULL: delayed start of receiver was too late (receiver
 */
BOOL radio_dw1000_receiver( t_ttc_radio_config* Config, BOOL Enable, u_ttc_packetimestamp_40* ReferenceTime, t_base TimeOut_us );


/** Change or read current setting of auto acknowledge feature
 *
 * Auto acknowledgement is a feature being provided by several transceivers. If a packet is received
 * with header flag acknowledge set, the transceiver can immediately generate and sent the acknowledge packet
 * without interaction of the connected microcontroller.
 *
 * The basic auto acknowledgment setting is configured via setting Config->Init.Flags.AutoAcknowledge before calling ttc_radio_init().
 * Even if enabled, the radio may not be able to generate auto acknowledgements because of its current state.
 * This function allows to set and read the current state from low-level driver.
 * The return value may differ from Enable if transceiver is currently not able to support this feature.
 *
 * See datasheet of your transceiver and current low-level driver for details!
 *
 * @param Config       (t_ttc_radio_config*)  Configuration of radio device as returned by ttc_radio_get_configuration()
 * @param LogicalIndex (t_u8)                 Logical index of radio instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RADIO<n> lines in extensions.active/makefile
 * @param Enable       (BOOL)                 ==TRUE: enable auto acknowledge; ==FALSE: disable auto acknowledge
 * @param Change       (BOOL)                 ==TRUE: write value of Enable to radio; ==FALSE: only read current setting
 * @return             (BOOL)                 current auto acknowledge setting; ==TRUE: radio will automatically reply to incoming packets with set acknowledge flag
 */
BOOL radio_dw1000_configure_auto_acknowledge( t_ttc_radio_config* Config, BOOL Enable, BOOL Change );

//InsertFunctionDeclarations  above (DO NOT DELETE THIS LINE!)

#endif //RADIO_DW1000_H
