#ifndef radio_dw1000_registers_h
#define radio_dw1000_registers_h

/** { radio_dw1000_registers.h *********************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.h revision 11 at 20151028 15:01:12 UTC
 *
 *  Authors: Gregor Rebel
 *
 * Definitions of and macros to read/ write dw1000-registers via SPI bus.
 *
}*/
/** { Includes *********************************************************
 *
 * Put all includes here that are required to include this header file.
 * Do NOT add includes required to compile radio_dw1000_registers.c here!
 */

#include "../ttc_basic.h"  // basic datatypes
#include "../ttc_memory.h" // memory checks and safe pointers
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Constant Defines / Macros ****************************************
 *
 * As a convention, constant definitions are all UPPERCASED to distinguish
 * them from variables and functions.
 *
 * Examples:

 #define ABC 1
 #define calculate(A,B) (A+B)
 */

/** register change macros
 *
 * Each macro will change value of single field or whole value of a local register copy
 * and mark it as changed.
 * All changed registers can be send to DW1000 via _radio_dw1000_register_write_changed() later.
 *
 * Required local variables:
 *   t_radio_dw1000_registers* Registers = &Config->LowLevelConfig.Registers;
 *   t_radio_dw1000_changed*   Changed   = &Config->LowLevelConfig.Changed;
 *
 * @param REGISTER   one field from *Registers
 * @param FIELD      one field from Registers->REGISTER.Fields
 * @param NEW_VALUE  value to assign to field or whole register
 * @return           Config->LowLevelConfig.Registers: value of single register copy changed
 * @return           Config->LowLevelConfig.Changed: corresponding bit set
 */
#define changeRegisterWord(REGISTER, NEW_VALUE)         Registers->REGISTER.Word         = NEW_VALUE; Changed->REGISTER = 1
#define changeRegisterField(REGISTER, FIELD, NEW_VALUE) Registers->REGISTER.Fields.FIELD = NEW_VALUE; Changed->REGISTER = 1
#define changeRegister(REGISTER, NEW_VALUE)             Registers->REGISTER              = NEW_VALUE; Changed->REGISTER = 1


/** { Register Access Macros
 *
 * All _radio_dw1000_register_*() macros operate the same way.
 * Register size and endianess are taken care of automatically.
 *
 * How to use
 * 1) _radio_dw1000_register_read_<FOO>(Config);               // read content of register <FOO> into Config->LowLevelConfig.Registers.<FOO>
 * 2) Config->LowLevelConfig.Registers.<FOO>.Fields.<BLA> = 1; // change individual fields in local copy of register <FOO>
 * 3) _radio_dw1000_register_write_<FOO>(Config);              // write Config->LowLevelConfig.Registers.<FOO> into register <FOO> in DW1000
 *
 */

// read/ write register Device ID
void _radio_dw1000_register_read_DEV_ID( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_DEV_ID( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_DEV_ID(Config)  \
    Config->LowLevelConfig.Registers.DEV_ID = _radio_dw1000_register_read_32bit(Config, rdr_DEV_ID)
#define _radio_dw1000_register_write_DEV_ID(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_DEV_ID, Config->LowLevelConfig.Registers.DEV_ID)


// read/ write register Extended Unique Identifier (64 bit Address64 address)
void _radio_dw1000_register_read_EUI64( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_EUI64( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_EUI64(Config)  \
    _radio_dw1000_spi_register_read( Config, rdr_EUI64, sizeof(Config->LocalID.Address64), Config->LocalID.Address64.Bytes)
#define _radio_dw1000_register_write_EUI64(Config) \
    _radio_dw1000_spi_register_write(Config, rdr_EUI64, sizeof(Config->LocalID.Address64), Config->LocalID.Address64.Bytes)


// read/ write register 16 bit Local ID
void _radio_dw1000_register_read_SHORT_ADDR( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_SHORT_ADDR( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_SHORT_ADDR(Config)  \
    Config->LocalID.Address16 = _radio_dw1000_register_read_16bit(Config, rdr_SHORT_ADDR)
#define _radio_dw1000_register_write_SHORT_ADDR(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_SHORT_ADDR, Config->LocalID.Address16)


// read/ write register 16 bit Local Pan ID
void _radio_dw1000_register_read_PAN_ID( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_PAN_ID( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_PAN_ID(Config)  \
    LocalID.PanID = _radio_dw1000_register_read_16bit(Config, rdr_PAN_ID)
#define _radio_dw1000_register_write_PAN_ID(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_PAN_ID, Config->LocalID.PanID)


// read/ write register System Configuration
void _radio_dw1000_register_read_SYS_CFG( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_SYS_CFG( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_SYS_CFG(Config)  \
    Config->LowLevelConfig.Registers.SYS_CFG.Word = _radio_dw1000_register_read_32bit(Config, rdr_SYS_CFG)
#define _radio_dw1000_register_write_SYS_CFG(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_SYS_CFG, Config->LowLevelConfig.Registers.SYS_CFG.Word)

// read/ write register System Configuration
void _radio_dw1000_register_read_SYS_STATE( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_SYS_STATE( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_SYS_STATE(Config)  \
    Config->LowLevelConfig.Registers.SYS_STATE.Word = _radio_dw1000_register_read_32bit(Config, rdr_SYS_STATE)
#define _radio_dw1000_register_write_SYS_STATE(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_SYS_STATE, Config->LowLevelConfig.Registers.SYS_STATE.Word)

// read/ write register System Time Counter
void _radio_dw1000_register_read_SYS_TIME( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_SYS_TIME( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_SYS_TIME(Config)  \
    _radio_dw1000_register_read_40bit(Config, rdr_SYS_TIME, Config->LowLevelConfig.Registers.SYS_TIME.Bytes)
#define _radio_dw1000_register_write_SYS_TIME(Config) \
    _radio_dw1000_register_write_40bit(Config, rdr_SYS_TIME, Config->LowLevelConfig.Registers.SYS_TIME.Bytes)


// read/ write register Transmitter Frame Control
void _radio_dw1000_register_read_TX_FCTRL( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_TX_FCTRL( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_TX_FCTRL(Config)  \
    _radio_dw1000_register_read_40bit(Config, rdr_TX_FCTRL, Config->LowLevelConfig.Registers.TX_FCTRL.Bytes)
#define _radio_dw1000_register_write_TX_FCTRL(Config) \
    _radio_dw1000_register_write_40bit(Config, rdr_TX_FCTRL, Config->LowLevelConfig.Registers.TX_FCTRL.Bytes)


// read/ write register Delayed Send or Receive Time (40 bits)
void _radio_dw1000_register_read_DX_TIME( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_DX_TIME( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_DX_TIME(Config)  \
    _radio_dw1000_register_read_40bit(Config, rdr_DX_TIME, Config->LowLevelConfig.Registers.DX_TIME.Bytes)
#define _radio_dw1000_register_write_DX_TIME(Config) \
    _radio_dw1000_register_write_40bit(Config, rdr_DX_TIME, Config->LowLevelConfig.Registers.DX_TIME.Bytes)


// read/ write register Receiver Frame Wait Timeout Period
void _radio_dw1000_register_read_RX_FWTO( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_RX_FWTO( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_RX_FWTO(Config)  \
    Config->LowLevelConfig.Registers.RX_FWTO = _radio_dw1000_register_read_16bit(Config, rdr_RX_FWTO)
#define _radio_dw1000_register_write_RX_FWTO(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_RX_FWTO, Config->LowLevelConfig.Registers.RX_FWTO)


// read/ write register System Control
void _radio_dw1000_register_read_SYS_CTRL( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_SYS_CTRL( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_SYS_CTRL(Config)  \
    Config->LowLevelConfig.Registers.SYS_CTRL.Word = _radio_dw1000_register_read_32bit(Config, rdr_SYS_CTRL)
#define _radio_dw1000_register_write_SYS_CTRL(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_SYS_CTRL, Config->LowLevelConfig.Registers.SYS_CTRL.Word)


// read/ write register System Event Masks
void _radio_dw1000_register_read_SYS_MASK( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_SYS_MASK( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_SYS_MASK(Config)  \
    Config->LowLevelConfig.Registers.SYS_MASK.Word = _radio_dw1000_register_read_32bit(Config, rdr_SYS_MASK)
#define _radio_dw1000_register_write_SYS_MASK(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_SYS_MASK, Config->LowLevelConfig.Registers.SYS_MASK.Word)


// read/ write register System Event Status
void _radio_dw1000_register_read_SYS_STATUS( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_SYS_STATUS( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_SYS_STATUS(Config)  \
    _radio_dw1000_register_read_40bit(Config, rdr_SYS_STATUS, Config->LowLevelConfig.Registers.SYS_STATUS.Bytes)
#define _radio_dw1000_register_write_SYS_STATUS(Config) \
    _radio_dw1000_register_write_40bit(Config, rdr_SYS_STATUS, Config->LowLevelConfig.Registers.SYS_STATUS.Bytes)


// read/ write register Receiver Frame Information (double buffered operation only)
void _radio_dw1000_register_read_RX_FINFO( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_RX_FINFO( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_RX_FINFO(Config)  \
    Config->LowLevelConfig.Registers.RX_FINFO.Word = _radio_dw1000_register_read_32bit(Config, rdr_RX_FINFO)
#define _radio_dw1000_register_write_RX_FINFO(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_RX_FINFO, Config->LowLevelConfig.Registers.RX_FINFO.Word)


// read/ write register Receiver Frame Quality Information
void _radio_dw1000_register_read_RX_FQUAL( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_RX_FQUAL( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_RX_FQUAL(Config)  \
    _radio_dw1000_spi_register_read( Config, rdr_RX_FQUAL, 8, Config->LowLevelConfig.Registers.RX_FQUAL.Bytes)
#define _radio_dw1000_register_write_RX_FQUAL(Config) \
    _radio_dw1000_spi_register_write( Config, rdr_RX_FQUAL, 8, Config->LowLevelConfig.Registers.RX_FQUAL.Bytes)


// read/ write register Receiver Time Tracking Interval
void _radio_dw1000_register_read_RX_TTCKI( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_RX_TTCKI( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_RX_TTCKI(Config)  \
    Config->LowLevelConfig.Registers.RX_TTCKI = _radio_dw1000_register_read_32bit(Config, rdr_RX_TTCKI)
#define _radio_dw1000_register_write_RX_TTCKI(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_RX_TTCKI, Config->LowLevelConfig.Registers.RX_TTCKI)


// read/ write register Receiver Time Tracking Offset
void _radio_dw1000_register_read_RX_TTCKO( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_RX_TTCKO( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_RX_TTCKO(Config)  \
    _radio_dw1000_register_read_40bit(Config, rdr_RX_TTCKO, Config->LowLevelConfig.Registers.RX_TTCKO.Bytes)
#define _radio_dw1000_register_write_RX_TTCKO(Config) \
    _radio_dw1000_register_write_40bit(Config, rdr_RX_TTCKO, Config->LowLevelConfig.Registers.RX_TTCKO.Bytes)


/* use RX_STAMP instead!
 // read/ write register adjusted time stamp of last frame
void _radio_dw1000_register_read_RX_TIME( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_RX_TIME( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_RX_TIME(Config)  \
    _radio_dw1000_register_read_40bit(Config, rdr_RX_TIME, Config->LowLevelConfig.Registers.RX_TIME.Bytes)
#define _radio_dw1000_register_write_RX_TIME(Config) \
    _radio_dw1000_register_write_40bit(Config, rdr_RX_TIME, Config->LowLevelConfig.Registers.RX_TIME.Bytes)
*/

// read/ write register Adjusted Receive Time of first ray (Leading Edge)
void _radio_dw1000_register_read_RX_STAMP( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_RX_STAMP( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_RX_STAMP(Config)  \
    _radio_dw1000_register_read_40bit(Config, rdr_RX_STAMP, Config->LowLevelConfig.Registers.RX_STAMP.Bytes)
#define _radio_dw1000_register_write_RX_STAMP(Config) \
    _radio_dw1000_register_write_40bit(Config, rdr_RX_STAMP, Config->LowLevelConfig.Registers.RX_STAMP.Bytes)

// read/ write register First Path Data of received frame
void _radio_dw1000_register_read_FIRST_PATH( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_FIRST_PATH( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_FIRST_PATH(Config)  \
    Config->LowLevelConfig.Registers.FIRST_PATH.Word = _radio_dw1000_register_read_32bit(Config, rdr_FIRST_PATH)
#define _radio_dw1000_register_write_FIRST_PATH(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_FIRST_PATH, Config->LowLevelConfig.Registers.FIRST_PATH.Word)


// read/ write register Receive Time Stamp First Path Amplitude Point 1
void _radio_dw1000_register_read_RX_FPAMPL1( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_RX_FPAMPL1( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_RX_FPAMPL1(Config)  \
    Config->LowLevelConfig.Registers.RX_FPAMPL1.Word = _radio_dw1000_register_read_32bit(Config, rdr_RX_FPAMPL1)
#define _radio_dw1000_register_write_RX_FPAMPL1(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_RX_FPAMPL1, Config->LowLevelConfig.Registers.RX_FPAMPL1.Word)


// read/ write register adjusted time stamp of last transmission (including antenna delay)
void _radio_dw1000_register_read_TX_STAMP( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_TX_STAMP( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_TX_STAMP(Config)  \
    _radio_dw1000_register_read_40bit(Config, rdr_TX_STAMP, Config->LowLevelConfig.Registers.TX_STAMP.Bytes)
#define _radio_dw1000_register_write_TX_STAMP(Config) \
    _radio_dw1000_register_write_40bit(Config, rdr_TX_STAMP, Config->LowLevelConfig.Registers.TX_STAMP.Bytes)


// read/ write register raw time stamp of last transmission
void _radio_dw1000_register_read_TX_RAWST( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_TX_RAWST( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_TX_RAWST(Config)  \
    _radio_dw1000_register_read_40bit(Config, rdr_TX_RAWST, Config->LowLevelConfig.Registers.TX_RAWST.Bytes)
#define _radio_dw1000_register_write_TX_RAWST(Config) \
    _radio_dw1000_register_write_40bit(Config, rdr_TX_RAWST, Config->LowLevelConfig.Registers.TX_RAWST.Bytes)


// read/ write register Transmitter Antenna Delay
void _radio_dw1000_register_read_TX_ANTD( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_TX_ANTD( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_TX_ANTD(Config)  \
    Config->LowLevelConfig.Registers.TX_ANTD = _radio_dw1000_register_read_16bit(Config, rdr_TX_ANTD)
#define _radio_dw1000_register_write_TX_ANTD(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_TX_ANTD, Config->LowLevelConfig.Registers.TX_ANTD)


// read/ write register Acknowledgement Time and Response Time
void _radio_dw1000_register_read_ACK_RESP_T( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_ACK_RESP_T( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_ACK_RESP_T(Config)  \
    Config->LowLevelConfig.Registers.ACK_RESP_T.Word = _radio_dw1000_register_read_32bit(Config, rdr_ACK_RESP_T)
#define _radio_dw1000_register_write_ACK_RESP_T(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_ACK_RESP_T, Config->LowLevelConfig.Registers.ACK_RESP_T.Word)


// read/ write register Acknowledgement Time and Response Time Delay
void _radio_dw1000_register_read_ACK_DELY( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_ACK_DELY( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_ACK_DELY(Config)  \
    Config->LowLevelConfig.Registers.ACK_DELY.Word = _radio_dw1000_register_read_32bit(Config, rdr_ACK_DELY)
#define _radio_dw1000_register_write_ACK_DELY(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_ACK_DELY, Config->LowLevelConfig.Registers.ACK_DELY.Word)


// read/ write register Pulsed Preamble Reception Configuration
void _radio_dw1000_register_read_RX_SNIFF( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_RX_SNIFF( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_RX_SNIFF(Config)  \
    Config->LowLevelConfig.Registers.RX_SNIFF.Word = _radio_dw1000_register_read_16bit(Config, rdr_RX_SNIFF)
#define _radio_dw1000_register_write_RX_SNIFF(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_RX_SNIFF, Config->LowLevelConfig.Registers.RX_SNIFF.Word)


// read/ write register Transmitter Power Control
void _radio_dw1000_register_read_TX_POWER( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_TX_POWER( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_TX_POWER(Config)  \
    Config->LowLevelConfig.Registers.TX_POWER = _radio_dw1000_register_read_32bit(Config, rdr_TX_POWER)
#define _radio_dw1000_register_write_TX_POWER(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_TX_POWER, Config->LowLevelConfig.Registers.TX_POWER)


// read/ write register Channel Control
void _radio_dw1000_register_read_CHAN_CTRL( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_CHAN_CTRL( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_CHAN_CTRL(Config)  \
    Config->LowLevelConfig.Registers.CHAN_CTRL.Word = _radio_dw1000_register_read_32bit(Config, rdr_CHAN_CTRL)
#define _radio_dw1000_register_write_CHAN_CTRL(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_CHAN_CTRL, Config->LowLevelConfig.Registers.CHAN_CTRL.Word)


// read/ write register User specified short/long TX/RX Start of Frame Delimiter sequences
void _radio_dw1000_register_read_USR_SFD( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_USR_SFD( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_USR_SFD(Config)  \
    Config->LowLevelConfig.Registers.USR_SFD.Word = _radio_dw1000_register_read_32bit(Config, rdr_USR_SFD)
#define _radio_dw1000_register_write_USR_SFD(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_USR_SFD, Config->LowLevelConfig.Registers.USR_SFD.Word)


/* DEPRECATED
// read/ write register Automatic Gain Control
void _radio_dw1000_register_read_AGC_CTRL( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_AGC_CTRL( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_AGC_CTRL(Config)  \
    Config->LowLevelConfig.Registers.AGC_CTRL.Word = _radio_dw1000_register_read_32bit(Config, rdr_AGC_CTRL)
#define _radio_dw1000_register_write_AGC_CTRL(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_AGC_CTRL, Config->LowLevelConfig.Registers.AGC_CTRL.Word)
*/

// read/ write register Automatic Gain Control 1
void _radio_dw1000_register_read_AGC_CTRL1( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_AGC_CTRL1( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_AGC_CTRL1(Config)  \
    Config->LowLevelConfig.Registers.AGC_CTRL1.Word = _radio_dw1000_register_read_16bit(Config, rdr_AGC_CTRL1)
#define _radio_dw1000_register_write_AGC_CTRL1(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_AGC_CTRL1, Config->LowLevelConfig.Registers.AGC_CTRL1.Word)


// read/ write register Automatic Gain Control Tuning 1
void _radio_dw1000_register_read_AGC_TUNE1( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_AGC_TUNE1( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_AGC_TUNE1(Config)  \
    Config->LowLevelConfig.Registers.AGC_TUNE1 = _radio_dw1000_register_read_16bit(Config, rdr_AGC_TUNE1)
#define _radio_dw1000_register_write_AGC_TUNE1(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_AGC_TUNE1, Config->LowLevelConfig.Registers.AGC_TUNE1)


// read/ write register Automatic Gain Control Tuning 2
void _radio_dw1000_register_read_AGC_TUNE2( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_AGC_TUNE2( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_AGC_TUNE2(Config)  \
    Config->LowLevelConfig.Registers.AGC_TUNE2 = _radio_dw1000_register_read_32bit(Config, rdr_AGC_TUNE2)
#define _radio_dw1000_register_write_AGC_TUNE2(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_AGC_TUNE2, Config->LowLevelConfig.Registers.AGC_TUNE2)


// read/ write register Automatic Gain Control Tuning 3
void _radio_dw1000_register_read_AGC_TUNE3( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_AGC_TUNE3( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_AGC_TUNE3(Config)  \
    Config->LowLevelConfig.Registers.AGC_TUNE3 = _radio_dw1000_register_read_16bit(Config, rdr_AGC_TUNE3)
#define _radio_dw1000_register_write_AGC_TUNE3(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_AGC_TUNE3, Config->LowLevelConfig.Registers.AGC_TUNE3)


// read/ write register Automatic Gain Control Status
void _radio_dw1000_register_read_AGC_STAT1( t_ttc_radio_config* Config );
//void _radio_dw1000_register_write_AGC_STAT1( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_AGC_STAT1(Config)  \
    _radio_dw1000_spi_register_read(Config,   rdr_AGC_STAT1, 3, Config->LowLevelConfig.Registers.AGC_STAT1.Bytes)
//#define _radio_dw1000_register_write_AGC_STAT1(Config)  register is readonly!
//    _radio_dw1000_register_write(Config, rdr_AGC_STAT1, 3, Config->LowLevelConfig.Registers.AGC_STAT1.Bytes)


// read/ write register External Synchronization Control
void _radio_dw1000_register_read_EC_CTRL( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_EC_CTRL( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_EC_CTRL(Config)  \
    Config->LowLevelConfig.Registers.EC_CTRL.Word = _radio_dw1000_register_read_32bit(Config, rdr_EC_CTRL)
#define _radio_dw1000_register_write_EC_CTRL(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_EC_CTRL, Config->LowLevelConfig.Registers.EC_CTRL.Word)


// read/ write register GPIO_MODE 0x26:00 GPIO Mode Control Register -> DW1000 UserManual v2.03 p.121
void _radio_dw1000_register_read_GPIO_MODE( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_GPIO_MODE( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_GPIO_MODE(Config)  \
    Config->LowLevelConfig.Registers.GPIO_MODE.Word = _radio_dw1000_register_read_32bit(Config, rdr_GPIO_MODE)
#define _radio_dw1000_register_write_GPIO_MODE(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_GPIO_MODE, Config->LowLevelConfig.Registers.GPIO_MODE.Word)


// read/ write register GPIO_DIR 0x26:08  GPIO Direction Control Register -> DW1000 UserManual v2.03 p.123
void _radio_dw1000_register_read_GPIO_DIR( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_GPIO_DIR( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_GPIO_DIR(Config)  \
    Config->LowLevelConfig.Registers.GPIO_DIR.Word = _radio_dw1000_register_read_32bit(Config, rdr_GPIO_DIR)
#define _radio_dw1000_register_write_GPIO_DIR(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_GPIO_DIR, Config->LowLevelConfig.Registers.GPIO_DIR.Word)

// read/ write register GPIO_DOUT 0x26:0c  GPIO Data Output register -> DW1000 User Manual p.125
void _radio_dw1000_register_read_GPIO_DOUT( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_GPIO_DOUT( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_GPIO_DOUT(Config)  \
    Config->LowLevelConfig.Registers.GPIO_DOUT.Word = _radio_dw1000_register_read_32bit(Config, rdr_GPIO_DOUT)
#define _radio_dw1000_register_write_GPIO_DOUT(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_GPIO_DOUT, Config->LowLevelConfig.Registers.GPIO_DOUT.Word)


// read/ write register GPIO_IRQE 0x26:10  GPIO Interrupt Enable -> DW1000 User Manual p.127
void _radio_dw1000_register_read_GPIO_IRQE( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_GPIO_IRQE( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_GPIO_IRQE(Config)  \
    Config->LowLevelConfig.Registers.GPIO_IRQE.Word = _radio_dw1000_register_read_32bit(Config, rdr_GPIO_IRQE)
#define _radio_dw1000_register_write_GPIO_IRQE(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_GPIO_IRQE, Config->LowLevelConfig.Registers.GPIO_IRQE.Word)


// read/ write register GPIO_ISEN 0x26:14  GPIO Interrupt Sense Selection -> DW1000 User Manual p.128
void _radio_dw1000_register_read_GPIO_ISEN( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_GPIO_ISEN( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_GPIO_ISEN(Config)  \
    Config->LowLevelConfig.Registers.GPIO_ISEN.Word = _radio_dw1000_register_read_32bit(Config, rdr_GPIO_ISEN)
#define _radio_dw1000_register_write_GPIO_ISEN(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_GPIO_ISEN, Config->LowLevelConfig.Registers.GPIO_ISEN.Word)


// read/ write register GPIO_IMODE 0x26:18  GPIO Interrupt Mode (Level / Edge) -> DW1000 User Manual p.129
void _radio_dw1000_register_read_GPIO_IMODE( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_GPIO_IMODE( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_GPIO_IMODE(Config)  \
    Config->LowLevelConfig.Registers.GPIO_IMODE.Word = _radio_dw1000_register_read_32bit(Config, rdr_GPIO_IMODE)
#define _radio_dw1000_register_write_GPIO_IMODE(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_GPIO_IMODE, Config->LowLevelConfig.Registers.GPIO_IMODE.Word)


// read/ write register GPIO_IBES 0x26:1c  GPIO Interrupt “Both Edge” Select -> DW1000 User Manual p.130
void _radio_dw1000_register_read_GPIO_IBES( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_GPIO_IBES( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_GPIO_IBES(Config)  \
    Config->LowLevelConfig.Registers.GPIO_IBES.Word = _radio_dw1000_register_read_32bit(Config, rdr_GPIO_IBES)
#define _radio_dw1000_register_write_GPIO_IBES(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_GPIO_IBES, Config->LowLevelConfig.Registers.GPIO_IBES.Word)


// read/ write register GPIO_ICLR 0x26:20  GPIO Interrupt Latch Clear -> DW1000 User Manual p.131
void _radio_dw1000_register_read_GPIO_ICLR( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_GPIO_ICLR( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_GPIO_ICLR(Config)  \
    Config->LowLevelConfig.Registers.GPIO_ICLR.Word = _radio_dw1000_register_read_32bit(Config, rdr_GPIO_ICLR)
#define _radio_dw1000_register_write_GPIO_ICLR(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_GPIO_ICLR, Config->LowLevelConfig.Registers.GPIO_ICLR.Word)


// read/ write register GPIO_IDBE 0x26:24  GPIO Interrupt De-bounce Enable -> DW1000 User Manual p.132
void _radio_dw1000_register_read_GPIO_IDBE( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_GPIO_IDBE( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_GPIO_IDBE(Config)  \
    Config->LowLevelConfig.Registers.GPIO_IDBE.Word = _radio_dw1000_register_read_32bit(Config, rdr_GPIO_IDBE)
#define _radio_dw1000_register_write_GPIO_IDBE(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_GPIO_IDBE, Config->LowLevelConfig.Registers.GPIO_IDBE.Word)


// read/ write register GPIO_RAW 0x26:28  GPIO raw state -> DW1000 User Manual p.133
void _radio_dw1000_register_read_GPIO_RAW( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_GPIO_RAW( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_GPIO_RAW(Config)  \
    Config->LowLevelConfig.Registers.GPIO_RAW.Word = _radio_dw1000_register_read_32bit(Config, rdr_GPIO_RAW)
#define _radio_dw1000_register_write_GPIO_RAW(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_GPIO_RAW, Config->LowLevelConfig.Registers.GPIO_RAW.Word)

// read/ write register RF_STATUS  0x28:2c PLL-Lock Status Register-> DW1000 User Manual p.143
void _radio_dw1000_register_read_RF_STATUS( t_ttc_radio_config* Config );
//void _radio_dw1000_register_write_RF_STATUS( t_ttc_radio_config* Config ); (register is read only!)

#define _radio_dw1000_register_read_RF_STATUS(Config)  \
    Config->LowLevelConfig.Registers.RF_STATUS.Word = _radio_dw1000_register_read_32bit(Config, rdr_RF_STATUS)
//#define _radio_dw1000_register_write_RF_STATUS(Config)
//    _radio_dw1000_register_write_32bit(Config, rdr_RF_STATUS, Config->LowLevelConfig.Registers.RF_STATUS.Word)

// read/ write register RF_CONF  0x28:2c PLL-Lock Status Register-> DW1000 User Manual p.143
void _radio_dw1000_register_read_RF_CONF( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_RF_CONF( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_RF_CONF(Config)  \
    Config->LowLevelConfig.Registers.RF_CONF.Word = _radio_dw1000_register_read_32bit(Config, rdr_RF_CONF)
#define _radio_dw1000_register_write_RF_CONF(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_RF_CONF, Config->LowLevelConfig.Registers.RF_CONF.Word)

// read/ write register Digital Receiver Tune 0b
void _radio_dw1000_register_read_DRX_TUNE0b( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_DRX_TUNE0b( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_DRX_TUNE0b(Config)  \
    Config->LowLevelConfig.Registers.DRX_TUNE0b = _radio_dw1000_register_read_16bit(Config, rdr_DRX_TUNE0b)
#define _radio_dw1000_register_write_DRX_TUNE0b(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_DRX_TUNE0b, Config->LowLevelConfig.Registers.DRX_TUNE0b)


// read/ write register Digital Receiver Tune 1a
void _radio_dw1000_register_read_DRX_TUNE1a( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_DRX_TUNE1a( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_DRX_TUNE1a(Config)  \
    Config->LowLevelConfig.Registers.DRX_TUNE1a = _radio_dw1000_register_read_16bit(Config, rdr_DRX_TUNE1a)
#define _radio_dw1000_register_write_DRX_TUNE1a(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_DRX_TUNE1a, Config->LowLevelConfig.Registers.DRX_TUNE1a)


// read/ write register Digital Receiver Tune 1b
void _radio_dw1000_register_read_DRX_TUNE1b( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_DRX_TUNE1b( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_DRX_TUNE1b(Config)  \
    Config->LowLevelConfig.Registers.DRX_TUNE1b = _radio_dw1000_register_read_16bit(Config, rdr_DRX_TUNE1b)
#define _radio_dw1000_register_write_DRX_TUNE1b(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_DRX_TUNE1b, Config->LowLevelConfig.Registers.DRX_TUNE1b)


// read/ write register Digital Receiver Tune 2
void _radio_dw1000_register_read_DRX_TUNE2( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_DRX_TUNE2( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_DRX_TUNE2(Config)  \
    Config->LowLevelConfig.Registers.DRX_TUNE2 = _radio_dw1000_register_read_32bit(Config, rdr_DRX_TUNE2)
#define _radio_dw1000_register_write_DRX_TUNE2(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_DRX_TUNE2, Config->LowLevelConfig.Registers.DRX_TUNE2)


// read/ write register Digital Receiver Start Frame Delimiter Timeout
void _radio_dw1000_register_read_DRX_SFDTOC( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_DRX_SFDTOC( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_DRX_SFDTOC(Config)  \
    Config->LowLevelConfig.Registers.DRX_SFDTOC = _radio_dw1000_register_read_16bit(Config, rdr_DRX_SFDTOC)
#define _radio_dw1000_register_write_DRX_SFDTOC(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_DRX_SFDTOC, Config->LowLevelConfig.Registers.DRX_SFDTOC)


// read/ write register Digital Receiver Preamble Detection Timeout
void _radio_dw1000_register_read_DRX_PRETOC( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_DRX_PRETOC( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_DRX_PRETOC(Config)  \
    Config->LowLevelConfig.Registers.DRX_PRETOC = _radio_dw1000_register_read_16bit(Config, rdr_DRX_PRETOC)
#define _radio_dw1000_register_write_DRX_PRETOC(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_DRX_PRETOC, Config->LowLevelConfig.Registers.DRX_PRETOC)


// read/ write register Digital Receiver Tuning Register 4H
void _radio_dw1000_register_read_DRX_TUNE4H( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_DRX_TUNE4H( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_DRX_TUNE4H(Config)  \
    Config->LowLevelConfig.Registers.DRX_TUNE4H = _radio_dw1000_register_read_16bit(Config, rdr_DRX_TUNE4H)
#define _radio_dw1000_register_write_DRX_TUNE4H(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_DRX_TUNE4H, Config->LowLevelConfig.Registers.DRX_TUNE4H)

// read/ write register Analog Transmitter Control
void _radio_dw1000_register_read_RF_RXCTRLH( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_RF_RXCTRLH( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_RF_RXCTRLH(Config)  \
    Config->LowLevelConfig.Registers.RF_RXCTRLH = _radio_dw1000_register_read_8bit(Config, rdr_RF_RXCTRLH)
#define _radio_dw1000_register_write_RF_RXCTRLH(Config) \
    _radio_dw1000_register_write_8bit(Config, rdr_RF_RXCTRLH, Config->LowLevelConfig.Registers.RF_RXCTRLH)

// read/ write register Analog Transmitter Control
void _radio_dw1000_register_read_RF_TXCTRL( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_RF_TXCTRL( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_RF_TXCTRL(Config)  \
    Config->LowLevelConfig.Registers.RF_TXCTRL = _radio_dw1000_register_read_32bit(Config, rdr_RF_TXCTRL)
#define _radio_dw1000_register_write_RF_TXCTRL(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_RF_TXCTRL, Config->LowLevelConfig.Registers.RF_TXCTRL)


// read/ write register Always On Configuration WCFG (2c:00)
void _radio_dw1000_register_read_AON_WCFG( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_AON_WCFG( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_AON_WCFG(Config)  \
    Config->LowLevelConfig.Registers.AON_WCFG.Word = _radio_dw1000_register_read_16bit(Config, rdr_AON_WCFG)
#define _radio_dw1000_register_write_AON_WCFG(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_AON_WCFG, Config->LowLevelConfig.Registers.AON_WCFG.Word)


// read/ write register Always-On Control Register (2c:02)
void _radio_dw1000_register_read_AON_CTRL( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_AON_CTRL( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_AON_CTRL(Config)  \
    Config->LowLevelConfig.Registers.AON_CTRL.Byte = _radio_dw1000_register_read_8bit(Config, rdr_AON_CTRL)
#define _radio_dw1000_register_write_AON_CTRL(Config) \
    _radio_dw1000_register_write_8bit(Config, rdr_AON_CTRL, Config->LowLevelConfig.Registers.AON_CTRL.Byte)

/* DEPRECATED
// read/ write register Always-On Read Data (2c:03)
void _radio_dw1000_register_read_AON_RDAT( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_AON_RDAT( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_AON_RDAT(Config)  \
    Config->LowLevelConfig.Registers.AON_RDAT = _radio_dw1000_register_read_8bit(Config, rdr_AON_RDAT)
#define _radio_dw1000_register_write_AON_RDAT(Config) \
    _radio_dw1000_register_write_8bit(Config, rdr_AON_RDAT, Config->LowLevelConfig.Registers.AON_RDAT)


// read/ write register Always-On Direct Access Address (2c:04)
void _radio_dw1000_register_read_AON_ADDR( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_AON_ADDR( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_AON_ADDR(Config)  \
    Config->LowLevelConfig.Registers.AON_ADDR = _radio_dw1000_register_read_8bit(Config, rdr_AON_ADDR)
#define _radio_dw1000_register_write_AON_ADDR(Config) \
    _radio_dw1000_register_write_8bit(Config, rdr_AON_ADDR, Config->LowLevelConfig.Registers.AON_ADDR)
*/

// read/ write register Always-On Configuration Register 0 (2c:06)
void _radio_dw1000_register_read_AON_CFG0( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_AON_CFG0( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_AON_CFG0(Config)  \
    Config->LowLevelConfig.Registers.AON_CFG0.Word = _radio_dw1000_register_read_32bit(Config, rdr_AON_CFG0)
#define _radio_dw1000_register_write_AON_CFG0(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_AON_CFG0, Config->LowLevelConfig.Registers.AON_CFG0.Word)


// read/ write register Always-On Configuration Register 1 (2c:0a)
void _radio_dw1000_register_read_AON_CFG1( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_AON_CFG1( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_AON_CFG1(Config)  \
    Config->LowLevelConfig.Registers.AON_CFG1.Word = _radio_dw1000_register_read_16bit(Config, rdr_AON_CFG1)
#define _radio_dw1000_register_write_AON_CFG1(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_AON_CFG1, Config->LowLevelConfig.Registers.AON_CFG1.Word)


/* read/ write register Transmitter Calibration Block
void _radio_dw1000_register_read_TX_CAL( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_TX_CAL( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_TX_CAL(Config)  \
    Config->LowLevelConfig.Registers.TX_CAL.Word = _radio_dw1000_register_read_32bit(Config, rdr_TX_CAL)
#define _radio_dw1000_register_write_TX_CAL(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_TX_CAL, Config->LowLevelConfig.Registers.TX_CAL.Word)
*/

//read/ write register Transmitter Pulse Generator Delay
void _radio_dw1000_register_read_TC_PGDELAY( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_TC_PGDELAY( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_TC_PGDELAY(Config)  \
    Config->LowLevelConfig.Registers.TC_PGDELAY = _radio_dw1000_register_read_8bit(Config, rdr_TC_PGDELAY)
#define _radio_dw1000_register_write_TC_PGDELAY(Config) \
    _radio_dw1000_register_write_8bit(Config, rdr_TC_PGDELAY, Config->LowLevelConfig.Registers.TC_PGDELAY)

/* read/ write register Frequency Synthesizer Control
void _radio_dw1000_register_read_FS_CTRL( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_FS_CTRL( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_FS_CTRL(Config)  \
    Config->LowLevelConfig.Registers.FS_CTRL.Word = _radio_dw1000_register_read_32bit(Config, rdr_FS_CTRL)
#define _radio_dw1000_register_write_FS_CTRL(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_FS_CTRL, Config->LowLevelConfig.Registers.FS_CTRL.Word)
*/

// read/ write register Frequency synthesiser – PLL configuration
void _radio_dw1000_register_read_FS_PLLCFG( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_FS_PLLCFG( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_FS_PLLCFG(Config)  \
    Config->LowLevelConfig.Registers.FS_PLLCFG = _radio_dw1000_register_read_32bit(Config, rdr_FS_PLLCFG)
#define _radio_dw1000_register_write_FS_PLLCFG(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_FS_PLLCFG, Config->LowLevelConfig.Registers.FS_PLLCFG)

// read/ write register Frequency synthesiser – PLL Tuning
void _radio_dw1000_register_read_FS_PLLTUNE( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_FS_PLLTUNE( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_FS_PLLTUNE(Config)  \
    Config->LowLevelConfig.Registers.FS_PLLTUNE = _radio_dw1000_register_read_8bit(Config, rdr_FS_PLLTUNE)
#define _radio_dw1000_register_write_FS_PLLTUNE(Config) \
    _radio_dw1000_register_write_8bit(Config, rdr_FS_PLLTUNE, Config->LowLevelConfig.Registers.FS_PLLTUNE)

// read/ write register Frequency synthesiser – Crystal trim
void _radio_dw1000_register_read_FS_XTALT( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_FS_XTALT( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_FS_XTALT(Config)  \
    Config->LowLevelConfig.Registers.FS_XTALT.Byte = _radio_dw1000_register_read_8bit(Config, rdr_FS_XTALT)
#define _radio_dw1000_register_write_FS_XTALT(Config) \
    _radio_dw1000_register_write_8bit(Config, rdr_FS_XTALT, Config->LowLevelConfig.Registers.FS_XTALT.Byte)

/* OTP-Registers have special interface. It makes no sense to cache them like normal registers
 * OTP-Access is handled by _radio_dw1000_read_otp() as

// read/ write register One Time Programmable Memory Interface
void _radio_dw1000_register_read_OTP_IF( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_OTP_IF( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_OTP_IF(Config)  \
    Config->LowLevelConfig.Registers.OTP_IF.Word = _radio_dw1000_register_read_32bit(Config, rdr_OTP_IF)
#define _radio_dw1000_register_write_OTP_IF(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_OTP_IF, Config->LowLevelConfig.Registers.OTP_IF.Word)

// read/ write register One Time Programmable Memory Read Data
void _radio_dw1000_register_read_OTP_STAT( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_OTP_STAT( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_OTP_STAT(Config)  \
    Config->LowLevelConfig.Registers.OTP_STAT.Word = _radio_dw1000_register_read_32bit(Config, rdr_OTP_STAT)
#define _radio_dw1000_register_write_OTP_STAT(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_OTP_STAT, Config->LowLevelConfig.Registers.OTP_STAT.Word)

// read/ write register One Time Programmable Memory Read Data
void _radio_dw1000_register_read_OTP_RDAT( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_OTP_RDAT( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_OTP_RDAT(Config)  \
    Config->LowLevelConfig.Registers.OTP_RDAT.Word = _radio_dw1000_register_read_32bit(Config, rdr_OTP_RDAT)
#define _radio_dw1000_register_write_OTP_RDAT(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_OTP_RDAT, Config->LowLevelConfig.Registers.OTP_RDAT.Word)

// read/ write register One Time Programmable Memory Write Data
void _radio_dw1000_register_read_OTP_WDAT( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_OTP_WDAT( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_OTP_WDAT(Config)  \
    Config->LowLevelConfig.Registers.OTP_WDAT = _radio_dw1000_register_read_32bit(Config, rdr_OTP_WDAT)
#define _radio_dw1000_register_write_OTP_WDAT(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_OTP_WDAT, Config->LowLevelConfig.Registers.OTP_WDAT)

// read/ write register One Time Programmable Memory Special Read Data
void _radio_dw1000_register_read_OTP_SRDAT( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_OTP_SRDAT( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_OTP_SRDAT(Config)  \
    Config->LowLevelConfig.Registers.OTP_SRDAT = _radio_dw1000_register_read_32bit(Config, rdr_OTP_SRDAT)
#define _radio_dw1000_register_write_OTP_SRDAT(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_OTP_SRDAT, Config->LowLevelConfig.Registers.OTP_SRDAT)

*/
// read/ write register One Time Programmable Memory Control
void _radio_dw1000_register_read_OTP_CTRL( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_OTP_CTRL( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_OTP_CTRL(Config)  \
    Config->LowLevelConfig.Registers.OTP_CTRL.Word = _radio_dw1000_register_read_32bit(Config, rdr_OTP_CTRL)
#define _radio_dw1000_register_write_OTP_CTRL(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_OTP_CTRL, Config->LowLevelConfig.Registers.OTP_CTRL.Word)

// read/ write register One Time Programmable Memory Read Data
void _radio_dw1000_register_read_OTP_SF( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_OTP_SF( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_OTP_SF(Config)  \
    Config->LowLevelConfig.Registers.OTP_SF.Byte = _radio_dw1000_register_read_32bit(Config, rdr_OTP_SF)
#define _radio_dw1000_register_write_OTP_SF(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_OTP_SF, Config->LowLevelConfig.Registers.OTP_SF.Byte)

// read/ write register Leading Edge Detection Threshold Report
void _radio_dw1000_register_read_LDE_THRESH( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_LDE_THRESH( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_LDE_THRESH(Config)  \
    Config->LowLevelConfig.Registers.LDE_THRESH = _radio_dw1000_register_read_16bit(Config, rdr_LDE_THRESH)
#define _radio_dw1000_register_write_LDE_THRESH(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_LDE_THRESH, Config->LowLevelConfig.Registers.LDE_THRESH)


// read/ write register Leading Edge Detection Configuration 1
void _radio_dw1000_register_read_LDE_CFG1( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_LDE_CFG1( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_LDE_CFG1(Config)  \
    Config->LowLevelConfig.Registers.LDE_CFG1.Word = _radio_dw1000_register_read_32bit(Config, rdr_LDE_CFG1)
#define _radio_dw1000_register_write_LDE_CFG1(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_LDE_CFG1, Config->LowLevelConfig.Registers.LDE_CFG1.Word)


// read/ write register Leading Edge Detection Peak Path Index
void _radio_dw1000_register_read_LDE_PPINDX( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_LDE_PPINDX( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_LDE_PPINDX(Config)  \
    Config->LowLevelConfig.Registers.LDE_PPINDX = _radio_dw1000_register_read_16bit(Config, rdr_LDE_PPINDX)
#define _radio_dw1000_register_write_LDE_PPINDX(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_LDE_PPINDX, Config->LowLevelConfig.Registers.LDE_PPINDX)


// read/ write register Leading Edge Detection Peak Path Amplitude
void _radio_dw1000_register_read_LDE_PPAMPL( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_LDE_PPAMPL( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_LDE_PPAMPL(Config)  \
    Config->LowLevelConfig.Registers.LDE_PPAMPL = _radio_dw1000_register_read_16bit(Config, rdr_LDE_PPAMPL)
#define _radio_dw1000_register_write_LDE_PPAMPL(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_LDE_PPAMPL, Config->LowLevelConfig.Registers.LDE_PPAMPL)


// read/ write register Leading Edge Detection Receive Antenna Delay
void _radio_dw1000_register_read_LDE_RXANTD( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_LDE_RXANTD( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_LDE_RXANTD(Config)  \
    Config->LowLevelConfig.Registers.LDE_RXANTD = _radio_dw1000_register_read_16bit(Config, rdr_LDE_RXANTD)
#define _radio_dw1000_register_write_LDE_RXANTD(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_LDE_RXANTD, Config->LowLevelConfig.Registers.LDE_RXANTD)


// read/ write register Leading Edge Detection Configuration 2
void _radio_dw1000_register_read_LDE_CFG2( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_LDE_CFG2( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_LDE_CFG2(Config)  \
    Config->LowLevelConfig.Registers.LDE_CFG2 = _radio_dw1000_register_read_16bit(Config, rdr_LDE_CFG2)
#define _radio_dw1000_register_write_LDE_CFG2(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_LDE_CFG2, Config->LowLevelConfig.Registers.LDE_CFG2)


// read/ write register Leading Edge Detection Control
void _radio_dw1000_register_read_LDE_REPC( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_LDE_REPC( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_LDE_REPC(Config)  \
    Config->LowLevelConfig.Registers.LDE_REPC = _radio_dw1000_register_read_16bit(Config, rdr_LDE_REPC)
#define _radio_dw1000_register_write_LDE_REPC(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_LDE_REPC, Config->LowLevelConfig.Registers.LDE_REPC)


// read/ write register Digital Diagnostics Interface
void _radio_dw1000_register_read_DIG_DIAG( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_DIG_DIAG( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_DIG_DIAG(Config)  \
    Config->LowLevelConfig.Registers.DIG_DIAG.Word = _radio_dw1000_register_read_32bit(Config, rdr_DIG_DIAG)
#define _radio_dw1000_register_write_DIG_DIAG(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_DIG_DIAG, Config->LowLevelConfig.Registers.DIG_DIAG.Word)


// read/ write register Power Management and System Control 0
void _radio_dw1000_register_read_PMSC_CTRL0( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_PMSC_CTRL0( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_PMSC_CTRL0(Config)  \
    Config->LowLevelConfig.Registers.PMSC_CTRL0.Word = _radio_dw1000_register_read_32bit(Config, rdr_PMSC_CTRL0)
#define _radio_dw1000_register_write_PMSC_CTRL0(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_PMSC_CTRL0, Config->LowLevelConfig.Registers.PMSC_CTRL0.Word)


// read/ write register Power Management and System Control 1
void _radio_dw1000_register_read_PMSC_CTRL1( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_PMSC_CTRL1( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_PMSC_CTRL1(Config)  \
    Config->LowLevelConfig.Registers.PMSC_CTRL1.Word = _radio_dw1000_register_read_32bit(Config, rdr_PMSC_CTRL1)
#define _radio_dw1000_register_write_PMSC_CTRL1(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_PMSC_CTRL1, Config->LowLevelConfig.Registers.PMSC_CTRL1.Word)


// read/ write register Power Management and System Control LED Configuration
void _radio_dw1000_register_read_PMSC_LEDC( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_PMSC_LEDC( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_PMSC_LEDC(Config)  \
    Config->LowLevelConfig.Registers.PMSC_LEDC.Word = _radio_dw1000_register_read_32bit(Config, rdr_PMSC_LEDC)
#define _radio_dw1000_register_write_PMSC_LEDC(Config) \
    _radio_dw1000_register_write_32bit(Config, rdr_PMSC_LEDC, Config->LowLevelConfig.Registers.PMSC_LEDC.Word)


// read/ write register PMSC Snooze Time
void _radio_dw1000_register_read_PMSC_SNOZT( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_PMSC_SNOZT( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_PMSC_SNOZT(Config)  \
    Config->LowLevelConfig.Registers.PMSC_SNOZT = _radio_dw1000_register_read_8bit(Config, rdr_PMSC_SNOZT)
#define _radio_dw1000_register_write_PMSC_SNOZT(Config) \
    _radio_dw1000_register_write_8bit(Config, rdr_PMSC_SNOZT, Config->LowLevelConfig.Registers.PMSC_SNOZT)


// read/ write register PMSC fine grain TX sequencing control
void _radio_dw1000_register_read_PMSC_TXFSEQ( t_ttc_radio_config* Config );
void _radio_dw1000_register_write_PMSC_TXFSEQ( t_ttc_radio_config* Config );

#define _radio_dw1000_register_read_PMSC_TXFSEQ(Config)  \
    Config->LowLevelConfig.Registers.PMSC_TXFSEQ = _radio_dw1000_register_read_16bit(Config, rdr_PMSC_TXFSEQ)
#define _radio_dw1000_register_write_PMSC_TXFSEQ(Config) \
    _radio_dw1000_register_write_16bit(Config, rdr_PMSC_TXFSEQ, Config->LowLevelConfig.Registers.PMSC_TXFSEQ)


//}Register Access Macros
//}ConstantDefines
/** { Structure Declarations *******************************************
 *
 * This can be typedef struct or typedef enum declarations.
 * Always prefix structures and enums with filename to avoid double declarations!
 *
 * Examples:

   typedef struct s_radio_dw1000_registers_list {
      struct s_radio_dw1000_registers_list* Next;
      t_base Value;
   } t_radio_dw1000_registers_list;

   typedef enum {
     radio_dw1000_registers_None,
     radio_dw1000_registers_Type1
   } t_radio_dw1000_registersypes_e;

 */

//}StructureDeclarations
/** { Function Declarations ********************************************
 *
 * Declare all functions here that may be called from outside.
 * Rules of Good Code:
 * - document every function declaration
 * - prefix public function names with their filename to avoid double definitions
 */
// { Example declaration
/** calculates the sum of A and B
 *
 * @param A  unsigned integer
 * @param B  unsigned integer
 * @return   A + B
 */
// t_u8 radio_dw1000_registers_calculate(t_u16 A, t_u16 B);
//}


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //radio_dw1000_registers_h
