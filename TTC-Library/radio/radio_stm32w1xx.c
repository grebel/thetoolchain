/** { radio_stm32w1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for radio devices on stm32w1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 21 at 20140514 04:32:30 UTC
 *
 *  Note: See ttc_radio.h for description of stm32w1xx independent RADIO implementation.
 * 
 *  Authors: Gregor Rebel
}*/

#error This low-level radio driver is currently not working due to reconstruction of ttc_radio. ToDo: re-implement for new ttc_radio API!

//{ Includes *******************************************************************

#include "radio_stm32w1xx.h"
#include "micro-common.h" // must be included before phy-library.h
#include "phy-library.h"
#include "error.h"
#include "../ttc_basic.h"
#include "../ttc_heap.h"
#include "../ttc_memory.h"
#include "../ttc_interrupt.h"
#include "../ttc_list.h"
#include "../ttc_gpio.h"


//}

void ttc_radio_receive_callback_isr(t_ttc_radio_config* Config, t_u8 *Packet, BOOL AckFramePendingSet, t_u32 Time, t_u16 Errors, t_s8 RSSI);

//{ Global Variables ***********************************************************

// Configuration variable required to use ST_RadioTransmit()
RadioTransmitConfig radioTransmitConfig = {
    .waitForAck         = 0,
    .checkCca           = 0,
    .ccaAttemptMax      = 0,
    .backoffExponentMin = RADIO_BACKOFF_EXPONENT_MIN_DEFAULT,
    .backoffExponentMax = RADIO_BACKOFF_EXPONENT_MIN_DEFAULT,
    .minimumBackoff     = 0,
    .appendCrc          = TRUE
};

// signal between transmit isr and application
volatile BOOL TransmitComplete = TRUE;

 t_ttc_radio_features const radio_stm32w1xx_Features = {
     .DefaultTxChannel = ST_MIN_802_15_4_CHANNEL_NUMBER,
     .DefaultRxChannel = ST_MIN_802_15_4_CHANNEL_NUMBER,
     .DefaultTxLevel = 3,
     .DefaultRxLevel = 0,
     .DefaultBitRate = 250000,
     .MaxBitRate = 10,
     .MaxChannel = ST_MAX_802_15_4_CHANNEL_NUMBER,
     .MaxTxLevel = MAX_RADIO_POWER,
     .MaxRxLevel = 0,
     .MinChannel = ST_MIN_802_15_4_CHANNEL_NUMBER,
     .MinTxLevel = MIN_RADIO_POWER,
     .MinBitRate = 0,
     .MinRxLevel = 0
};

// there can only be one radio so it's OK to store Configuation in global pointer
t_ttc_radio_config* radio_stm32w1xx_Config;

//}Global Variables
//{ Function Definitions *******************************************************

                 void radio_stm32w1xx_prepare() {




}
e_ttc_radio_errorcode radio_stm32w1xx_deinit(t_ttc_radio_config* Config) {
    Assert_RADIO(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning ToDO: implement radio_stm32w1xx_deinit()
    
    return (e_ttc_radio_errorcode) 0;
}
e_ttc_radio_errorcode radio_stm32w1xx_configuration_check(t_ttc_radio_config* Config) {
    Assert_RADIO(Config, ttc_assert_origin_auto); // pointers must not be NULL

    Config->Init.Flags.DelayedTransmits      = 0; // ToDo: implement
    Config->Init.Flags.EnableDoubleBuffering = 0;
    // ...
}
e_ttc_radio_errorcode radio_stm32w1xx_init(t_ttc_radio_config* Config) {
    Assert_RADIO(Config, ttc_assert_origin_auto); // pointers must not be NULL

    radio_stm32w1xx_Config = Config;

    Config->LowLevelConfig.Amount_Recalibrations = 0;
    t_u32 Seed;
    //StStatus Status;
    ST_RadioGetRandomNumbers( (t_u16*) &Seed, 2);
    halCommonSeedRandom(Seed);
    radioTransmitConfig.appendCrc  = Config->Init.Flags.AppendCRC;
    radioTransmitConfig.waitForAck = Config->Init.Flags.AutoAcknowledge;

    Assert_RADIO(ST_RadioInit(ST_RADIO_POWER_MODE_RX_ON) ==  ST_SUCCESS, ttc_assert_origin_auto);

    if (Config->TxLevel == Config->Features->MaxTxLevel) {
        // This radio
        ST_RadioSetPowerMode( Config->Init.Flags.UseAlternateTxPath << 1 |
                              1
                              );
    }
    else {
        ST_RadioSetPowerMode( Config->Init.Flags.UseAlternateTxPath << 1 |
                              0
                              );
    }
    if (Config->Init.Flags.UseAlternateTxPath && Config->Init.Pin_TxActive) {
        ttc_gpio_init(Config->Init.Pin_TxActive, E_ttc_gpio_mode_alternate_function_special_sclk, E_ttc_gpio_speed_min);
    }

    ST_RadioSetPower(Config->TxLevel);

    ST_RadioEnableAddressFiltering(Config->Init.Flags.AddressFilter);
    ST_RadioSetNodeId(Config->NodeID);
    ST_RadioSetPanId(Config->PanID);
    ST_RadioEnableReceiveCrc(radioTransmitConfig.appendCrc);
    ST_RadioEnableAutoAck(Config->Init.Flags.AutoAcknowledge);

    if (1) { // be sure that we receive everything
        ST_RadioEnableAddressFiltering(FALSE);
        ST_RadioEnableAutoAck(FALSE);
    }

    if (1) { INTERRUPTS_ON(); }
    else   { ttc_interrupt_all_enable(); }
    return (e_ttc_radio_errorcode) 0;
}
e_ttc_radio_errorcode radio_stm32w1xx_load_defaults(t_ttc_radio_config* Config) {
    Assert_RADIO(Config, ttc_assert_origin_auto); // pointers must not be NULL

#if TTC_RADIO1_ALTERNATE_TX_PATH==1
    // this pin is configured and controlled by low-level driver only
    Config->Init.Flags.PinTxActive_LowLevel = 1;
#endif

    return (e_ttc_radio_errorcode) 0;
}
e_ttc_radio_errorcode radio_stm32w1xx_reset(t_ttc_radio_config* Config) {
    Assert_RADIO(Config, ttc_assert_origin_auto); // pointers must not be NULL

    if (Config->Flags.Bits.Initialized) {
        radio_stm32w1xx_deinit(Config);
        ttc_memory_set(&(Config->LowLevelConfig), 0, sizeof(Config->LowLevelConfig));
        Config->Flags.Bits.Initialized = 0;
    }
    return (e_ttc_radio_errorcode) 0;
}
e_ttc_radio_errorcode radio_stm32w1xx_transmit(t_ttc_radio_config* Config, ttc_radio_packet_t* Packet) {
    Assert_RADIO(Config, ttc_assert_origin_auto); // pointers must not be NULL

    if (0) {
        ST_RadioWake();

        if (ST_RadioCheckRadio()) { // ToDo: make it a periodically task during off time
            Config->LowLevelConfig.Amount_Recalibrations++;
            ST_RadioCalibrateCurrentChannel();
        }
    }
    if (0) { // test mode
        static t_u8 TestBuffer[14] = { 0x0f, 0x21, 0x08, 0x00, 0x12, 0x23, 0x02, 0x22, 0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc };
        TransmitComplete = FALSE;
        if ( ST_RadioTransmit(TestBuffer) == ST_SUCCESS) {
            while (!TransmitComplete);
            return ec_radio_OK;
        }
    }
    else {
        while (!TransmitComplete); // previous transmit still in progress
        TransmitComplete = FALSE;
        if (ST_RadioTransmit( (t_u8*) & (Packet->Data) ) == ST_SUCCESS) {
            while (!TransmitComplete);
            return (e_ttc_radio_errorcode) 0;
        }

        TransmitComplete = TRUE;
    }

    return ec_radio_Busy; // packet could not be sent
}
e_ttc_radio_errorcode radio_stm32w1xx_configure_channel(t_ttc_radio_config* Config, t_u8 NewChannel) {
    Assert_RADIO(Config, ttc_assert_origin_auto); // pointers must not be NULL

    ST_RadioSetChannel(NewChannel);
    NewChannel = ST_RadioGetChannel();

    Config->TxChannel = NewChannel;
    Config->RxChannel = NewChannel;

    return (e_ttc_radio_errorcode) 0;
}
                 void radio_stm32w1xx_maintenance(t_ttc_radio_config* Config) {
    Assert_RADIO(Config, ttc_assert_origin_auto); // pointers must not be NULL

    if (ST_RadioCheckRadio()) { // ToDo: make it a periodically task during off time
      ST_RadioCalibrateCurrentChannel();
    }
    halCommonCheckXtalBiasTrim();
    halCommonCalibratePads();
}
e_ttc_radio_errorcode radio_stm32w1xx_configure_channel_rx(t_ttc_radio_config* Config, t_u8 NewChannel) {
    Assert_RADIO(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function radio_stm32w1xx_configure_channel_rx() is empty.
    
    return (e_ttc_radio_errorcode) 0;
}
e_ttc_radio_errorcode radio_stm32w1xx_configure_channel_tx(t_ttc_radio_config* Config, t_u8 NewChannel) {
    Assert_RADIO(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function radio_stm32w1xx_configure_channel_tx() is empty.
    
    return (e_ttc_radio_errorcode) 0;
}
void radio_stm32w1xx_isr_receive(t_physical_index Index, void* Config) {
    Assert_RADIO_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function radio_stm32w1xx_isr_receive() is empty.
    

}
void radio_stm32w1xx_change_local_address(t_ttc_radio_config* Config, t_ttc_packet_address* LocalAddress, t_u16* LocalPanID) {
    Assert_RADIO_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_RADIO_EXTRA(ttc_memory_is_writable(LocalAddress), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_RADIO_EXTRA(ttc_memory_is_writable(LocalPanID), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function radio_stm32w1xx_change_local_address() is empty.
    

}
u_ttc_radio_frame_filter radio_stm32w1xx_configure_frame_filter(t_ttc_radio_config* Config, u_ttc_radio_frame_filter Settings) {
    Assert_RADIO_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function radio_stm32w1xx_configure_frame_filter() is empty.
    
    return (u_ttc_radio_frame_filter) 0;
}
e_ttc_radio_errorcode radio_stm32w1xx_configure_channel_rxtx(t_ttc_radio_config* Config, t_u8 NewChannel) {
    Assert_RADIO_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function radio_stm32w1xx_configure_channel_rxtx() is empty.
    
    return (e_ttc_radio_errorcode) 0;
}
void radio_stm32w1xx_change_delay_rx2tx(t_ttc_radio_config* Config, t_base Delay_uSeconds) {
    Assert_RADIO_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function radio_stm32w1xx_change_delay_rx2tx() is empty.
    

}
void radio_stm32w1xx_change_delay_tx2rx(t_ttc_radio_config* Config, t_base Delay_uSeconds) {
    Assert_RADIO_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function radio_stm32w1xx_change_delay_tx2rx() is empty.
    

}
BOOL radio_stm32w1xx_update_meta(t_ttc_radio_config* Config, t_ttc_packet* Packet) {
    Assert_RADIO_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_RADIO_EXTRA(ttc_memory_is_writable(Packet), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function radio_stm32w1xx_update_meta() is empty.
    
    return (BOOL) 0;
}
void radio_stm32w1xx_change_delay_time(t_ttc_radio_config* Config, t_u16 Delay_uSeconds) {
    Assert_RADIO_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function radio_stm32w1xx_change_delay_time() is empty.
    

}
void radio_stm32w1xx_isr_transmit(t_physical_index Index, t_ttc_radio_config* Config) {
    Assert_RADIO_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function radio_stm32w1xx_isr_transmit() is empty.
    

}
void radio_stm32w1xx_receiver(t_ttc_radio_config* Config, BOOL Enable, ttc_radio_timestamp_u* ReferenceTime) {
    Assert_RADIO_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_RADIO_EXTRA(ttc_memory_is_writable(ReferenceTime), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function radio_stm32w1xx_receiver() is empty.
    

}
void radio_stm32w1xx_time_add_ns(u_ttc_packetimestamp_40* Time, t_u32 Delay_ns, u_ttc_packetimestamp_40* TimeDelayed) {
    Assert_RADIO_EXTRA(ttc_memory_is_writable(Time), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)
    Assert_RADIO_EXTRA(ttc_memory_is_writable(TimeDelayed), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function radio_stm32w1xx_time_add_ns() is empty.
    

}
BOOL radio_stm32w1xx_configure_auto_acknowledge(t_ttc_radio_config* Config, t_u8 UserID, BOOL Enable, BOOL Change) {
    Assert_RADIO_EXTRA_Writable(Config, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function radio_stm32w1xx_configure_auto_acknowledge() is empty.
    
    return (BOOL) 0;
}
BOOL radio_stm32w1xx_gpio_out(t_ttc_radio_config* Config, e_ttc_radio_gpio Pin, BOOL NewState) {
    Assert_RADIO_EXTRA_Writable(Config, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function radio_stm32w1xx_gpio_out() is empty.
    
    TTC_TASK_RETURN ( 0 );  // will perform stack overflow check and return given value
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

// ST SimpleMAC callback functions
void ST_RadioTransmitCompleteIsrCallback(StStatus Status, int32u SentTime, bool FramePending) {
    (void) SentTime;
    (void) FramePending;

    switch (Status) {
    case ST_SUCCESS:
        TransmitComplete = TRUE;
        radio_stm32w1xx_Config->Statistics.Amount_Tx++;
        break;
    default:
        break;
    }
}
boolean ST_RadioDataPendingShortIdIsrCallback(ut_int16 shortId) {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
    return TRUE;
}
boolean ST_RadioDataPendingLongIdIsrCallback(t_u8* longId) {
    ttc_assert_halt_origin(ttc_assert_origin_auto);

    return TRUE;
}
void ST_RadioReceiveIsrCallback(t_u8 *Packet, BOOL AckFramePendingSet, int32u Time, int16u Errors, int8s RSSI) {
    // note this is executed from interrupt context

    ttc_radio_receive_callback_isr(radio_stm32w1xx_Config, Packet, AckFramePendingSet, Time, Errors, RSSI);
}
void ST_RadioOverflowIsrCallback(void) {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void ST_RadioSfdSentIsrCallback(int32u sfdSentTime) {

    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void ST_RadioMacTimerCompareIsrCallback(void) {

    ttc_assert_halt_origin(ttc_assert_origin_auto);
}

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
