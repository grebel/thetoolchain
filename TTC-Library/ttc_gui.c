/*{ ttc_gui.c *******************************



}*/

#include "ttc_gui.h"

//{ Global Variables *****************************************************
t_gui_keyboard*   MyKeyboard         = NULL;
t_ttc_gui_number_area* NumberAreas_Used   = NULL;
t_ttc_heap_pool*  NumberMemory_block = NULL;

extern t_tit_inputarea *InputArea_Current;
// for each initialized input, a pointer to its generic and architecture definitions is stored

//} Global Variables

// functions that can be provided by low-level driver (set during ttc_gfx_reset() )


//{ Function definitions *************************************************

void ttc_gui_create_push_button(void (*TouchHandler)(void* Argument), void* Argument, t_u16 Left, t_u16 Top, t_u16 Right, t_u16 Bottom, const char* Text) {
    t_u8 Dist=(Right-Left-strlen(Text)*16)/2;
    ttc_gfx_text_between(Left+Dist,Top+(Bottom-Top)/2-10,Text,15);
    ttc_gfx_rect(Left,Top,Right-Left,Bottom-Top);
    ttc_task_msleep(50);
    ttc_input_register_input_area(TouchHandler,Argument,Left,Top,Right,Bottom,Text,gss_Rounded,me_MouseMove);
}
void ttc_gui_clear() {
    ttc_gfx_clear();
    ttc_input_delete_all_input_areas();
    ttc_gui_delete_all_number_areas();
}
void ttc_gui_plus_button(t_u16 Left, t_u16 Top, t_u16 Right, t_u16 Bottom, void* value) {
    ttc_gfx_rect(Left,Top,Right-Left,Bottom-Top);
    ttc_gfx_rect_fill(Left+(Right-Left)/8,Top+(Bottom-Top)/3+1,Right-Left-(Right-Left)/4,(Bottom-Top)/3); //draw vertikal square
    ttc_gfx_rect_fill(Left+(Right-Left)/3+1,Top+(Bottom-Top)/8,(Right-Left)/3,Bottom-Top-(Bottom-Top)/4); //draw horizontal square
    ttc_task_msleep(50);
    ttc_input_register_input_area(_ttc_add_one,(void*)value,Left,Top,Right,Bottom,NULL,gss_Rounded,me_MouseMove);
}
void ttc_gui_minus_button(t_u16 Left, t_u16 Top, t_u16 Right, t_u16 Bottom, void* value) {

    ttc_gfx_rect(Left,Top,Right-Left,Bottom-Top);
    ttc_gfx_rect_fill(Left+(Right-Left)/8,Top+(Bottom-Top)/3+1,Right-Left-(Right-Left)/4,(Bottom-Top)/3);
    ttc_task_msleep(50);
    ttc_input_register_input_area(_ttc_sub_one,(void*)value,Left,Top,Right,Bottom,NULL,gss_Rounded,me_MouseMove);
}
void ttc_gui_create_number_field(t_u16 X,t_u16 Y,t_u16* number, t_base Size) {
    t_ttc_gui_number_area* Number_Data;

    if(!NumberMemory_block)
    {
        NumberMemory_block = ttc_heap_pool_create(sizeof(t_ttc_gui_number_area),20);

    }

    if(!NumberAreas_Used) //if Input area list is empty
    {
        Number_Data = NumberAreas_Used = (t_ttc_gui_number_area*) ttc_heap_pool_block_get(NumberMemory_block); //the first pointer get the address of allocated memory
        Number_Data->X=X;
        Number_Data->Y=Y;
        Number_Data->DecPoint=0;
        Number_Data->number=number;
        Number_Data->Size=Size;
        Number_Data->Next=0;
    }
    else //if there is already a first pointer
    {
        Number_Data = NumberAreas_Used; //hand the adress over to Number_Data
        while(Number_Data->Next)
            Number_Data=Number_Data->Next; //while there is a next pointer, Number_Data become the next pointer
        //at the end of the list
        Number_Data->Next = (t_ttc_gui_number_area*) ttc_heap_pool_block_get(NumberMemory_block); //link old part with the new memory space
        Number_Data=Number_Data->Next; //the current pointer is the new pointer now
        Number_Data->X=X; //defines X to new struct
        Number_Data->Y=Y; //defines Y to new struct
        Number_Data->DecPoint=0; //defines Y to new struct
        Number_Data->number=number; //hand over the number pointer
        Number_Data->Next=0;
        Number_Data->Size=Size;
    }
    ttc_gui_number_refresh();

}
t_ttc_gui_number_area* ttc_gui_create_number_field_floating(t_u16 X,t_u16 Y,t_u16* number,t_u8 DecDigit, t_base Size) {
    t_ttc_gui_number_area* Number_Data;

    if(!NumberMemory_block)
    {
        NumberMemory_block = ttc_heap_pool_create(sizeof(t_ttc_gui_number_area),20);

    }

    if(!NumberAreas_Used) //if Input area list is empty
    {
        Number_Data = NumberAreas_Used = (t_ttc_gui_number_area*) ttc_heap_pool_block_get(NumberMemory_block); //the first pointer get the address of allocated memory
        Number_Data->X=X;
        Number_Data->Y=Y;
        Number_Data->DecPoint=DecDigit;
        Number_Data->number=number;
        Number_Data->Next=0;
        Number_Data->Size=Size;
    }
    else //if there is already a first pointer
    {
        Number_Data = NumberAreas_Used; //hand the adress over to Number_Data
        while(Number_Data->Next)
            Number_Data=Number_Data->Next; //while there is a next pointer, Number_Data become the next pointer
        //at the end of the list
        Number_Data->Next = (t_ttc_gui_number_area*) ttc_heap_pool_block_get(NumberMemory_block); //link old part with the new memory space
        Number_Data=Number_Data->Next; //the current pointer is the new pointer now
        Number_Data->X=X; //defines X to new struct
        Number_Data->Y=Y; //defines Y to new struct
        Number_Data->DecPoint=DecDigit;
        Number_Data->number=number; //hand over the number pointer
        Number_Data->Next=0;
        Number_Data->Size=Size;
    }
    ttc_gui_number_refresh();
    return Number_Data;

}
void ttc_gui_delete_all_number_areas() {
    t_ttc_gui_number_area* Number_Data= NumberAreas_Used;
    while(Number_Data)
    {
        NumberAreas_Used=Number_Data->Next;
        ttc_heap_pool_block_free((t_ttc_heap_block_from_pool*) Number_Data);
        Number_Data=NumberAreas_Used;
    }

    return;

}
void ttc_gui_number_refresh() {
    char Buffer[7];
    t_u8 i =0;
    t_ttc_gui_number_area* Number_Data= NumberAreas_Used;

    for(Number_Data= NumberAreas_Used;Number_Data;Number_Data=Number_Data->Next)
    {
        ttc_gfx_text_cursor_set(Number_Data->X,Number_Data->Y);
        ttc_string_snprintf((t_u8*)&Buffer,Number_Data->Size+1,"%i",*(Number_Data->number));
        for(i =0;Buffer[i]!='\0'&&i!=7;i++);
        ttc_gfx_text_solid((const char*)&Buffer,Number_Data->Size+1);
        if(Number_Data->OldNumber>(*Number_Data->number))
            ttc_gfx_char_solid(' ');
        Number_Data->OldNumber=*Number_Data->number;
        if(Number_Data->DecPoint){
            ttc_gfx_rect_fill((Number_Data->X+i-1-Number_Data->DecPoint)*16+14,(Number_Data->Y*24+16),2,2);
            ttc_gfx_color_fg24(TTC_GFX_COLOR24_WHITE);
            ttc_gfx_rect_fill((Number_Data->X+i-2-Number_Data->DecPoint)*16+14,(Number_Data->Y*24+16),2,2);
            ttc_gfx_color_fg24(TTC_GFX_COLOR24_RED);
        }

    }

}
void _ttc_add_one(void* data) {
    Assert_GFX(data, ttc_assert_origin_auto);
    Assert_GFX(ttc_memory_is_writable(data), ttc_assert_origin_auto);

    t_u8 i=0;
    t_ttc_gui_number_area* Number_Data=(t_ttc_gui_number_area*)data;
    Assert_GFX(ttc_memory_is_writable(Number_Data), ttc_assert_origin_auto);
    Assert_GFX(ttc_memory_is_writable(Number_Data->number), ttc_assert_origin_auto);
    t_u16 value= *Number_Data->number;
    for(i=0;value;value=(value+1)/10,i++); // ToDo PaPo: Was macht das?

    value= *Number_Data->number;
    if(i<=Number_Data->Size)
        (*Number_Data->number)++;
    ttc_gui_number_refresh();
    ttc_task_msleep(ADD_DELAY);
}
void _ttc_sub_one(void* data) {
    Assert_GFX(data, ttc_assert_origin_auto);
    Assert_GFX(ttc_memory_is_writable(data), ttc_assert_origin_auto);

    t_ttc_gui_number_area* Number_Data=(t_ttc_gui_number_area*)data;
    t_u16* value= Number_Data->number;
    Assert_GFX(ttc_memory_is_writable(value), ttc_assert_origin_auto);

    if (*value>0){
        *value=-1+*value;
        ttc_gui_number_refresh();
        ttc_task_msleep(SUB_DELAY);
    }
}
void ttc_gui_keyboard_1_9(void (*Source)(void* Argument), void (*Destination)(void* Argument),char Password[TTC_GUI_MAX_KEYBOARD_RETURN]) {
    if(!MyKeyboard)
    {
        MyKeyboard = (t_gui_keyboard*) ttc_heap_alloc(sizeof(t_gui_keyboard));
    }
    MyKeyboard->Destination=Destination;
    MyKeyboard->Source=Source;
    void* data = (void*) MyKeyboard;
    t_u16 Height = 320;
    t_u16 Width = 240;
    ttc_gui_clear();
    MyKeyboard->Counter=0;

    for(t_u8 i =0; i<TTC_GUI_MAX_KEYBOARD_RETURN;i++){
        MyKeyboard->Return[i]=0;
        MyKeyboard->Password[i]=Password[i];
    }
    ttc_gfx_text_solid_at(3,0,"Passwort",15);
    ttc_gui_create_push_button(&_ttc_gui_keyboard_add,data,0,(Height/6)*2,(Width/3),(Height/6)*3,"7");
    ttc_gui_create_push_button(&_ttc_gui_keyboard_add,data,0,((Height/6)*3),(Width/3),((Height/6)*4),"4");
    ttc_gui_create_push_button(&_ttc_gui_keyboard_add,data,0,((Height/6)*4),(Width/3),((Height/6)*5),"1");
    ttc_gui_create_push_button(&_ttc_gui_keyboard_add,data,0,(Height/6)*5,(Width/3),(Height/6)*6,"0");
    ttc_gui_create_push_button(&_ttc_gui_keyboard_add,data,(Width/3),(Height/6)*2,(Width/3)*2,(Height/6)*3,"8");
    ttc_gui_create_push_button(&_ttc_gui_keyboard_add,data,(Width/3),(Height/6)*3,(Width/3)*2,(Height/6)*4,"5");
    ttc_gui_create_push_button(&_ttc_gui_keyboard_add,data,(Width/3),(Height/6)*4,(Width/3)*2,(Height/6)*5,"2");
    ttc_gui_create_push_button(&_ttc_gui_keyboard_add,data,(Width/3),(Height/6)*5,(Width/3)*2,(Height/6)*6,"OK");
    ttc_gui_create_push_button(&_ttc_gui_keyboard_add,data,(Width/3)*2,(Height/6)*2,(Width/3)*3,(Height/6)*3,"9");
    ttc_gui_create_push_button(&_ttc_gui_keyboard_add,data,(Width/3)*2,(Height/6)*3,(Width/3)*3,(Height/6)*4,"6");
    ttc_gui_create_push_button(&_ttc_gui_keyboard_add,data,(Width/3)*2,(Height/6)*4,(Width/3)*3,(Height/6)*5,"3");
    ttc_gui_create_push_button(&_ttc_gui_keyboard_add,data,(Width/3)*2,(Height/6)*5,(Width/3)*3,(Height/6)*6,"C");


}
void _ttc_gui_keyboard_add(void* data) {

    t_gui_keyboard* my_keyboard = (t_gui_keyboard*) data;
    BOOL check=1;
    if(InputArea_Current->Text[0]=='O')
    {
        for(t_u8 i =0; i<TTC_GUI_MAX_KEYBOARD_RETURN;i++) check&= MyKeyboard->Return[i]==MyKeyboard->Password[i];
        if(check){
            my_keyboard->Destination((void*)my_keyboard->Return);
        }
        else{
            ttc_gui_keyboard_1_9(my_keyboard->Source,my_keyboard->Destination,my_keyboard->Password);
            return;
        }
    }

    if(InputArea_Current->Text[0]=='C')
    {
        if(my_keyboard->Counter!=0){
            ttc_gui_keyboard_1_9(my_keyboard->Source,my_keyboard->Destination,my_keyboard->Password);
            return;
        }
        else
        {
            my_keyboard->Source(NULL);
        }
    }

    if(TTC_GUI_MAX_KEYBOARD_RETURN<=my_keyboard->Counter)
        return;

    switch(InputArea_Current->Text[0]){
    case '0':my_keyboard->Return[my_keyboard->Counter]='0';
        ttc_gfx_text_solid_at(((my_keyboard->Counter)+5),2,"0",3);
        break;
    case '1':my_keyboard->Return[my_keyboard->Counter]='1';
        ttc_gfx_text_solid_at(((my_keyboard->Counter)+5),2,"1",3);
        break;
    case '2':my_keyboard->Return[my_keyboard->Counter]='2';
        ttc_gfx_text_solid_at(((my_keyboard->Counter)+5),2,"2",3);
        break;
    case '3':my_keyboard->Return[my_keyboard->Counter]='3';
        ttc_gfx_text_solid_at(((my_keyboard->Counter)+5),2,"3",3);
        break;
    case '4':my_keyboard->Return[my_keyboard->Counter]='4';
        ttc_gfx_text_solid_at(((my_keyboard->Counter)+5),2,"4",3);
        break;
    case '5':my_keyboard->Return[my_keyboard->Counter]='5';
        ttc_gfx_text_solid_at(((my_keyboard->Counter)+5),2,"5",3);
        break;
    case '6':my_keyboard->Return[my_keyboard->Counter]='6';
        ttc_gfx_text_solid_at(((my_keyboard->Counter)+5),2,"6",3);
        break;
    case '7':my_keyboard->Return[my_keyboard->Counter]='7';
        ttc_gfx_text_solid_at(((my_keyboard->Counter)+5),2,"7",3);
        break;
    case '8':my_keyboard->Return[my_keyboard->Counter]='8';
        ttc_gfx_text_solid_at(((my_keyboard->Counter)+5),2,"8",3);
        break;
    case '9':my_keyboard->Return[my_keyboard->Counter]='9';
        ttc_gfx_text_solid_at(((my_keyboard->Counter)+5),2,"9",3);
        break;
    default:
        break;
    }
    ttc_task_msleep(500);
    (my_keyboard->Counter)++;
}
void ttc_gui_create_input_line(t_u8 X, t_u8 Y,t_u8 XNumber, const char* Text, t_u16* Variable, t_u8 DecPoint, t_base Size) {
    t_ttc_gui_number_area* NumberData;
    ttc_gfx_text_cursor_set(X,Y);
    ttc_gfx_text(Text,15);
    NumberData = ttc_gui_create_number_field_floating(XNumber,Y,Variable,DecPoint,Size);

    ttc_gui_plus_button(187,23*(Y),213,23*(Y)+26,NumberData);
    ttc_gui_minus_button(214,23*(Y),240,23*(Y)+26,NumberData);
}

//}FunctionDefinitions


