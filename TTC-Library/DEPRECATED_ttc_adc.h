#ifndef TTC_ADC_H
#define TTC_ADC_H

/** { ttc_adc.h ***********************************************
 *
 * Written by Patrick von Poblotzki 2013
 *
 * Analog-Digital-Converter functions
 *
}*/

//{ includes

#include "ttc_basic.h"

#ifdef TARGET_ARCHITECTURE_STM32F1xx
#include "stm32/stm32_adc.h"
#endif


//}includes
//{ constants

//}
//{ external datatypes ***********************************************

//}external datatypes
//{ Function prototypes **************************************************
void ttc_adc_init(ADC_TypeDef* ADCx, BOOL WithDMA);
#ifdef _driver_ttc_adc_init
#define ttc_adc_init(ADCx,WithDMA) _driver_ttc_adc_init(ADCx,WithDMA);
#else
#error no ttc_adc_read_single_ch defined, please check whether your architecture is supported
#endif

void ttc_adc_init_dma(ADC_TypeDef* ADCx, u8_t AdcChannel, volatile u16_t* Value);
#ifdef _driver_ttc_adc_init_dma
#define  ttc_adc_init_dma( ADCx, AdcChannel, Value)  _driver_ttc_adc_init_dma(ADCx,AdcChannel,Value);
#else
#error no ttc_adc_read_single_ch defined, please check whether your architecture is supported
#endif

unsigned int ttc_adc_read_single_ch(ADC_TypeDef* ADCx, unsigned char Channel);
#ifdef _driver_ttc_adc_read_single_ch
#define ttc_adc_read_single_ch(ADCx,Channel) _driver_ttc_adc_read_single_ch(ADCx,Channel);
#else
#error no ttc_adc_read_single_ch defined, please check whether your architecture is supported
#endif

void ttc_adc_init_single_mode(void);
#ifdef _driver_ttc_adc_init_single
#define ttc_adc_init_single_mode(ADC) _driver_ttc_adc_init_single(ADC);
#else
#error no ttc_adc_init_single defined, please check whether your architecture is supported
#endif

void ttc_adc_activate_analog_single(ttc_gpio_bank_t Bank, u8_t Pin);
#ifdef _driver_ttc_adc_activate_analog_single
#define ttc_adc_activate_analog_single(Bank,Pin) _driver_ttc_adc_activate_analog_single(Bank,Pin);
#else
#error no ttc_adc_activate_analog_single defined, please check whether your architecture is supported
#endif

volatile u16_t ttc_adc_get_adc_value(u8_t Channel, ADC_TypeDef* ADC);
#ifdef _driver_ttc_adc_get_adc_value
#define ttc_adc_get_adc_value(Channel, ADC) _driver_ttc_adc_get_adc_value(Channel, ADC);
#else
#error no ttc_adc_get_adc_value defined, please check whether your architecture is supported
#endif

#endif // TTC_ADC_H
