#ifndef ttc_semaphore_types_H
#define ttc_semaphore_types_H

/*{ ttc_semaphore_types.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Architecture independent support for inter task communication & synchronization.
 *
 * Currently supported architectures/ schedulers: CortexM3, FreeRTOS
 *
}*/

//{ includes

#include "ttc_basic_types.h"
#include "ttc_task_types.h"

#ifdef EXTENSION_ttc_semaphore_cortexm3
    #include "cpu/cm3_semaphore_types.h"
    #define EXTENSION_ttc_semaphore_driver 1
#endif

#ifdef EXTENSION_ttc_semaphore_cortexm0
    #include "cm0/cm0_semaphore_types.h"
    #define EXTENSION_ttc_semaphore_driver 1
#endif

#ifdef EXTENSION_ttc_semaphore_freertos
    #include "scheduler/freertos_semaphore_types.h"
    #define EXTENSION_ttc_semaphore_driver 1
#endif

#ifndef EXTENSION_ttc_semaphore_driver
    #  error Missing low-level driver for ttc_semaphore! Did you forget to activate a low-level driver?
#endif


typedef enum ttc_semaphore_error_E {  // return type of most functions in ttc_semaphore
    tsme_OK,                           // no error
    tsme_WasAccessedByTask,            // an _isr()-call occured while Semaphore was accessed by a task

    tsme_Error,
    // Error Conditions below
    tsme_TimeOut,                      // an operation has timed out
    tsme_NotImplemented,               // required function not implemented for current architecture
    tsme_SemaphoreExpired,             // tried to take from semaphore that cannot provide given amount
    tsme_SemaphoreFull,                // tried to give to semaphore that has reached its limit (if supported)
    tsme_SemaphoreInterrupted,         // semaphore operation was interrupted by another task (retry your action)
    tsme_UNKNOWN
} e_ttc_semaphore_error;

//}includes
//{ Default settings (change via makefile) ******************************


#ifndef TTC_SEMAPHORE_TIMEOUT_USECS
    //#define TTC_SEMAPHORE_TIMEOUT_USECS 1000000 // >0: activate timeout-assert for endless semaphores (TimeOut==-1) in usecs
    #define TTC_SEMAPHORE_TIMEOUT_USECS 0    // no forced timeout
#endif

// enable use of Assert() for semaphores
#ifndef TTC_ASSERT_SEMAPHORE
    #define TTC_ASSERT_SEMAPHORE 1
    #define SEMAPHORE_VOLATILE volatile
#else
    #define SEMAPHORE_VOLATILE
#endif

//}Default settings
//{ Types & Structures ***************************************************

#if TTC_ASSERT_SEMAPHORE == 1 // use Assert()s in Queue code (somewhat slower but alot easier to debug)
    #define Assert_SEMAPHORE(Condition, Origin) Assert( ((Condition) != 0), Origin)
    #define Assert_SEMAPHORE_Writable(Address, Origin) Assert_Writable(Address, Origin)
#else  // us no Assert()s in USART code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_SEMAPHORE(Condition, Origin)
    #define Assert_SEMAPHORE_Writable(Address, Origin)
#endif

// enable use of smart semaphores
#ifndef TTC_SMART_SEMAPHORE
    #define TTC_SMART_SEMAPHORE 1
#endif

// define amount
#if TTC_SMART_SEMAPHORE == 1

    // set amount of entries to be stored in history of smart semaphore usage (debugging only)
    #ifndef TTC_SMART_SEMAPHORE_HISTORY_SIZE
        #define TTC_SMART_SEMAPHORE_HISTORY_SIZE 0
    #endif

#endif

/** data used to manage a single semaphore */
#ifndef t_driver_semaphore
    #  error Missing definition for t_driver_semaphore (Should be defined by low-level driver)!
#endif
typedef struct {
    t_driver_semaphore  Value __attribute__( ( aligned( 4 ) ) ); // low-level data storing value (defined in low-level driver: cm3_semaphore.h, ...)
#if TARGET_DATA_WIDTH==32 // pad bytes for 32 bit architectures
#if CM3_SEMAPHORE_SIZE == 8
    t_u8 PadByte1;
    t_u8 PadByte2;
    t_u8 PadByte3;
#endif
#if CM3_SEMAPHORE_SIZE == 16
    t_u8 PadByte1;
    t_u8 PadByte2;
#endif
#endif
#if TARGET_DATA_WIDTH==16 // pad bytes for 16 bit architectures
#if CM3_SEMAPHORE_SIZE == 8
    t_u8 PadByte1;
#endif
#endif

#ifdef TTC_MULTITASKING_SCHEDULER
    t_ttc_task_waiting_list WaitingTasks; // list of tasks waiting on this semaphore
#endif
} t_ttc_semaphore;

// debuggable semaphore that can store its owner
// This comes handy if you wanna know, who has taken a semaphore before.
#if TTC_SMART_SEMAPHORE == 1
typedef struct s_ttc_semaphore_smart {
    t_ttc_semaphore Semaphore;

    // points to function that last called a ttc_semaphore_*() function
    void ( *LastCaller )();

#ifdef TTC_REGRESSION
    // task info of owner
    volatile t_ttc_task_info* LastCallerTask;
#endif
} t_ttc_semaphore_smart;
#else
typedef t_ttc_semaphore t_ttc_semaphore_smart;
#endif

//}

/** Central waiting for semaphores. Puts task to sleep while waiting.
  *
  * Note: This function is used by low-level driver to wait on a semaphore in a high-level manner
  *
  *
  * @param Semaphore  semaphore struct to wait on
  * @param TimeOut    ==0: return immediately, == -1: wait endles, >0: amount of microseconds to wait maximum
  */
void ttc_semaphore_wait( t_ttc_semaphore* Semaphore, t_base TimeOut );



#endif
