#ifndef TTC_CAN_H
#define TTC_CAN_H
/** { ttc_can.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for CAN devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_CAN(tc_can_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_can_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_can_init(LogicalIndex);
 *  4) use:         ttc_can_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level can and application.
 *
 *  Created from template ttc_device.h revision 26 at 20140322 11:01:08 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef EXTENSION_ttc_can
#error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_can.sh
#endif

//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "ttc_interrupt.h"
#include "ttc_can_types.h"
#include "./interfaces/ttc_can_interface.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level can only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * can devices on all supported architectures.
 * Check can/can_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares can Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_can_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_can_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_CAN_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_can_config* ttc_can_get_configuration( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_can_get_max_LogicalIndex())
 * @return                == 0: CAN has been reset successfully; != 0: error-code
 */
e_ttc_can_errorcode ttc_can_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_can_get_max_LogicalIndex())
 */
void  ttc_can_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Seetings of indexed device are being reset to their default values.
 * Will automatically call ttc_can_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of can device (1..ttc_can_get_max_index() )
 */
void  ttc_can_load_defaults( t_u8 LogicalIndex );

/** maps from logical to physical device index
 *
 * High-level cans (ttc_can_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_CANn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any
 * order.
 *
 * @param LogicalIndex  logical index of can device (1..ttc_can_get_max_index() )
 * @return              physical index of can device (0 = first physical can device, ...)
 */
t_u8 ttc_can_logical_2_physical_index( t_u8 LogicalIndex );

/** maps from physical to logical device index
 *
 * This is a reverse function to ttc_can_logical_2_physical_index()
 *
 * @param  PhysicalIndex  physical index of can device (0 = first physical can device, ...)
 * @return                logical index of can device (1..ttc_can_get_max_index() )
 */
t_u8 ttc_can_physical_2_logical_index( t_u8 PhysicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_can_get_max_LogicalIndex())
 * @return                == 0: CAN has been reset successfully; != 0: error-code
 */
e_ttc_can_errorcode  ttc_can_reset( t_u8 LogicalIndex );

/** registers given function as a receiver for incoming data
 *
 * Note: This function will automatically call ttc_can_init() to init/reinit USART
 *
 * @param LogicalIndex    device index of CAN to use (1..ttc_can_get_max_logicalindex())
 * @param Argument        will be passed as first argument to receiveBlock()
 * @return                 == 0: Buffer has been stored successfully; != 0: error-code
 */
e_ttc_can_errorcode ttc_can_receive( t_u8 LogicalIndex, void* Argument );

/** Stores given raw buffer to be send in parallel. (Function copies data and returns immediately)
 *
 * Note: CAN must be initialized before!
 * Note: This function is partially thread safe (use it for each CAN from one thread only)!
 *
 * @param LogicalIndex     device index of CAN to use (1..ttc_can_get_max_logicalindex())
 * @param Buffer           memory location from which to read data
 * @param Amount           amount of bytes to send from Buffer[] (even zero bytes are sent)
 * @return                 == 0: Buffer has been stored for delivery successfully; != 0: error-code
 */
e_ttc_can_errorcode ttc_can_send( t_u8 LogicalIndex, t_u32 Destination, t_u8* Buffer, t_base Amount );

/** Stores given constant raw buffer to be send in parallel. (Function stores pointer to data and returns immediately)
 *
 * This function is especially optimized for constant data (e.g. strings in ROM) and avoids extra copying into buffers.
 *
 * Note: CAN must be initialized before!
 * Note: This function is partially thread safe (use it for each CAN from one thread only)!
 *
 * @param LogicalIndex     device index of CAN to use (1..ttc_can_get_max_logicalindex())
 * @param Buffer           memory location from which to read data (must not change!)
 * @param MaxLength        no more than this amount of bytes will be send
 * @return                 == 0: Buffer has been stored for delivery successfully; != 0: error-code
 */
e_ttc_can_errorcode ttc_can_send_const( t_u8 LogicalIndex, t_u32 Destination, const char* Buffer, t_base MaxLength );

/** Initializes the Filter in CANx.
 *
 * Note: CAN must be initialized before!
 * Note: This function is partially thread safe (use it for each CAN from one thread only)!
 *
 * @param Address          Packets from this address will be accepted
 * @param LogicalIndex     device index of CAN to init (1..ttc_can_get_max_logicalindex())
 * @return                 none
 */
void ttc_can_set_filter( t_u16 Address, t_u8 LogicalIndex );

/** Deinitializes the Filter in CANx.
 *
 * Note: CAN must be initialized before!
 * Note: This function is partially thread safe (use it for each CAN from one thread only)!
 *
 * @param Address          Packets from this address will be accepted
 * @param LogicalIndex     device index of CAN to init (1..ttc_can_get_max_logicalindex())
 * @return                 none
 */
void ttc_can_clr_filter( t_u16 Address, t_u8 LogicalIndex );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_can(t_u8 LogicalIndex)
/** Implements asynchronous transmitter. Reads data from one transmit queue.
 * @param Args  t_ttc_can_config* pointing to data of configured CAN
 */
t_u8 _ttc_can_task_transmit( t_ttc_can_config* Config, t_u32 Destination, char* Buffer, t_u8 Size );

/** Called from low level CAN interrupt service routine
 * Note: This function is private and should not be called from outside.
 *
 * @param CAN_Generic    structure containing configuration data of CAN which has received these bytes
 * @param Data           received bytes to add to receive buffer
 */
void _ttc_can_rx_isr( t_ttc_can_config* USART_Generic );

/** Check the status of a Mailbox
 * Note: This function is private and should not be called from outside.
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_can_get_max_LogicalIndex())
 * @param MailBox         Mailbox number
 */
t_u8 _ttc_can_tx_status( t_u8 LogicalIndex, t_u8 MailBox );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_heap_ are passed to interfaces/ttc_heap_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl heap UPDATE
 */

/** Prepares usart driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void _driver_can_prepare();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param Config        = pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @return                configuration of indexed device
 */
t_ttc_can_config* _driver_can_get_configuration( t_ttc_can_config* Config );

/** fills out given Config with maximum valid values for indexed CAN
 * @param Config        = pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @return              == 0: *Config has been initialized successfully; != 0: error-code
 */
e_ttc_can_errorcode _driver_can_get_features( t_ttc_can_config* Config );

/** shutdown single CAN unit device
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @return              == 0: CAN has been shutdown successfully; != 0: error-code
 */
e_ttc_can_errorcode _driver_can_deinit( t_ttc_can_config* Config );

/** initializes single CAN unit for operation
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @return              == 0: CAN has been initialized successfully; != 0: error-code
 */
e_ttc_can_errorcode _driver_can_init( t_ttc_can_config* Config );

/** loads configuration of indexed CAN unit with default values
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_can_errorcode _driver_can_load_defaults( t_ttc_can_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Architecture        pointer to struct t_ttc_can_architecture (must have valid value for PhysicalIndex)
 */
e_ttc_can_errorcode _driver_can_reset( t_ttc_can_architecture* Architecture );

/** Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: CAN must be initialized before!
 * Note: This function is partially thread safe (use it for each CAN from one thread only)!
 *
 * @param Register       pointer to low-level register for fast hardware operation
 * @param Word           pointer to 16 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_can_errorcode _driver_can_read( t_ttc_can_config* Config, t_u8* Byte );

/** Send single data word to output buffer (8 or 9 bits)
 *
 * Note: CAN must be initialized before!
 * Note: This function is partially thread safe (use it for each CAN from one thread only)!
 *
 * @param Register      pointer to low-level register for fast hardware operation
 * @return              == 0: Word has been read successfully; != 0: error-code
 */
t_u8 _driver_can_send( t_ttc_can_config* Config );

/** Initializes the Filter in CANx.
 *
 * Note: CAN must be initialized before!
 * Note: This function is partially thread safe (use it for each CAN from one thread only)!
 *
 * @param FilterNum        Packets from this address will be accepted
// * @param Config           pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @return                 none
 */
void _driver_can_set_filter( t_u8 FilterNum, t_ttc_can_config* Config );

/** Check the status of a Mailbox
 * Note: This function is private and should not be called from outside.
 *
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @param MailBox       Mailbox number
 */
t_u8 __driver_can_tx_status( t_ttc_can_config* Config, t_u8 MailBox );
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_CAN_H
