/*{ stm32_gpio.c ***********************************************
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Low Level Driver for General Purpose Input Output (GPIO) on STM32 architecture.
 * 
}*/

#include "DEPRECATED_stm32_gpio.h"

// all possible configuration configs of an 8-bit wide parallel port
const u32_t ParallelPortConfigs[16] = {  0x00000000,
                                         0x11111111,
                                         0x22222222,
                                         0x33333333,
                                         0x44444444,
                                         0x55555555,
                                         0x66666666,
                                         0x77777777,
                                         0x88888888,
                                         0x99999999,
                                         0xaaaaaaaa,
                                         0xbbbbbbbb,
                                         0xcccccccc,
                                         0xdddddddd,
                                         0xeeeeeeee,
                                         0xffffffff
                                      };
const GPIO_TypeDef* GPIOs[7] = { GPIOA, GPIOB, GPIOC, GPIOD, GPIOE, GPIOF, GPIOG };

 GPIOMode_TypeDef stm32_map_Type(ttc_gpio_mode_e Mode) {
    switch (Type) { // find best matching type for stm32-architecture
      case tgm_analog_in:                     return GPIO_Mode_AIN;
      case tgm_input_floating:                return GPIO_Mode_IN_FLOATING;
      case tgm_input_pull_down:               return GPIO_Mode_IPD;
      case tgm_input_pull_up:                 return GPIO_Mode_IPU;
      case tgm_output_open_drain:             return GPIO_Mode_Out_OD;
      case tgm_output_push_pull:              return GPIO_Mode_Out_PP;
      case tgm_alternate_function_push_pull:  return GPIO_Mode_AF_PP;
      case tgm_alternate_function_open_drain: return GPIO_Mode_AF_OD;
        
      default: Assert(0, ec_InvalidArgument);
    }
    
    return 0;
}
GPIOSpeed_TypeDef stm32_map_Speed(ttc_gpio_speed_e Speed) {
    Assert(Speed < tgs_Unknown, ec_InvalidArgument);

    switch (Speed) { // find best matching speed for stm32-architecture
      case tgs_Min:
      case tgs_2MHz:  return GPIO_Speed_2MHz;
      case tgs_10MHz: return GPIO_Speed_10MHz;
      case tgs_50MHz:
      case tgs_Max:
      default:
        return tgs_Max;
    }
    
    return 0;
}

// direct port manipulations ---------------------------------------------

void stm32_gpio_init(gpio_stm32f1xx_bank_e GPIOx, u8_t Pin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed) {
    if (1) { // port initialization by direct register access
        // runtime measured on STM32F107 @72MHz was < 3.7us

        u32_t PortConfig = _stm32_gpio_compile_port_config(Type, Speed);
        stm32_gpio_bank_enable(GPIOx);

        if (Pin < 8) { // pin configuration is in CRL
            u8_t Shift = Pin * 4;
            u32_t CRL = ((GPIO_TypeDef *) GPIOx)->CRL & ( ~(0x0f << Shift) ); // load register + clear configuration of Pin
            CRL |= (PortConfig << Shift);
            ((GPIO_TypeDef *) GPIOx)->CRL = CRL;                              // activate setting
        }
        else {         // pin configuration is in CRH
            u8_t Shift = (Pin - 8) * 4;
            u32_t CRH = ((GPIO_TypeDef *) GPIOx)->CRH & ( ~(0x0f << Shift) ); // load register + clear configuration of Pin
            CRH |= (PortConfig << Shift);
            ((GPIO_TypeDef *) GPIOx)->CRH = CRH;                              // activate setting
        }
    }
    else { // port initialization using STM Standard Peripheral Library
        // runtime measured on STM32F107 @72MHz was < 10us

        switch ( GPIOx) { // enable peripheral clock to this port
        case stm32_GPIOA: RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); break;
        case stm32_GPIOB: RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE); break;
        case stm32_GPIOC: RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE); break;
        case stm32_GPIOD: RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE); break;
        case stm32_GPIOE: RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE); break;
        case stm32_GPIOF: RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF, ENABLE); break;
#ifdef GPIOG
        case stm32_GPIOG: RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG, ENABLE); break;
#endif
        default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!
        }
        GPIOMode_TypeDef  GPIO_Mode  = stm32_map_Type(Type);

        GPIO_InitTypeDef GPIO_InitStructure;
        GPIO_InitStructure.GPIO_Mode  = GPIO_Mode;
        GPIO_InitStructure.GPIO_Pin   = 1 << Pin;
        GPIO_InitStructure.GPIO_Speed = stm32_map_Speed(Speed);
        GPIO_Init( (GPIO_TypeDef*) GPIOx, &GPIO_InitStructure);

        switch (GPIO_Mode) {
        case GPIO_Mode_IPU: {
            break;
        }
        case GPIO_Mode_Out_PP: {
            //? GPIO_WriteBit(GPIOx, 1 << Pin, Bit_SET);
            break;
        }
        case GPIO_Mode_Out_OD: {
            stm32_gpio_set(GPIOx, Pin); // safe value
            break;
        }
        default: break;
        }
    }
}
void stm32_gpio_bank_enable(gpio_stm32f1xx_bank_e GPIOx) {
    switch ( GPIOx) { // enable peripheral clock to this port
      case stm32_GPIOA: RCC->APB2ENR |= RCC_APB2Periph_GPIOA; break;
      case stm32_GPIOB: RCC->APB2ENR |= RCC_APB2Periph_GPIOB; break;
      case stm32_GPIOC: RCC->APB2ENR |= RCC_APB2Periph_GPIOC; break;
      case stm32_GPIOD: RCC->APB2ENR |= RCC_APB2Periph_GPIOD; break;
      case stm32_GPIOE: RCC->APB2ENR |= RCC_APB2Periph_GPIOE; break;
#ifdef GPIOF
      case stm32_GPIOF: RCC->APB2ENR |= RCC_APB2Periph_GPIOF; break;
#endif
#ifdef GPIOG
      case stm32_GPIOG: RCC->APB2ENR |= RCC_APB2Periph_GPIOG; break;
#endif
      default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!
    }
}
void stm32_gpio_bank_disable(gpio_stm32f1xx_bank_e GPIOx) {
    switch (GPIOx) { // enable peripheral clock to this port
      case stm32_GPIOA: RCC->APB2ENR &= ~RCC_APB2Periph_GPIOA; break;
      case stm32_GPIOB: RCC->APB2ENR &= ~RCC_APB2Periph_GPIOB; break;
      case stm32_GPIOC: RCC->APB2ENR &= ~RCC_APB2Periph_GPIOC; break;
      case stm32_GPIOD: RCC->APB2ENR &= ~RCC_APB2Periph_GPIOD; break;
      case stm32_GPIOE: RCC->APB2ENR &= ~RCC_APB2Periph_GPIOE; break;
#ifdef GPIOF
      case stm32_GPIOF: RCC->APB2ENR &= ~RCC_APB2Periph_GPIOF; break;
#endif
#ifdef GPIOG
      case stm32_GPIOG: RCC->APB2ENR &= ~RCC_APB2Periph_GPIOG; break;
#endif
      default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!
    }
}
inline bool stm32_gpio_get(gpio_stm32f1xx_bank_e GPIOx, u8_t Pin) {

    u32_t Data = ((GPIO_TypeDef *) GPIOx)->IDR;
    if ( Data & (1 << Pin) ) return TRUE;
    return FALSE;
}
inline void stm32_gpio_clr(gpio_stm32f1xx_bank_e GPIOx, u8_t Pin) {
    ((GPIO_TypeDef *) GPIOx)->BRR |= 1 << Pin;
}
inline void stm32_gpio_set(gpio_stm32f1xx_bank_e GPIOx, u8_t Pin) {
    ((GPIO_TypeDef *) GPIOx)->BSRR |= 1 << Pin;
}

// port manipulations by variable ----------------------------------------
void stm32_gpio_init2_variable(ttc_Port_t* Port, ttc_gpio_bank_t GPIOx, u8_t Pin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed) {

    stm32_gpio_variable(Port, GPIOx, Pin);
    stm32_gpio_init_variable(Port, Type, Speed);
}
inline void stm32_gpio_variable(tgp_e* Port, gpio_stm32f1xx_bank_e GPIOx, u8_t Pin) {
    Port->GPIOx= GPIOx;
    Port->Pin  = Pin;
    Port->BRR  = cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOx)->BRR),  Pin);
    Port->BSRR = cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOx)->BSRR), Pin);
    Port->IDR  = cm3_calc_peripheral_BitBandAddress( (void*) &(((GPIO_TypeDef *) GPIOx)->IDR),  Pin);
}
void stm32_gpio_init_variable(tgp_e* Port, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed) {
    stm32_gpio_init(Port->GPIOx, Port->Pin, Type, Speed);
}
inline bool stm32_gpio_get_variable(tgp_e* Port) {
    if (1)
        return *(Port->IDR); // read from bitband address
    else {
        u32_t Data = ((GPIO_TypeDef *) Port->GPIOx)->IDR;
        if ( Data & (1 << Port->Pin) ) return TRUE;
        return FALSE;
    }
}
inline void stm32_gpio_set_variable(tgp_e* Port) {
    *(Port->BSRR) = 1; // write to bitband address
}
inline void stm32_gpio_clr_variable(tgp_e* Port) {
    *(Port->BRR) = 1;
}
u8_t gpio_stm32f1xx_create_index8(ttc_gpio_bank_t GPIOx, u8_t Pin) {

    switch (GPIOx) {

#ifdef GPIOA
    case stm32_GPIOA: return 0x10 | (0x0f & Pin);
#endif
#ifdef GPIOB
    case stm32_GPIOB: return 0x20 | (0x0f & Pin);
#endif
#ifdef GPIOC
    case stm32_GPIOC: return 0x30 | (0x0f & Pin);
#endif
#ifdef GPIOD
    case stm32_GPIOD: return 0x40 | (0x0f & Pin);
#endif
#ifdef GPIOE
    case stm32_GPIOE: return 0x50 | (0x0f & Pin);
#endif
#ifdef GPIOF
    case stm32_GPIOF: return 0x60 | (0x0f & Pin);
#endif
#ifdef GPIOG
    case stm32_GPIOG: return 0x70 | (0x0f & Pin);
#endif

    default: break;
    }

    return 0;
}
void interrupt_stm32f1xx_gpio_from_index(u8_t PhysicalIndex, GPIO_TypeDef** GPIOx, u8_t* Pin) {

    switch (PhysicalIndex & 0xf0) {

#ifdef GPIOA
    case 0x10: *GPIOx = GPIOA; break;
#endif
#ifdef GPIOB
    case 0x20: *GPIOx = GPIOB; break;
#endif
#ifdef GPIOC
    case 0x30: *GPIOx = GPIOC; break;
#endif
#ifdef GPIOD
    case 0x40: *GPIOx = GPIOD; break;
#endif
#ifdef GPIOE
    case 0x50: *GPIOx = GPIOE; break;
#endif
#ifdef GPIOF
    case 0x60: *GPIOx = GPIOF; break;
#endif
#ifdef GPIOG
    case 0x70: *GPIOx = GPIOG; break;
#endif

    default: *GPIOx = NULL;
    }

    *Pin = PhysicalIndex & 0x0f;
}

// parallel ports
void stm32_gpio_init_u8_pin(ttc_gpio_bank_t GPIOx, u8_t FirstPin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed) {
    Assert(FirstPin < 9, ec_InvalidArgument); // 8-bit wide parallel ports may start at pins 0..8 only

    // compile single port configuration
    u8_t PortConfig = _stm32_gpio_compile_port_config(Type, Speed);
    u32_t Mask = 0xffffffff;
    u8_t ShiftLeft  = FirstPin * 4;
    u8_t ShiftRight = (8-FirstPin) * 4;

    stm32_gpio_bank_enable(GPIOx);

    // read current pin configurations + clear configurations of parallel port pins
    u32_t CRL = ((GPIO_TypeDef *) GPIOx)->CRL & ( 0xffffffff ^ (Mask << ShiftLeft) );
    u32_t CRH = ((GPIO_TypeDef *) GPIOx)->CRH & ( 0xffffffff ^ (Mask >> ShiftRight) );

    // write new configuration of all 8 bits in parallel
    u32_t ParallelPortConfig = ParallelPortConfigs[PortConfig];
    CRL |= (ParallelPortConfig << ShiftLeft);
    CRH |= (ParallelPortConfig >> ShiftRight);

    // activate new configuration
    ((GPIO_TypeDef *) GPIOx)->CRL = CRL;
    ((GPIO_TypeDef *) GPIOx)->CRH = CRH;
}
void stm32_gpio_init_u16(ttc_gpio_bank_t GPIOx, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed) {

    // compile single port configuration
    u8_t PortConfig = _stm32_gpio_compile_port_config(Type, Speed);

    stm32_gpio_bank_enable(GPIOx);

    // write new configuration of all 16 bits in parallel
    u32_t ParallelPortConfig = ParallelPortConfigs[PortConfig];
    ((GPIO_TypeDef *) GPIOx)->CRL = ParallelPortConfig;
    ((GPIO_TypeDef *) GPIOx)->CRH = ParallelPortConfig;
}
u8_t _stm32_gpio_compile_port_config(ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed) {
    u8_t PortConfig = 0;

    switch (Type) {
    case tgm_analog_in:
        PortConfig = (gpc_input_analog << 2)       | gpm_input;
        break;
    case tgm_input_floating:
        PortConfig = (gpc_input_floating << 2)     | gpm_input;
        break;
    case tgm_input_pull_down:
        PortConfig = (gpc_input_pull_up_down << 2) | gpm_input;
        break;
    case tgm_input_pull_up:
        PortConfig = (gpc_input_pull_up_down << 2) | gpm_input;
        break;
    case tgm_output_open_drain:
        if (Speed < tgs_2MHz)       { PortConfig = (gpc_output_open_drain << 2) | gpm_output_2mhz; }
        else if (Speed > tgs_10MHz) { PortConfig = (gpc_output_open_drain << 2) | gpm_output_50mhz; }
        else                        { PortConfig = (gpc_output_open_drain << 2) | gpm_output_10mhz; }
        break;
    case tgm_output_push_pull:
        if (Speed < tgs_2MHz)       { PortConfig = (gpc_output_push_pull << 2) | gpm_output_2mhz; }
        else if (Speed > tgs_10MHz) { PortConfig = (gpc_output_push_pull << 2) | gpm_output_50mhz; }
        else                        { PortConfig = (gpc_output_push_pull << 2) | gpm_output_10mhz; }
        break;
    case tgm_alternate_function_push_pull:
        if (Speed < tgs_2MHz)       { PortConfig = (gpc_output_alternate_push_pull << 2) | gpm_output_2mhz; }
        else if (Speed > tgs_10MHz) { PortConfig = (gpc_output_alternate_push_pull << 2) | gpm_output_50mhz; }
        else                        { PortConfig = (gpc_output_alternate_push_pull << 2) | gpm_output_10mhz; }
        break;
    case tgm_alternate_function_open_drain:
        if (Speed < tgs_2MHz)       { PortConfig = (gpc_output_alternate_open_drain << 2) | gpm_output_2mhz; }
        else if (Speed > tgs_10MHz) { PortConfig = (gpc_output_alternate_open_drain << 2) | gpm_output_50mhz; }
        else                        { PortConfig = (gpc_output_alternate_open_drain << 2) | gpm_output_10mhz; }
        break;
    default:
        Assert_Halt_EC(ec_InvalidArgument);
    }
    return PortConfig;
}
inline u8_t stm32_gpio_get_u8_pin(ttc_gpio_bank_t GPIOx, u8_t Pin) {
    return stm32_gpio_get_u8_pin2(GPIOx, Pin);
}
inline void stm32_gpio_set_u8_pin(ttc_gpio_bank_t GPIOx, u8_t Pin, u16_t Value) {
    stm32_gpio_set_u8_pin3(GPIOx, Pin, Value);
}
