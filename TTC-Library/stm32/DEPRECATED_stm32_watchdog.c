/*{ stm32_watchdog::.c ************************************************
 
  Interface to watchdog unit of CortexM3 microcontrollers.
  Note: For platform independent watchdogs, see ttc_watchdog.*
  
  written by Gregor Rebel 2012
 
}*/

#include "stm32_watchdog.h"

//{ Function definitions *************************************************

int stm32_init_IndependendWatchdog(unsigned int Reload, int Prescaler) {
  if (Reload    == 0)  Reload   =0xfff;
  if (Prescaler == 0)  Prescaler=16;
  
  RCC_LSICmd(ENABLE);
  IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
  
  if      (Prescaler <= 4)   IWDG_SetPrescaler(IWDG_Prescaler_4);
  else if (Prescaler <= 8)   IWDG_SetPrescaler(IWDG_Prescaler_8);
  else if (Prescaler <= 16)  IWDG_SetPrescaler(IWDG_Prescaler_16);
  else if (Prescaler <= 32)  IWDG_SetPrescaler(IWDG_Prescaler_32);
  else if (Prescaler <= 64)  IWDG_SetPrescaler(IWDG_Prescaler_64);
  else if (Prescaler <= 128) IWDG_SetPrescaler(IWDG_Prescaler_128);
  else                       IWDG_SetPrescaler(IWDG_Prescaler_256);
  
  IWDG_SetReload(Reload);
  IWDG_WriteAccessCmd(IWDG_WriteAccess_Disable);
  IWDG_Enable();
  
  return 0; // successfully initialized watchdog
}
void stm32_reload_IndependendWatchdog() {
  IWDG_ReloadCounter();
}
void stm32_deinit_IndependendWatchdog() { // ToDo
  //? IWDG_Disable();
}
//}FunctionDefinitions
