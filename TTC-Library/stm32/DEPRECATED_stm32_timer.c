/** { stm32_timer.c ************************************************
 
                           The ToolChain
                      
   High-Level interface for TIMER device.
     
   Note: See ttc_timer.h for description of stm32 independent TIMER implementation.
  
   Authors: 
}*/

#include "stm32_timer.h"

//{ Function definitions ***************************************************

bool stm32_timer_check_initialized(ttc_timer_config_t* Config) {
    switch(Config->LogicalIndex){
    case 1: if((TIM1->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
    case 2: if((TIM2->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
    case 3: if((TIM3->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#ifdef TTC_TIMER4
    case 4: if((TIM4->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER5
    case 5: if((TIM5->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER6
    case 6: if((TIM6->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER7
    case 7: if((TIM7->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER8
    case 8: if((TIM8->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER9
    case 9: if((TIM9->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER10
    case 10: if((TIM10->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER11
    case 11: if((TIM11->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER12
    case 12: if((TIM12->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER13
    case 13: if((TIM13->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER14
    case 14: if((TIM14->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER15
    case 15: if((TIM15->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER16
    case 16: if((TIM16->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
#ifdef TTC_TIMER17
    case 17: if((TIM17->CR1 & 0x01) == 1) return TRUE;
        else return FALSE;
        break;
#endif
    default: Assert_TIMER(0, ec_UNKNOWN);
    }

    return FALSE;
}
ttc_timer_errorcode_e stm32_timer_load_defaults(ttc_timer_config_t* Config) {
    Assert_TIMER(Config != NULL, tte_NotImplemented);
    Assert_TIMER(Config->LogicalIndex > 0, tte_InvalidArgument);

/*{    Config->Flags.All=0;
    Config->Timer_Arch->BaseRegister->CR1.URS=0;       // This bit is set and cleared by software to select the UEV event sources
    Config->Timer_Arch->BaseRegister->CR1.CMS=0;       // Center-aligned mode selection (Deactivate, control by DIR flag)
    Config->Timer_Arch->BaseRegister->CR1.DIR=0;       // 0-Upcounting Mode Selection, 1-Downcounting Mode Selection
    Config->Timer_Arch->BaseRegister->CR1.OPM=0;       // One Pulse Mode deactivated
    Config->Timer_Arch->BaseRegister->CR1.URS=1;       // Update request source
    Config->Timer_Arch->BaseRegister->SMCR.SMS=0;      // Slave mode selection disabled
       Config->Timer_Arch->BaseRegister->PSC = 1;   }*/         // Prescaler configuration
    switch (Config->LogicalIndex) {           // determine base register and other low-level configuration data
       case 1: { // load low-level configuration of second timer device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM1, ENABLE);
         break;
       }
       case 2: { // load low-level configuration of third timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM2, ENABLE);
         break;
       }
       case 3: { // load low-level configuration of fourth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM3, ENABLE);
         break;
       }
       case 4: { // load low-level configuration of fifth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM4, ENABLE);
         break;
       }
       case 5: { // load low-level configuration of sixth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM5, ENABLE);
         break;
       }
       case 6: { // load low-level configuration of seveth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM6, ENABLE);
         break;
       }
       case 7: { // load low-level configuration of eighth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM7, ENABLE);
         break;
       }
       case 8: { // load low-level configuration of ninth timer device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM8, ENABLE);
         break;
       }
       case 9: { // load low-level configuration of tenth timer device
         //Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM10; // load adress of register base of TIMER device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM9, ENABLE);
         break;
       }
       case 10: { // load low-level configuration of eleventh timer device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM10, ENABLE);
         break;
       }
       case 11: { // load low-level configuration of twelfth timer device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM11, ENABLE);
         break;
       }
       case 12: { // load low-level configuration of thirteenth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM12, ENABLE);
         break;
       }
       case 13: { // load low-level configuration of fourteenth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM13, ENABLE);
         break;
       }
       case 14: { // load low-level configuration of fifteenth timer device
         RCC_APB1PeriphClockCmd (RCC_APB1Periph_TIM14, ENABLE);
         break;
       }
       case 15: { // load low-level configuration of sixteenth timer device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM15, ENABLE);
         break;
       }
       case 16: { // load low-level configuration of seventeenth timer device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM16, ENABLE);
         break;
       }
       case 17: { // load low-level configuration of seventeenth timer device
         RCC_APB1PeriphClockCmd (RCC_APB2Periph_TIM17, ENABLE);
         break;
       }
       default: Assert_TIMER(0, tte_NotImplemented);
           break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
       }

    return tte_OK;
}
stm32_timer_config_t* stm32_timer_get_configuration(ttc_timer_config_t* Config) {
    Assert_TIMER(Config, tte_InvalidArgument);
    Assert_TIMER(Config->LogicalIndex > 0, tte_InvalidArgument);

    stm32_timer_config_t* Config_stm32 = Config->Timer_Arch;
    if (Config_stm32 == NULL)   // first call: allocate + init configuration
      Config_stm32 = ttc_heap_alloc_zeroed( sizeof(ttc_timer_arch_t) );


      switch (Config->LogicalIndex) {           // determine base register and other low-level configuration data
         case 1: { // load low-level configuration of second timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM1; // load adress of register base of TIMER device
           break;
         }
         case 2: { // load low-level configuration of third timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM2; // load adress of register base of TIMER device
           break;
         }
         case 3: { // load low-level configuration of fourth timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM3; // load adress of register base of TIMER device
           break;
         }
         case 4: { // load low-level configuration of fifth timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM4; // load adress of register base of TIMER device
           break;
         }
         case 5: { // load low-level configuration of sixth timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM5; // load adress of register base of TIMER device
           break;
         }
         case 6: { // load low-level configuration of seveth timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM6; // load adress of register base of TIMER device
           break;
         }
         case 7: { // load low-level configuration of eighth timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM7; // load adress of register base of TIMER device
           break;
         }
         case 8: { // load low-level configuration of ninth timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM8; // load adress of register base of TIMER device
           break;
         }
         case 9: { // load low-level configuration of tenth timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM9; // load adress of register base of TIMER device
           break;
         }
         case 10: { // load low-level configuration of eleventh timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM10; // load adress of register base of TIMER device
           break;
         }
         case 11: { // load low-level configuration of twelfth timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM11; // load adress of register base of TIMER device
           break;
         }
         case 12: { // load low-level configuration of thirteenth timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM12; // load adress of register base of TIMER device
           break;
         }
         case 13: { // load low-level configuration of fourteenth timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM13; // load adress of register base of TIMER device
           break;
         }
         case 14: { // load low-level configuration of fifteenth timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM14; // load adress of register base of TIMER device
           break;
         }
         case 15: { // load low-level configuration of sixteenth timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM15; // load adress of register base of TIMER device
           break;
         }
         case 16: { // load low-level configuration of seventeenth timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM16; // load adress of register base of TIMER device
           break;
         }
         case 17: { // load low-level configuration of seventeenth timer device
           Config_stm32->BaseRegister = (stm32_register_timer_t*)TIM17; // load adress of register base of TIMER device
           break;
         }
         default: Assert_TIMER(0, tte_NotImplemented);
             break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
         }

    Assert_TIMER(Config_stm32, tte_InvalidConfiguration);
    return Config_stm32;
}
ttc_timer_errorcode_e stm32_timer_get_features(ttc_timer_config_t* Config) {
    Assert_TIMER(Config,    tte_InvalidArgument);
    Assert_TIMER(Config->LogicalIndex > 0, tte_InvalidArgument);
    if (Config->LogicalIndex > TTC_INTERRUPT_TIMER_AMOUNT)
        return tte_DeviceNotFound;

    Config->Flags.All                        = 0;

    /*switch (Config->LogicalIndex) {           // determine features of indexed TIMER device
#ifdef TTC_TIMER1
      case 1: break;
#endif
      default: Assert_TIMER(0, tte_NotImplemented); break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
    }*/

    return tte_OK;
}
ttc_timer_errorcode_e stm32_timer_init(ttc_timer_config_t* Config) {
    Assert_TIMER(Config->LogicalIndex > 0, tte_InvalidArgument);
    Assert_TIMER(Config->TimerEvent.TimePeriod > 0, tte_InvalidArgument);
    //stm32_timer_config_t* Config_stm32 = stm32_timer_get_configuration(Config);
    if (Config->LogicalIndex > TTC_INTERRUPT_TIMER_AMOUNT)
        return tte_DeviceNotFound;
    TIM_TimeBaseInitTypeDef TimerStd;
    stm32_timer_deinit(Config);
    stm32_timer_load_defaults(Config);

    TimerStd.TIM_ClockDivision = TIM_CKD_DIV1;
    TimerStd.TIM_CounterMode = TIM_CounterMode_Up;
    TimerStd.TIM_Period = Config->TimerEvent.TimePeriod;
    TimerStd.TIM_Prescaler = (uint16_t) (SystemCoreClock / 2000) - 1;
    TimerStd.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(stm32_timer_physical_index_2_address(Config->LogicalIndex), &TimerStd);
    TIM_ARRPreloadConfig(stm32_timer_physical_index_2_address(Config->LogicalIndex), ENABLE);

    Config->Flags.Bits.Initialized = 1;
    TIM_Cmd (stm32_timer_physical_index_2_address(Config->LogicalIndex), ENABLE);

/*    switch (Config->LogicalIndex) {                // find TIMER corresponding to TIMER_index as defined by makefile.100_board_*
#ifdef TTC_TIMER1
      case TTC_TIMER1: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM1;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER2
      case 2: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM2;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER3
      case 3: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM3;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER4
      case 4: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM4;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER5
      case 5: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM5;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER6
      case 6: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM6;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER7
      case 7: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM7;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER8
      case 8: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM8;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER9
      case 9: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM9;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER10
      case 10: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM10;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER11
      case 11: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM11;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER12
      case 12: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM12;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER13
      case 13: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM13;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER14
      case 14: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM14;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER15
      case 15: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM15;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER16
      case 16: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM16;
        stm32_timer_load_defaults(Config);
        break;
#endif
#ifdef TTC_TIMER17
      case 17: Config_stm32->BaseRegister = (stm32_register_timer_t*) TIM17;
        stm32_timer_load_defaults(Config);
        break;
#endif
      default: Assert_TIMER(0, tte_NotImplemented); break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
    }*/

    return tte_OK;
}
ttc_timer_errorcode_e stm32_timer_deinit(ttc_timer_config_t* Config) {
    Assert_TIMER(Config, tte_NotImplemented);
    Assert_TIMER(Config->LogicalIndex > 0, tte_InvalidArgument);

    // ToDo: Deinitialize hardware device
    TIM_DeInit(stm32_timer_physical_index_2_address(Config->LogicalIndex));
    Config->Flags.Bits.Initialized = 0;

    return tte_OK;
}
TIM_TypeDef* stm32_timer_physical_index_2_address(u8_t LogicalIndex) {
    Assert_TIMER(LogicalIndex > 0, tte_InvalidArgument);
    Assert_TIMER(LogicalIndex < TTC_INTERRUPT_TIMER_AMOUNT, tte_InvalidArgument);

    switch (LogicalIndex) {           // determine base register and other low-level configuration data
  #ifdef TTC_TIMER1
             case TTC_TIMER1: return (TIM_TypeDef*)TIM1;
  #endif
  #ifdef TTC_TIMER2
             case TTC_TIMER2: return (TIM_TypeDef*)TIM2;
  #endif
  #ifdef TTC_TIMER3
             case TTC_TIMER3: return (TIM_TypeDef*)TIM3;
  #endif
  #ifdef TTC_TIMER4
             case TTC_TIMER4: return (TIM_TypeDef*)TIM4;
  #endif
  #ifdef TTC_TIMER5
             case TTC_TIMER5: return (TIM_TypeDef*)TIM5;
  #endif
  #ifdef TTC_TIMER6
             case TTC_TIMER6: return (TIM_TypeDef*)TIM6;
  #endif
  #ifdef TTC_TIMER7
             case TTC_TIMER7: return (TIM_TypeDef*)TIM7;
  #endif
  #ifdef TTC_TIMER8
             case TTC_TIMER8: return (TIM_TypeDef*)TIM8;
  #endif
  #ifdef TTC_TIMER9
             case TTC_TIMER9: return (TIM_TypeDef*)TIM9;
  #endif
  #ifdef TTC_TIMER10
             case TTC_TIMER10: return (TIM_TypeDef*)TIM10;
  #endif
  #ifdef TTC_TIMER11
             case TTC_TIMER11: return (TIM_TypeDef*)TIM11;
  #endif
  #ifdef TTC_TIMER12
             case TTC_TIMER12: return (TIM_TypeDef*)TIM12;
  #endif
  #ifdef TTC_TIMER13
             case TTC_TIMER13: return (TIM_TypeDef*)TIM13;
  #endif
  #ifdef TTC_TIMER14
             case TTC_TIMER14: return (TIM_TypeDef*)TIM14;
  #endif
  #ifdef TTC_TIMER15
             case TTC_TIMER15: return (TIM_TypeDef*)TIM15;
  #endif
  #ifdef TTC_TIMER16
             case TTC_TIMER16: return (TIM_TypeDef*)TIM16;
  #endif
  #ifdef TTC_TIMER17
             case TTC_TIMER17: return (TIM_TypeDef*)TIM17;
  #endif
        default: Assert_TIMER(0, tte_NotImplemented); break; // No TTC_TIMERn defined! Check your makefile.100_board_* file!
      }

    return (TIM_TypeDef*)-1;
}
ttc_timer_errorcode_e stm32_timer_reset(ttc_timer_config_t* Config){
    Assert_TIMER(Config, tte_NotImplemented);
    //To be defined
    return tte_OK;
}

//} Function definitions
//{ private functions (ideally) ********************************************

//} private functions
