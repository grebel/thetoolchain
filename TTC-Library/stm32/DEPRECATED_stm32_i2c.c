/*{ stm32_i2c::.c ************************************************

                      The ToolChain
                      
   Universal Synchronous Asynchronous Receiver and Transmitters (stm32_I2Cs)
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Gregor Rebel, Sascha Poggemann 2012-2013
   
   Note: See ttc_i2c.h for description of architecture independent I2C implementation.

}*/

#define USE_OWN_I2C_CODE 1  // do not use buggy I2C-code of StdPeripheral whenever possible

#include "stm32_i2c.h"

// stores configuration data of every initialized I2C-interface
ttc_heap_array_define(ttc_i2c_config_t*, stm32_I2C_Configs, TTC_AMOUNT_I2CS);


u8_t si2c_Initialized = 0;  // ==1: stm32_I2C_Configs has been initialized

//{ Function definitions ********************************************************

void stm32_i2c_reset_all() {
    ttc_memory_set( &(A(stm32_I2C_Configs,0)), 0, sizeof(stm32_i2c_architecture_t*) * TTC_AMOUNT_I2CS);
    si2c_Initialized = 1;
}
ttc_i2c_config_t* stm32_i2c_get_configuration(u8_t I2C_Index) {
    Assert_I2C(I2C_Index > 0, ec_InvalidArgument);
    if (!si2c_Initialized) stm32_i2c_reset_all();
    ttc_i2c_config_t* Config = A(stm32_I2C_Configs, I2C_Index-1);
    if (!Config) {
        Config = ttc_heap_alloc_zeroed( sizeof(ttc_i2c_config_t) );
        stm32_i2c_get_defaults(I2C_Index, Config);
        A(stm32_I2C_Configs, I2C_Index-1) = Config;
    }

    Assert(Config, ec_InvalidImplementation);
    return Config;
}
ttc_i2c_errorcode_e stm32_i2c_get_defaults(u8_t I2C_Index, ttc_i2c_config_t* Config) {
    if (!si2c_Initialized) stm32_i2c_reset_all();
    Assert_I2C(I2C_Index > 0,    ec_InvalidArgument);
    Assert_I2C(Config != NULL, ec_NULL);
    if (I2C_Index > TTC_AMOUNT_I2CS) return tie_DeviceNotFound;

    // assumption: *Config has been zeroed
    Config->Flags.All                   = 0;
    Config->Flags.Bits.Master           = 1;
    Config->Flags.Bits.ModeFast         = 0;
    Config->Flags.Bits.Slave            = 0;
    Config->Flags.Bits.DMA_TX           = 0;
    Config->Flags.Bits.DMA_RX           = 0;
    Config->Flags.Bits.SMBus            = 0;
    Config->Flags.Bits.SMBus_Alert      = 0;
    Config->Flags.Bits.SMBus_Host       = 0;
    Config->Flags.Bits.SMBus_ARP        = 0;
    Config->Flags.Bits.DutyCycle_16_9   = 0;
    Config->Flags.Bits.Acknowledgement  = 1;
    Config->Flags.Bits.Adress_10Bit     = 0;
    Config->Flags.Bits.PacketErrorCheck = 0;
    Config->Flags.Bits.Slave_NoStretch  = 0;
    Config->Flags.Bits.GeneralCalls     = 0;
    Config->Flags.Bits.Interrupt_Events = 0;
    Config->Flags.Bits.Interrupt_Errors = 0;
    Config->ClockSpeed                  = 10000; // Standard Mode

    Config->OwnAddress = 1;
    Config->TimeOut    = 200000;

    return tie_OK;
}
ttc_i2c_errorcode_e stm32_i2c_get_features(u8_t I2C_Index, ttc_i2c_config_t* Config) {
    if (!si2c_Initialized) stm32_i2c_reset_all();
    // assumption: *Config has been zeroed

    Assert_I2C(I2C_Index > 0,    ec_InvalidArgument);
    Assert_I2C(Config != NULL, ec_NULL);
    if (I2C_Index > TTC_AMOUNT_I2CS) return tie_DeviceNotFound;

    Config->Flags.All                   = 0;
    Config->Flags.Bits.Master           = 1;
    Config->Flags.Bits.ModeFast         = 1;
    Config->Flags.Bits.Slave            = 0; // ToDo: implement
    Config->Flags.Bits.DMA_TX           = 0; // ToDo: implement
    Config->Flags.Bits.DMA_RX           = 0; // ToDo: implement
    Config->Flags.Bits.SMBus            = 0; // ToDo: implement
    Config->Flags.Bits.SMBus_Alert      = 0; // ToDo: implement
    Config->Flags.Bits.SMBus_Host       = 0; // ToDo: implement
    Config->Flags.Bits.SMBus_ARP        = 0; // ToDo: implement
    Config->Flags.Bits.DutyCycle_16_9   = 1;
    Config->Flags.Bits.Acknowledgement  = 1;
    Config->Flags.Bits.Adress_10Bit     = 0; // ToDo: implement
    Config->Flags.Bits.PacketErrorCheck = 1;
    Config->Flags.Bits.Slave_NoStretch  = 0; // ToDo: implement
    Config->Flags.Bits.GeneralCalls     = 1;
    Config->Flags.Bits.Interrupt_Events = 0; // ToDo: implement
    Config->Flags.Bits.Interrupt_Errors = 0; // ToDo: implement
    Config->ClockSpeed                  = 400000;
    Config->TimeOut                     = -1; // max positive value

    I2C_TypeDef* MyI2C = NULL;
    switch (I2C_Index) {           // find corresponding I2C as defined by makefile.100_board_*
#ifdef TTC_I2C1
    case 1: MyI2C = TTC_I2C1; break;
#endif
#ifdef TTC_I2C2
    case 2: MyI2C = TTC_I2C2; break;
#endif
    default: Assert_I2C(0, ec_UNKNOWN); break; // No TTC_I2Cn defined! Check your makefile.100_board_* file!
    }
    switch ( (u32_t) MyI2C ) {  // find amount of available remapping-layouts (-> RM0008 p. 176)
    case (u32_t) I2C1:
        Config->Layout=1;     // I2C1 provides 1 alternate pin layout
        break;
    case (u32_t) I2C2:
        Config->Layout=0;     // I2C2 provides 0 alternate pin layouts
        break;
    default: Assert_I2C(0, ec_UNKNOWN);
    }

    switch (I2C_Index) {           // disable features requiring unconfigured pins
    case 1: {
#ifndef TTC_I2C1_SMBAL
        Config->Flags.Bits.SMBus_Alert = 0;
#endif

        break;
    }
    case 2: {
#ifndef TTC_I2C2_SMBAL
        Config->Flags.Bits.SMBus_Alert = 0;
#endif

        break;
    }
    default: Assert_I2C(0, ec_UNKNOWN); return tie_DeviceNotFound;; // I2C misconfigured! Check your makefile.100_board_* file!
    }
    
    return tie_OK;
}
ttc_i2c_errorcode_e stm32_i2c_init(u8_t I2C_Index) {
    // assumption: *ConfigArch has been zeroed

    Assert_I2C(I2C_Index > 0, ec_InvalidArgument);
    ttc_i2c_config_t*  Config = stm32_i2c_get_configuration(I2C_Index);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture;
    if (ConfigArch == NULL) {
        ConfigArch = ttc_heap_alloc_zeroed( sizeof(ttc_i2c_architecture_t) );
        Config->Architecture = ConfigArch;
    }
    if (1) { // validate I2C_Features
        ttc_i2c_config_t I2C_Features;
        I2C_Features.Layout = 0;
        ttc_i2c_errorcode_e Error=stm32_i2c_get_features(I2C_Index, &I2C_Features);
        if (Error) return Error;
        Config->Flags.All &= I2C_Features.Flags.All; // mask out unavailable flags
        if (Config->Layout > I2C_Features.Layout)
            Config->Layout = I2C_Features.Layout;
        if (Config->ClockSpeed > I2C_Features.ClockSpeed)
            Config->ClockSpeed = I2C_Features.ClockSpeed;
        if (Config->Flags.Bits.Adress_10Bit)
            Config->OwnAddress &= 0x03ff; // create 10 bit TargetAddress
        else
            Config->OwnAddress &= 0x007f; // create  7 bit TargetAddress

        if (I2C_Features.Flags.Bits.Master)     // override slave setting
            I2C_Features.Flags.Bits.Slave = 0;
        if (I2C_Features.Flags.Bits.Slave == 0) // not slave => being master
            I2C_Features.Flags.Bits.Master = 1;
    }
    switch (I2C_Index) {                // find I2C corresponding to I2C_index as defined by makefile.100_board_*
#ifdef TTC_I2C1
    case 1: ConfigArch->Base = (register_stm32f1xx_i2c_t*) TTC_I2C1; break;
#endif
#ifdef TTC_I2C2
    case 2: ConfigArch->Base = (register_stm32f1xx_i2c_t*) TTC_I2C2; break;
#endif
    default: Assert_I2C(0, ec_UNKNOWN); break; // No TTC_I2Cn defined! Check your makefile.100_board_* file!
    }
    Config->Layout = _stm32_i2c_get_pins(I2C_Index, ConfigArch);
    if (1) { // compare chosen pin layout to configured pins
        //tie_InvalidPinConfig
        //X ttc_gpio_pin_e Port;
        switch (I2C_Index) { // check if board has defined the TX-pin of I2C to init

        case 1: { // check configuration of I2C #1
#ifdef TTC_I2C1_SDA
            Assert_I2C(TTC_I2C1_SDA == ConfigArch->PortSDA, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
            //X ttc_gpio_variable(&Port, TTC_I2C1_SDA);
            //X Assert_I2C(Port.GPIOx == ConfigArch->PortSDA.GPIOx, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
            //X Assert_I2C(Port.Pin   == ConfigArch->PortSDA.Pin,   ec_InvalidArgument); // ERROR: configured Pin  cannot be used in current remapping layout!
#else
            ConfigArch->PortSDA = tgp_none;
#endif
#ifdef TTC_I2C1_SCL
            Assert_I2C(TTC_I2C1_SCL == ConfigArch->PortSCL, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
            //X ttc_gpio_variable(&Port, TTC_I2C1_SCL);
            //X Assert_I2C(Port.GPIOx == ConfigArch->PortSCL.GPIOx, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
            //X Assert_I2C(Port.Pin   == ConfigArch->PortSCL.Pin,   ec_InvalidArgument); // ERROR: configured Pin  cannot be used in current remapping layout!
#else
            ConfigArch->PortSCL = tgp_none;
#endif
#ifdef TTC_I2C1_SMBAL
            if (Config->Flags.Bits.SMBus_Alert) {
                Assert_I2C(TTC_I2C1_SMBAL == ConfigArch->PortSMBAL, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
                //X ttc_gpio_variable(&Port, TTC_I2C1_SMBAL);
                //X Assert_I2C(Port.GPIOx == ConfigArch->PortSMBAL.GPIOx, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
                //X Assert_I2C(Port.Pin   == ConfigArch->PortSMBAL.Pin,   ec_InvalidArgument); // ERROR: configured Pin  cannot be used in current remapping layout!
            }
#else
            ConfigArch->PortSMBAL = tgp_none;
#endif
            break;
        }
        case 2: { // check configuration of I2C #2
#ifdef TTC_I2C2_SDA
            Assert_I2C(TTC_I2C2_SDA == ConfigArch->PortSDA, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
            //X ttc_gpio_variable(&Port, TTC_I2C2_SDA);
            //X Assert_I2C(Port.GPIOx == ConfigArch->PortSDA.GPIOx, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
            //X Assert_I2C(Port.Pin   == ConfigArch->PortSDA.Pin,   ec_InvalidArgument); // ERROR: configured Pin  cannot be used in current remapping layout!
#else
            ConfigArch->PortSDA = tgp_none;
#endif
#ifdef TTC_I2C2_SCL
            Assert_I2C(TTC_I2C2_SCL == ConfigArch->PortSCL, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
            //X ttc_gpio_variable(&Port, TTC_I2C2_SCL);
            //X Assert_I2C(Port.GPIOx == ConfigArch->PortSCL.GPIOx, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
            //X Assert_I2C(Port.Pin   == ConfigArch->PortSCL.Pin,   ec_InvalidArgument); // ERROR: configured Pin  cannot be used in current remapping layout!
#else
            ConfigArch->PortSCL = tgp_none;
#endif
#ifdef TTC_I2C2_SMBAL
            if (Config->Flags.Bits.SMBus_Alert) {
                Assert_I2C(TTC_I2C2_SMBAL == ConfigArch->PortSMBAL, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
                //X ttc_gpio_variable(&Port, TTC_I2C2_SMBAL);
                //X Assert_I2C(Port.GPIOx == ConfigArch->PortSMBAL.GPIOx, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
                //X Assert_I2C(Port.Pin   == ConfigArch->PortSMBAL.Pin,   ec_InvalidArgument); // ERROR: configured Pin  cannot be used in current remapping layout!
            }
#else
            //ConfigArch->PortSMBAL = tgp_none;
#endif
            break;
        }
        default: Assert_I2C(0, ec_UNKNOWN); break; // No TTC_I2Cn defined! Check your makefile.100_board_* file!
            
        }
    }
    ConfigArch->I2C_HW_Lock = ttc_mutex_create();

    ttc_i2c_errorcode_e Error = _stm32_i2c_reinit_i2c(Config);
    if (Error != tie_OK)  return Error;

    if (0) { // ToDo: setup interrupts

    }

    return tie_OK;
}
ttc_i2c_errorcode_e stm32_i2c_write_register( u8_t I2C_Index, const u16_t TargetAddress, const u8_t Register, ttc_i2c_register_type_e RegisterType, const u8_t Byte) {
    return stm32_i2c_write_registers(I2C_Index, TargetAddress, Register, RegisterType, &Byte, 1);
}
ttc_i2c_errorcode_e stm32_i2c_write_registers(u8_t I2C_Index, const u16_t TargetAddress, const u8_t Register, ttc_i2c_register_type_e RegisterType, const u8_t* Buffer, u16_t Amount) {
    ttc_i2c_config_t*  Config = stm32_i2c_get_configuration(I2C_Index);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture;
    Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;
    Assert_I2C(I2C_Base, ec_NULL);

    ttc_i2c_errorcode_e Close_Status = tie_OK;
    ttc_i2c_errorcode_e Error_I2C    = tie_OK;
#if 1
    ttc_mutex_error_e Error_Mutex = ttc_mutex_lock(ConfigArch->I2C_HW_Lock, Config->TimeOut*10);// fabian removed argument: , stm32_i2c_write_registers
    if(Error_Mutex){
        Assert_I2C(FALSE, ec_Debug);// could not get Mutex Lock within specified Timeout
        return tie_BusError;
    }

    if(Close_Status == tie_OK){
        if (tie_OK != _stm32_i2c_wait_for_flag(Config, I2C_FLAG_BUSY, RESET) ){
            Close_Status = tie_TimeOut_Flag_Busy;
        }
    }
    if(Close_Status == tie_OK){
        if (tie_OK != _stm32_i2c_condition_start(Config)  ){
            Close_Status = tie_TimeOut_StartCondition;
        }
    }
    if(Close_Status == tie_OK){
        if (tie_OK != _stm32_i2c_send_target_address(Config, TargetAddress, tid_WritingToSlave) ){
            Close_Status = tie_TimeOut_Master_ModeSelect;
        }
    }
    if(Close_Status == tie_OK){
        if (tie_OK != _stm32_i2c_send_register_address(Config, Register, RegisterType)){

            Close_Status = tie_TimeOut_Master_RegisterSend;
        }
    }
    if(Close_Status == tie_OK){
        if (tie_OK != _stm32_i2c_send_bytes(Config, Buffer, Amount)){
            Close_Status = tie_TimeOut_Master_BufferSend;
        }
    }



    Error_I2C = _stm32_i2c_close_bus(Config, Close_Status);
    ttc_mutex_unlock(ConfigArch->I2C_HW_Lock);
    return Error_I2C;
#else
    //this implementation does not use hardware locks to protect the interface from concurrent access
    if (tie_OK != _stm32_i2c_wait_for_flag(Config, I2C_FLAG_BUSY, RESET) )                      return _stm32_i2c_close_bus(Config, tie_TimeOut_Flag_Busy);
    if (tie_OK != _stm32_i2c_condition_start(Config) )                                          return _stm32_i2c_close_bus(Config, tie_TimeOut_StartCondition);
    if (tie_OK != _stm32_i2c_send_target_address(Config, TargetAddress, tid_WritingToSlave) )   return _stm32_i2c_close_bus(Config, tie_TimeOut_Master_ModeSelect);
    if (tie_OK != _stm32_i2c_send_register_address(Config, Register, RegisterType) )            return _stm32_i2c_close_bus(Config, tie_TimeOut_Master_RegisterSend); //D
    if (tie_OK != _stm32_i2c_send_bytes(Config, Buffer, Amount) )                               return _stm32_i2c_close_bus(Config, tie_TimeOut_Master_BufferSend);

    return _stm32_i2c_close_bus(Config, tie_OK);
#endif

}
ttc_i2c_errorcode_e stm32_i2c_read_register(u8_t I2C_Index, const u16_t TargetAddress, const u8_t Register, ttc_i2c_register_type_e RegisterType, u8_t* Byte) {
    RegisterType = RegisterType; // avoids warning: unused parameter 'RegisterType'

    ttc_i2c_config_t*  Config = stm32_i2c_get_configuration(I2C_Index);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture;
    Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;
    Assert_I2C(I2C_Base, ec_NULL);

    u32_t MyTimeOut = Config->TimeOut;
    I2C_TypeDef *I2C_Module = (I2C_TypeDef *) ConfigArch->Base;
    ttc_i2c_errorcode_e Error_I2C = tie_OK;

    ttc_mutex_error_e Error_Mutex = ttc_mutex_lock(ConfigArch->I2C_HW_Lock, Config->TimeOut*10); //fabian removed argument: , stm32_i2c_read_register
    if(Error_Mutex){
        Assert_I2C(FALSE, ec_Debug);// could not get Mutex Lock within specified Timeout
        return tie_BusError;
    }
    I2C_AcknowledgeConfig(I2C_Module, ENABLE);

    /* While the bus is busy */
    MyTimeOut = Config->TimeOut;
    while (I2C_GetFlagStatus(I2C_Module, I2C_FLAG_BUSY)) {

        if (MyTimeOut-- == 0) {
            Error_I2C = tie_TimeOut_Flag_Busy;
        }
        ttc_task_yield();
    }
    if(Error_I2C == tie_OK){
        /* Send START condition */
        I2C_GenerateSTART(I2C_Module, ENABLE);

        /* Test on EV5 and clear it */
        MyTimeOut = Config->TimeOut;
        while (!I2C_CheckEvent(I2C_Module, I2C_EVENT_MASTER_MODE_SELECT)) {
            if (MyTimeOut-- == 0) {
                Error_I2C = tie_TimeOut_Master_ModeSelect;
            }
            ttc_task_yield();
        }
    }
    if(Error_I2C == tie_OK){
        /* Send address for write */
        I2C_Send7bitAddress(I2C_Module, TargetAddress<<1, I2C_Direction_Transmitter);

        /* Test on EV6 and clear it */
        MyTimeOut = Config->TimeOut;
        while (!I2C_CheckEvent(I2C_Module,
                               I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) {
            if (MyTimeOut-- == 0) {
                Error_I2C = tie_TimeOut_Master_TransmitterMode;
            }
            ttc_task_yield();
        }
    }
    if(Error_I2C == tie_OK){
        //uSleep(100);
        /* Clear EV6 by setting again the PE bit */
        I2C_Cmd(I2C_Module, ENABLE);

        /* Send the internal address to write to */
        I2C_SendData(I2C_Module, Register);

        //Send STOP Condition
        //I2C_GenerateSTOP(I2C_Module, ENABLE);

        /* While the bus is busy */
        //while (I2C_GetFlagStatus(I2C_Module, I2C_FLAG_BUSY))
        //	;

        /* Send START condition */
        I2C_GenerateSTART(I2C_Module, ENABLE);

        /* Test on EV5 and clear it */
        MyTimeOut = Config->TimeOut;
        while (!I2C_CheckEvent(I2C_Module, I2C_EVENT_MASTER_MODE_SELECT)) {
            if (MyTimeOut-- == 0) {
                Error_I2C = tie_TimeOut_Master_ModeSelect;
            }
            ttc_task_yield();
        }
    }
    if(Error_I2C == tie_OK){
        /* Send address for write */
        I2C_Send7bitAddress(I2C_Module, TargetAddress<<1, I2C_Direction_Receiver);
        /* Test on EV6 and clear it */
        MyTimeOut = Config->TimeOut;
        while (!I2C_CheckEvent(I2C_Module, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)) {
            if (MyTimeOut-- == 0) {
                Error_I2C = tie_TimeOut_Master_TransmitterMode;
            };
            ttc_task_yield();
        }
    }
    if(Error_I2C == tie_OK){
        //Disable Acknowledgement
        I2C_AcknowledgeConfig(I2C_Module, DISABLE);

        //I2C_FLAG_ADDR
        I2C_GetFlagStatus(I2C_Module, I2C_FLAG_ADDR);

        //Send STOP Condition
        I2C_GenerateSTOP(I2C_Module, ENABLE);

        /* Clear EV6 by setting again the PE bit */
        //I2C_Cmd(I2C_Module, ENABLE);


        // Test on EV7 and clear it
        MyTimeOut = Config->TimeOut;
        while (!I2C_CheckEvent(I2C_Module, I2C_EVENT_MASTER_BYTE_RECEIVED)) {
            if (MyTimeOut-- == 0) {
                Error_I2C = tie_TimeOut_Master_ByteRead;
            };
            ttc_task_yield();
        }
    }
    if(Error_I2C == tie_OK){
        // uSleep(150);
        /* Read a byte from the I2C */
        *Byte = I2C_ReceiveData(I2C_Module);

        /* Enable Acknowledgement to be ready for another reception */
        I2C_AcknowledgeConfig(I2C_Module, ENABLE);
    }
    ttc_mutex_unlock(ConfigArch->I2C_HW_Lock);
    Assert_I2C(Error_I2C == tie_OK, ec_UNKNOWN);
    return Error_I2C;
}
ttc_i2c_errorcode_e stm32_i2c_read_registers( u8_t I2C_Index, const u16_t TargetAddress, const u8_t Register, ttc_i2c_register_type_e RegisterType, u8_t* Buffer, u16_t Amount, u16_t* AmountRead) {
    ttc_i2c_config_t*  Config = stm32_i2c_get_configuration(I2C_Index);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture;
    Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;
    Assert_I2C(I2C_Base, ec_NULL);

    ttc_i2c_errorcode_e ReturnCode = tie_OK;
    while (Amount > 0) {
        ReturnCode = stm32_i2c_read_register(I2C_Index, TargetAddress, Register, RegisterType, Buffer);
        if (ReturnCode == tie_OK) {
            Buffer++;
            AmountRead++;
        }
        else return ReturnCode;
    }

    return tie_OK;
}

//} Function definitions
//{ private functions (ideally) *************************************************

u8_t _stm32_i2c_get_pins(u8_t I2C_Index, stm32_i2c_architecture_t* ConfigArch) {
    u8_t Layout = 0;
    ttc_gpio_pin_e PortSDA = tgp_none;

    switch (I2C_Index) { // check if board has defined the SDA-pin of I2C to init
#ifdef TTC_I2C1_SDA
    case 1:
        PortSDA = TTC_I2C1_SDA;

        break;
#endif
#ifdef TTC_I2C2_SDA
    case 2:
        PortSDA = TTC_I2C2_SDA;
        break;
#endif
    default:
        Assert_I2C(0, ec_UNKNOWN);   // No TTC_I2Cn_SDA defined! Check your makefile.100_board_* file!
        break;
    }

    switch ( (u32_t) ConfigArch->Base ) { // determine pin layout (-> RM0008 p.176)
    case (u32_t) I2C1: { // I2C1
        if (PortSDA == tgp_b7) Layout = 1;
        else                            Layout = 0;

        if (Layout) { //no remapping
            //X ConfigArch->PortSDA.GPIOx   = stm32_GPIOB;
            //X ConfigArch->PortSDA.Pin     = 7;
            //X ConfigArch->PortSCL.GPIOx   = stm32_GPIOB;
            //X ConfigArch->PortSCL.Pin     = 6;
            ConfigArch->PortSDA = tgp_b7;
            ConfigArch->PortSCL = tgp_b6;
            GPIO_PinRemapConfig(GPIO_Remap_I2C1, DISABLE);
        }
        else {       // alternate pin remapping (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
            //X ConfigArch->PortSDA.GPIOx   = stm32_GPIOB;
            //X ConfigArch->PortSDA.Pin     = 9;
            //X ConfigArch->PortSCL.GPIOx   = stm32_GPIOB;
            //X ConfigArch->PortSCL.Pin     = 8;
            ConfigArch->PortSDA = tgp_b9;
            ConfigArch->PortSCL = tgp_b8;
            GPIO_PinRemapConfig(GPIO_Remap_I2C1, ENABLE);
        }
        //X ConfigArch->PortSMBAL.GPIOx = stm32_GPIOB;
        //X ConfigArch->PortSMBAL.Pin   = 5;
        ConfigArch->PortSMBAL = tgp_b5;

        break;
    }        
    case (u32_t) I2C2: { // I2C2

        //X ConfigArch->PortSDA.GPIOx   = stm32_GPIOB;
        //X ConfigArch->PortSDA.Pin     = 11;
        //X ConfigArch->PortSCL.GPIOx   = stm32_GPIOB;
        //X ConfigArch->PortSCL.Pin     = 10;
        //X ConfigArch->PortSMBAL.GPIOx = stm32_GPIOB;
        //X ConfigArch->PortSMBAL.Pin   = 12;

        ConfigArch->PortSDA   = tgp_b11;
        ConfigArch->PortSCL   = tgp_b10;
        ConfigArch->PortSMBAL = tgp_b12;
        //no remapping avaliable
        break;
    }
    default: { return tie_DeviceNotFound; }
    }

    return Layout;
}
ttc_i2c_errorcode_e _stm32_i2c_wait_for_flag(ttc_i2c_config_t* Config, u32_t Flag, FlagStatus WaitForState) {
    Assert_I2C(Config, ec_NULL);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;                          Assert_I2C(I2C_Base, ec_NULL);
    u16_t MyTimeOut = Config->TimeOut;

    if (MyTimeOut) {
        while( WaitForState != I2C_GetFlagStatus( (I2C_TypeDef*) I2C_Base, Flag) && (MyTimeOut > 0) ) {
            MyTimeOut--;
            ttc_task_yield();
        }
        if (MyTimeOut == 0)
            return tie_TimeOut;
    }
    else
        while( WaitForState != I2C_GetFlagStatus( (I2C_TypeDef*) I2C_Base, Flag) ) ttc_task_yield();

    return tie_OK;
}
ttc_i2c_errorcode_e _stm32_i2c_wait_for_event(ttc_i2c_config_t* Config, u32_t Event) {
    Assert_I2C(Config, ec_NULL);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;                          Assert_I2C(I2C_Base, ec_NULL);
    u16_t MyTimeOut = Config->TimeOut;

    if (MyTimeOut) {
        if (1) {
            while( (SUCCESS != I2C_CheckEvent( (I2C_TypeDef*) I2C_Base, Event)) && (MyTimeOut > 0) ) {
                MyTimeOut--;
                ttc_task_yield();
            }
            if (MyTimeOut == 0)  return tie_TimeOut;
        }
        else {
            ttc_task_begin_criticalsection();
            while (SUCCESS != I2C_CheckEvent( (I2C_TypeDef*) I2C_Base, Event));
            ttc_task_end_criticalsection();
        }
    }
    else
        while( SUCCESS != I2C_CheckEvent( (I2C_TypeDef*) I2C_Base, Event) )
            ttc_task_yield();

    return tie_OK;
}
ttc_i2c_errorcode_e _stm32_i2c_read_bytes(ttc_i2c_config_t* Config, u8_t* Buffer, u16_t Amount, u16_t* AmountRead) {
    Assert_I2C(Config, ec_NULL);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;                          Assert_I2C(I2C_Base, ec_NULL);

    ttc_i2c_errorcode_e Error;

    _stm32_i2c_condition_stop(Config);
    if (0) ttc_task_begin_criticalsection();
    while (Amount-- > 0) {
        Error = _stm32_i2c_read_byte(Config, Buffer);
        if (Error) return Error;
        (*AmountRead)++;
        Buffer++;
    }
    if (0) ttc_task_end_criticalsection();

    return tie_OK;
}
ttc_i2c_errorcode_e _stm32_i2c_read_byte(ttc_i2c_config_t* Config, u8_t* Buffer) {
    Assert_I2C(Config, ec_NULL);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;                          Assert_I2C(I2C_Base, ec_NULL);

    // clear all flags
    (void) I2C_Base->SR1;
    (void) I2C_Base->SR2;

    u16_t MyTimeOut = Config->TimeOut;
    while ( (MyTimeOut > 0) && (I2C_Base->SR1.RXNE) ) {
        ttc_task_yield();
        MyTimeOut--;
    }
    if (MyTimeOut > 0) {
        *Buffer = *( (u8_t*) &(ConfigArch->Base->DR) );
        return tie_OK;
    }
    return tie_TimeOut;
}
ttc_i2c_errorcode_e _stm32_i2c_send_register_address(ttc_i2c_config_t* Config, u32_t Register, ttc_i2c_register_type_e RegisterType) {
    Assert_I2C(Config, ec_NULL);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;                          Assert_I2C(I2C_Base, ec_NULL);

    u8_t AmountBytes = 0;
    u8_t Buffer[4];

    switch (RegisterType) { // send register adress (MSB first)

    case tiras_MSB_First_32Bit:
        Buffer[0] = (u8_t) (Register & 0xff000000) >> 24;
        Buffer[1] = (u8_t) (Register & 0x00ff0000) >> 16;
        Buffer[2] = (u8_t) (Register & 0x0000ff00) >> 8;
        Buffer[3] = (u8_t) Register & 0x000000ff;
        AmountBytes = 4;
        break;
    case tiras_MSB_First_24Bit:
        Buffer[0] = (u8_t) (Register & 0x00ff0000) >> 16;
        Buffer[1] = (u8_t) (Register & 0x0000ff00) >> 8;
        Buffer[2] = (u8_t) Register & 0x000000ff;
        AmountBytes = 3;
        break;

    case tiras_MSB_First_16Bit:
        Buffer[0] = (u8_t) (Register & 0x0000ff00) >> 8;
        Buffer[1] = (u8_t) Register & 0x000000ff;
        AmountBytes = 2;
        break;

    case tiras_MSB_First_8Bit:
        Buffer[0] = (u8_t) Register & 0x000000ff;
        AmountBytes = 1;
        break;

    case tiras_LSB_First_32Bit:
        Buffer[0] = (u8_t) Register & 0x000000ff;
        Buffer[1] = (u8_t) (Register & 0x0000ff00) >> 8;
        Buffer[2] = (u8_t) (Register & 0x00ff0000) >> 16;
        Buffer[3] = (u8_t) (Register & 0xff000000) >> 24;
        AmountBytes = 4;
        break;
    case tiras_LSB_First_24Bit:
        Buffer[0] = (u8_t) Register & 0x000000ff;
        Buffer[1] = (u8_t) (Register & 0x0000ff00) >> 8;
        Buffer[2] = (u8_t) (Register & 0x00ff0000) >> 16;
        AmountBytes = 3;
        break;

    case tiras_LSB_First_16Bit:
        Buffer[0] = (u8_t) Register & 0x000000ff;
        Buffer[1] = (u8_t) (Register & 0x0000ff00) >> 8;
        AmountBytes = 2;
        break;

    case tiras_LSB_First_8Bit:
        Buffer[0] = (u8_t) Register & 0x000000ff;
        AmountBytes = 1;
        break;

    default: Assert_I2C(0, ec_InvalidArgument); // unknown register size
    }

    return _stm32_i2c_send_bytes(Config, Buffer, AmountBytes);
}
ttc_i2c_errorcode_e _stm32_i2c_send_bytes(ttc_i2c_config_t* Config, const u8_t* Buffer, u16_t Amount) {
    Assert_I2C(Config, ec_NULL);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;                          Assert_I2C(I2C_Base, ec_NULL);

    ttc_i2c_errorcode_e Error;

    while (Amount-- > 0) {
        Error = _stm32_i2c_send_byte(Config, *Buffer++);
        if (Error)
            return Error;
    }

    return tie_OK;
}
ttc_i2c_errorcode_e _stm32_i2c_send_byte(ttc_i2c_config_t* Config, const u8_t Byte) {
    Assert_I2C(Config, ec_NULL);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;                          Assert_I2C(I2C_Base, ec_NULL);

    if (USE_OWN_I2C_CODE) {
        *( (u8_t*) &(I2C_Base->DR) ) = Byte;

        //X I2C_Base->CR1.PE = 1;  // reset EV6
        if (0) {
            I2C_SR1_t SR1;
            I2C_SR2_t SR2;

            u16_t MyTimeOut = Config->TimeOut;
            while ( (MyTimeOut > 0) && (! SR1.TxE) ) {
                MyTimeOut--;

                // Read from SR1 and SR2 register to clear all flags
                SR1 = I2C_Base->SR1;
                SR2 = I2C_Base->SR2;

                (void) SR1;
                (void) SR2;

                ttc_task_yield();
            }
            if (MyTimeOut == 0)
                return tie_TimeOut_Master_TransmitterMode;
        }
        else {
            if (tie_OK != _stm32_i2c_wait_for_event(Config, I2C_EVENT_MASTER_BYTE_TRANSMITTED) )
                return tie_TimeOut_Master_ByteTransmitted;
        }
    }
    else {
        I2C_SendData( (I2C_TypeDef*) ConfigArch->Base, Byte);
        if (tie_OK != _stm32_i2c_wait_for_event(Config, I2C_EVENT_MASTER_BYTE_TRANSMITTED) )
            return tie_TimeOut_Master_ByteTransmitted;
    }

    return tie_OK;
}

ttc_i2c_errorcode_e _stm32_i2c_close_bus(ttc_i2c_config_t* Config, ttc_i2c_errorcode_e Status) {
    Assert_I2C(Config, ec_NULL);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;                          Assert_I2C(I2C_Base, ec_NULL);

    if (0) {
        I2C_SR1_t SR1 = I2C_Base->SR1;
        if (SR1.TIMEOUT)  Status = tie_TimeOut;
        if (SR1.BERR)     Status = tie_BusError;
        if (SR1.ARLO)     Status = tie_Master_ArbitrationLost;
        if (SR1.AF)       Status = tie_AcknowledgeFailure;
        if (SR1.PECERR)   Status = tie_PEC_Error;
        if (SR1.SMBALERT) Status = tie_SMBusAlert;
    }

    if (Status == tie_OK) {
        I2C_GenerateSTOP( (I2C_TypeDef*) I2C_Base, ENABLE);
    }
    else { // error-status: send out zero bits to reset all slaves
        _stm32_i2c_flush_slave(Config);
        _stm32_i2c_reinit_i2c(Config);
        //Assert_I2C(FALSE, ec_Debug);//D
        if (0) { // try to reset slaves
            // _stm32_i2c_condition_start(Config);

            // send out zero byte to flush slaves shift register
            u8_t Zero = 0;

            _stm32_i2c_send_bytes(Config, &Zero, 1);
            // _stm32_i2c_condition_stop(Config);
        }
        Config->ErrorCount++;
        Assert_I2C(Config->ErrorCount < 50, ec_DeviceNotFound);
    }

    // Read from SR1 and SR2 register to clear all flags
    (void) I2C_Base->SR1;
    (void) I2C_Base->SR2;

    return Status;
    //return tie_OK;
}
ttc_i2c_errorcode_e _stm32_i2c_flush_slave(ttc_i2c_config_t* Config) {
    Assert_I2C(Config, ec_NULL);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;                          Assert_I2C(I2C_Base, ec_NULL);

    if (ConfigArch->PortSDA)
        ttc_gpio_init(ConfigArch->PortSCL, tgm_output_push_pull, tgs_Max);

    for (int q = 0; q < 32; q++) {

        ttc_gpio_set(ConfigArch->PortSCL);
        uSleep(10);
        ttc_gpio_clr(ConfigArch->PortSCL);
        uSleep(10);

    }

    return tie_OK;
}
ttc_i2c_errorcode_e _stm32_i2c_reinit_i2c(ttc_i2c_config_t* Config) {
    Assert_I2C(Config, ec_NULL);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;                          Assert_I2C(I2C_Base, ec_NULL);

    if (1) { // deconfigure GPIOs

        if (ConfigArch->PortSDA)
            ttc_gpio_init(ConfigArch->PortSDA,    tgm_input_floating, tgs_Max);

        if (ConfigArch->PortSCL)
            ttc_gpio_init(ConfigArch->PortSCL,    tgm_input_floating, tgs_Max);

        if (ConfigArch->PortSMBAL && Config->Flags.Bits.SMBus_Alert)
            ttc_gpio_init(ConfigArch->PortSMBAL,  tgm_input_floating,  tgs_Max);
    }
    I2C_Base->CR1.SWRST = 1;
    I2C_DeInit( (I2C_TypeDef*) I2C_Base);
    if (1) { // activate clocks
        switch ( (u32_t) I2C_Base ) {
        case (u32_t) I2C1: {
            RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
            if (Config->Layout) { // GPIOB + GPIOA
                RCC_APB2PeriphClockCmd(  RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
            }
            else {                     // GPIOB
                RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
            }
            break;
        }
        case (u32_t) I2C2: {
            RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE);
            RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
            break;
        }
        default: Assert_I2C(0, ec_UNKNOWN); break; // should never occur!
        }
    }
    if (1) { // reset I2C device
        switch ( (u32_t) I2C_Base ) {
        case (u32_t) I2C1: {
        default:
                RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C1, ENABLE);  // Reset I2C
                RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C1, DISABLE); // Release I2C from reset state
                break;
            }
        case (u32_t) I2C2: {
            RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C2, ENABLE);  // Reset I2C
            RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C2, DISABLE); // Release I2C from reset state
            break;
        }
        }

    }
    if (1) { // apply remap layout

        switch ( (u32_t) I2C_Base ) {
        case (u32_t) I2C1: { // I2C1
            if (Config->Layout)
                GPIO_PinRemapConfig(GPIO_Remap_I2C1, DISABLE);
            else
                GPIO_PinRemapConfig(GPIO_Remap_I2C1, ENABLE);

            break;
        }
        default: { break; }
        }
    }

    I2C_Cmd( (I2C_TypeDef*) I2C_Base, ENABLE);

    if (1) { // init I2C after enabling it

        I2C_InitTypeDef I2C_MyInit;
        ttc_memory_set( &I2C_MyInit, 0, sizeof(I2C_InitTypeDef) );

        I2C_MyInit.I2C_OwnAddress1 = Config->OwnAddress;
        I2C_MyInit.I2C_ClockSpeed  = Config->ClockSpeed;
        if (Config->Flags.Bits.SMBus) {
            if (Config->Flags.Bits.SMBus_Host)
                I2C_MyInit.I2C_Mode = I2C_Mode_SMBusHost;
            else
                I2C_MyInit.I2C_Mode = I2C_Mode_SMBusDevice;
        }
        else  I2C_MyInit.I2C_Mode = I2C_Mode_I2C;

        if (Config->Flags.Bits.Acknowledgement)  I2C_MyInit.I2C_Ack = I2C_Ack_Enable;
        else                                          I2C_MyInit.I2C_Ack = I2C_Ack_Disable;

        if (Config->Flags.Bits.Adress_10Bit)     I2C_MyInit.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_10bit;
        else                                          I2C_MyInit.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;

        if (Config->Flags.Bits.DutyCycle_16_9)   I2C_MyInit.I2C_DutyCycle = I2C_DutyCycle_16_9;
        else                                          I2C_MyInit.I2C_DutyCycle = I2C_DutyCycle_2;

        I2C_Init((I2C_TypeDef*) I2C_Base, &I2C_MyInit);

        // do stuff that was not done by standard_peripherals_library
        I2C_Base->CCR.Fast = Config->Flags.Bits.ModeFast;
    }
    if (1) { // configure GPIOs

        if (0) { // test pins
            ttc_gpio_init(ConfigArch->PortSDA, tgm_output_open_drain, tgs_Max);
            ttc_gpio_init(ConfigArch->PortSCL, tgm_output_open_drain, tgs_Max);
            while(1){
                // ttc_gpio_set(ConfigArch->PortSDA);

                ttc_gpio_set(ConfigArch->PortSCL);
                ttc_gpio_set(PIN_PB8);
                ttc_gpio_set(PIN_PB9);
                uSleep(10);

                ttc_gpio_clr(PIN_PB8);
                ttc_gpio_clr(PIN_PB9);
                uSleep(10);
            }
            while (1) {

                ttc_gpio_set(ConfigArch->PortSDA);
                ttc_gpio_set(ConfigArch->PortSCL);
                uSleep(10);
                ttc_gpio_clr(ConfigArch->PortSCL);
                uSleep(10);
                ttc_gpio_clr(ConfigArch->PortSDA);
                ttc_gpio_set(ConfigArch->PortSCL);
                uSleep(10);
                ttc_gpio_clr(ConfigArch->PortSCL);
                uSleep(10);
            }
        }

        if (ConfigArch->PortSDA)
            ttc_gpio_init(ConfigArch->PortSDA, tgm_alternate_function_open_drain, tgs_Max);

        if (ConfigArch->PortSCL)
            ttc_gpio_init(ConfigArch->PortSCL, tgm_alternate_function_open_drain, tgs_Max);

        if (ConfigArch->PortSMBAL && Config->Flags.Bits.SMBus_Alert)
            ttc_gpio_init(ConfigArch->PortSMBAL,  tgm_alternate_function_open_drain,  tgs_Max);
    }

    return tie_OK;
}
ttc_i2c_errorcode_e _stm32_i2c_condition_start(ttc_i2c_config_t* Config) {
    Assert_I2C(Config, ec_NULL);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;                          Assert_I2C(I2C_Base, ec_NULL);

    I2C_Base->CR1.START = 1;
    u16_t MyTimeOut = Config->TimeOut;
    while ( (MyTimeOut > 0) && I2C_Base->CR1.START) {
        MyTimeOut--;
        ttc_task_yield();
    }

    if (MyTimeOut > 0)  return tie_OK;

    return tie_TimeOut_StopCondition;
}
ttc_i2c_errorcode_e _stm32_i2c_condition_stop(ttc_i2c_config_t* Config) {
    Assert_I2C(Config, ec_NULL);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;                          Assert_I2C(I2C_Base, ec_NULL);

    I2C_Base->CR1.STOP = 1;
    u16_t MyTimeOut = Config->TimeOut;
    while ( (MyTimeOut > 0) && I2C_Base->CR1.STOP) {
        MyTimeOut--;
        ttc_task_yield();
    }

    if (MyTimeOut > 0)  return tie_OK;

    return tie_TimeOut_StopCondition;
}
ttc_i2c_errorcode_e _stm32_i2c_send_target_address(ttc_i2c_config_t* Config, u16_t TargetAddress, ttc_i2c_direction_e Direction) {
    Assert_I2C(Config, ec_NULL);
    stm32_i2c_architecture_t* ConfigArch = Config->Architecture; Assert_I2C(ConfigArch, ec_NULL);
    register_stm32f1xx_i2c_t* I2C_Base = ConfigArch->Base;                          Assert_I2C(I2C_Base, ec_NULL);

    ttc_i2c_Address_7Bit_u Data;
    if (Direction == tid_WritingToSlave)
        Data.Fields.ReadMode = 0;               // writing to slave
    else
        Data.Fields.ReadMode = 1;               // reading from slave

    Data.Fields.Address = TargetAddress;

    if (0) {
        if (0) I2C_SendData( (I2C_TypeDef*) I2C_Base, Data.Byte);
        else   *( (u8_t*) &(I2C_Base->DR) ) = Data.Byte;
        u16_t MyTimeOut = Config->TimeOut;
        while ( (MyTimeOut > 0) && (!I2C_Base->SR1.ADDR) ) { // EV6
            MyTimeOut--;
            ttc_task_yield();
        }
        if (MyTimeOut == 0) return tie_TimeOut_Master_TransmitterMode;
        I2C_Base->CR1.PE = 1;  // reset EV6
        MyTimeOut = Config->TimeOut;
        while ( (MyTimeOut > 0) && (!I2C_Base->SR1.TxE) ) { // EV8_1
            MyTimeOut--;
            // Read from SR1 and SR2 register to clear all flags
            (void) I2C_Base->SR1;
            (void) I2C_Base->SR2;
            ttc_task_yield();
        }

        if (MyTimeOut == 0) return tie_TimeOut_Master_TransmitterMode;

        if (tie_OK != _stm32_i2c_wait_for_event(Config, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) )
            return tie_TimeOut_Master_TransmitterMode;
    }
    else {
        *( (u8_t*) &(I2C_Base->DR) ) = Data.Byte;
        u16_t MyTimeOut = Config->TimeOut;
        while ( (MyTimeOut-- > 0) && (!I2C_CheckEvent( (I2C_TypeDef*) I2C_Base, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) )
            ttc_task_yield();
        if (MyTimeOut == 0)
            return tie_TimeOut_Master_TransmitterMode;
    }

    return tie_OK;
}

//} private functions
