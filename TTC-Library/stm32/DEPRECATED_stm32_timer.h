#ifndef STM32_TIMER_H
#define STM32_TIMER_H

/** { stm32_timer.h **********************************************
 
                           The ToolChain
                      
   High-Level interface for TIMER device.

   Structures, Enums and Defines being required by high-level timer and application.

   Note: See ttc_timer.h for description of stm32 independent TIMER implementation.
  
   Authors: 

 
}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

#include "stm32_timer_types.h"
#include "../ttc_timer_types.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"

//} Includes
//{ Macro definitions ****************************************************

#define Driver_timer_load_defaults(Config)      stm32_timer_load_defaults(Config)
#define Driver_timer_deinit(Config)             stm32_timer_deinit(Config)
#define Driver_timer_init(Config)               stm32_timer_init(Config)
#define Driver_timer_hardware_reset(Config)     stm32_timer_reset(Config)
#define Driver_timer_check_initialized(Config)  DEPRECATED_stm32_timer.check_initialized(Config)

//} Includes
//{ Function prototypes **************************************************

/** checks if indexed TIMER bus already has been initialized
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == TRUE: indexed TIMER unit is already initialized
 */
bool DEPRECATED_stm32_timer.check_initialized(ttc_timer_config_t* Config);

/** returns reference to low-level configuration struct of indexed TIMER device
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              pointer to struct ttc_timer_config_t (will assert if no configuration available)
 */
DEPRECATED_stm32_timer.config_t* stm32_timer_get_configuration(ttc_timer_config_t* Config);

/** loads configuration of indexed TIMER interface with default values
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
ttc_timer_errorcode_e stm32_timer_load_defaults(ttc_timer_config_t* Config);

/** fills out given Config with maximum valid values for indexed TIMER
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
ttc_timer_errorcode_e stm32_timer_get_features(ttc_timer_config_t* Config);

/** reset configuration of indexed TIMER device into default state
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
ttc_timer_errorcode_e stm32_timer_reset(ttc_timer_config_t* Config);

/** initializes single TIMER
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
ttc_timer_errorcode_e stm32_timer_init(TimerIndex, TaskFunction, Argument, TimePeriod);

/** shutdown single TIMER device
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been shutdown successfully; != 0: error-code
 */
ttc_timer_errorcode_e stm32_timer_deinit(ttc_timer_config_t* Config);

/** maps from logical to physical device index
 *
 * High-level timers (ttc_timer_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_TIMERn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be 
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any 
 * order.
 *
 * @param LogicalIndex  logical index of timer device (1..ttc_timer_get_max_index() )
 * @return              physical index of timer device (0 = first physical timer device, ...)
 */
TIM_TypeDef* stm32_timer_physical_index_2_address(u8_t LogicalIndex);
//u8_t stm32_timer_logical_2_physical_index(u8_t LogicalIndex);

/** resets a single TIMER
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
ttc_timer_errorcode_e stm32_timer_reset(ttc_timer_config_t* Config);
//} Function prototypes

#endif //STM32_TIMER_H
