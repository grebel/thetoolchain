/*{ stm32_eeprom_emulation.c -------------------------------------------------------------
 
                      The ToolChain
                      
   Implementation of the STM AN2594 EEProm Emulation in STM32F10x microcontrollers

   Poggemann 2013
}*/

#include "stm32_eeprom_emulation.h"


FLASH_Status FlashStatus;
t_u16 VarValue = 0;

/* Virtual address defined by the user: 0xFFFF value is prohibited */
t_u16 VirtAddVarTab[NumbOfVar] = {0x5555, 0x6666, 0x7777};

BOOL stm32_eeprom_init(void){

    /* Unlock the Flash Program Erase controller */
    FLASH_Unlock();

    /* EEPROM Init */
    EE_Init();

//    /* --- Store successively many values of the three variables in the EEPROM ---*/
//    /* Store 1000 values of Variable1 in EEPROM */
//    for (VarValue = 0; VarValue < 1000; VarValue++)
//    {
//        EE_WriteVariable(VirtAddVarTab[0], VarValue);
//    }

//    /* Store 500 values of Variable2 in EEPROM */
//    for (VarValue = 0; VarValue < 500; VarValue++)
//    {
//        EE_WriteVariable(VirtAddVarTab[1], VarValue);
//    }

//    /* Store 800 values of Variable3 in EEPROM */
//    for (VarValue = 0; VarValue < 800; VarValue++)
//    {
//        EE_WriteVariable(VirtAddVarTab[2], VarValue);
//    }
    return TRUE;
}


//} private functions
