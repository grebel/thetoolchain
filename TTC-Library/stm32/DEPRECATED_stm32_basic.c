/*{ stm32_basic.h ***********************************************
 *
 * TheToolChain.com
 *
 * Written by Gregor Rebel, Sascha Poggemann 2010-2017
 *
 * Basic datatypes, enums and functions additional to cm3_basic.h
 *
}*/

#include "stm32_basic.h"

t_u32 stm32_get_MCU_Device_ID() {
    #ifdef TARGET_ARCHITECTURE_STM32F1xx
    return  *( __IO t_u32* )( 0xE0042000 );
    #else
    #ifdef TARGET_ARCHITECTURE_STM32L1xx
    return  *( __IO t_u32* )( DBGMCU_IDCODE_DEV_ID );
    #else
#warning stm32_get_UID not supported for current architecture
    #endif
    #endif
    return 0;
}
void stm32_get_UID( t_u32* Device_Serial0, t_u32* Device_Serial1, t_u32* Device_Serial2 ) {

    #ifdef TARGET_ARCHITECTURE_STM32F1xx
    #if defined(STM32L1XX_MD) || defined(STM32L1XX_HD) || defined(STM32L1XX_MD_PLUS)
    *Device_Serial0 = *( t_u32* )( 0x1FF80050 );
    *Device_Serial1 = *( t_u32* )( 0x1FF80054 );
    *Device_Serial2 = *( t_u32* )( 0x1FF80064 );
    #else
    *Device_Serial0 = *( __IO t_u32* )( 0x1FFFF7E8 );
    *Device_Serial1 = *( __IO t_u32* )( 0x1FFFF7EC );
    *Device_Serial2 = *( __IO t_u32* )( 0x1FFFF7F0 );
    #endif /* STM32L1XX_XD */
    #else
    #ifdef TARGET_ARCHITECTURE_STM32L1xx

    #else
#warning stm32_get_UID not supported for current architecture
    #endif
    #endif
}
