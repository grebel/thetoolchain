#ifndef STM32_WATCHDOG_H
#define STM32_WATCHDOG_H

/*{ stm32_watchdog.h *****************************************************

  Interface to watchdog unit of CortexM3 microcontrollers.
  Note: For platform independent watchdogs, see ttc_watchdog.*

  written by Gregor Rebel 2012

}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

// Basic set of helper functions
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "stm32f10x_rcc.h"
#include "stm32f10x_iwdg.h"

//} Includes
//{ Structures/ Enums ****************************************************

//} Structures/ Enums
//{ Global Variables *****************************************************



//} Global Variables
//{ Function prototypes **************************************************

/** initializes IWD
 * @param Reload    != 0: sets reload value; == 0: use default reload value
 * @param Prescaler != 0: sets frequency prescaler;  == 0: use default prescaler value
 * @return =0: watchdog has been successfully initialized (>0: error-code)
 */
int stm32_init_IndependendWatchdog( unsigned int Reload, int Prescaler );

/** reset IWD (must be called periodically to avoid reset from initialized watchdog)
 */
void stm32_reload_IndependendWatchdog();

/** deactivate IWD
 */
void stm32_deinit_IndependendWatchdog();

//} Function prototypes

#endif //STM32_WATCHDOG_H
