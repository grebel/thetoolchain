/*{ Template_FreeRTOS::.c ************************************************
 
 Empty template for new c-files.
 Copy and adapt to your needs.
 
}*/

#include "stm32_adc.h"

//{ Function definitions *************************************************

/* Prepares one ADC for later configuration
 *
 * ADCx        ADC1 .. ADC2
 * WithDMA  == TRUE: enable DMA-controller for this ADC
 */
void stm32_adc_init(ADC_TypeDef* ADCx, BOOL WithDMA) {
    /* Enable ADC clocks */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
#if defined (STM32F10X_LD_VL) || defined (STM32F10X_MD_VL)
    RCC_ADCCLKConfig(RCC_PCLK2_Div2);  // ADCCLK=PCLK2/2
#else
    //? RCC_ADCCLKConfig(RCC_PCLK2_Div4);  // ADCCLK=PCLK2/4
    RCC_ADCCLKConfig(RCC_PCLK2_Div6);
#endif

    if (WithDMA)
        RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1,   ENABLE); // ToDo: check if correct for ADCx==ADC2

    if (ADCx == ADC1)
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    else
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE);

    ADC_DeInit(ADCx);

}

//} Function definitions

/* initialize single ADC channel with/ without DMA transfer
 * ADCx        ADC1 .. ADC2
 * AdcChannel  ADC_Channel_0 .. ADC_Channel_17
 * Value       != NULL: use DMA to copy sampled value into variable
 */
void stm32_adc_init_dma(ADC_TypeDef* ADCx, u8_t AdcChannel, volatile u16_t* Value) {         // configure Analog Digital Converters
    stm32_adc_init(ADCx,1);
    ADC_InitTypeDef   ADC_InitStructure;
    DMA_InitTypeDef   DMA_InitStructure;

    if (Value) { // DMA channel1 configuration
        DMA_DeInit(DMA1_Channel1);
        DMA_StructInit(&DMA_InitStructure);

        DMA_InitStructure.DMA_PeripheralBaseAddr=(uint32_t)&ADCx->DR; //?ADC1_DR_Address;
        DMA_InitStructure.DMA_MemoryBaseAddr    =(u32_t) Value;
        DMA_InitStructure.DMA_DIR               =DMA_DIR_PeripheralSRC;
        DMA_InitStructure.DMA_BufferSize        =1;
        DMA_InitStructure.DMA_PeripheralInc     =DMA_PeripheralInc_Disable;
        DMA_InitStructure.DMA_MemoryInc         =DMA_MemoryInc_Disable;
        DMA_InitStructure.DMA_PeripheralDataSize=DMA_PeripheralDataSize_HalfWord;
        DMA_InitStructure.DMA_MemoryDataSize    =DMA_MemoryDataSize_HalfWord;
        DMA_InitStructure.DMA_Mode              =DMA_Mode_Circular;
        DMA_InitStructure.DMA_Priority          =DMA_Priority_High;
        DMA_InitStructure.DMA_M2M               =DMA_M2M_Disable;
        DMA_Init(DMA1_Channel1, &DMA_InitStructure);

        // Enable DMA1 channel1
        DMA_Cmd(DMA1_Channel1, ENABLE);
    }

    // ADCx configuration
    ADC_InitStructure.ADC_Mode              =ADC_Mode_Independent;
    ADC_InitStructure.ADC_ScanConvMode      =ENABLE;
    ADC_InitStructure.ADC_ContinuousConvMode=ENABLE;
    ADC_InitStructure.ADC_ExternalTrigConv  =ADC_ExternalTrigConv_None;
    ADC_InitStructure.ADC_DataAlign         =ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfChannel      =1;
    ADC_Init(ADCx, &ADC_InitStructure);
    ADC_RegularChannelConfig(ADCx, AdcChannel, 1, ADC_SampleTime_55Cycles5);

    if (Value) ADC_DMACmd(ADCx, ENABLE);         // Enable ADCx DMA
    ADC_Cmd(ADCx, ENABLE);                       // Enable ADCx
    ADC_SoftwareStartConvCmd(ADCx, ENABLE);
}

/* read value from single ADC channel
 *
 * Note: blocks until ADC has finished sampling!
 *
 * ADCx        ADC1 .. ADC2
 * AdcChannel  ADC_Channel_0 .. ADC_Channel_17
 * return      sampled value
 */
unsigned int stm32_adc_read_single_ch(ADC_TypeDef* ADCx, unsigned char Channel) {
    // Configure channel
    ADC_RegularChannelConfig(ADCx, Channel, 1, ADC_SampleTime_55Cycles5);

    // Start the conversion
    ADC_SoftwareStartConvCmd(ADCx, ENABLE);

    // Wait until conversion completion
    while (ADC_GetFlagStatus(ADCx, ADC_FLAG_EOC) == RESET);

    // Get the conversion value
    return ADC_GetConversionValue(ADCx);
}

/* deinitialize single ADC channel
 * ADCx        ADC1 .. ADC2
 * AdcChannel  ADC_Channel_0 .. ADC_Channel_17
 *
 *
 *
 */
void stm32_adc_deinit(ADC_TypeDef* ADCx){
 if (ADCx == ADC1)
  {
    /* Enable ADC1 reset state */
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_ADC1, ENABLE);
    /* Release ADC1 from reset state */
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_ADC1, DISABLE);
  }
  else if (ADCx == ADC2)
  {
    /* Enable ADC2 reset state */
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_ADC2, ENABLE);
    /* Release ADC2 from reset state */
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_ADC2, DISABLE);
  }
  else
  {
    if (ADCx == ADC3)
    {
      /* Enable ADC3 reset state */
      RCC_APB2PeriphResetCmd(RCC_APB2Periph_ADC3, ENABLE);
      /* Release ADC3 from reset state */
      RCC_APB2PeriphResetCmd(RCC_APB2Periph_ADC3, DISABLE);
    }
  }
    return;
}
/* initialize single ADC pin in single mode*/
void stm32_adc_init_single(ADC_TypeDef *ADC)
{
    ADC_InitTypeDef ADC_InitStructure;

    stm32_adc_deinit(ADC);                                                             // ADC auf Init-Werte rücksetzen
    if (ADC == ADC1)
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    else
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE);                         // ADC Takt freigeben

    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;                            // Arbeitsweise unabhängig
    ADC_InitStructure.ADC_ScanConvMode = DISABLE;                                 // kein Scan-Mode (nur 1 Kanal verwendet)
    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;                            // kontinuierliche Abtastung
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;           // keine externe Triggerung
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;                        // Datenformat der Wandlung (rechtsbündig 12-Bit)
    ADC_InitStructure.ADC_NbrOfChannel = 1;                                       // 1 Kanal
    ADC_Init(ADC, &ADC_InitStructure);                                           // ADC initialisieren

    ADC_Cmd(ADC, ENABLE);                                                        // ADC freigeben

    ADC_ResetCalibration(ADC);                                                   // ADC Kalibrierungswerte rücksetzen
    while(ADC_GetResetCalibrationStatus(ADC));                                   // Ende abwarten

    ADC_StartCalibration(ADC);                                                   // ADC Kalibrierung
    while(ADC_GetCalibrationStatus(ADC));                                        // Ende abwarten

    ADC_SoftwareStartConvCmd(ADC, ENABLE);                                       // ADC Abtastung starten

    /*
    ADC_InitTypeDef ADC_InitStructure;

    ADC_DeInit(ADC);

    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
    ADC_InitStructure.ADC_ScanConvMode = ENABLE;
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfChannel = 1;
    ADC_Init(ADC, &ADC_InitStructure);
    */

}
/* reactivate single adc pin, if it was deactivative by an digital pin activation for example or activate single adc pin*/

/* get the value of a ADC channel in single conversation mode*/
u16_t stm32_adc_get_adc_value(u8_t Channel, ADC_TypeDef* ADC){

    // 1Cycles5, 7Cycles5, 13Cycles5, 28Cycles5, 41Cycles5, 55Cycles5, 71Cycles5, 239Cycles5
    ADC_RegularChannelConfig(ADC, Channel, 1, ADC_SampleTime_28Cycles5);  // Kanalauswahl 11=PC1
    uSleep(80); // ca. 80us Pause

#if 0
    /* Enable ADC reset calibaration register */
    ADC_ResetCalibration(ADC);
    /* Check the end of ADC reset calibration register */
    while(ADC_GetResetCalibrationStatus(ADC));

    /* Start ADC calibaration */
    ADC_StartCalibration(ADC);
    /* Check the end of ADC calibration */
    while(ADC_GetCalibrationStatus(ADC));
#endif
    /* Start ADC Software Conversion */
    while (ADC_GetFlagStatus(ADC,ADC_FLAG_EOC) == RESET);
    return ADC_GetConversionValue(ADC);   // ADC-Daten (int) abholen, gleichzeitig wird EOC rückgesetzt

}

