/*{ Template_FreeRTOS::.h ************************************************
 
 Empty template for new header files.
 Copy and adapt to your needs.
 
}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

 // Basic set of helper functions
#include "../ttc_basic.h"
#include "../ttc_task.h"
#include "../ttc_gpio.h"


#define USE_STDPERIPH_DRIVER
#include "stm32f10x.h"
#include "core_cm3.h"
#include "stm32f10x_conf.h"

// ADC
#include "stm32f10x_adc.h"
#include "stm32f10x_dma.h"

/*{ optional

// Timers
#include "stm32f10x_tim.h"

#include "stm32f10x_it.h"
#include "system_stm32f10x.h"
}*/
/* ???
#include "bits.h"

#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
*/

//} Includes
//{ Structures/ Enums ****************************************************

//} Structures/ Enums
//{ Global Variables *****************************************************



//} Global Variables
//{ Function prototypes **************************************************

#define _driver_ttc_adc_init(ADCx,WithDMA) stm32_adc_init(ADCx,WithDMA)
#define _driver_ttc_adc_init_dma(ADCx,AdcChannel,Value) stm32_adc_init_dma(ADCx,AdcChannel,Value)
#define _driver_ttc_adc_read_single_ch(ADCx,Channel) stm32_adc_read_single_ch(ADCx,Channel)
#define _driver_ttc_adc_init_single(ADC) stm32_adc_init_single(ADC)
#define _driver_ttc_adc_deinit(ADCx) stm32_adc_deinit(ADCx)
#define _driver_ttc_adc_activate_analog_single(Bank,Pin) stm32_adc_activate_analog_single(Bank,Pin)
#define _driver_ttc_adc_get_adc_value(Channel,ADC) stm32_adc_get_adc_value(Channel,ADC)

void stm32_adc_init(ADC_TypeDef* ADCx, BOOL WithDMA);
void stm32_adc_init_dma(ADC_TypeDef* ADCx, u8_t AdcChannel, volatile u16_t* Value);
unsigned int stm32_adc_read_single_ch(ADC_TypeDef* ADCx, unsigned char Channel);
void stm32_adc_deinit(ADC_TypeDef* ADCx);
void stm32_adc_init_single(ADC_TypeDef* ADC);
void stm32_adc_activate_analog_single(gpio_stm32f1xx_bank_e GPIOx, u8_t Pin);
u16_t stm32_adc_get_adc_value(u8_t Channel, ADC_TypeDef* ADC);
//} Function prototypes
