#ifndef STM32_IO_H
#define STM32_IO_H

//{ includes

#include "../ttc_basic.h"

//}includes
//{ commonly used defines (required by FreeRTOS and/or StdPeripheralsLibrary)

//} Defines

t_u16* arraySet( t_u16* Array, t_u16 Value, t_u32 Amount );
bool circa( t_u32 A, t_u32 B, t_u8 Distance );
t_u16 toggleValue( t_u16 Value, t_u16 Max, t_u16 Min );

// limits given
t_s16 sLimit( t_s16 Value, t_s16 Min, t_s16 Max );
t_u16 uLimit( t_u16 Value, t_u16 Min, t_u16 Max );

// malloc replacement; uses Assert() for out-of-memory detection
void* myMalloc( t_base Size );


#endif
