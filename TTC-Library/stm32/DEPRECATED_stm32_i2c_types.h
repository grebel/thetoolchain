#ifndef STM32_I2C_TYPES_H
#define STM32_I2C_TYPES_H

/**{ ttc_i2c_types.h **********************************************************
 
                      The ToolChain
                      
   Universal Synchronous Asynchronous Receiver and Transmitters (I2Cs)
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Gregor Rebel 2012

   Basic data types.

}*/
//{ Includes *************************************************************

#include "ttc_mutex.h"
#include "../gpio/gpio_stm32f1xx.h"
#include "../register/register_stm32f1xx.h"

//} Includes
//{ Structures/ Enums ****************************************************

typedef struct {  // architecture specific configuration data of single I2C 
  register_stm32f1xx_i2c_t*       Base;              // base TargetAddress of I2C registers
  ttc_gpio_pin_e PortSCL;           // port pin for SCL
  ttc_gpio_pin_e PortSDA;           // port pin for SDA
  ttc_gpio_pin_e PortSMBAL;         // port pin for SMBAL
  ttc_mutex_smart_t * I2C_HW_Lock;  //protect I2C interface against simultaneous access from multiple tasks.
} __attribute__((__packed__)) stm32_i2c_architecture_t;

//typedef stm32_i2c_architecture_t ttc_i2c_architecture_t;
#define ttc_i2c_architecture_t stm32_i2c_architecture_t

//} Structures/ Enums
//{ Global Variables *****************************************************



//} Global Variables

#endif //STM32_I2C_TYPES_H
