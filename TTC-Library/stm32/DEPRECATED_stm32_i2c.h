#ifndef STM32_I2C_H
#define STM32_I2C_H

/** { stm32_i2c.h **********************************************************
 
                      The ToolChain
                      
   Universal Synchronous Asynchronous Receiver and Transmitters (I2Cs)
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Gregor Rebel 2012


   Basic usage:
   ttc_i2c_config_t* Config = stm32_i2c_get_configuration(I2C_Index);
   // change Config...
   Assert(stm32_i2c_init(I2C_Index) == tie_OK, ec_InvalidConfiguration);
   // use I2C

}*/
//{ Includes *************************************************************

#include "../ttc_i2c_types.h"
#include "../ttc_task.h"
#include "../ttc_gpio.h"
#include "../ttc_memory.h"

//X #include "../gpio/gpio_stm32f1xx.h"
#include "../register/register_stm32f1xx.h"

#define USE_STDPERIPH_DRIVER
#include "stm32f10x.h"
#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
#include "stm32f10x_conf.h"
#include "stm32f10x_i2c.h"
#include "misc.h"

//} Includes
//{ Defines/ TypeDefs ****************************************************
//} Defines
//{ Structures/ Enums ****************************************************

//} Structures/ Enums
//{ Global Variables *****************************************************



//} Global Variables
//{ Function prototypes **************************************************

/** resets library. Automatically called.
 */
void stm32_i2c_reset_all();

/** returns reference to configuration struct of indexed I2C device
 * @param I2C_Index     device index of I2C device to init (1..stm32_getMax_I2C_Index())
 * @return              != NULL: pointer to struct ttc_i2c_config_t
 */
ttc_i2c_config_t* stm32_i2c_get_configuration(u8_t I2C_Index);

/** fills out given I2C_ with default values for indexed I2C
 * @param I2C_Index     device index of I2C to init (1..stm32_i2c_get_MaxIndex())
 * @param Config   pointer to struct ttc_i2c_config_t
 * @return  == 0:         *Config has been initialized successfully; != 0: error-code
 */
ttc_i2c_errorcode_e stm32_i2c_get_defaults(u8_t I2C_Index, ttc_i2c_config_t* Config);

/** fills out given Config with maximum valid values for indexed I2C
 * @param I2C_Index     device index of I2C to init (1..stm32_i2c_get_MaxIndex())
 * @param Config   pointer to struct ttc_i2c_config_t
 * @return  == 0:         *Config has been initialized successfully; != 0: error-code
 */
ttc_i2c_errorcode_e stm32_i2c_get_features(u8_t I2C_Index, ttc_i2c_config_t* Config);

/** initializes single I2C
 * Note: *I2C_Index and *Config must stay in memory during operation of this I2C-interface!
 *
 * @param I2C_Index     device index of I2C to init (1..stm32_i2c_get_MaxIndex())
 * @param Config        filled out struct ttc_i2c_config_t                         (Note: referenced struct must stay in memory as long as device is in use!)
 * @param I2C_Arch      filled out struct stm32_i2c_architecture_t                  (Note: referenced struct must stay in memory as long as device is in use!)
 * @return  == 0:       I2C has been initialized successfully; != 0: error-code
 */
ttc_i2c_errorcode_e stm32_i2c_init(u8_t I2C_Index);

/** Send given byte to I2C device with TargetAddress TargetAddress to be stored in given register-
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param I2C_Index      device index of I2C to use (1..ttc_i2c_get_max_index())
 * @param TargetAddress  receivers own TargetAddress
 * @param Register       index of register where to store Byte in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Byte           8 bit value to send
 * @param TimeOut        > 0: maximum amount of uSecs to wait for TXE; ==0: no timeout
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_i2c_errorcode_e stm32_i2c_write_register(u8_t I2C_Index, const u16_t TargetAddress, const u8_t Register, ttc_i2c_register_type_e RegisterType, const u8_t Byte);

/** Send out given amount of raw bytes from buffer.
 * Will not interpret bytes in buffer.
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * 
 * @param I2C_Index      device index of I2C to init (0..stm32_i2c_get_MaxIndex()-1)
 * @param TargetAddress  receivers own TargetAddress
 * @param Register       index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Buffer         memory location from which to read data to be send to slave device
 * @param Amount         amount of bytes to send from Buffer[]
 * @param TimeOut        > 0: maximum amount of uSecs to wait for TXE; ==0: no timeout
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_i2c_errorcode_e stm32_i2c_write_registers(u8_t I2C_Index, const u16_t TargetAddress, const u8_t Register, ttc_i2c_register_type_e RegisterType, const u8_t* Buffer, u16_t Amount);


/** Reads single data byte from input buffer.
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param I2C_Index      device index of I2C to init (1..ttc_i2c_get_max_index())
 * @param TargetAddress  TargetAddress of device to read from
 * @param Register       index of register to read from in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Byte           pointer to 8 bit buffer where to store 1 byte
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return               == 0: Byte has been read successfully; != 0: error-code
 */
ttc_i2c_errorcode_e stm32_i2c_read_register(u8_t I2C_Index, const u16_t TargetAddress, const u8_t Register, ttc_i2c_register_type_e RegisterType, u8_t* Buffer);

/** Reads single data byte from input buffer.
 *
 * Note: I2C must be initialized before!
 * Note: This function is partially thread safe (use it for each I2C from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param I2C_Index      device index of I2C to init (1..ttc_i2c_get_max_index())
 * @param TargetAddress  TargetAddress of device to read from
 * @param Register       index of register to read from in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @param Buffer         pointer to 8 bit buffer where to store bytes
 * @param Amount         amount of bytes to be read from target device
 * @param AmountRead     will be loaded with amount of bytes being read
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return               == 0: Byte has been read successfully; != 0: error-code
 */
ttc_i2c_errorcode_e stm32_i2c_read_registers(u8_t I2C_Index, const u16_t TargetAddress, const u8_t Register, ttc_i2c_register_type_e RegisterType, u8_t* Buffer, u16_t Amount, u16_t* AmountRead);

/** load pin configuration into given structure
 * @return: index of pin layout being determined
 */
u8_t _stm32_i2c_get_pins(u8_t I2C_Index, stm32_i2c_architecture_t* I2C_Arch);

/** Wait until Byte has been received or timeout occured.
 * Note: This low-level function should not be called from outside!
 *
 * @param I2C_Arch       entry from stm32_I2C_Configs[] of preconfigured I2C unit to use
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: byte has been received within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _stm32_i2c_wait_for_RXNE(ttc_i2c_config_t* Config);

/** Generic wait for I2C flag.
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Flag           bitmask of flags to check
 * @param WaitForState   flag state to wait for
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: flag set/reset within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _stm32_i2c_wait_for_flag(ttc_i2c_config_t* Config, u32_t Flag, FlagStatus WaitForState);

/** Generic wait for I2C event.
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Flag           bitmask of flags to check
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: event occured within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _stm32_i2c_wait_for_event(ttc_i2c_config_t* Config, u32_t Event);

/** Reads in given amount of bytes from slave (low level read; does not generate Start- or Stop-event)
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Buffer         memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[]
 * @param AmountRead     increased on every byte being received successfully
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: all bytes successfully read  within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _stm32_i2c_read_bytes(ttc_i2c_config_t* Config, u8_t* Buffer, u16_t Amount, u16_t* AmountRead);

/** Reads in single byte from slave (low level receive; does not generate Start- or Stop-event)
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Buffer         8-bit storage for byte to read
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: byte successfully read within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _stm32_i2c_read_byte(ttc_i2c_config_t* Config, u8_t* Buffer);

/** Sends out register address to slave device
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Register       index of start register where to store bytes in receiving device
 * @param RegisterType   size and order of register adresses (amount of bytes to transfer for each register adress)
 * @return:              == 0: register sent successfully within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _stm32_i2c_send_register_address(ttc_i2c_config_t* Config, u32_t Register, ttc_i2c_register_type_e RegisterType);

/** Sends out given amount of bytes (low level send; does not generate Start- or Stop-event)
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Buffer         memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[]
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: event occured within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _stm32_i2c_send_bytes(ttc_i2c_config_t* Config, const u8_t* Buffer, u16_t Amount);

/** Sends out single byte (low level send; does not generate Start- or Stop-event)
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Byte           8-bit value to send out
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return:              == 0: event occured within timeout; != 0: ttc_i2c_errorcode_e
 */
ttc_i2c_errorcode_e _stm32_i2c_send_byte(ttc_i2c_config_t* Config, const u8_t Byte);

/** closes the I2C bus and brings it back to a defined state.
 * Note: This low-level function should not be called from outside!
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param Status         simply passed as return value
 * @return               Status
 */
ttc_i2c_errorcode_e _stm32_i2c_close_bus(ttc_i2c_config_t* Config, ttc_i2c_errorcode_e Status);
/** triese to push and pull to SCL line to flush the slave internal state machine and bring it back to normal operation
 *
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @return               Status
 */
ttc_i2c_errorcode_e _stm32_i2c_flush_slave(ttc_i2c_config_t* Config);
/** initialize/ reinitialize an I2C unit
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @return               Status
 */
ttc_i2c_errorcode_e _stm32_i2c_reinit_i2c(ttc_i2c_config_t* Config);

/** generate start condition
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return               Status
 */
ttc_i2c_errorcode_e _stm32_i2c_condition_start(ttc_i2c_config_t* Config);

/** generate start condition
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return               Status
 */
ttc_i2c_errorcode_e _stm32_i2c_condition_stop(ttc_i2c_config_t* Config);

/** send target address to slave
 * @param Config         filled out struct ttc_i2c_config_t from stm32_I2C_Configs[]
 * @param TargetAddress  TargetAddress of device to read from
 * @param Direction      =1: next data will be read from slave; =0: next data will be written to slave
 * @param TimeOut        >0: max loop iterations to wait; ==0: no timeout
 * @return               Status
 */
ttc_i2c_errorcode_e _stm32_i2c_send_target_address(ttc_i2c_config_t* Config, u16_t TargetAddress, ttc_i2c_direction_e Direction);

//} Function prototypes

#endif //STM32_I2C_H
