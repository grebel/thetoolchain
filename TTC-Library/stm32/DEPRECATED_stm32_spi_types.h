#ifndef STM32_SPI_TYPES_H
#define STM32_SPI_TYPES_H

/*{ stm32_spi.h **********************************************************
 
                      The ToolChain
                      
   Universal Synchronous Asynchronous Receiver and Transmitters (SPIs)
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Gregor Rebel 2012

 
}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

#include "../gpio/gpio_stm32f1xx.h"
#include "../register/register_stm32f1xx.h"

//} Includes
//{ Structures/ Enums required by ttc_spi_types.h *************************

typedef struct {  // architecture specific configuration data of single SPI 
  register_stm32f1xx_spi_t*       Base;       // base address of SPI registers
  ttc_gpio_pin_e PortMISO;   // port pin for MISO
  ttc_gpio_pin_e PortMOSI;   // port pin for MOSI
  ttc_gpio_pin_e PortSCK;    // port pin for SCK
  ttc_gpio_pin_e PortNSS;    // port pin for NSS
} __attribute__((__packed__)) stm32_spi_architecture_t;

typedef stm32_spi_architecture_t ttc_spi_architecture_t;

//} Structures/ Enums


#endif //STM32_SPI_TYPES_H
