/*{ stm32_usb::.c -------------------------------------------------------------
 
                      The ToolChain
                      
   Universal Synchronous Asynchronous Receiver and Transmitters (usbs)
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Gregor Rebel 2012
   
   Note: See ttc_usb.h for description of architecture independent usb implementation.
 
}*/

#include "stm32_usb.h"


extern void _ttc_usb_receive_buffer(t_u8 LogicalIndex, t_u8 * Buffer, t_u8 Amount); // implemented in ttc_usb.c

// stores pointers to structures of initialized usbs/ UARTs
// Note: index is not real, but logical usb-index as defined by makefile!
//static stm32_usb_architecture_t* stm32_USBs[TTC_AMOUNT_USBS];

#ifdef EXTENSION_400_stm32_usb_fs_host_lib
/** @defgroup USBH_USR_MAIN_Private_Variables
* @{
*/
__ALIGN_BEGIN USB_OTG_CORE_HANDLE           USB_OTG_Core_dev __ALIGN_END ;
__ALIGN_BEGIN USBH_HOST                     USB_Host __ALIGN_END ;

#endif
//{ low-level driver interface (mandatory functions) ----------------------------
t_u8  USB_TX_Buffer [USB_TX_BUFFER_SIZE];
t_u32 USB_TX_ptr_in = 0;
t_u32 USB_TX_ptr_out = 0;
t_u32 Xfer_USB_TX_lenght  = 0; //replace with TX_lenght
t_u8  XferUSB_Tx_State = 0;

e_ttc_usb_errorcode stm32_usb_get_defaults(t_u8 PhysicalIndex, t_ttc_usb_generic* USB_Generic) {

    Assert_USB(FALSE, ttc_assert_origin_auto);
    return ec_usb_OK;
}
e_ttc_usb_errorcode stm32_usb_get_features(t_u8 PhysicalIndex, t_ttc_usb_generic* USB_Generic) {

    Assert_USB(FALSE, ttc_assert_origin_auto);
    return ec_usb_OK;
}
e_ttc_usb_errorcode stm32_usb_init(t_ttc_usb_generic* USB_Generic) {


#ifdef EXTENSION_400_stm32_usb_fs_device_lib  //ToDo add stm32_usb layer
    if (1) Set_System();
    if (1) Set_USBClock();
    if (1) stm32_usb_interrupt_config(USB_Generic->LogicalIndex);
    if (1) USB_Init();

#elif EXTENSION_400_stm32_usb_fs_host_lib


    /*!< At this stage the microcontroller clock setting is already configured,
    this is done through SystemInit() function which is called from startup
    file (startup_stm32fxxx_xx.s) before to branch to application main.
    To reconfigure the default setting of SystemInit() function, refer to
    system_stm32fxxx.c file
    */

    /* Init Host Library */
    USBH_Init(&USB_OTG_Core_dev,
  #ifdef USE_USB_OTG_FS
              USB_OTG_FS_CORE_ID,
  #else
              USB_OTG_HS_CORE_ID,
  #endif
              &USB_Host,
              &HID_cb,
              &USR_Callbacks);

//    while (1)
//    {
//      /* Host Task handler */
      USBH_Process(&USB_OTG_Core_dev , &USB_Host);//ToDo spawn new task for USB Host processing

#endif

    return ec_usb_OK;
}

e_ttc_usb_errorcode stm32_usb_interrupt_config(t_u8 PhysicalIndex){

    (void) PhysicalIndex;
    NVIC_InitTypeDef NVIC_InitStructure;

    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

#if defined(STM32L1XX_MD) || defined(STM32L1XX_HD) || defined(STM32L1XX_MD_PLUS)
    NVIC_InitStructure.NVIC_IRQChannel = USB_LP_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    return ec_usb_OK;
#elif defined(STM32F10X_CL)
    /* Enable the USB Interrupts */
    NVIC_InitStructure.NVIC_IRQChannel = OTG_FS_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    return ec_usb_OK;
#else
    NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    return ec_usb_OK;
#endif /* STM32L1XX_XD */

return tusbe_NotImplemented;
}


e_ttc_usb_errorcode stm32_usb_send_raw(t_ttc_usb_generic * USB_Generic, t_u8* Buffer, t_base Amount){


#ifdef EXTENSION_400_stm32_usb_fs_device_lib

    for (int i = 0; i < Amount; i++) {
        USB_TX_Buffer[USB_TX_ptr_in] = Buffer[i];
        USB_TX_ptr_in++;

        /* To avoid buffer overflow */
        if(USB_TX_ptr_in == USB_TX_BUFFER_SIZE)
        {
            USB_TX_ptr_in = 0;
        }
    }

#else
    return tusbe_NotImplemented;
#endif
    if(USB_Generic->TxFunction_UCB)
        USB_Generic->TxFunction_UCB((e_ttc_usb_errorcode *) Buffer, (t_u8) Amount);

    return ec_usb_OK;
}
e_ttc_usb_errorcode stm32_usb_send_word_blocking(t_ttc_usb_generic* USB_Generic, const t_u16 Word) {

    Assert_USB(FALSE, ttc_assert_origin_auto);
    return ec_usb_OK;
}
e_ttc_usb_errorcode stm32_usb_read_byte_blocking(t_ttc_usb_generic* USB_Generic, t_u8* Byte) {

    Assert_USB(FALSE, ttc_assert_origin_auto);
    return ec_usb_OK;
}
e_ttc_usb_errorcode stm32_usb_read_word_blocking(t_ttc_usb_generic* USB_Generic, t_u16* Word) {

    Assert_USB(FALSE, ttc_assert_origin_auto);
    return ec_usb_OK;
}


//}FunctionDefinitions
//{ private functions (ideally) -------------------------------------------------

void _stm32_usb_data_rx(t_u8 * USB_RX_Buffer,t_u8 USB_Rx_Cnt){

    _ttc_usb_receive_buffer(1, USB_RX_Buffer, USB_Rx_Cnt);
}


/*******************************************************************************
* Function Name  : _stm32_usb_Handle_USBAsynchXfer.
* Description    : send data to USB.
* Input          : None.
* Return         : none.
*******************************************************************************/

void _stm32_usb_Handle_USBAsynchXfer (void)
{
    t_u16 USB_Tx_ptr;
    t_u16 USB_Tx_length;

    if(XferUSB_Tx_State != 1)
    {
      if (USB_TX_ptr_out == USB_TX_BUFFER_SIZE)
      {
        USB_TX_ptr_out = 0;
      }

      if(USB_TX_ptr_out == USB_TX_ptr_in)
      {
        XferUSB_Tx_State = 0;
        return;
      }

      if(USB_TX_ptr_out > USB_TX_ptr_in) /* rollback */
      {
        Xfer_USB_TX_lenght = USB_TX_BUFFER_SIZE - USB_TX_ptr_out;
      }
      else
      {
        Xfer_USB_TX_lenght = USB_TX_ptr_in - USB_TX_ptr_out;
      }

      if (Xfer_USB_TX_lenght > USB_TX_BUFFER_SIZE)
      {
        USB_Tx_ptr = USB_TX_ptr_out;
        USB_Tx_length = USB_TX_BUFFER_SIZE;

        USB_TX_ptr_out += USB_TX_BUFFER_SIZE;
        Xfer_USB_TX_lenght -= USB_TX_BUFFER_SIZE;
      }
      else
      {
        USB_Tx_ptr = USB_TX_ptr_out;
        USB_Tx_length = Xfer_USB_TX_lenght;

        USB_TX_ptr_out += Xfer_USB_TX_lenght;
        Xfer_USB_TX_lenght = 0;
      }
      XferUSB_Tx_State = 1;

  #ifdef USE_STM3210C_EVAL
      USB_SIL_Write(EP1_IN, &USB_TX_Buffer[USB_Tx_ptr], USB_Tx_length);
  #else
#   ifdef EXTENSION_400_stm32_usb_fs_device_lib

  //ToDo: support with Toolchain STM32 :Poggemann
      UserToPMABufferCopy(&USB_TX_Buffer[USB_Tx_ptr], ENDP1_TXADDR, USB_Tx_length);
      SetEPTxCount(ENDP1, USB_Tx_length);
      SetEPTxValid(ENDP1);
#   endif
  #endif /* USE_STM3210C_EVAL */
    }
}


void _stm32_usb_rx_isr(t_u8 PhysicalIndex, void* Argument) {
    Assert_USB(FALSE, ttc_assert_origin_auto);
}
void _stm32_usb_tx_isr(t_u8 PhysicalIndex, void* Argument) {
    Assert_USB(FALSE, ttc_assert_origin_auto);
}

//} private functions
