/*{ cm3_basic.h ***********************************************
 *
 * TheToolChain.com
 *
 * Written by Gregor Rebel, Sascha Poggemann 2010-2017
 *
 * Basic datatypes, enums and functions additional to cm3_basic.h
 *
}*/

#ifndef STM32_BASIC_H
#define STM32_BASIC_H

//{ includes *************************************************************

#include "../ttc_basic.h"

#ifdef TARGET_ARCHITECTURE_STM32F1xx
#include "stm32f10x.h"
#include "system_stm32f10x.h"
#endif

#ifdef TARGET_ARCHITECTURE_STM32L1xx
#include "stm32l1xx.h"
#include "system_stm32l1xx.h"
#endif

#include "../register/register_stm32f1xx_types.h"

//} includes *************************************************************
//{ types ****************************************************************

//} types ****************************************************************
//{ includes2 ************************************************************


//} includes2 ************************************************************
//{ Function prototypes **************************************************

/** Returns the MCU Device ID
  *
  * @return         the MCU Device ID
  */
t_u32 stm32_get_MCU_Device_ID();

/** read the STM32 Unique device ID registers
  *
  * @param t_u32 * Device_Serial0  part of the 96bit UID
  * @param t_u32 * Device_Serial1  part of the 96bit UID
  * @param t_u32 * Device_Serial2  part of the 96bit UID
  */
void stm32_get_UID( t_u32* Device_Serial0, t_u32* Device_Serial1, t_u32* Device_Serial2 );
//} Function prototypes **************************************************

#endif
