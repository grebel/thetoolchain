#ifndef stm32_gpio_H
#define stm32_gpio_H

/*{ ../gpio/gpio_stm32f1xx.h ***********************************************
 
 * Written by Gregor Rebel 2010-2012
 *
 * Low Level Driver for General Purpose Input Output (GPIO) on STM32 architecture.
 * 
}*/
//{ includes1

#ifndef EXTENSION_250_stm_std_peripherals
  #error EXTENSION_250_stm_std_peripherals not defined -> activate.200_cpu_stm32f10x_std_peripherals.sh
#endif

#include "../ttc_basic.h"
#include "../register/register_stm32f1xx.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"

//}includes
//{ Structures/ Enums

typedef enum {
    stm32_gpio_None,
    stm32_GPIOA = (u32_t) GPIOA,
    stm32_GPIOB = (u32_t) GPIOB,
    stm32_GPIOC = (u32_t) GPIOC,
    stm32_GPIOD = (u32_t) GPIOD,
    stm32_GPIOE = (u32_t) GPIOE,
    stm32_GPIOF = (u32_t) GPIOF,
    stm32_GPIOG = (u32_t) GPIOG,
    stm32_gpio_NotImplemented
} gpio_stm32f1xx_bank_e;

typedef struct { // tgp_e
    gpio_stm32f1xx_bank_e GPIOx;
    u8_t Pin;
    volatile u32_t* BSRR;  // bit-band address of BSRR register of this port bit
    volatile u32_t* BRR;   // bit-band address of BRR register of this port bit
    volatile u32_t* IDR;   // bit-band address of IDR register of this port bit
} __attribute__((__packed__)) tgp_e;

//}Structures/ Enums
//{ Architecture-Defines (required by ttc_gpio_types.h)

// amount of gpio banks (A..G)
#define TTC_GPIO_MAX_AMOUNT 7

// amount of port pins per bank
#define TTC_GPIO_MAX_PINS 16

#define ttc_gpio_bank_t  gpio_stm32f1xx_bank_e
#define ttc_Port_t     tgp_e

#define TTC_GPIO_BANK_A stm32_GPIOA
#define TTC_GPIO_BANK_B stm32_GPIOB
#define TTC_GPIO_BANK_C stm32_GPIOC
#define TTC_GPIO_BANK_D stm32_GPIOD
#define TTC_GPIO_BANK_E stm32_GPIOE
#ifdef GPIOF
#define TTC_GPIO_BANK_F stm32_GPIOF
#endif
#ifdef GPIOG
#define TTC_GPIO_BANK_G stm32_GPIOG
#endif

//{ Define shortcuts for all pins of GPIO Bank A
#define PIN_PA0  TTC_GPIO_BANK_A,0
#define PIN_PA1  TTC_GPIO_BANK_A,1
#define PIN_PA2  TTC_GPIO_BANK_A,2
#define PIN_PA3  TTC_GPIO_BANK_A,3
#define PIN_PA4  TTC_GPIO_BANK_A,4
#define PIN_PA5  TTC_GPIO_BANK_A,5
#define PIN_PA6  TTC_GPIO_BANK_A,6
#define PIN_PA7  TTC_GPIO_BANK_A,7
#define PIN_PA8  TTC_GPIO_BANK_A,8
#define PIN_PA9  TTC_GPIO_BANK_A,9
#define PIN_PA10 TTC_GPIO_BANK_A,10
#define PIN_PA11 TTC_GPIO_BANK_A,11
#define PIN_PA12 TTC_GPIO_BANK_A,12
#define PIN_PA13 TTC_GPIO_BANK_A,13
#define PIN_PA14 TTC_GPIO_BANK_A,14
#define PIN_PA15 TTC_GPIO_BANK_A,15
//}BankA
//{ Define shortcuts for all pins of GPIO Bank B
#define PIN_PB0  TTC_GPIO_BANK_B,0
#define PIN_PB1  TTC_GPIO_BANK_B,1
#define PIN_PB2  TTC_GPIO_BANK_B,2
#define PIN_PB3  TTC_GPIO_BANK_B,3
#define PIN_PB4  TTC_GPIO_BANK_B,4
#define PIN_PB5  TTC_GPIO_BANK_B,5
#define PIN_PB6  TTC_GPIO_BANK_B,6
#define PIN_PB7  TTC_GPIO_BANK_B,7
#define PIN_PB8  TTC_GPIO_BANK_B,8
#define PIN_PB9  TTC_GPIO_BANK_B,9
#define PIN_PB10 TTC_GPIO_BANK_B,10
#define PIN_PB11 TTC_GPIO_BANK_B,11
#define PIN_PB12 TTC_GPIO_BANK_B,12
#define PIN_PB13 TTC_GPIO_BANK_B,13
#define PIN_PB14 TTC_GPIO_BANK_B,14
#define PIN_PB15 TTC_GPIO_BANK_B,15
//}BankB
//{ Define shortcuts for all pins of GPIO Bank C
#define PIN_PC0  TTC_GPIO_BANK_C,0
#define PIN_PC1  TTC_GPIO_BANK_C,1
#define PIN_PC2  TTC_GPIO_BANK_C,2
#define PIN_PC3  TTC_GPIO_BANK_C,3
#define PIN_PC4  TTC_GPIO_BANK_C,4
#define PIN_PC5  TTC_GPIO_BANK_C,5
#define PIN_PC6  TTC_GPIO_BANK_C,6
#define PIN_PC7  TTC_GPIO_BANK_C,7
#define PIN_PC8  TTC_GPIO_BANK_C,8
#define PIN_PC9  TTC_GPIO_BANK_C,9
#define PIN_PC10 TTC_GPIO_BANK_C,10
#define PIN_PC11 TTC_GPIO_BANK_C,11
#define PIN_PC12 TTC_GPIO_BANK_C,12
#define PIN_PC13 TTC_GPIO_BANK_C,13
#define PIN_PC14 TTC_GPIO_BANK_C,14
#define PIN_PC15 TTC_GPIO_BANK_C,15
//}BankC
//{ Define shortcuts for all pins of GPIO Bank D
#define PIN_PD0  TTC_GPIO_BANK_D,0
#define PIN_PD1  TTC_GPIO_BANK_D,1
#define PIN_PD2  TTC_GPIO_BANK_D,2
#define PIN_PD3  TTC_GPIO_BANK_D,3
#define PIN_PD4  TTC_GPIO_BANK_D,4
#define PIN_PD5  TTC_GPIO_BANK_D,5
#define PIN_PD6  TTC_GPIO_BANK_D,6
#define PIN_PD7  TTC_GPIO_BANK_D,7
#define PIN_PD8  TTC_GPIO_BANK_D,8
#define PIN_PD9  TTC_GPIO_BANK_D,9
#define PIN_PD10 TTC_GPIO_BANK_D,10
#define PIN_PD11 TTC_GPIO_BANK_D,11
#define PIN_PD12 TTC_GPIO_BANK_D,12
#define PIN_PD13 TTC_GPIO_BANK_D,13
#define PIN_PD14 TTC_GPIO_BANK_D,14
#define PIN_PD15 TTC_GPIO_BANK_D,15
//}BankD
//{ Define shortcuts for all pins of GPIO Bank E
#define PIN_PE0  TTC_GPIO_BANK_E,0
#define PIN_PE1  TTC_GPIO_BANK_E,1
#define PIN_PE2  TTC_GPIO_BANK_E,2
#define PIN_PE3  TTC_GPIO_BANK_E,3
#define PIN_PE4  TTC_GPIO_BANK_E,4
#define PIN_PE5  TTC_GPIO_BANK_E,5
#define PIN_PE6  TTC_GPIO_BANK_E,6
#define PIN_PE7  TTC_GPIO_BANK_E,7
#define PIN_PE8  TTC_GPIO_BANK_E,8
#define PIN_PE9  TTC_GPIO_BANK_E,9
#define PIN_PE10 TTC_GPIO_BANK_E,10
#define PIN_PE11 TTC_GPIO_BANK_E,11
#define PIN_PE12 TTC_GPIO_BANK_E,12
#define PIN_PE13 TTC_GPIO_BANK_E,13
#define PIN_PE14 TTC_GPIO_BANK_E,14
#define PIN_PE15 TTC_GPIO_BANK_E,15
//}BankE
#ifdef TTC_GPIO_BANK_F
//{ Define shortcuts for all pins of GPIO Bank F
#define PIN_PF0  TTC_GPIO_BANK_F,0
#define PIN_PF1  TTC_GPIO_BANK_F,1
#define PIN_PF2  TTC_GPIO_BANK_F,2
#define PIN_PF3  TTC_GPIO_BANK_F,3
#define PIN_PF4  TTC_GPIO_BANK_F,4
#define PIN_PF5  TTC_GPIO_BANK_F,5
#define PIN_PF6  TTC_GPIO_BANK_F,6
#define PIN_PF7  TTC_GPIO_BANK_F,7
#define PIN_PF8  TTC_GPIO_BANK_F,8
#define PIN_PF9  TTC_GPIO_BANK_F,9
#define PIN_PF10 TTC_GPIO_BANK_F,10
#define PIN_PF11 TTC_GPIO_BANK_F,11
#define PIN_PF12 TTC_GPIO_BANK_F,12
#define PIN_PF13 TTC_GPIO_BANK_F,13
#define PIN_PF14 TTC_GPIO_BANK_F,14
#define PIN_PF15 TTC_GPIO_BANK_F,15
//}BankF
#endif
#ifdef TTC_GPIO_BANK_G
//{ Define shortcuts for all pins of GPIO Bank G
#define PIN_PG0  TTC_GPIO_BANK_G,0
#define PIN_PG1  TTC_GPIO_BANK_G,1
#define PIN_PG2  TTC_GPIO_BANK_G,2
#define PIN_PG3  TTC_GPIO_BANK_G,3
#define PIN_PG4  TTC_GPIO_BANK_G,4
#define PIN_PG5  TTC_GPIO_BANK_G,5
#define PIN_PG6  TTC_GPIO_BANK_G,6
#define PIN_PG7  TTC_GPIO_BANK_G,7
#define PIN_PG8  TTC_GPIO_BANK_G,8
#define PIN_PG9  TTC_GPIO_BANK_G,9
#define PIN_PG10 TTC_GPIO_BANK_G,10
#define PIN_PG11 TTC_GPIO_BANK_G,11
#define PIN_PG12 TTC_GPIO_BANK_G,12
#define PIN_PG13 TTC_GPIO_BANK_G,13
#define PIN_PG14 TTC_GPIO_BANK_G,14
#define PIN_PG15 TTC_GPIO_BANK_G,15
//}BankG
#endif


//}Defines
//{ includes2

#include "../ttc_gpio_types.h"  // must be included AFTER defining some ttc_XXXX thingies
//X #include <stdlib.h>
#include "stm32f10x.h"
#include "core_cm3.h"
#include "stm32f10x_conf.h"
#include "stm32f10x_gpio.h"

// FreeRTOS
#include <string.h>
#include "stddef.h"

//}includes2
//{ low-level driver interface for ttc_gpio.h

/** Macro Arguments
 *
 * PORT      pointer to type ttc_Port_t
 * BANK      pointer to type ttc_gpio_bank_t
 * PIN       0..15 pin number in GPIO bank
 * FIRST_PIN 0..8 pin number of first pin of am 8-bit wide parallel port
 * TYPE      type to configure pin to  (one from ttc_gpio_types.h:ttc_gpio_mode_e)
 * SPEED     speed to configure pin to (one from ttc_gpio_types.h:ttc_gpio_speed_e)
 * BANK_PIN  BANK,PIN  (allows use of PIN_Pxn definitions like PIN_A1)
 * INDEX     u8_t Value storing GPIO-Bank index (0..15) and Pin index (0..15) in high- and low-nibble
 *
 */

#define _driver_ttc_gpio_init(GPIOx, Pin, Type, Speed)                stm32_gpio_init(GPIOx, Pin, Type, Speed)
#define _driver_ttc_gpio_init_variable(Port, Bank, Pin, Type, Speed)  stm32_gpio_init2_variable(Port, Bank, Pin, Type, Speed)
#define _driver_ttc_gpio_variable(PORT,BANK,PIN)                      stm32_gpio_variable(PORT,BANK,PIN)
#define _driver_ttc_gpio_get_variable(PORT)                           stm32_gpio_get_variable(PORT)
#define _driver_ttc_gpio_set_variable(PORT)                           stm32_gpio_set_variable(PORT)
#define _driver_ttc_gpio_clr_variable(PORT)                           stm32_gpio_clr_variable(PORT)
//? #define _driver_ttc_gpio_set(BANK,PIN)                                stm32_gpio_set(BANK,PIN)
//? #define _driver_ttc_gpio_get(BANK,PIN)                                stm32_gpio_get(BANK,PIN)
//? #define _driver_ttc_gpio_clr(BANK,PIN)                                stm32_gpio_clr(BANK,PIN)
#define _driver_ttc_gpio_set(...)                                     stm32_gpio_set(__VA_ARGS__)
#define _driver_ttc_gpio_get(...)                                     stm32_gpio_get(__VA_ARGS__)
#define _driver_ttc_gpio_clr(...)                                     stm32_gpio_clr(__VA_ARGS__)
//X #define _driver_ttc_gpio_set2(BANK,PIN)                               stm32_gpio_set(BANK,PIN)
//X #define _driver_ttc_gpio_get2(BANK,PIN)                               stm32_gpio_get(BANK,PIN)
//X #define _driver_ttc_gpio_clr2(BANK,PIN)                               stm32_gpio_clr(BANK,PIN)
#define _driver_ttc_gpio_create_index8(...)                           gpio_stm32f1xx_create_index8(__VA_ARGS__)
#define _driver_ttc_gpio_from_index8(INDEX,BANK,PIN)                  interrupt_stm32f1xx_gpio_from_index(INDEX,BANK,PIN)

#define _driver_ttc_gpio_init_u8_pin(BANK_PIN,TYPE,SPEED)             stm32_gpio_init_u8_pin(BANK_PIN,TYPE,SPEED)
#define _driver_ttc_gpio_init_u8_pin4(BANK,FIRST_PIN,TYPE,SPEED)      stm32_gpio_init_u8_pin4(BANK,FIRST_PIN,TYPE,SPEED)
#define _driver_ttc_gpio_get_u8_pin(BANK_PIN)                         stm32_gpio_get_u8_pin(BANK_PIN)
#define _driver_ttc_gpio_get_u8_pin2(BANK,FIRST_PIN)                  stm32_gpio_get_u8_pin2(BANK,FIRST_PIN)
#define _driver_ttc_gpio_set_u8_pin(BANK_PIN,VALUE)                   stm32_gpio_set_u8_pin(BANK_PIN,VALUE)
#define _driver_ttc_gpio_set_u8_pin3(BANK,FIRST_PIN,VALUE)            stm32_gpio_set_u8_pin3(BANK,FIRST_PIN,VALUE)
#define _driver_ttc_gpio_init_u16(BANK,TYPE,SPEED)                    stm32_gpio_init_u16(BANK,TYPE,SPEED)
#define _driver_ttc_gpio_get_u16(BANK)                                stm32_gpio_get_u16(BANK)
#define _driver_ttc_gpio_set_u16(BANK,VALUE)                          stm32_gpio_set_u16(BANK,VALUE)

//} low-level driver interface for ttc_gpio.h
//{ Prototypes

/** converts from architecture independent pin type to stm32 pendant
 * @param    Type   one of tgm_analog_in, tgm_input_floating, tgm_input_pull_down, tgm_input_pull_up, tgm_output_open_drain, tgm_output_push_pull, tgm_alternate_function_push_pull, tgm_alternate_function_open_drain
 * @return   one of GPIO_Mode_AIN, GPIO_Mode_IN_FLOATING, GPIO_Mode_IPD, GPIO_Mode_IPU, GPIO_Mode_Out_OD, GPIO_Mode_Out_PP, GPIO_Mode_Out_OD, GPIO_Mode_AF_PP, GPIO_Mode_AF_OD
 */
GPIOMode_TypeDef stm32_map_Type(ttc_gpio_mode_e Mode);

/** converts from architecture independent pin speed to stm32 pendant
 * @param    Type   one of tgs_Min, tgs_2MHz, tgs_10MHz, tgs_50MHz, tgs_Max
 * @return   one of GPIO_Speed_2MHz, GPIO_Speed_10MHz, tgs_Max
 */
GPIOSpeed_TypeDef stm32_map_Speed(ttc_gpio_speed_e Speed);

/** initializes single port bit for use as input/ output
 * @param Port  filled out struct as being returned by stm32_gpio_variable()
 * @param Type   one of GPIO_Mode_AIN, GPIO_Mode_IN_FLOATING, GPIO_Mode_IPD, GPIO_Mode_IPU, GPIO_Mode_Out_OD, GPIO_Mode_Out_PP, GPIO_Mode_Out_OD, GPIO_Mode_AF_PP, GPIO_Mode_AF_OD
 * @param Speed  one of GPIO_Speed_2MHz, GPIO_Speed_10MHz, tgs_Max
 */
void stm32_gpio_init_variable(tgp_e* Port, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed);

/** initializes single port bit for use as input/ output
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, TTC_GPIO_BANK_D, TTC_GPIO_BANK_E, TTC_GPIO_BANK_F
 * @param Pin    0..15
 * @param Type   one of GPIO_Mode_AIN, GPIO_Mode_IN_FLOATING, GPIO_Mode_IPD, GPIO_Mode_IPU, GPIO_Mode_Out_OD, GPIO_Mode_Out_PP, GPIO_Mode_Out_OD, GPIO_Mode_AF_PP, GPIO_Mode_AF_OD
 * @param Speed  one of GPIO_Speed_2MHz, GPIO_Speed_10MHz, tgs_Max
 */
void stm32_gpio_init(gpio_stm32f1xx_bank_e GPIOx, u8_t Pin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed);

/** allows to load struct from list type definition
 *
 * @param Port   points to structure to be filled with given data
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, TTC_GPIO_BANK_D, TTC_GPIO_BANK_E, TTC_GPIO_BANK_F
 * @param Pin    0..15
 *
 * Example:
 * #define PB_LED1=TTC_GPIO_BANK_C,6
 * ttc_gpio_pin_e Port;
 * stm32_gpio_variable(&Port, PB_LED1);
 * stm32_gpio_init_variable(&Port, GPIO_Mode_Out_PP);
 * stm32_gpio_set_variable(&Port);
 */
void stm32_gpio_variable(tgp_e* Port, gpio_stm32f1xx_bank_e GPIOx, u8_t Pin);

/** initializes single port bit for use as input/ output with given speed by use of a state variable
 *
 * @param Port   points to structure to be filled with given data
 * @param Bank  GPIO bank to use (-> ttc_gpio_types.h:ttc_gpio_bank_t)
 *               Note: Bank, Pin can be given as one argument by use of Pin_Pxn macro (GPIOA, 7) = Pin_PA7
 * @param Pin    0..15
 * @param Type   mode to use (-> ttc_gpio_types.h:ttc_gpio_mode_e)
 * @param Speed  speed to use (-> ttc_gpio_types.h:ttc_gpio_speed_e)
 */
void stm32_gpio_init2_variable(ttc_Port_t* Port, ttc_gpio_bank_t Bank, u8_t Pin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed);

/** read current value of input pin
 * @param Port  filled out struct as being returned by stm32_gpio_variable()
 */
bool stm32_gpio_get_variable(tgp_e* Port);

/** set output pin to logical one
 * @param Port  filled out struct as being returned by stm32_gpio_variable()
 */
void stm32_gpio_set_variable(tgp_e* Port);

/** set output pin to logical zero
 * @param Port  filled out struct as being returned by stm32_gpio_variable()
 */
void stm32_gpio_clr_variable(tgp_e* Port);

/** read current value of input pin
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, TTC_GPIO_BANK_D, TTC_GPIO_BANK_E, TTC_GPIO_BANK_F
 * @param Pin    0..15
 */
bool stm32_gpio_get(gpio_stm32f1xx_bank_e GPIOx, u8_t Pin);

/** set output pin to logical one
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, TTC_GPIO_BANK_D, TTC_GPIO_BANK_E, TTC_GPIO_BANK_F
 * @param Pin    0..15
 */
void stm32_gpio_clr(gpio_stm32f1xx_bank_e GPIOx, u8_t Pin);

/** set output pin to logical zero
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, TTC_GPIO_BANK_D, TTC_GPIO_BANK_E, TTC_GPIO_BANK_F
 * @param Pin    0..15
 */
void stm32_gpio_set(gpio_stm32f1xx_bank_e GPIOx, u8_t Pin);

/** creates numeric 8-bit representing given GPIO-bank and pin number
 *
 * @param GPIOx  GPIO bank to use (-> ttc_gpio_types.h:ttc_gpio_bank_t)
 *               Note: GPIOx, Pin can be given as one argument by use of Pin_Pxn macro (TTC_GPIO_BANK_A, 7) = Pin_PA7
 * @param Pin    0..15
 * @return       8-bit value representing given GPIO-pin in a memory friendly format
 */
u8_t gpio_stm32f1xx_create_index8(ttc_gpio_bank_t GPIOx, u8_t Pin);

/** restores GPIO-bank and pin number from 8-bit value
 *
 * @param PhysicalIndex  8-bit value representing GPIO-bank + pin
 * @param GPIOx          loaded with GPIO bank (-> ttc_gpio_types.h:ttc_gpio_bank_t)
 * @param Pin            loaded with pin number
 */
void interrupt_stm32f1xx_gpio_from_index(u8_t PhysicalIndex, GPIO_TypeDef** GPIOx, u8_t* Pin);

/** enable/ disable a complete GPIO bank
 *
 * @param Bank     GPIO bank to enable/disable (GPIO_BANKA, GPIO_BANKB, ...)
 */
void stm32_gpio_bank_enable(gpio_stm32f1xx_bank_e GPIOx);
void stm32_gpio_bank_disable(gpio_stm32f1xx_bank_e GPIOx);

/** initialize all pins of 16-bit wide parallel port at once
 *
 * Note: Runtime measured on STM32F107 @72MHz as < 3us
 *
 * @param Bank     GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 */
void stm32_gpio_init_u16(ttc_gpio_bank_t GPIOx, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed);

/** read 16 bit value from gpio bank at once (fast access)
 *
 * @param GPIOx     GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 */
// u16_t stm32_gpio_get_u16(ttc_gpio_bank_t GPIOx);
#define stm32_gpio_get_u16(GPIOx)  ((GPIO_TypeDef *) GPIOx)->IDR

/** write 16 bit value into gpio bank at once (fast access)
 *
 * Note: runtime measured on stm32f107 @72MHz was 240ns
 *
 * @param GPIOx     GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 */
// void stm32_gpio_set_u16(ttc_gpio_bank_t GPIOx, u16_t Value);
#define stm32_gpio_set_u16(GPIOx, Value)  ((GPIO_TypeDef *) GPIOx)->ODR = Value

/** initialize 8 bit wide parallel port
 *
 * Note: Runtime measured on STM32F107 @72MHz as < 2.2us
 *
 * @param Bank+FirstPin  Bank and Pin given as one argument by use of Pin_Pxn macro: E.g. (GPIOA, 7) = Pin_PA7
 * @param Bank           GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 * @param FirstPin       0..8  lowest of 8 consecutive pins to use
 * @param Type           mode to use for all 8 pins  (-> ttc_gpio_types.h:ttc_gpio_mode_e)
 * @param Speed          speed to use for all 8 pins (-> ttc_gpio_types.h:ttc_gpio_speed_e)
 */
// defined as macros:
// void stm32_gpio_init_u8_pin4(Bank, FirstPin, Type, Speed);
#define stm32_gpio_init_u8_pin4(BANK, FIRST_PIN, TYPE, SPEED)  stm32_gpio_init_u8_pin(BANK, FIRST_PIN, TYPE, SPEED)
void stm32_gpio_init_u8_pin(ttc_gpio_bank_t GPIOx, u8_t FirstPin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed);

/** read 8 bit value starting at certain pin from gpio bank
 *
 * @param GPIOx     GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 * @param Pin       0..8  lowest of 8 consecutive pins to use
 * @return          bits Pin..Pin+7 from input data register of given gpio bank
 */
#define stm32_gpio_get_u8_pin2(GPIOx, Pin)   (  ( (0xff << (Pin) ) & ((GPIO_TypeDef *) GPIOx)->IDR ) >> (Pin)  ) & 0xff

// function required to break Bank+Pin into two parameters (required by gcc macros)
u8_t stm32_gpio_get_u8_pin(ttc_gpio_bank_t GPIOx, u8_t Pin);

/** write 8 bit value starting at certain pin into gpio bank
 *
 * Note: runtime measured on stm32f107 @72MHz was 890ns
 *
 * @param GPIOx     GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 * @param Pin       0..8  lowest of 8 consecutive pins to use
 * @param Value     written into bits Pin..Pin+7 of output data register of given gpio bank
 */
#define stm32_gpio_set_u8_pin3(GPIOx, Pin, Value)  ((GPIO_TypeDef *) GPIOx)->ODR = ( (Value) << (Pin) ) | ( (0xffff ^ (0xff << (Pin))) & ((GPIO_TypeDef *) GPIOx)->IDR )

// function required to break Bank+Pin into two parameters (required by gcc macros)
void stm32_gpio_set_u8_pin(ttc_gpio_bank_t GPIOx, u8_t Pin, u16_t Value);

/** compiles 4 bit configuration setting suitable for MODEx+CNFx fields of  GPIOx_CRL_t/ GPIOx_CRH_t
 *
 * Note: This function is private and normally should not be called from outside
 *
 * @param Type           mode to use for all 8 pins  (-> ttc_gpio_types.h:ttc_gpio_mode_e)
 * @param Speed          speed to use for all 8 pins (-> ttc_gpio_types.h:ttc_gpio_speed_e)
 * @return               4 bit value to be written at position corresponding to desired pin into GPIOx->CRL/ GPIOx->CRH
 */
u8_t _stm32_gpio_compile_port_config(ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed);

//


//} Prototypes

#endif
