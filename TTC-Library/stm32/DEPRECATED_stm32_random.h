#ifndef STM32_RANDOM_H
#define STM32_RANDOM_H

/*{ stm32_random.h ***********************************************

 * Written by Gregor Rebel 2010-2017
 *
 * Basic datatypes, enums and functions
 *
}*/
//{ Defines/ TypeDefs ****************************************************

#ifdef RAND_MAX
#undef RAND_MAX
#endif
#define RAND_MAX  65535

//} Defines

//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)

//} Includes

//{ Function prototypes **************************************************

/* returns random number 0..RAND_MAX analog to rand()
 * @return random number 0..RAND_MAX
 */
t_u16 stm32_rand( void );

/* initializes pseudo random number generator with given value analog to srand()
 * @param Seed  will be used as new starting point
 */
void stm32_srand( t_u32 Seed );

//}Function prototypes
#endif
