#ifndef STM32_INTERRUPT_H
#define STM32_INTERRUPT_H

/*{ stm32_interrupt.h **********************************************************
 
                      The ToolChain
                      
   Interrupt configuration
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Gregor Rebel 2012

 
}*/
//{ Compatibility interface for ttc_interrupt.h **************************
//
// We define all DRIVER-functions that are supported for stm32f1x architecture
//

#define _driver_interrupt_enable(Type, PhysicalIndex, EnableIRQ) \
             stm32_interrupt_enable(Type, PhysicalIndex, EnableIRQ)
#define _driver_interrupt_init(Type, PhysicalIndex) \
             stm32_interrupt_init(Type, PhysicalIndex)
#define _driver_interrupt_all_enable()   __enable_irq()
#define _driver_interrupt_all_disable()  __disable_irq()

#ifdef EXTENSION_500_ttc_usart
//#ifdef EXTENSION_450_usart_stm32f1
#  define _driver_interrupt_init_usart(Type, PhysicalIndex) \
               stm32_interrupt_init_usart(Type, PhysicalIndex)
#  define _driver_interrupt_enable_usart(Type, PhysicalIndex, EnableIRQ) \
               stm32_interrupt_enable_usart(Type, PhysicalIndex, EnableIRQ)
#endif
#ifdef EXTENSION_500_ttc_gpio
//#ifdef EXTENSION_450_gpio_stm32f1
#  define _driver_interrupt_init_gpio(Type, PhysicalIndex) \
               stm32_interrupt_init_gpio(Type, PhysicalIndex)
#  define _driver_interrupt_enable_gpio(Type, PhysicalIndex, EnableIRQ) \
               stm32_interrupt_enable_gpio(Type, PhysicalIndex, EnableIRQ)
#endif
#ifdef EXTENSION_500_ttc_timer
//#ifdef EXTENSION_450_gpio_stm32f1
#  define _driver_interrupt_init_timer(Type, PhysicalIndex) \
               stm32_interrupt_init_timer(Type, PhysicalIndex)
#  define _driver_interrupt_enable_timer(Type, PhysicalIndex, EnableIRQ) \
               stm32_interrupt_enable_timer(Type, PhysicalIndex, EnableIRQ)
#endif

#include "../cm3/cm3_basic.h"

// architecture specific constant values required by ttc_interrupt

#define _driver_AMOUNT_EXTERNAL_INTERRUPTS 16

//} Defines
//{ Includes *************************************************************

#include "../ttc_basic.h"
#ifdef EXTENSION_500_ttc_usart
#  include "../ttc_usart_types.h"
#endif
#ifdef EXTENSION_500_ttc_timer
#  include "../ttc_timer_types.h"
#endif

//} Includes
//{ Structures/ Enums ****************************************************

typedef struct { // stm32_usart_isrs_t - pointers of interrupt service routines (-> STM32_RM0008 p. 788)
    u8_t Unused; // not used in stm32
} stm32_usart_isrs_t;
typedef struct { // stm32_gpio_isrs_t - configuration data of single external interrupt line (-> STM32_RM0008 p. 201)
    u8_t Unused; // not used in stm32
} stm32_gpio_isrs_t;
typedef struct { // stm32_timer_isrs_t - configuration data of single external interrupt line (-> STM32_RM0008 p. 201)
    u8_t Unused; // not used in stm32
} stm32_timer_isrs_t;

// this will add above structs to ttc_interrupt_types.h/ttc_usart_isrs_t
#define _driver_usart_t stm32_usart_isrs_t
#define _driver_gpio_t  stm32_gpio_isrs_t
#define _driver_timer_t stm32_timer_isrs_t

//} Structures/ Enums
//{ Includes2 *************************************************************

#include "../ttc_memory.h"
#include "../ttc_interrupt_types.h"
#include "../ttc_gpio.h"
#include "../register/register_stm32f1xx.h"
#include "../cm3/cm3_basic.h"


#define USE_STDPERIPH_DRIVER
//#include "../../../../Source/TheToolChain_devel/Template/additionals/270_CPAL_CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h"
//#include "../../../../Source/TheToolChain_devel/Template/additionals/200_cpu_stm32f1xx_std_peripherals/CMSIS/CM3/CoreSupport/core_cm3.h"
//#include "../../../../Source/TheToolChain_devel/Template/configs/stm32f10x_conf.h"
//#include "../../../../Source/TheToolChain_devel/Template/configs/stm32f10x_it.h"
#include "stm32f10x.h"
#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
#include "stm32f10x_conf.h"
#include "stm32f10x_it.h"

// includes required for GPIO interrupts
//#include "../../../../Source/TheToolChain_devel/Template/additionals/270_CPAL_CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h"
//#include "../../../../Source/TheToolChain_devel/Template/additionals/250_STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_exti.h"
//#include "../../../../Source/TheToolChain_devel/Template/additionals/250_STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_rcc.h"
//#include "../../../../Source/TheToolChain_devel/Template/additionals/250_STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_misc.h"
#include "misc.h"
#include "stm32f10x.h"
#include "stm32f10x_exti.h"
#include "stm32f10x_rcc.h"

#ifdef EXTENSION_400_stm32_usb_fs_device_lib
#include "hw_config.h"
//#include "usb_lib.h"
#include "usb_istr.h"

#endif
#ifdef EXTENSION_400_stm32_usb_fs_host_lib
#include "usb_conf.h"
#include "usb_core.h"
#include "usbh_core.h"
#include "usb_hcd_int.h"
#endif

//} Includes2
//{ Function prototypes **************************************************

/* initializes interrupt of given type to call given ISR
 * Note: This will not activate your interrupt. Call stm32_interrupt_usart_enable() afterwards to enable/ disable this interrupt.
 *
 * @param Type          interrupt Type to use
 * @param PhysicalIndex real physical index of functional unit (e.g. 0 = USART1, 1 = USART2, ...)
 * @param ISR           function pointer of interrupt service routine to call;
 *                      ISR() gets two params: physical_index_t PhysicalIndex, void* Reference
 *                      Index     index functional unit in microcontroller (e.g. 0=first USART)
 *                      Reference hardware dependent data used by low level driver (e.g. register base address)
 * @param Argument      will be passed as argument to ISR()
 * @param OldISR        != NULL: previous stored pointer will be saved into given address
 * @param OldArgument   != NULL: previous stored argument will be saved into given address
 */
ttc_interrupt_errorcode_e stm32_interrupt_init(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);

// initializers for individual functional units (called from stm32_interrupt_init() )
ttc_interrupt_errorcode_e stm32_interrupt_init_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);
ttc_interrupt_errorcode_e stm32_interrupt_init_gpio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);
ttc_interrupt_errorcode_e stm32_interrupt_init_timer(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);

/* activates/ deactivates a previously initialized interrupt
 * @param Type        interrupt Type to use
 * @param PhysicalIndex real physical index of functional unit (e.g. 0 = USART1, 1 = USART2, ...)
 * @param EnableIRQ     =0: disable interrupt; !=0: enable interrupt
 */
void stm32_interrupt_enable(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);

// enablers for individual functional units (called from stm32_interrupt_enable() )
void stm32_interrupt_enable_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);
void stm32_interrupt_enable_gpio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);
void stm32_interrupt_enable_timer(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);

/* initializes interrupt of given type to call given ISR
 * @param PhysicalIndex  real index of physical functional unit (e.g 0=USART1, ...)
 * @param ISR_USART      pointer to register-base of USART that triggered the interrupt
 */
void USART_General_IRQHandler(physical_index_t PhysicalIndex, volatile register_stm32f1xx_usart_t* ISR_USART);
void GPIO_General_IRQHandler(physical_index_t PhysicalIndex);
void TIMER_General_IRQHandler(physical_index_t PhysicalIndex, TIM_TypeDef* TIMER);

//void Reset_Handler();
void NMI_Handler();
void HardFault_Handler();
void MemManage_Handler();
void BusFault_Handler();
void UsageFault_Handler();
void SVC_Handler();
void DebugMon_Handler();
void PendSV_Handler();
void SysTick_Handler();
void WWDG_IRQHandler();
void PVD_IRQHandler();
void TAMPER_IRQHandler();
void RTC_IRQHandler();
void FLASH_IRQHandler();
void RCC_IRQHandler();
void EXTI0_IRQHandler();
void EXTI1_IRQHandler();
void EXTI2_IRQHandler();
void EXTI3_IRQHandler();
void EXTI4_IRQHandler();
void DMA1_Channel1_IRQHandler();
void DMA1_Channel2_IRQHandler();
void DMA1_Channel3_IRQHandler();
void DMA1_Channel4_IRQHandler();
void DMA1_Channel5_IRQHandler();
void DMA1_Channel6_IRQHandler();
void DMA1_Channel7_IRQHandler();
void ADC1_2_IRQHandler();
void CAN1_TX_IRQHandler();
void CAN1_RX0_IRQHandler();
void CAN1_RX1_IRQHandler();
void CAN1_SCE_IRQHandler();
void EXTI9_5_IRQHandler();
void TIM1_BRK_IRQHandler();
void TIM1_UP_IRQHandler();
void TIM1_TRG_COM_IRQHandler();
void TIM1_CC_IRQHandler();
void TIM2_IRQHandler();
void TIM3_IRQHandler();
void TIM4_IRQHandler();
void I2C1_EV_IRQHandler();
void I2C1_ER_IRQHandler();
void I2C2_EV_IRQHandler();
void I2C2_ER_IRQHandler();
void SPI1_IRQHandler();
void SPI2_IRQHandler();
void USART1_IRQHandler();
void USART2_IRQHandler();
void USART3_IRQHandler();
void EXTI15_10_IRQHandler();
void RTCAlarm_IRQHandler();
void OTG_FS_WKUP_IRQHandler();
void TIM5_IRQHandler();
void SPI3_IRQHandler();
void UART4_IRQHandler();
void UART5_IRQHandler();
void TIM6_IRQHandler();
void TIM7_IRQHandler();
void DMA2_Channel1_IRQHandler();
void DMA2_Channel2_IRQHandler();
void DMA2_Channel3_IRQHandler();
void DMA2_Channel4_IRQHandler();
void DMA2_Channel5_IRQHandler();
void ETH_IRQHandler();
void ETH_WKUP_IRQHandler();
void CAN2_TX_IRQHandler();
void CAN2_RX0_IRQHandler();
void CAN2_RX1_IRQHandler();
void CAN2_SCE_IRQHandler();
void OTG_FS_IRQHandler();
void BootRAM();

//} Function prototypes

#endif //STM32_INTERRUPT_H
