#ifndef STM32_REGISTERS_H
#define STM32_REGISTERS_H

/*{ ../register/register_stm32f1xx.h ****************************************************
 
 Structure definitions of stm32-registers.
 
 written by Gregor Rebel 2012
 
 These structures are extremly handy when it comes to debugging via gdb.
 You can view any memory location through the eye of a struct:
 p *( (register_stm32f1xx_usart_t*) 0x40013800 )

 which is the same as
 p *register_stm32_USART1


 Note: Use of these structures in your code can be problematic
       if register access has side-effects.

 Example:
  + Works
    // Note: USART_TypeDef defined by StdPeripheral-library
    USART_CR1_t CR1;
    *( (u16_t*) &CR1 ) = USART1->CR1; // 16 bit transfer into local variable
    CR1.PCE  = 1;
    CR1.TCIE = 0;
    USART1->CR1 = *( (u16_t*) &CR1 ); // 16 bit transfer into register

  + does not work reliable
    ((register_stm32f1xx_usart_t*) MyUSART)->CR1.PCE = 1;

  Despite of this compiler dependend problem, the structures defined here
  allow an easy, self checking, way to read and write registers in a safe manner.

  You will not have to do any error prone bit-shifting like this:
  USART1->CR1 |= 1 << 10;             // PCE  = 1
  USART1->CR1 &= (0xffff - (1 << 6)); // TCIE = 0

}*/
//{ Defines/ TypeDefs ****************************************************

typedef enum {   // gpio_port_output_e -  values for GPIOx_CRL_t.MODEx.CNFx/ GPIOx_CRH_t.CNFx (use if gpio_port_mode_e!=gpm_input)
  gpc_output_push_pull            = 0b00,
  gpc_output_open_drain           = 0b01,
  gpc_output_alternate_push_pull  = 0b10,
  gpc_output_alternate_open_drain = 0b11
} gpio_port_output_e;
typedef enum {   // gpio_port_input_e  -  values for GPIOx_CRL_t.MODEx.CNFx/ GPIOx_CRH_t.CNFx (use if gpio_port_mode_e==gpm_input)
  gpc_input_analog,          // 00
  gpc_input_floating,        // 01
  gpc_input_pull_up_down,    // 10
  gpc_reserved               // 11
} gpio_port_input_e;
typedef enum {   // gpio_port_mode_e   -  values for GPIOx_CRL_t.MODEx.MODEx/ GPIOx_CRH_t.MODEx
  gpm_input        = 0b00,
  gpm_output_10mhz = 0b01,
  gpm_output_2mhz  = 0b10,
  gpm_output_50mhz = 0b11
} gpio_port_mode_e;
typedef struct { // GPIOx_CRL_t     Port configuration register low (x = A..G) (-> RM0008 p.166)
  unsigned MODE0    : 2; // gpio_port_mode_e
  unsigned CNF0     : 2; // gpio_port_input_e/ gpio_port_output_e
  unsigned MODE1    : 2; // gpio_port_mode_e
  unsigned CNF1     : 2; // gpio_port_input_e/ gpio_port_output_e
  unsigned MODE2    : 2; // gpio_port_mode_e
  unsigned CNF2     : 2; // gpio_port_input_e/ gpio_port_output_e
  unsigned MODE3    : 2; // gpio_port_mode_e
  unsigned CNF3     : 2; // gpio_port_input_e/ gpio_port_output_e
  unsigned MODE4    : 2; // gpio_port_mode_e
  unsigned CNF4     : 2; // gpio_port_input_e/ gpio_port_output_e
  unsigned MODE5    : 2; // gpio_port_mode_e
  unsigned CNF5     : 2; // gpio_port_input_e/ gpio_port_output_e
  unsigned MODE6    : 2; // gpio_port_mode_e
  unsigned CNF6     : 2; // gpio_port_input_e/ gpio_port_output_e
  unsigned MODE7    : 2; // gpio_port_mode_e
  unsigned CNF7     : 2; // gpio_port_input_e/ gpio_port_output_e
} __attribute__((__packed__)) GPIOx_CRL_t;
typedef struct { // GPIOx_CRH_t     Port configuration register high (x = A..G) (-> RM0008 p.167)
  unsigned MODE8    : 2; // gpio_port_mode_e
  unsigned CNF8     : 2; // gpio_port_input_e/ gpio_port_output_e
  unsigned MODE9    : 2; // gpio_port_mode_e
  unsigned CNF9     : 2; // gpio_port_input_e/ gpio_port_output_e
  unsigned MODE10   : 2; // gpio_port_mode_e
  unsigned CNF10    : 2; // gpio_port_input_e/ gpio_port_output_e
  unsigned MODE11   : 2; // gpio_port_mode_e
  unsigned CNF11    : 2; // gpio_port_input_e/ gpio_port_output_e
  unsigned MODE12   : 2; // gpio_port_mode_e
  unsigned CNF12    : 2; // gpio_port_input_e/ gpio_port_output_e
  unsigned MODE13   : 2; // gpio_port_mode_e
  unsigned CNF13    : 2; // gpio_port_input_e/ gpio_port_output_e
  unsigned MODE14   : 2; // gpio_port_mode_e
  unsigned CNF14    : 2; // gpio_port_input_e/ gpio_port_output_e
  unsigned MODE15   : 2; // gpio_port_mode_e
  unsigned CNF15    : 2; // gpio_port_input_e/ gpio_port_output_e
} __attribute__((__packed__)) GPIOx_CRH_t;
typedef struct { // GPIOx_IDR_t     GPIOx input data register (x = A..G) (-> RM0008 p.167)
  unsigned DATA     : 16; // input data register of all 16 port bits
  unsigned reserved : 16;
} __attribute__((__packed__)) GPIOx_IDR_t;
typedef struct { // GPIOx_ODR_t     GPIOx output data register (x = A..G) (-> RM0008 p.167)
  unsigned DATA     : 16; // output data register of all 16 port bits
  unsigned reserved : 16;
} __attribute__((__packed__)) GPIOx_ODR_t;
typedef struct { // GPIOx_BSRR_t    GPIOx set/reset register (x = A..G) (-> RM0008 p.168)
  unsigned BS0      : 1; // port set bit 1=set OSR0 bit; 0=no change on OSR0
  unsigned BS1      : 1; // port set bit 1=set OSR1 bit; 0=no change on OSR1
  unsigned BS2      : 1; // port set bit 1=set OSR2 bit; 0=no change on OSR2
  unsigned BS3      : 1; // port set bit 1=set OSR3 bit; 0=no change on OSR3
  unsigned BS4      : 1; // port set bit 1=set OSR4 bit; 0=no change on OSR4
  unsigned BS5      : 1; // port set bit 1=set OSR5 bit; 0=no change on OSR5
  unsigned BS6      : 1; // port set bit 1=set OSR6 bit; 0=no change on OSR6
  unsigned BS7      : 1; // port set bit 1=set OSR7 bit; 0=no change on OSR7
  unsigned BS8      : 1; // port set bit 1=set OSR8 bit; 0=no change on OSR8
  unsigned BS9      : 1; // port set bit 1=set OSR9 bit; 0=no change on OSR9
  unsigned BS10     : 1; // port set bit 1=set OSR10 bit; 0=no change on OSR10
  unsigned BS11     : 1; // port set bit 1=set OSR11 bit; 0=no change on OSR11
  unsigned BS12     : 1; // port set bit 1=set OSR12 bit; 0=no change on OSR12
  unsigned BS13     : 1; // port set bit 1=set OSR13 bit; 0=no change on OSR13
  unsigned BS14     : 1; // port set bit 1=set OSR14 bit; 0=no change on OSR14
  unsigned BS15     : 1; // port set bit 1=set OSR15 bit; 0=no change on OSR15

  unsigned BR0      : 1; // port reset bit 1=reset OSR0 bit; 0=no change on OSR0
  unsigned BR1      : 1; // port reset bit 1=reset OSR1 bit; 0=no change on OSR1
  unsigned BR2      : 1; // port reset bit 1=reset OSR2 bit; 0=no change on OSR2
  unsigned BR3      : 1; // port reset bit 1=reset OSR3 bit; 0=no change on OSR3
  unsigned BR4      : 1; // port reset bit 1=reset OSR4 bit; 0=no change on OSR4
  unsigned BR5      : 1; // port reset bit 1=reset OSR5 bit; 0=no change on OSR5
  unsigned BR6      : 1; // port reset bit 1=reset OSR6 bit; 0=no change on OSR6
  unsigned BR7      : 1; // port reset bit 1=reset OSR7 bit; 0=no change on OSR7
  unsigned BR8      : 1; // port reset bit 1=reset OSR8 bit; 0=no change on OSR8
  unsigned BR9      : 1; // port reset bit 1=reset OSR9 bit; 0=no change on OSR9
  unsigned BR10     : 1; // port reset bit 1=reset OSR10 bit; 0=no change on OSR10
  unsigned BR11     : 1; // port reset bit 1=reset OSR11 bit; 0=no change on OSR11
  unsigned BR12     : 1; // port reset bit 1=reset OSR12 bit; 0=no change on OSR12
  unsigned BR13     : 1; // port reset bit 1=reset OSR13 bit; 0=no change on OSR13
  unsigned BR14     : 1; // port reset bit 1=reset OSR14 bit; 0=no change on OSR14
  unsigned BR15     : 1; // port reset bit 1=reset OSR15 bit; 0=no change on OSR15
} __attribute__((__packed__)) GPIOx_BSRR_t;
typedef struct { // GPIOx_BRR_t     GPIOx reset register (x = A..G) (-> RM0008 p.169)
  unsigned BR0      : 1; // port reset bit 1=reset OSR0 bit; 0=no change on OSR0
  unsigned BR1      : 1; // port reset bit 1=reset OSR1 bit; 0=no change on OSR1
  unsigned BR2      : 1; // port reset bit 1=reset OSR2 bit; 0=no change on OSR2
  unsigned BR3      : 1; // port reset bit 1=reset OSR3 bit; 0=no change on OSR3
  unsigned BR4      : 1; // port reset bit 1=reset OSR4 bit; 0=no change on OSR4
  unsigned BR5      : 1; // port reset bit 1=reset OSR5 bit; 0=no change on OSR5
  unsigned BR6      : 1; // port reset bit 1=reset OSR6 bit; 0=no change on OSR6
  unsigned BR7      : 1; // port reset bit 1=reset OSR7 bit; 0=no change on OSR7
  unsigned BR8      : 1; // port reset bit 1=reset OSR8 bit; 0=no change on OSR8
  unsigned BR9      : 1; // port reset bit 1=reset OSR9 bit; 0=no change on OSR9
  unsigned BR10     : 1; // port reset bit 1=reset OSR10 bit; 0=no change on OSR10
  unsigned BR11     : 1; // port reset bit 1=reset OSR11 bit; 0=no change on OSR11
  unsigned BR12     : 1; // port reset bit 1=reset OSR12 bit; 0=no change on OSR12
  unsigned BR13     : 1; // port reset bit 1=reset OSR13 bit; 0=no change on OSR13
  unsigned BR14     : 1; // port reset bit 1=reset OSR14 bit; 0=no change on OSR14
  unsigned BR15     : 1; // port reset bit 1=reset OSR15 bit; 0=no change on OSR15
  unsigned reserved : 16;
} __attribute__((__packed__)) GPIOx_BRR_t;
typedef struct { // GPIOx_LCKR_t    GPIOx configuration lock register (x = A..G) (-> RM0008 p.169)

  unsigned LCK0     : 1; // lock key configuration for port bit 0
  unsigned LCK1     : 1; // lock key configuration for port bit 1
  unsigned LCK2     : 1; // lock key configuration for port bit 2
  unsigned LCK3     : 1; // lock key configuration for port bit 3
  unsigned LCK4     : 1; // lock key configuration for port bit 4
  unsigned LCK5     : 1; // lock key configuration for port bit 5
  unsigned LCK6     : 1; // lock key configuration for port bit 6
  unsigned LCK7     : 1; // lock key configuration for port bit 7
  unsigned LCK8     : 1; // lock key configuration for port bit 8
  unsigned LCK9     : 1; // lock key configuration for port bit 9
  unsigned LCK10    : 1; // lock key configuration for port bit 10
  unsigned LCK11    : 1; // lock key configuration for port bit 11
  unsigned LCK12    : 1; // lock key configuration for port bit 12
  unsigned LCK13    : 1; // lock key configuration for port bit 13
  unsigned LCK14    : 1; // lock key configuration for port bit 14
  unsigned LCK15    : 1; // lock key configuration for port bit 15
  unsigned LCKK     : 1; // lock key for all port bits
                         // Lock key writing sequence
                         // write 1
                         // write 0
                         // write 1
                         // read  0
                         // read  1 (confirms lock)
                         // Note: Any change of LCK0..LCK15 aborts this sequence
                         //       Lock Keys can be read at any time

  unsigned reserved : 15;
} __attribute__((__packed__)) GPIOx_LCKR_t;
typedef struct { // register_stm32f1xx_gpio_t         General Purpose Input Output registers
    GPIOx_CRL_t    CRL;     // Port configuration register low
    GPIOx_CRH_t    CRH;     // Port configuration register high
    GPIOx_IDR_t    IDR;     // GPIOx input data register
    GPIOx_ODR_t    ODR;     // GPIOx output data register
    GPIOx_BSRR_t   BSRR;    // GPIOx set/reset register
    GPIOx_BRR_t    BRR;     // GPIOx reset register
    GPIOx_LCKR_t   LCKR;    // GPIOx configuration lock register
} __attribute__((__packed__)) register_stm32f1xx_gpio_t;

//} Defines
//{ Includes *************************************************************

 // Basic set of helper functions
#include "../ttc_basic.h"
#include "../cm3/cm3_registers.h"

#define USE_STDPERIPH_DRIVER
#include "stm32f10x.h"

//} Includes
//{ STM32 Registers ******************************************************



typedef enum   { // afio_event_pin_e        - selects pin used to output Cortex EVENTOUT signal (-> RM0008 p.178)
    gep_px0,  // 0000: pin Px0 selected
    gep_px1,  // 0001: pin Px1 selected
    gep_px2,  // 0010: pin Px2 selected
    gep_px3,  // 0011: pin Px3 selected
    gep_px4,  // 0100: pin Px4 selected
    gep_px5,  // 0101: pin Px5 selected
    gep_px6,  // 0110: pin Px6 selected
    gep_px7,  // 0101: pin Px7 selected
    gep_px8,  // 0111: pin Px8 selected
    gep_px9,  // 1000: pin Px9 selected
    gep_px10, // 1001: pin Px10 selected
    gep_px11, // 1010: pin Px11 selected
    gep_px12, // 1011: pin Px12 selected
    gep_px13, // 1100: pin Px13 selected
    gep_px14, // 1101: pin Px14 selected
    gep_px15  // 1110: pin Px15 selected
} afio_event_pin_e;
typedef enum   { // afio_event_port_e       - selects port used to output Cortex EVENTOUT signal (-> RM0008 p.178)
    ges_pxa,  // 000: port PA selected
    ges_pxb,  // 001: port PB selected
    ges_pxc,  // 010: port PC selected
    ges_pxd,  // 011: port PD selected
    ges_pxe,  // 100: port PE selected
    get_unknown
} afio_event_port_e;
typedef struct { // AFIO_EVCR_t     AFIO event control register (x = A..G) (-> RM0008 p.178)

  unsigned PIN       : 4; // afio_event_select_e
  unsigned PORT      : 3; // afio_event_port_e
  unsigned EVOE      : 1; // event output enable
  unsigned reserved1 : 8;
  unsigned reserved2 : 16;
} __attribute__((__packed__)) AFIO_EVCR_t;
typedef enum   { // gpio_interrupt_source_e - selects source input for EXTIx external interrupt (-> RM0008 p.185)
  gis_pa,  // 0000: pin PAx selected
  gis_pb,  // 0001: pin PBx selected
  gis_pc,  // 0010: pin PCx selected
  gis_pd,  // 0011: pin PDx selected
  gis_pe,  // 0100: pin PEx selected
  gis_pf,  // 0101: pin PFx selected
  gis_pg   // 0110: pin PGx selected
} gpio_interrupt_source_e;
typedef struct { // AFIO_EXTICR1_t  AFIO external interrupt configuration register 1 (x = A..G) (-> RM0008 p.185)
  unsigned EXTI0    : 4; // gpio_interrupt_source_e - source for external interrupt line 0
  unsigned EXTI1    : 4; // gpio_interrupt_source_e - source for external interrupt line 1
  unsigned EXTI2    : 4; // gpio_interrupt_source_e - source for external interrupt line 2
  unsigned EXTI3    : 4; // gpio_interrupt_source_e - source for external interrupt line 3
  unsigned reserved : 16;
} __attribute__((__packed__)) AFIO_EXTICR1_t;
typedef struct { // AFIO_EXTICR2_t  AFIO external interrupt configuration register 2 (x = A..G) (-> RM0008 p.185) 
  unsigned EXTI4    : 4; // gpio_interrupt_source_e - source for external interrupt line 4
  unsigned EXTI5    : 4; // gpio_interrupt_source_e - source for external interrupt line 5
  unsigned EXTI6    : 4; // gpio_interrupt_source_e - source for external interrupt line 6
  unsigned EXTI7    : 4; // gpio_interrupt_source_e - source for external interrupt line 7 
  unsigned reserved : 16;
} __attribute__((__packed__)) AFIO_EXTICR2_t;
typedef struct { // AFIO_EXTICR3_t  AFIO external interrupt configuration register 3 (x = A..G) (-> RM0008 p.185) 
  unsigned EXTI8    : 4; // gpio_interrupt_source_e - source for external interrupt line 8
  unsigned EXTI9    : 4; // gpio_interrupt_source_e - source for external interrupt line 9
  unsigned EXTI10   : 4; // gpio_interrupt_source_e - source for external interrupt line 10
  unsigned EXTI11   : 4; // gpio_interrupt_source_e - source for external interrupt line 11 
  unsigned reserved : 16;
} __attribute__((__packed__)) AFIO_EXTICR3_t;
typedef struct { // AFIO_EXTICR4_t  AFIO external interrupt configuration register 4 (x = A..G) (-> RM0008 p.185) 
  unsigned EXTI12   : 4; // gpio_interrupt_source_e - source for external interrupt line 12
  unsigned EXTI13   : 4; // gpio_interrupt_source_e - source for external interrupt line 13
  unsigned EXTI14   : 4; // gpio_interrupt_source_e - source for external interrupt line 14
  unsigned EXTI15   : 4; // gpio_interrupt_source_e - source for external interrupt line 15 
  unsigned reserved : 16;
} __attribute__((__packed__)) AFIO_EXTICR4_t;
typedef struct { // register_stm32f1xx_afio_t          Alternate Function IO registers
  AFIO_EVCR_t    EVCR;    // AFIO event control register
  u32_t          MAPR;    // AF remap and debug I/O configuration register 1
  AFIO_EXTICR1_t EXTICR1; // AFIO external interrupt configuration register 1
  AFIO_EXTICR2_t EXTICR2; // AFIO external interrupt configuration register 2
  AFIO_EXTICR3_t EXTICR3; // AFIO external interrupt configuration register 3
  AFIO_EXTICR4_t EXTICR4; // AFIO external interrupt configuration register 4
  u32_t          MAPR2;   // AF remap and debug I/O configuration register 2
} __attribute__((__packed__)) register_stm32f1xx_afio_t;

typedef struct { // EXTI_IMR_t     EXTI interrupt mask register (-> RM0008 p.202) 
  unsigned MR0 : 1; // =1: interrupt request from line 0 is not masked/ =0: masked 
  unsigned MR1 : 1; // =1: interrupt request from line 1 is not masked/ =0: masked 
  unsigned MR2 : 1; // =1: interrupt request from line 2 is not masked/ =0: masked 
  unsigned MR3 : 1; // =1: interrupt request from line 3 is not masked/ =0: masked 
  unsigned MR4 : 1; // =1: interrupt request from line 4 is not masked/ =0: masked 
  unsigned MR5 : 1; // =1: interrupt request from line 5 is not masked/ =0: masked 
  unsigned MR6 : 1; // =1: interrupt request from line 6 is not masked/ =0: masked 
  unsigned MR7 : 1; // =1: interrupt request from line 7 is not masked/ =0: masked 
  unsigned MR8 : 1; // =1: interrupt request from line 8 is not masked/ =0: masked 
  unsigned MR9 : 1; // =1: interrupt request from line 9 is not masked/ =0: masked 
  unsigned MR10 : 1; // =1: interrupt request from line 10 is not masked/ =0: masked 
  unsigned MR11 : 1; // =1: interrupt request from line 11 is not masked/ =0: masked 
  unsigned MR12 : 1; // =1: interrupt request from line 12 is not masked/ =0: masked 
  unsigned MR13 : 1; // =1: interrupt request from line 13 is not masked/ =0: masked 
  unsigned MR14 : 1; // =1: interrupt request from line 14 is not masked/ =0: masked 
  unsigned MR15 : 1; // =1: interrupt request from line 15 is not masked/ =0: masked 
  unsigned MR16 : 1; // =1: interrupt request from line 16 is not masked/ =0: masked 
  unsigned MR17 : 1; // =1: interrupt request from line 17 is not masked/ =0: masked 
  unsigned MR18 : 1; // =1: interrupt request from line 18 is not masked/ =0: masked 
  unsigned MR19 : 1; // =1: interrupt request from line 19 is not masked/ =0: masked 
  unsigned reserved : 12;
} __attribute__((__packed__)) EXTI_IMR_t;
typedef struct { // EXTI_EMR_t     EXTI event mask register (-> RM0008 p.202) 
  unsigned MR0  : 1; // =1: event request from line 0 is not masked/ =0: masked 
  unsigned MR1  : 1; // =1: event request from line 1 is not masked/ =0: masked 
  unsigned MR2  : 1; // =1: event request from line 2 is not masked/ =0: masked 
  unsigned MR3  : 1; // =1: event request from line 3 is not masked/ =0: masked 
  unsigned MR4  : 1; // =1: event request from line 4 is not masked/ =0: masked 
  unsigned MR5  : 1; // =1: event request from line 5 is not masked/ =0: masked 
  unsigned MR6  : 1; // =1: event request from line 6 is not masked/ =0: masked 
  unsigned MR7  : 1; // =1: event request from line 7 is not masked/ =0: masked 
  unsigned MR8  : 1; // =1: event request from line 8 is not masked/ =0: masked 
  unsigned MR9  : 1; // =1: event request from line 9 is not masked/ =0: masked 
  unsigned MR10 : 1; // =1: event request from line 10 is not masked/ =0: masked 
  unsigned MR11 : 1; // =1: event request from line 11 is not masked/ =0: masked 
  unsigned MR12 : 1; // =1: event request from line 12 is not masked/ =0: masked 
  unsigned MR13 : 1; // =1: event request from line 13 is not masked/ =0: masked 
  unsigned MR14 : 1; // =1: event request from line 14 is not masked/ =0: masked 
  unsigned MR15 : 1; // =1: event request from line 15 is not masked/ =0: masked 
  unsigned MR16 : 1; // =1: event request from line 16 is not masked/ =0: masked 
  unsigned MR17 : 1; // =1: event request from line 17 is not masked/ =0: masked 
  unsigned MR18 : 1; // =1: event request from line 18 is not masked/ =0: masked 
  unsigned MR19 : 1; // =1: event request from line 19 is not masked/ =0: masked 
  unsigned reserved : 12;
} __attribute__((__packed__)) EXTI_EMR_t;
typedef struct { // EXTI_RTSR_t    EXTI rising trigger selection register (-> RM0008 p.203) 
  unsigned TR0  : 1; // =1: rising trigger enabled for line 0  
  unsigned TR1  : 1; // =1: rising trigger enabled for line 1  
  unsigned TR2  : 1; // =1: rising trigger enabled for line 2  
  unsigned TR3  : 1; // =1: rising trigger enabled for line 3  
  unsigned TR4  : 1; // =1: rising trigger enabled for line 4  
  unsigned TR5  : 1; // =1: rising trigger enabled for line 5  
  unsigned TR6  : 1; // =1: rising trigger enabled for line 6  
  unsigned TR7  : 1; // =1: rising trigger enabled for line 7  
  unsigned TR8  : 1; // =1: rising trigger enabled for line 8  
  unsigned TR9  : 1; // =1: rising trigger enabled for line 9  
  unsigned TR10 : 1; // =1: rising trigger enabled for line 10  
  unsigned TR11 : 1; // =1: rising trigger enabled for line 11  
  unsigned TR12 : 1; // =1: rising trigger enabled for line 12  
  unsigned TR13 : 1; // =1: rising trigger enabled for line 13  
  unsigned TR14 : 1; // =1: rising trigger enabled for line 14  
  unsigned TR15 : 1; // =1: rising trigger enabled for line 15  
  unsigned TR16 : 1; // =1: rising trigger enabled for line 16  
  unsigned TR17 : 1; // =1: rising trigger enabled for line 17  
  unsigned TR18 : 1; // =1: rising trigger enabled for line 18  
  unsigned TR19 : 1; // =1: rising trigger enabled for line 19  
  unsigned reserved : 12;
} __attribute__((__packed__)) EXTI_RTSR_t;
typedef struct { // EXTI_FTSR_t    EXTI falling trigger selection register (-> RM0008 p.203) 
  unsigned TR0  : 1; // =1: falling trigger enabled for line 0  
  unsigned TR1  : 1; // =1: falling trigger enabled for line 1  
  unsigned TR2  : 1; // =1: falling trigger enabled for line 2  
  unsigned TR3  : 1; // =1: falling trigger enabled for line 3  
  unsigned TR4  : 1; // =1: falling trigger enabled for line 4  
  unsigned TR5  : 1; // =1: falling trigger enabled for line 5  
  unsigned TR6  : 1; // =1: falling trigger enabled for line 6  
  unsigned TR7  : 1; // =1: falling trigger enabled for line 7  
  unsigned TR8  : 1; // =1: falling trigger enabled for line 8  
  unsigned TR9  : 1; // =1: falling trigger enabled for line 9  
  unsigned TR10 : 1; // =1: falling trigger enabled for line 10  
  unsigned TR11 : 1; // =1: falling trigger enabled for line 11  
  unsigned TR12 : 1; // =1: falling trigger enabled for line 12  
  unsigned TR13 : 1; // =1: falling trigger enabled for line 13  
  unsigned TR14 : 1; // =1: falling trigger enabled for line 14  
  unsigned TR15 : 1; // =1: falling trigger enabled for line 15  
  unsigned TR16 : 1; // =1: falling trigger enabled for line 16  
  unsigned TR17 : 1; // =1: falling trigger enabled for line 17  
  unsigned TR18 : 1; // =1: falling trigger enabled for line 18  
  unsigned TR19 : 1; // =1: falling trigger enabled for line 19  
  unsigned reserved : 12;
} __attribute__((__packed__)) EXTI_FTSR_t;
typedef struct { // EXTI_SWIER_t   EXTI software interrupt event register (-> RM0008 p.204) 
  // Writing a 1 to SWIERx sets corresponding bit in EXTI_PR.
  // SWIERx is cleared by writing a 1 to corresponding bit of EXTI_PR
  
  unsigned SWIER0 : 1; //   
  unsigned SWIER1 : 1; //   
  unsigned SWIER2 : 1; //   
  unsigned SWIER3 : 1; //   
  unsigned SWIER4 : 1; //   
  unsigned SWIER5 : 1; //   
  unsigned SWIER6 : 1; //   
  unsigned SWIER7 : 1; //   
  unsigned SWIER8 : 1; //   
  unsigned SWIER9 : 1; //   
  unsigned SWIER10 : 1; //   
  unsigned SWIER11 : 1; //   
  unsigned SWIER12 : 1; //   
  unsigned SWIER13 : 1; //   
  unsigned SWIER14 : 1; //   
  unsigned SWIER15 : 1; //   
  unsigned SWIER16 : 1; //   
  unsigned SWIER17 : 1; //   
  unsigned SWIER18 : 1; //   
  unsigned SWIER19 : 1; //   
  unsigned reserved : 12;
} __attribute__((__packed__)) EXTI_SWIER_t;
typedef struct { // EXTI_PR_t      EXTI pending interrupt register (-> RM0008 p.204) 

  unsigned PR0  : 1; // =1: trigger request has occured on line 0   
  unsigned PR1  : 1; // =1: trigger request has occured on line 1   
  unsigned PR2  : 1; // =1: trigger request has occured on line 2   
  unsigned PR3  : 1; // =1: trigger request has occured on line 3   
  unsigned PR4  : 1; // =1: trigger request has occured on line 4   
  unsigned PR5  : 1; // =1: trigger request has occured on line 5   
  unsigned PR6  : 1; // =1: trigger request has occured on line 6   
  unsigned PR7  : 1; // =1: trigger request has occured on line 7   
  unsigned PR8  : 1; // =1: trigger request has occured on line 8   
  unsigned PR9  : 1; // =1: trigger request has occured on line 9   
  unsigned PR10 : 1; // =1: trigger request has occured on line 10   
  unsigned PR11 : 1; // =1: trigger request has occured on line 11   
  unsigned PR12 : 1; // =1: trigger request has occured on line 12   
  unsigned PR13 : 1; // =1: trigger request has occured on line 13   
  unsigned PR14 : 1; // =1: trigger request has occured on line 14   
  unsigned PR15 : 1; // =1: trigger request has occured on line 15   
  unsigned PR16 : 1; // =1: trigger request has occured on line 16   
  unsigned PR17 : 1; // =1: trigger request has occured on line 17   
  unsigned PR18 : 1; // =1: trigger request has occured on line 18   
  unsigned PR19 : 1; // =1: trigger request has occured on line 19   
  unsigned reserved : 12;
} __attribute__((__packed__)) EXTI_PR_t;
typedef struct { // register_stm32f1xx_exti_t         external interrupt registers (-> RM0008 p.205) 

  EXTI_IMR_t   IMR;   // interrupt mask register
  EXTI_EMR_t   EMR;   // event mask register
  EXTI_RTSR_t  RTSR;  // rising trigger selection register
  EXTI_FTSR_t  FTSR;  // falling trigger selection register
  EXTI_SWIER_t SWIER; // software interrupt event register
  EXTI_PR_t    PR;    // pending interrupt register
  
} __attribute__((__packed__)) register_stm32f1xx_exti_t;

typedef struct { // RCC_CR_t       Clock Control Register (-> RM0008 p.96,129)

  unsigned HSION        : 1; // Internal high-speed clock enable
  unsigned HSIRDY       : 1; // Internal high-speed clock ready flag
  unsigned reserved1    : 1; // 
  unsigned HSITRIM      : 5; //  Internal high-speed clock trimming
  unsigned HSICAL       : 8; // Internal high-speed clock calibration. These bits are initialized automatically at startup.
  unsigned HSEON        : 1; // HSE clock enable
  unsigned HSERDY       : 1; // External high-speed clock ready flag
  unsigned HSEBYP       : 1; // External high-speed clock bypass
  unsigned CSSON        : 1; // Clock security system enable
  unsigned reserved2    : 4; // 
  unsigned PLLON        : 1; // PLL enable
  unsigned PLLRDY       : 1; // PLL clock ready flag
  unsigned PLL2ON       : 1; // PLL2 enable
  unsigned PLL2RDY      : 1; // PLL2 clock ready flag
  unsigned PLL3ON       : 1; // PLL3 enable
  unsigned PLL3RDY      : 1; // PLL3 clock ready flag
  unsigned reserved3    : 1; //
  
} __attribute__((__packed__)) RCC_CR_t;
typedef struct { // RCC_CFGR_t     Clock Configuration Register (-> RM0008 p.131)
  
  unsigned SW           : 2; // System clock switch
  unsigned SWS          : 2; // System clock switch status
  unsigned HPRE         : 4; // AHB prescaler
  unsigned PPRE1        : 3; // APB low-speed prescaler (APB1)
  unsigned PPRE2        : 3; // APB high-speed prescaler (APB2Set and cleared by software to control th(PCLK2).
  unsigned ADCPRE       : 2; // ADC prescaler
  unsigned PLLSRC       : 1; // PLL entry clock source
  unsigned PLLXTPRE     : 1; // HSE divider for PLL entry
  unsigned PLLMUL       : 4; // PLL multiplication factor
  unsigned USBPRE       : 1; // USB prescaler
  unsigned reserved1    : 1; // reserved
#ifdef STM32F10X_CL // Connection Line devices have more inputs to MCO muxer
  unsigned MCO          : 4; // Microcontroller clock output
  unsigned reserved2    : 4; // reserved
#else
  unsigned MCO          : 3; // Microcontroller clock output
  unsigned reserved2    : 5; // reserved
#endif
} __attribute__((__packed__)) RCC_CFGR_t;
typedef struct { // RCC_CIR_t      Clock interrupt register

  unsigned LSIRDYF      : 1; // LSI ready interrupt flag
  unsigned LSERDYF      : 1; // LSE ready interrupt flag
  unsigned HSIRDYF      : 1; // HSI ready interrupt flag
  unsigned HSERDYF      : 1; // HSE ready interrupt flag
  unsigned PLLRDYF      : 1; // PLL ready interrupt flag
  unsigned reserved1    : 2; // 
  unsigned CSSF         : 1; // Clock security system interrupt flag
  unsigned LSIRDYIE     : 1; // LSI ready interrupt enable
  unsigned LSERDYIE     : 1; // LSE ready interrupt enable
  unsigned HSIRDYIE     : 1; // HSI ready interrupt enable
  unsigned HSERDYIE     : 1; // HSE ready interrupt enable
  unsigned PLLRDYIE     : 1; // PLL ready interrupt enable
  unsigned reserved2    : 3; // 
  unsigned LSIRDYC      : 1; // LSI ready interrupt clear
  unsigned LSERDYC      : 1; // LSE ready interrupt clear
  unsigned HSIRDYC      : 1; // HSI ready interrupt clear
  unsigned HSERDYC      : 1; // HSE ready interrupt clear
  unsigned PLLRDYC      : 1; // PLL ready interrupt clear
  unsigned reserved3    : 2; // 
  unsigned CSSC         : 1; // Clock security system interrupt clear
  unsigned reserved     : 8; // 
} __attribute__((__packed__)) RCC_CIR_t;
typedef struct { // RCC_APB2RSTR_t APB2 peripheral reset register

  unsigned AFIORST      : 1; // Alternate function I/O reset
  unsigned reserved1    : 1; // 
  unsigned IOPARST      : 1; // I/O port A reset
  unsigned IOPBRST      : 1; // IO port B reset
  unsigned IOPCRST      : 1; // IO port C reset
  unsigned IOPDRST      : 1; // I/O port D reset
  unsigned IOPERST      : 1; // I/O port E reset
  unsigned reserved2    : 1; // 
  unsigned ADC1RST      : 1; // ADC 1 interface reset
  unsigned ADC2RST      : 1; // ADC 2 interface reset
  unsigned TIM1RST      : 1; // TIM1 timer reset
  unsigned SPI1RST      : 1; // SPI 1 reset
  unsigned reserved3    : 1; // 
  unsigned USART1RST    : 1; // USART1 reset
  unsigned reserved4    : 17; // 

} __attribute__((__packed__)) RCC_APB2RSTR_t;
typedef struct { // RCC_APB1RSTR_t APB1 Peripheral Reset Register

  unsigned TIM2RST      : 1; // Timer 2 reset
  unsigned TIM3RST      : 1; // Timer 3 reset
  unsigned TIM4RST      : 1; // Timer 4 reset
  unsigned TIM5RST      : 1; // Timer 5 reset
  unsigned TIM6RST      : 1; // Timer 6 reset
  unsigned TIM7RST      : 1; // Timer 7 reset
  unsigned reserved1    : 5; // 
  unsigned WWDGRST      : 1; // Window watchdog reset 
  unsigned reserved2    : 2; // 
  unsigned SPI2RST      : 1; // SPI2 reset
  unsigned SPI3RST      : 1; // SPI3 reset
  unsigned reserved3    : 1; // 
  unsigned USART2RST    : 1; // USART 2 reset
  unsigned USART3RST    : 1; // USART 3 reset
  unsigned UART4RST     : 1; // USART 4 reset
  unsigned UART5RST     : 1; // USART 5 reset
  unsigned I2C1RST      : 1; // I2C1 reset
  unsigned I2C2RST      : 1; // I2C 2 reset
  unsigned reserved4    : 2; // 
  unsigned CAN1RST      : 1; // CAN1 reset
  unsigned CAN2RST      : 1; // CAN2 reset
  unsigned BKPRST       : 1; // Backup interface reset
  unsigned PWRRST       : 1; // Power interface reset
  unsigned DACRST       : 1; // DAC interface reset
  unsigned reserved5    : 2; // 
} __attribute__((__packed__)) RCC_APB1RSTR_t;
typedef struct { // RCC_AHBENR_t   AHB Peripheral Clock enable register 

  unsigned DMA1EN       : 1; // DMA1 clock enable
  unsigned DMA2EN       : 1; // DMA2 clock enable
  unsigned SRAMEN       : 1; // SRAM interface clock enable 
  unsigned reserved1    : 1; // 
  unsigned FLITFEN      : 1; // FLITF clock enable
  unsigned reserved2    : 1; // 
  unsigned CRCEN        : 1; // CRC clock enable
  unsigned reserved3    : 5; // 
  unsigned OTGFSEN      : 1; // USB OTG FS clock enable
  unsigned reserved4    : 1; // 
  unsigned ETHMACEN     : 1; // Ethernet MAC clock enable
  unsigned ETHMACTXEN   : 1; // Ethernet MAC TX clock enable
  unsigned ETHMACRXEN   : 1; // Ethernet MAC RX clock enable
  unsigned reserved     : 15; // 

} __attribute__((__packed__)) RCC_AHBENR_t;
typedef struct { // RCC_APB2_ENR_t APB2 peripheral clock enable register
  unsigned reserved1    : 1;
  unsigned USART1_EN    : 1;
  unsigned reserved2    : 1;
  unsigned SPI1_EN      : 1;
  unsigned TIM1_EN      : 1;
  unsigned reserved3    : 1;
  unsigned ADC1_EN      : 1;
  unsigned reserved4    : 2;
  unsigned IOPE_EN      : 1;
  unsigned IOPD_EN      : 1;
  unsigned IOPC_EN      : 1;
  unsigned IOPB_EN      : 1;
  unsigned IOPA_EN      : 1;
  unsigned reserved5    : 1;
  unsigned AFIO_EN      : 1;
} __attribute__((__packed__)) RCC_APB2_ENR_t;
typedef struct { // RCC_APB1_ENR_t APB1 peripheral clock enable register
  unsigned TIM2EN       : 1;
  unsigned TIM3EN       : 1;
  unsigned TIM4EN       : 1;
  unsigned reserved7    : 1;
  unsigned TIM6EN       : 1;
  unsigned TIM7EN       : 1;
  unsigned reserved6    : 5;
  unsigned WWDGEN       : 1;
  unsigned reserved5    : 2;
  unsigned SPI2EN       : 1;
  unsigned reserved4    : 2;
  unsigned USART2EN     : 1;
  unsigned USART3EN     : 1;
  unsigned reserved3    : 2;
  unsigned I2C1EN       : 1;
  unsigned I2C2EN       : 1;
  unsigned reserved2    : 4;
  unsigned BKPEN        : 1;
  unsigned PWREN        : 1;
  unsigned DACEN        : 1;
  unsigned CECEN        : 1;
  unsigned reserved1    : 1;
} __attribute__((__packed__)) RCC_APB1_ENR_t;
typedef struct { // RCC_BDCR_t     Backup Domain Control Register

  unsigned LSEON      : 1; // External Low Speed oscillator enable
  unsigned LSERDY     : 1; // External Low Speed oscillator ready
  unsigned LSEBYP     : 1; // External Low Speed oscillator bypass
  unsigned reserved1  : 5; // 
  unsigned RTCSEL     : 2; // RTC clock source selection
  unsigned reserved2  : 5; // 
  unsigned RTCEN      : 1; // RTC clock enable
  unsigned BDRST      : 1; // Backup domain software reset
  unsigned reserved3  : 15; // 
} __attribute__((__packed__)) RCC_BDCR_t;
typedef struct { // RCC_CSR_t      Control/status register

  unsigned LSION      : 1; // Internal low speed oscillator enable
  unsigned LSIRDY     : 1; // Internal low speed oscillator ready
  unsigned reserved1  : 22; // 
  unsigned RMVF       : 1; // Remove reset flag
  unsigned reserved2  : 1; // 
  unsigned PINRSTF    : 1; // PIN reset flag
  unsigned PORRSTF    : 1; // POR/PDR reset flag
  unsigned SFTRSTF    : 1; // Software reset flag
  unsigned IWDGRSTF   : 1; // Independent watchdog reset flag
  unsigned WWDGRSTF   : 1; // Window watchdog reset flag
  unsigned LPWRRSTF   : 1; // Low-power reset flag
} __attribute__((__packed__)) RCC_CSR_t;
typedef struct { // RCC_AHBRSTR_t  AHB Peripheral Clock Reset Register

  unsigned reserved1  : 12; //
  unsigned OTGFSRST   : 1; // USB OTG FS reset
  unsigned reserved2  : 1; // 
  unsigned ETHMACRST  : 1; // Ethernet MAC reset
  unsigned reserved3  : 17; // 
} __attribute__((__packed__)) RCC_AHBRSTR_t;
typedef struct { // RCC_CFGR2_t    Clock Configuration Register2

  unsigned PREDIV1    : 4; // PREDIV1 division factor
  unsigned PREDIV2    : 4; // PREDIV2 division factor
  unsigned PLL2MUL    : 4; // PLL2 Multiplication Factor
  unsigned PLL3MUL    : 4; // PLL3 Multiplication Factor
  unsigned PREDIV1SRC : 1; // PREDIV1 entry clock source
  unsigned I2S2SRC    : 1; // I2S2 clock source
  unsigned I2S3SRC    : 1; // I2S3 clock source
  unsigned reserved1  : 13; // 
}  __attribute__((__packed__)) RCC_CFGR2_t;
typedef struct { // register_stm32f1xx_rcc_t   Reset and Clock Control (RCC)

  RCC_CR_t        RCC_CR;
  RCC_CFGR_t      RCC_CFGR;
  RCC_CIR_t       RCC_CIR;
  RCC_APB2RSTR_t  RCC_APB2RSTR;
  RCC_APB1RSTR_t  RCC_APB1RSTR;
  RCC_AHBENR_t    RCC_AHBENR;
  RCC_APB2_ENR_t  RCC_APB2_ENR;
  RCC_APB1_ENR_t  RCC_APB1_ENR;
  RCC_BDCR_t      RCC_BDCR;
  RCC_CSR_t       RCC_CSR;
  RCC_AHBRSTR_t   RCC_AHBRSTR;
  RCC_CFGR2_t     RCC_CFGR2;

} __attribute__((__packed__)) register_stm32f1xx_rcc_t;

typedef struct { // TIMx_CR1_t     timer control register 1
  unsigned  CEN         : 1;
  unsigned  UDIS        : 1;
  unsigned  URS         : 1;
  unsigned  OPM         : 1;
  unsigned  DIR         : 1;
  unsigned  CMS         : 2;
  unsigned  ARPE        : 1;
  unsigned  CKD         : 2;
  unsigned  reserved1   : 6;
} __attribute__((__packed__)) TIMx_CR1_t;
typedef union  { // TIMER_CR1_u
    TIMx_CR1_t Fields;
    u16_t U16;
} TIMx_CR1_u;
typedef struct { // TIMx_CR2_t     timer control register 2
  unsigned  CCPC        : 1;
  unsigned  reserved2   : 1;
  unsigned  CCUS        : 1;
  unsigned  CCDS        : 1;
  unsigned  MMS         : 2;
  unsigned  TI1S        : 1;
  unsigned  OIS1        : 1;
  unsigned  OIS1N       : 1;
  unsigned  OIS2        : 1;
  unsigned  OIS2N       : 1;
  unsigned  OIS3        : 1;
  unsigned  OIS3N       : 1;
  unsigned  OIS4        : 1;
  unsigned  reserved1   : 1;
} __attribute__((__packed__)) TIMx_CR2_t;
typedef struct { // TIMx_SMCR_t    timer slave mode control register
  unsigned  SMS         : 3;
  unsigned  TS          : 3;
  unsigned  MSM         : 1;
  unsigned  ETF         : 4;
  unsigned  ETPS        : 2;
  unsigned  EXE         : 1;
  unsigned  ETP         : 1;
} __attribute__((__packed__)) TIMx_SMCR_t;
typedef struct { // TIMx_DIER_t    timer DMA/interrupt enable register
  unsigned  UIE         : 1;
  unsigned  CC1IE       : 1;
  unsigned  CC2IE       : 1;
  unsigned  CC3IE       : 1;
  unsigned  CC4IE       : 1;
  unsigned  COMIE       : 1;
  unsigned  TIE         : 1;
  unsigned  BIE         : 1;
  unsigned  UDE         : 1;
  unsigned  CC1DE       : 1;
  unsigned  CC2DE       : 1;
  unsigned  CC3DE       : 1;
  unsigned  CC4DE       : 1;
  unsigned  COMDE       : 1;
  unsigned  TDE         : 1;
  unsigned  reserved1   : 1;
} __attribute__((__packed__)) TIMx_DIER_t;
typedef union  { // TIMER_DIER_u
    TIMx_DIER_t Fields;
    u16_t U16;
} TIMx_DIER_u;
typedef struct { // TIMx_SR_t      timer status register
  unsigned  UIF         : 1;
  unsigned  CC1IF       : 1;
  unsigned  CC2IF       : 1;
  unsigned  CC3IF       : 1;
  unsigned  CC4IF       : 1;
  unsigned  COMIF       : 1;
  unsigned  TIF         : 1;
  unsigned  BIF         : 1;
  unsigned  reserved2   : 1;
  unsigned  CC1OF       : 1;
  unsigned  CC2OF       : 1;
  unsigned  CC3OF       : 1;
  unsigned  CC4OF       : 1;
  unsigned  reserved1   : 3;
} __attribute__((__packed__)) TIMx_SR_t;
typedef union  { // TIMER_DIER_u
    TIMx_SR_t Fields;
    u16_t U16;
} TIMx_SR_u;
typedef struct { // TIMx_EGR_t     timer event generation register
  unsigned  UG          : 1;
  unsigned  CC1G        : 1;
  unsigned  CC2G        : 1;
  unsigned  CC3G        : 1;
  unsigned  CC4G        : 1;
  unsigned  COMG        : 1;
  unsigned  TG          : 1;
  unsigned  BG          : 1;
  unsigned  reserved1   : 8;
} __attribute__((__packed__)) TIMx_EGR_t;
typedef struct { // TIMx_CCMR1_t   timer capture/compare register 1
  unsigned  CC1S        : 2;
  unsigned  OC1FE       : 1;
  unsigned  OC1PE       : 1;
  unsigned  OC1M        : 3;
  unsigned  OC1CE       : 1;
  unsigned  CC2S        : 2;
  unsigned  OC2FE       : 1;
  unsigned  OC2PE       : 1;
  unsigned  OC2M        : 3;
  unsigned  OC2CE       : 1;
} __attribute__((__packed__)) TIMx_CCMR1_t;
typedef struct { // TIMx_CCMR2_t   timer capture/compare register 2
  unsigned  CC3S        : 2;
  unsigned  OC3FE       : 1;
  unsigned  OC3PE       : 1;
  unsigned  OC3M        : 3;
  unsigned  OC3CE       : 1;
  unsigned  CC4S        : 2;
  unsigned  OC4FE       : 1;
  unsigned  OC4PE       : 1;
  unsigned  OC4M        : 3;
  unsigned  OC4CE       : 1;
} __attribute__((__packed__)) TIMx_CCMR2_t;
typedef struct { // TIMx_CCER_t    timer DMA/interrupt enable register
  unsigned  CC1E      : 1;
  unsigned  CC1P      : 1;
  unsigned  CC1NE     : 1;
  unsigned  CC1NP     : 1;
  unsigned  CC2E      : 1;
  unsigned  CC2P      : 1;
  unsigned  CC2NE     : 1;
  unsigned  CC2NP     : 1;
  unsigned  CC3E      : 1;
  unsigned  CC3P      : 1;
  unsigned  CC3NE     : 1;
  unsigned  CC3NP     : 1;
  unsigned  CC4E      : 1;
  unsigned  CC4P      : 1;
  unsigned  reserved1 : 2;
} __attribute__((__packed__)) TIMx_CCER_t;
typedef struct { // TIMx_BDTR_t    timer break and dead-time register
  unsigned  DTG         : 8;
  unsigned  LOCK        : 2;
  unsigned  OSSI        : 1;
  unsigned  OSSR        : 1;
  unsigned  BKE         : 1;
  unsigned  BKP_        : 1;
  unsigned  AOE         : 1;
  unsigned  MOE         : 1;
} __attribute__((__packed__)) TIMx_BDTR_t;
typedef struct { // TIMx_DCR_t     timer DMA control register
  unsigned  DBA         : 5;
  unsigned  reserved2   : 3;
  unsigned  DBL         : 5;
  unsigned  reserved1   : 1;
} __attribute__((__packed__)) TIMx_DCR_t;
typedef struct { // register_stm32f1xx_timer_t        timer 1 registers
  TIMx_CR1_t    CR1;
  u16_t         RESERVED0;
  TIMx_CR2_t    CR2;
  u16_t         RESERVED1;
  TIMx_SMCR_t   SMCR;
  u16_t         RESERVED2;
  TIMx_DIER_t   DIER;
  u16_t         RESERVED3;
  TIMx_SR_t     SR;
  u16_t         RESERVED4;
  TIMx_EGR_t    EGR;
  u16_t         RESERVED5;
  TIMx_CCMR1_t  CCMR1;
  u16_t         RESERVED6;
  TIMx_CCMR2_t  CCMR2;
  u16_t         RESERVED7;
  TIMx_CCER_t   CCER;
  u16_t         RESERVED8;
  u16_t         CNT;
  u16_t         RESERVED9;
  u16_t         PSC;
  u16_t         RESERVED10;
  u16_t         ARR;
  u16_t         RESERVED11;
  u16_t         RCR_; // RCC already defined elsewhere
  u16_t         RESERVED12;
  u16_t         CCR1;
  u16_t         RESERVED13;
  u16_t         CCR2;
  u16_t         RESERVED14;
  u16_t         CCR3;
  u16_t         RESERVED15;
  u16_t         CCR4;
  u16_t         RESERVED16;
  TIMx_BDTR_t   BDTR;
  u16_t         RESERVED17;
  TIMx_DCR_t    DCR;
  u16_t         RESERVED18;
  u16_t         DMAR;
  u16_t         RESERVED19;
} register_stm32f1xx_timer_t;

typedef struct { // USART_SR_t     USART Status Register
  unsigned PE   : 1; // Parity Error
  unsigned FE   : 1; // Framing Error
  unsigned NE   : 1; // Noise Error
  unsigned ORE  : 1; // Overrun Error
  unsigned IDLE : 1; // IDLE line detected
  unsigned RXNE : 1; // Read Data Register not empty
  unsigned TC   : 1; // Transmission complete
  unsigned TXE  : 1; // Transmission Data Register empty
  unsigned LBD  : 1; // Lin break detection flag
  unsigned CTS  : 1; // CTS Flag
  unsigned Reserved1: 6;
} __attribute__((__packed__)) USART_SR_t;
typedef struct { // USART_DR_t     USART Data Register
  unsigned Data: 9;
  unsigned Reserved1: 7;
} __attribute__((__packed__)) USART_DR_t;
typedef struct { // USART_BRR_t    USART Baud Rate Register
  unsigned DIV_Fraction: 4;
  unsigned DIV_Mantissa: 12;
} __attribute__((__packed__)) USART_BRR_t;
typedef struct { // USART_CR1_t    USART Control Register 1
  unsigned SBK: 1;    // Send break
  unsigned RWU: 1;    // Receiver wakeup
  unsigned RE: 1;     // Receiver enable
  unsigned TE: 1;     // Transmitter enable
  unsigned IDLEIE: 1; // IDLE interrupt enable
  unsigned RXNEIE: 1; // RXNE interrupt enable
  unsigned TCIE: 1;   // Transmission complete interrupt enable
  unsigned TXEIE: 1;  // TXE interrupt enable
  unsigned PEIE: 1;   // PE interrupt enable
  unsigned PS: 1;     // Parity selection
  unsigned PCE: 1;    // Parity control enable
  unsigned WAKE: 1;   // Wakeup method
  unsigned M: 1;      // Word length
  unsigned UE: 1;     // USART enable
  unsigned Reserved1: 2;

} __attribute__((__packed__)) USART_CR1_t;
typedef union  { // USART_CR1_u
    USART_CR1_t Fields;
    u16_t U16;
} USART_CR1_u;
typedef struct { // USART_CR2_t    USART Control Register 2

  unsigned ADD: 4;        // Address of the USART node
  unsigned Reserved1 : 1; // forced by hardware to 0.
  unsigned LBDL: 1;       // lin break detection length
  unsigned LBDIE: 1;      // LIN break detection interrupt enable
  unsigned Reserved2 : 1; // forced by hardware to 0.
  unsigned LBCL: 1;       // Last bit clock pulse
  unsigned CPHA: 1;       // Clock phase
  unsigned CPOL: 1;       // Clock polarity
  unsigned CLKEN: 1;      // Clock enable
  unsigned STOP: 2;       // STOP bits
  unsigned LINEN: 1;      // LIN mode enable
  unsigned Reserved3 :1; // forced by hardware to 0.

} __attribute__((__packed__)) USART_CR2_t;
typedef union  { // USART_CR2_u
    USART_CR2_t Fields;
    u16_t U16;
} USART_CR2_u;
typedef struct { // USART_CR3_t    USART Control Register 3

  unsigned EIE: 1;   // Error interrupt enable
  unsigned IREN: 1;  // IrDA mode enable
  unsigned IRLP: 1;  // IrDA low-power
  unsigned HDSEL: 1; // Half-duplex selection
  unsigned NACK: 1;  // Smartcard NACK enable
  unsigned SCEN: 1;  // Smartcard mode enable
  unsigned DMAR: 1;  // DMA enable receiver
  unsigned DMAT: 1;  // DMA enable transmitter
  unsigned RTSE: 1;  // RTS enable
  unsigned CTSE: 1;  // CTS enable
  unsigned CTSIE: 1; // CTS interrupt enable
  unsigned Reserved : 5; // forced by hardware to 0.

} __attribute__((__packed__)) USART_CR3_t;
typedef union  { // USART_CR3_u
    USART_CR3_t Fields;
    u16_t U16;
} USART_CR3_u;
typedef struct { // USART_GPTR_t   USART Guard Time and Prescaler Register
  unsigned PSC : 8;       // Prescaler
  unsigned GT  : 8;       // Guard Time
//X  unsigned Reserved : 16; // forced by hardware to 0.
} __attribute__((__packed__)) USART_GPTR_t;
typedef struct { // register_stm32f1xx_usart_t        USART registers
  USART_SR_t   SR;
  u16_t        reserved1;
  USART_DR_t   DR;
  u16_t        reserved2;
  USART_BRR_t  BRR;
  u16_t        reserved3;
  USART_CR1_t  CR1;
  u16_t        reserved4;
  USART_CR2_t  CR2;
  u16_t        reserved5;
  USART_CR3_t  CR3;
  u16_t        reserved6;
  USART_GPTR_t GPTR;
  u16_t        reserved7;
} __attribute__((__packed__)) register_stm32f1xx_usart_t;

typedef struct { // SPI_CR1_t      SPI Control Register 1 (->RM0008 p.715)
  
  unsigned CPHA     :  1; // Bit 0 CPHA: Clock phase
  unsigned CPOL     :  1; // Bit 1 CPOL: Clock polarity
  unsigned MSTR     :  1; // Bit 2 MSTR: Master selection
  unsigned BR       :  3; // Bits 5:3 BR[2:0]: Baud rate control
  unsigned SPE      :  1; // Bit 6 SPE: SPI enable
  unsigned LSBFIRST :  1; // Bit 7 LSBFIRST: Frame format
  unsigned SSI      :  1; // Bit 8 SSI: Internal slave select
  unsigned SSM      :  1; // Bit 9 SSM: Software slave management
  unsigned RXONLY   :  1; // Bit 10 RXONLY: Receive only
  unsigned DFF      :  1; // Bit 11 DFF: Data frame format
  unsigned CRCNEXT  :  1; // Bit 12 CRCNEXT: CRC transfer next
  unsigned CRCEN    :  1; // Bit 13 CRCEN: Hardware CRC calculation enable
  unsigned BIDIOE   :  1; // Bit 14 BIDIOE: Output enable in bidirectional mode
  unsigned BIDIMODE :  1; // Bit 15 BIDIMODE: Bidirectional data mode enable
  unsigned Reserved : 16; // kept at zero


} __attribute__((__packed__)) SPI_CR1_t;
typedef struct { // SPI_CR2_t      SPI Control Register 2 (->RM0008 p.716)

  unsigned RXDMAEN   :  1; // Bit 0 RXDMAEN: Rx buffer DMA enable
  unsigned TXDMAEN   :  1; // Bit 1 TXDMAEN: Tx buffer DMA enable
  unsigned SSOE      :  1; // Bit 2 SSOE: SS output enable
  unsigned Reserved1 :  2; // Bits 4:3 Reserved, must be kept at reset value.
  unsigned ERRIE     :  1; // Bit 5 ERRIE: Error interrupt enable
  unsigned RXNEIE    :  1; // Bit 6 RXNEIE: RX buffer not empty interrupt enable
  unsigned TXEIE     :  1; // Bit 7 TXEIE: Tx buffer empty interrupt enable
  unsigned Reserved2 : 24; // Bits 15:8 Reserved, must be kept at reset value.

} __attribute__((__packed__)) SPI_CR2_t;
typedef struct { // SPI_SR_t       SPI Status Register (->RM0008 p.717)
  
  unsigned RXNE      :  1; // Bit 0 RXNE: Receive buffer not empty
  unsigned TXE       :  1; // Bit 1 TXE: Transmit buffer empty
  unsigned CHSIDE    :  1; // Bit 2 CHSIDE: Channel side
  unsigned UDR       :  1; // Bit 3 UDR: Underrun flag
  unsigned CRCERR    :  1; // Bit 4 CRCERR: CRC error flag
  unsigned MODF      :  1; // Bit 5 MODF: Mode fault
  unsigned OVR       :  1; // Bit 6 OVR: Overrun flag
  unsigned BSY       :  1; // Bit 7 BSY: Busy flag
  unsigned Reserved  : 24; // Bits 15:8 Reserved, must be kept at reset value.

} __attribute__((__packed__)) SPI_SR_t;
typedef struct { // SPI_DR_t       SPI Data Register (->RM0008 p.718)
  unsigned Data     : 16;        // data received or transmitted
  unsigned Reserved : 16;        // kept at zero
} __attribute__((__packed__)) SPI_DR_t;
typedef struct { // SPI_CRC_t      SPI CRC Register (->RM0008 p.718)
  unsigned Polynom  : 16;        // polynomial used for CRC calculation 
  unsigned Reserved : 16;        // kept at zero
} __attribute__((__packed__)) SPI_CRC_t;
typedef struct { // SPI_RX_CRC_t   SPI Computed CRC value of received data (->RM0008 p.719)
  unsigned Value    : 16;        // calculated CRC value (only lower 8 bits valid in 8-bit mode)
  unsigned Reserved : 16;        // kept at zero
} __attribute__((__packed__)) SPI_RX_CRC_t;
typedef struct { // SPI_TX_CRC_t   SPI Computed CRC value of transmitted data (->RM0008 p.719)
  unsigned Value    : 16;        // calculated CRC value (only lower 8 bits valid in 8-bit mode)
  unsigned Reserved : 16;        // kept at zero
} __attribute__((__packed__)) SPI_TX_CRC_t;
typedef struct { // SPI_I2SCFGR_t  SPI I2S-Configuration Register (->RM0008 p.720)
  
  unsigned CHLEN      :  1; // Bit 0 CHLEN: Channel length (number of bits per audio channel)
  unsigned DATLEN     :  2; // Bit 2:1 DATLEN: Data length to be transferred
  unsigned CKPOL      :  1; // Bit 3 CKPOL: Steady state clock polarity
  unsigned I2SSTD     :  1; // Bit 5:4 I2SSTD: I2S standard selection
  unsigned Reserved1  :  1; // Bit 6 Reserved: forced at 0 by hardware
  unsigned PCMSYNC    :  1; // Bit 7 PCMSYNC: PCM frame synchronization
  unsigned I2SCFG     :  2; // Bit 9:8 I2SCFG: I2S configuration mode
  unsigned I2SE       :  1; // Bit 10 I2SE: I2S Enable
  unsigned I2SMOD     :  1; // Bit 11 I2SMOD: I2S mode selection
  unsigned Reserved2  : 20; // Bits 15:12 Reserved, must be kept at reset value.

} __attribute__((__packed__)) SPI_I2SCFGR_t;
typedef struct { // SPI_I2SPR_t    SPI I2S Prescaler Register (->RM0008 p.721)

  unsigned I2SDIV    :  8; // Bit 7:0 I2SDIV: I2S Linear prescaler
  unsigned ODD       :  1; // Bit 8 ODD: Odd factor for the prescaler
  unsigned MCKOE     :  1; // Bit 9 MCKOE: Master clock output enable
  unsigned Reserved  : 22; // Bits 15:10 Reserved, must be kept at reset value.
  
} __attribute__((__packed__)) SPI_I2SPR_t;
typedef struct { // register_stm32f1xx_spi_t          Serial Peripheral Interface
  SPI_CR1_t     CR1;
  SPI_CR2_t     CR2;
  SPI_SR_t      SR;
  SPI_DR_t      DR;
  SPI_CRC_t     CRC_Polynom;
  SPI_RX_CRC_t  RX_CRC;
  SPI_TX_CRC_t  TX_CRC;
  SPI_I2SCFGR_t I2SCFGR;
  SPI_I2SPR_t   I2SPR;
} __attribute__((__packed__)) register_stm32f1xx_spi_t;

typedef struct { // I2C_CR1_t      I2C Control Register 1 (->RM0008 p.744)
  unsigned PE            : 1;  // Peripheral enable
  unsigned SMBUS         : 1;  // SMBus mode
  unsigned Reserved1     : 1;  // must be kept at reset value
  unsigned SMBTYPE       : 1;  // SMBus type
  unsigned ENARP         : 1;  // ARP enable
  unsigned ENPEC         : 1;  // PEC enable
  unsigned ENGC          : 1;  // General call enable
  unsigned NOSTRETCH     : 1;  // Clock stretching disable (Slave mode)
  unsigned START         : 1;  // Start generation
  unsigned STOP          : 1;  // Stop generation
  unsigned ACK           : 1;  // Acknowledge enable
  unsigned POS           : 1;  // Acknowledge/PEC Position (for data reception)
  unsigned PEC           : 1;  // Packet error checking
  unsigned ALERT         : 1;  // SMBus alert
  unsigned Reserved2     : 1;  // must be kept at reset value
  unsigned SWRST         : 1;  // Software reset

} __attribute__((__packed__)) I2C_CR1_t;
typedef struct { // I2C_CR2_t      I2C Control Register 2 (->RM0008 p.746)
  unsigned FREQ          : 6;  // Peripheral clock frequency
  unsigned Reserved1     : 2;  // must be kept at reset value
  unsigned ITERREN       : 1;  // Error interrupt enable
  unsigned ITEVTEN       : 1;  // Event interrupt enable
  unsigned ITBUFEN       : 1;  // Buffer interrupt enable
  unsigned DMAEN         : 1;  // DMA requests enable
  unsigned LAST          : 1;  // DMA last transfer
  unsigned Reserved2     : 3;  // must be kept at reset value

} __attribute__((__packed__)) I2C_CR2_t;
typedef struct { // I2C_OAR1_7b_t  I2C Own Address Register 1 for 7 bit adressing mode (->RM0008 p.748)
  unsigned ADD0          : 1;  // not used
  unsigned ADD           : 7;  // Interface address
  unsigned ADD98         : 2;  // not used
  unsigned Reserved1     : 4;  // must be kept at reset value
  unsigned Reserved2     : 1;  // must be kept at reset value
  unsigned ADDMODE       : 1;  // Addressing mode (slave mode)

} __attribute__((__packed__)) I2C_OAR1_7b_t;
typedef struct { // I2C_OAR1_10b_t I2C Own Address Register 1 for 10 bit adressing mode (->RM0008 p.748)
  unsigned ADD           : 10; // Interface address
  unsigned Reserved1     : 4;  // must be kept at reset value
  unsigned Reserved2     : 1;  // must be kept at reset value
  unsigned ADDMODE       : 1;  // Addressing mode (slave mode)

} __attribute__((__packed__)) I2C_OAR1_10b_t;
typedef struct { // I2C_OAR1_t     I2C Own Address Register 1 (->RM0008 p.748)
  union {
    I2C_OAR1_7b_t  OAR1_7Bit;
    I2C_OAR1_10b_t OAR1_10Bit;
  } Bits;
} __attribute__((__packed__)) I2C_OAR1_t;
typedef struct { // I2C_OAR2_t     I2C Own Address Register 2 (->RM0008 p.748)
  unsigned ENDUAL        : 1;  // Dual addressing mode enable
  unsigned ADD2          : 7;  // Interface address
  unsigned Reserved1     : 8;  // must be kept at reset value
} __attribute__((__packed__)) I2C_OAR2_t;
typedef struct { // I2C_DR_t       I2C Data Register (->RM0008 p.749)
  unsigned DR            : 8;  // 8-bit data register
  unsigned Reserved1     : 8;  // must be kept at reset value
} __attribute__((__packed__)) I2C_DR_t;
typedef struct { // I2C_SR1_t      I2C Status Register 1 (->RM0008 p.749)
  unsigned SB            : 1;  // Start bit (Master mode)
  unsigned ADDR          : 1;  // Address sent (master mode)/matched (slave mode)
  unsigned BTF           : 1;  // Byte transfer finished
  unsigned ADD10         : 1;  // 10-bit header sent (Master mode)
  unsigned STOPF         : 1;  // Stop detection (slave mode)
  unsigned Reserved1     : 1;  // must be kept at reset value
  unsigned RXNE          : 1;  // Data register not empty (receivers)
  unsigned TxE           : 1;  // Data register empty (transmitters)
  unsigned BERR          : 1;  // Bus error
  unsigned ARLO          : 1;  // Arbitration lost (master mode)
  unsigned AF            : 1;  // Acknowledge failure
  unsigned OVR           : 1;  // Overrun/Underrun
  unsigned PECERR        : 1;  // PEC Error in reception
  unsigned Reserved2     : 1;  // must be kept at reset value
  unsigned TIMEOUT       : 1;  // Timeout or Tlow error
  unsigned SMBALERT      : 1;  // SMBus alert
} __attribute__((__packed__)) I2C_SR1_t;
typedef struct { // I2C_SR2_t      I2C Status Register 2 (->RM0008 p.753)
  unsigned MSL           : 1;  // Master/ Slave
  unsigned BUSY          : 1;  // Bus busy
  unsigned TRA           : 1;  // Transmitter/receiver
  unsigned Reserved1     : 1;  // must be kept at reset value
  unsigned GENCALL       : 1;  // General call address (Slave mode)
  unsigned SMBDEFAULT    : 1;  // SMBus device default address (Slave mode)
  unsigned SMBHOST       : 1;  // SMBus host header (Slave mode)
  unsigned DUALF         : 1;  // Dual flag (Slave mode)
  unsigned PEC           : 8;  // Packet error checking register
} __attribute__((__packed__)) I2C_SR2_t;
typedef struct { // I2C_CCR_t      I2C Clock Control Register (->RM0008 p.754)
  unsigned CCR           : 12; // Clock control register in Fast/Standard mode (Master mode)
  unsigned Reserved1     : 2;  // must be kept at reset value
  unsigned DUTY          : 1;  // Fast mode duty cycle
  unsigned Fast          : 1;  // I2C fast mode selection
} __attribute__((__packed__)) I2C_CCR_t;
typedef struct { // I2C_TRISE_t    I2C Max Rise Time in Master Mode (->RM0008 p.755)
  unsigned TRISE         : 6;  // Maximum rise time in Fast/Standard mode (Master mode)
  unsigned Reserved1     : 10; // must be kept at reset value
} __attribute__((__packed__)) I2C_TRISE_t;
typedef struct { // register_stm32f1xx_i2c_t          Inter-Integrated Circuit Interface

  I2C_CR1_t    CR1;
  unsigned PadWord1 : 16; // pad to next 32-bit address
  I2C_CR2_t    CR2;
  unsigned PadWord2 : 16; // pad to next 32-bit address
  I2C_OAR1_t   OAR1;
  unsigned PadWord3 : 16; // pad to next 32-bit address
  I2C_OAR2_t   OAR2;
  unsigned PadWord4 : 16; // pad to next 32-bit address
  I2C_DR_t     DR;
  unsigned PadWord5 : 16; // pad to next 32-bit address
  I2C_SR1_t    SR1;
  unsigned PadWord6 : 16; // pad to next 32-bit address
  I2C_SR2_t    SR2;
  unsigned PadWord7 : 16; // pad to next 32-bit address
  I2C_CCR_t    CCR;
  unsigned PadWord8 : 16; // pad to next 32-bit address
  I2C_TRISE_t  TRISE;
  unsigned PadWord9 : 16; // pad to next 32-bit address
} __attribute__((__packed__)) register_stm32f1xx_i2c_t;


//}STM32 Registers
/** { Register Declarations
 *
 * Declaring this registers here allows easy code completion in all files including this header.
 *
 */

extern volatile register_stm32f1xx_rcc_t           register_stm32f1xx_RCC;
extern volatile register_stm32f1xx_timer_t         register_stm32f1xx_TIMER1;
extern volatile register_stm32f1xx_timer_t         register_stm32f1xx_TIMER2;
extern volatile register_stm32f1xx_timer_t         register_stm32f1xx_TIMER3;
extern volatile register_stm32f1xx_timer_t         register_stm32f1xx_TIMER4;
extern volatile register_stm32f1xx_usart_t         register_stm32f1xx_USART1;
extern volatile register_stm32f1xx_usart_t         register_stm32f1xx_USART2;
extern volatile register_stm32f1xx_usart_t         register_stm32f1xx_USART3;
extern volatile register_stm32f1xx_usart_t         register_stm32f1xx_UART4;
extern volatile register_stm32f1xx_usart_t         register_stm32f1xx_UART5;
extern volatile register_stm32f1xx_spi_t           register_stm32f1xx_SPI1;
extern volatile register_stm32f1xx_spi_t           register_stm32f1xx_SPI2;
extern volatile register_stm32f1xx_spi_t           register_stm32f1xx_SPI3;
extern volatile register_stm32f1xx_i2c_t           register_stm32f1xx_I2C1;
extern volatile register_stm32f1xx_i2c_t           register_stm32f1xx_I2C2;
extern volatile register_stm32f1xx_gpio_t          register_stm32f1xx_GPIOA;
extern volatile register_stm32f1xx_gpio_t          register_stm32f1xx_GPIOB;
extern volatile register_stm32f1xx_gpio_t          register_stm32f1xx_GPIOC;
extern volatile register_stm32f1xx_gpio_t          register_stm32f1xx_GPIOD;
extern volatile register_stm32f1xx_gpio_t          register_stm32f1xx_GPIOE;
extern volatile register_stm32f1xx_gpio_t          register_stm32f1xx_GPIOF;
extern volatile register_stm32f1xx_gpio_t          register_stm32f1xx_GPIOG;
extern volatile register_stm32f1xx_afio_t          register_stm32f1xx_AFIO;
extern volatile register_stm32f1xx_exti_t          register_stm32f1xx_EXTI;
extern volatile DBGMCU_TypeDef                   register_stm32f1xx_DGBMCU;

//}Register Declarations

#endif //STM32_REGISTERS_H
