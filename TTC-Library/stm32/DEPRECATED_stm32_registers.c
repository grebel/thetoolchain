/*{ stm32_Registers.c ************************************************
 
 Structured pointers to registers of ARM CortexM3 microcontrollers.

 written by Gregor Rebel 2011-2011

}*/

#include "../register/register_stm32f1xx.h"

/** { Global variables are mapped onto hardware registers in linker script memory_stm32f1xx.ld
 *
 * Note: When adding new register variables here, make sure to add them in the linker script too!
 *
 */

volatile register_stm32f1xx_rcc_t           register_stm32f1xx_RCC;
volatile register_stm32f1xx_timer_t         register_stm32f1xx_TIMER1;
volatile register_stm32f1xx_timer_t         register_stm32f1xx_TIMER2;
volatile register_stm32f1xx_timer_t         register_stm32f1xx_TIMER3;
volatile register_stm32f1xx_timer_t         register_stm32f1xx_TIMER4;
volatile register_stm32f1xx_usart_t         register_stm32f1xx_USART1;
volatile register_stm32f1xx_usart_t         register_stm32f1xx_USART2;
volatile register_stm32f1xx_usart_t         register_stm32f1xx_USART3;
volatile register_stm32f1xx_usart_t         register_stm32f1xx_UART4;
volatile register_stm32f1xx_usart_t         register_stm32f1xx_UART5;
volatile register_stm32f1xx_spi_t           register_stm32f1xx_SPI1;
volatile register_stm32f1xx_spi_t           register_stm32f1xx_SPI2;
volatile register_stm32f1xx_spi_t           register_stm32f1xx_SPI3;
volatile register_stm32f1xx_i2c_t           register_stm32f1xx_I2C1;
volatile register_stm32f1xx_i2c_t           register_stm32f1xx_I2C2;
volatile register_stm32f1xx_gpio_t          register_stm32f1xx_GPIOA;
volatile register_stm32f1xx_gpio_t          register_stm32f1xx_GPIOB;
volatile register_stm32f1xx_gpio_t          register_stm32f1xx_GPIOC;
volatile register_stm32f1xx_gpio_t          register_stm32f1xx_GPIOD;
volatile register_stm32f1xx_gpio_t          register_stm32f1xx_GPIOE;
volatile register_stm32f1xx_gpio_t          register_stm32f1xx_GPIOF;
volatile register_stm32f1xx_gpio_t          register_stm32f1xx_GPIOG;
volatile register_stm32f1xx_afio_t          register_stm32f1xx_AFIO;
volatile register_stm32f1xx_exti_t          register_stm32f1xx_EXTI;
volatile DBGMCU_TypeDef                     register_stm32f1xx_DGBMCU;


//}
