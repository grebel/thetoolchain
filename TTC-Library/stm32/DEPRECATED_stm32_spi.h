#ifndef STM32_SPI_H
#define STM32_SPI_H

/*{ stm32_spi.h **********************************************************
 
                      The ToolChain
                      
   Universal Synchronous Asynchronous Receiver and Transmitters (SPIs)
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Gregor Rebel 2012

 
}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

#include "DEPRECATED_stm32_spi_types.h"
#include "../DEPRECATED_ttc_spi_types.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"
#include "../gpio/gpio_stm32f1xx.h"
//X #include <stdlib.h>

#define USE_STDPERIPH_DRIVER
#include "stm32f10x.h"
#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
#include "stm32f10x_conf.h"
#include "stm32f10x_spi.h"
#include "misc.h"

//} Includes
//{ Function prototypes **************************************************

/** checks if indexed SPI bus already has been initialized
 * @param SPI_Index     device index of SPI to check (1..ttc_spi_get_max_index())
 * @return              == TRUE: indexed SPI unit is already initialized
 */
bool stm32_spi_check_initialized(u8_t SPI_Index);

/** returns reference to configuration struct of indexed SPI device
 * @param SPI_Index     device index of SPI to init (1..ttc_spi_get_max_index())
 * @return              pointer to struct ttc_spi_generic_t (will assert if no configuration available)
 */
ttc_spi_generic_t* stm32_spi_get_configuration(u8_t SPI_Index);

/** loads configuration of indexed SPI interface with default values
 * @param SPI_Index     device index of SPI to init (1..ttc_spi_get_max_index())
 * @return  == 0:       configuration was loaded successfully
 */
ttc_spi_errorcode_e stm32_spi_load_defaults(u8_t SPI_Index);

/** fills out given SPI_Generic with maximum valid values for indexed SPI
 * @param SPI_Index     device index of SPI to init (1..ttc_spi_get_max_index())
 * @param SPI_Generic   pointer to struct ttc_spi_generic_t
 * @return  == 0:         *SPI_Generic has been initialized successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32_spi_get_features(u8_t SPI_Index, ttc_spi_generic_t* SPI_Generic);

/** initializes single SPI
 * @param SPI_Index     device index of SPI to init (1..ttc_spi_get_max_index())
 * @return              == 0: SPI has been initialized successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32_spi_init(u8_t SPI_Index);

/** Send out given amount of raw bytes from buffer.
 * Will not interpret bytes in buffer.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * 
 * @param SPI_Index    device index of SPI to init (0..ttc_spi_get_max_index()-1)
 * @param Buffer         memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[]
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32_spi_send_raw(u8_t SPI_Index, const char* Buffer, u16_t Amount);

/** Send out given bytes from buffer until first zero byte or MaxLength reached.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * 
 * @param SPI_Index    device index of SPI to init (0..ttc_spi_get_max_index()-1)
 * @param Buffer         memory location from which to read data
 * @param MaxLength      no more than this amount of bytes will be send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32_spi_send_string(u8_t SPI_Index, const char* Buffer, u16_t MaxLength);

/** Send out given data word (8 or 9 bits)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param SPI_Index    device index of SPI to init (0..ttc_spi_get_max_index()-1)
 * @param Byte           8 or 9 bit value to send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32_spi_send_word(u8_t SPI_Index, const u16_t Word);

/** Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param SPI_Index    device index of SPI to init (0..ttc_spi_get_max_index()-1)
 * @param Word           pointer to 16 bit buffer where to store Word
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32_spi_read_word(u8_t SPI_Index, u16_t* Word, u32_t TimeOut);

/** Reads single data byte from input buffer.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param SPI_Index    device index of SPI to init (0..ttc_spi_get_max_index()-1)
 * @param Word           pointer to 16 bit buffer where to store Word
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32_spi_read_byte(u8_t SPI_Index, u8_t* Byte, u32_t TimeOut);

/** returns single port pin used for indexed SPI
 *
 * @param SPI_Index    device index of SPI to init (0..ttc_spi_get_max_index()-1)
 * @param Pin          name of pin to return
 * @return             !=NULL: pointer to requested port pin data
 *                     ==NULL: requested pin is not configured
 */
ttc_gpio_pin_e* stm32_spi_get_port(u8_t SPI_Index, ttc_spi_pins_e Pin);

/** resets library. Automatically called.
 * Note: This function is private to stm32_spi.h and should not be called from outside!
 */
void _spi_reset_all();

/** Sends out given byte (asserts nSS before and deasserts it after).
 * Note: This low-level function should not be called from outside!
 */
void _stm32_spi_send_single_word(stm32_spi_architecture_t* SPI_Arch, u16_t Word);

/** load pin configuration into given structure
 * @return: index of pin layout being determined
 */
u8_t _spi_get_pins(u8_t SPI_Index, stm32_spi_architecture_t* SPI_Arch);

/** Wait until word has been received or timeout occured.
 * Note: This low-level function should not be called from outside!
 */
ttc_spi_errorcode_e _stm32_spi_wait_for_RXNE(stm32_spi_architecture_t* SPI_Arch, u32_t TimeOut);

//} Function prototypes

#endif //STM32_SPI_H
