#ifndef STM32_EEPROMEMULATION_H
#define STM32_EEPROMEMULATION_H

/*{ stm32_EEPROM_EMULATION.h **********************************************************
 
                      The ToolChain
                      
   USB for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Sascha Poggemann 2012

 
}*/
//{ Defines/ TypeDefs ****************************************************



//} Defines
//{ Includes *************************************************************


#define stm32_eeprom_read_variable(VirtAddress, Data) EE_ReadVariable(VirtAddress, Data)
#define stm32_eeprom_write_variable(VirtAddress,Data) EE_WriteVariable(VirtAddress, Data)

#define USE_STDPERIPH_DRIVER
#include "stm32f10x.h"
#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
#include "stm32f10x_conf.h"
#include "misc.h"
#include "stm32f10x_flash.h"


#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)


//} Includes *************************************************************
//{ Function prototypes **************************************************

BOOL stm32_eeprom_init(void);

//} Function prototypes

#endif //STM32_EEPROMEMULATION_H
