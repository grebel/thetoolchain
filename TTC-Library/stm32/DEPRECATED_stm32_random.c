/** stm32_random.c ***********************************************

 * Written by Gregor Rebel 2010-2017
 *
 * Random number generation
 * Platform driver for 32-bit ARM architecture
 *
}*/
#include "stm32_random.h"

// still no real low-level support (even after 3 years of development ;-)
