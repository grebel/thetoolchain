/*{ stm32_spi::.c ************************************************
 
                      The ToolChain
                      
   Universal Synchronous Asynchronous Receiver and Transmitters (stm32_SPIs)
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Gregor Rebel 2012
   
   Note: See ttc_spi.h for description of architecture independent SPI implementation.
 
}*/

#include "DEPRECATED_stm32_spi.h"

ttc_heap_array_define(ttc_spi_generic_t*, stm32_SPI_Configs, TTC_AMOUNT_SPIS);

u8_t ss_Initialized=0;

//{ Function definitions *************************************************

               bool stm32_spi_check_initialized(u8_t SPI_Index) {
    if (! A(stm32_SPI_Configs, SPI_Index-1) )
        return FALSE;

    if (A(stm32_SPI_Configs, SPI_Index-1)->SPI_Arch)
        return TRUE;

    return FALSE;
}
ttc_spi_generic_t* stm32_spi_get_configuration(u8_t SPI_Index) {
    if (!ss_Initialized) _spi_reset_all();
    Assert_SPI(SPI_Index > 0, ec_InvalidArgument);
    Assert(SPI_Index <= TTC_AMOUNT_SPIS, ec_InvalidConfiguration);

    if (! A(stm32_SPI_Configs, SPI_Index-1)) {
        A(stm32_SPI_Configs, SPI_Index-1) = ttc_heap_alloc_zeroed( sizeof(ttc_spi_generic_t) );
        stm32_spi_load_defaults(SPI_Index);
    }
    return A(stm32_SPI_Configs, SPI_Index-1);
}
ttc_spi_errorcode_e stm32_spi_load_defaults(u8_t SPI_Index) {
    Assert_SPI(SPI_Index > 0, ec_InvalidArgument);

    ttc_spi_generic_t* SPI_Cfg = stm32_spi_get_configuration(SPI_Index);
    Assert_SPI(SPI_Cfg, ec_NULL);

    SPI_Cfg->Flags.All                        = 0;
    SPI_Cfg->Flags.Bits.Master                = 1;
    SPI_Cfg->Flags.Bits.MultiMaster           = 0;
    SPI_Cfg->Flags.Bits.ClockIdleHigh         = 1;
    SPI_Cfg->Flags.Bits.ClockPhase2ndEdge     = 1;
    SPI_Cfg->Flags.Bits.Simplex               = 0;
    SPI_Cfg->Flags.Bits.Bidirectional         = 1;
    SPI_Cfg->Flags.Bits.Receive               = 1;
    SPI_Cfg->Flags.Bits.Transmit              = 1;
    SPI_Cfg->Flags.Bits.WordSize16            = 0;
    SPI_Cfg->Flags.Bits.HardwareNSS           = 1;
    SPI_Cfg->Flags.Bits.CRC8                  = 0;
    SPI_Cfg->Flags.Bits.CRC16                 = 0;
    SPI_Cfg->Flags.Bits.TxDMA                 = 0;
    SPI_Cfg->Flags.Bits.RxDMA                 = 0;
    SPI_Cfg->Flags.Bits.FirstBitMSB           = 1;
    SPI_Cfg->Flags.Bits.Irq_Error             = 0;
    SPI_Cfg->Flags.Bits.Irq_Received          = 0;
    SPI_Cfg->Flags.Bits.Irq_Transmitted       = 0;

    SPI_Cfg->CRC_Polynom = 0x7;
    SPI_Cfg->Layout      = 0;
    
    RCC_ClocksTypeDef RCC_Clocks;
    RCC_GetClocksFreq(&RCC_Clocks);
    
    //X SPI_Cfg->BaudRatePrescaler   = 16;        // medium speed
    SPI_Cfg->BaudRate  = RCC_Clocks.PCLK2_Frequency / 16; // medium speed

    return tse_OK;
}
ttc_spi_errorcode_e stm32_spi_get_features(u8_t SPI_Index, ttc_spi_generic_t* SPI_Generic) {
    if (!ss_Initialized) _spi_reset_all();
#ifndef TTC_NO_ARGUMENT_CHECKS //{
    // assumption: *SPI_Generic has been zeroed

    Assert_SPI(SPI_Index > 0,    ec_InvalidArgument);
    Assert_SPI(SPI_Generic != NULL, ec_NULL);
    if (SPI_Index > TTC_AMOUNT_SPIS) return tse_DeviceNotFound;
#endif //}

    SPI_Generic->Flags.All                        = 0;
    SPI_Generic->Flags.Bits.Master                = 1;
    SPI_Generic->Flags.Bits.MultiMaster           = 1;
    SPI_Generic->Flags.Bits.ClockIdleHigh         = 1;
    SPI_Generic->Flags.Bits.ClockPhase2ndEdge     = 1;
    SPI_Generic->Flags.Bits.Simplex               = 1;
    SPI_Generic->Flags.Bits.Bidirectional         = 1;
    SPI_Generic->Flags.Bits.Receive               = 1;
    SPI_Generic->Flags.Bits.Transmit              = 1;
    SPI_Generic->Flags.Bits.WordSize16            = 1;
    SPI_Generic->Flags.Bits.HardwareNSS           = 1;
    SPI_Generic->Flags.Bits.CRC8                  = 1;
    SPI_Generic->Flags.Bits.CRC16                 = 1;
    SPI_Generic->Flags.Bits.FirstBitMSB           = 1;
    SPI_Generic->Flags.Bits.TxDMA                 = 0; // ToDo
    SPI_Generic->Flags.Bits.RxDMA                 = 0; // ToDo
    SPI_Generic->Flags.Bits.Irq_Error             = 0; // ToDo
    SPI_Generic->Flags.Bits.Irq_Received          = 0; // ToDo
    SPI_Generic->Flags.Bits.Irq_Transmitted       = 0; // ToDo

    SPI_Generic->CRC_Polynom = 0xffff;
    SPI_Generic->Layout      = 0;
    
    RCC_ClocksTypeDef RCC_Clocks;
    RCC_GetClocksFreq(&RCC_Clocks);
    
    //X SPI_Generic->BaudRatePrescaler   = 255;                             // max divider
    SPI_Generic->BaudRate  = RCC_Clocks.PCLK2_Frequency / 2;  // max frequency

    SPI_TypeDef* SPI_Base = NULL;
    switch (SPI_Index) {           // find corresponding SPI as defined by makefile.100_board_*
#ifdef TTC_SPI1
      case 1: SPI_Base = TTC_SPI1; break;
#endif
#ifdef TTC_SPI2
      case 2: SPI_Base = TTC_SPI2; break;
#endif
#ifdef TTC_SPI3
      case 3: SPI_Base = TTC_SPI3; break;
#endif
      default: Assert_SPI(0, ec_UNKNOWN); break; // No TTC_SPIn defined! Check your makefile.100_board_* file!
    }
    switch ( (u32_t) SPI_Base ) {  // find amount of available remapping-layouts (-> RM0008 p. 176)
    case (u32_t) SPI1:
        SPI_Generic->Layout=1;     // SPI1 provides 1 alternate pin layout
        break;
    case (u32_t) SPI2:
        SPI_Generic->Layout=0;     // SPI2 provides 0 alternate pin layouts
    case (u32_t) SPI3:
#ifdef STM32F10X_CL
            SPI_Generic->Layout=1; // SPI3 provides 1 alternate pin layout in Connection Line Devices
#else
            SPI_Generic->Layout=0; // SPI3 provides 0 alternate pin layout
#endif
        break;
      default: Assert_SPI(0, ec_UNKNOWN);
    }
    if (1) { // check if configured NSS-pin can be used for HardwareNSS
#if defined(TTC_SPI1_NSS) || defined(TTC_SPI2_NSS) || defined(TTC_SPI3_NSS)
        stm32_spi_architecture_t SPI_Arch;
        _spi_get_pins(SPI_Index, &SPI_Arch);
        ttc_gpio_pin_e Port;
#ifdef TTC_SPI1_NSS
        if (SPI_Index == 1)  Port = TTC_SPI1_NSS;
#endif
#ifdef TTC_SPI2_NSS
        if (SPI_Index == 2)  Port = TTC_SPI2_NSS;
#endif
#ifdef TTC_SPI3_NSS
        if (SPI_Index == 3)  Port = TTC_SPI3_NSS;
#endif
        if (Port != SPI_Arch.PortNSS)
            SPI_Generic->Flags.Bits.HardwareNSS = 0; // must use SoftwareNSS for configured pin
#else
        SPI_Generic->Flags.Bits.HardwareNSS = 0; // must use SoftwareNSS for configured pin
#endif
    }

    switch (SPI_Index) {           // disable features requiring unconfigured pins
      case 1: {
          #ifndef TTC_SPI1_MOSI
            SPI_Generic->Flags.Bits.Transmit = 0;
          #endif
          #ifndef TTC_SPI1_MISO
            SPI_Generic->Flags.Bits.Receive  = 0;
          #endif
          #ifndef TTC_SPI1_NSS
            SPI_Generic->Flags.Bits.HardwareNSS = 0;
          #else

          #endif
          #ifndef TTC_SPI1_SCK
            SPI_Generic->Flags.Bits.Transmit = 0;
            SPI_Generic->Flags.Bits.Receive  = 0;
          #endif
          
          break;
      }
      case 2: {
          #ifndef TTC_SPI2_MOSI
            SPI_Generic->Flags.Bits.Transmit = 0;
          #endif
          #ifndef TTC_SPI2_MISO
            SPI_Generic->Flags.Bits.Receive  = 0;
          #endif
          #ifndef TTC_SPI2_NSS
            SPI_Generic->Flags.Bits.HardwareNSS  = 0;
          #endif
          #ifndef TTC_SPI2_SCK
            SPI_Generic->Flags.Bits.Transmit = 0;
            SPI_Generic->Flags.Bits.Receive  = 0;
          #endif
          
          break;
      }
      case 3: {
          #ifndef TTC_SPI3_MOSI
            SPI_Generic->Flags.Bits.Transmit = 0;
          #endif
          #ifndef TTC_SPI3_MISO
            SPI_Generic->Flags.Bits.Receive  = 0;
          #endif
          #ifndef TTC_SPI3_NSS
            SPI_Generic->Flags.Bits.HardwareNSS  = 0;
          #endif
          #ifndef TTC_SPI3_SCK
            SPI_Generic->Flags.Bits.Transmit = 0;
            SPI_Generic->Flags.Bits.Receive  = 0;
          #endif

          break;
      }
      default: Assert_SPI(0, ec_UNKNOWN); return tse_DeviceNotFound;; // SPI misconfigured! Check your makefile.100_board_* file!
    }
    
    return tse_OK;
}
ttc_spi_errorcode_e stm32_spi_init(u8_t SPI_Index) {
    if (!ss_Initialized) _spi_reset_all();
    // assumption: *SPI_Arch has been zeroed

    Assert_SPI(SPI_Index > 0, ec_InvalidArgument);
    ttc_spi_generic_t* SPI_Generic = stm32_spi_get_configuration(SPI_Index);
    Assert_SPI(SPI_Generic != NULL, ec_NULL);
    stm32_spi_architecture_t* SPI_Arch = SPI_Generic->SPI_Arch;
    if (SPI_Arch == NULL)
        SPI_Arch = ttc_heap_alloc_zeroed( sizeof(ttc_spi_architecture_t) );

    if (1) { // validate SPI_Features
        ttc_spi_generic_t SPI_Features;
        ttc_spi_errorcode_e Error=stm32_spi_get_features(SPI_Index, &SPI_Features);
        if (Error) return Error;
        SPI_Generic->Flags.All &= SPI_Features.Flags.All; // mask out unavailable flags
        if (SPI_Generic->Layout > SPI_Features.Layout)
            SPI_Generic->Layout = SPI_Features.Layout;
//X        if (SPI_Generic->BaudRatePrescaler > SPI_Features.BaudRatePrescaler)
//X            SPI_Generic->BaudRatePrescaler = SPI_Features.BaudRatePrescaler;
        if (SPI_Generic->BaudRate > SPI_Features.BaudRate)
            SPI_Generic->BaudRate = SPI_Features.BaudRate;
    }
    switch (SPI_Index) {                // find SPI corresponding to SPI_index as defined by makefile.100_board_*
#ifdef TTC_SPI1
      case 1: SPI_Arch->Base = (register_stm32f1xx_spi_t*) TTC_SPI1; break;
#endif
#ifdef TTC_SPI2
      case 2: SPI_Arch->Base = (register_stm32f1xx_spi_t*) TTC_SPI2; break;
#endif
#ifdef TTC_SPI3
      case 3: SPI_Arch->Base = (register_stm32f1xx_spi_t*) TTC_SPI3; break;
#endif
      default: Assert_SPI(0, ec_UNKNOWN); break; // No TTC_SPIn defined! Check your makefile.100_board_* file!
    }
    SPI_Generic->Layout = _spi_get_pins(SPI_Index, SPI_Arch);
    if (1) { // compare chosen pin layout with configured pins
        //tse_InvalidPinConfig
        ttc_gpio_pin_e Port;
        switch (SPI_Index) { // check if board has defined the TX-pin of SPI to init
          
          case 1: { // check configuration of SPI #1
  #ifdef TTC_SPI1_MOSI
              Port = TTC_SPI1_MOSI;
              Assert_SPI(Port == SPI_Arch->PortMOSI, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              SPI_Arch->PortMOSI = tgp_none;
  #endif
  #ifdef TTC_SPI1_MISO
              Port = TTC_SPI1_MISO;
              Assert_SPI(Port == SPI_Arch->PortMISO, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              SPI_Arch->PortMISO = tgp_none;
  #endif
#ifdef TTC_SPI1_NSS
              if (SPI_Generic->Flags.Bits.HardwareNSS) {
                  Port = TTC_SPI1_NSS;
                  Assert_SPI(Port == SPI_Arch->PortNSS, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
              }
#else
              SPI_Arch->PortNSS = tgp_none;
  #endif
  #ifdef TTC_SPI1_SCK
              Port = TTC_SPI1_SCK;
              Assert_SPI(Port == SPI_Arch->PortSCK, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              SPI_Arch->PortSCK = tgp_none;
  #endif
              break;
          }
          case 2: { // check configuration of SPI #2
  #ifdef TTC_SPI2_MOSI
              Port = TTC_SPI2_MOSI;
              Assert_SPI(Port == SPI_Arch->PortMOSI, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              SPI_Arch->PortMOSI = tgp_none;
  #endif
  #ifdef TTC_SPI2_MISO
              Port = TTC_SPI2_MISO;
              Assert_SPI(Port == SPI_Arch->PortMISO, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              SPI_Arch->PortMISO = tgp_none;
  #endif
  #ifdef TTC_SPI2_NSS
              if (SPI_Generic->Flags.Bits.HardwareNSS) {
                  Port = TTC_SPI2_NSS;
                  Assert_SPI(Port == SPI_Arch->PortNSS, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              SPI_Arch->PortNSS = tgp_none;
  #endif
  #ifdef TTC_SPI2_SCK
              Port = TTC_SPI2_SCK;
              Assert_SPI(Port == SPI_Arch->PortSCK, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              SPI_Arch->PortSCK = tgp_none;
  #endif
              break;
          }
          case 3: { // check configuration of SPI #3
  #ifdef TTC_SPI3_MOSI
              stm32_gpio_variable(&Port, TTC_SPI3_MOSI);
              Assert_SPI(Port == SPI_Arch->PortMOSI.GPIOx, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              SPI_Arch->PortMOSI = tgp_none;
  #endif
  #ifdef TTC_SPI3_MISO
              Port = TTC_SPI3_MISO;
              Assert_SPI(Port == SPI_Arch->PortMISO, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              SPI_Arch->PortMISO = tgp_none;
  #endif
  #ifdef TTC_SPI3_NSS
              if (SPI_Generic->Flags.Bits.HardwareNSS) {
                  Port = TTC_SPI3_NSS;
                  Assert_SPI(Port == SPI_Arch->PortNSS, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              SPI_Arch->PortNSS = tgp_none;
  #endif
  #ifdef TTC_SPI3_SCK
              Port = TTC_SPI3_SCK;
              Assert_SPI(Port == SPI_Arch->PortSCK, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              SPI_Arch->PortSCK = tgp_none;
  #endif
              break;
          }
        default: Assert_SPI(0, ec_UNKNOWN); break; // No TTC_SPIn defined! Check your makefile.100_board_* file!
            
        }
    }
    SPI_I2S_DeInit( (SPI_TypeDef*) SPI_Arch->Base);
    if (1) { // activate clocks
        switch ( (u32_t) SPI_Arch->Base ) {
          case (u32_t) SPI1: {
              if (SPI_Generic->Layout) { // GPIOB + GPIOA
                  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1 | RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
              }
              else {                     // GPIOA
                  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1 | RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);
              }
              break;
          }
          case (u32_t) SPI2: {
              RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2 , ENABLE);
              RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
              break;
          }
          case (u32_t) SPI3: {
              if (SPI_Generic->Layout) { // GPPIOA, GPIOC
                  RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3, ENABLE);
                  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO, ENABLE);
              }
              else {                     // GPPIOA. GPPIOB
                  RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3, ENABLE);
                  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
              }
              break;
          }
          default: Assert_SPI(0, ec_UNKNOWN); break; // should never occur!
        }
    }
    if (1) { // configure GPIOs

        if (SPI_Generic->Flags.Bits.Master) {
            if (SPI_Arch->PortMOSI)  gpio_stm32f1xx_init(SPI_Arch->PortMOSI, tgm_alternate_function_push_pull,       tgs_Max);
            if (SPI_Arch->PortMISO)  gpio_stm32f1xx_init(SPI_Arch->PortMISO, tgm_input_floating, tgs_Max);
            if (SPI_Arch->PortSCK)   gpio_stm32f1xx_init(SPI_Arch->PortSCK,  tgm_alternate_function_push_pull,       tgs_Max);
            if (SPI_Arch->PortNSS) {
                if (SPI_Generic->Flags.Bits.HardwareNSS)
                    gpio_stm32f1xx_init(SPI_Arch->PortNSS,  tgm_alternate_function_push_pull,  tgs_Max);
                else
                    gpio_stm32f1xx_init(SPI_Arch->PortNSS,  tgm_output_push_pull, tgs_Max);
            }
        }
        else {
            if (SPI_Arch->PortMOSI)  gpio_stm32f1xx_init(SPI_Arch->PortMOSI, tgm_input_floating, tgs_Max);
            if (SPI_Arch->PortMISO)  gpio_stm32f1xx_init(SPI_Arch->PortMISO, tgm_alternate_function_push_pull,       tgs_Max);
            if (SPI_Arch->PortSCK)   gpio_stm32f1xx_init(SPI_Arch->PortSCK,  tgm_input_floating, tgs_Max);
            if (SPI_Arch->PortNSS)   gpio_stm32f1xx_init(SPI_Arch->PortNSS,  tgm_input_floating, tgs_Max);
        }
    }
    if (1) { // apply remap layout

        switch ( (u32_t) SPI_Arch->Base ) {
          case (u32_t) SPI1: { // SPI1
              if (SPI_Generic->Layout)
                  GPIO_PinRemapConfig(GPIO_Remap_SPI1, ENABLE);
              else
                  GPIO_PinRemapConfig(GPIO_Remap_SPI1, DISABLE);
  
              break;
          }
          case (u32_t) SPI3: { // SPI3
              if (SPI_Generic->Layout)   // (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
                  GPIO_PinRemapConfig(GPIO_Remap_SPI3, ENABLE);
              else                      // no pin remapping
                  GPIO_PinRemapConfig(GPIO_Remap_SPI3, DISABLE);
              break;
          }
  
          default: { break; }
        }
    }
    if (1) { // init SPI

        SPI_InitTypeDef SPI_MyInit;
        memset( &SPI_MyInit, 0, sizeof(SPI_InitTypeDef) );

        if (SPI_Generic->Flags.Bits.Transmit && SPI_Generic->Flags.Bits.Receive) {
          if (SPI_Generic->Flags.Bits.Bidirectional)   SPI_MyInit.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
          else                                         SPI_MyInit.SPI_Direction = SPI_Direction_1Line_Rx | SPI_Direction_1Line_Tx;
        }
        else if (SPI_Generic->Flags.Bits.Receive)      SPI_MyInit.SPI_Direction = SPI_Direction_2Lines_RxOnly;
        
        if (SPI_Generic->Flags.Bits.Master)            SPI_MyInit.SPI_Mode = SPI_Mode_Master;
        else                                           SPI_MyInit.SPI_Mode = SPI_Mode_Slave;
        
        if (SPI_Generic->Flags.Bits.WordSize16)        SPI_MyInit.SPI_DataSize = SPI_DataSize_16b;
        else                                           SPI_MyInit.SPI_DataSize = SPI_DataSize_8b;
        
        if (SPI_Generic->Flags.Bits.ClockIdleHigh)     SPI_MyInit.SPI_CPOL = SPI_CPOL_High;
        else                                           SPI_MyInit.SPI_CPOL = SPI_CPOL_Low;
        
        if (SPI_Generic->Flags.Bits.ClockPhase2ndEdge) SPI_MyInit.SPI_CPHA = SPI_CPHA_2Edge;
        else                                           SPI_MyInit.SPI_CPHA = SPI_CPHA_1Edge;
        
        if (SPI_Generic->Flags.Bits.HardwareNSS)       SPI_MyInit.SPI_NSS = SPI_NSS_Hard;
        else                                           SPI_MyInit.SPI_NSS = SPI_NSS_Soft;
        
        if (SPI_Generic->Flags.Bits.FirstBitMSB)       SPI_MyInit.SPI_FirstBit = SPI_FirstBit_MSB;
        else                                           SPI_MyInit.SPI_FirstBit = SPI_FirstBit_LSB;

        Assert_SPI(SPI_Generic->BaudRate > 0, ec_InvalidArgument);

        // calculate ideal prescaler from baudrate
        RCC_ClocksTypeDef RCC_Clocks;
        RCC_GetClocksFreq(&RCC_Clocks);
        u16_t Prescaler = RCC_Clocks.PCLK2_Frequency / SPI_Generic->BaudRate;

        // approximate baudrate prescaler + calculate effective baudrate
        if (Prescaler < 3) {
            SPI_MyInit.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
            SPI_Generic->BaudRate = RCC_Clocks.PCLK2_Frequency / 2;
        }
        else if (Prescaler < 7) {
            SPI_MyInit.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
            SPI_Generic->BaudRate = RCC_Clocks.PCLK2_Frequency / 4;
        }
        else if (Prescaler < 13) {
            SPI_MyInit.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
            SPI_Generic->BaudRate = RCC_Clocks.PCLK2_Frequency / 8;
        }
        else if (Prescaler < 25) {
            SPI_MyInit.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;
            SPI_Generic->BaudRate = RCC_Clocks.PCLK2_Frequency / 16;
        }
        else if (Prescaler < 49) {
            SPI_MyInit.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;
            SPI_Generic->BaudRate = RCC_Clocks.PCLK2_Frequency / 32;
        }
        else if (Prescaler < 97) {
            SPI_MyInit.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_64;
            SPI_Generic->BaudRate = RCC_Clocks.PCLK2_Frequency / 64;
        }
        else if (Prescaler < 193) {
            SPI_MyInit.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128;
            SPI_Generic->BaudRate = RCC_Clocks.PCLK2_Frequency / 128;
        }
        else {
            SPI_MyInit.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;
            SPI_Generic->BaudRate = RCC_Clocks.PCLK2_Frequency / 256;
        }

        if (SPI_Generic->Flags.Bits.CRC8 || SPI_Generic->Flags.Bits.CRC16) {
            SPI_MyInit.SPI_CRCPolynomial = SPI_Generic->CRC_Polynom;
            SPI_CalculateCRC( (SPI_TypeDef*) SPI_Arch->Base, ENABLE);
        }
        else {
            SPI_MyInit.SPI_CRCPolynomial = 0;
            SPI_CalculateCRC((SPI_TypeDef*) SPI_Arch->Base, DISABLE);
        }        
        
        // translate SPI_MyInit back to SPI_Generic
        if (SPI_MyInit.SPI_Direction & SPI_Direction_2Lines_FullDuplex) {
          SPI_Generic->Flags.Bits.Bidirectional = 1;
          SPI_Generic->Flags.Bits.Simplex       = 0;
          SPI_Generic->Flags.Bits.Transmit      = 1;
          SPI_Generic->Flags.Bits.Receive       = 1;
        }
        else if (SPI_MyInit.SPI_Direction & SPI_Direction_2Lines_RxOnly) {
          SPI_Generic->Flags.Bits.Bidirectional = 0;
          SPI_Generic->Flags.Bits.Simplex       = 0;
          SPI_Generic->Flags.Bits.Transmit      = 0;
          SPI_Generic->Flags.Bits.Receive       = 1;
          
        }
        else {
          SPI_Generic->Flags.Bits.Bidirectional = 0;
          SPI_Generic->Flags.Bits.Simplex       = 1;
          
          SPI_Generic->Flags.Bits.Receive       = 1;
          if (SPI_MyInit.SPI_Direction & SPI_Direction_1Line_Tx)  SPI_Generic->Flags.Bits.Transmit = 1;
          if (SPI_MyInit.SPI_Direction & SPI_Direction_1Line_Rx)  SPI_Generic->Flags.Bits.Receive  = 1;
          
        }
        
        if (SPI_MyInit.SPI_Mode == SPI_Mode_Master)       SPI_Generic->Flags.Bits.Master = 1;
        else                                              SPI_Generic->Flags.Bits.Master = 0;
        if (SPI_MyInit.SPI_DataSize == SPI_DataSize_16b)  SPI_Generic->Flags.Bits.WordSize16 = 1;
        else                                              SPI_Generic->Flags.Bits.WordSize16 = 0;
        
        if (SPI_MyInit.SPI_CPOL == SPI_CPOL_High) SPI_Generic->Flags.Bits.ClockIdleHigh = 1;
        else                                      SPI_Generic->Flags.Bits.ClockIdleHigh = 0;
        if (SPI_MyInit.SPI_CPHA == SPI_CPHA_2Edge) SPI_Generic->Flags.Bits.ClockPhase2ndEdge = 1;
        else                                       SPI_Generic->Flags.Bits.ClockPhase2ndEdge = 0;
        if (SPI_MyInit.SPI_NSS == SPI_NSS_Soft) SPI_Generic->Flags.Bits.HardwareNSS = 0;
        else                                    SPI_Generic->Flags.Bits.HardwareNSS = 1;
        if (SPI_MyInit.SPI_FirstBit == SPI_FirstBit_LSB) SPI_Generic->Flags.Bits.FirstBitMSB = 1;
        else                                             SPI_Generic->Flags.Bits.FirstBitMSB = 0;

        SPI_Generic->CRC_Polynom       = SPI_MyInit.SPI_CRCPolynomial;

        SPI_Init((SPI_TypeDef*) SPI_Arch->Base, &SPI_MyInit);

        if (SPI_Generic->Flags.Bits.Master) { // workaround: SSOE is not configured by StdPeripheralLib!
            if (SPI_Generic->Flags.Bits.HardwareNSS)
                ( (register_stm32f1xx_spi_t*) SPI_Arch->Base )->CR2.SSOE = 1;
            else
                ( (register_stm32f1xx_spi_t*) SPI_Arch->Base )->CR2.SSOE = 0;
        }
    }
    if (0) { // setup interrupts
        if (SPI_Generic->Flags.Bits.Irq_Error)
          SPI_I2S_ITConfig((SPI_TypeDef*) SPI_Arch->Base, SPI_I2S_IT_ERR, ENABLE);
        
        if (SPI_Generic->Flags.Bits.Irq_Received)
          SPI_I2S_ITConfig((SPI_TypeDef*) SPI_Arch->Base, SPI_I2S_IT_RXNE, ENABLE);
        
        if (SPI_Generic->Flags.Bits.Irq_Transmitted)
          SPI_I2S_ITConfig((SPI_TypeDef*) SPI_Arch->Base, SPI_I2S_IT_TXE, ENABLE);

        /* Enable and set SPIn Interrupt to the lowest priority */
        NVIC_InitTypeDef NVIC_InitStructure;
        switch ( (u32_t) SPI_Arch->Base ) {

#ifdef SPI1_IRQn
          case (u32_t) SPI1: NVIC_InitStructure.NVIC_IRQChannel=SPI1_IRQn; break;
#endif
#ifdef SPI2_IRQn
          case (u32_t) SPI2: NVIC_InitStructure.NVIC_IRQChannel=SPI2_IRQn; break;
#endif
#ifdef SPI3_IRQn
          case (u32_t) SPI3: NVIC_InitStructure.NVIC_IRQChannel=SPI3_IRQn; break;
#endif

          default: { break; }
        }

        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0x0F;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority=0x0F;
        NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
        NVIC_Init(&NVIC_InitStructure);
    }
    
    SPI_Cmd((SPI_TypeDef*) SPI_Arch->Base, ENABLE);
    A(stm32_SPI_Configs, SPI_Index-1)->SPI_Arch = SPI_Arch;
    
    return tse_OK;
}
ttc_spi_errorcode_e stm32_spi_send_raw(u8_t SPI_Index, const char* Buffer, u16_t Amount) {
  Assert_SPI(SPI_Index > 0, ec_NULL);
  stm32_spi_architecture_t* SPI_Arch = A(stm32_SPI_Configs, SPI_Index-1)->SPI_Arch;
  Assert_SPI(SPI_Arch != NULL, ec_NULL);

  if (1) { // use StdPeripheralLibrary (slower but maybe more compatible)
      while (Amount-- > 0) {
          _stm32_spi_send_single_word(SPI_Arch, (u16_t) *Buffer++);
      }
  }
  else { // direct register access (faster) (time for "Hello world!\n\r": 69.2us @2.2MHz SCLK)
      register_stm32f1xx_spi_t* MySPI = (register_stm32f1xx_spi_t*) SPI_Arch->Base;
      BOOL SoftNSS = MySPI->CR1.SSM && SPI_Arch->PortNSS;
      SPI_SR_t* SR = &(MySPI->SR);
      if (SoftNSS) { gpio_stm32f1xx_clr( &(SPI_Arch->PortNSS) ); }

      while (Amount-- > 0) {
          while (!SR->TXE);
          MySPI->DR.Data = (u16_t) *Buffer++;
          while (!SR->TXE);
          while (SR->BSY);
      }
      if (SoftNSS) { gpio_stm32f1xx_set( &(SPI_Arch->PortNSS) ); }
  }
  return tse_OK;
}
ttc_spi_errorcode_e stm32_spi_send_string(u8_t SPI_Index, const char* Buffer, u16_t MaxLength) {
    Assert_SPI(SPI_Index > 0, ec_NULL);
    stm32_spi_architecture_t* SPI_Arch = A(stm32_SPI_Configs, SPI_Index-1)->SPI_Arch;
    Assert_SPI(SPI_Arch != NULL, ec_NULL);
    register_stm32f1xx_spi_t* MySPI = (register_stm32f1xx_spi_t*) SPI_Arch->Base;

    char C=0;
    if (0) { // use StdPeripherals Library (slower) (time for "Hello world!\n\r": 122us @2.2MHz SCLK)
        while ( (MaxLength-- > 0) && ((C=*Buffer++) != 0)  ) {
            _stm32_spi_send_single_word(SPI_Arch, (u16_t) C);
        }
    }
    else { // direct register access (faster) (time for "Hello world!\n\r": 69.2us @2.2MHz SCLK)
        BOOL SoftNSS = MySPI->CR1.SSM && SPI_Arch->PortNSS;
        SPI_SR_t* SR = &(MySPI->SR);
        if (SoftNSS) { gpio_stm32f1xx_clr( &(SPI_Arch->PortNSS) ); }

        while ( (MaxLength-- > 0) && ((C=*Buffer++) != 0)  ) {
            while (!SR->TXE);
            MySPI->DR.Data = (u16_t) C;
            while (!SR->TXE);
            while (SR->BSY);
        }
        if (SoftNSS) { gpio_stm32f1xx_set( &(SPI_Arch->PortNSS) ); }
    }
    return tse_OK;
}
ttc_spi_errorcode_e stm32_spi_send_word(u8_t SPI_Index, const u16_t Word) {
    Assert_SPI(SPI_Index > 0, ec_NULL);
    _stm32_spi_send_single_word(A(stm32_SPI_Configs, SPI_Index-1)->SPI_Arch, Word);
    return tse_OK;
}
ttc_spi_errorcode_e stm32_spi_read_byte(u8_t SPI_Index, u8_t* Byte, u32_t TimeOut) {
    Assert_SPI(SPI_Index > 0, ec_NULL);
    stm32_spi_architecture_t* SPI_Arch = A(stm32_SPI_Configs, SPI_Index-1)->SPI_Arch;
    Assert_SPI(SPI_Arch != NULL, ec_NULL);
    register_stm32f1xx_spi_t* MySPI = (register_stm32f1xx_spi_t*) SPI_Arch->Base;
    Assert_SPI(MySPI != NULL, ec_NULL);

    if ( _stm32_spi_wait_for_RXNE(SPI_Arch, TimeOut) ) return tse_TimeOut;

    *Byte = (u8_t) 0xff & ((SPI_TypeDef*) MySPI)->DR; //SPI_I2S_ReceiveData( (SPI_TypeDef*) MySPI);

    return tse_OK;
}
ttc_spi_errorcode_e stm32_spi_read_word(u8_t SPI_Index, u16_t* Word, u32_t TimeOut) {
    Assert_SPI(SPI_Index > 0, ec_NULL);
    stm32_spi_architecture_t* SPI_Arch = A(stm32_SPI_Configs, SPI_Index-1)->SPI_Arch;
    Assert_SPI(SPI_Arch != NULL, ec_NULL);
    register_stm32f1xx_spi_t* MySPI = A(stm32_SPI_Configs, SPI_Index-1)->SPI_Arch->Base;
    Assert_SPI(MySPI != NULL, ec_NULL);

    if ( _stm32_spi_wait_for_RXNE(SPI_Arch, TimeOut) ) return tse_TimeOut;

    *Word = SPI_I2S_ReceiveData( (SPI_TypeDef*) MySPI  );
    // MySPI->SPI_SR.RXNE=0;

    return tse_OK;
}
ttc_gpio_pin_e* stm32_spi_get_port(u8_t SPI_Index, ttc_spi_pins_e Pin) {
    Assert_SPI(SPI_Index > 0, ec_NULL);
    stm32_spi_architecture_t* SPI_Arch = A(stm32_SPI_Configs, SPI_Index-1)->SPI_Arch;
    Assert_SPI(SPI_Arch != NULL, ec_NULL);

    switch (Pin) {
    case ttc_spi_MOSI:
        return (ttc_gpio_pin_e*) &(SPI_Arch->PortMOSI);
    case ttc_spi_MISO:
        return (ttc_gpio_pin_e*) &(SPI_Arch->PortMISO);
    case ttc_spi_SCK:
        return (ttc_gpio_pin_e*) &(SPI_Arch->PortSCK);
    case ttc_spi_NSS:
        return (ttc_gpio_pin_e*) &(SPI_Arch->PortNSS);
    default: break;
    }

    return NULL;
}

//} Function definitions
//{ private functions (ideally) -------------------------------------------------

void _spi_reset_all() {
    if (!ss_Initialized) {
        for (u8_t Index = 0; Index < TTC_AMOUNT_SPIS; Index++)
            A(stm32_SPI_Configs, Index) = NULL;
    }
    ss_Initialized=1;
}
u8_t _spi_get_pins(u8_t SPI_Index, stm32_spi_architecture_t* SPI_Arch) {
    u8_t Layout = 0;
    ttc_gpio_pin_e PortMOSI;

    switch (SPI_Index) { // check if board has defined the MOSI-pin of SPI to init
#ifdef TTC_SPI1_MOSI
    case 1:
        PortMOSI = TTC_SPI1_MOSI;

        break;
#endif
#ifdef TTC_SPI2_MOSI
    case 2:
        PortMOSI = TTC_SPI2_MOSI;
        break;
#endif
#ifdef TTC_SPI3_MOSI
    case 3:
        PortMOSI = TTC_SPI3_MOSI;
        break;
#endif
    default:
        Assert_SPI(0, ec_UNKNOWN);   // No TTC_SPIn_MOSI defined! Check your makefile.100_board_* file!
        break;
    }

    switch ( (u32_t) SPI_Arch->Base ) { // determine pin layout (-> RM0008 p.176)
    case (u32_t) SPI1: { // SPI1
        if (PortMOSI == tgp_a15) Layout = 1;
        else                    Layout = 0;

        if (Layout) { // alternate pin remapping (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
         /*   SPI_Arch->PortNSS.GPIOx  = stm32_GPIOA;
            SPI_Arch->PortNSS.Pin    = 15;
            SPI_Arch->PortSCK.GPIOx  = stm32_GPIOB;
            SPI_Arch->PortSCK.Pin    = 3;
            SPI_Arch->PortMISO.GPIOx = stm32_GPIOB;
            SPI_Arch->PortMISO.Pin   = 4;
            SPI_Arch->PortMOSI.GPIOx = stm32_GPIOA;
            SPI_Arch->PortMOSI.Pin   = 5;
         */
            SPI_Arch->PortNSS   = tgp_a15;
            SPI_Arch->PortSCK   = tgp_b3;
            SPI_Arch->PortMISO  = tgp_b4;
            SPI_Arch->PortMOSI  = tgp_a5;
        }
        else {        // no pin remapping
         /*
            SPI_Arch->PortNSS.GPIOx  = stm32_GPIOA;
            SPI_Arch->PortNSS.Pin    = 4;
            SPI_Arch->PortSCK.GPIOx  = stm32_GPIOA;
            SPI_Arch->PortSCK.Pin    = 5;
            SPI_Arch->PortMISO.GPIOx = stm32_GPIOA;
            SPI_Arch->PortMISO.Pin   = 6;
            SPI_Arch->PortMOSI.GPIOx = stm32_GPIOA;
            SPI_Arch->PortMOSI.Pin   = 7;
         */
            SPI_Arch->PortNSS   = tgp_a4;
            SPI_Arch->PortSCK   = tgp_a5;
            SPI_Arch->PortMISO  = tgp_a6;
            SPI_Arch->PortMOSI  = tgp_a7;
        }

        break;
    }
    case (u32_t) SPI2: { // SPI2 has no alternative layout
      /*
        SPI_Arch->PortMOSI.GPIOx = stm32_GPIOB;
        SPI_Arch->PortMOSI.Pin   = 15;
        SPI_Arch->PortMISO.GPIOx = stm32_GPIOB;
        SPI_Arch->PortMISO.Pin   = 14;
        SPI_Arch->PortSCK.GPIOx  = stm32_GPIOB;
        SPI_Arch->PortSCK.Pin    = 13;
        SPI_Arch->PortNSS.GPIOx  = stm32_GPIOB;
        SPI_Arch->PortNSS.Pin    = 12;
      */
        SPI_Arch->PortNSS   = tgp_b15;
        SPI_Arch->PortSCK   = tgp_b14;
        SPI_Arch->PortMISO  = tgp_b13;
        SPI_Arch->PortMOSI  = tgp_b12;
        break;
    }
    case (u32_t) SPI3: { // SPI3
        if (PortMOSI == tgp_c12)      Layout=1; // pin remapping
        else                         Layout=0; // no pin remapping

        if (Layout) { // alternate pin remapping (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
          /*
            SPI_Arch->PortNSS.GPIOx  = stm32_GPIOA;
            SPI_Arch->PortNSS.Pin    = 4;
            SPI_Arch->PortSCK.GPIOx  = stm32_GPIOC;
            SPI_Arch->PortSCK.Pin    = 10;
            SPI_Arch->PortMISO.GPIOx = stm32_GPIOC;
            SPI_Arch->PortMISO.Pin   = 11;
            SPI_Arch->PortMOSI.GPIOx = stm32_GPIOC;
            SPI_Arch->PortMOSI.Pin   = 12;
          */
            SPI_Arch->PortNSS   = tgp_a4;
            SPI_Arch->PortSCK   = tgp_c10;
            SPI_Arch->PortMISO  = tgp_c11;
            SPI_Arch->PortMOSI  = tgp_c12;
        }
        else {                     // no pin remapping
          /*
            SPI_Arch->PortNSS.GPIOx  = stm32_GPIOA;
            SPI_Arch->PortNSS.Pin    = 15;
            SPI_Arch->PortSCK.GPIOx  = stm32_GPIOB;
            SPI_Arch->PortSCK.Pin    = 3;
            SPI_Arch->PortMISO.GPIOx = stm32_GPIOB;
            SPI_Arch->PortMISO.Pin   = 4;
            SPI_Arch->PortMOSI.GPIOx = stm32_GPIOB;
            SPI_Arch->PortMOSI.Pin   = 5;
          */
            SPI_Arch->PortNSS   = tgp_a15;
            SPI_Arch->PortSCK   = tgp_b3;
            SPI_Arch->PortMISO  = tgp_b4;
            SPI_Arch->PortMOSI  = tgp_b5;
        }
        break;
    }
    default: { return tse_DeviceNotFound; }
    }
    switch (SPI_Index) { // if board has defined the NSS-pin: use it
#ifdef TTC_SPI1_NSS
    case 1:
        SPI_Arch->PortNSS = TTC_SPI1_NSS;

        break;
#endif
#ifdef TTC_SPI2_NSS
    case 2:
        SPI_Arch->PortNSS = TTC_SPI2_NSS;

        break;
#endif
#ifdef TTC_SPI3_NSS
    case 3:
        SPI_Arch->PortNSS = TTC_SPI3_NSS;

        break;
#endif
    default:
        break;
    }


    return Layout;
}
void _stm32_spi_send_single_word(stm32_spi_architecture_t* SPI_Arch, u16_t Word) {
    Assert_SPI(SPI_Arch != NULL, ec_NULL);
    register_stm32f1xx_spi_t* MySPI = SPI_Arch->Base;
    Assert_SPI(MySPI != NULL, ec_NULL);

    if (0) { // use StdPeripherals Library
        while (SPI_I2S_GetFlagStatus( (SPI_TypeDef*) MySPI , SPI_I2S_FLAG_TXE) == RESET);
        if ( (MySPI->CR1.SSM) && (SPI_Arch->PortNSS) ) {
            gpio_stm32f1xx_clr( &(SPI_Arch->PortNSS) );
            SPI_I2S_SendData( (SPI_TypeDef*) MySPI , Word);
            while (SPI_I2S_GetFlagStatus( (SPI_TypeDef*) MySPI , SPI_I2S_FLAG_TXE) == RESET);
            while (SPI_I2S_GetFlagStatus( (SPI_TypeDef*) MySPI , SPI_I2S_FLAG_BSY) != RESET);
            gpio_stm32f1xx_set( &(SPI_Arch->PortNSS) );
        }
        else
            SPI_I2S_SendData( (SPI_TypeDef*) MySPI, Word);
    }
    else { // direct register access (faster)
        SPI_SR_t* SR = &(MySPI->SR);
        while (!SR->TXE);
        if ( (MySPI->CR1.SSM) && (SPI_Arch->PortNSS) ) {
            gpio_stm32f1xx_clr( &(SPI_Arch->PortNSS) );
            MySPI->DR.Data = Word;
            while (!SR->TXE);
            while (SR->BSY);
            gpio_stm32f1xx_set( &(SPI_Arch->PortNSS) );
        }
        else
            MySPI->DR.Data = Word;
    }
}
ttc_spi_errorcode_e _stm32_spi_wait_for_RXNE(stm32_spi_architecture_t* SPI_Arch, u32_t TimeOut) {
    Assert_SPI(SPI_Arch != NULL, ec_NULL);
    register_stm32f1xx_spi_t* MySPI = (register_stm32f1xx_spi_t*) SPI_Arch->Base;
    Assert_SPI(MySPI != NULL, ec_NULL);

    if (TimeOut > 0) { // wait until word received or timeout
        TimeOut += ttc_task_get_elapsed_usecs();
        while (MySPI->SR.RXNE == 0) {
            if (TimeOut < ttc_task_get_elapsed_usecs())
                return tse_TimeOut;
            if (0) ttc_task_yield();
        }
    }
    else {             // wait until word received (no timeout)
        while (MySPI->SR.RXNE == 0);
            //ttc_task_yield();
    }

    return tse_OK;
}

//} private functions
