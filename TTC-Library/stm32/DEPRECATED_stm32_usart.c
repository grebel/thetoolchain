/*{ stm32_usart::.c -------------------------------------------------------------
 
                      The ToolChain
                      
   Universal Synchronous Asynchronous Receiver and Transmitters (stm32_USARTs)
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Gregor Rebel 2012
   
   Note: See ttc_usart.h for description of architecture independent USART implementation.
 
}*/

#include "DEPRECATED_stm32_usart.h"

// stores pointers to structures of initialized USARTs/ UARTs
// Note: index is not real, but logical USART-index as defined by makefile!
//static stm32_usart_architecture_t* stm32_USARTs[TTC_AMOUNT_USARTS];

// register base addresses of all USARTs
register_stm32f1xx_usart_t* const USART_Bases[5] = { (register_stm32f1xx_usart_t*) USART1,
                                  (register_stm32f1xx_usart_t*) USART2,
                                  (register_stm32f1xx_usart_t*) USART3,
                                  (register_stm32f1xx_usart_t*) UART4,
                                  (register_stm32f1xx_usart_t*) UART5
                                };

// these are defined by ttc_usart.c
extern void _ttc_usart_rx_isr(ttc_usart_config_t* USART_Generic, u8_t Byte);
extern void _ttc_usart_tx_isr(ttc_usart_config_t* USART_Generic, void* USART_Hardware);

//{ low-level driver interface (mandatory functions) ----------------------------

ttc_usart_errorcode_e stm32_usart_get_defaults(u8_t PhysicalIndex, ttc_usart_config_t* USART_Generic) {
//X     if (!su_Initialized) stm32_usart_reset_all();
    // assumption: *USART_Generic has been zeroed

    Assert_USART(USART_Generic != NULL, ec_NULL);
    if (PhysicalIndex >= TTC_USART_MAX_AMOUNT) return tue_DeviceNotFound;
    USART_Generic->USART_Arch.PhysicalIndex = PhysicalIndex;

    USART_Generic->Flags.All=0;
    USART_Generic->Flags.Bits.SendBreaks     = 1;
    USART_Generic->Flags.Bits.ControlParity  = 1;
    USART_Generic->Flags.Bits.Receive        = 1;
    USART_Generic->Flags.Bits.Transmit       = 1;
    USART_Generic->Flags.Bits.TransmitParity = 1;

    /*{ remaining bits 
    USART_Generic->Flags.Bits.LIN            =0;
    USART_Generic->Flags.Bits.IrDA           =0;
    USART_Generic->Flags.Bits.IrDALowPower   =0;
    USART_Generic->Flags.Bits.SmartCardNACK  =0;
    USART_Generic->Flags.Bits.RxDMA          =0;
    USART_Generic->Flags.Bits.TxDMA          =0;
    USART_Generic->Flags.Bits.Rts            =0;
    USART_Generic->Flags.Bits.IrqOnCts       =0;
    USART_Generic->Flags.Bits.IrqOnError     =0;
    USART_Generic->Flags.Bits.IrqOnIdle      =0;
    USART_Generic->Flags.Bits.IrqOnTxComplete=0;
    USART_Generic->Flags.Bits.IrqOnParityError= 0;
    USART_Generic->Flags.Bits.ParityEven     =0;
    USART_Generic->Flags.Bits.ParityOdd      =0;

    USART_Generic->Flags.Bits.RtsCts         =0;
    USART_Generic->Flags.Bits.SingleWire     =0;
    USART_Generic->Flags.Bits.SmartCard      =0;
    USART_Generic->Flags.Bits.Synchronous    =0;
    }*/

    USART_Generic->WordLength     = 8;
    USART_Generic->HalfStopBits   = 2;
    USART_Generic->Layout         = 0;
    USART_Generic->BaudRate       = 115200;
    USART_Generic->TimeOut        = 200000;

    return tue_OK;
}
ttc_usart_errorcode_e stm32_usart_get_features(u8_t PhysicalIndex, ttc_usart_config_t* USART_Generic) {
//X     if (!su_Initialized) stm32_usart_reset_all();
    // assumption: *USART_Generic has been zeroed

    Assert_USART(USART_Generic != NULL, ec_NULL);
    if (PhysicalIndex >= TTC_USART_MAX_AMOUNT) return tue_DeviceNotFound;
    USART_Generic->USART_Arch.PhysicalIndex = PhysicalIndex;

    USART_Generic->Flags.All=0;
    USART_Generic->Flags.Bits.Receive          = 1;
    USART_Generic->Flags.Bits.Transmit         = 1;
    USART_Generic->Flags.Bits.TransmitParity   = 1;
    USART_Generic->Flags.Bits.DelayedTransmits = 1;
    USART_Generic->Flags.Bits.SendBreaks       = 1;
    USART_Generic->Flags.Bits.ParityEven       = 1;
    USART_Generic->Flags.Bits.ParityOdd        = 1;
    USART_Generic->Flags.Bits.IrqOnRxNE        = 1;

    /*{ remaining bits
    USART_Generic->Flags.Bits.ControlParity  =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.RtsCts         =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.SingleWire     =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.SmartCard      =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.Synchronous    =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.LIN            =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.IrDA           =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.IrDALowPower   =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.SmartCardNACK  =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.RxDMA          =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.TxDMA          =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.Rts            =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.IrqOnCts       =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.IrqOnError     =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.IrqOnIdle      =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.IrqOnTxComplete=0; // (ToDo: implement)
    USART_Generic->Flags.Bits.IrqOnParityError= 0; // (ToDo: implement)
    }*/

    USART_Generic->WordLength      = 9;
    USART_Generic->HalfStopBits    = 4;
    USART_Generic->BaudRate        = 115200;
    USART_Generic->TimeOut         = -1;        // max positive value

    USART_TypeDef* USART_Base = NULL;
    if (1) { // find indexed USART on current board
      switch (PhysicalIndex) {                // find corresponding USART as defined by makefile.100_board_*
        case 0: USART_Base = USART1; break;
        case 1: USART_Base = USART2; break;
        case 2: USART_Base = USART3; break;
        case 3: USART_Base = UART4; break;
        case 4: USART_Base = UART5; break;
        default: Assert_USART(0, ec_UNKNOWN); break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
        }
    }

    switch ( (u32_t) USART_Base) { // find amount of available remapping-layouts (-> RM0008 p. 176)
      case (u32_t) USART1:
          USART_Generic->Layout=1;     // USART1 and USART2 provide 1 alternate pin layout
          break;
      case (u32_t) USART2:
          if (UC_PACKAGE_PINS >= 100)
              USART_Generic->Layout=1; // USART1 and USART2 provide 1 alternate pin layout
          else
              USART_Generic->Layout=0; // USART1 and USART2 provide 0 alternate pin layout
          break;
      case (u32_t) USART3:
          if (UC_PACKAGE_PINS >= 100)
              USART_Generic->Layout=2; // USART3 provides 2 alternate pin layout
          else if (UC_PACKAGE_PINS >= 64)
              USART_Generic->Layout=1; // USART3 provides 1 alternate pin layout
          else
              USART_Generic->Layout=0; // USART3 provides 0 alternate pin layout
          break;
      case (u32_t) UART4:
      case (u32_t) UART5:
          // USART4 and USART5 lack some features (-> ST RM0038 Table 138 on p. 651)
          USART_Generic->Flags.Bits.Rts            =0;
          USART_Generic->Flags.Bits.ControlParity  =0;
          USART_Generic->Flags.Bits.RtsCts         =0;
          USART_Generic->Flags.Bits.SmartCard      =0;
          USART_Generic->Flags.Bits.SmartCardNACK  =0;
          USART_Generic->Flags.Bits.Synchronous    =0;
          USART_Generic->Flags.Bits.IrqOnCts       =0;
          USART_Generic->Layout                    =0; // no alternate pin layout
          
          break;
      default: Assert_USART(0, ec_UNKNOWN); break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
    }
        
    return tue_OK;
}
ttc_usart_errorcode_e stm32_usart_init(ttc_usart_config_t* USART_Generic) {
    //X if (!su_Initialized) stm32_usart_reset_all();
    // assumption: *USART_Arch has been zeroed

    u8_t LogicalIndex = USART_Generic->LogicalIndex;
    Assert(LogicalIndex > 0, ec_InvalidArgument); // logical index starts at 1
    Assert_USART(USART_Generic != NULL, ec_NULL);
    if (LogicalIndex > TTC_AMOUNT_USARTS)
        return tue_DeviceNotFound;
    if ( (USART_Generic->WordLength   < 8) || (USART_Generic->WordLength   > 9) )
        return tue_InvalidWordSize;
    if ( (USART_Generic->HalfStopBits < 1) || (USART_Generic->HalfStopBits > 4) )
        return tue_InvalidStopBits;

    ttc_usart_architecture_t* USART_Arch = &(USART_Generic->USART_Arch);
    if (1) {                                   // validate USART_Features
        ttc_usart_config_t USART_Features;
        ttc_usart_errorcode_e Error=stm32_usart_get_features(LogicalIndex, &USART_Features);
        if (Error) return Error;
        USART_Generic->Flags.All &= USART_Features.Flags.All; // mask out unavailable flags
        if (USART_Generic->Layout > USART_Features.Layout)
            USART_Generic->Layout=USART_Features.Layout;
    }
    if (1) {                                   // find indexed USART on current board
        switch (USART_Arch->PhysicalIndex) {                // find corresponding USART as defined by makefile.100_board_*
        case 0: USART_Arch->Base = (register_stm32f1xx_usart_t*) USART1; break;
        case 1: USART_Arch->Base = (register_stm32f1xx_usart_t*) USART2; break;
        case 2: USART_Arch->Base = (register_stm32f1xx_usart_t*) USART3; break;
        case 3: USART_Arch->Base = (register_stm32f1xx_usart_t*) UART4;  break;
        case 4: USART_Arch->Base = (register_stm32f1xx_usart_t*) UART5;  break;
        default: Assert_USART(0, ec_UNKNOWN); break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
        }
    }
    if (1) {                                   // determine pin layout
 //#if defined(STM32F10X_LD) || defined(STM32F10X_LD_VL) || defined(STM32F10X_MD) || defined(STM32F10X_MD_VL) || defined(STM32F10X_HD) || defined(STM32F10X_HD_VL) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
        ttc_gpio_pin_e PortTx;
        switch (LogicalIndex) { // check if board has defined the TX-pin of USART to init
#ifdef TTC_USART1_TX
        case 1:
            PortTx = TTC_USART1_TX;
            break;
#endif
#ifdef TTC_USART2_TX
        case 2:
            PortTx = TTC_USART2_TX;
            break;
#endif
#ifdef TTC_USART3_TX
        case 3:
            PortTx = TTC_USART3_TX;
            break;
#endif
#ifdef TTC_USART4_TX
        case 4:
            PortTx = TTC_USART4_TX;
            break;
#endif
#ifdef TTC_USART5_TX
        case 5:
            PortTx = TTC_USART5_TX;
            break;
#endif
        default: Assert_USART(0, ec_UNKNOWN); break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
        }

        switch ( (u32_t) USART_Arch->Base ) { // determine pin layout
        case (u32_t) USART1: { // USART1
            if (PortTx == tgp_b6) USART_Generic->Layout=1;
            else                           USART_Generic->Layout=0;

            if (USART_Generic->Layout) { // alternate pin remapping (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
                USART_Arch->PortTxD = tgp_b6;
                USART_Arch->PortRxD = tgp_b7;
            }
            else {                       // no pin remapping
                USART_Arch->PortTxD = tgp_a9;
                USART_Arch->PortRxD = tgp_a10;
            }

            USART_Arch->PortCTS = tgp_a11;
            USART_Arch->PortRTS = tgp_a12;
            USART_Arch->PortCLK = tgp_a8;
            break;
        }
        case (u32_t) USART2: { // USART2
            if (PortTx == tgp_d5) USART_Generic->Layout=1;
            else                           USART_Generic->Layout=0;

            if (USART_Generic->Layout) { // alternate pin remapping (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
                USART_Arch->PortTxD = tgp_d5;
                USART_Arch->PortRxD = tgp_d6;
                USART_Arch->PortCTS = tgp_d3;
                USART_Arch->PortRTS = tgp_d4;
                USART_Arch->PortCLK = tgp_d7;
            }
            else {                       // no pin remapping
                USART_Arch->PortTxD = tgp_a2;
                USART_Arch->PortRxD = tgp_a3;
                USART_Arch->PortCTS = tgp_a0;
                USART_Arch->PortRTS = tgp_a1;
                USART_Arch->PortCLK = tgp_a4;
            }
            break;
        }
        case (u32_t) USART3: { // USART3
            if (PortTx == tgp_c10)       USART_Generic->Layout=1; // partial pin remapping
            else if (PortTx == tgp_d8)   USART_Generic->Layout=2; // full pin remapping
            else                                  USART_Generic->Layout=0; // no pin remapping

            switch (USART_Generic->Layout) { // (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
            case 1: {  // partial pin remapping
                USART_Arch->PortTxD = tgp_c10;
                USART_Arch->PortRxD = tgp_c11;
                USART_Arch->PortCTS = tgp_b13;
                USART_Arch->PortRTS = tgp_b14;
                USART_Arch->PortCLK = tgp_c12;
                break;
            }
            case 2: {  // full pin remapping
                USART_Arch->PortTxD = tgp_d8;
                USART_Arch->PortRxD = tgp_d9;
                USART_Arch->PortCTS = tgp_d11;
                USART_Arch->PortRTS = tgp_d12;
                USART_Arch->PortCLK = tgp_d10;
                break;
            }
            default: { // no pin remapping
                USART_Arch->PortTxD = tgp_b10;
                USART_Arch->PortRxD = tgp_b11;
                USART_Arch->PortCTS = tgp_b13;
                USART_Arch->PortRTS = tgp_b14;
                USART_Arch->PortCLK = tgp_b12;
                break;
            }
            }
            break;
        }
        case (u32_t) UART4:  { // UART4
            USART_Arch->PortTxD = tgp_c10;
            USART_Arch->PortRxD = tgp_c11;
            break;
        }
        case (u32_t) UART5:  { // UART5
            USART_Arch->PortTxD = tgp_c12;
            USART_Arch->PortRxD = tgp_d2;
            break;
        }
        default: { Assert_USART(0, tue_DeviceNotFound); }
        }
    }
    if (1) {                                   // compare chosen pin layout with configured pins
        ttc_gpio_pin_e Port;
        switch (LogicalIndex) { // check if board has defined the TX-pin of USART to init
          
          case 1: { // check configuration of USART #1
  #ifdef TTC_USART1_TX
              Port = TTC_USART1_TX;
              Assert_USART(Port == USART_Arch->PortTxD, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortTxD = tgp_none;
  #endif
  #ifdef TTC_USART1_RX
              Port = TTC_USART1_RX;
              Assert_USART(Port == USART_Arch->PortRxD, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortRxD = tgp_none;
  #endif
  #ifdef TTC_USART1_RTS
              Port = TTC_USART1_RTS;
              Assert_USART(Port == USART_Arch->PortRTS, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortRTS = tgp_none;
  #endif
  #ifdef TTC_USART1_CTS
              Port = TTC_USART1_CTS;
              Assert_USART(Port == USART_Arch->PortCTS, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortCTS = tgp_none;
  #endif
  #ifdef TTC_USART1_CK
              Port = TTC_USART1_CK;
              Assert_USART(Port == USART_Arch->PortCLK, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortCLK = tgp_none;
  #endif
              break;
          }
          case 2: { // check configuration of USART #2
  #ifdef TTC_USART2_TX
              Port = TTC_USART2_TX;
              Assert_USART(Port == USART_Arch->PortTxD, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortTxD = tgp_none;
  #endif
  #ifdef TTC_USART2_RX
              Port = TTC_USART2_RX;
              Assert_USART(Port == USART_Arch->PortRxD, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortRxD = tgp_none;
  #endif
  #ifdef TTC_USART2_RTS
              Port = TTC_USART2_RTS;
              Assert_USART(Port == USART_Arch->PortRTS, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortRTS = tgp_none;
  #endif
  #ifdef TTC_USART2_CTS
              Port = TTC_USART2_CTS;
              Assert_USART(Port == USART_Arch->PortCTS, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortCTS = tgp_none;
  #endif
  #ifdef TTC_USART2_CK
              Port = TTC_USART2_CK;
              Assert_USART(Port == USART_Arch->PortCLK, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortCLK = tgp_none;
  #endif
              break;
          }
          case 3: { // check configuration of USART #3
  #ifdef TTC_USART3_TX
              Port = TTC_USART3_TX;
              Assert_USART(Port == USART_Arch->PortTxD, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortTxD = tgp_none;
  #endif
  #ifdef TTC_USART3_RX
              Port = TTC_USART3_RX;
              Assert_USART(Port == USART_Arch->PortRxD, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortRxD = tgp_none;
  #endif
  #ifdef TTC_USART3_RTS
              Port = TTC_USART3_RTS;
              Assert_USART(Port == USART_Arch->PortRTS, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortRTS = tgp_none;
  #endif
  #ifdef TTC_USART3_CTS
              Port = TTC_USART3_CTS;
              Assert_USART(Port == USART_Arch->PortCTS, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortCTS = tgp_none;
  #endif
  #ifdef TTC_USART3_CK
              Port = TTC_USART3_CK;
              Assert_USART(Port == USART_Arch->PortCLK, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
#else
              USART_Arch->PortCLK = tgp_none;
  #endif
              break;
          }
          case 4: { // check configuration of USART #4
  #ifdef TTC_USART4_TX
              Port = TTC_USART4_TX;
              Assert_USART(Port == USART_Arch->PortTxD, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortTxD = tgp_none;
  #endif
  #ifdef TTC_USART4_RX
              Port = TTC_USART4_RX;
              Assert_USART(Port == USART_Arch->PortRxD, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortRxD = tgp_none;
  #endif
              break;
          }
          case 5: { // check configuration of USART #5
  #ifdef TTC_USART5_TX
              Port = TTC_USART5_TX;
              Assert_USART(Port == USART_Arch->PortTxD, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortTxD = tgp_none;
  #endif
  #ifdef TTC_USART5_RX
              Port = TTC_USART5_RX;
              Assert_USART(Port == USART_Arch->PortRxD, ec_InvalidArgument); // ERROR: configured GPIO cannot be used in current remapping layout!
  #else
              USART_Arch->PortRxD = tgp_none;
  #endif
              break;
          }
        default: Assert_USART(0, ec_UNKNOWN); break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
            
        }
    }
    if (1) {                                   // activate clocks
        switch ( (u32_t) USART_Arch->Base ) {
        case (u32_t) USART1: {
            if (USART_Generic->Layout) { // GPIOB
                RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
            }
            else {                       // GPIOA
                RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);
            }
            break;
        }
        case (u32_t) USART2: {
            if (USART_Generic->Layout) { // GPIOD
                RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2 , ENABLE);
                RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE);
            }
            else {                       // GPIOA
                RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2 , ENABLE);
                RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);
            }
            break;
        }
        case (u32_t) USART3: {
            switch (USART_Generic->Layout) {
            case 1: // GPPIOB, GPIOC
                RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
                RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO, ENABLE);
                break;
            case 2: // GPPIOD
                RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
                RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE);
                break;
            default: // GPPIOB
                RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
                RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
                break;
            }
            break;
        }
        case (u32_t) UART4:  { 
            RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4 , ENABLE);
            RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE);
            break;
        }
        case (u32_t) UART5:  { 
            RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5 , ENABLE);
            RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE);
            break;
        }
        default: Assert_USART(0, ec_UNKNOWN); break; // should never occur!
        }
    }
    USART_DeInit( (USART_TypeDef*) USART_Arch->Base);
    if (1) {                                   // configure GPIOs
        if (USART_Arch->PortTxD)  ttc_gpio_init(USART_Arch->PortTxD, tgm_alternate_function_push_pull,       tgs_Max);
        if (USART_Arch->PortRxD)  ttc_gpio_init(USART_Arch->PortRxD, tgm_input_floating,                     tgs_Max);
        if (USART_Arch->PortRTS)  ttc_gpio_init(USART_Arch->PortRTS, tgm_alternate_function_push_pull,       tgs_Max);
        if (USART_Arch->PortCLK)  ttc_gpio_init(USART_Arch->PortCLK, tgm_alternate_function_push_pull,       tgs_Max);
        if (USART_Arch->PortCTS)  ttc_gpio_init(USART_Arch->PortCTS, tgm_input_floating,                     tgs_Max);
    }
    if (1) {                                   // apply remap layout

        switch ( (u32_t) USART_Arch->Base ) {

        case (u32_t) USART1: { // USART1
            if (USART_Generic->Layout)
                GPIO_PinRemapConfig(GPIO_Remap_USART1, ENABLE);
            else
                GPIO_PinRemapConfig(GPIO_Remap_USART1, DISABLE);

            break;
        }
        case (u32_t) USART2: { // USART2
            if (USART_Generic->Layout)
                GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);
            else
                GPIO_PinRemapConfig(GPIO_Remap_USART2, DISABLE);
            break;
        }
        case (u32_t) USART3: { // USART3
            switch (USART_Generic->Layout) { // (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
            case 1:
                GPIO_PinRemapConfig(GPIO_PartialRemap_USART3, ENABLE);
                break;
            case 2:
                GPIO_PinRemapConfig(GPIO_FullRemap_USART3, ENABLE);
                break;
            default:  // no pin remapping
                GPIO_PinRemapConfig(GPIO_PartialRemap_USART3 | GPIO_FullRemap_USART3, DISABLE);
                break;
            }
            break;
        }

        default: { break; }
        }

    }
    if (1) {                                   // init USART

        USART_InitTypeDef USART_MyInit;
        USART_MyInit.USART_BaudRate=USART_Generic->BaudRate;

        USART_MyInit.USART_HardwareFlowControl= USART_HardwareFlowControl_None;
        if (USART_Generic->Flags.Bits.Rts)     USART_MyInit.USART_HardwareFlowControl= USART_HardwareFlowControl_RTS;
        if (USART_Generic->Flags.Bits.Cts)     USART_MyInit.USART_HardwareFlowControl= USART_HardwareFlowControl_CTS;
        if (USART_Generic->Flags.Bits.RtsCts)  USART_MyInit.USART_HardwareFlowControl= USART_HardwareFlowControl_RTS_CTS;

        USART_MyInit.USART_Mode= 0;
        if (USART_Generic->Flags.Bits.Transmit) USART_MyInit.USART_Mode |= USART_Mode_Tx;
        if (USART_Generic->Flags.Bits.Receive)  USART_MyInit.USART_Mode |= USART_Mode_Rx;

        USART_MyInit.USART_Parity=USART_Parity_No;
        if (USART_Generic->Flags.Bits.ParityEven) USART_MyInit.USART_Parity=USART_Parity_Even;
        if (USART_Generic->Flags.Bits.ParityOdd)  USART_MyInit.USART_Parity=USART_Parity_Odd;

        switch (USART_Generic->HalfStopBits) {
        case 1: USART_MyInit.USART_StopBits  =USART_StopBits_0_5; break;
        case 3: USART_MyInit.USART_StopBits  =USART_StopBits_1_5; break;
        case 4: USART_MyInit.USART_StopBits  =USART_StopBits_2;   break;
        default:
        case 2: USART_MyInit.USART_StopBits  =USART_StopBits_1;   break;
        }

        switch (USART_Generic->WordLength) {
        case 9: USART_MyInit.USART_WordLength=USART_WordLength_9b; break;

        default:
        case 8: USART_MyInit.USART_WordLength=USART_WordLength_8b; break;
        }

        // translate USART_MyInit back to USART_Generic
        switch (USART_MyInit.USART_HardwareFlowControl) {
        case USART_HardwareFlowControl_RTS:
            USART_Generic->Flags.Bits.RtsCts=0;
            USART_Generic->Flags.Bits.Rts   =1;
            USART_Generic->Flags.Bits.Cts   =0;
            break;
        case USART_HardwareFlowControl_CTS:
            USART_Generic->Flags.Bits.RtsCts=0;
            USART_Generic->Flags.Bits.Rts   =0;
            USART_Generic->Flags.Bits.Cts   =1;
            break;
        case USART_HardwareFlowControl_RTS_CTS:
            USART_Generic->Flags.Bits.RtsCts=1;
            USART_Generic->Flags.Bits.Rts   =1;
            USART_Generic->Flags.Bits.Cts   =1;
            break;
        default:
            USART_Generic->Flags.Bits.RtsCts=0;
            USART_Generic->Flags.Bits.Rts   =0;
            USART_Generic->Flags.Bits.Cts   =0;
            break;
        }
        switch (USART_MyInit.USART_Parity) {
        case USART_Parity_Even:
            USART_Generic->Flags.Bits.ParityEven=1;
            USART_Generic->Flags.Bits.ParityOdd =0;
            break;
        case USART_Parity_Odd:
            USART_Generic->Flags.Bits.ParityEven=0;
            USART_Generic->Flags.Bits.ParityOdd =1;
            break;
        default:
            USART_Generic->Flags.Bits.ParityEven=0;
            USART_Generic->Flags.Bits.ParityOdd =0;
            break;
        }
        switch (USART_MyInit.USART_StopBits) {
        case USART_StopBits_0_5:  USART_Generic->HalfStopBits=1; break;
        case USART_StopBits_1_5:  USART_Generic->HalfStopBits=3; break;
        case USART_StopBits_2:    USART_Generic->HalfStopBits=4; break;
        default:
        case USART_StopBits_1:    USART_Generic->HalfStopBits=2; break;
        }
        switch (USART_MyInit.USART_WordLength) {
        case USART_WordLength_9b:
            USART_Generic->WordLength=9; break;
        default:
            USART_Generic->WordLength=8; break;
        }

        if (USART_MyInit.USART_Mode & USART_Mode_Tx) USART_Generic->Flags.Bits.Transmit=1;
        if (USART_MyInit.USART_Mode & USART_Mode_Rx) USART_Generic->Flags.Bits.Receive =1;

        USART_Init( (USART_TypeDef*) USART_Arch->Base, &USART_MyInit);

    }
    if (1) {                                   // update stm32_USARTs[]

        //X stm32_USARTs[LogicalIndex - 1] = USART_Arch;
        //X stm32_USARTs[LogicalIndex - 1]->Base = (register_stm32f1xx_usart_t*) USART_Arch->Base;
    }
    // no interrupt definition here -> ttc_usart.c

    USART_Cmd( (USART_TypeDef*) USART_Arch->Base, ENABLE);

    return tue_OK;
}
ttc_usart_errorcode_e stm32_usart_send_word_blocking(ttc_usart_config_t* USART_Generic, const u16_t Word) {
    Assert_USART(USART_Generic != NULL, ec_InvalidArgument);
    register_stm32f1xx_usart_t* MyUSART = USART_Generic->USART_Arch.Base;
    Assert_USART(MyUSART != NULL, ec_NULL);

    return _stm32_usart_send_word_blocking(USART_Generic->USART_Arch.Base, Word, USART_Generic->TimeOut);
}
ttc_usart_errorcode_e stm32_usart_read_byte_blocking(ttc_usart_config_t* USART_Generic, u8_t* Byte) {

    register_stm32f1xx_usart_t* MyUSART = USART_Generic->USART_Arch.Base;
    if ( _stm32_usart_wait_for_RXNE(MyUSART, USART_Generic->TimeOut) )
        return tue_TimeOut;

    *Byte= (u8_t) 0xff & ( (USART_TypeDef*) MyUSART )->DR;

    return tue_OK;
}
ttc_usart_errorcode_e stm32_usart_read_word_blocking(ttc_usart_config_t* USART_Generic, u16_t* Word) {

    register_stm32f1xx_usart_t* MyUSART = USART_Generic->USART_Arch.Base;
    if ( _stm32_usart_wait_for_RXNE(MyUSART, USART_Generic->TimeOut) )
        return tue_TimeOut;

    *Word = 0x1ff & ( (USART_TypeDef*) MyUSART )->DR;

    return tue_OK;
}
u8_t stm32_usart_get_physicalindex(ttc_usart_config_t* USART_Generic) {
    Assert_USART(USART_Generic != NULL, ec_InvalidArgument);

    return USART_Generic->USART_Arch.PhysicalIndex;
}

//} Function definitions
//{ private functions (ideally) -------------------------------------------------

ttc_usart_errorcode_e _stm32_usart_send_word_blocking(register_stm32f1xx_usart_t* MyUSART, const u16_t Word, Base_t TimeOut) {
    Assert_USART(MyUSART != NULL, ec_NULL);

    ttc_usart_errorcode_e  Error = _stm32_usart_wait_for_TXE(MyUSART, TimeOut);
    if (Error != tue_OK)
        return Error;

    _stm32_usart_send_byte_isr(MyUSART, Word);
    return tue_OK;
}
ttc_usart_errorcode_e _stm32_usart_wait_for_RXNE(register_stm32f1xx_usart_t* MyUSART, Base_t TimeOut) {

    if (TimeOut != (Base_t) -1) { // wait until word received or timeout
        while ( MyUSART->SR.RXNE == 0) {
            if (TimeOut-- == 0)  return tue_TimeOut;
            ttc_task_yield();
        }
    }
    else {             // wait until word received (no timeout)
        while (MyUSART->SR.RXNE == 0)
            ttc_task_yield();
    }

    return tue_OK;
}
ttc_usart_errorcode_e _stm32_usart_wait_for_TXE(register_stm32f1xx_usart_t* MyUSART, Base_t TimeOut) {

    if (TimeOut != (Base_t) -1) {
        //X while (USART_GetFlagStatus( (USART_TypeDef*) MyUSART, USART_FLAG_TXE) == RESET) {
        while ( MyUSART->SR.TXE == 0) {
            if (TimeOut-- == 0)  return tue_TimeOut;
            ttc_task_yield();
        }
    }
    else { // no timeout
        //X while (USART_GetFlagStatus( (USART_TypeDef*) MyUSART, USART_FLAG_TXE) == RESET) {
        while ( MyUSART->SR.TXE == 0) {
            ttc_task_yield();
        }
    }

    return tue_OK;
}
void _stm32_usart_rx_isr(physical_index_t PhysicalIndex, void* Argument) {
    Assert_USART(PhysicalIndex <= TTC_AMOUNT_USARTS, ec_InvalidArgument);
    register_stm32f1xx_usart_t* USART_Base = USART_Bases[PhysicalIndex];

    u8_t Byte = (u8_t) 0xff & ( (USART_TypeDef*) USART_Base)->DR;

    _ttc_usart_rx_isr((ttc_usart_config_t*) Argument, Byte); // provide received byte to registered function

    // clear interrupt status flag to allow next interrupt
    if (0)  // more readable
        USART_Base->SR.RXNE = 0;
    else    // faster
        ( (USART_TypeDef*) USART_Base)->SR &= 0xff - (1 << 5);
}
void _stm32_usart_tx_isr(physical_index_t PhysicalIndex, void* Argument) {
    Assert_USART(PhysicalIndex <= TTC_AMOUNT_USARTS, ec_InvalidArgument);
    register_stm32f1xx_usart_t* USART_Base = USART_Bases[PhysicalIndex];

    _ttc_usart_tx_isr( (ttc_usart_config_t*) Argument, USART_Base ); // provide received byte to registered function
}

//} private functions
