#ifndef STM32_USART_H
#define STM32_USART_H

/*{ stm32_usart.h **********************************************************
 
                      The ToolChain
                      
   Universal Synchronous Asynchronous Receiver and Transmitters (USARTs)
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Gregor Rebel 2012

 
}*/
//{ Defines/ TypeDefs ****************************************************

// maximum amount of available USART/ UART units in current architecture
#define TTC_USART_MAX_AMOUNT 5

//} Defines
//{ Includes *************************************************************

#include "../ttc_task.h"
#include "../ttc_interrupt.h"

 // Basic set of helper functions
#include "../gpio/gpio_stm32f1xx.h"
#include "../register/register_stm32f1xx.h"
#include "DEPRECATED_stm32_usart_types.h"

#define USE_STDPERIPH_DRIVER
#include "stm32f10x.h"
#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
#include "stm32f10x_conf.h"
#include "stm32f10x_usart.h"
#include "misc.h"
#include "../ttc_usart_types.h"  // requires definition of ttc_usart_architecture_t

//} Includes *************************************************************

//{ define macros for ttc_usart.h interface

#define _driver_ttc_usart_init(USART_Generic) stm32_usart_init(USART_Generic)

//}
//{ Function prototypes **************************************************

//X /* resets library. Automatically called.
//X */
//X void stm32_usart_reset_all();

/* fills out given USART_ with default values for indexed USART
 * @param PhysicalIndex   index of physical device instance (0 = USART1, 1 = USART2, ...)
 * @param USART_Generic   pointer to struct ttc_usart_config_t (structure must be allocated by user and has to stay in memory as long as the USART is in use!)
 * @return  == 0:         *USART_Generic has been initialized successfully; != 0: error-code
 */
ttc_usart_errorcode_e stm32_usart_get_defaults(u8_t PhysicalIndex, ttc_usart_config_t* USART_Generic);

/* fills out given USART_Generic with maximum valid values for indexed USART
 * @param PhysicalIndex   index of physical device instance (0 = USART1, 1 = USART2, ...)
 * @param USART_Generic   pointer to struct ttc_usart_config_t (structure must be allocated by user and has to stay in memory as long as the USART is in use!)
 * @return  == 0:         *USART_Generic has been initialized successfully; != 0: error-code
 */
ttc_usart_errorcode_e stm32_usart_get_features(u8_t PhysicalIndex, ttc_usart_config_t* USART_Generic);

/* initializes single USART
 * @param USART_Generic   filled out struct ttc_usart_config_t                        (Note: referenced struct must stay in memory as long as device is in use!)
 * @return  == 0:         USART has been initialized successfully; != 0: error-code
 */
ttc_usart_errorcode_e stm32_usart_init(ttc_usart_config_t* USART_Generic);

/* Send out given data word (8 or 9 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param USART_Generic  struct ttc_usart_config_t initialized by _driver_ttc_usart_init()
 * @param Byte           8 or 9 bit value to send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_usart_errorcode_e stm32_usart_send_word_blocking(ttc_usart_config_t* USART_Generic, const u16_t Word);

/* Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param USART_Generic  struct ttc_usart_config_t initialized by _driver_ttc_usart_init()
 * @param Word           pointer to 16 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
ttc_usart_errorcode_e stm32_usart_read_word_blocking(ttc_usart_config_t* USART_Generic, u16_t* Word);

/* Reads single data byte from input buffer.
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param USART_Generic  struct ttc_usart_config_t initialized by _driver_ttc_usart_init()
 * @param Word           pointer to 16 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
ttc_usart_errorcode_e stm32_usart_read_byte_blocking(ttc_usart_config_t* USART_Generic, u8_t* Byte);

/* translates from logical index of functional unit to real hardware index (0=USART1, 1=USART2, ...)
 * Logical indices are defined by board makefiles.
 * E.g. #define TTC_USART1=USART3   declares third real hardware unit as first logical unit
 *
 * @param USART_Generic  struct ttc_usart_config_t initialized by _driver_ttc_usart_init()
 * @return               real device index of USART to init (0..MAX)
 */
u8_t stm32_usart_get_physicalindex(ttc_usart_config_t* USART_Generic);

/* Sends out given byte.
 * @param MyUSART        USART device to use
 * @param Word           pointer to 16 bit buffer where to store Word
 * @param TimeOut        >0: max task-switches to wait; ==0: return immediately; ==-1: no timeout
 *
 * Note: This low-level function should not be called from outside!
 */
ttc_usart_errorcode_e _stm32_usart_send_word_blocking(register_stm32f1xx_usart_t* MyUSART, const u16_t Word, Base_t TimeOut);

/* low level send routine especially to be called from _ttc_usart_tx_isr()
 *
 * @param MyUSART        USART device to use
 * @param Word           data word to send (8 or 9 bits)
 */
void _stm32_usart_send_byte_isr(register_stm32f1xx_usart_t* MyUSART, const u16_t Word);
#define _stm32_usart_send_byte_isr(MyUSART, Word) *( (u16_t*) &(MyUSART->DR) ) = Word

/* Wait until word has been received or timeout occured.
 * Note: This low-level function is private and should not be called from outside!
 *
 * @param MyUSART        USART device to use
 * @param TimeOut        >0: max task-switches to wait; ==0: return immediately; ==-1: no timeout
 */
ttc_usart_errorcode_e _stm32_usart_wait_for_RXNE(register_stm32f1xx_usart_t* MyUSART, Base_t TimeOut);

/* wait until transmit buffer is empty
 * Note: This low-level function is private and should not be called from outside!
 *
 * @param MyUSART        USART device to use
 * @param TimeOut        >0: max task-switches to wait; ==0: return immediately; ==-1: no timeout
 */
ttc_usart_errorcode_e _stm32_usart_wait_for_TXE(register_stm32f1xx_usart_t* MyUSART, Base_t TimeOut);


/* general interrupt handler for received byte; reads received byte and pass it to receiveSingleByte()
 * @param PhysicalIndex    0..TTC_AMOUNT_USARTS-1 - USART device to use (0=USART1, ...)
 * @param USART_Generic    pointer to configuration of corresponding USART
 */
void _stm32_usart_rx_isr(physical_index_t PhysicalIndex, void* Argument);

/* general interrupt handler for that will call _ttc_usart_tx_isr() to provide next byte to send
 * @param PhysicalIndex    0..TTC_AMOUNT_USARTS-1 - USART device to use (0=USART1, ...)
 * @param USART_Generic    pointer to configuration of corresponding USART
 */
void _stm32_usart_tx_isr(physical_index_t PhysicalIndex, void* Argument);

//} Function prototypes

#endif //STM32_USART_H
