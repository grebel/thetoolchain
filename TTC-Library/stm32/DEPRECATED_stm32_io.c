/*{ stm32_io.c ***********************************************
 
 * Written by Gregor Rebel 2010-2017
 *
 * Basic set of helper functions.
 * 
}*/
#error "stm32_io.c - This file is DEPRECATED: the functions here are about to be moved into other files!"
#include "stm32_io.h"

     t_u16* arraySet(t_u16* Array, t_u16 Value, t_u32 Amount) {                // fill 16-bit array with given value                                      
    t_u16* Writer16=Array;
    
    while (Amount-- > 0) {
      *Writer16++=Value;
    }
    
    return Array;
}
      void* myMalloc(t_base Size) {                                            // allocates zeroed memory
    
    void* Buffer=pvPortMalloc(Size);
    Assert(Buffer != 0, ttc_assert_origin_auto);
    memset(Buffer, 0, Size);
    
    return Buffer;
}
      t_s16 sLimit(t_s16 Value, t_s16 Min, t_s16 Max) {                        // sLimits a value to given minimum and maximum 
  if (Value < Min) Value=Min;
  if (Value > Max) Value=Max;
  
  return Value;
}
      t_u16 toggleValue(t_u16 Value, t_u16 Max, t_u16 Min) {                   // returns Max if Value tends to be Min, Min otherwise
    if (Value == Max)  return Min;
    if (Value == Min)  return Max;
    
    // Value is neither Min nor Max: see to which it tends to
    t_u16 Dist2Min=abs(Value - Min);
    t_u16 Dist2Max=abs(Max - Value);
    
    if (Dist2Min < Dist2Max)  return Max;
    return Min;
}
      t_u16 uLimit(t_u16 Value, t_u16 Min, t_u16 Max) {                        // sLimits a value to given minimum and maximum 
  if (Value < Min) Value=Min;
  if (Value > Max) Value=Max;
  
  return Value;
}
       bool circa(t_u32 A, t_u32 B, t_u8 Distance) {                           // checks if two values are nearly the same
/* checks if B is within [A-Distance..A+Distance]
 *
 * A         first value
 * B         second values
 * Distance  distance around A
 *
 * return: TRUE=B is within given distance around A.
 */
    t_u32 C=0xffffffff;
    if (C - Distance > A)  C=A + Distance;
    if (C < B)  return FALSE;  // B < A - Distance

    C=0;
    if (Distance < A)  C=A - Distance;
    if (C > B)  return FALSE;  // B > A + Distance
      
    return TRUE;               // A - Distance <= B <= A + Distance
}
