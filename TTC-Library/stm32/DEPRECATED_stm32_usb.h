#ifndef STM32_USB_H
#define STM32_USB_H

/*{ stm32_usb.h **********************************************************
 
                      The ToolChain
                      
   USB for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Sascha Poggemann 2012

 
}*/
//{ Includes *************************************************************

#include "../ttc_usb_types.h"

 // Basic set of helper functions
#include "../gpio/gpio_stm32f1xx.h"
#include "../register/register_stm32f1xx.h"
//#include "stm32_usb_types.h" //ToDo implement if needed

#define USE_STDPERIPH_DRIVER
#include "stm32f10x.h"
#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
#include "stm32f10x_conf.h"
#include "misc.h"

#include "../usb/serial_data_stream/inc/usb_conf.h"

#ifdef EXTENSION_400_stm32_usb_fs_device_lib
#ifdef TARGET_ARCHITECTURE_STM32F1xx
#include "hw_config.h"
#include "usb_mem.h"
#include "usb_lib.h"
#include "usb_desc.h"
#include "usb_pwr.h"
#endif //TARGET_ARCHITECTURE_STM32F1xx
#endif //EXTENSION_400_stm32_usb_fs_device_lib

#ifdef EXTENSION_400_stm32_usb_fs_host_lib
#ifdef TARGET_ARCHITECTURE_STM32F1xx
#include "usb_bsp.h"
#include "usbh_core.h"
#include "usbh_usr.h"
#include "usbh_hid_core.h"
#endif //TARGET_ARCHITECTURE_STM32F1xx
#endif //EXTENSION_400_stm32_usb_fs_host_lib

#include "../ttc_task.h"
#include "../ttc_interrupt.h"

//} Includes *************************************************************
//{ Defines/ TypeDefs ****************************************************

// maximum amount of available USB/ UART units in current architecture
#define TTC_USB_MAX_AMOUNT 5

//The size of the USB_TX_Buffer should equal the size the USB bus reports to the user
#define USB_TX_BUFFER_SIZE 64
//} Defines
//{ Function prototypes **************************************************

//X /* resets library. Automatically called.
//X */
//X void stm32_usb_reset_all();

/* fills out given USB_ with default values for indexed USB
 * @param PhysicalIndex   index of physical device instance (0 = USB1, 1 = USB2, ...)
 * @param USB_Generic   pointer to struct t_ttc_usb_generic (structure must be allocated by user and has to stay in memory as long as the USB is in use!)
 * @return  == 0:         *USB_Generic has been initialized successfully; != 0: error-code
 */
e_ttc_usb_errorcode stm32_usb_get_defaults(t_u8 PhysicalIndex, t_ttc_usb_generic* USB_Generic);

/* fills out given USB_Generic with maximum valid values for indexed USB
 * @param PhysicalIndex   index of physical device instance (0 = USB1, 1 = USB2, ...)
 * @param USB_Generic   pointer to struct t_ttc_usb_generic (structure must be allocated by user and has to stay in memory as long as the USB is in use!)
 * @return  == 0:         *USB_Generic has been initialized successfully; != 0: error-code
 */
e_ttc_usb_errorcode stm32_usb_get_features(t_u8 PhysicalIndex, t_ttc_usb_generic* USB_Generic);

/* initializes single USB
 * @param   USB_Generic   filled out struct t_ttc_usb_generic                        (Note: referenced struct must stay in memory as long as device is in use!)
 * @return  == 0:         USB has been initialized successfully; != 0: error-code
 */
e_ttc_usb_errorcode stm32_usb_init(t_ttc_usb_generic* USB_Generic);


/* initializes the USB interrupt
 * @param   PhysicalIndex   index of physical device instance (0 = USB1, 1 = USB2, ...)
 * @return  == 0:         USB interrupt has been initialized successfully; != 0: error-code
 */
e_ttc_usb_errorcode stm32_usb_interrupt_config(t_u8 PhysicalIndex);
/* Send out given raw data
 *
 * Note: USB must be initialized before!
 * Note: This function blocks until all bytes have been processed!
 *
 * @param USB_Generic    struct t_ttc_usb_generic initialized by stm32_usb_init()
 * @param Buffer         the Buffer to be send
 * @param Amount         the Amount of Bytes to be send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_usb_errorcode stm32_usb_send_raw(t_ttc_usb_generic * USB_Generic, t_u8* Buffer, t_base Amount);

/* Send out given data word (8 or 9 bits)
 *
 * Note: USB must be initialized before!
 * Note: This function is partially thread safe (use it for each USB from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param USB_Generic  struct t_ttc_usb_generic initialized by stm32_usb_init()
 * @param Byte           8 or 9 bit value to send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_usb_errorcode stm32_usb_send_word_blocking(t_ttc_usb_generic* USB_Generic, const t_u16 Word);

/* Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: USB must be initialized before!
 * Note: This function is partially thread safe (use it for each USB from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param USB_Generic  struct t_ttc_usb_generic initialized by stm32_usb_init()
 * @param Word           pointer to 16 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_usb_errorcode stm32_usb_read_word_blocking(t_ttc_usb_generic* USB_Generic, t_u16* Word);

/* Reads single data byte from input buffer.
 *
 * Note: USB must be initialized before!
 * Note: This function is partially thread safe (use it for each USB from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param USB_Generic  struct t_ttc_usb_generic initialized by stm32_usb_init()
 * @param Word           pointer to 16 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_usb_errorcode stm32_usb_read_byte_blocking(t_ttc_usb_generic* USB_Generic, t_u8* Byte);

/* translates from logical index of functional unit to real hardware index (0=USB1, 1=USB2, ...)
 * Logical indices are defined by board makefiles.
 * E.g. #define TTC_USB1=USB3   declares third real hardware unit as first logical unit
 *
 * @param USB_Generic  struct t_ttc_usb_generic initialized by stm32_usb_init()
 * @return               real device index of USB to init (0..MAX)
 */
t_u8 stm32_usb_get_physicalindex(t_ttc_usb_generic* USB_Generic);

/* Sends out given byte.
 * @param Myusb        usb device to use
 * @param Word           pointer to 16 bit buffer where to store Word
 * @param TimeOut        >0: max task-switches to wait; ==0: return immediately; ==-1: no timeout
 *
 * Note: This low-level function should not be called from outside!
 */
//e_ttc_usb_errorcode _stm32_usb_send_word_blocking(USB_t* MyUSB, const t_u16 Word, t_base TimeOut);

/* low level send routine especially to be called from _ttc_usb_tx_isr()
 *
 * @param MyUSB          USB device to use
 * @param Word           data word to send (8 or 9 bits)
 */
//void _stm32_usb_send_byte_isr(USB_t* MyUSB, const t_u16 Word);

/* Wait until word has been received or timeout occured.
 * Note: This low-level function is private and should not be called from outside!
 *
 * @param MyUSB          USB device to use
 * @param TimeOut        >0: max task-switches to wait; ==0: return immediately; ==-1: no timeout
 */
//e_ttc_usb_errorcode _stm32_usb_wait_for_RXNE(USB_t* MyUSB, t_base TimeOut);

/* wait until transmit buffer is empty
 * Note: This low-level function is private and should not be called from outside!
 *
 * @param MyUSB          USB device to use
 * @param TimeOut        >0: max task-switches to wait; ==0: return immediately; ==-1: no timeout
 */
//e_ttc_usb_errorcode _stm32_usb_wait_for_TXE(USB_t* MyUSB, t_base TimeOut);


/* callback function whenever there are bytes received by the usart
 * @param PhysicalIndex    0..TTC_AMOUNT_USBS-1 - USB device to use (0=USB1, ...)
 * @param USB_Generic    pointer to configuration of corresponding USB
 */
void _stm32_usb_data_rx(t_u8 * USB_RX_Buffer,t_u8 USB_Rx_Cnt);


/*******************************************************************************
* Function Name  : _stm32_usb_Handle_USBAsynchXfer.
* Description    : send data to USB.
* Input          : None.
* Return         : none.
*******************************************************************************/
void _stm32_usb_Handle_USBAsynchXfer (void);
/* general interrupt handler for received byte; reads received byte and pass it to receiveSingleByte()
 * @param PhysicalIndex    0..TTC_AMOUNT_USBS-1 - USB device to use (0=USB1, ...)
 * @param USB_Generic    pointer to configuration of corresponding USB
 */
void _stm32_usb_rx_isr(t_u8 PhysicalIndex, void* Argument);

/* general interrupt handler for that will call _ttc_usb_tx_isr() to provide next byte to send
 * @param PhysicalIndex    0..TTC_AMOUNT_USBS-1 - USB device to use (0=USB1, ...)
 * @param USB_Generic    pointer to configuration of corresponding USB
 */
void _stm32_usb_tx_isr(t_u8 PhysicalIndex, void* Argument);


/*******************************************************************************
* Function Name  : Handle_USBAsynchXfer.
* Description    : send data to USB.
* Input          : None.
* Return         : none.
*******************************************************************************/
void _stm32_usb_Handle_USBAsynchXfer (void);


//} Function prototypes

#endif //STM32_USB_H
