/*{ stm32_interrupt::.c -------------------------------------------------------------

                      The ToolChain
                      
   Interrupt configuration
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Gregor Rebel 2012
   
   Note: See ttc_interrupt.h for description of architecture independent INTERRUPT implementation.

}*/

#include "DEPRECATED_stm32_interrupt.h"

// provided by ttc_interrupt.c
#ifdef EXTENSION_500_ttc_usart
  ttc_array_dynamic_extern(ttc_usart_isrs_t*, ttc_usart_isrs);
#endif
#ifdef EXTENSION_500_ttc_gpio
ttc_array_dynamic_extern(ttc_gpio_isrs_t*,  ttc_gpio_isrs);
#endif
#ifdef EXTENSION_500_ttc_timer
ttc_array_dynamic_extern(ttc_timer_isrs_t*,  ttc_timer_isrs);
#endif

#ifdef EXTENSION_400_stm32_usb_fs_host_lib
    extern USB_OTG_CORE_HANDLE          USB_OTG_Core_dev;
    extern USBH_HOST                    USB_Host;
#endif
//{ Function definitions --------------------------------------------------------

#ifdef EXTENSION_500_ttc_usart  //{

ttc_interrupt_errorcode_e stm32_interrupt_init_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex) {
    (void) Type;
    (void) PhysicalIndex;

    // all done in ttc_interrupt.c
    return tine_OK;
}
void stm32_interrupt_enable_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ) {
    NVIC_InitTypeDef NVIC_Cfg;

    /* Enable and set USARTn Interrupt to the lowest priority */
    NVIC_Cfg.NVIC_IRQChannelPreemptionPriority= 0x0F;
    NVIC_Cfg.NVIC_IRQChannelSubPriority= 0x0F;
    NVIC_Cfg.NVIC_IRQChannelCmd=ENABLE;

    USART_TypeDef* USART_Base = NULL;

    switch (PhysicalIndex) { // physical index starts at 0

    case 0: USART_Base = USART1; NVIC_Cfg.NVIC_IRQChannel = USART1_IRQn; break;
    case 1: USART_Base = USART2; NVIC_Cfg.NVIC_IRQChannel = USART2_IRQn; break;
    case 2: USART_Base = USART3; NVIC_Cfg.NVIC_IRQChannel = USART3_IRQn; break;
#if defined(STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined(STM32F10X_XL) ||defined(STM32F10X_CL)
    case 3: USART_Base = UART4;  NVIC_Cfg.NVIC_IRQChannel =  UART4_IRQn; break;
#endif
#if defined(STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined(STM32F10X_XL) ||defined(STM32F10X_CL)
    case 4: USART_Base = UART5;  NVIC_Cfg.NVIC_IRQChannel =  UART5_IRQn; break;
#endif
    default: { Assert_Interrupt(0, ec_InvalidConfiguration); }
    }

#if 1==0
    // produces lots of warning in GCC 4.7 (ToDo: fix warnings)
    USART_CR1_t CR1; *( (u16_t*) &CR1 ) = (u16_t*) &(USART_Base->CR1);
    USART_CR2_t CR2; *( (u16_t*) &CR2 ) = (u16_t*) &(USART_Base->CR2);
    USART_CR3_t CR3; *( (u16_t*) &CR3 ) = (u16_t*) &(USART_Base->CR3);
#else
    USART_CR1_u CR1; CR1.U16 = USART_Base->CR1;
    USART_CR2_u CR2; CR2.U16 = USART_Base->CR2;
    USART_CR3_u CR3; CR3.U16 = USART_Base->CR3;
#endif

    switch (Type) { // set/ clear corresponding bits

    case tit_USART_Cts:
        if (EnableIRQ)
            Assert_Interrupt(A(ttc_usart_isrs, PhysicalIndex)->isr_ClearToSend != NULL, ec_InvalidConfiguration); // no isr configured for this interrupt!
        CR3.Fields.CTSIE  = EnableIRQ;
        break;
    case tit_USART_Idle:
        CR1.Fields.IDLEIE = EnableIRQ;
        if (EnableIRQ)
            Assert_Interrupt(A(ttc_usart_isrs, PhysicalIndex)->isr_IdleLine != NULL, ec_InvalidConfiguration); // no isr configured for this interrupt!
        break;
    case tit_USART_TxComplete:
        CR1.Fields.TCIE   = EnableIRQ;
        if (EnableIRQ)
            Assert_Interrupt(A(ttc_usart_isrs, PhysicalIndex)->isr_TransmissionComplete!= NULL, ec_InvalidConfiguration); // no isr configured for this interrupt!
        break;
    case tit_USART_TransmitDataEmpty:
        CR1.Fields.TXEIE  = EnableIRQ;
        if (EnableIRQ)
            Assert_Interrupt(A(ttc_usart_isrs, PhysicalIndex)->isr_TransmitDataEmpty!= NULL, ec_InvalidConfiguration); // no isr configured for this interrupt!
        break;
    case tit_USART_RxNE:
        CR1.Fields.RXNEIE = EnableIRQ;
        if (EnableIRQ)
            Assert_Interrupt(A(ttc_usart_isrs, PhysicalIndex)->isr_ReceiveDataNotEmpty!= NULL, ec_InvalidConfiguration); // no isr configured for this interrupt!
        break;
    case tit_USART_LinBreak:
        CR2.Fields.LBDIE  = EnableIRQ;
        if (EnableIRQ)
            Assert_Interrupt(A(ttc_usart_isrs, PhysicalIndex)->isr_LinBreakDetected!= NULL, ec_InvalidConfiguration); // no isr configured for this interrupt!
        break;
    case tit_USART_Error:
        if (EnableIRQ)
            Assert_Interrupt(A(ttc_usart_isrs, PhysicalIndex)->isr_Error!= NULL, ec_InvalidConfiguration); // no isr configured for this interrupt!
        CR1.Fields.PEIE   = EnableIRQ;  // irq on parity error
        CR3.Fields.EIE    = EnableIRQ;  // irq on general error
        break;
    default: break;
    }

    if (! EnableIRQ) { // disable interrupt channel only if no other interrupt event for this USART is active
        if ( (CR1.Fields.PEIE   == 0) &&
             (CR1.Fields.IDLEIE == 0) &&
             (CR1.Fields.TCIE   == 0) &&
             (CR1.Fields.TXEIE  == 0) &&
             (CR1.Fields.RXNEIE == 0) &&
             (CR2.Fields.LBDIE  == 0) &&
             (CR3.Fields.CTSIE  == 0) &&
             (CR3.Fields.EIE    == 0)
             )
            NVIC_Cfg.NVIC_IRQChannelCmd = DISABLE;
    }
    NVIC_Init(&NVIC_Cfg);

    // Note: We have to copy struct-data in this odd way because USART_Base->CR1.Fields.TXEIE = 1; gives unpredictable results!
    ((USART_TypeDef*) USART_Base)->CR1 = *( (u16_t*) &CR1 );
    ((USART_TypeDef*) USART_Base)->CR2 = *( (u16_t*) &CR2 );
    ((USART_TypeDef*) USART_Base)->CR3 = *( (u16_t*) &CR3 );
}

void USART_General_IRQHandler(physical_index_t PhysicalIndex, volatile register_stm32f1xx_usart_t* USART) {
    USART_SR_t StatusRegister = USART->SR;  // reading from SR and then from DR will reset flags in SR

    ttc_usart_isrs_t* ISRs = A(ttc_usart_isrs, PhysicalIndex);
    Assert_Interrupt(ISRs != NULL, ec_UNKNOWN); // should never happen: the usart indexed by PhysicalIndex has not been initialized!

    if (StatusRegister.RXNE && ISRs->isr_ReceiveDataNotEmpty) {
        ISRs->isr_ReceiveDataNotEmpty(PhysicalIndex, ISRs->Argument_ReceiveDataNotEmpty);
    }
    if (StatusRegister.TXE  && ISRs->isr_TransmitDataEmpty) {
        ISRs->isr_TransmitDataEmpty(PhysicalIndex, ISRs->Argument_TransmitDataEmpty);
    }
    if (StatusRegister.CTS  && ISRs->isr_ClearToSend) {
        ISRs->isr_ClearToSend(PhysicalIndex, ISRs->Argument_ClearToSend);
    }
    if (StatusRegister.TC   && ISRs->isr_TransmissionComplete) {
        ISRs->isr_TransmissionComplete(PhysicalIndex, ISRs->Argument_TransmissionComplete);
    }
    if (StatusRegister.IDLE && ISRs->isr_IdleLine) {
        ISRs->isr_IdleLine(PhysicalIndex, ISRs->Argument_IdleLine);
    }
    if (StatusRegister.LBD  && ISRs->isr_LinBreakDetected) {
        ISRs->isr_LinBreakDetected(PhysicalIndex, ISRs->Argument_LinBreakDetected);
    }

    if (  ( (*( (u8_t*) &StatusRegister )) & 0xf) &&  // error occured: gather data + call error handler
          (ISRs->isr_Error != NULL)                   // error-handler has been defined
          ) {
        ttc_interrupt_usart_errors_t Error; *((u8_t*) &Error) = 0;
        if (StatusRegister.FE)   Error.Framing = 1;
        if (StatusRegister.NE)   Error.Noise   = 1;
        if (StatusRegister.ORE)  Error.Overrun = 1;
        if (StatusRegister.PE)   Error.Parity  = 1;

        ISRs->isr_Error(PhysicalIndex, Error, ISRs->Argument_Error);
    }
}
void USART1_IRQHandler() {
    USART_General_IRQHandler(0, (register_stm32f1xx_usart_t*) USART1);
    USART1->SR = 0; // clear all interrupt flags
}
void USART2_IRQHandler() {
    USART_General_IRQHandler(1, (register_stm32f1xx_usart_t*) USART2);
    USART2->SR = 0; // clear all interrupt flags
}
void USART3_IRQHandler() {
    USART_General_IRQHandler(2, (register_stm32f1xx_usart_t*) USART3);
    USART3->SR = 0; // clear all interrupt flags
}
void UART4_IRQHandler() {
    USART_General_IRQHandler(3, (register_stm32f1xx_usart_t*) UART4);
    UART4->SR = 0; // clear all interrupt flags
}
void UART5_IRQHandler() {
    USART_General_IRQHandler(4, (register_stm32f1xx_usart_t*) UART5);
    UART5->SR = 0; // clear all interrupt flags
}

#endif //}EXTENSION_500_ttc_usart
#ifdef EXTENSION_500_ttc_gpio //{

ttc_interrupt_errorcode_e stm32_interrupt_init_gpio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex) {
    GPIO_TypeDef* GPIOx;
    u8_t Pin;
    interrupt_stm32f1xx_gpio_from_index(PhysicalIndex, (ttc_gpio_bank_t*) &GPIOx, &Pin);
    Assert_Interrupt(GPIOx,    ec_InvalidArgument);
    Assert_Interrupt(Pin < TTC_GPIO_MAX_PINS, ec_InvalidArgument);


    EXTI_InitTypeDef EXTI_Cfg;
    NVIC_InitTypeDef NVIC_Cfg;

    u8_t GPIO_PortSource = 0;
    u8_t GPIO_PinType    = Pin;

    EXTI_StructInit(&EXTI_Cfg);

    /* Enable AFIO clock */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

    switch ( (u32_t) GPIOx ) {
#ifdef GPIOA
    case (u32_t) GPIOA:
        GPIO_PortSource = GPIO_PortSourceGPIOA;
        break;
#endif
#ifdef GPIOB
    case (u32_t) GPIOB:
        GPIO_PortSource = GPIO_PortSourceGPIOB;
        break;
#endif
#ifdef GPIOC
    case (u32_t) GPIOC:
        GPIO_PortSource = GPIO_PortSourceGPIOC;
        break;
#endif
#ifdef GPIOD
    case (u32_t) GPIOD:
        GPIO_PortSource = GPIO_PortSourceGPIOD;
        break;
#endif
#ifdef GPIOE
    case (u32_t) GPIOE:
        GPIO_PortSource = GPIO_PortSourceGPIOE;
        break;
#endif
#ifdef GPIOF
    case (u32_t) GPIOF:
        GPIO_PortSource = GPIO_PortSourceGPIOF;
        break;
#endif
#ifdef GPIOG
    case (u32_t) GPIOG:
        GPIO_PortSource = GPIO_PortSourceGPIOG;
        break;
#endif
    default:
        Assert_Interrupt(0,ec_InvalidArgument);
    }

    EXTI_Cfg.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_Cfg.EXTI_Line = 1 << Pin;

    switch (Type) { // find memory location where to store function pointer + argument

    case tit_GPIO_Falling:
        EXTI_Cfg.EXTI_Trigger = EXTI_Trigger_Falling;
        break;

    case tit_GPIO_Rising:
        EXTI_Cfg.EXTI_Trigger = EXTI_Trigger_Rising;
        break;

    case tit_GPIO_Rising_Falling:
        EXTI_Cfg.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
        break;

    default:
        Assert_Interrupt(0,ec_InvalidArgument);
        break;
    }

    //configure the External interrupt/event line mapping
    GPIO_EXTILineConfig(GPIO_PortSource, GPIO_PinType);

    EXTI_Cfg.EXTI_LineCmd = DISABLE;
    EXTI_Init(&EXTI_Cfg);

    // 32-bit architecture allows fast constant arrays with direct pointer access
    static const u8_t stm32_Interrupt_IRQChannel[_driver_AMOUNT_EXTERNAL_INTERRUPTS] =
    { EXTI0_IRQn,
      EXTI1_IRQn,
      EXTI2_IRQn,
      EXTI3_IRQn,
      EXTI4_IRQn,
      EXTI9_5_IRQn,
      EXTI9_5_IRQn,
      EXTI9_5_IRQn,
      EXTI9_5_IRQn,
      EXTI9_5_IRQn,
      EXTI15_10_IRQn,
      EXTI15_10_IRQn,
      EXTI15_10_IRQn,
      EXTI15_10_IRQn,
      EXTI15_10_IRQn,
      EXTI15_10_IRQn
    };

    NVIC_Cfg.NVIC_IRQChannel                   = stm32_Interrupt_IRQChannel[Pin];
    NVIC_Cfg.NVIC_IRQChannelPreemptionPriority = 0x0F;
    NVIC_Cfg.NVIC_IRQChannelSubPriority        = 0x0F;
    NVIC_Cfg.NVIC_IRQChannelCmd                = ENABLE;
    NVIC_Init(&NVIC_Cfg);

    return tine_OK;
}
void stm32_interrupt_enable_gpio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ) {

    GPIO_TypeDef* GPIOx;
    u8_t Pin;
    interrupt_stm32f1xx_gpio_from_index(PhysicalIndex, (ttc_gpio_bank_t*) &GPIOx, &Pin);
#if (TTC_ASSERT_INTERRUPTS==1)
    Assert_Interrupt(GPIOx,    ec_InvalidArgument);
    Assert_Interrupt(Pin < _driver_AMOUNT_EXTERNAL_INTERRUPTS, ec_InvalidArgument);
    Assert_Interrupt(!ttc_array_is_null(ttc_gpio_isrs), ec_InvalidConfiguration); // init interrupt first!
    ttc_gpio_isrs_t* GPIO_ISR_Entry = A(ttc_gpio_isrs, Pin);
    Assert_Interrupt(GPIO_ISR_Entry, ec_InvalidConfiguration); // init interrupt first!
#endif

    static EXTI_InitTypeDef* EXTI_Cfg = NULL;
    if (EXTI_Cfg == NULL) { // first call: allocate structure
        EXTI_Cfg = ttc_memory_alloc_zeroed(sizeof(EXTI_InitTypeDef));
        EXTI_StructInit(EXTI_Cfg);
        EXTI_Cfg->EXTI_Mode = EXTI_Mode_Interrupt;
    }

    switch (Type) { // select trigger according to Type

    case tit_GPIO_Falling:
        EXTI_Cfg->EXTI_Trigger = EXTI_Trigger_Falling;
        break;

    case tit_GPIO_Rising:
        EXTI_Cfg->EXTI_Trigger = EXTI_Trigger_Rising;
        break;

    case tit_GPIO_Rising_Falling:
        EXTI_Cfg->EXTI_Trigger = EXTI_Trigger_Rising_Falling;
        break;

    default:
        Assert_Interrupt(0,ec_InvalidArgument);
        break;
    }

    EXTI_Cfg->EXTI_Line    = 1 << Pin;
    EXTI_Cfg->EXTI_LineCmd = EnableIRQ;

    EXTI_Init(EXTI_Cfg);
}
void GPIO_General_IRQHandler(physical_index_t InterruptLine) {
    ttc_gpio_isrs_t* Entry = A(ttc_gpio_isrs, InterruptLine);
    if (Entry->isr)
        Entry->isr(Entry->PhysicalIndex, Entry->Argument);
}
void EXTI0_IRQHandler() {
    GPIO_General_IRQHandler(0);
    EXTI_ClearITPendingBit(EXTI_Line0);
}
void EXTI1_IRQHandler() {
    GPIO_General_IRQHandler(1);
    EXTI_ClearITPendingBit(EXTI_Line1);
}
void EXTI2_IRQHandler() {
    GPIO_General_IRQHandler(2);
    EXTI_ClearITPendingBit(EXTI_Line2);
}
void EXTI3_IRQHandler() {
    GPIO_General_IRQHandler(3);
    EXTI_ClearITPendingBit(EXTI_Line3);
}
void EXTI4_IRQHandler() {
    GPIO_General_IRQHandler(4);
    EXTI_ClearITPendingBit(EXTI_Line4);
}
void EXTI9_5_IRQHandler() {

    if(EXTI_GetITStatus(EXTI_Line5)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(5);
        EXTI_ClearITPendingBit(EXTI_Line5);

    } else if(EXTI_GetITStatus(EXTI_Line6)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(6);
        EXTI_ClearITPendingBit(EXTI_Line6);

    } else if(EXTI_GetITStatus(EXTI_Line7)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(7);
        EXTI_ClearITPendingBit(EXTI_Line7);

    } else if(EXTI_GetITStatus(EXTI_Line8)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(8);
        EXTI_ClearITPendingBit(EXTI_Line8);

    } else if(EXTI_GetITStatus(EXTI_Line9)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(9);
        EXTI_ClearITPendingBit(EXTI_Line9);

    } else{
        Assert_Interrupt(0, ec_UNKNOWN);
    }
}
void EXTI15_10_IRQHandler() {
    if(EXTI_GetITStatus(EXTI_Line10)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(10);
        EXTI_ClearITPendingBit(EXTI_Line10);

    } else if(EXTI_GetITStatus(EXTI_Line11)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(11);
        EXTI_ClearITPendingBit(EXTI_Line11);

    } else if(EXTI_GetITStatus(EXTI_Line12)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(12);
        EXTI_ClearITPendingBit(EXTI_Line12);

    } else if(EXTI_GetITStatus(EXTI_Line13)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(13);
        EXTI_ClearITPendingBit(EXTI_Line13);

    } else if(EXTI_GetITStatus(EXTI_Line14)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(14);
        EXTI_ClearITPendingBit(EXTI_Line14);

    } else if(EXTI_GetITStatus(EXTI_Line15)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(15);
        EXTI_ClearITPendingBit(EXTI_Line15);

    } else{
        Assert_Interrupt(0, ec_UNKNOWN);
    }
}

#endif //}EXTENSION_500_ttc_gpio

#ifdef EXTENSION_500_ttc_timer //{
ttc_interrupt_errorcode_e stm32_interrupt_init_timer(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex) {
    NVIC_InitTypeDef NVIC_Cfg;
    TIM_TypeDef* TIMER_Base;
    (void)TIMER_Base;       //Avoid the warning

    switch (PhysicalIndex){ // physical index starts is the logical index
    case 1: TIMER_Base = TIM1;
        switch(Type){
        case tit_TIMER_Updating:
#  if defined (STM32F10X_LD_VL) || defined (STM32F10X_MD_VL) || defined (STM32F10X_HD_VL)
            NVIC_Cfg.NVIC_IRQChannel = TIM1_UP_TIM16_IRQn;
#  elif defined (STM32F10X_LD) || defined (STM32F10X_MD) || defined (STM32F10X_HD) || defined (STM32F10X_CL)
            NVIC_Cfg.NVIC_IRQChannel = TIM1_UP_IRQn;
#  else
            NVIC_Cfg.NVIC_IRQChannel = TIM1_UP_TIM10_IRQn;
#  endif
            break;
        case tit_TIMER_Break:
#  if defined (STM32F10X_LD_VL) || defined (STM32F10X_MD_VL) || defined (STM32F10X_HD_VL)
            NVIC_Cfg.NVIC_IRQChannel = TIM1_BRK_TIM15_IRQn;
#  elif defined (STM32F10X_LD) || defined (STM32F10X_MD) || defined (STM32F10X_HD) || defined (STM32F10X_CL)
            NVIC_Cfg.NVIC_IRQChannel = TIM1_BRK_IRQn;
#  else
            NVIC_Cfg.NVIC_IRQChannel = TIM1_BRK_TIM9_IRQn;
#  endif
            break;
        case tit_TIMER_CC1:
            NVIC_Cfg.NVIC_IRQChannel = TIM1_CC_IRQn;
            break;
        case tit_TIMER_CC2:
            NVIC_Cfg.NVIC_IRQChannel = TIM1_CC_IRQn;
            break;
        case tit_TIMER_CC3:
            NVIC_Cfg.NVIC_IRQChannel = TIM1_CC_IRQn;
            break;
        case tit_TIMER_CC4:
            NVIC_Cfg.NVIC_IRQChannel = TIM1_CC_IRQn;
            break;
        case tit_TIMER_Triggering:
#  if defined (STM32F10X_LD_VL) || defined (STM32F10X_MD_VL) || defined (STM32F10X_HD_VL)
            NVIC_Cfg.NVIC_IRQChannel = TIM1_TRG_COM_TIM17_IRQn;
#  elif defined (STM32F10X_LD) || defined (STM32F10X_MD) || defined (STM32F10X_HD) || defined (STM32F10X_CL)
            NVIC_Cfg.NVIC_IRQChannel = TIM1_TRG_COM_IRQn;
#  else
            NVIC_Cfg.NVIC_IRQChannel = TIM1_TRG_COM_TIM11_IRQn;
#  endif
            break;
        default: Assert_Interrupt(0, ec_InvalidConfiguration);
            break;
        }
        break;
    case 2: TIMER_Base = TIM2;
        NVIC_Cfg.NVIC_IRQChannel = TIM2_IRQn;
        break;
    case 3: TIMER_Base = TIM3;
        NVIC_Cfg.NVIC_IRQChannel = TIM3_IRQn;
        break;
#if defined(STM32F10X_MD) || defined(STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
    case 4: TIMER_Base = TIM4;
        NVIC_Cfg.NVIC_IRQChannel =  TIM4_IRQn;
        break;
#endif
#if defined(STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined(STM32F10X_XL) ||defined(STM32F10X_CL)
    case 5: TIMER_Base = TIM5;
        NVIC_Cfg.NVIC_IRQChannel =  TIM5_IRQn;
        break;
#endif
#if defined(STM32F10X_LD_VL) || defined(STM32F10X_MD_VL) || defined(STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
    case 6: TIMER_Base = TIM6;
#  if defined(STM32F10X_XL)
        NVIC_Cfg.NVIC_IRQChannel =  TIM6_IRQn;
#  else
        NVIC_Cfg.NVIC_IRQChannel =  54;//TIM6_DAC_IRQn;
#  endif
        break;
    case 7: TIMER_Base = TIM7;
        NVIC_Cfg.NVIC_IRQChannel =  TIM7_IRQn;
        break;
#endif
#if defined(STM32F10X_HD) || defined(STM32F10X_XL)
     case 8: TIMER_Base = TIM8;
        NVIC_Cfg.NVIC_IRQChannel =  TIM8_IRQn;
        break;
#endif
#if defined (STM32F10X_XL)
    case 9: TIMER_Base = TIM9;
        NVIC_Cfg.NVIC_IRQChannel =  TIM1_BRK_TIM9_IRQn;
        break;
    case 10: TIMER_Base = TIM10;
        NVIC_Cfg.NVIC_IRQChannel =  TIM1_UP_TIM10_IRQn;
        break;
    case 11: TIMER_Base = TIM11;
        NVIC_Cfg.NVIC_IRQChannel =  TIM1_TRG_COM_TIM11_IRQn;
        break;
#endif
#if defined (STM32F10X_HD_VL) || defined (STM32F10X_XL)
    case 12: TIMER_Base = TIM12;
#  if defined (STM32F10X_XL)
        NVIC_Cfg.NVIC_IRQChannel = TIM8_BRK_TIM12_IRQn;
#  else
        NVIC_Cfg.NVIC_IRQChannel =  TIM12_IRQn;
#  endif
        break;
    case 13: TIMER_Base = TIM13;
#  if defined (STM32F10X_XL)
        NVIC_Cfg.NVIC_IRQChannel = TIM8_UP_TIM13_IRQn;
#  else
        NVIC_Cfg.NVIC_IRQChannel =  TIM13_IRQn;
#  endif
        break;
    case 14: TIMER_Base = TIM14;
#  if defined (STM32F10X_XL)
        NVIC_Cfg.NVIC_IRQChannel = TIM8_TRG_COM_TIM14_IRQn;
#  else
        NVIC_Cfg.NVIC_IRQChannel =  TIM14_IRQn;
#  endif
        break;
#endif
#if defined (STM32F10X_HD_VL) || defined (STM32F10X_HD_VL) || defined (STM32F10X_LD_VL)
    case 15: TIMER_Base = TIM15;
        NVIC_Cfg.NVIC_IRQChannel =  TIM1_BRK_TIM15_IRQn;
        break;
    case 16: TIMER_Base = TIM16;
        NVIC_Cfg.NVIC_IRQChannel =  TIM1_UP_TIM16_IRQn;
        break;
    case 17: TIMER_Base = TIM17;
        NVIC_Cfg.NVIC_IRQChannel =  TIM1_TRG_COM_TIM17_IRQn;
        break;
#endif
    default: { Assert_Interrupt(0, ec_InvalidConfiguration); }
    }

    /* Enable and set TIMx Interrupt to the lowest priority */
    NVIC_Cfg.NVIC_IRQChannelPreemptionPriority= 0x0F;
    NVIC_Cfg.NVIC_IRQChannelSubPriority= 0x0F;
    NVIC_Cfg.NVIC_IRQChannelCmd=ENABLE;
    NVIC_Init(&NVIC_Cfg);
    return tine_OK;
}
void stm32_interrupt_enable_timer(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ) {
    Assert_Interrupt(Type, ec_InvalidArgument);
    Assert_Interrupt(PhysicalIndex < TTC_INTERRUPT_TIMER_AMOUNT, ec_InvalidArgument);

    NVIC_InitTypeDef NVIC_Cfg;
    TIM_TypeDef* TIMER_Base = NULL;

    switch (PhysicalIndex){
    case 1: TIMER_Base = (TIM_TypeDef*)TIM1;
        break;
    case 2: TIMER_Base = (TIM_TypeDef*)TIM2;
        break;
    case 3: TIMER_Base = (TIM_TypeDef*)TIM3;
        break;
#if defined(STM32F10X_MD) || defined(STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
    case 4: TIMER_Base = (TIM_TypeDef*)TIM4;
        break;
#endif
#if defined(STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined(STM32F10X_XL) ||defined(STM32F10X_CL)
    case 5: TIMER_Base = (TIM_TypeDef*)TIM5;
        break;
#endif
#if defined(STM32F10X_LD_VL) || defined(STM32F10X_MD_VL) || defined(STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
    case 6: TIMER_Base = (TIM_TypeDef*)TIM6;
        break;
    case 7: TIMER_Base = (TIM_TypeDef*)TIM7;
        break;
#endif
#if defined(STM32F10X_HD) || defined(STM32F10X_XL)
     case 8: TIMER_Base = (TIM_TypeDef*)TIM8;
        break;
#endif
#if defined (STM32F10X_XL)
    case 9: TIMER_Base = (TIM_TypeDef*)TIM9;
        break;
    case 10: TIMER_Base = (TIM_TypeDef*)TIM10;
        break;
    case 11: TIMER_Base = (TIM_TypeDef*)TIM11;
        break;
#endif
#if defined (STM32F10X_HD_VL) || defined (STM32F10X_XL)
    case 12: TIMER_Base = (TIM_TypeDef*)TIM12;
        break;
    case 13: TIMER_Base = (TIM_TypeDef*)TIM13;
        break;
    case 14: TIMER_Base = (TIM_TypeDef*)TIM14;
        break;
#endif
#if defined (STM32F10X_HD_VL) || defined (STM32F10X_HD_VL) || defined (STM32F10X_LD_VL)
    case 15: TIMER_Base = (TIM_TypeDef*)TIM15;
        break;
    case 16: TIMER_Base = (TIM_TypeDef*)TIM16;
        break;
    case 17: TIMER_Base = (TIM_TypeDef*)TIM17;
        break;
#endif
    default: Assert_Interrupt(0, ec_InvalidConfiguration);
        break;
    }

    TIMx_DIER_u EnableRegister;
    EnableRegister.U16 = (u16_t) TIMER_Base->DIER;

    if((TIMER_Base == TIM6) || (TIMER_Base == TIM7))
        TIMER_Base->DIER |= ((uint16_t)0x0001);
    else if((TIMER_Base == TIM9) || (TIMER_Base == TIM12) || (TIMER_Base == TIM15)){
        switch (Type) {
            case tit_TIMER_Updating:
            TIMER_Base->DIER |= ((uint16_t)0x0001);
            break;
        case tit_TIMER_CC1:
            TIMER_Base->DIER |= ((uint16_t)0x0002);
            break;
        case tit_TIMER_CC2:
            TIMER_Base->DIER |= ((uint16_t)0x0004);
            break;
        case tit_TIMER_Break:
            if(TIMER_Base == TIM15)
                TIMER_Base->DIER |= ((uint16_t)0x0080);
            break;
        default:
            Assert_Interrupt(0,ec_InvalidArgument);
            break;
        }
    }
    else if((TIMER_Base == TIM10) || (TIMER_Base == TIM11) || (TIMER_Base == TIM13) || (TIMER_Base == TIM14) || (TIMER_Base == TIM16) || (TIMER_Base == TIM17)){
        switch (Type) {
            case tit_TIMER_Updating:
            TIMER_Base->DIER |= ((uint16_t)0x0001);
            break;
        case tit_TIMER_CC1:
            TIMER_Base->DIER |= ((uint16_t)0x0002);
            break;
        default:
            Assert_Interrupt(0,ec_InvalidArgument);
            break;
        }
    }
    else{
        switch (Type) { // select trigger according to Type
        case tit_TIMER_Updating:
            TIMER_Base->DIER |= ((uint16_t)0x0001);
            break;
        case tit_TIMER_CC1:
            TIMER_Base->DIER |= ((uint16_t)0x0002);
            break;
        case tit_TIMER_CC2:
            TIMER_Base->DIER |= ((uint16_t)0x0004);
            break;
        case tit_TIMER_Triggering:
            TIMER_Base->DIER |= ((uint16_t)0x0040);
            break;
        case tit_TIMER_Break:
            if((TIMER_Base == TIM1) || (TIMER_Base == TIM8))
               TIMER_Base->DIER |= ((uint16_t)0x0080);
            break;
        default:
            Assert_Interrupt(0,ec_InvalidArgument);
            break;
        }
    }

    if (!EnableIRQ) { // disable interrupt channel only if no other interrupt event for this USART is active
         if ( (EnableRegister.Fields.UIE == 0)   &&
              (EnableRegister.Fields.CC1IE == 0) &&
              (EnableRegister.Fields.CC2IE == 0) &&
              (EnableRegister.Fields.CC3IE == 0) &&
              (EnableRegister.Fields.CC4IE == 0) &&
              (EnableRegister.Fields.COMIE == 0) &&
              (EnableRegister.Fields.TIE == 0)   &&
              (EnableRegister.Fields.BIE == 0)
              )
             NVIC_Cfg.NVIC_IRQChannelCmd = DISABLE;
         NVIC_Init(&NVIC_Cfg);
     }
}
void TIMER_General_IRQHandler(physical_index_t PhysicalIndex, TIM_TypeDef* TIMER) {
    Assert_Interrupt(TIMER != NULL, ec_InvalidArgument);
    TIMx_SR_u StatusRegister;
    StatusRegister.U16 = (u16_t)TIMER->SR;  // reading from SR and then from DR will reset flags in SR
    ttc_timer_isrs_t* ISRs = A(ttc_timer_isrs, PhysicalIndex);
    Assert_Interrupt(ISRs != NULL, ec_UNKNOWN); // should never happen: the usart indexed by PhysicalIndex has not been initialized!
    TIMER->CR1 &= (uint16_t)(~((uint16_t)TIM_CR1_CEN));

    if(StatusRegister.Fields.UIF)
       TIMER->SR = (uint16_t)~((uint16_t)0x0001);
    if(StatusRegister.Fields.CC1IF)
       TIMER->SR = (uint16_t)~((uint16_t)0x0002);
    if(StatusRegister.Fields.CC2IF)
       TIMER->SR = (uint16_t)~((uint16_t)0x0004);
    if(StatusRegister.Fields.CC3IF)
       TIMER->SR = (uint16_t)~((uint16_t)0x0008);
    if(StatusRegister.Fields.CC4IF)
       TIMER->SR = (uint16_t)~((uint16_t)0x0010);
    if(StatusRegister.Fields.TIF)
        TIMER->SR = (uint16_t)~((uint16_t)0x0040);
    if(StatusRegister.Fields.BIF)
        TIMER->SR = (uint16_t)~((uint16_t)0x0080);
    if(StatusRegister.Fields.CC1OF)
        TIMER->SR = (uint16_t)~((uint16_t)0x0200);
    if(StatusRegister.Fields.CC2OF)
        TIMER->SR = (uint16_t)~((uint16_t)0x0400);
    if(StatusRegister.Fields.CC3OF)
        TIMER->SR = (uint16_t)~((uint16_t)0x0800);
    if(StatusRegister.Fields.CC4OF)
        TIMER->SR = (uint16_t)~((uint16_t)0x1000);

    TIMER->CR1 |= TIM_CR1_CEN;
    ISRs->isr(ISRs->PhysicalIndex, ISRs->Argument);
}
void TIM1_UP_IRQHandler(){
    TIMER_General_IRQHandler(1, (TIM_TypeDef*) TIM1);
}
void TIM2_IRQHandler() {
    TIMER_General_IRQHandler(2, (TIM_TypeDef*) TIM2);
}
void TIM3_IRQHandler() {
    TIMER_General_IRQHandler(3, (TIM_TypeDef*) TIM3);
}
void TIM4_IRQHandler() {
    TIMER_General_IRQHandler(4, (TIM_TypeDef*) TIM4);
}
void TIM5_IRQHandler() {
    TIMER_General_IRQHandler(5, (TIM_TypeDef*) TIM5);
}
void TIM6_IRQHandler() {
    TIMER_General_IRQHandler(6, (TIM_TypeDef*) TIM6);
}
void TIM7_IRQHandler() {
    TIMER_General_IRQHandler(7, (TIM_TypeDef*) TIM7);
}
void TIM8_IRQHandler() {
    TIMER_General_IRQHandler(8, (TIM_TypeDef*) TIM8);
}
void TIM9_IRQHandler() {
    TIMER_General_IRQHandler(9, (TIM_TypeDef*) TIM9);
}
void TIM10_IRQHandler() {
    TIMER_General_IRQHandler(10, (TIM_TypeDef*) TIM10);
}
void TIM11_IRQHandler() {
    TIMER_General_IRQHandler(11, (TIM_TypeDef*) TIM11);
}
void TIM12_IRQHandler() {
    TIMER_General_IRQHandler(12, (TIM_TypeDef*) TIM12);
}
void TIM13_IRQHandler() {
    TIMER_General_IRQHandler(13, (TIM_TypeDef*) TIM13);
}
void TIM14_IRQHandler() {
    TIMER_General_IRQHandler(14, (TIM_TypeDef*) TIM14);
}
void TIM15_IRQHandler() {
    TIMER_General_IRQHandler(15, (TIM_TypeDef*) TIM15);
}
void TIM16_IRQHandler() {
    TIMER_General_IRQHandler(16, (TIM_TypeDef*) TIM16);
}
void TIM17_IRQHandler() {
    TIMER_General_IRQHandler(17, (TIM_TypeDef*) TIM17);
}
#endif //}EXTENSION_500_ttc_timer

//{ irq-handlers (supported) ---------------------------

void HardFault_Handler() {

    __asm__(".global HardFault_Handler");
    __asm__(".extern cm3_hard_fault_handler_c");
    __asm__("TST LR, #4");
    __asm__("ITE EQ");
    __asm__("MRSEQ R0, MSP");
    __asm__("MRSNE R0, PSP");
    __asm__("B cm3_hard_fault_handler_c");
}

//}irq-handlers (supported)
//{ irq-handlers (yet unsupported) ---------------------------

/* void Reset_Handler() {
  // write your own reset-handler here...


  Assert_Interrupt(0, ec_UNKNOWN);
} */
void NMI_Handler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void MemManage_Handler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void BusFault_Handler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void UsageFault_Handler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void SVC_Handler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void DebugMon_Handler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void PendSV_Handler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void SysTick_Handler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void WWDG_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void PVD_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void TAMPER_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void RTC_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void FLASH_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void RCC_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA1_Channel1_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA1_Channel2_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA1_Channel3_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA1_Channel4_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA1_Channel5_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA1_Channel6_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA1_Channel7_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void ADC1_2_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void CAN1_TX_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void CAN1_RX0_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void CAN1_RX1_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void CAN1_SCE_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void TIM1_BRK_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void TIM1_TRG_COM_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void TIM1_CC_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void I2C1_EV_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void I2C1_ER_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void I2C2_EV_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void I2C2_ER_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void SPI1_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void SPI2_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void RTCAlarm_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void OTG_FS_WKUP_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void SPI3_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA2_Channel1_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA2_Channel2_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA2_Channel3_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA2_Channel4_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA2_Channel5_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void ETH_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void ETH_WKUP_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void CAN2_TX_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void CAN2_RX0_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void CAN2_RX1_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}
void CAN2_SCE_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}


#ifdef USE_USB_OTG_FS
void OTG_FS_IRQHandler(void)
#else
void OTG_HS_IRQHandler(void)
#endif
{
#ifdef EXTENSION_400_stm32_usb_fs_device_lib
    STM32_PCD_OTG_ISR_Handler();
#elif EXTENSION_400_stm32_usb_fs_host_lib
    USBH_OTG_ISR_Handler(&USB_OTG_Core_dev);
#else
    Assert_Interrupt(0, ec_UNKNOWN);
#endif
}

//#ifdef STM32F10X_CL
//void OTG_FS_IRQHandler() {
//    // call your interrupt-handler right here, or else ...
//#ifdef EXTENSION_400_stm32_usb_fs_device_lib
//    STM32_PCD_OTG_ISR_Handler();
//#elif EXTENSION_400_stm32_usb_fs_host_lib
//    USBH_OTG_ISR_Handler(&USB_OTG_Core_dev);
//#else
//    Assert_Interrupt(0, ec_UNKNOWN);
//#endif
//}
//#endif
void BootRAM() {

    // call your interrupt-handler right here, or else ...


    Assert_Interrupt(0, ec_UNKNOWN);
}

//} irq-handlers (yet unsupported)

//} Function definitions
