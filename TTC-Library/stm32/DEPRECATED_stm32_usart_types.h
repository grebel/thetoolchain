#ifndef STM32_USART_TYPES_H
#define STM32_USART_TYPES_H

/*{ stm32_usart_types.h **********************************************************
 
                      The ToolChain
                      
   Universal Synchronous Asynchronous Receiver and Transmitters (USARTs)
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.

   Architecture specific structure definitions.

   written by Gregor Rebel 2012

 
}*/
//{ Defines/ TypeDefs ****************************************************

// maximum amount of available USART/ UART units in current architecture
#define TTC_USART_MAX_AMOUNT 5

//} Defines
//{ Includes *************************************************************

#include "../gpio/gpio_stm32f1xx.h"
#include "../register/register_stm32f1xx.h"

//} Includes *************************************************************
//{ Structures/ Enums ****************************************************


typedef struct {  // architecture specific configuration data of single USART 
  register_stm32f1xx_usart_t* Base;                // base address of USART registers to be used for this USART
  u8_t PhysicalIndex;           // index that directly corresponds to functional unit (0=USART1, 1=USART2, ...)
  //ttc_usart_config_t* Generic; // architecture independent configuration data of this USART
  ttc_gpio_pin_e PortTxD;         // port pin for TxD
  ttc_gpio_pin_e PortRxD;         // port pin for RxD
  ttc_gpio_pin_e PortRTS;         // port pin for RTS
  ttc_gpio_pin_e PortCTS;         // port pin for CTS
  ttc_gpio_pin_e PortCLK;         // port pin for CLK
} __attribute__((__packed__)) stm32_usart_architecture_t;

#define ttc_usart_architecture_t stm32_usart_architecture_t

//} Structures/ Enums

#endif //STM32_USART_TYPES_H
