/** { ttc_gyroscope.c ****************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for gyroscope devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported 
 *  architectures.
 *
 *  See corresponding header file for documentation.
 *
 *  Created from template ttc_device.c revision 27 at 20150121 10:09:22 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_gyroscope.h".
//
#include "ttc_gyroscope.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_GYROSCOPE_AMOUNT == 0
  #warning No GYROSCOPE devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of gyroscope devices.
 *
 */
 

// for each initialized device, a pointer to its generic and mpu6050 definitions is stored
A_define(t_ttc_gyroscope_config*, ttc_gyroscope_configs, TTC_GYROSCOPE_AMOUNT);

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8 ttc_gyroscope_get_max_index() {
    return TTC_GYROSCOPE_AMOUNT;
}
t_ttc_gyroscope_config* ttc_gyroscope_get_configuration(t_u8 LogicalIndex) {

    Assert_GYROSCOPE(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_gyroscope_config* Config = A(ttc_gyroscope_configs, LogicalIndex-1); // will assert if outside array bounds

    if (!Config) {
        Config = A(ttc_gyroscope_configs, LogicalIndex-1) = ttc_heap_alloc_zeroed(sizeof(t_ttc_gyroscope_config));
        ttc_gyroscope_reset(LogicalIndex);
    }

    return Config;
}
void ttc_gyroscope_deinit(t_u8 LogicalIndex) {
    Assert_GYROSCOPE(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_gyroscope_config* Config = ttc_gyroscope_get_configuration(LogicalIndex);

    e_ttc_gyroscope_errorcode Result = _driver_gyroscope_deinit(Config);
    if (Result == ec_gyroscope_OK)
      Config->Flags.Bits.Initialized = 0;
}
e_ttc_gyroscope_errorcode ttc_gyroscope_init(t_u8 LogicalIndex) {
    Assert_GYROSCOPE(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_gyroscope_config* Config = ttc_gyroscope_get_configuration(LogicalIndex);

    e_ttc_gyroscope_errorcode Result = _driver_gyroscope_init(Config);
    if (Result == ec_gyroscope_OK)
      Config->Flags.Bits.Initialized = 1;

    return Result;
}
e_ttc_gyroscope_errorcode ttc_gyroscope_load_defaults(t_u8 LogicalIndex) {
    Assert_GYROSCOPE(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_gyroscope_config* Config = ttc_gyroscope_get_configuration(LogicalIndex);

    if (Config->Flags.Bits.Initialized)
      ttc_gyroscope_deinit(LogicalIndex);

    ttc_memory_set( Config, 0, sizeof(t_ttc_gyroscope_config) );

    // load generic default values
    switch (LogicalIndex) { // load type of low-level driver for current architecture
#ifdef TTC_GYROSCOPE1_DRIVER
    case  1: Config->Architecture = TTC_GYROSCOPE1_DRIVER; break;
#endif
#ifdef TTC_GYROSCOPE2_DRIVER
    case  2: Config->Architecture = TTC_GYROSCOPE2_DRIVER; break;
#endif
#ifdef TTC_GYROSCOPE3_DRIVER
    case  3: Config->Architecture = TTC_GYROSCOPE3_DRIVER; break;
#endif
#ifdef TTC_GYROSCOPE4_DRIVER
    case  4: Config->Architecture = TTC_GYROSCOPE4_DRIVER; break;
#endif
#ifdef TTC_GYROSCOPE5_DRIVER
    case  5: Config->Architecture = TTC_GYROSCOPE5_DRIVER; break;
#endif
#ifdef TTC_GYROSCOPE6_DRIVER
    case  6: Config->Architecture = TTC_GYROSCOPE6_DRIVER; break;
#endif
#ifdef TTC_GYROSCOPE7_DRIVER
    case  7: Config->Architecture = TTC_GYROSCOPE7_DRIVER; break;
#endif
#ifdef TTC_GYROSCOPE8_DRIVER
    case  8: Config->Architecture = TTC_GYROSCOPE8_DRIVER; break;
#endif
#ifdef TTC_GYROSCOPE9_DRIVER
    case  9: Config->Architecture = TTC_GYROSCOPE9_DRIVER; break;
#endif
#ifdef TTC_GYROSCOPE10_DRIVER
    case 10: Config->Architecture = TTC_GYROSCOPE10_DRIVER; break;
#endif
    default: ttc_assert_halt_origin(ec_gyroscope_InvalidImplementation); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }
    Assert_GYROSCOPE( (Config->Architecture > ta_gyroscope_None) && (Config->Architecture < ta_gyroscope_unknown), ttc_assert_origin_auto); // architecture not set properly! Check makefile for TTC_GYROSCOPE<n>_DRIVER and compare with e_ttc_gyroscope_architecture

    Config->LogicalIndex = LogicalIndex;

    //Insert additional generic default values here ...

    return _driver_gyroscope_load_defaults(Config);
}
void ttc_gyroscope_prepare() {
  // add your startup code here (Singletasking!)
  _driver_gyroscope_prepare();
}
t_ttc_gyroscope_measures* ttc_gyroscope_read_measures(t_u8 LogicalIndex) {
    Assert_GYROSCOPE(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_gyroscope_config* Config = ttc_gyroscope_get_configuration(LogicalIndex);

    return _driver_gyroscope_read_measures(Config);
}
void ttc_gyroscope_reset(t_u8 LogicalIndex) {
    Assert_GYROSCOPE(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_gyroscope_config* Config = ttc_gyroscope_get_configuration(LogicalIndex);

    _driver_gyroscope_reset(Config);
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_gyroscope(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
