#ifndef packet_common_types_h
#define packet_common_types_h

/** { packet_common_types.h *********************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.h revision 11 at 20150928 23:22:11 UTC
 *
 *  Datatypes being used by all packet_* low-level drivers
 *
 *  Authors: gregor
}*/
/** { Includes *********************************************************
 *
 * Put all includes here that are required to include this header file.
 * Do NOT add includes required to compile packet_common_types.c here!
 */

#ifndef TTC_PACKET_TYPES_H
    #  error Do not include packet_common_types.h directly. Include ttc_packet_types.h instead!
#endif
#include "../ttc_basic_types.h"
#include "../ttc_packet_types.h" // will not include anything, because packet_common_types.h is always included from ttc_packet_types.h. This line helps IDE to "know" all early defines of ttc_packet_types.h.
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Constant Defines / Macros ****************************************
 *
 * As a convention, constant definitions are all UPPERCASED to distinguish
 * them from variables and functions.
 *
 * Examples:

 #define ABC 1
 #define calculate(A,B) (A+B)
 */

//}ConstantDefines
/** { Structure Declarations *******************************************
 *
 * This can be typedef struct or typedef enum declarations.
 * Always prefix structures and enums with filename to avoid double declarations!
 *
 * Examples:

   typedef struct s_packet_common_types_list {
      struct s_packet_common_types_list* Next;
      t_base Value;
   } t_packet_common_types_list;

   typedef enum {
     packet_common_types_None,
     packet_common_types_Type1
   } e_packet_common_types_types;

 */

typedef enum {   // e_ttc_packet_architecture    general packet types of all supported protocols
    ta_packet_None = 0,       // no architecture selected

    E_ttc_packet_type_802154,                 // all 802.15.4 packet types below (automatically added by ./create_DeviceDriver.pl)
    E_ttc_packet_type_802154_ack,             //                                        -> packet_802154_types.h:t_packet_802154_ack_000000

    // general mac header of all type of address modes
    E_ttc_packet_type_802154_MHR_000010,     // SrcAddr16+SrcPan16                     -> packet_802154_types.h:t_packet_802154_mhr_000010
    E_ttc_packet_type_802154_MHR_000011,     // SrcAddr64+SrcPan16                     -> packet_802154_types.h:t_packet_802154_mhr_000011
    E_ttc_packet_type_802154_MHR_001000,     // DstAddr16+DstPan16                     -> packet_802154_types.h:t_packet_802154_mhr_001000
    E_ttc_packet_type_802154_MHR_001010,     // DstAddr16+DstPan16+SrcAddr16+SrcPan16  -> packet_802154_types.h:t_packet_802154_mhr_001010
    E_ttc_packet_type_802154_MHR_001100,     // DstAddr64+DstPan16                     -> packet_802154_types.h:t_packet_802154_mhr_001100
    E_ttc_packet_type_802154_MHR_001110,     // DstAddr64+DstPan16+SrcAddr16+SrcPan16  -> packet_802154_types.h:t_packet_802154_mhr_001110
    E_ttc_packet_type_802154_MHR_001111,     // DstAddr64+DstPan16+SrcAddr64+SrcPan16  -> packet_802154_types.h:t_packet_802154_mhr_001111
    E_ttc_packet_type_802154_MHR_010010,     // SrcAddr16                              -> packet_802154_types.h:t_packet_802154_mhr_010010
    E_ttc_packet_type_802154_MHR_010011,     // SrcAddr64                              -> packet_802154_types.h:t_packet_802154_mhr_010011
    E_ttc_packet_type_802154_MHR_011100,     // DstAddr64+DstPan16                     -> packet_802154_types.h:t_packet_802154_mhr_011100
    E_ttc_packet_type_802154_MHR_011000,     // DstAddr16                              -> packet_802154_types.h:t_packet_802154_mhr_011000
    E_ttc_packet_type_802154_MHR_011010,     // DstAddr16+SrcAddr16                    -> packet_802154_types.h:t_packet_802154_mhr_011010
    E_ttc_packet_type_802154_MHR_011110,     // DstAddr64+SrcAddr16                    -> packet_802154_types.h:t_packet_802154_mhr_011110
    E_ttc_packet_type_802154_MHR_011111,     // DstAddr64+SrcAddr64                    -> packet_802154_types.h:t_packet_802154_mhr_011111

    // secured general mac header of all type of address modes
    E_ttc_packet_type_802154_MHR_100010,     // SrcAddr16+SrcPan16                     -> packet_802154_types.h:packet_802154_mhr_100010_t
    E_ttc_packet_type_802154_MHR_100011,     // SrcAddr64+SrcPan16                     -> packet_802154_types.h:packet_802154_mhr_100011_t
    E_ttc_packet_type_802154_MHR_101000,     // DstAddr16+DstPan16                     -> packet_802154_types.h:packet_802154_mhr_101000_t
    E_ttc_packet_type_802154_MHR_101010,     // DstAddr16+DstPan16+SrcAddr16+SrcPan16  -> packet_802154_types.h:packet_802154_mhr_101010_t
    E_ttc_packet_type_802154_MHR_101100,     // DstAddr64+DstPan16                     -> packet_802154_types.h:packet_802154_mhr_101100_t
    E_ttc_packet_type_802154_MHR_101110,     // DstAddr64+DstPan16+SrcAddr16+SrcPan16  -> packet_802154_types.h:packet_802154_mhr_101110_t
    E_ttc_packet_type_802154_MHR_101111,     // DstAddr64+DstPan16+SrcAddr64+SrcPan16  -> packet_802154_types.h:packet_802154_mhr_101111_t
    E_ttc_packet_type_802154_MHR_111100,     // DstAddr64+DstPan16                     -> packet_802154_types.h:packet_802154_mhr_111100_t
    E_ttc_packet_type_802154_MHR_111000,     // DstAddr16                              -> packet_802154_types.h:packet_802154_mhr_111000_t
    E_ttc_packet_type_802154_MHR_110010,     // SrcAddr16                              -> packet_802154_types.h:packet_802154_mhr_110010_t
    E_ttc_packet_type_802154_MHR_111010,     // DstAddr16+SrcAddr16                    -> packet_802154_types.h:packet_802154_mhr_111010_t
    E_ttc_packet_type_802154_MHR_111110,     // DstAddr64+SrcAddr16                    -> packet_802154_types.h:packet_802154_mhr_111110_t
    E_ttc_packet_type_802154_MHR_111111,     // DstAddr64+SrcAddr64                    -> packet_802154_types.h:packet_802154_mhr_111111_t

    E_ttc_packet_type_802154_mhr,            // all 802.15.4 mac header types above

    // data packets support all type of address modes
    E_ttc_packet_type_802154_Data_000010,     // SrcAddr16+SrcPan16                     -> packet_802154_types.h:t_packet_802154_data_000010
    E_ttc_packet_type_802154_Data_000011,     // SrcAddr64+SrcPan16                     -> packet_802154_types.h:t_packet_802154_data_000011
    E_ttc_packet_type_802154_Data_001000,     // DstAddr16+DstPan16                     -> packet_802154_types.h:t_packet_802154_data_001000
    E_ttc_packet_type_802154_Data_001010,     // DstAddr16+DstPan16+SrcAddr16+SrcPan16  -> packet_802154_types.h:t_packet_802154_data_001010
    E_ttc_packet_type_802154_Data_001100,     // DstAddr64+DstPan16                     -> packet_802154_types.h:t_packet_802154_data_001100
    E_ttc_packet_type_802154_Data_001110,     // DstAddr64+DstPan16+SrcAddr16+SrcPan16  -> packet_802154_types.h:t_packet_802154_data_001110
    E_ttc_packet_type_802154_Data_001111,     // DstAddr64+DstPan16+SrcAddr64+SrcPan16  -> packet_802154_types.h:t_packet_802154_data_001111
    E_ttc_packet_type_802154_Data_011100,     // DstAddr64+DstPan16                     -> packet_802154_types.h:t_packet_802154_data_011100
    E_ttc_packet_type_802154_Data_011000,     // DstAddr16                              -> packet_802154_types.h:t_packet_802154_data_011000
    E_ttc_packet_type_802154_Data_011010,     // DstAddr16+SrcAddr16                    -> packet_802154_types.h:t_packet_802154_data_011010
    E_ttc_packet_type_802154_Data_011110,     // DstAddr64+SrcAddr16                    -> packet_802154_types.h:t_packet_802154_data_011110
    E_ttc_packet_type_802154_Data_011111,     // DstAddr64+SrcAddr64                    -> packet_802154_types.h:t_packet_802154_data_011111

    // secured data mac header of all type of address modes
    E_ttc_packet_type_802154_Data_100010,     // SrcAddr16+SrcPan16                     -> packet_802154_types.h:packet_802154_data_100010_t
    E_ttc_packet_type_802154_Data_100011,     // SrcAddr64+SrcPan16                     -> packet_802154_types.h:packet_802154_data_100011_t
    E_ttc_packet_type_802154_Data_101000,     // DstAddr16+DstPan16                     -> packet_802154_types.h:packet_802154_data_101000_t
    E_ttc_packet_type_802154_Data_101010,     // DstAddr16+DstPan16+SrcAddr16+SrcPan16  -> packet_802154_types.h:packet_802154_data_101010_t
    E_ttc_packet_type_802154_Data_101100,     // DstAddr64+DstPan16                     -> packet_802154_types.h:packet_802154_data_101100_t
    E_ttc_packet_type_802154_Data_101110,     // DstAddr64+DstPan16                     -> packet_802154_types.h:packet_802154_data_101110_t
    E_ttc_packet_type_802154_Data_101111,     // DstAddr64+DstPan16+SrcAddr64+SrcPan16  -> packet_802154_types.h:packet_802154_data_101111_t
    E_ttc_packet_type_802154_Data_111000,     // DstAddr16                              -> packet_802154_types.h:packet_802154_data_111000_t
    E_ttc_packet_type_802154_Data_111010,     // DstAddr16+SrcAddr16                    -> packet_802154_types.h:packet_802154_data_111010_t
    E_ttc_packet_type_802154_Data_111100,     // DstAddr64+DstPan16                     -> packet_802154_types.h:packet_802154_data_111100_t
    E_ttc_packet_type_802154_Data_111110,     // DstAddr64+SrcAddr16+SrcPan16           -> packet_802154_types.h:packet_802154_data_111110_t
    E_ttc_packet_type_802154_Data_111111,     // DstAddr64+SrcAddr64                    -> packet_802154_types.h:packet_802154_data_111111_t

    E_ttc_packet_type_802154_data,            // all 802.15.4 data types above

    // command packets support all type of address modes
    E_ttc_packet_type_802154_Command_000010,  // SrcAddr16+SrcPan16                     -> packet_802154_types.h:t_packet_802154_command_000010
    E_ttc_packet_type_802154_Command_000011,  // SrcAddr64+SrcPan16                     -> packet_802154_types.h:t_packet_802154_command_000011
    E_ttc_packet_type_802154_Command_001000,  // DstAddr16+DstPan16                     -> packet_802154_types.h:t_packet_802154_command_001000
    E_ttc_packet_type_802154_Command_001010,  // DstAddr16+DstPan16+SrcAddr16+SrcPan16  -> packet_802154_types.h:t_packet_802154_command_001010
    E_ttc_packet_type_802154_Command_001100,  // DstAddr64+DstPan16                     -> packet_802154_types.h:t_packet_802154_command_001100
    E_ttc_packet_type_802154_Command_001110,  // DstAddr64+DstPan16+SrcAddr16+SrcPan16  -> packet_802154_types.h:t_packet_802154_command_001110
    E_ttc_packet_type_802154_Command_001111,  // DstAddr64+DstPan16+SrcAddr64+SrcPan16  -> packet_802154_types.h:t_packet_802154_command_001111
    E_ttc_packet_type_802154_Command_011000,  // DstAddr16                              -> packet_802154_types.h:t_packet_802154_command_011000
    E_ttc_packet_type_802154_Command_011100,  // DstAddr64                              -> packet_802154_types.h:t_packet_802154_command_011100
    E_ttc_packet_type_802154_Command_011010,  // DstAddr16+SrcAddr16                    -> packet_802154_types.h:t_packet_802154_command_011010
    E_ttc_packet_type_802154_Command_011110,  // DstAddr64+SrcAddr16                    -> packet_802154_types.h:t_packet_802154_command_011110
    E_ttc_packet_type_802154_Command_011111,  // DstAddr64+SrcAddr64                    -> packet_802154_types.h:t_packet_802154_command_011111

    // secured command mac header of all type of address modes
    E_ttc_packet_type_802154_Command_100010,  // SrcAddr16+SrcPan16                     -> packet_802154_types.h:packet_802154_command_100010_t
    E_ttc_packet_type_802154_Command_100011,  // SrcAddr64+SrcPan16                     -> packet_802154_types.h:packet_802154_command_100011_t
    E_ttc_packet_type_802154_Command_101000,  // DstAddr16+DstPan16                     -> packet_802154_types.h:packet_802154_command_101000_t
    E_ttc_packet_type_802154_Command_101010,  // DstAddr16+DstPan16+SrcAddr16+SrcPan16  -> packet_802154_types.h:packet_802154_command_101010_t
    E_ttc_packet_type_802154_Command_101100,  // DstAddr64+DstPan16                     -> packet_802154_types.h:packet_802154_command_101100_t
    E_ttc_packet_type_802154_Command_101110,  // DstAddr64+DstPan16                     -> packet_802154_types.h:packet_802154_command_101110_t
    E_ttc_packet_type_802154_Command_101111,  // DstAddr64+DstPan16+SrcAddr64+SrcPan16  -> packet_802154_types.h:packet_802154_command_101111_t
    E_ttc_packet_type_802154_Command_111000,  // DstAddr16                              -> packet_802154_types.h:packet_802154_command_111000_t
    E_ttc_packet_type_802154_Command_111100,  // DstAddr64                              -> packet_802154_types.h:packet_802154_command_111100_t
    E_ttc_packet_type_802154_Command_111010,  // DstAddr16+SrcAddr16                    -> packet_802154_types.h:packet_802154_command_111010_t
    E_ttc_packet_type_802154_Command_111110,  // DstAddr64+SrcAddr16+SrcPan16           -> packet_802154_types.h:packet_802154_command_111110_t
    E_ttc_packet_type_802154_Command_111111,  // DstAddr64+SrcAddr64                    -> packet_802154_types.h:packet_802154_command_111111_t

    E_ttc_packet_type_802154_command,         // all 802.15.4 command types above

    // beacon packets support only two types of address modes
    E_ttc_packet_type_802154_Beacon_000010,   // SrcAddr16+PanID16                      -> packet_802154_types.h:t_packet_802154_beacon_000010
    E_ttc_packet_type_802154_Beacon_000011,   // SrcAddr64+PanID16                      -> packet_802154_types.h:t_packet_802154_beacon_000011

    // encrypted beacon packets support only two types of address modes
    E_ttc_packet_type_802154_Beacon_100010,   // SrcAddr16                              -> packet_802154_types.h:packet_802154_beacon_110010_t
    E_ttc_packet_type_802154_Beacon_100011,   // SrcAddr64                              -> packet_802154_types.h:packet_802154_beacon_110011_t
    E_ttc_packet_type_802154_beacon,          // all 802.15.4 beacon types above

    // additional 802.15.4 packet types to be added above
    E_ttc_packet_type_802154_unknown,         // all types below do not belong to IEEE802.15.4
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    E_ttc_packet_type_unknown        // architecture not supported
} e_ttc_packet_type;

typedef enum { // e_ttc_packet_address_format - type of address data stored in packet
    E_ttc_packet_address_None = 0,                   // address format undefined

    //
    // Binary encoding allows fast tests:
    // s = 16 bit source address
    // S = 64 bit source address
    // d = 16 bit destination address
    // D = 64 bit destination address
    // p = 16 bit source PanID
    // P = 16 bit destination PanID
    //
    // E.g.:
    // e_ttc_packet_address_format AddressFormat = E_ttc_packet_address_Source64_Target16_Targetpan16;
    // if (AddressFormat & E_ttc_packet_address_Target16) ...   // true
    // if (AddressFormat & E_ttc_packet_address_Source16) ... // false
    //
    //                                                          Bits: 0bsSdDpP
    E_ttc_packet_address_Source16                                   = 0b100000, // 16 bit source address
    E_ttc_packet_address_Target16                                   = 0b001000, // 16 bit destination address
    E_ttc_packet_address_Source16_Sourcepan16                       = 0b100010, // 16 bit source address + 16 bit source PanID
    E_ttc_packet_address_Source16_Target16                          = 0b101000, // 16 bit source address + 16 bit destination address
    E_ttc_packet_address_Source16_Targetpan16                       = 0b100001, // 16 bit source address + 16 bit destination PanID
    E_ttc_packet_address_Target16_Targetpan16                       = 0b001001, // 16 bit destination address + 16 bit destination PanID
    E_ttc_packet_address_Source16_Target16_Targetpan16              = 0b101001, // 16 bit source address + 16 bit destination address + 16 bit destination PanID

    // source 64 bit, dest 64 bit
    E_ttc_packet_address_Source64                                   = 0b010000, // 64 bit source address
    E_ttc_packet_address_Target64                                   = 0b000100, // 64 bit destination address
    E_ttc_packet_address_Source64_Target64                          = 0b010100, // 64 bit source address + 64 bit destination address
    E_ttc_packet_address_Source64_Target64_Targetpan16              = 0b010101, // 64 bit source address + 64 bit destination address + 64 bit destination PanID

    // source 16 bit + dest 64 bit
    E_ttc_packet_address_Source16_Target64                          = 0b100100, // 16 bit source address + 64 bit destination address
    E_ttc_packet_address_Target64_Targetpan16                       = 0b000101, // 64 bit destination address + 16 bit destination PanID
    E_ttc_packet_address_Source16_Target64_Targetpan16              = 0b100101, // 16 bit source address + 64 bit destination address + 16 bit destination PanID

    // source 64 bit + dest 16 bit
    E_ttc_packet_address_Source64_Target16                          = 0b011000, // 64 bit source address + 16 bit destination address
    E_ttc_packet_address_Source64_Targetpan16                       = 0b010001, // 64 bit source address + 16 bit destination PanID
    E_ttc_packet_address_Source64_Target16_Targetpan16              = 0b011001, // 64 bit source address + 16 bit destination address + 16 bit destination PanID

    // inter PAN types (SourcePan + DestPan)
    E_ttc_packet_address_Source64_Target16_Sourcepan16_Targetpan16  = 0b011011, // 64 bit source address + 16 bit destination address + 16 bit source PanID + 16 bit destination PanID
    E_ttc_packet_address_Source64_Target64_Sourcepan16_Targetpan16  = 0b010111, // 64 bit source address + 64 bit destination address + 16 bit source PanID + 16 bit destination PanID
    E_ttc_packet_address_Source16_Target64_Sourcepan16_Targetpan16  = 0b100111, // 16 bit source address + 64 bit destination address + 16 bit source PanID + 16 bit destination PanID
    E_ttc_packet_address_Source16_Target16_Sourcepan16_Targetpan16  = 0b101011, // 16 bit source address + 16 bit destination address + 16 bit source PanID + 16 bit destination PanID

    // Note: Place biggest enum value in last line above!
    E_ttc_packet_address_unknown                 // valid formats above
} e_ttc_packet_address_format;

typedef enum { // e_ttc_packet_category - packets can be categorized by their function in a radio network (as defined in IEEE802.15.4)
    E_ttc_packet_category_None,             // category undefined
    E_ttc_packet_category_Beacon,           // broadcast transfer 1:n
    E_ttc_packet_category_Data,             // data transfer 1:1 or 1:n
    E_ttc_packet_category_Command,          // network layer command (e.g. acquire new logical id, associate to coordinator, ...)
    E_ttc_packet_category_Acknowledgement,  // acknowledgement for received packet
    E_ttc_packet_category_unknown           // unknown/ unsupported categories below
} e_ttc_packet_category;

typedef enum { // e_ttc_packet_pattern - first payload byte can identifier to help delivery to corresponding protocol statemachine/ task
    /* If TTC_PACKET_SOCKET_BYTE==1 then the first payload byte is used to identify its origin.
     * If more than one software application is using the same communication channel, then this extra byte
     * ensures that each protocol statemachine processes only packets generated by corresponding
     * protocol statemachine on other nodes.
     *
     * Packet patterns are used to find received packets by different aspects:
     * - packet category type (Acknowledge, Beacon, Command, Data)
     * + packet socket (first byte od payload stores socket identifier)
     *   Some packet socket patterns directly identify a single socket identifier value.
     *   Other packet socket patterns match to a group of socket identifiers.
     */
    E_ttc_packet_pattern_socket_None,                    // no protocol specified

    E_ttc_packet_pattern_type_Begin,     // patterns below check type of packet instead of SocketID byte in payload ---------------------
    E_ttc_packet_pattern_type_Acknowledge,                 // matches packets of type acknowledge  (IEEE 802.15.4)
    E_ttc_packet_pattern_type_Beacon,                      // matches packets of type beacon       (IEEE 802.15.4)
    E_ttc_packet_pattern_type_Command,                     // matches packets of type command      (IEEE 802.15.4)
    E_ttc_packet_pattern_type_Data,                        // matches packets of type data         (IEEE 802.15.4)

    E_ttc_packet_pattern_type_End,                         // packet type patterns above

    E_ttc_packet_pattern_socket_Begin, // patterns below check SocketID byte (first byte of payload) ..................................

    E_ttc_packet_pattern_socket_Begin_Application,       // valid dynamic application protocols below
    // These entries are dynamically assigned by calling ttc_radio_socket_new()
    E_ttc_packet_pattern_socket_Application1,            // general application protocol #1
    E_ttc_packet_pattern_socket_Application2,            // general application protocol #2
    E_ttc_packet_pattern_socket_Application3,            // general application protocol #3
    E_ttc_packet_pattern_socket_Application4,            // general application protocol #4
    E_ttc_packet_pattern_socket_Application5,            // general application protocol #5
    E_ttc_packet_pattern_socket_Application6,            // general application protocol #6
    E_ttc_packet_pattern_socket_Application7,            // general application protocol #7
    E_ttc_packet_pattern_socket_Application8,            // general application protocol #8
    E_ttc_packet_pattern_socket_Application9,            // general application protocol #9
    // simply add more entries if ttc_radio_socket_new() runs out of entries

    E_ttc_packet_pattern_socket_End_Application,         // valid dynamic application protocols above

    E_ttc_packet_pattern_socket_Begin_Radio,             // protocols used by ttc_radio and ttc_rtls below
    E_ttc_packet_pattern_socket_Radio_Ranging,           // ranging measure protocol                  (->radio_common.c)
    E_ttc_packet_pattern_socket_Radio_RTLS,              // generic realtime location service protocol (->ttc_rtls)
    E_ttc_packet_pattern_socket_Radio_RTLS_Simple4,      // special realtime location service protocol (->rtls_square4)
    E_ttc_packet_pattern_socket_End_Radio,               // protocols used by ttc_radio and ttc_rtls above

    // add more fixed socket identifiers...


    E_ttc_packet_pattern_socket_Begin_AnyOf,             // type pattern identifiers do not occur in packets but can be used to match a group of socket identifiers (e.g. ttc_radio_received_packet*() )
    E_ttc_packet_pattern_socket_AnyOf_Application,       // search pattern for everything inside  E_ttc_packet_pattern_socket_Begin_Application..ttc_packet_pattern_protocol_End_Application
    E_ttc_packet_pattern_socket_AnyOf_NonApplication,    // search pattern for everything outside E_ttc_packet_pattern_socket_Begin_Application..ttc_packet_pattern_protocol_End_Application
    E_ttc_packet_pattern_socket_AnyOf_Radio,             // search pattern for everything inside  E_ttc_packet_pattern_socket_Begin_Radio..ttc_packet_pattern_protocol_End_Radio
    E_ttc_packet_pattern_socket_AnyOf_NonRadio,          // search pattern for everything outside E_ttc_packet_pattern_socket_Begin_Radio..ttc_packet_pattern_protocol_End_Radio

    // add more search pattern identifiers...

    E_ttc_packet_pattern_socket_End_AnyOf,               // types above are special types which do not occur in packets

    E_ttc_packet_pattern_socket_unknown                  // all values from here are invalid
} e_ttc_packet_pattern;

#ifndef TTC_PACKET_SOCKET_BYTE
    #  error Missing definition for TTC_PACKET_SOCKET_BYTE. Must be defined as 1 or 0. Include ttc_packet_types.h instead of this file!
#endif

typedef struct s_ttc_packet_payload { // generic packet payload
#if TTC_PACKET_SOCKET_BYTE==1
    e_ttc_packet_pattern  SocketID;  // socket that handles Payload[]
#endif
    t_u8 Raw[0];                      // raw protocol specific packet payload
} __attribute__( ( __packed__ ) ) t_ttc_packet_payload;

//}StructureDeclarations

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //packet_common_types_h
