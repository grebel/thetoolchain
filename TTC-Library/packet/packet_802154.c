/** packet_802154.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for packet devices on 802154 architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 27 at 20150928 09:30:04 UTC
 *
 *  Note: See ttc_packet.h for description of 802154 independent PACKET implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "packet_802154.h".
 */

#include "packet_802154.h"
#include "../ttc_basic.h"
#include "../ttc_heap.h"
#include "../ttc_memory.h"
#include "packet_common.h" // generic functions shared by low-level drivers of all architectures
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

t_u16 packet_802154_InitialFCF_Offset = 0; // calculated by packet_802154_prepare()

const static t_u16 packet_802154_InitialFCF[] = {
    // note: keep in same order as e_ttc_packet_architecture!
    E_ttc_packet_type_802154, // must be first entry of this array!

    PACKET_802154_000000_ACK_INITIAL_FCF,       // == E_ttc_packet_type_802154_ack

    // general mac header of all type of address modes
    PACKET_802154_000010_MHR_INITIAL_FCF,       // == E_ttc_packet_type_802154_MHR_000010
    PACKET_802154_000011_MHR_INITIAL_FCF,       // == E_ttc_packet_type_802154_MHR_000011
    PACKET_802154_001000_MHR_INITIAL_FCF,       // == E_ttc_packet_type_802154_MHR_001000
    PACKET_802154_001010_MHR_INITIAL_FCF,       // == E_ttc_packet_type_802154_MHR_001010
    PACKET_802154_001100_MHR_INITIAL_FCF,       // == E_ttc_packet_type_802154_MHR_001100
    PACKET_802154_001110_MHR_INITIAL_FCF,       // == E_ttc_packet_type_802154_MHR_001110
    PACKET_802154_001111_MHR_INITIAL_FCF,       // == E_ttc_packet_type_802154_MHR_001111
    PACKET_802154_010010_MHR_INITIAL_FCF,       // == E_ttc_packet_type_802154_MHR_010010
    PACKET_802154_010011_MHR_INITIAL_FCF,       // == E_ttc_packet_type_802154_MHR_010011
    PACKET_802154_011100_MHR_INITIAL_FCF,       // == E_ttc_packet_type_802154_MHR_011100
    PACKET_802154_011000_MHR_INITIAL_FCF,       // == E_ttc_packet_type_802154_MHR_011000
    PACKET_802154_011010_MHR_INITIAL_FCF,       // == E_ttc_packet_type_802154_MHR_011010
    PACKET_802154_011110_MHR_INITIAL_FCF,       // == E_ttc_packet_type_802154_MHR_011110
    PACKET_802154_011111_MHR_INITIAL_FCF,       // == E_ttc_packet_type_802154_MHR_011111

    // secured general mac header of all type of address modes
    PACKET_802154_100010_MHR_INITIAL_FCF,       // PACKET_802154_MHR_100010
    PACKET_802154_100011_MHR_INITIAL_FCF,       // PACKET_802154_MHR_100011
    PACKET_802154_101000_MHR_INITIAL_FCF,       // PACKET_802154_MHR_101000
    PACKET_802154_101010_MHR_INITIAL_FCF,       // PACKET_802154_MHR_101010
    PACKET_802154_101100_MHR_INITIAL_FCF,       // PACKET_802154_MHR_101100
    PACKET_802154_101110_MHR_INITIAL_FCF,       // PACKET_802154_MHR_101110
    PACKET_802154_101111_MHR_INITIAL_FCF,       // PACKET_802154_MHR_101111
    PACKET_802154_111100_MHR_INITIAL_FCF,       // PACKET_802154_MHR_111100
    PACKET_802154_111000_MHR_INITIAL_FCF,       // PACKET_802154_MHR_111000
    PACKET_802154_110010_MHR_INITIAL_FCF,       // PACKET_802154_MHR_110010
    PACKET_802154_111010_MHR_INITIAL_FCF,       // PACKET_802154_MHR_111010
    PACKET_802154_111110_MHR_INITIAL_FCF,       // PACKET_802154_MHR_111110
    PACKET_802154_111111_MHR_INITIAL_FCF,       // PACKET_802154_MHR_111111

    E_ttc_packet_type_802154_mhr,

    // data packets support all type of address modes
    PACKET_802154_000010_DATA_INITIAL_FCF,      // == E_ttc_packet_type_802154_Data_000010
    PACKET_802154_000011_DATA_INITIAL_FCF,      // == E_ttc_packet_type_802154_Data_000011
    PACKET_802154_001000_DATA_INITIAL_FCF,      // == E_ttc_packet_type_802154_Data_001000
    PACKET_802154_001010_DATA_INITIAL_FCF,      // == E_ttc_packet_type_802154_Data_001010
    PACKET_802154_001100_DATA_INITIAL_FCF,      // == E_ttc_packet_type_802154_Data_001100
    PACKET_802154_001110_DATA_INITIAL_FCF,      // == E_ttc_packet_type_802154_Data_001110
    PACKET_802154_001111_DATA_INITIAL_FCF,      // == E_ttc_packet_type_802154_Data_001111
    PACKET_802154_011100_DATA_INITIAL_FCF,      // == E_ttc_packet_type_802154_Data_011100
    PACKET_802154_011000_DATA_INITIAL_FCF,      // == E_ttc_packet_type_802154_Data_011000
    PACKET_802154_011010_DATA_INITIAL_FCF,      // == E_ttc_packet_type_802154_Data_011010
    PACKET_802154_011110_DATA_INITIAL_FCF,      // == E_ttc_packet_type_802154_Data_011110
    PACKET_802154_011111_DATA_INITIAL_FCF,      // == E_ttc_packet_type_802154_Data_011111

    // secured data mac header of all type of address modes
    PACKET_802154_100010_DATA_INITIAL_FCF,      // PACKET_802154_DATA_100010
    PACKET_802154_100011_DATA_INITIAL_FCF,      // PACKET_802154_DATA_100011
    PACKET_802154_101000_DATA_INITIAL_FCF,      // PACKET_802154_DATA_101000
    PACKET_802154_101010_DATA_INITIAL_FCF,      // PACKET_802154_DATA_101010
    PACKET_802154_101100_DATA_INITIAL_FCF,      // PACKET_802154_DATA_101100
    PACKET_802154_101110_DATA_INITIAL_FCF,      // PACKET_802154_DATA_101110
    PACKET_802154_101111_DATA_INITIAL_FCF,      // PACKET_802154_DATA_101111
    PACKET_802154_111000_DATA_INITIAL_FCF,      // PACKET_802154_DATA_111000
    PACKET_802154_111010_DATA_INITIAL_FCF,      // PACKET_802154_DATA_111010
    PACKET_802154_111100_DATA_INITIAL_FCF,      // PACKET_802154_DATA_111100
    PACKET_802154_111110_DATA_INITIAL_FCF,      // PACKET_802154_DATA_111110
    PACKET_802154_111111_DATA_INITIAL_FCF,      // PACKET_802154_DATA_111111
    E_ttc_packet_type_802154_data,

    // command packets support all type of address modes
    PACKET_802154_000010_COMMAND_INITIAL_FCF,   // == E_ttc_packet_type_802154_Command_000010
    PACKET_802154_000011_COMMAND_INITIAL_FCF,   // == E_ttc_packet_type_802154_Command_000011
    PACKET_802154_001000_COMMAND_INITIAL_FCF,   // == E_ttc_packet_type_802154_Command_001000
    PACKET_802154_001010_COMMAND_INITIAL_FCF,   // == E_ttc_packet_type_802154_Command_001010
    PACKET_802154_001100_COMMAND_INITIAL_FCF,   // == E_ttc_packet_type_802154_Command_001100
    PACKET_802154_001110_COMMAND_INITIAL_FCF,   // == E_ttc_packet_type_802154_Command_001110
    PACKET_802154_001111_COMMAND_INITIAL_FCF,   // == E_ttc_packet_type_802154_Command_001111
    PACKET_802154_011000_COMMAND_INITIAL_FCF,   // == E_ttc_packet_type_802154_Command_011000
    PACKET_802154_011100_COMMAND_INITIAL_FCF,   // == E_ttc_packet_type_802154_Command_011100
    PACKET_802154_011010_COMMAND_INITIAL_FCF,   // == E_ttc_packet_type_802154_Command_011010
    PACKET_802154_011110_COMMAND_INITIAL_FCF,   // == E_ttc_packet_type_802154_Command_011110
    PACKET_802154_011111_COMMAND_INITIAL_FCF,   // == E_ttc_packet_type_802154_Command_011111

    // secured command mac header of all type of address modes
    PACKET_802154_100010_COMMAND_INITIAL_FCF,   // PACKET_802154_COMMAND_100010
    PACKET_802154_100011_COMMAND_INITIAL_FCF,   // PACKET_802154_COMMAND_100011
    PACKET_802154_101000_COMMAND_INITIAL_FCF,   // PACKET_802154_COMMAND_101000
    PACKET_802154_101010_COMMAND_INITIAL_FCF,   // PACKET_802154_COMMAND_101010
    PACKET_802154_101100_COMMAND_INITIAL_FCF,   // PACKET_802154_COMMAND_101100
    PACKET_802154_101110_COMMAND_INITIAL_FCF,   // PACKET_802154_COMMAND_101110
    PACKET_802154_101111_COMMAND_INITIAL_FCF,   // PACKET_802154_COMMAND_101111
    PACKET_802154_111000_COMMAND_INITIAL_FCF,   // PACKET_802154_COMMAND_111000
    PACKET_802154_111100_COMMAND_INITIAL_FCF,   // PACKET_802154_COMMAND_111100
    PACKET_802154_111010_COMMAND_INITIAL_FCF,   // PACKET_802154_COMMAND_111010
    PACKET_802154_111110_COMMAND_INITIAL_FCF,   // PACKET_802154_COMMAND_111110
    PACKET_802154_111111_COMMAND_INITIAL_FCF,   // PACKET_802154_COMMAND_111111
    E_ttc_packet_type_802154_command,


    PACKET_802154_000010_BEACON_INITIAL_FCF,    // == E_ttc_packet_type_802154_Beacon_000010
    PACKET_802154_000011_BEACON_INITIAL_FCF,    // == E_ttc_packet_type_802154_Beacon_000011

    // secured beacon headers
    PACKET_802154_100010_BEACON_INITIAL_FCF,    // PACKET_802154_BEACON_100010
    PACKET_802154_100011_BEACON_INITIAL_FCF,    // PACKET_802154_BEACON_100011
    E_ttc_packet_type_802154_beacon,

    0
};

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

/** test implementation of packet_802154_initialize() and packet_802154_identify() for given attributes
 *
 */
void _packet_802154_test_initialize( volatile t_ttc_packet* Packet, e_ttc_packet_category Category, e_ttc_packet_type Type, t_u16 InitialFCF );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

void                        packet_802154_acknowledge( t_ttc_packet* Packet, t_ttc_packet* PacketACK ) {
    Assert_PACKET_EXTRA_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_PACKET_EXTRA_Writable( PacketACK, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    packet_802154_initialize( PacketACK, E_ttc_packet_type_802154_ack );

    // acknowledgements must carry same sequence number as received packet
    PacketACK->MAC.packet_802154_ack_000000.SequenceNo = Packet->MAC.packet_802154_generic.SequenceNo;

    // acknowledgements have no payload: calculating frame length is constant
    PacketACK->MAC.Length = sizeof( t_packet_802154_ack_000000 )
                            + 2;                                  // Frame Checksum
}
BOOL                        packet_802154_acknowledge_request_get( t_ttc_packet* Packet ) {
    Assert_PACKET_EXTRA_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (packets in ROM make no sense)

    return ( BOOL ) Packet->MAC.packet_802154_generic.FCF.Bits.AckRequest;
}
void                        packet_802154_acknowledge_request_set( t_ttc_packet* Packet, BOOL RequestACK ) {
    Assert_PACKET_EXTRA_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    if ( RequestACK )
    { Packet->MAC.packet_802154_generic.FCF.Bits.AckRequest = 1; }
    else
    { Packet->MAC.packet_802154_generic.FCF.Bits.AckRequest = 0; }
}
void                        packet_802154_address_reply2( t_ttc_packet* Packet, t_ttc_packet* PacketReply ) {
    Assert_PACKET_EXTRA_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_PACKET_EXTRA_Writable( PacketReply, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    // ensure that reply packet has same sequence number
    PacketReply->MAC.packet_802154_generic.SequenceNo = Packet->MAC.packet_802154_generic.SequenceNo;
}
e_ttc_packet_address_format packet_802154_get_pointer_source( t_ttc_packet* Packet, void** Pointer2SourceID, t_u16** Pointer2SourcePanID ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    if ( Packet ) {
        // Source Address comes after Destination Address => two stage switch clause

        switch ( Packet->MAC.packet_802154_generic.FCF.Bits.DstAddressType ) {
            case 0b00: { // Packet does not store a destination address (must be a beacon packet)
                switch ( Packet->MAC.packet_802154_generic.FCF.Bits.SrcAddressType ) { // FCF is common to all packet types
                    case 0b00: break; // packet does not store a source address
                    case 0b10:
                        if ( Pointer2SourceID )
                        { *Pointer2SourceID  = & Packet->MAC.packet_802154_mhr_000010.SrcNodeID_L; }
                        if ( Pointer2SourcePanID )
                        { *Pointer2SourcePanID = ( t_u16* ) & Packet->MAC.packet_802154_mhr_000010.SrcPanID_L; }
                        return E_ttc_packet_address_Source16; // 16 bit address found
                        break;
                    case 0b11:
                        if ( Pointer2SourceID )
                        { *Pointer2SourceID  = & Packet->MAC.packet_802154_mhr_000011.SrcNodeID_L; }
                        if ( Pointer2SourcePanID )
                        { *Pointer2SourcePanID = ( t_u16* ) & Packet->MAC.packet_802154_mhr_000011.SrcPanID_L; }
                        return E_ttc_packet_address_Source64; // 64 bit address found
                        break;
                    default: break;
                        break;
                }
            }
            case 0b10: { // Packet stores a 16-bit destination address
                switch ( Packet->MAC.packet_802154_generic.FCF.Bits.SrcAddressType ) { // FCF is common to all packet types
                    case 0b00: break; // packet does not store a source address
                    case 0b10:
                        if ( Pointer2SourcePanID ) {
                            if ( Pointer2SourceID )
                            { *Pointer2SourceID  = & Packet->MAC.packet_802154_mhr_001010.SrcNodeID_L; }
                            if ( Packet->MAC.packet_802154_generic.FCF.Bits.PanID_Compression )
                            { *Pointer2SourcePanID = NULL; } // this packet contains only target PAN ID!
                            else
                            { *Pointer2SourcePanID = ( t_u16* ) & Packet->MAC.packet_802154_mhr_001010.SrcPanID_L; }
                        }
                        else {
                            if ( Pointer2SourceID )
                            { *Pointer2SourceID  = & Packet->MAC.packet_802154_mhr_001010.SrcNodeID_L; }
                        }
                        return E_ttc_packet_address_Source16; // 16 bit address found
                        break;
                    default: ttc_assert_halt_origin( ec_packet_unknownPacketType ); break; // this address type is not supported!
                        break;
                }
            }
            case 0b11: { // Packet stores a 64-bit destination address
                switch ( Packet->MAC.packet_802154_generic.FCF.Bits.SrcAddressType ) { // FCF is common to all packet types
                    case 0b00: break; // packet does not store a source address
                    case 0b10:
                        if ( Pointer2SourceID )
                        { *Pointer2SourceID  = & Packet->MAC.packet_802154_mhr_001111.SrcNodeID_L; }
                        if ( Pointer2SourcePanID ) {
                            if ( Packet->MAC.packet_802154_generic.FCF.Bits.PanID_Compression )
                            { *Pointer2SourcePanID = NULL; } // this packet contains only target PAN ID!
                            else
                            { *Pointer2SourcePanID = ( t_u16* ) & Packet->MAC.packet_802154_mhr_001111.SrcPanID_L; }
                        }
                        return E_ttc_packet_address_Source64; // 64 bit address found
                        break;
                    default: ttc_assert_halt_origin( ec_packet_unknownPacketType ); break; // this address type is not supported!
                        break;
                }
            }
            default: break;
        };
    }
    return E_ttc_packet_address_None;
}
e_ttc_packet_address_format packet_802154_get_pointer_target( t_ttc_packet* Packet, void** Pointer2TargetID, t_u16** Pointer2TargetPanID ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    // Assert_PACKET_EXTRA_Writable(Pointer2TargetID, ttc_assert_origin_auto, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    // Assert_PACKET_EXTRA_Writable(Pointer2TargetPanID, ttc_assert_origin_auto, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    if ( Packet ) {
        // Destination Address comes before Source Address => single stage switch clause

        switch ( Packet->MAC.packet_802154_generic.FCF.Bits.DstAddressType ) {
            case 0b00: { // Packet does not store a destination address (must be a beacon packet)
                break;
            }
            case 0b10: { // Packet stores a 16-bit destination address
                if ( Pointer2TargetID )
                { *Pointer2TargetID  = & Packet->MAC.packet_802154_mhr_001000.DestNodeID_L; }
                if ( Pointer2TargetPanID )
                { *Pointer2TargetPanID = ( t_u16* ) & Packet->MAC.packet_802154_mhr_001000.DestPanID_L; }
                return E_ttc_packet_address_Target16; // 16 bit address found
                break;
            }
            case 0b11: { // Packet stores a 64-bit destination address
                if ( Pointer2TargetID )
                { *Pointer2TargetID  = & Packet->MAC.packet_802154_mhr_001100.DestNodeID_L; }
                if ( Pointer2TargetPanID )
                { *Pointer2TargetPanID = ( t_u16* ) & Packet->MAC.packet_802154_mhr_001100.DestPanID_L; }
                return E_ttc_packet_address_Target64; // 64 bit address found
                break;
            }
            default: break;
        };
    }
    return E_ttc_packet_address_None;
}
e_ttc_packet_category       packet_802154_identify( t_ttc_packet* Packet ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    e_ttc_packet_type           PacketType          = E_ttc_packet_type_802154; // precise type of packet
    e_ttc_packet_address_format PacketAddressFormat = E_ttc_packet_address_unknown;     // CCCC part of packet type (0b**CCCC)
    e_ttc_packet_category       PacketCategory      = E_ttc_packet_category_unknown;      // more general type of packet

    u_packet_802154_fcf FCF; FCF.Word = Packet->MAC.packet_802154_generic.FCF.Word; // FCF is found at same position in all packet types
    e_packet_802154_fcf_frame_type FrameType = Packet->MAC.packet_802154_generic.FCF.Bits.FrameType;
    BOOL SecurityEnabled   = FCF.Bits.SecurityEnabled;
    BOOL PanID_Compression = FCF.Bits.PanID_Compression;

    switch ( FrameType ) { // identify PacketCategory
        case packet_ft_beacon:
            PacketCategory = E_ttc_packet_category_Beacon;          break;
        case packet_ft_data:
            PacketCategory = E_ttc_packet_category_Data;            break;
        case packet_ft_command:
            PacketCategory = E_ttc_packet_category_Command;         break;
        case packet_ft_acknowledgement:
            PacketCategory = E_ttc_packet_category_Acknowledgement; break;
        default:
            PacketCategory = E_ttc_packet_category_unknown;         break;
    }

    if ( PacketCategory == E_ttc_packet_category_Acknowledgement ) { // ack has no addressing
        PacketAddressFormat = E_ttc_packet_address_None;
        PacketType          = E_ttc_packet_type_802154_ack;
    }
    else {
        switch ( FCF.Bits.DstAddressType ) { // big switch identfies PacketType and PacketAddressFormat
            case packet_802154_address_type_00_bits: { // 0b**00** No Destination (-> Coordinator)
                switch ( FCF.Bits.SrcAddressType ) {
                    case packet_802154_address_type_16_bits: { // 0b**0010  No Destination + 16-bit Source
                        PacketAddressFormat = E_ttc_packet_address_Source16_Sourcepan16;
                        if ( SecurityEnabled ) {
                            if ( PanID_Compression )  { // type 0b110010
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_110010;
                                break; // makes no sense
                            }
                            else  { // type 0b100010
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_100010;
                                switch ( FrameType ) {
                                    case packet_ft_beacon:
                                        PacketType = E_ttc_packet_type_802154_Beacon_100010;  break;
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_100010;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_100010; break;
                                    default: break;
                                }
                            }
                        }
                        else {
                            if ( PanID_Compression ) { // type 0b010010
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_010010;
                                break; // makes no sense
                            }
                            else  { // type 0b000010
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_000010;
                                switch ( FrameType ) {
                                    case packet_ft_beacon:
                                        PacketType = E_ttc_packet_type_802154_Beacon_000010;  break;
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_000010;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_000010; break;
                                    default: break;
                                }
                            }
                        }
                        break;
                    }
                    case packet_802154_address_type_64_bits: { // 0b**0011  No Destination + 64-bit Source
                        PacketAddressFormat = E_ttc_packet_address_Source64_Targetpan16;
                        if ( SecurityEnabled ) {
                            if ( PanID_Compression )  { // type 0b110010
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_110010;
                                break; // makes no sense
                            }
                            else  { // type 0b100011
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_100011;
                                switch ( FrameType ) {
                                    case packet_ft_beacon:
                                        PacketType = E_ttc_packet_type_802154_Beacon_100011;  break;
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_100011;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_100011; break;
                                    default: break;
                                }
                            }
                        }
                        else {
                            if ( PanID_Compression ) { // type 0b010011
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_010011;
                                break; // makes no sense
                            }
                            else  { // type 0b000011
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_000011;
                                switch ( FrameType ) {
                                    case packet_ft_beacon:
                                        PacketType = E_ttc_packet_type_802154_Beacon_000011;  break;
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_000011;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_000011; break;
                                    default: break;
                                }
                            }
                        }
                        break;
                    }
                    default: break;
                } //switch
                break;
            }
            case packet_802154_address_type_16_bits: { // 0b**10** 16-bit Destination
                switch ( FCF.Bits.SrcAddressType ) {
                    case 0b00: { // 0b001000  16-bit Destination + no Source
                        PacketAddressFormat = E_ttc_packet_address_Target16_Targetpan16;
                        if ( SecurityEnabled ) {
                            if ( PanID_Compression )  { // type 0b111000
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_111000;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_111000;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_111000; break;
                                    default: break;
                                }
                            }
                            else  { // type 0b101000
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_101000;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_101000;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_101000; break;
                                    default: break;
                                }
                            }
                        }
                        else {
                            if ( PanID_Compression )  { // type 0b011000
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_011000;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_011000;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_011000; break;
                                    default: break;
                                }
                            }
                            else  { // type 0b001000
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_001000;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_001000;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_001000; break;
                                    default: break;
                                }
                            }
                        }
                        break;
                    }
                    case 0b10: { // 0b001010  16-bit Destination + 16-bit Source
                        if ( SecurityEnabled ) {
                            if ( PanID_Compression )  { // type 0b111010
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_111010;
                                PacketAddressFormat = E_ttc_packet_address_Source16_Target16_Targetpan16;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_101010;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_101010; break;
                                    default: break;
                                }
                            }
                            else  {  // type 0b101010
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_101010;
                                PacketAddressFormat = E_ttc_packet_address_Source16_Target16_Sourcepan16_Targetpan16;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_111010;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_111010; break;
                                    default: break;
                                }
                            }
                        }
                        else {
                            if ( PanID_Compression ) { // type 0b011010
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_011010;
                                PacketAddressFormat = E_ttc_packet_address_Source16_Target16_Targetpan16;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_011010;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_011010; break;
                                    default: break;
                                }
                            }
                            else  {                    // type 0b001010
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_001010;
                                PacketAddressFormat = E_ttc_packet_address_Source16_Target16_Sourcepan16_Targetpan16;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_001010;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_001010; break;
                                    default: break;
                                }
                            }
                        }
                        break;
                    }
                    case 0b11: { // 0b001011  16-bit Destination + 64-bit Source
                        PacketType = E_ttc_packet_type_802154_unknown; // packets with 16-bit source + 64-bit destination are not supported

                        if ( PanID_Compression )  { // type 0b101011
                            // PacketHeaderType = E_ttc_packet_type_802154_MHR_111011;
                            PacketAddressFormat = E_ttc_packet_address_Source64_Target16_Targetpan16;
                        }
                        else  {  // type 0b101010
                            // PacketHeaderType = E_ttc_packet_type_802154_MHR_101011;
                            PacketAddressFormat = E_ttc_packet_address_Source64_Target16_Sourcepan16_Targetpan16;
                        }
                        break;
                    }
                    default:   break;
                } //switch
                break;
            }
            case packet_802154_address_type_64_bits: { // 0b**11** 64-bit Destination
                switch ( FCF.Bits.SrcAddressType ) {
                    case packet_802154_address_type_00_bits: { // 0b**1100  64-bit Destination + No Source
                        PacketAddressFormat = E_ttc_packet_address_Target64_Targetpan16;
                        if ( SecurityEnabled ) {
                            if ( PanID_Compression )  { // type 0b111100
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_111100;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_111100;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_111100; break;
                                    default: break;
                                }
                            }
                            else  { // type 0b101100
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_101100;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_101100;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_101100; break;
                                    default: break;
                                }
                            }
                        }
                        else {
                            if ( PanID_Compression )  { // type 0b011100
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_011100;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_011100;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_011100; break;
                                    default: break;
                                }
                            }
                            else  { // type 0b001100
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_001100;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_001100;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_001100; break;
                                    default: break;
                                }
                            }
                        }
                        break;
                    }
                    case packet_802154_address_type_16_bits: { // 0b**1110  64-bit Destination + 16-bit Source
                        if ( SecurityEnabled ) {
                            if ( PanID_Compression ) { // 0b111110
                                PacketAddressFormat = E_ttc_packet_address_Source16_Target64_Targetpan16;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_111110;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_111110; break;
                                    default: break;
                                }
                            }
                            else  { // type 0b101110
                                PacketAddressFormat = E_ttc_packet_address_Source16_Target64_Sourcepan16_Targetpan16;
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_101110;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_101110;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_101110; break;
                                    default: break;
                                }
                            }
                        }
                        else {
                            if ( PanID_Compression )  { // type 0b011110
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_011110;
                                PacketAddressFormat = E_ttc_packet_address_Source16_Target64_Targetpan16;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_011110;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_011110; break;
                                    default: break;
                                }
                            }
                            else  { // type 0b001110
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_001110;
                                PacketAddressFormat = E_ttc_packet_address_Source16_Target64_Sourcepan16_Targetpan16;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_001110;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_001110; break;
                                    default: break;
                                }
                            }
                        }
                        break;
                    }
                    case packet_802154_address_type_64_bits: { // 0b**1111  64-bit Destination + 64-bit Source
                        if ( SecurityEnabled ) {
                            if ( PanID_Compression )  { // type 0b111111
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_111111;
                                PacketAddressFormat = E_ttc_packet_address_Source64_Target64_Targetpan16;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_111111;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_111111; break;
                                    default: break;
                                }
                            }
                            else  { // type 0b101111
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_101111;
                                PacketAddressFormat = E_ttc_packet_address_Source64_Target64_Sourcepan16_Targetpan16;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_101111;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_101111; break;
                                    default: break;
                                }
                            }
                        }
                        else {
                            if ( PanID_Compression )  { // type 0b011111
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_011111;
                                PacketAddressFormat = E_ttc_packet_address_Source64_Target64_Targetpan16;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_011111;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_011111; break;
                                    default: break;
                                }
                            }
                            else  { // type 0b001111
                                // PacketHeaderType = E_ttc_packet_type_802154_MHR_001111;
                                PacketAddressFormat = E_ttc_packet_address_Source64_Target64_Sourcepan16_Targetpan16;
                                switch ( FrameType ) {
                                    case packet_ft_data:
                                        PacketType = E_ttc_packet_type_802154_Data_001111;    break;
                                    case packet_ft_command:
                                        PacketType = E_ttc_packet_type_802154_Command_001111; break;
                                    default: break;
                                }
                            }
                        }
                        break;
                    }
                    default:   break;
                }
                break;
            } //case
            default:   break;
        } //switch
    }
    if ( ( PacketType <= E_ttc_packet_type_802154 ) || ( PacketType >= E_ttc_packet_type_802154_unknown ) ) // invalid packet: return 0
    { return 0; }

    Assert_PACKET_EXTRA( PacketType > E_ttc_packet_type_802154, ttc_assert_origin_auto );         // could not identify packet type
    Assert_PACKET_EXTRA( PacketType < E_ttc_packet_type_802154_unknown, ttc_assert_origin_auto );  // check implementation above!

    Packet->Meta.Type          = PacketType;
    Packet->Meta.Category      = PacketCategory;
    Packet->Meta.AddressFormat = PacketAddressFormat;

    if ( !Packet->Meta.SizeOfMacHeader ) { // size of header missing: calculate it
        packet_802154_mac_header_get_size( Packet );
    }

    return PacketCategory;
}
t_u8                        packet_802154_initialize( t_ttc_packet* Packet, e_ttc_packet_type Type ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    Assert_PACKET_EXTRA( ( Type > E_ttc_packet_type_802154 ) && ( Type < E_ttc_packet_type_802154_unknown ), ttc_assert_origin_auto );

    // size of MAC header depends on packet type
    t_u16 HeaderSize = packet_802154_mac_header_get_type_size( Type );
    ttc_memory_set( &( Packet->MAC ), 0, HeaderSize );

    // meta header has been zeroed: now fill all fields
    Packet->Meta.SizeOfMacHeader = HeaderSize;

    if ( 1 ) { // load initial FCF value
        t_u16 Index = ( ( t_u16 ) Type ) - packet_802154_InitialFCF_Offset;
        Assert_PACKET( Index < sizeof( packet_802154_InitialFCF ) / 2, ttc_assert_origin_auto );  // calculation ran out of array!
        Packet->MAC.packet_802154_generic.FCF.Word = packet_802154_InitialFCF[Index];
    }

    return HeaderSize;
}
BOOL                        packet_802154_is_acknowledge( t_ttc_packet* Packet ) {
    Assert_PACKET_EXTRA_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    if ( packet_802154_type_get( Packet ) == E_ttc_packet_type_802154_ack )
    { return TRUE; }

    return ( BOOL ) 0;
}
BOOL                        packet_802154_is_beacon( t_ttc_packet* Packet ) {
    Assert_PACKET_EXTRA_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    e_ttc_packet_type Type = packet_802154_type_get( Packet );
    if ( ( Type >= E_ttc_packet_type_802154_Beacon_000010 ) && ( Type < E_ttc_packet_type_802154_beacon ) )
    { return TRUE; }

    return ( BOOL ) 0;
}
BOOL                        packet_802154_is_command( t_ttc_packet* Packet ) {
    Assert_PACKET_EXTRA_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    e_ttc_packet_type Type = packet_802154_type_get( Packet );
    if ( ( Type >= E_ttc_packet_type_802154_Command_000010 ) && ( Type < E_ttc_packet_type_802154_command ) )
    { return TRUE; }

    return ( BOOL ) 0;
}
BOOL                        packet_802154_is_data( t_ttc_packet* Packet ) {
    Assert_PACKET_EXTRA_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    e_ttc_packet_type Type = packet_802154_type_get( Packet );
    if ( ( Type >= E_ttc_packet_type_802154_Data_000010 ) && ( Type < E_ttc_packet_type_802154_data ) )
    { return TRUE; }

    return ( BOOL ) 0;
}
t_u8                        packet_802154_mac_header_get_size( t_ttc_packet* Packet ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    t_u8 SizeOfMacHeader = Packet->Meta.SizeOfMacHeader;
    if ( SizeOfMacHeader )
    { return SizeOfMacHeader; }

    if ( !Packet->Meta.Type ) { // missing packet type in meta block: identify packet
        Packet->Meta.SizeOfMacHeader = -1; // avoid packet_802154_identify() to call us again
        packet_802154_identify( Packet );
        if ( !Packet->Meta.Type ) { // packet seems to be invalid: we cannot obtain size of its mac header
            Packet->Meta.SizeOfMacHeader = 0;
            return 0;
        }
    }

    SizeOfMacHeader = packet_802154_mac_header_get_type_size( Packet->Meta.Type );

    switch ( Packet->Meta.Type ) { // some headers have dynamic size

        case E_ttc_packet_type_802154_Beacon_000010:  {   // beacon packets store a variable amount of extra header data

            // calculate size of GTS_Descriptors[]
            t_u8 Size_GTS_Descriptors  = ( Packet->MAC.packet_802154_beacon_000010.GTS_Specification.Bits.DescriptorCount * sizeof( t_packet_802154_beacon_gts_descriptor ) );

            // PendingAddresses[] may store a combination of 64-bit extended and 16-bit short adresses
            t_u8 Size_PendingAddresses =
                ( Packet->MAC.packet_802154_beacon_000010.AddressSpecification.Bits.AmountPendingExtended * 8 ) +
                ( Packet->MAC.packet_802154_beacon_000010.AddressSpecification.Bits.AmountPendingShort * 2 );

            SizeOfMacHeader = sizeof( t_packet_802154_beacon_000010 ) + Size_GTS_Descriptors + Size_PendingAddresses;
            break;
        }
        case E_ttc_packet_type_802154_Beacon_000011: {    // beacon packets store a variable amount of extra header data
            // calculate size of GTS_Descriptors[]
            t_u8 Size_GTS_Descriptors  = ( Packet->MAC.packet_802154_beacon_000011.GTS_Specification.Bits.DescriptorCount * sizeof( t_packet_802154_beacon_gts_descriptor ) );

            // PendingAddresses[] may store a combination of 64-bit extended and 16-bit short adresses
            t_u8 Size_PendingAddresses =
                ( Packet->MAC.packet_802154_beacon_000011.AddressSpecification.Bits.AmountPendingExtended * 8 ) +
                ( Packet->MAC.packet_802154_beacon_000011.AddressSpecification.Bits.AmountPendingShort * 2 );

            SizeOfMacHeader = sizeof( t_packet_802154_beacon_000011 ) + Size_GTS_Descriptors + Size_PendingAddresses;
            break;
        }
        default: break;
    }

    return Packet->Meta.SizeOfMacHeader = SizeOfMacHeader;
}
t_u8                        packet_802154_mac_header_get_type_size( e_ttc_packet_type Type ) {
    if ( !Type ) { // packet seems to be invalid: we cannot obtain size of its mac header
        return 0;
    }

    t_u8 SizeOfMacHeader = 0;
    switch ( Type ) {
        case E_ttc_packet_type_802154_ack:                SizeOfMacHeader = sizeof( t_packet_802154_ack_000000 );    break;

        // data packets support all type of address modes
        case E_ttc_packet_type_802154_Data_000010:        SizeOfMacHeader = sizeof( t_packet_802154_mhr_000010 );    break;
        case E_ttc_packet_type_802154_Data_000011:        SizeOfMacHeader = sizeof( t_packet_802154_mhr_000011 );    break;
        case E_ttc_packet_type_802154_Data_001000:        SizeOfMacHeader = sizeof( t_packet_802154_mhr_001000 );    break;
        case E_ttc_packet_type_802154_Data_001010:        SizeOfMacHeader = sizeof( t_packet_802154_mhr_001010 );    break;
        case E_ttc_packet_type_802154_Data_001100:        SizeOfMacHeader = sizeof( t_packet_802154_mhr_001100 );    break;
        case E_ttc_packet_type_802154_Data_001110:        SizeOfMacHeader = sizeof( t_packet_802154_mhr_001110 );    break;
        case E_ttc_packet_type_802154_Data_001111:        SizeOfMacHeader = sizeof( t_packet_802154_mhr_001111 );    break;
        case E_ttc_packet_type_802154_Data_011100:        SizeOfMacHeader = sizeof( t_packet_802154_mhr_011100 );    break;
        case E_ttc_packet_type_802154_Data_011000:        SizeOfMacHeader = sizeof( t_packet_802154_mhr_011000 );    break;
        case E_ttc_packet_type_802154_Data_011010:        SizeOfMacHeader = sizeof( t_packet_802154_mhr_011010 );    break;
        case E_ttc_packet_type_802154_Data_011110:        SizeOfMacHeader = sizeof( t_packet_802154_mhr_011110 );    break;
        case E_ttc_packet_type_802154_Data_011111:        SizeOfMacHeader = sizeof( t_packet_802154_mhr_011111 );    break;

        // command packets support all type of address modes
        case E_ttc_packet_type_802154_Command_000010:     SizeOfMacHeader = sizeof( t_packet_802154_mhr_000010 ); break;
        case E_ttc_packet_type_802154_Command_000011:     SizeOfMacHeader = sizeof( t_packet_802154_mhr_000011 ); break;
        case E_ttc_packet_type_802154_Command_001000:     SizeOfMacHeader = sizeof( t_packet_802154_mhr_001000 ); break;
        case E_ttc_packet_type_802154_Command_001010:     SizeOfMacHeader = sizeof( t_packet_802154_mhr_001010 ); break;
        case E_ttc_packet_type_802154_Command_001100:     SizeOfMacHeader = sizeof( t_packet_802154_mhr_001100 ); break;
        case E_ttc_packet_type_802154_Command_001110:     SizeOfMacHeader = sizeof( t_packet_802154_mhr_001110 ); break;
        case E_ttc_packet_type_802154_Command_001111:     SizeOfMacHeader = sizeof( t_packet_802154_mhr_001111 ); break;
        case E_ttc_packet_type_802154_Command_011000:     SizeOfMacHeader = sizeof( t_packet_802154_mhr_011000 ); break;
        case E_ttc_packet_type_802154_Command_011100:     SizeOfMacHeader = sizeof( t_packet_802154_mhr_011100 ); break;
        case E_ttc_packet_type_802154_Command_011010:     SizeOfMacHeader = sizeof( t_packet_802154_mhr_011010 ); break;
        case E_ttc_packet_type_802154_Command_011110:     SizeOfMacHeader = sizeof( t_packet_802154_mhr_011110 ); break;
        case E_ttc_packet_type_802154_Command_011111:     SizeOfMacHeader = sizeof( t_packet_802154_mhr_011111 ); break;

        // beacon packets store a variable amount of extra header data (here we only calculate size of static part)
        case E_ttc_packet_type_802154_Beacon_000010:  {

            SizeOfMacHeader = sizeof( t_packet_802154_beacon_000010 );
            break;
        }
        case E_ttc_packet_type_802154_Beacon_000011: {
            SizeOfMacHeader = sizeof( t_packet_802154_beacon_000011 );
            break;
        }
        default:
            ttc_assert_halt_origin( ec_packet_unknownPacketType ); // did you add an entry to e_ttc_packet_type without updating this function?
            break;
    }

    return SizeOfMacHeader;
}
t_u8                        packet_802154_payload_get_pointer( t_ttc_packet* Packet, t_u8** PayloadPtr, t_u16* SizePtr, e_ttc_packet_pattern* SocketPtr ) {
    Assert_PACKET_EXTRA_Writable( Packet,     ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_PACKET_EXTRA_Writable( PayloadPtr, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    if ( SizePtr ) {
        Assert_PACKET_EXTRA_Writable( SizePtr, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
        if ( Packet->MAC.Length )
        { *SizePtr = packet_802154_payload_get_size( Packet ); }     // packet stores payload: return size of payload
        else
        { *SizePtr = packet_802154_payload_get_size_max( Packet ); } // packet stores no payload: return available payload size
    }
    else {
        if ( !Packet->Meta.Type )
        { packet_802154_identify( Packet ); } // must identify packet before we can access its payload (otherwise done by packet_802154_payload_get_size() )
    }

    e_ttc_packet_pattern SocketID = E_ttc_packet_pattern_socket_None;
    switch ( Packet->Meta.Type ) {
        case E_ttc_packet_type_802154_ack: {             //                                        -> packet_802154_types.h:t_packet_802154_ack_000000
            *PayloadPtr = NULL; // acknowledge has no payload
            break;
        }
        case E_ttc_packet_type_802154_Data_000010: {     // SrcAddr16+SrcPan16                     -> packet_802154_types.h:t_packet_802154_data_000010
            *PayloadPtr = Packet->MAC.packet_802154_data_000010.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_data_000010.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Data_000011: {     // SrcAddr64+SrcPan16                     -> packet_802154_types.h:t_packet_802154_data_000011
            *PayloadPtr = Packet->MAC.packet_802154_data_000011.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_data_000011.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Data_001000: {     // DstAddr16+DstPan16                     -> packet_802154_types.h:t_packet_802154_data_001000
            *PayloadPtr = Packet->MAC.packet_802154_data_001000.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_data_001000.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Data_001010: {     // DstAddr16+DstPan16+SrcAddr16+SrcPan16  -> packet_802154_types.h:t_packet_802154_data_001010
            *PayloadPtr = Packet->MAC.packet_802154_data_001010.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_data_001010.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Data_001100: {     // DstAddr64+DstPan16                     -> packet_802154_types.h:t_packet_802154_data_001100
            *PayloadPtr = Packet->MAC.packet_802154_data_001100.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_data_001100.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Data_011100: {     // DstAddr64+DstPan16                     -> packet_802154_types.h:t_packet_802154_data_001100
            *PayloadPtr = Packet->MAC.packet_802154_data_011100.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_data_011100.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Data_001111: {     // DstAddr64+DstPan16+SrcAddr64+SrcPan16  -> packet_802154_types.h:t_packet_802154_data_001111
            *PayloadPtr = Packet->MAC.packet_802154_data_001111.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_data_001111.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Data_011000: {     // DstAddr16                              -> packet_802154_types.h:t_packet_802154_data_011000
            *PayloadPtr = Packet->MAC.packet_802154_data_011000.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_data_011000.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Data_011010: {     // DstAddr16+SrcAddr16                    -> packet_802154_types.h:t_packet_802154_data_011010
            *PayloadPtr = Packet->MAC.packet_802154_data_011010.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_data_011010.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Data_011111: {     // DstAddr64+SrcAddr64                    -> packet_802154_types.h:t_packet_802154_data_011111
            *PayloadPtr = Packet->MAC.packet_802154_data_011111.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_data_011111.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Command_001000: {  // DstAddr16+DstPan16                     -> packet_802154_types.h:t_packet_802154_command_001000
            *PayloadPtr = Packet->MAC.packet_802154_command_001000.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_command_001000.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Command_001010: {  // DstAddr16+DstPan16+SrcAddr16+SrcPan16  -> packet_802154_types.h:t_packet_802154_command_001010
            *PayloadPtr = Packet->MAC.packet_802154_command_001010.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_command_001010.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Command_001100: {  // DstAddr64+DstPan16                     -> packet_802154_types.h:t_packet_802154_command_001100
            *PayloadPtr = Packet->MAC.packet_802154_command_001100.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_command_001100.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Command_001111: {  // DstAddr64+DstPan16+SrcAddr64+SrcPan16  -> packet_802154_types.h:t_packet_802154_command_001111
            *PayloadPtr = Packet->MAC.packet_802154_command_001111.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_command_001111.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Command_011000: {  // DstAddr16                              -> packet_802154_types.h:t_packet_802154_command_011000
            *PayloadPtr = Packet->MAC.packet_802154_command_011000.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_command_011000.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Command_011100: {  // DstAddr64                              -> packet_802154_types.h:t_packet_802154_command_011100
            *PayloadPtr = Packet->MAC.packet_802154_command_011100.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_command_011100.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Command_011010: {  // DstAddr16+SrcAddr16                    -> packet_802154_types.h:t_packet_802154_command_011010
            *PayloadPtr = Packet->MAC.packet_802154_command_011010.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_command_011010.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Command_011111: {  // DstAddr64+SrcAddr64                    -> packet_802154_types.h:t_packet_802154_command_011111
            *PayloadPtr = Packet->MAC.packet_802154_command_011111.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_command_011111.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Beacon_000010: {   // SrcAddr16+PanID16                      -> packet_802154_types.h:t_packet_802154_beacon_000010
            *PayloadPtr = Packet->MAC.packet_802154_beacon_000010.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_beacon_000010.Payload.SocketID;
#endif
            break;
        }
        case E_ttc_packet_type_802154_Beacon_000011: {   // SrcAddr64+PanID16                      -> packet_802154_types.h:t_packet_802154_beacon_000011
            *PayloadPtr = Packet->MAC.packet_802154_beacon_000011.Payload.Raw;
#if TTC_PACKET_SOCKET_BYTE==1
            SocketID = Packet->MAC.packet_802154_beacon_000011.Payload.SocketID;
#endif
            break;
        }

        default:                       // unknown packet type
            *PayloadPtr = ( t_u8* ) & ( Packet->MAC );
            return 1; // error-code
            break;
    }

#if TTC_ASSERT_PACKET_EXTRA == 1
    if ( Packet->Meta.Type != E_ttc_packet_type_802154_ack )
    { Assert_PACKET_Writable( *PayloadPtr, ttc_assert_origin_auto ); }  // packet should have payload, but payload address was mis-calculated
#endif

#if TTC_PACKET_SOCKET_BYTE==1
    if ( SocketPtr ) { // set socket identifier
        Assert_PACKET_Writable( SocketPtr, ttc_assert_origin_auto ); // given pointer does not target to writable memory. Check implementation of caller!
        *SocketPtr = SocketID;
    }
#else
    Assert_PACKET( 0, ttc_assert_origin_auto ); // must define TTC_PACKET_SOCKET_BYTE as 1 to use this feature. Check implementation of caller!
#endif

    return 0;
}
BOOL                        packet_802154_pattern_matches_type( e_ttc_packet_type Type, e_ttc_packet_pattern Pattern ) {

    switch ( Pattern ) {
        case E_ttc_packet_pattern_type_Acknowledge:
            return ( Type == E_ttc_packet_type_802154_ack );
            break;
        case E_ttc_packet_pattern_type_Beacon:
            return ( ( Type >= E_ttc_packet_type_802154_Beacon_000010 ) && ( Type < E_ttc_packet_type_802154_beacon ) );
            break;
        case E_ttc_packet_pattern_type_Command:
            return ( ( Type >= E_ttc_packet_type_802154_Command_000010 ) && ( Type < E_ttc_packet_type_802154_command ) );
            break;
        case E_ttc_packet_pattern_type_Data:
            return ( ( Type >= E_ttc_packet_type_802154_Data_000010 ) && ( Type < E_ttc_packet_type_802154_data ) );
            break;
        default:
            Assert_PACKET( 0, ttc_assert_origin_auto ); // unknown/ unsupported packet type given. Check implementation of caller or add a new entry here!
            break;
    }
    return FALSE;
}
t_u16                       packet_802154_payload_get_size( t_ttc_packet* Packet ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    // first byte of 802.15.4 packet stores size of whole packet without length byte
    // size of packet header depends on packet type
    t_u8 HeaderSize  = packet_802154_mac_header_get_size( Packet );
    if ( !HeaderSize ) // packet seems to be invalid!
    { return 0; }

    Assert_PACKET_EXTRA( Packet->Meta.MaxSizeMAC > 0, ttc_assert_origin_auto );  // field not initialized!

    if ( Packet->MAC.Length ) { // packet stores length: use it to calulcate payload size
        Assert_PACKET_EXTRA( Packet->MAC.Length > HeaderSize, ttc_assert_origin_auto );  // packet is smaller than its header. Check packet_802154 implementation!
        return Packet->MAC.Length
#if TTC_PACKET_SOCKET_BYTE==1
               - sizeof( Packet->MAC.packet_802154_data_000010.Payload.SocketID ) // first byte of payload stores socket identifier
#endif
               - HeaderSize        // MAC header
               - 2;                // Frame Checksum
    }
    return 0;  // packet length not set
}
t_u16                       packet_802154_payload_get_size_max( t_ttc_packet* Packet ) {
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)


    // Every packet belongs to a memory pool and carries a special header at negative offset
    const t_ttc_heap_block_from_pool* MemoryBlock = ( ( t_ttc_heap_block_from_pool* ) Packet ) - 1;

    // size of packet header depends on packet type
    t_u8 HeaderSize  = packet_802154_mac_header_get_size( Packet );

    // calculate maximum allowed payload size
    t_u16 MaxPayloadSize = MemoryBlock->Pool->BlockSize
                           - sizeof( Packet->Meta )        // Meta Header
#if TTC_PACKET_SOCKET_BYTE==1
                           - sizeof( Packet->MAC.packet_802154_data_000010.Payload.SocketID ) // first byte of payload stores socket identifier
#endif
                           - HeaderSize                    // MAC Header
                           - 2;                            // Frame Checksum


    return MaxPayloadSize;
}
void                        packet_802154_payload_set_size( t_ttc_packet* Packet, t_u16 PayloadSize ) {
#if (TTC_ASSERT_PACKET_EXTRA == 1)
    Assert_PACKET_Writable( Packet, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
#endif

    if ( ( Packet->Meta.Type > E_ttc_packet_type_802154_mhr ) &&
            ( Packet->Meta.Type < E_ttc_packet_type_802154_command )
       ) { // given packet can store payload

        Packet->MAC.Length = ttc_driver_packet_mac_header_get_size( Packet )  // MAC Header
#if TTC_PACKET_SOCKET_BYTE==1
                             + sizeof( Packet->MAC.packet_802154_data_000010.Payload.SocketID ) // first byte of payload stores socket identifier
#endif
                             + PayloadSize                                     // Payload
                             + 2;                                              // Frame Checksum
    }
    else { // given packet has no payload at all
        Assert_PACKET( PayloadSize == 0, ttc_assert_origin_auto ); // this packet type cannot store payload. Choose a different packet type or set PayloadSize=0!
        Packet->MAC.Length = ttc_driver_packet_mac_header_get_size( Packet )  // MAC Header
                             + 2;                                              // Frame Checksum
    }
}
void                        packet_802154_prepare() {

    // first array entry stores enum offset value
    packet_802154_InitialFCF_Offset = packet_802154_InitialFCF[0];
    Assert_PACKET_EXTRA( packet_802154_InitialFCF_Offset < sizeof( packet_802154_InitialFCF ) / 2, ttc_assert_origin_auto );  // invalid value in first array element. First array element must store enum value before corresponding element of first initial FCF value!

#if (TTC_ASSERT_PACKET_EXTRA == 1) // check structure definitions

    // Check individual array boundary markers
    // If one of the tests below fails, then fields have been inserted to/ deleted from e_ttc_packet_architecture without updating packet_802154_InitialFCF[]
    Assert_PACKET( packet_802154_InitialFCF[E_ttc_packet_type_802154_mhr     - packet_802154_InitialFCF_Offset] == E_ttc_packet_type_802154_mhr,     ttc_assert_origin_auto );
    Assert_PACKET( packet_802154_InitialFCF[E_ttc_packet_type_802154_data    - packet_802154_InitialFCF_Offset] == E_ttc_packet_type_802154_data,    ttc_assert_origin_auto );
    Assert_PACKET( packet_802154_InitialFCF[E_ttc_packet_type_802154_command - packet_802154_InitialFCF_Offset] == E_ttc_packet_type_802154_command, ttc_assert_origin_auto );
    Assert_PACKET( packet_802154_InitialFCF[E_ttc_packet_type_802154_beacon  - packet_802154_InitialFCF_Offset] == E_ttc_packet_type_802154_beacon,  ttc_assert_origin_auto );


    //{ check definitions of all initial FCF values to match their mode
    // declare variables as volatile to ensure their visibility in debugger
    volatile t_ttc_packet                 Packet;
    volatile u_packet_802154_fcf          FCF;
    volatile e_ttc_packet_category        PacketCategory;
    volatile e_ttc_packet_type            PacketType;
    volatile e_ttc_packet_address_format  PacketAddressFormat; ( void ) PacketAddressFormat;

    if ( TTC_ASSERT_PACKET ) { // perform self test to see if enum still fits into Type field
        Packet.Meta.Type = -1; // load maximum positive value
        Assert_PACKET( Packet.Meta.Type > E_ttc_packet_type_802154_unknown, ttc_assert_origin_auto ); // maximum packet type number does not fit into variable. Increase size of Type field or move E_ttc_packet_type_802154_* entries in e_ttc_packet_type towards smaller values!
    }

    FCF.Word = 0;
    FCF.Bits.FrameVersion = packet_fv_802154;
    ttc_memory_set( ( void* ) &Packet, 0, sizeof( Packet ) );

    // data packets
    FCF.Bits.FrameType         = packet_ft_data;
    FCF.Bits.SecurityEnabled   = 0;

    FCF.Bits.PanID_Compression = 0;
    FCF.Bits.DstAddressType    = 0b10;
    FCF.Bits.SrcAddressType    = 0b00;
    // 00 01 10 000 0 0 0 0 001 = 0b0001100000000001
    // 10 01 10 000 1 0 0 0 001 FCF.Word
    // 10 01 10 0010000001

    Assert_PACKET( FCF.Word == PACKET_802154_001000_DATA_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                              // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Data, ttc_assert_origin_auto );                                           // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Data_001000, ttc_assert_origin_auto );                       // wrong type identified
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Target16_Targetpan16, ttc_assert_origin_auto );                              // wrong address format identified

    FCF.Bits.PanID_Compression = 0;
    FCF.Bits.DstAddressType    = 0b10;
    FCF.Bits.SrcAddressType    = 0b10;
    Assert_PACKET( FCF.Word == PACKET_802154_001010_DATA_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Data, ttc_assert_origin_auto );                                           // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Data_001010, ttc_assert_origin_auto );                       // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Source16_Target16_Sourcepan16_Targetpan16, ttc_assert_origin_auto );         // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression = 0;
    FCF.Bits.DstAddressType    = 0b11;
    FCF.Bits.SrcAddressType    = 0b00;
    Assert_PACKET( FCF.Word == PACKET_802154_001100_DATA_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Data, ttc_assert_origin_auto );                                           // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Data_001100, ttc_assert_origin_auto );                       // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Target64_Targetpan16, ttc_assert_origin_auto );                              // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression = 0;
    FCF.Bits.DstAddressType    = 0b11;
    FCF.Bits.SrcAddressType    = 0b10;
    Assert_PACKET( FCF.Word == PACKET_802154_001110_DATA_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Data, ttc_assert_origin_auto );                                           // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Data_001110, ttc_assert_origin_auto );                       // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Source16_Target64_Sourcepan16_Targetpan16, ttc_assert_origin_auto );         // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression = 0;
    FCF.Bits.DstAddressType    = 0b11;
    FCF.Bits.SrcAddressType    = 0b11;
    Assert_PACKET( FCF.Word == PACKET_802154_001111_DATA_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Data, ttc_assert_origin_auto );                                           // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Data_001111, ttc_assert_origin_auto );                       // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Source64_Target64_Sourcepan16_Targetpan16, ttc_assert_origin_auto );         // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression =    1;
    FCF.Bits.DstAddressType    = 0b10;
    FCF.Bits.SrcAddressType    = 0b00;
    Assert_PACKET( FCF.Word == PACKET_802154_011000_DATA_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Data, ttc_assert_origin_auto );                                           // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Data_011000, ttc_assert_origin_auto );                       // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Target16_Targetpan16, ttc_assert_origin_auto );                              // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression =    1;
    FCF.Bits.DstAddressType    = 0b11;
    FCF.Bits.SrcAddressType    = 0b00;
    Assert_PACKET( FCF.Word == PACKET_802154_011100_DATA_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Data, ttc_assert_origin_auto );                                           // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Data_011100, ttc_assert_origin_auto );                       // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Target64_Targetpan16, ttc_assert_origin_auto );                              // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression =    1;
    FCF.Bits.DstAddressType    = 0b10;
    FCF.Bits.SrcAddressType    = 0b10;
    Assert_PACKET( FCF.Word == PACKET_802154_011010_DATA_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Data, ttc_assert_origin_auto );                                           // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Data_011010, ttc_assert_origin_auto );                       // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Source16_Target16_Targetpan16, ttc_assert_origin_auto );                     // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression =    1;
    FCF.Bits.DstAddressType    = 0b11;
    FCF.Bits.SrcAddressType    = 0b00;
    Assert_PACKET( FCF.Word == PACKET_802154_011100_DATA_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Data, ttc_assert_origin_auto );                                           // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Data_011100, ttc_assert_origin_auto );                       // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Target64_Targetpan16, ttc_assert_origin_auto );                              // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression = 1;
    FCF.Bits.DstAddressType    = 0b11;
    FCF.Bits.SrcAddressType    = 0b10;
    Assert_PACKET( FCF.Word == PACKET_802154_011110_DATA_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Data, ttc_assert_origin_auto );                                           // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Data_011110, ttc_assert_origin_auto );                       // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Source16_Target64_Targetpan16, ttc_assert_origin_auto );                     // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression =    1;
    FCF.Bits.DstAddressType    = 0b11;
    FCF.Bits.SrcAddressType    = 0b11;
    Assert_PACKET( FCF.Word == PACKET_802154_011111_DATA_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Data, ttc_assert_origin_auto );                                           // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Data_011111, ttc_assert_origin_auto );                       // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Source64_Target64_Targetpan16, ttc_assert_origin_auto );                     // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    // Command Packets
    FCF.Bits.FrameType         = packet_ft_command;
    FCF.Bits.SecurityEnabled   = 0;

    FCF.Bits.PanID_Compression = 0;
    FCF.Bits.DstAddressType    = 0b10;
    FCF.Bits.SrcAddressType    = 0b00;
    Assert_PACKET( FCF.Word == PACKET_802154_001000_COMMAND_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Command, ttc_assert_origin_auto );                                        // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Command_001000, ttc_assert_origin_auto );                    // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Target16_Targetpan16, ttc_assert_origin_auto );                              // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression = 0;
    FCF.Bits.DstAddressType    = 0b10;
    FCF.Bits.SrcAddressType    = 0b10;
    Assert_PACKET( FCF.Word == PACKET_802154_001010_COMMAND_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Command, ttc_assert_origin_auto );                                        // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Command_001010, ttc_assert_origin_auto );                    // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Source16_Target16_Sourcepan16_Targetpan16, ttc_assert_origin_auto );         // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression = 0;
    FCF.Bits.DstAddressType    = 0b11;
    FCF.Bits.SrcAddressType    = 0b00;
    Assert_PACKET( FCF.Word == PACKET_802154_001100_COMMAND_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Command, ttc_assert_origin_auto );                                        // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Command_001100, ttc_assert_origin_auto );                    // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Target64_Targetpan16, ttc_assert_origin_auto );                              // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression = 0;
    FCF.Bits.DstAddressType    = 0b11;
    FCF.Bits.SrcAddressType    = 0b11;
    Assert_PACKET( FCF.Word == PACKET_802154_001111_COMMAND_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Command, ttc_assert_origin_auto );                                        // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Command_001111, ttc_assert_origin_auto );                    // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Source64_Target64_Sourcepan16_Targetpan16, ttc_assert_origin_auto );         // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression =    1;
    FCF.Bits.DstAddressType    = 0b10;
    FCF.Bits.SrcAddressType    = 0b00;
    Assert_PACKET( FCF.Word == PACKET_802154_011000_COMMAND_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Command, ttc_assert_origin_auto );                                        // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Command_011000, ttc_assert_origin_auto );                    // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Target16_Targetpan16, ttc_assert_origin_auto );                              // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression =    1;
    FCF.Bits.DstAddressType    = 0b10;
    FCF.Bits.SrcAddressType    = 0b10;
    Assert_PACKET( FCF.Word == PACKET_802154_011010_COMMAND_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Command, ttc_assert_origin_auto );                                        // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Command_011010, ttc_assert_origin_auto );                    // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Source16_Target16_Targetpan16, ttc_assert_origin_auto );                     // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression =    1;
    FCF.Bits.DstAddressType    = 0b11;
    FCF.Bits.SrcAddressType    = 0b00;
    Assert_PACKET( FCF.Word == PACKET_802154_011100_COMMAND_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Command, ttc_assert_origin_auto );                                        // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Command_011100, ttc_assert_origin_auto );                    // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Target64_Targetpan16, ttc_assert_origin_auto );                              // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression =    1;
    FCF.Bits.DstAddressType    = 0b11;
    FCF.Bits.SrcAddressType    = 0b11;
    Assert_PACKET( FCF.Word == PACKET_802154_011111_COMMAND_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Command, ttc_assert_origin_auto );                                        // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Command_011111, ttc_assert_origin_auto );                    // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Source64_Target64_Targetpan16, ttc_assert_origin_auto );                     // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression =    1;
    FCF.Bits.DstAddressType    = 0b11;
    FCF.Bits.SrcAddressType    = 0b00;
    Assert_PACKET( FCF.Word == PACKET_802154_011100_COMMAND_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Command, ttc_assert_origin_auto );                                        // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Command_011100, ttc_assert_origin_auto );                    // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Target64_Targetpan16, ttc_assert_origin_auto );                              // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression =    1;
    FCF.Bits.DstAddressType    = 0b11;
    FCF.Bits.SrcAddressType    = 0b11;
    Assert_PACKET( FCF.Word == PACKET_802154_011111_COMMAND_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory   == Packet.Meta.Category, ttc_assert_origin_auto );                                  // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory   == E_ttc_packet_category_Command, ttc_assert_origin_auto );                                           // wrong packet category identified
    Assert_PACKET( Packet.Meta.Type == E_ttc_packet_type_802154_Command_011111, ttc_assert_origin_auto );                       // check implementation of packet_802154_identify()!
    Assert_PACKET( Packet.Meta.AddressFormat == E_ttc_packet_address_Source64_Target64_Targetpan16, ttc_assert_origin_auto );               // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    // Beacon Packets
    FCF.Bits.FrameType         = packet_ft_beacon;
    FCF.Bits.SecurityEnabled   = 0;

    FCF.Bits.PanID_Compression = 0;
    FCF.Bits.DstAddressType    = 0b00;
    FCF.Bits.SrcAddressType    = 0b10;
    Assert_PACKET( FCF.Word == PACKET_802154_000010_BEACON_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Beacon, ttc_assert_origin_auto );                                         // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Beacon_000010, ttc_assert_origin_auto );                     // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Source16_Sourcepan16, ttc_assert_origin_auto );                          // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    FCF.Bits.PanID_Compression = 0;
    FCF.Bits.DstAddressType    = 0b00;
    FCF.Bits.SrcAddressType    = 0b11;
    Assert_PACKET( FCF.Word == PACKET_802154_000011_BEACON_INITIAL_FCF, ttc_assert_origin_auto );
    Packet.MAC.packet_802154_generic.FCF.Word = FCF.Word;
    PacketCategory      = packet_802154_identify( ( t_ttc_packet* ) &Packet );
    PacketType          = Packet.Meta.Type;
    PacketAddressFormat = Packet.Meta.AddressFormat;

    // if any of these asserts fail: check implementation of packet_802154_identify()!
    Assert_PACKET( PacketCategory      == Packet.Meta.Category, ttc_assert_origin_auto );                               // return value must be the same as Packet.Meta.Category
    Assert_PACKET( PacketCategory      == E_ttc_packet_category_Beacon, ttc_assert_origin_auto );                                         // wrong packet category identified
    Assert_PACKET( PacketType          == E_ttc_packet_type_802154_Beacon_000011, ttc_assert_origin_auto );                     // check implementation of packet_802154_identify()!
    Assert_PACKET( PacketAddressFormat == E_ttc_packet_address_Source64_Targetpan16, ttc_assert_origin_auto );                            // wrong address format identified
    _packet_802154_test_initialize( &Packet, PacketCategory, PacketType, FCF.Word );

    //}

#endif
}
t_u8                        packet_802154_sequence_number_get( t_ttc_packet* Packet ) {
    Assert_PACKET_EXTRA_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    return ( t_u8 ) Packet->MAC.packet_802154_generic.SequenceNo;
}
void                        packet_802154_sequence_number_set( t_ttc_packet* Packet, t_u8 SequenceNo ) {
    Assert_PACKET_EXTRA_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)


    Packet->MAC.packet_802154_generic.SequenceNo = SequenceNo;
}
e_ttc_packet_type           packet_802154_type_get( t_ttc_packet* Packet ) {
    Assert_PACKET_EXTRA_Writable( Packet, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    if ( !Packet->Meta.Type ) { // missing packet type in meta block: identify packet
        packet_802154_identify( Packet );
    }

    return ( e_ttc_packet_address_format ) Packet->Meta.Type;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

#if (TTC_ASSERT_PACKET_EXTRA == 1)
void _packet_802154_test_initialize( volatile t_ttc_packet* Packet, e_ttc_packet_category Category, e_ttc_packet_type Type, t_u16 InitialFCF ) {

    packet_802154_initialize( ( t_ttc_packet* ) Packet, Type );
    packet_802154_identify( ( t_ttc_packet* ) Packet );

    Assert_PACKET( Category   == Packet->Meta.Category,                      ttc_assert_origin_auto );
    Assert_PACKET( Type       == Packet->Meta.Type,                          ttc_assert_origin_auto );
    Assert_PACKET( InitialFCF == Packet->MAC.packet_802154_generic.FCF.Word, ttc_assert_origin_auto );
}
#endif
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
