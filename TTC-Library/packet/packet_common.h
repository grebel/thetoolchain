#ifndef packet_common_h
#define packet_common_h

/** packet_common.h *****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to packet low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.h revision 12 at 20150928 09:30:04 UTC
 *
 *  Authors: Gregor Rebel
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_PACKET_COMMON
//
// Implementation status of low-level driver support for packet devices on common
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_PACKET_COMMON '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_PACKET_COMMON == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_PACKET_COMMON to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_PACKET_COMMON

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "packet_802154.c"
 */

#include "../ttc_packet_types.h" // will include packet_common_types.h, packet_802154_types.h (do not include them directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_packet_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_packet_foo
 */

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions
/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_packet.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_packet.h for details.
 *
 * You normally should not need to add functions here.
 * Global feature functions must be added in ttc_packet.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl packet UPDATE".
 */

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //packet_common_h
