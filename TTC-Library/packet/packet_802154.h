#ifndef PACKET_802154_H
#define PACKET_802154_H

/** { packet_802154.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for data packets using IEEE 802.15.4-2011 standard.
 *  Structures, Enums and Defines being required by low-level driver only.
 *
 *  Created from template device_architecture.h revision 25 at 20150928 09:30:04 UTC
 *
 *  Note: See ttc_packet.h for description of 802154 independent PACKET implementation.
 *
 *  Authors: Gregor Rebel
 *
 *
 *  Basic handling of IEEE 802.15.4-2011 compliant data packets for
 *  Low Rate Wireless Personal Area Networks (WPANs).
 *  See IEEE 802.15.4-2011 standard for more details.
 *
 *  The basic MAC header format is defined in packet_802154_mhr_EPDDSS_t named structs.
 *  The different types of MAC headers are distuinguished by their binary EPDDSS infix.
 *
 *  EPDDSS bits
 *    E  = Enable Security
 *    P  = PanID Compression
 *    DD = Destination Address Type (0b10 = 16 bit, 0b11 = 64 bit)
 *    SS = Source      Address Type (0b10 = 16 bit, 0b11 = 64 bit)
 *
 *  MAC headers are then used to build the three MAC types of 802.15.4 packets
 *  1) MAC DATA
 *  2) MAC COMMAND
 *  3) MAC BEACON
 *
 *  Acknowledge packets do not carry a MAC header. The receiver identfies them by their Sequence Number.
 *  4) ACKNOWLEDGE
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_PACKET_DRIVER_AVAILABLE
#define EXTENSION_PACKET_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_SOFTWARE_DOUBLE_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_PACKET_802154
//
// Implementation status of low-level driver support for packet devices on 802154
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_PACKET_802154 '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_PACKET_802154 == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_PACKET_802154 to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_PACKET_802154

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "packet_802154.c"
//
#include "../ttc_packet_types.h" // will include packet_802154_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_packet_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_packet_foo
//
#define ttc_driver_packet_acknowledge                                                                packet_802154_acknowledge
#define ttc_driver_packet_acknowledge_request_get                                                    packet_802154_acknowledge_request_get
#define ttc_driver_packet_acknowledge_request_set                                                    packet_802154_acknowledge_request_set
#define ttc_driver_packet_address_reply2                                                             packet_802154_address_reply2
#define ttc_driver_packet_address_source_get_pointer(Packet, Pointer2SourceID, Pointer2SourcePanID)  packet_802154_get_pointer_source(Packet, Pointer2SourceID, Pointer2SourcePanID)
#define ttc_driver_packet_address_target_get_pointer(Packet, Pointer2TargetID, Pointer2TargetPanID)  packet_802154_get_pointer_target(Packet, Pointer2TargetID, Pointer2TargetPanID)
#define ttc_driver_packet_identify(Packet)                                                           packet_802154_identify(Packet)
#define ttc_driver_packet_initialize(Packet, Type)                                                   packet_802154_initialize(Packet, Type)
#define ttc_driver_packet_is_acknowledge                                                             packet_802154_is_acknowledge
#define ttc_driver_packet_is_beacon                                                                  packet_802154_is_beacon
#define ttc_driver_packet_is_command                                                                 packet_802154_is_command
#define ttc_driver_packet_is_data                                                                    packet_802154_is_data
#define ttc_driver_packet_mac_header_get_size(Packet)                                                packet_802154_mac_header_get_size(Packet)
#define ttc_driver_packet_mac_header_get_type_size                                                   packet_802154_mac_header_get_type_size
#define ttc_driver_packet_pattern_matches_type                                                       packet_802154_pattern_matches_type
#define ttc_driver_packet_prepare()                                                                  packet_802154_prepare()
#define ttc_driver_packet_payload_get_pointer(Packet, PayloadPtr, SizePtr, SocketPtr)              packet_802154_payload_get_pointer(Packet, PayloadPtr, SizePtr, SocketPtr)
#define ttc_driver_packet_payload_get_size(Packet)                                                   packet_802154_payload_get_size(Packet)
#define ttc_driver_packet_payload_get_size_max(Packet)                                               packet_802154_payload_get_size_max(Packet)
#define ttc_driver_packet_payload_set_size(Packet, PayloadSize)                                      packet_802154_payload_set_size(Packet, PayloadSize)
#define ttc_driver_packet_type_get                                                                   packet_802154_type_get
#define ttc_driver_packet_sequence_number_get                                                        packet_802154_sequence_number_get
#define ttc_driver_packet_sequence_number_set                                                        packet_802154_sequence_number_set

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

// these macros are provided by ttc_packet_interface
#undef ttc_driver_packet_address_source_compare
#undef ttc_driver_packet_address_source_get
#undef ttc_driver_packet_address_source_set
#undef ttc_driver_packet_address_target_compare
#undef ttc_driver_packet_address_target_get
#undef ttc_driver_packet_address_target_set

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_packet.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_packet.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Prepares packet Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void packet_802154_prepare();

/** Calculates current size of payload in given packet
 *
 * Each IEEE 802.15.4 type packet stores size of whole packet in first byte.
 * Payload area starts behind the 802.15.4 header. The size of this header depends on
 * the current packet type.
 *
 * @param Packet      radio packet being returned by ttc_radio_packet_get_empty() before
 * @param PayloadPtr  pointer to variable to load with pointer to payload area
 * @param SizePtr     ==NULL: Not used; !=NULL: if (Packet->MAC.Length==0) { *SizePtr = Available payload size; } else { *SizePtr = Used payload size }
 * @param SocketPtr ==NULL: Not used; !=NULL: store socket identifier from first byte of payload in this variable (requires TTC_PACKET_SOCKET_BYTE defined ad 1)
 * @return            ==0: payload could be found successfully; errorcode otherwise
 */
t_u8 packet_802154_payload_get_pointer( t_ttc_packet* Packet, t_u8** PayloadPtr, t_u16* SizePtr, e_ttc_packet_pattern* SocketPtr );

/** Calculates current size of payload in given packet
 *
 * Each IEEE 802.15.4 type packet stores size of whole packet in first byte.
 * Payload area starts behind the 802.15.4 header. The size of this header depends on
 * the current packet type.
 *
 * @param Packet  radio packet being returned by ttc_radio_packet_get_empty() before
 * @return        Size of payload area in given packet
 */
t_u16 packet_802154_payload_get_size( t_ttc_packet* Packet );

/** Calculates size of payload buffer in given packet
 *
 * Packet buffers are stored in memory pools of equal sized memory blocks allocated from heap.
 * Depending on its type, the packet header size varies. The remaining space in the memory
 * block can be used as payload area.
 *
 * @param Packet  radio packet being returned by ttc_radio_packet_get_empty() before
 * @return        Maximum available size of payload area in given packet
 */
t_u16 packet_802154_payload_get_size_max( t_ttc_packet* Packet );

/** Reads in 16 or 64 bit source address from given packet
 *
 * Some 802.15.4 packets store 64-bit hardware addresses while others store short 16-bit protocol adresses.
 * This function finds start address of 16- or 64-bit source address in given packet and returns found type.
 * You may then read or change this address in the packet.
 *
 * @param  Packet               any supported packet type containing 16- or 64-bit addresses
 * @param  Pointer2SourceID     !=NULL: will be loaded with pointer to start of SourceID  inside given Packet
 * @param  Pointer2SourcePanID  !=NULL: will be loaded with pointer to start of SourcePanID inside given Packet
 * @return                      ==0: no address found in packet (packet type not supported); ==ttc_packet_address_Source16: Address16 read; ==ttc_packet_address_Source64: Address64 read
 */
e_ttc_packet_address_format packet_802154_get_pointer_source( t_ttc_packet* Packet, void** Pointer2SourceID, t_u16** Pointer2SourcePanID );

/** Reads in 16 or 64 bit target address from given packet
 *
 * Some 802.15.4 packets store 64-bit hardware addresses while others store short 16-bit protocol adresses.
 * This function finds start address of 16- or 64-bit target address in given packet and returns found type.
 * You may then read or change this address in the packet.
 *
 * @param  Packet               any supported packet type containing 16- or 64-bit addresses
 * @param  Pointer2TargetID     !=NULL: will be loaded with pointer to start of SourceID  inside given Packet
 * @param  Pointer2TargetPanID  !=NULL: will be loaded with pointer to start of SourcePanID inside given Packet
 * @return                      ==0: no address found in packet (packet type not supported); ==ttc_packet_address_Target16: Address16 read; ==ttc_packet_address_Target64: Address64 read
 */
e_ttc_packet_address_format packet_802154_get_pointer_target( t_ttc_packet* Packet, void** Pointer2TargetID, t_u16** Pointer2TargetPanID );

/** Finds out IEEE 802.15.4 packet type from given binary packet
 *
 * Identification has to be done once for each received packet. Once identified,
 * the unique identification number Packet->Info.Type can be used for fast switch
 * clauses.
 *
 * Note: identified packet type is also stored in Packet->Info.Type
 *
 * @param Packet  binary packet being received from radio
 * @return        protocol independent type of packet (also stored in Packet->Meta.Category)
 */
e_ttc_packet_category packet_802154_identify( t_ttc_packet* Packet );

/** Fills basic header fields of given packet according to given packet type
 *
 * @param Packet      network packet being returned by ttc_packet_get_empty_packet() before
 * @param Type        type of packet that is to be created (-> e_ttc_packet_type)
 * @return            size of initialized header (amount of bytes occupied by header at begin of packet)
 */
t_u8 packet_802154_initialize( t_ttc_packet* Packet, e_ttc_packet_type Type );

/** Returns size of header of given packet
 *
 * Each packet type defines its own header with a unique combination of fields.
 * The low-level driver knows all packet types and their corresponding header struct.
 * The header size may be used to calculate the minimum size of a memory block for a certain payload.
 * You may also use TTC_PACKET_MAX_HEADER_SIZE to calculate your memory usage in advance.
 *
 * @param Packet  network packet being returned by ttc_packet_get_empty_packet() before
 * @return       >0: Size of header of given Packet; ==0: packet is invalid and should not be used!
 */
t_u8 packet_802154_mac_header_get_size( t_ttc_packet* Packet );



/** Set size of payload being loaded into packet
 *
 * Call this function to set total packet length according to size of its payload
 * @param Packet       (t_ttc_packet*)  network packet being returned by ttc_packet_get_empty_packet() before
 * @param PayloadSize  (t_u16)          actual size of used payload (0.. ttc_packet_payload_get_size_max(Packet))
 */
void packet_802154_payload_set_size( t_ttc_packet* Packet, t_u16 PayloadSize );

/** Creates an acknowledge packet for given packet
 *
 * When a received packet requests an acknowledge (ttc_packet_acknowledge_request_get(Packet)==TRUE)
 * then this function can initialize a given packet buffer as a matching acknowledge packet.
 *
 * @param Packet    (t_ttc_packet*)     received network packet to inspect
 * @param PacketACK (t_ttc_packet*)     allocated memory buffer will be initialized with acknowledgement packet
 */
void packet_802154_acknowledge( t_ttc_packet* Packet, t_ttc_packet* PacketACK );

/** Returns flag acknowledge requested from header of given packet
 *
 * Most protocols define this flag to realize 2-way or 3-way handshakes for safe data transmission.
 *
 * @param Packet  (t_ttc_packet*)  initialized network packet
 * @return       (BOOL)            == TRUE: source node is requesting to send an acknowledge packet
 */
BOOL packet_802154_acknowledge_request_get( t_ttc_packet* Packet );


/** Set flag acknowledge requested in given packet
 *
 * Most protocols define this flag to realize 2-way or 3-way handshakes for safe data transmission.
 *
 * @param Packet  (t_ttc_packet*)  initialized network packet
 * @param RequestACK (BOOL)        == TRUE: request acknowledge packet from target node
 */
void packet_802154_acknowledge_request_set( t_ttc_packet* Packet, BOOL RequestACK );



/** Returns type of given packet
 *
 * @param Packet  (t_ttc_packet*)     network packet to inspect
 * @return        (e_ttc_packet_type) identified type
 */
e_ttc_packet_type packet_802154_type_get( t_ttc_packet* Packet );


/** Creates reply packet from given .
 *
 * This function comes handy when you use a received packet to prepare and answer.
 * It will copy the source identfier into the target identifier of a given second packet and set the given source identifier.
 * The low-level driver may copy extra informations like sequence number.
 *
 * @param  PacketRX     any received packet of supported type containing a 16- or 64-bit addresses
 * @param  PacketReply  fresh, empty packet which will be initialized as reply to PacketRX
 * @param  NewSourceID  !=NULL: pointer to 16- or 64-bit identifier to be written into source identifier; ==NULL: PacketRX.TargetID is used as Source ID
 */
void packet_802154_address_reply2( t_ttc_packet* Packet, t_ttc_packet* PacketReply );


/** Checks if given packet belongs to general class of acknowledge packets
 *
 * The actual check is implemented by current low-level driver.
 * Some ttc_packet low-level drivers may not know of acknowledge type packets.
 *
 * @param Packet  (t_ttc_packet*)     network packet to inspect
 * @return        (BOOL)              ==TRUE: given packet is an acknowledge type packet; ==FALSE: otherwise
 */
BOOL packet_802154_is_acknowledge( t_ttc_packet* Packet );


/** Checks if given packet belongs to general class of beacon packets
 *
 * The actual check is implemented by current low-level driver.
 * Some ttc_packet low-level drivers may not know of beacon type packets.
 *
 * @param Packet  (t_ttc_packet*)     network packet to inspect
 * @return        (BOOL)              ==TRUE: given packet is an beacon type packet; ==FALSE: otherwise
 */
BOOL packet_802154_is_beacon( t_ttc_packet* Packet );


/** Checks if given packet belongs to general class of command packets
 *
 * The actual check is implemented by current low-level driver.
 * Some ttc_packet low-level drivers may not know of command type packets.
 *
 * @param Packet  (t_ttc_packet*)     network packet to inspect
 * @return        (BOOL)              ==TRUE: given packet is a command type packet; ==FALSE: otherwise
 */
BOOL packet_802154_is_command( t_ttc_packet* Packet );


/** Checks if given packet belongs to general class of data packets
 *
 * The actual check is implemented by current low-level driver.
 * Some ttc_packet low-level drivers may not know of data type packets.
 *
 * @param Packet  (t_ttc_packet*)     network packet to inspect
 * @return        (BOOL)              ==TRUE: given packet is a data type packet; ==FALSE: otherwise
 */
BOOL packet_802154_is_data( t_ttc_packet* Packet );



/** Reads sequence number from given packet
 *
 * Some packet formats (E.g. IEEE 802.15.4) store a special key in each packet
 * to identify to which packet a reply corresponds to.
 *
 * @param Packet  (t_ttc_packet*)     network packet to inspect
 * @return       (t_u8)               Sequence number read from packet header
 */
t_u8 packet_802154_sequence_number_get( t_ttc_packet* Packet );


/** Writes given sequence number into given packet
 *
 * Some packet formats (E.g. IEEE 802.15.4) store a special key in each packet
 * to identify to which packet a reply corresponds to.
 *
 * @param Packet      (t_ttc_packet*)     network packet to inspect
 * @param SequenceNo  (t_u8)              sequence number to write into packet header
 */
void packet_802154_sequence_number_set( t_ttc_packet* Packet, t_u8 SequenceNo );


/** Checks if given packet pattern matches given packet type
 *
 * This is a helper function for ttc_packet_pattern_matches().
 * You may want to call it when checking multiple patterns agains same packet.
 *
 * Example Usage:
 *    // create + initialize variables
 *    t_ttc_packet* Packet;
 *    e_ttc_packet_pattern Pattern1;
 *    e_ttc_packet_pattern Pattern2;
 *    ...
 *
 *    // extracting packet type once
 *    e_ttc_packet_type Type = E_ttc_packet_type_get( Packet );
 *
 *    // checking multiple patterns agains same packet type
 *    BOOL Matches = ttc_packet_pattern_matches_type(Type, Pattern1) ||
 *                   ttc_packet_pattern_matches_type(Type, Pattern2) ||
 *                   ...;
 *
 * Note: This function can only check E_ttc_packet_pattern_type_* patterns.
 *       For E_ttc_packet_pattern_socket_* patterns, you have to use ttc_packet_pattern_matches_socket() instead!
 *
 * @param Type     (e_ttc_packet_type )    type of network packet ( return value from E_ttc_packet_type_get() )
 * @param Pattern  (e_ttc_packet_pattern)  ==0: match any type; E_ttc_packet_pattern_type_Begin ... E_ttc_packet_pattern_type_Begin_AnyOf
 * @return         (BOOL)                  ==TRUE: given packet type matches to given type pattern; FALSE otherwise
 */
BOOL packet_802154_pattern_matches_type( e_ttc_packet_type Type, e_ttc_packet_pattern Pattern );


/** Returns size of header of packets of given type.
 *
 * Each packet type defines its own header with a unique combination of fields.
 * The low-level driver knows all packet types and their corresponding header struct.
 * The header size may be used to calculate the minimum size of a memory block for a certain payload.
 * You may also use TTC_PACKET_MAX_HEADER_SIZE to calculate your maximum memory usage in advance.
 *
 * @param Type (e_ttc_packet_type)  valid network packet type
 * @return     (t_u8)               Size of header of given Packet
 */
t_u8 packet_802154_mac_header_get_type_size( e_ttc_packet_type Type );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //PACKET_802154_H
