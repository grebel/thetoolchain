#ifndef PACKET_802154_TYPES_H
#define PACKET_802154_TYPES_H

/** { packet_802154.h **********************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for data packets using IEEE 802.15.4-2011 standard.
 *  Structures, Enums and Defines being required by ttc_packet_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150928 09:30:04 UTC
 *
 *  Note: See packet_802154.h for description of 802154 packet implementation.
 *  Note: See ttc_packet.h for description of architecture independent packet implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *******************************************************************

#ifndef TTC_PACKET_TYPES_H
    #  error Do not include packet_common_types.h directly. Include ttc_packet_types.h instead!
#endif
#include "../ttc_basic_types.h"
#include "compile_options.h"
#include "packet_common_types.h" // will not include anything, because packet_common_types.h is always included from ttc_packet_types.h before including us. This line helps IDE to "know" all defines in packet_common_types.h.
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines/ TypeDefs **********************************************************

/** Some radios provide ranging feature like 802.15.4a compliant Low Rate Ultra Wideband (e.g. the DW1000 transceiver)
 *
 * Define as 1 in your makefile to enable ranging fields in t_packet_802154_meta:
 *   COMPILE_OPTS += -DTTC_PACKET_RANGING=1
 *
 *
 * You may check for enabled ranging in your C-code like this:
 *   #if TTC_PACKET_RANGING==1
 *     // ranging enabled
 *   #else
 *     // ranging disabled
 *   #endif
 */
#ifndef TTC_PACKET_RANGING
    #define TTC_PACKET_RANGING 0 // nobody defined it so we define it as disabled
#endif

// amount of bits in each packet to use as packet length field
#ifndef PACKET_802154_LENGTH_BITS
    #define PACKET_802154_LENGTH_BITS 8  // default size of length field for 802.15.4
#endif

#ifndef TTC_PACKET1   // device not defined in makefile
    #define TTC_PACKET1    ta_packet_802154   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums required by packet_802154_types.h ************************

typedef enum { // values of u_packet_802154_fcf.Bits.FrameType field
    packet_ft_beacon          = 0b000,
    packet_ft_data            = 0b001,
    packet_ft_acknowledgement = 0b010,
    packet_ft_command         = 0b011,
    packet_ft_reserved1       = 0b100,
    packet_ft_reserved2       = 0b101,
    packet_ft_reserved3       = 0b110,
    packet_ft_reserved4       = 0b111
} e_packet_802154_fcf_frame_type;

typedef enum { // values of u_packet_802154_fcf.Bits.DstAddressType and .SrcAddressType fields
    packet_802154_address_type_00_bits         = 0b00, // address field is empty
    packet_802154_address_type_reserved        = 0b01, //
    packet_802154_address_type_16_bits         = 0b10, // address field is 2 bytes
    packet_802154_address_type_64_bits         = 0b11, // address field is 8 bytes
} e_packet_802154_fcf_address_type;

typedef enum { // values of u_packet_802154_fcf.FrameVersion field
    packet_fv_801254_2003     = 0b00, // packet format is IEEE 802.15.4-2003
    packet_fv_802154          = 0b01, // packet format is IEEE 802.15.4
    packet_fv_reserved1       = 0b10, //
    packet_fv_reserved2       = 0b11, //
} e_packet_802154_fcf_frame_version;

typedef union { // u_packet_802154_fcf -> IEEE802.15.4-2011 p.58 / DW1000 UserManual p.207
    struct {
        unsigned FrameType         : 3;  // -> e_packet_802154_fcf_frame_type
        unsigned SecurityEnabled   : 1;  // ==1: frame is protected by MAC layer encryption (Auxiliary Header Field added to MAC Header)
        unsigned FramePending      : 1;  // ==1: more frames are pending to be sent to same recipient
        unsigned AckRequest        : 1;  // ==1: Acknowledgement is requested from recipient for this packet
        unsigned PanID_Compression : 1;  // ==1: if DstAddressType & SrcAddressType != 0 then only DestinationPanID is present (SourcePanID omitted)
        // ==0: PanID field is present exactly if corresponding address field is present
        unsigned Reserved1         : 3;  //
        unsigned DstAddressType    : 2;  // -> e_packet_802154_fcf_address_type
        unsigned FrameVersion      : 2;  // -> e_packet_802154_fcf_frame_version
        unsigned SrcAddressType    : 2;  // -> e_packet_802154_fcf_address_type
    } __attribute__( ( __packed__ ) ) Bits;
    t_u16 Word;
} u_packet_802154_fcf;

typedef struct s_packet_802154_generic { // IEEE 802.15.4 constant header of all packet types

unsigned Length : PACKET_802154_LENGTH_BITS; // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf         FCF;             // frame control bytes 00-01
    t_u8                        SequenceNo;      // running sequence number
} t_packet_802154_generic;

/** { MAC Headers (MHR) *******************************************************************
 *
 *  MAC Headers define packet type, sequence number, source + destination address and PanIDs.
 * They are used as prefix for data and command packets.
 * Different types of MHRs exist with different adress fields.
 * For each MHR type the initial FCF value is defined.
 *
 * Naming scheme: packet_802154_mhr_EPDDSS_t / struct packet_802154_mhr_EPDDSS_s
 *
 * Format of binary EPDDSS infix
 *    E  = Enable Security
 *    P  = PanID Compression
 *    DD = Destination Address Type (0b10 = 16 bit, 0b11 = 64 bit)
 *    SS = Source      Address Type (0b10 = 16 bit, 0b11 = 64 bit)
 *
 */

//{ MAC Headers without Auxiliary Security Header

typedef struct s_packet_802154_mhr_000010 {                     // MAC Header of type 0b000010
    // Note: Type == 0b000010 means:
    // FCF.Bits.SecurityEnabled      = 0b0
    // FCF.Bits.PanID_Compression    = 0b0
    // FCF.Bits.DstAddressType       = 0b00 (address omitted)
    // FCF.Bits.SrcAddressType       = 0b10 (16-bit address)

unsigned Length : PACKET_802154_LENGTH_BITS;       // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf  FCF;
    t_u8                 SequenceNo;      // running sequence number
    t_u8                 SrcPanID_L;      // source PAN ID (Low-Byte)
    t_u8                 SrcPanID_H;      // source PAN ID (High-Byte)
    t_u8                 SrcNodeID_L;     // source Node ID (Low-Byte)
    t_u8                 SrcNodeID_H;     // source Node ID (High-Byte)
    // t_ttc_packet_payload Payload;      // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_mhr_000010;
#define PACKET_802154_000010_MHR_INITIAL_FCF 0b1001000000000000 // initial value for FCF field for this type
#define PACKET_802154_100010_MHR_INITIAL_FCF PACKET_802154_000010_MHR_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_mhr_000011 {                     // MAC Header of type 0b000011
    // Note: Type == 0b000011 means:
    // FCF.Bits.SecurityEnabled      = 0b0
    // FCF.Bits.PanID_Compression    = 0b0
    // FCF.Bits.DstAddressType       = 0b00 (address omitted)
    // FCF.Bits.SrcAddressType       = 0b11 (64-bit address)

unsigned Length : PACKET_802154_LENGTH_BITS;       // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf  FCF;
    t_u8                 SequenceNo;      // running sequence number
    t_u8                 SrcPanID_L;      // source PAN ID (Low-Byte)
    t_u8                 SrcPanID_H;      // source PAN ID (High-Byte)
    t_u32                SrcNodeID_L;     // source Node ID (Low-Byte)
    t_u32                SrcNodeID_H;     // source Node ID (High-Byte)
    // t_ttc_packet_payload Payload;      // protocol specific payload of this packet
    // t_u16  FCS;          // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_mhr_000011;
#define PACKET_802154_000011_MHR_INITIAL_FCF 0b1101000000000000 // initial value for FCF field for this type
#define PACKET_802154_100011_MHR_INITIAL_FCF PACKET_802154_000011_MHR_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_mhr_001000 {                     // MAC Header of type 0b001000
    // Note: Type == 0b001000 means:
    // FCF.Bits.SecurityEnabled      = 0b0
    // FCF.Bits.PanID_Compression    = 0b0
    // FCF.Bits.DstAddressType       = 0b10 (16-bit address)
    // FCF.Bits.SrcAddressType       = 0b00 (address omitted)

unsigned Length : PACKET_802154_LENGTH_BITS;       // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf  FCF;
    t_u8                 SequenceNo;      // running sequence number
    t_u8                 DestPanID_L;     // destination PAN ID (Low-Byte)
    t_u8                 DestPanID_H;     // destination PAN ID (High-Byte)
    t_u8                 DestNodeID_L;    // destination Node ID (Low-Byte)
    t_u8                 DestNodeID_H;    // destination Node ID (High-Byte)
    // t_ttc_packet_payload Payload;      // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_mhr_001000;
#define PACKET_802154_001000_MHR_INITIAL_FCF 0b0001100000000000 // initial value for FCF field for this type
#define PACKET_802154_101000_MHR_INITIAL_FCF PACKET_802154_001000_MHR_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_mhr_001100 {                     // MAC Header of type 0b001100
    // Note: Type == 0b001100 means:
    // FCF.Bits.SecurityEnabled      = 0b0
    // FCF.Bits.PanID_Compression    = 0b0
    // FCF.Bits.DstAddressType       = 0b11 (64-bit address)
    // FCF.Bits.SrcAddressType       = 0b00 (address omitted)

unsigned Length : PACKET_802154_LENGTH_BITS;       // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf  FCF;
    t_u8                 SequenceNo;      // running sequence number
    t_u8                 DestPanID_L;     // destination PAN ID (Low-Byte)
    t_u8                 DestPanID_H;     // destination PAN ID (High-Byte)
    t_u32                DestNodeID_L;    // destination Node ID (Low-Byte)
    t_u32                DestNodeID_H;    // destination Node ID (High-Byte)
    // t_ttc_packet_payload Payload;      // protocol specific payload of this packet
    // t_u16  FCS;          // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_mhr_001100;
#define PACKET_802154_001100_MHR_INITIAL_FCF 0b0001110000000000 // initial value for FCF field for this type
#define PACKET_802154_101100_MHR_INITIAL_FCF PACKET_802154_001100_MHR_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_mhr_001110 {                     // MAC Header of type 0b001110
    // Note: Type == 0b001110 means:
    // FCF.Bits.SecurityEnabled      = 0b0
    // FCF.Bits.PanID_Compression    = 0b0
    // FCF.Bits.DstAddressType       = 0b11 (64-bit address)
    // FCF.Bits.SrcAddressType       = 0b10 (16-bit address)

unsigned Length : PACKET_802154_LENGTH_BITS;       // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf  FCF;
    t_u8                 SequenceNo;      // running sequence number
    t_u8                 DestPanID_L;     // destination PAN ID (Low-Byte)
    t_u8                 DestPanID_H;     // destination PAN ID (High-Byte)
    t_u32                DestNodeID_L;    // destination Node ID (Low-Byte)
    t_u32                DestNodeID_H;    // destination Node ID (High-Byte)
    t_u8                 SrcPanID_L;      // Source PAN ID (Low-Byte)
    t_u8                 SrcPanID_H;      // Source PAN ID (High-Byte)
    t_u32                SrcNodeID_L;     // Source Node ID (Low-Byte)
    t_u32                SrcNodeID_H;     // Source Node ID (High-Byte)
    // t_ttc_packet_payload Payload;      // protocol specific payload of this packet
    // t_u16  FCS;          // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_mhr_001110;
#define PACKET_802154_001110_MHR_INITIAL_FCF 0b1001110000000000 // initial value for FCF field for this type
#define PACKET_802154_101110_MHR_INITIAL_FCF PACKET_802154_001110_MHR_INITIAL_FCF | 0b1000  // encrypted packet 

/** Note: Type == 0b011100 means:
 * FCF.Bits.SecurityEnabled      = 0b0
 * FCF.Bits.PanID_Compression    = 0b1
 * FCF.Bits.DstAddressType       = 0b11 (64-bit address)
 * FCF.Bits.SrcAddressType       = 0b00 (address omitted)
 */
typedef t_packet_802154_mhr_001100 t_packet_802154_mhr_011100;  // both types have same structure
#define PACKET_802154_011100_MHR_INITIAL_FCF 0b0001110001000000 // initial value for FCF field for this type
#define PACKET_802154_111100_MHR_INITIAL_FCF PACKET_802154_011100_MHR_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_mhr_001010 {                     // MAC Header of type 0b001010
    // Note: Type == 0b001010 means:
    // FCF.Bits.SecurityEnabled      = 0b0
    // FCF.Bits.PanID_Compression    = 0b0
    // FCF.Bits.DstAddressType       = 0b10 (16-bit address)
    // FCF.Bits.SrcAddressType       = 0b10 (16-bit address)

unsigned Length : PACKET_802154_LENGTH_BITS;       // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf  FCF;
    t_u8                 SequenceNo;      // running sequence number
    t_u8                 DestPanID_L;     // destination PAN ID (Low-Byte)
    t_u8                 DestPanID_H;     // destination PAN ID (High-Byte)
    t_u8                 DestNodeID_L;    // destination Node ID (Low-Byte)
    t_u8                 DestNodeID_H;    // destination Node ID (High-Byte)
    t_u8                 SrcPanID_L;      // source PAN ID (Low-Byte)
    t_u8                 SrcPanID_H;      // source PAN ID (High-Byte)
    t_u8                 SrcNodeID_L;     // source Node ID (Low-Byte)
    t_u8                 SrcNodeID_H;     // source Node ID (High-Byte)
    // t_ttc_packet_payload Payload;      // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_mhr_001010;
#define PACKET_802154_001010_MHR_INITIAL_FCF 0b1001100000000000 // initial value for FCF field for this type
#define PACKET_802154_101010_MHR_INITIAL_FCF PACKET_802154_001010_MHR_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_mhr_001111 {                     // MAC Header of type 0b001111
    // Note: Type == 0b001111 means:
    // FCF.Bits.SecurityEnabled      = 0b0
    // FCF.Bits.PanID_Compression    = 0b0
    // FCF.Bits.DstAddressType       = 0b11 (64-bit address)
    // FCF.Bits.SrcAddressType       = 0b11 (64-bit address)

unsigned Length : PACKET_802154_LENGTH_BITS;       // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf  FCF;
    t_u8                 SequenceNo;      // running sequence number
    t_u8                 DestPanID_L;     // destination PAN ID (Low-Byte)
    t_u8                 DestPanID_H;     // destination PAN ID (High-Byte)
    t_u32                DestNodeID_L;    // destination Node ID (Low-Byte)
    t_u32                DestNodeID_H;    // destination Node ID (High-Byte)
    t_u8                 SrcPanID_L;      // source PAN ID (Low-Byte)
    t_u8                 SrcPanID_H;      // source PAN ID (High-Byte)
    t_u32                SrcNodeID_L;     // source Node ID (Low-Byte)
    t_u32                SrcNodeID_H;     // source Node ID (High-Byte)
    // t_ttc_packet_payload Payload;      // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_mhr_001111;
#define PACKET_802154_001111_MHR_INITIAL_FCF 0b1101110000000000 // initial value for FCF field for this type
#define PACKET_802154_101111_MHR_INITIAL_FCF PACKET_802154_001111_MHR_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_mhr_010010 {                     // MAC Header of type 0b010010
    // Note: Type == 0b010010 means:
    // FCF.Bits.SecurityEnabled      = 0b0
    // FCF.Bits.PanID_Compression    = 0b1
    // FCF.Bits.DstAddressType       = 0b00 (address omitted)
    // FCF.Bits.SrcAddressType       = 0b10 (16-bit address)

unsigned Length : PACKET_802154_LENGTH_BITS;       // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf  FCF;
    t_u8                 SequenceNo;      // running sequence number
    t_u8                 DestPanID_L;     // destination PAN ID (Low-Byte)
    t_u8                 DestPanID_H;     // destination PAN ID (High-Byte)
    t_u8                 SrcNodeID_L;     // source Node ID (Low-Byte)
    t_u8                 SrcNodeID_H;     // source Node ID (High-Byte)
    // t_ttc_packet_payload Payload;      // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_mhr_010010;
#define PACKET_802154_010010_MHR_INITIAL_FCF 0b0000000000000000 // initial value for FCF field for this type
#define PACKET_802154_110010_MHR_INITIAL_FCF PACKET_802154_010010_MHR_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_mhr_010011 {                     // MAC Header of type 0b010011
    // Note: Type == 0b010011 means:
    // FCF.Bits.SecurityEnabled      = 0b0
    // FCF.Bits.PanID_Compression    = 0b1
    // FCF.Bits.DstAddressType       = 0b00 (address omitted)
    // FCF.Bits.SrcAddressType       = 0b11 (64-bit address)

unsigned Length : PACKET_802154_LENGTH_BITS;       // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf  FCF;
    t_u8                 SequenceNo;      // running sequence number
    t_u8                 DestPanID_L;     // destination PAN ID (Low-Byte)
    t_u8                 DestPanID_H;     // destination PAN ID (High-Byte)
    t_u32                SrcNodeID_L;     // source Node ID (Low-Byte)
    t_u32                SrcNodeID_H;     // source Node ID (High-Byte)
    // t_ttc_packet_payload Payload;      // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_mhr_010011;
#define PACKET_802154_010011_MHR_INITIAL_FCF 0b0000000000000000 // initial value for FCF field for this type
#define PACKET_802154_110011_MHR_INITIAL_FCF PACKET_802154_010011_MHR_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_mhr_011000 {                     // MAC Header of type 0b011000
    // Note: Type == 0b001000 means:
    // FCF.Bits.SecurityEnabled      = 0b0
    // FCF.Bits.PanID_Compression    = 0b1
    // FCF.Bits.DstAddressType       = 0b10 (16-bit address)
    // FCF.Bits.SrcAddressType       = 0b00 (address omitted)

unsigned Length : PACKET_802154_LENGTH_BITS;       // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf  FCF;
    t_u8                 SequenceNo;      // running sequence number
    t_u8                 DestPanID_L;     // destination PAN ID (Low-Byte)
    t_u8                 DestPanID_H;     // destination PAN ID (High-Byte)
    t_u8                 DestNodeID_L;    // destination Node ID (Low-Byte)
    t_u8                 DestNodeID_H;    // destination Node ID (High-Byte)
    // t_ttc_packet_payload Payload;      // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_mhr_011000;
#define PACKET_802154_011000_MHR_INITIAL_FCF 0b0001100001000000 // initial value for FCF field for this type
#define PACKET_802154_111000_MHR_INITIAL_FCF PACKET_802154_011000_MHR_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_mhr_011010 {                     // MAC Header of type 0b011010
    // Note: Type == 0b011010 means:
    // FCF.Bits.SecurityEnabled      = 0b0
    // FCF.Bits.PanID_Compression    = 0b1
    // FCF.Bits.DstAddressType       = 0b10 (16-bit address)
    // FCF.Bits.SrcAddressType       = 0b10 (16-bit address)

unsigned Length : PACKET_802154_LENGTH_BITS;       // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf  FCF;
    t_u8                 SequenceNo;      // running sequence number
    t_u8                 DestPanID_L;     // destination PAN ID (Low-Byte)
    t_u8                 DestPanID_H;     // destination PAN ID (High-Byte)
    t_u8                 DestNodeID_L;    // destination Node ID (Low-Byte)
    t_u8                 DestNodeID_H;    // destination Node ID (High-Byte)
    t_u8                 SrcNodeID_L;     // source Node ID (Low-Byte)
    t_u8                 SrcNodeID_H;     // source Node ID (High-Byte)
    // t_ttc_packet_payload Payload;      // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_mhr_011010;
#define PACKET_802154_011010_MHR_INITIAL_FCF 0b1001100001000000 // initial value for FCF field for this type
#define PACKET_802154_111010_MHR_INITIAL_FCF PACKET_802154_011010_MHR_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_mhr_011110 {                     // MAC Header of type 0b011110
    // Note: Type == 0b011110 means:
    // FCF.Bits.SecurityEnabled      = 0b0
    // FCF.Bits.PanID_Compression    = 0b1
    // FCF.Bits.DstAddressType       = 0b11 (16-bit address)
    // FCF.Bits.SrcAddressType       = 0b10 (16-bit address)

unsigned Length : PACKET_802154_LENGTH_BITS;        // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf  FCF;
    t_u8                 SequenceNo;      // running sequence number
    t_u32                DestPanID_L;     // destination PAN ID (Low-Byte)
    t_u32                DestPanID_H;     // destination PAN ID (High-Byte)
    t_u32                DestNodeID_L;    // destination Node ID (Low-Byte)
    t_u32                DestNodeID_H;    // destination Node ID (High-Byte)
    t_u8                 SrcNodeID_L;     // source Node ID (Low-Byte)
    t_u8                 SrcNodeID_H;     // source Node ID (High-Byte)
    // t_ttc_packet_payload Payload;      // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_mhr_011110;
#define PACKET_802154_011110_MHR_INITIAL_FCF 0b1001110001000000 // initial value for FCF field for this type
#define PACKET_802154_111110_MHR_INITIAL_FCF PACKET_802154_011110_MHR_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_mhr_011111 {                    // MAC Header of type 0b011111
    // Note: Type == 0b011111 means:
    // FCF.Bits.SecurityEnabled      = 0b0
    // FCF.Bits.PanID_Compression    = 0b1
    // FCF.Bits.DstAddressType       = 0b11 (64-bit address)
    // FCF.Bits.SrcAddressType       = 0b11 (64-bit address)

unsigned Length : PACKET_802154_LENGTH_BITS;       // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf  FCF;
    t_u8                 SequenceNo;   // running sequence number
    t_u8                 DestPanID_L;  // destination PAN ID (Low-Byte)
    t_u8                 DestPanID_H;  // destination PAN ID (High-Byte)
    t_u32                DestNodeID_L; // destination Node ID (Low-Byte)
    t_u32                DestNodeID_H; // destination Node ID (High-Byte)
    t_u32                SrcNodeID_L;  // source Node ID (Low-Byte)
    t_u32                SrcNodeID_H;  // source Node ID (High-Byte)
    t_ttc_packet_payload Payload;      // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_mhr_011111;
#define PACKET_802154_011111_MHR_INITIAL_FCF 0b1101110001000000 // initial value for FCF field for this type
#define PACKET_802154_111111_MHR_INITIAL_FCF PACKET_802154_011111_MHR_INITIAL_FCF | 0b1000  // encrypted packet 

//}MAC Headers
/** { MAC Headers with Auxiliary Security Header **********************************
 *
 * Packets using encryption add one of these packet_802154_auxiliary_security_s<N>_t fields just
 * before their Payload field.
 */

typedef enum {
    packet_802154_sl_None       = 0b000, //
    packet_802154_sl_MIC32      = 0b001, // data                  authenticity;  4 Encryption Octets
    packet_802154_sl_MIC64      = 0b010, // data                  authenticity;  8 Encryption Octets
    packet_802154_sl_MIC128     = 0b011, // data                  authenticity; 16 Encryption Octets
    packet_802154_sl_ENC        = 0b100, // data confidentialty;                 0 Encryption Octets
    packet_802154_sl_ENC_MIC32  = 0b101, // data confidentialty + authenticity;  4 Encryption Octets
    packet_802154_sl_ENC_MIC64  = 0b110, // data confidentialty + authenticity;  8 Encryption Octets
    packet_802154_sl_ENC_MIC128 = 0b111, // data confidentialty + authenticity; 16 Encryption Octets
    packet_802154_sl_unknown
} e_packet_802154_security_level;

typedef enum {
    packet_802154_kim_implicit   = 0b00, // key determined implicitly from source and destination address
    packet_802154_kim_index      = 0b01, // key determined from key index field
    packet_802154_kim_explicit4  = 0b10, // key determined explicit from 4 byte key source field + key index field
    packet_802154_kim_explicit8  = 0b11, // key determined explicit from 8 byte key source field + key index field
    packet_802154_kim_unknown
} e_packet_802154_key_identifier_mode;

/** Different auxiliary security headers exist depending on value of KeyIdentifierMode
 */

typedef struct s_packet_802154_auxiliary_security_s00 { // auxiliary security header for key identifier mode 0b00

    struct  {
        unsigned Security_Level    : 3; // -> e_packet_802154_security_level
        unsigned KeyIdentifierMode : 2; // = 0b00
        unsigned reserved          : 3;
    } __attribute__( ( __packed__ ) ) SecurityControl;
    t_u32 FrameCounter;

} __attribute__( ( __packed__ ) ) t_packet_802154_auxiliary_security_s00;

typedef struct s_packet_802154_auxiliary_security_s01 { // auxiliary security header for key identifier mode 0b00

    struct  {
        unsigned Security_Level    : 3; // -> e_packet_802154_security_level
        unsigned KeyIdentifierMode : 2; // = 0b01
        unsigned reserved          : 3;
    } __attribute__( ( __packed__ ) ) SecurityControl;
    t_u32 FrameCounter;

    t_u8 KeyIndex;
} __attribute__( ( __packed__ ) ) t_packet_802154_auxiliary_security_s01;

typedef struct s_packet_802154_auxiliary_security_s10 { // auxiliary security header for key identifier mode 0b00

    struct  {
        unsigned Security_Level    : 3; // -> e_packet_802154_security_level
        unsigned KeyIdentifierMode : 2; // = 0b10
        unsigned reserved          : 3;
    } __attribute__( ( __packed__ ) ) SecurityControl;
    t_u32 FrameCounter;

    t_u8 KeySource[4];
    t_u8 KeyIndex;
} __attribute__( ( __packed__ ) ) t_packet_802154_auxiliary_security_s10;

typedef struct s_packet_802154_auxiliary_security_s11 { // auxiliary security header for key identifier mode 0b00

    struct  {
        unsigned Security_Level    : 3; // -> e_packet_802154_security_level
        unsigned KeyIdentifierMode : 2; // = 0b11
        unsigned reserved          : 3;
    } __attribute__( ( __packed__ ) ) SecurityControl;
    t_u32 FrameCounter;

    t_u8 KeySource[8];
    t_u8 KeyIndex;
} __attribute__( ( __packed__ ) ) t_packet_802154_auxiliary_security_s11;

// ToDo: define packet_802154_mhr_1*_t structs for all combinations of auxiliary security headers and packet_802154_mhr_0*_t structs

//}MAC Headers with Auxiliary Security Header

/** { MAC DATA Packets *************************************************************************************
 *
 * Data packets transport generic data payload between two nodes.
 * One type data packet exist or each MAC Header type.
 */

typedef struct s_packet_802154_data_000010 {  // data packet with MAC Header of type 0b000010
    t_packet_802154_mhr_000010 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_data_000010;
#define PACKET_802154_000010_DATA_INITIAL_FCF (PACKET_802154_000010_MHR_INITIAL_FCF | 0b001) // initial value for FCF field for this type
#define PACKET_802154_100010_DATA_INITIAL_FCF PACKET_802154_000010_DATA_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_data_000011 {  // data packet with MAC Header of type 0b000011
    t_packet_802154_mhr_000011 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_data_000011;
#define PACKET_802154_000011_DATA_INITIAL_FCF (PACKET_802154_000011_MHR_INITIAL_FCF | 0b001) // initial value for FCF field for this type
#define PACKET_802154_100011_DATA_INITIAL_FCF PACKET_802154_000011_DATA_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_data_001000 {  // data packet with MAC Header of type 0b001000
    t_packet_802154_mhr_001000 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_data_001000;
#define PACKET_802154_001000_DATA_INITIAL_FCF (PACKET_802154_001000_MHR_INITIAL_FCF | 0b001) // initial value for FCF field for this type
#define PACKET_802154_101000_DATA_INITIAL_FCF PACKET_802154_001000_DATA_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_data_001010 {  // data packet with MAC Header of type 0b001010
    t_packet_802154_mhr_001010 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_data_001010;
#define PACKET_802154_001010_DATA_INITIAL_FCF (PACKET_802154_001010_MHR_INITIAL_FCF | 0b001) // initial value for FCF field for this type
#define PACKET_802154_101010_DATA_INITIAL_FCF PACKET_802154_001010_DATA_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_data_001100 {  // data packet with MAC Header of type 0b001100
    t_packet_802154_mhr_001100 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_data_001100;
#define PACKET_802154_001100_DATA_INITIAL_FCF (PACKET_802154_001100_MHR_INITIAL_FCF | 0b001) // initial value for FCF field for this type
#define PACKET_802154_101100_DATA_INITIAL_FCF PACKET_802154_001100_DATA_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_data_001110 {  // data packet with MAC Header of type 0b001110
    t_packet_802154_mhr_001110 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_data_001110;
#define PACKET_802154_001110_DATA_INITIAL_FCF (PACKET_802154_001110_MHR_INITIAL_FCF | 0b001) // initial value for FCF field for this type
#define PACKET_802154_101110_DATA_INITIAL_FCF PACKET_802154_001110_DATA_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_data_001111 {  // data packet with MAC Header of type 0b001111
    t_packet_802154_mhr_001111 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_data_001111;
#define PACKET_802154_001111_DATA_INITIAL_FCF (PACKET_802154_001111_MHR_INITIAL_FCF | 0b001) // initial value for FCF field for this type
#define PACKET_802154_101111_DATA_INITIAL_FCF PACKET_802154_001111_DATA_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_data_011000 {  // data packet with MAC Header of type 0b011000
    t_packet_802154_mhr_011000 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_data_011000;
#define PACKET_802154_011000_DATA_INITIAL_FCF (PACKET_802154_011000_MHR_INITIAL_FCF | 0b001) // initial value for FCF field for this type
#define PACKET_802154_111000_DATA_INITIAL_FCF PACKET_802154_011000_DATA_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_data_011010 {  // data packet with MAC Header of type 0b011010
    t_packet_802154_mhr_011010 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_data_011010;
#define PACKET_802154_011010_DATA_INITIAL_FCF (PACKET_802154_011010_MHR_INITIAL_FCF | 0b001) // initial value for FCF field for this type
#define PACKET_802154_111010_DATA_INITIAL_FCF PACKET_802154_011010_DATA_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_data_011110 {  // data packet with MAC Header of type 0b011110
    t_packet_802154_mhr_011110 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_data_011110;
#define PACKET_802154_011110_DATA_INITIAL_FCF (PACKET_802154_011110_MHR_INITIAL_FCF | 0b001) // initial value for FCF field for this type
#define PACKET_802154_111110_DATA_INITIAL_FCF PACKET_802154_011110_DATA_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_data_011100 {  // data packet with MAC Header of type 0b011100
    t_packet_802154_mhr_011100 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_data_011100;
#define PACKET_802154_011100_DATA_INITIAL_FCF (PACKET_802154_011100_MHR_INITIAL_FCF | 0b001) // initial value for FCF field for this type
#define PACKET_802154_111100_DATA_INITIAL_FCF PACKET_802154_011100_DATA_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_data_011111 {  // data packet with MAC Header of type 0b011111
    t_packet_802154_mhr_011111 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_data_011111;
#define PACKET_802154_011111_DATA_INITIAL_FCF (PACKET_802154_011111_MHR_INITIAL_FCF | 0b001) // initial value for FCF field for this type
#define PACKET_802154_111111_DATA_INITIAL_FCF PACKET_802154_011111_DATA_INITIAL_FCF | 0b1000  // encrypted packet 

// ToDo: Define packet_802154_data_1*_t structs for encrypted packets

//}MAC Data Packets
/** { MAC COMMAND Packets *************************************************************************************
 *
 * Data packets transport special network commands between two nodes.
 * One type data packet exist or each MAC Header type.
 */

typedef struct s_packet_802154_command_000010 {  // command packet with MAC Header of type 0b000010
    t_packet_802154_mhr_000010 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_command_000010;
#define PACKET_802154_000010_COMMAND_INITIAL_FCF (PACKET_802154_000010_MHR_INITIAL_FCF | 0b011) // initial value for FCF field for this type
#define PACKET_802154_100010_COMMAND_INITIAL_FCF PACKET_802154_000010_COMMAND_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_command_000011 {  // command packet with MAC Header of type 0b000011
    t_packet_802154_mhr_000011 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_command_000011;
#define PACKET_802154_000011_COMMAND_INITIAL_FCF (PACKET_802154_000011_MHR_INITIAL_FCF | 0b011) // initial value for FCF field for this type
#define PACKET_802154_100011_COMMAND_INITIAL_FCF PACKET_802154_000011_COMMAND_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_command_001000 {  // command packet with MAC Header of type 0b001000
    t_packet_802154_mhr_001000 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_command_001000;
#define PACKET_802154_001000_COMMAND_INITIAL_FCF (PACKET_802154_001000_MHR_INITIAL_FCF | 0b011) // initial value for FCF field for this type
#define PACKET_802154_101000_COMMAND_INITIAL_FCF PACKET_802154_001000_COMMAND_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_command_001010 {  // command packet with MAC Header of type 0b001010
    t_packet_802154_mhr_001010 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_command_001010;
#define PACKET_802154_001010_COMMAND_INITIAL_FCF (PACKET_802154_001010_MHR_INITIAL_FCF | 0b011) // initial value for FCF field for this type
#define PACKET_802154_101010_COMMAND_INITIAL_FCF PACKET_802154_001010_COMMAND_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_command_001100 {  // command packet with MAC Header of type 0b001100
    t_packet_802154_mhr_001100 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_command_001100;
#define PACKET_802154_001100_COMMAND_INITIAL_FCF (PACKET_802154_001100_MHR_INITIAL_FCF | 0b011) // initial value for FCF field for this type
#define PACKET_802154_101100_COMMAND_INITIAL_FCF PACKET_802154_001100_COMMAND_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_command_001110 {  // command packet with MAC Header of type 0b001110
    t_packet_802154_mhr_001110 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_command_001110;
#define PACKET_802154_001110_COMMAND_INITIAL_FCF (PACKET_802154_001110_MHR_INITIAL_FCF | 0b011) // initial value for FCF field for this type
#define PACKET_802154_101110_COMMAND_INITIAL_FCF PACKET_802154_001110_COMMAND_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_command_001111 {  // command packet with MAC Header of type 0b001111
    t_packet_802154_mhr_001111 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_command_001111;
#define PACKET_802154_001111_COMMAND_INITIAL_FCF (PACKET_802154_001111_MHR_INITIAL_FCF | 0b011) // initial value for FCF field for this type
#define PACKET_802154_101111_COMMAND_INITIAL_FCF PACKET_802154_001111_COMMAND_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_command_011000 {  // command packet with MAC Header of type 0b011000
    t_packet_802154_mhr_011000 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_command_011000;
#define PACKET_802154_011000_COMMAND_INITIAL_FCF (PACKET_802154_011000_MHR_INITIAL_FCF | 0b011) // initial value for FCF field for this type
#define PACKET_802154_111000_COMMAND_INITIAL_FCF PACKET_802154_011000_COMMAND_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_command_011010 {  // command packet with MAC Header of type 0b011010
    t_packet_802154_mhr_011010 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_command_011010;
#define PACKET_802154_011010_COMMAND_INITIAL_FCF (PACKET_802154_011010_MHR_INITIAL_FCF | 0b011) // initial value for FCF field for this type
#define PACKET_802154_111010_COMMAND_INITIAL_FCF PACKET_802154_011010_COMMAND_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_command_011110 {  // command packet with MAC Header of type 0b011110
    t_packet_802154_mhr_011110 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_command_011110;
#define PACKET_802154_011110_COMMAND_INITIAL_FCF (PACKET_802154_011110_MHR_INITIAL_FCF | 0b011) // initial value for FCF field for this type
#define PACKET_802154_111110_COMMAND_INITIAL_FCF PACKET_802154_011110_COMMAND_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_command_011100 {  // command packet with MAC Header of type 0b011100
    t_packet_802154_mhr_011100 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_command_011100;
#define PACKET_802154_011100_COMMAND_INITIAL_FCF (PACKET_802154_011100_MHR_INITIAL_FCF | 0b011) // initial value for FCF field for this type
#define PACKET_802154_111100_COMMAND_INITIAL_FCF PACKET_802154_011100_COMMAND_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_command_011111 {  // command packet with MAC Header of type 0b011111
    t_packet_802154_mhr_011111 MHR;     // MAC Header providing packet type, sequence number and addressing
    t_ttc_packet_payload       Payload; // protocol specific payload of this packet
    // t_u16 FCS;                       // Frame CheckSum (after payload)
} __attribute__( ( __packed__ ) ) t_packet_802154_command_011111;
#define PACKET_802154_011111_COMMAND_INITIAL_FCF (PACKET_802154_011111_MHR_INITIAL_FCF | 0b011) // initial value for FCF field for this type
#define PACKET_802154_111111_COMMAND_INITIAL_FCF PACKET_802154_011111_COMMAND_INITIAL_FCF | 0b1000  // encrypted packet 

// ToDo: Define packet_802154_command_1*_t structs for encrypted packets

//}MAC Command Packets
/** { MAC BEACON Packets *************************************************************************************
 *
 * Beacon packets are broadcast packets that inform about current metaframe structure.
 * They provide only source address types.
 */

typedef union {  // superframe specification field of each beacon packet
    t_u16 Data;
    struct {
        unsigned BeaconOrder       : 4;
        unsigned SuperframeOrder   : 4;
        unsigned FinalCapSlot      : 4;
        unsigned BatteryLifeExt    : 1; // Battery Life Extension
        unsigned reserved          : 1;
        unsigned Coordinator       : 1; // PAN Coordinator
        unsigned AssociationPermit : 1; // ==1: This coordinator accepts new association requests
    } __attribute__( ( __packed__ ) ) Bits;
} t_packet_802154_beacon_superframe_spec;

typedef union {  // GTS specification field of each beacon packet (-> IEEE802.15.4-2011 p.63)
    t_u8 Data;
    struct {
        unsigned DescriptorCount : 3; // amount of 3-byte GTS descriptors in GTS list field
        unsigned reserved        : 4;
        unsigned Permit          : 1; // ==1: coordinator is accepting GTS requests
    } __attribute__( ( __packed__ ) ) Bits;
} t_packet_802154_beacon_gts_spec;

typedef union {  // GTS directions field of each beacon packet (-> IEEE802.15.4-2011 p.63)
    t_u8 Data;
    struct {
        unsigned DirectionsMask  : 7; // bit <n> == 1 <=> GTS list item #<n> is receive-only (transmit-only otherwise)
        unsigned reserved        : 1;
    } __attribute__( ( __packed__ ) ) Bits;
} t_packet_802154_beacon_gts_directions;

typedef union {  // single GTS descriptor stored in beacon packets (-> IEEE802.15.4-2011 p.64)
    t_u8 Data[3];
    struct {
        t_u16  DeviceShortAddress;
        unsigned StartingSlot : 4; // superframe slot where GTS begins
        unsigned Length       : 4; // amount of contiguos superframe slots of this GTS
    } __attribute__( ( __packed__ ) ) Bits;
} t_packet_802154_beacon_gts_descriptor;

typedef union {  // specification of pending address fields in beacon packet (-> IEEE802.15.4-2011 p.64)
    t_u8 Data;
    struct {
        unsigned AmountPendingShort    : 3; // amount of short addresses entries in PendingAddresses[]
        unsigned reserved1             : 1;
        unsigned AmountPendingExtended : 3; // amount of extended addresses entries in PendingAddresses[]
        unsigned reserved2             : 1;
    } __attribute__( ( __packed__ ) ) Bits;
} t_packet_802154_beacon_address_spec;

typedef struct s_packet_802154_beacon_000010 {  // beacon packet storing SrcAddr16+PanID16
    t_packet_802154_mhr_000010             MHR;                  // MAC Header providing packet type, sequence number and addressing
    t_packet_802154_beacon_superframe_spec Superframe;           // GTS Superframe Specification
    t_packet_802154_beacon_gts_spec        GTS_Specification;    // GTS information fields
    t_packet_802154_beacon_gts_directions  GTS_Directions;       // GTS directions bitmask
    t_packet_802154_beacon_gts_descriptor  GTS_Descriptors[0];   // GTS descriptor list (size specified in GTS_Specification)
    t_packet_802154_beacon_address_spec    AddressSpecification; // Specification of Pending Addresses
    t_u8                                   PendingAddresses[0];  // filled with all specified 16-bit short addresses followed by 64-bit extended addresses
    t_ttc_packet_payload                   Payload;              // protocol specific payload of this packet
    // t_u16                               FCS;                  // Frame Checksum Field
} __attribute__( ( __packed__ ) ) t_packet_802154_beacon_000010;
#define PACKET_802154_000010_BEACON_INITIAL_FCF (PACKET_802154_000010_MHR_INITIAL_FCF) // initial value for FCF field for this type
#define PACKET_802154_100010_BEACON_INITIAL_FCF PACKET_802154_000010_BEACON_INITIAL_FCF | 0b1000  // encrypted packet 

typedef struct s_packet_802154_beacon_000011 {  // beacon packet storing SrcAddr64+PanID16
    t_packet_802154_mhr_000011             MHR;                  // MAC Header providing packet type, sequence number and addressing
    t_packet_802154_beacon_superframe_spec Superframe;           // Superframe Specification
    t_packet_802154_beacon_gts_spec        GTS_Specification;    // GTS information fields
    t_packet_802154_beacon_gts_directions  GTS_Directions;       // GTS directions bitmask
    t_packet_802154_beacon_address_spec    AddressSpecification; // Specification of Pending Addresses
    t_u8                                   PendingAddresses[0];  // filled with all specified 16-bit short addresses followed by 64-bit extended addresses
    t_ttc_packet_payload                   Payload;              // protocol specific payload of this packet
    // t_u16                               FCS;                  // Frame Checksum Field
} __attribute__( ( __packed__ ) ) t_packet_802154_beacon_000011;
#define PACKET_802154_000011_BEACON_INITIAL_FCF (PACKET_802154_000011_MHR_INITIAL_FCF) // initial value for FCF field for this type
#define PACKET_802154_100011_BEACON_INITIAL_FCF PACKET_802154_000011_BEACON_INITIAL_FCF | 0b1000  // encrypted packet 

// ToDo: Define packet_802154_beacon_1*_t structs for encrypted packets

//}MAC Beacon Packets

/** { MAC ACKNOWLEDGE Packets *************************************************************************************
 *
 * Acknowledge packets carry no address data.
 */

typedef struct s_packet_802154_ack_000000 { // IEEE 802.15.4 acknowledgement frame

unsigned Length : PACKET_802154_LENGTH_BITS;         // total length of packet (must include two trailing CRC bytes if CRC is enabled)  (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    u_packet_802154_fcf         FCF;            // frame control bytes 00-01
    t_u8                        SequenceNo;     // running sequence number
    t_u16                       FCS;            // Frame Checksum Field
} t_packet_802154_ack_000000;
#define PACKET_802154_000000_ACK_INITIAL_FCF 0b0000000000000010 // initial value for FCF field for this type
#define PACKET_802154_100000_ACK_INITIAL_FCF PACKET_802154_000000_ACK_INITIAL_FCF | 0b1000  // encrypted packet 


//}Packets without Auxiliary Security Header



//}

typedef union { // (almost) complete media access control structure of all packet types
    // Note: beacon type packet structs are incomplete because their variable list sizes create too much types.
    // ToDo: add structures for encrypted packet types

unsigned Length : PACKET_802154_LENGTH_BITS;                  // first byte stores length of total packet including 16-bit checksum at end (Note: Always use s_ttc_packet_payloadet_size() to set this field!)
    t_packet_802154_generic          packet_802154_generic;       // generic header that fits to all types of packets

    // MAC headers are common to all data, command and beacon packets
    //                                                               Security | PanID_Compression | DstAddressType | SrcAddressType
    t_packet_802154_mhr_000010       packet_802154_mhr_000010;    //    0     |        0          |    0 Bits      |     16 Bits
    t_packet_802154_mhr_000011       packet_802154_mhr_000011;    //    0     |        0          |    0 Bits      |     64 Bits
    t_packet_802154_mhr_001000       packet_802154_mhr_001000;    //    0     |        0          |    16 Bits     |      0 Bits
    t_packet_802154_mhr_001010       packet_802154_mhr_001010;    //    0     |        0          |    16 Bits     |     16 Bits
    t_packet_802154_mhr_001100       packet_802154_mhr_001100;    //    0     |        0          |    64 Bits     |      0 Bits
    t_packet_802154_mhr_001110       packet_802154_mhr_001110;    //    0     |        0          |    64 Bits     |     16 Bits
    t_packet_802154_mhr_001111       packet_802154_mhr_001111;    //    0     |        0          |    64 Bits     |     64 Bits
    t_packet_802154_mhr_011000       packet_802154_mhr_011000;    //    0     |        1          |    16 Bits     |      0 Bits
    t_packet_802154_mhr_011100       packet_802154_mhr_011100;    //    0     |        1          |    64 Bits     |      0 Bits
    t_packet_802154_mhr_011010       packet_802154_mhr_011010;    //    0     |        1          |    16 Bits     |     16 Bits
    t_packet_802154_mhr_011111       packet_802154_mhr_011111;    //    0     |        1          |    64 Bits     |     64 Bits

    //                                                                 Security | PanID_Compression | DstAddressType | SrcAddressType
    //packet_802154_mhr_100010_t       packet_802154_mhr_100010;    //    1     |        0          |    0 Bits      |     16 Bits
    //packet_802154_mhr_100011_t       packet_802154_mhr_100011;    //    1     |        0          |    0 Bits      |     64 Bits
    //packet_802154_mhr_101000_t       packet_802154_mhr_101000;    //    1     |        0          |    16 Bits     |      0 Bits
    //packet_802154_mhr_101010_t       packet_802154_mhr_101010;    //    1     |        0          |    16 Bits     |     16 Bits
    //packet_802154_mhr_101100_t       packet_802154_mhr_101100;    //    1     |        0          |    64 Bits     |      0 Bits
    //packet_802154_mhr_101111_t       packet_802154_mhr_101111;    //    1     |        0          |    64 Bits     |     64 Bits
    //packet_802154_mhr_111100_t       packet_802154_mhr_111100;    //    1     |        1          |    64 Bits     |      0 Bits
    //packet_802154_mhr_111000_t       packet_802154_mhr_111000;    //    1     |        1          |    16 Bits     |      0 Bits
    //packet_802154_mhr_111010_t       packet_802154_mhr_111010;    //    1     |        1          |    16 Bits     |     16 Bits
    //packet_802154_mhr_111111_t       packet_802154_mhr_111111;    //    1     |        1          |    64 Bits     |     64 Bits

    // data packets transport data from one beacon to another one
    //                                                               Security | PanID_Compression | DstAddressType | SrcAddressType
    t_packet_802154_data_000010      packet_802154_data_000010;   //    0     |        0          |     0 Bits     |     16 Bits
    t_packet_802154_data_000011      packet_802154_data_000011;   //    0     |        0          |     0 Bits     |     64 Bits
    t_packet_802154_data_001000      packet_802154_data_001000;   //    0     |        0          |    16 Bits     |      0 Bits
    t_packet_802154_data_001010      packet_802154_data_001010;   //    0     |        0          |    16 Bits     |     16 Bits
    t_packet_802154_data_001100      packet_802154_data_001100;   //    0     |        0          |    64 Bits     |      0 Bits
    t_packet_802154_data_001110      packet_802154_data_001110;   //    0     |        0          |    64 Bits     |     16 Bits
    t_packet_802154_data_001111      packet_802154_data_001111;   //    0     |        0          |    64 Bits     |     64 Bits
    t_packet_802154_data_011000      packet_802154_data_011000;   //    0     |        1          |    16 Bits     |      0 Bits
    t_packet_802154_data_011010      packet_802154_data_011010;   //    0     |        1          |    16 Bits     |     16 Bits
    t_packet_802154_data_011100      packet_802154_data_011100;   //    0     |        1          |    64 Bits     |      0 Bits
    t_packet_802154_data_011110      packet_802154_data_011110;   //    0     |        1          |    64 Bits     |     16 Bits
    t_packet_802154_data_011111      packet_802154_data_011111;   //    0     |        1          |    64 Bits     |     64 Bits

    //                                                                 Security | PanID_Compression | DstAddressType | SrcAddressType
    //packet_802154_data_100010_t      packet_802154_data_100010;   //    1     |        0          |     0 Bits     |     16 Bits
    //packet_802154_data_100011_t      packet_802154_data_100011;   //    1     |        0          |     0 Bits     |     64 Bits
    //packet_802154_data_101000_t      packet_802154_data_101000;   //    1     |        0          |    16 Bits     |      0 Bits
    //packet_802154_data_101010_t      packet_802154_data_101010;   //    1     |        0          |    16 Bits     |     16 Bits
    //packet_802154_data_101100_t      packet_802154_data_101100;   //    1     |        0          |    64 Bits     |      0 Bits
    //packet_802154_data_101111_t      packet_802154_data_101111;   //    1     |        0          |    64 Bits     |     64 Bits
    //packet_802154_data_111000_t      packet_802154_data_111000;   //    1     |        1          |    16 Bits     |      0 Bits
    //packet_802154_data_111010_t      packet_802154_data_111010;   //    1     |        1          |    16 Bits     |     16 Bits
    //packet_802154_data_111100_t      packet_802154_data_111100;   //    1     |        1          |    64 Bits     |      0 Bits
    //packet_802154_data_111111_t      packet_802154_data_111111;   //    1     |        1          |    64 Bits     |     64 Bits

    // command packets transport special MAC commands from one beacon to another one
    //                                                               Security | PanID_Compression | DstAddressType | SrcAddressType
    t_packet_802154_command_000010  packet_802154_command_000010; //    0     |        0          |     0 Bits     |     16 Bits
    t_packet_802154_command_000011  packet_802154_command_000011; //    0     |        0          |     0 Bits     |     64 Bits
    t_packet_802154_command_001000  packet_802154_command_001000; //    0     |        0          |    16 Bits     |      0 Bits
    t_packet_802154_command_001010  packet_802154_command_001010; //    0     |        0          |    16 Bits     |     16 Bits
    t_packet_802154_command_001100  packet_802154_command_001100; //    0     |        0          |    64 Bits     |      0 Bits
    t_packet_802154_command_001110  packet_802154_command_001110; //    0     |        0          |    64 Bits     |      0 Bits
    t_packet_802154_command_001111  packet_802154_command_001111; //    0     |        0          |    64 Bits     |     64 Bits
    t_packet_802154_command_011000  packet_802154_command_011000; //    0     |        1          |    16 Bits     |      0 Bits
    t_packet_802154_command_011010  packet_802154_command_011010; //    0     |        1          |    16 Bits     |     16 Bits
    t_packet_802154_command_011100  packet_802154_command_011100; //    0     |        1          |    64 Bits     |      0 Bits
    t_packet_802154_command_011110  packet_802154_command_011110; //    0     |        1          |    64 Bits     |     16 Bits
    t_packet_802154_command_011111  packet_802154_command_011111; //    0     |        1          |    64 Bits     |     64 Bits

    //                                                                 Security | PanID_Compression | DstAddressType | SrcAddressType
    //packet_802154_command_100010_t  packet_802154_command_100010; //    1     |        0          |     0 Bits     |     16 Bits
    //packet_802154_command_100011_t  packet_802154_command_100011; //    1     |        0          |     0 Bits     |     64 Bits
    //packet_802154_command_101000_t  packet_802154_command_101000; //    1     |        0          |    16 Bits     |      0 Bits
    //packet_802154_command_101010_t  packet_802154_command_101010; //    1     |        0          |    16 Bits     |     16 Bits
    //packet_802154_command_101100_t  packet_802154_command_101100; //    1     |        0          |    64 Bits     |      0 Bits
    //packet_802154_command_101111_t  packet_802154_command_101111; //    1     |        0          |    64 Bits     |     64 Bits
    //packet_802154_command_111000_t  packet_802154_command_111000; //    1     |        1          |    16 Bits     |      0 Bits
    //packet_802154_command_111010_t  packet_802154_command_111010; //    1     |        1          |    16 Bits     |     16 Bits
    //packet_802154_command_111100_t  packet_802154_command_111100; //    1     |        1          |    64 Bits     |      0 Bits
    //packet_802154_command_111111_t  packet_802154_command_111111; //    1     |        1          |    64 Bits     |     64 Bits

    // beacon packets transport network information data from coordinator to all beacons
    //                                                               Security | PanID_Compression | DstAddressType | SrcAddressType
    t_packet_802154_beacon_000010   packet_802154_beacon_000010;  //    0     |        0          |    0 Bits      |     16 Bits
    t_packet_802154_beacon_000011   packet_802154_beacon_000011;  //    0     |        0          |    0 Bits      |     64 Bits
    //packet_802154_beacon_100010_t   packet_802154_beacon_100010;  //    1     |        0          |    0 Bits      |     16 Bits
    //packet_802154_beacon_100011_t   packet_802154_beacon_100011;  //    1     |        0          |    0 Bits      |     64 Bits

    // acknowledge packets
    //                                                               Security | PanID_Compression | DstAddressType | SrcAddressType
    t_packet_802154_ack_000000      packet_802154_ack_000000;     //    0     |        0          |    0 Bits      |     0 Bits
    //packet_802154_ack_100000_t      packet_802154_ack_100000;     //    1     |        0          |    0 Bits      |     0 Bits

    // raw packet data
    t_u8 packet_802154_raw[25];
} t_packet_802154_mac;

typedef struct { // additional meta data for each packet

    t_u32 unused;
} t_packet_802154_meta;

// define datatypes being required by ttc_packet_types.h
//#define t_ttc_packet_meta_lowlevel  t_packet_802154_meta
#define t_ttc_packet_mac            t_packet_802154_mac

//} Structures/ Enums

#endif //PACKET_802154_TYPES_H
