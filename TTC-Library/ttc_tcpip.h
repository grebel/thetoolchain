#ifndef TTC_TCPIP_H
#define TTC_TCPIP_H
/** { ttc_tcpip.h *******************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for TCPIP devices.
 *  The functions defined in this file provide a hardware independent interface 
 *  for your application.
 *                                          
 *  The basic usage scenario for devices:
 *  1) check:       Assert_TCPIP(tc_tcpip_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_tcpip_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_tcpip_init(LogicalIndex);
 *  4) use:         ttc_tcpip_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level tcpip and application.
 *
 *  Created from template ttc_device.h revision 30 at 20150309 09:09:54 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

#ifndef EXTENSION_ttc_tcpip
#error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_tcpip.sh
#endif

//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_tcpip.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_tcpip_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level tcpip only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * tcpip devices on all supported architectures.
 * Check tcpip/tcpip_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your 
 * activate_project.sh file inside your project folder.
 *
 */
 
/** Prepares tcpip Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_tcpip_prepare();
void _driver_tcpip_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_tcpip_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_TCPIP_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_tcpip_config* ttc_tcpip_get_configuration(t_u8 LogicalIndex);

/** fills out given Config with maximum valid values for indexed TCPIP
 * @param LogicalIndex    index of device to init (1..ttc_TCPIP_get_max_LogicalIndex())
 * @return                pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_tcpip_config* ttc_tcpip_get_features(t_u8 LogicalIndex);

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TCPIP_get_max_LogicalIndex())
 * @return                == 0: tcpip device has been initialized successfully; != 0: error-code
 */
e_ttc_tcpip_errorcode ttc_tcpip_init(t_u8 LogicalIndex);

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_TCPIP_get_max_LogicalIndex())
 */
void ttc_tcpip_deinit(t_u8 LogicalIndex);

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_tcpip_deinit() if device has been initialized.  
 *
 * @param LogicalIndex  logical index of tcpip device (1..ttc_tcpip_get_max_index() )
 * @return                == 0: default values have been loaded successfully for indexed tcpip device; != 0: error-code
 */
e_ttc_tcpip_errorcode  ttc_tcpip_load_defaults(t_u8 LogicalIndex);

/** maps from logical to physical device index
 *
 * High-level tcpips (ttc_tcpip_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_TCPIPn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be 
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any 
 * order.
 *
 * @param LogicalIndex  logical index of tcpip device (1..ttc_tcpip_get_max_index() )
 * @return              physical index of tcpip device (0 = first physical tcpip device, ...)
 */
t_u8 ttc_tcpip_logical_2_physical_index(t_u8 LogicalIndex);

/** maps from physical to logical device index
 *
 * This is a reverse function to ttc_tcpip_logical_2_physical_index()
 *
 * @param  PhysicalIndex  physical index of tcpip device (0 = first physical tcpip device, ...)
 * @return                logical index of tcpip device (1..ttc_tcpip_get_max_index() )
 */
t_u8 ttc_tcpip_physical_2_logical_index(t_u8 PhysicalIndex);

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TCPIP_get_max_LogicalIndex())
 */
void ttc_tcpip_reset(t_u8 LogicalIndex);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_tcpip(t_u8 LogicalIndex)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_tcpip_ are passed to interfaces/ttc_tcpip_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl tcpip UPDATE
 */


/** fills out given Config with maximum valid values for indexed TCPIP
 * @param Config  = pointer to struct t_ttc_tcpip_config (must have valid value for PhysicalIndex)
 * @return        pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_tcpip_config* _driver_tcpip_get_features(t_ttc_tcpip_config* Config);

/** shutdown single TCPIP unit device
 * @param Config        pointer to struct t_ttc_tcpip_config (must have valid value for PhysicalIndex)
 * @return              == 0: TCPIP has been shutdown successfully; != 0: error-code
 */
e_ttc_tcpip_errorcode _driver_tcpip_deinit(t_ttc_tcpip_config* Config);

/** initializes single TCPIP unit for operation
 * @param Config        pointer to struct t_ttc_tcpip_config (must have valid value for PhysicalIndex)
 * @return              == 0: TCPIP has been initialized successfully; != 0: error-code
 */
e_ttc_tcpip_errorcode _driver_tcpip_init(t_ttc_tcpip_config* Config);

/** loads configuration of indexed TCPIP unit with default values
 * @param Config        pointer to struct t_ttc_tcpip_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_tcpip_errorcode _driver_tcpip_load_defaults(t_ttc_tcpip_config* Config);

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_tcpip_config (must have valid value for PhysicalIndex)
 */
void _driver_tcpip_reset(t_ttc_tcpip_config* Config);
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_TCPIP_H
