#ifndef TTC_PACKET_H
#define TTC_PACKET_H
/** { ttc_packet.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for packet devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for ttc-devices:
 *  1) check:       Assert_PACKET(tc_packet_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_packet_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_packet_init(LogicalIndex);
 *  4) use:         ttc_packet_XXX(LogicalIndex);
 *
 *  Device Index
 *  Devices are addressed by a logical index with no correlation to the order of physical devices.
 *  Logical indices start at 1 to simply detect uninitialized values.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level packet and application.
 *
 *  Created from template ttc_device.h revision 35 at 20150928 09:30:04 UTC
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of ttc_packet (Do not delete this line!)
 *
 *  Universal network packet assembly and analyzation.
 *  Different low-level packet drivers can implement different binary encoding.
 *
 *  How to configure high-level network packets
 *
 *  Each network packet consists of three parts:
 *  1) Meta Header
 *  2) Packet Header
 *  3) Packet Payload
 *
 *  Meta Header
 *    Each packet may carry data that exists only in the local node and is not transmitted.
 *    Such meta data may be (among other):
 *    - Type of packet     (as been identifyied from packet header)
 *    - Category of packet (more general type of packet)
 *    - Time Stamp when packet has been received / is to be transmitted
 *    - Received Signal Strength (RSSI)
 *    - Distance to sender node (if radio supports ranging)
 *
 *  Packet Header
 *    This protocol specific header is transmitted with every packet.
 *    The packet header has a varying size depending on the current packet type. Its varying
 *    size makes it difficult to calculate the payload start offset. This calculation
 *    is done by ttc_packet_identify().
 *    The packet header stores source and destination address in long (64 bit)
 *    or short (16 bit) format. Protocol independent functions are provided to
 *    set, read and compare source and destination address in a uniform and fast way.
 *
 *  Packet Payload
 *    Raw data being provided and handled by application. Call ttc_packet_payload_get_pointer()
 *    for an initialized packet to get a pointer where to store your data in the packet.
 *
 *
 *  Example Usage
 *
 *    #include "ttc-lib/ttc_radio.h"     // radio transceiver
 *    #include "ttc-lib/ttc_packet.h"    // network packet handling
 *    #include "ttc-lib/ttc_string.h"    // string compare/ copy/ snprintf
 *    #include "ttc-lib/ttc_memory.h"    // memory copying, pointer testing
 *    #include "ttc-lib/ttc_heap.h"      // dynamic memory allocation + memory pools
 *
 *    t_ttc_packet_address LocalID = {
 *      // 64 bit address of current node
 *      .Address64.Bytes = { 0xaf, 0xfe, 0x00, 0x00, 0x00, 0x00, 0x12, 0x34},
 *
 *      // 16 bit address of current node
 *      .Address16 = 0x1234,
 *
 *      // Personal Area Network Identification
 *      .PanID = 0xc001
 *    };
 *
 *    // variable used to assemble target IDs
 *    t_ttc_packet_address TargetID;
 *
 *    // egnough memory space to create every type of packet
 *    // t_ttc_packet describes only the header. If we want to store payload,
 *    // we have to allocate more bytes.
 *    t_ttc_packet* PacketTX = ttc_heap_alloc(sizeof(t_ttc_packet) + 100);
 *
 *    // Optional feature that requires TTC_PACKET_SOCKET_BYTE defined as 1.
 *    // Protocol type is stored as first byte of payload. This extra byte allows to
 *    // share one radio among multiple protocol statemachines.
 *    // Important: Someone must call ttc_radio_packet_received_*(1, 0, SocketID)
 *    //            on other side regularly to process these packets!
 *    const e_ttc_packet_pattern SocketID = E_ttc_packet_pattern_socket_Application1;
 *
 *    // initialize data packet with 16-bit Source + Target address (->packet_802154_types.h)
 *    ttc_packet_initialize( &PacketTX, E_ttc_packet_type_802154_Data_001010, SocketID );
 *
 *    // set packet source address (will automatically copy Address16 according to packet type
 *    E_ttc_packet_address_source_set( PacketTX, &LocalID);
 *
 *    // we know that our packet stores only 16-bit target address
 *    TargetID.Address16 = 0x2345;
 *
 *    // copy Personal Area Network Identification
 *    TargetID.PanID = LocalID.PanID;
 *
 *    ttc_packet_address_target_set( PacketTX, &TargetID);
 *
 *    // prepare packet payload
 *    t_u8* Payload;
 *    t_u8  PayloadSize;
 *
 *    // using assert to ensure that we get valid pointer to payload area
 *    Assert( ttc_packet_payload_get_pointer( PacketTX, &Payload, &PayloadSize ) == ec_packet_OK, ttc_assert_origin_auto );
 *
 *    // copy some text into packet payload area
 *    ttc_string_snprintf( Payload, PayloadSize, "Sensor #%04x -> Anchor #%04x", LocalNodeID.Address16, TargetID->Address16 );
 *
 *    // send out packet
 *    ttc_radio_transmit_packet_now( 1, 0, PacketTX, PayloadSize, FALSE );
 *
 *    while (1) {
 *       ...
 *
 *      // look for incoming packets
 *      t_ttc_packet* PacketRX = ttc_radio_packet_received_tryget(1, 0, SocketID);
 *      if (PacketRX) {
 *        // process packet
 *
 *        // return packet buffer to its memory pool for reuse
 *        ttc_radio_packet_release(PacketRX);
 *      }
 *    }
}*/

#ifndef EXTENSION_ttc_packet
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_packet.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_packet.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_assert.h"
#include "ttc_memory.h"
#include "ttc_heap_types.h"
#include "interfaces/ttc_packet_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level packet only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function Declarations ************************************************{
 *
 * The functions declared below provide the main interface for
 * packet devices on all supported architectures.
 * Check packet/packet_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares packet Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_packet_prepare();
void _driver_packet_prepare();

/** Creates an acknowledge packet for given packet
 *
 * When a received packet requests an acknowledge (ttc_packet_acknowledge_request_get(Packet)==TRUE)
 * then this function can initialize a given packet buffer as a matching acknowledge packet.
 *
 * @param Packet    (t_ttc_packet*)     received network packet to inspect
 * @param PacketACK (t_ttc_packet*)     allocated memory buffer will be initialized with acknowledgement packet
 */
void ttc_packet_acknowledge( t_ttc_packet* Packet, t_ttc_packet* PacketACK );
void _driver_packet_acknowledge( t_ttc_packet* Packet, t_ttc_packet* PacketACK );
#define ttc_packet_acknowledge  _driver_packet_acknowledge  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Returns flag acknowledge requested from header of given packet
 *
 * Most protocols define this flag to realize 2-way or 3-way handshakes for safe data transmission.
 * The flag can be read from incoming packets to send a confirmation packet if set.
 *
 * @param Packet  (t_ttc_packet*)  initialized network packet
 * @return        (BOOL)           == TRUE: source node is requesting to send an acknowledge packet
 */
BOOL ttc_packet_acknowledge_request_get( t_ttc_packet* Packet );
BOOL _driver_packet_acknowledge_request_get( t_ttc_packet* Packet );
#define ttc_packet_acknowledge_request_get  _driver_packet_acknowledge_request_get  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Set flag acknowledge requested in given packet
 *
 * Most protocols define this flag to realize 2-way or 3-way handshakes for safe data transmission.
 * The flag can be set for outgoing packets to request a confirmation packet from target node.
 *
 * @param Packet     (t_ttc_packet*)  initialized network packet
 * @param RequestACK (BOOL)           == TRUE: request acknowledge packet from target node
 */
void ttc_packet_acknowledge_request_set( t_ttc_packet* Packet, BOOL RequestACK );
void _driver_packet_acknowledge_request_set( t_ttc_packet* Packet, BOOL RequestACK );
#define ttc_packet_acknowledge_request_set  _driver_packet_acknowledge_request_set  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Compares if given 16 or 64 bit address and PanID matches source address in given packet
 *
 * Some network packets store 64-bit hardware addresses while others store short 16-bit protocol adresses.
 * This functions implements the complete handling of both. Just provide a 16- and 64-bit address and the
 * function will automatically find out which one is to be used.
 *
 * @param  Packet     any supported packet type containing 16- or 64-bit addresses
 * @param  Address    16/64 bit address + PanID to use for comparison
 * @return  (e_ttc_packet_address_format)  ==0: no address found in packet (packet type not supported); ==ttc_packet_address_Source16: Address16 matches given Address; ==ttc_packet_address_Source64: Address64 matches given Address
 */
e_ttc_packet_address_format E_ttc_packet_address_source_compare( t_ttc_packet* Packet, const t_ttc_packet_address* Address );
e_ttc_packet_address_format _driver_packet_address_source_compare( t_ttc_packet* Packet, const t_ttc_packet_address* Address );
#define E_ttc_packet_address_source_compare(Packet, Address) _driver_packet_address_source_compare(Packet, Address)

/** Reads in 16 or 64 bit source address from given packet
 *
 * Some network packets store 64-bit hardware addresses while others store short 16-bit protocol adresses.
 * This function finds start address of 16- or 64-bit source address in given packet and returns found type.
 * You may then read or change this address in the packet.
 *
 * @param  Packet     any supported packet type containing 16- or 64-bit addresses
 * @param  Pointer2SourceID     !=NULL: will be loaded with pointer to start of SourceID  inside given Packet (maybe a 16- or 64-bit ID. Check return type to be sure!)
 * @param  Pointer2SourcePanID  !=NULL: will be loaded with pointer to start of SourcePanID inside given Packet
 * @return  (e_ttc_packet_address_format)  ==0: no address found in packet (packet type not supported or no source address in packet); ==ttc_packet_address_Source16: Address16 read; ==ttc_packet_address_Source64: Address64 read
 */
e_ttc_packet_address_format E_ttc_packet_address_source_get_pointer( t_ttc_packet* Packet, void** Pointer2SourceID, t_u16** Pointer2SourcePanID );
e_ttc_packet_address_format _driver_packet_address_source_get_pointer( t_ttc_packet* Packet, void** Pointer2SourceID, t_u16** Pointer2SourcePanID );
#define E_ttc_packet_address_source_get_pointer(Packet, Pointer2SourceID, Pointer2SourcePanID) _driver_packet_address_source_get_pointer(Packet, Pointer2SourceID, Pointer2SourcePanID)

/** Copies source address from given packet into given packet address storage
 *
 * Some network packets store 64-bit hardware addresses while others store short 16-bit protocol adresses.
 * This functions implements the complete handling of both. It will copy the 16- or 64-bit address into
 * corresponding field of given Address.
 * Missing Fields will be set to zero in *Address.
 *
 * @param   (t_ttc_packet*)                Packet     any supported packet type containing 16- or 64-bit addresses
 * @param   (const t_ttc_packet_address* ) Address    !=NULL: 16/64 bit address + PanID to set; ==NULL: set broadcast address
 * @return  (e_ttc_packet_address_format)  ==0: packet does not store source address; ==ttc_packet_address_Source16: Address->Address16 set; ==ttc_packet_address_Source64: Address->Address64 set
 */
e_ttc_packet_address_format E_ttc_packet_address_source_get( t_ttc_packet* Packet, t_ttc_packet_address* Address );
e_ttc_packet_address_format _driver_packet_address_source_get( t_ttc_packet* Packet, t_ttc_packet_address* Address );
#define E_ttc_packet_address_source_get  _driver_packet_address_source_get  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Writes given 16 or 64 bit address as source address into given packet
 *
 * Some network packets store 64-bit hardware addresses while others store short 16-bit protocol adresses.
 * This functions implements the complete handling of both. Just provide a 16- and 64-bit address and the
 * function will automatically find out which one is to be used.
 * If Packet does not store a PanID, it will be ignored.
 *
 * @param   (t_ttc_packet*)                Packet     any supported packet type containing 16- or 64-bit addresses
 * @param   (const t_ttc_packet_address* ) Address    16/64 bit address + PanID to set
 * @return  (e_ttc_packet_address_format)  ==0: packet does not store source address; ==ttc_packet_address_Source16: Address16 read; ==ttc_packet_address_Source64: Address64 read
 */
e_ttc_packet_address_format E_ttc_packet_address_source_set( t_ttc_packet* Packet, const t_ttc_packet_address* Address );
e_ttc_packet_address_format _driver_packet_address_source_set( t_ttc_packet* Packet, const t_ttc_packet_address* Address );
#define E_ttc_packet_address_source_set(Packet, Address) _driver_packet_address_source_set(Packet, Address)

/** Compares if given 16 or 64 bit address and PanID matches target address in given packet
 *
 * Some network packets store 64-bit hardware addresses while others store short 16-bit protocol adresses.
 * This functions implements the complete handling of both. Just provide a 16- and 64-bit address and the
 * function will automatically find out which one is to be used.
 *
 * @param  Packet     any supported packet type containing 16- or 64-bit addresses
 * @param  Address    16/64 bit address + PanID to use for comparison
 * @return  (e_ttc_packet_address_format)  ==0: no address found in packet (packet type not supported); ==ttc_packet_address_Target16: Address16 read; ==ttc_packet_address_Target64: Address64 read
 */
e_ttc_packet_address_format t_ttc_packet_addressarget_compare( t_ttc_packet* Packet, t_ttc_packet_address* Address );
e_ttc_packet_address_format _driver_packet_address_target_compare( t_ttc_packet* Packet, t_ttc_packet_address* Address );
#define t_ttc_packet_addressarget_compare(Packet, Address) _driver_packet_address_target_compare(Packet, Address)

/** Reads in 16 or 64 bit target address from given packet
 *
 * Some network packets store 64-bit hardware addresses while others store short 16-bit protocol adresses.
 * This function finds start address of 16- or 64-bit target address in given packet and returns found type.
 * You may then read or change this address in the packet.
 *
 * @param  Packet     any supported packet type containing 16- or 64-bit addresses
 * @param  Pointer2TargetID   !=NULL: will be loaded with pointer to start of SourceID  inside given Packet
 * @param  Pointer2TargetPanID  !=NULL: will be loaded with pointer to start of SourcePanID inside given Packet
 * @return  (e_ttc_packet_address_format)  ==0: no address found in packet (packet type not supported or no source address in packet); ==ttc_packet_address_Target16: Address16 read; ==ttc_packet_address_Target64: Address64 read
 */
e_ttc_packet_address_format t_ttc_packet_addressarget_get_pointer( t_ttc_packet* Packet, void** Pointer2TargetID, t_u16** Pointer2TargetPanID );
e_ttc_packet_address_format _driver_packet_address_target_get_pointer( t_ttc_packet* Packet, void** Pointer2TargetID, t_u16** Pointer2TargetPanID );
#define t_ttc_packet_addressarget_get_pointer(Packet, Pointer2TargetID, Pointer2TargetPanID) _driver_packet_address_target_get_pointer(Packet, Pointer2TargetID, Pointer2TargetPanID)

/** Copies target address from given packet into given packet address storage
 *
 * Some network packets store 64-bit hardware addresses while others store short 16-bit protocol adresses.
 * This functions implements the complete handling of both. It will copy the 16- or 64-bit address into
 * corresponding field of given Address.
 * Missing Fields will be set to zero in *Address.
 *
 * @param   (t_ttc_packet*)                Packet     any supported packet type containing 16- or 64-bit addresses
 * @param   (const t_ttc_packet_address* ) Address    !=NULL: 16/64 bit address + PanID to set; ==NULL: set broadcast address
 * @return  (e_ttc_packet_address_format)  ==0: packet does not store target address; ==ttc_packet_address_Target16: Address->Address16 set; ==ttc_packet_address_Target64: Address->Address64 set
 */
e_ttc_packet_address_format t_ttc_packet_addressarget_get( t_ttc_packet* Packet, t_ttc_packet_address* Address );
e_ttc_packet_address_format _driver_packet_address_target_get( t_ttc_packet* Packet, t_ttc_packet_address* Address );
#define t_ttc_packet_addressarget_get  _driver_packet_address_target_get  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Writes given 16 or 64 bit address as target address into given packet
 *
 * Some network packets store 64-bit hardware addresses while others store short 16-bit protocol adresses.
 * This functions implements the complete handling of both. Just provide a 16- and 64-bit address and the
 * function will automatically find out which one is to be used.
 * If Packet does not store a PanID, it will be ignored.
 *
 * @param   (t_ttc_packet*)                Packet     any supported packet type containing 16- or 64-bit addresses
 * @param   (const t_ttc_packet_address* ) Address    !=NULL: 16/64 bit address + PanID to set; ==NULL: set broadcast address
 * @return  (e_ttc_packet_address_format)  ==0: packet does not store target address; ==ttc_packet_address_Target16: Address16 read; ==ttc_packet_address_Target64: Address64 read
 */
e_ttc_packet_address_format ttc_packet_address_target_set( t_ttc_packet* Packet, const t_ttc_packet_address* Address );
e_ttc_packet_address_format _driver_packet_address_target_set( t_ttc_packet* Packet, const t_ttc_packet_address* Address );
#define ttc_packet_address_target_set(Packet, Address) _driver_packet_address_target_set(Packet, Address)

/** Creates reply packet by copying source- to target-address.
 *
 * This function comes handy when you use a received packet to prepare and answer in the same memory buffer.
 * It will copy the source identfier into the target identifier and set the given source identifier.
 *
 * @param  Packet       any supported packet type containing 16- or 64-bit addresses
 * @param  NewSourceID  !=NULL: pointer to 16- or 64-bit identifier to be written into source identifier ==NULL: TargetID is used as Source ID
 */
void ttc_packet_address_reply( t_ttc_packet* Packet, t_ttc_packet_address* NewSourceID );

/** Creates reply packet from given packet.
 *
 * This function comes handy when you use a received packet to prepare an answer in a second memory buffer.
 * It will copy the source identfier into the target identifier of a given second packet and set the given source identifier.
 * The low-level driver may copy extra informations like sequence number.
 *
 * @param  PacketRX     any received packet of supported type containing a 16- or 64-bit addresses
 * @param  PacketReply  fresh, empty packet which will be initialized as reply to PacketRX
 * @param  NewSourceID  !=NULL: pointer to 16- or 64-bit identifier to be written into source identifier; ==NULL: PacketRX.TargetID is used as Source ID
 */
void ttc_packet_address_reply2( t_ttc_packet* Packet, t_ttc_packet* PacketReply, t_ttc_packet_address* NewSourceID );
void _driver_packet_address_reply2( t_ttc_packet* Packet, t_ttc_packet* PacketReply );

/** Finds out IEEE network packet type from given binary packet
 *
 * @param Packet  binary packet being received from radio
 * @return        protocol independent type of packet (also stored in Packet->Meta.Category)
 * @return        also fills Packet->Meta.Category, Packet->Meta.AddressFormat, Packet->Meta.Type
 */
e_ttc_packet_category ttc_packet_identify( t_ttc_packet* Packet );
e_ttc_packet_category _driver_packet_identify( t_ttc_packet* Packet );
#define ttc_packet_identify(Packet) _driver_packet_identify(Packet)

/** Fills basic header fields of given packet according to given packet type
 *
 * @param MemoryBlock  buffer from a memory pool that will store new packet (return value from ttc_heap_pool_block_get() ) or any previously initialized packet (simply typecast to (t_ttc_heap_block_from_pool*) )
 * @param Type         ==0: don't initialize packet t_ttc_packet.MAC (faster); >0: packet to initialize t_ttc_packet.MAC for (-> e_ttc_packet_type)
 * @param Protocol     stored in first byte of payload (only if TTC_PACKET_SOCKET_BYTE==1 defined)
 * @return             size of initialized header (amount of bytes occupied by header at begin of packet)
 */
t_u8 ttc_packet_initialize( t_ttc_heap_block_from_pool* MemoryBlock, e_ttc_packet_type Type, e_ttc_packet_pattern Protocol );

/** Fills basic header fields of given packet according to given packet type
 *
 * @param Packet      network packet being returned by ttc_packet_get_empty_packet() before
 * @param Type        type of packet that is to be created (-> e_ttc_packet_type)
 * @return            size of initialized header (amount of bytes occupied by header at begin of packet)
 */
t_u8 _driver_packet_initialize( t_ttc_packet* Packet, e_ttc_packet_type Type );

/** Checks if given packet belongs to general class of acknowledge packets
 *
 * The actual check is implemented by current low-level driver.
 * Some ttc_packet low-level drivers may not know of acknowledge type packets.
 *
 * @param Packet  (t_ttc_packet*)     network packet to inspect
 * @return        (BOOL)              ==TRUE: given packet is an acknowledge type packet; ==FALSE: otherwise
 */
BOOL ttc_packet_is_acknowledge( t_ttc_packet* Packet );
BOOL _driver_packet_is_acknowledge( t_ttc_packet* Packet );
#define ttc_packet_is_acknowledge  _driver_packet_is_acknowledge  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Checks if given packet belongs to general class of command packets
 *
 * The actual check is implemented by current low-level driver.
 * Some ttc_packet low-level drivers may not know of command type packets.
 *
 * @param Packet  (t_ttc_packet*)     network packet to inspect
 * @return        (BOOL)              ==TRUE: given packet is a command type packet; ==FALSE: otherwise
 */
BOOL ttc_packet_is_command( t_ttc_packet* Packet );
BOOL _driver_packet_is_command( t_ttc_packet* Packet );
#define ttc_packet_is_command  _driver_packet_is_command  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Checks if given packet belongs to general class of data packets
 *
 * The actual check is implemented by current low-level driver.
 * Some ttc_packet low-level drivers may not know of data type packets.
 *
 * @param Packet  (t_ttc_packet*)     network packet to inspect
 * @return        (BOOL)              ==TRUE: given packet is a data type packet; ==FALSE: otherwise
 */
BOOL ttc_packet_is_data( t_ttc_packet* Packet );
BOOL _driver_packet_is_data( t_ttc_packet* Packet );
#define ttc_packet_is_data  _driver_packet_is_data  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Checks if given packet belongs to general class of beacon packets
 *
 * The actual check is implemented by current low-level driver.
 * Some ttc_packet low-level drivers may not know of beacon type packets.
 *
 * @param Packet  (t_ttc_packet*)     network packet to inspect
 * @return        (BOOL)              ==TRUE: given packet is an beacon type packet; ==FALSE: otherwise
 */
BOOL ttc_packet_is_beacon( t_ttc_packet* Packet );
BOOL _driver_packet_is_beacon( t_ttc_packet* Packet );
#define ttc_packet_is_beacon  _driver_packet_is_beacon  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Retrieves pointer to and size of payload area of an IEEE 802.15.4 network packet
 *
 * Different types of network packets with different sized headers exist.
 * This function finds start address and size of payload area in given packet.
 *
 * If TTC_PACKET_SOCKET_BYTE is defined as 1 then the protocol type is stored in first payload byte.
 * The protocol type from given Packet can be copied to *ProtocolPtr if a valid reference is given.
 *
 * Example Usage:

   // retrieving all data
   t_u8*                 Payload;
   e_ttc_packet_pattern Protocol;
   t_u16                 Size;
   ttc_packet_payload_get_pointer( PacketRX, &Payload, &Size, &Protocol );
   ...

   // retrieving only start of payload area
   t_u8*                 Payload;
   ttc_packet_payload_get_pointer( PacketRX, &Payload, NULL, NULL);
   ...

 * @param Packet      network packet being returned by ttc_packet_get_received_packet*() before
 * @param PayloadPtr  pointer to variable to load with pointer to payload area
 * @param SizePtr     ==NULL: Not used; !=NULL: if (Packet->MAC.Length==0) { *SizePtr = Available payload size; } else { *SizePtr = Used payload size }
 * @param ProtocolPtr ==NULL: Not used; !=NULL: store protocol type from first byte of payload in this variable (requires TTC_PACKET_SOCKET_BYTE defined ad 1)
 * @return            ==0: payload could be found successfully; errorcode otherwise
 */
e_ttc_packet_errorcode ttc_packet_payload_get_pointer( t_ttc_packet* Packet, t_u8** PayloadPtr, t_u16* SizePtr, e_ttc_packet_pattern* ProtocolPtr );
e_ttc_packet_errorcode _driver_packet_payload_get_pointer( t_ttc_packet* Packet, t_u8** PayloadPtr, t_u16* SizePtr, e_ttc_packet_pattern* ProtocolPtr );
#define ttc_packet_payload_get_pointer(Packet, PayloadPtr, SizePtr, ProtocolPtr) _driver_packet_payload_get_pointer(Packet, PayloadPtr, SizePtr, ProtocolPtr)

/** Calculates size of payload buffer in given packet
 *
 * Packet buffers are stored in memory pools of equal sized memory blocks allocated from heap.
 * Depending on its type, the packet header size varies. The remaining space in the memory
 * block can be used as payload area.
 *
 * @param Packet  network packet being returned by ttc_packet_get_empty_packet() before
 * @return        Maximum available size of payload area in given packet
 */
t_u16 ttc_packet_payload_get_size_max( t_ttc_packet* Packet );
t_u16 _driver_packet_payload_get_size_max( t_ttc_packet* Packet );
#define ttc_packet_payload_get_size_max(Packet) _driver_packet_payload_get_size_max(Packet)

/** Calculates current size of payload in given packet
 *
 * A packet consists of a header and its payload. The size of a packet is the sum of
 * packet header and payload. The Low level driver knows the size of packet header
 * and can subtract it from the total packet length.
 *
 * @param Packet  network packet being returned by ttc_packet_get_empty_packet() before
 * @return        Size of payload area in given packet
 */
t_u16 ttc_packet_payload_get_size( t_ttc_packet* Packet );
t_u16 _driver_packet_payload_get_size( t_ttc_packet* Packet );
#define ttc_packet_payload_get_size(Packet) _driver_packet_payload_get_size(Packet)

/** Set size of payload being loaded into packet
 *
 * Call this function to set total packet length according to size of its payload
 * @param Packet       (t_ttc_packet*)  network packet being returned by ttc_packet_get_empty_packet() before
 * @param PayloadSize  (t_u16)          actual size of used payload (0.. ttc_packet_payload_get_size_max(Packet))
 */
void s_ttc_packet_payloadet_size( t_ttc_packet* Packet, t_u16 PayloadSize );
void _driver_packet_payload_set_size( t_ttc_packet* Packet, t_u16 PayloadSize );
#if (TTC_ASSERT_PACKET < 1)
    #define s_ttc_packet_payloadet_size(Packet, PayloadSize) _driver_packet_payload_set_size(Packet, PayloadSize)
#endif

/** Extracts size of header from given packet
 *
 * Each packet type defines its own header with a unique combination of fields.
 * The low-level driver knows all packet types and their corresponding header struct.
 * The header size may be used to calculate the minimum size of a memory block for a certain payload.
 * You may also use TTC_PACKET_MAX_HEADER_SIZE to calculate your maximum memory usage in advance.
 *
 * @param Packet  network packet being returned by ttc_packet_get_empty_packet() before
 * @return       (t_u8) Size of header of given Packet
 */
t_u8 ttc_packet_mac_header_get_size( t_ttc_packet* Packet );
t_u8 _driver_packet_mac_header_get_size( t_ttc_packet* Packet );

/** Returns size of header of packets of given type.
 *
 * Each packet type defines its own header with a unique combination of fields.
 * The low-level driver knows all packet types and their corresponding header struct.
 * The header size may be used to calculate the minimum size of a memory block for a certain payload.
 * You may also use TTC_PACKET_MAX_HEADER_SIZE to calculate your maximum memory usage in advance.
 *
 * @param Type (e_ttc_packet_type)  valid network packet type
 * @return     (t_u8)               Size of header of given Packet
 */
t_u8 ttc_packet_mac_header_get_type_size( e_ttc_packet_type Type );
t_u8 _driver_packet_mac_header_get_type_size( e_ttc_packet_type Type );
#define ttc_packet_mac_header_get_type_size  _driver_packet_mac_header_get_type_size  // enable to directly forward function call to low-level driver (remove function definition before!)

typedef struct { // select debug details
    unsigned   SourceID : 1; // =1: source identification
    unsigned   TargetID : 1; // =1: destination identification
    unsigned   Payload  : 1; // =1: payload content
    unsigned   Size     : 1; // =1: size of packet and payload
    unsigned   Type     : 1; // =1: debug packet type (provided by low-level driver)

    //...
} __attribute__( ( packed ) ) t_ttc_packet_debug;

/** Appends human readable summary of given packet to given string buffer
 *
 * @param Packet      network packet to analyze
 * @param Buffer      memory buffer to append string to
 * @param BufferSize  pointer to variable storing remaining space in buffer (will be updated)
 * @param Debug       defines which packet details to debug
 * @return            amount of bytes being added to Buffer[]
 */
t_u16 ttc_packet_debug( t_ttc_packet* Packet, t_u8* Buffer, t_base* BufferSize, t_ttc_packet_debug Debug );

/** Calculates required size of memory buffer to store packets of given binary size.
 *
 * In memory, each packet stores an extra meta header. This header must be added to the binary size
 * to store such a packet in a buffer. You may want to use this function when creating a memory pool to use
 * for network packets. See ttc_radio_init() for an example.
 *
 * @param BinarySize (t_u16)  Maximum expected size of MAC part of packets. This is the part being send over transmission lines.
 * @return           (t_u16)  Required size of memory buffers storing MAC and Meta part of packets.
 */
t_u16 ttc_packet_calculate_buffer_size( t_u16 BinarySize );

/** Checks if given packet belongs to given protocol type
 *
 * Each ttc_packet can store a single extra byte in front of its payload to distinguish
 * packets of different protocols that share the same communication channel.
 * For this, TTC_PACKET_SOCKET_BYTE==1 must be defined in your makefile.
 *
 * @param Packet   (t_ttc_packet*)          network packet to inspect
 * @param Protocol (e_ttc_packet_pattern)  ==0: any type; <ttc_packet_pattern_protocol_Begin_AnyOf: individual protocol; >ttc_packet_pattern_protocol_Begin_AnyOf: search pattern matching a group of types (e.g. E_ttc_packet_pattern_socket_AnyOf_Application for all application type protocols)
 * @return         (BOOL)                   ==TRUE: given Packet belongs to given protocol
 */
BOOL ttc_packet_pattern_matches( t_ttc_packet* Packet, e_ttc_packet_pattern Pattern );

/** Checks if given protocl matches to given protocol pattern
 *
 * This is a helper function for ttc_packet_pattern_matches().
 * You may want to call it when checking multiple patterns agains same packet.
 *
 * Example Usage:
 *    // create + initialize variables
 *    t_ttc_packet* Packet;
 *    e_ttc_packet_pattern Pattern1;
 *    e_ttc_packet_pattern Pattern2;
 *    ...
 *
 *    // extracting packet type once
 *    e_ttc_packet_type Protocol = s_ttc_packetocket_get( Packet );
 *
 *    // checking multiple patterns agains same packet type
 *    BOOL Matches = ttc_packet_pattern_matches_socket(Protocol, Pattern1) ||
 *                   ttc_packet_pattern_matches_socket(Protocol, Pattern2) ||
 *                   ...;
 *
 * Note: This function can only check E_ttc_packet_pattern_socket_* patterns.
 *       For E_ttc_packet_pattern_type_* patterns, you have to use ttc_packet_pattern_matches_type() instead!
 *
 * @param Protocol (e_ttc_packet_pattern)  return value from s_ttc_packetocket_get() (always <ttc_packet_pattern_protocol_Begin_AnyOf)
 * @param Pattern  (e_ttc_packet_pattern)  ==0: match any type; E_ttc_packet_pattern_socket_Begin ... E_ttc_packet_pattern_socket_Begin_AnyOf: individual protocol; >ttc_packet_pattern_protocol_Begin_AnyOf: search pattern matching a group of types (e.g. E_ttc_packet_pattern_socket_AnyOf_Application for all application type protocols)
 * @return         (BOOL)                  ==TRUE: given protocol type matches to given protocol pattern; FALSE otherwise
 */
BOOL ttc_packet_pattern_matches_socket( e_ttc_packet_pattern Protocol, e_ttc_packet_pattern Pattern );

/** Checks if given packet pattern matches given packet type
 *
 * This is a helper function for ttc_packet_pattern_matches().
 * You may want to call it when checking multiple patterns agains same packet.
 *
 * Example Usage:
 *    // create + initialize variables
 *    t_ttc_packet* Packet;
 *    e_ttc_packet_pattern Pattern1;
 *    e_ttc_packet_pattern Pattern2;
 *    ...
 *
 *    // extracting packet type once
 *    e_ttc_packet_type Type = E_ttc_packet_type_get( Packet );
 *
 *    // checking multiple patterns agains same packet type
 *    BOOL Matches = ttc_packet_pattern_matches_type(Type, Pattern1) ||
 *                   ttc_packet_pattern_matches_type(Type, Pattern2) ||
 *                   ...;
 *
 * Note: This function can only check E_ttc_packet_pattern_type_* patterns.
 *       For E_ttc_packet_pattern_socket_* patterns, you have to use ttc_packet_pattern_matches_socket() instead!
 *
 * @param Type     (e_ttc_packet_type )    type of network packet ( return value from E_ttc_packet_type_get() )
 * @param Pattern  (e_ttc_packet_pattern)  ==0: match any type; E_ttc_packet_pattern_type_Begin ... E_ttc_packet_pattern_type_Begin_AnyOf
 * @return         (BOOL)                  ==TRUE: given packet type matches to given type pattern; FALSE otherwise
 */
BOOL ttc_packet_pattern_matches_type( e_ttc_packet_type Type, e_ttc_packet_pattern Pattern );
BOOL _driver_packet_pattern_matches_type( e_ttc_packet_type Type, e_ttc_packet_pattern Pattern );
#define ttc_packet_pattern_matches_type _driver_packet_pattern_matches_type

/** Extracts type of protocol to which given packet belongs to
 *
 * Each ttc_packet can store a single extra byte in front of its payload to distinguish
 * packets of different protocols that share the same communication channel.
 *
 * Note: TTC_PACKET_SOCKET_BYTE must be defined in your makefile as 1 to reserve extra byte in payload area!
 *       COMPILE_OPTS += -DTTC_PACKET_SOCKET_BYTE
 *       If TTC_PACKET_SOCKET_BYTE==0 then this function will always return 0.
 *
 * @param Packet   (t_ttc_packet*)  network packet to inspect
 * @return (e_ttc_packet_pattern)  ==0: any type; <ttc_packet_pattern_protocol_Begin_AnyOf: individual protocol; >ttc_packet_pattern_protocol_Begin_AnyOf: search pattern matching a group of types (e.g. E_ttc_packet_pattern_socket_AnyOf_Application for all application type protocols)
 */
e_ttc_packet_pattern s_ttc_packetocket_get( t_ttc_packet* Packet );

/** Write given protocol type into first byte of packet payload
 *
 * ADD_MORE_DESCRIPTION_HERE
 *
 * Note: TTC_PACKET_SOCKET_BYTE must be defined in your makefile as 1 to reserve extra byte in payload area!
 *       COMPILE_OPTS += -DTTC_PACKET_SOCKET_BYTE
 *       If TTC_PACKET_SOCKET_BYTE==0 then this function will not do anything.
 *
 * @param Packet   (t_ttc_packet*)         network packet to modify
 * @param SocketID (e_ttc_packet_pattern)  one from between E_ttc_packet_pattern_socket_Begin ... E_ttc_packet_pattern_socket_Begin_AnyOf
 */
void s_ttc_packetocket_set( t_ttc_packet* Packet, e_ttc_packet_pattern SocketID );

/** Returns type of given packet
 *
 * @param Packet  (t_ttc_packet*)     network packet to inspect
 * @return        (e_ttc_packet_type) identified type (specific low-level type)
 */
e_ttc_packet_type E_ttc_packet_type_get( t_ttc_packet* Packet );
e_ttc_packet_type _driver_packet_type_get( t_ttc_packet* Packet );
#define E_ttc_packet_type_get _driver_packet_type_get

/** Reads sequence number from given packet
 *
 * Some packet formats (E.g. IEEE 802.15.4) store a special key in each packet
 * to identify to which packet a reply corresponds to.
 *
 * @param Packet  (t_ttc_packet*)     network packet to inspect
 * @return        (t_u8)              Sequence number read from packet header
 */
t_u8 s_ttc_packetequence_number_get( t_ttc_packet* Packet );
t_u8 _driver_packet_sequence_number_get( t_ttc_packet* Packet );
#define s_ttc_packetequence_number_get  _driver_packet_sequence_number_get  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Writes given sequence number into given packet
 *
 * Some packet formats (E.g. IEEE 802.15.4) store a special key in each packet
 * to identify to which packet a reply corresponds to.
 *
 * @param Packet      (t_ttc_packet*)     network packet to inspect
 * @param SequenceNo  (t_u8)              sequence number to write into packet header
 */
void s_ttc_packetequence_number_set( t_ttc_packet* Packet, t_u8 SequenceNo );
void _driver_packet_sequence_number_set( t_ttc_packet* Packet, t_u8 SequenceNo );
#define s_ttc_packetequence_number_set  _driver_packet_sequence_number_set  // enable to directly forward function call to low-level driver (remove function definition before!)

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_packet_ are passed to interfaces/ttc_packet_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl packet UPDATE
 */


//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_PACKET_H
