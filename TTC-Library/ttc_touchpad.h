#ifndef TTC_TOUCHPAD_H
#define TTC_TOUCHPAD_H
/** { ttc_touchpad.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for TOUCHPAD devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_TOUCHPAD(tc_touchpad_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_touchpad_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_touchpad_init(LogicalIndex);
 *  4) use:         ttc_touchpad_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level touchpad and application.
 *
 *  Created from template ttc_device.h revision 30 at 20150316 07:48:52 UTC
 *
 *  Authors: Patrick von Poblotzki, Gregor Rebel
 *
 *
 *  How to configure touchpad devices
 *
 *  Several types of touchpad devices do exist. Read the configuration howto of the
 *  actual low-level driver for additional configuration settings.
 *  The configuration options are ideally being placed into a
 *  makefile.100_board_* or makefile.150_board_extension_* file to avoid double definitions.
 *
 *  The high-level touchpad driver needs to know
 *  1) which low-level driver is to be used
 *     TTC_TOUCHPAD<n> must be defined to be one from ttc_touchpad_types.h:e_ttc_touchpad_architecture
 *  2) to which dimension and depth should the touch input be scaled
 *     TTC_TOUCHPAD<n>_WIDTH   scale horizontal touchpad value to this size
 *     TTC_TOUCHPAD<n>_HEIGHT  scale vertical touchpad value to this size
 *     TTC_TOUCHPAD<n>_DEPTH   scale touchpad pressure value to this amount of bits (datatype to use)
 *
 *  Example makefile lines
    COMPILE_OPTS += -DTTC_TOUCHPAD1=ta_touchpad_analog4
    COMPILE_OPTS += -DTTC_TOUCHPAD1_WIDTH=240
    COMPILE_OPTS += -DTTC_TOUCHPAD1_HEIGHT=320
    COMPILE_OPTS += -DTTC_TOUCHPAD1_DEPTH=24
  *
}*/

#ifndef EXTENSION_ttc_touchpad
#error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_touchpad.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_touchpad.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_touchpad_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************
#if TTC_TOUCH_ROTATION_CLOCKWISE == 90
#    define ROTATED_TOUCH_X(X, Y) (TTC_GFX1_WIDTH - 1 - Y)
#endif
#if TTC_TOUCH_ROTATION_CLOCKWISE == 180
#    define ROTATED_TOUCH_X(X, Y) (TTC_GFX1_WIDTH - 1 - X)
#endif
#if TTC_TOUCH_ROTATION_CLOCKWISE == 270
#    define ROTATED_TOUCH_X(X, Y) (Y)
#endif
#ifndef ROTATED_TOUCH_X
#    define ROTATED_TOUCH_X(X, Y) (X)
#endif
#if TTC_TOUCH_ROTATION_CLOCKWISE == 90
#    define ROTATED_TOUCH_Y(X, Y) (X)
#endif
#if TTC_TOUCH_ROTATION_CLOCKWISE == 180
#    define ROTATED_TOUCH_Y(X, Y) (TTC_GFX1_HEIGHT - 1 - Y)
#endif
#if TTC_TOUCH_ROTATION_CLOCKWISE == 270
#    define ROTATED_TOUCH_Y(X, Y) (TTC_GFX1_HEIGHT - 1 - X)
#endif
#ifndef ROTATED_TOUCH_Y
#    define ROTATED_Y(X, Y) (Y)
#endif
//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level touchpad only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * touchpad devices on all supported architectures.
 * Check touchpad/touchpad_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares touchpad Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_touchpad_prepare();
void _driver_touchpad_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_touchpad_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_TOUCHPAD_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_touchpad_config* ttc_touchpad_get_configuration( t_u8 LogicalIndex );

/** Sets constants offset to adjust touchpad
 *
 * @param LogicalIndex    index of device to adjust (1..ttc_TOUCHPAD_get_max_LogicalIndex())
 * @param AdjustX         signed offset to be added to Position_X
 * @param AdjustY         signed offset to be added to Position_Y
 */
void ttc_touchpad_adjust( t_u8 LogicalIndex, t_s16 AdjustX, t_s16 AdjustY );

/** reads position data from touchpad into Config
 *
 * @param LogicalIndex    index of device to init (1..ttc_TOUCHPAD_get_max_LogicalIndex())
 * @return                == ttu_None: no update; != ttu_None: position in configuration has been updated
 */
e_ttc_touchpad_update ttc_touchpad_check( t_u8 LogicalIndex );

/** fills out given Config with maximum valid values for indexed TOUCHPAD
 * @param LogicalIndex    index of device to init (1..ttc_TOUCHPAD_get_max_LogicalIndex())
 * @return                pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_touchpad_config* ttc_touchpad_get_features( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TOUCHPAD_get_max_LogicalIndex())
 * @return                == 0: touchpad device has been initialized successfully; != 0: error-code
 */
e_ttc_touchpad_errorcode ttc_touchpad_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_TOUCHPAD_get_max_LogicalIndex())
 */
void ttc_touchpad_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_touchpad_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of touchpad device (1..ttc_touchpad_get_max_index() )
 * @return                == 0: default values have been loaded successfully for indexed touchpad device; != 0: error-code
 */
e_ttc_touchpad_errorcode  ttc_touchpad_load_defaults( t_u8 LogicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TOUCHPAD_get_max_LogicalIndex())
 */
void ttc_touchpad_reset( t_u8 LogicalIndex );

/** Run touchpad self calibration now
 *
 * Calibration is automatically run at touchpad initialization.
 * Some touchpads float over time and need periodic recalibration.
 *
 * @param LogicalIndex  logical index of touchpad device (1..ttc_touchpad_get_max_index() )
 */
void ttc_touchpad_calibrate( t_u8 LogicalIndex );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_touchpad(t_u8 LogicalIndex)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_touchpad_ are passed to interfaces/ttc_touchpad_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl touchpad UPDATE
 */


/** fills out given Config with maximum valid values for indexed TOUCHPAD
 * @param Config  = pointer to struct t_ttc_touchpad_config
 * @return        pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_touchpad_config* _driver_touchpad_get_features( t_ttc_touchpad_config* Config );

/** shutdown single TOUCHPAD unit device
 * @param Config        pointer to struct t_ttc_touchpad_config
 * @return              == 0: TOUCHPAD has been shutdown successfully; != 0: error-code
 */
e_ttc_touchpad_errorcode _driver_touchpad_deinit( t_ttc_touchpad_config* Config );

/** initializes single TOUCHPAD unit for operation
 * @param Config        pointer to struct t_ttc_touchpad_config
 * @return              == 0: TOUCHPAD has been initialized successfully; != 0: error-code
 */
e_ttc_touchpad_errorcode _driver_touchpad_init( t_ttc_touchpad_config* Config );

/** loads configuration of indexed TOUCHPAD unit with default values
 * @param Config        pointer to struct t_ttc_touchpad_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_touchpad_errorcode _driver_touchpad_load_defaults( t_ttc_touchpad_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_touchpad_config
 */
void _driver_touchpad_reset( t_ttc_touchpad_config* Config );

/** Run touchpad self calibration now
 *
 * Calibration is automatically run at touchpad initialization.
 * Some touchpads float over time and need periodic recalibration.
 *
 * @param Config        pointer to struct t_ttc_touchpad_config
 */
void _driver_touchpad_calibrate( t_ttc_touchpad_config* Config );

/** reads position data from touchpad into Config
 *
 * @param Config        pointer to struct t_ttc_touchpad_config
 */
void _driver_touchpad_check( t_ttc_touchpad_config* Config );
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_TOUCHPAD_H
