/** { ttc_timer_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for TIMER device.
 *  Structures, Enums and Defines being required by both, high- and low-level timer.
 *
 *  Created from template ttc_device_types.h revision 22 at 20140415 05:28:31 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_TIMER_TYPES_H
#define TTC_TIMER_TYPES_H

//{ Includes *************************************************************

#include "compile_options.h"
#include "ttc_basic_types.h"
//X #include "ttc_interrupt_types.h"

#ifdef EXTENSION_timer_stm32f1xx
    #include "timer/timer_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_timer_stm32w1xx
    #include "timer/timer_stm32w1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_timer_stm32l1xx
    #include "timer/timer_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_timer_stm32f0xx
    #include "timer/timer_stm32f0xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

// TTC_TIMERn has to be defined as constant by makefile.100_board_*

#define TTC_TIMER_AMOUNT 10

/* Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_TIMER 0  # disable assert handling for timer devices
 *
 */
#ifndef TTC_ASSERT_TIMER    // any previous definition set (Makefile)?
    #define TTC_ASSERT_TIMER 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_TIMER == 1)  // use Assert()s in TIMER code (somewhat slower but alot easier to debug)
    #define Assert_TIMER(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in TIMER code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_TIMER(Condition, ErrorCode)
#endif

/** Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_timer_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_timer_architecture
    #warning Missing low-level definition for t_ttc_timer_architecture (using default)
    #define t_ttc_timer_architecture void*
#endif

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_timer_physical_index (starts at zero!)
    //
    // You may use this enum to assign physical index to logical TTC_TIMERn constants in your makefile
    //
    // E.g.: Defining physical device #3 of timer as first logical device TTC_TIMER1
    //       COMPILE_OPTS += -DTTC_TIMER1=INDEX_TIMER3
    //
    INDEX_TIMER1,
    INDEX_TIMER2,
    INDEX_TIMER3,
    INDEX_TIMER4,
    INDEX_TIMER5,
    INDEX_TIMER6,
    INDEX_TIMER7,
    INDEX_TIMER8,
    INDEX_TIMER9,
    // add more if needed

    INDEX_TIMER_ERROR
} e_ttc_timer_physical_index;
typedef enum {    // e_ttc_timer_errorcode     return codes of TIMER devices
    ec_timer_OK = 0,

    // other warnings go here..

    ec_timer_ERROR,                  // general failure
    ec_timer_NULL,                   // NULL pointer not accepted
    ec_timer_DeviceNotFound,         // corresponding device could not be found
    ec_timer_InvalidImplementation,  // internal self-test failed
    ec_timer_InvalidConfiguration,   // sanity check of device configuration failed
    ec_timer_DeviceInUse,
    // other failures go here..
    ec_timer_unknown                // no valid errorcodes past this entry
} e_ttc_timer_errorcode;
typedef enum {    // e_ttc_timer_architecture  types of architectures supported by TIMER driver
    ta_timer_None,           // no architecture selected


    ta_timer_stm32w1xx, // automatically added by ./create_DeviceDriver.pl
    ta_timer_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
    ta_timer_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
    ta_timer_stm32f0xx, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_timer_unknown        // architecture not supported
} e_ttc_timer_architecture;
typedef struct s_ttc_timer_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_timer_init()
    //       and after ttc_timer_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    e_ttc_timer_architecture Architecture; // type of architecture used for current timer device

    // low-level configuration (structure defined by low-level driver)
    t_ttc_timer_architecture* LowLevelConfig;


    // architecture dependend TIMER configuration (each TIMER requires its own!)
    t_ttc_timer_architecture TIMER_Arch;

    void ( *Function )();
    void* Argument;
    t_base TimeStart;
    t_base TimePeriod;

    e_ttc_gpio_pin CH1;                    // port pin for Channel 1
    e_ttc_gpio_pin CH2;                    // port pin for Channel 2
    e_ttc_gpio_pin CH3;                    // port pin for Channel 3
    e_ttc_gpio_pin CH4;                    // port pin for Channel 4

    e_ttc_gpio_pin PWM1;                    // port pin for Channel 1
    e_ttc_gpio_pin PWM2;                    // port pin for Channel 2
    e_ttc_gpio_pin PWM3;                    // port pin for Channel 3
    e_ttc_gpio_pin PWM4;                    // port pin for Channel 4

    struct s_ttc_timer_config* Next; //pointer on the next Timer structure

    // activity functions for timer expiring
    // != NULL: registered function is called from _ttc_timer_isr() after a timer expired
    // Note: Registered function is called directly from interrupt service routine!
    //    void (*activity_update_isr)(struct s_ttc_timer_config*);

    // handlers of registered interrupts
    void* Interrupt_Updating;       // triggered when an update is completed

    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_TIMER1, ...)
    t_u8  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)

    union  { // generic configuration bits common for all low-level Drivers
        t_u8 All;
        struct {
            unsigned Initialized        : 1;  // ==1: device has been initialized successfully
            unsigned OutputPWM          : 1;  // ==1: enable PWM output for this timer
            unsigned PWM_Channel        : 2;  // channel # to use for PWM output (0..3)
            unsigned Reserved1          : 4; // pad to 8 bits
        } Bits;
    } Flags;
    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_timer_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_TIMER_TYPES_H
