/** { ttc_slam_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for SLAM device.
 *  Structures, Enums and Defines being required by both, high- and low-level slam.
 *  This file does not provide function declarations!
 *
 *  Created from template ttc_device_types.h revision 38 at 20160606 11:58:15 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_SLAM_TYPES_H
#define TTC_SLAM_TYPES_H

//{ Includes *************************************************************
//
// _types.h Header files mayshould include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_slam.h" or "ttc_slam.c"
//
#include "ttc_basic_types.h"
#include "ttc_math_types.h"
#include "ttc_list_types.h"
#include "ttc_heap_types.h"
#include "compile_options.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//{ Defines **************************************************************

#define TTC_SLAM_UNDEFINED_VALUE  TTC_MATH_CONST_NAN // used for uninitialized coordinates
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines

#ifdef EXTENSION_slam_simple_2d
    #include "slam/slam_simple_2d_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Static Configuration *************************************************

/** {  TTC_SLAMn has to be defined as constant by makefile.100_board_*
 *
 * Here we count how much devices have been defined and if all lower index devices have been defined too.
 */
#ifndef TTC_SLAM_AMOUNT //{ 10
    #ifdef TTC_SLAM10
        #ifndef TTC_SLAM9
            #error TTC_SLAM10 is defined, but not TTC_SLAM9 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM8
            #error TTC_SLAM10 is defined, but not TTC_SLAM8 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM7
            #error TTC_SLAM10 is defined, but not TTC_SLAM7 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM6
            #error TTC_SLAM10 is defined, but not TTC_SLAM6 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM5
            #error TTC_SLAM10 is defined, but not TTC_SLAM5 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM4
            #error TTC_SLAM10 is defined, but not TTC_SLAM4 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM3
            #error TTC_SLAM10 is defined, but not TTC_SLAM3 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM2
            #error TTC_SLAM10 is defined, but not TTC_SLAM2 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM1
            #error TTC_SLAM10 is defined, but not TTC_SLAM1 - all lower TTC_SLAMn must be defined!
        #endif

        #define TTC_SLAM_AMOUNT 10
    #endif
#endif //}
#ifndef TTC_SLAM_AMOUNT //{ 9
    #ifdef TTC_SLAM9
        #ifndef TTC_SLAM8
            #error TTC_SLAM9 is defined, but not TTC_SLAM8 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM7
            #error TTC_SLAM9 is defined, but not TTC_SLAM7 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM6
            #error TTC_SLAM9 is defined, but not TTC_SLAM6 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM5
            #error TTC_SLAM9 is defined, but not TTC_SLAM5 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM4
            #error TTC_SLAM9 is defined, but not TTC_SLAM4 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM3
            #error TTC_SLAM9 is defined, but not TTC_SLAM3 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM2
            #error TTC_SLAM9 is defined, but not TTC_SLAM2 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM1
            #error TTC_SLAM9 is defined, but not TTC_SLAM1 - all lower TTC_SLAMn must be defined!
        #endif

        #define TTC_SLAM_AMOUNT 9
    #endif
#endif //}
#ifndef TTC_SLAM_AMOUNT //{ 8
    #ifdef TTC_SLAM8
        #ifndef TTC_SLAM7
            #error TTC_SLAM8 is defined, but not TTC_SLAM7 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM6
            #error TTC_SLAM8 is defined, but not TTC_SLAM6 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM5
            #error TTC_SLAM8 is defined, but not TTC_SLAM5 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM4
            #error TTC_SLAM8 is defined, but not TTC_SLAM4 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM3
            #error TTC_SLAM8 is defined, but not TTC_SLAM3 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM2
            #error TTC_SLAM8 is defined, but not TTC_SLAM2 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM1
            #error TTC_SLAM8 is defined, but not TTC_SLAM1 - all lower TTC_SLAMn must be defined!
        #endif

        #define TTC_SLAM_AMOUNT 8
    #endif
#endif //}
#ifndef TTC_SLAM_AMOUNT //{ 7
    #ifdef TTC_SLAM7
        #ifndef TTC_SLAM6
            #error TTC_SLAM7 is defined, but not TTC_SLAM6 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM5
            #error TTC_SLAM7 is defined, but not TTC_SLAM5 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM4
            #error TTC_SLAM7 is defined, but not TTC_SLAM4 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM3
            #error TTC_SLAM7 is defined, but not TTC_SLAM3 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM2
            #error TTC_SLAM7 is defined, but not TTC_SLAM2 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM1
            #error TTC_SLAM7 is defined, but not TTC_SLAM1 - all lower TTC_SLAMn must be defined!
        #endif

        #define TTC_SLAM_AMOUNT 7
    #endif
#endif //}
#ifndef TTC_SLAM_AMOUNT //{ 6
    #ifdef TTC_SLAM6
        #ifndef TTC_SLAM5
            #error TTC_SLAM6 is defined, but not TTC_SLAM5 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM4
            #error TTC_SLAM6 is defined, but not TTC_SLAM4 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM3
            #error TTC_SLAM6 is defined, but not TTC_SLAM3 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM2
            #error TTC_SLAM6 is defined, but not TTC_SLAM2 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM1
            #error TTC_SLAM6 is defined, but not TTC_SLAM1 - all lower TTC_SLAMn must be defined!
        #endif

        #define TTC_SLAM_AMOUNT 6
    #endif
#endif //}
#ifndef TTC_SLAM_AMOUNT //{ 5
    #ifdef TTC_SLAM5
        #ifndef TTC_SLAM4
            #error TTC_SLAM5 is defined, but not TTC_SLAM4 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM3
            #error TTC_SLAM5 is defined, but not TTC_SLAM3 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM2
            #error TTC_SLAM5 is defined, but not TTC_SLAM2 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM1
            #error TTC_SLAM5 is defined, but not TTC_SLAM1 - all lower TTC_SLAMn must be defined!
        #endif

        #define TTC_SLAM_AMOUNT 5
    #endif
#endif //}
#ifndef TTC_SLAM_AMOUNT //{ 4
    #ifdef TTC_SLAM4
        #ifndef TTC_SLAM3
            #error TTC_SLAM4 is defined, but not TTC_SLAM3 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM2
            #error TTC_SLAM4 is defined, but not TTC_SLAM2 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM1
            #error TTC_SLAM4 is defined, but not TTC_SLAM1 - all lower TTC_SLAMn must be defined!
        #endif

        #define TTC_SLAM_AMOUNT 4
    #endif
#endif //}
#ifndef TTC_SLAM_AMOUNT //{ 3
    #ifdef TTC_SLAM3

        #ifndef TTC_SLAM2
            #error TTC_SLAM3 is defined, but not TTC_SLAM2 - all lower TTC_SLAMn must be defined!
        #endif
        #ifndef TTC_SLAM1
            #error TTC_SLAM3 is defined, but not TTC_SLAM1 - all lower TTC_SLAMn must be defined!
        #endif

        #define TTC_SLAM_AMOUNT 3
    #endif
#endif //}
#ifndef TTC_SLAM_AMOUNT //{ 2
    #ifdef TTC_SLAM2

        #ifndef TTC_SLAM1
            #error TTC_SLAM2 is defined, but not TTC_SLAM1 - all lower TTC_SLAMn must be defined!
        #endif

        #define TTC_SLAM_AMOUNT 2
    #endif
#endif //}
#ifndef TTC_SLAM_AMOUNT //{ 1
    #ifdef TTC_SLAM1
        #define TTC_SLAM_AMOUNT 1
    #else
        #if 1 // missing definition: use default value
            #      warning Missing definition for TTC_SLAM1. Using default type. Add valid definition to your makefile to get rid if this message!
            #define TTC_SLAM_AMOUNT 1
            #define TTC_SLAM1 ta_slam_simple_2d
        #else // no devices defined at all
            #define TTC_SLAM_AMOUNT 0
        #endif
    #endif
#endif //}

//}
/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_SLAM 0#         disable default asserts for slam driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_SLAM_EXTRA 1#   enable extra asserts for slam driver
 *
 */
#ifndef TTC_ASSERT_SLAM    // any previous definition set (Makefile)?
    #define TTC_ASSERT_SLAM 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_SLAM == 1)  // use Assert()s in SLAM code (somewhat slower but alot easier to debug)
    #define Assert_SLAM(Condition, Origin)        if (! (Condition) ) { ttc_assert_halt_origin(Origin); }
    #define Assert_SLAM_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_SLAM_Readable(Address, Origin) Assert_Readable(Address, Origin)
    #define TTC_SLAM_VOLATILE                     volatile
#else  // use no default Assert()s in SLAM code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_SLAM(Condition, Origin)
    #define Assert_SLAM_Writable(Address, Origin)
    #define Assert_SLAM_Readable(Address, Origin)
    #define TTC_SLAM_VOLATILE
#endif

#ifndef TTC_ASSERT_SLAM_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_SLAM_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_SLAM_EXTRA == 1)  // use Assert()s in SLAM code (somewhat slower but alot easier to debug)
    #define Assert_SLAM_EXTRA(Condition, Origin) if (! (Condition) ) { ttc_assert_halt_origin(Origin); }
    #define Assert_SLAM_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_SLAM_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in SLAM code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_SLAM_EXTRA(Condition, Origin)
    #define Assert_SLAM_EXTRA_Writable(Address, Origin)
    #define Assert_SLAM_EXTRA_Readable(Address, Origin)
#endif
//}

/** { Check static configuration of slam devices
 *
 * Each TTC_SLAMn must be defined as one from e_ttc_slam_architecture.
 * Additional defines of type TTC_SLAMn_* may be added.
 *
 * The constants being checked here must be defined in your makefile.
 * Most constants are defined by EXTENSION_100_board_* or EXTENSION_150_board_extension_* files.
 */

#ifdef TTC_SLAM1

    // check extra defines for slam device #1 here
    #ifndef TTC_SLAM1_AMOUNT_RAW_MEASURES
        #define TTC_SLAM1_AMOUNT_RAW_MEASURES 4  // amount of range measures to store for each distance (higher values mean more ram usage, network traffic and more precise localization)
    #endif
#endif
#ifdef TTC_SLAM2

    // check extra defines for slam device #2 here
    #ifndef TTC_SLAM2_AMOUNT_RAW_MEASURES
        #define TTC_SLAM2_AMOUNT_RAW_MEASURES 4  // amount of range measures to store for each distance (higher values mean more ram usage, network traffic and more precise localization)
    #endif

#endif
#ifdef TTC_SLAM3

    // check extra defines for slam device #3 here
    #ifndef TTC_SLAM3_AMOUNT_RAW_MEASURES
        #define TTC_SLAM3_AMOUNT_RAW_MEASURES 4  // amount of range measures to store for each distance (higher values mean more ram usage, network traffic and more precise localization)
    #endif

#endif
#ifdef TTC_SLAM4

    // check extra defines for slam device #4 here
    #ifndef TTC_SLAM4_AMOUNT_RAW_MEASURES
        #define TTC_SLAM4_AMOUNT_RAW_MEASURES 4  // amount of range measures to store for each distance (higher values mean more ram usage, network traffic and more precise localization)
    #endif

#endif
#ifdef TTC_SLAM5

    // check extra defines for slam device #5 here
    #ifndef TTC_SLAM5_AMOUNT_RAW_MEASURES
        #define TTC_SLAM5_AMOUNT_RAW_MEASURES 4  // amount of range measures to store for each distance (higher values mean more ram usage, network traffic and more precise localization)
    #endif

#endif
#ifdef TTC_SLAM6

    // check extra defines for slam device #6 here
    #ifndef TTC_SLAM6_AMOUNT_RAW_MEASURES
        #define TTC_SLAM6_AMOUNT_RAW_MEASURES 4  // amount of range measures to store for each distance (higher values mean more ram usage, network traffic and more precise localization)
    #endif

#endif
#ifdef TTC_SLAM7

    // check extra defines for slam device #7 here
    #ifndef TTC_SLAM7_AMOUNT_RAW_MEASURES
        #define TTC_SLAM7_AMOUNT_RAW_MEASURES 4  // amount of range measures to store for each distance (higher values mean more ram usage, network traffic and more precise localization)
    #endif

#endif
#ifdef TTC_SLAM8

    // check extra defines for slam device #8 here
    #ifndef TTC_SLAM8_AMOUNT_RAW_MEASURES
        #define TTC_SLAM8_AMOUNT_RAW_MEASURES 4  // amount of range measures to store for each distance (higher values mean more ram usage, network traffic and more precise localization)
    #endif

#endif
#ifdef TTC_SLAM9

    // check extra defines for slam device #9 here
    #ifndef TTC_SLAM9_AMOUNT_RAW_MEASURES
        #define TTC_SLAM9_AMOUNT_RAW_MEASURES 4  // amount of range measures to store for each distance (higher values mean more ram usage, network traffic and more precise localization)
    #endif

#endif
#ifdef TTC_SLAM10

    // check extra defines for slam device #10 here
    #ifndef TTC_SLAM10_AMOUNT_RAW_MEASURES
        #define TTC_SLAM10_AMOUNT_RAW_MEASURES 4  // amount of range measures to store for each distance (higher values mean more ram usage, network traffic and more precise localization)
    #endif

#endif
//}

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_slam_errorcode       return codes of SLAM devices
    ec_slam_OK = 0,

    // other warnings go here..

    ec_slam_ERROR,                  // general failure
    ec_slam_NULL,                   // NULL pointer not accepted
    ec_slam_DeviceNotFound,         // corresponding device could not be found
    ec_slam_InvalidConfiguration,   // sanity check of device configuration failed
    ec_slam_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..

    ec_slam_unknown                // no valid errorcodes past this entry
} e_ttc_slam_errorcode;

typedef enum {   // e_ttc_slam_architecture    types of architectures supported by SLAM driver
    ta_slam_None = 0,       // no architecture selected


    ta_slam_simple_2d, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_slam_unknown        // architecture not supported
} e_ttc_slam_architecture;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef union {  // architecture dependent configuration

    void* unknown;
#ifdef EXTENSION_slam_simple_2d
    t_slam_simple_2d_config simple_2d;  // automatically added by ./create_DeviceDriver.pl
#endif
    //InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_slam_architecture;

typedef union {  // extra node data data managed by low-level driver (see low-level driver for structure)
    t_slam_simple_2d_node simple_2d;
    // add more low-level node datatypes here...
} u_ttc_slam_node_driver;

typedef struct s_ttc_slam_node {
    t_ttc_math_vector3d_xyz Position;  // Calculated 3D position
    // Note: Coordinates initialized as TTC_SLAM_UNDEFINED_VALUE during ttc_slam_reset().
    // Note: Low-level driver shall use ttc_slam_update_coordinates() to update position!

    u_ttc_slam_node_driver  Driver;    // extra node data managed by low-level driver
    struct {
        unsigned DistancesComplete : 1; // ==1: node stores egnough distances to fixpoints to calculate its position
        unsigned PositionValid     : 1; // ==1: position contains valid coordinates; ==0: position of this node not yet calculated
    } Flags;
} t_ttc_slam_node;

typedef struct s_ttc_slam_distance {  // distance measure for a single node
    t_u16      NodeIndex;  // index number remote node
    ttm_number Distance;   // measured distance
} t_ttc_slam_distance;

typedef union {  // architecture dependent configuration

    //InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_slam_nodes;

typedef struct s_ttc_slam_features { // static minimum, maximum and default values of features of single slam

    // Add any amount of architecture independent values describing slam devices here.
    // You may also want to add a plausibility check for each value to _ttc_slam_configuration_check().

    // Flags of features being provided by low-level driver
    unsigned Coordinates_2D     : 1; // ==1: 2D node coordinates
    unsigned Coordinates_3D     : 1; // ==1: 3D node coordinates
    unsigned Placement_Simple   : 1; // ==1: nodes can be placed as described in slam_simple_2d.h
    unsigned Placement_Random   : 1; // ==1: node can be placed randomly

    t_u16    MaxAmountNodes;      // maximum allowed amount of nodes supported by low-level driver on current hardware
} __attribute__( ( __packed__ ) ) t_ttc_slam_features;

typedef struct s_ttc_slam_box { // 3D-box coordinates
    ttm_number X_Min; // minimum seen X-coordinate
    ttm_number X_Max; // maximum seen X-coordinate
    ttm_number Y_Min; // minimum seen Y-coordinate
    ttm_number Y_Max; // maximum seen Y-coordinate
    ttm_number Z_Min; // minimum seen Z-coordinate
    ttm_number Z_Max; // maximum seen Z-coordinate
} __attribute__( ( __packed__ ) ) t_ttc_slam_box;

typedef struct s_ttc_slam_config { //          architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_slam_init()
    //       and after ttc_slam_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // Hint: Order structure elements for optimal alignment.
    //       You should place smaller datatypes like t_u8 and t_u16 after all t_u32 and pointer types
    //       to prevent insertion of pad-bytes. Most architectures don't support placing 32-bit datatypes at
    //       unaligned address.

    struct { // Init: fields to be set by application before first calling ttc_states_init() --------------
        // Do not change these values after calling ttc_states_init()!

        t_u16 Amount_Nodes;      // amount of nodes in current setup (can be decreased but not increased after first initialization)
        t_u8  AmountRawMeasures; // amount of raw measures to store foreach distance (higher value increases RAM usage, network traffic and precision)

        // !=NULL: Pointer to function which will be called whenever a new mapping has been calculated.
        void ( *Function_MappingComplete )( struct s_ttc_slam_config* );

        struct  { // generic configuration bits common for all low-level Drivers
            unsigned Unused  : 1;  //
            // ToDo: additional high-level flags go here..
        } Flags;
    } Init;

    // Fields below are read-only for application! ---------------------------------------

    t_ttc_slam_node*           Nodes;                // array storing data of all nodes (structure known by low-level driver only)
    u_ttc_slam_architecture*   LowLevelConfig;       // low-level configuration (structure defined by low-level driver)
    const t_ttc_slam_features* Features;             // constant features of current low-level slam driver
    t_ttc_heap_pool            Pool_Distances;       // distances are stored in memory blocks being allocated from a pool (To be initialized by low-level driver!)
    t_ttc_slam_box             BoundingBox;          // coordinates of bounding box around all nodes (updated during ttc_slam_update_coordinates() )

    struct { // generic configuration bits common for all low-level Drivers
        unsigned Initialized          : 1;  // ==1: device has been initialized successfully
        // ToDo: additional high-level flags go here..

        unsigned Completed_Distances    : 1;  // ==1: stage 1: low-level driver got egnough distance data to calculate coordinates of all nodes
        unsigned Completed_Mapping      : 1;  // ==1: stage 2: low-level driver has successfully calculated mapping of all nodes (requires Completed_Distances==1)
    } Flags;

    // Last returned error code
    // IMPORTANT: Every ttc_slam_*() function that returns e_ttc_slam_errorcode has to update this value if it returns an error!
    e_ttc_slam_errorcode     LastError;

    t_u16                    Amount_Nodes_Initial;   // amount of nodes in current setup at first initialization
    e_ttc_slam_architecture  Architecture;           // type of implementation used for current slam setup
    t_u8                     LogicalIndex;           // automatically set: logical index of device to use (1 = TTC_SLAM1, ...)
    t_u8                     PhysicalIndex;          // automatically set: index of physical device to use (0 = slam #1, ...)

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_slam_config;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_SLAM_TYPES_H
