/** { ttc_network_types.h ************************************************

                           The ToolChain

   High-Level interface for NETWORK device.

   Structures, Enums and Defines being required by both, high- and low-level network.


   Authors:

}**/

#ifndef TTC_NETWORK_TYPES_H
#define TTC_NETWORK_TYPES_H

#include "ttc_basic_types.h"
#include "network/network_6lowpan_types.h"
#include "ttc_list_types.h"
#include "ttc_memory_types.h"

//{ Defines ***************************************************

// TTC_NETWORK_ADDRESS_SIZE must be defined outside (Makefile)

// test ------------------------------
#define TTC_NETWORK_ADDRESS_SIZE 2
#define TTC_NETWORK_1           1
#define TTC_NETWORKn_INTERFACE  Serial
//#define TTC_NETWORK1_MAC        IEEE_802_15_4

//------------------------------------

/**
  *
  * TTC_NETWORKn             (1, 2 ... TTC_MAX_NETWORK)
  * TTC_NETWORK_AMOUNT       (1, 2 ... TTC_MAX_NETWORK)
  * TTC_NETWORKn_INTERFACE   (Serial, WLAN-802.15.4, WLAN-802.11, Cabel)
  * TTC_NETWORKn_MAC         (Ethernet, IEEE 802.15.4, IEEE 802.11)
  * TTC_NETWORKn_NETWORK     (uIP, Rime, ZigBee, IP)
  * TTC_NETWORKn_PROTOCOL    (TCP, UDP, ZB_PROFILES)
  *
**/

//} Defines
//{ Static Configuration **************************************

// TTC_NETWORKn has to be defined as constant by makefile.100_board_*
#ifdef TTC_NETWORK5
#ifndef TTC_NETWORK4
#error TTC_NETWORK5 is defined, but not TTC_NETWORK4 - all lower TTC_NETWORKn must be defined!
#endif
#ifndef TTC_NETWORK3
#error TTC_NETWORK5 is defined, but not TTC_NETWORK3 - all lower TTC_NETWORKn must be defined!
#endif
#ifndef TTC_NETWORK2
#error TTC_NETWORK5 is defined, but not TTC_NETWORK2 - all lower TTC_NETWORKn must be defined!
#endif
#ifndef TTC_NETWORK1
#error TTC_NETWORK5 is defined, but not TTC_NETWORK1 - all lower TTC_NETWORKn must be defined!
#endif

#define TTC_NETWORK_AMOUNT 5
#else
#ifdef TTC_NETWORK4
#define TTC_NETWORK_AMOUNT 4

#ifndef TTC_NETWORK3
#error TTC_NETWORK5 is defined, but not TTC_NETWORK3 - all lower TTC_NETWORKn must be defined!
#endif
#ifndef TTC_NETWORK2
#error TTC_NETWORK5 is defined, but not TTC_NETWORK2 - all lower TTC_NETWORKn must be defined!
#endif
#ifndef TTC_NETWORK1
#error TTC_NETWORK5 is defined, but not TTC_NETWORK1 - all lower TTC_NETWORKn must be defined!
#endif
#else
#ifdef TTC_NETWORK3

#ifndef TTC_NETWORK2
#error TTC_NETWORK5 is defined, but not TTC_NETWORK2 - all lower TTC_NETWORKn must be defined!
#endif
#ifndef TTC_NETWORK1
#error TTC_NETWORK5 is defined, but not TTC_NETWORK1 - all lower TTC_NETWORKn must be defined!
#endif

#define TTC_NETWORK_AMOUNT 3
#else
#ifdef TTC_NETWORK2

#ifndef TTC_NETWORK1
#error TTC_NETWORK5 is defined, but not TTC_NETWORK1 - all lower TTC_NETWORKn must be defined!
#endif

#define TTC_NETWORK_AMOUNT 2
#else
#ifdef TTC_NETWORK1
#define TTC_NETWORK_AMOUNT 1
#else
#define TTC_NETWORK_AMOUNT 0
#endif
#endif
#endif
#endif
#endif

#ifndef TTC_ASSERT_NETWORK    // any previous definition set (Makefile)?
#define TTC_ASSERT_NETWORK 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_NETWORK == 1)  // use Assert()s in NETWORK code (somewhat slower but alot easier to debug)
#define Assert_NETWORK(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in NETWORK code (somewhat smaller + faster, but crashes are hard to debug)
#define Assert_NETWORK(Condition, ErrorCode)
#endif

#if TTC_NETWORK_ADDRESS_SIZE == 1
#  define ttc_network_address_t t_u8
#  define TTC_NETWORK_BROADCAST 0xff
#elif TTC_NETWORK_ADDRESS_SIZE == 2
#  define ttc_network_address_t t_u16
#  define TTC_NETWORK_BROADCAST 0xffff
#elif TTC_NETWORK_ADDRESS_SIZE == 4
#  define ttc_network_address_t t_u32
#  define TTC_NETWORK_BROADCAST 0xffffffff
#elif TTC_NETWORK_ADDRESS_SIZE == 8
#  define ttc_network_address_t struct { t_u8 B1; t_u8 B2; t_u8 B3; t_u8 B4; t_u8 B5; t_u8 B6; t_u8 B7; t_u8 B8; } __attribute__((__packed__))
#  define TTC_NETWORK_BROADCAST 0xffffffffffffffff
#else
#  warning Undefined constant TTC_NETWORK_ADDRESS_SIZE (using default size)
#  define ttc_network_address_t t_u8
#endif

//}Static Configuration
//{ Enums/ Structures *****************************************

typedef enum {    // return codes of NETWORK devices
    ec_network_OK = 0,

    // other warnings go here..

    ec_network_ERROR,           // general failure
    ec_network_DeviceNotFound,
    ec_network_InvalidImplementation

    // other failures go here..
} e_ttc_network_errorcode;
typedef enum {    // types of architectures supported by NETWORK driver
    ta_network_None,           // no architecture selected

    ta_network_6lowpan,  // first supported architecture
    //<MORE_ARCHITECTURES>

    ta_network_ERROR           // architecture not supported
} e_ttc_network_architecture;
typedef enum {
    tntm_undefined = 0, //
    tntm_ieee802154
} e_tnt_macstack;
typedef enum {
    tnts_undefined = 0, //
    tnts_uip,
    tnts_rime
} e_tnt_stack;
typedef enum {
    tntp_undefined = 0, //
    tntp_6lowpan,
    tntp_tcp,
    tntp_udp
} e_tnt_protocol;
typedef struct {
    // configuration of network that has created this Status
    struct s_ttc_network_config* Config;

    // protocol address of sender node
    ttc_network_address_t Sender;

    // received signal strength indicator (not yet implemented!)
    t_u8 RSSI;

    // more to come..
} ttc_network_packet_status;

typedef struct s_ttc_network_config { // 6lowpan independent configuration data

    // Note: Write-access to this structure is only allowed before first ttc_network_init() call!
    //       After initialization, only read accesses are allowed for high-level code.

    e_ttc_network_architecture Architecture; // type of architecture used for current network device
    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_NETWORK1, ...)
    t_u8  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)
    void* LowLevelConfig;  // low-level configuration (structure known by low-level driver only)

    union  { // generic configuration bits common for all low-level Drivers
        t_u16 All;
        struct {
            unsigned Initialized        : 1;  // ==1: device has been initialized successfully
            // ToDo: additional high-level flags go here..

            unsigned Reserved1        : 15; // pad to 16 bits
        } Bits;
    } Flags;

    // local protocol address
    ttc_network_address_t LocalAddress;

    // bitmask identifying all nodes in same network (aka PAN-ID)
    ttc_network_address_t Netmask;

    // PAN Identifier
    t_u16 NetworkID;

    // type of mac stack to use
    e_tnt_macstack    MacStack;

    // type of network stack to use
    e_tnt_stack    Stack;

    // type of protocol to use
    e_tnt_protocol Protocol;

    //Number of channel to use (when it is possible!)
    t_u8 Channel;

    /** function that processes a packet being received by protocol
     * @param Config  configuration of network that has received this packet
     * @param Status  meta information about received packet
     * @param Buffer  data being received
     * @param Amount  amount of valid bytes in Buffer[]
     */
    void ( *function_ReceivePacket )( ttc_network_packet_status* Status, t_u8* Buffer, t_base Amount );

    // configuration of low-level driver
    void* DriverConfig;

    //Memory Pools to store the information to exchange
    t_ttc_heap_pool* TxPool;
    t_ttc_heap_pool* RxPool;

    //Pipes to send and receive from other elements (radio, other networks, etc)
    t_ttc_list* TxPipe;
    t_ttc_list RxPipe;

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_network_config;

typedef struct s_ttc_network_node {
    t_ttc_list_item* ListItem;

    // protocol address of single network node
    ttc_network_address_t Address;

    // average RSSI of this node (optional)
    t_u8 RSSI;

} t_ttc_network_node;

//}Structures

#endif // TTC_NETWORK_TYPES_H
