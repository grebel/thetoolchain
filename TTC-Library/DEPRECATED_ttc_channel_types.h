#ifndef TTC_CHANNEL_TYPES_H
#define TTC_CHANNEL_TYPES_H

/*{ ttc_channel_types.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Basic defines, enums and structures.
 *
 * Include this file from
 * architecture independent header files (ttc_XXX.h) and
 * architecture depend      header files (e.g. channel_stm32w.h)
 *
}*/

#include "ttc_basic.h"
#include "ttc_memory.h"

//{ Defines/ TypeDefs ****************************************************

// These defines can be overridden by defining them in your makefile by
// adding a -D option to COMPILE_OPTS.
// E.g.: COMPILE_OPTS += -DTTC_CHANNEL_MAX_AMOUNT=20
//

// assert macro for channel code
#ifndef TTC_Assert_Channel    // any previous definition set (Makefile)?
#define TTC_Assert_Channel 1  // string asserts are enabled by default
#endif
#if (TTC_Assert_Channel == 1)  // use Assert()s in CHANNEL code (somewhat slower but alot easier to debug)
  #define Assert_Channel(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in CHANNEL code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_Channel(Condition, ErrorCode)
#endif

//will collect debug information of each channel into ttc_channel_node_t
#ifndef TTC_DEBUG_CHANNEL
#define TTC_DEBUG_CHANNEL 1
#endif
//#undef TTC_DEBUG_CHANNEL

// maximum allowed amount of channels
#ifndef TTC_CHANNEL_MAX_AMOUNT
#define TTC_CHANNEL_MAX_AMOUNT 10
#endif

//} Defines
//{ Includes *************************************************************

//} Includes
//{ Structures/ Enums 1 **************************************************

typedef enum {   // ttc_channel_errorcode_e
    tce_OK,                // =0: no error
    tce_TimeOut,           // timeout occured in called function
    tce_DeviceNotFound,    // adressed channel device not available in current uC
    tce_NotImplemented,    // function has no implementation for current architecture
    tce_InvalidArgument,   // general argument error
    tce_DeviceNotReady,    // choosen device has not been initialized properly
    tce_TxFIFO_Overrun,    // too many bytes written to hardware fifo
    tce_TxFIFO_Underrun,   // not egnough data in TX-FIFO
    tce_RxFIFO_Overrun,    // too many bytes in receive fifo
    tce_UnexpectedState,   // channel hardware reported an unexpected system state
    tce_RxCorruptPacket,   // corrupt packet received
    tce_TX_Timeout,        // channel spend to much time waiting for TX Mode
    tce_UnknownError
} ttc_channel_errorcode_e;

typedef enum { // ttc_channel_node_type_e - type of driver to use (defines type of connected device)
    tcnt_None,
    tcnt_Application, // a user application
    tcnt_Channel,     // another channel (kind of pipe)
    tcnt_Radio,       // raw radio transceiver communication (no special protocol)
    tcnt_Protocol,    // driver implementing a network protocol
    tcnt_USART,       // universal serial synchronous asynchronous receiver transmitter
    tcnt_SPI,         // serial peripheral interface
    tcnt_I2C          // inter interchange communication interface

} ttc_channel_node_type_e;

typedef enum {           // ttc_channel_device_mode_e - operating modes of connected device
    // Note: Not all mode may be available for a connected device

    tcd_Idle,            // device is doing nothing
    tcd_Reset,           // device is being reset (configuration gets lost)
    tcd_Sleep,           // device is doing even less than nothing (may loose configuration)
    tcd_PowerOn,         // device shall power up after sleep state or PowerOff (automatically reloads configuration)
    tcd_PowerOff,        // device shall be completely shut down (will loose configuration)
    tcd_Transmit,        // device will transmit current packet
    tcd_Receive,         // device is active listening
    tcd_TransmitReceive  // device is receiving + transmitting in parallel (some devices provide only half-duplex transmission)
} ttc_channel_device_mode_e;

typedef enum {  // ttc_channel_node_property_e - list of properties that might be available for individual channel nodes
    tcnp_None,
    tcnp_Destination,   // define destination address for outgoing data
    tcnp_Source,        // define source address for outgoing data
    tcnp_TxLevel,       // adjustment of transmit level (0..255=maximum)
    tcnp_RxLevel,       // adjustment of receice level (0..255=maximum)
    tcnp_Mode,          // change of device mode (-> ttc_channel_device_mode_e)
    tcnp_TxChannel,     // switching transmit channel (e.g. ttc_radio)
    tcnp_RxChannel,     // switching receive channel (e.g. ttc_radio)
    tcnp_MaxPacketSize, // maximum allowed size of data packets (readonly)
    tcnp_MaxTxChannel,  // maximum allowed transmit channel (readonly)
    tcnp_MaxRxChannel   // maximum allowed receive  channel (readonly)

} ttc_channel_node_property_e;
typedef struct {         // ttc_channel_node_application_t - configuration of an application type channel node

    // function being called to receive data from the queue
    ttc_heap_block_t* (*RxFunction)(void* SourceNode, ttc_heap_block_t* Block);

} __attribute__((__packed__)) ttc_channel_node_application_t;
typedef struct {         // ttc_channel_node_application_t - configuration of an application type channel node

    u8_t LogicalIndex;   // logical index of device unit to use

    // make struct same size as ttc_channel_node_application_t
    u8_t Unused1;
    u8_t Unused2;
    u8_t Unused3;

} __attribute__((__packed__)) ttc_channel_node_generic_t;

typedef union { // ttc_channel_device_u - used to store all kinds of channel dependend configurations

    ttc_channel_node_application_t  Application;   // configuration data of an application
    ttc_channel_node_generic_t      GenericDevice; // configuration data of a generic ttc-device

} ttc_channel_device_u;

typedef struct ttc_channel_device_interface_s { // ttc_channel_device_interface_t - static configuration of device interface (must be provided by device implementation)

    // function used to send data from a memory block to connected device
    ttc_channel_errorcode_e (*send_block)(u8_t LogicalIndex, ttc_heap_block_t* Block);

    // function used to send raw data (may contain zeroes) to connected device
    ttc_channel_errorcode_e (*send_raw)(u8_t LogicalIndex, u8_t* Buffer, Base_t Amount);

    // function used to send constant raw data (may contain zeroes; must not be copied) to connected device
    ttc_channel_errorcode_e (*send_raw_const)(u8_t LogicalIndex, u8_t* Buffer, Base_t Amount);

    // function used to send a non constant string to connected device
    ttc_channel_errorcode_e (*send_string)(u8_t LogicalIndex, const char* String, Base_t MaxSize);

    // function used to send a constant string to connected device (data must not be copied)
    ttc_channel_errorcode_e (*send_string_const)(u8_t LogicalIndex, const char* String, Base_t MaxSize);

    // function used to allocate empty blocks suitable for connected device
    ttc_heap_block_t* (*get_empty_block)();

    // maximum size of memory blocks supported by this device
    TTC_MEMORY_BLOCK_BASE BlockSize;
} ttc_channel_device_interface_t;

typedef struct ttc_channel_node_s { // ttc_channel_node_t - configuration of a single node attached to a channel

    // logical index of channel to which this node belongs to
    u8_t ChannelIndex;

    // first field: identifies type of Config
    ttc_channel_node_type_e Type;

    // configuration data of device connected to this node
    ttc_channel_device_u Device;

    // points to static configuration of device interface functions
    const ttc_channel_device_interface_t* Interface;

    // pointer to next node being connected to this channel
    struct ttc_channel_node_s* Next;

} __attribute__((__packed__)) ttc_channel_node_t;

// unique identifier for created channels
// A channel consists of at least one node in a linked list of channel nodes.
typedef struct ttc_channel_s {

    // function that will deliver memory blocks suitable for all nodes in channel
    ttc_heap_block_t* (*get_empty_block)();

    // first node in a circular list of nodes building the channel
    ttc_channel_node_t* FirstNode;

    // last node reporting an error
    ttc_channel_node_t* LastErrorNode;

    // last reported error code
    ttc_channel_errorcode_e LastErrorCode;

    // minimum/ maximum size of memory blocks accepted by connected nodes
    TTC_MEMORY_BLOCK_BASE Block_MinSize;
    TTC_MEMORY_BLOCK_BASE Block_MaxSize;

#if TTC_DEBUG_CHANNEL == 1
    u32_t Amount_Packets;  // amount of packets being sent to the channel so far
#endif
} __attribute__((__packed__)) ttc_channel_t;

// unique identifier of created channel nodes
typedef ttc_channel_node_t* ttc_channel_node_handle_t;

// unique identifier of created channels
typedef ttc_channel_t* ttc_channel_handle_t;

//}

#endif // TTC_CHANNEL_TYPES_H
