/** { ttc_basic.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for basic features common to all microcontrollers.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 27 at 20140603 04:04:24 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_basic.h".
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_task.h"

#ifndef TTC_ASSERT_OUTPUT_PIN
    #ifdef TTC_LED1
        // no assert gpio defined: Use first led as Assert indicator
        #define TTC_ASSERT_OUTPUT_PIN TTC_LED1
    #endif
#endif
#ifdef TTC_ASSERT_OUTPUT_PIN
    #include "ttc_gpio.h" // gpio pin defined to toggle during assert
#endif

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures.
 *
 */

extern t_s8 ttc_memory_is_writable( volatile const void* Address );
extern t_s8 ttc_memory_is_readable( volatile const void* Address );

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

void  ttc_basic_prepare() {
    // add your startup code here (Singletasking!)

#if (TTC_ASSERT_BASIC == 1)

    // test correct implementation of endian functions
    volatile t_u8 Test[4] = { 0x11, 0x22, 0x33, 0x44 };

    volatile t_u32 Word = ttc_basic_32_read_little_endian( ( t_u8* ) Test );
    Assert_BASIC( Word == 0x44332211, ttc_assert_origin_auto );  // check implementation of ttc_basic_32_read_little_endian()!

    Word = ttc_basic_32_read_big_endian( ( t_u8* ) Test );
    Assert_BASIC( Word == 0x11223344, ttc_assert_origin_auto );  // check implementation of ttc_basic_32_read_big_endian()!

    Word = ttc_basic_16_read_little_endian( ( t_u8* ) Test );
    Assert_BASIC( Word == 0x2211, ttc_assert_origin_auto );  // check implementation of ttc_basic_16_read_little_endian()!

    Word = ttc_basic_16_read_big_endian( ( t_u8* ) Test );
    Assert_BASIC( Word == 0x1122, ttc_assert_origin_auto );  // check implementation of ttc_basic_16_read_big_endian()!

    ttc_basic_32_write_little_endian( ( t_u8* ) Test, 0xaabbccdd );
    Assert_BASIC( ( Test[0] == 0xdd ) && ( Test[1] == 0xcc ) && ( Test[2] == 0xbb ) && ( Test[3] == 0xaa ), ttc_assert_origin_auto );  // check implementation of ttc_basic_32_write_little_endian()

    ttc_basic_32_write_big_endian( ( t_u8* ) Test, 0xaabbccdd );
    Assert_BASIC( ( Test[0] == 0xaa ) && ( Test[1] == 0xbb ) && ( Test[2] == 0xcc ) && ( Test[3] == 0xdd ), ttc_assert_origin_auto );  // check implementation of ttc_basic_32_write_big_endian()

    ttc_basic_16_write_little_endian( ( t_u8* ) Test, 0xaabb );
    Assert_BASIC( ( Test[0] == 0xbb ) && ( Test[1] == 0xaa ), ttc_assert_origin_auto );  // check implementation of ttc_basic_16_write_little_endian()

    ttc_basic_16_write_big_endian( ( t_u8* ) Test, 0xaabb );
    Assert_BASIC( ( Test[0] == 0xaa ) && ( Test[1] == 0xbb ), ttc_assert_origin_auto );  // check implementation of ttc_basic_16_write_big_endian()
#endif

#ifdef TTC_BASIC_TEST_PIN
#ifdef EXTENSION_ttc_gpio
    // initialize test pin
#include "ttc_gpio.h"

    ttc_gpio_init( TTC_BASIC_TEST_PIN, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    ttc_gpio_clr( TTC_BASIC_TEST_PIN );
#endif
#endif

    _driver_basic_prepare();

    ttc_sysclock_prepare();
}
void  ttc_basic_test_ok() {
#ifdef TTC_BASIC_TEST_PIN
#ifdef EXTENSION_ttc_gpio
    // toggle test pin to indicate successfull end of test
    ttc_gpio_put( TTC_BASIC_TEST_PIN, 1 - ttc_gpio_get( TTC_BASIC_TEST_PIN ) );
#endif
#endif
    t_u8 Foo = 1;
    ( void ) Foo; // break here
}
t_u8  ttc_basic_int_rankN( t_u32 A, t_u8 Base ) {
    if ( !A )
    { return 0; }

    t_u8  Rank = 0;
    t_u32 Compare = 1;

    while ( Compare && ( Compare <= A ) ) {
        Rank++;
        Compare *= Base; // multiplication is faster than division
    }

    return Rank;
}
t_u8  ttc_basic_int_rank2( t_u32 A ) {
    t_u8 Rank = 0;

    while ( A > 512 ) { // speed up for larger numbers
        Rank += 9;
        A = A >> 9;
    }
    while ( A ) {
        Rank++;
        A = A >> 1;
    }
    return Rank;
}
t_u32 ttc_basic_multiply_divide_u32x3( t_u32 A, t_u32 B, t_u32 C ) {
    t_u32 Result;

    if ( ( A == 0 ) || ( B == 0 ) )
    { return 0; }

    Assert_BASIC( C, ttc_assert_origin_auto );  // division by zero!

    // calculate amount of binary digits of A and B
    t_u8 RankA = ttc_basic_int_rank2( A );
    t_u8 RankB = ttc_basic_int_rank2( B );
    t_u8 RankAB = RankA + RankB;

    if ( 1 ) { // try to reduce A,B and C by some chosen prime numbers (effectively reduces by 2..22)
        while ( ( RankAB > 32 ) &&
                ( ( A & 1 ) == 0 ) && // A is even
                ( ( C & 1 ) == 0 )    // C is even
              ) {
            // divide A and C by 2 repeatedly

            // A * B / C -> (A/2) * B / (C/2)
            A = A >> 1;
            C = C >> 1;

            RankAB--;
        }
        while ( ( RankAB > 32 ) &&
                ( ( A & 1 ) == 0 ) && // A is even
                ( ( C & 1 ) == 0 )    // C is even
              ) {
            // divide B and C by 2 repeatedly

            // A * B / C -> (A/2) * B / (C/2)
            B = B >> 1;
            C = C >> 1;

            RankAB--;
        }
        while ( ( RankAB > 32 ) &&
                ( ( A % 3 ) == 0 ) && // A is dividable by 3
                ( ( C % 3 ) == 0 )    // C is dividable by 3
              ) {
            // divide A and C by 3 repeatedly

            // A * B / C -> (A/3) * B / (C/3)
            A = A / 3;
            C = C / 3;

            RankAB -= 2; // approximately
        }
        while ( ( RankAB > 32 ) &&
                ( ( A % 3 ) == 0 ) && // A is dividable by 3
                ( ( C % 3 ) == 0 )    // C is dividable by 3
              ) {
            // divide B and C by 3 repeatedly

            // A * B / C -> (A/3) * B / (C/3)
            B = B / 3;
            C = C / 3;

            RankAB -= 2; // approximately
        }
        while ( ( RankAB > 32 ) &&
                ( ( A % 5 ) == 0 ) && // A is dividable by 5
                ( ( C % 5 ) == 0 )    // C is dividable by 5
              ) {
            // divide A and C by 5 repeatedly

            // A * B / C -> (A/5) * B / (C/5)
            A = A / 5;
            C = C / 5;

            RankAB -= 2; // approximately
        }
        while ( ( RankAB > 32 ) &&
                ( ( A % 5 ) == 0 ) && // A is dividable by 5
                ( ( C % 5 ) == 0 )    // C is dividable by 5
              ) {
            // divide B and C by 5 repeatedly

            // A * B / C -> (A/5) * B / (C/5)
            B = B / 5;
            C = C / 5;

            RankAB -= 2; // approximately
        }
        while ( ( RankAB > 32 ) &&
                ( ( A % 7 ) == 0 ) && // A is dividable by 7
                ( ( C % 7 ) == 0 )    // C is dividable by 7
              ) {
            // divide A and C by 7 repeatedly

            // A * B / C -> (A/7) * B / (C/7)
            A = A / 7;
            C = C / 7;

            RankAB -= 2; // approximately
        }
        while ( ( RankAB > 32 ) &&
                ( ( A % 7 ) == 0 ) && // A is dividable by 7
                ( ( C % 7 ) == 0 )    // C is dividable by 7
              ) {
            // divide B and C by 7 repeatedly

            // A * B / C -> (A/7) * B / (C/7)
            B = B / 7;
            C = C / 7;

            RankAB -= 2; // approximately
        }
        while ( ( RankAB > 32 ) &&
                ( ( A % 11 ) == 0 ) && // A is dividable by 11
                ( ( C % 11 ) == 0 )    // C is dividable by 11
              ) {
            // divide A and C by 11 repeatedly

            // A * B / C -> (A/11) * B / (C/11)
            A = A / 11;
            C = C / 11;

            RankAB -= 2; // approximately
        }
        while ( ( RankAB > 32 ) &&
                ( ( A % 11 ) == 0 ) && // A is dividable by 11
                ( ( C % 11 ) == 0 )    // C is dividable by 11
              ) {
            // divide B and C by 11 repeatedly

            // A * B / C -> (A/11) * B / (C/11)
            B = B / 11;
            C = C / 11;

            RankAB -= 2; // approximately
        }
        while ( ( RankAB > 32 ) &&
                ( ( A % 13 ) == 0 ) && // A is dividable by 13
                ( ( C % 13 ) == 0 )    // C is dividable by 13
              ) {
            // divide A and C by 13 repeatedly

            // A * B / C -> (A/13) * B / (C/13)
            A = A / 13;
            C = C / 13;

            RankAB -= 2; // approximately
        }
        while ( ( RankAB > 32 ) &&
                ( ( A % 13 ) == 0 ) && // A is dividable by 13
                ( ( C % 13 ) == 0 )    // C is dividable by 13
              ) {
            // divide B and C by 13 repeatedly

            // A * B / C -> (A/13) * B / (C/13)
            B = B / 13;
            C = C / 13;

            RankAB -= 2; // approximately
        }
        while ( ( RankAB > 32 ) &&
                ( ( A % 17 ) == 0 ) && // A is dividable by 17
                ( ( C % 17 ) == 0 )    // C is dividable by 17
              ) {
            // divide A and C by 17 repeatedly

            // A * B / C -> (A/17) * B / (C/17)
            A = A / 17;
            C = C / 17;

            RankAB -= 2; // approximately
        }
        while ( ( RankAB > 32 ) &&
                ( ( A % 17 ) == 0 ) && // A is dividable by 17
                ( ( C % 17 ) == 0 )    // C is dividable by 17
              ) {
            // divide B and C by 17 repeatedly

            // A * B / C -> (A/17) * B / (C/17)
            B = B / 17;
            C = C / 17;

            RankAB -= 2; // approximately
        }
    }

    if ( RankAB <= 32 ) { // operation will not overflow
        Result = A * B / C;
    }
    else {                // try to avoid overflow by tricky calculation
        // catch all special cases
        if ( A == C ) // A * B / C = A * B / A = B
        { return B; }
        if ( B == C )
        { return A; } // A * B / C = A * B / B = A
        if ( A == 1 ) // A * B / C = 1 * B / C
        { return B / C; }
        if ( B == 1 ) // A * B / C = A * 1 / C
        { return A / C; }
        if ( C == 1 )
        { return A * B; }

        if ( A < B ) {
            Result = B / C * A;
        }
        else {
            Result = A / C * B;
        }
#if (TTC_ASSERT_BASIC == 1)
        RankAB -= ttc_basic_int_rank2( C );
        Assert_BASIC( RankAB <= 32, ttc_assert_origin_auto );  // cannot avoid overflow, change your calculation inputs!
#endif
    }

    return Result;
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_basic(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
