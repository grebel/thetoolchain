#ifndef TTC_ADC_H
#define TTC_ADC_H
/** { ttc_adc.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for ADC devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_ADC(tc_adc_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_adc_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_adc_init(LogicalIndex);
 *  4) use:         ttc_adc_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level adc and application.
 *
 *  Created from template ttc_device.h revision 30 at 20141008 14:25:12 UTC
 *
 *  Authors: Gregor Rebel, Victor Fuentes
 *
 *
 *  How to configure ADC devices
 *
 *  Analog Digital Converters can read analog voltage from individual GPIO pins.
 *
 *  TTC_ADC provides a static and a dynamic way to configure analog inputs
 *
 *  a) Static ADC configuration
 *     TTC_ADC requires an external configuration to know which GPIO pins should be used
 *     as analog input. For this a constant definition of type TTC_ADC<n>=<pin> is required.
 *     Where <n> is one from 1..10 and <pin> is one from e_ttc_gpio_pin.
 *     Normally these constants are defined in your board makefile. It is possible to define
 *     them in any makefile but this makes it difficult to avoid double definitions.
 *
 *     Example makefile line:
 *     COMPILE_OPTS += -DTTC_ADC1=E_ttc_gpio_pin_c1
 *
 *  b) Dynamic ADC configuration
 *     When an application or a driver wants to use a gpio pin as analog input, it can
 *     dynamically create an ADC configuration by calling ttc_adc_find_configuration_by_pin().
 *     This will create a new logical ADC index an
 *
 *
 *  Each analog input line has to be configured as an individual ttc device
 *
 *  // a) Example initialisation of first analog input (static configuration):
 *  t_ttc_adc_config* Config = ttc_adc_get_configuration(1);
 *  // change Config ..
 *  ttc_adc_init(1);
 *
 *  // b) Example initialisation of analog input for certain pin (dynamic configuration):
 *  t_ttc_adc_config* Config = ttc_adc_find_configuration_by_pin(E_ttc_gpio_pin_c1);
 *  t_u8 ADC_c1 = Config->LogicalIndex;
 *  // change Config ..
 *  ttc_adc_init(ADC_c1);
 *
 *
 *  // Manually read ADC value (blocks until conversion is done)
 *  t_base Value = ttc_adc_get_value(1);
 *
 *  // Configuring with automatic DMA transfer (if supported by low-level driver)
 *  Assert(ttc_adc_init_dma(1, &Value) == ec_adc_OK, ec_adc_FeatureNotSupported);
}*/

#ifndef EXTENSION_ttc_adc
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_adc.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_adc.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_adc_interface.h" // multi architecture support

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level adc only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * adc devices on all supported architectures.
 * Check adc/adc_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares adc Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_adc_prepare();
void _driver_adc_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_adc_get_max_index();

/** returns configuration of indexed analog input
 *
 * Note: If no configuration can be found for given index, a new one will be created.
 *
 * @param LogicalIndex    index of device to init (1..ttc_ADC_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_adc_config* ttc_adc_get_configuration( t_u8 LogicalIndex );

/** looks up configuration of indexed analog input in linked list by given input pin (slow)
 *
 * Note: If no configuration can be found for given pin, a new one will be created.
 *
 * Note: Finding the configuration in a linked list is time consuming.
 *       You should cache the returned pointer and reuse it instead of looking it up often.
 *
 * @param LogicalIndex    index of device to init (1..ttc_ADC_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_adc_config* ttc_adc_find_configuration_by_pin( e_ttc_gpio_pin InputPin );

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_ADC_get_max_LogicalIndex())
 * @return                == 0: adc device has been reset successfully; != 0: error-code
 */
e_ttc_adc_errorcode ttc_adc_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_ADC_get_max_LogicalIndex())
 */
void ttc_adc_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Seetings of indexed device are being reset to their default values.
 * Will automatically call ttc_adc_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of adc device (1..ttc_adc_get_max_index() )
 * @return                == 0: default values have been loaded successfully for indexed adc device; != 0: error-code
 */
e_ttc_adc_errorcode  ttc_adc_load_defaults( t_u8 LogicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_ADC_get_max_LogicalIndex())
 * @return                == 0: indexed adc device has been reset successfully; != 0: error-code
 */
e_ttc_adc_errorcode  ttc_adc_reset( t_u8 LogicalIndex );

/** initialize indexed device for use ADC in DMA mode.
 *
 * @param LogicalIndex    device index of device to init (1..ttc_ADC_get_max_LogicalIndex())
 * @param Value           Variable which will keep the value
 * @return                == 0: adc device has been reset successfully; != 0: error-code
 */
e_ttc_adc_errorcode ttc_adc_init_dma( t_u8 LogicalIndex, volatile t_u16* Value );

/** Get the value from an ADC channel in single mode.
 *
 * Note: The function will block until ADC has finished conversion.
 *
 * @param LogicalIndex    device index of device to init (1..ttc_ADC_get_max_LogicalIndex())
 * @return                Value being read (0..
 */
t_u16 ttc_adc_get_value( t_u8 LogicalIndex );

/** Get the minimum digital value for current architecture
 *
 * @param LogicalIndex    device index of device to init (1..ttc_ADC_get_max_LogicalIndex())
 * @return                Smallest return value of ttc_adc_get_value() (can be negative!)
 */
t_base_signed ttc_adc_get_minimum( t_u8 LogicalIndex );
t_base_signed _driver_adc_get_minimum( t_u8 LogicalIndex );
#define ttc_adc_get_minimum(LogicalIndex) _driver_adc_get_minimum(LogicalIndex)

/** Get the maximum digital value for current architecture
 *
 * @param LogicalIndex    device index of device to init (1..ttc_ADC_get_max_LogicalIndex())
 * @return                Biggest return value of ttc_adc_get_value()
 */
t_base_signed ttc_adc_get_maximum( t_u8 LogicalIndex );
t_base_signed _driver_adc_get_maximum( t_u8 LogicalIndex );
#define ttc_adc_get_maximum(LogicalIndex) _driver_adc_get_maximum(LogicalIndex)

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_adc(t_u8 LogicalIndex)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_heap_ are passed to interfaces/ttc_heap_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl heap UPDATE
 */


/** fills out given Config with maximum valid values for indexed ADC
 * @param Config        = pointer to struct t_ttc_adc_config
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_adc_errorcode _driver_adc_get_features( t_ttc_adc_config* Config );

/** shutdown single ADC unit device
 * @param Config        pointer to struct t_ttc_adc_config
 * @return              == 0: ADC has been shutdown successfully; != 0: error-code
 */
e_ttc_adc_errorcode _driver_adc_deinit( t_ttc_adc_config* Config );

/** initializes single ADC unit for operation
 * @param Config        pointer to struct t_ttc_adc_config
 * @return              == 0: ADC has been initialized successfully; != 0: error-code
 */
e_ttc_adc_errorcode _driver_adc_init( t_ttc_adc_config* Config );

/** updates Config->LowLevelConfig for a new value of Config->InputPin
 * @param Config        pointer to struct t_ttc_adc_config
 * @return              == 0: Config has been updated successfully; != 0: Config->InputPin cannot be used as analog input
 */
e_ttc_adc_errorcode _driver_adc_find_input_pin( t_ttc_adc_config* Config );

/** loads configuration of indexed ADC unit with default values
 * @param Config        pointer to struct t_ttc_adc_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_adc_errorcode _driver_adc_load_defaults( t_ttc_adc_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_adc_config
 */
e_ttc_adc_errorcode _driver_adc_reset( t_ttc_adc_config* Config );

/** initialize an ADC Channel in DMA mode
 *
 * @param Config        pointer to struct t_ttc_adc_config
 * @param Value         Variable which will store the value
 * @return              == 0: adc device has been reset successfully; != 0: error-code
 */
e_ttc_adc_errorcode _driver_adc_init_dma( t_ttc_adc_config* Config, volatile t_u16* Value );

/** get the value from an ADC channel in single mode
 *
 * @param Config          pointer to struct t_ttc_adc_config
 * @return                Value read
 */
t_u16 _driver_adc_get_value( t_ttc_adc_config* Config );


//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_ADC_H
