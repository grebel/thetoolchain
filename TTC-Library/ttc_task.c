/*{ ttc_task.c ***********************************************

 * Written by Gregor Rebel 2010-2018
 *
 * Scheduler and architecture independent multitasking support.
 * See ttc_task.h for more detailed description.
 *
}*/

//? #define ttc_task_C //DEBUG
#include "ttc_task.h"
#include "ttc_heap.h"
#include "ttc_interrupt.h"
#include "ttc_systick.h"
#include "ttc_string.h"

#ifndef TTC_TASK_SCHEDULER_AVAILABLE
    #  error Missing definition for TTC_TASK_SCHEDULER_AVAILABLE (must be defined either as 0 or 1 !)
#endif
#if TTC_TASK_SCHEDULER_AVAILABLE==0
    //#warning  No multitasking scheduler available: Using ttc_task in singletasking mode. (No real problem)
#endif

/* Global variable ttc_task_StackBytesFree is used by all tasks concurrently so we will loose
 * some measures when scheduler switches between its assignment and the Assert_TASK().
 * But it is faster and uses no stack memory.
 */
volatile t_base_signed ttc_task_StackBytesFree;

// global variables loaded with addresses of corresponding extern symbols above
extern volatile t_u8* ttc_memory_MainStack_Start; // first valid address of main stack (set by ttc_task_prepare())
extern volatile t_u8* ttc_memory_MainStack_End;   // last valid address of main stack (set by ttc_task_prepare())

// increased by __ttc_task_critical_begin(); decreased by _ttc_task_critical_end()

//X volatile void ( *ttc_task_CriticalSection_LastCaller )()   = NULL;  // stores last caller of _ttc_task_critical_begin()
//X volatile t_s16            ttc_task_CriticalSection_Counter = -1;    // -1 = scheduler not started, 0 = scheduler running + outside critical section, >0 = inside critical section
#if TTC_TASK_SCHEDULER_AVAILABLE
    #if TTC_ASSERT_TASK
        volatile TaskHandle_t     ttc_task_CriticalSection_Owner   = NULL;  // info of task that called ttc_task_critical_begin()
    #endif
#endif
volatile t_ttc_task_info* ttc_task_TaskInfos               = NULL;  // single linked list of task info data of all created tasks
volatile t_ttc_task_info* ttc_task_TaskInfosEnd            = NULL;  // points to last entry of single linked list for fast list append
volatile t_u8             ttc_task_AmountTasks             = 0;     // amount of tasks currently running
volatile BOOL             ttc_task_SchedulerIsRunning      = FALSE; // set to TRUE once ttc_task_start_scheduler() has been called

void*                     ttc_task_not_implemented() { return NULL; }
BOOL                      ttc_task_create( void TaskFunction( void* ), const char* TaskName, t_u16 StackSize, void* Argument, t_u8 Priority, t_ttc_task_info** TaskInfoPtr ) {

    Assert_TASK_Readable( TaskFunction, ttc_assert_origin_auto );  // this argument must not be NULL!
    Assert_TASK( StackSize, ttc_assert_origin_auto );  // this argument must not be zero!

    BOOL Result = FALSE;
    volatile t_ttc_task_info* TaskInfo = NULL;
    if ( ttc_memory_is_writable( TaskName ) ) { // got task name in RAM: copy to new buffer
        TaskInfo = ttc_heap_alloc_zeroed( sizeof( t_ttc_task_info ) + ttc_string_length8( TaskName, -1 ) + 1 );
        TaskInfo->Name = ( const char* )( TaskInfo + 1 ); // storing task name right after task info
        ttc_string_copy( ( t_u8* ) TaskInfo->Name, ( const t_u8* ) TaskName, 255 ); // casting away const for TaskInfo->Name!
    }
    else {
        TaskInfo = ttc_heap_alloc_zeroed( sizeof( t_ttc_task_info ) );
        TaskInfo->Name = TaskName;  // task name is constant: store only reference
    }

    TaskInfo->StackSize_Bytes = StackSize;
    TaskInfo->Function        = TaskFunction;
    TaskInfo->Argument        = Argument;
    TaskInfo->Priority        = Priority;

    // append new entry at list head (faster than at end)
#ifdef _driver_task_create
    if ( ttc_task_is_scheduler_started() )
    { ttc_task_critical_begin(); }
#endif
    if ( ttc_task_TaskInfos == NULL ) { // first list entry: set new entry as first and last list item
        ttc_task_TaskInfosEnd = ttc_task_TaskInfos = TaskInfo;
    }
    else {                            // append at end of single linked list
        ttc_task_TaskInfosEnd->Next = TaskInfo;
        ttc_task_TaskInfosEnd       = TaskInfo;
    }
    if ( TaskInfoPtr ) {
        Assert_TASK_Writable( TaskInfoPtr, ttc_assert_origin_auto );
    }

#ifdef _driver_task_create
    Result = _driver_task_create( TaskFunction, StackSize, Argument, Priority, ( t_ttc_task_info* ) TaskInfo );
    if ( ttc_task_is_scheduler_started() )
    { ttc_task_critical_end(); }
#else // no scheduler available: use plain function call
    /* DEPRECATED ( ttc_task_start_scheduler() will call all created tasks one after another   )
    //#   warning Missing low-level implementation for _driver_task_create() (ttc_task_create() will use plain function calls)
        ( void ) TaskFunction;
        ( void ) TaskName;
        ( void ) StackSize;
        ( void ) Argument;
        ( void ) Priority;
        ( void ) TaskInfoPtr;

        TaskFunction( Argument );
        return FALSE;
    */
    Result = TRUE; // task will be managed by ttc_task only
#endif

    Assert_TASK( Result, ttc_assert_origin_auto );  // task could not be created
    //X Assert_TASK_Writable( TaskInfo->Handle , ttc_assert_origin_auto); // Result was OK, but no handle returned (broken implementation of _driver_task_create()?)
    if ( ttc_task_TaskInfos->Next ) {
        Assert_TASK_Writable( ( void* ) ttc_task_TaskInfos->Next, ttc_assert_origin_auto );  // someone has changed Next field
    }
    if ( TaskInfoPtr )
    { *TaskInfoPtr = ( t_ttc_task_info* ) TaskInfo; }

    return Result;
}
void                     _ttc_task_msleep( t_base DelayMS ) {

#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    if ( ttc_task_SchedulerIsRunning ) {
        for ( ; DelayMS > 10; DelayMS -= 10 )
#ifdef ttc_task_usleep
            ttc_task_usleep( 10000 );
#else
#warning Missing low-level implementation for _driver_task_usleep() - Using uncalibrated delay!
            for ( t_u16 Delay2 = 10000; Delay2 > 0; Delay2-- );
#endif

        for ( ; DelayMS > 0; DelayMS-- )
#ifdef ttc_task_usleep
            ttc_task_usleep( 1000 );
#else
            for ( volatile t_u16 Delay2 = 1000; Delay2 > 0; Delay2-- );
#endif
    }
    else { // scheduler not running: use software delay
        while ( DelayMS > 10 ) { // reducing each delay to fit into 16 bit value (longer udelays may be inaccurate due to integer math overrun)
            ttc_sysclock_udelay( 10000 );
            DelayMS -= 10;
        }
        if ( DelayMS > 0 )     // delay remaining time < 10 milliseconds
        { ttc_sysclock_udelay( DelayMS * 1000 ); }
    }
#else

    /* Single Tasking msleep
     *
     * The implementation below tries to use as few ttc_systick_delay_init() calls as possible
     * for current architecture and systick configuration.
     */

    t_base DelayTimeUS = ( ttc_systick_delay_get_maximum() / 1000 ) * 1000; // make it a multiple of 1000
    t_base DelayTimeMS = DelayTimeUS / 1000; // time period of maximum delay in milliseconds
    t_ttc_systick_delay Delay;
    if ( ( DelayTimeMS ) > DelayMS ) { // can do it with a single iteration
        DelayTimeMS = DelayMS;
        DelayTimeUS = DelayTimeMS * 1000;
    }

    while ( DelayMS > DelayTimeMS ) { // long delay: use maximum available delay time
        ttc_systick_delay_init( &Delay, DelayTimeUS );
        while ( ttc_systick_delay_expired( &Delay ) ); // busy wait until delay has passed
        DelayMS -= DelayTimeMS;
    }

    if ( DelayMS > 0 ) {           // short delay remaining: extra single delay
        DelayTimeUS = DelayMS * 1000;
        ttc_systick_delay_init( &Delay, DelayTimeUS );
        while ( ! ttc_systick_delay_expired( &Delay ) ); // busy wait until delay has passed
    }
#endif
}
void                     _ttc_task_critical_begin() {

#if TTC_TASK_SCHEDULER_AVAILABLE==1
    if ( ttc_task_is_scheduler_started() ) { // multitasking scheduler running: let scheduler handle this
        _driver_task_critical_begin();

#  if (TTC_ASSERT_TASK == 1)  // record meta data + apply self tests to support debugging
        if ( ttc_interrupt_critical_level() == 1 ) {  // we've just disabled interrupts
            // record current task as owner of this critical section
            ttc_task_CriticalSection_Owner = ttc_task_current_handle();
        }
        else {                                        // continuing critical section
            Assert_TASK_EXTRA( ttc_interrupt_critical_level() != 0, ttc_assert_origin_auto );  // critical section counter not increased. Ensure that ttc_interrupt_critical_level() uses correct low-level driver to read current critical level!
            if ( !ttc_task_CriticalSection_Owner )
            { ttc_task_CriticalSection_Owner = ttc_task_current_handle(); }  // workaround to avoid assert in case that owner has not yet been set
            Assert_TASK( ttc_task_CriticalSection_Owner == ttc_task_current_handle(), ttc_assert_origin_auto );  // Already locked by another task. Check your implementation!
        }
#  endif
    }
    else {                                  // multitasking scheduler not started: ttc_interrupt will handle critical section
        ttc_interrupt_critical_begin();
    }
#else
    ttc_interrupt_critical_begin();
#endif

    /* DEPRECATED (now using ttc_interrupt_critical_*() to avoid double implementation)
    if ( ttc_task_CriticalSection_Counter < 1 ) { // beginning new critical section
        if ( ttc_task_is_scheduler_started() ) { // scheduler running: stop it
            #ifdef _driver_task_critical_begin
            _driver_task_critical_begin();
            #endif
            #if (TTC_ASSERT_TASK == 1)
            ttc_task_CriticalSection_Owner = ttc_task_current_handle();
            #endif
            ttc_task_CriticalSection_Counter++;
        }
        else {                                   // no scheduler: disable multitasking
            ttc_interrupt_all_disable();
            ttc_task_CriticalSection_Counter = 1;
        }
    }
    else {                                        // continuing critical section
        Assert_TASK( ttc_task_CriticalSection_Counter < 1000 , ttc_assert_origin_auto );
        Assert_TASK( ttc_task_CriticalSection_Owner == ttc_task_current_handle() , ttc_assert_origin_auto ); // already locked by another task!
        ttc_task_CriticalSection_Counter++;
    }

    // use return address as reference (next line of code after caller )
    ttc_task_CriticalSection_LastCaller = __builtin_return_address( 0 );
    */
}
void                     _ttc_task_critical_end() {
#if TTC_TASK_SCHEDULER_AVAILABLE==1
    if ( ttc_task_is_scheduler_started() ) {

#  if (TTC_ASSERT_TASK == 1)  // apply self tests to support debugging
        Assert_TASK_EXTRA( ttc_interrupt_critical_level() != 0, ttc_assert_origin_auto );  // not inside a critical section!. It seems as if you ended this critical section more often than you began it.
        Assert_TASK( ttc_task_CriticalSection_Owner == ttc_task_current_handle(), ttc_assert_origin_auto );  // Already locked by another task. Check your implementation!
#  endif
        _driver_task_critical_end();
    }
    else { // scheduler not running: ttc_interrupt will handle this directly
        ttc_interrupt_critical_end();
    }
#else
    // no scheduler available at all: ttc_interrupt will handle this directly
    ttc_interrupt_critical_end();
#endif

    /* DEPRECATED
    ttc_task_CriticalSection_Counter--;
    Assert_TASK( ttc_task_CriticalSection_Counter >= 0 , ttc_assert_origin_auto ); // ttc_task_critical_end() was called more often than ttc_task_critical_begin(). Check your code!

    if ( ttc_task_CriticalSection_Counter == 0 ) {
        if ( ttc_task_is_scheduler_started() ) {   // scheduler has been started: reenable scheduler
            #ifdef _driver_task_critical_end
            _driver_task_critical_end();
            #endif
        }
        else {                                     // scheduler not started: reenable interrupts
            ttc_interrupt_all_enable();
            ttc_task_CriticalSection_Counter = -1; // back to start value
        }
    }
    */
}
void                     _ttc_task_critical_all_end( t_u8* StoreCounter ) {
    Assert_TASK_Writable( StoreCounter, ttc_assert_origin_auto );

    *StoreCounter = ttc_interrupt_critical_level();

    while ( ttc_interrupt_critical_level() )
    { ttc_task_critical_end(); }
}
void                     _ttc_task_critical_all_restore( t_u8 StoreCounter ) {
    while ( StoreCounter-- )
    { ttc_task_critical_begin(); }
}
void                      ttc_task_prepare() {

}
void                      ttc_task_check_main_stack() {
    volatile t_u8 Test; // create test variable on stack


    if ( ttc_memory_MainStack_Start < ttc_memory_MainStack_End ) { // positive stack increase
        Assert_TASK( & Test >= ttc_memory_MainStack_Start, ttc_assert_origin_auto );  // Current stack pointer is even lower than stack start: Stackpointer seems to be corrupt!
        ttc_task_StackBytesFree = ttc_memory_MainStack_End - ( & Test );
    }
    else {                                                   // negative stack increase
        Assert_TASK( & Test <= ttc_memory_MainStack_Start, ttc_assert_origin_auto );  // Current stack pointer is even lower than stack start: Stackpointer seems to be corrupt!
        ttc_task_StackBytesFree = ( & Test ) - ttc_memory_MainStack_End;
    }

    // place a breakpoint here to see the currently remaining amount of main stack memory
    Assert_TASK( ttc_task_StackBytesFree > 0, ttc_assert_origin_auto );  // Stack overrun: Classical scenario (too much local variables, stack size too small): Increase main stack size in configs/memory_project.ld !
}
void                      ttc_task_start_scheduler() {

#ifdef _driver_task_start_scheduler
    ttc_task_SchedulerIsRunning      = TRUE;
    //X    ttc_task_CriticalSection_Counter = 0;
    _driver_task_start_scheduler();
#else

    INFO( "No task scheduler available. Will run functions registered via ttc_task_create() one after another in an endless loop." )

    Assert_TASK( ttc_task_TaskInfos != NULL, ttc_assert_origin_auto );          // no task has been created. Call ttc_task_create() before to create at least one task before calling this function!
    Assert_TASK_Writable( ( void* ) ttc_task_TaskInfos, ttc_assert_origin_auto );  // variable stores invalid pointer: check implementation! (Maybe memory corruption?)

    while ( 1 ) { // run one task after another in an endless loop

        volatile t_ttc_task_info* CurrentTaskInfo = ttc_task_TaskInfos;
        while ( CurrentTaskInfo != NULL ) {
            Assert_TASK_Writable( ( void* ) CurrentTaskInfo, ttc_assert_origin_auto ); // task infos are stored in RAM. This pointer seems to be invalid. Check for memory corruption!
            Assert_TASK_Readable( CurrentTaskInfo->Function, ttc_assert_origin_auto ); // task functions must reside in readable memory space (mostly ROM or FLASH). Check for memory corruption!

            // run task function
            CurrentTaskInfo->Function( CurrentTaskInfo->Argument );

            // switch to next task
            CurrentTaskInfo = CurrentTaskInfo->Next;
        }
    }
#endif
}
void                      ttc_task_udelay( t_base Delay_us ) {
    if ( ttc_interrupt_critical_level() ||             // interrupts are disabled: must use imprecise software delay
            ( Delay_us < ttc_systick_delay_get_recommended() ) // delay period too small for systick: use faster software delay
       )
    { ttc_sysclock_udelay( Delay_us ); }
    else {                                             // use precise systick counter
        t_ttc_systick_delay Delay;
        ttc_systick_delay_init( &Delay, Delay_us );

        while ( !ttc_systick_delay_expired( &Delay ) ); // busy wait for delay to pass
    }
}
void                      ttc_task_udelay_isr( t_base Delay_us ) {
    if ( Delay_us < ttc_systick_delay_get_recommended() ) { // delay period too small for systick: use faster software delay
        ttc_sysclock_udelay( Delay_us );
    }
    else {                                             // use precise systick counter
        t_ttc_systick_delay Delay;
        ttc_systick_delay_init_isr( &Delay, Delay_us );

        while ( !ttc_systick_delay_expired_isr( &Delay ) ); // busy wait for delay to pass
    }
}
#  if TTC_TASK_STACK_CHECKS_ENABLED == 1
void ttc_task_check_stack() {

    if ( !ttc_task_SchedulerIsRunning ) { // still in single tasking environment
        ttc_task_check_main_stack(); // check main stack instead of task stack
    }
    else {

        ttc_task_StackBytesFree = _driver_task_check_stack();
        Assert_TASK( ttc_task_StackBytesFree > 0, ttc_assert_origin_auto );  // stack is full: stop immediately
    }
}
#  endif
//InsertFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

// functions below are only provided if a scheduler is available
#if TTC_TASK_SCHEDULER_AVAILABLE!=0

volatile t_ttc_task_info* ttc_task_update_info( TaskHandle_t Handle ) {
    if ( !Handle ) {
        Handle = ttc_task_current_handle();
    }
    Assert_TASK_Writable( Handle, ttc_assert_origin_auto );  // Scheduler not running, use in task context only!

    ttc_task_critical_begin();

    volatile t_ttc_task_info* Info;
    if ( 1 ) { Info = ttc_task_get_task_info( Handle ); } // fast (uses Handle->pcTaskName as t_ttc_task_info*)
    else   { Info = ttc_task_find_info( Handle ); }   // slow + buggy (sometimes returns NULL)
    _driver_task_update_info( Handle, ( t_ttc_task_info* ) Info );

    ttc_task_critical_end();

    return Info;
}
volatile t_ttc_task_info* ttc_task_find_info( TaskHandle_t Handle ) {
    volatile t_ttc_task_info* TaskInfo = ttc_task_TaskInfos;

    // get entry containing given handle
    while ( TaskInfo && ( TaskInfo->Handle != Handle ) ) {
#ifdef TTC_REGRESSION
        Assert_TASK_Writable( ( void* ) TaskInfo, ttc_assert_origin_auto );
#endif
        TaskInfo = TaskInfo->Next;
    }
    Assert_TASK_Writable( ( void* ) TaskInfo, ttc_assert_origin_auto );  // no task has been stored with given Handle!

    return TaskInfo;
}
t_ttc_task_all*           ttc_task_get_all( t_ttc_task_all* TaskInfos ) {
    static t_ttc_task_all* MyInfo = NULL;
    static t_u8 Size_MyInfo = 0;
    if ( !TaskInfos ) {
        t_u8 AmountOfTasks = _driver_task_get_amount();
        if ( Size_MyInfo < AmountOfTasks )
        { AmountOfTasks++; } // allocating one entry ahead
        MyInfo = ttc_heap_alloc_zeroed( sizeof( t_ttc_task_all ) +
                                        ( sizeof( t_ttc_task_info ) * AmountOfTasks )
                                      );
        TaskInfos = MyInfo;
        Size_MyInfo = AmountOfTasks;
    }
    _driver_task_get_all( TaskInfos );

    return TaskInfos;
}
BOOL                      ttc_task_is_scheduler_started() {
    return ttc_task_SchedulerIsRunning;
}
BOOL                      ttc_task_is_switching_available() {
    return ttc_task_SchedulerIsRunning && ( ttc_interrupt_critical_level() == 0 );
}
void                     _ttc_task_resume( TaskHandle_t Handle ) {

#ifdef _driver_task_resume
    Assert_TASK_Writable( Handle, ttc_assert_origin_auto );  // task handles can only reside in RAM!
    _driver_task_resume( Handle );
#else
    //#  warning Missing implementation for ttc_task_resume(Handle)
#endif
}
#  ifdef _driver_task_resume_now
void                     _ttc_task_resume_now( TaskHandle_t Handle ) {

    Assert_TASK_Writable( Handle, ttc_assert_origin_auto );  // task handles can only reside in RAM!
    _driver_task_resume_now( Handle );
}
#  else
#ifdef TTC_MULTITASKING_SCHEDULER
    #    warning Missing implementation for ttc_task_resume_now(Handle)
#endif
#  endif
#  ifdef _driver_task_resume_now_isr
void                     _ttc_task_resume_now_isr( TaskHandle_t Handle ) {

    Assert_TASK_Writable( Handle, ttc_assert_origin_auto );  // task handles can only reside in RAM!
    _driver_task_resume_now_isr( Handle );
}
#  else
#ifdef TTC_MULTITASKING_SCHEDULER
    #    warning Missing implementation for ttc_task_resume_now_isr(Handle)
#endif
#  endif
void                      ttc_task_suspend( TaskHandle_t Handle ) {

#ifdef _driver_task_suspend
    if ( Handle == NULL )
    { Handle = ttc_task_current_handle(); }
    Assert_TASK_Writable( Handle, ttc_assert_origin_auto );  // task handles can only reside in RAM!

    _driver_task_suspend( Handle );
#else
    //#   warning ttc_task_suspend(Handle) - missing low-level implementation
#endif
}
#ifdef REGRESSION_PIN_QUEUE_PUSH
    extern void regression_queue_setPin_QueuePush( BOOL Set );
    extern void regression_queue_mark_test( t_u16 Time );
#endif
void                     ttc_task_waitinglist_wait( t_ttc_task_waiting_list* WaitingList, t_base TimeOut ) {
    Assert_TASK( ttc_task_is_inside_critical_section(), ttc_assert_origin_auto );  // must be called inside critical section!

    if ( !TimeOut )
    { return; }    // no timeout and no endless wait

    /* Note: Write access to WaitingList->First/ Last is allowed only here! */

    t_ttc_task_waiting_task ThisWaitingTask;
    ThisWaitingTask.Task = ttc_task_current_info();
    ThisWaitingTask.Next = NULL;
    Assert_TASK_Writable( WaitingList, ttc_assert_origin_auto );
    //Assert_TASK(ttc_task_is_inside_critical_section(), ttc_assert_origin_auto); // call ttc_task_critical_begin() before!

    if ( WaitingList->First ) { // >=1 task waiting: append ourself at end of list
        WaitingList->Last->Next = &ThisWaitingTask;
        WaitingList->Last       = &ThisWaitingTask;
    }
    else {                    // we're the only task waiting
        WaitingList->Last  = &ThisWaitingTask;
        WaitingList->First = &ThisWaitingTask;
    }
#ifdef REGRESSION_PIN_QUEUE_PUSH
    regression_queue_setPin_QueuePush( 0 );
#endif
    if ( TimeOut != -1 ) { // timed sleep
        ttc_task_critical_end();
        ttc_task_usleep( TimeOut );
    }
    else {               // endless sleep
        Assert_TASK_Writable( ThisWaitingTask.Task->Handle, ttc_assert_origin_auto );  // illegal task handle
        ttc_task_suspend( ThisWaitingTask.Task->Handle );
        ttc_task_critical_end();

    }
    ttc_task_critical_begin();

    // got woken up: remove ourself from list of waiting tasks
    Assert_TASK( WaitingList->First == &ThisWaitingTask, ttc_assert_origin_auto );  // INTERNAL: ERROR: someone has changed list in between!
    WaitingList->First = ThisWaitingTask.Next;
}
void                    _ttc_task_waitinglist_awake( t_ttc_task_waiting_list* WaitingList ) {
    Assert_TASK_Writable( WaitingList, ttc_assert_origin_auto );
    Assert_TASK( ttc_task_is_inside_critical_section(), ttc_assert_origin_auto );  // must be called inside critical section!
    Assert_TASK_Writable( WaitingList->First, ttc_assert_origin_auto );           // Condition must be checked by caller!
    Assert_TASK_Writable( WaitingList->First->Task, ttc_assert_origin_auto );     // illegal task handle

    ttc_task_resume_now( WaitingList->First->Task->Handle );
}
void                    _ttc_task_waitinglist_awake_isr( t_ttc_task_waiting_list* WaitingList ) {
    Assert_TASK_Writable( WaitingList, ttc_assert_origin_auto );
    Assert_TASK_Writable( WaitingList->First, ttc_assert_origin_auto );  // Condition must be checked by caller!
    Assert_TASK_Writable( WaitingList->First->Task, ttc_assert_origin_auto );  // illegal task handle

    ttc_task_resume_now_isr( WaitingList->First->Task->Handle );
}
#endif //TTC_TASK_SCHEDULER_AVAILABLE
