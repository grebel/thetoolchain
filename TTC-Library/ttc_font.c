/*{ ttc_font.c *******************************************************

   Font management.
   This implementation tries to use as few ram as possible.
   Most of the configuration is done by makefile and defines at compile time.
   Font data should be always declared as const to move it into rom.

   Implementation of high-level driver.

}*/
//{ Defines/ TypDefs *****************************************************
//} Defines

#include "ttc_font.h"

//{ Global Variables *****************************************************

#if TTC_FONTS_AMOUNT == 0
    #error No fonts defined, activate at least one font in your extensions.local/makefile.700_extra_settings
#endif
#ifdef TTC_FONT_USING_DEFAULT
    INFO( "No fonts defined in makefile. Defining default font. Check example font lines in extensions.local/makefile.700_extra_settings" );
#endif

// for each initialized device, a pointer to its generic and architecture definitions is stored
A_define( const t_ttc_font_data*, ttc_FontsData, TTC_FONTS_AMOUNT );

//} Global Variables
//{ Function definitions *************************************************

t_u8 ttc_font_get_max_index() {
    return TTC_FONTS_AMOUNT;
}
const t_ttc_font_data* ttc_font_get_configuration( t_u8 LogicalIndex ) {
    const t_ttc_font_data* Config = A( ttc_FontsData, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        ttc_font_init();
        Config = A( ttc_FontsData, LogicalIndex - 1 );
    }

    return Config;
}
void ttc_font_init() {
    t_u8 Index = 0;
    const t_ttc_font_data* FontData = NULL;

    // load adrresses of all configured fonts into ttc_FontsData[]

    if ( 1 <= TTC_FONTS_AMOUNT ) { // TTC_FONT1
#if TTC_FONT1==font_type1_16x24
        FontData = gfx_font_type1_16x24();
#elif TTC_FONT1==font_other // add other fonts here...
#endif
        A( ttc_FontsData, Index++ ) = FontData;
    }
    if ( 2 <= TTC_FONTS_AMOUNT ) { // TTC_FONT2
#if TTC_FONT2==font_type1_16x24
        FontData = gfx_font_type1_16x24();
#elif TTC_FONT2==font_other // add other fonts here...
#endif
        A( ttc_FontsData, Index++ ) = FontData;
    }
    if ( 3 <= TTC_FONTS_AMOUNT ) { // TTC_FONT3
#if TTC_FONT3==font_type1_16x24
        FontData = gfx_font_type1_16x24();
#elif TTC_FONT3==font_other // add other fonts here...
#endif
        A( ttc_FontsData, Index++ ) = FontData;
    }
    if ( 4 <= TTC_FONTS_AMOUNT ) { // TTC_FONT4
#if TTC_FONT4==font_type1_16x24
        FontData = gfx_font_type1_16x24();
#elif TTC_FONT4==font_other // add other fonts here...
#endif
        A( ttc_FontsData, Index++ ) = FontData;
    }
    if ( 5 <= TTC_FONTS_AMOUNT ) { // TTC_FONT5
#if TTC_FONT5==font_type1_16x24
        FontData = gfx_font_type1_16x24();
#elif TTC_FONT5==font_other // add other fonts here...
#endif
        A( ttc_FontsData, Index++ ) = FontData;
    }
    if ( 6 <= TTC_FONTS_AMOUNT ) { // TTC_FONT6
#if TTC_FONT6==font_type1_16x24
        FontData = gfx_font_type1_16x24();
#elif TTC_FONT6==font_other // add other fonts here...
#endif
        A( ttc_FontsData, Index++ ) = FontData;
    }
    if ( 7 <= TTC_FONTS_AMOUNT ) { // TTC_FONT7
#if TTC_FONT7==font_type1_16x24
        FontData = gfx_font_type1_16x24();
#elif TTC_FONT7==font_other // add other fonts here...
#endif
        A( ttc_FontsData, Index++ ) = FontData;
    }
    if ( 8 <= TTC_FONTS_AMOUNT ) { // TTC_FONT8
#if TTC_FONT8==font_type1_16x24
        FontData = gfx_font_type1_16x24();
#elif TTC_FONT8==font_other // add other fonts here...
#endif
        A( ttc_FontsData, Index++ ) = FontData;
    }
    if ( 9 <= TTC_FONTS_AMOUNT ) { // TTC_FONT9
#if TTC_FONT9==font_type1_16x24
        FontData = gfx_font_type1_16x24();
#elif TTC_FONT9==font_other // add other fonts here...
#endif
        A( ttc_FontsData, Index++ ) = FontData;
    }
}

//}FunctionDefinitions
