#ifndef ttc_task_types_H
#define ttc_task_types_H

/*{ ttc_task.h ***********************************************

 * Written by Gregor Rebel 2010-2018
 *
 * Architecture independent support for multitasking.
 *
 * Currently implemented architectures: stm32 + FreeRTOS
 *
}*/

//{ includes

#include "ttc_basic_types.h"
#include "compile_options.h"

#ifdef EXTENSION_300_scheduler_freertos
    #include "scheduler/freertos_task_types.h"
    #define TTC_TASK_SCHEDULER_AVAILABLE 1
#endif

#ifndef TTC_TASK_SCHEDULER_AVAILABLE
    #define TTC_TASK_SCHEDULER_AVAILABLE 0
#endif

#ifndef TaskHandle_t
    #define TaskHandle_t void*
    //? #  warning Missing type definition in low-level driver: TaskHandle_t
#endif

//}includes
//{ constants

#ifndef TTC_ASSERT_TASK    // any previous definition set (Makefile)?
    #define TTC_ASSERT_TASK 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_TASK == 1)  // use Assert()s in TASK code (somewhat slower but alot easier to debug)
    #define Assert_TASK(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_TASK_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_TASK_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in TASK code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_TASK(Condition, Origin)
    #define Assert_TASK_Writable(Address, Origin)
    #define Assert_TASK_Readable(Address, Origin)
#endif


#ifndef TTC_ASSERT_TASK_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_TASK_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_TASK_EXTRA == 1)  // use Assert()s in TASK code (somewhat slower but alot easier to debug)
    #define Assert_TASK_EXTRA(Condition, Origin) Assert( ((Condition) != 0), Origin)
#else  // use no extra Assert()s in TASK code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_TASK_EXTRA(Condition, Origin)
#endif

#ifndef TTC_TASK_MINIMAL_STACK_SIZE
    #define TTC_TASK_MINIMAL_STACK_SIZE 128
#endif

/** Low-level driver or makefile may define this as 1 (enabled) or 0 (disabled)
 *
 * Enabling stack checks will add extra code, consume more RAM and slow down your application.
 * Stack checks can detect stack overrun and stack pointer corruption at an early stage.
 * It is highly encouraged to enable these checks during development. It will help you to find
 * really nasty bugs.
 */
#ifndef TTC_TASK_STACK_CHECKS_ENABLED
    #define TTC_TASK_STACK_CHECKS_ENABLED 1 // default: enable stack checks
    //#  error Missing definition for TTC_TASK_STACK_CHECKS_ENABLED (low-level driver must define this as 1 or 0 !)
#endif
//}

/** collected data of a single task */
typedef struct s_ttc_task_update_info {
    const char* Name;
    TaskHandle_t Handle;                  // handle used to identify this task
    t_u8 Priority;                        // tasks of higher priority will block those of lower priority
    void ( *Function )();                 // function being started as task
    void*   Argument;                     // argument passed to task function
    t_base* StackTop;                     // address of last item push on stack
    t_base* StackStart;                   // start address of stack
    t_base* StackEnd;                     // end address of stack (only available if stack grows upwards)
    t_base  StackUsed;                    // current amount of bytes used from stack
    t_base  StackSize_Bytes;              // size of stack
    t_base  StackFree_HighWatermark;      // calculated free bytes in stack (according to high-watermark)
    void*   SchedulerControlBlock;        // points to task control block used by low level scheduler driver
    volatile struct s_ttc_task_update_info* Next;  // points to next item in single linked list
} t_ttc_task_info;

/** collected data of all created tasks */
typedef struct {
    t_u8 AmountAll;                     // total amount of tasks
    t_u8 AmountRunning;                 // total amount of running tasks
    t_u8 AmountDelayed;                 // total amount of delayed tasks
    t_u8 AmountOverflowDelayed;         // total amount of OverflowDelayed tasks
    t_u8 AmountWaitTermination;         // total amount of tasks waiting for termination
    t_u8 AmountSuspended;               // total amount of tasks being suspended
    t_ttc_task_info** Running;          // list of pointers to task-infos of running tasks
    t_ttc_task_info** Delayed;          // list of pointers to task-infos of delayed tasks
    t_ttc_task_info** OverflowDelayed;  // list of pointers to task-infos of OverflowDelayed tasks
    t_ttc_task_info** WaitTermination;  // list of task-infos of tasks waiting for termination
    t_ttc_task_info** Suspended;        // list of task-infos of tasks being suspended
    t_ttc_task_info* All[];             // list of task-infos of all tasks
} t_ttc_task_all;

/** individual item of a list of waiting tasks */
typedef struct s_ttc_task_waiting_task { /** list of tasks waiting for same resource */
    struct s_ttc_task_waiting_task* Next; // next waiting task in single linked list
    t_ttc_task_info* Task;                // info struct of task waiting for a resource
} t_ttc_task_waiting_task;

/** head of a list of waiting tasks */
typedef struct s_ttc_task_waitinglist_list { /** list of tasks waiting for same resource */
    t_ttc_task_waiting_task* First; // first waiting task in single linked list
    t_ttc_task_waiting_task* Last;  // last  waiting task in single linked list
} t_ttc_task_waiting_list;


#endif
