#ifndef ttc_memory_H
#define ttc_memory_H

/** ttc_memory.h ***********************************************{
 
 * Written by Gregor Rebel 2010-2014
 *
 * Architecture independent support for dynamic memory management.
 * 
}*/

//{ includes

#include "compile_options.h"
#include "ttc_basic.h"
#include "ttc_memory_types.h"
//? #include "ttc_gpio.h"
#ifdef EXTENSION_500_ttc_task
#include "ttc_task.h"
#endif
#include "ttc_mutex.h"
#include "ttc_semaphore.h"
#include "ttc_list.h"

#ifdef EXTENSION_500_ttc_memory_heap_sdma
#include "heap/heap_sdma.h"
#endif
#ifdef EXTENSION_500_ttc_memory_heap_freertos
#include "heap/heap_freertos.h"
#endif
#ifdef EXTENSION_500_ttc_memory_heap_simple
#include "heap/heap_simple.h"
#endif

#ifdef malloc
#error malloc from stdlib is not supported, use ttc_memory_alloc() instead!
#endif

//}includes
//{ enums and structures *******************************************

/** {_Safe Arrays

 In C and C++, array accesses are not checked at all.
 One can easily access an array element far beyond its allocated memory space and even at a
 negative index.
 Assume the following:
   u8_t A[10];
   A[10] = 1;

 This is a very common error. In C, arrays start at index 0 and run to Size-1.
 So A runs from A[0] to A[9]. A[10] is one byte behind the allocated array.
 If you were lucky, this access will address a padding byte that has been added by the
 compiler to let the next variable start at a proper address.
 Let me tell you a secret: Nobody is always lucky!

 So we can check array indices all the time by inserting extra code:

 #define A_SIZE 10
 u8_t A[A_SIZE];

 void setA(u8_t Index, u8_t Value) {
   Assert(Index < A_SIZE, ec_InvalidArgument);
   A[Index] = Value;
 }

 TheToolChain provides a more elegant support for safe arrays.
 You will have to type only a little more and your code will look pretty much the same as before.
 Safe arrays are defined by ttc_array_define() or by ttc_array_dynamic().

 See how similar the above code looks when using safe arrays:
 ttc_array_define(u8_t, A, 10);   // == u8_t A[10];
 A(A, 10) = 1;                    // == A[10] = 1;  -> WILL ASSERT BECAUSE 10 it out of range 0..9
 u8_t B = A(A, 9);                // == u8_t B = A[9];


 Just look how similar the definitions are:

 Defining a static array
             u8_t B[10];
 equals to   ttc_array_define(B, 10);

 Defininig a dynamic array (content buffer is allocated later)
             u8_t* C;
 equals to   ttc_array_dynamic(u8_t, C);

 Elements are accessed by use of A() macro.
 B[I] is substituted by A(B, I) in all cases.

 Of course, use of safe arrays inflicts some processing overhead.
 Therefore, all pointer checking can be disabled by defining TTC_SAFE_ARRAYS = 0.
 This can be done best in your makefile (yes its -D):
 COMPILE_OPTS += -DTTC_SAFE_ARRAYS=0

Example

    struct Data_t { u8_t A; u8_t B; };          // create own structure
    ttc_array_define(struct Data_t, Data, 4);   // equals to: struct Data_t Data[4]

    ...

    A(Data, 0).A = 1;                                 // lvalue     Data[0] = 1;
    u8_t A2 = A(Data, 1).A;                           // rvalue     u8_t A2 = Data[1];
    memset(&(A(Data, 2)), 1, sizeof(struct Data_t));  // reference  &(Data[2])
    A(Data, 4).B = A2;                                // will Assert!

Debug output:

Breakpoint 1, AssertFunction (Condition=FALSE, ErrorCode=ec_ArrayIndexOutOfBound) at ttc-lib/ttc_basic.c:13
13          volatile bool HoldOnAssert=TRUE;
(gdb) bt
#0  AssertFunction (Condition=FALSE, ErrorCode=ec_ArrayIndexOutOfBound) at ttc-lib/ttc_basic.c:13
#1  0x08005f00 in _ttc_array_index (Size=0x20000194, Index=4) at ttc-lib/ttc_memory.c:139
#2  0x08009f08 in example_usart_init () at examples/example_ttc_usart.c:38
#3  0x0800a3c4 in task_USART (TaskArgument=0x0) at examples/example_ttc_usart.c:100
#4  0x00000000 in ?? ()
(gdb) up
#1  0x08005f00 in _ttc_array_index (Size=0x20000194, Index=4) at ttc-lib/ttc_memory.c:139
139         Assert(Index < *Size, ec_ArrayIndexOutOfBound);
(gdb) up
#2  0x08009f08 in example_usart_init () at examples/example_ttc_usart.c:38
38          A(Data, 4).B = A2;                                // will Assert!
(gdb)


    If an array shall be allocated dynamically at runtime, then dynamic safe arrays can be used.

    ttc_array_dynamic(u8_t, Vars);      // equals to: u8_t* Vars = NULL;

    ...

    if ( ttc_array_is_null(Vars) )
        ttc_array_allocate(Vars, 100);  // equals to: Vars = malloc(100);

    // dynamic array may now be used like static array:
    A(Vars,42) = 123;
    u8_t Value = A(Vars,0);
    A(Vars, 100) = 66;                  // will Assert!

}*/

#ifndef EXTENSION_500_ttc_memory
#  error Extension must be activated: 500_ttc_memory
#endif

// checks array index + returns index
Base_t _ttc_array_index(Base_t* Size, Base_t Index);

#ifndef TTC_SAFE_ARRAYS
  #define TTC_SAFE_ARRAYS 1  // 1: use safe arrays (slower but secure); 0: use original arrays (insecure but no overhead)
#endif

// defines a new safe static array as local or global variable
#if (TTC_SAFE_ARRAYS==1)
  #define ttc_array_define(TYPE, NAME, SIZE) struct { Base_t Size; TYPE Data[SIZE]; } NAME = { .Size = SIZE }
#else
  #define ttc_array_define(TYPE, NAME, SIZE) TYPE Data[SIZE] NAME
#endif

// Gives acces to an external safe array being define as global variable in another file.
//
#if (TTC_SAFE_ARRAYS==1)
  #define ttc_array_define_extern(TYPE, NAME, SIZE) extern struct { Base_t Size; TYPE Data[SIZE]; } NAME
#else
  #define ttc_array_define_extern(TYPE, NAME, SIZE) extern TYPE Data[SIZE] NAME
#endif

// defines a new safe dynamic array as local or global variable.
// The array is created with Size=0; Data=NULL.
// Before its first usage, call ttc_array_allocate() to allocate its memory for a given size dynamically.
#if (TTC_SAFE_ARRAYS==1)
  #define ttc_array_dynamic(TYPE, NAME) struct { Base_t Size; TYPE* Data; } NAME = { 0, NULL }
#else
  #define ttc_array_dynamic(TYPE, NAME) TYPE* Data* NAME = NULL
#endif

// Gives acces to an external safe dynamic array being define as global variable in another file.
//
#if (TTC_SAFE_ARRAYS==1)
  #define ttc_array_dynamic_extern(TYPE, NAME) extern struct { Base_t Size; TYPE* Data; } NAME
#else
  #define ttc_array_dynamic_extern(TYPE, NAME) extern TYPE* Data* NAME
#endif

// resets a safe dynamic array to the uninitialized state.
// Note: Memory being allocated for this array will NOT be deallocated. You might loose memory!
//       => Deallocate its memory before or reset the memory manager.
//
#if (TTC_SAFE_ARRAYS==1)
  #define ttc_array_dynamic_reset(NAME) NAME.Data = NULL
#else
  #define ttc_array_dynamic_reset(NAME) NAME = NULL
#endif

// checks if dynamic array is still unallocated
#if (TTC_SAFE_ARRAYS==1)
  #define ttc_array_is_null(NAME) (NAME.Data == NULL)
#else
  #define ttc_array_is_null(NAME) (NAME == NULL)
#endif

// defines a new safe dynamic array as local or global variable.
// The array is created with Size=0; Data=NULL.
// Before its first usage, call ttc_array_allocate() to allocate its memory for a given size dynamically.
#if (TTC_SAFE_ARRAYS==1)
  #define ttc_array_allocate(NAME, SIZE) NAME.Data = ttc_memory_alloc_zeroed(sizeof(NAME) * SIZE); NAME.Size = SIZE
#else
  #define ttc_array_allocate(NAME, SIZE) NAME = ttc_memory_alloc(SIZE)
#endif

// access to individual array elements
#if (TTC_SAFE_ARRAYS==1)
  #define A(NAME, INDEX) NAME.Data[_ttc_array_index( (Base_t*) &NAME, INDEX )]
#else
  #define A(NAME, INDEX) NAME[INDEX]
#endif


/** { _Safe Pointers

 A common error in C applications is dereferencing an invalid pointer.
 Basically this means a construct of type A->B. If A points to an invalid memory region
 then a corresponding hardware exception should be raised. This often means, that no local
 variables are accessible any more. If the pointer adresses invalid data the processor not
 even recognises the illegal access and reads invalid data or corrupts foreign memory regions.

 In C, dereferenciations are not checked at all.
 One may cast any constant to any type and try to dereference it.
 Assume the following:  u8_t D = *( (u8_t*) 0x0 );
 Though this is fine for the compiler, your processor won't like it.
 Reading from address zero is forbidden on many architectures.
 Doing so will cause an exception to be raised and a special handler will be called.
 In most cases, this means that the program flow comes to an end because the
 program counter is held in an endless loop.

 TheToolChain supports an implicit NULL-pointer checking.
 For this paradigm, it is required that every unused pointer is initialized with NULL (0).
 Pointers can then be checked against NULL by use of P() macro. It will check if given argument
 is not NULL and return a pointer of same type. You don't have to cast anything. Even completion
 works in your IDE.

 Of course, use of safe pointers inflicts some processing overhead.
 Therefore, all pointer checking can be disabled by defining TTC_SAFE_POINTERS = 0.
 This can be done best in your makefile (yes its -D):
 COMPILE_OPTS += -DTTC_SAFE_POINTERS=0

 Don't overdue pointer checking!
 Not every dereferenciation needs a pointer check. In small functions, it is egnough to check
 pointers int the beginning. Good functions will check all their pointer arguments via Assert()
 in the first lines.
 All ttc_XXX() functions do check their pointer arguments. Therefore it is not necessary to check
 pointers given to a ttc_XXX() function. You can safely write
   ttc_memory_set(B, 0, 100);
 instead of
   ttc_memory_set(P(B), 0, 100);


 Example:

 struct Data_s { u8_t A; u8_t* B; };

 struct Data_s* MyPointer = NULL;      // always initialize your pointers!

 ...

 u8_t A2 = P(MyPointer)->A;           // if MyPointer is still NULL, Assert() will be called
 P(MyPointer)->B = &A2;

 u8_t B2 Data = *(P(P(MyPointer).B)); // double dereferenciation with individual checks
 // The above line equals to:
 //   Assert(MyPointer, ec_NULL);
 //   u8_t* B1 = MyPointer->B;
 //   Assert(B1, ec_NULL);
 //   u8_t B2 = *B1;

 // BTW: You don't have to encapsulate every dereferenciation with P().
 //      In small code snippets, it is safe to check your pointers only at the beginning.
 //
 void copy(u8_t* Dst, u8_t* Src, u8_t Amount) {
   u8_t* Writer = P(Dst);
   u8_t* Reader = P(Src);

   while (Amount-- > 0)
     *Writer++ = *Reader++;
 }
 */

#ifndef TTC_SAFE_POINTERS
#define TTC_SAFE_POINTERS 1  // =1: use safe pointers; =0: use normal pointers (no overhead)
#endif

#if (TTC_SAFE_POINTERS==1)
#define P(NAME) (&(NAME[ttc_memory_checkpointer(NAME)]))
#else
#define P(NAME) NAME
#endif

#if (TTC_SAFE_POINTERS==1)
#define P2(NAME, MIN, MAX) (&(NAME[ttc_memory_checkpointer_within(NAME, MIN, MAX)]))
#else
#define P2(NAME, MIN, MAX) NAME
#endif

//}enums and structures
//{ Function prototypes **************************************************

// Basic memory operations ***********************************************

/** Initializes memory heap for first use
 *
 * Note: This function must be called BEFORE any _ttc_memory_alloc() call!
 * Note: This function is not thread safe!
 *
 * Note: Use nm tool to get an overview of your individual memory layout:
 *       arm-none-eabi-nm --numeric-sort main.elf
 */
void ttc_memory_init_heap();

/** tries to allocate a memory area of given size
 * Note: For many setups, the allocated memory block cannot be freed!
 * Note: All ttc_malloc() functions will call Assert_Halt_EC(ec_OutOfMemory) if allocation fails.
 *
 * @param Size   size of memory block to allocate
 * @return       =! NULL: address of allocated memory block; == NULL: memory could not be allocated (out of memory)
 */
void* ttc_memory_alloc(Base_t Size);
#ifdef _driver_memory_alloc
#define ttc_memory_alloc(Size) _ttc_memory_alloc(Size)
#else
#error Missing implementation for _driver_memory_alloc()
#endif
void* _ttc_memory_alloc(Base_t Size);

/** tries to allocate a memory area of given size and fills it with zero-bytes.
 * Note: For many setups, the allocated memory block cannot be freed!
 * Note: All ttc_malloc() functions will call Assert_Halt_EC(ec_OutOfMemory) if allocation fails.
 *
 * @param Size   size of memory block to allocate
 * @return       =! NULL: address of allocated memory block; == NULL: memory could not be allocated (out of memory)
 */
void* ttc_memory_alloc_zeroed(Base_t Size);

/** frees given memory area
 * Note: For many setups, ttc_free() does not provide an implementation!
 *
 * @param Block  address of memory block to free (must be allocated by ttc_memory_alloc() before!)
 * @return       ==NULL: block has been freed; ==Block: block has not been freed
 */
void* ttc_memory_free(void* Block);
#ifdef _driver_memory_free
#define ttc_memory_free(Block) _driver_memory_free(Block)
#else
#error Missing implementation for _driver_memory_free()
#endif

/** compares two memory areas byte by byte. Provides optimizations for certain architectures.
 * @param A      starting address of first memory area
 * @param B      starting address of first memory area
 * @param AmountBytes AmountBytes of bytes to compare
 * @return       =0: all checked bytes were the same; >0: index of first differing byte
 */
Base_t ttc_memory_compare(void* A, void* B, Base_t AmountBytes);

/** Writes given fill value into memory area. Provides optimizations for certain architectures.
 * @param Destination  data is written starting at this address
 * @param Fill         value to write into memory
 * @param AmountBytes  amount of bytes to write
 */
void ttc_memory_set(void* Destination, Base_t Fill, Base_t AmountBytes);
#ifndef memset
#  define memset(Destination, Fill, AmountBytes)  use_instead_ttc_memory_set(Destination, Fill, AmountBytes)
#endif

/** Copies data from one memory area to another one. Provides optimizations for certain architectures.
 * @param Destination  data is written starting at this address
 * @param Source       data is read starting from this address
 * @param AmountBytes       AmountBytes of bytes to copy
 */
void ttc_memory_copy(void* Destination, const void* Source, Base_t AmountBytes);


/** returns amount of available dynamic memory
 *
 * @return       amount of bytes available for allocation
 */
Base_t ttc_memory_get_free_size();
#ifdef _driver_memory_get_free_size
#define ttc_memory_get_free_size() _driver_memory_get_free_size()
#else
#error Missing implementation of _driver_memory_get_free_size()!
#endif

// Memory Blocks *********************************************************

/** Concept of memory blocks
 *
 * Unified datatype
 * Memory blocks are accepted by all ttc_XXX communication interfaces.
 *
 * Zero copy strategy
 * Every ttc-communication interface receives data into memory blocks of type ttc_memory_from_pool_t.
 * These memory blocks can be passed by reference to any other ttc-communication interface or
 * a compatible application
 *
 * Automatic release
 * Each memory block Block stores a function pointer Block->releaseBuffer().
 * After processing a memory block, ttc_memory_block_release(Block)-call has to be called to reduce Block->UseCount.
 * If Block->UseCount==0 then Block->releaseBuffer(Block) is called automatically to return
 * the memory block to its originator. The processor of a memory block must not know, who has initially created it.
 *
 * Memory blocks can travel freely between several tasks. Each blocks stores a ReturnQueue-handle.
 *
 * Scenario
 * If two tasks A and B want to communicate with each other. A creates data, puts it into buffers
 * and sends them to B for processing. After the processing, the memory of the buffers has to
 * return to A. If not, task B would eat up all dynamic memory and after a while, the whole
 * application stops because A cannot allocate more memory.
 *
 * In desktop applications, this problem is often avoided by sending a copy of all data inside the
 * memory block to task B. The big disadvantage of this approach is, that copying of data is very time consuming and
 * often not less memory intensive.
 *
 * The concept of memory blocks provides a simple, yet safe mechanism to recycle memory.
 * For this to work, task A first creates a ReturnQueue Q of pointers to memory blocks.
 * Whenever A wants to use a memory block, it first tries to pull one from Q.
 * If Q is empty, A allocates a new memory block and stores Q inside the block header.
 * When it sends the Pointer P to this ttc_memory_block_t to task B, Q is passed inside the
 * memory block too. After task B has finished processing data in the memory block, it simply
 * pushes P onto Q.
 */

/** allocates a memory block that can store given AmountBytes of bytes + some extra data.
 *
 * Note: All ttc_malloc() functions will call Assert_Halt_EC(ec_OutOfMemory) if allocation fails.
 *
 * @param Size           AmountBytes of bytes to be allocated for Buffer[]
 * @param Hint           argument passed to releaseBuffer() to allow fast release of buffers from different pools by single function
 * @param releaseBuffer  pointer to function that will take back ownership of block
 * @return               points to head of memory block struct. User data is part of this. (DO NOT CAST TO SOMETHING ELSE!)
 */
ttc_memory_block_t* ttc_memory_alloc_block(Base_t Size, u8_t Hint, void (*releaseBuffer)(ttc_memory_block_t* Block));

/** A virtual memory block is just a header without a buffer. Instead its Buffer-element points to a buffer somewhere else.
 * Virtual buffers can use existing buffers or even constant text in flash memory (if architecture supports pointers to flash memory)
 *
 * Note: All ttc_malloc() functions will call Assert_Halt_EC(ec_OutOfMemory) if allocation fails.
 *
 * @param Buffer         pointer to buffer to use for this memory block
 * @param Hint           argument passed to releaseBuffer() to allow fast release of buffers from different pools by single function
 * @param releaseBuffer  pointer to function that will take back ownership of block
 * @return               points to head of memory block struct. User data is part of this. (DO NOT CAST TO SOMETHING ELSE!)
 */
ttc_memory_block_t* ttc_memory_alloc_virtual_block(u8_t* Buffer, u8_t Hint, void (*releaseBuffer)(ttc_memory_block_t* Block));

/** Increases UseCounter of given memory block by 1
  *
  * You want to increase the Use-Counter whenever you want to give a memory block to more than one user.
  * (e.g. a second thread that works on the same block)
  * A block is released back only when UseCounter reaches 0 again.
  *
  * @param Block  address of memory block to release (must be allocated by ttc_memory_alloc() before!)
  */
void ttc_memory_block_use(ttc_memory_block_t* Block);

/** Decreases UseCount of given memory block by 1; if (UseCount==0) it is returned back to its origin by calling stored release function
 *
 * @param Block  address of memory block to release (must be allocated by ttc_memory_alloc() before!)
 */
void ttc_memory_block_release(ttc_memory_block_t* Block);

/** registers given data for debugging purposes
  *
  * Note: low-level heap allocator must call this function to register each new allocated block!
  *
  * @param Address  start address of allocated block
  * @param Size     size of allocated block (bytes)
  */
void _ttc_memory_register_block(void* Address, Base_t Size);

/** removes registration data of a memory block
  *
  * Note: low-level heap allocator must call this function for each new freed block!
  *
  * @param Address  start address of allocated block
  * @param Size     size of allocated block (bytes)
  */
void _ttc_memory_unregister_block(void* Address, Base_t Size);

// memory pools *********************************************************

/** Concept of memory pools
 *
 * Asynchronous communication between tasks requires one or more memory blocks to be allocated and reused.
 * The required amount of memory blocks is often not known at compile time. Incoming data often occurs
 * in bursts.
 * A memory pool provides an automatic block management. New memory blocks are allocated if required.
 * Processed memory blocks have to be released for later reuse.
 *
 * For access from interrupt service routines, special functions are provided.
 */

/** tries to allocate a new memory pool that will manage memory blocks of same size
 *
 * @param BufferSize    payload size of each memory block in this pool (size of Buffer[] element)
 * @param MaxAllocated     !=-1: maximum allowed amount of blocks to be allocated by this pool
 *                      ==-1: amount of allocatable blocks only limited by memory heap
 * @return              =! NULL: address of allocated memory pool; == NULL: memory pool could not be created (out of memory)
 */
ttc_memory_pool_t* ttc_memory_pool_create(TTC_MEMORY_BLOCK_BASE BufferSize, Base_t MaxAllocated);

/** initializes given memory for use as a memory pool
 *
 * @param Pool          memory to use as pool management data
 * @param BufferSize    payload size of each memory block in this pool (size of Buffer[] element)
 * @param MaxAllocated  !=-1: maximum allowed amount of blocks to be allocated by this pool
 *                      ==-1: amount of allocatable blocks only limited by memory heap
 * @return              =! NULL: address of allocated memory pool; == NULL: memory pool could not be created (out of memory)
 */
void ttc_memory_pool_init(ttc_memory_pool_t* Pool, TTC_MEMORY_BLOCK_BASE Size, Base_t MaxAllocated);

/** returns list-item embedded into given memory block for use with ttc_list.c functions
 *
 * Note: you may define TTC_MEMORY_MAGICKEY to enable extra validity checks
 *
 * @param Block     !=NULL: pointer to a pool memory block as returned by ttc_memory_pool_block_get() before
 *                  ==NULL: simply returns NULL
 * @return          pointer to a list-item for use with ttc_list.c functions
 */
ttc_list_item_t* ttc_memory_pool_to_list_item(ttc_memory_from_pool_t* Block);

/** converts list-item returned from ttc_memory_pool_to_list_item() back to pool memory
 *
 * Note: you may define TTC_MEMORY_MAGICKEY to enable extra validity checks
 *
 * @param Item      !=NULL: list-item that has been derived from a pool memory block via ttc_memory_pool_to_list_item() before
 *                  ==NULL: simply returns NULL
 * @return          pointer to restored pool memory block
 */
ttc_memory_from_pool_t* ttc_memory_pool_from_list_item(ttc_list_item_t* Item);

/** allocates a new/ reuses a released memory block from given memory pool
 *
 * Note: This function is to be used from tasks and not from interrupt service routines!
 * Note: returned address points to allocated memory directly AFTER header struct and can be directly casted to something else
 *
 * @param Pool      memory pool created via ttc_memory_pool_create()
 * @return          =! NULL: address of newly allocated or reused  memory block without header
 *                  == NULL: memory pool could not provide another memory block (blocks limited/ out of memory)
 */
ttc_memory_from_pool_t* ttc_memory_pool_block_get(ttc_memory_pool_t* Pool);

/** allocates a new/ reuses a released memory block from given memory pool (use from interrupt service routine)
 *
 * Note: This function is to be used from interrupt service routines and not from tasks!
 *
 * @param Pool      memory pool created via ttc_memory_pool_create()
 * @return          =! NULL: address of newly allocated or reused  memory block; == NULL: memory pool could not provide another memory block (blocks limited/ out of memory)
 */
//? ttc_memory_block_t* ttc_memory_pool_get_block_isr(ttc_memory_pool_t* Pool);

/** returns a memory block back to its memory pool for later reuse
 *
 * Note: This function is to be used from tasks and not from interrupt service routines!
 *
 * @param Pool      memory pool created via ttc_memory_pool_create()
 * @param Block     memory block that has been got from same memory pool Pool
 */
void ttc_memory_pool_block_free(ttc_memory_from_pool_t* Block);

/** returns a memory block back to its memory pool for later reuse (use from interrupt service routine)
 *
 * Note: This function is to be used from interrupt service routines and not from tasks!
 *
 * @param Pool      memory pool created via ttc_memory_pool_create()
 * @param Block     memory block that has been got from same memory pool Pool
 */
//? void ttc_memory_pool_block_free_block_isr(ttc_memory_pool_t* Pool, ttc_memory_block_t* Block);


//}Function prototypes

#endif
