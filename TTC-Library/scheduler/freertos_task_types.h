#ifndef freertos_task_types_H
#define freertos_task_types_H

/** { freertos_task_types.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Datatypes used by Low-level multitasking interface using FreeRTOS scheduler.
 *
}*/

//{ includes

//? #include "../ttc_basic_types.h"

// includes from FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "list.h"
#include "portmacro.h"

//}
//{ external datatypes ***********************************************

// FreeRTOS provides two types of stack checking
#ifdef INCLUDE_uxTaskGetStackHighWaterMark
    #ifdef EXTENSION_300_scheduler_freertos_stack_check_simple
        #define TTC_TASK_STACK_CHECKS_ENABLED 1
    #endif
    #ifdef EXTENSION_300_scheduler_freertos_stack_check_intense
        #define TTC_TASK_STACK_CHECKS_ENABLED 1
    #endif
#endif

#ifndef FREERTOS_INCLUDE // protect from freertos includes (freertos source include ttc_memory.h instead of <string.h>. ttc_memory.h will include us and unveil these double definitions)

/** Task control block.  A task control block (TCB) is allocated for each task,
 *  and stores task state information, including a pointer to the task's context
 *  (the task's run time environment, including register values)
 *
 * (taken from additionals/300_scheduler_freertos/Source/tasks.c)
 */
#if 1 // FreeRTOS 8.x
//ToDo: automatically extract struct tskTaskControlBlock from tasks.c into extra header file for automated update!
typedef enum {
    eNotWaitingNotification = 0,
    eWaitingNotification,
    eNotified
} eNotifyValue;

typedef struct tskTaskControlBlock {
    volatile StackType_t*    pxTopOfStack;  /*< Points to the location of the last item placed on the tasks stack.  THIS MUST BE THE FIRST MEMBER OF THE TCB STRUCT. */

#if ( portUSING_MPU_WRAPPERS == 1 )
    xMPU_SETTINGS   xMPUSettings;       /*< The MPU settings are defined as part of the port layer.  THIS MUST BE THE SECOND MEMBER OF THE TCB STRUCT. */
    BaseType_t      xUsingStaticallyAllocatedStack; /* Set to pdTRUE if the stack is a statically allocated array, and pdFALSE if the stack is dynamically allocated. */
#endif

    ListItem_t          xGenericListItem;   /*< The list that the state list item of a task is reference from denotes the state of that task (Ready, Blocked, Suspended ). */
    ListItem_t          xEventListItem;     /*< Used to reference a task from an event list. */
    UBaseType_t         uxPriority;         /*< The priority of the task.  0 is the lowest priority. */
    StackType_t*         pxStack;           /*< Points to the start of the stack. */
    char                pcTaskName[ configMAX_TASK_NAME_LEN ];/*< Descriptive name given to the task when created.  Facilitates debugging only. */ /*lint !e971 Unqualified char types are allowed for strings and single characters only. */

#if ( portSTACK_GROWTH > 0 )
    StackType_t*     pxEndOfStack;      /*< Points to the end of the stack on architectures where the stack grows up from low memory. */
#endif

#if ( portCRITICAL_NESTING_IN_TCB == 1 )
    UBaseType_t     uxCriticalNesting;  /*< Holds the critical section nesting depth for ports that do not maintain their own count in the port layer. */
#endif

#if ( configUSE_TRACE_FACILITY == 1 )
    UBaseType_t     uxTCBNumber;        /*< Stores a number that increments each time a TCB is created.  It allows debuggers to determine when a task has been deleted and then recreated. */
    UBaseType_t     uxTaskNumber;       /*< Stores a number specifically for use by third party trace code. */
#endif

#if ( configUSE_MUTEXES == 1 )
    UBaseType_t     uxBasePriority;     /*< The priority last assigned to the task - used by the priority inheritance mechanism. */
    UBaseType_t     uxMutexesHeld;
#endif

#if ( configUSE_APPLICATION_TASK_TAG == 1 )
    TaskHookFunction_t pxTaskTag;
#endif

#if( configNUM_THREAD_LOCAL_STORAGE_POINTERS > 0 )
    void* pvThreadLocalStoragePointers[ configNUM_THREAD_LOCAL_STORAGE_POINTERS ];
#endif

#if ( configGENERATE_RUN_TIME_STATS == 1 )
    unsigned long     ulRunTimeCounter;   /*< Stores the amount of time the task has spent in the Running state. */
#endif

#if ( configUSE_NEWLIB_REENTRANT == 1 )
    /* Allocate a Newlib reent structure that is specific to this task.
    Note Newlib support has been included by popular demand, but is not
    used by the FreeRTOS maintainers themselves.  FreeRTOS is not
    responsible for resulting newlib operation.  User must be familiar with
    newlib and must provide system-wide implementations of the necessary
    stubs. Be warned that (at the time of writing) the current newlib design
    implements a system-wide malloc() that must be provided with locks. */
    struct  _reent xNewLib_reent;
#endif

#if ( configUSE_TASK_NOTIFICATIONS == 1 )
    volatile unsigned long ulNotifiedValue;
    volatile eNotifyValue eNotifyState;
#endif

} tskTCB;
#else // FreeRTOS 7.x
typedef struct tskTaskControlBlock {
    volatile portSTACK_TYPE*    pxTopOfStack;       /*< Points to the location of the last item placed on the tasks stack.  THIS MUST BE THE FIRST MEMBER OF THE TCB STRUCT. */

#if ( portUSING_MPU_WRAPPERS == 1 )
    xMPU_SETTINGS xMPUSettings;             /*< The MPU settings are defined as part of the port layer.  THIS MUST BE THE SECOND MEMBER OF THE TCB STRUCT. */
#endif


    xListItem               xGenericListItem;   /*< The list that the state list item of a task is reference from denotes the state of that task (Ready, Blocked, Suspended ). */
    xListItem               xEventListItem;     /*< Used to reference a task from an event list. */
    unsigned portBASE_TYPE  uxPriority;         /*< The priority of the task.  0 is the lowest priority. */
    portSTACK_TYPE*         pxStack;            /*< Points to the start of the stack. */
    signed char             pcTaskName[ configMAX_TASK_NAME_LEN ];/*< Descriptive name given to the task when created.  Facilitates debugging only. */

#if ( portSTACK_GROWTH > 0 )
    portSTACK_TYPE* pxEndOfStack;           /*< Points to the end of the stack on architectures where the stack grows up from low memory. */
#endif

#if ( portCRITICAL_NESTING_IN_TCB == 1 )
    unsigned portBASE_TYPE uxCriticalNesting; /*< Holds the critical section nesting depth for ports that do not maintain their own count in the port layer. */
#endif

#if ( configUSE_TRACE_FACILITY == 1 )
    unsigned portBASE_TYPE  uxTCBNumber;    /*< Stores a number that increments each time a TCB is created.  It allows debuggers to determine when a task has been deleted and then recreated. */
    unsigned portBASE_TYPE  uxTaskNumber;   /*< Stores a number specifically for use by third party trace code. */
#endif

#if ( configUSE_MUTEXES == 1 )
    unsigned portBASE_TYPE uxBasePriority;  /*< The priority last assigned to the task - used by the priority inheritance mechanism. */
#endif

#if ( configUSE_APPLICATION_TASK_TAG == 1 )
    pdTASK_HOOK_CODE pxTaskTag;
#endif

#if ( configGENERATE_RUN_TIME_STATS == 1 )
    unsigned long ulRunTimeCounter;         /*< Stores the amount of time the task has spent in the Running state. */
#endif

#if ( configUSE_NEWLIB_REENTRANT == 1 )
    /* Allocate a Newlib reent structure that is specific to this task.
    Note Newlib support has been included by popular demand, but is not
    used by the FreeRTOS maintainers themselves.  FreeRTOS is not
    responsible for resulting newlib operation.  User must be familiar with
    newlib and must provide system-wide implementations of the necessary
    stubs. Be warned that (at the time of writing) the current newlib design
    implements a system-wide malloc() that must be provided with locks. */
    struct _reent xNewLib_reent;
#endif
} tskTCB;
#endif

#define TaskHandle_t tskTCB*

// defined in tasks.c
void prvListTaskWithinSingleList( const signed char* pcWriteBuffer, xList* pxList, signed char cStatus ) PRIVILEGED_FUNCTION;
extern volatile tskTCB* pxCurrentTCB;
extern xList pxReadyTasksLists[configMAX_PRIORITIES + 1];
extern xList* pxDelayedTaskList;
extern xList* pxOverflowDelayedTaskList;
extern xList xPendingReadyList;
extern xList xTasksWaitingTermination;
extern xList xSuspendedTaskList;
extern volatile const unsigned portBASE_TYPE uxTopUsedPriority;
#define tskBLOCKED_CHAR     ( ( signed char ) 'B' )
#define tskREADY_CHAR       ( ( signed char ) 'R' )
#define tskDELETED_CHAR     ( ( signed char ) 'D' )
#define tskSUSPENDED_CHAR   ( ( signed char ) 'S' )

#endif
//}external datatypes

#endif
