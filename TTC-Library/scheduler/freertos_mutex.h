#ifndef freertos_mutex_H
#define freertos_mutex_H

/*{ freertos_mutex.h ***********************************************
 *
 * Written by Gregor Rebel 2012-2013
 *
 * Architecture independent support for inter task communication & synchronization by use of
 * multitasking scheduler FreeRTOS.
 *
}*/

//{ includes

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "freertos_mutex_types.h"
#include "freertos_queue_types.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "portmacro.h"
#include "list.h"

#ifndef EXTENSION_300_scheduler_freertos
    #error Missing extension EXTENSION_300_scheduler_freertos (did you forget to activate it?)
#endif

//}includes
//{ Default settings (change via makefile) *******************************
//}Default settings
//{ Structures ***********************************************************

//}Structures
//{ Low-Level driver macros for ttc_mutex.h ******************************

#define _driver_mutex_create()               freertos_mutex_create()
#define _driver_mutex_init(Mutex)            freertos_mutex_init(Mutex)
#define _driver_mutex_lock(Mutex, TimeOut)   freertos_mutex_lock(Mutex, TimeOut)
#define _driver_mutex_lock_isr(Mutex)        freertos_mutex_lock_isr(Mutex)
#define _driver_mutex_unlock(Mutex)          freertos_mutex_unlock(Mutex)
#define _driver_mutex_unlock_isr(Mutex)      freertos_mutex_unlock_isr(Mutex)
#define _driver_mutex_is_locked(Mutex)       freertos_mutex_is_locked(Mutex)

//}
//{ includes2

#include "../ttc_mutex_types.h"
#include "../ttc_memory.h"
#include "../ttc_task.h"

//} includes2
//{ Function prototypes **************************************************


/** creates a new non-recursive mutex lock
 * @return                 =! NULL: mutex has been created successfully (use handle to operate on mutex)
 */
t_ttc_mutex* freertos_mutex_create();

/** Initializes given structure as mutex in unlocked state
 * @param Mutex  reference of t_ttc_mutex-structure to init
 */
e_ttc_mutex_error freertos_mutex_init( volatile t_ttc_mutex* Mutex );

/** Tries to obtain lock on mutex within timeout
 * @param Mutex  handle of already created mutex
 * @param TimeOut  !=-1: amount of system ticks to wait for lock
 *                 ==-1: wait forever
 * @return         == tme_OK: Lock has been successfully obtained within TimeOut
 */
e_ttc_mutex_error freertos_mutex_lock( volatile t_ttc_mutex* Mutex, t_base TimeOut );

/** Checks if given Mutex is currently locked
 * @param Mutex    handle of already created mutex
 * @return         == TRUE: given Mutex is currently in locked state; FALSE otherwise
 */
// BOOL freertos_mutex_is_locked(t_ttc_mutex* Mutex);
#define freertos_mutex_is_locked(Mutex) (Mutex && (Mutex->uxMessagesWaiting > 0))

/** Tries to obtain lock on mutex; this function will never block
 * @param Mutex                handle of already created mutex
 * @return         == tme_OK: Lock has been successfully obtained within TimeOut
 *                 == tme_WasLockedByTask: - a task of higher priority has been interrupted => call ttc_task_yield() at end of your interrupt service routine!
 *                 >= tme_Error: Error condition
 */
e_ttc_mutex_error freertos_mutex_lock_isr( volatile t_ttc_mutex* Mutex );


/** Releases an already obtained lock from mutex
 * @param Mutex  handle of already created smart mutex
 * @return         == tme_OK: Lock has been released successfully
 */
e_ttc_mutex_error freertos_mutex_unlock( volatile t_ttc_mutex* Mutex );

/** Releases an already obtained lock from mutex (does never block)
 * @param Mutex  handle of already created mutex
 * @return         == tme_OK: Lock has been released successfully
 *                 == tme_WasLockedByTask: call task ttc_task_yield_isr() before exiting your ISR!
 */
e_ttc_mutex_error freertos_mutex_unlock_isr( t_ttc_mutex* Mutex );


//}Function prototypes

#endif
