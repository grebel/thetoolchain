/*{ freertos_semaphore.c ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Architecture independent support for inter task communication & synchronization.
 *
 * Currently supported schedulers: FreeRTOS
}*/
#include "freertos_semaphore.h"
#include "../systick/systick_freertos.h"
#include "../ttc_heap.h"

//{ functions *************************************************

t_driver_semaphore* freertos_semaphore_create() {

    t_driver_semaphore* NewSemaphore = ttc_heap_alloc_zeroed( sizeof( t_driver_semaphore ) );
    Assert_SEMAPHORE_Writable( NewSemaphore, ttc_assert_origin_auto );
    freertos_semaphore_init( NewSemaphore );

    return NewSemaphore;
}
void freertos_semaphore_init( t_driver_semaphore* Semaphore ) {


#define fsi_uxItemSize 0
#define fsi_uxQueueLength -1
#define fsi_ucQueueType queueQUEUE_TYPE_COUNTING_SEMAPHORE
#define fsi_uxInitialCount 0


    // copied from queue.c/xQueueGenericCreate()
    // (FreeRTOS does not provide an _init() by itself

    /* Create the list of pointers to queue items.  The queue is one byte
       longer than asked for to make wrap checking easier/faster. */
    t_base xQueueSizeInBytes = ( size_t )( fsi_uxQueueLength * fsi_uxItemSize ) + ( size_t ) 1;

    Semaphore->pcHead = ( signed char* ) pvPortMalloc( xQueueSizeInBytes );
    if ( Semaphore->pcHead != NULL ) {
        /* Initialise the queue members as described above where the queue type is defined. */
        Semaphore->uxLength   = fsi_uxQueueLength;
        Semaphore->uxItemSize = fsi_uxItemSize;
        xQueueGenericReset( Semaphore, pdTRUE );
        #if ( configUSE_TRACE_FACILITY == 1 )
        {
            Semaphore->ucQueueType = fsi_ucQueueType;
        }
        #endif /* configUSE_TRACE_FACILITY */

        traceQUEUE_CREATE( Semaphore );
    }
    else {
        traceQUEUE_CREATE_FAILED( fsi_ucQueueType );
        vPortFree( Semaphore );
    }

    // copied from queue.c/xQueueCreateCountingSemaphore()
    // (FreeRTOS does not provide an _init() by itself
    if ( Semaphore != NULL ) {
        Semaphore->uxMessagesWaiting = fsi_uxInitialCount;

        traceCREATE_COUNTING_SEMAPHORE();
    }
    else {
        traceCREATE_COUNTING_SEMAPHORE_FAILED();
    }
}
e_ttc_semaphore_error freertos_semaphore_give( t_driver_semaphore* Semaphore, t_base Amount ) {
    Assert_SEMAPHORE( Semaphore != 0, ttc_assert_origin_auto );

    signed portBASE_TYPE Result;
    for ( ; Amount > 0; Amount-- ) {
        Result = xSemaphoreGive( Semaphore );
        Assert_SEMAPHORE( Result == pdTRUE, ttc_assert_origin_auto );
    }
    return tsme_OK;
}
e_ttc_semaphore_error freertos_semaphore_take( t_driver_semaphore* Semaphore, t_base Amount, t_base TimeOut ) {
    Assert_SEMAPHORE( Semaphore != 0, ttc_assert_origin_auto );

    // calculate ticks from usecs
    TimeOut = systick_freertos_calculate_ticks( TimeOut );

    if ( Amount > 1 ) { // FreeRTOS does not provide taking >1 from a semaphore at once
        for ( t_base I = Amount; I > 0; I-- ) { // take one from semaphore several times within TimeOut
            t_base StartTime = systick_freertos_get_elapsed_ticks();
            if ( xSemaphoreTake( Semaphore, TimeOut ) != pdTRUE ) { // timeout occured: abort complete operation
                for ( ; I < Amount; I++ ) { // roll back operation
                    xSemaphoreGive( Semaphore );
                }
                return tsme_TimeOut;
            }
            if ( Amount > 1 ) {
                t_base Duration = systick_freertos_get_elapsed_ticks() - StartTime;
                if ( TimeOut < Duration )
                { TimeOut -= Duration; }
                else
                { TimeOut = 0; }
            }
        }
    }
    else {
        if ( xSemaphoreTake( Semaphore, TimeOut ) != pdTRUE )
        { return tsme_TimeOut; }
    }
    return tsme_OK;
}
e_ttc_semaphore_error freertos_semaphore_give_isr( t_driver_semaphore* Semaphore, t_base Amount ) {
    Assert_SEMAPHORE( Semaphore != 0, ttc_assert_origin_auto );

    signed portBASE_TYPE Result;
    for ( ; Amount > 0; Amount-- ) {
        Result = xSemaphoreGiveFromISR( Semaphore, NULL ); // second argument may be NULL (FreeRTOS >= 7.3.0)
        if ( Result == errQUEUE_FULL )
        { return tsme_SemaphoreFull; }
        Assert_SEMAPHORE( Result == pdTRUE, ttc_assert_origin_auto );
    }
    return tsme_OK;
}
e_ttc_semaphore_error freertos_semaphore_take_isr( t_driver_semaphore* Semaphore, t_base Amount ) {

    signed portBASE_TYPE Result;
    for ( t_base I = Amount; I > 0; I-- ) { // FreeRTOS does not provide taking >1 from semaphore at once
        Result = xSemaphoreTakeFromISR( Semaphore, NULL ); // second argument may be NULL (FreeRTOS >= 7.3.0)
        if ( Result != pdTRUE ) {
            for ( ; I < Amount; I++ ) { // roll back operation
                xSemaphoreGiveFromISR( Semaphore, NULL ); // second argument may be NULL (FreeRTOS >= 7.3.0)
            }
            return tsme_SemaphoreExpired;
        }
    }
    return tsme_OK;
}

// private functions ********************************************

//}functions
