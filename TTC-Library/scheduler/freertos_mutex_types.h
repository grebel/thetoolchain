#ifndef freertos_mutex_types_H
#define freertos_mutex_types_H

/*{ freertos_mutex.h ***********************************************
 *
 * Written by Gregor Rebel 2012-2013
 *
 * Architecture independent support for inter task communication & synchronization by use of
 * multitasking scheduler FreeRTOS.
 *
}*/

//{ includes

#include "../ttc_basic.h"
#include "freertos_queue_types.h"

#ifndef EXTENSION_300_scheduler_freertos
    #error Missing extension EXTENSION_300_scheduler_freertos (did you forget to activate it?)
#endif

//}includes
//{ Default settings (change via makefile) *******************************
//}Default settings
//{ Structures ***********************************************************

//}Structures
//{ Low-Level driver macros for ttc_mutex.h ******************************

//typedef xQUEUE_Structure t_ttc_mutex;
#define t_ttc_mutex xQUEUE_Structure

//}

#endif
