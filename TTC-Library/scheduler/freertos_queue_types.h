#ifndef freertos_queue_types_H
#define freertos_queue_types_H

/*{ freertos_queue_types.h *************************************************
 *
 * Written by Gregor Rebel 2013
 *
 * queue-sturctures being declared as private in FreeRTOS queue.c
 * These structures are required for some low-level TTC-drivers.
 *
}*/

//{ includes

#include "../ttc_basic.h"
//? #include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "list.h"
//? #include "queue.h"

//}includes
//{ Types & Structures ***************************************************

/*
 * Definition of the queue used by the scheduler.
 * Items are queued by copy, not reference.
 */
#ifndef FREERTOS_INCLUDE
typedef struct QueueDefinition {
    signed char* pcHead;                // Points to the beginning of the queue storage area.
    signed char* pcTail;                // Points to the byte at the end of the queue storage area.  Once more byte is allocated than necessary to store the queue items, this is used as a marker.

    signed char* pcWriteTo;             // Points to the free next place in the storage area.
    signed char* pcReadFrom;            // Points to the last place that a queued item was read from.

    xList xTasksWaitingToSend;              // List of tasks that are blocked waiting to post onto this queue.  Stored in priority order.
    xList xTasksWaitingToReceive;           // List of tasks that are blocked waiting to read from this queue.  Stored in priority order.

    volatile unsigned portBASE_TYPE uxMessagesWaiting;// The number of items currently in the queue.
    unsigned portBASE_TYPE uxLength;        // The length of the queue defined as the number of items it will hold, not the number of bytes.
    unsigned portBASE_TYPE uxItemSize;      // The size of each items that the queue will hold.

    volatile signed portBASE_TYPE xRxLock;  // Stores the number of items received from the queue (removed from the queue) while the queue was locked.  Set to queueUNLOCKED when the queue is not locked.
    volatile signed portBASE_TYPE xTxLock;  // Stores the number of items transmitted to the queue (added to the queue) while the queue was locked.  Set to queueUNLOCKED when the queue is not locked.

#if ( configUSE_TRACE_FACILITY == 1 )
    unsigned char ucQueueNumber;
    unsigned char ucQueueType;
#endif

} xQUEUE_Structure;
#endif
//}

#endif
