#ifndef freertos_task_H
#define freertos_task_H

/** { freertos_task.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Low-level multitasking interface using FreeRTOS scheduler.
 *
}*/

//{ includes

#ifndef EXTENSION_300_scheduler_freertos
    #error Missing extension! Please add this to your activate_project.sh script: activate.300_scheduler_freertos.sh
#endif

#include "freertos_task_types.h"
#include "../ttc_basic.h"
#include "../ttc_memory_types.h"
#include "../ttc_task_types.h"

// headers from FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "list.h"
#include "portmacro.h"

//}includes
//{ constants

#ifndef configTICK_RATE_HZ
    #  error configTICK_RATE_HZ not defined, check your FreeRTOSConfig.h!
    #define configTICK_RATE_HZ 1
#endif

#ifndef freertos_task_MINIMAL_STACK_SIZE
    #ifdef configMINIMAL_STACK_SIZE
        #define freertos_task_MINIMAL_STACK_SIZE configMINIMAL_STACK_SIZE
    #else
        #define freertos_task_MINIMAL_STACK_SIZE 128
    #endif
#endif

//}

// provided by port.c
extern unsigned portBASE_TYPE uxCriticalNesting;
// extern volatile TCB_t* pxCurrentTCB;

//{ Function prototypes **************************************************

#define _driver_task_critical_begin()   portENTER_CRITICAL()
#define _driver_task_critical_end()     portEXIT_CRITICAL()
#define _driver_task_critical_all_end(StoreCounter) freertos_task_critical_all_end(StoreCounter)
#define _driver_task_critical_all_restore(StoreCounter) freertos_task_critical_all_restore(StoreCounter)

#if ( INCLUDE_vTaskSuspend == 1 )
    #define _driver_task_resume(Handle)          vTaskResume(Handle)
    #define _driver_task_resume_isr(Handle)      xTaskResumeFromISR(Handle)
    #define _driver_task_suspend(Handle)         vTaskSuspend(Handle)
    #define _driver_task_resume_now(Handle)      freertos_task_resume_now(Handle)
    #define _driver_task_resume_now_isr(Handle)  freertos_task_resume_now_isr(Handle)
#endif
#define _driver_task_start_scheduler()         vTaskStartScheduler()
#define _driver_task_yield_isr()               vPortYieldFromISR()
#define _driver_task_yield()                   taskYIELD()
#define _driver_task_create(TaskFunction,StackSize,Argument,Priority,Handle) freertos_task_create(TaskFunction,StackSize,Argument,Priority,Handle)
#define _driver_task_current_handle()          ((TaskHandle_t) pxCurrentTCB)
#define _driver_task_update_info(Handle, Info) freertos_task_update_info(Handle, Info)
#define _driver_task_usleep(Delay)             freertos_task_usleep(Delay)
#define _driver_task_min_delay                 (1000000 / configTICK_RATE_HZ) // smallest delay (usecs) depends on tick rate <-- test setting via ttc_task_calibrate_usleep()
#define _driver_task_get_amount()              uxTaskGetNumberOfTasks()
#define _driver_task_get_all(Buffer)           freertos_task_get_all(Buffer)
#define _driver_task_get_info(Handle)          freertos_task_get_info(Handle)
/* Deprecated  (moved into systick_freertos)
#define _driver_task_get_elapsed_usecs()          (1000000 / (configTICK_RATE_HZ)) * freertos_get_tick_count()
#define _driver_task_get_elapsed_ticks()          freertos_get_tick_count()
#define _driver_task_get_us_ticks(Microseconds)   (Microseconds / (1000000 / configTICK_RATE_HZ))
*/
#define _driver_task_is_inside_critical_section() (uxCriticalNesting > 0)

#if defined(INCLUDE_uxTaskGetStackHighWaterMark)
    #define _driver_task_check_stack()           uxTaskGetStackHighWaterMark(NULL)
#else
    // dummy value that is always > 0
    #define _driver_task_check_stack()           1
#endif

/** set task to sleep for some amount of time
 * @param Delay    delay time (usecs)
 */
void freertos_task_usleep( t_base Delay );

/** spawns a function as separate task
 * @param TaskFunction  function to start as task (must never return!)
 * @param StackSize     each task has its own size; choose wisely
 * @param Argument      passed as argument to TaskFunction()
 * @param Priority      tasks with higher priority get more cpu time
 * @param TaskInfo      info structure where to store task handle (required to operate on task later
 * @return              == TRUE: task was created successfully
 */
BOOL freertos_task_create( void TaskFunction( void* ), t_u16 StackSize, void* Argument, t_u8 Priority, t_ttc_task_info* TaskInfo );

/** resumes a suspended task to be executed now
  * The given task will be set to be executed first after next context switch
  * Note: Do not call this function from an interrupt service routine!
  *
  * @param !=NULL: Handle task identifier as returned by ttc_task_create
  */
void freertos_task_resume_now( TaskHandle_t Handle );

/** resumes a suspended task to be executed after current interrupt service routine has finished
  * The given task will be set to be executed first after next context switch
  *
  * @param !=NULL: Handle task identifier as returned by ttc_task_create
  */
void freertos_task_resume_now_isr( TaskHandle_t Handle );

/** Resets critical section counter and immediately enables task scheduler
 *
 * This function is required if you want to force a task switch and to restore the original state afterwards.
 *
 * Note: This function differs from _criticalsection() above.
 *       In opposite to them, these get called in order
 *       1) ttc_task_critical_all_end()
 *       2) ttc_task_critical_all_restore()
 *
 * @param StoreCounter  stores old value of critical section counter for later restore
 */
void freertos_task_critical_all_end( t_u8* StoreCounter );

/** Restores previously ended value of critical sections
 *
 * @param StoreCounter  storage provided to ttc_task_critical_all_end()
 */
void freertos_task_critical_all_restore( t_u8 StoreCounter );

/** returns pointer to configuration data of current/ given task
  *
  * Note: This function is very fast because it does not update configuration.
  *
  * @param Handle  !=NULL: get info of corresponding task
  *                ==NULL: get info of current task
  * @param Info    !=NULL: Info configuration of current task control block is copied into given variable
  *                ==NULL: a local static buffer will be allocated (do not use from multiple threads!)
  */
t_ttc_task_info* freertos_task_get_info( TaskHandle_t Handle );

/** collects configuration data of current task
  *
  * Note: This function is slow because it does update configuration data.
  *
  * @param Handle  !=NULL: get info of corresponding task
  *                ==NULL: get info of current task
  * @param Info    !=NULL: Info configuration of current task control block is copied into given variable
  *                ==NULL: a local static buffer will be allocated (do not use from multiple threads!)
  */
void freertos_task_update_info( TaskHandle_t Handle, t_ttc_task_info* Info );

/** spawns a function as separate task
 * @param  TaskInfos reference of array where to store task-infos (use _driver_task_get_amount() to calculate size!)
 * @return TaskInfos is loaded with data of all tasks (Make sure memory block big egnough!)
 */
void freertos_task_get_all( t_ttc_task_all* TaskInfos );

/** called in case pvPortMalloc() fails */
void vApplicationMallocFailedHook( void );

void vApplicationStackOverflowHook( xTaskHandle pxTask, char* pcTaskName );

//}Function prototypes

#endif
