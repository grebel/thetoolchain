#ifndef freertos_semaphore_H
#define freertos_semaphore_H

/*{ freertos_semaphore.h *************************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Low-level driver for Mutual Exclusive Amounts (Semaphores) using implementation provided by FreeRTOS.
 *
}*/

//{ includes

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "freertos_queue_types.h"
#include "freertos_task.h"
#include "semphr.h"

//}includes
//{ Default settings (change via makefile) *******************************


//}Default settings
//{ Types & Structures ***************************************************


//}
//{ Low-Level driver macros for ttc_semaphore.h **************************

#define _driver_semaphore_create()                         freertos_semaphore_create()
#define _driver_semaphore_init(Semaphore)                  freertos_semaphore_init(Semaphore)
#define _driver_semaphore_take(Semaphore, Amount, TimeOut) freertos_semaphore_take(Semaphore, Amount, TimeOut)
#define _driver_semaphore_take_isr(Semaphore, Amount)      freertos_semaphore_take_isr(Semaphore, Amount)
#define _driver_semaphore_can_provide(Semaphore, Amount)   freertos_semaphore_can_provide(Semaphore, Amount)
#define _driver_semaphore_give(Semaphore, Amount)          freertos_semaphore_give(Semaphore, Amount)
#define _driver_semaphore_give_isr(Semaphore, Amount)      freertos_semaphore_give_isr(Semaphore, Amount)
#define _driver_semaphore_available(Semaphore)             (Semaphore)->uxMessagesWaiting
#define _driver_semaphore_take_try(Semaphore, Amount)      freertos_semaphore_take_isr(Semaphore, Amount)

//}
//{ includes 2 ***********************************************************

#include "freertos_semaphore_types.h"

// these includes require macros and types above
#include "../ttc_semaphore_types.h"
#include "../ttc_memory.h"

//}includes 2
//{ Function prototypes **************************************************


/** allocates + initializes a new non-recursive semaphore Amount
 * @return                 =! NULL: semaphore has been created successfully (use handle to operate on semaphore)
 */
t_driver_semaphore* freertos_semaphore_create();

/** initializes an existing non-recursive semaphore Amount to the unAmounted state
 * @Amount                 storage to be initialized as semaphore
 */
void freertos_semaphore_init( t_driver_semaphore* Semaphore );

/** Tries to take given amount from semaphore within timeout
 * @param Semaphore  handle of already created semaphore
 * @param TimeOut  !=-1: amount of system ticks to wait for Amount
 *                 ==-1: wait forever
 * @return         == tsme_OK: Amount has been successfully obtained within TimeOut
 */
e_ttc_semaphore_error freertos_semaphore_take( t_driver_semaphore* Semaphore, t_base Amount, t_base TimeOut );

/** Tries to take given amount from semaphore; this function will never block and must only be called with disabled interrupts! (only to be called by interrupt service routines!)
 * @param Semaphore    handle of already created semaphore
 * @return         == tsme_OK: Amount has been successfully obtained within TimeOut
 *                 == tsme_WasAmountedByTask: - a task of higher priority has been interrupted => call freertos_task_yield() at end of your interrupt service routine!
 *                 >= tsme_Error: Error condition
 */
e_ttc_semaphore_error freertos_semaphore_take_isr( t_driver_semaphore* Semaphore, t_base Amount );

/** Checks if given Semaphore can provide given amount
 * @param Semaphore    handle of already created semaphore
 * @return         == TRUE: given Semaphore is currently in Amounted state; FALSE otherwise
 */
// BOOL freertos_semaphore_can_provide(t_driver_semaphore* Semaphore, t_base Amount);
#define freertos_semaphore_can_provide(Semaphore, Amount) ( (Semaphore) ? (Semaphore)->uxMessagesWaiting >= Amount : FALSE )

/** Gives amount of tokens back to semaphore
 * @param Semaphore  handle of already created smart semaphore
 * @return         == tsme_OK: Amount has been given back successfully
 */
e_ttc_semaphore_error freertos_semaphore_give( t_driver_semaphore* Semaphore, t_base Amount );

/** Gives amount of tokens back to semaphore (
 * @param Semaphore  handle of already created smart semaphore
 * @return         == tsme_OK: Amount has been given back successfully
 */
e_ttc_semaphore_error freertos_semaphore_give_isr( t_driver_semaphore* Semaphore, t_base Amount );

//}Function prototypes

#endif
