/** { freertos_task.c ***********************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Low-level multitasking interface using FreeRTOS scheduler.
 *
}*/
#include "freertos_task.h"
#include "../ttc_sysclock.h"
#include "../../additionals/300_scheduler_freertos/Source/include/StackMacros.h"

// provided by tasks.c
extern volatile unsigned portBASE_TYPE uxTopReadyPriority;

// workaround for OpenOCD issue (->http://sourceforge.net/p/openocd/mailman/message/33852210/)
volatile const unsigned portBASE_TYPE uxTopUsedPriority = configMAX_PRIORITIES;


#ifdef EXTENSION_cpu_stm32w1xx
    #  error Multitasking not yet supported on CPU stm32w1xx. Deactivate 300_scheduler*!
#endif

#define taskRECORD_READY_PRIORITY( uxPriority )                                                                     \
    {                                                                                                                   \
        if( ( uxPriority ) > uxTopReadyPriority )                                                                       \
        {                                                                                                               \
            uxTopReadyPriority = ( uxPriority );                                                                        \
        }                                                                                                               \
    } /* taskRECORD_READY_PRIORITY */

// task priority to be used when switching to a certain task (should ensure direct switching to certain task)
#define RESUME_NOW_TASK_PRIORITY (configMAX_PRIORITIES - 1)

// provided by ttc_task.c
extern t_u8 ttc_task_AmountTasks;
extern BOOL ttc_task_SchedulerIsRunning;

void vApplicationMallocFailedHook( void ) {
    /* Called if a call to pvPortMalloc() fails because there is insufficient
       free memory available in the FreeRTOS heap.  pvPortMalloc() is called
       internally by FreeRTOS API functions that create tasks, queues, software
       timers, and semaphores.  The size of the FreeRTOS heap is set by the
       configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */
    ttc_assert_halt_origin( ttc_assert_origin_auto );
}
void vApplicationStackOverflowHook( xTaskHandle pxTask, char* pcTaskName ) {
    ( void ) pcTaskName;
    ( void ) pxTask;

    // keep this info to aid debugging
    volatile t_ttc_task_info** TCB_TaskInfoPtr = ( volatile t_ttc_task_info** ) pcTaskName;
    ( void ) TCB_TaskInfoPtr;

    /* Run time stack overflow checking is performed if
       configconfigCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
       function is called if a stack overflow is detected.
       You may want to analyse the TaskInfo of your current task in GDB like this:

       p **TCB_TaskInfoPtr
     */
    ttc_assert_halt_origin( ttc_assert_origin_auto );
}


#if TTC_ASSERT_TASK==1
    /** increased by _freertos_task_critical_begin2(); decreased by freertos_task_critical_end() */
    t_u16 freertos_task_CountCriticalSection = 0;
#endif

void freertos_task_usleep( t_base Delay ) {
    if ( ttc_task_SchedulerIsRunning && ( !_driver_task_is_inside_critical_section() ) ) {

        // compensate for current system clock frequency
        Delay *= ttc_sysclock_get_delay_scale();
        Delay /= TTC_SYSCLOCK_SCALE_UNIT;

        if ( Delay >= _driver_task_min_delay ) { // delay possible for current settings
            Delay /= _driver_task_min_delay;
            while ( Delay > 0xffff ) { // split long delays into smaller ones
                vTaskDelay( 0xffff );
                Delay -= 0xffff;
            }
            if ( Delay )
            { vTaskDelay( Delay ); }
        }
    }
    else {
        ttc_sysclock_udelay( Delay ); // automatically compensated for current system clock frequency
    }
}
BOOL freertos_task_create( void TaskFunction( void* ), t_u16 StackSize, void* Argument, t_u8 Priority, t_ttc_task_info* TaskInfo ) {
#if (configMAX_TASK_NAME_LEN < 4)
#error Value too small to store a pointer + a zero byte!
#endif
#if (configMAX_TASK_NAME_LEN > 4)
#warning Tasknames are managed by ttc_task. Using bigger configMAX_TASK_NAME_LEN than 5 will waste RAM!
#endif

    Assert_TASK_Readable( TaskFunction, ttc_assert_origin_auto );
    Assert_TASK( StackSize > 0, ttc_assert_origin_auto );
    Assert_TASK_Writable( TaskInfo, ttc_assert_origin_auto );

    if ( Priority >= configMAX_PRIORITIES )
    { Priority = configMAX_PRIORITIES - 1; }

    TaskInfo->StackSize_Bytes = StackSize * sizeof( portSTACK_TYPE );

    portENTER_CRITICAL();
    ttc_task_AmountTasks++;
    portEXIT_CRITICAL();

#define IMPLEMENTATION_freertos_task_create
    signed portBASE_TYPE Result = xTaskCreate( ( pdTASK_CODE ) TaskFunction,
                                               NULL,
                                               ( unsigned short ) StackSize,
                                               Argument,
                                               ( t_base ) Priority,
                                               ( void* ) & ( TaskInfo->Handle )
                                             );

    if ( Result > 0 ) {
        volatile tskTCB* TCB = ( tskTCB* ) TaskInfo->Handle;

        // storing task info pointer in task name for fast freertos_task_get_info()
        t_ttc_task_info** TCB_TaskInfoPtr = ( t_ttc_task_info** ) TCB->pcTaskName;
        *TCB_TaskInfoPtr = TaskInfo;

        // storing pointer to this control block for later reference
        TaskInfo->SchedulerControlBlock = ( tskTCB* ) TCB; // cast away volatile
        TaskInfo->StackStart = TCB->pxStack;
#if (portSTACK_GROWTH < 0)
        TaskInfo->StackEnd   = TCB->pxStack - StackSize;
#else
        TaskInfo->StackEnd   = TCB->pxStack + StackSize;
#endif
        return TRUE;
    }

    return FALSE;
}
t_ttc_task_info* freertos_task_get_info( TaskHandle_t Handle ) {
    volatile tskTCB* TCB = NULL;

    if ( Handle )
    { TCB = ( tskTCB* ) Handle; }
    else
    { TCB = pxCurrentTCB; }

    t_ttc_task_info** TCB_TaskInfoPtr = ( t_ttc_task_info** ) TCB->pcTaskName; // set by freertos_task_create()
    Assert_TASK_Writable( TCB_TaskInfoPtr, ttc_assert_origin_auto );
    Assert_TASK_Writable( *TCB_TaskInfoPtr, ttc_assert_origin_auto );  // returned pointer can be read + written safely
    t_ttc_task_info* TaskInfo = ( t_ttc_task_info* ) *TCB_TaskInfoPtr;
    Assert_TASK_Writable( TaskInfo, ttc_assert_origin_auto );  // someone has changed this pointer!

    // perform a backwards check to make sure that we got the correct task info block
    Assert_TASK( TaskInfo->SchedulerControlBlock == TCB, ttc_assert_origin_auto );

    if ( 0 ) { // copy current stack level (only updated during task switch)
        TaskInfo->StackTop = ( t_base* ) TCB->pxTopOfStack;
        if ( TaskInfo->StackEnd < TaskInfo->StackStart ) { // stack grows negative on current architecture
            Assert_TASK( ( TaskInfo->StackEnd <= TaskInfo->StackTop ) && ( TaskInfo->StackTop < TaskInfo->StackStart ), ttc_assert_origin_auto );
        }
        else {                                         // stack grows positive on current architecture
            Assert_TASK( ( TaskInfo->StackEnd >= TaskInfo->StackTop ) && ( TaskInfo->StackTop >= TaskInfo->StackStart ), ttc_assert_origin_auto );
        }
    }

    return TaskInfo;
}
void freertos_task_update_info( TaskHandle_t Handle, t_ttc_task_info* Info ) {
    volatile tskTCB* TCB = NULL;

    if ( Handle )
    { TCB = ( tskTCB* ) Handle; }
    else
    { TCB = pxCurrentTCB; }

    Assert_TASK_Writable( Handle, ttc_assert_origin_auto );
    Assert_TASK_Writable( Info, ttc_assert_origin_auto );

    Info->Handle     = ( TaskHandle_t ) TCB;
    Info->Priority   = TCB->uxBasePriority;
    Info->StackStart = ( t_base* ) TCB->pxStack;
    Info->StackTop   = ( t_base* ) TCB->pxTopOfStack;
#if INCLUDE_uxTaskGetStackHighWaterMark == 1
    Info->StackFree_HighWatermark = uxTaskGetStackHighWaterMark( ( void* ) TCB );
#else
    Info->StackFree_HighWatermark = 0;
#endif
#if (portSTACK_GROWTH < 0)
    Info->StackUsed = ( ( t_base ) Info->StackStart ) + Info->StackSize_Bytes - ( ( t_base ) Info->StackTop );
#else
    Info->StackEnd   = ( t_base* ) TCB->pxEndOfStack;
    Info->StackUsed = ( ( t_base ) Info->StackTop ) + Info->StackSize_Bytes - ( ( t_base ) Info->StackEnd );
    //D Info->StackUsed = Info->StackSize_Bytes - ( ((t_base) Info->StackStart) - ((t_base) Info->StackTop) );
#endif

    Info->StackUsed /= sizeof( portSTACK_TYPE );
}
t_ttc_task_info** _freertos_collect_task_infos( t_ttc_task_info**  TaskInfoWriter, xList* pxList, signed char cStatus ) {
    volatile tskTCB* pxNextTCB, *pxFirstTCB;

    listGET_OWNER_OF_NEXT_ENTRY( pxFirstTCB, pxList );
    do {
        listGET_OWNER_OF_NEXT_ENTRY( pxNextTCB, pxList );
        if ( pxNextTCB != pxFirstTCB ) {
            t_ttc_task_info* TaskInfo = freertos_task_get_info( ( tskTCB* ) pxNextTCB );
            if ( TaskInfo ) {
                *TaskInfoWriter++ = TaskInfo;
                //X _freertos_task_update_info( (TaskHandle_t*) pxNextTCB, TaskInfoWriter++);
                freertos_task_update_info( ( TaskHandle_t ) pxNextTCB, TaskInfo++ );
            }
        }
    }
    while ( pxNextTCB != pxFirstTCB );

    // return pointer to next free info struct
    return TaskInfoWriter;
}
void freertos_task_get_all( t_ttc_task_all* TaskInfos ) {
    Assert_TASK_Writable( TaskInfos, ttc_assert_origin_auto );
    t_ttc_task_info** TaskInfoWriter = &( TaskInfos->All[0] );
    TaskInfos->AmountAll              = 0;
    TaskInfos->AmountRunning          = 0;
    TaskInfos->AmountDelayed          = 0;
    TaskInfos->AmountOverflowDelayed  = 0;
    TaskInfos->AmountWaitTermination  = 0;
    TaskInfos->AmountSuspended        = 0;

    _driver_task_critical_begin();

    unsigned portBASE_TYPE uxQueue;

    uxQueue = configMAX_PRIORITIES + ( unsigned portBASE_TYPE ) 1U;
    TaskInfos->Running = TaskInfos->All;
    do {                                                     // lists of all task priorities
        uxQueue--;

        if ( listLIST_IS_EMPTY( &( pxReadyTasksLists[ uxQueue ] ) ) == pdFALSE ) {
            TaskInfoWriter = _freertos_collect_task_infos( TaskInfoWriter, ( xList* ) & ( pxReadyTasksLists[ uxQueue ] ), tskREADY_CHAR );
        }
    }
    while ( uxQueue > ( unsigned short ) tskIDLE_PRIORITY );
    TaskInfos->AmountRunning = TaskInfoWriter - TaskInfos->Running;
    if ( !TaskInfos->AmountRunning )
    { TaskInfos->Running = NULL; }

    TaskInfos->Delayed = TaskInfoWriter;
    if ( listLIST_IS_EMPTY( pxDelayedTaskList ) == pdFALSE ) { // list of delayed tasks
        TaskInfoWriter = _freertos_collect_task_infos( TaskInfoWriter, ( xList* ) pxDelayedTaskList, tskBLOCKED_CHAR );
    }
    TaskInfos->AmountDelayed = TaskInfoWriter - TaskInfos->Delayed;
    if ( !TaskInfos->AmountDelayed )
    { TaskInfos->Delayed = NULL; }

    TaskInfos->OverflowDelayed = TaskInfoWriter;
    if ( listLIST_IS_EMPTY( pxOverflowDelayedTaskList ) == pdFALSE ) {
        TaskInfoWriter = _freertos_collect_task_infos( TaskInfoWriter, ( xList* ) pxOverflowDelayedTaskList, tskBLOCKED_CHAR );
    }
    TaskInfos->AmountOverflowDelayed = TaskInfoWriter - TaskInfos->OverflowDelayed;
    if ( !TaskInfos->AmountOverflowDelayed )
    { TaskInfos->OverflowDelayed = NULL; }

    TaskInfos->WaitTermination = TaskInfoWriter;
#if( INCLUDE_vTaskDelete == 1 )
    {
        if ( listLIST_IS_EMPTY( &xTasksWaitingTermination ) == pdFALSE ) {
            TaskInfoWriter = _freertos_collect_task_infos( TaskInfoWriter, &xTasksWaitingTermination, tskDELETED_CHAR );
        }
    }
#endif
    TaskInfos->AmountWaitTermination = TaskInfoWriter - TaskInfos->WaitTermination;
    if ( !TaskInfos->AmountWaitTermination )
    { TaskInfos->WaitTermination = NULL; }

    TaskInfos->Suspended = TaskInfoWriter;
#if ( INCLUDE_vTaskSuspend == 1 )
    {
        if ( listLIST_IS_EMPTY( &xSuspendedTaskList ) == pdFALSE ) {
            TaskInfoWriter = _freertos_collect_task_infos( TaskInfoWriter, &xSuspendedTaskList, tskSUSPENDED_CHAR );
        }
    }
#endif
    TaskInfos->AmountSuspended = TaskInfoWriter - TaskInfos->Suspended;
    if ( !TaskInfos->AmountSuspended )
    { TaskInfos->Suspended = NULL; }

    _driver_task_critical_end();
    TaskInfos->AmountAll = TaskInfoWriter - &( TaskInfos->All[0] );
}

#ifdef REGRESSION_PIN_QUEUE_PUSH
    extern void regression_queue_setPin_QueuePushActivity( BOOL Set ); //D
    extern void regression_queue_setPin_QueuePushISR( BOOL Set ); //D
    extern void regression_queue_mark_test( t_u16 Time );
#endif

void freertos_task_resume_now( TaskHandle_t Handle ) {
    Assert_TASK( ttc_task_is_inside_critical_section(), ttc_assert_origin_auto ); // must be called inside critical section!

    unsigned portBASE_TYPE  Priority = Handle->uxPriority;
    taskRECORD_READY_PRIORITY( Priority );
    vListInsert( &( pxReadyTasksLists[Priority] ), &( Handle->xGenericListItem ) );

#ifdef REGRESSION_PIN_QUEUE_PUSH
    regression_queue_setPin_QueuePushISR( 1 ); //D
#endif

    // request task switch
    portNVIC_INT_CTRL_REG = portNVIC_PENDSVSET_BIT; // portYIELD_WITHIN_API();

    // disable all critical sections right now
    t_u8 PreviousCriticalSections;
    freertos_task_critical_all_end( &PreviousCriticalSections );

#ifdef EXTENSION_cpu_cortexm3
    // activate memory barriers
    __asm volatile( "dsb" );
    __asm volatile( "isb" );
#endif

    // back from task switch: restore previous critical section
    freertos_task_critical_all_restore( PreviousCriticalSections );
#ifdef REGRESSION_PIN_QUEUE_PUSH
    regression_queue_setPin_QueuePushISR( 0 ); //D
#endif
}
void freertos_task_resume_now_isr( TaskHandle_t Handle ) {
    //X unsigned portBASE_TYPE  Priority = Handle->uxPriority;
    //X taskRECORD_READY_PRIORITY(Priority);
    //X vListInsert( &( pxReadyTasksLists[Priority] ), &(Handle->xGenericListItem) );

    // Resume the suspended task.
    t_base xYieldRequired = xTaskResumeFromISR( Handle );

    if ( xYieldRequired == pdTRUE ) {
        // We should switch context so the ISR returns to a different task.
        // NOTE:  How this is done depends on the port you are using.  Check
        // the documentation and examples for your port.
        portYIELD_FROM_ISR( 1 );
    }
}
void freertos_task_critical_all_end( t_u8* StoreCounter ) {
    *StoreCounter = uxCriticalNesting;
    uxCriticalNesting = 1;
    ttc_task_critical_end();
}
void freertos_task_critical_all_restore( t_u8 StoreCounter ) {
    ttc_task_critical_begin();
    uxCriticalNesting = StoreCounter;
}
