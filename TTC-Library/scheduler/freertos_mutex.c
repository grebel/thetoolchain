/*{ freertos_mutex.c ***********************************************
 *
 * Written by Gregor Rebel 2012-2013
 *
 * Architecture independent support for inter task communication & synchronization by use of
 * multitasking scheduler FreeRTOS.
 *
}*/
#include "freertos_mutex.h"
#include "../ttc_systick.h"

// constants copied from queue.c

/* Constants used with the cRxLock and xTxLock structure members. */
#define queueUNLOCKED                   ( ( signed portBASE_TYPE ) -1 )
#define queueLOCKED_UNMODIFIED          ( ( signed portBASE_TYPE ) 0 )

#define queueERRONEOUS_UNBLOCK          ( -1 )

/* For internal use only. */
#ifndef queueSEND_TO_BACK
#define queueSEND_TO_BACK               ( 0 )
#endif
#ifndef queueSEND_TO_FRONT
#define queueSEND_TO_FRONT              ( 1 )
#endif

/* Effectively make a union out of the xQUEUE structure. */
#define pxMutexHolder                   pcTail
#define uxQueueType                     pcHead
#define uxRecursiveCallCount            pcReadFrom
#define queueQUEUE_IS_MUTEX             NULL

/* Semaphores do not actually store or copy data, so have an items size of
zero. */
#define queueSEMAPHORE_QUEUE_ITEM_LENGTH ( ( unsigned portBASE_TYPE ) 0 )
#define queueDONT_BLOCK                  ( ( portTickType ) 0U )
#define queueMUTEX_GIVE_BLOCK_TIME       ( ( portTickType ) 0U )

/* These definitions *must* match those in queue.h. */
#ifndef queueQUEUE_TYPE_BASE
#define queueQUEUE_TYPE_BASE                ( 0U )
#endif
#ifndef queueQUEUE_TYPE_MUTEX
#define queueQUEUE_TYPE_MUTEX               ( 1U )
#endif
#ifndef queueQUEUE_TYPE_COUNTING_SEMAPHORE
#define queueQUEUE_TYPE_COUNTING_SEMAPHORE  ( 2U )
#endif
#ifndef queueQUEUE_TYPE_BINARY_SEMAPHORE
#define queueQUEUE_TYPE_BINARY_SEMAPHORE    ( 3U )
#endif
#ifndef queueQUEUE_TYPE_RECURSIVE_MUTEX
#define queueQUEUE_TYPE_RECURSIVE_MUTEX     ( 4U )
#endif

//{ functions *************************************************

xQUEUE_Structure* freertos_mutex_create() {

    return xSemaphoreCreateMutex();
}
e_ttc_mutex_error freertos_mutex_lock( volatile xQUEUE_Structure* Mutex, t_base TimeOut ) {
    xQUEUE_Structure* Mutex_ = ( xQUEUE_Structure* ) Mutex; // cast away volatile
    Assert_MUTEX_Writable( Mutex_, ttc_assert_origin_auto );

    if ( TimeOut != -1 ) {
        portTickType Ticks2Wait = ( portTickType ) TimeOut / ( 1000000 / configTICK_RATE_HZ );

        if ( xSemaphoreTake( Mutex_, Ticks2Wait ) ) {
            return tme_OK;
        }
        else {
            return tme_TimeOut;
        }
    }
    else { // wait forever
        if ( 0 )
        { while ( xSemaphoreTake( Mutex_, ( portTickType ) - 1 ) == 0 ); }
        else { // easier to debug: will call ttc_task_yield() periodically while waiting
            t_base StartTime = ttc_systick_get_elapsed_usecs();
            while ( xSemaphoreTake( Mutex_, ( portTickType ) 1000 ) == 0 ) {
                _ttc_mutex_wait();
                Assert_MUTEX( ttc_systick_get_elapsed_usecs() - StartTime < 10000000, ttc_assert_origin_auto ); // waiting >10 seconds (probably a bug?)
            }
        }
        return tme_OK;
    }

    return tme_NotImplemented;
}
e_ttc_mutex_error freertos_mutex_lock_isr( volatile xQUEUE_Structure* Mutex ) {
    xQUEUE_Structure* Mutex_ = ( xQUEUE_Structure* ) Mutex; // cast away volatile
    Assert_MUTEX_Writable( Mutex_, ttc_assert_origin_auto );
    static signed portBASE_TYPE HigherPrioTaskWoken; HigherPrioTaskWoken = FALSE;

    #ifdef EXTENSION_300_scheduler_freertos
    signed portBASE_TYPE Return;

    Return = xSemaphoreTakeFromISR( Mutex_, &HigherPrioTaskWoken );

    if ( Return ) {
        if ( HigherPrioTaskWoken )
        { return tme_WasLockedByTask; }
        else
        { return tme_OK; }
    }
    else
    { return tme_MutexAlreadyLocked; }
    #endif

    return tme_NotImplemented;
}
e_ttc_mutex_error freertos_mutex_unlock( volatile xQUEUE_Structure* Mutex ) {
    xQUEUE_Structure* Mutex_ = ( xQUEUE_Structure* ) Mutex; // cast away volatile
    Assert_MUTEX_Writable( Mutex_, ttc_assert_origin_auto );

    #ifdef EXTENSION_300_scheduler_freertos
    if ( xSemaphoreGive( Mutex_ ) )
    { return tme_OK; }
    else
    { return tme_UNKNOWN; }
    #endif

    return tme_NotImplemented;
}
e_ttc_mutex_error freertos_mutex_init( volatile xQUEUE_Structure* Mutex ) {
    xQUEUE_Structure* Mutex_ = ( xQUEUE_Structure* ) Mutex; // cast away volatile

    Assert_MUTEX( Mutex_ != 0, ttc_assert_origin_auto );

    /* Information required for priority inheritance. */
    Mutex_->pxMutexHolder = NULL;
    Mutex_->uxQueueType = queueQUEUE_IS_MUTEX;

    /* Queues used as a mutex no data is actually copied into or out
    of the queue. */
    Mutex_->pcWriteTo = NULL;
    Mutex_->pcReadFrom = NULL;

    /* Each mutex has a length of 1 (like a binary semaphore) and
    an item size of 0 as nothing is actually copied into or out
    of the mutex. */
    Mutex_->uxMessagesWaiting = ( unsigned portBASE_TYPE ) 0U;
    Mutex_->uxLength = ( unsigned portBASE_TYPE ) 1U;
    Mutex_->uxItemSize = ( unsigned portBASE_TYPE ) 0U;
    Mutex_->xRxLock = queueUNLOCKED;
    Mutex_->xTxLock = queueUNLOCKED;

    #if ( configUSE_TRACE_FACILITY == 1 )
    Mutex->ucQueueType = queueQUEUE_TYPE_MUTEX;
    #endif

    /* Ensure the event queues start with the correct state. */
    vListInitialise( &( Mutex_->xTasksWaitingToSend ) );
    vListInitialise( &( Mutex_->xTasksWaitingToReceive ) );

    traceCREATE_MUTEX( Mutex_ );

    /* Start with the semaphore in the expected state. */
    xQueueGenericSend( Mutex_, NULL, ( portTickType ) 0U, queueSEND_TO_BACK );

    return tme_OK;
}
e_ttc_mutex_error freertos_mutex_unlock_isr( xQUEUE_Structure* Mutex ) {
    Assert_MUTEX_Writable( Mutex, ttc_assert_origin_auto );

    #ifdef EXTENSION_300_scheduler_freertos
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    xSemaphoreGiveFromISR( Mutex, &xHigherPriorityTaskWoken );

    if ( xHigherPriorityTaskWoken == pdFALSE )
    { return tme_OK; }
    else
    { return tme_WasLockedByTask; }
    #endif

    return tme_NotImplemented;
}

//}functions
