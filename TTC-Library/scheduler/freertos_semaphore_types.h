#ifndef freertos_semaphore_types_H
#define freertos_semaphore_types_H

/*{ freertos_semaphore.h *************************************************
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Low-level driver for Mutual Exclusive Amounts (Semaphores) using implementation provided by FreeRTOS.
 *
}*/

//{ includes

#include "../ttc_basic.h"
#include "freertos_queue_types.h"

//}includes

// datatype used to store individual semaphore
#define t_driver_semaphore xQUEUE_Structure
// #define _driver_t_ttc_semaphore xQUEUE_Structure

//}

#endif
