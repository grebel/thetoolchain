#ifndef BASIC_STM32L1XX_H
#define BASIC_STM32L1XX_H

/** { basic_stm32l1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for basic devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by high-level basic and application.
 *
 *  Created from template device_architecture.h revision 22 at 20140614 21:30:51 UTC
 *
 *  Note: See ttc_basic.h for description of stm32l1xx independent BASIC implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_BASIC_STM32L1XX
//
// Implementation status of low-level driver support for basic devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_BASIC_STM32L1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_BASIC_STM32L1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_BASIC_STM32L1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_BASIC_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "basic_stm32l1xx.c"
//
#include "basic_stm32l1xx_types.h"
#include "stm32l1xx.h"
#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_basic_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_basic_foo
//

#include "basic_cm3.h" // will implement most of our driver functions
#define ttc_driver_basic_prepare()                               basic_stm32l1xx_prepare()

//#define ttc_driver_basic_16_atomic_add basic_stm32l1xx_16_atomic_add
//#define ttc_driver_basic_32_atomic_add basic_stm32l1xx_32_atomic_add
//#define ttc_driver_basic_08_atomic_add basic_stm32l1xx_08_atomic_add
//#define ttc_driver_basic_16_atomic_read_write basic_stm32l1xx_16_atomic_read_write
//#define ttc_driver_basic_32_atomic_read_write basic_stm32l1xx_32_atomic_read_write
//#define ttc_driver_basic_08_atomic_read_write basic_stm32l1xx_08_atomic_read_write
//#undef ttc_driver_basic_prepare
//#undef ttc_driver_basic_16_swap_endian
//#undef ttc_driver_basic_32_swap_endian
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)


//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_basic.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_basic.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _basic_stm32l1xx_foo(t_ttc_basic_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //BASIC_STM32L1XX_H
