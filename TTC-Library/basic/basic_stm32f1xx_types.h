#ifndef BASIC_STM32F1XX_TYPES_H
#define BASIC_STM32F1XX_TYPES_H

/** { basic_stm32f1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_basic.h providing
 *  Structures, Enums and Defines being required by ttc_basic_types.h
 *
 *  basic definitions and system start on stm32f1xx
 *
 *  Created from template device_architecture_types.h revision 24 at 20161104 03:02:08 UTC
 *
 *  Note: See ttc_basic.h for description of high-level basic implementation.
 *  Note: See basic_stm32f1xx.h for description of stm32f1xx basic implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_BASIC1   // device not defined in makefile
    #define TTC_BASIC1    ta_basic_stm32f1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_basic_types.h *************************

typedef struct { // register description (adapt according to stm32f1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_basic_register;

typedef struct {  // stm32f1xx specific configuration data of single basic device
} __attribute__( ( __packed__ ) ) t_basic_stm32f1xx_config;

// t_ttc_basic_architecture is required by ttc_basic_types.h
#define t_ttc_basic_architecture t_basic_stm32f1xx_config

//} Structures/ Enums


#endif //BASIC_STM32F1XX_TYPES_H
