#ifndef BASIC_STM32F1XX_H
#define BASIC_STM32F1XX_H

/** { basic_stm32f1xx.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_basic.h providing
 *  function prototypes being required by ttc_basic.h
 *
 *  basic definitions and system start on stm32f1xx
 *
 *  Created from template device_architecture.h revision 28 at 20161104 03:02:08 UTC
 *
 *  Note: See ttc_basic.h for description of stm32f1xx independent BASIC implementation.
 *
 *  Authors: Gregor Rebel
 *
 *
 *  How to configure stm32f1xx basic devices
 *
 *  PLACE YOUR DESCRIPTION HERE!
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_BASIC_DRIVER_AVAILABLE
#define EXTENSION_BASIC_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_basic_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "basic_stm32f1xx.c"
//
#include "../ttc_basic_types.h" // will include basic_stm32f1xx_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

#include "basic_cm3.h" // will implement most of our driver functions

// define all supported low-level functions for ttc_basic_interface.h
//
// Some driver functions may be optional to implement.
// Check ttc_basic_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_basic_foo
//
//#define ttc_driver_basic_16_atomic_add basic_stm32f1xx_16_atomic_add
//#define ttc_driver_basic_32_atomic_add basic_stm32f1xx_32_atomic_add
//#define ttc_driver_basic_08_atomic_add basic_stm32f1xx_08_atomic_add
//#define ttc_driver_basic_16_atomic_read_write basic_stm32f1xx_16_atomic_read_write
//#define ttc_driver_basic_32_atomic_read_write basic_stm32f1xx_32_atomic_read_write
//#define ttc_driver_basic_08_atomic_read_write basic_stm32f1xx_08_atomic_read_write

// commented undefs required to avoid getting unimplemented functions being inserted again
//#undef ttc_driver_basic_16_atomic_add
//#undef ttc_driver_basic_32_atomic_add
//#undef ttc_driver_basic_08_atomic_add
//#undef ttc_driver_basic_16_atomic_read_write
//#undef ttc_driver_basic_32_atomic_read_write
//#undef ttc_driver_basic_08_atomic_read_write
//#undef ttc_driver_basic_prepare
//#undef ttc_driver_basic_16_swap_endian
//#undef ttc_driver_basic_32_swap_endian
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_basic.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_basic.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
 * @param Amount   =
 */
t_u16 basic_stm32f1xx_16_atomic_add( t_u16* Address, t_s16 Amount );

/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
 * @param Amount   =
 */
t_u32 basic_stm32f1xx_32_atomic_add( t_u32* Address, t_s32 Amount );

/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
 * @param Amount   =
 */
t_u8 basic_stm32f1xx_08_atomic_add( t_u8* Address, t_s8 Amount );

/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u16 basic_stm32f1xx_16_atomic_read_write( t_u16* Address, t_u16 NewValue );

/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u32 basic_stm32f1xx_32_atomic_read_write( t_u32* Address, t_u32 NewValue );

/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u8 basic_stm32f1xx_08_atomic_read_write( t_u8* Address, t_u8 NewValue );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //BASIC_STM32F1XX_H
