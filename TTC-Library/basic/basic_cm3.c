/** { basic_cm3.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for basic devices on cm3 architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20140603 04:04:24 UTC
 *
 *  Note: See ttc_basic.h for description of cm3 independent BASIC implementation.
 *
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "basic_cm3.h".
//
#include "basic_cm3.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

// declared in ttc_basic.h which cannot be included from here
//X extern void ttc_assert_test( BOOL TestOK );
//X extern void Assert_Writable( void* Address );

//}Global Variables
//{ Function Definitions *******************************************************

void  basic_cm3_prepare() {


}
t_u8  basic_cm3_08_atomic_add( t_u8* Address, t_s8 Amount ) {
    Assert_BASIC_Writable( Address, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_BASIC( ( ( ( t_u32 ) Address ) & 0x3 ) == 0, ttc_assert_origin_auto );  // Address not 4-byte aligned!

    volatile t_u8 Value;
    volatile t_u32 Result;
    do {
        // Value = __LDREXB( (t_u32*) Address);
        __asm__ __volatile__( "ldrexb %0, [%1]" : "=r"( Value ) : "r"( Address ) );

        Value += Amount;

        // Result = __STREXWB( Value, (t_u32*) Address)
        __asm__ __volatile__( "strexb %0, %2, [%1]" : "=&r"( Result ) : "r"( Address ), "r"( Value ) );
    }
    while ( Result == 1 );

    return Value;
}
t_u8  basic_cm3_08_atomic_read_write( t_u8* Address, t_u8 NewValue ) {
    Assert_BASIC_Writable( Address, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_BASIC( ( ( ( t_u32 ) Address ) & 0x3 ) == 0, ttc_assert_origin_auto );  // Address not 4-byte aligned!

    volatile t_u8 OldValue;
    volatile t_u32 Result;
    do {
        //OldValue = __LDREXB( (t_u32*) Address);
        __asm__ __volatile__( "ldrexb %0, [%1]" : "=r"( OldValue ) : "r"( Address ) );

        // Result = __STREXWB( NewValue, (t_u32*) Address)
        __asm__ __volatile__( "strexb %0, %2, [%1]" : "=&r"( Result ) : "r"( Address ), "r"( NewValue ) );
    }
    while ( Result != 0 );

    return OldValue;
}
t_u16 basic_cm3_16_atomic_add( t_u16* Address, t_s16 Amount ) {
    Assert_BASIC_Writable( Address, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_BASIC( ( ( ( t_u32 ) Address ) & 0x3 ) == 0, ttc_assert_origin_auto );  // Address not 4-byte aligned!

    volatile t_u16 Value;
    volatile t_u32 Result;
    do {
        // Value = __LDREXH( (t_u32*) Address);
        __asm__ __volatile__( "ldrexh %0, [%1]" : "=r"( Value ) : "r"( Address ) );

        Value += Amount;

        // Result = __STREXH( Value, (t_u32*) Address)
        __asm__ __volatile__( "strexh %0, %2, [%1]" : "=&r"( Result ) : "r"( Address ), "r"( Value ) );
    }
    while ( Result == 1 );

    return Value;
}
t_u16 basic_cm3_16_atomic_read_write( t_u16* Address, t_u16 NewValue ) {
    Assert_BASIC_Writable( Address, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_BASIC( ( ( ( t_u32 ) Address ) & 0x3 ) == 0, ttc_assert_origin_auto );  // Address not 4-byte aligned!

    volatile t_u16 OldValue;
    volatile t_u32 Result;
    do {
        //OldValue = __LDREXH( (t_u32*) Address);
        __asm__ __volatile__( "ldrexh %0, [%1]" : "=r"( OldValue ) : "r"( Address ) );

        // Result = __STREXH( NewValue, (t_u32*) Address)
        __asm__ __volatile__( "strexh %0, %2, [%1]" : "=&r"( Result ) : "r"( Address ), "r"( NewValue ) );
    }
    while ( Result != 0 );

    return OldValue;
}
t_u16 basic_cm3_16_read_big_endian( const t_u8* Buffer ) {

    volatile t_u16 Value = *( ( t_u16* ) Buffer );
    __asm__ __volatile__( "rev16 %0, %0" : "=rw"( Value ) ); // convert big-/ little-endian

    return Value;
}
void  basic_cm3_16_swap_endian( t_u16* Variable ) {
    Assert_BASIC_EXTRA_Writable( Variable, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    volatile t_u16 Value = *Variable;
    __asm__ __volatile__( "rev16 %0, %0" : "=rw"( Value ) ); // convert big-/ little-endian
    *Variable = Value;
}
t_u32 basic_cm3_32_atomic_add( t_u32* Address, t_s32 Amount ) {
    Assert_BASIC_Writable( Address, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_BASIC( ( ( ( t_u32 ) Address ) & 0x3 ) == 0, ttc_assert_origin_auto );  // Address not 4-byte aligned!

    volatile t_u32 Value;
    volatile t_u32 Result;
    do {
        // Value = __LDREXW( (t_u32*) Address);
        __asm__ __volatile__( "ldrex %0, [%1]" : "=r"( Value ) : "r"( Address ) );

        Value += Amount;

        // Result = __STREXW( Value, (t_u32*) Address)
        __asm__ __volatile__( "strex %0, %2, [%1]" : "=&r"( Result ) : "r"( Address ), "r"( Value ) );
    }
    while ( Result == 1 );

    return Value;
}
t_u32 basic_cm3_32_atomic_read_write( t_u32* Address, t_u32 NewValue ) {
    Assert_BASIC_Writable( Address, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_BASIC( ( ( ( t_u32 ) Address ) & 0x3 ) == 0, ttc_assert_origin_auto );  // Address not 4-byte aligned!

    volatile t_u32 OldValue;
    volatile t_u32 Result;
    do {
        //OldValue = __LDREXW( (t_u32*) Address);
        __asm__ __volatile__( "ldrex %0, [%1]" : "=r"( OldValue ) : "r"( Address ) );

        // Result = __STREXW( NewValue, (t_u32*) Address)
        __asm__ __volatile__( "strex %0, %2, [%1]" : "=&r"( Result ) : "r"( Address ), "r"( NewValue ) );
    }
    while ( Result != 0 );

    return OldValue;
}
t_u32 basic_cm3_32_read_big_endian( t_u8* Buffer ) {

    volatile t_u32 Value = *( ( t_u32* ) Buffer );
    __asm__ __volatile__( "rev %0, %0" : "=rw"( Value ) ); // convert big-/ little-endian

    return Value;
}
void  basic_cm3_32_swap_endian( t_u32* Variable ) {
    Assert_BASIC_EXTRA_Writable( Variable, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    volatile t_u32 Value = *Variable;
    __asm__ __volatile__( "rev %0, %0" : "=rw"( Value ) ); // convert big-/ little-endian
    *Variable = Value;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
