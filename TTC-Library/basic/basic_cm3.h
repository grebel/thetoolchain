#ifndef BASIC_CM3_H
#define BASIC_CM3_H

/** { basic_cm3.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for basic devices on cm3 architectures.
 *  Structures, Enums and Defines being required by high-level basic and application.
 *
 *  Created from template device_architecture.h revision 22 at 20140603 04:04:24 UTC
 *
 *  Note: See ttc_basic.h for description of cm3 independent BASIC implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_BASIC_CM3
//
// Implementation status of low-level driver support for basic devices on cm3
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_BASIC_CM3 '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_BASIC_CM3 == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_BASIC_CM3 to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_BASIC_CM3

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "basic_cm3.c"
//
#include "basic_cm3_types.h"
#include "../ttc_basic_types.h"

#ifdef EXTENSION_cpu_stm32w1xx
#  ifdef EXTENSION_255_stm32w1xx_standard_peripherals
#    include "stm32w108xx.h"   // must be included BEFORE core_cm3.h
#  else

// required by core_cm3.h
typedef enum IRQn {
    /******  Cortex-M3 Processor Exceptions Numbers ****************************************************************/
    NonMaskableInt_IRQn         = -14,    /*!< 2 Non Maskable Interrupt                                          */
    MemoryManagement_IRQn       = -12,    /*!< 4 Cortex-M3 Memory Management Interrupt                           */
    BusFault_IRQn               = -11,    /*!< 5 Cortex-M3 Bus Fault Interrupt                                   */
    UsageFault_IRQn             = -10,    /*!< 6 Cortex-M3 Usage Fault Interrupt                                 */
    SVCall_IRQn                 = -5,     /*!< 11 Cortex-M3 SV Call Interrupt                                    */
    DebugMonitor_IRQn           = -4,     /*!< 12 Cortex-M3 Debug Monitor Interrupt                              */
    PendSV_IRQn                 = -2,     /*!< 14 Cortex-M3 Pend SV Interrupt                                    */
    SysTick_IRQn                = -1,     /*!< 15 Cortex-M3 System Tick Interrupt                                */
    /******  STM32W108xx specific Interrupt Numbers ****************************************************************/
    TIM1_IRQn                   = 0,      /*!< Timer 1 Interrupt                                                 */
    TIM2_IRQn                   = 1,      /*!< Timer 1 Interrupt                                                 */
    MNG_IRQn                    = 2,      /*!< Management Peripheral Interrupt                                   */
    BASEBAND_IRQn               = 3,      /*!< Base Band Interrupt                                               */
    SLPTIM_IRQn                 = 4,      /*!< Sleep Timer Interrupt                                             */
    SC1_IRQn                    = 5,      /*!< Serial Controller 1 Interrupt                                     */
    SC2_IRQn                    = 6,      /*!< Serial Controller 2 Interrupt                                     */
    SECURITY_IRQn               = 7,      /*!< Security Interrupt                                                */
    MAC_TIM_IRQn                = 8,      /*!< MAC Timer Interrupt                                               */
    MAC_TR_IRQn                 = 9,      /*!< MAC Transmit Interrupt                                            */
    MAC_RE_IRQn                 = 10,     /*!< MAC Receive Interrupt                                             */
    ADC_IRQn                    = 11,     /*!< ADC Interrupt                                                     */
    EXTIA_IRQn                  = 12,     /*!< EXTI port A interrupt                                             */
    EXTIB_IRQn                  = 13,     /*!< EXTI port B interrupt                                             */
    EXTIC_IRQn                  = 14,     /*!< EXTI port C interrupt                                             */
    EXTID_IRQn                  = 15,     /*!< EXTI port D interrupt                                             */
    DEBUG_IRQn                  = 16      /*!< Debug Interrupt                                                   */
} IRQn_Type;

#  endif
#endif
#ifdef EXTENSION_cpu_stm32f1xx
    #include "stm32f10x.h" // must be included before core_cm3.h
#endif
#ifdef EXTENSION_cpu_stm32l1xx
    #include "stm32l1xx.h" // must be included before core_cm3.h
#endif
#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_basic_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_basic_foo
//
//#define ttc_driver_basic_prepare
#define ttc_driver_basic_32_atomic_add(Address, Amount)          basic_cm3_32_atomic_add(Address, Amount)
#define ttc_driver_basic_16_atomic_add(Address, Amount)          basic_cm3_16_atomic_add(Address, Amount)
#define ttc_driver_basic_08_atomic_add(Address, Amount)          basic_cm3_08_atomic_add(Address, Amount)
#define ttc_driver_basic_32_atomic_read_write(Address, NewValue) basic_cm3_32_atomic_read_write(Address, NewValue)
#define ttc_driver_basic_16_atomic_read_write(Address, NewValue) basic_cm3_16_atomic_read_write(Address, NewValue)
#define ttc_driver_basic_08_atomic_read_write(Address, NewValue) basic_cm3_08_atomic_read_write(Address, NewValue)
#define ttc_driver_basic_16_write_little_endian(Buffer, Data)    basic_cm3_16_write_little_endian(Buffer, Data)
#define ttc_driver_basic_16_write_big_endian(Buffer, Data)       basic_cm3_16_write_big_endian(Buffer, Data)
#define ttc_driver_basic_32_write_little_endian(Buffer, Data)    basic_cm3_32_write_little_endian(Buffer, Data)
#define ttc_driver_basic_32_write_big_endian(Buffer, Data)       basic_cm3_32_write_big_endian(Buffer, Data)
#define ttc_driver_basic_16_read_big_endian(Buffer)              basic_cm3_16_read_big_endian(Buffer)
#define ttc_driver_basic_16_read_little_endian(Buffer)           basic_cm3_16_read_little_endian(Buffer)
#define ttc_driver_basic_32_read_big_endian(Buffer)              basic_cm3_32_read_big_endian(Buffer)
#define ttc_driver_basic_32_read_little_endian(Buffer)           basic_cm3_32_read_little_endian(Buffer)
#define ttc_driver_basic_16_swap_endian                          basic_cm3_16_swap_endian
#define ttc_driver_basic_32_swap_endian                          basic_cm3_32_swap_endian
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_basic.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_basic.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Prepares Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void basic_cm3_prepare();

/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u32 basic_cm3_32_atomic_add( t_u32* Address, t_s32 Amount );

/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u16 basic_cm3_16_atomic_add( t_u16* Address, t_s16 Amount );

/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u8 basic_cm3_08_atomic_add( t_u8* Address, t_s8 Amount );

/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u32 basic_cm3_32_atomic_read_write( t_u32* Address, t_u32 NewValue );

/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u16 basic_cm3_16_atomic_read_write( t_u16* Address, t_u16 NewValue );

/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u8 basic_cm3_08_atomic_read_write( t_u8* Address, t_u8 NewValue );

/** Calculate bit-banding address that corresponds to given bit in given 32-bit word
 *
 * @param   PeripheralWord   address of 32-bit word in which desired bit is located
 * @param   BitNumber        0..31
 * @return  address in peripheral bit-banding region that corresponds to given word and bit-number
 */
// volatile t_u32* cm3_calc_peripheral_BitBandAddress(void* PeripheralWord, t_u8 BitNumber);
#define cm3_calc_peripheral_BitBandAddress(PeripheralWord, BitNumber) ( (t_u32*) (0x42000000 + 32 * (((t_u32) PeripheralWord)  - 0x40000000) + BitNumber * 4) )

/** Calculate peripheral word that corresponds to given bit-banding address
   *
   * This function is a reverse of cm3_calc_peripheral_BitBandAddress()
   *
   * @param   BitBandAddress address in peripheral bit-banding region that corresponds to given word and bit-number
   * @return  address of peripheral 32-bit word
   */
// volatile t_u32* cm3_calc_peripheral_Word(void* BitBandAddress);
#define cm3_calc_peripheral_Word(BitBandAddress) (    (   (  ( ((t_u32) BitBandAddress) & 0xffffff80 ) - 0x42000000  ) / 32   ) + 0x40000000    )

/** Atomic single bit write operation for peripheral register.
 *
 * The classical read-modify-write approach to set a bit in a peripheral register goes as follows:
 * 1) Temp = *Register;
 * 2) if (BitValue) Temp |= 1 << BitNumber;
 *    else          Temp &= ~(1 << BitNumber);
 * 3) *Register = Temp;
 *
 * This classical approach has two main disadvantages
 * 1) Its somewhat slow
 * 2) Its not atomic (someone else could read the register while we're modifying Temp)
 *
 * Using the CortexM3 bitbanding feature is faster than read-modify-write and it's atomic:
 * cm3_set_peripheral_bit(PeripheralWord, BitNumber, BitValue);
 *
 * @param   PeripheralWord   address of 32-bit word in which desired bit is located
 * @param   BitNumber        0..31
 * @param   BitValue         0..1 value to write
 * @return  Address in peripheral bit-banding region that corresponds to given word and bit-number
 */
void cm3_set_peripheral_bit( void* PeripheralWord, t_u8 BitNumber, t_u8 BitValue );
#define cm3_set_peripheral_bit(PeripheralWord, BitNumber, BitValue)     *( (t_u8*) (0x42000000 + 32 * (((t_u32) PeripheralWord)  - 0x40000000) + BitNumber * 4) ) = BitValue;

/** Single bit read operation for peripheral register.
 *
 * The classical bit read approach to set a bit in a peripheral register goes as follows:
 * 1) Temp = *Register;
 * 2) Temp &= Mask;
 * 3) t_u8 BitValue = (Temp) ? 1 : 0;
 *
 * Using the CortexM3 bitbanding feature allows a faster bit read operation:
 * t_u8 BitValue = cm3_get_peripheral_bit(PeripheralWord, BitNumber);
 *
 * @param   PeripheralWord   address of 32-bit word in which desired bit is located
 * @param   BitNumber        0..31
 * @param   BitValue         0..1 bit value to set
 * @return  Address in peripheral bit-banding region that corresponds to given word and bit-number
 */
t_u8 cm3_get_peripheral_bit( void* PeripheralWord, t_u8 BitNumber );
#define cm3_get_peripheral_bit(PeripheralWord, BitNumber)     *( (t_u8*) (0x42000000 + 32 * (((t_u32) PeripheralWord)  - 0x40000000) + BitNumber * 4) )

/** Calculate bit number that corresponds to given bit-banding address
   *
   * This function is a reverse of cm3_calc_peripheral_BitBandAddress()
   *
   * @param   BitBandAddress address in peripheral bit-banding region that corresponds to given word and bit-number
   * @return  BitNumber     0..31
   */
// volatile t_u32* cm3_calc_peripheral_Word(void* BitBandAddress);
#define cm3_calc_peripheral_BitNumber(BitBandAddress) (  ( ((t_u32) BitBandAddress) & 0x7f ) / 4  ) // CortexM3 is little endian by default

/** Write given 16 bit data in little endian order into given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * Little Endian stores byte of lowest value first.
 * E.g.
 * t_u8 Buffer[2];
 * ttc_basic_16_write_little_endian(Buffer, 0x1234);
 *
 * // Equals to
 * Buffer[0] = 0x34;
 * Buffer[1] = 0x12;
 *
 * @param Buffer
 * @param Data
 */
//void basic_cm3_16_write_little_endian(t_u8* Buffer, t_u16 Data);
#define basic_cm3_16_write_little_endian(Buffer, Data)  (*((t_u16*) Buffer) =  Data) // CortexM3 is little endian by default

/** Write given 16 bit data in big endian order into given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * bBg Endian stores byte of highest value first.
 * E.g.
 * t_u8 Buffer[2];
 * ttc_basic_16_write_big_endian(Buffer, 0x1234);
 *
 * // Equals to
 * Buffer[0] = 0x12;
 * Buffer[1] = 0x34;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     16 bit value to copy into Buffer[]
 */
//void basic_cm3_16_write_big_endian(t_u8* Buffer, t_u16 Data);
//#define basic_cm3_16_write_big_endian(Buffer, Data) __asm__ __volatile__  ("rev16 %0, %0; str " : "=rw" (Data) ); (*((t_u16*) Buffer) =  Data)
#define basic_cm3_16_write_big_endian(Buffer, Data) __asm__ __volatile__  ("rev16 %0, %0; strh %0, [%1]; rev16 %0, %0" : : "h" (Data), "h" (Buffer) )

/** Write given 32 bit data in little endian order into given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * Little Endian stores byte of lowest value first.
 * E.g.
 * t_u8 Buffer[4];
 * ttc_basic_32_write_little_endian(Buffer, 0x12345678);
 *
 * // Equals to
 * Buffer[0] = 0x78;
 * Buffer[1] = 0x56;
 * Buffer[2] = 0x34;
 * Buffer[3] = 0x12;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     32 bit value to copy into Buffer[]
 */
//void basic_cm3_32_write_little_endian(t_u8* Buffer, t_u32 Data);
#define basic_cm3_32_write_little_endian(Buffer, Data)  (*((t_u32*) Buffer) =  Data) // CortexM3 is little endian by default

/** Write given 32 bit data in big endian order into given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * big Endian stores byte of highest value first.
 * E.g.
 * t_u8 Buffer[4];
 * ttc_basic_32_write_big_endian(Buffer, 0x12345678);
 *
 * // Equals to
 * Buffer[0] = 0x12;
 * Buffer[1] = 0x34;
 * Buffer[2] = 0x56;
 * Buffer[3] = 0x78;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     32 bit value to copy into Buffer[]
 */
void basic_cm3_32_write_big_endian( t_u8* Buffer, t_u32 Data );
#define basic_cm3_32_write_big_endian(Buffer, Data) __asm__ __volatile__  ("rev %0, %0; str %0, [%1]; rev %0, %0" : : "h" (Data), "h" (Buffer) )

/** Read 16 bit data in big endian order from given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * Big Endian stores byte of highest value first.
 * E.g.
 * t_u8 Buffer[2];
 * Buffer[0] = 0x12;
 * Buffer[1] = 0x34;
 *
 * t_u16 Value = ttc_basic_16_read_big_endian(Buffer);
 * // Equals to
 * t_u16 Value = 0x3412;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     16 bit value to copy into Buffer[]
 * @return   =
 */
t_u16 basic_cm3_16_read_big_endian( const t_u8* Buffer );

/** Read 16 bit data in little endian order from given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * Little Endian stores byte of highest value first.
 * E.g.
 * t_u8 Buffer[2];
 * Buffer[0] = 0x12;
 * Buffer[1] = 0x34;
 *
 * t_u16 Value = ttc_basic_16_read_little_endian(Buffer);
 * // Equals to
 * t_u16 Value = 0x1234;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     16 bit value to copy into Buffer[]
 * @return   =
 */
t_u16 basic_cm3_16_read_little_endian( t_u8* Buffer );
#define basic_cm3_16_read_little_endian(Buffer) *((t_u16*) Buffer)  // CortexM3 is little endian by default

/** Read 32 bit data in big endian order from given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * Big Endian stores byte of highest value first.
 * E.g.
 * t_u8 Buffer[4];
 * Buffer[0] = 0x12;
 * Buffer[1] = 0x34;
 * Buffer[2] = 0x56;
 * Buffer[3] = 0x78;
 *
 * t_u32 Value = ttc_basic_32_read_big_endian(Buffer);
 * // Equals to
 * t_u32 Value = 0x78563412;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     32 bit value to copy into Buffer[]
 * @return   =
 */
t_u32 basic_cm3_32_read_big_endian( t_u8* Buffer );

/** Read 32 bit data in little endian order from given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * Little Endian stores byte of highest value first.
 * E.g.
 * t_u8 Buffer[4];
 * Buffer[0] = 0x12;
 * Buffer[1] = 0x34;
 * Buffer[2] = 0x56;
 * Buffer[3] = 0x78;
 *
 * t_u32 Value = ttc_basic_32_read_little_endian(Buffer);
 * // Equals to
 * t_u32 Value = 0x12345678;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     32 bit value to copy into Buffer[]
 * @return   =
 */
t_u32 basic_cm3_32_read_little_endian( t_u8* Buffer );
#define basic_cm3_32_read_little_endian(Buffer) *((t_u32*) Buffer)  // CortexM3 is little endian by default

/** Swaps bytes in referenced value to switch big<->little endian
 *
 * Note: Calling this function twice on same value will restore original content.
 * Note: This function is not thread safe!
 *
 * @param Variable (t_u16*)  address of value to convert
 */
void basic_cm3_16_swap_endian( t_u16* Variable );

/** Swaps bytes in referenced value to switch big<->little endian
 *
 * Note: Calling this function twice on same value will restore original content.
 * Note: This function is not thread safe!
 *
 * @param Variable (t_u32*)  address of value to convert
 */
void basic_cm3_32_swap_endian( t_u32* Variable );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _basic_cm3_foo(t_ttc_basic_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //BASIC_CM3_H
