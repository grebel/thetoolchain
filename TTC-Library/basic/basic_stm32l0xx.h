#ifndef BASIC_STM32L0XX_H
#define BASIC_STM32L0XX_H

/** { basic_stm32l0xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for basic devices on stm32l0xx architectures.
 *  Structures, Enums and Defines being required by high-level basic and application.
 *
 *  Created from template device_architecture.h revision 22 at 20150303 09:07:30 UTC
 *
 *  Note: See ttc_basic.h for description of stm32l0xx independent BASIC implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_BASIC_STM32L0XX
//
// Implementation status of low-level driver support for basic devices on stm32l0xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_BASIC_STM32L0XX '?'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_BASIC_STM32L0XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_BASIC_STM32L0XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_BASIC_STM32L0XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "basic_stm32l0xx.c"
//
#include "basic_stm32l0xx_types.h"
#include "../ttc_basic_types.h"
#include "stm32l0xx_hal.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_basic_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_basic_foo
//

// These are already defined by basic_cm3.h
//#undef ttc_driver_basic_16_atomic_add(Address, Amount)
//#undef ttc_driver_basic_32_atomic_add(Address, Amount)
//#undef ttc_driver_basic_08_atomic_add(Address, Amount)
//#undef ttc_driver_basic_16_atomic_read_write(Address, NewValue)
//#undef ttc_driver_basic_32_atomic_read_write(Address, NewValue)
//#undef ttc_driver_basic_08_atomic_read_write(Address, NewValue)
//#undef ttc_driver_basic_16_write_little_endian(Buffer, Data)
//#undef ttc_driver_basic_32_write_big_endian(Buffer, Data)
//#undef ttc_driver_basic_32_write_little_endian(Buffer, Data)
//#undef ttc_driver_basic_16_write_big_endian(Buffer, Data)
//#undef ttc_driver_basic_16_read_big_endian(Buffer)
//#undef ttc_driver_basic_16_read_little_endian(Buffer)
//#undef ttc_driver_basic_32_read_big_endian(Buffer)
//#undef ttc_driver_basic_32_read_little_endian(Buffer)
//#undef ttc_driver_basic_prepare
//#undef ttc_driver_basic_16_swap_endian
//#undef ttc_driver_basic_32_swap_endian
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes


/** Prepares basic Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void basic_stm32l0xx_prepare();

/** Calculate bit-banding address that corresponds to given bit in given 32-bit word
 *
 * @param   PeripheralWord   address of 32-bit word in which desired bit is located
 * @param   BitNumber        0..31
 * @return  address in peripheral bit-banding region that corresponds to given word and bit-number
 */
// volatile t_u32* cm0_calc_peripheral_BitBandAddress(void* PeripheralWord, t_u8 BitNumber);
#define cm0_calc_peripheral_BitBandAddress(PeripheralWord, BitNumber) (t_u32*) ((((t_u32) PeripheralWord)  - 0x50000000) + BitNumber * 4)

/** Calculate peripheral word that corresponds to given bit-banding address
   *
   * This function is a reverse of cm0_calc_peripheral_BitBandAddress()
   *
   * @param   BitBandAddress address in peripheral bit-banding region that corresponds to given word and bit-number
   * @return  address of peripheral 32-bit word
   */
// volatile t_u32* cm0_calc_peripheral_Word(void* BitBandAddress);
#define cm0_calc_peripheral_Word(BitBandAddress) (    (   (  ( ((t_u32) BitBandAddress) & 0xffffff80))) + 0x50000000    )

/** Calculate bit number that corresponds to given bit-banding address
   *
   * This function is a reverse of cm0_calc_peripheral_BitBandAddress()
   *
   * @param   BitBandAddress address in peripheral bit-banding region that corresponds to given word and bit-number
   * @return  BitNumber     0..31
   */
// volatile t_u32* cm0_calc_peripheral_Word(void* BitBandAddress);
#define cm0_calc_peripheral_BitNumber(BitBandAddress) ( ((t_u32) BitBandAddress) & 0x7f ) / 4;


//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _basic_stm32l0xx_foo(t_ttc_basic_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //BASIC_STM32L0XX_H
