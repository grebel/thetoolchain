/** { basic_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for basic devices on stm32l1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20140614 21:30:51 UTC
 *
 *  Note: See ttc_basic.h for description of stm32l1xx independent BASIC implementation.
 *
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "basic_stm32l1xx.h".
//
#include "basic_cm3.h"
#include "basic_stm32l1xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

void basic_stm32l1xx_prepare() {

    basic_cm3_prepare();


#if 0  // DEPRECATED initialize stm32l1xx using CMSIS library
#warning ToDo: Replace deprecated system init in basic_stm32l1xx_prepare()!

    //(already called from startup_stm32l1xx_md.s) SystemInit(); // init STM32 standard peripherals library

    t_u32 StartUpCounter = 0, HSIStatus = 0;
    /* Enable HSI */
    RCC->CR |= ( ( t_u32 ) RCC_CR_HSION );


    /* Wait till HSI is ready and if Time out is reached exit */
    do {
        HSIStatus = RCC->CR & RCC_CR_HSIRDY;
        StartUpCounter++;
    }
    while ( ( HSIStatus == 0 ) && ( StartUpCounter != HSI_STARTUP_TIMEOUT ) );

    if ( ( RCC->CR & RCC_CR_HSIRDY ) != RESET ) {
        HSIStatus = ( t_u32 )0x01;
    }
    else {
        HSIStatus = ( t_u32 )0x00;
    }
    /* Select HSI as system clock source */
    RCC->CFGR &= ( t_u32 )( ( t_u32 )~( RCC_CFGR_SW ) );
    RCC->CFGR |= ( t_u32 )RCC_CFGR_SW_HSI;

    /* Wait till HSI is used as system clock source */
    while ( ( RCC->CFGR & ( t_u32 )RCC_CFGR_SWS ) != ( t_u32 )RCC_CFGR_SWS_HSI ) {
    }
#endif
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
