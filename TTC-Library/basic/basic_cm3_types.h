#ifndef BASIC_CM3_TYPES_H
#define BASIC_CM3_TYPES_H

/** { basic_cm3.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for BASIC devices on cm3 architectures.
 *  Structures, Enums and Defines being required by ttc_basic_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140603 04:04:24 UTC
 *
 *  Note: See ttc_basic.h for description of architecture independent BASIC implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_BASIC1   // device not defined in makefile
    #define TTC_BASIC1    ta_basic_cm3   // example device definition for current architecture (Disable line if not desired!)
#endif

//?#include <stdint.h>

/* define compiler specific symbols */
#if defined ( __CC_ARM   )
    #define __ASM            __asm                                      /*!< asm keyword for ARM Compiler          */
    #define __INLINE         __inline                                   /*!< inline keyword for ARM Compiler       */

#elif defined ( __ICCARM__ )
    #define __ASM           __asm                                       /*!< asm keyword for IAR Compiler          */
    #define __INLINE        inline                                      /*!< inline keyword for IAR Compiler. Only avaiable in High optimization mode! */

#elif defined   (  __GNUC__  )
    #define __ASM            __asm                                      /*!< asm keyword for GNU Compiler          */
    #define __INLINE         inline                                     /*!< inline keyword for GNU Compiler       */

#elif defined   (  __TASKING__  )
    #define __ASM            __asm                                      /*!< asm keyword for TASKING Compiler      */
    #define __INLINE         inline                                     /*!< inline keyword for TASKING Compiler   */

#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

// basic definitions required by ttc_basic_types.h
#define  t_base                    t_u32  // basic (native) datatype of Cortex M3 architectures
#define  t_base_signed             t_s32  // signed basic (native) datatype
#define  TTC_BASIC_TYPES_DATAWIDTH 32     // This architecture provides native 32 bit datatypes
#define  TTC_BASIC_LITTLE_ENDIAN   1      // CortexM3 stores 16- and 32-bit data in little endian format (if not configured otherwise)
#define  TTC_BASIC_BIG_ENDIAN      0
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

//? #include "../ttc_basic.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_basic_types.h *************************


typedef struct {  // cm3 specific configuration data of single BASIC device
    char unused;
} __attribute__( ( __packed__ ) ) t_basic_cm3_config;

// t_ttc_basic_architecture is required by ttc_basic_types.h
//?#define t_ttc_basic_architecture t_basic_cm3_config

//} Structures/ Enums


#endif //BASIC_CM3_TYPES_H
