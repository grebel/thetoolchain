#ifndef BASIC_STM32L0XX_TYPES_H
#define BASIC_STM32L0XX_TYPES_H

/** { basic_stm32l0xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for BASIC devices on stm32l0xx architectures.
 *  Structures, Enums and Defines being required by ttc_basic_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150303 09:07:30 UTC
 *
 *  Note: See ttc_basic.h for description of architecture independent BASIC implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_BASIC1   // device not defined in makefile
    #define TTC_BASIC1    ta_basic_stm32l0xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

#define  TTC_BASIC_TYPES_DATAWIDTH 32

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_basic_types.h *************************

typedef struct { // register description (adapt according to stm32l0xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_basic_register;

typedef struct {  // stm32l0xx specific configuration data of single BASIC device
    t_basic_register* BaseRegister;       // base address of BASIC device registers
} __attribute__( ( __packed__ ) ) t_basic_stm32l0xx_config;

// t_ttc_basic_architecture is required by ttc_basic_types.h
#define t_ttc_basic_architecture t_basic_stm32l0xx_config

//} Structures/ Enums


#endif //BASIC_STM32L0XX_TYPES_H
