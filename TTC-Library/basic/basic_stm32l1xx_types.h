#ifndef BASIC_STM32L1XX_TYPES_H
#define BASIC_STM32L1XX_TYPES_H

/** { basic_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for BASIC devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_basic_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20140614 21:30:51 UTC
 *
 *  Note: See ttc_basic.h for description of architecture independent BASIC implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_BASIC1   // device not defined in makefile
    #define TTC_BASIC1    ta_basic_stm32l1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_basic_types.h *************************

// t_ttc_basic_architecture is already defined by basic_cm3_types.h
//X #define t_ttc_basic_architecture t_basic_stm32l1xx_config

//} Structures/ Enums


#endif //BASIC_STM32L1XX_TYPES_H
