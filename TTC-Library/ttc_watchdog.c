/*{ ttc_watchdog.c ************************************************

                      The ToolChain

  Initialisation and usage of watchdog timers.

  written by Gregor Rebel 2012

  This file shall provide a mostly hardware independent interface
  to watchdog timers of current activated microcontroller.
  It should always be preferred over e.g. stm32_watchdog.c because it
  allows to port your code more easily to other microcontroller architectures.

  How to use watchdogs
  1) activate watchdogs in your project
     SHELL> activate.500_ttc_watchdog.sh

  2) initialize a watchdog
     main() {
       ttc_watchdog_init1(0,0);
     }

  3) periodically reload watchdog
     void SomeTask(void* TaskArgument) {
       while (1) {
         // do something
         ttc_watchdog_reload1(); // must be called before watchdog1 underflows!
       }
     }
}*/

#include "ttc_watchdog.h"

//{ Function definitions *************************************************


int ttc_watchdog_init1( unsigned int Reload, int Prescaler ) {
    #ifdef EXTENSION_cpu_stm32f10x
    return stm32_init_IndependendWatchdog( Reload, Prescaler );
    #endif
    return 1; // error
}
void ttc_watchdog_reload1() {
    #ifdef EXTENSION_cpu_stm32f10x
    stm32_reload_IndependendWatchdog();
    #endif
}
void ttc_watchdog_deinit1() {
    #ifdef EXTENSION_cpu_stm32f10x
    stm32_deinit_IndependendWatchdog();
    #endif
}

//}FunctionDefinitions

