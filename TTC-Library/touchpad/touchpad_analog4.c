/** { touchpad_analog4.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for touchpad devices on analog architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20150316 07:48:52 UTC
 *
 *  Note: See ttc_touchpad.h for description of analog independent TOUCHPAD implementation.
 *
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "touchpad_analog4.h".
//
#include "touchpad_analog4.h"
#include "../ttc_gpio.h"
#include "../ttc_adc.h"
#include "../ttc_memory.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

const t_touchpad_analog4_pins touchpad_analog_pins[TTC_TOUCHPAD_AMOUNT] = {
    #ifdef TTC_TOUCHPAD1 //{
    {
        TTC_TOUCHPAD1_PIN_XR,
        TTC_TOUCHPAD1_PIN_XL,
        TTC_TOUCHPAD1_PIN_YU,
        TTC_TOUCHPAD1_PIN_YD,
    }
    #endif //}TTC_TOUCHPAD1
    #ifdef TTC_TOUCHPAD2 //{
    , {
        TTC_TOUCHPAD2_PIN_XR,
        TTC_TOUCHPAD2_PIN_XL,
        TTC_TOUCHPAD2_PIN_YU,
        TTC_TOUCHPAD2_PIN_YD,
    }
    #endif //}TTC_TOUCHPAD2
    #ifdef TTC_TOUCHPAD3 //{
    , {
        TTC_TOUCHPAD3_PIN_XR,
        TTC_TOUCHPAD3_PIN_XL,
        TTC_TOUCHPAD3_PIN_YU,
        TTC_TOUCHPAD3_PIN_YD,
    }
    #endif //}TTC_TOUCHPAD3
    #ifdef TTC_TOUCHPAD4 //{
    , {
        TTC_TOUCHPAD4_PIN_XR,
        TTC_TOUCHPAD4_PIN_XL,
        TTC_TOUCHPAD4_PIN_YU,
        TTC_TOUCHPAD4_PIN_YD,
    }
    #endif //}TTC_TOUCHPAD4
    #ifdef TTC_TOUCHPAD5 //{
    , {
        TTC_TOUCHPAD5_PIN_XR,
        TTC_TOUCHPAD5_PIN_XL,
        TTC_TOUCHPAD5_PIN_YU,
        TTC_TOUCHPAD5_PIN_YD,
    }
    #endif //}TTC_TOUCHPAD5
    #ifdef TTC_TOUCHPAD6 //{
    , {
        TTC_TOUCHPAD6_PIN_XR,
        TTC_TOUCHPAD6_PIN_XL,
        TTC_TOUCHPAD6_PIN_YU,
        TTC_TOUCHPAD6_PIN_YD,
    }
    #endif //}TTC_TOUCHPAD6
    #ifdef TTC_TOUCHPAD7 //{
    , {
        TTC_TOUCHPAD7_PIN_XR,
        TTC_TOUCHPAD7_PIN_XL,
        TTC_TOUCHPAD7_PIN_YU,
        TTC_TOUCHPAD7_PIN_YD,
    }
    #endif //}TTC_TOUCHPAD7
    #ifdef TTC_TOUCHPAD8 //{
    , {
        TTC_TOUCHPAD8_PIN_XR,
        TTC_TOUCHPAD8_PIN_XL,
        TTC_TOUCHPAD8_PIN_YU,
        TTC_TOUCHPAD8_PIN_YD,
    }
    #endif //}TTC_TOUCHPAD8
    #ifdef TTC_TOUCHPAD9 //{
    , {
        TTC_TOUCHPAD9_PIN_XR,
        TTC_TOUCHPAD9_PIN_XL,
        TTC_TOUCHPAD9_PIN_YU,
        TTC_TOUCHPAD9_PIN_YD,
    }
    #endif //}TTC_TOUCHPAD9
    #ifdef TTC_TOUCHPAD10 //{
    , {
        TTC_TOUCHPAD10_PIN_XR,
        TTC_TOUCHPAD10_PIN_XL,
        TTC_TOUCHPAD10_PIN_YU,
        TTC_TOUCHPAD10_PIN_YD,
    }
    #endif //}TTC_TOUCHPAD10
};

//}Global Variables

/** Take a single, two way measure
 *
 * Analog touchpads must take four measures to read a single coordinate
 * 1) Positive voltage is applied from left to right.
 *    ValueLR = Analog measure taken from up and down endpoint of the orthogonal line.
 * 2) Positive voltage is applied from right to left
 *    ValueRL = Analog measure taken from up and down endpoint of the orthogonal line.
 *
 * The absolute Uncertainty between ValueLR and ValueRL is a measure to detect a finger contact.
 * if (abs(ValueLR - ValueRL) < ContactThreshold)
 *   valid coordinate is (ValueLR + ValueRL)/2
 * else
 *   coordinate is not valid (no finger contact)
 *
 *
 * @param IndexADC_U      logical index of analog input used for up endpoint of line being orthogonal to LR-line (ADC must be initialized)
 * @param IndexADC_D      logical index of analog input used for up endpoint of line being orthogonal to LR-line (ADC must be initialized)
 * @param PinL            gpio pin used to output digital voltage on left  endpoint of line to measure
 * @param PinR            gpio pin used to output digital voltage on right endpoint of line to measure
 * @param Amount_Rereads  amount of measure to take for an average
 * @return Coordinate     loaded with averaged analog measure
 * @return Uncertainty    difference between high- and low-measure
 */
void _touchpad_analog4_check_line( t_u8 IndexADC_U, t_u8  IndexADC_D, e_ttc_gpio_pin PinL, e_ttc_gpio_pin PinR, t_u8 Amount_Rereads, t_u16* Coordinate, t_s16* Uncertainty );

/** Check for finger contact using digital inputs (faster than touchpad_analog4_check() )
 *
 * @param Config        pointer to struct t_ttc_touchpad_config
 * @return == 255: finger contact established; == 0: no contact
 */
t_u8 _touchpad_analog4_check_contact( t_ttc_touchpad_config* Config );

//{ Function Definitions *******************************************************

e_ttc_touchpad_errorcode touchpad_analog4_deinit( t_ttc_touchpad_config* Config ) {
    Assert_TOUCHPAD_EXTRA_Writable( Config , ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TOUCHPAD_EXTRA_Writable( Config->LowLevelConfig, ttc_assert_origin_auto );
    const t_touchpad_analog4_pins* Pins = Config->LowLevelConfig->analog4.Pins;

    // configure all pins of this touchpad device as floating inputs to be safe
    ttc_gpio_init( Pins->Pin_X_Left,  E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
    ttc_gpio_init( Pins->Pin_X_Right, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
    ttc_gpio_init( Pins->Pin_Y_Up,    E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
    ttc_gpio_init( Pins->Pin_Y_Down,  E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );

    if ( Config->LowLevelConfig->analog4.ADCIndex_X_Left )  { ttc_adc_deinit( Config->LowLevelConfig->analog4.ADCIndex_X_Left ); }
    if ( Config->LowLevelConfig->analog4.ADCIndex_X_Right ) { ttc_adc_deinit( Config->LowLevelConfig->analog4.ADCIndex_X_Right ); }
    if ( Config->LowLevelConfig->analog4.ADCIndex_Y_Up )    { ttc_adc_deinit( Config->LowLevelConfig->analog4.ADCIndex_Y_Up ); }
    if ( Config->LowLevelConfig->analog4.ADCIndex_Y_Down )  { ttc_adc_deinit( Config->LowLevelConfig->analog4.ADCIndex_Y_Down ); }

    return ec_touchpad_OK;
}
e_ttc_touchpad_errorcode touchpad_analog4_init( t_ttc_touchpad_config* Config ) {
    Assert_TOUCHPAD_EXTRA_Writable( Config , ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TOUCHPAD_EXTRA_Writable( Config->LowLevelConfig, ttc_assert_origin_auto );
    const t_touchpad_analog4_pins* Pins = Config->LowLevelConfig->analog4.Pins;

    // configure all pins of this touchpad device as floating inputs to be safe
    ttc_gpio_init( Pins->Pin_X_Left,  E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
    ttc_gpio_init( Pins->Pin_X_Right, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
    ttc_gpio_init( Pins->Pin_Y_Down,  E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
    ttc_gpio_init( Pins->Pin_Y_Up,    E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );

    // initialize all pins as analog input
    Config->LowLevelConfig->analog4.ADCIndex_X_Left  = ttc_adc_find_configuration_by_pin( Pins->Pin_X_Left )->LogicalIndex;
    Config->LowLevelConfig->analog4.ADCIndex_X_Right = ttc_adc_find_configuration_by_pin( Pins->Pin_X_Right )->LogicalIndex;
    Config->LowLevelConfig->analog4.ADCIndex_Y_Up    = ttc_adc_find_configuration_by_pin( Pins->Pin_Y_Up )->LogicalIndex;
    Config->LowLevelConfig->analog4.ADCIndex_Y_Down  = ttc_adc_find_configuration_by_pin( Pins->Pin_Y_Down )->LogicalIndex;

    ttc_adc_init( Config->LowLevelConfig->analog4.ADCIndex_X_Left );
    ttc_adc_init( Config->LowLevelConfig->analog4.ADCIndex_X_Right );
    ttc_adc_init( Config->LowLevelConfig->analog4.ADCIndex_Y_Up );
    ttc_adc_init( Config->LowLevelConfig->analog4.ADCIndex_Y_Down );

    Config->Flags.Bits.ContactLost = 1; // required for first _touchpad_analog4_check_contact() call

    return ec_touchpad_OK;
}
e_ttc_touchpad_errorcode touchpad_analog4_load_defaults( t_ttc_touchpad_config* Config ) {
    Assert_TOUCHPAD_EXTRA_Writable( Config , ttc_assert_origin_auto ); // pointers must not be NULL

    Config->Amount_Rereads = 2;
    Config->Architecture   = ta_touchpad_analog4;

    if ( !Config->LowLevelConfig ) { // no config: allocate it
        Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_touchpad_analog4_config ) );
    }
    t_touchpad_analog4_config* LowLevelConfig = &( Config->LowLevelConfig->analog4 );
    Assert( Config->LogicalIndex, ttc_assert_origin_auto );                       // logical index must not be zero!
    Assert( Config->LogicalIndex - 1 < TTC_TOUCHPAD_AMOUNT, ttc_assert_origin_auto ); // logical index must fit into array!

    LowLevelConfig->Pins = &( touchpad_analog_pins[Config->LogicalIndex - 1] );

    return ( e_ttc_touchpad_errorcode ) 0;
}
void touchpad_analog4_prepare() {

}
void touchpad_analog4_reset( t_ttc_touchpad_config* Config ) {
    Assert_TOUCHPAD_EXTRA_Writable( Config , ttc_assert_origin_auto ); // pointers must not be NULL

    touchpad_analog4_deinit( Config );
    touchpad_analog4_init( Config ); // ToDo: check if this works
}
void touchpad_analog4_check( t_ttc_touchpad_config* Config ) {
    Assert_TOUCHPAD_EXTRA_Writable( Config , ttc_assert_origin_auto ); // pointers must not be NULL

    const t_touchpad_analog4_pins* Pins = Config->LowLevelConfig->analog4.Pins;

    t_u16 Coordinate;
    if ( !Config->Pressure ) // no contact yet: use a simple check
    { Config->Pressure = _touchpad_analog4_check_contact( Config ); }

    if ( Config->Pressure ) { // contact established: read analog inputs
        if ( 1 ) { // read analog X-input

            // reconfigure two GPIOs as analog input to read in X value
            ttc_gpio_init( Pins->Pin_Y_Up,    E_ttc_gpio_mode_analog_in,        E_ttc_gpio_speed_min );
            ttc_gpio_init( Pins->Pin_Y_Down,  E_ttc_gpio_mode_analog_in,        E_ttc_gpio_speed_min );
            t_s16 Uncertainty;

            _touchpad_analog4_check_line( Config->LowLevelConfig->analog4.ADCIndex_Y_Up,
                                          Config->LowLevelConfig->analog4.ADCIndex_Y_Down,
                                          Pins->Pin_X_Left,
                                          Pins->Pin_X_Right,
                                          Config->Amount_Rereads,
                                          &Coordinate,
                                          &Uncertainty
                                        );

            if ( abs( Uncertainty ) < Config->LowLevelConfig->analog4.ContactThresholdX ) {
                //? if ( Config->LowLevelConfig->analog4.Calibrate_Xmax < Coordinate )
                //? { Config->LowLevelConfig->analog4.Calibrate_Xmax = Coordinate; }

                Config->Position_X = Config->Max_X * Coordinate / Config->LowLevelConfig->analog4.Calibrate_Xmax;
                Config->Pressure = 255; // this touch does not support pressure
            }
            else {
                Config->Pressure = 0;
            }
        }
        if ( Config->Pressure ) { // x-coordinate valid: read analog Y-input

            // reconfigure two GPIOs as analog input to read in X value
            ttc_gpio_init( Pins->Pin_X_Left,  E_ttc_gpio_mode_analog_in,        E_ttc_gpio_speed_min );
            ttc_gpio_init( Pins->Pin_X_Right, E_ttc_gpio_mode_analog_in,        E_ttc_gpio_speed_min );

            t_s16 Uncertainty;
            _touchpad_analog4_check_line( Config->LowLevelConfig->analog4.ADCIndex_X_Left,
                                          Config->LowLevelConfig->analog4.ADCIndex_X_Right,
                                          Pins->Pin_Y_Up,
                                          Pins->Pin_Y_Down,
                                          Config->Amount_Rereads,
                                          &Coordinate,
                                          &Uncertainty
                                        );

            if ( abs( Uncertainty ) < Config->LowLevelConfig->analog4.ContactThresholdY ) {
                if ( Config->LowLevelConfig->analog4.Calibrate_Ymax < Coordinate )
                { Config->LowLevelConfig->analog4.Calibrate_Ymax = Coordinate; }

                Config->Position_Y = Config->Max_Y - ( Config->Max_Y * Coordinate / Config->LowLevelConfig->analog4.Calibrate_Ymax );
            }
            else {
                Config->Pressure = 0;
            }
        }
    }
}
void touchpad_analog4_calibrate( t_ttc_touchpad_config* Config ) {
    Assert_TOUCHPAD_EXTRA_Writable( Config , ttc_assert_origin_auto ); // pointers must not be NULL

    const t_touchpad_analog4_pins* Pins = Config->LowLevelConfig->analog4.Pins;

    t_u16 Coordinate;

    if ( _touchpad_analog4_check_contact( Config ) ) { // contact: calculate ContactThreshold
        if ( 1 ) { // calibrate X-input

            // configure two GPIOs as analog input to read in X value
            //?        ttc_adc_init(Config->LowLevelConfig->analog4.ADCIndex_Y_Up);
            //?        ttc_adc_init(Config->LowLevelConfig->analog4.ADCIndex_Y_Down);
            ttc_gpio_init( Pins->Pin_Y_Down,  E_ttc_gpio_mode_analog_in,        E_ttc_gpio_speed_min );
            ttc_gpio_init( Pins->Pin_Y_Up,    E_ttc_gpio_mode_analog_in,        E_ttc_gpio_speed_min );

            t_s16 Uncertainty;
            t_s16 UncertaintyAverage = 0;
            t_u16 Calibrate_Xmax = 0;
            t_u16 Calibrate_Xmin = -1;
            for ( t_u8 Retries = 200; Retries > 0; Retries-- ) {
                _touchpad_analog4_check_line( Config->LowLevelConfig->analog4.ADCIndex_Y_Up,
                                              Config->LowLevelConfig->analog4.ADCIndex_Y_Down,
                                              Pins->Pin_X_Left,
                                              Pins->Pin_X_Right,
                                              5,
                                              &Coordinate,
                                              &Uncertainty
                                            );

                t_u16 Delta = abs( Uncertainty ) / 2;
                // update min and max values
                if ( Calibrate_Xmax < Coordinate + Delta )
                { Calibrate_Xmax = Coordinate + Delta; }
                if ( Calibrate_Xmin > Coordinate - Delta )
                { Calibrate_Xmin = Coordinate - Delta; }

                if ( UncertaintyAverage )
                { UncertaintyAverage = ( UncertaintyAverage + abs( Uncertainty ) ) / 2; }
                else
                { UncertaintyAverage = Uncertainty; }
            }

            Config->LowLevelConfig->analog4.ContactThresholdX = abs( UncertaintyAverage * 4 ); // using measured uncertainty as contact threshold
            if ( Config->LowLevelConfig->analog4.Calibrate_Xmax < Calibrate_Xmax )
            { Config->LowLevelConfig->analog4.Calibrate_Xmax = Calibrate_Xmax; }
            if ( Config->LowLevelConfig->analog4.Calibrate_Xmin >  Calibrate_Xmin )
            { Config->LowLevelConfig->analog4.Calibrate_Xmin = Calibrate_Xmin; }
        }
        if ( 1 ) { // calibrate Y-input

            // configure two GPIOs as analog input to read in X value
            //?        ttc_adc_init(Config->LowLevelConfig->analog4.ADCIndex_X_Left);
            //?        ttc_adc_init(Config->LowLevelConfig->analog4.ADCIndex_X_Right);
            ttc_gpio_init( Pins->Pin_X_Left,  E_ttc_gpio_mode_analog_in, E_ttc_gpio_speed_min );
            ttc_gpio_init( Pins->Pin_X_Right, E_ttc_gpio_mode_analog_in, E_ttc_gpio_speed_min );

            t_s16 Uncertainty;
            t_s16 UncertaintyAverage = 0;
            t_u16 Calibrate_Ymax = 0;
            t_u16 Calibrate_Ymin = -1;
            for ( t_u8 Retries = 200; Retries > 0; Retries-- ) {
                _touchpad_analog4_check_line( Config->LowLevelConfig->analog4.ADCIndex_X_Left,
                                              Config->LowLevelConfig->analog4.ADCIndex_X_Right,
                                              Pins->Pin_Y_Up,
                                              Pins->Pin_Y_Down,
                                              5,
                                              &Coordinate,
                                              &Uncertainty
                                            );

                t_u16 Delta = abs( Uncertainty ) / 2;
                // update min and max values
                if ( Calibrate_Ymax < Coordinate + Delta )
                { Calibrate_Ymax = Coordinate + Delta; }
                if ( Calibrate_Ymin > Coordinate - Delta )
                { Calibrate_Ymin = Coordinate - Delta; }

                if ( UncertaintyAverage )
                { UncertaintyAverage = ( UncertaintyAverage + abs( Uncertainty ) ) / 2; }
                else
                { UncertaintyAverage = Uncertainty; }
            }

            Config->LowLevelConfig->analog4.ContactThresholdY = abs( UncertaintyAverage * 4 ); // using measured uncertainty as contact threshold
            if ( Config->LowLevelConfig->analog4.Calibrate_Ymax < Calibrate_Ymax )
            { Config->LowLevelConfig->analog4.Calibrate_Ymax = Calibrate_Ymax; }
            if ( Config->LowLevelConfig->analog4.Calibrate_Ymin > Calibrate_Ymin )
            { Config->LowLevelConfig->analog4.Calibrate_Ymin = Calibrate_Ymin; }
        }
    }
}
void _touchpad_analog4_check_line( t_u8 IndexADC_U,
                                   t_u8  IndexADC_D,
                                   e_ttc_gpio_pin PinL,
                                   e_ttc_gpio_pin PinR,
                                   t_u8 Amount_Rereads,
                                   t_u16* Coordinate,
                                   t_s16* Uncertainty ) {
    t_u16 ValueRL = -1;
    t_u16 ValueLR = -1;
    t_u16 Value;

    // configure two GPIOs as digital output to apply voltageContactThreshold to X-line
    ttc_gpio_init( PinR, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min );
    ttc_gpio_init( PinL, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min );
    t_base MaxADC_Value = 2 * ttc_adc_get_maximum( IndexADC_U ); // two analog measures are summed to give one reading

    // apply voltage on X-line from right to left
    ttc_gpio_clr( PinL );
    ttc_gpio_set( PinR );
    ttc_task_usleep( 10 );

    // read analog X-input right to left multiple times to average analog input
    for ( t_u8 ReadCount = Amount_Rereads; ReadCount > 0; ReadCount-- ) {
        t_u16 ValueU = ttc_adc_get_value( IndexADC_U );
        t_u16 ValueD = ttc_adc_get_value( IndexADC_D );
        Value = MaxADC_Value - ( ValueU + ValueD ); // one way has to be inverted
        if ( ValueRL == ( t_u16 ) - 1 )
        { ValueRL = Value; }                  // first measure: simply take readout
        else
        { ValueRL = ( ValueRL + Value ) / 2; } // other measure: calculate floating average
    }

    // apply voltage on X-line from left to right
    ttc_gpio_set( PinL );
    ttc_gpio_clr( PinR );
    ttc_task_usleep( 10 );

    // read analog X-input left to right multiple times to average analog input
    for ( t_u8 ReadCount = Amount_Rereads; ReadCount > 0; ReadCount-- ) {
        t_u16 ValueU = ttc_adc_get_value( IndexADC_U );
        t_u16 ValueD = ttc_adc_get_value( IndexADC_D );
        Value = ValueU + ValueD;
        if ( ValueLR == ( t_u16 ) - 1 )
        { ValueLR = Value; }                  // first measure: simply take readout
        else
        { ValueLR = ( ValueLR + Value ) / 2; } // other measure: calculate floating average
    }

    // absolute Uncertainty between LR- and RL-value is a decision base to detect finger contact
    *Uncertainty = ( ValueRL > ValueLR ) ? ValueRL - ValueLR : ValueLR - ValueRL;
    //*Uncertainty = ValueLR - ValueRL;

    // calculate center coordinate
    *Coordinate = ( ValueLR + ValueRL ) / 2;
}
t_u8 _touchpad_analog4_check_contact( t_ttc_touchpad_config* Config ) {
    Assert_TOUCHPAD_EXTRA_Writable( Config , ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_TOUCHPAD_EXTRA_Writable( Config->LowLevelConfig, ttc_assert_origin_auto );
    const t_touchpad_analog4_pins* Pins = Config->LowLevelConfig->analog4.Pins;
    Assert_TOUCHPAD_EXTRA( ttc_memory_is_readable( Pins ), ttc_assert_origin_auto );

    if ( 1 || Config->Flags.Bits.ContactLost ) { // just lost contact: reinit GPIOs to digital
        ttc_gpio_init( Pins->Pin_X_Left,  E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min );
        ttc_gpio_init( Pins->Pin_X_Right, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min );

        ttc_gpio_set( Pins->Pin_X_Left ); // applying voltage to X-line
        ttc_gpio_set( Pins->Pin_X_Right ); // applying voltage to X-line
    }
    t_u8 Contact = 0;

    // we have to take two measures to detect contacts in upper and lower half of touchpad
    if ( 1 ) { // ground Y_Down and measure Y_Up
        ttc_gpio_init( Pins->Pin_Y_Down,  E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min );
        ttc_gpio_init( Pins->Pin_Y_Up,    E_ttc_gpio_mode_input_floating,   E_ttc_gpio_speed_min );
        ttc_gpio_clr( Pins->Pin_Y_Down ); // ground end of Y-line
        if ( ttc_gpio_get( Pins->Pin_Y_Up ) ) // taking measure on Y_up
        { Contact = 255; } // contact established in upper half of touchpad
    }
    if ( 1 ) { // ground Y_Up and measure Y_Down
        ttc_gpio_init( Pins->Pin_Y_Up,   E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min );
        ttc_gpio_init( Pins->Pin_Y_Down, E_ttc_gpio_mode_input_floating,   E_ttc_gpio_speed_min );
        ttc_gpio_clr( Pins->Pin_Y_Up ); // ground end of Y-line
        if ( ttc_gpio_get( Pins->Pin_Y_Down ) ) // taking measure on Y_down
        { Contact = 255; } // contact established in lower half of touchpad
    }
    if ( Contact && !Config->LowLevelConfig->analog4.ContactThresholdX ) {
        // first contact: callibrate touchpad
        Config->LowLevelConfig->analog4.ContactThresholdX = 1; // avoid endless loop (touchpad_analog4_calibrate() calls us again!)
        touchpad_analog4_calibrate( Config );
    }

    return Contact;   // no contact
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions

//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
