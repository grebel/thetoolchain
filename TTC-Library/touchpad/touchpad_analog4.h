#ifndef TOUCHPAD_ANALOG_H
#define TOUCHPAD_ANALOG_H

/** { touchpad_analog4.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for analog touchpad devices using 4 control pins.
 *
 *  Created from template device_architecture.h revision 22 at 20150316 07:48:52 UTC
 *
 *  Note: See ttc_touchpad.h for description of analog independent TOUCHPAD implementation.
 *
 *  Authors: Patrick von Poblotzki, Gregor Rebel
 *
 *
 *  How to configure analog4 touchpad devices
 *
 *  This low-level driver requires configuration defines in addtion to the howto section
 *  of ttc_touchpad.h.
 *  The additional configuration options define the 4 touchpad pins
 *  TTC_TOUCHPAD1_PIN_XR  X-right pin
 *  TTC_TOUCHPAD1_PIN_XL  X-left  pin
 *  TTC_TOUCHPAD1_PIN_YU  Y-up    pin
 *  TTC_TOUCHPAD1_PIN_YD  Y-down  pin
 *
 *  Example makefile lines
    COMPILE_OPTS += -DTTC_TOUCHPAD1_PIN_XR=E_ttc_gpio_pin_c3
    COMPILE_OPTS += -DTTC_TOUCHPAD1_PIN_XL=E_ttc_gpio_pin_c2
    COMPILE_OPTS += -DTTC_TOUCHPAD1_PIN_YU=E_ttc_gpio_pin_c1
    COMPILE_OPTS += -DTTC_TOUCHPAD1_PIN_YD=E_ttc_gpio_pin_c0
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_TOUCHPAD_ANALOG4
//
// Implementation status of low-level driver support for touchpad devices on analog4
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_TOUCHPAD_ANALOG4 '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_TOUCHPAD_ANALOG4 == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_TOUCHPAD_ANALOG4 to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_TOUCHPAD_ANALOG4

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "touchpad_analog4.c"
//
#include "touchpad_analog4_types.h"
#include "../ttc_touchpad_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_touchpad_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_touchpad_foo
//
#define ttc_driver_touchpad_deinit(Config) touchpad_analog4_deinit(Config)
#undef ttc_driver_touchpad_get_features
#define ttc_driver_touchpad_init(Config) touchpad_analog4_init(Config)
#define ttc_driver_touchpad_load_defaults(Config) touchpad_analog4_load_defaults(Config)
#define ttc_driver_touchpad_prepare() touchpad_analog4_prepare()
#define ttc_driver_touchpad_reset(Config) touchpad_analog4_reset(Config)
#define ttc_driver_touchpad_check(Config) touchpad_analog4_check(Config)
#define ttc_driver_touchpad_calibrate(Config) touchpad_analog4_calibrate(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_touchpad.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_touchpad.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** shutdown single TOUCHPAD unit device
 * @param Config        pointer to struct t_ttc_touchpad_config
 * @return              == 0: TOUCHPAD has been shutdown successfully; != 0: error-code
 */
e_ttc_touchpad_errorcode touchpad_analog4_deinit( t_ttc_touchpad_config* Config );


/** initializes single TOUCHPAD unit for operation
 * @param Config        pointer to struct t_ttc_touchpad_config
 * @return              == 0: TOUCHPAD has been initialized successfully; != 0: error-code
 */
e_ttc_touchpad_errorcode touchpad_analog4_init( t_ttc_touchpad_config* Config );


/** loads configuration of indexed TOUCHPAD unit with default values
 * @param Config        pointer to struct t_ttc_touchpad_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_touchpad_errorcode touchpad_analog4_load_defaults( t_ttc_touchpad_config* Config );


/** Prepares touchpad Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void touchpad_analog4_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_touchpad_config
 */
void touchpad_analog4_reset( t_ttc_touchpad_config* Config );


/** reads position data from touchpad into Config
 *
 * @param Config        pointer to struct t_ttc_touchpad_config
 */
void touchpad_analog4_check( t_ttc_touchpad_config* Config );


/** Run touchpad self calibration now
 *
 * Calibration is automatically run at touchpad initialization.
 * Some touchpads float over time and need periodic recalibration.
 *
 * @param Config        pointer to struct t_ttc_touchpad_config
 */
void touchpad_analog4_calibrate( t_ttc_touchpad_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _touchpad_analog4_foo(t_ttc_touchpad_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TOUCHPAD_ANALOG_H