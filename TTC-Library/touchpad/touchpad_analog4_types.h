#ifndef TOUCHPAD_ANALOG_TYPES_H
#define TOUCHPAD_ANALOG_TYPES_H

/** { touchpad_analog4.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for TOUCHPAD devices on analog architectures.
 *  Structures, Enums and Defines being required by ttc_touchpad_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150316 07:48:52 UTC
 *
 *  Note: See ttc_touchpad.h for description of architecture independent TOUCHPAD implementation.
 *
 *  Authors: Patrick von Poblotzki, Gregor Rebel
 *
 * Analog touchpads consist of a horizontal and a vertical row.
 * Each row has two digital outputs and two analog inputs.
 * The digital outputs are set to one of two complementary voltage settings (one end high while
 * the other end is low and vice versa). The analog input is then read
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#include "../ttc_gpio_types.h"

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Check Static Configuration *************************************************

#ifdef TTC_TOUCHPAD1
#  ifndef TTC_TOUCHPAD1_PIN_XR
#    error Missing definition of TTC_TOUCHPAD1_PIN_XR. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD1_PIN_XL
#    error Missing definition of TTC_TOUCHPAD1_PIN_XL. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD1_PIN_YU
#    error Missing definition of TTC_TOUCHPAD1_PIN_YU. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD1_PIN_YD
#    error Missing definition of TTC_TOUCHPAD1_PIN_YD. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#endif
#ifdef TTC_TOUCHPAD2
#  ifndef TTC_TOUCHPAD2_PIN_XR
#    error Missing definition of TTC_TOUCHPAD2_PIN_XR. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD2_PIN_XL
#    error Missing definition of TTC_TOUCHPAD2_PIN_XL. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD2_PIN_YU
#    error Missing definition of TTC_TOUCHPAD2_PIN_YU. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD2_PIN_YD
#    error Missing definition of TTC_TOUCHPAD2_PIN_YD. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#endif
#ifdef TTC_TOUCHPAD3
#  ifndef TTC_TOUCHPAD3_PIN_XR
#    error Missing definition of TTC_TOUCHPAD3_PIN_XR. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD3_PIN_XL
#    error Missing definition of TTC_TOUCHPAD3_PIN_XL. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD3_PIN_YU
#    error Missing definition of TTC_TOUCHPAD3_PIN_YU. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD3_PIN_YD
#    error Missing definition of TTC_TOUCHPAD3_PIN_YD. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#endif
#ifdef TTC_TOUCHPAD4
#  ifndef TTC_TOUCHPAD4_PIN_XR
#    error Missing definition of TTC_TOUCHPAD4_PIN_XR. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD4_PIN_XL
#    error Missing definition of TTC_TOUCHPAD4_PIN_XL. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD4_PIN_YU
#    error Missing definition of TTC_TOUCHPAD4_PIN_YU. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD4_PIN_YD
#    error Missing definition of TTC_TOUCHPAD4_PIN_YD. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#endif
#ifdef TTC_TOUCHPAD5
#  ifndef TTC_TOUCHPAD5_PIN_XR
#    error Missing definition of TTC_TOUCHPAD5_PIN_XR. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD5_PIN_XL
#    error Missing definition of TTC_TOUCHPAD5_PIN_XL. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD5_PIN_YU
#    error Missing definition of TTC_TOUCHPAD5_PIN_YU. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD5_PIN_YD
#    error Missing definition of TTC_TOUCHPAD5_PIN_YD. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#endif
#ifdef TTC_TOUCHPAD6
#  ifndef TTC_TOUCHPAD6_PIN_XR
#    error Missing definition of TTC_TOUCHPAD6_PIN_XR. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD6_PIN_XL
#    error Missing definition of TTC_TOUCHPAD6_PIN_XL. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD6_PIN_YU
#    error Missing definition of TTC_TOUCHPAD6_PIN_YU. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD6_PIN_YD
#    error Missing definition of TTC_TOUCHPAD6_PIN_YD. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#endif
#ifdef TTC_TOUCHPAD7
#  ifndef TTC_TOUCHPAD7_PIN_XR
#    error Missing definition of TTC_TOUCHPAD7_PIN_XR. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD7_PIN_XL
#    error Missing definition of TTC_TOUCHPAD7_PIN_XL. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD7_PIN_YU
#    error Missing definition of TTC_TOUCHPAD7_PIN_YU. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD7_PIN_YD
#    error Missing definition of TTC_TOUCHPAD7_PIN_YD. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#endif
#ifdef TTC_TOUCHPAD8
#  ifndef TTC_TOUCHPAD8_PIN_XR
#    error Missing definition of TTC_TOUCHPAD8_PIN_XR. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD8_PIN_XL
#    error Missing definition of TTC_TOUCHPAD8_PIN_XL. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD8_PIN_YU
#    error Missing definition of TTC_TOUCHPAD8_PIN_YU. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD8_PIN_YD
#    error Missing definition of TTC_TOUCHPAD8_PIN_YD. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#endif
#ifdef TTC_TOUCHPAD9
#  ifndef TTC_TOUCHPAD9_PIN_XR
#    error Missing definition of TTC_TOUCHPAD9_PIN_XR. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD9_PIN_XL
#    error Missing definition of TTC_TOUCHPAD9_PIN_XL. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD9_PIN_YU
#    error Missing definition of TTC_TOUCHPAD9_PIN_YU. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD9_PIN_YD
#    error Missing definition of TTC_TOUCHPAD9_PIN_YD. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#endif
#ifdef TTC_TOUCHPAD10
#  ifndef TTC_TOUCHPAD10_PIN_XR
#    error Missing definition of TTC_TOUCHPAD10_PIN_XR. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD10_PIN_XL
#    error Missing definition of TTC_TOUCHPAD10_PIN_XL. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD10_PIN_YU
#    error Missing definition of TTC_TOUCHPAD10_PIN_YU. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#  ifndef TTC_TOUCHPAD10_PIN_YD
#    error Missing definition of TTC_TOUCHPAD10_PIN_YD. Add a corresponding COMPILE_OPTS line to your board makefile!
#  endif
#endif

//}Check Static Configuration
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_touchpad_types.h *************************

typedef struct {

    // pins to which analog touch input is connected to
    e_ttc_gpio_pin Pin_X_Left;  // gpio pin connected to left  end of X line
    e_ttc_gpio_pin Pin_X_Right; // gpio pin connected to right end of X line
    e_ttc_gpio_pin Pin_Y_Up;    // gpio pin connected to upper end of Y line
    e_ttc_gpio_pin Pin_Y_Down;  // gpio pin connected to lower end of Y line
    t_u16          TimeOut;     // Amount of milliseconds to pass before finger contact is established/ ended
} __attribute__( ( __packed__ ) ) t_touchpad_analog4_pins;

typedef struct {  // analog specific configuration data of single TOUCHPAD device

    // pins to which analog touch input is connected to
    const t_touchpad_analog4_pins* Pins;

    t_u8 ADCIndex_X_Left;       // logical index of ADC device corresponding to analog input for left  end of X line
    t_u8 ADCIndex_X_Right;      // logical index of ADC device corresponding to analog input for right end of X line
    t_u8 ADCIndex_Y_Up;         // logical index of ADC device corresponding to analog input for upper end of X line
    t_u8 ADCIndex_Y_Down;       // logical index of ADC device corresponding to analog input for lower end of X line

    // minimum/ maximum analog values from calibration run
    t_u16 Calibrate_Xmin;
    t_u16 Calibrate_Xmax;
    t_u16 Calibrate_Ymin;
    t_u16 Calibrate_Ymax;

    t_u8  ContactThresholdX;     // detect contact if (abs(ValueLR - ValueRL) < Threshold)
    t_u8  ContactThresholdY;     // detect contact if (abs(ValueLR - ValueRL) < Threshold)
} __attribute__( ( __packed__ ) ) t_touchpad_analog4_config;

// t_ttc_touchpad_architecture is required by ttc_touchpad_types.h
//X #define t_ttc_touchpad_architecture t_touchpad_analog4_config

//} Structures/ Enums


#endif //TOUCHPAD_ANALOG_TYPES_H
