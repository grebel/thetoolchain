#ifndef USART_STM32W1XX_TYPES_H
#define USART_STM32W1XX_TYPES_H

/** { usart_stm32w1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for USART devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by ttc_usart_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140217 09:36:59 UTC
 *
 *  Note: See ttc_usart.h for description of architecture independent USART implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ******************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

// maximum amount of available USART/ UART units in current architecture
#define TTC_USART_MAX_AMOUNT 1

//InsertEnums above (DO NOT REMOVE THIS LINE!)

// ttc_usart_arch_t is required by ttc_usart_types.h
#define _driver_usart_rx_isr      _usart_stm32w1xx_rx_isr // define function pointer for receive interrupt service routine
#define _driver_usart_tx_isr      _usart_stm32w1xx_tx_isr // define function pointer for transmit interrupt service routine
#define t_ttc_usart_architecture  t_usart_stm32w1xx_config
#define t_ttc_usart_register      t_register_stm32w1xx_usart
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

// low-level register used to send out single byte
#ifndef t_ttc_usart_register_tx
    #define t_ttc_usart_register_tx t_base
#endif

// low-level register used to receive single byte
#ifndef t_ttc_usart_register_rx
    #define t_ttc_usart_register_rx t_base
#endif

//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic.h"
#include "../gpio/gpio_stm32w1xx.h"
#include "../register/register_stm32w1xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_usart_types.h ***********************

typedef struct { // t_ttc_interrupt_usart_errors - support for ttc_interrupt_types.h
    unsigned Overrun  : 1;
    unsigned Noise    : 1;
    unsigned Framing  : 1;
    unsigned Parity   : 1;
    unsigned reserved : 4;

} t_ttc_interrupt_usart_errors;

typedef struct { // register description (adapt according to stm32w1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_usart_register;

typedef struct { // all registers of single USART together
    volatile t_register_stm32w1xx_scx*         Control;
    volatile t_register_stm32w1xx_int_scxflag* Flags;
    volatile t_register_stm32w1xx_int_scxcfg*  Config;
    volatile t_register_stm32w1xx_scx_intmode* Interrupts;
} t_register_stm32w1xx_usart;

typedef struct { // stm32w1xx specific configuration data of single USART device
    t_register_stm32w1xx_usart BaseRegister;   // base address of USART device registers
    e_ttc_gpio_pin PortTxD;                    // port pin for TxD
    e_ttc_gpio_pin PortRxD;                    // port pin for RxD
    e_ttc_gpio_pin PortRTS;                    // port pin for RTS
    e_ttc_gpio_pin PortCTS;                    // port pin for CTS
    e_ttc_gpio_pin PortCLK;                    // port pin for CLK
} __attribute__( ( __packed__ ) ) t_usart_stm32w1xx_config;


//} Structures/ Enums


#endif //USART_STM32W1XX_TYPES_H
