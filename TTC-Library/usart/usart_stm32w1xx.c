/** { usart_stm32w1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for usart devices on stm32w1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 20 at 20140217 09:36:59 UTC
 *
 *  Note: See ttc_usart.h for description of stm32w1xx independent USART implementation.
 * 
 *  Authors: Gregor Rebel
}*/

#include "usart_stm32w1xx.h"

#ifdef EXTENSION_255_stm32w1xx_standard_peripherals
#  include "stm32w108xx_exti.h"
#endif

// these are defined by ttc_usart.c
extern void _ttc_usart_rx_isr(t_ttc_usart_config* Config, t_u8 Byte);
extern void _ttc_usart_tx_isr(t_ttc_usart_config* Config, volatile void* USART_Hardware);
extern t_base ttc_usart_Timeout;

// register base addresses of all USARTs (not so many)
t_register_stm32w1xx_usart USART1_Base;

//{ Function definitions *******************************************************

void usart_stm32w1xx_prepare() {
  // load addresses of UART registers into single struct
  USART1_Base.Control    = & register_stm32w1xx_SC1;
  USART1_Base.Flags      = & register_stm32w1xx_INT_SC1FLAG;
  USART1_Base.Config     = & register_stm32w1xx_INT_SC1CFG;
  USART1_Base.Interrupts = & register_stm32w1xx_SC1_INTMODE;

}
e_ttc_usart_errorcode usart_stm32w1xx_init(t_ttc_usart_config* Config) {
    /** TheToolChain/Documentation/uC/STM32W1xx/Ember_Manual_EM35x.pdf p. 76 (chapter 8-2)
     *
     * Before using a serial controller, configure and initialize it as follows:
     *
     * Set up the parameters specific to the operating mode (master/slave for SPI, baud rate for UART, etc.).
     * Configure the GPIO pins used by the serial controller as shown in Table 8-1 and Table 8-2. Section 2 in
     * Chapter 7, GPIO shows how to configure GPIO pins.
     * If using DMA, set up the DMA and buffers. This is described fully in section 8.7.
     * If using interrupts, select edge- or level-triggered interrupts with the SCx_INTMODE register, enable
     * the desired second-level interrupt sources in the INT_SCxCFG register, and finally enable the top-level
     * SCx interrupt in the NVIC.
     * Write the serial interface operating mode — SPI, TWI, or UART — to the SCx_MODE register.
     */

    t_u8 LogicalIndex = Config->LogicalIndex;
    //X ttc_usart_flags_u Flags; //X Flags.All = Config->Init.//X Flags.All;
    t_register_stm32w1xx_scx_mode MODE = register_stm32w1xx_SC1.MODE;

    if (1) { // switch of serial controller for reconfiguration
        MODE.bSC_MODE = sc_mode_disabled;
        register_stm32w1xx_SC1.MODE = MODE; // must access register like this to prevent bus fault from non 32-bit access
    }
    if (1) { // check Config
        Assert(LogicalIndex > 0, ttc_assert_origin_auto); // logical index starts at 1
        Assert_USART(Config != NULL, ttc_assert_origin_auto);
        if (LogicalIndex > TTC_USART_AMOUNT)
            return ec_usart_DeviceNotFound;
        if ( (Config->Init.WordLength   < 8) || (Config->Init.WordLength   > 9) )
            return ec_usart_InvalidWordSize;
        if ( (Config->Init.HalfStopBits < 1) || (Config->Init.HalfStopBits > 4) )
            return ec_usart_InvalidStopBits;
    }
    if (1) { // validate USART_Features
        t_ttc_usart_config USART_Features;
        ttc_memory_set(&USART_Features, 0, sizeof(t_ttc_usart_config));
        USART_Features.LogicalIndex  = LogicalIndex;
        USART_Features.PhysicalIndex = Config->PhysicalIndex;
        e_ttc_usart_errorcode Error=usart_stm32w1xx_get_features(&USART_Features);
        if (Error) return Error;

        //X Config->Init.Flags.All &= USART_Features.Flags.All; // mask out unavailable flags
        Assert_USART_EXTRA( sizeof(Config->Init.Flags) == 4, ttc_assert_origin_auto ); // size of bitfield must exactly match typecast below. Change code to fit!
        // mask out unavailable flags
        *( (t_u32*) &(Config->Init.Flags) ) &= *( (t_u32*) &(USART_Features.Init.Flags) );

        if (Config->Layout > USART_Features.Layout)
            Config->Layout=USART_Features.Layout;
    }
    if (1) { // write architecture settings in Config
        // set pointers to registers for fast transmit/receive operations
        Config->RegisterTx = (t_ttc_usart_register_rx*) USART_STM32W1XX_SC1_DATA;
        Config->RegisterRx = (t_ttc_usart_register_rx*) USART_STM32W1XX_SC1_DATA;
    }
    if (1) { // BaudRate
        // Calculate best settings for given BaudRate and current system clock (Ember_Manual_EM35x.pdf p. 99)
        t_ttc_sysclock_config* SysClock = ttc_sysclock_get_configuration();
        t_u16 Best_UARTPER16 = 0, Best_UARTFRAC16 = 0;
        t_u32 MinBaudRateError = -1;

        for (t_u8 UARTFRAC16 = 0; UARTFRAC16 <= 1; UARTFRAC16++) {
            t_u32 LastRateError = -1;
            for (t_u16 UARTPER16 = 1; UARTPER16 <= 65535; UARTPER16++) {
                t_u32 EffectiveBaudRate = SysClock->SystemClockFrequency / (2*UARTPER16 + UARTFRAC16);
                t_u32 BaudRateError;
                if (EffectiveBaudRate > Config->Init.BaudRate)
                    BaudRateError = EffectiveBaudRate - Config->Init.BaudRate;
                else
                    BaudRateError = Config->Init.BaudRate - EffectiveBaudRate;
                if (MinBaudRateError > BaudRateError) { // found better setting
                    MinBaudRateError = BaudRateError;
                    Best_UARTPER16  = UARTPER16;
                    Best_UARTFRAC16 = UARTFRAC16;
                }
                if (LastRateError < BaudRateError)
                    break; // results won't get better
                LastRateError = BaudRateError;
            }
        }

        // need to use local variable as intermediate to ensure 32 bit access and avoid bus fault
        t_register_stm32w1xx_sc1_uartper   UARTPER  = register_stm32w1xx_SC1.UARTPER;
        t_register_stm32w1xx_sc1_uartfrac  UARTFRAC = register_stm32w1xx_SC1.UARTFRAC;
        UARTPER.bSC_UARTPER   = Best_UARTPER16;
        UARTFRAC.bSC_UARTFRAC = Best_UARTFRAC16;
        register_stm32w1xx_SC1.UARTPER  = UARTPER;
        register_stm32w1xx_SC1.UARTFRAC = UARTFRAC;
    }

    t_register_stm32w1xx_sc1_uartcfg UARTCFG = register_stm32w1xx_SC1.UARTCFG;
    if (1) { // Hardware Flow Control
        if (Flags.Bits.Cts)
            Flags.Bits.RtsCts = 1; // no Cts without Rts
        if (Flags.Bits.RtsCts) {
            Flags.Bits.Cts = 1;
            Flags.Bits.Rts = 1;
        }

        UARTCFG.bSC_UARTAUTO = Flags.Bits.Rts;
        UARTCFG.bSC_UARTFLOW = Flags.Bits.Cts;
    }
    if (1) { // Parity
        if (Flags.Bits.ParityEven) {
            UARTCFG.bSC_UARTPAR = 1;
            UARTCFG.bSC_UARTODD = 0;
        }
        else if (Flags.Bits.ParityOdd) {
            UARTCFG.bSC_UARTPAR = 1;
            UARTCFG.bSC_UARTODD = 1;
        }
        else {
            UARTCFG.bSC_UARTPAR = 0;
        }
    }
    if (1) { // word size
        if (Config->Init.WordLength > 7) UARTCFG.bSC_UART8BIT = 1;
        else                        UARTCFG.bSC_UART8BIT = 0;
    }
    if (1) { // stop bits
        if (Config->Init.HalfStopBits <3) UARTCFG.bSC_UART2STP = 0; // 1 StopBit
        else                         UARTCFG.bSC_UART2STP = 1; // 2 StopBits
    }
    register_stm32w1xx_SC1.UARTCFG = UARTCFG;

    if (1) { // configure Interrupts
        if (Flags.Bits.IrqOnRxNE) {
            Config->Interrupt_RxNE = ttc_interrupt_init(tit_USART_RxNE, 0, _usart_stm32w1xx_rx_isr, Config, NULL, NULL);
            ttc_interrupt_enable_usart(Config->Interrupt_RxNE);
        }
        if (Flags.Bits.IrqOnTxComplete) {
            Config->Interrupt_TxComplete = ttc_interrupt_init(tit_USART_TxComplete, 0, _usart_stm32w1xx_tx_isr, Config, NULL, NULL);
            ttc_interrupt_enable_usart(Config->Interrupt_TxComplete);
        }
        if (Flags.Bits.IrqOnIdle) {
            Config->Interrupt_Idle = ttc_interrupt_init(tit_USART_Idle, 0, _usart_stm32w1xx_tx_isr, Config, NULL, NULL);
            ttc_interrupt_enable_usart(Config->Interrupt_Idle);
        }
        // ToDo: Flags.Bits.IrqOnParityError, Flags.Bits.IrqOnError, Flags.Bits.IrqOnCts

    }
    if (1) { // configure GPIO pins
        if (Flags.Bits.Transmit) {
            Config->LowLevelConfig->PortTxD = TTC_USART1_TX;
            ttc_gpio_init(TTC_USART1_TX, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_min);
        }
        if (Flags.Bits.Receive) {
            Config->LowLevelConfig->PortRxD = TTC_USART1_RX;
            ttc_gpio_init(TTC_USART1_RX, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min);
        }
#ifdef TTC_USART1_RTS
        if (Flags.Bits.Rts) {
            Config->LowLevelConfig->PortRTS= TTC_USART1_RTS;
            ttc_gpio_init(TTC_USART1_RTS, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_min);
        }
#endif
#ifdef TTC_USART1_CTS
        if (Flags.Bits.Cts) {
            Config->LowLevelConfig->PortCTS = TTC_USART1_CTS;
            ttc_gpio_init(TTC_USART1_CTS, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min);
        }
#endif
    }
    if (1) { // Last Step: Activate UART controller
        MODE.bSC_MODE = sc_mode_UART;
        register_stm32w1xx_SC1.MODE = MODE;
    }

    //X Config->Init.Flags.All = Flags.All;  // update Config
    return ec_usart_OK;
}
e_ttc_usart_errorcode usart_stm32w1xx_deinit(t_ttc_usart_config* Config) {
    Assert_USART(Config, ttc_assert_origin_auto); // pointers must not be NULL

    if (1) { // deconfigure Interrupts
        if (Config->Interrupt_RxNE)
            ttc_interrupt_disable_usart(Config->Interrupt_RxNE);

        if (Config->Interrupt_TxComplete)
            ttc_interrupt_disable_usart(Config->Interrupt_TxComplete);

        if (Config->Interrupt_Idle)
            ttc_interrupt_disable_usart(Config->Interrupt_Idle);
    }
    return ec_usart_OK;
}
e_ttc_usart_errorcode usart_stm32w1xx_reset(t_ttc_usart_config* Config) {
    Assert_USART(Config, ttc_assert_origin_auto); // pointers must not be NULL

    Config->Architecture = ta_usart_stm32w1xx;
    if (!Config->LowLevelConfig)
        Config->LowLevelConfig = ttc_heap_alloc( sizeof(t_usart_stm32w1xx_config) );

    ttc_memory_set(Config->LowLevelConfig, 0, sizeof(t_usart_stm32w1xx_config));
    Config->LowLevelConfig->BaseRegister.Control    = &register_stm32w1xx_SC1;
    Config->LowLevelConfig->BaseRegister.Flags      = &register_stm32w1xx_INT_SC1FLAG;
    Config->LowLevelConfig->BaseRegister.Config     = &register_stm32w1xx_INT_SC1CFG;
    Config->LowLevelConfig->BaseRegister.Interrupts = &register_stm32w1xx_SC1_INTMODE;

    return usart_stm32w1xx_load_defaults(Config);

}
e_ttc_usart_errorcode usart_stm32w1xx_load_defaults(t_ttc_usart_config* Config) {
    // assumption: *Config has been zeroed

    Assert_USART(Config != NULL, ttc_assert_origin_auto);
    if (Config->PhysicalIndex >= TTC_USART_MAX_AMOUNT) return ec_usart_DeviceNotFound;

    ttc_memory_set(&( Config->Init.Flags ), 0, sizeof(Config->Init.Flags) ); // reset all flags
    Config->Init.Flags.Receive          = 1;
    Config->Init.Flags.Transmit         = 1;
    Config->Init.Flags.RtsCts           = 1;
    Config->Init.Flags.Rts              = 1;
    Config->Init.Flags.Cts              = 1;
    Config->Init.Flags.IrqOnRxNE        = 1;

    /*{ remaining bits
    Config->Init.Flags.SendBreaks       = 1;
    Config->Init.Flags.ControlParity    = 1;
    Config->Init.Flags.TransmitParity   = 1;
    Config->Init.Flags.LIN              = 0;
    Config->Init.Flags.IrDA             = 0;
    Config->Init.Flags.IrDALowPower     = 0;
    Config->Init.Flags.SmartCardNACK    = 0;
    Config->Init.Flags.RxDMA            = 0;
    Config->Init.Flags.TxDMA            = 0;
    Config->Init.Flags.IrqOnCts         = 0;
    Config->Init.Flags.IrqOnError       = 0;
    Config->Init.Flags.IrqOnIdle        = 0;
    Config->Init.Flags.IrqOnTxComplete  = 0;
    Config->Init.Flags.IrqOnParityError = 0;
    Config->Init.Flags.ParityEven       = 0;
    Config->Init.Flags.ParityOdd        = 0;

    Config->Init.Flags.SingleWire       = 0;
    Config->Init.Flags.SmartCard        = 0;
    Config->Init.Flags.Synchronous      = 0;
    }*/

    Config->Init.WordLength     = 8;
    Config->Init.HalfStopBits   = 2;
    Config->Layout         = 0;
    Config->Init.BaudRate       = 115200;
    Config->Timeout        = 200000;

    return ec_usart_OK;

}
e_ttc_usart_errorcode usart_stm32w1xx_get_features(t_ttc_usart_config* Config) {
//X     if (!su_Initialized) usart_stm32w1xx_reset_all();
    // assumption: *Config has been zeroed

    t_u8 PhysicalIndex = Config->PhysicalIndex;
    Assert_USART(Config != NULL, ttc_assert_origin_auto);
    if (PhysicalIndex >= TTC_USART_MAX_AMOUNT) return ec_usart_DeviceNotFound;

    Config->Init.//X Flags.All= 0; // reset all flags
    Config->Init.Flags.Receive          = 1;
    Config->Init.Flags.Transmit         = 1;
    Config->Init.Flags.TransmitParity   = 1;
    Config->Init.Flags.SendBreaks       = 1;
    Config->Init.Flags.ParityEven       = 1;
    Config->Init.Flags.ParityOdd        = 1;
    Config->Init.Flags.IrqOnRxNE        = 0;
#warning implement Config->Init.Flags.IrqOnRxNE (get IRQs running)
    Config->Init.Flags.RtsCts           = 1;
    Config->Init.Flags.Rts              = 1;
    Config->Init.Flags.Cts              = 1;

    /*{ remaining bits
    Config->Init.Flags.DelayedTransmits = 1;
    Config->Init.Flags.ControlParity    = 0; // (ToDo: implement)
    Config->Init.Flags.SingleWire       = 0; // (ToDo: implement)
    Config->Init.Flags.SmartCard        = 0; // (ToDo: implement)
    Config->Init.Flags.Synchronous      = 0; // (ToDo: implement)
    Config->Init.Flags.LIN              = 0; // (ToDo: implement)
    Config->Init.Flags.IrDA             = 0; // (ToDo: implement)
    Config->Init.Flags.IrDALowPower     = 0; // (ToDo: implement)
    Config->Init.Flags.SmartCardNACK    = 0; // (ToDo: implement)
    Config->Init.Flags.RxDMA            = 0; // (ToDo: implement)
    Config->Init.Flags.TxDMA            = 0; // (ToDo: implement)
    Config->Init.Flags.IrqOnCts         = 0; // (ToDo: implement)
    Config->Init.Flags.IrqOnError       = 0; // (ToDo: implement)
    Config->Init.Flags.IrqOnIdle        = 0; // (ToDo: implement)
    Config->Init.Flags.IrqOnTxComplete  = 0; // (ToDo: implement)
    Config->Init.Flags.IrqOnParityError = 0; // (ToDo: implement)
    }*/

    Config->Init.WordLength      = 9;
    Config->Init.HalfStopBits    = 4;
    Config->Init.BaudRate        = 115200;

    return ec_usart_OK;

}
e_ttc_usart_errorcode usart_stm32w1xx_send_word_blocking(t_ttc_usart_register_tx* Register, const t_u16 Word) {
    Assert_USART(Register != NULL, ttc_assert_origin_auto);

    return _usart_stm32w1xx_send_word_blocking(Register, Word);

}
e_ttc_usart_errorcode usart_stm32w1xx_read_byte_blocking(t_ttc_usart_register_rx* Register, t_u8* Byte) {
    Assert_USART(Register, ttc_assert_origin_auto); // pointers must not be NULL
    Assert_USART(ttc_memory_is_writable(Byte), ttc_assert_origin_auto);

    if ( _usart_stm32w1xx_1_wait_for_RXNE() )
        return ec_usart_TimeOut;

    *Byte = (t_u8) 0xff & *Register;

    //X *Byte= (t_u8) 0xff & ( (USART_TypeDef*) Register )->DR;

    return ec_usart_OK;

}
e_ttc_usart_errorcode usart_stm32w1xx_read_word_blocking(t_ttc_usart_register_rx* Register, t_u16* Word) {
    Assert_USART(Register, ttc_assert_origin_auto);
    if ( _usart_stm32w1xx_1_wait_for_RXNE() )
        return ec_usart_TimeOut;

    *Word = (t_u16) 0x1ff & *Register;
    //X *Word = 0x1ff & ( (USART_TypeDef*) Register )->DR;

    return ec_usart_OK;

}
t_u8 usart_stm32w1xx_get_physicalindex(t_ttc_usart_config* Config) {
    Assert_USART(Config != NULL, ttc_assert_origin_auto);

    return Config->PhysicalIndex;

}
t_ttc_usart_config* usart_stm32w1xx_get_configuration(t_ttc_usart_config* Config) {
    Assert_USART(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (t_ttc_usart_config*) 0;

}

e_ttc_usart_errorcode usart_stm32w1xx_read_byte(t_ttc_usart_register_rx* Register, t_u8* Byte) {
    Assert_USART(Register, ttc_assert_origin_auto); // pointers must not be NULL
    Assert_USART(Byte, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (e_ttc_usart_errorcode) 0;
}
t_u8 usart_stm32w1xx_check_bytes_available(t_ttc_usart_config* Config) {
    Assert_USART(Config, ttc_assert_origin_auto); // pointers must not be NULL

    if (register_stm32w1xx_SC1.UARTSTAT.bSC_UARTRXVAL)
        return 1;
    return (t_u8) 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************

e_ttc_usart_errorcode _usart_stm32w1xx_send_word_blocking(t_ttc_usart_register_tx* Register, const t_u16 Word) {
    Assert_USART(Register != NULL, ttc_assert_origin_auto);

    e_ttc_usart_errorcode  Error = _usart_stm32w1xx_1_wait_for_TXE();
    if (Error != ec_usart_OK)
        return Error;

    usart_stm32w1xx_1_send_byte_isr(Register, Word);
    return ec_usart_OK;

}
e_ttc_usart_errorcode _usart_stm32w1xx_1_wait_for_RXNE() {

    t_base TimeOut = ttc_usart_Timeout;
    if (TimeOut != (t_base) -1) { // wait until word received or timeout
        while ( register_stm32w1xx_SC1.UARTSTAT.bSC_UARTRXVAL == 0) {
            if (TimeOut-- == 0)  return ec_usart_TimeOut;
            ttc_task_yield();
        }
    }
    else {             // wait until word received (no timeout)
        while (register_stm32w1xx_SC1.UARTSTAT.bSC_UARTRXVAL == 0)
            ttc_task_yield();
    }

    return ec_usart_OK;
}
e_ttc_usart_errorcode _usart_stm32w1xx_1_wait_for_TXE() {

    t_base TimeOut = ttc_usart_Timeout;
    if (TimeOut != (t_base) -1) {
        while ( register_stm32w1xx_SC1.UARTSTAT.bSC_UARTTXFREE == 0) {
            if (TimeOut-- == 0)  return ec_usart_TimeOut;
            ttc_task_yield();
        }
    }
    else { // no timeout
        while ( register_stm32w1xx_SC1.UARTSTAT.bSC_UARTTXFREE == 0) {
            ttc_task_yield();
        }
    }

    return ec_usart_OK;

}
void _usart_stm32w1xx_rx_isr(t_physical_index PhysicalIndex, void* Argument) {

/* DEPRECATED
    Assert_USART(PhysicalIndex <= TTC_USART_AMOUNT, ttc_assert_origin_auto);
    Assert_USART(Argument,                          ttc_assert_origin_auto);

    t_u8 Byte = (t_u8) 0xff & ( (USART_TypeDef*) USART_Base)->DR;

    // clear interrupt status flag to allow next interrupt
    if (0)  // more readable
        USART_Base->SR.RXNE = 0;
    else    // faster
        ( (USART_TypeDef*) USART_Base)->SR &= 0xff - (1 << 5);
*/

    (void) PhysicalIndex;
    t_ttc_usart_config* Config = ( t_ttc_usart_config*) Argument;

    // Only one UART on STM32W1xx
    t_u8 Byte = *USART_STM32W1XX_SC1_DATA;
    _ttc_usart_rx_isr(Config, Byte); // provide received byte to registered function

}
void _usart_stm32w1xx_tx_isr(t_physical_index PhysicalIndex, void* Argument) {
    Assert_USART(PhysicalIndex <= TTC_USART_AMOUNT, ttc_assert_origin_auto);

    // forward interrupt to upper driver to send out next byte from queue
    _ttc_usart_tx_isr( (t_ttc_usart_config*) Argument, &(register_stm32w1xx_SC1.DATA) );

}


//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)



//} private functions
