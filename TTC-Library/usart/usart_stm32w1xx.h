#ifndef USART_STM32F1XX_H
#define USART_STM32F1XX_H

/** { usart_stm32w1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for usart devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by high-level usart and application.
 *
 *  Created from template device_architecture.h revision 21 at 20140217 09:36:59 UTC
 *
 *  Note: See ttc_usart.h for description of stm32w1xx independent USART implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

// register address for data transmit/ receive
#define USART_STM32W1XX_SC1_DATA ((t_u16*) 0x4000c83c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_USART_STM32W1XX
//
// Implementation status of low-level driver support for usart devices on stm32w1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_USART_STM32W1XX 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_USART_STM32W1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_USART_STM32W1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_USART_STM32W1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "usart_stm32w1xx_types.h"
#include "../ttc_basic_types.h"
#include "../ttc_usart_types.h"
#include "../ttc_task.h"
#include "../ttc_interrupt.h"
#include "../ttc_gpio.h"
#include "../ttc_heap.h"
#include "../ttc_memory.h"
#include "../ttc_sysclock.h"

#define USE_STDPERIPH_DRIVER
#ifdef EXTENSION_255_stm32w1xx_standard_peripherals
    #include "stm32w108xx.h"     // must be included BEFORE core_cm3.h
    #include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
    #include "stm32w108xx_sc.h"
#endif
//X #include "stm32f10x_conf.h"
//? #include "misc.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_usart_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_<device>_prepare
//
#define ttc_driver_usart_deinit(Config)                     usart_stm32w1xx_deinit(Config)
#define ttc_driver_usart_get_features(Config)               usart_stm32w1xx_get_features(Config)
#define ttc_driver_usart_init(Config)                       usart_stm32w1xx_init(Config)
#define ttc_driver_usart_load_defaults(Config)              usart_stm32w1xx_load_defaults(Config)
#define ttc_driver_usart_prepare()                          usart_stm32w1xx_prepare()
#define ttc_driver_usart_reset(Config)                      usart_stm32w1xx_reset(Config)
#define ttc_driver_usart_get_configuration(Config)          usart_stm32w1xx_get_configuration(Config)
#define ttc_driver_usart_read_word_blocking(Register, Word) usart_stm32w1xx_read_word_blocking(Register, Word)
#define ttc_driver_usart_send_word_blocking(Register, Word) usart_stm32w1xx_send_word_blocking(Register, Word)
#define ttc_driver_usart_send_byte_isr(Register, Word)      usart_stm32w1xx_1_send_byte_isr(Register, Word)
#define ttc_driver_usart_read_byte(Register, Byte)          usart_stm32w1xx_read_byte_blocking(Register, Byte)
#define ttc_driver_usart_check_bytes_available(Config) usart_stm32w1xx_check_bytes_available(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_usart.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_usart.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** shutdown single USART unit device
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for PhysicalIndex)
 * @return              == 0: USART has been shutdown successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32w1xx_deinit( t_ttc_usart_config* Config );

/** fills out given Config with maximum valid values for indexed USART
 * @param Config        = pointer to struct t_ttc_usart_config (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32w1xx_get_features( t_ttc_usart_config* Config );

/** initializes single USART unit for operation
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for PhysicalIndex)
 * @return              == 0: USART has been initialized successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32w1xx_init( t_ttc_usart_config* Config );

/** loads configuration of indexed USART unit with default values
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_usart_errorcode usart_stm32w1xx_load_defaults( t_ttc_usart_config* Config );

/** Prepares usart driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void usart_stm32w1xx_prepare();

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was reset successfully
 */
e_ttc_usart_errorcode usart_stm32w1xx_reset( t_ttc_usart_config* Config );

/** Send out given data word (8 or 9 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param Register       pointer to hardware register for fast access
 * @param Byte           8 or 9 bit value to send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32w1xx_send_word_blocking( t_ttc_usart_register_tx* Register, const t_u16 Word );

/** Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param USART_Generic  struct t_ttc_usart_config initialized by _driver_usart_init()
 * @param Register       pointer to hardware register for fast access
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32w1xx_read_word_blocking( t_ttc_usart_register_rx* Register, t_u16* Word );

/** Reads single data byte from input buffer.
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param USART_Generic  struct t_ttc_usart_config initialized by _driver_usart_init()
 * @param Register       pointer to hardware register for fast access
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32w1xx_read_byte_blocking( t_ttc_usart_register_rx* Register, t_u8* Byte );

/** Translates from logical index of functional unit to real hardware index (0=USART1, 1=USART2, ...)
 * Logical indices are defined by board makefiles.
 * E.g. #define TTC_USART1=USART3   declares third real hardware unit as first logical unit
 *
 * @param USART_Generic  struct t_ttc_usart_config initialized by _driver_usart_init()
 * @return               real device index of USART to init (0..MAX)
 */
t_u8 usart_stm32w1xx_get_physicalindex( t_ttc_usart_config* USART_Generic );

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_USART_get_max_LogicalIndex())
 * @return                configuration of indexed device
 * @param Config          loaded with pointer to device configuration
 */
t_ttc_usart_config* usart_stm32w1xx_get_configuration( t_ttc_usart_config* Config );

/** low level send routine for USART1 especially to be called from _ttc_usart_tx_isr()
 *
 * @param Register      pointer to low-level register for fast hardware operation
 * @param Word          data word to send (8 or 9 bits)
 */
//?void usart_stm32w1xx_1_send_byte_isr(t_ttc_usart_register_tx* Register, const t_u16 Word);
//#define usart_stm32w1xx_1_send_byte_isr(Word) *( (t_u16*) (void*) &( Register->DR) ) = Word
#define usart_stm32w1xx_1_send_byte_isr(Register, Word) ( *( (volatile t_u32*) Register) = (Word) )
//X void usart_stm32w1xx_1_send_byte_isr(t_base* Register, t_u16 Word);




/** Reads single data byte from input buffer (8 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param Register      pointer to low-level register for fast hardware operation
 * @param Byte          pointer to 8 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32w1xx_read_byte( t_ttc_usart_register_rx* Register, t_u8* Byte );


/** returns amount of bytes being held in receive buffer
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @return               == 0: no bytes available; >0: amount of bytes available
 * @param Config   =
 */
t_u8 usart_stm32w1xx_check_bytes_available( t_ttc_usart_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _usart_stm32w1xx_foo(t_ttc_usart_config* Config)

/** Sends out given byte.
 * @param Register       pointer to hardware register for fast access
 * @param Word           pointer to 16 bit buffer where to store Word
 *
 * Note: This low-level function should not be called from outside!
 */
e_ttc_usart_errorcode _usart_stm32w1xx_send_word_blocking( t_ttc_usart_register_tx* Register, const t_u16 Word );

/** Wait until word has been received or timeout occured in UART1
 * Note: This low-level function is private and should not be called from outside!
 */
e_ttc_usart_errorcode _usart_stm32w1xx_1_wait_for_RXNE();

/** wait until transmit buffer is empty in UART1
 * Note: This low-level function is private and should not be called from outside!
 */
e_ttc_usart_errorcode _usart_stm32w1xx_1_wait_for_TXE();

/** general interrupt handler for received byte; reads received byte and pass it to receiveSingleByte()
 * @param PhysicalIndex    0..TTC_USART_AMOUNT-1 -pointer to hardware register for fast access (0=USART1, ...)
 * @param USART_Generic    pointer to configuration of corresponding USART
 */
void _usart_stm32w1xx_rx_isr( t_physical_index PhysicalIndex, void* Argument );

/** general interrupt handler for that will call _ttc_usart_tx_isr() to provide next byte to send
 * @param PhysicalIndex    0..TTC_USART_AMOUNT-1 -pointer to hardware register for fast access (0=USART1, ...)
 * @param USART_Generic    pointer to configuration of corresponding USART
 */
void _usart_stm32w1xx_tx_isr( t_physical_index PhysicalIndex, void* Argument );

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //USART_STM32F1XX_H