#ifndef USART_STM32L1XX_TYPES_H
#define USART_STM32L1XX_TYPES_H

/** { usart_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for USART devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_usart_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140423 01:03:12 UTC
 *
 *  Note: See ttc_usart.h for description of architecture independent USART implementation.
 *
 *  Authors: Greg Knoll 2014.
 *
}*/


//{ Defines/ TypeDefs **********************************************************

#define TTC_USART_MAX_AMOUNT 3

#define _driver_usart_rx_isr                    _usart_stm32l1xx_rx_isr // define function pointer for receive interrupt service routine
#define _driver_usart_tx_isr                    _usart_stm32l1xx_tx_isr // define function pointer for transmit interrupt service routine
#define t_ttc_usart_register                    t_register_stm32l1xx_usart
#define t_ttc_usart_register_tx                 t_register_stm32l1xx_usart_dr
#define t_ttc_usart_register_rx                 t_register_stm32l1xx_usart_dr

#define t_ttc_interrupt_usart_errors t_stm32l1xx_interrupt_usart_errors
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)
typedef struct { // t_ttc_interrupt_usart_errors - support for ttc_interrupt_types.h
    unsigned Overrun  : 1;
    unsigned Noise    : 1;
    unsigned Framing  : 1;
    unsigned Parity   : 1;
    unsigned reserved : 4;

} t_stm32l1xx_interrupt_usart_errors;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines



//{ Includes *******************************************************************

#include "../ttc_basic.h"
#include "../gpio/gpio_stm32l1xx.h"
#include "../register/register_stm32l1xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//} Includes


//{ Structures/ Enums required by ttc_usart_types.h *************************
#if 1==0
typedef struct {  // stm32l1xx specific configuration data of single USART device
    /* ALREADY DEFINED IN t_ttc_usart_config !
      t_register_stm32l1xx_usart* BaseRegister;       // base address of USART device registers
      t_u32 BaudRate;
      t_u16 WordLength;
      t_u16 StopBits;
      t_u16 Parity;
      t_u16 Mode;
      t_u16 HardwareFlowControl;
    */
} __attribute__( ( __packed__ ) ) t_usart_stm32l1xx_config;

// t_ttc_usart_architecture is required by ttc_usart_types.h
#define t_ttc_usart_architecture t_usart_stm32l1xx_config
#else
#define t_ttc_usart_architecture t_u8 // unused

#endif

//} Structures/ Enums


#endif //USART_STM32L1XX_TYPES_H
