/** { usart_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for usart devices on stm32l1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 21 at 20140423 01:03:12 UTC
 *
 *  Note: See ttc_usart.h for description of stm32l1xx independent USART implementation.
 *
 *  Authors: Greg Knoll, Gregor Rebel.
}*/

#include "usart_stm32l1xx.h"
#include "../ttc_sysclock.h"

//{ Global Variables ***********************************************************

extern void _ttc_usart_rx_isr( t_ttc_usart_config* Config, t_u8 Byte );
extern BOOL _ttc_usart_tx_isr( t_ttc_usart_config* Config, volatile void* USART_Hardware );
extern t_base ttc_usart_Timeout;

//}Global Variables
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _usart_stm32f1xx_foo(t_ttc_usart_config* Config)

/** Sends out given byte.
 * @param MyUSART        USART device to use
 * @param Word           pointer to 16 bit buffer where to store Word

 *
 * Note: This low-level function should not be called from outside!
 */
e_ttc_usart_errorcode _usart_stm32l1xx_send_word_blocking( volatile t_register_stm32l1xx_usart* MyUSART, const t_u16 Word );


/** Wait until word has been received or timeout occured.
 * Note: This low-level function is private and should not be called from outside!
 *
 * @param MyUSART        USART device to use

 */
e_ttc_usart_errorcode _usart_stm32l1xx_wait_for_RXNE( volatile t_register_stm32l1xx_usart* MyUSART );

/** wait until transmit buffer is empty
 * Note: This low-level function is private and should not be called from outside!
 *
 * @param MyUSART        USART device to use

 */
e_ttc_usart_errorcode _usart_stm32l1xx_wait_for_TXE( volatile t_register_stm32l1xx_usart* MyUSART );

/** calculate baudrate for given variables
 *
 * @param ClockAPB     PCLK1 or PCLK2 as returned by ttc_syclock_get_configuration()
 * @param Mantissa     content of register BRR->Bits.DIV_Mantissa
 * @param Fractional   content of register BRR->Bits.DIV_Fraction
 * @param StandardMode = (BaseRegister->CR2.Bits.LINEN || BaseRegister->CR3.Bits.SC_EN || BaseRegister->CR3.Bits.IREN) ? 0 : 1;
 * @return             baudrate calculated as described in RM0038 p.624
 */
t_base _usart_stm32l1xx_calculate_baudrate( t_base ClockAPB, t_u16 Mantissa, t_u16 Fractional, BOOL Over8, BOOL StandardMode );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function Definitions *******************************************************

e_ttc_usart_errorcode usart_stm32l1xx_deinit( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    // deinit all configured pins
    if ( Config->PortCLK != E_ttc_gpio_pin_none )
    { ttc_gpio_init( Config->PortCLK, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min ); }
    if ( Config->PortRTS != E_ttc_gpio_pin_none )
    { ttc_gpio_init( Config->PortRTS, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min ); }
    if ( Config->PortCTS != E_ttc_gpio_pin_none )
    { ttc_gpio_init( Config->PortCTS, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min ); }
    if ( Config->PortTxD != E_ttc_gpio_pin_none )
    { ttc_gpio_init( Config->PortTxD, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min ); }
    if ( Config->PortRxD != E_ttc_gpio_pin_none )
    { ttc_gpio_init( Config->PortRxD, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min ); }

    switch ( Config->PhysicalIndex ) {
        case 0: { // USART1
            register_stm32l1xx_RCC.APB2ENR.Bits.USART1EN = 0;
            break;
        }
        case 1: { // USART2
            register_stm32l1xx_RCC.APB1ENR.Bits.USART2_EN = 0;
            break;
        }
        case 2: { // USART3
            register_stm32l1xx_RCC.APB1ENR.Bits.USART3_EN = 0;
            break;
        }
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // Illegal PhysicalIndex!
    }

    /* DEPRECATED
    if (USARTx == (t_register_stm32l1xx_usart*)USART1)
    {
      sysclock_stm32l1xx_RCC_APB2PeriphResetCmd(RCC_APB2Periph_USART1, ENABLE);
      sysclock_stm32l1xx_RCC_APB2PeriphResetCmd(RCC_APB2Periph_USART1, DISABLE);
    }
    else if (USARTx == (t_register_stm32l1xx_usart*)USART2)
    {
      sysclock_stm32l1xx_RCC_APB1PeriphResetCmd(RCC_APB1Periph_USART2, ENABLE);
      sysclock_stm32l1xx_RCC_APB1PeriphResetCmd(RCC_APB1Periph_USART2, DISABLE);
    }
    else
    {
      if (USARTx == (t_register_stm32l1xx_usart*)USART3)
      {
        sysclock_stm32l1xx_RCC_APB1PeriphResetCmd(RCC_APB1Periph_USART3, ENABLE);
        sysclock_stm32l1xx_RCC_APB1PeriphResetCmd(RCC_APB1Periph_USART3, DISABLE);
      }
    }
    */

    return ( e_ttc_usart_errorcode ) 0;
}
void                  usart_stm32l1xx_prepare() {

}
e_ttc_usart_errorcode usart_stm32l1xx_reset( t_ttc_usart_config* Config ) {

    Assert_USART_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    return usart_stm32l1xx_load_defaults( Config );
}
e_ttc_usart_errorcode usart_stm32l1xx_load_defaults( t_ttc_usart_config* Config ) {
    // assumption: *Config has been zeroed

    Assert_USART_Writable( Config, ttc_assert_origin_auto );
    if ( Config->PhysicalIndex >= TTC_USART_MAX_AMOUNT ) { return ec_usart_DeviceNotFound; }

    switch ( Config->PhysicalIndex ) {
        case 0: Config->BaseRegister = & register_stm32l1xx_USART1; break;
        case 1: Config->BaseRegister = & register_stm32l1xx_USART2; break;
        case 2: Config->BaseRegister = & register_stm32l1xx_USART3; break;
        case 4: Config->BaseRegister = & register_stm32l1xx_UART4;  break;
        case 5: Config->BaseRegister = & register_stm32l1xx_UART5;  break;
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unkown physical index!
    }

    ttc_memory_set( &( Config->Init.Flags ), 0, sizeof( Config->Init.Flags ) );
    Config->Init.Flags.SendBreaks       = 1;
    Config->Init.Flags.ControlParity    = 1;
    Config->Init.Flags.Receive          = 1;
    Config->Init.Flags.Transmit         = 1;
    Config->Init.Flags.TransmitParity   = 1;
    Config->Init.Flags.DelayedTransmits = 1;
    Config->Init.Flags.IrqOnCts         = 1;
    Config->Init.Flags.IrqOnTxComplete  = 1;
    Config->Init.Flags.IrqOnRxNE        = 1;

    /*{ remaining bits
    Config->Init.Flags.LIN              = 1;
    Config->Init.Flags.IrDA             = 1;
    Config->Init.Flags.IrDALowPower     = 1;
    Config->Init.Flags.SmartCardNACK    = 1;
    Config->Init.Flags.RxDMA            = 1;
    Config->Init.Flags.TxDMA            = 1;
    Config->Init.Flags.Rts              = 1;
    Config->Init.Flags.IrqOnError       = 1;
    Config->Init.Flags.IrqOnIdle        = 1;
    Config->Init.Flags.IrqOnParityError = 1;
    Config->Init.Flags.ParityEven       = 1;
    Config->Init.Flags.ParityOdd        = 1;

    Config->Init.Flags.RtsCts           = 1;
    Config->Init.Flags.SingleWire       = 1;
    Config->Init.Flags.SmartCard        = 1;
    Config->Init.Flags.Synchronous      = 1;
    }*/


    // 115200 bit/s 8n1
    Config->Init.WordLength     = 8;
    Config->Init.HalfStopBits   = 2;
    Config->Init.BaudRate       = 115200;

    return ec_usart_OK;
}
e_ttc_usart_errorcode usart_stm32l1xx_get_features( t_ttc_usart_config* Config ) {
    //X     if (!su_Initialized) usart_stm32f1xx_reset_all();
    // assumption: *Config has been zeroed

    t_u8 PhysicalIndex = Config->PhysicalIndex;
    Assert_USART_Writable( Config, ttc_assert_origin_auto );
    if ( PhysicalIndex >= TTC_USART_MAX_AMOUNT ) { return ec_usart_DeviceNotFound; }

    ttc_memory_set( &( Config->Init.Flags ), 0, sizeof( Config->Init.Flags ) );
    Config->Init.Flags.Receive          = 1;
    Config->Init.Flags.Transmit         = 1;
    Config->Init.Flags.TransmitParity   = 1;
    Config->Init.Flags.DelayedTransmits = 1;
    Config->Init.Flags.SendBreaks       = 1;
    Config->Init.Flags.ParityEven       = 1;
    Config->Init.Flags.ParityOdd        = 1;
    Config->Init.Flags.IrqOnRxNE        = 1;
    Config->Init.Flags.ControlParity    = 1;
    Config->Init.Flags.RtsCts           = 1;
    Config->Init.Flags.SingleWire       = 1;
    Config->Init.Flags.SmartCard        = 1;
    Config->Init.Flags.Synchronous      = 1;
    Config->Init.Flags.LIN              = 1;
    Config->Init.Flags.IrDA             = 1;
    Config->Init.Flags.IrDALowPower     = 1;
    Config->Init.Flags.SmartCardNACK    = 1;
    Config->Init.Flags.RxDMA            = 1;
    Config->Init.Flags.TxDMA            = 1;
    Config->Init.Flags.Rts              = 1;
    Config->Init.Flags.IrqOnCts         = 1;
    Config->Init.Flags.IrqOnError       = 1;
    Config->Init.Flags.IrqOnIdle        = 1;
    Config->Init.Flags.IrqOnTxComplete  = 1;
    Config->Init.Flags.IrqOnParityError = 1;

    /*{ remaining bits (ToDo: implement)
    }*/

    Config->Init.WordLength      = 9;
    Config->Init.HalfStopBits    = 2;
    Config->Init.BaudRate        = 4000000;

    return ec_usart_OK;
}
e_ttc_usart_errorcode usart_stm32l1xx_init( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto );
    Assert_USART( ( Config->Init.WordLength   >= 8 ) && ( Config->Init.WordLength   <= 9 ), ttc_assert_origin_auto );
    Assert_USART( ( Config->Init.HalfStopBits >= 1 ) && ( Config->Init.HalfStopBits <= 4 ), ttc_assert_origin_auto );

    if ( 1 ) {                                 // validate USART_Features
        t_ttc_usart_config USART_Features;
        ttc_memory_set( &USART_Features, 0, sizeof( USART_Features ) );
        USART_Features.LogicalIndex  = Config->LogicalIndex;
        USART_Features.PhysicalIndex = Config->PhysicalIndex;
        e_ttc_usart_errorcode Error = usart_stm32l1xx_get_features( &USART_Features );
        if ( Error ) { return Error; }
        //X Config->Init.Flags.All &= USART_Features.Flags.All; // mask out unavailable flags
        Assert_USART_EXTRA( sizeof( Config->Init.Flags ) == 4, ttc_assert_origin_auto ); // size of bitfield must exactly match typecast below. Change code to fit!
        // mask out unavailable flags
        *( ( t_u32* ) & ( Config->Init.Flags ) ) &= *( ( t_u32* ) & ( USART_Features.Init.Flags ) );
    }
    if ( 1 ) {                                 // find indexed USART on current board
        switch ( Config->PhysicalIndex ) {     // find corresponding USART as defined by makefile.100_board_*
            case 0:
                Config->BaseRegister     = ( t_register_stm32l1xx_usart* ) USART1;
                break;
            case 1:
                Config->BaseRegister     = ( t_register_stm32l1xx_usart* ) USART2;
                break;
            case 2:
                Config->BaseRegister     = ( t_register_stm32l1xx_usart* ) USART3;
                break;
            default:
                Assert_USART( 0, ttc_assert_origin_auto ); break;  // No TTC_USARTn defined! Check your makefile.100_board_* file!
        }
    }
    if ( 1 ) {                                 // determine pin layout (some pins can be remapped to other pins)
        // Compare with TheToolChain/Documentation/uC/STM32L1xx/STM32L100xx-Datasheet.pdf p.36

        Config->Layout = 0; // not yet determined
        switch ( ( t_u32 )  Config->BaseRegister ) { // determine pin layout

            case ( t_u32 )  USART1: // USART1
                if ( ( Config->PortTxD == E_ttc_gpio_pin_a9 ) || ( Config->PortRxD == E_ttc_gpio_pin_a10 ) )
                { Config->Layout = 1; }

                if ( ( Config->PortTxD == E_ttc_gpio_pin_b6 ) || ( Config->PortRxD == E_ttc_gpio_pin_b7 ) )
                { Config->Layout = 2; }

                // check if all configured pins match to detected layout
                if ( Config->Layout == 1 ) {
                    Assert_USART( ( Config->PortTxD == E_ttc_gpio_pin_none ) || ( Config->PortTxD == E_ttc_gpio_pin_a9 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                    Assert_USART( ( Config->PortRxD == E_ttc_gpio_pin_none ) || ( Config->PortRxD == E_ttc_gpio_pin_a10 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                }
                else if ( Config->Layout == 2 ) {
                    Assert_USART( ( Config->PortTxD == E_ttc_gpio_pin_none ) || ( Config->PortTxD == E_ttc_gpio_pin_b6 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                    Assert_USART( ( Config->PortRxD == E_ttc_gpio_pin_none ) || ( Config->PortRxD == E_ttc_gpio_pin_b7 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                }
                Assert_USART( ( Config->PortCLK == E_ttc_gpio_pin_none ) || ( Config->PortCLK == E_ttc_gpio_pin_a8 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                Assert_USART( ( Config->PortCTS == E_ttc_gpio_pin_none ) || ( Config->PortCTS == E_ttc_gpio_pin_a11 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                Assert_USART( ( Config->PortRTS == E_ttc_gpio_pin_none ) || ( Config->PortRTS == E_ttc_gpio_pin_a12 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                break;

            case ( t_u32 )  USART2: // USART2

                Config->Layout = 1; // only one layout available for USART2 (->STM32L100xx-Datasheet.pdf p.36)

                // check if all configured pins match to detected layout
                Assert_USART( ( Config->PortCTS == E_ttc_gpio_pin_none ) || ( Config->PortCTS == E_ttc_gpio_pin_a0 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                Assert_USART( ( Config->PortRTS == E_ttc_gpio_pin_none ) || ( Config->PortRTS == E_ttc_gpio_pin_a1 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                Assert_USART( ( Config->PortTxD == E_ttc_gpio_pin_none ) || ( Config->PortTxD == E_ttc_gpio_pin_a2 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                Assert_USART( ( Config->PortRxD == E_ttc_gpio_pin_none ) || ( Config->PortRxD == E_ttc_gpio_pin_a3 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                Assert_USART( ( Config->PortCLK == E_ttc_gpio_pin_none ) || ( Config->PortCLK == E_ttc_gpio_pin_a4 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                break;

            case ( t_u32 )  USART3: // USART3
                if ( ( Config->PortTxD == E_ttc_gpio_pin_b10 ) || ( Config->PortRxD == E_ttc_gpio_pin_b11 )  || ( Config->PortCLK == E_ttc_gpio_pin_b12 ) )
                { Config->Layout = 1; }

                if ( ( Config->PortTxD == E_ttc_gpio_pin_c10 ) || ( Config->PortRxD == E_ttc_gpio_pin_c11 )  || ( Config->PortCLK == E_ttc_gpio_pin_c12 ) )
                { Config->Layout = 2; }

                // check if all configured pins match to detected layout
                if ( Config->Layout == 1 ) {
                    Assert_USART( ( Config->PortTxD == E_ttc_gpio_pin_none ) || ( Config->PortTxD == E_ttc_gpio_pin_b10 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                    Assert_USART( ( Config->PortRxD == E_ttc_gpio_pin_none ) || ( Config->PortRxD == E_ttc_gpio_pin_b11 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                    Assert_USART( ( Config->PortCLK == E_ttc_gpio_pin_none ) || ( Config->PortCLK == E_ttc_gpio_pin_b11 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                }
                else if ( Config->Layout == 2 ) {
                    Assert_USART( ( Config->PortTxD == E_ttc_gpio_pin_none ) || ( Config->PortTxD == E_ttc_gpio_pin_c10 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                    Assert_USART( ( Config->PortRxD == E_ttc_gpio_pin_none ) || ( Config->PortRxD == E_ttc_gpio_pin_c11 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                    Assert_USART( ( Config->PortCLK == E_ttc_gpio_pin_none ) || ( Config->PortCLK == E_ttc_gpio_pin_c12 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                }
                Assert_USART( ( Config->PortCTS == E_ttc_gpio_pin_none ) || ( Config->PortCTS == E_ttc_gpio_pin_b13 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                Assert_USART( ( Config->PortRTS == E_ttc_gpio_pin_none ) || ( Config->PortRTS == E_ttc_gpio_pin_b14 ), ttc_assert_origin_auto );  // This Pin is not available in this layout! (->STM32L100xx-Datasheet.pdf p.36)
                break;

            default: { Assert_USART( 0, ttc_assert_origin_auto ); }
        }
    }
    if ( 1 ) {                                 // setup all configured gpio pins + enable USART clock
        Assert_USART( Config->Layout, ttc_assert_origin_auto );  // could not determine pin layout: check your configuration!
        t_u8 AlternateFunction = 0;
        switch ( Config->PhysicalIndex ) {
            case 0: { // USART1
                //X RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
                register_stm32l1xx_RCC.APB2ENR.Bits.USART1EN = 1;
                AlternateFunction = GPIO_AF_USART1;
                break;
            }
            case 1: { // USART2
                //X RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
                register_stm32l1xx_RCC.APB1ENR.Bits.USART2_EN = 1;
                AlternateFunction = GPIO_AF_USART2;
                break;
            }
            case 2: { // USART3
                //X RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
                register_stm32l1xx_RCC.APB1ENR.Bits.USART3_EN = 1;
                AlternateFunction = GPIO_AF_USART3;
                break;
            }
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // Illegal PhysicalIndex!
        }

        if ( Config->PortCLK != E_ttc_gpio_pin_none ) {
            ttc_gpio_init( Config->PortCLK, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max );
            ttc_gpio_alternate_function( Config->PortCLK, AlternateFunction );
        }
        if ( Config->PortTxD != E_ttc_gpio_pin_none ) {
            ttc_gpio_init( Config->PortTxD, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max );
            ttc_gpio_alternate_function( Config->PortTxD, AlternateFunction );
        }
        if ( Config->PortRxD != E_ttc_gpio_pin_none ) {
            ttc_gpio_init( Config->PortRxD, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max );
            ttc_gpio_alternate_function( Config->PortRxD, AlternateFunction );
        }
        if ( Config->PortRTS != E_ttc_gpio_pin_none ) {
            ttc_gpio_init( Config->PortRTS, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max );
            ttc_gpio_alternate_function( Config->PortRTS, AlternateFunction );
        }
        if ( Config->PortCTS != E_ttc_gpio_pin_none ) {
            ttc_gpio_init( Config->PortCTS, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max );
            ttc_gpio_alternate_function( Config->PortCTS, AlternateFunction );
        }
    }
    if ( 1 ) {                                 // init USART
        if ( 1 ) { // init USART via register structs (-> RM0038 p.654)
            // disable USART before reconfiguring it
            Config->BaseRegister->CR1.Bits.UE = 0;
            Config->BaseRegister->CR1.Bits.TE = 0; // must be disabled before changing some fields

            t_register_stm32l1xx_usart_cr1 CR1; CR1.All = Config->BaseRegister->CR1.All;
            t_register_stm32l1xx_usart_cr2 CR2; CR2.All = Config->BaseRegister->CR2.All;
            t_register_stm32l1xx_usart_cr3 CR3; CR3.All = Config->BaseRegister->CR3.All;

            CR1.Bits.UE = 0;

            if ( Config->Init.WordLength == 9 ) { CR1.Bits.M = 1; }
            else                           { CR1.Bits.M = 0; }

            CR1.Bits.PCE    = Config->Init.Flags.ControlParity;
            CR1.Bits.PS     = Config->Init.Flags.ParityOdd;
            CR1.Bits.TXEIE  = Config->Init.Flags.IrqOnTxComplete;
            CR1.Bits.TCIE   = Config->Init.Flags.IrqOnTxComplete;
            CR1.Bits.RXNEIE = Config->Init.Flags.IrqOnRxNE;
            CR1.Bits.IDLEIE = Config->Init.Flags.IrqOnIdle;
            CR1.Bits.TE     = Config->Init.Flags.Transmit;
            CR1.Bits.RE     = Config->Init.Flags.Receive;

            CR2.Bits.CLKEN  = Config->Init.Flags.Synchronous;
            CR2.Bits.LINEN  = Config->Init.Flags.LIN;
            switch ( Config->Init.HalfStopBits ) {
                case 1: CR2.Bits.STOP   = 0b01; break; // 0.5 Stop Bits
                case 2: CR2.Bits.STOP   = 0b00; break; // 1   Stop Bit
                case 3: CR2.Bits.STOP   = 0b11; break; // 1.5 Stop Bits
                case 4: CR2.Bits.STOP   = 0b10; break; // 2   Stop Bits
                default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid value, check your code!
            }

            CR3.Bits.CTSIE  = Config->Init.Flags.IrqOnCts;
            CR3.Bits.CTSE   = Config->Init.Flags.Cts;
            CR3.Bits.RTSE   = Config->Init.Flags.Rts;
            CR3.Bits.DMAR   = Config->Init.Flags.RxDMA;
            CR3.Bits.DMAT   = Config->Init.Flags.TxDMA;
            CR3.Bits.SC_EN   = Config->Init.Flags.SmartCard;
            CR3.Bits.NACK   = Config->Init.Flags.SmartCardNACK;
            CR3.Bits.HDSEL  = Config->Init.Flags.HalfDuplex;
            CR3.Bits.IRLP   = Config->Init.Flags.IrDALowPower;
            CR3.Bits.IREN   = Config->Init.Flags.IrDA;
            CR3.Bits.EIE    = Config->Init.Flags.IrqOnError;

            Config->BaseRegister->CR1.All = CR1.All;
            Config->BaseRegister->CR2.All = CR2.All;
            Config->BaseRegister->CR3.All = CR3.All;

            usart_stm32l1xx_configure_baudrate( Config );
            CR1.Bits.UE = 1;      // enable USART
            Config->BaseRegister->CR1.All = CR1.All;
        }
        else {
            /* DEPRECATED Using Standard Peripheral Style is deprecated (kept as a comparison)

                t_u32 tmpreg = 0x00;
                volatile t_register_stm32l1xx_usart* Register = Config->BaseRegister;

                //---------------------------- USART CR2 Configuration -----------------------
                tmpreg = Register->CR2.All;

                // Clear STOP[13:12] bits
                tmpreg &= ( t_u32 ) ~( ( t_u32 )  USART_CR2_STOP );

                // Configure the USART Stop Bits, Clock, CPOL, CPHA and LastBit ------------
                // Set STOP[13:12] bits according to USART_StopBits value
                switch ( Config->Init.HalfStopBits ) {
                    case 1: tmpreg |= ( t_u32 )  USART_StopBits_0_5; break;
                    case 3: tmpreg |= ( t_u32 )  USART_StopBits_1_5; break;
                    case 4: tmpreg |= ( t_u32 )  USART_StopBits_2;   break;
                    default:
                    case 2: tmpreg |= ( t_u32 )  USART_StopBits_1;   break;
                }

                // Enable clk pin
                tmpreg |= ( t_u32 ) 0x00000800;

                // Write to USART CR2
                Register->CR2.All = ( t_u16 ) tmpreg;

                //---------------------------- USART CR1 Configuration -----------------------
                tmpreg = Register->CR1.All;
                // Clear M, PCE, PS, TE and RE bits
                tmpreg &= ( t_u32 ) ~( ( t_u32 ) CR1_CLEAR_MASK );

                // Configure the USART Word Length, Parity and mode -----------------------
                // Set the M bits according to USART_WordLength value
                // Set PCE and PS bits according to USART_Parity value
                // Set TE and RE bits according to USART_Mode value

                if ( Config->Init.WordLength != 9 ) { // 8 bits
                    Config->Init.WordLength = 8;
                    tmpreg |= ( t_u32 ) USART_WordLength_8b;
                }
                else {                         // 9 bits
                    tmpreg |= ( t_u32 ) USART_WordLength_9b;
                }


                if ( Config->Init.Flags.ParityEven )
                { tmpreg |= ( t_u32 ) USART_Parity_Even; }
                if ( Config->Init.Flags.ParityOdd )
                { tmpreg |= ( t_u32 ) USART_Parity_Odd; }

                if ( Config->Init.Flags.Transmit )
                { tmpreg |= ( t_u32 ) USART_Mode_Tx; }
                if ( Config->Init.Flags.Receive )
                { tmpreg |= ( t_u32 ) USART_Mode_Rx; }

                // Write to USART CR1
                Register->CR1.All = ( t_u16 ) tmpreg;

                //---------------------------- USART CR3 Configuration -----------------------
                tmpreg = Register->CR3.All;

                // Clear CTSE and RTSE bits
                tmpreg &= ( t_u32 ) ~( ( t_u32 ) CR3_CLEAR_MASK );

                // Configure the USART HFC -------------------------------------------------
                // Set CTSE and RTSE bits according to USART_HardwareFlowControl value
                if ( Config->Init.Flags.Rts )
                { tmpreg |= USART_HardwareFlowControl_RTS; }
                if ( Config->Init.Flags.Cts )
                { tmpreg |= USART_HardwareFlowControl_CTS; }
                if ( Config->Init.Flags.RtsCts )
                { tmpreg |= USART_HardwareFlowControl_RTS_CTS; }

                // Write to USART CR3
                Register->CR3.All = ( t_u16 ) tmpreg;

                usart_stm32l1xx_configure_baudrate( Config );
                */
        }
    }

    return ec_usart_OK;
}
e_ttc_usart_errorcode usart_stm32l1xx_send_word_blocking( volatile t_ttc_usart_register* Register, const t_u16 Word ) {
    Assert_USART( Register != NULL, ttc_assert_origin_auto );

    //ttc_task_msleep(10);
    return _usart_stm32l1xx_send_word_blocking( Register, Word );
}
e_ttc_usart_errorcode usart_stm32l1xx_read_byte_blocking( volatile t_ttc_usart_register* Register, t_u8* Byte ) {

    if ( _usart_stm32l1xx_wait_for_RXNE( Register ) )
    { return ec_usart_TimeOut; }

    *Byte = ( t_u8 ) 0xff & Register->DR.All;

    return ec_usart_OK;
}
e_ttc_usart_errorcode usart_stm32l1xx_read_word_blocking( volatile t_register_stm32l1xx_usart* Register, t_u16* Word ) {

    if ( _usart_stm32l1xx_wait_for_RXNE( Register ) )
    { return ec_usart_TimeOut; }

    *Word = 0x1ff & Register->DR.All;

    return ec_usart_OK;
}
t_u8                  usart_stm32l1xx_get_physicalindex( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto );

    return Config->PhysicalIndex;
}
t_ttc_usart_config*   usart_stm32l1xx_get_configuration( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( t_ttc_usart_config* ) 0;
}
e_ttc_usart_errorcode usart_stm32l1xx_read_byte( volatile t_ttc_usart_register* Register, t_u8* Byte ) {
    Assert_USART_Writable( ( void* ) Register, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_USART_Writable( Byte, ttc_assert_origin_auto );  // pointers must not be NULL

    if ( Register->SR.Bits.RXNE ) {
        *Byte = ( t_u8 ) 0xff & * ( ( t_u32* ) &Register->DR );
        return ec_usart_OK;
    }
    return ec_usart_No_Receive;
}
t_u8                  usart_stm32l1xx_check_bytes_available( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    //#warning Function usart_stm32l1xx_check_bytes_available() is empty.
    if ( Config->BaseRegister->SR.Bits.RXNE )
    { return 1; }

    return ( t_u8 ) 0;
}
e_ttc_usart_errorcode usart_stm32l1xx_configure_baudrate( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    // configure baudrate using TTC register structs (readable + tested)
    volatile t_register_stm32l1xx_usart* BaseRegister = Config->BaseRegister;
    t_register_stm32l1xx_usart_cr1 CR1; CR1.All = BaseRegister->CR1.All;
    t_register_stm32l1xx_usart_brr BRR; BRR.All = 0;

    t_ttc_sysclock_config* SysClock = ttc_sysclock_get_configuration( 1 );

    // Gather data required to calculate baudrate
    BOOL StandardMode = ( BaseRegister->CR2.Bits.LINEN || BaseRegister->CR3.Bits.SC_EN || BaseRegister->CR3.Bits.IREN ) ? 0 : 1;
    t_base ClockAPB;
    if ( Config->PhysicalIndex == 0 )
    { ClockAPB = SysClock->LowLevelConfig.frequency_apb2; } // USART1 is clocked from APB2 clock
    else
    { ClockAPB = SysClock->LowLevelConfig.frequency_apb1; } // USART2, USART3 are clocked from APB1 clock

    // step 1: calculate integer part of USARTDIV
    t_base Divider = ( 8 * ( 2 - CR1.Bits.OVER8 ) * Config->Init.BaudRate );
    BRR.Bits.DIV_Mantissa = 0b111111111111 & ( ClockAPB / Divider );

    // step 2: calulate clock remainder
    t_base ClockRemainder = ClockAPB - ( BRR.Bits.DIV_Mantissa * Divider );

    // step 3: calculate fraction part of USARTDIV
    BRR.Bits.DIV_Fraction = 0b1111 & ( ClockRemainder / Config->Init.BaudRate );

    // update configuration with exact baudrate
    Config->Init.BaudRate = _usart_stm32l1xx_calculate_baudrate( ClockAPB,
                                                                 BRR.Bits.DIV_Mantissa,
                                                                 BRR.Bits.DIV_Fraction,
                                                                 CR1.Bits.OVER8,
                                                                 StandardMode
                                                               );
    // update baudrate register
    BaseRegister->BRR.All = BRR.All;

    /* DEPRECATED: configure baudrate using Standard Peripheral library (kept as comparison)

        volatile t_register_stm32l1xx_usart* Register  = Config->BaseRegister;

        t_u32 tmpreg;
        t_u32 apbclock = 0x00;
        t_u32 integerdivider = 0x00;
        t_u32 fractionaldivider = 0x00;
        RCC_ClocksTypeDef RCC_ClocksStatus;

        // Configure the USART Baud Rate
        RCC_GetClocksFreq( &RCC_ClocksStatus );

        if ( Register == ( t_register_stm32l1xx_usart* ) USART1 ) {
            apbclock = RCC_ClocksStatus.PCLK2_Frequency;
        }
        else {
            apbclock = RCC_ClocksStatus.PCLK1_Frequency;
        }

        // Determine the integer part
        if ( ( Register->CR1.All & USART_CR1_OVER8 ) != 0 ) {
            // Integer part computing in case Oversampling mode is 8 Samples
            integerdivider = ( ( 25 * apbclock ) / ( 2 * ( Config->Init.BaudRate ) ) );
        }
        else { // if ((USARTx->CR1 & CR1_OVER8_Set) == 0)
            // Integer part computing in case Oversampling mode is 16 Samples
            integerdivider = ( ( 25 * apbclock ) / ( 4 * ( Config->Init.BaudRate ) ) );
        }
        tmpreg = ( integerdivider / 100 ) << 4;

        // Determine the fractional part
        fractionaldivider = integerdivider - ( 100 * ( tmpreg >> 4 ) );

        // Implement the fractional part in the register
        if ( ( Register->CR1.All & USART_CR1_OVER8 ) != 0 ) {
            tmpreg |= ( ( ( ( fractionaldivider * 8 ) + 50 ) / 100 ) ) & ( ( t_u8 )0x07 );
        }
        else { // if ((USARTx->CR1 & CR1_OVER8_Set) == 0)
            tmpreg |= ( ( ( ( fractionaldivider * 16 ) + 50 ) / 100 ) ) & ( ( t_u8 )0x0F );
        }

        // Write to USART BRR
        Register->BRR.All = ( t_u16 ) tmpreg;
        */

    return ec_usart_OK;
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************
t_base                _usart_stm32l1xx_calculate_baudrate( t_base ClockAPB, t_u16 Mantissa, t_u16 Fractional, BOOL Over8, BOOL StandardMode ) {
    // Calculate BaudRate according to RM0038 p.624

    t_base BaudRate;

    if ( StandardMode ) { // Standard USART and SPI Mode
        // BaudRate = ClockAPB / (8 * (2 - Over8) * (Mantissa + Fractional / 16) );

        // optimized calculation
        if ( Over8 )
        { BaudRate = ClockAPB / 2 / ( 16 * Mantissa + Fractional ); }
        else
        { BaudRate = ClockAPB / ( 16 * Mantissa + Fractional ); }
    }
    else {              // SmartCard, LIN and IrDA Mode
        // BaudRate = ClockAPB / ( 16 * (Mantissa + Fractional / 16) );
        BaudRate = ClockAPB / ( 16 * Mantissa + Fractional );
    }

    return BaudRate;
}
e_ttc_usart_errorcode _usart_stm32l1xx_send_word_blocking( volatile t_register_stm32l1xx_usart* MyUSART, const t_u16 Word ) {
    Assert_USART( MyUSART != NULL, ttc_assert_origin_auto );

    e_ttc_usart_errorcode  Error = _usart_stm32l1xx_wait_for_TXE( MyUSART );
    if ( Error != ec_usart_OK )
    { return Error; }

    usart_stm32l1xx_send_byte_isr( & ( MyUSART->DR ), Word );
    return ec_usart_OK;
}
e_ttc_usart_errorcode _usart_stm32l1xx_wait_for_RXNE( volatile t_register_stm32l1xx_usart* MyUSART ) {

    while ( ( MyUSART->SR.All & USART_FLAG_RXNE ) == 0 ) {
        ttc_task_yield();
    }

    return ec_usart_OK;
}
e_ttc_usart_errorcode _usart_stm32l1xx_wait_for_TXE( volatile t_register_stm32l1xx_usart* MyUSART ) {

    while ( ! MyUSART->SR.Bits.TXE ) {
        ttc_task_yield();
    }

    return ec_usart_OK;
}
void                  _usart_stm32l1xx_rx_isr( t_physical_index PhysicalIndex, void* Argument ) {
    Assert_USART( PhysicalIndex <= TTC_USART_AMOUNT, ttc_assert_origin_auto );
    Assert_USART_Writable( Argument, ttc_assert_origin_auto );

    t_ttc_usart_config* Config = ( t_ttc_usart_config* ) Argument;
    volatile t_register_stm32l1xx_usart* USART_Base = Config->BaseRegister;

    t_u8 Byte = 0xff & * ( ( t_u16* ) & ( USART_Base->DR ) ) ;

    _ttc_usart_rx_isr( ( t_ttc_usart_config* ) Argument, Byte ); // provide received byte to registered function

    // clear interrupt status flag to allow next interrupt
    if ( 0 ) // more readable
    { USART_Base->SR.Bits.RXNE = 0; }
    else    // faster
    { USART_Base->SR.All &= 0xff - ( 1 << 5 ); }

}
void                  _usart_stm32l1xx_tx_isr( t_physical_index PhysicalIndex, void* Argument ) {
    Assert_USART( PhysicalIndex <= TTC_USART_AMOUNT, ttc_assert_origin_auto );
    Assert_USART_Writable( Argument, ttc_assert_origin_auto );

    t_ttc_usart_config* Config = ( t_ttc_usart_config* ) Argument;
    volatile t_register_stm32l1xx_usart* USART_Base = Config->BaseRegister;

    BOOL BytesRemaining;
    do {
        BytesRemaining = _ttc_usart_tx_isr( Config, & ( USART_Base->DR ) ); // let high level driver choose next byte to send
        USART_Base->SR.Bits.TXE = 0;
    }
    while ( BytesRemaining && USART_Base->SR.Bits.TXE ); // stay in isr for high data rates
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
