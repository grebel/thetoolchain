#ifndef USART_STM32L1XX_H
#define USART_STM32L1XX_H

/** { usart_stm32l1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for usart devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by high-level usart and application.
 *
 *  Created from template device_architecture.h revision 22 at 20140423 01:03:12 UTC
 *
 *  Note: See ttc_usart.h for description of stm32l1xx independent USART implementation.
 *
 *  Authors: Greg Knoll 2014.
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//data register addresses for data transmit/receive
#define USART1_DR ((t_u16*)  USART1_BASE + 0x04)
#define USART2_DR ((t_u16*)  USART2_BASE + 0x04)
#define USART3_DR ((t_u16*)  USART3_BASE + 0x04)
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_USART_STM32L1XX
//
// Implementation status of low-level driver support for usart devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_USART_STM32L1XX 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_USART_STM32L1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_USART_STM32L1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_USART_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "usart_stm32l1xx_types.h"
#include "../ttc_usart_types.h"
#include "../ttc_task.h"
#include "../ttc_interrupt.h"
#include "../ttc_gpio.h"

//?required #include "stm32l1xx.h"       // main header file for stm32L1 STDPeriph Lib
//#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
//#include "stm32l1xx_conf.h"
//#include "stm32l1xx_usart.h" // stdPeriph for STM32L USART
//#include "misc.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_usart_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_usart_foo
//
#define ttc_driver_usart_init(Config)                       usart_stm32l1xx_init(Config)
#define ttc_driver_usart_deinit(Config)                     usart_stm32l1xx_deinit(Config)
#define ttc_driver_usart_reset(Config)                      usart_stm32l1xx_reset(Config)

#define ttc_driver_usart_get_configuration(Config)          usart_stm32l1xx_get_configuration(Config)
#define ttc_driver_usart_get_features(Config)               usart_stm32l1xx_get_features(Config)
#define ttc_driver_usart_load_defaults(Config)              usart_stm32l1xx_load_defaults(Config)


#define ttc_driver_usart_read_byte(Register, Byte)          usart_stm32l1xx_read_byte(Register, Byte)
#define ttc_driver_usart_read_word_blocking(Register, Word) usart_stm32l1xx_read_word_blocking(Register, Word)
#define ttc_driver_usart_send_word_blocking(Register, Word) usart_stm32l1xx_send_word_blocking(Register, Word)
#define ttc_driver_usart_prepare()                          usart_stm32l1xx_prepare()

#define ttc_driver_usart_check_bytes_available(Config)      usart_stm32l1xx_check_bytes_available(Config)
#define ttc_driver_usart_configure_baudrate(Config)         usart_stm32l1xx_configure_baudrate(Config)

#define ttc_driver_usart_send_byte_isr(Register, Word)      usart_stm32l1xx_send_byte_isr(Register, Word)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_usart.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_usart.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

/** initializes single USART unit for operation
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for LogicalIndex)
 * @return              == 0: USART has been initialized successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32l1xx_init( t_ttc_usart_config* Config );

/** shutdown single USART unit device
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for LogicalIndex)
 * @return              == 0: USART has been shutdown successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32l1xx_deinit( t_ttc_usart_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for LogicalIndex)
 * @return              == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32l1xx_reset( t_ttc_usart_config* Config );



/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_USART_get_max_LogicalIndex())
 * @return                configuration of indexed device
 * @param Config   =
 */
t_ttc_usart_config* usart_stm32l1xx_get_configuration( t_ttc_usart_config* Config );

/** fills out given Config with maximum valid values for indexed USART
 * @param Config        = pointer to struct t_ttc_usart_config (must have valid value for LogicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32l1xx_get_features( t_ttc_usart_config* Config );

/** loads configuration of indexed USART unit with default values
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_usart_errorcode usart_stm32l1xx_load_defaults( t_ttc_usart_config* Config );


/** Reads single data byte from input buffer (8 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 *
 * @param Register      pointer to low-level register for fast hardware operation
 * @param Byte          pointer to 8 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32l1xx_read_byte( volatile t_ttc_usart_register* Register, t_u8* Byte );

/** Reads single data byte from input buffer.
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param Register      pointer to low-level register for fast hardware operation
 * @param Word           pointer to 16 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32l1xx_read_byte_blocking( volatile t_ttc_usart_register* Register, t_u8* Byte );

/** Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param Register      pointer to low-level register for fast hardware operation
 * @param Word           pointer to 16 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32l1xx_read_word_blocking( volatile t_ttc_usart_register* Register, t_u16* Word );

/** Send single data word to output buffer (8 or 9 bits)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 * Note: This function blocks until word has been sent or timeout occurs
 *
 * @param Register      pointer to low-level register for fast hardware operation
 * @param Word          pointer to 16 bit buffer where to store Word
 * @return              == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_usart_errorcode usart_stm32l1xx_send_word_blocking( volatile t_ttc_usart_register* Register, const t_u16 Word );

/** Prepares usart driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void usart_stm32l1xx_prepare();

/** returns amount of bytes being held in receive buffer
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usart_get_max_logicalindex())
 * @return               == 0: no bytes available; >0: amount of bytes available
 * @param Config   =
 */
t_u8 usart_stm32l1xx_check_bytes_available( t_ttc_usart_config* Config );

e_ttc_usart_errorcode usart_stm32l1xx_configure_baudrate( t_ttc_usart_config* Config );

/** general interrupt handler for received byte; reads received byte and pass it to receiveSingleByte()
 * @param PhysicalIndex    0..TTC_USART_AMOUNT-1 - USART device to use (0=USART1, ...)
 * @param USART_Generic    pointer to configuration of corresponding USART
 */
void _usart_stm32l1xx_rx_isr( t_physical_index PhysicalIndex, void* Argument );

/** general interrupt handler for that will call _ttc_usart_tx_isr() to provide next byte to send
 * @param PhysicalIndex    0..TTC_USART_AMOUNT-1 - USART device to use (0=USART1, ...)
 * @param USART_Generic    pointer to configuration of corresponding USART
 */
void _usart_stm32l1xx_tx_isr( t_physical_index PhysicalIndex, void* Argument );


/** low level send routine especially to be called from _ttc_usart_tx_isr()
 *
 * @param Register       pointer to hardware register for fast access
 * @param Word           data word to send (8 or 9 bits)
 */
//void usart_stm32l1xx_send_byte_isr( volatile t_ttc_usart_register_tx* Register, const t_u16 Word );
#define usart_stm32l1xx_send_byte_isr(Register, Word)  (* ( ( t_u16* ) Register ) ) = (t_u16) (Word)
// #define usart_stm32l1xx_send_byte_isr(Register, Word)  (* ( ( t_u16* ) Register ) = ( Word & ( t_u16 ) 0x01FF ))

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
//} Function prototypes

#endif //USART_STM32L1XX_H
