/** { usart_stm32f1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for usart devices on stm32f1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 20 at 20140217 09:36:59 UTC
 *
 *  Note: See ttc_usart.h for description of stm32f1xx independent USART implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "usart_stm32f1xx.h"
#include "../ttc_assert.h"
#include "../ttc_cpu.h"
#include "../ttc_task.h"
#include "../ttc_interrupt.h"
#include "../ttc_gpio.h"
#include "../ttc_sysclock.h"

/* DEPRECATED
#define USE_STDPERIPH_DRIVER
#include "additionals/270_CPAL/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h"
#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
#include "stm32f10x_conf.h"
#include "stm32f10x_usart.h"
#include "misc.h"
//? #include "../ttc_sysclock.h"
*/

/* just for testing
#ifndef TTC_USART2
#define TTC_USART2
#endif
#ifndef TTC_USART2_RX
#define TTC_USART2_RX
#endif
#ifndef TTC_USART2_TX
#define TTC_USART2_TX
#endif
#define TTC_USART2_RTS
#define TTC_USART2_CTS
#define TTC_USART2_CLK
#define TTC_USART3
#define TTC_USART3_RX
#define TTC_USART3_TX
#define TTC_USART3_RTS
#define TTC_USART3_CTS
#define TTC_USART3_CLK
#define TTC_USART3
#define TTC_USART3_RX
#define TTC_USART3_TX
#define TTC_USART3_RTS
#define TTC_USART3_CTS
#define TTC_USART3_CLK
#define TTC_USART5
#define TTC_USART5_RX
#define TTC_USART5_TX
#define TTC_USART5_RTS
#define TTC_USART5_CTS
#define TTC_USART5_CLK
#warning remove block above!
*/

// these are defined by ttc_usart.c
extern void _ttc_usart_rx_isr( t_ttc_usart_config* Config, t_u8 Byte );
extern BOOL _ttc_usart_tx_isr( t_ttc_usart_config* Config, volatile void* USART_Hardware );
extern t_base ttc_usart_Timeout;

/* DEPRECATED
// register base addresses of all USARTs
t_register_stm32f1xx_usart* const usart_stm32f1xx_BaseRegisters[TTC_USART_MAX_AMOUNT] = {
    ( t_register_stm32f1xx_usart* ) USART1,
    ( t_register_stm32f1xx_usart* ) USART2,
    ( t_register_stm32f1xx_usart* ) USART3,
    ( t_register_stm32f1xx_usart* ) UART4,
    ( t_register_stm32f1xx_usart* ) UART5
};
*/

volatile t_register_stm32f1xx_usart* _usart_stm32f1xx_get_base_register( t_u8 PhysicalIndex ) {
    switch ( PhysicalIndex ) {              // find corresponding USART as defined by makefile.100_board_*
        case 0: return &register_stm32f1xx_USART1; break;
        case 1: return &register_stm32f1xx_USART2; break;
        case 2: return &register_stm32f1xx_USART3; break;
        case 3: return &register_stm32f1xx_UART4;  break;
        case 4: return &register_stm32f1xx_UART5;  break;
        default: Assert_USART( 0, ttc_assert_origin_auto ); break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
    }
    return NULL;
}

//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _usart_stm32f1xx_foo(t_ttc_usart_config* Config)

/** Sends out given byte.
 * @param MyUSART        USART device to use
 * @param Word           pointer to 16 bit buffer where to store Word

 *
 * Note: This low-level function should not be called from outside!
 */
e_ttc_usart_errorcode _usart_stm32f1xx_send_word_blocking( volatile t_register_stm32f1xx_usart* MyUSART, const t_u16 Word );

/** low level send routine especially to be called from _ttc_usart_tx_isr()
 *
 * @param MyUSART        USART device to use
 * @param Word           data word to send (8 or 9 bits)
 */
void _usart_stm32f1xx_send_byte_isr( volatile t_register_stm32f1xx_usart* MyUSART, const t_u16 Word );

/** Wait until word has been received or timeout occured.
 * Note: This low-level function is private and should not be called from outside!
 *
 * @param MyUSART        USART device to use

 */
e_ttc_usart_errorcode _usart_stm32f1xx_wait_for_RXNE( volatile t_register_stm32f1xx_usart* MyUSART );

/** wait until transmit buffer is empty
 * Note: This low-level function is private and should not be called from outside!
 *
 * @param MyUSART        USART device to use

 */
e_ttc_usart_errorcode _usart_stm32f1xx_wait_for_TXE( volatile t_register_stm32f1xx_usart* MyUSART );

void _usart_stm32f1xx_start( volatile t_register_stm32f1xx_usart* Register );

e_ttc_usart_errorcode _usart_stm32f1xx_init( t_ttc_usart_config* Config );

e_ttc_usart_errorcode _usart_stm32f1xx_configure_baudrate( t_ttc_usart_config* Config );

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

//{ Function definitions *******************************************************

e_ttc_usart_errorcode usart_stm32f1xx_deinit( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( e_ttc_usart_errorcode ) 0;
}
void usart_stm32f1xx_prepare() {




}
e_ttc_usart_errorcode usart_stm32f1xx_reset( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    return usart_stm32f1xx_load_defaults( Config );
}
e_ttc_usart_errorcode usart_stm32f1xx_load_defaults( t_ttc_usart_config* Config ) {
    //X     if (!su_Initialized) usart_stm32f1xx_reset_all();
    // assumption: *Config has been zeroed

    Assert_USART_Writable( Config, ttc_assert_origin_auto );
    Assert_USART( Config->PhysicalIndex < TTC_USART_MAX_AMOUNT, ttc_assert_origin_auto );
    Config->BaseRegister =  _usart_stm32f1xx_get_base_register( Config->PhysicalIndex );

    ttc_memory_set( &( Config->Init.Flags ), 0, sizeof( Config->Init.Flags ) );
    Config->Init.Flags.SendBreaks     = 1;
    Config->Init.Flags.ControlParity  = 1;
    Config->Init.Flags.Receive        = 1;
    Config->Init.Flags.Transmit       = 1;
    Config->Init.Flags.TransmitParity = 1;

    /*{ remaining bits
    Config->Init.Flags.LIN            =0;
    Config->Init.Flags.IrDA           =0;
    Config->Init.Flags.IrDALowPower   =0;
    Config->Init.Flags.SmartCardNACK  =0;
    Config->Init.Flags.RxDMA          =0;
    Config->Init.Flags.TxDMA          =0;
    Config->Init.Flags.Rts            =0;
    Config->Init.Flags.IrqOnCts       =0;
    Config->Init.Flags.IrqOnError     =0;
    Config->Init.Flags.IrqOnIdle      =0;
    Config->Init.Flags.IrqOnTxComplete=0;
    Config->Init.Flags.IrqOnParityError= 0;
    Config->Init.Flags.ParityEven     =0;
    Config->Init.Flags.ParityOdd      =0;

    Config->Init.Flags.RtsCts         =0;
    Config->Init.Flags.SingleWire     =0;
    Config->Init.Flags.SmartCard      =0;
    Config->Init.Flags.Synchronous    =0;
    }*/

    Config->Init.WordLength     = 8;
    Config->Init.HalfStopBits   = 2;
    Config->Layout         = 0;
    Config->Init.BaudRate       = 115200;

    return ec_usart_OK;
}
e_ttc_usart_errorcode usart_stm32f1xx_get_features( t_ttc_usart_config* Config ) {
    // assumption: *Config has been zeroed

    t_u8 PhysicalIndex = Config->PhysicalIndex;
    Assert_USART( Config != NULL, ttc_assert_origin_auto );
    if ( PhysicalIndex >= TTC_USART_MAX_AMOUNT ) { return ec_usart_DeviceNotFound; }
    Config->BaseRegister = _usart_stm32f1xx_get_base_register( PhysicalIndex );

    ttc_memory_set( &( Config->Init.Flags ), 0, sizeof( Config->Init.Flags ) );
    Config->Init.Flags.Receive          = 1;
    Config->Init.Flags.Transmit         = 1;
    Config->Init.Flags.TransmitParity   = 1;
    Config->Init.Flags.DelayedTransmits = 0; // ToDo: Fix Assert()-issue when activating this bit
    Config->Init.Flags.SendBreaks       = 1;
    Config->Init.Flags.ParityEven       = 1;
    Config->Init.Flags.ParityOdd        = 1;
    Config->Init.Flags.IrqOnRxNE        = 1;

    /*{ remaining bits
    Config->Init.Flags.ControlParity  =0; // (ToDo: implement)
    Config->Init.Flags.RtsCts         =0; // (ToDo: implement)
    Config->Init.Flags.SingleWire     =0; // (ToDo: implement)
    Config->Init.Flags.SmartCard      =0; // (ToDo: implement)
    Config->Init.Flags.Synchronous    =0; // (ToDo: implement)
    Config->Init.Flags.LIN            =0; // (ToDo: implement)
    Config->Init.Flags.IrDA           =0; // (ToDo: implement)
    Config->Init.Flags.IrDALowPower   =0; // (ToDo: implement)
    Config->Init.Flags.SmartCardNACK  =0; // (ToDo: implement)
    Config->Init.Flags.RxDMA          =0; // (ToDo: implement)
    Config->Init.Flags.TxDMA          =0; // (ToDo: implement)
    Config->Init.Flags.Rts            =0; // (ToDo: implement)
    Config->Init.Flags.IrqOnCts       =0; // (ToDo: implement)
    Config->Init.Flags.IrqOnError     =0; // (ToDo: implement)
    Config->Init.Flags.IrqOnIdle      =0; // (ToDo: implement)
    Config->Init.Flags.IrqOnTxComplete=0; // (ToDo: implement)
    Config->Init.Flags.IrqOnParityError= 0; // (ToDo: implement)
    }*/

    Config->Init.WordLength      = 9;
    Config->Init.HalfStopBits    = 4;
    Config->Init.BaudRate        = 115200;

    switch ( PhysicalIndex ) { // find amount of available remapping-layouts (-> RM0008 p. 176)
        case 0:
            Config->Layout = 1;   // USART1 and USART2 provide 1 alternate pin layout
            break;
        case 1:
            if ( ttc_cpu_get_configuration( 1 )->Info->Amount_Pins >= 100 )
            { Config->Layout = 1; } // USART1 and USART2 provide 1 alternate pin layout
            else
            { Config->Layout = 0; } // USART1 and USART2 provide 0 alternate pin layout
            break;
        case 2:
            if ( ttc_cpu_get_configuration( 1 )->Info->Amount_Pins >= 100 )
            { Config->Layout = 2; } // USART3 provides 2 alternate pin layout
            else if ( ttc_cpu_get_configuration( 1 )->Info->Amount_Pins >= 64 )
            { Config->Layout = 1; } // USART3 provides 1 alternate pin layout
            else
            { Config->Layout = 0; } // USART3 provides 0 alternate pin layout
            break;
        case 3:
        case 4:
            // USART4 and USART5 lack some features (-> ST RM0038 Table 138 on p. 651)
            Config->Init.Flags.Rts            = 0;
            Config->Init.Flags.ControlParity  = 0;
            Config->Init.Flags.RtsCts         = 0;
            Config->Init.Flags.SmartCard      = 0;
            Config->Init.Flags.SmartCardNACK  = 0;
            Config->Init.Flags.Synchronous    = 0;
            Config->Init.Flags.IrqOnCts       = 0;
            Config->Layout                    = 0; // no alternate pin layout

            break;
        default: ttc_assert_halt( ); break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
    }

    return ec_usart_OK;
}
e_ttc_usart_errorcode usart_stm32f1xx_init( t_ttc_usart_config* Config ) {
    // assumption: *Config.USART_Arch has been zeroed

    t_u8 LogicalIndex = Config->LogicalIndex;
    Assert_USART( LogicalIndex > 0, ttc_assert_origin_auto ); // logical index starts at 1
    Assert_USART( Config != NULL, ttc_assert_origin_auto );
    if ( LogicalIndex > TTC_USART_AMOUNT )
    { return ec_usart_DeviceNotFound; }
    if ( ( Config->Init.WordLength   < 8 ) || ( Config->Init.WordLength   > 9 ) )
    { return ec_usart_InvalidWordSize; }
    if ( ( Config->Init.HalfStopBits < 1 ) || ( Config->Init.HalfStopBits > 4 ) )
    { return ec_usart_InvalidStopBits; }

    if ( 1 ) {                                 // validate USART_Features
        t_ttc_usart_config USART_Features;
        ttc_memory_set( &USART_Features, 0, sizeof( USART_Features ) );
        USART_Features.LogicalIndex  = LogicalIndex;
        USART_Features.PhysicalIndex = Config->PhysicalIndex;
        e_ttc_usart_errorcode Error = usart_stm32f1xx_get_features( &USART_Features );
        if ( Error ) { return Error; }

        //X Config->Init.Flags.All &= USART_Features.Flags.All; // mask out unavailable flags
        Assert_USART_EXTRA( sizeof( Config->Init.Flags ) == 4, ttc_assert_origin_auto ); // size of bitfield must exactly match typecast below. Change code to fit!
        // mask out unavailable flags
        *( ( t_u32* ) & ( Config->Init.Flags ) ) &= *( ( t_u32* ) & ( USART_Features.Init.Flags ) );
        TODO( "Above Required? Movable to ttc_usart_init?" )

        if ( Config->Layout > USART_Features.Layout )
        { Config->Layout = USART_Features.Layout; }
    }
    /* DEPRECATED
    switch ( ( t_u32 ) Config->BaseRegister ) {
        case ( t_u32 ) USART1: {
            if ( Config->Layout ) { // GPIOB
                RCC_APB2PeriphClockCmd( RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE );
            }
            else {                       // GPIOA
                RCC_APB2PeriphClockCmd( RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE );
            }
            break;
        }
        case ( t_u32 ) USART2: {
            if ( Config->Layout ) { // GPIOD
                RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART2, ENABLE );
                RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE );
            }
            else {                       // GPIOA
                RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART2, ENABLE );
                RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE );
            }
            break;
        }
        case ( t_u32 ) USART3: {
            switch ( Config->Layout ) {
                case 1: // GPPIOB, GPIOC
                    RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART3, ENABLE );
                    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO, ENABLE );
                    break;
                case 2: // GPPIOD
                    RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART3, ENABLE );
                    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE );
                    break;
                default: // GPPIOB
                    RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART3, ENABLE );
                    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE );
                    break;
            }
            break;
        }
        case ( t_u32 ) UART4:  {
            RCC_APB1PeriphClockCmd( RCC_APB1Periph_UART4, ENABLE );
            RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE );
            break;
        }
        case ( t_u32 ) UART5:  {
            RCC_APB1PeriphClockCmd( RCC_APB1Periph_UART5, ENABLE );
            RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE );
            break;
        }
        default: ttc_assert_halt( ); break; // should never occur!
    }
    */
    switch ( Config->LogicalIndex ) { // enable + reset device
#ifdef TTC_USART1
        case 1: {
            register_stm32f1xx_RCC.APB2ENR.USART1_EN  = 1; // power on USART device
            register_stm32f1xx_RCC.APB2RSTR.USART1RST = 1; // reset USART device
            break;
        }
#endif
#ifdef TTC_USART2
        case 2: {
            register_stm32f1xx_RCC.APB1ENR.USART2_EN   = 1; // power on USART device
            register_stm32f1xx_RCC.APB1RSTR.USART2RST = 1; // reset USART device
            break;
        }
#endif
#ifdef TTC_USART3
        case 3: {
            register_stm32f1xx_RCC.APB1ENR.USART3_EN   = 1; // power on USART device
            register_stm32f1xx_RCC.APB1RSTR.USART3RST = 1; // reset USART device
            break;
        }
#endif
#ifdef TTC_USART4
        case 4: {
            register_stm32f1xx_RCC.APB1ENR.UART4_EN    = 1; // power on USART device
            register_stm32f1xx_RCC.APB1RSTR.UART4RST  = 1; // reset USART device
            break;
        }
#endif
#ifdef TTC_USART5
        case 5: {
            register_stm32f1xx_RCC.APB1ENR.UART5_EN    = 1; // power on USART device
            register_stm32f1xx_RCC.APB1RSTR.UART5RST  = 1; // reset USART device
            break;
        }
#endif

        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown physical index. Check implementation of caller!
    }
    /* DEPRECATED
        if ( 1 ) {                                 // find indexed USART on current board
            switch ( Config->PhysicalIndex ) {              // find corresponding USART as defined by makefile.100_board_*
                case 0: Config->BaseRegister = ( t_register_stm32f1xx_usart* ) USART1; break;
                case 1: Config->BaseRegister = ( t_register_stm32f1xx_usart* ) USART2; break;
                case 2: Config->BaseRegister = ( t_register_stm32f1xx_usart* ) USART3; break;
                case 3: Config->BaseRegister = ( t_register_stm32f1xx_usart* ) UART4;  break;
                case 4: Config->BaseRegister = ( t_register_stm32f1xx_usart* ) UART5;  break;
                default: ttc_assert_halt( ); break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
            }
            Config->RegisterRx = Config->BaseRegister;
            Config->RegisterTx = Config->BaseRegister;
        }
        */


    switch ( Config->PhysicalIndex ) { // determine pin layout (->RM0008 p.176)
        case 0: { // USART1
            if ( Config->PortTxD ) { // using TX pin to identify remap layout
                if ( Config->PortTxD == E_ttc_gpio_pin_b6 ) { Config->Layout = 1; }
                else                                        { Config->Layout = 0; }
            }
            else {                 // using RX pin to identify remap layout
                Assert_USART( Config->PortRxD != 0, ttc_assert_origin_auto ); // either PortTX or PortRX must be configured. Check implementation of caller or static configuration in your makefile!
                if ( Config->PortRxD == E_ttc_gpio_pin_b7 ) { Config->Layout = 1; }
                else                                        { Config->Layout = 0; }
            }

            if ( Config->Layout ) { // test alternate pin remapping (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
                Assert_USART( ( !Config->PortRxD ) || ( Config->PortRxD == E_ttc_gpio_pin_b7 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                Assert_USART( ( !Config->PortTxD ) || ( Config->PortTxD == E_ttc_gpio_pin_b6 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
            }
            else {
                Assert_USART( ( !Config->PortRxD ) || ( Config->PortRxD == E_ttc_gpio_pin_a10 ), ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                Assert_USART( ( !Config->PortTxD ) || ( Config->PortTxD == E_ttc_gpio_pin_a9 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
            }
            // test unmappable pins
            Assert_USART( ( !Config->PortCTS ) || ( Config->PortCTS == E_ttc_gpio_pin_a11 ), ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
            Assert_USART( ( !Config->PortRTS ) || ( Config->PortRTS == E_ttc_gpio_pin_a12 ), ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
            Assert_USART( ( !Config->PortCLK ) || ( Config->PortCLK == E_ttc_gpio_pin_a8 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
            break;
        }
        case 1: { // USART2
            if ( Config->PortTxD ) { // using TX pin to identify remap layout
                if ( Config->PortTxD == E_ttc_gpio_pin_d5 ) { Config->Layout = 1; }
                else                                        { Config->Layout = 0; }
            }
            else {                   // using RX pin to identify remap layout
                Assert_USART( Config->PortRxD != 0, ttc_assert_origin_auto ); // either PortTX or PortRX must be configured. Check implementation of caller or static configuration in your makefile!
                if ( Config->PortRxD == E_ttc_gpio_pin_d6 ) { Config->Layout = 1; }
                else                                        { Config->Layout = 0; }
            }

            if ( Config->Layout ) { // test alternate pin remapping (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
                Assert_USART( ( !Config->PortTxD ) || ( Config->PortTxD == E_ttc_gpio_pin_d5 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                Assert_USART( ( !Config->PortRxD ) || ( Config->PortRxD == E_ttc_gpio_pin_d6 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                Assert_USART( ( !Config->PortCTS ) || ( Config->PortCTS == E_ttc_gpio_pin_d3 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                Assert_USART( ( !Config->PortRTS ) || ( Config->PortRTS == E_ttc_gpio_pin_d4 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                Assert_USART( ( !Config->PortCLK ) || ( Config->PortCLK == E_ttc_gpio_pin_d7 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
            }
            else {
                Assert_USART( ( !Config->PortTxD ) || ( Config->PortTxD == E_ttc_gpio_pin_a2 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                Assert_USART( ( !Config->PortRxD ) || ( Config->PortRxD == E_ttc_gpio_pin_a3 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                Assert_USART( ( !Config->PortCTS ) || ( Config->PortCTS == E_ttc_gpio_pin_a0 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                Assert_USART( ( !Config->PortRTS ) || ( Config->PortRTS == E_ttc_gpio_pin_a1 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                Assert_USART( ( !Config->PortCLK ) || ( Config->PortCLK == E_ttc_gpio_pin_a4 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
            }
            break;
        }
        case 2: { // USART3
            if ( Config->PortTxD ) {    // using TX pin to identify remap layout
                switch ( Config->PortTxD ) {
                    case E_ttc_gpio_pin_c10: Config->Layout = 0; break;
                    case E_ttc_gpio_pin_d8:  Config->Layout = 1; break;
                    case E_ttc_gpio_pin_b10: Config->Layout = 2; break;
                    default: break;
                }
            }
            else {                      // using RX pin to identify remap layout
                Assert_USART( Config->PortRxD != 0, ttc_assert_origin_auto ); // either PortTX or PortRX must be configured. Check implementation of caller or static configuration in your makefile!
                switch ( Config->PortRxD ) {
                    case E_ttc_gpio_pin_c11: Config->Layout = 0; break;
                    case E_ttc_gpio_pin_d9:  Config->Layout = 1; break;
                    case E_ttc_gpio_pin_b11: Config->Layout = 2; break;
                    default: break;
                }
            }
            switch ( Config->Layout ) { // test alternate pin remapping (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
                case 0: { // no pin remapping
                    Assert_USART( ( !Config->PortTxD ) || ( Config->PortTxD == E_ttc_gpio_pin_b10 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    Assert_USART( ( !Config->PortRxD ) || ( Config->PortRxD == E_ttc_gpio_pin_b11 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    Assert_USART( ( !Config->PortCTS ) || ( Config->PortCTS == E_ttc_gpio_pin_b13 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    Assert_USART( ( !Config->PortRTS ) || ( Config->PortRTS == E_ttc_gpio_pin_b14 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    Assert_USART( ( !Config->PortCLK ) || ( Config->PortCLK == E_ttc_gpio_pin_b12 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    break;
                }
                case 1: { // partial pin remapping
                    Assert_USART( ( !Config->PortTxD ) || ( Config->PortTxD == E_ttc_gpio_pin_c10 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    Assert_USART( ( !Config->PortRxD ) || ( Config->PortRxD == E_ttc_gpio_pin_c11 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    Assert_USART( ( !Config->PortCTS ) || ( Config->PortCTS == E_ttc_gpio_pin_b13 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    Assert_USART( ( !Config->PortRTS ) || ( Config->PortRTS == E_ttc_gpio_pin_b14 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    Assert_USART( ( !Config->PortCLK ) || ( Config->PortCLK == E_ttc_gpio_pin_c12 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    break;
                }
                case 2: { // full pin remapping
                    Assert_USART( ( !Config->PortTxD ) || ( Config->PortTxD == E_ttc_gpio_pin_d8 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    Assert_USART( ( !Config->PortRxD ) || ( Config->PortRxD == E_ttc_gpio_pin_d9 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    Assert_USART( ( !Config->PortCTS ) || ( Config->PortCTS == E_ttc_gpio_pin_d11 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    Assert_USART( ( !Config->PortRTS ) || ( Config->PortRTS == E_ttc_gpio_pin_d12 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    Assert_USART( ( !Config->PortCLK ) || ( Config->PortCLK == E_ttc_gpio_pin_d10 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                    break;
                }
            }
            case 3:  { // UART4
                Assert_USART( ( !Config->PortTxD ) || ( Config->PortTxD == E_ttc_gpio_pin_c10 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                Assert_USART( ( !Config->PortRxD ) || ( Config->PortRxD == E_ttc_gpio_pin_c11 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                break;
            }
            case 4:  { // UART5
                Assert_USART( ( !Config->PortTxD ) || ( Config->PortTxD == E_ttc_gpio_pin_c12 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                Assert_USART( ( !Config->PortRxD ) || ( Config->PortRxD == E_ttc_gpio_pin_d2 ),  ttc_assert_origin_auto ); // invalid pin configuration. Check implementation of caller or static configuration in your makefile!
                break;
            }
            default: { ttc_assert_halt( ); }
            }
    }
    if ( 1 ) {                         // configure GPIOs
        if ( Config->PortTxD )  { ttc_gpio_init( Config->PortTxD, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max ); }
        if ( Config->PortRxD )  { ttc_gpio_init( Config->PortRxD, E_ttc_gpio_mode_input_floating,               E_ttc_gpio_speed_max ); }
        if ( Config->PortRTS )  { ttc_gpio_init( Config->PortRTS, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max ); }
        if ( Config->PortCLK )  { ttc_gpio_init( Config->PortCLK, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max ); }
        if ( Config->PortCTS )  { ttc_gpio_init( Config->PortCTS, E_ttc_gpio_mode_input_floating,               E_ttc_gpio_speed_max ); }
    }
    /* DEPRECATED
        if ( 1 ) {                                 // apply remap layout

            switch ( Config->PhysicalIndex ) {

                case 0: { // USART1
                    if ( Config->Layout )
                    { GPIO_PinRemapConfig( GPIO_Remap_USART1, ENABLE ); }
                    else
                    { GPIO_PinRemapConfig( GPIO_Remap_USART1, DISABLE ); }

                    break;
                }
                case 1: { // USART2
                    if ( Config->Layout )
                    { GPIO_PinRemapConfig( GPIO_Remap_USART2, ENABLE ); }
                    else
                    { GPIO_PinRemapConfig( GPIO_Remap_USART2, DISABLE ); }
                    break;
                }
                case 2: { // USART3
                    switch ( Config->Layout ) { // (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
                        case 1:
                            GPIO_PinRemapConfig( GPIO_PartialRemap_USART3, ENABLE );
                            break;
                        case 2:
                            GPIO_PinRemapConfig( GPIO_FullRemap_USART3, ENABLE );
                            break;
                        default:  // no pin remapping
                            GPIO_PinRemapConfig( GPIO_PartialRemap_USART3 | GPIO_FullRemap_USART3, DISABLE );
                            break;
                    }
                    break;
                }

                default: { break; }
            }

        }
        */

    t_afio_mapr MAPR;
    // force 32 bit read from register into local variable
    *( ( volatile t_u32* )( &MAPR ) ) = * ( ( volatile t_u32* )( &register_stm32f1xx_AFIO.MAPR ) );

    switch ( Config->PhysicalIndex ) { // configure GPIO port remapping

        case 0: { // USART1
            MAPR.USART1_REMAP = Config->Layout;
            break;
        }
        case 1: { // USART2
            MAPR.USART2_REMAP = Config->Layout;
            break;
        }
        case 2: { // USART3
            switch ( Config->Layout ) { // (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
                case 0: MAPR.USART3_REMAP = 0b01; break;
                case 1: MAPR.USART3_REMAP = 0b11; break;
                case 2: MAPR.USART3_REMAP = 0;    break;
                default:  Assert_USART_EXTRA( 0, ttc_assert_origin_auto ); // illegal configuration. Check implementation above!
                    break;
            }
            break;
        }

        default: { break; }
    }

    // force 32 bit write to register into local variable
    * ( ( volatile t_u32* )( &register_stm32f1xx_AFIO.MAPR ) ) = *( ( volatile t_u32* )( &MAPR ) );

    _usart_stm32f1xx_configure_baudrate( Config );
    if ( 1 ) {                        // configure rest of USART features
        t_register_stm32f1xx_usart_cr1  CR1;
        t_register_stm32f1xx_usart_cr2  CR2;
        t_register_stm32f1xx_usart_cr3  CR3;

        CR1.Bits.TE     = Config->Init.Flags.Transmit;
        CR1.Bits.RE     = Config->Init.Flags.Receive;
        CR1.Bits.IDLEIE = Config->Init.Flags.IrqOnIdle;
        CR1.Bits.RXNEIE = Config->Init.Flags.IrqOnRxNE;
        CR1.Bits.TCIE   = Config->Init.Flags.IrqOnTxComplete;
        CR1.Bits.PEIE   = Config->Init.Flags.IrqOnParityError;
        CR1.Bits.PCE    = Config->Init.Flags.ParityEven | Config->Init.Flags.ParityOdd;
        CR1.Bits.M      = ( Config->Init.WordLength == 8 ) ? 0 : 1;

        CR2.Bits.ADD    = Config->Init.LIN_Address;
        CR2.Bits.LBDL   = Config->Init.Flags.LIN_Break11Bits;
        CR2.Bits.LBDIE  = Config->Init.Flags.LIN_IrqOnBreak;
        CR2.Bits.LBCL   = 0; // not used
        CR2.Bits.CPHA   = 1 - Config->Init.Flags.ClockOnFirst;
        CR2.Bits.CPOL   = 1 - Config->Init.Flags.ClockIdleLow;
        CR2.Bits.CLKEN  = Config->Init.Flags.Synchronous;
        CR2.Bits.LINEN  = Config->Init.Flags.LIN;

        switch ( Config->Init.HalfStopBits ) { // -> RM0008 p.795
            case 1: CR2.Bits.STOP = 0b01; break; // 0.5 stop bits
            case 2: CR2.Bits.STOP = 0b00; break; // 1   stop bit
            case 3: CR2.Bits.STOP = 0b11; break; // 1.5 stop bits
            case 4: CR2.Bits.STOP = 0b10; break; // 2   stop bits
            default: Assert_USART( 0, ttc_assert_origin_auto ); // invalid configuration
        }

        CR3.Bits.EIE   = Config->Init.Flags.IrqOnError;
        CR3.Bits.IREN  = Config->Init.Flags.IrDA;
        CR3.Bits.IRLP  = Config->Init.Flags.IrDALowPower;
        CR3.Bits.HDSEL = Config->Init.Flags.HalfDuplex;
        CR3.Bits.NACK  = Config->Init.Flags.SmartCardNACK;
        CR3.Bits.SC_EN  = Config->Init.Flags.SmartCard;
        CR3.Bits.DMAR  = Config->Init.Flags.RxDMA;
        CR3.Bits.DMAT  = Config->Init.Flags.TxDMA;
        CR3.Bits.RTSE  = Config->Init.Flags.Rts | Config->Init.Flags.RtsCts;
        CR3.Bits.CTSE  = Config->Init.Flags.RtsCts;
        CR3.Bits.CTSIE = Config->Init.Flags.IrqOnCts;

        // update USART configuration registers
        Config->BaseRegister->CR1.All = CR1.All;
        Config->BaseRegister->CR2.All = CR2.All;
        Config->BaseRegister->CR3.All = CR3.All;
    }

    _usart_stm32f1xx_start( Config->BaseRegister );

    /* DEPRECATED
            t_register_stm32f1xx_usart* USART = NULL;
                //X Config->Init.BaudRate = Config->Init.BaudRate;

            Config->USART_Arch.HardwareFlowControl = USART_HardwareFlowControl_None;
            if ( Config->Init.Flags.Rts )     { Config->USART_Arch.HardwareFlowControl = USART_HardwareFlowControl_RTS; }
            if ( Config->Init.Flags.Cts )     { Config->USART_Arch.HardwareFlowControl = USART_HardwareFlowControl_CTS; }
            if ( Config->Init.Flags.RtsCts )  { Config->USART_Arch.HardwareFlowControl = USART_HardwareFlowControl_RTS_CTS; }

            Config->USART_Arch.Mode = 0;
            if ( Config->Init.Flags.Transmit ) { Config->USART_Arch.Mode |= USART_Mode_Tx; }
            if ( Config->Init.Flags.Receive )  { Config->USART_Arch.Mode |= USART_Mode_Rx; }

            Config->USART_Arch.Parity = USART_Parity_No;
            if ( Config->Init.Flags.ParityEven ) { Config->USART_Arch.Parity = USART_Parity_Even; }
            if ( Config->Init.Flags.ParityOdd )  { Config->USART_Arch.Parity = USART_Parity_Odd; }

            switch ( Config->Init.HalfStopBits ) {
                case 1: Config->USART_Arch.StopBits  = USART_StopBits_0_5; break;
                case 3: Config->USART_Arch.StopBits  = USART_StopBits_1_5; break;
                case 4: Config->USART_Arch.StopBits  = USART_StopBits_2;   break;
                default:
                case 2: Config->USART_Arch.StopBits  = USART_StopBits_1;   break;
            }

            switch ( Config->Init.WordLength ) {
                case 9: Config->USART_Arch.WordLength = USART_WordLength_9b; break;

                default:
                case 8: Config->USART_Arch.WordLength = USART_WordLength_8b; break;
            }

            // translate USART_MyInit back to Config
            switch ( Config->USART_Arch.HardwareFlowControl ) {
                case USART_HardwareFlowControl_RTS:
                    Config->Init.Flags.RtsCts = 0;
                    Config->Init.Flags.Rts   = 1;
                    Config->Init.Flags.Cts   = 0;
                    break;
                case USART_HardwareFlowControl_CTS:
                    Config->Init.Flags.RtsCts = 0;
                    Config->Init.Flags.Rts   = 0;
                    Config->Init.Flags.Cts   = 1;
                    break;
                case USART_HardwareFlowControl_RTS_CTS:
                    Config->Init.Flags.RtsCts = 1;
                    Config->Init.Flags.Rts   = 1;
                    Config->Init.Flags.Cts   = 1;
                    break;
                default:
                    Config->Init.Flags.RtsCts = 0;
                    Config->Init.Flags.Rts   = 0;
                    Config->Init.Flags.Cts   = 0;
                    break;
            }
            switch ( Config->USART_Arch.Parity ) {
                case USART_Parity_Even:
                    Config->Init.Flags.ParityEven = 1;
                    Config->Init.Flags.ParityOdd = 0;
                    break;
                case USART_Parity_Odd:
                    Config->Init.Flags.ParityEven = 0;
                    Config->Init.Flags.ParityOdd = 1;
                    break;
                default:
                    Config->Init.Flags.ParityEven = 0;
                    Config->Init.Flags.ParityOdd = 0;
                    break;
            }
            switch ( Config->USART_Arch.StopBits ) {
                case USART_StopBits_0_5:  Config->Init.HalfStopBits = 1; break;
                case USART_StopBits_1_5:  Config->Init.HalfStopBits = 3; break;
                case USART_StopBits_2:    Config->Init.HalfStopBits = 4; break;
                default:
                case USART_StopBits_1:    Config->Init.HalfStopBits = 2; break;
            }
            switch ( Config->USART_Arch.WordLength ) {
                case USART_WordLength_9b:
                    Config->Init.WordLength = 9; break;
                default:
                    Config->Init.WordLength = 8; break;
            }

            if ( Config->USART_Arch.Mode & USART_Mode_Tx ) { Config->Init.Flags.Transmit = 1; }
            if ( Config->USART_Arch.Mode & USART_Mode_Rx ) { Config->Init.Flags.Receive = 1; }

            _usart_stm32f1xx_init( Config );

            */

    return ec_usart_OK;
}
e_ttc_usart_errorcode usart_stm32f1xx_send_word_blocking( volatile t_ttc_usart_register_tx* Register, const t_u16 Word ) {
    Assert_USART( Register != NULL, ttc_assert_origin_auto );

    return _usart_stm32f1xx_send_word_blocking( Register, Word );
}
e_ttc_usart_errorcode usart_stm32f1xx_read_byte_blocking( volatile t_register_stm32f1xx_usart* Register, t_u8* Byte ) {

    if ( _usart_stm32f1xx_wait_for_RXNE( Register ) )
    { return ec_usart_TimeOut; }

    *Byte = ( t_u8 ) 0xff & ( ( USART_TypeDef* ) Register )->DR;

    return ec_usart_OK;
}
e_ttc_usart_errorcode usart_stm32f1xx_read_word_blocking( volatile t_register_stm32f1xx_usart* Register, t_u16* Word ) {

    if ( _usart_stm32f1xx_wait_for_RXNE( Register ) )
    { return ec_usart_TimeOut; }

    *Word = 0x1ff & ( ( USART_TypeDef* ) Register )->DR;

    return ec_usart_OK;
}
t_u8                  usart_stm32f1xx_get_physicalindex( t_ttc_usart_config* Config ) {
    Assert_USART( Config != NULL, ttc_assert_origin_auto );

    return Config->PhysicalIndex;
}
t_ttc_usart_config*   usart_stm32f1xx_get_configuration( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( t_ttc_usart_config* ) 0;
}
e_ttc_usart_errorcode usart_stm32f1xx_read_byte( volatile t_ttc_usart_register_rx* Register, t_u8* Byte ) {
    Assert_USART_Writable( ( void* ) Register, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_USART_Writable( Byte, ttc_assert_origin_auto ); // pointers must not be NULL

    *Byte = ( t_u8 ) 0xff & ( ( USART_TypeDef* ) Register )->DR;

    return ( e_ttc_usart_errorcode ) 0;
}
t_u8                  usart_stm32f1xx_check_bytes_available( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    if ( Config->BaseRegister->SR.Bits.RXNE )
    { return 1; }
    return 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************

e_ttc_usart_errorcode _usart_stm32f1xx_send_word_blocking( volatile t_register_stm32f1xx_usart* MyUSART, const t_u16 Word ) {
    Assert_USART( MyUSART != NULL, ttc_assert_origin_auto );

    e_ttc_usart_errorcode  Error = _usart_stm32f1xx_wait_for_TXE( MyUSART );
    if ( Error != ec_usart_OK )
    { return Error; }

    usart_stm32f1xx_send_byte_isr( &( MyUSART->DR ), Word );
    return ec_usart_OK;
}
e_ttc_usart_errorcode _usart_stm32f1xx_wait_for_RXNE( volatile t_register_stm32f1xx_usart* MyUSART ) {

    if ( ttc_usart_Timeout != ( t_base ) - 1 ) { // wait until word received or timeout
        t_base TimeOut = ttc_usart_Timeout;
        while ( MyUSART->SR.Bits.RXNE == 0 ) {
            if ( TimeOut-- == 0 )  { return ec_usart_TimeOut; }
            ttc_task_yield();
        }
    }
    else {             // wait until word received (no timeout)
        while ( MyUSART->SR.Bits.RXNE == 0 )
        { ttc_task_yield(); }
    }

    return ec_usart_OK;
}
e_ttc_usart_errorcode _usart_stm32f1xx_wait_for_TXE( volatile t_register_stm32f1xx_usart* MyUSART ) {

    while ( ( MyUSART->SR.All & USART_FLAG_TXE ) == 0 ) {
        ttc_task_yield();
    }

    return ec_usart_OK;
}
void                  usart_stm32f1xx_rx_isr( t_physical_index PhysicalIndex, void* Argument ) {
    Assert_USART( PhysicalIndex <= TTC_USART_AMOUNT, ttc_assert_origin_auto );
    t_ttc_usart_config* Config = ( t_ttc_usart_config* ) Argument;
    volatile t_register_stm32f1xx_usart* USART_Base = Config->BaseRegister;

    t_u8 Byte = ( t_u8 ) 0xff & ( ( USART_TypeDef* ) USART_Base )->DR;

    _ttc_usart_rx_isr( ( t_ttc_usart_config* ) Argument, Byte ); // provide received byte to registered function

    // clear interrupt status flag to allow next interrupt
    if ( 0 ) // more readable
    { USART_Base->SR.Bits.RXNE = 0; }
    else     // faster
    { ( ( USART_TypeDef* ) USART_Base )->SR &= 0xff - ( 1 << 5 ); }
    TODO( "Use bit banding for fastest access!" )
}
void                  usart_stm32f1xx_tx_isr( t_physical_index PhysicalIndex, void* Argument ) {
    Assert_USART( PhysicalIndex <= TTC_USART_AMOUNT, ttc_assert_origin_auto );
    Assert_USART( ttc_memory_is_writable( Argument ), ttc_assert_origin_auto );

    t_ttc_usart_config* Config = ( t_ttc_usart_config* ) Argument;
    volatile t_register_stm32f1xx_usart* USART_Base = Config->BaseRegister; //X _usart_stm32f1xx_get_base_register(PhysicalIndex);

    BOOL BytesRemaining;
    do {
        BytesRemaining = _ttc_usart_tx_isr( Config, & ( USART_Base->DR ) ); // let high level driver choose next byte to send
        USART_Base->SR.Bits.TXE = 0;
    }
    while ( BytesRemaining && USART_Base->SR.Bits.TXE ); // stay in isr for high data rates
}
void                  _usart_stm32f1xx_start( volatile t_register_stm32f1xx_usart* Register ) {

    /* Enable the selected USART by setting the UE bit in the CR1 register */
    Register->CR1.Bits.UE = 1; //.All |= USART_CR1_UE;

}
e_ttc_usart_errorcode _usart_stm32f1xx_configure_baudrate( t_ttc_usart_config* Config ) {
    Assert_USART_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    t_ttc_sysclock_architecture* Frequencies = &( ttc_sysclock_get_configuration()->LowLevelConfig );

    t_u32 Clock = ( Config->PhysicalIndex == 0 ) ?
                  Frequencies->frequency_apb2 :  // USART1 is clocked from APB2
                  Frequencies->frequency_apb2;   // all other USARTs are clocked from APB1


    t_register_stm32f1xx_usart_brr  BRR;
    t_u32 IntegerDivider = ( ( 25 * Clock ) / ( 2 * Config->Init.BaudRate ) );

    // Integer part computing in case Oversampling mode is 8 Samples or 16 Samples
    BRR.Bits.DIV_Mantissa = ( Config->BaseRegister->CR1.Bits.OVER8 ) ?
                            IntegerDivider / 100 :
                            IntegerDivider / 200;

    // Fraction is rest from division
    t_u32 FractionalDivider = IntegerDivider - ( BRR.Bits.DIV_Mantissa * 100 );

    BRR.Bits.DIV_Fraction = ( Config->BaseRegister->CR1.Bits.OVER8 ) ?
                            ( ( FractionalDivider *  8 ) + 50 ) / 100 :
                            ( ( FractionalDivider * 16 ) + 50 ) / 100;

    // update bitrate register
    Config->BaseRegister->BRR.All = BRR.All;

    /* DEPRECATED
    t_u32 apbclock = 0x00;
    t_u32 integerdivider = 0x00;
    t_u32 fractionaldivider = 0x00;
    RCC_ClocksTypeDef RCC_ClocksStatus;

    t_u32 tmpreg;
    volatile t_register_stm32f1xx_usart* Register = Config->BaseRegister;

    //---------------------------- USART BRR Configuration -----------------------
    // Configure the USART Baud Rate -------------------------------------------
    RCC_GetClocksFreq( &RCC_ClocksStatus );

    if ( Register == ( t_register_stm32f1xx_usart* )USART1 ) {
        apbclock = RCC_ClocksStatus.PCLK2_Frequency;
    }
    else {
        apbclock = RCC_ClocksStatus.PCLK1_Frequency;
    }

    // Determine the integer part
    if ( ( Register->CR1.All & ( t_u16 )0x8000 ) != 0 ) {
        // Integer part computing in case Oversampling mode is 8 Samples
        integerdivider = ( ( 25 * apbclock ) / ( 2 * Config->Init.BaudRate ) );
    }
    else { // if ((USARTx->CR1 & CR1_OVER8_Set) == 0)
        // Integer part computing in case Oversampling mode is 16 Samples
        integerdivider = ( ( 25 * apbclock ) / ( 4 * Config->Init.BaudRate ) );
    }
    tmpreg = ( integerdivider / 100 ) << 4;

    // Determine the fractional part
    fractionaldivider = integerdivider - ( 100 * ( tmpreg >> 4 ) );

    // Implement the fractional part in the register
    if ( ( Register->CR1.All & ( t_u16 )0x8000 ) != 0 ) {
        tmpreg |= ( ( ( ( fractionaldivider * 8 ) + 50 ) / 100 ) ) & ( ( t_u8 )0x07 );
    }
    else { // if ((USARTx->CR1 & CR1_OVER8_Set) == 0)
        tmpreg |= ( ( ( ( fractionaldivider * 16 ) + 50 ) / 100 ) ) & ( ( t_u8 )0x0F );
    }

    // Write to USART BRR
    Register->BRR.All = ( t_u16 )tmpreg;
    */

    return ec_usart_OK;

}
/* DEPRECATED
    e_ttc_usart_errorcode _usart_stm32f1xx_init( t_ttc_usart_config * Config ) {
        Assert_USART_Writable( Config, ttc_assert_origin_auto );

        t_u32 tmpreg = 0x00;


        t_register_stm32f1xx_usart* Register;

        Register = Config->BaseRegister;

        //usartxbase = (t_u32)USARTx;

        //---------------------------- USART CR2 Configuration -----------------------
        tmpreg = Register->CR2.All;

        // Clear STOP[13:12] bits

        tmpreg &= ( t_u16 ) 0xCFFF;

        // Configure the USART Stop Bits, Clock, CPOL, CPHA and LastBit ------------
        // Set STOP[13:12] bits according to USART_StopBits value
        tmpreg |= ( t_u32 )Config->USART_Arch.StopBits;

        // Write to USART CR2
        Register->CR2.All = ( t_u16 )tmpreg;

        //---------------------------- USART CR1 Configuration -----------------------
        tmpreg = Register->CR1.All;
        // Clear M, PCE, PS, TE and RE bits
        tmpreg &= ( t_u16 )0xE9F3;

        // Configure the USART Word Length, Parity and mode -----------------------
        // Set the M bits according to USART_WordLength value
        // Set PCE and PS bits according to USART_Parity value
        // Set TE and RE bits according to USART_Mode value
        tmpreg |= ( t_u32 )Config->USART_Arch.WordLength | Config->USART_Arch.Parity | Config->USART_Arch.Mode;

        // Write to USART CR1
        Register->CR1.All = ( t_u16 )tmpreg;

        //---------------------------- USART CR3 Configuration -----------------------
        tmpreg = Register->CR3.All;
        // Clear CTSE and RTSE bits
        tmpreg &= 0xFCFF;
        // Configure the USART HFC -------------------------------------------------
        // Set CTSE and RTSE bits according to USART_HardwareFlowControl value
        tmpreg |= Config->USART_Arch.HardwareFlowControl;
        // Write to USART CR3
        Register->CR3.All = ( t_u16 )tmpreg;

        _usart_stm32f1xx_configure_baudrate( Config );

        return ec_usart_OK;

    }
        */
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
