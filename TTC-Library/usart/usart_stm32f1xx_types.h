#ifndef ARCHITECTURE_USART_TYPES_H
#define ARCHITECTURE_USART_TYPES_H


/** { usart_stm32f1xx_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for USART devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by ttc_usart_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140217 09:36:59 UTC
 *
 *  Note: See ttc_usart.h for description of architecture independent USART implementation.
 *
 *  Authors: Gregor Rebel and Adrián Romero
 *
}*/
//{ Defines/ TypeDefs ******************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

// maximum amount of available USART/ UART units in current architecture
#define TTC_USART_MAX_AMOUNT 5

//InsertEnums above (DO NOT REMOVE THIS LINE!)

#define _driver_usart_rx_isr    usart_stm32f1xx_rx_isr // define function pointer for receive interrupt service routine
#define _driver_usart_tx_isr    usart_stm32f1xx_tx_isr // define function pointer for transmit interrupt service routine
#define t_ttc_usart_register    t_register_stm32f1xx_usart
#define t_ttc_usart_register_tx t_register_stm32f1xx_usart
#define t_ttc_usart_register_rx t_register_stm32f1xx_usart
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic.h"
#include "../gpio/gpio_stm32f1xx.h"
#include "../register/register_stm32f1xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_usart_types.h ***********************

typedef struct { // t_ttc_interrupt_usart_errors - support for ttc_interrupt_types.h
    unsigned Overrun  : 1;
    unsigned Noise    : 1;
    unsigned Framing  : 1;
    unsigned Parity   : 1;
    unsigned reserved : 4;

} t_ttc_interrupt_usart_errors;

typedef struct { // register description (adapt according to stm32f1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_usart_register;

/* Unused
typedef struct {  // stm32f1xx specific configuration data of single USART device
    //X    t_register_stm32f1xx_usart* volatile BaseRegister;  // base address of USART device registers
    //X e_ttc_gpio_pin PortTxD;                    // port pin for TxD
    //X e_ttc_gpio_pin PortRxD;                    // port pin for RxD
    //X e_ttc_gpio_pin PortRTS;                    // port pin for RTS
    //X e_ttc_gpio_pin PortCTS;                    // port pin for CTS
    //X e_ttc_gpio_pin PortCLK;                    // port pin for CLK

    //?    t_u32 BaudRate;
    //?    t_u16 WordLength;
    //?    t_u16 StopBits;
    //?    t_u16 Parity;
    //?    t_u16 Mode;
    //?    t_u16 HardwareFlowControl;
} __attribute__( ( __packed__ ) ) t_usart_stm32f1xx_config;
// ttc_usart_arch_t is required by ttc_usart_types.h
#define t_ttc_usart_architecture t_usart_stm32f1xx_config
*/
#undef t_ttc_usart_architecture // not storing low-level configuration data

//} Structures/ Enums


#endif //ARCHITECTURE_USART_TYPES_H
