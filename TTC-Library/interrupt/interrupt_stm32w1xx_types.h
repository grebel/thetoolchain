#ifndef INTERRUPT_STM32W1XX_TYPES_H
#define INTERRUPT_STM32W1XX_TYPES_H

/** { interrupt_stm32w1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for INTERRUPT devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by ttc_interrupt_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140224 21:37:20 UTC
 *
 *  Note: See ttc_interrupt.h for description of architecture independent INTERRUPT implementation.
 *
 *  Authors: Gregor Rebel, Greg Knoll
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_INTERRUPT1   // device not defined in makefile
    #define TTC_INTERRUPT1    E_ttc_interrupt_architecture_stm32w1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

#define TTC_INTERRUPT_GPIO_AMOUNT  4 // up to four external interrupts
#define TTC_INTERRUPT_USART_AMOUNT 1 // only one usart
#define TTC_INTERRUPT_TIMER_AMOUNT 0 // ToDo: implement

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_interrupt_types.h *************************

typedef enum { // NVIC peripheral interrupt map (STM32_PM0056_Cortex-M3_Programming_Manual.pdf p.192)
    isil_tmr1      = 0,
    isil_tmr2      = 1,
    isil_mgmt      = 2,
    isil_bb        = 3,
    isil_sleeptmr  = 4,
    isil_sc1       = 5,
    isil_sc2       = 6,
    isil_sec       = 7,
    isil_mactmr    = 8,
    isil_mactx     = 9,
    isil_macrx     = 10,
    isil_adc       = 11,
    isil_irqa      = 12,
    isil_irqb      = 13,
    isil_irqc      = 14,
    isil_irqd      = 15,
    isil_debug     = 16,
    isil_unknown   = 255
} e_interrupt_stm32w_interrupt_lines;

typedef struct { // register description (adapt according to stm32w1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_interrupt_register;

typedef struct {  // stm32w1xx specific configuration data of single INTERRUPT device
    t_interrupt_register* BaseRegister;       // base address of INTERRUPT device registers
} __attribute__( ( __packed__ ) ) t_interrupt_stm32w1xx_config;

// t_ttc_interrupt_architecture is required by ttc_interrupt_types.h
#define t_ttc_interrupt_architecture t_interrupt_stm32w1xx_config

typedef struct {
    t_u8 InterruptLine;        // CortexM3 NVIC interrupt line to use for external interrupt
    t_u32 RegisterSetMask;     // bitmask required to set/ test interrupt line in responsible 32 bit register
} t_interrupt_stm32w1xx_gpio;
#define interrupt_driver_config_gpio_t t_interrupt_stm32w1xx_gpio

//} Structures/ Enums


#endif //INTERRUPT_STM32W1XX_TYPES_H
