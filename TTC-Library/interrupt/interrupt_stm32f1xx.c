/** { interrupt_stm32f1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for interrupt devices on stm32f1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 20 at 20140224 21:37:20 UTC
 *
 *  Note: See ttc_interrupt.h for description of stm32f1xx independent INTERRUPT implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "interrupt_stm32f1xx.h"
#include "../ttc_heap.h"

/****************************************************************************//**
 * @brief Bit definitions for register SYS_STATUS
**/
#define SYS_STATUS_ID           0x0F            /* System event Status Register */
#define SYS_STATUS_LEN          (5)             /* Note 40 bit register */
/*masks */
#define SYS_STATUS_MASK_32      0xFFF7FFFFUL    /* System event Status Register access mask (all unused fields should always be writen as zero) */
/*offset 0 */
#define SYS_STATUS_IRQS         0x00000001UL    /* Interrupt Request Status READ ONLY */
#define SYS_STATUS_CPLOCK       0x00000002UL    /* Clock PLL Lock */
#define SYS_STATUS_ESYNCR       0x00000004UL    /* External Sync Clock Reset */
#define SYS_STATUS_AAT          0x00000008UL    /* Automatic Acknowledge Trigger */
#define SYS_STATUS_TXFRB        0x00000010UL    /* Transmit Frame Begins */
#define SYS_STATUS_TXPRS        0x00000020UL    /* Transmit Preamble Sent */
#define SYS_STATUS_TXPHS        0x00000040UL    /* Transmit PHY Header Sent */
#define SYS_STATUS_TXFRS        0x00000080UL    /* Transmit Frame Sent: This is set when the transmitter has completed the sending of a frame */
/*offset 8 */
#define SYS_STATUS_RXPRD        0x00000100UL    /* Receiver Preamble Detected status */
#define SYS_STATUS_RXSFDD       0x00000200UL    /* Receiver Start Frame Delimiter Detected. */
#define SYS_STATUS_LDEDONE      0x00000400UL    /* LDE processing done */
#define SYS_STATUS_RXPHD        0x00000800UL    /* Receiver PHY Header Detect */
#define SYS_STATUS_RXPHE        0x00001000UL    /* Receiver PHY Header Error */
#define SYS_STATUS_RXDFR        0x00002000UL    /* Receiver Data Frame Ready */
#define SYS_STATUS_RXFCG        0x00004000UL    /* Receiver FCS Good */
#define SYS_STATUS_RXFCE        0x00008000UL    /* Receiver FCS Error */
/*offset 16 */
#define SYS_STATUS_RXRFSL       0x00010000UL    /* Receiver Reed Solomon Frame Sync Loss */
#define SYS_STATUS_RXRFTO       0x00020000UL    /* Receive Frame Wait Timeout */
#define SYS_STATUS_LDEERR       0x00040000UL    /* Leading edge detection processing error */
#define SYS_STATUS_reserved     0x00080000UL    /* bit19 reserved */
#define SYS_STATUS_RXOVRR       0x00100000UL    /* Receiver Overrun */
#define SYS_STATUS_RXPTO        0x00200000UL    /* Preamble detection timeout */
#define SYS_STATUS_GPIOIRQ      0x00400000UL    /* GPIO interrupt */
#define SYS_STATUS_SLP2INIT     0x00800000UL    /* SLEEP to INIT */
/*offset 24 */
#define SYS_STATUS_RFPLL_LL     0x01000000UL    /* RF PLL Losing Lock */
#define SYS_STATUS_CLKPLL_LL    0x02000000UL    /* Clock PLL Losing Lock */
#define SYS_STATUS_RXSFDTO      0x04000000UL    /* Receive SFD timeout */
#define SYS_STATUS_HPDWARN      0x08000000UL    /* Half Period Delay Warning */
#define SYS_STATUS_TXBERR       0x10000000UL    /* Transmit Buffer Error */
#define SYS_STATUS_AFFREJ       0x20000000UL    /* Automatic Frame Filtering rejection */
#define SYS_STATUS_HSRBP        0x40000000UL    /* Host Side Receive Buffer Pointer */
#define SYS_STATUS_ICRBP        0x80000000UL    /* IC side Receive Buffer Pointer READ ONLY */
/*offset 32 */
#define SYS_STATUS_RXRSCS       0x0100000000ULL /* Receiver Reed-Solomon Correction Status */
#define SYS_STATUS_RXPREJ       0x0200000000ULL /* Receiver Preamble Rejection */
#define SYS_STATUS_TXPUTE       0x0400000000ULL /* Transmit power up time error */

#define CLEAR_ALLRXGOOD_EVENTS  (SYS_STATUS_RXDFR | SYS_STATUS_RXFCG | SYS_STATUS_RXPRD |                                  SYS_STATUS_RXSFDD | SYS_STATUS_RXPHD | SYS_STATUS_LDEDONE) /* Clear all RX event flags after a packet reception */
#define CLEAR_DBLBUFF_EVENTS    (SYS_STATUS_RXDFR | SYS_STATUS_RXFCG) //| SYS_STATUS_LDEDONE)

#define CLEAR_ALLRXERROR_EVENTS (SYS_STATUS_RXPHE | SYS_STATUS_RXFCE | SYS_STATUS_RXRFSL |                                  SYS_STATUS_RXSFDTO | SYS_STATUS_RXRFTO | SYS_STATUS_RXPTO |                                 SYS_STATUS_AFFREJ | SYS_STATUS_LDEERR) /* Clear all RX event flags after an rx error */

#define CLEAR_ALLTX_EVENTS      (SYS_STATUS_AAT | SYS_STATUS_TXFRB | SYS_STATUS_TXPRS |                                  SYS_STATUS_TXPHS | SYS_STATUS_TXFRS    ) /* Clear all TX event flags */


//{ Global Variables ***********************************************************

// All initializers use same configuration variable to safe memory and speed up initialization.
// Note: This makes interrupt initialization thread unsafe! Always enclose your initialisation calls in critical sections!
NVIC_InitTypeDef interrupt_stm32f1xx_NVIC_Cfg;

// provided by ttc_interrupt.c
#ifdef EXTENSION_ttc_usart
    A_dynamic_extern( t_ttc_interrupt_config_usart*, ttc_interrupt_config_usart );
#endif
#ifdef EXTENSION_ttc_gpio
    A_dynamic_extern( t_ttc_interrupt_config_gpio*,  ttc_interrupt_config_gpio );
#endif
#ifdef EXTENSION_ttc_timer
    A_dynamic_extern( t_ttc_interrupt_configimer_t*,  t_ttc_interrupt_configimer );
#endif
#ifdef EXTENSION_ttc_can
    A_dynamic_extern( t_ttc_interrupt_config_can*,  ttc_interrupt_config_can );
#endif

//}
//{ Function definitions *******************************************************

void interrupt_stm32f1xx_prepare() {

}
#ifdef EXTENSION_ttc_gpio //{
const t_u8 interrupt_stm32f1xx_IRQChannel_GPIO[TTC_INTERRUPT_GPIO_AMOUNT] = { EXTI0_IRQn,
                                                                              EXTI1_IRQn,
                                                                              EXTI2_IRQn,
                                                                              EXTI3_IRQn,
                                                                              EXTI4_IRQn,
                                                                              EXTI9_5_IRQn,
                                                                              EXTI9_5_IRQn,
                                                                              EXTI9_5_IRQn,
                                                                              EXTI9_5_IRQn,
                                                                              EXTI9_5_IRQn,
                                                                              EXTI15_10_IRQn,
                                                                              EXTI15_10_IRQn,
                                                                              EXTI15_10_IRQn,
                                                                              EXTI15_10_IRQn,
                                                                              EXTI15_10_IRQn,
                                                                              EXTI15_10_IRQn
                                                                            };

e_ttc_interrupt_errorcode interrupt_stm32f1xx_init_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {
#if 1
    t_ttc_gpio_bank Bank;
    t_u8 Pin;
    ttc_gpio_from_index( Config_ISRs->PhysicalIndex, &Bank, &Pin );
    Assert_INTERRUPT( Bank != 0, ttc_assert_origin_auto );
    Assert_INTERRUPT( Pin < TTC_GPIO_MAX_PINS, ttc_assert_origin_auto );

    // ensure interrupt is disabled before reconfiguring it
    interrupt_stm32f1xx_disable_gpio( Config_ISRs );

    // initialize GPIO pin as input
    ttc_gpio_init( Config_ISRs->PhysicalIndex, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_max );

    if ( 1 ) { // configure interrupt source multiplexer for EXTI line
        // enable AFIO clockc
        register_stm32f1xx_RCC.APB2ENR.AFIO_EN = 1;

        t_u8 Value = 0;
        switch ( Bank ) {
            case E_ttc_gpio_bank_a: Value = gis_pa; break;
            case E_ttc_gpio_bank_b: Value = gis_pb; break;
            case E_ttc_gpio_bank_c: Value = gis_pc; break;
            case E_ttc_gpio_bank_d: Value = gis_pd; break;
            case E_ttc_gpio_bank_e: Value = gis_pe; break;
            case E_ttc_gpio_bank_f: Value = gis_pf; break;
            case E_ttc_gpio_bank_g: Value = gis_pg; break;
            default: Assert_INTERRUPT_EXTRA( 0, ttc_assert_origin_auto );
        }

        switch ( Pin ) { // configure EXTI line multiplexer that corresponds to given pin
            case  0: register_stm32f1xx_AFIO.EXTICR1.EXTI0  = Value; break;
            case  1: register_stm32f1xx_AFIO.EXTICR1.EXTI1  = Value; break;
            case  2: register_stm32f1xx_AFIO.EXTICR1.EXTI2  = Value; break;
            case  3: register_stm32f1xx_AFIO.EXTICR1.EXTI3  = Value; break;
            case  4: register_stm32f1xx_AFIO.EXTICR2.EXTI4  = Value; break;
            case  5: register_stm32f1xx_AFIO.EXTICR2.EXTI5  = Value; break;
            case  6: register_stm32f1xx_AFIO.EXTICR2.EXTI6  = Value; break;
            case  7: register_stm32f1xx_AFIO.EXTICR2.EXTI7  = Value; break;
            case  8: register_stm32f1xx_AFIO.EXTICR3.EXTI8  = Value; break;
            case  9: register_stm32f1xx_AFIO.EXTICR3.EXTI9  = Value; break;
            case 10: register_stm32f1xx_AFIO.EXTICR3.EXTI10 = Value; break;
            case 11: register_stm32f1xx_AFIO.EXTICR3.EXTI11 = Value; break;
            case 12: register_stm32f1xx_AFIO.EXTICR4.EXTI12 = Value; break;
            case 13: register_stm32f1xx_AFIO.EXTICR4.EXTI13 = Value; break;
            case 14: register_stm32f1xx_AFIO.EXTICR4.EXTI14 = Value; break;
            case 15: register_stm32f1xx_AFIO.EXTICR4.EXTI15 = Value; break;
        }
    }

    // calculate bit banding address of EXTI-line (==Pin) in first register of
    t_u32* BitBand_EXTI = cm3_calc_peripheral_BitBandAddress( &register_stm32f1xx_EXTI, Pin );

    // calculate bit banding adresses of corresponding bits in EXTI sub registers by adding constant register offsets
    t_u32* BitBand_RTSR = ( t_u32* )( ( ( t_u32 ) BitBand_EXTI )
                                      + 32 * ( ( ( t_u32 ) &register_stm32f1xx_EXTI.RTSR ) - ( ( t_u32 ) &register_stm32f1xx_EXTI ) )
                                    );
    t_u32* BitBand_FTSR = ( t_u32* )( ( ( t_u32 ) BitBand_EXTI )
                                      + 32 * ( ( ( t_u32 ) &register_stm32f1xx_EXTI.FTSR ) - ( ( t_u32 ) &register_stm32f1xx_EXTI ) )
                                    );

    switch ( Config_ISRs->Common.Type ) { // configure trigger (EXTI_RTSR + EXTI_FTSR)

        case  tit_GPIO_Active_Low:         // voltage on input pin is low
        case  tit_GPIO_Falling:            // voltage on input pin falls from logical 1 to 0

            *BitBand_RTSR = 0; // disable rising trigger
            *BitBand_FTSR = 1; // enable falling trigger
            break;

        case  tit_GPIO_Active_High:        // voltage on input pin is high
        case  tit_GPIO_Rising:             // voltage on input pin rises from logical 0 to 1

            *BitBand_FTSR = 0; // disable falling trigger
            *BitBand_RTSR = 1; // enable rising trigger
            break;

        case  tit_GPIO_Rising_Falling:     // voltage on input pin falls or rises
            *BitBand_FTSR = 1; // enable falling trigger
            *BitBand_RTSR = 1; // enable rising trigger
            break;

        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown/ invalid interrupt type!
    }

    t_u8 IRQ_Channel = interrupt_stm32f1xx_IRQChannel_GPIO[Pin];
    Assert_INTERRUPT( IRQ_Channel < 57, ttc_assert_origin_auto );  // only interrupt lines 0..56 available in STM32L1xx (->RM0038 p,181,182)

    // set interrupt priority of corresponding IRQ channel in NVIC
    interrupt_cortexm3_priority( IRQ_Channel, 0, 3 );

    // enable selected NVIC IRQ channel
    interrupt_cortexm3_enable( IRQ_Channel, 1 );

#else // initialize using StdPeripheral Library
    GPIO_TypeDef* GPIOx;
    t_u8 Pin;

    ttc_gpio_from_index( Config_ISRs->PhysicalIndex, ( t_ttc_gpio_bank* ) &GPIOx, &Pin );
    Assert_INTERRUPT_Writable( GPIOx, ttc_assert_origin_auto );
    Assert_INTERRUPT( Pin < TTC_GPIO_MAX_PINS, ttc_assert_origin_auto );


    EXTI_InitTypeDef EXTI_Cfg;

    t_u8 GPIO_PortSource = 0;
    t_u8 GPIO_PinType    = Pin;

    EXTI_StructInit( &EXTI_Cfg );

    /* Enable AFIO clock */
    RCC_APB2PeriphClockCmd( RCC_APB2Periph_AFIO, ENABLE );

    switch ( ( t_u32 ) GPIOx ) {
#ifdef GPIOA
        case ( t_u32 ) GPIOA:
            GPIO_PortSource = GPIO_PortSourceGPIOA;
            break;
#endif
#ifdef GPIOB
        case ( t_u32 ) GPIOB:
            GPIO_PortSource = GPIO_PortSourceGPIOB;
            break;
#endif
#ifdef GPIOC
        case ( t_u32 ) GPIOC:
            GPIO_PortSource = GPIO_PortSourceGPIOC;
            break;
#endif
#ifdef GPIOD
        case ( t_u32 ) GPIOD:
            GPIO_PortSource = GPIO_PortSourceGPIOD;
            break;
#endif
#ifdef GPIOE
        case ( t_u32 ) GPIOE:
            GPIO_PortSource = GPIO_PortSourceGPIOE;
            break;
#endif
#ifdef GPIOF
        case ( t_u32 ) GPIOF:
            GPIO_PortSource = GPIO_PortSourceGPIOF;
            break;
#endif
#ifdef GPIOG
        case ( t_u32 ) GPIOG:
            GPIO_PortSource = GPIO_PortSourceGPIOG;
            break;
#endif
        default:
            ttc_assert_halt_origin( ttc_assert_origin_auto ); break;
    }

    EXTI_Cfg.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_Cfg.EXTI_Line = 1 << Pin;

    switch ( Type ) { // find memory location where to store function pointer + argument

        case tit_GPIO_Falling:
            EXTI_Cfg.EXTI_Trigger = EXTI_Trigger_Falling;
            break;

        case tit_GPIO_Rising:
            EXTI_Cfg.EXTI_Trigger = EXTI_Trigger_Rising;
            break;

        case tit_GPIO_Rising_Falling:
            EXTI_Cfg.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
            break;

        default:
            ttc_assert_halt_origin( ttc_assert_origin_auto );
            break;
    }

    //configure the External interrupt/event line mapping
    GPIO_EXTILineConfig( GPIO_PortSource, GPIO_PinType );

    EXTI_Cfg.EXTI_LineCmd = DISABLE;
    EXTI_Init( &EXTI_Cfg );

    // 32-bit architecture allows fast constant arrays with direct pointer access
    static const t_u8 stm32_Interrupt_IRQChannel[TTC_INTERRUPT_GPIO_AMOUNT] = {
        EXTI0_IRQn,
        EXTI1_IRQn,
        EXTI2_IRQn,
        EXTI3_IRQn,
        EXTI4_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn
    };

    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel                   = stm32_Interrupt_IRQChannel[Pin];
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelPreemptionPriority = 0x0F;
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelSubPriority        = 0x0F;
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelCmd                = ENABLE;
    NVIC_Init( &interrupt_stm32f1xx_NVIC_Cfg );
#endif

    return ec_interrupt_OK;
}
void interrupt_stm32f1xx_deinit_all_gpio() {
    ttc_memory_set( ( t_u8* ) & register_stm32f1xx_EXTI, 0, sizeof( register_stm32f1xx_EXTI ) );
    t_ttc_interrupt_config_gpio Config_ISR;
    Config_ISR.PhysicalIndex = E_ttc_gpio_pin_a0;

    for ( t_u8 IndexEXTI = 0; IndexEXTI < 32; IndexEXTI++ ) { // deinit all 32 EXTI lines
        interrupt_stm32f1xx_deinit_gpio( &Config_ISR );
        Config_ISR.PhysicalIndex += 4;
    }
}
void interrupt_stm32f1xx_deinit_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

    interrupt_stm32f1xx_disable_gpio( Config_ISRs );

    if ( 1 ) { // disable interrupt channel in NVIC ICER register
        t_ttc_gpio_bank* GPIOx;
        t_u8 Pin;
        ttc_gpio_from_index( Config_ISRs->PhysicalIndex, ( t_ttc_gpio_bank* ) &GPIOx, &Pin );

        t_u8 IRQ_Channel = interrupt_stm32f1xx_IRQChannel_GPIO[Pin];
        t_u8 RegisterIndex = IRQ_Channel / 32;
        Assert_INTERRUPT( RegisterIndex < 3, ttc_assert_origin_auto );  // each NVIC register has three 32 bit fields!

        // disable selected IRQ channel
        volatile t_u32* ICER = &register_cortexm3_NVIC.ICER0.All;
        t_u32* BitBand_ICER = cm3_calc_peripheral_BitBandAddress( ICER + RegisterIndex, IRQ_Channel );
        *BitBand_ICER = 1; // set corresponding bit in ICER register

        // disble selected NVIC IRQ channel
        interrupt_cortexm3_enable( IRQ_Channel, 0 );
    }
}
void interrupt_stm32f1xx_disable_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {
#if 1
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

    GPIO_TypeDef* GPIOx;
    t_u8 Pin;
    ttc_gpio_from_index( Config_ISRs->PhysicalIndex, ( t_ttc_gpio_bank* ) &GPIOx, &Pin );

    // clear corresponding bit in Interrupt Mask Register
    volatile t_u32 IMR = * ( ( t_u32* ) &register_stm32f1xx_EXTI.IMR.All );
    IMR &= ~( 1 << Pin );
    register_stm32f1xx_EXTI.IMR.All = IMR;
#else
    EXTI_InitTypeDef* EXTI_Cfg = _interrupt_stm32f1xx_enable_gpio( Config_ISRs );
    EXTI_Cfg->EXTI_LineCmd = 0;

    EXTI_Init( EXTI_Cfg );
#endif
}
void interrupt_stm32f1xx_enable_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {
#if 1
#if (TTC_Assert_INTERRUPTS==1)
    Assert_INTERRUPT( !A_is_null( ttc_interrupt_config_gpio ), ttc_assert_origin_auto );  // init interrupt first!
    t_ttc_interrupt_config_gpio* GPIO_ISR_Entry = A( ttc_interrupt_config_gpio, Pin );
    Assert_INTERRUPT( GPIO_ISR_Entry, ttc_assert_origin_auto );  // init interrupt first!
#endif

    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

    t_ttc_gpio_bank* GPIOx;
    t_u8 Pin;
    ttc_gpio_from_index( Config_ISRs->PhysicalIndex, ( t_ttc_gpio_bank* ) &GPIOx, &Pin );
    t_u32 Mask = 1 << Pin;

    // set corresponding bit in Interrupt Mask Register
    register_stm32f1xx_EXTI.IMR.All |= Mask;

    // re-set pending bit (required to re-activate if interrupt already occured)
    register_stm32f1xx_EXTI.PR.All  |= Mask;

#else
    EXTI_InitTypeDef* EXTI_Cfg = _interrupt_stm32f1xx_enable_gpio( Config_ISRs );
    EXTI_Cfg->EXTI_LineCmd = 1;

    EXTI_Init( EXTI_Cfg );
#endif
}
void interrupt_stm32f1xx_generate_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {
#if (TTC_Assert_INTERRUPTS==1)
    Assert_INTERRUPT( !A_is_null( ttc_interrupt_config_gpio ), ttc_assert_origin_auto );  // init interrupt first!
    t_ttc_interrupt_config_gpio* GPIO_ISR_Entry = A( ttc_interrupt_config_gpio, Pin );
    Assert_INTERRUPT( GPIO_ISR_Entry, ttc_assert_origin_auto );  // init interrupt first!
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL
#endif

    t_ttc_gpio_bank* GPIOx;
    t_u8 Pin;
    ttc_gpio_from_index( Config_ISRs->PhysicalIndex, ( t_ttc_gpio_bank* ) &GPIOx, &Pin );
    t_u32 Mask = 1 << Pin;

    // set corresponding bit in Interrupt Mask Register to trigger interrupt
    register_stm32f1xx_EXTI.SWIER.All |= Mask;
}
t_u8 interrupt_stm32f1xx_gpio_calc_interruptline( t_base PhysicalIndex ) {
    t_u8 InterruptLine;
    t_ttc_gpio_bank GPIOx;

    // STM32F1xx provides one interrupt line for each pin index. All GPIO banks share the same interrupt lines.
    ttc_gpio_from_index( PhysicalIndex, &GPIOx, &InterruptLine );
    Assert_INTERRUPT( GPIOx != 0, ttc_assert_origin_auto );
    Assert_INTERRUPT( InterruptLine < TTC_INTERRUPT_GPIO_AMOUNT, ttc_assert_origin_auto );

    return InterruptLine;
}
/* moved to ttc_gpio_from_index
void interrupt_stm32f1xx_gpio_from_index(t_base PhysicalIndex, t_ttc_gpio_bank* GPIOx, t_u8* Pin) {
    Assert_GPIO(GPIOx); // pointers must not be NULL
    Assert_GPIO(Pin); // pointers must not be NULL

    *GPIOx = (t_ttc_gpio_bank) cm3_calc_peripheral_Word(PhysicalIndex);
    *Pin   = (t_u8)            cm3_calc_peripheral_BitNumber(PhysicalIndex);
}
*/
t_base interrupt_stm32f1xx_gpio_create_index( t_ttc_gpio_bank GPIOx, t_u8 Pin ) {
    return ( t_base ) cm3_calc_peripheral_BitBandAddress( GPIOx, Pin );
}
t_u8 interrupt_stm32f1xx_gpio_calc_config_index( t_base PhysicalIndex ) {

    e_ttc_gpio_pin GPIO = ( e_ttc_gpio_pin ) PhysicalIndex; // we know that physical index is bit banding address of gpio pin
    t_u8 PinNo = cm3_calc_peripheral_BitNumber( GPIO );

    return PinNo; // stm32f1xx has one interrupt line for each pin over all banks (E_ttc_gpio_pin_a1 == tgpb1 == ...) so only pin # is important
}

/* Interrupt handlers for GPIO on stm32f1xx
 *
 * The interrupt handling implemented here is a compromise between
 * handling speed and code size.
 *
 * The interrupt starts in one of the EXTI*_IRQHandler() functions below:
 * 1) Reset interrupt line as fast as possible.
 *    It writes a constant 32 bit value to register_stm32f1xx_EXTI.PR.
 *    The C code is a one liner that compiles to a very fast 32-bit constant write to PR register.
 * 2) GPIO_General_IRQHandler() is called with the current interrupt line.
 *    The use of a central interrupt handler avoids to duplicate its code seven times to reduce code size.
 * 3) GPIO_General_IRQHandler()
 *    a) Looks up registration entry for given interrupt line.
 *    b) Calls registered function pointer Entry->isr() with two arguments:
 *       - Entry->PhysicalIndex = GPIO pin identifyer
 *       - Entry->Argument      = Registered argument (32 bit value given to interrupt_stm32f1xx_init_gpio() before)
 *
 */
void GPIO_General_IRQHandler( t_physical_index InterruptLine ) {

    // declaring static variable is a little bit faster as we do not have to allocate stack space
    static t_ttc_interrupt_config_gpio* Entry = NULL;

    Entry = A( ttc_interrupt_config_gpio, InterruptLine );
    if ( Entry->isr )
    { Entry->isr( Entry->PhysicalIndex, Entry->Argument ); }
}
void EXTI0_IRQHandler() {
    // reset interrupt source (required to be retriggered)
    *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 0;

    // call the general GPIO Interrupt handler (will call registered interrupt service routine)
    GPIO_General_IRQHandler( 0 );
}
void EXTI1_IRQHandler() {
    // reset interrupt source (required to be retriggered)
    *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 1;
    GPIO_General_IRQHandler( 1 );
}
void EXTI2_IRQHandler() {
    // reset interrupt source (required to be retriggered)
    *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 2;

    GPIO_General_IRQHandler( 2 );
}
void EXTI3_IRQHandler() {
    // reset interrupt source (required to be retriggered)
    *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 3;

    GPIO_General_IRQHandler( 3 );
}
void EXTI4_IRQHandler() {
    // reset interrupt source (required to be retriggered)
    *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 4;

    GPIO_General_IRQHandler( 4 );
}
void EXTI9_5_IRQHandler() {

    t_u32 PR = *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR );

    if ( PR & 1 << 5 ) { // EXTI5 interrupt active
        // reset interrupt source (required to be retriggered)
        *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 5;

        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 5 );
    }
    if ( PR & 1 << 6 ) { // EXTI6 interrupt active
        // reset interrupt source (required to be retriggered)
        *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 6;

        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 6 );
    }
    if ( PR & 1 << 7 ) { // EXTI7 interrupt active
        // reset interrupt source (required to be retriggered)
        *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 7;

        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 7 );
    }
    if ( PR & 1 << 8 ) { // EXTI8 interrupt active
        // reset interrupt source (required to be retriggered)
        *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 8;

        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 8 );
    }
    if ( PR & 1 << 9 ) { // EXTI9 interrupt active
        // reset interrupt source (required to be retriggered)
        *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 9;

        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 9 );
    }
}
void EXTI15_10_IRQHandler() {

    t_u32 PR = *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR );

    if ( PR & 1 << 10 ) { // EXTI10 interrupt active
        // reset interrupt source (required to be retriggered)
        *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 10;

        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 10 );
    }
    if ( PR & 1 << 11 ) { // EXTI11 interrupt active
        // reset interrupt source (required to be retriggered)
        *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 11;

        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 11 );
    }
    if ( PR & 1 << 12 ) { // EXTI12 interrupt active
        // reset interrupt source (required to be retriggered)
        *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 12;

        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 12 );
    }
    if ( PR & 1 << 13 ) { // EXTI13 interrupt active
        // reset interrupt source (required to be retriggered)
        *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 13;

        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 13 );
    }
    if ( PR & 1 << 14 ) { // EXTI14 interrupt active
        // reset interrupt source (required to be retriggered)
        *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 14;

        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 14 );
    }
    if ( PR & 1 << 15 ) { // EXTI15 interrupt active
        // reset interrupt source (required to be retriggered)
        *( ( volatile t_u32* ) & register_stm32f1xx_EXTI.PR ) = 1 << 15;

        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 15 );
    }
}

#endif //}
#ifdef EXTENSION_ttc_i2c //{
TODO( "Implement I2C interrupt handling for stm32f1xx!" )

e_ttc_interrupt_errorcode interrupt_stm32f1xx_init_i2c( t_ttc_interrupt_config_i2c* Config_ISRs ) {

    ttc_assert_halt_origin( ttc_assert_origin_auto );

    return ( e_ttc_interrupt_errorcode ) 0;
}
void interrupt_stm32f1xx_enable_i2c( t_ttc_interrupt_config_i2c* Config_ISRs ) {

    ttc_assert_halt_origin( ttc_assert_origin_auto );
}
void interrupt_stm32f1xx_disable_i2c( t_ttc_interrupt_config_i2c* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL



}
void interrupt_stm32f1xx_deinit_all_i2c() {




}
void interrupt_stm32f1xx_deinit_i2c( t_ttc_interrupt_config_i2c* Config_ISRs ) {

}

#endif //}
#ifdef EXTENSION_ttc_spi //{
TODO( "Implement SPI interrupt handling for stm32f1xx!" )

e_ttc_interrupt_errorcode interrupt_stm32f1xx_init_spi( t_ttc_interrupt_config_spi* Config_ISRs ) {

    ttc_assert_halt_origin( ttc_assert_origin_auto ); // ToDo: implement!


    return ( e_ttc_interrupt_errorcode ) 0;
}
void interrupt_stm32f1xx_enable_spi( t_ttc_interrupt_config_spi* Config_ISRs ) {

    ttc_assert_halt_origin( ttc_assert_origin_auto ); // ToDo: implement!
}
void interrupt_stm32f1xx_deinit_all_spi() {




}
void interrupt_stm32f1xx_disable_spi( t_ttc_interrupt_config_spi* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL



}
void interrupt_stm32f1xx_deinit_spi( t_ttc_interrupt_config_spi* Config_ISRs ) {

}
#endif //}
#ifdef EXTENSION_ttc_timer //{

e_ttc_interrupt_errorcode interrupt_stm32f1xx_init_timer( t_ttc_interrupt_configimer_t* Config_ISRs ) {

    t_register_stm32f1xx_timer* TIMER_Base;
    ( void )TIMER_Base;     //Avoid the warning

    switch ( Config_ISRs->PhysicalIndex ) { // physical index starts is the logical index
        case 1: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER1;
            switch ( Config_ISRs->Common.Type ) {
                case tit_TIMER_Updating:
#  if defined (STM32F10X_LD_VL) || defined (STM32F10X_MD_VL) || defined (STM32F10X_HD_VL)
                    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM1_UP_TIM16_IRQn;
#  elif defined (STM32F10X_LD) || defined (STM32F10X_MD) || defined (STM32F10X_HD) || defined (STM32F10X_CL)
                    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM1_UP_IRQn;
#  else
                    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM1_UP_TIM10_IRQn;
#  endif
                    break;
                case tit_TIMER_Break:
#  if defined (STM32F10X_LD_VL) || defined (STM32F10X_MD_VL) || defined (STM32F10X_HD_VL)
                    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM1_BRK_TIM15_IRQn;
#  elif defined (STM32F10X_LD) || defined (STM32F10X_MD) || defined (STM32F10X_HD) || defined (STM32F10X_CL)
                    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM1_BRK_IRQn;
#  else
                    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM1_BRK_TIM9_IRQn;
#  endif
                    break;
                case tit_TIMER_CC1:
                    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM1_CC_IRQn;
                    break;
                case tit_TIMER_CC2:
                    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM1_CC_IRQn;
                    break;
                case tit_TIMER_CC3:
                    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM1_CC_IRQn;
                    break;
                case tit_TIMER_CC4:
                    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM1_CC_IRQn;
                    break;
                case tit_TIMER_Triggering:
#  if defined (STM32F10X_LD_VL) || defined (STM32F10X_MD_VL) || defined (STM32F10X_HD_VL)
                    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM1_TRG_COM_TIM17_IRQn;
#  elif defined (STM32F10X_LD) || defined (STM32F10X_MD) || defined (STM32F10X_HD) || defined (STM32F10X_CL)
                    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM1_TRG_COM_IRQn;
#  else
                    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM1_TRG_COM_TIM11_IRQn;
#  endif
                    break;
                default: ttc_assert_halt_origin( ttc_assert_origin_auto );
                    break;
            }
            break;
        case 2: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER2;
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM2_IRQn;
            break;
        case 3: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER3;
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM3_IRQn;
            break;
        case 4: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER4;
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM4_IRQn;
            break;
#if defined (STM32F10X_HD) || defined(STM32F10X_HD_VL) || defined(STM32F10X_XL) || defined (STM32F10X_CL)
        case 5: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER5;
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM5_IRQn;
            break;
#endif
#if defined(STM32F10X_LD_VL) || defined(STM32F10X_MD_VL) || defined(STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
        case 6: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER6;
#  if defined(STM32F10X_XL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM6_IRQn;
#  else
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  54;//TIM6_DAC_IRQn;
#  endif
            break;
        case 7: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER7;
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM7_IRQn;
            break;
#endif
#if defined(STM32F10X_HD) || defined(STM32F10X_XL)
        case 8: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER8;
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM8_IRQn;
            break;
#endif
#if defined (STM32F10X_XL)
        case 9: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER9;
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM1_BRK_TIM9_IRQn;
            break;
        case 10: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER10;
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM1_UP_TIM10_IRQn;
            break;
        case 11: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER11;
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM1_TRG_COM_TIM11_IRQn;
            break;
#endif
#if defined (STM32F10X_HD_VL) || defined (STM32F10X_XL)
        case 12: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER12;
#  if defined (STM32F10X_XL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM8_BRK_TIM12_IRQn;
#  else
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM12_IRQn;
#  endif
            break;
        case 13: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER13;
#  if defined (STM32F10X_XL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM8_UP_TIM13_IRQn;
#  else
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM13_IRQn;
#  endif
            break;
        case 14: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER14;
#  if defined (STM32F10X_XL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = TIM8_TRG_COM_TIM14_IRQn;
#  else
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM14_IRQn;
#  endif
            break;
#endif
#if defined (STM32F10X_HD_VL) || defined (STM32F10X_HD_VL) || defined (STM32F10X_LD_VL)
        case 15: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER15;
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM1_BRK_TIM15_IRQn;
            break;
        case 16: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER16;
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM1_UP_TIM16_IRQn;
            break;
        case 17: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER17;
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM1_TRG_COM_TIM17_IRQn;
            break;
#endif
        default: { ttc_assert_halt_origin( ttc_assert_origin_auto ); }
    }

    /* Enable and set TIMx Interrupt to the lowest priority */
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelPreemptionPriority = 0x0F;
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelSubPriority = 0x0F;
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( &interrupt_stm32f1xx_NVIC_Cfg );
    return ec_interrupt_OK;

}
void _interrupt_stm32f1xx_enable_timer( t_ttc_interrupt_configimer_t* Config_ISRs ) {
    t_register_stm32f1xx_timer* TIMER_Base = NULL;

    switch ( Config_ISRs->PhysicalIndex ) {
        case 1: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER1;
            break;
        case 2: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER2;
            break;
        case 3: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER3;
            break;
        case 4: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER4;
            break;
        case 5: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER5;
            break;
#if defined (STM32F10X_XL)
        case 9: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER9;
            break;
        case 10: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER10;
            break;
        case 11: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER11;
            break;
        case 12: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER12;
            break;
        case 13: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER13;
            break;
        case 14: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER14;
            break;
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto );
            break;
    }

#ifndef STM32F10X_XL
    switch ( Config_ISRs->Common.Type ) { // select trigger according to Type
        case tit_TIMER_Updating:
            TIMER_Base->DIER.All |= ( ( ut_int16 )0x0001 );
            break;
        case tit_TIMER_CC1:
            TIMER_Base->DIER.All |= ( ( ut_int16 )0x0002 );
            break;
        case tit_TIMER_CC2:
            TIMER_Base->DIER.All |= ( ( ut_int16 )0x0004 );
            break;
        case tit_TIMER_Triggering:
            TIMER_Base->DIER.All |= ( ( ut_int16 )0x0040 );
            break;
        case tit_TIMER_Break:
            if ( TIMER_Base == &register_stm32f1xx_TIMER1 ) //|| (TIMER_Base == TIM8))
            { TIMER_Base->DIER.All |= ( ( ut_int16 )0x0080 ); }
            break;
        default:
            ttc_assert_halt_origin( ttc_assert_origin_auto );
            break;
    }
#endif
# ifdef STM32F10X_XL
    if ( ( TIMER_Base == &register_stm32f1xx_TIMER10 ) || ( TIMER_Base == &register_stm32f1xx_TIMER11 ) || ( TIMER_Base == &register_stm32f1xx_TIMER13 ) || ( TIMER_Base == &register_stm32f1xx_TIMER14 ) ) {
        switch ( Type ) {
            case tit_TIMER_Updating:
                TIMER_Base->DIER.All |= ( ( ut_int16 )0x0001 );
                break;
            case tit_TIMER_CC1:
                TIMER_Base->DIER.All |= ( ( ut_int16 )0x0002 );
                break;
            default:
                ttc_assert_halt_origin( ttc_assert_origin_auto );
                break;
        }
    }
    else {
        switch ( Type ) { // select trigger according to Type
            case tit_TIMER_Updating:
                TIMER_Base->DIER.All |= ( ( ut_int16 )0x0001 );
                break;
            case tit_TIMER_CC1:
                TIMER_Base->DIER.All |= ( ( ut_int16 )0x0002 );
                break;
            case tit_TIMER_CC2:
                TIMER_Base->DIER.All |= ( ( ut_int16 )0x0004 );
                break;
            case tit_TIMER_Triggering:
                TIMER_Base->DIER.All |= ( ( ut_int16 )0x0040 );
                break;
            case tit_TIMER_Break:
                if ( TIMER_Base == &register_stm32f1xx_TIMER1 ) //|| (TIMER_Base == TIM8))
                { TIMER_Base->DIER.All |= ( ( ut_int16 )0x0080 ); }
                break;
            default:
                ttc_assert_halt_origin( ttc_assert_origin_auto );
                break;
        }
    }
#endif
}
void interrupt_stm32f1xx_enable_timer( t_ttc_interrupt_configimer_t* Config_ISRs ) {
    _interrupt_stm32f1xx_enable_timer( Config_ISRs ); // initializes interrupt_stm32f1xx_NVIC_Cfg
    NVIC_Init( &interrupt_stm32f1xx_NVIC_Cfg );
}
void interrupt_stm32f1xx_disable_timer( t_ttc_interrupt_configimer_t* Config_ISRs ) {

    _interrupt_stm32f1xx_enable_timer( Config_ISRs );

    // disable interrupt channel only if no other interrupt event for this TIMER is active
    t_register_stm32f1xx_timer* TIMER_Base = NULL;
    u_register_stm32f1xx_timx_dier EnableRegister;

    switch ( Config_ISRs->PhysicalIndex ) {
        case 1: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER1;
            break;
        case 2: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER2;
            break;
        case 3: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER3;
            break;
        case 4: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER4;
            break;
        case 5: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER5;
            break;
#if defined (STM32F10X_XL)
        case 9: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER9;
            break;
        case 10: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER10;
            break;
        case 11: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER11;
            break;
        case 12: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER12;
            break;
        case 13: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER13;
            break;
        case 14: TIMER_Base = ( t_register_stm32f1xx_timer* )&register_stm32f1xx_TIMER14;
            break;
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto );
            break;
    }

    EnableRegister.All = ( t_u16 ) TIMER_Base->DIER.All;

    if ( ( EnableRegister.Bits.UIE == 0 )   &&
            ( EnableRegister.Bits.CC1IE == 0 ) &&
            ( EnableRegister.Bits.CC2IE == 0 ) &&
            ( EnableRegister.Bits.CC3IE == 0 ) &&
            ( EnableRegister.Bits.CC4IE == 0 ) &&
            ( EnableRegister.Bits.COMIE == 0 ) &&
            ( EnableRegister.Bits.TIE == 0 )   &&
            ( EnableRegister.Bits.BIE == 0 )
       )
    { interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelCmd = DISABLE; }
    else
    { interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelCmd = ENABLE; }
    NVIC_Init( &interrupt_stm32f1xx_NVIC_Cfg );
}

#endif //}
#ifdef EXTENSION_ttc_usart //{

e_ttc_interrupt_errorcode interrupt_stm32f1xx_init_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type ) {
    TODO( "Implement initialization of different usart interrupt types!" )
    ( void ) Config_ISRs;
    // all done in ttc_interrupt.c

    return ( e_ttc_interrupt_errorcode ) 0;
}
void interrupt_stm32f1xx_enable_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type ) {
    TODO( "implement like interrupt_stm32l1xx_enable_usart()" )

    /* Enable and set USARTn Interrupt to the lowest priority */
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelPreemptionPriority = 0x0F;
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelSubPriority = 0x0F;
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelCmd = ENABLE;

    USART_TypeDef* USART_Base = NULL;

    switch ( Config_ISRs->PhysicalIndex ) { // physical index starts at 0

        case 0: USART_Base = USART1; interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = USART1_IRQn; break;
        case 1: USART_Base = USART2; interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = USART2_IRQn; break;
        case 2: USART_Base = USART3; interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = USART3_IRQn; break;
#if defined(STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined(STM32F10X_XL) ||defined(STM32F10X_CL)
        case 3: USART_Base = UART4;  interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  UART4_IRQn; break;
#endif
#if defined(STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined(STM32F10X_XL) ||defined(STM32F10X_CL)
        case 4: USART_Base = UART5;  interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  UART5_IRQn; break;
#endif
        default: { ttc_assert_halt_origin( ttc_assert_origin_auto ); }
    }

#if 1==0
    // produces lots of warning in GCC 4.7 (ToDo: fix warnings)
    t_register_stm32f1xx_usart_cr1 CR1; *( ( t_u16* ) &CR1 ) = ( t_u16* ) & ( USART_Base->CR1 );
    t_register_stm32f1xx_usart_cr2 CR2; *( ( t_u16* ) &CR2 ) = ( t_u16* ) & ( USART_Base->CR2 );
    t_register_stm32f1xx_usart_cr3 CR3; *( ( t_u16* ) &CR3 ) = ( t_u16* ) & ( USART_Base->CR3 );
#else
    t_register_stm32f1xx_usart_cr1 CR1; CR1.All = USART_Base->CR1;
    t_register_stm32f1xx_usart_cr2 CR2; CR2.All = USART_Base->CR2;
    t_register_stm32f1xx_usart_cr3 CR3; CR3.All = USART_Base->CR3;
#endif

    switch ( Type ) { // set/ clear corresponding bits

        case tit_USART_Cts:
            Assert_INTERRUPT( A( ttc_interrupt_config_usart, Config_ISRs->PhysicalIndex )->isr_ClearToSend != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            CR3.Bits.CTSIE  = 1;
            break;
        case tit_USART_Idle:
            CR1.Bits.IDLEIE = 1;

            Assert_INTERRUPT( A( ttc_interrupt_config_usart, Config_ISRs->PhysicalIndex )->isr_IdleLine != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_USART_TxComplete:
            CR1.Bits.TCIE   = 1;
            Assert_INTERRUPT( A( ttc_interrupt_config_usart, Config_ISRs->PhysicalIndex )->isr_TransmissionComplete != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_USART_TransmitDataEmpty:
            CR1.Bits.TXEIE  = 1;
            Assert_INTERRUPT( A( ttc_interrupt_config_usart, Config_ISRs->PhysicalIndex )->isr_TransmitDataEmpty != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_USART_RxNE:
            CR1.Bits.RXNEIE = 1;
            Assert_INTERRUPT( A( ttc_interrupt_config_usart, Config_ISRs->PhysicalIndex )->isr_ReceiveDataNotEmpty != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_USART_LinBreak:
            CR2.Bits.LBDIE  = 1;
            Assert_INTERRUPT( A( ttc_interrupt_config_usart, Config_ISRs->PhysicalIndex )->isr_LinBreakDetected != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_USART_Error:
            Assert_INTERRUPT( A( ttc_interrupt_config_usart, Config_ISRs->PhysicalIndex )->isr_Error != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            CR1.Bits.PEIE   = 1;  // irq on parity error
            CR3.Bits.EIE    = 1;  // irq on general error
            break;
        default: break;
    }

    if ( ! 1 ) { // disable interrupt channel only if no other interrupt event for this USART is active
        if ( ( CR1.Bits.PEIE   == 0 ) &&
                ( CR1.Bits.IDLEIE == 0 ) &&
                ( CR1.Bits.TCIE   == 0 ) &&
                ( CR1.Bits.TXEIE  == 0 ) &&
                ( CR1.Bits.RXNEIE == 0 ) &&
                ( CR2.Bits.LBDIE  == 0 ) &&
                ( CR3.Bits.CTSIE  == 0 ) &&
                ( CR3.Bits.EIE    == 0 )
           )
        { interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelCmd = DISABLE; }
    }
    NVIC_Init( &interrupt_stm32f1xx_NVIC_Cfg );

    // Note: We have to copy struct-data in this odd way because USART_Base->CR1.TXEIE = 1; gives unpredictable results!
    ( ( USART_TypeDef* ) USART_Base )->CR1 = *( ( t_u16* ) &CR1 );
    ( ( USART_TypeDef* ) USART_Base )->CR2 = *( ( t_u16* ) &CR2 );
    ( ( USART_TypeDef* ) USART_Base )->CR3 = *( ( t_u16* ) &CR3 );
}
void interrupt_stm32f1xx_disable_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type ) {
    TODO( "implement like interrupt_stm32l1xx_disable_usart()" )

    /* Enable and set USARTn Interrupt to the lowest priority */
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelPreemptionPriority = 0x0F;
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelSubPriority = 0x0F;
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelCmd = ENABLE;

    USART_TypeDef* USART_Base = NULL;

    switch ( Config_ISRs->PhysicalIndex ) { // physical index starts at 0

        case 0: USART_Base = USART1; interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = USART1_IRQn; break;
        case 1: USART_Base = USART2; interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = USART2_IRQn; break;
        case 2: USART_Base = USART3; interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel = USART3_IRQn; break;
#if defined(STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined(STM32F10X_XL) ||defined(STM32F10X_CL)
        case 3: USART_Base = UART4;  interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  UART4_IRQn; break;
#endif
#if defined(STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined(STM32F10X_XL) ||defined(STM32F10X_CL)
        case 4: USART_Base = UART5;  interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel =  UART5_IRQn; break;
#endif
        default: { ttc_assert_halt_origin( ttc_assert_origin_auto ); }
    }

#if 1==0
    // produces lots of warning in GCC 4.7 (ToDo: fix warnings)
    t_register_stm32f1xx_usart_cr1 CR1; *( ( t_u16* ) &CR1 ) = ( t_u16* ) & ( USART_Base->CR1 );
    t_register_stm32f1xx_usart_cr2 CR2; *( ( t_u16* ) &CR2 ) = ( t_u16* ) & ( USART_Base->CR2 );
    t_register_stm32f1xx_usart_cr3 CR3; *( ( t_u16* ) &CR3 ) = ( t_u16* ) & ( USART_Base->CR3 );
#else
    t_register_stm32f1xx_usart_cr1 CR1; CR1.All = USART_Base->CR1;
    t_register_stm32f1xx_usart_cr2 CR2; CR2.All = USART_Base->CR2;
    t_register_stm32f1xx_usart_cr3 CR3; CR3.All = USART_Base->CR3;
#endif

    switch ( Type ) { // set/ clear corresponding bits

        case tit_USART_Cts:
            if ( 0 )
            { Assert_INTERRUPT( A( ttc_interrupt_config_usart, Config_ISRs->PhysicalIndex )->isr_ClearToSend != NULL, ttc_assert_origin_auto ); }  // no isr configured for this interrupt!
            CR3.Bits.CTSIE  = 0;
            break;
        case tit_USART_Idle:
            CR1.Bits.IDLEIE = 0;
            if ( 0 )
            { Assert_INTERRUPT( A( ttc_interrupt_config_usart, Config_ISRs->PhysicalIndex )->isr_IdleLine != NULL, ttc_assert_origin_auto ); }  // no isr configured for this interrupt!
            break;
        case tit_USART_TxComplete:
            CR1.Bits.TCIE   = 0;
            if ( 0 )
            { Assert_INTERRUPT( A( ttc_interrupt_config_usart, Config_ISRs->PhysicalIndex )->isr_TransmissionComplete != NULL, ttc_assert_origin_auto ); }  // no isr configured for this interrupt!
            break;
        case tit_USART_TransmitDataEmpty:
            CR1.Bits.TXEIE  = 0;
            if ( 0 )
            { Assert_INTERRUPT( A( ttc_interrupt_config_usart, Config_ISRs->PhysicalIndex )->isr_TransmitDataEmpty != NULL, ttc_assert_origin_auto ); }  // no isr configured for this interrupt!
            break;
        case tit_USART_RxNE:
            CR1.Bits.RXNEIE = 0;
            if ( 0 )
            { Assert_INTERRUPT( A( ttc_interrupt_config_usart, Config_ISRs->PhysicalIndex )->isr_ReceiveDataNotEmpty != NULL, ttc_assert_origin_auto ); }  // no isr configured for this interrupt!
            break;
        case tit_USART_LinBreak:
            CR2.Bits.LBDIE  = 0;
            if ( 0 )
            { Assert_INTERRUPT( A( ttc_interrupt_config_usart, Config_ISRs->PhysicalIndex )->isr_LinBreakDetected != NULL, ttc_assert_origin_auto ); }  // no isr configured for this interrupt!
            break;
        case tit_USART_Error:
            if ( 0 )
            { Assert_INTERRUPT( A( ttc_interrupt_config_usart, Config_ISRs->PhysicalIndex )->isr_Error != NULL, ttc_assert_origin_auto ); }  // no isr configured for this interrupt!
            CR1.Bits.PEIE   = 0;  // irq on parity error
            CR3.Bits.EIE    = 0;  // irq on general error
            break;
        default: break;
    }

    // disable interrupt channel only if no other interrupt event for this USART is active
    if ( ( CR1.Bits.PEIE   == 0 ) &&
            ( CR1.Bits.IDLEIE == 0 ) &&
            ( CR1.Bits.TCIE   == 0 ) &&
            ( CR1.Bits.TXEIE  == 0 ) &&
            ( CR1.Bits.RXNEIE == 0 ) &&
            ( CR2.Bits.LBDIE  == 0 ) &&
            ( CR3.Bits.CTSIE  == 0 ) &&
            ( CR3.Bits.EIE    == 0 )
       )
    { interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelCmd = DISABLE; }
    else
    { interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelCmd = ENABLE; }

    NVIC_Init( &interrupt_stm32f1xx_NVIC_Cfg );

    // Note: We have to copy struct-data in this odd way because USART_Base->CR1.TXEIE = 1; gives unpredictable results!
    ( ( USART_TypeDef* ) USART_Base )->CR1 = *( ( t_u16* ) &CR1 );
    ( ( USART_TypeDef* ) USART_Base )->CR2 = *( ( t_u16* ) &CR2 );
    ( ( USART_TypeDef* ) USART_Base )->CR3 = *( ( t_u16* ) &CR3 );
}
#endif //}
#ifdef EXTENSION_ttc_can //{
TODO( "Implement CAN interrupt handling for stm32f1xx!" )

e_ttc_interrupt_errorcode interrupt_stm32f1xx_init_can( t_ttc_interrupt_config_can* Config_ISRs ) {
    t_register_stm32f1xx_can* CAN_Base = NULL;
    ( void )CAN_Base;

    /* Enable and set CANn Interrupt to the lowest priority */
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelPreemptionPriority = 0x00;
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelSubPriority = 0x00;
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelCmd = ENABLE;

    switch ( Config_ISRs->PhysicalIndex ) { // physical index starts at 0
        case 0: CAN_Base = ( t_register_stm32f1xx_can* )&register_stm32f1xx_CAN1;
            break;
        case 1: CAN_Base = ( t_register_stm32f1xx_can* )&register_stm32f1xx_CAN2;        break;
        default: { ttc_assert_halt_origin( ttc_assert_origin_auto ); }
    }

    switch ( Type ) { // set/clear corresponding bits
        case tit_CAN_Transmit_Empty:
#if defined(STM32F10X_LD) || defined(STM32F10X_MD) || defined(STM32F10X_HD) || defined(STM32F10X_XL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= USB_HP_CAN1_TX_IRQn;
#elif  defined(STM32F10X_CL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= CAN1_TX_IRQn;
#endif
            break;
        case tit_CAN_FIFO0_Pending:
#if defined(STM32F10X_LD) || defined(STM32F10X_MD) || defined(STM32F10X_HD) || defined(STM32F10X_XL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= USB_LP_CAN1_RX0_IRQn;
#elif  defined(STM32F10X_CL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= CAN1_RX0_IRQn;
#endif
            break;
        case tit_CAN_FIFO0_Full:
#if defined(STM32F10X_LD) || defined(STM32F10X_MD) || defined(STM32F10X_HD) || defined(STM32F10X_XL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= USB_LP_CAN1_RX0_IRQn;
#elif  defined(STM32F10X_CL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= CAN1_RX0_IRQn;
#endif
            break;
        case tit_CAN_FIFO0_Overrun:
#if defined(STM32F10X_LD) || defined(STM32F10X_MD) || defined(STM32F10X_HD) || defined(STM32F10X_XL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= USB_LP_CAN1_RX0_IRQn;
#elif  defined(STM32F10X_CL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= CAN1_RX0_IRQn;
#endif
            break;
        case tit_CAN_FIFO1_Pending:
#if defined(STM32F10X_LD) || defined(STM32F10X_MD) || defined(STM32F10X_HD) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= CAN1_RX1_IRQn;
#endif
            break;
        case tit_CAN_FIFO1_Full:
#if defined(STM32F10X_LD) || defined(STM32F10X_MD) || defined(STM32F10X_HD) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= CAN1_RX1_IRQn;
#endif
            break;
        case tit_CAN_FIFO1_Overrun:
#if defined(STM32F10X_LD) || defined(STM32F10X_MD) || defined(STM32F10X_HD) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= CAN1_RX1_IRQn;
#endif
            break;
        case tit_CAN_Error_Warning:
#if defined(STM32F10X_LD) || defined(STM32F10X_MD) || defined(STM32F10X_HD) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= CAN1_SCE_IRQn;
#endif
            break;
        case tit_CAN_Error_Passive:
#if defined(STM32F10X_LD) || defined(STM32F10X_MD) || defined(STM32F10X_HD) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= CAN1_SCE_IRQn;
#endif
            break;
        case tit_CAN_Bus_Off:
#if defined(STM32F10X_LD) || defined(STM32F10X_MD) || defined(STM32F10X_HD) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= CAN1_SCE_IRQn;
#endif
            break;
        case tit_CAN_WakeUp_State:
#if defined(STM32F10X_LD) || defined(STM32F10X_MD) || defined(STM32F10X_HD) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= CAN1_SCE_IRQn;
#endif
            break;
        case tit_CAN_Sleep_State:
#if defined(STM32F10X_LD) || defined(STM32F10X_MD) || defined(STM32F10X_HD) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
            interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannel |= CAN1_SCE_IRQn;
#endif
            break;
        default: break;
    }

    NVIC_Init( &interrupt_stm32f1xx_NVIC_Cfg );

    return ( e_ttc_interrupt_errorcode ) 0;
}
void interrupt_stm32f1xx_enable_can( t_ttc_interrupt_config_can* Config_ISRs ) {
    t_register_stm32f1xx_can* CAN_Base = NULL;

    switch ( Config_ISRs->PhysicalIndex ) { // physical index starts at 0
        case 0: CAN_Base = ( t_register_stm32f1xx_can* )&register_stm32f1xx_CAN1;
            break;
        case 1: CAN_Base = ( t_register_stm32f1xx_can* )&register_stm32f1xx_CAN2;        break;
        default: { ttc_assert_halt_origin( ttc_assert_origin_auto ); }
    }

    switch ( Type ) { // set/clear corresponding bits
        case tit_CAN_Transmit_Empty:
            Assert_INTERRUPT( A( ttc_interrupt_config_can, Config_ISRs->PhysicalIndex )->isr_Transmit != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            CAN_Base->IER.All |= ( ( t_u32 )0x00000001 );
            break;
        case tit_CAN_FIFO0_Pending:
            Assert_INTERRUPT( A( ttc_interrupt_config_can, Config_ISRs->PhysicalIndex )->isr_FIFO0_Pending != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            CAN_Base->IER.All |= ( ( t_u32 )0x00000002 );
            break;
        case tit_CAN_FIFO0_Full:
            Assert_INTERRUPT( A( ttc_interrupt_config_can, Config_ISRs->PhysicalIndex )->isr_FIFO0_Full != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            CAN_Base->IER.All |= ( ( t_u32 )0x00000004 );
            break;
        case tit_CAN_FIFO0_Overrun:
            Assert_INTERRUPT( A( ttc_interrupt_config_can, Config_ISRs->PhysicalIndex )->isr_FIFO0_Overrun != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            CAN_Base->IER.All |= ( ( t_u32 )0x00000008 );
            break;
        case tit_CAN_FIFO1_Pending:
            Assert_INTERRUPT( A( ttc_interrupt_config_can, Config_ISRs->PhysicalIndex )->isr_FIFO1_Pending != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            CAN_Base->IER.All |= ( ( t_u32 )0x00000010 );
            break;
        case tit_CAN_FIFO1_Full:
            Assert_INTERRUPT( A( ttc_interrupt_config_can, Config_ISRs->PhysicalIndex )->isr_FIFO1_Full != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            CAN_Base->IER.All |= ( ( t_u32 )0x00000020 );
            break;
        case tit_CAN_FIFO1_Overrun:
            Assert_INTERRUPT( A( ttc_interrupt_config_can, Config_ISRs->PhysicalIndex )->isr_FIFO1_Overrun != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            CAN_Base->IER.All |= ( ( t_u32 )0x00000040 );
            break;
        case tit_CAN_Error_Warning:
            Assert_INTERRUPT( A( ttc_interrupt_config_can, Config_ISRs->PhysicalIndex )->isr_Error_Warning != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            CAN_Base->IER.All |= ( ( t_u32 )0x00000100 );
            break;
        case tit_CAN_Error_Passive:
            Assert_INTERRUPT( A( ttc_interrupt_config_can, Config_ISRs->PhysicalIndex )->isr_Error_Passive != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            CAN_Base->IER.All |= ( ( t_u32 )0x00000200 );
            break;
        case tit_CAN_Bus_Off:
            Assert_INTERRUPT( A( ttc_interrupt_config_can, Config_ISRs->PhysicalIndex )->isr_Bus_Off != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            CAN_Base->IER.All |= ( ( t_u32 )0x00000400 );
            break;
        case tit_CAN_WakeUp_State:
            Assert_INTERRUPT( A( ttc_interrupt_config_can, Config_ISRs->PhysicalIndex )->isr_WakeUp_State != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            CAN_Base->IER.All |= ( ( t_u32 )0x00010000 );
            break;
        case tit_CAN_Sleep_State:
            Assert_INTERRUPT( A( ttc_interrupt_config_can, Config_ISRs->PhysicalIndex )->isr_Sleep_State != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            CAN_Base->IER.All |= ( ( t_u32 )0x00020000 );
            break;
        default: break;
    }

    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( &interrupt_stm32f1xx_NVIC_Cfg );
}
void interrupt_stm32f1xx_disable_can( t_ttc_interrupt_config_can* Config_ISRs ) {
    t_register_stm32f1xx_can* CAN_Base = NULL;

    switch ( Config_ISRs->PhysicalIndex ) { // physical index starts at 0
        case 0: CAN_Base = ( t_register_stm32f1xx_can* )&register_stm32f1xx_CAN1;
            break;
        case 1: CAN_Base = ( t_register_stm32f1xx_can* )&register_stm32f1xx_CAN2;        break;
        default: { ttc_assert_halt_origin( ttc_assert_origin_auto ); }
    }

    switch ( Type ) { // set/clear corresponding bits
        case tit_CAN_Transmit_Empty:
            CAN_Base->IER.All |= ( ( t_u32 )0x00000001 );
            break;
        case tit_CAN_FIFO0_Pending:
            CAN_Base->IER.All |= ( ( t_u32 )0x00000002 );
            break;
        case tit_CAN_FIFO0_Full:
            CAN_Base->IER.All |= ( ( t_u32 )0x00000004 );
            break;
        case tit_CAN_FIFO0_Overrun:
            CAN_Base->IER.All |= ( ( t_u32 )0x00000008 );
            break;
        case tit_CAN_FIFO1_Pending:
            CAN_Base->IER.All |= ( ( t_u32 )0x00000010 );
            break;
        case tit_CAN_FIFO1_Full:
            CAN_Base->IER.All |= ( ( t_u32 )0x00000020 );
            break;
        case tit_CAN_FIFO1_Overrun:
            CAN_Base->IER.All |= ( ( t_u32 )0x00000040 );
            break;
        case tit_CAN_Error_Warning:
            CAN_Base->IER.All |= ( ( t_u32 )0x00000100 );
            break;
        case tit_CAN_Error_Passive:
            CAN_Base->IER.All |= ( ( t_u32 )0x00000200 );
            break;
        case tit_CAN_Bus_Off:
            CAN_Base->IER.All |= ( ( t_u32 )0x00000400 );
            break;
        case tit_CAN_WakeUp_State:
            CAN_Base->IER.All |= ( ( t_u32 )0x00010000 );
            break;
        case tit_CAN_Sleep_State:
            CAN_Base->IER.All |= ( ( t_u32 )0x00020000 );
            break;
        default: break;
    }

    // disable interrupt channel only if no other interrupt event for this CAN is active
    interrupt_stm32f1xx_NVIC_Cfg.NVIC_IRQChannelCmd = DISABLE;
    NVIC_Init( &interrupt_stm32f1xx_NVIC_Cfg );
}
#endif //}

void interrupt_stm32f1xx_deinit_all_usart() {




}
#ifdef EXTENSION_ttc_rtc //{
TODO( "Implement RTC interrupt handling for stm32f1xx!" )

void interrupt_stm32f1xx_deinit_all_rtc() {


#warning Function interrupt_stm32f1xx_deinit_all_rtc() is empty.


}

void interrupt_stm32f1xx_deinit_rtc( t_ttc_interrupt_config_rtc* Config_ISRs ) {
    Assert_INTERRUPT_EXTRA_Writable( Config_ISRs, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function interrupt_stm32f1xx_deinit_rtc() is empty.


}
void interrupt_stm32f1xx_disable_rtc( t_ttc_interrupt_config_rtc* Config_ISRs ) {
    Assert_INTERRUPT_EXTRA_Writable( Config_ISRs, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function interrupt_stm32f1xx_disable_rtc() is empty.


}
void interrupt_stm32f1xx_enable_rtc( t_ttc_interrupt_config_rtc* Config_ISRs ) {
    Assert_INTERRUPT_EXTRA_Writable( Config_ISRs, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function interrupt_stm32f1xx_enable_rtc() is empty.


}
e_ttc_interrupt_errorcode interrupt_stm32f1xx_init_rtc( t_ttc_interrupt_config_rtc* Config_ISRs ) {
    Assert_INTERRUPT_EXTRA_Writable( Config_ISRs, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

#warning Function interrupt_stm32f1xx_init_rtc() is empty.

    return ( e_ttc_interrupt_errorcode ) 0;
}
#endif //}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//{ irq-handlers (supported) ***************************************************

#ifdef EXTENSION_ttc_usart
void USART_General_IRQHandler( t_physical_index PhysicalIndex, volatile t_register_stm32f1xx_usart* USART ) {

    t_register_stm32f1xx_usart_sr StatusRegister = USART->SR;  // reading from SR and then from DR will reset flags in SR

    t_ttc_interrupt_config_usart* ISRs = A( ttc_interrupt_config_usart, PhysicalIndex );
    Assert_INTERRUPT( ISRs != NULL, ttc_assert_origin_auto );  // should never happen: the usart indexed by PhysicalIndex has not been initialized!

    if ( StatusRegister.Bits.RXNE && ISRs->isr_ReceiveDataNotEmpty ) {
        ISRs->isr_ReceiveDataNotEmpty( PhysicalIndex, ISRs->Argument_ReceiveDataNotEmpty );
    }
    if ( StatusRegister.Bits.TXE  && ISRs->isr_TransmitDataEmpty ) {
        ISRs->isr_TransmitDataEmpty( PhysicalIndex, ISRs->Argument_TransmitDataEmpty );
    }
    if ( StatusRegister.Bits.CTS  && ISRs->isr_ClearToSend ) {
        ISRs->isr_ClearToSend( PhysicalIndex, ISRs->Argument_ClearToSend );
    }
    if ( StatusRegister.Bits.TC   && ISRs->isr_TransmissionComplete ) {
        ISRs->isr_TransmissionComplete( PhysicalIndex, ISRs->Argument_TransmissionComplete );
    }
    if ( StatusRegister.Bits.IDLE && ISRs->isr_IdleLine ) {
        ISRs->isr_IdleLine( PhysicalIndex, ISRs->Argument_IdleLine );
    }
    if ( StatusRegister.Bits.LBD  && ISRs->isr_LinBreakDetected ) {
        ISRs->isr_LinBreakDetected( PhysicalIndex, ISRs->Argument_LinBreakDetected );
    }

    if ( ( ( *( ( t_u8* ) &StatusRegister ) ) & 0xf ) && // error occured: gather data + call error handler
            ( ISRs->isr_Error != NULL )                 // error-handler has been defined
       ) {
        t_ttc_interrupt_usart_errors Error; *( ( t_u8* ) &Error ) = 0;
        if ( StatusRegister.Bits.FE )   { Error.Framing = 1; }
        if ( StatusRegister.Bits.NE )   { Error.Noise   = 1; }
        if ( StatusRegister.Bits.ORE )  { Error.Overrun = 1; }
        if ( StatusRegister.Bits.PE )   { Error.Parity  = 1; }

        ISRs->isr_Error( PhysicalIndex, Error, ISRs->Argument_Error );
    }
}
void USART1_IRQHandler() {
    USART_General_IRQHandler( 0, ( t_register_stm32f1xx_usart* ) USART1 );
    USART1->SR = 0; // clear all interrupt flags
}
void USART2_IRQHandler() {
    USART_General_IRQHandler( 1, ( t_register_stm32f1xx_usart* ) USART2 );
    USART2->SR = 0; // clear all interrupt flags
}
void USART3_IRQHandler() {
    USART_General_IRQHandler( 2, ( t_register_stm32f1xx_usart* ) USART3 );
    USART3->SR = 0; // clear all interrupt flags
}
void UART4_IRQHandler() {
    USART_General_IRQHandler( 3, ( t_register_stm32f1xx_usart* ) UART4 );
    UART4->SR = 0; // clear all interrupt flags
}
void UART5_IRQHandler() {
    USART_General_IRQHandler( 4, ( t_register_stm32f1xx_usart* ) UART5 );
    UART5->SR = 0; // clear all interrupt flags
}
void interrupt_stm32f1xx_deinit_usart( t_ttc_interrupt_config_usart* Config_ISRs ) {
    ( void ) Config_ISRs;
    // all done in ttc_interrupt
}
#endif
#ifdef EXTENSION_ttc_timer
void TIMER_General_IRQHandler( t_physical_index PhysicalIndex, volatile t_register_stm32f1xx_timer* TIMER ) {
    Assert_INTERRUPT( TIMER != NULL, ttc_assert_origin_auto );
    u_register_stm32f1xx_timx_sr StatusRegister;
    StatusRegister.All = ( t_u16 )TIMER->SR.All; // reading from SR and then from DR will reset flags in SR
    t_ttc_interrupt_configimer_t* ISRs = A( t_ttc_interrupt_configimer, PhysicalIndex );
    Assert_INTERRUPT( ISRs != NULL, ttc_assert_origin_auto );  // should never happen: the usart indexed by PhysicalIndex has not been initialized!

    if ( StatusRegister.Bits.UIF )
    { ISRs->isr_Update( PhysicalIndex, ISRs->Argument_Update ); }
    /*
    if(StatusRegister.Bits.CC1IF)
       ISRs->isr_CC1IF(PhysicalIndex, ISRs->Argument_CC1IF);
    if(StatusRegister.Bits.CC2IF)
       ISRs->isr_CC2IF(PhysicalIndex, ISRs->Argument_CC2IF);
    if(StatusRegister.Bits.CC3IF)
       ISRs->isr_CC3IF(PhysicalIndex, ISRs->Argument_CC3IF);
    if(StatusRegister.Bits.CC4IF)
       ISRs->isr_CC4IF(PhysicalIndex, ISRs->Argument_CC4IF);
    if(StatusRegister.Bits.TIF)
       ISRs->isr_Trigger(PhysicalIndex, ISRs->Argument_Trigger);
    if(StatusRegister.Bits.BIF)
       ISRs->isr_Break(PhysicalIndex, ISRs->Argument_Break);
    if(StatusRegister.Bits.CC1OF)
       ISRs->isr_CC10F(PhysicalIndex, ISRs->Argument_CC10F);
    if(StatusRegister.Bits.CC2OF)
       ISRs->isr_CC20F(PhysicalIndex, ISRs->Argument_CC20F);
    if(StatusRegister.Bits.CC3OF)
       ISRs->isr_CC30F(PhysicalIndex, ISRs->Argument_CC30F);
    if(StatusRegister.Bits.CC4OF)
       ISRs->isr_CC40F(PhysicalIndex, ISRs->Argument_CC40F);
    */
}
void TIM1_UP_IRQHandler() {
    TIMER_General_IRQHandler( 1, ( t_register_stm32f1xx_timer* ) &register_stm32f1xx_TIMER1 );
    register_stm32f1xx_TIMER1.SR.All = 0; // clear all interrupt flags
}
void TIM2_IRQHandler() {
    TIMER_General_IRQHandler( 2, ( t_register_stm32f1xx_timer* ) &register_stm32f1xx_TIMER2 );
    register_stm32f1xx_TIMER2.SR.All = 0; // clear all interrupt flags
}
void TIM3_IRQHandler() {
    TIMER_General_IRQHandler( 3, ( t_register_stm32f1xx_timer* ) &register_stm32f1xx_TIMER3 );
    register_stm32f1xx_TIMER3.SR.All = 0; // clear all interrupt flags
}
void TIM4_IRQHandler() {
    TIMER_General_IRQHandler( 4, ( t_register_stm32f1xx_timer* ) &register_stm32f1xx_TIMER4 );
    register_stm32f1xx_TIMER4.SR.All = 0; // clear all interrupt flags
}
void TIM5_IRQHandler() {
    TIMER_General_IRQHandler( 5, ( t_register_stm32f1xx_timer* ) &register_stm32f1xx_TIMER5 );
    register_stm32f1xx_TIMER5.SR.All = 0; // clear all interrupt flags
}
# ifdef STM32F10X_XL
void TIM9_IRQHandler() {
    TIMER_General_IRQHandler( 9, ( t_register_stm32f1xx_timer* ) TIM9 );
}
void TIM10_IRQHandler() {
    TIMER_General_IRQHandler( 10, ( t_register_stm32f1xx_timer* ) TIM10 );
}
void TIM11_IRQHandler() {
    TIMER_General_IRQHandler( 11, ( t_register_stm32f1xx_timer* ) TIM11 );
}
void TIM12_IRQHandler() {
    TIMER_General_IRQHandler( 12, ( t_register_stm32f1xx_timer* ) TIM12 );
}
void TIM13_IRQHandler() {
    TIMER_General_IRQHandler( 13, ( t_register_stm32f1xx_timer* ) TIM13 );
}
void TIM14_IRQHandler() {
    TIMER_General_IRQHandler( 14, ( t_register_stm32f1xx_timer* ) TIM14 );
}
# endif
void interrupt_stm32f1xx_deinit_all_timer() {




}
void interrupt_stm32f1xx_deinit_timer( t_ttc_interrupt_configimer_t* Config_ISRs ) {

}
#endif
#ifdef EXTENSION_ttc_can
TODO( "Implement CAN interrupt handling for stm32f1xx!" )

void interrupt_stm32f1xx_deinit_all_can() {




}

void CAN_General_IRQHandler( t_physical_index PhysicalIndex, volatile t_register_stm32f1xx_can* ISR_CAN ) {
#warning Replace stm32l1xx dependent code by hardware independent structures and functions!
    register_stm32l1xx_can_msr_t MSR = ISR_CAN->MSR;
    register_stm32l1xx_can_tsr_t TSR = ISR_CAN->TSR;  // reading from SR and then from DR will reset flags in SR
    register_stm32l1xx_can_rf0r_t RF0R = ISR_CAN->RF0R;
    register_stm32l1xx_can_rf1r_t RF1R = ISR_CAN->RF1R;
    register_stm32l1xx_can_esr_t ESR = ISR_CAN->ESR;

    t_ttc_interrupt_config_can* ISRs = A( ttc_interrupt_config_can, PhysicalIndex );
    Assert_INTERRUPT( ISRs != NULL, ttc_assert_origin_auto );  // should never happen: the can indexed by PhysicalIndex has not been initialized!

    if ( TSR.Bits.RQCP0 && ISRs->isr_Transmit ) {
        TSR.Bits.RQCP0 = 0;
        ISRs->isr_Transmit( PhysicalIndex, ISRs->Argument_Transmit );
    }
    if ( TSR.Bits.RQCP1 && ISRs->isr_Transmit ) {
        TSR.Bits.RQCP1 = 0;
        ISRs->isr_Transmit( PhysicalIndex, ISRs->Argument_Transmit );
    }
    if ( TSR.Bits.RQCP2 && ISRs->isr_Transmit ) {
        TSR.Bits.RQCP2 = 0;
        ISRs->isr_Transmit( PhysicalIndex, ISRs->Argument_Transmit );
    }
    if ( RF0R.Bits.FMP0 && ISRs->isr_FIFO0_Pending ) {
        RF0R.Bits.FMP0 = 0;
        ISRs->isr_FIFO0_Pending( PhysicalIndex, ISRs->Argument_FIFO0_Pending );
    }
    if ( RF0R.Bits.FULL0 && ISRs->isr_FIFO0_Full ) {
        RF0R.Bits.FULL0 = 0;
        ISRs->isr_FIFO0_Full( PhysicalIndex, ISRs->Argument_FIFO0_Full );
    }
    if ( RF0R.Bits.FOVR0 && ISRs->isr_FIFO0_Overrun ) {
        RF0R.Bits.FOVR0 = 0;
        ISRs->isr_FIFO0_Overrun( PhysicalIndex, ISRs->Argument_FIFO0_Overrun );
    }
    if ( RF1R.Bits.FMP1 && ISRs->isr_FIFO1_Pending ) {
        RF1R.Bits.FMP1 = 0;
        ISRs->isr_FIFO1_Pending( PhysicalIndex, ISRs->Argument_FIFO1_Pending );
    }
    if ( RF1R.Bits.FULL1 && ISRs->isr_FIFO1_Full ) {
        RF1R.Bits.FULL1 = 0;
        ISRs->isr_FIFO1_Full( PhysicalIndex, ISRs->Argument_FIFO1_Full );
    }
    if ( RF1R.Bits.FOVR1 && ISRs->isr_FIFO1_Overrun ) {
        RF1R.Bits.FOVR1 = 0;
        ISRs->isr_FIFO1_Overrun( PhysicalIndex, ISRs->Argument_FIFO1_Overrun );
    }
    if ( ESR.Bits.BOFF && ISRs->isr_Bus_Off ) {
        ESR.Bits.BOFF = 0;
        ISRs->isr_Bus_Off( PhysicalIndex, ISRs->Argument_Bus_Off );
    }
    if ( ESR.Bits.EWGF && ISRs->isr_Error_Warning ) {
        ESR.Bits.EWGF = 0;
        ISRs->isr_Error_Warning( PhysicalIndex, ISRs->Argument_Error_Warning );
    }
    if ( ESR.Bits.EPVF && ISRs->isr_Error_Passive ) {
        ESR.Bits.EPVF = 0;
        ISRs->isr_Error_Passive( PhysicalIndex, ISRs->Argument_Error_Passive );
    }
    if ( MSR.Bits.WKUI && ISRs->isr_WakeUp_State ) {
        MSR.Bits.WKUI = 0;
        ISRs->isr_WakeUp_State( PhysicalIndex, ISRs->Argument_WakeUp_State );
    }
    if ( MSR.Bits.SLAKI && ISRs->isr_Sleep_State ) {
        MSR.Bits.SLAKI = 0;
        ISRs->isr_Sleep_State( PhysicalIndex, ISRs->Argument_Sleep_State );
    }
}

void USB_HP_CAN1_TX_IRQHandler() {
    CAN_General_IRQHandler( 0, ( t_register_stm32f1xx_can* ) &register_stm32f1xx_CAN1 );
}
void CAN1_TX_IRQHandler() {
    CAN_General_IRQHandler( 0, ( t_register_stm32f1xx_can* ) &register_stm32f1xx_CAN1 );
}
void USB_LP_CAN1_RX0_IRQHandler() {
    CAN_General_IRQHandler( 0, ( t_register_stm32f1xx_can* ) &register_stm32f1xx_CAN1 );
}
void CAN1_RX0_IRQHandler() {
    CAN_General_IRQHandler( 0, ( t_register_stm32f1xx_can* ) &register_stm32f1xx_CAN1 );
}
void CAN1_RX1_IRQHandler() {
    CAN_General_IRQHandler( 0, ( t_register_stm32f1xx_can* ) &register_stm32f1xx_CAN1 );
}
void CAN1_SCE_IRQHandler() {
    CAN_General_IRQHandler( 0, ( t_register_stm32f1xx_can* ) &register_stm32f1xx_CAN1 );
}
void CAN2_TX_IRQHandler() {
    CAN_General_IRQHandler( 1, ( t_register_stm32f1xx_can* ) &register_stm32f1xx_CAN2 );
}
void CAN2_RX0_IRQHandler() {
    CAN_General_IRQHandler( 1, ( t_register_stm32f1xx_can* ) &register_stm32f1xx_CAN2 );
}
void CAN2_RX1_IRQHandler() {
    CAN_General_IRQHandler( 1, ( t_register_stm32f1xx_can* ) &register_stm32f1xx_CAN2 );
}
void CAN2_SCE_IRQHandler() {
    CAN_General_IRQHandler( 1, ( t_register_stm32f1xx_can* ) &register_stm32f1xx_CAN2 );
}
void interrupt_stm32f1xx_deinit_can( t_ttc_interrupt_config_can* Config_ISRs ) {

}
#endif

//}irq-handlers (supported)
//{ irq-handlers (yet unsupported) *********************************************

/* void Reset_Handler() {
  // write your own reset-handler here...


  Assert_INTERRUPT(0, ttc_assert_origin_auto);
} */
void DebugMon_Handler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void WWDG_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void PVD_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void TAMPER_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void FLASH_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void RCC_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void DMA1_Channel1_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void DMA1_Channel2_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void DMA1_Channel3_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void DMA1_Channel4_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void DMA1_Channel5_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void DMA1_Channel6_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void DMA1_Channel7_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void ADC1_2_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void TIM1_BRK_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void TIM1_TRG_COM_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void TIM1_CC_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void I2C1_EV_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void I2C1_ER_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void I2C2_EV_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void I2C2_ER_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void SPI1_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void SPI2_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void RTCAlarm_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void OTG_FS_WKUP_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void SPI3_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void DMA2_Channel1_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void DMA2_Channel2_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void DMA2_Channel3_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void DMA2_Channel4_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void DMA2_Channel5_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void ETH_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void ETH_WKUP_IRQHandler() {
    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void SDIO_IRQHandler( void ) {
    /* Process All SDIO Interrupt Sources */
    //X  SD_ProcessIRQSrc();
}
#ifdef USE_USB_OTG_FS
    void OTG_FS_IRQHandler( void )
#else
    void OTG_HS_IRQHandler( void )
#endif
{
#ifdef EXTENSION_400_stm32_usb_fs_device_lib
    STM32_PCD_OTG_ISR_Handler();
#elif EXTENSION_400_stm32_usb_fs_host_lib
    USBH_OTG_ISR_Handler( &USB_OTG_Core_dev );
#else
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
#endif
}

//#ifdef STM32F10X_CL
//void OTG_FS_IRQHandler() {
//    // call your interrupt-handler right here, or else ...
//#ifdef EXTENSION_400_stm32_usb_fs_device_lib
//    STM32_PCD_OTG_ISR_Handler();
//#elif EXTENSION_400_stm32_usb_fs_host_lib
//    USBH_OTG_ISR_Handler(&USB_OTG_Core_dev);
//#else
//    Assert_INTERRUPT(0, ttc_assert_origin_auto);
//#endif
//}
//#endif
void BootRAM() {

    // call your interrupt-handler right here, or else ...


    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}

//} irq-handlers (yet unsupported)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
