#ifndef INTERRUPT_STM32W1XX_H
#define INTERRUPT_STM32W1XX_H

/** { interrupt_stm32w1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for interrupt devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by high-level interrupt and application.
 *
 *  Created from template device_architecture.h revision 21 at 20140224 21:37:20 UTC
 *
 *  Note: See ttc_interrupt.h for description of stm32w1xx independent INTERRUPT implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_INTERRUPT_STM32W1XX
//
// Implementation status of low-level driver support for interrupt devices on stm32w1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_INTERRUPT_STM32W1XX '?'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_INTERRUPT_STM32W1XX == 'o')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_INTERRUPT_STM32W1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_INTERRUPT_STM32W1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "interrupt_cortexm3.h"
#include "../basic/basic_cm3.h"
#include "../ttc_heap.h"
#include "interrupt_stm32w1xx_types.h"
#include "../ttc_interrupt_types.h"
#include "../ttc_register.h"
#include "../register/register_stm32w1xx.h" // already included by ttc_register.h, but stating it here helps IDE to find defines
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_interrupt_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_<device>_prepare
//
#define ttc_driver_interrupt_prepare()                  interrupt_stm32w1xx_prepare()
#define ttc_driver_interrupt_deinit(Config_ISRs)        interrupt_stm32w1xx_deinit(Config_ISRs)
#define ttc_driver_interrupt_reset(Config_ISRs)         interrupt_stm32w1xx_reset(Config_ISRs)
#define ttc_driver_interrupt_all_enable()               interrupt_cortexm3_all_enable()
#define ttc_driver_interrupt_all_disable()              interrupt_cortexm3_all_disable()
#define ttc_driver_interrupt_check_all_disabled()       interrupt_cortexm3_check_all_disabled()
#define ttc_driver_interrupt_deinit_all_rtc()           interrupt_stm32w1xx_deinit_all_rtc()
#define ttc_driver_interrupt_deinit_rtc(Config_ISRs)    interrupt_stm32w1xx_deinit_rtc(Config_ISRs)
#define ttc_driver_interrupt_disable_rtc(Config_ISRs)   interrupt_stm32w1xx_disable_rtc(Config_ISRs)
#define ttc_driver_interrupt_enable_rtc(Config_ISRs)    interrupt_stm32w1xx_enable_rtc(Config_ISRs)
#define ttc_driver_interrupt_generate_gpio(Handle)      interrupt_stm32w1xx_generate_gpio(Handle)
#define ttc_driver_interrupt_init_rtc(Config_ISRs)      interrupt_stm32w1xx_init_rtc(Config_ISRs)
//#define ttc_driver_interrupt_check_inside_isr()  (provided by interrupt_cortexm3.h)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_interrupt.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_interrupt.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

/** Prepares interrupt driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void interrupt_stm32w1xx_prepare();


#ifdef EXTENSION_ttc_i2c //{

    #define ttc_driver_interrupt_init_i2c(Config_ISRs)      interrupt_stm32w1xx_init_i2c(Config_ISRs)
    #define ttc_driver_interrupt_deinit_i2c(Config_ISRs)    interrupt_stm32w1xx_deinit_i2c(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_i2c()           interrupt_stm32w1xx_deinit_all_i2c()
    #define ttc_driver_interrupt_enable_i2c(Config_ISRs)    interrupt_stm32w1xx_enable_i2c(Config_ISRs)
    #define ttc_driver_interrupt_disable_i2c(Config_ISRs)   interrupt_stm32w1xx_disable_i2c(Config_ISRs)

    /** initializes single interrupt from i2c device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return               ==0: initialization successfull; error code otherwise
    */
    e_ttc_interrupt_errorcode interrupt_stm32w1xx_init_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

    /** initializes single interrupt from i2c device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    */
    void interrupt_stm32w1xx_deinit_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

    /** { resets interrupt settings of all I2C units to their reset value
    *
    }*/
    void interrupt_stm32w1xx_deinit_all_i2c();

    /** enables single interrupt from i2c device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    */
    void interrupt_stm32w1xx_enable_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

    /** { enables/disables single interrupt from i2c device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void interrupt_stm32w1xx_disable_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

#endif //}
#ifdef EXTENSION_ttc_spi //{

    #define ttc_driver_interrupt_init_spi(Config_ISRs)      interrupt_stm32w1xx_init_spi(Config_ISRs)
    #define ttc_driver_interrupt_deinit_spi(Config_ISRs)    interrupt_stm32w1xx_deinit_spi(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_spi()           interrupt_stm32w1xx_deinit_all_spi()
    #define ttc_driver_interrupt_enable_spi(Config_ISRs)    interrupt_stm32w1xx_enable_spi(Config_ISRs)
    #define ttc_driver_interrupt_disable_spi(Config_ISRs)   interrupt_stm32w1xx_disable_spi(Config_ISRs)

    /** initializes single interrupt from spi device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return               ==0: initialization successfull; error code otherwise
    */
    e_ttc_interrupt_errorcode interrupt_stm32w1xx_init_spi( t_ttc_interrupt_config_spi* Config_ISRs );

    /** deinitializes single interrupt from spi device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    */
    void interrupt_stm32w1xx_deinit_spi( t_ttc_interrupt_config_spi* Config_ISRs );

    /** { resets interrupt settings of all SPI units to their reset value
    *
    }*/
    void interrupt_stm32w1xx_deinit_all_spi();

    /** enables single interrupt from spi device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    */
    void interrupt_stm32w1xx_enable_spi( t_ttc_interrupt_config_spi* Config_ISRs );

    /** { enables/disables single interrupt from spi device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void interrupt_stm32w1xx_disable_spi( t_ttc_interrupt_config_spi* Config_ISRs );

#endif //}
#ifdef EXTENSION_ttc_usart //{

    #define ttc_driver_interrupt_init_usart(Config_ISRs)    interrupt_stm32w1xx_init_usart(Config_ISRs)
    #define ttc_driver_interrupt_deinit_usart(Config_ISRs)  interrupt_stm32w1xx_deinit_usart(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_usart()         interrupt_stm32w1xx_deinit_all_usart()
    #define ttc_driver_interrupt_enable_usart(Config_ISRs)  interrupt_stm32w1xx_enable_usart(Config_ISRs)
    #define ttc_driver_interrupt_disable_usart(Config_ISRs) interrupt_stm32w1xx_disable_usart(Config_ISRs)

    /** initializes single interrupt from usart device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return               ==0: initialization successfull; error code otherwise
    */
    e_ttc_interrupt_errorcode interrupt_stm32w1xx_init_usart( t_ttc_interrupt_config_usart* Config_ISRs );


    /** deinitializes single interrupt from usart device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    */
    void interrupt_stm32w1xx_deinit_usart( t_ttc_interrupt_config_usart* Config_ISRs );


    /** { resets interrupt settings of all USART units to their reset value
    *
    }*/
    void interrupt_stm32w1xx_deinit_all_usart();

    /** enables single interrupt from usart device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    */
    void interrupt_stm32w1xx_enable_usart( t_ttc_interrupt_config_usart* Config_ISRs );

    /** { enables/disables single interrupt from usart device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void interrupt_stm32w1xx_disable_usart( t_ttc_interrupt_config_usart* Config_ISRs );

#endif //}
#ifdef EXTENSION_ttc_gpio //{

    #define ttc_driver_interrupt_init_gpio(Config_ISRs)                      interrupt_stm32w1xx_init_gpio(Config_ISRs)
    #define ttc_driver_interrupt_deinit_gpio(Config_ISRs)                    interrupt_stm32w1xx_deinit_gpio(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_gpio()                           interrupt_stm32w1xx_deinit_all_gpio()
    #define ttc_driver_interrupt_enable_gpio(Config_ISRs)                    interrupt_stm32w1xx_enable_gpio(Config_ISRs)
    #define ttc_driver_interrupt_disable_gpio(Config_ISRs)                   interrupt_stm32w1xx_disable_gpio(Config_ISRs)
    #define ttc_driver_interrupt_gpio_create_index(GPIOx, Pin)               gpio_stm32w1xx_create_index(GPIOx, Pin)
    #define ttc_driver_interrupt_gpio_from_index(PhysicalIndex, GPIOx, Pin)  gpio_stm32w1xx_from_index(PhysicalIndex, GPIOx, Pin)
    #define ttc_driver_interrupt_gpio_calc_config_index(PhysicalIndex)       interrupt_stm32w1xx_gpio_calc_config_index(PhysicalIndex)

    void interrupt_stm32w1xx_deinit_all_gpio();

    /** initializes single interrupt from gpio device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return               ==0: interrupt was initialized successfully; !=0: error occured
    */
    e_ttc_interrupt_errorcode interrupt_stm32w1xx_init_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );

    /** deinitializes single interrupt from gpio device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    */
    void interrupt_stm32w1xx_deinit_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );

    /** enables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    */
    void interrupt_stm32w1xx_enable_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );

    /** { enables/ disables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void interrupt_stm32w1xx_disable_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );

    /** Calculates index of internal interrupt line that is used to serve external interrupts from given pin
    *
    * @param   PhysicalIndex  32-bit value representing GPIO-bank + pin
    * @return  Index of internal interrupt line serving given pin
    */
    t_u8 interrupt_stm32w1xx_gpio_calc_config_index( t_base PhysicalIndex );

#endif //}
#ifdef EXTENSION_ttc_can //{

    #define ttc_driver_interrupt_init_can(Config_ISRs)    interrupt_stm32w1xx_init_can(Config_ISRs)
    #define ttc_driver_interrupt_deinit_can(Config_ISRs)  interrupt_stm32w1xx_deinit_can(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_can()         interrupt_stm32w1xx_deinit_all_can()
    #define ttc_driver_interrupt_enable_can(Config_ISRs)  interrupt_stm32w1xx_enable_can(Config_ISRs)
    #define ttc_driver_interrupt_disable_can(Config_ISRs) interrupt_stm32w1xx_disable_can(Config_ISRs)

    /** initializes single interrupt from can device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return               ==0: initialization successfull; error code otherwise
    */
    e_ttc_interrupt_errorcode interrupt_stm32w1xx_init_can( t_ttc_interrupt_config_can* Config_ISRs );

    /** deinitializes single interrupt from can device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    */
    e_ttc_interrupt_errorcode interrupt_stm32w1xx_deinit_can( t_ttc_interrupt_config_can* Config_ISRs );

    /** { resets interrupt settings of all CAN units to their reset value
    *
    }*/
    void interrupt_stm32w1xx_deinit_all_can();

    /** enables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    */
    void interrupt_stm32w1xx_enable_can( t_ttc_interrupt_config_can* Config_ISRs );

    /** { enables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void interrupt_stm32w1xx_disable_can( t_ttc_interrupt_config_can* Config_ISRs );

#endif //}
#ifdef EXTENSION_ttc_timer //{

    #define ttc_driver_interrupt_init_timer(Config_ISRs)    interrupt_stm32w1xx_init_timer(Config_ISRs)
    #define ttc_driver_interrupt_deinit_timer(Config_ISRs)  interrupt_stm32w1xx_deinit_timer(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_timer()         interrupt_stm32w1xx_deinit_all_timer()
    #define ttc_driver_interrupt_enable_timer(Config_ISRs)  interrupt_stm32w1xx_enable_timer(Config_ISRs)
    #define ttc_driver_interrupt_disable_timer(Config_ISRs) interrupt_stm32w1xx_disable_timer(Config_ISRs)

    /** initializes single interrupt from timer device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return               ==0: initialization successfull; error code otherwise
    */
    e_ttc_interrupt_errorcode interrupt_stm32w1xx_init_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

    /** deinitializes single interrupt from timer device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    */
    void interrupt_stm32w1xx_deinit_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

    /** { resets interrupt settings of all TIMER units to their reset value
    *
    }*/
    void interrupt_stm32w1xx_deinit_all_timer();

    /** enables single interrupt from timer device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    */
    void interrupt_stm32w1xx_enable_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

    /** { enables/disables single interrupt from timer device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void interrupt_stm32w1xx_disable_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

#endif //}


/** { resets interrupt settings of all CAN units to their reset value
 *
}*/
void interrupt_stm32w1xx_deinit_all_rtc();


/** { deinitializes single interrupt from rtc device on current architecture
 *
 * Note: After deinitialization, the interrupt rtcnot be enabled anymore.
 *
 * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
}*/
void interrupt_stm32w1xx_deinit_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );


/** { enables single interrupt from gpio device on current architecture
 *
 * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
}*/
void interrupt_stm32w1xx_disable_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );


/** { enables single interrupt from gpio device on current architecture
 *
 * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
}*/
void interrupt_stm32w1xx_enable_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );


/** Generates a software interrupt for an external GPIO interrupt.
 *
 * @param Handle (t_ttc_interrupt_handle)
 */
void interrupt_stm32w1xx_generate_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );


/** { initializes single interrupt from rtc device on current architecture
 *
 * Note: After initialization, the interrupt must be enabled to trigger.
 *
 * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
}
 * @return   =
 */
e_ttc_interrupt_errorcode interrupt_stm32w1xx_init_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );


//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _interrupt_stm32w1xx_foo(ttc_foo_isrs_t* Config_ISRs)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //INTERRUPT_STM32W1XX_H
