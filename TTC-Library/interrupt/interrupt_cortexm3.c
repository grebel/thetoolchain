/** { interrupt_cortexm3.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for interrupt devices on cortexm3 architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 21 at 20140429 04:20:07 UTC
 *
 *  Note: See ttc_interrupt.h for description of cortexm3 independent INTERRUPT implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "interrupt_cortexm3.h"
#ifdef EXTENSION_cpu_stm32w1xx
    #include "../register/register_stm32w1xx.h"
#endif
#ifdef EXTENSION_cpu_stm32f1xx
    #include "../register/register_stm32f1xx.h"
#endif
#ifdef EXTENSION_cpu_stm32l1xx
    #include "../register/register_stm32l1xx.h"
#endif

//{ Global Variables ***********************************************************

typedef struct {
    // Source: Joseph Yiu; The definitive guid to the ARM Cotrex-M3 2nd Edition; Newnes 2010; p. 426
    unsigned MemoryManageFault            : 1; // Memory Manage Address Register (MMAR) contains valid fault addressing value
    unsigned BusFault                     : 1; // Bus Fault Address Register (BFAR) contains valid fault addressing value
    unsigned BusError_DataAccess          : 1; // Bus error occured during data access
    unsigned BusError_DataAccessImprecise : 1; // bus error during data access (no stack frame or fault address known)
    unsigned StackError                   : 1; // Error during stacking: Stack pointer corrupt/ Stack size too large (reaching forbidden region)
    unsigned UnStackError                 : 1; // Error during unstacking: Stack pointer corrupted during exception/ MPU configuration changed during exception
    unsigned DataAccessViolation          : 1; // user application tried to access privileged memory region
    unsigned InstructionAccessViolation   : 1; // user application tried to execute code in privileged memory region
    unsigned BusFaultDuringStacking       : 1; // a bus fault occured during a stacking operation (Stack pointer corrupt/ Stack overrun/ PSP used uninitialized)
    unsigned BusFaultDuringUnStacking     : 1; // a bus fault occured during a unstacking operation (Stack pointer corrupted during exception)
    unsigned InstructionBusError          : 1; // branch to invalid memory region
    unsigned DivisionByZeroError          : 1; // division by zero
    unsigned UnalignedDataAccess          : 1; // unaligned data access with UNALIGN_TRP set
    unsigned NoCoprocessorAvailable       : 1; // attempt to execute a coprocessor instruction
    unsigned InvalidProgramCounter        : 1; // compare "The definitive guid to the ARM Cortex-M3" p.427
    unsigned InvalidState                 : 1; // compare "The definitive guid to the ARM Cortex-M3" p.428
    unsigned UndefinedInstruction         : 1; // instruction found not known to CortexM3
    unsigned DebugFaultEvent              : 1; // fault is caused by a debug event
    unsigned ForcedEscalation             : 1; // hardfault was forced by escalation of fault that could not be handled
    unsigned VectorFetchFailed            : 1; // bus fault at vector fetch/ incorrect vector table offset
    unsigned reserved                     : 12;
} t_interrupt_cortexm3_interrupt_faults;

//}Global Variables
//{ Function Definitions *******************************************************

static void interrupt_cortexm3_simple_memcopy( volatile void* Target, volatile void* Source, t_u32 AmountBytes ) {
    AmountBytes /= 4;
    volatile t_u32* Writer = ( t_u32* ) Target;
    volatile t_u32* Reader = ( t_u32* ) Source;
    while ( AmountBytes-- > 0 ) {
        *Writer++ = *Reader++;
    }
}
void interrupt_cortexm3_prepare() {

}
void interrupt_cortexm3_HardFault_Handler( unsigned int* hardfault_args ) {

    // reconstruct registers
    // We declare all local variables as static to be sure that we do not overwrite any data
    volatile static unsigned int R0;  R0  =  hardfault_args[0];
    volatile static unsigned int R1;  R1  =  hardfault_args[1];
    volatile static unsigned int R2;  R2  = ( ( unsigned int ) hardfault_args[2] );
    volatile static unsigned int R3;  R3  = ( ( unsigned int ) hardfault_args[3] );
    volatile static unsigned int R12; R12 = ( ( unsigned int ) hardfault_args[4] );
    volatile static unsigned int LR;  LR  = ( ( unsigned int ) hardfault_args[5] );
    volatile static unsigned int PC;  PC  = ( ( unsigned int ) hardfault_args[6] );
    volatile static unsigned int PSR; PSR = ( ( unsigned int ) hardfault_args[7] );

    volatile static t_register_cortexm3_actlr     ACLTR;
    volatile static t_register_cortexm3_scb_cfsr   CFSR;
    volatile static t_register_cortexm3_scb_hfsr   HFSR;
    volatile static t_u32                         MMFAR;
    volatile static t_u32                          BFAR;

    interrupt_cortexm3_simple_memcopy( &ACLTR, &( register_cortexm3_ACTLR ),     sizeof( t_register_cortexm3_actlr ) );
    interrupt_cortexm3_simple_memcopy( &CFSR,  &( register_cortexm3_SCB.CFSR ),  sizeof( t_register_cortexm3_scb_cfsr ) );
    interrupt_cortexm3_simple_memcopy( &HFSR,  &( register_cortexm3_SCB.HFSR ),  sizeof( t_register_cortexm3_scb_hfsr ) );
    interrupt_cortexm3_simple_memcopy( &MMFAR, &( register_cortexm3_SCB.MMFAR ), sizeof( t_u32 ) );
    interrupt_cortexm3_simple_memcopy( &BFAR,  &( register_cortexm3_SCB.BFAR ),  sizeof( t_u32 ) );

    volatile static t_interrupt_cortexm3_interrupt_faults Reasons;
    *( ( t_u32* )( & Reasons ) ) = 0;

    // determine address of fault
    volatile void* volatile FaultingAddress = ( void* ) PC;
    ( void ) FaultingAddress; // avoid warning unused variable

    if ( CFSR.MMFSR.MMARVALID ) {
        Reasons.MemoryManageFault = 1;
        FaultingAddress = ( void* ) MMFAR;
    }
    if ( CFSR.MMFSR.MSTKERR ) {
        Reasons.StackError = 1;
    }
    if ( CFSR.MMFSR.MUNSTKERR ) {
        Reasons.UnStackError = 1;
    }
    if ( CFSR.MMFSR.DACCVIOL ) {
        Reasons.DataAccessViolation = 1;
    }
    if ( CFSR.MMFSR.IACCVIOL ) {
        Reasons.InstructionAccessViolation = 1;
    }
    if ( CFSR.BFSR.BFARVALID ) {
        Reasons.BusFault = 1;
        FaultingAddress = ( void* ) BFAR;
    }
    if ( CFSR.BFSR.STKERR ) {
        Reasons.BusFaultDuringStacking = 1;
    }
    if ( CFSR.BFSR.UNSTKERR ) {
        Reasons.BusFaultDuringUnStacking = 1;
    }
    if ( CFSR.BFSR.IMPRECISERR ) {
        Reasons.BusError_DataAccessImprecise = 1;
    }
    if ( CFSR.BFSR.PRECISEERR ) {
        Reasons.BusError_DataAccess = 1;
    }
    if ( CFSR.BFSR.IBUSERR ) {
        Reasons.InstructionBusError = 1;
    }
    if ( CFSR.UFSR.DIVBYZERO ) {
        Reasons.DivisionByZeroError = 1;
    }
    if ( CFSR.UFSR.UNALIGNED ) {
        Reasons.UnalignedDataAccess = 1;
    }
    if ( CFSR.UFSR.NOCP ) {
        Reasons.NoCoprocessorAvailable = 1;
    }
    if ( CFSR.UFSR.INVPC ) {
        Reasons.InvalidProgramCounter = 1;
    }
    if ( CFSR.UFSR.INVSTATE ) {
        Reasons.InvalidState = 1;
    }
    if ( CFSR.UFSR.UNDEFINSTR ) {
        Reasons.UndefinedInstruction = 1;
    }
    if ( HFSR.DebugVT ) {
        Reasons.DebugFaultEvent = 1;
    }
    if ( HFSR.Forced ) {
        Reasons.ForcedEscalation = 1;
    }
    if ( HFSR.VectorTable ) {
        Reasons.VectorFetchFailed = 1;
    }

    ( ( void )( R0 + R1 + R2 + R3 + R12 + LR + PC + PSR ) ); // unused vars

    // Hint - To see cause of hardfault type the following in your gdb:
    // l *PC
    // p Reasons
    // x FaultingAddress

    ttc_assert_halt_origin( ttc_assert_hardfault );
}
void interrupt_cortexm3_enable( t_u8 Channel, BOOL Enable ) {

    volatile t_u32* Register;
    if ( Enable )
    { Register = & register_cortexm3_NVIC.ISER0.All; } // choosing first Interrupt Set-Enable Register
    else
    { Register = & register_cortexm3_NVIC.ICER0.All; } // choosing first Interrupt Clear-Enable Register

    while ( Channel > 32 ) { // move to ISER register containing desired bit
        Register++;
        Channel -= 32;
    }

    // set/ clear interrupt
    *Register = 1 << ( Channel );
}
void interrupt_cortexm3_priority( t_u8 Channel, t_u8 GroupPriority, t_u8 SubPriority ) {
    //-> PM0056 p.135
    Assert_INTERRUPT( Channel <= 67, ttc_assert_origin_auto );  // CortexM3 has only 68 interrupt channels!

    t_register_cortexm3_nvic_ip IP_Value;
    switch ( register_cortexm3_SCB.AIRCR.PRIGROUP ) { // load register value according to current binary point (-> PM0056 p.135)
        case 0b000:
            IP_Value.Priority000.GroupPriority = GroupPriority;
            IP_Value.Priority000.SubPriority   = SubPriority;
            break;
        case 0b001:
            IP_Value.Priority001.GroupPriority = GroupPriority;
            IP_Value.Priority001.SubPriority   = SubPriority;
            break;
        case 0b010:
            IP_Value.Priority010.GroupPriority = GroupPriority;
            IP_Value.Priority010.SubPriority   = SubPriority;
            break;
        case 0b011:
            IP_Value.Priority011.GroupPriority = GroupPriority;
            IP_Value.Priority011.SubPriority   = SubPriority;
            break;
        case 0b100:
            IP_Value.Priority100.GroupPriority = GroupPriority;
            IP_Value.Priority100.SubPriority   = SubPriority;
            break;
        case 0b101:
            IP_Value.Priority101.GroupPriority = GroupPriority;
            IP_Value.Priority101.SubPriority   = SubPriority;
            break;
        case 0b110:
            IP_Value.Priority110.GroupPriority = GroupPriority;
            IP_Value.Priority110.SubPriority   = SubPriority;
            break;
        case 0b111:
            IP_Value.Priority111.SubPriority   = SubPriority;
            break;
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid register value
    }

    // update register
    register_cortexm3_NVIC.IP[Channel].Byte = IP_Value.Byte;
}
void HardFault_Handler() {

    __ASM volatile( ".global HardFault_Handler" );
    __ASM volatile( ".extern interrupt_cortexm3_HardFault_Handler" );
    __ASM volatile( "TST LR, #4" );
    __ASM volatile( "ITE EQ" );
    __ASM volatile( "MRSEQ R0, MSP" );
    __ASM volatile( "MRSNE R0, PSP" );
    __ASM volatile( "B interrupt_cortexm3_HardFault_Handler" );
}
void Default_Handler_C() {
    ttc_assert_halt_origin( ttc_assert_origin_auto );
}
void NMI_Handler() {
    HardFault_Handler();
    //? ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void MemManage_Handler() {
    ttc_assert_halt_origin( ttc_assert_origin_auto );
}
void SVC_Handler() {
    ttc_assert_halt_origin( ttc_assert_origin_auto );
}
void DebugMonitor_Handler() {
    ttc_assert_halt_origin( ttc_assert_origin_auto );
}
void __attribute__( ( weak ) ) PendSV_Handler() {
    ttc_assert_halt_origin( ttc_assert_origin_auto );
}
void __attribute__( ( weak ) ) SysTick_Handler() {
    ttc_assert_halt_origin( ttc_assert_origin_auto );
}
void BusFault_Handler() {

    __ASM volatile( ".global BusFault_Handler" );
    __ASM volatile( ".extern interrupt_cortexm3_HardFault_Handler" );
    __ASM volatile( "TST LR, #4" );
    __ASM volatile( "ITE EQ" );
    __ASM volatile( "MRSEQ R0, MSP" );
    __ASM volatile( "MRSNE R0, PSP" );
    __ASM volatile( "B interrupt_cortexm3_HardFault_Handler" );
}
void UsageFault_Handler() {

    __ASM volatile( ".global UsageFault_Handler" );
    __ASM volatile( ".extern interrupt_cortexm3_HardFault_Handler" );
    __ASM volatile( "TST LR, #4" );
    __ASM volatile( "ITE EQ" );
    __ASM volatile( "MRSEQ R0, MSP" );
    __ASM volatile( "MRSNE R0, PSP" );
    __ASM volatile( "B interrupt_cortexm3_HardFault_Handler" );
}
BOOL interrupt_cortexm3_check_inside_isr() {

    return ( BOOL ) register_cortexm3_SCB.ICSR.VECTACTIVE;
}
BOOL interrupt_cortexm3_check_all_disabled() {
    t_u32 AllInterruptsDisabled;

    //? __ASM volatile( "mrs R0, PRIMASK; strb R0, [%0]" : "=rw"( AllInterruptsDisabled ) );
    __ASM volatile( "mrs %0, PRIMASK" : "=r"( AllInterruptsDisabled ) );

    if ( AllInterruptsDisabled )
    { return TRUE; }
    return FALSE;
}


//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
