#ifndef INTERRUPT_CORTEXM3_TYPES_H
#define INTERRUPT_CORTEXM3_TYPES_H

/** { interrupt_cortexm3.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for INTERRUPT devices on cortexm3 architectures.
 *  Structures, Enums and Defines being required by ttc_interrupt_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140429 04:20:07 UTC
 *
 *  Note: See ttc_interrupt.h for description of architecture independent INTERRUPT implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_INTERRUPT1   // device not defined in makefile
    #define TTC_INTERRUPT1    E_ttc_interrupt_architecture_cortexm3   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_interrupt_types.h *************************

//} Structures/ Enums


#endif //INTERRUPT_CORTEXM3_TYPES_H
