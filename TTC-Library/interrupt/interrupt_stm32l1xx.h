#ifndef INTERRUPT_STM32L1XX_H
#define INTERRUPT_STM32L1XX_H

/** { interrupt_stm32l1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for interrupt devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by high-level interrupt and application.
 *
 *  Created from template device_architecture.h revision 22 at 20140424 04:22:44 UTC
 *
 *  Note: See ttc_interrupt.h for description of stm32l1xx independent INTERRUPT implementation.
 *
 *  Authors: Greg Knoll 2014.
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_INTERRUPT_STM32L1XX
//
// Implementation status of low-level driver support for interrupt devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_INTERRUPT_STM32L1XX 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_INTERRUPT_STM32L1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_INTERRUPT_STM32L1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_INTERRUPT_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "interrupt_stm32l1xx_types.h"
#include "../ttc_interrupt_types.h"
#include "stm32l1xx.h"       // must be included BEFORE core_cm3.h
#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
#include "stm32l1xx_exti.h"
#include "misc.h"            // provides NVIC_InitTypeDef
#include "stm32l1xx_rcc.h"   // provides RCC_APB2PeriphClockCmd()
#include "stm32l1xx_syscfg.h"
#include "../ttc_register.h"
#include "../ttc_memory.h"

#ifdef EXTENSION_ttc_usart
    #include "../ttc_usart_types.h"
    #include "stm32l1xx_usart.h"
    #include "../usart/usart_stm32l1xx.h"
#endif
#ifdef EXTENSION_ttc_timer
    #include "../ttc_timer_types.h"
    #include "stm32l1xx_tim.h"
#endif
#ifdef EXTENSION_ttc_gpio
    #include "stm32l1xx_gpio.h"  // provides GPIO_PortSourceGPIOA
    #include "../ttc_gpio.h"
#endif
#ifdef EXTENSION_ttc_can
    #include "../ttc_can_types.h"
#endif
#ifdef EXTENSION_ttc_rtc
    #include "../ttc_rtc_types.h"
    #include "stm32l1xx_rtc.h"
    #include "../rtc/rtc_stm32l1xx.h"
#endif

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_interrupt_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_interrupt_foo
//
#define ttc_driver_interrupt_all_enable()               interrupt_cortexm3_all_enable()
#define ttc_driver_interrupt_all_disable()              interrupt_cortexm3_all_disable()
#define ttc_driver_interrupt_check_all_disabled         interrupt_cortexm3_check_all_disabled
#define ttc_driver_interrupt_deinit(Config_ISRs)        interrupt_stm32f1xx_deinit(Config_ISRs)
#define ttc_driver_interrupt_prepare()                  interrupt_stm32l1xx_prepare()
#define ttc_driver_interrupt_reset(Config_ISRs)         interrupt_stm32f1xx_reset(Config_ISRs)
#define ttc_driver_interrupt_deinit_all_usart()         interrupt_stm32l1xx_deinit_all_usart()
#define ttc_driver_interrupt_generate_gpio(Config_ISRs) interrupt_stm32l1xx_generate_gpio(Config_ISRs)
//#define ttc_driver_interrupt_check_inside_isr()  (provided by interrupt_cortexm3.h)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_interrupt.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_interrupt.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


//Implemented

#ifdef EXTENSION_ttc_spi
    #define ttc_driver_interrupt_deinit_spi(Config_ISRs) interrupt_stm32l1xx_deinit_spi(Config_ISRs)
    #define ttc_driver_interrupt_disable_spi(Config_ISRs) interrupt_stm32l1xx_disable_spi(Config_ISRs)
    #define ttc_driver_interrupt_enable_spi(Config_ISRs) interrupt_stm32l1xx_enable_spi(Config_ISRs)
    #define ttc_driver_interrupt_init_spi(Config_ISRs) interrupt_stm32l1xx_init_spi(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_spi() interrupt_stm32l1xx_deinit_all_spi()

    /** { resets interrupt settings of all SPI units to their reset value
    *
    }*/
    void interrupt_stm32l1xx_deinit_all_spi();

    /** { deinitializes single interrupt from spi device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_deinit_spi( t_ttc_interrupt_config_spi* Config_ISRs );

    /** { enables/disables single interrupt from spi device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_disable_spi( t_ttc_interrupt_config_spi* Config_ISRs );


    /** { enables/disables single interrupt from spi device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_enable_spi( t_ttc_interrupt_config_spi* Config_ISRs );

    /** { initializes single interrupt from spi device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    e_ttc_interrupt_errorcode interrupt_stm32l1xx_init_spi( t_ttc_interrupt_config_spi* Config_ISRs );

#endif

#ifdef EXTENSION_ttc_usart

    #define ttc_driver_interrupt_deinit_usart(Config_ISRs)        interrupt_stm32l1xx_deinit_usart(Config_ISRs)
    #define ttc_driver_interrupt_disable_usart(Config_ISRs, Type) interrupt_stm32l1xx_disable_usart(Config_ISRs, Type)
    #define ttc_driver_interrupt_enable_usart(Config_ISRs, Type)  interrupt_stm32l1xx_enable_usart(Config_ISRs, Type)
    #define ttc_driver_interrupt_init_usart(Config_ISRs, Type)    interrupt_stm32l1xx_init_usart(Config_ISRs, Type)
    #undef  ttc_driver_interrupt_deinit_all_usart  // using default implementation in ttc_interrupt_interface.c: ttc_driver_interrupt_deinit_usart()

    /** { deinitializes single interrupt from usart device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_deinit_usart( t_ttc_interrupt_config_usart* Config_ISRs );

    /** { enables/disables single interrupt from usart device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_disable_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type );

    /** { enables/disables single interrupt from usart device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_enable_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type );

    /** { initializes single interrupt from usart device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    e_ttc_interrupt_errorcode interrupt_stm32l1xx_init_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type );

#endif



#ifdef EXTENSION_ttc_timer

    #define ttc_driver_interrupt_deinit_timer(Config_ISRs) interrupt_stm32l1xx_deinit_timer(Config_ISRs)
    #define ttc_driver_interrupt_disable_timer(Config_ISRs) interrupt_stm32l1xx_disable_timer(Config_ISRs)
    #define ttc_driver_interrupt_enable_timer(Config_ISRs) interrupt_stm32l1xx_enable_timer(Config_ISRs)
    #define ttc_driver_interrupt_init_timer(Config_ISRs) interrupt_stm32l1xx_init_timer(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_timer() interrupt_stm32l1xx_deinit_all_timer()

    /** { resets interrupt settings of all TIMER units to their reset value
    *
    }*/
    void interrupt_stm32l1xx_deinit_all_timer();

    /** { initializes single interrupt from timer device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_deinit_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

    /** { enables/disables single interrupt from timer device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_disable_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

    /** { enables/disables single interrupt from timer device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_enable_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

    /** { initializes single interrupt from timer device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    e_ttc_interrupt_errorcode interrupt_stm32l1xx_init_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

#endif
#ifdef EXTENSION_ttc_i2c

    #define ttc_driver_interrupt_deinit_i2c(Config_ISRs) interrupt_stm32l1xx_deinit_i2c(Config_ISRs)
    #define ttc_driver_interrupt_disable_i2c(Config_ISRs) interrupt_stm32l1xx_disable_i2c(Config_ISRs)
    #define ttc_driver_interrupt_enable_i2c(Config_ISRs) interrupt_stm32l1xx_enable_i2c(Config_ISRs)
    #define ttc_driver_interrupt_init_i2c(Config_ISRs) interrupt_stm32l1xx_init_i2c(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_i2c() interrupt_stm32l1xx_deinit_all_i2c()

    /** { resets interrupt settings of all I2C units to their reset value
    *
    }*/
    void interrupt_stm32l1xx_deinit_all_i2c();

    /** { initializes single interrupt from i2c device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_deinit_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

    /** { enables/disables single interrupt from i2c device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_disable_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

    /** { enables/disables single interrupt from i2c device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_enable_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

    /** { initializes single interrupt from i2c device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    e_ttc_interrupt_errorcode interrupt_stm32l1xx_init_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

#endif

#ifdef EXTENSION_ttc_gpio

    #define ttc_driver_interrupt_deinit_gpio(Config_ISRs) interrupt_stm32l1xx_deinit_gpio(Config_ISRs)
    #define ttc_driver_interrupt_disable_gpio(Config_ISRs) interrupt_stm32l1xx_disable_gpio(Config_ISRs)
    #define ttc_driver_interrupt_enable_gpio(Config_ISRs) interrupt_stm32l1xx_enable_gpio(Config_ISRs)
    #define ttc_driver_interrupt_gpio_calc_config_index(PhysicalIndex) interrupt_stm32l1xx_gpio_calc_config_index(PhysicalIndex)
    #define ttc_driver_interrupt_init_gpio(Config_ISRs) interrupt_stm32l1xx_init_gpio(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_gpio() interrupt_stm32l1xx_deinit_all_gpio()

    /** { deinitializes single interrupt from gpio device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void interrupt_stm32l1xx_deinit_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );


    /** { enables/ disables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    */
    void interrupt_stm32l1xx_enable_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );



    /** { Calculates index of ttc_interrupt_config_gpio[] corresponding for given pin
    *
    * @param   PhysicalIndex  32-bit value representing GPIO-bank + pin
    * @return  Index of internal interrupt line serving given pin (0..TTC_INTERRUPT_GPIO_AMOUNT-1)
    }*/
    t_u8 interrupt_stm32l1xx_gpio_calc_config_index( t_base PhysicalIndex );

    /** { initializes single interrupt from gpio device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    e_ttc_interrupt_errorcode interrupt_stm32l1xx_init_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );

    /** { resets interrupt settings of all GPIO units to their reset value
    *
    }*/
    void interrupt_stm32l1xx_deinit_all_gpio();

    /** { enables/ disables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_disable_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );

    /** Generates a software interrupt for an external GPIO interrupt.
    *
    * @param Handle (t_ttc_interrupt_handle)
    */
    void interrupt_stm32l1xx_generate_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );

#endif

/** { Prepares interrupt driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
}*/
void interrupt_stm32l1xx_prepare();

//Not Implemented

#ifdef EXTENSION_ttc_can

    #define ttc_driver_interrupt_deinit_can(Config_ISRs) interrupt_stm32l1xx_deinit_can(Config_ISRs)
    #define ttc_driver_interrupt_disable_can(Config_ISRs) interrupt_stm32l1xx_disable_can(Config_ISRs)
    #define ttc_driver_interrupt_enable_can(Config_ISRs) interrupt_stm32l1xx_enable_can(Config_ISRs)
    #define ttc_driver_interrupt_init_can(Config_ISRs) interrupt_stm32l1xx_init_can(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_can() interrupt_stm32l1xx_deinit_all_can()

    /** { deinitializes single interrupt from can device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    e_ttc_interrupt_errorcode interrupt_stm32l1xx_deinit_can( t_ttc_interrupt_config_can* Config_ISRs );

    /** { enables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_disable_can( t_ttc_interrupt_config_can* Config_ISRs );
    /** { enables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_enable_can( t_ttc_interrupt_config_can* Config_ISRs );

    /** { initializes single interrupt from can device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    e_ttc_interrupt_errorcode interrupt_stm32l1xx_init_can( t_ttc_interrupt_config_can* Config_ISRs );


    /** { resets interrupt settings of all CAN units to their reset value
    *
    }*/
    void interrupt_stm32l1xx_deinit_all_can();

#endif
#ifdef EXTENSION_ttc_rtc
    #define ttc_driver_interrupt_deinit_rtc(Config_ISRs)  interrupt_stm32l1xx_deinit_rtc(Config_ISRs)
    #define ttc_driver_interrupt_disable_rtc(Config_ISRs) interrupt_stm32l1xx_disable_rtc(Config_ISRs)
    #define ttc_driver_interrupt_enable_rtc(Config_ISRs)  interrupt_stm32l1xx_enable_rtc(Config_ISRs)
    #define ttc_driver_interrupt_init_rtc(Config_ISRs)    interrupt_stm32l1xx_init_rtc(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_rtc()         interrupt_stm32l1xx_deinit_all_rtc()

    /** { resets interrupt settings of all RTC units to their reset value
    *
    }*/
    void interrupt_stm32l1xx_deinit_all_usart();

    /** { deinitializes single interrupt from usart device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_deinit_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );

    /** { enables/disables single interrupt from usart device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_disable_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );

    /** { enables/disables single interrupt from usart device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    void interrupt_stm32l1xx_enable_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );

    /** { initializes single interrupt from usart device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    * @return              = ec_interrupt_OK or error code
    }*/
    e_ttc_interrupt_errorcode interrupt_stm32l1xx_init_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );

    e_ttc_interrupt_errorcode interrupt_stm32l1xx_pin_line_wakeup_rtc();

#endif


/* initializes interrupt of given type to call given ISR
 * @param PhysicalIndex  real index of physical functional unit (e.g 0=USART1, ...)
 * @param ISR_USART      pointer to register-base of USART that triggered the interrupt
 */
//void USART_General_IRQConfig_ISRsr(t_physical_index PhysicalIndex, volatile t_register_stm32f1xx_usart* ISR_USART);

//void TIMER_General_IRQConfig_ISRsr(t_physical_index PhysicalIndex, volatile t_register_stm32f1xx_timer* TIMER);
//void CAN_General_IRQConfig_ISRsr(t_physical_index PhysicalIndex, volatile t_register_stm32f1xx_can* ISR_CAN);

//GPIO
void GPIO_General_IRQHandler( t_physical_index InterruptLine );
void EXTI0_IRQHandler();
void EXTI1_IRQHandler();
void EXTI2_IRQHandler();
void EXTI3_IRQHandler();
void EXTI4_IRQHandler();
void EXTI9_5_IRQHandler();
void EXTI15_10_IRQHandler();


//Overwrite weak definitions defined in startup?stm32l1xx_md.s (in 270_CPAL_CMSIS folder)
void WWDG_IRQHandler();
void PVD_IRQHandler();
void TAMPER_STAMP_IRQHandler();
void RTC_WKUP_IRQHandler();
void FLASH_IRQHandler();
void RCC_IRQHandler();

void DMA1_Channel1_IRQHandler();
void DMA1_Channel2_IRQHandler();
void DMA1_Channel3_IRQHandler();
void DMA1_Channel4_IRQHandler();
void DMA1_Channel5_IRQHandler();
void DMA1_Channel6_IRQHandler();
void DMA1_Channel7_IRQHandler();
void ADC1_IRQHandler();
void USB_HP_IRQHandler();
void USB_LP_IRQHandler();
void DAC_IRQHandler();
void COMP_IRQHandler();

void LCD_IRQHandler();
void TIM9_IRQHandler();
void TIM10_IRQHandler();
void TIM11_IRQHandler();
void TIM2_IRQHandler();
void TIM3_IRQHandler();
void TIM4_IRQHandler();
void I2C1_EV_IRQHandler();
void I2C1_ER_IRQHandler();
void I2C2_EV_IRQHandler();
void I2C2_ER_IRQHandler();
void SPI1_IRQHandler();
void SPI2_IRQHandler();
void USART1_IRQHandler();
void USART2_IRQHandler();
//void USART3_IRQHandler();

void RTC_Alarm_IRQHandler();
void USB_FS_WKUP_IRQHandler();
void TIM6_IRQHandler();
void TIM7_IRQHandler();


//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _interrupt_stm32l1xx_foo(t_ttc_interrupt_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //INTERRUPT_STM32L1XX_H
