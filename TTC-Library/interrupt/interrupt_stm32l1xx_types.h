#ifndef INTERRUPT_STM32L1XX_TYPES_H
#define INTERRUPT_STM32L1XX_TYPES_H

/** { interrupt_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for INTERRUPT devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_interrupt_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140424 04:22:44 UTC
 *
 *  Note: See ttc_interrupt.h for description of architecture independent INTERRUPT implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************
#define TTC_INTERRUPT_GPIO_AMOUNT  16 // CortexM3 provides 16 lines for external gpio-interrupts
#define TTC_INTERRUPT_USART_AMOUNT 3
#define TTC_INTERRUPT_RTC_AMOUNT 5
#define TTC_INTERRUPT_TIMER_AMOUNT 9
#define TTC_INTERRUPT_SPI_AMOUNT 3
#define TTC_INTERRUPT_I2C_AMOUNT 2
#ifndef TTC_INTERRUPT1   // device not defined in makefile
    #define TTC_INTERRUPT1    E_ttc_interrupt_architecture_stm32l1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic.h"
#include "../register/register_stm32l1xx_types.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_interrupt_types.h *************************


typedef struct { // register description (adapt according to stm32l1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_interrupt_register;

typedef struct {  // stm32l1xx specific configuration data of single INTERRUPT device
    t_register_stm32l1xx_exti* BaseRegister;       // base address of INTERRUPT device registers
} __attribute__( ( __packed__ ) ) t_interrupt_stm32l1xx_config;

// t_ttc_interrupt_architecture is required by ttc_interrupt_types.h
#define t_ttc_interrupt_architecture t_interrupt_stm32l1xx_config

typedef struct {  // stm32l1xx specific interrupt configuration data of single I2C device
    t_register_stm32l1xx_i2c* BaseRegister;       // base address of I2C device registers
} __attribute__( ( __packed__ ) ) t_interrupt_stm32l1xx_config_i2c;

typedef struct {  // stm32l1xx specific interrupt configuration data of single I2C device
    volatile t_register_stm32l1xx_usart* BaseRegister;       // base address of I2C device registers
} __attribute__( ( __packed__ ) ) t_interrupt_stm32l1xx_config_usart;

#define interrupt_driver_config_i2c_t   t_interrupt_stm32l1xx_config_i2c
#define interrupt_driver_config_usart_t t_interrupt_stm32l1xx_config_usart
//} Structures/ Enums


#endif //INTERRUPT_STM32L1XX_TYPES_H
