#ifndef INTERRUPT_CORTEXM3_H
//#define INTERRUPT_CORTEXM3_H

/** { interrupt_cortexm3.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for interrupt devices on cortexm3 architectures.
 *  Structures, Enums and Defines being required by high-level interrupt and application.
 *
 *  Created from template device_architecture.h revision 22 at 20140429 04:20:07 UTC
 *
 *  Note: See ttc_interrupt.h for description of cortexm3 independent INTERRUPT implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

/* NOTE!! IF THIS VALUE IS CHANGED, NVIC-CONFIG.H MUST ALSO BE UPDATED */
#define INTERRUPTS_DISABLED_PRIORITY (12 << 3)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_INTERRUPT_CORTEXM3
//
// Implementation status of low-level driver support for interrupt devices on cortexm3
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_INTERRUPT_CORTEXM3 '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_INTERRUPT_CORTEXM3 == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_INTERRUPT_CORTEXM3 to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_INTERRUPT_CORTEXM3

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "interrupt_cortexm3_types.h"
#include "../ttc_interrupt_types.h"
#include "core_cm3.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_interrupt_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_interrupt_foo
//
// This driver is not directly used by ttc_interrupt but from all CortexM3 based
// low-level drivers. We do not implement the standard ttc_driver_interrupt_*() macros.
//
#if 0 // undefines below must not be compiled!
    // They are required to signal create_DeviceDriver.pl not to insert them again.

    #undef ttc_driver_interrupt_deinit_all_can
    #undef ttc_driver_interrupt_deinit_all_gpio
    #undef ttc_driver_interrupt_deinit_all_i2c
    #undef ttc_driver_interrupt_deinit_all_spi
    #undef ttc_driver_interrupt_deinit_all_timer
    #undef ttc_driver_interrupt_deinit_all_usart
    #undef ttc_driver_interrupt_deinit_can
    #undef ttc_driver_interrupt_deinit_gpio
    #undef ttc_driver_interrupt_deinit_i2c
    #undef ttc_driver_interrupt_deinit_spi
    #undef ttc_driver_interrupt_deinit_timer
    #undef ttc_driver_interrupt_deinit_usart
    #undef ttc_driver_interrupt_disable_can
    #undef ttc_driver_interrupt_disable_gpio
    #undef ttc_driver_interrupt_disable_i2c
    #undef ttc_driver_interrupt_disable_spi
    #undef ttc_driver_interrupt_disable_timer
    #undef ttc_driver_interrupt_disable_usart
    #undef ttc_driver_interrupt_enable_can
    #undef ttc_driver_interrupt_enable_gpio
    #undef ttc_driver_interrupt_enable_i2c
    #undef ttc_driver_interrupt_enable_spi
    #undef ttc_driver_interrupt_enable_timer
    #undef ttc_driver_interrupt_enable_usart
    #undef ttc_driver_interrupt_generate_gpio
    #undef ttc_driver_interrupt_gpio_create_index
    #undef ttc_driver_interrupt_gpio_from_index
    #undef ttc_driver_interrupt_init_can
    #undef ttc_driver_interrupt_init_gpio
    #undef ttc_driver_interrupt_init_i2c
    #undef ttc_driver_interrupt_init_spi
    #undef ttc_driver_interrupt_init_timer
    #undef ttc_driver_interrupt_init_usart
    #undef ttc_driver_interrupt_prepare
    #undef ttc_driver_interrupt_init_rtc
    #undef ttc_driver_interrupt_deinit_all_rtc
    #undef ttc_driver_interrupt_deinit_rtc
    #undef ttc_driver_interrupt_disable_rtc
    #undef ttc_driver_interrupt_enable_rtc
    #undef ttc_driver_interrupt_init_rtc
    #undef ttc_driver_interrupt_check_all_disabled
#endif
#define ttc_driver_interrupt_check_inside_isr   interrupt_cortexm3_check_inside_isr
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_interrupt.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_interrupt.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

/** { Prepares interrupt driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
}*/
void interrupt_cortexm3_prepare();

/** Globally disables all interrupts
 *
 * Disabling all interrupts is very fast (one assembly instruction) and will not change configurations of individual interrupts.
 *
 */
#define interrupt_cortexm3_all_disable() __ASM volatile( "cpsid i" )

/** Globally enables all interrupts
 *
 * Enabling all interrupts is very fast (one assembly instruction) and will not change configurations of individual interrupts.
 *
 */
#define interrupt_cortexm3_all_enable() __ASM volatile( "cpsie i" )

/** Handler for Hardfault interrupt that tries to give you a clue what has happened
  *
  * Note: Will call ttc_assert_halt_origin()
  *       Place a breakpoint inside ttc_assert_halt_origin() to get informed when this handler is executed!
  *
  */
void interrupt_cortexm3_HardFault_Handler( unsigned int* hardfault_args );

/** Enable/ Disable given NVIC interrupt channel
 *
 * Note: CortexM3 interrupts are described in STM's PM0056 p.120
 *
 * @param Channel   interrupt channel (0..67) - mapped functionality depends on your current microcontroller
 * @param Enable    ==0: disable; !=0: enable interrupt line
 */
void interrupt_cortexm3_enable( t_u8 Channel, BOOL Enable );

/** set group- and sub-priority of single interrupt channel
 *
 * Note: Register values are calculated according to current binary point setting in SCB_AICR.PRIGROUP.
 *       Refer to PM0056 p.135 for details.
 *
 * @param Channel        NVIC interrupt channel (0..67)
 * @param GroupPriority
 */
void interrupt_cortexm3_priority( t_u8 Channel, t_u8 GroupPriority, t_u8 SubPriority );


/** Checks if current function is running inside an interrupt service routine.
 *
 * @return (BOOL)  ==TRUE: current function has been called from an interrupt service routine; ==FALSE: called from main() or a task.
 */
BOOL interrupt_cortexm3_check_inside_isr();


/** Checks if interrupts are globally disabled.
 *
 * Most microcontrollers allow to globally disabled interrrupts.
 * Some operations may depend on interrupts being disabled or enabled.
 * This check is typically very fast.
 *
 * @return (BOOL)  == 0: interrupts not known to be disabled (does not check individual interrupts); !=0: all interrupts are disabled
 */
BOOL interrupt_cortexm3_check_all_disabled();

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _interrupt_cortexm3_foo(t_ttc_interrupt_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //INTERRUPT_CORTEXM3_H
