/** { interrupt_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for interrupt devices on stm32l1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 21 at 20140424 04:22:44 UTC
 *
 *  Note: See ttc_interrupt.h for description of stm32l1xx independent INTERRUPT implementation.
 *
 *  Authors: Greg Knoll, Gregor Rebel
 *
 *  ToDo: Replace all Standard Peripheral code by direct register access and remove dependency on stm32l1xx_usart.c
}*/

#include "interrupt_cortexm3.h"
#include "interrupt_stm32l1xx.h"
#include "../basic/basic_cm3.h"
#include "../ttc_heap.h"

//{ Global Variables ***********************************************************
// All initializers use same configuration variable to safe memory and speed up initialization.
// Note: This makes interrupt initialization thread unsafe! Always enclose your initialisation calls in critical sections!

NVIC_InitTypeDef interrupt_stm32l1xx_NVIC_Cfg; // ToDo: get rid of this!
const t_u8 interrupt_stm32l1xx_IRQChannel_USART[TTC_INTERRUPT_USART_AMOUNT] = { USART1_IRQn, USART2_IRQn, USART3_IRQn };
volatile t_register_stm32l1xx_usart* const interrupt_stm32l1xx_BaseRegister_USART[TTC_INTERRUPT_USART_AMOUNT] = {
    ( volatile t_register_stm32l1xx_usart* ) USART1,
    ( volatile t_register_stm32l1xx_usart* ) USART2,
    ( volatile t_register_stm32l1xx_usart* ) USART3
};

// provided by ttc_interrupt.c
#ifdef EXTENSION_ttc_usart
    A_dynamic_extern( t_ttc_interrupt_config_usart*, ttc_interrupt_config_usart );
#endif
#ifdef EXTENSION_ttc_gpio
    A_dynamic_extern( t_ttc_interrupt_config_gpio*,  ttc_interrupt_config_gpio );
#endif
#ifdef EXTENSION_ttc_rtc
    A_dynamic_extern( t_ttc_interrupt_config_rtc*,  ttc_interrupt_config_rtc );
#endif
#ifdef EXTENSION_ttc_timer
    A_dynamic_extern( t_ttc_interrupt_configimer_t*,  t_ttc_interrupt_configimer );
#endif
#ifdef EXTENSION_ttc_i2c
    A_dynamic_extern( t_ttc_interrupt_config_i2c*, ttc_interrupt_config_i2c );
#endif
#ifdef EXTENSION_ttc_can
    # warning stm32l1xx has no CAN Interface.
#endif
//}Global Variables
//{ Function Definitions *******************************************************


//------------------------------GPIO----------------------------------------//
#ifdef EXTENSION_ttc_gpio
const t_u8 interrupt_stm32l1xx_IRQChannel_GPIO[TTC_INTERRUPT_GPIO_AMOUNT] = { EXTI0_IRQn,
                                                                              EXTI1_IRQn,
                                                                              EXTI2_IRQn,
                                                                              EXTI3_IRQn,
                                                                              EXTI4_IRQn,
                                                                              EXTI9_5_IRQn,
                                                                              EXTI9_5_IRQn,
                                                                              EXTI9_5_IRQn,
                                                                              EXTI9_5_IRQn,
                                                                              EXTI9_5_IRQn,
                                                                              EXTI15_10_IRQn,
                                                                              EXTI15_10_IRQn,
                                                                              EXTI15_10_IRQn,
                                                                              EXTI15_10_IRQn,
                                                                              EXTI15_10_IRQn,
                                                                              EXTI15_10_IRQn
                                                                            };
e_ttc_interrupt_errorcode interrupt_stm32l1xx_init_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {
    t_ttc_gpio_bank GPIOx;
    t_u8 Pin;

    ttc_gpio_from_index( Config_ISRs->PhysicalIndex, &GPIOx, &Pin );
    Assert_INTERRUPT( GPIOx, ttc_assert_origin_auto );
    Assert_INTERRUPT( Pin < TTC_GPIO_MAX_PINS, ttc_assert_origin_auto );

    Config_ISRs->LowLevel.Mask = 1 << Pin;

    // ensure interrupt is disabled before reconfiguring it
    interrupt_stm32l1xx_disable_gpio( Config_ISRs );

    // initialize GPIO pin as input
    ttc_gpio_init( Config_ISRs->PhysicalIndex, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_max );

    // calculate bit banding address of EXTI-line (==Pin) in first register of
    t_u32* BitBand_EXTI = cm3_calc_peripheral_BitBandAddress( &register_stm32l1xx_EXTI, Pin );

    // calculate bit banding adresses of corresponding bits in EXTI sub registers by adding constant register offsets
    t_u32* BitBand_RTSR = ( t_u32* )( ( ( t_u32 ) BitBand_EXTI )
                                      + 32 * ( ( ( t_u32 ) &register_stm32l1xx_EXTI.RTSR ) - ( ( t_u32 ) &register_stm32l1xx_EXTI ) )
                                    );
    t_u32* BitBand_FTSR = ( t_u32* )( ( ( t_u32 ) BitBand_EXTI )
                                      + 32 * ( ( ( t_u32 ) &register_stm32l1xx_EXTI.FTSR ) - ( ( t_u32 ) &register_stm32l1xx_EXTI ) )
                                    );

    // EXTI_RTSR + EXTI_FTSR
    switch ( Config_ISRs->Common.Type ) {

        case  tit_GPIO_Active_Low:         // voltage on input pin is low
        case  tit_GPIO_Falling:            // voltage on input pin falls from logical 1 to 0

            *BitBand_RTSR = 0; // disable rising trigger
            *BitBand_FTSR = 1; // enable falling trigger
            break;

        case  tit_GPIO_Active_High:        // voltage on input pin is high
        case  tit_GPIO_Rising:             // voltage on input pin rises from logical 0 to 1

            *BitBand_FTSR = 0; // disable falling trigger
            *BitBand_RTSR = 1; // enable rising trigger
            break;

        case  tit_GPIO_Rising_Falling:     // voltage on input pin falls or rises
            *BitBand_FTSR = 1; // enable falling trigger
            *BitBand_RTSR = 1; // enable rising trigger
            break;

        default: ttc_assert_halt_origin( ec_basic_InvalidImplementation ); break; // unknown/ invalid interrupt type!
    }

    t_u8 IRQ_Channel = interrupt_stm32l1xx_IRQChannel_GPIO[Pin];
    Assert_INTERRUPT( IRQ_Channel < 57, ttc_assert_origin_auto );  // only interrupt lines 0..56 available in STM32L1xx (->RM0038 p,181,182)

    // set interrupt priority of corresponding IRQ channel in NVIC
    interrupt_cortexm3_priority( IRQ_Channel, 0, 3 );

    // enable selected NVIC IRQ channel
    interrupt_cortexm3_enable( IRQ_Channel, 1 );

    return ec_interrupt_OK;
}
void   interrupt_stm32l1xx_enable_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {
#if (TTC_Assert_INTERRUPTS==1)
    Assert_INTERRUPT( !A_is_null( ttc_interrupt_config_gpio ), ttc_assert_origin_auto );  // init interrupt first!
    t_ttc_interrupt_config_gpio* GPIO_ISR_Entry = A( ttc_interrupt_config_gpio, Pin );
    Assert_INTERRUPT( GPIO_ISR_Entry, ttc_assert_origin_auto );  // init interrupt first!
#endif

    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL


    // set corresponding bit in Interrupt Mask Register
    register_stm32l1xx_EXTI.IMR.All |= Config_ISRs->LowLevel.Mask;

    // re-set pending bit (required to re-activate if interrupt already occured)
    register_stm32l1xx_EXTI.PR.All = Config_ISRs->LowLevel.Mask;
}
void   interrupt_stm32l1xx_deinit_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

    interrupt_stm32l1xx_disable_gpio( Config_ISRs );

    if ( 1 ) { // disable interrupt channel in NVIC ICER register
        t_ttc_gpio_bank* GPIOx;
        t_u8 Pin;
        ttc_gpio_from_index( Config_ISRs->PhysicalIndex, ( t_ttc_gpio_bank* ) &GPIOx, &Pin );

        t_u8 IRQ_Channel = interrupt_stm32l1xx_IRQChannel_GPIO[Pin];
        t_u8 RegisterIndex = IRQ_Channel / 32;
        Assert_INTERRUPT( RegisterIndex < 3, ttc_assert_origin_auto );  // each NVIC register has three 32 bit fields!

        // disable selected IRQ channel
        volatile t_u32* ICER = &register_cortexm3_NVIC.ICER0.All;
        t_u32* BitBand_ICER = cm3_calc_peripheral_BitBandAddress( ICER + RegisterIndex, IRQ_Channel );
        *BitBand_ICER = 1; // set corresponding bit in ICER register

        // disble selected NVIC IRQ channel
        interrupt_cortexm3_enable( IRQ_Channel, 0 );
    }
}
void   interrupt_stm32l1xx_deinit_all_gpio() {

    ttc_memory_set( ( t_u8* ) & register_stm32l1xx_EXTI, 0, sizeof( register_stm32l1xx_EXTI ) );
    t_ttc_interrupt_config_gpio Config_ISR;
    Config_ISR.PhysicalIndex = E_ttc_gpio_pin_a0;

    for ( t_u8 IndexEXTI = 0; IndexEXTI < 32; IndexEXTI++ ) { // deinit all 32 EXTI lines
        interrupt_stm32l1xx_deinit_gpio( &Config_ISR );
        Config_ISR.PhysicalIndex += 4;
    }
}
void   interrupt_stm32l1xx_disable_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

    // clear corresponding bit in Interrupt Mask Register
    volatile t_u32 IMR = * ( ( t_u32* ) &register_stm32l1xx_EXTI.IMR.All );
    IMR &= ~Config_ISRs->LowLevel.Mask;
    register_stm32l1xx_EXTI.IMR.All = IMR;
}
void   interrupt_stm32l1xx_generate_gpio( t_ttc_interrupt_config_gpio* Config_ISRs ) {
#if (TTC_Assert_INTERRUPTS==1)
    Assert_INTERRUPT( !A_is_null( ttc_interrupt_config_gpio ), ttc_assert_origin_auto );  // init interrupt first!
    t_ttc_interrupt_config_gpio* GPIO_ISR_Entry = A( ttc_interrupt_config_gpio, Pin );
    Assert_INTERRUPT( GPIO_ISR_Entry, ttc_assert_origin_auto );  // init interrupt first!
#endif

    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

    // re-set pending bit (required to re-activate if interrupt already occured)
    register_stm32l1xx_EXTI.PR.All = Config_ISRs->LowLevel.Mask;

    // set corresponding bit in Interrupt Mask Register to trigger interrupt
    register_stm32l1xx_EXTI.SWIER.All = Config_ISRs->LowLevel.Mask;
}
t_u8   interrupt_stm32l1xx_gpio_calc_config_index( t_base PhysicalIndex ) {

    e_ttc_gpio_pin GPIO = ( e_ttc_gpio_pin ) PhysicalIndex; // we know that physical index is bit banding address of gpio pin
    t_u8 PinNo = cm3_calc_peripheral_BitNumber( GPIO );

    return PinNo; // stm32l1xx has one interrupt line for each pin over all banks (E_ttc_gpio_pin_a1 == tgpb1 == ...) so only pin # is important
}

// interrupt handlers for GPIO
void GPIO_General_IRQHandler( t_physical_index InterruptLine ) {
    t_ttc_interrupt_config_gpio* Entry = A( ttc_interrupt_config_gpio, InterruptLine );
    Assert_INTERRUPT_Writable( Entry, ttc_assert_origin_auto );  // no entry found for this interrupt: call ttc_interrupt_init() before!
    if ( Entry && Entry->isr )
    { Entry->isr( Entry->PhysicalIndex, Entry->Argument ); }
}
void EXTI0_IRQHandler() {
    *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 0;
    GPIO_General_IRQHandler( 0 );
}
void EXTI1_IRQHandler() {
    *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 1;
    GPIO_General_IRQHandler( 1 );
}
void EXTI2_IRQHandler() {
    *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 2;
    GPIO_General_IRQHandler( 2 );
}
void EXTI3_IRQHandler() {
    *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 3;
    GPIO_General_IRQHandler( 3 );
}
void EXTI4_IRQHandler() {
    *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 4;
    GPIO_General_IRQHandler( 4 );
}
void EXTI9_5_IRQHandler() {

    t_u32 PR = *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR );

    if ( PR & 1 << 5 ) { // EXTI5 interrupt active
        *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 5;
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 5 );
    }
    if ( PR & 1 << 6 ) { // EXTI6 interrupt active
        *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 6;
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 6 );
    }
    if ( PR & 1 << 7 ) { // EXTI7 interrupt active
        *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 7;
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 7 );
    }
    if ( PR & 1 << 8 ) { // EXTI8 interrupt active
        *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 8;
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 8 );
    }
    if ( PR & 1 << 9 ) { // EXTI9 interrupt active
        *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 9;
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 9 );
    }
}
void EXTI15_10_IRQHandler() {

    t_u32 PR = *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR );

    if ( PR & 1 << 10 ) { // EXTI10 interrupt active
        *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 10;
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 10 );
    }
    if ( PR & 1 << 11 ) { // EXTI11 interrupt active
        *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 11;
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 11 );
    }
    if ( PR & 1 << 12 ) { // EXTI12 interrupt active
        *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 12;
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 12 );
    }
    if ( PR & 1 << 13 ) { // EXTI13 interrupt active
        *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 13;
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 13 );
    }
    if ( PR & 1 << 14 ) { // EXTI14 interrupt active
        *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 14;
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 14 );
    }
    if ( PR & 1 << 15 ) { // EXTI15 interrupt active
        *( ( volatile t_u32* ) & register_stm32l1xx_EXTI.PR ) = 1 << 15;
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler( 15 );
    }
}
#endif //EXTENSION_ttc_gpio

//------------------------------USART---------------------------------------//
#ifdef EXTENSION_ttc_usart
e_ttc_interrupt_errorcode interrupt_stm32l1xx_init_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type ) {
    TODO( "Implement initialization of different usart interrupt types!" )
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_INTERRUPT( Config_ISRs->PhysicalIndex < 3, ttc_assert_origin_auto );  // stm32l1xx has only three USARTs

    Config_ISRs->LowLevel.BaseRegister = interrupt_stm32l1xx_BaseRegister_USART[Config_ISRs->PhysicalIndex];

    // make sure all interrupts are disabled for this usart
    interrupt_stm32l1xx_deinit_usart( Config_ISRs );

    // reading from SR and then from DR will reset flags in SR
    volatile t_u32 Dummy = Config_ISRs->LowLevel.BaseRegister->SR.All;
    Dummy = Config_ISRs->LowLevel.BaseRegister->DR.All;
    ( void ) Dummy;

    if ( 1 ) { // configure NVIC
        t_u8 IRQ_Channel = interrupt_stm32l1xx_IRQChannel_USART[Config_ISRs->PhysicalIndex];

        // set interrupt priority of corresponding IRQ channel in NVIC
        interrupt_cortexm3_priority( IRQ_Channel, 0, 3 );

    }

    /* DEPRECATED
    //    //Enable NVIC interrupt channel for USART
    //    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
    //    NVIC_InitTypeDef NVIC_InitStructure;

    //    switch (Config_ISRs->PhysicalIndex)
    //    {
    //        case 0:
    //            NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    //        break;
    //        case 1:
    //            NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
    //        break;
    //        case 2:
    //            NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
    //        break;
    //    }
    //    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    //    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    //    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    //    NVIC_Init(&NVIC_InitStructure);
    */

    return ec_interrupt_OK;
}
void _interrupt_stm32l1xx_deinit_usart( volatile t_register_stm32l1xx_usart* BaseRegister ) {
    Assert_INTERRUPT_Writable( ( void* ) BaseRegister, ttc_assert_origin_auto );

    // disable all interrupt sources of this USART

    t_register_stm32l1xx_usart_cr1  CR1; *( ( t_u32* ) & CR1 ) = *( ( volatile t_u32* ) &BaseRegister->CR1 );
    t_register_stm32l1xx_usart_cr2  CR2; *( ( t_u32* ) & CR2 ) = *( ( volatile t_u32* ) &BaseRegister->CR2 );
    t_register_stm32l1xx_usart_cr3  CR3; *( ( t_u32* ) & CR3 ) = *( ( volatile t_u32* ) &BaseRegister->CR3 );

    // disable all interrupt bits for this usart (will be enabled by interrupt_stm32l1xx_enable_usart() )
    CR1.Bits.PEIE   = 0;
    CR1.Bits.TXEIE  = 0;
    CR1.Bits.TCIE   = 0;
    CR1.Bits.RXNEIE = 0;
    CR1.Bits.IDLEIE = 0;
    CR2.Bits.LBDIE  = 0;
    CR3.Bits.EIE    = 0;
    CR3.Bits.CTSIE  = 0;

    // update registers
    *( ( volatile t_u32* ) &BaseRegister->CR1 ) = *( ( volatile t_u32* ) & CR1 );
    *( ( volatile t_u32* ) &BaseRegister->CR2 ) = *( ( volatile t_u32* ) & CR2 );
    *( ( volatile t_u32* ) &BaseRegister->CR3 ) = *( ( volatile t_u32* ) & CR3 );
}
void interrupt_stm32l1xx_deinit_all_usart() {
    _interrupt_stm32l1xx_deinit_usart( & register_stm32l1xx_USART1 );
    _interrupt_stm32l1xx_deinit_usart( & register_stm32l1xx_USART2 );
    _interrupt_stm32l1xx_deinit_usart( & register_stm32l1xx_USART3 );
    _interrupt_stm32l1xx_deinit_usart( & register_stm32l1xx_UART4 );
    _interrupt_stm32l1xx_deinit_usart( & register_stm32l1xx_UART5 );
}
void interrupt_stm32l1xx_deinit_usart( t_ttc_interrupt_config_usart* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

    Assert_INTERRUPT( Config_ISRs->PhysicalIndex < TTC_USART_AMOUNT, ttc_assert_origin_auto );
    Assert_INTERRUPT_Writable( ( void* ) Config_ISRs->LowLevel.BaseRegister, ttc_assert_origin_auto );

    if ( 1 ) { // disable all interrupt sources of this USART
        _interrupt_stm32l1xx_deinit_usart( Config_ISRs->LowLevel.BaseRegister );

    }
    else {
        /* DEPRECATED: Deinit using Standard Peripheral Library
            switch ( Config_ISRs->PhysicalIndex ) {
                case 0:
                    if ( Config_ISRs->Common.Type == tit_USART_TransmitDataEmpty ) {
                        USART_ITConfig( USART1, USART_IT_TXE, DISABLE );
                        USART_ClearITPendingBit( USART1, USART_IT_TXE );
                    }
                    else if ( Config_ISRs->Common.Type == tit_USART_RxNE ) {
                        USART_ITConfig( USART1, USART_IT_RXNE, DISABLE );
                        USART_ClearFlag( USART1, USART_FLAG_RXNE );
                        USART_ClearITPendingBit( USART1, USART_IT_RXNE );

                    }
                    break;
                case 1:
                    if ( Config_ISRs->Common.Type == tit_USART_TransmitDataEmpty ) {
                        USART_ITConfig( USART2, USART_IT_TXE, DISABLE );
                        USART_ClearITPendingBit( USART2, USART_IT_TXE );
                    }
                    else if ( Config_ISRs->Common.Type == tit_USART_RxNE ) {
                        USART_ITConfig( USART2, USART_IT_RXNE, DISABLE );
                        USART_ClearFlag( USART3, USART_FLAG_RXNE );
                        USART_ClearITPendingBit( USART2, USART_IT_RXNE );
                    }
                    break;
                case 2:
                    if ( Config_ISRs->Common.Type == tit_USART_TransmitDataEmpty ) {
                        USART_ITConfig( USART3, USART_IT_TXE, DISABLE );
                        USART_ClearITPendingBit( USART3, USART_IT_TXE );
                    }
                    else if ( Config_ISRs->Common.Type == tit_USART_RxNE ) {
                        USART_ITConfig( USART3, USART_IT_RXNE, DISABLE );
                        USART_ClearFlag( USART3, USART_FLAG_RXNE );
                        USART_ClearITPendingBit( USART3, USART_IT_RXNE );
                    }
                    break;
            }
            */
    }
}
void interrupt_stm32l1xx_disable_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type ) {

    Assert_INTERRUPT( Config_ISRs->PhysicalIndex < 3, ttc_assert_origin_auto );  // stm32l1xx has only three USARTs

    volatile t_register_stm32l1xx_usart* BaseRegister = Config_ISRs->LowLevel.BaseRegister;
    Assert_INTERRUPT_Writable( ( void* ) BaseRegister, ttc_assert_origin_auto );

    switch ( Type ) { // disable individual USART interrupt source

        case tit_USART_Cts: {
            t_register_stm32l1xx_usart_cr3  CR3; *( ( volatile t_u32* ) & CR3 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR3 ) );
            CR3.Bits.CTSIE  = 0;
            * ( ( volatile t_u32* ) & ( BaseRegister->CR3 ) ) = *( ( volatile t_u32* ) & CR3 );
            break;
        }
        case tit_USART_Idle: {
            t_register_stm32l1xx_usart_cr1  CR1; *( ( volatile t_u32* ) & CR1 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) );
            CR1.Bits.IDLEIE = 0;
            * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) ) = *( ( volatile t_u32* ) & CR1 );
            break;
        }
        case tit_USART_TxComplete: {
            t_register_stm32l1xx_usart_cr1  CR1; *( ( volatile t_u32* ) & CR1 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) );
            CR1.Bits.TCIE   = 0;
            * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) ) = *( ( volatile t_u32* ) & CR1 );
            break;
        }
        case tit_USART_TransmitDataEmpty: {
            t_register_stm32l1xx_usart_cr1  CR1; *( ( volatile t_u32* ) & CR1 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) );
            CR1.Bits.TXEIE  = 0;
            * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) ) = *( ( volatile t_u32* ) & CR1 );
            break;
        }
        case tit_USART_RxNE: {
            t_register_stm32l1xx_usart_cr1  CR1; *( ( volatile t_u32* ) & CR1 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) );
            CR1.Bits.RXNEIE = 0;
            * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) ) = *( ( volatile t_u32* ) & CR1 );
            break;
        }
        case tit_USART_LinBreak: {
            t_register_stm32l1xx_usart_cr2  CR2; *( ( volatile t_u32* ) & CR2 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR2 ) );
            CR2.Bits.LBDIE  = 0;
            * ( ( volatile t_u32* ) & ( BaseRegister->CR2 ) ) = *( ( volatile t_u32* ) & CR2 );
            break;
        }
        case tit_USART_Error: {
            t_register_stm32l1xx_usart_cr1  CR1; *( ( volatile t_u32* ) & CR1 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) );
            t_register_stm32l1xx_usart_cr3  CR3; *( ( volatile t_u32* ) & CR3 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR3 ) );
            CR1.Bits.PEIE   = 0;  // irq on parity error
            CR3.Bits.EIE    = 0;  // irq on general error
            * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) ) = *( ( volatile t_u32* ) & CR1 );
            * ( ( volatile t_u32* ) & ( BaseRegister->CR3 ) ) = *( ( volatile t_u32* ) & CR3 );
            break;
        }
        default: ttc_assert_halt_origin( ec_interrupt_InvalidImplementation ); break; // invalid type given!
    }

    if ( 0 ) {
        /*/ interrupt configuration via Standard Peripheral Library (kept as a comparison)

            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel = IRQ_Channel;

            // Enable and set USARTn Interrupt to the lowest priority
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelPreemptionPriority = 0x0F;
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelSubPriority = 0x0F;
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelCmd = ENABLE;

            // disable interrupt channel only if no other interrupt event for this USART is active
            if ( ( CR1.Bits.PEIE   == 0 ) &&
                    ( CR1.Bits.IDLEIE == 0 ) &&
                    ( CR1.Bits.TCIE   == 0 ) &&
                    ( CR1.Bits.TXEIE  == 0 ) &&
                    ( CR1.Bits.RXNEIE == 0 ) &&
                    ( CR2.Bits.LBDIE  == 0 ) &&
                    ( CR3.Bits.CTSIE  == 0 ) &&
                    ( CR3.Bits.EIE    == 0 )
               )
            { interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelCmd = DISABLE; }
            else
            { interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelCmd = ENABLE; }

            NVIC_Init( &interrupt_stm32l1xx_NVIC_Cfg );
            */
    }
}
void interrupt_stm32l1xx_enable_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL
    t_physical_index PhysicalIndex = Config_ISRs->PhysicalIndex;
    Assert_INTERRUPT( PhysicalIndex < TTC_USART_AMOUNT, ttc_assert_origin_auto );

    volatile t_register_stm32l1xx_usart* BaseRegister = Config_ISRs->LowLevel.BaseRegister;
    Assert_INTERRUPT_Writable( ( void* ) BaseRegister, ttc_assert_origin_auto );

    switch ( Type ) { // enable individual USART interrupt source

        case tit_USART_Cts: {
            t_register_stm32l1xx_usart_cr3  CR3; *( ( volatile t_u32* ) & CR3 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR3 ) );
            CR3.Bits.CTSIE  = 1;
            * ( ( volatile t_u32* ) & ( BaseRegister->CR3 ) ) = *( ( volatile t_u32* ) & CR3 );
            break;
        }
        case tit_USART_Idle: {
            t_register_stm32l1xx_usart_cr1  CR1; *( ( volatile t_u32* ) & CR1 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) );
            CR1.Bits.IDLEIE = 1;
            * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) ) = *( ( volatile t_u32* ) & CR1 );
            break;
        }
        case tit_USART_TxComplete: {
            t_register_stm32l1xx_usart_cr1  CR1; *( ( volatile t_u32* ) & CR1 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) );
            CR1.Bits.TCIE   = 1;
            * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) ) = *( ( volatile t_u32* ) & CR1 );
            break;
        }
        case tit_USART_TransmitDataEmpty: {
            t_register_stm32l1xx_usart_cr1  CR1; *( ( volatile t_u32* ) & CR1 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) );
            CR1.Bits.TXEIE  = 1;
            * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) ) = *( ( volatile t_u32* ) & CR1 );
            break;
        }
        case tit_USART_RxNE: {
            t_register_stm32l1xx_usart_cr1  CR1; *( ( volatile t_u32* ) & CR1 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) );
            CR1.Bits.RXNEIE = 1;
            * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) ) = *( ( volatile t_u32* ) & CR1 );
            break;
        }
        case tit_USART_LinBreak: {
            t_register_stm32l1xx_usart_cr2  CR2; *( ( volatile t_u32* ) & CR2 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR2 ) );
            CR2.Bits.LBDIE  = 1;
            * ( ( volatile t_u32* ) & ( BaseRegister->CR2 ) ) = *( ( volatile t_u32* ) & CR2 );
            break;
        }
        case tit_USART_Error: {
            t_register_stm32l1xx_usart_cr1  CR1; *( ( volatile t_u32* ) & CR1 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) );
            t_register_stm32l1xx_usart_cr3  CR3; *( ( volatile t_u32* ) & CR3 ) = * ( ( volatile t_u32* ) & ( BaseRegister->CR3 ) );
            CR1.Bits.PEIE   = 1;  // irq on parity error
            CR3.Bits.EIE    = 1;  // irq on general error
            * ( ( volatile t_u32* ) & ( BaseRegister->CR1 ) ) = *( ( volatile t_u32* ) & CR1 );
            * ( ( volatile t_u32* ) & ( BaseRegister->CR3 ) ) = *( ( volatile t_u32* ) & CR3 );
            break;
        }
        default: ttc_assert_halt_origin( ec_interrupt_InvalidImplementation ); break; // invalid type given!
    }

    t_u8 IRQ_Channel = interrupt_stm32l1xx_IRQChannel_USART[Config_ISRs->PhysicalIndex];

    // enable selected NVIC IRQ channel
    interrupt_cortexm3_enable( IRQ_Channel, 1 );

    if ( 0 ) { // interrupt configuration via Standard Peripheral Library (kept as a comparison)

        // Enable and set USARTn Interrupt to the lowest priority */
        interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelPreemptionPriority = 0;
        interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelSubPriority = 0;
        interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelCmd = ENABLE;

        USART_TypeDef* USART_Base = NULL;


        switch ( PhysicalIndex ) { // physical index starts at 0

            case 0:
                USART_Base = USART1; interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel = USART1_IRQn; break;
            case 1:
                USART_Base = USART2; interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel = USART2_IRQn; break;
            case 2:
                USART_Base = USART3; interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel = USART3_IRQn; break;
            default: { Assert_INTERRUPT( 0, ttc_assert_origin_auto ); }
        }


        if ( ! 1 ) { // disable interrupt channel only if no other interrupt event for this USART is active
            t_register_stm32l1xx_usart_cr1 CR1; *( ( t_u32* ) & CR1 ) = USART_Base->CR1;
            t_register_stm32l1xx_usart_cr2 CR2; *( ( t_u32* ) & CR2 ) = USART_Base->CR2;
            t_register_stm32l1xx_usart_cr3 CR3; *( ( t_u32* ) & CR3 ) = USART_Base->CR3;

            if ( ( CR1.Bits.PEIE   == 0 ) &&
                    ( CR1.Bits.IDLEIE == 0 ) &&
                    ( CR1.Bits.TCIE   == 0 ) &&
                    ( CR1.Bits.TXEIE  == 0 ) &&
                    ( CR1.Bits.RXNEIE == 0 ) &&
                    ( CR2.Bits.LBDIE  == 0 ) &&
                    ( CR3.Bits.CTSIE  == 0 ) &&
                    ( CR3.Bits.EIE    == 0 )
               )
            { interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelCmd = DISABLE; }
        }
        NVIC_Init( &interrupt_stm32l1xx_NVIC_Cfg );



        //    switch (PhysicalIndex)
        //    {
        //        case 0:
        //            if(Config_ISRs->Common.Type == tit_USART_TransmitDataEmpty)
        //            {
        //                USART_ClearITPendingBit(USART1, USART_IT_TXE);
        //                USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
        //            }
        //            else if(Config_ISRs->Common.Type == tit_USART_RxNE)
        //            {
        //                USART_ClearFlag(USART1,USART_FLAG_RXNE);
        //                USART_ClearITPendingBit(USART1, USART_IT_RXNE);
        //                USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
        //            }
        //            break;
        //        case 1:
        //            if(Config_ISRs->Common.Type == tit_USART_TransmitDataEmpty)
        //            {
        //                USART_ClearITPendingBit(USART2, USART_IT_TXE);
        //                USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
        //            }
        //            else if(Config_ISRs->Common.Type == tit_USART_RxNE)
        //            {
        //                USART_ClearFlag(USART2,USART_FLAG_RXNE);
        //                USART_ClearITPendingBit(USART2, USART_IT_RXNE);
        //                USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
        //            }
        //            break;
        //        case 2:
        //            if(Config_ISRs->Common.Type == tit_USART_TransmitDataEmpty)
        //            {
        //                USART_ClearITPendingBit(USART3, USART_IT_TXE);
        //                USART_ITConfig(USART3, USART_IT_TXE, ENABLE);
        //            }
        //            else if(Config_ISRs->Common.Type == tit_USART_RxNE)
        //            {
        //                USART_ClearFlag(USART3,USART_FLAG_RXNE);
        //                USART_ClearITPendingBit(USART3, USART_IT_RXNE);
        //                USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
        //            }
        //            break;
        //    }
    }
}
void USART_General_IRQHandler( t_physical_index PhysicalIndex, volatile t_register_stm32l1xx_usart* USART ) {

    t_register_stm32l1xx_usart_sr StatusRegister;
    StatusRegister.All = USART->SR.All; // reading from SR and then from DR will reset flags in SR

    t_ttc_interrupt_config_usart* ISRs = A( ttc_interrupt_config_usart, PhysicalIndex );
    Assert_INTERRUPT( ISRs != NULL, ttc_assert_origin_auto );  // should never happen: the usart indexed by PhysicalIndex has not been initialized!

    if ( StatusRegister.Bits.RXNE && ISRs->isr_ReceiveDataNotEmpty ) {
        ISRs->isr_ReceiveDataNotEmpty( PhysicalIndex, ISRs->Argument_ReceiveDataNotEmpty );
    }
    if ( StatusRegister.Bits.TXE  && ISRs->isr_TransmitDataEmpty ) {
        ISRs->isr_TransmitDataEmpty( PhysicalIndex, ISRs->Argument_TransmitDataEmpty );
    }
    if ( StatusRegister.Bits.CTS  && ISRs->isr_ClearToSend ) {
        ISRs->isr_ClearToSend( PhysicalIndex, ISRs->Argument_ClearToSend );
    }
    if ( StatusRegister.Bits.TC   && ISRs->isr_TransmissionComplete ) {
        ISRs->isr_TransmissionComplete( PhysicalIndex, ISRs->Argument_TransmissionComplete );
    }
    if ( StatusRegister.Bits.IDLE && ISRs->isr_IdleLine ) {
        ISRs->isr_IdleLine( PhysicalIndex, ISRs->Argument_IdleLine );
    }
    if ( StatusRegister.Bits.LBD  && ISRs->isr_LinBreakDetected ) {
        ISRs->isr_LinBreakDetected( PhysicalIndex, ISRs->Argument_LinBreakDetected );
    }

    if ( ( ( *( ( t_u8* ) &StatusRegister ) ) & 0xf ) && // error occured: gather data + call error handler
            ( ISRs->isr_Error != NULL )                  // error-handler has been defined
       ) {
        t_ttc_interrupt_usart_errors Error; *( ( t_u8* ) &Error ) = 0;
        if ( StatusRegister.Bits.FE )   { Error.Framing = 1; }
        //if (StatusRegister.Bits.NE)   Error.Noise   = 1;
        if ( StatusRegister.Bits.ORE )  { Error.Overrun = 1; }
        if ( StatusRegister.Bits.PE )   { Error.Parity  = 1; }

        ISRs->isr_Error( PhysicalIndex, Error, ISRs->Argument_Error );
    }
}
void USART1_IRQHandler() {
    USART_General_IRQHandler( 0, ( t_register_stm32l1xx_usart* ) USART1 );
    USART1->SR = 0; // clear all interrupt flags
}
void USART2_IRQHandler() {
    USART_General_IRQHandler( 1, ( t_register_stm32l1xx_usart* ) USART2 );
    USART2->SR = 0; // clear all interrupt flags
}
void USART3_IRQHandler() {
    USART_General_IRQHandler( 2, ( t_register_stm32l1xx_usart* ) USART3 );
    USART3->SR = 0; // clear all interrupt flags
}
#endif


//------------------------------RTC (REAL TIME CLOCK)---------------------------------------//

#ifdef EXTENSION_ttc_rtc
void interrupt_stm32l1xx_enable_rtc( t_ttc_interrupt_config_rtc* Config_ISRs ) {

    Assert_INTERRUPT( Config_ISRs->PhysicalIndex < TTC_RTC_AMOUNT, ttc_assert_origin_auto );

    if ( Config_ISRs->Common.Type == tit_RTC_AlarmA ) {

        EXTI_ClearITPendingBit( EXTI_Line17 );

        RTC_ITConfig( RTC_IT_ALRA, DISABLE );


        register_stm32l1xx_RTC.WPR.All = 0xCA;
        register_stm32l1xx_RTC.WPR.All = 0x53;

        /* Clear RTC AlarmA Flags */
        RTC_ClearITPendingBit( RTC_IT_ALRA );

        /* Clear RTC Alarm A flag */
        RTC_ClearFlag( RTC_FLAG_ALRAF );

        register_stm32l1xx_RTC.WPR.All = 0xFF;

        /* Enable AlarmA interrupt */
        RTC_ITConfig( RTC_IT_ALRA, ENABLE );
    }
    else if ( Config_ISRs->Common.Type == tit_RTC_AlarmB ) {
        EXTI_ClearITPendingBit( EXTI_Line17 );

        RTC_ITConfig( RTC_IT_ALRB, DISABLE );


        register_stm32l1xx_RTC.WPR.All = 0xCA;
        register_stm32l1xx_RTC.WPR.All = 0x53;

        /* Clear RTC AlarmB Flags */
        RTC_ClearITPendingBit( RTC_IT_ALRB );

        /* Clear RTC AlarmB flag */
        RTC_ClearFlag( RTC_FLAG_ALRBF );

        register_stm32l1xx_RTC.WPR.All = 0xFF;

        /* Enable AlarmB interrupt */
        RTC_ITConfig( RTC_IT_ALRB, ENABLE );
    }
    else if ( Config_ISRs->Common.Type == tit_RTC_Tamper ) {

        EXTI_ClearITPendingBit( EXTI_Line19 );

        RTC_ITConfig( RTC_IT_TAMP, DISABLE );


        register_stm32l1xx_RTC.WPR.All = 0xCA;
        register_stm32l1xx_RTC.WPR.All = 0x53;

        /* Clear Tamper 1 pin interrupt pending bit */
        RTC_ClearITPendingBit( RTC_IT_TAMP1 );
        RTC_ClearFlag( RTC_FLAG_TAMP1F );

        register_stm32l1xx_RTC.WPR.All = 0xFF;

        /* Enable the Tamper interrupt */
        RTC_ITConfig( RTC_IT_TAMP, ENABLE );
    }
    else if ( Config_ISRs->Common.Type == tit_RTC_WakeUP ) {

        EXTI_ClearITPendingBit( EXTI_Line20 );

        /* Disable WakeUp interrupt */
        RTC_ITConfig( RTC_IT_WUT, DISABLE );


        register_stm32l1xx_RTC.WPR.All = 0xCA;
        register_stm32l1xx_RTC.WPR.All = 0x53;

        /* Clear RTC WakeUP Flags */
        RTC_ClearITPendingBit( RTC_IT_WUT );
        RTC_ClearFlag( RTC_FLAG_WUTF );

        register_stm32l1xx_RTC.WPR.All = 0xFF;

        /* Enable WakeUp interrupt */
        RTC_ITConfig( RTC_IT_WUT, ENABLE );
    }
    else if ( Config_ISRs->Common.Type == tit_RTC_TimeStamp ) {

        EXTI_ClearITPendingBit( EXTI_Line19 );

        RTC_ITConfig( RTC_IT_TS, DISABLE );


        register_stm32l1xx_RTC.WPR.All = 0xCA;
        register_stm32l1xx_RTC.WPR.All = 0x53;

        /* Clear RTC TimeStamp Flags */
        RTC_ClearITPendingBit( RTC_IT_TS );
        RTC_ClearFlag( RTC_FLAG_TSF );
        RTC_ClearFlag( RTC_FLAG_TSOVF );

        register_stm32l1xx_RTC.WPR.All = 0xFF;

        /* Enable TimeStamp interrupt */
        RTC_ITConfig( RTC_IT_TS, ENABLE );
    }
    else if ( Config_ISRs->Common.Type == tit_RTC_Pin_line_wakeup ) {


    }
    else {

        Assert_INTERRUPT( 0, ttc_assert_origin_auto );
    }

}
e_ttc_interrupt_errorcode interrupt_stm32l1xx_alarm_rtc() {

    //Enable NVIC interrupt channel for RTC
    NVIC_InitTypeDef NVIC_InitStructure;
    EXTI_InitTypeDef EXTI_InitStructure;


    /* Alarm Interrupt Configuration */
    /* EXTI configuration */
    EXTI_ClearITPendingBit( EXTI_Line17 );
    EXTI_InitStructure.EXTI_Line = EXTI_Line17;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init( &EXTI_InitStructure );

    /* Enable the RTC Alarm Interrupt */
    NVIC_PriorityGroupConfig( NVIC_PriorityGroup_2 );
    NVIC_InitStructure.NVIC_IRQChannel = RTC_Alarm_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( &NVIC_InitStructure );

    return ( ec_interrupt_OK );

}
e_ttc_interrupt_errorcode interrupt_stm32l1xx_wakeup_rtc() {

    //Enable NVIC interrupt channel for RTC
    NVIC_InitTypeDef NVIC_InitStructure;
    EXTI_InitTypeDef EXTI_InitStructure;


    /* Alarm Interrupt Configuration */
    /* EXTI configuration */
    EXTI_ClearITPendingBit( EXTI_Line20 );
    EXTI_InitStructure.EXTI_Line = EXTI_Line20;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Event;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init( &EXTI_InitStructure );

    /* Enable the RTC Alarm Interrupt */

    NVIC_PriorityGroupConfig( NVIC_PriorityGroup_1 );
    NVIC_InitStructure.NVIC_IRQChannel = RTC_WKUP_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( &NVIC_InitStructure );

    return ( ec_interrupt_OK );

}
e_ttc_interrupt_errorcode interrupt_stm32l1xx_tamper_rtc() {

    NVIC_InitTypeDef NVIC_InitStructure;
    EXTI_InitTypeDef  EXTI_InitStructure;

    /* Enable The external line19 interrupt */
    EXTI_ClearITPendingBit( EXTI_Line19 );
    EXTI_InitStructure.EXTI_Line = EXTI_Line19;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init( &EXTI_InitStructure );

    /* Enable TAMPER IRQChannel */
    NVIC_PriorityGroupConfig( NVIC_PriorityGroup_1 );
    NVIC_InitStructure.NVIC_IRQChannel = TAMPER_STAMP_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( &NVIC_InitStructure );


    return ( e_ttc_interrupt_errorcode )0;


}
e_ttc_interrupt_errorcode interrupt_stm32l1xx_timestamp_rtc() {

    return ec_interrupt_OK;
}
e_ttc_interrupt_errorcode interrupt_stm32l1xx_pin_line_wakeup_rtc() {

    // Enable GPIOA's AHB interface clock
    register_stm32l1xx_RCC.AHBENR.Bits.GPIOA_EN = 1;
    //X sysclock_stm32l1xx_RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA, ENABLE );

    // Enable SYSCFG's APB interface clock
    register_stm32l1xx_RCC.APB2ENR.Bits.SYSCFGEN = 1;
    //X sysclock_stm32l1xx_RCC_APB2PeriphClockCmd( RCC_APB2Periph_SYSCFG, ENABLE );

    //    GPIO_InitTypeDef GPIO_InitStructure;

    //    Configure PA0 pin in input mode
    //    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    //    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    //    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    //    GPIO_Init(GPIOA, &GPIO_InitStructure);
    ttc_gpio_init( E_ttc_gpio_pin_a0, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_max );

    /* Connect EXTI Line0 to PA0 pin */
    SYSCFG_EXTILineConfig( EXTI_PortSourceGPIOA, EXTI_PinSource0 );

    EXTI_InitTypeDef EXTI_InitStructure;

    /* Configure EXTI line0 */
    EXTI_InitStructure.EXTI_Line = EXTI_Line0;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init( &EXTI_InitStructure );

    return ec_interrupt_OK;
}
e_ttc_interrupt_errorcode interrupt_stm32l1xx_init_rtc( t_ttc_interrupt_config_rtc* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

    Assert_INTERRUPT( Config_ISRs->PhysicalIndex < TTC_RTC_AMOUNT, ttc_assert_origin_auto );

    /* Configure Alarm intrruption */
    e_ttc_interrupt_errorcode Error = -1;

    if ( Config_ISRs->Common.Type == tit_RTC_AlarmA ) {

        Error = interrupt_stm32l1xx_alarm_rtc();

    }
    else if ( Config_ISRs->Common.Type == tit_RTC_AlarmB ) {

        Error = interrupt_stm32l1xx_alarm_rtc();


    }
    else if ( Config_ISRs->Common.Type == tit_RTC_Tamper ) {


        Error = interrupt_stm32l1xx_tamper_rtc();

    }
    else if ( Config_ISRs->Common.Type == tit_RTC_WakeUP ) {

        Error = interrupt_stm32l1xx_wakeup_rtc();


    }
    else if ( Config_ISRs->Common.Type == tit_RTC_TimeStamp ) {

        Error = interrupt_stm32l1xx_timestamp_rtc();

    }
    else if ( Config_ISRs->Common.Type == tit_RTC_Pin_line_wakeup ) {
        Error = interrupt_stm32l1xx_pin_line_wakeup_rtc();

    }
    else {

        Assert_INTERRUPT( 0, ttc_assert_origin_auto );

    }

    return Error;
}
void interrupt_stm32l1xx_deinit_rtc( t_ttc_interrupt_config_rtc* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL



}
void interrupt_stm32l1xx_disable_rtc( t_ttc_interrupt_config_rtc* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

}
void RTC_General_IRQHandler() {


}
void TAMPER_STAMP_IRQHandler() {
    RTC_General_IRQHandler();

    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void RTC_WKUP_IRQHandler() {

    RTC_General_IRQHandler();


    if ( RTC_GetITStatus( RTC_IT_WUT ) != RESET ) {

        EXTI_ClearITPendingBit( EXTI_Line20 ); //OK

        /* Disable WakeUp interrupt */
        RTC_ITConfig( RTC_IT_WUT, DISABLE );

        register_stm32l1xx_RTC.WPR.All = 0xCA;
        register_stm32l1xx_RTC.WPR.All = 0x53;

        /* Clear RTC WakeUP Flags */
        RTC_ClearITPendingBit( RTC_IT_WUT );
        RTC_ClearFlag( RTC_FLAG_WUTF );

        register_stm32l1xx_RTC.WPR.All = 0xFF;

        /* Enable WakeUp interrupt */
        RTC_ITConfig( RTC_IT_WUT, ENABLE );
    }
}
void RTC_Alarm_IRQHandler() {

    RTC_General_IRQHandler();

    if ( RTC_GetITStatus( RTC_IT_ALRA ) != RESET ) {
        EXTI_ClearITPendingBit( EXTI_Line17 );

        RTC_ITConfig( RTC_IT_ALRA, DISABLE );

        register_stm32l1xx_RTC.WPR.All = 0xCA;
        register_stm32l1xx_RTC.WPR.All = 0x53;

        /* Clear RTC AlarmA Flags */
        RTC_ClearITPendingBit( RTC_IT_ALRA );
        /* Clear RTC Alarm A flag */
        RTC_ClearFlag( RTC_FLAG_ALRAF );

        register_stm32l1xx_RTC.WPR.All = 0xFF;

        /* Enable AlarmA interrupt */
        RTC_ITConfig( RTC_IT_ALRA, ENABLE );

    }
    else if ( RTC_GetITStatus( RTC_IT_ALRB ) != RESET ) {

        RTC_ITConfig( RTC_IT_ALRB, DISABLE );

        register_stm32l1xx_RTC.WPR.All = 0xCA;
        register_stm32l1xx_RTC.WPR.All = 0x53;

        /* Clear RTC AlarmA Flags */
        RTC_ClearITPendingBit( RTC_IT_ALRB );
        /* Clear RTC Alarm A flag */
        RTC_ClearFlag( RTC_FLAG_ALRBF );

        register_stm32l1xx_RTC.WPR.All = 0xFF;

        /* Enable AlarmA interrupt */
        RTC_ITConfig( RTC_IT_ALRB, ENABLE );
    }

}
#endif


//------------------------------I2C---------------------------------------//
#ifdef EXTENSION_ttc_i2c
e_ttc_interrupt_errorcode interrupt_stm32l1xx_init_i2c( t_ttc_interrupt_config_i2c* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL
    t_register_stm32l1xx_i2c* I2C_Base = NULL;

    switch ( Config_ISRs->PhysicalIndex ) {
        case 1: I2C_Base = ( t_register_stm32l1xx_i2c* )&register_stm32l1xx_I2C1;
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel = I2C1_EV_IRQn | I2C1_ER_IRQn;
            break;
        case 2: I2C_Base = ( t_register_stm32l1xx_i2c* )&register_stm32l1xx_I2C2;
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel = I2C2_EV_IRQn | I2C2_ER_IRQn;
            break;
        default: Assert_INTERRUPT( 0, ttc_assert_origin_auto );
            break;
    }
    I2C_Base->CR2.Bits.ITBUF_EN = 1;
    Config_ISRs->LowLevel.BaseRegister = I2C_Base;


    /* Enable and set I2Cx Interrupt */
    interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelPreemptionPriority = 0x0F;
    interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelSubPriority = 0x0F;
    interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( &interrupt_stm32l1xx_NVIC_Cfg );

    return ( e_ttc_interrupt_errorcode ) 0;
}

void interrupt_stm32l1xx_deinit_i2c( t_ttc_interrupt_config_i2c* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL
}
void interrupt_stm32l1xx_enable_i2c( t_ttc_interrupt_config_i2c* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

    /* Enable and set USARTn Interrupt to the lowest priority */
    interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelPreemptionPriority = 0x0F;
    interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelSubPriority = 0x0F;
    interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelCmd = ENABLE;

    t_register_stm32l1xx_i2c* I2C_Base = Config_ISRs->LowLevel.BaseRegister;

    I2C_Base->CR2.Bits.ITBUF_EN = 1;
    switch ( Config_ISRs->PhysicalIndex ) {
        case 1: //X I2C_Base = (t_register_stm32l1xx_i2c*)&register_stm32l1xx_I2C1;
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel = I2C1_EV_IRQn | I2C1_ER_IRQn;
            break;
        case 2: //X I2C_Base = (t_register_stm32l1xx_i2c*)&register_stm32l1xx_I2C2;
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel = I2C2_EV_IRQn | I2C2_ER_IRQn;
            break;
        default: Assert_INTERRUPT( 0, ttc_assert_origin_auto );
            break;
    }

    switch ( Config_ISRs->Common.Type ) { // select trigger according to Type
        case tit_I2C_StartBit:
            Assert_INTERRUPT( A( ttc_interrupt_config_i2c, Config_ISRs->PhysicalIndex )->isr_StartBitSent != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_I2C_AddrSent:
            Assert_INTERRUPT( A( ttc_interrupt_config_i2c, Config_ISRs->PhysicalIndex )->isr_AddressSent != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_I2C_10_HDR:
            Assert_INTERRUPT( A( ttc_interrupt_config_i2c, Config_ISRs->PhysicalIndex )->isr_10BitHeader != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_I2C_Stop:
            Assert_INTERRUPT( A( ttc_interrupt_config_i2c, Config_ISRs->PhysicalIndex )->isr_StopReceived != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_I2C_DB_TF:
            Assert_INTERRUPT( A( ttc_interrupt_config_i2c, Config_ISRs->PhysicalIndex )->isr_DataByteTransferFinished != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_I2C_RxNE:
            Assert_INTERRUPT( A( ttc_interrupt_config_i2c, Config_ISRs->PhysicalIndex )->isr_ReceiveBufferNotEmpty != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_I2C_TxE:
            Assert_INTERRUPT( A( ttc_interrupt_config_i2c, Config_ISRs->PhysicalIndex )->isr_TransmitBufferEmpty != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_I2C_BusError:
            Assert_INTERRUPT( A( ttc_interrupt_config_i2c, Config_ISRs->PhysicalIndex )->isr_BusError != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_I2C_ArbitrationLoss:
            Assert_INTERRUPT( A( ttc_interrupt_config_i2c, Config_ISRs->PhysicalIndex )->isr_ArbitrationLoss != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_I2C_AckFail:
            Assert_INTERRUPT( A( ttc_interrupt_config_i2c, Config_ISRs->PhysicalIndex )->isr_AckFail != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_I2C_Overrun:
            Assert_INTERRUPT( A( ttc_interrupt_config_i2c, Config_ISRs->PhysicalIndex )->isr_Overrun != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_I2C_PECError:
            Assert_INTERRUPT( A( ttc_interrupt_config_i2c, Config_ISRs->PhysicalIndex )->isr_PECError != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_I2C_Timeout:
            Assert_INTERRUPT( A( ttc_interrupt_config_i2c, Config_ISRs->PhysicalIndex )->isr_Timeout != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        case tit_I2C_SMBUSAlert:
            Assert_INTERRUPT( A( ttc_interrupt_config_i2c, Config_ISRs->PhysicalIndex )->isr_SMBusAlert != NULL, ttc_assert_origin_auto );  // no isr configured for this interrupt!
            break;
        default: Assert_INTERRUPT( 0, ttc_assert_origin_auto );
    }


    NVIC_Init( &interrupt_stm32l1xx_NVIC_Cfg );
}
void interrupt_stm32l1xx_disable_i2c( t_ttc_interrupt_config_i2c* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL
}
#endif

//------------------------------NOT IMPLEMENTED----------------------------//

#ifdef EXTENSION_ttc_spi
e_ttc_interrupt_errorcode interrupt_stm32l1xx_init_spi( t_ttc_interrupt_config_spi* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

    ttc_assert_halt_origin( ttc_assert_origin_auto ); // ToDo: implement!

    //#warning missing implementation for interrupt_stm32l1xx_init_spi()!
    return ( e_ttc_interrupt_errorcode ) 0;
}
void interrupt_stm32l1xx_deinit_spi( t_ttc_interrupt_config_spi* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

    ttc_assert_halt_origin( ttc_assert_origin_auto ); // ToDo: implement!
    //#warning missing implementation for interrupt_stm32l1xx_deinit_spi()!


}
void interrupt_stm32l1xx_enable_spi( t_ttc_interrupt_config_spi* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

    ttc_assert_halt_origin( ttc_assert_origin_auto ); // ToDo: implement!
    //#warning missing implementation for interrupt_stm32l1xx_enable_spi()!


}
void interrupt_stm32l1xx_disable_spi( t_ttc_interrupt_config_spi* Config_ISRs ) {
    Assert_INTERRUPT_Writable( Config_ISRs, ttc_assert_origin_auto );  // pointers must not be NULL

    ttc_assert_halt_origin( ttc_assert_origin_auto ); // ToDo: implement!
    //#warning missing implementation for interrupt_stm32l1xx_disable_spi()!


}
#endif
#ifdef EXTENSION_ttc_timer


e_ttc_interrupt_errorcode interrupt_stm32l1xx_init_timer( t_ttc_interrupt_configimer_t* Config_ISRs ) {

    t_register_stm32l1xx_timer* TIMER_Base;
    ( void )TIMER_Base;     //Avoid the warning


    NVIC_PriorityGroupConfig( NVIC_PriorityGroup_1 );

    switch ( Config_ISRs->PhysicalIndex ) { // physical index starts is the logical index

        case 1:
            TIMER_Base = ( t_register_stm32l1xx_timer* )TIM2;
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel = TIM2_IRQn;
            break;
        case 2: TIMER_Base = ( t_register_stm32l1xx_timer* )TIM3;
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel = TIM3_IRQn;
            break;
        case 3: TIMER_Base = ( t_register_stm32l1xx_timer* )TIM4;
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM4_IRQn;
            break;
        case 5: TIMER_Base = ( t_register_stm32l1xx_timer* )TIM6;
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM6_IRQn;
            break;
        case 6: TIMER_Base = ( t_register_stm32l1xx_timer* )TIM7;
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM7_IRQn;
            break;
        case 8: TIMER_Base = ( t_register_stm32l1xx_timer* )TIM9;
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM9_IRQn;
            break;
        case 9: TIMER_Base = ( t_register_stm32l1xx_timer* )TIM10;
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM10_IRQn;
            break;
        case 10: TIMER_Base = ( t_register_stm32l1xx_timer* )TIM11;
            interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel =  TIM11_IRQn;
            break;

        default: { Assert_INTERRUPT( 0, ttc_assert_origin_auto ); }
    }

    /* Enable and set TIMx Interrupt to the lowest priority */
    interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelPreemptionPriority = 0;
    interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelSubPriority = 0;
    interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init( &interrupt_stm32l1xx_NVIC_Cfg );

    NVIC_EnableIRQ( interrupt_stm32l1xx_NVIC_Cfg.NVIC_IRQChannel );

    return ec_interrupt_OK;

}

void interrupt_stm32l1xx_enable_timer( t_ttc_interrupt_configimer_t* Config_ISRs ) {
    t_register_stm32l1xx_timer* TIMER_Base = NULL;

    switch ( Config_ISRs->PhysicalIndex ) {

        case 1:

            TIMER_Base = ( t_register_stm32l1xx_timer* ) TIM2;
            register_stm32l1xx_RCC.APB1ENR.Bits.TIM2_EN = 1;
            //DEPRECATED sysclock_stm32l1xx_RCC_APB1PeriphClockCmd( 0x00000001, ENABLE );
            break;

        case 2:

            TIMER_Base = ( t_register_stm32l1xx_timer* ) TIM3;
            register_stm32l1xx_RCC.APB1ENR.Bits.TIM3_EN = 1;
            //DEPRECATED sysclock_stm32l1xx_RCC_APB1PeriphClockCmd( 0x00000002, ENABLE );
            break;

        case 3:

            TIMER_Base = ( t_register_stm32l1xx_timer* ) TIM4;
            register_stm32l1xx_RCC.APB1ENR.Bits.TIM4_EN = 1;
            //DEPRECATED sysclock_stm32l1xx_RCC_APB1PeriphClockCmd( 0x00000004, ENABLE );
            break;

        case 5:
            TIMER_Base = ( t_register_stm32l1xx_timer* ) TIM6;
            register_stm32l1xx_RCC.APB1ENR.Bits.TIM6_EN = 1;
            //DEPRECATED sysclock_stm32l1xx_RCC_APB1PeriphClockCmd( 0x00000010, ENABLE );

            break;
        case 6:

            TIMER_Base = ( t_register_stm32l1xx_timer* ) TIM7;
            register_stm32l1xx_RCC.APB1ENR.Bits.TIM7_EN = 1;
        //DEPRECATED sysclock_stm32l1xx_RCC_APB1PeriphClockCmd( 0x00000020, ENABLE );

        case 8:

            TIMER_Base = ( t_register_stm32l1xx_timer* ) TIM9;
            register_stm32l1xx_RCC.APB2ENR.Bits.TIM9EN = 1;
            //DEPRECATED sysclock_stm32l1xx_RCC_APB2PeriphClockCmd( 0x00000004, ENABLE );
            break;

        case 9:
            TIMER_Base = ( t_register_stm32l1xx_timer* ) TIM10;
            register_stm32l1xx_RCC.APB2ENR.Bits.TIM10EN = 1;
            //DEPRECATED sysclock_stm32l1xx_RCC_APB2PeriphClockCmd( 0x00000008, ENABLE );

            break;
        case 10:

            TIMER_Base = ( t_register_stm32l1xx_timer* ) TIM11;
            register_stm32l1xx_RCC.APB2ENR.Bits.TIM11EN = 1;
            //DEPRECATED sysclock_stm32l1xx_RCC_APB2PeriphClockCmd( 0x00000010, ENABLE );
            break;
    }

    switch ( Config_ISRs->Common.Type ) { // select trigger according to Type
        case tit_TIMER_Updating:

            /* Disable the Interrupt sources */
            TIMER_Base->DIER.All &= ( t_u16 )~0x0001;

            /* Clear Interrupt Flag */
            TIMER_Base->SR.All = ( t_u16 )~0x0001;

            /* Clear Update Flag */
            TIMER_Base->SR.All = ( t_u16 )~0x0001;

            /* Enable Interrupt */
            TIMER_Base->DIER.All |= ( ( t_u16 )0x0001 );

            break;

        case tit_TIMER_CC1:

            /* Disable the Interrupt sources */
            TIMER_Base->DIER.All &= ( t_u16 )~0x0002;

            /* Clear Interrupt Flag */
            TIMER_Base->SR.All = ( t_u16 )~0x0002;

            /* Clear Update Flag */
            TIMER_Base->SR.All = ( t_u16 )~0x0002;

            /* Enable Interrupt */
            TIMER_Base->DIER.All |= ( ( t_u16 )0x0002 );
            break;

        case tit_TIMER_CC2:
            /* Disable the Interrupt sources */
            TIMER_Base->DIER.All &= ( t_u16 )~0x0004;

            /* Clear Interrupt Flag */
            TIMER_Base->SR.All = ( t_u16 )~0x0004;

            /* Clear Update Flag */
            TIMER_Base->SR.All = ( t_u16 )~0x0004;

            /* Enable Interrupt */
            TIMER_Base->DIER.All |= ( ( t_u16 )0x0004 );
            break;

        case tit_TIMER_CC3:
            /* Disable the Interrupt sources */
            TIMER_Base->DIER.All &= ( t_u16 )~0x0008;

            /* Clear Interrupt Flag */
            TIMER_Base->SR.All = ( t_u16 )~0x0008;

            /* Clear Update Flag */
            TIMER_Base->SR.All = ( t_u16 )~0x0008;

            /* Enable Interrupt */
            TIMER_Base->DIER.All |= ( ( t_u16 )0x0008 );
            break;

        case tit_TIMER_CC4:
            /* Disable the Interrupt sources */
            TIMER_Base->DIER.All &= ( t_u16 )~0x0010;

            /* Clear Interrupt Flag */
            TIMER_Base->SR.All = ( t_u16 )~0x0010;

            /* Clear Update Flag */
            TIMER_Base->SR.All = ( t_u16 )~0x0010;

            /* Enable Interrupt */
            TIMER_Base->DIER.All |= ( ( t_u16 )0x0010 );
            break;

        case tit_TIMER_Triggering:
            /* Disable the Interrupt sources */
            TIMER_Base->DIER.All &= ( t_u16 )~0x0040;

            /* Clear Interrupt Flag */
            TIMER_Base->SR.All = ( t_u16 )~0x0040;

            /* Clear Update Flag */
            TIMER_Base->SR.All = ( t_u16 )~0x0040;

            /* Enable Interrupt */
            TIMER_Base->DIER.All |= ( ( t_u16 )0x0040 );
            break;
        default:
            Assert_INTERRUPT( 0, ttc_assert_origin_auto );
            break;
    }
}


void TIMER_General_IRQHandler( t_physical_index PhysicalIndex, volatile t_register_stm32l1xx_timer* TIMER ) {

    //ttc_gpio_set(TTC_LED2);

    Assert_INTERRUPT( TIMER != NULL, ttc_assert_origin_auto );
    t_register_stm32l1xx_timx_sr StatusRegister;
    StatusRegister.All = ( t_u16 )TIMER->SR.All; // reading from SR and then from DR will reset flags in SR
    t_ttc_interrupt_configimer_t* ISRs = A( t_ttc_interrupt_configimer, PhysicalIndex );
    Assert_INTERRUPT( ISRs != NULL, ttc_assert_origin_auto );  // should never happen: the usart indexed by PhysicalIndex has not been initialized!

    if ( StatusRegister.Bits.UIF )
    { ISRs->isr_Update( PhysicalIndex, ISRs->Argument_Update ); }
    /*
    if(StatusRegister.Bytes.CC1IF)
       ISRs->isr_CC1IF(PhysicalIndex, ISRs->Argument_CC1IF);
    if(StatusRegister.Bytes.CC2IF)
       ISRs->isr_CC2IF(PhysicalIndex, ISRs->Argument_CC2IF);
    if(StatusRegister.Bytes.CC3IF)
       ISRs->isr_CC3IF(PhysicalIndex, ISRs->Argument_CC3IF);
    if(StatusRegister.Bytes.CC4IF)
       ISRs->isr_CC4IF(PhysicalIndex, ISRs->Argument_CC4IF);
    if(StatusRegister.Bytes.TIF)
       ISRs->isr_Trigger(PhysicalIndex, ISRs->Argument_Trigger);
    if(StatusRegister.Bytes.BIF)
       ISRs->isr_Break(PhysicalIndex, ISRs->Argument_Break);
    if(StatusRegister.Bytes.CC1OF)
       ISRs->isr_CC10F(PhysicalIndex, ISRs->Argument_CC10F);
    if(StatusRegister.Bytes.CC2OF)
       ISRs->isr_CC20F(PhysicalIndex, ISRs->Argument_CC20F);
    if(StatusRegister.Bytes.CC3OF)
       ISRs->isr_CC30F(PhysicalIndex, ISRs->Argument_CC30F);
    if(StatusRegister.Bytes.CC4OF)
       ISRs->isr_CC40F(PhysicalIndex, ISRs->Argument_CC40F);
    */
}
void TIM2_IRQHandler() {
    TIMER_General_IRQHandler( 1, ( t_register_stm32l1xx_timer* )TIM2 );
    register_stm32l1xx_TIMER2.SR.All &= ~0x01; // clear all interrupt flags

}
void TIM3_IRQHandler() {
    TIMER_General_IRQHandler( 2, ( t_register_stm32l1xx_timer* )TIM3 );
    register_stm32l1xx_TIMER3.SR.All &= ~0x01; // clear all interrupt flags
}
void TIM4_IRQHandler() {
    TIMER_General_IRQHandler( 3, ( t_register_stm32l1xx_timer* )TIM4 );
    register_stm32l1xx_TIMER4.SR.All &= ~0x01; // clear all interrupt flags

}
void TIM6_IRQHandler() {
    TIMER_General_IRQHandler( 5, ( t_register_stm32l1xx_timer* )TIM6 );
    register_stm32l1xx_TIMER6.SR.All &= ~0x01; // clear all interrupt flags

}
void TIM7_IRQHandler() {
    TIMER_General_IRQHandler( 6, ( t_register_stm32l1xx_timer* )TIM7 );
    register_stm32l1xx_TIMER7.SR.All &= ~0x01; // clear all interrupt flags

}
void TIM9_IRQHandler() {
    TIMER_General_IRQHandler( 8, ( t_register_stm32l1xx_timer* ) TIM9 );
    register_stm32l1xx_TIMER9.SR.All = 0; // clear all interrupt flags
}
void TIM10_IRQHandler() {
    TIMER_General_IRQHandler( 9, ( t_register_stm32l1xx_timer* ) TIM10 );
    register_stm32l1xx_TIMER10.SR.All = 0; // clear all interrupt flags

}

void TIM11_IRQHandler() {
    TIMER_General_IRQHandler( 10, ( t_register_stm32l1xx_timer* ) TIM11 );
    register_stm32l1xx_TIMER11.SR.All = 0;
}

#endif

void interrupt_stm32l1xx_prepare() {




}


/*
void interrupt_stm32l1xx_deinit_all_can() {




}

void interrupt_stm32l1xx_deinit_all_i2c() {




}




}
void interrupt_stm32l1xx_deinit_all_rtc() {




}
void interrupt_stm32l1xx_deinit_all_spi() {




}
void interrupt_stm32l1xx_deinit_all_timer() {




}

*/

//Overwrite weak functions from startup_stm32l1xx_md.s

//{-----------------------------------------ISRs-----------------------------------------//

//--------------------------------------Implemented-----------------------------------------//


//t_u8 _usart_stm32l1xx_get_flag_status(t_register_stm32l1xx_usart* Register, t_u16 Flag){



//      I2C
#ifdef EXTENSION_ttc_i2c
void I2C_General_IRQHandler( t_physical_index PhysicalIndex, t_register_stm32l1xx_i2c* I2Cx ) {
    t_ttc_interrupt_config_i2c* ISRs = A( ttc_interrupt_config_i2c, PhysicalIndex );
    Assert_INTERRUPT( ISRs != NULL, ttc_assert_origin_auto );  // should never happen: the usart indexed by PhysicalIndex has not been initialized!

    t_register_stm32l1xx_i2c_sr1 StatusRegister = I2Cx->SR1;

    if ( StatusRegister.Bits.SB == 1 ) {
        ISRs->isr_StartBitSent( PhysicalIndex, ISRs->Argument_StartBitSent );
        StatusRegister.Bits.SB = 0;
    }
    else if ( StatusRegister.Bits.ADDR == 1 ) {
        ISRs->isr_AddressSent( PhysicalIndex, ISRs->Argument_AddressSent );
        StatusRegister.Bits.ADDR = 0;
    }
    else if ( StatusRegister.Bits.BTF == 1 ) {
        ISRs->isr_DataByteTransferFinished( PhysicalIndex, ISRs->Argument_10BitHeader );
        StatusRegister.Bits.BTF = 0;
    }
    else if ( StatusRegister.Bits.ADD10 == 1 ) {
        ISRs->isr_10BitHeader( PhysicalIndex, ISRs->Argument_10BitHeader );
        StatusRegister.Bits.ADD10 = 0;
    }
    else if ( StatusRegister.Bits.STOPF == 1 ) {
        ISRs->isr_StopReceived( PhysicalIndex, ISRs->Argument_StopReceived );
        StatusRegister.Bits.STOPF = 0;
    }
    else if ( StatusRegister.Bits.RXNE == 1 ) {
        ISRs->isr_ReceiveBufferNotEmpty( PhysicalIndex, ISRs->Argument_ReceiveBufferNotEmpty );
        StatusRegister.Bits.RXNE = 0;
    }
    else if ( StatusRegister.Bits.TxE == 1 ) {
        ISRs->isr_TransmitBufferEmpty( PhysicalIndex, ISRs->Argument_TransmitBufferEmpty );
        StatusRegister.Bits.TxE = 0;
    }
    else if ( StatusRegister.Bits.BERR == 1 ) {
        ISRs->isr_BusError( PhysicalIndex, ISRs->Argument_BusError );
        StatusRegister.Bits.BERR = 0;
    }
    else if ( StatusRegister.Bits.ARLO == 1 ) {
        ISRs->isr_ArbitrationLoss( PhysicalIndex, ISRs->Argument_ArbitrationLoss );
        StatusRegister.Bits.ARLO = 0;
    }
    else if ( StatusRegister.Bits.AF == 1 ) {
        ISRs->isr_AckFail( PhysicalIndex, ISRs->Argument_AckFail );
        StatusRegister.Bits.AF = 0;
    }
    else if ( StatusRegister.Bits.OVR == 1 ) {
        ISRs->isr_Overrun( PhysicalIndex, ISRs->Argument_Overrun );
        StatusRegister.Bits.OVR = 0;
    }
    else if ( StatusRegister.Bits.PECERR == 1 ) {
        ISRs->isr_PECError( PhysicalIndex, ISRs->Argument_PECError );
        StatusRegister.Bits.PECERR = 0;
    }
    else if ( StatusRegister.Bits.TIMEOUT == 1 ) {
        ISRs->isr_Timeout( PhysicalIndex, ISRs->Argument_Timeout );
        StatusRegister.Bits.TIMEOUT = 0;
    }
    else if ( StatusRegister.Bits.SMBALERT == 1 ) {
        ISRs->isr_SMBusAlert( PhysicalIndex, ISRs->Argument_SMBusAlert );
        StatusRegister.Bits.SMBALERT = 0;
    }

    I2Cx->SR1 = StatusRegister;
}

void I2C1_EV_IRQHandler() {
    I2C_General_IRQHandler( 1, ( t_register_stm32l1xx_i2c* ) I2C1 );
}

void I2C1_ER_IRQHandler() {
    I2C_General_IRQHandler( 1, ( t_register_stm32l1xx_i2c* ) I2C1 );
}

void I2C2_EV_IRQHandler() {
    I2C_General_IRQHandler( 2, ( t_register_stm32l1xx_i2c* )I2C2 );
}

void I2C2_ER_IRQHandler() {
    I2C_General_IRQHandler( 2, ( t_register_stm32l1xx_i2c* ) I2C2 );
}
#endif


//----------------------------------Not Implemented-----------------------------------------//


void WWDG_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void PVD_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );
}
void FLASH_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void RCC_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void DMA1_Channel1_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void DMA1_Channel2_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void DMA1_Channel3_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void DMA1_Channel4_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void DMA1_Channel5_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void DMA1_Channel6_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void DMA1_Channel7_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void ADC1_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void USB_HP_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void USB_LP_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void DAC_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void COMP_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void LCD_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void SPI1_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void SPI2_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}
void USB_FS_WKUP_IRQHandler() {
    Assert_INTERRUPT( 0, ttc_assert_origin_auto );

}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
