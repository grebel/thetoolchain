#ifndef INTERRUPT_STM32W1XX_H
#define INTERRUPT_STM32W1XX_H

/** { interrupt_stm32f1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for interrupt devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by high-level interrupt and application.
 *
 *  Created from template device_architecture.h revision 21 at 20140224 21:37:20 UTC
 *
 *  Note: See ttc_interrupt.h for description of stm32f1xx independent INTERRUPT implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_INTERRUPT_STM32F1XX
//
// Implementation status of low-level driver support for interrupt devices on stm32f1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_INTERRUPT_STM32F1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_INTERRUPT_STM32F1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_INTERRUPT_STM32F1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_INTERRUPT_STM32F1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "additionals/270_CPAL/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h" // must be included BEFORE core_cm3.h
#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
#include "stm32f10x_exti.h"
#include "misc.h"            // provides NVIC_InitTypeDef
#include "stm32f10x_rcc.h"   // provides RCC_APB2PeriphClockCmd()
#include "stm32f10x_rtc.h"
#include "interrupt_cortexm3.h"
#include "interrupt_stm32f1xx_types.h"
#include "../basic/basic_cm3.h"
#include "../ttc_interrupt_types.h"
#include "../ttc_memory.h"


#ifdef EXTENSION_ttc_usart
    #include "../ttc_usart_types.h"
#endif
#ifdef EXTENSION_ttc_timer
    #include "../ttc_timer_types.h"
#endif
#ifdef EXTENSION_ttc_gpio
    #include "stm32f10x_gpio.h"  // provides GPIO_PortSourceGPIOA
    #include "../ttc_gpio.h"
#endif
#ifdef EXTENSION_ttc_can
    #include "../ttc_can_types.h"
#endif
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_interrupt_interface.h
#define ttc_driver_interrupt_all_enable()                           interrupt_cortexm3_all_enable()
#define ttc_driver_interrupt_all_disable()                          interrupt_cortexm3_all_disable()
#define ttc_driver_interrupt_check_all_disabled()                   interrupt_cortexm3_check_all_disabled()
#define ttc_driver_interrupt_deinit(Config_ISRs)                    interrupt_stm32f1xx_deinit(Config_ISRs)
#define ttc_driver_interrupt_prepare()                              interrupt_stm32f1xx_prepare()
#define ttc_driver_interrupt_reset(Config_ISRs)                     interrupt_stm32f1xx_reset(Config_ISRs)
#define ttc_driver_interrupt_gpio_calc_config_index(PhysicalIndex)  interrupt_stm32f1xx_gpio_calc_config_index(PhysicalIndex)
#define ttc_driver_interrupt_deinit_all_rtc()                       interrupt_stm32f1xx_deinit_all_rtc()
#define ttc_driver_interrupt_deinit_rtc(Config_ISRs)                interrupt_stm32f1xx_deinit_rtc(Config_ISRs)
#define ttc_driver_interrupt_disable_rtc(Config_ISRs)               interrupt_stm32f1xx_disable_rtc(Config_ISRs)
#define ttc_driver_interrupt_enable_rtc(Config_ISRs)                interrupt_stm32f1xx_enable_rtc(Config_ISRs)
#define ttc_driver_interrupt_generate_gpio(Handle)                  interrupt_stm32f1xx_generate_gpio(Handle)
#define ttc_driver_interrupt_init_rtc(Config_ISRs)                  interrupt_stm32f1xx_init_rtc(Config_ISRs)
//#define ttc_driver_interrupt_check_inside_isr()  (provided by interrupt_cortexm3.h)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_interrupt.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_interrupt.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

/** Prepares interrupt driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void interrupt_stm32f1xx_prepare();

#ifdef EXTENSION_ttc_gpio //{

    #define ttc_driver_interrupt_init_gpio(Config_ISRs)                        interrupt_stm32f1xx_init_gpio(Config_ISRs)
    #define ttc_driver_interrupt_deinit_gpio(Config_ISRs)                      interrupt_stm32f1xx_deinit_gpio(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_gpio()                             interrupt_stm32f1xx_deinit_all_gpio()
    #define ttc_driver_interrupt_enable_gpio(Config_ISRs)                      interrupt_stm32f1xx_enable_gpio(Config_ISRs)
    #define ttc_driver_interrupt_disable_gpio(Config_ISRs)                     interrupt_stm32f1xx_disable_gpio(Config_ISRs)
    #define ttc_driver_interrupt_gpio_calc_interruptline(PhysicalIndex)        interrupt_stm32f1xx_gpio_calc_interruptline(PhysicalIndex)

    /** initializes single interrupt from gpio device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs     configuration of single interrupt line
    */
    e_ttc_interrupt_errorcode interrupt_stm32f1xx_init_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );

    /** deinitializes single interrupt from gpio device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs     configuration of single interrupt line
    * @param InterruptLine  interrupt line to use (return value from interrupt_stm32w1xx_gpio_calc_interruptline() )
    */
    void interrupt_stm32f1xx_deinit_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );

    /** { resets interrupt settings of all GPIO units to their reset value
    *
    }*/
    void interrupt_stm32f1xx_deinit_all_gpio();

    /** enables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs     configuration of single interrupt line
    */
    void interrupt_stm32f1xx_enable_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );

    /** { enables/ disables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs     configuration of single interrupt line
    */
    void interrupt_stm32f1xx_disable_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );

    /** { Calculates index of ttc_interrupt_config_gpio[] corresponding for given pin
    *
    * @param   PhysicalIndex  32-bit value representing GPIO-bank + pin
    * @return  Index of internal interrupt line serving given pin (0..TTC_INTERRUPT_GPIO_AMOUNT-1)
    }*/
    t_u8 interrupt_stm32f1xx_gpio_calc_config_index( t_base PhysicalIndex );

    /** Calculates index of internal interrupt line that is used to serve external interrupts from given pin
    *
    * @param   PhysicalIndex  32-bit value representing GPIO-bank + pin
    * @return  Index of internal interrupt line serving given pin
    */
    t_u8 interrupt_stm32f1xx_gpio_calc_interruptline( t_base PhysicalIndex );

#endif //}
#ifdef EXTENSION_ttc_i2c //{

    #define ttc_driver_interrupt_init_i2c(Config_ISRs)    interrupt_stm32f1xx_init_i2c(Config_ISRs)
    #define ttc_driver_interrupt_deinit_i2c(Config_ISRs)  interrupt_stm32f1xx_deinit_i2c(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_i2c()                       interrupt_stm32f1xx_deinit_all_i2c()
    #define ttc_driver_interrupt_enable_i2c(Config_ISRs)  interrupt_stm32f1xx_enable_i2c(Config_ISRs)
    #define ttc_driver_interrupt_disable_i2c(Config_ISRs) interrupt_stm32f1xx_disable_i2c(Config_ISRs)

    /** initializes single interrupt from i2c device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs     configuration of single interrupt line
    * @return               ==0: initialization successfull; error code otherwise
    */
    e_ttc_interrupt_errorcode interrupt_stm32f1xx_init_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

    /** initializes single interrupt from i2c device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs     configuration of single interrupt line
    */
    void interrupt_stm32f1xx_deinit_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

    /** { resets interrupt settings of all I2C units to their reset value
    *
    }*/
    void interrupt_stm32f1xx_deinit_all_i2c();

    /** enables single interrupt from i2c device on current architecture
    *
    * @param Config_ISRs     configuration of single interrupt line
    * @param EnableIRQ      == TRUE: enable interrupt; otherwise disabled Interrupt
    */
    void interrupt_stm32f1xx_enable_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

    /** { enables/disables single interrupt from i2c device on current architecture
    *
    * @param Config_ISRs     configuration of single interrupt line
    }*/
    void interrupt_stm32f1xx_disable_i2c( t_ttc_interrupt_config_i2c* Config_ISRs );

#endif //}
#ifdef EXTENSION_ttc_rtc //{

    /** { resets interrupt settings of all CAN units to their reset value
    *
    }*/
    void interrupt_stm32f1xx_deinit_all_rtc();


    /** { deinitializes single interrupt from rtc device on current architecture
    *
    * Note: After deinitialization, the interrupt rtcnot be enabled anymore.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void interrupt_stm32f1xx_deinit_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );


    /** { enables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void interrupt_stm32f1xx_disable_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );


    /** { enables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }*/
    void interrupt_stm32f1xx_enable_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );


    /** Generates a software interrupt for an external GPIO interrupt.
    *
    * @param Handle (t_ttc_interrupt_handle)
    */
    void interrupt_stm32f1xx_generate_gpio( t_ttc_interrupt_config_gpio* Config_ISRs );


    /** { initializes single interrupt from rtc device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs    pointer to struct storing configuration data of all interrupt service routines for single physical interrupt unit
    }
    * @return   =
    */
    e_ttc_interrupt_errorcode interrupt_stm32f1xx_init_rtc( t_ttc_interrupt_config_rtc* Config_ISRs );

#endif //}
#ifdef EXTENSION_ttc_spi //{

    #define ttc_driver_interrupt_init_spi(Config_ISRs)     interrupt_stm32f1xx_init_spi(Config_ISRs)
    #define ttc_driver_interrupt_deinit_spi(Config_ISRs)   interrupt_stm32f1xx_deinit_spi(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_spi()                       interrupt_stm32f1xx_deinit_all_spi()
    #define ttc_driver_interrupt_enable_spi(Config_ISRs)   interrupt_stm32f1xx_enable_spi(Config_ISRs)
    #define ttc_driver_interrupt_disable_spi(Config_ISRs)  interrupt_stm32f1xx_disable_spi(Config_ISRs)

    /** initializes single interrupt from spi device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs     configuration of single interrupt line
    * @return               ==0: initialization successfull; error code otherwise
    */
    e_ttc_interrupt_errorcode interrupt_stm32f1xx_init_spi( t_ttc_interrupt_config_spi* Config_ISRs );

    /** deinitializes single interrupt from spi device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs     configuration of single interrupt line
    */
    void interrupt_stm32f1xx_deinit_spi( t_ttc_interrupt_config_spi* Config_ISRs );

    /** { resets interrupt settings of all SPI units to their reset value
    *
    }*/
    void interrupt_stm32f1xx_deinit_all_spi();

    /** enables single interrupt from spi device on current architecture
    *
    * @param Config_ISRs     configuration of single interrupt line
    * @param EnableIRQ      == TRUE: enable interrupt; otherwise disabled Interrupt
    */
    void interrupt_stm32f1xx_enable_spi( t_ttc_interrupt_config_spi* Config_ISRs );

    /** { enables/disables single interrupt from spi device on current architecture
    *
    * @param Config_ISRs     configuration of single interrupt line
    }*/
    void interrupt_stm32f1xx_disable_spi( t_ttc_interrupt_config_spi* Config_ISRs );

#endif //}
#ifdef EXTENSION_ttc_usart //{

    #define ttc_driver_interrupt_init_usart(Config_ISRs, Type)    interrupt_stm32f1xx_init_usart(Config_ISRs, Type)
    #define ttc_driver_interrupt_deinit_usart(Config_ISRs)        interrupt_stm32f1xx_deinit_usart(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_usart()               interrupt_stm32f1xx_deinit_all_usart()
    #define ttc_driver_interrupt_enable_usart(Config_ISRs, Type)  interrupt_stm32f1xx_enable_usart(Config_ISRs, Type)
    #define ttc_driver_interrupt_disable_usart(Config_ISRs, Type) interrupt_stm32f1xx_disable_usart(Config_ISRs, Type)

    /** initializes single interrupt from usart device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs     configuration of single interrupt line
    * @return               ==0: initialization successfull; error code otherwise
    */
    e_ttc_interrupt_errorcode interrupt_stm32f1xx_init_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type );

    /** deinitializes single interrupt from usart device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs     configuration of single interrupt line
    */
    void interrupt_stm32f1xx_deinit_usart( t_ttc_interrupt_config_usart* Config_ISRs );

    /** { resets interrupt settings of all USART units to their reset value
    *
    }*/
    void interrupt_stm32f1xx_deinit_all_usart();

    /** enables single interrupt from usart device on current architecture
    *
    * @param Config_ISRs     configuration of single interrupt line
    */
    void interrupt_stm32f1xx_enable_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type );

    /** { enables/disables single interrupt from usart device on current architecture
    *
    * @param Config_ISRs     configuration of single interrupt line
    }*/
    void interrupt_stm32f1xx_disable_usart( t_ttc_interrupt_config_usart* Config_ISRs, e_ttc_interrupt_type Type );

#endif //}
#ifdef EXTENSION_ttc_timer //{

    #define ttc_driver_interrupt_init_timer(Config_ISRs)    interrupt_stm32f1xx_init_timer(Config_ISRs)
    #define ttc_driver_interrupt_deinit_timer(Config_ISRs)  interrupt_stm32f1xx_deinit_timer(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_timer()                     interrupt_stm32f1xx_deinit_all_timer()
    #define ttc_driver_interrupt_enable_timer(Config_ISRs)  interrupt_stm32f1xx_enable_timer(Config_ISRs)
    #define ttc_driver_interrupt_disable_timer(Config_ISRs) interrupt_stm32f1xx_disable_timer(Config_ISRs)

    /** initializes single interrupt from timer device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs     configuration of single interrupt line
    * @return               ==0: initialization successfull; error code otherwise
    */
    e_ttc_interrupt_errorcode interrupt_stm32f1xx_init_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

    /** initializes single interrupt from timer device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs     configuration of single interrupt line
    */
    void interrupt_stm32f1xx_deinit_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

    /** { resets interrupt settings of all TIMER units to their reset value
    *
    }*/
    void interrupt_stm32f1xx_deinit_all_timer();

    /** enables single interrupt from timer device on current architecture
    *
    * @param Config_ISRs     configuration of single interrupt line
    * @param EnableIRQ      == TRUE: enable interrupt; otherwise disabled Interrupt
    */
    void interrupt_stm32f1xx_enable_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

    /** { enables/disables single interrupt from timer device on current architecture
    *
    * @param Config_ISRs     configuration of single interrupt line
    }*/
    void interrupt_stm32f1xx_disable_timer( t_ttc_interrupt_configimer_t* Config_ISRs );

#endif //}
#ifdef EXTENSION_ttc_can //{

    #define ttc_driver_interrupt_init_can(Config_ISRs)    interrupt_stm32f1xx_init_can(Config_ISRs)
    #define ttc_driver_interrupt_deinit_can(Config_ISRs)  interrupt_stm32f1xx_deinit_can(Config_ISRs)
    #define ttc_driver_interrupt_deinit_all_can()         interrupt_stm32f1xx_deinit_all_can()
    #define ttc_driver_interrupt_enable_can(Config_ISRs)  interrupt_stm32f1xx_enable_can(Config_ISRs)
    #define ttc_driver_interrupt_disable_can(Config_ISRs) interrupt_stm32f1xx_disable_can(Config_ISRs)

    /** initializes single interrupt from can device on current architecture
    *
    * Note: After initialization, the interrupt must be enabled to trigger.
    *
    * @param Config_ISRs     configuration of single interrupt line
    * @return               ==0: initialization successfull; error code otherwise
    */
    e_ttc_interrupt_errorcode interrupt_stm32f1xx_init_can( t_ttc_interrupt_config_can* Config_ISRs );

    /** deinitializes single interrupt from can device on current architecture
    *
    * Note: After deinitialization, the interrupt cannot be enabled anymore.
    *
    * @param Config_ISRs     configuration of single interrupt line
    */
    void interrupt_stm32f1xx_deinit_can( t_ttc_interrupt_config_can* Config_ISRs );

    /** { resets interrupt settings of all CAN units to their reset value
    *
    }*/
    void interrupt_stm32f1xx_deinit_all_can();

    /** enables single interrupt from can device on current architecture
    *
    * @param Config_ISRs     configuration of single interrupt line
    */
    void interrupt_stm32f1xx_enable_can( t_ttc_interrupt_config_can* Config_ISRs );

    /** { enables single interrupt from gpio device on current architecture
    *
    * @param Config_ISRs     configuration of single interrupt line
    */
    void interrupt_stm32f1xx_disable_can( t_ttc_interrupt_config_can* Config_ISRs );

#endif //}

/* initializes interrupt of given type to call given ISR
 * @param PhysicalIndex  real index of physical functional unit (e.g 0=USART1, ...)
 * @param ISR_USART      pointer to register-base of USART that triggered the interrupt
 */
void USART_General_IRQConfig_ISRsr( t_physical_index PhysicalIndex, volatile t_register_stm32f1xx_usart* ISR_USART );
void GPIO_General_IRQConfig_ISRsr( t_physical_index PhysicalIndex );
void TIMER_General_IRQConfig_ISRsr( t_physical_index PhysicalIndex, volatile t_register_stm32f1xx_timer* TIMER );
void CAN_General_IRQConfig_ISRsr( t_physical_index PhysicalIndex, volatile t_register_stm32f1xx_can* ISR_CAN );

//void Reset_Config_ISRsr();
void NMI_Config_ISRsr();
void HardFault_Config_ISRsr();
void MemManage_Config_ISRsr();
void BusFault_Config_ISRsr();
void UsageFault_Config_ISRsr();
void SVC_Config_ISRsr();
void DebugMon_Config_ISRsr();
void PendSV_Config_ISRsr();
void SysTick_Config_ISRsr();
void WWDG_IRQConfig_ISRsr();
void PVD_IRQConfig_ISRsr();
void TAMPER_IRQConfig_ISRsr();
void RTC_IRQConfig_ISRsr();
void FLASH_IRQConfig_ISRsr();
void RCC_IRQConfig_ISRsr();
void EXTI0_IRQConfig_ISRsr();
void EXTI1_IRQConfig_ISRsr();
void EXTI2_IRQConfig_ISRsr();
void EXTI3_IRQConfig_ISRsr();
void EXTI4_IRQConfig_ISRsr();
void DMA1_Channel1_IRQConfig_ISRsr();
void DMA1_Channel2_IRQConfig_ISRsr();
void DMA1_Channel3_IRQConfig_ISRsr();
void DMA1_Channel4_IRQConfig_ISRsr();
void DMA1_Channel5_IRQConfig_ISRsr();
void DMA1_Channel6_IRQConfig_ISRsr();
void DMA1_Channel7_IRQConfig_ISRsr();
void ADC1_2_IRQConfig_ISRsr();
void CAN1_TX_IRQConfig_ISRsr();
void CAN1_RX0_IRQConfig_ISRsr();
void CAN1_RX1_IRQConfig_ISRsr();
void CAN1_SCE_IRQConfig_ISRsr();
void EXTI9_5_IRQConfig_ISRsr();
void TIM1_BRK_IRQConfig_ISRsr();
void TIM1_UP_IRQConfig_ISRsr();
void TIM1_TRG_COM_IRQConfig_ISRsr();
void TIM1_CC_IRQConfig_ISRsr();
void TIM2_IRQConfig_ISRsr();
void TIM3_IRQConfig_ISRsr();
void TIM4_IRQConfig_ISRsr();
void I2C1_EV_IRQConfig_ISRsr();
void I2C1_ER_IRQConfig_ISRsr();
void I2C2_EV_IRQConfig_ISRsr();
void I2C2_ER_IRQConfig_ISRsr();
void SPI1_IRQConfig_ISRsr();
void SPI2_IRQConfig_ISRsr();
void USART1_IRQConfig_ISRsr();
void USART2_IRQConfig_ISRsr();
void USART3_IRQConfig_ISRsr();
void EXTI15_10_IRQConfig_ISRsr();
void RTCAlarm_IRQConfig_ISRsr();
void OTG_FS_WKUP_IRQConfig_ISRsr();
void TIM5_IRQConfig_ISRsr();
void SPI3_IRQConfig_ISRsr();
void UART4_IRQConfig_ISRsr();
void UART5_IRQConfig_ISRsr();
void TIM6_IRQConfig_ISRsr();
void TIM7_IRQConfig_ISRsr();
void DMA2_Channel1_IRQConfig_ISRsr();
void DMA2_Channel2_IRQConfig_ISRsr();
void DMA2_Channel3_IRQConfig_ISRsr();
void DMA2_Channel4_IRQConfig_ISRsr();
void DMA2_Channel5_IRQConfig_ISRsr();
void ETH_IRQConfig_ISRsr();
void ETH_WKUP_IRQConfig_ISRsr();
void CAN2_TX_IRQConfig_ISRsr();
void CAN2_RX0_IRQConfig_ISRsr();
void CAN2_RX1_IRQConfig_ISRsr();
void CAN2_SCE_IRQConfig_ISRsr();
void OTG_FS_IRQConfig_ISRsr();
void BootRAM();
void SDIO_IRQHandler();


//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _interrupt_stm32f1xx_foo(ttc_foo_isrs_t* Config_ISRs)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //INTERRUPT_STM32W1XX_H
