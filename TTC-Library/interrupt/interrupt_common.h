#ifndef interrupt_common_h
#define interrupt_common_h

/** interrupt_common.h ****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to interrupt low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.h revision 14 at 20180124 09:40:44 UTC
 *
 *  Authors: Gregor Rebel
 *
 *  Description of interrupt_common (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "interrupt_cortexm3.c"
 */

#include "../ttc_basic.h"
#include "../ttc_interrupt_types.h" // will include interrupt_common_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_interrupt_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_interrupt_foo
 */

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions
/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_interrupt.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_interrupt.h for details.
 *
 * You normally should not need to add functions here.
 * Global feature functions must be added in ttc_interrupt.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl interrupt UPDATE".
 */

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //interrupt_common_h
