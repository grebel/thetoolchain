#ifndef INTERRUPT_STM32F1XX_TYPES_H
#define INTERRUPT_STM32F1XX_TYPES_H

/** { interrupt_stm32f1xx.h ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for INTERRUPT devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by ttc_interrupt_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140224 21:37:20 UTC
 *
 *  Note: See ttc_interrupt.h for description of architecture independent INTERRUPT implementation.
 *
 *  Authors: Gregor Rebel, Greg Knoll
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_INTERRUPT1   // device not defined in makefile
    #define TTC_INTERRUPT1    E_ttc_interrupt_architecture_stm32f1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

#define TTC_INTERRUPT_GPIO_AMOUNT  16 // CortexM3 provides 16 lines for external gpio-interrupts
#define TTC_INTERRUPT_USART_AMOUNT 5
#ifdef STM32F10X_XL
    #define TTC_INTERRUPT_TIMER_AMOUNT 11
#else
    #define TTC_INTERRUPT_TIMER_AMOUNT 5
#endif
#define TTC_INTERRUPT_SPI_AMOUNT 2
#define TTC_INTERRUPT_CAN_AMOUNT 2
#define TTC_INTERRUPT_I2C_AMOUNT 2
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic.h"
#include "../register/register_stm32f1xx_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_interrupt_types.h ************************

typedef struct { // register description (adapt according to stm32f1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_interrupt_gpio_register;

typedef struct {  // stm32f1xx specific configuration data of single INTERRUPT device
    t_register_stm32f1xx_exti* regEXTI;
} __attribute__( ( __packed__ ) ) t_interrupt_stm32f1xx_config;

// t_ttc_interrupt_architecture is required by ttc_interrupt_types.h
#define t_ttc_interrupt_architecture t_interrupt_stm32f1xx_config

//} Structures/ Enums


#endif //INTERRUPT_STM32F1XX_TYPES_H
