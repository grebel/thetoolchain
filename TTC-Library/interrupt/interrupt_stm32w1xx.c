/** { interrupt_stm32w1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for interrupt devices on stm32w1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 20 at 20140224 21:37:20 UTC
 *
 *  Note: See ttc_interrupt.h for description of stm32w1xx independent INTERRUPT implementation.
 * 
 *  Authors: Gregor Rebel
}*/

#include "interrupt_stm32w1xx.h"

//{ Global Variables ***********************************************************

// provided by ttc_interrupt.c
#ifdef EXTENSION_ttc_usart
  A_dynamic_extern(t_ttc_interrupt_config_usart*, ttc_interrupt_config_usart);
#endif
#ifdef EXTENSION_ttc_gpio
A_dynamic_extern(t_ttc_interrupt_config_gpio*,    ttc_interrupt_config_gpio);
#endif
#ifdef EXTENSION_ttc_timer
A_dynamic_extern(t_ttc_interrupt_configimer_t*,   t_ttc_interrupt_configimer);
#endif

//}
//{ Function definitions *******************************************************

                     void interrupt_stm32w1xx_prepare() {

}
                     void interrupt_stm32w1xx_deinit() {
#ifdef EXTENSION_ttc_gpio
    interrupt_stm32w1xx_deinit_all_gpio();
#endif
}
#ifdef EXTENSION_ttc_gpio //{

                     void interrupt_stm32w1xx_deinit_all_gpio() {
    *( (t_u32*) &register_stm32w1xx_GPIO_INT_CFG.INTCFGA ) = 0;
    *( (t_u32*) &register_stm32w1xx_GPIO_INT_CFG.INTCFGB ) = 0;
    *( (t_u32*) &register_stm32w1xx_GPIO_INT_CFG.INTCFGC ) = 0;
    *( (t_u32*) &register_stm32w1xx_GPIO_INT_CFG.INTCFGD ) = 0;
                         // ToDo: Reset other registers
                         // ToDo: move to interrupt_cortexm3.c
}
e_ttc_interrupt_errorcode interrupt_stm32w1xx_init_gpio(t_ttc_interrupt_config_gpio* Config_ISRs) {
    t_u8 ConfigIndex = Config_ISRs->Common.ConfigIndex;
    Assert_INTERRUPT(ConfigIndex < TTC_INTERRUPT_GPIO_AMOUNT, ttc_assert_origin_auto);

    if (ConfigIndex >= 2) { // selectable interrupt line: find corresponding setting

        e_ttc_gpio_pin GPIO = Config_ISRs->PhysicalIndex;

        t_u32 GPIO_IRQxSEL = -1;
        switch (GPIO) { // find setting for interrupt select multiplexer
        case E_ttc_gpio_pin_a0: GPIO_IRQxSEL =  0; break;
        case E_ttc_gpio_pin_a1: GPIO_IRQxSEL =  1; break;
        case E_ttc_gpio_pin_a2: GPIO_IRQxSEL =  2; break;
        case E_ttc_gpio_pin_a3: GPIO_IRQxSEL =  3; break;
        case E_ttc_gpio_pin_a4: GPIO_IRQxSEL =  4; break;
        case E_ttc_gpio_pin_a5: GPIO_IRQxSEL =  5; break;
        case E_ttc_gpio_pin_a6: GPIO_IRQxSEL =  6; break;
        case E_ttc_gpio_pin_a7: GPIO_IRQxSEL =  7; break;
        case E_ttc_gpio_pin_b0: GPIO_IRQxSEL =  8; break;
        case E_ttc_gpio_pin_b1: GPIO_IRQxSEL =  9; break;
        case E_ttc_gpio_pin_b2: GPIO_IRQxSEL = 10; break;
        case E_ttc_gpio_pin_b3: GPIO_IRQxSEL = 11; break;
        case E_ttc_gpio_pin_b4: GPIO_IRQxSEL = 12; break;
        case E_ttc_gpio_pin_b5: GPIO_IRQxSEL = 13; break;
        case E_ttc_gpio_pin_b6: GPIO_IRQxSEL = 14; break;
        case E_ttc_gpio_pin_b7: GPIO_IRQxSEL = 15; break;
        case E_ttc_gpio_pin_c0: GPIO_IRQxSEL = 16; break;
        case E_ttc_gpio_pin_c1: GPIO_IRQxSEL = 17; break;
        case E_ttc_gpio_pin_c2: GPIO_IRQxSEL = 18; break;
        case E_ttc_gpio_pin_c3: GPIO_IRQxSEL = 19; break;
        case E_ttc_gpio_pin_c4: GPIO_IRQxSEL = 20; break;
        case E_ttc_gpio_pin_c5: GPIO_IRQxSEL = 21; break;
        case E_ttc_gpio_pin_c6: GPIO_IRQxSEL = 22; break;
        case E_ttc_gpio_pin_c7: GPIO_IRQxSEL = 23; break;
        default: Assert_INTERRUPT(0, ttc_assert_origin_auto); break; // unsupported gpio pin
        }
        if (ConfigIndex == 2) // IRQC
        { ttc_register_set32(&(register_stm32w1xx_GPIO_DB_WAKE_IRQ.IRQCSEL), bSEL, GPIO_IRQxSEL); }
        else                  // IRQD
        { ttc_register_set32(&(register_stm32w1xx_GPIO_DB_WAKE_IRQ.IRQDSEL), bSEL, GPIO_IRQxSEL); }
    }
    e_register_stm32w1xx_gpio_irqx_mode InterruptMode = stm32w_irq_mode_disabled;
    switch (Config_ISRs->Common.Type) {          // select interrupt mode
    case tit_GPIO_Falling: {
        InterruptMode = stm32w_irq_mode_falling_edge;
        break;
    }
    case tit_GPIO_Rising: {
        InterruptMode = stm32w_irq_mode_rising_edge;
        break;
    }
    case tit_GPIO_Rising_Falling: {
        InterruptMode = stm32w_irq_mode_rising_and_falling_edge;
        break;
    }
    case tit_GPIO_Active_High: {
        InterruptMode = stm32w_irq_mode_active_high;
        break;
    }
    case tit_GPIO_Active_Low: {
        InterruptMode = stm32w_irq_mode_active_low;
        break;
    }
    default:
        return ec_interrupt_IrqSourceNotFound;
    }
    t_u8 FilterSetting = 1; // ==1: enable digital input filer; ==0: disable
    switch (ConfigIndex) { // configure interrupt line
    case 0: {
        ttc_register_set32_2(&(register_stm32w1xx_GPIO_INT_CFG.INTCFGA), bINTMOD, InterruptMode, bINTFILT, FilterSetting & 1);
        Config_ISRs->LowLevel.InterruptLine = isil_irqa;
        break;
    }
    case 1: {
        ttc_register_set32_2(&(register_stm32w1xx_GPIO_INT_CFG.INTCFGB), bINTMOD, InterruptMode, bINTFILT, FilterSetting & 1);
        Config_ISRs->LowLevel.InterruptLine = isil_irqb;
        break;
    }
    case 2: {
        ttc_register_set32_2(&(register_stm32w1xx_GPIO_INT_CFG.INTCFGC), bINTMOD, InterruptMode, bINTFILT, FilterSetting & 1);
        Config_ISRs->LowLevel.InterruptLine = isil_irqc;
        break;
    }
    case 3: {
        ttc_register_set32_2(&(register_stm32w1xx_GPIO_INT_CFG.INTCFGD), bINTMOD, InterruptMode, bINTFILT, FilterSetting & 1);
        Config_ISRs->LowLevel.InterruptLine = isil_irqd;
        break;
    }
    default: ttc_assert_halt_origin(ttc_assert_origin_auto); break;
    }
    Config_ISRs->LowLevel.RegisterSetMask = 1 << Config_ISRs->LowLevel.InterruptLine;

    // Compute corresponding IRQ priority level
    t_u32 PreemptionPriority = 0x00;
    t_u32 ChannelSubPriority = 0x00;

    t_u32 tmppriority = 0x00, tmppre = 0x00, tmpsub = 0x0F;
    tmppriority = register_cortexm3_SCB.AIRCR.PRIGROUP;

    tmppre = (0x4 - tmppriority);
    tmpsub = tmpsub >> tmppriority;

    tmppriority = PreemptionPriority << tmppre;
    tmppriority |= (ChannelSubPriority & tmpsub);

    *( (t_u8*) &(register_cortexm3_NVIC.IP[Config_ISRs->LowLevel.InterruptLine]) ) = (t_u8) tmppriority << 4;

    return (e_ttc_interrupt_errorcode) 0;
}
void interrupt_stm32w1xx_deinit_gpio(t_ttc_interrupt_config_gpio* Config_ISRs) {
    Assert_INTERRUPT(ttc_memory_is_writable(Config_ISRs), ttc_assert_origin_auto);
    t_u8 InterruptLine = Config_ISRs->LowLevel.InterruptLine;

    switch (InterruptLine) {
    case isil_irqa: {
        ttc_register_set32_2(&(register_stm32w1xx_GPIO_INT_CFG.INTCFGA), bINTMOD, stm32w_irq_mode_disabled, bINTFILT, 0);
        break;
    }
    case isil_irqb: {
        ttc_register_set32_2(&(register_stm32w1xx_GPIO_INT_CFG.INTCFGB), bINTMOD, stm32w_irq_mode_disabled, bINTFILT, 0);
        break;
    }
    case isil_irqc: {
        ttc_register_set32(&(register_stm32w1xx_GPIO_DB_WAKE_IRQ.IRQCSEL), bSEL, 0);
        ttc_register_set32_2(&(register_stm32w1xx_GPIO_INT_CFG.INTCFGC), bINTMOD, stm32w_irq_mode_disabled, bINTFILT, 0);
        break;
    }
    case isil_irqd: {
        ttc_register_set32(&(register_stm32w1xx_GPIO_DB_WAKE_IRQ.IRQDSEL), bSEL, 0);
        ttc_register_set32_2(&(register_stm32w1xx_GPIO_INT_CFG.INTCFGD), bINTMOD, stm32w_irq_mode_disabled, bINTFILT, 0);
        break;
    }
    default: ttc_assert_halt_origin(ttc_assert_origin_auto); break;
    }
}
void interrupt_stm32w1xx_enable_gpio(t_ttc_interrupt_config_gpio* Config_ISRs) {

    *( (t_u32*) &register_cortexm3_NVIC.ISER0 ) = Config_ISRs->LowLevel.RegisterSetMask;
}
void interrupt_stm32w1xx_disable_gpio(t_ttc_interrupt_config_gpio* Config_ISRs) {

    *( (t_u32*) &register_cortexm3_NVIC.ICER0 ) = Config_ISRs->LowLevel.RegisterSetMask;
}
                     t_u8 interrupt_stm32w1xx_gpio_calc_config_index(t_base PhysicalIndex) {
    /** External Interrupts on STM32W (== EM357) according to DS6473.pdf p. 64
*
* External Interrupts IRQA and IRQB have fixed pin assignments.
* IRQC and IRQD can use any GPIO pin.
*
* Physical Index is used to identify corresponding pin.
*
* Excerpt from DS6473:
* External interrupts have individual triggering and filtering options selected using the
* registers GPIO_INTCFGA, GPIO_INTCFGB, GPIO_INTCFGC, and GPIO_INTCFGD. The
* bit field GPIO_INTMOD of the GPIO_INTCFGx register enables IRQx's second level
* interrupt and selects the triggering mode: 0 is disabled; 1 for rising edge; 2 for falling edge; 3
* for both edges; 4 for active high level; 5 for active low level. The minimum width needed to
* latch an unfiltered external interrupt in both level- and edge-triggered mode is 80 ns. With
* the digital filter enabled (the GPIO_INTFILT bit in the GPIO_INTCFGx register is set), the
* minimum width needed is 450 ns.
*/

    e_ttc_gpio_pin Pin = (e_ttc_gpio_pin) PhysicalIndex;
    e_interrupt_stm32w_interrupt_lines InterruptLineSTM32w = isil_unknown;
    switch (Pin) {
    case E_ttc_gpio_pin_b0: InterruptLineSTM32w = 0; break; // IRQA
    case E_ttc_gpio_pin_c0: InterruptLineSTM32w = 3; break; // IRQD
    case E_ttc_gpio_pin_b7: InterruptLineSTM32w = 2; break; // IRQC
    case E_ttc_gpio_pin_b6: InterruptLineSTM32w = 1; break; // IRQB
    default: break;
    }

    if (InterruptLineSTM32w == isil_unknown) { // interrupt source not fixed: use interrupt selection

        // check registered external interrupts for first unused selectable interrupt
        if ( (A(ttc_interrupt_config_gpio, 2) == NULL) || (A(ttc_interrupt_config_gpio, 2)->isr == NULL) )
            InterruptLineSTM32w = 2; // IRQC not used: use IRQC
        else if ( (A(ttc_interrupt_config_gpio, 3) == NULL) || (A(ttc_interrupt_config_gpio, 3)->isr == NULL) )
            InterruptLineSTM32w = 3; // IRQD not used: use IRQD
        else
            return ec_interrupt_NotEgnoughResources; // all external, selectable interrupt sources in use
    }
    Assert_INTERRUPT(InterruptLineSTM32w < isil_unknown, ttc_assert_origin_auto);

    return InterruptLineSTM32w;
}

#endif //}
#ifdef EXTENSION_ttc_i2c //{

                     e_ttc_interrupt_errorcode interrupt_stm32w1xx_init_i2c(t_ttc_interrupt_config_i2c* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);
    return (e_ttc_interrupt_errorcode) 0;
}
                     void interrupt_stm32w1xx_deinit_i2c(t_ttc_interrupt_config_i2c* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
                     void interrupt_stm32w1xx_enable_i2c(t_ttc_interrupt_config_i2c* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
                     void interrupt_stm32w1xx_disable_i2c(t_ttc_interrupt_config_i2c* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
                     void interrupt_stm32w1xx_deinit_all_i2c() {




}

#endif //}
#ifdef EXTENSION_ttc_spi //{

e_ttc_interrupt_errorcode interrupt_stm32w1xx_init_spi(t_ttc_interrupt_config_spi* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);

    return (e_ttc_interrupt_errorcode) 0;
}
                     void interrupt_stm32w1xx_deinit_spi(t_ttc_interrupt_config_spi* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
                     void interrupt_stm32w1xx_enable_spi(t_ttc_interrupt_config_spi* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
                     void interrupt_stm32w1xx_disable_spi(t_ttc_interrupt_config_spi* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
                     void interrupt_stm32w1xx_deinit_all_spi() {




}

#endif //}
#ifdef EXTENSION_ttc_usart //{

e_ttc_interrupt_errorcode interrupt_stm32w1xx_init_usart(t_ttc_interrupt_config_usart* Config_ISRs) {

    // all done in ttc_interrupt.c

    return (e_ttc_interrupt_errorcode) 0;
}
void interrupt_stm32w1xx_deinit_usart(t_ttc_interrupt_config_usart* Config_ISRs) {

}
                     void interrupt_stm32w1xx_enable_usart(t_ttc_interrupt_config_usart* Config_ISRs) {
    Assert_INTERRUPT(ttc_memory_is_writable(Config_ISRs), ttc_assert_origin_auto);
    t_u8 ConfigIndex = Config_ISRs->Common.ConfigIndex;
    volatile t_register_stm32w1xx_int_scxcfg INT_SC1CFG = register_stm32w1xx_INT_SC1CFG;

    switch (Config_ISRs->Common.Type) {
        case tit_USART_All:
            Assert_INTERRUPT(!1, ttc_assert_origin_auto); // enabling all interrupts at once is not supported
            *( (t_u32*) &INT_SC1CFG ) &= (0xffffffff - 0x7fff); // set all interrupt flags to zero
            break;
        case tit_USART_Idle:
            INT_SC1CFG.bSCTXIDLE = 1;
            Assert_INTERRUPT(A(ttc_interrupt_config_usart, ConfigIndex)->isr_IdleLine != NULL, ttc_assert_origin_auto); // no isr configured for this interrupt!
            break;
        case tit_USART_TxComplete:
            INT_SC1CFG.bSCTXFREE = 1;
            Assert_INTERRUPT(A(ttc_interrupt_config_usart, ConfigIndex)->isr_TransmissionComplete!= NULL, ttc_assert_origin_auto); // no isr configured for this interrupt!
            break;
        case tit_USART_TransmitDataEmpty:
            INT_SC1CFG.bSCTXUND = 1;
            Assert_INTERRUPT(A(ttc_interrupt_config_usart, ConfigIndex)->isr_TransmitDataEmpty!= NULL, ttc_assert_origin_auto); // no isr configured for this interrupt!
            break;
        case tit_USART_RxNE:
            INT_SC1CFG.bSCRXVAL= 1;
            Assert_INTERRUPT(A(ttc_interrupt_config_usart, ConfigIndex)->isr_ReceiveDataNotEmpty!= NULL, ttc_assert_origin_auto); // no isr configured for this interrupt!
            break;
        case tit_USART_Error:
            Assert_INTERRUPT(A(ttc_interrupt_config_usart, ConfigIndex)->isr_Error!= NULL, ttc_assert_origin_auto); // no isr configured for this interrupt!
            INT_SC1CFG.bSC1PARERR = 1;  // irq on parity error
            INT_SC1CFG.bSC1FRMERR = 1;  // irq on frame error
            break;
        default:
            ttc_assert_halt_origin(ttc_assert_origin_auto);
            break;
    }
    register_stm32w1xx_INT_SC1CFG = INT_SC1CFG;

    // enable interrupts for SC1
    t_register_stm32w1xx_int_cfgset CFGSET = register_stm32w1xx_INT_CFGSET;
    CFGSET.bSC1 = 1;
    register_stm32w1xx_INT_CFGSET = CFGSET;
}
                     void interrupt_stm32w1xx_disable_usart(t_ttc_interrupt_config_usart* Config_ISRs) {
    Assert_INTERRUPT(ttc_memory_is_writable(Config_ISRs), ttc_assert_origin_auto);
    volatile t_register_stm32w1xx_int_scxcfg INT_SC1CFG = register_stm32w1xx_INT_SC1CFG;

    switch (Config_ISRs->Common.Type) {
        case tit_USART_All:
            *( (t_u32*) &INT_SC1CFG ) &= (0xffffffff - 0x7fff); // set all interrupt flags to zero
            break;
        case tit_USART_Idle:
            INT_SC1CFG.bSCTXIDLE = 0;
            break;
        case tit_USART_TxComplete:
            INT_SC1CFG.bSCTXFREE = 0;
            break;
        case tit_USART_TransmitDataEmpty:
            INT_SC1CFG.bSCTXUND = 0;
            break;
        case tit_USART_RxNE:
            INT_SC1CFG.bSCRXVAL= 0;
            break;
        case tit_USART_Error:
            INT_SC1CFG.bSC1PARERR = 0;  // irq on parity error
            INT_SC1CFG.bSC1FRMERR = 0;  // irq on frame error
            break;
        default:
            ttc_assert_halt_origin(ttc_assert_origin_auto);
            break;
    }
    register_stm32w1xx_INT_SC1CFG = INT_SC1CFG;

    if (! ( *( (t_u32*) &INT_SC1CFG ) & 0x7fff ) ) { // disable interrupts for SC1 only if all interrupts are disabled
        t_register_stm32w1xx_int_cfgset CFGSET = register_stm32w1xx_INT_CFGSET;
        CFGSET.bSC1 = 0;
        register_stm32w1xx_INT_CFGSET = CFGSET;
    }
}
                     void interrupt_stm32w1xx_deinit_all_usart() {




}

#endif //}
#ifdef EXTENSION_ttc_timer //{

                     void interrupt_stm32w1xx_enable_timer(t_ttc_interrupt_configimer_t* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
                     void interrupt_stm32w1xx_disable_timer(t_ttc_interrupt_configimer_t* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
                     void interrupt_stm32w1xx_deinit_timer(t_ttc_interrupt_configimer_t* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
e_ttc_interrupt_errorcode interrupt_stm32w1xx_init_timer(t_ttc_interrupt_configimer_t* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);
    return (e_ttc_interrupt_errorcode) 0;
}
void interrupt_stm32w1xx_deinit_all_timer() {




}

#endif //}
#ifdef EXTENSION_ttc_can //{

                     void interrupt_stm32w1xx_enable_can(t_ttc_interrupt_config_can* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
                     void interrupt_stm32w1xx_disable_can(t_ttc_interrupt_config_can* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}

e_ttc_interrupt_errorcode interrupt_stm32w1xx_init_can(t_ttc_interrupt_config_can* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);

    return (e_ttc_interrupt_errorcode) 0;
}
e_ttc_interrupt_errorcode interrupt_stm32w1xx_deinit_can(t_ttc_interrupt_config_can* Config_ISRs) {
    Assert_INTERRUPT(Config_ISRs, ttc_assert_origin_auto); // pointers must not be NULL
    ttc_assert_halt_origin(ttc_assert_origin_auto);

    return (e_ttc_interrupt_errorcode) 0;
}
void interrupt_stm32w1xx_deinit_all_can() {




}

#endif //}

void interrupt_stm32w1xx_deinit_all_rtc() {


#warning Function interrupt_stm32w1xx_deinit_all_rtc() is empty.
    

}
void interrupt_stm32w1xx_deinit_rtc(t_ttc_interrupt_config_rtc* Config_ISRs) {
    Assert_INTERRUPT_EXTRA(ttc_memory_is_writable(Config_ISRs), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function interrupt_stm32w1xx_deinit_rtc() is empty.
    

}
void interrupt_stm32w1xx_disable_rtc(t_ttc_interrupt_config_rtc* Config_ISRs) {
    Assert_INTERRUPT_EXTRA(ttc_memory_is_writable(Config_ISRs), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function interrupt_stm32w1xx_disable_rtc() is empty.
    

}
void interrupt_stm32w1xx_enable_rtc(t_ttc_interrupt_config_rtc* Config_ISRs) {
    Assert_INTERRUPT_EXTRA(ttc_memory_is_writable(Config_ISRs), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function interrupt_stm32w1xx_enable_rtc() is empty.
    

}
void interrupt_stm32w1xx_generate_gpio(t_ttc_interrupt_config_gpio* Config_ISRs) {


#warning Function interrupt_stm32w1xx_generate_gpio() is empty.
    

}
e_ttc_interrupt_errorcode interrupt_stm32w1xx_init_rtc(t_ttc_interrupt_config_rtc* Config_ISRs) {
    Assert_INTERRUPT_EXTRA(ttc_memory_is_writable(Config_ISRs), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function interrupt_stm32w1xx_init_rtc() is empty.
    
    return (e_ttc_interrupt_errorcode) 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)


//{ irq-handlers (supported) ---------------------------

//}irq-handlers (supported)
//{ irq-handlers (yet unsupported) ---------------------------

#if 0 //Used?
/******************************************************************************/
/*                 stm32w108xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32w108xx.s).                                            */
/******************************************************************************/

/**
  * @brief  This function handles MGMT interrupt request.
  * @param  None
  * @retval None
  */


extern void halManagementIsr();
void MNG_IRQHandler(void)
{
  halManagementIsr();
}


/**
  * @brief  This function handles SLPTMR interrupt request.
  * @param  None
  * @retval None
  */
extern void halSleepTimerIsr();
void SLPTIM_IRQHandler(void)
{
  halSleepTimerIsr();
}

/**
  * @brief  This function handles SC1 interrupt request.
  * @param  None
  * @retval None
  */
extern void halSc1Isr();
void SC1_IRQHandler(void)
{
  halSc1Isr();
}

/**
  * @brief  This function handles MAC_TMR interrupt request.
  * @param  None
  * @retval None
  */
extern void halStackMacTimerIsr();
void MAC_TIM_IRQHandler(void)
{
  halStackMacTimerIsr();
}

/**
  * @brief  This function handles MAC_TR interrupt request.
  * @param  None
  * @retval None
  */
extern void stmRadioTransmitIsr();
void MAC_TR_IRQHandler(void)
{
  stmRadioTransmitIsr();
}

/**
  * @brief  This function handles MAC_RE interrupt request.
  * @param  None
  * @retval None
  */
extern void stmRadioReceiveIsr();
void MAC_RE_IRQHandler(void)
{
  stmRadioReceiveIsr();
}

/**
  * @brief  This function handles ADC interrupt request.
  * @param  None
  * @retval None
  */
extern void halAdcIsr();
void ADC_IRQHandler(void)
{
  halAdcIsr();
}

#endif

#ifndef EXTENSION_251_stm32w1xx_simple_mac // these are defined by SimpleMAC Library
void halSysTickIsr() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void halManagementIsr() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void halAdcIsr() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void stmRadioReceiveIsr() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void stmRadioTransmitIsr() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void halStackMacTimerIsr() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
#endif
#ifndef EXTENSION_253_stm32w1xx_hal
void halSleepTimerIsr() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
#endif

void halTimer1Isr() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void halTimer2Isr() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void halBaseBandIsr() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void halSc1Isr() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void halSc2Isr() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void halSecurityIsr() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void halIrqAIsr() {
    t_ttc_interrupt_config_gpio* Config_ISR = A(ttc_interrupt_config_gpio, 0);
    if (Config_ISR->isr) {
        Config_ISR->isr(Config_ISR->PhysicalIndex, Config_ISR->Argument);

        // clear pending interrupt
        if (0) *( (t_u32*) &register_cortexm3_NVIC.ICPR0 ) = 1 << 12; //Config_ISR->LowLevel.RegisterSetMask;
        *( (t_u32*) &register_stm32w1xx_INT_GPIOFLAG ) = 1 << 0; // clearing second level interrupt is sufficient on STM32W108
    }
    else
    { ttc_assert_halt_origin(ttc_assert_origin_auto); }
}
void halIrqBIsr() {
    t_ttc_interrupt_config_gpio* Config_ISR = A(ttc_interrupt_config_gpio, 1);
    if (Config_ISR->isr) {
        Config_ISR->isr(Config_ISR->PhysicalIndex, Config_ISR->Argument);

        // clear pending interrupt
        if (0) *( (t_u32*) &register_cortexm3_NVIC.ICPR0 ) = 1 << 13; //Config_ISR->LowLevel.RegisterSetMask;
        *( (t_u32*) &register_stm32w1xx_INT_GPIOFLAG ) = 1 << 1; // clearing second level interrupt is sufficient on STM32W108
    }
    else
    { ttc_assert_halt_origin(ttc_assert_origin_auto); }
}
void halIrqCIsr() {
    t_ttc_interrupt_config_gpio* Config_ISR = A(ttc_interrupt_config_gpio, 2);
    if (Config_ISR->isr) {
        Config_ISR->isr(Config_ISR->PhysicalIndex, Config_ISR->Argument);

        // clear pending interrupt
        if (0) *( (t_u32*) &register_cortexm3_NVIC.ICPR0 ) = 1 << 14; //Config_ISR->LowLevel.RegisterSetMask;
        *( (t_u32*) &register_stm32w1xx_INT_GPIOFLAG ) = 1 << 2; // clearing second level interrupt is sufficient on STM32W108
    }
    else
    { ttc_assert_halt_origin(ttc_assert_origin_auto); }
}
void halIrqDIsr() {
    t_ttc_interrupt_config_gpio* Config_ISR = A(ttc_interrupt_config_gpio, 3);
    if (Config_ISR->isr) {
        Config_ISR->isr(Config_ISR->PhysicalIndex, Config_ISR->Argument);

        // clear pending interrupt
        if (0) *( (t_u32*) &register_cortexm3_NVIC.ICPR0 ) = 1 << 15; //Config_ISR->LowLevel.RegisterSetMask;
        *( (t_u32*) &register_stm32w1xx_INT_GPIOFLAG ) = 1 << 3; // clearing second level interrupt is sufficient on STM32W108
    }
    else
    { ttc_assert_halt_origin(ttc_assert_origin_auto); }
}
void halDebugIsr() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}
void ReservedEntry() {
    ttc_assert_halt_origin(ttc_assert_origin_auto);
}

//} irq-handlers (yet unsupported)


//}FunctionDefinitions
//{ private functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
