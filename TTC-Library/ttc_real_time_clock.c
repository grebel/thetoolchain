/** { ttc_real_time_clock.c ****************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level interface for real_time_clock device.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported 
 *  architectures.
 *
 *  Authors: Gregor Rebel
 *  
}*/
//{ Defines/ TypDefs *****************************************************
//} Defines

#include "ttc_real_time_clock.h"

//{ Global Variables *****************************************************

#if TTC_REAL_TIME_CLOCK_AMOUNT == 0
  #warning No devices defined, check your makefile!
#endif

// for each initialized device, a pointer to its generic and stm32l1 definitions is stored
A_define(t_ttc_real_time_clock_config*, ttc_real_time_clock_configs, TTC_REAL_TIME_CLOCK_AMOUNT);

// Set to TRUE on first call of ttc_real_time_clock_init()
BOOL driver_Initialized = 0;

//} Global Variables
//{ Function definitions *************************************************

t_ttc_real_time_clock_config* ttc_real_time_clock_get_configuration(t_u8 LogicalIndex) {
    Assert_REAL_TIME_CLOCK(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_real_time_clock_config* Config = A(ttc_real_time_clock_configs, LogicalIndex-1); // will assert if outside array bounds

    if (!Config) {
        Config = A(ttc_real_time_clock_configs, LogicalIndex-1) = ttc_heap_alloc_zeroed(sizeof(t_ttc_real_time_clock_config));
        ttc_real_time_clock_load_defaults(LogicalIndex);
    }

    return Config;
}
void ttc_real_time_clock_deinit(t_u8 LogicalIndex) {
    Assert_REAL_TIME_CLOCK(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_real_time_clock_config* Config = ttc_real_time_clock_get_configuration(LogicalIndex);
  
    e_ttc_real_time_clock_errorcode Result = ttc_real_time_clock_interface_deinit(Config);
    if (Result == ec_real_time_clock_OK)
      Config->Flags.Bits.Initialized = 0;
}
void ttc_real_time_clock_init(t_u8 LogicalIndex) {
    Assert_REAL_TIME_CLOCK(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_real_time_clock_config* Config = ttc_real_time_clock_get_configuration(LogicalIndex);

    e_ttc_real_time_clock_errorcode Result = ttc_real_time_clock_interface_init(Config);
    if (Result == ec_real_time_clock_OK)
      Config->Flags.Bits.Initialized = 1;
}
void ttc_real_time_clock_load_defaults(t_u8 LogicalIndex) {
    Assert_REAL_TIME_CLOCK(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_real_time_clock_config* Config = ttc_real_time_clock_get_configuration(LogicalIndex);

    ttc_memory_set( Config, 0, sizeof(t_ttc_real_time_clock_config) );

    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    Config->PhysicalIndex = ttc_real_time_clock_logical_2_physical_index(LogicalIndex);

    ttc_real_time_clock_interface_load_defaults(Config);    
}
t_u8 ttc_real_time_clock_logical_2_physical_index(t_u8 LogicalIndex) {
  switch (LogicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_REAL_TIME_CLOCK1
           case 1: return TTC_REAL_TIME_CLOCK1;
#endif
#ifdef TTC_REAL_TIME_CLOCK2
           case 2: return TTC_REAL_TIME_CLOCK2;
#endif
#ifdef TTC_REAL_TIME_CLOCK3
           case 3: return TTC_REAL_TIME_CLOCK3;
#endif
      // extend as required
      
      default: Assert_REAL_TIME_CLOCK(0, ttc_assert_origin_auto); break; // No TTC_REAL_TIME_CLOCKn defined! Check your makefile.100_board_* file!
    }
    
    return -1;
}
void ttc_real_time_clock_prepare() {
  // add your startup code here (Singletasking!)
  ttc_real_time_clock_interface_prepare();
}
void ttc_real_time_clock_reset(t_u8 LogicalIndex) {
    Assert_REAL_TIME_CLOCK(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_real_time_clock_config* Config = ttc_real_time_clock_get_configuration(LogicalIndex);

    ttc_real_time_clock_interface_reset(Config);    
}

void ttc_real_time_clock_get_time(t_u8 LogicalIndex,ttc_real_time_t_clock_time* Time){
    Assert_REAL_TIME_CLOCK(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    Assert_REAL_TIME_CLOCK(Time,ttc_assert_origin_auto); //Time isn't initialized
    ttc_real_time_clock_interface_get_time(LogicalIndex,Time);
}

// ToDo: additional functions go here...

//}FunctionDefinitions
