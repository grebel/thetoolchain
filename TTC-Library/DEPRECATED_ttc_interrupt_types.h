#ifndef TTC_INTERRUPT_TYPES_H
#define TTC_INTERRUPT_TYPES_H

/*{ ttc_interrupt_types.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Basic defines, enums and structures.
 *
 * Include this file from 
 * architecture independent header files (ttc_XXX.h) and 
 * architecture depend      header files (e.g. stm32_XXX.h)
 *
 * Note: include this file AFTER including low-level architecture drivers which should provide interrupts!
 * 
}*/
#include "ttc_basic.h"
#ifdef EXTENSION_500_ttc_usart
#  include "ttc_usart_types.h"
#endif
#ifdef EXTENSION_500_ttc_gpio
#  include "ttc_gpio_types.h"
#endif
#ifdef EXTENSION_500_ttc_timer
#  include "ttc_timer_types.h"
#endif

//{ Defines/ TypeDefs ****************************************************

#ifndef TTC_ASSERT_INTERRUPTS
  #define TTC_ASSERT_INTERRUPTS 1
#endif

#if (TTC_ASSERT_INTERRUPTS==1)  // use Assert()s in Interrupt code (somewhat slower but alot easier to debug)
  #define Assert_Interrupt(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in Interrupt code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_Interrupt(Condition, ErrorCode)
#endif

// avoid compile errors for yet undefined constants
#ifndef TTC_INTERRUPT_ENUMS_GPIO
  #define TTC_INTERRUPT_ENUMS_GPIO
#endif
#ifndef TTC_INTERRUPT_ENUMS_I2C
  #define TTC_INTERRUPT_ENUMS_I2C
#endif
#ifndef TTC_INTERRUPT_ENUMS_RADIO
  #define TTC_INTERRUPT_ENUMS_RADIO
#endif
#ifndef TTC_INTERRUPT_ENUMS_SPI
  #define TTC_INTERRUPT_ENUMS_SPI
#endif
#ifndef TTC_INTERRUPT_ENUMS_interrupt
  #define TTC_INTERRUPT_ENUMS_interrupt
#endif
#ifndef TTC_INTERRUPT_ENUMS_GPIO
  #define TTC_INTERRUPT_ENUMS_GPIO
#endif
#ifndef TTC_INTERRUPT_ENUMS_TIMER
  #define TTC_INTERRUPT_ENUMS_TIMER
#endif

// list of function pointers to initialization functions of this type:
// ttc_interrupt_errorcode_e (*interrupt_init)(ttc_interrupt_type_e Source, void* ISR);
// each architecture driver (GPIO, I2C, ...) may define one init() and multiple enums (-> ttc_interrupt_type_e).
#ifndef TTC_INTERRUPT_INIT_GPIO
#define TTC_INTERRUPT_INIT_GPIO NULL
#endif
#ifndef TTC_INTERRUPT_INIT_I2C
#define TTC_INTERRUPT_INIT_I2C NULL
#endif
#ifndef TTC_INTERRUPT_INIT_TIMER
#define TTC_INTERRUPT_INIT_TIMER NULL
#endif
#ifndef TTC_INTERRUPT_INIT_RADIO
#define TTC_INTERRUPT_INIT_RADIO NULL
#endif
#ifdef TTC_INTERRUPT_INIT_SPI
#define TTC_INTERRUPT_INIT_SPI NULL
#endif
#ifndef TTC_INTERRUPT_INIT_interrupt
#define TTC_INTERRUPT_INIT_interrupt NULL
#endif

//} Defines
//{ Structures/ Enums  ***************************************************

// makes isr() definitions more readable
#define physical_index_t Base_t

typedef enum {   // ttc_interrupt_errorcode_e
    tine_OK,                // =0: no error
    tine_TimeOut,           // timeout occured in called function
    tine_IrqSourceNotFound, // adressed interrupt source not available in current uC
    tine_NotImplemented,    // function has no implementation for current architecture
    tine_InvalidArgument,   // general argument error

    tine_UnknownError
} ttc_interrupt_errorcode_e;

typedef enum { // ttc_interrupt_type_e (not all sources listed here may be supported by current architecture driver)
    tit_None,

    // Note: do not change order of or insert addionalls into following lines!

    tit_First_GPIO,   //{ interrupt type for general purpose IO

    // interrupt types supported for GPIO pins
    // Note: PhysicalIndex = ttc_gpio_create_index8(GPIOx, Pin)
    // Note: Don't forget to configure your port pin as input!
    tit_GPIO_Falling,            // voltage on input pin falls from logical 1 to 0
    tit_GPIO_Rising,             // voltage on input pin rises from logical 0 to 1
    tit_GPIO_Rising_Falling,     // voltage on input pin falls or rises

    tit_Last_GPIO, //}
    tit_First_I2C,    //{ interrupt types for I2C-bus

    tit_Last_I2C, //}
    tit_First_TIMER,  //{ interrupt types for timers

    tit_TIMER_Updating,         // Hardware Timer expired, update needed
    tit_TIMER_CC1,              // Channel 1 Capture/Compare Flag
    tit_TIMER_CC2,              // Channel 2 Capture/Compare Flag
    tit_TIMER_CC3,              // Channel 3 Capture/Compare Flag
    tit_TIMER_CC4,              // Channel 4 Capture/Compare Flag
    tit_TIMER_Triggering,       // Trigger Interrupt Flag
    tit_TIMER_Break,            // Hardware break required

    tit_Last_TIMER, //}
    tit_First_RADIO,  //{ interrupt types for external or internal radio transceivers

    tit_Last_RADIO, //}
    tit_First_SPI,   //{ interrupt types for SPI bus

    tit_Last_SPI, //}
    tit_First_USART, //{ interrupt types for universal serial asynchronous receiver transmitter

    tit_USART_Cts,               // Cts signal has been received
    tit_USART_Error,             // error condition has arised (detailed data is handed to error handler)
    tit_USART_Idle,              // deivce has gone idle
    tit_USART_TxComplete,        // transfer completed
    tit_USART_TransmitDataEmpty, // transmit buffer ist empty ( bytes sent)
    tit_USART_RxNE,              // receive buffer not empty  ( >= 1 byte received)
    tit_USART_LinBreak,          // break condition detected on LIN bus

    tit_Last_USART, //}

    // additional types of sources go here...

    tit_Error
} ttc_interrupt_type_e;

typedef union {
    u8_t U8;
    struct {
        unsigned Bank : 4; // index of GPIO-bank to use (0 = GPIOA, ...)
        unsigned Pin  : 4; // index of GPIO-pin to use (0 = Pin1, ...)
    } B;
} __attribute__((__packed__)) ttc_interrupt_gpio_index_t;

#ifdef EXTENSION_500_ttc_usart
typedef struct { // ttc_usart_isrs_t - pointers of interrupt service routines (-> STM32_RM0008 p. 788)

    // interrupt service routines being called
    void (*isr_ClearToSend)(physical_index_t,              ttc_usart_config_t*);
    void (*isr_Error)(physical_index_t, ttc_interrupt_usart_errors_t, ttc_usart_config_t*);
    void (*isr_IdleLine)(physical_index_t,                 ttc_usart_config_t*);
    void (*isr_TransmitDataEmpty)(physical_index_t,        ttc_usart_config_t*);
    void (*isr_ReceiveDataNotEmpty)(physical_index_t,      ttc_usart_config_t*);
    void (*isr_TransmissionComplete)(physical_index_t,     ttc_usart_config_t*);
    void (*isr_LinBreakDetected)(physical_index_t,         ttc_usart_config_t*);

    // arguments passed to isr_XXX()
    ttc_usart_config_t* Argument_ClearToSend;
    ttc_usart_config_t* Argument_Error;
    ttc_usart_config_t* Argument_IdleLine;
    ttc_usart_config_t* Argument_TransmitDataEmpty;
    ttc_usart_config_t* Argument_ReceiveDataNotEmpty;
    ttc_usart_config_t* Argument_TransmissionComplete;
    ttc_usart_config_t* Argument_LinBreakDetected;

#ifdef _driver_usart_t
    // low-level architecture dependent interrupt configuration
    _driver_usart_t LowLevel;
#endif
} ttc_usart_isrs_t;
#endif

#ifdef EXTENSION_500_ttc_gpio
typedef struct { // ttc_gpio_isrs_t - configuration data of single external interrupt line (-> STM32_RM0008 p. 201)

    // interrupt service routine being called
    void (*isr)(physical_index_t, void*);

    // argument passed to isr()
    void* Argument;

    // physical index given to stm32_interrupt_init()
    physical_index_t PhysicalIndex;

#ifdef _driver_gpio_t
    // low-level architecture dependent interrupt configuration
    _driver_gpio_t LowLevel;
#endif
} ttc_gpio_isrs_t;
#endif
#ifdef EXTENSION_500_ttc_timer
typedef struct { // ttc_timer_isrs_t - configuration data of single external interrupt line (-> STM32_RM0008 p. 201)

    // interrupt service routine being called
    void (*isr)(physical_index_t, void*);

    // argument passed to isr()
    void* Argument;

    // physical index given to stm32_interrupt_init()
    physical_index_t PhysicalIndex;

#ifdef _driver_timer_t
    // low-level architecture dependent interrupt configuration
    _driver_timer_t LowLevel;
#endif
} ttc_timer_isrs_t;
#endif
//}

#endif // TTC_INTERRUPT_TYPES_H
