#ifndef TTC_ACCELEROMETER_H
#define TTC_ACCELEROMETER_H
/** { ttc_accelerometer.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for ACCELEROMETER devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_ACCELEROMETER(tc_accelerometer_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_accelerometer_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_accelerometer_init(LogicalIndex);
 *  4) use:         ttc_accelerometer_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level accelerometer and application.
 *
 *  Created from template ttc_device.h revision 30 at 20141020 08:58:47 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef EXTENSION_ttc_accelerometer
#error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_accelerometer.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_accelerometer.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_accelerometer_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level accelerometer only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * accelerometer devices on all supported architectures.
 * Check accelerometer/accelerometer_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares accelerometer Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_accelerometer_prepare();
void _driver_accelerometer_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_accelerometer_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_ACCELEROMETER_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_accelerometer_config* ttc_accelerometer_get_configuration( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_ACCELEROMETER_get_max_LogicalIndex())
 * @return                == 0: accelerometer device has been reset successfully; != 0: error-code
 */
e_ttc_accelerometer_errorcode ttc_accelerometer_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_ACCELEROMETER_get_max_LogicalIndex())
 */
void ttc_accelerometer_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Seetings of indexed device are being reset to their default values.
 * Will automatically call ttc_accelerometer_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of accelerometer device (1..ttc_accelerometer_get_max_index() )
 * @return                == 0: default values have been loaded successfully for indexed accelerometer device; != 0: error-code
 */
e_ttc_accelerometer_errorcode  ttc_accelerometer_load_defaults( t_u8 LogicalIndex );

/** manually read latest measures from indexed accelerometer device
 *
 * @param LogicalIndex  logical index of accelerometer device (1..ttc_accelerometer_get_max_index() )
 * @return              !=NULL: pointer to measures being read; ==NULL: measures could NOT be read
 */
t_ttc_accelerometer_measures* ttc_accelerometer_read_measures( t_u8 LogicalIndex );
t_ttc_accelerometer_measures* _driver_accelerometer_read_measures( t_ttc_accelerometer_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_ACCELEROMETER_get_max_LogicalIndex())
 * @return                == 0: indexed accelerometer device has been reset successfully; != 0: error-code
 */
void ttc_accelerometer_reset( t_u8 LogicalIndex );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_accelerometer(t_u8 LogicalIndex)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_accelerometer_ are passed to interfaces/ttc_accelerometer_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl accelerometer UPDATE
 */


/** fills out given Config with maximum valid values for indexed ACCELEROMETER
 * @param Config        = pointer to struct t_ttc_accelerometer_config (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_accelerometer_errorcode _driver_accelerometer_get_features( t_ttc_accelerometer_config* Config );

/** shutdown single ACCELEROMETER unit device
 * @param Config        pointer to struct t_ttc_accelerometer_config (must have valid value for PhysicalIndex)
 * @return              == 0: ACCELEROMETER has been shutdown successfully; != 0: error-code
 */
e_ttc_accelerometer_errorcode _driver_accelerometer_deinit( t_ttc_accelerometer_config* Config );

/** initializes single ACCELEROMETER unit for operation
 * @param Config        pointer to struct t_ttc_accelerometer_config (must have valid value for PhysicalIndex)
 * @return              == 0: ACCELEROMETER has been initialized successfully; != 0: error-code
 */
e_ttc_accelerometer_errorcode _driver_accelerometer_init( t_ttc_accelerometer_config* Config );

/** loads configuration of indexed ACCELEROMETER unit with default values
 * @param Config        pointer to struct t_ttc_accelerometer_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_accelerometer_errorcode _driver_accelerometer_load_defaults( t_ttc_accelerometer_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_accelerometer_config (must have valid value for PhysicalIndex)
 */
void _driver_accelerometer_reset( t_ttc_accelerometer_config* Config );
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_ACCELEROMETER_H
