/** { ttc_interrupt.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for interrupt devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 23 at 20140224 21:37:20 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_interrupt.h"
#include "ttc_heap.h"

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of interrupt devices.
 *
 */

// Note: Dynamic Arrays must be reset in ttc_interrupt_interface_prepare()
#ifdef EXTENSION_ttc_can
    A_dynamic( t_ttc_interrupt_config_can*,   ttc_interrupt_config_can );
#endif
#ifdef EXTENSION_ttc_gpio
    A_dynamic( t_ttc_interrupt_config_gpio*,  ttc_interrupt_config_gpio );
#endif
#ifdef EXTENSION_ttc_i2c
    A_dynamic( t_ttc_interrupt_config_i2c*,   ttc_interrupt_config_i2c );
#endif
#ifdef EXTENSION_ttc_spi
    A_dynamic( t_ttc_interrupt_config_spi*,   ttc_interrupt_config_spi );
#endif
#ifdef EXTENSION_ttc_timer
    A_dynamic( t_ttc_interrupt_configimer_t*, t_ttc_interrupt_configimer );
#endif
#ifdef EXTENSION_ttc_usart
    A_dynamic( t_ttc_interrupt_config_usart*, ttc_interrupt_config_usart );
#endif
#ifdef EXTENSION_ttc_rtc
    A_dynamic( t_ttc_interrupt_config_rtc*, ttc_interrupt_config_rtc );
#endif

#include "ttc_list.h"

t_u8 ttc_interrupt_CriticalSectionCounter = 0;
#ifndef TTC_INTERRUPT_CRITICAL_DEBUG
    #define TTC_INTERRUPT_CRITICAL_DEBUG 1
#endif

#if (TTC_INTERRUPT_CRITICAL_DEBUG == 1)
    // activate certain meta data recording to aid debugging

    // stores caller of ttc_interrupt_critical_begin() that disabled interrupts
    volatile void ( *ttc_interrupt_critical_FirstCaller )() = NULL;

    // counts every ttc_interrupt_critical_begin() call
    volatile t_u32 ttc_interrupt_critical_CallCounter = 0;

    // counts every ttc_interrupt_critical_begin() call that begins a new critical section
    volatile t_u32 ttc_interrupt_critical_BeginCounter = 0;

    // The debug numbers below can be changed during a debug session to proceed
    // to individual function calls over hundreds of calls.

    // Set to number of generic ttc_interrupt_critical_begin() function call to debug
    volatile t_u32 ttc_interrupt_critical_CallCounter_Debug = 0;

    // Set to number of ttc_interrupt_critical_begin() function call with ttc_interrupt_CriticalSectionCounter==0
    volatile t_u32 ttc_interrupt_critical_BeginCounter_Debug = -1;

#endif

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

void ttc_interrupt_prepare() {

    // Note: We do not deallocate memory. Make sure, that this function is called after a reset of the memory management!

    Assert_INTERRUPT_EXTRA( !ttc_interrupt_check_all_disabled(), ttc_assert_origin_auto ); // interrupts must be enabled when first calling this function. Check implementation of caller and previous *_prepare() functions!

    ttc_interrupt_all_disable_gdb();
    Assert_INTERRUPT_EXTRA( ttc_interrupt_check_all_disabled(), ttc_assert_origin_auto );  // interrupts are disabled now but the test did not show this. Check low-level implementation of ttc_interrupt_check_all_disabled()!

#ifdef EXTENSION_ttc_usart
    A_dynamic_reset( ttc_interrupt_config_usart );
#endif

#ifdef EXTENSION_ttc_rtc
    A_dynamic_reset( ttc_interrupt_config_rtc );
#endif

#ifdef EXTENSION_ttc_gpio
    A_dynamic_reset( ttc_interrupt_config_gpio );
#endif
#ifdef EXTENSION_ttc_timer
    A_dynamic_reset( t_ttc_interrupt_configimer );
#endif
#ifdef EXTENSION_ttc_can
    A_dynamic_reset( ttc_interrupt_config_can );
#endif
    ttc_interrupt_interface_prepare();

    ttc_interrupt_all_enable_gdb();
}
t_ttc_interrupt_handle ttc_interrupt_init( e_ttc_interrupt_type Type, t_physical_index PhysicalIndex, void ( *ISR )( t_physical_index, void* ), void* Argument, void ( **OldISR )( t_physical_index, void* ), void** OldArgument ) {
    // Important: enums must checked in order from high to low!

    // Highest enum

#ifdef EXTENSION_ttc_rtc
    if ( Type > tit_First_RTC )
    { return _ttc_interrupt_init_rtc( Type, PhysicalIndex, ISR, Argument, OldISR, OldArgument ); }
#endif

#ifdef EXTENSION_ttc_can
    if ( Type > tit_First_CAN )
    { return _ttc_interrupt_init_can( Type, PhysicalIndex, ISR, Argument, OldISR, OldArgument ); }
#endif
#ifdef EXTENSION_ttc_usart
    if ( Type > tit_First_USART )
    { return _ttc_interrupt_init_usart( Type, PhysicalIndex, ISR, Argument, OldISR, OldArgument ); }
#endif
#ifdef EXTENSION_ttc_spi
    if ( Type > tit_First_SPI )
    { return NULL; }
#endif
#ifdef EXTENSION_ttc_timer
    if ( Type > tit_First_TIMER )
    { return _ttc_interrupt_init_timer( Type, PhysicalIndex, ISR, Argument, OldISR, OldArgument ); }
#endif
#ifdef EXTENSION_ttc_i2c
    if ( Type > tit_First_I2C )
    { return ( t_ttc_interrupt_handle ) ec_interrupt_NotImplemented; }
#endif
#ifdef EXTENSION_ttc_gpio
    if ( Type > tit_First_GPIO )
    { return _ttc_interrupt_init_gpio( Type, PhysicalIndex, ISR, Argument, OldISR, OldArgument ); }
#endif

    // Lowest enum

    Assert_INTERRUPT( 0, ttc_assert_origin_auto );  // unknown Type given!
    return NULL;    // just to make compiler happy
}

#ifdef EXTENSION_ttc_usart //{

/** finds list entry with matching type in single linked list of usart interrupt sources
 *
 * @param FirstItem  start of single linked list of usart interrupt sources
 * @param Type       type of desired entry
 * @return           ==NULL: no item with matching type found; !=NULL: matching item
 */
t_ttc_interrupt_source_usart* _ttc_interrupt_usart_find_source( t_ttc_interrupt_source_usart* FirstItem, e_ttc_interrupt_type Type ) {

    while ( FirstItem ) {
        if ( FirstItem->Config.Type != Type )
        { FirstItem = FirstItem->Next; }
    }

    return FirstItem;
}
t_ttc_interrupt_handle        _ttc_interrupt_init_usart( e_ttc_interrupt_type Type, t_physical_index PhysicalIndex, void ( *ISR )( t_physical_index, void* ), void* Argument, void ( **OldISR )( t_physical_index, void* ), void** OldArgument ) {
    if ( A_is_null( ttc_interrupt_config_usart ) ) { // first init of an USART-irq: create list of pointers
        A_allocate( ttc_interrupt_config_usart, TTC_INTERRUPT_USART_AMOUNT );
    }
    if ( A( ttc_interrupt_config_usart, PhysicalIndex ) == NULL ) { // first init of irq for USART of this index: create empty pointer table
        A( ttc_interrupt_config_usart, PhysicalIndex ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_interrupt_config_usart ) );
    }

    void ( **Entry_ISR )( t_physical_index, void* ) = NULL;
    t_ttc_usart_config** Entry_Argument = NULL;
    t_ttc_interrupt_config_usart* Entry = A( ttc_interrupt_config_usart, PhysicalIndex );
    Entry->PhysicalIndex        = ( e_ttc_gpio_pin ) PhysicalIndex;
    t_ttc_interrupt_source_usart* InterruptSource = _ttc_interrupt_usart_find_source( Entry->Sources, Type );
    if ( !InterruptSource ) { // new interrupt source: create entry
        InterruptSource = ttc_heap_alloc( sizeof( t_ttc_interrupt_source_usart ) );

        InterruptSource->Config.Type        = Type;
        InterruptSource->Config.ConfigIndex = PhysicalIndex;

        // make this item first in list of interrupt sources
        InterruptSource->Next = Entry->Sources;
        Entry->Sources = InterruptSource;
    }

    switch ( Type ) { // find memory location where to store function pointer + argument

        case tit_USART_Cts:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_ClearToSend );
            Entry_Argument = &( Entry->Argument_ClearToSend );
            break;
        case tit_USART_Idle:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_IdleLine );
            Entry_Argument = &( Entry->Argument_IdleLine );
            break;
        case tit_USART_TxComplete:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_TransmissionComplete );
            Entry_Argument = &( Entry->Argument_TransmissionComplete );
            break;
        case tit_USART_TransmitDataEmpty:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_TransmitDataEmpty );
            Entry_Argument = &( Entry->Argument_TransmitDataEmpty );
            break;
        case tit_USART_RxNE:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_ReceiveDataNotEmpty );
            Entry_Argument = &( Entry->Argument_ReceiveDataNotEmpty );
            break;
        case tit_USART_LinBreak:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_LinBreakDetected );
            Entry_Argument = &( Entry->Argument_LinBreakDetected );
            break;
        case tit_USART_Error:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_Error );
            Entry_Argument = &( Entry->Argument_Error );
            break;
        default: break;
    }

    if ( OldISR != NULL )      // save old value of function ptr
    { *OldISR = *Entry_ISR; }
    if ( OldArgument != NULL ) // save old argument
    { *OldArgument = ( void* ) *Entry_Argument; }

    *Entry_ISR = ISR;  // establish new isr
    Assert_INTERRUPT( Entry_Argument != NULL, ttc_assert_origin_auto );
    *Entry_Argument = ( t_ttc_usart_config* ) Argument;

    if ( _driver_interrupt_init_usart( Entry, Type ) == ec_interrupt_OK )
    { return InterruptSource; }
    return NULL;
}
void                          ttc_interrupt_enable_usart( t_ttc_interrupt_handle Handle ) {
    Assert_INTERRUPT_Writable( Handle, ttc_assert_origin_auto );

    // derive USART configuration and interrupt source type from pointer
    t_ttc_interrupt_source_usart* InterruptSource = ( t_ttc_interrupt_source_usart* ) Handle;
    t_ttc_interrupt_config_usart* Config =  A( ttc_interrupt_config_usart, InterruptSource->Config.ConfigIndex );

    _driver_interrupt_enable_usart( Config, InterruptSource->Config.Type );
}
void                          ttc_interrupt_disable_usart( t_ttc_interrupt_handle Handle ) {
    Assert_INTERRUPT_Writable( Handle, ttc_assert_origin_auto );

    // derive USART configuration and interrupt source type from pointer
    t_ttc_interrupt_source_usart* InterruptSource = ( t_ttc_interrupt_source_usart* ) Handle;
    t_ttc_interrupt_config_usart* Config =  A( ttc_interrupt_config_usart, InterruptSource->Config.ConfigIndex );

    _driver_interrupt_disable_usart( Config, InterruptSource->Config.Type );
}

#endif //}
#ifdef EXTENSION_ttc_gpio  //{

t_ttc_interrupt_handle _ttc_interrupt_init_gpio( e_ttc_interrupt_type Type, t_physical_index PhysicalIndex, void ( *ISR )( t_physical_index, void* ), void* Argument, void ( **OldISR )( t_physical_index, void* ), void** OldArgument ) {

    if ( A_is_null( ttc_interrupt_config_gpio ) ) { // first init of a gpio-irq: create list of pointers
        A_allocate( ttc_interrupt_config_gpio, TTC_INTERRUPT_GPIO_AMOUNT ); // one entry for each interrupt line
    }
    t_u8 ConfigIndex = _driver_interrupt_gpio_calc_config_index( PhysicalIndex );
    if ( A( ttc_interrupt_config_gpio, ConfigIndex ) == NULL ) {       // first init of irq for gpio of this index: create emptx pointer table
        A( ttc_interrupt_config_gpio, ConfigIndex ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_interrupt_config_gpio ) );
    }

    t_ttc_interrupt_config_gpio* ConfigEntry = A( ttc_interrupt_config_gpio, ConfigIndex );
    Assert_INTERRUPT_Writable( ConfigEntry, ttc_assert_origin_auto );  // why is it empty?

    // save previous entries
    if ( OldISR )      { *OldISR      = ConfigEntry->isr; }
    if ( OldArgument ) { *OldArgument = ConfigEntry->Argument; }

    // load new entries
    ConfigEntry->isr                  = ISR;
    ConfigEntry->Argument             = Argument;
    ConfigEntry->PhysicalIndex        = ( e_ttc_gpio_pin ) PhysicalIndex;
    ConfigEntry->Common.ConfigIndex   = ConfigIndex;
    ConfigEntry->Common.Type          = Type;

    if ( ISR ) { // interrupt service routine given: initialize interrupt
        if ( _driver_interrupt_init_gpio( ConfigEntry ) == ec_interrupt_OK )
        { return ConfigEntry; }
    }
    else
    { _driver_interrupt_deinit_gpio( ConfigEntry ); }
    return 0;
}
void                   ttc_interrupt_enable_gpio( t_ttc_interrupt_handle Handle ) {
    Assert_INTERRUPT_Writable( Handle, ttc_assert_origin_auto );
    _driver_interrupt_enable_gpio( ( t_ttc_interrupt_config_gpio* ) Handle );
}
void                   ttc_interrupt_disable_gpio( t_ttc_interrupt_handle Handle ) {
    Assert_INTERRUPT_Writable( Handle, ttc_assert_origin_auto );
    _driver_interrupt_enable_gpio( ( t_ttc_interrupt_config_gpio* ) Handle );
}
void                   ttc_interrupt_generate_gpio( t_ttc_interrupt_handle Handle ) {
    Assert_INTERRUPT_Writable( Handle, ttc_assert_origin_auto );

    _driver_interrupt_generate_gpio( ( t_ttc_interrupt_config_gpio* )  Handle );
}
#endif //}
#ifdef EXTENSION_ttc_timer //{
t_ttc_interrupt_handle _ttc_interrupt_init_timer( e_ttc_interrupt_type Type, t_physical_index PhysicalIndex, void ( *ISR )( t_physical_index, void* ), void* Argument, void ( **OldISR )( t_physical_index, void* ), void** OldArgument ) {

    if ( A_is_null( t_ttc_interrupt_configimer ) ) { // first init of a timer-irq: create list of pointers
        A_allocate( t_ttc_interrupt_configimer, TTC_INTERRUPT_TIMER_AMOUNT ); // CortexM3 provides 16 lines for external gpio-interrupts
    }
    if ( A( t_ttc_interrupt_configimer, PhysicalIndex ) == NULL ) {  // first init of irq for timer of this index: create emptx pointer table

        A( t_ttc_interrupt_configimer, PhysicalIndex ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_interrupt_configimer_t ) );
    }

    void ( **Entry_ISR )( t_physical_index, void* ) = NULL;
    t_ttc_timer_config** Entry_Argument = NULL;
    t_ttc_interrupt_configimer_t* Entry = A( t_ttc_interrupt_configimer, PhysicalIndex );
    if ( Entry->PhysicalIndex != PhysicalIndex )
    { Entry->PhysicalIndex = PhysicalIndex; }

    switch ( Type ) { // find memory location where to store function pointer + argument
        case tit_TIMER_Updating:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_Update );
            Entry_Argument = &( Entry->Argument_Update );
            if ( !Entry->Common.Type )
            { Entry->Common.Type = tit_TIMER_Updating; }
            break;
        case tit_TIMER_CC1:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_Update );
            Entry_Argument = &( Entry->Argument_Update );
            Entry->Common.Type = tit_TIMER_CC1;
            break;
        case tit_TIMER_CC2:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_Update );
            Entry_Argument = &( Entry->Argument_Update );
            Entry->Common.Type = tit_TIMER_CC2;
            break;
        case tit_TIMER_CC3:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_Update );
            Entry_Argument = &( Entry->Argument_Update );
            Entry->Common.Type = tit_TIMER_CC3;
            break;
        case tit_TIMER_CC4:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_Update );
            Entry_Argument = &( Entry->Argument_Update );
            Entry->Common.Type = tit_TIMER_CC4;
            break;
        case tit_TIMER_Triggering:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_Update );
            Entry_Argument = &( Entry->Argument_Update );
            Entry->Common.Type = tit_TIMER_Triggering;
            break;

        default:
            ttc_assert_halt( );
            break;
    }


    if ( OldISR != NULL )      // save old value of function ptr
    { *OldISR = *Entry_ISR; }
    if ( OldArgument != NULL ) // save old argument
    { *OldArgument = ( void* ) *Entry_Argument; }

    *Entry_ISR = ISR;  // establish new isr
    Assert_INTERRUPT_Writable( Entry_Argument, ttc_assert_origin_auto );  // could not find valid pointer. Check implementation above!
    *Entry_Argument = ( t_ttc_timer_config* ) Argument;

    if ( _driver_interrupt_init_timer( Entry ) == ec_interrupt_OK )
    { return Entry; }
    return 0;
}
void ttc_interrupt_enable_timer( t_ttc_interrupt_handle Handle ) {
    Assert_INTERRUPT_Writable( Handle, ttc_assert_origin_auto );
    _driver_interrupt_enable_timer( ( t_ttc_interrupt_configimer_t* ) Handle );
}
void ttc_interrupt_disable_timer( t_ttc_interrupt_handle Handle ) {
    Assert_INTERRUPT_Writable( Handle, ttc_assert_origin_auto );
    _driver_interrupt_enable_timer( ( t_ttc_interrupt_configimer_t* ) Handle );
}

#endif //}
#ifdef EXTENSION_ttc_can   //{

t_ttc_interrupt_handle _ttc_interrupt_init_can( e_ttc_interrupt_type Type, t_physical_index PhysicalIndex, void ( *ISR )( t_physical_index, void* ), void* Argument, void ( **OldISR )( t_physical_index, void* ), void** OldArgument ) {

    if ( A_is_null( ttc_interrupt_config_can ) ) { // first init of an gpio-irq: create list of pointers
        A_allocate( ttc_interrupt_config_can, TTC_INTERRUPT_CAN_AMOUNT ); // CortexM3 provides 16 lines for external gpio-interrupts
    }

    if ( A( ttc_interrupt_config_can, PhysicalIndex ) == NULL ) {  // first init of irq for timer of this index: create emptx pointer table

        A( ttc_interrupt_config_can, PhysicalIndex ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_interrupt_config_can ) );
    }

    void ( **Entry_ISR )( t_physical_index, void* ) = NULL;
    t_ttc_can_config** Entry_Argument = NULL;
    t_ttc_interrupt_config_can* ConfigEntry = A( ttc_interrupt_config_can, PhysicalIndex );
    ConfigEntry->PhysicalIndex = PhysicalIndex;
    ConfigEntry->Common.ConfigIndex   = PhysicalIndex;
    ConfigEntry->Common.Type          = Type;

    switch ( Type ) { // find memory location where to store function pointer + argument

        case tit_CAN_Transmit_Empty:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( ConfigEntry->isr_Transmit );
            Entry_Argument = &( ConfigEntry->Argument_Transmit );
            break;
        case tit_CAN_FIFO0_Pending:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( ConfigEntry->isr_FIFO0_Pending );
            Entry_Argument = &( ConfigEntry->Argument_FIFO0_Pending );
            break;
        case tit_CAN_FIFO0_Full:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( ConfigEntry->isr_FIFO0_Full );
            Entry_Argument = &( ConfigEntry->Argument_FIFO0_Full );
            break;
        case tit_CAN_FIFO0_Overrun:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( ConfigEntry->isr_FIFO0_Overrun );
            Entry_Argument = &( ConfigEntry->Argument_FIFO0_Overrun );
            break;
        case tit_CAN_FIFO1_Pending:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( ConfigEntry->isr_FIFO1_Pending );
            Entry_Argument = &( ConfigEntry->Argument_FIFO1_Pending );
            break;
        case tit_CAN_FIFO1_Full:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( ConfigEntry->isr_FIFO1_Full );
            Entry_Argument = &( ConfigEntry->Argument_FIFO1_Full );
            break;
        case tit_CAN_FIFO1_Overrun:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( ConfigEntry->isr_FIFO1_Overrun );
            Entry_Argument = &( ConfigEntry->Argument_FIFO1_Overrun );
            break;
        case tit_CAN_Error_Warning:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( ConfigEntry->isr_Error_Warning );
            Entry_Argument = &( ConfigEntry->Argument_Error_Warning );
            break;
        case tit_CAN_Error_Passive:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( ConfigEntry->isr_Error_Passive );
            Entry_Argument = &( ConfigEntry->Argument_Error_Passive );
            break;
        case tit_CAN_Bus_Off:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( ConfigEntry->isr_Bus_Off );
            Entry_Argument = &( ConfigEntry->Argument_Bus_Off );
            break;
        case tit_CAN_WakeUp_State:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( ConfigEntry->isr_WakeUp_State );
            Entry_Argument = &( ConfigEntry->Argument_WakeUp_State );
            break;
        case tit_CAN_Sleep_State:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( ConfigEntry->isr_Sleep_State );
            Entry_Argument = &( ConfigEntry->Argument_Sleep_State );
            break;
        default: break;
    }

    if ( OldISR != NULL )      // save old value of function ptr
    { *OldISR = *Entry_ISR; }
    if ( OldArgument != NULL ) // save old argument
    { *OldArgument = ( void* ) *Entry_Argument; }

    *Entry_ISR = ISR;  // establish new isr
    Assert_INTERRUPT( Entry_Argument, ttc_assert_origin_auto );
    *Entry_Argument = ( t_ttc_can_config* ) Argument;

    if ( _driver_interrupt_init_can( ConfigEntry ) == ec_interrupt_OK )
    { return ConfigEntry; }
    else
    { _driver_interrupt_deinit_can( ConfigEntry ); }
    return 0;
}
void ttc_interrupt_enable_can( t_ttc_interrupt_handle Handle ) {
    Assert_INTERRUPT_Writable( Handle, ttc_assert_origin_auto );
    _driver_interrupt_enable_can( ( t_ttc_interrupt_config_can* ) Handle );
}
void ttc_interrupt_disable_can( t_ttc_interrupt_handle Handle ) {
    Assert_INTERRUPT_Writable( Handle, ttc_assert_origin_auto );
    _driver_interrupt_enable_can( ( t_ttc_interrupt_config_can* ) Handle );
}

#endif //}

#ifdef EXTENSION_ttc_rtc //{

t_ttc_interrupt_handle _ttc_interrupt_init_rtc( e_ttc_interrupt_type Type, t_physical_index PhysicalIndex, void ( *ISR )( t_physical_index, void* ), void* Argument, void ( **OldISR )( t_physical_index, void* ), void** OldArgument ) {

    if ( A_is_null( ttc_interrupt_config_rtc ) ) { // first init of an rtc-irq: create list of pointers
        A_allocate( ttc_interrupt_config_rtc, TTC_INTERRUPT_RTC_AMOUNT );
    }
    if ( A( ttc_interrupt_config_rtc, PhysicalIndex ) == NULL ) { // first init of irq for rtc of this index: create emptx pointer table
        A( ttc_interrupt_config_rtc, PhysicalIndex ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_interrupt_config_rtc ) );

        t_ttc_interrupt_config_rtc* Entry = A( ttc_interrupt_config_rtc, PhysicalIndex );
        ttc_memory_set( &Entry->Common, 0, sizeof( Entry->Common ) );

    }

    void ( **Entry_ISR )( t_physical_index, void* ) = NULL;
    t_ttc_rtc_config** Entry_Argument = NULL;
    t_ttc_interrupt_config_rtc* Entry = A( ttc_interrupt_config_rtc, PhysicalIndex );
    Entry->PhysicalIndex        = ( e_ttc_gpio_pin ) PhysicalIndex;
    Entry->Common.Type = Type;

    switch ( Type ) { // find memory location where to store function pointer + argument

        case tit_RTC_AlarmA:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_AlarmA );
            Entry_Argument = &( Entry->Argument_AlarmA );
            break;
        case tit_RTC_AlarmB:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_AlarmB );
            Entry_Argument = &( Entry->Argument_AlarmB );
            break;
        case tit_RTC_Tamper:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_Tamper );
            Entry_Argument = &( Entry->Argument_Tamper );
            break;
        case tit_RTC_TimeStamp:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_TimeStamp );
            Entry_Argument = &( Entry->Argument_TimeStamp );
            break;
        case tit_RTC_WakeUP:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_Wakeup );
            Entry_Argument = &( Entry->Argument_Wakeup );
            break;
        case tit_RTC_Pin_line_wakeup:
            Entry_ISR = ( void( ** )( t_physical_index, void* ) ) & ( Entry->isr_Wakeup );
            Entry_Argument = &( Entry->Argument_Wakeup );
            break;
        default: break;
    }

    if ( OldISR != NULL )      // save old value of function ptr
    { *OldISR = *Entry_ISR; }
    if ( OldArgument != NULL ) // save old argument
    { *OldArgument = ( void* ) *Entry_Argument; }

    *Entry_ISR = ISR;  // establish new isr
    Assert_INTERRUPT_Writable( Entry_Argument, ttc_assert_origin_auto );
    *Entry_Argument = ( t_ttc_rtc_config* ) Argument;

    if ( _driver_interrupt_init_rtc( Entry ) == ec_interrupt_OK )
    { return Entry; }
    return 0;
}

void ttc_interrupt_enable_rtc( t_ttc_interrupt_handle Handle ) {
    Assert_INTERRUPT_Writable( Handle, ttc_assert_origin_auto );
    _driver_interrupt_enable_rtc( ( t_ttc_interrupt_config_rtc* ) Handle );
}
void ttc_interrupt_disable_rtc( t_ttc_interrupt_handle Handle ) {
    Assert_INTERRUPT_Writable( Handle, ttc_assert_origin_auto );
    _driver_interrupt_enable_rtc( ( t_ttc_interrupt_config_rtc* ) Handle );
}

#endif //}

void __attribute__( ( noinline ) ) ttc_interrupt_critical_begin() {

    // disable interrupts first to ensure that no interruption occures while increasing CriticalSectionCounter
    ttc_interrupt_all_disable();

#if (TTC_INTERRUPT_CRITICAL_DEBUG == 1)
    ttc_interrupt_critical_CallCounter++;

    if ( ttc_interrupt_critical_CallCounter == ttc_interrupt_critical_CallCounter_Debug )
    { ttc_interrupt_critical_CallCounter += 0; } // place breakpoint here to find caller of specific function call

    if ( !ttc_interrupt_CriticalSectionCounter ) { // beginning a new critical section
        ttc_interrupt_critical_FirstCaller = __builtin_return_address( 0 );

        if ( ttc_interrupt_critical_BeginCounter == ttc_interrupt_critical_BeginCounter_Debug )
        { ttc_interrupt_critical_BeginCounter += 0; } // place breakpoint here to find caller of specific function call
    }
#endif

    Assert_INTERRUPT( ttc_interrupt_CriticalSectionCounter < 255, ttc_assert_origin_auto ); // critical section counter is about to overflow. Do you really want to create so much critical sections?
    ttc_interrupt_CriticalSectionCounter++;
}
void __attribute__( ( noinline ) ) ttc_interrupt_critical_end() {

    Assert_INTERRUPT( ttc_interrupt_CriticalSectionCounter > 0, ttc_assert_origin_auto );  // _end() was called more often than _begin()!
    ttc_interrupt_CriticalSectionCounter--;

    if ( ttc_interrupt_CriticalSectionCounter == 0 )
    { ttc_interrupt_all_enable(); }
}
t_u8 __attribute__( ( noinline ) ) ttc_interrupt_critical_end_all() {

    t_u8 PreviousCriticalSectionCounter = ttc_interrupt_CriticalSectionCounter;
    ttc_interrupt_CriticalSectionCounter = 0;

    ttc_interrupt_all_enable();

    return PreviousCriticalSectionCounter;
}
void __attribute__( ( noinline ) ) ttc_interrupt_critical_restore( t_u8 Level ) {
    // disable interrupts first to ensure that no interruption occures while increasing CriticalSectionCounter
    ttc_interrupt_all_disable();

    Assert_INTERRUPT( Level > 0, ttc_assert_origin_auto ); // it makes no sense to restore Level==0. Check Level before calling this function!
    Assert_INTERRUPT( ttc_interrupt_CriticalSectionCounter == 0, ttc_assert_origin_auto );  // This function is typically only called after calling ttc_interrupt_critical_end_all() and interrupts should be enabled. It seems that someone meanwhile called ttc_interrupt_critical_begin(). Check your implementation or insert/move ttc_interrupt_critical_end() calls!

#if (TTC_INTERRUPT_CRITICAL_DEBUG == 1)
    ttc_interrupt_critical_CallCounter++;

    if ( ttc_interrupt_critical_CallCounter == ttc_interrupt_critical_CallCounter_Debug )
    { ttc_interrupt_critical_CallCounter += 0; } // place breakpoint here to find caller of specific function call

    if ( !ttc_interrupt_CriticalSectionCounter ) { // beginning a new critical section
        ttc_interrupt_critical_FirstCaller = __builtin_return_address( 0 );

        if ( ttc_interrupt_critical_BeginCounter == ttc_interrupt_critical_BeginCounter_Debug )
        { ttc_interrupt_critical_BeginCounter += 0; } // place breakpoint here to find caller of specific function call
    }
#endif

    Assert_INTERRUPT( ttc_interrupt_CriticalSectionCounter < 255, ttc_assert_origin_auto ); // critical section counter is about to overflow. Do you really want to create so much critical sections?
    ttc_interrupt_CriticalSectionCounter = Level;
}

#ifdef EXTENSION_300_scheduler_freertos
    extern t_u32 uxCriticalNesting;
#endif

t_u8 ttc_interrupt_critical_level() {
#ifdef EXTENSION_300_scheduler_freertos
    TODO( "provide an extra low-level driver for Multitasking Scheduler!" )
    if ( uxCriticalNesting == 0xaaaaaaaa )
    { return 0; }
    return uxCriticalNesting;
#else
    return ttc_interrupt_CriticalSectionCounter;
#endif
}
void __attribute__( ( noinline ) ) ttc_interrupt_all_disable_gdb() {

    _driver_interrupt_all_disable();
}
void __attribute__( ( noinline ) ) ttc_interrupt_all_enable_gdb() {

    _driver_interrupt_all_enable();
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_interrupt(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
