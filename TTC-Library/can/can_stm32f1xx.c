/** { can_stm32f1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for can devices on stm32f1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 21 at 20140324 09:30:54 UTC
 *
 *  Note: See ttc_can.h for description of stm32f1xx independent CAN implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "can_stm32f1xx.h"

//{ Global Variables ***********************************************************

// register base addresses of all USARTs
t_register_stm32f1xx_can* const CAN_Bases[2] = {
    ( t_register_stm32f1xx_can* ) & register_stm32f1xx_CAN1,
    ( t_register_stm32f1xx_can* ) & register_stm32f1xx_CAN2,
};

//}Global Variables
//{ Function Definitions *******************************************************

e_ttc_can_errorcode can_stm32f1xx_deinit( t_ttc_can_config* Config ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    switch ( Config->LogicalIndex ) {
        case 1:
            RCC_APB1PeriphClockCmd( RCC_APB1Periph_CAN1, DISABLE );
            break;
        case 2:
            RCC_APB1PeriphClockCmd( RCC_APB1Periph_CAN2, DISABLE );
            break;
        default:
            break;
    }

    return ec_can_OK;
}
t_ttc_can_config* can_stm32f1xx_get_configuration( t_ttc_can_config* Config ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    return ( t_ttc_can_config* ) 0;
}
e_ttc_can_errorcode can_stm32f1xx_get_features( t_ttc_can_config* Config ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    if ( Config->PhysicalIndex >= TTC_CAN_MAX_AMOUNT )
    { return ttc_assert_origin_auto; }

    /* Set the architecture value*/
    Config->Architecture = ta_can_stm32f1xx;
    /* Reset CAN init structure parameters values */
    Config->Flags.All = 0;
    /* Initialize the time triggered communication mode */
    Config->Flags.Bits.TTCM = 0;
    /* Initialize the automatic bus-off management */
    Config->Flags.Bits.ABOM = 0;
    /* Initialize the automatic wake-up mode */
    Config->Flags.Bits.AWUM = 0;
    /* Initialize the no automatic retransmission */
    Config->Flags.Bits.NART = 0;
    /* Initialize the receive FIFO locked mode */
    Config->Flags.Bits.RFLM = 0;
    /* Initialize the transmit FIFO priority */
    Config->Flags.Bits.TXFP = 0;
    /* Initialize the CAN_Mode member */
    Config->Flags.Bits.Mode = 0;
    /* Initialize the CAN_SJW member */
    Config->Flags.Bits.SJW = 1;
    /* Initialize the CAN_BS1 member */
    Config->BS1 = bs1_can_4tq;
    /* Initialize the CAN_BS2 member */
    Config->BS2 = bs2_can_3tq;
    /* Initialize the CAN_Prescaler member */
    Config->Prescaler = 1;

    return ec_can_OK;
}
e_ttc_can_errorcode can_stm32f1xx_init( t_ttc_can_config* Config ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    t_u32 wait_ack = 0;

    if ( Config->LogicalIndex > TTC_CAN_AMOUNT )
    { return ttc_assert_origin_auto; }

    t_ttc_can_architecture* CAN_Arch = &( Config->CAN_Arch );

    if ( 1 ) {                                 // validate CAN_Features

        t_ttc_can_config CAN_Features;
        ttc_memory_set( &CAN_Features, 0, sizeof( CAN_Features ) );
        CAN_Features.LogicalIndex = Config->LogicalIndex;
        CAN_Features.PhysicalIndex = Config->PhysicalIndex;
        e_ttc_can_errorcode Error = can_stm32f1xx_get_features( &CAN_Features );
        if ( Error ) { return Error; }
        if ( Config->Flags.Bits.Layout > CAN_Features.Flags.Bits.Layout )
        { Config->Flags.Bits.Layout = CAN_Features.Flags.Bits.Layout; }
    }

    if ( 1 ) {                                 // find indexed CAN on current board
        switch ( Config->PhysicalIndex ) {     // find corresponding CAN as defined by makefile.100_board_*
            case 0: CAN_Arch->BaseRegister = ( t_register_stm32f1xx_can* ) &register_stm32f1xx_CAN1; break; // == CAN1
            case 1: CAN_Arch->BaseRegister = ( t_register_stm32f1xx_can* ) &register_stm32f1xx_CAN2; break; // == CAN2
            default: Assert_CAN( 0, ttc_assert_origin_auto ); break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
        }
    }

    if ( 1 ) {                                 // determine pin layout
        e_ttc_gpio_pin PortTx;
        switch ( Config->LogicalIndex ) { // check if board has defined the TX-pin of CAN to init
                #ifdef TTC_CAN1_PIN_TX
            case 1:
                PortTx = TTC_CAN1_PIN_TX;
                break;
                #endif
                #ifdef TTC_CAN2_PIN_TX
            case 2:
                PortTx = TTC_CAN2_PIN_TX;
                break;
                #endif
            default:
                PortTx = E_ttc_gpio_pin_none;
                Assert_CAN( 0, ttc_assert_origin_auto );
                break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
        }

        switch ( ( t_u32 ) CAN_Arch->BaseRegister ) {
            case ( t_u32 ) 0x40006400: { // CAN1 (register_stm32f1xx_CAN1)
                if ( PortTx == E_ttc_gpio_pin_a12 )
                { Config->Flags.Bits.Layout = 0; }
                else if ( PortTx == E_ttc_gpio_pin_b9 )
                { Config->Flags.Bits.Layout = 2; }
                else
                { Config->Flags.Bits.Layout = 3; }
                break;
            }
            case ( t_u32 ) 0x40006800: { // CAN2 (register_stm32f1xx_CAN2)
                if ( PortTx == E_ttc_gpio_pin_b6 )
                { Config->Flags.Bits.Layout = 1; }
                else
                { Config->Flags.Bits.Layout = 0; }
                break;
            }
            default: { Assert_CAN( 0, ttc_assert_origin_auto ); break;}
        }
    }

    if ( 1 ) {                                 // activate clocks
        switch ( ( t_u32 ) CAN_Arch->BaseRegister ) {
            case ( t_u32 ) 0x40006400: { // (register_stm32f1xx_CAN1)
                if ( Config->Flags.Bits.Layout == 0 ) { // GPIOA
                    RCC_APB1PeriphClockCmd( RCC_APB1Periph_CAN1, ENABLE );
                    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA, ENABLE );
                    RCC_APB2PeriphClockCmd( RCC_APB2Periph_AFIO, ENABLE );
                }
                else if ( Config->Flags.Bits.Layout == 2 ) { // GPIOB
                    RCC_APB1PeriphClockCmd( RCC_APB1Periph_CAN1, ENABLE );
                    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB, ENABLE );
                    RCC_APB2PeriphClockCmd( RCC_APB2Periph_AFIO, ENABLE );
                }
                else {                       // GPIOD
                    RCC_APB1PeriphClockCmd( RCC_APB1Periph_CAN1, ENABLE );
                    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOD, ENABLE );
                    RCC_APB2PeriphClockCmd( RCC_APB2Periph_AFIO, ENABLE );
                }
                break;
            }
            case ( t_u32 ) 0x40006800: { // (register_stm32f1xx_CAN2)
                // GPIOB
                RCC_APB1PeriphClockCmd( RCC_APB1Periph_CAN2 , ENABLE );
                RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB, ENABLE );
                RCC_APB2PeriphClockCmd( RCC_APB2Periph_AFIO, ENABLE );
                break;
            }
            default: Assert_CAN( 0, ttc_assert_origin_auto ); break; // should never occur!
        }
    }

    if ( 1 ) {                                 // apply remap layout

        switch ( ( t_u32 ) CAN_Arch->BaseRegister ) {
            case ( t_u32 ) 0x40006400: { // CAN1 (register_stm32f1xx_CAN1)
                if ( Config->Flags.Bits.Layout == 0 ) {
                    GPIO_PinRemapConfig( GPIO_Remap1_CAN1, DISABLE );
                    GPIO_PinRemapConfig( GPIO_Remap2_CAN1, DISABLE );
                }
                else if ( Config->Flags.Bits.Layout == 2 ) {
                    GPIO_PinRemapConfig( GPIO_Remap2_CAN1, DISABLE );
                    GPIO_PinRemapConfig( GPIO_Remap1_CAN1, ENABLE );
                }
                else {
                    GPIO_PinRemapConfig( GPIO_Remap1_CAN1, DISABLE );
                    GPIO_PinRemapConfig( GPIO_Remap2_CAN1, ENABLE );
                }
                break;
            }
            case ( t_u32 ) 0x40006800: { // CAN2 (register_stm32f1xx_CAN2)
                if ( Config->Flags.Bits.Layout )
                { GPIO_PinRemapConfig( GPIO_Remap_CAN2, ENABLE ); }
                else
                { GPIO_PinRemapConfig( GPIO_Remap_CAN2, DISABLE ); }
                break;
            }
            default: { break; }
        }
    }

    if ( 0 ) {                                 // compare chosen pin layout with configured pins
        e_ttc_gpio_pin Port;
        switch ( Config->LogicalIndex ) { // check if board has defined the TX-pin of CAN to init

            case 1: { // check configuration of CAN #1
                #ifdef TTC_CAN1_PIN_TX
                Port = TTC_CAN1_PIN_TX;
                Assert_CAN( Port == CAN_Arch->PortTxD, ttc_assert_origin_auto ); // ERROR: configured GPIO cannot be used in current remapping layout!
                #else
                CAN_Arch->PortTxD = E_ttc_gpio_pin_none;
                #endif
                #ifdef TTC_CAN1_PIN_RX
                Port = TTC_CAN1_PIN_RX;
                Assert_CAN( Port == CAN_Arch->PortRxD, ttc_assert_origin_auto ); // ERROR: configured GPIO cannot be used in current remapping layout!
                #else
                CAN_Arch->PortRxD = E_ttc_gpio_pin_none;
                #endif
                break;
            }
            case 2: { // check configuration of CAN #2
                #ifdef TTC_CAN2_PIN_TX
                Port = TTC_CAN2_PIN_TX;
                Assert_CAN( Port == CAN_Arch->PortTxD, ttc_assert_origin_auto ); // ERROR: configured GPIO cannot be used in current remapping layout!
                #else
                CAN_Arch->PortTxD = E_ttc_gpio_pin_none;
                #endif
                #ifdef TTC_CAN2_PIN_RX
                Port = TTC_CAN2_PIN_RX;
                Assert_CAN( Port == CAN_Arch->PortRxD, ttc_assert_origin_auto ); // ERROR: configured GPIO cannot be used in current remapping layout!
                #else
                CAN_Arch->PortRxD = E_ttc_gpio_pin_none;
                #endif
                break;
            }
            default: Assert_CAN( 0, ttc_assert_origin_auto ); break; // No TTC_CANn defined! Check your makefile.100_board_* file!

        }
    }

    if ( 1 ) {                                  // determine pins for specific layout
        switch ( ( t_u32 ) CAN_Arch->BaseRegister ) {
            case ( t_u32 ) 0x40006400: { // CAN1 (register_stm32f1xx_CAN1)
                if ( Config->Flags.Bits.Layout == 0 ) { // alternate pin remapping (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
                    CAN_Arch->PortTxD = E_ttc_gpio_pin_a12;
                    CAN_Arch->PortRxD = E_ttc_gpio_pin_a11;
                }
                else if ( Config->Flags.Bits.Layout == 2 ) { // alternate pin remapping (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
                    CAN_Arch->PortTxD = E_ttc_gpio_pin_b9;
                    CAN_Arch->PortRxD = E_ttc_gpio_pin_b8;
                }
                else {                       // no pin remapping
                    CAN_Arch->PortTxD = E_ttc_gpio_pin_d1;
                    CAN_Arch->PortRxD = E_ttc_gpio_pin_d0;
                }
                break;
            }
            case ( t_u32 ) 0x40006800: { // CAN2 (register_stm32f1xx_CAN2)
                if ( Config->Flags.Bits.Layout ) { // alternate pin remapping (-> ST RM0041 STM32F100xx advanced ARM-based 32-bit MCUs p.118)
                    CAN_Arch->PortTxD = E_ttc_gpio_pin_b6;
                    CAN_Arch->PortRxD = E_ttc_gpio_pin_b5;
                }
                else {                       // no pin remapping
                    CAN_Arch->PortTxD = E_ttc_gpio_pin_b13;
                    CAN_Arch->PortRxD = E_ttc_gpio_pin_b12;
                }
                break;
            }
            default: { Assert_CAN( 0, ttc_assert_origin_auto ); }
        }
    }

    if ( 1 ) {                                 // configure GPIOs
        if ( CAN_Arch->PortTxD )
        { ttc_gpio_init( CAN_Arch->PortTxD, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_50mhz ); }
        if ( CAN_Arch->PortRxD )
        { ttc_gpio_init( CAN_Arch->PortRxD, E_ttc_gpio_mode_input_pull_up,               E_ttc_gpio_speed_50mhz ); }
    }

    can_stm32f1xx_reset( CAN_Arch );

    if ( 1 ) {                                 // init CAN
        /* Exit from sleep mode */
        Config->CAN_Arch.BaseRegister->MCR.Bits.SLEEP = 0; //&= (~(t_u32)0x0002);
//        Config->CAN_Arch.BaseRegister->MCR.All &= (~(t_u32)0x0002);

        /* Request initialisation */
        Config->CAN_Arch.BaseRegister->MCR.Bits.INRQ = 1; //|= ((t_u16)0x0001);

        /* Wait the acknowledge */
        while ( ( Config->CAN_Arch.BaseRegister->MSR.Bits.INAK /*& ((t_u16)0x0001))*/ != 1 ) && ( wait_ack != ( ( t_u32 )0x0000FFFF ) ) )
        { wait_ack++; }

        /* Check acknowledge */
        if ( Config->CAN_Arch.BaseRegister->MSR.Bits.INAK /*& ((t_u16)0x0001))*/ != 1 )
        { return ec_can_ACKError; }
        else {
            /* Set the time triggered communication mode */
            if ( Config->Flags.Bits.TTCM != 0 ) {
                //Config->CAN_Arch.BaseRegister->MCR |= ((t_u16)0x0080);
                Config->CAN_Arch.BaseRegister->MCR.Bits.NART = 1;
                Config->CAN_Arch.BaseRegister->MCR.Bits.ABOM = 1;
            }
            else {
                //Config->CAN_Arch.BaseRegister->MCR &= ~((t_u32)0x0080);
                Config->CAN_Arch.BaseRegister->MCR.Bits.NART = 0;
                Config->CAN_Arch.BaseRegister->MCR.Bits.ABOM = 0;
            }


            /* Set the automatic bus-off management */
            if ( Config->Flags.Bits.ABOM != 0 ) {
                //Config->CAN_Arch.BaseRegister->MCR |= ((t_u16)0x0040);
                Config->CAN_Arch.BaseRegister->MCR.Bits.RFLM = 1;
                Config->CAN_Arch.BaseRegister->MCR.Bits.AWUM = 1;
            }
            else {
                //Config->CAN_Arch.BaseRegister->MCR &= ~((t_u32)0x0040);
                Config->CAN_Arch.BaseRegister->MCR.Bits.RFLM = 0;
                Config->CAN_Arch.BaseRegister->MCR.Bits.AWUM = 0;
            }

            /* Set the automatic wake-up mode */
            if ( Config->Flags.Bits.AWUM != 0 ) {
                //Config->CAN_Arch.BaseRegister->MCR |= ((t_u16)0x0020);
                Config->CAN_Arch.BaseRegister->MCR.Bits.TXFP = 1;
                Config->CAN_Arch.BaseRegister->MCR.Bits.NART = 1;
            }
            else {
                //Config->CAN_Arch.BaseRegister->MCR &= ~((t_u32)0x0020);
                Config->CAN_Arch.BaseRegister->MCR.Bits.TXFP = 0;
                Config->CAN_Arch.BaseRegister->MCR.Bits.NART = 0;
            }

            /* Set the no automatic retransmission */
            if ( Config->Flags.Bits.NART != 0 ) {
                //Config->CAN_Arch.BaseRegister->MCR |= ((t_u16)0x0010);
                Config->CAN_Arch.BaseRegister->MCR.Bits.SLEEP = 1;
                Config->CAN_Arch.BaseRegister->MCR.Bits.NART  = 1;
            }
            else {
                //Config->CAN_Arch.BaseRegister->MCR &= ~((t_u32)0x0010);
                Config->CAN_Arch.BaseRegister->MCR.Bits.SLEEP = 0;
                Config->CAN_Arch.BaseRegister->MCR.Bits.NART  = 0;
            }

            /* Set the receive FIFO locked mode */
            if ( Config->Flags.Bits.RFLM != 0 ) {
                //Config->CAN_Arch.BaseRegister->MCR |= ((t_u16)0x0008);
                Config->CAN_Arch.BaseRegister->MCR.Bits.RFLM = 1;
            }
            else {
                //Config->CAN_Arch.BaseRegister->MCR &= ~((t_u32)0x0008);
                Config->CAN_Arch.BaseRegister->MCR.Bits.RFLM = 0;
            }

            /* Set the transmit FIFO priority */
            if ( Config->Flags.Bits.TXFP != 0 ) {
                //Config->CAN_Arch.BaseRegister->MCR |= ((t_u16)0x0004);
                Config->CAN_Arch.BaseRegister->MCR.Bits.TXFP = 1;
            }
            else {
                //Config->CAN_Arch.BaseRegister->MCR &= ~((t_u32)0x0004);
                Config->CAN_Arch.BaseRegister->MCR.Bits.TXFP = 0;
            }

            /* Set the bit timing register */
            Config->CAN_Arch.BaseRegister->BTR.All = ( t_u32 )( ( t_u32 )Config->Flags.Bits.Mode << 30 ) | \
                                                     ( ( t_u32 )Config->Flags.Bits.SJW  << 24 ) | \
                                                     ( ( t_u32 )Config->BS1 << 16 ) | \
                                                     ( ( t_u32 )Config->BS2 << 20 ) | \
                                                     ( ( t_u32 )Config->Prescaler - 1 );

            /* Request leave initialisation */
            //Config->CAN_Arch.BaseRegister->MCR &= ~(t_u32)0x0001;
            Config->CAN_Arch.BaseRegister->MCR.Bits.INRQ = 0;

            /* Wait the acknowledge */
            wait_ack = 0;

            while ( ( Config->CAN_Arch.BaseRegister->MSR.Bits.INAK == 1 ) && ( wait_ack != ( ( t_u32 )0x0000FFFF ) ) )
            { wait_ack++; }

            /* ...and check acknowledged */
            if ( Config->CAN_Arch.BaseRegister->MSR.Bits.INAK == 1 )
            { return ec_can_ACKError; }
            else
            { return ec_can_OK; }
        }
    }

    return ec_can_OK;
}
e_ttc_can_errorcode can_stm32f1xx_load_defaults( t_ttc_can_config* Config ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    /* Reset CAN init structure parameters values */
    Config->Flags.All = 0;
    /* Initialize the time triggered communication mode */
    Config->Flags.Bits.TTCM = 0;
    /* Initialize the automatic bus-off management */
    Config->Flags.Bits.ABOM = 0;
    /* Initialize the automatic wake-up mode */
    Config->Flags.Bits.AWUM = 0;
    /* Initialize the no automatic retransmission */
    Config->Flags.Bits.NART = 0;
    /* Initialize the receive FIFO locked mode */
    Config->Flags.Bits.RFLM = 0;
    /* Initialize the transmit FIFO priority */
    Config->Flags.Bits.TXFP = 0;
    /* Initialize the CAN_Mode member */
    Config->Flags.Bits.Mode = 0;
    /* Initialize the CAN_SJW member */
    Config->Flags.Bits.SJW = 0;
    /* Initialize the CAN_BS1 member */
    Config->BS1 = bs1_can_2tq;
    /* Initialize the CAN_BS2 member */
    Config->BS2 = bs2_can_3tq;
    /* Initialize the CAN_Prescaler member */
    Config->Prescaler = 40;

    return ec_can_OK;
}
void can_stm32f1xx_prepare() {
}

e_ttc_can_errorcode can_stm32f1xx_read( t_ttc_can_config* Config, t_u8* Byte ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    Assert_CAN( Byte, ttc_assert_origin_auto ); // pointers must not be NULL
    //_can_stm32f1xx_message_pending(Config, 0);
    //_can_stm32f1xx_message_pending(Config, 1);

    /* Get the Id */
    Byte = ( t_u8* )Config->CAN_Rx_Message.Data;

    return ec_can_OK;
}
e_ttc_can_errorcode can_stm32f1xx_reset( t_ttc_can_architecture* Architecture ) {
    Assert_CAN( Architecture, ttc_assert_origin_auto ); // pointers must not be NULL

    if ( Architecture->BaseRegister == &register_stm32f1xx_CAN1 ) {
        /* Enable CAN1 reset state */
        RCC_APB1PeriphResetCmd( RCC_APB1Periph_CAN1, ENABLE );
        /* Release CAN1 from reset state */
        RCC_APB1PeriphResetCmd( RCC_APB1Periph_CAN1, DISABLE );
        return ec_can_OK;
    }
    else if ( Architecture->BaseRegister == &register_stm32f1xx_CAN2 ) {
        /* Enable CAN2 reset state */
        RCC_APB1PeriphResetCmd( RCC_APB1Periph_CAN2, ENABLE );
        /* Release CAN2 from reset state */
        RCC_APB1PeriphResetCmd( RCC_APB1Periph_CAN2, DISABLE );
        return ec_can_OK;
    }
    else { return ttc_assert_origin_auto; }
}
t_u8 can_stm32f1xx_send( t_ttc_can_config* Config ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    t_u8 transmit_mailbox = 0;

    /* Select one empty transmit mailbox */
    if ( Config->CAN_Arch.BaseRegister->TSR.Bits.TME0 == 1 )
    { transmit_mailbox = 0; }
    else if ( Config->CAN_Arch.BaseRegister->TSR.Bits.TME1 == 1 )
    { transmit_mailbox = 1; }
    else if ( Config->CAN_Arch.BaseRegister->TSR.Bits.TME2 == 1 )
    { transmit_mailbox = 2; }
    else
    { transmit_mailbox = TTC_NO_MAILBOX; }

//    if ((Config->CAN_Arch.BaseRegister->TSR.All & ((t_u32)0x04000000)) == ((t_u32)0x04000000))
//      transmit_mailbox = 0;
//    else if ((Config->CAN_Arch.BaseRegister->TSR.All & ((t_u32)0x08000000)) == ((t_u32)0x08000000))
//      transmit_mailbox = 1;
//    else if ((Config->CAN_Arch.BaseRegister->TSR.All & ((t_u32)0x10000000)) == ((t_u32)0x10000000))
//      transmit_mailbox = 2;
//    else
//      transmit_mailbox = TTC_NO_MAILBOX;


    if ( transmit_mailbox != TTC_NO_MAILBOX ) {
        /* Set up the Id */
        Config->CAN_Arch.BaseRegister->CAN_Tx_Mailbox[transmit_mailbox].TIR.All &= ( ( t_u32 )0x00000001 );
        if ( Config->CAN_Tx_Message.Flags.Bits.IDE == 0 ) {
            Config->CAN_Arch.BaseRegister->CAN_Tx_Mailbox[transmit_mailbox].TIR.All |= ( ( Config->CAN_Tx_Message.StdId << 21 ) | ( ( t_u8 )Config->CAN_Tx_Message.Flags.Bits.RTR ) );
        }
        else {
            Config->CAN_Arch.BaseRegister->CAN_Tx_Mailbox[transmit_mailbox].TIR.All |= ( ( Config->CAN_Tx_Message.ExtId << 3 ) | ( ( t_u8 )Config->CAN_Tx_Message.Flags.Bits.IDE ) | ( ( t_u8 )Config->CAN_Tx_Message.Flags.Bits.RTR ) );
        }

        /* Set up the DLC */
        Config->CAN_Tx_Message.Flags.Bits.DLC &= ( t_u8 )0x0000000F;
        Config->CAN_Arch.BaseRegister->CAN_Tx_Mailbox[transmit_mailbox].TDTR.All &= ( t_u32 )0xFFFFFFF0;
        Config->CAN_Arch.BaseRegister->CAN_Tx_Mailbox[transmit_mailbox].TDTR.Bits.DLC |= ( t_u8 )Config->CAN_Tx_Message.Flags.Bits.DLC;

        /* Set up the data field */
        Config->CAN_Arch.BaseRegister->CAN_Tx_Mailbox[transmit_mailbox].TDLR = ( t_u32 )Config->CAN_Tx_Message.Data[0];
        Config->CAN_Arch.BaseRegister->CAN_Tx_Mailbox[transmit_mailbox].TDHR = ( t_u32 )Config->CAN_Tx_Message.Data[1];

        /* Request transmission */
        Config->CAN_Arch.BaseRegister->CAN_Tx_Mailbox[transmit_mailbox].TIR.All |= ( ( t_u32 )0x00000001 );
    }
    else { return ec_can_TX_NoMailBox; }

    return transmit_mailbox;
}
void can_stm32f1xx_set_filter( t_u8 FilterNum, t_ttc_can_config* Config ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    t_u32 filter_number_bit_pos = 0;
    filter_number_bit_pos = ( ( t_u32 )1 ) << Config->CAN_Filter[FilterNum].Flags.Bits.FilterNumber;

    /* Initialisation mode for the filter */
    Config->CAN_Arch.BaseRegister->FMR |= ( ( t_u32 )0x00000001 );
//    Config->CAN_Arch.BaseRegister->FMR.FINIT = 1;

    /* Filter Deactivation */
    Config->CAN_Arch.BaseRegister->FA1R &= ~( t_u32 )filter_number_bit_pos;

    /* Filter Scale */
    if ( Config->CAN_Filter[FilterNum].Flags.Bits.FilterScale == 0 ) {
        /* 16-bit scale for the filter */
        Config->CAN_Arch.BaseRegister->FS1R &= ~( t_u32 )filter_number_bit_pos;

        /* First 16-bit identifier and First 16-bit mask */
        /* Or First 16-bit identifier and Second 16-bit identifier */
        Config->CAN_Arch.BaseRegister->CAN_Filter[Config->CAN_Filter[FilterNum].Flags.Bits.FilterNumber].FR1 =
            ( ( 0x0000FFFF & ( t_u32 )Config->CAN_Filter[FilterNum].MaskIdLow ) << 16 ) |
            ( 0x0000FFFF & ( t_u32 )Config->CAN_Filter[FilterNum].IdLow );

        /* Second 16-bit identifier and Second 16-bit mask */
        /* Or Third 16-bit identifier and Fourth 16-bit identifier */
        Config->CAN_Arch.BaseRegister->CAN_Filter[Config->CAN_Filter[FilterNum].Flags.Bits.FilterNumber].FR2 =
            ( ( 0x0000FFFF & ( t_u32 )Config->CAN_Filter[FilterNum].MaskIdHigh ) << 16 ) |
            ( 0x0000FFFF & ( t_u32 )Config->CAN_Filter[FilterNum].IdHigh );
    }

    if ( Config->CAN_Filter[FilterNum].Flags.Bits.FilterScale == 1 ) {
        /* 32-bit scale for the filter */
        Config->CAN_Arch.BaseRegister->FS1R |= filter_number_bit_pos;
        /* 32-bit identifier or First 32-bit identifier */
        Config->CAN_Arch.BaseRegister->CAN_Filter[Config->CAN_Filter[FilterNum].Flags.Bits.FilterNumber].FR1 =
            ( ( 0x0000FFFF & ( t_u32 )Config->CAN_Filter[FilterNum].IdHigh ) << 16 ) |
            ( 0x0000FFFF & ( t_u32 )Config->CAN_Filter[FilterNum].IdLow );
        /* 32-bit mask or Second 32-bit identifier */
        Config->CAN_Arch.BaseRegister->CAN_Filter[Config->CAN_Filter[FilterNum].Flags.Bits.FilterNumber].FR2 =
            ( ( 0x0000FFFF & ( t_u32 )Config->CAN_Filter[FilterNum].MaskIdHigh ) << 16 ) |
            ( 0x0000FFFF & ( t_u32 )Config->CAN_Filter[FilterNum].MaskIdLow );
    }

    /* Filter Mode */

    /*Id/Mask mode for the filter*/
    if ( Config->CAN_Filter[FilterNum].Flags.Bits.FilterMode == 0 )
    { Config->CAN_Arch.BaseRegister->FM1R &= ~( t_u32 )filter_number_bit_pos; }
    else /*Identifier list mode for the filter*/
    { Config->CAN_Arch.BaseRegister->FM1R |= ( t_u32 )filter_number_bit_pos; }

    /* Filter FIFO assignment */

    /* FIFO 0 assignation for the filter */
    if ( Config->CAN_Filter[FilterNum].Flags.Bits.FifoAssign == 0 )
    { Config->CAN_Arch.BaseRegister->FFA1R &= ~( t_u32 )filter_number_bit_pos; }

    /* FIFO 1 assignation for the filter */
    if ( Config->CAN_Filter[FilterNum].Flags.Bits.FifoAssign == 1 )
    { Config->CAN_Arch.BaseRegister->FFA1R |= ( t_u32 )filter_number_bit_pos; }

    /* Filter activation */
    if ( Config->CAN_Filter[FilterNum].Flags.Bits.FilterActivation == 1 )
    { Config->CAN_Arch.BaseRegister->FA1R |= filter_number_bit_pos; }

    /* Leave the initialisation mode for the filter */
    //Config->CAN_Arch.BaseRegister->FMR &= ~FMR_FINIT;
    Config->CAN_Arch.BaseRegister->FMR &= ~( ( t_u32 )0x00000001 );
}

void _can_stm32f1xx_rx_isr( t_physical_index PhysicalIndex, void* Argument ) {
    Assert_CAN( PhysicalIndex <= TTC_CAN_AMOUNT, ttc_assert_origin_auto );
    t_register_stm32f1xx_can* CAN_Base = CAN_Bases[PhysicalIndex];
    ( void ) CAN_Base;  // Avoid Warnings

    t_ttc_can_config* Config = ( t_ttc_can_config* ) Argument;

    Config->CAN_Rx_Message.Data[0] = ( ( t_register_stm32f1xx_can* ) CAN_Base )->CAN_FIFO_Mailbox[PhysicalIndex].RDLR;
    Config->CAN_Rx_Message.Data[1] = ( ( t_register_stm32f1xx_can* ) CAN_Base )->CAN_FIFO_Mailbox[PhysicalIndex].RDHR;
    Config->CAN_Rx_Message.StdId = ( ( t_u32 )( ( t_register_stm32f1xx_can* ) CAN_Base )->CAN_FIFO_Mailbox[PhysicalIndex].RIR.Bits.STID );
    Config->CAN_Rx_Message.ExtId = ( ( t_u32 )( ( ( t_register_stm32f1xx_can* ) CAN_Base )->CAN_FIFO_Mailbox[PhysicalIndex].RIR.All & 0x1FFFFFFF ) >> 3 );
    Config->CAN_Rx_Message.Flags.Bits.DLC = ( ( t_register_stm32f1xx_can* ) CAN_Base )->CAN_FIFO_Mailbox[PhysicalIndex].RDTR.Bits.DLC;
    Config->CAN_Rx_Message.Flags.Bits.IDE = ( ( t_register_stm32f1xx_can* ) CAN_Base )->CAN_FIFO_Mailbox[PhysicalIndex].RIR.Bits.IDE;
    Config->CAN_Rx_Message.Flags.Bits.RTR = ( ( t_register_stm32f1xx_can* ) CAN_Base )->CAN_FIFO_Mailbox[PhysicalIndex].RIR.Bits.RTR;
    Config->CAN_Rx_Message.FMI = ( ( t_register_stm32f1xx_can* ) CAN_Base )->CAN_FIFO_Mailbox[PhysicalIndex].RDTR.Bits.FMI;

    /* Release FIFO0 */
    if ( ( t_register_stm32f1xx_can* ) CAN_Base == ( t_register_stm32f1xx_can* ) &register_stm32f1xx_CAN1 ) {
        Config->CAN_Arch.BaseRegister->RF0R.All |= ( ( t_u8 )0x20 );
    }
    /* Release FIFO1 */
    else { /* FIFONumber == CAN_FIFO1 */
        Config->CAN_Arch.BaseRegister->RF1R.All |= ( ( t_u8 )0x20 );
    }
    _ttc_can_rx_isr( Config ); // provide received byte to registered function
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions

//{ Private Functions (ideally) ************************************************

t_u8 _can_stm32f1xx_message_pending( t_ttc_can_config* Config, t_u8 FIFONumber ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    t_u8 message_pending = 0;

    if ( FIFONumber == 0 ) {
        message_pending = ( t_u8 )Config->CAN_Arch.BaseRegister->RF0R.Bits.FMP0;
    }
    else if ( FIFONumber == 1 ) {
        message_pending = ( t_u8 )Config->CAN_Arch.BaseRegister->RF1R.Bits.FMP1;
    }
    else {
        message_pending = 0;
    }

    return message_pending;
}

t_u8 _can_stm32f1xx_tx_status( t_ttc_can_config* Config, t_u8 MailboxNumber ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // pointers must not be NULL
    t_u32 state = 0;

    switch ( MailboxNumber ) {
        case ( 0 ):
//      state = Config->CAN_Arch.BaseRegister->TSR.All & (((t_u32)0x00000001) | ((t_u32)0x00000002) | ((t_u32)0x04000000));
            state = Config->CAN_Arch.BaseRegister->TSR.Bits.RQCP0 | Config->CAN_Arch.BaseRegister->TSR.Bits.TXOK0 | Config->CAN_Arch.BaseRegister->TSR.Bits.TME0;
            break;
        case ( 1 ):
//      state = Config->CAN_Arch.BaseRegister->TSR.All &  (((t_u32)0x00000100) | ((t_u32)0x00000200) | ((t_u32)0x08000000));
            state = Config->CAN_Arch.BaseRegister->TSR.Bits.RQCP1 | Config->CAN_Arch.BaseRegister->TSR.Bits.TXOK1 | Config->CAN_Arch.BaseRegister->TSR.Bits.TME1;
            break;
        case ( 2 ):
//      state = Config->CAN_Arch.BaseRegister->TSR.All &  (((t_u32)0x00010000) | ((t_u32)0x00020000) | ((t_u32)0x10000000));
            state = Config->CAN_Arch.BaseRegister->TSR.Bits.RQCP2 | Config->CAN_Arch.BaseRegister->TSR.Bits.TXOK2 | Config->CAN_Arch.BaseRegister->TSR.Bits.TME2;
            break;
        default:
            state = tx_status_Failed;
            break;
    }

    switch ( state ) {
        /* transmit pending  */
        case ( 0x0 ): state = tx_status_Pending;
            break;
        /* transmit failed  */
        case 67108865 /* RQCP0 | TME0 */: state = tx_status_Failed;
            break;
        case 134217984 /* RQCP1 | TME1 */: state = tx_status_Failed;
            break;
        case 268500992 /* RQCP2 | TME2 */: state = tx_status_Failed;
            break;
        /* transmit succeeded  */
        case 67108867 /* RQCP0 | TXOK0 | TME0 */: state = tx_status_OK;
            break;
        case 134218496 /* RQCP1| TXOK1 | TME1 */: state = tx_status_OK;
            break;
        case 268632064 /* RQCP2 | TXOK2 | TME2 */: state = tx_status_OK;
            break;
        default: state = tx_status_Failed;
            break;
    }
    return ( t_u8 ) state;
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
