#ifndef CAN_STM32F1XX_H
#define CAN_STM32F1XX_H

/** { can_stm32f1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for can devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by high-level can and application.
 *
 *  Created from template device_architecture.h revision 22 at 20140324 09:30:54 UTC
 *
 *  Note: See ttc_can.h for description of stm32f1xx independent CAN implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_CAN_STM32F1XX
//
// Implementation status of low-level driver support for can devices on stm32f1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_CAN_STM32F1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_CAN_STM32F1XX == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_CAN_STM32F1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_CAN_STM32F1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "can_stm32f1xx_types.h"
#include "../ttc_can.h"
#include "../ttc_task.h"
#include "../ttc_interrupt.h"
#include "../ttc_gpio.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_can_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_can_foo
//
#define ttc_driver_can_deinit(Config) can_stm32f1xx_deinit(Config)
#define ttc_driver_can_get_configuration(Config) can_stm32f1xx_get_configuration(Config)
#define ttc_driver_can_get_features(Config) can_stm32f1xx_get_features(Config)
#define ttc_driver_can_init(Config) can_stm32f1xx_init(Config)
#define ttc_driver_can_load_defaults(Config) can_stm32f1xx_load_defaults(Config)
#define ttc_driver_can_prepare() can_stm32f1xx_prepare()
#define ttc_driver_can_read(Config, Byte) can_stm32f1xx_read(Config, Byte)
#define ttc_driver_can_reset(Architecture) can_stm32f1xx_reset(Architecture)
#define ttc_driver_can_send(Config) can_stm32f1xx_send(Config)
#define ttc_driver_can_set_filter(FilterNum, Config) can_stm32f1xx_set_filter(FilterNum, Config)
#define _ttc_driver_can_tx_status(Config, MailBox) _can_stm32f1xx_tx_status(Config, MailBox)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_can.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_can.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

/** shutdown single CAN unit device
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @return              == 0: CAN has been shutdown successfully; != 0: error-code
 */
e_ttc_can_errorcode can_stm32f1xx_deinit( t_ttc_can_config* Config );

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param Config        = pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @return                configuration of indexed device
 */
t_ttc_can_config* can_stm32f1xx_get_configuration( t_ttc_can_config* Config );

/** fills out given Config with maximum valid values for indexed CAN
 * @param Config        = pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @return              == 0: *Config has been initialized successfully; != 0: error-code
 */
e_ttc_can_errorcode can_stm32f1xx_get_features( t_ttc_can_config* Config );

/** initializes single CAN unit for operation
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @return              == 0: CAN has been initialized successfully; != 0: error-code
 */
e_ttc_can_errorcode can_stm32f1xx_init( t_ttc_can_config* Config );

/** loads configuration of indexed CAN unit with default values
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @return              == 0: configuration was loaded successfully
 */
e_ttc_can_errorcode can_stm32f1xx_load_defaults( t_ttc_can_config* Config );

/** Prepares usart driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void can_stm32f1xx_prepare();

/** Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: CAN must be initialized before!
 * Note: This function is partially thread safe (use it for each CAN from one thread only)!
 *
 * @param Register       pointer to low-level register for fast hardware operation
 * @param Word           pointer to 16 bit buffer where to store Word
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_can_errorcode can_stm32f1xx_read( t_ttc_can_config* Config, t_u8* Byte );

/** reset configuration of indexed device and connected hardware
 *
 * @param Architecture        pointer to struct t_ttc_can_architecture (must have valid value for PhysicalIndex)
 * @return   =
 */
e_ttc_can_errorcode can_stm32f1xx_reset( t_ttc_can_architecture* Architecture );

/** Send single data word to output buffer (8 or 9 bits)
 *
 * Note: CAN must be initialized before!
 * Note: This function is partially thread safe (use it for each CAN from one thread only)!
 *
 * @param Register      pointer to low-level register for fast hardware operation
 * @param Word          pointer to 16 bit buffer where to store Word
 * @return              == 0: Word has been read successfully; != 0: error-code
 */
t_u8 can_stm32f1xx_send( t_ttc_can_config* Config );

/** Initializes the Filter in CANx.
 *
 * Note: CAN must be initialized before!
 * Note: This function is partially thread safe (use it for each CAN from one thread only)!
 *
 * @param FilterNum        Filter options in CAN1
 * @param Config           pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @return                 none
 */
void can_stm32f1xx_set_filter( t_u8 FilterNum, t_ttc_can_config* Config );

/** general interrupt handler for received byte; reads received byte and pass it to receiveSingleByte()
 * @param PhysicalIndex     0..TTC_CAN_AMOUNT-1 - CAN device to use (0=CAN1, ...)
 * @param CAN_Generic       pointer to configuration of corresponding CAN
 */
void _can_stm32f1xx_rx_isr( t_physical_index PhysicalIndex, void* Argument );
//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _can_stm32f1xx_foo(t_ttc_can_config* Config)
/** Check if there is a pending message in the FIFOn Buffer
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @param FIFONumber    index of FIFO Buffer to check (0 or 1)
 */
t_u8 _can_stm32f1xx_message_pending( t_ttc_can_config* Config, t_u8 FIFONumber );

/** Check the status of the last transmission
 * @param Config        pointer to struct t_ttc_can_config (must have valid value for PhysicalIndex)
 * @param MailboxNumber index of Mailbox to check (1, 2 or 3)
 */
t_u8 _can_stm32f1xx_tx_status( t_ttc_can_config* Config, t_u8 MailboxNumber );

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //CAN_STM32F1XX_H