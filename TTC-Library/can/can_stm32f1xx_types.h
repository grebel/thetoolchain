#ifndef CAN_STM32F1XX_TYPES_H
#define CAN_STM32F1XX_TYPES_H

/** { can_stm32f1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for CAN devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by ttc_can_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140324 09:30:54 UTC
 *
 *  Note: See ttc_can.h for description of architecture independent CAN implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)
// maximum amount of available USART/ UART units in current architecture
#define TTC_CAN_MAX_AMOUNT 2

//InsertEnums above (DO NOT REMOVE THIS LINE!)
#define _driver_can_rx_isr    _can_stm32f1xx_rx_isr // define function pointer for receive interrupt service routine
#define _driver_can_tx_isr    _can_stm32f1xx_tx_isr // define function pointer for transmit interrupt service routine
#define ttc_can_t_register    t_register_stm32f1xx_can

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic.h"
#include "../gpio/gpio_stm32f1xx.h"
#include "../register/register_stm32f1xx.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_can_types.h *************************

typedef struct { // register description (adapt according to stm32f1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_can_register;

typedef struct {  // stm32f1xx specific configuration data of single CAN device
    t_register_stm32f1xx_can* BaseRegister;       // base address of CAN device registers
    e_ttc_gpio_pin PortTxD;                       // port pin for TxD
    e_ttc_gpio_pin PortRxD;                       // port pin for RxD
} __attribute__( ( __packed__ ) ) t_can_stm32f1xx_config;

// t_ttc_can_architecture is required by ttc_can_types.h
#define t_ttc_can_architecture t_can_stm32f1xx_config

//} Structures/ Enums


#endif //CAN_STM32F1XX_TYPES_H
