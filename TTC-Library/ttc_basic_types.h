/** { ttc_basic_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for BASIC device.
 *  Structures, Enums and Defines being required by both, high- and low-level basic.
 *  This file does not provide function declarations!
 *
 *  Created from template ttc_device_types.h revision 27 at 20140603 04:08:18 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_BASIC_TYPES_H
#define TTC_BASIC_TYPES_H

//#define STM32F10X_CL //DEBUG
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_basic.h" or "ttc_basic.c"
//

// Activate include below to see constant definitions in your IDE
//#include "compile_options.h" // Note: do not activate for compilation (will cause lots of error messages due to circular includes)

#include "compile_options.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)
#ifdef EXTENSION_basic_cm3
    #include "basic/basic_cm3_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_basic_stm32f30x
    #include "basic/basic_stm32f30x_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_basic_stm32l1xx
    #include "basic/basic_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_basic_stm32l0xx
    #include "basic/basic_stm32l0xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_basic_stm32f1xx
    #include "basic/basic_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Basic Datatypes ******************************************************
typedef unsigned char       t_u8;
typedef unsigned short int  t_u16;
typedef unsigned long       t_u32;
typedef signed char         t_s8;
typedef signed short int    t_s16;
typedef signed int          t_s32;
#ifdef FALSE
    #undef FALSE
#endif
#ifdef TRUE
    #undef TRUE
#endif
typedef enum { FALSE = 0, TRUE } BOOL;
typedef BOOL bool;

#ifndef t_base
    #  warning Missing architecture definition for t_base, using default size!
    #define  t_base        t_u32  // basic (native) datatype of 32-bit architectures
#endif
#ifndef t_base_signed
    #  warning Missing architecture definition for t_base_signed, using default size!
    #define  t_base_signed t_s32  // signed basic (native) datatype
#endif

// Check static endianess configuration of current architecture
#ifndef TTC_BASIC_LITTLE_ENDIAN
    #  error Missing architecture definition for TTC_BASIC_LITTLE_ENDIAN. Define it as 1 if current architecture stores 16- and 32-bit values in little-endian format, Define it a 0 otherwise!
#endif
#ifndef TTC_BASIC_BIG_ENDIAN
    #  error Missing architecture definition for TTC_BASIC_BIG_ENDIAN. Define it as 1 if current architecture stores 16- and 32-bit values in big-endian format, Define it a 0 otherwise!
#endif
#if TTC_BASIC_LITTLE_ENDIAN == 1
    #if TTC_BASIC_BIG_ENDIAN != 0
        #    error Invalid configuration. One of TTC_BASIC_LITTLE_ENDIAN and TTC_BASIC_BIG_ENDIAN must be defined as 1, the other one as 0!
    #endif
#endif
#if TTC_BASIC_BIG_ENDIAN == 1
    #if TTC_BASIC_LITTLE_ENDIAN != 0
        #    error Invalid configuration. One of TTC_BASIC_LITTLE_ENDIAN and TTC_BASIC_BIG_ENDIAN must be defined as 1, the other one as 0!
    #endif
#endif

// datatypes used for small amounts
#ifndef base_small_t
    #define base_small_t       t_u8
#endif
#ifndef base_small_signed_t
    #define base_small_signed_t t_s8
#endif

#ifndef NULL
    #define NULL 0
#endif

typedef enum ttc_physical_index_E {    // basic index for physical devices (E.g. 0=first USART, ...)
    ttc_device_1 = 0,                  // physical indices start at zero!
    ttc_device_2,
    ttc_device_3,
    ttc_device_4,
    ttc_device_5,
    ttc_device_6,
    ttc_device_7,
    ttc_device_8,
    ttc_device_9,
    ttc_device_10,
    ttc_device_11,
    ttc_device_12,
    ttc_device_13,
    ttc_device_14,
    ttc_device_15,
    ttc_device_161,
    ttc_device_17,
    ttc_device_18,
    ttc_device_19,
    ttc_device_20,
    ttc_device_unknown
} e_ttc_physical_index;

#if TARGET_DATA_WIDTH==32
    #define TTC_BASIC_MAX_UNSIGNED 0xffffffff
    #define TTC_BASIC_MAX_POSITIVE 0x7fffffff
    #define TTC_BASIC_MAX_NEGATIVE 0xffffffff
#endif
#if TARGET_DATA_WIDTH==16
    #define TTC_BASIC_MAX_UNSIGNED 0xffff
    #define TTC_BASIC_MAX_POSITIVE 0x7fff
    #define TTC_BASIC_MAX_NEGATIVE 0xffff
#endif
#if TARGET_DATA_WIDTH==8
    #define TTC_BASIC_MAX_UNSIGNED 0xff
    #define TTC_BASIC_MAX_POSITIVE 0x7f
    #define TTC_BASIC_MAX_NEGATIVE 0xff
#endif

//}Basic Datatypes
//{ Other Includes *******************************************************

#include "ttc_assert_types.h"

//}
//{ Defines **************************************************************

#ifndef TTC_ASSERT_BASIC    // any previous definition set (Makefile)?
    #define TTC_ASSERT_BASIC 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_BASIC == 1)  // use Assert()s in BASIC code (somewhat slower but alot easier to debug)
    #define Assert_BASIC(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_BASIC_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_BASIC_Readable(Address, Origin) Assert_Readable(Address, Origin)
    #define Assert_BASIC_Address_Matches( Address1, Address2 ) ttc_assert_address_matches( Address1, Address2 )
#else  // use no default Assert()s in BASIC code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_BASIC(Condition, Origin)
    #define Assert_BASIC_Writable(Address, Origin)
    #define Assert_BASIC_Readable(Address, Origin)
    #define Assert_BASIC_Address_Matches( Address1, Address2 )
#endif


#ifndef TTC_ASSERT_BASIC_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_BASIC_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_BASIC_EXTRA == 1)  // use Assert()s in BASIC code (somewhat slower but alot easier to debug)
    #define Assert_BASIC_EXTRA(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_BASIC_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_BASIC_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in BASIC code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_BASIC_EXTRA(Condition, Origin)
    #define Assert_BASIC_EXTRA_Writable(Address, Origin)
    #define Assert_BASIC_EXTRA_Readable(Address, Origin)
#endif

/* TODO message macro
 *
 * This macro can serve as a memo sticker to remember you of something that
 * should be implemented or changed in the future.
 * You could use #warning for this, but TODO() can be switched off to avoid
 * them from flooding your compile log.
 *
 * To switch off TODO messages, simply define TTC_BASIC_TODO to zero in your
 * makefile or change the default value below.
 *
 * Usage:
 *   void foo() {
 *     TODO("Implement foo()")
 *   }
 */
#ifndef TTC_BASIC_TODO
    #define TTC_BASIC_TODO 0 // ==1: display TODO() statements; ==0: don't display them
#endif
#if TTC_BASIC_TODO == 1
    #ifdef __GNUC__
        #define DO_PRAGMA(x) _Pragma (#x)
        #define TODO(x) DO_PRAGMA( message "ToDo: " x )
    #else
        #define TODO(x) __pragma(message("ToDo: " #x ))
    #endif
#else
    #define TODO(x)
#endif

/* INFO message macro
 *
 * This macro can serve to display info messages during compilation
 * You could use #warning for this, but INFO() can be switched off to avoid
 * them from flooding your compile log.
 *
 * To switch off INFO messages, simply define TTC_BASIC_INFO to zero in your
 * makefile or change the default value below.
 *
 * Usage:
 * #if A == 1
 *     INFO("Using A==1")
 * #endif
 */
#ifndef TTC_BASIC_INFO
    #define TTC_BASIC_INFO 1 // ==1: display INFO() statements; ==0: don't display them
#endif
#if TTC_BASIC_INFO == 1
    #ifdef __GNUC__
        #define DO_PRAGMA(x) _Pragma (#x)
        #define INFO(x) DO_PRAGMA( message "INFO: " x )
    #else
        #define INFO(x) __pragma(message("INFO: " #x ))
    #endif
#else
    #define INFO(x)
#endif

/** {_Safe Arrays

 In C and C++, array accesses are not checked at all.
 One can easily access an array element far beyond its allocated memory space and even at a
 negative index.
 Assume the following:
   t_u8 MyArray[10];
   MyArray[10] = 1;

 This is a very common error. In C, arrays start at index 0 and run to Size-1.
 So A runs from MyArray[0] to MyArray[9]. MyArray[10] is one byte behind the allocated array.
 If you were lucky, this access will address a padding byte that has been added by the
 compiler to let the next variable start at a proper address.
 Let me tell you a secret: Nobody is always lucky!

 So we can check array indices all the time by inserting extra code:

 #define A_SIZE 10
 t_u8 MyArray[A_SIZE];

 void setA(t_u8 Index, t_u8 Value) {
   Assert(Index < A_SIZE, ttc_assert_origin_auto);
   MyArray[Index] = Value;
 }

 TheToolChain provides a more elegant support for safe arrays.
 You will have to type only a little more and your code will look pretty much the same as before.
 Safe arrays are defined by A_define() or by A_dynamic().

 See how similar the above code looks when using safe arrays:
 A_define(t_u8, MyArray, 10);   // == t_u8 MyArray[10];
 A(MyArray, 10) = 1;            // == MyArray[10] = 1;  -> WILL ASSERT BECAUSE 10 it out of range 0..9
 t_u8 B = A(MyArray, 9);        // == t_u8 B = MyArray[9];


 Just look how similar the definitions are:

 Defining a static array
             t_u8 MyArray[10];
 equals to   A_define(MyArray, 10);

 Defininig a dynamic array (content buffer is allocated later)
             t_u8* MyArray;
 equals to   A_dynamic(t_u8, MyArray);

 Elements are accessed by use of A() macro.
 MyArray[I] is substituted by A(MyArray, I) in all cases.

 Of course, use of safe arrays inflicts some processing overhead.
 Once your code is assumed to be safe, you can disable all array safety checks by defining ENABLE_TTC_BASIC_SAFE_ARRAYS = 0.
 This can be done best in your makefile (yes its -D):
 COMPILE_OPTS += -DENABLE_TTC_BASIC_SAFE_ARRAYS=0

 Once disabled, safe arrays are as fast as normal arrays!


Example #1 - Simple Static Safe Arrays

    struct Data_t { t_u8 A; t_u8 B; };                // create own structure
    A_define(struct Data_t, Data, 4);                 // equals to: struct Data_t Data[4]

    ...

    A_reset(String);                                  // safe array is placed in data section and must be reset manually if desired!
    A(Data, 0).A = 1;                                 // lvalue     Data[0] = 1;
    t_u8 A2 = A(Data, 1).A;                           // rvalue     t_u8 A2 = Data[1];
    memset(&(A(Data, 2)), 1, sizeof(struct Data_t));  // reference  &(Data[2])
    memset(Aptr(Data), 0, Data.size);                 // reference  Data (Note: using .size field forbids to disable safe arrays!)
    memset(Aptr(Data), 0, sizeof(struct Data_t));     // reference  Data (will also compile if safe arrays are disabled)
    A(Data, 4).B = A2;                                // will Assert!

Debug output:

Breakpoint 1, AssertFunction (Condition=FALSE, ErrorCode=ttc_assert_origin_auto) at ttc-lib/ttc_basic.c:13
13          volatile bool HoldOnAssert=TRUE;
(gdb) bt
#0  AssertFunction (Condition=FALSE, ErrorCode=ttc_assert_origin_auto) at ttc-lib/ttc_basic.c:13
#1  0x08005f00 in _A_index (Size=0x20000194, Index=4) at ttc-lib/ttc_memory.c:139
#2  0x08009f08 in example_usart_init () at examples/example_ttc_usart.c:38
#3  0x0800a3c4 in task_USART (TaskArgument=0x0) at examples/example_ttc_usart.c:100
#4  0x00000000 in ?? ()
(gdb) up
#1  0x08005f00 in _A_index (Size=0x20000194, Index=4) at ttc-lib/ttc_memory.c:139
139         Assert(Index < *Size, ttc_assert_origin_auto);
(gdb) up
#2  0x08009f08 in example_usart_init () at examples/example_ttc_usart.c:38
38          A(Data, 4).B = A2;                                // will Assert!
(gdb)


Example #2 - Using Static Safe Array as String Buffer

    As long as safe arrays are not disabled via ENABLE_TTC_BASIC_SAFE_ARRAYS=0, the size of each array is stored with them.
    This allows an elegant way of using string buffers in a safe manner.

    Let's see an example of vanilla style string buffers:

    #define BUFFER_SIZE 10
    char* Buffer[BUFFER_SIZE];
    ttc_string_snprintf(Buffer, BUFFER_SIZE, "Time = %i s", Seconds++);
    ttc_gfx_text_at(9,2, Buffer, BUFFER_SIZE);


    With static safe arrays, it gets more elegant:

    A_define(char, String, 10); // Note: array data is not initialized!
    ttc_string_snprintf( Aptr(String), String.Size, "%3i", Seconds++);
    ttc_gfx_text_at(9,2, String.Data, String.Size);


Example #3 - Dynamic Safe Arrays are allocated during Runtime from Heap Memory

    If an array shall be allocated dynamically at runtime, then dynamic safe arrays can be used.
    Note: Dynamic safe arrays require that ttc_heap_prepare() has been called before
         (Enable activate.500_ttc_heap.sh in your ./activate_project.sh file)

    #include "ttc_heap.h"
    A_dynamic(t_u8, Vars);      // equals to: t_u8* Vars = NULL;

    ...

    if ( A_is_null(Vars) )
        A_allocate(Vars, 100);  // equals to: Vars = malloc(100);

    // dynamic array may now be used like static array:
    A(Vars,42) = 123;
    t_u8 Value = A(Vars,0);
    A(Vars, 100) = 66;                  // will Assert!


Example #4 - Passing pointer to a Safe Array as function argument

    If a function shall operate on different arrays, it is required to pass the pointer to a
    safe array to it.

    void a() {
      // local definition of safe array similar to t_u8 String[14];
      A_define(t_u8, String, 14);

      // must reset data of safe array manually!
      A_reset(String, 14);

      // pass pointer to safe array as argument to b()
      b(&String);
    }

    void b(A_type(t_u8)* String) {
      A_dynamic_assign(t_u8, StringArray, TaskArgument);
    }
}*/

#ifndef ENABLE_TTC_BASIC_SAFE_ARRAYS
    #define ENABLE_TTC_BASIC_SAFE_ARRAYS 1  // 1: use safe arrays (slower but secure); 0: use original arrays (insecure but no overhead)
#endif

#if (ENABLE_TTC_BASIC_SAFE_ARRAYS==1)

    /** Define a new safe static array as local or global variable
    *
    * Safe version of: TYPE NAME[SIZE];
    *
    * Note: Array data is not reset. Reset it before first use via A_reset!
    *
    * @param TYPE   datatype of individual array entries
    * @param NAME   name of array (-> NAME[])
    * @param SIZE   amount of entries in NAME[] (ignored if safe arrays are enabled)
    */
    #define A_define(TYPE, NAME, SIZE) struct { t_base Size; TYPE Data[SIZE]; } NAME  = { .Size = SIZE }

    /** Initializes all entries of safe static array to zero
    *
    * Safe version of: memclr(NAME, SIZE * sizeof(NAME[0]));
    *
    * Note: Safe static arrays are placed in data-section and are not reset at system start!
    *       Use A_reset() to initialize array data if required!
    *
    * @param NAME   name of array (-> NAME[])
    * @param SIZE   amount of entries in NAME[] (ignored if safe arrays are enabled)
    */
    #define A_reset(NAME, SIZE) ttc_memory_set( NAME.Data, 0, NAME.Size * sizeof(NAME.Data[0]) )

    /** Gives acces to an external safe array being define as global variable in another file.
    *
    * Safe version of: extern TYPE NAME[SIZE];
    *
    * @param TYPE   datatype of individual array entries
    * @param NAME   name of array (-> NAME[])
    * @param SIZE   amount of entries in NAME[] (ignored if safe arrays are enabled)
    *
    */
    #define A_define_extern(TYPE, NAME, SIZE) extern struct { t_base Size; TYPE Data[SIZE]; } NAME

    /** Define a new safe dynamic array as local or global variable.
    *
    * Safe version of: TYPE* NAME;
    *
    * The array is created with Size=0; Data=NULL.
    * Before its first usage, call A_allocate() to allocate its memory for a given size dynamically.
    *
    * @param TYPE   datatype of individual array entries
    * @param NAME   name of array (-> NAME[])
    */
    #define A_dynamic(TYPE, NAME) struct { t_base Size; TYPE* Data; } NAME = { 0, NULL }

    //?#  define A_dynamic_copy(TYPE, NAME) struct { t_base Size; TYPE* Data; } NAME = { 0, NULL }

    /** Define a dynamic safe array and lets it point to given referenced safe array
    *
    * Safe version of: TYPE* NAME; TYPE = &(SOURCE[0]);
    *
    * @param TYPE   datatype of individual array entries
    * @param NAME   name of array (-> NAME[])
    * @param SOURCE name of other array to point to
    */
    #define A_dynamic_assign(TYPE, NAME, SOURCE)     struct { t_base Size; TYPE* Data; } NAME;     NAME.Size = ((struct { t_base Size; TYPE* Data; }*) SOURCE)->Size;     NAME.Data = ((struct { t_base Size; TYPE* Data; }*) SOURCE)->Data

    /** Give acces to an external safe dynamic array being define as global variable in another file.
    *
    * Safe version of: TYPE* NAME; TYPE = &(SOURCE[0]);
    *
    * @param TYPE   datatype of individual array entries
    * @param NAME   name of array (-> NAME[])
    */
    #define A_dynamic_extern(TYPE, NAME) extern struct { t_base Size; TYPE* Data; } NAME

    //X // Provides a typedef name for safe array of given type
    //X #  define A_typedef(TYPE, TYPENAME) typedef struct { t_base Size; TYPE* Data; } TYPENAME

    /** Provide datatype of a safe array storing given type of data
    *
    * Safe version of: TYPE
    *
    * @param TYPE   datatype of individual array entries
    */
    #define A_type(TYPE) struct { t_base Size; TYPE* Data; }

    /** Reset a safe dynamic array to the uninitialized state.
    *
    * Safe version of: NAME = NULL;
    *
    * Note: Memory being allocated for this array will NOT be deallocated. You might loose memory!
    *       => Deallocate its memory before or reset the memory manager.
    *
    * @param NAME   name of array (-> NAME[])
    */
    #define A_dynamic_reset(NAME) NAME.Data = NULL

    /** Check if dynamic array is still unallocated
    *
    * Safe version of: (NAME == NULL)
    *
    * @param NAME   name of array (-> NAME[])
    */
    #define A_is_null(NAME) (NAME.Data == NULL)

    /** Allocate memory for an already defined safe dynamic array.
    *
    * Safe version of: NAME = calloc(SIZE);
    *
    * The array is created with Size=0; Data=NULL.
    * Before its first usage, call A_allocate() to allocate its memory for a given size dynamically.
    *
    * @param NAME   name of array (-> NAME[])
    * @param SIZE   amount of entries in NAME[] (ignored if safe arrays are enabled)
    */
    #define A_allocate(NAME, SIZE) NAME.Data = ttc_heap_alloc_zeroed(sizeof(NAME) * SIZE); NAME.Size = SIZE

    /** Access to individual array elements.
    *
    * Safe version of: NAME[INDEX]
    *
    * @param NAME   name of array (-> NAME[])
    * @param INDEX  array index of NAME[] (valid values = 0..SIZE-1)
    */
    #define A(NAME, INDEX) NAME.Data[_A_index( (t_base*) &NAME, INDEX )]

    /** Return pointer to data being stored in array.
    *
    * Safe version of: &(NAME[])
    *
    * @param NAME   name of array (-> NAME[])
    */
    #define Aptr(NAME) NAME.Data

    /** Return type of data being stored in array to be used in a typecast.
    *
    * Safe version of: typeof(NAME[0])
    *
    * @param NAME   name of array (-> NAME[])
    */
    #define Atype(NAME) typeof(NAME.Data)*

    /** Private helper function: Check array index and simply return index.
    *
    * @param Size   amount of valid entries in array
    * @param Index  array index of NAME[] (valid values = 0..Size-1)
    */
    t_base _A_index( t_base* Size, t_base Index );

#else
    // All array self tests disabled (using just plain C arrays)
    #define A_define(TYPE, NAME, SIZE)        TYPE NAME[SIZE]
    #define A_define_extern(TYPE, NAME, SIZE) extern TYPE NAME[SIZE]
    #define A_dynamic(TYPE, NAME)             TYPE* NAME = NULL
    #define A_dynamic_extern(TYPE, NAME)      extern TYPE* NAME
    #define A_dynamic_reset(NAME)             NAME = NULL
    #define A_pointer(NAME)                   NAME
    #define A_is_null(NAME)                   (NAME == NULL)
    #define A_allocate(NAME, SIZE)            NAME = ttc_heap_alloc(SIZE)
    #define A_type(TYPE)                      TYPE*
    #define A(NAME, INDEX)                    NAME[INDEX]
    #define A_reset(NAME, SIZE)               ttc_memory_set( NAME, 0, SIZE * sizeof(NAME[0]) )
    #define Atype(NAME)                       typeof(NAME)*
    //X #  define A_typedef(TYPE, TYPENAME)     typedef TYPE* TYPENAME
#endif


//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************


//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_basic_errorcode      return codes of BASIC devices
    ec_basic_OK = 0,

    // other warnings go here..

    ec_basic_ERROR,                    // general failure
    ec_basic_NULL,                     // NULL pointer not accepted
    ec_basic_Memory,                   // error occured in ttc_memory
    ec_basic_DeviceNotFound,           // corresponding device could not be found
    ec_basic_InvalidConfiguration,     // sanity check of device configuration failed
    ec_basic_InvalidImplementation,    // your code does not behave as expected
    ec_basic_InvalidArgument,          // invalid value given as argument to a function
    ec_basic_IncompleteImplementation, // implementation incomplete => check corresponding driver implementation
    // other failures go here..

    ec_basic_unknown                // no valid errorcodes past this entry
} e_ttc_basic_errorcode;
typedef enum {   // e_ttc_basic_architecture  types of architectures supported by BASIC driver
    ta_basic_None,           // no architecture selected


    ta_basic_cm3, // automatically added by ./create_DeviceDriver.pl
    ta_basic_stm32f30x, // automatically added by ./create_DeviceDriver.pl
    ta_basic_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
    ta_basic_stm32l0xx, // automatically added by ./create_DeviceDriver.pl
    ta_basic_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
    E_ttc_basic_architecture_cm3, // automatically added by ./create_DeviceDriver.pl
    E_ttc_basic_architecture_stm32l0xx, // automatically added by ./create_DeviceDriver.pl
    E_ttc_basic_architecture_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
    E_ttc_basic_architecture_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_basic_unknown        // architecture not supported
} e_ttc_basic_architecture;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

/** { macros for online task debugging ****************************************
  *
  * All task debugging macros start with Task_.
  * You may use double tab-key in GDB to get a full list in every source that includes ttc_task.h
  */

//{TASKS_MACROS
/** get information about current task
  *
  * Note: This function is specially designed for online debugging.
  *        To find out more about your current task, you may want to issue this in your gdb session:
  *        (gdb) p Tasks_This
  */
#define Tasks_This          (* ttc_task_update_info(NULL, NULL) )

/** get information about single task
  *
  * Note: This function is specially designed for online debugging.
  *        To find out more about your current task, you may want to issue this in your gdb session:
  *        (gdb) p Tasks_Other(HANDLE)
  *
  * @param TASK_HANDLE   handle of task as returned by ttc_task_create()
  */
#define Tasks_Other(TASK_HANDLE) (* ttc_task_update_info(TASK_HANDLE, NULL) )

/** get information about single task
  *
  * Note: This function is specially designed for online debugging.
  *        To find out more about your current task, you may want to issue this in your gdb session:
  *        E.g.: show summary of all tasks
  *        (gdb) p Tasks_All
  *        E.g.: show first running task (array index = 0..Tasks_All.AmountRunning)
  *        (gdb) p * Tasks_All.Running[0]
  *
  * @param TASK_HANDLE   handle of task as returned by ttc_task_create()
  */
#define Tasks_All         (* ttc_task_get_all(NULL) )

/** get information about single task
  *
  * Note: This function is specially designed for online debugging.
  *        To find out more about your current task, you may want to issue this in your gdb session:
  *        (gdb) p Tasks_Amount
  *
  * @param TASK_HANDLE   handle of task as returned by ttc_task_create()
  */
#define Tasks_Amount      ttc_task_get_amount()

//}TASKS_MACROS

#endif // TTC_BASIC_TYPES_H
