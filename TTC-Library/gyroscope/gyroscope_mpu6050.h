#ifndef GYROSCOPE_MPU6050_H
#define GYROSCOPE_MPU6050_H

/** { gyroscope_mpu6050.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for gyroscope devices on mpu6050 architectures.
 *  Structures, Enums and Defines being required by high-level gyroscope and application.
 *
 *  Created from template device_architecture.h revision 22 at 20150121 10:09:22 UTC
 *
 *  Note: See ttc_gyroscope.h for description of mpu6050 independent GYROSCOPE implementation.
 *  
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_GYROSCOPE_MPU6050
//
// Implementation status of low-level driver support for gyroscope devices on mpu6050
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_GYROSCOPE_MPU6050 '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_GYROSCOPE_MPU6050 == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_GYROSCOPE_MPU6050 to one from '-', 'o', '+' 
#endif

//}EXTENSION_STATUS_TTC_GYROSCOPE_MPU6050

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "gyroscope_mpu6050.c"
//
#include "gyroscope_mpu6050_types.h"
#include "../ttc_gyroscope_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_gyroscope_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_gyroscope_foo
//
#define ttc_driver_gyroscope_deinit(Config) gyroscope_mpu6050_deinit(Config)
#define ttc_driver_gyroscope_get_features(Config) gyroscope_mpu6050_get_features(Config)
#define ttc_driver_gyroscope_init(Config) gyroscope_mpu6050_init(Config)
#define ttc_driver_gyroscope_load_defaults(Config) gyroscope_mpu6050_load_defaults(Config)
#define ttc_driver_gyroscope_prepare() gyroscope_mpu6050_prepare()
#define ttc_driver_gyroscope_reset(Config) gyroscope_mpu6050_reset(Config)
#define ttc_driver_gyroscope_read_measures(Config) gyroscope_mpu6050_read_measures(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_gyroscope.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_gyroscope.h for details.
 *
 * You normally should not need to add functions here. 
 * Add your private functions in the private section below. 
 */


/** shutdown single GYROSCOPE unit device
 * @param Config        pointer to struct t_ttc_gyroscope_config (must have valid value for PhysicalIndex)
 * @return              == 0: GYROSCOPE has been shutdown successfully; != 0: error-code
 */
e_ttc_gyroscope_errorcode gyroscope_mpu6050_deinit(t_ttc_gyroscope_config* Config);


/** fills out given Config with maximum valid values for indexed GYROSCOPE
 * @param Config        = pointer to struct t_ttc_gyroscope_config (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_gyroscope_errorcode gyroscope_mpu6050_get_features(t_ttc_gyroscope_config* Config);


/** initializes single GYROSCOPE unit for operation
 * @param Config        pointer to struct t_ttc_gyroscope_config (must have valid value for PhysicalIndex)
 * @return              == 0: GYROSCOPE has been initialized successfully; != 0: error-code
 */
e_ttc_gyroscope_errorcode gyroscope_mpu6050_init(t_ttc_gyroscope_config* Config);


/** loads configuration of indexed GYROSCOPE unit with default values
 * @param Config        pointer to struct t_ttc_gyroscope_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_gyroscope_errorcode gyroscope_mpu6050_load_defaults(t_ttc_gyroscope_config* Config);


/** Prepares gyroscope Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void gyroscope_mpu6050_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_gyroscope_config (must have valid value for PhysicalIndex)
 */
void gyroscope_mpu6050_reset(t_ttc_gyroscope_config* Config);

/** manually read latest measures from indexed gyroscope device
 *
 * @param LogicalIndex  logical index of gyroscope device (1..ttc_gyroscope_get_max_index() )
 * @return              !=NULL: pointer to measures being read; ==NULL: measures could NOT be read
 * @param Config   =
 */
t_ttc_gyroscope_measures* gyroscope_mpu6050_read_measures(t_ttc_gyroscope_config* Config);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _gyroscope_mpu6050_foo(t_ttc_gyroscope_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //GYROSCOPE_MPU6050_H