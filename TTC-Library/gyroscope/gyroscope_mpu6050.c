/** { gyroscope_mpu6050.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for gyroscope devices on mpu6050 architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150121 10:09:22 UTC
 *
 *  Note: See ttc_gyroscope.h for description of mpu6050 independent GYROSCOPE implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "gyroscope_mpu6050.h".
//
#include "gyroscope_mpu6050.h"
#include "../ttc_task.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

e_ttc_gyroscope_errorcode gyroscope_mpu6050_deinit(t_ttc_gyroscope_config* Config) {
    Assert_GYROSCOPE(Config, ttc_assert_origin_auto); // pointers must not be NULL

    return (e_ttc_gyroscope_errorcode) 0;
}
e_ttc_gyroscope_errorcode gyroscope_mpu6050_get_features(t_ttc_gyroscope_config* Config) {
    Assert_GYROSCOPE(Config, ttc_assert_origin_auto); // pointers must not be NULL



    return (e_ttc_gyroscope_errorcode) 0;
}
e_ttc_gyroscope_errorcode gyroscope_mpu6050_init(t_ttc_gyroscope_config* Config) {
    Assert_GYROSCOPE(Config, ttc_assert_origin_auto); // pointers must not be NULL


    t_u8 LogicalIndex = Config->LogicalIndex;
    Assert_GYROSCOPE(LogicalIndex > 0, ttc_assert_origin_auto); // logical index starts at 1
    Assert_GYROSCOPE(Config != NULL, ttc_assert_origin_auto);
    if (LogicalIndex > TTC_GYROSCOPE_AMOUNT)
        return ec_gyroscope_DeviceNotFound;


    t_ttc_gyroscope_architecture * Gyr_Arch = &Config->LowLevelConfig;

    Gyr_Arch->t_register.who_am_i = 0x75;
    Gyr_Arch->t_register.pwr_mgmt_1 = 0x6B;

    Gyr_Arch->t_register.outx_l = 0x44;
    Gyr_Arch->t_register.outx_h = 0x43;

    Gyr_Arch->t_register.outy_l = 0x46;
    Gyr_Arch->t_register.outy_h = 0x45;

    Gyr_Arch->t_register.outz_l = 0x48;
    Gyr_Arch->t_register.outz_h = 0x47;

    t_u8 Data;

    // Reset MPU6050
    Data = 0x80;
    if (ttc_i2c_master_write_register(Config->LogicalIndex_Interface, Gyr_Arch->address, Gyr_Arch->t_register.pwr_mgmt_1, E_ttc_i2c_address_type_Default, 1, &Data) )
        return ec_gyroscope_CommunicationI2C;

    ttc_task_msleep(50);

    t_u8 StatusRegister = 0;
    if ( ttc_i2c_master_read_register(Config->LogicalIndex_Interface, Gyr_Arch->address, Gyr_Arch->t_register.who_am_i, E_ttc_i2c_address_type_Default, 1,  &StatusRegister) )
        return ec_gyroscope_CommunicationI2C;
    ttc_task_msleep(50);

    if(StatusRegister!=104){
        return ec_gyroscope_InvalidConfiguration;
    }

   //Wake up MPU6050
    do {
        Data = 0x00;
        if ( ttc_i2c_master_write_register(Config->LogicalIndex_Interface, Gyr_Arch->address, Gyr_Arch->t_register.pwr_mgmt_1, E_ttc_i2c_address_type_Default, 1, &Data) )
            return ec_gyroscope_CommunicationI2C;
        ttc_task_msleep(50);


        if ( ttc_i2c_master_read_register(Config->LogicalIndex_Interface, Gyr_Arch->address, Gyr_Arch->t_register.pwr_mgmt_1, E_ttc_i2c_address_type_Default, 1,  &StatusRegister) )
            return ec_gyroscope_CommunicationI2C;
        ttc_task_msleep(50);

    } while(StatusRegister!=0);

    //Set GYRO Config -- 250°/s
    Data = 0b00000000;
    if ( ttc_i2c_master_write_register(Config->LogicalIndex_Interface, Gyr_Arch->address, 0x1B, E_ttc_i2c_address_type_Default, 1,  &Data) )
        return ec_gyroscope_CommunicationI2C;
    if ( ttc_i2c_master_read_register(Config->LogicalIndex_Interface, Gyr_Arch->address, 0x1B, E_ttc_i2c_address_type_Default, 1,  &StatusRegister) )
        return ec_gyroscope_CommunicationI2C;

    return (e_ttc_gyroscope_errorcode) 0;
}
e_ttc_gyroscope_errorcode gyroscope_mpu6050_load_defaults(t_ttc_gyroscope_config* Config) {
    Assert_GYROSCOPE(Config, ttc_assert_origin_auto); // pointers must not be NULL

    Config->Architecture = ta_gyroscope_mpu6050;
    Config->LowLevelConfig.address = 0x68;

    return (e_ttc_gyroscope_errorcode) 0;
}
void gyroscope_mpu6050_prepare() {



}
void gyroscope_mpu6050_reset(t_ttc_gyroscope_config* Config) {
    Assert_GYROSCOPE(Config, ttc_assert_origin_auto); // pointers must not be NULL

    gyroscope_mpu6050_load_defaults(Config);

}
t_ttc_gyroscope_measures* gyroscope_mpu6050_read_measures(t_ttc_gyroscope_config* Config) {
    Assert_GYROSCOPE(Config, ttc_assert_origin_auto); // pointers must not be NULL

    t_u8 LogicalIndex = Config->LogicalIndex;
    Assert_GYROSCOPE(LogicalIndex > 0, ttc_assert_origin_auto); // logical index starts at 1
    Assert_GYROSCOPE(Config != NULL, ttc_assert_origin_auto);

    t_ttc_gyroscope_architecture * GYROSCOPE_Arch = &Config->LowLevelConfig;

    t_u8 StatusRegister = 0;
    ttc_i2c_master_read_register(Config->LogicalIndex_Interface, GYROSCOPE_Arch->address, GYROSCOPE_Arch->t_register.who_am_i, E_ttc_i2c_address_type_Default, 1,  &StatusRegister);
    Assert(StatusRegister == 104, ttc_assert_origin_auto);

    t_s16 GyroX;
    t_s16 GyroY;
    t_s16 GyroZ;

    Assert_GYROSCOPE( ttc_i2c_master_read_register(Config->LogicalIndex_Interface, GYROSCOPE_Arch->address, GYROSCOPE_Arch->t_register.outx_l, E_ttc_i2c_address_type_Default, 1,  (t_u8*) &GyroX ), ttc_assert_origin_auto );
    //ttc_task_msleep(50);

    Assert_GYROSCOPE( ttc_i2c_master_read_register(Config->LogicalIndex_Interface, GYROSCOPE_Arch->address, GYROSCOPE_Arch->t_register.outx_h, E_ttc_i2c_address_type_Default, 1,  ( (t_u8*) &GyroX ) + 1), ttc_assert_origin_auto );
    //ttc_task_msleep(50);

    Assert_GYROSCOPE( ttc_i2c_master_read_register(Config->LogicalIndex_Interface, GYROSCOPE_Arch->address, GYROSCOPE_Arch->t_register.outy_l, E_ttc_i2c_address_type_Default, 1,  (t_u8*) &GyroY ), ttc_assert_origin_auto );
    //? return ec_gyroscope_CommunicationI2C;
    //ttc_task_msleep(100);

    Assert_GYROSCOPE( ttc_i2c_master_read_register(Config->LogicalIndex_Interface, GYROSCOPE_Arch->address, GYROSCOPE_Arch->t_register.outy_h, E_ttc_i2c_address_type_Default, 1,  ( (t_u8*) &GyroY ) + 1), ttc_assert_origin_auto );
    //? return ec_gyroscope_CommunicationI2C;
    //ttc_task_msleep(50);

    Assert_GYROSCOPE( ttc_i2c_master_read_register(Config->LogicalIndex_Interface, GYROSCOPE_Arch->address, GYROSCOPE_Arch->t_register.outz_l, E_ttc_i2c_address_type_Default, 1,  (t_u8*) &GyroZ ), ttc_assert_origin_auto );
    //? return ec_gyroscope_CommunicationI2C;
    //ttc_task_msleep(50);

    Assert_GYROSCOPE( ttc_i2c_master_read_register(Config->LogicalIndex_Interface, GYROSCOPE_Arch->address, GYROSCOPE_Arch->t_register.outz_h, E_ttc_i2c_address_type_Default, 1,  ( (t_u8*) &GyroZ ) + 1), ttc_assert_origin_auto );
    //? return ec_gyroscope_CommunicationI2C;
    //ttc_task_msleep(50);

    Config->Measures.GyroX = GyroX;
    Config->Measures.GyroY = GyroY;
    Config->Measures.GyroZ = GyroZ;

    return &Config->Measures;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
