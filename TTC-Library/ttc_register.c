/** { ttc_register.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for register devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 23 at 20140223 10:54:39 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_register.h"

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of register devices.
 *
 */


//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

void ttc_register_prepare() {
    // add your startup code here (Singletasking!)
    _driver_register_prepare();
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_register(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
