/** { ttc_network.h *******************************************************

                           The ToolChain

   High-Level interface for NETWORK device.

   Structures, Enums and Defines being required by high-level network and application.

   Authors:

}*/

#ifndef TTC_NETWORK_H
#define TTC_NETWORK_H

//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "ttc_list_types.h"
#include "interfaces/ttc_network_interface.h"
#include "ttc_network_types.h"
#include "network/support/network_rimeaddr.h"

//} Includes
//{ Check for missing low-level implementations **************************

// Note: functions declared below must be redefined to implementations provided by low-level Driver!

/** Prepares network Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_network_prepare();

/** loads configuration of indexed NETWORK interface with default values
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_network_errorcode _driver_network_load_defaults( t_ttc_network_config* Config );

/** shutdown single NETWORK device
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return              == 0: NETWORK has been shutdown successfully; != 0: error-code
 */
e_ttc_network_errorcode _driver_network_deinit( t_ttc_network_config* Config );

/** initializes single NETWORK
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return              == 0: NETWORK has been initialized successfully; != 0: error-code
 */
e_ttc_network_errorcode _driver_network_init( t_ttc_network_config* Config );
//}
//{ Defines/ TypeDefs ****************************************************

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level network only go here...

//} Structures/ Enums
//{ Function prototypes **************************************************

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_network_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_NETWORK_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_network_config* ttc_network_get_configuration( t_u8 LogicalIndex );

/** Loads the default configuration for the device to be used
 *
 * @param LogicalIndex    device index of device to init (1..ttc_NETWORK_get_max_LogicalIndex())
 */
e_ttc_network_errorcode ttc_network_load_defaults( t_ttc_network_config* Config );

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_NETWORK_get_max_LogicalIndex())
 */
void ttc_network_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_NETWORK_get_max_LogicalIndex())
 */
void ttc_network_deinit( t_u8 LogicalIndex );

/** maps from logical to physical device index
 *
 * High-level networks (ttc_network_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_NETWORKn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any
 * order.
 *
 * @param LogicalIndex  logical index of network device (1..ttc_network_get_max_index() )
 * @return              physical index of network device (0 = first physical network device, ...)
 */
t_u8 ttc_network_logical_2_physical_index( t_u8 LogicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_NETWORK_get_max_LogicalIndex())
 */
void ttc_network_reset( t_u8 LogicalIndex );

// additional prototypes go here...
/** sends data from given buffer via network to given target address
 *
 * @param LogicalIndex  1..TTC_NETWORK_MAX
 * @param Target        protocol address of target node
 * @param Buffer        memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[] (even zero bytes are sent)
 */
e_ttc_network_errorcode ttc_network_send_unicast( t_u8 LogicalIndex, ttc_network_address_t Target, const t_u8* Buffer, t_base Amount );

/** sends data from given buffer via network to all other nodes
 *
 * @param LogicalIndex  1..TTC_NETWORK_MAX
 * @param Buffer        memory location from which to read data
 * @param Amount        amount of bytes to send from Buffer[] (even zero bytes are sent)
 */
e_ttc_network_errorcode ttc_network_send_broadcast( t_ttc_network_config* Config, const t_u8* Buffer, t_base Amount );

/** registers function to be called for every packet being received from network
 *
 * @param LogicalIndex  1..TTC_NETWORK_MAX
 * @param receivePacket function will be called whenever a valid packet has beed received
 */
void ttc_network_receive( t_u8 LogicalIndex );

/** returns list of all known neighbour nodes
 *
 * @param LogicalIndex  1..TTC_NETWORK_MAX
 * @return  list of protocol adresses of neighbour nodes
 */
t_ttc_list* ttc_network_get_neighbours( t_u8 LogicalIndex );

/** returns a network address on rime address format
 *
 * @param Address  Address in network address format
 * @return  Address in rime format
 */
t_rimeaddr ttc_network_address_to_rimeaddr( ttc_network_address_t* Address );
//} Function prototypes
//{ private Function prototypes ******************************************

/**
 *
 * Note: This function is marked private and should be called from outside!
 *
 * @param
 * @return
 */
void _ttc_network_();


//}PrivateFunctions
//{ Macros ***************************************************************

//}Macros

#endif //TTC_NETWORK_H
