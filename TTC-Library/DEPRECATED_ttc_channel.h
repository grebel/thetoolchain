#ifndef TTC_CHANNEL_H
#define TTC_CHANNEL_H

/*{ TTC_CHANNEL.h **********************************************************
 
                      The ToolChain
                      
   Device independent support for bidirectional communication channels 
   between application or device and any type of io device.
   
   IO devices can be:
   - io interfaces like usart, spi, i2c, ...
   - radio interfaces
   - network protocols
   - filesystem drivers
   - ...
   
   Procedure of channel creation:
   - configure connected device/ driver via activates + makefile settings
   - initialize connected device via its own _init()
   - create channel
   - add nodes to the channel
   - configure individual devices on the channel

   // example: initialising USART #1 with all defaults
   Assert(ttc_usart_init(1) == ec_usart_OK);

   // create a new channel
   ttc_channel_handle_t MyChannel = ttc_channel_create();

   // add USART #1 as node to the channel
   ttc_channel_node_handle_t Node_USART = ttc_channel_add_application(MyChannel, tcd_USART, 1);

   // add application as node to the channel
   ttc_channel_node_handle_t Node_Application = ttc_channel_add_application(MyChannel, receiveData);

   // set device dependend properties for individual nodes on the channel
   ttc_channel_set_property(MyChannel, tcnp_Destination, 0x123); // destination adress ignored if not supported by connected device

   // send constant string to channel
   ttc_channel_send_string(ChannelIndex, "Hello World\n", -1);
   
   // get memory block and send it out
   ttc_heap_block_t* NewBlock = ttc_channel_get_empty_block(MyChannel);
   memcpy(NewBlock.Buffer, "Some Data", 9);
   NewBlock.Size = 9;
   ttc_channel_send_block(MyChannel, NewBlock);
   
   
   // This function gets called whenever data has been sent out from one node connected to same channel
   ttc_heap_block_t* receiveData(ttc_channel_node_config_t* SourceNode, ttc_heap_block_t* Block) {
     // received Block->Size bytes of data in Block->Buffer
     
     // ToDo: How to retrieve corresponding meta data: u8_t RSSI, u8_t TargetAddress
     
     // return Block: allow immediate release of this block
     // return NULL:  we must call ttc_channel_release_block(Packet) later to allo reuse of this block!
     return Block;
   }
   
   written by Gregor Rebel 2012
   
}*/
//{ Defines/ TypeDefs ****************************************************

//} Defines
//{ Includes *************************************************************

#include "ttc_basic.h"
#include "ttc_memory.h"
#include "ttc_string.h"
#include "ttc_task.h"
#include "ttc_channel.h"
#include "ttc_usart.h"
#include "ttc_spi.h"
#include "ttc_i2c.h"

// include structures & enums of connectable device drivers
#ifdef EXTENSION_500_ttc_usart      // defined by extension makefile
  #include "ttc_usart.h"
#endif
#ifdef EXTENSION_500_ttc_network    // defined by extension makefile
  #include "ttc_network.h"
#endif
#ifdef EXTENSION_500_ttc_radio      // defined by extension makefile
  #include "ttc_radio.h"
#endif
#ifdef EXTENSION_500_ttc_spi        // defined by extension makefile
  #include "ttc_spi.h"
#endif
#ifdef EXTENSION_500_ttc_i2c        // defined by extension makefile
  #include "ttc_i2c.h"
#endif

//} Includes
//{ Structures/ Enums ****************************************************

//} Structures/ Enums
//{ Function prototypes **************************************************

/** returns maximum allowed amount of channels to be created (compile time setting)
 *
 * Note: The amount of created channels is limited by predefined value of TTC_CHANNEL_MAX_AMOUNT (-> ttc_channel_types.h)
 *
 * @return        maximum allowed amount of channels to be created
 */
u8_t ttc_channel_get_max_index();

/** creates a new channel to an already initialized device
 *
 * Note: The amount of created channels is limited by predefined value of TTC_CHANNEL_MAX_AMOUNT (-> ttc_channel_types.h)
 *
 * @return        logical index of created channel
 */
u8_t ttc_channel_create();

/** adds an application node to given channel
 *
 * @param ChannelIndex logical index of a channel as returned by ttc_channel_create() before
 * @param rxFunction   pointer to function that will parse data received from other channel nodes
 * @return             handle of created channel node; required to send data to the channel
 */
ttc_channel_node_handle_t ttc_channel_add_application(u8_t ChannelIndex,  ttc_heap_block_t* (*RxFunction)(ttc_channel_node_handle_t, ttc_heap_block_t *) );

/** adds a generic device as node to given channel
 *
 * @param ChannelIndex logical index of a channel as returned by ttc_channel_create() before
 * @param Type         type of device to add as node
 * @param DeviceIndex  logical index of device unit to use (E.g. for Type==tcd_USART: DeviceIndex=n means to use TTC_USARTn)
 * @return             handle of created channel node; required to send data to the channel
 */
ttc_channel_node_handle_t ttc_channel_add_device(u8_t ChannelIndex,  ttc_channel_node_type_e Type, u8_t DeviceIndex);

/** removes a single node from a channel (*Node gets invalid and must not be used anymore)
 *
 * @param ChannelIndex logical index of a channel as returned by ttc_channel_create() before
 */
void ttc_channel_del_node(u8_t ChannelIndex, ttc_channel_node_handle_t Node);

/** adds a generic node to given channel
 *
 * Note: This function is private and should not be called from outside!
 * Note: This function is not thread safe!
 * Note: This function will Assert() in case of a detected illegal memory configuration.
 *
 * @param Channel      configuration data of an already created channel
 * @param NewNode      configuration of node to add to Channel
 */
void _ttc_channel_add_node(ttc_channel_t* Channel, ttc_channel_node_t* NewNode);

/** releases an allocated node for later reuse (will not free any memory)
 *
 * Note: This function is private and should not be called from outside!
 * Note: Delete Node from its channel BEFORE releasing it!
 *
 * @return             pointer to configuration of an unused node that yet does not belong to a channel
 */
void _ttc_channel_release_node(ttc_channel_node_t* Node);

/** returns configuration data of indexed channel (creates new one if required)
 *
 * Note: This function is private and should not be called from outside!
 * Note: This function is not thread safe!
 * Note: This function will Assert() in case of an illegal ChannelIndex or an out-of-memory situation.
 *
 * @param ChannelIndex logical index of a channel as returned by ttc_channel_create() before
 * @return             pointer to configuration of first node of indexed channel
 */
ttc_channel_t* _ttc_channel_get_configuration(u8_t ChannelIndex);

/** returns configuration data of indexed channel (creates new one if required)
 *
 * Note: This function is private and should not be called from outside!
 * Note: This function is not thread safe!
 * Note: This function will Assert() in case of a detected illegal memory configuration.
 *
 * @param Channel      configuration data of an already created channel
 * @return             !=NULL: pointer to configuration of last node of given channel
 *                     ==NULL: given channel has no nodes
 */
ttc_channel_node_t* _ttc_channel_get_last_node(ttc_channel_t* Channel);

/** checks if special property is available on given channel node.
 *
 * Not all properties are available for all devices.
 *
 * @param ChannelNode    handle of channel node being added to a created channel before
 * @param Property       index of requested property
 * @return               == TRUE: given Property is available on this node
 */
BOOL ttc_channel_has_node_property(ttc_channel_node_handle_t ChannelNode, ttc_channel_node_property_e Property);

/** returns value of a special property of given channel node.
 *
 * Not all properties are available for all devices. Unsupported properties will always return 0.
 *
 * @param ChannelNode      handle of channel node being added to a created channel before
 * @return                 current value of requested property
 */
Base_t ttc_channel_get_node_property(ttc_channel_node_handle_t ChannelNode, ttc_channel_node_property_e Property);

/** set value of a special property of given channel node.
 *
 * Not all properties are available for all devices. Unsupported properties will be silently ignored.
 *
 * @param ChannelNode    handle of channel node being added to a created channel before
 * @return               value of
 */
void ttc_channel_set_node_property(ttc_channel_node_handle_t ChannelNode, ttc_channel_node_property_e Property, Base_t NewValue);

/** delivers a memory block usable as transmit or receive buffer in given channel
 *
 * @return               != NULL: pointer to new or reused network packet;
 *                       == NULL: currently not available (-> increase channel_Generic->Size_Queue_Tx or wait until a packet is released)
 */
ttc_heap_block_t* ttc_channel_get_empty_block(u8_t ChannelIndex);


/** delivers a memory block usable as transmit or receive buffer in given channel
 *
 * Note: This function is private and should not be called from outside
 *
 * @return               != NULL: pointer to new or reused network packet;
 *                       == NULL: currently not available (-> increase channel_Generic->Size_Queue_Tx or wait until a packet is released)
 */
ttc_heap_block_t* _ttc_channel_get_empty_block(ttc_channel_t* Channel);

/** returns newly allocated or reused configuration data of an unused channel node
 *
 * Note: The amount of created channel nodes is only limited by available heap memory
 * Note: This function will Assert() in case of an out-of-memory situation.
 *
 * @return             pointer to configuration of an unused node that yet does not belong to a channel
 */
ttc_channel_node_t* _ttc_channel_get_empty_node();

/** Returns last error that has been occured in given channel + resets error
 *
 * Note: channel must be initialized before!
 * Note: This function is partially thread safe (use it for each channel from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param Channel         configuration of already initialized channel
 * @param ErrorNode       will be loaded with handler of node that reported the error
 * @return                 == 0: no error occured since last ttc_channel_get_last_error()-call; != 0: last occured error-code
 */
ttc_channel_errorcode_e ttc_channel_get_last_error(ttc_channel_handle_t Channel, ttc_channel_node_handle_t* ErrorNode);

/** Send out data from given memory block to all nodes added to given channel.
 *
 * Note: channel must be initialized before!
 * Note: This function is partially thread safe (use it for each channel from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param ChannelIndex    device index of channel to init (1..ttc_channel_get_max_index()); 1 = TTC_CHANNEL1, ...
 * @param Block           memory block being allocated by ttc_heap_alloc_block()/ ttc_heap_alloc_virtual_block()
 *                        Block goes into channel custody and must not be used by caller before it has been released!
 */
ttc_channel_errorcode_e ttc_channel_send_block(u8_t ChannelIndex, ttc_heap_block_t* Block);

/** Send out data from given memory block to all nodes added to given channel except to Sender.
 *
 * Note: channel must be initialized before!
 * Note: This function blocks until all nodes have acknowledged to send the block.
 *
 * @param ChannelIndex    device index of channel to init (1..ttc_channel_get_max_index()); 1 = TTC_CHANNEL1, ...
 * @param Block           memory block being allocated by ttc_heap_alloc_block()/ ttc_heap_alloc_virtual_block()
 *                        Block goes into channel custody and must not be used by caller before it has been released!
 * @return                 == 0: Buffer has been successfully queued for transmit; != 0: error-code (-> ttc_channel_get_last_error())
 */
ttc_channel_errorcode_e ttc_channel_send_block_from_node(ttc_channel_node_t* Sender, ttc_heap_block_t* Block);

/** Send out data from given memory block to all nodes added to given channel.
 *
 * Note: channel must be initialized before!
 * Note: This function is partially thread safe (use it for each channel from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param Channel         configuration of channel to send Block to
 * @param Block           memory block being allocated by ttc_heap_alloc_block()/ ttc_heap_alloc_virtual_block()
 * @param ExceptNode      == NULL: send Block to all nodes in channel; != NULL: do not send Block to this node
 *                        Block goes into channel custody and must not be used by caller before it has been released!
 * @return                 == 0: Buffer has been successfully queued for transmit; != 0: error-code (-> ttc_channel_get_last_error())
 */
ttc_channel_errorcode_e _ttc_channel_send_block(ttc_channel_t* Channel, ttc_heap_block_t* Block, ttc_channel_node_t* ExceptNode);

/** Send out given amount of raw bytes from buffer to all nodes added to given channel.
 * Will not interpret bytes in buffer.
 *
 * Note: channel must be initialized before!
 * Note: This function is partially thread safe (use it for each channel from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * 
 * @param ChannelIndex     device index of channel to init (1..ttc_channel_get_max_index()); 1 = TTC_CHANNEL1, ...
 * @param Buffer           memory location from which to read data
 * @param Amount           amount of bytes to send from Buffer[]
 * @return                 == 0: Buffer has been successfully queued for transmit; != 0: error-code
 */
ttc_channel_errorcode_e ttc_channel_send_raw(u8_t ChannelIndex, const u8_t* Buffer, Base_t Amount);

/** Send out given bytes from buffer until first zero byte or MaxLength reached to all nodes in given channel.
 *
 * Note: channel must be initialized before!
 * Note: This function is partially thread safe (use it for each channel from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * 
 * @param ChannelIndex     device index of channel to init (1..ttc_channel_get_max_index()); 1 = TTC_CHANNEL1, ...
 * @param Buffer           memory location from which to read data
 * @param MaxLength        no more than this amount of bytes will be send
 * @return                 == 0: Buffer has been successfully queued for transmit; != 0: error-code
 */
ttc_channel_errorcode_e ttc_channel_send_string(u8_t ChannelIndex, const u8_t* String, Base_t MaxSize);

/** will send given constant string to default channel being set by ttc_channel_stdout_set()
 *
 * Note: This function will not copy string to temporary buffer, thus saving memory + cpu time
 *
 * @param ChannelIndex   device index of channel to use (1..ttc_channel_get_max_index()); 1 = TTC_CHANNEL1, ...
 * @param String         pointer to constant string (must not change during transmission!)
 * @param MaxSize        size of String[] buffer (contained, zero-terminated string may be shorter)
 * @return                 == 0: Buffer has been successfully queued for transmit; != 0: error-code
 */
ttc_channel_errorcode_e ttc_channel_send_string_const(u8_t ChannelIndex, const u8_t* String, Base_t MaxSize);

/** Defines which logical device shall be used by ttc_channel_stdout_send_string()/ ttc_channel_stdout_send_block()
 *
 * @param ChannelIndex   >0: logical device index of channel to use (1..ttc_channel_get_max_index()); 1 = TTC_CHANNEL1, ...
 * @return               != tre_OK: error message
 */
ttc_channel_errorcode_e ttc_channel_stdout_set(u8_t ChannelIndex);

/** will send given memory block to default channel being set by ttc_channel_stdout_set()
 *
 * @param Block         pointer to a ttc_heap_block_t (will be released after sending automatically)
 */
void ttc_channel_stdout_send_block(ttc_heap_block_t* Block);

/** will send given volatile string to default channel being set by ttc_channel_stdout_set()
 *
 * @param String         pointer to constant string (must not change during transmission!)
 * @param MaxSize        size of String[] buffer (contained, zero-terminated string may be shorter)
 */
void ttc_channel_stdout_send_string(const u8_t* String, Base_t MaxSize);

/** Waits until transmit queue has run empty on all nodes in given channel
 *
 * Note: Channel must be initialized before!
 * Note: This function blocks until all bytes have been sent!
 * Note: You may want to call ttc_task_begin_criticalsection() before to be sure that no other task transmits in parallel.
 *
 * @param ChannelIndex      device index of channel to init (1..ttc_channel_get_max_index())
 */
void ttc_channel_flush_tx(u8_t ChannelIndex);

/** Uses block management of given node if provides smaller memory blocks than current provider of get_empty_blocks()
 *
 * Note: channel must be initialized before!
 * Note: This function is not thread safe
 * Note: This function is private and must not be called from outside!
 *
 * @param Channel  initialized channel that shall be updated
 * @param Node     handle of channel node that has been attached to a device
 */
void _ttc_channel_update_block_management(ttc_channel_t* Channel, ttc_channel_node_t* Node);

/** releases a used Tx/ Rx packet for later reuse
 *
 * Note: This function is private and should not be called from outside
 *
 * @param Packet        pointer to a ttc_heap_block_t
 */
void _ttc_channel_release_block(ttc_heap_block_t* Packet);

/** returns type of low-level driver that is responsible for indexed channel
 *
 * @param  ChannelIndex  logical index of channel device (1=TTC_CHANNEL1, ...)
 * @return each low-level channel driver has its own unique enum
 */
ttc_channel_node_type_e ttc_channel_get_node_type(ttc_channel_node_handle_t ChannelNode);

/** Reads last received packet from single node into given memory block
 *
 * Note: channel must be initialized before!
 * Note: This function is partially thread safe (use it for each channel from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 * Note: This function is private and must not be called from outside!
 *
 * @param channel_Generic pointer to configuration of already initialized channel
 * @param Buffer          will be loaded with pointer to receive buffer
 * @param Amount          will be loaded with amount of valid bytes in Buffer[]
 * @return                != NULL: given memory block can be reused; ==NULL: memory block is still in use
 */
ttc_heap_block_t* _ttc_channel_read_packet(ttc_channel_node_handle_t ChannelNode, ttc_heap_block_t* Block);

/** returns amount of bytes waiting in receive queue of channel node
 *
 * Note: This function is private and must not be called from outside!
 *
 * @param channel_Generic  pointer to configuration of already initialized channel
 * @return               amount of bytes waiting in receive queue
 */
u8_t _ttc_channel_check_bytes_received(ttc_channel_node_handle_t ChannelNode);

/** Enables communication between a device and given channel node
 *
 * Note: channel must be initialized before!
 * Note: This function is not thread safe
 * Note: This function is private and must not be called from outside!
 *
 * @param ChannelNode  handle of channel node being added to a created channel before
 * @return != 0: error occured during attach
 */
ttc_channel_errorcode_e _ttc_channel_device_attach(ttc_channel_node_t* ChannelNode);

/** Breaks communication between a device and given channel node
 *
 * Note: channel must be initialized before!
 * Note: This function is not thread safe
 * Note: This function is private and must not be called from outside!
 *
 * @param ChannelNode  handle of channel node being added to a created channel before
 * @return != 0: error occured during detach
 */
ttc_channel_errorcode_e _ttc_channel_device_detach(ttc_channel_node_t* ChannelNode);

/** Receives data from a connected USART device
 *
 * Note: channel must be initialized before!
 * Note: This function is not thread safe
 * Note: This function is private and must not be called from outside!
 *
 * @param Argument      pointer to a ttc_channel_node_t storing configuration data of node being attached to receiving usart
 * @param USART_Generic configuration data of a configured USART
 * @param Block         memory block storing received data
 * @return != NULL: memory block can be reused immediately
 */
ttc_heap_block_t* _ttc_channel_receive_from_usart(void* Argument, ttc_usart_config_t* USART_Generic, ttc_heap_block_t* Block);


//}Function prototypes

#endif //TTC_CHANNEL_H
