/** { ttc_interface_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for INTERFACE device.
 *  Structures, Enums and Defines being required by both, high- and low-level interface.
 *  This file does not provide function declarations!
 *  
 *  Created from template ttc_device_types.h revision 27 at 20150310 09:22:28 UTC
 *
 *  Authors: Gregor Rebel
 * 
}*/

#ifndef TTC_INTERFACE_TYPES_H
#define TTC_INTERFACE_TYPES_H

//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_interface.h" or "ttc_interface.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h" // only required to help IDE to know constant definitions
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
#ifdef EXTENSION_interface_ste101p
#  include "interface/interface_ste101p_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)
 
//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

//{  TTC_INTERFACEn has to be defined as constant by makefile.100_board_*
#ifdef TTC_INTERFACE5
  #ifndef TTC_INTERFACE4
    #error TTC_INTERFACE5 is defined, but not TTC_INTERFACE4 - all lower TTC_INTERFACEn must be defined!
  #endif
  #ifndef TTC_INTERFACE3
    #error TTC_INTERFACE5 is defined, but not TTC_INTERFACE3 - all lower TTC_INTERFACEn must be defined!
  #endif
  #ifndef TTC_INTERFACE2
    #error TTC_INTERFACE5 is defined, but not TTC_INTERFACE2 - all lower TTC_INTERFACEn must be defined!
  #endif
  #ifndef TTC_INTERFACE1
    #error TTC_INTERFACE5 is defined, but not TTC_INTERFACE1 - all lower TTC_INTERFACEn must be defined!
  #endif

  #define TTC_INTERFACE_AMOUNT 5
#else
  #ifdef TTC_INTERFACE4
    #define TTC_INTERFACE_AMOUNT 4

    #ifndef TTC_INTERFACE3
      #error TTC_INTERFACE5 is defined, but not TTC_INTERFACE3 - all lower TTC_INTERFACEn must be defined!
    #endif
    #ifndef TTC_INTERFACE2
      #error TTC_INTERFACE5 is defined, but not TTC_INTERFACE2 - all lower TTC_INTERFACEn must be defined!
    #endif
    #ifndef TTC_INTERFACE1
      #error TTC_INTERFACE5 is defined, but not TTC_INTERFACE1 - all lower TTC_INTERFACEn must be defined!
    #endif
  #else
    #ifdef TTC_INTERFACE3

      #ifndef TTC_INTERFACE2
        #error TTC_INTERFACE5 is defined, but not TTC_INTERFACE2 - all lower TTC_INTERFACEn must be defined!
      #endif
      #ifndef TTC_INTERFACE1
        #error TTC_INTERFACE5 is defined, but not TTC_INTERFACE1 - all lower TTC_INTERFACEn must be defined!
      #endif

      #define TTC_INTERFACE_AMOUNT 3
    #else
      #ifdef TTC_INTERFACE2

        #ifndef TTC_INTERFACE1
          #error TTC_INTERFACE5 is defined, but not TTC_INTERFACE1 - all lower TTC_INTERFACEn must be defined!
        #endif

        #define TTC_INTERFACE_AMOUNT 2
      #else
        #ifdef TTC_INTERFACE1
          #define TTC_INTERFACE_AMOUNT 1
        #else
          #define TTC_INTERFACE_AMOUNT 0
        #endif
      #endif
    #endif
  #endif
#endif
//}
/**{ Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop. 
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_INTERFACE 0  # disable assert handling for interface devices
 *
 */
#ifndef TTC_ASSERT_INTERFACE    // any previous definition set (Makefile)?
#define TTC_ASSERT_INTERFACE 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_INTERFACE == 1)  // use Assert()s in INTERFACE code (somewhat slower but alot easier to debug)
  #define Assert_INTERFACE(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in INTERFACE code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_INTERFACE(Condition, ErrorCode)
#endif
//}
/**{ Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_interface_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_interface_architecture
#warning Missing low-level definition for t_ttc_interface_architecture (using default)
#define t_ttc_interface_architecture void
#endif
//}
/**{ Check if definitions of logical indices are taken from e_ttc_interface_physical_index
 *
 * See definition of e_ttc_interface_physical_index below for details.
 *
 */

#define TTC_INDEX_INTERFACE_MAX 10  // must have same value as TTC_INDEX_INTERFACE_ERROR
#ifdef TTC_INTERFACE1
#  if TTC_INTERFACE1 >= TTC_INDEX_INTERFACE_MAX
#    error Wrong definition of TTC_INTERFACE1, check your makefile (must be one from e_ttc_interface_physical_index)!
#  endif
#  ifndef TTC_INTERFACE1_DRIVER
#    warning Missing definition, check your makefile: TTC_INTERFACE1_DRIVER (choose one from e_ttc_interface_architecture)
#    define TTC_INTERFACE1_DRIVER ta_interface_None
#  endif
#endif
#ifdef TTC_INTERFACE2
#  if TTC_INTERFACE2 >= TTC_INDEX_INTERFACE_MAX
#    error Wrong definition of TTC_INTERFACE2, check your makefile (must be one from e_ttc_interface_physical_index)!
#  endif
#  ifndef TTC_INTERFACE2_DRIVER
#    warning Missing definition, check your makefile: TTC_INTERFACE2_DRIVER (choose one from e_ttc_interface_architecture)
#    define TTC_INTERFACE2_DRIVER ta_interface_None
#  endif
#endif
#ifdef TTC_INTERFACE3
#  if TTC_INTERFACE3 >= TTC_INDEX_INTERFACE_MAX
#    error Wrong definition of TTC_INTERFACE3, check your makefile (must be one from e_ttc_interface_physical_index)!
#  endif
#  ifndef TTC_INTERFACE3_DRIVER
#    warning Missing definition, check your makefile: TTC_INTERFACE3_DRIVER (choose one from e_ttc_interface_architecture)
#    define TTC_INTERFACE3_DRIVER ta_interface_None
#  endif
#endif
#ifdef TTC_INTERFACE4
#  if TTC_INTERFACE4 >= TTC_INDEX_INTERFACE_MAX
#    error Wrong definition of TTC_INTERFACE4, check your makefile (must be one from e_ttc_interface_physical_index)!
#  endif
#  ifndef TTC_INTERFACE4_DRIVER
#    warning Missing definition, check your makefile: TTC_INTERFACE4_DRIVER (choose one from e_ttc_interface_architecture)
#    define TTC_INTERFACE4_DRIVER ta_interface_None
#  endif
#endif
#ifdef TTC_INTERFACE5
#  if TTC_INTERFACE5 >= TTC_INDEX_INTERFACE_MAX
#    error Wrong definition of TTC_INTERFACE5, check your makefile (must be one from e_ttc_interface_physical_index)!
#  endif
#  ifndef TTC_INTERFACE5_DRIVER
#    warning Missing definition, check your makefile: TTC_INTERFACE5_DRIVER (choose one from e_ttc_interface_architecture)
#    define TTC_INTERFACE5_DRIVER ta_interface_None
#  endif
#endif
#ifdef TTC_INTERFACE6
#  if TTC_INTERFACE6 >= TTC_INDEX_INTERFACE_MAX
#    error Wrong definition of TTC_INTERFACE6, check your makefile (must be one from e_ttc_interface_physical_index)!
#  endif
#  ifndef TTC_INTERFACE6_DRIVER
#    warning Missing definition, check your makefile: TTC_INTERFACE6_DRIVER (choose one from e_ttc_interface_architecture)
#    define TTC_INTERFACE6_DRIVER ta_interface_None
#  endif
#endif
#ifdef TTC_INTERFACE7
#  if TTC_INTERFACE7 >= TTC_INDEX_INTERFACE_MAX
#    error Wrong definition of TTC_INTERFACE7, check your makefile (must be one from e_ttc_interface_physical_index)!
#  endif
#  ifndef TTC_INTERFACE7_DRIVER
#    warning Missing definition, check your makefile: TTC_INTERFACE7_DRIVER (choose one from e_ttc_interface_architecture)
#    define TTC_INTERFACE7_DRIVER ta_interface_None
#  endif
#endif
#ifdef TTC_INTERFACE8
#  if TTC_INTERFACE8 >= TTC_INDEX_INTERFACE_MAX
#    error Wrong definition of TTC_INTERFACE8, check your makefile (must be one from e_ttc_interface_physical_index)!
#  endif
#  ifndef TTC_INTERFACE8_DRIVER
#    warning Missing definition, check your makefile: TTC_INTERFACE8_DRIVER (choose one from e_ttc_interface_architecture)
#    define TTC_INTERFACE8_DRIVER ta_interface_None
#  endif
#endif
#ifdef TTC_INTERFACE9
#  if TTC_INTERFACE9 >= TTC_INDEX_INTERFACE_MAX
#    error Wrong definition of TTC_INTERFACE9, check your makefile (must be one from e_ttc_interface_physical_index)!
#  endif
#  ifndef TTC_INTERFACE9_DRIVER
#    warning Missing definition, check your makefile: TTC_INTERFACE9_DRIVER (choose one from e_ttc_interface_architecture)
#    define TTC_INTERFACE9_DRIVER ta_interface_None
#  endif
#endif
#ifdef TTC_INTERFACE10
#  if TTC_INTERFACE10 >= TTC_INDEX_INTERFACE_MAX
#    error Wrong definition of TTC_INTERFACE10, check your makefile (must be one from e_ttc_interface_physical_index)!
#  endif
#  ifndef TTC_INTERFACE10_DRIVER
#    warning Missing definition, check your makefile: TTC_INTERFACE10_DRIVER (choose one from e_ttc_interface_architecture)
#    define TTC_INTERFACE10_DRIVER ta_interface_None
#  endif
#endif
//}

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef e_ttc_physical_index e_ttc_interface_physical_index;
typedef enum {   // e_ttc_interface_errorcode      return codes of INTERFACE devices
  ec_interface_OK = 0, 
  
  // other warnings go here..

  ec_interface_ERROR,                  // general failure
  ec_interface_NULL,                   // NULL pointer not accepted
  ec_interface_DeviceNotFound,         // corresponding device could not be found
  ec_interface_InvalidConfiguration,   // sanity check of device configuration failed
  ec_interface_InvalidImplementation,  // your code does not behave as expected

  // other failures go here..
  
  ec_interface_unknown                // no valid errorcodes past this entry 
} e_ttc_interface_errorcode;
typedef enum {   // e_ttc_interface_architecture  types of architectures supported by INTERFACE driver
  ta_interface_None,           // no architecture selected
  

  ta_interface_ste101p, // automatically added by ./create_DeviceDriver.pl
//InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)
  
  ta_interface_unknown        // architecture not supported
} e_ttc_interface_architecture;
typedef struct s_ttc_interface_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_interface_init()
    //       and after ttc_interface_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    e_ttc_interface_architecture Architecture; // type of architecture used for current interface device
    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_INTERFACE1, ...)
    t_u8  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)
    
    // low-level configuration (structure defined by low-level driver)
    t_ttc_interface_architecture* LowLevelConfig;      
    
    union  { // generic configuration bits common for all low-level Drivers
        t_u16 All;
        struct {
          unsigned Initialized        : 1;  // ==1: device has been initialized successfully
          // ToDo: additional high-level flags go here..
            
            unsigned Reserved1        : 15; // pad to 16 bits
        } Bits;
    } Flags;

    // Additional high-level attributes go here..
    
} __attribute__((__packed__)) t_ttc_interface_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_INTERFACE_TYPES_H
