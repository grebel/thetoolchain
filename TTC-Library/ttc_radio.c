/** { ttc_radio.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for radio devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Features of high-level driver ttc_radio:
 *  - handling of memory pool of equal sized blocks for incoming and outgoing packets
 *  - single linked list to buffer incoming and outgoing packets
 *  - synchronization between application (single- or multitasking) and
 *    transmit/ receive interrupt service routines
 *  - interrupt service routine appends received packets to receive list
 *  - transmit function for complete packets appends packets to transmit list
 *  - transmit function for automated transmission of raw data buffers
 *    (creates required amount of packets automatically)
 *  - receive function removes packets from receive list
 *  - reuse of memory blocks of processed packets
 *  - radio independent frame filter configuration
 *  - initialization of packet headers
 *
 *  For each radio, a low-level driver has to
 *  - define constant struct t_ttc_radio_features describing basic features of transceiver
 *  - read/ write registers inside radio transceiver
 *    (using SPI, I2C, UART, Memory Mapped IO or other transport mechanism)
 *  - initialize radio according to given t_ttc_radio_config
 *  - deinitialize radio (shut it down)
 *  - switch transmit/ receive channel
 *  - configure frame filter
 *  - configure interrupt sources for radio events (packet received, buffers empty, error, ...)
 *  - transmit raw packets
 *  - receive raw packets and pass them to ttc_radio_isr_receive()
 *  - set local 16-/64-bit Identification and 16-Bit Personal Area Network Identification
 *
 *  Optional low-level functions
 *  - change power level of transmitter/ receiver (if supported by transceiver)
 *  - calculate ranging data (distance between two nodes)
 *
 *  Created from template ttc_device.c revision 26 at 20140514 04:32:30 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_radio.h"
#include "radio/radio_common.h"
#include "ttc_packet.h"
#include "ttc_list.h"
#include "ttc_semaphore.h"
#include "ttc_heap.h"
#include "ttc_memory.h"
#include "ttc_interrupt.h"
#include "ttc_random.h"
#include "ttc_systick.h"
#include "ttc_mutex.h"

#if TTC_RADIO_AMOUNT == 0
    #warning No RADIO devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of radio devices.
 *
 */

// for each initialized device, a pointer to its generic and stm32w1xx definitions is stored
A_define( t_ttc_radio_config*, ttc_radio_configs, TTC_RADIO_AMOUNT );

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Private Function declarations ******************************************

/** Check if given user identifier has radio access and return configuration pointer
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID          ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @return ==NULL: ttc_radio is locked by someone else (try again later); !=NULL; radio access granted
 */
t_ttc_radio_config* _ttc_radio_obtain_configuration( t_u8 LogicalIndex, t_u8 UserID );

//}Private Function declarations
//{ Function definitions ***************************************************

e_ttc_radio_errorcode    ttc_radio_change_local_address( t_u8 LogicalIndex, t_u8 UserID, const t_ttc_packet_address* LocalAddress ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return ec_radio_IsLocked; } // radio locked by someone else!

    _driver_radio_change_local_address( Config, LocalAddress );
    if ( LocalAddress ) { // change Local address
        Assert_RADIO_Readable( LocalAddress, ttc_assert_origin_auto );  // pointer seems to direct to invalid memory address
        ttc_memory_copy( & Config->LocalID, LocalAddress, sizeof( Config->LocalID ) );

        if ( 1 ) { // initialize sequence number start values
            // using existing value as start point to get a new seed on every #
            // function call even with same LocalAddress
            t_u32 Seed = ttc_random_generate();

            // use as many information as possible to get a different seed for every node
            Seed += LocalAddress->Address16;
            Seed += LocalAddress->Address64.Words[0];
            Seed += LocalAddress->Address64.Words[1];
            Seed += LocalAddress->PanID;

            // initialize random number generator
            ttc_random_seed( Seed );

            Config->SequenceNo_Generic = 0xff & ttc_random_generate();
            Config->SequenceNo_Beacons = 0xff & ttc_random_generate();
        }
    }

    return ec_radio_OK;
}
e_ttc_radio_errorcode    ttc_radio_change_delay_time( t_u8 LogicalIndex, t_u8 UserID, t_u16 Delay_Microseconds ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return ec_radio_IsLocked; } // radio locked by someone else!

    _driver_radio_change_delay_time( Config, Delay_Microseconds );
    return ec_radio_OK;
}
void                     ttc_radio_configuration_check( t_u8 LogicalIndex ) {
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );

    // perform some generic plausibility checks
    const t_ttc_radio_features* Features = Config->Features;
    Assert_RADIO_Readable( Features, ttc_assert_origin_auto );  // low-level driver has provided illegal address (or even did not initialize this mandatory field)

    if ( Config->Init.MaxPacketSize > Features->MaxPacketSize )  { Config->Init.MaxPacketSize = Features->MaxPacketSize; }
    if ( Config->Init.RawBitrate    > Features->MaxBitRate )     { Config->Init.RawBitrate    = Features->MaxBitRate; }
    if ( Config->Init.RawBitrate    < Features->MinBitRate )     { Config->Init.RawBitrate    = Features->MinBitRate; }
    if ( Config->Init.LevelRX       > Features->MaxRxLevel )     { Config->Init.LevelRX       = Features->MaxRxLevel; }
    if ( Config->Init.LevelRX       < Features->MinRxLevel )     { Config->Init.LevelRX       = Features->MinRxLevel; }
    if ( Config->Init.LevelTX       > Features->MaxTxLevel )     { Config->Init.LevelTX       = Features->MaxTxLevel; }
    if ( Config->Init.LevelTX       < Features->MinTxLevel )     { Config->Init.LevelTX       = Features->MinTxLevel; }
    if ( Config->ChannelRX     > Features->MaxChannel )     { Config->ChannelRX     = Features->MaxChannel; }
    if ( Config->ChannelRX     < Features->MinChannel )     { Config->ChannelRX     = Features->MinChannel; }
    if ( Config->ChannelTX     > Features->MaxChannel )     { Config->ChannelTX     = Features->MaxChannel; }
    if ( Config->ChannelTX     < Features->MinChannel )     { Config->ChannelTX     = Features->MinChannel; }

    if ( ( !Config->RoundTripTime2Distance_Multiplier ) ||
            ( !Config->RoundTripTime2Distance_Divider )
       ) { // Not egnough data given by low-level driver to enable ranging!
        Config->Init.Flags.EnableRanging = 0;
    }
    if ( !Config->Init.Flags.EnableRanging ) { // no ranging available: disable automatic ranging replies too
        Config->Init.Flags.EnableRangingReplies = 0;
    }

    // let low-level driver check this configuration
    _driver_radio_configuration_check( Config );
}
t_u8                     ttc_radio_configure_auto_acknowledge( t_u8 LogicalIndex, t_u8 UserID, BOOL Enable, BOOL Change ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return -1; } // radio locked by someone else!

    // pass function call to interface or low-level driver function
    return _driver_radio_configure_auto_acknowledge( Config, Enable, Change );
}
e_ttc_radio_errorcode    ttc_radio_configure_channel( t_u8 LogicalIndex, t_u8 UserID, t_u8 NewChannel ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return ec_radio_IsLocked; } // radio locked by someone else!

    if ( NewChannel < Config->Features->MinChannel )
    { NewChannel = Config->Features->MinChannel; }
    if ( NewChannel > Config->Features->MaxChannel )
    { NewChannel = Config->Features->MaxChannel; }
    e_ttc_radio_errorcode Result = ec_radio_NoChange;

    if ( Config->ChannelTX != NewChannel ) { // switching to different channel:
        ttc_task_critical_begin();

#if (TTC_ASSERT_RADIO_EXTRA == 1)
        Config->TransmitTime_AntennaDelay = 0; // reset value to check if low-level driver is updating it
#endif

        Config->ChannelRX = Config->ChannelTX = NewChannel;
        Result = _driver_radio_configure_channel_rxtx( Config, NewChannel );

#if (TTC_ASSERT_RADIO_EXTRA == 1)
        Assert_RADIO_EXTRA( Config->TransmitTime_AntennaDelay != 0, ttc_assert_origin_auto ); // Accurate ranging requires that low-level driver updates this value on every channel switch!
#endif
        ttc_task_critical_end();
    }

    return Result;
}
e_ttc_radio_errorcode    ttc_radio_configure_channel_tx( t_u8 LogicalIndex, t_u8 UserID, t_u8 NewChannel ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return ec_radio_IsLocked; } // radio locked by someone else!

    e_ttc_radio_errorcode Result = ec_radio_NoChange;

    if ( Config->ChannelTX != NewChannel ) { // switching to different channel:
        ttc_task_critical_begin();
        if ( NewChannel < Config->Features->MinChannel )
        { NewChannel = Config->Features->MinChannel; }

        if ( NewChannel > Config->Features->MaxChannel )
        { NewChannel = Config->Features->MaxChannel; }

        Config->ChannelTX = NewChannel;
        Result = _driver_radio_configure_channel_tx( Config, NewChannel );
        ttc_task_critical_end();
    }

    return Result;
}
e_ttc_radio_errorcode    ttc_radio_configure_channel_rx( t_u8 LogicalIndex, t_u8 UserID, t_u8 NewChannel ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return ec_radio_IsLocked; } // radio locked by someone else!

    e_ttc_radio_errorcode Result = ec_radio_OK;

    ttc_task_critical_begin();
    if ( NewChannel < Config->Features->MinChannel )
    { NewChannel = Config->Features->MinChannel; }
    if ( NewChannel > Config->Features->MaxChannel )
    { NewChannel = Config->Features->MaxChannel; }
    if ( Config->ChannelRX != NewChannel ) { // switching to different channel:
        Config->ChannelRX = NewChannel;
        Result = _driver_radio_configure_channel_rx( Config, NewChannel );
    }
    ttc_task_critical_end();

    return Result;
}
u_ttc_radio_frame_filter ttc_radio_configure_frame_filter( t_u8 LogicalIndex, t_u8 UserID, e_ttc_radio_configure_frame_filter Filter ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return Config->FrameFilter; }

    u_ttc_radio_frame_filter FrameFilter = Config->FrameFilter;

    t_u8 ChangeFilter = 1;

    switch ( Filter ) {
        case ttc_radio_frame_filter_read:
            ChangeFilter = 0;
            break;
        case ttc_radio_frame_filter_disable:
            FrameFilter.All = 0;
            break;
        case ttc_radio_frame_filter_coordinator:
            FrameFilter.Allowed.Acknowledge          = 1;
            FrameFilter.Allowed.Beacon               = 1;
            FrameFilter.Allowed.Data                 = 1;
            FrameFilter.Allowed.MAC_Control          = 1;
            FrameFilter.Allowed.WithoutDestinationID = 1;
            break;
        case ttc_radio_frame_filter_sensor:
            FrameFilter.Allowed.Acknowledge          = 1;
            FrameFilter.Allowed.Beacon               = 1;
            FrameFilter.Allowed.Data                 = 1;
            FrameFilter.Allowed.MAC_Control          = 1;
            break;
        case ttc_radio_frame_filter_beacon:
            FrameFilter.Allowed.Beacon = 1;
            break;
        case ttc_radio_frame_filter_data:
            FrameFilter.Allowed.Data = 1;
            break;
        case ttc_radio_frame_filter_ack:
            FrameFilter.Allowed.Acknowledge = 1;
            break;
        case ttc_radio_frame_filter_mac:
            FrameFilter.Allowed.MAC_Control = 1;
            break;
        case ttc_radio_frame_filter_reserved:
            FrameFilter.Allowed.ReservedTypes = 1;
            break;
        default: ttc_assert_halt_origin( ec_basic_InvalidArgument ); break; // unknown/ invalid Filter type!
    };

    Config->FrameFilter = FrameFilter = _driver_radio_configure_frame_filter( Config, FrameFilter, ChangeFilter );

    return FrameFilter;
}
BOOL                     ttc_radio_debug_activity( t_ttc_radio_config* volatile Config, t_ttc_packet* volatile Packet ) {
    Assert_RADIO_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    BOOL volatile Result = FALSE;

    if ( Packet ) {
        Assert_RADIO_Writable( Packet, ttc_assert_origin_auto );

    }
    return Result; // BOOL
}
void                     ttc_radio_deinit( t_u8 LogicalIndex ) {
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );
    ttc_task_critical_begin();

    e_ttc_radio_errorcode Result = _driver_radio_deinit( Config ); ( void ) Result;
    Assert_RADIO( Result == ec_radio_OK, ttc_assert_origin_auto );  // something went wrong during low-level deinit: Check low-level deinit()!

    if ( Config->Flags.Initialized )
    { ttc_radio_reset( LogicalIndex ); }

    Config->Flags.Initialized = 0;
    ttc_task_critical_end();
}
t_u8                     ttc_radio_get_max_index() {
    return TTC_RADIO_AMOUNT;
}
t_ttc_radio_config*      ttc_radio_get_configuration( t_u8 LogicalIndex ) {
    t_ttc_radio_config* Config = A( ttc_radio_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        ttc_interrupt_all_disable();

        Config = A( ttc_radio_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_radio_config ) );
        Config->Flags.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_radio_load_defaults( LogicalIndex );

        // prepare transmit- /receive-lists
        ttc_list_init( &( Config->List_PacketsRx ), "RADIO_RX" );
        ttc_list_init( &( Config->List_PacketsTx ), "RADIO_TX" );

        ttc_interrupt_all_enable();
    }

    Assert_RADIO_Writable( Config, ttc_assert_origin_auto );  // Config must be located in RAM (corrupt stack/heap/no heap at all?)

    TTC_TASK_RETURN( Config ); // implicit stack check
}
e_ttc_radio_errorcode    ttc_radio_init( t_u8 LogicalIndex ) {
#if (TTC_ASSERT_RADIO_EXTRA == 1)
    t_u8 InitialCriticalLevel = ttc_interrupt_critical_level();
#endif
    ttc_task_critical_begin();
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );

    if ( Config->Flags.Initialized ) {
        Config->UserID_Lock = 0; // unlock ttc_radio
        ttc_radio_deinit( LogicalIndex );
    }

    ttc_radio_configuration_check( LogicalIndex );

    if ( !Config->Pool_Packets.BlockSize ) { // initialize + allocate memory pool for outgoing and incoming packets
        ttc_heap_pool_init( &( Config->Pool_Packets ),
                            ttc_packet_calculate_buffer_size( Config->Init.MaxPacketSize ), // must calculate buffer size to take extra Meta header into account
                            Config->Init.MaxAmountPackets                                   // amount of packet buffers to allocate
                          );
    }

    // update ranging delay time
    ttc_radio_set_ranging_delay( LogicalIndex, 0, Config->Delay_RangingReply_us );

    e_ttc_radio_errorcode Result = _driver_radio_init( Config );
    if ( Result == ec_radio_OK ) {
        Config->Flags.Initialized = 1;

        // switch to configured channel
        ttc_radio_configure_channel_tx( LogicalIndex, 0, Config->ChannelTX );
        ttc_radio_configure_channel_rx( LogicalIndex, 0, Config->ChannelRX );

        // disable frame filter to receive everything
        ttc_radio_configure_frame_filter( LogicalIndex, 0, ttc_radio_frame_filter_disable );
    }
    ttc_task_critical_end();
    Assert_RADIO_EXTRA( InitialCriticalLevel == ttc_interrupt_critical_level(), ttc_assert_origin_auto ); // amount of  ttc_task_critical_end() calls does not match. Check driver implementation!

    // calling debug function to aid debugging as described for ttc_radio_debug_activity()
    Config->Init.function_end_tx_isr = &ttc_radio_debug_activity; //DEBUG
    ttc_radio_debug_activity( Config, NULL );

    TTC_TASK_RETURN( Result ); // implicit stack check
}
e_ttc_radio_errorcode    ttc_radio_load_defaults( t_u8 LogicalIndex ) {
    ttc_task_critical_begin();
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );

    if ( Config->Flags.Initialized ) {
        ttc_radio_deinit( LogicalIndex );

        // save packet before from reset to avoid memory leak
        t_ttc_heap_pool Pool_Packets;
        ttc_memory_copy( &Pool_Packets, &( Config->Pool_Packets ), sizeof( Pool_Packets ) );
        ttc_memory_set( Config, 0, sizeof( t_ttc_radio_config ) );
        ttc_memory_copy( &( Config->Pool_Packets ), &Pool_Packets, sizeof( Pool_Packets ) );
    }
    else
    { ttc_memory_set( Config, 0, sizeof( t_ttc_radio_config ) ); }

    // load generic default values
    switch ( LogicalIndex ) { // load type of low-level driver for current architecture
#ifdef TTC_RADIO1_DRIVER //{
        case  1:
            Config->Architecture = TTC_RADIO1_DRIVER;

#  if TTC_RADIO1_ALTERNATE_TX_PATH == 1
            Config->Init.Flags.UseAlternateTxPath = 1;
#  else
            Config->Init.Flags.UseAlternateTxPath = 0;
#  endif

#  ifdef TTC_RADIO1_PIN_ENABLE_TX
            Config->Init.Pin_TxActive = TTC_RADIO1_PIN_ENABLE_TX;
#  else
            Config->Init.Pin_TxActive = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO1_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif
            Config->Init.Flags.PinTxActive_LowLevel = 0;

#  ifdef TTC_RADIO1_Pin_Enable_Amplifier
            Config->Init.Pin_Enable_Amplifier = TTC_RADIO1_PIN_AMP_ACTIVE; // one from e_ttc_gpio_pin
#  else
            Config->Init.Pin_Enable_Amplifier = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO1_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif

            break;
#endif //}
#ifdef TTC_RADIO2_DRIVER //{
        case  2:
            Config->Architecture = TTC_RADIO2_DRIVER;
#  if TTC_RADIO2_ALTERNATE_TX_PATH == 1
            Config->Init.Flags.UseAlternateTxPath = 1;
#  else
            Config->Init.Flags.UseAlternateTxPath = 0;
#  endif
#  ifdef TTC_RADIO2_PIN_ENABLE_TX
            Config->Init.Pin_TxActive = TTC_RADIO2_PIN_ENABLE_TX;
#  else
            Config->Init.Pin_TxActive = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO2_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif
            Config->Init.Flags.PinTxActive_LowLevel = 0;
            break;

#  ifdef TTC_RADIO2_Pin_Enable_Amplifier
            Config->Init.Pin_Enable_Amplifier = TTC_RADIO2_PIN_AMP_ACTIVE; // one from e_ttc_gpio_pin
#  else
            Config->Init.Pin_Enable_Amplifier = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO2_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif

#endif //}
#ifdef TTC_RADIO3_DRIVER //{
        case  3:
            Config->Architecture = TTC_RADIO3_DRIVER;
#  if TTC_RADIO3_ALTERNATE_TX_PATH == 1
            Config->Init.Flags.UseAlternateTxPath = 1;
#  else
            Config->Init.Flags.UseAlternateTxPath = 0;
#  endif
#  ifdef TTC_RADIO3_PIN_ENABLE_TX
            Config->Init.Pin_TxActive = TTC_RADIO3_PIN_ENABLE_TX;
#  else
            Config->Init.Pin_TxActive = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO3_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif
            Config->Init.Flags.PinTxActive_LowLevel = 0;
            break;

#  ifdef TTC_RADIO3_Pin_Enable_Amplifier
            Config->Init.Pin_Enable_Amplifier = TTC_RADIO3_PIN_AMP_ACTIVE; // one from e_ttc_gpio_pin
#  else
            Config->Init.Pin_Enable_Amplifier = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO3_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif

#endif //}
#ifdef TTC_RADIO4_DRIVER //{
        case  4:
            Config->Architecture = TTC_RADIO4_DRIVER;
#  if TTC_RADIO4_ALTERNATE_TX_PATH == 1
            Config->Init.Flags.UseAlternateTxPath = 1;
#  else
            Config->Init.Flags.UseAlternateTxPath = 0;
#  endif
#  ifdef TTC_RADIO4_PIN_ENABLE_TX
            Config->Init.Pin_TxActive = TTC_RADIO4_PIN_ENABLE_TX;
#  else
            Config->Init.Pin_TxActive = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO4_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif
            Config->Init.Flags.PinTxActive_LowLevel = 0;
            break;

#  ifdef TTC_RADIO4_Pin_Enable_Amplifier
            Config->Init.Pin_Enable_Amplifier = TTC_RADIO4_PIN_AMP_ACTIVE; // one from e_ttc_gpio_pin
#  else
            Config->Init.Pin_Enable_Amplifier = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO4_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif

#endif //}
#ifdef TTC_RADIO5_DRIVER //{
        case  5:
            Config->Architecture = TTC_RADIO5_DRIVER;
#  if TTC_RADIO5_ALTERNATE_TX_PATH == 1
            Config->Init.Flags.UseAlternateTxPath = 1;
#  else
            Config->Init.Flags.UseAlternateTxPath = 0;
#  endif
#  ifdef TTC_RADIO5_PIN_ENABLE_TX
            Config->Init.Pin_TxActive = TTC_RADIO5_PIN_ENABLE_TX;
#  else
            Config->Init.Pin_TxActive = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO5_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif
            Config->Init.Flags.PinTxActive_LowLevel = 0;
            break;

#  ifdef TTC_RADIO5_Pin_Enable_Amplifier
            Config->Init.Pin_Enable_Amplifier = TTC_RADIO5_PIN_AMP_ACTIVE; // one from e_ttc_gpio_pin
#  else
            Config->Init.Pin_Enable_Amplifier = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO5_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif

#endif //}
#ifdef TTC_RADIO6_DRIVER //{
        case  6:
            Config->Architecture = TTC_RADIO6_DRIVER;
#  if TTC_RADIO6_ALTERNATE_TX_PATH == 1
            Config->Init.Flags.UseAlternateTxPath = 1;
#  else
            Config->Init.Flags.UseAlternateTxPath = 0;
#  endif
#  ifdef TTC_RADIO6_PIN_ENABLE_TX
            Config->Init.Pin_TxActive = TTC_RADIO6_PIN_ENABLE_TX;
#  else
            Config->Init.Pin_TxActive = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO6_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif
            Config->Init.Flags.PinTxActive_LowLevel = 0;
            break;

#  ifdef TTC_RADIO6_Pin_Enable_Amplifier
            Config->Init.Pin_Enable_Amplifier = TTC_RADIO6_PIN_AMP_ACTIVE; // one from e_ttc_gpio_pin
#  else
            Config->Init.Pin_Enable_Amplifier = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO6_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif

#endif //}
#ifdef TTC_RADIO7_DRIVER //{
        case  7:
            Config->Architecture = TTC_RADIO7_DRIVER;
#  if TTC_RADIO7_ALTERNATE_TX_PATH == 1
            Config->Init.Flags.UseAlternateTxPath = 1;
#  else
            Config->Init.Flags.UseAlternateTxPath = 0;
#  endif
#  ifdef TTC_RADIO7_PIN_ENABLE_TX
            Config->Init.Pin_TxActive = TTC_RADIO7_PIN_ENABLE_TX;
#  else
            Config->Init.Pin_TxActive = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO7_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif
            Config->Init.Flags.PinTxActive_LowLevel = 0;
            break;

#  ifdef TTC_RADIO7_Pin_Enable_Amplifier
            Config->Init.Pin_Enable_Amplifier = TTC_RADIO7_PIN_AMP_ACTIVE; // one from e_ttc_gpio_pin
#  else
            Config->Init.Pin_Enable_Amplifier = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO7_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif

#endif //}
#ifdef TTC_RADIO8_DRIVER //{
        case  8:
            Config->Architecture = TTC_RADIO8_DRIVER;
#  if TTC_RADIO8_ALTERNATE_TX_PATH == 1
            Config->Init.Flags.UseAlternateTxPath = 1;
#  else
            Config->Init.Flags.UseAlternateTxPath = 0;
#  endif
#  ifdef TTC_RADIO8_PIN_ENABLE_TX
            Config->Init.Pin_TxActive = TTC_RADIO8_PIN_ENABLE_TX;
#  else
            Config->Init.Pin_TxActive = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO8_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif
            Config->Init.Flags.PinTxActive_LowLevel = 0;
            break;

#  ifdef TTC_RADIO8_Pin_Enable_Amplifier
            Config->Init.Pin_Enable_Amplifier = TTC_RADIO8_PIN_AMP_ACTIVE; // one from e_ttc_gpio_pin
#  else
            Config->Init.Pin_Enable_Amplifier = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO8_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif

#endif //}
#ifdef TTC_RADIO9_DRIVER //{
        case  9:
            Config->Architecture = TTC_RADIO9_DRIVER;
#  if TTC_RADIO9_ALTERNATE_TX_PATH == 1
            Config->Init.Flags.UseAlternateTxPath = 1;
#  else
            Config->Init.Flags.UseAlternateTxPath = 0;
#  endif
#  ifdef TTC_RADIO9_PIN_ENABLE_TX
            Config->Init.Pin_TxActive = TTC_RADIO9_PIN_ENABLE_TX;
#  else
            Config->Init.Pin_TxActive = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO9_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif
            Config->Init.Flags.PinTxActive_LowLevel = 0;
            break;

#  ifdef TTC_RADIO9_Pin_Enable_Amplifier
            Config->Init.Pin_Enable_Amplifier = TTC_RADIO9_PIN_AMP_ACTIVE; // one from e_ttc_gpio_pin
#  else
            Config->Init.Pin_Enable_Amplifier = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO9_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif

#endif //}
#ifdef TTC_RADIO10_DRIVER //{
        case 10:
            Config->Architecture = TTC_RADIO10_DRIVER;
#  if TTC_RADIO10_ALTERNATE_TX_PATH == 1
            Config->Init.Flags.UseAlternateTxPath = 1;
#  else
            Config->Init.Flags.UseAlternateTxPath = 0;
#  endif
#  ifdef TTC_RADIO10_PIN_ENABLE_TX
            Config->Init.Pin_TxActive = TTC_RADIO10_PIN_ENABLE_TX;
#  else
            Config->Init.Pin_TxActive = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO10_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif
            Config->Init.Flags.PinTxActive_LowLevel = 0;
            break;

#  ifdef TTC_RADIO10_Pin_Enable_Amplifier
            Config->Init.Pin_Enable_Amplifier = TTC_RADIO10_PIN_AMP_ACTIVE; // one from e_ttc_gpio_pin
#  else
            Config->Init.Pin_Enable_Amplifier = E_ttc_gpio_architecture_none;
#  endif
#  if TTC_RADIO10_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#  else
            Config->Init.Flags.PinTxActive_LowActive = 0;
#  endif

#endif //}
        default: ttc_assert_halt_origin( ec_radio_InvalidImplementation ); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }
    Assert_RADIO( ( Config->Architecture > ta_radio_None ) && ( Config->Architecture < ta_radio_unknown ), ttc_assert_origin_auto );  // architecture not set properly! Check makefile for TTC_RADIO<n>_DRIVER and compare with e_ttc_radio_architecture

    Config->LogicalIndex               = LogicalIndex;
    Config->PhysicalIndex              = ttc_radio_logical_2_physical_index( LogicalIndex );

    switch ( LogicalIndex ) { // load default settings from static configuration
        case 1: {
#ifdef TTC_RADIO1_PIN_AMP_ACTIVE
            Config->Init.Pin_Enable_Amplifier       = TTC_RADIO1_PIN_AMP_ACTIVE;
#endif
#ifdef TTC_RADIO1_PIN_TX_ACTIVE
            Config->Init.Pin_TxActive               = TTC_RADIO1_PIN_TX_ACTIVE;
#endif
#if TTC_RADIO1_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#endif
#if TTC_RADIO1_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinAmp_LowActive = 1;
#endif
            break;
        }
        case 2: {
#ifdef TTC_RADIO2_PIN_AMP_ACTIVE
            Config->Init.Pin_Enable_Amplifier       = TTC_RADIO2_PIN_AMP_ACTIVE;
#endif
#ifdef TTC_RADIO2_PIN_TX_ACTIVE
            Config->Init.Pin_TxActive               = TTC_RADIO2_PIN_TX_ACTIVE;
#endif
#if TTC_RADIO2_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#endif
#if TTC_RADIO2_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinAmp_LowActive = 1;
#endif
            break;
        }
        case 3: {
#ifdef TTC_RADIO3_PIN_AMP_ACTIVE
            Config->Init.Pin_Enable_Amplifier       = TTC_RADIO3_PIN_AMP_ACTIVE;
#endif
#ifdef TTC_RADIO3_PIN_TX_ACTIVE
            Config->Init.Pin_TxActive               = TTC_RADIO3_PIN_TX_ACTIVE;
#endif
#if TTC_RADIO3_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#endif
#if TTC_RADIO3_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinAmp_LowActive = 1;
#endif
            break;
        }
        case 4: {
#ifdef TTC_RADIO4_PIN_AMP_ACTIVE
            Config->Init.Pin_Enable_Amplifier       = TTC_RADIO4_PIN_AMP_ACTIVE;
#endif
#ifdef TTC_RADIO4_PIN_TX_ACTIVE
            Config->Init.Pin_TxActive               = TTC_RADIO4_PIN_TX_ACTIVE;
#endif
#if TTC_RADIO4_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#endif
#if TTC_RADIO4_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinAmp_LowActive = 1;
#endif
            break;
        }
        case 5: {
#ifdef TTC_RADIO5_PIN_AMP_ACTIVE
            Config->Init.Pin_Enable_Amplifier       = TTC_RADIO5_PIN_AMP_ACTIVE;
#endif
#ifdef TTC_RADIO5_PIN_TX_ACTIVE
            Config->Init.Pin_TxActive               = TTC_RADIO5_PIN_TX_ACTIVE;
#endif
#if TTC_RADIO5_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#endif
#if TTC_RADIO5_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinAmp_LowActive = 1;
#endif
            break;
        }
        case 6: {
#ifdef TTC_RADIO6_PIN_AMP_ACTIVE
            Config->Init.Pin_Enable_Amplifier       = TTC_RADIO6_PIN_AMP_ACTIVE;
#endif
#ifdef TTC_RADIO6_PIN_TX_ACTIVE
            Config->Init.Pin_TxActive               = TTC_RADIO6_PIN_TX_ACTIVE;
#endif
#if TTC_RADIO6_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#endif
#if TTC_RADIO6_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinAmp_LowActive = 1;
#endif
            break;
        }
        case 7: {
#ifdef TTC_RADIO7_PIN_AMP_ACTIVE
            Config->Init.Pin_Enable_Amplifier       = TTC_RADIO7_PIN_AMP_ACTIVE;
#endif
#ifdef TTC_RADIO7_PIN_TX_ACTIVE
            Config->Init.Pin_TxActive               = TTC_RADIO7_PIN_TX_ACTIVE;
#endif
#if TTC_RADIO7_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#endif
#if TTC_RADIO7_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinAmp_LowActive = 1;
#endif
            break;
        }
        case 8: {
#ifdef TTC_RADIO8_PIN_AMP_ACTIVE
            Config->Init.Pin_Enable_Amplifier       = TTC_RADIO8_PIN_AMP_ACTIVE;
#endif
#ifdef TTC_RADIO8_PIN_TX_ACTIVE
            Config->Init.Pin_TxActive               = TTC_RADIO8_PIN_TX_ACTIVE;
#endif
#if TTC_RADIO8_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#endif
#if TTC_RADIO8_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinAmp_LowActive = 1;
#endif
            break;
        }
        case 9: {
#ifdef TTC_RADIO9_PIN_AMP_ACTIVE
            Config->Init.Pin_Enable_Amplifier       = TTC_RADIO9_PIN_AMP_ACTIVE;
#endif
#ifdef TTC_RADIO9_PIN_TX_ACTIVE
            Config->Init.Pin_TxActive               = TTC_RADIO9_PIN_TX_ACTIVE;
#endif
#if TTC_RADIO9_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#endif
#if TTC_RADIO9_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinAmp_LowActive = 1;
#endif
            break;
        }
        case 10: {
#ifdef TTC_RADIO10_PIN_AMP_ACTIVE
            Config->Init.Pin_Enable_Amplifier       = TTC_RADIO10_PIN_AMP_ACTIVE;
#endif
#ifdef TTC_RADIO10_PIN_TX_ACTIVE
            Config->Init.Pin_TxActive               = TTC_RADIO10_PIN_TX_ACTIVE;
#endif
#if TTC_RADIO10_PIN_TX_LOWACTIVE == 1
            Config->Init.Flags.PinTxActive_LowActive = 1;
#endif
#if TTC_RADIO10_PIN_AMP_LOWACTIVE == 1
            Config->Init.Flags.PinAmp_LowActive = 1;
#endif
            break;
        }
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // no static configuration available for given logical index. Check your makefile!
    }

    // set some generic defaults (may be overwritten by _driver_radio_load_defaults()
    Config->Init.Flags.AppendCRC             = 1;
    Config->Init.Flags.AutoAcknowledge       = 0;
    Config->Init.Flags.RxAutoReenable        = 1;
    Config->Init.Flags.AddressFilter         = 0;
    Config->Init.Flags.EnableDoubleBuffering = 1;
    Config->Init.Flags.EnableRSSI            = 1;
    Config->Init.MaxPacketSize               = Config->Features->MaxPacketSize;
    Config->Init.MaxAmountPackets            = TTC_RADIO_INITIAL_AMOUNT_PACKETS;
    Config->Init.Transceiver_Timeout_usecs   = 100000; // 100ms
    Config->Init.Transmit_Amount_Retries     = 3;
    Config->Init.Acknowledge_Timeout_usecs   = 1000000;

    //Insert additional generic default values here ...

    // let low-level driver check and correct generic defaults
    e_ttc_radio_errorcode Result = _driver_radio_load_defaults( Config );

    Assert_RADIO_Readable( Config->Features, ttc_assert_origin_auto );  // low-level driver must set this pointer to a struct defining radio specific features
    if ( !Config->ChannelRX )
    { Config->ChannelRX  = Config->Features->DefaultRxChannel; }
    if ( !Config->ChannelTX )
    { Config->ChannelTX  = Config->Features->DefaultTxChannel; }
    if ( !Config->Init.RawBitrate )
    { Config->Init.RawBitrate = Config->Features->DefaultBitRate; }
    if ( !Config->Init.LevelRX )
    { Config->Init.LevelRX    = Config->Features->DefaultRxLevel; }
    if ( !Config->Init.LevelTX )
    { Config->Init.LevelTX    = Config->Features->DefaultTxLevel; }

    // perform a final check of default configuration
    ttc_radio_configuration_check( LogicalIndex );
    ttc_task_critical_end();

    return Result;
}
t_u8                     ttc_radio_logical_2_physical_index( t_u8 LogicalIndex ) {
    Assert_RADIO( LogicalIndex > 0, ttc_assert_origin_auto );  // logical index starts at 1 !

    return LogicalIndex - 1; // DW1000 radio has no physical index
}
void                     ttc_radio_maintenance( t_u8 LogicalIndex, t_u8 UserID ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return; } // radio locked by someone else!

    if ( ( Config->UserID_Lock == 0 ) ||  // ttc_radio not locked
            ( Config->UserID_Lock == UserID ) // ttc_radio locked by caller
       )
    { _driver_radio_maintenance( Config ); }
}
void                     ttc_radio_prepare() {
    // add your startup code here (Singletasking!)

    A_reset( ttc_radio_configs, TTC_RADIO_AMOUNT ); // must reset data of safe array manually!

    // maybe low-level driver wants to initialize too
    _driver_radio_prepare();
}
e_ttc_packet_pattern     ttc_radio_socket_new( t_u8 LogicalIndex, e_ttc_packet_pattern SocketID ) {
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );
    ( void ) SocketID;


    e_ttc_packet_pattern Protocol = 0;

    switch ( SocketID ) {
        case E_ttc_packet_pattern_socket_AnyOf_Application: { // generic application protocol: E_ttc_packet_pattern_socket_Begin_Application+1 ..ttc_packet_pattern_protocol_End_Application-1
            if ( Config->MaxProtocol_Application == 0 ) {
                Config->MaxProtocol_Application = E_ttc_packet_pattern_socket_Application1;
            }
            else {
                Assert_RADIO( Config->MaxProtocol_Application < E_ttc_packet_pattern_socket_End_Application, ttc_assert_origin_auto ); // got called too many times. Check code of caller or add more E_ttc_packet_pattern_socket_Application<N> entries!
                Config->MaxProtocol_Application++;
            }
            Protocol = Config->MaxProtocol_Application;
            break;
        }
        default:
            ttc_assert_halt_origin( ttc_assert_origin_auto ); // given protocol type not supported: Check code of caller!
            break;
    }

    return Protocol;
}
t_u8                     ttc_radio_physical_2_logical_index( t_u8 PhysicalIndex ) {

    switch ( PhysicalIndex ) {         // determine base register and other low-level configuration data
#ifdef TTC_RADIO1
        case TTC_RADIO1: return 1;
#endif
#ifdef TTC_RADIO2
        case TTC_RADIO2: return 2;
#endif
#ifdef TTC_RADIO3
        case TTC_RADIO3: return 3;
#endif
#ifdef TTC_RADIO4
        case TTC_RADIO4: return 4;
#endif
#ifdef TTC_RADIO5
        case TTC_RADIO5: return 5;
#endif
#ifdef TTC_RADIO6
        case TTC_RADIO6: return 6;
#endif
#ifdef TTC_RADIO7
        case TTC_RADIO7: return 7;
#endif
#ifdef TTC_RADIO8
        case TTC_RADIO8: return 8;
#endif
#ifdef TTC_RADIO9
        case TTC_RADIO9: return 9;
#endif
#ifdef TTC_RADIO10
        case TTC_RADIO10: return 10;
#endif
        // extend as required

        default: break;
    }

    Assert_RADIO( 0, ttc_assert_origin_auto );  // No TTC_RADIOn defined! Check your makefile.100_board_* file!
    return -1; // program flow should never go here
}
t_ttc_packet*            ttc_radio_packet_get_empty( t_u8 LogicalIndex, e_ttc_packet_type Type, e_ttc_packet_pattern Protocol ) {
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );

    return radio_common_packet_get_empty( Config, Type, Protocol );
}
t_ttc_packet*            ttc_radio_packet_get_empty_try( t_u8 LogicalIndex, e_ttc_packet_type Type, e_ttc_packet_pattern Protocol ) {
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );

    return radio_common_packet_get_empty_try( Config, Type, Protocol );
}
t_ttc_packet*            ttc_radio_packet_get_empty_isr( t_u8 LogicalIndex, e_ttc_packet_type Type, e_ttc_packet_pattern Protocol ) {
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );

    return radio_common_packet_get_empty_isr( Config, Type, Protocol );
}
const t_ttc_packet*      ttc_radio_packet_received_peek( t_u8 LogicalIndex, t_u8 UserID ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return NULL; } // radio locked by someone else!

    return radio_common_packet_received_peek( Config );
}
t_ttc_packet*            ttc_radio_packet_received_tryget( t_u8 LogicalIndex, t_u8 UserID, t_ttc_radio_job_socket* SocketJob ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return NULL; } // radio locked by someone else!

    t_ttc_packet* PacketRX = radio_common_packet_received_tryget( Config, SocketJob );

    if ( PacketRX ) {
#if TTC_HEAP_POOL_STATISTICS == 1
        // set owner info to aid debugging
        ( ( ( t_ttc_heap_block_from_pool* ) PacketRX ) - 1 )->MyOwner = ( void ( * )() ) __builtin_return_address( 0 );
#endif
    }

    return PacketRX;
}
t_ttc_packet*            ttc_radio_packet_received_waitfor( t_u8 LogicalIndex, t_u8 UserID, e_ttc_packet_pattern Protocol ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return NULL; } // radio locked by someone else!

    t_ttc_packet* PacketRX = radio_common_packet_received_waitfor( Config, Protocol );

    if ( PacketRX ) {
#if TTC_HEAP_POOL_STATISTICS == 1
        // set owner info to aid debugging
        ( ( ( t_ttc_heap_block_from_pool* ) PacketRX ) - 1 )->MyOwner = ( void ( * )() ) __builtin_return_address( 0 );
#endif
    }

    return PacketRX;
}
t_u16                    ttc_radio_packets_received_ignore( t_u8 LogicalIndex, t_u8 UserID, e_ttc_packet_pattern Protocol ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return 0; } // radio locked by someone else!

    return radio_common_packets_received_ignore( Config, Protocol );
}
t_u8                     ttc_radio_ranging_request( t_u8 LogicalIndex, t_u8 UserID, e_radio_common_ranging_type Type, t_u8 AmountRepliesRequired, const t_ttc_packet_address* RemoteID, t_u16 TimeOutMS, u_ttc_packetimestamp_40* ReferenceTime, t_ttc_radio_distance* Distances, t_ttc_math_vector3d_xyz* Locations, BOOL ReenableReceiver ) {

#if TTC_PACKET_RANGING==1
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return 0; } // radio locked by someone else!

    if ( ! Config->Init.Flags.EnableRanging )
    { return 0; }

    if ( UserID && !ttc_radio_user_lock( LogicalIndex, UserID ) )
    { return 0; } // radio locked by someone else!

    Assert_RADIO_Writable( Distances, ttc_assert_origin_auto );  // always check incoming pointer arguments
    Assert_RADIO_Readable( RemoteID,  ttc_assert_origin_auto );  // always check incoming pointer arguments
    Assert_RADIO( ( !ReferenceTime ) || ttc_memory_is_readable( ReferenceTime ), ttc_assert_origin_auto );  // must be NULL or readable address!

    if ( Locations ) {
        Assert_RADIO_Writable( Locations, ttc_assert_origin_auto ); // if given, Locations must point to writable memory. Check implementation of calling function!
    }

    t_u8 AmountReplies = radio_common_ranging_measure( Config,
                                                       Type,
                                                       AmountRepliesRequired,
                                                       RemoteID,
                                                       TimeOutMS,
                                                       ReferenceTime,
                                                       Distances,
                                                       Locations,
                                                       ReenableReceiver
                                                     );
    ttc_radio_user_unlock( LogicalIndex, UserID );
    return AmountReplies;

#else

    // avoid compiler warnings "Unused variable..."
    ( void ) LogicalIndex;
    ( void ) Type;
    ( void ) AmountRepliesRequired;
    ( void ) RemoteID;
    ( void ) TimeOutMS;
    ( void ) ReferenceTime;
    ( void ) Distances;
    ( void ) Locations;

    if ( ReenableReceiver ) {
        ttc_radio_receiver( LogicalIndex, TRUE, NULL, 0 );
    }
    return 0;
#endif
}
e_ttc_radio_errorcode    ttc_radio_receiver( t_u8 LogicalIndex, t_u8 UserID, BOOL Enable, u_ttc_packetimestamp_40* ReferenceTime, t_base TimeOut_us ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return ec_radio_IsLocked; } // radio locked by someone else!

    if ( s_ttc_listize( &( Config->List_PacketsTx ) ) ) // still packet to be transmitted: wait until complete
    { ttc_radio_transmit_waitfor( LogicalIndex, UserID ); }

    if ( _driver_radio_receiver( Config, Enable, ReferenceTime, TimeOut_us ) )
    { return ec_radio_OK; }
    return ec_radio_unknown;
}
void                     ttc_radio_reset( t_u8 LogicalIndex ) {
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );

    ttc_task_critical_begin();
    if ( Config->Flags.Initialized ) { // deinit radio
        Config->UserID_Lock = 0; // unlock ttc_radio

        t_ttc_packet* Packet;
        do { // return all remaining memory blocks in List_PacketsTx to their memory pool
            Packet = radio_common_pop_list_tx( Config );
            if ( Packet )
            { ttc_radio_packet_release( Packet ); }
        }
        while ( Packet );

        ttc_radio_packets_received_ignore( LogicalIndex, 0, E_ttc_packet_pattern_socket_None );

        // let low-level driver perform reset
        _driver_radio_reset( Config );

        ttc_mutex_unlock( &( Config->FlagRunningTX ) );

#if TTC_HEAP_POOL_STATISTICS == 1
        // reset statistics
        ttc_memory_set( &Config->Statistics, 0, sizeof( Config->Statistics ) );
#endif

        Config->Flags.Initialized = 0;
    }
    ttc_radio_load_defaults( LogicalIndex );
    ttc_task_critical_end();
}
e_ttc_radio_errorcode    ttc_radio_set_ranging_delay( t_u8 LogicalIndex, t_u8 UserID, t_u16 Delay_us ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return ec_radio_IsLocked; } // radio locked by someone else!

    radio_common_set_ranging_delay( Config, Delay_us );
    return ec_radio_OK;
}
e_ttc_radio_errorcode    ttc_radio_transmit_buffer( t_u8 VOLATILE_RADIO LogicalIndex, t_u8 VOLATILE_RADIO UserID, e_ttc_packet_pattern VOLATILE_RADIO Protocol, VOLATILE_RADIO e_ttc_packet_type PacketType, const t_u8* VOLATILE_RADIO Data, t_u16 VOLATILE_RADIO AmountBytes, const t_ttc_packet_address* VOLATILE_RADIO DestinationID, t_u32 VOLATILE_RADIO DelayNS, BOOL VOLATILE_RADIO Acknowledged ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return ec_radio_IsLocked; } // radio locked by someone else!

    if ( UserID && !ttc_radio_user_lock( LogicalIndex, UserID ) )
    { return ec_radio_IsLocked; } // radio locked by someone else!

    VOLATILE_RADIO e_ttc_radio_errorcode ReturnCode = ec_radio_OK;
    t_u16 PayloadSize = 0;
    t_u8* Payload    = NULL;
    e_ttc_packet_errorcode ErrorPacket; ( void ) ErrorPacket;

    // allocate + initialize packet buffer (same buffer is reused if more than one packet must be transmitted)
    t_ttc_packet* VOLATILE_RADIO PacketTX = ttc_radio_packet_get_empty( LogicalIndex, PacketType, Protocol );
    if ( !PacketTX ) { // cannot obtain packet: return error
        ReturnCode = ec_radio_unknown;
        goto trtb_Abort;
    }
    if ( 1 ) { // address packet
        E_ttc_packet_address_source_set( PacketTX, & Config->LocalID );
        ttc_packet_address_target_set( PacketTX, DestinationID );
        ttc_packet_acknowledge_request_set( PacketTX, Acknowledged );
        PacketTX->Meta.DoNotReleaseBuffer = 1;

        ErrorPacket = ttc_packet_payload_get_pointer( PacketTX, &Payload, &PayloadSize, NULL );
        Assert_RADIO( !ErrorPacket, ttc_assert_origin_auto );  // cannot find packet payload (unsupported packet type?)
        Assert_RADIO( PayloadSize > 0, ttc_assert_origin_auto );  // this packet cannot transport data (wrong packet-type?)
    }

    while ( AmountBytes && ( PacketTX->Meta.StatusTX < tpst_Error ) ) { // we might need more than one packet to send out all data

        if ( 1 ) {           // fill packet payload
            if ( PayloadSize > AmountBytes )
            { PayloadSize = AmountBytes; }
            ttc_memory_copy( Payload, Data, PayloadSize );
        }

        t_u8 RetriesRemaining = Config->Init.Transmit_Amount_Retries;
        do {                 // try to transmit single packet (using multiple retries if Acknowledged==TRUE)

            PacketTX->Meta.StatusTX = 0;

            if ( DelayNS ) {    // transmit packet exactly at Packet->Meta.ReceiveTime + DelayNS
                ReturnCode = ttc_radio_transmit_packet_delayed( LogicalIndex, UserID, PacketTX, PayloadSize, FALSE, DelayNS );
                DelayNS = 0; // only first packet is delayed (rest of packets is sent consequently)
            }
            else {              // transmit packet as soon as possible
                ReturnCode = ttc_radio_transmit_packet_now( LogicalIndex, UserID, PacketTX, PayloadSize, FALSE );
            }

            if ( ReturnCode ) { // something went wrong during transmission: retry or abort
                Assert_RADIO( ReturnCode != ec_radio_IsLocked, ttc_assert_origin_auto ); // We have locked radio before so this should not happen. Check implementation of ttc_radio_user_lock()!

                ttc_systick_delay_simple( Config->Init.Acknowledge_Timeout_usecs );
                RetriesRemaining--;
            }
            else {              // transmission successfull (received acknowledge before timeout)
                RetriesRemaining = 0;
            }

        }
        while ( RetriesRemaining );

        if ( PacketTX->Meta.StatusTX < tpst_Error ) {   // Transmission completed successfully: update data buffer
            AmountBytes -= PayloadSize;
            Data += PayloadSize;
        }
        else { ReturnCode = ec_radio_TransmissionFailed; }
    }

    PacketTX->Meta.DoNotReleaseBuffer = 0;            // re-allow releasing this buffer to avoid assertion inside radio_common_packet_release()
    PacketTX = ttc_radio_packet_release( PacketTX );  // return packet to its memory pool

trtb_Abort:
    if ( UserID )
    { ttc_radio_user_unlock( LogicalIndex, UserID ); }

    return ReturnCode;
}
e_ttc_radio_errorcode    ttc_radio_transmit_packet_now( t_u8 LogicalIndex, t_u8 UserID, t_ttc_packet* Packet, t_u16 PayloadSize, BOOL ReceiveAfterTransmit ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return ec_radio_IsLocked; } // radio locked by someone else!

    Assert_RADIO_Writable( Packet, ttc_assert_origin_auto );  // always check incoming pointer arguments

    return radio_common_transmit_packet( Config, Packet, PayloadSize, ReceiveAfterTransmit, NULL );
}
e_ttc_radio_errorcode    ttc_radio_transmit_packet_later( t_u8 LogicalIndex, t_u8 UserID, t_ttc_packet* Packet, t_u16 PayloadSize, BOOL ReceiveAfterTransmit ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return ec_radio_IsLocked; } // radio locked by someone else!
    Assert_RADIO_Writable( Packet, ttc_assert_origin_auto );  // always check incoming pointer arguments

    return radio_common_transmit_packet( Config, Packet, PayloadSize, ReceiveAfterTransmit, RCT_QUEUE_ONLY );
}
e_ttc_radio_errorcode    ttc_radio_transmit_packet_delayed( t_u8 LogicalIndex, t_u8 UserID, t_ttc_packet* Packet, t_u16 PayloadSize, BOOL ReceiveAfterTransmit, t_u32 Delay_ns ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return ec_radio_IsLocked; } // radio locked by someone else!
    Assert_RADIO_Writable( Packet, ttc_assert_origin_auto );  // always check incoming pointer arguments

    // calculate + send delayed packet
    u_ttc_packetimestamp_40 TransmitTime;
    ttc_radio_time_add_ns( LogicalIndex, & Packet->Meta.ReceiveTime, Delay_ns, &TransmitTime );

    return radio_common_transmit_packet( Config, Packet, PayloadSize, ReceiveAfterTransmit, &TransmitTime );
}
e_ttc_radio_errorcode    ttc_radio_transmit_start( t_u8 LogicalIndex, t_u8 UserID, u_ttc_packetimestamp_40* TransmitTime ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return ec_radio_IsLocked; } // radio locked by someone else!

    _driver_radio_transmit( Config, TransmitTime );
    return ec_radio_OK;
}
e_ttc_radio_errorcode    ttc_radio_transmit_waitfor( t_u8 LogicalIndex, t_u8 UserID ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return ec_radio_IsLocked; } // radio locked by someone else!

    if ( ttc_mutex_is_locked( & ( Config->FlagRunningTX ) ) ||      // transmission is running
            s_ttc_listize( & ( Config->List_PacketsTx ) )              // still packets to be transmitted
       ) {
        t_ttc_systick_delay Delay;
        ttc_systick_delay_init( &Delay, 100000 ); // 100ms

        while ( ttc_mutex_is_locked( & ( Config->FlagRunningTX ) ) ||      // transmission is running
                s_ttc_listize( & ( Config->List_PacketsTx ) )              // still packets to be transmitted
              ) {
            ttc_task_yield();
            if ( ttc_systick_delay_expired( &Delay ) ) { // still waiting: check radio transceiver
                _driver_radio_maintenance( Config );
                ttc_systick_delay_init( &Delay, 100000 ); // 100ms
            }
        }
    }

    return ec_radio_OK;
}
void                     ttc_radio_time_add_ns( t_u8 LogicalIndex, u_ttc_packetimestamp_40* Time, t_u32 Delay_ns, u_ttc_packetimestamp_40* TimeDelayed ) {
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );
    Assert_RADIO_Writable( Time, ttc_assert_origin_auto );  // always check incoming pointer arguments
    Assert_RADIO_Writable( TimeDelayed, ttc_assert_origin_auto );  // always check incoming pointer arguments

    radio_common_time_add_ns( Config, Time, Delay_ns, TimeDelayed );
}
e_ttc_radio_errorcode    ttc_radio_update_meta( t_u8 LogicalIndex, t_u8 UserID, t_ttc_packet* Packet ) {
    t_ttc_radio_config* Config = _ttc_radio_obtain_configuration( LogicalIndex, UserID );
    if ( !Config )
    { return ec_radio_IsLocked; } // radio locked by someone else!

    Assert_RADIO_Writable( Packet, ttc_assert_origin_auto );  // always check incoming pointer arguments

    return _driver_radio_update_meta( Config, Packet );
}
t_u8                     ttc_radio_user_new( t_u8 LogicalIndex, t_u16 AdditionalPacketBuffers ) {
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );

    t_u8 NewUserID = ++Config->UserID_Max;
    t_base Increase = ttc_heap_pool_increase( &( Config->Pool_Packets ), AdditionalPacketBuffers ); ( void ) Increase;
    Assert_RADIO( Increase == AdditionalPacketBuffers, ttc_assert_origin_auto ); // could not increase amount of pool buffers as requested. Out of memory? Check your memory usage!

    return NewUserID;
}
BOOL                     ttc_radio_user_lock( t_u8 LogicalIndex, t_u8 UserId ) {
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );
    Assert_RADIO( UserId, ttc_assert_origin_auto ); // must give nonzero user identification number. Check caller implementation!

    ttc_task_critical_begin();
    if ( Config->UserID_Lock ) {  // radio already locked!
        if ( Config->UserID_Lock == UserId ) { // same user is locking again
            Config->UserID_LockCount++;
            goto trul_Success;
        }
        ttc_task_critical_end();
        return FALSE;
    }

    Config->UserID_Lock = UserId;
    Config->UserID_LockCount = 1;

trul_Success:
    ttc_task_critical_end();
    return TRUE; // ttc_radio has been locked successfully
}
void                     ttc_radio_user_unlock( t_u8 LogicalIndex, t_u8 UserId ) {
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );

#if (TTC_ASSERT_RADIO == 1)
    ttc_task_critical_begin();

    Assert_RADIO( ( ( Config->UserID_Lock ) && ( Config->UserID_Lock == UserId ) ), ttc_assert_origin_auto ); // ttc_radio has been locked by someone else: You must only unlock if your lock was successfull before. Check your implementation!
    if ( Config->UserID_LockCount == 1 ) { // unlocked radio
        Config->UserID_Lock      = 0;
        Config->UserID_LockCount = 0;
    }
    else
    { Config->UserID_LockCount--; }

    ttc_task_critical_end();
#else
    Config->UserID_Lock = 0; // check correctness of code by activating asserts above
#endif
}
t_u8                     ttc_radio_user_locked( t_u8 LogicalIndex ) {
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );

    return Config->UserID_Lock;
}
BOOL                     ttc_radio_gpio_out( t_u8 LogicalIndex, e_ttc_radio_gpio Pin, BOOL NewState ) {
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );

    BOOL Result = 0;

    // high-level code being executed before calling low-level driver...

    // pass function call to interface or low-level driver function
    Result = _driver_radio_gpio_out( Config, Pin, NewState );

    // high-level code may check Result before returning it...

    return Result;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_radio(t_u8 LogicalIndex) {  }

t_ttc_radio_config* _ttc_radio_obtain_configuration( t_u8 LogicalIndex, t_u8 UserID ) {
    t_ttc_radio_config* Config = ttc_radio_get_configuration( LogicalIndex );

    if ( Config->UserID_Lock && ( Config->UserID_Lock != UserID ) )
    { return NULL; } // radio locked by someone else!

    return Config; // radio access granted
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
