#ifndef ARCHITECTURE_REAL_TIME_CLOCK_TYPES_H
#define ARCHITECTURE_REAL_TIME_CLOCK_TYPES_H

/** { real_time_clock_stm32l1.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *                     
 *  Low-Level datatypes for REAL_TIME_CLOCK devices on stm32l1 architectures.
 *  Structures, Enums and Defines being required by ttc_real_time_clock_types.h
 *
 *  Note: See ttc_real_time_clock.h for description of architecture independent REAL_TIME_CLOCK implementation.
 * 
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ******************************************************


//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic.h"

//} Includes
//{ Structures/ Enums required by ttc_real_time_clock_types.h ***********************

typedef struct { // register description (adapt according to stm32l1 registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} t_real_time_clock_register;

typedef struct {  // stm32l1 specific configuration data of single REAL_TIME_CLOCK device
  t_real_time_clock_register* BaseRegister;       // base address of REAL_TIME_CLOCK device registers
} __attribute__((__packed__)) t_real_time_clock_stm32l1_config;

// t_ttc_real_time_clock_arch is required by ttc_real_time_clock_types.h
typedef t_real_time_clock_stm32l1_config t_ttc_real_time_clock_arch;

//} Structures/ Enums


#endif //ARCHITECTURE_REAL_TIME_CLOCK_TYPES_H
