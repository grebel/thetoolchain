#ifndef ARCHITECTURE_REAL_TIME_CLOCK_H
#define ARCHITECTURE_REAL_TIME_CLOCK_H

/** { real_time_clock_stm32l1.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for real_time_clock devices on stm32l1 architectures.
 *  Structures, Enums and Defines being required by high-level real_time_clock and application.
 *
 *  Note: See ttc_real_time_clock.h for description of stm32l1 independent REAL_TIME_CLOCK implementation.
 *  
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************
#include "stm32l1xx_pwr.h"
#include "stm32l1xx_rcc.h"
#include "stm32l1xx_rtc.h"
#include "real_time_clock_stm32l1_types.h"
#include "../ttc_real_time_clock_types.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"

//} Includes
//{ Macro definitions ****************************************************

#define TTC_TR_RESERVED_MASK    ((t_u32)0x007F7F7F)

// define all supported low-level functions for ttc_real_time_clock_interface.h
#define ttc_driver_real_time_clock_prepare()                          real_time_clock_stm32l1_prepare()
#define ttc_driver_real_time_clock_reset(Config)                      real_time_clock_stm32l1_reset(Config)
#define ttc_driver_real_time_clock_load_defaults(Config)              real_time_clock_stm32l1_load_defaults(Config)
#define ttc_driver_real_time_clock_deinit(Config)                     real_time_clock_stm32l1_deinit(Config)
#define ttc_driver_real_time_clock_init(Config)                       real_time_clock_stm32l1_init(Config)
#define ttc_driver_real_time_clock_get_time(LogicalIndex,Time)        real_time_clock_stm32l1_get_time(LogicalIndex,Time)

//} Includes
//{ Function prototypes **************************************************

/** Prepares real_time_clock Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void real_time_clock_stm32l1_prepare();

/** checks if REAL_TIME_CLOCK driver already has been initialized
 * @param Config        pointer to struct t_ttc_real_time_clock_config (must have valid value for LogicalIndex)
 * @return              == TRUE: indexed REAL_TIME_CLOCK unit is already initialized
 */
bool real_time_clock_stm32l1_check_initialized(t_ttc_real_time_clock_config* Config);

/** returns reference to low-level configuration struct of REAL_TIME_CLOCK driver
 * @param Config        pointer to struct t_ttc_real_time_clock_config (must have valid value for LogicalIndex)
 * @return              pointer to struct t_ttc_real_time_clock_config (will assert if no configuration available)
 */
t_real_time_clock_stm32l1_config* real_time_clock_stm32l1_get_configuration(t_ttc_real_time_clock_config* Config);

/** loads configuration of REAL_TIME_CLOCK driver with default values
 * @param Config        pointer to struct t_ttc_real_time_clock_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_real_time_clock_errorcode real_time_clock_stm32l1_load_defaults(t_ttc_real_time_clock_config* Config);

/** fills out given Config with maximum valid values for indexed REAL_TIME_CLOCK
 * @param Config        pointer to struct t_ttc_real_time_clock_config (must have valid value for LogicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_real_time_clock_errorcode real_time_clock_stm32l1_get_features(t_ttc_real_time_clock_config* Config);

/** initializes single REAL_TIME_CLOCK
 * @param Config        pointer to struct t_ttc_real_time_clock_config (must have valid value for LogicalIndex)
 * @return              == 0: REAL_TIME_CLOCK has been initialized successfully; != 0: error-code
 */
e_ttc_real_time_clock_errorcode real_time_clock_stm32l1_init(t_ttc_real_time_clock_config* Config);

/** shutdown single REAL_TIME_CLOCK device
 * @param Config        pointer to struct t_ttc_real_time_clock_config (must have valid value for LogicalIndex)
 * @return              == 0: REAL_TIME_CLOCK has been shutdown successfully; != 0: error-code
 */
e_ttc_real_time_clock_errorcode real_time_clock_stm32l1_deinit(t_ttc_real_time_clock_config* Config);

/** reset configuration of real_time_clock driver and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_REAL_TIME_CLOCK_get_max_LogicalIndex())
 */
e_ttc_real_time_clock_errorcode real_time_clock_stm32l1_reset(t_ttc_real_time_clock_config* Config);

e_ttc_real_time_clock_errorcode  real_time_clock_stm32l1_get_time(t_u8 LogicalIndex,ttc_real_time_t_clock_time* Time);

e_ttc_real_time_clock_errorcode _real_time_clock_stm32l1_config(t_ttc_real_time_clock_config* Config);
//} Function prototypes

#endif //ARCHITECTURE_REAL_TIME_CLOCK_H