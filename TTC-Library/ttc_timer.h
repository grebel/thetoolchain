#ifndef TTC_TIMER_H
#define TTC_TIMER_H
/** { ttc_timer.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for TIMER devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_TIMER(tc_timer_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_timer_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_timer_init(LogicalIndex);
 *  4) use:         ttc_timer_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level timer and application.
 *
 *  Created from template ttc_device.h revision 27 at 20140415 05:28:31 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef EXTENSION_ttc_timer
#error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_timer.sh
#endif

//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "ttc_timer_types.h"
#include "interfaces/ttc_timer_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level timer only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * timer devices on all supported architectures.
 * Check timer/timer_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares timer Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_timer_prepare();
void _driver_timer_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_timer_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_timer_config* ttc_timer_get_configuration( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_timer_errorcode ttc_timer_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_TIMER_get_max_LogicalIndex())
 */
e_ttc_timer_errorcode  ttc_timer_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Seetings of indexed device are being reset to their default values.
 * Will automatically call ttc_timer_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of timer device (1..ttc_timer_get_max_index() )
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_timer_errorcode  ttc_timer_load_defaults( t_u8 LogicalIndex );

/** maps from logical to physical device index
 *
 * High-level timers (ttc_timer_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_TIMERn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any
 * order.
 *
 * @param LogicalIndex  logical index of timer device (1..ttc_timer_get_max_index() )
 * @return              physical index of timer device (0 = first physical timer device, ...)
 */
t_u8 ttc_timer_logical_2_physical_index( t_u8 LogicalIndex );

/** maps from physical to logical device index
 *
 * This is a reverse function to ttc_timer_logical_2_physical_index()
 *
 * @param  PhysicalIndex  physical index of timer device (0 = first physical timer device, ...)
 * @return                logical index of timer device (1..ttc_timer_get_max_index() )
 */
t_u8 ttc_timer_physical_2_logical_index( t_u8 PhysicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
void  ttc_timer_reset( t_u8 LogicalIndex );

/** set a timer with a given value
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 * @param TaskFunction    CallBack Function that will be executed. TaskFunction == NULL, do not use interruptions
 * @param Argument        passed as argument to Function()
 * @param Period          timer value (us)
 */
e_ttc_timer_errorcode ttc_timer_set( t_u8 LogicalIndex, void TaskFunction( void* ), void* Argument, t_u32 Period );

/** set a new reload value for given timer
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 * @param Period          reload value (us)
 */
void ttc_timer_change_period( t_u8 LogicalIndex, t_u32 Period );

/** Reload a given timer
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 *
 */
void ttc_timer_reload( t_u8 LogicalIndex );

/** read current timer value
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 * @return                current elapsed time in given timer (us)
 */
t_u32 ttc_timer_read_value( t_u8 LogicalIndex );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_timer(t_u8 LogicalIndex)

/** Called from low level TIMER interrupt service routine
 * Note: This function is private and should not be called from outside.
 *
 * @param TIMER_Generic  structure containing configuration data of TIMER which has expired the timer
 *
 */
void _ttc_timer_isr( t_ttc_timer_config* Config );

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_heap_ are passed to interfaces/ttc_heap_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl heap UPDATE
 */


/** fills out given Config with maximum valid values for indexed TIMER
 * @param Config        = pointer to struct t_ttc_timer_config (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_timer_errorcode _driver_timer_get_features( t_ttc_timer_config* Config );

/** shutdown single TIMER unit device
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for PhysicalIndex)
 * @return              == 0: TIMER has been shutdown successfully; != 0: error-code
 */
e_ttc_timer_errorcode _driver_timer_deinit( t_ttc_timer_config* Config );

/** initializes single TIMER unit for operation
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for PhysicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
e_ttc_timer_errorcode _driver_timer_init( t_ttc_timer_config* Config );

/** loads configuration of indexed TIMER unit with default values
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_timer_errorcode _driver_timer_load_defaults( t_ttc_timer_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for PhysicalIndex)
 */
void _driver_timer_reset( t_ttc_timer_config* Config );

/** set a timer with a given value
 *
 * @param Config          pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @param TaskFunction    CallBack Function that will be executed
 * @param Argument        passed as argument to Function()
 * @param Period          reload value (us)
 */
e_ttc_timer_errorcode _driver_timer_set( t_ttc_timer_config* Config );

/** set a new reload value for given timer
 *
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @param Period        reload value (us)
 */
void _driver_timer_change_period( t_ttc_timer_config* Config, t_u32 Period );

/** Reload value for given timer
 *
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 *
 */
void _driver_timer_reload( t_ttc_timer_config* Config );

/** read current timer value
 *
 * @param Config        pointer to struct t_ttc_timer_config (must have valid value for LogicalIndex)
 * @return   current elapsed time in given timer (us)
 */
t_u32 _driver_timer_read_value( t_ttc_timer_config* Config );

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_TIMER_H
