/*{ TTC_CHANNEL::.c ************************************************
 
                      The ToolChain
                      
   Device independent support for 
   
   
   written by Gregor Rebel 2012
   
 
}*/

#include "ttc_channel.h"

//{ Global variables *****************************************************

// stores pointers to created channels
ttc_heap_array_define(ttc_channel_t*, ttc_channel_configurations, TTC_CHANNEL_MAX_AMOUNT);

// protects channel creation/ modification from concurrent threads
ttc_mutex_t* ttc_channel_mutex_channels = NULL;

// current amount of created channels (<= TTC_CHANNEL_MAX_AMOUNT)
u8_t ttc_channel_amount_created = 0;

// Amount of channel nodes allocated/ freed so far
u8_t ttc_channel_amount_nodes_allocated = 0;

// protects node creation/ modification from concurrent threads
ttc_mutex_t* ttc_channel_mutex_nodes = NULL;

// stores empty nodes in a linked list
ttc_channel_node_t* FirstEmptyNode = NULL;

u32_t ttc_channel_used_nodes = 0;
u32_t ttc_channel_free_nodes = 0;

// Index of channel device to use for standard output
u8_t ttc_channel_index_stdout = 0;

//}Global variables
//{ Function definitions *************************************************

// channel management
inline u8_t ttc_channel_get_max_index() {

    return TTC_CHANNEL_MAX_AMOUNT; // -> ttc_channel_types.h
}
u8_t ttc_channel_create() {

    if (!ttc_channel_mutex_channels) { // first call: init global variables
        ttc_channel_mutex_channels = ttc_mutex_create();
        ttc_channel_mutex_nodes   = ttc_mutex_create();
    }

    ttc_mutex_lock(ttc_channel_mutex_channels, -1, (void(*)()) ttc_channel_create);

    Assert_Channel(ttc_channel_amount_created < TTC_CHANNEL_MAX_AMOUNT, ec_OutOfResources);
    u8_t ChannelIndex = ++ttc_channel_amount_created;
    ttc_channel_t* NewChannel = _ttc_channel_get_configuration(ChannelIndex);
    NewChannel->FirstNode = NULL;

    ttc_mutex_unlock(ttc_channel_mutex_channels);

    return ChannelIndex;
}
ttc_channel_node_handle_t ttc_channel_add_application(u8_t ChannelIndex,  ttc_heap_block_t* (*RxFunction)(ttc_channel_node_handle_t SourceNode, ttc_heap_block_t* Block) ) {
    ttc_mutex_lock(ttc_channel_mutex_channels, -1, (void(*)()) ttc_channel_add_application);

    ttc_channel_node_t* NewNode = _ttc_channel_get_empty_node();
    NewNode->Type = tcnt_Application;
    NewNode->Device.Application.RxFunction = (ttc_heap_block_t* (*)(void*, ttc_heap_block_t*)) RxFunction;

    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);
    _ttc_channel_add_node(Channel, NewNode);

    ttc_mutex_unlock(ttc_channel_mutex_channels);

    return (ttc_channel_node_handle_t) NewNode;
}
ttc_channel_node_handle_t ttc_channel_add_device(u8_t ChannelIndex,  ttc_channel_node_type_e Type, u8_t DeviceIndex) {
    ttc_mutex_lock(ttc_channel_mutex_channels, -1, (void(*)()) ttc_channel_add_device);

    ttc_channel_node_t* NewNode = _ttc_channel_get_empty_node();
    NewNode->Type = Type;
    NewNode->Device.GenericDevice.LogicalIndex = DeviceIndex;

    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);
    _ttc_channel_add_node(Channel, NewNode);
    if (_ttc_channel_device_attach(NewNode) == tce_OK) {
        _ttc_channel_add_node(Channel, NewNode);
    }
    else { // could not attach device: release node and return NULL
        _ttc_channel_release_node(NewNode);
        NewNode = NULL;
    }
    ttc_mutex_unlock(ttc_channel_mutex_channels);

    return (ttc_channel_node_handle_t) NewNode;
}
void ttc_channel_del_node(u8_t ChannelIndex, ttc_channel_node_handle_t Node) {
    ttc_channel_node_t* Node2Delete = (ttc_channel_node_t*) Node;
    Assert_Channel(Node2Delete, ec_InvalidArgument);

    ttc_mutex_lock(ttc_channel_mutex_channels, -1, (void(*)()) ttc_channel_add_device);
    ttc_channel_t* Channel = A(ttc_channel_configurations, ChannelIndex-1);
    Assert_Channel(Channel, ec_InvalidArgument); // corresponding channel not found!

    if (Channel->FirstNode == Node2Delete) { // removing first node of channel
        ttc_channel_node_t* LastNode = _ttc_channel_get_last_node(Channel);
        Channel->FirstNode = Node2Delete->Next;
        LastNode->Next = Channel->FirstNode;
    }
    else {                                   // remove from list of nodes
        ttc_channel_node_t* FirstNode    = Channel->FirstNode;
        ttc_channel_node_t* CurrentNode  = FirstNode->Next;
        ttc_channel_node_t* PreviousNode = FirstNode;
        while ( (CurrentNode != FirstNode) && (CurrentNode != Node2Delete) ) {
            Assert_Channel(CurrentNode, ec_InvalidImplementation); // current node should never be NULL
            PreviousNode = CurrentNode;
            CurrentNode = CurrentNode->Next;
        }
        Assert_Channel(CurrentNode == Node2Delete, ec_InvalidImplementation); // trying to delete node not present in channel

        // delete node from channel
        PreviousNode->Next = Node2Delete->Next;
    }

    _ttc_channel_device_detach(Node2Delete);
    _ttc_channel_release_node(Node2Delete);

    ttc_mutex_unlock(ttc_channel_mutex_channels);
}
ttc_channel_errorcode_e ttc_channel_get_last_error(ttc_channel_handle_t Channel, ttc_channel_node_handle_t* ErrorNode) {
    ttc_channel_t* ChannelConfig = (ttc_channel_t*) Channel;

    ttc_channel_errorcode_e ErrorCode = ChannelConfig->LastErrorCode;
    if (ErrorNode)
        *ErrorNode = ChannelConfig->LastErrorNode;

    // reset channel error
    ChannelConfig->LastErrorCode = tce_OK;

    return ErrorCode;
}
ttc_heap_block_t* ttc_channel_get_empty_block(u8_t ChannelIndex) {
    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);

    return _ttc_channel_get_empty_block(Channel);
}

// send data to a channel
ttc_channel_errorcode_e ttc_channel_send_block_from_node(ttc_channel_node_t* Sender, ttc_heap_block_t* Block) {
    Assert_Channel(Sender, ec_InvalidArgument);
    ttc_channel_t* Channel = _ttc_channel_get_configuration(Sender->ChannelIndex);
    return _ttc_channel_send_block(Channel, Block, Sender);
}
ttc_channel_errorcode_e ttc_channel_send_block(u8_t ChannelIndex, ttc_heap_block_t* Block) {
    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);

    return _ttc_channel_send_block(Channel, Block, NULL);
}
ttc_channel_errorcode_e ttc_channel_send_string(u8_t ChannelIndex, const u8_t* String, Base_t MaxSize) {
    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);

    Base_t Bytes2Copy = MaxSize;

    while (Bytes2Copy > 0) {
        ttc_heap_block_t* Block = _ttc_channel_get_empty_block(Channel);
        if (!Block)
            return tre_UnknownError;

        Block->Size = ttc_string_copy(Block->Buffer, (u8_t*) String, Block->MaxSize);
        if (Block->Size > 0) {
            ttc_channel_send_block(ChannelIndex, Block);

            String += Block->Size;
            if (*String == 0) // reached end of String[]
                Bytes2Copy = 0;
            else              // String[] is bigger than memory block: will continue with next block
                Bytes2Copy -= Block->Size;
        }
        else {
            ttc_heap_block_release(Block);
            Bytes2Copy = 0;
        }
    }
    return tre_OK;
}
ttc_channel_errorcode_e ttc_channel_send_string_const(u8_t ChannelIndex, const u8_t* String, Base_t MaxSize) {
    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);
    ttc_heap_block_t* Block = _ttc_channel_get_empty_block(Channel);
    if (!Block)
        return tce_DeviceNotReady;

    Block->Buffer = (u8_t*) (u8_t*) String;
    Block->Size   = ttc_string_length16( (const char*) String, MaxSize);

    ttc_channel_send_block(ChannelIndex, Block);
    return tre_OK;
}

// STDOUT-interface
ttc_channel_errorcode_e ttc_channel_stdout_set(u8_t ChannelIndex) {
    _ttc_channel_get_configuration(ChannelIndex); // will check if ChannelIndex is valid

    ttc_channel_index_stdout = ChannelIndex;

    return tre_OK;
}
void ttc_channel_stdout_send_block(ttc_heap_block_t* Block) {
    if (ttc_channel_index_stdout) {
        ttc_channel_send_block(ttc_channel_index_stdout, Block);
    }
}
void ttc_channel_stdout_send_string(const u8_t* String, Base_t MaxSize) {
    if (ttc_channel_index_stdout) {
        ttc_channel_send_string(ttc_channel_index_stdout, String, MaxSize);
    }
}

// private functions
void _ttc_channel_add_node(ttc_channel_t* Channel, ttc_channel_node_t* NewNode) {
    ttc_channel_node_t* LastNode = _ttc_channel_get_last_node(Channel);

    if (LastNode) { // add new node after last node of channel
        NewNode->Next  = Channel->FirstNode;
        LastNode->Next = NewNode;
    }
    else {          // NewNode will be only node in channel
        NewNode->Next      = NewNode;
        Channel->FirstNode = NewNode;
    }
    _ttc_channel_update_block_management(Channel, NewNode);
}
void _ttc_channel_update_block_management(ttc_channel_t* Channel, ttc_channel_node_t* Node) {
    if ( Node->Interface && (Node->Interface->get_empty_block) )
    { // block-management provided by attached device: test it

        ttc_heap_block_t* EmptyBlock = Node->Interface->get_empty_block();
        if (EmptyBlock && EmptyBlock->MaxSize) { // got non empty block: compare it with current channel settings
            if ( (Channel->Block_MinSize == 0) ||
                 (Channel->Block_MinSize > EmptyBlock->MaxSize)
               ) { // blocks from this device are smaller than from all other nodes: use its block management
                Channel->get_empty_block = Node->Interface->get_empty_block;
                Channel->Block_MinSize = EmptyBlock->MaxSize;
            }
            if (Channel->Block_MaxSize < EmptyBlock->MaxSize) // update statistics
                Channel->Block_MaxSize = EmptyBlock->MaxSize;

            // give back memory block
            ttc_heap_block_release(EmptyBlock);
        }
    }
}
ttc_channel_node_t* _ttc_channel_get_last_node(ttc_channel_t* Channel) {
    Assert_Channel(Channel, ec_InvalidArgument);
    ttc_channel_node_t* FirstNode = Channel->FirstNode;
    if (!FirstNode) return NULL;

    ttc_channel_node_t* NodeInChannel = FirstNode->Next;
    Assert_Channel(NodeInChannel, ec_InvalidImplementation); // Next-pointer should never be NULL

    u8_t Limit = 255;
    while (NodeInChannel != FirstNode) {
        NodeInChannel = NodeInChannel->Next;
        Assert_Channel(NodeInChannel, ec_InvalidConfiguration); // Next-pointer must not be NULL
        Assert_Channel(Limit--,       ec_InvalidConfiguration); // this channel is definitely too large!
    }

    return NodeInChannel;
}
ttc_channel_t* _ttc_channel_get_configuration(u8_t ChannelIndex) {
     Assert_Channel(ChannelIndex > 0,                            ec_InvalidArgument); // logical index starts at 1
     Assert_Channel(ChannelIndex <= ttc_channel_get_max_index(), ec_InvalidArgument); // outside range of activated channels

     ttc_channel_t* ChannelConfig = (ttc_channel_t*) A(ttc_channel_configurations, ChannelIndex-1);
     if (!ChannelConfig) {
         ChannelConfig = ttc_heap_alloc_zeroed( sizeof(ttc_channel_t) );
         A(ttc_channel_configurations, ChannelIndex-1) = ChannelConfig;
     }

     return ChannelConfig;
 }
ttc_channel_node_t* _ttc_channel_get_empty_node() {
    ttc_mutex_lock(ttc_channel_mutex_nodes, -1, (void(*)()) _ttc_channel_get_empty_node);

    ttc_channel_node_t* EmptyNode;
    if (FirstEmptyNode) { // remove first empty node from linked list of empty nodes
        EmptyNode = FirstEmptyNode;
        FirstEmptyNode = EmptyNode->Next;
        ttc_memory_set(EmptyNode, 0, sizeof(ttc_channel_node_t));
    }
    else {                // allocate new empty node
        EmptyNode = (ttc_channel_node_t*) ttc_heap_alloc_zeroed( sizeof(ttc_channel_node_t) );
    }
    Assert_Channel(EmptyNode, ec_Malloc); // node could not be allocated

    ttc_mutex_unlock(ttc_channel_mutex_nodes);

    return EmptyNode;
}
void _ttc_channel_release_node(ttc_channel_node_t* Node) {
    Assert_Channel(Node, ec_InvalidArgument);

    ttc_mutex_lock(ttc_channel_mutex_nodes, -1, (void(*)()) _ttc_channel_release_node);
    if (FirstEmptyNode)
        Node->Next = FirstEmptyNode; // Node now will be the first empty node
    else
        Node->Next = NULL;           // node is only element in linked list of empty nodes
    FirstEmptyNode = Node;

    ttc_mutex_unlock(ttc_channel_mutex_nodes);
}
ttc_channel_errorcode_e _ttc_channel_device_attach(ttc_channel_node_t* ChannelNode) {

    ttc_channel_errorcode_e Error = tce_OK;

    switch (ChannelNode->Type) {

    case tcnt_Application: break;
    case tcnt_Channel: {

        break;
    }
    case tcnt_Protocol: {
        Assert_Halt_EC(ec_NotImplemented); break; // ToDo: implement this type
        break;
    }
#ifdef EXTENSION_500_ttc_radio
    case tcnt_Radio: {
        Assert_Halt_EC(ec_NotImplemented); break; // ToDo: implement this type
        break;
    }
#endif
#ifdef EXTENSION_500_ttc_usart
    case tcnt_USART: {
        Error = ttc_usart_register_receive(ChannelNode->Device.GenericDevice.LogicalIndex, _ttc_channel_receive_from_usart, (void*) ChannelNode);
        if (Error == tce_OK) {
            ChannelNode->Interface = ttc_usart_get_channel_interface();
        }
        break;
    }
#endif
#ifdef EXTENSION_500_ttc_spi
    case tcnt_SPI: {
        Assert_Halt_EC(ec_NotImplemented); break; // ToDo: implement this type
        break;
    }
#endif
#ifdef EXTENSION_500_ttc_i2c
    case tcnt_I2C: {
        Assert_Halt_EC(ec_NotImplemented); break; // ToDo: implement this type
        break;
    }
#endif
    default: Assert_Halt_EC(ec_NotImplemented); break; // given Type not supported (Did you forget to activate something?)
    }

    if (Error) { // could not attach device: reset configuration
        ChannelNode->Interface = NULL;
    }

    return Error;
}
ttc_channel_errorcode_e _ttc_channel_device_detach(ttc_channel_node_t* ChannelNode) {

    ttc_channel_errorcode_e Error = tce_OK;

    switch (ChannelNode->Type) {

    case tcnt_Application: break;
    case tcnt_Channel: {
        Assert_Halt_EC(ec_NotImplemented); break; // ToDo: implement this type
        break;
    }
    case tcnt_Protocol: {
        Assert_Halt_EC(ec_NotImplemented); break; // ToDo: implement this type
        break;
    }
#ifdef EXTENSION_500_ttc_radio
    case tcnt_Radio: {
        Assert_Halt_EC(ec_NotImplemented); break; // ToDo: implement this type
        break;
    }
#endif
#ifdef EXTENSION_500_ttc_usart
    case tcnt_USART: {
        ttc_usart_register_receive(ChannelNode->Device.GenericDevice.LogicalIndex, NULL, ChannelNode);
        
        break;
    }
#endif
#ifdef EXTENSION_500_ttc_spi
    case tcnt_SPI: {
        Assert_Halt_EC(ec_NotImplemented); break; // ToDo: implement this type
        break;
    }
#endif
#ifdef EXTENSION_500_ttc_i2c
    case tcnt_I2C: {
        Assert_Halt_EC(ec_NotImplemented); break; // ToDo: implement this type
        break;
    }
#endif
    default: Assert_Halt_EC(ec_NotImplemented); break; // given Type not supported
    }

    return Error;
}
ttc_heap_block_t* _ttc_channel_get_empty_block(ttc_channel_t* Channel) {
    if (Channel->get_empty_block)
        return Channel->get_empty_block();

    // ToDo: allocate block on our own
    return NULL;
}
ttc_channel_errorcode_e _ttc_channel_send_block(ttc_channel_t* Channel, ttc_heap_block_t* Block, ttc_channel_node_t* ExceptNode) {
    ttc_channel_errorcode_e Error = tce_OK;

    ttc_heap_block_use(Block);
    ttc_channel_node_t* CurrentNode = Channel->FirstNode;
    if (CurrentNode) {
        ttc_channel_errorcode_e LastError;
        ttc_channel_node_t* LastErrorNode = NULL;
        do {
            if (CurrentNode != ExceptNode) {
                if (CurrentNode->Type == tcnt_Application) { // pass block to application
                    ttc_heap_block_use(Block);
                    ttc_heap_block_t* ReturnedBlock = CurrentNode->Device.Application.RxFunction(ExceptNode, Block);
                    if (ReturnedBlock) { // application returned Block: decrease its UseCounter
                        Assert_Channel(ReturnedBlock == Block, ec_InvalidImplementation); // application must not return different block!
                        ttc_heap_block_release(Block);
                    }
                }
                else {                                      // pass block to generic device
                    if (CurrentNode->Interface && CurrentNode->Interface->send_block) { // function available: directly pass memory block
                        ttc_heap_block_use(Block); // block will be released by device after being sent
                        // ToDo: Check why send_block() does not release Block
                        LastError = CurrentNode->Interface->send_block(CurrentNode->Device.GenericDevice.LogicalIndex, Block);
                        if (LastError != tce_OK) {
                            Error = LastError;
                            LastErrorNode = CurrentNode;
                        }
                    }
                }
            }
            CurrentNode = CurrentNode->Next;
        } while (CurrentNode->Next != Channel->FirstNode);
    }
    ttc_heap_block_release(Block);

    return Error;
}

// low-level device support
ttc_heap_block_t* _ttc_channel_receive_from_usart(void* Argument, ttc_usart_config_t* USART_Generic, ttc_heap_block_t* Block) {
    ttc_channel_node_t* SenderNode = (ttc_channel_node_t*) Argument;
    (void) USART_Generic;

    ttc_channel_send_block_from_node(SenderNode, Block);
    return NULL;
}


/* HEAP of functions

ttc_channel_errorcode_e ttc_channel_get_features(u8_t ChannelIndex, ttc_channel_t* Channel) {
    if ( (ChannelIndex > TTC_CHANNEL_MAX_AMOUNT) || (ChannelIndex < 1) )
        return ec_DeviceNotFound;
    Assert_Channel(Channel, ec_InvalidArgument);

    memset(Channel, 0, sizeof(Channel));
    ttc_channel_errorcode_e Error = tre_NotImplemented;

    switch (ttc_channel_get_driver_type(ChannelIndex)) {
#ifdef EXTENSION_400_channel_stm32w
    case trd_cc1101_spi:
        channel_stm32w_get_features(ChannelIndex, Channel);
        Error = tre_OK;
        break;
#endif
#ifdef EXTENSION_400_channel_cc1101_spi
    case trd_cc1101_spi:
        channel_cc1101_get_features(ChannelIndex, Channel);
        Error = tre_OK;
        break;
#endif
#ifdef EXTENSION_400_channel_cc1120_spi
    case trd_cc1120_spi:
        channel_cc1120_get_features(ChannelIndex, Channel);
        Error = tre_OK;
        break;
#endif
    default:
        break;
    }

    if (Error == tre_OK) { // low-level driver available: fill in generic maximum values
        Channel->ChannelIndex = ChannelIndex;
        Channel->Driver = ttc_channel_get_driver_type(ChannelIndex);
        Channel->Flags.Bit.DelayedTransmits = 0;
        Channel->Flags.Bit.RxDMA            = 0;
        Channel->Flags.Bit.TxDMA            = 0;
    }

    return Error;
}
ttc_channel_errorcode_e ttc_channel_get_max_channel(u8_t ChannelIndex, u8_t* MaxChannelTX, u8_t* MaxChannelRX) {

    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);
    Assert_Channel(MaxChannelTX, ec_InvalidArgument);
    Assert_Channel(MaxChannelRX, ec_InvalidArgument);

    Assert_Channel(FALSE,ec_NotImplemented); //D Problem here with function calls not low level function avaliable for CC1101 and CC1120
    ttc_channel_errorcode_e Error = tre_NotImplemented;
#ifdef EXTENSION_400_channel_stm32w
    if (Channel->Driver == trd_stm32w)
        Error = ttc_channel_get_max_channel(ChannelIndex, MaxChannelTX, MaxChannelRX); //ToDo: Wrong function call
#endif
#ifdef EXTENSION_400_channel_cc1101_spi
    if (Channel->Driver == trd_cc1101_spi)
        Error = ttc_channel_get_max_channel(ChannelIndex, MaxChannelTX, MaxChannelRX); //ToDo: Wrong function call
#endif
#ifdef EXTENSION_400_channel_cc1120_spi
    if (Channel->Driver == trd_cc1120_spi)
        Error = ttc_channel_get_max_channel(ChannelIndex, MaxChannelTX, MaxChannelRX); //ToDo: Wrong function call
#endif

  return Error;
}
ttc_channel_errorcode_e ttc_channel_init(u8_t ChannelIndex) {
    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);
    Assert_Channel(Channel, ec_InvalidArgument);
    Assert_Channel(Channel->ChannelIndex > 0, ec_InvalidArgument);
    Assert_Channel(Channel->ChannelIndex <= ttc_channel_get_max_index(), ec_InvalidArgument);

#ifdef EXTENSION_500_ttc_watchdog
    ttc_watchdog_init1(0,256);
    ttc_watchdog_reload1();
#endif
    // check given configuration against features of configured channel
    ttc_channel_t channel_Features;
    ttc_channel_get_features(Channel->ChannelIndex, &channel_Features);

    Assert_Channel(Channel->ChannelRx   <= channel_Features.ChannelRx,   ec_InvalidConfiguration);
    Assert_Channel(Channel->ChannelTx   <= channel_Features.ChannelTx,   ec_InvalidConfiguration);
    Assert_Channel(Channel->Max_Header  <= channel_Features.Max_Header,  ec_InvalidConfiguration);
    Assert_Channel(Channel->Max_Footer  <= channel_Features.Max_Footer,  ec_InvalidConfiguration);
    Assert_Channel(Channel->Max_Payload <= channel_Features.Max_Payload, ec_InvalidConfiguration);
    Assert_Channel(Channel->LevelRx     <= channel_Features.LevelRx,     ec_InvalidConfiguration);
    Assert_Channel(Channel->LevelRx     <= channel_Features.LevelTx,     ec_InvalidConfiguration);
    Assert_Channel(Channel->Driver      == channel_Features.Driver,      ec_InvalidConfiguration);

    // limit configured flags to supported features
    Channel->Flags.All &= channel_Features.Flags.All;

    if (!Channel->Queue_Tx)
        Channel->Queue_Tx = ttc_queue_pointer_create(Channel->Size_QueueTx);

    if (!Channel->Queue_Rx)
        Channel->Queue_Rx = ttc_queue_pointer_create(Channel->Size_QueueRx);

    if (!Channel->MutexPacketReceived) {
        Channel->MutexPacketReceived = ttc_mutex_create();
        ttc_mutex_lock(Channel->MutexPacketReceived, 0);
    }
    Channel->TaskYield = ttc_channel_task_yield;

    ttc_channel_errorcode_e Error = tre_NotImplemented;
    ttc_channel_set_amplifier(ChannelIndex, tram_Init);

    Error = tre_NotImplemented;
#ifdef EXTENSION_400_channel_stm32w
    if (Channel->Driver == trd_stm32w_spi)
        Error = channel_stm32w_init(Channel);
#elif EXTENSION_400_channel_cc1101_spi
    if (Channel->Driver == trd_cc1101_spi)
        Error = channel_cc1101_init(Channel);
#elif EXTENSION_400_channel_cc1120_spi
    if (Channel->Driver == trd_cc1120_spi)
        Error = channel_cc1120_init(Channel);
#else
    Assert(0,ec_InvalidConfiguration); //no channel type selected
#endif

    ttc_channel_set_operating_mode(ChannelIndex,trm_Receive);
    if (Error == tre_OK) { // channel has been initialized successfully: initialize some high-level stuff
        if (!Channel->Flags.Bit.Task_RX_Started) {
            Channel->Flags.Bit.Task_RX_Started = 1;
            ttc_task_create( _ttc_channel_task_rx, "_ttc_channel_task_rx", 128, Channel, 1, NULL);
        }
        if (!Channel->Flags.Bit.Task_TX_Started) {
            Channel->Flags.Bit.Task_TX_Started = 1;
            ttc_task_create( _ttc_channel_task_tx, "_ttc_channel_task_tx", 256, Channel, 0, NULL);
        }
        ttc_channel_set_power_tx(Channel->ChannelIndex, Channel->LevelTx);
    }
#ifdef EXTENSION_500_ttc_watchdog
    ttc_watchdog_reload1();
#endif
    return Error;
}
                 void ttc_channel_send_block(u8_t ChannelIndex, ttc_heap_block_t* Block) {
    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);

    Assert(Channel != NULL, ec_NULL);//D
    if(Channel!=0){
        Block->Hint = Channel->TargetAddress;
        while (ttc_queue_pointer_push_back(Channel->Queue_Tx, Block) != tqe_OK)
            ttc_channel_task_yield();
    }
}
ttc_channel_errorcode_e ttc_channel_send_raw(u8_t ChannelIndex, const u8_t* Buffer, Base_t Amount) {
    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);

    while (Amount > 0) {
        ttc_heap_block_t* Block = _ttc_channel_get_empty_block(Channel);
        if (!Block)
            return tre_TimeOut;

        u8_t Size;

        if (Amount > Channel->Max_Payload)
            Size = Channel->Max_Payload;
        else
            Size = Amount;

        ttc_memory_copy(Block->Buffer, (u8_t*) Buffer, Size);
        Block->Size = Size;
        ttc_channel_send_block(ChannelIndex, Block);

        Buffer += Size ;
        Amount -= Size ;
    }
    return tre_OK;
}
                 void ttc_channel_register_rx_function(u8_t ChannelIndex, ttc_heap_block_t* (*RxFunction)(struct ttc_Channel_s* channelGeneric, ttc_heap_block_t* Block, u8_t RSSI, u8_t TargetAddress)) {
    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);

    Channel->RxFunction = RxFunction;
}
                 void ttc_channel_register_tx_function(u8_t ChannelIndex, void (*TxFunction)(ttc_channel_t* channelGeneric, u8_t Size, const u8_t* Buffer)) {
    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);

    Channel->TxFunction = TxFunction;
}
                 void ttc_channel_set_destination(u8_t ChannelIndex, u8_t Address) {
                     ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);
                     Channel->TargetAddress = Address;
                 }
                 void ttc_channel_set_channel_rx(u8_t ChannelIndex, u8_t ChannelIndex) {
    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);
    Assert_Channel(ChannelIndex > 0, ec_InvalidArgument);                             // logical index starts at 1
    Assert_Channel(ChannelIndex <= Channel->MaxChannelRx, ec_InvalidArgument);  // invalid channel index

    ttc_channel_errorcode_e Error = tre_NotImplemented;
#ifdef EXTENSION_400_channel_stm32w
    if (Channel->Driver == trd_stm32w)
        Error = channel_stm32w_set_channel(Channel, ChannelIndex); // cc1101 does not provide independent rx/tx channels;
#endif
#ifdef EXTENSION_400_channel_cc1101_spi
    if (Channel->Driver == trd_cc1101_spi)
        Error = channel_cc1101_set_channel(Channel, ChannelIndex); // cc1101 does not provide independent rx/tx channels;
#endif
#ifdef EXTENSION_400_channel_cc1120_spi
    if (Channel->Driver == trd_cc1120_spi)
        Error = channel_cc1120_set_channel(Channel, ChannelIndex); // cc1101 does not provide independent rx/tx channels;
#endif

  return;
}
                 void ttc_channel_set_channel_tx(u8_t ChannelIndex, u8_t ChannelIndex) {
    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);
    Assert_Channel(ChannelIndex > 0, ec_InvalidArgument); // logical index starts at 1
    Assert_Channel(ChannelIndex <= Channel->MaxChannelTx, ec_InvalidArgument);  // invalid channel index

    ttc_channel_errorcode_e Error = tre_NotImplemented;
#ifdef EXTENSION_400_channel_stm32w
    if (Channel->Driver == trd_stm32w)
        Error = channel_stm32w_set_channel(Channel, ChannelIndex); // cc1101 does not provide independent rx/tx channels;
#endif
#ifdef EXTENSION_400_channel_cc1101_spi
    if (Channel->Driver == trd_cc1101_spi)
        Error = channel_cc1101_set_channel(Channel, ChannelIndex); // cc1101 does not provide independent rx/tx channels;
#endif
#ifdef EXTENSION_400_channel_cc1120_spi
    if (Channel->Driver == trd_cc1120_spi)
        Error = channel_cc1120_set_channel(Channel, ChannelIndex); // cc1101 does not provide independent rx/tx channels;
#endif

  return;
}
                 void ttc_channel_flush_tx(u8_t ChannelIndex) {
    ttc_channel_t* Channel = _ttc_channel_get_configuration(ChannelIndex);

    if (Channel->Flags.Bit.DelayedTransmits) { // delayed transmits have been activated for this channel

        // wait for transmit queue to run empty
        while (ttc_queue_pointer_get_amount(Channel->Queue_Tx) > 0)
            ttc_channel_task_yield();
    }
}
   ttc_channel_driver_e ttc_channel_get_driver_type(u8_t ChannelIndex) {
       switch (ChannelIndex) {
#ifdef TTC_CHANNEL1
       case 1: return TTC_CHANNEL1; // shall be defined as one from ttc_channel_types.h/ttc_channel_driver_e
#endif
#ifdef TTC_CHANNEL2
       case 2: return TTC_CHANNEL2; // shall be defined as one from ttc_channel_types.h/ttc_channel_driver_e
#endif
#ifdef TTC_CHANNEL3
       case 3: return TTC_CHANNEL3; // shall be defined as one from ttc_channel_types.h/ttc_channel_driver_e
#endif
#ifdef TTC_CHANNEL4
       case 4: return TTC_CHANNEL4; // shall be defined as one from ttc_channel_types.h/ttc_channel_driver_e
#endif
#ifdef TTC_CHANNEL5
       case 5: return TTC_CHANNEL5; // shall be defined as one from ttc_channel_types.h/ttc_channel_driver_e
#endif
       default: return trd_None;
       }
 }
  ttc_heap_block_t* _ttc_channel_get_empty_block(Channel) {

    if (ttc_channel_queue_empty_blocks == NULL) { // first call: create queue
        ttc_channel_queue_empty_blocks = ttc_queue_pointer_create(ttc_channel_MAX_MEMORY_BUFFERS+1);
        ttc_channel_mutex_empty_blocks = ttc_mutex_create();
    }
    ttc_heap_block_t* Block = NULL;
    u16_t TimeOut = 1000;

    while (Block == NULL) {
        Assert_Channel(ttc_mutex_lock(ttc_channel_mutex_empty_blocks, 1000000) == tqe_OK, ec_TimeOut); // could not get lock!
        ttc_queue_error_e  Error = ttc_queue_pointer_pull_front(ttc_channel_queue_empty_blocks, (void**) &Block);

        if (Error != tqe_OK) { // no released memory block available: alloc new block
            if (ttc_channel_allocated_blocks < ttc_channel_MAX_MEMORY_BUFFERS) {
                Block = ttc_heap_alloc_block(TTC_NETWORK_MAX_PAYLOAD_SIZE,
                                               0, // will be used for target address
                                               _ttc_channel_release_block
                                               );
                ttc_channel_allocated_blocks++;
            }
            else
                Block = NULL;
        }
        else { // reuse released memory block
            // Buffer-pointer might have been changed
            Assert_Channel(Block != NULL, ec_IllegalPointer); //D
            Block->Buffer = (u8_t*) (  ( (u32_t) Block ) + sizeof(ttc_heap_block_t)  );
            Block->Size = 0;
        }
        if (1) {
            if (0) ttc_task_end_criticalsection();
            else   ttc_mutex_unlock(ttc_channel_mutex_empty_blocks);
        }
        if (Block == NULL) { // no empty block available: wait some time and try again
            Assert(TimeOut-- > 0, ec_UNKNOWN); // cannot get empty memory block!
            ttc_task_yield();
        }
    }
    ttc_channel_used_blocks++;

    return Block;
}
                 void ttc_channel_task_yield() {
    ttc_task_yield();
}
                 u8_t _ttc_channel_check_bytes_received(ttc_channel_t* Channel) {
#ifdef EXTENSION_400_channel_stm32w
    if (Channel->Driver == trd_stm32w)
        return channel_stm32w_spi_register_single_read( (channel_stm32w_driver_t*) Channel->DriverCfg, rcr_RXBYTES);
#endif
#ifdef EXTENSION_400_channel_cc1101_spi
    if (Channel->Driver == trd_cc1101_spi)
        return channel_cc1101_spi_register_single_read( (channel_cc1101_driver_t*) Channel->DriverCfg, rcr_RXBYTES);
#endif
#ifdef EXTENSION_400_channel_cc1120_spi
    if (Channel->Driver == trd_cc1120_spi)
        return channel_cc1120_spi_register_read((channel_cc1120_driver_t*) Channel->DriverCfg, rcr_CC112X_SINGLE_RXFIFO);
#endif
    return 0;
}
                 BOOL _ttc_channel_check_mode(ttc_channel_t* Channel) {

    BOOL IllegalMode = FALSE;
#ifdef EXTENSION_400_channel_stm32w
    if (Channel->Driver == trd_stm32w)
        IllegalMode = channel_stm32w_check_mode(Channel);
#endif
#ifdef EXTENSION_400_channel_cc1101_spi
    if (Channel->Driver == trd_cc1101_spi)
        IllegalMode = channel_cc1101_check_mode(Channel);
#endif
#ifdef EXTENSION_400_channel_cc1120_spi
    if (Channel->Driver == trd_cc1120_spi)
        IllegalMode = channel_cc1120_check_mode(Channel);
#endif
    return IllegalMode;
}
                 void _ttc_channel_release_block(ttc_heap_block_t* Block) {
    Assert(Block != NULL, ec_NULL);

    if (1) {
        if (0) ttc_task_begin_criticalsection(_ttc_channel_release_block);
        else   ttc_mutex_lock(ttc_channel_mutex_empty_blocks, -1);
    }
    ttc_queue_error_e Error = tqe_UNKNOWN;

    Error = ttc_queue_pointer_push_back(ttc_channel_queue_empty_blocks, Block);
    Assert_Channel(Error == tre_OK, ec_UNKNOWN); // could not release memory block (maybe queue is too small?)

    if (1) {
        if (0) ttc_task_end_criticalsection();
        else   ttc_mutex_unlock(ttc_channel_mutex_empty_blocks);
    }
    ttc_channel_free_blocks++;
}
                 void _ttc_channel_task_rx(void* Argument) {
    ttc_channel_t* Channel = (ttc_channel_t*) Argument;
    ttc_heap_block_t* Block = NULL;

    while (1) {
#ifdef EXTENSION_500_ttc_watchdog
    ttc_watchdog_reload1();
#endif

        // wait for interrupt service routine to unlock rx-mutex
        if (ttc_mutex_lock(Channel->MutexPacketReceived, 200000) != tqe_OK) {
#if TTC_DEBUG_channel==1
            if (1) ttc_gpio_set(TTC_DEBUG_channel_PIN3); //D
#endif
            if (_ttc_channel_check_mode(Channel) ) { // did not receive anything for a while: check channel
                ttc_channel_set_operating_mode(Channel->ChannelIndex, Channel->Mode); // channel in unexpected mode: try to correct
                Assert_Channel(_ttc_channel_check_mode(Channel) == 0, ec_UNKNOWN); // could not fix unexpected channel mode
            }
#if TTC_DEBUG_channel==1
            if (1) ttc_gpio_clr(TTC_DEBUG_channel_PIN3);
#endif
        }
        else {
#if TTC_DEBUG_channel==1
            ttc_gpio_set(TTC_DEBUG_channel_PIN1); //D
#endif
            do {
                if (!Block) {
                    Block = _ttc_channel_get_empty_block(Channel);
                    Assert_Channel(Block, ec_NULL);
                }
                Block = _ttc_channel_read_packet(Channel, Block);
            } while (_ttc_channel_check_bytes_received(Channel));
            if (1) ttc_task_check_stack();
#if TTC_DEBUG_channel==1
    ttc_gpio_clr(TTC_DEBUG_channel_PIN1);
#endif
        }
    }
}
                 void _ttc_channel_task_tx(void* Argument) {
    ttc_channel_t* Channel = (ttc_channel_t*) Argument;

    ttc_queue_pointers_t* Queue_Tx = Channel->Queue_Tx;
    ttc_heap_block_t*        Block;
    Base_t MaxPayloadSize = Channel->Max_Payload;
    Base_t BytesRemaining;
    u16_t  Bytes2Send;
    u8_t*  Reader;

    while (1) {
        while (ttc_queue_pointer_pull_front(Queue_Tx, (void**) &Block) == tqe_OK) {
            Reader = Block->Buffer;
            BytesRemaining = Block->Size;

            while (BytesRemaining > 0) { // send out packets until complete block has been transmitted
                if (BytesRemaining > MaxPayloadSize)
                    Bytes2Send = MaxPayloadSize;
                else
                    Bytes2Send = BytesRemaining;

                if (Channel->TxFunction)
                    Channel->TxFunction(Channel, Bytes2Send, Reader);

                ttc_channel_errorcode_e Error = tre_OK;
                ttc_channel_set_amplifier(Channel->ChannelIndex, tram_TransmitHighGain);
                switch (Channel->Driver) { // transmit single packet

#ifdef EXTENSION_400_channel_stm32w
                case trd_stm32w: {
                    Error = channel_stm32w_packet_send(Channel, Block->Hint, Bytes2Send, Reader);
                    break;
                }
#endif
#ifdef EXTENSION_400_channel_cc1101_spi
                case trd_cc1101_spi: {
                    Error = channel_cc1101_packet_send(Channel, Block->Hint, Bytes2Send, Reader);
                    break;
                }
#endif
#ifdef EXTENSION_400_channel_cc1120_spi
                case trd_cc1120_spi: {
                    Error = channel_cc1120_packet_send(Channel, Block->Hint, Bytes2Send, Reader);
                    break;
                }
#endif

                default:
                    ttc_task_msleep(200); // Simulate transmission
                }
                if (Error != tre_OK) { // handle error condition
                    Assert_Channel(0, ec_UNKNOWN); // ToDo: proper generic error handling
                }

                BytesRemaining -= Bytes2Send;
                Reader += Bytes2Send;
            }
            ttc_channel_set_amplifier(Channel->ChannelIndex, tram_ReceiveHighGain);

            // send block back to its originator (mostly _ttc_channel_release_block())
            ttc_heap_block_release(Block);
        }
        ttc_channel_task_yield();
        ttc_task_check_stack();
    }
}
  ttc_heap_block_t* _ttc_channel_read_packet(ttc_channel_t* Channel, ttc_heap_block_t* Block) {
    Assert_Channel(Channel, ec_NULL);
    Assert_Channel(Block, ec_NULL);

    u8_t RSSI = 0;
#ifdef EXTENSION_400_channel_stm32w
    if (Channel->Driver == trd_stm32w) {
        Block->Size = channel_stm32w_packet_receive(Channel, Block->Buffer, TTC_NETWORK_MAX_PAYLOAD_SIZE, &(Block->Hint), &RSSI);
        Assert_Channel(Block->Size < TTC_NETWORK_MAX_PAYLOAD_SIZE, ec_InvalidArgument); // got invalid block size (maybe corrupted data?)
    }
#endif
#ifdef EXTENSION_400_channel_cc1101_spi
    if (Channel->Driver == trd_cc1101_spi) {
        Block->Size = channel_cc1101_packet_receive(Channel, Block->Buffer, TTC_NETWORK_MAX_PAYLOAD_SIZE, &(Block->Hint), &RSSI);
        Assert_Channel(Block->Size < TTC_NETWORK_MAX_PAYLOAD_SIZE, ec_InvalidArgument); // got invalid block size (maybe corrupted data?)
    }
#endif
#ifdef EXTENSION_400_channel_cc1120_spi
    if (Channel->Driver == trd_cc1120_spi) {
        Block->Size = channel_cc1120_packet_receive(Channel, Block->Buffer, TTC_NETWORK_MAX_PAYLOAD_SIZE, &(Block->Hint), &RSSI);
        Assert_Channel(Block->Size < TTC_NETWORK_MAX_PAYLOAD_SIZE, ec_InvalidArgument); // got invalid block size (maybe corrupted data?)
    }
#endif
    if (Channel->RxFunction) {
        Block = Channel->RxFunction(Channel, Block, RSSI, Block->Hint);
    }

    return Block;
}

HEAP of functions */

//} Function definitions
