/*{ ttc_usb::.c ************************************************

                      The ToolChain
                      
   Device independent support for
   the USB Bus
   written by Sascha Poggemann 2012
   

}*/

#include "ttc_usb.h"
//{ Global variables *****************************************************

ttc_heap_array_define(ttc_usb_generic_t*, USBConfigs, TTC_AMOUNT_USBS);

//// stores released memory blocks for reuse as transmit or receive blocks
//ttc_queue_pointers_t* ttc_usb_queue_empty_blocks = NULL;   // stores empty memory blocks for reuse

//// protects queues from simultaneous access
//ttc_mutex_t*            ttc_usb_mutex_empty_blocks = NULL;

//}Global variables
//{ Function definitions *************************************************

    ttc_usb_generic_t* ttc_usb_get_configuration(u8_t LogicalIndex) {
    Assert_USB(LogicalIndex > 0,                          ec_InvalidArgument); // logical index starts at 1
    //Assert_USB(LogicalIndex <= ttc_usb_get_max_index(), ec_InvalidArgument); // outside range of activated USBs
    ttc_usb_generic_t* USB_Generic = A(USBConfigs, LogicalIndex-1);

    if (!USB_Generic) {
        USB_Generic = (ttc_usb_generic_t*) ttc_heap_alloc_zeroed( sizeof(ttc_usb_generic_t) );
        A(USBConfigs, LogicalIndex-1) = USB_Generic;

        ttc_usb_load_defaults(LogicalIndex);
    }
    return USB_Generic;
}
    ttc_usb_errorcode_e ttc_usb_load_defaults(u8_t LogicalIndex) {
    if ( (LogicalIndex > TTC_AMOUNT_USBS) || (LogicalIndex < 1) )
        return ec_DeviceNotFound;

    ttc_usb_generic_t* USBGeneric = ttc_usb_get_configuration(LogicalIndex);
    memset(USBGeneric, 0, sizeof(ttc_usb_generic_t));
    ttc_usb_errorcode_e Error = tusbe_NotImplemented;

    USBGeneric->LogicalIndex = LogicalIndex;
    return Error;
}
                   u8_t ttc_usb_get_max_index() {

    return TTC_AMOUNT_USBS; // -> ttc_usb_types.h
}
ttc_channel_errorcode_e ttc_usb_reset(u8_t LogicalIndex) {
    // ttc_usb_generic_t* USB_Generic = _ttc_usb_get_configuration(LogicalIndex);

    Assert_USB(FALSE, ec_NotImplemented);
    return 0;
}
ttc_channel_errorcode_e ttc_usb_get_features(u8_t LogicalIndex, ttc_usb_generic_t* USB_Generic) {
    // Assert_USB(LogicalIndex > 0, ec_InvalidArgument); // logical USB-indices start at 1 !
    // Assert_USB(USB_Generic != NULL, ec_InvalidArgument);


    return tusbe_NotImplemented;
}
    ttc_usb_errorcode_e ttc_usb_init(u8_t LogicalIndex) {

    ttc_usb_generic_t* USB_Generic = ttc_usb_get_configuration(LogicalIndex);
    Assert_USB(USB_Generic, ec_InvalidArgument);
    Assert_USB(USB_Generic->LogicalIndex > 0, ec_InvalidArgument);
    Assert_USB(USB_Generic->LogicalIndex <= ttc_usb_get_max_index(), ec_InvalidArgument);

    if (LogicalIndex > ttc_usb_get_max_index() )
        return tusbe_DeviceNotFound;

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_usb_init(USB_Generic);
#endif

    return tusbe_NotImplemented;
}
                   void ttc_usb_register_rx_function(u8_t LogicalIndex, void (*RxFunction)(u8_t* Data_Buffer, u8_t Amount)) {
    ttc_usb_generic_t* USB_Generic = ttc_usb_get_configuration(LogicalIndex);

    USB_Generic->RxFunction_UCB = RxFunction;
    Assert_USB(USB_Generic->RxFunction_UCB, ec_NULL);

}
                   void ttc_usb_register_tx_UCB_function(u8_t LogicalIndex, void (*TxFunction)(u8_t* Data_Buffer, u8_t Amount)) {
    ttc_usb_generic_t* USB_Generic = ttc_usb_get_configuration(LogicalIndex);

    USB_Generic->TxFunction_UCB = TxFunction;
    Assert_USB(USB_Generic->TxFunction_UCB, ec_NULL);
}
                   void _ttc_usb_receive_buffer(u8_t LogicalIndex, u8_t * Buffer, u8_t Amount){

    ttc_usb_generic_t* USB_Generic = ttc_usb_get_configuration(LogicalIndex);
    if(USB_Generic->RxFunction_UCB != NULL)
        USB_Generic->RxFunction_UCB((u8_t *) Buffer, (u8_t) Amount);

}
//} private functions
ttc_channel_errorcode_e ttc_usb_send_raw(u8_t LogicalIndex, u8_t* Buffer, Base_t Amount) {
    ttc_usb_generic_t* USB_Generic = ttc_usb_get_configuration(LogicalIndex);

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    stm32_usb_send_raw(USB_Generic, Buffer, Amount);
#else
    Assert_USB(FALSE, ec_NotImplemented);
#endif
    return tce_OK;
}
