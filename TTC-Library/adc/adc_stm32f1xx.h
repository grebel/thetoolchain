#ifndef ADC_STM32F1XX_H
#define ADC_STM32F1XX_H

/** { adc_stm32f1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for adc devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by high-level adc and application.
 *
 *  Created from template device_architecture.h revision 22 at 20141008 14:25:12 UTC
 *
 *  Note: See ttc_adc.h for description of stm32f1xx independent ADC implementation.
 *
 *  Authors: Gregor Rebel, Victor Fuentes
 *
 *
 *  How it works
 *
 *  In STM32f1xx architecture, ADCs provide two lists of channels to convert:
 *
 *  1) Regular Group (1..16 entries)
 *     This driver uses the regular group for automatic, constant conversion.
 *     The conversion results are directly copied to given memory location via DMA.
 *
 *  2) Injected Group (1..4 entries)
 *     This driver uses only the first entry of this group to provide manaual conversion
 *     requests via ttc_driver_adc_get_value(). The group configuration is changed for
 *     the configured input channel, a conversion is started and the converted result is returned.
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_ADC_STM32F1XX
//
// Implementation status of low-level driver support for adc devices on stm32f1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_ADC_STM32F1XX 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_ADC_STM32F1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_ADC_STM32F1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_ADC_STM32F1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "adc_stm32f1xx.c"
//
#include "adc_stm32f1xx_types.h"
#include "../ttc_adc_types.h"
#include "../ttc_gpio.h"
#include "../ttc_task.h"

#define USE_STDPERIPH_DRIVER
#include "stm32f10x.h"
#include "core_cm3.h"
#include "stm32f10x_conf.h"

// ADC
#include "stm32f10x_adc.h"
#include "stm32f10x_dma.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_adc_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_adc_foo
//
#define ttc_driver_adc_deinit(Config) adc_stm32f1xx_deinit(Config)
#define ttc_driver_adc_get_features(Config) adc_stm32f1xx_get_features(Config)
#define ttc_driver_adc_init(Config) adc_stm32f1xx_init(Config)
#define ttc_driver_adc_load_defaults(Config) adc_stm32f1xx_load_defaults(Config)
#define ttc_driver_adc_prepare() adc_stm32f1xx_prepare()
#define ttc_driver_adc_reset(Config) adc_stm32f1xx_reset(Config)
#define ttc_driver_adc_init_single(Config) adc_stm32f1xx_init_single(Config)
#define ttc_driver_adc_init_dma(Config, Value) adc_stm32f1xx_init_dma(Config, Value)
#define ttc_driver_adc_get_value(Config) adc_stm32f1xx_get_value(Config)
#define ttc_driver_adc_find_input_pin(Config) adc_stm32f1xx_find_input_pin(Config)
#define ttc_driver_adc_get_maximum(LogicalIndex) adc_stm32f1xx_get_maximum(LogicalIndex)
#define ttc_driver_adc_get_minimum(LogicalIndex) adc_stm32f1xx_get_minimum(LogicalIndex)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes

/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_adc.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_adc.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

/** shutdown single ADC unit device
 * @param Config        pointer to struct t_ttc_adc_config
 * @return              == 0: ADC has been shutdown successfully; != 0: error-code
 */
e_ttc_adc_errorcode adc_stm32f1xx_deinit( t_ttc_adc_config* Config );

/** fills out given Config with maximum valid values for indexed ADC
 * @param Config        = pointer to struct t_ttc_adc_config
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_adc_errorcode adc_stm32f1xx_get_features( t_ttc_adc_config* Config );

/** initializes single ADC unit for operation
 * @param Config        pointer to struct t_ttc_adc_config
 * @return              == 0: ADC has been initialized successfully; != 0: error-code
 */
e_ttc_adc_errorcode adc_stm32f1xx_init( t_ttc_adc_config* Config );

/** loads configuration of indexed ADC unit with default values
 * @param Config        pointer to struct t_ttc_adc_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_adc_errorcode adc_stm32f1xx_load_defaults( t_ttc_adc_config* Config );

/** Prepares adc Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void adc_stm32f1xx_prepare();

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_adc_config
 * @return   =
 */
e_ttc_adc_errorcode adc_stm32f1xx_reset( t_ttc_adc_config* Config );

/** Initilize single ADC pin in single mode
 *
 * @param Config        pointer to struct t_ttc_adc_config
 */
e_ttc_adc_errorcode adc_stm32f1xx_init_single( t_ttc_adc_config* Config );

/** Get the value of a ADC channel in single mode
 *
 * @param Config        pointer to struct t_ttc_adc_config
 * @return              Value
 */
t_u16 adc_stm32f1xx_get_value( t_ttc_adc_config* Config );

/** Initialize single ADC channel with/without DMA transfer
 *
 * @param Config        pointer to struct t_ttc_adc_config
 * @param Value         Use DMA to copy sampled value into variable
 */
e_ttc_adc_errorcode adc_stm32f1xx_init_dma( t_ttc_adc_config* Config, volatile t_u16* Value );

/** updates Config->LowLevelConfig for a new value of Config->InputPin
 * @param Config        pointer to struct t_ttc_adc_config
 * @return              == 0: Config has been updated successfully; != 0: Config->InputPin cannot be used as analog input
 */
e_ttc_adc_errorcode adc_stm32f1xx_find_input_pin( t_ttc_adc_config* Config );


/** Get the maximum digital value for current architecture
 *
 * @param LogicalIndex    device index of device to init (1..ttc_ADC_get_max_LogicalIndex())
 * @return                Biggest return value of ttc_adc_get_value()
 */
static inline t_base_signed adc_stm32f1xx_get_maximum( t_u8 LogicalIndex ) { ( void ) LogicalIndex; return 4095; }


/** Get the minimum digital value for current architecture
 *
 * @param LogicalIndex    device index of device to init (1..ttc_ADC_get_max_LogicalIndex())
 * @return                Smallest return value of ttc_adc_get_value() (can be negative!)
 */
static inline t_base_signed adc_stm32f1xx_get_minimum( t_u8 LogicalIndex ) { ( void ) LogicalIndex; return 0; }


/** Get the maximum digital value for current architecture
 *
 * @param LogicalIndex    device index of device to init (1..ttc_ADC_get_max_LogicalIndex())
 * @return                Biggest return value of ttc_adc_get_value()
 */
t_base_signed adc_stm32f1xx_get_maximum( t_u8 LogicalIndex );


/** Get the minimum digital value for current architecture
 *
 * @param LogicalIndex    device index of device to init (1..ttc_ADC_get_max_LogicalIndex())
 * @return                Smallest return value of ttc_adc_get_value() (can be negative!)
 */
t_base_signed adc_stm32f1xx_get_minimum( t_u8 LogicalIndex );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

//}PrivateFunctions

#endif //ADC_STM32F1XX_H