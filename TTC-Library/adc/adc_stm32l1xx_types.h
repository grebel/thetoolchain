#ifndef ADC_STM32L1XX_TYPES_H
#define ADC_STM32L1XX_TYPES_H

/** { adc_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for ADC devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_adc_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20141014 12:37:34 UTC
 *
 *  Note: See ttc_adc.h for description of architecture independent ADC implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

#define TTC_ADC_MAX_AMOUNT 5

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "../register/register_stm32l1xx_types.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_adc_types.h *************************

/*???
typedef struct  // stm32l1xx ADC Init structure definition
{
    t_u32 adc_Resolution;
    t_u8 adc_ScanConvMode;                // Scan(1) or single(0) mode
    t_u8 adc_ContinuousConvMode;          // Continuous(1) or Single(0) mode.
    t_u32 adc_ExternalTrigConv;          // Defines the external trigger.
    t_u32 adc_ExternalTrigConvEdge;          // Defines the external trigger.
    t_u32 adc_DataAlign;                 // ADC data alignment left or right
    t_u32 adc_NbrOfConversion;               // Number of channel that will be converted
} adc_init_structure;
*/

typedef struct {  // stm32l1xx specific configuration data of single ADC device
    volatile t_register_stm32l1xx_adc* BaseRegister;
} __attribute__( ( __packed__ ) ) t_adc_stm32l1xx_config;

// t_ttc_adc_architecture is required by ttc_adc_types.h
#define t_ttc_adc_architecture t_adc_stm32l1xx_config

//} Structures/ Enums


#endif //ADC_STM32L1XX_TYPES_H
