#ifndef ADC_STM32F1XX_TYPES_H
#define ADC_STM32F1XX_TYPES_H

/** { adc_stm32f1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for ADC devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by ttc_adc_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20141008 14:25:12 UTC
 *
 *  Note: See ttc_adc.h for description of architecture independent ADC implementation.
 *
 *  Authors: Gregor Rebel, Victor Fuentes
 *
}*/
//{ Defines/ TypeDefs **********************************************************

// maximum allowed rank for regular and injected channel groups
#define ADC_STM32F1XX_MAX_RANK_REGULAR  16
#define ADC_STM32F1XX_MAX_RANK_INJECTED  4

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//#include "stm32f10x.h"
#include "../register/register_stm32f1xx_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_adc_types.h *************************


typedef struct { // stm32f1xx ADC Init structure definition
    t_u32 adc_mode;                      // Configures the ADC to operate in independent or dual mode.
    bool adc_ScanSingleMode;                // Scan(1) or single(0) mode
    bool adc_ContinuousSingleMode;          // Continuous(1) or Single(0) mode.
    t_u32 adc_ExternalTrigConv;          // Defines the external trigger.
    t_u32 adc_DataAlign;                 // ADC data alignment left or right
    t_u32 adc_numberofChannel;               // Number of channel that will be converted */
} adc_init_estructure;

typedef struct { // register description (adapt according to stm32f1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_adc_register;

typedef struct s_adc_stm32f1xx_device { // status data of a single ADC device
    volatile t_register_stm32f1xx_adc* BaseRegister; // base address of ADC device registers (more precise definition)
    BOOL PoweredUp;           // == true: ADC has been powered up
    t_u8 AmountAssigned;      // amount of analog inputs assigned to this ADC device so far
    t_u8 AmountRanksRegular;  // amount of configured regular ranks
    t_u8 AmountRanksInjected; // amount of configured injected ranks
} t_adc_stm32f1xx_device;

typedef struct {  // stm32f1xx specific configuration data of single ADC device
    t_adc_stm32f1xx_device* Device;    // configuration of assigned physical ADC device (STM32F1xx provides 3 individual ADC devices)
    t_u8 Channel;                      // >0: number + 1 of ADC channel used; ==0: channel not yet determined
    t_u8 Rank_Regular;                 // >0: rank + 1 in which a regular conversion should take place
    t_u8 Rank_Injected;                // >0: rank + 1 in which an injected conversion should take place
    t_u8 Cycles;                       // amount of clock cycles to use for each conversion
} __attribute__( ( __packed__ ) ) t_adc_stm32f1xx_config;

// t_ttc_adc_architecture is required by ttc_adc_types.h
#define t_ttc_adc_architecture t_adc_stm32f1xx_config

//InsertEnums above (DO NOT REMOVE THIS LINE!)

#define e_ttc_gpio_alternate_function  e_gpio_stm32f1xx_alternate_function
#define TTC_ADC_MAX_AMOUNT 40

//} Structures/ Enums


#endif //ADC_STM32F1XX_TYPES_H
