#ifndef ADC_STM32L1XX_H
#define ADC_STM32L1XX_H

/** { adc_stm32l1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for adc devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by high-level adc and application.
 *
 *  Created from template device_architecture.h revision 22 at 20141014 12:37:34 UTC
 *
 *  Note: See ttc_adc.h for description of stm32l1xx independent ADC implementation.
 *
 *  Authors: Victor Fuentes, Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_ADC_STM32L1XX
//
// Implementation status of low-level driver support for adc devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_ADC_STM32L1XX 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_ADC_STM32L1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_ADC_STM32L1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_ADC_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "adc_stm32l1xx.c"
//
#include "adc_stm32l1xx_types.h"
#include "../ttc_adc_types.h"

//} Includes
//{ Macro definitions ****************************************************

#if 1==1 //DEPRECATED - ToDo: Use register_stm32l1xx_types.h instead!
    #define adc_Resolution_12b                         ((t_u32)0x00000000)
    #define adc_Resolution_10b                         ((t_u32)0x01000000)
    #define adc_Resolution_8b                          ((t_u32)0x02000000)
    #define adc_Resolution_6b                          ((t_u32)0x03000000)

    #define adc_ExternalTrigConv_T2_CC3                ((t_u32)0x02000000)
    #define adc_ExternalTrigConv_T2_CC2                ((t_u32)0x03000000)
    #define adc_ExternalTrigConv_T2_TRGO               ((t_u32)0x06000000)
    #define adc_ExternalTrigConv_T3_CC1                ((t_u32)0x07000000)
    #define adc_ExternalTrigConv_T3_CC3                ((t_u32)0x08000000)
    #define adc_ExternalTrigConv_T3_TRGO               ((t_u32)0x04000000)
    #define adc_ExternalTrigConv_T4_CC4                ((t_u32)0x05000000)
    #define adc_ExternalTrigConv_T4_TRGO               ((t_u32)0x09000000)
    #define adc_ExternalTrigConv_T6_TRGO               ((t_u32)0x0A000000)
    #define adc_ExternalTrigConv_T9_CC2                ((t_u32)0x00000000)
    #define adc_ExternalTrigConv_T9_TRGO               ((t_u32)0x01000000)
    #define adc_ExternalTrigConv_Ext_IT11              ((t_u32)0x0F000000)

    #define adc_ExternalTrigConvEdge_None              ((t_u32)0x00000000)
    #define adc_ExternalTrigConvEdge_Rising            ((t_u32)0x10000000)
    #define adc_ExternalTrigConvEdge_Falling           ((t_u32)0x20000000)
    #define adc_ExternalTrigConvEdge_RisingFalling     ((t_u32)0x30000000)

    #define adc_DataAlign_Right                        ((t_u32)0x00000000)
    #define adc_DataAlign_Left                         ((t_u32)0x00000800)

    #define adc1                                       ((t_register_stm32l1xx_adc*) 0x40012400)

    #define  rcc_AHBENR_GPIOA_EN                        ((t_u32)0x00000001)
    #define  rcc_APB2ENR_ADC1EN                        ((t_u32)0x00000200)

    #define adc_FLAG_EOC                               ((t_u16)0x0002)
    #define adc_FLAG_ADONS                             ((t_u16)0x0040)

    //typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;
    //#define IS_FUNCTIONAL_STATE(STATE) (((STATE) == DISABLE) || ((STATE) == ENABLE))

#endif //DEPRECATED

// define all supported low-level functions for ttc_adc_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_adc_foo
//
#define ttc_driver_adc_deinit(Config) adc_stm32l1xx_deinit(Config)
#define ttc_driver_adc_get_features(Config) adc_stm32l1xx_get_features(Config)
#define ttc_driver_adc_init(Config) adc_stm32l1xx_init(Config)
#define ttc_driver_adc_init_dma(Config, Value) adc_stm32l1xx_init_dma(Config, Value)
#define ttc_driver_adc_init_single(Config) adc_stm32l1xx_init_single(Config)
#define ttc_driver_adc_load_defaults(Config) adc_stm32l1xx_load_defaults(Config)
#define ttc_driver_adc_prepare() adc_stm32l1xx_prepare()
#define ttc_driver_adc_reset(Config) adc_stm32l1xx_reset(Config)
#define ttc_driver_adc_init_single(Config) adc_stm32l1xx_init_single(Config)
#define ttc_driver_adc_get_value(Config) adc_stm32l1xx_get_value(Config)
#define ttc_driver_adc_find_input_pin(Config) adc_stm32l1xx_find_input_pin(Config)
#define ttc_driver_adc_get_maximum(LogicalIndex) adc_stm32l1xx_get_maximum(LogicalIndex)
#define ttc_driver_adc_get_minimum(LogicalIndex) adc_stm32l1xx_get_minimum(LogicalIndex)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_adc.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_adc.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** shutdown single ADC unit device
 * @param Config        pointer to struct t_ttc_adc_config
 * @return              == 0: ADC has been shutdown successfully; != 0: error-code
 */
e_ttc_adc_errorcode adc_stm32l1xx_deinit( t_ttc_adc_config* Config );

/** fills out given Config with maximum valid values for indexed ADC
 * @param Config        = pointer to struct t_ttc_adc_config
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_adc_errorcode adc_stm32l1xx_get_features( t_ttc_adc_config* Config );

/** get the value of a ADC channel in single conversation mode - private function
 *
 * @param Channel         Channel that is going to be read
 * @param Config          pointer to struct t_ttc_adc_config
 * @return                == 0: adc device has been reset successfully; != 0: error-code
 */
t_u16 adc_stm32l1xx_get_value( t_ttc_adc_config* Config );

/** initialize a Channel ADC
 *
 * @param Config        pointer to struct t_ttc_adc_config
 * @return   =
 */
e_ttc_adc_errorcode adc_stm32l1xx_init( t_ttc_adc_config* Config );

/** initialize a Channel ADC in DMA mode
 *
 * @param Config        pointer to struct t_ttc_adc_config
 * @param Channel       Channel that is going to be read
 * @param Value         Variable in which the value is going to be stored
 * @return        =
 */
e_ttc_adc_errorcode adc_stm32l1xx_init_dma( t_ttc_adc_config* Config, volatile t_u16* Value );

/** loads configuration of indexed ADC unit with default values
 * @param Config        pointer to struct t_ttc_adc_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_adc_errorcode adc_stm32l1xx_load_defaults( t_ttc_adc_config* Config );

/** Prepares adc Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void adc_stm32l1xx_prepare();

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_adc_config
 * @return   =
 */
e_ttc_adc_errorcode adc_stm32l1xx_reset( t_ttc_adc_config* Config );

/** updates Config->LowLevelConfig for a new value of Config->InputPin
 * @param Config        pointer to struct t_ttc_adc_config
 * @return              == 0: Config has been updated successfully; != 0: Config->InputPin cannot be used as analog input
 */
e_ttc_adc_errorcode adc_stm32l1xx_find_input_pin( t_ttc_adc_config* Config );

/** Get the maximum digital value for current architecture
 *
 * @param LogicalIndex    device index of device to init (1..ttc_ADC_get_max_LogicalIndex())
 * @return                Biggest return value of ttc_adc_get_value()
 */
t_base_signed adc_stm32l1xx_get_maximum( t_u8 LogicalIndex );

/** Get the minimum digital value for current architecture
 *
 * @param LogicalIndex    device index of device to init (1..ttc_ADC_get_max_LogicalIndex())
 * @return                Smallest return value of ttc_adc_get_value() (can be negative!)
 */
t_base_signed adc_stm32l1xx_get_minimum( t_u8 LogicalIndex );

/**
 * @brief Find the correct channel, for this GPIO bank and this Pin
 * @param GPIOX GPIO Bank
 * @param Pin Pin selected
 * @return Channel
 */
t_u16 adc_stm32l1xx_find_channel( e_gpio_stm32l1xx_bank GPIOX, t_u8 Pin );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //ADC_STM32L1XX_H