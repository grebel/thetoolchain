/** { adc_stm32f1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for adc devices on stm32f1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20141008 14:25:12 UTC
 *
 *  Note: See ttc_adc.h for description of stm32f1xx ADC implementation.
 *
 *  Authors: Gregor Rebel, Victor Fuentes
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "adc_stm32f1xx.h".
//
#include "adc_stm32f1xx.h"
#include "stm32f10x_adc.h" // defines provided by ST Standard Peripheral Library
#include "../basic/basic_cm3.h"
#include "../register/register_stm32f1xx.h"
#include "../ttc_memory.h"
#include "../ttc_cpu.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

// status of all physical ADC devices
t_adc_stm32f1xx_device adc_stm32f1xx_Device1;
t_adc_stm32f1xx_device adc_stm32f1xx_Device2;
t_adc_stm32f1xx_device adc_stm32f1xx_Device3;

//}Global Variables
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _adc_stm32f1xx_foo(t_ttc_adc_config* Config)

/** Power single ADC device Up or Down
 *
 * @param BaseRegister  base register of ADC device to power up or down
 * @param PowerUp       !=0: power up ADC, otherwise power it down
 */
void _adc_stm32f1xx_power( t_ttc_adc_config* Config, BOOL PowerUp );

/** Configure given analog input as regular ADC channel for automatic conversion
 *
 * @param Config        pointer to struct t_ttc_adc_config describing single analog input
 * @param Install       ==true: install analog input; remove it otherwise
 * @param Replace       ==true: install in same rank as before (replaces existing channel)
 */
void _adc_stm32f1xx_configure_regular_channel( t_ttc_adc_config* Config, BOOL Install, BOOL Replace );

/** Configure given analog input as injected ADC channel for manual conversion
 *
 * How it works
 *
 * The STM32f1xx provides a sequence of 1..4 injected channels. Starting conversion of this group
 * will interrupt conversion of regular group. The result is written into JDR1..JDR4.
 * JDR1 always gets first value regardless which rank was the first to be converted.
 * Strangely, if sequence length is less than 4, conversion will start at rank 5-Length.
 * Compare with RM0008 p.241
 * Analog input channel for given config can be installed to or removed from the sequence.
 * 1) Install Input Channel
 *    a) Replacing existing rank
 *       The channel will be installed in rank Config->Rank.
 *    b) Installing in new rank
 *       A new free rank will be used (up to 4 available).
 * 2) Remove Input Channel
 *    The channel will be removed and its rank is being freed.
 *
 * @param Config        pointer to struct t_ttc_adc_config describing single analog input
 * @param Install       ==true: install analog input; remove it otherwise
 * @param Replace       ==true: install in same rank as before (replaces existing channel)
 */
void _adc_stm32f1xx_configure_injected_channel( t_ttc_adc_config* Config, BOOL Install, BOOL Replace );

//}InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)
//{ Function Definitions *******************************************************

void adc_stm32f1xx_prepare() {

    // reset status data of all ADC devices
    ttc_memory_set( & adc_stm32f1xx_Device1, 0, sizeof( adc_stm32f1xx_Device1 ) );
    ttc_memory_set( & adc_stm32f1xx_Device2, 0, sizeof( adc_stm32f1xx_Device2 ) );
    ttc_memory_set( & adc_stm32f1xx_Device3, 0, sizeof( adc_stm32f1xx_Device3 ) );

    // reset peripheral registers of all ADC devices
    ttc_memory_set( ( void* ) &register_stm32f1xx_ADC1, 0, sizeof( register_stm32f1xx_ADC1 ) );
    ttc_memory_set( ( void* ) &register_stm32f1xx_ADC2, 0, sizeof( register_stm32f1xx_ADC2 ) );
    ttc_memory_set( ( void* ) &register_stm32f1xx_ADC3, 0, sizeof( register_stm32f1xx_ADC3 ) );
}
e_ttc_adc_errorcode adc_stm32f1xx_deinit( t_ttc_adc_config* Config ) {
    Assert_ADC( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // pointers must not be NULL

    if ( Config->InputPin ) { // reset configuration of GPIO pin
        ttc_gpio_init( Config->InputPin, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
    }
    if ( Config->Flags.Bits.Initialized ) {
        if ( Config->LowLevelConfig.Rank_Regular ) { // remove from regular group of channels
            _adc_stm32f1xx_configure_regular_channel( Config, FALSE, FALSE );
            Config->LowLevelConfig.Rank_Regular = 0;
        }
        if ( Config->LowLevelConfig.Rank_Injected ) { // remove from injected group of channels
            _adc_stm32f1xx_configure_injected_channel( Config, FALSE, FALSE );
            Config->LowLevelConfig.Rank_Injected = 0;
        }
    }
    Config->Flags.Bits.Initialized = 0;

    return ( e_ttc_adc_errorcode ) 0;
}
e_ttc_adc_errorcode adc_stm32f1xx_get_features( t_ttc_adc_config* Config ) {
    Assert_ADC( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // pointers must not be NULL

    Config->Flags.Bits.UseDMA = 0; // Currently not supported

    return ( e_ttc_adc_errorcode ) 0;
}
e_ttc_adc_errorcode adc_stm32f1xx_init( t_ttc_adc_config* Config ) {
    Assert_ADC( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // pointers must be valid
    volatile t_register_stm32f1xx_adc* BaseRegister = Config->LowLevelConfig.Device->BaseRegister;

    Assert_ADC( IS_ADC_SAMPLE_TIME( Config->LowLevelConfig.Cycles ), ttc_assert_origin_auto );
    _adc_stm32f1xx_power( Config, 1 ); // power up ADC

    t_u8 Channel = Config->LowLevelConfig.Channel;
    if ( Channel > 9 ) { // if ADC_Channel_10 ... ADC_Channel_17 is selected
        t_u32 Content = *( ( t_u32* ) & ( BaseRegister->SMPR1 ) );
        t_u8 Shift = 3 * ( Channel - 10 );
        t_u32 Mask = 0b111 << Shift;

        Content &= ~Mask;
        Content |= ( Config->LowLevelConfig.Cycles & 0b111 ) << Shift;

        *( ( t_u32* ) & ( BaseRegister->SMPR1 ) ) = Content;
    }
    else {             // ADC_Channel include in ADC_Channel_[0..9]

        t_u32 Content = *( ( t_u32* ) & ( BaseRegister->SMPR1 ) );
        t_u8 Shift = 3 * Channel;
        t_u32 Mask = 0b111 << Shift;

        Content &= ~Mask;
        Content |= ( Config->LowLevelConfig.Cycles & 0b111 ) << Shift;

        *( ( t_u32* ) & ( BaseRegister->SMPR1 ) ) = Content;
    }

    Config->Architecture = ta_adc_stm32f1xx;

    return ( e_ttc_adc_errorcode ) 0;
}
e_ttc_adc_errorcode adc_stm32f1xx_load_defaults( t_ttc_adc_config* Config ) {
    Assert_ADC( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // pointers must not be NULL

    Config->Flags.Bits.UseDMA = 0;    // ToDo: fix dma mode
    //X Config->MaxValue = (1 << 12) - 1; // our ADC provides 12 bit accuracy
    Config->LowLevelConfig.Cycles = ADC_SampleTime_28Cycles5; // ToDo: Find best setting

    return ( e_ttc_adc_errorcode ) 0;
}
e_ttc_adc_errorcode adc_stm32f1xx_reset( t_ttc_adc_config* Config ) {
    Assert_ADC( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // pointers must not be NULL

    adc_stm32f1xx_load_defaults( Config );
    ttc_memory_set( &( Config->LowLevelConfig ), 0, sizeof( Config->LowLevelConfig ) );

    return ( e_ttc_adc_errorcode ) 0;
}
t_u16 adc_stm32f1xx_get_value( t_ttc_adc_config* Config ) {
    Assert_ADC( ttc_memory_is_writable( Config ), ttc_assert_origin_auto );

    volatile t_register_stm32f1xx_adc* BaseRegister = Config->LowLevelConfig.Device->BaseRegister;

    // 1Cycles5(0), 7Cycles5(1), 13Cycles5(2), 28Cycles5(3), 41Cycles5(4), 55Cycles5(5), 71Cycles5(6), 239Cycles5(7)
    Config->LowLevelConfig.Rank_Injected = 1;      // forcing first rank
    _adc_stm32f1xx_configure_injected_channel( Config, TRUE, TRUE );            // configure injected group for single channel

    cm3_set_peripheral_bit( &BaseRegister->CR1, BITNUMBER_ADC_CR1_DISC_EN,  0 );
    cm3_set_peripheral_bit( &BaseRegister->CR1, BITNUMBER_ADC_CR1_JDISC_EN, 1 );
    cm3_set_peripheral_bit( &BaseRegister->CR1, BITNUMBER_ADC_CR1_JAUTO,   0 );
    cm3_set_peripheral_bit( &BaseRegister->CR2, BITNUMBER_ADC_CR2_JEXTTRIG, 1 );

    t_register_stm32f1xx_adc_cr2 CR2; CR2.All = BaseRegister->CR2.All;
    CR2.Bits.JEXTSEL             = 0b111;                                       // enable trigger JSWSTART for injected group
    *( ( t_u32* ) &BaseRegister->CR2 ) = *( ( t_u32* ) &CR2 );                  // update register (32-bit access)

    BaseRegister->SR.All = 0;                                                   // clear status register
    cm3_set_peripheral_bit( &BaseRegister->CR2, BITNUMBER_ADC_CR2_JSWSTART, 1 ); // start conversion of injected channel group
    while ( !cm3_get_peripheral_bit( &BaseRegister->SR, BITNUMBER_ADC_SR_JSTRT ) ); // wait until conversion is started

    Assert_ADC( cm3_get_peripheral_bit( &BaseRegister->SR, BITNUMBER_ADC_SR_JSTRT ), ttc_assert_origin_auto ); // injected group conversion did not start! (Something is misconfigured)
    while ( ! cm3_get_peripheral_bit( &BaseRegister->SR, BITNUMBER_ADC_SR_JEOC ) )
    { ttc_task_yield(); } // wait for end of conversion
    t_u16 Value = *( ( t_u16* ) &BaseRegister->JDR1 ); // read converted value

    return Value;
}
e_ttc_adc_errorcode adc_stm32f1xx_init_dma( t_ttc_adc_config* Config, volatile t_u16* Value ) {
    ttc_assert_halt_origin( ttc_assert_origin_auto );
    TODO( "implement adc_stm32f1xx_init_dma()" );

    /* DEPRECATED
    Config->Flags.Bits.UseDMA = 1;
    adc_stm32f1xx_init(Config);
    ADC_InitTypeDef   ADC_InitStructure;
    //    DMA_InitTypeDef   DMA_InitStructure;

    if (Config->Flags.Bits.UseDMA) { // DMA channel1 configuration
    //        DMA_DeInit(DMA1_Channel1);
    //        DMA_StructInit(&DMA_InitStructure);

    //        DMA_InitStructure.DMA_PeripheralBaseAddr=(t_u32)&Config->LowLevelConfig.Device->BaseRegister->DR; //?ADC1_DR_Address;
    //        DMA_InitStructure.DMA_MemoryBaseAddr    =(t_u32) Value;
    //        DMA_InitStructure.DMA_DIR               =DMA_DIR_PeripheralSRC;
    //        DMA_InitStructure.DMA_BufferSize        =1;
    //        DMA_InitStructure.DMA_PeripheralInc     =DMA_PeripheralInc_Disable;
    //        DMA_InitStructure.DMA_MemoryInc         =DMA_MemoryInc_Disable;
    //        DMA_InitStructure.DMA_PeripheralDataSize=DMA_PeripheralDataSize_HalfWord;
    //        DMA_InitStructure.DMA_MemoryDataSize    =DMA_MemoryDataSize_HalfWord;
    //        DMA_InitStructure.DMA_Mode              =DMA_Mode_Circular;
    //        DMA_InitStructure.DMA_Priority          =DMA_Priority_High;
    //        DMA_InitStructure.DMA_M2M               =DMA_M2M_Disable;
    //        DMA_Init(DMA1_Channel1, &DMA_InitStructure);

    //        // Enable DMA1 channel1
    //        DMA_Cmd(DMA1_Channel1, ENABLE);
    }

    // ADCx configuration
    ADC_InitStructure.ADC_Mode              =ADC_Mode_Independent;
    ADC_InitStructure.ADC_ScanConvMode      =ENABLE;
    ADC_InitStructure.ADC_ContinuousConvMode=ENABLE;
    ADC_InitStructure.ADC_ExternalTrigConv  =ADC_ExternalTrigConv_None;
    ADC_InitStructure.ADC_DataAlign         =ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfChannel      =1;
    ADC_Init((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister, &ADC_InitStructure);
    ADC_RegularChannelConfig((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister, Config->LowLevelConfig.Channel, 1, ADC_SampleTime_28Cycles5);

    if (Config->Flags.Bits.UseDMA) ADC_DMACmd((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister, ENABLE);         // Enable ADCx DMA
    ADC_Cmd((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister, ENABLE);                       // Enable ADCx
    ADC_SoftwareStartConvCmd((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister, ENABLE);
    */
    return ( e_ttc_adc_errorcode ) 0;
}
e_ttc_adc_errorcode adc_stm32f1xx_find_input_pin( t_ttc_adc_config* Config ) {
    Assert_ADC( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // pointers must not be NULL

    volatile t_register_stm32f1xx_adc* BaseRegister = NULL;
    t_u8 Channel = 0;
    t_adc_stm32f1xx_device* Device = NULL;

    t_u8 ADCs = 0; // filled with bitmask (Bit 0 == 1 <=> channel connected to ADC1, ...)

    switch ( Config->InputPin ) { // find channel and connected ADC devices
#if (TTC_CPU_VARIANT > TTC_CPU_VARIANT_stm32f103_start) && (TTC_CPU_VARIANT < TTC_CPU_VARIANT_stm32f103_end) // -> STM32F103x-Datasheet.pdf p.31
        case E_ttc_gpio_pin_a0:       ADCs = 0b111; Channel =  0; break; //ADC321
        case E_ttc_gpio_pin_a1:       ADCs = 0b111; Channel =  1; break; //ADC321
        case E_ttc_gpio_pin_a2:       ADCs = 0b111; Channel =  2; break; //ADC321
        case E_ttc_gpio_pin_a3:       ADCs = 0b111; Channel =  3; break; //ADC321
        case E_ttc_gpio_pin_a4:       ADCs = 0b011; Channel =  4; break; //ADC21
        case E_ttc_gpio_pin_a5:       ADCs = 0b011; Channel =  5; break; //ADC21
        case E_ttc_gpio_pin_a6:       ADCs = 0b011; Channel =  6; break; //ADC21
        case E_ttc_gpio_pin_a7:       ADCs = 0b011; Channel =  7; break; //ADC21
        case E_ttc_gpio_pin_b0:       ADCs = 0b011; Channel =  8; break; //ADC21
        case E_ttc_gpio_pin_b1:       ADCs = 0b011; Channel =  9; break; //ADC21
        case E_ttc_gpio_pin_c0:       ADCs = 0b111; Channel = 10; break; //ADC321
        case E_ttc_gpio_pin_c1:       ADCs = 0b111; Channel = 11; break; //ADC321
        case E_ttc_gpio_pin_c2:       ADCs = 0b111; Channel = 12; break; //ADC321
        case E_ttc_gpio_pin_c3:       ADCs = 0b111; Channel = 13; break; //ADC321
        case E_ttc_gpio_pin_c4:       ADCs = 0b011; Channel = 14; break; //ADC21
        case E_ttc_gpio_pin_c5:       ADCs = 0b011; Channel = 15; break; //ADC21
        case E_ttc_gpio_pin_f6:       ADCs = 0b100; Channel =  4; break; //ADC3
        case E_ttc_gpio_pin_f7:       ADCs = 0b100; Channel =  5; break; //ADC3
        case E_ttc_gpio_pin_f8:       ADCs = 0b100; Channel =  6; break; //ADC3
        case E_ttc_gpio_pin_f9:       ADCs = 0b100; Channel =  7; break; //ADC3
        case E_ttc_gpio_pin_f10:      ADCs = 0b100; Channel =  8; break; //ADC3
        case E_ttc_gpio_pin_adc_temp: ADCs = 0b001; Channel = 16; break; //ADC1
        case E_ttc_gpio_pin_adc_vref: ADCs = 0b001; Channel = 17; break; //ADC1
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); // Cannot find analog input for this pin. Check your configuration!
    }

    //BaseRegister = (t_register_stm32f1xx_adc*) ADC1; Channel =  0; Device = &adc_stm32f1xx_Device1;
    do { // find best physical ADC device (maximum 2 iterations)
        switch ( ADCs ) {
            case 0b001: BaseRegister = ( t_register_stm32f1xx_adc* ) ADC1; Device = &adc_stm32f1xx_Device1; adc_stm32f1xx_Device1.AmountAssigned++; break;
            case 0b010: BaseRegister = ( t_register_stm32f1xx_adc* ) ADC2; Device = &adc_stm32f1xx_Device2; adc_stm32f1xx_Device2.AmountAssigned++; break;
            case 0b100: BaseRegister = ( t_register_stm32f1xx_adc* ) ADC3; Device = &adc_stm32f1xx_Device3; adc_stm32f1xx_Device3.AmountAssigned++; break;

            // cases below will cause another while-iteration
            case 0b011: { // decide between ADC1 and ADC2
                if ( Config->Flags.Bits.UseDMA ) { ADCs = 0b001; } // ADC2 does not provide DMA (->RM0008 p.218)
                else {
                    if ( adc_stm32f1xx_Device1.AmountAssigned < adc_stm32f1xx_Device2.AmountAssigned )
                    { ADCs = 0b001; } // using ADC1 because it has less inputs assigned so far
                    else
                    { ADCs = 0b010; } // using ADC2 because it has not more inputs assigned so far
                }
                break;
            }
            case 0b111: { // decide between ADC1, ADC2 and ADC3
                if ( Config->Flags.Bits.UseDMA ) { // Only ADC1 and ADC3 provide DMA (->RM0008 p.218)
                    if ( adc_stm32f1xx_Device1.AmountAssigned < adc_stm32f1xx_Device3.AmountAssigned )
                    { ADCs = 0b001; } // #1 < #3
                    else
                    { ADCs = 0b100; } // #3 <= #1
                }
                else { // free choice: assign ADC with least assignements so far
                    if ( adc_stm32f1xx_Device1.AmountAssigned < adc_stm32f1xx_Device2.AmountAssigned ) {
                        if ( adc_stm32f1xx_Device1.AmountAssigned < adc_stm32f1xx_Device3.AmountAssigned )
                        { ADCs = 0b001; } // #1 < #2, #1 < #3
                        else
                        { ADCs = 0b100; } // #1 < #2, #3 <= #1 ???
                    }
                    else { // #2 <= #1
                        if ( adc_stm32f1xx_Device2.AmountAssigned < adc_stm32f1xx_Device3.AmountAssigned )
                        { ADCs = 0b010; } // #2 <= #1, #2 < #3
                        else
                        { ADCs = 0b100; } // #2 <= #1, #3 <= #2
                    }
                }
                break;
            }

            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // cannot find physical device!
        }
    }
    while ( !BaseRegister );

    Config->LowLevelConfig.Channel = Channel;
    Config->LowLevelConfig.Device  = Device;
    Device->BaseRegister           = BaseRegister;

    return ( e_ttc_adc_errorcode ) 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

void _adc_stm32f1xx_power( t_ttc_adc_config* Config, BOOL PowerUp ) {
    Assert_ADC( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // pointers must not be NULL

    t_ttc_adc_architecture* LowLevelConfig = &Config->LowLevelConfig;
    volatile t_register_stm32f1xx_adc* BaseRegister = LowLevelConfig->Device->BaseRegister;

    static t_u8 _adc_stm32f1xx_AmountPoweredUp = 0;
    if ( PowerUp ) { // power up ADC
        if ( !LowLevelConfig->Device->PoweredUp ) {

            if ( 0 ) { // enable individual ADC (untested)
                switch ( ( t_base ) BaseRegister ) { // switch on clocks for ADC
                    case ( t_base ) ADC1: register_stm32f1xx_RCC.APB2ENR.ADC1_EN = 1; break;
                    case ( t_base ) ADC2: register_stm32f1xx_RCC.APB2ENR.ADC2_EN = 1; break;
                    case ( t_base ) ADC3: register_stm32f1xx_RCC.APB2ENR.ADC3_EN = 1; break;
                    default: ttc_assert_halt_origin( ttc_assert_origin_auto ); // BaseRegister points to invalid location: Check your implementation!
                }

                cm3_set_peripheral_bit( &BaseRegister->CR2, BITNUMBER_ADC_CR2_ADON, 0 ); // switch off ADC to be sure its really powered down
                ttc_task_usleep( 50 );
                cm3_set_peripheral_bit( &BaseRegister->CR2, BITNUMBER_ADC_CR2_ADON, 1 ); // switch back on ADC
                ttc_task_usleep( 50 );
                Assert_ADC( BaseRegister->CR2.Bits.ADON, ttc_assert_origin_auto );  // ADC did not switch on! Maybe clocks are misconfigured?

                BaseRegister->CR2.Bits.RSTCAL = 1;     // reset calibration data
                while ( BaseRegister->CR2.Bits.RSTCAL ); // wait for reset to finish

                // calibrate ADC (-> RM0008 p.214)
                BaseRegister->CR2.Bits.CAL = 1;        // start calibration process
                while ( BaseRegister->CR2.Bits.CAL )   // wait for end of calibration
                { ttc_task_usleep( 10 ); }
            }
            else {   // enable all ADCs at once
                if ( _adc_stm32f1xx_AmountPoweredUp == 0 ) {
                    if ( 1 ) { // enable ADC clocks
                        register_stm32f1xx_RCC.APB2ENR.ADC1_EN = 1;
                        register_stm32f1xx_RCC.APB2ENR.ADC2_EN = 1;
                        register_stm32f1xx_RCC.APB2ENR.ADC3_EN = 1;
                    }
                    if ( 1 ) { // reset ADC interfaces
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC1.CR2, BITNUMBER_ADC_CR2_ADON, 0 );
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC2.CR2, BITNUMBER_ADC_CR2_ADON, 0 );
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC3.CR2, BITNUMBER_ADC_CR2_ADON, 0 );

                        // activate register reset
                        register_stm32f1xx_RCC.APB2RSTR.ADC1RST = 1;
                        register_stm32f1xx_RCC.APB2RSTR.ADC2RST = 1;
                        register_stm32f1xx_RCC.APB2RSTR.ADC3RST = 1;

                        // take back reset
                        register_stm32f1xx_RCC.APB2RSTR.ADC1RST = 0;
                        register_stm32f1xx_RCC.APB2RSTR.ADC2RST = 0;
                        register_stm32f1xx_RCC.APB2RSTR.ADC3RST = 0;
                    }
                    if ( 1 ) { // switch on ADC devices
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC1.CR2, BITNUMBER_ADC_CR2_ADON, 1 );
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC2.CR2, BITNUMBER_ADC_CR2_ADON, 1 );
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC3.CR2, BITNUMBER_ADC_CR2_ADON, 1 );
                        ttc_task_usleep( 50 );
                    }
                    if ( 1 ) { // start conversion on all interfaces (setting ADON 2nd time)
                        // enable discontinous mode
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC1.CR1, BITNUMBER_ADC_CR1_DISC_EN,  1 );
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC2.CR1, BITNUMBER_ADC_CR1_DISC_EN,  1 );
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC2.CR1, BITNUMBER_ADC_CR1_DISC_EN,  1 );

                        // start conversions
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC1.CR2, BITNUMBER_ADC_CR2_ADON, 1 );
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC2.CR2, BITNUMBER_ADC_CR2_ADON, 1 );
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC3.CR2, BITNUMBER_ADC_CR2_ADON, 1 );
                        ttc_task_usleep( 50 );

                        // check if all ADCs have powered up
                        Assert_ADC( cm3_get_peripheral_bit( &register_stm32f1xx_ADC1.CR2, BITNUMBER_ADC_CR2_ADON ), ttc_assert_origin_auto ); // ADC did not switch on! Maybe clocks are misconfigured?
                        Assert_ADC( cm3_get_peripheral_bit( &register_stm32f1xx_ADC2.CR2, BITNUMBER_ADC_CR2_ADON ), ttc_assert_origin_auto ); // ADC did not switch on! Maybe clocks are misconfigured?
                        Assert_ADC( cm3_get_peripheral_bit( &register_stm32f1xx_ADC3.CR2, BITNUMBER_ADC_CR2_ADON ), ttc_assert_origin_auto ); // ADC did not switch on! Maybe clocks are misconfigured?

                        // Wait for conversions to finish
                        // If one of these while loops is hanging, we have a configuration issue.
                        while ( ! cm3_get_peripheral_bit( &register_stm32f1xx_ADC1.SR, BITNUMBER_ADC_SR_EOC ) );
                        while ( ! cm3_get_peripheral_bit( &register_stm32f1xx_ADC2.SR, BITNUMBER_ADC_SR_EOC ) );
                        while ( ! cm3_get_peripheral_bit( &register_stm32f1xx_ADC3.SR, BITNUMBER_ADC_SR_EOC ) );

                        // Reset status flags
                        *( ( t_u32* ) &register_stm32f1xx_ADC1.SR ) = 0;
                        *( ( t_u32* ) &register_stm32f1xx_ADC2.SR ) = 0;
                        *( ( t_u32* ) &register_stm32f1xx_ADC3.SR ) = 0;
                    }
                    if ( 1 ) { // reset calibration data
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC1.CR2, BITNUMBER_ADC_CR2_RSTCAL, 1 );
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC2.CR2, BITNUMBER_ADC_CR2_RSTCAL, 1 );
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC3.CR2, BITNUMBER_ADC_CR2_RSTCAL, 1 );

                        while ( cm3_get_peripheral_bit( &register_stm32f1xx_ADC1.CR2, BITNUMBER_ADC_CR2_RSTCAL ) ); // wait for reset to finish
                        while ( cm3_get_peripheral_bit( &register_stm32f1xx_ADC2.CR2, BITNUMBER_ADC_CR2_RSTCAL ) ); // wait for reset to finish
                        while ( cm3_get_peripheral_bit( &register_stm32f1xx_ADC3.CR2, BITNUMBER_ADC_CR2_RSTCAL ) ); // wait for reset to finish
                    }
                    if ( 1 ) { // calibrate all ADC devices (-> RM0008 p.214)

                        // start calibration process
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC1.CR2, BITNUMBER_ADC_CR2_CAL, 1 );
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC2.CR2, BITNUMBER_ADC_CR2_CAL, 1 );
                        cm3_set_peripheral_bit( &register_stm32f1xx_ADC3.CR2, BITNUMBER_ADC_CR2_CAL, 1 );

                        while ( cm3_get_peripheral_bit( &register_stm32f1xx_ADC1.CR2, BITNUMBER_ADC_CR2_CAL ) ||
                                cm3_get_peripheral_bit( &register_stm32f1xx_ADC1.CR2, BITNUMBER_ADC_CR2_CAL ) ||
                                cm3_get_peripheral_bit( &register_stm32f1xx_ADC1.CR2, BITNUMBER_ADC_CR2_CAL )
                              ) // wait for end of calibration
                        { ttc_task_usleep( 50 ); }
                    }
                }
            }
            LowLevelConfig->Device->PoweredUp = 1;
            _adc_stm32f1xx_AmountPoweredUp++;
        }
    }
    else {         // power down ADC
        if ( LowLevelConfig->Device->PoweredUp ) {

            // reset ADC configuration
            ttc_memory_set( ( void* ) &register_stm32f1xx_ADC1, 0, sizeof( register_stm32f1xx_ADC1 ) );

            // switch off ADC device
            cm3_set_peripheral_bit( &BaseRegister->CR2, BITNUMBER_ADC_CR2_ADON, 0 );

            switch ( ( t_base ) BaseRegister ) { // switch off clocks for ADC
                case ( t_base ) ADC1: register_stm32f1xx_RCC.APB2ENR.ADC1_EN = 0; break;
                case ( t_base ) ADC2: register_stm32f1xx_RCC.APB2ENR.ADC2_EN = 0; break;
                case ( t_base ) ADC3: register_stm32f1xx_RCC.APB2ENR.ADC3_EN = 0; break;
                default: ttc_assert_halt_origin( ttc_assert_origin_auto ); // BaseRegister points to invalid location: Check your implementation!
            }

            LowLevelConfig->Device->PoweredUp = 0;
            _adc_stm32f1xx_AmountPoweredUp--;
        }
    }
}
void _adc_stm32f1xx_configure_regular_channel( t_ttc_adc_config* Config, BOOL Install, BOOL Replace ) {
    Assert_ADC( ttc_memory_is_writable( Config ), ttc_assert_origin_auto );
    volatile t_register_stm32f1xx_adc* BaseRegister = Config->LowLevelConfig.Device->BaseRegister;

    // Check the parameters
    Assert_ADC( IS_ADC_ALL_PERIPH( ( ADC_TypeDef* ) BaseRegister ), ttc_assert_origin_auto );
    Assert_ADC( IS_ADC_CHANNEL( Config->LowLevelConfig.Channel ),  ttc_assert_origin_auto );
    t_u8 Channel = Config->LowLevelConfig.Channel;

    t_u8 Rank;
    if ( Install ) { // using first free rank
        // ToDo: implement replacing rank like in _adc_stm32f1xx_configure_injected_channel()
        Rank = BaseRegister->SQR1.Bits.L;
        Config->LowLevelConfig.Rank_Regular = Rank + 1;
    }
    else {         // load previously used rank from Config
        Rank = Config->LowLevelConfig.Rank_Regular - 1;
        Assert_ADC( Rank != -1, ttc_assert_origin_auto ); // channel already removed from list
        //X Config->LowLevelConfig.Rank_Regular = 0;
    }

    Assert_ADC( Rank <= ADC_STM32F1XX_MAX_RANK_REGULAR, ttc_assert_origin_auto ); // Invalid rank for this architecture!
    if ( Rank < 7 ) {     // Rank 1 to 6
        t_u32 Content = *( ( t_u32* ) & ( BaseRegister->SQR3 ) );
        t_u8 Shift = 5 * ( Rank - 0 );
        t_u32 Mask = 0b111 << Shift;

        Content &= ~Mask;
        Content |= ( Channel & 0b111 ) << Shift;

        *( ( t_u32* ) & ( BaseRegister->SQR3 ) ) = Content;
    }
    else if ( Rank < 13 ) { // Rank 7 to 12
        t_u32 Content = *( ( t_u32* ) & ( BaseRegister->SQR2 ) );
        t_u8 Shift = 5 * ( Rank - 7 );
        t_u32 Mask = 0b111 << Shift;

        Content &= ~Mask;
        Content |= ( Channel & 0b111 ) << Shift;

        *( ( t_u32* ) & ( BaseRegister->SQR2 ) ) = Content;
    }
    else {                // Rank 13 to 16
        t_u32 Content = *( ( t_u32* ) & ( BaseRegister->SQR1 ) );
        t_u8 Shift = 5 * ( Rank - 13 );
        t_u32 Mask = 0b111 << Shift;

        Content &= ~Mask;
        Content |= ( Channel & 0b111 ) << Shift;

        *( ( t_u32* ) & ( BaseRegister->SQR1 ) ) = Content;
    }
}
void _adc_stm32f1xx_configure_injected_channel( t_ttc_adc_config* Config, BOOL Install, BOOL Replace ) {
    Assert_ADC( ttc_memory_is_writable( Config ), ttc_assert_origin_auto );
    volatile t_register_stm32f1xx_adc* BaseRegister = Config->LowLevelConfig.Device->BaseRegister;

    // Check the parameters
    Assert_ADC( IS_ADC_ALL_PERIPH( ( ADC_TypeDef* ) BaseRegister ), ttc_assert_origin_auto );
    Assert_ADC( IS_ADC_CHANNEL( Config->LowLevelConfig.Channel ),  ttc_assert_origin_auto );
    t_u8 Channel = Config->LowLevelConfig.Channel;

    t_u8 Rank;
    if ( Install ) {
        if ( Replace && Config->LowLevelConfig.Rank_Injected ) { // replace existing rank
            Rank = Config->LowLevelConfig.Rank_Injected  - 1;
        }
        else { // find first free rank
            Assert_ADC( Config->LowLevelConfig.Device->AmountRanksInjected < 4, ttc_assert_origin_auto ); // no more ranks available: remove a rank before
            Rank = BaseRegister->JSQR.Bits.JL;
            Config->LowLevelConfig.Rank_Injected = Rank + 1;
            Config->LowLevelConfig.Device->AmountRanksInjected++;
        }
    }
    else {         // load previously used rank from Config
        Rank = Config->LowLevelConfig.Rank_Injected - 1;
        Assert_ADC( Rank != -1, ttc_assert_origin_auto ); // invalid rank! (Maybe never installed?)
    }

    if ( BaseRegister->JSQR.Bits.JL < Rank - 1 )
    { BaseRegister->JSQR.Bits.JL = Rank - 1; } // increase sequence length to include this rank

    // -> RM0008 p. 241
    Assert_ADC( Rank <= ADC_STM32F1XX_MAX_RANK_INJECTED, ttc_assert_origin_auto ); // Invalid rank for this architecture!
    t_u32 Content = *( ( t_u32* ) & ( BaseRegister->SQR3 ) );
    t_u8  Shift   = 15 - ( 5 * Rank ); // Reversing rank order because injected sequences are converted in strange order (->RM0008 p.241)
    t_u32 Mask    = 0b111 << Shift;

    Content &= ~Mask;
    Content |= ( Channel & 0b11111 ) << Shift;

    *( ( t_u32* ) & ( BaseRegister->JSQR ) ) = Content;
}

/** DEPRECATED

void _adc_de_init(t_ttc_adc_config* Config);
void _adc_init(t_ttc_adc_config* Config, adc_init_estructure* adc_init);
void _adc_cmd(t_ttc_adc_config* Config, bool Enable);
void _adc_resetcalibration(t_ttc_adc_config* Config);
t_u8 _adc_get_reset_calibration_status(t_ttc_adc_config* Config);
void _adc_startcalibration(t_ttc_adc_config* Config);
t_u8 _adc_get_calibration_status(t_ttc_adc_config* Config);
void _adc_software_start_conversion_cmd(t_ttc_adc_config* Config, bool Enable);
t_u8 _adc_get_flag_status(t_ttc_adc_config* Config);
t_u16 _adc_get_conversion_value(t_ttc_adc_config* Config);

e_ttc_adc_errorcode adc_stm32f1xx_init_single(t_ttc_adc_config* Config) {

    if (0) {
    adc_init_estructure adc_init;

    _adc_de_init(Config);

    if ((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister == ADC1)
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    else
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE);

    adc_init.adc_mode = ((t_u32)0x00000000);             // ADC_Mode_Independent
    adc_init.adc_ScanSingleMode = 0;                        // Enable single mode
    adc_init.adc_ContinuousSingleMode = 1;                  // Enable continuous mode
    adc_init.adc_ExternalTrigConv = ((t_u32)0x000E0000); // ADC_ExternalTrig_Conv_none
    adc_init.adc_DataAlign = ((t_u32)0x00000000);        // ADC_DATAAlign_RIGHT
    adc_init.adc_numberofChannel = 1;                       // 1 Channel

    _adc_init(Config, &adc_init);

    _adc_cmd(Config, TRUE);

    _adc_resetcalibration(Config);
    while(_adc_get_reset_calibration_status(Config));

    _adc_startcalibration(Config);
    while(_adc_get_calibration_status(Config));

    _adc_software_start_conversion_cmd(Config, TRUE);
    }

    ADC_InitTypeDef ADC_InitStructure;

    if (Config->LogicalIndex == 1) {
        Config->LowLevelConfig.Device->BaseRegister = (t_register_stm32f1xx_adc*) ADC1;
    }
    else {
        Config->LowLevelConfig.Device->BaseRegister = (t_register_stm32f1xx_adc*) ADC2;
    }

    ADC_DeInit( (ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister);                                                             // ADC auf Init-Werte rücksetzen
    if ((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister == ADC1)
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    else
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE);                      // ADC Takt freigeben

    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;                            // Arbeitsweise unabhängig
    ADC_InitStructure.ADC_ScanConvMode = DISABLE;                                 // kein Scan-Mode (nur 1 Kanal verwendet)
    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;                            // kontinuierliche Abtastung
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;           // keine externe Triggerung
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;                        // Datenformat der Wandlung (rechtsbündig 12-Bit)
    ADC_InitStructure.ADC_NbrOfChannel = 1;                                       // 1 Kanal
    ADC_Init( (ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister, &ADC_InitStructure);                                          // ADC initialisieren

    ADC_Cmd((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister, ENABLE);                                                        // ADC freigeben

    ADC_ResetCalibration((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister);                                                   // ADC Kalibrierungswerte rücksetzen
    while(ADC_GetResetCalibrationStatus((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister));                                   // Ende abwarten

    ADC_StartCalibration((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister);                                                   // ADC Kalibrierung
    while(ADC_GetCalibrationStatus((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister));                                        // Ende abwarten

    ADC_SoftwareStartConvCmd((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister, ENABLE);                                       // ADC Abtastung starten

    return (e_ttc_adc_errorcode) 0;
}

 void _adc_cmd(t_ttc_adc_config* Config, bool Enable) {
   t_register_stm32f1xx_adc_cr2 CR2;

   CR2 = Config->LowLevelConfig.Device->BaseRegister->CR2;

   // Check the parameters
   Assert_ADC(IS_ADC_ALL_PERIPH((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister), ttc_assert_origin_auto);
   Assert_ADC(IS_FUNCTIONAL_STATE(Enable), ttc_assert_origin_auto);
   if (Enable != 0)
   {
       // Set the ADON bit to wake up the ADC from power down mode
       //Config->LowLevelConfig.Device->BaseRegister->CR2 |= ((t_u32)0x00000001);    //CR2_ADON_Set
       CR2.Bits.ADON = 1;
   }
   else
   {
       // Disable the selected ADC peripheral
       //Config->LowLevelConfig.Device->BaseRegister->CR2 &= ((t_u32)0xFFFFFFFE);    //CR2_ADON_Reset
       CR2.Bits.ADON = 0;
   }

   Config->LowLevelConfig.Device->BaseRegister->CR2 = CR2;

}
 void _adc_de_init(t_ttc_adc_config* Config) {

    // Check the parameters
      Assert_ADC(IS_ADC_ALL_PERIPH((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister), ttc_assert_origin_auto);

      if ( (t_base) Config->LowLevelConfig.Device->BaseRegister == (t_base) ADC1)
      {
        // Enable ADC1 reset state
        RCC_APB2PeriphResetCmd(RCC_APB2Periph_ADC1, ENABLE);
        // Release ADC1 from reset state
        RCC_APB2PeriphResetCmd(RCC_APB2Periph_ADC1, DISABLE);
      }
      else if ( (t_base) Config->LowLevelConfig.Device->BaseRegister == (t_base) ADC2)
      {
        // Enable ADC2 reset state
        RCC_APB2PeriphResetCmd(RCC_APB2Periph_ADC2, ENABLE);
        // Release ADC2 from reset state
        RCC_APB2PeriphResetCmd(RCC_APB2Periph_ADC2, DISABLE);
      }
      else
      {
        if ( (t_base) Config->LowLevelConfig.Device->BaseRegister == (t_base) ADC3)
        {
          // Enable ADC3 reset state
          RCC_APB2PeriphResetCmd(RCC_APB2Periph_ADC3, ENABLE);
          // Release ADC3 from reset state
          RCC_APB2PeriphResetCmd(RCC_APB2Periph_ADC3, DISABLE);
        }
      }

}
 t_u8 _adc_get_calibration_status(t_ttc_adc_config* Config) {
    t_register_stm32f1xx_adc_cr2 CR2;
    CR2 = Config->LowLevelConfig.Device->BaseRegister->CR2;

    t_u8 bitstatus = 0;
    // Check the parameters
    Assert_ADC(IS_ADC_ALL_PERIPH((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister), ttc_assert_origin_auto);
    // Check the status of CAL bit
    if (CR2.Bits.CAL != (t_u32)0)
    {
        // CAL bit is set: calibration on going
        bitstatus = 1;
    }
    else
    {
        // CAL bit is reset: end of calibration
        bitstatus = 0;
    }
    // Return the CAL bit status
    return  bitstatus;
}
t_u16 _adc_get_conversion_value(t_ttc_adc_config* Config) {
     t_u16 data;

     // Check the parameters
     Assert_ADC(IS_ADC_ALL_PERIPH((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister), ttc_assert_origin_auto);
     // Return the selected ADC conversion value

     data = Config->LowLevelConfig.Device->BaseRegister->DR.Bits.DATA;
     return data;
 }
 t_u8 _adc_get_flag_status(t_ttc_adc_config* Config) {
    t_s_register_stm32f1xx_adcr SR;
    SR = Config->LowLevelConfig.Device->BaseRegister->SR;

    t_u8 bitstatus = 0;

    // Check the parameters
    Assert_ADC(IS_ADC_ALL_PERIPH((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister), ttc_assert_origin_auto);
    Assert_ADC(IS_ADC_GET_FLAG(Flag), ttc_assert_origin_auto);

    // Check the status of the specified ADC flag
    if (SR.Bits.EOC != 0)
    {
        // ADC_FLAG is set
        bitstatus = 1;
    }
    else
    {
        // ADC_FLAG is reset
        bitstatus = 0;
    }
    // Return the ADC_FLAG status
    return  bitstatus;
}
 t_u8 _adc_get_reset_calibration_status(t_ttc_adc_config* Config) {
    t_register_stm32f1xx_adc_cr2 CR2;
    CR2 = Config->LowLevelConfig.Device->BaseRegister->CR2;

    t_u8 bitstatus = 0;

    // Check the parameters
    Assert_ADC(IS_ADC_ALL_PERIPH((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister), ttc_assert_origin_auto);
    // Check the status of RSTCAL bit
    if (CR2.Bits.RSTCAL != (t_u32)0)
    {
        // RSTCAL bit is set
        bitstatus = 1;
    }
    else
    {
        // RSTCAL bit is reset
        bitstatus = 0;
    }
    // Return the RSTCAL bit status
    return  bitstatus;

}
 void _adc_init(t_ttc_adc_config* Config, adc_init_estructure* adc_init) {

    // Check the parameters
    Assert_ADC(IS_ADC_ALL_PERIPH((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister), ttc_assert_origin_auto);
    Assert_ADC(IS_ADC_MODE(adc_init->adc_mode), ttc_assert_origin_auto);
    Assert_ADC(IS_FUNCTIONAL_STATE(adc_init->adc_ScanSingleMode), ttc_assert_origin_auto);
    Assert_ADC(IS_FUNCTIONAL_STATE(adc_init->adc_ContinuousSingleMode), ttc_assert_origin_auto);
    Assert_ADC(IS_ADC_EXT_TRIG(adc_init->adc_ExternalTrigConv), ttc_assert_origin_auto);
    Assert_ADC(IS_ADC_DATA_ALIGN(adc_init->adc_DataAlign), ttc_assert_origin_auto);
    Assert_ADC(IS_ADC_REGULAR_LENGTH(adc_init->adc_numberofChannel), ttc_assert_origin_auto);

    // ---------------------------- ADCx CR1 Configuration -----------------
    // Get the ADCx CR1 value
    //tmpreg1 = Config->LowLevelConfig.Device->BaseRegister->CR1;
    // Clear DUALMOD and SCAN bits
    //tmpreg1 &= ((t_u32)0xFFF0FEFF);     //CR1_CLEAR_Mask
    // Configure ADCx: Dual mode and scan conversion mode
    // Set DUALMOD bits according to ADC_Mode value
    // Set SCAN bit according to ADC_ScanConvMode value
    //tmpreg1 |= (t_u32)(adc_init->adc_mode | ((t_u32)adc_init->adc_ScanSingleMode << 8));
    // Write to ADCx CR1

    t_register_stm32f1xx_adc_cr1 CR1; //Config->LowLevelConfig.Device->BaseRegister->CR1;

    //((t_u32)(adc_init->adc_mode | ((t_u32)adc_init->adc_ScanSingleMode << 8)));
    CR1.All = 0;

    Config->LowLevelConfig.Device->BaseRegister->CR1 = CR1;

    // ---------------------------- ADCx CR2 Configuration -----------------
    // Get the ADCx CR2 value
    //tmpreg1 = Config->LowLevelConfig.Device->BaseRegister->CR2;
    // Clear CONT, ALIGN and EXTSEL bits
    //tmpreg1 &= ((t_u32)0xFFF1F7FD);  //CR2_CLEAR_Mask
    // Configure ADCx: external trigger event and continuous conversion mode
    // Set ALIGN bit according to ADC_DataAlign value
    // Set EXTSEL bits according to ADC_ExternalTrigConv value
    // Set CONT bit according to ADC_ContinuousConvMode value
    //tmpreg1 |= (t_u32)(adc_init->adc_DataAlign | adc_init->adc_ExternalTrigConv |
    //        ((t_u32)adc_init->adc_ContinuousSingleMode << 1));
    // Write to ADCx CR2

    t_register_stm32f1xx_adc_cr2 CR2 = Config->LowLevelConfig.Device->BaseRegister->CR2;

    //(t_u32)(adc_init->adc_DataAlign | adc_init->adc_ExternalTrigConv |  ((t_u32)adc_init->adc_ContinuousSingleMode << 1));
    CR2.All = 0;
    CR2.Bits.TSVREFE = 1;
    CR2.Bits.SWSTART = 1;
    CR2.Bits.JSWSTART = 1;
    CR2.Bits.EXTTRIG = 1;
    CR2.Bits.CONT = 1;

    Config->LowLevelConfig.Device->BaseRegister->CR2 = CR2;

    // ---------------------------- ADCx SQR1 Configuration -----------------
    // Get the ADCx SQR1 value
    //tmpreg1 = Config->LowLevelConfig.Device->BaseRegister->SQR1;
    // Clear L bits
    //tmpreg1 &= ((t_u32)0xFF0FFFFF);  //SQR1_CLEAR_Mask
    // Configure ADCx: regular channel sequence length
    // Set L bits according to ADC_NbrOfChannel value
    //tmpreg2 |= (t_u8) (adc_init->adc_numberofChannel - (t_u8)1);
    //tmpreg1 |= (t_u32)tmpreg2 << 20;
    // Write to ADCx SQR1

    t_s_register_stm32f1xx_adcqr1 SQR1 = Config->LowLevelConfig.Device->BaseRegister->SQR1;

    SQR1.All = 0;

    Config->LowLevelConfig.Device->BaseRegister->SQR1 = SQR1;


}
 void _adc_resetcalibration(t_ttc_adc_config* Config) {

    // Check the parameters
    Assert_ADC(IS_ADC_ALL_PERIPH((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister), ttc_assert_origin_auto);

    t_register_stm32f1xx_adc_cr2 CR2 = Config->LowLevelConfig.Device->BaseRegister->CR2;

    // Resets the selected ADC calibration registers
    //Config->LowLevelConfig.Device->BaseRegister->CR2 |= ((t_u32)0x00000008);    //CR2_RSTCAL_Set
    CR2.Bits.RSTCAL = 1;
    Config->LowLevelConfig.Device->BaseRegister->CR2 = CR2;

}
 void _adc_startcalibration(t_ttc_adc_config* Config) {
    t_register_stm32f1xx_adc_cr2 CR2;
    CR2 = Config->LowLevelConfig.Device->BaseRegister->CR2;

    // Check the parameters
    Assert_ADC(IS_ADC_ALL_PERIPH((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister->ADCx), ttc_assert_origin_auto);

    // Enable the selected ADC calibration process
    //Config->LowLevelConfig.Device->BaseRegister->CR2 |= ((t_u32)0x00000004);    //CR2_CAL_Set
    CR2.Bits.CAL = 1;
    Config->LowLevelConfig.Device->BaseRegister->CR2 = CR2;
}
 void _adc_software_start_conversion_cmd(t_ttc_adc_config* Config, bool Enable) {
    t_register_stm32f1xx_adc_cr2 CR2;
    CR2 = Config->LowLevelConfig.Device->BaseRegister->CR2;

      // Check the parameters //
      Assert_ADC(IS_ADC_ALL_PERIPH((ADC_TypeDef *) Config->LowLevelConfig.Device->BaseRegister), ttc_assert_origin_auto);
      Assert_ADC(IS_FUNCTIONAL_STATE(Enable), ttc_assert_origin_auto);
      if (Enable != 0)
      {
        // Enable the selected ADC conversion on external event and start the selected ADC conversion
        //Config->LowLevelConfig.Device->BaseRegister->CR2 |= ((t_u32)0x00500000);    //CR2_EXTTRIG_SWSTART_Set
          CR2.Bits.EXTTRIG = 1;
          CR2.Bits.SWSTART = 1;
      }
      else
      {
        // Disable the selected ADC conversion on external event and stop the selected ADC conversion
        //Config->LowLevelConfig.Device->BaseRegister->CR2 &= ((t_u32)0xFFAFFFFF);    //CR2_EXTTRIG_SWSTART_Reset
          CR2.Bits.EXTTRIG = 0;
          CR2.Bits.SWSTART = 0;
      }

      Config->LowLevelConfig.Device->BaseRegister->CR2 = CR2;
}
*/
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
