/** { adc_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for adc devices on stm32l1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20141014 12:37:34 UTC
 *
 *  Note: See ttc_adc.h for description of stm32l1xx independent ADC implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "adc_stm32l1xx.h".
//
#include "adc_stm32l1xx.h"
#include "../ttc_basic.h"
#include "../register/register_stm32l1xx.h"
#include "../ttc_sysclock.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Private Function Declarations **********************************************

/** ADD_DESCRIPTION_HERE
 *
 * @param Config (t_ttc_adc_config*)  
 * @param Enable (bool)               

 */
void _adc_stm32l1xx_cmd(t_ttc_adc_config* Config, bool Enable);

/** ADD_DESCRIPTION_HERE
 *
 * @param Config (t_ttc_adc_config*)  

 */
void _adc_stm32l1xx_regular_channel_config(t_ttc_adc_config* Config, t_u8 Channel, t_u8 Rank, t_u8 Cycles);

/** ADD_DESCRIPTION_HERE
 *
 * @param Config   (t_ttc_adc_config*)  
 * @param ADC_FLAG (t_u16)              
 * @return         (t_u8)               
 */
t_u8 _adc_stm32l1xx_get_flag_status(t_ttc_adc_config* Config, t_u16 ADC_FLAG);

/** ADD_DESCRIPTION_HERE
 *
 * @param Config (t_ttc_adc_config*)  

 */
void _adc_stm32l1xx_software_start_conv(t_ttc_adc_config* Config);

/** ADD_DESCRIPTION_HERE
 *
 * @param Config (t_ttc_adc_config*)  

 */
void _adc_stm32l1xx_adc_stm32l1xx_software_start_conv(t_ttc_adc_config* Config);

//InsertPrivateFunctionDeclarations
//} Private Function Declarations
//{ Function Definitions *******************************************************

e_ttc_adc_errorcode adc_stm32l1xx_deinit(t_ttc_adc_config* Config) {
    Assert_ADC(Config, ttc_assert_origin_auto); // pointers must not be NULL

    if (Config->LowLevelConfig.BaseRegister->SR.Bits.ADONS) { //  switch of ADC
        volatile t_register_stm32l1xx_adc_cr2 CR2;
        CR2.All = Config->LowLevelConfig.BaseRegister->CR2.All;
        CR2.Bits.ADON = 0;
        Config->LowLevelConfig.BaseRegister->CR2.All = CR2.All;
        t_u16 TimeOut = -1;
        while (TimeOut-- && Config->LowLevelConfig.BaseRegister->SR.Bits.ADONS);
        Assert(!Config->LowLevelConfig.BaseRegister->SR.Bits.ADONS, ec_adc_TimeOut); // could not switch off ADC
    }

    return (e_ttc_adc_errorcode) 0;
}
e_ttc_adc_errorcode adc_stm32l1xx_get_features(t_ttc_adc_config* Config) {
    Assert_ADC(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function adc_stm32l1xx_get_features() is empty.
    
    return (e_ttc_adc_errorcode) 0;
}
e_ttc_adc_errorcode adc_stm32l1xx_init(t_ttc_adc_config* Config) {
    Assert_ADC(Config, ttc_assert_origin_auto); // pointers must not be NULL

    //adc_init_structure adc_init;

    e_gpio_stm32l1xx_bank GPIOx = (t_ttc_gpio_bank) cm3_calc_peripheral_Word(Config->InputPin);
    t_u8                    Pin = (t_u8)            cm3_calc_peripheral_BitNumber(Config->InputPin);

    Config->LowLevelConfig.BaseRegister = (t_register_stm32l1xx_adc*) 0x40012400;
    ttc_sysclock_enable_oscillator(E_ttc_sysclock_oscillator_HighSpeedInternal, 1);

    if ((t_u32) GPIOx == (t_u32) GPIOA) {
        register_stm32l1xx_RCC.AHBENR.Bits.GPIOA_EN = 1;
    }
    else if ((t_u32) GPIOx == (t_u32) GPIOB) {
        register_stm32l1xx_RCC.AHBENR.Bits.GPIOB_EN = 1;
    }
    else if ((t_u32) GPIOx == (t_u32) GPIOC) {
        register_stm32l1xx_RCC.AHBENR.Bits.GPIOC_EN = 1;
    }

    //sysclock_stm32l1xx_RCC_APB2PeriphClockCmd(rcc_APB2ENR_ADC1EN, ENABLE);
    register_stm32l1xx_RCC.APB2ENR.Bits.ADC1EN = 1;

    adc_stm32l1xx_deinit(Config);
    register_stm32l1xx_RCC.APB2RSTR.Bits.ADC1RST = 1;
    register_stm32l1xx_RCC.APB2RSTR.Bits.ADC1RST = 0;

    volatile t_register_stm32l1xx_adc_cr1   CR1;
    CR1.All = Config->LowLevelConfig.BaseRegister->CR1.All;
    CR1.Bits.RES = 0;    // 12b Resolution
    CR1.Bits.SCAN = 0;   // Scan Conversion Mode Disable
    Config->LowLevelConfig.BaseRegister->CR1.All = CR1.All;

    volatile t_register_stm32l1xx_adc_cr2 CR2;
    CR2.All = Config->LowLevelConfig.BaseRegister->CR2.All;
    CR2.Bits.ALIGN = 0;  // Data Align right
    CR2.Bits.EXTSEL = 6; // ExternalTrigConv_T2_TRGO
    CR2.Bits.EXTEN = 0;  // ADC_ExternalTrigConvEdge_None
    CR2.Bits.CONT = 1;   // Continous Conversion Mode Enable

    Config->LowLevelConfig.BaseRegister->SQR1.Bits.L = 0;     // Number of Conversions = 1

    Config->Channel = adc_stm32l1xx_find_channel(GPIOx, Pin);
    CR2.Bits.ADON = 1;
    Config->LowLevelConfig.BaseRegister->CR2.All = CR2.All;

    t_u16 TimeOut = -1;
    while (TimeOut-- && !Config->LowLevelConfig.BaseRegister->SR.Bits.ADONS);
    Assert(Config->LowLevelConfig.BaseRegister->SR.Bits.ADONS, ttc_assert_origin_auto); // cannot switch on ADC! (maybe wrong clock?)

    ttc_task_msleep(100);

    return (e_ttc_adc_errorcode) 0;
}
e_ttc_adc_errorcode adc_stm32l1xx_init_dma(t_ttc_adc_config* Config, volatile t_u16* Value) {
    Assert_ADC(Config, ttc_assert_origin_auto); // pointers must not be NULL
    //Assert_ADC(Value, ttc_assert_origin_auto); // pointers must not be NULL

#warning adc_stm32l1xx_init_dma() is empty!

//    ADC_InitTypeDef ADC_InitStructure;
//    DMA_InitTypeDef DMA_InitStructure;
//    GPIO_InitTypeDef GPIO_InitStructure;

//    /* Enable DMA1 clock */
//    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

//    /* DMA1 channel1 configuration */
//    DMA_DeInit(DMA1_Channel1);
//    DMA_InitStructure.DMA_BufferSize = 2;
//    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
//    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
//    DMA_InitStructure.DMA_MemoryBaseAddr = (t_u32)Config->AD_Value;
//    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
//    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
//    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
//    DMA_InitStructure.DMA_PeripheralBaseAddr = ((t_u32)0x40012458);
//    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
//    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
//    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
//    DMA_Init(DMA1_Channel1, &DMA_InitStructure);

//    /* Enable DMA1 channel1 */
//    DMA_Cmd(DMA1_Channel1, ENABLE);

//    /* Enable GPIOA clock */
//    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
//    /* Configure PA1 (Channel 1) in analog mode */
//    GPIO_DeInit(GPIOA);
//    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
//    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
//    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
//    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
//    GPIO_Init(GPIOA, &GPIO_InitStructure);

//    /* Check that HSI oscillator is ready */
//    while(RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET);

//    /* Enable ADC1 clock */
//    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
//    /* ADC1 configuration */
//    ADC_DeInit(ADC1);
//    ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
//    ADC_InitStructure.ADC_ScanConvMode = ENABLE;
//    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
//    ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
//    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
//    ADC_InitStructure.ADC_NbrOfConversion = 1;
//    ADC_Init(ADC1, &ADC_InitStructure);

//    /* ADC1 regular channel1 configuration */
//    ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_4Cycles);

//    /* Enable the request after last transfer for DMA Circular mode */
//    ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);

//    /* Enable ADC1 DMA */
//    ADC_DMACmd(ADC1, ENABLE);

//    /* Enable ADC1 */
//    ADC_Cmd(ADC1, ENABLE);

//    /* Wait until the ADC1 is ready */
//    while(ADC_GetFlagStatus(ADC1, ADC_FLAG_ADONS) == RESET) {}

//    /* Start ADC1 Software Conversion */
//    ADC_SoftwareStartConv(ADC1);

    return (e_ttc_adc_errorcode) 0;
}
t_u16               adc_stm32l1xx_find_channel(e_gpio_stm32l1xx_bank GPIOx, t_u8 Pin) {
    if((t_u32) GPIOx == (t_u32) GPIOA) {
        if(Pin<8)
            return Pin;
        else
            Assert_ADC(1,ttc_assert_origin_auto);

    }
    else if((t_u32) GPIOx == (t_u32) GPIOB) {
        if(Pin == 0) return 8;
        else if(Pin == 1) return 9;
        else Assert_ADC(1,ttc_assert_origin_auto);
    }
    else if((t_u32) GPIOx == (t_u32) GPIOC) {
        if(Pin<6) return (Pin+10);
        else Assert_ADC(1,ttc_assert_origin_auto);
    }
    return 0;
}
e_ttc_adc_errorcode adc_stm32l1xx_load_defaults(t_ttc_adc_config* Config) {
    Assert_ADC(Config, ttc_assert_origin_auto); // pointers must not be NULL

    Config->Architecture = ta_adc_stm32l1xx;
    return (e_ttc_adc_errorcode) 0;
}
void                adc_stm32l1xx_prepare() {

    register_stm32l1xx_RCC.APB2ENR.Bits.ADC1EN = 0;
}
e_ttc_adc_errorcode adc_stm32l1xx_reset(t_ttc_adc_config* Config) {
    Assert_ADC(Config, ttc_assert_origin_auto); // pointers must not be NULL

    adc_stm32l1xx_deinit(Config);
    return adc_stm32l1xx_init(Config);
}
t_u16               adc_stm32l1xx_get_value(t_ttc_adc_config* Config) {

    t_register_stm32l1xx_adc_cr2 tmpr;

    _adc_stm32l1xx_regular_channel_config(Config, Config->Channel, 1, 7);

    tmpr = Config->LowLevelConfig.BaseRegister->CR2;
    tmpr.All |= ((t_u32)0x00000010);      //Freeze mode
    Config->LowLevelConfig.BaseRegister->CR2 = tmpr;

    _adc_stm32l1xx_software_start_conv(Config);

    //while(_adc_stm32l1xx_get_flag_status(Config, adc_FLAG_EOC) == 0)
    while(Config->LowLevelConfig.BaseRegister->SR.Bits.EOC == 0)
        ttc_task_yield();

    return (t_u16)Config->LowLevelConfig.BaseRegister->DR.Bits.DATA;

}
e_ttc_adc_errorcode adc_stm32l1xx_find_input_pin(t_ttc_adc_config* Config) {
    Assert_ADC(Config, ttc_assert_origin_auto); // pointers must not be NULL

    // Only one ADC and direct Pin-# to ADC-Channel assignement on STM32L1xx architecture

    return (e_ttc_adc_errorcode) 0;
}
t_base_signed       adc_stm32l1xx_get_maximum(t_u8 LogicalIndex) {

    
    return (t_base_signed) (1 << 12)- 1;
}
t_base_signed       adc_stm32l1xx_get_minimum(t_u8 LogicalIndex) {
    
    return (t_base_signed) 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//Just for Channel:[1-9] and Rank:[1-6]
void _adc_stm32l1xx_regular_channel_config(t_ttc_adc_config* Config, t_u8 Channel, t_u8 Rank, t_u8 Cycles) {

    /* Check the parameters */
    assert_param(IS_ADC_ALL_PERIPH(Config->LowLevelConfig.BaseRegister));
    assert_param(IS_ADC_CHANNEL(Channel));
    assert_param(IS_ADC_REGULAR_RANK(Rank));
    assert_param(IS_ADC_SAMPLE_TIME(Cycles));

    //SMPR3 Config
    t_register_stm32l1xx_adc_smpr3 SMPR3;
    SMPR3.All = 0;

    if(Channel==0) {
        SMPR3.Bits.SMP0 = Cycles;
    }
    else if(Channel==1) {
        SMPR3.Bits.SMP1 = Cycles;
    }
    else if(Channel==2) {
        SMPR3.Bits.SMP2 = Cycles;
    }
    else if(Channel==3) {
        SMPR3.Bits.SMP3 = Cycles;
    }
    else if(Channel==4) {
        SMPR3.Bits.SMP4 = Cycles;
    }
    else if(Channel==5) {
        SMPR3.Bits.SMP5 = Cycles;
    }
    else if(Channel==6) {
        SMPR3.Bits.SMP6 = Cycles;
    }
    else if(Channel==7) {
        SMPR3.Bits.SMP7 = Cycles;
    }
    else if(Channel==8) {
        SMPR3.Bits.SMP8 = Cycles;
    }
    else if(Channel==9) {
        SMPR3.Bits.SMP9 = Cycles;
    }

    Config->LowLevelConfig.BaseRegister->SMPR3 = SMPR3;


    //SQR5 Config
    t_register_stm32l1xx_adc_sqr5 SQR5;
    SQR5.All = 0;

    if(Rank==1) {
        SQR5.Bits.SQ1 = Channel;
    }
    else if(Rank==2) {
        SQR5.Bits.SQ2 = Channel;
    }
    else if(Rank==3) {
        SQR5.Bits.SQ3 = Channel;
    }
    else if(Rank==4) {
        SQR5.Bits.SQ4 = Channel;
    }
    else if(Rank==5) {
        SQR5.Bits.SQ5 = Channel;
    }
    else if(Rank==6) {
        SQR5.Bits.SQ6 = Channel;
    }

    Config->LowLevelConfig.BaseRegister->SQR5= SQR5;
}
void _adc_stm32l1xx_cmd(t_ttc_adc_config* Config, bool Enable) {

//    t_register_stm32l1xx_adc_cr2 CR2;
//    CR2 = Config->LowLevelConfig.BaseRegister->CR2;

//    /* Check the parameters */
//    assert_param(IS_ADC_ALL_PERIPH(Config->LowLevelConfig.BaseRegister));
//    assert_param(IS_FUNCTIONAL_STATE(Enable));

//    if (Enable != 0)
//    {
//        /* Set the ADON bit to wake up the ADC from power down mode */
//        CR2.Bits.ADON = 1;
//    }
//    else
//    {
//        /* Disable the selected ADC peripheral */
//        CR2.All = 0; //Bits.ADON = 0;
//    }

//    Config->LowLevelConfig.BaseRegister->CR2 = CR2;
}
void _adc_stm32l1xx_software_start_conv(t_ttc_adc_config* Config) {

    t_register_stm32l1xx_adc_cr2 CR2;
    CR2 = Config->LowLevelConfig.BaseRegister->CR2;

    /* Check the parameters */
    assert_param(IS_ADC_ALL_PERIPH(Config->LowLevelConfig.BaseRegister));
    assert_param(IS_FUNCTIONAL_STATE(Enable));

    CR2.Bits.SWSTART = 1;
    Config->LowLevelConfig.BaseRegister->CR2 = CR2;
}
t_u8 _adc_stm32l1xx_get_flag_status(t_ttc_adc_config* Config, t_u16 ADC_FLAG) {

//    ADC_SR_t SR;
//    SR = Config->LowLevelConfig.BaseRegister->SR;

//    t_u8 bitstatus = 0;

//    /* Check the parameters */
//    assert_param(IS_ADC_ALL_PERIPH(Config->LowLevelConfig.BaseRegister));
//    assert_param(IS_ADC_GET_FLAG(Flag));

//    /* Check the status of the specified ADC flag */
//    if ((SR.All & ADC_FLAG) != (t_u8)0)
//    {
//        /* ADC_FLAG is set */
//        bitstatus = 1;
//    }
//    else
//    {
//        /* ADC_FLAG is reset */
//        bitstatus = 0;
//    }
//    /* Return the ADC_FLAG status */
//    return  bitstatus;
    return 0;
}
void _adc_stm32l1xx_adc_stm32l1xx_software_start_conv(t_ttc_adc_config* Config) {
    Assert(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // always check incoming pointer arguments
  
    //your_code_goes_here

}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
