/** { ttc_filesystem_types2.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Datatype definitions required by ttc_filesystem and external storage drivers.
 *  Structures, Enums and Defines being required by both, high- and low-level filesystem.
 *  This file does not provide function declarations.
 *
 *  Note: See ttc_filesystem.h for description of high-level filesystem implementation!
 *
 *  Created from template ttc_device_types.h revision 45 at 20180413 11:21:12 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_FILESYSTEM_TYPES2_H
#define TTC_FILESYSTEM_TYPES2_H

typedef enum {   // e_ttc_storage_event             event codes of all kind of storage devices
    E_ttc_storage_event_media_missing = 0,            // media slot is empty as was on last call

    E_ttc_storage_event_media_present,                // media is present and available
    E_ttc_storage_event_media_inserted,               // media has just been inserted (only reported once)
    E_ttc_storage_event_media_removed,                // media has been removed
    E_ttc_storage_event_media_removed_medium_mounted, // media has been removed while being mounted (data loss may have occured!)

    E_ttc_storage_event_ERROR,                        // failures below
    E_ttc_storage_event_invalid_storage_configuration,// ttc_filesystem device has detected an invalid configuration of storage device driver

    // other failures go here..

    E_ttc_storage_event_unknown                // no valid events past this entry
} e_ttc_storage_event;

#endif
