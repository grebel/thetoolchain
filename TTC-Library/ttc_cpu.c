/** { ttc_cpu.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for cpu devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  See corresponding header file for documentation.
 *
 *  Created from template ttc_device.c revision 27 at 20150318 12:33:43 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_cpu.h".
//
#include "ttc_cpu.h"
#include "ttc_memory.h"
#include "ttc_heap.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_CPU_AMOUNT == 0
    #warning No CPU devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of cpu devices.
 *
 */


// for each initialized device, a pointer to its generic and stm32f1xx definitions is stored
A_define( t_ttc_cpu_config*, ttc_cpu_configs, TTC_CPU_AMOUNT );

const t_ttc_cpu_info ttc_cpu_Info = {
    .Size_RAM     = ( t_base ) TTC_CPU_SIZE_RAM,
    .Size_FLASH   = ( t_base ) TTC_CPU_SIZE_FLASH,
    .Size_EEPROM  = ( t_base ) TTC_CPU_SIZE_FLASH, // ToDo: How to configure size of EEPROM emulated in FLASH?
    .Amount_Pins  = ( t_u16 )  TTC_CPU_AMOUNT_PINS,
    .Amount_GPIOs = ( t_u16 )  TTC_CPU_AMOUNT_GPIOS,
    .Variant      = TTC_CPU_VARIANT,
    .Architecture = TTC_CPU_ARCHITECTURE
};

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8 ttc_cpu_get_max_index() {
    return TTC_CPU_AMOUNT;
}
t_ttc_cpu_config* ttc_cpu_get_configuration( t_u8 LogicalIndex ) {
    Assert_CPU( LogicalIndex, ttc_assert_origin_auto );  // logical index starts at 1
    t_ttc_cpu_config* Config = A( ttc_cpu_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        Config = A( ttc_cpu_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_cpu_config ) );
        Config->Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_cpu_load_defaults( LogicalIndex );
    }

    return Config;
}
e_ttc_cpu_errorcode ttc_cpu_init( t_u8 LogicalIndex ) {
    Assert_CPU( LogicalIndex, ttc_assert_origin_auto );  // logical index starts at 1
    t_ttc_cpu_config* Config = ttc_cpu_get_configuration( LogicalIndex );

    e_ttc_cpu_errorcode Result = _driver_cpu_init( Config );
    if ( Result == ec_cpu_OK )
    { Config->Flags.Bits.Initialized = 1; }

    return Result;
}
e_ttc_cpu_errorcode ttc_cpu_load_defaults( t_u8 LogicalIndex ) {
    Assert_CPU( LogicalIndex, ttc_assert_origin_auto );  // logical index starts at 1
    t_ttc_cpu_config* Config = ttc_cpu_get_configuration( LogicalIndex );

    ttc_memory_set( Config, 0, sizeof( t_ttc_cpu_config ) );

    Config->Info = &ttc_cpu_Info;

    // load generic default values
    e_ttc_cpu_errorcode Error = _driver_cpu_load_defaults( Config );


    Assert_CPU( ( Config->Info->Architecture > ta_cpu_None ) && ( Config->Info->Architecture < ta_cpu_unknown ), ttc_assert_origin_auto ); // architecture not set properly! Check makefile for TTC_CPU<n>_DRIVER and compare with e_ttc_cpu_architecture

    //Insert additional generic default values here ...
    return Error;
}
void ttc_cpu_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)
    A_reset( ttc_cpu_configs, TTC_CPU_AMOUNT ); // must reset data of safe array manually!
    _driver_cpu_prepare();
    //  ttc_cpu_get_configuration(1);
}
void ttc_cpu_reset( t_u8 LogicalIndex ) {
    Assert_CPU( LogicalIndex, ttc_assert_origin_auto );  // logical index starts at 1
    t_ttc_cpu_config* Config = ttc_cpu_get_configuration( LogicalIndex );

    _driver_cpu_reset( Config );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_cpu(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
