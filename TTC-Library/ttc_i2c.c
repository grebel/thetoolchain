/** { ttc_i2c.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for i2c devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  See corresponding header file for documentation.
 *
 *  Created from template ttc_device.c revision 29 at 20150531 21:58:32 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_i2c.h".
//
#include "ttc_i2c.h"
#include "ttc_memory.h"
#include "ttc_heap.h"
#include "ttc_sysclock.h"
#include "ttc_gpio.h"
#include "i2c/i2c_common.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_I2C_AMOUNT == 0
#error No I2C devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of i2c devices.
 *
 */


// for each initialized device, a pointer to its generic and stm32l1xx definitions is stored
A_define( t_ttc_i2c_config*, ttc_i2c_configs, TTC_I2C_AMOUNT );

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ******************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_i2c(t_ttc_i2c_config* Config)

/** Handles given I2C error
 *
 * If timeouts occour on I2C bus, then this function will cause a reinitialisation of I2C device to avoid lockups.
 *
 * @param Config  (t_ttc_i2c_config)     Configuration of i2c device being used
 * @param Error   (e_ttc_i2c_errorcode)  error being occured during low-level action
 * @return        (e_ttc_i2c_errorcode)  Error being given
 */
e_ttc_i2c_errorcode _ttc_i2c_master_handle_error( t_ttc_i2c_config* Config, e_ttc_i2c_errorcode Error );
e_ttc_i2c_errorcode _ttc_i2c_slave_handle_error( t_ttc_i2c_config* Config, e_ttc_i2c_errorcode Error );

/** Checks if I2C bus is free (helper function matching to i2c_common_wait_for() )
 *
 * @param BaseRegister (t_ttc_i2c_base_register*) adress of base of low-level peripheral register responsible for I2C device
 * @return             (BOOL)                     != 0: bus is free; == 0: communication ongoing on bus
 */
BOOL _ttc_i2c_check_bus_free( volatile t_ttc_i2c_base_register* BaseRegister );

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions ***************************************************

t_u8                 ttc_i2c_get_max_index() {
    return TTC_I2C_AMOUNT;
}
t_ttc_i2c_config*    ttc_i2c_get_configuration( t_u8 LogicalIndex ) {
    Assert_I2C( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_i2c_config* Config = A( ttc_i2c_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        Config = A( ttc_i2c_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_i2c_config ) );
        Config->Init.Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_i2c_load_defaults( LogicalIndex );
    }

    return Config;
}
t_ttc_i2c_config*    ttc_i2c_get_features( t_u8 LogicalIndex ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    return _driver_i2c_get_features( Config );
}
void                 ttc_i2c_deinit( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration

    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );

    if ( Config->Init.Flags.Bits.Initialized ) {
        e_ttc_i2c_errorcode Result = _driver_i2c_deinit( Config );
        if ( Result == E_ttc_i2c_errorcode_OK )
        { Config->Init.Flags.Bits.Initialized = 0; }
    }
    ttc_task_critical_end(); // other tasks now may access this driver
}
e_ttc_i2c_errorcode  ttc_i2c_init( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );

    if ( 1 ) { // limit given configuration to features provided by current low-level driver
        t_ttc_i2c_config* Features = ttc_i2c_get_features( LogicalIndex );

        Config->Init.Flags.All &= Features->Init.Flags.All;
        if ( Config->Init.ClockSpeed > Features->Init.ClockSpeed )
        { Config->Init.ClockSpeed = Features->Init.ClockSpeed; };
    }

    Config->ErrorCount = 0;
    Config->LastError = _driver_i2c_init( Config );
    if ( Config->LastError == E_ttc_i2c_errorcode_OK )
    { Config->Init.Flags.Bits.Initialized = 1; }

    ttc_task_critical_end(); // other tasks now may access this driver

    return Config->LastError;
}
e_ttc_i2c_errorcode  ttc_i2c_load_defaults( t_u8 LogicalIndex ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );

    if ( Config->Init.Flags.Bits.Initialized )
    { ttc_i2c_deinit( LogicalIndex ); }

    ttc_memory_set( Config, 0, sizeof( t_ttc_i2c_config ) );

    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    switch ( LogicalIndex ) { // load type of low-level driver for current architecture
            #ifdef TTC_I2C1
        case  1: {
            Config->PhysicalIndex = TTC_I2C1;
            #ifdef TTC_I2C1_SMBAL
            Config->Init.Port_SCL      = TTC_I2C1_SCL;
            #endif
            #ifdef TTC_I2C1_SDA
            Config->Init.Port_SDA      = TTC_I2C1_SDA;
            #endif
            #ifdef TTC_I2C1_SMBAL
            Config->Init.Port_SMBALERT = TTC_I2C1_SMBAL;
            #endif
            break;
        }
        #endif
        #ifdef TTC_I2C2
        case  2: {
            Config->PhysicalIndex = TTC_I2C2;
            #ifdef TTC_I2C2_SMBAL
            Config->Init.Port_SCL      = TTC_I2C2_SCL;
            #endif
            #ifdef TTC_I2C2_SDA
            Config->Init.Port_SDA      = TTC_I2C2_SDA;
            #endif
            #ifdef TTC_I2C2_SMBAL
            Config->Init.Port_SMBALERT = TTC_I2C2_SMBAL;
            #endif
            break;
        }
        #endif
        #ifdef TTC_I2C3
        case  3: {
            Config->PhysicalIndex = TTC_I2C3;
            #ifdef TTC_I2C3_SMBAL
            Config->Init.Port_SCL      = TTC_I2C3_SCL;
            #endif
            #ifdef TTC_I2C3_SDA
            Config->Init.Port_SDA      = TTC_I2C3_SDA;
            #endif
            #ifdef TTC_I2C3_SMBAL
            Config->Init.Port_SMBALERT = TTC_I2C3_SMBAL;
            #endif
            break;
        }
        #endif
        #ifdef TTC_I2C4
        case  4: {
            Config->PhysicalIndex = TTC_I2C4;
            #ifdef TTC_I2C4_SMBAL
            Config->Init.Port_SCL      = TTC_I2C4_SCL;
            #endif
            #ifdef TTC_I2C4_SDA
            Config->Init.Port_SDA      = TTC_I2C4_SDA;
            #endif
            #ifdef TTC_I2C4_SMBAL
            Config->Init.Port_SMBALERT = TTC_I2C4_SMBAL;
            #endif
            break;
        }
        #endif
        #ifdef TTC_I2C5
        case  5: {
            Config->PhysicalIndex = TTC_I2C5;
            #ifdef TTC_I2C5_SMBAL
            Config->Init.Port_SCL      = TTC_I2C5_SCL;
            #endif
            #ifdef TTC_I2C5_SDA
            Config->Init.Port_SDA      = TTC_I2C5_SDA;
            #endif
            #ifdef TTC_I2C5_SMBAL
            Config->Init.Port_SMBALERT = TTC_I2C5_SMBAL;
            #endif
            break;
        }
        #endif
        #ifdef TTC_I2C6
        case  6: {
            Config->PhysicalIndex = TTC_I2C6;
            #ifdef TTC_I2C6_SMBAL
            Config->Init.Port_SCL      = TTC_I2C6_SCL;
            #endif
            #ifdef TTC_I2C6_SDA
            Config->Init.Port_SDA      = TTC_I2C6_SDA;
            #endif
            #ifdef TTC_I2C6_SMBAL
            Config->Init.Port_SMBALERT = TTC_I2C6_SMBAL;
            #endif
            break;
        }
        #endif
        #ifdef TTC_I2C7
        case  7: {
            Config->PhysicalIndex = TTC_I2C7;
            #ifdef TTC_I2C7_SMBAL
            Config->Init.Port_SCL      = TTC_I2C7_SCL;
            #endif
            #ifdef TTC_I2C7_SDA
            Config->Init.Port_SDA      = TTC_I2C7_SDA;
            #endif
            #ifdef TTC_I2C7_SMBAL
            Config->Init.Port_SMBALERT = TTC_I2C7_SMBAL;
            #endif
            break;
        }
        #endif
        #ifdef TTC_I2C8
        case  8: {
            Config->PhysicalIndex = TTC_I2C8;
            #ifdef TTC_I2C8_SMBAL
            Config->Init.Port_SCL      = TTC_I2C8_SCL;
            #endif
            #ifdef TTC_I2C8_SDA
            Config->Init.Port_SDA      = TTC_I2C8_SDA;
            #endif
            #ifdef TTC_I2C8_SMBAL
            Config->Init.Port_SMBALERT = TTC_I2C8_SMBAL;
            #endif
            break;
        }
        #endif
        #ifdef TTC_I2C9
        case  9: {
            Config->PhysicalIndex = TTC_I2C9;
            #ifdef TTC_I2C9_SMBAL
            Config->Init.Port_SCL      = TTC_I2C9_SCL;
            #endif
            #ifdef TTC_I2C9_SDA
            Config->Init.Port_SDA      = TTC_I2C9_SDA;
            #endif
            #ifdef TTC_I2C9_SMBAL
            Config->Init.Port_SMBALERT = TTC_I2C9_SMBAL;
            #endif
            break;
        }
        #endif
        #ifdef TTC_I2C10
        case  10: {
            Config->PhysicalIndex = TTC_I2C10;
            #ifdef TTC_I2C10_SMBAL
            Config->Init.Port_SCL      = TTC_I2C10_SCL;
            #endif
            #ifdef TTC_I2C10_SDA
            Config->Init.Port_SDA      = TTC_I2C10_SDA;
            #endif
            #ifdef TTC_I2C10_SMBAL
            Config->Init.Port_SMBALERT = TTC_I2C10_SMBAL;
            #endif
            break;
        }
        #endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid logical index or not egnough I2C devices configured. Check your application and your board-makefile!
    }

    // allocate memory for low-level driver (most low-level driver developers had difficulties to allocate it correctly)
    u_ttc_i2c_architecture* LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_ttc_i2c_architecture ) );
    Config->LowLevelConfig = LowLevelConfig;

    //Insert additional generic default values here ...
    Config->MaxRiseTime_ns = 1000;
    Config->Init.AmountRetries  = 10;
    Config->Init.TimeOut        = 20000;

    // let low-level driver fill remaining fields
    Config->LastError = _driver_i2c_load_defaults( Config );
    Assert_I2C_EXTRA( LowLevelConfig == Config->LowLevelConfig, ttc_assert_origin_auto ); // LowLevelConfig already allocated, low-level driver must not reallocate it!
    Assert_I2C_EXTRA( ( Config->Architecture > ta_i2c_None ) && ( Config->Architecture < ta_i2c_unknown ), ttc_assert_origin_auto ); // architecture not set properly! Low-Level driver must set this field!

    return Config->LastError;
}
void                 ttc_i2c_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)
    ttc_sysclock_register_for_update( ttc_i2c_sysclock_changed );

    _driver_i2c_prepare();
}
void                 ttc_i2c_reset( t_u8 LogicalIndex ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );

    _driver_i2c_reset( Config );
}
void                 ttc_i2c_sysclock_changed() {

    // deinit + reinit all initialized I2C devices
    for ( t_u8 LogicalIndex = 1; LogicalIndex < ttc_i2c_get_max_index(); LogicalIndex++ ) {
        t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
        if ( Config->Init.Flags.Bits.Initialized ) {
            ttc_i2c_deinit( LogicalIndex );
            ttc_i2c_init( LogicalIndex );
        }
    }
}
e_ttc_i2c_errorcode  ttc_i2c_master_write_register( t_u8 LogicalIndex, t_u16 SlaveAddress, t_u32 RegisterAddress, e_ttc_i2c_address_type AddressType, t_u8 AmountBytes, const t_u8* Buffer ) {
    Assert_I2C( ttc_memory_is_readable( Buffer ), ttc_assert_origin_auto ); // always check incoming pointer arguments
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C( Config->Init.Flags.Bits.Master == 1, ttc_assert_origin_auto ); // reinit device to master mode before calling this function!

    e_ttc_i2c_errorcode Error = E_ttc_i2c_errorcode_OK;

    if ( ! ttc_i2c_wait_until_event( LogicalIndex, E_ttc_i2c_event_code_bus_free ) )
        // Another master or a slave seem to block the bus: reset it
    { Error = ttc_i2c_master_reset_bus( LogicalIndex ); }

    if ( !Error ) // send start condition
    { Error = _ttc_i2c_master_handle_error( Config, _driver_i2c_master_condition_start( Config ) ); }

    if ( !Error ) // send slave address and switch to write mode
    { Error = _ttc_i2c_master_handle_error( Config, _driver_i2c_master_send_slave_address( Config, SlaveAddress, FALSE ) ); }

    if ( !Error ) // transmit register address
    { Error = _ttc_i2c_master_handle_error( Config, ttc_i2c_master_send_register_adddress( LogicalIndex, RegisterAddress, AddressType ) ); }

    if ( !Error ) // transmit data
    { Error = _ttc_i2c_master_handle_error( Config, _driver_i2c_master_send_bytes( Config, AmountBytes, Buffer ) ); }

    if ( !Error ) // end transmission
    { Error = _ttc_i2c_master_handle_error( Config, _driver_i2c_master_condition_stop( Config ) ); }

    return Error;
}
e_ttc_i2c_errorcode  ttc_i2c_master_read_register( t_u8 LogicalIndex, t_u16 SlaveAddress, t_u32 RegisterAddress, e_ttc_i2c_address_type AddressType, t_u8 AmountBytes, t_u8* Buffer ) {
    Assert_I2C( ttc_memory_is_writable( Buffer ), ttc_assert_origin_auto ); // always check incoming pointer arguments
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C( Config->Init.Flags.Bits.Master == 1, ttc_assert_origin_auto ); // reinit device to master mode before calling this function!

    e_ttc_i2c_errorcode Error = E_ttc_i2c_errorcode_OK;

    if ( ! ttc_i2c_wait_until_event( LogicalIndex, E_ttc_i2c_event_code_bus_free ) )
        // Another master or a slave seem to block the bus: reset it
    { Error = ttc_i2c_master_reset_bus( LogicalIndex ); }

    if ( !Error ) // send start condition
    { Error = _ttc_i2c_master_handle_error( Config, _driver_i2c_master_condition_start( Config ) ); }

    if ( !Error ) // send slave address and switch to write mode
    { Error = _ttc_i2c_master_handle_error( Config, _driver_i2c_master_send_slave_address( Config, SlaveAddress, FALSE ) ); }

    if ( !Error ) // transmit register address
    { Error = _ttc_i2c_master_handle_error( Config, ttc_i2c_master_send_register_adddress( LogicalIndex, RegisterAddress, AddressType ) ); }

    if ( !Error ) // send start condition second time
    { Error = _ttc_i2c_master_handle_error( Config, _driver_i2c_master_condition_start( Config ) ); }

    if ( AmountBytes > 1 )
    { ttc_i2c_enable_acknowledge( LogicalIndex, 1 ); } // make sure first received byte is acknowledged
    else
    { ttc_i2c_enable_acknowledge( LogicalIndex, 0 ); } // first byte is last byte which must not be acknowledged

    if ( !Error ) // send slave address and switch to read mode
    { Error = _ttc_i2c_master_handle_error( Config, _driver_i2c_master_send_slave_address( Config, SlaveAddress, TRUE ) ); }

    if ( !Error ) // read data from register
        //X Error = _ttc_i2c_master_handle_error( Config, _driver_i2c_master_read_bytes(Config, AmountBytes, Buffer) );
    { Error = ttc_i2c_master_read_bytes( LogicalIndex, AmountBytes, Buffer ); }

    if ( !Error ) // end transmission
    { Error = _ttc_i2c_master_handle_error( Config, _driver_i2c_master_condition_stop( Config ) ); }

    return Error;
}
e_ttc_i2c_errorcode  ttc_i2c_master_read_bytes( t_u8 LogicalIndex, t_u8 AmountBytes, t_u8* Buffer ) {
    Assert( ttc_memory_is_writable( Buffer ), ttc_assert_origin_auto ); // always check incoming pointer arguments
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C( Config->Init.Flags.Bits.Master == 1, ttc_assert_origin_auto ); // reinit device to master mode before calling this function!

    if ( AmountBytes > 1 )
    { ttc_i2c_enable_acknowledge( LogicalIndex, 1 ); } // make sure first received byte is acknowledged
    else
    { ttc_i2c_enable_acknowledge( LogicalIndex, 0 ); } // first byte is last byte which must not be acknowledged

    e_ttc_i2c_errorcode Error = 0;
    //?Required Error = _ttc_i2c_master_handle_error( Config, _driver_i2c_master_condition_stop(Config) );


    if ( !Error ) {
        ttc_task_critical_begin();
        Error = _ttc_i2c_master_handle_error( Config, _driver_i2c_master_read_bytes( Config, AmountBytes, Buffer ) );
        ttc_task_critical_end();
    }

    //X if (!Error)
    //X     Error = _ttc_i2c_master_handle_error( Config, _driver_i2c_master_condition_stop(Config) );

    return Error;
}
e_ttc_i2c_errorcode  ttc_i2c_master_send_bytes( t_u8 LogicalIndex, t_u8 AmountBytes, const t_u8* Buffer ) {
    Assert( ttc_memory_is_readable( Buffer ), ttc_assert_origin_auto ); // always check incoming pointer arguments
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C( Config->Init.Flags.Bits.Master == 1, ttc_assert_origin_auto ); // reinit device to master mode before calling this function!

    return _ttc_i2c_master_handle_error( Config, _driver_i2c_master_send_bytes( Config, AmountBytes, Buffer ) );
}
e_ttc_i2c_errorcode  ttc_i2c_master_condition_start( t_u8 LogicalIndex ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C( Config->Init.Flags.Bits.Master == 1, ttc_assert_origin_auto ); // reinit device to master mode before calling this function!

    return _ttc_i2c_master_handle_error( Config, _driver_i2c_master_condition_start( Config ) );
}
e_ttc_i2c_errorcode  ttc_i2c_master_condition_stop( t_u8 LogicalIndex ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C( Config->Init.Flags.Bits.Master == 1, ttc_assert_origin_auto ); // reinit device to master mode before calling this function!

    return _ttc_i2c_master_handle_error( Config, _driver_i2c_master_condition_stop( Config ) );
}
e_ttc_i2c_errorcode  ttc_i2c_master_reset_bus( t_u8 LogicalIndex ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C( Config->Init.Flags.Bits.Master == 1, ttc_assert_origin_auto ); // reinit device to master mode before calling this function!

    ttc_i2c_deinit( LogicalIndex );

    if ( 1 ) { // send out clock cycles to reset all slaves
        ttc_gpio_init( Config->Init.Port_SCL, E_ttc_gpio_mode_output_open_drain, E_ttc_gpio_speed_max );

        for ( t_u8 AmountBytes = 100; AmountBytes > 0; AmountBytes-- ) {
            ttc_gpio_clr( Config->Init.Port_SCL );
            ttc_task_usleep( 30 );
            ttc_gpio_set( Config->Init.Port_SCL );
            ttc_task_usleep( 30 );
        }

        ttc_gpio_init( Config->Init.Port_SCL, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_max );
    }

    ttc_i2c_init( LogicalIndex );

    return E_ttc_i2c_errorcode_OK;
}
e_ttc_i2c_errorcode  ttc_i2c_master_send_slave_address( t_u8 LogicalIndex, t_u16 SlaveAddress, BOOL ReadMode ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C( Config->Init.Flags.Bits.Master == 1, ttc_assert_origin_auto ); // reinit device to master mode before calling this function!

    return _ttc_i2c_master_handle_error( Config, _driver_i2c_master_send_slave_address( Config, SlaveAddress, ReadMode ) );
}
e_ttc_i2c_errorcode  ttc_i2c_master_send_register_adddress( t_u8 LogicalIndex, t_u32 RegisterAddress, e_ttc_i2c_address_type AddressType ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C( Config->Init.Flags.Bits.Master == 1, ttc_assert_origin_auto ); // reinit device to master mode before calling this function!

    t_u8 AmountBytes = 0;
    t_u8 Buffer[4];

    switch ( AddressType ) { // send register adress (MSB first)

        case E_ttc_i2c_address_type_MSB_First_32Bit: {
            Buffer[0] = ( t_u8 )( RegisterAddress & 0xff000000 ) >> 24;
            Buffer[1] = ( t_u8 )( RegisterAddress & 0x00ff0000 ) >> 16;
            Buffer[2] = ( t_u8 )( RegisterAddress & 0x0000ff00 ) >> 8;
            Buffer[3] = ( t_u8 ) RegisterAddress & 0x000000ff;
            AmountBytes = 4;
            break;
        }
        case E_ttc_i2c_address_type_MSB_First_24Bit: {
            Buffer[0] = ( t_u8 )( RegisterAddress & 0x00ff0000 ) >> 16;
            Buffer[1] = ( t_u8 )( RegisterAddress & 0x0000ff00 ) >> 8;
            Buffer[2] = ( t_u8 ) RegisterAddress & 0x000000ff;
            AmountBytes = 3;
            break;
        }
        case E_ttc_i2c_address_type_MSB_First_16Bit: {
            Buffer[0] = ( t_u8 )( RegisterAddress & 0x0000ff00 ) >> 8;
            Buffer[1] = ( t_u8 ) RegisterAddress & 0x000000ff;
            AmountBytes = 2;
            break;
        }
        case E_ttc_i2c_address_type_MSB_First_8Bit: {
            Buffer[0] = ( t_u8 ) RegisterAddress & 0x000000ff;
            AmountBytes = 1;
            break;
        }
        case E_ttc_i2c_address_type_LSB_First_32Bit: {
            Buffer[0] = ( t_u8 ) RegisterAddress & 0x000000ff;
            Buffer[1] = ( t_u8 )( RegisterAddress & 0x0000ff00 ) >> 8;
            Buffer[2] = ( t_u8 )( RegisterAddress & 0x00ff0000 ) >> 16;
            Buffer[3] = ( t_u8 )( RegisterAddress & 0xff000000 ) >> 24;
            AmountBytes = 4;
            break;
        }
        case E_ttc_i2c_address_type_LSB_First_24Bit: {
            Buffer[0] = ( t_u8 ) RegisterAddress & 0x000000ff;
            Buffer[1] = ( t_u8 )( RegisterAddress & 0x0000ff00 ) >> 8;
            Buffer[2] = ( t_u8 )( RegisterAddress & 0x00ff0000 ) >> 16;
            AmountBytes = 3;
            break;
        }
        case E_ttc_i2c_address_type_LSB_First_16Bit: {
            Buffer[0] = ( t_u8 ) RegisterAddress & 0x000000ff;
            Buffer[1] = ( t_u8 )( RegisterAddress & 0x0000ff00 ) >> 8;
            AmountBytes = 2;
            break;
        }
        case E_ttc_i2c_address_type_LSB_First_8Bit: {
            Buffer[0] = ( t_u8 ) RegisterAddress & 0x000000ff;
            AmountBytes = 1;
            break;
        }
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); // unknown register type!
    }

    return _ttc_i2c_master_handle_error( Config, _driver_i2c_master_send_bytes( Config, AmountBytes, Buffer ) );
}
BOOL                 ttc_i2c_slave_check_own_address_received( t_u8 LogicalIndex ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config->BaseRegister ), ttc_assert_origin_auto ); // maybe corrupted memory?
    Assert_I2C( Config->Init.Flags.Bits.Slave, ttc_assert_origin_auto ); // enable slave mode before calling ttc_i2c_init()!

    //X    return _driver_i2c_check_event_slave_receiver_address_matched( _driver_i2c_get_event_value(Config->BaseRegister) );
    return i2c_common_check_event( _driver_i2c_get_event_value( Config->BaseRegister ), E_ttc_i2c_event_code_slave_receiver_address_matched );
}
BOOL                 ttc_i2c_slave_check_read_mode( t_u8 LogicalIndex ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config->BaseRegister ), ttc_assert_origin_auto ); // maybe corrupted memory?
    Assert_I2C( Config->Init.Flags.Bits.Slave, ttc_assert_origin_auto ); // enable slave mode before calling ttc_i2c_init()!

    //X return _driver_i2c_slave_check_read_mode(Config->BaseRegister);
    return _driver_i2c_check_flag( Config->BaseRegister, E_ttc_i2c_flag_code_read_mode_detected );
}
t_u8                 ttc_i2c_slave_read_bytes( t_u8 LogicalIndex, t_u8 BufferSize, t_u8* Buffer ) {
    Assert( ttc_memory_is_writable( Buffer ), ttc_assert_origin_auto ); // always check incoming pointer arguments
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config->BaseRegister ), ttc_assert_origin_auto ); // maybe corrupted memory?
    Assert_I2C( Config->Init.Flags.Bits.Slave, ttc_assert_origin_auto ); // enable slave mode before calling ttc_i2c_init()!

    ttc_task_critical_begin();
    t_u8 AmountRead = _driver_i2c_slave_read_bytes( Config, BufferSize, Buffer );
    ttc_task_critical_end();
    if ( !AmountRead ) {
        ttc_i2c_slave_reset_bus( LogicalIndex ); // make sure, that lines are released
        Config->ErrorCount++;
    }

    return AmountRead;
}
t_u8                 ttc_i2c_slave_send_bytes( t_u8 LogicalIndex, t_u8 BufferSize, const t_u8* Buffer ) {
    Assert( ttc_memory_is_readable( Buffer ), ttc_assert_origin_auto ); // always check incoming pointer arguments
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config->BaseRegister ), ttc_assert_origin_auto ); // maybe corrupted memory?
    Assert_I2C( Config->Init.Flags.Bits.Slave, ttc_assert_origin_auto ); // enable slave mode before calling ttc_i2c_init()!

    ttc_i2c_enable_acknowledge( LogicalIndex, 0 ); // disable acknowledgments
    t_u8 AmountSent = _driver_i2c_slave_send_bytes( Config, BufferSize, Buffer );
    ttc_i2c_enable_acknowledge( LogicalIndex, 1 ); // re-enable acknowledgments
    if ( !AmountSent ) {
        ttc_i2c_slave_reset_bus( LogicalIndex ); // make sure, that lines are released
        Config->ErrorCount++;
    }
    if ( ! ttc_i2c_wait_until_event( LogicalIndex, E_ttc_i2c_event_code_bus_free ) )
    { ttc_i2c_slave_reset_bus( LogicalIndex ); }

    if ( _driver_i2c_check_flag( Config->BaseRegister, E_ttc_i2c_flag_code_bus_error ) )
    { _ttc_i2c_slave_handle_error( Config, E_ttc_i2c_errorcode_BusError ); }

    return AmountSent;
}
e_ttc_i2c_errorcode  ttc_i2c_slave_reset_bus( t_u8 LogicalIndex ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );

    _driver_i2c_slave_reset_bus( Config );
    return  ttc_i2c_init( LogicalIndex );
}
BOOL                 ttc_i2c_check_flag( t_u8 LogicalIndex, e_ttc_i2c_flag_code FlagCode ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config->BaseRegister ), ttc_assert_origin_auto ); // maybe corrupted memory?

    return _driver_i2c_check_flag( Config->BaseRegister, FlagCode );
}
void                 ttc_i2c_enable_acknowledge( t_u8 LogicalIndex, BOOL Enable ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config->BaseRegister ), ttc_assert_origin_auto ); // maybe corrupted memory?

    _driver_i2c_enable_acknowledge( Config->BaseRegister, Enable );
}
e_ttc_i2c_event_code ttc_i2c_wait_until_event( t_u8 LogicalIndex, e_ttc_i2c_event_code EventCode ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );

    return i2c_common_wait_until_event( EventCode,
                                        Config->BaseRegister,
                                        Config->Init.TimeOut,
                                        Config->Init.AmountRetries
                                      );
}
e_ttc_i2c_event_code ttc_i2c_wait_until_events( t_u8 LogicalIndex, const e_ttc_i2c_event_code EventCodes[] ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );

    return i2c_common_wait_until_events( EventCodes,
                                         Config->BaseRegister,
                                         Config->Init.TimeOut,
                                         Config->Init.AmountRetries
                                       );
}
t_base               ttc_i2c_get_event_value( t_u8 LogicalIndex ) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration( LogicalIndex );
    Assert_I2C_EXTRA( ttc_memory_is_writable( Config->BaseRegister ), ttc_assert_origin_auto ); // maybe corrupted memory?

    return _driver_i2c_get_event_value( Config->BaseRegister );
}
#ifndef ttc_i2c_check_event
BOOL                 ttc_i2c_check_event( t_base EventValue, e_ttc_i2c_event_code EventCode ) {

    return i2c_common_check_event( EventValue, EventCode );
}
#endif
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_i2c(t_u8 LogicalIndex) {  }

e_ttc_i2c_errorcode _ttc_i2c_master_handle_error( t_ttc_i2c_config* Config, e_ttc_i2c_errorcode Error ) {

    if ( Error ) {
        ttc_i2c_master_reset_bus( Config->LogicalIndex );
        Config->LastError = Error;
        Config->ErrorCount++;
    }
    return Error; // e_ttc_i2c_errorcode
}
e_ttc_i2c_errorcode _ttc_i2c_slave_handle_error( t_ttc_i2c_config* Config, e_ttc_i2c_errorcode Error ) {

    if ( !Error ) { // check error status flags
        if ( _driver_i2c_check_flag( Config->BaseRegister, E_ttc_i2c_flag_code_bus_error ) )
        { Error = E_ttc_i2c_errorcode_BusError; }
    }
    if ( Error ) {
        ttc_i2c_slave_reset_bus( Config->LogicalIndex );
        Config->LastError = Error;
        Config->ErrorCount++;
    }
    return Error; // e_ttc_i2c_errorcode
}

/* DEPECATED
BOOL                _ttc_i2c_check_event_bus_not_busy(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_bus_not_busy(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_master_slave_has_acknowledged_start(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_master_slave_has_acknowledged_start(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_master_transmitter_mode_selected(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_master_transmitter_mode_selected(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_master_receiver_mode_selected(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_master_receiver_mode_selected(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_master_byte_transmitted(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_master_byte_transmitted(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_master_byte_received(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_master_byte_received(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_master_byte_transmitting(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_master_byte_transmitting(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_slave_receiver_address_matched(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_slave_receiver_address_matched(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_slave_receiver_second_address_matched(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_slave_receiver_second_address_matched(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_slave_transmitter_address_matched(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_slave_transmitter_address_matched(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_slave_transmitter_second_address_matched(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_slave_transmitter_second_address_matched(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_slave_general_call_address_matched(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_slave_general_call_address_matched(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_slave_byte_received(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_slave_byte_received(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_slave_stop_detected(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_slave_stop_detected(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_slave_byte_transmitted(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_slave_byte_transmitted(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_slave_byte_transmitting(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_slave_byte_transmitting(_driver_i2c_get_event_value(Config->BaseRegister));
}
BOOL                _ttc_i2c_check_event_slave_ack_failure(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    return _driver_i2c_check_event_slave_ack_failure(_driver_i2c_get_event_value(Config->BaseRegister));
}


BOOL                _ttc_i2c_check_flag_master_mode(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    //X return _driver_i2c_check_flag_master_mode(Config->BaseRegister);
    return _driver_i2c_check_flag(Config->BaseRegister, E_ttc_i2c_flag_code_master_mode);
}
BOOL                _ttc_i2c_check_flag_smb_host_header_received(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    //X return _driver_i2c_check_flag_smb_host_header_received(Config->BaseRegister);
    return _driver_i2c_check_flag(Config->BaseRegister, E_ttc_i2c_flag_code_smbus_host_header_received);
}
BOOL                _ttc_i2c_check_flag_smb_default_address_received(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    //X return _driver_i2c_check_flag_smb_default_address_received(Config->BaseRegister);
    return _driver_i2c_check_flag(Config->BaseRegister, E_ttc_i2c_flag_code_smbus_default_address_received);
}
BOOL                _ttc_i2c_check_flag_general_call_received(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    //X return _driver_i2c_check_flag_general_call_received(Config->BaseRegister);
    return _driver_i2c_check_flag(Config->BaseRegister, E_ttc_i2c_flag_code_smbus_general_call_address_received);
}
BOOL                _ttc_i2c_check_flag_bytes_transmitted(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    //X return _driver_i2c_check_flag_bytes_transmitted(Config->BaseRegister);
    return _driver_i2c_check_flag(Config->BaseRegister, E_ttc_i2c_flag_code_byte_transfer_finished);
}
BOOL                _ttc_i2c_check_flag_bytes_received(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    //X return _driver_i2c_check_flag_bytes_received(Config->BaseRegister);
    return _driver_i2c_check_flag(Config->BaseRegister, E_ttc_i2c_flag_code_receive_buffer_not_empty);
}
BOOL                _ttc_i2c_check_flag_error_misplaced_condition(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    //X return _driver_i2c_check_flag_error_misplaced_condition(Config->BaseRegister);
    return _driver_i2c_check_flag(Config->BaseRegister, E_ttc_i2c_flag_code_bus_error);
}
BOOL                _ttc_i2c_check_flag_error_arbitration_lost(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    //X return _driver_i2c_check_flag_error_arbitration_lost(Config->BaseRegister);
    return _driver_i2c_check_flag(Config->BaseRegister, E_ttc_i2c_flag_code_bus_error);
}
BOOL                _ttc_i2c_check_flag_error_no_acknowledge(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

//X    return _driver_i2c_check_flag_error_no_acknowledge(Config->BaseRegister);
    return _driver_i2c_check_flag(Config->BaseRegister, E_ttc_i2c_flag_code_acknowledge_failure);
}
BOOL                _ttc_i2c_check_flag_error_parity(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    //X return _driver_i2c_check_flag_error_parity(Config->BaseRegister);
    return _driver_i2c_check_flag(Config->BaseRegister, E_ttc_i2c_flag_code_crc_error);
}
BOOL                _ttc_i2c_check_flag_error_slave_buffer_overrun(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    //X return _driver_i2c_check_flag_error_slave_buffer_overrun(Config->BaseRegister);
    return _driver_i2c_check_flag(Config->BaseRegister, E_ttc_i2c_flag_code_buffer_overrun);
}
BOOL                _ttc_i2c_check_flag_smb_error_timeout(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    //X return _driver_i2c_check_flag_smb_error_timeout(Config->BaseRegister);
    return _driver_i2c_check_flag(Config->BaseRegister, E_ttc_i2c_flag_code_smbus_timeout);
}
BOOL                _ttc_i2c_check_flag_bus_is_busy(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    //X return _driver_i2c_check_flag_bus_is_busy(Config->BaseRegister);
    return _driver_i2c_check_flag(Config->BaseRegister, E_ttc_i2c_flag_code_bus_busy);
}
BOOL                _ttc_i2c_check_flag_transmit_buffer_empty(t_u8 LogicalIndex) {
    t_ttc_i2c_config* Config = ttc_i2c_get_configuration(LogicalIndex);
    Assert_I2C_EXTRA(ttc_memory_is_writable(Config->BaseRegister), ttc_assert_origin_auto); // maybe corrupted memory?

    //X return _driver_i2c_check_flag_transmit_buffer_empty(Config->BaseRegister);
    return _driver_i2c_check_flag(Config->BaseRegister, E_ttc_i2c_flag_code_transmit_buffer_empty);
}
BOOL                _ttc_i2c_check_bus_free(volatile t_ttc_i2c_base_register* BaseRegister) {

    // DEPRECATED
    // if (_driver_i2c_check_flag_bus_is_busy(BaseRegister))
    //    return FALSE;
    //
    // return TRUE;

    if ( _driver_i2c_check_flag(BaseRegister, E_ttc_i2c_flag_code_bus_busy) )
        return FALSE;

    return TRUE;
}
*/
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
