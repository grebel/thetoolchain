/** { ttc_memory.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for memory devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 23 at 20140228 13:50:05 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_memory.h"
#include "ttc_list.h"

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of memory devices.
 *
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)
// global variables loaded with addresses of corresponding extern symbols above
volatile t_u8* ttc_memory_Address_BSS_Start  = 0; // start of .bss section as defined in linker script
volatile t_u8* ttc_memory_Address_BSS_End    = 0; // end   of .bss section as defined in linker script
volatile t_u8* ttc_memory_Address_Data_Start = 0; // start of .data section as defined in linker script
volatile t_u8* ttc_memory_Address_Data_End   = 0; // end   of .data section as defined in linker script
volatile t_u8* ttc_memory_Address_Data_Init  = 0; // start of initialization of .data section as defined in linker script
volatile t_u8* ttc_memory_MainStack_Start    = 0; // first valid address of main stack (set by ttc_task_prepare())
volatile t_u8* ttc_memory_MainStack_End      = 0; // last valid address of main stack (set by ttc_task_prepare())

// these symbols have to be defined in linker script (ttc-lib/_linker/memory_*.ld or configs/memory:*.ld)
extern volatile t_u8 _ttc_memory_stack_start; // start of main stack
extern volatile t_u8 _ttc_memory_stack_end;   // end of main stack
extern volatile t_u8 _sbss;                   // start of .bss section
extern volatile t_u8 _ebss;                   // end   of .bss section
extern volatile t_u8 _sdata;                  // start of .data section
extern volatile t_u8 _edata;                  // end   of .data section
extern volatile t_u8 _sidata;                 // start of initialization of .data section

//} Global Variables
//{ Function definitions ***************************************************

void ttc_memory_prepare() {
    // add your startup code here (Singletasking!)

    // load memory addresses of sections and stack as defined in ttc-lib/_linker/memory_*.ld or configs/memory:*.ld
    ttc_memory_Address_BSS_Start  = &_sbss;
    ttc_memory_Address_BSS_End    = &_ebss;
    ttc_memory_Address_Data_Start = &_sdata;
    ttc_memory_Address_Data_End   = &_edata;
    ttc_memory_Address_Data_Init  = &_sidata;
    ttc_memory_MainStack_Start    = &_ttc_memory_stack_start;
    ttc_memory_MainStack_End      = &_ttc_memory_stack_end;

    // check if linker script has defined adresses inside RAM
    Assert_MEMORY_Writable( ( void* ) ttc_memory_Address_BSS_Start,  ttc_assert_origin_auto ); // should be start address of the .bss section
    Assert_MEMORY_Writable( ( void* ) ttc_memory_Address_BSS_Start,  ttc_assert_origin_auto ); // should be end address of the .bss section
    Assert_MEMORY_Writable( ( void* ) ttc_memory_Address_Data_Start, ttc_assert_origin_auto ); // should be start address of the .data section
    Assert_MEMORY_Writable( ( void* ) ttc_memory_Address_Data_End,   ttc_assert_origin_auto ); // should be end address of the .data section
    Assert_MEMORY_Readable( ( void* ) ttc_memory_Address_Data_Init,  ttc_assert_origin_auto ); // should be start address of the initialization values of the .data section which typically lies in ROM or FLASH
    Assert_MEMORY_Writable( ( void* ) ttc_memory_MainStack_Start,   ttc_assert_origin_auto );  // does your memory_*.ld provide _ttc_memory_stack_start? Maybe your project is outdated. Create a new TTC-project and copy the contents of config/ into this project!
    Assert_MEMORY_Writable( ( void* ) ttc_memory_MainStack_End,     ttc_assert_origin_auto );   // does your memory_*.ld provide _ttc_memory_stack_end?   Maybe your project is outdated. Create a new TTC-project and copy the contents of config/ into this project!

}
void ttc_memory_copy( void* Destination, const void* Source, t_base AmountBytes ) {
    Assert_MEMORY_Writable( Destination, ttc_assert_origin_auto );
    Assert_MEMORY_Readable( Source, ttc_assert_origin_auto );

#if (TARGET_DATA_WIDTH == 32) // make use of 32-bit registers to speed up copying
    while ( AmountBytes > 3 ) {
        *( ( t_u32* ) Destination ) = *( ( t_u32* ) Source );
        Destination = ( void* )( ( ( t_u32 ) Destination ) + 4 );
        Source      = ( void* )( ( ( t_u32 ) Source )      + 4 );
        AmountBytes -= 4;
    }
#else

#if (TARGET_DATA_WIDTH == 16) // make use of 16-bit registers to speed up copying
    while ( AmountBytes > 1 ) {
        *( ( t_u16* ) Destination ) = *( ( t_u16* ) Source );
        Destination = ( void* )( ( ( t_u32 ) Destination ) + 2 );
        Source      = ( void* )( ( ( t_u32 ) Source )      + 2 );
        AmountBytes -= 2;

    }
#endif

#endif

    while ( AmountBytes-- > 0 ) {
        *( ( t_u8* ) Destination ) = *( ( t_u8* ) Source );
        Destination = ( void* )( ( ( t_u32 ) Destination ) + 1 );
        Source      = ( void* )( ( ( t_u32 ) Source )      + 1 );
    }
}
void ttc_memory_set_byte( void* Destination, t_u8 Fill, t_base AmountBytes ) {

#if (TARGET_DATA_WIDTH == 32) // make use of 32-bit registers to speed up copying
    if ( AmountBytes > 31 ) {
        while ( ( ( t_base ) Destination ) & 0x3 ) { // move to next 4-byte aligned address
            *( ( t_u8* ) Destination ) = Fill;
            Destination = ( void* )( ( ( t_u8* ) Destination ) + 1 );
            AmountBytes--;
        }
        t_u32 Fill32 = Fill | ( Fill << 8 ) | ( Fill << 16 )  | ( Fill << 24 );
        while ( AmountBytes > 15 ) { // fill 16 bytes in each iteration
            *( ( t_u32* ) Destination ) = Fill32;
            Destination = ( void* )( ( ( t_u32 ) Destination ) + 4 );
            *( ( t_u32* ) Destination ) = Fill32;
            Destination = ( void* )( ( ( t_u32 ) Destination ) + 4 );
            *( ( t_u32* ) Destination ) = Fill32;
            Destination = ( void* )( ( ( t_u32 ) Destination ) + 4 );
            *( ( t_u32* ) Destination ) = Fill32;
            Destination = ( void* )( ( ( t_u32 ) Destination ) + 4 );
            AmountBytes -= 16;
        }
        while ( AmountBytes > 3 ) { // fill 4 bytes in each iteration
            *( ( t_u32* ) Destination ) = Fill32;
            Destination = ( void* )( ( ( t_u32 ) Destination ) + 4 );
            AmountBytes -= 4;
        }
    }
#else
#if (TARGET_DATA_WIDTH == 16) // make use of 16-bit registers to speed up copying

    if ( AmountBytes > 9 ) {
        t_u16 Fill16 = Fill & ( Fill << 8 );
        while ( AmountBytes > 1 ) {
            *( ( t_u16* ) Destination ) = Fill16;
            Destination = ( void* )( ( ( t_u32 ) Destination ) + 2 );
            AmountBytes -= 2;
        }
    }

#endif
#endif

    while ( AmountBytes-- > 0 ) {
        *( ( t_u8* ) Destination ) = Fill;
        Destination = ( void* )( ( ( t_u32 ) Destination ) + 1 );
    }
}
void ttc_memory_set( void* Destination, t_base Fill, t_base AmountBytes ) {
    Assert_MEMORY_Writable( Destination, ttc_assert_origin_auto );

#if (TARGET_DATA_WIDTH == 32) // make use of 32-bit registers to speed up copying
    if ( AmountBytes > 11 ) {
        while ( ( ( t_base ) Destination ) & 0x3 ) { // move to next 4-byte aligned address
            *( ( t_u8* ) Destination ) = ( t_u8 )( Fill & 0xff );
            Destination = ( void* )( ( ( t_u8* ) Destination ) + 1 );
            AmountBytes--;
        }
        while ( AmountBytes > 15 ) { // fill 16 bytes in each iteration
            *( ( t_u32* ) Destination ) = Fill;
            Destination = ( void* )( ( ( t_u32 ) Destination ) + 4 );
            *( ( t_u32* ) Destination ) = Fill;
            Destination = ( void* )( ( ( t_u32 ) Destination ) + 4 );
            *( ( t_u32* ) Destination ) = Fill;
            Destination = ( void* )( ( ( t_u32 ) Destination ) + 4 );
            *( ( t_u32* ) Destination ) = Fill;
            Destination = ( void* )( ( ( t_u32 ) Destination ) + 4 );
            AmountBytes -= 16;
        }
        while ( AmountBytes > 3 ) { // fill 4 bytes in each iteration
            *( ( t_u32* ) Destination ) = Fill;
            Destination = ( void* )( ( ( t_u32 ) Destination ) + 4 );
            AmountBytes -= 4;
        }
    }
#else
#if (TARGET_DATA_WIDTH == 16) // make use of 16-bit registers to speed up copying

    if ( AmountBytes > 5 ) {
        while ( AmountBytes > 1 ) {
            *( ( t_u16* ) Destination ) = ( t_u16 ) Fill );
            Destination = ( void* )( ( ( t_u32 ) Destination ) + 2 );
            AmountBytes -= 2;

        }
    }

#endif
#endif

    while ( AmountBytes-- > 0 ) {
        *( ( t_u8* ) Destination ) = ( t_u8 ) Fill;
        Destination = ( void* )( ( ( t_u32 ) Destination ) + 1 );
    }

    return ;
}
t_base ttc_memory_compare( const void* A, const void* B, t_base AmountBytes ) {
    t_base Index = 0;

#if (TARGET_DATA_WIDTH == 32) // make use of 32-bit registers to speed up copying
    if ( AmountBytes > 11 ) {
        while ( AmountBytes > 3 ) {
            if ( *( ( t_u32* ) A ) != *( ( t_u32* ) B ) ) // words differ: will determine exact position later
            { break; }
            Index += 4;
            A += 4;
            B += 4;
            AmountBytes -= 4;
        }
    }
#endif
#if (TARGET_DATA_WIDTH == 16) // make use of 16-bit registers to speed up copying
    if ( AmountBytes > 11 ) {
        while ( AmountBytes > 3 ) {
            if ( *( ( t_u32* ) A ) != *( ( t_u32* ) B ) ) // words differ: will determine exact position later
            { break; }
            Index += 2;
            A += 2;
            B += 2;
            AmountBytes -= 2;
        }
    }
#endif

    while ( AmountBytes-- > 0 ) {
        if ( *( ( t_u8* ) A ) != *( ( t_u8* ) B ) )
        { return Index; }
        A++;
        B++;
        Index++;
    }

    return 0;  // all checked bytes are the same
}


//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_memory(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
