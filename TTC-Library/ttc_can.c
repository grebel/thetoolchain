/** { ttc_can.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for can devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 23 at 20140322 11:01:08 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_can.h"
#include "./interfaces/ttc_can_interface.h"

#if TTC_CAN_AMOUNT == 0
#warning No CAN devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of can devices.
 *
 */

// for each initialized device, a pointer to its generic and stm32f1xx definitions is stored
A_define( t_ttc_can_config*, ttc_can_configs, TTC_CAN_AMOUNT );

// logical index of CAN to use by ttc_can_stdout_send()
t_u8   ttc_can_stdout_index = 0;
t_base ttc_can_Timeout      = 200;     // > 0: every transaction must finish within this amount of time (task-switches/ msecs)
// = 0: return immediately if transaction cannot be done right now (Note: untested!)
// =-1: no timeouts (wait indefinitely)
//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8 ttc_can_get_max_index() {
    return TTC_CAN_AMOUNT;
}
t_ttc_can_config* ttc_can_get_configuration( t_u8 LogicalIndex ) {
    Assert_CAN( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_can_config* Config = A( ttc_can_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        Config = A( ttc_can_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_can_config ) );
        Config->Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_can_load_defaults( LogicalIndex );
    }

    return Config;
}
void ttc_can_deinit( t_u8 LogicalIndex ) {
    Assert_CAN( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_can_config* Config = ttc_can_get_configuration( LogicalIndex );

    e_ttc_can_errorcode Result = _driver_can_deinit( Config );
    if ( Result == ec_can_OK )
    { Config->Flags.Bits.Initialized = 0; }
}
e_ttc_can_errorcode ttc_can_init( t_u8 LogicalIndex ) {
    Assert_CAN( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_can_config* Config = ttc_can_get_configuration( LogicalIndex );

    //CAN Configuration should be here ...
    e_ttc_can_errorcode Result = _driver_can_init( Config );
    if ( Result == ec_can_OK ) {
        Config->Flags.Bits.Initialized = 1;
        Config->Interrupt_FIFO0 = ttc_interrupt_init( tit_CAN_FIFO0_Pending, Config->PhysicalIndex, _driver_can_rx_isr, ( void* )Config, NULL, NULL );
        ttc_interrupt_enable_can( Config->Interrupt_FIFO0 );
        Config->Interrupt_FIFO1 = ttc_interrupt_init( tit_CAN_FIFO1_Pending, Config->PhysicalIndex, _driver_can_rx_isr, ( void* )Config, NULL, NULL );
        ttc_interrupt_enable_can( Config->Interrupt_FIFO1 );

        #ifndef STM32F10X_CL
        for ( int i = 0; i < 14; i++ )
        #else
        for ( int i = 0; i < 28; i++ )
        #endif
        {
            Config->CAN_Filter[i].IdHigh = 0;
            Config->CAN_Filter[i].IdLow = 0;
            Config->CAN_Filter[i].MaskIdHigh = 0;
            Config->CAN_Filter[i].MaskIdLow = 0;
            Config->CAN_Filter[i].Flags.All = 0;
        }
    }

    return Result;
}
void ttc_can_load_defaults( t_u8 LogicalIndex ) {
    Assert_CAN( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_can_config* Config = ttc_can_get_configuration( LogicalIndex );

    if ( Config->Flags.Bits.Initialized )
    { ttc_can_deinit( LogicalIndex ); }

    ttc_memory_set( Config, 0, sizeof( t_ttc_can_config ) );

    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    Config->PhysicalIndex = ttc_can_logical_2_physical_index( LogicalIndex );
    Config->LowLevelConfig = &Config->CAN_Arch;

    //Insert additional generic default values here ...

    _driver_can_load_defaults( Config );
}
t_u8 ttc_can_logical_2_physical_index( t_u8 LogicalIndex ) {

    switch ( LogicalIndex ) {         // determine base register and other low-level configuration data
            #ifdef TTC_CAN1
        case 1: return TTC_CAN1;
            #endif
            #ifdef TTC_CAN2
        case 2: return TTC_CAN2;
            #endif
        // extend as required

        default: break;
    }

    Assert_CAN( 0, ttc_assert_origin_auto ); // No TTC_CANn defined! Check your makefile.100_board_* file!
    return -1; // program flow should never go here
}
t_u8 ttc_can_physical_2_logical_index( t_u8 PhysicalIndex ) {

    switch ( PhysicalIndex ) {         // determine base register and other low-level configuration data
            #ifdef TTC_CAN1
        case TTC_CAN1: return 1;
            #endif
            #ifdef TTC_CAN2
        case TTC_CAN2: return 2;
            #endif
        // extend as required

        default: break;
    }

    Assert_CAN( 0, ttc_assert_origin_auto ); // No TTC_CANn defined! Check your makefile.100_board_* file!
    return -1; // program flow should never go here
}
void ttc_can_prepare() {
    // add your startup code here (Singletasking!)
    _driver_can_prepare();
}
e_ttc_can_errorcode ttc_can_reset( t_u8 LogicalIndex ) {
    Assert_CAN( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_can_config* Config = ttc_can_get_configuration( LogicalIndex );

    return _driver_can_reset( Config->LowLevelConfig );
}
e_ttc_can_errorcode ttc_can_receive( t_u8 LogicalIndex, void* Argument ) {
    Assert_CAN( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_can_config* Config = ttc_can_get_configuration( LogicalIndex );

    return _driver_can_read( Config, Argument );
}
e_ttc_can_errorcode ttc_can_send( t_u8 LogicalIndex, t_u32 Destination, t_u8* Buffer, t_base Amount ) {
    Assert_CAN( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    Assert_CAN( Destination, ttc_assert_origin_auto );
    Assert_CAN( Buffer, ttc_assert_origin_auto );
    Assert_CAN( Amount, ttc_assert_origin_auto );
    t_ttc_can_config* Config = ttc_can_get_configuration( LogicalIndex );

    t_u8 status = _ttc_can_task_transmit( Config, Destination, ( void* ) Buffer, Amount );
    if ( ( status < 3 ) && ( status >= 0 ) )
    { return ec_can_OK; }
    else { return ec_can_ERROR; }
}
e_ttc_can_errorcode ttc_can_send_const( t_u8 LogicalIndex, t_u32 Destination, const char* Buffer, t_base MaxLength ) {
    Assert_CAN( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    Assert_CAN( Destination, ttc_assert_origin_auto );
    Assert_CAN( Buffer, ttc_assert_origin_auto );
    Assert_CAN( MaxLength, ttc_assert_origin_auto );
    t_ttc_can_config* Config = ttc_can_get_configuration( LogicalIndex );

    t_u8 status = _ttc_can_task_transmit( Config, Destination, ( void* ) Buffer, MaxLength );
    if ( ( status < 3 ) && ( status >= 0 ) )
    { return ec_can_OK; }
    else { return ec_can_ERROR; }
}
void ttc_can_set_filter( t_u16 Address, t_u8 LogicalIndex ) {
    Assert_CAN( Address, ttc_assert_origin_auto );
    Assert_CAN( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_can_config* Config = ttc_can_get_configuration( LogicalIndex );
    t_u8 FilterNum = 0;

    while ( Config->CAN_Filter[FilterNum].Flags.Bits.FilterActivation != 0 )
    { FilterNum++; }

    #ifndef STM32F10X_CL
    if ( FilterNum >= 14 )
    { Assert_CAN( FilterNum, ttc_assert_origin_auto ); }
    #else
    if ( FilterNum >= 28 )
    { Assert_CAN( FilterNum, ttc_assert_origin_auto ); }
    #endif

    Config->CAN_Filter[FilterNum].FilterAddress = Address;
    Config->CAN_Filter[FilterNum].Flags.Bits.FilterNumber = FilterNum;
    Config->CAN_Filter[FilterNum].Flags.Bits.FilterMode = 0;
    Config->CAN_Filter[FilterNum].Flags.Bits.FilterScale = 0;
    Config->CAN_Filter[FilterNum].IdHigh = ( t_u16 )( Address << 5 );
    Config->CAN_Filter[FilterNum].IdLow = 0;
    Config->CAN_Filter[FilterNum].MaskIdHigh = 0xFFFF;
    Config->CAN_Filter[FilterNum].MaskIdLow = 0xFFFF;
    Config->CAN_Filter[FilterNum].Flags.Bits.FifoAssign = 0;
    Config->CAN_Filter[FilterNum].Flags.Bits.FilterActivation = 1;

    _driver_can_set_filter( FilterNum, Config );
}
void ttc_can_clr_filter( t_u16 Address, t_u8 LogicalIndex ) {
    Assert_CAN( Address, ttc_assert_origin_auto );
    Assert_CAN( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_can_config* Config = ttc_can_get_configuration( LogicalIndex );
    t_u8 FilterNum = 0;
    bool findIt = FALSE;

    while ( ( Config->CAN_Filter[FilterNum].Flags.Bits.FilterActivation != 0 ) && !findIt ) {
        if ( Address == Config->CAN_Filter[FilterNum].FilterAddress )
        { findIt = TRUE; }
        else { FilterNum++; }
    }

    #ifndef STM32F10X_CL
    if ( FilterNum >= 14 )
    { Assert_CAN( FilterNum, ttc_assert_origin_auto ); }
    #else
    if ( FilterNum >= 28 )
    { Assert_CAN( FilterNum, ttc_assert_origin_auto ); }
    #endif

    Config->CAN_Filter[FilterNum].IdHigh = 0;
    Config->CAN_Filter[FilterNum].FilterAddress = 0;
    Config->CAN_Filter[FilterNum].Flags.All = 0;

    _driver_can_set_filter( FilterNum, Config );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_can(t_u8 LogicalIndex) {  }

t_u8 _ttc_can_task_transmit( t_ttc_can_config* Config, t_u32 Destination, char* Buffer, t_u8 Size ) {
    Assert_CAN( Config, ttc_assert_origin_auto ); // logical index starts at 1
    Assert_CAN( Destination, ttc_assert_origin_auto );
    Assert_CAN( Buffer, ttc_assert_origin_auto );
    Assert_CAN( Size, ttc_assert_origin_auto );

    if ( Size < 8 ) {
        Config->CAN_Tx_Message.StdId = Destination;
        Config->CAN_Tx_Message.ExtId = 0;
        Config->CAN_Tx_Message.Flags.Bits.IDE = 0;
        Config->CAN_Tx_Message.Flags.Bits.RTR = 0;
        Config->CAN_Tx_Message.Flags.Bits.DLC = Size;
        Config->CAN_Tx_Message.Data[0] |= *Buffer;
        Buffer++;
        Config->CAN_Tx_Message.Data[0] |= ( *Buffer ) << 8;
        Buffer++;
        Config->CAN_Tx_Message.Data[0] |= ( *Buffer ) << 16;
        Buffer++;
        Config->CAN_Tx_Message.Data[0] |= ( *Buffer ) << 24;
        Buffer++;
        Config->CAN_Tx_Message.Data[1] |= *Buffer;
        Buffer++;
        Config->CAN_Tx_Message.Data[1] |= ( *Buffer ) << 8;
        Buffer++;
        Config->CAN_Tx_Message.Data[1] |= ( *Buffer ) << 16;
        Buffer++;
        Config->CAN_Tx_Message.Data[1] |= ( *Buffer ) << 24;
    }
    else {
        t_u8 BuffSize = Size;
        while ( BuffSize >= 8 ) {
            Config->CAN_Tx_Message.StdId = Destination;
            Config->CAN_Tx_Message.ExtId = 0;
            Config->CAN_Tx_Message.Flags.Bits.IDE = 0;
            Config->CAN_Tx_Message.Flags.Bits.RTR = 0;
            Config->CAN_Tx_Message.Flags.Bits.DLC = 8;
            Config->CAN_Tx_Message.Data[0] |= *Buffer;
            Buffer++;
            Config->CAN_Tx_Message.Data[0] |= ( *Buffer ) << 8;
            Buffer++;
            Config->CAN_Tx_Message.Data[0] |= ( *Buffer ) << 16;
            Buffer++;
            Config->CAN_Tx_Message.Data[0] |= ( *Buffer ) << 24;
            Buffer++;
            Config->CAN_Tx_Message.Data[1] |= *Buffer;
            Buffer++;
            Config->CAN_Tx_Message.Data[1] |= ( *Buffer ) << 8;
            Buffer++;
            Config->CAN_Tx_Message.Data[1] |= ( *Buffer ) << 16;
            Buffer++;
            Config->CAN_Tx_Message.Data[1] |= ( *Buffer ) << 24;

            //t_u8 MailboxNum = ttc_can_interface_send(Config);
            t_u8 MailboxNum = can_stm32f1xx_send( Config );
            while ( _ttc_can_tx_status( Config->LogicalIndex, MailboxNum ) != tx_status_OK );

            BuffSize -= 8;
        }
        Config->CAN_Tx_Message.StdId = Destination;
        Config->CAN_Tx_Message.ExtId = 0;
        Config->CAN_Tx_Message.Flags.Bits.IDE = 0;
        Config->CAN_Tx_Message.Flags.Bits.RTR = 0;
        Config->CAN_Tx_Message.Flags.Bits.DLC = BuffSize;
        Config->CAN_Tx_Message.Data[0] |= *Buffer;
        Buffer++;
        Config->CAN_Tx_Message.Data[0] |= ( *Buffer ) << 8;
        Buffer++;
        Config->CAN_Tx_Message.Data[0] |= ( *Buffer ) << 16;
        Buffer++;
        Config->CAN_Tx_Message.Data[0] |= ( *Buffer ) << 24;
        Buffer++;
        Config->CAN_Tx_Message.Data[1] |= *Buffer;
        Buffer++;
        Config->CAN_Tx_Message.Data[1] |= ( *Buffer ) << 8;
        Buffer++;
        Config->CAN_Tx_Message.Data[1] |= ( *Buffer ) << 16;
        Buffer++;
        Config->CAN_Tx_Message.Data[1] |= ( *Buffer ) << 24;
    }

    //return ttc_can_interface_send(Config);
    return can_stm32f1xx_send( Config );
}
void _ttc_can_rx_isr( t_ttc_can_config* Config ) {
    Assert_CAN( Config != NULL, ttc_assert_origin_auto );

    if ( Config->Init.activity_rx_isr )
    { Config->Init.activity_rx_isr( Config ); }
}
t_u8 _ttc_can_tx_status( t_u8 LogicalIndex, t_u8 MailBox ) {
    Assert_CAN( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_can_config* Config = ttc_can_get_configuration( LogicalIndex );

//    return _ttc_can_interface_tx_status(Config, MailBox);
    return _can_stm32f1xx_tx_status( Config, MailBox );
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)


//}PrivateFunctions
