/** { ttc_systick.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for systick devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Most functions of this driver are defined as macros and being implemented
 *  in interfaces/ttc_systick_interface.c or in low-level drivers systick/systick_*.c.
 *
 *  See corresponding ttc_systick.h for documentation of all high-level functions.
 *
 *  Created from template ttc_device.c revision 40 at 20160926 13:55:21 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_systick.h".
//
#include "ttc_systick.h"
#include "ttc_sysclock.h"
#include "ttc_heap.h"      // dynamic memory and safe arrays
#ifdef EXTENSION_ttc_interrupt
    #include "ttc_interrupt.h"
#endif
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes

#if TTC_SYSTICK_AMOUNT == 0
    #warning No SYSTICK devices defined, cdid you forget to activate something? - Define at least TTC_SYSTICK1 as one from e_ttc_systick_architecture in your makefile!
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of systick devices.
 *
 */


// for each initialized device, a pointer to its generic and architecture specific definitions is stored
A_define( t_ttc_systick_config*, ttc_systick_configs, TTC_SYSTICK_AMOUNT );

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ private Function prototypes ********************************************
//
// Private functions can normally only be accessed from inside this file.
// Declaring helper functions as private makes your public interface more understandable.
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_systick(t_ttc_systick_config* Config)

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * @param LogicalIndex    1.. logical index of SYSTICK device. Each logical device <n> is defined via TTC_SYSTICK<n>* constants in compile_options.h and extensions.active/makefile
 */
void _ttc_systick_configuration_check( t_u8 LogicalIndex );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

extern BOOL DebugMode; //DEBUG

//}PrivateFunctions
//{ Function definitions ***************************************************

t_u8                    ttc_systick_get_max_index() {
    return TTC_SYSTICK_AMOUNT;
}
t_ttc_systick_config*   ttc_systick_get_configuration( t_u8 LogicalIndex ) {
    Assert_SYSTICK( LogicalIndex > 0, ttc_assert_origin_auto );  // logical index starts at 1
    Assert_SYSTICK( LogicalIndex <= TTC_SYSTICK_AMOUNT, ttc_assert_origin_auto );  // invalid index given (did you configure engnough ttc_systick devices in your makefile?)
    t_ttc_systick_config* Config = A( ttc_systick_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_disable();                      // ensure that we are not interrupted while creating configuration
#endif

        Config = A( ttc_systick_configs, LogicalIndex - 1 ); // have to reload with disabled interrupts to avoid racing conditions
        if ( !Config ) {
            Config = A( ttc_systick_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_systick_config ) );
            Config->Flags.Bits.Initialized = 0;             // make sure that deinit() is not called during load_defaults()
            ttc_systick_load_defaults( LogicalIndex );
        }

#ifdef EXTENSION_ttc_interrupt
        ttc_interrupt_all_enable();
#endif
    }

    Assert_SYSTICK_EXTRA_Writable( Config, ttc_assert_origin_auto );  // memory corrupted?
    return Config;
}
void                    ttc_systick_deinit( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration

    t_ttc_systick_config* Config = ttc_systick_get_configuration( LogicalIndex );

    if ( Config->Flags.Bits.Initialized ) {
        e_ttc_systick_errorcode Result = _driver_systick_deinit( Config );
        if ( Result == ec_systick_OK )
        { Config->Flags.Bits.Initialized = 0; }
    }
    ttc_task_critical_end(); // other tasks now may access this driver
}
void                    ttc_systick_delay_init( t_ttc_systick_delay* Delay, t_base TimeUS ) {
    Assert_SYSTICK_Writable( Delay, ttc_assert_origin_auto );  // always check incoming pointer arguments
    Assert_SYSTICK( !ttc_interrupt_check_inside_isr(), ttc_assert_origin_auto );  // interrupt service routine must use ttc_systick_delay_init_isr() instead!

    if ( TimeUS < ttc_systick_delay_get_minimum() )
    { TimeUS = ttc_systick_delay_get_minimum(); }

    Delay->TimeStart = ttc_systick_get_elapsed_ticks();
    Delay->TimeEnd   = Delay->TimeStart + ttc_systick_get_us_ticks( TimeUS );
    Assert_SYSTICK_EXTRA( Delay->TimeStart != Delay->TimeEnd, ttc_assert_origin_auto ); // zero time delays are not supported. Check low-level driver implementation!
}
void                    ttc_systick_delay_init_isr( t_ttc_systick_delay* Delay, t_base TimeUS ) {
    Assert_SYSTICK_Writable( Delay, ttc_assert_origin_auto );  // always check incoming pointer arguments
    Assert_SYSTICK( ttc_interrupt_check_inside_isr() || ttc_interrupt_critical_level(), ttc_assert_origin_auto );  // user space must use ttc_systick_delay_init() instead!

    if ( TimeUS < ttc_systick_delay_get_minimum() )
    { TimeUS = ttc_systick_delay_get_minimum(); }

    Delay->TimeStart = ttc_systick_get_elapsed_ticks_isr();
    Delay->TimeEnd   = Delay->TimeStart + ttc_systick_get_us_ticks( TimeUS );
    Assert_SYSTICK_EXTRA( Delay->TimeStart != Delay->TimeEnd, ttc_assert_origin_auto ); // zero time delays are not supported. Check low-level driver implementation!
}
BOOL                    ttc_systick_delay_expired( t_ttc_systick_delay* Delay ) {
    Assert_SYSTICK_Readable( Delay, ttc_assert_origin_auto );                  // always check incoming pointer arguments

    t_base Now = ttc_systick_get_elapsed_ticks();

    if ( Delay->TimeEnd < Delay->TimeStart ) { // handle overflow
        if ( Now > Delay->TimeStart )
        { return FALSE; } // waiting for sysclock to overrun too
    }
    if ( Delay->TimeEnd > Now )
    { return FALSE; }  // waiting for delay to expire

    return TRUE; // delay has expired
}
BOOL                    ttc_systick_delay_expired_isr( t_ttc_systick_delay* Delay ) {
    Assert_SYSTICK_Readable( Delay, ttc_assert_origin_auto );  // always check incoming pointer arguments

    t_base Now = ttc_systick_get_elapsed_ticks_isr();

    if ( Delay->TimeEnd < Delay->TimeStart ) { // handle overflow
        if ( Delay->TimeStart < Now )
        { return FALSE; } // waiting for sysclock to overrun too
    }
    if ( Delay->TimeEnd > Now )
    { return FALSE; }  // waiting for delay to expire

    return TRUE; // delay has expired
}
t_base                  ttc_systick_delay_get_minimum() {

    return ttc_systick_get_configuration( 1 )->MinimumDelayUS;
}
t_base                  ttc_systick_delay_get_recommended() {

    return ttc_systick_get_configuration( 1 )->RecommendedDelayUS;
}
e_ttc_systick_errorcode ttc_systick_init( t_u8 LogicalIndex ) {
    ttc_task_critical_begin(); // avoid concurrent access to driver configuration
    t_ttc_systick_config* Config = ttc_systick_get_configuration( LogicalIndex );

    // check configuration to meet all limits of current architecture
    _ttc_systick_configuration_check( LogicalIndex );

    // reset some fields to ensure that they are set by low-level driver
    Config->Architecture          = 0;
    Config->MinimumDelayUS        = 0;
    Config->RecommendedDelayUS    = 0;
    Config->NanoSecondsPerSysTick = 0;

    Config->LastError = _driver_systick_init( Config );
    if ( ! Config->LastError ) // == 0
    { Config->Flags.Bits.Initialized = 1; }

    // check low-level driver initialization
    Assert_SYSTICK_EXTRA( Config->Architecture          != 0, ttc_assert_origin_auto );  // Low-Level driver must set this value!
    Assert_SYSTICK_EXTRA( Config->MinimumDelayUS        != 0, ttc_assert_origin_auto );  // Low-Level driver must set this value!
    Assert_SYSTICK_EXTRA( Config->NanoSecondsPerSysTick != 0, ttc_assert_origin_auto );  // Low-Level driver must set this value!
    Config->RecommendedDelayUS = Config->MinimumDelayUS * 10;

    // check profile (allowed to be changed by application)
    Assert_SYSTICK( Config->Profile != 0, ttc_assert_origin_auto );  // invalid value!
    Assert_SYSTICK( Config->Profile < profile_systick_unknown, ttc_assert_origin_auto );  // invalid value!

    // check fields that have to be filled by low-level driver
    Assert_SYSTICK( Config->MinimumDelayUS        > 0, ttc_assert_origin_auto ); // low-level driver must set this value!
    Assert_SYSTICK( Config->RecommendedDelayUS    > 0, ttc_assert_origin_auto ); // low-level driver must set this value!
    Assert_SYSTICK( Config->NanoSecondsPerSysTick > 0, ttc_assert_origin_auto ); // low-level driver must set this value!

    ttc_task_critical_end(); // other tasks now may access this driver

    return Config->LastError;
}
e_ttc_systick_errorcode ttc_systick_load_defaults( t_u8 LogicalIndex ) {
    t_ttc_systick_config* Config = ttc_systick_get_configuration( LogicalIndex );

    if ( Config->Flags.Bits.Initialized )
    { ttc_systick_deinit( LogicalIndex ); }

    ttc_memory_set( Config, 0, sizeof( t_ttc_systick_config ) );

    // load generic default values
    Config->LogicalIndex = LogicalIndex;
    switch ( LogicalIndex ) { // physical index of systick device from static configuration
#ifdef TTC_SYSTICK1
        case  1: Config->PhysicalIndex = TTC_SYSTICK1; break;
#endif
#ifdef TTC_SYSTICK2
        case  2: Config->PhysicalIndex = TTC_SYSTICK2; break;
#endif
#ifdef TTC_SYSTICK3
        case  3: Config->PhysicalIndex = TTC_SYSTICK3; break;
#endif
#ifdef TTC_SYSTICK4
        case  4: Config->PhysicalIndex = TTC_SYSTICK4; break;
#endif
#ifdef TTC_SYSTICK5
        case  5: Config->PhysicalIndex = TTC_SYSTICK5; break;
#endif
#ifdef TTC_SYSTICK6
        case  6: Config->PhysicalIndex = TTC_SYSTICK6; break;
#endif
#ifdef TTC_SYSTICK7
        case  7: Config->PhysicalIndex = TTC_SYSTICK7; break;
#endif
#ifdef TTC_SYSTICK8
        case  8: Config->PhysicalIndex = TTC_SYSTICK8; break;
#endif
#ifdef TTC_SYSTICK9
        case  9: Config->PhysicalIndex = TTC_SYSTICK9; break;
#endif
#ifdef TTC_SYSTICK10
        case 10: Config->PhysicalIndex = TTC_SYSTICK10; break;
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
    }

    // allocate memory for low-level driver (most low-level driver developers had difficulties to allocate it correctly)
    u_ttc_systick_architecture* LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_ttc_systick_architecture ) );
    Config->LowLevelConfig = LowLevelConfig;

    //Insert additional generic default values here ...
    Config->Profile = profile_systick_Default;

    // Let low-level driver initialize remaining fields
    Config->LastError = _driver_systick_load_defaults( Config );

    // Check mandatory configuration items
    Assert_SYSTICK_EXTRA( LowLevelConfig == Config->LowLevelConfig, ttc_assert_origin_auto );  // LowLevelConfig already allocated, low-level driver must not reallocate it!
    Assert_SYSTICK_EXTRA( ( Config->Architecture > ta_systick_None ) && ( Config->Architecture < ta_systick_unknown ), ttc_assert_origin_auto );  // architecture not set properly! Low-Level driver must set this field!

    return Config->LastError;
}
void                    ttc_systick_prepare() {
    // This function is automatically called from ttc_exensions:ttc_extensions_start() at system startup
    // add your startup code here (Singletasking!)

    A_reset( ttc_systick_configs, TTC_SYSTICK_AMOUNT ); // safe arrays are placed in data section and must be initialized manually

    // register this device for a sysclock change update
    ttc_sysclock_register_for_update( ttc_systick_sysclock_changed );

    _driver_systick_prepare();

    // initialize first systick device automatically
    ttc_systick_get_configuration( 1 );
    ttc_systick_init( 1 );
}
void                    ttc_systick_reset( t_u8 LogicalIndex ) {
    t_ttc_systick_config* Config = ttc_systick_get_configuration( LogicalIndex );

    _driver_systick_reset( Config );
}
void                    ttc_systick_sysclock_changed() {

    // deinit + reinit all initialized SYSTICK devices
    for ( t_u8 LogicalIndex = 1; LogicalIndex < ttc_systick_get_max_index(); LogicalIndex++ ) {
        t_ttc_systick_config* Config = ttc_systick_get_configuration( LogicalIndex );
        if ( Config->Flags.Bits.Initialized ) {
            ttc_systick_deinit( LogicalIndex );
            ttc_systick_init( LogicalIndex );
        }
    }
}
void                    ttc_systick_delay_wait( t_ttc_systick_delay* Delay ) {
    Assert_SYSTICK_Writable( Delay, ttc_assert_origin_auto ); // always check incoming pointer arguments

    while ( ! ttc_systick_delay_expired( Delay ) )
    { ttc_task_yield(); }
    TODO( "ttc_systick_delay_wait(): Put task to complete sleep if multitasking is available." )
}
void                    ttc_systick_delay_simple( t_base TimeUS ) {
    Assert_SYSTICK( TimeUS, ttc_assert_origin_auto ); // if no Delay is given, TimeUS is required!

    t_ttc_systick_delay Delay;
    ttc_systick_delay_init( &Delay, TimeUS );
    ttc_systick_delay_wait( &Delay );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function definitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_systick(t_u8 LogicalIndex) {  }

void _ttc_systick_configuration_check( t_u8 LogicalIndex ) {
    t_ttc_systick_config* Config = ttc_systick_get_configuration( LogicalIndex );

    // perform some generic plausibility checks
    const t_ttc_systick_features* Features = Features;

    // add more architecture independent checks...

    if ( ( Config->Profile == profile_systick_None ) ||
            ( Config->Profile >= profile_systick_unknown )
       )
    { Config->Profile = profile_systick_Default; }


    // let low-level driver check this configuration too
    _driver_systick_configuration_check( Config );
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
