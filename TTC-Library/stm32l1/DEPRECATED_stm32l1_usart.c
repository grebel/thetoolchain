/** { stm32l1_usart.c ************************************************
 
                           The ToolChain
                      
   High-Level interface for USART device.
     
   Note: See ttc_usart.h for description of stm32l1 independent USART implementation.
  
   Authors: 
}*/

#include "stm32l1_usart.h"

//{Arrays
// stores pointers to structures of initialized USARTs/ UARTs
// Note: index is not real, but logical USART-index as defined by makefile!
//static stm32_usart_architecture_t* stm32_USARTs[TTC_AMOUNT_USARTS];

// register base addresses of all USARTs
stm32l1_register_usart_t* const stm32l1_USART_Bases[3] = {
                                  (stm32l1_register_usart_t*) USART1,
                                  (stm32l1_register_usart_t*) USART2,
                                  (stm32l1_register_usart_t*) USART3,

};
//} END Arrays

// these are defined by ttc_usart.c
extern void _ttc_usart_rx_isr(ttc_usart_config_t* USART_Generic, u8_t Byte);
extern void _ttc_usart_tx_isr(ttc_usart_config_t* USART_Generic, void* USART_Hardware);

//{ Function definitions ***************************************************

ttc_usart_errorcode_e stm32l1_usart_init(ttc_usart_config_t* USART_Generic)
{
    if(1){//validate USART_Generic to make sure it complies with architecture-specific availability
       //Make sure USART_Generic is filled out
       Assert_USART(USART_Generic != NULL, ec_NULL);

       //Correct number of USARTS
       u8_t LogicalIndex = USART_Generic->LogicalIndex;
       Assert(LogicalIndex > 0, ec_InvalidArgument); // logical index starts at 1
       if (LogicalIndex > TTC_AMOUNT_USARTS)
        return tue_DeviceNotFound;

       //Correct word length
       if ( (USART_Generic->WordLength   < 8) || (USART_Generic->WordLength   > 9) )
        return tue_InvalidWordSize;

       //Correct number of stop bits
       if ( (USART_Generic->HalfStopBits < 1) || (USART_Generic->HalfStopBits > 4) )
        return tue_InvalidStopBits;
    }


#ifdef USE_STDPERIPH_DRIVER
    //create Standard Peripheral Init Structure
    USART_InitTypeDef USART_InitStructure ;
    //clear the structure
    USART_StructInit(&USART_InitStructure);


    //Get info from USART_Generic and fill out Init Structure
    USART_InitStructure.USART_BaudRate = USART_Generic->BaudRate;

    //Set Mode
     USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

    //------------Word Length (Data Bits)-----------------//
    if(1){
        //How many Data bits: 8 or 9
        if(USART_Generic->WordLength == 8)
            USART_InitStructure.USART_WordLength = USART_WordLength_8b;
        else if(USART_Generic->WordLength == 9)
            USART_InitStructure.USART_WordLength = USART_WordLength_9b;
    }
    //------------END Word Length-------------//

    //------------Stop Bits-----------------//
    if(1){
        //How many stop bits: clear for 1 bit, set for 2 bits
        switch(USART_Generic->HalfStopBits)
        {
        case 1:
            USART_InitStructure.USART_StopBits   = USART_StopBits_0_5; //1*.5 = .5 stop bits
            break;
        case 2:
            USART_InitStructure.USART_StopBits   = USART_StopBits_1; //one stop bit
            break;
        case 3:
            USART_InitStructure.USART_StopBits   = USART_StopBits_1_5; //1.5 stop bits
            break;
        case 4:
            USART_InitStructure.USART_StopBits   = USART_StopBits_2;  //two stop bits
            break;
        default:
            USART_InitStructure.USART_StopBits   = USART_StopBits_1; //one stop bit
            break;
        }
    }
    //------------END Stop Bits-------------//

    //------------Parity--------------------//
    if(1){
        if(USART_Generic->Flags.Bits.TransmitParity) //Parity: 0->no parity, 1->parity
        {//Enable the parity bit

          //Is parity even (0) or odd (1)
            if(USART_Generic->Flags.Bits.ParityEven)
                USART_InitStructure.USART_Parity = USART_Parity_Even; //even
            if(USART_Generic->Flags.Bits.ParityOdd)
                USART_InitStructure.USART_Parity = USART_Parity_Odd; //odd
        }
        else  USART_InitStructure.USART_Parity = USART_Parity_No; //No parity
    }
    //------------END Parity----------------//

    //--------------nCTS and nRTS-------------------//
    if(USART_Generic->Flags.Bits.Cts)
    {
        if(USART_Generic->Flags.Bits.Rts)
          USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_RTS_CTS;
        else
          USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_CTS;
    }
    else
    {
        if(USART_Generic->Flags.Bits.Rts)
          USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_RTS;
        else
          USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    }
    //----------END nCTS and nRTS-------------------//

    //-------Configure specific USART Device-------//
    switch(USART_Generic->LogicalIndex)
    {
    case 1:
        //Power USART1 by turning on clock via APB2
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

        //--------------Set pins specific to USART1---------------//
        //function of pin
        _driver_ttc_gpio_init(TTC_GPIO_BANK_A,9,tgm_alternate_function_push_pull, tgs_40MHz ); //GPIO init(can also be PB6 - have to change board install script)
        ttc_gpio_init(PIN_PA10,tgm_alternate_function_push_pull);                         //GPIO init(can also be PB7)

        //define alternative function
        GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1); //set pins to AF
        GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);//set pins to AF
        //------------END Set pins specific to USART1---------------//

        //Give the USART_Generic the proper handle to the USART
        USART_Generic->USART_Arch.Base=stm32l1_USART_Bases[0];

        //Initialize and enable device
        USART_Init(USART1, &USART_InitStructure);
        USART_Cmd(USART1 , ENABLE );
        break;

    case 2:
        //Power USART2 by turning on clock via APB1
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

        //--------------Set pins specific to USART2---------------//
        //function of pin
        _driver_ttc_gpio_init(TTC_GPIO_BANK_A,2,tgm_alternate_function_push_pull, tgs_40MHz ); //TX GPIO init
        ttc_gpio_init(PIN_PA3,tgm_alternate_function_push_pull);                          //RX GPIO init

        //define alternative function
        GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2); //set pins to AF
        GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);//set pins to AF
        //------------END Set pins specific to USART2---------------//

        //Give the USART_Generic the proper handle to the USART
        USART_Generic->USART_Arch.Base=stm32l1_USART_Bases[1];

        //Initialize and enable device
        USART_Init(USART2, &USART_InitStructure);
        USART_Cmd(USART2 , ENABLE );
        break;

    case 3:
        //Power USART3 by turning on clock via APB1
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

        //--------------Set pins specific to USART3---------------//
        //function of pin
        _driver_ttc_gpio_init(TTC_GPIO_BANK_B,10,tgm_alternate_function_push_pull, tgs_40MHz ); //GPIO init
        ttc_gpio_init(PIN_PB11,tgm_alternate_function_push_pull);                         //GPIO init

        //define alternative function
        GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_USART3); //set pins to AF
        GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_USART3);//set pins to AF
        //------------END Set pins specific to USART3---------------//

        //Give the USART_Generic the proper handle to the USART
        USART_Generic->USART_Arch.Base=stm32l1_USART_Bases[2];

        //Initialize and enable device
        USART_Init(USART3, &USART_InitStructure);
        USART_Cmd(USART3 , ENABLE );
        break;
    }
    //------END Initialize and enable the USART Device-----//


#else
#endif
    return tue_OK;
}

//} Function definitions


//{ private functions (ideally) ********************************************
void _stm32l1_usart_rx_isr(physical_index_t PhysicalIndex, void* Argument){
    Assert_USART(PhysicalIndex <= TTC_AMOUNT_USARTS, ec_InvalidArgument);

    stm32l1_register_usart_t* USART_Base = stm32l1_USART_Bases[PhysicalIndex];


    u8_t Byte = (u8_t) 0xff & ( (USART_TypeDef*) USART_Base)->DR;


    _ttc_usart_rx_isr((ttc_usart_config_t*) Argument, Byte);

}


void _stm32l1_usart_tx_isr(physical_index_t PhysicalIndex, void* Argument){
    Assert_USART(PhysicalIndex <= TTC_AMOUNT_USARTS, ec_InvalidArgument);

    stm32l1_register_usart_t* USART_Base = stm32l1_USART_Bases[PhysicalIndex];

    _ttc_usart_tx_isr( (ttc_usart_config_t*) Argument, USART_Base ); // provide received byte to registered function

}

//} private functions
