#ifndef STM32L1_USART_TYPES_H
#define STM32L1_USART_TYPES_H

/** { DEPRECATED_stm32l1_usart.h ************************************************
 
                           The ToolChain
                      
   Low-Level (Architecture-specific) interface for USART device.

   Structures, Enums and Defines being required by ttc_usart_types.h

   Note: See ttc_usart.h for description of stm32l1 independent USART implementation.
  
   Authors: Greg Knoll 2013

 
}*/
//{ Defines/ TypeDefs ******************************************************
  #define TTC_USART_MAX_AMOUNT 3
//} Defines

//{ Includes ***************************************************************
#include "../ttc_basic.h"
#include "stm32l1_registers.h"
//} Includes

//{ Structures/ Enums required by ttc_usart_types.h ***********************



typedef struct {  // stm32l1 specific configuration data of single USART device
  stm32l1_register_usart_t* Base;       // base address of USART registers
               u8_t  PhysicalIndex;      // physical device index (1=first USART device, ...)
} __attribute__((__packed__)) stm32l1_usart_architecture_t;

// ttc_usart_arch_t is required by ttc_usart_types.h
typedef stm32l1_usart_architecture_t ttc_usart_architecture_t;




#endif //STM32L1_USART_TYPES_H

