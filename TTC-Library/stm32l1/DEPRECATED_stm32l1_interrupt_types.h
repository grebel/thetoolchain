#ifndef STM32L1_INTERRUPT_TYPES_H
#define STM32L1_INTERRUPT_TYPES_H

/** { stm32l1_interrupt.h ************************************************
 
                           The ToolChain
                      
   High-Level interface for INTERRUPT device.

   Structures, Enums and Defines being required by ttc_interrupt_types.h

   Note: See ttc_interrupt.h for description of stm32l1 independent INTERRUPT implementation.
  
   Authors: Greg Knoll 2013

 
}*/
//{ Defines/ TypeDefs ******************************************************


//} Defines

//{ Includes ***************************************************************

#include "../ttc_basic.h"

//} Includes


//{ Structures/ Enums required by ttc_interrupt_types.h ***********************

/*typedef struct { // register description (adapt according to stm32l1 registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} INTERRUPT_Register_t;

typedef struct {  // stm32l1 specific configuration data of single INTERRUPT device
  INTERRUPT_Register_t* BaseRegister;       // base address of INTERRUPT registers
               u8_t  PhysicalIndex;      // physical device index (1=first INTERRUPT device, ...)
} __attribute__((__packed__)) stm32l1_interrupt_config_t;

// ttc_interrupt_arch_t is required by ttc_interrupt_types.h
typedef stm32l1_interrupt_config_t ttc_interrupt_arch_t;
*/

//} Structures/ Enums


#endif //STM32L1_INTERRUPT_TYPES_H
