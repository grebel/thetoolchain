#ifndef STM32L1_REGISTERS_H
#define STM32L1_REGISTERS_H


/*{ stm32l1_registers.h ******************************************************


   Structure definitions of stm32l1xx-registers.

   written by Greg Knoll 2013


}*/



//{ Includes *************************************************************

// Basic set of helper functions
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "register_cortexm3.h"


#include "stm32l1xx.h"

//} Includes

//{ ENUMS****************************************************************
typedef enum {   // e_stm32l1_gpio_mode -  pin modes
    gpio_input,                    // 00
    gpio_general_purpose_output,   // 01
    gpc_alternate_function,        // 10
    gpc_analog                     // 11
} e_stm32l1_gpio_mode;

typedef enum {
    gpio_otype_push_pull = 0x00,
    gpio_otype_open_drain = 0x01
} e_stm32l1_gpio_otype;

typedef enum {   // e_stm32l1_gpio_speed - speeds of pins
    gpio_speed_400kHz,       // 00
    gpio_speed_2MHz,         // 01
    gpio_speed_10MHz,        // 10
    gpio_speed_40MHz         // 11
} e_stm32l1_gpio_speed;

typedef enum {   //e_stm32l1_gpio_pupd - pull-up/pull-down configuration
    gpio_noPULL,          // 00
    gpio_pull_up,         // 01
    gpio_pull_down        // 10
} e_stm32l1_gpio_pupd;

typedef enum {   //e_stm32l1_gpio_af - alternate functions
    gpio_AF0 = 0x0,          // 00
    gpio_AF1  = 0x1,         // 01
    gpio_AF2  = 0x2,        // 10
    gpio_AF3  = 0x3,
    gpio_AF4  = 0x4,
    gpio_AF5  = 0x5,
    gpio_AF6  = 0x6,
    gpio_AF7  = 0x7,
    gpio_AF8  = 0x8,
    gpio_AF9  = 0x9,
    gpio_AF10  = 0xA,
    gpio_AF11  = 0xB,
    gpio_AF12  = 0xC,
    gpio_AF13  = 0xD,
    gpio_AF14  = 0xE,
    gpio_AF15  = 0xF         //1111
} e_stm32l1_gpio_af;

//} ENUMS_________________________________________________________________

//{ REGISTER STRUCTS********************************************************
typedef struct { //RESERVED - for whole 32-bit spaces in reg maps
    unsigned Reserved   : 32; //whole 32-bit register is reserved
} __attribute__( ( __packed__ ) ) stm32l1_t_reserved_reg;

//---------------------GPIO-----------------------------//
typedef struct { //GPIOx_MODER - GPIO port mode register  (p.142)
    unsigned MODER0   : 2; //Port configuration for one pin - see e_stm32l1_gpio_mode
    unsigned MODER1   : 2; //Port configuration for one pin.
    unsigned MODER2   : 2; //Port configuration for one pin.
    unsigned MODER3   : 2; //Port configuration for one pin.
    unsigned MODER4   : 2; //Port configuration for one pin.
    unsigned MODER5   : 2; //Port configuration for one pin.
    unsigned MODER6   : 2; //Port configuration for one pin.
    unsigned MODER7   : 2; //Port configuration for one pin.
    unsigned MODER8   : 2; //Port configuration for one pin.
    unsigned MODER9   : 2; //Port configuration for one pin.
    unsigned MODER10  : 2; //Port configuration for one pin.
    unsigned MODER11  : 2; //Port configuration for one pin.
    unsigned MODER12  : 2; //Port configuration for one pin.
    unsigned MODER13  : 2; //Port configuration for one pin.
    unsigned MODER14  : 2; //Port configuration for one pin.
    unsigned MODER15  : 2; //Port configuration for one pin.
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_gpiox_moder;

typedef struct { //GPIOx_OTYPER - GPIO port output type register  (p.143)
    unsigned OT0   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT1   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT2   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT3   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT4   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT5   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT6   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT7   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT8   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT9   : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT10  : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT11  : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT12  : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT13  : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT14  : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned OT15  : 1; //Output type: 0 = push-pull (reset), 1=open-drain.
    unsigned Reserved1: 16;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_gpiox_otyper;

typedef struct { //GPIOx_OSPEEDR - GPIO output speed Register  (p.143)
    unsigned OSPEEDR0   : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR1   : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR2   : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR3   : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR4   : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR5   : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR6   : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR7   : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR8   : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR9   : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR10  : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR11  : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR12  : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR13  : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR14  : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
    unsigned OSPEEDR15  : 2; //Speed of port pin - see e_stm32l1_gpio_speed.
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_gpiox_ospeedr;

typedef struct { //GPIOx_PUPDR - GPIO port pull-up/pull-down Register  (p.144)
    unsigned PUPDR0   : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR1   : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR2   : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR3   : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR4   : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR5   : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR6   : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR7   : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR8   : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR9   : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR10  : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR11  : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR12  : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR13  : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR14  : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
    unsigned PUPDR15  : 2; //pull-up or pull-down configuration - see e_stm32l1_gpio_pupd.
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_gpiox_pupdr;

typedef struct { //GPIOx_IDR - GPIO port input data Register  (p.144)
    unsigned IDR0   : 1; //Input value of I/O (read-only).
    unsigned IDR1   : 1; //Input value of I/O (read-only).
    unsigned IDR2   : 1; //Input value of I/O (read-only).
    unsigned IDR3   : 1; //Input value of I/O (read-only).
    unsigned IDR4   : 1; //Input value of I/O (read-only).
    unsigned IDR5   : 1; //Input value of I/O (read-only).
    unsigned IDR6   : 1; //Input value of I/O (read-only).
    unsigned IDR7   : 1; //Input value of I/O (read-only).
    unsigned IDR8   : 1; //Input value of I/O (read-only).
    unsigned IDR9   : 1; //Input value of I/O (read-only).
    unsigned IDR10  : 1; //Input value of I/O (read-only).
    unsigned IDR11  : 1; //Input value of I/O (read-only).
    unsigned IDR12  : 1; //Input value of I/O (read-only).
    unsigned IDR13  : 1; //Input value of I/O (read-only).
    unsigned IDR14  : 1; //Input value of I/O (read-only).
    unsigned IDR15  : 1; //Input value of I/O (read-only).
    unsigned Reserved1: 16;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_gpiox_idr;

typedef struct { //GPIOx_ODR - output data Register  (p.145)
    unsigned ODR0   : 1; //Output value of port (read and write).
    unsigned ODR1   : 1; //Output value of port (read and write).
    unsigned ODR2   : 1; //Output value of port (read and write).
    unsigned ODR3   : 1; //Output value of port (read and write).
    unsigned ODR4   : 1; //Output value of port (read and write).
    unsigned ODR5   : 1; //Output value of port (read and write).
    unsigned ODR6   : 1; //Output value of port (read and write).
    unsigned ODR7   : 1; //Output value of port (read and write).
    unsigned ODR8   : 1; //Output value of port (read and write).
    unsigned ODR9   : 1; //Output value of port (read and write).
    unsigned ODR10  : 1; //Output value of port (read and write).
    unsigned ODR11  : 1; //Output value of port (read and write).
    unsigned ODR12  : 1; //Output value of port (read and write).
    unsigned ODR13  : 1; //Output value of port (read and write).
    unsigned ODR14  : 1; //Output value of port (read and write).
    unsigned ODR15  : 1; //Output value of port (read and write).
    unsigned Reserved1: 16;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_gpiox_odr;

typedef struct { //GPIOx_BSRR - bit set/reset Register  (p.145)
    unsigned BR0   : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR1   : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR2   : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR3   : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR4   : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR5   : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR6   : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR7   : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR8   : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR9   : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR10  : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR11  : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR12  : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR13  : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR14  : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BR15  : 1; //Setting this bit resets corresponding ODR bit (write-only).
    unsigned BS0   : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS1   : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS2   : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS3   : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS4   : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS5   : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS6   : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS7   : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS8   : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS9   : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS10  : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS11  : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS12  : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS13  : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS14  : 1; //Setting this bit sets corresponding ODR bit (write-only).
    unsigned BS15  : 1; //Setting this bit sets corresponding ODR bit (write-only).
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_gpiox_bsrr;

typedef struct { //GPIOx_LCKR - configuration lock Register  (p.145-146)
    unsigned LCK0   : 1; //1: Port config locked. 0: not locked.
    unsigned LCK1   : 1; //1: Port config locked. 0: not locked.
    unsigned LCK2   : 1; //1: Port config locked. 0: not locked.
    unsigned LCK3   : 1; //1: Port config locked. 0: not locked.
    unsigned LCK4   : 1; //1: Port config locked. 0: not locked.
    unsigned LCK5   : 1; //1: Port config locked. 0: not locked.
    unsigned LCK6   : 1; //1: Port config locked. 0: not locked.
    unsigned LCK7   : 1; //1: Port config locked. 0: not locked.
    unsigned LCK8   : 1; //1: Port config locked. 0: not locked.
    unsigned LCK9   : 1; //1: Port config locked. 0: not locked.
    unsigned LCK10  : 1; //1: Port config locked. 0: not locked.
    unsigned LCK11  : 1; //1: Port config locked. 0: not locked.
    unsigned LCK12  : 1; //1: Port config locked. 0: not locked.
    unsigned LCK13  : 1; //1: Port config locked. 0: not locked.
    unsigned LCK14  : 1; //1: Port config locked. 0: not locked.
    unsigned LCK15  : 1; //1: Port config locked. 0: not locked.
    unsigned LCKK   : 1; //Special bit. See p. 146.
    unsigned Reserved1: 15;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_gpiox_lckr;

typedef struct { //GPIOx_AFRL - alternate function low Register  (p.147)
    unsigned AFRL0   : 4; //Alternate function selection - see e_stm32l1_gpio_af.
    unsigned AFRL1   : 4; //Alternate function selection - see e_stm32l1_gpio_af.
    unsigned AFRL2   : 4; //Alternate function selection - see e_stm32l1_gpio_af.
    unsigned AFRL3   : 4; //Alternate function selection - see e_stm32l1_gpio_af.
    unsigned AFRL4   : 4; //Alternate function selection - see e_stm32l1_gpio_af.
    unsigned AFRL5   : 4; //Alternate function selection - see e_stm32l1_gpio_af.
    unsigned AFRL6   : 4; //Alternate function selection - see e_stm32l1_gpio_af.
    unsigned AFRL7   : 4; //Alternate function selection - see e_stm32l1_gpio_af.
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_gpiox_afrl;

typedef struct { //GPIOx_AFRH - alternate function high Register  (p.147)
    unsigned AFRL8   : 4; //Alternate function selection - see e_stm32l1_gpio_af.
    unsigned AFRL9   : 4; //Alternate function selection - see e_stm32l1_gpio_af.
    unsigned AFRL10  : 4; //Alternate function selection - see e_stm32l1_gpio_af.
    unsigned AFRL11  : 4; //Alternate function selection - see e_stm32l1_gpio_af.
    unsigned AFRL12  : 4; //Alternate function selection - see e_stm32l1_gpio_af.
    unsigned AFRL13  : 4; //Alternate function selection - see e_stm32l1_gpio_af.
    unsigned AFRL14  : 4; //Alternate function selection - see e_stm32l1_gpio_af.
    unsigned AFRL15  : 4; //Alternate function selection - see e_stm32l1_gpio_af.
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_gpiox_afrh;

typedef struct { //  All registers for GPIO
    t_register_stm32l1xx_gpiox_moder   MODE;  //Mode of pin (see e_stm32l1_gpio_mode)
    t_register_stm32l1xx_gpiox_otyper  TYPE;  //Type of output (push-pull or open-drain)
    t_register_stm32l1xx_gpiox_ospeedr SPEED; //Speed of output (see e_stm32l1_gpio_speed)
    t_register_stm32l1xx_gpiox_pupdr   PUPD;  //Pull up/down config (see e_stm32l1_gpio_pupd)
    t_register_stm32l1xx_gpiox_idr     IDR;   //Input data register
    t_register_stm32l1xx_gpiox_odr     ODR;   //Output data register
    t_register_stm32l1xx_gpiox_bsrr    BSRR;  //Bit set/reset register
    t_register_stm32l1xx_gpiox_lckr    LCKR;  //Configuration lock register
    t_register_stm32l1xx_gpiox_afrl    AFRL;  //Alternate Function (see e_stm32l1_gpio_af)
    t_register_stm32l1xx_gpiox_afrh    AFRH;  //Alternate Function (see e_stm32l1_gpio_af)
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_gpio;

//-----------------END GPIO-----------------------------//


//------------------USART-----------------------------//
typedef struct { // t_usart_sr     USART Status Register  (p.690)
    unsigned PE   : 1; // Parity Error
    unsigned FE   : 1; // Framing Error
    unsigned NF   : 1; // Noise Detection Flag
    unsigned ORE  : 1; // Overrun Error
    unsigned IDLE : 1; // IDLE line detected
    unsigned RXNE : 1; // Read Data Register not empty
    unsigned TC   : 1; // Transmission complete
    unsigned TXE  : 1; // Transmission Data Register empty
    unsigned LBD  : 1; // Lin break detection flag
    unsigned CTS  : 1; // CTS Flag
    unsigned Reserved1: 22;
} __attribute__( ( __packed__ ) ) t_stm32l1_usart_sr;

typedef struct { // t_usart_dr     USART Data Register
    unsigned Data: 9;
    unsigned Reserved1: 23;
} __attribute__( ( __packed__ ) ) t_stm32l1_usart_dr;

typedef struct { // t_usart_brr    USART Baud Rate Register
    unsigned DIV_Fraction: 4;
    unsigned DIV_Mantissa: 12;
    unsigned Reserved1: 16;
} __attribute__( ( __packed__ ) ) t_stm32l1_usart_brr;

typedef struct { // t_usart_cr1    USART Control Register 1 (p. 694)
    unsigned SBK: 1;    // Send break
    unsigned RWU: 1;    // Receiver wakeup
    unsigned RE: 1;     // Receiver enable
    unsigned TE: 1;     // Transmitter enable
    unsigned IDLEIE: 1; // IDLE interrupt enable
    unsigned RXNEIE: 1; // RXNE interrupt enable
    unsigned TCIE: 1;   // Transmission complete interrupt enable
    unsigned TXEIE: 1;  // TXE interrupt enable
    unsigned PEIE: 1;   // PE interrupt enable
    unsigned PS: 1;     // Parity selection
    unsigned PCE: 1;    // Parity control enable
    unsigned WAKE: 1;   // Wakeup method
    unsigned M: 1;      // Word length
    unsigned UE: 1;     // USART enable
    unsigned Resereved: 1;
    unsigned OVER8: 1;  //Oversampling mode
    unsigned Reserved1: 16;

} __attribute__( ( __packed__ ) ) t_stm32l1_usart_cr1;

typedef struct { // t_usart_cr2    USART Control Register 2

    unsigned ADD: 4;        // Address of the USART node
    unsigned Reserved1 : 1; // forced by hardware to 0.
    unsigned LBDL: 1;       // lin break detection length
    unsigned LBDIE: 1;      // LIN break detection interrupt enable
    unsigned Reserved2 : 1; // forced by hardware to 0.
    unsigned LBCL: 1;       // Last bit clock pulse
    unsigned CPHA: 1;       // Clock phase
    unsigned CPOL: 1;       // Clock polarity
    unsigned CLKEN: 1;      // Clock enable
    unsigned STOP: 2;       // STOP bits
    unsigned LINEN: 1;      // LIN mode enable
    unsigned Reserved3 : 17; // forced by hardware to 0.

} __attribute__( ( __packed__ ) ) t_stm32l1_usart_cr2;

typedef struct { // t_usart_cr3    USART Control Register 3

    unsigned EIE: 1;   // Error interrupt enable
    unsigned IREN: 1;  // IrDA mode enable
    unsigned IRLP: 1;  // IrDA low-power
    unsigned HDSEL: 1; // Half-duplex selection
    unsigned NACK: 1;  // Smartcard NACK enable
    unsigned SC_EN: 1;  // Smartcard mode enable
    unsigned DMAR: 1;  // DMA enable receiver
    unsigned DMAT: 1;  // DMA enable transmitter
    unsigned RTSE: 1;  // RTS enable
    unsigned CTSE: 1;  // CTS enable
    unsigned CTSIE: 1; // CTS interrupt enable
    unsigned ONEBIT: 1; //One sample bit method enable
    unsigned Reserved : 20; // forced by hardware to 0.

} __attribute__( ( __packed__ ) ) t_stm32l1_usart_cr3;

typedef struct { // USART_GTPR_t   USART Guard Time and Prescaler Register
    unsigned PSC : 8;       // Prescaler
    unsigned GT  : 8;       // Guard Time
    unsigned Reserved : 16; // forced by hardware to 0.
} __attribute__( ( __packed__ ) ) t_stm32l1_usart_gtpr;

typedef struct { // All USART registers
    t_stm32l1_usart_sr   SR;

    t_stm32l1_usart_dr   DR;

    t_stm32l1_usart_brr  BRR;

    t_stm32l1_usart_cr1  CR1;

    t_stm32l1_usart_cr2  CR2;

    t_stm32l1_usart_cr3  CR3;

    t_stm32l1_usart_gtpr GTPR;

} __attribute__( ( __packed__ ) ) t_stm32l1_register_usart;
//--------------END USART-----------------------------//


//------------------I2C-----------------------------//
typedef struct { // t_register_stm32l1xx_i2c_cr1      I2C Control Register 1 (->RM0038 p.636)
    unsigned PE            : 1;  // Peripheral enable
    unsigned SMBUS         : 1;  // SMBus mode
    unsigned Reserved1     : 1;  // must be kept at reset value
    unsigned SMBTYPE       : 1;  // SMBus type
    unsigned ENARP         : 1;  // ARP enable
    unsigned ENPEC         : 1;  // PEC enable
    unsigned ENGC          : 1;  // General call enable
    unsigned NOSTRETCH     : 1;  // Clock stretching disable (Slave mode)
    unsigned START         : 1;  // Start generation
    unsigned STOP          : 1;  // Stop generation
    unsigned ACK           : 1;  // Acknowledge enable
    unsigned POS           : 1;  // Acknowledge/PEC Position (for data reception)
    unsigned PEC           : 1;  // Packet error checking
    unsigned ALERT         : 1;  // SMBus alert
    unsigned Reserved2     : 1;  // must be kept at reset value
    unsigned SWRST         : 1;  // Software reset

} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_cr1;
typedef struct { // t_register_stm32l1xx_i2c_cr2      I2C Control Register 2 (->RM0038 p.638)
    unsigned FREQ          : 6;  // Peripheral clock frequency
    unsigned Reserved1     : 2;  // must be kept at reset value
    unsigned ITERR_EN       : 1;  // Error interrupt enable
    unsigned ITEVT_EN       : 1;  // Event interrupt enable
    unsigned ITBUF_EN       : 1;  // Buffer interrupt enable
    unsigned DMA_EN         : 1;  // DMA requests enable
    unsigned LAST          : 1;  // DMA last transfer
    unsigned Reserved2     : 3;  // must be kept at reset value

} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_cr2;
typedef struct { // t_i2c_oar1_7b  I2C Own Address Register 1 for 7 bit adressing mode (->RM0038 p.640)
    unsigned ADD0          : 1;  // not used
    unsigned ADD           : 7;  // Interface address
    unsigned ADD98         : 2;  // not used
    unsigned Reserved1     : 4;  // must be kept at reset value
    unsigned Reserved2     : 1;  // must be kept at reset value
    unsigned ADDMODE       : 1;  // Addressing mode (slave mode)

} __attribute__( ( __packed__ ) ) t_i2c_oar1_7b;

/*
typedef struct { // t_i2c_oar1_10b I2C Own Address Register 1 for 10 bit adressing mode (->RM0038 p.748)
  unsigned ADD           : 10; // Interface address
  unsigned Reserved1     : 4;  // must be kept at reset value
  unsigned Reserved2     : 1;  // must be kept at reset value
  unsigned ADDMODE       : 1;  // Addressing mode (slave mode)

} __attribute__((__packed__)) t_i2c_oar1_10b;
*/

typedef struct { // t_register_stm32l1xx_i2c_oar1     I2C Own Address Register 1 (->RM0038 p.640)
    union {
        t_i2c_oar1_7b  OAR1_7Bit;
//   t_i2c_oar1_10b OAR1_10Bit;
    } Bits;
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_oar1;

typedef struct { // t_register_stm32l1xx_i2c_oar2     I2C Own Address Register 2 (->RM0038 p.640)
    unsigned ENDUAL        : 1;  // Dual addressing mode enable
    unsigned ADD2          : 7;  // Interface address
    unsigned Reserved1     : 8;  // must be kept at reset value
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_oar2;
typedef struct { // t_register_stm32l1xx_i2c_dr       I2C Data Register (->RM0038 p.641)
    unsigned DR            : 8;  // 8-bit data register
    unsigned Reserved1     : 8;  // must be kept at reset value
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_dr;
typedef struct { // t_register_stm32l1xx_i2c_sr1      I2C Status Register 1 (->RM0038 p.641)
    unsigned SB            : 1;  // Start bit (Master mode)
    unsigned ADDR          : 1;  // Address sent (master mode)/matched (slave mode)
    unsigned BTF           : 1;  // Byte transfer finished
    unsigned ADD10         : 1;  // 10-bit header sent (Master mode)
    unsigned STOPF         : 1;  // Stop detection (slave mode)
    unsigned Reserved1     : 1;  // must be kept at reset value
    unsigned RXNE          : 1;  // Data register not empty (receivers)
    unsigned TxE           : 1;  // Data register empty (transmitters)
    unsigned BERR          : 1;  // Bus error
    unsigned ARLO          : 1;  // Arbitration lost (master mode)
    unsigned AF            : 1;  // Acknowledge failure
    unsigned OVR           : 1;  // Overrun/Underrun
    unsigned PECERR        : 1;  // PEC Error in reception
    unsigned Reserved2     : 1;  // must be kept at reset value
    unsigned TIMEOUT       : 1;  // Timeout or Tlow error
    unsigned SMBALERT      : 1;  // SMBus alert
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_sr1;
typedef struct { // t_register_stm32l1xx_i2c_sr2      I2C Status Register 2 (->RM0038 p.645)
    unsigned MSL           : 1;  // Master/ Slave
    unsigned BUSY          : 1;  // Bus busy
    unsigned TRA           : 1;  // Transmitter/receiver
    unsigned Reserved1     : 1;  // must be kept at reset value
    unsigned GENCALL       : 1;  // General call address (Slave mode)
    unsigned SMBDEFAULT    : 1;  // SMBus device default address (Slave mode)
    unsigned SMBHOST       : 1;  // SMBus host header (Slave mode)
    unsigned DUALF         : 1;  // Dual flag (Slave mode)
    unsigned PEC           : 8;  // Packet error checking register
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_sr2;
typedef struct { // t_register_stm32l1xx_i2c_ccr      I2C Clock Control Register (->RM0038 p.646)
    unsigned CCR           : 12; // Clock control register in Fast/Standard mode (Master mode)
    unsigned Reserved1     : 2;  // must be kept at reset value
    unsigned DUTY          : 1;  // Fast mode duty cycle
    unsigned Fast          : 1;  // I2C fast mode selection
} __attribute__( ( __packed__ ) ) t_register_stm32l1xx_i2c_ccr;
typedef struct { // t_t_register_stm32l1xx_i2crise    I2C Max Rise Time in Master Mode (->RM0038 p.647)
    unsigned TRISE         : 6;  // Maximum rise time in Fast/Standard mode (Master mode)
    unsigned Reserved1     : 10; // must be kept at reset value
} __attribute__( ( __packed__ ) ) t_t_register_stm32l1xx_i2crise;
typedef struct { // t_register_stm32f1xx_i2c          Inter-Integrated Circuit Interface

    t_register_stm32l1xx_i2c_cr1    CR1;
    unsigned PadWord1 : 16; // pad to next 32-bit address
    t_register_stm32l1xx_i2c_cr2    CR2;
    unsigned PadWord2 : 16; // pad to next 32-bit address
    t_register_stm32l1xx_i2c_oar1   OAR1;
    unsigned PadWord3 : 16; // pad to next 32-bit address
    t_register_stm32l1xx_i2c_oar2   OAR2;
    unsigned PadWord4 : 16; // pad to next 32-bit address
    t_register_stm32l1xx_i2c_dr     DR;
    unsigned PadWord5 : 16; // pad to next 32-bit address
    t_register_stm32l1xx_i2c_sr1    SR1;
    unsigned PadWord6 : 16; // pad to next 32-bit address
    t_register_stm32l1xx_i2c_sr2    SR2;
    unsigned PadWord7 : 16; // pad to next 32-bit address
    t_register_stm32l1xx_i2c_ccr    CCR;
    unsigned PadWord8 : 16; // pad to next 32-bit address
    t_t_register_stm32l1xx_i2crise  TRISE;
    unsigned PadWord9 : 16; // pad to next 32-bit address
} __attribute__( ( __packed__ ) ) t_stm32l1_register_i2c;

//--------------END I2C-----------------------------//

//--------------EXTI INTERRUPTS-----------------------------//
typedef struct { // EXTI_IMR - Interrupt Mask Register  (p.200)
    unsigned MR0  : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR1  : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR2  : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR3  : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR4  : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR5  : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR6  : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR7  : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR8  : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR9  : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR10 : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR11 : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR12 : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR13 : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR14 : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR15 : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR16 : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR17 : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR18 : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR19 : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR20 : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR21 : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR22 : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned MR23 : 1; //Interrupt Mask for line x. 1: not masked, 0: masked.
    unsigned Reserved1: 8;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_exti_imr;

typedef struct { // EXTI_EMR - Event Mask Register  (p.200)
    unsigned MR0  : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR1  : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR2  : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR3  : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR4  : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR5  : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR6  : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR7  : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR8  : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR9  : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR10 : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR11 : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR12 : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR13 : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR14 : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR15 : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR16 : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR17 : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR18 : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR19 : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR20 : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR21 : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR22 : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned MR23 : 1; //Event Mask for line x. 1: not masked, 0: masked.
    unsigned Reserved1: 8;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_exti_emr;

typedef struct { // EXTI_RTSR - Rising Edge Trigger Selection Register  (p.201)
    unsigned TR0  : 1; //Rising edge trigger event configuration bit of line x. (1: enabled)
    unsigned TR1  : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR2  : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR3  : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR4  : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR5  : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR6  : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR7  : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR8  : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR9  : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR10 : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR11 : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR12 : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR13 : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR14 : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR15 : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR16 : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR17 : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR18 : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR19 : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR20 : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR21 : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR22 : 1; //Rising edge trigger event configuration bit of line x.
    unsigned TR23 : 1; //Rising edge trigger event configuration bit of line x.
    unsigned Reserved1: 8;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_exti_rtsr;

typedef struct { // EXTI_FTSR - Falling Edge Trigger Selection Register  (p.201)
    unsigned TR0  : 1; //Falling edge trigger event configuration bit of line x. (1: enabled)
    unsigned TR1  : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR2  : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR3  : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR4  : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR5  : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR6  : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR7  : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR8  : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR9  : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR10 : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR11 : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR12 : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR13 : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR14 : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR15 : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR16 : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR17 : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR18 : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR19 : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR20 : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR21 : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR22 : 1; //Falling edge trigger event configuration bit of line x.
    unsigned TR23 : 1; //Falling edge trigger event configuration bit of line x.
    unsigned Reserved1: 8;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_exti_ftsr;

typedef struct { // EXTI_SWIER - Software Interrupt Event Register  (p.202)
    unsigned SWIER0  : 1; //Software interrupt on line x.
    unsigned SWIER1  : 1; //Software interrupt on line x.
    unsigned SWIER2  : 1; //Software interrupt on line x.
    unsigned SWIER3  : 1; //Software interrupt on line x.
    unsigned SWIER4  : 1; //Software interrupt on line x.
    unsigned SWIER5  : 1; //Software interrupt on line x.
    unsigned SWIER6  : 1; //Software interrupt on line x.
    unsigned SWIER7  : 1; //Software interrupt on line x.
    unsigned SWIER8  : 1; //Software interrupt on line x.
    unsigned SWIER9  : 1; //Software interrupt on line x.
    unsigned SWIER10 : 1; //Software interrupt on line x.
    unsigned SWIER11 : 1; //Software interrupt on line x.
    unsigned SWIER12 : 1; //Software interrupt on line x.
    unsigned SWIER13 : 1; //Software interrupt on line x.
    unsigned SWIER14 : 1; //Software interrupt on line x.
    unsigned SWIER15 : 1; //Software interrupt on line x.
    unsigned SWIER16 : 1; //Software interrupt on line x.
    unsigned SWIER17 : 1; //Software interrupt on line x.
    unsigned SWIER18 : 1; //Software interrupt on line x.
    unsigned SWIER19 : 1; //Software interrupt on line x.
    unsigned SWIER20 : 1; //Software interrupt on line x.
    unsigned SWIER21 : 1; //Software interrupt on line x.
    unsigned SWIER22 : 1; //Software interrupt on line x.
    unsigned SWIER23 : 1; //Software interrupt on line x.
    unsigned Reserved1: 8;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_exti_swier;

typedef struct { // EXTI_PR - EXTI Pending Register  (p.203)
    unsigned PR0  : 1; //Pending bit.
    unsigned PR1  : 1; //Pending bit.
    unsigned PR2  : 1; //Pending bit.
    unsigned PR3  : 1; //Pending bit.
    unsigned PR4  : 1; //Pending bit.
    unsigned PR5  : 1; //Pending bit.
    unsigned PR6  : 1; //Pending bit.
    unsigned PR7  : 1; //Pending bit.
    unsigned PR8  : 1; //Pending bit.
    unsigned PR9  : 1; //Pending bit.
    unsigned PR10 : 1; //Pending bit.
    unsigned PR11 : 1; //Pending bit.
    unsigned PR12 : 1; //Pending bit.
    unsigned PR13 : 1; //Pending bit.
    unsigned PR14 : 1; //Pending bit.
    unsigned PR15 : 1; //Pending bit.
    unsigned PR16 : 1; //Pending bit.
    unsigned PR17 : 1; //Pending bit.
    unsigned PR18 : 1; //Pending bit.
    unsigned PR19 : 1; //Pending bit.
    unsigned PR20 : 1; //Pending bit.
    unsigned PR21 : 1; //Pending bit.
    unsigned PR22 : 1; //Pending bit.
    unsigned PR23 : 1; //Pending bit.
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_exti_pr;

typedef struct { // External Interrupt Registers
    stm32l1_t_register_stm32l1xx_exti_imr     IMR;    // Interrupt Mask Register
    stm32l1_t_register_stm32l1xx_exti_emr     EMR;    // Event Mask Register
    stm32l1_t_register_stm32l1xx_exti_rtsr    RTSR;   // Rising Edge Trigger Selection Register
    stm32l1_t_register_stm32l1xx_exti_ftsr    FTSR;   // Falling Edge Trigger Selection Register
    stm32l1_t_register_stm32l1xx_exti_swier   SWIER;  // Software Interrupt Event Register
    stm32l1_t_register_stm32l1xx_exti_pr      PR;     // EXTI Pending Register

} __attribute__( ( __packed__ ) ) t_stm32l1_register_exti;

//--------------END EXTI INTERRUPTS-----------------------------//

//-----------------------TIMERS---------------------------------//
//---Genera-purpose timer (TIM2-4) Registers---//
typedef struct {// TIMx_CR1 - control reg 1 (p. 375)
    unsigned C_EN:  1;
    unsigned UDIS: 1;
    unsigned URS:  1;
    unsigned OPM:  1;
    unsigned DIR:  1;
    unsigned CMS:  2;
    unsigned ARPE: 1;
    unsigned CKD:  2;
    unsigned Reserved: 22;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_cr1;

typedef struct {// TIMx_CR2 - control reg 2 (p. 377)
    unsigned Reserved1:  3;
    unsigned CCDS:       1;
    unsigned MMS:        3;
    unsigned TI1S:       1;
    unsigned Reserved2:  24;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_cr2;

typedef struct {// TIMx_SMCR - slave mode control reg (p.378)
    unsigned SMS:        3; //see
    unsigned OCCS:       1;
    unsigned TS:         3;
    unsigned MSM:        1;
    unsigned ETF:        4;
    unsigned ETPS:       2;
    unsigned ECE:        1;
    unsigned ETP:        1;
    unsigned Reserved:   16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_smcr;

typedef struct {// TIMx_DIER - DMA/Interrupt enable reg (p.380)
    unsigned UIE:    1;
    unsigned CC1IE:  1;
    unsigned CC2IE:  1;
    unsigned CC3IE:  1;
    unsigned CC4IE:  1;
    unsigned Reserved1:  1;
    unsigned TIE:  1;
    unsigned Reserved2:  1;
    unsigned UDE:  1;
    unsigned CC1DE:  1;
    unsigned CC2DE:  1;
    unsigned CC3DE:  1;
    unsigned CC4DE:  1;
    unsigned Reserved3:  1;
    unsigned TDE:  1;
    unsigned Reserved4:  17;

} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_dier;

typedef struct {// TIMx_SR - Status Register (p.381)
    unsigned UIF:       1;
    unsigned CC1IF:     1;
    unsigned CC2IF:     1;
    unsigned CC3IF:     1;
    unsigned CC4IF:     1;
    unsigned Reserved1: 1;
    unsigned TIF:       1;
    unsigned Reserved2: 2;
    unsigned CC1OF:     1;
    unsigned CC2OF:     1;
    unsigned CC3OF:     1;
    unsigned CC4OF:     1;
    unsigned Reserved3: 19;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_sr;

typedef struct {// TIMx_EGR - Event Generation Register
    unsigned UG:        1;
    unsigned CC1G:      1;
    unsigned CC2G:      1;
    unsigned CC3G:      1;
    unsigned CC4G:      1;
    unsigned Reserved1: 1;
    unsigned TG:        1;
    unsigned Reserved2: 25;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_egr;

typedef struct {// TIMx_CCMR1 (output compare mode) - capture/compare mode reg 1 (p.384)
    unsigned CC1S:  2;
    unsigned OC1FE: 1;
    unsigned OC1PE: 1;
    unsigned OC1M:  3;
    unsigned OC1CE: 1;
    unsigned CC2S:  2;
    unsigned OC2FE: 1;
    unsigned OC2PE: 1;
    unsigned OC2M:  3;
    unsigned OC2CE: 1;
    unsigned Reserved: 16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_ccmr1_compare_mode;

typedef struct {// TIMx_CCMR1 (input capture mode) - capture/compare mode reg 1 (p.384)
    unsigned CC1S:   2;
    unsigned IC1PSC: 2;
    unsigned IC1F:   4;
    unsigned CC2S:   2;
    unsigned IC2PSC: 2;
    unsigned IC2F:   4;
    unsigned Reserved: 16;
} __attribute__( ( __packed__ ) ) t_stm32l1_timx_ccmr1_input_capture_mode;

typedef struct {// TIMx_CCMR2 (output compare mode) - capture/compare mode reg 2 (p.387)
    unsigned CC3S:  2;
    unsigned OC3FE: 1;
    unsigned OC3PE: 1;
    unsigned OC3M:  3;
    unsigned OC3CE: 1;
    unsigned CC4S:  2;
    unsigned OC4FE: 1;
    unsigned OC4PE: 1;
    unsigned OC4M:  3;
    unsigned OC4CE: 1;
    unsigned Reserved: 16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_ccmr2_compare_mode;

typedef struct {// TIMx_CCMR2 (input capture mode) - capture/compare mode reg 2 (p.387)
    unsigned CC3S:   2;
    unsigned IC3PSC: 2;
    unsigned IC3F:   4;
    unsigned CC4S:   2;
    unsigned IC4PSC: 2;
    unsigned IC4F:   4;
    unsigned Reserved: 16;
} __attribute__( ( __packed__ ) ) t_stm32l1_timx_ccmr2_input_capture_mode;

typedef struct {// TIMx_CCER - capture/compare enable reg (p.388)
    unsigned CC1E:      1;
    unsigned CC1P:      1;
    unsigned Reserved1: 1;
    unsigned CC1NP:     1;
    unsigned CC2E:      1;
    unsigned CC2P:      1;
    unsigned Reserved2: 1;
    unsigned CC2NP:     1;
    unsigned CC3E:      1;
    unsigned CC3P:      1;
    unsigned Reserved3: 1;
    unsigned CC3NP:     1;
    unsigned CC4E:      1;
    unsigned CC4P:      1;
    unsigned Reserved4: 1;
    unsigned CC4NP:     1;
    unsigned Reserved5: 16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_ccer;

typedef struct {// TIMx_CNT - counter (p.390)
    unsigned CNT:  16;
    unsigned Reserved: 16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_cnt;

typedef struct {// TIMx_PSC - prescalar (p.390)
    unsigned PSC:  16;
    unsigned Reserved: 16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_psc;

typedef struct {// TIMx_ARR - auto-reload reg (p.390)
    unsigned ARR:  16;
    unsigned Reserved: 16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_arr;

typedef struct {// TIMx_CCR1 - capture/compare reg 1 (p.391)
    unsigned CCR1:  16;
    unsigned Reserved: 16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_ccr1;

typedef struct {// TIMx_CCR2 - capture/compare reg 2 (p.391)
    unsigned CCR2:  16;
    unsigned Reserved: 16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_ccr2;

typedef struct {// TIMx_CCR3 - capture/compare reg 3 (p.392)
    unsigned CCR3:  16;
    unsigned Reserved: 16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_ccr3;

typedef struct {// TIMx_CCR4 - capture/compare reg 4 (p.392)
    unsigned CCR4:  16;
    unsigned Reserved: 16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_ccr4;

typedef struct {// TIMx_DCR - DMA control reg (p.393)
    unsigned  DBA:          5;
    unsigned  Reserved1:    3;
    unsigned  DBL:          5;
    unsigned  Reserved2:    19;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_dcr;

typedef struct {// TIMx_DMAR - DMA address for full transfer (p.393)
    unsigned  DMAB:     16;
    unsigned Reserved: 16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_timx_dmar;
//---END Genera-purpose timer (TIM2-5) Registers---//



//---Timer-specific Registers for General-purpose timers---//
typedef struct {// TIM2_OR - TIM2 option reg (p.395)
    unsigned ITR1_RMP:      1;
    unsigned Reserved:      31;
} __attribute__( ( __packed__ ) ) t_stm32l1_tim2_or;

typedef struct {// TIM3_OR - TIM3 option reg (p.395)
    unsigned ITR2_RMP:      1;
    unsigned Reserved:      31;
} __attribute__( ( __packed__ ) ) t_stm32l1_tim3_or;
//---END Timer-specific Registers for General-purpose timers---//



//-------------COMPLETE TIMER DEVICE REGISTER MAPS-------------//
typedef struct { // General-Purpose Timer Registers (TIM2 - TIM4) - with output compare registers
    stm32l1_t_register_stm32l1xx_timx_cr1                  CR1;    //
    stm32l1_t_register_stm32l1xx_timx_cr2                  CR2;    //
    stm32l1_t_register_stm32l1xx_timx_smcr                 SMCR;   //
    stm32l1_t_register_stm32l1xx_timx_dier                 DIER;   //
    stm32l1_t_register_stm32l1xx_timx_sr                   SR;     //
    stm32l1_t_register_stm32l1xx_timx_egr                  ER;     //
    stm32l1_t_register_stm32l1xx_timx_ccmr1_compare_mode   CCMR1;  //
    stm32l1_t_register_stm32l1xx_timx_ccmr2_compare_mode   CCMR2;  //
    stm32l1_t_register_stm32l1xx_timx_ccer                 CCER;   //
    stm32l1_t_register_stm32l1xx_timx_cnt                  CNT;    //counter
    stm32l1_t_register_stm32l1xx_timx_psc                  PSC;    //prescalar
    stm32l1_t_register_stm32l1xx_timx_arr                  ARR;    //auto-reload reg
    stm32l1_t_reserved_reg              XXX;    //reserved
    stm32l1_t_register_stm32l1xx_timx_ccr1                 CCR1;   //
    stm32l1_t_register_stm32l1xx_timx_ccr2                 CCR2;   //
    stm32l1_t_register_stm32l1xx_timx_ccr3                 CCR3;   //
    stm32l1_t_register_stm32l1xx_timx_ccr4                 CCR4;   //
    stm32l1_t_reserved_reg              YYY;    //reserved
    stm32l1_t_register_stm32l1xx_timx_dcr                  DCR;    //
    stm32l1_t_register_stm32l1xx_timx_dmar                 DMAR;   //
} __attribute__( ( __packed__ ) ) stm32l1_t_registerim2_to_tim4_output_compare_mode_t;

typedef struct { // General-Purpose Timer Registers (TIM2 - TIM4) - with input capture registers
    stm32l1_t_register_stm32l1xx_timx_cr1                      CR1;    //
    stm32l1_t_register_stm32l1xx_timx_cr2                      CR2;    //
    stm32l1_t_register_stm32l1xx_timx_smcr                     SMCR;   //
    stm32l1_t_register_stm32l1xx_timx_dier                     DIER;   //
    stm32l1_t_register_stm32l1xx_timx_sr                       SR;     //
    stm32l1_t_register_stm32l1xx_timx_egr                      ER;     //
    t_stm32l1_timx_ccmr1_input_capture_mode CCMR1;  //
    t_stm32l1_timx_ccmr2_input_capture_mode CCMR2;  //
    stm32l1_t_register_stm32l1xx_timx_ccer                     CCER;   //
    stm32l1_t_register_stm32l1xx_timx_cnt                      CNT;    //counter
    stm32l1_t_register_stm32l1xx_timx_psc                      PSC;    //prescalar
    stm32l1_t_register_stm32l1xx_timx_arr                      ARR;    //auto-reload reg
    stm32l1_t_reserved_reg                  XXX;    //reserved
    stm32l1_t_register_stm32l1xx_timx_ccr1                     CCR1;   //
    stm32l1_t_register_stm32l1xx_timx_ccr2                     CCR2;   //
    stm32l1_t_register_stm32l1xx_timx_ccr3                     CCR3;   //
    stm32l1_t_register_stm32l1xx_timx_ccr4                     CCR4;   //
    stm32l1_t_reserved_reg                  YYY;    //reserved
    stm32l1_t_register_stm32l1xx_timx_dcr                      DCR;    //
    stm32l1_t_register_stm32l1xx_timx_dmar                     DMAR;   //
} __attribute__( ( __packed__ ) ) stm32l1_t_registerim2_to_tim4_input_capture_mode_t;
//---------END COMPLETE TIMER DEVICE REGISTER MAPS-------------//

//-------------------------------------------END TIMERS--------------------------------------------------//

//----------------------------------------------DAC------------------------------------------------------//
typedef struct {//CR - control register (p.280)
    unsigned  EN1:              1;
    unsigned  BOFF1:            1;
    unsigned  TEN1:             1;
    unsigned  TSEL1:            3;
    unsigned  WAVE1:            2;
    unsigned  MAMP1:            4;
    unsigned  DMA_EN1:           1;
    unsigned  DMAUDRIE1:        1;
    unsigned  Reserved1:        2;
    unsigned  EN2:              1;
    unsigned  BOFF2:            1;
    unsigned  TEN2:             1;
    unsigned  TSEL2:            3;
    unsigned  WAVE2:            2;
    unsigned  MAMP2:            4;
    unsigned  DMA_EN2:           1;
    unsigned  DMAUDRIE2:        1;
    unsigned  Reserved2:        2;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_dac_cr;

typedef struct {//SWTRIGR
    unsigned  SWTRIG1:          1;
    unsigned  SWTRIG2:          1;
    unsigned  Reserved:         30;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_dac_swtrigr;

typedef struct {//DAC_DHR12R1
    unsigned  DACC1DHR:         12;
    unsigned  Reserved:         20;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_dac_dhr12r1;

typedef struct {//DHR12L1
    unsigned  Reserved1:         4;
    unsigned  DACC1DHR:         12;
    unsigned  Reserved2:        16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_dac_dhr12l1;

typedef struct {//DHR8R1
    unsigned  DACC1DHR:          8;
    unsigned  Reserved:         24;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_dac_dhr8r1;

typedef struct {//DHR12R2
    unsigned  DACC2DHR:         12;
    unsigned  Reserved:         20;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_dac_dhr12r2;

typedef struct {//DHR12L2
    unsigned  Reserved1:         4;
    unsigned  DACC2DHR:         12;
    unsigned  Reserved2:        16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_dac_dhr12l2;

typedef struct {//DHR8R2
    unsigned  DACC2DHR:          8;
    unsigned  Reserved:         24;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_dac_dhr8r2;

typedef struct {//DHR12RD
    unsigned  DACC1DHR:         12;
    unsigned  Reserved1:         4;
    unsigned  DACC2DHR:         12;
    unsigned  Reserved2:         4;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_dac_dhr12rd;

typedef struct {//DHR12LD
    unsigned  Reserved1:         4;
    unsigned  DACC1DHR:         12;
    unsigned  Reserved2:         4;
    unsigned  DACC2DHR:         12;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_dac_dhr12ld;

typedef struct {//DHR8RD
    unsigned  DACC1DHR:          8;
    unsigned  DACC2DHR:          8;
    unsigned  Reserved:         16;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_dac_dhr8rd;

typedef struct {//DOR1
    unsigned  DACC1DOR:         12;
    unsigned  Reserved:         20;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_dac_dor1;

typedef struct {//DOR2
    unsigned  DACC2DOR:         12;
    unsigned  Reserved:         20;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_dac_dor2;

typedef struct {//SR - status register  (p.288)
    unsigned  Reserved1:         13;
    unsigned  DMAUDR1:            1;
    unsigned  Reserved2:         15;
    unsigned  DMAUDR2:            1;
    unsigned  Reserved3:          2;
} __attribute__( ( __packed__ ) ) stm32l1_t_register_stm32l1xx_dac_sr;


typedef struct {//DAC register map
    stm32l1_t_register_stm32l1xx_dac_cr                CR; //
    stm32l1_t_register_stm32l1xx_dac_swtrigr           SWTRIGR;
    stm32l1_t_register_stm32l1xx_dac_dhr12r1           DHR12R1;
    stm32l1_t_register_stm32l1xx_dac_dhr12l1           DHR12L1;
    stm32l1_t_register_stm32l1xx_dac_dhr8r1            DHR8R1;
    stm32l1_t_register_stm32l1xx_dac_dhr12r2           DHR12R2;
    stm32l1_t_register_stm32l1xx_dac_dhr12l2           DHR12L2;
    stm32l1_t_register_stm32l1xx_dac_dhr8r2            DHR8R2;
    stm32l1_t_register_stm32l1xx_dac_dhr12rd           DHR12RD;
    stm32l1_t_register_stm32l1xx_dac_dhr12ld           DHR12LD;
    stm32l1_t_register_stm32l1xx_dac_dhr8rd            DHR8RD;
    stm32l1_t_register_stm32l1xx_dac_dor1              DOR1;
    stm32l1_t_register_stm32l1xx_dac_dor2              DOR2;
    stm32l1_t_register_stm32l1xx_dac_sr                SR;
} __attribute__( ( __packed__ ) ) t_stm32l1_register_dac;



//------------------------------------------END DAC------------------------------------------------------//




//} REGISTER STRUCTS_______________________________________________________

//{ STM32 Registers ******************************************************


/*
typedef struct {//
    unsigned  :     ;
} __attribute__((__packed__)) t_stm32l1_unknown;
*/


































//} STM32 Registers

#endif //STM32L1_REGISTERS_H
