#ifndef STM32L1_USART_H
#define STM32L1_USART_H

/** { DEPRECATED_stm32l1_usart.h **********************************************
 
                           The ToolChain
                      
   Low-Level interface for USART device.

   Structures, Enums and Defines being required by high-level usart and application.

   Note: See ttc_usart.h for description of stm32l1 independent USART implementation.
  
   Authors: Greg Knoll 2013

 
}*/

//{ Defines/ TypeDefs ****************************************************
#define TTC_USART_MAX_AMOUNT 3
//} Defines

//{ Includes *************************************************************
#include "stm32l1_usart_types.h"
#include "DEPRECATED_stm32l1_gpio.h"

#include "../ttc_usart_types.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"
#include "../ttc_interrupt.h"


#ifdef USE_STDPERIPH_DRIVER
#include <stm32l1xx_usart.h>
#include <stm32l1xx_rcc.h>  //RCC_APB2PeriphClockCmd()
#include <stm32l1xx_gpio.h> //for GPIO_PinAFConfig()
#endif
//} Includes

//{ Macro definitions ****************************************************

#define _driver_ttc_usart_init(USART_Generic)     stm32l1_usart_init(USART_Generic)

//}  Macro definitions

//{ Function prototypes **************************************************

/* initializes single USART
 * @param USART_Generic   filled out struct ttc_usart_config_t (NOTE: referenced struct must stay in memory as long as device is in use!)
 * @return  == 0:         USART has been initialized successfully; != 0: error-code
 */
ttc_usart_errorcode_e stm32l1_usart_init(ttc_usart_config_t* USART_Generic);










    //-----------Private Functions----------------//

/* general interrupt handler for received byte; reads received byte and pass it to receiveSingleByte()
 * @param PhysicalIndex    {0..(TTC_AMOUNT_USARTS-1)} = USART device to use (0=USART1, ...)
 * @param USART_Generic    pointer to configuration of corresponding USART
 */
void _stm32l1_usart_rx_isr(physical_index_t PhysicalIndex, void* Argument);

/* general interrupt handler for that will call _ttc_usart_tx_isr() to provide next byte to send
 * @param PhysicalIndex    {0..(TTC_AMOUNT_USARTS-1)} = USART device to use (0=USART1, ...)
 * @param USART_Generic    pointer to configuration of corresponding USART
 */
void _stm32l1_usart_tx_isr(physical_index_t PhysicalIndex, void* Argument);
//} Function prototypes

#endif //STM32L1_USART_H
