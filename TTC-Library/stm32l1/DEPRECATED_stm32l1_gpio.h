#ifndef STM32L1_GPIO_H
#define STM32L1_GPIO_H

/** { DEPRECATED_stm32l1_gpio.h **********************************************
 
                           The ToolChain
                      
   High-Level interface for GPIO device.

   Structures, Enums and Defines being required by high-level gpio and application.

   Note: See ttc_gpio.h for description of stm32l1 independent GPIO implementation.
  
   Authors: 

 
}*/

//{ Defines/ TypeDefs ****************************************************

#define TTC_GPIO_MAX_AMOUNT 3

// amount of port pins per bank
#define TTC_GPIO_MAX_PINS 51

#define ttc_gpio_bank_t  stm32l1_GPIO_e
#define ttc_Port_t     stm32l1_Port_t  //bank+pin

#define TTC_GPIO_BANK_A stm32l1_GPIOA
#define TTC_GPIO_BANK_B stm32l1_GPIOB
#define TTC_GPIO_BANK_C stm32l1_GPIOC

//{ Define shortcuts for all pins of GPIO Bank A
#define PIN_PA0  TTC_GPIO_BANK_A,0
#define PIN_PA1  TTC_GPIO_BANK_A,1
#define PIN_PA2  TTC_GPIO_BANK_A,2
#define PIN_PA3  TTC_GPIO_BANK_A,3
#define PIN_PA4  TTC_GPIO_BANK_A,4
#define PIN_PA5  TTC_GPIO_BANK_A,5
#define PIN_PA6  TTC_GPIO_BANK_A,6
#define PIN_PA7  TTC_GPIO_BANK_A,7
#define PIN_PA8  TTC_GPIO_BANK_A,8
#define PIN_PA9  TTC_GPIO_BANK_A,9
#define PIN_PA10 TTC_GPIO_BANK_A,10
#define PIN_PA11 TTC_GPIO_BANK_A,11
#define PIN_PA12 TTC_GPIO_BANK_A,12
#define PIN_PA13 TTC_GPIO_BANK_A,13
#define PIN_PA14 TTC_GPIO_BANK_A,14
#define PIN_PA15 TTC_GPIO_BANK_A,15
//}BankA
//{ Define shortcuts for all pins of GPIO Bank B
#define PIN_PB0  TTC_GPIO_BANK_B,0
#define PIN_PB1  TTC_GPIO_BANK_B,1
#define PIN_PB2  TTC_GPIO_BANK_B,2
#define PIN_PB3  TTC_GPIO_BANK_B,3
#define PIN_PB4  TTC_GPIO_BANK_B,4
#define PIN_PB5  TTC_GPIO_BANK_B,5
#define PIN_PB6  TTC_GPIO_BANK_B,6
#define PIN_PB7  TTC_GPIO_BANK_B,7
#define PIN_PB8  TTC_GPIO_BANK_B,8
#define PIN_PB9  TTC_GPIO_BANK_B,9
#define PIN_PB10 TTC_GPIO_BANK_B,10
#define PIN_PB11 TTC_GPIO_BANK_B,11
#define PIN_PB12 TTC_GPIO_BANK_B,12
#define PIN_PB13 TTC_GPIO_BANK_B,13
#define PIN_PB14 TTC_GPIO_BANK_B,14
#define PIN_PB15 TTC_GPIO_BANK_B,15
//}BankB
//{ Define shortcuts for all pins of GPIO Bank C
#define PIN_PC0  TTC_GPIO_BANK_C,0
#define PIN_PC1  TTC_GPIO_BANK_C,1
#define PIN_PC2  TTC_GPIO_BANK_C,2
#define PIN_PC3  TTC_GPIO_BANK_C,3
#define PIN_PC4  TTC_GPIO_BANK_C,4
#define PIN_PC5  TTC_GPIO_BANK_C,5
#define PIN_PC6  TTC_GPIO_BANK_C,6
#define PIN_PC7  TTC_GPIO_BANK_C,7
#define PIN_PC8  TTC_GPIO_BANK_C,8
#define PIN_PC9  TTC_GPIO_BANK_C,9
#define PIN_PC10 TTC_GPIO_BANK_C,10
#define PIN_PC11 TTC_GPIO_BANK_C,11
#define PIN_PC12 TTC_GPIO_BANK_C,12
#define PIN_PC13 TTC_GPIO_BANK_C,13
#define PIN_PC14 TTC_GPIO_BANK_C,14
#define PIN_PC15 TTC_GPIO_BANK_C,15
//}BankC

//} Defines
//{ Includes *************************************************************
#include "../ttc_basic.h"
#include "stm32l1_gpio_types.h"
#include "../ttc_gpio_types.h"
//#include "../ttc_task.h"  -> remove from template or causes problems
//#include "../ttc_memory.h"-> remove from template or causes problems
#include <stm32l1xx.h>
#include <stm32l1xx_rcc.h>
#include <stm32l1xx_gpio.h>

//} Includes
//{ Macro definitions ****************************************************

#define _driver_ttc_gpio_init(GPIOx, Pin, Type, Speed)                stm32l1_gpio_init(GPIOx, Pin, Type, Speed)
#define _driver_ttc_gpio_set(...)                                     stm32l1_gpio_set(__VA_ARGS__)
#define _driver_ttc_gpio_get(...)                                     stm32l1_gpio_get(__VA_ARGS__)
#define _driver_ttc_gpio_clr(...)                                     DEPRECATED_stm32l1_gpio.clr(__VA_ARGS__)

#define _driver_ttc_gpio_init_variable(Port, Bank, Pin, Type, Speed)  stm32l1_gpio_init_variable(Port, Bank, Pin, Type, Speed)
#define _driver_ttc_gpio_variable(PORT,BANK,PIN)                      stm32l1_gpio_variable(PORT,BANK,PIN)
#define _driver_ttc_gpio_get_variable(PORT)                           stm32l1_gpio_get_variable(PORT)
#define _driver_ttc_gpio_set_variable(PORT)                           stm32l1_gpio_set_variable(PORT)
#define _driver_ttc_gpio_clr_variable(PORT)                           DEPRECATED_stm32l1_gpio.clr_variable(PORT)

#define _driver_ttc_gpio_create_index8(BANK,PIN)                      DEPRECATED_stm32l1_gpio.create_index8(BANK,PIN)
#define _driver_ttc_gpio_from_index8(INDEX,BANK,PIN)                  stm32l1_gpio_from_index8(INDEX,BANK,PIN)


//} Includes
//{ Function prototypes **************************************************
//------------regular init/get/set/clear-----------//
/** initializes single port bit for use as input/ output
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C
 * @param Pin    0..7
 * @param Type   one of GPIO_Mode_AIN, GPIO_Mode_IN_FLOATING, GPIO_Mode_IPD, GPIO_Mode_IPU, GPIO_Mode_Out_OD, GPIO_Mode_Out_PP, GPIO_Mode_Out_OD, GPIO_Mode_AF_PP, GPIO_Mode_AF_OD
 * @param Speed  one of GPIO_Speed_2MHz, GPIO_Speed_10MHz, tgs_Max
 */
void stm32l1_gpio_init(stm32l1_GPIO_e GPIOx, u8_t Pin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed);


/** read current value of input pin
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, TTC_GPIO_BANK_D, TTC_GPIO_BANK_E, TTC_GPIO_BANK_F
 * @param Pin    0..15
 */
bool stm32l1_gpio_get(stm32l1_GPIO_e GPIOx, u8_t Pin);

/** set output pin to logical one
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, TTC_GPIO_BANK_D, TTC_GPIO_BANK_E, TTC_GPIO_BANK_F
 * @param Pin    0..15
 */
void DEPRECATED_stm32l1_gpio.clr(stm32l1_GPIO_e GPIOx, u8_t Pin);

/** set output pin to logical zero
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, TTC_GPIO_BANK_D, TTC_GPIO_BANK_E, TTC_GPIO_BANK_F
 * @param Pin    0..15
 */
void stm32l1_gpio_set(stm32l1_GPIO_e GPIOx, u8_t Pin);
//-------------------------------------------------//



//-----------with Variables/Ports-----------------//
/** allows to load struct from list type definition
 *
 * @param Port   points to structure to be filled with given data
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, TTC_GPIO_BANK_D, TTC_GPIO_BANK_E, TTC_GPIO_BANK_F
 * @param Pin    0..15
 *
 * Example:
 * #define PB_LED1=TTC_GPIO_BANK_C,6
 * stm32l1_Port_t Port;
 * stm32l1_gpio_variable(&Port, PB_LED1);
 * stm32l1_gpio_init_variable(&Port, GPIO_Mode_Out_PP);
 * stm32l1_gpio_set_variable(&Port);
 */
void stm32l1_gpio_variable(stm32l1_Port_t* Port, stm32l1_GPIO_e GPIOx, u8_t Pin);

/** initializes single port bit for use as input/ output with given speed by use of a state variable
 *
 * @param Port   points to structure to be filled with given data
 * @param Bank  GPIO bank to use (-> ttc_gpio_types.h:ttc_gpio_bank_t)
 *               Note: Bank, Pin can be given as one argument by use of Pin_Pxn macro (GPIOA, 7) = Pin_PA7
 * @param Pin    0..15
 * @param Type   mode to use (-> ttc_gpio_types.h:ttc_gpio_mode_e)
 * @param Speed  speed to use (-> ttc_gpio_types.h:ttc_gpio_speed_e)
 */
void stm32l1_gpio_init_variable(stm32l1_Port_t* Port, ttc_gpio_bank_t Bank, u8_t Pin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed);


/** read current value of input pin
 * @param Port  filled out struct as being returned by stm32l1_gpio_variable()
 */
bool stm32l1_gpio_get_variable(stm32l1_Port_t* Port);

/** set output pin to logical one
 * @param Port  filled out struct as being returned by stm32l1_gpio_variable()
 */
void stm32l1_gpio_set_variable(stm32l1_Port_t* Port);

/** set output pin to logical zero
 * @param Port  filled out struct as being returned by stm32l1_gpio_variable()
 */
void DEPRECATED_stm32l1_gpio.clr_variable(stm32l1_Port_t* Port);

/** creates numeric 8-bit representing given GPIO-bank and pin number
 *
 * @param GPIOx  GPIO bank to use (-> ttc_gpio_types.h:ttc_gpio_bank_t)
 *               Note: GPIOx, Pin can be given as one argument by use of Pin_Pxn macro (TTC_GPIO_BANK_A, 7) = Pin_PA7
 * @param Pin    0..15
 * @return       8-bit value representing given GPIO-pin in a memory friendly format
 */
u8_t DEPRECATED_stm32l1_gpio.create_index8(ttc_gpio_bank_t GPIOx, u8_t Pin);

/** restores GPIO-bank and pin number from 8-bit value
 *
 * @param PhysicalIndex  8-bit value representing GPIO-bank + pin
 * @param GPIOx          loaded with GPIO bank (-> ttc_gpio_types.h:ttc_gpio_bank_t)
 * @param Pin            loaded with pin number
 */
void stm32l1_gpio_from_index8(u8_t PhysicalIndex, GPIO_TypeDef** GPIOx, u8_t* Pin);


//------------Private Functions----------------------------//
/** Power the GPIO bank by enabling its clock
 * @param GPIOx          GPIO bank to power
 */
void _DEPRECATED_stm32l1_gpio.clock_enable(stm32l1_GPIO_e GPIOx);

/** GPIO Standard Peripheral definition
 * @param  Pin    Pin number
 * @return Standard Peripheral definition of the pin
 */
//uint16_t _stm32l_stndLib_gpio_pin(u8_t Pin);
GPIOSpeed_TypeDef _stm32l1_map_Speed(ttc_gpio_speed_e Speed);

//} Function prototypes


#endif //STM32L1_GPIO_H
