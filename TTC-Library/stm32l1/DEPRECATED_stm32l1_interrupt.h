#ifndef STM32L1_INTERRUPT_H
#define STM32L1_INTERRUPT_H

/** { stm32l1_interrupt.h **********************************************
 
                           The ToolChain
                      
   High-Level interface for INTERRUPT device.

   Structures, Enums and Defines being required by high-level interrupt and application.

   Note: See ttc_interrupt.h for description of stm32l1 independent INTERRUPT implementation.
  
   Authors: 

 
}*/
//{ Defines/ TypeDefs ****************************************************

//{ Compatibility interface for ttc_interrupt.h **************************
//
// We define all DRIVER-functions that are supported for stm32f1x architecture
//

#define _driver_ttc_interrupt_enable(Type, PhysicalIndex, EnableIRQ) \
             stm32l1_interrupt_enable(Type, PhysicalIndex, EnableIRQ)

#define _driver_ttc_interrupt_init(Type, PhysicalIndex) \
             stm32l1_interrupt_init(Type, PhysicalIndex)

#define _driver_ttc_interrupt_all_enable()   __enable_irq()

#define _driver_ttc_interrupt_all_disable()  __disable_irq()

#define _driver_ttc_interrupt_init_usart(Type, PhysicalIndex) \
             stm32l1_interrupt_init_usart(Type, PhysicalIndex)

#define _driver_ttc_interrupt_init_gpio(Type, PhysicalIndex) \
             stm32l1_interrupt_init_gpio(Type, PhysicalIndex)

#define _driver_ttc_interrupt_enable_usart(Type, PhysicalIndex, EnableIRQ) \
             stm32l1_interrupt_enable_usart(Type, PhysicalIndex, EnableIRQ)

#define _driver_ttc_interrupt_enable_gpio(Type, PhysicalIndex, EnableIRQ) \
             stm32l1_interrupt_enable_gpio(Type, PhysicalIndex, EnableIRQ)

#define physical_index_t u8_t


#define _driver_ttc_interrupt_init_spi(Type, PhysicalIndex)         stm32l1_interrupt_init_spi(Type, PhysicalIndex)

#define _driver_ttc_interrupt_init_radio(Type, PhysicalIndex)       stm32l1_interrupt_init_radio(Type, PhysicalIndex)

#define _driver_ttc_interrupt_init_timer(Type, PhysicalIndex)       stm32l1_interrupt_init_timer(Type, PhysicalIndex)

#define _driver_ttc_interrupt_init_i2c(Type, PhysicalIndex)         stm32l1_interrupt_init_i2c(Type, PhysicalIndex)

#define _driver_ttc_interrupt_enable_spi(Type, PhysicalIndex, EnableIRQ)       stm32l1_interrupt_enable_spi(Type, PhysicalIndex, EnableIRQ)

#define _driver_ttc_interrupt_enable_radio(Type, PhysicalIndex, EnableIRQ)     stm32l1_interrupt_enable_radio(Type, PhysicalIndex, EnableIRQ)

#define _driver_ttc_interrupt_enable_timer(Type, PhysicalIndex, EnableIRQ)     stm32l1_interrupt_enable_timer(Type, PhysicalIndex, EnableIRQ)

#define _driver_ttc_interrupt_enable_i2c(Type, PhysicalIndex, EnableIRQ)       stm32l1_interrupt_enable_i2c(Type, PhysicalIndex, EnableIRQ)

// architecture specific constant values required by ttc_interrupt

#define _driver_TTC_AMOUNT_EXTERNAL_INTERRUPTS 16
//} Compatibility

//} Defines
//{ Includes *************************************************************

#include "../cm3/cm3_basic.h"
#include "stm32l1_interrupt_types.h"
#include "../ttc_interrupt_types.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"
#include "../ttc_usart_types.h"
#include "../ttc_gpio_types.h"
#include "../stm32l1/stm32l1_gpio.h"
#include <stm32l1xx_rcc.h>
#include <stm32l1xx_gpio.h>
#include <stm32l1xx_syscfg.h>
#include <stm32l1xx_exti.h>
#include <misc.h>

//} Includes



//{ Function prototypes **************************************************

/* initializes interrupt of given type to call given ISR
 * Note: This will not activate your interrupt. Call stm32l1_interrupt_usart_enable() afterwards to enable/ disable this interrupt.
 *
 * @param Type          interrupt Type to use
 * @param PhysicalIndex real physical index of functional unit (e.g. 0 = USART1, 1 = USART2, ...)
 * @param ISR           function pointer of interrupt service routine to call;
 *                      ISR() gets two params: physical_index_t PhysicalIndex, void* Reference
 *                      Index     index functional unit in microcontroller (e.g. 0=first USART)
 *                      Reference hardware dependent data used by low level driver (e.g. register base address)
 * @param Argument      will be passed as argument to ISR()
 * @param OldISR        != NULL: previous stored pointer will be saved into given address
 * @param OldArgument   != NULL: previous stored argument will be saved into given address
 */
ttc_interrupt_errorcode_e stm32l1_interrupt_init(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);

// initializers for individual functional units (called from stm32l1_interrupt_init() )
ttc_interrupt_errorcode_e stm32l1_interrupt_init_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);
ttc_interrupt_errorcode_e stm32l1_interrupt_init_gpio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);

/* activates/ deactivates a previously initialized interrupt
 * @param Type        interrupt Type to use
 * @param PhysicalIndex real physical index of functional unit (e.g. 0 = USART1, 1 = USART2, ...)
 * @param EnableIRQ     =0: disable interrupt; !=0: enable interrupt
 */
void stm32l1_interrupt_enable(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);

// enablers for individual functional units (called from stm32l1_interrupt_enable() )
void stm32l1_interrupt_enable_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);
void stm32l1_interrupt_enable_gpio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);


ttc_interrupt_errorcode_e stm32l1_interrupt_init_spi(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);
ttc_interrupt_errorcode_e stm32l1_interrupt_init_radio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);
ttc_interrupt_errorcode_e stm32l1_interrupt_init_timer(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);
ttc_interrupt_errorcode_e stm32l1_interrupt_init_i2c(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex);

void stm32l1_interrupt_enable_spi(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);
void stm32l1_interrupt_enable_radio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);
void stm32l1_interrupt_enable_timer(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);
void stm32l1_interrupt_enable_i2c(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ);


//----------------Interrupt Handlers (ISRs)---------------------//

/* initializes interrupt of given type to call given ISR
 * @param PhysicalIndex  real index of physical functional unit (e.g 0=USART1, ...)
 * @param ISR_USART      pointer to register-base of USART that triggered the interrupt
 */
void USART_General_IRQHandler(physical_index_t PhysicalIndex, volatile stm32l1_register_usart_t* ISR_USART);
void GPIO_General_IRQHandler(u8_t InterruptLine);



//Defined by CM3 NVIC
void NMI_Handler();
void HardFault_Handler();
void MemManage_Handler();
void BusFault_Handler();
void UsageFault_Handler();
//void 0 //Reserved
//void 0 //Reserved
//void 0//Reserved
//void 0//Reserved
//void SVC_Handler();
void DebugMon_Handler();
//void 0//Reserved
//void PendSV_Handler();
//void SysTick_Handler();

//Defined by STM32L1xx
void WWDG_IRQHandler();
void PVD_IRQHandler();
void TAMPER_STAMP_IRQHandler();
void RTC_WKUP_IRQHandler();
void FLASH_IRQHandler();
void RCC_IRQHandler();
void EXTI0_IRQHandler();
void EXTI1_IRQHandler();
void EXTI2_IRQHandler();
void EXTI3_IRQHandler();
void EXTI4_IRQHandler();
void DMA1_Channel1_IRQHandler();
void DMA1_Channel2_IRQHandler();
void DMA1_Channel3_IRQHandler();
void DMA1_Channel4_IRQHandler();
void DMA1_Channel5_IRQHandler();
void DMA1_Channel6_IRQHandler();
void DMA1_Channel7_IRQHandler();
void ADC1_IRQHandler();
void USB_HP_IRQHandler();
void USB_LP_IRQHandler();
void DAC_IRQHandler();
void COMP_IRQHandler();
void EXTI9_5_IRQHandler();
void LCD_IRQHandler();
void TIM9_IRQHandler();
void TIM10_IRQHandler();
void TIM11_IRQHandler();
void TIM2_IRQHandler();
void TIM3_IRQHandler();
void TIM4_IRQHandler();
void I2C1_EV_IRQHandler();
void I2C1_ER_IRQHandler();
void I2C2_EV_IRQHandler();
void I2C2_ER_IRQHandler();
void SPI1_IRQHandler();
void SPI2_IRQHandler();
void USART1_IRQHandler();
void USART2_IRQHandler();
void USART3_IRQHandler();
void EXTI15_10_IRQHandler();
void RTC_Alarm_IRQHandler();
void USB_FS_WKUP_IRQHandler();
void TIM6_IRQHandler();
void TIM7_IRQHandler();
//void 0//Reserved - No SDIO
void TIM5_IRQHandler();
void SPI3_IRQHandler();
//void 0//Reserved - No USART 4
//void 0//Reserved - No USART 5
void DMA2_Channel1_IRQHandler();
void DMA2_Channel2_IRQHandler();
void DMA2_Channel3_IRQHandler();
void DMA2_Channel4_IRQHandler();
void DMA2_Channel5_IRQHandler();
void AES_IRQHandler();
void COMP_ACQ_IRQHandler();
//------------END Interrupt Handlers (ISRs)---------------------//



//} Function prototypes
#endif //STM32L1_INTERRUPT_H
