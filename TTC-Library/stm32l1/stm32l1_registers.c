/*{ stm32l1_registers.c ************************************************

 Structured pointers to registers of ARM CortexM3 microcontrollers.

 written by Greg Knoll 2013

}*/

#include "stm32l1_registers.h"

//{ Global pointers to hardware registers





//GPIO
t_register_stm32l1xx_gpio*         register_stm32l1xx_GPIOA  = (t_register_stm32l1xx_gpio*) GPIOA;
t_register_stm32l1xx_gpio*         register_stm32l1xx_GPIOB  = (t_register_stm32l1xx_gpio*) GPIOB;
t_register_stm32l1xx_gpio*         register_stm32l1xx_GPIOC  = (t_register_stm32l1xx_gpio*) GPIOC;
t_register_stm32l1xx_gpio*         register_stm32l1xx_GPIOD  = (t_register_stm32l1xx_gpio*) GPIOD;
t_register_stm32l1xx_gpio*         register_stm32l1xx_GPIOE  = (t_register_stm32l1xx_gpio*) GPIOE;
t_register_stm32l1xx_gpio*         register_stm32l1xx_GPIOH  = (t_register_stm32l1xx_gpio*) GPIOH;

//USART
t_stm32l1_register_usart*        register_stm32l1_USART1 = (t_stm32l1_register_usart*) USART1;
t_stm32l1_register_usart*        register_stm32l1_USART2 = (t_stm32l1_register_usart*) USART2;
t_stm32l1_register_usart*        register_stm32l1_USART3 = (t_stm32l1_register_usart*) USART3;

//EXTI
t_stm32l1_register_exti*         register_stm32l1_EXTI   = (t_stm32l1_register_exti* ) EXTI;


//TIMERS

stm32l1_t_registerim2_to_tim4_output_compare_mode_t * register_stm32l1_TIM2_OC = (stm32l1_t_registerim2_to_tim4_output_compare_mode_t *) TIM2;
stm32l1_t_registerim2_to_tim4_input_capture_mode_t *  register_stm32l1_TIM2_IC = (stm32l1_t_registerim2_to_tim4_input_capture_mode_t *) TIM2;
//t_stm32l1_tim2_or               register_stm32l1_TIM2_OR = (t_stm32l1_tim2_or *) (TIM2 + 0x50);//not for medium-density (only md+ and high)

stm32l1_t_registerim2_to_tim4_output_compare_mode_t * register_stm32l1_TIM3_OC = (stm32l1_t_registerim2_to_tim4_output_compare_mode_t *) TIM3;
stm32l1_t_registerim2_to_tim4_input_capture_mode_t *  register_stm32l1_TIM3_IC = (stm32l1_t_registerim2_to_tim4_input_capture_mode_t *) TIM3;
//t_stm32l1_tim3_or               register_stm32l1_TIM3_OR = (t_stm32l1_tim3_or *) (TIM3 + 0x50); //not for medium-density

stm32l1_t_registerim2_to_tim4_output_compare_mode_t * register_stm32l1_TIM4_OC = (stm32l1_t_registerim2_to_tim4_output_compare_mode_t *) TIM4;
stm32l1_t_registerim2_to_tim4_input_capture_mode_t *  register_stm32l1_TIM4_IC = (stm32l1_t_registerim2_to_tim4_input_capture_mode_t *) TIM4;


//I guess no TIM5 except in medium-density-plus models
//stm32l1_t_registerim5_output_compare_mode_t register_stm32l1_TIM5_OC = (stm32l1_t_registerim5_output_compare_mode_t)
//stm32l1_t_registerim5_input_capture_mode_t


//DAC
t_stm32l1_register_dac *            register_stm32l1_DAC = (t_stm32l1_register_dac *) DAC;


//}
