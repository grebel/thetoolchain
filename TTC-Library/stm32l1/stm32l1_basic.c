
/*{ stm32l1_basic.h ***********************************************
 *
 * TheToolChain.com
 *
 * Written by Gregor Rebel, Sascha Poggemann 2010-2018
 *
 * Basic datatypes, enums and functions additional to cm3_basic.h
 * function for the uC to enter sleep mode etc
 *
}*/

#include "stm32l1_basic.h"

void stm32l1_hardware_init() {
    t_u32 StartUpCounter=0, HSIStatus=0;
    SystemInit(); // init STM32 standard peripherals library
    /* Enable HSI */
    RCC->CR |= ((t_u32)CR_HSION);

    /* Wait till HSI is ready and if Time out is reached exit */
    do
    {
      HSIStatus = RCC->CR & CR_HSIRDY;
      StartUpCounter++;
    } while((HSIStatus == 0) && (StartUpCounter != HSI_STARTUP_TIMEOUT));

    if ((RCC->CR & CR_HSIRDY) != RESET)
    {
      HSIStatus = (t_u32)0x01;
    }
    else
    {
      HSIStatus = (t_u32)0x00;
    }
    /* Select HSI as system clock source */
    RCC->CFGR &= (t_u32)((t_u32)~(CFGR_SW));
    RCC->CFGR |= (t_u32)CFGR_SW_HSI;

    /* Wait till HSI is used as system clock source */
    while ((RCC->CFGR & (t_u32)CFGR_SWS) != (t_u32)CFGR_SWS_HSI)
    {
    }
}

