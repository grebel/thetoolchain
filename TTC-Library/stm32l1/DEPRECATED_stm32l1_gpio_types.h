#ifndef STM32L1_GPIO_TYPES_H
#define STM32L1_GPIO_TYPES_H

/** { DEPRECATED_stm32l1_gpio.h ************************************************
 
                           The ToolChain
                      
   High-Level interface for GPIO device.

   Structures, Enums and Defines being required by ttc_gpio_types.h

   Note: See ttc_gpio.h for description of stm32l1 independent GPIO implementation.
  
   Authors: Greg Knoll 2013

 
}*/
//{ Defines/ TypeDefs ******************************************************


//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic.h"

//} Includes
//{ Structures/ Enums required by ttc_gpio_types.h ***********************

typedef enum { //pointers to GPIO Bank addresses
    stm32l1_GPIO_None,
    stm32l1_GPIOA = (u32_t) GPIOA,
    stm32l1_GPIOB = (u32_t) GPIOB,
    stm32l1_GPIOC = (u32_t) GPIOC,
    stm32l1_GPIOD = (u32_t) GPIOD, //Not used in stm32l152RC
    stm32l1_GPIOE = (u32_t) GPIOE, //Not used in stm32l152RC
    stm32l1_GPIOH = (u32_t) GPIOH, //Not used in stm32l152RC
    stm32l1_GPIO_NotImplemented
} stm32l1_GPIO_e;

typedef struct { // stm32l1_Port_t - (These are VARIABLES=GPIO bank + pin)
    stm32l1_GPIO_e GPIOx;
    u8_t Pin;
    volatile u32_t* BSRR;  // bit-band address of BSRR register of this port bit
    volatile u32_t* BRR;   // bit-band address of BRR register of this port bit
    volatile u32_t* IDR;   // bit-band address of IDR register of this port bit
} __attribute__((__packed__)) stm32l1_Port_t;
//} Structures/ Enums


#endif //STM32L1_GPIO_TYPES_H
