    /** { stm32l1_gpio.c ************************************************

                               The ToolChain

       High-Level interface for GPIO device.

       Note: See ttc_gpio.h for description of stm32l1 independent GPIO implementation.

       Authors:
    }*/

    #include "stm32l1_gpio.h"


    //{ Function definitions ***************************************************


    void stm32l1_gpio_init(stm32l1_GPIO_e GPIOx, u8_t Pin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed){
        //Have to give power to the bank by enabling its clock
        _stm32l1_gpio_clock_enable(GPIOx);

    #ifdef USE_STDPERIPH_DRIVER

        //Define standard library GPIO init structure
        GPIO_InitTypeDef  GPIO_InitStructure;

        //Clear the structure
        GPIO_StructInit(& GPIO_InitStructure );

        //Define the pin
        GPIO_InitStructure.GPIO_Pin = 1 << Pin; //_stm32l_stndLib_gpio_pin(Pin);

        //Define the pin mode and accompanying PU/PD resistor and speed (if applicable)
        switch(Type)
        {// find best matching type for stm32-architecture
        case tgm_analog_in:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
            break;
        case tgm_input_floating:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
            break;
        case tgm_input_pull_down:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
            break;
        case tgm_input_pull_up:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
            break;

        case tgm_output_open_drain:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
            GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
            GPIO_InitStructure.GPIO_Speed =  _stm32l1_map_Speed(Speed);
            break;

        case tgm_output_open_drain_pull_up_resistor:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
            GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
            GPIO_InitStructure.GPIO_Speed =  _stm32l1_map_Speed(Speed);
            break;
        case tgm_output_open_drain_pull_down_resistor:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
            GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
            GPIO_InitStructure.GPIO_Speed =  _stm32l1_map_Speed(Speed);
            break;

        case tgm_output_push_pull:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
            GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
            GPIO_InitStructure.GPIO_Speed =  _stm32l1_map_Speed(Speed);
            break;
        case tgm_output_push_pull_pull_up_resistor:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
            GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
            GPIO_InitStructure.GPIO_Speed =  _stm32l1_map_Speed(Speed);
            break;
        case tgm_output_push_pull_pull_down_resistor:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
            GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
            GPIO_InitStructure.GPIO_Speed =  _stm32l1_map_Speed(Speed);
            break;
        case tgm_alternate_function_push_pull:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
            GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
            GPIO_InitStructure.GPIO_Speed =  _stm32l1_map_Speed(Speed);
            break;
        case tgm_alternate_function_push_pull_pull_up_resistor:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
            GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
            GPIO_InitStructure.GPIO_Speed =  _stm32l1_map_Speed(Speed);
            break;
        case tgm_alternate_function_push_pull_pull_down_resistor:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
            GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
            GPIO_InitStructure.GPIO_Speed =  _stm32l1_map_Speed(Speed);
            break;

        case tgm_alternate_function_open_drain:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
            GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
            GPIO_InitStructure.GPIO_Speed =  _stm32l1_map_Speed(Speed);
            break;

        case tgm_alternate_function_open_drain_pull_up_resistor:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
            GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
            GPIO_InitStructure.GPIO_Speed =  _stm32l1_map_Speed(Speed);
            break;

        case tgm_alternate_function_open_drain_pull_down_resistor:
            GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
            GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
            GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
            GPIO_InitStructure.GPIO_Speed =  _stm32l1_map_Speed(Speed);
            break;

        default: Assert(0, ec_InvalidArgument);

        }

        //Initialize appropriate bank with the filled-out InitStructure
        GPIO_Init ( (GPIO_TypeDef *)GPIOx , & GPIO_InitStructure );

    #else  //Not using the standard peripheral library (direct register access)

    #endif


    }


    bool stm32l1_gpio_get(stm32l1_GPIO_e GPIOx, u8_t Pin){
    #ifdef USE_STDPERIPH_DRIVER
        return GPIO_ReadInputDataBit( (GPIO_TypeDef *)GPIOx, (1<<Pin) );
    #else  //Not using the standard peripheral library (direct register access)
        return 0;
    #endif
    }



    void stm32l1_gpio_clr(stm32l1_GPIO_e GPIOx, u8_t Pin){
    #ifdef USE_STDPERIPH_DRIVER
        GPIO_WriteBit((GPIO_TypeDef *)GPIOx, (1<<Pin), Bit_RESET);
    #else  //Not using the standard peripheral library (direct register access)

    #endif
    }



    void stm32l1_gpio_set(stm32l1_GPIO_e GPIOx, u8_t Pin){
    #ifdef USE_STDPERIPH_DRIVER
       // GPIO_WriteBit((GPIO_TypeDef *)GPIOx, (1<<Pin), Bit_SET);
        GPIO_SetBits((GPIO_TypeDef *)GPIOx,1<<Pin);
    #else  //Not using the standard peripheral library (direct register access)

    #endif
    }

    //-------------------------------------------------//



    //-----------with Variables/Ports-----------------//

    void stm32l1_gpio_variable(stm32l1_Port_t* Port, stm32l1_GPIO_e GPIOx, u8_t Pin){
        Port->GPIOx= GPIOx;
        Port->Pin  = Pin;
    }

    void stm32l1_gpio_init_variable(ttc_Port_t* Port, ttc_gpio_bank_t Bank, u8_t Pin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed){
        stm32l1_gpio_variable(Port, Bank, Pin);
    #ifdef USE_STDPERIPH_DRIVER
        stm32l1_gpio_init(Port->GPIOx, Port->Pin, Type, Speed);
    #else  //Not using the standard peripheral library (direct register access)

    #endif
    }

    bool stm32l1_gpio_get_variable(stm32l1_Port_t* Port){
    #ifdef USE_STDPERIPH_DRIVER
        if (1)
            return *(Port->IDR); // read from bitband address
        else {
            u32_t Data = ((GPIO_TypeDef *) Port->GPIOx)->IDR;
            if ( Data & (1 << Port->Pin) ) return TRUE;
            return FALSE;
        }
    #else  //Not using the standard peripheral library (direct register access)

    #endif
    }

    void stm32l1_gpio_set_variable(stm32l1_Port_t* Port){
    #ifdef USE_STDPERIPH_DRIVER
        return stm32l1_gpio_set(Port->GPIOx, Port->Pin);
    #else  //Not using the standard peripheral library (direct register access)

    #endif
    }

    void stm32l1_gpio_clr_variable(stm32l1_Port_t* Port){
    #ifdef USE_STDPERIPH_DRIVER
         return stm32l1_gpio_clr(Port->GPIOx, Port->Pin);
    #else  //Not using the standard peripheral library (direct register access)

    #endif
    }

    u8_t stm32l1_gpio_create_index8(ttc_gpio_bank_t GPIOx, u8_t Pin){
        switch (GPIOx) {

    #ifdef GPIOA
        case stm32l1_GPIOA: return 0x10 | (0x0f & Pin);
    #endif
    #ifdef GPIOB
        case stm32l1_GPIOB: return 0x20 | (0x0f & Pin);
    #endif
    #ifdef GPIOC
        case stm32l1_GPIOC: return 0x30 | (0x0f & Pin);
    #endif
    #ifdef GPIOD
        case stm32l1_GPIOD: return 0x40 | (0x0f & Pin);
    #endif
    #ifdef GPIOE
        case stm32l1_GPIOE: return 0x50 | (0x0f & Pin);
    #endif
    #ifdef GPIOH
        case stm32l1_GPIOH: return 0x60 | (0x0f & Pin);
    #endif


        default: break;
        }

        return 0;
    }

    void stm32l1_gpio_from_index8(u8_t PhysicalIndex, GPIO_TypeDef** GPIOx, u8_t* Pin){
        switch (PhysicalIndex & 0xf0) {

    #ifdef GPIOA
        case 0x10: *GPIOx = GPIOA; break;
    #endif
    #ifdef GPIOB
        case 0x20: *GPIOx = GPIOB; break;
    #endif
    #ifdef GPIOC
        case 0x30: *GPIOx = GPIOC; break;
    #endif
    #ifdef GPIOD
        case 0x40: *GPIOx = GPIOD; break;
    #endif
    #ifdef GPIOE
        case 0x50: *GPIOx = GPIOE; break;
    #endif
    #ifdef GPIOF
        case 0x60: *GPIOx = GPIOF; break;
    #endif
    #ifdef GPIOG
        case 0x70: *GPIOx = GPIOG; break;
    #endif

        default: *GPIOx = NULL;
        }

        *Pin = PhysicalIndex & 0x0f;

    }

    //} Function definitions








    //{ private functions  ********************************************


    void _stm32l1_gpio_clock_enable(stm32l1_GPIO_e GPIOx){

    #ifdef USE_STDPERIPH_DRIVER
        switch(GPIOx){
            case stm32l1_GPIOA:
               RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
               break;
            case stm32l1_GPIOB:
               RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
               break;
            case stm32l1_GPIOC:
               RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
               break;
            default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!
        }
    #else //Not using the standard peripheral library (direct register access)

    #endif
    }

    GPIOSpeed_TypeDef _stm32l1_map_Speed(ttc_gpio_speed_e Speed) {
        Assert(Speed < tgs_Unknown, ec_InvalidArgument);

        switch (Speed) { // find best matching speed for stm32-architecture
          case tgs_Min:
          case tgs_400KHz:  return GPIO_Speed_400KHz;
          case tgs_2MHz:    return GPIO_Speed_2MHz;
          case tgs_10MHz:   return GPIO_Speed_10MHz;
          case tgs_40MHz:
          case tgs_50MHz:
          case tgs_Max:     return GPIO_Speed_40MHz;
        default:
          return GPIO_Speed_40MHz;
        }

        return 0;
    }


    //} private functions
