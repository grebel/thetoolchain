/** { stm32l1_interrupt.c ************************************************
 
                           The ToolChain
                      
   High-Level interface for INTERRUPT device.
     
   Note: See ttc_interrupt.h for description of stm32l1 independent INTERRUPT implementation.
  
   Authors: 
}*/

#include "stm32l1_interrupt.h"

// provided by ttc_interrupt.c
ttc_array_dynamic_extern(ttc_usart_isrs_t*, ttc_usart_isrs);
ttc_array_dynamic_extern(ttc_gpio_isrs_t*,  ttc_gpio_isrs);



//{ Function definitions ***************************************************

ttc_interrupt_errorcode_e stm32l1_interrupt_init(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex){
    return tine_OK;
}


ttc_interrupt_errorcode_e stm32l1_interrupt_init_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex)
{

    // all done in ttc_interrupt.c
    return tine_OK;
}
#ifdef EXTENSION_500_ttc_gpio
ttc_interrupt_errorcode_e stm32l1_interrupt_init_gpio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex)
{



    //*****Get handle to the GPIO bank and Pin*****//
    GPIO_TypeDef* GPIOx;
    u8_t Pin;
    stm32l1_gpio_from_index8(PhysicalIndex, &GPIOx, &Pin);
    Assert_Interrupt(GPIOx,    ec_InvalidArgument);
    Assert_Interrupt(Pin < TTC_GPIO_MAX_PINS, ec_InvalidArgument);






    //*****SYSCONFIG*****//
    //Power Sysconfig by enabling its clock
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    //Select the input source port and pin for the EXTI line using SYSCFG_EXTILineConfig()
    uint8_t EXTI_PortSourceX;
    switch ( (u32_t) GPIOx ) {
#ifdef GPIOA
    case (u32_t) GPIOA:
        EXTI_PortSourceX = EXTI_PortSourceGPIOA;
        break;
#endif
#ifdef GPIOB
    case (u32_t) GPIOB:
        EXTI_PortSourceX = EXTI_PortSourceGPIOB;
        break;
#endif
#ifdef GPIOC
    case (u32_t) GPIOC:
        EXTI_PortSourceX = EXTI_PortSourceGPIOC;
        break;
#endif
#ifdef GPIOD
    case (u32_t) GPIOD:
        EXTI_PortSourceX = EXTI_PortSourceGPIOD;
        break;
#endif
#ifdef GPIOE
    case (u32_t) GPIOE:
        EXTI_PortSourceX = EXTI_PortSourceGPIOE;
        break;
#endif
#ifdef GPIOH
    case (u32_t) GPIOH:
        EXTI_PortSourceX = EXTI_PortSourceGPIOH;
        break;
#endif
    default:
        Assert_Interrupt(0,ec_InvalidArgument);
    }

    uint8_t EXTI_PinSourceX;
    switch (Pin){
    case 0:
        EXTI_PinSourceX = EXTI_PinSource0;
        break;
    case 1:
        EXTI_PinSourceX = EXTI_PinSource1;
        break;
    case 2:
        EXTI_PinSourceX = EXTI_PinSource2;
        break;
    case 3:
        EXTI_PinSourceX = EXTI_PinSource3;
        break;
    case 4:
        EXTI_PinSourceX = EXTI_PinSource4;
        break;
    case 5:
        EXTI_PinSourceX = EXTI_PinSource5;
        break;
    case 6:
        EXTI_PinSourceX = EXTI_PinSource6;
        break;
    case 7:
        EXTI_PinSourceX = EXTI_PinSource7;
        break;
    case 8:
        EXTI_PinSourceX = EXTI_PinSource8;
        break;
    case 9:
        EXTI_PinSourceX = EXTI_PinSource9;
        break;
    case 10:
        EXTI_PinSourceX = EXTI_PinSource10;
        break;
    case 11:
        EXTI_PinSourceX = EXTI_PinSource11;
        break;
    case 12:
        EXTI_PinSourceX = EXTI_PinSource12;
        break;
    case 13:
        EXTI_PinSourceX = EXTI_PinSource13;
        break;
    case 14:
        EXTI_PinSourceX = EXTI_PinSource14;
        break;
    case 15:
        EXTI_PinSourceX = EXTI_PinSource15;
        break;
    default:
        Assert_Interrupt(0,ec_InvalidArgument);
    }

    SYSCFG_EXTILineConfig(EXTI_PortSourceX, EXTI_PinSourceX);






    //*****EXTI*****//
    //Select the mode(interrupt, event) and configure the trigger selection (Rising, falling or both) using EXTI_Init()
    EXTI_InitTypeDef EXTI_InitStructure;
    EXTI_StructInit(&EXTI_InitStructure); //Initialize stucture

    switch (Pin){
    case 0:
        EXTI_InitStructure.EXTI_Line = EXTI_Line0;
        break;
    case 1:
        EXTI_InitStructure.EXTI_Line = EXTI_Line1;
        break;
    case 2:
        EXTI_InitStructure.EXTI_Line = EXTI_Line2;
        break;
    case 3:
        EXTI_InitStructure.EXTI_Line = EXTI_Line3;
        break;
    case 4:
        EXTI_InitStructure.EXTI_Line = EXTI_Line4;
        break;
    case 5:
        EXTI_InitStructure.EXTI_Line = EXTI_Line5;
        break;
    case 6:
        EXTI_InitStructure.EXTI_Line = EXTI_Line6;
        break;
    case 7:
        EXTI_InitStructure.EXTI_Line = EXTI_Line7;
        break;
    case 8:
        EXTI_InitStructure.EXTI_Line = EXTI_Line8;
        break;
    case 9:
        EXTI_InitStructure.EXTI_Line = EXTI_Line9;
        break;
    case 10:
        EXTI_InitStructure.EXTI_Line = EXTI_Line10;
        break;
    case 11:
        EXTI_InitStructure.EXTI_Line = EXTI_Line11;
        break;
    case 12:
        EXTI_InitStructure.EXTI_Line = EXTI_Line12;
        break;
    case 13:
        EXTI_InitStructure.EXTI_Line = EXTI_Line13;
        break;
    case 14:
        EXTI_InitStructure.EXTI_Line = EXTI_Line14;
        break;
    case 15:
        EXTI_InitStructure.EXTI_Line = EXTI_Line15;
        break;
    default:
        Assert_Interrupt(0,ec_InvalidArgument);
    }

    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;

    switch (Type) { // Set edge-trigger preference

    case tit_GPIO_Falling:
        EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
        break;

    case tit_GPIO_Rising:
        EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
        break;

    case tit_GPIO_Rising_Falling:
        EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
        break;

    default:
        Assert_Interrupt(0,ec_InvalidArgument);
        break;
    }

    EXTI_InitStructure.EXTI_LineCmd=DISABLE; //Enabled in stm32l1_interrupt_enable_gpio

    //Initialize the EXI using the structure
    EXTI_Init(&EXTI_InitStructure);






    //*****NVIC*****//
    //Configure NVIC IRQ channel mapped to the EXTI line using NVIC_Init()
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
    NVIC_InitTypeDef NVIC_InitStructure;

    //Get IRQ Channel
    static const u8_t stm32l1_Interrupt_IRQChannel[_driver_TTC_AMOUNT_EXTERNAL_INTERRUPTS] = {
        EXTI0_IRQn,
        EXTI1_IRQn,
        EXTI2_IRQn,
        EXTI3_IRQn,
        EXTI4_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn
    };
    NVIC_InitStructure.NVIC_IRQChannel = stm32l1_Interrupt_IRQChannel[Pin];

    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 3;
    NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE;
    NVIC_Init (& NVIC_InitStructure );


    return tine_OK;
}
#endif
 //END stm32l1_interrupt_init_gpio

void stm32l1_interrupt_enable(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ)
{

}

void stm32l1_interrupt_enable_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ)
{

    //-----Define Standard Peripheral NVIC init structure------//
    NVIC_InitTypeDef NVIC_Cfg;

    /* Enable and set USARTn Interrupt to the lowest priority */
    NVIC_Cfg.NVIC_IRQChannelPreemptionPriority= 0x0F;
    NVIC_Cfg.NVIC_IRQChannelSubPriority= 0x0F;
    NVIC_Cfg.NVIC_IRQChannelCmd=ENABLE;



    //Set physical device in NVIC and in USART_Base
    stm32l1_register_usart_t* USART_Base = NULL;

    switch (PhysicalIndex) { // physical index starts at 0

    case 0: USART_Base = (stm32l1_register_usart_t*)USART1; NVIC_Cfg.NVIC_IRQChannel = USART1_IRQn; break;
    case 1: USART_Base = (stm32l1_register_usart_t*)USART2; NVIC_Cfg.NVIC_IRQChannel = USART2_IRQn; break;
    case 2: USART_Base = (stm32l1_register_usart_t*)USART3; NVIC_Cfg.NVIC_IRQChannel = USART3_IRQn; break;

    default: { Assert_Interrupt(0, ec_InvalidConfiguration); }
    }

    //-----END Define Standard Peripheral NVIC init structure------//

    //-----Set USART registers via stm32l1 pointers (in register.c/h files)------//
    //Use USART_Base to access control registers
     stm32l1_USART_CR1_t CR1 = USART_Base->CR1;
     stm32l1_USART_CR2_t CR2 = USART_Base->CR2;
     stm32l1_USART_CR3_t CR3 = USART_Base->CR3;

     //Use control registers to enable mode
     switch (Type) { // set/ clear corresponding bits

     case tit_USART_Cts:
         if (EnableIRQ)
             Assert_Interrupt(A(ttc_usart_isrs, PhysicalIndex)->isr_ClearToSend != NULL, ec_InvalidConfiguration); // no isr configured for this interrupt!
         CR3.CTSIE  = EnableIRQ;
         break;
     case tit_USART_Idle:
         CR1.IDLEIE = EnableIRQ;
         if (EnableIRQ)
             Assert_Interrupt(A(ttc_usart_isrs, PhysicalIndex)->isr_IdleLine != NULL, ec_InvalidConfiguration); // no isr configured for this interrupt!
         break;
     case tit_USART_TxComplete:
         CR1.TCIE   = EnableIRQ;
         if (EnableIRQ)
             Assert_Interrupt(A(ttc_usart_isrs, PhysicalIndex)->isr_TransmissionComplete!= NULL, ec_InvalidConfiguration); // no isr configured for this interrupt!
         break;
     case tit_USART_TransmitDataEmpty:
         CR1.TXEIE  = EnableIRQ;
         if (EnableIRQ)
             Assert_Interrupt(A(ttc_usart_isrs, PhysicalIndex)->isr_TransmitDataEmpty!= NULL, ec_InvalidConfiguration); // no isr configured for this interrupt!
         break;
     case tit_USART_RxNE:
         CR1.RXNEIE = EnableIRQ;
         if (EnableIRQ)
             Assert_Interrupt(A(ttc_usart_isrs, PhysicalIndex)->isr_ReceiveDataNotEmpty!= NULL, ec_InvalidConfiguration); // no isr configured for this interrupt!
         break;
     case tit_USART_LinBreak:
         CR2.LBDIE  = EnableIRQ;
         if (EnableIRQ)
             Assert_Interrupt(A(ttc_usart_isrs, PhysicalIndex)->isr_LinBreakDetected!= NULL, ec_InvalidConfiguration); // no isr configured for this interrupt!
         break;
     case tit_USART_Error:
         if (EnableIRQ)
             Assert_Interrupt(A(ttc_usart_isrs, PhysicalIndex)->isr_Error!= NULL, ec_InvalidConfiguration); // no isr configured for this interrupt!
         CR1.PEIE   = EnableIRQ;  // irq on parity error
         CR3.EIE    = EnableIRQ;  // irq on general error
         break;
     default: break;
     }
     //---END Set USART registers via stm32l1 pointers (in register.c/h files)------//

     //To Disable (passed via EnableIRQ)
     if (! EnableIRQ) { // disable interrupt channel only if no other interrupt event for this USART is active
         if ( (CR1.PEIE   == 0) &&
              (CR1.IDLEIE == 0) &&
              (CR1.TCIE   == 0) &&
              (CR1.TXEIE  == 0) &&
              (CR1.RXNEIE == 0) &&
              (CR2.LBDIE  == 0) &&
              (CR3.CTSIE  == 0) &&
              (CR3.EIE    == 0)
              )
             NVIC_Cfg.NVIC_IRQChannelCmd = DISABLE;
     }//END DISABLE

     //Finally, call NVIC_Init()
     NVIC_Init(&NVIC_Cfg);


}

#ifdef EXTENSION_500_ttc_gpio
void stm32l1_interrupt_enable_gpio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ)
{
    //Get handle to the GPIO bank and Pin
    GPIO_TypeDef* GPIOx;
    u8_t Pin;
    stm32l1_gpio_from_index8(PhysicalIndex, &GPIOx, &Pin);

    //Check all arguments
#if (TTC_ASSERT_INTERRUPTS==1)
    Assert_Interrupt(GPIOx,    ec_InvalidArgument);
    Assert_Interrupt(Pin < _driver_TTC_AMOUNT_EXTERNAL_INTERRUPTS, ec_InvalidArgument);
    Assert_Interrupt(!ttc_array_is_null(ttc_gpio_isrs), ec_InvalidConfiguration); // init interrupt first!
    ttc_gpio_isrs_t* GPIO_ISR_Entry = A(ttc_gpio_isrs, Pin);
    Assert_Interrupt(GPIO_ISR_Entry, ec_InvalidConfiguration); // init interrupt first!
#endif

    //Static pointer to EXTI_InitType -> Only defined once, so may already be present
    static EXTI_InitTypeDef* EXTI_Cfg = NULL;

    if (EXTI_Cfg == NULL) { // first call: allocate structure
        EXTI_Cfg = ttc_memory_alloc_zeroed(sizeof(EXTI_InitTypeDef));
        EXTI_StructInit(EXTI_Cfg);
        EXTI_Cfg->EXTI_Mode = EXTI_Mode_Interrupt;
    }//End if


    switch (Type) { // select trigger according to Type

    case tit_GPIO_Falling:
        EXTI_Cfg->EXTI_Trigger = EXTI_Trigger_Falling;
        break;

    case tit_GPIO_Rising:
        EXTI_Cfg->EXTI_Trigger = EXTI_Trigger_Rising;
        break;

    case tit_GPIO_Rising_Falling:
        EXTI_Cfg->EXTI_Trigger = EXTI_Trigger_Rising_Falling;
        break;

    default:
        Assert_Interrupt(0,ec_InvalidArgument);
        break;
    }//End switch

    EXTI_Cfg->EXTI_Line = 1 << Pin;
    EXTI_Cfg->EXTI_LineCmd = ENABLE;

    EXTI_Init(EXTI_Cfg);

}//END stm32l1_interrupt_enable_gpio
#endif

ttc_interrupt_errorcode_e stm32l1_interrupt_init_spi(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex){
    return tine_NotImplemented;
}

ttc_interrupt_errorcode_e stm32l1_interrupt_init_radio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex){
    return tine_NotImplemented;
}

ttc_interrupt_errorcode_e stm32l1_interrupt_init_timer(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex){
    return tine_NotImplemented;
}

ttc_interrupt_errorcode_e stm32l1_interrupt_init_i2c(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex){
    return tine_NotImplemented;
}

void stm32l1_interrupt_enable_spi(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ){
    //Not implemented
}

void stm32l1_interrupt_enable_radio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ){
//Not implemented
}

void stm32l1_interrupt_enable_timer(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ){
//Not implemented
}

void stm32l1_interrupt_enable_i2c(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, BOOL EnableIRQ){
//Not implemented
}

//----------------Interrupt Handlers (ISRs)---------------------//

//----------supported/implemented so far----------------//

void HardFault_Handler() {

    __asm__(".global HardFault_Handler");
    __asm__(".extern cm3_hard_fault_handler_c");
    __asm__("TST LR, #4");
    __asm__("ITE EQ");
    __asm__("MRSEQ R0, MSP");
    __asm__("MRSNE R0, PSP");
    __asm__("B cm3_hard_fault_handler_c");
}

void USART_General_IRQHandler(physical_index_t PhysicalIndex, volatile stm32l1_register_usart_t* USART){
    //Get a handle to the USART's status register
    stm32l1_USART_SR_t StatusRegister = USART->SR;  // reading from SR and then from DR will reset flags in SR

    //Get ISR from Array of ISRs (extern defined above)
    ttc_usart_isrs_t* ISRs = A(ttc_usart_isrs, PhysicalIndex);
    Assert_Interrupt(ISRs != NULL, ec_UNKNOWN); // should never happen: the usart indexed by PhysicalIndex has not been initialized!

    //Check what caused the interrupt in status register, then call appropriate ISR in USART ISRs
    if (StatusRegister.RXNE && ISRs->isr_ReceiveDataNotEmpty) {
         ISRs->isr_ReceiveDataNotEmpty(PhysicalIndex, ISRs->Argument_ReceiveDataNotEmpty);
    }
    if (StatusRegister.TXE  && ISRs->isr_TransmitDataEmpty) {
        ISRs->isr_TransmitDataEmpty(PhysicalIndex, ISRs->Argument_TransmitDataEmpty);
    }
    if (StatusRegister.CTS  && ISRs->isr_ClearToSend) {
        ISRs->isr_ClearToSend(PhysicalIndex, ISRs->Argument_ClearToSend);
    }
    if (StatusRegister.TC   && ISRs->isr_TransmissionComplete) {
        ISRs->isr_TransmissionComplete(PhysicalIndex, ISRs->Argument_TransmissionComplete);
    }
    if (StatusRegister.IDLE && ISRs->isr_IdleLine) {
        ISRs->isr_IdleLine(PhysicalIndex, ISRs->Argument_IdleLine);
    }
    if (StatusRegister.LBD  && ISRs->isr_LinBreakDetected) {
        ISRs->isr_LinBreakDetected(PhysicalIndex, ISRs->Argument_LinBreakDetected);
    }

    //Check for error
    if (  ( (*( (u8_t*) &StatusRegister )) & 0xf) &&  // error occured: gather data + call error handler
          (ISRs->isr_Error != NULL)                   // error-handler has been defined
          ) {
        ttc_usart_errorcode_e Error; *((u8_t*) &Error) = 0;
        if (StatusRegister.FE)   Error.Framing = 1;
        if (StatusRegister.NF)   Error.Noise   = 1;
        if (StatusRegister.ORE)  Error.Overrun = 1;
        if (StatusRegister.PE)   Error.Parity  = 1;

        ISRs->isr_Error(PhysicalIndex, Error, ISRs->Argument_Error);
    }
}


void USART1_IRQHandler(){
    USART_General_IRQHandler(0, (stm32l1_register_usart_t*) USART1);
    USART1->SR = 0; // clear all interrupt flags
}
void USART2_IRQHandler(){
    USART_General_IRQHandler(1, (stm32l1_register_usart_t*) USART2);
    USART1->SR = 0; // clear all interrupt flags
}
void USART3_IRQHandler(){
    USART_General_IRQHandler(2, (stm32l1_register_usart_t*) USART3);
    USART1->SR = 0; // clear all interrupt flags
}

void GPIO_General_IRQHandler(u8_t InterruptLine){
    ttc_gpio_isrs_t* Entry = A(ttc_gpio_isrs, InterruptLine);
    if (Entry->isr)
        Entry->isr(Entry->PhysicalIndex, Entry->Argument);
}
void EXTI0_IRQHandler() {
    //EXTI_ClearFlag(EXTI_Line0);
    //GPIO_WriteBit(GPIOB , GPIO_Pin_7 , Bit_SET);
    GPIO_General_IRQHandler(0);
    EXTI_ClearITPendingBit(EXTI_Line0);
    return;

    //GPIO_General_IRQHandler(0);
    //EXTI_ClearITPendingBit(EXTI_Line0);
}
void EXTI1_IRQHandler() {
    EXTI_ClearITPendingBit(EXTI_Line1);
    return;
    /*GPIO_General_IRQHandler(1);
    EXTI_ClearITPendingBit(EXTI_Line1);*/
}
void EXTI2_IRQHandler() {
    EXTI_ClearITPendingBit(EXTI_Line2);
    return;
    /*GPIO_General_IRQHandler(1);
    EXTI_ClearITPendingBit(EXTI_Line1);*/
}
void EXTI3_IRQHandler() {
    EXTI_ClearITPendingBit(EXTI_Line3);
    return;
    /*GPIO_General_IRQHandler(1);
    EXTI_ClearITPendingBit(EXTI_Line1);*/
}
void EXTI4_IRQHandler() {
    EXTI_ClearITPendingBit(EXTI_Line4);
    return;
    /*GPIO_General_IRQHandler(1);
    EXTI_ClearITPendingBit(EXTI_Line1);*/
}
void EXTI9_5_IRQHandler() {
    EXTI_ClearITPendingBit(EXTI_Line5);
    return;
    /*GPIO_General_IRQHandler(1);
    EXTI_ClearITPendingBit(EXTI_Line1);
    if(EXTI_GetITStatus(EXTI_Line5)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(5);
        EXTI_ClearITPendingBit(EXTI_Line5);

    } else if(EXTI_GetITStatus(EXTI_Line6)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(6);
        EXTI_ClearITPendingBit(EXTI_Line6);

    } else if(EXTI_GetITStatus(EXTI_Line7)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(7);
        EXTI_ClearITPendingBit(EXTI_Line7);

    } else if(EXTI_GetITStatus(EXTI_Line8)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(8);
        EXTI_ClearITPendingBit(EXTI_Line8);

    } else if(EXTI_GetITStatus(EXTI_Line9)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(9);
        EXTI_ClearITPendingBit(EXTI_Line9);

    } else{
        Assert_Interrupt(0, ec_UNKNOWN);
    }*/
}
void EXTI15_10_IRQHandler() {
    EXTI_ClearITPendingBit(EXTI_Line6);
    return;
    /*GPIO_General_IRQHandler(1);
    EXTI_ClearITPendingBit(EXTI_Line1);
    if(EXTI_GetITStatus(EXTI_Line10)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(10);
        EXTI_ClearITPendingBit(EXTI_Line10);

    } else if(EXTI_GetITStatus(EXTI_Line11)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(11);
        EXTI_ClearITPendingBit(EXTI_Line11);

    } else if(EXTI_GetITStatus(EXTI_Line12)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(12);
        EXTI_ClearITPendingBit(EXTI_Line12);

    } else if(EXTI_GetITStatus(EXTI_Line13)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(13);
        EXTI_ClearITPendingBit(EXTI_Line13);

    } else if(EXTI_GetITStatus(EXTI_Line14)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(14);
        EXTI_ClearITPendingBit(EXTI_Line14);

    } else if(EXTI_GetITStatus(EXTI_Line15)) {
        // call the general GPIO Interrupt handler
        GPIO_General_IRQHandler(15);
        EXTI_ClearITPendingBit(EXTI_Line15);

    } else{
        Assert_Interrupt(0, ec_UNKNOWN);
    }*/
}

//-----------NOT supported/implemented so far----------------//



//Defined by CM3 NVIC
void NMI_Handler(){
	// call your interrupt-handler right here, or else ...

	Assert_Interrupt(0, ec_UNKNOWN);
}
void MemManage_Handler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void BusFault_Handler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void UsageFault_Handler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
/*void SVC_Handler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}*/
void DebugMon_Handler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
/*void PendSV_Handler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}*/
/*void SysTick_Handler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}*/

//Defined by STM32L1xx
void WWDG_IRQHandler(){
    //call your interrupt-handler right here, or else ...

    Assert_Interrupt(0, ec_UNKNOWN);
}
void PVD_IRQHandler(){
    //call your interrupt-handler right here, or else ...

    Assert_Interrupt(0, ec_UNKNOWN);
}
void TAMPER_STAMP_IRQHandler(){
    //call your interrupt-handler right here, or else ...

    Assert_Interrupt(0, ec_UNKNOWN);
}
void RTC_WKUP_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void FLASH_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void RCC_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}


void DMA1_Channel1_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA1_Channel2_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA1_Channel3_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA1_Channel4_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA1_Channel5_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA1_Channel6_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA1_Channel7_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void ADC1_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void USB_HP_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void USB_LP_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void DAC_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void COMP_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}

void LCD_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void TIM9_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void TIM10_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void TIM11_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void TIM2_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void TIM3_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void TIM4_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void I2C1_EV_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void I2C1_ER_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void I2C2_EV_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void I2C2_ER_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void SPI1_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void SPI2_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}

void RTC_Alarm_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void USB_FS_WKUP_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void TIM6_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void TIM7_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void TIM5_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void SPI3_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA2_Channel1_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA2_Channel2_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA2_Channel3_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA2_Channel4_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void DMA2_Channel5_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void AES_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
void COMP_ACQ_IRQHandler(){
	//call your interrupt-handler right here, or else ...
	
	Assert_Interrupt(0, ec_UNKNOWN);
}
//------------END Interrupt Handlers (ISRs)---------------------//

//} Function definitions
//{ private functions (ideally) ********************************************

//} private functions
