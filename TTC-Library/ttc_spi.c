/** { ttc_spi.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for spi devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 23 at 20140423 11:04:14 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "ttc_spi.h"
#include "ttc_task.h"
#include "ttc_heap.h"

#if TTC_SPI_AMOUNT == 0
    #warning No SPI devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of spi devices.
 *
 */


// for each initialized device, a pointer to its generic and stm32f1xx definitions is stored
A_define( t_ttc_spi_config*, ttc_spi_configs, TTC_SPI_AMOUNT );

// Store all Slave Select Pins of all SPI devices in constant arrays (safes RAM).
#ifdef TTC_SPI1 //{
#if TTC_SPI1_NSS_AMOUNT == 0
    #    warning  No Slave Select pins defined for SPI1. Define at least TTC_SPI1_NSS1 from e_ttc_gpio_pin as pin being connected to SlaveSelect input of first slave device!
#endif

const e_ttc_gpio_pin ttc_spi_1_NSS[TTC_SPI1_NSS_AMOUNT + 1] = {
    E_ttc_gpio_pin_none, // investing this extra constant 32 bit makes ttc_spi routines a little bit faster as array index calculation is simpler.
#  ifdef TTC_SPI1_NSS1
    TTC_SPI1_NSS1
#  endif
#  ifdef TTC_SPI1_NSS2
    TTC_SPI1_NSS2,
#  endif
#  ifdef TTC_SPI1_NSS3
    TTC_SPI1_NSS3,
#  endif
#  ifdef TTC_SPI1_NSS4
    TTC_SPI1_NSS4,
#  endif
#  ifdef TTC_SPI1_NSS5
    TTC_SPI1_NSS5,
#  endif
#  ifdef TTC_SPI1_NSS6
    TTC_SPI1_NSS6,
#  endif
#  ifdef TTC_SPI1_NSS7
    TTC_SPI1_NSS7,
#  endif
#  ifdef TTC_SPI1_NSS8
    TTC_SPI1_NSS8,
#  endif
#  ifdef TTC_SPI1_NSS9
    TTC_SPI1_NSS9,
#  endif
#  ifdef TTC_SPI1_NSS10
    TTC_SPI1_NSS10,
#  endif
};
#endif //}
#ifdef TTC_SPI2 //{
#if TTC_SPI2_NSS_AMOUNT == 0
    #    warning  No Slave Select pins defined for SPI2. Define at least TTC_SPI2_NSS1 from e_ttc_gpio_pin as pin being connected to SlaveSelect input of first slave device!
#endif

const e_ttc_gpio_pin ttc_spi_2_NSS[TTC_SPI2_NSS_AMOUNT + 1] = {
    E_ttc_gpio_pin_none, // investing this extra constant 32 bit makes ttc_spi routines a little bit faster as array index calculation is simpler.
#  ifdef TTC_SPI2_NSS1
    TTC_SPI2_NSS1
#  endif
#  ifdef TTC_SPI2_NSS2
    TTC_SPI2_NSS2,
#  endif
#  ifdef TTC_SPI2_NSS3
    TTC_SPI2_NSS3,
#  endif
#  ifdef TTC_SPI2_NSS4
    TTC_SPI2_NSS4,
#  endif
#  ifdef TTC_SPI2_NSS5
    TTC_SPI2_NSS5,
#  endif
#  ifdef TTC_SPI2_NSS6
    TTC_SPI2_NSS6,
#  endif
#  ifdef TTC_SPI2_NSS7
    TTC_SPI2_NSS7,
#  endif
#  ifdef TTC_SPI2_NSS8
    TTC_SPI2_NSS8,
#  endif
#  ifdef TTC_SPI2_NSS9
    TTC_SPI2_NSS9,
#  endif
#  ifdef TTC_SPI2_NSS10
    TTC_SPI2_NSS10,
#  endif
};
#endif //}
#ifdef TTC_SPI3 //{

#if TTC_SPI3_NSS_AMOUNT == 0
    #    warning  No Slave Select pins defined for SPI3. Define at least TTC_SPI3_NSS1 from e_ttc_gpio_pin as pin being connected to SlaveSelect input of first slave device!
#endif

const e_ttc_gpio_pin ttc_spi_3_NSS[TTC_SPI3_NSS_AMOUNT + 1] = {
    E_ttc_gpio_pin_none, // investing this extra constant 32 bit makes ttc_spi routines a little bit faster as array index calculation is simpler.
#  ifdef TTC_SPI3_NSS1
    TTC_SPI3_NSS1
#  endif
#  ifdef TTC_SPI3_NSS2
    TTC_SPI3_NSS2,
#  endif
#  ifdef TTC_SPI3_NSS3
    TTC_SPI3_NSS3,
#  endif
#  ifdef TTC_SPI3_NSS4
    TTC_SPI3_NSS4,
#  endif
#  ifdef TTC_SPI3_NSS5
    TTC_SPI3_NSS5,
#  endif
#  ifdef TTC_SPI3_NSS6
    TTC_SPI3_NSS6,
#  endif
#  ifdef TTC_SPI3_NSS7
    TTC_SPI3_NSS7,
#  endif
#  ifdef TTC_SPI3_NSS8
    TTC_SPI3_NSS8,
#  endif
#  ifdef TTC_SPI3_NSS9
    TTC_SPI3_NSS9,
#  endif
#  ifdef TTC_SPI3_NSS10
    TTC_SPI3_NSS10,
#  endif
};
#endif //}
#ifdef TTC_SPI4 //{

#if TTC_SPI4_NSS_AMOUNT == 0
    #    warning  No Slave Select pins defined for SPI4. Define at least TTC_SPI4_NSS1 from e_ttc_gpio_pin as pin being connected to SlaveSelect input of first slave device!
#endif

const e_ttc_gpio_pin ttc_spi_4_NSS[TTC_SPI4_NSS_AMOUNT + 1] = {
    E_ttc_gpio_pin_none, // investing this extra constant 32 bit makes ttc_spi routines a little bit faster as array index calculation is simpler.
#  ifdef TTC_SPI4_NSS1
    TTC_SPI4_NSS1
#  endif
#  ifdef TTC_SPI4_NSS2
    TTC_SPI4_NSS2,
#  endif
#  ifdef TTC_SPI4_NSS3
    TTC_SPI4_NSS3,
#  endif
#  ifdef TTC_SPI4_NSS4
    TTC_SPI4_NSS4,
#  endif
#  ifdef TTC_SPI4_NSS5
    TTC_SPI4_NSS5,
#  endif
#  ifdef TTC_SPI4_NSS6
    TTC_SPI4_NSS6,
#  endif
#  ifdef TTC_SPI4_NSS7
    TTC_SPI4_NSS7,
#  endif
#  ifdef TTC_SPI4_NSS8
    TTC_SPI4_NSS8,
#  endif
#  ifdef TTC_SPI4_NSS9
    TTC_SPI4_NSS9,
#  endif
#  ifdef TTC_SPI4_NSS10
    TTC_SPI4_NSS10,
#  endif
};
#endif //}
#ifdef TTC_SPI5 //{

#if TTC_SPI5_NSS_AMOUNT == 0
    #      warning  No Slave Select pins defined for SPI5. Define at least TTC_SPI5_NSS1 from e_ttc_gpio_pin as pin being connected to SlaveSelect input of first slave device!
#endif

const e_ttc_gpio_pin ttc_spi_5_NSS[TTC_SPI5_NSS_AMOUNT + 1] = {
    E_ttc_gpio_pin_none, // investing this extra constant 32 bit makes ttc_spi routines a little bit faster as array index calculation is simpler.
#  ifdef TTC_SPI5_NSS1
    TTC_SPI5_NSS1
#  endif
#  ifdef TTC_SPI5_NSS2
    TTC_SPI5_NSS2,
#  endif
#  ifdef TTC_SPI5_NSS3
    TTC_SPI5_NSS3,
#  endif
#  ifdef TTC_SPI5_NSS4
    TTC_SPI5_NSS4,
#  endif
#  ifdef TTC_SPI5_NSS5
    TTC_SPI5_NSS5,
#  endif
#  ifdef TTC_SPI5_NSS6
    TTC_SPI5_NSS6,
#  endif
#  ifdef TTC_SPI5_NSS7
    TTC_SPI5_NSS7,
#  endif
#  ifdef TTC_SPI5_NSS8
    TTC_SPI5_NSS8,
#  endif
#  ifdef TTC_SPI5_NSS9
    TTC_SPI5_NSS9,
#  endif
#  ifdef TTC_SPI5_NSS10
    TTC_SPI5_NSS10,
#  endif
};
#endif //}
#ifdef TTC_SPI6 //{

#if TTC_SPI6_NSS_AMOUNT == 0
    #    warning  No Slave Select pins defined for SPI6. Define at least TTC_SPI6_NSS1 from e_ttc_gpio_pin as pin being connected to SlaveSelect input of first slave device!
#endif

const e_ttc_gpio_pin ttc_spi_6_NSS[TTC_SPI6_NSS_AMOUNT + 1] = {
    E_ttc_gpio_pin_none, // investing this extra constant 32 bit makes ttc_spi routines a little bit faster as array index calculation is simpler.
#  ifdef TTC_SPI6_NSS1
    TTC_SPI6_NSS1
#  endif
#  ifdef TTC_SPI6_NSS2
    TTC_SPI6_NSS2,
#  endif
#  ifdef TTC_SPI6_NSS3
    TTC_SPI6_NSS3,
#  endif
#  ifdef TTC_SPI6_NSS4
    TTC_SPI6_NSS4,
#  endif
#  ifdef TTC_SPI6_NSS5
    TTC_SPI6_NSS5,
#  endif
#  ifdef TTC_SPI6_NSS6
    TTC_SPI6_NSS6,
#  endif
#  ifdef TTC_SPI6_NSS7
    TTC_SPI6_NSS7,
#  endif
#  ifdef TTC_SPI6_NSS8
    TTC_SPI6_NSS8,
#  endif
#  ifdef TTC_SPI6_NSS9
    TTC_SPI6_NSS9,
#  endif
#  ifdef TTC_SPI6_NSS10
    TTC_SPI6_NSS10,
#  endif
};
#endif //}
#ifdef TTC_SPI7 //{

#if TTC_SPI7_NSS_AMOUNT == 0
    #    warning  No Slave Select pins defined for SPI7. Define at least TTC_SPI7_NSS1 from e_ttc_gpio_pin as pin being connected to SlaveSelect input of first slave device!
#endif

const e_ttc_gpio_pin ttc_spi_7_NSS[TTC_SPI7_NSS_AMOUNT + 1] = {
    E_ttc_gpio_pin_none, // investing this extra constant 32 bit makes ttc_spi routines a little bit faster as array index calculation is simpler.
#  ifdef TTC_SPI7_NSS1
    TTC_SPI7_NSS1
#  endif
#  ifdef TTC_SPI7_NSS2
    TTC_SPI7_NSS2,
#  endif
#  ifdef TTC_SPI7_NSS3
    TTC_SPI7_NSS3,
#  endif
#  ifdef TTC_SPI7_NSS4
    TTC_SPI7_NSS4,
#  endif
#  ifdef TTC_SPI7_NSS5
    TTC_SPI7_NSS5,
#  endif
#  ifdef TTC_SPI7_NSS6
    TTC_SPI7_NSS6,
#  endif
#  ifdef TTC_SPI7_NSS7
    TTC_SPI7_NSS7,
#  endif
#  ifdef TTC_SPI7_NSS8
    TTC_SPI7_NSS8,
#  endif
#  ifdef TTC_SPI7_NSS9
    TTC_SPI7_NSS9,
#  endif
#  ifdef TTC_SPI7_NSS10
    TTC_SPI7_NSS10,
#  endif
};
#endif //}
#ifdef TTC_SPI8 //{

#if TTC_SPI8_NSS_AMOUNT == 0
    #    warning  No Slave Select pins defined for SPI8. Define at least TTC_SPI8_NSS1 from e_ttc_gpio_pin as pin being connected to SlaveSelect input of first slave device!
#endif

const e_ttc_gpio_pin ttc_spi_8_NSS[TTC_SPI8_NSS_AMOUNT + 1] = {
    E_ttc_gpio_pin_none, // investing this extra constant 32 bit makes ttc_spi routines a little bit faster as array index calculation is simpler.
#  ifdef TTC_SPI8_NSS1
    TTC_SPI8_NSS1
#  endif
#  ifdef TTC_SPI8_NSS2
    TTC_SPI8_NSS2,
#  endif
#  ifdef TTC_SPI8_NSS3
    TTC_SPI8_NSS3,
#  endif
#  ifdef TTC_SPI8_NSS4
    TTC_SPI8_NSS4,
#  endif
#  ifdef TTC_SPI8_NSS5
    TTC_SPI8_NSS5,
#  endif
#  ifdef TTC_SPI8_NSS6
    TTC_SPI8_NSS6,
#  endif
#  ifdef TTC_SPI8_NSS7
    TTC_SPI8_NSS7,
#  endif
#  ifdef TTC_SPI8_NSS8
    TTC_SPI8_NSS8,
#  endif
#  ifdef TTC_SPI8_NSS9
    TTC_SPI8_NSS9,
#  endif
#  ifdef TTC_SPI8_NSS10
    TTC_SPI8_NSS10,
#  endif
};
#endif //}
#ifdef TTC_SPI9 //{

#if TTC_SPI9_NSS_AMOUNT == 0
    #    warning  No Slave Select pins defined for SPI9. Define at least TTC_SPI9_NSS1 from e_ttc_gpio_pin as pin being connected to SlaveSelect input of first slave device!
#endif

const e_ttc_gpio_pin ttc_spi_9_NSS[TTC_SPI9_NSS_AMOUNT + 1] = {
    E_ttc_gpio_pin_none, // investing this extra constant 32 bit makes ttc_spi routines a little bit faster as array index calculation is simpler.
#  ifdef TTC_SPI9_NSS1
    TTC_SPI9_NSS1
#  endif
#  ifdef TTC_SPI9_NSS2
    TTC_SPI9_NSS2,
#  endif
#  ifdef TTC_SPI9_NSS3
    TTC_SPI9_NSS3,
#  endif
#  ifdef TTC_SPI9_NSS4
    TTC_SPI9_NSS4,
#  endif
#  ifdef TTC_SPI9_NSS5
    TTC_SPI9_NSS5,
#  endif
#  ifdef TTC_SPI9_NSS6
    TTC_SPI9_NSS6,
#  endif
#  ifdef TTC_SPI9_NSS7
    TTC_SPI9_NSS7,
#  endif
#  ifdef TTC_SPI9_NSS8
    TTC_SPI9_NSS8,
#  endif
#  ifdef TTC_SPI9_NSS9
    TTC_SPI9_NSS9,
#  endif
#  ifdef TTC_SPI9_NSS10
    TTC_SPI9_NSS10,
#  endif
};
#endif //}
#ifdef TTC_SPI10 //{

#if TTC_SPI10_NSS_AMOUNT == 0
    #    warning  No Slave Select pins defined for SPI10. Define at least TTC_SPI10_NSS1 from e_ttc_gpio_pin as pin being connected to SlaveSelect input of first slave device!
#endif

const e_ttc_gpio_pin ttc_spi_10_NSS[TTC_SPI10_NSS_AMOUNT + 1] = {
    E_ttc_gpio_pin_none, // investing this extra constant 32 bit makes ttc_spi routines a little bit faster as array index calculation is simpler.
#  ifdef TTC_SPI10_NSS1
    TTC_SPI10_NSS1
#  endif
#  ifdef TTC_SPI10_NSS2
    TTC_SPI10_NSS2,
#  endif
#  ifdef TTC_SPI10_NSS3
    TTC_SPI10_NSS3,
#  endif
#  ifdef TTC_SPI10_NSS4
    TTC_SPI10_NSS4,
#  endif
#  ifdef TTC_SPI10_NSS5
    TTC_SPI10_NSS5,
#  endif
#  ifdef TTC_SPI10_NSS6
    TTC_SPI10_NSS6,
#  endif
#  ifdef TTC_SPI10_NSS7
    TTC_SPI10_NSS7,
#  endif
#  ifdef TTC_SPI10_NSS8
    TTC_SPI10_NSS8,
#  endif
#  ifdef TTC_SPI10_NSS9
    TTC_SPI10_NSS9,
#  endif
#  ifdef TTC_SPI10_NSS10
    TTC_SPI10_NSS10,
#  endif
};
#endif //}
//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

void                ttc_spi_deinit( t_u8 IndexSPI ) {
    t_ttc_spi_config* Config = ttc_spi_get_configuration( IndexSPI );

    _driver_spi_deinit( Config );
    Config->Flags.Initialized = 0;
}
t_u8                ttc_spi_get_max_index() {
    return TTC_SPI_AMOUNT;
}
t_ttc_spi_config*   ttc_spi_get_configuration( t_u8 IndexSPI ) {
    Assert_SPI( IndexSPI, ttc_assert_origin_auto );  // logical index starts at 1 !!!
    t_ttc_spi_config* Config = A( ttc_spi_configs, IndexSPI - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        Config = A( ttc_spi_configs, IndexSPI - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_spi_config ) );
        Config->Flags.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_spi_load_defaults( IndexSPI );
    }

    Assert_SPI_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto );  // memory corrupted?
    return Config;
}
e_ttc_gpio_pin      ttc_spi_get_pin_slave_select( t_u8 IndexSPI, t_u8 IndexSlave ) {
    t_ttc_spi_config* Config = ttc_spi_get_configuration( IndexSPI );
    Assert_SPI( IndexSlave > 0, ttc_assert_origin_auto ); // valid slave index starts at 1. Check implementation of caller!
    Assert_SPI( IndexSlave <= Config->Init.Pins_NSS_Amount, ttc_assert_origin_auto );  // exceeding amount of defined slave select pins! (Slave #1 <=> IndexSlave=0)

    return Config->Init.Pins_NSS[IndexSlave ];
}
e_ttc_spi_errorcode ttc_spi_init( t_u8 IndexSPI ) {

#if (TTC_ASSERT_SPI_EXTRA == 1)
    t_u8 InitialCriticalLevel = ttc_interrupt_critical_level();
#endif
    ttc_task_critical_begin();
    t_ttc_spi_config* Config = ttc_spi_get_configuration( IndexSPI );

    for ( t_u8 IndexNSS = 1; IndexNSS <= Config->Init.Pins_NSS_Amount; IndexNSS++ ) { // initialize all Slave Select pins
        e_ttc_gpio_pin PinNSS = Config->Init.Pins_NSS[IndexNSS];
        if ( PinNSS ) { // initialize pin and avoid to pull it low during initialization

            // power up GPIO bank
            ttc_gpio_init( PinNSS, E_ttc_gpio_mode_input_pull_up, E_ttc_gpio_speed_max );

            // output to high
            ttc_gpio_set( PinNSS );

            // initialize as output
            ttc_gpio_init( PinNSS, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );

            // set pin again to be sure
            ttc_gpio_set( PinNSS );
        }
    }

    Config->Init.Flags.HardwareNSS = 0; // slave select pin handling is done in software to provide more than one slave in a user friendly manner

    e_ttc_spi_errorcode Result = _driver_spi_init( Config );

    // check configuration changes done by low-level driver
    Assert_SPI( Config->Init.Flags.HardwareNSS == 0, ttc_assert_origin_auto );  // NSS pins are handled by ttc_spi or application only. Remove hardware NSS pin handling from low-level driver!
    if ( Result == ec_spi_OK )
    { Config->Flags.Initialized = 1; }

    ttc_task_critical_end();
    Assert_SPI_EXTRA( InitialCriticalLevel == ttc_interrupt_critical_level(), ttc_assert_origin_auto ); // amount of  ttc_task_critical_end() calls does not match. Check driver implementation!
    return Result;
}
e_ttc_spi_errorcode ttc_spi_master_send( t_u8 IndexSPI, t_u8 IndexSlave, const t_u8* BufferTx, t_u16 AmountTx ) {
    t_ttc_spi_config* Config = ttc_spi_get_configuration( IndexSPI );
    Assert_SPI( IndexSlave <= Config->Init.Pins_NSS_Amount, ttc_assert_origin_auto );
    e_ttc_spi_errorcode Error;

    if ( IndexSlave ) { // select+deselect slave manually (if configured)
        e_ttc_gpio_pin PinNSS = Config->Init.Pins_NSS[IndexSlave];
        if ( PinNSS ) { // handling slave select
            ttc_gpio_clr( PinNSS ); // select slave
        }

        Error = _driver_spi_send_raw( Config, BufferTx, AmountTx );

        if ( PinNSS ) { // handling slave select
            ttc_gpio_set( PinNSS ); // deselect slave
        }
    }
    else {              // no slave-select pin handling (shall be done outside)
        Error = _driver_spi_send_raw( Config, BufferTx, AmountTx );
    }

    return Error;
}
e_ttc_spi_errorcode ttc_spi_master_send_read( t_u8 IndexSPI, t_u8 IndexSlave, const t_u8* BufferTx, t_u16 AmountTx, t_u16 AmountZerosTx, t_u8* BufferRx, t_u16 AmountRx ) {
    t_ttc_spi_config* Config = ttc_spi_get_configuration( IndexSPI );
    Assert_SPI( IndexSlave <= Config->Init.Pins_NSS_Amount, ttc_assert_origin_auto );
    e_ttc_spi_errorcode Error;

    if ( IndexSlave ) { // select+deselect slave manually (if configured)
        e_ttc_gpio_pin PinNSS = Config->Init.Pins_NSS[IndexSlave];
        if ( PinNSS ) { // handling slave select
            ttc_gpio_clr( PinNSS ); // select slave
        }

        Error = _driver_spi_send_read( Config, BufferTx, AmountTx, AmountZerosTx, BufferRx, AmountRx );

        if ( PinNSS ) { // handling slave select
            ttc_gpio_set( PinNSS ); // deselect slave
        }
    }
    else {            // no slave-select pin handling (shall be done outside)
        Error = _driver_spi_send_read( Config, BufferTx, AmountTx, AmountZerosTx, BufferRx, AmountRx );
    }

    return Error;
}
t_u16               ttc_spi_master_read( t_u8 IndexSPI, t_u8 IndexSlave, t_u8* BufferRx, t_u16 AmountRx ) {
    t_ttc_spi_config* Config = ttc_spi_get_configuration( IndexSPI );
    Assert_SPI( IndexSlave <= Config->Init.Pins_NSS_Amount, ttc_assert_origin_auto );
    t_u16 AmountRead;

    if ( IndexSlave ) { // select+deselect slave manually (if configured)
        e_ttc_gpio_pin PinNSS = Config->Init.Pins_NSS[IndexSlave];
        if ( PinNSS ) { // handling slave select
            ttc_gpio_clr( PinNSS ); // select slave
        }

        AmountRead = _driver_spi_read_raw( Config, BufferRx, AmountRx );

        if ( PinNSS ) { // handling slave select
            ttc_gpio_set( PinNSS ); // deselect slave
        }
    }
    else {            // no slave-select pin handling (shall be done outside)
        AmountRead = _driver_spi_read_raw( Config, BufferRx, AmountRx );
    }

    return AmountRead;
}
e_ttc_spi_errorcode ttc_spi_load_defaults( t_u8 IndexSPI ) {
    t_ttc_spi_config* Config = ttc_spi_get_configuration( IndexSPI );

    if ( Config->Flags.Initialized )
    { ttc_spi_deinit( IndexSPI ); }

    ttc_memory_set( Config, 0, sizeof( t_ttc_spi_config ) );

    // load generic default values
    Config->LogicalIndex  = IndexSPI;
    Config->PhysicalIndex = ttc_spi_logical_2_physical_index( IndexSPI );

    //Insert additional generic default values here ...

    e_ttc_spi_errorcode Result = _driver_spi_load_defaults( Config );
    switch ( IndexSPI ) {
#ifdef TTC_SPI1
        case 1: {
            Config->Init.Pin_MISO = TTC_SPI1_MISO;
            Config->Init.Pin_MOSI = TTC_SPI1_MOSI;
            Config->Init.Pin_SCLK = TTC_SPI1_SCK;
            Config->Init.Pins_NSS = ttc_spi_1_NSS;
            Config->Init.Pins_NSS_Amount = TTC_SPI1_NSS_AMOUNT;
            break;
        }
#endif
#ifdef TTC_SPI2
        case 2: {
            Config->Init.Pin_MISO = TTC_SPI2_MISO;
            Config->Init.Pin_MOSI = TTC_SPI2_MOSI;
            Config->Init.Pin_SCLK = TTC_SPI2_SCK;
            Config->Init.Pins_NSS = ttc_spi_2_NSS;
            Config->Init.Pins_NSS_Amount = TTC_SPI2_NSS_AMOUNT;
            break;
        }
#endif
#ifdef TTC_SPI3
        case 3: {
            Config->Init.Pin_MISO = TTC_SPI3_MISO;
            Config->Init.Pin_MOSI = TTC_SPI3_MOSI;
            Config->Init.Pin_SCLK = TTC_SPI3_SCK;
            Config->Init.Pins_NSS = ttc_spi_3_NSS;
            Config->Init.Pins_NSS_Amount = TTC_SPI3_NSS_AMOUNT;
            break;
        }
#endif
#ifdef TTC_SPI4
        case 4: {
            Config->Init.Pin_MISO = TTC_SPI4_MISO;
            Config->Init.Pin_MOSI = TTC_SPI4_MOSI;
            Config->Init.Pin_SCLK = TTC_SPI4_SCK;
            Config->Init.Pins_NSS = ttc_spi_4_NSS;
            Config->Init.Pins_NSS_Amount = TTC_SPI4_NSS_AMOUNT;
            break;
        }
#endif
#ifdef TTC_SPI5
        case 5: {
            Config->Init.Pin_MISO = TTC_SPI5_MISO;
            Config->Init.Pin_MOSI = TTC_SPI5_MOSI;
            Config->Init.Pin_SCLK = TTC_SPI5_SCK;
            Config->Init.Pins_NSS = ttc_spi_5_NSS;
            Config->Init.Pins_NSS_Amount = TTC_SPI5_NSS_AMOUNT;
            break;
        }
#endif
#ifdef TTC_SPI6
        case 6: {
            Config->Init.Pin_MISO = TTC_SPI6_MISO;
            Config->Init.Pin_MOSI = TTC_SPI6_MOSI;
            Config->Init.Pin_SCLK = TTC_SPI6_SCK;
            Config->Init.Pins_NSS = ttc_spi_6_NSS;
            Config->Init.Pins_NSS_Amount = TTC_SPI6_NSS_AMOUNT;
            break;
        }
#endif
#ifdef TTC_SPI7
        case 7: {
            Config->Init.Pin_MISO = TTC_SPI7_MISO;
            Config->Init.Pin_MOSI = TTC_SPI7_MOSI;
            Config->Init.Pin_SCLK = TTC_SPI7_SCK;
            Config->Init.Pins_NSS = ttc_spi_7_NSS;
            Config->Init.Pins_NSS_Amount = TTC_SPI7_NSS_AMOUNT;
            break;
        }
#endif
#ifdef TTC_SPI8
        case 8: {
            Config->Init.Pin_MISO = TTC_SPI8_MISO;
            Config->Init.Pin_MOSI = TTC_SPI8_MOSI;
            Config->Init.Pin_SCLK = TTC_SPI8_SCK;
            Config->Init.Pins_NSS = ttc_spi_8_NSS;
            Config->Init.Pins_NSS_Amount = TTC_SPI8_NSS_AMOUNT;
            break;
        }
#endif
#ifdef TTC_SPI9
        case 9: {
            Config->Init.Pin_MISO = TTC_SPI9_MISO;
            Config->Init.Pin_MOSI = TTC_SPI9_MOSI;
            Config->Init.Pin_SCLK = TTC_SPI9_SCK;
            Config->Init.Pins_NSS = ttc_spi_9_NSS;
            Config->Init.Pins_NSS_Amount = TTC_SPI9_NSS_AMOUNT;
            break;
        }
#endif
#ifdef TTC_SPI10
        case 10: {
            Config->Init.Pin_MISO = TTC_SPI10_MISO;
            Config->Init.Pin_MOSI = TTC_SPI10_MOSI;
            Config->Init.Pin_SCLK = TTC_SPI10_SCK;
            Config->Init.Pins_NSS = ttc_spi_10_NSS;
            Config->Init.Pins_NSS_Amount = TTC_SPI10_NSS_AMOUNT;
            break;
        }
#endif
        default: ttc_assert_halt_origin( ec_spi_DeviceNotFound ); break;
    }

    return Result;
}
t_u8                ttc_spi_logical_2_physical_index( t_u8 IndexSPI ) {

    switch ( IndexSPI ) {         // determine base register and other low-level configuration data
#ifdef TTC_SPI1
        case 1: return TTC_SPI1;
#endif
#ifdef TTC_SPI2
        case 2: return TTC_SPI2;
#endif
#ifdef TTC_SPI3
        case 3: return TTC_SPI3;
#endif
#ifdef TTC_SPI4
        case 4: return TTC_SPI4;
#endif
#ifdef TTC_SPI5
        case 5: return TTC_SPI5;
#endif
#ifdef TTC_SPI6
        case 6: return TTC_SPI6;
#endif
#ifdef TTC_SPI7
        case 7: return TTC_SPI7;
#endif
#ifdef TTC_SPI8
        case 8: return TTC_SPI8;
#endif
#ifdef TTC_SPI9
        case 9: return TTC_SPI9;
#endif
#ifdef TTC_SPI10
        case 10: return TTC_SPI10;
#endif
        // extend as required

        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // constant defined as illegal value
    }

    Assert_SPI( 0, ttc_assert_origin_auto );  // No TTC_SPIn defined! Check your makefile.100_board_* file!
    return -1; // program flow should never go here
}
t_u8                ttc_spi_physical_2_logical_index( t_u8 PhysicalIndex ) {

    switch ( ( t_base ) PhysicalIndex ) {        // determine base register and other low-level configuration data
#ifdef TTC_SPI1
        case ( t_base ) TTC_SPI1: return 1;
#endif
#ifdef TTC_SPI2
        case ( t_base ) TTC_SPI2: return 2;
#endif
#ifdef TTC_SPI3
        case ( t_base ) TTC_SPI3: return 3;
#endif
#ifdef TTC_SPI4
        case ( t_base ) TTC_SPI4: return 4;
#endif
#ifdef TTC_SPI5
        case ( t_base ) TTC_SPI5: return 5;
#endif
#ifdef TTC_SPI6
        case ( t_base ) TTC_SPI6: return 6;
#endif
#ifdef TTC_SPI7
        case ( t_base ) TTC_SPI7: return 7;
#endif
#ifdef TTC_SPI8
        case ( t_base ) TTC_SPI8: return 8;
#endif
#ifdef TTC_SPI9
        case ( t_base ) TTC_SPI9: return 9;
#endif
#ifdef TTC_SPI10
        case ( t_base ) TTC_SPI10: return 10;
#endif
        // extend as required

        default: break;
    }

    Assert_SPI( 0, ttc_assert_origin_auto );  // No TTC_SPIn defined! Check your makefile.100_board_* file!
    return -1; // program flow should never go here
}
void                ttc_spi_prepare() {
    // add your startup code here (Singletasking!)
    A_reset( ttc_spi_configs, TTC_SPI_AMOUNT ); // must reset data of safe array manually!
    _driver_spi_prepare();
}
e_ttc_spi_errorcode ttc_spi_reset( t_u8 IndexSPI ) {
    t_ttc_spi_config* Config = ttc_spi_get_configuration( IndexSPI );

    if ( Config->Flags.Initialized )
    { return _driver_spi_reset( Config ); }

    // this is not really a problem
    return E_ttc_sysclock_errorcode_DeviceNotInitialized;
}
e_ttc_spi_errorcode ttc_spi_slave_send( t_u8 IndexSPI, const t_u8* BufferTx, t_u16 AmountTx ) {
    t_ttc_spi_config* Config = ttc_spi_get_configuration( IndexSPI );

    return _driver_spi_send_raw( Config, BufferTx, AmountTx );
}
e_ttc_spi_errorcode ttc_spi_slave_read( t_u8 IndexSPI, t_u8* BufferRx, t_u16 AmountRx ) {
    t_ttc_spi_config* Config = ttc_spi_get_configuration( IndexSPI );

    return _driver_spi_read_raw( Config, BufferRx, AmountRx );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_spi(t_u8 IndexSPI) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
