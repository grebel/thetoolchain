#ifndef TTC_USB_H
#define TTC_USB_H

/*{ ttc_usb.h **********************************************************
 
                      The ToolChain
                      
   Device independent support for USB Interfaces
   
   Currently implemented architectures: stm32

   
   written by Sascha Poggemann
   
   Reference:
   
   RM008 - STM32 Reference Manual p. 757
   -> http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/REFERENCE_MANUAL/CD00171190.pdf


}*/

//{ Includes *************************************************************

#include "ttc_basic.h"
#include "ttc_queue.h"
#include "ttc_memory.h"
#include "ttc_usb_types.h"
//#include "ttc_channel_types.h"

#ifdef EXTENSION_400_stm32_usb_fs_device_lib
#ifdef TARGET_ARCHITECTURE_STM32F1xx
#include "stm32/stm32_usb.h"
//#include "hw_config.h"
//#include "usb_lib.h"
//#include "usb_desc.h"
//#include "usb_pwr.h"
#endif //TARGET_ARCHITECTURE_STM32F1xx
#endif //EXTENSION_400_stm32_usb_fs_device_lib

#ifdef EXTENSION_400_stm32_usb_fs_host_lib
#ifdef TARGET_ARCHITECTURE_STM32F1xx
#include "stm32/stm32_usb.h"
#include "usb_bsp.h"
#include "usbh_core.h"
#include "usbh_usr.h"
#include "usbh_hid_core.h"
#endif //TARGET_ARCHITECTURE_STM32F1xx
#endif //EXTENSION_400_stm32_usb_fs_host_lib


//} Includes
//{ Defines/ TypeDefs ****************************************************

//} Defines
//{ Structures/ Enums ****************************************************

//} Structures/ Enums
//{ Global Variables *****************************************************



//} Global Variables
//{ Function prototypes **************************************************

const ttc_channel_device_interface_t* ttc_usb_get_channel_interface();

/** returns amount of USB devices available on current uC
 * @return amount of available USB devices
 */
u8_t ttc_usb_get_max_index() ;

/** returns configuration of indexed usb (asserts if no valid configuration was read)
 *
 * @param LogicalIndex    device index of USB to init (1..ttc_usb_get_max_logicalindex())
 * @return                !=NULL: pointer to struct ttc_usb_generic_t
 */
ttc_usb_generic_t* ttc_usb_get_configuration(u8_t LogicalIndex);

/** fills out given USB_Generic with default values for indexed USB
 * @param LogicalIndex    device index of USB to init (1..ttc_usb_get_max_logicalindex())
 * @return  == 0:         everything is ok otherwise ttc_usb_errorcode_e
 */
ttc_usb_errorcode_e ttc_usb_load_defaults(u8_t LogicalIndex) ;

/** resets given USB Interface
 * @param LogicalIndex    device index of USB to be reset (1..ttc_usb_get_max_logicalindex())
 * @return                == 0: USB has been resetted successfully; != 0: error-code
 */
ttc_channel_errorcode_e ttc_usb_reset(u8_t LogicalIndex);

/** fills out given USART_Generic with maximum valid values for indexed USART
 * @param LogicalIndex    device index of USART to init (1..ttc_usb_get_max_logicalindex())
 * @param USART_Generic   pointer to struct ttc_usb_generic_t
 * @return  == 0:         *USART_Generic has been initialized successfully; != 0: error-code
 */
ttc_channel_errorcode_e ttc_usb_get_features(u8_t LogicalIndex, ttc_usb_generic_t* USART_Generic);

/** delivers a memory block usable as transmit or receive buffer
 *
 * @param LogicalIndex   device index of USART to init (1..ttc_usb_get_max_logicalindex())
 * @return               != NULL: pointer to new or reused memory block; == NULL: currently no tx-buffer available (-> increase USART_Generic->Size_Queue_Tx or wait until tx-buffer is released)
 */
ttc_heap_block_t* ttc_usb_get_empty_block();

/** initializes single USART with current configuration
 * @param LogicalIndex    device index of USART to init (1..ttc_usb_get_max_logicalindex())
 * @return                == 0: USART has been initialized successfully; != 0: error-code
 */
ttc_usb_errorcode_e ttc_usb_init(u8_t LogicalIndex);

/** registers given function as a receiver for incoming data
 *
 * Note: The registered receive function must process the data. Until it returns, no new data can be received by the USB Interface.
 *       hence the user should try to keept this function as short as possible.
 *
 * @param LogicalIndex    device index of USB Interface to use (1..ttc_usb_get_max_logicalindex())
 * @param RxFunction      user callback function to be registered
 */
void ttc_usb_register_rx_function(u8_t LogicalIndex, void (*RxFunction)(u8_t* Data_Buffer, u8_t Amount));
/** registers given function as a User Callback function for the transmitted data
 *
 * Note: The registered user callback function will block the transmission of further data via USB until it returns
 *       Hence the user should try to keept this function as short as possible.
 *
 * @param LogicalIndex    device index of USB Interface to use (1..ttc_usb_get_max_logicalindex())
 * @param TxFunction      user callback function to be registered
 */
void ttc_usb_register_tx_UCB_function(u8_t LogicalIndex, void (*TxFunction)(u8_t* Data_buffer, u8_t Amount));

/** Stores given raw buffer to be send in parallel. (Function copies data and returns immediately)
 *
 * Note: USART must be initialized before!
 * Note: This function is partially thread safe (use it for each USART from one thread only)!
 *
 * @param LogicalIndex     device index of USB to init (1..ttc_usb_get_max_logicalindex())
 * @param Buffer           memory location from which to send data
 * @param Amount           amount of bytes to send from Buffer[] (even zero bytes are sent)
 * @return                 == 0: Buffer has been stored for delivery successfully; != 0: error-code
 */
ttc_channel_errorcode_e ttc_usb_send_raw(u8_t LogicalIndex, u8_t* Buffer, Base_t Amount);

/** private function that will be called to get Data received via USB
 *
 * @param LogicalIndex     device index of USB to init (1..ttc_usb_get_max_logicalindex())
 * @param Buffer           memory location from which to read data
 * @param Amount           amount of bytes to read from Buffer[]
 */
void _ttc_usb_receive_buffer(u8_t LogicalIndex, u8_t * Buffer, u8_t Amount);


//} Function prototypes

#endif //ttc_usb_H
