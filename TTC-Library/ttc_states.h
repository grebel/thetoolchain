#ifndef TTC_STATES_H
#define TTC_STATES_H
/** { ttc_states.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for states devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for ttc_states differs from other ttc_* devices:
 *  1) Define an enumeration that names all states of your statemachine.
 *     typedef enum {
 *         my_state_number_Reset = 0, // not usable as state number!
 *         my_state_number_State1,
 *         ...
 *     } e_my_my_state_number;
 *  2) Declare and define one statemachine function for each state:
 *     void state_1( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );
 *     void state_1( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition ) { ... }
 *     ...
 *  3) const t_ttc_states_state State1 = { Declaration of state #1 }.
 *     ...
 *  4) t_ttc_states_state* AllStates[] = { &State1, ..., 0 }
 *  5) ANY_TYPE* StateData = allocated memory block to hold variables required by state functions.
 *  6) create new:  MyStatemachine = ttc_states_create();
 *  7) configure:   MyStatemachine->Init_AllStates  = AllStates; // array declaring all states
 *                  MyStatemachine->Init_StateData  = StateData; // pointer to data storage
 *                  MyStatemachine->Init_Stack_Size = 1..;       // maximum depth of push operations
 *  8) initialize:  ttc_states_init(MyStatemachine->LogicalIndex);
 *  9) use:         ttc_states_run(MyStatemachine);
 *                  ttc_states_switch(MyStatemachine, NewState);
 *                  ttc_states_call(MyStatemachine, NewState, (void*) Argument);
 *                  ttc_states_return(MyStatemachine);
 *                  Argument = ttc_states_get_argument(MyStatemachine);
 *                  ReturnValue = ttc_states_get_return(MyStatemachine);
 *
 *  LogicalIndex
 *    Different statemachines can be created. Each one gets a new, enumerated logical index.
 *    ttc_states differs from other ttc_ devices. The logical index is only used to
 *    obtain the configuration pointer to an existing statemachine. All other functions
 *    require a configuration pointer for performance issues.
 *
 */
/** Description of ttc_states (Do not delete this line!)
 *
 *  What is a Statemachine?
 *
 *    A statemachine written in C is a function that is called at high frequency and
 *    manages tasks of different bandwidths.
 *    Such a task can be (among others):
 *    - User input from keyboard/ switches
 *    - Graphical display
 *    - Data measures (from analog or digital inputs)
 *    - Controlling (to analog or digital outputs)
 *    - Communication (via UART, SPI, I2C, Ethernet, Radio or other interfaces)
 *    - Network protocol handling (asynchronous communication with remote devices)
 *
 *  A Typical Statemachine Implementation
 *
 *    // main event loop (e.g. inside main() )
 *    while (1) {
 *      statemachine1(Data);
 *      ..call other statemachines...
 *    }
 *
 *    void statemachine1(MyDate_t* Data) {
 *      switch (Data->State) {
 *        case 1: ...handle state #1...
 *                break;
 *        case 2: ...handle state #2...
 *                Data->State = 3; // switch to state 3
 *                break;
 *        ...
 *      }
 *    }
 *
 *
 *  Advantages of Statemachines over Multitasking
 *    + Statemachines have no locking issues
 *      Each state can access each register without being interrupted by a task switch.
 *      So no extra mutexes and semaphores are required to secure read-modify-write operations.
 *
 *    + Statemachines work in Single- and Multitasking Environments
 *      A statemachine function can be run from a main eventloop or from a single task.
 *      If run from a task, the application must be designed so that no two statemachines concurrently
 *      operate on the same resources (mostly registers and data fields).
 *
 *    + Simple Stack Management
 *      In a single tasking setup, only two dynamic memory storages are in use:
 *      - The heap
 *      - One main stack
 *      Typically, these start at opposite ends of available RAM and grow towards each other:
 *      | BSS | DATA | Heap --> | unused | <-- Stack |
 *      | --static-- | -----------dynamic----------- |
 *      Having only one main stack allows to call lots of nested functions or even implement recursions.
 *      A single stack typically also reduces amount of unused RAM than with multiple stacks as in
 *      multitasking environments.
 *
 *
 *  Disadvantes of Statemachines compared to Multitasking
 *    + If multiple statemachines are run, processing time is difficult to balance between them.
 *    + Statemachine implementations are complex and difficult to read.
 *    + A statemachine does not benefit from a multi core cpu.
 *    + Use of interrupt service routines requires same locking mechanisms as for multitasking.
 *
 *
 *  How can ttc_states help to write your Statemachine?
 *    1) Static Configuration of allowed State Switches.
 *       No RAM is occupied for state declarations.
 *       The declarations of all states is checked during ttc_state_init() to avoid invalid configurations.
 *    2) Dynamic Transition Checks.
 *       Every state switch is checked during runtime. An assert will trigger if application tries
 *       to switch to a state that should not be reachable from the current one.
 *    3) Self Checks can be disabled.
 *       If TTC_ASSERT_STATES==0 then all state check overhead is disabled and removed from
 *       compiled binary for maximum speed.
 *    4) Sub-States work like Subroutines.
 *       The current state can be pushed onto a Last-In-First-Out stack to be able to write universal
 *       states that can return to multiple previous.
 *       A single argument and a return value are also supported.
 *    5) Multiple Statemachines can run kind of parallel.
 *       As shown above, a main eventloop can run several statemachine functions.
 *       On a single core cpu this is basically the same as in cooperative multitasking.
 *       The granularity of task switching is determined by the worst case runtime of each
 *       statemachine function.
 *    6) An individual monitor function allows to debug state transitions.
 *       MyStatemachine->Init_MonitorTransition can be loaded with a pointer to a function that will be
 *       called for every state transition. Place a breakpoint inside and see every transition.
 *
 *
 * Using ttc_states with Multitasking and Interrupt Service Routines
 *
 *   Different tasks may use different instances of ttc_states without interference.
 *   No ttc_state_*() function, despite ttc_states_init(), is protected from concurrent
 *   access to the same configuration for performance reasons.
 *   If multiple tasks should access the same ttc_states configuration, the application
 *   has to use ttc_mutex to secure every ttc_states_*() call.
 *
 *   The same applies if the same statemachine shall be accessed by main or single task
 *   application and an interrupt service routine. In such scenarios, it is typically
 *   safer to have the isr push incoming events into an event queue (->ttc_queue) or list
 *   (->ttc_list) and use one state to check for pushed events.
 *
 *
 * See example_ttc_states for a life ttc_states statemachine.
 *
 *
 * Created from template ttc_device.h revision 36 at 20180126 14:53:05 UTC
 *
 * Authors: Gregor Rebel
}*/
/** Template for your own statemachine using ttc_states

// copy lines below into your header (.h) file ------------------------------------

typedef struct { // t_my_states_data
    t_ttc_states_config* MyStatemachine; // pointer to configuration of ttc_states device to use
    t_ttc_systick_delay  Delay;          // this type of delay is usable in single- and multitasking setup

    // more data...
} t_my_states_data;


typedef enum {  // named states of our statemachine improve readability
    my_state_number_Reset = 0,  // no real state (reset value)

    // states below provide an example setup of a fictional application
    my_state_number_Init,       // statemachine initialization (run as first state)
    //... add more state numbers here
    my_state_number_SubFoo,     // example of a sub-state

    // state numbers of several statemachines may be defined in same enumeration
    my_state_number_2_Init,     // statemachine initialization (run as first state)
    //... add more state numbers here

    my_state_number_unknown     // higher values are invalid
} e_my_state_number;


// copy lines below into your source (.c) file ------------------------------------

/ ** monitor function being called for every state transition as a debugging aid
 *
 * Note: This function is only called if TTC_ASSERT_STATES == 1
 * Note: Rename all my_ keywords with your own prefix to avoid name collisions!
 * Usage: Start a debug session and place a breakpoint inside:
 *        gdb> b ttc_states_monitor
 *        gdb> c
 *
 * @param MyStatemachine       (volatile t_ttc_states_config*)  configuration of initialized ttc_states instance
 * @param State_Current        (volatile TTC_STATES_TYPE)       state being transitioned to
 * @param State_Previous       (volatile TTC_STATES_TYPE)       state being transitioned from
 * @param Transition           (e_ttc_states_transition)        cause of transition
 * /  // <- remove space!
void _my_states_monitor( volatile t_ttc_states_config* MyStatemachine, t_my_states_data* MyStateData, volatile e_my_state_number State_Current, volatile e_my_state_number State_Previous, e_ttc_states_transition Transition );

void _my_states_monitor( volatile t_ttc_states_config* MyStatemachine, t_my_states_data* MyStateData, volatile e_my_state_number State_Current, volatile e_my_state_number State_Previous, e_ttc_states_transition Transition ) {
    // place breakpoint here!

    // avoid compiler warnings about unused variables
    (void) MyStatemachine;
    (void) MyStateData;
    (void) State_Current;
    (void) State_Previous;
    (void) Transition;

    volatile static t_u32 CallCounter = 0; // counts function calls
    const           t_u32 BreakAt     = 0; // set to value > 0 to enable breakpoint at certain function call
    CallCounter++;

    if (BreakAt && (CallCounter == BreakAt) ) {
        ttc_assert_break_origin(ttc_assert_origin_auto); // desired function call reached
    }
}

/ * statemachine state function for state init
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_my_states_data*)        pointer to data known by state function
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 * /  // <- remove space!
void _my_state_init( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );

/ ** Declare each individual state of our statemachine as static data.
 *  As everything is constant, it will be put in FLASH and does not occupy any RAM.
 *
 * Attributes of each state:
 * + StateFunction
 *   Pointer to a function that will be called while this state is active.
 *
 * + AmountReachable
 *   Amount of other states to which may be switched from this state.
 *   Any other state switch will cause an assert.
 *
 * + ReachableStates[]
 *   Array of state enums to which this state may switch to.
 *   The array must be terminated by a zero (0) for extra safety.
 *
 * /  // <- remove space!
const t_ttc_states_state ets_State_Init = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
                     & _my_state_init,

    // Unique number identifying this state
    .Number = my_state_number_Init,

    // amount of reachable states listed below
    .AmountReachable = ...,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        ..., // place entries from e_my_state_number here
        0
    }
};

const t_ttc_states_state ets_SubState_Foo = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) )
                     & _my_state_sub_foo,

    // Unique number identifying this state
    .Number = my_state_number_SubFoo,

    // amount of reachable states listed below
    .AmountReachable = ...,

    // substates must be declared explicitely
    .Flags = { .IsSubstate = 1 },

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        ..., // place entries from e_my_state_number here
        0
    }
};

// More state declarations...


/ ** Store references of all states in a zero terminated array.
 *
 *  Entries must be sorted by their number without vacancies!
 *  my_AllStates[i]->Number + 1 = my_AllStates[i + 1]>Number
 * /  // <- remove space!
const t_ttc_states_state* my_AllStates[] = {
    &ets_State_Init,      // statemachine always starts in state of first entry
    &ets_SubState_Foo,    // example of a sub-state
    // More state declaration pointers..

    NULL                  // array must be zero terminated!
};


// implementation of a simple state function
void _my_state_init( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition ) {
    // only we know what type of pointer is stored because we set it before
    t_my_states_data* MyStateData = ( t_my_states_data* ) StateData;

    switch (Transition) {
        case states_transition_Switch: { // came from different state: initialize this state

            //.. do some initialization stuff
            break;
        }
        case states_transition_Return:  { // came back from a sub-state via ttc_state_return()
            // obtain return value of sub-state
            //SOMETYPE ReturnValue = ( SOMETYPE ) ttc_states_get_return( MyStatemachine );

            //.. process return from sub-state
            break;
        }
        default: {                      // state did not change since previous call

            //..

            // Switch to next state
            //ttc_states_switch( MyStatemachine, NEXT_STATE );

            // OR call a sub-state
            //ttc_states_call( MyStatemachine, NEXT_STATE, (void*) ARGUMENT );

            break;
        }
    }

}


// implementation of a sub-state function
void _my_state_sub_foo( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition ) {
    // only we know what type of pointer is stored because we set it before
    t_my_states_data* MyStateData = ( t_my_states_data* ) StateData;

    switch (Transition) {
        case states_transition_Switch: { // came from different state: initialize this state
            // obtaining value of Argument in ttc_states_call() call
            //SOMETYPE ArgumentValue = ( SOMETYPE ) ttc_states_get_argument( MyStatemachine );

            //.. do some initialization stuff
            break;
        }
        case states_transition_Return:  { // came back from a sub-state via ttc_state_return()
            // obtain return value of sub-state
            //SOMETYPE ReturnValue = ( SOMETYPE ) ttc_states_get_return( MyStatemachine );

            //.. process return from sub-state
            break;
        }
        default: {                      // state did not change since previous call

            //..

            // Switch to next state (which should call ttc_states_return and never switch to our calling state!)
            //ttc_states_switch( MyStatemachine, NEXT_STATE );

            // call a sub-state
            //ttc_states_call( MyStatemachine, NEXT_STATE, (void*) ARGUMENT );

            // return to calling state (mandatory at end of sub-state processing)
            ttc_states_return( MyStatemachine, (void*) RETURN );

            break;
        }
    }
}

// More state function definitions...

*/

#ifndef EXTENSION_ttc_states
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_states.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_states.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "ttc_states_types.h" // architecture independent states datatypes
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level states only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function Declarations ************************************************{
 *
 * This high-level only device has no low-level drivers or any interface.
 * All functions are implemented in ttc_states.c.
 */

/** Prepares states Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_states_prepare();
void _driver_states_prepare();

/** Create configuration for a new statemachine.
 *
 * Each new configuration gets a unique, enumerated logical index stored inside
 * returned reference. Its value is required to initialize the new statemachine before usage.
 *
 * Example:
 *   // create new statemachine instance
 *   t_ttc_states_config* MyStatemachine = ttc_states_create();
 *
 *   // configure new statemachine
 *   MyStatemachine->Init_AllStates = ...;
 *   MyStatemachine->Init_StateData = ...;
 *   MyStatemachine->Init_Stack_Size = ...;
 *
 *   // initialize new statemachine for operation
 *   ttc_states_init( MyStatemachine->LogicalIndex );
 *
 *   // run new statemachine
 *   while (1) {
 *     ttc_states_run( MyStatemachine );
 *   }
 *
 * @return (t_ttc_states_config*)  allocated and reset configuration to be filled by calling application
 */
t_ttc_states_config* ttc_states_create();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_states_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    logical index of STATES device. Each logical device <n> is defined via TTC_STATES<n>* constants in compile_options.h and extensions.active/makefile
 * @return                configuration of indexed device
 */
t_ttc_states_config* ttc_states_get_configuration( t_u8 LogicalIndex );

/** initialize indexed statemachine for use
 *
 * Prior to initialization, a statemachine must be created (->ttc_states_create()) and configured.
 * Configuration means to set certain fields in a t_ttc_states_config instance:
 *
 * MyStatemachine = ttc_states_create();
 *
 * // All fields to initialize have prefix Init_ (easy, isn't it?)
 * MyStatemachine->Init_AllStates         = ...; // array of pointers to t_ttc_states_state instances
 * MyStatemachine->Init_StateData         = ...; // pointer to a memory block of any structure being usable by statemachine functions
 * MyStatemachine->Init_Stack_Size        = ...; // maximum allowed depth of ttc_states_call() calls (size of LIFO stack)
 * MyStatemachine->Init_MonitorTransition = ...; // pointer to debug function to call in every state transition (whenever ttc_states_run() is called for a different state)
 * ...
 * ttc_states_init(MyStatemachine);
 *
 * See example_ttc_states.c for a life example of a statemachine declaration and usage!
 *
 * @param Config   (t_ttc_states_config*)  Configuration of an initialized ttc_states instance
 * @return         == 0: states device has been initialized successfully; != 0: error-code
 */
e_ttc_states_errorcode ttc_states_init( t_ttc_states_config* Config );

/** shutdown indexed device
 *
 * @param LogicalIndex    logical index of STATES device. Each logical device <n> is defined via TTC_STATES<n>* constants in compile_options.h and extensions.active/makefile
 */
void ttc_states_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_states_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of STATES device. Each logical device <n> is defined via TTC_STATES<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: default values have been loaded successfully for indexed states device; != 0: error-code
 */
e_ttc_states_errorcode  ttc_states_load_defaults( t_u8 LogicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    logical index of STATES device. Each logical device <n> is defined via TTC_STATES<n>* constants in compile_options.h and extensions.active/makefile
 */
void ttc_states_reset( t_u8 LogicalIndex );

/** Switch statemachine to a new state.
 *
 * Corresponding state function will be called on next ttc_states_run() call.
 * If state is switched multiple times before next ttc_states_run() call, only the state
 * function for the last switch will be called.
 *
 * Note: Will cause an assert if new state is not configured to be reachable from current state.
 *
 * @param Config   (t_ttc_states_config*)  Configuration of an initialized ttc_states instance
 * @param NewState (TTC_STATES_TYPE)       Number of new state to switch to
 */
void ttc_states_switch( t_ttc_states_config* Config, TTC_STATES_TYPE NewState );

/** Reports array of previous states for debugging purposes.
 *
 * @param LogicalIndex  (t_u8)  logical index of STATES device. Each logical device <n> is defined via TTC_STATES<n>* constants in compile_options.h and extensions.active/makefile
 * @return  (TTC_STATES_TYPE*)  !=NULL: zero terminated array of recorded previous states
 */
TTC_STATES_TYPE* ttc_states_history( t_u8 LogicalIndex );

/** Call this function repeatedly from a main-eventloop to keep your statemachine running.
 *
 * This function must NOT BE CALLED FROM A STATE FUNCTION!
 * Doing so will lead to unpredected results.
 * State functions should only call ttc_states_switch(), ttc_states_call() or ttc_states_return().
 *
 * @param Config   (t_ttc_states_config*)  Configuration of an initialized ttc_states instance
 */
void ttc_states_run( t_ttc_states_config* Config );

/** Pushes current state on a LIFO (Last In First Out) stack and increases Config->Index_Stack.
 *
 * This function allows to use a state as a kind of subroutine.
 *
 * Example:
 *   state_Foo:
 *     // data on which sub-state should operate on
 *     // (must be static or allocated from heap to survive end of this function!)
 *     static data_t Data;
 *
 *     ttc_states_call(MyStatemachine, state_Subroutine, &Data);
 *
 *   state_Subroutine:
 *     // get argument of sub-state call
 *     data_t* MyData = (data_t*) ttc_states_argument(MyStatemachine);
 *     ...
 *     // back to calling state (returns value MyData->A)
 *     ttc_states_return(MyStatemachine, (void*) MyData->A);
 *
 * @param Config   (t_ttc_states_config*)  Configuration of an initialized ttc_states instance
 */
void ttc_states_call( t_ttc_states_config* Config, TTC_STATES_TYPE NewState, void* Argument );

/** return from a sub-state to its calling state
 *
 * What it does: Switches to state which previously called ttc_states_call()
 *
 * Note: This function may only be called from a state function that:
 *       + Has been declared as a substate
 *         .Flags = { .IsSubstate = 1 },
 *       + Has been switched to via a ttc_states_call() call
 *
 * @param Config   (t_ttc_states_config*)  Configuration of an initialized ttc_states instance
 * @param Return   (void*)                 any value to return (calling state can read it via ttc_states_get_return() )
 */
void ttc_states_return( t_ttc_states_config* Config, void* Return );


/** get value of Argument given to previous ttc_states_call() call
 *
 * Note: This function may only be called from a state function as described for ttc_states_return()!
 *
 * @param Config (t_ttc_states_config*)
 * @return       (void*)
 */
void* ttc_states_get_argument( t_ttc_states_config* Config );

/** get value from latest return from sub-state
 *
 * Note: Return value is erased by next ttc_states_call() call.
 *       Read and store this value immediately when returning to calling state!
 *
 * @param Config   (t_ttc_states_config*)  Configuration of an initialized ttc_states instance
 * @return         (void*)                 value given as argument to Return on previous ttc_states_return() call (== return value from sub-state)
 */
void* ttc_states_get_return( t_ttc_states_config* Config );


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_states_ are passed to interfaces/ttc_states_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl states UPDATE
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_states_configuration_check() has already checked Config against
 *       all fields being found in ttc_states_features_t.
 *
 * @param Config        Configuration of states device
 * @return              Fields of *Config have been adjusted if necessary
 */
void _driver_states_configuration_check( t_ttc_states_config* Config );

/** shutdown single STATES unit device
 * @param Config        Configuration of states device
 * @return              == 0: STATES has been shutdown successfully; != 0: error-code
 */
e_ttc_states_errorcode _driver_states_deinit( t_ttc_states_config* Config );

/** initializes single STATES unit for operation
 * @param Config        Configuration of states device
 * @return              == 0: STATES has been initialized successfully; != 0: error-code
 */
e_ttc_states_errorcode _driver_states_init( t_ttc_states_config* Config );

/** loads configuration of indexed STATES unit with default values
 * @param Config        Configuration of states device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_states_errorcode _driver_states_load_defaults( t_ttc_states_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of states device
 */
void _driver_states_reset( t_ttc_states_config* Config );
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_STATES_H
