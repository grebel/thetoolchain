/*{ ttc_basic.c ***********************************************
 *
 * TheToolChain.com
 *
 * Written by Gregor Rebel 2010-2011
 *
 * Basic set of helper functions.
 * 
}*/
#include "ttc_basic.h"
#include "ttc_sysclock.h"

//? extern void ttc_task_begin_criticalsection(void (*Owner)());
//? extern void ttc_task_end_criticalsection();
//? extern void _ttc_task_yield();
//? extern void ttc_sysclock_prepare(); // -> ttc_sysclock.c

void ttc_hardware_init() { // ToDo: put into extension initializer

    //ttc_sysclock_prepare();

//#ifdef TARGET_ARCHITECTURE_STM32L1xx
//#warning ToDo: Move to ttc_sysclock_prepare(): stm32l1_hardware_init() -> done.
//    stm32l1_hardware_init();
//#endif
}
bool Assert_On_False(BOOL Condition) {
    if (!Condition)
        Assert_Halt_EC(ec_UNKNOWN);

    return Condition;
}
void Assert_SameAddress(volatile void* Address1, volatile void* Address2) {
    volatile u32_t A = (volatile u32_t) (Address1);
    volatile u32_t B = (volatile u32_t) (Address2);
    if (A != B)
        Assert_Halt_EC(ec_InvalidImplementation);
}
void assert_failed(u8_t* File, u32_t Line) {
    (void) File;
    (void) Line;
    Assert_Halt_EC(ec_UNKNOWN);
}
void Assert_Halt_EC(volatile ErrorCode_e ErrorCode) {  // block endless in case Condition is false
    volatile bool HoldOnAssert=TRUE;

    while (HoldOnAssert) {
        ttc_task_yield();
        ErrorCode = ErrorCode; // <-- breakpoint here!
    }
    // Only returns if HoldOnAssert==FALSE
}
void Assert_Halt_NULL() {
    Assert_Halt_EC(ec_NULL);
}
void ttc_testpoint() {
    u8_t Foo = 1;
    (void) Foo; // break here
}
u32_t _ttc_atomic_read_write_32(u32_t* Address, u32_t NewValue) {
    Assert(Address, ec_InvalidArgument);

    ttc_task_begin_criticalsection();
    u32_t OldValue = *Address;
    *Address = NewValue;
    ttc_task_end_criticalsection();

    return OldValue;
}
u16_t _ttc_atomic_read_write_16(u16_t* Address, u16_t NewValue) {
    Assert(Address, ec_InvalidArgument);

    ttc_task_begin_criticalsection();
    u16_t OldValue = *Address;
    *Address = NewValue;
    ttc_task_end_criticalsection();

    return OldValue;
}
u8_t  _ttc_atomic_read_write_8(u8_t* Address, u8_t NewValue) {
    Assert(Address, ec_InvalidArgument);

    ttc_task_begin_criticalsection();
    u8_t OldValue = *Address;
    *Address = NewValue;
    ttc_task_end_criticalsection();

    return OldValue;
}
u32_t _ttc_atomic_add_32(s32_t* Address, s32_t Amount) {
    Assert(Address, ec_InvalidArgument);

    ttc_task_begin_criticalsection();
    s32_t Value = *Address;
    Value = Value  + Amount;
    //X if (Amount > 0)
    //X     Value += (u32_t) Amount;
    //X else
    //X     Value -= (u32_t) -Amount;
    *Address = Value;
    ttc_task_end_criticalsection();

    return Value;
}
u16_t _ttc_atomic_add_16(s16_t* Address, s16_t Amount) {
    Assert(Address, ec_InvalidArgument);

    ttc_task_begin_criticalsection();
    s16_t Value = *Address;
    Value = Value  + Amount;
    //X if (Amount > 0)
    //X     Value += (u16_t) Amount;
    //X else
    //X     Value -= (u16_t) -Amount;
    *Address = Value;
    ttc_task_end_criticalsection();

    return Value;
}
u8_t  _ttc_atomic_add_8(s8_t* Address, s8_t Amount) {
    Assert(Address, ec_InvalidArgument);

    ttc_task_begin_criticalsection();
    s8_t Value = *Address;
    Value = Value  + Amount;
    //X if (Amount > 0)
    //X     Value += (u8_t) Amount;
    //X else
    //X     Value -= (u8_t) -Amount;
    *Address = Value;
    ttc_task_end_criticalsection();

    return Value;
}
