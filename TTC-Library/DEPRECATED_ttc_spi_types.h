#ifndef TTC_SPI_TYPES_H
#define TTC_SPI_TYPES_H

/*{ ttc_spi_types.h ***********************************************
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Basic defines, enums and structures.
 *
 * Include this file from 
 * architecture independent header files (ttc_XXX.h) and 
 * architecture depend      header files (e.g. stm32_XXX.h)
 * 
}*/
//{ Enums1 *********************************************************

typedef enum {   // ttc_spi_errorcode_e
    tse_OK,                // =0: no error
    tse_TimeOut,           // timeout occured in called function
    tse_DeviceNotFound,    // adressed SPI device not available in current uC
    tse_NotImplemented,    // function has no implementation for current architecture
    tse_InvalidArgument,   // general argument error
    tse_DeviceNotReady,    // choosen device has not been initialized properly

    tse_UnknownError
} ttc_spi_errorcode_e;

typedef enum {   // ttc_spi_physical_index_e
    tsp_none,
    SPI1_Physical,
    SPI2_Physical,
    SPI3_Physical,
    SPI4_Physical,
    SPI5_Physical
} ttc_spi_physical_index_e;

//} Enums1
//{ Includes *************************************************************

#include "ttc_basic_types.h"

#ifdef TARGET_ARCHITECTURE_STM32F1xx
#  include "stm32/stm32_spi_types.h"
#endif

#ifdef TARGET_ARCHITECTURE_STM32W1xx
  #include "stm32w/stm32w_spi_types.h"
  typedef stm32w_spi_architecture_t ttc_spi_architecture_t;
#endif


//} Includes
//{ Defines/ TypeDefs ****************************************************

// TTC_SPIn has to be defined as constant by makefile.100_board_*
#ifdef TTC_SPI5
  #ifndef TTC_SPI4
    #error TTC_SPI5 is defined, but not TTC_SPI4 - all lower TTC_SPIn must be defined!
  #endif
  #ifndef TTC_SPI3
    #error TTC_SPI5 is defined, but not TTC_SPI3 - all lower TTC_SPIn must be defined!
  #endif
  #ifndef TTC_SPI2
    #error TTC_SPI5 is defined, but not TTC_SPI2 - all lower TTC_SPIn must be defined!
  #endif
  #ifndef TTC_SPI1
    #error TTC_SPI5 is defined, but not TTC_SPI1 - all lower TTC_SPIn must be defined!
  #endif

  #define TTC_AMOUNT_SPIS 5
#else
  #ifdef TTC_SPI4
    #define TTC_AMOUNT_SPIS 4

    #ifndef TTC_SPI3
      #error TTC_SPI5 is defined, but not TTC_SPI3 - all lower TTC_SPIn must be defined!
    #endif
    #ifndef TTC_SPI2
      #error TTC_SPI5 is defined, but not TTC_SPI2 - all lower TTC_SPIn must be defined!
    #endif
    #ifndef TTC_SPI1
      #error TTC_SPI5 is defined, but not TTC_SPI1 - all lower TTC_SPIn must be defined!
    #endif
  #else
    #ifdef TTC_SPI3
    
      #ifndef TTC_SPI2
        #error TTC_SPI5 is defined, but not TTC_SPI2 - all lower TTC_SPIn must be defined!
      #endif
      #ifndef TTC_SPI1
        #error TTC_SPI5 is defined, but not TTC_SPI1 - all lower TTC_SPIn must be defined!
      #endif
    
      #define TTC_AMOUNT_SPIS 3
    #else
      #ifdef TTC_SPI2

        #ifndef TTC_SPI1
          #error TTC_SPI5 is defined, but not TTC_SPI1 - all lower TTC_SPIn must be defined!
        #endif

        #define TTC_AMOUNT_SPIS 2
      #else
        #ifdef TTC_SPI1
          #define TTC_AMOUNT_SPIS 1
        #else
          #define TTC_AMOUNT_SPIS 0
        #endif
      #endif
    #endif
  #endif
#endif

#ifndef TTC_ASSERT_SPI    // any previous definition set (Makefile)?
#define TTC_ASSERT_SPI 1  // spi asserts are enaböed by default
#endif
#if (TTC_ASSERT_SPI == 1)  // use Assert()s in spi functions (somewhat slower but alot easier to debug)
  #define Assert_SPI(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in spi functions (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_SPI(Condition, ErrorCode)
#endif

//} Defines
//{ Structures/ Enums 1 **************************************************

typedef enum { // ttc_spi_pins_e - names of pins used for SPI interface
    ttc_spi_None,
    ttc_spi_MOSI,       // Master Out Slave In
    ttc_spi_MISO,       // Master In Slave Out
    ttc_spi_SCK,        // Serial Clock
    ttc_spi_NSS         // Not Slave Select (avtive low)
} ttc_spi_pins_e;
typedef enum {   // ttc_spi_frequency_divider
  tsfd_BaudRatePrescaler_1,
  tsfd_BaudRatePrescaler_2,
  tsfd_BaudRatePrescaler_4,
  tsfd_BaudRatePrescaler_8,
  tsfd_BaudRatePrescaler_16,
  tsfd_BaudRatePrescaler_32,
  tsfd_BaudRatePrescaler_64,
  tsfd_BaudRatePrescaler_128,
  tsfd_BaudRatePrescaler_256,
  tsfd_Unknown
} ttc_spi_frequency_divider;

// Reference
// RM008 - STM32 Reference Manual p. 672 ff
// -> http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/REFERENCE_MANUAL/CD00171190.pdf

typedef struct { // Bits
  unsigned Master            : 1; // =1: enable master mode                       ; =0: slave-mode
  unsigned MultiMaster       : 1; // =1: enable multi master mode                 ; =0: single master-mode
  unsigned ClockIdleHigh     : 1; // =1: clock line is high during inactivity     ; =0: low
  unsigned ClockPhase2ndEdge : 1; // =1: data are latched on second edge of clock ; =0: first edge
  unsigned Simplex           : 1; // =1: use only two lines (MISO+MOSI on 1 line) ; =0: use three lines (separate MISO, MOSI)
  unsigned Bidirectional     : 1; // =1: bidirectional data wire                  ; =0: receive-/ transmit-only
  unsigned Receive           : 1; // =1: receive data                             ; =0: no receive
  unsigned Transmit          : 1; // =1: transmit data                            ; =0: no transmit
  unsigned WordSize16        : 1; // =1: transport 16-bits in each word           ; =0: 8-bits in each word
  unsigned HardwareNSS       : 1; // =0: NSS-pin is configured as GPIO and must be controlled by software
  unsigned CRC8              : 1; // =1: activate 8-Bit CRC calculator            ; =0: no CRC-checking
  unsigned CRC16             : 1; // =1: activate 16-Bit CRC calculator           ; =0: use 8-Bit CRC
  unsigned TxDMA             : 1; // =1: use DMA engine to load transmit data     ; =0: transmit data is loaded by software
  unsigned RxDMA             : 1; // =1: use DMA engine to store received data    ; =0: received data is stored by software
  unsigned FirstBitMSB       : 1; // =1: send Most Significant Bit first          ; =0: send Least Significant Bit first
  unsigned Irq_Error         : 1; // =1: generate interrupt when error occurs     ; =0: ignore errors
  unsigned Irq_Received      : 1; // =1: generate interrupt when data is received ; =0: ignore condition
  unsigned Irq_Transmitted   : 1; // =1: generate interrupt when data is sent     ; =0: ignore condition

  unsigned Reserved1        : 14; // pad to 32 bits
} tsg_Bits_t;

typedef struct { // architecture independent configuration data of single SPI
  union  {
    u32_t All;
    tsg_Bits_t Bits;
  } Flags;
  u32_t BaudRate;                     // transfer speed (RM0008 p.715)
  u16_t CRC_Polynom;                  // 8-/16-bit polynom used for CRC-calculation
  u8_t  Layout;                       // select pin layout to use (some uC allow pin remapping)

  ttc_spi_architecture_t* SPI_Arch;   // architecture dependend configuration data
} __attribute__((__packed__)) ttc_spi_generic_t;

//}

#endif // TTC_SPI_TYPES_H
