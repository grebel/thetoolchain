#ifndef TTC_RTLS_H
#define TTC_RTLS_H
/** { ttc_rtls.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for rtls devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_RTLS(tc_rtls_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_rtls_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_rtls_init(LogicalIndex);
 *  4) use:         ttc_rtls_XXX(LogicalIndex);
 *
 *  Device Index
 *  Devices are addressed by a logical index with no correlation to the order of physical devices.
 *  Logical indices start at 1 to simply detect uninitialized values.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level rtls and application.
 *
 *  Created from template ttc_device.h revision 36 at 20161208 10:11:47 UTC
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of ttc_rtls (Do not delete this line!)
 *
 *  ttc_rtls provides different radio network protocols for synchronous localization and
 *  mapping (SLAM). It's a junction between ttc_slam and ttc_radio.
 *
 *  ttc_rtls may operate in one of two node modes:
 *  1) Anchor Node
 *     Builds up a coordinate system among all nearby anchor nodes.
 *     The protocol being used to build up the coordinate system is implemented by the
 *     current rtls low-level driver (e.g. ttc-lib/rtls/rtls_square4).
 *  2) Mobile Node
 *     Can request current location from nearby anchor nodes.
 *
 *  ttc_rtls will receive and send data packets of a special type via configured radio.
 *  Your application will still be able to transmit and receive its own packets.
 *  Multiple instances of ttc_rtls may exist for different ttc_slam and ttc_radio instances.
 *  ttc_rtls stores only minimal node data to reduce memory usage. Most node date is
 *  stored in ttc_slam.
 *
 *
 *  How to configure and use rtls devices
 *
 *  This chapter gives an abstract description how to use ttc_rtls.
 *  Check example code for different rtls low-level drivers for details!
 *  E.g.: examples/example_ttc_rtls_square4.c
 *
 *  1) Configure Extension ttc_rtls and its low-level driver
 *     a) define TTC_RTLS<n> as one from e_ttc_rtls_architecture in makefile
 *     b) enable low-level driver rtls_*
 *        enableFeature 450_rtls_square4
 *     c) activate ttc_rtls in activate_project.sh
 *
 *  2) Configure and initialize a ttc_slam instance
 *     - See ttc_slam.h for details.
 *
 *  3) Configure and initialize a ttc_radio instance with ranging capability
 *     - See ttc_radio.h for details.
 *
 *  4) Add C-source to instantiate and configure ttc_rtls
 *     t_ttc_rtls_config* ConfigRTLS = ttc_rtls_get_configuration(<N>)
 *     - ConfigRTLS->Index_SLAM  = logical index of ttc_slam instance to use
 *     - ConfigRTLS->Index_Radio = logical index of ttc_radio instance to use
 *     - ConfigRTLS->ID_Local    = pointer to packet address of this local node (used as source address in outgoing packets)
 *     - Special initialization as required by low-level driver (see ttc-lib/rtls/rtls_*.h for detailed description. E.g. rtls_crtof_simple_2d.h)
 *
 *  4) Initialize RTLS instance for operation
 *     ttc_rtls_init(<N>)
 *
 *  5) // Add frequent calls to rtls statemachine to your main event loop
 *     while (1) { // main event loop
 *       ttc_rtls_statemachine(ConfigRTLS); // statemachine for anchor nodes
 *       ...
 *
 *  6)   // read coordinates of anchor nodes from data model
 *       if (ConfigRTLS->Flags.Bits.IsCoordinateSystemReady) {
 *         // Coordinate system has been built.
 *         // We now may read coordinates of individual anchor nodes:
 *         t_ttc_math_vector3d_xyz* Position_Node1 = ttc_slam_get_node_position(ConfigRTLS->Index_SLAM, 1);
 *         ...
 *       }
 *
 *       ...
 *       ttc_task_yield(); // give cpu to other tasks
 *     }
}*/

#ifndef EXTENSION_ttc_rtls
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_rtls.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_rtls.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "ttc_radio.h"
#include "interfaces/ttc_rtls_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level rtls only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function Declarations ************************************************{
 *
 * The functions declared below provide the main interface for
 * rtls devices on all supported architectures.
 * Check rtls/rtls_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares rtls Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_rtls_prepare();
void _driver_rtls_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_rtls_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    logical index of RTLS device. Each logical device <n> is defined via TTC_RTLS<n>* constants in compile_options.h and extensions.active/makefile
 * @return                configuration of indexed device
 */
t_ttc_rtls_config* ttc_rtls_get_configuration( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    logical index of RTLS device. Each logical device <n> is defined via TTC_RTLS<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: rtls device has been initialized successfully; != 0: error-code
 */
e_ttc_rtls_errorcode ttc_rtls_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    logical index of RTLS device. Each logical device <n> is defined via TTC_RTLS<n>* constants in compile_options.h and extensions.active/makefile
 */
void ttc_rtls_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_rtls_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of RTLS device. Each logical device <n> is defined via TTC_RTLS<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: default values have been loaded successfully for indexed rtls device; != 0: error-code
 */
e_ttc_rtls_errorcode  ttc_rtls_load_defaults( t_u8 LogicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    logical index of RTLS device. Each logical device <n> is defined via TTC_RTLS<n>* constants in compile_options.h and extensions.active/makefile
 */
void ttc_rtls_reset( t_u8 LogicalIndex );

/** Reinitializes all initialized devices after changed systemcloc profile.
 *
 * Note: This function is called automatically from _ttc_sysclock_call_update_functions()
 */
void ttc_rtls_sysclock_changed();

/** Slamradio statemachine that will run all activated sub-statemachines. Call regularly to process events.
 *
 * @param Config       (t_ttc_rtls_config*)  Configuration of rtls device as returned by ttc_rtls_get_configuration()
 */
void ttc_rtls_statemachine( t_ttc_rtls_config* Config );

/** Try to localize ourself by sending a localization request to all nearby anchor nodes
 *
 * If localization was successfull, the first calculates location is stored in *Location1.
 * If less than four valid distances to reference nodes could be determined then a second mathematical solution is stored in *Location2.
 * The second location is mirrored on the plane being spanned by three reference nodes. Often one location can simply be outruled.
 * One simple reason can be that all anchor nodes are known to be placed on the ceiling. So either Z>0 or Z<0 would place a node in the room above.
 *
 * @param LogicalIndex    logical index of RTLS device. Each logical device <n> is defined via TTC_RTLS<n>* co#nstants in compile_options.h and extensions.active/makefile
 * @param Type            type of localization to use (rcrt_Request_Localization_SSTOFCR)
 * @param Location1       where to store first possible location (calculated from averaged raw distance measures)
 * @param Location2       where to store second possible location (if not egnough distances were available for exact localization)
 * @return  ==NULL: localization was not successfull; !=NULL: pointer to gathered localization data
 */
t_ttc_rtls_localization* ttc_rtls_mobile_localize( t_u8 LogicalIndex, e_radio_common_ranging_type Type, t_ttc_math_vector3d_xyz* Location1, t_ttc_math_vector3d_xyz* Location2 );

// anchor node support is only available if at least one TTC_RTLS<n>_ANCHOR has been defined as 1
#if TTC_RTLS_IMPLEMENT_ANCHOR == 1

    /** Start building up coordinate system.
    *
    * Will send range requests to all neighbouring anchor nodes and exchange data to build up a coordinate system.
    * Low-level driver sets Config->Flags.IsCoordinateSystemReady = 1 once coordinates of all anchor nodes are complete.
    * Amount of participating anchor nodes depends on current rtls low-level driver.
    *
    * Note: Requires Config->Flags.Bits.IsAnchor=1 before ttc_rtls_init() call!
    *
    * @param Config       (t_ttc_rtls_config*)  Configuration of rtls device as returned by ttc_rtls_get_configuration()
    * @param LogicalIndex (t_u8)                     Logical index of rtls instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RTLS<n> lines in extensions.active/makefile
    * @return  ==0: buildup of coordinate system has been started; >0: error code (ec_rtls_RadioLocked, ec_rtls_NotAvailable)
    */
    e_ttc_rtls_errorcode ttc_rtls_anchor_buildup_start( t_u8 LogicalIndex );
    void _driver_rtls_anchor_buildup_start( t_ttc_rtls_config* Config );


    /** Statemachine of anchor_*() functions. Call regularly to process events.
    *
    * Note: Requires Config->Flags.Bits.IsAnchor==1 before ttc_rtls_init() call!
    *
    * @param Config       (t_ttc_rtls_config*)  Configuration of rtls device as returned by ttc_rtls_get_configuration()
    */
    void _driver_rtls_anchor_statemachine( t_ttc_rtls_config* Config );

#endif


// mobile node support is only available if at least one TTC_RTLS<n>_MOBILE has been defined as 1
#if TTC_RTLS_IMPLEMENT_MOBILE == 1

    /** Statemachine of mobile_*() functions. Call regularly to process events.
    *
    * @param Config       (t_ttc_rtls_config*)  Configuration of rtls device as returned by ttc_rtls_get_configuration()
    */
    void _driver_rtls_mobile_statemachine( t_ttc_rtls_config* Config );

#endif

/** set new network identifier for local node
 *
 * Note: Will not update Local ID in ttc_radio. You might have to do this manually!
 *
 * @param LogicalIndex (t_u8)                        Logical index of rtls instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RTLS<n> lines in extensions.active/makefile
 * @param LocalID      (const t_ttc_packet_address*) pointer to network packet identifier
 */
void ttc_rtls_set_local_id( t_u8 LogicalIndex, const t_ttc_packet_address* LocalID );

/** allocates a new rtls instance and returns pointer to its configuration
 *
 * @return (t_ttc_rtls_config*)   pointer to newly created rtls instance (->LogicalIndex stores its logical index to be used by several ttc_rtls_*() functions)
 */
t_ttc_rtls_config* ttc_rtls_create();


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_rtls_ are passed to interfaces/ttc_rtls_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl rtls UPDATE
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_rtls_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_rtls_features.
 *
 * @param Config        Configuration of rtls device
 * @return              Fields of *Config have been adjusted if necessary
 */
void _driver_rtls_configuration_check( t_ttc_rtls_config* Config );

/** shutdown single RTLS unit device
 * @param Config        Configuration of rtls device
 * @return              == 0: RTLS has been shutdown successfully; != 0: error-code
 */
e_ttc_rtls_errorcode _driver_rtls_deinit( t_ttc_rtls_config* Config );

/** initializes single RTLS unit for operation
 * @param Config        Configuration of rtls device
 * @return              == 0: RTLS has been initialized successfully; != 0: error-code
 */
e_ttc_rtls_errorcode _driver_rtls_init( t_ttc_rtls_config* Config );

/** loads configuration of indexed RTLS unit with default values
 * @param Config        Configuration of rtls device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_rtls_errorcode _driver_rtls_load_defaults( t_ttc_rtls_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of rtls device
 */
void _driver_rtls_reset( t_ttc_rtls_config* Config );
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_RTLS_H
