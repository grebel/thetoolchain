/*{ ttc_radio::.c ************************************************
 
                      The ToolChain
                      
   Device independent support for 
   
   
   written by Gregor Rebel 2012
   
 
}*/

#include "ttc_radio.h"

//{ Global variables *****************************************************

ttc_heap_array_define(ttc_radio_generic_t*, RadioConfigs, TTC_AMOUNT_RADIOS);

// stores released memory blocks for reuse as transmit or receive blocks
ttc_queue_pointers_t* ttc_radio_queue_empty_blocks = NULL;   // stores empty memory blocks for reuse

// protects queues from simultaneous access
//ttc_mutex_smart_t* ttc_radio_mutex_empty_blocks = NULL;

// Amount of radio network memory blocks allocated so far
u8_t  ttc_radio_allocated_blocks = 0;
u32_t ttc_radio_used_blocks = 0;
u32_t ttc_radio_released_blocks = 0;

// Index of radio device to use for standard output
u8_t RadioIndex_StdOut = 0;

//}Global variables
//{ Function definitions *************************************************

          inline u8_t ttc_radio_get_max_index() {

    return TTC_AMOUNT_RADIOS; // -> ttc_radio_types.h
}
 ttc_radio_generic_t* ttc_radio_get_configuration(u8_t LogicalIndex) {
             Assert_Radio(LogicalIndex > 0,                          ec_InvalidArgument); // logical index starts at 1
             Assert_Radio(LogicalIndex <= ttc_radio_get_max_index(), ec_InvalidArgument); // outside range of activated radios
             ttc_radio_generic_t* Radio_Generic = A(RadioConfigs, LogicalIndex-1);

             if (!Radio_Generic) {
                 Radio_Generic = (ttc_radio_generic_t*) ttc_heap_alloc_zeroed( sizeof(ttc_radio_generic_t) );
                 A(RadioConfigs, LogicalIndex-1) = Radio_Generic;

                 ttc_radio_load_defaults(LogicalIndex);
             }
             return Radio_Generic;
         }
ttc_radio_errorcode_e ttc_radio_load_defaults(u8_t LogicalIndex) {
    if ( (LogicalIndex > TTC_AMOUNT_RADIOS) || (LogicalIndex < 1) )
        return ec_DeviceNotFound;

    ttc_radio_generic_t* RadioGeneric = ttc_radio_get_configuration(LogicalIndex);
    memset(RadioGeneric, 0, sizeof(ttc_radio_generic_t));
    ttc_radio_errorcode_e Error = tre_NotImplemented;

    switch (ttc_radio_get_driver_type(LogicalIndex)) {
#ifdef EXTENSION_400_radio_stm32w
    case trd_stm32w:
        radio_stm32w_get_defaults(LogicalIndex, RadioGeneric);
        Error = tre_OK;
        break;
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
    case trd_cc1101_spi:
        radio_cc1101_get_defaults(LogicalIndex, RadioGeneric);
        Error = tre_OK;
        break;
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
    case trd_cc1120_spi:
        radio_cc1120_get_defaults(LogicalIndex, RadioGeneric);
        Error = tre_OK;
        break;
#endif
#ifdef EXTENSION_400_radio_serial
    case trd_serial:
        radio_serial_get_defaults(LogicalIndex, RadioGeneric);
        Error = tre_OK;
        break;
#endif
    default: break;
    }

    if (Error == tre_OK) { // low-level driver available: fill in generic configuration defaults
        RadioGeneric->LogicalIndex = LogicalIndex;
        RadioGeneric->Driver = ttc_radio_get_driver_type(LogicalIndex);
        Assert_Radio(RadioGeneric->Driver != trd_None, ec_InvalidConfiguration);

        if (!RadioGeneric->Size_QueueRx)
            RadioGeneric->Size_QueueRx = 5;
        if (!RadioGeneric->Size_QueueTx)
            RadioGeneric->Size_QueueTx = 5;

        RadioGeneric->AMP_Initialized = FALSE;
    }

    return Error;
}
ttc_radio_errorcode_e ttc_radio_get_features(u8_t LogicalIndex, ttc_radio_generic_t* Radio_Generic) {
    if ( (LogicalIndex > TTC_AMOUNT_RADIOS) || (LogicalIndex < 1) )
        return ec_DeviceNotFound;
    Assert_Radio(Radio_Generic, ec_InvalidArgument);

    memset(Radio_Generic, 0, sizeof(Radio_Generic));
    ttc_radio_errorcode_e Error = tre_NotImplemented;

    switch (ttc_radio_get_driver_type(LogicalIndex)) {
#ifdef EXTENSION_400_radio_stm32w
    case trd_cc1101_spi:
        radio_stm32w_get_features(LogicalIndex, Radio_Generic);
        Error = tre_OK;
        break;
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
    case trd_cc1101_spi:
        radio_cc1101_get_features(LogicalIndex, Radio_Generic);
        Error = tre_OK;
        break;
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
    case trd_cc1120_spi:
        radio_cc1120_get_features(LogicalIndex, Radio_Generic);
        Error = tre_OK;
        break;
#endif
#ifdef EXTENSION_400_radio_serial
    case trd_serial:
        radio_serial_get_features(LogicalIndex, Radio_Generic);
        Error = tre_OK;
        break;
#endif
    default:
        break;
    }

    if (Error == tre_OK) { // low-level driver available: fill in generic maximum values
        Radio_Generic->LogicalIndex = LogicalIndex;
        Radio_Generic->Driver = ttc_radio_get_driver_type(LogicalIndex);
        Radio_Generic->Flags.Bit.DelayedTransmits = 0;
        Radio_Generic->Flags.Bit.RxDMA            = 0;
        Radio_Generic->Flags.Bit.TxDMA            = 0;
        Radio_Generic->Flags.Bit.Duty_Cycle_Check = 1;
    }

    return Error;
}
ttc_radio_errorcode_e ttc_radio_get_max_channel(u8_t LogicalIndex, u8_t* MaxChannelTX, u8_t* MaxChannelRX) {

    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
    Assert_Radio(MaxChannelTX, ec_InvalidArgument);
    Assert_Radio(MaxChannelRX, ec_InvalidArgument);

    Assert_Radio(FALSE, ec_NotImplemented); //D Problem here with function calls no low level function avaliable for CC1101 and CC1120
    ttc_radio_errorcode_e Error = tre_NotImplemented;
#ifdef EXTENSION_400_radio_stm32w
    if (Radio_Generic->Driver == trd_stm32w)
        Error = radio_stm32w_get_max_channel(LogicalIndex, MaxChannelTX, MaxChannelRX); //ToDo: Wrong function call
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
    if (Radio_Generic->Driver == trd_cc1101_spi)
        Error = radio_cc1101_get_max_channel(LogicalIndex, MaxChannelTX, MaxChannelRX); //ToDo: Wrong function call
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
    if (Radio_Generic->Driver == trd_cc1120_spi){
        // Error = radio_cc1120_get_max_channel(LogicalIndex, MaxChannelTX, MaxChannelRX); //ToDo: Wrong function call
    }
#endif
#ifdef EXTENSION_400_radio_serial
    if (Radio_Generic->Driver == trd_serial){
        Assert_Radio(FALSE, ec_NotImplemented);
        //*MaxChannelRX = 0 ;
        //*MaxChannelTX = 0 ;
        // Error = radio_cc1120_get_max_channel(LogicalIndex, MaxChannelTX, MaxChannelRX); //ToDo: Wrong function call
    }
#endif

  return Error;
}
ttc_radio_errorcode_e ttc_radio_get_max_level(u8_t LogicalIndex, u8_t* MaxLevelTX, u8_t* MaxLevelRX) {
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
    Assert_Radio(MaxLevelTX, ec_InvalidArgument);
    Assert_Radio(MaxLevelRX, ec_InvalidArgument);

    Assert_Radio(FALSE,ec_NotImplemented); //D Problem here with function calls no low level function avaliable for CC1101 and CC1120
    ttc_radio_errorcode_e Error = tre_NotImplemented;
#ifdef EXTENSION_400_radio_stm32w
    if (Radio_Generic->Driver == trd_stm32w)
        Error = radio_stm32w_get_max_level(LogicalIndex, MaxLevelTX, MaxLevelRX); //ToDo: Wrong function call
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
    if (Radio_Generic->Driver == trd_cc1101_spi)
        Error = radio_cc1101_get_max_level(LogicalIndex, MaxLevelTX, MaxLevelRX); //ToDo: Wrong function call
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
    if (Radio_Generic->Driver == trd_cc1120_spi){
        //  Error = radio_cc1120_get_max_level(LogicalIndex, MaxLevelTX, MaxLevelRX);  //ToDo: Wrong function call
        // write function and use CC1120_MAX_TX_LEVEL (check with Datasheet if value is right!!!)
    }
#endif
#ifdef EXTENSION_400_radio_serial
    if (Radio_Generic->Driver == trd_serial){
        Assert_Radio(FALSE, ec_NotImplemented);
    }
#endif

  return Error;
}
ttc_radio_errorcode_e ttc_radio_init(u8_t LogicalIndex, u8_t DeviceAddress) {
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
    Assert_Radio(Radio_Generic, ec_InvalidArgument);
    Assert_Radio(Radio_Generic->LogicalIndex > 0, ec_InvalidArgument);
    Assert_Radio(Radio_Generic->LogicalIndex <= ttc_radio_get_max_index(), ec_InvalidArgument);

#ifdef EXTENSION_500_ttc_watchdog
    ttc_watchdog_init1(0,256);
    ttc_watchdog_reload1();
#endif
#ifdef TTC_DEBUG_RADIO
#ifdef TTC_DEBUG_RADIO_PIN1
    ttc_gpio_init(TTC_DEBUG_RADIO_PIN1,tgm_output_push_pull);
#endif
#ifdef TTC_DEBUG_RADIO_PIN2
    ttc_gpio_init(TTC_DEBUG_RADIO_PIN2,tgm_output_push_pull);
#endif
#ifdef TTC_DEBUG_RADIO_PIN3
    ttc_gpio_init(TTC_DEBUG_RADIO_PIN3,tgm_output_push_pull);
#endif

#endif

    ttc_radio_get_features(Radio_Generic->LogicalIndex, Radio_Generic);
    // check given configuration against features of configured radio
    ttc_radio_generic_t Radio_Features;
    ttc_radio_get_features(Radio_Generic->LogicalIndex, &Radio_Features);

    Assert_Radio(Radio_Generic->ChannelRx   <= Radio_Features.ChannelRx,   ec_InvalidConfiguration);
    Assert_Radio(Radio_Generic->ChannelTx   <= Radio_Features.ChannelTx,   ec_InvalidConfiguration);
    Assert_Radio(Radio_Generic->Max_Header  <= Radio_Features.Max_Header,  ec_InvalidConfiguration);
    Assert_Radio(Radio_Generic->Max_Footer  <= Radio_Features.Max_Footer,  ec_InvalidConfiguration);
    Assert_Radio(Radio_Generic->Max_Payload <= Radio_Features.Max_Payload, ec_InvalidConfiguration);
    Assert_Radio(Radio_Generic->LevelRx     <= Radio_Features.MaxLevelRx,  ec_InvalidConfiguration);
    Assert_Radio(Radio_Generic->LevelTx     <= Radio_Features.MaxLevelTx,  ec_InvalidConfiguration);
    Assert_Radio(Radio_Generic->Driver      == Radio_Features.Driver,      ec_InvalidConfiguration);

    // limit configured flags to supported features
    Radio_Generic->Flags.All &= Radio_Features.Flags.All;

    if (!Radio_Generic->Queue_Tx)
        Radio_Generic->Queue_Tx = ttc_queue_pointer_create(Radio_Generic->Size_QueueTx);

    if (!Radio_Generic->Queue_Rx)
        Radio_Generic->Queue_Rx = ttc_queue_pointer_create(Radio_Generic->Size_QueueRx);

    if (!Radio_Generic->MutexPacketReceived) {
        Radio_Generic->MutexPacketReceived = ttc_mutex_create();
        ttc_mutex_lock(Radio_Generic->MutexPacketReceived, -1, ttc_radio_init);
    }
    Radio_Generic->TaskYield = ttc_radio_task_yield;
    Radio_Generic->DeviceAddress = DeviceAddress; //set the device address used for packet filtering
    ttc_radio_errorcode_e Error = tre_NotImplemented;
    ttc_radio_set_amplifier(LogicalIndex, tram_Init);

    Error = tre_NotImplemented;
#ifdef EXTENSION_400_radio_stm32w
    if (Radio_Generic->Driver == trd_stm32w_spi)
        Error = radio_stm32w_init(Radio_Generic);
#elif EXTENSION_400_radio_cc1101_spi
    if (Radio_Generic->Driver == trd_cc1101_spi)
        Error = radio_cc1101_init(Radio_Generic);
#elif EXTENSION_400_radio_cc1120_spi
    if (Radio_Generic->Driver == trd_cc1120_spi)
        Error = radio_cc1120_init(Radio_Generic);
#elif EXTENSION_400_radio_serial
    if (Radio_Generic->Driver == trd_serial)
        Error = radio_serial_init(Radio_Generic);
#else
    Assert(0,ec_InvalidConfiguration); //no radio type selected
#endif
#if 0
    ttc_radio_duty_cycle_config(LogicalIndex, duty_cycle_limit_none, duty_cycle_Timeslot_3600_sek, ttc_radio_getDatarate(LogicalIndex));
#else
    ttc_radio_duty_cycle_config(LogicalIndex, duty_cycle_limit_none, duty_cycle_Timeslot_60_sek, ttc_radio_getDatarate(LogicalIndex));
#endif
    BOOL T_Created = FALSE;
    if (Error == tre_OK) { // radio has been initialized successfully: initialize some high-level stuff
        if (!Radio_Generic->Flags.Bit.Task_RX_Started) {
            T_Created = ttc_task_create( _ttc_radio_task_rx, "_ttc_radio_task_rx",configMINIMAL_STACK_SIZE, &LogicalIndex, 1, NULL);
            Radio_Generic->Flags.Bit.Task_RX_Started = 1;
            Assert_Radio(T_Created == TRUE, ec_Malloc);
        }
        if (!Radio_Generic->Flags.Bit.Task_TX_Started) {
            T_Created = ttc_task_create( _ttc_radio_task_tx, "_ttc_radio_task_tx", configMINIMAL_STACK_SIZE, &LogicalIndex, 1, NULL);
            Radio_Generic->Flags.Bit.Task_TX_Started = 1;
            Assert_Radio(T_Created == TRUE, ec_Malloc);
        }
        if (Radio_Generic->Flags.Bit.Duty_Cycle_Check) { //spawn task to increment the duty_cycle check
            T_Created = ttc_task_create( _task_ttc_radio_duty_cycle_timer, "_ttc_radio_task_tx", configMINIMAL_STACK_SIZE, &LogicalIndex, 1, NULL);
            Assert_Radio(T_Created == TRUE, ec_Malloc);
        }
        ttc_radio_set_operating_mode(LogicalIndex,trm_Receive);
        ttc_radio_set_power_tx(Radio_Generic->LogicalIndex, Radio_Generic->LevelTx);
    }else {
        Assert_Radio(FALSE, ec_Debug);//D
    }
#ifdef EXTENSION_500_ttc_watchdog
    ttc_watchdog_reload1();
#endif
    return Error;
}
                 void ttc_radio_send_block(u8_t LogicalIndex, ttc_heap_block_t* Block) {
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);

    if (Radio_Generic) {
        Block->Hint = Radio_Generic->TargetAddress;
        while (ttc_queue_pointer_push_back(Radio_Generic->Queue_Tx, Block) != tqe_OK)
            ttc_radio_task_yield();
    }
}
ttc_radio_errorcode_e ttc_radio_send_raw(u8_t LogicalIndex, const u8_t* Buffer, Base_t Amount) {
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);

    while (Amount > 0) {
        ttc_heap_block_t* Block = ttc_radio_get_empty_block();
        if (!Block)
            return tre_TimeOut;

        u8_t Size;

        if (Amount > Radio_Generic->Max_Payload)
            Size = Radio_Generic->Max_Payload;
        else
            Size = Amount;

        ttc_memory_copy(Block->Buffer, (u8_t*) Buffer, Size);
        Block->Size = Size;
        ttc_radio_send_block(LogicalIndex, Block);

        Buffer += Size ;
        Amount -= Size ;
    }
    return tre_OK;
}
ttc_radio_errorcode_e ttc_radio_send_string(u8_t LogicalIndex, const char* String, Base_t MaxSize) {
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);

    Base_t Bytes2Copy = MaxSize;

    while (Bytes2Copy > 0) {
        ttc_heap_block_t* Block = ttc_radio_get_empty_block();
        if (!Block)
            return tre_UnknownError;

        Block->Size = ttc_string_copy(Block->Buffer, (u8_t*) String, Radio_Generic->Max_Payload);
        if (Block->Size > 0) {
            ttc_radio_send_block(LogicalIndex, Block);

            String += Block->Size;
            if (*String == 0) // reached end of String[]
                Bytes2Copy = 0;
            else              // String[] is bigger than memory block: will continue with next block
                Bytes2Copy -= Block->Size;
        }
        else {
            ttc_heap_block_release(Block);
            Bytes2Copy = 0;
        }
    }
    return tre_OK;
}
ttc_radio_errorcode_e ttc_radio_send_string_const(u8_t LogicalIndex, const char* String, Base_t MaxSize) {
    ttc_heap_block_t* Block = ttc_radio_get_empty_block();
    if (!Block)
        return tre_TimeOut;

    Block->Buffer = (u8_t*) (char*) String;
    Block->Size   = ttc_string_length16(String, MaxSize);

    ttc_radio_send_block(LogicalIndex, Block);
    return tre_OK;
}
                 void ttc_radio_register_rx_function(u8_t LogicalIndex, ttc_heap_block_t* (*RxFunction)(struct ttc_radio_generic_s* RadioGeneric, ttc_heap_block_t* Block, u8_t RSSI, u8_t TargetAddress)) {
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);

    Radio_Generic->RxFunction_UCB = RxFunction;
}
                 void ttc_radio_register_tx_function(u8_t LogicalIndex, void (*TxFunction)(ttc_radio_generic_t* RadioGeneric, u8_t Size, const u8_t* Buffer)) {
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);

    Radio_Generic->TxFunction_UCB = TxFunction;
}
                 void ttc_radio_set_amplifier(u8_t LogicalIndex, ttc_radio_amplifier_mode_e Mode) {
      ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);

#ifdef EXTENSION_400_radio_cc1190
      radio_cc1190_set_mode(Radio_Generic, Mode); // ToDo: check if indexed radio has this amplifier!
#else

      (void) Mode;
      (void) Radio_Generic;
#endif
  }
                 void ttc_radio_set_destination(u8_t LogicalIndex, u8_t Address) {
                     ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
                     Radio_Generic->TargetAddress = Address;
                 }
ttc_radio_errorcode_e ttc_radio_set_operating_mode(u8_t LogicalIndex, ttc_radio_mode_e Mode) {
                     ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
    Radio_Generic->Mode = Mode;

#ifdef EXTENSION_400_radio_stm32w
    if (Radio_Generic->Driver == trd_stm32w)
        return radio_stm32w_set_operating_mode(Radio_Generic);
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
    if (Radio_Generic->Driver == trd_cc1101_spi)
        return radio_cc1101_set_operating_mode(Radio_Generic);
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
    if (Radio_Generic->Driver == trd_cc1120_spi)
        return radio_cc1120_set_operating_mode(Radio_Generic);
#endif
#ifdef EXTENSION_400_radio_serial
    if (Radio_Generic->Driver == trd_serial)
        //return radio_serial_set_operating_mode(Radio_Generic);
        return tre_NotImplemented;
#endif
    return tre_NotImplemented;
}
                 void ttc_radio_set_power_tx(u8_t LogicalIndex, s8_t Level) {
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);

#ifdef EXTENSION_400_radio_stm32w
    if (Radio_Generic->Driver == trd_stm32w)
        radio_stm32w_set_power_tx(Radio_Generic, Level);
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
    if (Radio_Generic->Driver == trd_cc1101_spi)
        radio_cc1101_set_power_tx(Radio_Generic, Level);
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
    if (Radio_Generic->Driver == trd_cc1120_spi)
        radio_cc1120_set_power_tx(Radio_Generic, Level);
#endif
#ifdef EXTENSION_400_radio_serial
    if (Radio_Generic->Driver == trd_serial)
        //radio_serial_set_power_tx(Radio_Generic, Level);
        Assert_Radio(FALSE, ec_NotImplemented);
#endif

}
                 void ttc_radio_set_channel_rx(u8_t LogicalIndex, u8_t ChannelIndex) {
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
    Assert_Radio(ChannelIndex > 0, ec_InvalidArgument);                             // logical index starts at 1
    Assert_Radio(ChannelIndex <= Radio_Generic->MaxChannelRx, ec_InvalidArgument);  // invalid channel index

    ttc_radio_errorcode_e Error = tre_NotImplemented;
#ifdef EXTENSION_400_radio_stm32w
    if (Radio_Generic->Driver == trd_stm32w)
        Error = radio_stm32w_set_channel(Radio_Generic, ChannelIndex); // stm32w   does not provide independent rx/tx channels;
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
    if (Radio_Generic->Driver == trd_cc1101_spi)
        Error = radio_cc1101_set_channel(Radio_Generic, ChannelIndex); // cc1101 does not provide independent rx/tx channels;
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
    if (Radio_Generic->Driver == trd_cc1120_spi)
        Error = radio_cc1120_set_channel(Radio_Generic, ChannelIndex); // cc1120 does not provide independent rx/tx channels;
#endif
#ifdef EXTENSION_400_radio_serial
    if (Radio_Generic->Driver == trd_serial)
        //Error = radio_serial_set_channel(Radio_Generic, ChannelIndex); // cc1120 does not provide independent rx/tx channels;
        Assert_Radio(FALSE, ec_NotImplemented);
#endif

    Assert_Radio(Error == tre_OK, ec_NotImplemented);
}
                 void ttc_radio_set_channel_tx(u8_t LogicalIndex, u8_t ChannelIndex) {
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
    Assert_Radio(ChannelIndex > 0, ec_InvalidArgument); // logical index starts at 1
    Assert_Radio(ChannelIndex <= Radio_Generic->MaxChannelTx, ec_InvalidArgument);  // invalid channel index

    ttc_radio_errorcode_e Error = tre_NotImplemented;
#ifdef EXTENSION_400_radio_stm32w
    if (Radio_Generic->Driver == trd_stm32w)
        Error = radio_stm32w_set_channel(Radio_Generic, ChannelIndex); // stm32w   does not provide independent rx/tx channels;
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
    if (Radio_Generic->Driver == trd_cc1101_spi)
        Error = radio_cc1101_set_channel(Radio_Generic, ChannelIndex); // cc1101 does not provide independent rx/tx channels;
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
    if (Radio_Generic->Driver == trd_cc1120_spi)
        Error = radio_cc1120_set_channel(Radio_Generic, ChannelIndex); // cc1120 does not provide independent rx/tx channels;
#endif
#ifdef EXTENSION_400_radio_serial
    if (Radio_Generic->Driver == trd_serial){
        //Error = radio_cc1120_set_channel(Radio_Generic, ChannelIndex); // cc1120 does not provide independent rx/tx channels;
        Error = tre_NotImplemented;
    }
#endif

    Assert_Radio(Error == tre_OK, ec_NotImplemented);
}
ttc_radio_errorcode_e ttc_radio_duty_cycle_config(u8_t LogicalIndex, ttc_duty_cycle_limit_e Limit, ttc_duty_cycle_Timeslot_e Timeslot, u32_t Datarate){
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
    Assert_Radio(Radio_Generic, ec_NULL);

    Radio_Generic->Duty_Cycle.Duty_Cycle_Limit  = Limit;
    Radio_Generic->Duty_Cycle.Timeslot          = Timeslot;
    Radio_Generic->Duty_Cycle.Datarate          = Datarate;//The transmitter Datarate in Bit/sek
    Radio_Generic->Duty_Cycle.Bytes_Transmitted = 0 ;      //Amount Bytes transfered within the last Timeslot
    Radio_Generic->Duty_Cycle.Time_elapsed      = 0 ;      //stores the amount of time elapsed for the current timeframe in seconds

    u32_t Ts       = 0;//timeslot

    if(Timeslot == duty_cycle_Timeslot_1_sek){
        Ts = 1;
    }else if(Timeslot == duty_cycle_Timeslot_60_sek){
        Ts = 60;
    }else if(Timeslot == duty_cycle_Timeslot_3600_sek){
        Ts = 3600;
    }else {
        Assert_Radio(FALSE, ec_InvalidConfiguration);
    }

    Radio_Generic->Duty_Cycle.MaxBytes_within_timeframe = Ts * (Datarate>>3);//devided by 8 to get bytes out of datarate

    if(Limit == duty_cycle_limit_0_0_1_percent){
        Radio_Generic->Duty_Cycle.MaxBytes_within_timeframe /= 10000;
    } else if(Limit == duty_cycle_limit_0_1_percent){
        Radio_Generic->Duty_Cycle.MaxBytes_within_timeframe /= 1000;
    }else if (Limit == duty_cycle_limit_1_percent){
        Radio_Generic->Duty_Cycle.MaxBytes_within_timeframe /= 100;
    }else if (Limit == duty_cycle_limit_10_percent){
        Radio_Generic->Duty_Cycle.MaxBytes_within_timeframe /= 10;
    }else if(Limit == duty_cycle_limit_none){
        //Radio_Generic->Duty_Cycle.MaxBytes_within_timeframe /= 1;
        //devided by 1 (no effect)
    }else{
        Assert_Radio(FALSE, ec_InvalidConfiguration);
    }

    Assert_Radio( Radio_Generic->Duty_Cycle.MaxBytes_within_timeframe > 0, ec_InvalidConfiguration);
    Radio_Generic->Duty_Cycle.Transmitted_Packets_Total = 0; //D
    return tre_OK;
}
ttc_radio_errorcode_e ttc_radio_duty_cycle_UCB_config(u8_t LogicalIndex,
                                                      void (*UCB_dc_25_used)(u8_t), //user callback - duty cycle has been 25% used
                                                      void (*UCB_dc_50_used)(u8_t), //user callback - duty cycle has been 50% used
                                                      void (*UCB_dc_75_used)(u8_t), //user callback - duty cycle has been 75% used
                                                      void (*UCB_dc_exceed)(u8_t), //user callback - duty cycle has been expired (completely used)
                                                      void (*UCB_dc_timeframe_expired)(u8_t) //user callback - duty cycle timeframe has expired / new slot is going to be started
                                                      ){
     ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
     Assert_Radio(Radio_Generic, ec_NULL);

     if(UCB_dc_25_used)
         Radio_Generic->Duty_Cycle.UCB_dc_25_used = UCB_dc_25_used;
     if(UCB_dc_50_used)
         Radio_Generic->Duty_Cycle.UCB_dc_50_used = UCB_dc_50_used;
     if(UCB_dc_75_used)
         Radio_Generic->Duty_Cycle.UCB_dc_75_used = UCB_dc_75_used;
     if(UCB_dc_exceed)
         Radio_Generic->Duty_Cycle.UCB_dc_exceed = UCB_dc_exceed;
     if(UCB_dc_timeframe_expired)
         Radio_Generic->Duty_Cycle.UCB_dc_timeframe_expired = UCB_dc_timeframe_expired;

     return tre_OK;
}
                 void _ttc_radio_duty_cycle_add_tx_bytes(u8_t LogicalIndex,u8_t Amount){
     ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
     Assert_Radio(Radio_Generic, ec_NULL);

     Radio_Generic->Duty_Cycle.Bytes_Transmitted += Amount;
      Radio_Generic->Duty_Cycle.Transmitted_Packets_Total ++;
     _ttc_radio_duty_cycle_check_limits(LogicalIndex);
     while(Radio_Generic->Duty_Cycle.Bytes_Transmitted > Radio_Generic->Duty_Cycle.MaxBytes_within_timeframe){
         ttc_task_msleep(100);
     }

}
                 u8_t _ttc_radio_duty_cycle_check_limits(u8_t LogicalIndex){
     ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
     Assert_Radio(Radio_Generic, ec_NULL);

     u8_t Ret = 0 ;
     if(Radio_Generic->Duty_Cycle.Bytes_Transmitted > (Radio_Generic->Duty_Cycle.MaxBytes_within_timeframe>>2)){
         Ret++; //25 % criteria
         if(Radio_Generic->Duty_Cycle.UCB_dc_25_used)
             Radio_Generic->Duty_Cycle.UCB_dc_25_used(LogicalIndex);
     }
     if(Radio_Generic->Duty_Cycle.Bytes_Transmitted > (Radio_Generic->Duty_Cycle.MaxBytes_within_timeframe>>1)){
         Ret++; //50 % criteria
         if(Radio_Generic->Duty_Cycle.UCB_dc_50_used)
                 Radio_Generic->Duty_Cycle.UCB_dc_50_used(LogicalIndex);
     }
     if(Radio_Generic->Duty_Cycle.Bytes_Transmitted > ((u8_t)(Radio_Generic->Duty_Cycle.MaxBytes_within_timeframe*2/3))){
         Ret++; //75 % criteria
         if(Radio_Generic->Duty_Cycle.UCB_dc_75_used)
             Radio_Generic->Duty_Cycle.UCB_dc_75_used(LogicalIndex);
     }
     if(Radio_Generic->Duty_Cycle.Bytes_Transmitted > Radio_Generic->Duty_Cycle.MaxBytes_within_timeframe){
         Ret++; //duty cycle has been expired
         if(Radio_Generic->Duty_Cycle.UCB_dc_exceed)
             Radio_Generic->Duty_Cycle.UCB_dc_exceed(LogicalIndex);
     }
     return Ret;
                 }
                 u32_t ttc_radio_duty_cycle_getFreeBytes(u8_t LogicalIndex){
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
    Assert_Radio(Radio_Generic, ec_NULL);

    if(Radio_Generic->Duty_Cycle.Bytes_Transmitted >= Radio_Generic->Duty_Cycle.MaxBytes_within_timeframe){
        return 0 ;
    }else{
        return Radio_Generic->Duty_Cycle.MaxBytes_within_timeframe - Radio_Generic->Duty_Cycle.Bytes_Transmitted; //return free bytes
    }

}
                 void _task_ttc_radio_duty_cycle_timer(void * TaskArgument){
    u8_t LogicalIndex = *((u8_t*) TaskArgument);
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
    Assert_Radio(Radio_Generic, ec_NULL);

    Radio_Generic->Duty_Cycle.Time_elapsed = 0;
    Radio_Generic->Duty_Cycle.Bytes_Transmitted = 0;
    while(1){
        ttc_task_msleep(1000);
        Radio_Generic->Duty_Cycle.Time_elapsed ++;

        if(Radio_Generic->Duty_Cycle.Timeslot == duty_cycle_Timeslot_1_sek){
            _ttc_radio_duty_cycle_timeslot_expired(LogicalIndex);
            Radio_Generic->Duty_Cycle.Time_elapsed = 0;     //1 second duty cycle timeslot
        }else if(Radio_Generic->Duty_Cycle.Timeslot == duty_cycle_Timeslot_60_sek){
            if(Radio_Generic->Duty_Cycle.Time_elapsed >= 60){ //60 seconds duty cycle timeslot
                _ttc_radio_duty_cycle_timeslot_expired(LogicalIndex);
                Radio_Generic->Duty_Cycle.Time_elapsed = 0 ;
            }
        }else if(Radio_Generic->Duty_Cycle.Timeslot == duty_cycle_Timeslot_3600_sek){
            if(Radio_Generic->Duty_Cycle.Time_elapsed >= 3600){//1 hour duty cycle timeslot
                _ttc_radio_duty_cycle_timeslot_expired(LogicalIndex);
                Radio_Generic->Duty_Cycle.Time_elapsed = 0 ;
            }
        }else{
            Radio_Generic->Duty_Cycle.Time_elapsed = 0 ;
            Assert_Radio(FALSE,ec_Debug);//duty cycle not configured but task started
        }
    }

}
ttc_radio_errorcode_e _ttc_radio_duty_cycle_timeslot_expired(u8_t LogicalIndex){
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
    Assert_Radio(Radio_Generic, ec_NULL);

    Radio_Generic->Duty_Cycle.Bytes_Transmitted = 0 ; //reset trasmitted bytes

    if(Radio_Generic->Duty_Cycle.UCB_dc_timeframe_expired)
        Radio_Generic->Duty_Cycle.UCB_dc_timeframe_expired(LogicalIndex);
    return tre_OK;
}
ttc_radio_errorcode_e ttc_radio_stdout_set(u8_t LogicalIndex) {
    ttc_radio_get_configuration(LogicalIndex); // will check if LogicalIndex is valid

    RadioIndex_StdOut = LogicalIndex;

    return tre_OK;
}
                 void ttc_radio_stdout_send_block(ttc_heap_block_t* Block) {
    if (RadioIndex_StdOut) {
        ttc_radio_send_block(RadioIndex_StdOut, Block);
    }
}
                 void ttc_radio_stdout_send_string(const char* String, Base_t MaxSize) {
    if (RadioIndex_StdOut) {
        ttc_radio_send_string(RadioIndex_StdOut, String, MaxSize);
    }
}
                 void ttc_radio_flush_tx(u8_t LogicalIndex) {
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);

    if (Radio_Generic->Flags.Bit.DelayedTransmits) { // delayed transmits have been activated for this radio

        // wait for transmit queue to run empty
        while (ttc_queue_pointer_get_amount(Radio_Generic->Queue_Tx) > 0)
            ttc_radio_task_yield();
    }
}
   ttc_radio_driver_e ttc_radio_get_driver_type(u8_t LogicalIndex) {
       switch (LogicalIndex) {
#ifdef TTC_RADIO1
       case 1: return TTC_RADIO1; // shall be defined as one from ttc_radio_types.h/ttc_radio_driver_e
#endif
#ifdef TTC_RADIO2
       case 2: return TTC_RADIO2; // shall be defined as one from ttc_radio_types.h/ttc_radio_driver_e
#endif
#ifdef TTC_RADIO3
       case 3: return TTC_RADIO3; // shall be defined as one from ttc_radio_types.h/ttc_radio_driver_e
#endif
#ifdef TTC_RADIO4
       case 4: return TTC_RADIO4; // shall be defined as one from ttc_radio_types.h/ttc_radio_driver_e
#endif
#ifdef TTC_RADIO5
       case 5: return TTC_RADIO5; // shall be defined as one from ttc_radio_types.h/ttc_radio_driver_e
#endif
       default: return trd_None;
       }
 }
  ttc_heap_block_t* ttc_radio_get_empty_block() {

    if (ttc_radio_queue_empty_blocks == NULL) { // first call: create queue
        ttc_radio_queue_empty_blocks = ttc_queue_pointer_create(TTC_RADIO_MAX_MEMORY_BUFFERS+1);
       // ttc_radio_mutex_empty_blocks = ttc_mutex_create();
       // ttc_mutex_unlock(ttc_radio_mutex_empty_blocks);
    }
    ttc_heap_block_t* Block = NULL;
    u16_t TimeOut = 1000;

    while (Block == NULL) {
        //Assert_Radio(ttc_mutex_lock(ttc_radio_mutex_empty_blocks, 10, ttc_radio_get_empty_block) == tme_OK, ec_TimeOut); // could not get lock!
        ttc_queue_error_e  Error = ttc_queue_pointer_try_pull_front(ttc_radio_queue_empty_blocks, (void**) &Block);

        if (Error != tqe_OK) { // no released memory block available: alloc new block
            if (ttc_radio_allocated_blocks < TTC_RADIO_MAX_MEMORY_BUFFERS) {
                Block = ttc_heap_alloc_block(TTC_NETWORK_MAX_PAYLOAD_SIZE,
                                               0, // will be used for target address
                                               _ttc_radio_release_block
                                               );
                ttc_radio_allocated_blocks++;
            }
            else{
                Block = NULL;
                Assert_Radio(FALSE, ec_Debug);//D can not get another empty block
            }
        }
        else { // reuse released memory block
            // Buffer-pointer might have been changed
            Assert_Radio(Block != NULL, ec_IllegalPointer); //D
            Block->Buffer = (u8_t*) (  ( (u32_t) Block ) + sizeof(ttc_heap_block_t)  );
            Block->Size = 0;
            Block->UseCount = 1;
        }

        //ttc_mutex_unlock(ttc_radio_mutex_empty_blocks);

        if (Block == NULL) { // no empty block available: wait some time and try again
            if(1) Assert(TimeOut-- > 0, ec_UNKNOWN); // cannot get empty memory block!
            //not possibele to assert here because if new buffers to be send over the radio are generated faster than the radio is able to transmitt them,
            //this timeout will be decreased causing this function to assert.
            //Hence it is not possible to wait until the ttc_radio_queue_empty_blocks has new free buffers
            ttc_task_yield();
        }
    }
    ttc_radio_used_blocks++;

    return Block;
}
                u32_t ttc_radio_getDatarate(u8_t LogicalIndex){
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
    Assert_Radio(Radio_Generic, ec_NULL);
#ifdef EXTENSION_400_radio_stm32w
    if (Radio_Generic->Driver == trd_stm32w)
        return radio_stm32w_getDatarate(Radio_Generic);
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
    if (Radio_Generic->Driver == trd_cc1101_spi)
        return radio_cc1101_getDatarate(Radio_Generic);
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
    if (Radio_Generic->Driver == trd_cc1120_spi)
        return radio_cc1120_getDatarate(Radio_Generic);
#endif
#ifdef EXTENSION_400_radio_serial
    if (Radio_Generic->Driver == trd_serial)
        //return radio_cc1120_getDatarate(Radio_Generic);
        Assert_Radio(FALSE, ec_NotImplemented);
#endif
    Assert_Radio(FALSE, ec_NotImplemented);
    return 0;
}
              u8_t ttc_radio_getPacketHeaderSize(u8_t LogicalIndex){
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
    Assert_Radio(Radio_Generic, ec_NULL);
#ifdef EXTENSION_400_radio_stm32w
    if (Radio_Generic->Driver == trd_stm32w)
        return radio_stm32w_getPacketHeaderSize(Radio_Generic);
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
    if (Radio_Generic->Driver == trd_cc1101_spi)
        return radio_cc1101_getPacketHeaderSize(Radio_Generic);
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
    if (Radio_Generic->Driver == trd_cc1120_spi)
        return radio_cc1120_getPacketHeaderSize(Radio_Generic);
#endif
#ifdef EXTENSION_400_radio_serial
    if (Radio_Generic->Driver == trd_serial){
        //return radio_cc1120_getPacketHeaderSize(Radio_Generic);
        Assert_Radio(FALSE, ec_NotImplemented);
    }
#endif
    Assert_Radio(FALSE, ec_NotImplemented);
    return 0;
}
              void ttc_radio_task_yield() {
   ttc_task_yield();

}
                 u8_t _ttc_radio_check_bytes_received(ttc_radio_generic_t* Radio_Generic) {
#ifdef EXTENSION_400_radio_stm32w
    if (Radio_Generic->Driver == trd_stm32w)
        return radio_stm32w_spi_register_single_read( (radio_stm32w_driver_t*) Radio_Generic->DriverCfg, rcr_RXBYTES);
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
    if (Radio_Generic->Driver == trd_cc1101_spi)
        return radio_cc1101_spi_register_single_read( (radio_cc1101_driver_t*) Radio_Generic->DriverCfg, rcr_RXBYTES);
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
    if (Radio_Generic->Driver == trd_cc1120_spi)
        return radio_cc1120_spi_register_read((radio_cc1120_driver_t*) Radio_Generic->DriverCfg, rcr_CC112X_FIFO_NUM_RXBYTES);
#endif
#ifdef EXTENSION_400_radio_serial
    if (Radio_Generic->Driver == trd_serial){
        //return radio_cc1120_spi_register_read((radio_cc1120_driver_t*) Radio_Generic->DriverCfg, rcr_CC112X_FIFO_NUM_RXBYTES);
        Assert_Radio(FALSE, ec_NotImplemented);
    }
#endif
    return 0;
}
                 BOOL _ttc_radio_check_mode(ttc_radio_generic_t* Radio_Generic) {

    BOOL IllegalMode = FALSE;
#ifdef EXTENSION_400_radio_stm32w
    if (Radio_Generic->Driver == trd_stm32w){
        IllegalMode = radio_stm32w_check_mode(Radio_Generic);
    }else {
        Assert_Radio(FALSE, ec_NotImplemented);
    }
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
    if (Radio_Generic->Driver == trd_cc1101_spi){
        IllegalMode = radio_cc1101_check_mode(Radio_Generic);
    }else {
        Assert_Radio(FALSE, ec_NotImplemented);
    }
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
    if (Radio_Generic->Driver == trd_cc1120_spi){
        IllegalMode = radio_cc1120_check_mode(Radio_Generic);
    }else {
        Assert_Radio(FALSE, ec_NotImplemented);
    }
#endif
#ifdef EXTENSION_400_radio_serial
    if (Radio_Generic->Driver == trd_serial){
        //IllegalMode = radio_cc1120_check_mode(Radio_Generic);
        Assert_Radio(FALSE, ec_NotImplemented);
    }else {
        Assert_Radio(FALSE, ec_NotImplemented);
    }
#endif
    return IllegalMode;
}
                 void _ttc_radio_release_block(ttc_heap_block_t* Block) {
    Assert(Block != NULL, ec_NULL);

    //ttc_mutex_lock(ttc_radio_mutex_empty_blocks, -1, _ttc_radio_release_block);

    ttc_queue_error_e Error = tqe_UNKNOWN;

    Error = ttc_queue_pointer_push_back(ttc_radio_queue_empty_blocks, Block);
    Assert_Radio(Error == tqe_OK, ec_UNKNOWN); // could not release memory block (maybe queue is too small?)

    //ttc_mutex_unlock(ttc_radio_mutex_empty_blocks);

    ttc_radio_released_blocks++;
}
                 void _ttc_radio_task_rx(void* Argument) {
    //ttc_radio_generic_t* Radio_Generic = (ttc_radio_generic_t*) Argument;
    u8_t LogicalIndex = *((u8_t*) Argument);
    Assert_Radio(LogicalIndex, ec_InvalidArgument);
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
    Assert_Radio(Radio_Generic, ec_NULL);
    ttc_heap_block_t* Block = NULL;
    ttc_radio_errorcode_e Error = tre_OK;

    while (1) {
#ifdef EXTENSION_500_ttc_watchdog
        ttc_watchdog_reload1();
#endif
            // wait for interrupt service routine to unlock rx-mutex
            if (ttc_mutex_lock(Radio_Generic->MutexPacketReceived, 200000, _ttc_radio_task_rx) != tme_OK) {
#if TTC_DEBUG_RADIO==1
                if (1) ttc_gpio_set(TTC_DEBUG_RADIO_PIN3); //D
#endif
                Radio_Generic->Mode = trm_Receive;//make sure we are checking for receive mode
                if (_ttc_radio_check_mode(Radio_Generic) ) { // did not receive anything for a while: check radio
                    Error = ttc_radio_set_operating_mode(Radio_Generic->LogicalIndex, Radio_Generic->Mode); // radio in unexpected mode: try to correct
                    Assert_Radio(Error == tre_OK, ec_TimeOut);// could not fix unexpected radio mode


                }
#if TTC_DEBUG_RADIO==1
                if (1) ttc_gpio_clr(TTC_DEBUG_RADIO_PIN3);
#endif
            }
            else {
#if TTC_DEBUG_RADIO==1
                ttc_gpio_set(TTC_DEBUG_RADIO_PIN1); //D
#endif
                do {
                    if (!Block) {
                        Block = ttc_radio_get_empty_block();
                        Assert_Radio(Block, ec_NULL);
                    }
                    Block = _ttc_radio_read_packet(Radio_Generic, Block);
                } while (_ttc_radio_check_bytes_received(Radio_Generic));
                if (1) ttc_task_check_stack();
#if TTC_DEBUG_RADIO==1
                ttc_gpio_clr(TTC_DEBUG_RADIO_PIN1);
#endif
            }

    }
}
                 void _ttc_radio_task_tx(void* Argument) {
    u8_t LogicalIndex = *((u8_t*) Argument);
    Assert_Radio(LogicalIndex, ec_InvalidArgument);
    ttc_radio_generic_t* Radio_Generic = ttc_radio_get_configuration(LogicalIndex);
    Assert_Radio(Radio_Generic, ec_NULL);

    ttc_queue_pointers_t* Queue_Tx = Radio_Generic->Queue_Tx;
    ttc_heap_block_t*        Block;
    Base_t MaxPayloadSize = Radio_Generic->Max_Payload;
    Base_t BytesRemaining;
    u16_t  Bytes2Send;
    u8_t*  Reader;

    while (1) {
        while (ttc_queue_pointer_pull_front(Queue_Tx, (void**) &Block) == tqe_OK) {
            Reader = Block->Buffer;
            BytesRemaining = Block->Size;
#if TTC_DEBUG_RADIO==1
                if (1) ttc_gpio_set(TTC_DEBUG_RADIO_PIN2); //D
#endif
            while (BytesRemaining > 0) { // send out packets until complete block has been transmitted
                if (BytesRemaining > MaxPayloadSize)
                    Bytes2Send = MaxPayloadSize;
                else
                    Bytes2Send = BytesRemaining;

                //add bytes for duty cycle calculation
                //NOTE: This function will block until it is allowed to send more bytes in case the duty cycle is exeeded for the current timeframe.
                _ttc_radio_duty_cycle_add_tx_bytes(LogicalIndex, Bytes2Send + ttc_radio_getPacketHeaderSize(LogicalIndex));

                if (Radio_Generic->TxFunction_UCB)
                    Radio_Generic->TxFunction_UCB(Radio_Generic, Bytes2Send, Reader);

                ttc_radio_errorcode_e Error = tre_OK;
                ttc_radio_set_amplifier(Radio_Generic->LogicalIndex, tram_TransmitHighGain);
                switch (Radio_Generic->Driver) { // transmit single packet

#ifdef EXTENSION_400_radio_stm32w
                case trd_stm32w: {
                    Error = radio_stm32w_packet_send(Radio_Generic, Block->Hint, Bytes2Send, Reader);
                    break;
                }
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
                case trd_cc1101_spi: {
                    Error = radio_cc1101_packet_send(Radio_Generic, Block->Hint, Bytes2Send, Reader);
                    break;
                }
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
                case trd_cc1120_spi: {
                    Error = radio_cc1120_packet_send(Radio_Generic, Block->Hint, Bytes2Send, Reader);
                    break;
                }
#endif
#ifdef EXTENSION_400_radio_serial
                case trd_serial: {
                    //Error = radio_cc1120_packet_send(Radio_Generic, Block->Hint, Bytes2Send, Reader);
                    Assert_Radio(FALSE, ec_NotImplemented);
                    break;
                }
#endif
                default:
                    ttc_task_msleep(200); // Simulate transmission
                }
                if (Error != tre_OK) { // handle error condition
                    //Assert_Radio(FALSE, ec_UNKNOWN); // ToDo: proper generic error handling
                    if(Error == tre_TX_Packet_Not_Send){
                        Error = radio_cc1120_packet_send(Radio_Generic, Block->Hint, Bytes2Send, Reader);
                        Assert_Radio(Error == tre_OK, ec_UNKNOWN); //resend not possible, major radio problem.
                    }else if(Error == tre_Radio_Reset_Request){
                        Assert_Radio(FALSE, ec_Debug); // ToDo: proper error handling with radio Reset
                    }else{
                        Assert_Radio(FALSE, ec_Debug); // another Error has occured

                    }

                }
               

                BytesRemaining -= Bytes2Send;
                Reader += Bytes2Send;
            }
            ttc_radio_set_amplifier(Radio_Generic->LogicalIndex, tram_ReceiveHighGain);

            // send block back to its originator (mostly _ttc_radio_release_block())
            //ttc_heap_block_use(Block);
            // Block->UseCount = 1; //D
            ttc_heap_block_release(Block);

#if TTC_DEBUG_RADIO==1
            if (1) ttc_gpio_clr(TTC_DEBUG_RADIO_PIN2); //D
#endif
        }
        ttc_radio_task_yield();
        ttc_task_check_stack();
    }
}
  ttc_heap_block_t* _ttc_radio_read_packet(ttc_radio_generic_t* Radio_Generic, ttc_heap_block_t* Block) {
    Assert_Radio(Radio_Generic, ec_NULL);
    Assert_Radio(Block, ec_NULL);

    u8_t RSSI = 0;
    //ToDo: add to Radio_Generic->Metainformation.RSSI
#ifdef EXTENSION_400_radio_stm32w
    if (Radio_Generic->Driver == trd_stm32w) {
        Block->Size = radio_stm32w_packet_receive(Radio_Generic, Block->Buffer, Block->MaxSize, &(Block->Hint), &RSSI);
        Assert_Radio(Block->Size < TTC_NETWORK_MAX_PAYLOAD_SIZE, ec_InvalidArgument); // got invalid block size (maybe corrupted data?)
    }
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
    if (Radio_Generic->Driver == trd_cc1101_spi) {
        Block->Size = radio_cc1101_packet_receive(Radio_Generic, Block->Buffer, Block->MaxSize, &(Block->Hint), &RSSI);
        Assert_Radio(Block->Size < TTC_NETWORK_MAX_PAYLOAD_SIZE, ec_InvalidArgument); // got invalid block size (maybe corrupted data?)
    }
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
    if (Radio_Generic->Driver == trd_cc1120_spi) {
        Block->Size = radio_cc1120_packet_receive(Radio_Generic, Block->Buffer, Block->MaxSize, &(Block->Hint), &RSSI);
        Assert_Radio(Block->Size < TTC_NETWORK_MAX_PAYLOAD_SIZE, ec_InvalidArgument); // got invalid block size (maybe corrupted data?)
    }
#endif
#ifdef EXTENSION_400_radio_serial
    if (Radio_Generic->Driver == trd_serial) {
        //Block->Size = radio_cc1120_packet_receive(Radio_Generic, Block->Buffer, Block->MaxSize, &(Block->Hint), &RSSI);
        //Assert_Radio(Block->Size < TTC_NETWORK_MAX_PAYLOAD_SIZE, ec_InvalidArgument); // got invalid block size (maybe corrupted data?)
        Assert_Radio(FALSE, ec_NotImplemented);
    }
#endif
    if (Radio_Generic->RxFunction_UCB) {
        Block = Radio_Generic->RxFunction_UCB(Radio_Generic, Block, RSSI, Block->Hint);
    }

    return Block;
}

//} Function definitions
