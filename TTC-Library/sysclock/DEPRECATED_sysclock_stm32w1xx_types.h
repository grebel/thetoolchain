#ifndef SYSCLOCK_STM32W1XX_TYPES_H
#define SYSCLOCK_STM32W1XX_TYPES_H

/** { sysclock_stm32w1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *                     
 *  Low-Level datatypes for SYSCLOCK devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by ttc_sysclock_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140222 19:41:29 UTC
 *
 *  Note: See ttc_sysclock.h for description of architecture independent SYSCLOCK implementation.
 * 
 *  Authors: <AUTHOR>
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_sysclock_types.h *************************

typedef struct { // register description (adapt according to stm32w1xx registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} SYSCLOCK_Register_t;

typedef struct {  // stm32w1xx specific configuration data of single SYSCLOCK device
  SYSCLOCK_Register_t* BaseRegister;       // base address of SYSCLOCK device registers
} __attribute__((__packed__)) sysclock_stm32w1xx_config_t;

// ttc_sysclock_architecture_t is required by ttc_sysclock_types.h
#define ttc_sysclock_architecture_t sysclock_stm32w1xx_config_t

//} Structures/ Enums


#endif //SYSCLOCK_STM32W1XX_TYPES_H
