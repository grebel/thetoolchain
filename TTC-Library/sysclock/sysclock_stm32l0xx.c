/** { sysclock_stm32l0xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for sysclock devices on stm32l0xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150303 12:19:32 UTC
 *
 *  Note: See ttc_sysclock.h for description of stm32l0xx independent SYSCLOCK implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "sysclock_stm32l0xx.h".
//
#include "sysclock_stm32l0xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

void sysclock_stm32l0xx_prepare(t_ttc_sysclock_config* Config) {
    Assert_SYSCLOCK(Config, ttc_assert_origin_auto); // pointers must not be NULL

    SystemInit(); // init STM32 standard peripherals library
    sysclock_stm32l0xx_reset(Config);
    
}
void sysclock_stm32l0xx_profile_switch(t_ttc_sysclock_config* Config, e_ttc_sysclock_profile NewProfile) {
    Assert_SYSCLOCK(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function sysclock_stm32l0xx_profile_switch() is empty.
    

}
void sysclock_stm32l0xx_udelay(t_ttc_sysclock_config* Config, t_base Microseconds) {
    Assert_SYSCLOCK(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function sysclock_stm32l0xx_udelay() is empty.
    

}
void sysclock_stm32l0xx_update(t_ttc_sysclock_config* Config) {
    Assert_SYSCLOCK(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function sysclock_stm32l0xx_update() is empty.
    

}
void sysclock_stm32l0xx_reset(t_ttc_sysclock_config* Config) {
    Assert_SYSCLOCK(Config, ttc_assert_origin_auto); // pointers must not be NULL

    Config->Architecture                = E_ttc_sysclock_architecture_stm32l0xx;
    Config->LowLevelConfig.BaseRegister = RCC;
    Config->SystemClockFrequency        = 0;
    Config->Profile                     = E_ttc_sysclock_profile_None;

}
e_ttc_sysclock_errorcode sysclock_stm32l0xx_enable_oscillator(e_ttc_sysclock_oscillator Oscillator, BOOL On) {


#warning Function sysclock_stm32l0xx_enable_oscillator() is empty.
    
    return (e_ttc_sysclock_errorcode) 0;
}
const t_base* sysclock_stm32l0xx_frequency_get_all(t_ttc_sysclock_config* Config) {
    Assert_SYSCLOCK_EXTRA_Writable(Config, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function sysclock_stm32l0xx_frequency_get_all() is empty.
    
    return (const t_base*) 0;
}
void sysclock_stm32l0xx_frequency_set(t_ttc_sysclock_config* Config, t_base NewFrequency) {
    Assert_SYSCLOCK_EXTRA_Writable(Config, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function sysclock_stm32l0xx_frequency_set() is empty.
    

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************


//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions