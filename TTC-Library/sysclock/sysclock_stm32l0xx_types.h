#ifndef SYSCLOCK_STM32L0XX_TYPES_H
#define SYSCLOCK_STM32L0XX_TYPES_H

/** { sysclock_stm32l0xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for SYSCLOCK devices on stm32l0xx architectures.
 *  Structures, Enums and Defines being required by ttc_sysclock_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150303 12:19:32 UTC
 *
 *  Note: See ttc_sysclock.h for description of architecture independent SYSCLOCK implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_SYSCLOCK1   // device not defined in makefile
    #define TTC_SYSCLOCK1    E_ttc_sysclock_architecture_stm32l0xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_sysclock_types.h *************************

typedef struct { // register description (adapt according to stm32l0xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_sysclock_register;

typedef struct {  // stm32l0xx specific configuration data of single SYSCLOCK device
    t_sysclock_register* BaseRegister;       // base address of SYSCLOCK device registers
} __attribute__( ( __packed__ ) ) t_sysclock_stm32l0xx_config;

// t_ttc_sysclock_architecture is required by ttc_sysclock_types.h
#define t_ttc_sysclock_architecture t_sysclock_stm32l0xx_config

//} Structures/ Enums


#endif //SYSCLOCK_STM32L0XX_TYPES_H
