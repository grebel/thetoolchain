/** { sysclock_stm32w1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for sysclock devices on stm32w1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20141106 14:10:34 UTC
 *
 *  Note: See ttc_sysclock.h for description of stm32w1xx independent SYSCLOCK implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "sysclock_stm32w1xx.h".
//
#include "sysclock_stm32w1xx.h"
#include "micro-common.h"
#include "phy-library.h"
#include "mfg-token.h"
#include "../ttc_interrupt.h"
#include "../register/register_stm32w1xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

void sysclock_stm32w1xx_prepare(t_ttc_sysclock_config* Config) {
    Assert_SYSCLOCK(Config, ttc_assert_origin_auto); // pointers must not be NULL

    halInit(); // init EM357 hardware abstraction library

    if (1) { ttc_interrupt_all_enable(); }
    else   { INTERRUPTS_ON(); }

    sysclock_stm32w1xx_reset(Config);
}
void sysclock_stm32w1xx_profile_switch(t_ttc_sysclock_config* Config, e_ttc_sysclock_profile NewProfile) {
    Assert_SYSCLOCK(Config, ttc_assert_origin_auto); // pointers must not be NULL
    (void) NewProfile;

#warning Function sysclock_stm32w1xx_profile_switch() is empty.
    

}
void sysclock_stm32w1xx_reset(t_ttc_sysclock_config* Config) {
    Assert_SYSCLOCK(Config, ttc_assert_origin_auto); // pointers must not be NULL

    Assert_SYSCLOCK(Config, ttc_assert_origin_auto);
    Config->Architecture                = E_ttc_sysclock_architecture_stm32w1xx;
    Config->LowLevelConfig.BaseRegister = NULL; // ToDo: implement
    Config->SystemClockFrequency        = 0;
    Config->Profile                     = E_ttc_sysclock_profile_None;
}
void sysclock_stm32w1xx_udelay(t_ttc_sysclock_config* Config, t_base Microseconds) {
    Assert_SYSCLOCK(Config, ttc_assert_origin_auto); // pointers must not be NULL

    // Hand tuned delay calibrated on STM32W108 @ 24MHz
    // minimum delay: 9 us for Microseconds = 0
    // minimum usable delay: 20 us
    volatile t_base_signed Remaining = (Microseconds - 9 ) * 100000 / 213864;

    // adapt to current system clock frequency (simple linear approach)
    if (Config->SystemClockFrequency)
        Remaining *= 72000000 / Config->SystemClockFrequency;

    for (; Remaining > 0; Remaining--)
        Remaining = Remaining;
}
void sysclock_stm32w1xx_update(t_ttc_sysclock_config* Config) {
    Assert_SYSCLOCK(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function sysclock_stm32w1xx_update() is empty.
    

}
const t_base* sysclock_stm32w1xx_frequency_get_all(t_ttc_sysclock_config* Config) {
    Assert_SYSCLOCK_EXTRA_Writable(Config, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function sysclock_stm32w1xx_frequency_get_all() is empty.
    
    return (const t_base*) 0;
}
void sysclock_stm32w1xx_frequency_set(t_ttc_sysclock_config* Config, t_base NewFrequency) {
    Assert_SYSCLOCK_EXTRA_Writable(Config, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function sysclock_stm32w1xx_frequency_set() is empty.
    

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions