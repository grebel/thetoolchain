/** { sysclock_stm32f1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for sysclock devices on stm32f1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 20 at 20140218 17:32:51 UTC
 *
 *  Note: See ttc_sysclock.h for description of stm32f1xx independent SYSCLOCK implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "sysclock_stm32f1xx.h"
#include "../ttc_basic.h"

extern t_u32 SystemCoreClock;          /*!< System Clock Frequency (Core Clock) */

/** read PLL settings from registers
 *
 * stm32f107 connectivity line devices provide two sets of multiplier and divider (PLL_Index = 1..2).
 * All other stm32f1xx devices provide only one set (PLL_Index = 1)
 */
void _sysclock_stm32f1xx_pll_read( t_u8 PLL_Index, t_u8* Multiplier, t_u8* Divider );

const t_u32 sysclock_stm32f1xx_MaxClock_ADC    = 14000000;
const t_u32 sysclock_stm32f1xx_MaxClock_SYSCLK = 72000000;
const t_u32 sysclock_stm32f1xx_MaxClock_AHB    = 72000000;
const t_u32 sysclock_stm32f1xx_MaxClock_APB1   = 36000000;
const t_u32 sysclock_stm32f1xx_MaxClock_APB2   = 72000000;

#ifdef STM32F10X_CL  //  Connectivity line devices: reset and clock control (-> RM0008 p.132)
const t_u8 PLL1_Multiplier_x2_Min = 2 * 4; // using 2x Multiplier to allow integer math for factor 6.5
const t_u8 PLL1_Multiplier_x2_Max = 2 * 9; //

const t_u8 PLL2_Divider_Min    = 1;
const t_u8 PLL2_Divider_Max    = 16;
const t_u8 PLL2_Multiplier_Min = 8;
const t_u8 PLL2_Multiplier_Max = 16;

const t_u8  PLL1_Divider_Min   = 1;
const t_u8  PLL1_Divider_Max   = 16;

// Note: It is not possible to define all available frequencies in a generic way.
//       This list ist just an example, define your own frequency table as described in
//       ttc_sysclock.c:ttc_sysclock_frequency_get_all()!
const t_u32 sysclock_stm32f1xx_AvailableFrequencies[] = {
#if TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0 // HSE on Connectivity Line
    TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL / 4,
    TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL / 2,
    TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL / 1,
    72000000,
#else                                           // HSI on Connectivity Line
    8000000, 16000000, 20000000, 24000000, 26000000, 28000000, 32000000, 36000000,
#endif
    0 // array must be zero terminated!
};
#else                //  Low-, medium-, high- and XL-density reset and clock control (-> RM0008 p.99)
const t_u8 PLL1_Multiplier_x2_Min = 2 * 2;
const t_u8 PLL1_Multiplier_x2_Max = 2 * 16;

#define Frequency1 (TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL * 1)

#if TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0 // HSE on NON Connectivity Line

const t_u32 sysclock_stm32f1xx_AvailableFrequencies[] = {
    TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL * 1,
    TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL * 2,
    TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL * 3,
    TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL * 4,
    72000000,
    0 // array must be zero terminated!
};
#else                                           // HSI on NON Connectivity Line
const t_u32 sysclock_stm32f1xx_AvailableFrequencies[] = {
    8000000, 12000000, 16000000, 20000000, 24000000, 28000000, 32000000, 36000000,
    40000000, 44000000, 48000000, 52000000, 56000000, 60000000, 64000000,
    0 // array must be zero terminated!
};
#endif

#endif

#define AMOUNT_AHB_Dividers 9
const t_u16 AHB_Dividers[AMOUNT_AHB_Dividers] = { 1, 2, 4, 8, 16, 64, 128, 256, 512 }; // CFGR.HPRE values 0b1000..0b1111

typedef enum { // USB divider value
    ssup_None = 0,
    ssup_USB_DivideBy_1,        // USB clock = PLL Clock
    ssup_USB_DivideBy_1_5,      // USB clock = PLL Clock / 1.5
    ssup_USB_DivideBy_unknown
} ssup_Divider;

//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _sysclock_stm32f1xx_foo(t_ttc_sysclock_config* Config)

/** Setup system clocks for maximum speed.
 *
 * The correct settings depend on current hardware configuration.
 * Therefore this function is usually only called from low-level board
 * initialization functions.
 *
 * @param   Config         Referenced configuration will be changed.
 * @param   Source         clock source to use
 * @param   NewFrequency   desired new clock frequency
 * @return  new system clock frequency will be written into Config->SystemClockFrequency
 */
void _sysclock_stm32f1xx_set_clock_frequency( t_ttc_sysclock_config* Config, e_stm32_clock_source Source, t_u32 NewFrequency );

/** Configure High Speed External clock prescaler (PLLXTPRE) on non Connectivity Line devices to given value
 *
 */
void _sysclock_stm32f1xx_prescaler_hse( t_ttc_sysclock_config* Config, ssup_Divider Divider );

typedef enum { // e_sysclock_stm32f1xx_sourcepll - input of multiplexer PLLSRC
    sssp_None,
    sssp_SYSCLK_8MHz,                // f(PLLCLK) = 8 MHz HSI                                     (all variants)
    sssp_SYSCLK_PLL1_HSI,            // f(PLLCLK) = 8 MHz HSI / 2 * PLLMUL                        (all variants)
    sssp_SYSCLK_HSE,                 // f(PLLCLK) = f(HSE)                                        (all variants)

#ifdef STM32F10X_CL              // Connectivity Line devices have one additional set of prescaler
    sssp_SYSCLK_PLL1_HSE,            // f(PLLCLK) = f(HSE) / PLLXTPRE * PREDIV1 * PLLMUL                     (Connectivity Line)
    sssp_SYSCLK_PLL1_PLL2_HSE,       // f(PLLCLK) = f(HSE) / PREDIV2 * PLL2MUL / PREDIV1 * PLLMUL (Connectivity Line)
#else
    sssp_SYSCLK_PLLMUL_HSE,          // f(PLLCLK) = f(HSE) / PLLXTPRE * PLLMUL                    (non Connectivity Line)
#endif
    sssp_unknown

} e_sysclock_stm32f1xx_sourcepll;

/** Configure multiplexer input PLLSRC
 *
 */
void _sysclock_stm32f1xx_switch_pllsrc( e_sysclock_stm32f1xx_sourcepll SourcePLL );

/** Configure USB prescaler to given value
 *
 * If USB interface is enabled, it will be disabled and reenabled during
 * prescaler reconfiguration.
 *
 */
void _sysclock_stm32f1xx_prescaler_usb( ssup_Divider Divider );

/** Configure prescaler of AHB bus.
 *
 * The correct settings depend on current hardware configuration.
 * Therefore this function is usually only called from low-level board
 * initialization functions.
 *
 * @param   Divider    divide clock source by this factor
 */
void _sysclock_stm32f1xx_prescaler_ahb( t_u16 Divider );

/** Automatically configure prescaler of APB1 bus for max speed < 36MHz.
 *
 * Note: Will call _sysclock_stm32f1xx_update() to update clock tree frequencies ->ssu_AHBCLK
 *
 * @param   FrequencyAHB  frequency of AHB for which APB1 prescaler should be calculated
 */
void _sysclock_stm32f1xx_prescaler_apb1( t_u32 FrequencyAHB );

/** Configure prescaler of APB2 bus (max 72MHz).
 *
 * The correct settings depend on current hardware configuration.
 * Therefore this function is usually only called from low-level board
 * initialization functions.
 *
 * @param   Divider divide clock source by this factor
 */
void _sysclock_stm32f1xx_prescaler_apb2( t_u8 Divider );

/** Automatically configure prescaler of ADCCLK line to max frequency <= 14MHz.
 *
 * @param   FrequencyAPB2  frequency of APB2 for which ADC prescaler should be calculated
 */
void _sysclock_stm32f1xx_prescaler_adc( t_u32 FrequencyAPB2 );

/** Updates frequencies of clock tree levels from current register settings.
 *
 * Updated clock tree frequencies can be restricted to first levels to reduce runtime or during
 * a clock tree reconfiguration to avoid asserts.
 *
 * @param Config               Config->LowLevelConfig will be updated with calculated frequencies
 * @param MaxStage  ==ssu_ALL: update complete clock tree; up to given stage otherwise
 */
void _sysclock_stm32f1xx_update( t_ttc_sysclock_config* Config, ssu_MaxStage MaxStage );

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)
//{ Function definitions *******************************************************

e_ttc_sysclock_errorcode sysclock_stm32f1xx_deinit( t_ttc_sysclock_config* Config ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_sysclock_errorcode ) 0;
}
e_ttc_sysclock_errorcode sysclock_stm32f1xx_get_features( t_ttc_sysclock_config* Config ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_sysclock_errorcode ) 0;
}
e_ttc_sysclock_errorcode sysclock_stm32f1xx_init( t_ttc_sysclock_config* Config ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL


    return ( e_ttc_sysclock_errorcode ) 0;
}
void                     sysclock_stm32f1xx_prepare( t_ttc_sysclock_config* Config ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );

    SystemInit(); // init STM32 standard peripherals library (ToDo: required anymore?)
    sysclock_stm32f1xx_reset( Config );
}
void                     sysclock_stm32f1xx_reset( t_ttc_sysclock_config* Config ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );
    Config->Architecture                = E_ttc_sysclock_architecture_stm32f1xx;
    Config->LowLevelConfig.BaseRegister = &register_stm32f1xx_RCC;
    Config->SystemClockFrequency        = 0;
    Config->Profile                     = E_ttc_sysclock_profile_None;

    // this driver can consider USB operation during clock configuration (enabling USB reduces amount of available clock frequencies)
#ifdef STM32F10X_CL
#if TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0
    Config->Flags.Bits.ConsiderUSB = 1; // 48MHz USB clock available on Connective Line with HSE
#else
    Config->Flags.Bits.ConsiderUSB = 0; // 48MHz USB clock not available on Connective Line without HSE (SYSCLK max 36MHz)
#endif
#else
    Config->Flags.Bits.ConsiderUSB = 1; // 48MHz USB clock available on non Connective Line with HSI
#endif

    sysclock_stm32f1xx_profile_switch( Config, E_ttc_sysclock_profile_MaxSpeed );
}
void                     sysclock_stm32f1xx_profile_switch( t_ttc_sysclock_config* Config, e_ttc_sysclock_profile NewProfile ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );

#if (TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0)
    switch ( NewProfile ) {
        case E_ttc_sysclock_profile_Efficient: {
            _sysclock_stm32f1xx_set_clock_frequency( Config, scs_ExternalCrystal, TTC_SYSCLOCK_FREQUENCY_MAX ); // exemplary value (ToDo: find out most efficient one!)
        }
        case E_ttc_sysclock_profile_MaxSpeed: {
            _sysclock_stm32f1xx_set_clock_frequency( Config, scs_ExternalCrystal, TTC_SYSCLOCK_FREQUENCY_MAX );
            break;
        }
        case E_ttc_sysclock_profile_MinSpeed: {
            _sysclock_stm32f1xx_set_clock_frequency( Config, scs_ExternalCrystal, TTC_SYSCLOCK_FREQUENCY_MIN );
            break;
        }
        default: {
            ttc_assert_halt( ); // ToDo: implement
            SystemCoreClockUpdate();
            Config->SystemClockFrequency = SystemCoreClock;
            return;
            break;
        }
    }
#else // no HSE clock available: use internal oscillator HSI
    switch ( NewProfile ) {
        case E_ttc_sysclock_profile_Efficient: {
            _sysclock_stm32f1xx_set_clock_frequency( Config, scs_InternalHighSpeed, TTC_SYSCLOCK_FREQUENCY_MAX ); // exemplary value (ToDo: change to a better one!)
            break;
        }
        case E_ttc_sysclock_profile_MaxSpeed: {
            _sysclock_stm32f1xx_set_clock_frequency( Config, scs_InternalHighSpeed, TTC_SYSCLOCK_FREQUENCY_MAX );
            break;
        }
        case E_ttc_sysclock_profile_MinSpeed: {
            _sysclock_stm32f1xx_set_clock_frequency( Config, scs_InternalHighSpeed, TTC_SYSCLOCK_FREQUENCY_MIN );
            break;
        }
        default: {
            ttc_assert_halt( ); // ToDo: implement
            SystemCoreClockUpdate();
            Config->SystemClockFrequency = SystemCoreClock;
            return;
            break;
        }
    }
#endif
    Config->Profile = NewProfile; // profile changed
}
void                     sysclock_stm32f1xx_udelay( t_ttc_sysclock_config* Config, t_base Microseconds ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );

    // This delay has been calibrated on STM32F107 @ 72MHz (Protoboard Olimex P107)
    // Minimum delay 1usecs
    // Minimum usable delay 10usec

    volatile t_base_signed Remaining = ( ( Microseconds - 1 ) * 599 / 100 );

    // adapt to current system clock frequency (simple linear approach)
    Remaining *= ( Config->SystemClockFrequency / 10000 );
    Remaining /= ( ( sysclock_stm32f1xx_MaxClock_SYSCLK + 1 ) / 10000 );

    for ( ; Remaining > 0; Remaining-- );
}
void                     sysclock_stm32f1xx_update( t_ttc_sysclock_config* Config ) {
    _sysclock_stm32f1xx_update( Config, ssu_ALL );
}
e_ttc_sysclock_errorcode sysclock_stm32f1xx_enable_oscillator( e_ttc_sysclock_oscillator Oscillator, BOOL On ) {
    t_u8 TimeOut = -1;

    switch ( Oscillator ) {
        case E_ttc_sysclock_oscillator_HighSpeedInternal: {
            if ( On ) {
                register_stm32f1xx_RCC.CR.HSION = 1;
                while ( TimeOut && !register_stm32f1xx_RCC.CR.HSIRDY ) { TimeOut--; }
                if ( TimeOut == 0 ) {
                    register_stm32f1xx_RCC.CR.HSION = 0;
                    return E_ttc_sysclock_errorcode_OscillatorError; // could not start oscillator!
                }
            }
            else {
                register_stm32f1xx_RCC.CR.HSION = 0;
                while ( TimeOut && register_stm32f1xx_RCC.CR.HSIRDY ) { TimeOut--; }
                if ( TimeOut == 0 )
                { return E_ttc_sysclock_errorcode_OscillatorError; } // could not stop HSI!
            }
            break;
        }
        case E_ttc_sysclock_oscillator_HighSpeedExternal: {
            if ( On ) {
                register_stm32f1xx_RCC.CR.HSEON = 1;
                while ( TimeOut && !register_stm32f1xx_RCC.CR.HSERDY ) { TimeOut--; }
                if ( TimeOut == 0 ) {
                    register_stm32f1xx_RCC.CR.HSEON = 0;
                    return E_ttc_sysclock_errorcode_OscillatorError; // could not start oscillator!
                }
            }
            else {
                register_stm32f1xx_RCC.CR.HSEON = 0;
                while ( TimeOut && register_stm32f1xx_RCC.CR.HSERDY ) { TimeOut--; }
                if ( TimeOut == 0 )
                { return E_ttc_sysclock_errorcode_OscillatorError; } // could not stop HSE!
            }
            break;
        }
        case E_ttc_sysclock_oscillator_PLL1: {
            if ( On ) {
                register_stm32f1xx_RCC.CR.PLLON = 1;
                while ( TimeOut && !register_stm32f1xx_RCC.CR.PLLRDY ) { TimeOut--; }
                if ( TimeOut == 0 ) {
                    register_stm32f1xx_RCC.CR.PLLON = 0;
                    return E_ttc_sysclock_errorcode_OscillatorError; // could not start oscillator!
                }
            }
            else {
                register_stm32f1xx_RCC.CR.PLLON = 0;
                while ( TimeOut && register_stm32f1xx_RCC.CR.PLLRDY ) { TimeOut--; }
                if ( TimeOut == 0 )
                { return E_ttc_sysclock_errorcode_OscillatorError; } // could not stop PLL!
            }
            break;
        }
#ifdef STM32F10X_CL
        case E_ttc_sysclock_oscillator_PLL2: {
            if ( On ) {
                register_stm32f1xx_RCC.CR.PLL2ON = 1;
                while ( TimeOut && !register_stm32f1xx_RCC.CR.PLL2RDY ) { TimeOut--; }
                if ( TimeOut == 0 ) {
                    register_stm32f1xx_RCC.CR.PLL2ON = 0;
                    return E_ttc_sysclock_errorcode_OscillatorError; // could not start oscillator!
                }
            }
            else {
                register_stm32f1xx_RCC.CR.PLL2ON = 0;
                while ( TimeOut && register_stm32f1xx_RCC.CR.PLL2RDY ) { TimeOut--; }
                if ( TimeOut == 0 )
                { return E_ttc_sysclock_errorcode_OscillatorError; } // could not stop PLL!
            }
            break;
        }
        case E_ttc_sysclock_oscillator_PLL3: {
            if ( On ) {
                register_stm32f1xx_RCC.CR.PLL3ON = 1;
                while ( TimeOut && !register_stm32f1xx_RCC.CR.PLL3RDY ) { TimeOut--; }
                if ( TimeOut == 0 ) {
                    register_stm32f1xx_RCC.CR.PLL3ON = 0;
                    return E_ttc_sysclock_errorcode_OscillatorError; // could not start oscillator!
                }
            }
            else {
                register_stm32f1xx_RCC.CR.PLL3ON = 0;
                while ( TimeOut && register_stm32f1xx_RCC.CR.PLL3RDY ) { TimeOut--; }
                if ( TimeOut == 0 )
                { return E_ttc_sysclock_errorcode_OscillatorError; } // could not stop PLL!
            }
            break;
        }
#endif
        case E_ttc_sysclock_oscillator_LowSpeedInternal: {
            ttc_assert_halt( ); // ToDo: Implement
            break;
        }
        case E_ttc_sysclock_oscillator_LowSpeedExternal: {
            ttc_assert_halt( ); // ToDo: Implement
            break;
        }
        default: return E_ttc_sysclock_errorcode_OscillatorError; break;
    }

    return ( e_ttc_sysclock_errorcode ) 0;
}
const t_base*            sysclock_stm32f1xx_frequency_get_all( t_ttc_sysclock_config* Config ) {
    Assert_SYSCLOCK_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)
    ( void ) Config;

    return sysclock_stm32f1xx_AvailableFrequencies;
}
void                     sysclock_stm32f1xx_frequency_set( t_ttc_sysclock_config* Config, t_base NewFrequency ) {
    Assert_SYSCLOCK_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    if ( !Config->LowLevelConfig.SystemClockSource ) {
#if TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0
        Config->LowLevelConfig.SystemClockSource = scs_ExternalCrystal;
#else
        Config->LowLevelConfig.SystemClockSource = scs_InternalHighSpeed;
#endif
    }

    _sysclock_stm32f1xx_set_clock_frequency( Config,
                                             Config->LowLevelConfig.SystemClockSource,
                                             NewFrequency
                                           );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************

void _sysclock_stm32f1xx_prescaler_ahb( t_u16 Divider ) {
    t_u32 Value = 0;
    switch ( Divider ) { // -> RM0008 p.100
        case   1: Value = 0b0000; break;
        case   2: Value = 0b1000; break;
        case   4: Value = 0b1001; break;
        case   8: Value = 0b1010; break;
        case  16: Value = 0b1011; break;
        case  32: // <- same as for 64!
        case  64: Value = 0b1100; break;
        case 128: Value = 0b1101; break;
        case 256: Value = 0b1110; break;
        case 512: Value = 0b1111; break;
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break;
    }
    register_stm32f1xx_RCC.CFGR.HPRE = Value;
}
void _sysclock_stm32f1xx_switch_pllsrc( e_sysclock_stm32f1xx_sourcepll SourcePLL ) {

    switch ( SourcePLL ) {
        case sssp_SYSCLK_8MHz:
            break;
        case sssp_SYSCLK_PLL1_HSI:
            register_stm32f1xx_RCC.CFGR.PLLSRC = 0; // PLL input = HSI
            break;
#ifdef STM32F10X_CL
        // -> clock tree Connectivity Line in RM0008 p.123
        case sssp_SYSCLK_PLL1_HSE:
            register_stm32f1xx_RCC.CFGR.PLLSRC = 1;      // PLL input = PREDIV1
            register_stm32f1xx_RCC.CFGR2.PREDIV1SRC = 0; // PREDIV1 input = HSE (->RM0008 p.150)
            break;
        case sssp_SYSCLK_PLL1_PLL2_HSE:
            register_stm32f1xx_RCC.CFGR.PLLSRC = 1;      // PLL input = PREDIV1
            register_stm32f1xx_RCC.CFGR2.PREDIV1SRC = 1; // PREDIV1 input = PLL2 (->RM0008 p.150)
            break;
#else
        // -> clock tree NON Connectivity Line in RM0008 p.90
        case sssp_SYSCLK_HSE:
            break;
        case sssp_SYSCLK_PLLMUL_HSE:
            register_stm32f1xx_RCC.CFGR.PLLSRC = 1; // PLL input = HSE
            break;
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid clock source!
    }
}
void _sysclock_stm32f1xx_prescaler_usb( ssup_Divider Divider ) {
    t_u8 NewRegisterValue = 0;
    switch ( Divider ) {
        // select register value according to RM0008 p.98 (non connectivity) or p.132 (connectivity line)
        case ssup_USB_DivideBy_1:
            NewRegisterValue = 1;
            break;
        case ssup_USB_DivideBy_1_5:
            NewRegisterValue = 0;
            break;
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unsupported/ invalid divider for current architecture
    }

#ifdef STM32F10X_CL // configure USB Prescaler on connectivity line device
    if ( register_stm32f1xx_RCC.CFGR.USBPRE != NewRegisterValue ) { // changing settings
        if ( register_stm32f1xx_RCC.AHBENR.OTGFS_EN ) { // USB clock enabled: disable to change settings
            // disable USB clock
            register_stm32f1xx_RCC.AHBENR.OTGFS_EN = 0;

            // change prescaler setting
            register_stm32f1xx_RCC.CFGR.USBPRE = NewRegisterValue;

            // reenable USB clock
            register_stm32f1xx_RCC.AHBENR.OTGFS_EN = 1;
        }
        else { // directly change prescaler setting
            register_stm32f1xx_RCC.CFGR.USBPRE = NewRegisterValue;
        }

    }

#else               // configure USB Prescaler on non connectivity line device
    if ( register_stm32f1xx_RCC.CFGR.USBPRE != NewRegisterValue ) { // changing settings
        if ( register_stm32f1xx_RCC.APB1ENR.USB_EN ) { // USB clock enabled: disable to change settings
            // disable USB clock
            register_stm32f1xx_RCC.APB1ENR.USB_EN = 0;

            // change prescaler setting
            register_stm32f1xx_RCC.CFGR.USBPRE = NewRegisterValue;

            // reenable USB clock
            register_stm32f1xx_RCC.APB1ENR.USB_EN = 1;
        }
        else { // directly change prescaler setting
            // change prescaler setting
            register_stm32f1xx_RCC.CFGR.USBPRE = NewRegisterValue;
        }
    }
#endif
}
void _sysclock_stm32f1xx_prescaler_apb1( t_u32 FrequencyAHB ) {

    Assert_SYSCLOCK_EXTRA( FrequencyAHB > 0, ttc_assert_origin_auto );
    Assert_SYSCLOCK_EXTRA( FrequencyAHB <= sysclock_stm32f1xx_MaxClock_AHB, ttc_assert_origin_auto );

    t_u8 Divider_APB1 = 1;  // (APB2) = f(AHB) / Divider_APB1  -> RM0008 p.90
    t_base Frequency_APB1 = FrequencyAHB / Divider_APB1;
    while ( Frequency_APB1 > sysclock_stm32f1xx_MaxClock_APB1 ) { // ensure that APB1 bus is clocked < 36MHz (RM0008 p.100)
        Frequency_APB1 /= 2;
        Divider_APB1 *= 2;
    }

    switch ( Divider_APB1 ) { // -> RM0008 p.100
        case   1: register_stm32f1xx_RCC.CFGR.PPRE1 = 0b000; break;
        case   2: register_stm32f1xx_RCC.CFGR.PPRE1 = 0b100; break;
        case   4: register_stm32f1xx_RCC.CFGR.PPRE1 = 0b101; break;
        case   8: register_stm32f1xx_RCC.CFGR.PPRE1 = 0b110; break;
        case  16: register_stm32f1xx_RCC.CFGR.PPRE1 = 0b111; break;
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unsupported divider!
    }
}
void _sysclock_stm32f1xx_prescaler_apb2( t_u8 Divider ) {

    switch ( Divider ) { // -> RM0008 p.100
        case   1: register_stm32f1xx_RCC.CFGR.PPRE2 = 0b000; break;
        case   2: register_stm32f1xx_RCC.CFGR.PPRE2 = 0b100; break;
        case   4: register_stm32f1xx_RCC.CFGR.PPRE2 = 0b101; break;
        case   8: register_stm32f1xx_RCC.CFGR.PPRE2 = 0b110; break;
        case  16: register_stm32f1xx_RCC.CFGR.PPRE2 = 0b111; break;
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break;
    }
}
void _sysclock_stm32f1xx_prescaler_adc( t_u32 FrequencyAPB2 ) {
    const t_u8 DividersADC[4] = { 2, 4, 6, 8 }; // ADC prescaler (-> RM0008 p.99)

    const t_u8* DividerADC = DividersADC;
    t_u8 RegisterValue = 0;
    t_base FrequencyADC;

    do { // find smallest divider that gives Frequency <= sysclock_stm32f1xx_MaxClock_ADC
        FrequencyADC  = FrequencyAPB2 / *DividerADC;
        if ( FrequencyADC > sysclock_stm32f1xx_MaxClock_ADC ) {
            DividerADC++;
            RegisterValue++;
        }
    }
    while ( FrequencyADC > sysclock_stm32f1xx_MaxClock_ADC );

    Assert_SYSCLOCK_EXTRA( RegisterValue < 4, ttc_assert_origin_auto );  // ADCPRE is 2 bits only
    register_stm32f1xx_RCC.CFGR.ADCPRE = RegisterValue;
}
#ifndef STM32F10X_CL
void _sysclock_stm32f1xx_prescaler_hse( t_ttc_sysclock_config* Config, ssup_Divider Divider ) {

    Assert_SYSCLOCK_EXTRA( Divider <= 2, ttc_assert_origin_auto );
    Assert_SYSCLOCK_EXTRA( Divider >  0, ttc_assert_origin_auto );
    register_stm32f1xx_RCC.CFGR.PLLXTPRE = Divider - 1;

    Assert_SYSCLOCK_EXTRA_Writable( Config, ttc_assert_origin_auto );
    Config->LowLevelConfig.divider_hse    = Divider;
}
#endif
void _sysclock_stm32f1xx_pll_configure( t_u8 PLL_Index, t_u8 Multiplier_x2, t_u8 Divider, e_sysclock_stm32f1xx_sourcepll SourcePLL ) {
    switch ( PLL_Index ) {
        case 1: {
#ifdef STM32F10X_CL
            //  Expecting PLL Multiplication Factor x2 (-> RM0008 p.132)
            Assert_SYSCLOCK( ( Multiplier_x2 >= 2 * 4 ) && ( Multiplier_x2 <= 2 * 9 ), ttc_assert_origin_auto );
            Assert_SYSCLOCK( ( Divider  < 17 ), ttc_assert_origin_auto );  // -> RM0008 p.152
#else
            //  Low-, medium-, high- and XL-density reset and clock control (-> RM0008 p.99)
            Assert_SYSCLOCK( ( Multiplier_x2 >= 2 * 2 ) && ( Multiplier_x2 <= 2 * 16 ), ttc_assert_origin_auto );
            Assert_SYSCLOCK( ( Divider  <= 2 ), ttc_assert_origin_auto );  // this devices have only PLLXTPRE
#endif

            t_u8 Value_SWS = register_stm32f1xx_RCC.CFGR.SWS;
            if ( Value_SWS == 0b10 ) { // System Clock is using this PLL: switch to another source
                if ( register_stm32f1xx_RCC.CR.HSIRDY ) {
                    register_stm32f1xx_RCC.CFGR.SW = 0x00; // select HSI as system clock
                    while ( register_stm32f1xx_RCC.CFGR.SWS != 0x00 ); // wait for switch to happen
                }
            }
            sysclock_stm32f1xx_enable_oscillator( E_ttc_sysclock_oscillator_PLL1, 0 ); // switch of PLL to allow reconfiguration

            _sysclock_stm32f1xx_switch_pllsrc( SourcePLL ); // switch PLL input to desired clock source

#ifdef STM32F10X_CL
            // -> RM0008 p.132
            if ( Multiplier_x2 == 13 )
            { register_stm32f1xx_RCC.CFGR.PLLMUL = 0b1101; }   // factor 6.5
            else
            { register_stm32f1xx_RCC.CFGR.PLLMUL      = ( Multiplier_x2 / 2 ) - 2; }

            register_stm32f1xx_RCC.CFGR2.PREDIV1    = Divider - 1;
#else

            register_stm32f1xx_RCC.CFGR.PLLXTPRE = Divider - 1;
            //X _sysclock_stm32f1xx_prescaler_ahb( Divider );
            register_stm32f1xx_RCC.CFGR.PLLMUL      = ( Multiplier_x2 / 2 ) - 2;
#endif
            sysclock_stm32f1xx_enable_oscillator( E_ttc_sysclock_oscillator_PLL1, 1 ); // switch back on PLL

            register_stm32f1xx_RCC.CFGR.SW = Value_SWS; // restore previous system clock source
            while ( register_stm32f1xx_RCC.CFGR.SWS != Value_SWS ); // wait for switch to happen

            break;
        }
#ifdef STM32F10X_CL
        case 2: {
            Assert_SYSCLOCK( ( Multiplier_x2 >= 2 * 8 ) && ( Multiplier_x2 <= 2 * 20 ), ttc_assert_origin_auto );  // -> RM0008 p.151
            Assert_SYSCLOCK( ( Divider  < 17 ), ttc_assert_origin_auto );  // -> RM0008 p.152
            t_u8 Previous_PLL3ON = register_stm32f1xx_RCC.CR.PLL3ON;
            register_stm32f1xx_RCC.CR.PLL2ON     = 0; // switch of PLL to allow reconfiguration
            register_stm32f1xx_RCC.CR.PLL3ON     = 0; // switch of PLL to allow reconfiguration

            _sysclock_stm32f1xx_switch_pllsrc( SourcePLL ); // switch PLL input to desired clock source

            register_stm32f1xx_RCC.CFGR2.PLL2MUL = ( Multiplier_x2 / 2 ) - 2;
            register_stm32f1xx_RCC.CFGR2.PREDIV2 = Divider - 1;

            // switch back on PLL2 + PLL3
            register_stm32f1xx_RCC.CR.PLL2ON     = 1;
            register_stm32f1xx_RCC.CR.PLL3ON     = Previous_PLL3ON;
            while ( !register_stm32f1xx_RCC.CR.PLL2RDY ); // wait for PLL to lock
            break;
        }
        case 3: {
            Assert_SYSCLOCK( ( Multiplier_x2 >= 2 * 8 ) && ( Multiplier_x2 <= 2 * 20 ), ttc_assert_origin_auto );  // -> RM0008 p.151
            Assert_SYSCLOCK( ( Divider  < 17 ), ttc_assert_origin_auto );  // -> RM0008 p.152
            register_stm32f1xx_RCC.CR.PLL2ON    = 0; // switch of PLL to allow reconfiguration
            register_stm32f1xx_RCC.CR.PLL3ON    = 0; // switch of PLL to allow reconfiguration

            register_stm32f1xx_RCC.CFGR2.PLL3MUL = ( Multiplier_x2 / 2 ) - 2;
            register_stm32f1xx_RCC.CFGR2.PREDIV2 = Divider - 1;

            // switch back on PLL2 + PLL3
            register_stm32f1xx_RCC.CR.PLL2ON     = 1;
            register_stm32f1xx_RCC.CR.PLL3ON     = 1;
            while ( !register_stm32f1xx_RCC.CR.PLL2RDY ); // wait for PLL to lock
            while ( !register_stm32f1xx_RCC.CR.PLL3RDY ); // wait for PLL to lock
            break;
        }
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break;
    }
}
void _sysclock_stm32f1xx_pll_read( t_u8 PLL_Index, t_u8* Multiplier_x2, t_u8* Divider ) {
    Assert_SYSCLOCK_Writable( Multiplier_x2, ttc_assert_origin_auto );
    Assert_SYSCLOCK_Writable( Divider, ttc_assert_origin_auto );

    *Divider = 1;

    switch ( PLL_Index ) {
        case 1: {
            // returning PLL multiplication factor x2 to support factor 6.5 (-> RM0008 p.132)
            t_u8 Value = register_stm32f1xx_RCC.CFGR.PLLMUL;
            if ( Value == 0b1101 ) {
                *Multiplier_x2 = 13; // factor 6.5
            }
            else {
                *Multiplier_x2 = ( register_stm32f1xx_RCC.CFGR.PLLMUL   + 2 ) * 2;
            }
#ifdef STM32F10X_CL
            *Divider    = register_stm32f1xx_RCC.CFGR2.PREDIV1 + 1;
#else
            if ( register_stm32f1xx_RCC.CFGR.PLLSRC == 1 ) { // PLL gets clock from HSE OSC
                if ( register_stm32f1xx_RCC.CFGR.PLLXTPRE == 1 )
                { *Divider = 2; }                              // PLL clock = HSE/2
            }
            else {                                               // PLL gets clock from HSI/2
                *Divider = 2;
            }
#endif
            break;
        }
#ifdef STM32F10X_CL
        case 2: {
            *Multiplier_x2 = ( register_stm32f1xx_RCC.CFGR2.PLL2MUL + 2 ) * 2;
            *Divider    = register_stm32f1xx_RCC.CFGR2.PREDIV2 + 1;
            break;
        }
        case 3: {
            *Multiplier_x2 = ( register_stm32f1xx_RCC.CFGR2.PLL3MUL + 2 ) * 2;
            *Divider    = register_stm32f1xx_RCC.CFGR2.PREDIV2 + 1;
            break;
        }
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break;
    }
    if ( *Multiplier_x2 == 2 * 17 ) // -> RM0008 p.151
    { *Multiplier_x2 = 2 * 20; }
}
void _sysclock_stm32f1xx_set_clock_frequency( t_ttc_sysclock_config* Config, e_stm32_clock_source Source, t_u32 TargetFrequency ) {
    /** Register settings after SystemInit()

    (gdb) p register_stm32f1xx_RCC.CR
    $1 = {HSION = 1, HSIRDY = 1, reserved1 = 0, HSITRIM = 16, HSICAL = 86, HSEON = 1, HSERDY = 1,
          HSEBYP = 0, CSSON = 0, reserved2 = 0, PLLON = 1, PLLRDY = 1, PLL2ON = 1, PLL2RDY = 1,
          PLL3ON = 0, PLL3RDY = 0, reserved3 = 0}
    (gdb) p register_stm32f1xx_RCC.CFGR
    $2 = {SW = 2, SWS = 2, HPRE = 0, PPRE1 = 4, PPRE2 = 0, ADCPRE = 0, PLLSRC = 1, PLLXTPRE = 0,
          PLLMUL = 7, USBPRE = 0, reserved1 = 0, MCO = 7, reserved2 = 0}
    (gdb) p register_stm32f1xx_RCC.CFGR2
    $3 = {PREDIV1 = 0, PREDIV2 = 0, PLL2MUL = 0, PLL3MUL = 0, PREDIV1SRC = 0, I2S2SRC = 0, I2S3SRC = 1, reserved1 = 200}
    */

    t_u8  Optimal_PLL1_Divider       = 0;  // f(AHB)      = f(SYSCLOCK) / Optimal_PLL1_Divider             (=1..512)
    t_u8  Optimal_PLL1_Multiplier_x2 = 0;  // f(SYSCLOCK) = f(PLLSRC)   * Optimal_PLL1_Multiplier_x2 / 2   (=4..16)
    ssup_Divider Optimal_DividerUSB  = ssup_None; // >0: configure new USB prescaler divider (must be 48 MHz to allow USB operation)
    e_sysclock_stm32f1xx_sourcepll Optimal_SourcePLL = sssp_None; // PLL 2 clock input source
#ifdef STM32F10X_CL // connectivity line devices have more PLLs than other family members
    t_u8  Optimal_PLL2_Multiplier_x2 = 0;  // using PLL 2 multiplier * 2 because STM32F107 provides one strange multiplier x6.5 (and we want to use it with integer math)
    t_u8  Optimal_PLL2_Divider       = 0;
#endif
    t_u32 Optimal_FrequencySYSCLK    = 0;  // calculated SYSCLK frequency (base frequency for clock tree calculation)
    t_u32 MinDeviation               = 0;  // difference between calculated frequency and TargetFrequency

    t_u8 OriginalClockSwitch = register_stm32f1xx_RCC.CFGR.SWS;
    t_u8 NewClockSwitch = OriginalClockSwitch;

    // read current PLL prescaler settings
    _sysclock_stm32f1xx_pll_read( 1, &Optimal_PLL1_Multiplier_x2, &Optimal_PLL1_Divider );
#ifdef STM32F10X_CL // only connectivity line devices have more than one PLL
    _sysclock_stm32f1xx_pll_read( 2, &Optimal_PLL2_Multiplier_x2, &Optimal_PLL2_Divider );
#endif

    switch ( Source ) {
        case scs_InternalHighSpeed: {
            NewClockSwitch = 0b10; // will switch system clock to PLL ->field SWS in RM0008 p. 100
            Optimal_SourcePLL = sssp_SYSCLK_PLL1_HSI; // default: send HSI/2 through PLLMUL

            MinDeviation = TargetFrequency;

            // Find optimal setting for HighSpeed internal clock + PLLMUL
            // compare with stm32f1xx clock tree -> RM0008 p.123
#ifdef STM32F10X_CL
            Assert_SYSCLOCK( Config->Flags.Bits.ConsiderUSB == 0, ttc_assert_origin_auto );  // Connectivity Line cannot support USB 48MHz clock from HSI. Disable ConsiderUSB or use external crystal!
#else
            if ( Config->Flags.Bits.ConsiderUSB ) { // choose multiplier to allow 48MHz USB operation

                // max SYSCLK frequency from HSI: 64 MHz => can only use 48MHz to operate USB
                MinDeviation = ttc_basic_distance( TargetFrequency, 48000000 );
                Optimal_DividerUSB = ssup_USB_DivideBy_1;
                Optimal_PLL1_Multiplier_x2 = 2 * 12;
                Optimal_FrequencySYSCLK = 48000000;
            }
#endif
            if ( !Optimal_FrequencySYSCLK ) { // calculate optimal multiplier
                if ( TargetFrequency == 8000000 ) { // special case: run on HSI directly
                    Optimal_SourcePLL = sssp_SYSCLK_8MHz;
                    NewClockSwitch    = 0b00; // will switch directly to HSI (8MHz) without PLL
                }
                else { // calculate best PLL configuration for TargetFrequency
#ifdef STM32F10X_CL
                    Optimal_PLL1_Multiplier_x2 = TargetFrequency / ( 4000000 / 2 );
                    if ( Optimal_PLL1_Multiplier_x2 > 2 * 9 )
                    { Optimal_PLL1_Multiplier_x2 = 2 * 9; }
                    else {
                        if ( Optimal_PLL1_Multiplier_x2 <= 2 * 4 )
                        { Optimal_PLL1_Multiplier_x2 = 2 * 4; }
                        else {
                            if ( Optimal_PLL1_Multiplier_x2 == 13 )
                            { Optimal_PLL1_Multiplier_x2 = 13; }  // x6.5
                            else {
                                Optimal_PLL1_Multiplier_x2 = Optimal_PLL1_Multiplier_x2 & 0b11111110; // make it a multiple of 2
                            }
                        }
                    }
                    Optimal_FrequencySYSCLK = 4000000 * Optimal_PLL1_Multiplier_x2;
#else
                    Optimal_PLL1_Multiplier_x2 = TargetFrequency / ( 4000000 / 2 );
                    if ( Optimal_PLL1_Multiplier_x2 <= 2 * 2 )
                    { Optimal_PLL1_Multiplier_x2 = 2 * 2; }
                    else {
                        if ( Optimal_PLL1_Multiplier_x2 >= 2 * 16 )
                        { Optimal_PLL1_Multiplier_x2 = 2 * 16; }
                        else {
                            Optimal_PLL1_Multiplier_x2 &= 0b11111110; // make it a multiple of 2
                        }
                    }
                    Optimal_FrequencySYSCLK = 4000000 / 2 * Optimal_PLL1_Multiplier_x2;
#endif
                }
            }
            break;
        }
#ifdef TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL
#if TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0
        case scs_ExternalCrystal: {
            NewClockSwitch = 0b10; // will switch system clock to PLL ->field SWS in RM0008 p. 100

#ifdef STM32F10X_CL // connectivity line devices have two complete divider-multiplier sets
            // calculate current SYSCLK frequency ->RM0008 p.123
            t_u32 SystemClockFrequency = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL
                                         / Optimal_PLL2_Divider * Optimal_PLL2_Multiplier_x2 / 2
                                         / Optimal_PLL1_Divider * Optimal_PLL1_Multiplier_x2 / 2;
            MinDeviation = ttc_basic_distance( TargetFrequency, SystemClockFrequency );

            // Find optimal setting for two PLLs (HSE -> PLL2 -> PLL1 -> SystemClock)
            // compare with stm32f1xx clock tree -> RM0008 p.123
            if ( MinDeviation ) { // PLL-config suboptimal: calculate best PLL configuration
                if ( Config->Flags.Bits.ConsiderUSB ) { // choose multiplier to allow 48MHz USB operation

                    // With USB dividers 1.0 and 1.5, PLL frequency on Connectivity Line can only be 48MHz or 72MHz (RM0008 p.123)
                    t_u32 Deviation1 = ttc_basic_distance( TargetFrequency, 48000000 );
                    t_u32 Deviation2 = ttc_basic_distance( TargetFrequency, 72000000 );
                    if ( Deviation1 < Deviation2 ) { // choosing USB Divider 1.5
                        Optimal_DividerUSB = ssup_USB_DivideBy_1;
                        TargetFrequency = 48000000;
                    }
                    else {                         // choosing USB Divider 1.0
                        Optimal_DividerUSB = ssup_USB_DivideBy_1_5;
                        TargetFrequency = 72000000;
                    }
                }
                Optimal_PLL2_Multiplier_x2 = 0;
                Optimal_PLL2_Divider       = 0;

                // try to calculate optimal fit NewFrequency with only one prescaler set
                for ( t_u8 PLL1_Divider = PLL1_Divider_Min; PLL1_Divider <= PLL1_Divider_Max; PLL1_Divider++ ) {
                    for ( t_u8 PLL1_Multiplier_x2 = PLL1_Multiplier_x2_Min; PLL1_Multiplier_x2 <= PLL1_Multiplier_x2_Max; PLL1_Multiplier_x2 += 2 ) {

                        SystemClockFrequency = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL
                                               / PLL1_Divider * PLL1_Multiplier_x2 / 2;

                        if ( SystemClockFrequency <= sysclock_stm32f1xx_MaxClock_SYSCLK ) {
                            t_u32 Deviation = ttc_basic_distance( TargetFrequency, SystemClockFrequency );

                            if ( MinDeviation > Deviation ) {
                                MinDeviation = Deviation;
                                Optimal_PLL1_Multiplier_x2 = PLL1_Multiplier_x2;
                                Optimal_PLL1_Divider       = PLL1_Divider;
                            }
                            if ( !MinDeviation ) { goto end_fors2; } // found optimal solution
                        }
                        else
                        { break; }  // SYSCLK frequency already too high: skip bigger multipliers
                    }
                }
            end_fors2:
                if ( !MinDeviation ) { // found optimal configuration using only one prescaler
                    Optimal_SourcePLL = sssp_SYSCLK_PLL1_HSE;

                    // calculate found SYSCLK frequency using only Prescaler 1 ->RM0008 p.123
                    Optimal_FrequencySYSCLK = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL
                                              / Optimal_PLL1_Divider * Optimal_PLL1_Multiplier_x2 / 2;
                }
                else {                // found configuration not optimal: try with two prescalers
                    Optimal_SourcePLL = sssp_SYSCLK_PLL1_PLL2_HSE; // will use both prescalers

                    // calculate divider and multiplier to optimal fit NewFrequency
                    for ( t_u8 PLL1_Divider = PLL1_Divider_Min; PLL1_Divider <= PLL1_Divider_Max; PLL1_Divider++ ) {
                        for ( t_u8 PLL1_Multiplier_x2 = PLL1_Multiplier_x2_Min; PLL1_Multiplier_x2 <= PLL1_Multiplier_x2_Max; ) {
                            for ( t_u8 PLL2_Divider = PLL2_Divider_Min; PLL2_Divider <= PLL2_Divider_Max; PLL2_Divider++ ) {
                                for ( t_u8 PLL2_Multiplier = PLL2_Multiplier_Min; PLL2_Multiplier <= PLL2_Multiplier_Max; PLL2_Multiplier++ ) {

                                    SystemClockFrequency = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL
                                                           / PLL2_Divider * PLL2_Multiplier
                                                           / PLL1_Divider * PLL1_Multiplier_x2 / 2;
                                    if ( SystemClockFrequency <= sysclock_stm32f1xx_MaxClock_SYSCLK ) {
                                        t_u32 Deviation = ttc_basic_distance( TargetFrequency, SystemClockFrequency );

                                        if ( MinDeviation > Deviation ) {
                                            MinDeviation = Deviation;
                                            Optimal_PLL1_Multiplier_x2 = PLL1_Multiplier_x2;
                                            Optimal_PLL1_Divider       = PLL1_Divider;
                                            Optimal_PLL2_Multiplier_x2 = PLL2_Multiplier * 2;
                                            Optimal_PLL2_Divider       = PLL2_Divider;
                                        }
                                        if ( !MinDeviation ) { goto end_fors3; } // found optimal solution
                                    }
                                    else
                                    { break; }  // SYSCLK frequency already too high: skip bigger multipliers
                                }
                            }

                            // proceeding to next multiplier is not simple because of special factor x6.5 (-> RM0008 p.132)
                            if ( PLL1_Multiplier_x2 == 2 * 6 )
                            { PLL1_Multiplier_x2 = 13; }  // x6.5
                            else {
                                if ( PLL1_Multiplier_x2 == 13 )
                                { PLL1_Multiplier_x2 = 14; }
                                else
                                { PLL1_Multiplier_x2 += 2; }
                            }
                        }
                    }
                end_fors3:
                    // calculate found SYSCLK frequency ->RM0008 p.123
                    Optimal_FrequencySYSCLK = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL
                                              / Optimal_PLL2_Divider * Optimal_PLL2_Multiplier_x2 / 2
                                              / Optimal_PLL1_Divider * Optimal_PLL1_Multiplier_x2 / 2;
                }
            }
            else {
                Optimal_FrequencySYSCLK = SystemClockFrequency;

                // read PLL input source from current register settings
                Optimal_SourcePLL = ( register_stm32f1xx_RCC.CFGR2.PREDIV1SRC ) ?
                                    sssp_SYSCLK_PLL1_HSE :
                                    sssp_SYSCLK_PLL1_PLL2_HSE;
            }

#else
            // select HSE with Prescaler as PLL input clock
            Optimal_SourcePLL = sssp_SYSCLK_PLLMUL_HSE;

            // calculate current frequency of highspeed external clock (may be divided by 2)
            t_u32 FrequencyHSE = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL
                                 / Optimal_PLL1_Divider
                                 * Optimal_PLL1_Multiplier_x2 / 2;
            if ( register_stm32f1xx_RCC.CFGR.PLLXTPRE )
            { FrequencyHSE /= 2; }
            MinDeviation = ttc_basic_distance( TargetFrequency, FrequencyHSE );

            // Find optimal setting for one PLLs (HSE -> PLL -> SystemClock)
            // compare with stm32f1xx clock tree -> RM0008 p.123
            if ( MinDeviation ) { // PLL-config suboptimal: calculate best PLL configuration
                if ( Config->Flags.Bits.ConsiderUSB ) { // choose multiplier to allow 48MHz USB operation

                    // find optimal setting to provide 48MHz USB clock from current HSE crystal
                    t_u8 PLL1_Multiplier10 = ( t_u8 )( 48000000 / TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL ); // USB Prescaler = 1.0
                    t_u8 PLL1_Multiplier15 = ( t_u8 )( 72000000 / TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL ); // USB Prescaler = 1.5

                    // calculate possible USB clock frequencies for both USB Prescaler settings
                    t_base FrequencyUSB10 = FrequencyHSE * PLL1_Multiplier10;
                    t_base FrequencyUSB15 = FrequencyHSE * PLL1_Multiplier15 / 2 * 3;

                    // calculate frequency differences for both USB prescaler settings
                    t_base_signed FrequencyUSB10_Difference = 48000000 - FrequencyUSB10;
                    t_base_signed FrequencyUSB15_Difference = 48000000 - FrequencyUSB15;
                    FrequencyUSB10_Difference = abs( FrequencyUSB10_Difference );
                    FrequencyUSB15_Difference = abs( FrequencyUSB15_Difference );

                    // choose prescaler setting that gives USB frequency nearer to 48MHz
                    t_u8 PLL1_Multiplier;
                    if ( FrequencyUSB10_Difference == FrequencyUSB15_Difference ) { // both settings are precise: choose SYSCLOCK nearest to TargetFrequency
                        // difference is now delta between SYSCLK frequency and TargetFrequency
                        FrequencyUSB10_Difference = TargetFrequency - FrequencyUSB10;
                        FrequencyUSB15_Difference = TargetFrequency - FrequencyUSB15;
                        FrequencyUSB10_Difference = abs( FrequencyUSB10_Difference );
                        FrequencyUSB15_Difference = abs( FrequencyUSB15_Difference );
                    }
                    t_base NewFrequencySYSCLK; // system clock frequency
                    if ( FrequencyUSB15_Difference <= FrequencyUSB10_Difference ) {
                        PLL1_Multiplier = PLL1_Multiplier15;
                        Optimal_DividerUSB = ssup_USB_DivideBy_1_5; // PLL divided by 1.5
                        NewFrequencySYSCLK = FrequencyUSB15;
                    }
                    else {
                        PLL1_Multiplier = PLL1_Multiplier10;
                        Optimal_DividerUSB = ssup_USB_DivideBy_1; // PLL divided by 1.0
                        NewFrequencySYSCLK = FrequencyUSB10;
                    }

                    // calculate desired devider
                    t_u16 PLL1_Divider = ( t_u8 )( TargetFrequency / NewFrequencySYSCLK );

                    // ensure that real frequency <= NewFrequency
                    while ( NewFrequencySYSCLK / PLL1_Divider > TargetFrequency ) {
                        PLL1_Divider++;
                    }

                    // select next available divider
                    if ( PLL1_Divider <= 4 ) {
                        if ( PLL1_Divider == 3 )
                        { PLL1_Divider = 4; }  // divider 3 not available
                        else {
                            if ( PLL1_Divider < 2 )
                            { PLL1_Divider = 1; }
                        }
                    }
                    else {
                        if ( PLL1_Divider <= 8 )
                        { PLL1_Divider = 8; }
                        else {
                            if ( PLL1_Divider <= 16 )
                            { PLL1_Divider = 16; }
                            else {
                                if ( PLL1_Divider <= 64 )
                                { PLL1_Divider = 64; }
                                else {
                                    if ( PLL1_Divider <= 128 )
                                    { PLL1_Divider = 128; }
                                    else {
                                        if ( PLL1_Divider <= 256 )
                                        { PLL1_Divider = 256; }
                                        else {
                                            PLL1_Divider = 512;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    Optimal_PLL1_Multiplier_x2 = PLL1_Multiplier;
                    Optimal_PLL1_Divider       = PLL1_Divider;
                }
                else { // calculate divider and multiplier to optimal fit NewFrequency
                    for ( t_u8 AHB_DividerIndex = 0; AHB_DividerIndex < AMOUNT_AHB_Dividers; AHB_DividerIndex++ ) {
                        t_u16 AHB_Divider = AHB_Dividers[AHB_DividerIndex];
                        for ( t_u8 PLL1_Multiplier_x2 = 2; PLL1_Multiplier_x2 <= PLL1_Multiplier_x2_Max; PLL1_Multiplier_x2 += 2 ) {

                            t_u32 NewFrequencySYSCLK = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL * PLL1_Multiplier_x2;
                            if ( NewFrequencySYSCLK <= sysclock_stm32f1xx_MaxClock_SYSCLK ) {
                                t_u32 FrequencyAHB    = NewFrequencySYSCLK / AHB_Divider;
                                t_u32 Deviation;
                                if ( TargetFrequency < NewFrequencySYSCLK ) { // SYSCLK will be higher that TargetFrequency => comparing AHB clock to target frequency
                                    Deviation = ttc_basic_distance( TargetFrequency, FrequencyAHB );
                                }
                                else {                                   // compare SYSCLOCK to TargetFrequency
                                    Deviation = ttc_basic_distance( TargetFrequency, NewFrequencySYSCLK );
                                }
                                if ( MinDeviation > Deviation ) { // found better optimal configuration
                                    MinDeviation = Deviation;
                                    Optimal_PLL1_Divider    = AHB_Divider;
                                    if ( Optimal_PLL1_Multiplier_x2 == 1 ) { // can only achieve x1 = /2 x2
                                        Optimal_PLL1_Multiplier_x2 = 2;
                                        Optimal_PLL1_Divider       = 2;
                                    }
                                    else {
                                        Optimal_PLL1_Multiplier_x2 = PLL1_Multiplier_x2;
                                        Optimal_PLL1_Divider       = 1;
                                    }
                                }
                                if ( !MinDeviation ) { goto end_fors; } // found optimal solution
                            }
                            else
                            { break; }  // frequency already too high: skip larger multipliers
                        }
                    }
                }
            }
            else { // current settings are optimal: load HSE-Divider from register
                Optimal_PLL1_Divider = ( register_stm32f1xx_RCC.CFGR.PLLXTPRE ) + 1;
            }
        end_fors:
            // calculate configured SYSCLK frequency ->RM0008 p.90
            Assert_SYSCLOCK_EXTRA( Optimal_PLL1_Divider > 0, ttc_assert_origin_auto );   // will divide by zero!
            Optimal_FrequencySYSCLK = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL
                                      / Optimal_PLL1_Divider
                                      * Optimal_PLL1_Multiplier_x2 / 2;
            Assert_SYSCLOCK_EXTRA( Optimal_FrequencySYSCLK <= sysclock_stm32f1xx_MaxClock_SYSCLK, ttc_assert_origin_auto );
#endif

            break;
        }
#endif
#endif
        default: ttc_assert_halt( ); break;
    }

    if ( Optimal_PLL1_Multiplier_x2 > 0 ) { // apply new clock frequency configuration
        Assert_SYSCLOCK_EXTRA( Optimal_FrequencySYSCLK > 0, ttc_assert_origin_auto );  // value should have been calculated. Check code above!

        if ( OriginalClockSwitch ) { // running external clock as system clock: have to switch to internal before reconfiguration
            if ( sysclock_stm32f1xx_enable_oscillator( E_ttc_sysclock_oscillator_HighSpeedInternal, 1 ) == E_ttc_sysclock_errorcode_OK ) {
                while ( register_stm32f1xx_RCC.CFGR.SWS )  // switch to internal high speed clock
                { register_stm32f1xx_RCC.CFGR.SW = 0; }
            }
            else { ttc_assert_halt_origin( ttc_assert_origin_auto ); }
        }

        // switch off all PLLs to be able to reconfigure them
        sysclock_stm32f1xx_enable_oscillator( E_ttc_sysclock_oscillator_PLL1, 0 );

#ifdef STM32F10X_CL

        t_u8 Original_PLL3ON = register_stm32f1xx_RCC.CR.PLL3ON;
        t_u8 Original_PLL2ON = register_stm32f1xx_RCC.CR.PLL2ON;
        if ( Optimal_SourcePLL == sssp_SYSCLK_PLL1_PLL2_HSE )
        { sysclock_stm32f1xx_enable_oscillator( E_ttc_sysclock_oscillator_PLL2, 0 ); }
        sysclock_stm32f1xx_enable_oscillator( E_ttc_sysclock_oscillator_PLL3, 0 );

        register_stm32f1xx_RCC.CFGR2.PREDIV1SRC = 1; // select PLL2 oscillator clock as PREDIV1 clock entry

        if ( Optimal_SourcePLL == sssp_SYSCLK_PLL1_PLL2_HSE )  // configure PLL2 with new settings
        { _sysclock_stm32f1xx_pll_configure( 2, Optimal_PLL2_Multiplier_x2, Optimal_PLL2_Divider, Optimal_SourcePLL ); }
#endif

        // configure PLL1 with new settings
        _sysclock_stm32f1xx_pll_configure( 1, Optimal_PLL1_Multiplier_x2, Optimal_PLL1_Divider, Optimal_SourcePLL );

        // find AHB divider for maximum AHB clock frequency <= TargetFrequency
        t_u16 AHB_Divider;
        t_u32 AHB_Frequency;
        for ( t_u8 AHB_DividerIndex = 0; AHB_DividerIndex < AMOUNT_AHB_Dividers; AHB_DividerIndex++ ) {
            AHB_Divider = AHB_Dividers[AHB_DividerIndex];
            AHB_Frequency = Optimal_FrequencySYSCLK / AHB_Divider;
            if ( AHB_Frequency <= TargetFrequency ) {
                break;
            }
        }
        _sysclock_stm32f1xx_prescaler_ahb( AHB_Divider );  // configure AHB bus for max speed and < TargetFrequency
        _sysclock_stm32f1xx_prescaler_apb2( AHB_Divider ); // configure APB2 bus for max speed and < TargetFrequency

        // confgure APB1 prescaler for max frequency <= 36MHz
        _sysclock_stm32f1xx_prescaler_apb1( AHB_Frequency );

        // configure ADC prescaler for max ADC clock <= 14MHz
        _sysclock_stm32f1xx_prescaler_adc( AHB_Frequency ); // APB2 prescaler was configured identical to AHB prescaler

        if ( Optimal_DividerUSB ) { // configure usb prescaler to optimal value
            _sysclock_stm32f1xx_prescaler_usb( Optimal_DividerUSB );
        }

        // switch on PLL 1 and 2 and revert 3 to its original state
        sysclock_stm32f1xx_enable_oscillator( E_ttc_sysclock_oscillator_PLL1, 1 );
#ifdef STM32F10X_CL
        if ( Optimal_SourcePLL == sssp_SYSCLK_PLL1_PLL2_HSE )
        { sysclock_stm32f1xx_enable_oscillator( E_ttc_sysclock_oscillator_PLL2, 1 ); }
        else
        { sysclock_stm32f1xx_enable_oscillator( E_ttc_sysclock_oscillator_PLL2, Original_PLL2ON ); }
        sysclock_stm32f1xx_enable_oscillator( E_ttc_sysclock_oscillator_PLL3, Original_PLL3ON );

#else

        // Non Connectivity Line devices only have HSE and HSE/2
        _sysclock_stm32f1xx_switch_pllsrc( sssp_SYSCLK_HSE );
#endif

        register_stm32f1xx_RCC.CFGR.SW = NewClockSwitch;             // switch SYSCLK to new clock source
        while ( register_stm32f1xx_RCC.CFGR.SWS != NewClockSwitch ); // wait for switch to take place

        // recalculate frequencies of all clock tree levels
        _sysclock_stm32f1xx_update( Config, ssu_ALL );
    }
}
void _sysclock_stm32f1xx_update( t_ttc_sysclock_config* Config, ssu_MaxStage MaxStage ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_SYSCLOCK_EXTRA( MaxStage < ssu_unknown, ttc_assert_origin_auto );
    Assert_SYSCLOCK_EXTRA( MaxStage > ssu_None, ttc_assert_origin_auto );

    // compare with clock tree RM0008 p.123

    const t_u8 ss_DividerADC[4]  = { 2, 4, 6, 8 };                     // value of register ADCPRE
    const t_u8 ss_DividerAPB[4]  = { 2, 4, 8, 16 };                    // value of register PPRE1/PPRE2

    t_u8 PLL1_Multiplier_x2 = 0;
    t_u8 PLL1_Divider    = 0;
    _sysclock_stm32f1xx_pll_read( 1, &PLL1_Multiplier_x2, &PLL1_Divider );

    t_base Frequency_PLL = 0; // base frequency for all lower clock tree levels
    if ( MaxStage >= ssu_PLLCLK )  { // calculate PLLCLK frequency
        t_u8 Value_SWS = register_stm32f1xx_RCC.CFGR.SWS;
        switch ( Value_SWS ) {
            case 0b00: { // running from HSI (8Mhz)
                Frequency_PLL = 8000000;
                break;
            }
            case 0b01: { // running from HSE
#if TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0
                Frequency_PLL = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL;
#else
                ttc_assert_halt_origin( ttc_assert_origin_auto ); // running from HSE but TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL not defined. Define it in your makefile!
#endif
                break;
            }
            case 0b10: { // running from PLL: inspect prescalers to calculate current PLL output frequency
#ifdef STM32F10X_CL
                t_u8 PLL2_Multiplier_x2 = 0;
                t_u8 PLL2_Divider       = 0;
                if ( register_stm32f1xx_RCC.CFGR2.PREDIV1SRC ) { // PREDIV1 gets clock from PLL2 (only Connectivity Line Devices)
                    _sysclock_stm32f1xx_pll_read( 2, &PLL2_Multiplier_x2, &PLL2_Divider );
                    Frequency_PLL = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL
                                    / PLL2_Divider * PLL2_Multiplier_x2 / 2
                                    / PLL1_Divider * PLL1_Multiplier_x2 / 2;
                }
                else {                                             // PREDIV1 gets clock from HSE
                    Assert_SYSCLOCK( PLL1_Multiplier_x2 != 0, ttc_assert_origin_auto );  // invalid divider value!
                    if ( register_stm32f1xx_RCC.CFGR.PLLSRC ) { // PLL gets clock from PREDIV1
                        Assert_SYSCLOCK( PLL1_Divider != 0, ttc_assert_origin_auto );  // invalid divider value!
                        Frequency_PLL = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL / PLL1_Divider * PLL1_Multiplier_x2 / 2;
                    }
                    else {                                         // PLL gets clock from HSI/2
                        Frequency_PLL = TTC_SYSCLOCK_HSI_CLOCK / 2 * PLL1_Multiplier_x2 / 2;
                    }
                }
#else
                Assert_SYSCLOCK_EXTRA( PLL1_Multiplier_x2 != 0, ttc_assert_origin_auto );  // invalid divider value!
                if ( register_stm32f1xx_RCC.CFGR.PLLSRC ) { // PLL gets clock from PREDIV1
                    Assert_SYSCLOCK( PLL1_Divider != 0, ttc_assert_origin_auto );  // invalid divider value!
                    Frequency_PLL = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL / PLL1_Divider * PLL1_Multiplier_x2 / 2;
                    if ( register_stm32f1xx_RCC.CFGR.PLLXTPRE == 1 )
                    { Frequency_PLL /= 2; } // HSE is divided by 2
                }
                else {                                          // PLL gets clock from HSI/2
                    Frequency_PLL = TTC_SYSCLOCK_HSI_CLOCK / 2 * PLL1_Multiplier_x2 / 2;
                }
#endif
                break;
            }
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid/ unsupported register value! (-> RM0008 p.133)
        }

        Assert_SYSCLOCK_EXTRA( Frequency_PLL <= sysclock_stm32f1xx_MaxClock_SYSCLK, ttc_assert_origin_auto );
        Config->LowLevelConfig.frequency_pll = Frequency_PLL;
    }
    if ( MaxStage >= ssu_USBCLK )  { // calculate USB frequency
        // read divider_usb  (RM0008 p. 98, 132)
        t_u8 RegisterValue = register_stm32f1xx_RCC.CFGR.USBPRE;
        Assert_SYSCLOCK( RegisterValue < 2, ttc_assert_origin_auto );  // invalid/ unexpected register value
        Config->LowLevelConfig.divider_usb_x10 = ( RegisterValue ) ? 10 : 15; // 1.0 : 1.5

        Assert_SYSCLOCK( Config->LowLevelConfig.divider_usb_x10, ttc_assert_origin_auto );  // must not be zero!

        // Frequency USB = Frequency PLL / (divider_usb / 10.0)
        Config->LowLevelConfig.frequency_usb = Frequency_PLL * 10 / Config->LowLevelConfig.divider_usb_x10;
    }
    if ( MaxStage >= ssu_SYSCLK )  { // calculate SYSCLK frequency
        switch ( register_stm32f1xx_RCC.CFGR.SWS ) { // -> RM0008 p. 133
            case 0b00: // HSI oscillator used as system clock
                Config->SystemClockFrequency = TTC_SYSCLOCK_HSI_CLOCK;
                break;
            case 0b01: // HSE oscillator used as system clock
                Assert_SYSCLOCK( TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0, ttc_assert_origin_auto );  // define TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL in your board makefile!
                Config->SystemClockFrequency = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL;
                break;
            case 0b10: { // PLL used as system clock
                Config->SystemClockFrequency = Frequency_PLL;
                break;
            }
            default:
                ttc_assert_halt_origin( ttc_assert_origin_auto ); // not applicable
                return;
                break;
        }
        Assert_SYSCLOCK_EXTRA( Config->SystemClockFrequency <= sysclock_stm32f1xx_MaxClock_SYSCLK, ttc_assert_origin_auto );
    }
    if ( MaxStage >= ssu_AHBCLK )  { // calculate AHB clock frequency
        // read divider_ahb  (RM0008 p. 100, 133)
        t_u8 RegisterValue = register_stm32f1xx_RCC.CFGR.HPRE;
        Assert_SYSCLOCK( RegisterValue < 16, ttc_assert_origin_auto );  // invalid/ unexpected register value
        if ( RegisterValue > 7 )
        { Config->LowLevelConfig.divider_ahb = AHB_Dividers[RegisterValue - 7]; }
        else
        { Config->LowLevelConfig.divider_ahb = 1; }
        Assert_SYSCLOCK( Config->LowLevelConfig.divider_ahb, ttc_assert_origin_auto );     // must not be zero!

        Config->LowLevelConfig.frequency_ahb  =
            Config->SystemClockFrequency /
            Config->LowLevelConfig.divider_ahb;
    }
    if ( MaxStage >= ssu_APB1CLK ) { // calculate APB2 = PCLK2 clock frequency
        // read divider_apb1 (RM0008 p. 99)
        t_u8 RegisterValue = register_stm32f1xx_RCC.CFGR.PPRE1;
        Assert_SYSCLOCK( RegisterValue < 8, ttc_assert_origin_auto );  // invalid/ unexpected register value
        if ( RegisterValue > 3 )
        { Config->LowLevelConfig.divider_apb1 = ss_DividerAPB[RegisterValue - 4]; }
        else
        { Config->LowLevelConfig.divider_apb1 = 1; }
        Assert_SYSCLOCK( Config->LowLevelConfig.divider_apb1, ttc_assert_origin_auto );    // must not be zero!

        Config->LowLevelConfig.frequency_apb1 =
            Config->LowLevelConfig.frequency_ahb /
            Config->LowLevelConfig.divider_apb1;
    }
    if ( MaxStage >= ssu_APB2CLK ) { // calculate APB2 = PCLK2 clock frequency
        // read divider_apb2 (RM0008 p. 100)
        t_u8 RegisterValue = register_stm32f1xx_RCC.CFGR.PPRE2;
        Assert_SYSCLOCK( RegisterValue < 8, ttc_assert_origin_auto );  // invalid/ unexpected register value
        if ( RegisterValue > 3 )
        { Config->LowLevelConfig.divider_apb2 = ss_DividerAPB[RegisterValue - 4]; }
        else
        { Config->LowLevelConfig.divider_apb2 = 1; }
        Assert_SYSCLOCK( Config->LowLevelConfig.divider_apb2, ttc_assert_origin_auto );    // must not be zero!

        Config->LowLevelConfig.frequency_apb2 =
            Config->LowLevelConfig.frequency_ahb /
            Config->LowLevelConfig.divider_apb2;
    }
    if ( MaxStage >= ssu_ALL )     { // calculate all other frequencies

        if ( 1 ) { // calculate frequency_adc
            // read divider_adc  (RM0008 p. 99, 133)
            t_u8 RegisterValue = register_stm32f1xx_RCC.CFGR.ADCPRE;
            Assert_SYSCLOCK( RegisterValue < 4, ttc_assert_origin_auto );  // invalid/ unexpected register value
            Config->LowLevelConfig.divider_adc = ss_DividerADC[RegisterValue];
            Assert_SYSCLOCK( Config->LowLevelConfig.divider_adc, ttc_assert_origin_auto );     // must not be zero!

            Config->LowLevelConfig.frequency_adc =
                Config->LowLevelConfig.frequency_apb2 /
                Config->LowLevelConfig.divider_adc;
        }
        if ( 1 ) { // calculate frequency_timer1
            Config->LowLevelConfig.frequency_timer1 = Config->LowLevelConfig.frequency_apb2;
            if ( Config->LowLevelConfig.divider_apb2 > 1 )
            { Config->LowLevelConfig.frequency_timer1 *= 2; }
        }
        if ( 1 ) { // calculate frequency_timerX
            Config->LowLevelConfig.frequency_timerX = Config->LowLevelConfig.frequency_apb1;
            if ( Config->LowLevelConfig.divider_apb1 > 1 )
            { Config->LowLevelConfig.frequency_timerX *= 2; }
        }
        if ( 1 ) { // calculate frequency_rtc
            Config->LowLevelConfig.frequency_rtc = 0;
            if ( register_stm32f1xx_RCC.BDCR.RTC_EN ) {
                switch ( register_stm32f1xx_RCC.BDCR.RTCSEL ) {
                    case 0b00:  break; // No RTCCLOCK
                    case 0b01: {       // LSE
#ifdef TTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL
                        Config->LowLevelConfig.frequency_rtc = TTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL;
#endif
                        break;
                    }
                    case 0b10: {       // LSI
                        Config->LowLevelConfig.frequency_rtc = 40000;
                        break;
                    }
                    case 0b11: {       // HSE / 128
#ifdef TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL
                        if ( register_stm32f1xx_RCC.CR.HSERDY )
                        { Config->LowLevelConfig.frequency_rtc = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL / 128; }
#endif
                        break;
                    }
                    default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break;
                }
            }
        }
        if ( 1 ) { // calculate Config->LowLevelConfig.frequency_systick (SysTick Timer)
            if ( ! register_cortexm3_STK.CTRL.Bits.Enable )
            { Config->LowLevelConfig.frequency_systick = 0; } // SysTick is switched off
            else {
                // -> RM0008 p. 90
                if ( register_cortexm3_STK.CTRL.Bits.ClkSource ) // using processor clock
                { Config->LowLevelConfig.frequency_systick = Config->LowLevelConfig.frequency_ahb; }
                else                                          // using external clock = HCLK / 8
                { Config->LowLevelConfig.frequency_systick = Config->LowLevelConfig.frequency_ahb / 8; }
            }
        }

        // check for max frequencies (->RM0008 p.123)
        Assert_SYSCLOCK( Config->LowLevelConfig.frequency_adc  <= sysclock_stm32f1xx_MaxClock_ADC, ttc_assert_origin_auto );
        Assert_SYSCLOCK( Config->LowLevelConfig.frequency_apb1 <= sysclock_stm32f1xx_MaxClock_APB1, ttc_assert_origin_auto );
        Assert_SYSCLOCK( Config->LowLevelConfig.frequency_apb2 <= sysclock_stm32f1xx_MaxClock_APB2, ttc_assert_origin_auto );
        Assert_SYSCLOCK( Config->SystemClockFrequency          <= sysclock_stm32f1xx_MaxClock_SYSCLK, ttc_assert_origin_auto );
    }
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
