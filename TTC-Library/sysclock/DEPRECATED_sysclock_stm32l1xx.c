/** { sysclock_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  Low-Level driver for sysclock devices on stm32l1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 21 at 20140423 14:19:06 UTC
 *
 *  Note: See ttc_sysclock.h for description of stm32l1xx independent SYSCLOCK implementation.
 * 
 *  Authors: <AUTHOR>
}*/

#include "sysclock_stm32l1xx.h"

extern uint32_t SystemCoreClock;          /*!< System Clock Frequency (Core Clock) */

/** read PLL settings */
void _sysclock_stm32f1xx_pll_read(u8_t PLL_Index, u8_t* Multiplier, u8_t* Divisor);

#ifdef STM32F10X_CL  //  Connectivity line devices: reset and clock control (-> RM0038 p.132)
const u8_t PLL1_Divisor_Min    = 1;
const u8_t PLL1_Divisor_Max    = 16;
const u8_t PLL1_Multiplier_Min = 4;
const u8_t PLL1_Multiplier_Max = 9;

#else                //  Low-, medium-, high- and XL-density reset and clock control (-> RM0038 p.99)
const u8_t PLL1_Divisor_Min    = 1;
const u8_t PLL1_Divisor_Max    = 16;
const u8_t PLL1_Multiplier_Min = 2;
const u8_t PLL1_Multiplier_Max = 16;
#endif

//{ Global Variables ***********************************************************

//}Global Variables

extern uint32_t SystemCoreClock;          /*!< System Clock Frequency (Core Clock) */

//{ Function Definitions *******************************************************

ttc_sysclock_errorcode_e sysclock_stm32l1xx_deinit(ttc_sysclock_config_t* Config) {
    Assert_SYSCLOCK(Config, ec_NULL); // pointers must not be NULL


    return (ttc_sysclock_errorcode_e) 0;
}
ttc_sysclock_errorcode_e sysclock_stm32l1xx_get_features(ttc_sysclock_config_t* Config) {
    Assert_SYSCLOCK(Config, ec_NULL); // pointers must not be NULL


    return (ttc_sysclock_errorcode_e) 0;
}
ttc_sysclock_errorcode_e sysclock_stm32l1xx_init(ttc_sysclock_config_t* Config) {
    Assert_SYSCLOCK(Config, ec_NULL); // pointers must not be NULL


    return (ttc_sysclock_errorcode_e) 0;
}
ttc_sysclock_errorcode_e sysclock_stm32l1xx_load_defaults(ttc_sysclock_config_t* Config) {
    Assert_SYSCLOCK(Config, ec_NULL); // pointers must not be NULL


    return (ttc_sysclock_errorcode_e) 0;
}
void sysclock_stm32l1xx_prepare(ttc_sysclock_config_t* Config) {

    SystemInit(); // init STM32 standard peripherals library
    sysclock_stm32l1xx_reset(Config);
    _sysclock_get_board_info(Config);
    sysclock_stm32l1xx_profile_switch(Config);
}
void sysclock_stm32l1xx_reset(ttc_sysclock_config_t* Config) {
    Assert_SYSCLOCK(Config, ec_NULL);
    Config->Architecture                = ta_sysclock_stm32l1xx;
    Config->LowLevelConfig.BaseRegister = (SYSCLOCK_Register_t*) &register_stm32l1xx_RCC;
    Config->SystemClockFrequency        = 0;
    Config->Profile                     = tsp_Default;
}
void sysclock_stm32l1xx_profile_switch(ttc_sysclock_config_t* Config) {


    switch (Config->Profile) {
    case tsp_MaxSpeed:
    case tsp_LowEnergy:
    case tsp_SleepKeepRam:
    case tsp_Sleep:
    case tsp_Default:
    case tsp_HighSpeedInternal:
    {
        u32_t StartUpCounter=0, HSIStatus=0;
        /* Enable HSI */
        RCC->CR |= ((uint32_t)RCC_CR_HSION);

        /* Wait till HSI is ready and if Time out is reached exit */
        do
        {
          HSIStatus = RCC->CR & RCC_CR_HSIRDY;
          StartUpCounter++;
        } while((HSIStatus == 0) && (StartUpCounter != HSI_STARTUP_TIMEOUT));

        if ((RCC->CR & RCC_CR_HSIRDY) != RESET)
        {
          HSIStatus = (uint32_t)0x01;
        }
        else
        {
          HSIStatus = (uint32_t)0x00;
        }
        /* Select HSI as system clock source */
        RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW));
        RCC->CFGR |= (uint32_t)RCC_CFGR_SW_HSI;

        /* Wait till HSI is used as system clock source */
        while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS) != (uint32_t)RCC_CFGR_SWS_HSI)
        {
        }
        break;
    }
    default:

        SystemCoreClockUpdate();
        Config->SystemClockFrequency = SystemCoreClock;
        break;
    }
}
void sysclock_stm32l1xx_udelay(Base_t Microseconds) {


  volatile Base_signed_t Remaining = abs( (Base_signed_t) Microseconds );
  Remaining = (Remaining - 1) * 599 / 100;
  for (; Remaining > 0; Remaining--);

}
void sysclock_stm32l1xx_update(ttc_sysclock_config_t* Config) {
    Assert_SYSCLOCK(Config, ec_NULL); // pointers must not be NULL



}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} Function definitions
//{ private functions (ideally) ************************************************

void _sysclock_stm32l1xx_ahb_prescaler(u16_t Divisor) {
    switch (Divisor) { // -> RM0038 p.100
    case   1: register_stm32l1xx_RCC.RCC_CFGR.HPRE = 0b0000; break;
    case   2: register_stm32l1xx_RCC.RCC_CFGR.HPRE = 0b1000; break;
    case   4: register_stm32l1xx_RCC.RCC_CFGR.HPRE = 0b1001; break;
    case   8: register_stm32l1xx_RCC.RCC_CFGR.HPRE = 0b1010; break;
    case  16: register_stm32l1xx_RCC.RCC_CFGR.HPRE = 0b1011; break;
    case  32: register_stm32l1xx_RCC.RCC_CFGR.HPRE = 0b1100; break; // <- same as for 64!
    case  64: register_stm32l1xx_RCC.RCC_CFGR.HPRE = 0b1100; break;
    case 128: register_stm32l1xx_RCC.RCC_CFGR.HPRE = 0b1101; break;
    case 256: register_stm32l1xx_RCC.RCC_CFGR.HPRE = 0b1110; break;
    case 512: register_stm32l1xx_RCC.RCC_CFGR.HPRE = 0b1111; break;
    default: Assert_Halt_EC(ec_InvalidConfiguration); break;
    }
}
void _sysclock_stm32l1xx_pll_configure(u8_t PLL_Index, u8_t Multiplier, u8_t Divisor) {
    switch (PLL_Index) {
    case 1: {
        //  Low-, medium-, high- and XL-density reset and clock control (-> RM0038 p.99)
        Assert( (Multiplier > 1) && (Multiplier < 18), ec_InvalidConfiguration);
        Assert( (Divisor  < 17), ec_InvalidConfiguration);

        u8_t Value_SWS = register_stm32l1xx_RCC.RCC_CFGR.SWS;
        if (Value_SWS == 0b10) { // System Clock is using this PLL: switch to another source
            if (register_stm32l1xx_RCC.RCC_CR.HSIRDY) {
                register_stm32l1xx_RCC.RCC_CFGR.SW = 0x00; // select HSI as system clock
                while (register_stm32l1xx_RCC.RCC_CFGR.SWS != 0x00); // wait for switch to happen
            }
        }
        register_stm32l1xx_RCC.RCC_CR.PLLON         = 0; // switch of PLL to allow reconfiguration
        while (register_stm32l1xx_RCC.RCC_CR.PLLRDY); // wait for PLL to shut down

        register_stm32l1xx_RCC.RCC_CFGR.PLLSRC   = 1;    // HSE as PLL input source

        register_stm32l1xx_RCC.RCC_CFGR.PLLMUL      = Multiplier - 2;
        register_stm32l1xx_RCC.RCC_CR.PLLON         = 1; // switch back on PLL
        while (!register_stm32l1xx_RCC.RCC_CR.PLLRDY); // wait for PLL to lock

        register_stm32l1xx_RCC.RCC_CFGR.SW = Value_SWS; // select HSI as system clock
        while (register_stm32l1xx_RCC.RCC_CFGR.SWS != Value_SWS); // wait for switch to happen

        break;
    }

    default: Assert_Halt_EC(ec_InvalidConfiguration); break;
    }

}
void _sysclock_stm32l1xx_apb_prescaler(u8_t BusIndex, u8_t Divisor) {
    switch (BusIndex) {
    case 1: {
        switch (Divisor) { // -> RM0038 p.10
        case   1: register_stm32l1xx_RCC.RCC_CFGR.PPRE1 = 0b000; break;
        case   2: register_stm32l1xx_RCC.RCC_CFGR.PPRE1 = 0b100; break;
        case   4: register_stm32l1xx_RCC.RCC_CFGR.PPRE1 = 0b101; break;
        case   8: register_stm32l1xx_RCC.RCC_CFGR.PPRE1 = 0b110; break;
        case  16: register_stm32l1xx_RCC.RCC_CFGR.PPRE1 = 0b111; break;
        default: Assert_Halt_EC(ec_InvalidConfiguration); break;
        }
        break;
    }
    case 2: {
        switch (Divisor) { // -> RM0038 p.100
        case   1: register_stm32l1xx_RCC.RCC_CFGR.PPRE2 = 0b000; break;
        case   2: register_stm32l1xx_RCC.RCC_CFGR.PPRE2 = 0b100; break;
        case   4: register_stm32l1xx_RCC.RCC_CFGR.PPRE2 = 0b101; break;
        case   8: register_stm32l1xx_RCC.RCC_CFGR.PPRE2 = 0b110; break;
        case  16: register_stm32l1xx_RCC.RCC_CFGR.PPRE2 = 0b111; break;
        default: Assert_Halt_EC(ec_InvalidConfiguration); break;
        }
        break;
    }
    }

}
u32_t _sysclock_stm32l1xx_set_max_speed(stm32_clock_source_e Source, u32_t Frequency, u16_t FrequencyMultiplier, u16_t FrequencyDivider) {
    u32_t SystemClockFrequency = Frequency * FrequencyMultiplier / FrequencyDivider;
    u8_t OriginalClockSwitch = register_stm32l1xx_RCC.RCC_CFGR.SWS;

/** Settings after SystemInit()

(gdb) p register_stm32l1xx_RCC.RCC_CR
$1 = {HSION = 1, HSIRDY = 1, reserved1 = 0, HSITRIM = 16, HSICAL = 86, HSEON = 1, HSERDY = 1,
      HSEBYP = 0, CSSON = 0, reserved2 = 0, PLLON = 1, PLLRDY = 1, PLL2ON = 1, PLL2RDY = 1,
      PLL3ON = 0, PLL3RDY = 0, reserved3 = 0}
(gdb) p register_stm32l1xx_RCC.RCC_CFGR
$2 = {SW = 2, SWS = 2, HPRE = 0, PPRE1 = 4, PPRE2 = 0, ADCPRE = 0, PLLSRC = 1, PLLXTPRE = 0,
      PLLMUL = 7, USBPRE = 0, reserved1 = 0, MCO = 7, reserved2 = 0}
(gdb) p register_stm32l1xx_RCC.RCC_CFGR2
$3 = {PREDIV1 = 0, PREDIV2 = 0, PLL2MUL = 0, PLL3MUL = 0, PREDIV1SRC = 0, I2S2SRC = 0, I2S3SRC = 1, reserved1 = 200}
*/
    switch(Source) {
    case scs_ExternalCrystal: {
        if (OriginalClockSwitch) { // running external clock as system clock
            while (!register_stm32l1xx_RCC.RCC_CR.HSIRDY) // switch on internal high speed clock
                register_stm32l1xx_RCC.RCC_CR.HSION = 1;
            while (!register_stm32l1xx_RCC.RCC_CFGR.SW)   // switch to internal high speed clock
                register_stm32l1xx_RCC.RCC_CFGR.SW = 0;
        }

        u8_t APB_Prescaler = 1;
        while (SystemClockFrequency / APB_Prescaler > 36000000) // ensure that APB1 bus is clocked < 36MHz (RM0038 p.100)
            APB_Prescaler *= 2;
        _sysclock_stm32l1xx_apb_prescaler(1, APB_Prescaler);

        _sysclock_stm32l1xx_apb_prescaler(2, 1); // configure APB bus for max speed
        _sysclock_stm32l1xx_ahb_prescaler(1);    // configure AHB bus for max speed

        // enable PLL (RM0038 p.96-100)
        Assert(FrequencyMultiplier >  1, ec_InvalidArgument);
        Assert(FrequencyMultiplier < 16, ec_InvalidArgument);
        Assert(FrequencyDivider    >  0, ec_InvalidArgument);
        Assert(FrequencyDivider    <  3, ec_InvalidArgument);

        _sysclock_stm32l1xx_pll_configure(1, 9, 2);    // configure PLL1 for 4/2 frequency multiplier
        register_stm32l1xx_RCC.RCC_CFGR.HPRE = 0;      // reset AHB Prescaler
        while (!register_stm32l1xx_RCC.RCC_CR.PLLRDY); // wait for PLL to lock in
        register_stm32l1xx_RCC.RCC_CFGR.SW = 2;        // swicth SYSCLK to PLL

        break;
    }
    default: Assert_Halt_EC(ec_NotImplemented); break;
    }
    return SystemClockFrequency;
}
void _sysclock_get_board_info(ttc_sysclock_config_t* Config){
#ifdef TTC_SYSCLOCK_HSI
Config->Profile = tsp_HighSpeedInternal;
#endif
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
