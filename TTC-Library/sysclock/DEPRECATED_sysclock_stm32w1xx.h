#ifndef SYSCLOCK_STM32W1XX_H
#define SYSCLOCK_STM32W1XX_H

/** { sysclock_stm32w1xx.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  Low-Level driver for sysclock devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by high-level sysclock and application.
 *
 *  Created from template device_architecture.h revision 21 at 20140222 19:41:29 UTC
 *
 *  Note: See ttc_sysclock.h for description of stm32w1xx independent SYSCLOCK implementation.
 *  
 *  Authors: <AUTHOR>
 *
}*/
//{ Defines/ TypeDefs ****************************************************

#define StStatus u8_t   // required by phy-library.h
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "sysclock_stm32w1xx_types.h"
#include "../ttc_sysclock_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_sysclock_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_<device>_prepare
//
#define ttc_driver_sysclock_prepare(Config)        sysclock_stm32w1xx_prepare(Config)
#define ttc_driver_sysclock_profile_switch(Config) sysclock_stm32w1xx_profile_switch(Config)
#define ttc_driver_sysclock_reset(Config)          sysclock_stm32w1xx_reset(Config)
#define ttc_driver_sysclock_udelay(Microseconds)   sysclock_stm32w1xx_udelay(Microseconds)
#define ttc_driver_sysclock_update(Config) sysclock_stm32w1xx_update(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_sysclock.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_sysclock.h for details.
 *
 * You normally should not need to add functions here. 
 * Add your private functions in the private section below. 
 */


/** Prepares sysclock driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void sysclock_stm32w1xx_prepare(ttc_sysclock_config_t* Config);


/** Switches current system clock profile.
 *
 * Each architecture and board may provide its own clocking profile implementation.
 *
 * @param NewProfile  new clocking profile to switch to
 * @param Config   = 
 */
void sysclock_stm32w1xx_profile_switch(ttc_sysclock_config_t* Config);


/** Reset given configuration according to current hardware platform
 *
 * @param Config        pointer to struct ttc_sysclock_config_t (must have valid value for LogicalIndex)
 */
void sysclock_stm32w1xx_reset(ttc_sysclock_config_t* Config);


/** Busy waits for given amount of microseconds.
 *
 * Note: This wait function has to be calibrated for each target platform and clocking profile.
 *
 * @param Microseconds  time to wait (for-loop)
 */
void sysclock_stm32w1xx_udelay(Base_t Microseconds);


/** Updates given Config with current hardware settings.
 *
 * Hardware configuration may have changed by third party libraries.
 * Low-Level driver will read current register settings and update Config accordingly.
 *
 * @param Config  Referenced configuration will be changed.
 */
void sysclock_stm32w1xx_update(ttc_sysclock_config_t* Config);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _sysclock_stm32w1xx_foo(ttc_sysclock_config_t* Config)

//InsertPrivatePrototypes above (DO NOT DELETE THIS LINE!)

//}private functions

#endif //SYSCLOCK_STM32W1XX_H
