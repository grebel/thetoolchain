/** { sysclock_stm32w1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  Low-Level driver for sysclock devices on stm32w1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 20 at 20140222 19:41:29 UTC
 *
 *  Note: See ttc_sysclock.h for description of stm32w1xx independent SYSCLOCK implementation.
 * 
 *  Authors: <AUTHOR>
}*/

#include "sysclock_stm32w1xx.h"
#include "phy-library.h"
#include "micro-common.h"
#include "mfg-token.h"
#include "../ttc_interrupt.h"
#include "../register/register_stm32w1xx.h"

//{ Function definitions *******************************************************

void sysclock_stm32w1xx_prepare(ttc_sysclock_config_t* Config) {
    Assert_SYSCLOCK(Config, ec_NULL); // pointers must not be NULL

    if (1) { // initialize Hardware Abstraction Layer (HAL) library
        halInit(); // init EM357 hardware abstraction library

        if (1) { ttc_interrupt_all_enable(); }
        else   { INTERRUPTS_ON(); }
    }
    sysclock_stm32w1xx_reset(Config);
}
void sysclock_stm32w1xx_profile_switch(ttc_sysclock_config_t* Config) {
    Assert_SYSCLOCK(Config, ec_NULL); // pointers must not be NULL

    // ToDo: implement profile switch

}
void sysclock_stm32w1xx_reset(ttc_sysclock_config_t* Config) {
    Assert_SYSCLOCK(Config, ec_NULL); // pointers must not be NULL

    // ToDo: implement reset

}
inline void sysclock_stm32w1xx_udelay(Base_t Microseconds) {

    // Hand tuned delay calibrated on STM32W108 @ 24MHz
    // minimum delay: 9 us for Microseconds = 0
    // minimum usable delay: 20 us
    volatile Base_signed_t Remaining = (Microseconds - 9 ) * 100000 / 213864;
    for (; Remaining > 0; Remaining--)
        Remaining = Remaining;
}
void sysclock_stm32w1xx_update(ttc_sysclock_config_t* Config) {
    Assert_SYSCLOCK(Config, ec_NULL); // pointers must not be NULL

    if (register_stm32w1xx_OSC24M_CTRL.bSelect)
        Config->SystemClockFrequency = 24000000; // 24MHz Oscillator selected
    else
        Config->SystemClockFrequency = 12433734; // 12 MHz internal RC Oscillator
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} Function definitions
//{ private functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
