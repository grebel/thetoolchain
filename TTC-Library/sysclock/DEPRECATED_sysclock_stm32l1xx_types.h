#ifndef SYSCLOCK_STM32L1XX_TYPES_H
#define SYSCLOCK_STM32L1XX_TYPES_H

/** { sysclock_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *                     
 *  Low-Level datatypes for SYSCLOCK devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_sysclock_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140423 14:19:06 UTC
 *
 *  Note: See ttc_sysclock.h for description of architecture independent SYSCLOCK implementation.
 * 
 *  Authors: <AUTHOR>
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef enum { // stm32_clock_source_e  (->RM0008 p.91)
    scs_None,
    scs_ExternalClock,     // input from external clock generator
    scs_ExternalCrystal,   // input from external crystal
    scs_InternalHighSpeed, // internal high speed clock
    scs_InternalLowSpeed,  // internal low speed clock
    svs_External_32kHz,    // precise external 32768 Hz crystal
    scs_Unknown            // no items below

} stm32_clock_source_e;

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_sysclock_types.h *************************

typedef struct { // register description (adapt according to stm32l1xx registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} SYSCLOCK_Register_t;

typedef struct {  // stm32l1xx specific configuration data of single SYSCLOCK device
  SYSCLOCK_Register_t* BaseRegister;       // base address of SYSCLOCK device registers
} __attribute__((__packed__)) sysclock_stm32l1xx_config_t;

// ttc_sysclock_architecture_t is required by ttc_sysclock_types.h
#define ttc_sysclock_architecture_t sysclock_stm32l1xx_config_t

//} Structures/ Enums


#endif //SYSCLOCK_STM32L1XX_TYPES_H
