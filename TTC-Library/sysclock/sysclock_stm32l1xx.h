#ifndef SYSCLOCK_STM32L1XX_H
#define SYSCLOCK_STM32L1XX_H

/** { sysclock_stm32l1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for sysclock devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by high-level sysclock and application.
 *
 *  Created from template device_architecture.h revision 22 at 20141106 14:10:16 UTC
 *
 *  Note: See ttc_sysclock.h for description of stm32l1xx independent SYSCLOCK implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_SYSCLOCK_STM32L1XX
//
// Implementation status of low-level driver support for sysclock devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_SYSCLOCK_STM32L1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_SYSCLOCK_STM32L1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_SYSCLOCK_STM32L1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_SYSCLOCK_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "sysclock_stm32l1xx.c"
//
#include "sysclock_stm32l1xx_types.h"
#include "../ttc_sysclock_types.h"
#include "../ttc_gpio_types.h"
#include "../register/register_stm32l1xx.h"
#include "stm32l1xx.h"
#include "stm32l1xx_pwr.h"
#include "system_stm32l1xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_sysclock_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_sysclock_foo
//
#define ttc_driver_sysclock_prepare(Config)                    sysclock_stm32l1xx_prepare(Config)
#define ttc_driver_sysclock_profile_switch(Config, NewProfile) sysclock_stm32l1xx_profile_switch(Config, NewProfile)
#define ttc_driver_sysclock_reset(Config)                      sysclock_stm32l1xx_reset(Config)
#define ttc_driver_sysclock_udelay(Config, Microseconds)       sysclock_stm32l1xx_udelay(Config, Microseconds)
#define ttc_driver_sysclock_update(Config)                     sysclock_stm32l1xx_update(Config)
#define ttc_driver_sysclock_enable_oscillator(Oscillator, On)  sysclock_stm32l1xx_enable_oscillator(Oscillator, On)
#define ttc_driver_sysclock_frequency_get_all                  sysclock_stm32l1xx_frequency_get_all
#define ttc_driver_sysclock_frequency_set                      sysclock_stm32l1xx_frequency_set
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_sysclock.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_sysclock.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Prepares sysclock driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 * @param Config      pointer to struct t_ttc_sysclock_config (field Profile stores the current profile)
 */
void sysclock_stm32l1xx_prepare( t_ttc_sysclock_config* Config );



/** Reset given configuration according to current hardware platform
 *
 * @param Config        pointer to struct t_ttc_sysclock_config (must have valid value for LogicalIndex)
 */
void sysclock_stm32l1xx_reset( t_ttc_sysclock_config* Config );


/** Busy waits for given amount of microseconds.
 *
 * Note: This wait function has to be calibrated for each target platform and clocking profile.
 *
 * @param Microseconds  time to wait (for-loop)
 * @param Config   =
 */
void sysclock_stm32l1xx_udelay( t_ttc_sysclock_config* Config, t_base Microseconds );


/** Updates given Config with current hardware settings.
 *
 * Hardware configuration may have changed by third party libraries.
 * Low-Level driver will read current register settings and update Config accordingly.
 *
 * @param Config  Referenced configuration will be changed.
 */
void sysclock_stm32l1xx_update( t_ttc_sysclock_config* Config );


/** Switches single oscillator or clock input on or off
 *
 * @param Oscillator (e_ttc_sysclock_oscillator)  Not all oscillators may be available on current architecture
 * @param On         (BOOL)                       ==0: switch off oscillator, !=0: switch on oscillator
 * @return           (e_ttc_sysclock_errorcode)   ==0: switch was successfull, error code otherwise
 */
e_ttc_sysclock_errorcode sysclock_stm32l1xx_enable_oscillator( e_ttc_sysclock_oscillator Oscillator, BOOL On );


/** Returns a list of currently available system clock frequencies
 *
 * The current low-level driver knows which frequencies are available on current hardware.
 * Format of return array is check at system boot by ttc_sysclock_prepare():
 * - Array contains at least one non-zero entry.
 * - No double entries are allowed.
 * - Values are increasing with increasing array index.
 * - Last array entry is zero (0) to terminate list.
 *
 * @return (base_t*)  zero terminated array of valid, increasing clock frequencies for ttc_sysclock_set_frequency()
 */
const t_base* sysclock_stm32l1xx_frequency_get_all( t_ttc_sysclock_config* Config );


/** Set new system clock frequency.
 *
 * Note: Depending on current hardware configuration and ttc_sysclock configuration, different clock frequencies may be available.
 *       The low-level driver will choose the best matching frequency.
 *
 * @param NewFrequency (t_base)  new frequency (Hz) should be taken from list of available frequencies as returned by ttc_sysclock_frequency_get_all()
 * @param Config   =
 */
void sysclock_stm32l1xx_frequency_set( t_ttc_sysclock_config* Config, t_base NewFrequency );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes


//}Global Variables
//{ Function Definitions *******************************************************

#endif
