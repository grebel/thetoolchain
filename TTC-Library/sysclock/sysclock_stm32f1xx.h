#ifndef ARCHITECTURE_SYSCLOCK_H
#define ARCHITECTURE_SYSCLOCK_H

/** { sysclock_stm32f1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for sysclock devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by high-level sysclock and application.
 *
 *  Created from template device_architecture.h revision 21 at 20140218 17:32:51 UTC
 *
 *  Note: See ttc_sysclock.h for description of stm32f1xx independent SYSCLOCK implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_SYSCLOCK_STM32F1XX
//
// Implementation status of low-level driver support for sysclock devices on stm32f1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_SYSCLOCK_STM32F1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_SYSCLOCK_STM32F1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_SYSCLOCK_STM32F1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_SYSCLOCK_STM32F1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)


//} Defines
//{ Includes *************************************************************

#include "../ttc_cpu_types.h"
#include "sysclock_stm32f1xx_types.h"
#include "../ttc_sysclock_types.h"
#include "../register/register_stm32f1xx.h"
#include "stm32f10x.h"
#include "system_stm32f10x.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_sysclock_interface.h
#define ttc_driver_sysclock_prepare(Config)                   sysclock_stm32f1xx_prepare(Config)
#define ttc_driver_sysclock_reset(Config)                     sysclock_stm32f1xx_reset(Config)
#define ttc_driver_sysclock_profile_switch(Config,NewProfile) sysclock_stm32f1xx_profile_switch(Config,NewProfile)
#define ttc_driver_sysclock_udelay(Config,Microseconds)       sysclock_stm32f1xx_udelay(Config,Microseconds)
#define ttc_driver_sysclock_update(Config)                    sysclock_stm32f1xx_update(Config)
#define ttc_driver_sysclock_enable_oscillator(Oscillator, On) sysclock_stm32f1xx_enable_oscillator(Oscillator, On)
#define ttc_driver_sysclock_frequency_get_all                 sysclock_stm32f1xx_frequency_get_all
#define ttc_driver_sysclock_frequency_set                     sysclock_stm32f1xx_frequency_set
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_sysclock.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_sysclock.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */



/** Prepares sysclock driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 *
 * @param Config        pointer to struct t_ttc_sysclock_config
 */
void sysclock_stm32f1xx_prepare( t_ttc_sysclock_config* Config );


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_sysclock_config
 */
void sysclock_stm32f1xx_reset( t_ttc_sysclock_config* Config );

/** Switches current system clock profile.
 *
 * Each architecture and board may provide its own clocking profile implementation.
 *
 * @param Config      pointer to struct t_ttc_sysclock_config (field Profile stores the current profile)
 * @param NewProfile  new clocking profile to switch to
 * @return Config->Profile is loaded with updated clocking profile (unchanged if NewProfile is not available on current architecture)
 */
void sysclock_stm32f1xx_profile_switch( t_ttc_sysclock_config* Config, e_ttc_sysclock_profile NewProfile );


/** Busy waits for given amount of microseconds.
 *
 * Note: This wait function has to be calibrated for each target platform.
 *
 * @param Microseconds  time to wait (for-loop)
 */
void sysclock_stm32f1xx_udelay( t_ttc_sysclock_config* Config, t_base Microseconds );


typedef enum { // clock tree update stages
    ssu_None,     // invalid value
    ssu_PLLCLK,   // ..->PLLCLK
    ssu_USBCLK,   // --->PLLCLK->USBCLK
    ssu_SYSCLK,   // --->PLLCLK->SYSCLK
    ssu_AHBCLK,   // --->PLLCLK->SYSCLK->AHBCLK
    ssu_APB1CLK,  // --->PLLCLK->SYSCLK->AHBCLK->APB1CLK
    ssu_APB2CLK,  // --->PLLCLK->SYSCLK->AHBCLK->APB2CLK
    ssu_ALL,      // --->PLLCLK->SYSCLK->AHBCLK->APB2CLK->All other clocks + Assert checks
    ssu_unknown   // invalid value
} ssu_MaxStage;

/** Updates frequencies of clock tree levels from current register settings.
 *
 *
 * @param Config               Config->LowLevelConfig will be updated with calculated frequencies
 */
void sysclock_stm32f1xx_update( t_ttc_sysclock_config* Config );


/** Switches single oscillator or clock input on or off
 *
 * @param Oscillator (e_ttc_sysclock_oscillator)  Not all oscillators may be available on current architecture
 * @param On         (BOOL)                       ==0: switch off oscillator, !=0: switch on oscillator
 * @return           (e_ttc_sysclock_errorcode)   ==0: switch was successfull, error code otherwise
 */
e_ttc_sysclock_errorcode sysclock_stm32f1xx_enable_oscillator( e_ttc_sysclock_oscillator Oscillator, BOOL On );



/** Returns a list of currently available system clock frequencies
 *
 * The current low-level driver knows which frequencies are available on current hardware.
 * Format of return array is check at system boot by ttc_sysclock_prepare():
 * - Array contains at least one non-zero entry.
 * - No double entries are allowed.
 * - Values are increasing with increasing array index.
 * - Last array entry is zero (0) to terminate list.
 *
 * @return (base_t*)  zero terminated array of valid, increasing clock frequencies for ttc_sysclock_set_frequency()
 * @param Config   =
 */
const t_base* sysclock_stm32f1xx_frequency_get_all( t_ttc_sysclock_config* Config );


/** Set new system clock frequency.
 *
 * Note: Depending on current hardware configuration and ttc_sysclock configuration, different clock frequencies may be available.
 *       The low-level driver will choose the best matching frequency.
 *
 * @param NewFrequency (t_base)  new frequency (Hz) should be taken from list of available frequencies as returned by ttc_sysclock_frequency_get_all()
 * @param Config   =
 */
void sysclock_stm32f1xx_frequency_set( t_ttc_sysclock_config* Config, t_base NewFrequency );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

//}PrivateFunctions

#endif //ARCHITECTURE_SYSCLOCK_H
