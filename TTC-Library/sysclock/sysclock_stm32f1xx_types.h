#ifndef STM32F1XX_SYSCLOCK_TYPES_H
#define STM32F1XX_SYSCLOCK_TYPES_H

/** { sysclock_stm32f1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for SYSCLOCK devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by ttc_sysclock_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140218 17:57:21 UTC
 *
 *  Note: See ttc_sysclock.h for description of architecture independent SYSCLOCK implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ******************************************************

// frequency of internal high-speed clock
#define TTC_SYSCLOCK_HSI_CLOCK 8000000

#if TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0
    // calculate according to clock tree: HSE->PLL2->PLL1->SYSCLK (-> RM0008 p.123)
    #ifdef STM32F10X_CL
        #define TTC_SYSCLOCK_FREQUENCY_MIN (TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL / 16 * 8 / 16 * 4)
    #else
        #define TTC_SYSCLOCK_FREQUENCY_MIN (TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL / 2)
    #endif
    #define TTC_SYSCLOCK_FREQUENCY_MAX  TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL * 9 // highest stable system clock frequency
    #if TTC_SYSCLOCK_FREQUENCY_MAX > 72000000 // limit to 72MHz
        #undef TTC_SYSCLOCK_FREQUENCY_MAX
        #define TTC_SYSCLOCK_FREQUENCY_MAX 72000000
    #endif
#else
    // only internal highspeed RC-clock available
    #ifdef STM32F10X_CL
        #define TTC_SYSCLOCK_FREQUENCY_MIN 8000000
        #define TTC_SYSCLOCK_FREQUENCY_MAX 36000000 // highest reachable system clock frequency = 8 MHz / 2 * 9
    #else
        #define TTC_SYSCLOCK_FREQUENCY_MIN 8000000
        #define TTC_SYSCLOCK_FREQUENCY_MAX 64000000 // highest reachable system clock frequency = 8 MHz / 2 * 16
    #endif
#endif

// Define constant for STM StdPeripheral Library
#ifndef HSE_VALUE
    #define HSE_VALUE TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL
#endif

#ifndef TTC_SYSCLOCK1   // device not defined in makefile
    #define TTC_SYSCLOCK1    E_ttc_sysclock_architecture_stm32f1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef enum { // e_stm32_clock_source  (->RM0008 p.91)
    scs_None,
    scs_ExternalClock,     // input from external clock generator
    scs_ExternalCrystal,   // input from external crystal
    scs_InternalHighSpeed, // internal high speed clock
    scs_InternalLowSpeed,  // internal low speed clock
    svs_External_32kHz,    // precise external 32768 Hz crystal
    scs_unknown            // no items below

} e_stm32_clock_source;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)


//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic_types.h"
#include "../register/register_stm32f1xx_types.h"
#include "../ttc_cpu_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_sysclock_types.h ***********************

typedef struct {  // stm32f1xx specific configuration data of single SYSCLOCK device (-> RM0008 p.123)
    volatile t_register_stm32f1xx_rcc* BaseRegister;     // base address of SYSCLOCK device registers
    t_base frequency_adc;     // frequency of ADCCLK
    t_base frequency_rtc;     // frequency of RTCCLK (realtime clock)
    t_base frequency_pll;     // frequency of PLLCLK (input to USB prescaler)
    t_base frequency_ahb;    // frequency of AHB  (HCLK)
    t_base frequency_apb1;   // frequency of APB1 (PCLK1)
    t_base frequency_apb2;   // frequency of APB2 (PCLK2)
    t_base frequency_systick; // frequency of Systick Timer
    t_base frequency_timer1;  // frequency of TIM1
    t_base frequency_timerX;  // frequency of TIM2,3,4,5,6,7
    t_base frequency_usb;     // frequency of Universal Serial Bus Interface

    t_u8  divider_hse;        // frequency divider configured for HSE prescaler     (input is OSC_IN clock)
    t_u16 divider_ahb;        // frequency divider configured for AHB prescaler     (input is SYSCLOCK)
    t_u8  divider_adc;        // frequency divider configured for ADC prescaler     (input is AHB clock)
    t_u8  divider_apb1;       // frequency divider configured for APB1 prescaler    (input is AHB clock)
    t_u8  divider_apb2;       // frequency divider configured for APB2 prescaler    (input is AHB clock)
    t_u8  divider_usb_x10;    // frequency divider x 10       for USB               (input is PLL clock)

    e_stm32_clock_source SystemClockSource; // clock source of System Clock
} __attribute__( ( __packed__ ) ) t_sysclock_stm32f1xx_config;

// t_ttc_sysclock_architecture is required by ttc_sysclock_types.h
#define t_ttc_sysclock_architecture t_sysclock_stm32f1xx_config

//} Structures/ Enums


#endif //ARCHITECTURE_SYSCLOCK_TYPES_H
