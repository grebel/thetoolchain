#ifndef SYSCLOCK_STM32L1XX_TYPES_H
#define SYSCLOCK_STM32L1XX_TYPES_H

/** { sysclock_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for SYSCLOCK devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_sysclock_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20141106 14:10:16 UTC
 *
 *  Note: See ttc_sysclock.h for description of architecture independent SYSCLOCK implementation.
 *
 *  Authors: Adrián Romero
 *
}*/


#include "stm32l1xx.h"

//{ Defines/ TypeDefs **********************************************************

#define TTC_SYSCLOCK_INTERNAL_HIGHSPEED_CLOCK   16000000
#define TTC_SYSCLOCK_INTERNAL_LOWSPEED_CLOCK    37000
#define TTC_SYSCLOCK_MULTISPEED_CLOCK           524288

#include "../register/register_stm32l1xx_types.h"

// this uC can always use full range of clock frequency from internal clocks
#define TTC_SYSCLOCK_FREQUENCY_MAX  32000000 // highest stable system clock frequency

#define TTC_SYSCLOCK_FREQUENCY_MIN  32768    // lowest stable system clock frequency (running from MSI)

#ifndef TTC_SYSCLOCK1   // device not defined in makefile
    #define TTC_SYSCLOCK1    E_ttc_sysclock_architecture_stm32l1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef enum { // e_stm32_clock_source  (->RM0008 p.91)
    scs_None,
    scs_ExternalClock,     // input from external clock generator
    scs_ExternalCrystal,   // input from external crystal
    scs_InternalHighSpeed, // internal high speed clock
    scs_MultiSpeedClock,   // special multipurpose internal clock (e.g. STM32L1xx)
    scs_InternalLowSpeed,  // internal low speed clock
    svs_External_32kHz,    // precise external 32768 Hz crystal
    scs_unknown            // no items below

} e_stm32_clock_source;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_sysclock_types.h *************************

typedef volatile t_register_stm32l1xx_rcc t_sysclock_register;


typedef enum { // clock source of SYSCLK line
    ssss_MSI = 0b00, // Multi Speed Internal Clock
    ssss_HSI = 0b01, // High Speed Internal Clock
    ssss_HSE = 0b10, // High Speed External Clock or Crystal
    ssss_PLL = 0b11, // PLL generated Clock
    ssss_unknown
} e_sysclock_stm32l1xx_sysclock_source;

typedef enum { // clock source of MCO output pin
    ssms_SYSCLK = 0b001,
    ssms_HSE    = 0b100,
    ssms_MSI    = 0b011,
    ssms_PLL    = 0b101,
    ssms_HSI    = 0b010,
    ssms_LSI    = 0b110,
    ssms_LSE    = 0b111,
    ssms_unknown
} e_sysclock_stm32l1xx_mco_source;

typedef struct {  // stm32l1xx specific configuration data of single SYSCLOCK device
    t_sysclock_register* BaseRegister;       // base address of SYSCLOCK device registers

    //t_u32 sysclk_frequency;  // SYSCLOCK (stored in  ttc_sysclock_types.h:t_ttc_sysclock_config.SystemClockFrequency)
    t_u32                frequency_ahb;     // AHB-Bus, Core, Memory, DMA
    t_u32                frequency_apb1;    // APB1 bus peripherals
    t_u32                frequency_apb2;    // APB2 bus peripherals
    t_u32                frequency_rtc;     // RTC/LCD clock frequency
    t_u32                frequency_systick; // Systick Timer frequency
    e_stm32_clock_source SysclockSource;    // clock generator used to drive system clock
} __attribute__( ( __packed__ ) ) t_sysclock_stm32l1xx_config;

// t_ttc_sysclock_architecture is required by ttc_sysclock_types.h
#define t_ttc_sysclock_architecture t_sysclock_stm32l1xx_config

//} Structures/ Enums


#endif //SYSCLOCK_STM32L1XX_TYPES_H
