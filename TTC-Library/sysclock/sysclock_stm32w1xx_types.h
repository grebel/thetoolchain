#ifndef SYSCLOCK_STM32W1XX_TYPES_H
#define SYSCLOCK_STM32W1XX_TYPES_H

/** { sysclock_stm32w1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for SYSCLOCK devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by ttc_sysclock_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20141106 14:10:34 UTC
 *
 *  Note: See ttc_sysclock.h for description of architecture independent SYSCLOCK implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#if TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0
    #define TTC_SYSCLOCK_FREQUENCY_MAX 24000000
#else
    // only internal highspeed RC-clock available
    #define TTC_SYSCLOCK_FREQUENCY_MAX 6000000
#endif

#define TTC_SYSCLOCK_FREQUENCY_MIN 6000000

#ifndef TTC_SYSCLOCK1   // device not defined in makefile
    #define TTC_SYSCLOCK1    E_ttc_sysclock_architecture_stm32w1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_sysclock_types.h *************************

typedef struct { // register description (adapt according to stm32w1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_sysclock_register;

typedef struct {  // stm32w1xx specific configuration data of single SYSCLOCK device
    t_sysclock_register* BaseRegister;       // base address of SYSCLOCK device registers
} __attribute__( ( __packed__ ) ) t_sysclock_stm32w1xx_config;

// t_ttc_sysclock_architecture is required by ttc_sysclock_types.h
#define t_ttc_sysclock_architecture t_sysclock_stm32w1xx_config

//} Structures/ Enums


#endif //SYSCLOCK_STM32W1XX_TYPES_H
