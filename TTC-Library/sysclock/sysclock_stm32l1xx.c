/** { sysclock_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for sysclock devices on stm32l1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20141106 14:10:16 UTC
 *
 *  Note: See ttc_sysclock.h for description of stm32l1xx independent SYSCLOCK implementation.
 *
 *  Authors: Adrián Romero, Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "sysclock_stm32l1xx.h".
//
#include "sysclock_stm32l1xx.h"
#include "../ttc_cpu.h"
#include "../ttc_gpio.h"
//X #include "stm32l1xx_gpio.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _sysclock_stm32l1xx_foo(t_ttc_sysclock_config* Config)

/** Set AHB bus prescaler to given value
 *
 * @param Divisor  = 1, 2, 4, 8, ..., 512
 */
void _sysclock_stm32l1xx_ahb_prescaler( t_u16 Divisor );

/** Set APB1 or APB2 bus prescaler to given value
 *
 * @param Divisor  = 1, 2, 4, 8, 6
 */
void _sysclock_stm32l1xx_apb_prescaler( t_u8 BusIndex, t_u8 Divisor );

/** configure all prescalers automatically to reach given target frequency as close as possible
 */
void _sysclock_stm32llxx_set_clock_frequency( t_ttc_sysclock_config* Config, e_stm32_clock_source Source, t_u32 NewFrequency );

/** configure PLL to given values
 */
void _sysclock_stm32l1xx_pll_configure( t_u8 Multiplier, t_u8 Divisor, e_stm32_clock_source Source );

/** read current settings from indexed PLL
 */
void _sysclock_stm32l1xx_pll_read( t_u8* Multiplier, t_u8* Divisor );

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Global Variables ***********************************************************

const t_u8 PLL_Divisor_Min    = 2; // BUG: datasheet RM0038 p.98 says 2 but this does not seem to be correct!
const t_u8 PLL_Divisor_Max    = 4; // BUG: datasheet RM0038 p.98 says 4 but this does not seem to be correct!
const t_u8 PLL_Multiplier_Min = 3;
const t_u8 PLL_Multiplier_Max = 48;
const t_u8 sysclock_stm32l1xx_PLL_Multipliers[9] = {3, 4, 6, 8, 12, 16, 24, 32, 48};
const t_u8 sysclock_stm32l1xx_PLL_AmountMultipliers = 9;
const t_u8 sysclock_stm32l1xx_PLL_Dividers[16] = {0, 0, 0, 0, 1, 2, 3, 4, 1, 2, 3, 4, 6, 7, 8, 9};
const t_base sysclock_stm32l1xx_available_frequencies[] = { 65536,
                                                            131072,
                                                            262144,
                                                            524288,
                                                            1048000,
                                                            2097000,
                                                            4194000,
                                                            16000000,
                                                            24000000,
                                                            32000000,
                                                            0
                                                          };
const t_base sysclock_stm32l1xx_available_frequencies_USB[] = { 24000000, 32000000, 0 };

#define SYSCLOCK_STM32L1XX_ENABLE_MCO 1 // == 1: enable clock output on MCO pin for debugging

//}Global Variables
//{ Private Function Declarations **********************************************

/** switches clock source of SYSCLK (sets CFGR.SW)
 *
 * @param   Source (e_sysclock_stm32l1xx_sysclock_source)  new clock source to be used
 * @return  ==0: switch was successfull; !=0: error code
 */
e_ttc_sysclock_errorcode _sysclock_stm32l1xx_switch_sysclock( e_sysclock_stm32l1xx_sysclock_source Source );

/** tries to configure, enable and switch to given clock source at given frequency
 *
 * @param Config        pointer to struct t_ttc_sysclock_config (field Profile stores the current profile)
 * @param Source        clock signal input to use for SYSCLK
 * @param NewFrequency  desired SYSCLK frequency
 * @return ==0: switch to new clock source was successfull; !=0: error code
 */
e_ttc_sysclock_errorcode _sysclock_stm32l1xx_set_clock_frequency( t_ttc_sysclock_config* Config, e_stm32_clock_source Source, t_u32 NewFrequency );


//InsertPrivateFunctionDeclarations above
//}Private Function Declarations
//{ Function Definitions *******************************************************

const t_base*            sysclock_stm32l1xx_frequency_get_all( t_ttc_sysclock_config* Config ) {

    if ( Config->Flags.Bits.ConsiderUSB )
    { return &( sysclock_stm32l1xx_available_frequencies_USB[0] ); }
    else
    { return &( sysclock_stm32l1xx_available_frequencies[0] ); }
}
void                     sysclock_stm32l1xx_prepare( t_ttc_sysclock_config* Config ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    //(already called from startup_stm32l1xx_md.s) SystemInit(); // init STM32 standard peripherals library
    sysclock_stm32l1xx_reset( Config );

    if ( SYSCLOCK_STM32L1XX_ENABLE_MCO ) { // send SYSCLOCK to MCO pin for debugging
        ttc_gpio_init( E_ttc_gpio_pin_a8, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max );
        gpio_stm32l1xx_alternate_function( E_ttc_gpio_pin_a8, 0 );
        //? ttc_gpio_init(E_ttc_gpio_pin_a12, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_max);

        register_stm32l1xx_RCC.CFGR.Bits.MCOPRE = 4;     // 1 / 16
        register_stm32l1xx_RCC.CFGR.Bits.MCOSEL = ssms_SYSCLK;
    }
}
void                     sysclock_stm32l1xx_profile_switch( t_ttc_sysclock_config* Config, e_ttc_sysclock_profile NewProfile ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_SYSCLOCK( NewProfile < E_ttc_sysclock_profile_unknown, ttc_assert_origin_auto );

    switch ( NewProfile ) {

        case E_ttc_sysclock_profile_MaxSpeed: {
            Config->Profile = NewProfile; // profile changed
            if ( TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL ) {
                if ( sysclock_stm32l1xx_enable_oscillator( E_ttc_sysclock_oscillator_HighSpeedExternal, 1 ) == E_ttc_sysclock_errorcode_OK ) { // HighSpeed External is available: try to use it
                    if ( _sysclock_stm32l1xx_set_clock_frequency( Config, scs_ExternalCrystal, TTC_SYSCLOCK_FREQUENCY_MAX ) == E_ttc_sysclock_errorcode_OK )
                    { break; } // switch was successfull
                }
            }
            if ( TTC_SYSCLOCK_INTERNAL_HIGHSPEED_CLOCK ) {
                _sysclock_stm32l1xx_set_clock_frequency( Config, scs_InternalHighSpeed, TTC_SYSCLOCK_FREQUENCY_MAX );
                break;
            }
            if ( TTC_SYSCLOCK_MULTISPEED_CLOCK ) {
                _sysclock_stm32l1xx_set_clock_frequency( Config, scs_MultiSpeedClock, TTC_SYSCLOCK_FREQUENCY_MAX );
                break;
            }
            ttc_assert_halt_origin( ttc_assert_origin_auto ); // Unsupported system configuration. Check your TTC_SYSCLOCK_* defines!
            break;
        }
        case E_ttc_sysclock_profile_MinSpeed: {
            Config->Profile = NewProfile; // profile changed


            if ( TTC_SYSCLOCK_INTERNAL_HIGHSPEED_CLOCK ) {
                _sysclock_stm32l1xx_set_clock_frequency( Config, scs_InternalHighSpeed, TTC_SYSCLOCK_FREQUENCY_MIN );
                break;
            }
            if ( TTC_SYSCLOCK_MULTISPEED_CLOCK ) {
                _sysclock_stm32l1xx_set_clock_frequency( Config, scs_MultiSpeedClock, TTC_SYSCLOCK_FREQUENCY_MIN );
                break;
            }
            if ( TTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL ) {
                _sysclock_stm32l1xx_set_clock_frequency( Config, scs_ExternalCrystal, TTC_SYSCLOCK_FREQUENCY_MIN );
                break;
            }
            ttc_assert_halt_origin( ttc_assert_origin_auto ); // Unsupported system configuration. Check your TTC_SYSCLOCK_* defines!
            break;
        }
        default:
            SystemCoreClockUpdate();
            Config->SystemClockFrequency = SystemCoreClock;
            break;
    }
}
void                     sysclock_stm32l1xx_reset( t_ttc_sysclock_config* Config ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    Config->Architecture                = E_ttc_sysclock_architecture_stm32l1xx;
    Config->LowLevelConfig.BaseRegister = &register_stm32l1xx_RCC;
    Config->SystemClockFrequency        = 0;
    Config->Profile                     = E_ttc_sysclock_profile_None;

    // this driver can consider USB operation during clock configuration (enabling USB reduces amount of available clock frequencies)
#if TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0
    Config->Flags.Bits.ConsiderUSB = 1; // 48MHz USB clock available only with enabled HSE
#else
    Config->Flags.Bits.ConsiderUSB = 0; // 48MHz USB clock not available without HSE (STM32L100xx-Datasheet p. 21)
#endif

}
void                     sysclock_stm32l1xx_udelay( t_ttc_sysclock_config* Config, t_base Microseconds ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    // This delay has been calibrated on STM32L100 @ 32MHz
    // Minimum delay 1usecs
    // Minimum usable delay 10usec

    volatile t_base_signed Remaining = ( ( Microseconds - 1 ) * 524 / 100 );

    // adapt to current system clock frequency (simple linear approach)
    Remaining *= TTC_SYSCLOCK_FREQUENCY_MAX   / 10000;
    Remaining /= Config->SystemClockFrequency / 10000;

    for ( ; Remaining > 0; )
    { Remaining--; }
}
void                     sysclock_stm32l1xx_update( t_ttc_sysclock_config* Config ) {
    Assert_SYSCLOCK_Writable( Config, ttc_assert_origin_auto );  // pointers must not be NULL

    if ( 1 ) { // calculate Config->SystemClockFrequency
        switch ( register_stm32l1xx_RCC.CFGR.Bits.SWS ) { // -> RM0038 p. 98

            case 0b00: // MSI oscillator used as system clock
                Config->SystemClockFrequency = TTC_SYSCLOCK_MULTISPEED_CLOCK; //min frequency
                break;
            case 0b01: // HSI oscillator used as system clock
                Assert_SYSCLOCK( TTC_SYSCLOCK_INTERNAL_HIGHSPEED_CLOCK > 0, ttc_assert_origin_auto );  // define TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL in your board makefile!
                Config->SystemClockFrequency = TTC_SYSCLOCK_INTERNAL_HIGHSPEED_CLOCK; //max frequency
                break;
            case 0b10: // HSE oscillator used as system clock
                Assert_SYSCLOCK( TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0, ttc_assert_origin_auto );  // define TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL in your board makefile!
                Config->SystemClockFrequency = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL; //max frequency
                break;
            case 0b11: // PLL used as system clock
                if ( register_stm32l1xx_RCC.CFGR.Bits.PLLSRC ) { // PLL gets clock from HSE
                    Assert_SYSCLOCK( TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0, ttc_assert_origin_auto );  // we're running from HSE but Crystal Frequency is not defined: define TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL in your board makefile!
                    t_u8 PLL1_Multiplier = 0;
                    t_u8 PLL1_Divisor    = 0;
                    _sysclock_stm32l1xx_pll_read( &PLL1_Multiplier, &PLL1_Divisor );
                    Config->SystemClockFrequency = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL / PLL1_Divisor * PLL1_Multiplier;
                }
                else {                                              // PLL gets clock from HSI
                    Assert_SYSCLOCK( TTC_SYSCLOCK_INTERNAL_HIGHSPEED_CLOCK > 0, ttc_assert_origin_auto );  // define TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL in your board makefile!
                    t_u8 PLL1_Multiplier = 0;
                    t_u8 PLL1_Divisor    = 0;
                    _sysclock_stm32l1xx_pll_read( &PLL1_Multiplier, &PLL1_Divisor );
                    Config->SystemClockFrequency = TTC_SYSCLOCK_INTERNAL_HIGHSPEED_CLOCK / PLL1_Divisor * PLL1_Multiplier;
                }
                break;
            default:
                ttc_assert_halt_origin( ttc_assert_origin_auto ); // not applicable
                return;
                break;
        }
    }
    if ( 1 ) { // calculate Config->LowLevelConfig.frequency_ahb (AHB-Bus)
        t_u16 Divisor = 1;
        switch ( register_stm32l1xx_RCC.CFGR.Bits.HPRE ) { // AHB-Prescaler -> RM0038 p. 99
            case 0b1000: Divisor = 2;   break;
            case 0b1001: Divisor = 4;   break;
            case 0b1010: Divisor = 8;   break;
            case 0b1011: Divisor = 16;  break;
            case 0b1100: Divisor = 64;  break;
            case 0b1101: Divisor = 128; break;
            case 0b1110: Divisor = 256; break;
            case 0b1111: Divisor = 512; break;
            default: break; // sysclock not divided
        }
        Config->LowLevelConfig.frequency_ahb =  Config->SystemClockFrequency / Divisor;
    }
    if ( 1 ) { // calculate Config->LowLevelConfig.frequency_apb1 (APB1-Bus)
        t_u16 Divisor = 1;
        switch ( register_stm32l1xx_RCC.CFGR.Bits.PPRE1 ) { // APB1-Prescaler -> RM0038 p. 99
            case 0b100: Divisor = 2;   break;
            case 0b101: Divisor = 4;   break;
            case 0b110: Divisor = 8;   break;
            case 0b111: Divisor = 16;  break;
            default: break;
        }
        Config->LowLevelConfig.frequency_apb1 =  Config->LowLevelConfig.frequency_ahb / Divisor;
    }
    if ( 1 ) { // calculate Config->LowLevelConfig.frequency_apb2 (APB2-Bus)
        t_u16 Divisor = 1;
        switch ( register_stm32l1xx_RCC.CFGR.Bits.PPRE2 ) { // APB2-Prescaler -> RM0038 p. 99
            case 0b100: Divisor = 2;   break;
            case 0b101: Divisor = 4;   break;
            case 0b110: Divisor = 8;   break;
            case 0b111: Divisor = 16;  break;
            default: break;
        }
        Config->LowLevelConfig.frequency_apb2 =  Config->LowLevelConfig.frequency_ahb / Divisor;
    }
    if ( 1 ) { // calculate Config->LowLevelConfig.frequency_rtc (RTC/LED-Clock)
        switch ( register_stm32l1xx_RCC.CSR.Bits.RTCSEL ) { // RTC Selection-> RM0038 p. 99
            case 0b00: Config->LowLevelConfig.frequency_rtc = 0;                                        break; // no clock
            case 0b01: Config->LowLevelConfig.frequency_rtc = TTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL;   break; // LSE oscillator
            case 0b10: Config->LowLevelConfig.frequency_rtc = TTC_SYSCLOCK_INTERNAL_LOWSPEED_CLOCK;     break; // LSI oscillator
            case 0b11: { // HSE oscillator divided by RTCPRE
                t_u8 Divisor = 2 << register_stm32l1xx_RCC.CR.Bits.RTCPRE;  // RTC-Prescaler -> RM0038 p. 95
                Config->LowLevelConfig.frequency_rtc = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL / Divisor;
                break;
            }
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // size of RTCSEL field seems to be > 2 bits, check your struct definitions!
        }
    }
    if ( 1 ) { // calculate Config->LowLevelConfig.frequency_systick (SysTick Timer)
        if ( ! register_cortexm3_STK.CTRL.Bits.Enable )
        { Config->LowLevelConfig.frequency_systick = 0; } // SysTick is switched off
        else {
            // -> RM0038 p. 85
            if ( register_cortexm3_STK.CTRL.Bits.ClkSource ) // using processor clock
            { Config->LowLevelConfig.frequency_systick = Config->LowLevelConfig.frequency_ahb; }
            else                                          // using external clock = HCLK / 8
            { Config->LowLevelConfig.frequency_systick = Config->LowLevelConfig.frequency_ahb / 8; }
        }
    }
}
e_ttc_sysclock_errorcode sysclock_stm32l1xx_enable_oscillator( e_ttc_sysclock_oscillator Oscillator, BOOL On ) {
    t_u8 TimeOut = -1;

    switch ( Oscillator ) {
        case E_ttc_sysclock_oscillator_MultiSpeedInternal: {
            if ( On ) {
                register_stm32l1xx_RCC.CR.Bits.MSION = 1;
                while ( TimeOut && !register_stm32l1xx_RCC.CR.Bits.MSIRDY ) { TimeOut--; }
                if ( TimeOut == 0 ) {
                    register_stm32l1xx_RCC.CR.Bits.MSION = 0;
                    return E_ttc_sysclock_errorcode_OscillatorError; // could not start oscillator!
                }
            }
            else {
                register_stm32l1xx_RCC.CR.Bits.MSION = 0;
                while ( TimeOut && register_stm32l1xx_RCC.CR.Bits.MSIRDY ) { TimeOut--; }
                if ( TimeOut == 0 )
                { return E_ttc_sysclock_errorcode_OscillatorError; } // could not stop oscillator!
            }
            break;
        }
        case E_ttc_sysclock_oscillator_HighSpeedInternal: {
            if ( On ) {
                register_stm32l1xx_RCC.CR.Bits.HSION = 1;
                while ( TimeOut && !register_stm32l1xx_RCC.CR.Bits.HSIRDY ) { TimeOut--; }
                if ( TimeOut == 0 ) {
                    register_stm32l1xx_RCC.CR.Bits.HSION = 0;
                    return E_ttc_sysclock_errorcode_OscillatorError; // could not start oscillator!
                }
            }
            else {
                register_stm32l1xx_RCC.CR.Bits.HSION = 0;
                while ( TimeOut && register_stm32l1xx_RCC.CR.Bits.HSIRDY ) { TimeOut--; }
                if ( TimeOut == 0 )
                { return E_ttc_sysclock_errorcode_OscillatorError; } // could not stop HSI!
            }
            break;
        }
        case E_ttc_sysclock_oscillator_HighSpeedExternal: {
            if ( On ) {
                register_stm32l1xx_RCC.CR.Bits.HSEON = 1;
                while ( TimeOut && !register_stm32l1xx_RCC.CR.Bits.HSERDY ) { TimeOut--; }
                if ( TimeOut == 0 ) {
                    register_stm32l1xx_RCC.CR.Bits.HSEON = 0;
                    return E_ttc_sysclock_errorcode_OscillatorError; // could not start oscillator!
                }
            }
            else {
                register_stm32l1xx_RCC.CR.Bits.HSEON = 0;
                while ( TimeOut && register_stm32l1xx_RCC.CR.Bits.HSERDY ) { TimeOut--; }
                if ( TimeOut == 0 )
                { return E_ttc_sysclock_errorcode_OscillatorError; } // could not stop HSE!
            }
            break;
        }
        case E_ttc_sysclock_oscillator_PLL1: {
            if ( On ) {
                register_stm32l1xx_RCC.CR.Bits.PLLON = 1;
                while ( TimeOut && !register_stm32l1xx_RCC.CR.Bits.PLLRDY ) { TimeOut--; }
                if ( TimeOut == 0 ) {
                    register_stm32l1xx_RCC.CR.Bits.PLLON = 0;
                    return E_ttc_sysclock_errorcode_OscillatorError; // could not start oscillator!
                }
            }
            else {
                register_stm32l1xx_RCC.CR.Bits.PLLON = 0;
                while ( TimeOut && register_stm32l1xx_RCC.CR.Bits.PLLRDY ) { TimeOut--; }
                if ( TimeOut == 0 )
                { return E_ttc_sysclock_errorcode_OscillatorError; } // could not stop PLL!
            }
            break;
        }
        case E_ttc_sysclock_oscillator_LowSpeedInternal: {
            ttc_assert_halt_origin( ttc_assert_origin_auto ); // ToDo: Implement
            break;
        }
        case E_ttc_sysclock_oscillator_LowSpeedExternal: {
            ttc_assert_halt_origin( ttc_assert_origin_auto ); // ToDo: Implement
            break;
        }
        default: return E_ttc_sysclock_errorcode_OscillatorError; break;
    }

    return ( e_ttc_sysclock_errorcode ) 0;
}
void                     sysclock_stm32l1xx_frequency_set( t_ttc_sysclock_config* Config, t_base NewFrequency ) {
    Assert_SYSCLOCK_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    if ( !Config->LowLevelConfig.SysclockSource ) { // no clock source defined yet: select default one
#if TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0
        Config->LowLevelConfig.SysclockSource = scs_ExternalCrystal;
#else
        Config->LowLevelConfig.SysclockSource = scs_InternalHighSpeed;
#endif
    }
    _sysclock_stm32l1xx_set_clock_frequency( Config, Config->LowLevelConfig.SysclockSource, NewFrequency );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

e_ttc_sysclock_errorcode _sysclock_stm32l1xx_switch_sysclock( e_sysclock_stm32l1xx_sysclock_source Source ) {

    if ( register_stm32l1xx_RCC.CFGR.Bits.SWS != Source ) {
        e_ttc_sysclock_oscillator Oscillator = E_ttc_sysclock_oscillator_None;
        switch ( Source ) { // ensure that clock source is already active
            case ssss_MSI: Oscillator = E_ttc_sysclock_oscillator_MultiSpeedInternal; break;
            case ssss_HSI: Oscillator = E_ttc_sysclock_oscillator_HighSpeedInternal;  break;
            case ssss_HSE: Oscillator = E_ttc_sysclock_oscillator_HighSpeedExternal;  break;
            case ssss_PLL: Oscillator = E_ttc_sysclock_oscillator_PLL1;               break;
            default: ttc_assert_halt_origin( ttc_assert_origin_auto );   break;
        }

        e_ttc_sysclock_errorcode Error = sysclock_stm32l1xx_enable_oscillator( Oscillator, TRUE );
        if ( Error )
        { return Error; } // could not start oscillator!

        register_stm32l1xx_RCC.CFGR.Bits.SW = Source;
        t_u16 TimeOut = -1;
        while ( TimeOut && ( register_stm32l1xx_RCC.CFGR.Bits.SWS != Source ) ) { TimeOut--; }
        Assert_SYSCLOCK( TimeOut != 0, ttc_assert_origin_auto ); // TimeOut: cannot switch to desired clock source!
        if ( TimeOut == 0 )
        { return E_ttc_sysclock_errorcode_CannotSwitchClock; } // could not switch to given clock source!
    }
    return E_ttc_sysclock_errorcode_OK;
}
void                     _sysclock_stm32l1xx_ahb_prescaler( t_u16 Divisor ) {
    switch ( Divisor ) { // -> RM0038 p.100
        case   1: register_stm32l1xx_RCC.CFGR.Bits.HPRE = 0b0000; break;
        case   2: register_stm32l1xx_RCC.CFGR.Bits.HPRE = 0b1000; break;
        case   4: register_stm32l1xx_RCC.CFGR.Bits.HPRE = 0b1001; break;
        case   8: register_stm32l1xx_RCC.CFGR.Bits.HPRE = 0b1010; break;
        case  16: register_stm32l1xx_RCC.CFGR.Bits.HPRE = 0b1011; break;
        case  32: register_stm32l1xx_RCC.CFGR.Bits.HPRE = 0b1100; break; // <- same as for 64!
        case  64: register_stm32l1xx_RCC.CFGR.Bits.HPRE = 0b1100; break;
        case 128: register_stm32l1xx_RCC.CFGR.Bits.HPRE = 0b1101; break;
        case 256: register_stm32l1xx_RCC.CFGR.Bits.HPRE = 0b1110; break;
        case 512: register_stm32l1xx_RCC.CFGR.Bits.HPRE = 0b1111; break;
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break;
    }
}
void                     _sysclock_stm32l1xx_apb_prescaler( t_u8 BusIndex, t_u8 Divisor ) {
    switch ( BusIndex ) {
        case 1: {
            switch ( Divisor ) { // -> RM0038 p.10
                case   1: register_stm32l1xx_RCC.CFGR.Bits.PPRE1 = 0b000; break;
                case   2: register_stm32l1xx_RCC.CFGR.Bits.PPRE1 = 0b100; break;
                case   4: register_stm32l1xx_RCC.CFGR.Bits.PPRE1 = 0b101; break;
                case   8: register_stm32l1xx_RCC.CFGR.Bits.PPRE1 = 0b110; break;
                case  16: register_stm32l1xx_RCC.CFGR.Bits.PPRE1 = 0b111; break;
                default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break;
            }
            break;
        }
        case 2: {
            switch ( Divisor ) { // -> RM0038 p.100
                case   1: register_stm32l1xx_RCC.CFGR.Bits.PPRE2 = 0b000; break;
                case   2: register_stm32l1xx_RCC.CFGR.Bits.PPRE2 = 0b100; break;
                case   4: register_stm32l1xx_RCC.CFGR.Bits.PPRE2 = 0b101; break;
                case   8: register_stm32l1xx_RCC.CFGR.Bits.PPRE2 = 0b110; break;
                case  16: register_stm32l1xx_RCC.CFGR.Bits.PPRE2 = 0b111; break;
                default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break;
            }
            break;
        }
    }

}
void                     _sysclock_stm32l1xx_busses_configure( t_u32 SystemClockFrequency, t_u32 TargetFrequency ) {

    // configure AHB Divider to get <= TargetFrequency
    t_u8 AHB_Prescaler = 1;
    while ( SystemClockFrequency / AHB_Prescaler > TargetFrequency ) // ensure that APB1 bus is clocked < 32MHz (RM0038 p.100)
    { AHB_Prescaler *= 2; }
    _sysclock_stm32l1xx_ahb_prescaler( AHB_Prescaler );

    // configure APB busses for same speed as AHB (max speed <= TargetFrequency)
    _sysclock_stm32l1xx_apb_prescaler( 1, 1 );
    _sysclock_stm32l1xx_apb_prescaler( 2, 1 );

}

e_ttc_sysclock_errorcode _sysclock_stm32l1xx_set_clock_frequency( t_ttc_sysclock_config* Config, e_stm32_clock_source Source, t_u32 TargetFrequency ) {
    t_u8  DivisorMin = PLL_Divisor_Min;
    t_u8  DivisorMax = PLL_Divisor_Max;
    t_u32 MinDeviation            = -1;
    t_u32 SystemClockFrequency    = 0;
    t_u8  Optimal_PLL_Multiplier  = 0;
    t_u8  Optimal_PLL_Divisor     = 0;

    if ( TargetFrequency > 32000000 )
    { TargetFrequency = 32000000; } // maximum supported SYSCLK on STM32L1xx

    e_ttc_sysclock_errorcode SwitchError = E_ttc_sysclock_errorcode_OK;

#if TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL > 0
    if ( Config->Flags.Bits.ConsiderUSB ) { // use of USB limits available frequencies
        // Choose PLLCLK frequency next to TargetFrequency that allows USB operation
        // -> STM32L100-Datasheet p. 21
        t_u32 MinDeviation1 = ttc_basic_distance( TargetFrequency, 24000000 ); // PLLDIV = 4
        t_u32 MinDeviation2 = ttc_basic_distance( TargetFrequency, 32000000 ); // PLLDIV = 3
        if ( MinDeviation1 < MinDeviation2 ) { // choosing 24MHz SYSCLK
            TargetFrequency = 24000000;
            DivisorMin = DivisorMax = 4; // limit range of divisor
        }
        else {                               // choosing 32MHz SYSCLK
            TargetFrequency = 32000000;
            DivisorMin = DivisorMax = 3; // limit range of divisor
        }
    }
#else
    Assert_SYSCLOCK( Config->Flags.Bits.ConsiderUSB == 0, ttc_assert_origin_auto );  // USB operation requires external crystal on STM32L1xx! (-> STM32L100-Datasheet p. 21)
#endif

    switch ( Source ) {

        case scs_ExternalCrystal: {
            for ( t_u8 PLL_Divisor = DivisorMin; PLL_Divisor <= DivisorMax; PLL_Divisor++ ) {
                for ( t_u8 i = 0; i < sysclock_stm32l1xx_PLL_AmountMultipliers; i++ ) {

                    SystemClockFrequency = TTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL / PLL_Divisor * sysclock_stm32l1xx_PLL_Multipliers[i];

                    t_u32 Deviation;
                    if ( TargetFrequency > SystemClockFrequency )
                    { Deviation = TargetFrequency - SystemClockFrequency; }
                    else
                    { Deviation = SystemClockFrequency - TargetFrequency; }

                    if ( MinDeviation > Deviation ) {
                        MinDeviation = Deviation;
                        Optimal_PLL_Multiplier = sysclock_stm32l1xx_PLL_Multipliers[i];
                        Optimal_PLL_Divisor    = PLL_Divisor;
                    }
                    if ( !MinDeviation ) { goto end_fors; } // found optimal solution
                }
            }

        end_fors:
            SwitchError = _sysclock_stm32l1xx_switch_sysclock( ssss_HSE ); ( void ) SwitchError;
            Assert_SYSCLOCK( SwitchError == E_ttc_sysclock_errorcode_OK, ttc_assert_origin_auto );

            _sysclock_stm32l1xx_busses_configure( SystemClockFrequency, TargetFrequency );

            // enable PLL (RM0038 p.96-100)
            Assert_SYSCLOCK( Optimal_PLL_Multiplier >=  3, ttc_assert_origin_auto );
            Assert_SYSCLOCK( Optimal_PLL_Multiplier <= 48, ttc_assert_origin_auto );
            Assert_SYSCLOCK( Optimal_PLL_Divisor    >=  2, ttc_assert_origin_auto );
            Assert_SYSCLOCK( Optimal_PLL_Divisor    <=  4, ttc_assert_origin_auto );

            _sysclock_stm32l1xx_pll_configure( Optimal_PLL_Multiplier, Optimal_PLL_Divisor, Source ); // configure PLL

            t_register_stm32l1xx_rcc_cfgr CFGR = register_stm32l1xx_RCC.CFGR;
            CFGR.Bits.HPRE = 0;      // reset AHB Prescaler
            register_stm32l1xx_RCC.CFGR = CFGR;

            SwitchError = _sysclock_stm32l1xx_switch_sysclock( ssss_PLL );
            Assert_SYSCLOCK( SwitchError == E_ttc_sysclock_errorcode_OK, ttc_assert_origin_auto );

            sysclock_stm32l1xx_enable_oscillator( E_ttc_sysclock_oscillator_MultiSpeedInternal, 0 );
            sysclock_stm32l1xx_enable_oscillator( E_ttc_sysclock_oscillator_HighSpeedInternal,  0 );
            break;
        }
        case scs_InternalHighSpeed: {
            for ( t_u8 PLL_Divisor = DivisorMin; PLL_Divisor <= DivisorMax; PLL_Divisor++ ) {
                for ( t_u8 i = 0; i < sysclock_stm32l1xx_PLL_AmountMultipliers; i++ ) {

                    Assert_SYSCLOCK( PLL_Divisor != 0, ttc_assert_origin_auto );  // division by zero error!
                    SystemClockFrequency = TTC_SYSCLOCK_INTERNAL_HIGHSPEED_CLOCK / PLL_Divisor * sysclock_stm32l1xx_PLL_Multipliers[i];

                    t_u32 Deviation = ttc_basic_distance( TargetFrequency, SystemClockFrequency );
                    if ( MinDeviation > Deviation ) {
                        MinDeviation = Deviation;
                        Optimal_PLL_Multiplier = sysclock_stm32l1xx_PLL_Multipliers[i];
                        Optimal_PLL_Divisor    = PLL_Divisor;
                    }
                    if ( !MinDeviation )
                    { goto end_fors2; } // found optimal solution
                }
            }
        end_fors2:

            SwitchError = _sysclock_stm32l1xx_switch_sysclock( ssss_HSI );
            Assert_SYSCLOCK( SwitchError == E_ttc_sysclock_errorcode_OK, ttc_assert_origin_auto );

            _sysclock_stm32l1xx_busses_configure( SystemClockFrequency, TargetFrequency );

            // enable PLL (RM0038 p.96-100)
            Assert_SYSCLOCK( Optimal_PLL_Multiplier >= PLL_Multiplier_Min, ttc_assert_origin_auto );
            Assert_SYSCLOCK( Optimal_PLL_Multiplier <= PLL_Multiplier_Max, ttc_assert_origin_auto );
            Assert_SYSCLOCK( Optimal_PLL_Divisor    >= PLL_Divisor_Min, ttc_assert_origin_auto );
            Assert_SYSCLOCK( Optimal_PLL_Divisor    <= PLL_Divisor_Max, ttc_assert_origin_auto );

            _sysclock_stm32l1xx_pll_configure( Optimal_PLL_Multiplier, Optimal_PLL_Divisor, Source ); // configure PLL

            t_register_stm32l1xx_rcc_cfgr CFGR = register_stm32l1xx_RCC.CFGR;
            CFGR.Bits.HPRE = 0;      // reset AHB Prescaler
            register_stm32l1xx_RCC.CFGR = CFGR;

#ifdef EXTENSION_board_stm32l152_discovery
            INFO( "Switching to PLL on STM32L152 was observed to crash. Will switch to HSI instead!" )
            SwitchError = _sysclock_stm32l1xx_switch_sysclock( ssss_HSI ); // switch to HSI instead (slower but works) - ToDo: fix switching to PLL!
#else
            SwitchError = _sysclock_stm32l1xx_switch_sysclock( ssss_PLL );
#endif
            ( void ) SwitchError;
            Assert_SYSCLOCK( SwitchError == E_ttc_sysclock_errorcode_OK, ttc_assert_origin_auto );

            // Switch off all other clocks to save energy
            sysclock_stm32l1xx_enable_oscillator( E_ttc_sysclock_oscillator_MultiSpeedInternal, 0 );
            sysclock_stm32l1xx_enable_oscillator( E_ttc_sysclock_oscillator_HighSpeedExternal,  0 );

            break;
        }
        case scs_MultiSpeedClock: {
            /* DEPRECATED clock switch to MSI
                        // Set MSION bit
                        register_stm32l1xx_RCC.CR.All |= ( t_u32 )0x00000100;

                        // Reset SW[1:0], HPRE[3:0], PPRE1[2:0], PPRE2[2:0], MCOSEL[2:0] and MCOPRE[2:0] Bits
                        register_stm32l1xx_RCC.CFGR.All &= ( t_u32 )0x88FFC00C;

                        // Reset HSION, HSEON, CSSON and PLLON Bits
                        register_stm32l1xx_RCC.CR.All &= ( t_u32 )0xEEFEFFFE;

                        // Reset HSEBYP bit
                        register_stm32l1xx_RCC.CR.All &= ( t_u32 )0xFFFBFFFF;

                        // Reset PLLSRC, PLLMUL[3:0] and PLLDIV[1:0] Bits
                        register_stm32l1xx_RCC.CFGR.All &= ( t_u32 )0xFF02FFFF;

                        // Disable all interrupts
                        register_stm32l1xx_RCC.CIR.All = ( t_u32 )0x00000000;

                        // Set MSI clock range to 65.536KHz

                        t_register_stm32l1xx_rcc_icscr ICSR;

                        t_u32 temp;

                        temp = register_stm32l1xx_RCC.ICSCR.All;

                        // Clear MSIRANGE[2:0] Bits
                        temp &= ~( t_u32 )RCC_ICSCR_MSIRANGE;


                        if ( TargetFrequency == 524288 ) { //MSI MIN FREQUENCY


                            // Set the MSIRANGE[2:0] Bits according to RCC_MSIRange value
                            temp |= ( t_u32 )RCC_ICSCR_MSIRANGE_3;
                            SystemClockFrequency = 524288 ; //Min frequency



                        }
                        else {  // MSI MAX FREQUENCY

                            temp |= ( t_u32 )RCC_ICSCR_MSIRANGE_6;
                            SystemClockFrequency = 4194000;

                        }

                        register_stm32l1xx_RCC.ICSCR.All = temp;


                        // Select MSI as system clock source

                        // Clear SW[1:0] Bits

                        temp = register_stm32l1xx_RCC.CFGR.All;

                        temp &= ~( t_u32 )0x00000003;

                        // Set SW[1:0] Bits according to RCC_SYSCLKSource value
                        temp |= ( ( t_u32 )0x00000000 );


                        register_stm32l1xx_RCC.CFGR.All = temp;


                        // Wait till PLL is used as system clock source
                        while ( ( ( t_u8 )( register_stm32l1xx_RCC.CFGR.All & CFGR_SWS ) ) != 0x00 ) {}

                        // Reset HSION, HSEON, CSSON and PLLON Bits
                        register_stm32l1xx_RCC.CR.All &= ( t_u32 )0xEEFEFFFE;
            */

            // enable MSI clock
            register_stm32l1xx_RCC.CR.Bits.MSION = 1;

            // prepare register modifications in local variable (allows compiler to merge bitfield accesses)
            t_register_stm32l1xx_rcc_cfgr CFGR;
            CFGR.All = register_stm32l1xx_RCC.CFGR.All;
            CFGR.Bits.HPRE   = 0;
            CFGR.Bits.SW     = 0;
            CFGR.Bits.PPRE1  = 0;
            CFGR.Bits.PPRE2  = 0;
            CFGR.Bits.MCOSEL = 0;
            CFGR.Bits.MCOPRE = 0;

            register_stm32l1xx_RCC.CFGR.All = CFGR.All;

            // disable all other clocks
            t_register_stm32l1xx_rcc_cr CR;
            CR.All = register_stm32l1xx_RCC.CR.All;
            CR.Bits.HSION  = 0;
            CR.Bits.HSEON  = 0;
            CR.Bits.CSSON  = 0;
            CR.Bits.PLLON  = 0;
            CR.Bits.HSEBYP = 0;
            register_stm32l1xx_RCC.CR.All = CR.All;

            // disable all interrupts
            register_stm32l1xx_RCC.CIR.All = ( t_u32 ) 0;

            if ( TargetFrequency == 524288 ) { //MSI MIN FREQUENCY
                register_stm32l1xx_RCC.ICSCR.Bits.MSIRANGE = 3;
            }
            else {                             // MSI MAX FREQUENCY
                register_stm32l1xx_RCC.ICSCR.Bits.MSIRANGE = 6;
            }

            // Select MSI as system clock source
            register_stm32l1xx_RCC.CFGR.Bits.SW = rcc_cfgr_sw_msi;

            // Wait till PLL is used as system clock source
            while ( register_stm32l1xx_RCC.CFGR.Bits.SWS != rcc_cfgr_sw_msi );



            // Switch off all other clocks to save energy
            sysclock_stm32l1xx_enable_oscillator( E_ttc_sysclock_oscillator_HighSpeedExternal, 0 );
            sysclock_stm32l1xx_enable_oscillator( E_ttc_sysclock_oscillator_HighSpeedInternal, 0 );
            break;

        }
        default: ttc_assert_halt_origin( ttc_assert_origin_auto );

            break;
    }

    Config->LowLevelConfig.SysclockSource = Source;
    Config->SystemClockFrequency = SystemClockFrequency;
    return E_ttc_sysclock_errorcode_OK;
}
void                     _sysclock_stm32l1xx_pll_configure( t_u8 Multiplier, t_u8 Divisor, e_stm32_clock_source Source ) {

    Assert_SYSCLOCK( ( Multiplier >= 3 && Multiplier <= 48 ), ttc_assert_origin_auto );
    Assert_SYSCLOCK( ( Divisor >= PLL_Divisor_Min && Divisor <= PLL_Divisor_Max ), ttc_assert_origin_auto );

    t_u8 Value_SWS = register_stm32l1xx_RCC.CFGR.Bits.SWS;

    if ( Value_SWS == 0b11 ) { // System Clock is using this PLL: switch to another source
        e_ttc_sysclock_errorcode SwitchError = _sysclock_stm32l1xx_switch_sysclock( ssss_HSI ); ( void ) SwitchError;
        Assert_SYSCLOCK( SwitchError == E_ttc_sysclock_errorcode_OK, ttc_assert_origin_auto );
    }

    sysclock_stm32l1xx_enable_oscillator( E_ttc_sysclock_oscillator_PLL1, 0 ); // switch off PLL to allow reconfiguration

    // Select PLL input Source
    if ( Source == scs_InternalHighSpeed ) {
        register_stm32l1xx_RCC.CFGR.Bits.PLLSRC   = 0;    // HSI as PLL input source
    }
    else {
        register_stm32l1xx_RCC.CFGR.Bits.PLLSRC   = 1;    // HSE as PLL input source
    }

    // Select PLLMUL
    if ( Multiplier == 3 ) {
        register_stm32l1xx_RCC.CFGR.Bits.PLLMUL      = 0b0000;

    }
    else if ( Multiplier == 4 ) {
        register_stm32l1xx_RCC.CFGR.Bits.PLLMUL      =  0b0001;

    }
    else if ( Multiplier == 6 ) {
        register_stm32l1xx_RCC.CFGR.Bits.PLLMUL      =  0b0010;

    }
    else if ( Multiplier == 8 ) {

        register_stm32l1xx_RCC.CFGR.Bits.PLLMUL      =  0b0011;


    }
    else if ( Multiplier == 12 ) {
        register_stm32l1xx_RCC.CFGR.Bits.PLLMUL      =  0b0100;

    }
    else if ( Multiplier == 16 ) {
        register_stm32l1xx_RCC.CFGR.Bits.PLLMUL      =  0b0101;

    }
    else if ( Multiplier == 24 ) {
        register_stm32l1xx_RCC.CFGR.Bits.PLLMUL      =  0b0110;

    }
    else if ( Multiplier == 32 ) {
        register_stm32l1xx_RCC.CFGR.Bits.PLLMUL      =  0b0111;

    }
    else {
        register_stm32l1xx_RCC.CFGR.Bits.PLLMUL      =  0b1000;

    }
    // Select PLLDIV

    switch ( Divisor ) { // -> RM0038 p.98
        case 2: register_stm32l1xx_RCC.CFGR.Bits.PLLDIV = 0b01; break;
        case 3: register_stm32l1xx_RCC.CFGR.Bits.PLLDIV = 0b10; break;
        case 4: register_stm32l1xx_RCC.CFGR.Bits.PLLDIV = 0b11; break;
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown/illegal divider value!
    }
    //X register_stm32l1xx_RCC.CFGR.Bits.PLLDIV    = Divisor - 1;

    e_ttc_sysclock_errorcode Error = sysclock_stm32l1xx_enable_oscillator( E_ttc_sysclock_oscillator_PLL1, 1 ); ( void ) Error;
    Assert_SYSCLOCK( !Error, ttc_assert_origin_auto );

    while ( register_stm32l1xx_RCC.CFGR.Bits.SWS != Value_SWS ) // restore previous system clock source
    { register_stm32l1xx_RCC.CFGR.Bits.SW = Value_SWS; }

}
void                     _sysclock_stm32l1xx_pll_read( t_u8* Multiplier, t_u8* Divisor ) {
    Assert_SYSCLOCK_Writable( Multiplier, ttc_assert_origin_auto );
    Assert_SYSCLOCK_Writable( Divisor, ttc_assert_origin_auto );

    switch ( register_stm32l1xx_RCC.CFGR.Bits.PLLMUL ) { // (-> RM0039 p.99)
        case 0b0000: *Multiplier =  3; break;
        case 0b0001: *Multiplier =  4; break;
        case 0b0010: *Multiplier =  6; break;
        case 0b0011: *Multiplier =  8; break;
        case 0b0100: *Multiplier = 12; break;
        case 0b0101: *Multiplier = 16; break;
        case 0b0110: *Multiplier = 24; break;
        case 0b0111: *Multiplier = 32; break;
        case 0b1000: *Multiplier = 48; break;
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown/ illegal register value (-> RM0039 p.99)
    }

    switch ( register_stm32l1xx_RCC.CFGR.Bits.PLLDIV ) { // (-> RM0039 p.99)
        case 0b00: *Divisor = 1; break;
        case 0b01: *Divisor = 2; break;
        case 0b10: *Divisor = 3; break;
        case 0b11: *Divisor = 4; break;
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown/ illegal register value (-> RM0039 p.99)
    }
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
