/** { ttc_board_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for BOARD device.
 *  Structures, Enums and Defines being required by both, high- and low-level board.
 *  This file does not provide function declarations!
 *
 *  Created from template ttc_device_types.h revision 42 at 20180420 13:26:22 UTC
 *
 *  Define ranks of high-level and low-level drivers (required by create_DeviceDriver.pl to generate correct #ifdefs)
 *  TTC_RANK_HIGHLEVEL=490
 *  TTC_RANK_LOWLEVEL=110
 *
 *  Authors: GREGOR REBEL
 *
}*/

#ifndef TTC_BOARD_TYPES_H
#define TTC_BOARD_TYPES_H

//{ Includes1 ************************************************************
//
// Includes being required for static configuration.
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_board.h" or "ttc_board.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h"
#include "ttc_cpu.h"

//} Includes
/** { Static Configuration *************************************************
 *
 * Most ttc drivers are statically configured by constant definitions in makefiles.
 * A static configuration allows to hide certain code fragments and variables for the
 * compilation process. If used wisely, your code will be lean and fast and do only
 * what is required for the current configuration.
 */

/** {  TTC_BOARDn has to be defined as constant in one of your makefiles
 *
 * Here we count how much devices have been defined and if all lower index devices have been defined too.
 */
#ifndef TTC_BOARD_AMOUNT //{ 10
    #ifdef TTC_BOARD10
        #ifndef TTC_BOARD9
            #      error TTC_BOARD10 is defined, but not TTC_BOARD9 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD8
            #      error TTC_BOARD10 is defined, but not TTC_BOARD8 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD7
            #      error TTC_BOARD10 is defined, but not TTC_BOARD7 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD6
            #      error TTC_BOARD10 is defined, but not TTC_BOARD6 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD5
            #      error TTC_BOARD10 is defined, but not TTC_BOARD5 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD4
            #      error TTC_BOARD10 is defined, but not TTC_BOARD4 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD3
            #      error TTC_BOARD10 is defined, but not TTC_BOARD3 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD2
            #      error TTC_BOARD10 is defined, but not TTC_BOARD2 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD1
            #      error TTC_BOARD10 is defined, but not TTC_BOARD1 - all lower TTC_BOARDn must be defined!
        #endif

        #define TTC_BOARD_AMOUNT 10
    #endif
#endif //}
#ifndef TTC_BOARD_AMOUNT //{ 9
    #ifdef TTC_BOARD9
        #ifndef TTC_BOARD8
            #      error TTC_BOARD9 is defined, but not TTC_BOARD8 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD7
            #      error TTC_BOARD9 is defined, but not TTC_BOARD7 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD6
            #      error TTC_BOARD9 is defined, but not TTC_BOARD6 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD5
            #      error TTC_BOARD9 is defined, but not TTC_BOARD5 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD4
            #      error TTC_BOARD9 is defined, but not TTC_BOARD4 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD3
            #      error TTC_BOARD9 is defined, but not TTC_BOARD3 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD2
            #      error TTC_BOARD9 is defined, but not TTC_BOARD2 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD1
            #      error TTC_BOARD9 is defined, but not TTC_BOARD1 - all lower TTC_BOARDn must be defined!
        #endif

        #define TTC_BOARD_AMOUNT 9
    #endif
#endif //}
#ifndef TTC_BOARD_AMOUNT //{ 8
    #ifdef TTC_BOARD8
        #ifndef TTC_BOARD7
            #      error TTC_BOARD8 is defined, but not TTC_BOARD7 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD6
            #      error TTC_BOARD8 is defined, but not TTC_BOARD6 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD5
            #      error TTC_BOARD8 is defined, but not TTC_BOARD5 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD4
            #      error TTC_BOARD8 is defined, but not TTC_BOARD4 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD3
            #      error TTC_BOARD8 is defined, but not TTC_BOARD3 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD2
            #      error TTC_BOARD8 is defined, but not TTC_BOARD2 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD1
            #      error TTC_BOARD8 is defined, but not TTC_BOARD1 - all lower TTC_BOARDn must be defined!
        #endif

        #define TTC_BOARD_AMOUNT 8
    #endif
#endif //}
#ifndef TTC_BOARD_AMOUNT //{ 7
    #ifdef TTC_BOARD7
        #ifndef TTC_BOARD6
            #      error TTC_BOARD7 is defined, but not TTC_BOARD6 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD5
            #      error TTC_BOARD7 is defined, but not TTC_BOARD5 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD4
            #      error TTC_BOARD7 is defined, but not TTC_BOARD4 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD3
            #      error TTC_BOARD7 is defined, but not TTC_BOARD3 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD2
            #      error TTC_BOARD7 is defined, but not TTC_BOARD2 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD1
            #      error TTC_BOARD7 is defined, but not TTC_BOARD1 - all lower TTC_BOARDn must be defined!
        #endif

        #define TTC_BOARD_AMOUNT 7
    #endif
#endif //}
#ifndef TTC_BOARD_AMOUNT //{ 6
    #ifdef TTC_BOARD6
        #ifndef TTC_BOARD5
            #      error TTC_BOARD6 is defined, but not TTC_BOARD5 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD4
            #      error TTC_BOARD6 is defined, but not TTC_BOARD4 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD3
            #      error TTC_BOARD6 is defined, but not TTC_BOARD3 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD2
            #      error TTC_BOARD6 is defined, but not TTC_BOARD2 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD1
            #      error TTC_BOARD6 is defined, but not TTC_BOARD1 - all lower TTC_BOARDn must be defined!
        #endif

        #define TTC_BOARD_AMOUNT 6
    #endif
#endif //}
#ifndef TTC_BOARD_AMOUNT //{ 5
    #ifdef TTC_BOARD5
        #ifndef TTC_BOARD4
            #      error TTC_BOARD5 is defined, but not TTC_BOARD4 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD3
            #      error TTC_BOARD5 is defined, but not TTC_BOARD3 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD2
            #      error TTC_BOARD5 is defined, but not TTC_BOARD2 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD1
            #      error TTC_BOARD5 is defined, but not TTC_BOARD1 - all lower TTC_BOARDn must be defined!
        #endif

        #define TTC_BOARD_AMOUNT 5
    #endif
#endif //}
#ifndef TTC_BOARD_AMOUNT //{ 4
    #ifdef TTC_BOARD4
        #ifndef TTC_BOARD3
            #      error TTC_BOARD4 is defined, but not TTC_BOARD3 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD2
            #      error TTC_BOARD4 is defined, but not TTC_BOARD2 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD1
            #      error TTC_BOARD4 is defined, but not TTC_BOARD1 - all lower TTC_BOARDn must be defined!
        #endif

        #define TTC_BOARD_AMOUNT 4
    #endif
#endif //}
#ifndef TTC_BOARD_AMOUNT //{ 3
    #ifdef TTC_BOARD3

        #ifndef TTC_BOARD2
            #      error TTC_BOARD3 is defined, but not TTC_BOARD2 - all lower TTC_BOARDn must be defined!
        #endif
        #ifndef TTC_BOARD1
            #      error TTC_BOARD3 is defined, but not TTC_BOARD1 - all lower TTC_BOARDn must be defined!
        #endif

        #define TTC_BOARD_AMOUNT 3
    #endif
#endif //}
#ifndef TTC_BOARD_AMOUNT //{ 2
    #ifdef TTC_BOARD2

        #ifndef TTC_BOARD1
            #      error TTC_BOARD2 is defined, but not TTC_BOARD1 - all lower TTC_BOARDn must be defined!
        #endif

        #define TTC_BOARD_AMOUNT 2
    #endif
#endif //}
#ifndef TTC_BOARD_AMOUNT //{ 1
    #ifdef TTC_BOARD1
        #define TTC_BOARD_AMOUNT 1
    #endif
#endif //}
#ifndef TTC_BOARD_AMOUNT //{ no devices defined at all
    #  warning Missing definition for TTC_BOARD1. Using default type. Add valid definition to your makefile to get rid if this message!
    // option 1: no devices available
    #define TTC_BOARD_AMOUNT 0
    // option 2: define a default device (enable lines below)
    // #define TTC_BOARD_AMOUNT 1
    // define TTC_BOARD1 ??? // <- place entry from e_ttc_board_architecture here!!!
#endif

//}
//}
/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_BOARD 0#         disable default asserts for board driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_BOARD_EXTRA 1#   enable extra asserts for board driver
 *
 * When debugging code that has been compiled with optimization (gcc option -O) then
 * certain variables may be optimized away. Declaring variables as VOLATILE_BOARD makes them
 * visible in debug session when asserts are enabled. You have no overhead at all if asserts are disabled.
 + foot_t* VOLATILE_BOARD VisiblePointer;
 *
 */
#ifndef TTC_ASSERT_BOARD    // any previous definition set (Makefile)?
    #define TTC_ASSERT_BOARD 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_BOARD == 1)  // use Assert()s in BOARD code (somewhat slower but alot easier to debug)
    #define Assert_BOARD(Condition, Origin)        if (! (Condition) ) { ttc_assert_halt_origin(Origin); }
    #define Assert_BOARD_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_BOARD_Readable(Address, Origin) Assert_Readable(Address, Origin)
    #define VOLATILE_BOARD                         volatile
#else  // use no default Assert()s in BOARD code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_BOARD(Condition, Origin)
    #define Assert_BOARD_Writable(Address, Origin)
    #define Assert_BOARD_Readable(Address, Origin)
    #define VOLATILE_BOARD
#endif

#ifndef TTC_ASSERT_BOARD_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_BOARD_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_BOARD_EXTRA == 1)  // use Assert()s in BOARD code (somewhat slower but alot easier to debug)
    #define Assert_BOARD_EXTRA(Condition, Origin) if (! (Condition) ) { ttc_assert_halt_origin(Origin); }
    #define Assert_BOARD_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_BOARD_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in BOARD code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_BOARD_EXTRA(Condition, Origin)
    #define Assert_BOARD_EXTRA_Writable(Address, Origin)
    #define Assert_BOARD_EXTRA_Readable(Address, Origin)
#endif
//}
/** { Check static configuration of board devices
 *
 * Each TTC_BOARDn must be defined as one from e_ttc_board_architecture.
 * Additional defines of type TTC_BOARDn_* may be added.
 *
 * The constants being checked here must be defined in your makefile.
 * Most constants are defined by EXTENSION_100_board_* or EXTENSION_150_board_extension_* files.
 */

#ifdef TTC_BOARD1

    // check extra defines for board device #1 here

#endif
#ifdef TTC_BOARD2

    // check extra defines for board device #2 here

#endif
#ifdef TTC_BOARD3

    // check extra defines for board device #3 here

#endif
#ifdef TTC_BOARD4

    // check extra defines for board device #4 here

#endif
#ifdef TTC_BOARD5

    // check extra defines for board device #5 here

#endif
#ifdef TTC_BOARD6

    // check extra defines for board device #6 here

#endif
#ifdef TTC_BOARD7

    // check extra defines for board device #7 here

#endif
#ifdef TTC_BOARD8

    // check extra defines for board device #8 here

#endif
#ifdef TTC_BOARD9

    // check extra defines for board device #9 here

#endif
#ifdef TTC_BOARD10

    // check extra defines for board device #10 here

#endif
//}

//}Static Configuration

//{ Includes2 ************************************************************
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_board.h" or "ttc_board.c"
//

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_board_stm32l100c_discovery
    #include "board/board_stm32l100c_discovery_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_board_stm32l152_discovery
    #include "board/board_stm32l152_discovery_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_board_stm32l053_discovery
    #include "board/board_stm32l053_discovery_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_board_sensor_dwm1000
    #include "board/board_sensor_dwm1000_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_board_sensor_dwm10001000
    #include "board/board_sensor_dwm1000_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_board_olimex_stm32_lcd
    #include "board/board_olimex_stm32_lcd_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_board_dso_0138
    #include "board/board_dso_0138_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_board_olimex_stm32_p107
    #include "board/board_olimex_stm32_p107_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** { Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_board_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_board_architecture
    #  warning Missing low-level definition for t_ttc_board_architecture (using default)
    #define t_ttc_board_architecture void
#endif
//}
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_board_errorcode       return codes of BOARD devices
    E_ttc_board_errorcode_OK = 0,

    // other warnings go here..

    E_ttc_board_errorcode_ERROR,                  // general failure
    E_ttc_board_errorcode_NULL,                   // NULL pointer not accepted
    E_ttc_board_errorcode_DeviceNotFound,         // corresponding device could not be found
    E_ttc_board_errorcode_InvalidConfiguration,   // sanity check of device configuration failed
    E_ttc_board_errorcode_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..

    E_ttc_board_errorcode_unknown                // no valid errorcodes past this entry
} e_ttc_board_errorcode;
typedef enum {   // e_ttc_board_architecture    types of architectures supported by BOARD driver
    E_ttc_board_architecture_none = 0,       // no architecture selected

    E_ttc_board_architecture_sensor_dwm1000, // automatically added by ./create_DeviceDriver.pl
    E_ttc_board_architecture_stm32l053_discovery, // automatically added by ./create_DeviceDriver.pl
    E_ttc_board_architecture_stm32l100c_discovery, // automatically added by ./create_DeviceDriver.pl
    E_ttc_board_architecture_stm32l152_discovery, // automatically added by ./create_DeviceDriver.pl
    E_ttc_board_architecture_olimex_stm32_lcd, // automatically added by ./create_DeviceDriver.pl
    E_ttc_board_architecture_dso_0138, // automatically added by ./create_DeviceDriver.pl
    E_ttc_board_architecture_olimex_stm32_p107, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    E_ttc_board_architecture_unknown        // architecture not supported
} e_ttc_board_architecture;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef union {  // architecture dependent configuration

#ifdef EXTENSION_board_sensor_dwm1000
    t_board_sensor_dwm1000_config sensor_dwm1000;  // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_board_stm32l053_discovery
    t_board_stm32l053_discovery_config stm32l053_discovery;  // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_board_stm32l100c_discovery
    t_board_stm32l100c_discovery_config stm32l100c_discovery;  // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_board_stm32l152_discovery
    t_board_stm32l152_discovery_config stm32l152_discovery;  // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_board_olimex_stm32_lcd
    t_board_olimex_stm32_lcd_config olimex_stm32_lcd;  // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_board_dso_0138
    t_board_dso_0138_config dso_0138;  // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_board_olimex_stm32_p107
    t_board_olimex_stm32_p107_config olimex_stm32_p107;  // automatically added by ./create_DeviceDriver.pl
#endif
    //InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_board_architecture;
typedef struct s_ttc_board_features { // static minimum, maximum and default values of features of single board

    // Add any amount of architecture independent values describing board devices here.
    // You may also want to add a plausibility check for each value to _ttc_board_configuration_check().

    t_u8 Unused;

} __attribute__( ( __packed__ ) ) t_ttc_board_features;
typedef struct s_ttc_board_config { //          architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_board_init()
    //       and after ttc_board_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // Hint: Order structure elements for optimal alignment.
    //       You should place smaller datatypes like t_u8 and t_u16 after all t_u32 and pointer types
    //       to prevent insertion of pad-bytes. Most architectures don't support placing 32-bit datatypes at
    //       unaligned address.

    struct { // Init: fields to be set by application before first calling ttc_board_init() --------------
        // Do not change these values after calling ttc_board_init()!

        //...

        struct { // generic initial configuration bits common for all low-level Drivers
            unsigned unused : 1;  // ==1:
            // ToDo: additional high-level flags go here..
        } Flags;
    } Init;

    // Fields below are read-only for application! ----------------------------------------------------------

    u_ttc_board_architecture* LowLevelConfig; // low-level configuration (structure defined by low-level driver)

    const t_ttc_board_features* Features;     // constant features of this board

    struct { // status flags common for all architectures
        unsigned Initialized        : 1;  // ==1: device has been initialized successfully
        // ToDo: additional high-level flags go here..
    } Flags;

    // Last returned error code
    // IMPORTANT: Every ttc_board_*() function that returns e_ttc_board_errorcode has to update this value if it returns an error!
    e_ttc_board_errorcode LastError;

    t_u8  LogicalIndex;                          // automatically set: logical index of device to use (1 = TTC_BOARD1, ...)
    t_u8  PhysicalIndex;                         // automatically set: index of physical device to use (0 = board #1, ...)
    e_ttc_board_architecture  Architecture;   // type of architecture used for current board device

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_board_config;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_BOARD_TYPES_H
