/** { rtc_stm32f1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for rtc devices on stm32f1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20150317 10:07:45 UTC
 *
 *  Note: See ttc_rtc.h for description of stm32f1xx independent RTC implementation.
 *
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "rtc_stm32f1xx.h".
//
#include "rtc_stm32f1xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

t_ttc_rtc_config* rtc_stm32f1xx_get_features( t_ttc_rtc_config* Config ) {
    Assert_RTC_Writable( Config , ttc_assert_origin_auto); // pointers must not be NULL

    static t_ttc_rtc_config Features;
    if ( !Features.LogicalIndex ) { // called first time: initialize data

#     warning Function rtc_stm32f1xx_get_features() is empty.

    }
    Features.LogicalIndex = Config->LogicalIndex;

    return ( t_ttc_rtc_config* ) 0;
}
e_ttc_rtc_errorcode rtc_stm32f1xx_deinit( t_ttc_rtc_config* Config ) {
    Assert_RTC_Writable( Config , ttc_assert_origin_auto); // pointers must not be NULL

#warning Function rtc_stm32f1xx_deinit() is empty.

    return ( e_ttc_rtc_errorcode ) 0;
}
e_ttc_rtc_errorcode rtc_stm32f1xx_init( t_ttc_rtc_config* Config ) {
    Assert_RTC_Writable( Config , ttc_assert_origin_auto); // pointers must not be NULL

#warning Function rtc_stm32f1xx_init() is empty.

    return ( e_ttc_rtc_errorcode ) 0;
}
e_ttc_rtc_errorcode rtc_stm32f1xx_load_defaults( t_ttc_rtc_config* Config ) {
    Assert_RTC_Writable( Config , ttc_assert_origin_auto); // pointers must not be NULL

#warning Function rtc_stm32f1xx_load_defaults() is empty.

    return ( e_ttc_rtc_errorcode ) 0;
}
void rtc_stm32f1xx_prepare() {


#warning Function rtc_stm32f1xx_prepare() is empty.


}
void rtc_stm32f1xx_reset( t_ttc_rtc_config* Config ) {
    Assert_RTC_Writable( Config , ttc_assert_origin_auto); // pointers must not be NULL

#warning Function rtc_stm32f1xx_reset() is empty.


}
void rtc_stm32f1xx_get_time( t_u32 Format , t_ttc_rtc_config* Config ) {
    Assert_RTC_Writable( Config , ttc_assert_origin_auto); // pointers must not be NULL

#warning Function rtc_stm32f1xx_get_time() is empty.
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions