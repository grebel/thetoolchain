#ifndef RTC_STM32F1XX_H
#define RTC_STM32F1XX_H

/** { rtc_stm32f1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for rtc devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by high-level rtc and application.
 *
 *  Created from template device_architecture.h revision 22 at 20150317 10:07:45 UTC
 *
 *  Note: See ttc_rtc.h for description of stm32f1xx independent RTC implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_RTC_STM32F1XX
//
// Implementation status of low-level driver support for rtc devices on stm32f1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_RTC_STM32F1XX '-'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_RTC_STM32F1XX == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_RTC_STM32F1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_RTC_STM32F1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "rtc_stm32f1xx.c"
//
#include "rtc_stm32f1xx_types.h"
#include "../ttc_rtc_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_rtc_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_rtc_foo
//
#define ttc_driver_rtc_deinit(Config) rtc_stm32f1xx_deinit(Config)
#define ttc_driver_rtc_get_features(Config) rtc_stm32f1xx_get_features(Config)
#define ttc_driver_rtc_init(Config) rtc_stm32f1xx_init(Config)
#define ttc_driver_rtc_load_defaults(Config) rtc_stm32f1xx_load_defaults(Config)
#define ttc_driver_rtc_prepare() rtc_stm32f1xx_prepare()
#define ttc_driver_rtc_reset(Config) rtc_stm32f1xx_reset(Config)
#define ttc_driver_rtc_get_time(Config,Time) rtc_stm32f1xx_get_time(Config,Time)

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_rtc.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_rtc.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** shutdown single RTC unit device
 * @param Config        pointer to struct t_ttc_rtc_config (must have valid value for PhysicalIndex)
 * @return              == 0: RTC has been shutdown successfully; != 0: error-code
 */
e_ttc_rtc_errorcode rtc_stm32f1xx_deinit( t_ttc_rtc_config* Config );


/** fills out given Config with maximum valid values for indexed RTC
 * @param Config  = pointer to struct t_ttc_rtc_config (must have valid value for PhysicalIndex)
 * @return        pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_rtc_config* rtc_stm32f1xx_get_features( t_ttc_rtc_config* Config );


/** initializes single RTC unit for operation
 * @param Config        pointer to struct t_ttc_rtc_config (must have valid value for PhysicalIndex)
 * @return              == 0: RTC has been initialized successfully; != 0: error-code
 */
e_ttc_rtc_errorcode rtc_stm32f1xx_init( t_ttc_rtc_config* Config );


/** loads configuration of indexed RTC unit with default values
 * @param Config        pointer to struct t_ttc_rtc_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_rtc_errorcode rtc_stm32f1xx_load_defaults( t_ttc_rtc_config* Config );


/** Prepares rtc Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void rtc_stm32f1xx_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_rtc_config (must have valid value for PhysicalIndex)
 */
void rtc_stm32f1xx_reset( t_ttc_rtc_config* Config );

void rtc_stm32f1xx_get_time( t_u32 Format, t_ttc_rtc_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _rtc_stm32f1xx_foo(t_ttc_rtc_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //RTC_STM32F1XX_H