/** { rtc_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for rtc devices on stm32l1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20141201 13:17:02 UTC
 *
 *  Note: See ttc_rtc.h for description of stm32l1xx independent RTC implementation.
 *
 *  Authors: Adrián Romero
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "rtc_stm32l1xx.h".
//
#include "rtc_stm32l1xx.h"
#include "stm32l1xx_rtc.h"
#include "stm32l1xx_pwr.h"
#include "stm32l1xx_rcc.h"
#include "../register/register_stm32l1xx.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//{ Function definitions ***************************************************



void                    rtc_stm32l1xx_prepare() {

}
t_rtc_stm32l1xx_config* rtc_stm32l1xx_get_configuration( t_ttc_rtc_config* Config ) {

    Assert_RTC( Config != NULL , ttc_assert_origin_auto);
    Assert_RTC( Config->LogicalIndex > 0 , ttc_assert_origin_auto);

    t_ttc_rtc_architecture* Config_rtc = Config->LowLevelConfig;

    if ( Config_rtc == NULL ) { // first call: allocate + init configuration

        Config_rtc = ttc_heap_alloc_zeroed( sizeof( t_ttc_rtc_config ) );

        switch ( Config->PhysicalIndex ) {         // determine base register and other low-level configuration data
            case 0: { // load low-level configuration of first rtc device
                Config_rtc->BaseRegister = NULL; // load adress of register base of RTC device
                break;
            }
            case 1: { // load low-level configuration of second rtc device
                Config_rtc->BaseRegister = NULL; // load adress of register base of RTC device
                break;
            }
            case 2: { // load low-level configuration of third rtc device
                Config_rtc->BaseRegister = NULL; // load adress of register base of RTC device
                break;
            }
            case 3: { // load low-level configuration of fourth rtc device
                Config_rtc->BaseRegister = NULL; // load adress of register base of RTC device
                break;
            }
            case 4: { // load low-level configuration of fifth rtc device
                Config_rtc->BaseRegister = NULL; // load adress of register base of RTC device
                break;
            }
            case 5: { // load low-level configuration of sixth rtc device
                Config_rtc->BaseRegister = NULL; // load adress of register base of RTC device
                break;
            }
            case 6: { // load low-level configuration of seveth rtc device
                Config_rtc->BaseRegister = NULL; // load adress of register base of RTC device
                break;
            }
            case 7: { // load low-level configuration of eighth rtc device
                Config_rtc->BaseRegister = NULL; // load adress of register base of RTC device
                break;
            }
            default: Assert_RTC( 0 , ttc_assert_origin_auto); break; // No TTC_RTCn defined! Check your makefile.100_board_* file!
        }
    }

    return Config_rtc;
}
e_ttc_rtc_errorcode     rtc_stm32l1xx_get_features( t_ttc_rtc_config* Config ) {

    Assert_RTC_Writable( Config , ttc_assert_origin_auto);
    Assert_RTC( Config->LogicalIndex > 0 , ttc_assert_origin_auto);
    if ( Config->LogicalIndex > TTC_RTC_AMOUNT ) { return ec_rtc_DeviceNotFound; }


    switch ( Config->LogicalIndex ) {         // determine features of indexed RTC device
            #ifdef TTC_RTC1
        case 1: break;
            #endif
            #ifdef TTC_RTC2
        case 2: break;
            #endif
            #ifdef TTC_RTC3
        case 3: break;
            #endif
            #ifdef TTC_RTC4
        case 4: break;
            #endif
            #ifdef TTC_RTC5
        case 5: break;
            #endif
        default: Assert_RTC( 0 , ttc_assert_origin_auto); break; // No TTC_RTCn defined! Check your makefile.100_board_* file!
    }


    /* Initialize the RTC_HourFormat member */
    Config->HourFormat = 0x00000000;

    /* Initialize the RTC_AsynchPrediv member */
    Config->AsynchPrediv = ( t_u32 )0x7F;

    /* Initialize the RTC_SynchPrediv member */
    Config->SynchPrediv = ( t_u32 )0x00FF;


    /* Set the time to 01h 00mn 00s AM */
    Config->LowLevelConfig->Time.H12     = 0x00;
    Config->LowLevelConfig->Time.Hours   = 0x01;
    Config->LowLevelConfig->Time.Minutes = 0x00;
    Config->LowLevelConfig->Time.Seconds = 0x00;

    return ec_rtc_OK;
}
t_u8                    rtc_stm32l1xx_enter_init_mode() {

    volatile t_u32 initcounter = 0x00;
    t_u32 initstatus = 0x00;

    /* Check if the Initialization mode is set */
    if ( ( register_stm32l1xx_RTC.ISR.All & ISR_INITF ) == ( t_u32 )RESET ) {

        /* Set the Initialization mode */
        register_stm32l1xx_RTC.ISR.All = ( t_u32 )ttc_rtc_INIT_MASK;

        /* Wait till RTC is in INIT state and if Time out is reached exit */
        do {
            initstatus = register_stm32l1xx_RTC.ISR.All & ISR_INITF;
            initcounter++;
        }
        while ( ( initcounter != ttc_rtc_INITMODE_TIMEOUT ) && ( initstatus == 0x00 ) );

        if ( ( register_stm32l1xx_RTC.ISR.All & ISR_INITF ) != RESET ) {
            return ec_rtc_OK;

        }
        else {

            return ec_rtc_ERROR;
        }
    }
    else {

        return ec_rtc_OK;
    }

}
void                    ttc_rtc_exit_init_mode() {

    /* Exit Initialization mode */
    register_stm32l1xx_RTC.ISR.All &= ( t_u32 )~ISR_INIT;

}
e_ttc_rtc_errorcode     rtc_stm32l1xx_init( t_ttc_rtc_config* Config ) {

    Assert_RTC_Writable( Config , ttc_assert_origin_auto);
    Assert_RTC( Config->LogicalIndex > 0 , ttc_assert_origin_auto);


    /* select RTC clock */
    _rtc_stm32l1xx_config( Config );

    if ( !( Config->Flags.Bits.Initialized ) ) {

        #if (TTC_ASSERT_RTC == 1)
        e_ttc_rtc_errorcode Error = rtc_stm32l1xx_get_features( Config );
        Assert_RTC( !Error , ttc_assert_origin_auto);
        #else
        rtc_stm32l1xx_get_features( Config );
        #endif
        /* Disable the write protection for RTC registers */
        register_stm32l1xx_RTC.WPR.All = 0xCA;
        register_stm32l1xx_RTC.WPR.All = 0x53;

        if ( rtc_stm32l1xx_enter_init_mode() == ec_rtc_ERROR ) {

            /* Enable the write protection for RTC registers */
            register_stm32l1xx_RTC.WPR.All |= 0x00FF;

            return ec_rtc_ERROR;
        }

        /* Clear RTC CR FMT Bit */
        register_stm32l1xx_RTC.CR.All &= ( ( t_u32 )~( CR_FMT ) );

        /* Set CR register */
        register_stm32l1xx_RTC.CR.All |= ( ( t_u32 )( Config->HourFormat ) );

        /* Configure the RTC PRER */
        register_stm32l1xx_RTC.PRER.All = ( t_u32 )( Config->SynchPrediv );
        register_stm32l1xx_RTC.PRER.All |= ( t_u32 )( Config->AsynchPrediv << 16 );


        /* Exit Initialization mode */
        ttc_rtc_exit_init_mode();

        /* Enable the write protection for RTC registers */
        register_stm32l1xx_RTC.WPR.All = 0xFF;

        /* Set time */
        rtc_stm32l1xx_set_time( RTC_Format_BCD, Config );

    }

    if ( Config->Flags.Bits.Alarm_A ) {

        /*Stop de Alarm A*/
        rtc_stm32l1xx_stop_alarm( ( t_u32 )0x00000100 );

        /*Stop de Alarm B*/
        rtc_stm32l1xx_stop_alarm( ( t_u32 )0x00000200 );


        RTC_WaitForSynchro();                                   /// Wait for RTC APB registers synchronisation.

        rtc_stm32l1xx_get_time( RTC_Format_BCD, Config );

        /* Set the alarm X+5s */
        Config->LowLevelConfig->Alarm_Time.H12     =  Config->LowLevelConfig->Time.H12;
        Config->LowLevelConfig->Alarm_Time.Hours   = Config->LowLevelConfig->Time.Hours;
        Config->LowLevelConfig->Alarm_Time.Minutes = ( Config->LowLevelConfig->Time.Minutes ) % 60;
        Config->LowLevelConfig->Alarm_Time.Seconds = ( Config->LowLevelConfig->Time.Seconds + 0x5 ) % 60;
        Config->LowLevelConfig->Alarm_date_week_day = 0x31;
        Config->LowLevelConfig->Alarm_date_week_day_sel = RTC_AlarmDateWeekDaySel_Date;
        Config->LowLevelConfig->Alarm_mask = RTC_AlarmMask_DateWeekDay;


        /* Set Alarm A */
        rtc_stm32l1xx_set_alarm( Format_BCD, ( t_u32 )0x00000100, Config );

        Config->Interrupt_AlarmA = ttc_interrupt_init( tit_RTC_AlarmA,
                                                       Config->PhysicalIndex,
                                                       _driver_rtc_alarmA_isr,
                                                       ( void* ) Config,
                                                       NULL,
                                                       NULL
                                                     );


        Assert_RTC_Writable( Config->Interrupt_AlarmA , ttc_assert_origin_auto);
        ttc_interrupt_enable_rtc( Config->Interrupt_AlarmA );

        /* Enable the alarmA */
        rtc_stm32l1xx_start_alarm( ( t_u32 )0x00000100 );




    }

    if ( Config->Flags.Bits.Alarm_B ) {


        /*Stop de Alarms */
        rtc_stm32l1xx_stop_alarm( ( t_u32 )0x00000100 );
        rtc_stm32l1xx_stop_alarm( ( t_u32 )0x00000200 );


        RTC_WaitForSynchro();                                   /// Wait for RTC APB registers synchronisation.

        rtc_stm32l1xx_get_time( RTC_Format_BCD, Config );

        /* Set the alarm X+5s */
        Config->LowLevelConfig->Alarm_Time.H12     =  Config->LowLevelConfig->Time.H12;
        Config->LowLevelConfig->Alarm_Time.Hours   = Config->LowLevelConfig->Time.Hours;
        Config->LowLevelConfig->Alarm_Time.Minutes = Config->LowLevelConfig->Time.Minutes;
        Config->LowLevelConfig->Alarm_Time.Seconds = ( Config->LowLevelConfig->Time.Seconds + 0x15 ) % 60;
        Config->LowLevelConfig->Alarm_date_week_day = 0x31;
        Config->LowLevelConfig->Alarm_date_week_day_sel = RTC_AlarmDateWeekDaySel_Date;
        Config->LowLevelConfig->Alarm_mask = RTC_AlarmMask_DateWeekDay;


        /* Set Alarm B */
        rtc_stm32l1xx_set_alarm( Format_BCD, ( t_u32 )0x00000200, Config );

        Config->Interrupt_AlarmB = ttc_interrupt_init( tit_RTC_AlarmB,
                                                       Config->PhysicalIndex,
                                                       _driver_rtc_alarmB_isr,
                                                       ( void* ) Config,
                                                       NULL,
                                                       NULL
                                                     );


        Assert_RTC_Writable( Config->Interrupt_AlarmB , ttc_assert_origin_auto);
        ttc_interrupt_enable_rtc( Config->Interrupt_AlarmB );

        /* Enable the alarmA */
        rtc_stm32l1xx_start_alarm( ( t_u32 )0x00000200 );




    }

    if ( Config->Flags.Bits.Timestap ) {




    }

    if ( Config->Flags.Bits.Tamper ) {


        /* Disable the Tamper 1 detection */
        rtc_stm32l1xx_stop_tamper( RTC_Tamper_1 );


        Config->Interrupt_Tamper = ttc_interrupt_init( tit_RTC_Tamper,
                                                       Config->PhysicalIndex,
                                                       _driver_rtc_tamper_isr,
                                                       ( void* ) Config,
                                                       NULL,
                                                       NULL
                                                     );

        /* Configure the Tamper 1 Trigger */
        rtc_stm32l1xx_tamper_trigger_config( RTC_Tamper_1, RTC_TamperTrigger_FallingEdge );

        Assert_RTC_Writable( Config->Interrupt_Tamper , ttc_assert_origin_auto);
        ttc_interrupt_enable_rtc( Config->Interrupt_Tamper );

        /* Enable the Tamper 1 detection */
        rtc_stm32l1xx_start_tamper( RTC_Tamper_1 );



    }

    if ( Config->Flags.Bits.WakeUp ) {


        rtc_stm32l1xx_stop_wakeup();

        Config->Interrupt_WakeUP = ttc_interrupt_init( tit_RTC_WakeUP,
                                                       Config->PhysicalIndex,
                                                       _driver_rtc_wakeup_isr,
                                                       ( void* ) Config,
                                                       NULL,
                                                       NULL
                                                     );

        rtc_stm32l1xx_wakeup_config( RTC_WakeUpClock_RTCCLK_Div16 );
        rtc_stm32l1xx_set_wakeup_counter( 0x1FFF );


        Assert_RTC_Writable( Config->Interrupt_WakeUP , ttc_assert_origin_auto);
        ttc_interrupt_enable_rtc( Config->Interrupt_WakeUP );

        rtc_stm32l1xx_start_wakeup();

    }

    if ( Config->Flags.Bits.Pin_line ) {

        Config->Interrupt_Pin_line_wakeup = ttc_interrupt_init( tit_RTC_Pin_line_wakeup,
                                                                Config->PhysicalIndex,
                                                                _driver_rtc_PinLineWakeup_isr,
                                                                ( void* ) Config,
                                                                NULL,
                                                                NULL
                                                              );
    }



    return ec_rtc_OK;
}
e_ttc_rtc_errorcode     rtc_stm32l1xx_deinit( t_ttc_rtc_config* Config ) {

    Assert_RTC_Writable( Config , ttc_assert_origin_auto);
    Assert_RTC( Config->LogicalIndex > 0 , ttc_assert_origin_auto);

    volatile t_u32 wutcounter = 0x00;
    t_u32 wutwfstatus = 0x00;
    t_u8 status = SUCCESS;

    if ( Config->Flags.Bits.Initialized ) {

        if ( rtc_stm32l1xx_enter_init_mode() == ec_rtc_ERROR ) {

            return ec_rtc_ERROR;
        }

        /* Disable the write protection for RTC registers */
        register_stm32l1xx_RTC.WPR.All = 0xCA;
        register_stm32l1xx_RTC.WPR.All = 0x53;

        /* Reset TR, DR and CR registers */
        register_stm32l1xx_RTC.TR.All = ( t_u32 )0x00000000;
        register_stm32l1xx_RTC.DR.All = ( t_u32 )0x00002101;

        /* Reset All CR bits except CR[2:0] */
        register_stm32l1xx_RTC.CR.All &= ( t_u32 )0x00000007;

        /* Wait till RTC WUTWF flag is set and if Time out is reached exit */
        do {

            wutwfstatus = register_stm32l1xx_RTC.ISR.All & ISR_WUTWF;
            wutcounter++;

        }
        while ( ( wutcounter != ttc_rtc_INITMODE_TIMEOUT ) && ( wutwfstatus == 0x00 ) );

        if ( ( register_stm32l1xx_RTC.ISR.All & ISR_WUTWF ) == RESET ) {

            status = ERROR;
        }
        else {

            /* Reset all RTC CR register bits */
            register_stm32l1xx_RTC.CR.All &= ( t_u32 )0x00000000;
            register_stm32l1xx_RTC.WUTR.All = ( t_u32 )0x0000FFFF;
            register_stm32l1xx_RTC.PRER.All = ( t_u32 )0x007F00FF;
            register_stm32l1xx_RTC.CALIBR.All = ( t_u32 )0x00000000;
            register_stm32l1xx_RTC.ALRMAR.All = ( t_u32 )0x00000000;
            register_stm32l1xx_RTC.ALRMBR.All = ( t_u32 )0x00000000;

            /* Reset ISR register and exit initialization mode */
            register_stm32l1xx_RTC.ISR.All = ( t_u32 )0x00000000;

            /* Reset Tamper and alternate functions configuration register */
            register_stm32l1xx_RTC.TAFCR.All = 0x00000000;

            if ( rtc_stm32l1xx_WaitForSynchro() == ERROR ) {
                status = ERROR;
            }
            else {
                status = SUCCESS;
            }
        }

        /* Enable the write protection for RTC registers */
        register_stm32l1xx_RTC.WPR.All = 0xFF;

        Config->Flags.Bits.Initialized = 0;
    }


    return status;
}
e_ttc_rtc_errorcode     rtc_stm32l1xx_load_defaults( t_ttc_rtc_config* Config ) {
    Assert_RTC( Config != NULL , ttc_assert_origin_auto);
    Assert_RTC( Config->LogicalIndex > 0 , ttc_assert_origin_auto);


    Config->Flags.All                        = 0;
    // RTC_Cfg->Flags.Bits.Master                = 1;


    t_ttc_rtc_architecture* ConfigArch = Config->LowLevelConfig;

    if ( ConfigArch == NULL ) {
        ConfigArch = ( t_ttc_rtc_architecture* )ttc_heap_alloc_zeroed( sizeof( t_ttc_rtc_architecture ) );
        Config->LowLevelConfig = ConfigArch;
    }
    ttc_memory_set( ConfigArch, 0, sizeof( t_ttc_rtc_architecture ) );


    return ec_rtc_OK;
}
e_ttc_rtc_errorcode     rtc_stm32l1xx_reset( t_ttc_rtc_config* Config ) {
    Assert_RTC( Config != NULL , ttc_assert_origin_auto);
    Assert_RTC( Config->LogicalIndex > 0 , ttc_assert_origin_auto);

    return ec_rtc_OK;
}
void                    rtc_stm32l1xx_get_time( t_u32 Format, t_ttc_rtc_config* Config ) {

    t_u32 tmpreg = 0;

    /* Get the TR register */
    tmpreg = ( t_u32 )( register_stm32l1xx_RTC.TR.All & TTC_TR_RESERVED_MASK );

    /* Fill the structure fields with the read parameters */
    Config->LowLevelConfig->Time.Hours = ( t_u8 )( ( tmpreg & ( TR_HT | TR_HU ) ) >> 16 );
    Config->LowLevelConfig->Time.Minutes = ( t_u8 )( ( tmpreg & ( TR_MNT | TR_MNU ) ) >> 8 );
    Config->LowLevelConfig->Time.Seconds = ( t_u8 )( tmpreg & ( TR_ST | TR_SU ) );
    Config->LowLevelConfig->Time.H12 = ( t_u8 )( ( tmpreg & ( TR_PM ) ) >> 16 );

    /* Check the input parameters format */
    if ( Format == RTC_Format_BIN ) {

        /* Convert the structure parameters to Binary format */
        Config->LowLevelConfig->Time.Hours = ( t_u8 )rtc_stm32l1xx_bcd2_to_byte( Config->LowLevelConfig->Time.Hours );
        Config->LowLevelConfig->Time.Minutes = ( t_u8 )rtc_stm32l1xx_bcd2_to_byte( Config->LowLevelConfig->Time.Minutes );
        Config->LowLevelConfig->Time.Seconds = ( t_u8 )rtc_stm32l1xx_bcd2_to_byte( Config->LowLevelConfig->Time.Seconds );
    }

}
t_u8                    rtc_stm32l1xx_WaitForSynchro() {

    volatile t_u32 synchrocounter = 0;
    t_u8 status = ERROR;
    t_u32 synchrostatus = 0x00;

    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    /* Clear RSF flag */
    register_stm32l1xx_RTC.ISR.All &= ( t_u32 )ttc_rtc_RSF_MASK;

    /* Wait the registers to be synchronised */
    do {
        synchrostatus = register_stm32l1xx_RTC.ISR.All & ISR_RSF;
        synchrocounter++;
    }
    while ( ( synchrocounter != ttc_rtc_SYNCHRO_TIMEOUT ) && ( synchrostatus == 0x00 ) );

    if ( ( register_stm32l1xx_RTC.ISR.All & ISR_RSF ) != RESET ) {
        status = SUCCESS;
    }
    else {
        status = ERROR;
    }

    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;

    return status;

}
void                    rtc_stm32l1xx_time_stamp( t_u32 TimeStampEdge, t_u8 NewState ) {

    t_u32 tmpreg = 0;

    /* Get the CR register and clear the bits to be configured */
    tmpreg = ( t_u32 )( register_stm32l1xx_RTC.CR.All & ( t_u32 )~( CR_TSEDGE | CR_TSE ) );

    /* Get the new configuration */
    if ( NewState != DISABLE ) {
        tmpreg |= ( t_u32 )( TimeStampEdge | CR_TSE );
    }
    else {
        tmpreg |= ( t_u32 )( TimeStampEdge );
    }

    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    /* Configure the Time Stamp TSEDGE and Enable bits */
    register_stm32l1xx_RTC.CR.All = ( t_u32 )tmpreg;

    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;
}
static t_u8             rtc_stm32l1xx_byte_to_bcd2( t_u8 Value ) {

    t_u8 bcdhigh = 0;

    while ( Value >= 10 ) {

        bcdhigh++;
        Value -= 10;
    }

    return ( ( t_u8 )( bcdhigh << 4 ) | Value );
}
t_u8                    rtc_stm32l1xx_bcd2_to_byte( t_u8 Value ) {

    t_u8 tmp = 0;
    tmp = ( ( t_u8 )( Value & ( t_u8 )0xF0 ) >> ( t_u8 )0x4 ) * 10;
    return ( tmp + ( Value & ( t_u8 )0x0F ) );
}
void                    rtc_stm32l1xx_set_alarm( t_u32 Format, t_u32 Alarm, t_ttc_rtc_config* Config ) {

    Assert_RTC( Config != NULL , ttc_assert_origin_auto);
    t_ttc_rtc_architecture* Rtc = ( Config->LowLevelConfig );

    t_u32 tmpreg = 0;

    if ( Format == Format_BIN ) {
        if ( ( register_stm32l1xx_RTC.CR.All & CR_FMT ) != ( t_u32 )RESET ) {
            assert_param( IS_RTC_HOUR12( Rtc->Alarm_Time.Hours ) );
            assert_param( IS_H12( Rtc->Alarm_Time.H12 ) );
        }
        else {
            Rtc->Alarm_Time.H12 = 0x00;
            assert_param( IS_RTC_HOUR24( Rtc->Alarm_Time.Hours ) );
        }
        assert_param( IS_RTC_MINUTES( Rtc->Alarm_Time.Minutes ) );
        assert_param( IS_Seconds( Rtc->Alarm_Time.Seconds ) );

        if ( Rtc->Alarm_date_week_day_sel == RTC_AlarmDateWeekDaySel_Date ) {
            assert_param( IS_RTC_ALARM_DATE_WEEKDAY_DATE( Rtc->Alarm_date_week_day ) );
        }
        else {
            assert_param( IS_RTC_ALARM_DATE_WEEKDAY_WEEKDAY( Rtc->Alarm_date_week_day ) );
        }
    }
    else {
        if ( ( register_stm32l1xx_RTC.CR.All & CR_FMT ) != ( t_u32 )RESET ) {
            tmpreg = rtc_stm32l1xx_bcd2_to_byte( Rtc->Alarm_Time.Hours );
            assert_param( IS_RTC_HOUR12( tmpreg ) );
            assert_param( IS_H12( Rtc->Alarm_Time.H12 ) );
        }
        else {
            Rtc->Alarm_Time.H12 = 0x00;
            assert_param( IS_RTC_HOUR24( rtc_stm32l1xx_bcd2_to_byte( Rtc->Alarm_Time.Hours ) ) );
        }

        assert_param( IS_RTC_MINUTES( rtc_stm32l1xx_bcd2_to_byte( Rtc->Alarm_Time.Minutes ) ) );
        assert_param( IS_Seconds( rtc_stm32l1xx_bcd2_to_byte( Rtc->Alarm_Time.Seconds ) ) );

        if ( Rtc->Alarm_date_week_day_sel == RTC_AlarmDateWeekDaySel_Date ) {
            tmpreg = rtc_stm32l1xx_bcd2_to_byte( Rtc->Alarm_date_week_day );
            assert_param( IS_RTC_ALARM_DATE_WEEKDAY_DATE( tmpreg ) );
        }
        else {
            tmpreg = rtc_stm32l1xx_bcd2_to_byte( Rtc->Alarm_date_week_day );
            assert_param( IS_RTC_ALARM_DATE_WEEKDAY_WEEKDAY( tmpreg ) );
        }
    }

    /* Check the input parameters format */
    if ( Format != Format_BIN ) {
        tmpreg = ( ( ( t_u32 )( Rtc->Alarm_Time.Hours ) << 16 ) | \
                   ( ( t_u32 )( Rtc->Alarm_Time.Minutes ) << 8 ) | \
                   ( ( t_u32 )Rtc->Alarm_Time.Seconds ) | \
                   ( ( t_u32 )( Rtc->Alarm_Time.H12 ) << 16 ) | \
                   ( ( t_u32 )( Rtc->Alarm_date_week_day ) << 24 ) | \
                   ( ( t_u32 )Rtc->Alarm_date_week_day_sel ) | \
                   ( ( t_u32 )Rtc->Alarm_mask ) );
    }
    else {
        tmpreg = ( ( ( t_u32 )rtc_stm32l1xx_byte_to_bcd2( Rtc->Alarm_Time.Hours ) << 16 ) | \
                   ( ( t_u32 )rtc_stm32l1xx_byte_to_bcd2( Rtc->Alarm_Time.Minutes ) << 8 ) | \
                   ( ( t_u32 )rtc_stm32l1xx_byte_to_bcd2( Rtc->Alarm_Time.Seconds ) ) | \
                   ( ( t_u32 )( Rtc->Alarm_Time.H12 ) << 16 ) | \
                   ( ( t_u32 )rtc_stm32l1xx_byte_to_bcd2( Rtc->Alarm_date_week_day ) << 24 ) | \
                   ( ( t_u32 )Rtc->Alarm_date_week_day_sel ) | \
                   ( ( t_u32 )Rtc->Alarm_mask ) );
    }

    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    /* Configure the Alarm register */
    if ( Alarm == RTC_Alarm_A ) {
        register_stm32l1xx_RTC.ALRMAR.All = ( t_u32 )tmpreg;
    }
    else {
        register_stm32l1xx_RTC.ALRMBR.All = ( t_u32 )tmpreg;
    }

    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;
}
e_ttc_rtc_errorcode     rtc_stm32l1xx_set_time( t_u32 Format,  t_ttc_rtc_config* Config ) {

    t_u32 tmpreg = 0;
    e_ttc_rtc_errorcode status;

    if ( Format == Format_BIN ) {
        if ( ( register_stm32l1xx_RTC.CR.All & CR_FMT ) != ( t_u32 )RESET ) {
            assert_param( IS_RTC_HOUR12( Config->LowLevelConfig->Time.Hours ) );
            assert_param( IS_RTC_H12( Config->LowLevelConfig->Time.H12 ) );
        }
        else {
            Config->LowLevelConfig->Time.H12 = 0x00;
            assert_param( IS_RTC_HOUR24( Config->LowLevelConfig->Time.H12 ) );
        }
        assert_param( IS_RTC_MINUTES( Config->LowLevelConfig->Time.Minutes ) );
        assert_param( IS_RTC_SECONDS( Config->LowLevelConfig->Time.Seconds ) );
    }
    else {
        if ( ( register_stm32l1xx_RTC.CR.All & CR_FMT ) != ( t_u32 )RESET ) {
            tmpreg = rtc_stm32l1xx_bcd2_to_byte( Config->LowLevelConfig->Time.Hours );
            assert_param( IS_RTC_HOUR12( tmpreg ) );
            assert_param( IS_RTC_H12( Config->LowLevelConfig->Time.H12 ) );
        }
        else {
            Config->LowLevelConfig->Time.H12 = 0x00;
            assert_param( IS_RTC_HOUR24( rtc_stm32l1xx_bcd2_to_byte( Config->LowLevelConfig->Time.Hours ) ) );
        }
        assert_param( IS_RTC_MINUTES( rtc_stm32l1xx_bcd2_to_byte( Config->LowLevelConfig->Time.Minutes ) ) );
        assert_param( IS_RTC_SECONDS( rtc_stm32l1xx_bcd2_to_byte( Config->LowLevelConfig->Time.Seconds ) ) );
    }

    /* Check the input parameters format */
    if ( Format != Format_BIN ) {
        tmpreg = ( ( ( t_u32 )( Config->LowLevelConfig->Time.Hours ) << 16 ) | \
                   ( ( t_u32 )( Config->LowLevelConfig->Time.Minutes ) << 8 ) | \
                   ( ( t_u32 )Config->LowLevelConfig->Time.Seconds ) | \
                   ( ( t_u32 )( Config->LowLevelConfig->Time.H12 ) << 16 ) );
    }
    else {
        tmpreg = ( t_u32 )( ( ( t_u32 )rtc_stm32l1xx_byte_to_bcd2( Config->LowLevelConfig->Time.Hours ) << 16 ) | \
                            ( ( t_u32 )rtc_stm32l1xx_byte_to_bcd2( Config->LowLevelConfig->Time.Minutes ) << 8 ) | \
                            ( ( t_u32 )rtc_stm32l1xx_byte_to_bcd2( Config->LowLevelConfig->Time.Seconds ) ) | \
                            ( ( ( t_u32 )Config->LowLevelConfig->Time.H12 ) << 16 ) );
    }

    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    /* Set Initialization mode */
    if ( rtc_stm32l1xx_enter_init_mode() == ERROR ) {
        status = ERROR;
    }
    else {
        /* Set the TR register */
        register_stm32l1xx_RTC.TR.All = ( t_u32 )( tmpreg & 0x007F7F7F );

        /* Exit Initialization mode */
        ttc_rtc_exit_init_mode();


        if ( RTC_WaitForSynchro() == ERROR ) {
            status = ERROR;
        }
        else {
            status = SUCCESS;
        }

    }
    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;

    return status;
}
e_ttc_rtc_errorcode     rtc_stm32l1xx_start_alarm( t_u32 Alarm ) {


    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    /* Enable Alarm */
    register_stm32l1xx_RTC.CR.All |= ( t_u32 )Alarm;

    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;

    return ( e_ttc_rtc_errorcode )0;
}
e_ttc_rtc_errorcode     rtc_stm32l1xx_stop_alarm( t_u32 Alarm ) {

    volatile t_u32 alarmcounter = 0x00;
    t_u32 alarmstatus = 0x00;
    e_ttc_rtc_errorcode status = ERROR;

    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    /* Disable the Alarm in CR register */
    register_stm32l1xx_RTC.CR.All &= ( t_u32 )~Alarm;

    /* Wait till RTC ALRxWF flag is set and if Time out is reached exit */
    do {
        alarmstatus = register_stm32l1xx_RTC.ISR.All & ( Alarm >> 8 );
        alarmcounter++;
    }
    while ( ( alarmcounter != INITMODE_TIMEOUT ) && ( alarmstatus == 0x00 ) );

    if ( ( register_stm32l1xx_RTC.ISR.All & ( Alarm >> 8 ) ) == RESET ) {
        status = ERROR;
    }
    else {
        status = SUCCESS;
    }

    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;

    return status;
}
void                    rtc_stm32l1xx_tamper_trigger_config( t_u32 Tamper, t_u32 TamperTrigger ) {


    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    if ( TamperTrigger == RTC_TamperTrigger_RisingEdge ) {
        /* Configure the TAFCR register */
        register_stm32l1xx_RTC.TAFCR.All &= ( t_u32 )( ( t_u32 )~( Tamper << 1 ) );
    }
    else {
        /* Configure the TAFCR register */
        register_stm32l1xx_RTC.TAFCR.All |= ( t_u32 )( Tamper << 1 );
    }


    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;
}
void                    rtc_stm32l1xx_start_tamper( t_u32 Tamper ) {

    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    /* Enable the selected Tamper pin */
    register_stm32l1xx_RTC.TAFCR.All |= ( t_u32 )Tamper;

    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;

}
void                    rtc_stm32l1xx_stop_tamper( t_u32 Tamper ) {


    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    /* Disable the selected Tamper pin */
    register_stm32l1xx_RTC.TAFCR.All &= ( t_u32 )~Tamper;

    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;

}
void                    rtc_stm32l1xx_clear_flag( t_u32 FLAG ) {

    /* Clear the Flags in the ISR register */
    register_stm32l1xx_RTC.ISR.All = ( t_u32 )( ( t_u32 )( ~( ( FLAG | ISR_INIT ) & 0x0000FFFF ) | ( t_u32 )( RTC->ISR & ISR_INIT ) ) );

}
e_ttc_rtc_errorcode     rtc_stm32l1xx_start_wakeup() {

    e_ttc_rtc_errorcode status = ERROR;

    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;


    /* Enable the Wakeup Timer */
    register_stm32l1xx_RTC.CR.All |= ( t_u32 )CR_WUTE;
    status = SUCCESS;

    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;

    return status;
}
e_ttc_rtc_errorcode     rtc_stm32l1xx_stop_wakeup() {


    volatile t_u32 wutcounter = 0x00;
    t_u32 wutwfstatus = 0x00;
    e_ttc_rtc_errorcode status = ERROR;


    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;


    /* Disable the Wakeup Timer */
    register_stm32l1xx_RTC.CR.All &= ( t_u32 )~CR_WUTE;

    /* Wait till RTC WUTWF flag is set and if Time out is reached exit */
    do {
        wutwfstatus = register_stm32l1xx_RTC.ISR.All & ISR_WUTWF;
        wutcounter++;
    }
    while ( ( wutcounter != INITMODE_TIMEOUT ) && ( wutwfstatus == 0x00 ) );

    if ( ( register_stm32l1xx_RTC.ISR.All & ISR_WUTWF ) == RESET ) {
        status = ERROR;
    }
    else {
        status = SUCCESS;
    }



    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;

    return status;
}
void                    rtc_stm32l1xx_wakeup_config( t_u32 wakeupClock ) {

    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    /* Clear the Wakeup Timer clock source bits in CR register */
    register_stm32l1xx_RTC.CR.All &= ( t_u32 )~CR_WUCKSEL;

    /* Configure the clock source */
    register_stm32l1xx_RTC.CR.All |= ( t_u32 )wakeupClock;

    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;
}
void                    rtc_stm32l1xx_set_wakeup_counter( t_u32 WakeUpCounter ) {

    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    /* Configure the Wakeup Timer counter */
    register_stm32l1xx_RTC.WUTR.All = ( t_u32 )WakeUpCounter;

    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;
}
t_u32                   rtc_stm32l1xx_get_wakeup_counter( void ) {

    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    /* Get the counter value */
    return ( ( t_u32 )( register_stm32l1xx_RTC.WUTR.All & WUTR_WUT ) );


    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;
}
void                    _rtc_stm32l1xx_alarmA_isr( t_physical_index PhysicalIndex, void* Argument ) {

    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    // clear interrupt status flag to allow next interrupt
    t_u32 FLAG = RTC_FLAG_ALRAF;

    register_stm32l1xx_RTC.ISR.All = ( t_u32 )( ( t_u32 )( ~( ( FLAG | ISR_INIT ) & 0x0000FFFF ) | ( t_u32 )( RTC->ISR & ISR_INIT ) ) );


    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;
}
void                    _rtc_stm32l1xx_alarmB_isr( t_physical_index PhysicalIndex, void* Argument ) {

    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    // clear interrupt status flag to allow next interrupt
    t_u32 FLAG = RTC_FLAG_ALRBF;

    register_stm32l1xx_RTC.ISR.All = ( t_u32 )( ( t_u32 )( ~( ( FLAG | ISR_INIT ) & 0x0000FFFF ) | ( t_u32 )( RTC->ISR & ISR_INIT ) ) );


    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;
}
void                    _rtc_stm32l1xx_wakeup_isr( t_physical_index PhysicalIndex, void* Argument ) {

    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    // clear interrupt status flag to allow next interrupt
    t_u32 FLAG = RTC_FLAG_WUTF;

    register_stm32l1xx_RTC.ISR.All = ( t_u32 )( ( t_u32 )( ~( ( FLAG | ISR_INIT ) & 0x0000FFFF ) | ( t_u32 )( RTC->ISR & ISR_INIT ) ) );


    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;

}
void                    _rtc_stm32l1xx_tamper_isr( t_physical_index PhysicalIndex, void* Argument ) {

    /* Disable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xCA;
    register_stm32l1xx_RTC.WPR.All = 0x53;

    // clear interrupt status flag to allow next interrupt
    t_u32 FLAG = RTC_FLAG_TAMP1F;

    register_stm32l1xx_RTC.ISR.All = ( t_u32 )( ( t_u32 )( ~( ( FLAG | ISR_INIT ) & 0x0000FFFF ) | ( t_u32 )( RTC->ISR & ISR_INIT ) ) );


    /* Enable the write protection for RTC registers */
    register_stm32l1xx_RTC.WPR.All = 0xFF;
}
void                    _rtc_stm32l1xx_PinLineWakeup_isr( t_physical_index PhysicalIndex, void* Argument ) {


}
e_ttc_rtc_errorcode     _rtc_stm32l1xx_config( t_ttc_rtc_config* Config ) {

    /* Enable the PWR clock */
    register_stm32l1xx_RCC.APB1ENR.Bits.PWR_EN = 1;

    /* Allow access to RTC */
    //PWR_RTCAccessCmd(ENABLE);
    register_stm32l1xx_PWR.CR.Bits.DBP = 0b1;

    #ifdef TTC_RTC_CLOCK_SOURCE_LSI

    /* LSI used as RTC source clock*/
    /* The RTC Clock may varies due to LSI frequency dispersion. */
    /* Enable the LSI OSC */
    sysclock_stm32l1xx_RCC_LSICmd( ENABLE );

    /* Wait till LSI is ready */
    while ( sysclock_stm32l1xx_RCC_GetFlagStatus( ttc_sysclock_RCC_FLAG_LSIRDY ) == RESET ) {
    }

    /* Select the RTC Clock Source */
    sysclock_stm32l1xx_RCC_RTCCLKConfig( ttc_sysclock_RCC_RTCCLKSource_LSI );


    #else
    #ifdef TTC_RTC_CLOCK_SOURCE_LSE /* LSE used as RTC source clock */

    RCC_RTCResetCmd( ENABLE );
    RCC_RTCResetCmd( DISABLE );

    PWR_RTCAccessCmd( ENABLE );

    /* Enable the LSE OSC */
    RCC_LSEConfig( RCC_LSE_ON );

    /* Wait till LSE is ready */
    while ( RCC_GetFlagStatus( RCC_FLAG_LSERDY ) == RESET )
    {}

    /* Select the RTC Clock Source */
    RCC_RTCCLKConfig( RCC_RTCCLKSource_LSE );

    /* Enable the RTC Clock */
    RCC_RTCCLKCmd( ENABLE );

    /* Wait for RTC APB registers synchronisation */
    RTC_WaitForSynchro();

    #else
    #ifdef TTC_RTC_CLOCK_SOURCE_HSE /* LSE used as RTC source clock */
    /* Enable the LSE OSC */
    sysclock_stm32l1xx_RCC_HSEConfig( ttc_sysclock_RCC_HSE_ON );
    /* Wait till LSE is ready */
    while ( sysclock_stm32l1xx_RCC_GetFlagStatus( ttc_sysclock_RCC_FLAG_HSERDY ) == RESET ) {
    }

    /* Select the RTC Clock Source */
    sysclock_stm32l1xx_RCC_RTCCLKConfig( CSR_RTCSEL_HSE ); //HSE DIVIDED BY 2

    #else
#error Please select the RTC Clock source inside the main.c file
    #endif /* RTC_CLOCK_SOURCE_LSI */
    #endif
    #endif

    /* Enable the RTC Clock */
    RCC_RTCCLKCmd( ENABLE );

    /* Wait for RTC APB registers synchronisation */
    rtc_stm32l1xx_WaitForSynchro();

    return ec_rtc_OK;
}


//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) *********************************************

//} private functions

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions