#ifndef rtc_stm32l1XX_H
#define rtc_stm32l1XX_H

/** { rtc_stm32l1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for rtc devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by high-level rtc and application.
 *
 *  Created from template device_architecture.h revision 22 at 20141201 13:17:02 UTC
 *
 *  Note: See ttc_rtc.h for description of stm32l1xx independent RTC implementation.
 *
 *  Authors: Adrián Romero
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_RTC_STM32L1XX
//
// Implementation status of low-level driver support for rtc devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_RTC_STM32L1XX 'o'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_RTC_STM32L1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_RTC_STM32L1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_RTC_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "rtc_stm32l1xxxx.c"
//
#include "rtc_stm32l1xx_types.h"
#include "../sysclock/sysclock_stm32l1xx.h"
#include "../ttc_rtc_types.h"
#include "../ttc_interrupt.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_rtc_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_rtc_foo
//
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

#define ttc_driver_rtc_init(Config)                       rtc_stm32l1xx_init(Config)
#define ttc_driver_rtc_deinit(Config)                     rtc_stm32l1xx_deinit(Config)
#define ttc_driver_rtc_reset(Config)                      rtc_stm32l1xx_reset(Config)
#define ttc_driver_rtc_get_time(Format,Config)            rtc_stm32l1xx_get_time(Format,Config)
#define ttc_driver_rtc_get_configuration(Config)          rtc_stm32l1xx_get_configuration(Config)
#define ttc_driver_rtc_get_features(Config)               rtc_stm32l1xx_get_features(Config)
#define ttc_driver_rtc_load_defaults(Config)              rtc_stm32l1xx_load_defaults(Config)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_rtc.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_rtc.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

#define TTC_TR_RESERVED_MASK    ((t_u32)0x007F7F7F)

// define all supported low-level functions for ttc_rtc_interface.h
#define ttc_driver_rtc_prepare()             rtc_stm32l1xx_prepare()
#define ttc_driver_rtc_reset(Config)         rtc_stm32l1xx_reset(Config)
#define ttc_driver_rtc_load_defaults(Config) rtc_stm32l1xx_load_defaults(Config)
#define ttc_driver_rtc_deinit(Config)        rtc_stm32l1xx_deinit(Config)
#define ttc_driver_rtc_init(Config)          rtc_stm32l1xx_init(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Function prototypes **************************************************

/** Prepares rtc Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void rtc_stm32l1xx_prepare();

/** checks if RTC driver already has been initialized
 * @param Config        pointer to struct t_ttc_rtc_config (must have valid value for LogicalIndex)
 * @return              == TRUE: indexed RTC unit is already initialized
 */
bool rtc_stm32l1xx_check_initialized( t_ttc_rtc_config* Config );

/** returns reference to low-level configuration struct of RTC driver
 * @param Config        pointer to struct t_ttc_rtc_config (must have valid value for LogicalIndex)
 * @return              pointer to struct t_ttc_rtc_config (will assert if no configuration available)
 */
t_rtc_stm32l1xx_config* rtc_stm32l1xx_get_configuration( t_ttc_rtc_config* Config );

/** loads configuration of RTC driver with default values
 * @param Config        pointer to struct t_ttc_rtc_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_rtc_errorcode rtc_stm32l1xx_load_defaults( t_ttc_rtc_config* Config );

/** fills out given Config with maximum valid values for indexed RTC
 * @param Config        pointer to struct t_ttc_rtc_config (must have valid value for LogicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_rtc_errorcode rtc_stm32l1xx_get_features( t_ttc_rtc_config* Config );

/** initializes single RTC
 * @param Config        pointer to struct t_ttc_rtc_config (must have valid value for LogicalIndex)
 * @return              == 0: RTC has been initialized successfully; != 0: error-code
 */
e_ttc_rtc_errorcode rtc_stm32l1xx_init( t_ttc_rtc_config* Config );

/** shutdown single RTC device
 * @param Config        pointer to struct t_ttc_rtc_config (must have valid value for LogicalIndex)
 * @return              == 0: RTC has been shutdown successfully; != 0: error-code
 */
e_ttc_rtc_errorcode rtc_stm32l1xx_deinit( t_ttc_rtc_config* Config );

/** reset configuration of rtc driver and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_RTC_get_max_LogicalIndex())
 */
e_ttc_rtc_errorcode rtc_stm32l1xx_reset( t_ttc_rtc_config* Config );

void rtc_stm32l1xx_get_time( t_u32 Format, t_ttc_rtc_config* Config );

e_ttc_rtc_errorcode rtc_stm32l1xx_set_time( t_u32 Format,  t_ttc_rtc_config* Config );

e_ttc_rtc_errorcode _rtc_stm32l1xx_config( t_ttc_rtc_config* Config );
/**
  * @brief  Waits until the RTC Time and Date registers (TR and DR) are
  *         synchronized with RTC APB clock.
  * @note   The RTC Resynchronization mode is write protected, use the
  *         RTC_WriteProtectionCmd(DISABLE) before calling this function.
  * @note   To read the calendar through the shadow registers after Calendar
  *         initialization, calendar update or after wakeup from low power modes
  *         the software must first clear the RSF flag.
  *         The software must then wait until it is set again before reading
  *         the calendar, which means that the calendar registers have been
  *         correctly copied into the TR and DR shadow registers.
  * @param  None
  * @retval An ErrorStatus enumeration value:
  *          - SUCCESS: RTC registers are synchronised
  *          - ERROR: RTC registers are not synchronised
  */
t_u8 rtc_stm32l1xx_WaitForSynchro();
void rtc_stm32l1xx_time_stamp( t_u32 TimeStampEdge, t_u8 NewState );
/**
  * @brief  Set the specified RTC Alarm.
  * @note   The Alarm register can only be written when the corresponding Alarm
  *         is disabled (Use the RTC_AlarmCmd(DISABLE)).
  * @param  Format: specifies the format of the returned parameters.
  *   This parameter can be one of the following values:
  *     @arg Format_BIN: Binary data format
  *     @arg Format_BCD: BCD data format
  * @param  RTC_Alarm: specifies the alarm to be configured.
  *   This parameter can be one of the following values:
  *     @arg RTC_Alarm_A: to select Alarm A
  *     @arg RTC_Alarm_B: to select Alarm B
  * @param  Rtc: pointer to a RTC_AlarmTypeDef structure that
  *                          contains the alarm configuration parameters.
  * @retval None
  */
void rtc_stm32l1xx_set_alarm( t_u32 Format, t_u32 Alarm, t_ttc_rtc_config* Config );
e_ttc_rtc_errorcode rtc_stm32l1xx_start_alarm( t_u32 Alarm );
e_ttc_rtc_errorcode rtc_stm32l1xx_stop_alarm( t_u32 Alarm );
/**
  * @brief  Configures the select Tamper pin edge.
  * @param  RTC_Tamper: Selected tamper pin.
  *   This parameter can be RTC_Tamper_1.
  * @param  RTC_TamperTrigger: Specifies the trigger on the tamper pin that
  *         stimulates tamper event.
  *   This parameter can be one of the following values:
  *     @arg RTC_TamperTrigger_RisingEdge: Rising Edge of the tamper pin causes tamper event.
  *     @arg RTC_TamperTrigger_FallingEdge: Falling Edge of the tamper pin causes tamper event.
  * @retval None
  */
void rtc_stm32l1xx_tamper_trigger_config( t_u32 Tamper, t_u32 TamperTrigger );
void rtc_stm32l1xx_start_tamper( t_u32 Tamper );
void rtc_stm32l1xx_stop_tamper( t_u32 Tamper );
/**
  * @brief  Clears the RTC's pending flags.
  * @param  RTC_FLAG: specifies the RTC flag to clear.
  *   This parameter can be any combination of the following values:
  *     @arg RTC_FLAG_TAMP1F: Tamper 1 event flag
  *     @arg RTC_FLAG_TSOVF: Time Stamp Overflow flag
  *     @arg RTC_FLAG_TSF: Time Stamp event flag
  *     @arg RTC_FLAG_WUTF: WakeUp Timer flag
  *     @arg RTC_FLAG_ALRBF: Alarm B flag
  *     @arg RTC_FLAG_ALRAF: Alarm A flag
  *     @arg RTC_FLAG_RSF: Registers Synchronized flag
  * @retval None
  */
void rtc_stm32l1xx_clear_flag( t_u32 FLAG );

void rtc_stm32l1xx_wakeup_config( t_u32 wakeupClock );
void rtc_stm32l1xx_set_wakeup_counter( t_u32 WakeUpCounter );
t_u32 rtc_stm32l1xx_get_wakeup_counter( void );
e_ttc_rtc_errorcode rtc_stm32l1xx_start_wakeup();
e_ttc_rtc_errorcode rtc_stm32l1xx_stop_wakeup();

/**
  * @brief  Exits the RTC Initialization mode.
  * @note   When the initialization sequence is complete, the calendar restarts
  *         counting after 4 RTCCLK cycles.
  * @note   The RTC Initialization mode is write protected, use the
  *         RTC_WriteProtectionCmd(DISABLE) before calling this function.
  * @param  None
  * @retval None
  */
void ttc_rtc_exit_init_mode();
/**
  * @brief  Convert from 2 digit BCD to Binary.
  * @param  Value: BCD value to be converted.
  * @retval Converted word
  */

t_u8 rtc_stm32l1xx_bcd2_to_byte( t_u8 Value );

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) *********************************************

//} private functions

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions


//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _rtc_stm32l1xxxx_foo(t_ttc_rtc_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

void _rtc_stm32l1xx_alarmA_isr( t_physical_index PhysicalIndex, void* Argument );
void _rtc_stm32l1xx_alarmB_isr( t_physical_index PhysicalIndex, void* Argument );
void _rtc_stm32l1xx_wakeup_isr( t_physical_index PhysicalIndex, void* Argument );
void _rtc_stm32l1xx_tamper_isr( t_physical_index PhysicalIndex, void* Argument );
void _rtc_stm32l1xx_PinLineWakeup_isr( t_physical_index PhysicalIndex, void* Argument );


//}PrivateFunctions

#endif //rtc_stm32l1xxXX_H