#ifndef ARCHITECTURE_RTC_TYPES_H
#define ARCHITECTURE_RTC_TYPES_H

/** { rtc_stm32l1.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *                     
 *  Low-Level datatypes for RTC devices on stm32l1 architectures.
 *  Structures, Enums and Defines being required by ttc_rtc_types.h
 *
 *  Note: See ttc_rtc.h for description of architecture independent RTC implementation.
 * 
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ******************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_rtc_types.h ***********************

typedef struct { // register description (adapt according to stm32l1 registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} t_rtc_register;

typedef struct {  // stm32l1 specific configuration data of single RTC device
  t_rtc_register* BaseRegister;       // base address of RTC device registers
} __attribute__((__packed__)) t_rtc_stm32l1_config;

// t_ttc_rtc_arch is required by ttc_rtc_types.h
typedef t_rtc_stm32l1_config t_ttc_rtc_arch;

//} Structures/ Enums


#endif //ARCHITECTURE_RTC_TYPES_H
