#ifndef RTC_STM32L1XX_TYPES_H
#define RTC_STM32L1XX_TYPES_H

/** { rtc_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for RTC devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_rtc_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20141201 13:17:02 UTC
 *
 *  Note: See ttc_rtc.h for description of architecture independent RTC implementation.
 *
 *  Authors: Adrián Romero
 *
}*/
//{ Defines/ TypeDefs **********************************************************
#define TTC_RTC_MAX_AMOUNT 3

//InsertDefines above (DO NOT REMOVE THIS LINE!)
#define ttc_rtc_FLAG_TAMP1F                   ((t_u32)0x00002000)
#define ttc_rtc_FLAG_TSOVF                    ((t_u32)0x00001000)
#define ttc_rtc_FLAG_TSF                      ((t_u32)0x00000800)
#define ttc_rtc_FLAG_WUTF                     ((t_u32)0x00000400)
#define ttc_rtc_FLAG_ALRBF                    ((t_u32)0x00000200)
#define ttc_rtc_FLAG_ALRAF                    ((t_u32)0x00000100)
#define ttc_rtc_FLAG_INITF                    ((t_u32)0x00000040)
#define ttc_rtc_FLAG_RSF                      ((t_u32)0x00000020)
#define ttc_rtc_FLAG_INITS                    ((t_u32)0x00000010)
#define ttc_rtc_FLAG_WUTWF                    ((t_u32)0x00000004)
#define ttc_rtc_FLAG_ALRBWF                   ((t_u32)0x00000002)
#define ttc_rtc_FLAG_ALRAWF                   ((t_u32)0x00000001)
#define ttc_rtc_IS_RTC_GET_FLAG(FLAG) (((FLAG) == ttc_rtc_FLAG_TSOVF) || ((FLAG) == ttc_rtc_FLAG_TSF) || \
                                       ((FLAG) == ttc_rtc_FLAG_WUTF) || ((FLAG) == ttc_rtc_FLAG_ALRBF) || \
                                       ((FLAG) == ttc_rtc_FLAG_ALRAF) || ((FLAG) == ttc_rtc_FLAG_INITF) || \
                                       ((FLAG) == ttc_rtc_FLAG_RSF) || ((FLAG) == ttc_rtc_FLAG_WUTWF) || \
                                       ((FLAG) == ttc_rtc_FLAG_ALRBWF) || ((FLAG) == ttc_rtc_FLAG_ALRAWF) || \
                                       ((FLAG) == ttc_rtc_FLAG_TAMP1F))


#define ttc_rtc_TR_RESERVED_MASK    ((t_u32)0x007F7F7F)
#define ttc_rtc_DR_RESERVED_MASK    ((t_u32)0x00FFFF3F)
#define ttc_rtc_INIT_MASK           ((t_u32)0xFFFFFFFF)
#define ttc_rtc_RSF_MASK            ((t_u32)0xFFFFFF5F)
#define ttc_rtc_FLAGS_MASK          ((t_u32)(ttc_rtc_FLAG_TSOVF | ttc_rtc_FLAG_TSF | ttc_rtc_FLAG_WUTF | \
                                             ttc_rtc_FLAG_ALRBF | ttc_rtc_FLAG_ALRAF | ttc_rtc_FLAG_INITF | \
                                             ttc_rtc_FLAG_RSF | ttc_rtc_FLAG_INITS | ttc_rtc_FLAG_WUTWF | \
                                             ttc_rtc_FLAG_ALRBWF | ttc_rtc_FLAG_ALRAWF | ttc_rtc_FLAG_TAMP1F ))

#define ttc_rtc_INITMODE_TIMEOUT         ((t_u32) 0x00002000)
#define ttc_rtc_SYNCHRO_TIMEOUT          ((t_u32) 0x00001000)

#define ttc_rtc_TimeStampEdge_Rising          ((t_u32)0x00000000)
#define ttc_rtc_TimeStampEdge_Falling         ((t_u32)0x00000008)
#define ttc_rtc_IS_RTC_TIMESTAMP_EDGE(EDGE) (((EDGE) == ttc_rtc_TimeStampEdge_Rising) || \
                                             ((EDGE) == ttc_rtc_TimeStampEdge_Falling))

#define Format_BIN                    ((t_u32)0x000000000)
#define Format_BCD                    ((t_u32)0x000000001)

#define INITMODE_TIMEOUT         ((t_u32) 0x00002000)
#define SYNCHRO_TIMEOUT          ((t_u32) 0x00001000)

#undef TTC_RTC_CLOCK_SOURCE_LSI
//#undef TTC_RTC_CLOCK_SOURCE_LSE
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "stm32l1xx.h"
#include "../sysclock/sysclock_stm32l1xx.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_rtc_types.h *************************


#define _driver_rtc_alarmA_isr                    _rtc_stm32l1xx_alarmA_isr // define function pointer for receive interrupt service routine
#define _driver_rtc_alarmB_isr                    _rtc_stm32l1xx_alarmB_isr // define function pointer for receive interrupt service routine
#define _driver_rtc_wakeup_isr                    _rtc_stm32l1xx_wakeup_isr // define function pointer for receive interrupt service routine
#define _driver_rtc_tamper_isr                    _rtc_stm32l1xx_tamper_isr
#define _driver_rtc_PinLineWakeup_isr             _rtc_stm32l1xx_PinLineWakeup_isr

typedef struct { // register description (adapt according to stm32l1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_rtc_register;

typedef struct {

    t_u8 Hours;    /*!< Specifies the RTC Time Hour.
                        This parameter must be set to a value in the 0-12 range
                        if the RTC_HourFormat_12 is selected or 0-23 range if
                        the RTC_HourFormat_24 is selected. */

    t_u8 Minutes;  /*!< Specifies the RTC Time Minutes.
                        This parameter must be set to a value in the 0-59 range. */

    t_u8 Seconds;  /*!< Specifies the RTC Time Seconds.
                        This parameter must be set to a value in the 0-59 range. */

    t_u8 H12;      /*!< Specifies the RTC AM/PM Time.
                        This parameter can be a value of @ref RTC_AM_PM_Definitions */

} rtc_stm32l1xx_time_format;


typedef struct {  // stm32l1xx specific configuration data of single RTC device
    t_register_stm32l1xx_rtc* BaseRegister;       // base address of RTC device registers

    rtc_stm32l1xx_time_format Alarm_Time;     /*!< Specifies the RTC Alarm Time members. */
    rtc_stm32l1xx_time_format Time;     /*!< Specifies the RTC Alarm Time members. */


    t_u32 Alarm_mask;              /*!< Specifies the RTC Alarm Masks.
                                     This parameter can be a value of @ref RTC_AlarmMask_Definitions */

    t_u32 Alarm_date_week_day_sel;  /*!< Specifies the RTC Alarm is on Date or WeekDay.
                                     This parameter can be a value of @ref RTC_AlarmDateWeekDay_Definitions */

    t_u8 Alarm_date_week_day;      /*!< Specifies the RTC Alarm Date/WeekDay.
                                     This parameter must be set to a value in the 1-31 range
                                     if the Alarm Date is selected.
                                     This parameter can be a value of @ref R */

    e_ttc_gpio_pin Pin_wakeup;
    t_u32 Line;

} __attribute__( ( __packed__ ) ) t_rtc_stm32l1xx_config;

// t_ttc_rtc_architecture is required by ttc_rtc_types.h
#define t_ttc_rtc_architecture t_rtc_stm32l1xx_config


//} Structures/ Enums


#endif //RTC_STM32L1XX_TYPES_H
