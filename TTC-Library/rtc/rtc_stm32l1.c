/** { rtc_stm32l1.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for rtc devices on stm32l1 architectures.
 *  Implementation of low-level driver. 
 *    
 *  Note: See ttc_rtc.h for description of stm32l1 independent RTC implementation.
 * 
 *  Authors: Gregor Rebel
}*/

#include "rtc_stm32l1.h"

//{ Function definitions ***************************************************

void rtc_stm32l1_prepare() {
  // add your startup code here (Singletasking!)
  
}
bool rtc_stm32l1_check_initialized(t_ttc_rtc_config* Config) {

    if (Config->Flags.Bits.Initialized)
        return TRUE;

    return FALSE;
}
t_rtc_stm32l1_config* rtc_stm32l1_get_configuration(t_ttc_rtc_config* Config) {
    Assert_RTC(Config != NULL, ttc_assert_origin_auto);
    Assert_RTC(Config->LogicalIndex > 0, ttc_assert_origin_auto);

    t_rtc_stm32l1_config* Config_stm32l1 = Config->LowLevelConfig;
    if (Config_stm32l1 == NULL) {  // first call: allocate + init configuration
      Config_stm32l1 = ttc_memory_alloc_zeroed( sizeof(t_ttc_rtc_arch) );
      
      switch (Config->PhysicalIndex) {           // determine base register and other low-level configuration data
         case 0: { // load low-level configuration of first rtc device 
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of RTC device
           break;
         }
         case 1: { // load low-level configuration of second rtc device
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of RTC device
           break;
         } 
         case 2: { // load low-level configuration of third rtc device
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of RTC device
           break;             
         }
         case 3: { // load low-level configuration of fourth rtc device
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of RTC device
           break;
         }
         case 4: { // load low-level configuration of fifth rtc device
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of RTC device
           break;
         }
         case 5: { // load low-level configuration of sixth rtc device
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of RTC device
           break;
         }
         case 6: { // load low-level configuration of seveth rtc device
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of RTC device
           break;
         }
         case 7: { // load low-level configuration of eighth rtc device
           Config_stm32l1->BaseRegister = NULL; // load adress of register base of RTC device
           break;
         }
      default: Assert_RTC(0, ttc_assert_origin_auto); break; // No TTC_RTCn defined! Check your makefile.100_board_* file!
      }
    }
    
    Assert_RTC(Config_stm32l1, ttc_assert_origin_auto);
    return Config_stm32l1;
}
e_ttc_rtc_errorcode rtc_stm32l1_get_features(t_ttc_rtc_config* Config) {
    Assert_RTC(Config,    ttc_assert_origin_auto);
    Assert_RTC(Config->LogicalIndex > 0, ttc_assert_origin_auto);
    if (Config->LogicalIndex > TTC_RTC_AMOUNT) return ec_rtc_DeviceNotFound;

    
    Config->Flags.All                        = 0;
    Config->Flags.Bits.HourFormat_12         = 0;
    Config->Flags.Bits.Timestamp_ISR         = 0;
    Config->Flags.Bits.Wakeup_ISR            = 0;
    Config->Flags.Bits.Alarm_B_ISR           = 0;
    Config->Flags.Bits.Alarm_A_ISR           = 0;
    Config->Flags.Bits.Timestap              = 0;
    Config->Flags.Bits.Alarm_A               = 0;
    Config->Flags.Bits.Alarm_B               = 0;
    //    Config->Flags.Bits.Initialized           = 0;
    //    Config->Flags.Bits.Initialized           = 0;
    //    Config->Flags.Bits.Initialized           = 0;
    //    Config->Flags.Bits.Initialized           = 0;
    //    Config->Flags.Bits.Initialized           = 0;

    switch (Config->LogicalIndex) {           // determine features of indexed RTC device
#ifdef TTC_RTC1
      case 1: break;
#endif
#ifdef TTC_RTC2
      case 2: break;
#endif
#ifdef TTC_RTC3
      case 3: break;
#endif
#ifdef TTC_RTC4
      case 4: break;
#endif
#ifdef TTC_RTC5
      case 5: break;
#endif
      default: Assert_RTC(0, ttc_assert_origin_auto); break; // No TTC_RTCn defined! Check your makefile.100_board_* file!
    }
    
    return ec_rtc_OK;
}
e_ttc_rtc_errorcode rtc_stm32l1_init(t_ttc_rtc_config* Config) {
     Assert_RTC(Config, ttc_assert_origin_auto);
    Assert_RTC(Config->LogicalIndex > 0, ttc_assert_origin_auto);
    t_real_time_clock_stm32l1_config* Config_stm32l1 = real_time_clock_stm32l1_get_configuration(Config);
    (void) Config_stm32l1; // avoid warning: "unused variable"

    if (1) { // validate RTC_Features
        t_ttc_real_time_clock_config RTC_Features;
        RTC_Features.LogicalIndex = Config->LogicalIndex;
        e_ttc_real_time_clock_errorcode Error = real_time_clock_stm32l1_get_features(&RTC_Features);
        if (Error) return Error;
        Config->Flags.All &= RTC_Features.Flags.All; // mask out unavailable flags
    }
    /*
    switch (Config->LogicalIndex) {                // find RTC corresponding to RTC_index as defined by makefile.100_board_*
#ifdef TTC_RTC1
    case 1: Config_stm32l1->Base = (RTC_t*) TTC_RTC1; break;
#endif
#ifdef TTC_RTC2
    case 2: Config_stm32l1->Base = (RTC_t*) TTC_RTC2; break;
#endif
    default: Assert_RTC(0, ttc_assert_origin_auto); break; // No TTC_RTCn defined! Check your makefile.100_board_* file!
    }
    */

    _real_time_clock_stm32l1_config(Config);

    Config->Flags.Bits.Initialized=1;
    return ec_real_time_clock_OK;
}
e_ttc_real_time_clock_errorcode real_time_clock_stm32l1_deinit(t_ttc_real_time_clock_config* Config) {
    Assert_RTC(Config, ttc_assert_origin_auto);
    Assert_RTC(Config->LogicalIndex > 0, ttc_assert_origin_auto);
    t_real_time_clock_stm32l1_config* Config_stm32l1 = real_time_clock_stm32l1_get_configuration(Config);
    (void) Config_stm32l1; // avoid warning: "unused variable"

    // ToDo: Deinitialize hardware device
    Config->Flags.Bits.Initialized=0;
    return ec_real_time_clock_OK;
}
e_ttc_rtc_errorcode rtc_stm32l1_deinit(t_ttc_rtc_config* Config) {
    Assert_RTC(Config, ttc_assert_origin_auto);
    Assert_RTC(Config->LogicalIndex > 0, ttc_assert_origin_auto);
    t_rtc_stm32l1_config* Config_stm32l1 = rtc_stm32l1_get_configuration(Config);
    (void) Config_stm32l1; // avoid warning: "unused variable"

    // ToDo: Deinitialize hardware device
    
    return ec_rtc_OK;
}
e_ttc_rtc_errorcode rtc_stm32l1_load_defaults(t_ttc_rtc_config* Config) {
    Assert_RTC(Config != NULL, ttc_assert_origin_auto);
    Assert_RTC(Config->LogicalIndex > 0, ttc_assert_origin_auto);

    Config->Flags.All                        = 0;
    // RTC_Cfg->Flags.Bits.Master                = 1;

    return ec_rtc_OK;
}
e_ttc_rtc_errorcode  rtc_stm32l1_reset(t_ttc_rtc_config* Config) {
    Assert_RTC(Config != NULL, ttc_assert_origin_auto);
    Assert_RTC(Config->LogicalIndex > 0, ttc_assert_origin_auto);
  
    return ec_rtc_OK;
}
e_ttc_rtc_errorcode  rtc_stm32l1_get_time(t_u8 LogicalIndex,t_ttc_rtc_time* Time) {
    Assert_RTC(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    Assert_RTC(Time,ttc_assert_origin_auto); //Time isn't initialized
    PWR_RTCAccessCmd(ENABLE);
    t_u32 tmpreg = 0;
    /* Get the TR register */
    tmpreg = (t_u32)(RTC->TR & TTC_TR_RESERVED_MASK);

    /* Fill the structure fields with the read parameters */
    Time->sec = (t_u8)((tmpreg & (TR_SU)));
    Time->dec_sec = (t_u8)((tmpreg & (TR_ST))>>4);
    Time->min = (t_u8)((tmpreg & (TR_MNU))>>8);
    Time->dec_min = (t_u8)((tmpreg & (TR_MNT)) >> 12);
    Time->hour = (t_u8)((tmpreg & (TR_HU)) >> 16);
    Time->dec_hour = (t_u8)((tmpreg & (TR_MNT)) >>20);

    return ec_rtc_OK;
}
e_ttc_rtc_errorcode _rtc_stm32l1_config(t_ttc_rtc_config* Config){
    /* Enable the PWR clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

    /* Allow access to RTC */
    PWR_RTCAccessCmd(ENABLE);

#ifdef TTC_RTC_CLOCK_SOURCE_LSI  /* LSI used as RTC source clock*/
    /* The RTC Clock may varies due to LSI frequency dispersion. */
    /* Enable the LSI OSC */
    RCC_LSICmd(ENABLE);

    /* Wait till LSI is ready */
    while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
    {
    }

    /* Select the RTC Clock Source */
    RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);

    Config->SynchPrediv = 0xFF;
    Config->AsynchPrediv = 0x7F;

#else
#ifdef TTC_RTC_CLOCK_SOURCE_LSE /* LSE used as RTC source clock */
    /* Enable the LSE OSC */
    RCC_LSEConfig(RCC_LSE_ON);

    /* Wait till LSE is ready */
    while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
    {
    }

    /* Select the RTC Clock Source */
    RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

    Config->SynchPrediv = 0xFF;
    Config->AsynchPrediv = 0x7F;

#else
#error Please select the RTC Clock source inside the main.c file
#endif /* RTC_CLOCK_SOURCE_LSI */
#endif
    /* Enable the RTC Clock */
    RCC_RTCCLKCmd(ENABLE);

    /* Wait for RTC APB registers synchronisation */
    RTC_WaitForSynchro();

    /* Enable The TimeStamp */
    if(Config->Flags.Bits.Timestap)
        RTC_TimeStampCmd(RTC_TimeStampEdge_Falling, ENABLE);
    PWR_RTCAccessCmd(DISABLE);
    return ec_rtc_OK;
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) *********************************************

//} private functions
