#ifndef RTC_STM32F1XX_TYPES_H
#define RTC_STM32F1XX_TYPES_H

/** { rtc_stm32f1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for RTC devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by ttc_rtc_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150317 10:07:45 UTC
 *
 *  Note: See ttc_rtc.h for description of architecture independent RTC implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_rtc_types.h *************************

typedef struct { // register description (adapt according to stm32f1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_rtc_register;

typedef struct {  // stm32f1xx specific configuration data of single RTC device
    t_rtc_register* BaseRegister;       // base address of RTC device registers
} __attribute__( ( __packed__ ) ) t_rtc_stm32f1xx_config;

// t_ttc_rtc_architecture is required by ttc_rtc_types.h
#define t_ttc_rtc_architecture t_rtc_stm32f1xx_config

//} Structures/ Enums


#endif //RTC_STM32F1XX_TYPES_H
