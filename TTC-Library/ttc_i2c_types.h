/** { ttc_i2c_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for I2C device.
 *  Structures, Enums and Defines being required by both, high- and low-level i2c.
 *  This file does not provide function declarations!
 *
 *  Created from template ttc_device_types.h revision 31 at 20150527 12:35:09 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_I2C_TYPES_H
#define TTC_I2C_TYPES_H

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_i2c.h" or "ttc_i2c.c"
//
#include "ttc_basic_types.h"
#include "ttc_gpio_types.h"
#include "ttc_mutex_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//? #ifdef TTC_COMPILE_OPTS_H
//? #  error DO NOT INCLUDE compile_options.h before including this file! It make it impossible to correctly detect static configuration from makefile.
//? #endif

#ifdef EXTENSION_i2c_stm32l1xx
    #include "i2c/i2c_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_i2c_stm32f1xx
    #include "i2c/i2c_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

/** {  TTC_I2Cn has to be defined as constant by makefile.100_board_*
 *
 * Here we count how much devices have been defined and if all lower index devices have been defined too.
 */
#ifndef TTC_I2C_AMOUNT //{ 10
    #ifdef TTC_I2C10
        #ifndef TTC_I2C9
            #error TTC_I2C10 is defined, but not TTC_I2C9 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C8
            #error TTC_I2C10 is defined, but not TTC_I2C8 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C7
            #error TTC_I2C10 is defined, but not TTC_I2C7 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C6
            #error TTC_I2C10 is defined, but not TTC_I2C6 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C5
            #error TTC_I2C10 is defined, but not TTC_I2C5 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C4
            #error TTC_I2C10 is defined, but not TTC_I2C4 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C3
            #error TTC_I2C10 is defined, but not TTC_I2C3 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C2
            #error TTC_I2C10 is defined, but not TTC_I2C2 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C1
            #error TTC_I2C10 is defined, but not TTC_I2C1 - all lower TTC_I2Cn must be defined!
        #endif

        #define TTC_I2C_AMOUNT 10
    #endif
#endif //}
#ifndef TTC_I2C_AMOUNT //{ 9
    #ifdef TTC_I2C9
        #ifndef TTC_I2C8
            #error TTC_I2C9 is defined, but not TTC_I2C8 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C7
            #error TTC_I2C9 is defined, but not TTC_I2C7 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C6
            #error TTC_I2C9 is defined, but not TTC_I2C6 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C5
            #error TTC_I2C9 is defined, but not TTC_I2C5 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C4
            #error TTC_I2C9 is defined, but not TTC_I2C4 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C3
            #error TTC_I2C9 is defined, but not TTC_I2C3 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C2
            #error TTC_I2C9 is defined, but not TTC_I2C2 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C1
            #error TTC_I2C9 is defined, but not TTC_I2C1 - all lower TTC_I2Cn must be defined!
        #endif

        #define TTC_I2C_AMOUNT 9
    #endif
#endif //}
#ifndef TTC_I2C_AMOUNT //{ 8
    #ifdef TTC_I2C8
        #ifndef TTC_I2C7
            #error TTC_I2C8 is defined, but not TTC_I2C7 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C6
            #error TTC_I2C8 is defined, but not TTC_I2C6 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C5
            #error TTC_I2C8 is defined, but not TTC_I2C5 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C4
            #error TTC_I2C8 is defined, but not TTC_I2C4 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C3
            #error TTC_I2C8 is defined, but not TTC_I2C3 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C2
            #error TTC_I2C8 is defined, but not TTC_I2C2 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C1
            #error TTC_I2C8 is defined, but not TTC_I2C1 - all lower TTC_I2Cn must be defined!
        #endif

        #define TTC_I2C_AMOUNT 8
    #endif
#endif //}
#ifndef TTC_I2C_AMOUNT //{ 7
    #ifdef TTC_I2C7
        #ifndef TTC_I2C6
            #error TTC_I2C7 is defined, but not TTC_I2C6 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C5
            #error TTC_I2C7 is defined, but not TTC_I2C5 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C4
            #error TTC_I2C7 is defined, but not TTC_I2C4 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C3
            #error TTC_I2C7 is defined, but not TTC_I2C3 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C2
            #error TTC_I2C7 is defined, but not TTC_I2C2 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C1
            #error TTC_I2C7 is defined, but not TTC_I2C1 - all lower TTC_I2Cn must be defined!
        #endif

        #define TTC_I2C_AMOUNT 7
    #endif
#endif //}
#ifndef TTC_I2C_AMOUNT //{ 6
    #ifdef TTC_I2C6
        #ifndef TTC_I2C5
            #error TTC_I2C6 is defined, but not TTC_I2C5 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C4
            #error TTC_I2C6 is defined, but not TTC_I2C4 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C3
            #error TTC_I2C6 is defined, but not TTC_I2C3 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C2
            #error TTC_I2C6 is defined, but not TTC_I2C2 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C1
            #error TTC_I2C6 is defined, but not TTC_I2C1 - all lower TTC_I2Cn must be defined!
        #endif

        #define TTC_I2C_AMOUNT 6
    #endif
#endif //}
#ifndef TTC_I2C_AMOUNT //{ 5
    #ifdef TTC_I2C5
        #ifndef TTC_I2C4
            #error TTC_I2C5 is defined, but not TTC_I2C4 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C3
            #error TTC_I2C5 is defined, but not TTC_I2C3 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C2
            #error TTC_I2C5 is defined, but not TTC_I2C2 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C1
            #error TTC_I2C5 is defined, but not TTC_I2C1 - all lower TTC_I2Cn must be defined!
        #endif

        #define TTC_I2C_AMOUNT 5
    #endif
#endif //}
#ifndef TTC_I2C_AMOUNT //{ 4
    #ifdef TTC_I2C4
        #ifndef TTC_I2C3
            #error TTC_I2C4 is defined, but not TTC_I2C3 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C2
            #error TTC_I2C4 is defined, but not TTC_I2C2 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C1
            #error TTC_I2C4 is defined, but not TTC_I2C1 - all lower TTC_I2Cn must be defined!
        #endif

        #define TTC_I2C_AMOUNT 4
    #endif
#endif //}
#ifndef TTC_I2C_AMOUNT //{ 3
    #ifdef TTC_I2C3

        #ifndef TTC_I2C2
            #error TTC_I2C3 is defined, but not TTC_I2C2 - all lower TTC_I2Cn must be defined!
        #endif
        #ifndef TTC_I2C1
            #error TTC_I2C3 is defined, but not TTC_I2C1 - all lower TTC_I2Cn must be defined!
        #endif

        #define TTC_I2C_AMOUNT 3
    #endif
#endif //}
#ifndef TTC_I2C_AMOUNT //{ 2
    #ifdef TTC_I2C2

        #ifndef TTC_I2C1
            #error TTC_I2C2 is defined, but not TTC_I2C1 - all lower TTC_I2Cn must be defined!
        #endif

        #define TTC_I2C_AMOUNT 2
    #endif
#endif //}
#ifndef TTC_I2C_AMOUNT //{ 1
    #ifdef TTC_I2C1
        #define TTC_I2C_AMOUNT 1
    #else
        #define TTC_I2C_AMOUNT 0
    #endif
#endif //}

//}
/**{ Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_I2C 0        # disable default asserts for i2c driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_I2C_EXTRA 1  # enable extra asserts for i2c driver
 *
 */
#ifndef TTC_ASSERT_I2C    // any previous definition set (Makefile)?
    #define TTC_ASSERT_I2C 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_I2C == 1)  // use Assert()s in I2C code (somewhat slower but alot easier to debug)
    #define Assert_I2C(Condition, Origin) Assert( ((Condition) != 0), Origin)
    #define Assert_I2C_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_I2C_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in I2C code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_I2C(Condition, Origin)
    #define Assert_I2C_Writable(Address, Origin)
    #define Assert_I2C_Readable(Address, Origin)
#endif

#ifndef TTC_ASSERT_I2C_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_I2C_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_I2C_EXTRA == 1)  // use Assert()s in I2C code (somewhat slower but alot easier to debug)
    #define Assert_I2C_EXTRA(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_I2C_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_I2C_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in I2C code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_I2C_EXTRA(Condition, Origin)
    #define Assert_I2C_EXTRA_Writable(Address, Origin)
    #define Assert_I2C_EXTRA_Readable(Address, Origin)
#endif
/**{ Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_i2c_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_i2c_architecture
    #  warning Missing low-level definition for t_ttc_i2c_architecture (using default)
    #define t_ttc_i2c_architecture void
#endif
//}
/** { Check static configuration of i2c devices
 *
 * Each TTC_I2Cn must be defined as one from e_ttc_i2c_architecture.
 * Additional defines of type TTC_I2Cn_* may be added.
 *
 * The constants being checked here must be defined in your makefile.
 * Most constants are defined by EXTENSION_100_board_* or EXTENSION_150_board_extension_* files.
 */

#ifdef TTC_I2C1

    // check extra defines for i2c device #1 here
    #ifndef TTC_I2C1_SCL
        #    error Missing definition of TTC_I2C1_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif
    #ifndef TTC_I2C1_SDA
        #    error Missing definition of TTC_I2C1_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif

#endif
#ifdef TTC_I2C2

    // check extra defines for i2c device #2 here
    #ifndef TTC_I2C2_SCL
        #    error Missing definition of TTC_I2C2_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif
    #ifndef TTC_I2C2_SDA
        #    error Missing definition of TTC_I2C2_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif

#endif
#ifdef TTC_I2C3

    // check extra defines for i2c device #3 here
    #ifndef TTC_I2C3_SCL
        #    error Missing definition of TTC_I2C3_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif
    #ifndef TTC_I2C3_SDA
        #    error Missing definition of TTC_I2C3_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif

#endif
#ifdef TTC_I2C4

    // check extra defines for i2c device #4 here
    #ifndef TTC_I2C4_SCL
        #    error Missing definition of TTC_I2C4_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif
    #ifndef TTC_I2C4_SDA
        #    error Missing definition of TTC_I2C4_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif

#endif
#ifdef TTC_I2C5

    // check extra defines for i2c device #5 here
    #ifndef TTC_I2C5_SCL
        #    error Missing definition of TTC_I2C5_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif
    #ifndef TTC_I2C5_SDA
        #    error Missing definition of TTC_I2C5_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif

#endif
#ifdef TTC_I2C6

    // check extra defines for i2c device #6 here
    #ifndef TTC_I2C6_SCL
        #    error Missing definition of TTC_I2C6_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif
    #ifndef TTC_I2C6_SDA
        #    error Missing definition of TTC_I2C6_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif

#endif
#ifdef TTC_I2C7

    // check extra defines for i2c device #7 here
    #ifndef TTC_I2C7_SCL
        #    error Missing definition of TTC_I2C7_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif
    #ifndef TTC_I2C7_SDA
        #    error Missing definition of TTC_I2C7_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif

#endif
#ifdef TTC_I2C8

    // check extra defines for i2c device #8 here

#endif
#ifdef TT8_I2C9

    // check extra defines for i2c de8ice #1 here
    #ifndef TTC_I2C1_SCL
        #    error Missing8definition of TTC_I2C1_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif
    #ifndef TTC_I2C1_SDA
        #er8or Missing definition of TTC_I2C1_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif

#endif
#ifdef TTC_I2C10

    // check extra defines for i2c device #10 here
    #ifndef TTC_I2C10_SCL
        #    error Missing definition of TTC_I2C10_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif
    #ifndef TTC_I2C10_SDA
        #    error Missing definition of TTC_I2C10_SCL. Define it as gpio pin to be used (one from e_ttc_gpio_pin)!
    #endif

#endif
//}

/** Check definition of Event Codes and Flag Codes
 *
 * See e_ttc_i2c_event_code for individual descriptions.
 */
#ifndef TTC_I2C_EVENT_CODE_MASTER_RECEIVER_MODE_SELECTED            //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_MASTER_RECEIVER_MODE_SELECTED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_MASTER_RECEIVER_MODE_SELECTED 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_MASTER_TRANSMITTER_MODE_SELECTED         //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_MASTER_TRANSMITTER_MODE_SELECTED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_MASTER_TRANSMITTER_MODE_SELECTED 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_MASTER_MODE_SELECT                       //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_MASTER_MODE_SELECT. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_MASTER_MODE_SELECT 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_BUS_BUSY                                 //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_BUS_BUSY. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_BUS_BUSY 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_BUS_FREE                                 //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_BUS_FREE. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_BUS_FREE 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_MASTER_BYTE_TRANSMITTED                  //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_MASTER_BYTE_TRANSMITTED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_MASTER_BYTE_TRANSMITTED 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_MASTER_BYTE_RECEIVED                     //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_MASTER_BYTE_RECEIVED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_MASTER_BYTE_RECEIVED 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_MASTER_BYTE_TRANSMITTING                 //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_MASTER_BYTE_TRANSMITTING. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_MASTER_BYTE_TRANSMITTING 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_SLAVE_RECEIVER_ADDRESS_MATCHED           //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_SLAVE_RECEIVER_ADDRESS_MATCHED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_SLAVE_RECEIVER_ADDRESS_MATCHED 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_SLAVE_TRANSMITTER_ADDRESS_MATCHED        //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_SLAVE_TRANSMITTER_ADDRESS_MATCHED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_SLAVE_TRANSMITTER_ADDRESS_MATCHED 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_SLAVE_RECEIVER_SECONDADDRESS_MATCHED     //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_SLAVE_RECEIVER_SECONDADDRESS_MATCHED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_SLAVE_RECEIVER_SECONDADDRESS_MATCHED 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED  //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_SLAVE_GENERALCALLADDRESS_MATCHED         //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_SLAVE_GENERALCALLADDRESS_MATCHED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_SLAVE_GENERALCALLADDRESS_MATCHED 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_SLAVE_BYTE_RECEIVED                      //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_SLAVE_BYTE_RECEIVED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_SLAVE_BYTE_RECEIVED 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_SLAVE_STOP_DETECTED                      //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_SLAVE_STOP_DETECTED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_SLAVE_STOP_DETECTED 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_SLAVE_BYTE_TRANSMITTED                   //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_SLAVE_BYTE_TRANSMITTED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_SLAVE_BYTE_TRANSMITTED 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_SLAVE_BYTE_TRANSMITTING                  //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_SLAVE_BYTE_TRANSMITTING. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_SLAVE_BYTE_TRANSMITTING 0
#endif //}
#ifndef TTC_I2C_EVENT_CODE_SLAVE_ACK_FAILURE                        //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_SLAVE_ACK_FAILURE. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_SLAVE_ACK_FAILURE 0
#endif //}

#ifndef TTC_I2C_EVENT_CODE_SLAVE_BUFFER_OVERRUN                        //{
    #  warning Missing definition for TTC_I2C_EVENT_CODE_SLAVE_BUFFER_OVERRUN. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_EVENT_CODE_SLAVE_BUFFER_OVERRUN 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_SMBUS_ALERT                                  //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_SMBUS_ALERT. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_SMBUS_ALERT 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_SMBUS_TIMEOUT                                   //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_SMBUS_TIMEOUT. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_SMBUS_TIMEOUT 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_CRC_ERROR                                    //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_CRC_ERROR. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_CRC_ERROR 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_BUFFER_OVERRUN                                       //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_BUFFER_OVERRUN. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_BUFFER_OVERRUN 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_ACKNOWLEDGE_FAILURE                                        //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_ACKNOWLEDGE_FAILURE. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_ACKNOWLEDGE_FAILURE 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_ARBITRATION_LOST                                      //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_ARBITRATION_LOST. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_ARBITRATION_LOST 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_BUS_ERROR                                      //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_BUS_ERROR. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_BUS_ERROR 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_TRANSMIT_BUFFER_EMPTY                                       //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_TRANSMIT_BUFFER_EMPTY. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_TRANSMIT_BUFFER_EMPTY 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_RECEIVE_BUFFER_NOT_EMPTY                                      //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_RECEIVE_BUFFER_NOT_EMPTY. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_RECEIVE_BUFFER_NOT_EMPTY 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_STOP_DETECTED                                     //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_STOP_DETECTED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_STOP_DETECTED 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_10_BIT_ADDRESS                                     //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_10_BIT_ADDRESS. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_10_BIT_ADDRESS 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_BYTE_TRANSFER_FINISHED                                       //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_BYTE_TRANSFER_FINISHED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_BYTE_TRANSFER_FINISHED 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_ADDRESS_SENT                                      //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_ADDRESS_SENT. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_ADDRESS_SENT 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_START_BIT_DETECTED                                        //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_START_BIT_DETECTED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_START_BIT_DETECTED 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_OWNADDRESS1_MATCHED                                     //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_OWNADDRESS1_MATCHED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_OWNADDRESS1_MATCHED 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_SMBUS_HOST_HEADER_RECEIVED                                   //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_SMBUS_HOST_HEADER_RECEIVED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_SMBUS_HOST_HEADER_RECEIVED 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_SMBUS_DEFAULT_ADDRESS_RECEIVED                                //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_SMBUS_DEFAULT_ADDRESS_RECEIVED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_SMBUS_DEFAULT_ADDRESS_RECEIVED 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_SMBUS_GENERAL_CALL_ADDRESS_RECEIVED                                   //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_SMBUS_GENERAL_CALL_ADDRESS_RECEIVED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_SMBUS_GENERAL_CALL_ADDRESS_RECEIVED 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_READ_MODE_DETECTED                                       //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_READ_MODE_DETECTED. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_READ_MODE_DETECTED 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_BUS_BUSY                                      //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_BUS_BUSY. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_BUS_BUSY 0
#endif //}
#ifndef TTC_I2C_FLAG_CODE_MASTER_MODE                                       //{
    #  warning Missing definition for TTC_I2C_FLAG_CODE_MASTER_MODE. Add a corresponding definition to your low-level driver!
    #define TTC_I2C_FLAG_CODE_MASTER_MODE 0
#endif //}

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_i2c_errorcode       return codes of I2C devices
    E_ttc_i2c_errorcode_OK = 0,

    // other warnings go here..

    E_ttc_i2c_errorcode_ERROR,                  // general failure
    E_ttc_i2c_errorcode_NULL,                   // NULL pointer not accepted
    E_ttc_i2c_errorcode_DeviceNotFound,         // corresponding device could not be found
    E_ttc_i2c_errorcode_InvalidConfiguration,   // sanity check of device configuration failed
    E_ttc_i2c_errorcode_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..
    E_ttc_i2c_errorcode_BusError,                                 // hardware has detected a Bus-Error
    E_ttc_i2c_errorcode_ParityCheckError,                         // hardware has received an invalid parity
    E_ttc_i2c_errorcode_TimeOut_StartCondition,                   // timeout occured during setting start condition
    E_ttc_i2c_errorcode_TimeOut_StopCondition,                    // timeout occured during setting stop condition
    E_ttc_i2c_errorcode_TimeOut_Master_TransmitterModeSelected,   // timeout while waiting for slave to acknowledge transmitter mode after sending slave address
    E_ttc_i2c_errorcode_TimeOut_Master_ReceiverModeSelected,      // timeout while waiting for slave to acknowledge receiver mode after sending slave address
    E_ttc_i2c_errorcode_TimeOut_WaitingForBusFree,                // timeout while waiting for SDA + SCL lines to become high
    E_ttc_i2c_errorcode_TimeOut_WaitingForTransmitBuffer,         // timeout while waiting for transmit buffer to free
    E_ttc_i2c_errorcode_TimeOut_WaitingForByteTransmitted,        // timeout while waiting for byte to be transmitted
    E_ttc_i2c_errorcode_TimeOut_WaitingForByteReceivedFromSlave,  // timeout while waiting for byte to be received from slave in master mode
    E_ttc_i2c_errorcode_TimeOut_WaitingForByteReceivedFromMaster, // timeout while waiting for byte to be received from master in slave mode

    E_ttc_i2c_errorcode_unknown                 // no valid errorcodes past this entry
} e_ttc_i2c_errorcode;

typedef enum {   // e_ttc_i2c_architecture    types of architectures supported by I2C driver
    ta_i2c_None = 0,       // no architecture selected

    ta_i2c_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
    ta_i2c_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
    E_ttc_i2c_architecture_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
    E_ttc_i2c_architecture_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_i2c_unknown        // architecture not supported
} e_ttc_i2c_architecture;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef union {  // architecture dependent configuration

#ifdef EXTENSION_i2c_stm32l1xx
    t_i2c_stm32l1xx_config stm32l1xx;  // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_i2c_stm32f1xx
    t_i2c_stm32f1xx_config stm32f1xx;  // automatically added by ./create_DeviceDriver.pl
#endif
    //InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_i2c_architecture;

typedef enum { // e_ttc_i2c_event_code
    E_ttc_i2c_event_code_None = 0,

    // current values defined by low-level driver for current  architecture
    E_ttc_i2c_event_code_master_receiver_mode_selected            = TTC_I2C_EVENT_CODE_MASTER_RECEIVER_MODE_SELECTED,             // slave has acknowledged a master receiver mode (EV6)
    E_ttc_i2c_event_code_master_transmitter_mode_selected         = TTC_I2C_EVENT_CODE_MASTER_TRANSMITTER_MODE_SELECTED,          // slave has acknowledged a master transmitter mode (EV6)
    E_ttc_i2c_event_code_master_mode_select                       = TTC_I2C_EVENT_CODE_MASTER_MODE_SELECT,                        // slave has acknowledged start condition (EV5)
    E_ttc_i2c_event_code_bus_busy                                 = TTC_I2C_EVENT_CODE_BUS_BUSY,                                  // communication ongoing on bus (SDA ==0 || SCK==0)
    E_ttc_i2c_event_code_bus_free                                 = TTC_I2C_EVENT_CODE_BUS_FREE,                                  // bus is free                  (SDA ==1 && SCK==1)
    E_ttc_i2c_event_code_master_byte_transmitted                  = TTC_I2C_EVENT_CODE_MASTER_BYTE_TRANSMITTED,                   // master has transmitted a byte (EV8_2)
    E_ttc_i2c_event_code_master_byte_received                     = TTC_I2C_EVENT_CODE_MASTER_BYTE_RECEIVED,                      // master has received one byte
    E_ttc_i2c_event_code_master_byte_transmitting                 = TTC_I2C_EVENT_CODE_MASTER_BYTE_TRANSMITTING,                  // master has started to transmit previous byte
    E_ttc_i2c_event_code_slave_receiver_address_matched           = TTC_I2C_EVENT_CODE_SLAVE_RECEIVER_ADDRESS_MATCHED,            // address matching to OwnAddress1 has been received (ReadBit==1)
    E_ttc_i2c_event_code_slave_transmitter_address_matched        = TTC_I2C_EVENT_CODE_SLAVE_TRANSMITTER_ADDRESS_MATCHED,         // address matching to OwnAddress1 has been received (ReadBit==0)
    E_ttc_i2c_event_code_slave_receiver_secondaddress_matched     = TTC_I2C_EVENT_CODE_SLAVE_RECEIVER_SECONDADDRESS_MATCHED,      // address matching to OwnAddress2 has been received (ReadBit==1)
    E_ttc_i2c_event_code_slave_transmitter_secondaddress_matched  = TTC_I2C_EVENT_CODE_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED,   // address matching to OwnAddress2 has been received (ReadBit==0)
    E_ttc_i2c_event_code_slave_generalcalladdress_matched         = TTC_I2C_EVENT_CODE_SLAVE_GENERALCALLADDRESS_MATCHED,          // received a matching general call address
    E_ttc_i2c_event_code_slave_byte_received                      = TTC_I2C_EVENT_CODE_SLAVE_BYTE_RECEIVED,                       // a single byte has been received in slave mode
    E_ttc_i2c_event_code_slave_stop_detected                      = TTC_I2C_EVENT_CODE_SLAVE_STOP_DETECTED,                       // stop condition has been set by master
    E_ttc_i2c_event_code_slave_byte_transmitted                   = TTC_I2C_EVENT_CODE_SLAVE_BYTE_TRANSMITTED,                    // slave has transmitted single byte
    E_ttc_i2c_event_code_slave_byte_transmitting                  = TTC_I2C_EVENT_CODE_SLAVE_BYTE_TRANSMITTING,                   // slave has started to transmit single byte
    E_ttc_i2c_event_code_slave_ack_failure                        = TTC_I2C_EVENT_CODE_SLAVE_ACK_FAILURE,                         // slave has detected an acknoweldgement failure
    E_ttc_i2c_event_code_slave_buffer_overrun                     = TTC_I2C_EVENT_CODE_SLAVE_BUFFER_OVERRUN,                      // slave received more data than it could process

} e_ttc_i2c_event_code;

typedef enum { // e_ttc_i2c_flag_code
    E_ttc_i2c_flag_code_None = 0,

    // current values defined by low-level driver for current  architecture
    E_ttc_i2c_flag_code_crc_error                             = TTC_I2C_FLAG_CODE_CRC_ERROR,                              // CRC Error for received byte
    E_ttc_i2c_flag_code_buffer_overrun                        = TTC_I2C_FLAG_CODE_BUFFER_OVERRUN,                         // Buffer Overrun (master sent data too fast)
    E_ttc_i2c_flag_code_acknowledge_failure                   = TTC_I2C_FLAG_CODE_ACKNOWLEDGE_FAILURE,                    // Acknowledge Failure
    E_ttc_i2c_flag_code_arbitration_lost                      = TTC_I2C_FLAG_CODE_ARBITRATION_LOST,                       // Arbitration Lost detected                           (Master mode)
    E_ttc_i2c_flag_code_bus_error                             = TTC_I2C_FLAG_CODE_BUS_ERROR,                              // Bus Error detected                                  (Master mode)
    E_ttc_i2c_flag_code_transmit_buffer_empty                 = TTC_I2C_FLAG_CODE_TRANSMIT_BUFFER_EMPTY,                  // Transmit Data Register Empty     (not set during address phase)
    E_ttc_i2c_flag_code_receive_buffer_not_empty              = TTC_I2C_FLAG_CODE_RECEIVE_BUFFER_NOT_EMPTY,               // Receive Data Register not Empty  (not set during address phase)
    E_ttc_i2c_flag_code_stop_detected                         = TTC_I2C_FLAG_CODE_STOP_DETECTED,                          // Stop Condition detected                             (Slave mode)
    E_ttc_i2c_flag_code_10_bit_address                        = TTC_I2C_FLAG_CODE_10_BIT_ADDRESS,                         // 10 bit header sent                                  (Master mode)
    E_ttc_i2c_flag_code_byte_transfer_finished                = TTC_I2C_FLAG_CODE_BYTE_TRANSFER_FINISHED,                 // Byte Transfer Finished
    E_ttc_i2c_flag_code_address_sent                          = TTC_I2C_FLAG_CODE_ADDRESS_SENT,                           // Address Sent                                        (Master mode)
    E_ttc_i2c_flag_code_start_bit_detected                    = TTC_I2C_FLAG_CODE_START_BIT_DETECTED,                     // Start Bit                                           (Master mode)
    E_ttc_i2c_flag_code_ownaddress1_matched                   = TTC_I2C_FLAG_CODE_OWNADDRESS1_MATCHED,                    // ==1: received address matches OAR1; OAR2 otherwise  (Slave mode)
    E_ttc_i2c_flag_code_smbus_timeout                         = TTC_I2C_FLAG_CODE_SMBUS_TIMEOUT,                          // SMBus Timeout occured
    E_ttc_i2c_flag_code_smbus_alert                           = TTC_I2C_FLAG_CODE_SMBUS_ALERT,                            // SMBus Alert occured (SMBALERT pin triggered or response address header received)
    E_ttc_i2c_flag_code_smbus_host_header_received            = TTC_I2C_FLAG_CODE_SMBUS_HOST_HEADER_RECEIVED,             // Matching SMBus Host Header received                 (Slave mode)
    E_ttc_i2c_flag_code_smbus_default_address_received        = TTC_I2C_FLAG_CODE_SMBUS_DEFAULT_ADDRESS_RECEIVED,         // Matching SMBus Default Address received             (Slave mode)
    E_ttc_i2c_flag_code_smbus_general_call_address_received   = TTC_I2C_FLAG_CODE_SMBUS_GENERAL_CALL_ADDRESS_RECEIVED,    // Matching General Call Address received              (Slave mode)
    E_ttc_i2c_flag_code_read_mode_detected                    = TTC_I2C_FLAG_CODE_READ_MODE_DETECTED,                     // read/write bit of last address byte
    E_ttc_i2c_flag_code_bus_busy                              = TTC_I2C_FLAG_CODE_BUS_BUSY,                               //  I2C bus is busy (SDA or SCL is low)
    E_ttc_i2c_flag_code_master_mode                           = TTC_I2C_FLAG_CODE_MASTER_MODE                             // ==1: Device operates in master mode; slave mode otherwise
} e_ttc_i2c_flag_code;

typedef struct s_ttc_i2c_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_i2c_init()
    //       and after ttc_i2c_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.
    //? void*(*ISR_Events)(t_ttc_i2c_event);  // function pointer to interrupt service routine for events
    //? void*(*ISR_Errors)(t_ttc_i2c_error);  // function pointer to interrupt service routine for errors

    // low-level configuration (structure defined by low-level driver)
    u_ttc_i2c_architecture* LowLevelConfig;

    // base address of I2C registers on current architecture
    volatile t_ttc_i2c_base_register* BaseRegister;

    struct { // Init: fields to be set by application before first calling ttc_<device>_init() --------------
        // Do not change these values after calling ttc_<device>_init()!

        union {
            t_u32 All;
            struct {
                // Enabling a flag does not mean that the corresponding feature is available on current architecture.
                // Check these flags after initialization to find out which are supported by low-level driver!

                unsigned Master                : 1;   // =1: enable master mode, =0: slave mode
                unsigned Slave                 : 1;   // =1: enable slave mode,  =0: no slave functionality
                unsigned MultiMaster           : 1;   // =1: enable multiple masters on bus; =0: single master
                unsigned ModeFast              : 1;   // =1: enable fast mode; =0: standard mode
                unsigned ModeFastDuty169       : 1;   // =1: enable 16/9 duty cycle in fast mode; =0: use 2/1 duty cycle
                unsigned DMA_TX                : 1;   // =1: enable direct memory access for transmitting data; =0: no DMA
                unsigned DMA_RX                : 1;   // =1: enable direct memory access for receiving data; =0: no DMA
                unsigned SMBus                 : 1;   // =1: enable SMBus mode; =0: normal I2C mode
                unsigned SMBus_Alert           : 1;   // =1: use extra pin SMB_Alert; =0: only use SCL+SDA
                unsigned SMBus_Host            : 1;   // =1: enable Host configuration for SMBus mode; =0: Device configuration
                unsigned SMBus_ARP             : 1;   // =1: enable Address Resolution Protocol for SMBus mode; =0: no ARP
                unsigned Address_10Bit         : 1;   // =1: enable 10 bit wide adresses; =0: adresses are 7 bit wide
                unsigned PacketErrorCheck      : 1;   // =1: enable packet error checking; =0: no error checking
                unsigned Slave_Acknowledgement : 1;   // =1: enable acknowledgement of start bits in slave mode (required for most slaves); =0: no acknowledgements
                unsigned Slave_NoStretch       : 1;   // =1: enable clock stretching in slave mode; =0: no clock stretching
                unsigned Slave_GeneralCalls    : 1;   // =1: enable ACK of general address 0x00 in slave mode; =0: general address is not acknowledged
                unsigned Slave_DualAddress     : 1;   // =1: enable using OwnAddress1 and OwnAddress2 in slave mode; =0: slave listens to OwnAddress1 only
                unsigned Interrupt_Events      : 1;   // =1: enable interrupts for events: =0: no interrupt generation for events
                unsigned Interrupt_Errors      : 1;   // =1: enable interrupts for errors: =0: no interrupt generation for errors
                unsigned Initialized           : 1;   // =1: if I2C is initialized
                unsigned Reserved1             : 12;  // pad to 32 bits

            } Bits;
        } Flags;

        e_ttc_gpio_pin Port_SCL;             // port pin for SCL
        e_ttc_gpio_pin Port_SDA;             // port pin for SDA
        e_ttc_gpio_pin Port_SMBALERT;        // port pin for ALERT pin in SMBus mode (if enabled)
        t_u32 ClockSpeed;                    // clock frequency to use (limited by current architecture driver)
        t_u32 TimeOut;                       // >0: maximum allowed time for individual transactions (usecs), == 0: no timeout
        t_u16 AmountRetries;                 // >0: amount of automatic retries in case of bus errors (each retry will increase ErrorCount); ==-1: endless retries (required for single step debugging) (DO NOT CHANGE DATATYPE!)
    } Init;

    t_ttc_mutex_smart Mutex_I2C;         // protects I2C interface against simultaneous access from multiple tasks
    t_u32 MaxRiseTime_ns;                // maximum rise time (nanoseconds) in master mode according to I2C specification for Fast/Standard Mode
    t_u32 ErrorCount;                    // increased on every error condition
    t_u16 OwnAddress1;                   // adress #1 of this device in slave mode
    t_u16 OwnAddress2;                   // adress #2 of this device in slave mode (only used if Flags.Bits.Slave_DualAddress == 1)
    t_u8  Layout;                        // select pin layout to use (some uC allow pin remapping)
    t_u8  LogicalIndex;                  // automatically set: logical index of device to use (1 = TTC_I2C1, ...)
    t_u8  PhysicalIndex;                 // automatically set: physical index of device to use (0 = first hardware device, ...)
    e_ttc_i2c_architecture Architecture; // type of architecture used for current i2c device

    // Last returned error code
    // IMPORTANT: Every ttc_<device>_*() function that returns ttc_<device>_errorcode_e has to update this value if it returns an error!
    e_ttc_i2c_errorcode    LastError;

} __attribute__( ( __packed__ ) ) t_ttc_i2c_config;

typedef enum {     // e_ttc_i2c_address_type  address format to use to transmit a register address
    /** Some I2C slaves may require a special format to transmit a register address
     *
     * In fact, most slaves use the MSB-First 8Bit format.
     */

    E_ttc_i2c_address_type_Default        = 0,             // most slaves will use this format
    E_ttc_i2c_address_type_MSB_First_8Bit = E_ttc_i2c_address_type_Default,  // 1 byte  used to transmit a register adress (Most significant byte transferred first)
    E_ttc_i2c_address_type_MSB_First_16Bit,                // 2 bytes used to transmit a register adress (Most significant byte transferred first)
    E_ttc_i2c_address_type_MSB_First_24Bit,                // 3 bytes used to transmit a register adress (Most significant byte transferred first)
    E_ttc_i2c_address_type_MSB_First_32Bit,                // 4 bytes used to transmit a register adress (Most significant byte transferred first)
    E_ttc_i2c_address_type_LSB_First_8Bit,                 // 1 byte  used to transmit a register adress (Least significant byte transferred first)
    E_ttc_i2c_address_type_LSB_First_16Bit,                // 2 bytes used to transmit a register adress (Least significant byte transferred first)
    E_ttc_i2c_address_type_LSB_First_24Bit,                // 3 bytes used to transmit a register adress (Least significant byte transferred first)
    E_ttc_i2c_address_type_LSB_First_32Bit                 // 4 bytes used to transmit a register adress (Least significant byte transferred first)
} e_ttc_i2c_address_type;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_I2C_TYPES_H
