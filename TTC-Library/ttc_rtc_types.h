/** { ttc_rtc_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for RTC device.
 *
 *  Structures, Enums and Defines being required by both, high- and low-level rtc.
 *
 * Authors: Gregor Rebel
 *
}*/

#ifndef TTC_RTC_TYPES_H
#define TTC_RTC_TYPES_H

//{ Includes *************************************************************

#include "ttc_basic_types.h"
#include "compile_options.h"

#ifdef EXTENSION_rtc_stm32l1xx
    #include "rtc/rtc_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_rtc_stm32f1xx
    #include "rtc/rtc_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif

//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************
#ifndef TTC_RTC_MAX_AMOUNT
    #warning Missing Low-Level definition of TTC_RTC_MAX_AMOUNT!
    #define TTC_RTC_MAX_AMOUNT 0
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

//} Includes
//{ Defines ***************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration **************************************

// TTC_RTCn has to be defined as constant by makefile.100_board_*


/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_RTC 0        # disable default asserts for RTC driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_RTC_EXTRA 1  # enable extra asserts for RTC driver
 *
 */
#ifndef TTC_ASSERT_RTC    // any previous definition set (Makefile)?
    #define TTC_ASSERT_RTC 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_RTC == 1)  // use Assert()s in RTC code (somewhat slower but alot easier to debug)
    #define Assert_RTC(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_RTC_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_RTC_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in RTC code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_RTC(Condition, Origin)
    #define Assert_RTC_Writable(Address, Origin)
    #define Assert_RTC_Readable(Address, Origin)
#endif


#ifndef TTC_ASSERT_RTC_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_RTC_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_RTC_EXTRA == 1)  // use Assert()s in RTC code (somewhat slower but alot easier to debug)
    #define Assert_RTC_EXTRA(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_RTC_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_RTC_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in RTC code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_RTC_EXTRA(Condition, Origin)
    #define Assert_RTC_EXTRA_Writable(Address, Origin)
    #define Assert_RTC_EXTRA_Readable(Address, Origin)
#endif
//}

#define TTC_RTC1 0

#ifdef TTC_RTC5
    #ifndef TTC_RTC4
        #error TTC_RTC5 is defined, but not TTC_RTC4 - all lower TTC_RTCn must be defined!
    #endif
    #ifndef TTC_RTC3
        #error TTC_RTC5 is defined, but not TTC_RTC3 - all lower TTC_RTCn must be defined!
    #endif
    #ifndef TTC_RTC2
        #error TTC_RTC5 is defined, but not TTC_RTC2 - all lower TTC_RTCn must be defined!
    #endif
    #ifndef TTC_RTC1
        #error TTC_RTC5 is defined, but not TTC_RTC1 - all lower TTC_RTCn must be defined!
    #endif

    #define TTC_RTC_AMOUNT 5
#else
    #ifdef TTC_RTC4
        #define TTC_RTC_AMOUNT 4

        #ifndef TTC_RTC3
            #error TTC_RTC5 is defined, but not TTC_RTC3 - all lower TTC_RTCn must be defined!
        #endif
        #ifndef TTC_RTC2
            #error TTC_RTC5 is defined, but not TTC_RTC2 - all lower TTC_RTCn must be defined!
        #endif
        #ifndef TTC_RTC1
            #error TTC_RTC5 is defined, but not TTC_RTC1 - all lower TTC_RTCn must be defined!
        #endif
    #else
        #ifdef TTC_RTC3

            #ifndef TTC_RTC2
                #error TTC_RTC5 is defined, but not TTC_RTC2 - all lower TTC_RTCn must be defined!
            #endif
            #ifndef TTC_RTC1
                #error TTC_RTC5 is defined, but not TTC_RTC1 - all lower TTC_RTCn must be defined!
            #endif

            #define TTC_RTC_AMOUNT 3
        #else
            #ifdef TTC_RTC2

                #ifndef TTC_RTC1
                    #error TTC_RTC5 is defined, but not TTC_RTC1 - all lower TTC_RTCn must be defined!
                #endif

                #define TTC_RTC_AMOUNT 2
            #else
                #ifdef TTC_RTC1
                    #define TTC_RTC_AMOUNT 1
                #else
                    #define TTC_RTC_AMOUNT 0
                #endif
            #endif
        #endif
    #endif
#endif


//}Static Configuration
//{ Enums/ Structures *****************************************

typedef enum {    // e_ttc_rtc_errorcode     return codes of RTC devices
    ec_rtc_OK = 0,
    ec_rtc_ERROR,           // general failure
    ec_rtc_DeviceNotFound,
    ec_rtc_InvalidImplementation

    // other failures go here..
} e_ttc_rtc_errorcode;

typedef struct s_ttc_rtc_time {
    t_u16 mil_sec;
    t_u8 sec;
    t_u8 dec_sec;
    t_u8 min;
    t_u8 dec_min;
    t_u8 hour;
    t_u8 dec_hour;
} t_ttc_rtc_time;

typedef struct s_ttc_rtc_config { //         stm32l1 independent configuration data

    // Note: Write-access to this structure is only allowed before first ttc_rtc_init() call!
    //       After initialization, only read accesses are allowed for high-level code.

    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_rtc1, ...)
    t_u8  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)

#ifdef t_ttc_rtc_architecture
    t_ttc_rtc_architecture* LowLevelConfig;
#endif
    t_u32 SynchPrediv;
    t_u32 AsynchPrediv;
    t_u32 HourFormat;

    // handlers of registered interrupts
    void* Interrupt_WakeUP;
    void* Interrupt_TimerStamp;
    void* Interrupt_Tamper;
    void* Interrupt_AlarmA;
    void* Interrupt_AlarmB;
    void* Interrupt_Pin_line_wakeup;

    union  { // generic configuration bits common for all low-level Drivers
        t_u16 All;
        struct {
            unsigned Initialized        : 1;  // ==1: device has been initialized successfully
            unsigned HourFormat_12      : 1;  // ==1: device use AM/PM hour format :else device use 24 hour/day format
            unsigned Timestamp_ISR      : 1;
            unsigned Wakeup_ISR         : 1;
            unsigned Alarm_B_ISR        : 1;
            unsigned Alarm_A_ISR        : 1;
            unsigned Timestap           : 1;
            unsigned Alarm_A            : 1;
            unsigned Alarm_B            : 1;
            unsigned WakeUp             : 1;
            unsigned Tamper             : 1;
            unsigned Pin_line           : 1;
            unsigned Reserved5          : 4;
        } Bits;
    } Flags;

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_rtc_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_RTC_TYPES_H
