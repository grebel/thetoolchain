/**
 * \file
 *         Initialiation file for the IEEE 802.15.4 mac stack (NETSTACK)
 * \author
 *         Francisco Estevez <fjestevez@fh-muenster.de>
 */

#include "mac_ieee802_15_4_macstack.h"
/*---------------------------------------------------------------------------*/
void
macstack_init( void ) {
    NETSTACK_RADIO.init(); //NULLRDC calls RADIO.on()
    NETSTACK_RDC.init();
    NETSTACK_MAC.init();
}
/*---------------------------------------------------------------------------*/
void
macstack_deinit( void ) {
    //NETSTACK_RADIO.off(); //NULLRDC calls RADIO.on()
    //NETSTACK_RDC.off(0);
    NETSTACK_MAC.off( 0 );
}
