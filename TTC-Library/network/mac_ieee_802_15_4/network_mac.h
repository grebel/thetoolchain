#ifndef NETWORK_MAC_H
#define NETWORK_MAC_H

/** { network_mac.h **********************************************

                           The ToolChain

   High-Level interface for NETWORK device.

   Structures, Enums and Defines being required by high-level network and application.

   Note: See ttc_network.h for description of 6lowpan independent NETWORK implementation.

   Authors:


}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

#include "network_mac_types.h"
#include "../../ttc_network.h"
#include "../../ttc_task.h"
#include "../../ttc_memory.h"
#include "../../ttc_watchdog.h"
//#include "network_mac_netstack.h"
#include "mac_ieee802_15_4_macstack.h"

#include "../support/network_rime_packetbuf.h"
#include "../support/network_rimeaddr.h"

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_network_interface.h
#define Driver_network_prepare()             network_mac_prepare()
#define Driver_network_reset(Config)         network_mac_reset(Config)
#define Driver_network_load_defaults(Config) network_mac_load_defaults(Config)
#define Driver_network_deinit(Config)        network_mac_deinit(Config)
#define Driver_network_init(Config)          network_mac_init(Config)
#define Driver_network_send_unicast(Config, Target, Buffer, Amount) network_mac_send_unicast(Config, Target, Buffer, Amount)
#define Driver_network_send_broadcast(Config, Buffer, Amount) network_mac_send_broadcast(Config, Buffer, Amount)
#define Driver_network_receive(Config)       network_mac_receive(Config)
#define Driver_network_get_neighbours(Config) network_mac_get_neighbours(Config)

//} Includes
//{ Function prototypes **************************************************

/** Prepares mac Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void network_mac_prepare();

/** checks if MAC driver already has been initialized
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return              == TRUE: indexed NETWORK unit is already initialized
 */
bool network_mac_check_initialized( t_ttc_network_config* Config );

/** returns reference to low-level configuration struct of MAC driver
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return              pointer to struct t_ttc_network_config (will assert if no configuration available)
 */
//t_network_mac_config* network_mac_get_configuration(t_ttc_network_config* Config);

/** loads configuration of MAC driver with default values
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_network_errorcode network_mac_load_defaults( t_ttc_network_config* Config );

/** fills out given Config with maximum valid values for indexed NETWORK
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_network_errorcode network_mac_get_features( t_ttc_network_config* Config );

/** initializes single MAC
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return              == 0: NETWORK has been initialized successfully; != 0: error-code
 */
e_ttc_network_errorcode network_mac_init( t_ttc_network_config* Config );

/** shutdown single MAC device
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return              == 0: NETWORK has been shutdown successfully; != 0: error-code
 */
e_ttc_network_errorcode network_mac_deinit( t_ttc_network_config* Config );

/** reset configuration of mac driver and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_NETWORK_get_max_LogicalIndex())
 */
e_ttc_network_errorcode network_mac_reset( t_ttc_network_config* Config );

/** sends data from given buffer via network to given target address
 *
 * @param LogicalIndex  1..TTC_NETWORK_MAX
 * @param Target        protocol address of target node
 * @param Buffer        memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[] (even zero bytes are sent)
 */
e_ttc_network_errorcode network_mac_send_unicast( t_ttc_network_config* Config, ttc_network_address_t Target, t_u8* Buffer, t_base Amount );

/** sends data from given buffer via network to all other nodes
 *
 * @param LogicalIndex  1..TTC_NETWORK_MAX
 * @param Buffer        memory location from which to read data
 * @param Amount        amount of bytes to send from Buffer[] (even zero bytes are sent)
 */
e_ttc_network_errorcode network_mac_send_broadcast( t_ttc_network_config* Config, t_u8* Buffer, t_base Amount );

/** registers function to be called for every packet being received from network
 *
 * @param LogicalIndex  1..TTC_NETWORK_MAX
 * @param receivePacket function will be called whenever a valid packet has beed received
 */
void network_mac_receive( t_ttc_network_config* Config );

/** returns list of all known neighbour nodes
 *
 * @param LogicalIndex  1..TTC_NETWORK_MAX
 * @return  list of protocol adresses of neighbour nodes
 */
t_ttc_list* network_mac_get_neighbours( t_ttc_network_config* Config );

//} Function prototypes
//{ private functions (ideally) *********************************************
/**
 * Callback function for the MAC packet sent callback
 */
void _network_packet_sent_( void* ptr, int status, int transmissions );

//} private functions
#endif //NETWORK_MAC_H
