/** { network_mac.c ************************************************

                           The ToolChain

   High-Level interface for NETWORK device.

   Note: See ttc_network.h for description of mac independent NETWORK implementation.

   Authors:
}*/

#include "network_mac.h"

//{ Global varables *********************************************************

#ifndef SICSLOWPAN_MAX_MAC_TRANSMISSIONS
#  define MAX_MAC_TRANSMISSIONS 3
#else
#  define MAX_MAC_TRANSMISSIONS SICSLOWPAN_MAX_MAC_TRANSMISSIONS
#endif

t_ttc_list TxPipe;
t_ttc_list RxPipe;

#ifndef MAC_MAX_PAYLOAD
/** \brief Size of the 802.15.4 payload (127byte - 25 for MAC header) */
#  define MAC_MAX_PAYLOAD 102

/**
 * the result of the last transmitted fragment
 */
static int last_tx_status;

static t_u8* rime_ptr;
#endif

//} Global varables
//{ Function definitions ***************************************************

void network_mac_prepare() {
    #if RIMEADDR_SIZE == 1
    const t_rimeaddr addr_ff = { { 0xff } };
    #else /*RIMEADDR_SIZE == 2*/
    #if RIMEADDR_SIZE == 2
    const t_rimeaddr addr_ff = { { 0xff, 0xff } };
    #else /*RIMEADDR_SIZE == 2*/
    #if RIMEADDR_SIZE == 8
    const t_rimeaddr addr_ff = { { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff } };
    #endif /*RIMEADDR_SIZE == 8*/
    #endif /*RIMEADDR_SIZE == 2*/
    #endif /*RIMEADDR_SIZE == 1*/

    // add your startup code here (Singletasking!)
    ( void ) addr_ff; //Avoid unused variable
}
bool network_mac_check_initialized( t_ttc_network_config* Config ) {

    if ( Config->LowLevelConfig )
    { return TRUE; }

    return FALSE;
}
t_network_mac_config* network_mac_get_configuration( t_ttc_network_config* Config ) {
    Assert_NETWORK( Config != NULL, ttc_assert_origin_auto );
    Assert_NETWORK( Config->LogicalIndex > 0, ttc_assert_origin_auto );

    t_network_mac_config* Config_mac = Config->LowLevelConfig;
    if ( Config_mac == NULL ) { // first call: allocate + init configuration
        Config_mac = ttc_heap_alloc_zeroed( sizeof( t_ttc_network_arch ) );

        switch ( Config->PhysicalIndex ) {         // determine base register and other low-level configuration data
            case 0: { // load low-level configuration of first network device
                Config_mac->BaseRegister = NULL; // load adress of register base of NETWORK device
                break;
            }
            case 1: { // load low-level configuration of second network device
                Config_mac->BaseRegister = NULL; // load adress of register base of NETWORK device
                break;
            }
            case 2: { // load low-level configuration of third network device
                Config_mac->BaseRegister = NULL; // load adress of register base of NETWORK device
                break;
            }
            case 3: { // load low-level configuration of fourth network device
                Config_mac->BaseRegister = NULL; // load adress of register base of NETWORK device
                break;
            }
            case 4: { // load low-level configuration of fifth network device
                Config_mac->BaseRegister = NULL; // load adress of register base of NETWORK device
                break;
            }
            case 5: { // load low-level configuration of sixth network device
                Config_mac->BaseRegister = NULL; // load adress of register base of NETWORK device
                break;
            }
            case 6: { // load low-level configuration of seveth network device
                Config_mac->BaseRegister = NULL; // load adress of register base of NETWORK device
                break;
            }
            case 7: { // load low-level configuration of eighth network device
                Config_mac->BaseRegister = NULL; // load adress of register base of NETWORK device
                break;
            }
            default: Assert_NETWORK( 0, ttc_assert_origin_auto ); break; // No TTC_NETWORKn defined! Check your makefile.100_board_* file!
        }
    }

    Assert_NETWORK( Config_mac, ttc_assert_origin_auto );
    return Config_mac;
}
e_ttc_network_errorcode network_mac_get_features( t_ttc_network_config* Config ) {
    Assert_NETWORK( Config,    ttc_assert_origin_auto );
    Assert_NETWORK( Config->LogicalIndex > 0, ttc_assert_origin_auto );
    if ( Config->LogicalIndex > TTC_NETWORK_AMOUNT ) { return ec_network_DeviceNotFound; }

    Config->Flags.All                        = 0;
    //Config->Flags.Bits.Master                = 1;

    switch ( Config->LogicalIndex ) {         // determine features of indexed NETWORK device
            #ifdef TTC_NETWORK1
        case 1: break;
            #endif
            #ifdef TTC_NETWORK2
        case 2: break;
            #endif
            #ifdef TTC_NETWORK3
        case 3: break;
            #endif
            #ifdef TTC_NETWORK4
        case 4: break;
            #endif
            #ifdef TTC_NETWORK5
        case 5: break;
            #endif
        default: Assert_NETWORK( 0, ttc_assert_origin_auto ); break; // No TTC_NETWORKn defined! Check your makefile.100_board_* file!
    }

    return ec_network_OK;
}
e_ttc_network_errorcode network_mac_init( t_ttc_network_config* Config ) {
    Assert_NETWORK( Config, ttc_assert_origin_auto );
    Assert_NETWORK( Config->LogicalIndex > 0, ttc_assert_origin_auto );
    t_network_mac_config* Config_mac = network_mac_get_configuration( Config );
    ( void ) Config_mac; // avoid warning: "unused variable"


    if ( 1 ) { // validate NETWORK_Features
        t_ttc_network_config NETWORK_Features;
        NETWORK_Features.LogicalIndex = Config->LogicalIndex;
        e_ttc_network_errorcode Error = network_mac_get_features( &NETWORK_Features );
        if ( Error ) { return Error; }
        Config->Flags.All &= NETWORK_Features.Flags.All; // mask out unavailable flags
    }
    switch ( Config->LogicalIndex ) { // find NETWORK corresponding to MAC_index as defined by makefile.100_board_*
            #ifdef TTC_NETWORK1
        case 1: Config_mac->BaseRegister = ( t_network_register* ) TTC_NETWORK1;
            t_rimeaddr temp;
            switch ( TTC_NETWORK_ADDRESS_SIZE ) {
                case 1: temp.u8[0] = Config->LocalAddress;
                    break;
                case 2: temp.u8[0] = Config->LocalAddress;
                    temp.u8[1] = Config->LocalAddress >> 8;
                    break;
                case 4: temp.u8[0] = Config->LocalAddress;
                    temp.u8[1] = Config->LocalAddress >> 8;
                    temp.u8[2] = Config->LocalAddress >> 16;
                    temp.u8[3] = Config->LocalAddress >> 24;
                    break;
                case 8: temp.u8[0] = Config->LocalAddress;
                    temp.u8[1] = Config->LocalAddress >> 8;
                    temp.u8[2] = Config->LocalAddress >> 16;
                    temp.u8[3] = Config->LocalAddress >> 24;
                    t_u32 aux = Config->LocalAddress >> 32;
                    temp.u8[4] = aux;
                    temp.u8[5] = aux >> 8;
                    temp.u8[6] = aux >> 16;
                    temp.u8[7] = aux >> 24;
                    break;
                default: temp.u8[0] = Config->LocalAddress;
                    temp.u8[1] = Config->LocalAddress >> 8;
                    break;
            }

            rimeaddr_set_node_addr( &temp );
            queuebuf_init();
            macstack_init();
            packetbuf_clear();
            break;
            #endif
            #ifdef TTC_NETWORK2
        case 2: //Config_mac->BaseRegister = (t_network_register*) TTC_NETWORK2;
            t_rimeaddr temp;
            switch ( TTC_NETWORK_ADDRESS_SIZE ) {
                case 1: temp.u8[0] = Config->LocalAddress;
                    break;
                case 2: temp.u8[0] = Config->LocalAddress;
                    temp.u8[1] = Config->LocalAddress >> 8;
                    break;
                case 4: temp.u8[0] = Config->LocalAddress;
                    temp.u8[1] = Config->LocalAddress >> 8;
                    temp.u8[2] = Config->LocalAddress >> 16;
                    temp.u8[3] = Config->LocalAddress >> 24;
                    break;
                case 8: temp.u8[0] = Config->LocalAddress;
                    temp.u8[1] = Config->LocalAddress >> 8;
                    temp.u8[2] = Config->LocalAddress >> 16;
                    temp.u8[3] = Config->LocalAddress >> 24;
                    t_u32 aux = Config->LocalAddress >> 32;
                    temp.u8[4] = aux;
                    temp.u8[5] = aux >> 8;
                    temp.u8[6] = aux >> 16;
                    temp.u8[7] = aux >> 24;
                    break;
                default: temp.u8[0] = Config->LocalAddress;
                    temp.u8[1] = Config->LocalAddress >> 8;
                    break;
            }

            rimeaddr_set_node_addr( &temp );
            queuebuf_init();
            macstack_init();
            packetbuf_clear();
            break;
            #endif
            #ifdef TTC_NETWORK3
        case 3: //Config_mac->BaseRegister = (t_network_register*) TTC_NETWORK3;
            t_rimeaddr temp;
            switch ( TTC_NETWORK_ADDRESS_SIZE ) {
                case 1: temp.u8[0] = Config->LocalAddress;
                    break;
                case 2: temp.u8[0] = Config->LocalAddress;
                    temp.u8[1] = Config->LocalAddress >> 8;
                    break;
                case 4: temp.u8[0] = Config->LocalAddress;
                    temp.u8[1] = Config->LocalAddress >> 8;
                    temp.u8[2] = Config->LocalAddress >> 16;
                    temp.u8[3] = Config->LocalAddress >> 24;
                    break;
                case 8: temp.u8[0] = Config->LocalAddress;
                    temp.u8[1] = Config->LocalAddress >> 8;
                    temp.u8[2] = Config->LocalAddress >> 16;
                    temp.u8[3] = Config->LocalAddress >> 24;
                    t_u32 aux = Config->LocalAddress >> 32;
                    temp.u8[4] = aux;
                    temp.u8[5] = aux >> 8;
                    temp.u8[6] = aux >> 16;
                    temp.u8[7] = aux >> 24;
                    break;
                default: temp.u8[0] = Config->LocalAddress;
                    temp.u8[1] = Config->LocalAddress >> 8;
                    break;
            }

            rimeaddr_set_node_addr( &temp );
            queuebuf_init();
            macstack_init();
            packetbuf_clear();
            break;
            #endif
        default: Assert_NETWORK( 0, ttc_assert_origin_auto ); break; // No TTC_NETWORKn defined! Check your makefile.100_board_* file!
    }

    return ec_network_OK;
}

e_ttc_network_errorcode network_mac_deinit( t_ttc_network_config* Config ) {
    Assert_NETWORK( Config, ttc_assert_origin_auto );
    Assert_NETWORK( Config->LogicalIndex > 0, ttc_assert_origin_auto );
    t_network_mac_config* Config_mac = network_mac_get_configuration( Config );
    ( void ) Config_mac; // avoid warning: "unused variable"

    // ToDo: Deinitialize hardware device

    macstack_deinit();
    for ( t_u8 i = 0; i <= Config->TxPipe->Size; i++ ) {
        t_ttc_list_item* Item = ttc_list_pop_front_single_wait( Config->TxPipe );
        Assert( Item, ttc_assert_origin_auto );
        t_ttc_heap_block_from_pool* Block = ttc_heap_pool_from_list_item( Item );
        ttc_heap_pool_block_free( Block );
    }
    for ( t_u8 i = 0; i <= Config->RxPipe.Size; i++ ) {
        t_ttc_list_item* Item = ttc_list_pop_front_single_wait( &( Config->RxPipe ) );
        Assert( Item, ttc_assert_origin_auto );
        t_ttc_heap_block_from_pool* Block = ttc_heap_pool_from_list_item( Item );
        ttc_heap_pool_block_free( Block );
    }

    return ec_network_OK;
}

e_ttc_network_errorcode network_mac_load_defaults( t_ttc_network_config* Config ) {
    Assert_NETWORK( Config != NULL, ttc_assert_origin_auto );
    Assert_NETWORK( Config->LogicalIndex > 0, ttc_assert_origin_auto );
    if ( Config->LogicalIndex > TTC_NETWORK_AMOUNT )
    { return ec_network_DeviceNotFound; }

    Config->TxPipe = &TxPipe;
    if ( Config->TxPipe == NULL )
    { ttc_list_init( Config->TxPipe, "TxPipe-MACx" ); }

    Config->RxPipe = RxPipe;
    if ( Config->RxPipe.First == NULL )
    { ttc_list_init( &( Config->RxPipe ), "RxPipe-MACx" ); }

    switch ( TTC_NETWORK_ADDRESS_SIZE ) {
        case 1:
            Config->LocalAddress = 0x00;
            Config->Netmask = 0x00;
            break;
        case 2:
            Config->LocalAddress = 0x0000;
            Config->Netmask = 0x0000;
            break;
        case 4:
            Config->LocalAddress = 0x00000000;
            Config->Netmask = 0x00000000;
            break;
        case 8:
            Config->LocalAddress = 0x0000000000000000;
            Config->Netmask = 0x0000000000000000;
            break;
        default: Assert_NETWORK( 0, ttc_assert_origin_auto );
            break; // No right size defined!
    }

    Config->MacStack = tntm_undefined;
    Config->Stack = tnts_undefined;
    Config->Protocol = tntp_undefined;
    Config->Channel = 0;

    Config->Flags.All                        = 0;

    return ec_network_OK;
}

e_ttc_network_errorcode  network_mac_reset( t_ttc_network_config* Config ) {
    Assert_NETWORK( Config != NULL, ttc_assert_origin_auto );
    Assert_NETWORK( Config->LogicalIndex > 0, ttc_assert_origin_auto );

    macstack_deinit();

    for ( t_u8 i = 0; i <= Config->TxPipe->Size; i++ ) {
        t_ttc_list_item* Item = ttc_list_pop_front_single_wait( Config->TxPipe );
        Assert( Item, ttc_assert_origin_auto );
        t_ttc_heap_block_from_pool* Block = ttc_heap_pool_from_list_item( Item );
        ttc_heap_pool_block_free( Block );
    }
    for ( t_u8 i = 0; i <= Config->RxPipe.Size; i++ ) {
        t_ttc_list_item* Item = ttc_list_pop_front_single_wait( &( Config->RxPipe ) );
        Assert( Item, ttc_assert_origin_auto );
        t_ttc_heap_block_from_pool* Block = ttc_heap_pool_from_list_item( Item );
        ttc_heap_pool_block_free( Block );
    }

    if ( network_mac_init( Config ) )
    { return ec_network_OK; }
    else { return ec_network_ERROR; }
}

e_ttc_network_errorcode network_mac_send_unicast( t_ttc_network_config* Config, ttc_network_address_t Target, t_u8* Buffer, t_base Amount ) {
    Assert_NETWORK( Config, ttc_assert_origin_auto );
    Assert_NETWORK( Target, ttc_assert_origin_auto );
    t_rimeaddr Dest = ttc_network_address_to_rimeaddr( &Target );

    packetbuf_clear();
    rime_ptr = packetbuf_dataptr();
    packetbuf_set_attr( PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS, MAX_MAC_TRANSMISSIONS );
    packetbuf_set_attr( PACKETBUF_ATTR_NETWORK_ID, Config->NetworkID );
    packetbuf_set_attr( PACKETBUF_ATTR_CHANNEL, Config->Channel );
    packetbuf_set_attr( PACKETBUF_ATTR_PACKET_TYPE, PACKETBUF_ATTR_PACKET_TYPE_DATA );

    //rimeaddr_copy(&dest, (const t_rimeaddr *)Target);

    memcpy( rime_ptr, Buffer, Amount );
    packetbuf_set_datalen( Amount );
    //send_packet(&dest);

//{ send_packet(&dest)
    packetbuf_set_addr( PACKETBUF_ADDR_RECEIVER, &Dest );
    packetbuf_set_attr( PACKETBUF_ATTR_RELIABLE, 1 ); //???
    NETSTACK_MAC.send( &_network_packet_sent_, NULL );
    //NETSTACK_MAC.send((mac_callback_t)Config->function_ReceivePacket, NULL);

    //watchdog_periodic();
    ttc_watchdog_reload1();
//} send_packet(&dest)

    if ( ( last_tx_status == MAC_TX_COLLISION ) || ( last_tx_status == MAC_TX_ERR ) ||
            ( last_tx_status == MAC_TX_ERR_FATAL ) )
    { return ec_network_ERROR; }
    else { return ec_network_OK; }
}

e_ttc_network_errorcode network_mac_send_broadcast( t_ttc_network_config* Config, t_u8* Buffer, t_base Amount ) {
    Assert_NETWORK( Config, ttc_assert_origin_auto );

    packetbuf_clear();
    rime_ptr = packetbuf_dataptr();
    packetbuf_set_attr( PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS, MAX_MAC_TRANSMISSIONS );
    packetbuf_set_attr( PACKETBUF_ATTR_NETWORK_ID, Config->NetworkID );
    packetbuf_set_attr( PACKETBUF_ATTR_CHANNEL, Config->Channel );
    packetbuf_set_attr( PACKETBUF_ATTR_PACKET_TYPE, PACKETBUF_ATTR_PACKET_TYPE_DATA );

    //rimeaddr_copy(&dest, (const t_rimeaddr *)Target);

    memcpy( rime_ptr, Buffer, Amount );
    packetbuf_set_datalen( Amount );
    //send_packet(&dest);

//{ send_packet(&dest)
    packetbuf_set_addr( PACKETBUF_ADDR_RECEIVER, &rimeaddr_null );
    packetbuf_set_attr( PACKETBUF_ATTR_RELIABLE, 1 ); //???
    NETSTACK_MAC.send( &_network_packet_sent_, NULL );
    //NETSTACK_MAC.send((mac_callback_t)Config->function_ReceivePacket, NULL);

    //watchdog_periodic();
    ttc_watchdog_reload1();
//} send_packet(&dest)

    if ( ( last_tx_status == MAC_TX_COLLISION ) || ( last_tx_status == MAC_TX_ERR ) ||
            ( last_tx_status == MAC_TX_ERR_FATAL ) )
    { return ec_network_ERROR; }
    else { return ec_network_OK; }
}

void network_mac_receive( t_ttc_network_config* Config ) {
    #ifdef EXTENSION_network_6lowpan_uip
    NETSTACK_NETWORK.input();
    #else
    void* Item;
    Item = NETSTACK_MAC.pending_packet();
    if ( Item != NULL )
    { ttc_list_push_back_single( &( Config->RxPipe ), ( t_ttc_list_item* )Item ); }
    #endif
}

t_ttc_list* network_mac_get_neighbours( t_ttc_network_config* Config ) {
    //NETSTACK_MAC.send(TaskFunction, ptr);
    ( void ) Config;
    return 0;
}

//}FunctionDefinitions
//{ private functions (ideally) *********************************************
void _network_packet_sent_( void* ptr, int status, int transmissions ) {
    ( void )transmissions;
    ( void )ptr;

//    if(callback != NULL)
//        callback->output_callback(status);

    last_tx_status = status;
}

//} private functions
