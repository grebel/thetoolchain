/*
 * Copyright (c) 2010, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 * $Id: csma.c,v 1.27 2011/01/25 14:24:38 adamdunkels Exp $
 */

/**
 * \file
 *         A Carrier Sense Multiple Access (CSMA) MAC layer
 * \author
 *         Adam Dunkels <adam@sics.se>
 */

#include "../../ttc_memory.h"
#include "../../ttc_list.h"
#include "mac_ieee802_15_4_csma.h"
#include "mac_ieee802_15_4_rdc.h"

#include "../support/network_adapt_ctimer.h"
#include "../support/network_adapt_clock.h"
#include "../support/network_adapt_random.h"
#include "../support/network_adapt_list.h"
#include "../support/network_adapt_memb.h"

#include "../support/network_rime_packetbuf.h"
#include "../support/network_rime_queuebuf.h"

#ifdef EXTENSION_network_6lowpan_uip
#  include "network_6lowpan_netstack.h"
#else
#  include "mac_ieee802_15_4_macstack.h"
#endif

#include <string.h>

//#define DEBUG 0
//#if DEBUG
//#include <stdio.h>
//#define PRINTF(...) printf(__VA_ARGS__)
//#else /* DEBUG */
#define PRINTF(...)
//#endif /* DEBUG */

#ifndef CSMA_MAX_MAC_TRANSMISSIONS
#ifdef CSMA_CONF_MAX_MAC_TRANSMISSIONS
#define CSMA_MAX_MAC_TRANSMISSIONS CSMA_CONF_MAX_MAC_TRANSMISSIONS
#else
#define CSMA_MAX_MAC_TRANSMISSIONS 3
#endif /* CSMA_CONF_MAX_MAC_TRANSMISSIONS */
#endif /* CSMA_MAX_MAC_TRANSMISSIONS */

#if CSMA_MAX_MAC_TRANSMISSIONS < 1
#error CSMA_CONF_MAX_MAC_TRANSMISSIONS must be at least 1.
#error Change CSMA_CONF_MAX_MAC_TRANSMISSIONS in contiki-conf.h or in your Makefile.
#endif /* CSMA_CONF_MAX_MAC_TRANSMISSIONS < 1 */

/* Packet metadata */
struct qbuf_metadata {
    mac_callback_t sent;
    void* cptr;
    t_u8 max_transmissions;
};

/* Every neighbor has its own packet queue */
struct neighbor_queue {
    struct neighbor_queue* next;
    t_rimeaddr addr;
    ctimer transmit_timer;
    t_u8 transmissions;
    t_u8 collisions, deferrals;
    list_t queued_packet_list;
};

/* The maximum number of co-existing neighbor queues */
#ifdef CSMA_CONF_MAX_NEIGHBOR_QUEUES
#define CSMA_MAX_NEIGHBOR_QUEUES CSMA_CONF_MAX_NEIGHBOR_QUEUES
#else
#define CSMA_MAX_NEIGHBOR_QUEUES 2
#endif /* CSMA_CONF_MAX_NEIGHBOR_QUEUES */

#define MAX_QUEUED_PACKETS QUEUEBUF_NUM
t_ttc_heap_pool* neighbor_memb = NULL;
t_ttc_heap_pool* packet_memb = NULL;
t_ttc_heap_pool* metadata_memb = NULL;

//MEMB(neighbor_memb, sizeof(struct neighbor_queue), CSMA_MAX_NEIGHBOR_QUEUES);
//MEMB(packet_memb, sizeof(struct rdc_buf_list), MAX_QUEUED_PACKETS);
//MEMB(metadata_memb, sizeof(struct qbuf_metadata), MAX_QUEUED_PACKETS);

//list_t neighbor_list;
t_ttc_list neighbor_list;

static void packet_sent( void* ptr, int status, int num_transmissions );
static void transmit_packet_list( void* ptr );

#ifndef EXTENSION_network_6lowpan_uip
t_ttc_list mac_rx_buffer;
#endif

/*---------------------------------------------------------------------------*/
#ifndef EXTENSION_network_6lowpan_uip
static void* pending_packet( void ) {
    t_ttc_list_item* item;

    if ( mac_rx_buffer.First != NULL ) {
        item = ttc_list_pop_front_single_wait( &mac_rx_buffer );
        return ( void* )item;
    }
    return NULL;
}
#endif
/*---------------------------------------------------------------------------*/
static struct
neighbor_queue* neighbor_queue_from_addr( const t_rimeaddr* addr ) {
    struct neighbor_queue* n = list_head( &neighbor_list );
    while ( n != NULL ) {
        if ( rimeaddr_cmp( &n->addr, addr ) ) {
            return n;
        }
        n = list_item_next( n );
    }
    return NULL;
}
/*---------------------------------------------------------------------------*/
static t_clock_time
default_timebase( void ) {
    t_clock_time time;
    /* The retransmission time must be proportional to the channel
       check interval of the underlying radio duty cycling layer. */
    time = NETSTACK_RDC.channel_check_interval();

    /* If the radio duty cycle has no channel check interval (i.e., it
       does not turn the radio off), we make the retransmission time
       proportional to the configured MAC channel check rate. */
    if ( time == 0 ) {
        time = CLOCK_SECOND / NETSTACK_RDC_CHANNEL_CHECK_RATE;
    }
    return time;
}
/*---------------------------------------------------------------------------*/
static void
transmit_packet_list( void* ptr ) {
    struct neighbor_queue* n = ptr;
    if ( n ) {
        struct rdc_buf_list* q = list_head( n->queued_packet_list );
        if ( q != NULL ) {
            PRINTF( "csma: preparing number %d %p, queue len %d\n", n->transmissions, q,
                    list_length( n->queued_packet_list ) );
            /* Send packets in the neighbor's list */
            NETSTACK_RDC.send_list( packet_sent, n, q );
        }
    }
}
/*---------------------------------------------------------------------------*/
static void
free_first_packet( struct neighbor_queue* n ) {
    struct rdc_buf_list* q = list_head( n->queued_packet_list );
    if ( q != NULL ) {
        /* Remove first packet from list and deallocate */
        queuebuf_free( q->buf );
        list_pop( n->queued_packet_list );
        memb_free( metadata_memb, q->ptr );
        memb_free( packet_memb, q );
        PRINTF( "csma: free_queued_packet, queue length %d\n",
                list_length( n->queued_packet_list ) );
        if ( list_head( n->queued_packet_list ) ) {
            /* There is a next packet. We reset current tx information */
            n->transmissions = 0;
            n->collisions = 0;
            n->deferrals = 0;
            /* Set a timer for next transmissions */
            ctimer_set( &n->transmit_timer, default_timebase(), transmit_packet_list, n );
        }
        else {
            /* This was the last packet in the queue, we free the neighbor */
            ctimer_stop( &n->transmit_timer );
            list_remove( &neighbor_list, n );
            memb_free( neighbor_memb, n );
        }
    }
}
/*---------------------------------------------------------------------------*/
static void
packet_sent( void* ptr, int status, int num_transmissions ) {
    struct neighbor_queue* n = ptr;
    struct rdc_buf_list* q = list_head( n->queued_packet_list );
    struct qbuf_metadata* metadata = ( struct qbuf_metadata* )q->ptr;
    t_clock_time time = 0;
    mac_callback_t sent;
    void* cptr;
    int num_tx;
    int backoff_transmissions;

    switch ( status ) {
        case MAC_TX_OK:
        case MAC_TX_NOACK:
            n->transmissions++;
            break;
        case MAC_TX_COLLISION:
            n->collisions++;
            break;
        case MAC_TX_DEFERRED:
            n->deferrals++;
            break;
    }

    sent = metadata->sent;
    cptr = metadata->cptr;
    num_tx = n->transmissions;

    if ( status == MAC_TX_COLLISION ||
            status == MAC_TX_NOACK ) {

        /* If the transmission was not performed because of a collision or
           noack, we must retransmit the packet. */

        switch ( status ) {
            case MAC_TX_COLLISION:
                PRINTF( "csma: rexmit collision %d\n", n->transmissions );
                break;
            case MAC_TX_NOACK:
                PRINTF( "csma: rexmit noack %d\n", n->transmissions );
                break;
            default:
                PRINTF( "csma: rexmit err %d, %d\n", status, n->transmissions );
        }

        /* The retransmission time must be proportional to the channel
           check interval of the underlying radio duty cycling layer. */
        time = default_timebase();

        /* The retransmission time uses a linear backoff so that the
           interval between the transmissions increase with each
           retransmit. */
        backoff_transmissions = n->transmissions + 1;

        /* Clamp the number of backoffs so that we don't get a too long
           timeout here, since that will delay all packets in the
           queue. */
        if ( backoff_transmissions > 3 ) {
            backoff_transmissions = 3;
        }

        time = time + ( random_rand() % ( backoff_transmissions * time ) );

        if ( n->transmissions < metadata->max_transmissions ) {
            PRINTF( "csma: retransmitting with time %lu %p\n", time, q );
            ctimer_set( &n->transmit_timer, time,
                        transmit_packet_list, n );
            /* This is needed to correctly attribute energy that we spent
               transmitting this packet. */
            queuebuf_update_attr_from_packetbuf( q->buf );
        }
        else {
            PRINTF( "csma: drop with status %d after %d transmissions, %d collisions\n",
                    status, n->transmissions, n->collisions );
            free_first_packet( n );
            mac_call_sent_callback( sent, cptr, status, num_tx );
        }
    }
    else {
        if ( status == MAC_TX_OK ) {
            PRINTF( "csma: rexmit ok %d\n", n->transmissions );
        }
        else {
            PRINTF( "csma: rexmit failed %d: %d\n", n->transmissions, status );
        }
        free_first_packet( n );
        mac_call_sent_callback( sent, cptr, status, num_tx );
    }
}
/*---------------------------------------------------------------------------*/
static void
send_packet( mac_callback_t sent, void* ptr ) {
    struct rdc_buf_list* q;
    struct neighbor_queue* n;
    static ut_int16 seqno;

    packetbuf_set_attr( PACKETBUF_ATTR_MAC_SEQNO, seqno++ );

    /* If the packet is a broadcast, do not allocate a queue
       entry. Instead, just send it out.  */
    if ( !rimeaddr_cmp( packetbuf_addr( PACKETBUF_ADDR_RECEIVER ), &rimeaddr_null ) ) {
        const t_rimeaddr* addr = packetbuf_addr( PACKETBUF_ADDR_RECEIVER );

        /* Look for the neighbor entry */
        n = neighbor_queue_from_addr( addr );
        if ( n == NULL ) {
            /* Allocate a new neighbor entry */
            n = memb_alloc( neighbor_memb );
            if ( n != NULL ) {
                /* Init neighbor entry */
                rimeaddr_copy( &n->addr, addr );
                n->transmissions = 0;
                n->collisions = 0;
                n->deferrals = 0;
                /* Init packet list for this neighbor */
                n->queued_packet_list = ttc_list_create( ( const char* )n->queued_packet_list );
                //ttc_list_init(n->queued_packet_list, (const char*)n->queued_packet_list);
                /* Add neighbor to the list */
                list_add( &neighbor_list, n );
            }
        }

        if ( n != NULL ) {
            /* Add packet to the neighbor's queue */
            q = memb_alloc( packet_memb );
            if ( q != NULL ) {
                q->ptr = memb_alloc( metadata_memb );
                if ( q->ptr != NULL ) {
                    q->buf = queuebuf_new_from_packetbuf();
                    if ( q->buf != NULL ) {
                        struct qbuf_metadata* metadata = ( struct qbuf_metadata* )q->ptr;
                        /* Neighbor and packet successfully allocated */
                        if ( packetbuf_attr( PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS ) == 0 ) {
                            /* Use default configuration for max transmissions */
                            metadata->max_transmissions = CSMA_MAX_MAC_TRANSMISSIONS;
                        }
                        else {
                            metadata->max_transmissions =
                                packetbuf_attr( PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS );
                        }
                        metadata->sent = sent;
                        metadata->cptr = ptr;

                        if ( packetbuf_attr( PACKETBUF_ATTR_PACKET_TYPE ) ==
                                PACKETBUF_ATTR_PACKET_TYPE_ACK ) {
                            list_push( n->queued_packet_list, q );
                        }
                        else {
                            list_add( n->queued_packet_list, q );
                        }

                        /* If q is the first packet in the neighbor's queue, send asap */
                        if ( list_head( n->queued_packet_list ) == q ) {
                            ctimer_set( &n->transmit_timer, 0, transmit_packet_list, n );
                        }
                        return;
                    }
                    memb_free( metadata_memb, q->ptr );
                    PRINTF( "csma: could not allocate queuebuf, dropping packet\n" );
                }
                memb_free( packet_memb, q );
                PRINTF( "csma: could not allocate queuebuf, dropping packet\n" );
            }
            /* The packet allocation failed. Remove and free neighbor entry if empty. */
            if ( list_length( n->queued_packet_list ) == 0 ) {
                list_remove( &neighbor_list, n );
                memb_free( neighbor_memb, n );
            }
            PRINTF( "csma: could not allocate packet, dropping packet\n" );
        }
        else {
            PRINTF( "csma: could not allocate neighbor, dropping packet\n" );
        }
        mac_call_sent_callback( sent, ptr, MAC_TX_ERR, 1 );
    }
    else {
        PRINTF( "csma: send broadcast\n" );
        NETSTACK_RDC.send( sent, ptr );
    }
}
/*---------------------------------------------------------------------------*/
static void
input_packet( void ) {
    #ifdef EXTENSION_network_6lowpan_uip
    NETSTACK_NETWORK.input();
    #else
//    t_ttc_list_item* Item = NULL;

    if ( packetbuf_attr( PACKETBUF_ATTR_PACKET_TYPE ) == PACKETBUF_ATTR_PACKET_TYPE_DATA ) {
//        memcpy(Item, packetbuf_dataptr(), packetbuf_datalen());
        ttc_list_push_back_single( &mac_rx_buffer, ( t_ttc_list_item* )packetbuf_dataptr() );
    }
    #endif
}
/*---------------------------------------------------------------------------*/
static int
on( void ) {
    return NETSTACK_RDC.on();
}
/*---------------------------------------------------------------------------*/
static int
off( int keep_radio_on ) {
    return NETSTACK_RDC.off( keep_radio_on );
}
/*---------------------------------------------------------------------------*/
static unsigned short
channel_check_interval( void ) {
    if ( NETSTACK_RDC.channel_check_interval ) {
        return NETSTACK_RDC.channel_check_interval();
    }
    return 0;
}
/*---------------------------------------------------------------------------*/
static void
init( void ) {
    /*  packet_memb = ttc_heap_pool_create(sizeof(struct qbuf_metadata), MAX_QUEUED_PACKETS);
      metadata_memb = ttc_heap_pool_create(sizeof(struct rdc_buf_list), MAX_QUEUED_PACKETS);
      neighbor_memb = ttc_heap_pool_create(sizeof(struct neighbor_queue), CSMA_MAX_NEIGHBOR_QUEUES);
    */

    MEMB( neighbor_memb, sizeof( struct neighbor_queue ), CSMA_MAX_NEIGHBOR_QUEUES );
    MEMB( packet_memb, sizeof( struct rdc_buf_list ), MAX_QUEUED_PACKETS );
    MEMB( metadata_memb, sizeof( struct qbuf_metadata ), MAX_QUEUED_PACKETS );

    ttc_list_init( &neighbor_list, "Neighbor_list" );

    #ifndef EXTENSION_network_6lowpan_uip
    ttc_list_init( &mac_rx_buffer, "MAC-APL-Buffer" );
    #endif

}
/*---------------------------------------------------------------------------*/
const struct mac_driver csma_driver = {
    "CSMA",
    init,
    send_packet,
    input_packet,
    on,
    off,
    channel_check_interval,
    #ifndef EXTENSION_network_6lowpan_uip
    pending_packet,
    #endif
};
/*---------------------------------------------------------------------------*/
