/**
 * \file
 *         Include file for the IEEE 802.15.4 mac stack (NETSTACK)
 * \author
 *         Francisco Estevez <fjestevez@fh-muenster.de>
 */

#ifndef MACSTACK_H
#define MACSTACK_H

#include "network_conf.h"

#ifndef NETSTACK_MAC
#ifdef NETSTACK_CONF_MAC
#define NETSTACK_MAC NETSTACK_CONF_MAC
#else /* NETSTACK_CONF_MAC */
#define NETSTACK_MAC     csma_driver //nullmac_driver
#endif /* NETSTACK_CONF_MAC */
#endif /* NETSTACK_MAC */

#ifndef NETSTACK_RDC
#ifdef NETSTACK_CONF_RDC
#define NETSTACK_RDC NETSTACK_CONF_RDC
#else /* NETSTACK_CONF_RDC */
#define NETSTACK_RDC     nullrdc_driver
#endif /* NETSTACK_CONF_RDC */
#endif /* NETSTACK_RDC */

#ifndef NETSTACK_RDC_CHANNEL_CHECK_RATE
#ifdef NETSTACK_CONF_RDC_CHANNEL_CHECK_RATE
#define NETSTACK_RDC_CHANNEL_CHECK_RATE NETSTACK_CONF_RDC_CHANNEL_CHECK_RATE
#else /* NETSTACK_CONF_RDC_CHANNEL_CHECK_RATE */
#define NETSTACK_RDC_CHANNEL_CHECK_RATE 8
#endif /* NETSTACK_CONF_RDC_CHANNEL_CHECK_RATE */
#endif /* NETSTACK_RDC_CHANNEL_CHECK_RATE */

#if (NETSTACK_RDC_CHANNEL_CHECK_RATE & (NETSTACK_RDC_CHANNEL_CHECK_RATE - 1)) != 0
#error NETSTACK_RDC_CONF_CHANNEL_CHECK_RATE must be a power of two (i.e., 1, 2, 4, 8, 16, 32, 64, ...).
#error Change NETSTACK_RDC_CONF_CHANNEL_CHECK_RATE in contiki-conf.h, project-conf.h or in your Makefile.
#endif


#ifndef NETSTACK_RADIO
#ifdef NETSTACK_CONF_RADIO
#define NETSTACK_RADIO NETSTACK_CONF_RADIO
#else /* NETSTACK_CONF_RADIO */
#define NETSTACK_RADIO serialradio_driver //nullradio_driver
#endif /* NETSTACK_CONF_RADIO */
#endif /* NETSTACK_RADIO */

#ifndef NETSTACK_FRAMER
#ifdef NETSTACK_CONF_FRAMER
#define NETSTACK_FRAMER NETSTACK_CONF_FRAMER
#else /* NETSTACK_CONF_FRAMER */
#define NETSTACK_FRAMER   framer_802154 //framer_nullmac
#endif /* NETSTACK_CONF_FRAMER */
#endif /* NETSTACK_FRAMER */

#include "mac_ieee802_15_4.h"
#include "mac_ieee802_15_4_rdc.h"
#include "mac_ieee802_15_4_framer.h"
#include "network_radio.h"

/**
 * The structure of a network driver in Contiki.
 */
struct network_driver {
    char* name;

    /** Initialize the network driver */
    void ( * init )( void );

    /** Callback for getting notified of incoming packet. */
    void ( * input )( void );
};

extern const struct rdc_driver     NETSTACK_RDC;
extern const struct mac_driver     NETSTACK_MAC;
extern const struct radio_driver   NETSTACK_RADIO;
extern const struct framer         NETSTACK_FRAMER;

void macstack_init( void );
void macstack_deinit( void );

#endif /* MACSTACK_H */
