#ifndef NETWORK_MAC_TYPES_H
#define NETWORK_MAC_TYPES_H

/** { 6lowpan_network.h ************************************************

                           The ToolChain

   High-Level interface for NETWORK device.

   Structures, Enums and Defines being required by ttc_network_types.h

   Note: See ttc_network.h for description of 6lowpan independent NETWORK implementation.

   Authors:


}*/
//{ Defines/ TypeDefs ******************************************************


//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic.h"
#include "../ttc_network_types.h"

//} Includes
//{ Structures/ Enums required by ttc_network_types.h ***********************

typedef struct { // register description (adapt according to 6lowpan registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_mac_register;

typedef struct {  // 6lowpan specific configuration data of single NETWORK device
    t_mac_register* BaseRegister;       // base address of NETWORK device registers
} __attribute__( ( __packed__ ) ) t_network_mac_config;

// t_ttc_network_arch is required by ttc_network_types.h
#ifndef EXTENSION_network_6lowpan_uip
typedef t_network_mac_config t_ttc_network_arch;
#endif
//} Structures/ Enums


#endif //NETWORK_MAC_TYPES_H
