#ifndef NETWORK_6LOWPAN_H
#define NETWORK_6LOWPAN_H

/** { network_6lowpan.h **********************************************
 
                           The ToolChain
                      
   High-Level interface for NETWORK device.

   Structures, Enums and Defines being required by high-level network and application.

   Note: See ttc_network.h for description of 6lowpan independent NETWORK implementation.
  
   Authors: 

 
}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

#include "network_6lowpan_types.h"
#include "../ttc_network_types.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"
//#include "network_6lowpan_netstack.h"

#include "network_rime_packetbuf.h"
#include "network_rimeaddr.h"

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_network_interface.h
#define Driver_network_prepare()             network_6lowpan_prepare()
#define Driver_network_reset(Config)         network_6lowpan_reset(Config)
#define Driver_network_load_defaults(Config) network_6lowpan_load_defaults(Config)
#define Driver_network_deinit(Config)        network_6lowpan_deinit(Config)
#define Driver_network_init(Config)          network_6lowpan_init(Config)
#define Driver_network_send_direct_raw(LogicalIndex, Target, Buffer, Amount) network_6lowpan_send_direct_raw(LogicalIndex, Target, Buffer, Amount)
#define Driver_network_send_broadcast_raw(LogicalIndex, Buffer, Amount) network_6lowpan_send_broadcast_raw(LogicalIndex, Buffer, Amount)
#define Driver_network_register_receive(Config, Packet) network_6lowpan_register_receive(Config, Packet)
#define Driver_network_get_neighbours(LogicalIndex) network_6lowpan_get_neighbours(LogicalIndex)

//} Includes
//{ Function prototypes **************************************************

/** Prepares network Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void network_6lowpan_prepare();

/** checks if NETWORK driver already has been initialized
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return              == TRUE: indexed NETWORK unit is already initialized
 */
bool network_6lowpan_check_initialized(t_ttc_network_config* Config);

/** returns reference to low-level configuration struct of NETWORK driver
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return              pointer to struct t_ttc_network_config (will assert if no configuration available)
 */
t_network_6lowpan_config* network_6lowpan_get_configuration(t_ttc_network_config* Config);

/** loads configuration of NETWORK driver with default values
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_network_errorcode network_6lowpan_load_defaults(t_ttc_network_config* Config);

/** fills out given Config with maximum valid values for indexed NETWORK
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_network_errorcode network_6lowpan_get_features(t_ttc_network_config* Config);

/** initializes single NETWORK
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return              == 0: NETWORK has been initialized successfully; != 0: error-code
 */
e_ttc_network_errorcode network_6lowpan_init(t_ttc_network_config* Config);

/** shutdown single NETWORK device
 * @param Config        pointer to struct t_ttc_network_config (must have valid value for LogicalIndex)
 * @return              == 0: NETWORK has been shutdown successfully; != 0: error-code
 */
e_ttc_network_errorcode network_6lowpan_deinit(t_ttc_network_config* Config);

/** reset configuration of network driver and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_NETWORK_get_max_LogicalIndex())
 */
e_ttc_network_errorcode network_6lowpan_reset(t_ttc_network_config* Config);

/** sends data from given buffer via network to given target address
 *
 * @param LogicalIndex  1..TTC_NETWORK_MAX
 * @param Target        protocol address of target node
 * @param Buffer        memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[] (even zero bytes are sent)
 */
void network_6lowpan_send_direct_raw(t_ttc_network_config* Config, ttc_network_address_t Target, t_u8* Buffer, t_base Amount);
#ifndef _driver_ttc_network_send_direct_raw
#warning Missing implementation for _driver_ttc_network_send_direct_raw()
#define _driver_ttc_network_send_direct_raw(Config, Target, Buffer, Amount)
#endif

/** sends data from given buffer via network to all other nodes
 *
 * @param LogicalIndex  1..TTC_NETWORK_MAX
 * @param Buffer        memory location from which to read data
 * @param Amount        amount of bytes to send from Buffer[] (even zero bytes are sent)
 */
void network_6lowpan_send_broadcast_raw(t_ttc_network_config* Config, t_u8* Buffer, t_base Amount);

/** registers function to be called for every packet being received from network
 *
 * @param LogicalIndex  1..TTC_NETWORK_MAX
 * @param receivePacket function will be called whenever a valid packet has beed received
 */
void network_6lowpan_register_receive(t_ttc_network_config* Config, void (*receivePacket)(ttc_network_packet_status* Status, t_u8* Buffer, t_base Amount));

/** returns list of all known neighbour nodes
 *
 * @param LogicalIndex  1..TTC_NETWORK_MAX
 * @return  list of protocol adresses of neighbour nodes
 */
t_ttc_network_node* network_6lowpan_get_neighbours(t_ttc_network_config* Config);
#ifndef _driver_ttc_network_get_neighbours
#  warning Missing implementation for _driver_ttc_network_get_neighbours()
#endif
//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //NETWORK_6LOWPAN_H