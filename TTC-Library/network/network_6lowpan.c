/** { network_6lowpan.c ************************************************
 
                           The ToolChain
                      
   High-Level interface for NETWORK device.
     
   Note: See ttc_network.h for description of 6lowpan independent NETWORK implementation.
  
   Authors: 
}*/

#include "network_6lowpan.h"

//{ Function definitions ***************************************************

void network_6lowpan_prepare() {
#if RIMEADDR_SIZE == 1
    const t_rimeaddr addr_ff = { { 0xff } };
#else /*RIMEADDR_SIZE == 2*/
#if RIMEADDR_SIZE == 2
    const t_rimeaddr addr_ff = { { 0xff, 0xff } };
#else /*RIMEADDR_SIZE == 2*/
#if RIMEADDR_SIZE == 8
    const t_rimeaddr addr_ff = { { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff } };
#endif /*RIMEADDR_SIZE == 8*/
#endif /*RIMEADDR_SIZE == 2*/
#endif /*RIMEADDR_SIZE == 1*/

  // add your startup code here (Singletasking!)
    queuebuf_init();
    netstack_init();
    rimeaddr_set_node_addr(&addr_ff);

    /*NETSTACK_RDC.init();
    NETSTACK_MAC.init();
    NETSTACK_NETWORK.init();*/

    //NETSTACK_RADIO.on();
    //NETSTACK_MAC.on();
    packetbuf_clear();
}
bool network_6lowpan_check_initialized(t_ttc_network_config* Config) {

    if (Config->LowLevelConfig)
        return TRUE;

    return FALSE;
}
t_network_6lowpan_config* network_6lowpan_get_configuration(t_ttc_network_config* Config) {
    Assert_NETWORK(Config != NULL, ttc_assert_origin_auto);
    Assert_NETWORK(Config->LogicalIndex > 0, ttc_assert_origin_auto);

    t_network_6lowpan_config* Config_6lowpan = Config->LowLevelConfig;
    if (Config_6lowpan == NULL) {  // first call: allocate + init configuration
      Config_6lowpan = ttc_heap_alloc_zeroed( sizeof(t_ttc_network_arch) );
      
      switch (Config->PhysicalIndex) {           // determine base register and other low-level configuration data
         case 0: { // load low-level configuration of first network device 
           Config_6lowpan->BaseRegister = NULL; // load adress of register base of NETWORK device
           break;
         }
         case 1: { // load low-level configuration of second network device
           Config_6lowpan->BaseRegister = NULL; // load adress of register base of NETWORK device
           break;
         } 
         case 2: { // load low-level configuration of third network device
           Config_6lowpan->BaseRegister = NULL; // load adress of register base of NETWORK device
           break;             
         }
         case 3: { // load low-level configuration of fourth network device
           Config_6lowpan->BaseRegister = NULL; // load adress of register base of NETWORK device
           break;
         }
         case 4: { // load low-level configuration of fifth network device
           Config_6lowpan->BaseRegister = NULL; // load adress of register base of NETWORK device
           break;
         }
         case 5: { // load low-level configuration of sixth network device
           Config_6lowpan->BaseRegister = NULL; // load adress of register base of NETWORK device
           break;
         }
         case 6: { // load low-level configuration of seveth network device
           Config_6lowpan->BaseRegister = NULL; // load adress of register base of NETWORK device
           break;
         }
         case 7: { // load low-level configuration of eighth network device
           Config_6lowpan->BaseRegister = NULL; // load adress of register base of NETWORK device
           break;
         }
      default: Assert_NETWORK(0, ttc_assert_origin_auto); break; // No TTC_NETWORKn defined! Check your makefile.100_board_* file!
      }
    }
    
    Assert_NETWORK(Config_6lowpan, ttc_assert_origin_auto);
    return Config_6lowpan;
}
e_ttc_network_errorcode network_6lowpan_get_features(t_ttc_network_config* Config) {
    Assert_NETWORK(Config,    ttc_assert_origin_auto);
    Assert_NETWORK(Config->LogicalIndex > 0, ttc_assert_origin_auto);
    if (Config->LogicalIndex > TTC_NETWORK_AMOUNT) return ec_network_DeviceNotFound;

    Config->Flags.All                        = 0;
    //Config->Flags.Bits.Master                = 1;

    switch (Config->LogicalIndex) {           // determine features of indexed NETWORK device
#ifdef TTC_NETWORK1
      case 1: break;
#endif
#ifdef TTC_NETWORK2
      case 2: break;
#endif
#ifdef TTC_NETWORK3
      case 3: break;
#endif
#ifdef TTC_NETWORK4
      case 4: break;
#endif
#ifdef TTC_NETWORK5
      case 5: break;
#endif
      default: Assert_NETWORK(0, ttc_assert_origin_auto); break; // No TTC_NETWORKn defined! Check your makefile.100_board_* file!
    }
    
    return ec_network_OK;
}
e_ttc_network_errorcode network_6lowpan_init(t_ttc_network_config* Config) {
    Assert_NETWORK(Config, ttc_assert_origin_auto);
    Assert_NETWORK(Config->LogicalIndex > 0, ttc_assert_origin_auto);
    t_network_6lowpan_config* Config_6lowpan = network_6lowpan_get_configuration(Config);
    (void) Config_6lowpan; // avoid warning: "unused variable"

    if (1) { // validate NETWORK_Features
        t_ttc_network_config NETWORK_Features;
        NETWORK_Features.LogicalIndex = Config->LogicalIndex;
        e_ttc_network_errorcode Error = network_6lowpan_get_features(&NETWORK_Features);
        if (Error) return Error;
        Config->Flags.All &= NETWORK_Features.Flags.All; // mask out unavailable flags
    }
    switch (Config->LogicalIndex) {                // find NETWORK corresponding to NETWORK_index as defined by makefile.100_board_*
#ifdef TTC_NETWORK1
      case 1: Config_6lowpan->Base = (NETWORK_t*) TTC_NETWORK1;
        rimeaddr_set_node_addr((t_rimeaddr) Config_mac->LocalAddress);
        queuebuf_init();
        netstack_init();
        packetbuf_clear();
        break;
#endif
#ifdef TTC_NETWORK2
      case 2: Config_6lowpan->Base = (NETWORK_t*) TTC_NETWORK2;
        rimeaddr_set_node_addr((t_rimeaddr) Config_mac->LocalAddress);
        queuebuf_init();
        netstack_init();
        packetbuf_clear();
        break;
#endif
#ifdef TTC_NETWORK3
      case 3: Config_6lowpan->Base = (NETWORK_t*) TTC_NETWORK3;
        rimeaddr_set_node_addr((t_rimeaddr) Config_mac->LocalAddress);
        queuebuf_init();
        netstack_init();
        packetbuf_clear();
        break;
#endif
      default: Assert_NETWORK(0, ttc_assert_origin_auto); break; // No TTC_NETWORKn defined! Check your makefile.100_board_* file!
    }
    
    return ec_network_OK;
}
e_ttc_network_errorcode network_6lowpan_deinit(t_ttc_network_config* Config) {
    Assert_NETWORK(Config, ttc_assert_origin_auto);
    Assert_NETWORK(Config->LogicalIndex > 0, ttc_assert_origin_auto);
    t_network_6lowpan_config* Config_6lowpan = network_6lowpan_get_configuration(Config);
    (void) Config_6lowpan; // avoid warning: "unused variable"

    // ToDo: Deinitialize hardware device
    
    return ec_network_OK;
}
e_ttc_network_errorcode network_6lowpan_load_defaults(t_ttc_network_config* Config) {
    Assert_NETWORK(Config != NULL, ttc_assert_origin_auto);
    Assert_NETWORK(Config->LogicalIndex > 0, ttc_assert_origin_auto);
    if (Config->LogicalIndex > TTC_NETWORK_AMOUNT)
        return ec_network_DeviceNotFound;

    if((!Config->TxPool) & (!Config->RxPool)){
    Config->TxPool = ttc_heap_pool_create(128,10);
    Config->RxPool = ttc_heap_pool_create(128,10);

    ttc_list_init(Config->TxPipe, "TxPipe-MACx");
    ttc_list_init(Config->RxPipe, "RxPipe-MACx");
    }

    switch(TTC_NETWORK_ADDRESS_SIZE){
    case 1:
        Config->LocalAddress = 0x00;
        Config->Netmask = 0x00;
        break;
    case 2:
        Config->LocalAddress = 0x0000;
        Config->Netmask = 0x0000;
        break;
    case 4:
        Config->LocalAddress = 0x00000000;
        Config->Netmask = 0x00000000;
        break;
    case 8:
        Config->LocalAddress = 0x0000000000000000;
        Config->Netmask = 0x0000000000000000;
        break;
    default:
        Config->LocalAddress = 0x00;
        Config->Netmask = 0x00;
        break;
    }

    Config->MacStack = tntm_undefined;
    Config->Stack = tnts_undefined;
    Config->Protocol = tntp_undefined;
    Config->Channel = 0;
    Config->Flags.All                        = 0;
    // NETWORK_Cfg->Flags.Bits.Master                = 1;

    return ec_network_OK;
}
e_ttc_network_errorcode  network_6lowpan_reset(t_ttc_network_config* Config) {
    Assert_NETWORK(Config != NULL, ttc_assert_origin_auto);
    Assert_NETWORK(Config->LogicalIndex > 0, ttc_assert_origin_auto);
  
    return ec_network_OK;
}
void network_6lowpan_send_direct_raw(t_ttc_network_config* Config, ttc_network_address_t Target, t_u8* Buffer, t_base Amount){
        //NETSTACK_MAC.send(TaskFunction, ptr);
    (void) Config;
    (void) Target;
    (void) Buffer;
    (void) Amount;
}
void network_6lowpan_send_broadcast_raw(t_ttc_network_config* Config, t_u8* Buffer, t_base Amount){
    //NETSTACK_MAC.send(TaskFunction, ptr);
    (void) Config;
    (void) Buffer;
    (void) Amount;
}
void network_6lowpan_register_receive(t_ttc_network_config* Config, void (*receivePacket)(ttc_network_packet_status* Status, t_u8* Buffer, t_base Amount)){
    //NETSTACK_MAC.send(TaskFunction, ptr);
    (void) Config;
}
t_ttc_network_node* network_6lowpan_get_neighbours(t_ttc_network_config* Config){
    //NETSTACK_MAC.send(TaskFunction, ptr);
    (void) Config;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) *********************************************


//} private functions
