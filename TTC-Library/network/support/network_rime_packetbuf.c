/**
 * \addtogroup packetbuf
 * @{
 */

/*
 * Copyright (c) 2006, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 * $Id: packetbuf.c,v 1.1 2010/06/14 19:19:16 adamdunkels Exp $
 */

/**
 * \file
 *         Rime buffer (packetbuf) management
 * \author
 *         Adam Dunkels <adam@sics.se>
 */

#include <string.h>

//#include "contiki-net.h"
#include "network_rime_packetbuf.h"
#include "network_rimeaddr.h"

struct packetbuf_attr packetbuf_attrs[PACKETBUF_NUM_ATTRS];
struct packetbuf_addr packetbuf_addrs[PACKETBUF_NUM_ADDRS];


static t_u16 buflen, bufptr;
static t_u8 hdrptr;

/* The declarations below ensure that the packet buffer is aligned on
   an even 16-bit boundary. On some platforms (most notably the
   msp430), having apotentially misaligned packet buffer may lead to
   problems when accessing 16-bit values. */
static t_u16 packetbuf_aligned[( PACKETBUF_SIZE + PACKETBUF_HDR_SIZE ) / 2 + 1];
static t_u8* packetbuf = ( t_u8* )packetbuf_aligned;

static t_u8* packetbufptr;

#define DEBUG 0
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

/*---------------------------------------------------------------------------*/
void
packetbuf_clear( void ) {
    buflen = bufptr = 0;
    hdrptr = PACKETBUF_HDR_SIZE;

    packetbufptr = &packetbuf[PACKETBUF_HDR_SIZE];
    packetbuf_attr_clear();
}
/*---------------------------------------------------------------------------*/
void
packetbuf_clear_hdr( void ) {
    hdrptr = PACKETBUF_HDR_SIZE;
}
/*---------------------------------------------------------------------------*/
int
packetbuf_copyfrom( const void* from, t_u16 len ) {
    t_u16 l;

    packetbuf_clear();
    l = len > PACKETBUF_SIZE ? PACKETBUF_SIZE : len;
    memcpy( packetbufptr, from, l );
    buflen = l;
    return l;
}
/*---------------------------------------------------------------------------*/
void
packetbuf_compact( void ) {
    int i, len;

    if ( packetbuf_is_reference() ) {
        memcpy( &packetbuf[PACKETBUF_HDR_SIZE], packetbuf_reference_ptr(),
                packetbuf_datalen() );
    }
    else if ( bufptr > 0 ) {
        len = packetbuf_datalen() + PACKETBUF_HDR_SIZE;
        for ( i = PACKETBUF_HDR_SIZE; i < len; i++ ) {
            packetbuf[i] = packetbuf[bufptr + i];
        }

        bufptr = 0;
    }
}
/*---------------------------------------------------------------------------*/
int
packetbuf_copyto_hdr( t_u8* to ) {
    #if DEBUG_LEVEL > 0
    {
        int i;
        PRINTF( "packetbuf_write_hdr: header:\n" );
        for ( i = hdrptr; i < PACKETBUF_HDR_SIZE; ++i ) {
            PRINTF( "0x%02x, ", packetbuf[i] );
        }
        PRINTF( "\n" );
    }
    #endif /* DEBUG_LEVEL */
    memcpy( to, packetbuf + hdrptr, PACKETBUF_HDR_SIZE - hdrptr );
    return PACKETBUF_HDR_SIZE - hdrptr;
}
/*---------------------------------------------------------------------------*/
int
packetbuf_copyto( void* to ) {
    #if DEBUG_LEVEL > 0
    {
        int i;
        char buffer[1000];
        char* bufferptr = buffer;

        bufferptr[0] = 0;
        for ( i = hdrptr; i < PACKETBUF_HDR_SIZE; ++i ) {
            bufferptr += sprintf( bufferptr, "0x%02x, ", packetbuf[i] );
        }
        PRINTF( "packetbuf_write: header: %s\n", buffer );
        bufferptr = buffer;
        bufferptr[0] = 0;
        for ( i = bufptr; i < buflen + bufptr; ++i ) {
            bufferptr += sprintf( bufferptr, "0x%02x, ", packetbufptr[i] );
        }
        PRINTF( "packetbuf_write: data: %s\n", buffer );
    }
    #endif /* DEBUG_LEVEL */
    if ( PACKETBUF_HDR_SIZE - hdrptr + buflen > PACKETBUF_SIZE ) {
        /* Too large packet */
        return 0;
    }
    memcpy( to, packetbuf + hdrptr, PACKETBUF_HDR_SIZE - hdrptr );
    memcpy( ( t_u8* )to + PACKETBUF_HDR_SIZE - hdrptr, packetbufptr + bufptr,
            buflen );
    return PACKETBUF_HDR_SIZE - hdrptr + buflen;
}
/*---------------------------------------------------------------------------*/
int
packetbuf_hdralloc( int size ) {
    if ( hdrptr >= size && packetbuf_totlen() + size <= PACKETBUF_SIZE ) {
        hdrptr -= size;
        return 1;
    }
    return 0;
}
/*---------------------------------------------------------------------------*/
void
packetbuf_hdr_remove( int size ) {
    hdrptr += size;
}
/*---------------------------------------------------------------------------*/
int
packetbuf_hdrreduce( int size ) {
    if ( buflen < size ) {
        return 0;
    }

    bufptr += size;
    buflen -= size;
    return 1;
}
/*---------------------------------------------------------------------------*/
void
packetbuf_set_datalen( t_u16 len ) {
    PRINTF( "packetbuf_set_len: len %d\n", len );
    buflen = len;
}
/*---------------------------------------------------------------------------*/
void*
packetbuf_dataptr( void ) {
    return ( void* )( &packetbuf[bufptr + PACKETBUF_HDR_SIZE] );
}
/*---------------------------------------------------------------------------*/
void*
packetbuf_hdrptr( void ) {
    return ( void* )( &packetbuf[hdrptr] );
}
/*---------------------------------------------------------------------------*/
void
packetbuf_reference( void* ptr, t_u16 len ) {
    packetbuf_clear();
    packetbufptr = ptr;
    buflen = len;
}
/*---------------------------------------------------------------------------*/
int
packetbuf_is_reference( void ) {
    return packetbufptr != &packetbuf[PACKETBUF_HDR_SIZE];
}
/*---------------------------------------------------------------------------*/
void*
packetbuf_reference_ptr( void ) {
    return packetbufptr;
}
/*---------------------------------------------------------------------------*/
t_u16
packetbuf_datalen( void ) {
    return buflen;
}
/*---------------------------------------------------------------------------*/
t_u8
packetbuf_hdrlen( void ) {
    return PACKETBUF_HDR_SIZE - hdrptr;
}
/*---------------------------------------------------------------------------*/
t_u16
packetbuf_totlen( void ) {
    return packetbuf_hdrlen() + packetbuf_datalen();
}
/*---------------------------------------------------------------------------*/
void
packetbuf_attr_clear( void ) {
    int i;
    for ( i = 0; i < PACKETBUF_NUM_ATTRS; ++i ) {
        packetbuf_attrs[i].val = 0;
    }
    for ( i = 0; i < PACKETBUF_NUM_ADDRS; ++i ) {
        rimeaddr_copy( &packetbuf_addrs[i].addr, &rimeaddr_null );
    }
}
/*---------------------------------------------------------------------------*/
void
packetbuf_attr_copyto( struct packetbuf_attr* attrs,
                       struct packetbuf_addr* addrs ) {
    memcpy( attrs, packetbuf_attrs, sizeof( packetbuf_attrs ) );
    memcpy( addrs, packetbuf_addrs, sizeof( packetbuf_addrs ) );
}
/*---------------------------------------------------------------------------*/
void
packetbuf_attr_copyfrom( struct packetbuf_attr* attrs,
                         struct packetbuf_addr* addrs ) {
    memcpy( packetbuf_attrs, attrs, sizeof( packetbuf_attrs ) );
    memcpy( packetbuf_addrs, addrs, sizeof( packetbuf_addrs ) );
}
/*---------------------------------------------------------------------------*/
#if !PACKETBUF_CONF_ATTRS_INLINE
int
packetbuf_set_attr( t_u8 type, const t_packetbuf_attr val ) {
    /*   packetbuf_attrs[type].type = type; */
    packetbuf_attrs[type].val = val;
    return 1;
}
/*---------------------------------------------------------------------------*/
t_packetbuf_attr
packetbuf_attr( t_u8 type ) {
    return packetbuf_attrs[type].val;
}
/*---------------------------------------------------------------------------*/
int
packetbuf_set_addr( t_u8 type, const t_rimeaddr* addr ) {
    /*   packetbuf_addrs[type - PACKETBUF_ADDR_FIRST].type = type; */
    rimeaddr_copy( &packetbuf_addrs[type - PACKETBUF_ADDR_FIRST].addr, addr );
    return 1;
}
/*---------------------------------------------------------------------------*/
const t_rimeaddr*
packetbuf_addr( t_u8 type ) {
    return &packetbuf_addrs[type - PACKETBUF_ADDR_FIRST].addr;
}
/*---------------------------------------------------------------------------*/
#endif /* PACKETBUF_CONF_ATTRS_INLINE */
/** @} */
