//
//  memb.h
//
//
//  Created by Francisco Estevez on 08/07/13.
//
//

#ifndef __MEMB_NETWORK_H__
#define __MEMB_NETWORK_H__

#include "../ttc_memory.h"

//typedef ttc_memory_t*{} ContikiMemb;
//#define ContikiMemb t_ttc_heap_pool
#define MEMB(name, structure, num) \
    name = ttc_heap_pool_create(structure, num)

void  memb_init( t_ttc_heap_pool* m );

void* memb_alloc( t_ttc_heap_pool* m );

char  memb_free( t_ttc_heap_pool* m, void* ptr );

int memb_inmemb( t_ttc_heap_pool* m, void* ptr );

#endif /* __MEMB_NETWORK_H__ */

