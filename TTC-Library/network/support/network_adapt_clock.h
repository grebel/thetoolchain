//
//  clock.h
//
//
//  Created by Francisco Estevez on 10/07/13.
//
//

#ifndef __CLOCK_H__
#define __CLOCK_H__

#include "../ttc_task.h"

#define CLOCK_CONF_SECOND 1000000*ttc_systick_get_elapsed_usecs()
#define CLOCK_SECOND CLOCK_CONF_SECOND

typedef unsigned int t_clock_time;

t_clock_time clock_time( void );

#endif /* __CLOCK_H__ */

