//
//  random.c
//
//
//  Created by Francisco Estevez on 08/07/13.
//
//

#include "network_adapt_random.h"

void random_init( unsigned short seed ) {
    ttc_random_seed( seed );
}

unsigned short random_rand( void ) {
    return ttc_random_generate();
}
