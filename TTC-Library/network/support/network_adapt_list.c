//
//  list.c
//
//
//  Created by Francisco Estevez on 08/07/13.
//
//

#include "network_adapt_list.h"

/*void list_init(list_t list){
    const char* cad = "NWK-list";
    ttc_list_init(list, cad);
}*/

void* list_head( list_t list ) {
    return ( void* ) list->First;
}

void* list_pop( list_t list ) {
    return ( void* ) ttc_list_pop_front_single_wait( list );
}

void* list_push( list_t list, void* item ) {
    t_ttc_list_item temp;
    t_ttc_list_item* aux = ( t_ttc_list_item* )item;
    temp.Next = list->First;
    ( *aux ).Next = temp.Next;
    list->First = aux;
    return ( void* ) list->First;
}

void* list_chop( list_t list ) {
    t_ttc_list_item* it = list->First;
    t_ttc_list_item* temp = list->Last;
    int i;
    for ( i = 0; i < list->Size; i++ ) {
        it = it->Next;
    }
    it->Next = NULL;
    list->Last = it;
    return ( void* ) temp;
}

void list_add( list_t list, void* item ) {
    ttc_list_push_back_single( list, ( t_ttc_list_item* )item );
}

void list_remove( list_t list, void* item ) {
    t_ttc_list_item* it = list->First;
    t_ttc_list_item* prev = list->First;
    bool found = FALSE;
    while ( ( it != list->Last ) && !found ) {
        if ( it == item ) {
            prev->Next = it->Next;
            found = TRUE;
        }

        *prev = *it;
        it = it->Next;
    }
}

int list_length( list_t list ) {
    return ( int ) s_ttc_listize( list );
}

void* list_item_next( void* item ) {
    t_ttc_list_item* it = ( t_ttc_list_item* )item;

    if ( it->Next != NULL )
    { return ( void* ) it->Next; }
    else { return NULL; }
}
