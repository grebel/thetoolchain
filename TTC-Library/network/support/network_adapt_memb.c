//
//  memb.c
//
//
//  Created by Francisco Estevez on 08/07/13.
//
//

#include "network_adapt_memb.h"

void  memb_init( t_ttc_heap_pool* m ) {
    //ttc_heap_pool_init(m, name_size, name_num);
}

void* memb_alloc( t_ttc_heap_pool* m ) {
    t_ttc_heap_block_from_pool* NewBlock = ttc_heap_pool_block_get( m );
    if ( NewBlock == NULL )
    { return ( void* ) NULL; }
    else { return ( void* ) NewBlock; }
}

char memb_free( t_ttc_heap_pool* m, void* ptr ) {
    t_ttc_heap_block_from_pool* ReceivedBlock = ( t_ttc_heap_block_from_pool* )ptr;
    ( void ) m;

    ttc_heap_pool_block_free( ReceivedBlock );

    return 0; //0 - Sucessfully desallocated
    //-1 - Error in the process
}

int memb_inmemb( t_ttc_heap_pool* m, void* ptr ) {
    return 1;
}

