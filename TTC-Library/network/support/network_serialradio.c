#include "network_serialradio.h"

static t_ttc_list TxList;
static t_ttc_list RxList;
static volatile t_u16 last_packet_timestamp;

/*---------------------------------------------------------------------------*/
static int receiving_packet( void );
t_ttc_heap_block* serial_receiveData( void* Argument, t_ttc_usart_config* USART_Generic, t_ttc_heap_block* Data );

/*---------------------------------------------------------------------------*/
void exu_activity_rx( struct s_ttc_usart_generic* USART_Generic, t_u8 Byte ) {
    ( void ) USART_Generic;
    ( void ) Byte;

    #ifdef TTC_LED2
    static t_u8 StateRX = 0;
    if ( StateRX++ & 1 )
    { ttc_gpio_set( TTC_LED2 ); }
    else
    { ttc_gpio_clr( TTC_LED2 ); }
    #endif
}

t_ttc_heap_block* serial_receiveData( void* Argument, t_ttc_usart_config* USART_Generic, t_ttc_heap_block* Data ) {
    Assert( USART_Generic != NULL, ttc_assert_origin_auto );
    Assert( Data          != NULL, ttc_assert_origin_auto );

    ( void ) Argument;
    char ReceivedMessage[PACKETBUF_SIZE];


    if ( Data->Size < PACKETBUF_SIZE - 1 ) {
        ttc_memory_copy( ReceivedMessage, Data->Buffer, Data->Size );
        Data->Buffer[Data->Size] = 0; // terminate string
    }
    else {
        ttc_memory_copy( ReceivedMessage, Data->Buffer, PACKETBUF_SIZE - 1 );
        Data->Buffer[PACKETBUF_SIZE - 1] = 0; // terminate string
    }
    ttc_list_push_back_single( &RxList, ( t_ttc_list_item* )ReceivedMessage );
    return Data;
}
/*---------------------------------------------------------------------------*/
static int
init( void ) {
    t_u8 MaxUSART = ttc_usart_get_max_logicalindex();
    Assert( MaxUSART > 0, ttc_assert_origin_auto ); // no USARTs defined by board makefile!
    e_ttc_usart_errorcode UsartError = tue_unknownError;

    if ( USART1_INDEX <= MaxUSART ) { // USART defined by board: initialize it
        t_ttc_usart_config* Config = ttc_usart_get_configuration( USART1_INDEX );
        Assert( Config, ttc_assert_origin_auto );

        // change some settings
        Config->Flags.Bits.IrqOnRxNE = 1; // enable IRQ on Rx-buffer not empty
        Config->Init.Char_EndOfLine = 13;      // packets end at carriage return

        if ( 0 )
        { Config->Init.Flags.DelayedTransmits = 1; } // all transmits are queued and sent in interrupt service routin parallel to this task
        else
        { Config->Init.Flags.DelayedTransmits = 0; } // all transmits are blocking until data is send (can loose rx-data while waiting for transmit!)

        // register activity indicators (will be called from interrupt service routine!)
        Config->Init.activity_rx_isr = exu_activity_rx;
//        Config->Init.activity_tx_isr = exu_activity_tx;

        UsartError = ttc_usart_init( USART1_INDEX ); // note: first bytes might get lost until ttc_usart_register_receive()
        ttc_usart_register_receive( USART1_INDEX, serial_receiveData, ( void* ) USART1_INDEX );
        Assert( UsartError == tue_OK, ttc_assert_origin_auto );
    }

    ttc_list_init( &TxList, "TX-Pipe" );
    ttc_list_init( &RxList, "RX-Pipe" );
    return 1;
}
/*---------------------------------------------------------------------------*/
static int
prepare( const void* payload, unsigned short payload_len ) {
    ( void )payload_len;
    ttc_list_push_back_single( &TxList, ( t_ttc_list_item* )payload );
    return RADIO_TX_OK;
}
/*---------------------------------------------------------------------------*/
static int
transmit( unsigned short transmit_len ) {
    t_ttc_usart_config* USART_Generic = ttc_usart_get_configuration( USART1_INDEX );
    t_u8* aux = NULL;
    t_ttc_list_item* ListItem;

    if ( TxList.First != NULL ) {
        ListItem = ttc_list_pop_front_single_wait( &TxList );
        aux = ( t_u8* ) ListItem;
    }
    else { return RADIO_TX_ERR; }

    //ttc_usart_send_string(USART1_INDEX, "Serial Radio TX Message: ", -1);
    //ttc_usart_send_string(USART1_INDEX, TempBuffer->Payload, -1);
    if ( ListItem != NULL ) {
        if ( ( receiving_packet() == RADIO_TX_OK ) && ( USART_Generic->Queue_Rx != NULL ) ) {
            ttc_usart_send_string( USART1_INDEX, ( char* )aux, transmit_len );
            ttc_usart_send_string( USART1_INDEX, packetbuf_dataptr(), 10 );
            ttc_usart_flush_tx( USART1_INDEX );
            //ttc_list_push_back_single(&RxList, ListItem);
            last_packet_timestamp = ( t_u16 )ttc_systick_get_elapsed_usecs();
            return RADIO_TX_OK;
        }
    }
    else { return RADIO_TX_ERR; }

    return RADIO_TX_OK;
}
/*---------------------------------------------------------------------------*/
static int
send( const void* payload, unsigned short payload_len ) {
    prepare( payload, payload_len );
    transmit( payload_len );
    return RADIO_TX_OK;
}
/*---------------------------------------------------------------------------*/
static int
read( void* buf, unsigned short buf_len ) {
    if ( RxList.First != NULL ) {
        buf = ttc_list_pop_front_single_wait( &RxList );
        buf_len = 19;

        packetbuf_set_attr( PACKETBUF_ATTR_RSSI, 80 );
        packetbuf_set_attr( PACKETBUF_ATTR_LINK_QUALITY, 85 );

        return buf_len;
    }
    else { return 0; }
}
/*---------------------------------------------------------------------------*/
static int
channel_clear( void ) {
    return RADIO_TX_OK;
}
/*---------------------------------------------------------------------------*/
static int
receiving_packet( void ) {
    int len = 0;

    if ( RxList.First != NULL ) {
        packetbuf_clear();
        packetbuf_set_attr( PACKETBUF_ATTR_TIMESTAMP, last_packet_timestamp );
        len = read( packetbuf_dataptr(), PACKETBUF_SIZE );

        packetbuf_set_datalen( len );

        NETSTACK_RDC.input();
    }
    else { return RADIO_TX_OK; }
    return 0; //Just to avoid warnings
}
/*---------------------------------------------------------------------------*/
static int
pending_packet( void ) {
    if ( TxList.First != NULL )
    { send( TxList.First, sizeof( TxList.First ) ); }
    else { return RADIO_TX_OK; }
    return 0; //Just to avoid warnings
}
/*---------------------------------------------------------------------------*/
static int
on( void ) {
    return RADIO_TX_OK;
}
/*---------------------------------------------------------------------------*/
static int
off( void ) {
    return RADIO_TX_OK;
}
/*---------------------------------------------------------------------------*/
const struct radio_driver serialradio_driver = {
    init,
    prepare,
    transmit,
    send,
    read,
    channel_clear,
    receiving_packet,
    pending_packet,
    on,
    off,
};
/*---------------------------------------------------------------------------*/
