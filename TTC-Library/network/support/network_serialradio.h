/**
 *
 * $Id: network_serialradio.h, 15.11.2013 fjestevez Exp $
 */

/**
 * \file
 *         Serial Radio definition
 * \author
 *         Francisco Estevez <fjestevez@fh-muenster.de>
 */

#ifndef __SERIALRADIO_H__
#define __SERIALRADIO_H__

#include "network_radio.h"
#include "../../ttc_basic.h"
#include "../../ttc_gpio.h"
#include "../../ttc_usart.h"
#include "../../ttc_string.h"
#include "../../ttc_queue.h"
#include "../../ttc_network_types.h"
#include "network_rime_packetbuf.h"
#include "../mac_ieee_802_15_4/mac_ieee802_15_4_macstack.h"

extern const struct radio_driver serialradio_driver;

#define USART1_INDEX 1                    // using first  USART available on current board

#endif /* __SERIALRADIO_H__ */


/** @} */
/** @} */
