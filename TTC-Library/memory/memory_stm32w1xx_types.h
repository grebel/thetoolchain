#ifndef MEMORY_STM32W1XX_TYPES_H
#define MEMORY_STM32W1XX_TYPES_H

/** { memory_stm32w1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for MEMORY devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by ttc_memory_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140228 13:50:05 UTC
 *
 *  Note: See ttc_memory.h for description of architecture independent MEMORY implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_MEMORY1   // device not defined in makefile
    #define TTC_MEMORY1    ta_memory_stm32w1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_memory_types.h *************************


//} Structures/ Enums


#endif //MEMORY_STM32W1XX_TYPES_H
