/** { memory_stm32f1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for memory devices on stm32f1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 21 at 20140304 09:29:26 UTC
 *
 *  Note: See ttc_memory.h for description of stm32f1xx independent MEMORY implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "memory_stm32f1xx.h"

//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

t_s8 memory_stm32f1xx_is_constant( volatile const void* Address ) {

    if ( ( ( ( t_u32 ) Address ) >= TTC_MEMORY_REGION_ROM_START ) &&
            ( ( ( t_u32 ) Address ) < TTC_MEMORY_REGION_ROM_START + ( TTC_MEMORY_REGION_ROM_SIZEK * 1024 ) )
       )
    { return 1; } // address is in ROM
    return 0;
}
t_s8 memory_stm32f1xx_is_readable( volatile const void* Address ) {
    if ( ( ( ( t_u32 ) Address ) >= TTC_MEMORY_REGION_ROM_START ) &&
            ( ( ( t_u32 ) Address ) < TTC_MEMORY_REGION_ROM_START + ( TTC_MEMORY_REGION_ROM_SIZEK * 1024 ) )
       )
    { return 1; } // address is in ROM

    return memory_stm32f1xx_is_writable( Address ); // if it's writable, it's also readable
}
t_s8 memory_stm32f1xx_is_writable( volatile const void* Address ) {

    // -> RM0008 p.50
    if ( ( ( ( t_u32 ) Address ) >= TTC_MEMORY_REGION_RAM_START ) &&
            ( ( ( t_u32 ) Address ) < TTC_MEMORY_REGION_RAM_START + ( TTC_MEMORY_REGION_RAM_SIZEK * 1024 ) )
       )
    { return 1; } // address is in RAM

    if ( ( ( ( t_u32 ) Address ) >= 0x40000000 ) &&
            ( ( ( t_u32 ) Address ) <  0x40030000 )
       )
    { return 2; } // address is in Peripherals

    if ( ( ( ( t_u32 ) Address ) >= 0x50000000 ) &&
            ( ( ( t_u32 ) Address ) <  0x50040000 )
       )
    { return 3; } // address is in USB OTG FS

    if ( ( ( ( t_u32 ) Address ) >= 0xa0000000 ) &&
            ( ( ( t_u32 ) Address ) <  0xa0001000 )
       )
    { return 4; } // address is in FSMC

    return 0;
}
t_s8 memory_stm32f1xx_is_executable( volatile const void* Address ) {

    if ( ( ( ( t_u32 ) Address ) >= TTC_MEMORY_REGION_ROM_START ) &&
            ( ( ( t_u32 ) Address ) < TTC_MEMORY_REGION_ROM_START + ( TTC_MEMORY_REGION_ROM_SIZEK * 1024 ) )
       )
    { return 1; } // address is in ROM

    // -> RM0008 p.50
    if ( ( ( ( t_u32 ) Address ) >= TTC_MEMORY_REGION_RAM_START ) &&
            ( ( ( t_u32 ) Address ) < TTC_MEMORY_REGION_RAM_START + ( TTC_MEMORY_REGION_RAM_SIZEK * 1024 ) )
       )
    { return 1; } // address is in RAM

    return ( t_s8 ) 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
