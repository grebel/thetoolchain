/** { memory_stm32l0xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for memory devices on stm32l0xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150317 10:29:21 UTC
 *
 *  Note: See ttc_memory.h for description of stm32l0xx independent MEMORY implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "memory_stm32l0xx.h".
//
#include "memory_stm32l0xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

t_s8 memory_stm32l0xx_is_constant(volatile const void* Address) {
    Assert_MEMORY(Address, ttc_assert_origin_auto); // pointers must not be NULL

    if (  ( ((t_u32) Address) > 0x08000000 ) && ( ((t_u32) Address) < 0x08040000 )  )
         return 1;
     return 0;
}
t_s8 memory_stm32l0xx_is_readable(volatile const void* Address) {
    Assert_MEMORY(Address, ttc_assert_origin_auto); // pointers must not be NULL

    if (( ((t_u32) Address)< 0x2000C000)  || (((((t_u32) Address) > 0x40000000) && ((t_u32) Address)< 0x40026800)))
        return 1;
    return 0;
}
t_s8 memory_stm32l0xx_is_writable(volatile const void* Address) {
    Assert_MEMORY(Address, ttc_assert_origin_auto); // pointers must not be NULL
    if (  ( ((t_u32) Address) >= 0x20000000) && ( ((t_u32) Address) < 0x2000C000)  )
        return 1;
    return 0;
}
t_s8 memory_stm32l0xx_is_executable(volatile const void* Address) {
    Assert_MEMORY_EXTRA_Writable(Address, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function memory_stm32l0xx_is_executable() is empty.
    
    return (t_s8) 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
