/** { memory_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for memory devices on stm32l1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20141115 10:41:24 UTC
 *
 *  Note: See ttc_memory.h for description of stm32l1xx independent MEMORY implementation.
 *
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "memory_stm32l1xx.h".
//
#include "memory_stm32l1xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

// declared in ttc_basic.h which cannot be included from here
extern void ttc_assert_test( BOOL TestOK );

//}Global Variables
//{ Function Definitions *******************************************************

t_s8 memory_stm32l1xx_is_constant( volatile const void* Address ) {

    if ( ( ( ( t_u32 ) Address ) > 0x08000000 ) && ( ( ( t_u32 ) Address ) < 0x08040000 ) )
    { return 1; } // address is in FLASH

    return 0;
}
t_s8 memory_stm32l1xx_is_executable( volatile const void* Address ) {
    if ( ( ( ( t_u32 ) Address ) > 0x08000000 ) && ( ( ( t_u32 ) Address ) < 0x08040000 ) )
    { return 1; } // address is in FLASH

    // -> RM0008 p.50
    if ( ( ( ( t_u32 ) Address ) >= TTC_MEMORY_REGION_RAM_START ) &&
            ( ( ( t_u32 ) Address ) < TTC_MEMORY_REGION_RAM_START + ( TTC_MEMORY_REGION_RAM_SIZEK * 1024 ) )
       )
    { return 1; } // address is in RAM

    return 0;
}
t_s8 memory_stm32l1xx_is_readable( volatile const void* Address ) {
    if ( Address == NULL )
    { return 0; }

    if ( ( ( ( t_u32 ) Address ) < 0x2000C000 )  || ( ( ( ( ( t_u32 ) Address ) > 0x40000000 ) && ( ( t_u32 ) Address ) < 0x40026800 ) ) )
    { return 1; }

    return memory_stm32l1xx_is_writable( Address ); // if it's writable, it's also readable
}
t_s8 memory_stm32l1xx_is_writable( volatile const void* Address ) {

    // -> RM0008 p.50
    if ( ( ( ( t_u32 ) Address ) >= TTC_MEMORY_REGION_RAM_START ) &&
            ( ( ( t_u32 ) Address ) < TTC_MEMORY_REGION_RAM_START + ( TTC_MEMORY_REGION_RAM_SIZEK * 1024 ) )
       )
    { return 1; } // address is in RAM

    if ( ( ( ( t_u32 ) Address ) >= 0x40000000 ) &&
            ( ( ( t_u32 ) Address ) <  0x40030000 )
       )
    { return 2; } // address is in Peripherals

    if ( ( ( ( t_u32 ) Address ) >= 0x50000000 ) &&
            ( ( ( t_u32 ) Address ) <  0x50040000 )
       )
    { return 3; } // address is in USB OTG FS

    if ( ( ( ( t_u32 ) Address ) >= 0xa0000000 ) &&
            ( ( ( t_u32 ) Address ) <  0xa0001000 )
       )
    { return 4; } // address is in FSMC

    return 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
