#ifndef MEMORY_STM32F1XX_H
#define MEMORY_STM32F1XX_H

/** { memory_stm32f1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for memory devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by high-level memory and application.
 *
 *  Created from template device_architecture.h revision 22 at 20140304 09:29:26 UTC
 *
 *  Note: See ttc_memory.h for description of stm32f1xx independent MEMORY implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_MEMORY_STM32F1XX
//
// Implementation status of low-level driver support for memory devices on stm32f1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_MEMORY_STM32F1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_MEMORY_STM32F1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_MEMORY_STM32F1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_MEMORY_STM32F1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "memory_stm32f1xx_types.h"
#include "../ttc_memory_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_memory_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_memory_foo
//
#define ttc_driver_memory_is_constant(Address) memory_stm32f1xx_is_constant(Address)
#define ttc_driver_memory_is_readable(Address) memory_stm32f1xx_is_readable(Address)
#define ttc_driver_memory_is_writable(Address) memory_stm32f1xx_is_writable(Address)
#define ttc_driver_memory_is_executable memory_stm32f1xx_is_executable
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_memory.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_memory.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** checks if given memory address is known as being readonly by a pointer.
 *
 * This function is designed to be used for Assert() checks.
 *
 * E.g.: Assert(ttc_memory_is_constant(Pointer), ttc_assert_origin_auto);
 *
 * @return  ==1: memory is constant (resides in ROM or FLASH);
 *          ==0: memory is known not to be readonly (E.g. resides in RAM or is invalid)
 *         ==-1: memory status not known for current architecture
 * @param Address   =
 */
t_s8 memory_stm32f1xx_is_constant( volatile const void* Address );


/** checks if given memory address is known as being readable by a pointer.
 *
 * This function is designed to be used for Assert() checks.
 *
 * E.g.: Assert(ttc_memory_is_readable(Pointer), ttc_assert_origin_auto);
 *       foo = *Pointer;
 *
 * @return  ==1: memory is writable (resides in RAM or Registers);
 *          ==0: memory is known not to be writable != memory is readonly!
 *         ==-1: memory status not known for current architecture
 * @param Address   =
 */
t_s8 memory_stm32f1xx_is_readable( volatile const void* Address );


/** checks if given memory address is known as being writable by a pointer.
 *
 * This function is designed to be used for Assert() checks.
 *
 * E.g.: Assert(ttc_memory_is_writable(Pointer), ttc_assert_origin_auto);
 *       *Pointer = foo;
 *
 * @return  ==1: memory is writable (resides in RAM or Registers);
 *          ==0: memory is known not to be writable != memory is readonly!
 *         ==-1: memory status not known for current architecture
 * @param Address   =
 */
t_s8 memory_stm32f1xx_is_writable( volatile const void* Address );


/** checks if given memory address is known to store program code.
 *
 * This function is designed to be used for Assert() checks.
 *
 * E.g.: Assert(ttc_memory_is_executable(FunctionPointer), ttc_assert_origin_auto);
 *       FunctionPointer();
 *
 * @param Address  memory pointer to check
 * @return  ==1: memory is executable (resides in ROM or RAM);
 *          ==0: memory is known not to be executable
 *         ==-1: memory status not known for current architecture
 */
t_s8 memory_stm32f1xx_is_executable( volatile const void* Address );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _memory_stm32f1xx_foo(t_ttc_memory_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //MEMORY_STM32F1XX_H
