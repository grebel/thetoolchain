#ifndef MEMORY_STM32F1XX_TYPES_H
#define MEMORY_STM32F1XX_TYPES_H

/** { memory_stm32f1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for MEMORY devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by ttc_memory_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140304 09:29:26 UTC
 *
 *  Note: See ttc_memory.h for description of architecture independent MEMORY implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_MEMORY1   // device not defined in makefile
    #define TTC_MEMORY1    ta_memory_stm32f1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_memory_types.h *************************

typedef struct { // register description (adapt according to stm32f1xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_memory_register;

typedef struct {  // stm32f1xx specific configuration data of single MEMORY device
    t_memory_register* BaseRegister;       // base address of MEMORY device registers
} __attribute__( ( __packed__ ) ) t_memory_stm32f1xx_config;

// t_ttc_memory_architecture is required by ttc_memory_types.h
#define t_ttc_memory_architecture t_memory_stm32f1xx_config

//} Structures/ Enums


#endif //MEMORY_STM32F1XX_TYPES_H
