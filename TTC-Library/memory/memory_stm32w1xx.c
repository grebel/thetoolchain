/** { memory_stm32w1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for memory devices on stm32w1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 20 at 20140228 13:50:05 UTC
 *
 *  Note: See ttc_memory.h for description of stm32w1xx independent MEMORY implementation.
 * 
 *  Authors: Gregor Rebel
}*/

#include "memory_stm32w1xx.h"

//{ Function definitions *******************************************************

inline t_s8 memory_stm32w1xx_is_constant(volatile const void* Address) {

   if (  ( ((t_u32) Address) > 0x08000000 ) && ( ((t_u32) Address) < 0x08040000 )  )
        return 1;
    return 0;
}
inline t_s8 memory_stm32w1xx_is_readable(volatile const void* Address) {
    if ( ((t_u32) Address)< 0x5fffffff)
        return 1;
    return 0;
}
inline t_s8 memory_stm32w1xx_is_writable(volatile const void* Address) {
    if (  ( ((t_u32) Address) >= 0x20000000) && ( ((t_u32) Address) < 0x20010000)  )
        return 1;
    return 0;
}
t_s8 memory_stm32w1xx_is_executable(volatile const void* Address) {
    Assert_MEMORY_EXTRA_Writable(Address, ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function memory_stm32w1xx_is_executable() is empty.
    
    return (t_s8) 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
