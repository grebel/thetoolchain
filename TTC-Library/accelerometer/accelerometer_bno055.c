/** { accelerometer_bno055.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for accelerometer devices on bno055 architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 23 at 20150408 14:39:35 UTC
 *
 *  Note: See ttc_accelerometer.h for description of bno055 independent ACCELEROMETER implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "accelerometer_bno055.h".
//
#include "accelerometer_bno055.h"
#include "../ttc_memory.h"            // basic memory checks
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

e_ttc_accelerometer_errorcode accelerometer_bno055_get_features(t_ttc_accelerometer_config* Config) {
    Assert_ACCELEROMETER(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // always check incoming pointers!

    static t_ttc_accelerometer_config Features;
    if (!Features.LogicalIndex) { // called first time: initialize data
        
#     warning Function accelerometer_bno055_get_features() is empty.

    }
    Features.LogicalIndex = Config->LogicalIndex;
    
    return ec_accelerometer_OK;
}
e_ttc_accelerometer_errorcode accelerometer_bno055_deinit(t_ttc_accelerometer_config* Config) {
    Assert_ACCELEROMETER_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function accelerometer_bno055_deinit() is empty.
    
    return (e_ttc_accelerometer_errorcode) 0;
}
e_ttc_accelerometer_errorcode accelerometer_bno055_init(t_ttc_accelerometer_config* Config) {
    Assert_ACCELEROMETER_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function accelerometer_bno055_init() is empty.
    
    return (e_ttc_accelerometer_errorcode) 0;
}
e_ttc_accelerometer_errorcode accelerometer_bno055_load_defaults(t_ttc_accelerometer_config* Config) {
    Assert_ACCELEROMETER_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function accelerometer_bno055_load_defaults() is empty.
    
    return (e_ttc_accelerometer_errorcode) 0;
}
void accelerometer_bno055_prepare() {


#warning Function accelerometer_bno055_prepare() is empty.
    

}
t_ttc_accelerometer_measures* accelerometer_bno055_read_measures(t_ttc_accelerometer_config* Config) {
    Assert_ACCELEROMETER_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function accelerometer_bno055_read_measures() is empty.
    
    return (t_ttc_accelerometer_measures*) 0;
}
void accelerometer_bno055_reset(t_ttc_accelerometer_config* Config) {
    Assert_ACCELEROMETER_EXTRA(ttc_memory_is_writable(Config), ttc_assert_origin_auto); // check if it points to RAM (change for read only memory!)

#warning Function accelerometer_bno055_reset() is empty.
    

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
