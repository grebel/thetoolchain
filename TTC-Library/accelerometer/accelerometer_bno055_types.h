#ifndef ACCELEROMETER_BNO055_TYPES_H
#define ACCELEROMETER_BNO055_TYPES_H

/** { accelerometer_bno055.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *                     
 *  Low-Level datatypes for ACCELEROMETER devices on bno055 architectures.
 *  Structures, Enums and Defines being required by ttc_accelerometer_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150408 14:39:35 UTC
 *
 *  Note: See ttc_accelerometer.h for description of architecture independent ACCELEROMETER implementation.
 * 
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_accelerometer_types.h *************************

typedef struct { // register description (adapt according to bno055 registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} t_accelerometer_register;

typedef struct {  // bno055 specific configuration data of single accelerometer device
  t_accelerometer_register* BaseRegister;       // base address of accelerometer device registers
} __attribute__((__packed__)) t_accelerometer_bno055_config;

// t_ttc_accelerometer_architecture is required by ttc_accelerometer_types.h
#define t_ttc_accelerometer_architecture t_accelerometer_bno055_config

//} Structures/ Enums


#endif //ACCELEROMETER_BNO055_TYPES_H
