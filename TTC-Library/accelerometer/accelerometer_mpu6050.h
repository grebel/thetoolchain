#ifndef ACCELEROMETER_MPU6050_H
#define ACCELEROMETER_MPU6050_H

/** { accelerometer_mpu6050.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for accelerometer devices on mpu6050 architectures.
 *  Structures, Enums and Defines being required by high-level accelerometer and application.
 *
 *  Created from template device_architecture.h revision 22 at 20141020 08:58:47 UTC
 *
 *  Note: See ttc_accelerometer.h for description of mpu6050 independent ACCELEROMETER implementation.
 *  
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_ACCELEROMETER_MPU6050
//
// Implementation status of low-level driver support for accelerometer devices on mpu6050
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_ACCELEROMETER_MPU6050 '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_ACCELEROMETER_MPU6050 == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_ACCELEROMETER_MPU6050 to one from '-', 'o', '+' 
#endif

//}EXTENSION_STATUS_TTC_ACCELEROMETER_MPU6050

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "accelerometer_mpu6050.c"
//
#include "accelerometer_mpu6050_types.h"
#include "../ttc_accelerometer_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_accelerometer_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_accelerometer_foo
//
#define ttc_driver_accelerometer_deinit(Config) accelerometer_mpu6050_deinit(Config)
#define ttc_driver_accelerometer_get_features(Config) accelerometer_mpu6050_get_features(Config)
#define ttc_driver_accelerometer_init(Config) accelerometer_mpu6050_init(Config)
#define ttc_driver_accelerometer_load_defaults(Config) accelerometer_mpu6050_load_defaults(Config)
#define ttc_driver_accelerometer_prepare() accelerometer_mpu6050_prepare()
#define ttc_driver_accelerometer_reset(Config) accelerometer_mpu6050_reset(Config)
#define ttc_driver_accelerometer_read_measures(Config) accelerometer_mpu6050_read_measures(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_accelerometer.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_accelerometer.h for details.
 *
 * You normally should not need to add functions here. 
 * Add your private functions in the private section below. 
 */


/** shutdown single ACCELEROMETER unit device
 * @param Config        pointer to struct t_ttc_accelerometer_config (must have valid value for PhysicalIndex)
 * @return              == 0: ACCELEROMETER has been shutdown successfully; != 0: error-code
 */
e_ttc_accelerometer_errorcode accelerometer_mpu6050_deinit(t_ttc_accelerometer_config* Config);


/** fills out given Config with maximum valid values for indexed ACCELEROMETER
 * @param Config        = pointer to struct t_ttc_accelerometer_config (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_accelerometer_errorcode accelerometer_mpu6050_get_features(t_ttc_accelerometer_config* Config);


/** initializes single ACCELEROMETER unit for operation
 * @param Config        pointer to struct t_ttc_accelerometer_config (must have valid value for PhysicalIndex)
 * @return              == 0: ACCELEROMETER has been initialized successfully; != 0: error-code
 */
e_ttc_accelerometer_errorcode accelerometer_mpu6050_init(t_ttc_accelerometer_config* Config);


/** loads configuration of indexed ACCELEROMETER unit with default values
 * @param Config        pointer to struct t_ttc_accelerometer_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_accelerometer_errorcode accelerometer_mpu6050_load_defaults(t_ttc_accelerometer_config* Config);


/** Prepares accelerometer Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void accelerometer_mpu6050_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_accelerometer_config (must have valid value for PhysicalIndex)
 * @return   = 
 */
void accelerometer_mpu6050_reset(t_ttc_accelerometer_config* Config);


/** manually read latest measures from indexed accelerometer device
 *
 * @param LogicalIndex  logical index of accelerometer device (1..ttc_accelerometer_get_max_index() )
 * @return              !=NULL: pointer to measures being read; ==NULL: measures could NOT be read
 * @param Config   = 
 */
t_ttc_accelerometer_measures* accelerometer_mpu6050_read_measures(t_ttc_accelerometer_config* Config);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _accelerometer_mpu6050_foo(t_ttc_accelerometer_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //ACCELEROMETER_MPU6050_H