#ifndef ACCELEROMETER_MPU6050_TYPES_H
#define ACCELEROMETER_MPU6050_TYPES_H

/** { accelerometer_mpu6050.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *                     
 *  Low-Level datatypes for ACCELEROMETER devices on mpu6050 architectures.
 *  Structures, Enums and Defines being required by ttc_accelerometer_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20141020 08:58:47 UTC
 *
 *  Note: See ttc_accelerometer.h for description of architecture independent ACCELEROMETER implementation.
 * 
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "../ttc_i2c.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_accelerometer_types.h *************************

typedef struct {
    t_u16 who_am_i;
    t_u16 pwr_mgmt_1;
    t_u16 ctrl_reg1;
    t_u16 ctrl_reg2;
    t_u16 ctrl_reg3;
    t_u16 outx_l;
    t_u16 outx_h;
    t_u16 outy_l;
    t_u16 outy_h;
    t_u16 outz_l;
    t_u16 outz_h;
} __attribute__((__packed__)) t_accelerometer_mpu6050_registers;


typedef struct { // register description (adapt according to mpu6050 registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} t_accelerometer_register;

typedef struct {  // mpu6050 specific configuration data of single ACCELEROMETER device
  t_accelerometer_mpu6050_registers Registers;
  t_u16 SlaveAddress;                             // I2C slave address of MPU6050 to use
} __attribute__((__packed__)) t_accelerometer_mpu6050_config;

// t_ttc_accelerometer_architecture is required by ttc_accelerometer_types.h
#define t_ttc_accelerometer_architecture t_accelerometer_mpu6050_config

//} Structures/ Enums


#endif //ACCELEROMETER_MPU6050_TYPES_H
