#ifndef ACCELEROMETER_LIS3LV02DL_TYPES_H
#define ACCELEROMETER_LIS3LV02DL_TYPES_H

/** { accelerometer_lis3lv02dl.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for ACCELEROMETER devices on lis3lv02dl architectures.
 *  Structures, Enums and Defines being required by ttc_accelerometer_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20141020 08:59:34 UTC
 *
 *  Note: See ttc_accelerometer.h for description of architecture independent ACCELEROMETER implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "../ttc_gpio_types.h"
#include "../ttc_i2c.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

typedef struct {   // i2c_lis3lv02dl_REG_CTLREG1
    unsigned XEnable: 1;
    unsigned YEnable: 1;
    unsigned ZEnable: 1;
    unsigned Selftest: 1;
    unsigned FrequencyRate: 2;
    unsigned DevicePowermode: 2;
} __attribute__( ( __packed__ ) ) accelerometer_lis3lv02dl_REG_CTLREG1;

typedef struct {   // i2c_lis3lv02dl_REG_CTLREG2
    unsigned DataAligment: 1;
    unsigned SPIMode: 1;
    unsigned DataReadyEnable: 1;
    unsigned InterruptEnable: 1;
    unsigned Boot: 1;
    unsigned BigEndian: 1;
    unsigned BlockDataUpdate: 1;
    unsigned Fullscale: 1;
} __attribute__( ( __packed__ ) ) accelerometer_lis3lv02dl_REG_CTLREG2;

typedef struct {   // i2c_lis3lv02dl_REG_CTLREG3
    unsigned HPF_CutoffFreq: 2;
    unsigned reserved0: 2;
    unsigned FilterDataSelect: 1;
    unsigned HPF_FreeFall: 1;
    unsigned HPF_Direction: 1;
    unsigned ExternalClock: 1;
} __attribute__( ( __packed__ ) ) accelerometer_lis3lv02dl_REG_CTLREG3;

typedef struct {
    t_u16 who_am_i;
    t_u16 status;
    t_u16 ctrl_reg1;
    t_u16 ctrl_reg2;
    t_u16 ctrl_reg3;
    t_u16 outx_l;
    t_u16 outx_h;
    t_u16 outy_l;
    t_u16 outy_h;
    t_u16 outz_l;
    t_u16 outz_h;
} __attribute__( ( __packed__ ) )t_accelerometer_lis3lv02dlr_registers;

//} Includes
//{ Structures/ Enums required by ttc_accelerometer_types.h *************************

typedef struct {  // lis3lv02dl specific configuration data of single ACCELEROMETER device

    accelerometer_lis3lv02dl_REG_CTLREG1  CTRL_REG1;
    accelerometer_lis3lv02dl_REG_CTLREG2  CTRL_REG2;
    accelerometer_lis3lv02dl_REG_CTLREG3  CTRL_REG3;
    t_accelerometer_lis3lv02dlr_registers t_register;
    t_u16 address;

} __attribute__( ( __packed__ ) ) t_accelerometer_lis3lv02dl_config;

// t_ttc_accelerometer_architecture is required by ttc_accelerometer_types.h
#define t_ttc_accelerometer_architecture t_accelerometer_lis3lv02dl_config
//#define t_ttc_accelerometer_architecture void

//} Structures/ Enums


#endif //ACCELEROMETER_LIS3LV02DL_TYPES_H
