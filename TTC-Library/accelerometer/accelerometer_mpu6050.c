/** { accelerometer_mpu6050.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for accelerometer devices on mpu6050 architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20141020 08:58:47 UTC
 *
 *  Note: See ttc_accelerometer.h for description of mpu6050 independent ACCELEROMETER implementation.
 *
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "accelerometer_mpu6050.h".
//
#include "accelerometer_mpu6050.h"
#include "../ttc_gpio.h"
#include "../ttc_i2c.h"
#include "../ttc_task.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

e_ttc_accelerometer_errorcode accelerometer_mpu6050_deinit(t_ttc_accelerometer_config* Config) {
    Assert_ACCELEROMETER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    return (e_ttc_accelerometer_errorcode) 0;
}
e_ttc_accelerometer_errorcode accelerometer_mpu6050_get_features (t_ttc_accelerometer_config* Config) {
    Assert_ACCELEROMETER (Config, ttc_assert_origin_auto); // pointers must not be NULL



    return (e_ttc_accelerometer_errorcode) 0;
}
e_ttc_accelerometer_errorcode accelerometer_mpu6050_init (t_ttc_accelerometer_config* Config) {
    Assert_ACCELEROMETER (Config, ttc_assert_origin_auto); // pointers must not be NULL


    t_u8 LogicalIndex = Config->LogicalIndex;
    Assert_ACCELEROMETER (LogicalIndex > 0, ttc_assert_origin_auto); // logical index starts at 1
    Assert_ACCELEROMETER (Config != NULL, ttc_assert_origin_auto);
    if (LogicalIndex > TTC_ACCELEROMETER_AMOUNT)
        return ec_accelerometer_DeviceNotFound;

    ttc_i2c_master_reset_bus(Config->LogicalIndex_Interface);

    t_ttc_accelerometer_architecture * Architecture = &Config->LowLevelConfig;

    Architecture->Registers.who_am_i = 0x75;
    Architecture->Registers.pwr_mgmt_1 = 0x6B;

    Architecture->Registers.outx_l = 0x3C;
    Architecture->Registers.outx_h = 0x3B;

    Architecture->Registers.outy_l = 0x3E;
    Architecture->Registers.outy_h = 0x3D;

    Architecture->Registers.outz_l = 0x40;
    Architecture->Registers.outz_h = 0x3F;


    // Reset MPU6050
    e_ttc_i2c_errorcode ErrorI2C ;
    t_u8 Data = 0x80;
    ErrorI2C = ttc_i2c_master_write_register(Config->LogicalIndex_Interface,
                                             Architecture->SlaveAddress,
                                             Architecture->Registers.pwr_mgmt_1,
                                             E_ttc_i2c_address_type_Default,
                                             1,
                                             &Data
                                            );
    if (ErrorI2C)
        return ec_accelerometer_CommunicationErrorI2C;
    ttc_task_msleep(50);

    t_u8 StatusRegister = 0;
    ErrorI2C = ttc_i2c_master_read_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, Architecture->Registers.who_am_i, E_ttc_i2c_address_type_Default, 1, &StatusRegister);
    ttc_task_msleep(50);
    if (ErrorI2C)
        return ec_accelerometer_CommunicationErrorI2C;

    if (StatusRegister != 104)
        return ec_accelerometer_InvalidConfiguration;

   //Wake up MPU6050
    do {
        Data = 0x00;
        ErrorI2C = ttc_i2c_master_write_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, Architecture->Registers.pwr_mgmt_1, E_ttc_i2c_address_type_Default, 1, &Data);
        if (ErrorI2C)
            return ErrorI2C;
        ttc_task_msleep(50);


        ErrorI2C = ttc_i2c_master_read_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, Architecture->Registers.pwr_mgmt_1, E_ttc_i2c_address_type_Default, 1, &StatusRegister);
        if (ErrorI2C)
            return ErrorI2C;
        ttc_task_msleep(50);

    } while (StatusRegister != 0);

    //Set Output Rate
    Data = 0x00;
    ErrorI2C = ttc_i2c_master_write_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, 0x19, E_ttc_i2c_address_type_Default, 1, &Data);
    if (ErrorI2C)
        return ErrorI2C;
    ttc_task_msleep(50);

    //Set DLPF Mode 6 - Bandwidth 5 Hz
    Data = 0b00000110;
    ErrorI2C = ttc_i2c_master_write_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, 0x1A, E_ttc_i2c_address_type_Default, 1, &Data);
    if (ErrorI2C)
        return ErrorI2C;

    ErrorI2C = ttc_i2c_master_read_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, 0x1A, E_ttc_i2c_address_type_Default, 1, &StatusRegister);
    if (ErrorI2C)
        return ErrorI2C;

    //Set Accel config
    Data = 0b00000000;
    ErrorI2C = ttc_i2c_master_write_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, 0x1C, E_ttc_i2c_address_type_Default, 1, &Data);
    if (ErrorI2C)
        return ErrorI2C;

    ErrorI2C = ttc_i2c_master_read_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, 0x1C, E_ttc_i2c_address_type_Default, 1, &StatusRegister);
    if (ErrorI2C)
        return ErrorI2C;

    return (e_ttc_accelerometer_errorcode) 0;
}
e_ttc_accelerometer_errorcode accelerometer_mpu6050_load_defaults (t_ttc_accelerometer_config* Config) {
    Assert_ACCELEROMETER (Config, ttc_assert_origin_auto); // pointers must not be NULL

    Config->Architecture = ta_accelerometer_mpu6050_i2c;
    Config->LowLevelConfig.SlaveAddress = 0x68;

    return (e_ttc_accelerometer_errorcode) 0;
}
void                          accelerometer_mpu6050_prepare () {



}
void                          accelerometer_mpu6050_reset (t_ttc_accelerometer_config* Config) {
    Assert_ACCELEROMETER (Config, ttc_assert_origin_auto); // pointers must not be NULL

    accelerometer_mpu6050_load_defaults (Config);

}
t_ttc_accelerometer_measures* accelerometer_mpu6050_read_measures (t_ttc_accelerometer_config* Config) {
    Assert_ACCELEROMETER (Config, ttc_assert_origin_auto); // pointers must not be NULL

    t_u8 LogicalIndex = Config->LogicalIndex;
    Assert_ACCELEROMETER (LogicalIndex > 0, ttc_assert_origin_auto); // logical index starts at 1
    Assert_ACCELEROMETER (Config != NULL, ttc_assert_origin_auto);

    t_ttc_accelerometer_architecture * Architecture = &Config->LowLevelConfig;

    t_u8 StatusRegister = 0;
    if ( ttc_i2c_master_read_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, Architecture->Registers.who_am_i, E_ttc_i2c_address_type_Default, 1, &StatusRegister) )
        return NULL;

    Assert (StatusRegister == 104, ttc_assert_origin_auto);

    t_s16 AccelerationX;
    t_s16 AccelerationY;
    t_s16 AccelerationZ;


    if ( ttc_i2c_master_read_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, Architecture->Registers.outx_l, E_ttc_i2c_address_type_Default, 1, (t_u8*) &AccelerationX ) )
        return NULL;
    //ttc_task_msleep(50);

    if ( ttc_i2c_master_read_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, Architecture->Registers.outx_h, E_ttc_i2c_address_type_Default, 1, ( (t_u8*) &AccelerationX ) + 1) )
        return NULL;
    //ttc_task_msleep(50);

    if ( ttc_i2c_master_read_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, Architecture->Registers.outy_l, E_ttc_i2c_address_type_Default, 1, (t_u8*) &AccelerationY ) )
        return NULL;
    //ttc_task_msleep(50);

    if ( ttc_i2c_master_read_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, Architecture->Registers.outy_h, E_ttc_i2c_address_type_Default, 1, ( (t_u8*) &AccelerationY ) + 1) )
        return NULL;
    //ttc_task_msleep(50);

    if ( ttc_i2c_master_read_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, Architecture->Registers.outz_l, E_ttc_i2c_address_type_Default, 1, (t_u8*) &AccelerationZ ) )
        return NULL;
    //ttc_task_msleep(50);

    if ( ttc_i2c_master_read_register (Config->LogicalIndex_Interface, Architecture->SlaveAddress, Architecture->Registers.outz_h, E_ttc_i2c_address_type_Default, 1, ( (t_u8*) &AccelerationZ ) + 1) )
        return NULL;
    //ttc_task_msleep(50);


    Config->Measures.AccelerationX = AccelerationX;
    Config->Measures.AccelerationY = AccelerationY;
    Config->Measures.AccelerationZ = AccelerationZ;

    return &Config->Measures;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
