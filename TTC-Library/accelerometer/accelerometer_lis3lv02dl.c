/** { accelerometer_DEPRECATED_lis3lv02dl.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for accelerometer devices on lis3lv02dl architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20141020 08:59:34 UTC
 *
 *  Note: See ttc_accelerometer.h for description of lis3lv02dl independent ACCELEROMETER implementation.
 *
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "accelerometer_lis3lv02dl.h".
//
#include "accelerometer_lis3lv02dl.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

e_ttc_accelerometer_errorcode accelerometer_lis3lv02dl_deinit( t_ttc_accelerometer_config* Config ) {
    Assert_ACCELEROMETER_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    return ( e_ttc_accelerometer_errorcode ) 0;
}
e_ttc_accelerometer_errorcode accelerometer_lis3lv02dl_get_features( t_ttc_accelerometer_config* Config ) {
    Assert_ACCELEROMETER_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL



    return ( e_ttc_accelerometer_errorcode ) 0;
}
e_ttc_accelerometer_errorcode accelerometer_lis3lv02dl_init( t_ttc_accelerometer_config* Config ) {
    Assert_ACCELEROMETER_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL


    t_u8 LogicalIndex = Config->LogicalIndex;
    Assert_ACCELEROMETER( LogicalIndex > 0, ttc_assert_origin_auto ); // logical index starts at 1
    Assert_ACCELEROMETER_Writable( Config, ttc_assert_origin_auto );
    if ( LogicalIndex > TTC_ACCELEROMETER_AMOUNT )
    { return ec_accelerometer_DeviceNotFound; }


    t_ttc_accelerometer_architecture* Acc_Arch = &Config->LowLevelConfig;

    Acc_Arch->CTRL_REG1.DevicePowermode = 3;
    Acc_Arch->CTRL_REG1.FrequencyRate = 0;
    Acc_Arch->CTRL_REG1.Selftest = 0;
    Acc_Arch->CTRL_REG1.XEnable = 1;
    Acc_Arch->CTRL_REG1.YEnable = 1;
    Acc_Arch->CTRL_REG1.ZEnable = 1;

    Acc_Arch->CTRL_REG2.Fullscale = 0;
    Acc_Arch->CTRL_REG2.BlockDataUpdate = 1;
    Acc_Arch->CTRL_REG2.BigEndian = 0;


    Acc_Arch->t_register.who_am_i = 0x0F;
    Acc_Arch->t_register.status = 0x27;
    Acc_Arch->t_register.ctrl_reg1 = 0x20;
    Acc_Arch->t_register.ctrl_reg2 = 0x21;
    Acc_Arch->t_register.ctrl_reg3 = 0x22;

    Acc_Arch->t_register.outx_l = 0x28;
    Acc_Arch->t_register.outx_h = 0x29;

    Acc_Arch->t_register.outy_l = 0x2A;
    Acc_Arch->t_register.outy_h = 0x2B;

    Acc_Arch->t_register.outz_l = 0x2C;
    Acc_Arch->t_register.outz_h = 0x2D;


    t_u8 StatusRegister = 0;
    e_ttc_i2c_errorcode Error = ttc_i2c_master_read_register( Config->LogicalIndex_Interface,
                                                              Acc_Arch->address,
                                                              Acc_Arch->t_register.who_am_i,
                                                              E_ttc_i2c_address_type_MSB_First_8Bit,
                                                              1,
                                                              &StatusRegister
                                                            );

    if ( Error )
    { return ec_accelerometer_ERROR; }

    if ( StatusRegister != 0x3A )
    { return ec_accelerometer_DeviceNotFound; }

    t_u8* r1 = ( t_u8* ) &Acc_Arch->CTRL_REG1;
    t_u8* r2 = ( t_u8* ) &Acc_Arch->CTRL_REG2;
    t_u8* r3 = ( t_u8* ) &Acc_Arch->CTRL_REG3;

    ttc_i2c_master_write_register( Config->LogicalIndex_Interface,
                                   Acc_Arch->address,
                                   Acc_Arch->t_register.ctrl_reg1,
                                   E_ttc_i2c_address_type_MSB_First_8Bit,
                                   1,
                                   r1
                                 );
    Assert( !Error, ttc_assert_origin_auto );

    ttc_i2c_master_write_register( Config->LogicalIndex_Interface,
                                   Acc_Arch->address,
                                   Acc_Arch->t_register.ctrl_reg2,
                                   E_ttc_i2c_address_type_MSB_First_8Bit,
                                   1,
                                   r2
                                 );
    Assert( !Error, ttc_assert_origin_auto );

    ttc_i2c_master_write_register( Config->LogicalIndex_Interface,
                                   Acc_Arch->address,
                                   Acc_Arch->t_register.ctrl_reg3,
                                   E_ttc_i2c_address_type_MSB_First_8Bit,
                                   1,
                                   r3
                                 );
    Assert( !Error, ttc_assert_origin_auto );

    if ( 1 ) { // compare written values
        t_u8 Compare1 = 0;
        t_u8 Compare2 = 0;
        t_u8 Compare3 = 0;

        Error = ttc_i2c_master_read_register( Config->LogicalIndex_Interface,
                                              Acc_Arch->address,
                                              Acc_Arch->t_register.ctrl_reg1,
                                              E_ttc_i2c_address_type_MSB_First_8Bit,
                                              1,
                                              &Compare1
                                            );
        Assert( !Error, ttc_assert_origin_auto );

        Error = ttc_i2c_master_read_register( Config->LogicalIndex_Interface,
                                              Acc_Arch->address,
                                              Acc_Arch->t_register.ctrl_reg2,
                                              E_ttc_i2c_address_type_MSB_First_8Bit,
                                              1,
                                              &Compare2
                                            );
        Assert( !Error, ttc_assert_origin_auto );

        Error = ttc_i2c_master_read_register( Config->LogicalIndex_Interface,
                                              Acc_Arch->address,
                                              Acc_Arch->t_register.ctrl_reg3,
                                              E_ttc_i2c_address_type_MSB_First_8Bit,
                                              1,
                                              &Compare3
                                            );
        Assert( !Error, ttc_assert_origin_auto );


        Assert( *r1 == Compare1, ttc_assert_origin_auto );
        Assert( *r2 == Compare2, ttc_assert_origin_auto );
        Assert( *r3 == Compare3, ttc_assert_origin_auto );
    }


    return ( e_ttc_accelerometer_errorcode ) 0;
}

e_ttc_accelerometer_errorcode accelerometer_lis3lv02dl_load_defaults( t_ttc_accelerometer_config* Config ) {
    Assert_ACCELEROMETER_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    Config->Architecture = ta_accelerometer_lis3lv02dl;
    Config->LowLevelConfig.address = 29;

    /*
        Config->LowLevelConfig.CTRL_REG1.XEnable=1;
        Config->LowLevelConfig.CTRL_REG1.YEnable=1;
        Config->LowLevelConfig.CTRL_REG1.ZEnable =1;
        Config->LowLevelConfig.CTRL_REG1.Selftest=1;
        Config->LowLevelConfig.CTRL_REG1.FrequencyRate =2;
        Config->LowLevelConfig.CTRL_REG1.DevicePowermode=2;

        Config->LowLevelConfig.CTRL_REG2.DataAligment=1;
        Config->LowLevelConfig.CTRL_REG2.SPIMode=1;
        Config->LowLevelConfig.CTRL_REG2.DataReadyEnable=1;
        Config->LowLevelConfig.CTRL_REG2.InterruptEnable=1;
        Config->LowLevelConfig.CTRL_REG2.Boot               = 1;
        Config->LowLevelConfig.CTRL_REG2.BigEndian          = 1;
        Config->LowLevelConfig.CTRL_REG2.BlockDataUpdate    = 1;
        Config->LowLevelConfig.CTRL_REG2.Fullscale          = 1;

        Config->LowLevelConfig.CTRL_REG3.HPF_CutoffFreq     = 2;
        Config->LowLevelConfig.CTRL_REG3.reserved0          = 2;
        Config->LowLevelConfig.CTRL_REG3.FilterDataSelect   = 1;
        Config->LowLevelConfig.CTRL_REG3.HPF_FreeFall       = 1;
        Config->LowLevelConfig.CTRL_REG3.HPF_Direction      = 1;
        Config->LowLevelConfig.CTRL_REG3.ExternalClock      = 1;

        */
    return ( e_ttc_accelerometer_errorcode ) 0;
}

void accelerometer_lis3lv02dl_prepare() {

}

void accelerometer_lis3lv02dl_reset( t_ttc_accelerometer_config* Config ) {
    Assert_ACCELEROMETER_Writable( Config, ttc_assert_origin_auto ); // pointers must not be NULL

    accelerometer_lis3lv02dl_load_defaults( Config );

}

t_ttc_accelerometer_measures* accelerometer_lis3lv02dl_read_measures( t_ttc_accelerometer_config* Config ) {

    t_u8 LogicalIndex = Config->LogicalIndex;
    Assert_ACCELEROMETER( LogicalIndex > 0, ttc_assert_origin_auto ); // logical index starts at 1
    Assert_ACCELEROMETER_Writable( Config, ttc_assert_origin_auto );


    t_ttc_accelerometer_architecture* ACCELEROMETER_Arch = &Config->LowLevelConfig;

    t_u8 StatusRegister = 0;
    ttc_i2c_master_read_register( Config->LogicalIndex_Interface,
                                  ACCELEROMETER_Arch->address,
                                  ACCELEROMETER_Arch->t_register.who_am_i,
                                  E_ttc_i2c_address_type_MSB_First_8Bit,
                                  1,
                                  &StatusRegister
                                );
    Assert( StatusRegister == 0x3A, ttc_assert_origin_auto );

    t_s16 AccelerationX;
    t_s16 AccelerationY;
    t_s16 AccelerationZ;

    ttc_i2c_master_read_register( Config->LogicalIndex_Interface,
                                  ACCELEROMETER_Arch->address,
                                  ACCELEROMETER_Arch->t_register.outx_l,
                                  E_ttc_i2c_address_type_MSB_First_8Bit,
                                  1,
                                  ( t_u8* ) &AccelerationX
                                );
    ttc_i2c_master_read_register( Config->LogicalIndex_Interface,
                                  ACCELEROMETER_Arch->address,
                                  ACCELEROMETER_Arch->t_register.outx_h,
                                  E_ttc_i2c_address_type_MSB_First_8Bit,
                                  1,
                                  ( ( t_u8* ) &AccelerationX ) + 1
                                );

    ttc_i2c_master_read_register( Config->LogicalIndex_Interface,
                                  ACCELEROMETER_Arch->address,
                                  ACCELEROMETER_Arch->t_register.outy_l,
                                  E_ttc_i2c_address_type_MSB_First_8Bit,
                                  1,
                                  ( t_u8* ) &AccelerationY
                                );
    ttc_i2c_master_read_register( Config->LogicalIndex_Interface,
                                  ACCELEROMETER_Arch->address,
                                  ACCELEROMETER_Arch->t_register.outy_h,
                                  E_ttc_i2c_address_type_MSB_First_8Bit,
                                  1,
                                  ( ( t_u8* ) &AccelerationY ) + 1
                                );

    ttc_i2c_master_read_register( Config->LogicalIndex_Interface,
                                  ACCELEROMETER_Arch->address,
                                  ACCELEROMETER_Arch->t_register.outz_l,
                                  E_ttc_i2c_address_type_MSB_First_8Bit,
                                  1,
                                  ( t_u8* ) &AccelerationZ
                                );
    ttc_i2c_master_read_register( Config->LogicalIndex_Interface,
                                  ACCELEROMETER_Arch->address,
                                  ACCELEROMETER_Arch->t_register.outz_h,
                                  E_ttc_i2c_address_type_MSB_First_8Bit,
                                  1,
                                  ( ( t_u8* ) &AccelerationZ ) + 1
                                );


    Config->Measures.AccelerationX = AccelerationX;
    Config->Measures.AccelerationY = AccelerationY;
    Config->Measures.AccelerationZ = AccelerationZ;

    return &Config->Measures;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
