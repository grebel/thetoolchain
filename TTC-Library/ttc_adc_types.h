#ifndef TTC_ADC_TYPES_H
#define TTC_ADC_TYPES_H


/** { ttc_adc_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for ADC device.
 *  Structures, Enums and Defines being required by both, high- and low-level ADC.
 *
 *  Created from template ttc_device_types.h revision 22 at 20140423 11:04:14 UTC
 *
 *  Authors: Gregor Rebel, Victor Fuentes
 *
}*/

//{ Includes *************************************************************

#include "ttc_basic_types.h"
#include "ttc_gpio_types.h"
#ifdef EXTENSION_adc_stm32f1xx
    #include "adc/adc_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_adc_stm32l1xx
    #include "adc/adc_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_adc_stm32w1xx
    #include "adc/adc_stm32w1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_adc_stm32f30x
    #include "adc/adc_stm32f30x_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_adc_stm32l0xx
    #include "adc/adc_stm32l0xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

/* Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_ADC 0  # disable assert handling for ADC devices
 *
 */
#ifndef TTC_ASSERT_ADC    // any previous definition set (Makefile)?
    #define TTC_ASSERT_ADC 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_ADC == 1)  // use Assert()s in ADC code (somewhat slower but alot easier to debug)
    #define Assert_ADC(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in ADC code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_ADC(Condition, ErrorCode)
#endif

/** Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_adc__config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_adc_architecture
    #warning Missing low-level definition for ttc_ADC_architecture_t (using default)
    #define t_ttc_adc_architecture void
#endif

//}Static Configuration
//{ Enums/ Structures ****************************************************

//{ Check if ADCs are configured correctly
#ifndef TTC_ADC_MAX_AMOUNT
    #  error Missing definition of TTC_ADC_MAX_AMOUNT. ADC low level driver must define it as maximum allowed amount of analog inputs on current microcontroller.
#endif

// static adc configuration has been placed in extra header file to keep this file readable
#include "ttc_adc_configuration.h"
//}

typedef enum {    // ttc_ADC_errorcode_e     return codes of ADC devices
    ec_adc_OK = 0,

    // other warnings go here..
    ec_adc_TimeOut,                // given timeout has exceeded

    ec_adc_ERROR,                  // general failure
    ec_adc_NULL,                   // NULL pointer not accepted
    ec_adc_DeviceNotFound,         // corresponding device could not be found
    ec_adc_InvalidConfiguration,   // sanity check of device configuration failed
    ec_adc_InvalidImplementation,  // your code does not behave as expected
    ec_adc_FeatureNotSupported,    // required feature (e.g. dma transfer is not supported on current architecture)

    // other failures go here..

    ec_adc_unknown                // no valid errorcodes past this entry
} e_ttc_adc_errorcode;

typedef enum {    // e_ttc_adc_architecture  types of architectures supported by ADC driver
    ta_adc_None,           // no architecture selected


    ta_adc_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
    ta_adc_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
    ta_adc_stm32w1xx, // automatically added by ./create_DeviceDriver.pl
    ta_adc_stm32f30x, // automatically added by ./create_DeviceDriver.pl
    ta_adc_stm32l0xx, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_adc_unknown        // architecture not supported
} e_ttc_adc_architecture;


typedef struct {
    unsigned Initialized       : 1; // =1: interface has been initialized and is usable
    unsigned UseDMA            : 1; // =1: use Direct Memory Access to copy digital value to destination
    unsigned SingleShot        : 1; // =1: run in single shot mode (stop adc after each conversion)

    unsigned reserved          : 5;
} __attribute__( ( __packed__ ) ) t_ttc_adc_config_bits;
typedef struct s_ttc_adc_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_ADC_init()
    //       and after ttc_ADC_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    e_ttc_adc_architecture Architecture; // type of architecture used for current ADC device
    t_u8  LogicalIndex;        // Lookup index for ttc_adc_configs[] (1 = TTC_ADC1, ...).

    // low-level configuration (structure defined by low-level driver)
    t_ttc_adc_architecture LowLevelConfig;

    union  { // generic configuration bits common for all low-level Drivers
        t_u8 All;
        t_ttc_adc_config_bits Bits;
    } Flags;

    // input pin being used to read analog signal
    e_ttc_gpio_pin InputPin;

    //Channel
    t_u16 Channel;

} __attribute__( ( __packed__ ) ) t_ttc_adc_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_ADC_TYPES_H

