#ifndef TCPIP_UIP_TYPES_H
#define TCPIP_UIP_TYPES_H

/** { tcpip_uip.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *                     
 *  Low-Level datatypes for TCPIP devices on uip architectures.
 *  Structures, Enums and Defines being required by ttc_tcpip_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150306 13:00:16 UTC
 *
 *  Note: See ttc_tcpip.h for description of architecture independent TCPIP implementation.
 * 
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_tcpip_types.h *************************

typedef struct { // register description (adapt according to uip registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} t_tcpip_register;

typedef struct {  // uip specific configuration data of single TCPIP device
  t_tcpip_register* BaseRegister;       // base address of TCPIP device registers
} __attribute__((__packed__)) t_tcpip_uip_config;

// t_ttc_tcpip_architecture is required by ttc_tcpip_types.h
#define t_ttc_tcpip_architecture t_tcpip_uip_config

// Datatypes required by uIP
typedef struct s_ttc_tcpip_uip_httpd_state {
  t_u8 state;
  t_u16 count;
  char *dataptr;
  char *script;
} t_tut_httpd_state;
typedef t_tut_httpd_state t_uip_tcp_appstate;

//} Structures/ Enums


#endif //TCPIP_UIP_TYPES_H
