/** { tcpip_uip.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for tcpip devices on uip architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150309 09:09:54 UTC
 *
 *  Note: See ttc_tcpip.h for description of uip independent TCPIP implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "tcpip_uip.h".
//
#include "tcpip_uip.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

t_ttc_tcpip_config* tcpip_uip_get_features(t_ttc_tcpip_config* Config) {
    Assert_TCPIP(Config, ttc_assert_origin_auto); // pointers must not be NULL

    static t_ttc_tcpip_config Features;
    if (!Features.LogicalIndex) { // called first time: initialize data
        
#     warning Function tcpip_uip_get_features() is empty.

    }
    Features.LogicalIndex = Config->LogicalIndex;
    
    return (t_ttc_tcpip_config*) 0;
}
e_ttc_tcpip_errorcode tcpip_uip_deinit(t_ttc_tcpip_config* Config) {
    Assert_TCPIP(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function tcpip_uip_deinit() is empty.
    
    return (e_ttc_tcpip_errorcode) 0;
}
e_ttc_tcpip_errorcode tcpip_uip_init(t_ttc_tcpip_config* Config) {
    Assert_TCPIP(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function tcpip_uip_init() is empty.
    
    return (e_ttc_tcpip_errorcode) 0;
}
e_ttc_tcpip_errorcode tcpip_uip_load_defaults(t_ttc_tcpip_config* Config) {
    Assert_TCPIP(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function tcpip_uip_load_defaults() is empty.
    
    return (e_ttc_tcpip_errorcode) 0;
}
void tcpip_uip_prepare() {


#warning Function tcpip_uip_prepare() is empty.
    

}
void tcpip_uip_reset(t_ttc_tcpip_config* Config) {
    Assert_TCPIP(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function tcpip_uip_reset() is empty.
    

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

void _tcpip_uip_app_call() {

}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
