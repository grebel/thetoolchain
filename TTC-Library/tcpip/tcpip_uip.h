#ifndef TCPIP_UIP_H
#define TCPIP_UIP_H

/** { tcpip_uip.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for tcpip devices on uip architectures.
 *  Structures, Enums and Defines being required by high-level tcpip and application.
 *
 *  Created from template device_architecture.h revision 22 at 20150309 09:09:54 UTC
 *
 *  Note: See ttc_tcpip.h for description of uip independent TCPIP implementation.
 *  
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_TCPIP_UIP
//
// Implementation status of low-level driver support for tcpip devices on uip
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_TCPIP_UIP '?'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_TCPIP_UIP == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_TCPIP_UIP to one from '-', 'o', '+' 
#endif

//}EXTENSION_STATUS_TTC_TCPIP_UIP

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "tcpip_uip.c"
//
#include "tcpip_uip_types.h"
#include "../ttc_tcpip_types.h"
#include "uip-conf.h"
#include "../../additionals/400_network_uip_ste101p/Ethernet/uip/uip.h"
#include "../../additionals/400_network_uip_ste101p/Ethernet/uip/uip_arp.h"
//#include "../../additionals/400_network_uip_ste101p/Ethernet/uip/httpd/httpd.h"
#include "../../additionals/400_network_uip_ste101p/Ethernet/uip/timer.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_tcpip_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_tcpip_foo
//
#define ttc_driver_tcpip_deinit(Config) tcpip_uip_deinit(Config)
#define ttc_driver_tcpip_get_features(Config) tcpip_uip_get_features(Config)
#define ttc_driver_tcpip_init(Config) tcpip_uip_init(Config)
#define ttc_driver_tcpip_load_defaults(Config) tcpip_uip_load_defaults(Config)
#define ttc_driver_tcpip_prepare() tcpip_uip_prepare()
#define ttc_driver_tcpip_reset(Config) tcpip_uip_reset(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_tcpip.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_tcpip.h for details.
 *
 * You normally should not need to add functions here. 
 * Add your private functions in the private section below. 
 */


/** shutdown single TCPIP unit device
 * @param Config        pointer to struct t_ttc_tcpip_config (must have valid value for PhysicalIndex)
 * @return              == 0: TCPIP has been shutdown successfully; != 0: error-code
 */
e_ttc_tcpip_errorcode tcpip_uip_deinit(t_ttc_tcpip_config* Config);


/** fills out given Config with maximum valid values for indexed TCPIP
 * @param Config  = pointer to struct t_ttc_tcpip_config (must have valid value for PhysicalIndex)
 * @return        pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_tcpip_config* tcpip_uip_get_features(t_ttc_tcpip_config* Config);


/** initializes single TCPIP unit for operation
 * @param Config        pointer to struct t_ttc_tcpip_config (must have valid value for PhysicalIndex)
 * @return              == 0: TCPIP has been initialized successfully; != 0: error-code
 */
e_ttc_tcpip_errorcode tcpip_uip_init(t_ttc_tcpip_config* Config);


/** loads configuration of indexed TCPIP unit with default values
 * @param Config        pointer to struct t_ttc_tcpip_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_tcpip_errorcode tcpip_uip_load_defaults(t_ttc_tcpip_config* Config);


/** Prepares tcpip Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void tcpip_uip_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_tcpip_config (must have valid value for PhysicalIndex)
 */
void tcpip_uip_reset(t_ttc_tcpip_config* Config);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _tcpip_uip_foo(t_ttc_tcpip_config* Config)

/** Callback function for uIP stack.
 *
 * This function gets called whenever the stack is idle or has received data.
 */
void _tcpip_uip_app_call();
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //TCPIP_UIP_H