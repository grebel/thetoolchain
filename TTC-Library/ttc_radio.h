#ifndef TTC_RADIO_H
#define TTC_RADIO_H
/** { ttc_radio.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for RADIO devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_RADIO(tc_radio_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_radio_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_radio_init(LogicalIndex);
 *  4) use:         ttc_radio_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Created from template ttc_device.h revision 28 at 20140514 04:32:30 UTC
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of ttc_queue (Do not delete this line!)
 *
 *  ttc_radio provides a universal digital radio transceiver + some additional features:
 *     - 802.15.4a (2011) compliant packet headers (any packet format being supported by ttc_packet).
 *     - Transmit/ receive generic payload.
 *     - Automatic fast packet buffer management via ttc_heap_pool.
 *     - Transmission of raw packets.
 *     - Transmission of generic binary buffers with automatic packet generation.
 *     - Interrupt driven packet receival.
 *     - Virtual channels for simplified, generic channel switching.
 *     - Transparemt, individual or combined switching of RX- and TX-channel.
 *     - Generic maintenance function for application specific transceiver monitoring intervalls.
 *     - Exclusive radio access for non-interrupted transactions.
 *     - Full ranging support for selected transceivers (e.g. DecaWave DW1000).
 *
 *
 * Shared Radio Access
 *
 *     Different applications may require access to ttc_radio in parallel.
 *     These can be implemented as multiple tasks or different statemachines.
 *     The problems with multiple applications using same radio are
 *     - protocol layers might steal foreign packets (those from other applications) because it cannot read them
 *     - once removed from ListRX, it is difficult to reinsert it to the list (interrupt driven communication is ongoing)
 *     - processing foreign packets is wasting cpu-time
 *     - received packets of unhandled type can block ListRX
 *
 *     ttc_radio can filter received packets by their socket identifier byte.
 *     For this, TTC_PACKET_SOCKET_BYTE=1 is defined in makefile.500_ttc_radio to add an
 *     extra socket identifier byte in front of payload area of every t_ttc_packet.
 *     This extra byte ensures that
 *     - each application statemachine is not feed with foreign packets
 *     - no application statemachine can steal foreign packets
 *     - all packets of unhandled types can be flushed from ListRX without loosing any packets of handled types
 *
 *     How to use multiple applications with ttc_radio
 *
 *     The most reliable way is to establish your own unique and fixed socket identifier:
 *     1) Add a socket identifier entry to e_ttc_packet_pattern.
 *     2) Use this socket identifier for every ttc_radio_*() call
 *
 *     For local applications, you may also use dynamically assigned socket identifiers.
 *     1) e_ttc_packet_pattern SocketID = ttc_radio_socket_new();
 *     2) Use SocketID for every ttc_radio_*() call
 *
 *
 * Exclusive Radio Access
 *
 *     A radio user is a task or statemachine that performs communication transactions.
 *     If such a transaction must not be interrupted by other users, the user may lock ttc_radio
 *     for exclusive access. For this, the user has to obtain a unique user identifier once.
 *     While locked, function calls of other users simply return an appropriate error code.
 *     The functions provided for exclusive user access are (see decriptions for more details):
 *     - t_u8 UserID = ttc_radio_user_new(...);
 *     - ttc_radio_user_lock(..., UserID)
 *     - ttc_radio_user_unlock(..., UserID)
 *
 *
 * Ranging
 *
 *     802.15.4a compliant Ultra Wide Band (UWB) radio transceivers provide two basic features to be used for rangine:
 *     - exact timestamps for transmited and receiveed packets
 *     - exactly delayed packet transmission
 *
 *     These features allow to calculate the distance between two radios with a precision of around 20cm.
 *     The application has to implement the range measuring around these timestamps.
 *
 *     Two basic types of range measuring exists:
 *     1) Time of Flight (TOF)
 *        This is the most simple way of range measurement. It requires two or more packets to be exchanged
 *        between node A and node B. For two packets, it goes as follows:
 *        a) Node A sends a packet with a ranging request to node B
 *        b) Node B answers after an exact time delay
 *        c) Node A calculates the difference between transmission time of first packet and receive time of
 *           second packet by taking into account the known delay time of node B.
 *        ttc_radio_ranging_request() provides an example implementation for a TOF measurement with two messages.
 *
 *     2) Time Differential of Arrival (TDOA)
 *        This technique requires one sender node S and multiple receiver nodes A1, .. An. The receiver nodes are often called
 *        anchor nodes. Several anchor nodes receive the packet from S and note its receive time.
 *        The anchor nodes then communicate with each other to exchange the receive times. The sender node then can
 *        be located by use of the differences between receive times.
 *
 *
 *     radio_common.c implements generic elements of ranging and localization.
 *     radio_common_ranging_reply_isr() is called from radio_common_push_list_rx_isr() for every received packet
 *     with set flag Packet->Meta.RangingMessage.
 *     radio_common_ranging_reply_isr() then inspects first payload byte:
 *     == rcrt_Request_Ranging_SSTOFCR:
 *        A ranging reply message is transmitted immediately or after predefined Config->Init.Delay_RangingReply.
 *     == rcrt_Request_Localization_SSTOFCR:
 *        A localization reply message is transmitted immediately or after predefined Delay_RangingReply.
 *        The reply message also transports the current node location Config->Init.MyLocation to provide all
 *        data required by the requesting node to calculate its own position.
 *
 *     The current node can always reply to incoming ranging requests.
 *     Replies to localization requests require that Config->Init.MyLocation points to a 3D vector with
 *     valid coordinates of local node. These coordinates can be stored in any compatible vector.
 *     If stored in a ttc_slam instance, the coordinates can be dynamically mapped using ttc_rtls.
 *     See example_ttc_rtls_crtof_simple_2d.c for details.
 *
 *
 * Debugging ttc_radio
 *
 *   + Received Packets
 *     Every packet received by low-level radio_* driver must be given to radio_common_push_list_rx_isr().
 *     => Place a breakpoint inside to see every received packet (will affect timing and makes ranging impossible!):
 *        # during gdb session
 *        (gdb) b radio_common_push_list_rx_isr
 *        (gdb) c
 *
 *   + Received Ranging Packets
 *     Every received packet with ranging bit set is handed to radio_common_ranging_reply_isr()
 *     => Place a breakpoint inside to handling of ranging requests:
 *        # during gdb session
 *        (gdb) b radio_common_ranging_reply_isr
 *        (gdb) c
 *
 *   + Transmitted Packets
 *     Low-level driver should call activity functions if set in t_ttc_radio_config.Init.
 *     ttc_radio_debug_activity() is provided as a helper function during debugging.
 *     See its description below for details.
 *
 *
 * Example Usage of ttc_radio (non-ranging)
 *
    const t_u8 RadioIndex = 1;
    t_ttc_radio_config* ConfigRadio = ttc_radio_get_configuration( RadioIndex );

    // configure max tx power (optional)
    ConfigRadio->Init.LevelTX = ConfigRadio->Features->MaxTxLevel;

    // configure other fields in ConfigRadio->Init..

    // apply new configuration on radio device
    ttc_radio_init( RadioIndex );

    // create a new socket (socket id is stored in first payload byte of every packet)
    e_ttc_packet_pattern SocketID = ttc_radio_socket_new(RadioIndex, E_ttc_packet_pattern_socket_AnyOf_Application);

    // obtain our own user identifier
    t_u8 UserID = ttc_radio_user_new(example_ttc_radio_Config.RadioIndex, 0);

    // set local address (for incoming packets and to set source address in outgoing packets)
    const t_ttc_packet_address LocalAddress = {
                                              //.Address64 = { ... },    // not using 64 bit addressing in this example
                                                .Address16 = 0x1000,     // 16-bit logical address
                                                .PanID     = 0xaffe      // Personal Area Network Identification
                                              };

    ttc_radio_change_local_address(etr_Config.RadioIndex, etr_Config.UserID, &LocalAddress);


    // transmit a buffer as broadcast
    const t_u8* Hello = "Hello World!\n";
    ttc_radio_transmit_buffer( Config->RadioIndex,                   // index of radio device to use
                               Config->UserID,                       // have no user id (generic user)
                               Config->SocketID,                     // stored as first payload byte to deliver packet to correct endpoint
                               E_ttc_packet_type_802154_Data_001010, // IEEE 802.15.4 packet type (data packet with DstAddr16+DstPan16+SrcAddr16+SrcPan16)
                               Hello,                                 // data to be transmitted using one or more packets
                               ttc_string_length8(Hello, -1),        // maximum amount of bytes to send
                               NULL,                                 // DestinationID==NULL: send as broadcast
                               0,                                    // DelayNS==0:          send as soon as possible
                               FALSE                                 // Acknowledged==FALSE: do not request acknowledge handshake
                             );

    // transmit a buffer to a certain target node
    t_ttc_packet_address DestinationID;
    DestinationID.Address16 = 123;
    DestinationID.PanID     = 0xc001;

    ttc_radio_transmit_buffer( Config->RadioIndex,                   // index of radio device to use
                               Config->UserID,                       // have no user id (generic user)
                               Config->SocketID,                     // stored as first payload byte to deliver packet to correct endpoint
                               E_ttc_packet_type_802154_Data_001010, // IEEE 802.15.4 packet type (data packet with DstAddr16+DstPan16+SrcAddr16+SrcPan16)
                               Hello,                                 // data to be transmitted using one or more packets
                               ttc_string_length8(Hello, -1),        // maximum amount of bytes to send
                               &DestinationID,                       // DestinationID==NULL: send as broadcast
                               0,                                    // DelayNS==0:          send as soon as possible
                               FALSE                                 // Acknowledged==FALSE: do not request acknowledge handshake
                             );

    while (1) { // receive packets for our socket
        // initialize data of a search job that looks for packets matching our socket
        t_ttc_radio_job_socket SocketJob;
        SocketJob.Init.Initialized = 0;
        SocketJob.Init.Pattern     = Config->SocketID; // looking for packet directed at our socket

        t_ttc_packet* PacketRX = ttc_radio_packet_received_tryget( Config->RadioIndex, // logical index of radio device
                                                                   Config->UserID,     // required to check if we have access
                                                                   &SocketJob          // defines which kind of packets to look for
                                                                 );
        if (PacketRX) {
           // process packet...

           // return memory block to its pool for reuse
           ttc_radio_packet_release(PacketRX);
        }

        // other statemachines...
    }
}*/

#ifndef EXTENSION_ttc_radio
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_radio.sh
#endif

//{ Includes *************************************************************

#include "ttc_basic.h"                      // Include First! (basic datatypes and definitions)
#include "interfaces/ttc_radio_interface.h" // multi architecture support
#include "radio/radio_common.h"             // common functions for high-and low-level drivers
#include "ttc_math_types.h"                 // 3D coordinates (ranging only)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************
//
// Note: You may change these constant settings by adding corresponding COMPILE_OPTS lines to your makefile.700_extra_settings file.

//InsertDefines above (DO NOT REMOVE THIS LINE!)
#ifndef TTC_RADIO_INITIAL_AMOUNT_PACKETS
    #define TTC_RADIO_INITIAL_AMOUNT_PACKETS 5  // amount of packet buffers to allocate for packet pool (each user may increase size of this pool by calling ttc_radio_user_new() )
#endif

#ifndef TTC_RADIO_MAX_PACKET_SIZE
    #define TTC_RADIO_MAX_PACKET_SIZE 128       // maximum allowed size of binary packets (depends on your radio. cannot be increased at runtime)
#endif

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level radio only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * radio devices on all supported architectures.
 * Check radio/radio_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares radio Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_radio_prepare();
void _driver_radio_prepare();

/** Sets new source address for outgoing and incoming packets
 *
 * After changing the local address, all further packets given to ttc_radio_transmit()
 * are will use it as source address. Also the address filter in the radio device will be updated to
 * accept packets for the new address.
 *
 * Note: Calling this function will also reinitialize ttc_random with a new seed.
 *       This will provide different random numbers for differet nodes.
 * Note: Every consecutive call of this function leads to new seeds even with same LocalAddress.
 * Note: Also creates new random values for
 *       Config->SequenceNo_Beacons
 *       Config->SequenceNo_Generic
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID          ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param Config          configuration of radio device as returned by ttc_radio_get_configuration()
 * @param SourceAddress   pointer to struct containing logical 16-bit, physical 64-bit address and PAN-ID
 * @return  == 0: successfull access; ==ec_radio_IsLocked: someone else called ttc_radio_user_lock(); generic error otherwise
 */
e_ttc_radio_errorcode ttc_radio_change_local_address( t_u8 LogicalIndex, t_u8 UserID, const t_ttc_packet_address* LocalAddress );
void _driver_radio_change_local_address( t_ttc_radio_config* Config, const t_ttc_packet_address* LocalAddress );

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * @param Config = pointer to struct t_ttc_radio_config
 */
void ttc_radio_configuration_check( t_u8 LogicalIndex );

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_radio_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_RADIO_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_radio_config* ttc_radio_get_configuration( t_u8 LogicalIndex );

/** Provides empty, pre-initialized packet to be addressed and filled with data
 *
 * Packet buffers are stored in memory pools of equal sized memory blocks allocated from heap.
 * Whenever a packet shall be created for transmit or for incoming data, it is delivered by this
 * function. The function will block and go to sleep when the pool is empty. The sleeping task
 * is automatically awaken when a packet buffer is returned by calling ttc_radio_packet_release().
 *
 * @param LogicalIndex    index of radio device for which the packet shall be used
 * @param Type            !=0: returned packet will be pre-initialized via ttc_radio_packet_initialize() to this type
 * @return                !=NULL: empty, pre-initialized packet of given type
 */
t_ttc_packet* ttc_radio_packet_get_empty( t_u8 LogicalIndex, e_ttc_packet_type Type, e_ttc_packet_pattern SocketID );

/** Tries to provide empty, pre-initialized packet to be addressed and filled with data
 *
 * This function does nearly the same as ttc_radio_packet_get_empty().
 * The only difference is that it will always return immediately.
 *
 * @param LogicalIndex    index of radio device for which the packet shall be used
 * @param Type            !=0: returned packet will be pre-initialized via ttc_radio_packet_initialize() to this type
 * @param SocketID        will be copied into first payload byte (only if Type>0)
 * @return                !=NULL: empty, pre-initialized packet of given type; ==NULL: packet pool is empty (try again later)
 */
t_ttc_packet* ttc_radio_packet_get_empty_try( t_u8 LogicalIndex, e_ttc_packet_type Type, e_ttc_packet_pattern SocketID );

/** Provides empty buffer to create outgoing/ incoming data packets
 *
 * Note: This function may only be called from interrupt service routine or with disabled interrupts.
 * Note: This function will never block.
 *
 * Packet buffers are stored in memory pools of equal sized memory blocks allocated from heap.
 * Whenever a packet shall be created for transmit or for incoming data, it is delivered by this
 * function.
 *
 * @param LogicalIndex    index of radio device for which the packet shall be used
 * @param Type            !=0: returned packet will be pre-initialized via ttc_radio_packet_initialize() to this type
 * @return                !=NULL: empty, uninitialized packet, ==NULL: packet pool is empty
 */
t_ttc_packet* ttc_radio_packet_get_empty_isr( t_u8 LogicalIndex, e_ttc_packet_type Type, e_ttc_packet_pattern SocketID );

/** Fills basic header fields of given packet according to given packet type according to current radio settings.
 *
 * Initialized packet will use currently set source and destination ID and PanID.
 * Call ttc_radio_change_destination_address() before if you want to create a packet for a different target node.
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param Packet          radio packet being returned by ttc_radio_packet_get_empty() before
 * @param Type            type of packet that is to be created (-> e_ttc_radio_packet_type)
 */
void ttc_radio_packet_initialize( t_u8 LogicalIndex, t_ttc_packet* Packet, e_ttc_packet_type Type );

/** return a memory buffer of a packet for reuse
 *
 * Note: packets from other memory pools are automatically returned to their original pool.
 *
 * Example usage:
 *   MyPacket = ttc_radio_packet_release( MyPacket );
 *
 * @param Packet   packet buffer as being returned from ttc_radio_packet_get_empty()
 * @return         NULL (always returns NULL)
 */
void* ttc_radio_packet_release( t_ttc_packet* Packet );
#define ttc_radio_packet_release(Packet) radio_common_packet_release(Packet)

/** returns pointer to oldest packet in list of received packets
 *
 * Note: This function allows a fast way to inspect oldest packet without removing it from list.
 *       Returned pointer must be handled with care and not written to.
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID          ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @return                == NULL: no packet received; != NULL: oldest packet being received from radio
 */
const t_ttc_packet* ttc_radio_packet_received_peek( t_u8 LogicalIndex, t_u8 UserID );

/** returns oldest received data packet (if any)
 *
 * This function allows application to poll for received data from radio and other event sources.
 * This function will always return immediately and is usable in single- and multitasking configuration.
 *
 * Background
 *   ttc_radio automatically appends all received packets in one single linked list ListRX.
 *   ttc_radio_packet_received_tryget() can be called any time to obtain all packets in their arrival order
 *   until NULL is returned. No packets can get lost until memory pool is exhausted.
 *
 * Receiving only Packets of certain Socket Identifier (similar to classic socket implementations)
 *   If different applications share the same radio, it is important to deliver received packets to the correct application.
 *   For this, ttc_packet reserves the first payload byte as a socket identifier
 *   (TTC_PACKET_SOCKET_BYTE must be defined as 1). This identifier is used to deliver received packets to
 *   correct application.
 *   To understand, how this delivery works, we first give a short introduction to the widely used sockect concept.
 *
 *   + Socket Concept
 *     The TCP/IP packet format define socket numbers. A socket is a FIFO queue being associated with a socket number.
 *     When a packet is received, it is pushed to the corresponding FIFO with matching socket number.
 *     If no matching FIFO is found, the packet is rejected.
 *     + Disadvantages of sockets in embedded systems
 *       1) Higher RAM usage (each socket is a complete FIFO queue + extra data)
 *       2) Slower interrupt service routine (has to find corresponding FIFO for every received packet)
 *
 *   Because of its disadvantages for small microcontrollers, ttc_radio implements a slightly different
 *   scheme called Socket Jobs.
 *
 *   + Socket Job Concept
 *     Only one central receive FIFO list exists. When an application wants to check for newly received
 *     packets, it has to start a search job for a packet with matching socket identifier.
 *     + Advantages (over sockets)
 *       1) Less RAM usage (only one receive FIFO)
 *       2) Fast interrupt service routine (always pushes to same FIFO)
 *       3) Order of received packets unchanged
 *       4) Packets still received and processed when packets carry no socket identifier
 *     + Disadvantages (compard to sockets)
 *       1) Retrieving packets from receive FIFO more complex for application
 *
 *   When looking for received packets for a certain socket then a data struct has to be
 *   preallocated and its .Init part being initialized before calling this function.
 *
 * Example for receiving any type of packets:

     while (1) { // main event loop
         ...
         t_ttc_packet* PacketRX = ttc_radio_packet_received_tryget( Config, NULL);
         if (PacketRX) {
             // process received packet

             // return packet buffer to its memory pool for reuse
             ttc_radio_packet_release(PacketRX);
         }
         ...
     }


 * Example for receiving only packets of type application:

     // preparing to run main event loop
     t_ttc_radio_job_socket SocketJob;
     SocketJob.Init.Initialized = 0;
     SocketJob.Init.Pattern    = E_ttc_packet_pattern_socket_AnyOf_Application; // looking for any application type packet
     ...

     while (1) { // main event loop
         ...
         t_ttc_packet* PacketRX = ttc_radio_packet_received_tryget( Config, &SocketJob );
         if (PacketRX) {
             // process received packet

             // return packet buffer to its memory pool for reuse
             ttc_radio_packet_release(PacketRX);
         }
         ...
     }

 * Two other options are available to retrieve received packets:
 * 1) ttc_radio_packet_received_waitfor() will wait endless until packet is received (only sensefull in multitasking configuration)
 * 2) write pointer to your own interrupt service into t_ttc_radio_config.Init.function_end_tx_isr before calling ttc_radio_init()
 *
 * @param LogicalIndex  device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID        ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param SocketJob   ==NULL: look for any type of packet: !=NULL: preallocated search job with filled out SocketJob->Init part
 * @return              ==NULL: no packet received; !=NULL: packet as received from radio
 */
t_ttc_packet* ttc_radio_packet_received_tryget( t_u8 LogicalIndex, t_u8 UserID, t_ttc_radio_job_socket* SocketJob );

/** returns oldest received data packet or waits for packet to be received
 *
 * Use of this function is recommended if multitasking is enabled and an extra task is used to
 * process received packets. This task will sleep when no packets are available for processing.
 * The drawback is, that this task can only handle incoming data from this radio.
 *
 * ttc_radio automatically appends all received packets in a single linked list.
 * ttc_radio_packet_received_tryget() can be called any time to obtain all packets in their arrival order
 * until NULL is returned.
 * This function will put current task to sleep if receive list is empty until a packet is received.
 *
 * Two other options are available to retrieve received packets:
 * 1) ttc_radio_packet_received_tryget()
 * 2) write pointer to your own interrupt service into t_ttc_radio_config.Init.function_end_tx_isr before calling ttc_radio_init()
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID          ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param SocketID        ==0: look for any type of packet: >0: look for received packet of certain socket identifier (use E_ttc_packet_pattern_socket_AnyOf_* identifiers to select a group of sockets)
 * @return                == NULL: no packet received (possible if ttc_radio is locked by someone else); != NULL: packet as received from radio
 */
t_ttc_packet* ttc_radio_packet_received_waitfor( t_u8 LogicalIndex, t_u8 UserID, e_ttc_packet_pattern SocketID );

/** Switches radio receiver on or off
 *
 * Note: Will wait until all queued packets are being transmitted.
 *
 * The receiver can be switched on now or after a specified delay time.
 * Delayed switch times allow to reduce energy usage.
 * This can be done by answering an incoming message after a fixed, predefined delay.
 * Scenario:
 * A sends a message to B. B processes this message and sends an answer to A.
 * 1) Messages are sent immediately
 *    A does not know how long B will take to prepare its answer as runtime of software is difficult to
 *    forecast. So A has to switch on it's receiver after sending to B.
 * 2) Messages are answered after fixed delay
 *    A goes to sleep after sending to B for a predefined delay time.
 *    B has egnough time to prepare its answer.
 *    A switches on its receiver at SendTime + DelayTime
 *    B switches on its transmitter at ReceiveTime + DelayTime
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID          ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param Config          configuration of radio device as returned by ttc_radio_get_configuration()
 * @param Enable          !=0: switch on receiver to listen for incoming messages; ==0: switch of receiver (reduces power)
 * @param ReferenceTime   == NULL: enable/disable receiver immediately; !=NULL: switch state at ReferenceTime + Config->Delay_RX
 * @param TimeOut_us      ==0: receiver will stay enabled indefinitely; >0: receiver will switch off after given time (microseconds)
 * @return                ==0: receiver has been activated successfully; >0: error code (ec_radio_DelayedTransmitFailed or ec_radio_IsLocked)
 */
e_ttc_radio_errorcode ttc_radio_receiver( t_u8 LogicalIndex, t_u8 UserID, BOOL Enable, u_ttc_packetimestamp_40* ReferenceTime, t_base TimeOut_us );
BOOL _driver_radio_receiver( t_ttc_radio_config* Config, BOOL Enable, u_ttc_packetimestamp_40* ReferenceTime, t_base TimeOut_us );

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @return                == 0: RADIO device has been reset successfully; != 0: error-code
 */
e_ttc_radio_errorcode ttc_radio_init( t_u8 LogicalIndex );

/** Change or read current setting of auto acknowledge feature
 *
 * Auto acknowledgement is a feature being provided by several transceivers. If a packet is received
 * with header flag acknowledge set, the transceiver can immediately generate and sent the acknowledge packet
 * without interaction of the connected microcontroller.
 *
 * The basic auto acknowledgment setting is configured via setting Config->Init.Flags.AutoAcknowledge before calling ttc_radio_init().
 * Even if enabled, the radio may not be able to generate auto acknowledgements because of its current state.
 * This function allows to set and read the current state from low-level driver.
 * The return value may differ from Enable if transceiver is currently not able to support this feature.
 *
 * See datasheet of your transceiver and current low-level driver for details!
 *
 * @param Config       (t_ttc_radio_config*)  Configuration of radio device as returned by ttc_radio_get_configuration()
 * @param LogicalIndex (t_u8)                 Logical index of radio instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RADIO<n> lines in extensions.active/makefile
 * @param UserID       (t_u8)                 ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param Enable       (BOOL)                 ==TRUE: enable auto acknowledge; ==FALSE: disable auto acknowledge
 * @param Change       (BOOL)                 ==TRUE: write value of Enable to radio; ==FALSE: only read current setting
 * @return             (t_u8)                 current auto acknowledge setting; ==1: radio will automatically reply to incoming packets with set acknowledge flag; ==0: radio will not reply to auto acknowledges (handled by common_radio instead); ==-1: radio is locked (try again later)
 */
t_u8 ttc_radio_configure_auto_acknowledge( t_u8 LogicalIndex, t_u8 UserID, BOOL Enable, BOOL Change );
BOOL _driver_radio_configure_auto_acknowledge( t_ttc_radio_config* Config, BOOL Enable, BOOL Change );

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_RADIO_get_max_LogicalIndex())
 */
void ttc_radio_deinit( t_u8 LogicalIndex );

/** Issues regular maintenance on indexed radio.
 *
 * Call this function regularly to compensate temperature changes
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID          ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 */
void ttc_radio_maintenance( t_u8 LogicalIndex, t_u8 UserID );

/** load default setting values for indexed device
 *
 * Seetings of indexed device are being reset to their default values.
 * Will automatically call ttc_radio_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of radio device (1..ttc_radio_get_max_index() )
 * @return              == 0: default values have been successfully loaded for indexed radio device
 */
e_ttc_radio_errorcode  ttc_radio_load_defaults( t_u8 LogicalIndex );

/** maps from logical to physical device index
 *
 * High-level radios (ttc_radio_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_RADIOn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any
 * order.
 *
 * @param LogicalIndex  logical index of radio device (1..ttc_radio_get_max_index() )
 * @return              physical index of radio device (0 = first physical radio device, ...)
 */
t_u8 ttc_radio_logical_2_physical_index( t_u8 LogicalIndex );

/** maps from physical to logical device index
 *
 * This is a reverse function to ttc_radio_logical_2_physical_index()
 *
 * @param  PhysicalIndex  physical index of radio device (0 = first physical radio device, ...)
 * @return                logical index of radio device (1..ttc_radio_get_max_index() )
 */
t_u8 ttc_radio_physical_2_logical_index( t_u8 PhysicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @return                == 0: radio has been reset successfully; != 0: error-code
 */
void ttc_radio_reset( t_u8 LogicalIndex );

/** transmits content of given buffer as one or more packets of given type to current destination address
 *
 * Note: Content of Data[] is copied into temporary buffers before being queued for transmission.
 * Note: This function provides no flow control. Packets are send out one after another.
 * Note: Transmission may start later depending on current delay settings (see ttc_radio_transmit_packet() for details )
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID          ==0: generic access; >0: radio will be locked while transmitting all buffers for given payload (ensures that all buffers are sent one after another)
 * @param SocketID        ==0: no special socket; >0: will be copied into first byte of payload (requires TTC_PACKET_SOCKET_BYTE==1)
 * @param PacketType      type of packet to send (one from e_ttc_packet_type)
 * @param Data            binary buffer storing data to be send
 * @param AmountBytes     amount of valid bytes in Data[]
 * @param DestinationID   !=NULL: 16/64 bit address + PanID to set; ==NULL: set broadcast address
 * @param DelayNS         == 0: start transmission now; >0: transmitter will be switched on at Packet->Meta.ReceiveTime + DelayNS (nanoseconds)
 * @param Acknowledged    == FALSE: send out one packet after another ; ==TRUE: Use 2-way acknowledge for safe transmission of every single packet (slower)
 * @return  == 0: data has been queued for transmission/ has been sent successfully; ==ec_radio_IsLocked: someone else called ttc_radio_user_lock(); generic error otherwise
 */
e_ttc_radio_errorcode ttc_radio_transmit_buffer( t_u8 LogicalIndex, t_u8 UserID, e_ttc_packet_pattern SocketID, e_ttc_packet_type PacketType, const t_u8* Data, t_u16 AmountBytes, const t_ttc_packet_address* DestinationID, t_u32 DelayNS, BOOL Acknowledged );

/** Wait until current list of packets to transmit is completed
 *
 * @param LogicalIndex device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID          ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @return  == 0: transmissions completed successfully; ==ec_radio_IsLocked: someone else called ttc_radio_user_lock(). Call again to check!
 */
e_ttc_radio_errorcode ttc_radio_transmit_waitfor( t_u8 LogicalIndex, t_u8 UserID );

/** Queue given packet for later transmission.
 *
 * Note: After a data packet has been given to this function it must not be accessed by application any more!
 * Note: Given Packet will be returned to its memory pool automatically.
 * Note: The state of each packet transmission can be monitored if its Meta.StatusTX field has been set.
 * Note: If ec_radio_IsLocked is returned then caller must store or release Packet manually to avoid memory leaks!
 *
 * @param LogicalIndex           device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID                 ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param Packet                 data packet to transmit ( must have been obtained from ttc_radio_packet_get_empty() )
 * @param PayloadSize            amount of bytes stored in Payload[] (calculated by ttc_packet_payload_get_pointer() )
 * @param ReceiveAfterTransmit   == TRUE: turn on receiver after transmission to receive immediate answer
 * @return  == 0: successfull access; ==ec_radio_IsLocked: ttc_radio is locked and Packet was neither queued nor released; generic error otherwise
 */
e_ttc_radio_errorcode ttc_radio_transmit_packet_later( t_u8 LogicalIndex, t_u8 UserID, t_ttc_packet* Packet, t_u16 PayloadSize, BOOL ReceiveAfterTransmit );

/** Transmit given packet after exact delay time relative to receive time.
 *
 * This function allows to send a reply for a received message after an exact delay time.
 * The intention is to reuse the received packet to build a reply message.
 *
 * Scenario:
 *   1) Node A sends request to Node B.
 *   2) Node B performs some software calculations to prepare its answer.
 *   3) Node A switches on its Receiver to wait for answer packet from B
 *
 * When a node prepares an answer for a received packet, the exact response time depends on the processing
 * speed of the microcontroller. Exact response time calculation is difficult for software implementations.
 * In a simple scenario, Node A switches on its receiver right after sending its request to Node B.
 * RX-On time drains energy which is critical in low-power operations. Instead of sending its answer
 * immediately, Nodes A and B can add a common delay time to the end of the first transmission. This delay
 * must be long egnough to give Node B egnough time to prepare its answer.
 *
 * The delay time can be set using ttc_radio_change_delay_time().
 *
 * 2-way Handshake
 * For every packet, a 2-way handshake can be requested to check if it has been delivered.
 * Call ttc_packet_acknowledge_request_set(Packet, TRUE) before to enable 2-way handshake for this packet.
 * The function will block until timeout occurs or acknowledge has been received from target node.
 * Check return code to see if remote node sent acknowledge packet within timeout!
 *
 * Note: After a data packet has been given to this function it must not be accessed by application any more!
 * Note: Given Packet will be returned to its memory pool automatically.
 * Note: The state of each packet transmission can be monitored if its Meta.StatusTX field has been set.
 * Note: If ec_radio_IsLocked is returned then caller must store or release Packet manually to avoid memory leaks!
 * Note: See description of radio_common_transmit_packet() (which handles transmission) for more details!
 *
 * @param LogicalIndex           device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID                 ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param Packet                 reused, received packet
 * @param Packet                 data packet to transmit ( must have been obtained from ttc_radio_packet_get_empty() )
 * @param PayloadSize            amount of bytes stored in Payload[] (calculated by ttc_packet_payload_get_pointer() )
 * @param ReceiveAfterTransmit   == TRUE: turn on receiver after transmission to receive immediate answer
 * @param DelayNS                == 0: start transmission now; >0: transmitter will be switched on at Packet->Meta.ReceiveTime + DelayNS (nanoseconds)
 * @return  == 0: successfull access; ==ec_radio_IsLocked: ttc_radio is locked and Packet was neither queued nor released; == ec_radio_TimeOut: target node did not send acknowledge within timeout; generic error otherwise
 */
e_ttc_radio_errorcode ttc_radio_transmit_packet_delayed( t_u8 LogicalIndex, t_u8 UserID, t_ttc_packet* Packet, t_u16 PayloadSize, BOOL ReceiveAfterTransmit, t_u32 Delay_ns );

/** Transmit given packet now.
 *
 * Given packet will be transmitted BEFORE all queued packets.
 *
 * 2-way Handshake
 * For every packet, a 2-way handshake can be requested to ensure its delivery.
 * Call ttc_packet_acknowledge_request_set(Packet, TRUE) before to enable 2-way handshake for this packet.
 * The function will block until timeout occurs or acknowledge has been received from target node.
 * Check return code to see if remote node sent acknowledge packet within timeout!
 *
 * Note: After a data packet has been given to this function it must not be accessed by application any more!
 * Note: Given Packet will be returned to its memory pool automatically.
 * Note: The state of each packet transmission can be monitored if its Meta.StatusTX field has been set.
 * Note: If ec_radio_IsLocked is returned then caller must store or release Packet manually to avoid memory leaks!
 * Note: See description of radio_common_transmit_packet() (which handles transmission) for more details!
 *
 * @param LogicalIndex           device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID                 ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param Packet                 data packet to transmit ( must have been obtained from ttc_radio_packet_get_empty() )
 * @param PayloadSize            amount of bytes stored in Payload[] (calculated by ttc_packet_payload_get_pointer() )
 * @param ReceiveAfterTransmit   == TRUE: turn on receiver after transmission to receive immediate answer
 * @return  == 0: successfull access; ==ec_radio_IsLocked: ttc_radio is locked and Packet was neither queued nor released; == ec_radio_TimeOut: target node did not send acknowledge within timeout; generic error otherwise (packet has been sent and released)
 */
e_ttc_radio_errorcode ttc_radio_transmit_packet_now( t_u8 LogicalIndex, t_u8 UserID, t_ttc_packet* Packet, t_u16 PayloadSize, BOOL ReceiveAfterTransmit );

/** Start transmission of all queued packets in TX-list now or at defined time.
 *
 * All packets in TX-list will be transmitted one after another until list is empty.
 * Receiver will be switched on after last transmission if requested in last packet meta
 * or in radio configuration.
 *
 * Note: The state of each packet transmission can be monitored if its Meta.StatusTX field has been set.
 *
 * @param LogicalIndex  (t_u8)                       device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID        (t_u8)                       ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param TransmitTime  (u_ttc_packetimestamp_40*) ==0: start transmission now; >0: time when to switch on transmitter
 * @return == 0: successfull access; ==ec_radio_IsLocked: someone else called ttc_radio_user_lock(); generic error otherwise
 */
e_ttc_radio_errorcode ttc_radio_transmit_start( t_u8 LogicalIndex, t_u8 UserID, u_ttc_packetimestamp_40* TransmitTime );

/** Switches channel used to receive and transmit data
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID          ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param NewChannel      Config->Features->MinChannel .. Config->Features->MaxChannel
 * @return == 0: channel switched successfully; ==ec_radio_IsLocked: someone else called ttc_radio_user_lock(); generic error otherwise
 * @return == 0: successfull access; ==ec_radio_IsLocked: someone else called ttc_radio_user_lock(); generic error otherwise
 */
e_ttc_radio_errorcode ttc_radio_configure_channel( t_u8 LogicalIndex, t_u8 UserID, t_u8 NewChannel );

/** Switches channel used to transmit data.
 *
 * Note: On most architectures, this will also set the receive channel.
 *       It is safe to call ttc_radio_configure_channel_tx() and ttc_radio_configure_channel_rx() for same channel.
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID          ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param NewChannel      Config->Features->MinChannel .. Config->Features->MaxChannel
 * @return == 0: channel switched successfully; ==ec_radio_IsLocked: someone else called ttc_radio_user_lock(); generic error otherwise
 */
e_ttc_radio_errorcode ttc_radio_configure_channel_tx( t_u8 LogicalIndex, t_u8 UserID, t_u8 NewChannel );

/** Switches channel used to receive data.
 *
 * Note: On most architectures, this will also set the transmit channel.
 *       It is safe to call ttc_radio_configure_channel_tx() and ttc_radio_configure_channel_rx() for same channel.
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID          ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param NewChannel      Config->Features->MinChannel .. Config->Features->MaxChannel
 * @return == 0: channel switched successfully; ==ec_radio_IsLocked: someone else called ttc_radio_user_lock(); generic error otherwise
 */
e_ttc_radio_errorcode ttc_radio_configure_channel_rx( t_u8 LogicalIndex, t_u8 UserID, t_u8 NewChannel );

typedef enum { // valid options for Filter argument of ttc_radio_configure_frame_filter()
    ttc_radio_frame_filter_read,        // just read current frame filter configuration

    ttc_radio_frame_filter_disable,     // no frame types allowed (FF disabled)

    // combined filter options
    ttc_radio_frame_filter_coordinator, // behave as coordinator (can receive frames with no dest address (PAN ID has to match))
    ttc_radio_frame_filter_sensor,      // behave as sensor (can receive beacon, data, mac and ack frames with matching Destination and PAN ID)

    // individual filter options
    ttc_radio_frame_filter_beacon,      // beacon frames allowed
    ttc_radio_frame_filter_data,        // data frames allowed
    ttc_radio_frame_filter_ack,         // acknowledge frames allowed
    ttc_radio_frame_filter_mac,         // MAC control frames allowed
    ttc_radio_frame_filter_reserved,    // reserves frame types allowed

    u_ttc_radio_frame_filternknown      // invalid argument (Do not use!)
} e_ttc_radio_configure_frame_filter;

/** Configure/ check current frame filter settings
 *
 * Most radio transceivers provide an intergrated filter to automatically ignore received packets
 * that are not to be processed by this node.
 * Providing zero for Filter and Enable will just return the current filter configuration without changing it.
 * E.g.:
 *   u_ttc_radio_frame_filter FrameFilter = ttc_radio_configure_frame_filter(LogicalIndex, 0, 0);
 *   if (FrameFilter.ForeignPanID) { ... }
 *
 * @param LogicalIndex (t_u8)                      device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID       (t_u8)                      ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param Filter       (e_ttc_radio_configure_frame_filter)  Type of filter to enable or disable
 * @return             (u_ttc_radio_frame_filter)  current frame filter settings
 */
u_ttc_radio_frame_filter ttc_radio_configure_frame_filter( t_u8 LogicalIndex, t_u8 UserID, e_ttc_radio_configure_frame_filter Filter );

/** Set new delay to exactly enable transmitter or receiver after given reference time
 *
 * Exact time delays allow to reduce energy by switching off receiver while remote node is preparing an answer packet.
 * Scenario:
 * A:TX @T1a ---------------> B:RX @T1b
 *                            B:prepare answer
 * A:wait until T1a + Delay   B:wait until T1b + Delay
 * A:RX <-------------------- B:TX @T2a
 *
 * @param LogicalIndex    device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID          ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param Delay_Microseconds  >0: time between receival of next message and transmission start of next message; ==0: transmissions are sent immediately
 * @return == 0: successfull access; ==ec_radio_IsLocked: someone else called ttc_radio_user_lock(); generic error otherwise
 */
e_ttc_radio_errorcode ttc_radio_change_delay_time( t_u8 LogicalIndex, t_u8 UserID, t_u16 Delay_Microseconds );

/** Updates meta header of given packet.
 *
 * During reception of a packet frame, an update of Meta header often requires complex calculations.
 * Running these calculations in the interrupt service routine would increase the response time.
 * This function shall be called from application to run the calculations required to fill all fields
 * in Packet->Meta.
 *
 * @param LogicalIndex device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID       ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param Packet       (t_ttc_packet*)  packet which Meta header is to be updated
 * @return             ==0: Meta header has been updated; >0: error code.
 */
e_ttc_radio_errorcode  ttc_radio_update_meta( t_u8 LogicalIndex, t_u8 UserID, t_ttc_packet* Packet );

/** Calculates distance to remote node using simple single-sided two-way ranging.
 *
 *   If the current transceiver provides ranging capabilites (required 802.15.4a compliant Ultra Wide Band radio)
 *   then a ranging request can be sent with this function.
 *
 *   The generic ranging protocols are implemented in radio_common_ranging_measure()
 *   - create ranging request network packet of desired type
 *   - send out ranging request
 *   - wait for ranging reply (or timeout)
 *   - calculate distance to remote node
 *
 *   The underlying low-level driver will then
 *   - create special payload with required ranging data
 *   - set RNG but in PHY Header (PHR)
 *   - store exact transmission time of packet
 *
 *   Incoming ranging requests are handled in radio_common_ranging_reply_isr().
 *
 *  Note: If the current radio device does not support ranging, this function will simply return 0.
 *  Note: This function will block until answer packet has been received or timeout occured
 *
 * @param LogicalIndex           device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID                 ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param Type                   different types of ranging requests may be implemented (one request entry from e_radio_common_ranging_type)
 * @param AmountRepliesRequired  >0: maximum amount of reply messages to store; ==0: store all replies being received within timeout (as long as packet buffers are available)
 * @param RemoteID               Identifier of remote node (RemoteID->Address16 != 0: will use packet with 16-bit addresses, 64-bit adresses otherwise)
 * @param TimeOutMS              function will abort to wait for answer after Timeout milliseconds
 * @param ReferenceTime          == NULL: start transmission now; !=NULL: transmitter will be switched on at ReferenceTime + Config->ReferenceTimeTX
 * @param Distances              array where to write computed distances (must be large egnough to store AmountRepliesRequired entries)
 * @param Locations              !=NULL: array where to store replied locations (only for Type == *location*) (must be large egnough to store AmountRepliesRequired entries); ==NULL: do not store location data
 * @param ReenableReceiver       ==TRUE: receiver will be switched on after last transmission (allows to receive further packets but uses more energy)
 * @return                       amount of valid ranging measures being stored in Distances[] (0..AmountRepliesRequired)
 */
t_u8 ttc_radio_ranging_request( t_u8 LogicalIndex, t_u8 UserID, e_radio_common_ranging_type Type, t_u8 AmountRepliesRequired, const t_ttc_packet_address* RemoteID, t_u16 TimeOutMS, u_ttc_packetimestamp_40* ReferenceTime, t_ttc_radio_distance* Distances, t_ttc_math_vector3d_xyz* Locations, BOOL ReenableReceiver );

/** Add a nanosecond delay to given time stamp
 *
 * Timestamps are transceiver specific. Only current low-level knows its timebase.
 *
 * @param LogicalIndex                              device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param Time        (u_ttc_packetimestamp_40*)  time stamp of a received or transmitted packet
 * @param Delay_ns    (t_u32)                       delay time to add to Time (nanoseconds)
 * @param TimeDelayed (u_ttc_packetimestamp_40*)  will be loaded with Time + Delay_ns; may be same as Time (TimeDelayed==Time)
 * @return  delayed timestamp has been written to *TimeDelayed
 */
void ttc_radio_time_add_ns( t_u8 LogicalIndex, u_ttc_packetimestamp_40* Time, t_u32 Delay_ns, u_ttc_packetimestamp_40* TimeDelayed );

/** Set new delay time for ranging replies
 *
 * If ranging is enabled and when this node receives a ranging request, it will send the
 * ranging reply message at receive time + delay time.
 * The delay time must be long egnough to prepare the reply message.
 *
 * @param LogicalIndex          device index of radio device (1..ttc_RADIO_get_max_LogicalIndex())
 * @param UserID       (t_u8)   ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param Delay_us     (t_u16)  delay time between ranging request receival and ranging reply transmission (microseconds)
 * @return == 0: successfull access; ==ec_radio_IsLocked: someone else called ttc_radio_user_lock(); generic error otherwise
 */
e_ttc_radio_errorcode ttc_radio_set_ranging_delay( t_u8 LogicalIndex, t_u8 UserID, t_u16 Delay_us );

/** Releases all packets from list of received packets for reuse.
 *
 * Call this function to simply ignore received packets.
 * To be sure that no further packets are received, you might want
 * to call ttc_radio_receiver() before to switch of the receiver.
 *
 * @param LogicalIndex Logical index of radio instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RADIO<n> lines in extensions.active/makefile
 * @param UserID       ==0: generic access; >0: unique key identifying ttc_radio user. See ttc_radio_user_new() for details!
 * @param SocketID     ==0: ignore all types of packets: >0: ignore only packets of certain socket identifiers (requires TTC_PACKET_SOCKET_BYTE==1)
 * @return             amount of ignored packets
 */
t_u16 ttc_radio_packets_received_ignore( t_u8 LogicalIndex, t_u8 UserID, e_ttc_packet_pattern SocketID );

/** Register a new radio user
 *
 * Many ttc_radio functions take a UserID argument. This can be set to zero for many applications.
 * Registering and passing a UserID has two advantages:
 * 1) It allows to lock ttc_radio for exclusive access (e.g. during a range measurement)
 * 2) It allows to increase the amount of allocated packet buffers on a per user basis.
 *
 * Example usage:
 *
 * // create a new UserID on radio #1 and increase PoolPackets by three buffers
 * t_u8 MyUserID = ttc_radio_user_new(1, 3);
 *
 * // lock ttc_radio for exclusive use
 * ttc_radio_user_lock(1, MyUserID);
 *
 * // All functions with UserID argument will now be blocked for different UserID values..
 *
 * // do some exclusive actions without intereference from other radio users
 * ttc_radio_*(1, MyUserID);
 * ...
 *
 * // unlock ttc_radio for general use
 * ttc_radio_user_unlock(1, MyUserID);
 *
 * @param LogicalIndex      (t_u8)  Logical index of radio instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RADIO<n> lines in extensions.active/makefile
 * @param AdditionalBuffers (t_u16) Increases amount of buffers stored in PoolPackets by this value.
 * @return                  (t_u8)  new unique user id
 */
t_u8 ttc_radio_user_new( t_u8 LogicalIndex, t_u16 AdditionalPacketBuffers );

/** Locks ttc_radio for exclusive access
*
 * While locked, all ttc_radio_*() functions which take an UserID argument will return an error code for different UserID value.
 * Locking ttc_radio allows to perform radio transactions without interruption from other radio users.
 * If ttc_radio_user_lock() is called for same UserID several times, the same amount of ttc_radio_user_unlock() calls is required to unlock ttc_radio().
 *
 * @param LogicalIndex (t_u8)  Logical index of radio instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RADIO<n> lines in extensions.active/makefile
 * @param UserId       (t_u8)  Unique radio user identification number as returned by ttc_radio_user_new() before
 * @return             (BOOL)  ==TRUE: ttc_radio has been locked for exclusive access; ==FALSE: ttc_radio is already locked by someone else
 */
BOOL ttc_radio_user_lock( t_u8 LogicalIndex, t_u8 UserId );

/** Unlocks ttc_radio from exclusive access
*
 * After unlocking, ttc_radio can be accessed by different UserID again.
 * Note: Will assert if ttc_ttc_radio is locked by different UserID!
 * Note: ttc_radio_user_unlock() must be called as often as ttc_radio_user_lock() to unlock ttc_radio.
 *
 * @param LogicalIndex (t_u8)  Logical index of radio instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RADIO<n> lines in extensions.active/makefile
 * @param UserId       (t_u8)  Unique radio user identification number as returned by ttc_radio_user_new() before
 */
void ttc_radio_user_unlock( t_u8 LogicalIndex, t_u8 UserId );

/** Checks if ttc_radio is currenty locked by somebody
 *
 *
 * @param LogicalIndex (t_u8)  Logical index of radio instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RADIO<n> lines in extensions.active/makefile
 * @return             (t_u8)  ==0: ttc_radio can be accessed by anybody; >0: UserID that locked ttc_radio
 */
t_u8 ttc_radio_user_locked( t_u8 LogicalIndex );

/** Helper function for debug sessions
 *
 * This function has no effect.
 * You may register it as an activity function during a debug session to exactly monitor
 * incoming/ outgoing packets. You may register this function as an activitiy function
 * during a gdb session.
 *
 * Example for debugging outgoing packets after they have ben send to transceiver:
 * (gdb) # install breakpoint (add line to PROJECT_FOLDER/configs/debug2_general.gdb to make it permanent)
 * (gdb) b ttc_radio_debug_activity
 *
 * (gdb) # advance until ttc_radio has been initialized
 * (gdb) c
 *
 * (gdb) # register activity function for all packets after being sent to transceiver
 * (gdb) p Config->Init.function_end_tx_isr = &ttc_radio_debug_activity
 * (gdb) # Note for CortexM3: LSB of memory address must be 1 to avoid hardfault!
 * (gdb) p Config->Init.function_end_tx_isr = ( (t_u32) &ttc_radio_debug_activity) | 1
 *
 * (gdb) # continue execution (stops automatically after transmitting next packet)
 * (gdb) c
 *
 * See t_ttc_radio_config.Init for other activity function hooks!
 *
 * @param Config (t_ttc_radio_config* volatile)  current ttc_radio configuration
 * @param Packet (t_ttc_packet* volatile)        packet bufffer to inspect
 * @return ==TRUE:  function has processed and released Packet via ttc_heap_pool_block_free()
 *         ==FALSE: Packet will be added to List_PacketsRx for later processing
 */
BOOL ttc_radio_debug_activity( t_ttc_radio_config* volatile Config, t_ttc_packet* volatile Packet );

/** Assigns a new dynamic socket identifier
 *
 * ttc_radio implements sockets in a different way than classic network stacks.
 * t_ttc_packet_payload stores an extra socket identifier byte to deliver it to the correct endpoint.
 * All valid socket identifiers have to be added to e_ttc_packet_pattern.
 * These entries in e_ttc_packet_pattern can be assigned dynamically:
 * SocketID == E_ttc_packet_pattern_socket_AnyOf_Application
 *   E_ttc_packet_pattern_socket_Begin_Application+1 .. E_ttc_packet_pattern_socket_End_Application-1
 *
 * Note: Dynamic allocation of socket identifiers is only usefull for special applications in a
 *       closed scenario that do not communicate with nodes runnign different code.
 *       The returned number depends on the order of function calls and can change when the
 *       program flow is changed. This is no problem, when all nodes are running the same code.
 *       If you want to be sure, that your socket identifier always gets the same number, add your own
 *       fixed entry to e_ttc_packet_pattern!
 *
 * Usage:
 *
 * // call this only once (amount of dynamic socket identifiers is limited)!
 * e_ttc_packet_pattern MySocketID = (ttc_radio_socket_new(1, E_ttc_packet_pattern_socket_AnyOf_Application);
 *
 * t_ttc_packet* NewPacket = ttc_radio_packet_get_empty(..., MySocketID);
 * ...Fill NewPacket...
 *
 * // socket identifier is transmitted as first byte of packet payload
 * ttc_radio_transmit_packet_now(...);
 *
 * // check for received packets of matching socket identifier
 * t_ttc_packet* PacketRX = ttc_radio_packet_received_tryget(..., MySocketID);
 * if (PacketRX) {  ... }
 *
 * @param LogicalIndex (t_u8)                   Logical index of radio instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RADIO<n> lines in extensions.active/makefile
 * @param SocketID (e_ttc_packet_pattern)  currently supported: E_ttc_packet_pattern_socket_AnyOf_Application
 * @return             (e_ttc_packet_pattern)  one assigned entry from e_ttc_packet_pattern
 */
e_ttc_packet_pattern ttc_radio_socket_new( t_u8 LogicalIndex, e_ttc_packet_pattern SocketID );


/** Sets new output state of a GPIO line connected to radio transceiver
 *
 * Some external radio transceivers have configurable gpio lines that can be controlled via software commands.
 * The current implementation must be provided by the low level radio_* driver.
 *
 * @param Config       (t_ttc_radio_config*)  Configuration of radio device as returned by ttc_radio_get_configuration()
 * @param LogicalIndex (t_u8)                 Logical index of radio instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_RADIO<n> lines in extensions.active/makefile
 * @param Pin          (e_ttc_radio_gpio)     gpio pin to control (current radio may not provide all possible pins)
 * @param NewState     (BOOL)                 ==TRUE: set GPIO to logical 1; ==FALSE: set GPIO to logical 0
 * @return             (BOOL)                 ==TRUE: desired GPIO pin could be configured to new output state
 */
BOOL ttc_radio_gpio_out( t_u8 LogicalIndex, e_ttc_radio_gpio Pin, BOOL NewState );
BOOL _driver_radio_gpio_out( t_ttc_radio_config* Config, e_ttc_radio_gpio Pin, BOOL NewState );


//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_heap_ are passed to interfaces/ttc_heap_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl heap UPDATE
 */

/** Set new delay to exactly enable transmitter after receiving next message
 *
 * Exact time delays allow to reduce energy by switching off receiver while remote node is preparing an answer packet.
 * Scenario:
 * A:TX @T1a ---------------> B:RX @T1b
 *                            B:prepare answer
 * A:wait until T1a + Delay   B:wait until T1b + Delay
 * A:RX <-------------------- B:TX @T2a
 *
 * @param Config          pointer to struct t_ttc_radio_config
 * @param Delay_Microseconds  >0: time between receival of next message and transmission start of next message; ==0: transmissions are sent immediately
 */
void _driver_radio_change_delay_time( t_ttc_radio_config* Config, t_u16 Delay_Microseconds );

/** Configure frame filter in connected radio
 *
 * Most radio transceivers provide an intergrated filter to automatically ignore received packets
 * that are not to be processed by this node.
 * This function allows to change or just read the current frame filter settings.
 *
 * @param Config       configuration of radio device as returned by ttc_radio_get_configuration()
 * @param Settings     (u_ttc_radio_frame_filter)  configuration to use
 * @param ChangeFilter (t_u8)                      == 1: load new filter setting into radio; ==0: only read current filter setting; ==-1: store changes in cache for later update
 * @return             (u_ttc_radio_frame_filter)  updated configuration (unsupported filters may be reset)
 */
u_ttc_radio_frame_filter _driver_radio_configure_frame_filter( t_ttc_radio_config* Config, u_ttc_radio_frame_filter Settings, BOOL ChangeFilters );

/** transmit single packet now or at specified time
 *
 * Duties of low-level driver:
 * do {
 *   (1) pop oldest packet from transmission list via radio_common_pop_list_tx()
 *   (2) calculate and configure exact time of transmission (if DelayNS!=0)
 *   (3) configure according to Packet->Meta.Flags
 *   (4) call Config->Init.function_start_tx_isr(Packet) (if !=NULL)
 *   (5) send packet to radio transmit buffer
 *   (6) start packet transmission
 *   (7) call ttc_heap_pool_block_free(Packet) after successfull transmission to return packet it to its memory pool
 *   (8) call Config->Init.function_end_tx_isr(Packet) (if !=NULL)
 * } while ( s_ttc_listize(Config->List_PacketsTX) );
 *
 * Note: ttc_radio_transmit_packet() takes care that this function is not called from multiple tasks in parallel.
 *
 * @param Config                pointer to struct t_ttc_radio_config
 * @param DelayNS               == 0: start transmission now; >0: transmitter will be switched on at Packet->Meta.ReceiveTime + DelayNS (nanoseconds)
 * @return                      == 0: data has been queued for transmission/ has been sent successfully; != 0: error-code
 */
void _driver_radio_transmit( t_ttc_radio_config* Config, u_ttc_packetimestamp_40* TransmitTime );

/** Switches channel used to transmit + receive data.
 *
 * @param Config        = pointer to struct t_ttc_radio_config
 * @param NewChannel      Config->Features->MinChannel .. Config->Features->MaxChannel
 * @return              == 0: channel switched successfully; != 0: error-code
 */
e_ttc_radio_errorcode _driver_radio_configure_channel_rxtx( t_ttc_radio_config* Config, t_u8 NewChannel );

/** Switches channel used to transmit data.
 *
 * Note: If current transceiver does not support to change rx-/tx-channel independently, just undefine this macro!
 *
 * @param Config        = pointer to struct t_ttc_radio_config
 * @param NewChannel      Config->Features->MinChannel .. Config->Features->MaxChannel
 * @return              == 0: channel switched successfully; != 0: error-code
 */
e_ttc_radio_errorcode _driver_radio_configure_channel_tx( t_ttc_radio_config* Config, t_u8 NewChannel );

/** Switches channel used to receive data.
 *
 * Note: If current transceiver does not support to change rx-/tx-channel independently, just undefine this macro!
 *
 * @param Config        = pointer to struct t_ttc_radio_config
 * @param NewChannel      Config->Features->MinChannel .. Config->Features->MaxChannel
 * @return              == 0: channel switched successfully; != 0: error-code
 */
e_ttc_radio_errorcode _driver_radio_configure_channel_rx( t_ttc_radio_config* Config, t_u8 NewChannel );

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_radio_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_radio_features.
 *
 * @param Config = pointer to struct t_ttc_radio_config
 */
void _driver_radio_configuration_check( t_ttc_radio_config* Config );

/** shutdown single RADIO unit device
 * @param Config        pointer to struct t_ttc_radio_config
 * @return              == 0: RADIO has been shutdown successfully; != 0: error-code
 */
e_ttc_radio_errorcode _driver_radio_deinit( t_ttc_radio_config* Config );

/** initializes single RADIO unit for operation
 * @param Config        pointer to struct t_ttc_radio_config
 * @return              == 0: RADIO has been initialized successfully; != 0: error-code
 */
e_ttc_radio_errorcode _driver_radio_init( t_ttc_radio_config* Config );

/** Issues regular maintenance on indexed radio.
 *
 * Call this function regularly to compensate temperature changes
 *
 * @param Config        pointer to struct t_ttc_radio_config
 */
void _driver_radio_maintenance( t_ttc_radio_config* Config );

/** loads configuration of indexed RADIO unit with default values
 * @param Config        pointer to struct t_ttc_radio_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_radio_errorcode _driver_radio_load_defaults( t_ttc_radio_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_radio_config
 */
e_ttc_radio_errorcode _driver_radio_reset( t_ttc_radio_config* Config );

/** Updates meta header of given packet.
 *
 * During reception of a packet frame, an update of Meta header often requires complex calculations.
 * Running these calculations in the interrupt service routine would increase the response time.
 * This function shall be called from application to run the calculations required to fill all fields
 * in Packet->Meta.
 *
 * @param Config        pointer to struct t_ttc_radio_config
 * @param Packet       (t_ttc_packet*)  packet which Meta header is to be updated
 * @return             ==0: Meta header has been updated; >0: error code
 */
e_ttc_radio_errorcode _driver_radio_update_meta( t_ttc_radio_config* Config, t_ttc_packet* Packet );

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

/** low-level interrupt service routine for incoming packets
 *
 * This function must be registered as interrupt service routine for incoming packets.
 * Example for interrupts via GPIO pin A4:
 *    t_ttc_interrupt_handle RadioInterrupt = ttc_interrupt_init(tit_GPIO_Rising, E_ttc_gpio_pin_a4, _driver_radio_isr_receive, Config, 0, 0);
 *    Assert(RadioInterrupt, ttc_assert_origin_auto); // could not initialize interrupt (configured GPIO pin not usable for external interrupts on current uC architecture?)
 *    ttc_interrupt_enable_gpio(RadioInterrupt);
 *
 * This function has to be implemented by low-level radio driver:
 * 1) get an empty packet buffer via radio_common_packet_get_empty_isr()
 * 2) copy received raw data bytes from transceiver into empty packet
 * 3) call radio_common_push_list_rx_isr() to deliver filled packet to application
 *    The delivered packet will automatically been released to its memory pool after being processed.
 *
 * The low-level radio driver may declare these functions as extern to be able to call
 * @param Index   index of triggered interrupt (for gpio interrupts, this can be casted to e_ttc_gpio_pin)
 * @param Config  radio configuration as returned by ttc_radio_get_configuration()
 */
void _driver_radio_isr_receive( t_physical_index Index, t_ttc_radio_config* Config );

/** low-level interrupt service routine for outgoing packets
 *
 * This function must be registered as interrupt service routine for outgoing packets.
 * Example for interrupts via GPIO pin A4:
 *    t_ttc_interrupt_handle RadioInterrupt = ttc_interrupt_init(tit_GPIO_Rising, E_ttc_gpio_pin_a4, _driver_radio_isr_transmit, Config, 0, 0);
 *    Assert(RadioInterrupt, ttc_assert_origin_auto); // could not initialize interrupt (configured GPIO pin not usable for external interrupts on current uC architecture?)
 *    ttc_interrupt_enable_gpio(RadioInterrupt);
 *
 * This function has to be implemented by low-level radio driver:
 * 1) t_ttc_packet* Packet = radio_common_pop_list_tx( Config )
 * 2) call Config->Init.function_start_tx_isr() (if set)
 * 3) send packet data to transceiver transmit buffer
 * 4) observe transmission
 * 5) call Config->Init.function_end_tx_isr() (if set)
 *
 * The low-level radio driver may declare these functions as extern to be able to call
 * @param Index   index of triggered interrupt (for gpio interrupts, this can be casted to e_ttc_gpio_pin)
 * @param Config  radio configuration as returned by ttc_radio_get_configuration()
 */
void _driver_radio_isr_transmit( t_physical_index Index, t_ttc_radio_config* Config );

//}

#endif //TTC_RADIO_H
