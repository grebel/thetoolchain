#ifndef TTC_BASIC_H
#define TTC_BASIC_H
/** { ttc_basic.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for BASIC devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_BASIC(tc_basic_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_basic_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_basic_init(LogicalIndex);
 *  4) use:         ttc_basic_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level basic and application.
 *
 *  Created from template ttc_device.h revision 30 at 20140603 04:04:24 UTC
 *
 *  Authors: Gregor Rebel
 */
/** Description of ttc_basic (Do not delete this line!)
 *
 *
}*/

#ifndef EXTENSION_ttc_basic
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_basic.sh
#endif
#ifndef TTC_COMPILE_OPTS_H
    #include "compile_options.h"    // all constant definitions from makefile
#endif
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_basic.c"
//
#include "interfaces/ttc_basic_interface.h" // multi architecture support
#include "ttc_memory_types.h"               // datatypes and definitions for memory layout of current microcontroller
#include "ttc_assert.h"                     // configurable self tests and debuggable error reporting mechanism
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************


/**{ basic macros *************************************************************
  *
  */

#ifndef _STDLIB_H_ // abs is defined in stdlib.h
    #ifndef abs
        #define abs(VALUE)  ( ((VALUE) < 0) ? -(VALUE) : (VALUE) )
    #endif
#endif

/** Calculate positive distance between two values even for two t_u32 values
 *
 * @param VALUE1  signed or unsigned value
 * @param VALUE2  signed or unsigned value
 */
#define ttc_basic_distance(VALUE1, VALUE2) (  (VALUE1 > VALUE2) ? ( (VALUE1) - (VALUE2) )  : ( (VALUE2) - (VALUE1) )  )

/** checks of value is equal to compare value according to given error
 *
 * Precisely: Checks if VALUE1 is within interval [VALUE2 - EPSILON, ...m VALUE2 + EPSILON]
 *
 */
#define ttc_basic_within(VALUE, COMPARE, ERROR) ( ttc_basic_distance(VALUE, COMPARE) < ERROR )

/** Calulates integer logarithm of A for given base
 *
 * This is a lightweight implementation to find out how often Base can be
 * multiplied to get bigger than or equal to A.
 * In other words: If A is to be represented by digits of Base amount of symbols,
 *                 how many digits are required?
 * A special case is Base == 2. Then the computed rank is the amount of bits required
 * to represent A.
 *
 * @param A     input for log(A) operation
 * @param Base  A = ttc_math_pow(Base, Return)
 * @return      amount of multiplications of Base to reach A
 */
t_u8 ttc_basic_int_rankN( t_u32 A, t_u8 Base );

/** Computes amount of bits required to store given number
 *
 * @param A     input for log2(A) operation
 * @return      amount of multiplications of 2 to reach A
 */
t_u8 ttc_basic_int_rank2( t_u32 A );


/** Smart integer calculation of A * B / C which tries to avoid overrun
 *
 * Integer multiplication can overflow if the product exceeds 4 giga (for t_u32).
 * Smart rearrangement of multiplication and division in a calculation can avoid the overflow.
 * Note: Will Assert if overflow occurs!
 *
 * @param  A  32 bit unsigned integer
 * @param  B  32 bit unsigned integer
 * @param  C  32 bit unsigned integer
 * @return A * B / C
 */
t_u32 ttc_basic_multiply_divide_u32x3( t_u32 A, t_u32 B, t_u32 C );

/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * Not: This macro will automatically use native data-size for current architecture.
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_base ttc_atomic_read_write( void* Address, t_base NewValue );
#if TARGET_DATA_WIDTH == 32
    #define ttc_atomic_read_write(Address, NewValue) ttc_basic_32_atomic_read_write(Address, NewValue)
#endif
#if TARGET_DATA_WIDTH == 16
    #define ttc_atomic_read_write(Address, NewValue) ttc_basic_16_atomic_read_write(Address, NewValue)
#endif
#if TARGET_DATA_WIDTH == 8
    #define ttc_atomic_read_write(Address, NewValue) ttc_basic_08_atomic_read_write(Address, NewValue)
#endif
#ifndef ttc_atomic_read_write
    #error TARGET_DATA_WIDTH not defined!
#endif

/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * Not: This macro will automatically use native data-size for current architecture.
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_base_signed ttc_basic_atomic_add( void* Address, t_base_signed Amount );
#if TARGET_DATA_WIDTH == 32
    #define ttc_basic_atomic_add(Address, Amount) ttc_basic_32_atomic_add(Address, Amount)
#endif
#if TARGET_DATA_WIDTH == 16
    #define ttc_basic_atomic_add(Address, Amount) ttc_basic_16_atomic_add(Address, Amount)
#endif
#if TARGET_DATA_WIDTH == 8
    #define ttc_basic_atomic_add(Address, Amount) ttc_basic_08_atomic_add(Address, Amount)
#endif
#ifndef ttc_basic_atomic_add
    #error TARGET_DATA_WIDTH not defined!
#endif

/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u8 ttc_basic_08_atomic_add( t_u8* Address, t_s8 Amount );
t_u8 _driver_basic_08_atomic_add( t_u8* Address, t_s8 Amount );
#define ttc_basic_08_atomic_add(Address, Amount) _driver_basic_08_atomic_add(Address, Amount)

/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u8 ttc_basic_08_atomic_read_write( t_u8* Address, t_u8 NewValue );
t_u8 _driver_basic_08_atomic_read_write( t_u8* Address, t_u8 NewValue );
#define ttc_basic_08_atomic_read_write(Address, NewValue) _driver_basic_08_atomic_read_write(Address, NewValue)

/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u16 ttc_basic_16_atomic_add( t_u16* Address, t_s16 Amount );
t_u16 _driver_basic_16_atomic_add( t_u16* Address, t_s16 Amount );
#define ttc_basic_16_atomic_add(Address, Amount) _driver_basic_16_atomic_add(Address, Amount)

/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u16 ttc_basic_16_atomic_read_write( t_u16* Address, t_u16 NewValue );
t_u16 _driver_basic_16_atomic_read_write( t_u16* Address, t_u16 NewValue );
#define ttc_basic_16_atomic_read_write(Address, NewValue) _driver_basic_16_atomic_read_write(Address, NewValue)

/** Write given 16 bit data in little endian order into given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * Little Endian stores byte of lowest value first.
 * E.g.
 * t_u8 Buffer[2];
 * ttc_basic_16_write_little_endian(Buffer, 0x1234);
 *
 * // Equals to
 * Buffer[0] = 0x34;
 * Buffer[1] = 0x12;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     16 bit value to copy into Buffer[]
 */
void ttc_basic_16_write_little_endian( t_u8* Buffer, t_u16 Data );
// void _driver_basic_16_write_little_endian(t_u8* Buffer, t_u16 Data);
#define ttc_basic_16_write_little_endian(Buffer, Data) _driver_basic_16_write_little_endian(Buffer, Data)

/** Write given 16 bit data in big endian order into given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * bBg Endian stores byte of highest value first.
 * E.g.
 * t_u8 Buffer[2];
 * ttc_basic_16_write_big_endian(Buffer, 0x1234);
 *
 * // Equals to
 * Buffer[0] = 0x12;
 * Buffer[1] = 0x34;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     16 bit value to copy into Buffer[]
 */
void ttc_basic_16_write_big_endian( t_u8* Buffer, t_u16 Data );
// void _driver_basic_16_write_big_endian(t_u8* Buffer, t_u16 Data);
#define ttc_basic_16_write_big_endian(Buffer, Data) _driver_basic_16_write_big_endian(Buffer, Data)

/** Read 16 bit data in little endian order from given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * Little Endian stores byte of highest value first.
 * E.g.
 * t_u8 Buffer[2];
 * Buffer[0] = 0x12;
 * Buffer[1] = 0x34;
 *
 * t_u16 Value = ttc_basic_16_read_little_endian(Buffer);
 * // Equals to
 * t_u16 Value = 0x1234;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     16 bit value to copy into Buffer[]
 */
t_u16 ttc_basic_16_read_little_endian( const t_u8* Buffer );
// t_u16 _driver_basic_16_read_little_endian(const t_u8* Buffer);
#define ttc_basic_16_read_little_endian(Buffer) _driver_basic_16_read_little_endian(Buffer)

/** Read 16 bit data in big endian order from given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * Big Endian stores byte of highest value first.
 * E.g.
 * t_u8 Buffer[2];
 * Buffer[0] = 0x12;
 * Buffer[1] = 0x34;
 *
 * t_u16 Value = ttc_basic_16_read_big_endian(Buffer);
 * // Equals to
 * t_u16 Value = 0x3412;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     16 bit value to copy into Buffer[]
 */
t_u16 ttc_basic_16_read_big_endian( const t_u8* Buffer );
// t_u16 _driver_basic_16_read_big_endian(const t_u8* Buffer);
#define ttc_basic_16_read_big_endian(Buffer) _driver_basic_16_read_big_endian(Buffer)

/** Convert referenced value into 16 bit little endian format
 *
 * Note: On little endian architectures, this will generate no code at all.
 * Note: This function is not thread safe!
 *
 * @param Variable (t_u16*)  address of value to convert
 */
void ttc_basic_16_make_little_endian( t_u16* Variable );
#if TTC_BASIC_BIG_ENDIAN
    #define ttc_basic_16_make_little_endian(Variable) ttc_basic_16_swap_endian(Variable)
#else
    #define ttc_basic_16_make_little_endian(Variable)
#endif

/** Convert referenced value into 16 bit big endian format
 *
 * Note: On big endian architectures, this will generate no code at all.
 * Note: This function is not thread safe!
 *
 * @param Variable (t_u16*)  address of value to convert
 */
void ttc_basic_16_make_big_endian( t_u16* Variable );
#if TTC_BASIC_LITTLE_ENDIAN
    #define ttc_basic_16_make_big_endian(Variable) ttc_basic_16_swap_endian(Variable)
#else
    #define ttc_basic_16_make_big_endian(Variable)
#endif

/** Swaps bytes in referenced value to switch big<->little endian
 *
 * Note: Calling this function twice on same value will restore original content.
 * Note: This function is not thread safe!
 *
 * @param Variable (t_u16*)  address of value to convert
 */
void ttc_basic_16_swap_endian( t_u16* Variable );
void _driver_basic_16_swap_endian( t_u16* Variable );
#ifdef _driver_basic_16_swap_endian
    #define ttc_basic_16_swap_endian _driver_basic_16_swap_endian
#endif


/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u32 ttc_basic_32_atomic_read_write( t_u32* Address, t_u32 NewValue );
t_u32 _driver_basic_32_atomic_read_write( t_u32* Address, t_u32 NewValue );
#define ttc_basic_32_atomic_read_write(Address, NewValue) _driver_basic_32_atomic_read_write(Address, NewValue)

/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
t_u32 ttc_basic_32_atomic_add( t_u32* Address, t_s32 Amount );
t_u32 _driver_basic_32_atomic_add( t_u32* Address, t_s32 Amount );
#define ttc_basic_32_atomic_add(Address, Amount) _driver_basic_32_atomic_add(Address, Amount)

/** Write given 32 bit data in little endian order into given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * Little Endian stores byte of lowest value first.
 * E.g.
 * t_u8 Buffer[4];
 * ttc_basic_32_write_little_endian(Buffer, 0x12345678);
 *
 * // Equals to
 * Buffer[0] = 0x78;
 * Buffer[1] = 0x56;
 * Buffer[2] = 0x34;
 * Buffer[3] = 0x12;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     32 bit value to copy into Buffer[]
 */
void ttc_basic_32_write_little_endian( t_u8* Buffer, t_u32 Data );
// void _driver_basic_32_write_little_endian(t_u8* Buffer, t_u32 Data);
#define ttc_basic_32_write_little_endian(Buffer, Data) _driver_basic_32_write_little_endian(Buffer, Data)

/** Write given 32 bit data in big endian order into given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * big Endian stores byte of highest value first.
 * E.g.
 * t_u8 Buffer[4];
 * ttc_basic_32_write_big_endian(Buffer, 0x12345678);
 *
 * // Equals to
 * Buffer[0] = 0x12;
 * Buffer[1] = 0x34;
 * Buffer[2] = 0x56;
 * Buffer[3] = 0x78;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     32 bit value to copy into Buffer[]
 */
void ttc_basic_32_write_big_endian( t_u8* Buffer, t_u32 Data );
// void _driver_basic_32_write_big_endian(t_u8* Buffer, t_u32 Data);
#define ttc_basic_32_write_big_endian(Buffer, Data) _driver_basic_32_write_big_endian(Buffer, Data)

/** Read 32 bit data in little endian order from given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * Little Endian stores byte of highest value first.
 * E.g.
 * t_u8 Buffer[4];
 * Buffer[0] = 0x12;
 * Buffer[1] = 0x34;
 * Buffer[2] = 0x56;
 * Buffer[3] = 0x78;
 *
 * t_u32 Value = ttc_basic_32_read_little_endian(Buffer);
 * // Equals to
 * t_u32 Value = 0x12345678;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     32 bit value to copy into Buffer[]
 */
t_u32 ttc_basic_32_read_little_endian( const t_u8* Buffer );
// t_u32 _driver_basic_32_read_little_endian(const t_u8* Buffer);
#define ttc_basic_32_read_little_endian(Buffer) _driver_basic_32_read_little_endian(Buffer)

/** Read 32 bit data in big endian order from given buffer
 *
 * The native storage order of 16- and 32-bit values depends on current microcontroller architecture.
 * The low-level driver implementation of this function ensures that given data is always stored in correct order.
 *
 * Big Endian stores byte of highest value first.
 * E.g.
 * t_u8 Buffer[4];
 * Buffer[0] = 0x12;
 * Buffer[1] = 0x34;
 * Buffer[2] = 0x56;
 * Buffer[3] = 0x78;
 *
 * t_u32 Value = ttc_basic_32_read_big_endian(Buffer);
 * // Equals to
 * t_u32 Value = 0x78563412;
 *
 * @param Buffer   buffer storing single bytes
 * @param Data     32 bit value to copy into Buffer[]
 */
t_u32 ttc_basic_32_read_big_endian( const t_u8* Buffer );
// t_u32 _driver_basic_32_read_big_endian(const t_u8* Buffer);
#define ttc_basic_32_read_big_endian(Buffer) _driver_basic_32_read_big_endian(Buffer)

/** Convert referenced value into 32 bit little endian format
 *
 * Note: On little endian architectures, this will generate no code at all.
 *
 * @param Variable (t_u32*)  address of value to convert
 */
void ttc_basic_32_make_little_endian( t_u32* Variable );
#if TTC_BASIC_BIG_ENDIAN
    #define ttc_basic_32_make_little_endian(Variable) ttc_basic_32_swap_endian(Variable)
#else
    #define ttc_basic_32_make_little_endian(Variable)
#endif

/** Convert referenced value into 32 bit big endian format
 *
 * Note: On nig endian architectures, this will generate no code at all.
 * Note: This function is not thread safe!
 *
 * @param Variable (t_u32*)  address of value to convert
 */
void ttc_basic_32_make_big_endian( t_u32* Variable );
#if TTC_BASIC_LITTLE_ENDIAN
    #define ttc_basic_32_make_big_endian(Variable) ttc_basic_32_swap_endian(Variable)
#else
    #define ttc_basic_32_make_big_endian(Variable)
#endif

/** Swaps bytes in referenced value to switch big<->little endian
 *
 * Note: Calling this function twice on same value will restore original content.
 * Note: This function is not thread safe!
 *
 * @param Variable (t_u32*)  address of value to convert
 */
void ttc_basic_32_swap_endian( t_u32* Variable );
void _driver_basic_32_swap_endian( t_u32* Variable );
#ifdef _driver_basic_32_swap_endian
    #define ttc_basic_32_swap_endian _driver_basic_32_swap_endian
#endif

//}

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level basic only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * basic devices on all supported architectures.
 * Check basic/basic_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares basic Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_basic_prepare();
void _driver_basic_prepare();


/** Used for automatic code tests.
 *
 * Call from a place in your code that must be reached to successfully pass the test.
 * Place a breakpoint inside ttc_basic_test_ok() to check if your test passes.
 *
 */
void ttc_basic_test_ok();

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_basic(t_u8 LogicalIndex)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_heap_ are passed to interfaces/ttc_heap_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl heap UPDATE
 */

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_BASIC_H
