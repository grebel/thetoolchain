#ifndef TTC_GUI
#define TTC_GUI
/*{ ttc_gui.h ************************************************
}*/


//{ Includes *************************************************************
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "ttc_touchpad.h"
#include "ttc_gfx.h"

//} Includes
//{ Defines/ TypeDefs ****************************************************
#define ADD_DELAY 100
#define SUB_DELAY 100
#define TTC_GUI_MAX_KEYBOARD_RETURN 6
//} Defines
//{ Structures/ Enums ****************************************************

typedef struct s_ttc_gui_number_area {
    t_u16* number;
    t_u16 OldNumber;
    t_u16 X;
    t_u16 Y;
    t_u8 DecPoint;
    t_base Size;
    struct s_ttc_gui_number_area* Next;
} __attribute__((__packed__)) t_ttc_gui_number_area;

typedef struct gui_Keyboard_s{
    char Return[TTC_GUI_MAX_KEYBOARD_RETURN];
    char Password[TTC_GUI_MAX_KEYBOARD_RETURN];
    t_u8 Counter;
    BOOL Lock;
    void (*Source)(void* Argument);
    void (*Destination) (void* Argument);
} __attribute__((__packed__)) t_gui_keyboard;

typedef enum {
    gss_Default,
    gss_Rectangle,
    gss_Rounded
} e_tit_inputareashape;

typedef enum {
    me_MouseDown,
    me_MouseUp,
    me_MouseMove,
    me_MoveIn,
    me_MoveOut,
    me_noEvent

} e_tit_mouseevent;

typedef struct {
    unsigned EventOnMouseDown:1;
    unsigned EventOnMouseUp:1;
    unsigned EventOnMouseIn:1;
    unsigned EventOnMouseContinous:1;
    unsigned EventOnMouseOut:1;
    unsigned Reserved:1;
    unsigned Reserved1:1;
    unsigned Reserved2:1;
}__attribute__((__packed__)) t_events;

typedef struct s_ttc_gui_input_area {
    t_u16 Left;   //left end of the botton
    t_u16 Top;    //top of the botton
    t_u16 Right;  //right end of the botton
    t_u16 Bottom; //buttom of the button
    t_u16 X;      //activ X position
    t_u16 Y;      //activ Y position
    const char* Text; //Text in the case the area is a bottom or the draw area has got a head
    e_tit_mouseevent Event; //gives information about the events that is necessary for executing the handler
    e_tit_mouseevent CurrentEvent;
    e_tit_inputareashape Shape; //Layout of the font
    void (*TouchHandler)(void* Argument); //pointer on the handler function
    void* Argument; //argument of the handler funtion
    struct s_ttc_gui_input_area* Next; //pointer on the next InputArea handler

} t_tit_inputarea;

typedef struct s_ttc_gui_scroll_data {
    t_tit_inputarea Area; //Area of the Button
    t_u16 oldX;      //previous X position
    t_u16 oldY;      //previous Y position
} t_tit_scrolldata;

//} Structures/ Enums

//{ Global Variables *****************************************************


//} Global Variables

//{ Function prototypes **************************************************

void ttc_gui_create_push_button(void (*TouchHandler)(void* Argument), void* Argument, t_u16 Left, t_u16 Top, t_u16 Right, t_u16 Bottom, const char* Text);

void ttc_gui_clear();

void ttc_gui_plus_button(t_u16 Left, t_u16 Top, t_u16 Right, t_u16 Bottom, void* value);

void ttc_gui_minus_button(t_u16 Left, t_u16 Top, t_u16 Right, t_u16 Bottom, void* value);

void ttc_gui_create_number_field(t_u16 X, t_u16 Y, t_u16* number, t_base Size);

t_ttc_gui_number_area *ttc_gui_create_number_field_floating(t_u16 X, t_u16 Y, t_u16* number, t_u8 DecDigit, t_base Size);

void ttc_gui_number_refresh();

void ttc_gui_delete_all_number_areas();

void _ttc_add_one(void* data);

void _ttc_sub_one(void* data);

void ttc_gui_keyboard_1_9(void (*Source)(void* Argument), void (*Destination)(void* Argument),char Password[TTC_GUI_MAX_KEYBOARD_RETURN]);

void _ttc_gui_keyboard_add(void* number);

void ttc_gui_create_input_line(t_u8 X, t_u8 Y,t_u8 XNumber, const char* Text, t_u16* Variable, t_u8 DecPoint, t_base Size);
#endif
