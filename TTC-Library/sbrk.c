/** sbrk.c -  compatibility interface for libraries depending on virtual memory support
  *
  * written by Gregor Rebel 2012-2015
  *
  * This file provides a very basic implementation of virtual memory support functions to
  * allow compilation of some usefull, Linux based C-libraries.
  * Many functions will assert on call to indicate use of unimplemented functionality.
  *
  * Note: This file does not provide any functionality! It only helps to compile things.
  */

#include <stddef.h>
#include <stdio.h>
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)

TODO( "make sbrk compatible to newlib to allow semihosting!" )

/* We do not provide a Heap for virtual memory */
#define HEAPSIZE    0x000

signed char* sbrk( size_t size );

union HEAP_TYPE {
    signed long dummy;
#if HEAPSIZE == 0
    signed char heap[1]; // array of size 0 not allowed by ISO C99
#else
    signed char heap[HEAPSIZE];
#endif
};

static union HEAP_TYPE  heap_area;

/* End address allocated by sbrk */
static signed char*     brk = ( signed char* ) & heap_area;

__attribute__( ( weak ) ) signed char* sbrk( size_t size ) {
    signed char* p;
    Assert( HEAPSIZE != 0, ttc_assert_origin_auto ); // Will fail

    if ( brk + size > heap_area.heap + HEAPSIZE ) {
        p = ( signed char* ) - 1;
    }
    else {
        p = brk;
        brk += size;
    }

    return p;
}

__attribute__( ( weak ) ) signed char* _sbrk( size_t size )                       { return sbrk( size ); }
__attribute__( ( weak ) ) pid_t _getpid( void )                                   { return 1; }
__attribute__( ( weak ) ) void _exit( int Status )                                { ttc_assert_halt_origin( ttc_assert_origin_auto ); ( void ) Status; }
__attribute__( ( weak ) ) int _kill( int pid, int sig )                           { ttc_assert_halt_origin( ttc_assert_origin_auto ); ( void ) pid; ( void ) sig; return -1; }
__attribute__( ( weak ) ) ssize_t _write( int fd, const void* buf, size_t count ) { ttc_assert_halt_origin( ttc_assert_origin_auto ); ( void ) fd; ( void ) buf; ( void ) count; return count; }
__attribute__( ( weak ) ) int _close( int fd )                                    { ttc_assert_halt_origin( ttc_assert_origin_auto ); ( void ) fd; return -1; }
__attribute__( ( weak ) ) int _fstat( int fd, void* buf )                         { ( void ) fd; ( void ) buf; return 0; }
__attribute__( ( weak ) ) int _isatty( int fd )                                   { ttc_assert_halt_origin( ttc_assert_origin_auto ); ( void ) fd; return 0; }
__attribute__( ( weak ) ) off_t _lseek( int fd, off_t offset, int whence )        { ttc_assert_halt_origin( ttc_assert_origin_auto ); ( void ) fd; ( void ) offset; ( void ) whence; return 0; }
__attribute__( ( weak ) ) ssize_t _read( int fd, void* buf, size_t count )        { ttc_assert_halt_origin( ttc_assert_origin_auto ); ( void ) fd; ( void ) buf; ( void ) count; return 0; }

