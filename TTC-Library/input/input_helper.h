/** { input_helper.h *********************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.h revision 10 at 20150406 20:57:04 UTC
 *
 *  Authors: gregor
}*/
/** { Includes *********************************************************
 *
 * Put all includes here that are required to include this header file.
 * Do NOT add includes required to compile input_helper.c here!
 */

#include "../ttc_basic.h"  // basic datatypes
#include "../ttc_memory.h" // memory checks and safe pointers
#include "../ttc_input_types.h"

// #include "../../ttc-lib/ttc_heap.h"   // dynamic memory and safe arrays
// #include "../../ttc-lib/ttc_string.h" // string compare and copy
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Constant Defines / Macros ****************************************
 *
 * As a convention, constant definitions are all UPPERCASED to distinguish
 * them from variables and functions.
 *
 * Examples:

 #define ABC 1
 #define calculate(A,B) (A+B)
 */

//}ConstantDefines
/** { Structure Declarations *******************************************
 *
 * This can be typedef struct or typedef enum declarations.
 * Always prefix structures and enums with filename to avoid double d0eclarations!
 *
 * Examples:

   typedef struct s_input_helper_list {
      struct s_input_helper_list* Next;
      t_base Value;
   } t_input_helper_list;

   typedef enum {
     input_helper_None,
     input_helper_Type1
   } e_input_helper_types;

 */

//}StructureDeclarations
/** { Function Declarations ********************************************
 *
 * Declare all functions here that may be called from outside.
 * Rules of Good Code:
 * - document every function declaration
 * - prefix public function names with their filename to avoid double definitions
 */
// { Example declaration
/** calculates the sum of A and B
 *
 * @param A  unsigned integer
 * @param B  unsigned integer
 * @return   A + B
 */
// t_u8 input_helper_calculate(t_u16 A, t_u16 B);
//}


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations