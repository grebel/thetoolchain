/** { input_helper.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  Created from template ttc-lib/templates/new_file.c revision 10 at 20150406 20:57:04 UTC
 *
 *  Authors: gregor
}*/

/** { Includes ********************************************************
 *
 * Put all includes here that are required to compile this source code.
 */

#include "input_helper.h"
#include "../ttc_gpio.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Global Variables ************************************************
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//}GlobalVariables
/** { Private Function Declarations ***********************************
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc(); 
 */

//}PrivateFunctions
/** { Function Definitions ********************************************
 */

//}FunctionDefinitions
