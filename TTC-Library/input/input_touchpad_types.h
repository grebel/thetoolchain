#ifndef INPUT_TOUCHPAD_TYPES_H
#define INPUT_TOUCHPAD_TYPES_H

/** { input_touchpad.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for INPUT devices on touchpad architectures.
 *  Structures, Enums and Defines being required by ttc_input_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150317 17:35:58 UTC
 *
 *  Note: See ttc_input.h for description of architecture independent INPUT implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "../ttc_touchpad_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_input_types.h *************************


typedef struct {  // touchpad specific configuration data of single INPUT device
    t_ttc_touchpad_config* TouchPad;       // configuration of touchpad device
} __attribute__( ( __packed__ ) ) t_input_touchpad_config;

// t_ttc_input_architecture is required by ttc_input_types.h
#define t_ttc_input_architecture t_input_touchpad_config

//} Structures/ Enums


#endif //INPUT_TOUCHPAD_TYPES_H
