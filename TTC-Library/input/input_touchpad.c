/** { input_touchpad.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for input devices on touchpad architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 22 at 20150317 17:35:58 UTC
 *
 *  Note: See ttc_input.h for description of touchpad independent INPUT implementation.
 *
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "input_touchpad.h".
//
#include "input_touchpad.h"
#include "input_helper.h"
#include "../ttc_gfx.h"
#include "../ttc_touchpad.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

t_ttc_input_config* input_touchpad_get_features( t_ttc_input_config* Config ) {
    Assert_INPUT_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // pointers must not be NULL

    static t_ttc_input_config Features;
    if ( !Features.LogicalIndex ) { // called first time: initialize data

    }
    Features.LogicalIndex = Config->LogicalIndex;

    return ( t_ttc_input_config* ) 0;
}
e_ttc_input_event input_touchpad_check( t_ttc_input_config* Config ) {
    Assert_INPUT_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    e_ttc_touchpad_update TouchpadUpdate = ttc_touchpad_check( Config->LogicalIndex );
    if ( TouchpadUpdate ) {

        e_ttc_input_event InputEvent;
        switch ( TouchpadUpdate ) { // map touchpad update to input event
            case ttu_TouchMove: InputEvent = tie_Drag;      break;
            case ttu_TouchOut:  InputEvent = tie_MouseUp;   break;
            case ttu_TouchIn:   InputEvent = tie_MouseDown; break;
            default:            InputEvent = tie_None;      break;
        }

        if ( InputEvent ) { // update event data in Config

            // remember previous event (usefull for some applications)
            Config->EventPrevious  = Config->Event;
            Config->EventPreviousX = Config->EventX;
            Config->EventPreviousY = Config->EventY;

            // store current event
            Config->Event  = InputEvent;
            Config->EventX = Config->LowLevelConfig->TouchPad->Position_X;
            Config->EventY = Config->LowLevelConfig->TouchPad->Position_Y;

            return InputEvent;
        }
    }

    return tie_None; // nothing happened
}
e_ttc_input_errorcode input_touchpad_deinit( t_ttc_input_config* Config ) {
    Assert_INPUT_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // pointers must not be NULL

    ttc_touchpad_deinit( Config->LogicalIndex );

    return ec_input_OK;
}
e_ttc_input_errorcode input_touchpad_init( t_ttc_input_config* Config ) {
    Assert_INPUT_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // pointers must not be NULL

    t_u8  LogicalIndex = Config->LogicalIndex;
    t_ttc_touchpad_config* Config_TouchPad = ttc_touchpad_get_configuration( LogicalIndex );
    t_ttc_gfx_config*      Config_Gfx      = ttc_gfx_get_configuration( LogicalIndex );

    Assert_INPUT_EXTRA( ttc_memory_is_writable( Config_TouchPad ), ttc_assert_origin_auto );
    Assert_INPUT_EXTRA( ttc_memory_is_writable( Config_Gfx ),      ttc_assert_origin_auto );

    // configure touchpad to match current graphic display
    Config_TouchPad->Max_X = Config_Gfx->Width;
    Config_TouchPad->Max_Y = Config_Gfx->Height;

    if ( !Config->LowLevelConfig )
    { Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_ttc_input_architecture ) ); }
    Assert_INPUT_EXTRA( ttc_memory_is_writable( Config->LowLevelConfig ), ttc_assert_origin_auto );
    Config->LowLevelConfig->TouchPad = Config_TouchPad;

    if ( ttc_touchpad_init( LogicalIndex ) != ec_touchpad_OK )
    { return ec_input_CannotInitializeTouchPad; }

    return ec_input_OK;
}
e_ttc_input_errorcode input_touchpad_load_defaults( t_ttc_input_config* Config ) {
    Assert_INPUT_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // pointers must not be NULL

    return ttc_touchpad_load_defaults( Config->LogicalIndex );
}
void input_touchpad_prepare() {

    //ttc_touchpad_prepare(); touchpad driver will prepare itself
}
void input_touchpad_reset( t_ttc_input_config* Config ) {
    Assert_INPUT_EXTRA( ttc_memory_is_writable( Config ), ttc_assert_origin_auto ); // pointers must not be NULL

    ttc_touchpad_reset( Config->LogicalIndex );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
