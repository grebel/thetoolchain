#ifndef INPUT_TOUCHPAD_H
#define INPUT_TOUCHPAD_H

/** { input_touchpad.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for input devices on touchpad architectures.
 *  Structures, Enums and Defines being required by low-level driver only.
 *
 *  Created from template device_architecture.h revision 22 at 20150317 17:35:58 UTC
 *
 *  Note: See ttc_input.h for description of touchpad independent INPUT implementation.
 *
 *  Authors: Gregor Rebel
 *
 *
 *  How to configure touchpad input devices
 *
 *  This low-level driver requires configuration defines in addtion to the howto section
 *  of ttc_input.h.
 *
 *  Activation of this driver requires existence of any file matching to:
 *    extension.active/ *_touchpad_*
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_INPUT_TOUCHPAD
//
// Implementation status of low-level driver support for input devices on touchpad
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_INPUT_TOUCHPAD '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_INPUT_TOUCHPAD == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_INPUT_TOUCHPAD to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_INPUT_TOUCHPAD

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "input_touchpad.c"
//
#include "input_touchpad_types.h"
#include "../ttc_input_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_input_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_input_foo
//
#define ttc_driver_input_deinit(Config) input_touchpad_deinit(Config)
#define ttc_driver_input_get_features(Config) input_touchpad_get_features(Config)
#define ttc_driver_input_init(Config) input_touchpad_init(Config)
#define ttc_driver_input_load_defaults(Config) input_touchpad_load_defaults(Config)
#define ttc_driver_input_prepare() input_touchpad_prepare()
#define ttc_driver_input_reset(Config) input_touchpad_reset(Config)
#define ttc_driver_input_check(Config) input_touchpad_check(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_input.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_input.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** shutdown single INPUT unit device
 * @param Config        pointer to struct t_ttc_input_config
 * @return              == 0: INPUT has been shutdown successfully; != 0: error-code
 */
e_ttc_input_errorcode input_touchpad_deinit( t_ttc_input_config* Config );


/** fills out given Config with maximum valid values for indexed INPUT
 * @param Config  = pointer to struct t_ttc_input_config
 * @return        pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_input_config* input_touchpad_get_features( t_ttc_input_config* Config );


/** initializes single INPUT unit for operation
 * @param Config        pointer to struct t_ttc_input_config
 * @return              == 0: INPUT has been initialized successfully; != 0: error-code
 */
e_ttc_input_errorcode input_touchpad_init( t_ttc_input_config* Config );


/** loads configuration of indexed INPUT unit with default values
 * @param Config        pointer to struct t_ttc_input_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_input_errorcode input_touchpad_load_defaults( t_ttc_input_config* Config );


/** Prepares input Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void input_touchpad_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_input_config
 */
void input_touchpad_reset( t_ttc_input_config* Config );


/** checks connected device for input events
 *
 * @param Config           pointer to struct t_ttc_input_config
 * @return Config->EventX  x-coordinate of input event
 * @return Config->EventY  y-coordinate of input event
 * @return == tie_None: no event; != tie_None: input event
 */
e_ttc_input_event input_touchpad_check( t_ttc_input_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _input_touchpad_foo(t_ttc_input_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //INPUT_TOUCHPAD_H