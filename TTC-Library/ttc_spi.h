#ifndef TTC_SPI_H
#define TTC_SPI_H
/** { ttc_spi.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for SPI devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_SPI(tc_spi_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_spi_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_spi_init(LogicalIndex);
 *  4) use:         ttc_spi_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level spi and application.
 *
 *  Created from template ttc_device.h revision 27 at 20140423 11:04:14 UTC
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of ttc_spi (Do not delete this line!)
 *
 * Serial Peripheral Interface (SPI) is a universal communication interface being
 * implemented by most microcontrollers.
 *
 * H2: Main Features of SPI:
 * - A microcontroller typically provides multiple SPI busses
 * - Single master and multiple slaves on same SPI bus
 * - Fast communication speeds (typically 2 Mbit/s)
 * - One slave select (SS) line for each slave device
 * - Few communication lines (MISO, MOSI, SCLK, SS, GND)
 * - Simple configuration
 * - Only short distances (typically only on same printed circuit board)
 * - Reading data from slave requires that master sends out zeroes
 * - Microcontroller can act as master or slave device (not both on same bus!)
 *
 * H2: Special ttc_spi features
 * - Multiple spi bus units
 * - Multiple slaves per spi bus
 * - Each slave requires individual slave select pin configuration TTC_SPI<n>_NSS<M>
 *
 * H2: Using ttc_spi as Master
 *
 * - Send data to slave (->ttc_spi_master_send() )
 * - Read data from slave (->ttc_spi_master_read() )
 * - Send and read data to and from slave (->ttc_spi_master_send_read() )
 * - Static configuration of TTC_SPI<n> devices
 *   TTC_SPI<n>        Index of physical device to use for SPI bus #<n> (one from e_ttc_physical_index)
 *                     Many microcontrollers provide multiple internal SPI units.
 *                     The pcb designer decides which spi units are connected to external connectors or
 *                     other devices. Therefore, TTC_SPI<n> must not always be defined as <n>-1.
 *   TTC_SPI<n>_NSS<m> Defines one gpio pin for each slave device connected to TTC_SPI<n>.
 *                     For each configured TTC_SPI<n> device, at least TTC_SPI<n>_NSS1 must be defined!
 *
 * The configuration of SPI devices is divided into static and dynamic configuration
 *
 * H3: Static SPI Configuration
 *   The static TTC_SPI configuration typically takes place in a board makefile.
 *   Attributes:
 *     <n>  Logical index of spi device (=1..10)
 *     <m>  Logical index of slave chip connected to a spi device (=1..10)
 *
 *   TTC_SPI<n>=<m>            Defines the physical spi device to be connected to logical SPI device #<n>.
 *                             <m> can be one from e_ttc_physical_index as defined in ttc_basic_types.h
 *   TTC_SPI<n>_MOSI=<gpio>    Defines gpio pin to use as Master Out Slave In line for logical SPI device #<n>.
 *                             <gpio> can be one from e_ttc_gpio_pin as defined in ttc_gpio_types.h.
 *   TTC_SPI<n>_MISO=<gpio>    Defines gpio pin to use as Master In Slave Out line for logical SPI device #<n>.
 *                             <gpio> can be one from e_ttc_gpio_pin as defined in ttc_gpio_types.h.
 *   TTC_SPI<n>_SCK=<gpio>     Defines gpio pin to use as Serial Clock line for logical SPI device #<n>.
 *                             <gpio> can be one from e_ttc_gpio_pin as defined in ttc_gpio_types.h.
 *   TTC_SPI<n>_NSS<m>=<gpio>  Defines gpio pin to use as Slave Select #<m> line for logical SPI device #<n>.
 *                             <gpio> can be one from e_ttc_gpio_pin as defined in ttc_gpio_types.h.
 *                             If this pin is given, it will be controlled by ttc_spi automatically.
 *                             This can be unwanted if more than one slave is connected or if the slave
 *                             requires constant low NSS line during transmissions. If E_ttc_gpio_pin_none is given
 *                             for <gpio> then the application has to initialize and control this line.
 *
 *   Example Static SPI Configuration taken from a board makefile
     COMPILE_OPTS += -DTTC_SPI1=ttc_device_1   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
     COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a7    # GPIO-Pin used for Master Out Slave In
     COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a6    # GPIO-Pin used for Master In Slave Out
     COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_a5     # GPIO-Pin used for Serial Clock
     COMPILE_OPTS += -DTTC_SPI1_NSS1=E_ttc_gpio_pin_b11   # slave #1 is selected via GPIOB pin 11
     COMPILE_OPTS += -DTTC_SPI1_NSS2=E_ttc_gpio_pin_c3    # slave #2 is selected via GPIOC pin 3
 *
 *   Note: The current static configuration is copied into dynamic configuration data and can be changed before initialization.
 *
 * H3: Dynamic Configuration
 *   Before calling ttc_spi_init(), call ttc_spi_get_configuration() and modify the returned configuration to suit your needs.
 *
 *   Example Dynamic SPI Configuration
     // Load configuration of first logical spi interface
     t_ttc_spi_config * ConfigSPI = ttc_spi_get_configuration(1);
     ConfigSPI->Flags.Bits.Master            = 1;        // use master mode
     ConfigSPI->Flags.Bits.Slave             = 0;        // not using slave mode
     ConfigSPI->Flags.Bits.WordSize16        = 0;        // ==0: select 8 bits
     ConfigSPI->Flags.Bits.FirstBitMSB       = 1;        // bit order
     ConfigSPI->Flags.Bits.ClockIdleHigh     = 1;        // behaviour of SCK
     ConfigSPI->Flags.Bits.ClockPhase2ndEdge = 0;        // behaviour of SCK
     e_ttc_spi_errorcode Error = ttc_spi_init(ConfigSPI->LogicalIndex);

     // Check if interface has been initialized successfully
     Assert_SPI(!Error, ec_spi_unknown);

 *
 * H2: Using ttc_spi as Slave (ToDo)
}*/

#ifndef EXTENSION_ttc_spi
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_spi.sh
#endif

//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "ttc_interrupt.h" // initialisation uses critical sections
#include "interfaces/ttc_spi_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level spi only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * spi devices on all supported architectures.
 * Check spi/spi_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares spi Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_spi_prepare();
void _driver_spi_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_spi_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_SPI_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_spi_config* ttc_spi_get_configuration( t_u8 LogicalIndex );

/** initialize indexed SPI device before using it
 *
 * See description above to learn more about how to configure and initialize ttc_spi !
 *
 * @param LogicalIndex  logical index of spi device. Each logical device <n> is defined via TTC_SPI<n>* constants in compile_options.h and extensions.active/makefile
 * @param mode            == 1: Mode Master; == 0: Mode Slave
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_spi_errorcode ttc_spi_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_SPI_get_max_LogicalIndex())
 */
void ttc_spi_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Seetings of indexed device are being reset to their default values.
 * Will automatically call ttc_spi_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of spi device (1..ttc_spi_get_max_index() )
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_spi_errorcode  ttc_spi_load_defaults( t_u8 LogicalIndex );


/** Send out given amount of raw bytes from buffer.
 *
 * This function will pull corresponding slave select pin low for complete operation if slave index is given.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * Note: Stable operation may required to disable multitasking before (ttc_task_critical_begin(); ... ttc_spi_master_send(); ... ttc_task_critical_end();)
 *
 * Usage Example:
 * 1) Send out single byte to slave #1 via SPI device #1
 *    const t_u8 Byte = 9;
 *    ttc_spi_master_send(1, 1, &Byte, 1);
 *
 * 2) Send 10 bytes from BufferTx[] to slave #3 via SPI device #2
 *    t_u8 Buffer[10] = { 1,2,3,4,5,6,7,8,9,10 }; // allocate + load transmit buffer
 *    ttc_spi_master_send(2, 3, Buffer, 10);
 *
 * 3) Send data from multiple buffers to slave #3 via SPI device #2 and handle slave select manually
 *    Assert(ttc_spi_get_configuration(1)->Init.Pins_NSS_Amount >= 3, ttc_assert_origin_auto); // slave #3 not defined!
 *    e_ttc_gpio_pin SlaveSelect = ttc_spi_get_configuration(1)->Init.Pins_NSS[3];
 *    BufferTx1[5] = { 1,2,3,4,5 }; // allocate + load transmit buffer 1
 *    BufferTx2[3] = { 1,2,3 };     // allocate + load transmit buffer 1
 *    ttc_gpio_clr(SlaveSelect); // select slave by pulling line low
 *    ttc_spi_master_send(2, 0, BufferTx1, 5); // IndexSlave == 0: ttc_spi will not handle slave select pin
 *    ttc_spi_master_send(2, 0, BufferTx2, 3); // IndexSlave == 0: ttc_spi will not handle slave select pin
 *    ttc_gpio_set(SlaveSelect); // deselect slave by pushing line high
 *
 * @param IndexSPI    device index of SPI to init (1..ttc_spi_get_max_index() )
 * @param IndexSlave  >0: index of slave select pin to use (1=TTC_SPI<n>_NSS1, 2=TTC_SPI<n>_NSS2, ...); ==0: no handling of slave select pin
 * @param BufferTx    memory location from which to read data
 * @param Amount      amount of bytes to send from Buffer[]
 * @return            == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_spi_errorcode ttc_spi_master_send( t_u8 IndexSPI, t_u8 IndexSlave, const t_u8* BufferTx, t_u16 AmountTx );

/** Combined data sending and receiving as SPI-master
 *
 * An SPI master always receives one byte of data when it sends one byte to a slave.
 * This combined send+read function allows to send data from given buffer to slave and to store received data
 * in another buffer. BufferTx[] and BufferRx[] must not be of same size.
 *
 * This function will pull corresponding slave select pin low for complete operation if slave index is given.
 *
 * ttc_spi_master_send_read() operates in three phases.
 *   BufferTx)  Send data from BufferTx[]
 *   ZeroBytes) Send value of Config.Init.SendAsZero given times
 *   BufferRx)  Store received bytes in BufferRx[]
 *
 *   Phase ZeroBytes must not exist. If it exists, it runs after phase BufferTx.
 *   Phase BufferRx runs in parallel to phase BufferTx and phase ZeroBytes and ends with phase ZeroBytes.
 *   <---- BufferTx -------><----- ZeroBytes ----->
 *             <--------------- BufferRx --------->
 *
 * Usage Examples:
 * 1) Send 2-byte command to slave and then read 6 bytes of data from slave (BufferTx and BufferRx one after another)
 *    t_u8 BufferTx[2] = { ... }; // load transmit buffer
 *    t_u8 BufferRx[6];           // allocate receive buffer
 *    ttc_spi_master_send_read(1, 1, BufferTx, 2, 6, BufferRx, 6); // AmountZerosTx = size of BufferRx[] = 6
 *
 * 2) Send 10 bytes of data to slave and store all bytes being received
 *    t_u8 BufferTx[10] = { ... }; // load transmit buffer
 *    t_u8 BufferRx[10];           // allocate receive buffer
 *    ttc_spi_master_send_read(1, 1, BufferTx, 10, 0, BufferRx, 10); // AmountZerosTx = 0 (not sending any zero)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * Note: Stable operation may required to disable multitasking before (ttc_task_critical_begin(); ... ttc_spi_master_send_read(); ... ttc_task_critical_end();)
 *
 * @param IndexSPI       device index of SPI to init (1..ttc_spi_get_max_index() )
 * @param IndexSlave     >0: index of slave select pin to use (1=TTC_SPI<n>_NSS1, 2=TTC_SPI<n>_NSS2, ...); ==0: no handling of slave select pin
 * @param Config         pointer to struct t_ttc_spi_config
 * @param BufferTx       memory location from which to read data
 * @param AmountTx       amount of bytes to send from BufferTx[]
 * @param AmountZerosTx  amount of zero bytes to send after sending BufferTx[]
 * @param BufferRx       memory location where to store received data
 * @param AmountRx       amount of bytes to store in BufferRx[]
 * @param Amount         amount of bytes to send from Buffer[]
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_spi_errorcode ttc_spi_master_send_read( t_u8 IndexSPI, t_u8 IndexSlave, const t_u8* BufferTx, t_u16 AmountTx, t_u16 AmountZerosTx, t_u8* BufferRx, t_u16 AmountRx );
e_ttc_spi_errorcode _driver_spi_send_read( t_ttc_spi_config* Config, const t_u8* BufferTx, t_u16 AmountTx, t_u16 AmountZerosTx, t_u8* BufferRx, t_u16 AmountRx );

/** Reads given amount of bytes from slave as SPI-master
 *
 * Will automatically send out one zero byte for every byte to read from slave.
 * Will use 16 bit wide transfers if configured and available in current low-level driver.
 *
 * This function will pull corresponding slave select pin low for complete operation if slave index is given.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 * Note: Stable operation may required to disable multitasking before (ttc_task_critical_begin(); ... ttc_spi_master_read(); ... ttc_task_critical_end();)
 *
 * @param IndexSPI      device index of SPI to init (1..ttc_spi_get_max_index() )
 * @param IndexSlave    >0: index of slave select pin to use (1=TTC_SPI<n>_NSS1, 2=TTC_SPI<n>_NSS2, ...); ==0: no handling of slave select pin
 * @param Config        pointer to struct t_ttc_spi_config
 * @param BufferRx      memory location where to store received data
 * @param AmountRx      amount of bytes to read from slave
 * @return              amount of bytes being read
 */
t_u16 ttc_spi_master_read( t_u8 IndexSPI, t_u8 IndexSlave, t_u8* BufferRx, t_u16 AmountRx );
t_u16 _driver_spi_read_raw( t_ttc_spi_config* Config, t_u8* BufferRx, t_u16 AmountRx );

/** maps from logical to physical device index
 *
 * High-level spis (ttc_spi_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_SPIn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any
 * order.
 *
 * @param LogicalIndex  logical index of spi device. Each logical device <n> is defined via TTC_SPI<n>* constants in compile_options.h and extensions.active/makefile
 * @return              physical index of spi device (0 = first physical spi device, ...)
 */
t_u8 ttc_spi_logical_2_physical_index( t_u8 LogicalIndex );

/** maps from physical to logical device index
 *
 * This is a reverse function to ttc_spi_logical_2_physical_index()
 *
 * @param  PhysicalIndex  physical index of spi device (0 = first physical spi device, ...)
 * @return                logical index of spi device (1..ttc_spi_get_max_index() )
 */
t_u8 ttc_spi_physical_2_logical_index( t_u8 PhysicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * Note: Calling this function does nothing, if ttc_spi_init() has not been called for this index before.
 *
 * @param LogicalIndex  logical index of spi device. Each logical device <n> is defined via TTC_SPI<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: USART has been reset successfully; != 0: error-code
 */
e_ttc_spi_errorcode  ttc_spi_reset( t_u8 LogicalIndex );

/** Delivers gpio pin used to select indexed slave device.
 *
 * The SPI bus requires one slave select line for each slave device connected to the bus.
 * ttc_spi manages all slave selects automatically. This serves most use cases.
 * Special protocols may require that the application handles the slave select line on its own.
 *
 * Usage Example:
 *   e_ttc_gpio_pin PinNSS = ttc_spi_get_pin_slave_select(1,2); // slave select #2 of spi device #1
 *   t_u8 Buffer1[7] = { 1,2,3,4,5,6,7 };
 *   t_u8 Buffer2[9] = { 1,2,3,4,5,6,7,8,9 };
 *
 *   ttc_gpio_clr(PinNSS);                  // select slave
 *   ttc_spi_master_send(1, 0, Buffer1, 7); // IndexSlave == 0: ttc_spi will not handle slave select line
 *   ttc_spi_master_send(1, 0, Buffer1, 9); // IndexSlave == 0: ttc_spi will not handle slave select line
 *   ttc_gpio_set(PinNSS);                  // deselect slave
 *
 * @param IndexSPI   (t_u8)           device index of SPI to init (1..ttc_spi_get_max_index() )
 * @param IndexSlave (t_u8)           >0: index of slave select pin to use (1=TTC_SPI<n>_NSS1, 2=TTC_SPI<n>_NSS2, ...); ==0: no handling of slave select pin
 * @return           (e_ttc_gpio_pin) gpio pin connected to slave select input of corresponding slave device
 */
e_ttc_gpio_pin ttc_spi_get_pin_slave_select( t_u8 IndexSPI, t_u8 IndexSlave );

/** Send out data to another spi master (current uC is operating as spi slave)
 *
 * @param IndexSPI (t_u8)         logical index of spi device. Each logical device <n> is defined via TTC_SPI<n>* constants in compile_options.h and extensions.active/makefile
 * @param BufferTx (const t_u8*)  start of buffer to send data from
 * @param AmountTx (t_u16)        amount of bytes to send from BufferTx[]
 * @return         (e_ttc_spi_errorcode)   ==0: operation finished successfully; error-code otherwise
 */
e_ttc_spi_errorcode ttc_spi_slave_send( t_u8 IndexSPI, const t_u8* BufferTx, t_u16 AmountTx );

/** Read data from another spi master (current uC is operating as spi slave)
 *
 * @param IndexSPI (t_u8)   logical index of spi device. Each logical device <n> is defined via TTC_SPI<n>* constants in compile_options.h and extensions.active/makefile
 * @param BufferRx (t_u8*)  start of buffer where to store incoming bytes
 * @param AmountRx (t_u16)  amount of bytes to read into BufferRx[]
 * @return         (e_ttc_spi_errorcode)   ==0: operation finished successfully; error-code otherwise
 */
e_ttc_spi_errorcode ttc_spi_slave_read( t_u8 IndexSPI, t_u8* BufferRx, t_u16 AmountRx );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_spi(t_u8 LogicalIndex)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_heap_ are passed to interfaces/ttc_heap_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl heap UPDATE
 */


/** fills out given Config with maximum valid values for indexed SPI
 * @param Config        = pointer to struct t_ttc_spi_config
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_spi_errorcode _driver_spi_get_features( t_ttc_spi_config* Config );

/** shutdown single SPI unit device
 * @param Config        pointer to struct t_ttc_spi_config
 * @return              == 0: SPI has been shutdown successfully; != 0: error-code
 */
void _driver_spi_deinit( t_ttc_spi_config* Config );

/** initializes single SPI unit for operation
 * @param Config        pointer to struct t_ttc_spi_config
 * @return              == 0: SPI has been initialized successfully; != 0: error-code
 */
e_ttc_spi_errorcode _driver_spi_init( t_ttc_spi_config* Config );

/** loads configuration of indexed SPI unit with default values
 *
 * Note: Pin configuration has already been set from TTC_SPIx_* constants but may be reset/ checked by low-level driver
 *
 * @param Config        pointer to struct t_ttc_spi_config
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_spi_errorcode _driver_spi_load_defaults( t_ttc_spi_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_spi_config
 */
e_ttc_spi_errorcode _driver_spi_reset( t_ttc_spi_config* Config );

/** Send out given amount of raw bytes from buffer.
 * Will not interpret bytes in buffer.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param Config        pointer to struct t_ttc_spi_config
 * @param Buffer        memory location from which to read data
 * @param Amount        amount of bytes to send from Buffer[]
 * @return              == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_spi_errorcode _driver_spi_send_raw( t_ttc_spi_config* Config, const t_u8* Buffer, t_u16 Amount );

/** Send out given bytes from buffer until first zero byte or MaxLength reached.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param Config        pointer to struct t_ttc_spi_config
 * @param Buffer        memory location from which to read data
 * @param MaxLength     no more than this amount of bytes will be send
 * @return              == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_spi_errorcode _driver_spi_send_string( t_ttc_spi_config* Config, const char* Buffer, t_u16 MaxLength );

/** Send out given data word (8 or 9 bits)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param Config        pointer to struct t_ttc_spi_config
 * @param Word          16 bits value to send
 * @return              == 0: Buffer has been sent successfully; != 0: error-code
 */
void _driver_spi_send_word( t_ttc_spi_config* Config, const t_u16 Word );

/** Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param Config        pointer to struct t_ttc_spi_config
 * @param Word          pointer to 16 bit buffer where to store Word
 * @return              == 0: Word has been read successfully; != 0: error-code
 */
void _driver_spi_read_word( t_ttc_spi_config* Config, t_u16* Word );

/** Reads single data byte from input buffer.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param Config        pointer to struct t_ttc_spi_config
 * @param Byte          pointer to 8 bit buffer where to store Byte
 * @return              == 0: Word has been read successfully; != 0: error-code
 */
void _driver_spi_read_byte( t_ttc_spi_config* Config, t_u8* Byte );

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_SPI_H
