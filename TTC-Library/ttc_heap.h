#ifndef TTC_HEAP_H
#define TTC_HEAP_H
/** { ttc_heap.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for HEAP devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_HEAP(tc_heap_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_heap_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_heap_init(LogicalIndex);
 *  4) use:         ttc_heap_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level heap and application.
 *
 *  Created from template ttc_device.h revision 25 at 20140301 07:28:17 UTC
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of ttc_heap (Do not delete this line!)
 *
 * ttc_heap provides a dynamic memory allocator with selectable implementation.
 * The idea of dynamic memory is to use all non static allocated memory as one big heap.
 * When application wants to allocate additional memory during runtime, it can get it from
 * this heap. The amount to be allocated can be determined at runtime, which makes it more
 * flexible than static memory.
 *
 * H2: Dynamic memory also has drawbacks:
 * - A dynamic allocation may fail (if not egnough heap is left).
 *   So your application must be able to handle this situation.
 * - Dynamic allocation makes your application slower.
 *   You have to call functions to obtain dynamic memory instead of allocating it statically.
 * - A memory heap always requires extra RAM for management overhead.
 *
 * H2: Different low-level drivers for ttc_heap provide different features
 * - heap_zdefault
 *   The most simple ttc_heap driver can only allocate memory. This makes it the
 *   fastest of all thinkable drivers. It also has the smallest management overhead.
 * - heap_freertos
 *   This is just a wrapper to use the heap driver provided by FreeRTOS.
 *   FreeRTOS itself provides different heap drivers. See FreeRTOS documentation for details.
 *   And you need to activate the FreeRTOS extension too!
 *
 * H2: Memory Blocks
 * Instead of plain, unstructured chunks of memory, the special ttc_heap_alloc_block() function
 * can allocate a memory block with some extra data. Memory blocks are a pretty old TTC feature which might
 * get deprecated some time. See ttc_heap_pool*() functions as a replacement!
 *
 * H2_ Memory Pools
 * The real work horse of dynamic memories are memory pools. Each memory pool is a set of equal sized
 * pool blocks. Each pool block is preceeded by a data header:
 * - A universal next pointer
 *   This allows to form single linked lists using ttc_list.
 * - A pointer to the pool to which this block belongs too
 *   This allows any function or task that finally has processed a given pool block to return the block
 *   to its pool for reuse.
 *
 * Pool blocks are the key communication vehicle between tasks and between interrupt service routines and
 * the main application. A typical scenario looks like this:
 * 1) An event occurs and is first handled by a task or an interrupt service routine.
 *    An event handler has very little time to react on incoing events. So it will
 *    a) obtain an empty block from a pool (->ttc_heap_pool_block_get() )
 *    b) fill the pool block with all data of the event
 *    c) push the pool block to a list of blocks to be processed later (->ttc_list_push_back_single() )
 * 2) The main event loop checks one or more lists. If a block is found in such a list, it
 *    a) pops the block from the list (->ttc_list_pop_front_single_try() )
 *    b) processes the event
 *    c) returns the pool block back to its pool for reuse (->ttc_heap_pool_block_free() )
}*/

#ifndef EXTENSION_ttc_heap
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_heap.sh
#endif

//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_heap_interface.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

#ifdef malloc
    #error malloc from stdlib is not supported, use ttc_heap_alloc() instead!
#endif

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

// constants defined by linker script (configs/stm32f1xx.ld, configs/stm32w1xx.ld, ...)
extern volatile t_u8 _ttc_heap_start; //TTC heap start address defined in linker script (e.g. memory_stm32f1xx.ld)
extern volatile t_u8 _ttc_heap_end;   //TTC heap start address defined in linker script (e.g. memory_stm32f1xx.ld)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level heap only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * heap devices on all supported architectures.
 * Check heap/heap_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares heap Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_heap_prepare();

/** Tries to allocate a memory area of given size
 *
 * Note: For many setups, the allocated memory block cannot be freed!
 * Note: All ttc_malloc() functions will call ttc_assert_halt_origin(ttc_assert_origin_auto) if allocation fails.
 *
 * @param Size   size of memory block to allocate
 * @return       =! NULL: address of allocated memory block; == NULL: memory could not be allocated (out of memory)
 */
void* ttc_heap_alloc( t_base Size );
void* _driver_heap_alloc( t_base Size );

/** Provides releasable dynamic memory for a short time frame.
 *
 * This is lightweight function that only calculates start of next memory chunk
 * and checks if egnough heap memory is available. No real allocation takes place which makes
 * allocation and release faster than ttc_heap_alloc().
 * The idea is that the caller "knows" that no one else wants to allocate memory before the
 * temporary chunk is given back.
 * Such a memory chunk works like a local variable with the advantage of a dynamic size.
 * After usage, the buffer can just be released without memory fragmentation.
 *
 * Pros
 * - dynamic size allocation
 * - very fast
 * - releasable
 * - no memory fragmentation
 *
 * Cons
 * - no other memory allocation allowed during use
 *
 * Note: Allocated memory must be given back befor any other ttc_heap_alloc*() call using ttc_heap_release_temporary()!
 * Note: You may want to disable multitasking to prevent other tasks from heap allocations.
 *
 * Example usage:
 *     ttc_task_critical_begin();
 *     void* Temporary = ttc_heap_temporary_alloc(SomeSize);
 *     ...use Temporary[]..
 *     ttc_heap_temporary_release(SomeSize);
 *     ttc_task_critical_end();
 *
 * @param Size   size of memory block to allocate
 * @return       =! NULL: address of allocated memory block; == NULL: memory could not be allocated (out of memory)
 */
void* ttc_heap_temporary_alloc( t_base Size );
void* _driver_heap_temporary_alloc( t_base Size );

/** Gives back temporary memory being allocated by ttc_heap_temporary_alloc() before
 *
 *
 * @param Size   same size as given to ttc_heap_temporary_alloc() before
 */
void ttc_heap_temporary_release( t_base Size );

/** tries to allocate a memory area of given size and fills it with zero-bytes.
 * Note: For many setups, the allocated memory block cannot be freed!
 * Note: All ttc_malloc() functions will call ttc_assert_halt_origin(ttc_assert_origin_auto) if allocation fails.
 *
 * @param Size   size of memory block to allocate
 * @return       =! NULL: address of allocated memory block; == NULL: memory could not be allocated (out of memory)
 */
void* ttc_heap_alloc_zeroed( t_base Size );
void* _driver_heap_alloc_zeroed( t_base Size );

/** frees given memory area
 * Note: For many setups, ttc_free() does not provide an implementation!
 *
 * @param Block  address of memory block to free (must be allocated by ttc_heap_alloc() before!)
 * @return       ==NULL: block has been freed; ==Block: block has not been freed
 */
void* ttc_heap_free( void* Block );
void* _driver_heap_free( void* Block );
#define ttc_heap_free(Block) _driver_heap_free(Block)

/** returns amount of available dynamic memory
 *
 * @return       amount of bytes available for allocation
 */
t_base ttc_heap_get_free_size();
t_base _driver_heap_get_free_size();
#define ttc_heap_get_free_size() _driver_heap_get_free_size()

//{ Memory Blocks *********************************************************

/** Concept of memory blocks
 *
 * Unified datatype
 * Memory blocks are accepted by all ttc_XXX communication interfaces.
 *
 * Zero copy strategy
 * Every ttc-communication interface receives data into memory blocks of type t_ttc_heap_block_from_pool.
 * These memory blocks can be passed by reference to any other ttc-communication interface or
 * a compatible application
 *
 * Automatic release
 * Each memory block Block stores a function pointer Block->releaseBuffer().
 * After processing a memory block, ttc_heap_block_release(Block)-call has to be called to reduce Block->UseCount.
 * If Block->UseCount==0 then Block->releaseBuffer(Block) is called automatically to return
 * the memory block to its originator. The processor of a memory block must not know, who has initially created it.
 *
 * Memory blocks can travel freely between several tasks. Each blocks stores a ReturnQueue-handle.
 *
 * Scenario
 * If two tasks A and B want to communicate with each other. A creates data, puts it into buffers
 * and sends them to B for processing. After the processing, the memory of the buffers has to
 * return to A. If not, task B would eat up all dynamic memory and after a while, the whole
 * application stops because A cannot allocate more memory.
 *
 * In desktop applications, this problem is often avoided by sending a copy of all data inside the
 * memory block to task B. The big disadvantage of this approach is, that copying of data is very time consuming and
 * often not less memory intensive.
 *
 * The concept of memory blocks provides a simple, yet safe mechanism to recycle memory.
 * For this to work, task A first creates a ReturnQueue Q of pointers to memory blocks.
 * Whenever A wants to use a memory block, it first tries to pull one from Q.
 * If Q is empty, A allocates a new memory block and stores Q inside the block header.
 * When it sends the Pointer P to this t_ttc_heap_block to task B, Q is passed inside the
 * memory block too. After task B has finished processing data in the memory block, it simply
 * pushes P onto Q.
 */

/** allocates a memory block that can store given AmountBytes of bytes + some extra data.
 *
 * Note: All ttc_malloc() functions will call ttc_assert_halt_origin(ttc_assert_origin_auto) if allocation fails.
 *
 * @param Size           AmountBytes of bytes to be allocated for Buffer[]
 * @param Hint           argument passed to releaseBuffer() to allow fast release of buffers from different pools by single function
 * @param releaseBuffer  pointer to function that will take back ownership of block (Note: releaseBuffer() must check if (ttc_interrupt_critical_level()>0) and use _isr() functions if true! )
 * @return               points to head of memory block struct. User data is part of this. (DO NOT CAST TO SOMETHING ELSE!)
 */
t_ttc_heap_block* ttc_heap_alloc_block( t_base Size, t_u8 Hint, void ( *releaseBuffer )( t_ttc_heap_block* Block ) );

/** A virtual memory block is just a header without a buffer. Instead its Buffer-element points to a buffer somewhere else.
 * Virtual buffers can use existing buffers or even constant text in flash memory (if architecture supports pointers to flash memory)
 *
 * Note: All ttc_malloc() functions will call ttc_assert_halt_origin(ttc_assert_origin_auto) if allocation fails.
 *
 * @param Buffer         pointer to buffer to use for this memory block
 * @param Hint           argument passed to releaseBuffer() to allow fast release of buffers from different pools by single function
 * @param releaseBuffer  pointer to function that will take back ownership of block
 * @return               points to head of memory block struct. User data is part of this. (DO NOT CAST TO SOMETHING ELSE!)
 */
t_ttc_heap_block* ttc_heap_alloc_virtual_block( t_u8* Buffer, t_u8 Hint, void ( *releaseBuffer )( t_ttc_heap_block* Block ) );

/** Increases UseCounter of given memory block by 1
  *
  * You want to increase the Use-Counter whenever you want to give a memory block to more than one user.
  * (e.g. a second thread that works on the same block)
  * A block is released back only when UseCounter reaches 0 again.
  *
  * @param Block  address of memory block to release (must be allocated by ttc_heap_alloc() before!)
  */
void ttc_heap_block_use( t_ttc_heap_block* Block );

/** Decreases UseCount of given memory block by 1; if (UseCount==0) it is returned back to its origin by calling stored release function
 *
 * @param Block  address of memory block to release (must be allocated by ttc_heap_alloc() before!)
 */
void ttc_heap_block_release( t_ttc_heap_block* Block );

/** registers given data for debugging purposes
  *
  * Note: low-level heap allocator must call this function to register each new allocated block!
  *
  * @param Address  start address of allocated block
  * @param Size     size of allocated block (bytes)
  */
void ttc_heap_register_block( void* Address, t_base Size );

/** removes registration data of a memory block
  *
  * Note: low-level heap allocator must call this function for each new freed block!
  *
  * @param Address  start address of allocated block
  * @param Size     size of allocated block (bytes)
  */
void ttc_heap_unregister_block( void* Address, t_base Size );

//}Memory Blocks
//{ Memory Pools *********************************************************

/** Concept of memory pools
 *
 * Asynchronous communication between tasks requires one or more memory blocks to be allocated and reused.
 * The required amount of memory blocks is often not known at compile time. Incoming data often occurs
 * in bursts.
 * A memory pool provides an automatic block management. New memory blocks are allocated if required.
 * Processed memory blocks have to be released for later reuse.
 *
 * For access from interrupt service routines, special functions are provided.
 */

/** tries to allocate a new memory pool that will manage memory blocks of same size
 *
 * @param BufferSize    payload size of each memory block in this pool (size of Buffer[] element)
 * @param MaxAllocated     !=-1: maximum allowed amount of blocks to be allocated by this pool
 *                      ==-1: amount of allocatable blocks only limited by memory heap
 * @return              =! NULL: address of allocated memory pool; == NULL: memory pool could not be created (out of memory)
 */
t_ttc_heap_pool* ttc_heap_pool_create( TTC_MEMORY_BLOCK_BASE BufferSize, t_base MaxAllocated );

/** Initialize given memory for use as a memory pool
 *
 * @param Pool          memory to use as pool management data
 * @param BufferSize    payload size of each memory block in this pool (size of Buffer[] element)
 * @param AmountBlocks  amount of memory blocks to be allocated for this pool
 * @return              =! NULL: address of allocated memory pool; == NULL: memory pool could not be created (out of memory)
 */
void ttc_heap_pool_init( t_ttc_heap_pool* Pool, TTC_MEMORY_BLOCK_BASE Size, t_base AmountBlocks );

/** Increase amount of allocated blocks of given memory pool
 *
 * After initialization, the amount of blocks managed by a pool can be increased at any time while
 * egnough heap space is available.
 *
 * Note: If a more complex heap driver than heap_zdefault is used, increasing a pool several times
 *       may introduce memory management overhead over initializing it to final size once.
 *
 * @param Pool             (t_ttc_heap_pool*)  memory pool being created or initialized before
 * @param AmountAdditional (t_base)            amount of memory blocks to allocate and add to the pool
 * @return                 (t_base)            amount of memory blocks being added (may be less than required if heap is exhausted)
 */
t_base ttc_heap_pool_increase( t_ttc_heap_pool* Pool, t_base AmountAdditional );

/** helper function to display given pool in GDB session
 *
 *
 * @param Pool          pointer to memory pool to debug
 *
 * Example usage:
 *   (gdb) p *ttc_heap_pool_debug(MyPool)
arg 0 in r0 = 0x2000066c
$1 = {
  Pool = {
    BlocksAvailable = {
      Semaphore = {
        Value = 0x2,
        PadByte1 = 0x0,
        PadByte2 = 0x0,
        PadByte3 = 0x0
      },
      Owner = 0x800d2b1 <ttc_heap_pool_block_get_isr+252>
    },
    Lock = {
      Mutex = 0x1,
      PadByte1 = 0x0,
      PadByte2 = 0x0,
      PadByte3 = 0x0,
      LastCaller = 0x800cfbf <ttc_heap_pool_block_free+90>,
      OwnerTask = 0x0
    },
    FirstFree = 0x20000864,
    BlockSize = 0x93,
    FirstFree_FromISR = 0x0,
    Statistics = {
      AmountBlockGet = 0x0,
      AmountBlockFree = 0xf,
      AmountBlockFreeISR = 0x0,
      AmountBlockGetISR = 0x11,
      AmountBlockGetISR_Failed = 0x2,
      AmountFreeCurrently = 0x2,
      AmountGrabbedFromISR = 0x0,
      AmountSpecialFreedFromISR = 0x0,
      AmountAllocated = 0x4
    }
  },
  List_Size = 0x3,
  FreeBlocks_Size = 0xa,
  FreeBlocks = {{
      Address = 0x20000864,
      Data = {
        ListItem = {
          Next = 0x20000904
        },
        Pool = 0x2000066c,
        MagicKey = 0xc001babe
      }
    }, {
      Address = 0x20000904,
      Data = {
        ListItem = {
          Next = 0x20000a44
        },
        Pool = 0x2000066c,
        MagicKey = 0xc001babe
      }
    }, {
      Address = 0x20000a44,
      Data = {
        ListItem = {
          Next = 0x0
        },
        Pool = 0x2000066c,
        MagicKey = 0xc001babe
      }
    }, {
      Address = 0x0,
      Data = {
        ListItem = {
          Next = 0x0
        },
        Pool = 0x0,
        MagicKey = 0x0
      }
 */
t_ttc_heap_pool_debug* ttc_heap_pool_debug( t_ttc_heap_pool* Pool );

/** returns list-item embedded into given memory block for use with ttc_list.c functions
 *
 * Note: you may define TTC_HEAP_MAGICKEY to enable extra validity checks
 *
 * @param Block     !=NULL: pointer to a pool memory block as returned by ttc_heap_pool_block_get() before
 *                  ==NULL: simply returns NULL
 * @return          pointer to a list-item for use with ttc_list.c functions
 */
t_ttc_list_item* ttc_heap_pool_to_list_item( t_ttc_heap_block_from_pool* Block );

/** converts list-item returned from ttc_heap_pool_to_list_item() back to pool memory
 *
 * Note: you may define TTC_HEAP_MAGICKEY to enable extra validity checks
 *
 * @param Item      !=NULL: list-item that has been derived from a pool memory block via ttc_heap_pool_to_list_item() before
 *                  ==NULL: simply returns NULL
 * @return          pointer to restored pool memory block
 */
t_ttc_heap_block_from_pool* ttc_heap_pool_from_list_item( t_ttc_list_item* Item );

/** allocates a new/ reuses a released memory block from given memory pool
 *
 * Note: This function will block endlessly if pool is empty!
 * Note: This function is to be used from tasks and not from interrupt service routines!
 * Note: Returned address points to allocated memory directly AFTER header struct and can be directly casted to something else
 *
 * @param Pool      memory pool created via ttc_heap_pool_create()
 * @return          =! NULL: address of newly allocated or reused  memory block without header
 *                  == NULL: memory pool could not provide another memory block (blocks limited/ out of memory)
 */
t_ttc_heap_block_from_pool* ttc_heap_pool_block_get( t_ttc_heap_pool* Pool );

/** allocates a new/ reuses a released memory block from given memory pool
 *
 * Note: This function will return immediately if pool is empty!
 * Note: This function is to be used from tasks and not from interrupt service routines!
 * Note: Returned address points to allocated memory directly AFTER header struct and can be directly casted to something else
 *
 * @param Pool      memory pool created via ttc_heap_pool_create()
 * @return          =! NULL: address of newly allocated or reused  memory block without header
 *                  == NULL: memory pool could not provide another memory block (blocks limited/ out of memory)
 */
t_ttc_heap_block_from_pool* ttc_heap_pool_block_get_try( t_ttc_heap_pool* Pool );

/** allocates a new/ reuses a released memory block from given memory pool
 *
 * Note: This function is to be used from interrupt service routines or with disabled interrupts. It will never block.
 * Note: Returned address points to allocated memory directly AFTER header struct and can be directly casted to something else
 * Note: Make sure that no second interrupt service routine with higher priority operates on same mutex!
 *
 * @param Pool      memory pool created via ttc_heap_pool_create()
 * @return          =! NULL: address of newly allocated or reused  memory block without header
 *                  == NULL: memory pool could not provide another memory block (blocks limited/ out of memory)
 */
t_ttc_heap_block_from_pool* ttc_heap_pool_block_get_isr( t_ttc_heap_pool* Pool );

/** returns a memory block back to its memory pool for later reuse
 *
 * Note: Do not access data inside Block after calling this function!
 * Note: This function is to be used from tasks and not from interrupt service routines!
 *
 * @param Pool      memory pool created via ttc_heap_pool_create()
 * @param Block     memory block that has been got from same memory pool Pool
 */
void ttc_heap_pool_block_free( void* Block );

/** returns a memory block back to its memory pool for later reuse (use from interrupt service routine)
 *
 * Note: This function is to be used from interrupt service routines and not from tasks!
 * Note: Make sure that no second interrupt service routine with higher priority operates on same mutex!
 *
 * @param Block     memory block that has been got from same memory pool Pool
 */
void ttc_heap_pool_block_free_isr( void* Block );

//}Memory Pools


//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_heap(t_u8 LogicalIndex)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_heap_ are passed to interfaces/ttc_heap_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl heap UPDATE
 */

/** Prepares heap driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 *
 * @param HeapStart  points to lowest address of memory block to be used as heap
 * @param HeapSize   amount of bytes to be used for heap
 */
void _driver_heap_prepare( volatile t_u8* HeapStart, t_base HeapSize );

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_HEAP_H
