/*{ ttc_input.c *******************************



}*/

#include "DEPRECATED_ttc_input.h"

//{ Global Variables *****************************************************

// single linked list of active input areas
tit_InputArea_t* InputAreas_Used    = NULL;

tit_InputArea_t* InputArea_Current  = NULL;
tit_MouseEvent_e Global_Mouse_Event = me_noEvent;
ttc_input_generic_t Pen_Point;
ttc_heap_pool_t* InputMemory_block = NULL;

// for each initialized input, a pointer to its generic and architecture definitions is stored
A_define(ttc_input_generic_t*, ttc_INPUTs, TTC_AMOUNT_INPUTS);

// logical index of display to use by ttc_input_stdout_send_string()/ ttc_input_stdout_send_block()
u8_t ttc_input_stdout_index = 0;

// updated by _ttc_input_update_configuration()
ttc_input_generic_t* ttc_input_CurrentInput = NULL;

//} Global Variables

// This driver requires multitasking support!
#ifndef TTC_MULTITASKING_SCHEDULER
#  error Missing multitasking scheduler: Activate a scheduler in rank 300!
#endif

// functions that can be provided by low-level driver (set during ttc_gfx_reset() )
BOOL (*function_read_position)(volatile u16_t *x, volatile u16_t *y);

//{ Function definitions *************************************************

                u8_t ttc_input_get_max_display_index() {
    return TTC_AMOUNT_INPUTS;
}
ttc_input_generic_t* ttc_input_get_configuration(u8_t DisplayIndex) {
    _ttc_input_update_configuration(DisplayIndex);

    return ttc_input_CurrentInput;
}
                void ttc_input_init(u8_t DisplayIndex) {

    _ttc_input_update_configuration(DisplayIndex);

    switch (ttc_input_CurrentInput->Driver) {
#ifdef EXTENSION_400_input_ads7843
    case tid_ADS7843: input_ads7843_init(ttc_input_CurrentInput); break;
#endif
#ifdef EXTENSION_400_input_analog
    case tid_Analog: input_analog_init(ttc_input_CurrentInput); break;
#endif
    default: break;
    }
    InputArea_Current = InputAreas_Used;

    function_read_position = ttc_input_CurrentInput->function_read_position;
}
                void ttc_input_start(u8_t DisplayIndex) {
    ttc_input_init(DisplayIndex);

    InputMemory_block = ttc_heap_pool_create(sizeof(tit_InputArea_t),20);
    ttc_heap_pool_init(InputMemory_block,sizeof(tit_InputArea_t),20);

    ttc_task_create(ttc_input_maintask,
                    "Input",
                    256, // ToDo: minimize stack size
                    NULL,
                    2,
                    NULL);
}
                void ttc_input_maintask(void* TaskArgument) {
    (void) TaskArgument;


    while (1) {
        // only one task of this function will be created, so we can declare all local variables as static and reduce stack usage
        static u16_t ti_X = 0;
        static u16_t ti_Y = 0;
        static tit_InputArea_t* ti_InputArea = NULL;

        ttc_task_msleep(300); //?

        if (InputArea_Current->CurrentEvent==me_MouseUp)
        {
            InputArea_Current->CurrentEvent=me_noEvent;
            Global_Mouse_Event=me_noEvent;
        }
        if ((InputArea_Current->CurrentEvent==me_MouseMove)||InputArea_Current->CurrentEvent==me_MouseDown)
        {
            InputArea_Current->CurrentEvent=me_MouseUp;
            Global_Mouse_Event=me_MouseUp;
        }

        while (function_read_position(&ti_X,&ti_Y)) { //get Touchpanel info, if touchpanel active, go into while loop
            ti_X=0;
            ti_Y=0;
            ttc_task_msleep(5);
            if (!function_read_position(&ti_X,&ti_Y)) break;
            for (ti_InputArea = InputAreas_Used; ti_InputArea; ti_InputArea = ti_InputArea->Next)
            { //check every input area until active input area is empty
                if (ti_InputArea->Left<ti_X&&ti_InputArea->Right>ti_X&&ti_InputArea->Top<ti_Y&&ti_InputArea->Bottom>ti_Y)  //check : fit any input area?
                {
                    InputArea_Current = ti_InputArea;

                    if (InputArea_Current->CurrentEvent==me_MouseDown)
                    {
                        InputArea_Current->CurrentEvent=me_MouseMove;
                        Global_Mouse_Event=me_MouseMove;
                    }
                    if (InputArea_Current->CurrentEvent==me_noEvent)
                    {
                        InputArea_Current->CurrentEvent=me_MouseDown;
                        Global_Mouse_Event=me_MouseDown;
                    }
                    if (InputArea_Current->CurrentEvent==InputArea_Current->Event)
                    {
                        InputArea_Current->TouchHandler(InputArea_Current->Argument);

                        if (InputArea_Current->Event==me_MouseDown)
                            Global_Mouse_Event=me_noEvent;
                    }

                    ti_X=0;
                    ti_Y=0;
                    break;
                }
            }
        }

        if ((InputArea_Current->CurrentEvent==InputArea_Current->Event)&&(InputArea_Current->CurrentEvent==me_MouseUp))
            InputArea_Current->TouchHandler(InputArea_Current->Argument);
    }
}
                void ttc_input_single_check_all(void* pvParamters)
{
    u16_t X,Y;
    ttc_task_msleep(500);

    if (InputArea_Current->CurrentEvent==me_MouseUp)
    {
        InputArea_Current->CurrentEvent=me_noEvent;
        Global_Mouse_Event=me_noEvent;
    }
    if ((InputArea_Current->CurrentEvent==me_MouseMove)||InputArea_Current->CurrentEvent==me_MouseDown)
    {
        InputArea_Current->CurrentEvent=me_MouseUp;
        Global_Mouse_Event=me_MouseUp;
    }

    while (function_read_position(&X,&Y)) { //get Touchpanel info, if touchpanel activ, go into while loop

        X=0;
        Y=0;
        ttc_task_msleep(5);
        if (!function_read_position(&X,&Y)) break;


        for (tit_InputArea_t* InputArea = InputAreas_Used ; InputArea ;InputArea= InputArea->Next) //check every input area until activ input area is empty
        {
            if (InputArea->Left<X&&InputArea->Right>X&&InputArea->Top<Y&&InputArea->Bottom>Y)  //check : fit any input area?
            {
                InputArea_Current = InputArea;

                if (InputArea_Current->CurrentEvent==me_MouseDown)
                {
                    InputArea_Current->CurrentEvent=me_MouseMove;
                    Global_Mouse_Event=me_MouseMove;
                }
                if (InputArea_Current->CurrentEvent==me_noEvent)
                {
                    InputArea_Current->CurrentEvent=me_MouseDown;
                    Global_Mouse_Event=me_MouseDown;
                }
                if (InputArea_Current->CurrentEvent==InputArea_Current->Event)
                {
                    InputArea_Current->TouchHandler(InputArea_Current->Argument);

                    if (InputArea_Current->Event==me_MouseDown)
                        Global_Mouse_Event=me_noEvent;
                }

                X=0;
                Y=0;
                break;
            }
        }

    }

    if ((InputArea_Current->CurrentEvent==InputArea_Current->Event)&&(InputArea_Current->CurrentEvent==me_MouseUp))
        InputArea_Current->TouchHandler(InputArea_Current->Argument);

}
                void ttc_input_register_input_area(void (*TouchHandler)(void* Argument), void* Argument, u16_t Left, u16_t Top, u16_t Right, u16_t Bottom, const char* Text, tit_InputAreaShape_e Shape, tit_MouseEvent_e Event) {
    tit_InputArea_t* Input_Data;

    if (!InputAreas_Used) //if Input area list is empty
    {
        Input_Data = InputAreas_Used = (tit_InputArea_t*) ttc_heap_pool_block_get(InputMemory_block); //the first pointer get the address of allocated memory
        Input_Data->TouchHandler=TouchHandler;
        Input_Data->Argument=Argument;
        Input_Data->Bottom=Bottom;
        Input_Data->Left=Left;
        Input_Data->Right=Right;
        Input_Data->Top=Top;
        Input_Data->Text=Text;
        Input_Data->Shape=Shape;
        Input_Data->Event=Event;
        Input_Data->Next=0;
    }
    else //if there is already a first pointer
    {
        Input_Data = InputAreas_Used; //hand the adress over to Input_Data
        while(Input_Data->Next)
            Input_Data=Input_Data->Next; //while there is a next pointer, Input_Data become the next pointer

        //at the end of the list
        Input_Data->Next = (tit_InputArea_t*) ttc_heap_pool_block_get(InputMemory_block);
        Input_Data = Input_Data->Next;
        Input_Data->TouchHandler=TouchHandler;
        Input_Data->Argument=Argument;
        Input_Data->Bottom=Bottom;
        Input_Data->Left=Left;
        Input_Data->Right=Right;
        Input_Data->Top=Top;
        Input_Data->Text=Text;
        Input_Data->Shape=Shape;
        Input_Data->Event=Event;
        Input_Data->CurrentEvent=Global_Mouse_Event;
        Input_Data->Next=0;
    }
}
                bool ttc_input_check_touch(tit_InputArea_t* InputData) {
    u16_t X,Y,sumX,sumY;
    sumX = 0; sumY = 0;

    function_read_position(&X,&Y);
    if (InputData->Left<X&&InputData->Right>X&&InputData->Top<Y&&InputData->Bottom>Y) {
        ttc_task_msleep(2);
        function_read_position(&X,&Y);
        if (InputData->Left<X&&InputData->Right>X&&InputData->Top<Y&&InputData->Bottom>Y) {
            for (u8_t i=0;i<10;i++)
            {
                function_read_position(&X,&Y);
                sumX=(X+sumX)/2;
                sumY=(Y+sumY)/2;
                ttc_task_msleep(2);
            }
            if (InputData->Left<sumX&&InputData->Right>sumX&&InputData->Top<sumY&&InputData->Bottom>sumY) {
                InputData->TouchHandler(InputData);
                return 1;
            }
        }
    }

    return 0;
}
                void ttc_input_delete_all_input_areas() {
    tit_InputArea_t* InputArea = InputAreas_Used;
    while(InputArea)
    {
        InputAreas_Used=InputArea->Next;
        ttc_heap_pool_block_free((ttc_heap_block_from_pool_t*) InputArea);
        InputArea=InputAreas_Used;
    }

    return;

}
                void ttc_input_delete_input_area(tit_InputArea_t* DeleteArea) {

    if (!DeleteArea)
        return;

    tit_InputArea_t* InputArea = InputAreas_Used;

    while ( InputArea && (InputArea->Next != DeleteArea) ) {
        InputArea = InputArea->Next;
    }

    if ( InputArea &&
         (InputArea->Next == DeleteArea)
         )
        InputArea->Next = DeleteArea->Next;
}
                void ttc_input_reset(u8_t DisplayIndex) {
    ttc_memory_set( ttc_input_CurrentInput, 0, sizeof(ttc_input_generic_t) );

    // load generic default values
    ttc_input_CurrentInput->LogicalIndex = DisplayIndex;

    switch (DisplayIndex) { // load settings from makefile
#ifdef TTC_INPUT1_DRIVER
    case 1: {
        ttc_input_CurrentInput->Driver = TTC_INPUT1_DRIVER;
#ifdef TTC_INPUT1_WIDTH
        ttc_input_CurrentInput->Width =  TTC_INPUT1_WIDTH;
#endif
#ifdef TTC_INPUT1_HEIGHT
        ttc_input_CurrentInput->Height = TTC_INPUT1_HEIGHT;
#endif
#ifdef TTC_INPUT1_DEPTH
        ttc_input_CurrentInput->Depth =  TTC_INPUT1_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_INPUT2_DRIVER
    case 2: {
        ttc_input_CurrentInput->Driver = TTC_INPUT2_DRIVER;
#ifdef TTC_INPUT2_WIDTH
        ttc_input_CurrentInput->Width = TTC_INPUT2_WIDTH;
#endif
#ifdef TTC_INPUT2_HEIGHT
        ttc_input_CurrentInput->Height = TTC_INPUT2_HEIGHT;
#endif
#ifdef TTC_INPUT2_DEPTH
        ttc_input_CurrentInput->Depth = TTC_INPUT2_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_INPUT3_DRIVER
    case 3: {
        ttc_input_CurrentInput->Driver = TTC_INPUT3_DRIVER;
#ifdef TTC_INPUT3_WIDTH
        ttc_input_CurrentInput->Width = TTC_INPUT3_WIDTH;
#endif
#ifdef TTC_INPUT3_HEIGHT
        ttc_input_CurrentInput->Height = TTC_INPUT3_HEIGHT;
#endif
#ifdef TTC_INPUT3_DEPTH
        ttc_input_CurrentInput->Depth = TTC_INPUT3_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_INPUT4_DRIVER
    case 4: {
        ttc_input_CurrentInput->Driver = TTC_INPUT4_DRIVER:
        #ifdef TTC_INPUT4_WIDTH
                ttc_input_CurrentInput->Width = TTC_INPUT4_WIDTH;
#endif
#ifdef TTC_INPUT4_HEIGHT
        ttc_input_CurrentInput->Height = TTC_INPUT4_HEIGHT;
#endif
#ifdef TTC_INPUT4_DEPTH
        ttc_input_CurrentInput->Depth = TTC_INPUT4_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_INPUT5_DRIVER
    case 5: {
        ttc_input_CurrentInput->Driver = TTC_INPUT5_DRIVER;
#ifdef TTC_INPUT5_WIDTH
        ttc_input_CurrentInput->Width = TTC_INPUT5_WIDTH;
#endif
#ifdef TTC_INPUT5_HEIGHT
        ttc_input_CurrentInput->Height = TTC_INPUT5_HEIGHT;
#endif
#ifdef TTC_INPUT5_DEPTH
        ttc_input_CurrentInput->Depth = TTC_INPUT5_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_INPUT6_DRIVER
    case 6: {
        ttc_input_CurrentInput->Driver = TTC_INPUT6_DRIVER;
#ifdef TTC_INPUT6_WIDTH
        ttc_input_CurrentInput->Width = TTC_INPUT6_WIDTH;
#endif
#ifdef TTC_INPUT6_HEIGHT
        ttc_input_CurrentInput->Height = TTC_INPUT6_HEIGHT;
#endif
#ifdef TTC_INPUT6_DEPTH
        ttc_input_CurrentInput->Depth = TTC_INPUT6_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_INPUT7_DRIVER
    case 7: {
        ttc_input_CurrentInput->Driver = TTC_INPUT7_DRIVER;
#ifdef TTC_INPUT7_WIDTH
        ttc_input_CurrentInput->Width = TTC_INPUT7_WIDTH;
#endif
#ifdef TTC_INPUT7_HEIGHT
        ttc_input_CurrentInput->Height = TTC_INPUT7_HEIGHT;
#endif
#ifdef TTC_INPUT7_DEPTH
        ttc_input_CurrentInput->Depth = TTC_INPUT7_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_INPUT8_DRIVER
    case 8: {
        ttc_input_CurrentInput->Driver = TTC_INPUT8_DRIVER;
#ifdef TTC_INPUT8_WIDTH
        ttc_input_CurrentInput->Width = TTC_INPUT8_WIDTH;
#endif
#ifdef TTC_INPUT8_HEIGHT
        ttc_input_CurrentInput->Height = TTC_INPUT8_HEIGHT;
#endif
#ifdef TTC_INPUT8_DEPTH
        ttc_input_CurrentInput->Depth = TTC_INPUT8_DEPTH;
#endif
        break;
    }
#endif
#ifdef TTC_INPUT9_DRIVER
    case 9: {
        ttc_input_CurrentInput->Driver = TTC_INPUT9_DRIVER;
#ifdef TTC_INPUT9_WIDTH
        ttc_input_CurrentInput->Width = TTC_INPUT9_WIDTH;
#endif
#ifdef TTC_INPUT9_HEIGHT
        ttc_input_CurrentInput->Height = TTC_INPUT9_HEIGHT;
#endif
#ifdef TTC_INPUT9_DEPTH
        ttc_input_CurrentInput->Depth = TTC_INPUT9_DEPTH;
#endif
        break;
    }
#endif

    }


    switch (ttc_input_CurrentInput->Driver) { // load rest of configuration from low-level driver
#ifdef EXTENSION_400_input_ads7843
    case tid_ADS7843:
        input_ads7843_get_configuration(ttc_input_CurrentInput);
        break;
#endif
#ifdef EXTENSION_400_input_analog
    case tid_Analog:
        input_analog_get_configuration(ttc_input_CurrentInput);
        break;
#endif
    default: Assert_Halt_EC(ec_NotImplemented); break; // no matching input driver found: activate an input driver
    }
}
                void _ttc_input_update_configuration(u8_t DisplayIndex) {

    Assert_INPUT(DisplayIndex > 0, ec_InvalidArgument);
    ttc_input_CurrentInput = A(ttc_INPUTs, DisplayIndex-1); // will assert if outside array bounds

    if (!ttc_input_CurrentInput) {
        ttc_input_CurrentInput = A(ttc_INPUTs, DisplayIndex-1) = ttc_heap_alloc_zeroed(sizeof(ttc_input_generic_t));
        ttc_input_reset(DisplayIndex);
    }

}

//} Function definitions
