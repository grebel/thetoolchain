/** { ttc_adc.c ****************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for adc devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported
 *  architectures.
 *
 *  Created from template ttc_device.c revision 27 at 20141008 14:25:12 UTC
 *
 *  Authors: Gregor Rebel, Victor Fuentes
 *
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_adc.h".
//
#include "ttc_adc.h"
#include "ttc_list.h"
#include "ttc_gpio.h"
#include "ttc_heap.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes


/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of adc devices.
 *
 */


// for each initialized device, a pointer to its generic and stm32f1xx definitions is stored
A_define( t_ttc_adc_config*, ttc_adc_configs, TTC_ADC_MAX_AMOUNT );

// Highest valid logical index (can increase when dynamic analog inputs are configured)
t_u8 ttc_adc_LastValidIndex = TTC_ADC_AMOUNT_STATIC;

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8 ttc_adc_get_max_index() {
    return ( t_u8 ) TTC_ADC_AMOUNT_STATIC;
}
t_ttc_adc_config* ttc_adc_get_configuration( t_u8 LogicalIndex ) {
    Assert_ADC( LogicalIndex, ttc_assert_origin_auto );                    // logical index starts at 1
    Assert_ADC( LogicalIndex < TTC_ADC_MAX_AMOUNT, ttc_assert_origin_auto ); // logical index starts at 1

    t_ttc_adc_config* Config = A( ttc_adc_configs, LogicalIndex - 1 ); // will assert if outside array bounds

    if ( !Config ) {
        Config = A( ttc_adc_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_adc_config ) );
        Config->Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_adc_load_defaults( LogicalIndex );
    }

    return Config;
}
t_ttc_adc_config* ttc_adc_find_configuration_by_pin( e_ttc_gpio_pin InputPin ) {
    Assert_ADC( InputPin, ttc_assert_origin_auto ); // invalid argument

    t_ttc_adc_config* Config = NULL;
    t_ttc_adc_config* Search;
    t_u8 LogicalIndex = 1;
    for ( ; LogicalIndex < ttc_adc_LastValidIndex; LogicalIndex++ ) {
        // we try not to create unused configuration entries
        Search = A( ttc_adc_configs, LogicalIndex - 1 );
        if ( Search ) { // entry already exists: compare its input pin
            if ( Search->InputPin == InputPin ) { // corresponding config found: break
                Config = Search;
                break;
            }
        }
        else {         // empty entry: check static configuration
            t_u8 NewLogicalIndex = 0;
            switch ( LogicalIndex ) {
#ifdef TTC_ADC1
                case 1: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC1 )
                    { NewLogicalIndex = 1; }    // static configuration TTC_ADC1 matches: remember its index
                    break;
                }
#else
#  warning TTC_ADC: No analog input pin defined. Did you forget to add a "COMPILE_OPTS += -DTTC_ADC1=E_ttc_gpio_pin_XX" line to your makefile?
#endif
#ifdef TTC_ADC2
                case 2: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC2 )
                    { NewLogicalIndex = 2; }    // static configuration TTC_ADC2 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC3
                case 3: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC3 )
                    { NewLogicalIndex = 3; }    // static configuration TTC_ADC3 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC4
                case 4: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC4 )
                    { NewLogicalIndex = 4; }    // static configuration TTC_ADC4 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC5
                case 5: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC5 )
                    { NewLogicalIndex = 5; }    // static configuration TTC_ADC5 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC6
                case 6: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC6 )
                    { NewLogicalIndex = 6; }    // static configuration TTC_ADC6 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC7
                case 7: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC7 )
                    { NewLogicalIndex = 7; }    // static configuration TTC_ADC7 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC8
                case 8: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC8 )
                    { NewLogicalIndex = 8; }    // static configuration TTC_ADC8 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC9
                case 9: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC9 )
                    { NewLogicalIndex = 9; }    // static configuration TTC_ADC9 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC10
                case 10: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC10 )
                    { NewLogicalIndex = 10; }    // static configuration TTC_ADC10 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC11
                case 11: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC11 )
                    { NewLogicalIndex = 11; }    // static configuration TTC_ADC11 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC12
                case 12: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC12 )
                    { NewLogicalIndex = 12; }    // static configuration TTC_ADC12 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC13
                case 13: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC13 )
                    { NewLogicalIndex = 13; }    // static configuration TTC_ADC13 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC14
                case 14: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC14 )
                    { NewLogicalIndex = 14; }    // static configuration TTC_ADC14 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC15
                case 15: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC15 )
                    { NewLogicalIndex = 15; }    // static configuration TTC_ADC15 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC16
                case 16: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC16 )
                    { NewLogicalIndex = 16; }    // static configuration TTC_ADC16 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC17
                case 17: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC17 )
                    { NewLogicalIndex = 17; }    // static configuration TTC_ADC17 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC18
                case 18: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC18 )
                    { NewLogicalIndex = 18; }    // static configuration TTC_ADC18 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC19
                case 19: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC19 )
                    { NewLogicalIndex = 19; }    // static configuration TTC_ADC19 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC20
                case 20: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC20 )
                    { NewLogicalIndex = 20; }    // static configuration TTC_ADC20 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC21
                case 21: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC21 )
                    { NewLogicalIndex = 21; }    // static configuration TTC_ADC21 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC22
                case 22: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC22 )
                    { NewLogicalIndex = 22; }    // static configuration TTC_ADC22 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC23
                case 23: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC23 )
                    { NewLogicalIndex = 23; }    // static configuration TTC_ADC23 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC24
                case 24: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC24 )
                    { NewLogicalIndex = 24; }    // static configuration TTC_ADC24 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC25
                case 25: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC25 )
                    { NewLogicalIndex = 25; }    // static configuration TTC_ADC25 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC26
                case 26: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC26 )
                    { NewLogicalIndex = 26; }    // static configuration TTC_ADC26 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC27
                case 27: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC27 )
                    { NewLogicalIndex = 27; }    // static configuration TTC_ADC27 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC28
                case 28: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC28 )
                    { NewLogicalIndex = 28; }    // static configuration TTC_ADC28 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC29
                case 29: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC29 )
                    { NewLogicalIndex = 29; }    // static configuration TTC_ADC29 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC30
                case 30: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC30 )
                    { NewLogicalIndex = 30; }    // static configuration TTC_ADC30 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC31
                case 31: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC31 )
                    { NewLogicalIndex = 31; }    // static configuration TTC_ADC31 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC32
                case 32: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC32 )
                    { NewLogicalIndex = 32; }    // static configuration TTC_ADC32 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC33
                case 33: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC33 )
                    { NewLogicalIndex = 33; }    // static configuration TTC_ADC33 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC34
                case 34: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC34 )
                    { NewLogicalIndex = 34; }    // static configuration TTC_ADC34 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC35
                case 35: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC35 )
                    { NewLogicalIndex = 35; }    // static configuration TTC_ADC35 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC36
                case 36: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC36 )
                    { NewLogicalIndex = 36; }    // static configuration TTC_ADC36 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC37
                case 37: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC37 )
                    { NewLogicalIndex = 37; }    // static configuration TTC_ADC37 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC38
                case 38: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC38 )
                    { NewLogicalIndex = 38; }    // static configuration TTC_ADC38 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC39
                case 39: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC39 )
                    { NewLogicalIndex = 39; }    // static configuration TTC_ADC39 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC40
                case 40: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC40 )
                    { NewLogicalIndex = 40; }    // static configuration TTC_ADC40 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC41
                case 41: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC41 )
                    { NewLogicalIndex = 41; }    // static configuration TTC_ADC41 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC42
                case 42: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC42 )
                    { NewLogicalIndex = 42; }    // static configuration TTC_ADC42 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC43
                case 43: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC43 )
                    { NewLogicalIndex = 43; }    // static configuration TTC_ADC43 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC44
                case 44: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC44 )
                    { NewLogicalIndex = 44; }    // static configuration TTC_ADC44 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC45
                case 45: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC45 )
                    { NewLogicalIndex = 45; }    // static configuration TTC_ADC45 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC46
                case 46: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC46 )
                    { NewLogicalIndex = 46; }    // static configuration TTC_ADC46 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC47
                case 47: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC47 )
                    { NewLogicalIndex = 47; }    // static configuration TTC_ADC47 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC48
                case 48: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC48 )
                    { NewLogicalIndex = 48; }    // static configuration TTC_ADC48 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC49
                case 49: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC49 )
                    { NewLogicalIndex = 49; }    // static configuration TTC_ADC49 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC50
                case 50: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC50 )
                    { NewLogicalIndex = 50; }    // static configuration TTC_ADC50 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC51
                case 51: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC51 )
                    { NewLogicalIndex = 51; }    // static configuration TTC_ADC51 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC52
                case 52: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC52 )
                    { NewLogicalIndex = 52; }    // static configuration TTC_ADC52 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC53
                case 53: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC53 )
                    { NewLogicalIndex = 53; }    // static configuration TTC_ADC53 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC54
                case 54: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC54 )
                    { NewLogicalIndex = 54; }    // static configuration TTC_ADC54 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC55
                case 55: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC55 )
                    { NewLogicalIndex = 55; }    // static configuration TTC_ADC55 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC56
                case 56: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC56 )
                    { NewLogicalIndex = 56; }    // static configuration TTC_ADC56 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC57
                case 57: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC57 )
                    { NewLogicalIndex = 57; }    // static configuration TTC_ADC57 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC58
                case 58: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC58 )
                    { NewLogicalIndex = 58; }    // static configuration TTC_ADC58 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC59
                case 59: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC59 )
                    { NewLogicalIndex = 59; }    // static configuration TTC_ADC59 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC60
                case 60: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC60 )
                    { NewLogicalIndex = 60; }    // static configuration TTC_ADC60 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC61
                case 61: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC61 )
                    { NewLogicalIndex = 61; }    // static configuration TTC_ADC61 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC62
                case 62: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC62 )
                    { NewLogicalIndex = 62; }    // static configuration TTC_ADC62 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC63
                case 63: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC63 )
                    { NewLogicalIndex = 63; }    // static configuration TTC_ADC63 matches: remember its index
                    break;
                }
#endif
#ifdef TTC_ADC64
                case 64: { // check static ADC pin configuration
                    if ( InputPin == TTC_ADC64 )
                    { NewLogicalIndex = 64; }    // static configuration TTC_ADC64 matches: remember its index
                    break;
                }
#endif
                default: break; // no analog input configured for this pin
            }

            if ( NewLogicalIndex ) { // matching static configuration found: create new configuration entry
                Config = ttc_adc_get_configuration( NewLogicalIndex );
            }
        }
    }
    if ( !Config ) { // no static or existing dynamic configuration found: create new dynamic configuration
        LogicalIndex = ++ttc_adc_LastValidIndex;
        Assert_ADC( LogicalIndex < TTC_ADC_MAX_AMOUNT, ttc_assert_origin_auto ); // logical index starts at 1

        Config = A( ttc_adc_configs, LogicalIndex - 1 ) = ttc_heap_alloc_zeroed( sizeof( t_ttc_adc_config ) );
        Config->InputPin = InputPin;
        Config->LogicalIndex = LogicalIndex;
        e_ttc_adc_errorcode Error = _driver_adc_find_input_pin( Config );
        ttc_adc_load_defaults( LogicalIndex );
        Assert_ADC( !Error, ttc_assert_origin_auto ); // given pin cannot be used as analog input on current architecture: Check your configuration!
    }

    return Config;
}
void ttc_adc_deinit( t_u8 LogicalIndex ) {
    Assert_ADC( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_adc_config* Config = ttc_adc_get_configuration( LogicalIndex );

    e_ttc_adc_errorcode Result = _driver_adc_deinit( Config );
    if ( Result == ec_adc_OK )
    { Config->Flags.Bits.Initialized = 0; }
}
e_ttc_adc_errorcode ttc_adc_init( t_u8 LogicalIndex ) {
    Assert_ADC( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_adc_config* Config = ttc_adc_get_configuration( LogicalIndex );

    if ( !Config->InputPin ) { // no input pin configured up to now: read from static configuration
        switch ( LogicalIndex ) {
#ifdef TTC_ADC1
            case 1: { // load config for ADC input
                Config->InputPin = TTC_ADC1;
                break;
            }
#endif
#ifdef TTC_ADC2
            case 2: { // load config for ADC input
                Config->InputPin = TTC_ADC2;
                break;
            }
#endif
#ifdef TTC_ADC3
            case 3: { // load config for ADC input
                Config->InputPin = TTC_ADC3;
                break;
            }
#endif
#ifdef TTC_ADC4
            case 4: { // load config for ADC input
                Config->InputPin = TTC_ADC4;
                break;
            }
#endif
#ifdef TTC_ADC5
            case 5: { // load config for ADC input
                Config->InputPin = TTC_ADC5;
                break;
            }
#endif
#ifdef TTC_ADC6
            case 6: { // load config for ADC input
                Config->InputPin = TTC_ADC6;
                break;
            }
#endif
#ifdef TTC_ADC7
            case 7: { // load config for ADC input
                Config->InputPin = TTC_ADC7;
                break;
            }
#endif
#ifdef TTC_ADC8
            case 8: { // load config for ADC input
                Config->InputPin = TTC_ADC8;
                break;
            }
#endif
#ifdef TTC_ADC9
            case 9: { // load config for ADC input
                Config->InputPin = TTC_ADC9;
                break;
            }
#endif
#ifdef TTC_ADC10
            case 10: { // load config for ADC input
                Config->InputPin = TTC_ADC10;
                break;
            }
#endif
#ifdef TTC_ADC11
            case 11: { // load config for ADC input
                Config->InputPin = TTC_ADC11;
                break;
            }
#endif
#ifdef TTC_ADC12
            case 12: { // load config for ADC input
                Config->InputPin = TTC_ADC12;
                break;
            }
#endif
#ifdef TTC_ADC13
            case 13: { // load config for ADC input
                Config->InputPin = TTC_ADC13;
                break;
            }
#endif
#ifdef TTC_ADC14
            case 14: { // load config for ADC input
                Config->InputPin = TTC_ADC14;
                break;
            }
#endif
#ifdef TTC_ADC15
            case 15: { // load config for ADC input
                Config->InputPin = TTC_ADC15;
                break;
            }
#endif
#ifdef TTC_ADC16
            case 16: { // load config for ADC input
                Config->InputPin = TTC_ADC16;
                break;
            }
#endif
#ifdef TTC_ADC17
            case 17: { // load config for ADC input
                Config->InputPin = TTC_ADC17;
                break;
            }
#endif
#ifdef TTC_ADC18
            case 18: { // load config for ADC input
                Config->InputPin = TTC_ADC18;
                break;
            }
#endif
#ifdef TTC_ADC19
            case 19: { // load config for ADC input
                Config->InputPin = TTC_ADC19;
                break;
            }
#endif
#ifdef TTC_ADC20
            case 20: { // load config for ADC input
                Config->InputPin = TTC_ADC20;
                break;
            }
#endif
#ifdef TTC_ADC21
            case 21: { // load config for ADC input
                Config->InputPin = TTC_ADC21;
                break;
            }
#endif
#ifdef TTC_ADC22
            case 22: { // load config for ADC input
                Config->InputPin = TTC_ADC22;
                break;
            }
#endif
#ifdef TTC_ADC23
            case 23: { // load config for ADC input
                Config->InputPin = TTC_ADC23;
                break;
            }
#endif
#ifdef TTC_ADC24
            case 24: { // load config for ADC input
                Config->InputPin = TTC_ADC24;
                break;
            }
#endif
#ifdef TTC_ADC25
            case 25: { // load config for ADC input
                Config->InputPin = TTC_ADC25;
                break;
            }
#endif
#ifdef TTC_ADC26
            case 26: { // load config for ADC input
                Config->InputPin = TTC_ADC26;
                break;
            }
#endif
#ifdef TTC_ADC27
            case 27: { // load config for ADC input
                Config->InputPin = TTC_ADC27;
                break;
            }
#endif
#ifdef TTC_ADC28
            case 28: { // load config for ADC input
                Config->InputPin = TTC_ADC28;
                break;
            }
#endif
#ifdef TTC_ADC29
            case 29: { // load config for ADC input
                Config->InputPin = TTC_ADC29;
                break;
            }
#endif
#ifdef TTC_ADC30
            case 30: { // load config for ADC input
                Config->InputPin = TTC_ADC30;
                break;
            }
#endif
#ifdef TTC_ADC31
            case 31: { // load config for ADC input
                Config->InputPin = TTC_ADC31;
                break;
            }
#endif
#ifdef TTC_ADC32
            case 32: { // load config for ADC input
                Config->InputPin = TTC_ADC32;
                break;
            }
#endif
#ifdef TTC_ADC33
            case 33: { // load config for ADC input
                Config->InputPin = TTC_ADC33;
                break;
            }
#endif
#ifdef TTC_ADC34
            case 34: { // load config for ADC input
                Config->InputPin = TTC_ADC34;
                break;
            }
#endif
#ifdef TTC_ADC35
            case 35: { // load config for ADC input
                Config->InputPin = TTC_ADC35;
                break;
            }
#endif
#ifdef TTC_ADC36
            case 36: { // load config for ADC input
                Config->InputPin = TTC_ADC36;
                break;
            }
#endif
#ifdef TTC_ADC37
            case 37: { // load config for ADC input
                Config->InputPin = TTC_ADC37;
                break;
            }
#endif
#ifdef TTC_ADC38
            case 38: { // load config for ADC input
                Config->InputPin = TTC_ADC38;
                break;
            }
#endif
#ifdef TTC_ADC39
            case 39: { // load config for ADC input
                Config->InputPin = TTC_ADC39;
                break;
            }
#endif
#ifdef TTC_ADC40
            case 40: { // load config for ADC input
                Config->InputPin = TTC_ADC40;
                break;
            }
#endif
#ifdef TTC_ADC41
            case 41: { // load config for ADC input
                Config->InputPin = TTC_ADC41;
                break;
            }
#endif
#ifdef TTC_ADC42
            case 42: { // load config for ADC input
                Config->InputPin = TTC_ADC42;
                break;
            }
#endif
#ifdef TTC_ADC43
            case 43: { // load config for ADC input
                Config->InputPin = TTC_ADC43;
                break;
            }
#endif
#ifdef TTC_ADC44
            case 44: { // load config for ADC input
                Config->InputPin = TTC_ADC44;
                break;
            }
#endif
#ifdef TTC_ADC45
            case 45: { // load config for ADC input
                Config->InputPin = TTC_ADC45;
                break;
            }
#endif
#ifdef TTC_ADC46
            case 46: { // load config for ADC input
                Config->InputPin = TTC_ADC46;
                break;
            }
#endif
#ifdef TTC_ADC47
            case 47: { // load config for ADC input
                Config->InputPin = TTC_ADC47;
                break;
            }
#endif
#ifdef TTC_ADC48
            case 48: { // load config for ADC input
                Config->InputPin = TTC_ADC48;
                break;
            }
#endif
#ifdef TTC_ADC49
            case 49: { // load config for ADC input
                Config->InputPin = TTC_ADC49;
                break;
            }
#endif
#ifdef TTC_ADC50
            case 50: { // load config for ADC input
                Config->InputPin = TTC_ADC50;
                break;
            }
#endif
#ifdef TTC_ADC51
            case 51: { // load config for ADC input
                Config->InputPin = TTC_ADC51;
                break;
            }
#endif
#ifdef TTC_ADC52
            case 52: { // load config for ADC input
                Config->InputPin = TTC_ADC52;
                break;
            }
#endif
#ifdef TTC_ADC53
            case 53: { // load config for ADC input
                Config->InputPin = TTC_ADC53;
                break;
            }
#endif
#ifdef TTC_ADC54
            case 54: { // load config for ADC input
                Config->InputPin = TTC_ADC54;
                break;
            }
#endif
#ifdef TTC_ADC55
            case 55: { // load config for ADC input
                Config->InputPin = TTC_ADC55;
                break;
            }
#endif
#ifdef TTC_ADC56
            case 56: { // load config for ADC input
                Config->InputPin = TTC_ADC56;
                break;
            }
#endif
#ifdef TTC_ADC57
            case 57: { // load config for ADC input
                Config->InputPin = TTC_ADC57;
                break;
            }
#endif
#ifdef TTC_ADC58
            case 58: { // load config for ADC input
                Config->InputPin = TTC_ADC58;
                break;
            }
#endif
#ifdef TTC_ADC59
            case 59: { // load config for ADC input
                Config->InputPin = TTC_ADC59;
                break;
            }
#endif
#ifdef TTC_ADC60
            case 60: { // load config for ADC input
                Config->InputPin = TTC_ADC60;
                break;
            }
#endif
#ifdef TTC_ADC61
            case 61: { // load config for ADC input
                Config->InputPin = TTC_ADC61;
                break;
            }
#endif
#ifdef TTC_ADC62
            case 62: { // load config for ADC input
                Config->InputPin = TTC_ADC62;
                break;
            }
#endif
#ifdef TTC_ADC63
            case 63: { // load config for ADC input
                Config->InputPin = TTC_ADC63;
                break;
            }
#endif
#ifdef TTC_ADC64
            case 64: { // load config for ADC input
                Config->InputPin = TTC_ADC64;
                break;
            }
#endif
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); // ADC of this index is not configured (-> check your board makefile)
        }
        Assert_ADC( Config->InputPin, ttc_assert_origin_auto ); // invalid configuration: check your board makefile!
    }


    e_ttc_adc_errorcode Result = _driver_adc_find_input_pin( Config ); // identify low-level settings (if not already done)
    if ( Result == ec_adc_OK ) { ttc_gpio_init( Config->InputPin, E_ttc_gpio_mode_analog_in, E_ttc_gpio_speed_max );}
    if ( Result == ec_adc_OK ) { Result = _driver_adc_init( Config );}
    if ( Result == ec_adc_OK ) { Config->Flags.Bits.Initialized = 1;}

    //    Assert_ADC(Config->Architecture, ttc_assert_origin_auto); // Low-Level driver must set this value!

    return Result;
}
e_ttc_adc_errorcode ttc_adc_load_defaults( t_u8 LogicalIndex ) {
    Assert_ADC( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_adc_config* Config = ttc_adc_get_configuration( LogicalIndex );

    if ( Config->Flags.Bits.Initialized )
    { ttc_adc_deinit( LogicalIndex ); }

    //X ttc_memory_set( Config, 0, sizeof(t_ttc_adc_config) );

    // load generic default values
    if ( !Config->InputPin ) {
        switch ( LogicalIndex ) { // load input pin for given logical index from static configuration
#ifdef TTC_ADC1
            case  1: Config->InputPin = TTC_ADC1; break;
#endif
#ifdef TTC_ADC2
            case  2: Config->InputPin = TTC_ADC2; break;
#endif
#ifdef TTC_ADC3
            case  3: Config->InputPin = TTC_ADC3; break;
#endif
#ifdef TTC_ADC4
            case  4: Config->InputPin = TTC_ADC4; break;
#endif
#ifdef TTC_ADC5
            case  5: Config->InputPin = TTC_ADC5; break;
#endif
#ifdef TTC_ADC6
            case  6: Config->InputPin = TTC_ADC6; break;
#endif
#ifdef TTC_ADC7
            case  7: Config->InputPin = TTC_ADC7; break;
#endif
#ifdef TTC_ADC8
            case  8: Config->InputPin = TTC_ADC8; break;
#endif
#ifdef TTC_ADC9
            case  9: Config->InputPin = TTC_ADC9; break;
#endif
#ifdef TTC_ADC10
            case  10: Config->InputPin = TTC_ADC10; break;
#endif
#ifdef TTC_ADC11
            case  11: Config->InputPin = TTC_ADC11; break;
#endif
#ifdef TTC_ADC12
            case  12: Config->InputPin = TTC_ADC12; break;
#endif
#ifdef TTC_ADC13
            case  13: Config->InputPin = TTC_ADC13; break;
#endif
#ifdef TTC_ADC14
            case  14: Config->InputPin = TTC_ADC14; break;
#endif
#ifdef TTC_ADC15
            case  15: Config->InputPin = TTC_ADC15; break;
#endif
#ifdef TTC_ADC16
            case  16: Config->InputPin = TTC_ADC16; break;
#endif
#ifdef TTC_ADC17
            case  17: Config->InputPin = TTC_ADC17; break;
#endif
#ifdef TTC_ADC18
            case  18: Config->InputPin = TTC_ADC18; break;
#endif
#ifdef TTC_ADC19
            case  19: Config->InputPin = TTC_ADC19; break;
#endif
#ifdef TTC_ADC20
            case  20: Config->InputPin = TTC_ADC20; break;
#endif
#ifdef TTC_ADC21
            case  21: Config->InputPin = TTC_ADC21; break;
#endif
#ifdef TTC_ADC22
            case  22: Config->InputPin = TTC_ADC22; break;
#endif
#ifdef TTC_ADC23
            case  23: Config->InputPin = TTC_ADC23; break;
#endif
#ifdef TTC_ADC24
            case  24: Config->InputPin = TTC_ADC24; break;
#endif
#ifdef TTC_ADC25
            case  25: Config->InputPin = TTC_ADC25; break;
#endif
#ifdef TTC_ADC26
            case  26: Config->InputPin = TTC_ADC26; break;
#endif
#ifdef TTC_ADC27
            case  27: Config->InputPin = TTC_ADC27; break;
#endif
#ifdef TTC_ADC28
            case  28: Config->InputPin = TTC_ADC28; break;
#endif
#ifdef TTC_ADC29
            case  29: Config->InputPin = TTC_ADC29; break;
#endif
#ifdef TTC_ADC30
            case  30: Config->InputPin = TTC_ADC30; break;
#endif
#ifdef TTC_ADC31
            case  31: Config->InputPin = TTC_ADC31; break;
#endif
#ifdef TTC_ADC32
            case  32: Config->InputPin = TTC_ADC32; break;
#endif
#ifdef TTC_ADC33
            case  33: Config->InputPin = TTC_ADC33; break;
#endif
#ifdef TTC_ADC34
            case  34: Config->InputPin = TTC_ADC34; break;
#endif
#ifdef TTC_ADC35
            case  35: Config->InputPin = TTC_ADC35; break;
#endif
#ifdef TTC_ADC36
            case  36: Config->InputPin = TTC_ADC36; break;
#endif
#ifdef TTC_ADC37
            case  37: Config->InputPin = TTC_ADC37; break;
#endif
#ifdef TTC_ADC38
            case  38: Config->InputPin = TTC_ADC38; break;
#endif
#ifdef TTC_ADC39
            case  39: Config->InputPin = TTC_ADC39; break;
#endif
#ifdef TTC_ADC40
            case  40: Config->InputPin = TTC_ADC40; break;
#endif
#ifdef TTC_ADC41
            case  41: Config->InputPin = TTC_ADC41; break;
#endif
#ifdef TTC_ADC42
            case  42: Config->InputPin = TTC_ADC42; break;
#endif
#ifdef TTC_ADC43
            case  43: Config->InputPin = TTC_ADC43; break;
#endif
#ifdef TTC_ADC44
            case  44: Config->InputPin = TTC_ADC44; break;
#endif
#ifdef TTC_ADC45
            case  45: Config->InputPin = TTC_ADC45; break;
#endif
#ifdef TTC_ADC46
            case  46: Config->InputPin = TTC_ADC46; break;
#endif
#ifdef TTC_ADC47
            case  47: Config->InputPin = TTC_ADC47; break;
#endif
#ifdef TTC_ADC48
            case  48: Config->InputPin = TTC_ADC48; break;
#endif
#ifdef TTC_ADC49
            case  49: Config->InputPin = TTC_ADC49; break;
#endif
#ifdef TTC_ADC50
            case  50: Config->InputPin = TTC_ADC50; break;
#endif
#ifdef TTC_ADC51
            case  51: Config->InputPin = TTC_ADC51; break;
#endif
#ifdef TTC_ADC52
            case  52: Config->InputPin = TTC_ADC52; break;
#endif
#ifdef TTC_ADC53
            case  53: Config->InputPin = TTC_ADC53; break;
#endif
#ifdef TTC_ADC54
            case  54: Config->InputPin = TTC_ADC54; break;
#endif
#ifdef TTC_ADC55
            case  55: Config->InputPin = TTC_ADC55; break;
#endif
#ifdef TTC_ADC56
            case  56: Config->InputPin = TTC_ADC56; break;
#endif
#ifdef TTC_ADC57
            case  57: Config->InputPin = TTC_ADC57; break;
#endif
#ifdef TTC_ADC58
            case  58: Config->InputPin = TTC_ADC58; break;
#endif
#ifdef TTC_ADC59
            case  59: Config->InputPin = TTC_ADC59; break;
#endif
#ifdef TTC_ADC60
            case  60: Config->InputPin = TTC_ADC60; break;
#endif
#ifdef TTC_ADC61
            case  61: Config->InputPin = TTC_ADC61; break;
#endif
#ifdef TTC_ADC62
            case  62: Config->InputPin = TTC_ADC62; break;
#endif
#ifdef TTC_ADC63
            case  63: Config->InputPin = TTC_ADC63; break;
#endif
#ifdef TTC_ADC64
            case  64: Config->InputPin = TTC_ADC64; break;
#endif
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
        }
        //Assert_ADC( (Config->Architecture->ta_adc_None) && (Config->Architecture < ta_adc_unknown), ttc_assert_origin_auto); // architecture not set properly! Check makefile for TTC_ADC<n> and compare with e_ttc_adc_architecture
    }
    Config->LogicalIndex = LogicalIndex;
    Config->Flags.Bits.SingleShot   = 1;    // enable most simple mode as default setting

    //Insert additional generic default values here ...

    return _driver_adc_load_defaults( Config );
}
void ttc_adc_prepare() {

    // add your startup code here (Singletasking!)
    ttc_memory_set( &( A( ttc_adc_configs, 0 ) ), 0, sizeof( t_ttc_adc_config* ) * TTC_ADC_MAX_AMOUNT );
    _driver_adc_prepare();
}
e_ttc_adc_errorcode ttc_adc_reset( t_u8 LogicalIndex ) {
    Assert_ADC( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_adc_config* Config = ttc_adc_get_configuration( LogicalIndex );

    return _driver_adc_reset( Config );
}
/* DEPRECATED
 e_ttc_adc_errorcode ttc_adc_init_dma(t_u8 LogicalIndex, volatile t_u16* Value) {
    Assert_ADC(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_adc_config* Config = ttc_adc_get_configuration(LogicalIndex);

    e_ttc_adc_errorcode Result = _driver_adc_init_dma(Config, Value);
    if (Result == ec_adc_OK)
        Config->Flags.Bits.Initialized = 1;

    return Result;
}
*/
t_u16 ttc_adc_get_value( t_u8 LogicalIndex ) {
    Assert_ADC( LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1
    t_ttc_adc_config* Config = ttc_adc_get_configuration( LogicalIndex );

    return _driver_adc_get_value( Config );
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_adc(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
