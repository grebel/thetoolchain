/** { ttc_heap_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for HEAP device.
 *  Structures, Enums and Defines being required by both, high- and low-level heap.
 *
 *  Created from template ttc_device_types.h revision 22 at 20140301 07:28:17 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_HEAP_TYPES_H
#define TTC_HEAP_TYPES_H

//{ Includes *************************************************************

#include "ttc_basic_types.h"
#include "ttc_memory_types.h"
#ifdef EXTENSION_heap_simple
    #include "heap/heap_simple_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_heap_zdefault
    #include "heap/heap_zdefault_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_heap_freertos
    #include "heap/heap_freertos_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

// TTC_HEAPn has to be defined as constant by makefile.100_board_*
#ifdef TTC_HEAP5
    #ifndef TTC_HEAP4
        #error TTC_HEAP5 is defined, but not TTC_HEAP4 - all lower TTC_HEAPn must be defined!
    #endif
    #ifndef TTC_HEAP3
        #error TTC_HEAP5 is defined, but not TTC_HEAP3 - all lower TTC_HEAPn must be defined!
    #endif
    #ifndef TTC_HEAP2
        #error TTC_HEAP5 is defined, but not TTC_HEAP2 - all lower TTC_HEAPn must be defined!
    #endif
    #ifndef TTC_HEAP1
        #error TTC_HEAP5 is defined, but not TTC_HEAP1 - all lower TTC_HEAPn must be defined!
    #endif

    #define TTC_HEAP_AMOUNT 5
#else
    #ifdef TTC_HEAP4
        #define TTC_HEAP_AMOUNT 4

        #ifndef TTC_HEAP3
            #error TTC_HEAP5 is defined, but not TTC_HEAP3 - all lower TTC_HEAPn must be defined!
        #endif
        #ifndef TTC_HEAP2
            #error TTC_HEAP5 is defined, but not TTC_HEAP2 - all lower TTC_HEAPn must be defined!
        #endif
        #ifndef TTC_HEAP1
            #error TTC_HEAP5 is defined, but not TTC_HEAP1 - all lower TTC_HEAPn must be defined!
        #endif
    #else
        #ifdef TTC_HEAP3

            #ifndef TTC_HEAP2
                #error TTC_HEAP5 is defined, but not TTC_HEAP2 - all lower TTC_HEAPn must be defined!
            #endif
            #ifndef TTC_HEAP1
                #error TTC_HEAP5 is defined, but not TTC_HEAP1 - all lower TTC_HEAPn must be defined!
            #endif

            #define TTC_HEAP_AMOUNT 3
        #else
            #ifdef TTC_HEAP2

                #ifndef TTC_HEAP1
                    #error TTC_HEAP5 is defined, but not TTC_HEAP1 - all lower TTC_HEAPn must be defined!
                #endif

                #define TTC_HEAP_AMOUNT 2
            #else
                #ifdef TTC_HEAP1
                    #define TTC_HEAP_AMOUNT 1
                #else
                    #define TTC_HEAP_AMOUNT 0
                #endif
            #endif
        #endif
    #endif
#endif

/* Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_HEAP 0  # disable assert handling for heap devices
 *
 */
#ifndef TTC_ASSERT_HEAP    // any previous definition set (Makefile)?
    #define TTC_ASSERT_HEAP 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_HEAP == 1)  // use Assert()s in HEAP code (somewhat slower but alot easier to debug)
    #define Assert_HEAP(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_HEAP_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_HEAP_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in HEAP code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_HEAP(Condition, Origin)
    #define Assert_HEAP_Writable(Address, Origin)
    #define Assert_HEAP_Readable(Address, Origin)
#endif


#ifndef TTC_ASSERT_HEAP_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_HEAP_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_HEAP_EXTRA == 1)  // use Assert()s in HEAP code (somewhat slower but alot easier to debug)
    #define Assert_HEAP_EXTRA(Condition, Origin)        Assert( ((Condition) != 0), Origin)
    #define Assert_HEAP_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_HEAP_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in HEAP code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_HEAP_EXTRA(Condition, Origin)
    #define Assert_HEAP_EXTRA_Writable(Address, Origin)
    #define Assert_HEAP_EXTRA_Readable(Address, Origin)
#endif

/** Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_heap_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_heap_architecture
    #warning Missing low-level definition for t_ttc_heap_architecture (using default)
    #define t_ttc_heap_architecture void
#endif

/** Activate extra runtime pointer checks
 *
 * Defining a random value != 0 will add an extra 32-bit field to each pool.
 * The containing magic-key allows to identify invalid pointer to pool or memory block.
 * Define as 0 in your makefile to disable this feature.
 */
#ifndef TTC_HEAP_MAGICKEY
    #if (TTC_ASSERT_HEAP == 1)
        #define TTC_HEAP_MAGICKEY 3221338814 // == 0xcoo1babe  Asserts enabled: by default use magic keys to protect from invalid pointers
    #else
        #define TTC_HEAP_MAGICKEY 0          // Asserts disabled: use magic keys only if defined outside
    #endif
#endif

#ifndef TTC_HEAP_ENUMERATE
    #define TTC_HEAP_ENUMERATE 0  // == 1: each allocated memory block gets its own number at offset -sizeof(t_base)
#endif

#ifndef TTC_HEAP_STATISTICS
    #define TTC_HEAP_STATISTICS 0
    #ifndef TTC_HEAP_RECORDS
        #define TTC_HEAP_RECORDS 0 // >0: amount of memory allocations to record; ==0: no recording
    #endif
#endif

#ifndef TTC_HEAP_POOL_STATISTICS
    #define TTC_HEAP_POOL_STATISTICS TTC_ASSERT_HEAP // == 1: add extra statistic data to each memory pool
#endif

#ifndef TTC_HEAP_POOL_AMOUNT_MYBLOCKS
    #define TTC_HEAP_POOL_AMOUNT_MYBLOCKS 10 // >0: amount of pointers to allocated pool blocks to store in t_ttc_heap_pool (allows to find blocks during debugging session when pool stays empty); ==0: disable this debugging feature
#endif

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef struct s_ttc_heap_block { // t_ttc_heap_block - memory block that stores up to 2GB (on 32-bit architectures)
    // After usage, a memory block can be returned to its memory
    // pool by calling ttc_heap_block_release().

    struct s_ttc_heap_block* Next; // memory blocks can build a linked list of memory blocks
    void ( *releaseBuffer )( struct s_ttc_heap_block* Block ); // function that will take back ownership of this block
    t_u8 Hint;                       // general hint that can be used by application to assign this block to a certain class
    t_u8 UseCount;                   // increased on every ttc_heap_block_use(); decreased on every ttc_heap_block_release()
    TTC_MEMORY_BLOCK_BASE MaxSize;   // maximum allowed value of Size (size of Buffer[])
    TTC_MEMORY_BLOCK_BASE Size;      // >0: current amount of bytes stored in Buffer[]
    // <0: max size of zero terminated string stored in Buffer[]
    t_u8* Buffer;                    // points to continous memory area containing data of this block
} __attribute__( ( __packed__ ) ) t_ttc_heap_block;

#include "ttc_list_types.h"

/** Header of memory block being allocated from a memory pool
  */
typedef struct s_ttc_heap_block_from_pool { // t_ttc_heap_block_from_pool

    // memory blocks can build a linked list
    // This pointer can be used by the application
    // until a block has been released
    t_ttc_list_item ListItem;

    // reference of memory pool serving this block
    struct s_ttc_heap_pool* Pool;

#if TTC_HEAP_ENUMERATE == 1
    // every memory block gets its own unique number
    t_u16 Enum;

#endif
#if TTC_HEAP_POOL_STATISTICS == 1

    // single-linked list of all blocks belonging to this pool
    struct s_ttc_heap_block_from_pool* MyNextBlock;

    // points to return address right after ttc_heap_pool_block_get*() call
    void ( *MyOwner )();
#endif

#if (TTC_HEAP_MAGICKEY != 0)
    // loaded with special number to distinguish between valid and invalid memory block pointers
    void* MagicKey;
#endif
    // End of Header - Start of Data
} t_ttc_heap_block_from_pool;

typedef struct s_s_ttc_heap_pooltatistics {
    // Note: Access statistics is not secured via mutex/ semaphore to reduce performance penalty.
    //       Therefore, data in this struct must not be 100% exact.

    // statistic: amount of ttc_heap_pool_block_get()-calls
    t_base AmountBlockGet;

    // statistic: amount of ttc_heap_pool_block_free()-calls
    t_base AmountBlockFree;

    // statistic: amount of ttc_heap_pool_block_free_isr()-calls
    t_base AmountBlockFreeISR;

    // statistic: amount of ttc_heap_pool_block_get_isr()-calls
    t_base AmountBlockGetISR;

    // statistic: amount of ttc_heap_pool_block_get_isr() calls for empty pool (NULL returned)
    t_u8 AmountBlockGetISR_Failed1;
    t_u8 AmountBlockGetISR_Failed2;
    t_u8 AmountBlockGetISR_Failed3;

    // statistic: current list-size of released, unused memory blocks
    t_u8 AmountFreeCurrently;

    // statistic: current list-size of released, unused memory blocks waiting in list t_ttc_heap_pool.FirstFree_FromISR
    t_u8 AmountFreeCurrentlyFromISR;

    // statistic: amount of blocks grabbed from second list item by interrupt service routine
    t_u8 AmountGrabbedFromISR;

    // statistic: amount of blocks freed from isr to t_ttc_heap_pool.FirstFree_FromISR
    t_u8 AmountSpecialFreedFromISR;

    // amount of memory blocks allocated from Heap[]
    t_u8 AmountAllocated;

#if TTC_HEAP_POOL_AMOUNT_MYBLOCKS > 0
    // Array of pointers to fixed amount of blocks being allocated for this pool.
    // One can use this array to identify the owners of blocks not being returned to the pool.
    t_ttc_heap_block_from_pool* MyBlocks[TTC_HEAP_POOL_AMOUNT_MYBLOCKS];
#endif
} t_s_ttc_heap_pooltatistics;

/** Management data of a pool of equal sized memory blocks
  */
typedef struct s_ttc_heap_pool {

    // used by ttc_heap_pool_block_get() to wait for memory block to be released
    t_ttc_semaphore_smart BlocksAvailable;

    // Mutex that protects this pool from concurrent accesses from other tasks or from interrupt service routines.
    t_ttc_mutex_smart Lock;

    // first entry in a linked list of unused memory blocks
    t_ttc_heap_block_from_pool* FirstFree;

    // first entry in a linked list of unused memory blocks
    // These blocks have been freed from interrupt service routine.
    // Accesses to this list must be done with disabled interrupts to avoid concurrent ISR access.
    t_ttc_heap_block_from_pool* FirstFree_FromISR;

    // size of each memory block belonging to this pool
    t_u16 BlockSize;

#if TTC_HEAP_POOL_STATISTICS == 1
    // statistic data
    t_s_ttc_heap_pooltatistics Statistics;

    // points to function that last called a ttc_heap_pool_get*() function
    void ( *LastGet )();

    // points to function that last called a ttc_heap_pool_free*() function
    void ( *LastFree )();

#endif
} t_ttc_heap_pool;

typedef struct {                       // debugging info of a memory block from a pool (->ttc_heap_pool_debug())
    t_ttc_heap_block_from_pool* Address; // address of memory block
    t_ttc_heap_block_from_pool  Data;    // data copied from memory block
} t_ttc_heap_pool_block_debug;

#define THPD_BLOCKS_SIZE 10

typedef struct s_ttc_heap_pool_debug { // debugging info of a memory pool (->ttc_heap_pool_debug())
    t_ttc_heap_pool             Pool;
    t_u16                       List_Size;                     // amount of items in single linked list Pool->FirstFree
    t_u8                        FreeBlocks_Size;               // size of FreeBlocks[] (amount of allocated entries)
    t_ttc_heap_pool_block_debug FreeBlocks[THPD_BLOCKS_SIZE];  // first available memory blocks
} t_ttc_heap_pool_debug;

#if TTC_HEAP_RECORDS > 0

typedef struct {
    t_u16 Enum;     // each block gets a unique, consecutive number
    void* Address;  // start address of memory block
    t_base Size;    // size of memory block
} t_ttc_heap_record;

#endif

typedef enum {   // e_ttc_heap_physical_index (starts at zero!)
    //
    // You may use this enum to assign physical index to logical TTC_HEAPn constants in your makefile
    //
    // E.g.: Defining physical device #3 of heap as first logical device TTC_HEAP1
    //       COMPILE_OPTS += -DTTC_HEAP1=INDEX_HEAP3
    //
    INDEX_HEAP1,
    INDEX_HEAP2,
    INDEX_HEAP3,
    INDEX_HEAP4,
    INDEX_HEAP5,
    INDEX_HEAP6,
    INDEX_HEAP7,
    INDEX_HEAP8,
    INDEX_HEAP9,
    // add more if needed

    INDEX_HEAP_ERROR
} e_ttc_heap_physical_index;
typedef enum {    // e_ttc_heap_errorcode     return codes of HEAP devices
    ec_heap_OK = 0,

    // other warnings go here..

    ec_heap_ERROR,                  // general failure
    ec_heap_NULL,                   // NULL pointer not accepted
    ec_heap_DeviceNotFound,         // corresponding device could not be found
    ec_heap_InvalidArgument,        // invalid argument given to a function
    ec_heap_InvalidImplementation,  // your code does not behave as expected
    ec_heap_OutOfMemory,            // dynamic memory allocator ran out of memory

    // other failures go here..

    ec_heap_unknown                // no valid errorcodes past this entry
} e_ttc_heap_errorcode;
typedef enum {    // e_ttc_heap_architecture  types of architectures supported by HEAP driver
    ta_heap_None,           // no architecture selected


    ta_heap_simple, // automatically added by ./create_DeviceDriver.pl
    ta_heap_zdefault, // automatically added by ./create_DeviceDriver.pl
    ta_heap_freertos, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_heap_unknown        // architecture not supported
} e_ttc_heap_architecture;
typedef struct s_ttc_heap_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_heap_init()
    //       and after ttc_heap_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    e_ttc_heap_architecture Architecture; // type of architecture used for current heap device
    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_HEAP1, ...)
    t_u8  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)

    // low-level configuration (structure defined by low-level driver)
    t_ttc_heap_architecture* LowLevelConfig;

    union  { // generic configuration bits common for all low-level Drivers
        t_u16 All;
        struct {
            unsigned Initialized        : 1;  // ==1: device has been initialized successfully
            // ToDo: additional high-level flags go here..

            unsigned Reserved1        : 15; // pad to 16 bits
        } Bits;
    } Flags;

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_heap_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_HEAP_TYPES_H
