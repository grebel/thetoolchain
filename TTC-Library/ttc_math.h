#ifndef TTC_MATH_H
#define TTC_MATH_H
/** { ttc_math.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of ttc_interrupt (Do not delete this line!)
 *
 *  ttc_math provides a configurable mathematical precision
 *  using software or hardware assisted implementations.
 *
 *  For many functions, the default implementation can be replaced by an architecture
 *  or application specific implementation without changing your application source code.
 *
 *  Instead of providing different functions for integer, float or double precision only
 *  one high-level function is provided for each mathematical operation. The datatype ttm_number
 *  is defined according to the current activated low-level implementation.
 *  Applications calling ttc_math_*() functions should use ttm_number instead of double or float
 *  to allow switching the precision.
 *
 *  Converting to and from low-level ttm_number
 *  Special macros are provided to load integer, fixed point or floating point constants into
 *  variables of type ttm_number. For the basic types float and double, the basic C assignment
 *  via = is used. Implementations using fixed point representation may use different operations.
 *  Examples:
 *  ttm_number A = ttm_from_int(1000);               // load integer constant 1000 into A
 *  ttm_number A = ttm_from_float(23.45);            // load single precision floating point constant 23.45 into A
 *  ttm_number A = ttm_from_double(23.45343455433);  // load double precision floating point constant 23.45343455433 into A
 *  t_base_signed B = ttm_to_int(A);                 // convert A into signed integer format and store it in B
 *  float         C = ttm_to_float(A);               // convert A into single precision floating point format and store it in C
 *  double        D = ttm_to_double(A);              // convert A into double precision floating point format and store it in D
 *
 *  Activate Extension
 *  This Extension uses different low-level drivers to switch between different precisions and
 *  hardware acceleration features. To activate ttc_math, a low-level driver has to be provided as
 *  feature in your activate script before.
 *  Example for single precision software floating point implementation:
 *  + activate_project.sh
 *      enableFeature 450_math_software_float
 *      activate.500_ttc_math.sh   QUIET "\$0"
 *
}*/

#ifndef EXTENSION_ttc_math
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_math.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_math.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_math_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level math only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function Declarations ************************************************{
 *
 * The functions declared below provide the main interface for
 * math devices on all supported architectures.
 * Check math/math_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares math Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_math_prepare();
void _driver_math_prepare();

/** performs modulo operation to map given angle into -PI ... + PI for radiant arguments
 *
 * @param Angle (ttm_number)  angle (radiant)
 * @return      (ttm_number)  angle from ]-PI, +PI] corresponding to given Angle (radiant)
 */
ttm_number ttc_math_angle_reduce( ttm_number Angle );
ttm_number _driver_math_angle_reduce( ttm_number Angle );
#define ttc_math_angle_reduce  _driver_math_angle_reduce

/** Calculate angle of given cartesian 2D vector.
 *
 * Using ttc_math_arctan() for a given inklination vector will only compute angle in 1st and 4th quadrant.
 * This function works in all four quadrants.
 *
 *    Q2       A y     Q1     a := clock-wise angle between (0,0)->(0,1) and (0,0)->(DX,DY)
 *     \       |       /
 *      \      |      /       Q1: DX>0,DY>0
 *       \   DY+     /            a = arctan(DY / DX);
 *        \   _|____/             a € [0, PI/2]     |  [0°, 90°]
 *         \<- |   / *          \  |  /  \        Q2: DX<0,DY>0
 *           \ | /   |            a = arctan(DX / DY);
 *            \|/a>0 |            a € ]PI/2, PI]    |  ]90°, 180°[
 *   <-+-------+-------+->
 *       DX   /|\a<0 | DX x   Q3: DX<0,DY<0
 *           / | \   |            a = arctan(DY / DX) - PI/2;
 *          /  |  \  /            a € ]-PI, -PI/2[  |  ]-180°, -90°[
 *         /   |   \/
 *        /<-__|___/\         Q4: DX>0,DY<0
 *       /   DY+     \            a = arctan(DY / DX) + PI/2;
 *      /      |      \           a € ]0, -PI/2]    |  ]0°, -90°]
 *    Q3       V       Q4
 *
 * @param DX (ttm_number)  x-increase of a 2D-vector
 * @param DY (ttm_number)  y-increase of a 2D-vector
 * @return   (ttm_number)  ]-PI, +PI]: angle a between positive y-axis and vector (0,0)->(DX,DY) (radiant)
 */
ttm_number ttc_math_vector2d_angle( ttm_number DX, ttm_number DY );
ttm_number _driver_math_vector2d_angle( ttm_number DX, ttm_number DY );
#define ttc_math_vector2d_angle _driver_math_vector2d_angle

/** Calculate cosinus of given number
 *
 * @param X (ttm_number)  angle (radiant)
 * @return  (ttm_number)  -1 .. 1
 */
ttm_number ttc_math_sin( ttm_number X );
#if 0
    ttm_number _driver_math_sin( ttm_number X );
#endif
#define ttc_math_sin     _driver_math_sin

/** Calculate cosinus of given number
 *
 * @param X (ttm_number)  angle (radiant)
 * @return  (ttm_number)  -1 .. 1
 */
ttm_number ttc_math_cos( ttm_number X );
ttm_number _driver_math_cos( ttm_number X );
#define ttc_math_cos  _driver_math_cos

/** Calculate 'tangens of given number
 *
 * @param X (ttm_number)  angle (radiant)
 * @return  (ttm_number)  tan(X)
 */
ttm_number ttc_math_tan( ttm_number X );
ttm_number _driver_math_tan( ttm_number X );
#define ttc_math_tan  _driver_math_tan

/** Calculates inverse cosinus of given argument
 *
 * @param Precision (t_u8)        amount of taylor polynoms to calculate (12 = +-0.1  128 = +-0.01)
 * @param X         (ttm_number)  argument to calculate arcus cosinus for
 * @return          (ttm_number)  arccos(X) at desired precision (radiant)
 */
ttm_number ttc_math_arccos( ttm_number X );
#ifndef _driver_math_arccos // avoids compile error due to macro definition in ttc_math_interface.h
    ttm_number _driver_math_arccos( ttm_number X );
#endif
#define ttc_math_arccos  _driver_math_arccos// enable to directly forward function call to low-level driver (remove function definition before!)

/** Calculates inverse sinus of given argument
 *
 * @param Precision (t_u8)        amount of taylor polynoms to calculate (12 = +-0.1  128 = +-0.01)
 * @param X         (ttm_number)  argument to calculate arcus sinus for
 * @return          (ttm_number)  arcsin(X) at desired precision (radiant)
 */
ttm_number ttc_math_arcsin( ttm_number X );
ttm_number _driver_math_arcsin( ttm_number X );
#define ttc_math_arcsin  _driver_math_arcsin// enable to directly forward function call to low-level driver (remove function definition before!)

/** Calculate inverse tangens of given number
 *
 * Note: To calculate 4 quadrant angle of a vector, use ttc_math_vector2d_angle() instead!
 * Note: For highest precision, calculate inverse atan() if X<0.5:
 *       abs(X) >=0.5: atan(x) = ttc_math_arctan(X)
 *       abs(X) < 0.5: atan(x) = PI/2 - ttc_math_arctan(1/X)
 *
 * @param X (ttm_number)  input value
 * @return  (ttm_number)  atan(X) € ]-PI/2, +P/2]; (radiant)
 */
ttm_number ttc_math_arctan( ttm_number X );
ttm_number _driver_math_arctan( ttm_number X );
#define ttc_math_arctan  _driver_math_arctan

/** Convert 2-dimensional polar coordinate (A,L) into corresponding cartesian coordinate (X,Y)
 *
 * See description of ttc_math_vector2d_angle() for description of coordinate system.
 *
 * @param Angle  (ttm_number)   Angle of line from origin to point (radiant)
 * @param Length (ttm_number)   Distance from origin to point
 * @param X      (ttm_number*)  return: X-coordinate
 * @param Y      (ttm_number*)  return: Y-coordinate
 */
void ttc_math_polar2cartesian( ttm_number Angle, ttm_number Length, ttm_number* X, ttm_number* Y );
void _driver_math_polar2cartesian( ttm_number Angle, ttm_number Length, ttm_number* X, ttm_number* Y );
#define ttc_math_polar2cartesian  _driver_math_polar2cartesian

/** Convert 2-dimensional cartesian coordinate (X,Y) to its polar correspondence (A,L)
 *
 * @param X      (ttm_number)   X-coordinate
 * @param Y      (ttm_number)   Y-coordinate
 * @param Angle  (ttm_number*)  return:  Angle of vector from origin to (X,Y) (radiant)
 * @param Length (ttm_number*)  return: Length of vector from origin to (X,Y)
 */
void ttc_math_cartesian2polar( ttm_number X, ttm_number Y, ttm_number* Angle, ttm_number* Length );
void _driver_math_cartesian2polar( ttm_number X, ttm_number Y, ttm_number* Angle, ttm_number* Length );
#define ttc_math_cartesian2polar  _driver_math_cartesian2polar

/** Calculate positive distance between to given numbers
 *
 * When calculating the difference between two t_s32 variables, the result may not fit into an t_s32.
 * Assigning result of t_s32 A - t_s32 B to t_u32 C will only work if A > B.
 * This macro helps to correctly calculate an always positive distance between two signed or unsigned variables.
 *
 * @param A   any type of number
 * @param B   any type of number
 * @return    abs(A-B)
 */
#define ttc_math_distance( A, B ) ((A) > (B)) ? ((A) - (B)) : ((B) - (A))

/** Get biggest/ smallest of two values
 *
 * @param A  first numeric value
 * @param B  second numeric value
 * @return   biggest or smallest from both given values
 */
#define ttc_math_max(A,B) ( ((A) > (B)) ? (A) : (B) )
#define ttc_math_min(A,B) ( ((A) < (B)) ? (A) : (B) )

/** Calculate distance between two points A = (A.x, A.y), B = (B.x, B.y) in 2D space
 *
 * Note: Use ttc_math_distance() to calculate DistanceX and DistanceY fast and with full range!
 *
 * @param DistanceX (t_u32) abs(B.x - A.x) = ttc_math_distance( A.x, B.x )
 * @param DistanceY (t_u32) abs(B.y - A.y) = ttc_math_distance( A.y, B.y )
 * @return   (ttm_number)   2D distance A-B
 */
ttm_number ttc_math_length_2d( ttm_number DistanceX, ttm_number DistanceY );
ttm_number _driver_math_length_2d( ttm_number DistanceX, ttm_number DistanceY );
#define ttc_math_length_2d  _driver_math_length_2d  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Calculate distance between two points A = (Ax, Ay), B = (Bx, By) in 2D space
 *
 * @param Ax (ttm_number)  x-coordinate of point A
 * @param Ay (ttm_number)  y-coordinate of point A
 * @param Bx (ttm_number)  x-coordinate of point B
 * @param By (ttm_number)  y-coordinate of point B
 * @return   (ttm_number)  length of straight line A -> B
 */
ttm_number ttc_math_distance_2d( ttm_number Ax, ttm_number Ay, ttm_number Bx, ttm_number By );
ttm_number _driver_math_distance_2d( ttm_number Ax, ttm_number Ay, ttm_number Bx, ttm_number By );
#define ttc_math_distance_2d  _driver_math_distance_2d  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Computes power of given base number
 *
 * @param Base     (ttm_number)  base of power
 * @param Exponent (t_u8)        integer value
 * @return         (ttm_number)  Base * Base * ... * Base (Exponent times)
 */
ttm_number ttc_math_pow( ttm_number Base, t_u8 Exponent );
ttm_number _driver_math_pow( ttm_number Base, t_u8 Exponent );
#define ttc_math_pow  _driver_math_pow

/** Calculcates A modulo B
 *
 * @param A (ttm_number)  any number
 * @param B (ttm_number)  any number
 * @return  (ttm_number)  A modulo B
 */
ttm_number ttc_math_modulo( ttm_number A, ttm_number B );
//ttm_number _driver_math_modulo( ttm_number A, ttm_number B );
#define ttc_math_modulo  _driver_math_modulo

/** Calulates integer logarithm of A for given base
 *
 * This is a lightweight implementation to find out how often Base can be
 * multiplied before getting bigger than A.
 *
 * @param A     input for log(A) operation
 * @param Base  A = ttc_math_pow(Base, Return)
 * @return      amount of multiplications of Base to reach A
 */
t_s8 ttc_math_log_int( ttm_number A, t_u8 Base );
t_s8 _driver_math_log_int( ttm_number A, t_u8 Base );
#define ttc_math_log_int  _driver_math_log_int

/** Multiplies two 32 bit integers into one 64 bit integer
 *
 * @param A    first 32 bit value
 * @param B    second 32 bit value
 * @param C32l pointer where to store low 32 bit word of A*B
 * @param C32h pointer where to store high 32 bit word of A*B
 */
void ttc_math_int_multiply_32x32( t_u32 A, t_u32 B, t_u32* C32l, t_u32* C32h );

/** Calculates the faculty of given positive integer value
 *
 * @param Value (t_u8)   positive integer value >=0
 * @return      (t_base) Value!
 */
t_base ttc_math_int_faculty( t_u8 Value );

/** Calculates 2D coordinates of point N by using origin (O), one fix point F=(Fx, Fy) and measured distances
 *
 * It is assumed that one fixpoint is the origin for faster calculation.
 *
 * Lateration with two generic fixpoints requires to shift nodes:
 * 1) Define generic fixpoints:              t_ttc_math_int_vector2d_xy F1, F2;
 * 2) Calculate new fixpoint relative to F1: t_ttc_math_int_vector2d_xy F; F.X = F2.X - F1.X; F.Y = F2.Y - F1.Y;
 *    We can laterate on O=(0,0) and F=(F.x, F.y) instead of F1 and F2.
 * 3) Define 2 nodes to be laterated:        t_ttc_math_int_vector2d_xy N1, N2;
 * 4) Calculate lateration:                  BOOL OK = ttc_math_lateration_2d(Distance(F1,N), Distance(F2,N), F.X, F.Y, &N1.X, &N1.Y, &N2.X, &N2.Y);
 *                                           if (OK) {
 * 5) Select which laterated node to use:      t_ttc_math_int_vector2d_xy N;
 *                                             if (N1..)  N = N1;
 *                                             else       N = N2;
 * 6) Shift N to final position:               N.X += F1.X; N.Y += F1.Y;
 *                                           }
 *
 *
 * Generic 2D lateration implementation with fast optimizations for special cases.
 *
 *     General Case                  |   Special Case (Fx == 0)   |    Special Case (Fy == 0)
 *                                   |                            |
 *    F (fixed)                      |                            |
 *   O--__                           |     O F (fixed)            |    O (Origin)         F (fixed)
 *    \   --__ DistanceFN            |     |\                     |    O-----------------O
 *     |      --__                   |     | \                    |     \             __/
 *      \         O N (calculated)   |     |  \ DistanceFN        |      |DistanceON_/
 *       |       /                   |     |   \                  |       \       _/
 *        \     /                    |     |    O N (calculated)  |        |    _/  DistanceFN
 *         |   / DistanceON          |     | __/                  |         \ _/
 *          \ /                      |     |/   DistanceON        |          O
 *           O                       |     O                      |            N (calculated)
 *            O (0,0)                |       O (0,0)              |
 *
 *
 * Note: 2D Lateration always delivers two possible positions for point N.
 *       Caller has to choose in advance if N should be located on right or left side
 *       of the vector O->F.
 *
 * Note: Special cases (Fx == 0) or (Fy == 0) are calculated faster than general case.
 * Note: N is allowed to lie on line O->F.
 *       If sum of distances is smaller than distance(O,F), it might be approximated:
 *       a) 0.9 * distance(O,F) <= DistanceON + DistanceFN < distance(O,F): position of F is approximated.
 *       b) 0.9 * distance(O,F) >  DistanceON + DistanceFN : lateration is aborted (invalid data)
 *
 * Typical runtime was measured as high-time of gpio pin TTC_MATH_PIN_LATERATION_3D using an oscilloscope (Tektronix MSO4104):
 *   Settings: ttc_math_Precision==tmp_1m; TTC_ASSERT_MATH_EXTRA==0
 *   Compiler: arm-none-eabi-gcc (GNU Tools for ARM Embedded Processors) 5.4.1 20160609 (release) [ARM/embedded-5-branch revision 237715
 *   Board: Olimex STM32-LCD (STM32F107 @ 72MHz)
 *
 * + Special Case Fx==0
 *   - (math_float):  58us  (OPTIMIZE+=-g3)
 *   - (math_float):  50us  (OPTIMIZE+=-g3 -Os)
 *   - (math_double): 120us (OPTIMIZE+=-g3)
 *   - (math_double): 110us (OPTIMIZE+=-g3 -Os)
 *
 * + Generic Case (Fx!=0) && (Fy!=0)
 *   - (math_float):  218..313 us average=250 us (OPTIMIZE+=-g3)
 *   - (math_float):  167..288 us average=230 us (OPTIMIZE+=-g3 -Os)
 *   - (math_double): us average= us (OPTIMIZE+=-g3)
 *   - (math_double): 384..440 us average=370 us (OPTIMIZE+=-g3 -Os)
 *
 *
 * @param DistanceON (ttm_number)     measured distance between origin O and N
 * @param DistanceFN (ttm_number)     measured distance between fixed point F and N
 * @param Fx         (ttm_number)     x-coordinate of fixed point F
 * @param Fy         (ttm_number)     y-coordinate of fixed point F
 * @param N1x        (ttm_number*)    !=NULL: where to store calculated x-coordinate of point N1 on left side of line O->F
 * @param N1y        (ttm_number*)    !=NULL: where to store calculated y-coordinate of point N1 on left side of line O->F
 * @param N2x        (ttm_number*)    !=NULL: where to store calculated x-coordinate of point N2 on right side of line O->F
 * @param N2y        (ttm_number*)    !=NULL: where to store calculated y-coordinate of point N2 on right side of line O->F
 * @return           (e_ttc_math_errorcode) ==0: *N1, *N2 have been calculates successfully; >0: given data does not allow lateration (see argument descriptions above!)
 */
e_ttc_math_errorcode ttc_math_lateration_2d( ttm_number DistanceON, ttm_number DistanceFN, ttm_number Fx, ttm_number Fy, ttm_number* N1x, ttm_number* N1y, ttm_number* N2x, ttm_number* N2y );

/** Calculates 3D coordinates of points N1, N2 by using origin (O), fix points F1, F2 and measured distances
 *
 * Laterating a point in 3D space with three distance measures provides two possible solutions in most cases.
 * This function is faster than a lateration using four or more distances.
 * Many real life scenarios allow to select the correct laterated node. E.g. if your anchor nodes are placed on the
 * ceiling of a room, then the nodes to be laterated are always at z-coordinate <= 0.
 *
 * The implementation works similar to ttc_math_lateration_2d().
 * Given:
 * - Three fixed points F1, F2, F3 with known 3D coordinates
 * - Point N=(Nx, Ny, Nz) with unknown position
 * - Three distance measures DistanceF1->N, DistanceF2->N, DistanceF3->N
 *
 * Preparation:
 * - Allocate variables
 *   t_ttc_math_lateration_3d     Lateration_3D_Cache;
 *   t_ttc_math_int_vector3d_xyz* N1, N2;
 *
 * - If none of F1,F2,F3 is (0,0,0) = origin then
 *    - subtract F1 from F2,F3 to get F2t, F3t.
 *    - call ttc_math_lateration_3d(&Lateration_3D_Cache, F2t, F3t, FALSE, DistanceF1->N, DistanceF2->N, DistanceF3->N, &N1, &N2)
 *  - else
 *    - rearrange F1,F2,F3 and DistanceF1->N, DistanceF2->N, DistanceF3->N,F2t,F3t so that
 *      F1 = (0,0,0) = O (Origin)
 *    - call ttc_math_lateration_3d(&Lateration_3D_Cache, FALSE, F2 ,F3, DistanceF1->N, DistanceF2->N, DistanceF3->N, &N1, &N2)
 *
 * Reduce processing time of subsequent calls with same fixed nodes (F2, F3 unchanged):
 *    while (Condition) {
 *      // update distances DistanceF1->N, DistanceF2->N, DistanceF3->N ...
 *
 *      // calculate new positions N1, N2 using samed fixed nodes F1, F2, F3
 *      ttc_math_lateration_3d(&Lateration_3D_Cache, TRUE, NULL ,NULL, DistanceF1->N, DistanceF2->N, DistanceF3->N, &N1, &N2);
 *    }
 *
 * First the coordinate system is rotated so that F2 and F3 are located on XY-plane.
 * Hint: Choosing F1, F2 and F3 to all lay in XY-plane will reduce computational effort.
 *
 * Typical runtime was measured as high-time of gpio pin TTC_MATH_PIN_LATERATION_3D using an oscilloscope (Tektronix MSO4104):
 *   Settings: ttc_math_Precision==tmp_1m; TTC_ASSERT_MATH_EXTRA==0
 *   Compiler: arm-none-eabi-gcc (GNU Tools for ARM Embedded Processors) 5.4.1 20160609 (release) [ARM/embedded-5-branch revision 237715
 *   Board:   Olimex STM32-LCD (STM32F107 @ 72MHz)
 *
 * + Use_DirectCalculation=1; Use_QuadraticApproximation=1 (high precision + fast)
 *   + FixedNodesUnchanged=0;
 *     - (math_float):  1.085..2.425 ms average=2.03 ms (OPTIMIZE+=-g3)
 *     - (math_float):  1.034..2.175 ms average=1.86 ms (OPTIMIZE+=-g3 -Os)
 *     - (math_double): 1.842..4.157 ms average=3.60 ms (OPTIMIZE+=-g3)
 *     - (math_double): 2.820..4.156 ms average=3.57 ms (OPTIMIZE+=-g3 -Os)
 *   + FixedNodesUnchanged=1;
 *     - (math_float):  0.847..1.727 ms average=1.41 ms (OPTIMIZE+=-g3)
 *     - (math_float):  0.741..1.584 ms average=1.25 ms (OPTIMIZE+=-g3 -Os)
 *     - (math_double): 1.311..3.328 ms average=2.55 ms (OPTIMIZE+=-g3)
 *     - (math_double): 1.222..3.134 ms average=2.45 ms (OPTIMIZE+=-g3 -Os)
 *
 * + Use_DirectCalculation=1; Use_QuadraticApproximation=0 (fastest but reduced precision fails some regression tests in example_ttc_math!)
 *   + FixedNodesUnchanged=0;
 *     - (math_float):  1.258..1.843 ms average=1.68 ms (OPTIMIZE+=-g3)
 *     - (math_float):  0.963..1.683 ms average=1.57 ms (OPTIMIZE+=-g3 -Os)
 *     - (math_double): 2.009..3.488 ms average=3.08 ms (OPTIMIZE+=-g3)
 *     - (math_double): 1.880..3.192 ms average=2.93 ms (OPTIMIZE+=-g3 -Os)
 *   + FixedNodesUnchanged=1;
 *     - (math_float):  0.808..1.193 ms average=1.07 ms (OPTIMIZE+=-g3)
 *     - (math_float):  0.705..1.120 ms average=0.99 ms (OPTIMIZE+=-g3 -Os)
 *     - (math_double): 1.125..3.062 ms average=2.00 ms (OPTIMIZE+=-g3)
 *     - (math_double): 1.101..2.148 ms average=1.90 ms (OPTIMIZE+=-g3 -Os)
 *
 * + Use_DirectCalculation=0; Use_QuadraticApproximation=1 (high precision but slow)
 *   + FixedNodesUnchanged=0;
 *     - (math_float):  2.149..2.842 ms average=2.54 ms (OPTIMIZE+=-g3)
 *     - (math_float):  1.937..2.655 ms average=2.38 ms (OPTIMIZE+=-g3 -Os)
 *     - (math_double): 2.145..5.166 ms average=4.49 ms (OPTIMIZE+=-g3)
 *     - (math_double): 2.825..4.893 ms average=4.22 ms (OPTIMIZE+=-g3 -Os)
 *   + FixedNodesUnchanged=1;
 *     - (math_float):  1.009..2.336 ms average=1.96 ms (OPTIMIZE+=-g3)
 *     - (math_float):  0.666..2.016 ms average=1.74 ms (OPTIMIZE+=-g3 -Os)
 *     - (math_double): 2.201..4.123 ms average=3.46 ms (OPTIMIZE+=-g3)
 *     - (math_double): 2.176..3.957 ms average=3.34 ms (OPTIMIZE+=-g3 -Os)
 *
 *  Note: Runtime measures will vary with different ttc_math_set_precision() settings and different sin()/cos() implementations!
 *
 * @param Rotated             (t_ttc_math_lateration_3d*)     pointer to rotated coordinate system (must be allocated by caller and can be reused for subsequent calls if fixed points do not change)
 * @param FixedNodesUnchanged (BOOL)                          ==TRUE: fixed nodes F2, F3 did not change since last call with same Cache pointer (avoids recalculations)
 * @param F2                  (t_ttc_math_vector3d_xyz*)      fixed point #2 (first fixed point is origin)
 * @param F3                  (t_ttc_math_vector3d_xyz*)      fixed point #3 (must not be colinear to F2: F3 != k * F2)
 * @param DistanceON          (ttm_number)                    measured distance between origin O (F1) and point N to be laterated
 * @param DistanceF2N         (ttm_number)                    measured distance between fixed point F2 and point N to be laterated
 * @param DistanceF3N         (ttm_number)                    measured distance between fixed point F3 and point N to be laterated
 * @param N1                  (t_ttc_math_int_vector3d_xyz*)  !=NULL: first mathematical solution of laterated coordinates will be stored here
 * @param N2                  (t_ttc_math_int_vector3d_xyz*)  !=NULL: second mathematical solution of laterated coordinates will be stored here
 * @return                    (e_ttc_math_errorcode)          ==0: *N1, *N2 have been calculates successfully; >0: given data does not allow lateration (see argument descriptions above!)
 */
typedef struct { // rotated coordinate system can be reused for subsequent ttc_math_lateration_3d() calls
    ttm_number              RotationX;    // rotation of coordinate system around x-axis
    ttm_number              RotationY;    // rotation of coordinate system around y-axis
    ttm_number              RotationZ;    // rotation of coordinate system around y-axis
    ttm_number              DistanceOF2;  // distance Origin->F2
    ttm_number              DistanceOF3;  // distance Origin->F3
    t_ttc_math_vector3d_xyz F2;           // coordinate system is rotated so that F2 it lies on y-axis
    t_ttc_math_vector3d_xyz F3;           // coordinate system is rotated so that F3 it lies wihin xy-axis
} t_ttc_math_lateration_3d;

e_ttc_math_errorcode ttc_math_lateration_3d( t_ttc_math_lateration_3d* Rotated, BOOL FixedNodesUnchanged, t_ttc_math_vector3d_xyz* F2, t_ttc_math_vector3d_xyz* F3, ttm_number DistanceON, ttm_number DistanceF2N, ttm_number DistanceF3N, t_ttc_math_vector3d_xyz* N1, t_ttc_math_vector3d_xyz* N2 );

/** Sets precision to use for various approximation algorithms.
 *
 * All logarithmic algorithms implement an approximation with configurable precision.
 * Higher precision requires more time to compute the result.
 *
 * @param Precision (e_ttc_math_precision)  !=0: new precision to use; ==0: only query current precision setting
 * @return          (ttm_number)            current precision value (calculated results are within +- returned value)
 */
ttm_number ttc_math_set_precision( e_ttc_math_precision Precision );

/** Calculate inverse of square root of X
 *
 * @param X (ttm_number)  argument to function
 * @return  (ttm_number)  1 / sqrt(X)
 */
ttm_number ttc_math_inv_sqrt( ttm_number X );
#ifndef _driver_math_inv_sqrt // avoids compile error due to macro definition in ttc_math_interface.h
    ttm_number _driver_math_inv_sqrt( ttm_number X );
#endif
#define ttc_math_inv_sqrt  _driver_math_inv_sqrt  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Calculate square root of X
 *
 * @param X (ttm_number)  argument to function
 * @return  (ttm_number)  sqrt(X)
 */
ttm_number ttc_math_sqrt( ttm_number X );
ttm_number _driver_math_sqrt( ttm_number X );
#define ttc_math_sqrt  _driver_math_sqrt  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Rotate two dimensional cartesian point by given angle.
 *
 * Note: This function can also be used to rotate a 3D point around one axis.
 *
 *           Y A
 *             |
 *             |
 *             |    /<-__ +Angle
 *             |   /     \_
 *             |  /    __-
 *             | / __--
 *             |/--
 *   <---------+--------->
 *   -X        |         X
 *             |
 *             |
 *             |
 *             |
 *             |
 *             |
 *          -Y V
 *
 * Typical runtime was measured as high-time of gpio pin TTC_MATH_PIN_LATERATION_3D using an oscilloscope (Tektronix MSO4104):
 *   Settings: ttc_math_Precision==tmp_1m; TTC_ASSERT_MATH_EXTRA==0; critical section inserted
 *   Compiler: arm-none-eabi-gcc (GNU Tools for ARM Embedded Processors) 5.4.1 20160609 (release) [ARM/embedded-5-branch revision 237715
 *   Board:   Olimex STM32-LCD (STM32F107 @ 72MHz)
 *     - (math_float):  115..180 us average= 147 us (OPTIMIZE+=-g3)
 *     - (math_float):  106..169 us average= 126 us (OPTIMIZE+=-g3 -Os)
 *     - (math_double): 166..346 us average= 287 us (OPTIMIZE+=-g3)
 *     - (math_double): 190..317 us average= 242 us (OPTIMIZE+=-g3 -Os)
 *
 *
 * @param X     (ttm_number*)  pointer to x-coordinate of vector to rotate (will be modified)
 * @param Y     (ttm_number*)  pointer to y-coordinate of vector to rotate (will be modified)
 * @param Angle (ttm_number)   =-PI..PI: >0: rotate counter clockwise; <=0: rotate clockwise
 */
void ttc_math_vector2d_rotate( ttm_number* X, ttm_number* Y, ttm_number Angle );

/** Calculate length of 3D vector
 *
 * @param X (ttm_number)  x-coordinate of 3D vector
 * @param Y (ttm_number)  y-coordinate of 3D vector
 * @param Z (ttm_number)  z-coordinate of 3D vector
 * @return  (ttm_number)  length of given vector
 */
ttm_number ttc_math_length_3d( ttm_number DistanceX, ttm_number DistanceY, ttm_number DistanceZ );
ttm_number _driver_math_length_3d( ttm_number DistanceX, ttm_number DistanceY, ttm_number DistanceZ );
#define ttc_math_length_3d  _driver_math_length_3d  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Calculate distance between two points A = (Ax, Ay, Az), B = (Bx, By, Bz) in 3D space
 *
 * @param A (t_ttc_math_vector3d_xyz*)  coordinates of point A
 * @param B (t_ttc_math_vector3d_xyz*)  coordinates of point B
 * @return  (ttm_number)                length of straight line A -> B
 */
ttm_number ttc_math_vector3d_distance( t_ttc_math_vector3d_xyz* A, t_ttc_math_vector3d_xyz* B );
ttm_number _driver_math_vector3d_distance( t_ttc_math_vector3d_xyz* A, t_ttc_math_vector3d_xyz* B );
#define ttc_math_vector3d_distance  _driver_math_vector3d_distance  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Rotate given cartesian vector around 1..3 axis in given order
 *
 * Rotating a 3D vector around axis can give different results for different rotation orders.
 *
 * @param Vector   (t_ttc_math_vector3d_xyz*)  cartesian vector to rotate
 * @param RotateX  (ttm_number)                angle to rotate vector around x-axis (radiant)
 * @param RotateY  (ttm_number)                angle to rotate vector around y-axis (radiant)
 * @param RotateZ  (ttm_number)                angle to rotate vector around z-axis (radiant)
 * @return Vector                              *Vector has been modified
 */
void ttc_math_vector3d_rotate_xyz( t_ttc_math_vector3d_xyz* Vector, ttm_number RotateX, ttm_number RotateY, ttm_number RotateZ );
void ttc_math_vector3d_rotate_xzy( t_ttc_math_vector3d_xyz* Vector, ttm_number RotateX, ttm_number RotateY, ttm_number RotateZ );
void ttc_math_vector3d_rotate_yxz( t_ttc_math_vector3d_xyz* Vector, ttm_number RotateX, ttm_number RotateY, ttm_number RotateZ );
void ttc_math_vector3d_rotate_yzx( t_ttc_math_vector3d_xyz* Vector, ttm_number RotateX, ttm_number RotateY, ttm_number RotateZ );
void ttc_math_vector3d_rotate_zxy( t_ttc_math_vector3d_xyz* Vector, ttm_number RotateX, ttm_number RotateY, ttm_number RotateZ );
void ttc_math_vector3d_rotate_zyx( t_ttc_math_vector3d_xyz* Vector, ttm_number RotateX, ttm_number RotateY, ttm_number RotateZ );

/** Copy coordinates from vector Source to Destination
 *
 * Same as
 *   Destination->X = Source->X;
 *   Destination->Y = Source->Y;
 *   Destination->Z = Source->Z;
 *
 * @param Target (t_ttc_math_vector3d_xyz*)  vector to write x,y,z coordinates to
 * @param Source (t_ttc_math_vector3d_xyz*)  vector to read x,y,z coordinates from
 */
void ttc_math_vector3d_copy( t_ttc_math_vector3d_xyz* Target, t_ttc_math_vector3d_xyz* Source );

/** Add one vector to another
 *
 * Same as
 *   Target->X += Add->X;
 *   Target->Y += Add->Y;
 *   Target->Z += Add->Z;
 *
 * @param Target (t_ttc_math_vector3d_xyz*)  coordinates of vector will be changed
 * @param Add    (t_ttc_math_vector3d_xyz*)  vector to add
 */
void ttc_math_vector3d_add( t_ttc_math_vector3d_xyz* Target, t_ttc_math_vector3d_xyz* Add );

/** Subtract one vector from another
 *
 * Same as
 *   Target->X -= Add->X;
 *   Target->Y -= Add->Y;
 *   Target->Z -= Add->Z;
 *
 * @param Target   (t_ttc_math_vector3d_xyz*)  coordinates of vector will be changed
 * @param Subtract (t_ttc_math_vector3d_xyz*)  vector to subtract
 */
void ttc_math_vector3d_sub( t_ttc_math_vector3d_xyz* Target, t_ttc_math_vector3d_xyz* Subtract );

/** Calculate length of 3D vector
 *
 * @param  Vector (t_ttc_math_vector3d_xyz*)  vector to calculate length of
 * @return calculated length = sqrt(Vector->X² + Vector->Y² + Vector->Z²)
 */
ttm_number ttc_math_vector3d_length( t_ttc_math_vector3d_xyz* Vector );

/** Calculate length of 2D vector
 *
 * @param  Vector (t_ttc_math_vector3d_xyz*)  vector to calculate length of
 * @return calculated length = sqrt(Vector->X² + Vector->Y²)
 */
ttm_number ttc_math_vector2d_length( t_ttc_math_vector2d_xy* Vector );

/** Multiply all coordinates of a vector by a scalar value
 *
 * Same as
 *   Target->X *= Scale;
 *   Target->Y *= Scale;
 *   Target->Z *= Scale;
 *
 * @param Target (t_ttc_math_vector3d_xyz*)  coordinates of vector will be changed
 * @param Scale  (ttm_number)                numeric scalar
 */
void ttc_math_vector3d_scale( t_ttc_math_vector3d_xyz* Vector, ttm_number Scale );

/** Load signed or unsigned integer type variable or constant into ttm_number Variable
 *
 * For ttm_number = float or double, this is simply a macro using = to assign the value.
 * Fixed-point low-level ttc_math drivers may require more effort to load the value.
 * Use of this function instead of simple = allows to switch from floating point to
 * fixed point mathematic support without changing your application code.
 *
 * @param Value (t_base_signed)  value to load
 * @return      (ttm_number)     given value in fixed or floating point format as supported by current low-level driver
 */
ttm_number ttc_math_from_int( t_base_signed Value );
#if 0 // must disable _driver_*() prototype to allow simple macro (line is still required to propagate function to all low-level drivers via create_DeviceDriver.pl)
    ttm_number _driver_math_from_int( t_base_signed Value );
#endif
#define ttc_math_from_int(Value)  _driver_math_from_int(Value)  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Load single precision floating point type variable or constant into ttm_number Variable
 *
 * For ttm_number = float or double, this is simply a macro using = to assign the value.
 * Fixed-point low-level ttc_math drivers may require more effort to load the value.
 * Use of this function instead of simple = allows to switch from floating point to
 * fixed point mathematic support without changing your application code.
 *
 * @param Value (float)          value to load
 * @return      (ttm_number)     given value in fixed or floating point format as supported by current low-level driver
 */
ttm_number ttc_math_from_float( float Value );
#if 0 // must disable _driver_*() prototype to allow simple macro (line is still required to propagate function to all low-level drivers via create_DeviceDriver.pl)
    ttm_number _driver_math_from_float( float Value );
#endif
#define ttc_math_from_float(Value)  _driver_math_from_float(Value)  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Load double precision floating point type variable or constant into ttm_number Variable
 *
 * For ttm_number = float or double, this is simply a macro using = to assign the value.
 * Fixed-point low-level ttc_math drivers may require more effort to load the value.
 * Use of this function instead of simple = allows to switch from floating point to
 * fixed point mathematic support without changing your application code.
 *
 * @param Value (double)         value to load
 * @return      (ttm_number)     given value in fixed or floating point format as supported by current low-level driver
 */
ttm_number ttc_math_from_double( double Value );
#if 0 // must disable _driver_*() prototype to allow simple macro (line is still required to propagate function to all low-level drivers via create_DeviceDriver.pl)
    ttm_number _driver_math_from_double( double Value );
#endif
#define ttc_math_from_double(Value)  _driver_math_from_double(Value)  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Load signed or unsigned integer type variable or constant into ttm_number Variable
 *
 * For ttm_number = float or double, this is simply a macro using = to assign the value.
 * Fixed-point low-level ttc_math drivers may require more effort to load the value.
 * Use of this function instead of simple = allows to switch from floating point to
 * fixed point mathematic support without changing your application code.
 *
 * @param Value (ttm_number)    value in fixed or floating point format as supported by current low-level driver
 * @return      (t_base_signed) Value being converted to signed integer
 */
t_base_signed ttc_math_to_int( ttm_number Value );
#if 0 // must disable _driver_*() prototype to allow simple macro (line is still required to propagate function to all low-level drivers via create_DeviceDriver.pl)
    t_base_signed _driver_math_to_int( ttm_number Value );
#endif
#define ttc_math_to_int(Value)  _driver_math_to_int(Value)  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Load single precision floating point type variable or constant into ttm_number Variable
 *
 * For ttm_number = float or double, this is simply a macro using = to assign the value.
 * Fixed-point low-level ttc_math drivers may require more effort to load the value.
 * Use of this function instead of simple = allows to switch from floating point to
 * fixed point mathematic support without changing your application code.
 *
 * @param Value (ttm_number)  value in fixed or floating point format as supported by current low-level driver
 * @return      (float)       Value being converted to single precision floating point format
 */
float ttc_math_to_float( ttm_number Value );
#if 0 // must disable _driver_*() prototype to allow simple macro (line is still required to propagate function to all low-level drivers via create_DeviceDriver.pl)
    float _driver_math_to_float( ttm_number Value );
#endif
#define ttc_math_to_float(Value)  _driver_math_to_float(Value)  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Load double precision floating point type variable or constant into ttm_number Variable
 *
 * For ttm_number = float or double, this is simply a macro using = to assign the value.
 * Fixed-point low-level ttc_math drivers may require more effort to load the value.
 * Use of this function instead of simple = allows to switch from floating point to
 * fixed point mathematic support without changing your application code.
 *
 * @param Value (ttm_number)  value in fixed or floating point format as supported by current low-level driver
 * @return      (double)      Value being converted to single precision floating point format
 */
double ttc_math_to_double( ttm_number Value );
#if 0 // must disable _driver_*() prototype to allow simple macro (line is still required to propagate function to all low-level drivers via create_DeviceDriver.pl)
    double _driver_math_to_double( ttm_number Value );
#endif
#define ttc_math_to_double(Value)  _driver_math_to_double(Value)  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Return absolute positive value of given positive or negative value
 *
 * @param X (ttm_number)  positive or negativ value
 * @return  (ttm_number)  abs(X)
 */
ttm_number ttc_math_abs( ttm_number X );
ttm_number _driver_math_abs( ttm_number X );
#define ttc_math_abs(X)  _driver_math_abs(X)  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Checks all coordinates of given vector are valid (!= TTC_MATH_CONST_NAN)
 *
 * @param Vector (t_ttc_math_vector2d_xy*)  vector to check
 * @return       (BOOL)                     ==TRUE: Vector->X and Vector->Y are valid numbers
 */
BOOL ttc_math_vector2d_valid( const t_ttc_math_vector2d_xy* Vector );

/** Checks all coordinates of given vector are valid (!= TTC_MATH_CONST_NAN)
 *
 * @param Vector (t_ttc_math_vector3d_xyz*)  vector to check
 * @return       (BOOL)                      ==TRUE: Vector->X, Vector->Y and Vector->Z are valid numbers
 */
BOOL ttc_math_vector3d_valid( const t_ttc_math_vector3d_xyz* Vector );
#if 0 // must disable _driver_*() prototype to allow simple macro (line is still required to propagate function to all low-level drivers via create_DeviceDriver.pl)
    BOOL _driver_math_vector3d_valid( const t_ttc_math_vector3d_xyz* Vector );
#endif
#define ttc_math_vector3d_valid  _driver_math_vector3d_valid // enable to directly forward function call to low-level driver (remove function definition before!)

/** Calculates intersection point of two lines in two dimensional space
*
* @param Line1_Point   (t_ttc_math_vector2d_xy*)  coordinates of one point on line 1
* @param Line1_Incline (t_ttc_math_vector2d_xy*)  inclination vector of line 1
* @param Line1_Point   (t_ttc_math_vector2d_xy*)  coordinates of one point on line 2
* @param Line1_Incline (t_ttc_math_vector2d_xy*)  inclination vector of line 2
* @param Intersection  (t_ttc_math_vector2d_xy*)  if intersection point exists, its coordinates will be stored in this vector
*/
void ttc_math_intersection_2d( t_ttc_math_vector2d_xy* Line1_Point, t_ttc_math_vector2d_xy* Line1_Incline, t_ttc_math_vector2d_xy* Line2_Point, t_ttc_math_vector2d_xy* Line2_Incline, t_ttc_math_vector2d_xy* Intersection );

/** Calculate 2D inclination vector B - A
*
 * @param A (t_ttc_math_vector2d_xy*)  2D coordinates of point A
 * @param B (t_ttc_math_vector2d_xy*)  2D coordinates of point B
 * @return  (t_ttc_math_vector2d_xy)   2D vector from A to B
 */
t_ttc_math_vector2d_xy ttc_math_vector2d_incline( t_ttc_math_vector2d_xy* A, t_ttc_math_vector2d_xy* B );

//InsertFunctionDeclarations
//}

#endif //TTC_MATH_H
