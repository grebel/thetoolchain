#ifndef TTC_I2C_H
#define TTC_I2C_H
/** { ttc_i2c.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for I2C devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_I2C(tc_i2c_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_i2c_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_i2c_init(LogicalIndex);
 *  4) use:         ttc_i2c_XXX(LogicalIndex);
 *
 *  Device Index
 *  Devices are addressed by a logical index with no correlation to the order of physical devices.
 *  Logical indices start at 1 to simply detect uninitialized values.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level i2c and application.
 *
 *  Created from template ttc_device.h revision 33 at 20150527 12:22:44 UTC
 *
 *  Authors: Gregor Rebel
 *
 *
 *  How it works
 *
 *  Each makefile.100_board* can define one or more I2Cs being routed to connectors.
 *  This is done via lines of this type:
 *    COMPILE_OPTS += -D<OPTION>=<VALUE>
 *  This sets compile option <OPTION> to value <VALUE>
 *
 *  Each I2Cn can be configured via these constants:
 *  TTC_I2C1       t_u8             sets internal I2C device to use (ttc_device_1, ...)
 *
 *  Some microcontrollers allow to use alternate pins for same I2C device.
 *  This is why each pin has to be defined individually.
 *  TTC_I2C1_SDA   e_ttc_gpio_pin   sets port to use for Serial Data pin
 *  TTC_I2C1_SCL   e_ttc_gpio_pin   sets port to use for Serial Clock pin
 *  TTC_I2C1_SMBAL e_ttc_gpio_pin   sets port to use for SMB pin
 *
 *  # Example excerpt from makefile.100_board_olimex_p107
 *  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1  # internal device being used for this port
 *  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b7    # EXT1-19
 *  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b6    # EXT1-18
 *  COMPILE_OPTS += -DTTC_I2C1_SMBAL=E_ttc_gpio_pin_b5  # EXT1-17
 *
 *  The values of TTC_I2C1 and other defines can be found in compile_options.h or
 *  extensions.active/makefile AFTER a successfull compilation run.
 *
 *  Basic Usage of ttc_i2c Driver
 *
 *  I2C configuration
 *  ttc_i2c is configured like any other ttc-driver by
 *  1) requesting a configuration pointer
 *  2) Change configuration
 *  3) Initialize driver
 *
 *  t_ttc_i2c_config* Config = ttc_i2c_get_configuration(1);
 *  ... // change Config
 *  ttc_i2c_init(1);
 *
 *  I2C Function Groups
 *  The functions are divided into two main groups: master and slave
 *  The interface is assumend to work in slave mode until it starts a master sequence.
 *
 *  Master sequences always have this form:
 *  1) ttc_i2c_wait_until_event(LogicalIndex, E_ttc_i2c_event_code_bus_free);
 *  2) ttc_i2c_master_condition_start(LogicalIndex);
 *  ...
 *  n) ttc_i2c_master_condition_stop(LogicalIndex);
 *
 *  A slave sequence starts by receiving a matching own address:
 *
 *  while (1) {
 *    if ( ttc_i2c_slave_check_own_address_received(1) ) { // master is calling us
 *      if ( ttc_i2c_slave_check_read_mode(1) == 0) { // master wants to send us data
 *        // read data from I2C bus
 *      }
 *      else {                                        // master wants to read data from us
 *        // send data to I2C bus
 *      }
 *
 *     ...
 *   }
}*/

#ifndef EXTENSION_ttc_i2c
#error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_i2c.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_i2c.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "i2c/i2c_common.h"
#include "interfaces/ttc_i2c_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level i2c only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * i2c devices on all supported architectures.
 * Check i2c/i2c_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares i2c Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_i2c_prepare();
void _driver_i2c_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_i2c_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_I2C_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
t_ttc_i2c_config* ttc_i2c_get_configuration( t_u8 LogicalIndex );

/** fills out given Config with maximum valid values for indexed I2C
 * @param LogicalIndex    index of device to init (1..ttc_I2C_get_max_LogicalIndex())
 * @return                pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_i2c_config* ttc_i2c_get_features( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_I2C_get_max_LogicalIndex())
 * @return                == 0: i2c device has been initialized successfully; != 0: error-code
 */
e_ttc_i2c_errorcode ttc_i2c_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_I2C_get_max_LogicalIndex())
 */
void ttc_i2c_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_i2c_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of i2c device (1..ttc_i2c_get_max_index() )
 * @return                == 0: default values have been loaded successfully for indexed i2c device; != 0: error-code
 */
e_ttc_i2c_errorcode  ttc_i2c_load_defaults( t_u8 LogicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_I2C_get_max_LogicalIndex())
 */
void ttc_i2c_reset( t_u8 LogicalIndex );

/** ttc_i2c_master_write_register() - Generic data write operation to registers inside slave device
 *
 * Some I2C slave devices organize their configuration in so called registers.
 * Each register has a register address and a size. The exact details are described
 * in the datasheet of each I2C device (e.g. a MPU6050 Accelerometer).
 * A register can be read or written by a register read or write sequence.
 * Writing to a register will change the configuration of your I2C slave device.
 * Reading from a register reads sensor data from the I2C slave device (e.g. a temperature).
 *
 * Register Write Sequence
 * 1) Wait until I2C bus is free
 * 2) Send Start Condition
 * 3) Send Slave Address including a read bit (7 bits slave address + 1 read bit)
 *    with read bit = 0
 * 4) Send register address (1 byte) according to datasheet
 * 5) Send amount of bytes to write in next step (1 byte)
 * 6) Send data to write into register
 * 7) Send Stop Condition
 *
 * ttc_i2c_master_write_register() implements the whole sequence in a single function call.
 *
 * Master Write Register Example:
 * t_ttc_i2c_config* Config = ttc_i2c_get_configuration(1);
 * ... // change Config
 * ttc_i2c_init(1);
 *
 * t_u8 Buffer[4] = { 1, 2, 3, 4 };
 *
 * ttc_i2c_wait_until_event(1, E_ttc_i2c_event_code_bus_free);
 * ttc_i2c_master_condition_start(1);
 * ttc_i2c_master_send_slave_address(1, 0x42, 1);                // 0x42 == slave I2C address
 * ttc_i2c_master_send_register_adddress(1, 0x12, E_ttc_i2c_address_type_Default); // 0x12 = register address on slave
 * ttc_i2c_master_send_bytes(1, 4, Buffer);                      // read 4 bytes into Buffer[]
 * ttc_i2c_master_condition_stop(1);
 *
 * @param LogicalIndex    (t_u8)                    logical index of I2C device to use (1==TTC_I2C1, ...)
 * @param SlaveAddress    (t_u16)                   7- or 10-bit address of slave device on I2C bus
 * @param RegisterAddress (t_u32)                   8-, 16- or 32-bit register address inside slave device
 * @param AddressType     (e_ttc_i2c_address_type)  you may use E_ttc_i2c_address_type_Default in most cases (-> ttc_i2c_types.h/e_ttc_i2c_address_type)
 * @param AmountBytes     (t_u8)                    amount of bytes to write into register (most slaves allow auto address increment)
 * @param Buffer          (const t_u8*)             memory buffer to read data to be sent from
 * @return                (e_ttc_i2c_errorcode)     == 0: register write operation was successfull, error-code instead (operation may timeout)
**/
e_ttc_i2c_errorcode ttc_i2c_master_write_register( t_u8 LogicalIndex, t_u16 SlaveAddress, t_u32 RegisterAddress, e_ttc_i2c_address_type AddressType, t_u8 AmountBytes, const t_u8* Buffer );

/** ttc_i2c_master_read_register() - Generic data read operation from registers inside slave device
 *
 * See description of ttc_i2c_master_write_register() above for details about I2C slave registers.
 *
 * Register Read Sequence
 * 1) Wait until I2C bus is free
 * 2) Send Start Condition
 * 3) Send Slave Address including a read bit (7 bits slave address + 1 read bit)
 *    with read bit = 0
 * 4) Send register address (1 byte) according to datasheet
 * 5) Send amount of bytes to read in next step (1 byte)
 * 6) Send Slave Address including a read bit (7 bits slave address + 1 read bit)
 *    with read bit = 1 (switches bus to read mode)
 * 7) Read data from slave register
 * 8) Send Stop Condition
 *
 * ttc_i2c_master_read_register() implements the whole sequence in a single function call.
 *
 * Master Read Register Example:
 * t_ttc_i2c_config* Config = ttc_i2c_get_configuration(1);
 * ... // change Config
 * ttc_i2c_init(1);
 *
 * t_u8 Buffer[4];
 *
 * ttc_i2c_wait_until_event(LogicalIndex, E_ttc_i2c_event_code_bus_free);
 * ttc_i2c_master_condition_start(1);
 * ttc_i2c_master_send_slave_address(1, 0x42, 1);                // 0x42 == slave I2C address
 * ttc_i2c_master_send_register_adddress(1, 0x12, E_ttc_i2c_address_type_Default); // 0x12 = register address on slave
 * ttc_i2c_master_condition_start(1);
 * ttc_i2c_master_send_slave_address(1, 0x42, 0);                // 0x42 == slave I2C address + switch to read mode
 * ttc_i2c_master_read_bytes(1, 4, Buffer);                      // read 4 bytes into Buffer[]
 * ttc_i2c_master_condition_stop(1);
 *
 * @param LogicalIndex    (t_u8)                    logical index of I2C device to use (1==TTC_I2C1, ...)
 * @param SlaveAddress    (t_u16)                   7- or 10-bit address of slave device on I2C bus
 * @param RegisterAddress (t_u32)                   8-, 16- or 32-bit register address inside slave device
 * @param AddressType     (e_ttc_i2c_address_type)  you may use E_ttc_i2c_address_type_Default in most cases (-> ttc_i2c_types.h/e_ttc_i2c_address_type)
 * @param AmountBytes     (t_u8)                    amount of bytes to write into register (most slaves allow auto address increment)
 * @param Buffer          (const t_u8*)             memory buffer to read data to be sent from
 * @return                (e_ttc_i2c_errorcode)     == 0: register write operation was successfull, error-code instead (operation may timeout)
 */
e_ttc_i2c_errorcode ttc_i2c_master_read_register( t_u8 LogicalIndex, t_u16 SlaveAddress, t_u32 RegisterAddress, e_ttc_i2c_address_type AddressType, t_u8 AmountBytes, t_u8* Buffer );

/** Inform ttc_i2c about a changed system clock setting.
 *
 * After changing system clock setting, the interface may have to be reinitialized to adapt to new clock frequencies.
 *
 * Note: This function is normally called automatically from ttc_sysclock_profile_switch().
 *       It should not be required to call it from outside.
 */
void ttc_i2c_sysclock_changed();

/** ttc_i2c_master_send_slave_address() - Sends out given slave address
 *
 * Whenever a master wants to talk to an I2C slave device, the master has to send the address of desired
 * slave device. The address carries a 7 bit slave address (in 7 bit addressing mode) and a single read bit.
 * if read bit == 1 then the address transmission will switch the I2C bus from write- into read-mode.
 *
 * Send Slave Address Example:
 * t_ttc_i2c_config* Config = ttc_i2c_get_configuration(1);
 * ... // change Config
 * ttc_i2c_init(1);
 *
 * ttc_i2c_wait_until_event(LogicalIndex, E_ttc_i2c_event_code_bus_free);
 * ttc_i2c_master_condition_start(1);
 *
 * // send slave address 0x42 and switch to read mode
 * ttc_i2c_master_send_slave_address(1, 0x42, 1);
 * ...
 *
 * // send slave address 0x42 and stay in write mode
 * ttc_i2c_master_send_slave_address(1, 0x42, 0);
 * ...
 *
 * @param LogicalIndex (t_u8)                device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param SlaveAddress (t_u16)               I2C address of desired slave to send (only lower 7- or 10-bits are used)
 * @param ReadMode     (BOOL)                ==TRUE: Switch interface into read mode after sending address; write mode otherwise
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
e_ttc_i2c_errorcode ttc_i2c_master_send_slave_address( t_u8 LogicalIndex, t_u16 SlaveAddress, BOOL ReadMode );
e_ttc_i2c_errorcode _driver_i2c_master_send_slave_address( t_ttc_i2c_config* Config, t_u16 SlaveAddress, BOOL ReadMode );

/** ttc_i2c_master_condition_start() - Generates a start condition on I2C bus.
 *
 * A stop condition marks the beginning of every master sequence.
 *
 * @param LogicalIndex (t_u8)                device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param Config       (t_ttc_i2c_config*)   configuration of I2C device
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
e_ttc_i2c_errorcode ttc_i2c_master_condition_start( t_u8 LogicalIndex );
e_ttc_i2c_errorcode _driver_i2c_master_condition_start( t_ttc_i2c_config* Config );

/** ttc_i2c_master_condition_stop() - Generates a stop condition on I2C bus.
 *
 * A stop condition marks the end of every master sequence.
 *
 * @param LogicalIndex (t_u8)                device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param Config       (t_ttc_i2c_config*)   configuration of I2C device
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
e_ttc_i2c_errorcode ttc_i2c_master_condition_stop( t_u8 LogicalIndex );
e_ttc_i2c_errorcode _driver_i2c_master_condition_stop( t_ttc_i2c_config* Config );

/** ttc_i2c_master_reset_bus() - Reset I2C bus in case a slave is blocking the bus
 *
 * Sometimes slave devices get out of sync with master. Sending out hundrets of
 * clock cycles should reset all connected I2C slaves.
 * Will deinit I2C device, send out several clock pulses and reinit I2C device again.
 *
 * @param LogicalIndex (t_u8)  device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
e_ttc_i2c_errorcode ttc_i2c_master_reset_bus( t_u8 LogicalIndex );

/** ttc_i2c_master_send_register_adddress() - Send register address to slave.
 *
 * @param LogicalIndex    (t_u8)                     device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param RegisterAddress (t_u32)                    register address inside slave device
 * @param AddressType     (e_ttc_i2c_address_type)   some slaves provide 16- or 32-bit wide register addresses
 * @return                (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
e_ttc_i2c_errorcode ttc_i2c_master_send_register_adddress( t_u8 LogicalIndex, t_u32 RegisterAddress, e_ttc_i2c_address_type AddressType );

/** ttc_i2c_slave_check_own_address_received() - Checks in slave mode if an address matching to OwnAddress has been received
 *
 * In slave mode, the I2C interface is constantly watching the lines for an incoming slave address.
 * If this address matches OwnAddress1 or OwnAddress2 (in dual addressing mode), then another
 * master is calling us. An application implementing an I2C slave should periodically call
 * ttc_i2c_slave_check_own_address_received() to check if it has been called or register a
 * callback function.
 *
 * @param LogicalIndex (t_u8)  device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param Config       (t_ttc_i2c_config)    Configuration of i2c device
 * @return             (BOOL)  !=0: someone is calling us; ==0: no matching address received
 */
BOOL ttc_i2c_slave_check_own_address_received( t_u8 LogicalIndex );

/** ttc_i2c_slave_check_read_mode() - Checks in slave mode if last received address implied a read mode.
 *
 * Calling this function makes only sense after ttc_i2c_slave_check_own_address_received()
 * returned TRUE.
 *
 * @param LogicalIndex (t_u8)  device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param Config       (t_ttc_i2c_config)    Configuration of i2c device
 * @return             (t_u8)  ==TRUE: read mode, write mode otherwise
 */
BOOL ttc_i2c_slave_check_read_mode( t_u8 LogicalIndex );

/** ttc_i2c_slave_read_bytes() - Reads bytes being sent by master
 *
 * A slave may only read bytes from a master after receiving its own address with read bit = 0!
 * Use ttc_i2c_slave_check_read_mode() to find out state of read bit of last received slave address.
 *
 * Slave Read Bytes Example:
 * t_ttc_i2c_config* Config = ttc_i2c_get_configuration(1);
 * ... // change Config
 * ttc_i2c_init(1);
 *
 * t_u8 Buffer[4];
 *
 * while (1) {
 *   if ( ttc_i2c_slave_check_own_address_received(1) ) { // master is calling us
 *     if ( ttc_i2c_slave_check_read_mode(1) == 0) { // master wants to send us data
 *       ttc_i2c_slave_read_bytes(t_u8 LogicalIndex, 4, Buffer);
 *     }
 *   }
 * }
 *
 * Calling this function makes only sense after ttc_i2c_slave_check_own_address_received()
 * and ttc_i2c_slave_check_read_mode() both returned TRUE.
 *
 * @param LogicalIndex (t_u8)  device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param Config       (t_ttc_i2c_config)    Configuration of i2c device
 * @param BufferSize   (t_u8)  no more than this amount of bytes will be read
 * @param Buffer       (t_u8*) received bytes will be stored in Buffer[0..BufferSize-1]
 * @return             (t_u8)  amount of bytes being received
 */
t_u8 ttc_i2c_slave_read_bytes( t_u8 LogicalIndex, t_u8 BufferSize, t_u8* Buffer );
t_u8 _driver_i2c_slave_read_bytes( t_ttc_i2c_config* Config, t_u8 BufferSize, t_u8* Buffer );

/** ttc_i2c_slave_send_bytes() - Sends bytes to master
 *
 * A slave may only send bytes to a master after receiving its own address with read bit = 1!
 * Use ttc_i2c_slave_check_read_mode() to find out state of read bit of last received slave address.
 *
 * Slave Send Bytes Example:
 * t_ttc_i2c_config* Config = ttc_i2c_get_configuration(1);
 * ... // change Config
 * ttc_i2c_init(1);
 *
 * t_u8 Buffer[4] = {1, 2, 3, 4};
 *
 * while (1) {
 *   if ( ttc_i2c_slave_check_own_address_received(1) ) { // master is calling us
 *     if ( ttc_i2c_slave_check_read_mode(1) ) { // master wants to read data from us
 *       ttc_i2c_slave_send_bytes(t_u8 LogicalIndex, 4, Buffer);
 *     }
 *   }
 * }
 *
 * @param LogicalIndex (t_u8)  device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param Config       (t_ttc_i2c_config)    Configuration of i2c device
 * @param BufferSize   (t_u8)  no more than this amount of bytes will be send
 * @param Buffer       (t_u8*) bytes will be sent from Buffer[0..BufferSize-1]
 * @return             (t_u8)  amount of bytes being received
 */
t_u8 ttc_i2c_slave_send_bytes( t_u8 LogicalIndex, t_u8 BufferSize, const t_u8* Buffer );
t_u8 _driver_i2c_slave_send_bytes( t_ttc_i2c_config* Config, t_u8 BufferSize, const t_u8* Buffer );

/** ttc_i2c_master_read_bytes() - Generic receiving of individual bytes from I2C bus in master mode
 *
 * Note: Reading of individual bytes only makes sense if someone on the bus is willing to send at the moment.
 *
 * Master Read Bytes Example:
 * t_ttc_i2c_config* Config = ttc_i2c_get_configuration(1);
 * ... // change Config
 * ttc_i2c_init(1);
 *
 * t_u8 Buffer[4];
 *
 * ttc_i2c_wait_until_event(LogicalIndex, E_ttc_i2c_event_code_bus_free);
 * ttc_i2c_master_condition_start(1);
 * ttc_i2c_master_send_slave_address(1, 0x42, 1); // 0x42 == slave I2C address
 * ttc_i2c_master_read_bytes(1, 4, Buffer);       // read 4 bytes from I2C bus into Buffer[]
 * ttc_i2c_master_condition_stop(1);
 *
 * @param LogicalIndex  (t_u8)               device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param Config       (t_ttc_i2c_config)    Configuration of i2c device
 * @param Amount       (t_u8)                amount of bytes to read
 * @param Buffer       (t_u8*)               bytes being read will be stored in this buffer
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
e_ttc_i2c_errorcode  ttc_i2c_master_read_bytes( t_u8 LogicalIndex, t_u8 AmountBytes, t_u8* Buffer );
e_ttc_i2c_errorcode _driver_i2c_master_read_bytes( t_ttc_i2c_config* Config, t_u8 Amount, t_u8* Buffer );

/** ttc_i2c_master_send_bytes() - Generic sending of individual bytes to I2C bus in master mode.
 *
 * Note: Sending of individual bytes only makes sense if someone on the bus is expecting this at the moment.
 *
 * Master Send Bytes Example:
 * t_ttc_i2c_config* Config = ttc_i2c_get_configuration(1);
 * ... // change Config
 * ttc_i2c_init(1);
 *
 * t_u8 Buffer[4] = {1, 2, 3, 4};
 *
 * ttc_i2c_master_condition_start(1);
 * ttc_i2c_master_send_slave_address(1, 0x42, 0); // 0x42 == slave I2C address
 * ttc_i2c_master_send_bytes(1, 4, Buffer);
 * ttc_i2c_master_condition_stop(1);
 *
 * @param LogicalIndex (t_u8)                device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param Amount       (t_u8)                amount of bytes to write
 * @param Buffer       (const t_u8*)         bytes to send will be read from this buffer
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
e_ttc_i2c_errorcode ttc_i2c_master_send_bytes( t_u8 LogicalIndex, t_u8 Amount, const t_u8* Buffer );

/** resets interface to release SDA and SCK lines in slave mode
 *
 * @param LogicalIndex (t_u8)  device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param Config       (t_ttc_i2c_config)    Configuration of i2c device
 * @return             (e_ttc_i2c_errorcode) ==0: reset went successfull; error-code otherwise
 */
e_ttc_i2c_errorcode ttc_i2c_slave_reset_bus( t_u8 LogicalIndex );
e_ttc_i2c_errorcode _driver_i2c_slave_reset_bus( t_ttc_i2c_config* Config );

/** Checks if given event value contains event for given event code
 *
 * See ttc_i2c_get_event_value() for I2C event description
 *
 * @param EventValue (t_base)               return value from ttc_i2c_get_event_value()
 * @param EventCode  (e_ttc_i2c_event_code) assembly of bits defining a certain I2C event
 * @return           (BOOL)                 !=0: event is contained in EventValue; ==0: event is not contained
 */
BOOL ttc_i2c_check_event( t_base EventValue, e_ttc_i2c_event_code EventCode );
#define ttc_i2c_check_event(EventValue, EventCode) i2c_common_check_event(EventValue, EventCode)

/** Returns current state of flag corresponding to given flag code
 *
 * @param LogicalIndex  (t_u8)  device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param BaseRegister (t_ttc_i2c_base_register*)  address of base of peripheral I2C register
 * @param FlagCode      (e_ttc_i2c_flag_code)      code identifying single status register and single state flag
 * @return              (BOOL)
 */
BOOL ttc_i2c_check_flag( t_u8 LogicalIndex, e_ttc_i2c_flag_code FlagCode );
BOOL _driver_i2c_check_flag( volatile t_ttc_i2c_base_register* BaseRegister, e_ttc_i2c_flag_code FlagCode );

/** Reads all I2C flags for given register base and assembles them into an event value
 *
 * An I2C Event is a combination of one or more state flags.
 * State flags may be distributed over two or more registers on current architecture.
 * An event value is the assembly of state flags from several registers into a 32 bit value.
 * Reading all state flags typically resets certain state flags. If code should wait for
 * more than one event, then the last even value has to be compared to all corresponding
 * event codes (->ttc_i2c_types.h:e_ttc_i2c_event_code).
 *
 * @param LogicalIndex (t_u8)  device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param BaseRegister (t_ttc_i2c_base_register*)  address of base of peripheral I2C register
 * @return             (t_base)                    assembly of all state flags (architecture dependent)
 */
t_base ttc_i2c_get_event_value( t_u8 LogicalIndex );
t_base _driver_i2c_get_event_value( volatile t_ttc_i2c_base_register* BaseRegister );

/** waits until event for given event code or timeout occurs
 *
 * @param LogicalIndex  (t_u8)                  device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param EventCode     (e_ttc_i2c_event_code)  code identifying all flag bits required for event
 * @return              (e_ttc_i2c_event_code)  ==0: no event from EventCodes[] did not occure before timeout; event code otherwise
 */
e_ttc_i2c_event_code ttc_i2c_wait_until_event( t_u8 LogicalIndex, e_ttc_i2c_event_code EventCode );

/** Waits until one of severals events for given event codes takes place or timeout occurs
 *
 * @param LogicalIndex  (t_u8)                           device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param EventCodes    (const e_ttc_i2c_event_code [])  zero terminated array of event codes
 * @return              (e_ttc_i2c_event_code)           ==0: no event from EventCodes[] did not occure before timeout; event code otherwise
 */
e_ttc_i2c_event_code ttc_i2c_wait_until_events( t_u8 LogicalIndex, const e_ttc_i2c_event_code EventCodes[] );

/** Enables/ Disables acknowledgement of incoming bytes
 *
 * @param LogicalIndex (t_u8)  device index of I2C device to use (1..ttc_I2C_get_max_LogicalIndex())
 * @param BaseRegister (t_ttc_i2c_base_register*)  address of base of peripheral I2C register
 */
void ttc_i2c_enable_acknowledge( t_u8 LogicalIndex, BOOL Enable );
void _driver_i2c_enable_acknowledge( volatile t_ttc_i2c_base_register* BaseRegister, BOOL Enable );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function Declarations
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_i2c_ are passed to interfaces/ttc_i2c_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl i2c UPDATE
 */

/** Generic sending of individual bytes to I2C bus in master mode.
 *
 * Note: Sending of individual bytes only makes sense if someone on the bus is expecting this at the moment.
 *
 * @param Config       (t_ttc_i2c_config)    Configuration of i2c device
 * @param Amount       (t_u8)                amount of bytes to write
 * @param Buffer       (const t_u8*)         bytes to send will be read from this buffer
 * @return             (e_ttc_i2c_errorcode) ==0: operation completed successfully, error code otherwise
 */
e_ttc_i2c_errorcode _driver_i2c_master_send_bytes( t_ttc_i2c_config* Config, t_u8 Amount, const t_u8* Buffer );


/** Checks in slave mode if an address matching to OwnAddress has been received
 *
 * In slave mode, the I2C interface is constantly watching the lines for an incoming slave address.
 * If this address matches OwnAddress1 or OwnAddress2 (in dual addressing mode), then another
 * master is calling us.
 *
 * @param BaseRegister (t_ttc_i2c_base_register*)  address of base of peripheral I2C register
 * @return             (BOOL)  !=0: someone is calling us; ==0: no matching address received
 */
BOOL _driver_i2c_slave_check_own_address_received( volatile t_ttc_i2c_base_register* BaseRegister );

/** Checks in slave mode if last received address implied a read mode.
 *
 * Calling this function makes only sense after ttc_i2c_slave_check_own_address_received()
 * returned TRUE.
 *
 * @param BaseRegister (t_ttc_i2c_base_register*)  address of base of peripheral I2C register
 * @return             (t_u8)  ==TRUE: master wants to read data, master wants to write data
 */
BOOL _driver_i2c_slave_check_read_mode( volatile t_ttc_i2c_base_register* BaseRegister );

/** fills out given Config with maximum valid values for indexed I2C
 * @param Config  = Configuration of i2c device
 * @return        pointer to configuration loaded with maximum allowed values for current architecture
 */
t_ttc_i2c_config* _driver_i2c_get_features( t_ttc_i2c_config* Config );

/** shutdown single I2C unit device
 * @param Config        Configuration of i2c device
 * @return              == 0: I2C has been shutdown successfully; != 0: error-code
 */
e_ttc_i2c_errorcode _driver_i2c_deinit( t_ttc_i2c_config* Config );

/** initializes single I2C unit for operation
 * @param Config        Configuration of i2c device
 * @return              == 0: I2C has been initialized successfully; != 0: error-code
 */
e_ttc_i2c_errorcode _driver_i2c_init( t_ttc_i2c_config* Config );

/** loads configuration of indexed I2C unit with default values
 * @param Config        Configuration of i2c device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_i2c_errorcode _driver_i2c_load_defaults( t_ttc_i2c_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of i2c device
 */
void _driver_i2c_reset( t_ttc_i2c_config* Config );

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_I2C_H
