#ifndef t_ttc_list_types_H
#define t_ttc_list_types_H

/** ttc_list.h ***********************************************{
 *
 * Written by Gregor Rebel 2013
 *
 * Architecture independent support for single linked lists.
 *
 * See ttc_list.h for a complete description!
}*/


//{ includes

#include "ttc_basic_types.h"      // basic datatypes
#include "compile_options.h"
#include "ttc_list_item_types.h"  // structures of individual list items
#include "ttc_assert_types.h"
#include "ttc_mutex_types.h"

//}includes

#ifndef TTC_LIST_STATISTICS
    #define TTC_LIST_STATISTICS TTC_ASSERT_LIST_EXTRA
#endif
#if TTC_LIST_STATISTICS == 1
    #define TTC_LIST_STATS_INCREASE(LIST,FIELD)  (LIST->Stats.FIELD != -1) ? LIST->Stats.FIELD++ : 0
#else
    #define TTC_LIST_STATS_INCREASE(LIST,FIELD)
#endif

// value used as Next-pointer in last list item
#define TTC_LIST_TERMINATOR (t_ttc_list_item*) 0xfefefefe

//{ enums/ structures *******************************************************

typedef enum ttc_list_error_E {      // return type of most functions in ttc_list
    tle_OK,                          // no error
    tle_ListInUseByTask,             // an _isr()-call occured while the list was in use by a task

    tle_Error,                       // errors come below
    // Error Conditions below
    tle_TimeOut,                     // an operation has timed out
    tle_NotImplemented,              // required function not implemented for current architecture
    tle_ListIsEmpty,                 // pulling data from a list has failed because its empty
    tle_UNKNOWN
} e_ttc_list_error;

typedef enum {
    tlo_None,
    tlo_PushBackSingle,
    tlo_PushBackSingleISR,
    tlo_PushBackMultiple,
    tlo_PushBackMultipleISR,
    tlo_PushFrontSingle,
    tlo_PushFrontSingleISR,
    tlo_PushFrontMultiple,
    tlo_PushFrontMultipleISR,
    tlo_PopFrontSingle,
    tlo_PopFrontSingleISR,
    tlo_PopFrontMultiple,
    tlo_PopFrontMultipleISR,
    tlo_RemoveItem,         // removing single item somewhere in list
    tlo_End
} e_ttc_list_operation;

typedef struct s_s_ttc_listtatistics { // statistic data of single list
    // Note: Increase size of these counters if required!
    t_u8  AmountPending;             // total amount of pending push operations so far (counter will overrun!)
    t_u8  AmountPendingCompletion;   // total amount of completed pending push operations so far (counter will overrun!)
    t_u8  AmountPushBack;            // total amount of push back operations so far (counter will overrun!)
    t_u8  AmountPushBackISR;         // total amount of push back operations from interrupt service routines so far (counter will overrun!)
    t_u8  AmountPushBackMultiple;    // total amount of multiple push back operations so far (counter will overrun!)
    t_u8  AmountPushFront;           // total amount of push front operations so far (counter will overrun!)
    t_u8  AmountPushFrontISR;        // total amount of push front operations from interrupt service routines so far (counter will overrun!)
    t_u8  AmountPopFront;            // total amount of pull front operations so far (counter will overrun!)
    t_u8  AmountPopFrontISR;         // total amount of pull front operations from interrupt service routines so far (counter will overrun!)
    t_u8  AmountRemoveItem;          // total amount of remove item operations so far (counter will overrun!)
    //X    e_ttc_list_operation Operation; // current operation on list
    const char* Name;
} t_s_ttc_listtatistics;

// t_ttc_listsize always uses native data width of current architecture
// This avoids mutex locks in s_ttc_listize()
#if TARGET_DATA_WIDTH == 32
    typedef t_u32 t_ttc_listsize;
#elif TARGET_DATA_WIDTH == 16
    typedef t_u16 t_ttc_listsize;
#else
    typedef t_u8 t_ttc_listsize;
#endif

#include "ttc_semaphore_types.h"

/** Management data of single linked list
  */
typedef struct s_ttc_list {
    t_ttc_semaphore_smart AmountItems; // used by tasks waiting for data
    t_ttc_mutex_smart     Lock;        // protects this list from concurrent accesses

    t_ttc_list_item* First; // pointer to first item
    t_ttc_list_item* Last;  // pointer to last item

    // single linked list of items pending pushes from interrupt-service routine to list-end while list was locked
    t_ttc_list_item* Pending_Push_Back_First;
    t_ttc_list_item* Pending_Push_Back_Last;

    // single linked list of items pending pushes from interrupt-service routine to list-head while list was locked
    t_ttc_list_item* Pending_Push_Front_First;
    t_ttc_list_item* Pending_Push_Front_Last;

    //X t_ttc_listsize   Size;                // amount of items currently stored
    base_small_t     Pending_Push_Amount; // amount of items pending to be pushed

#if (TTC_ASSERT_LIST_EXTRA == 1)
    volatile e_ttc_list_operation  LockedBy; // software only lock used to detect faulty ttc_mutex implementation of Lock
#endif
#if (TTC_LIST_STATISTICS== 1)
    t_s_ttc_listtatistics          Stats;    // statistic data of this list (aids debugging and enables extra self tests)
#endif
} __attribute__( ( packed, aligned( 4 ) ) ) t_ttc_list;

//}

#endif
