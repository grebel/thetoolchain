/**
  ******************************************************************************
  * @file      startup_stm32w108.s (based on startup_stm32f10x_md.s)
  * @author    MCD Application Team, Gregor Rebel
  * @version   V3.5.0
  * @date      11-March-2011, 1-Jan-2013
  * @brief     STM32W108 startup code for GNU Compiler Collection
  *            This module performs:
  *                - Set the initial SP
  *                - Set the initial PC == Reset_Handler,
  *                - Set the vector table entries with the exceptions ISR address
  *                - Configure the clock system 
  *                - Branches to main in the C library (which eventually
  *                  calls main()).
  *            After Reset the Cortex-M3 processor is in Thread mode,
  *            priority is Privileged, and the Stack is set to Main.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */
    
  .syntax unified
  .cpu cortex-m3
  .fpu softvfp
  .thumb

.global	g_pfnVectors
.global	Default_Handler

/* start address for the initialization values of the .data section. 
defined in linker script */
.word	_sidata
/* start address for the .data section. defined in linker script */  
.word	_sdata
/* end address for the .data section. defined in linker script */
.word	_edata
/* start address for the .bss section. defined in linker script */
.word	_sbss
/* end address for the .bss section. defined in linker script */
.word	_ebss

.equ  BootRAM, 0xF108F85F
/**
 * @brief  This is the code that gets called when the processor first
 *          starts execution following a reset event. Only the absolutely
 *          necessary set is performed, after which the application
 *          supplied main() routine is called. 
 * @param  None
 * @retval : None
*/

  .section	.text.Reset_Handler
  .weak         Reset_Handler
  .type         Reset_Handler, %function

Reset_Handler:	


/* Copy the data segment initializers from flash to SRAM */  
  movs	r1, #0
  b	LoopCopyDataInit

CopyDataInit:
	ldr	r3, =_sidata
	ldr	r3, [r3, r1]
	str	r3, [r0, r1]
	adds	r1, r1, #4
    
LoopCopyDataInit:
	ldr	r0, =_sdata
	ldr	r3, =_edata
	adds	r2, r0, r1
	cmp	r2, r3
	bcc	CopyDataInit
	ldr	r2, =_sbss
	b	LoopFillZerobss
/* Zero fill the bss segment. */  
FillZerobss:
	movs	r3, #0
	str	r3, [r2], #4
    
LoopFillZerobss:
	ldr	r3, = _ebss
	cmp	r2, r3
	bcc	FillZerobss
/* Call the clock system intitialization function.*/
        bl      SystemInit
/* Call the applications entry point.*/
	bl	main
	bx	lr    
.size	Reset_Handler, .-Reset_Handler

/**
 * @brief  This is the code that gets called when the processor receives an 
 *         unexpected interrupt. This simply enters an infinite loop, preserving
 *         the system state for examination by a debugger.
 * @param  None     
 * @retval None       
*/
        .section	.text.Default_Handler,"ax",%progbits
Default_Handler:
Infinite_Loop:
	b	Infinite_Loop
	.size	Default_Handler, .-Default_Handler
/******************************************************************************
*
* The minimal vector table for a Cortex M3.  Note that the proper constructs
* must be placed on this to ensure that it ends up at physical address
* 0x0000.0000.
*
******************************************************************************/    
 	.section	.isr_vector,"a",%progbits
	.type	g_pfnVectors, %object
	.size	g_pfnVectors, .-g_pfnVectors
    
    
g_pfnVectors:
	.word	_estack
        .word	Reset_Handler
        .word	NMI_Handler
	.word	HardFault_Handler
	.word	MemManage_Handler
	.word	BusFault_Handler
	.word	UsageFault_Handler
	.word	0
	.word	0
	.word	0
	.word	0
	.word	SVC_Handler
	.word	DebugMon_Handler
	.word	0
	.word	PendSV_Handler
	.word	SysTick_Handler
	
        @; External Interrupts
        .word TIM1_IRQHandler                   @; 16+ 0   Timer 1 Interrupt
        .word TIM2_IRQHandler                   @; 16+ 1   Timer 2 Interrupt
        .word MNG_IRQHandler                    @; 16+ 2   Management Peripheral Interrupt
        .word BASEBAND_IRQHandler               @; 16+ 3   Base Band Interrupt
        .word SLPTIM_IRQHandler                 @; 16+ 4   Sleep Timer Interrupt
        .word SC1_IRQHandler                    @; 16+ 5   Serial Controller 1 Interrupt
        .word SC2_IRQHandler                    @; 16+ 6   Serial Controller 2 Interrupt
        .word SECURITY_IRQHandler               @; 16+ 7   Security Interrupt
        .word MAC_TIM_IRQHandler                @; 16+ 8   MAC Timer Interrupt
        .word MAC_TR_IRQHandler                 @; 16+ 9   MAC Transmit Interrupt
        .word MAC_RE_IRQHandler                 @; 16+10   MAC Receive Interrupt
        .word ADC_IRQHandler                    @; 16+11   ADC Interrupt
        .word EXTIA_IRQHandler                  @; 16+12   EXTIA Interrupt
        .word EXTIB_IRQHandler                  @; 16+13   EXTIB Interrupt
        .word EXTIC_IRQHandler                  @; 16+14   EXTIC Interrupt
        .word EXTID_IRQHandler                  @; 16+15   EXTID Interrupt
        .word DEBUG_IRQHandler                  @; 16+16   Debug Interrupt
   
g_pfnVectors_End:


/*******************************************************************************
*
* Provide weak aliases for each Exception handler to the Default_Handler. 
* As they are weak aliases, any function with the same name will override 
* this definition.
*
*******************************************************************************/
    
  .weak	NMI_Handler
	.thumb_set NMI_Handler,Default_Handler
	
  .weak	HardFault_Handler
	.thumb_set HardFault_Handler,Default_Handler
	
  .weak	MemManage_Handler
	.thumb_set MemManage_Handler,Default_Handler
	
  .weak	BusFault_Handler
	.thumb_set BusFault_Handler,Default_Handler

	.weak	UsageFault_Handler
	.thumb_set UsageFault_Handler,Default_Handler

	.weak	SVC_Handler
	.thumb_set SVC_Handler,Default_Handler

	.weak	DebugMon_Handler
	.thumb_set DebugMon_Handler,Default_Handler

	.weak	PendSV_Handler
	.thumb_set PendSV_Handler,Default_Handler

	.weak	SysTick_Handler
	.thumb_set SysTick_Handler,Default_Handler

	.weak TIM1_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak TIM2_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak MNG_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak BASEBAND_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak SLPTIM_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak SC1_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak SC2_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak SECURITY_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak MAC_TIM_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak MAC_TR_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak MAC_RE_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak ADC_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak EXTIA_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak EXTIB_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak EXTIC_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak EXTID_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler
  
	.weak DEBUG_IRQHandler
	.thumb_set SysTick_Handler,Default_Handler

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
