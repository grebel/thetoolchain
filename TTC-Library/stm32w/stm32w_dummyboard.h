/** stm32w_dummyboard.h
  *
  * The ToolChain - written by Gregor Rebel 2013
  *
  * Compatibility layer for STM32W Firmware-Library >=v2.0.0 provided by ST Microelectronics Corp.
  *
  */
  
#ifndef _STM32W_DUMMYBOARD_H_
#define _STM32W_DUMMYBOARD_H_


/* Includes ------------------------------------------------------------------*/
#ifdef abs
#  undef abs // avoid compile error in stdlib.h caused by abs() already defined in ttc_basic.h
#endif
#include "../ttc_basic.h"
//#include "stm32w108xx_gpio.h" // missing include added for The ToolChain'
#include "hal.h"
#include "gnu.h"
#ifdef EXTENSION_253_stm32w1xx_hal110
#include "board.h"
#endif
//X #include "error.h"
//X #include "mfg-token.h"
//X #include <string.h>

#ifndef Button_TypeDef
#define Button_TypeDef t_u8
#endif

/* Private typedef -----------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private function ----------------------------------------------------------*/

/** @defgroup  board_Private_Functions
  * @{
  */


/**
  * @brief  Perform board specific actions to power down the system, usually before going to deep sleep.
  * @param  None
  * @retval None
  */
void halBoardPowerDown(void);

/**
  * @brief  Perform board specific actions to power up the system.
  * @param  None
  * @retval None
  */
void halBoardPowerUp(void);

/**
  * @brief  Wake up GPIO button configuration
  * @param  None
  * @retval Wake button 
  */
t_u32 halButtonSnWakeSource(Button_TypeDef Button);

#endif /* _STM32W_DUMMYBOARD_H_ */




