/** stm32w_dummyboard.c
  *
  * The ToolChain - written by Gregor Rebel 2013-2014
  *
  * Compatibility layer for STM32W Firmware-Library 1.1.0 - 2.0.1 provided by ST Microelectronics Corp.
  *
  * For SimpleMAC 1.1.0 compare with file
  * additionals/253_stm32w1xx_hal110/micro/cortexm3/board.c
  */

#include "stm32w_dummyboard.h"

void halBoardPowerDown(void) {
}
void halBoardPowerUp(void) {
}
t_u32 halButtonSnWakeSource(Button_TypeDef Button) {
      return 0;
}
void halBoardInit(void) {

}
void stSerialPrintf(t_u8 PortIndex, const char* Format, ...) {
    (void) PortIndex;
    (void) Format;

    // This function is required to make linker happy for SimpleMAC v1.1.0.
    // ToDo: implement to allow SimpleMAC debugging
}
