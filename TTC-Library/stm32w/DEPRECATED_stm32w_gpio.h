#ifndef STM32W_GPIO_H
#define STM32W_GPIO_H

/** { DEPRECATED_stm32w_gpio.h **********************************************
 
                           The ToolChain
                      
   High-Level interface for GPIO device.

   Structures, Enums and Defines being required by high-level gpio and application.

   Note: See ttc_gpio.h for description of stm32w independent GPIO implementation.
  
   Authors: Greg Knoll 2013

 
}*/





//{ Defines/ TypeDefs ****************************************************
// amount of gpio banks (A,B,C)
#define TTC_GPIO_MAX_AMOUNT 3

// amount of port pins per bank
#define TTC_GPIO_MAX_PINS 8

#define ttc_gpio_bank_t  stm32w_GPIO_e
#define ttc_Port_t     stm32w_Port_t  //bank+pin

#define TTC_GPIO_BANK_A stm32w_GPIOA
#define TTC_GPIO_BANK_B stm32w_GPIOB
#define TTC_GPIO_BANK_C stm32w_GPIOC

//{ Define shortcuts for all pins of GPIO Bank A
#define PIN_PA0  TTC_GPIO_BANK_A,0
#define PIN_PA1  TTC_GPIO_BANK_A,1
#define PIN_PA2  TTC_GPIO_BANK_A,2
#define PIN_PA3  TTC_GPIO_BANK_A,3
#define PIN_PA4  TTC_GPIO_BANK_A,4
#define PIN_PA5  TTC_GPIO_BANK_A,5
#define PIN_PA6  TTC_GPIO_BANK_A,6
#define PIN_PA7  TTC_GPIO_BANK_A,7
//}BankA
//{ Define shortcuts for all pins of GPIO Bank B
#define PIN_PB0  TTC_GPIO_BANK_B,0
#define PIN_PB1  TTC_GPIO_BANK_B,1
#define PIN_PB2  TTC_GPIO_BANK_B,2
#define PIN_PB3  TTC_GPIO_BANK_B,3
#define PIN_PB4  TTC_GPIO_BANK_B,4
#define PIN_PB5  TTC_GPIO_BANK_B,5
#define PIN_PB6  TTC_GPIO_BANK_B,6
#define PIN_PB7  TTC_GPIO_BANK_B,7
//}BankB
//{ Define shortcuts for all pins of GPIO Bank C
#define PIN_PC0  TTC_GPIO_BANK_C,0
#define PIN_PC1  TTC_GPIO_BANK_C,1
#define PIN_PC2  TTC_GPIO_BANK_C,2
#define PIN_PC3  TTC_GPIO_BANK_C,3
#define PIN_PC4  TTC_GPIO_BANK_C,4
#define PIN_PC5  TTC_GPIO_BANK_C,5
#define PIN_PC6  TTC_GPIO_BANK_C,6
#define PIN_PC7  TTC_GPIO_BANK_C,7
//}BankC

//} Defines




//{ Includes *************************************************************

#include "stm32w_gpio_types.h"
#include "../ttc_gpio_types.h"
//#include "stm32w108xx.h"      //use for GPIO_Typedef
//#include "../ttc_task.h"
//#include "../ttc_memory.h"

//} Includes






//{ Macro definitions ****************************************************

#define _driver_ttc_gpio_init(GPIOx, Pin, Type, Speed)                stm32w_gpio_init(GPIOx, Pin, Type, Speed)
#define _driver_ttc_gpio_set(...)                                     stm32w_gpio_set(__VA_ARGS__)
#define _driver_ttc_gpio_get(...)                                     stm32w_gpio_get(__VA_ARGS__)
#define _driver_ttc_gpio_clr(...)                                     DEPRECATED_stm32w_gpio.clr(__VA_ARGS__)

#define _driver_ttc_gpio_init_variable(Port, Bank, Pin, Type, Speed)  stm32w_gpio_init2_variable(Port, Bank, Pin, Type, Speed)
#define _driver_ttc_gpio_variable(PORT,BANK,PIN)                      stm32w_gpio_variable(PORT,BANK,PIN)
#define _driver_ttc_gpio_get_variable(PORT)                           stm32w_gpio_get_variable(PORT)
#define _driver_ttc_gpio_set_variable(PORT)                           stm32w_gpio_set_variable(PORT)
#define _driver_ttc_gpio_clr_variable(PORT)                           DEPRECATED_stm32w_gpio.clr_variable(PORT)

#define _driver_ttc_gpio_from_index8(INDEX,BANK,PIN)                  stm32w_gpio_from_index8(INDEX,BANK,PIN)
#define _driver_ttc_gpio_create_index8(BANK,PIN)                      DEPRECATED_stm32w_gpio.create_index8(BANK,PIN);

//} Macro definitions





//{ Function prototypes **************************************************

//------------regular init/get/set/clear-----------//
/** initializes single port bit for use as input/ output
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C
 * @param Pin    0..7
 * @param Type   one of GPIO_Mode_AIN, GPIO_Mode_IN_FLOATING, GPIO_Mode_IPD, GPIO_Mode_IPU, GPIO_Mode_Out_OD, GPIO_Mode_Out_PP, GPIO_Mode_Out_OD, GPIO_Mode_AF_PP, GPIO_Mode_AF_OD
 * @param Speed  one of GPIO_Speed_2MHz, GPIO_Speed_10MHz, tgs_Max
 */
void stm32w_gpio_init(stm32w_GPIO_e GPIOx, u8_t Pin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed);

/** returns 4-bit configuration setting suitable for MODE of GPIO_PxCFGL/H (see gpio_port_ioMode_e in stm32w:registers.h)
 *
 * Note: This function is private and normally should not be called from outside
 *
 * @param Type           mode to use for all 8 pins  (-> ttc_gpio_types.h:ttc_gpio_mode_e)
 * @param Speed          speed to use for all 8 pins (-> ttc_gpio_types.h:ttc_gpio_speed_e)
 *                       used for compatibility with ttc, but not used in STM32W
 * @return               4 bit value to be written at position corresponding to desired pin into GPIOx->CFGL/ GPIOx->CFGH
 */
u8_t _DEPRECATED_stm32w_gpio.compile_port_config(ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed);

/** read current value of input pin
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, TTC_GPIO_BANK_D, TTC_GPIO_BANK_E, TTC_GPIO_BANK_F
 * @param Pin    0..7
 */
bool stm32w_gpio_get(stm32w_GPIO_e GPIOx, u8_t Pin);

/** set output pin to logical one
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, TTC_GPIO_BANK_D, TTC_GPIO_BANK_E, TTC_GPIO_BANK_F
 * @param Pin    0..7
 */
void DEPRECATED_stm32w_gpio.clr(stm32w_GPIO_e GPIOx, u8_t Pin);

/** set output pin to logical zero
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, TTC_GPIO_BANK_D, TTC_GPIO_BANK_E, TTC_GPIO_BANK_F
 * @param Pin    0..7
 */
void stm32w_gpio_set(stm32w_GPIO_e GPIOx, u8_t Pin);
//-------------------------------------------------//



//-----------with Variables/Ports-----------------//
/** allows to load struct from list type definition
 *
 * @param Port   points to structure to be filled with given data
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, TTC_GPIO_BANK_D, TTC_GPIO_BANK_E, TTC_GPIO_BANK_F
 * @param Pin    0..7
 *
 * Example:
 * #define PB_LED1=TTC_GPIO_BANK_C,6
 * stm32w_Port_t Port;
 * stm32w_gpio_variable(&Port, PB_LED1);
 * stm32w_gpio_init_variable(&Port, GPIO_Mode_Out_PP);
 * stm32w_gpio_set_variable(&Port);
 */
void stm32w_gpio_variable(stm32w_Port_t* Port, stm32w_GPIO_e GPIOx, u8_t Pin);

/** initializes single port bit for use as input/ output with given speed by use of a state variable
 *
 * @param Port   points to structure being created by stm32w_gpio_variable()
 * @param Type   mode to use (-> ttc_gpio_types.h:ttc_gpio_mode_e)
 * @param Speed  [NOT IN STM32W] speed to use (-> ttc_gpio_types.h:ttc_gpio_speed_e)
 */
void stm32w_gpio_init_variable(ttc_Port_t* Port, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed);

/** initializes single port bit for use as input/ output with maximum speed by use of a state variable
 *
 * @param Port   points to structure to be filled with given data
 * @param Bank   GPIO bank to use (-> ttc_gpio_types.h:ttc_gpio_bank_t)
 *               Note: Bank, Pin can be given as one argument by use of Pin_Pxn macro (GPIOA, 7) = Pin_PA7
 * @param Pin    0..7
 * @param Type   mode to use (-> ttc_gpio_types.h:ttc_gpio_mode_e)
 * @param Speed  pin speed to configure (-> ttc_gpio_types.h:ttc_gpio_speed_e)
 */
void stm32w_gpio_init2_variable(ttc_Port_t* Port, ttc_gpio_bank_t GPIOx, u8_t Pin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed);

/** read current value of input pin
 * @param Port  filled out struct as being returned by stm32w_gpio_variable()
 */
bool stm32w_gpio_get_variable(stm32w_Port_t* Port);

/** set output pin to logical one
 * @param Port  filled out struct as being returned by stm32w_gpio_variable()
 */
void stm32w_gpio_set_variable(stm32w_Port_t* Port);

/** set output pin to logical zero
 * @param Port  filled out struct as being returned by stm32w_gpio_variable()
 */
void DEPRECATED_stm32w_gpio.clr_variable(stm32w_Port_t* Port);

/** restores GPIO-bank and pin number from 8-bit value
 *
 * @param PhysicalIndex  8-bit value representing GPIO-bank + pin
 * @param GPIOx          loaded with GPIO bank (-> ttc_gpio_types.h:ttc_gpio_bank_t)
 * @param Pin            loaded with pin number
 */
void stm32w_gpio_from_index8(u8_t PhysicalIndex, GPIO_TypeDef** GPIOx, u8_t* Pin);

/** creates numeric 8-bit representing given GPIO-bank and pin number
 *
 * @param Bank  GPIO bank to use (-> ttc_gpio_types.h:ttc_gpio_bank_t)
 *               Note: Bank, Pin can be given as one argument by use of Pin_Pxn macro (GPIOA, 7) = Pin_PA7
 * @param Pin    0..7
 * @return       8-bit value representing given GPIO-pin in a memory friendly format
 */
u8_t DEPRECATED_stm32w_gpio.create_index8(ttc_gpio_bank_t Bank, u8_t Pin);

//------------------------------------------------//






//} Function prototypes

#endif //STM32W_GPIO_H
