#ifndef register_stm32w1xx_H
#define register_stm32w1xx_H

#error This file is DEPRECATED, Use ttc_register.h instead!
/*{ register_stm32w1xx_registers.h ******************************************************

  Structure definitions of stm32w-registers.

  Written by Greg Knoll 2013

  The below explanation written by Gregor Rebel 2012
     These structures are extremly handy when it comes to debugging via gdb.
     You can view any memory location through the eye of a struct:
     p *( (register_stm32w1xx_register_usart_t*) 0x40013800 )

     which is the same as
     p *register_stm32_USART1


     Note: Use of these structures in your code can be problematic
           if register access has side-effects.

     Example:
      + Works
        // Note: USART_TypeDef defined by StdPeripheral-library
        USART_CR1_t CR1;
        *( (u16_t*) &CR1 ) = USART1->CR1; // 16 bit transfer into local variable
        CR1.PCE  = 1;
        CR1.TCIE = 0;
        USART1->CR1 = *( (u16_t*) &CR1 ); // 16 bit transfer into register

      + does not work reliable
        ((register_stm32w1xx_register_usart_t*) MyUSART)->CR1.PCE = 1;

      Despite of this compiler dependend problem, the structures defined here
      allow an easy, self checking, way to read and write registers in a safe manner.

      You will not have to do any error prone bit-shifting like this:
      USART1->CR1 |= 1 << 10;             // PCE  = 1
      USART1->CR1 &= (0xffff - (1 << 6)); // TCIE = 0

}*/

/*{ Defines/ TypeDefs *******************************************************


//} Defines*/

#warning Use of DEPRECATED_stm32w_registers.h is deprecated - Use ttc_register instead!

//{ Includes ****************************************************************

 // Basic set of helper functions
//? #include "../ttc_basic.h"
#include "../cm3/cm3_registers.h"

//#define USE_STDPERIPH_DRIVER
//#include "stm32w108xx.h"

//} Includes

//{ STM32 Registers *********************************************************

//---Configuration mode types for GPIO---//

typedef enum { //gpio_port_ioMode_e - possible 4-bit values for GPIO pin mode set in GPIO_PxCFGH/L
    gpmode_analog = 0x0,
    gpmode_input_floating = 0x4,
    gpmode_input_pull_upORdown = 0x8,
    gpmode_output_push_pull = 0x1,               
    gpmode_output_open_drain = 0x5,              
    gpmode_output_alternate_push_pull = 0x9,
    gpmode_output_alternate_open_drain =0xD,
    gpmode_output_alternate_pp_SPI_SCLK = 0xB
} register_stm32w1xx_gpio_port_ioMode_e;

/* DEPRECATED
//-----------Pin hex representations----------//
typedef enum {
    PA0,             //0x00
    PA1,
    PA2,
    PA3,
    PA4,
    PA5,
    PA6,
    PA7,
    PB0,
    PB1,
    PB2,
    PB3,
    PB4,
    PB5,
    PB6,
    PB7,
    PC0,
    PC1,
    PC2,
    PC3,
    PC4,
    PC5,
    PC6,
    PC7             //0x17
} register_stm32w1xx_gpio_IRQx_pin_e;
*/

//----------IRQ modes----------//
typedef enum {
    irq_mode_disabled,                //0x0
    irq_mode_rising_edge,             //0x1
    irq_mode_falling_edge,            //0x2
    irq_mode_rising_and_falling_edge, //0x3
    irq_mode_active_high,             //0x4
    irq_mode_active_low               //0x5
} register_stm32w1xx_gpio_IRQx_mode_e;

//-----Serial Controller Modes-----//
typedef enum {
    sc_mode_disabled,   //0
    sc_mode_UART,       //1
    sc_mode_SPI,        //2
    sc_mode_I2C         //3

} register_stm32w1xx_sc_mode_e;


//---------------------------------GPIO Registers----------------------------------------------//
//{

//{The basic GPIO registers for each port are kept in blocks at
//Port A -> 0xB000
//Port B -> 0xB400
//Port C -> 0xB800
//The datasheet has set and clear switched
typedef struct { //GPIO_PxCFGL - config of each pin's mode [starting with LSB] 	(p.67)
    unsigned bMODE0   	:4; //gpio_port_ioMode_e for GPIOx pin 0
    unsigned bMODE1   	:4; //gpio_port_ioMode_e for GPIOx pin 1
    unsigned bMODE2   	:4; //gpio_port_ioMode_e for GPIOx pin 2
    unsigned bMODE3   	:4; //gpio_port_ioMode_e for GPIOx pin 3
    unsigned breserved	:16; //- - - - - - - - - - - - - - - - -
} __attribute__((__packed__)) register_stm32w1xx_GPIO_PxCFGL_t;

typedef struct { //GPIO_PxCFGH 							(p.67)
    unsigned bMODE4   	:4; //gpio_port_ioMode_e for GPIOx pin 4
    unsigned bMODE5   	:4; //gpio_port_ioMode_e for GPIOx pin 5
    unsigned bMODE6   	:4; //gpio_port_ioMode_e for GPIOx pin 6
    unsigned bMODE7   	:4; //gpio_port_ioMode_e for GPIOx pin 7
    unsigned breserved	:16; //- - - - - - - - - - - - - - - - -
} __attribute__((__packed__)) register_stm32w1xx_GPIO_PxCFGH_t;

typedef struct { //GPIO_PxIN - input data register           			(p.68)
    unsigned bDATA	:8; //input data register of all 8 port bits
    unsigned breserved	:24; // ------------------------------------
	
} __attribute__((__packed__)) register_stm32w1xx_GPIO_PxIN_t;

typedef struct { //GPIO_PxOUT - output data register				(p.69)
    unsigned bDATA	:8; //output data register of all 8 port bits
    unsigned breserved	:24; // ------------------------------------
	
} __attribute__((__packed__)) register_stm32w1xx_GPIO_PxOUT_t;

typedef struct { //GPIO_PxSET - output set register				(p.70)
    unsigned bSET0	:1; //1 to set, 0 no effect
    unsigned bSET1	:1; //1 to set, 0 no effect
    unsigned bSET2	:1; //1 to set, 0 no effect
    unsigned bSET3	:1; //1 to set, 0 no effect
    unsigned bSET4	:1; //1 to set, 0 no effect
    unsigned bSET5	:1; //1 to set, 0 no effect
    unsigned bSET6	:1; //1 to set, 0 no effect
    unsigned bSET7	:1; //1 to set, 0 no effect
    unsigned breserved 	:24; //----------------------
} __attribute__((__packed__)) register_stm32w1xx_GPIO_PxSET_t;

typedef struct { //GPIO_PxCLR - output clear register				(p.69)
    unsigned bCLR0	:1; //1 to clear, 0 no effect
    unsigned bCLR1	:1; //1 to clear, 0 no effect
    unsigned bCLR2	:1; //1 to clear, 0 no effect
    unsigned bCLR3	:1; //1 to clear, 0 no effect
    unsigned bCLR4	:1; //1 to clear, 0 no effect
    unsigned bCLR5	:1; //1 to clear, 0 no effect
    unsigned bCLR6	:1; //1 to clear, 0 no effect
    unsigned bCLR7	:1; //1 to clear, 0 no effect
    unsigned breserved 	:24; //----------------------
} __attribute__((__packed__)) register_stm32w1xx_GPIO_PxCLR_t;

typedef struct {//GPIOx - General Purpose I/O basic registers
    register_stm32w1xx_GPIO_PxCFGL_t    CFGL;    //Port configuration register low
    register_stm32w1xx_GPIO_PxCFGH_t    CFGH;    //Port configuration register high
    register_stm32w1xx_GPIO_PxIN_t      IDR;     //GPIO input data register
    register_stm32w1xx_GPIO_PxOUT_t     ODR;     //GPIO output data register
    register_stm32w1xx_GPIO_PxSET_t     SETR;    //GPIO set register
    register_stm32w1xx_GPIO_PxCLR_t     CLR;     //GPIO clear register
} __attribute__((__packed__)) register_stm32w1xx_GPIOx_t;
//} END GPIO basic registers


//{ GPIO registers for DEBUG, WAKE, and IRQ (for external interrupts)

//-----first block for debug, wake, and some of irq-----//
typedef struct { //GPIO_DBGCFG - GPIO debug config register                        (p.73)
    unsigned breserved1       :4; //-----------------------
    unsigned bEXTREGEN         :1; //Disable REG_EN override of PA7's normal GPIO config
                                 //0: Enable override. 1: Disable override.
    unsigned bDEBUGDIS         :1; //Disable debug interface override of normal GPIO config
                                 //0:Permit debug interface to be active. 1: Disable debug interface (if it's not already active)
    unsigned breserved        :26; //----------------------
} __attribute__((__packed__)) register_stm32w1xx_GPIO_DBGCFG_t;

typedef struct { //GPIO_DBGSTAT - GPIO debug status register                       (p.74)
    unsigned bSWEN       :1; //Status of Serial Wire interface
                            //0: Not enabled by SWJ-DP. 1: Enabled by SWJ-DP.
    unsigned bFORCEDBG   :1; //Status of debugger interface
                            //0: Debugger interface not forced active. 1: Debugger interface forced active by debugger cable.
    unsigned breserved   :1; //------------------------------
    unsigned bBOOTMODE   :1; //The state of the nBOOTMODE signal sampled at the end of reset
                                 //0: nBOOTMODE was not asserted (HIGH). 1: asserted (LOW)
    unsigned breserved2        :28;//----------------------
} __attribute__((__packed__)) register_stm32w1xx_GPIO_DBGSTAT_t;

typedef struct { //GPIO_PxWAKE - enables wakeup monitoring for the pins of port x  (p.70)
    unsigned bWAKE0	:1;     //1 to enable wakeup monitoring of PA0
    unsigned bWAKE1	:1;     //1 to enable wakeup monitoring of PA1
    unsigned bWAKE2	:1;     //1 to enable wakeup monitoring of PA2
    unsigned bWAKE3	:1;     //1 to enable wakeup monitoring of PA3
    unsigned bWAKE4	:1;     //1 to enable wakeup monitoring of PA4
    unsigned bWAKE5	:1;     //1 to enable wakeup monitoring of PA5
    unsigned bWAKE6	:1;     //1 to enable wakeup monitoring of PA6
    unsigned bWAKE7	:1;     //1 to enable wakeup monitoring of PA7
    unsigned breserved :24;  //------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_GPIO_PxWAKE_t;

typedef struct { //GPIO_IRQCSEL - interrupt C select register                    (p.71)
    unsigned bSEL         :5; //pin assigned to IRQC (gpio_IRQx_pin_e)
    unsigned breserved    :27;//--------------------
} __attribute__((__packed__)) register_stm32w1xx_GPIO_IRQCSEL_t;

typedef struct { //GPIO_IRQDSEL - interrupt D select register                    (p.71)
    unsigned bSEL         :5; //pin assigned to IRQD (gpio_IRQx_pin_e)
    unsigned breserved    :27;//--------------------
} __attribute__((__packed__)) register_stm32w1xx_GPIO_IRQDSEL_t;

typedef struct { //GPIO_WAKEFILT - wakeup filtering register                    (p.71)
    unsigned bWFILTER_GPIO  :1; //enable filter on wakeup sources designated in GPIO_PnWake
    unsigned bWFILTER_SC1   :1; //enable filter on wakeup source SC1(PB2)
    unsigned bWFILTER_SC2   :1; //enable filter on wakeup source SC2(PA2)
    unsigned bWFILTER_IRQD  :1; //enable filter on wakeup source IRQD
    unsigned breserved      :28; //-------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_GPIO_WAKEFILT_t;

typedef struct {//registers_GPIO_debug_wake_irq
    register_stm32w1xx_GPIO_DBGCFG_t    DBGCFG;   //GPIO Debug configuration register
    register_stm32w1xx_GPIO_DBGSTAT_t   DBGSTAT;  //GPIO Debug status register
    register_stm32w1xx_GPIO_PxWAKE_t    AWAKE;    //Wake monitoring enable register
    register_stm32w1xx_GPIO_PxWAKE_t    BWAKE;    //Wake monitoring enable register
    register_stm32w1xx_GPIO_PxWAKE_t    CWAKE;    //Wake monitoring enable register
    register_stm32w1xx_GPIO_IRQCSEL_t   IRQCSEL;  //IRQ pin selection register
    register_stm32w1xx_GPIO_IRQDSEL_t   IRQDSEL;  //IRQ pin selection register
    register_stm32w1xx_GPIO_WAKEFILT_t  WAKEFILT; //Enable filter on wakeup register
}__attribute__((__packed__)) register_stm32w1xx_GPIO_debug_wake_irq_t;
//-------------------END first block------------------//

//-----second block for config registers for interrupts A-D----//
typedef struct { //GPIO_INTCFGx - interrupt x config register                    (p.72)
    unsigned breserved1    :5; //-------------------------------------------
    unsigned bINTMOD  :3; //IRQx triggering mode (gpio_IRQx_mode_e)
    unsigned bINTFILT :1; //set bit to enable digital filtering on IRQx
    unsigned breserved2   :23; //-------------------------------------------
 } __attribute__((__packed__)) register_stm32w1xx_GPIO_INTCFGx_t;

typedef struct { //GPIO_interruptCFG - interrupt x config register                    (p.72)
    register_stm32w1xx_GPIO_INTCFGx_t INTCFGA;
    register_stm32w1xx_GPIO_INTCFGx_t INTCFGB;
    register_stm32w1xx_GPIO_INTCFGx_t INTCFGC;
    register_stm32w1xx_GPIO_INTCFGx_t INTCFGD;
 } __attribute__((__packed__)) register_stm32w1xx_GPIO_interruptCFG_t;
//------------------END second block----------------------//

//-------third block for GPIO interrupt flags----------------//
typedef struct { //GPIO_interruptFLAG - GPIO interrupt flag register                   (p.73)
    unsigned bIRQAFLAG    :1; //IRQA interrupt pending
    unsigned bIRQBFLAG    :1; //IRQB interrupt pending
    unsigned bIRQCFLAG    :1; //IRQC interrupt pending
    unsigned bIRQDFLAG    :1; //IRQD interrupt pending
    unsigned breserved        :28;//----------------------
} __attribute__((__packed__)) register_stm32w1xx_GPIO_interruptFLAG_t;
//--------------------END third block-----------------------//

//} END GPIO registers for DEBUG, WAKE, and IRQ

//}
//-------------------------------------END GPIO REGISTERS-----------------------------------------//


//------------------------------------INTERRUPT REGISTERS-----------------------------------------//
//{

//-----------NVIC----------------//

typedef struct { //INT_CFGSET - top-level set interrupts config register  [ENABLE ints]       (p.184)
    unsigned bTIM1       :1;     //1: enable timer 1      interrupt (0: no effect)
    unsigned bTIM2       :1;     //1: enable timer 2      interrupt (0: no effect)
    unsigned bMGMT       :1;     //1: enable management   interrupt (0: no effect)
    unsigned bBB         :1;     //1: enable baseband     interrupt (0: no effect)
    unsigned bSLEEPTMR   :1;     //1: enable sleep timer  interrupt (0: no effect)
    unsigned bSC1        :1;     //1: enable serial 1     interrupt (0: no effect)
    unsigned bSC2        :1;     //1: enable serial 2     interrupt (0: no effect)
    unsigned bSEC        :1;     //1: enable security     interrupt (0: no effect)
    unsigned bMACTMR     :1;     //1: enable MAC timer    interrupt (0: no effect)
    unsigned bMACTX      :1;     //1: enable MAC transmit interrupt (0: no effect)
    unsigned bMACRX      :1;     //1: enable MAC receive  interrupt (0: no effect)
    unsigned bADC        :1;     //1: enable ADC          interrupt (0: no effect)
    unsigned bIRQA       :1;     //1: enable IRQA         interrupt (0: no effect)
    unsigned bIRQB       :1;     //1: enable IRQB         interrupt (0: no effect)
    unsigned bIRQC       :1;     //1: enable IRQC         interrupt (0: no effect)
    unsigned bIRQD       :1;     //1: enable IRQD         interrupt (0: no effect)
    unsigned bDEBUG      :1;     //1: enable debug        interrupt (0: no effect)
    unsigned breserved       :15;    //-----------------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_INT_CFGSET_t;

typedef struct { //INT_CFGCLR - top-level clear interrupts config register [DISABLE ints]     (p.185)
    unsigned bTIM1       :1;     //1: disable timer 1      interrupt (0: no effect)
    unsigned bTIM2       :1;     //1: disable timer 2      interrupt (0: no effect)
    unsigned bMGMT       :1;     //1: disable management   interrupt (0: no effect)
    unsigned bBB         :1;     //1: disable baseband     interrupt (0: no effect)
    unsigned bSLEEPTMR   :1;     //1: disable sleep timer  interrupt (0: no effect)
    unsigned bSC1        :1;     //1: disable serial 1     interrupt (0: no effect)
    unsigned bSC2        :1;     //1: disable serial 2     interrupt (0: no effect)
    unsigned bSEC        :1;     //1: disable security     interrupt (0: no effect)
    unsigned bMACTMR     :1;     //1: disable MAC timer    interrupt (0: no effect)
    unsigned bMACTX      :1;     //1: disable MAC transmit interrupt (0: no effect)
    unsigned bMACRX      :1;     //1: disable MAC receive  interrupt (0: no effect)
    unsigned bADC        :1;     //1: disable ADC          interrupt (0: no effect)
    unsigned bIRQA       :1;     //1: disable IRQA         interrupt (0: no effect)
    unsigned bIRQB       :1;     //1: disable IRQB         interrupt (0: no effect)
    unsigned bIRQC       :1;     //1: disable IRQC         interrupt (0: no effect)
    unsigned bIRQD       :1;     //1: disable IRQD         interrupt (0: no effect)
    unsigned bDEBUG      :1;     //1: disable debug        interrupt (0: no effect)
    unsigned breserved       :15;    //-----------------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_INT_CFGCLR_t;

typedef struct { //INT_PENDSET - top-level set interrupts pending register [TRIGGER ints]     (p.186)
    unsigned bTIM1       :1;     //1: pend timer 1      interrupt (0: no effect)
    unsigned bTIM2       :1;     //1: pend timer 2      interrupt (0: no effect)
    unsigned bMGMT       :1;     //1: pend management   interrupt (0: no effect)
    unsigned bBB         :1;     //1: pend baseband     interrupt (0: no effect)
    unsigned bSLEEPTMR   :1;     //1: pend sleep timer  interrupt (0: no effect)
    unsigned bSC1        :1;     //1: pend serial 1     interrupt (0: no effect)
    unsigned bSC2        :1;     //1: pend serial 2     interrupt (0: no effect)
    unsigned bSEC        :1;     //1: pend security     interrupt (0: no effect)
    unsigned bMACTMR     :1;     //1: pend MAC timer    interrupt (0: no effect)
    unsigned bMACTX      :1;     //1: pend MAC transmit interrupt (0: no effect)
    unsigned bMACRX      :1;     //1: pend MAC receive  interrupt (0: no effect)
    unsigned bADC        :1;     //1: pend ADC          interrupt (0: no effect)
    unsigned bIRQA       :1;     //1: pend IRQA         interrupt (0: no effect)
    unsigned bIRQB       :1;     //1: pend IRQB         interrupt (0: no effect)
    unsigned bIRQC       :1;     //1: pend IRQC         interrupt (0: no effect)
    unsigned bIRQD       :1;     //1: pend IRQD         interrupt (0: no effect)
    unsigned bDEBUG      :1;     //1: pend debug        interrupt (0: no effect)
    unsigned breserved       :15;    //-----------------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_INT_PENDSET_t;

typedef struct { //INT_PENDCLR - top-level clear interrupts pending register [CLEAR ints]     (p.187)
    unsigned bTIM1       :1;     //1: unpend timer 1      interrupt (0: no effect)
    unsigned bTIM2       :1;     //1: unpend timer 2      interrupt (0: no effect)
    unsigned bMGMT       :1;     //1: unpend management   interrupt (0: no effect)
    unsigned bBB         :1;     //1: unpend baseband     interrupt (0: no effect)
    unsigned bSLEEPTMR   :1;     //1: unpend sleep timer  interrupt (0: no effect)
    unsigned bSC1        :1;     //1: unpend serial 1     interrupt (0: no effect)
    unsigned bSC2        :1;     //1: unpend serial 2     interrupt (0: no effect)
    unsigned bSEC        :1;     //1: unpend security     interrupt (0: no effect)
    unsigned bMACTMR     :1;     //1: unpend MAC timer    interrupt (0: no effect)
    unsigned bMACTX      :1;     //1: unpend MAC transmit interrupt (0: no effect)
    unsigned bMACRX      :1;     //1: unpend MAC receive  interrupt (0: no effect)
    unsigned bADC        :1;     //1: unpend ADC          interrupt (0: no effect)
    unsigned bIRQA       :1;     //1: unpend IRQA         interrupt (0: no effect)
    unsigned bIRQB       :1;     //1: unpend IRQB         interrupt (0: no effect)
    unsigned bIRQC       :1;     //1: unpend IRQC         interrupt (0: no effect)
    unsigned bIRQD       :1;     //1: unpend IRQD         interrupt (0: no effect)
    unsigned bDEBUG      :1;     //1: unpend debug        interrupt (0: no effect)
    unsigned breserved       :15;    //-----------------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_INT_PENDCLR_t;

typedef struct { //INT_ACTIVE - top-level active interrupts register                        (p.188)
    unsigned bTIM1       :1;     // timer 1      interrupt active
    unsigned bTIM2       :1;     //timer 2      interrupt active
    unsigned bMGMT       :1;     //management   interrupt active
    unsigned bBB         :1;     //baseband     interrupt active
    unsigned bSLEEPTMR   :1;     //sleep timer  interrupt active
    unsigned bSC1        :1;     //serial 1     interrupt active
    unsigned bSC2        :1;     //serial 2     interrupt active
    unsigned bSEC        :1;     //security     interrupt active
    unsigned bMACTMR     :1;     //MAC timer    interrupt active
    unsigned bMACTX      :1;     //MAC transmit interrupt active
    unsigned bMACRX      :1;     //MAC receive  interrupt active
    unsigned bADC        :1;     //ADC          interrupt active
    unsigned bIRQA       :1;     //IRQA         interrupt active
    unsigned bIRQB       :1;     //IRQB         interrupt active
    unsigned bIRQC       :1;     //IRQC         interrupt active
    unsigned bIRQD       :1;     //IRQD         interrupt active
    unsigned bDEBUG      :1;     //debug        interrupt active
    unsigned breserved       :15;    //----------------------------------
} __attribute__((__packed__)) register_stm32w1xx_INT_ACTIVE_t;

typedef struct { //INT_MISS - top-level missed interrupts register                          (p.189)
    unsigned breserved           :2;     //-------------------------------
    unsigned bMISSMGMT       :1;     // management   interrupt  missed
    unsigned bMISSBB         :1;     // baseband     interrupt  missed
    unsigned bMISSSLEEPTMR   :1;     // sleep timer  interrupt  missed
    unsigned bMISSSC1        :1;     // serial 1     interrupt  missed
    unsigned bMISSSC2        :1;     // serial 2     interrupt  missed
    unsigned bMISSSEC        :1;     // security     interrupt  missed
    unsigned bMISSMACTMR     :1;     // MAC timer    interrupt  missed
    unsigned bMISSMACTX      :1;     // MAC transmit interrupt  missed
    unsigned bMISSMACRX      :1;     // MAC receive  interrupt  missed
    unsigned bMISSADC        :1;     // ADC          interrupt  missed
    unsigned bMISSIRQA       :1;     // IRQA         interrupt  missed
    unsigned bMISSIRQB       :1;     // IRQB         interrupt  missed
    unsigned bMISSIRQC       :1;     // IRQC         interrupt  missed
    unsigned bMISSIRQD       :1;     // IRQD         interrupt  missed
    unsigned breserved2          :16;    //-------------------------------
} __attribute__((__packed__)) register_stm32w1xx_INT_MISS_t;

typedef struct { //SCS_AFSR - Auxiliary fault status register                               (p.190)
    unsigned bMISSED         :1; //A bus fault occurred when a bit was already set in this register.
    unsigned bRESERVED       :1; //Read in reserved memory block
    unsigned bPROTECTED      :1; //Bus fault from unpriveleged write
    unsigned bWRONGSIZE      :1; //Bus fault from 8 or 16-bit r/w from APB
    unsigned bresereved      :28;//---------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCS_AFSR_t;

/*typedef struct { //NVIC_interrupt_register - top-level interrupt registers
   register_stm32w1xx_INT_CFGSET_t      CFGSET;     //register to enable interrupts
   register_stm32w1xx_INT_CFGCLR_t      CFGCLR;     //register to disable interrupts
   register_stm32w1xx_INT_PENDSET_t     PENDSET;    //register to trigger interrupts
   register_stm32w1xx_INT_PENDCLR_t     PENDCLR;    //register to clear interrupts
   register_stm32w1xx_INT_ACTIVE_t      ACTIVE;     //indictates which interrupts are currently active
   register_stm32w1xx_INT_MISS_t        MISS;       //flag register for interrupts not processed by NVIC (set by event manager)
   register_stm32w1xx_SCS_AFSR_t        AFSR;       //register for recording bus faults

} __attribute__((__packed__)) register_stm32w1xx_NVIC_interrupt_registers_t;*/
//----------END NVIC------------//

//EVENT MANAGER REGISTERS grouped with peripherals (i.e.: INT_GPIOCFGx and INT_GPIOFLAG)

//}
//-------------------------------------END INTERRUPT REGISTERS------------------------------------//


//------------------------------------SERIAL REGISTERS-----------------------------------------//
//{

//--------Register Banks for SC1 and SC2--------------//
typedef struct { //SCx_RXBEGA - Receive DMA begin address register A (p.107)
    unsigned bSCx_RXBEGA :14;  //Receive DMA begin address register A
    unsigned breserved   :18;  //------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_RXBEGA_t;

typedef struct { //SCx_RXENDA - Receive DMA end address register A      (p.108)
    unsigned bSCx_RXENDA :14;  //Receive DMA end address register A
    unsigned breserved   :18;  //------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_RXENDA_t;

typedef struct { //SCx_RXBEGB - Receive DMA begin address register B    (p.107)
    unsigned bSCx_RXBEGB :14;  //Receive DMA begin address register B
    unsigned breserved   :18;  //------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_RXBEGB_t;

typedef struct { //SCx_RXENDB - Receive DMA end address register B      (p.108)
    unsigned bSCx_RXENDB :14;  //Receive DMA end address register B
    unsigned breserved   :18;  //------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_RXENDB_t;

typedef struct { //SCx_TXBEGA - Transmit DMA begin address register A   (p.105)
    unsigned bSCx_TXBEGA :14;  //Transmit DMA begin address register A
    unsigned breserved   :18;  //------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_TXBEGA_t;

typedef struct { //SCx_TXENDA - Transmit DMA end address register A     (p.106)
    unsigned bSCx_TXENDA :14;  //Transmit DMA end address register A
    unsigned breserved   :18;  //------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_TXENDA_t;

typedef struct { //SCx_TXBEGB - Transmit DMA begin address register B   (p.105)
    unsigned bSCx_TXBEGB :14;  //Transmit DMA begin address register B
    unsigned breserved   :18;  //------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_TXBEGB_t;

typedef struct { //SCx_TXENDB - Transmit DMA end address register B     (p.106)
    unsigned bSCx_TXENDB :14;  //Transmit DMA end address register B
    unsigned breserved   :18;  //------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_TXENDB_t;

typedef struct { //SCx_RXCNTA - Receive DMA count register A            (p.109)
    unsigned bSCx_RXCNTA :14;  //Receive DMA count register A
    unsigned breserved   :18;  //------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_RXCNTA_t;

typedef struct { //SCx_RXCNTB - Receive DMA count register B            (p.109)
    unsigned bSCx_RXCNTB :14;  //Receive DMA count register B
    unsigned breserved   :18;  //------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_RXCNTB_t;

typedef struct { //SCx_TXCNT - Transmit DMA count register              (p.109)
    unsigned bSCx_TXCNT :14;  //Transmit DMA count register
    unsigned breserved   :18;  //------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_TXCNT_t;

typedef struct { //SCx_DMASTAT - Serial DMA status register             (p.104)
    unsigned bSC_RXACTA  :1;     //This bit is set when DMA receive buffer A is active.
    unsigned bSC_RXACTB  :1;     //This bit is set when DMA receive buffer B is active.
    unsigned bSC_TXACTA  :1;     //This bit is set when DMA transmit buffer A is active.
    unsigned bSC_TXACTB  :1;     //This bit is set when DMA transmit buffer B is active.
    unsigned bSC_RXOVFA  :1;     //DMA receive buffer A was passed an overrun error from the receive FIFO
    unsigned bSC_RXOVFB  :1;     //DMA receive buffer B was passed an overrun error from the receive FIFO.
    unsigned bSC_RXPARA  :1;     //DMA receive buffer A reads a byte with a parity error from the receive FIFO.
    unsigned bSC_RXPARB  :1;     //DMA receive buffer B reads a byte with a parity error from the receive FIFO.
    unsigned bSC_RXFRMA  :1;     //DMA receive buffer A reads a byte with a frame error from the receive FIFO.
    unsigned bSC_RXFRMB  :1;     //DMA receive buffer B reads a byte with a frame error from the receive FIFO.
    unsigned bSC_RXSSEL  :3;     //Status of the receive count saved in SCx_RXCNTSAVED (SPI slave mode)
                                //when nSSEL deasserts. Cleared when a receive buffer is loaded and
                                //when the receive DMA is reset.
    unsigned breserved   :19;    //------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_DMASTAT_t;

typedef struct { //SCx_DMACTRL - Serial DMA control register            (p.103)
    unsigned bSC_RXLODA    :1;   //Setting this bit loads DMA receive buffer A addresses and allows the DMA
                                //controller to start processing receive buffer A.
    unsigned bSC_RXLODB    :1;   //Same as above for buffer B
    unsigned bSC_TXLODA    :1;   //Setting this bit loads DMA transmit buffer A addresses and allows the DMA
                                //controller to start processing transmit buffer A.
    unsigned bSC_TXLODB    :1;   //Same as above for buffer B
    unsigned bSC_RXDMARST  :1;   //Setting this bit resets the receive DMA. The bit clears automatically.
    unsigned bSC_TXDMARST  :1;   //Setting this bit resets the transmit DMA. The bit clears automatically.
    unsigned breserved     :26;  //------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_DMACTRL_t;

typedef struct { //SCx_RXERRA - DMA first receive error register A      (p.110)
    unsigned bSC_RXERRA      :14;
    unsigned breserved       :18;
} __attribute__((__packed__)) register_stm32w1xx_SCx_RXERRA_t;

typedef struct { //SCx_RXERRB - DMA first receive error register B      (p.111)
    unsigned bSC_RXERRB      :14;
    unsigned breserved       :18;
} __attribute__((__packed__)) register_stm32w1xx_SCx_RXERRB_t;

typedef struct { //SCx_DATA - Serial data register                      (p.111)
    unsigned bSC_DATA        :8;
    unsigned breserved       :24;
} __attribute__((__packed__)) register_stm32w1xx_SCx_DATA_t;

typedef struct { //SCx_SPISTAT - SPI status regiser                     (p.97)
    unsigned bSC_SPIRXOVF        :1; //This bit is set if a byte is received when the receive FIFO is full.
                                    //This bit is cleared by reading the data register.
    unsigned bSC_SPIRXVAL        :1; //This bit is set when the receive FIFO contains at least one byte.
    unsigned bSC_SPITXFREE       :1; //This bit is set when the transmit FIFO has space to accept at least one byte.
    unsigned bSC_SPITXIDLE       :1; //This bit is set when both the transmit FIFO and the transmit serializer are empty.
    unsigned breserved           :28;//-------------------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_SPISTAT_t;

typedef struct { //SCx_TWISTAT - I2C status regiser                     (p.98)
    unsigned bSC_TWIRXNAK        :1; //This bit is set when a NACK is received from the slave. It clears on the next I2C bus activity.
    unsigned bSC_TWITXFIN        :1; //This bit is set when a byte is transmitted. It clears on the next I2C bus activity.
    unsigned bSC_TWIRXFIN        :1; //This bit is set when a byte is received. It clears on the next I2C bus activity.
    unsigned bSC_TWICMDFIN       :1; //This bit is set when a START or STOP command completes. It clears on the next I2C bus activity.
    unsigned breserved           :28;//-------------------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_TWISTAT_t;

typedef struct { //SC1_UARTSTAT - UART status regiser - ONLY IN SC1     (p.100)
    unsigned bSC_UARTCTS          :1; //This bit is set when both the transmit FIFO and the transmit serializer are empty.
    unsigned bSC_UARTRXVAL        :1; //This bit is set when the receive FIFO contains at least one byte.
    unsigned bSC_UARTTXFREE       :1; //This bit is set when the transmit FIFO has space for at least one byte.
    unsigned bSC_UARTRXOVF        :1; //This bit is set when the receive FIFO has been overrun. This occurs if a byte
                                     //is received when the receive FIFO is full. This bit is cleared by reading the data register.
    unsigned bSC_UARTFRMERR       :1; //This bit is set when the byte in the data register was received with a frame
                                     //error. This bit is updated when the data register is read, and is cleared if the receive FIFO is empty.
    unsigned bSC_UARTPARERR       :1; //This bit is set when the byte in the data register was received with a parity
                                     //error. This bit is updated when the data register is read, and is cleared if the receive FIFO is empty.
    unsigned bSC_UARTTXIDLE       :1; //This bit is set when both the transmit FIFO and the transmit serializer are empty.
    unsigned breserved            :25;//-------------------------------------------------
} __attribute__((__packed__)) register_stm32w1xx_SC1_UARTSTAT_t;

typedef struct { //SCx_TWICTRL1 - I2C control register 1                (p.99)
    unsigned bSC_TWIRECV         :1; //Setting this bit receives a byte. It clears when the command completes.
    unsigned bSC_TWISEND         :1; //Setting this bit transmits a byte. It clears when the command completes.
    unsigned bSC_TWISTART        :1; //Setting this bit sends the START or repeated START command. It clears when the command completes.
    unsigned bSC_TWISTOP         :1; //Setting this bit sends the STOP command. It clears when the command completes.
    unsigned breserved           :28;//--------------------------------------------------
}  __attribute__((__packed__)) register_stm32w1xx_SCx_TWICTRL1_t;

typedef struct { //SCx_TWICTRL2 - I2C control register 2                (p.99)
    unsigned bSC_TWIACK          :1; //Setting this bit signals ACK after a received byte. Clearing this bit signals NACK after a received byte.
    unsigned breserved           :31;//--------------------------------------------------
}  __attribute__((__packed__)) register_stm32w1xx_SCx_TWICTRL2_t;

typedef struct { //SCx_MODE - Serial mode register                      (p.92)
    unsigned bSC_MODE          :2; //set with register_stm32w1xx_sc_mode_e
    unsigned breserved         :30;//--------------------------------------------------
}  __attribute__((__packed__)) register_stm32w1xx_SCx_MODE_t;

typedef struct { //SCx_SPICFG - SPI configuration register              (p.96)
    unsigned bSC_SPIPOL      :1; //Clock polarity configuration: clear this bit for a rising leading edge and
                                //set this bit for a falling leading edge.
    unsigned bSC_SPIPHA      :1; //Clock phase configuration: clear this bit to sample on the leading (first edge) and
                                //set this bit to sample on the second edge.
    unsigned bSC_SPIORD      :1; //This bit specifies the bit order in which SPI data is transmitted and received.
                                //0: Most significant bit first. 1: Least significant bit first.
    unsigned bSC_SPIRPT      :1; //This bit controls behavior on a transmit buffer underrun condition in slave mode.
    unsigned bSC_SPIMST      :1; //Set this bit to put the SPI in master mode, clear this bit to put the SPI in slave mode.
    unsigned bSC_SPIRXDRV    :1; //Receiver-driven mode selection bit (SPI master mode only).
    unsigned breserved       :26;//--------------------------------------------------
}  __attribute__((__packed__)) register_stm32w1xx_SCx_SPICFG_t;

typedef struct { //SC1_UARTCFG - UART configuration register - ONLY SC1 (p.101)
    unsigned bSC_UARTRTS          :1; //nRTS is an output to control the flow of serial data sent to the STM32W108 from another device.
    unsigned bSC_UART8BIT         :1; //Number of data bits.
    unsigned bSC_UART2STP         :1; //Number of stop bits transmitted.
    unsigned bSC_UARTPAR          :1; //Specifies whether to use parity bits.
    unsigned bSC_UARTODD          :1; //If parity is enabled, specifies the kind of parity.
    unsigned bSC_UARTFLOW         :1; //Set this bit to enable using nRTS/nCTS flow control signals.
    unsigned bSC_UARTAUTO         :1; //Set this bit to enable automatic nRTS control by hardware (SC_UARTFLOW must also be set).
    unsigned breserved            :25;//--------------------------------------------------
}  __attribute__((__packed__)) register_stm32w1xx_SC1_UARTCFG_t;

typedef struct { //SCx_RATELIN - Serial clock linear prescalar reg       (p.97)
    unsigned bSC_RATELIN         :4; //The linear component (LIN) of the clock rate in the equation:Rate = 12 MHz / ( (LIN + 1) * (2^EXP) )
    unsigned breserved           :28;//--------------------------------------------------
}  __attribute__((__packed__)) register_stm32w1xx_SCx_RATELIN_t;

typedef struct { //SCx_RATEEXP - Serial clock exponential prescalar reg  (p.98)
    unsigned bSC_RATEEXP         :4; //The exponential component (EXP) of the clock rate in the equation: Rate = 12 MHz / ( (LIN + 1) * (2^EXP) )
    unsigned breserved           :28;//--------------------------------------------------
}  __attribute__((__packed__)) register_stm32w1xx_SCx_RATEEXP_t;

typedef struct { //SC1_UARTPER - UART baud rate period register          (p.102)
    unsigned bSC_UARTPER         :16; //The integer part of baud rate period (N) in the equation: Rate = 24 MHz / ( (2 * N) + F )
    unsigned breserved           :16; //--------------------------------------------------
}  __attribute__((__packed__)) register_stm32w1xx_SC1_UARTPER_t;

typedef struct { //SC1_UARTFRAC - UART baud rate fractional period reg   (p.102)
    unsigned bSC_UARTFRAC        :1; //The fractional part of the baud rate period (F) in the equation: Rate = 24 MHz / ( (2 * N) + F )
    unsigned breserved           :31;//--------------------------------------------------
}  __attribute__((__packed__)) register_stm32w1xx_SC1_UARTFRAC_t;

typedef struct { //SCx_RXCNTSAVED - Saved receive DMA count reg          (p.110)
    unsigned bSC_RXCNTSAVED      :14; //Receive DMA count saved in SPI slave mode when nSSEL deasserts.
    unsigned breserved           :18; //--------------------------------------------------
}  __attribute__((__packed__)) register_stm32w1xx_SCx_RXCNTSAVED_t;

typedef struct {
    register_stm32w1xx_SCx_RXBEGA_t         RXBEGA;
    register_stm32w1xx_SCx_RXENDA_t         RXENDA;
    register_stm32w1xx_SCx_RXBEGB_t         RXBEGB;
    register_stm32w1xx_SCx_RXENDB_t         RXENDB;
    register_stm32w1xx_SCx_TXBEGA_t         TXBEGA;
    register_stm32w1xx_SCx_TXENDA_t         TXENDA;
    register_stm32w1xx_SCx_TXBEGB_t         TXBEGB;
    register_stm32w1xx_SCx_TXENDB_t         TXENDB;
    register_stm32w1xx_SCx_RXCNTA_t         RXCNTA;
    register_stm32w1xx_SCx_RXCNTB_t         RXCNTB;
    register_stm32w1xx_SCx_TXCNT_t          TXCNT;
    register_stm32w1xx_SCx_DMASTAT_t        DMASTAT;
    register_stm32w1xx_SCx_DMACTRL_t        DMACTRL;
    register_stm32w1xx_SCx_RXERRA_t         RXERRA;
    register_stm32w1xx_SCx_RXERRB_t         RXERRB;
    register_stm32w1xx_SCx_DATA_t           DATA;
    register_stm32w1xx_SCx_SPISTAT_t        SPISTAT;
    register_stm32w1xx_SCx_TWISTAT_t        TWISTAT;
    register_stm32w1xx_SC1_UARTSTAT_t       UARTSTAT;
    register_stm32w1xx_SCx_TWICTRL1_t       TWICTRL1;
    register_stm32w1xx_SCx_TWICTRL2_t       TWICTRL2;
    register_stm32w1xx_SCx_MODE_t           MODE;
    register_stm32w1xx_SCx_SPICFG_t         SPICFG;
    register_stm32w1xx_SC1_UARTCFG_t        UARTCFG;
    register_stm32w1xx_SCx_RATELIN_t        RATELIN;
    register_stm32w1xx_SCx_RATEEXP_t        RATEEXP;
    register_stm32w1xx_SC1_UARTPER_t        UARTPER;
    register_stm32w1xx_SC1_UARTFRAC_t       UARTFRAC;
    register_stm32w1xx_SCx_RXCNTSAVED_t     RXCNTSAVED;
} __attribute__((__packed__)) register_stm32w1xx_SCx_t;
//--------END Register Banks for SC1 and SC2--------------//

typedef struct { // INT_SCxFLAG - Serial controller interrupt flag reg   (p.93)
    unsigned bSCRXVAL                :1; //Receive buffer has data interrupt pending.
    unsigned bSCTXFREE               :1; //Transmit buffer free interrupt pending.
    unsigned bSCTXIDLE               :1; //Transmitter idle interrupt pending.
    unsigned bSCRXOVF                :1; //Receive buffer overrun interrupt pending.
    unsigned bSCTXUND                :1; //Transmit buffer underrun interrupt pending.
    unsigned bSCRXFIN                :1; //Receive operation complete (I2C) interrupt pending.
    unsigned bSCTXFIN                :1; //Transmit operation complete (I2C) interrupt pending.
    unsigned bSCCMDFIN               :1; //START/STOP command complete (I2C) interrupt pending.
    unsigned bSCNAK                  :1; //NACK received (I2C) interrupt pending.
    unsigned bSCRXULDA               :1; //DMA receive buffer A unloaded interrupt pending.
    unsigned bSCRXULDB               :1; //DMA receive buffer B unloaded interrupt pending.
    unsigned bSCTXULDA               :1; //DMA transmit buffer A unloaded interrupt pending.
    unsigned bSCTXULDB               :1; //DMA transmit buffer B unloaded interrupt pending.
    unsigned bSC1FRMERR              :1; //Frame error received (UART) interrupt pending.
    unsigned bSC1PARERR              :1; //Parity error received (UART) interrupt pending.
    unsigned breserved                   :17;//-------------------------
} __attribute__((__packed__)) register_stm32w1xx_INT_SCxFLAG_t;

typedef struct { // INT_SCxCFG - Serial controller interrupt config reg  (p.94)
    unsigned bSCRXVAL                :1; //Receive buffer has data interrupt enable.
    unsigned bSCTXFREE               :1; //Transmit buffer free interrupt enable.
    unsigned bSCTXIDLE               :1; //Transmitter idle interrupt enable.
    unsigned bSCRXOVF                :1; //Receive buffer overrun interrupt enable.
    unsigned bSCTXUND                :1; //Transmit buffer underrun interrupt enable.
    unsigned bSCRXFIN                :1; //Receive operation complete (I2C) interrupt enable.
    unsigned bSCTXFIN                :1; //Transmit operation complete (I2C) interrupt enable.
    unsigned bSCCMDFIN               :1; //START/STOP command complete (I2C) interrupt enable.
    unsigned bSCNAK                  :1; //NACK received (I2C) interrupt enable.
    unsigned bSCRXULDA               :1; //DMA receive buffer A unloaded interrupt enable.
    unsigned bSCRXULDB               :1; //DMA receive buffer B unloaded interrupt enable.
    unsigned bSCTXULDA               :1; //DMA transmit buffer A unloaded interrupt enable.
    unsigned bSCTXULDB               :1; //DMA transmit buffer B unloaded interrupt enable.
    unsigned bSC1FRMERR              :1; //Frame error received (UART) interrupt enable.
    unsigned bSC1PARERR              :1; //Parity error received (UART) interrupt enable.
    unsigned breserved                   :17;//-------------------------
} __attribute__((__packed__)) register_stm32w1xx_INT_SCxCFG_t;

typedef struct { // SCx_INTMODE - Serial controller interrupt mode reg   (p.95)
    unsigned bSC_RXVALLEVEL                :1; //Receive buffer has data interrupt mode  [FOR ALL 3, 0=EDGE TRIGGERED; 1=LEVEL TRIGGERED]
    unsigned bSC_TXFREELEVEL               :1; //Transmit buffer free interrupt mode
    unsigned bSC_TXIDLELEVEL               :1; //Transmitter idle interrupt mode
    unsigned breserved                     :29;//-------------------------
} __attribute__((__packed__)) register_stm32w1xx_SCx_INTMODE_t;

//}
//----------------------------------END SERIAL REGISTERS-----------------------------------------//

//} STM32 Registers



#endif  /*register_stm32w1xx_H*/

