/*{ cm3_basic.h ***********************************************
 *
 * TheToolChain.com
 *
 * Written by Gregor Rebel, Sascha Poggemann 2010-2018
 *
 * Basic datatypes, enums and functions additional to cm3_basic.h
 * 
}*/

#ifndef EM357_BASIC_H
#define EM357_BASIC_H

//{ includes *************************************************************


#include <stdint.h>
#include "../basic/basic_cm3.h"

// define basic datatypes required to include HAL-Library
typedef unsigned long t_u32;
typedef t_u16 ut_int16;
typedef  t_u8 t_u8;

#ifdef abs
#undef abs // abs() is also defined in gnu.h
#endif

#include "gnu.h"   // micro/cortexm3/compiler/gnu.h"
//X #include "stm32w108xx_sc.h"
#include "board.h" //micro/cortexm3/stm32w108/board.h"
#include "hal.h"

//} includes *************************************************************
#ifdef EXTENSION_300_scheduler_freertos
#  error Architecture STM32W currently does not support multitasking scheduler (systick is used by SimpleMAC library) - Disable multitasking scheduler in your activate_project.sh file!
#endif

//{ types ****************************************************************


// shall be defined by stdint.h
#ifndef __INT32_TYPE__
#ifndef __have_long32 // defined in ~/Source/TheToolChain_devel/InstallData/050_compiler_sourcery_gpp/arm-2010q1/bin/../lib/gcc/arm-none-eabi/4.4.1/../../../../arm-none-eabi/include/stdint.h:79
  typedef t_s32 t_int32;
  typedef t_s16 t_int16;
  typedef  t_s8 t_int8;
#endif
#endif

//} types ****************************************************************
//{ macros ***************************************************************

#define assert_param(CONDITION) Assert(CONDITION)

//} macros ***************************************************************
//{ includes2 ************************************************************

//? #include "regs.h"
//? #include "stm32w108_type.h"
#include "micro-common.h"

//} includes2 ************************************************************
//{ Function prototypes **************************************************

/* basic hardware initialization
 */
void stm32w_hardware_init();

void stm32w_stupid_sleep();

//} Function prototypes **************************************************

#endif
