#ifndef STM32W_USART_H
#define STM32W_USART_H

/** { stm32w_usart.h **********************************************
 
                           The ToolChain
                      
   High-Level interface for USART device.

   Structures, Enums and Defines being required by high-level usart and application.

   Note: See ttc_usart.h for description of stm32w independent USART implementation.
  
   Authors: Greg Knoll 2013

 
}*/

/*{ Defines/ TypeDefs ****************************************************
//see stm32w_usart_types.h
//} Defines*/

//{ Includes *************************************************************

#include "stm32w_usart_types.h"
#include "../ttc_usart_types.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"

//} Includes

//{ Macro definitions ****************************************************

#define _driver_ttc_usart_init(USART_Generic) stm32w_usart_init(USART_Generic)

//} Macros

//{ Function prototypes **************************************************

e_ttc_usart_errorcode stm32w_usart_get_features(t_u8 PhysicalIndex, t_ttc_usart_config* USART_Generic);

/* initializes single USART
 * @param USART_Generic   filled out struct t_ttc_usart_config (Note: referenced struct must stay in memory as long as device is in use!)
 * @return  == 0:         USART has been initialized successfully; != 0: error-code
 */
e_ttc_usart_errorcode stm32w_usart_init(t_ttc_usart_config* USART_Generic);













//------------------------------Written by TEMPLATE----------------------------------//
/** checks if indexed USART bus already has been initialized
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for LogicalIndex)
 * @return              == TRUE: indexed USART unit is already initialized
 */
//bool stm32w_usart_check_initialized(t_ttc_usart_config* Config);

/** returns reference to low-level configuration struct of indexed USART device
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for LogicalIndex)
 * @return              pointer to struct t_ttc_usart_config (will assert if no configuration available)
 */
//t_stm32w_usart_config* stm32w_usart_get_configuration(t_ttc_usart_config* Config);

/** loads configuration of indexed USART interface with default values
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
//e_ttc_usart_errorcode stm32w_usart_load_defaults(t_ttc_usart_config* Config);

/** fills out given Config with maximum valid values for indexed USART
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for LogicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
//e_ttc_usart_errorcode stm32w_usart_get_features(t_ttc_usart_config* Config);

/** reset configuration of indexed USART device into default state
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for LogicalIndex)
 * @return              == 0: USART has been initialized successfully; != 0: error-code
 */
//e_ttc_usart_errorcode stm32w_usart_reset(t_ttc_usart_config* Config);

/** initializes single USART
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for LogicalIndex)
 * @return              == 0: USART has been initialized successfully; != 0: error-code
 */
//e_ttc_usart_errorcode stm32w_usart_init(t_ttc_usart_config* Config);

/** shutdown single USART device
 * @param Config        pointer to struct t_ttc_usart_config (must have valid value for LogicalIndex)
 * @return              == 0: USART has been shutdown successfully; != 0: error-code
 */
//e_ttc_usart_errorcode stm32w_usart_deinit(t_ttc_usart_config* Config);

/** maps from logical to physical device index
 *
 * High-level usarts (ttc_usart_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_USARTn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be 
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any 
 * order.
 *
 * @param LogicalIndex  logical index of usart device (1..ttc_usart_get_max_index() )
 * @return              physical index of usart device (0 = first physical usart device, ...)
 */
//t_u8 stm32w_usart_logical_2_physical_index(t_u8 LogicalIndex);

//} Function prototypes

#endif //STM32W_USART_H
