/*{ stm32w_basic.h ***********************************************
 *
 * TheToolChain.com
 *
 * Written by Gregor Rebel, Sascha Poggemann 2010-2018
 *
 * Basic datatypes, enums and functions additional to cm3_basic.h
 * 
}*/

#include "stm32w_basic.h"

extern void ttc_string_printf(const char *Format, ...);

void stm32w_hardware_init() {

    //***halInit() was commented out because halInternalSleep() was not working properly with configuration at the time
    //halInit(); // init EM357 hardware abstraction library


}
void stSerialPrintf(const char *Format, ...) {
    va_list Args;
    va_start(Args, Format);
    //? ttc_string_printf(Format, Args);
}
void stm32w_stupid_sleep() {
    for (volatile t_u32 I = 0; I < 1000000; I++);
}
