/** { stm32w_interrupt.c ************************************************
 
                           The ToolChain
                      
   High-Level interface for INTERRUPT device.
     
   Note: See ttc_interrupt.h for description of stm32w independent INTERRUPT implementation.
  
   Authors: 
}*/

#include "stm32w_interrupt.h"

// Arrays of interrupt service routines (provided by ttc_interrupt.c)
ttc_array_dynamic_extern(ttc_usart_isrs_t*, ttc_usart_isrs);
ttc_array_dynamic_extern(ttc_gpio_isrs_t*,  ttc_gpio_isrs);


//{ Function definitions ****************************************************
ttc_interrupt_errorcode_e stm32_interrupt_init_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, void (*ISR)(physical_index_t, void*), void* Argument, void (**OldISR)(physical_index_t, void*), void** OldArgument){

}
ttc_interrupt_errorcode_e stm32_interrupt_init_gpio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, void (*ISR)(physical_index_t, void*), void* Argument, void (**OldISR)(physical_index_t, void*), void** OldArgument){

}


//} Function definitions ====================================================

//{ Interrupt Handlers (ISRs) ***********************************************

//--------------implemented/supported-------------------//

void GPIO_General_IRQHandler(u8_t InterruptLine) {
    /*ttc_gpio_isrs_t* Entry = A(ttc_gpio_isrs, InterruptLine);
    if (Entry->isr)
        Entry->isr(Entry->PhysicalIndex, Entry->Argument);*/
    Assert_Interrupt(0, ec_UNKNOWN);
}

void halIrqAIsr() {
    //GPIO_General_IRQHandler(0);  //what to pass as InterruptLine to A(ttc_gpio_isrs, InterruptLine)
    //EXTI_ClearITPendingBit(EXTI_Line0); //What to pass instead of EXTI_Line0. And is there an EXTI_ClearITPendingBit() for 32w?
}
void halIrqBIsr() {
    //GPIO_General_IRQHandler(0);
    //EXTI_ClearITPendingBit(EXTI_Line0);
}
void halIrqCIsr() {
    //GPIO_General_IRQHandler(0);
    //EXTI_ClearITPendingBit(EXTI_Line0);
}
void halIrqDIsr() {
    //GPIO_General_IRQHandler(0);
    //EXTI_ClearITPendingBit(EXTI_Line0);
}
void Default_Handler(){
    Assert_Interrupt(0, ec_UNKNOWN);
}

//----------------not (YET) implemented/supported-------//
void NMI_Handler() {
	//call your interrupt-handler right here, or else ...
	Assert_Interrupt(0, ec_UNKNOWN);
}
void HardFault_Handler() {
	//call your interrupt-handler right here, or else ...
	Assert_Interrupt(0, ec_UNKNOWN);
}
void MemManage_Handler() {
	//call your interrupt-handler right here, or else ...
	Assert_Interrupt(0, ec_UNKNOWN);
}
void BusFault_Handler() {
	//call your interrupt-handler right here, or else ...
	Assert_Interrupt(0, ec_UNKNOWN);
}
void UsageFault_Handler() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}
void SVC_Handler() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}
void DebugMonitor_Handler() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}
void PendSV_Handler() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}
/*void halSysTickIsr() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}*/
void halTimer1Isr() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}
void halTimer2Isr() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}
/*void halManagementIsr() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}*/
void halBaseBandIsr() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}
void halSleepTimerIsr() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}
void halSc1Isr() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}
void halSc2Isr() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}
void halSecurityIsr() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}
/*void halStackMacTimerIsr() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}*/
/*void stmRadioTransmitIsr() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}*/
/*void stmRadioReceiveIsr() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}*/
/*void halAdcIsr() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}*/
void halDebugIsr() {
    //call your interrupt-handler right here, or else ...
    Assert_Interrupt(0, ec_UNKNOWN);
}





//} END Interrupt Handlers =====================================================
