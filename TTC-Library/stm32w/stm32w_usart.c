/** { stm32w_usart.c ************************************************
 
                           The ToolChain
                      
   High-Level interface for USART device.
     
   Note: See ttc_usart.h for description of stm32w independent USART implementation.
  
   Authors: Greg Knoll 2013
}*/

#include "stm32w_usart.h"


//{ Function definitions ***************************************************

e_ttc_usart_errorcode stm32w_usart_get_features(t_u8 PhysicalIndex, t_ttc_usart_config* USART_Generic) {
//X     if (!su_Initialized) stm32_usart_reset_all();
    // assumption: *USART_Generic has been zeroed

    Assert_USART(USART_Generic != NULL, ttc_assert_origin_auto);
    if (PhysicalIndex >= TTC_USART_MAX_AMOUNT) return tue_DeviceNotFound;
    USART_Generic->USART_Arch.PhysicalIndex = PhysicalIndex;

    USART_Generic->Flags.All=0;
    USART_Generic->Flags.Bits.Receive          = 1;
    USART_Generic->Flags.Bits.Transmit         = 1;
    USART_Generic->Flags.Bits.TransmitParity   = 1;
    USART_Generic->Init.Flags.DelayedTransmits = 1;
    USART_Generic->Flags.Bits.SendBreaks       = 1;
    USART_Generic->Flags.Bits.ParityEven       = 1;
    USART_Generic->Flags.Bits.ParityOdd        = 1;
    USART_Generic->Flags.Bits.IrqOnRxNE        = 1;

    /*{ remaining bits
    USART_Generic->Flags.Bits.ControlParity  =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.RtsCts         =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.SingleWire     =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.SmartCard      =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.Synchronous    =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.LIN            =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.IrDA           =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.IrDALowPower   =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.SmartCardNACK  =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.RxDMA          =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.TxDMA          =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.Rts            =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.IrqOnCts       =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.IrqOnError     =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.IrqOnIdle      =0; // (ToDo: implement)
    USART_Generic->Flags.Bits.IrqOnTxComplete=0; // (ToDo: implement)
    USART_Generic->Flags.Bits.IrqOnParityError= 0; // (ToDo: implement)
    }*/

    USART_Generic->Init.WordLength      = 9;
    USART_Generic->Init.HalfStopBits    = 4;
    USART_Generic->Init.BaudRate        = 115200;
    USART_Generic->Init.TimeOut         = -1;        // max positive value

    USART_TypeDef* USART_Base = NULL;

    if (1) { // find indexed USART on current board
      switch (PhysicalIndex) {                // find corresponding USART as defined by makefile.100_board_*
        case 0: USART_Base = USART1; break;
        default: Assert_USART(0, ttc_assert_origin_auto); break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
        }
    }

    USART_Generic->Layout=1;     // USART1 and USART2 provide 1 alternate pin layout


    return tue_OK;
}

e_ttc_usart_errorcode stm32w_usart_init(t_ttc_usart_config* USART_Generic){

    //-----------------Check USART_Generic---------------------//
    if(1){
    //Check that the USART argument is not empty
    Assert_USART(USART_Generic != NULL, ttc_assert_origin_auto);

    //In USART_Generic, check member LogicalIndex to find which USART device to use
    //[in stm32w, there is only one USART, so it should be 1]
    t_u8 LogicalIndex = USART_Generic->LogicalIndex;

    //There is no USART0, so make sure index > 0
    Assert(LogicalIndex > 0, ttc_assert_origin_auto); // logical index starts at 1

    //Make sure that the logic index points to a possible device
    if (LogicalIndex > TTC_AMOUNT_USARTS)
        return tue_DeviceNotFound;

    //In the stm32w, the word (character) length can be 9-12
    if ( (USART_Generic->Init.WordLength   < 9) || (USART_Generic->Init.WordLength   > 12) )
        return tue_InvalidWordSize;

    //Are HalfStopBits implemented in stm32w????
    /*if ( (USART_Generic->Init.HalfStopBits < 1) || (USART_Generic->HControlParityalfStopBits > 4) )
        return tue_InvalidStopBits;*/

    // validate USART_Features
    t_ttc_usart_config USART_Features;
    e_ttc_usart_errorcode Error=stm32w_usart_get_features(LogicalIndex, &USART_Features);
    if (Error) return Error;

    USART_Generic->Flags.All &= USART_Features.Flags.All; // mask out unavailable flags

    if (USART_Generic->Layout > USART_Features.Layout)
      USART_Generic->Layout=USART_Features.Layout;
    }
    //-------------END Check USART_Generic---------------------//


    //----------------Set USART_Arch of USART_Generic----------//

    //Get pointer to architecture-specific register struct:should return t_stm32w_usart_architecture
    t_ttc_usart_architecture* USART_Arch = &(USART_Generic->USART_Arch);

    //-------------Base and mode------------//
    if(1){

    //Assign Base address of SC1 (defined in StdPeripheral) to Base of USART
    USART_Arch->Base = register_stm32w_SC1; //register_stm32w_SC1 defined in DEPRECATED_stm32w_registers.c

    //Set SC1_Mode
    USART_Arch->Base->MODE = sc_mode_UART;

    }
    //---------END Base and mode------------//


    //-------------Configure GPIOs----------//
    if(1){
        //Assign pins to TX. TX on stm32w is PB1
        USART_Arch->PortTxD.GPIOx = stm32w_GPIOB;
        USART_Arch->PortTxD.Pin = 1;
        //TXD is on pin PB1 and must be set to alternate output push-pull
        stm32w_gpio_init(stm32w_GPIOB, 1, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max);

        //Assign pins to RX. RX on stm32w is PB2
        USART_Arch->PortRxD.GPIOx = stm32w_GPIOB;
        USART_Arch->PortRxD.Pin = 2;
        //RXD is on PB2 and must be set to input (floating?)
        stm32w_gpio_init(stm32w_GPIOB, 2,E_ttc_gpio_mode_input_floating , E_ttc_gpio_speed_max);

        //for nCTS and nRTS, see sections below
    }
    //---------END Configure GPIOs----------//


    //------------Data Bits-----------------//
    if(1){
        //How many Data bits: clear for 7 bit, set for 8 bits
        if(USART_Generic->Init.WordLength == 7)
            USART_Arch->Base->UARTCFG.SC_UART8BIT = 0;  //7 bits
        else if(USART_Generic->Init.WordLength == 8)
            USART_Arch->Base->UARTCFG.SC_UART8BIT  = 1; //8 bits
    }
    //------------END Data Bits-------------//


    //------------Stop Bits-----------------//
    if(1){
        //How many stop bits: clear for 1 bit, set for 2 bits
        if(USART_Generic->Init.HalfStopBits == 2)
            USART_Arch->Base->UARTCFG.SC_UART2STP = 0; //one stop bit
        else if(USART_Generic->Init.HalfStopBits == 4)
            USART_Arch->Base->UARTCFG.SC_UART2STP  = 1;  //two stop bits
    }
    //------------END Stop Bits-------------//


    //------------Parity--------------------//
    if(1){
        if(USART_Generic->Flags.Bits.TransmitParity) //Parity: 0->no parity, 1->parity
        {
            //Enable the parity bit
            USART_Arch->Base->UARTCFG.SC_UARTPAR = 1;

            //Is parity even (0) or odd (1)
            if(USART_Generic->Flags.ParityEven)
                USART_Arch->Base->UARTCFG.SC_UARTODD = 0; //even
            if(USART_Generic->Flags.ParityOdd)
                USART_Arch->Base->UARTCFG.SC_UARTODD = 1; //odd
        }
        else USART_Arch->Base->UARTCFG.SC_UARTPAR = 0;
    }
    //------------END Parity----------------//


    //--------------nCTS-------------------//
    if(USART_Generic->Flags.Bits.Cts)
    {
        USART_Arch->Base->UARTCFG.SC_UARTFLOW = 1;

        //Assign pins to CTS. CTS on stm32w is PB3
        USART_Arch->PortCTS.GPIOx = stm32w_GPIOB;
        USART_Arch->PortCTS.Pin = 3;
        //init PB3 and set to input
        stm32w_gpio_init(stm32w_GPIOB, 3,E_ttc_gpio_mode_input_floating , E_ttc_gpio_speed_max);

    }
    //----------END nCTS-------------------//


    //--------------nRTS-------------------//
    if(USART_Generic->Flags.Bits.Rts)
    {
        USART_Arch->Base->UARTCFG.SC_UARTAUTO = 1;

        //Assign pins to RTS. RTS on stm32w is PB4
        USART_Arch->PortRTS.GPIOx = stm32w_GPIOB;
        USART_Arch->PortRTS.Pin = 4;
        //init PB4 and set to alternate output push-pull
        stm32w_gpio_init(stm32w_GPIOB, 4, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max);
    }
    //----------END nRTS-------------------//


    //-------------set Baudrate------------//
    switch (USART_Generic->Init.BaudRate){
    case 300:{
        USART_Arch->Base->UARTPER = 40000 ;
        USART_Arch->Base->UARTFRAC = 0;
        break;
    }
    case 2400:{
        USART_Arch->Base->UARTPER = 5000;
        USART_Arch->Base->UARTFRAC = 0;
        break;
    }
    case 4800:{
        USART_Arch->Base->UARTPER = 2500;
        USART_Arch->Base->UARTFRAC = 0;
        break;
    }
    case 9600:{
        USART_Arch->Base->UARTPER = 1250;
        USART_Arch->Base->UARTFRAC = 0;
        break;
    }
    case 19200:{
        USART_Arch->Base->UARTPER = 625;
        USART_Arch->Base->UARTFRAC = 0;
        break;
    }
    case 38400:{
        USART_Arch->Base->UARTPER = 312;
        USART_Arch->Base->UARTFRAC = 1;
        break;
    }
    case 57600:{
        USART_Arch->Base->UARTPER = 208;
        USART_Arch->Base->UARTFRAC = 1;
        break;
    }
    case 115200:{
        USART_Arch->Base->UARTPER = 104;
        USART_Arch->Base->UARTFRAC = 0;
        break;
    }
    case 230400:{
        USART_Arch->Base->UARTPER = 52;
        USART_Arch->Base->UARTFRAC = 0;
        break;
    }
    case 460800:{
        USART_Arch->Base->UARTPER = 26;
        USART_Arch->Base->UARTFRAC = 0;
        break;
    }
    case 921600:{
        USART_Arch->Base->UARTPER = 13;
        USART_Arch->Base->UARTFRAC = 0;
        break;
    }
    default: return tue_InvalidBaudRate;
    }
    //---------END set Baudrate------------//


    //-------------END Set USART_Arch of USART_Generic----------//

}


//}FunctionDefinitions
//{ private functions (ideally) ********************************************

//} private functions
