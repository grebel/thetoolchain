#ifndef STM32W_INTERRUPT_H
#define STM32W_INTERRUPT_H

/** { stm32w_interrupt.h **********************************************
 
                           The ToolChain
                      
   High-Level interface for INTERRUPT device.

   Structures, Enums and Defines being required by high-level interrupt and application.

   Note: See ttc_interrupt.h for description of stm32w independent INTERRUPT implementation.
  
   Authors: Greg Knoll 2013

 
}*/


/*{ Defines/ TypeDefs ****************************************************

    //Located in stm32w_interrupt_types.h

//} Defines*/


//{ Includes *************************************************************

#include "stm32w_interrupt_types.h"
#include "../ttc_interrupt_types.h"
#include "../ttc_task.h"
#include "../ttc_memory.h"
#include "stm32w108xx_exti.h"

//} Includes

/*{ Macro definitions ****************************************************

//#define Driver_interrupt_load_defaults(Config) stm32w_interrupt_load_defaults(Config)
//#define Driver_interrupt_deinit(Config)        stm32w_interrupt_deinit(Config)
//#define Driver_interrupt_init(Config)          stm32w_interrupt_init(Config)


//} Macros */

//{ Function prototypes **************************************************





// initializers for individual functional units (called from ttc_interrupt_init_ () functions in ttc_interrupt.h )
ttc_interrupt_errorcode_e stm32_interrupt_init_usart(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, void (*ISR)(physical_index_t, void*), void* Argument, void (**OldISR)(physical_index_t, void*), void** OldArgument);
ttc_interrupt_errorcode_e stm32_interrupt_init_gpio(ttc_interrupt_type_e Type, physical_index_t PhysicalIndex, void (*ISR)(physical_index_t, void*), void* Argument, void (**OldISR)(physical_index_t, void*), void** OldArgument);






//} Function prototypes

//{ Interrupt Handlers (ISRs)

void NMI_Handler();
void HardFault_Handler();
void MemManage_Handler();
void BusFault_Handler();
void UsageFault_Handler();
void SVC_Handler();
void DebugMonitor_Handler();
void PendSV_Handler();
//void halSysTickIsr();
void halTimer1Isr();
void halTimer2Isr();
//void halManagementIsr();
void halBaseBandIsr();
void halSleepTimerIsr();
void halSc1Isr();
void halSc2Isr();
void halSecurityIsr();
//void halStackMacTimerIsr();
//void stmRadioTransmitIsr();
//void stmRadioReceiveIsr();
//void halAdcIsr();
void halIrqAIsr();
void halIrqBIsr();
void halIrqCIsr();
void halIrqDIsr();
void halDebugIsr();

void Default_Handler();

//} END Interrupt Handlers



#endif //STM32W_INTERRUPT_H
