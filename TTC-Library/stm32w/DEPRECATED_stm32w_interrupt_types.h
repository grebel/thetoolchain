#ifndef STM32W_INTERRUPT_TYPES_H
#define STM32W_INTERRUPT_TYPES_H

/** { stm32w_interrupt.h ************************************************
 
                           The ToolChain
                      
   High-Level interface for INTERRUPT device.

   Structures, Enums and Defines being required by ttc_interrupt_types.h

   Note: See ttc_interrupt.h for description of stm32w independent INTERRUPT implementation.
  
   Authors: Greg Knoll 2013

 
}*/





//{ Defines/ TypeDefs **********************************************************************
//-------------------Compatibility interface for ttc_interrupt.h------------
#define _driver_ttc_interrupt_init(Type, PhysicalIndex, ISR, Argument, OldISR, OldArgument) \
             stm32w_interrupt_init(Type, PhysicalIndex, ISR, Argument, OldISR, OldArgument)

#define _driver_ttc_interrupt_init_gpio(Type, PhysicalIndex, ISR, Argument, OldISR, OldArgument) \
             stm32w_interrupt_init_gpio(Type, PhysicalIndex, ISR, Argument, OldISR, OldArgument)

#define _driver_ttc_interrupt_enable(Type, PhysicalIndex, EnableIRQ) \
             stm32w_interrupt_enable(Type, PhysicalIndex, EnableIRQ)

#define _driver_ttc_interrupt_all_enable()   stm32w_enable_irq()

#define _driver_ttc_interrupt_all_disable()  stm32w_disable_irq()

#define _driver_AMOUNT_EXTERNAL_INTERRUPTS 4

//} Defines







//{ Includes ***************************************************************

#include "../ttc_basic.h"
#include "../ttc_interrupt_types.h"
#include "../register/register_stm32w1xx_types.h"

//} Includes





//{ Structures/ Enums required by ttc_interrupt_types.h ***********************


//Defined by template
/*typedef struct { // register description (adapt according to stm32w registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} INTERRUPT_Register_t;

typedef struct {  // stm32w specific configuration data of single INTERRUPT device
  INTERRUPT_Register_t* BaseRegister;       // base address of INTERRUPT registers
               u8_t  PhysicalIndex;      // physical device index (1=first INTERRUPT device, ...)
} __attribute__((__packed__)) stm32w_interrupt_config_t;

// ttc_interrupt_arch_t is required by ttc_interrupt_types.h
typedef stm32w_interrupt_config_t ttc_interrupt_arch_t;*/

//} Structures/ Enums


#endif //STM32W_INTERRUPT_TYPES_H
