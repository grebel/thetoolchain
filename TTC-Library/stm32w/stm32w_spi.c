/*{ stm32_spi::.c ************************************************

                      The ToolChain
                      
   Universal Synchronous Asynchronous Receiver and Transmitters (stm32_SPIs)
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Gregor Rebel 2012
   
   Note: See ttc_spi.h for description of architecture independent SPI implementation.

}*/

#include "stm32w_spi.h"

t_register_stm32f1xx_spi* stm32_SPIs[TTC_AMOUNT_SPIS];
t_u8 ss_Initialized=0;

//{ Function definitions *************************************************

void stm32w_SPI_ResetAll() {
    memset(stm32_SPIs, 0, sizeof(SPI_TypeDef*) * TTC_AMOUNT_SPIS);
    ss_Initialized=1;
}
e_ttc_spi_errorcode stm32w_getSPI_Defaults(t_u8 SPI_Index, t_ttc_spi_generic* SPI_Generic) {
    if (!ss_Initialized) stm32w_SPI_ResetAll();
#ifndef TTC_NO_ARGUMENT_CHECKS //{
    // assumption: *SPI_Generic has been zeroed

    Assert(SPI_Index > 0,    ttc_assert_origin_auto);
    Assert(SPI_Generic != NULL, ttc_assert_origin_auto);
    if (SPI_Index > TTC_AMOUNT_SPIS) return tue_DeviceNotFound;
#endif //}

    SPI_Generic->Flags.All                        = 0;
    SPI_Generic->Flags.Bits.Master                = 1;
    SPI_Generic->Flags.Bits.ClockIdleHigh         = 1;
    SPI_Generic->Flags.Bits.ClockPhase2ndEdge     = 1;
    SPI_Generic->Flags.Bits.Simplex               = 0;
    SPI_Generic->Flags.Bits.Bidirectional         = 1;
    SPI_Generic->Flags.Bits.Receive               = 1;
    SPI_Generic->Flags.Bits.Transmit              = 1;
    SPI_Generic->Flags.Bits.WordSize16            = 0;
    SPI_Generic->Flags.Bits.SoftNSS               = 1;
    SPI_Generic->Flags.Bits.CRC8                  = 0;
    SPI_Generic->Flags.Bits.CRC16                 = 0;
    SPI_Generic->Flags.Bits.TxDMA                 = 0;
    SPI_Generic->Flags.Bits.RxDMA                 = 0;
    SPI_Generic->Flags.Bits.FirstBitMSB           = 1;
    SPI_Generic->Flags.Bits.Irq_Error             = 0;
    SPI_Generic->Flags.Bits.Irq_Received          = 0;
    SPI_Generic->Flags.Bits.Irq_Transmitted       = 0;

    SPI_Generic->Init.CRC_Polynom = 0x7;
    SPI_Generic->Layout      = 0;
    
    //    RCC_ClocksTypeDef RCC_Clocks;
    //    RCC_GetClocksFreq(&RCC_Clocks);
    
    SPI_Generic->Init.BaudRatePrescaler   = 16;        // medium speed
    //  SPI_Generic->Init.BaudRate  = RCC_Clocks.PCLK2_Frequency / SPI_Generic->Init.BaudRatePrescaler;

    return tue_OK;
}
e_ttc_spi_errorcode stm32w_getSPI_Features(t_u8 SPI_Index, t_ttc_spi_generic* SPI_Generic) {
    if (!ss_Initialized) stm32w_SPI_ResetAll();
#ifndef TTC_NO_ARGUMENT_CHECKS //{
    // assumption: *SPI_Generic has been zeroed

    Assert(SPI_Index > 0,    ttc_assert_origin_auto);
    Assert(SPI_Generic != NULL, ttc_assert_origin_auto);
    if (SPI_Index > TTC_AMOUNT_SPIS) return tue_DeviceNotFound;
#endif //}

    SPI_Generic->Flags.All                        = 0;
    SPI_Generic->Flags.Bits.Master                = 1;
    SPI_Generic->Flags.Bits.ClockIdleHigh         = 1;
    SPI_Generic->Flags.Bits.ClockPhase2ndEdge     = 1;
    SPI_Generic->Flags.Bits.Simplex               = 1;
    SPI_Generic->Flags.Bits.Bidirectional         = 1;
    SPI_Generic->Flags.Bits.Receive               = 1;
    SPI_Generic->Flags.Bits.Transmit              = 1;
    SPI_Generic->Flags.Bits.WordSize16            = 1;
    SPI_Generic->Flags.Bits.SoftNSS               = 1;
    SPI_Generic->Flags.Bits.CRC8                  = 1;
    SPI_Generic->Flags.Bits.CRC16                 = 1;
    SPI_Generic->Flags.Bits.FirstBitMSB           = 1;
    SPI_Generic->Flags.Bits.TxDMA                 = 0; // ToDo
    SPI_Generic->Flags.Bits.RxDMA                 = 0; // ToDo
    SPI_Generic->Flags.Bits.Irq_Error             = 0; // ToDo
    SPI_Generic->Flags.Bits.Irq_Received          = 0; // ToDo
    SPI_Generic->Flags.Bits.Irq_Transmitted       = 0; // ToDo

    SPI_Generic->Init.CRC_Polynom = 0xffff;
    SPI_Generic->Layout      = 0;
    
    //   RCC_ClocksTypeDef RCC_Clocks;
    //  RCC_GetClocksFreq(&RCC_Clocks);
    
    SPI_Generic->Init.BaudRatePrescaler   = 255;                             // max divider
    //  SPI_Generic->Init.BaudRate  = RCC_Clocks.PCLK2_Frequency / 2;  // max frequency

    //    SPI_TypeDef* SPI_Base = NULL;
    //    switch (SPI_Index) {           // find corresponding SPI as defined by makefile.100_board_*
    //#ifdef TTC_SPI1
    //      case 1: SPI_Base = TTC_SPI1; break;
    //#endif
    //#ifdef TTC_SPI2
    //      case 2: SPI_Base = TTC_SPI2; break;
    //#endif
    //#ifdef TTC_SPI3
    //      case 3: SPI_Base = TTC_SPI3; break;
    //#endif
    //      default: Assert(0, ttc_assert_origin_auto); break; // No TTC_SPIn defined! Check your makefile.100_board_* file!
    //    }
    //    switch ( (t_u32) SPI_Base ) {  // find amount of available remapping-layouts (-> RM0008 p. 176)
    //    case (t_u32) SPI1:
    //        SPI_Generic->Layout=1;     // SPI1 provides 1 alternate pin layout
    //        break;
    //    case (t_u32) SPI2:
    //        SPI_Generic->Layout=0;     // SPI2 provides 0 alternate pin layouts
    //    case (t_u32) SPI3:
    //#ifdef STM32F10X_CL
    //            SPI_Generic->Layout=1; // SPI3 provides 1 alternate pin layout in Connection Line Devices
    //#else
    //            SPI_Generic->Layout=0; // SPI3 provides 0 alternate pin layout
    //#endif
    //        break;
    //      default: Assert(0, ttc_assert_origin_auto);
    //    }
    
    //    switch (SPI_Index) {           // disable features requiring unconfigured pins
    //      case 1: {
    //          #ifndef TTC_SPI1_MOSI
    //            SPI_Generic->Flags.Bits.Transmit = 0;
    //          #endif
    //          #ifndef TTC_SPI1_MISO
    //            SPI_Generic->Flags.Bits.Receive  = 0;
    //          #endif
    //          #ifndef TTC_SPI1_NSS
    //            SPI_Generic->Flags.Bits.SoftNSS  = 1;
    //          #endif
    //          #ifndef TTC_SPI1_SCK
    //            SPI_Generic->Flags.Bits.Transmit = 0;
    //            SPI_Generic->Flags.Bits.Receive  = 0;
    //          #endif

    //          break;
    //      }
    //      case 2: {
    //          #ifndef TTC_SPI2_MOSI
    //            SPI_Generic->Flags.Bits.Transmit = 0;
    //          #endif
    //          #ifndef TTC_SPI2_MISO
    //            SPI_Generic->Flags.Bits.Receive  = 0;
    //          #endif
    //          #ifndef TTC_SPI2_NSS
    //            SPI_Generic->Flags.Bits.SoftNSS  = 1;
    //          #endif
    //          #ifndef TTC_SPI2_SCK
    //            SPI_Generic->Flags.Bits.Transmit = 0;
    //            SPI_Generic->Flags.Bits.Receive  = 0;
    //          #endif

    //          break;
    //      }
    //      case 3: {
    //          #ifndef TTC_SPI3_MOSI
    //            SPI_Generic->Flags.Bits.Transmit = 0;
    //          #endif
    //          #ifndef TTC_SPI3_MISO
    //            SPI_Generic->Flags.Bits.Receive  = 0;
    //          #endif
    //          #ifndef TTC_SPI3_NSS
    //            SPI_Generic->Flags.Bits.SoftNSS  = 1;
    //          #endif
    //          #ifndef TTC_SPI3_SCK
    //            SPI_Generic->Flags.Bits.Transmit = 0;
    //            SPI_Generic->Flags.Bits.Receive  = 0;
    //          #endif

    //          break;
    //      }
    //      default: Assert(0, ttc_assert_origin_auto); return tue_DeviceNotFound;; // SPI misconfigured! Check your makefile.100_board_* file!
    //    }
    
    return tue_OK;
}
e_ttc_spi_errorcode stm32w_initSPI(t_u8 SPI_Index, t_ttc_spi_generic* SPI_Generic, t_stm32_spi_architecture* SPI_Arch) {
    if (!ss_Initialized) stm32w_SPI_ResetAll();
#ifndef TTC_NO_ARGUMENT_CHECKS //{
    // assumption: *SPI_Arch has been zeroed

    //  Assert(SPI_Generic != NULL, ttc_assert_origin_auto);
#endif //}


    //SCx_MODE_t * sc1_mode = (SCx_MODE_t *) SC1_MODE_ADDR;
    //SCx_SPICFG_t * thisSCx_SPICFG  =  SC1_SPICFG_ADDR
    //sc1_mode->ScMode = 2;


    //if(STM32W_SPIx==STM32W_SPI1){
    if(SPI_Index == SC1){
        SC1_MODE_REG = SC1_MODE_DISABLED;

        SC1_SPICFG_REG |= SC_SPIMST;//(1<<SC_SPIMST_BIT); //set mode master
        SC1_TWICTRL2_REG |=SC_TWIACK;//(1<<SC_TWIACK_BIT); //dont know what this is for
        SC1_RATELIN_REG = 1; //set speed
        SC1_RATEEXP_REG =1; //set speed

        //SPI 1
        halGpioConfig (PORTB_PIN(1), GPIOCFG_OUT_ALT); //MOSI
        halGpioConfig (PORTB_PIN(2), GPIOCFG_IN); //MISO
        halGpioConfig (PORTB_PIN(3), GPIOCFG_OUT_ALT_SPECIAL_SCLK); //SCLK  //GPIOCFG not defined in regs.h

        SC1_MODE_REG = SC1_MODE_SPI;


        //        INT_SC1CFG |= (INT_SCRXVAL   |
        //                       INT_SCRXOVF   |
        //                       INT_SC1FRMERR |
        //                       INT_SC1PARERR);
        //        INT_SC1FLAG = 0xFFFF; // Clear any stale interrupts
        //        INT_CFGSET = INT_SC1;

        //        To enable CPU interrupts, set the desired interrupt bits in the second level INT_SCxCFG
        //        register, and enable the top level SCx interrupt in the NVIC by writing the INT_SCx bit in the
        //        INT_CFGSET register.

        //        SC1_INTMODE |= SC_SPIRXVAL

        //INT_SC1CFG |= (INT_SCRXVAL);


        //}else if(STM32W_SPIx==STM32W_SPI2){
    }else if(SPI_Index == SC2 ){
        SC2_MODE_REG = SC2_MODE_DISABLED;

        SC2_SPICFG_REG |= SC_SPIMST;//(1<<SC_SPIMST_BIT); //set mode master
        SC2_TWICTRL2_REG |=SC_TWIACK;//(1<<SC_TWIACK_BIT); //dont know what this is for
        SC2_RATELIN_REG = 1; //set speed
        SC2_RATEEXP_REG =1; //set speed


        //SPI 2
        halGpioConfig (PORTA_PIN(0), GPIOCFG_OUT_ALT); //MOSI
        halGpioConfig (PORTA_PIN(1), GPIOCFG_IN); //MISO
        halGpioConfig (PORTA_PIN(2), GPIOCFG_OUT_ALT_SPECIAL_SCLK); //SCLK  //GPIOCFG not defined in regs.h

        SC2_MODE_REG = SC2_MODE_SPI;
    }else {

        Assert(0, ttc_assert_origin_auto);
    }
    
    return tue_OK;
}
e_ttc_spi_errorcode stm32w_spi_sendBytes(t_u8 SPI_Index, const char* Buffer, t_u16 Amount) {
    t_register_stm32f1xx_spi* MySPI ;//= stm32_SPIs[SPI_Index];
    //////////////////////////////////////
    //bypass some selection stuff for spi
    MySPI = SC1;
    //////////////////////////////////////
    // Assert(MySPI != NULL, ttc_assert_origin_auto);
    //assert can not be used because t_register_stm32f1xx_spi defined as enum with SC1 = 0

    while((Amount-- )>0) {
        // _stm32w_spi_sendWord(MySPI, (t_u16) *Buffer);
        _stm32w_spi_send_uint8(SPI_Index, (t_u8) *Buffer++);


    }
    return tue_OK;
}
e_ttc_spi_errorcode stm32w_spi_sendASCII(t_u8 SPI_Index, const char* Buffer, t_u16 MaxLength) {
    t_register_stm32f1xx_spi* MySPI = stm32_SPIs[SPI_Index];
    // Assert(MySPI != NULL, ttc_assert_origin_auto);
    //assert can not be used because t_register_stm32f1xx_spi defined as enum with SC1 = 0
    //////////////////////////////////////
    //bypass some selection stuff for spi
    MySPI = SC1;
    //////////////////////////////////////

    char C=0;
    while ( (MaxLength-- > 0) && ((C=*Buffer++) != 0)  ) {
        _stm32w_spi_send_uint8(SPI_Index, (t_u8) *Buffer++);
    }
    return tue_OK;
}
e_ttc_spi_errorcode stm32w_spi_sendWord(t_u8 SPI_Index, const t_u16 Word) {
    t_register_stm32f1xx_spi* MySPI = stm32_SPIs[SPI_Index];
    Assert(MySPI != NULL, ttc_assert_origin_auto);

    //can not send ut_int16 at once, have to split into two actions
    //_stm32w_spi_sendWord(MySPI, Word);
    _stm32w_spi_send_uint8(SPI_Index, (t_u8) Word&0xFF);
    _stm32w_spi_send_uint8(SPI_Index, (t_u8) (Word>>8)&0xFF);
    return tue_OK;
}
e_ttc_spi_errorcode stm32w_spi_readByte(t_u8 SPI_Index, char* Byte, t_u32 TimeOut) {
    t_register_stm32f1xx_spi* MySPI = stm32_SPIs[SPI_Index];
    // Assert(MySPI != NULL, ttc_assert_origin_auto);
    //assert can not be used because t_register_stm32f1xx_spi defined as enum with SC1 = 0
    //////////////////////////////////////
    //bypass some selection stuff for spi
    MySPI = SC1;
    //////////////////////////////////////
    if ( _stm32w_spi_waitForRXNE(SPI_Index, TimeOut) ) return tue_TimeOut;

    // *Byte=(char) 0xff & SPI_I2S_ReceiveData( (SPI_TypeDef*) MySPI );
    // MySPI->SPI_SR.RXNE=0;

    return _stm32w_spi_receive_uint8(SPI_Index, (t_u8* )Byte );

}
e_ttc_spi_errorcode stm32w_spi_readWord(t_u8 SPI_Index, t_u16* Word, t_u32 TimeOut) {
    t_register_stm32f1xx_spi* MySPI = stm32_SPIs[SPI_Index];
    //Assert(MySPI != NULL, ttc_assert_origin_auto);
    //assert can not be used because t_register_stm32f1xx_spi defined as enum with SC1 = 0
    //////////////////////////////////////
    //bypass some selection stuff for spi
    MySPI = SC1;
    //////////////////////////////////////

    if ( _stm32w_spi_waitForRXNE(SPI_Index, TimeOut) ) return tue_TimeOut;


    // *Word=SPI_I2S_ReceiveData( (SPI_TypeDef*) MySPI  );
    // MySPI->SPI_SR.RXNE=0;
    return _stm32w_spi_receive_uint8(MySPI, (t_u8* )Word );

    return tue_OK;
}

//}FunctionDefinitions
//{ private functions (ideally) -------------------------------------------------

void _stm32w_spi_sendWord(t_register_stm32f1xx_spi* MySPI, const t_u16 Word) {
    //Assert(MySPI != NULL, ttc_assert_origin_auto);
    //assert can not be used because t_register_stm32f1xx_spi defined as enum with SC1 = 0
    //    if(MySPI== SC1){
    //        while ((SC1_SPISTAT&SC_SPITXFREE)!=SC_SPITXFREE) {}
    //        SC1_DATA = Word;
    //    }else if(MySPI == SC2){
    //        while ((SC2_SPISTAT&SC_SPITXFREE)!=SC_SPITXFREE) {}
    //        SC2_DATA = Word;
    //    }else {
    //        Assert(0, ttc_assert_origin_auto);
    //    }
}
void _stm32w_spi_send_uint8(t_register_stm32f1xx_spi* MySPI, const t_u8 data) {
    //Assert(MySPI != NULL, ttc_assert_origin_auto);
    //assert can not be used because t_register_stm32f1xx_spi defined as enum with SC1 = 0

    if(MySPI== SC1){
        while ((SC1_SPISTAT&SC_SPITXFREE)!=SC_SPITXFREE) {}
        SC1_DATA = (t_u8)data;
    }else if(MySPI == SC2){
        while ((SC2_SPISTAT&SC_SPITXFREE)!=SC_SPITXFREE) {}
        SC2_DATA = (t_u8)data;
    }else {
        Assert(0, ttc_assert_origin_auto);
    }
}

e_ttc_spi_errorcode _stm32w_spi_receive_uint8(t_register_stm32f1xx_spi* MySPI, t_u8 * data) {
    //Assert(MySPI != NULL, ttc_assert_origin_auto);
    //assert can not be used because t_register_stm32f1xx_spi defined as enum with SC1 = 0

    if(MySPI== SC1){
        while ((SC1_SPISTAT&SC_SPIRXVAL)!=SC_SPIRXVAL) {}
        *data = SC1_DATA ;
    }else if(MySPI == SC2){
        while ((SC2_SPISTAT&SC_SPIRXVAL)!=SC_SPIRXVAL) {}
       *data = SC2_DATA ;
    }else {
        Assert(0, ttc_assert_origin_auto);
    }
    return tue_OK;
}
e_ttc_spi_errorcode _stm32w_spi_waitForRXNE(t_register_stm32f1xx_spi* MySPI, t_u32 TimeOut) {

    //    Characters received are stored in the receive FIFO. Receiving characters sets the
    //    SC_SPIRXVAL bit in the SCx_SPISTAT register, indicating that characters can be read from
    //    the receive FIFO
    if(MySPI== SC1){
        while ((SC1_SPISTAT&SC_SPIRXVAL)!=SC_SPIRXVAL) {

            TimeOut--;
            if (TimeOut == 0)  return tue_TimeOut;
            //uSleep(10);
            for(int i = 0; i<10000;i++);
        }
    }else if(MySPI == SC2){
        while ((SC2_SPISTAT&SC_SPIRXVAL)!=SC_SPIRXVAL) {

            TimeOut--;
            if (TimeOut == 0)  return tue_TimeOut;
           // uSleep(10);
            for(int i = 0; i<10000;i++);
        }
    }

    return tue_OK;
}

//} private functions
