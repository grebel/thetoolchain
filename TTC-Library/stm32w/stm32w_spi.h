#ifndef STM32_SPI_H
#define STM32_SPI_H

/*{ stm32_spi.h **********************************************************
 
                      The ToolChain
                      
   Universal Synchronous Asynchronous Receiver and Transmitters (SPIs)
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32W types.
   
   written by Gregor Rebel and Sascha Poggemann 2012

 
}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

#include "ttc_spi_types.h"
//#include "ttc_multitask.h"

#include "stm32w_t_registerypedefs.h"

 // Basic set of helper functions
#include "DEPRECATED_stm32w_gpio.h"
//#include "../register/register_stm32f1xx.h"

#include "gnu.h"
#include "regs.h"
#include "STM32W_regs_patch.h"
#include "stm32w108_type.h"
#include "STM32W_types_patch.h"
#include "micro-common.h"



#include <stdlib.h>

//#define USE_STDPERIPH_DRIVER
//#include "stm32f10x.h"
//#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
//#include "stm32f10x_conf.h"
//#include "stm32f10x_spi.h"
//#include "misc.h"

/*{ optional
// ADC
#include "stm32f10x_adc.h"
#include "stm32f10x_dma.h"

// Timers
#include "stm32f10x_tim.h"

#include "stm32f10x_it.h"
#include "system_stm32f10x.h"

#include "bits.h"
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
}*/

//} Includes
//{ Structures/ Enums ****************************************************
typedef enum {SC1, SC2}t_register_stm32f1xx_spi;
typedef struct {void * SPI1;
                void * SPI2;
               }SPI_TypeDef;
typedef struct {  // architecture specific configuration data of single SPI 
  SPI_TypeDef* Base;     // base address of SPI registers
  e_ttc_gpio_pin PortMISO;   // port pin for MISO
  e_ttc_gpio_pin PortMOSI;   // port pin for MOSI
  e_ttc_gpio_pin PortSCK;    // port pin for SCK
  e_ttc_gpio_pin PortNSS;    // port pin for NSS
} __attribute__((__packed__)) t_stm32_spi_architecture;

//} Structures/ Enums
//{ Global Variables *****************************************************



//} Global Variables
//{ Function prototypes **************************************************

/* resets library. Automatically called.
 */
void stm32_SPI_ResetAll();

/* fills out given SPI_ with default values for indexed SPI
 * @param SPI_Index     device index of SPI to init (1..stm32_getMax_SPI_Index())
 * @param SPI_Generic   pointer to struct t_ttc_spi_generic
 * @return  == 0:         *SPI_Generic has been initialized successfully; != 0: error-code
 */
e_ttc_spi_errorcode stm32w_getSPI_Defaults(t_u8 SPI_Index, t_ttc_spi_generic* SPI_Generic);

/* fills out given SPI_Generic with maximum valid values for indexed SPI
 * @param SPI_Index     device index of SPI to init (1..stm32_getMax_SPI_Index())
 * @param SPI_Generic   pointer to struct t_ttc_spi_generic
 * @return  == 0:         *SPI_Generic has been initialized successfully; != 0: error-code
 */
e_ttc_spi_errorcode stm32w_getSPI_Features(t_u8 SPI_Index, t_ttc_spi_generic* SPI_Generic);

/* initializes single SPI
 * @param SPI_Index     device index of SPI to init (1..stm32_getMax_SPI_Index())
 * @param SPI_Generic   filled out struct t_ttc_spi_generic
 * @return  == 0:         SPI has been initialized successfully; != 0: error-code
 */
e_ttc_spi_errorcode stm32w_initSPI(t_u8 SPI_Index, t_ttc_spi_generic* SPI_Generic, t_stm32_spi_architecture* SPI_Arch);

/* Send out given amount of raw bytes from buffer.
 * Will not interpret bytes in buffer.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * 
 * @param SPI_Index    device index of SPI to init (0..stm32_getMax_SPI_Index()-1)
 * @param Buffer         memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[]
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_spi_errorcode stm32w_spi_sendBytes(t_u8 SPI_Index, const char* Buffer, t_u16 Amount);

/* Send out given bytes from buffer until first zero byte or MaxLength reached.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * 
 * @param SPI_Index    device index of SPI to init (0..stm32_getMax_SPI_Index()-1)
 * @param Buffer         memory location from which to read data
 * @param MaxLength      no more than this amount of bytes will be send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_spi_errorcode stm32w_spi_sendASCII(t_u8 SPI_Index, const char* Buffer, t_u16 MaxLength);

/* Send out given data word (8 or 9 bits)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param SPI_Index    device index of SPI to init (0..ttc_getMax_SPI_Index()-1)
 * @param Byte           8 or 9 bit value to send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
e_ttc_spi_errorcode stm32w_spi_sendWord(t_u8 SPI_Index, const t_u16 Word);

/* Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param SPI_Index    device index of SPI to init (0..ttc_getMax_SPI_Index()-1)
 * @param Word           pointer to 16 bit buffer where to store Word
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_spi_errorcode stm32w_spi_readWord(t_u8 SPI_Index, t_u16* Word, t_u32 TimeOut);

/* Reads single data byte from input buffer.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param SPI_Index    device index of SPI to init (0..ttc_getMax_SPI_Index()-1)
 * @param Word           pointer to 16 bit buffer where to store Word
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
e_ttc_spi_errorcode stm32w_spi_readByte(t_u8 SPI_Index, char* Byte, t_u32 TimeOut);

/* Sends out given byte.
 * Note: This low-level function should not be called from outside!
 */
void _stm32w_spi_sendWord(t_register_stm32f1xx_spi* MySPI, const t_u16 Word);
void _stm32w_spi_send_uint8(t_register_stm32f1xx_spi* MySPI, const t_u8 Word);

/* Sends out given byte.
 * Note: This low-level function should not be called from outside!
 */
e_ttc_spi_errorcode _stm32w_spi_receive_uint8(t_register_stm32f1xx_spi* MySPI, t_u8 * data);

/* Wait until word has been received or timeout occured.
 * Note: This low-level function should not be called from outside!
 */
e_ttc_spi_errorcode _stm32w_spi_waitForRXNE(t_register_stm32f1xx_spi* MySPI, t_u32 TimeOut);

//} Function prototypes

#endif //STM32_SPI_H
