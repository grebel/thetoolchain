/** { DEPRECATED_stm32w_gpio.c ************************************************
 
                           The ToolChain
                      
   High-Level interface for GPIO device.
     
   Note: See ttc_gpio.h for description of stm32w independent GPIO implementation.
  
   Authors: Greg Knoll 2013
}*/

#include "DEPRECATED_stm32w_gpio.h"
#include "DEPRECATED_stm32w_registers.h"

//{ Function definitions ***************************************************

//------------regular init/get/set/clear-----------//
void stm32w_gpio_init(stm32w_GPIO_e GPIOx, u8_t Pin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed){
    //get 4-bit designator for Type (mode) - see gpio_port_ioMode_e in DEPRECATED_stm32w_registers.h
    u32_t PortConfig = _DEPRECATED_stm32w_gpio.compile_port_config(Type, Speed);

// ((stm32w_registers_GPIOx_t *) GPIOx)->CFGL.MODE1 = PortConfig;

    if (Pin < 4){ //write designator into GPIO_PxCFGL if Pin 0-3
        u8_t Shift = Pin * 4; //offset from base address
        u32_t* CFGL = (u32_t*) &( ((stm32w_registers_GPIOx_t *) GPIOx)->CFGL ); //assign type-casted address to pointer (u32_t pointer)
        *CFGL = *CFGL & (~(0x0f<<Shift));                                       //then use pointer in bool expression
        *CFGL |= (PortConfig <<Shift);                                          //shift portconfig, use binary OR, then put back in register address
 //       ((stm32w_registers_GPIOx_t *) GPIOx)->CFGL = CFGL;  //NO LONGER NEEDED
    }
    else { //or GPIO_PxCFGH if Pin 4-7
        u8_t Shift = (Pin-4) * 4; //offset from base address for high 4 pins
        u32_t* CFGH = (u32_t*) &(((stm32w_registers_GPIOx_t *) GPIOx)->CFGH);
        *CFGH = *CFGH & (~(0x0f<<Shift));
        *CFGH |= (PortConfig <<Shift);
        //((stm32w_registers_GPIOx_t *) GPIOx)->CFGH = CFGH;
    }

} //END stm32w_gpio_init()

bool stm32w_gpio_get(stm32w_GPIO_e GPIOx, u8_t Pin){
    u32_t* Data = (u32_t*) &(((stm32w_registers_GPIOx_t *) GPIOx)->IDR);
    if ( *Data & (1 << Pin) ) return TRUE;
    return FALSE;
} //END stm32w_gpio_get

void DEPRECATED_stm32w_gpio.clr(stm32w_GPIO_e GPIOx, u8_t Pin){
    *((u32_t *) &((stm32w_registers_GPIOx_t *) GPIOx)->CLR) = 1 << Pin;
}

void stm32w_gpio_set(stm32w_GPIO_e GPIOx, u8_t Pin){
     *((u32_t *) &((stm32w_registers_GPIOx_t *) GPIOx)->SETR) = 1 << Pin;
}
//------------------------------------------------//


//-----------with Variables/Ports-----------------//

void stm32w_gpio_variable(stm32w_Port_t* Port, stm32w_GPIO_e GPIOx, u8_t Pin){
    Assert(Port, ec_InvalidArgument);
    Port->GPIOx = GPIOx;
    Port->Pin   = Pin;
    Port->BRR   = cm3_calc_peripheral_BitBandAddress( (void*) &(((stm32w_registers_GPIOx_t *) GPIOx)->CLR),  Pin);
    Port->BSRR  = cm3_calc_peripheral_BitBandAddress( (void*) &(((stm32w_registers_GPIOx_t *) GPIOx)->SETR), Pin);
    Port->IDR   = cm3_calc_peripheral_BitBandAddress( (void*) &(((stm32w_registers_GPIOx_t *) GPIOx)->IDR),  Pin);
}
void stm32w_gpio_init_variable(ttc_Port_t* Port, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed){
     stm32w_gpio_init(Port->GPIOx, Port->Pin, Type, Speed);
}
void stm32w_gpio_init2_variable(ttc_Port_t* Port, ttc_gpio_bank_t GPIOx, u8_t Pin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed) {

    stm32w_gpio_variable(Port, GPIOx, Pin);
    stm32w_gpio_init_variable(Port, Type, Speed);
}
bool stm32w_gpio_get_variable(stm32w_Port_t* Port){
    return *(Port->IDR); // read from bitband address
}
void stm32w_gpio_set_variable(stm32w_Port_t* Port){
    *(Port->BSRR) = 1; // write to bitband address
}
void DEPRECATED_stm32w_gpio.clr_variable(stm32w_Port_t* Port){
    *(Port->BRR) = 1; // write to bitband address
}
void stm32w_gpio_from_index8(u8_t PhysicalIndex, GPIO_TypeDef** GPIOx, u8_t* Pin){
    Assert(GPIOx, ec_InvalidArgument);
    Assert(Pin, ec_InvalidArgument);

    switch (PhysicalIndex & 0xf0) {

        #ifdef GPIOA
            case 0x10: *GPIOx = GPIOA; break;
        #endif
        #ifdef GPIOB
            case 0x20: *GPIOx = GPIOB; break;
        #endif
        #ifdef GPIOC
            case 0x30: *GPIOx = GPIOC; break;
        #endif
        #ifdef GPIOD
            case 0x40: *GPIOx = GPIOD; break;
        #endif
        #ifdef GPIOE
            case 0x50: *GPIOx = GPIOE; break;
        #endif
        #ifdef GPIOF
            case 0x60: *GPIOx = GPIOF; break;
        #endif
        #ifdef GPIOG
            case 0x70: *GPIOx = GPIOG; break;
        #endif

    default: *GPIOx = NULL;
    }
    *Pin = PhysicalIndex & 0x0f;
}
u8_t DEPRECATED_stm32w_gpio.create_index8(ttc_gpio_bank_t Bank, u8_t Pin) {
    switch (Bank) {

#ifdef GPIOA
    case stm32w_GPIOA: return 0x10 | (0x0f & Pin);
#endif
#ifdef GPIOB
    case stm32w_GPIOB: return 0x20 | (0x0f & Pin);
#endif
#ifdef GPIOC
    case stm32w_GPIOC: return 0x30 | (0x0f & Pin);
#endif
#ifdef GPIOD
    case stm32w_GPIOD: return 0x40 | (0x0f & Pin);
#endif
#ifdef GPIOE
    case stm32w_GPIOE: return 0x50 | (0x0f & Pin);
#endif
#ifdef GPIOF
    case stm32w_GPIOF: return 0x60 | (0x0f & Pin);
#endif
#ifdef GPIOG
    case stm32w_GPIOG: return 0x70 | (0x0f & Pin);
#endif

    default: break;
    }
    return 0;
}

//------------------------------------------------//

//} Function definitions



//{ private functions (ideally) ********************************************
u8_t _DEPRECATED_stm32w_gpio.compile_port_config(ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed){
    u8_t PortConfig = 0;

    switch (Type) {
    case tgm_analog_in:
        PortConfig = gpmode_analog;
        break;
    case tgm_input_floating:
        PortConfig = gpmode_input_floating;
        break;
    case tgm_input_pull_down:
        PortConfig = gpmode_input_pull_upORdown;
        break;
    case tgm_input_pull_up:
        PortConfig = gpmode_input_pull_upORdown;
        break;
    case tgm_output_open_drain:
        PortConfig = gpmode_output_open_drain;
        break;
    case tgm_output_push_pull:
        PortConfig = gpmode_output_push_pull;
        break;
    case tgm_alternate_function_push_pull:
        PortConfig = gpmode_output_alternate_push_pull;
        break;
    case tgm_alternate_function_open_drain:
        PortConfig = gpmode_output_alternate_open_drain;
        break;
    case tgm_alternate_function_SPECIAL_SCLK:
        PortConfig = gpmode_output_alternate_pp_SPI_SCLK;
        break;
    default:
        Assert_Halt_EC(ec_InvalidArgument);
    }
    return PortConfig;
}
//} private functions
