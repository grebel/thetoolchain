#ifndef STM32W_USART_TYPES_H
#define STM32W_USART_TYPES_H

/** { stm32w_usart.h ************************************************
 
                           The ToolChain
                      
   High-Level interface for USART device.

   Structures, Enums and Defines being required by ttc_usart_types.h

   Note: See ttc_usart.h for description of stm32w independent USART implementation.
  
   Authors: Greg Knoll 2013

 
}*/

//{ Defines/ TypeDefs ******************************************************

// maximum amount of available USART/ UART units in current architecture
#define TTC_USART_MAX_AMOUNT 1

//} Defines


//{ Includes ***************************************************************

#include "../ttc_basic.h"
#include "DEPRECATED_stm32w_registers.h"
#include "DEPRECATED_stm32w_gpio.h"

//} Includes


//{ Structures/ Enums required by ttc_usart_types.h ***********************

typedef struct {  // stm32w specific configuration data of single USART device
    stm32w_registers_SCx_t * Base;      // base address of serial controller 1 (SC2 doesn't have USART)
                                        // contains all registers necessary for operation
    t_stm32w_port PortTxD;              // port pin for TxD
    t_stm32w_port PortRxD;              // port pin for RxD
    t_stm32w_port PortRTS;              // port pin for RTS
    t_stm32w_port PortCTS;              // port pin for CTS

} __attribute__((__packed__)) t_stm32w_usart_architecture;


// t_ttc_usart_architecture is required by ttc_usart_types.h
typedef t_stm32w_usart_architecture t_ttc_usart_architecture;

//} Structures/ Enums


#endif //STM32W_USART_TYPES_H
