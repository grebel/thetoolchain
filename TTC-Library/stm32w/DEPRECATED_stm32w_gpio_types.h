#ifndef STM32W_GPIO_TYPES_H
#define STM32W_GPIO_TYPES_H

/** { DEPRECATED_stm32w_gpio.h ************************************************
 
                           The ToolChain
                      
   High-Level interface for GPIO device.

   Structures, Enums and Defines being required by ttc_gpio_types.h

   Note: See ttc_gpio.h for description of stm32w independent GPIO implementation.
  
   Authors: Greg Knoll 2013

 
}*/
//{ Defines/ TypeDefs ******************************************************


//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic.h"
#include "DEPRECATED_stm32w_registers.h"

//} Includes
//{ Structures/ Enums required by ttc_gpio_types.h ***********************

typedef enum { //pointers to GPIO Bank addresses
    stm32w_GPIO_None,
    stm32w_GPIOA = (u32_t) GPIOA,
    stm32w_GPIOB = (u32_t) GPIOB,
    stm32w_GPIOC = (u32_t) GPIOC,
    stm32w_GPIO_NotImplemented
} stm32w_GPIO_e;


typedef struct { // stm32w_Port_t - (These are VARIABLES=GPIO bank + pin)
    stm32w_GPIO_e GPIOx;
    u8_t Pin;
    volatile u32_t* BSRR;  // bit-band address of BSRR register of this port bit
    volatile u32_t* BRR;   // bit-band address of BRR register of this port bit
    volatile u32_t* IDR;   // bit-band address of IDR register of this port bit
} __attribute__((__packed__)) stm32w_Port_t;


//} Structures/ Enums


#endif //STM32W_GPIO_TYPES_H
