/*{ register_stm32w1xx_registers.c ************************************************

 Structured pointers to registers of ARM CortexM3 microcontrollers.

 written by Greg Knoll 2013

}*/

#include "DEPRECATED_stm32w_registers.h"

#warning
//{ Global pointers to hardware registers

//GPIOS
register_stm32w1xx_GPIOx_t*                   register_stm32w1xx_GPIOA       = (register_stm32w1xx_GPIOx_t*)         GPIOA;
register_stm32w1xx_GPIOx_t*                   register_stm32w1xx_GPIOB       = (register_stm32w1xx_GPIOx_t*)         GPIOB;
register_stm32w1xx_GPIOx_t*                   register_stm32w1xx_GPIOC       = (register_stm32w1xx_GPIOx_t*)         GPIOC;

register_stm32w1xx_GPIO_debug_wake_irq_t *    register_stm32w1xx_GPIO_DB_WAKE_IRQ = (register_stm32w1xx_GPIO_debug_wake_irq_t *)   (PERIPH_BASE + 0xBC00);
register_stm32w1xx_GPIO_interruptCFG *        register_stm32w1xx_GPIO_INT_CFG     = (register_stm32w1xx_GPIO_interruptCFG *)       (PERIPH_BASE + 0xA860);
register_stm32w1xx_GPIO_interruptFLAG_t *     register_stm32w1xx_GPIO_INT_FLAG    = (register_stm32w1xx_GPIO_interruptFLAG_t *)    (PERIPH_BASE + 0xA814);

//INTERRUPTS
//GKnoll: couldn't find variables with base addresses below. Addresses from STM32W108.pdf - p.184-190
register_stm32w1xx_INT_CFGSET_t *   register_stm32w1xx_INT_ENABLE  = (register_stm32w1xx_INT_CFGSET_t*)    0xE000E100;
register_stm32w1xx_INT_CFGCLR_t *   register_stm32w1xx_INT_DISABLE = (register_stm32w1xx_INT_CFGCLR_t*)    0xE000E180;
register_stm32w1xx_INT_PENDSET_t *  register_stm32w1xx_INT_TRIGGER = (register_stm32w1xx_INT_PENDSET_t*)   0xE000E200;
register_stm32w1xx_INT_PENDCLR_t *  register_stm32w1xx_INT_CLEAR   = (register_stm32w1xx_INT_PENDCLR_t*)   0xE000E280;
register_stm32w1xx_INT_ACTIVE_t *   register_stm32w1xx_INT_ACTIVE  = (register_stm32w1xx_INT_ACTIVE_t*)    0xE000E300;
register_stm32w1xx_INT_MISS_t *     register_stm32w1xx_INT_MISS    = (register_stm32w1xx_INT_MISS_t*)      0x4000A820;
register_stm32w1xx_SCS_AFSR_t *     register_stm32w1xx_BUS_FAULT   = (register_stm32w1xx_SCS_AFSR_t*)      0xE000ED3C;


//SERIAL CONTROLLERS
register_stm32w1xx_SCx_t *  register_stm32w1xx_SC1           = (register_stm32w1xx_SCx_t*) SC1_DMA_ChannelRx;
register_stm32w1xx_SCx_t *  register_stm32w1xx_SC2           = (register_stm32w1xx_SCx_t*) SC2_DMA_ChannelRx;

register_stm32w1xx_INT_SCxFLAG_t *    register_stm32w1xx_SC1_FLAG      = (register_stm32w1xx_INT_SCxFLAG_t*)   SC1_IT;
register_stm32w1xx_INT_SCxFLAG_t *    register_stm32w1xx_SC2_FLAG      = (register_stm32w1xx_INT_SCxFLAG_t*)   SC2_IT;

register_stm32w1xx_INT_SCxCFG_t *     register_stm32w1xx_SC1_CFG       = (register_stm32w1xx_INT_SCxCFG_t*)    0xA848;
register_stm32w1xx_INT_SCxCFG_t *     register_stm32w1xx_SC2_CFG       = (register_stm32w1xx_INT_SCxCFG_t*)    0xA84C;

register_stm32w1xx_SCx_INTMODE_t *    register_stm32w1xx_SC1_INTMODE   = (register_stm32w1xx_SCx_INTMODE_t*)   0xA854;
register_stm32w1xx_SCx_INTMODE_t *    register_stm32w1xx_SC2_INTMODE   = (register_stm32w1xx_SCx_INTMODE_t*)   0xA858;


//}
