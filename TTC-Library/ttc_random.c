/*{ ttc_random.c ***********************************************

 * Written by Gregor Rebel 2010-2018
 *
 * Platform independent Random number generation
 *
}*/
#include "ttc_random.h"

#ifndef TTC_RANDOM_SEED_INITIAL
    #define TTC_RANDOM_SEED_INITIAL 1
#endif

static t_base ttc_random_Seed = TTC_RANDOM_SEED_INITIAL;

BOOL ttc_random_is_seeded() {
    if ( ttc_random_Seed == TTC_RANDOM_SEED_INITIAL )
    { return FALSE; }
    return TRUE;
}
t_u16 _ttc_random_generate16( void ) {
    ttc_random_Seed = ttc_random_Seed * 1103515245 + 12345;
    return ( ttc_random_Seed >> 16 ) & RAND_MAX16;
}
t_u32 _ttc_random_generate32_parker_miller( void ) {

    const t_u32 A = 7 * 7 * 7 * 7 * 7;
    const t_u32 B = 0;
    const t_u32 M = 0x7ffffff;

    ttc_random_Seed = ( A * ttc_random_Seed + B ) & M;
    return ttc_random_Seed;
}
t_u32 _ttc_random_generate32_xorshift32( void ) {
    // Using second variable to avoid some issues with multitasking
    // As long as we don't use a mutex or a critical section, race conditions can occur.
    t_u32 X = ttc_random_Seed;

    X ^= X << 13;
    X ^= X >> 17;
    X ^= X << 5;

    ttc_random_Seed = X;
    return X;
}
void  _ttc_random_seed16( t_u16 Seed ) {
    ttc_random_Seed = Seed;
}
void  _ttc_random_seed32( t_u32 Seed ) {
    ttc_random_Seed = Seed;
}
