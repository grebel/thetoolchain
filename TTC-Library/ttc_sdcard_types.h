/** { ttc_sdcard_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for SDCARD device.
 *  Structures, Enums and Defines being required by both, high- and low-level sdcard.
 *  This file does not provide function declarations.
 *
 *  Note: See ttc_sdcard.h for description of high-level sdcard implementation!
 *
 *  Created from template ttc_device_types.h revision 45 at 20180130 09:42:26 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_SDCARD_TYPES_H
#define TTC_SDCARD_TYPES_H

//{ Includes1 ************************************************************
//
// Includes being required for static configuration.
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_sdcard.h" or "ttc_sdcard.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h"
#include "ttc_storage_types.h"

//} Includes
/** { Static Configuration ***********************************************
 *
 * Most ttc drivers are statically configured by constant definitions in makefiles.
 * A static configuration allows to hide certain code fragments and variables for the
 * compilation process. If used wisely, your code will be lean and fast and do only
 * what is required for the current configuration.
 */

/** {  TTC_SDCARDn has to be defined as constant in one of your makefiles
 *
 * Here we count how much devices have been defined and if all lower index devices have been defined too.
 */
#ifndef TTC_SDCARD_AMOUNT //{ 10
    #ifdef TTC_SDCARD10
        #ifndef TTC_SDCARD9
            #      error TTC_SDCARD10 is defined, but not TTC_SDCARD9 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD8
            #      error TTC_SDCARD10 is defined, but not TTC_SDCARD8 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD7
            #      error TTC_SDCARD10 is defined, but not TTC_SDCARD7 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD6
            #      error TTC_SDCARD10 is defined, but not TTC_SDCARD6 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD5
            #      error TTC_SDCARD10 is defined, but not TTC_SDCARD5 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD4
            #      error TTC_SDCARD10 is defined, but not TTC_SDCARD4 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD3
            #      error TTC_SDCARD10 is defined, but not TTC_SDCARD3 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD2
            #      error TTC_SDCARD10 is defined, but not TTC_SDCARD2 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD1
            #      error TTC_SDCARD10 is defined, but not TTC_SDCARD1 - all lower TTC_SDCARDn must be defined!
        #endif

        #define TTC_SDCARD_AMOUNT 10
    #endif
#endif //}
#ifndef TTC_SDCARD_AMOUNT //{ 9
    #ifdef TTC_SDCARD9
        #ifndef TTC_SDCARD8
            #      error TTC_SDCARD9 is defined, but not TTC_SDCARD8 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD7
            #      error TTC_SDCARD9 is defined, but not TTC_SDCARD7 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD6
            #      error TTC_SDCARD9 is defined, but not TTC_SDCARD6 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD5
            #      error TTC_SDCARD9 is defined, but not TTC_SDCARD5 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD4
            #      error TTC_SDCARD9 is defined, but not TTC_SDCARD4 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD3
            #      error TTC_SDCARD9 is defined, but not TTC_SDCARD3 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD2
            #      error TTC_SDCARD9 is defined, but not TTC_SDCARD2 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD1
            #      error TTC_SDCARD9 is defined, but not TTC_SDCARD1 - all lower TTC_SDCARDn must be defined!
        #endif

        #define TTC_SDCARD_AMOUNT 9
    #endif
#endif //}
#ifndef TTC_SDCARD_AMOUNT //{ 8
    #ifdef TTC_SDCARD8
        #ifndef TTC_SDCARD7
            #      error TTC_SDCARD8 is defined, but not TTC_SDCARD7 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD6
            #      error TTC_SDCARD8 is defined, but not TTC_SDCARD6 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD5
            #      error TTC_SDCARD8 is defined, but not TTC_SDCARD5 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD4
            #      error TTC_SDCARD8 is defined, but not TTC_SDCARD4 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD3
            #      error TTC_SDCARD8 is defined, but not TTC_SDCARD3 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD2
            #      error TTC_SDCARD8 is defined, but not TTC_SDCARD2 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD1
            #      error TTC_SDCARD8 is defined, but not TTC_SDCARD1 - all lower TTC_SDCARDn must be defined!
        #endif

        #define TTC_SDCARD_AMOUNT 8
    #endif
#endif //}
#ifndef TTC_SDCARD_AMOUNT //{ 7
    #ifdef TTC_SDCARD7
        #ifndef TTC_SDCARD6
            #      error TTC_SDCARD7 is defined, but not TTC_SDCARD6 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD5
            #      error TTC_SDCARD7 is defined, but not TTC_SDCARD5 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD4
            #      error TTC_SDCARD7 is defined, but not TTC_SDCARD4 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD3
            #      error TTC_SDCARD7 is defined, but not TTC_SDCARD3 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD2
            #      error TTC_SDCARD7 is defined, but not TTC_SDCARD2 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD1
            #      error TTC_SDCARD7 is defined, but not TTC_SDCARD1 - all lower TTC_SDCARDn must be defined!
        #endif

        #define TTC_SDCARD_AMOUNT 7
    #endif
#endif //}
#ifndef TTC_SDCARD_AMOUNT //{ 6
    #ifdef TTC_SDCARD6
        #ifndef TTC_SDCARD5
            #      error TTC_SDCARD6 is defined, but not TTC_SDCARD5 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD4
            #      error TTC_SDCARD6 is defined, but not TTC_SDCARD4 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD3
            #      error TTC_SDCARD6 is defined, but not TTC_SDCARD3 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD2
            #      error TTC_SDCARD6 is defined, but not TTC_SDCARD2 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD1
            #      error TTC_SDCARD6 is defined, but not TTC_SDCARD1 - all lower TTC_SDCARDn must be defined!
        #endif

        #define TTC_SDCARD_AMOUNT 6
    #endif
#endif //}
#ifndef TTC_SDCARD_AMOUNT //{ 5
    #ifdef TTC_SDCARD5
        #ifndef TTC_SDCARD4
            #      error TTC_SDCARD5 is defined, but not TTC_SDCARD4 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD3
            #      error TTC_SDCARD5 is defined, but not TTC_SDCARD3 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD2
            #      error TTC_SDCARD5 is defined, but not TTC_SDCARD2 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD1
            #      error TTC_SDCARD5 is defined, but not TTC_SDCARD1 - all lower TTC_SDCARDn must be defined!
        #endif

        #define TTC_SDCARD_AMOUNT 5
    #endif
#endif //}
#ifndef TTC_SDCARD_AMOUNT //{ 4
    #ifdef TTC_SDCARD4
        #ifndef TTC_SDCARD3
            #      error TTC_SDCARD4 is defined, but not TTC_SDCARD3 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD2
            #      error TTC_SDCARD4 is defined, but not TTC_SDCARD2 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD1
            #      error TTC_SDCARD4 is defined, but not TTC_SDCARD1 - all lower TTC_SDCARDn must be defined!
        #endif

        #define TTC_SDCARD_AMOUNT 4
    #endif
#endif //}
#ifndef TTC_SDCARD_AMOUNT //{ 3
    #ifdef TTC_SDCARD3

        #ifndef TTC_SDCARD2
            #      error TTC_SDCARD3 is defined, but not TTC_SDCARD2 - all lower TTC_SDCARDn must be defined!
        #endif
        #ifndef TTC_SDCARD1
            #      error TTC_SDCARD3 is defined, but not TTC_SDCARD1 - all lower TTC_SDCARDn must be defined!
        #endif

        #define TTC_SDCARD_AMOUNT 3
    #endif
#endif //}
#ifndef TTC_SDCARD_AMOUNT //{ 2
    #ifdef TTC_SDCARD2

        #ifndef TTC_SDCARD1
            #      error TTC_SDCARD2 is defined, but not TTC_SDCARD1 - all lower TTC_SDCARDn must be defined!
        #endif

        #define TTC_SDCARD_AMOUNT 2
    #endif
#endif //}
#ifndef TTC_SDCARD_AMOUNT //{ 1
    #ifdef TTC_SDCARD1
        #define TTC_SDCARD_AMOUNT 1
    #endif
#endif //}
#ifndef TTC_SDCARD_AMOUNT //{ no devices defined at all
    #  warning Missing definition for TTC_SDCARD1. Using default type. Add valid definition to your makefile to get rid if this message!
    // option 1: no devices available
    #define TTC_SDCARD_AMOUNT 0
    // option 2: define a default device (enable lines below)
    // #define TTC_SDCARD_AMOUNT 1
    // define TTC_SDCARD1 ??? // <- place entry from e_ttc_sdcard_architecture here!!!
#endif

//}
//}
/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_SDCARD 0#         disable default asserts for sdcard driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_SDCARD_EXTRA 1#   enable extra asserts for sdcard driver
 *
 * When debugging code that has been compiled with optimization (gcc option -O) then
 * certain variables may be optimized away. Declaring variables as VOLATILE_SDCARD makes them
 * visible in debug session when asserts are enabled. You have no overhead at all if asserts are disabled.
 + foot_t* VOLATILE_SDCARD VisiblePointer;
 *
 */
#ifndef TTC_ASSERT_SDCARD    // any previous definition set (Makefile)?
    #define TTC_ASSERT_SDCARD 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_SDCARD == 1)  // use Assert()s in SDCARD code (somewhat slower but alot easier to debug)
    #define Assert_SDCARD(Condition, Origin)        Assert(Condition, Origin)
    #define Assert_SDCARD_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_SDCARD_Readable(Address, Origin) Assert_Readable(Address, Origin)
    #define VOLATILE_SDCARD                         volatile
#else  // use no default Assert()s in SDCARD code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_SDCARD(Condition, Origin)
    #define Assert_SDCARD_Writable(Address, Origin)
    #define Assert_SDCARD_Readable(Address, Origin)
    #define VOLATILE_SDCARD
#endif

#ifndef TTC_ASSERT_SDCARD_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_SDCARD_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_SDCARD_EXTRA == 1)  // use Assert()s in SDCARD code (somewhat slower but alot easier to debug)
    #define Assert_SDCARD_EXTRA(Condition, Origin) Assert(Condition, Origin)
    #define Assert_SDCARD_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_SDCARD_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in SDCARD code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_SDCARD_EXTRA(Condition, Origin)
    #define Assert_SDCARD_EXTRA_Writable(Address, Origin)
    #define Assert_SDCARD_EXTRA_Readable(Address, Origin)
#endif
//}
/** { Check static configuration of sdcard devices
 *
 * Each TTC_SDCARDn must be defined as one from e_ttc_sdcard_architecture.
 * Additional defines of type TTC_SDCARDn_* may be added.
 *
 * The constants being checked here must be defined in your makefile.
 * Most constants are defined by EXTENSION_100_board_* or EXTENSION_150_board_extension_* files.
 */

#ifdef TTC_SDCARD1
    #ifndef TTC_SDCARD1_VOLTAGE_MIN
        #define TTC_SDCARD1_VOLTAGE_MIN E_ttc_sdcard_voltage_3V5
        INFO( "Missing definition for TTC_SDCARD1_VOLTAGE_MIN. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD1_VOLTAGE_MIN=<MIN_VOLTAGE> with <MIN_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    #ifndef TTC_SDCARD1_VOLTAGE_MAX
        #define TTC_SDCARD1_VOLTAGE_MAX E_ttc_sdcard_voltage_3V6
        INFO( "Missing definition for TTC_SDCARD1_VOLTAGE_MAX. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD1_VOLTAGE_MAX=<MAX_VOLTAGE> with <MAX_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    // check extra defines for sdcard device #1 here
#endif
#ifdef TTC_SDCARD2
    #ifndef TTC_SDCARD2_VOLTAGE_MIN
        #define TTC_SDCARD2_VOLTAGE_MIN E_ttc_sdcard_voltage_3V5
        INFO( "Missing definition for TTC_SDCARD2_VOLTAGE_MIN. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD2_VOLTAGE_MIN=<MIN_VOLTAGE> with <MIN_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    #ifndef TTC_SDCARD2_VOLTAGE_MAX
        #define TTC_SDCARD2_VOLTAGE_MAX E_ttc_sdcard_voltage_3V6
        INFO( "Missing definition for TTC_SDCARD2_VOLTAGE_MAX. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD2_VOLTAGE_MAX=<MAX_VOLTAGE> with <MAX_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    // check extra defines for sdcard device #2 here

#endif
#ifdef TTC_SDCARD3
    #ifndef TTC_SDCARD3_VOLTAGE_MIN
        #define TTC_SDCARD3_VOLTAGE_MIN E_ttc_sdcard_voltage_3V5
        INFO( "Missing definition for TTC_SDCARD3_VOLTAGE_MIN. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD3_VOLTAGE_MIN=<MIN_VOLTAGE> with <MIN_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    #ifndef TTC_SDCARD3_VOLTAGE_MAX
        #define TTC_SDCARD3_VOLTAGE_MAX E_ttc_sdcard_voltage_3V6
        INFO( "Missing definition for TTC_SDCARD3_VOLTAGE_MAX. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD3_VOLTAGE_MAX=<MAX_VOLTAGE> with <MAX_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    // check extra defines for sdcard device #3 here

#endif
#ifdef TTC_SDCARD4
    #ifndef TTC_SDCARD4_VOLTAGE_MIN
        #define TTC_SDCARD4_VOLTAGE_MIN E_ttc_sdcard_voltage_3V5
        INFO( "Missing definition for TTC_SDCARD4_VOLTAGE_MIN. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD4_VOLTAGE_MIN=<MIN_VOLTAGE> with <MIN_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    #ifndef TTC_SDCARD4_VOLTAGE_MAX
        #define TTC_SDCARD4_VOLTAGE_MAX E_ttc_sdcard_voltage_3V6
        INFO( "Missing definition for TTC_SDCARD4_VOLTAGE_MAX. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD4_VOLTAGE_MAX=<MAX_VOLTAGE> with <MAX_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    // check extra defines for sdcard device #4 here

#endif
#ifdef TTC_SDCARD5
    #ifndef TTC_SDCARD5_VOLTAGE_MIN
        #define TTC_SDCARD5_VOLTAGE_MIN E_ttc_sdcard_voltage_3V5
        INFO( "Missing definition for TTC_SDCARD5_VOLTAGE_MIN. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD5_VOLTAGE_MIN=<MIN_VOLTAGE> with <MIN_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    #ifndef TTC_SDCARD5_VOLTAGE_MAX
        #define TTC_SDCARD5_VOLTAGE_MAX E_ttc_sdcard_voltage_3V6
        INFO( "Missing definition for TTC_SDCARD5_VOLTAGE_MAX. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD5_VOLTAGE_MAX=<MAX_VOLTAGE> with <MAX_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    // check extra defines for sdcard device #5 here

#endif
#ifdef TTC_SDCARD6
    #ifndef TTC_SDCARD6_VOLTAGE_MIN
        #define TTC_SDCARD6_VOLTAGE_MIN E_ttc_sdcard_voltage_3V5
        INFO( "Missing definition for TTC_SDCARD6_VOLTAGE_MIN. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD6_VOLTAGE_MIN=<MIN_VOLTAGE> with <MIN_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    #ifndef TTC_SDCARD6_VOLTAGE_MAX
        #define TTC_SDCARD6_VOLTAGE_MAX E_ttc_sdcard_voltage_3V6
        INFO( "Missing definition for TTC_SDCARD6_VOLTAGE_MAX. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD6_VOLTAGE_MAX=<MAX_VOLTAGE> with <MAX_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    // check extra defines for sdcard device #6 here

#endif
#ifdef TTC_SDCARD7
    #ifndef TTC_SDCARD7_VOLTAGE_MIN
        #define TTC_SDCARD7_VOLTAGE_MIN E_ttc_sdcard_voltage_3V5
        INFO( "Missing definition for TTC_SDCARD7_VOLTAGE_MIN. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD7_VOLTAGE_MIN=<MIN_VOLTAGE> with <MIN_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    #ifndef TTC_SDCARD7_VOLTAGE_MAX
        #define TTC_SDCARD7_VOLTAGE_MAX E_ttc_sdcard_voltage_3V6
        INFO( "Missing definition for TTC_SDCARD7_VOLTAGE_MAX. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD7_VOLTAGE_MAX=<MAX_VOLTAGE> with <MAX_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    // check extra defines for sdcard device #7 here

#endif
#ifdef TTC_SDCARD8
    #ifndef TTC_SDCARD8_VOLTAGE_MIN
        #define TTC_SDCARD8_VOLTAGE_MIN E_ttc_sdcard_voltage_3V5
        INFO( "Missing definition for TTC_SDCARD8_VOLTAGE_MIN. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD8_VOLTAGE_MIN=<MIN_VOLTAGE> with <MIN_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    #ifndef TTC_SDCARD8_VOLTAGE_MAX
        #define TTC_SDCARD8_VOLTAGE_MAX E_ttc_sdcard_voltage_3V6
        INFO( "Missing definition for TTC_SDCARD8_VOLTAGE_MAX. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD8_VOLTAGE_MAX=<MAX_VOLTAGE> with <MAX_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    // check extra defines for sdcard device #8 here

#endif
#ifdef TTC_SDCARD9
    #ifndef TTC_SDCARD9_VOLTAGE_MIN
        #define TTC_SDCARD9_VOLTAGE_MIN E_ttc_sdcard_voltage_3V5
        INFO( "Missing definition for TTC_SDCARD9_VOLTAGE_MIN. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD9_VOLTAGE_MIN=<MIN_VOLTAGE> with <MIN_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    #ifndef TTC_SDCARD9_VOLTAGE_MAX
        #define TTC_SDCARD9_VOLTAGE_MAX E_ttc_sdcard_voltage_3V6
        INFO( "Missing definition for TTC_SDCARD9_VOLTAGE_MAX. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD9_VOLTAGE_MAX=<MAX_VOLTAGE> with <MAX_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    // check extra defines for sdcard device #9 here

#endif
#ifdef TTC_SDCARD10
    #ifndef TTC_SDCARD10_VOLTAGE_MIN
        #define TTC_SDCARD10_VOLTAGE_MIN E_ttc_sdcard_voltage_3V5
        INFO( "Missing definition for TTC_SDCARD10_VOLTAGE_MIN. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD10_VOLTAGE_MIN=<MIN_VOLTAGE> with <MIN_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    #ifndef TTC_SDCARD10_VOLTAGE_MAX
        #define TTC_SDCARD10_VOLTAGE_MAX E_ttc_sdcard_voltage_3V6
        INFO( "Missing definition for TTC_SDCARD10_VOLTAGE_MAX. Using default value. Add a valid definition to your board makefile (COMPILE_OPTS += -DTTC_SDCARD10_VOLTAGE_MAX=<MAX_VOLTAGE> with <MAX_VOLTAGE> as one from e_ttc_sdcard_voltage" );
    #endif
    // check extra defines for sdcard device #10 here

#endif
//}

//}Static Configuration

//{ Includes2 ************************************************************
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_sdcard.h" or "ttc_sdcard.c"
//

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_sdcard_spi
    #include "sdcard/sdcard_spi_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

/** { Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_sdcard_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_sdcard_architecture
    #  warning Missing low-level definition for t_ttc_sdcard_architecture (using default)
    #define t_ttc_sdcard_architecture void
#endif
//}

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_sdcard_errorcode           return codes of SDCARD devices
    E_ttc_sdcard_errorcode_OK = 0,

    // other warnings go here..

    E_ttc_sdcard_errorcode_ERROR,                  // general failure
    E_ttc_sdcard_errorcode_NULL,                   // NULL pointer not accepted
    E_ttc_sdcard_errorcode_DeviceNotFound,         // corresponding device could not be found
    E_ttc_sdcard_errorcode_InvalidConfiguration,   // sanity check of device configuration failed
    E_ttc_sdcard_errorcode_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..
    E_ttc_sdcard_errorcode_card_not_found,         // trying to operate on a card that has been removed
    E_ttc_sdcard_errorcode_card_not_answering,     // sdcard seems to be present but does not answer
    E_ttc_sdcard_errorcode_card_not_initializing,  // sdcard did not react to E_ttc_sdcard_cmd1_get_operating_condition within timeout
    E_ttc_sdcard_errorcode_card_not_compatible,    // card did not correctly respond to ACMD41 (incompatible voltage range)
    E_ttc_sdcard_errorcode_card_not_supported,     // card of this type is not supported by current driver
    E_ttc_sdcard_errorcode_card_not_writable,      // sdcard seems to be write protected
    E_ttc_sdcard_errorcode_card_writing,           // unknown error occured while writing to card
    E_ttc_sdcard_errorcode_card_writing_crc,       // crc error while writing to card
    E_ttc_sdcard_errorcode_card_writing_ecc,       // error correction code error occured while writing to card (media defect)
    E_ttc_sdcard_errorcode_card_writing_cc,        // Internal card controller error during writing
    E_ttc_sdcard_errorcode_card_writing_locked,    // card is locked for writing
    E_ttc_sdcard_errorcode_card_writing_write,     // write error while writing to card (media error)
    E_ttc_sdcard_errorcode_card_writing_timeout,   // timeout occured while waiting for card to write block
    E_ttc_sdcard_errorcode_card_writing_eraseparam,// An invalid selection for erase, sectors or groups
    E_ttc_sdcard_errorcode_card_writing_eraseseq,  // erase sequence error while writing to card
    E_ttc_sdcard_errorcode_card_writing_address,   // write attempt to illegal block address
    E_ttc_sdcard_errorcode_card_reading,           // unknown error occured while reading from card
    E_ttc_sdcard_errorcode_card_reading_timeout,   // timeout occured while waiting for card to read block
    E_ttc_sdcard_errorcode_crc_reading,            // received data from card with non-matching CRC16
    E_ttc_sdcard_errorcode_crc_writing,            // send data to card with non-matching CRC16
    E_ttc_sdcard_errorcode_card_status,            // sdcard responded with error to command E_ttc_sdcard_command_get_card_status
    E_ttc_sdcard_errorcode_card_illegalcommand,    // sdcard has received an illegal command
    E_ttc_sdcard_errorcode_card_relative_address,  // sdcard did not generate a new relative card address (RCA)
    E_ttc_sdcard_errorcode_card_blocklength,       // sdcard responded with error to command E_ttc_sdcard_cmd16_set_blocklength
    E_ttc_sdcard_errorcode_card_crc_on_off,        // sdcard responded with error to command E_ttc_sdcard_cmd59_crc_on_off
    E_ttc_sdcard_errorcode_card_too_large,         // sdcard of such high capacity is not (yet) supported
    E_ttc_sdcard_errorcode_card_invalid_csd,       // failed to read card specific data register
    E_ttc_sdcard_errorcode_card_invalid_cid,       // failed to read card identification register

    E_ttc_sdcard_errorcode_unknown                // no valid errorcodes past this entry
} e_ttc_sdcard_errorcode;

typedef enum {   // e_ttc_sdcard_speed               interface can be operated at different speeds
    E_ttc_sdcard_speed_minimum = 0,           // lowest supported speed (required to wake up sdcard)
    E_ttc_sdcard_speed_normal,                // interface speed for normal operation
    E_ttc_sdcard_speed_high,                  // maximum supported interface speed
    E_ttc_sdcard_speed_unknown                // no valid events past this entry
} e_ttc_sdcard_speed;

typedef enum {   // e_ttc_sdcard_speed               interface can be operated at different speeds
    E_ttc_sdcard_voltage_none = 0,           // voltage not set
    E_ttc_sdcard_voltage_2V7,                 // 2.7V
    E_ttc_sdcard_voltage_2V8,                 // 2.8V
    E_ttc_sdcard_voltage_2V9,                 // 2.9V
    E_ttc_sdcard_voltage_3V0,                 // 3.0V
    E_ttc_sdcard_voltage_3V1,                 // 3.1V
    E_ttc_sdcard_voltage_3V2,                 // 3.2V
    E_ttc_sdcard_voltage_3V3,                 // 3.3V
    E_ttc_sdcard_voltage_3V4,                 // 3.4V
    E_ttc_sdcard_voltage_3V5,                 // 3.5V
    E_ttc_sdcard_voltage_3V6,                 // 3.6V
    E_ttc_sdcard_voltage_unknown              // no valid events past this entry
} e_ttc_sdcard_voltage;

typedef enum {   // e_ttc_sdcard_command             valid SDCARD command codes
    E_ttc_sdcard_cmd00_go_idle_state                 = 0x40 | 0x00,  // CMD0  - resets card
    E_ttc_sdcard_cmd01_get_operating_condition       = 0x40 | 0x01,  // CMD1  - MMC: read operating condition register
    E_ttc_sdcard_cmd03_get_relative_address          = 0x40 | 0x03,  // CMD3  - ask sdcard for a new relative card address (RCA)
    E_ttc_sdcard_cmd08_get_interface_condition       = 0x40 | 0x08,  // CMD8  - check interface operating conditions
    E_ttc_sdcard_cmd09_get_card_specific_data        = 0x40 | 0x09,  // CMD9  - read card specific data (CSD) register
    E_ttc_sdcard_cmd10_get_card_identification       = 0x40 | 0x0a,  // CMD10 - read card identification (CID) register
    E_ttc_sdcard_cmd12_stop                          = 0x40 | 0x0c,  // CMD12 - stop current operation
    E_ttc_sdcard_cmd13_sd_status                     = 0x40 | 0x0d,  // CMD13 - send sd card status
    E_ttc_sdcard_cmd16_set_blocklength               = 0x40 | 0x10,  // CMD16 - set blocklength
    E_ttc_sdcard_cmd17_block_read_single             = 0x40 | 0x11,  // CMD17 - read single block
    E_ttc_sdcard_cmd23_set_pre_erase_amount          = 0x40 | 0x17,  // CMD23 - set amount of blocks to be pre-erased during multiple block writing
    E_ttc_sdcard_cmd24_block_write_single            = 0x40 | 0x18,  // CMD24 - write (single) block
    E_ttc_sdcard_cmd25_block_write_multiple          = 0x40 | 0x19,  // CMD25 - write multiple blocks
    E_ttc_sdcard_cmd32_block_erase_start             = 0x40 | 0x20,  // CMD32 - set address of first block to be erased
    E_ttc_sdcard_cmd33_block_erase_end               = 0x40 | 0x21,  // CMD33 - set address of last block to be erased
    E_ttc_sdcard_cmd38_block_erase                   = 0x40 | 0x26,  // CMD38 - erase previously selected range of blocks
    E_ttc_sdcard_cmd55_application_cmd               = 0x40 | 0x37,  // CMD55 - next command is an application specific command, not standard
    E_ttc_sdcard_cmd58_get_ocr                       = 0x40 | 0x3a,  // CMD58 - read the ocr register
    E_ttc_sdcard_cmd59_crc_on_off                    = 0x40 | 0x3b,  // CMD59 - turns crc on or off
    E_ttc_sdcard_command_start_block                 = 0xfe,         // start a data block
    E_ttc_sdcard_command_mmc_floating_bus            = 0xff,         // put bus into high-Z (disconnect from bus)
} e_ttc_sdcard_command;
typedef enum {   // e_ttc_sdcard_command_application valid SDCARD application command codes (must be preceeded by CMD55)
    E_ttc_sdcard_acmd00_none                         = 0,            // no command at all
    E_ttc_sdcard_acmd41_sd_send_operating_condition  = 0x40 | 0x29,  // ACMD41 - SDCARD: read operating condition
    E_ttc_sdcard_acmd_unknown
} e_ttc_sdcard_command_application;

typedef enum {   // e_ttc_sdcard_architecture        types of architectures supported by SDCARD driver
    E_ttc_sdcard_architecture_None = 0,       // no architecture selected


    E_ttc_sdcard_architecture_spi, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    E_ttc_sdcard_architecture_unknown        // architecture not supported
} e_ttc_sdcard_architecture;

typedef enum {   // e_ttc_sdcard_state       current card state (stored in SD_STATUS response to CMD13) (-> Documentation/Devices/SDCARD_Physical_Layer_Spec.pdf p.63)
    E_ttc_sdcard_state_idle  = 0,
    E_ttc_sdcard_state_ready,
    E_ttc_sdcard_state_ident,
    E_ttc_sdcard_state_stby,
    E_ttc_sdcard_state_tran,
    E_ttc_sdcard_state_data,
    E_ttc_sdcard_state_rcv,
    E_ttc_sdcard_state_prg,
    E_ttc_sdcard_state_dis,
    E_ttc_sdcard_state_reserved9,
    E_ttc_sdcard_state_reserved10,
    E_ttc_sdcard_state_reserved11,
    E_ttc_sdcard_state_reserved12,
    E_ttc_sdcard_state_reserved13,
    E_ttc_sdcard_state_reserved14,
    E_ttc_sdcard_state_reserved15,
    E_ttc_sdcard_state_unknown     // unkown states below
} e_ttc_sdcard_state;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef struct {                             // t_ttc_sdcard_command_argument  combination of SDCARD command + argument
    e_ttc_sdcard_command Command;    // bit #6 must be set (|= 0x40)
    t_u32                Argument;   // 32 bit argument value
    t_u8                 CRC7;       // checksum
} __attribute__( ( __packed__ ) ) t_ttc_sdcard_command_argument;

typedef union {                              // architecture dependent configuration

#ifdef EXTENSION_sdcard_spi
    t_sdcard_spi_config spi;  // automatically added by ./create_DeviceDriver.pl
#endif
    //InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_sdcard_architecture;

typedef struct s_ttc_sdcard_features {       // static minimum, maximum and default values of features of single sdcard

    // Add any amount of architecture independent values describing sdcard devices here.
    // You may also want to add a plausibility check for each value to _ttc_sdcard_configuration_check().

    /** Example Features

    t_u16 MinBitRate;   // minimum allowed transfer speed
    t_u16 MaxBitRate;   // maximum allowed transfer speed
    */

    const t_u8 unused; // remove me!

} __attribute__( ( __packed__ ) ) t_ttc_sdcard_features;

typedef struct s_ttc_sdcard_data_response_token { // response token send by card after each received block (-> Documentation/Devices/SDCARD_Physical_Layer_Spec.pdf p.112)
    unsigned AlwaysOne  : 1;
    unsigned Status     : 3; // ==0b010: data accepted; ==0b101: crc error; ==0b110: write error
    unsigned AlwaysZero : 1;
    unsigned reserved   : 3;
} __attribute__( ( __packed__ ) ) t_ttc_sdcard_data_response_token;

typedef struct s_ttc_sdcard_data_error_token { // response token send by card after each received block (-> Documentation/Devices/SDCARD_Physical_Layer_Spec.pdf p.112)
    unsigned Error       : 1; // ==1: general error
    unsigned FailedCRC   : 1; // ==1: CRC failed
    unsigned CardECC     : 1; // ==1: media error
    unsigned OutOfRange  : 1; // ==1: invalid address given
    unsigned AlwaysZero  : 4;
} __attribute__( ( __packed__ ) ) t_ttc_sdcard_data_error_token;

typedef struct {                               // Card Status  (-> Documentation/Devices/SDCARD_Physical_Layer_Spec.pdf p.61)
    unsigned OUT_OF_RANGE          : 1; // The command’s argument was out of the allowed range for this card.
    unsigned ADDRESS_ERROR         : 1; // A misaligned address which did notmatch the block length was used in the command.
    unsigned BLOCK_LEN_ERROR       : 1; // The transferred block length is notallowed for this card, or the number of transferred bytes does not match the block length.
    unsigned ERASE_SEQ_ERROR       : 1; // An error in the sequence of erase commands occurred.
    unsigned ERASE_PARAM           : 1; // An invalid selection of write-blocks for erase occurred.
    unsigned WP_VIOLATION          : 1; // Set when the host attempts to writeto a protected block or to the temporary or permanent write protected card.
    unsigned CARD_IS_LOCKED        : 1; // When set, signals that the card is locked by the host
    unsigned LOCK_UNLOCK_FAILED    : 1; // Set when a sequence or password error has been detected in lock/unlock card command.
    unsigned COM_CRC_ERROR         : 1; // The CRC check of the previous command failed.
    unsigned ILLEGAL_COMMAND       : 1; // Command not legal for the card state
    unsigned CARD_ECC_FAILED       : 1; // Card internal ECC was applied but failed to correct the data.
    unsigned CC_ERROR              : 1; // Internal card controller error
    unsigned ERROR                 : 1; // A general or an unknown error occurred during the operation.
    unsigned reserved              : 2; // reserved
    unsigned CSD_OVERWRITE         : 1; // Can be either one of the following errors: - The read only section of the CSD does not match the card content. - An attempt to reverse the copy (setas original) or permanent WP (unprotected) bits was made.
    unsigned WP_ERASE_SKIP         : 1; // Set when only partial address space was erased due to existing write protected blocks or the temporary or permanent write protected card was erased.
    unsigned CARD_ECC_DISABLED     : 1; // The command has been executed without using the internal ECC.
    unsigned ERASE_RESET           : 1; // An erase sequence was cleared before executing because an out of erase sequence command was received
    unsigned CURRENT_STATE         : 4; // (-> e_ttc_sdcard_state) The state of the card when receiving the command. If the command execution causes a state change, it will be visible to the host in the response to the next command. The four bits are interpreted as a binary coded number between 0 and 15.
    unsigned READY_FOR_DATA        : 1; // Corresponds to buffer empty signaling on the bus
    unsigned reserved2             : 2; // reserved
    unsigned APP_CMD               : 1; // The card will expect ACMD, or an indication that the command has been interpreted as ACMD
    unsigned reserved_sdio         : 1; // reserved for SDIO Card
    unsigned AKE_SEQ_ERROR         : 1; // Error in the sequence of the authentication process
    unsigned reserved_app          : 1; // reserved for application specifig commands
    unsigned reserved_manufacturer : 2; // reserved for manufacturer test mode
} __attribute__( ( __packed__ ) ) t_ttc_sdcard_status;

typedef union {                               //  Status Response R2 to CMD13 (-> Documentation/Devices/SDCARD_Physical_Layer_Spec.pdf p.110)
    t_u16 Word;
    struct {
        unsigned  InIdleState                 : 1;  // card is in idle state
        unsigned  EraseReset                  : 1;  // erase reset flag
        unsigned  Error_IllegalCommand        : 1;  // illegal command flag
        unsigned  Error_CRC                   : 1;  // crc error flag
        unsigned  Error_EraseSequence         : 1;  // erase sequence error flag
        unsigned  Error_Address               : 1;  // address error flag
        unsigned  Error_Parameter             : 1;  // parameter flag
        unsigned  AlwaysZero                  : 1;  // unused bit 7

        unsigned CardIsLocked                 : 1; // Set when the card is locked by the user. Reset when it is unlocked.
        unsigned WPeraseSkip_LockUnlockFailed : 1; // Write protect erase skip | lock/unlock command failed: This status bit has two functions over-loaded. It is set when the host attempts to erase a write-protected sector or makes a sequence or password errors during card lock/unlock operation.
        unsigned Error                        : 1; // A general or an unknown error occurred during the operation.
        unsigned CC_Error                     : 1; // Internal card controller error.
        unsigned CardECCfailed                : 1; // Card internal ECC was applied but failed to correct the data.
        unsigned WP_Violation                 : 1; // The command tried to write a write-protected block.
        unsigned EraseParam                   : 1; // An invalid selection for erase, sectors or groups.
        unsigned OutOfRange_CSD_Overwrite     : 1; //
    } Bits;
} __attribute__( ( __packed__ ) ) t_ttc_sdcard_response_r2;

typedef union {                               // Status Response R1 (returned by most commands)
    t_u8 Byte;
    struct {
        unsigned  InIdleState          : 1;  // card is in idle state
        unsigned  EraseReset           : 1;  // erase reset flag
        unsigned  Error_IllegalCommand : 1;  // illegal command flag
        unsigned  Error_CRC            : 1;  // crc error flag
        unsigned  Error_EraseSequence  : 1;  // erase sequence error flag
        unsigned  Error_Address        : 1;  // address error flag
        unsigned  Error_Parameter      : 1;  // parameter flag
        unsigned  AlwaysZero           : 1;  // unused bit 7
    } Bits;
} __attribute__( ( __packed__ ) ) t_ttc_sdcard_response_r1;

typedef union {                                // t_ttc_sdcard_response_r1 - r1 type response byte from sdcard
    t_u8                             Byte;      // Response as single byte (R1 type response)
    t_u16                            Word;      // Response as two bytes   (R2 type response)
    t_ttc_sdcard_response_r1         R1;        // R1 Status Response returned by most commands
    t_ttc_sdcard_response_r2         R2;        // R2 Response returned by send status command  (CMD13)
    t_ttc_sdcard_data_response_token Data;      // response token send by card after each received block
    t_ttc_sdcard_data_error_token    DataError; // response token send by card after each received block
} __attribute__( ( __packed__ ) ) u_ttc_sdcard_response;

typedef struct s_ttc_sdcard_specific_data_v1 { // card specific data (CSD) register for version 1 cards
    // byte 0
    unsigned reserved1                  : 6; // == 0
    unsigned Version                    : 2;
    // byte 1
    t_u8 DataReadAccesTime1;                 // TAAC
    // byte 2
    t_u8 DataReadAccessTime2;                // NSAC
    // byte 3
    t_u8 MaxDataTransferRate;                // TRAN_SPEED
    // byte 4
    t_u8 CardCommandClasses_High;            // CCC High
    // byte 5
    unsigned MaxReadDataBlockLength     : 4; // READ_BL_LEN;
    unsigned CardCommandClasses_Low     : 4;
    // byte 6
    unsigned DeviceSize_High            : 2; // C_SIZE High
    unsigned reserved2                  : 2;
    unsigned ConfigurableDriverStage    : 1; // DSR_IMP
    unsigned ReadBlockMisalign          : 1; // READ_BLK_MISALIGN
    unsigned WriteBlockMisalign         : 1; // WRITE_BLK_MISALIGN
    unsigned PartialBlockRead           : 1; // READ_BL_PARTIAL
    // byte 7
    t_u8     DeviceSize_Mid;                 // C_SIZE Mid
    // byte 8
    unsigned VddReadCurrentMax          : 3; // VDD_R_CUR_MAX
    unsigned VddReadCurrentMin          : 3; // VDD_R_CUR_MIN
    unsigned DeviceSize_Low             : 2; // C_SIZE Low
    // byte 9
    unsigned CapacitySize_Multiply_High : 2; // C_SIZE_MULT High
    unsigned VddWriteCurrentMax         : 3; // VDD_W_CUR_MAX
    unsigned VddWriteCurrentMin         : 3; // VDD_W_CUR_MIN
    // byte 10
    unsigned EraseableSectorSize_High   : 6; // SECTOR_SIZE High
    unsigned Can_EraseMultipleOf512     : 1; // ERASE_BLK_EN
    unsigned CapacitySize_Multiply_Low  : 1; // C_SIZE_MULT Low
    // byte 11
    unsigned WriteProtectedGroupSize    : 7; // WP_GRP_SIZE
    unsigned EraseableSectorSize_Low    : 1; // SECTOR_SIZE Low
    // byte 12
    unsigned WriteBlockLengthMax_High   : 2; // WRITE_BL_LEN High
    unsigned Read2WriteFactor           : 3; // R2W_FACTOR
    unsigned reserved3                  : 2;
    unsigned Can_WriteProtectedGroup    : 1; // WP_GRP_ENABLE
    // byte 13
    unsigned reserved4                  : 5;
    unsigned Can_WritePartialBlocks     : 1; // WRITE_BL_PARTIAL (write_partial : 1;)
    unsigned WriteBlockLengthMax_Low    : 2; // WRITE_BL_LEN Low
    // byte 14
    unsigned reserved5                  : 2;
    unsigned FileFormat                 : 2; // FILE_FORMAT
    unsigned WriteProtectedTemporarily  : 1; // TMP_WRITE_PROTECT
    unsigned WriteProtectedPermanently  : 1; // PERM_WRITE_PROTECT
    unsigned ContentIsCopy              : 1; // COPY
    unsigned FileFormatGroup            : 1; // FILE_FORMAT_GRP (==0x1)
    // byte 15
    t_u8     CRC7;                           // checksum in bits 1..7, bit 0 is always ==1

} __attribute__( ( __packed__ ) ) t_ttc_sdcard_specific_data_v1;

typedef struct s_ttc_sdcard_specific_data_v2 { // card specific data (CSD) register for version 1 cards
    // byte 0
    unsigned reserved1                  : 6; // == 0
    unsigned Version                    : 2;
    // byte 1
    t_u8 DataReadAccesTime1;                 // TAAC
    // byte 2
    t_u8 DataReadAccessTime2;                // NSAC
    // byte 3
    t_u8 MaxDataTransferRate;                // TRAN_SPEED
    // byte 4
    t_u8 CardCommandClasses_High;            // CCC High
    // byte 5
    unsigned MaxReadDataBlockLength     : 4; // READ_BL_LEN;
    unsigned CardCommandClasses_Low     : 4;
    // byte 6
    unsigned reserved2                  : 4;
    unsigned ConfigurableDriverStage    : 1; // DSR_IMP
    unsigned ReadBlockMisalign          : 1; // READ_BLK_MISALIGN
    unsigned WriteBlockMisalign         : 1; // WRITE_BLK_MISALIGN
    unsigned PartialBlockRead           : 1; // READ_BL_PARTIAL
    // byte 7
    unsigned reserved3                  : 2;
    unsigned DeviceSize_High            : 6; // C_SIZE High
    // byte 8
    t_u8     DeviceSize_Mid;                 // C_SIZE Mid
    // byte 9
    t_u8     DeviceSize_Low;                 // C_SIZE Low
    // byte 10
    unsigned EraseableSectorSize_High   : 6; // SECTOR_SIZE High
    unsigned Can_EraseMultipleOf512     : 1; // ERASE_BLK_EN
    unsigned reserved4                  : 1;
    // byte 11
    unsigned WriteProtectedGroupSize    : 7; // WP_GRP_SIZE
    unsigned EraseableSectorSize_Low    : 1; // SECTOR_SIZE Low
    // byte 12
    unsigned WriteBlockLengthMax_High   : 2; // WRITE_BL_LEN High
    unsigned Read2WriteFactor           : 3; // R2W_FACTOR
    unsigned reserved5                  : 2;
    unsigned Can_WriteProtectedGroup    : 1; // WP_GRP_ENABLE
    // byte 13
    unsigned reserved6                  : 5;
    unsigned Can_WritePartialBlocks     : 1; // WRITE_BL_PARTIAL == WRITE_PARTIAL
    unsigned WriteBlockLengthMax_Low    : 2; // WRITE_BL_LEN Low
    // byte 14
    unsigned reserved7                  : 2;
    unsigned FileFormat                 : 2; // FILE_FORMAT
    unsigned WriteProtectedTemporarily  : 1; // TMP_WRITE_PROTECT
    unsigned WriteProtectedPermanently  : 1; // PERM_WRITE_PROTECT
    unsigned ContentIsCopy              : 1; // COPY
    unsigned FileFormatGroup            : 1; // FILE_FORMAT_GRP (==0x1)
    // byte 15
    t_u8     CRC7;                           // checksum in bits 1..7, bit 0 is always ==1
} __attribute__( ( __packed__ ) ) t_ttc_sdcard_specific_data_v2;

typedef union {                              // u_ttc_sdcard_specific_data - card specific data (CSD) register
    t_u8                          Bytes[16];
    t_ttc_sdcard_specific_data_v1 V1; // SDCARD v1 card specific data register
    t_ttc_sdcard_specific_data_v2 V2; // SDCARD v2 card specific data register
} __attribute__( ( __packed__ ) ) u_ttc_sdcard_specific_data;

typedef struct s_ttc_sdcard_csd_response { // 136 bit response to CMD9 == CSD register of sdcard/mmc
    t_u8                       reserved1;
    u_ttc_sdcard_specific_data CSD;
} __attribute__( ( __packed__ ) ) t_ttc_sdcard_csd_response;

typedef struct s_ttc_sdcard_identification {
    // Byte 0
    t_u8     ManufacturerID;
    t_u16    ApplicationID;
    t_u8     Name[5];             // Product Name

    // Byte 3
    unsigned RevisionM       : 4; // Product revision N.M
    unsigned RevisionN       : 4;

    // Byte 4-7
    t_u32    SerialNumber;        // Product serial number

    // Byte 8
    unsigned Date_Year_High  : 4; // Manufacturing date
    unsigned reserved        : 4;

    // Byte 9
    unsigned Date_Month      : 4;
    unsigned Date_Year_Low   : 4;

    // Byte 10
    t_u8     CRC7;                // checksum in bits 1..7, bit 0 is always ==1
} __attribute__( ( __packed__ ) ) t_ttc_sdcard_identification;

typedef struct s_ttc_sdcard_cid_response { // 136 bit response to CMD10 == CID register of sdcard/mmc
    t_u8                        reserved1;
    t_ttc_sdcard_identification CID;
} __attribute__( ( __packed__ ) ) t_ttc_sdcard_cid_response;

typedef struct s_ttc_sdcard_ocr { // operation conditions (OCR) register of sdcard/mmc
    union {
        t_u32 Word;
        struct {
            unsigned reserved1          : 7;
            unsigned LowVoltageRange    : 1; // reserved for low voltage range

            unsigned reserved2          : 7;
            unsigned Vdd_27_28          : 1; // Vdd = 2.7-2.8V

            unsigned Vdd_28_29          : 1; // Vdd = 2.8-2.8V
            unsigned Vdd_29_30          : 1; // Vdd = 2.9-3.0V
            unsigned Vdd_30_31          : 1; // Vdd = 3.0-3.1V
            unsigned Vdd_31_32          : 1; // Vdd = 3.1-3.2V
            unsigned Vdd_32_33          : 1; // Vdd = 3.2-3.3V
            unsigned Vdd_33_34          : 1; // Vdd = 3.3-3.4V
            unsigned Vdd_34_35          : 1; // Vdd = 3.4-3.5V
            unsigned Vdd_35_36          : 1; // Vdd = 3.5-3.6V

            unsigned reserved3          : 6;
            unsigned BlockAddressing    : 1; // CCS ==1: card uses block addressing for higher capacity; ==0: card uses byte adressing
            unsigned CardPowerUpStatus  : 1; // BUSY

        } Bits;
    };
} __attribute__( ( __packed__ ) ) t_ttc_sdcard_ocr;

typedef struct s_ttc_sdcard_ocr_response { // 48 bit response to ACMD41 containing operation conditions (OCR) register of sdcard/mmc
    t_u8             reserved1;
    t_ttc_sdcard_ocr OCR;
    t_u8             reserved2;
} __attribute__( ( __packed__ ) ) t_ttc_sdcard_ocr_response;

typedef struct s_ttc_sdcard_data { // latest data being read from sdcard
    u_ttc_sdcard_response       LastResponse;    // latest command response returned from sdcard
    t_u16                       RelativeAddress; // current relative address of this card (call sdcard_common_new_relative_address() to update)
    t_u16                       BlockSize;       // >0: size of each data block on current sdcard; ==0: no sdcard present
    t_u16                       BlockMaxLength;  // >0: maximum block length supported by current card (==512..2048)
    t_u32                       BlockCount;      // >0: amount of data blocks on current sdcard; ==0: no sdcard present
    t_u32                       SerialNumber;    // >0: unique serial number off current sdcard; ==0: no sdcard present
    t_u16                       Year;            // year of manufacture
    //? t_ttc_sdcard_status         Status;          // latest card status from ttc_sdcard_status()/ sdcard_common_status()
    //? e_ttc_sdcard_state          CurrentState;    // decoded Status.CURRENT_STATE field (updated by sdcard_common_status())
    t_u8                        Month;           // month of manufacture (1=january, 2=february, ..., 12=december)
} t_ttc_sdcard_data;

typedef struct s_ttc_sdcard_config { //          architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_sdcard_init()
    //       and after ttc_sdcard_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // Hint: Order structure elements for optimal alignment.
    //       You should place smaller datatypes like t_u8 and t_u16 after all t_u32 and pointer types
    //       to prevent insertion of pad-bytes. Most architectures don't support placing 32-bit datatypes at
    //       unaligned address.

    struct { // Init: fields to be set by application before first calling ttc_sdcard_init() --------------
        // Do not change these values after calling ttc_sdcard_init()!

        t_u8 Retries;  // amount of retries when reading from sdcard

        struct { // generic initial configuration bits common for all low-level Drivers
            unsigned Support_HighCapacity : 1; // ==1: driver supports high-capacity cards (>2GB)
            unsigned Support_Vdd_27_28    : 1; // ==1: current system supports Vdd = 2.7-2.8V
            unsigned Support_Vdd_28_29    : 1; // ==1: current system supports Vdd = 2.8-2.8V
            unsigned Support_Vdd_29_30    : 1; // ==1: current system supports Vdd = 2.9-3.0V
            unsigned Support_Vdd_30_31    : 1; // ==1: current system supports Vdd = 3.0-3.1V
            unsigned Support_Vdd_31_32    : 1; // ==1: current system supports Vdd = 3.1-3.2V
            unsigned Support_Vdd_32_33    : 1; // ==1: current system supports Vdd = 3.2-3.3V
            unsigned Support_Vdd_33_34    : 1; // ==1: current system supports Vdd = 3.3-3.4V
            unsigned Support_Vdd_34_35    : 1; // ==1: current system supports Vdd = 3.4-3.5V
            unsigned Support_Vdd_35_36    : 1; // ==1: current system supports Vdd = 3.5-3.6V
        } Flags;
    } Init;

    // Fields below are read-only for application! ----------------------------------------------------------

    u_ttc_sdcard_architecture* LowLevelConfig; // low-level configuration (structure defined by low-level driver)

    const t_ttc_sdcard_features* Features;     // constant features of this sdcard

    struct { // status flags common for all architectures
        unsigned Initialized         : 1;  // ==1: device has been initialized successfully
        unsigned CardPresent         : 1;  // ==1: sdcard is currently inserted into slot
        unsigned CardMounted         : 1;  // ==1: sdcard is currently mounted and ready to be used
        unsigned CardBusyWriting     : 1;  // ==1: sdcard has started writing a block => call ttc_sdcard_wait_busy() before next command
        unsigned CardDisabledCRC     : 1;  // ==1: crc has been disabled (faster operations); ==0: all sdcard operations will use checksum byte (slower but more secure)
        unsigned CardWriteProtected  : 1;  // ==1: sdcard is write protected
        unsigned CardVersion         : 2;  // ==0: no sdcard; ==1: sdcard v1; ==2: sdcard v2
        unsigned CardDataValid       : 1;  // ==1: data in t_ttc_sdcard_config.Card is valid; ==0: call sdcard_common_card_data()!
        unsigned CardIsMMC           : 1;  // ==1: card is MMC; ==0: card is SDCARD
        unsigned CardIsHighCapacity  : 1;  // ==0: SD Card <= 2GB; ==1 SDH Card > 2GB
        unsigned CardBlockAddressing : 1;  // ==1: SD Card >=v2 requires block adressing; ==0: byte adressing

        // ToDo: additional high-level flags go here..
    } Flags;

    t_ttc_sdcard_data  Card;           // summary of data relevant for current card
    e_ttc_sdcard_speed InterfaceSpeed; // current interface speed (==E_ttc_sdcard_speed_minimum during sdcard wakeup)

    // Last returned error code
    // IMPORTANT: Every ttc_sdcard_*() function that returns e_ttc_sdcard_errorcode has to update this value if it returns an error!
    e_ttc_sdcard_errorcode      LastError;
    e_ttc_storage_event         LastEvent;        // last event being reported by ttc_sdcard_medium_detect()
    t_u8                        LogicalIndex;     // automatically set: logical index of device to use (1 = TTC_SDCARD1, ...)
    e_ttc_sdcard_architecture   Architecture;     // type of architecture used for current sdcard device

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_sdcard_config;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_SDCARD_TYPES_H
