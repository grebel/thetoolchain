/** ttc_basic_types.h ***********************************************{
 *
 * TheToolChain.com
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Basic datatypes and enums
 * 
}*/

#ifndef TTC_BASIC_TYPES_H
#define TTC_BASIC_TYPES_H

#include "cm3/cm3_basic_types.h"

// datatypes used for small amounts
typedef u8_t SmallBase_t;
typedef u8_t SmallBaseSigned_t;

#ifndef NULL
#define NULL 0
#endif

typedef enum ErrorCode_E {
    ec_None,                           // 0
    ec_Malloc,                         // 1
    ec_Debug,
    ec_NULL,
    ec_InvalidArgument,
    ec_InvalidConfiguration,
    ec_InvalidImplementation,          // an check-routine detected a low-level implementation that does not work as expected
    ec_InvalidAddressAlignment,        // given address does not match alignment criteria
    ec_ArrayIndexOutOfBound,           // array access with invalid index (-> ttc_memory.h/A() )
    ec_IllegalPointer,
    ec_NotImplemented,
    ec_OutOfMemory,                    // requested memory could not be allocated (maybe packet pool empty?)
    ec_OutOfResources,                 // requested amount of resources has exceeded predefined allowed amount
    ec_DeviceNotFound,
    ec_HardFault,                      // hard fault handler called: programm has stopped
    ec_TimeOut,                        // TimeOut occured during waiting for a resource
    ec_UnhandledInterrupt,             // an unimplemented interrupt vector has been called
    ec_UNKNOWN
} ErrorCode_e;

#if TARGET_DATA_WIDTH==32
  #define TARGET_MAX_UNSIGNED 0xffffffff
  #define TARGET_MAX_POSITIVE 0x7fffffff
  #define TARGET_MAX_NEGATIVE 0xffffffff
#endif
#if TARGET_DATA_WIDTH==16
  #define TARGET_MAX_UNSIGNED 0xffff
  #define TARGET_MAX_POSITIVE 0x7fff
  #define TARGET_MAX_NEGATIVE 0xffff
#endif
#if TARGET_DATA_WIDTH==8
  #define TARGET_MAX_UNSIGNED 0xff
  #define TARGET_MAX_POSITIVE 0x7f
  #define TARGET_MAX_NEGATIVE 0xff
#endif

// make these functions known to sources that cannot include ttc_basic.h
extern void Assert_Halt_EC(volatile ErrorCode_e ErrorCode);
extern bool Assert_On_False(BOOL Condition);

// This function ensures that given two addresses are the same.
// It is required to safe check register addresses (direct comparison created some strange errors)
void Assert_SameAddress(volatile void* Address1, volatile void* Address2);

/*
#if TARGET_DATA_WIDTH==32
  #define strnlen ttc_string_length32
#endif
#if TARGET_DATA_WIDTH==16
  #define strnlen ttc_string_length32
#endif
#ifndef strnlen
  #define strnlen ttc_string_length8
#endif
*/

#endif
