#ifndef systick_common_h
#define systick_common_h

/** systick_common.h ****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to systick low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.h revision 14 at 20160926 13:55:21 UTC
 *
 *  Authors: Gregor Rebel
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "systick_cortexm3.c"
 */

#include "../ttc_basic.h"
#include "../ttc_systick_types.h" // will include systick_common_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_systick_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_systick_foo
 */

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions
/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_systick.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_systick.h for details.
 *
 * You normally should not need to add functions here.
 * Global feature functions must be added in ttc_systick.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl systick UPDATE".
 */

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //systick_common_h
