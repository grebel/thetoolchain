#ifndef SYSTICK_FREERTOS_TYPES_H
#define SYSTICK_FREERTOS_TYPES_H

/** { systick_freertos.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_systick.h providing
 *  Structures, Enums and Defines being required by ttc_systick_types.h
 *
 *  system timer for precise time measures and periodic interrupt generation using freertos systick driver
 *
 *  Created from template device_architecture_types.h revision 24 at 20160928 08:55:05 UTC
 *
 *  Note: See ttc_systick.h for description of high-level systick implementation.
 *  Note: See systick_freertos.h for description of freertos systick implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_SYSTICK1   // device not defined in makefile
    #define TTC_SYSTICK1    ta_systick_freertos   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)



//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "FreeRTOS.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes

// System Tick Counter provided by FreeRTOS
// -> additionals/300_scheduler_freertos/Source/tasks.c
extern volatile portTickType xTickCount;

//{ Structures/ Enums required by ttc_systick_types.h *************************

typedef struct { // register description (adapt according to freertos registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_systick_register;

typedef struct {  // freertos specific configuration data of single systick device
    t_systick_register* BaseRegister;       // base address of systick device registers
} __attribute__( ( __packed__ ) ) t_systick_freertos_config;

// t_ttc_systick_architecture is required by ttc_systick_types.h
#define t_ttc_systick_architecture t_systick_freertos_config

//} Structures/ Enums


#endif //SYSTICK_FREERTOS_TYPES_H
