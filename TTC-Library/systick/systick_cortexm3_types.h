#ifndef SYSTICK_CORTEXM3_TYPES_H
#define SYSTICK_CORTEXM3_TYPES_H

/** { systick_cortexm3.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_systick.h providing
 *  Structures, Enums and Defines being required by ttc_systick_types.h
 *
 *  system timer for precise time measures and periodic interrupt generation on cortexm3 based microcontrollers (configures systick timer by itself)
 *
 *  Created from template device_architecture_types.h revision 24 at 20160928 08:55:32 UTC
 *
 *  Note: See ttc_systick.h for description of high-level systick implementation.
 *  Note: See systick_cortexm3.h for description of cortexm3 systick implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_SYSTICK1   // device not defined in makefile
    #define TTC_SYSTICK1    ta_systick_cortexm3   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_systick_types.h *************************

typedef struct {  // cortexm3 specific configuration data of single systick device
    t_u8 Unused;
    //X t_u16 ReloadValue;  // reload value for systick counter (values < 1000 mean high interrupt stress)
} __attribute__( ( __packed__ ) ) t_systick_cortexm3_config;

// t_ttc_systick_architecture is required by ttc_systick_types.h
#define t_ttc_systick_architecture t_systick_cortexm3_config

//} Structures/ Enums


#endif //SYSTICK_CORTEXM3_TYPES_H
