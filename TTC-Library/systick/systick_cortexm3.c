/** systick_cortexm3.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_systick.c.
 *
 *  system timer for precise time measures and periodice interrupt generation
 *
 *  Created from template device_architecture.c revision 33 at 20160926 13:55:21 UTC
 *
 *  Note: See ttc_systick.h for description of high-level systick implementation.
 *  Note: See systick_cortexm3.h for description of cortexm3 systick implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "systick_cortexm3.h".
 */

#include "systick_cortexm3.h"
#include "../ttc_memory.h"            // basic memory checks
#include "../ttc_task.h"
#include "../ttc_sysclock.h"
#include "systick_common.h"           // generic functions shared by low-level drivers of all architectures
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_systick_features systick_cortexm3_Features = {
    /** example features
      .MinBitRate       = 4800,
      .MaxBitRate       = 230400,
    */
    // set more feature values...

    .unused = 0
};

// increased by 1 on every clock tick interrupt
t_u32 systick_cortexm3_Ticks              = 0;
t_u32 systick_cortexm3_NanoSecondsPerTick = 0; // copy from Config->NanoSecondsPerSysTick
//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

/** Implementation for interrupt table entry SysTick_Handler.
 *
 * This function must be called at every systick event automatically to increase the system tick counter.
 *
 * Note: The SysTick_Handler should be defined as a weak symbol and being placed
 *       in Nested Vector Interrupt Configuration table in startup_stm32l1xx_ms.s assembly file.
 */
void __attribute( ( noinline ) ) SysTick_Handler() {
    // Note: systick_cortexm3_get_elapsed_ticks_isr() contains doubled implementation. Keep in sync if you change anything here!
    systick_cortexm3_Ticks++;
}
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

void                     systick_cortexm3_configuration_check( t_ttc_systick_config* Config ) {
    Assert_SYSTICK_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

}
e_ttc_systick_errorcode  systick_cortexm3_deinit( t_ttc_systick_config* Config ) {
    ( void ) Config;

    // disable systick timer and interrupt
    register_cortexm3_STK.CTRL.All = 0;

    return ( e_ttc_systick_errorcode ) 0;
}
t_base                   systick_cortexm3_get_elapsed_ticks() {
    Assert_SYSTICK( register_cortexm3_SCB.ICSR.VECTACTIVE == 0, ttc_assert_origin_auto );  // Do not call this function from an interrupt service routine! (use systick_cortexm3_get_elapsed_ticks_isr() instead!)
    Assert_SYSTICK( ttc_interrupt_critical_level() == 0, ttc_assert_origin_auto ); // counter is not running while interrupts are disabled. Exit critical section or call systick_cortexm3_get_elapsed_ticks_isr() instead!

#if (TTC_ASSERT_SYSTICK == 1) // check if systick counter is still running
    static t_base scget_TimeOut = -1;
    static t_base scget_Now     = 0;

    if ( scget_Now != systick_cortexm3_Ticks ) { // systick has changed: reset timeout
        scget_Now = systick_cortexm3_Ticks;
        scget_TimeOut = register_cortexm3_STK.LOAD.Bits.Reload;
    }
    else {                           // system clock value did not change since last call: check if clock has stopped
        if ( scget_TimeOut-- == 0 ) { // system tick clock does not seem to run. Check if interrupts are enabled!
            Assert_SYSTICK( register_cortexm3_STK.CTRL.Bits.Enable, ttc_assert_origin_auto );  // systick counter not enabled!
            Assert_SYSTICK( register_cortexm3_STK.LOAD.All, ttc_assert_origin_auto );         // no reload value >0 set!
            ttc_assert_halt_origin( ttc_assert_origin_auto ); // unkown reason (maybe interrupts are disabled?)
            systick_cortexm3_Ticks++; // we returned from assert for some reason: continue counter manually
        }
    }
#endif

    return ( t_base ) systick_cortexm3_Ticks;
}
t_base                   systick_cortexm3_get_elapsed_ticks_isr() {
    //(will hinder calling this function inside a critical section) Assert_SYSTICK( (register_cortexm3_SCB.ICSR.VECTACTIVE != 0) , ttc_assert_origin_auto ); // Only call this function from an interrupt service routine!

    if ( register_cortexm3_STK.CTRL.Bits.CountFlag ) { // counter did underrun since last check: handle underrun manually
#if  (TTC_ASSERT_SYSTICK == 1)
        SysTick_Handler();
#else
        systick_cortexm3_Ticks++; // faster than function call
#endif
    }

    return ( t_base ) systick_cortexm3_Ticks;
}
t_base                   systick_cortexm3_get_elapsed_usecs() {
    Assert_SYSTICK( register_cortexm3_SCB.ICSR.VECTACTIVE == 0, ttc_assert_origin_auto );  // Do not call this function from an interrupt service routine! (use systick_cortexm3_get_elapsed_ticks_isr() instead!)

    return ( t_base ) ttc_basic_multiply_divide_u32x3( systick_cortexm3_Ticks,
                                                       systick_cortexm3_NanoSecondsPerTick,
                                                       1000000
                                                     );
}
t_base                   systick_cortexm3_get_us_ticks( t_base Microseconds ) {

    // return Microseconds * 1000000 / systick_cortexm3_NanoSecondsPerTick;
    return ( t_base ) ttc_basic_multiply_divide_u32x3( Microseconds,
                                                       1000,
                                                       systick_cortexm3_NanoSecondsPerTick
                                                     );
}
e_ttc_systick_errorcode  systick_cortexm3_init( t_ttc_systick_config* Config ) {
    Assert_SYSTICK_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    systick_cortexm3_deinit( Config );
    systick_cortexm3_configuration_check( Config );

    t_u32 NanoSecondsPerTick;
    t_u32 MinimumDelayUS;

    // load calibration value for 1ms @HCLK max / 8
    t_u32 ReloadValue = register_cortexm3_STK.CALIB.Bits.TenMS;

    t_register_cortexm3_ctrl CTRL; CTRL.All = 0;
    switch ( Config->Profile ) { // configure systick frequency
        case profile_systick_Relaxed:
            register_cortexm3_STK.LOAD.Bits.Reload    = ( ReloadValue / 10 ) - 1;      // 100 us / systick step
            NanoSecondsPerTick                        = 100000; // 100 us @ 32Mhz SYSCLK
            MinimumDelayUS                            = 100;    // 1 systick count
            CTRL.Bits.ClkSource                       = 0;      // clock from Processor clock / 8 (smaller frequency = lower power consumption)
            break;
        case profile_systick_Precise:
            register_cortexm3_STK.LOAD.Bits.Reload    = ( ReloadValue * 8 / 1000 ) - 1; // 1 us / systick step
            NanoSecondsPerTick                        = 1000;   // 1 us @ 32Mhz SYSCLK
            MinimumDelayUS                            = 1;      // 1 systick count
            CTRL.Bits.ClkSource                       = 1;      // clock from Processor clock (higher frequency = more precise frequency adjustment)
            break;
        default:
            Config->Profile = profile_systick_Default;
            register_cortexm3_STK.LOAD.Bits.Reload    = ( ReloadValue / 100 ) - 1; // 10 us / systick step
            NanoSecondsPerTick                        = 10000;  // 10 us @ 32Mhz SYSCLK
            MinimumDelayUS                            = 10;     // 1 systick count
            CTRL.Bits.ClkSource                       = 0;      // clock from Processor clock / 8 (smaller frequency = lower power consumption)
            break;
    }

    // Calculate real period time for current system clock frequency.
    // Value of register_cortexm3_STK.CALIB is relative to maximum SYSCLK frequency.
    t_u32 SystemClock_kHz = ( ttc_sysclock_frequency_get() + 500 ) / 1000;
    const t_u32 SystemClock_kHz_Max = TTC_SYSCLOCK_FREQUENCY_MAX / 1000;
    NanoSecondsPerTick = ttc_basic_multiply_divide_u32x3( NanoSecondsPerTick, SystemClock_kHz_Max, SystemClock_kHz );
    MinimumDelayUS     = ttc_basic_multiply_divide_u32x3( MinimumDelayUS,     SystemClock_kHz_Max, SystemClock_kHz );

    // update configuration
    systick_cortexm3_NanoSecondsPerTick = NanoSecondsPerTick;
    Config->NanoSecondsPerSysTick = systick_cortexm3_NanoSecondsPerTick * 1000;
    Config->MinimumDelayUS        = MinimumDelayUS;
    Config->Architecture          = ta_systick_cortexm3;

    CTRL.Bits.TickInt   = 1;
    CTRL.Bits.Enable    = 1;

    // configure + enable systick timer
    register_cortexm3_STK.CTRL.All = CTRL.All;

    return ( e_ttc_systick_errorcode ) 0;
}
e_ttc_systick_errorcode  systick_cortexm3_load_defaults( t_ttc_systick_config* Config ) {
    Assert_SYSTICK_EXTRA_Writable( Config, ttc_assert_origin_auto );  // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = ta_systick_cortexm3;         // set type of architecture
    Config->Features     = &systick_cortexm3_Features;  // assign feature set

    if ( 0 ) { // enable for memory mapped devices (everything that has a base register)
        switch ( Config->PhysicalIndex ) { // load address of peripheral base register
            //case 0: Config->BaseRegister = & register_cortexm3_STK1; break;
            //case 1: Config->BaseRegister = & register_cortexm3_STK2; break;
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid physical index: check your makefile and high-level driver implementation!
        }
    }


    return ec_systick_OK; // will perform stack overflow check + return value
}
void                     systick_cortexm3_prepare() {
    systick_cortexm3_deinit( NULL );
}
void                     systick_cortexm3_reset( t_ttc_systick_config* Config ) {
    Assert_SYSTICK_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    systick_cortexm3_Ticks = 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
