#ifndef SYSTICK_CORTEXM3_H
#define SYSTICK_CORTEXM3_H

/** { systick_cortexm3.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_systick.h providing
 *  function prototypes being required by ttc_systick.h
 *
 *  system timer for precise time measures and periodice interrupt generation
 *
 *  Created from template device_architecture.h revision 28 at 20160926 13:55:21 UTC
 *
 *  Note: See ttc_systick.h for description of cortexm3 independent SYSTICK implementation.
 *
 *  Authors: Gregor Rebel
 *
 *
 *  How to configure cortexm3 systick devices
 *
 *  PLACE YOUR DESCRIPTION HERE!
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_SYSTICK_DRIVER_AVAILABLE
#define EXTENSION_SYSTICK_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_systick_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "systick_cortexm3.c"
//
#include "../ttc_systick_types.h" // will include systick_cortexm3_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_systick_interface.h
//
// Some driver functions may be optional to implement.
// Check ttc_systick_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_systick_foo
//
#define ttc_driver_systick_configuration_check systick_cortexm3_configuration_check
#define ttc_driver_systick_deinit              systick_cortexm3_deinit
#define ttc_driver_systick_init                systick_cortexm3_init
#define ttc_driver_systick_load_defaults       systick_cortexm3_load_defaults
#define ttc_driver_systick_prepare             systick_cortexm3_prepare
#define ttc_driver_systick_reset               systick_cortexm3_reset
#define ttc_driver_systick_get_elapsed_ticks   systick_cortexm3_get_elapsed_ticks
#define ttc_driver_systick_get_elapsed_usecs   systick_cortexm3_get_elapsed_usecs
#define ttc_driver_systick_get_us_ticks        systick_cortexm3_get_us_ticks
#define ttc_driver_systick_get_elapsed_ticks_isr systick_cortexm3_get_elapsed_ticks_isr
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_systick.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_systick.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_systick_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_systick_features.
 *
 * @param Config        Configuration of systick device
 * @return              Fields of *Config have been adjusted if necessary
 */
void systick_cortexm3_configuration_check( t_ttc_systick_config* Config );


/** shutdown single SYSTICK unit device
 * @param Config        Configuration of systick device
 * @return              == 0: SYSTICK has been shutdown successfully; != 0: error-code
 */
e_ttc_systick_errorcode systick_cortexm3_deinit( t_ttc_systick_config* Config );


/** initializes single SYSTICK unit for operation
 * @param Config        Configuration of systick device
 * @return              == 0: SYSTICK has been initialized successfully; != 0: error-code
 */
e_ttc_systick_errorcode systick_cortexm3_init( t_ttc_systick_config* Config );


/** loads configuration of indexed SYSTICK unit with default values
 * @param Config        Configuration of systick device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_systick_errorcode systick_cortexm3_load_defaults( t_ttc_systick_config* Config );


/** Prepares systick Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void systick_cortexm3_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of systick device
 */
void systick_cortexm3_reset( t_ttc_systick_config* Config );


/** returns current system clock value
 *
 * Note: Unit of returned value hardware dependent. Use ttc_systick_get_us_ticks() to convert
 * Note: Check configuration of your scheduler to achieve real world usecs! (E.g. FreeRTOSConfig.h:configTICK_RATE_HZ)
 * Note: See ttc_systick_delay_expired() for delays that work with multiple tasks in single- and multitasking setup!
 *
 * Example usage:
 *
 * // Need to store start time in case of variable overrun
 * t_base TimeNow = ttc_systick_get_elapsed_ticks();
 *
 * // Calculate wake up time 1 ms in the future in ticks (1 tick is NOT a microsecond)
 * t_base TimeWakeUp = TimeNow + ttc_systick_get_us_ticks(1000);
 *
 * if (TimeWakeUp < TimeNow) { // handle overrun
 *
 *   // wait until system counter overruns
 *   while (ttc_systick_get_elapsed_ticks() > TimeWakeUp)
 *     ttc_task_yield(); // give cpu to other tasks
 * }
 * while (ttc_systick_get_elapsed_ticks() < TimeWakeUp)
 *   ttc_task_yield(); // give cpu to other tasks
 *
 * 1ms has passed ...
 *
 * @return amount of counter events (ticks) since start of system/ scheduler (may have been overrun since last call!)
 */
t_base systick_cortexm3_get_elapsed_ticks();


/** returns current system clock time in microseconds
 *
 * Note: Every call to this function implies a multiplication. See ttc_systick_get_elapsed_ticks() for a faster implementation!
 * Note: Check configuration of current low-level driver to achieve real world usecs! (E.g. FreeRTOSConfig.h:configTICK_RATE_HZ)
 * Note: See ttc_systick_delay_expired() for delays that work with multiple tasks in single- and multitasking setup!
 *
 * @return amount of usecs since start of system/ scheduler (may have been overrun since last call!)
 */
t_base systick_cortexm3_get_elapsed_usecs();


/** Calculates amount of system clock ticks that correspond to 1 microseconds
 *
 * Using hardware ticks instead of microseconds allows to write faster delay functions:
 * 1) Calculate amount of ticks to pass that correspond to desired microseconds delay (requires multiplications)
 * 2) Wait until amount of calculated ticks has passed (no further multiplications required)
 *
 * Note: Check configuration of low-level driver to achieve real world usecs! (E.g. FreeRTOSConfig.h:configTICK_RATE_HZ)
 *
 * @param Microseconds (t_base) time period in microseconds
 * @return             (t_base) amount of counter events (ticks) since start of system/ scheduler
 */
t_base systick_cortexm3_get_us_ticks( t_base Microseconds );


/** Returns current system clock value for an interrupt service routine
 *
 * During an interrupt service routine, the systick counter may not be running.
 * This function will provide similar functionality as ttc_systick_get_elapsed_ticks().
 * Normally the only reason to know the system time is if you want to wait inside an
 * interrupt service routine. This "feature" was required during development of the radio_dw1000 driver.
 * This driver may use very long interrupt service routines with precise timeouts.
 *
 * @return amount of counter events (ticks) since start of system/ scheduler (may have been overrun since last call!)
 */
t_base systick_cortexm3_get_elapsed_ticks_isr();

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //SYSTICK_CORTEXM3_H
