/** systick_freertos.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_systick.c.
 *
 *  system timer for precise time measures and periodice interrupt generation
 *
 *  Created from template device_architecture.c revision 33 at 20160926 17:51:11 UTC
 *
 *  Note: See ttc_systick.h for description of high-level systick implementation.
 *  Note: See systick_freertos.h for description of freertos systick implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "systick_freertos.h".
 */

#include "systick_freertos.h"
#include "../ttc_memory.h"            // basic memory checks
#include "../ttc_task.h"              // stack checking (provides TTC_TASK_RETURN() macro)
#include "systick_common.h"          // generic functions shared by low-level drivers of all architectures
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_systick_features systick_freertos_Features = {
    /** example features
      .MinBitRate       = 4800,
      .MaxBitRate       = 230400,
    */
    // set more feature values...

    .unused = 0
};

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

e_ttc_systick_errorcode  systick_freertos_load_defaults( t_ttc_systick_config* Config ) {
    Assert_SYSTICK_EXTRA_Writable( Config, ttc_assert_origin_auto );  // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = ta_systick_freertos;         // set type of architecture
    Config->Features     = &systick_freertos_Features;  // assign feature set

    if ( 0 ) { // enable for memory mapped devices (everything that has a base register)
        switch ( Config->PhysicalIndex ) { // load address of peripheral base register
            //case 0: Config->BaseRegister = & register_freertos_SYSTICK1; break;
            //case 1: Config->BaseRegister = & register_freertos_SYSTICK2; break;
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid physical index: check your makefile and high-level driver implementation!
        }
    }

    // set individual feature flags
    Config->Flags.All                        = 0;
    //Config->Flags.Bits.Foo                   = 1;
    // add more flags...


    TTC_TASK_RETURN( ec_systick_OK ); // will perform stack overflow check + return value
}
void systick_freertos_configuration_check( t_ttc_systick_config* Config ) {
    Assert_SYSTICK_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)


}
e_ttc_systick_errorcode systick_freertos_deinit( t_ttc_systick_config* Config ) {
    Assert_SYSTICK_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)


    return ( e_ttc_systick_errorcode ) 0;
}
e_ttc_systick_errorcode systick_freertos_init( t_ttc_systick_config* Config ) {
    Assert_SYSTICK_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

    Config->Architecture          = ta_systick_freertos;
    Config->MinimumDelayUS        = 10 * 1000000 / configTICK_RATE_HZ; // at least ten times of tick period (to reduce overhead of function calls)
    Config->NanoSecondsPerSysTick = 1000000000 / configTICK_RATE_HZ;

    return ( e_ttc_systick_errorcode ) 0;
}
void systick_freertos_prepare() {


}
void systick_freertos_reset( t_ttc_systick_config* Config ) {
    Assert_SYSTICK_EXTRA_Writable( Config, ttc_assert_origin_auto );  // check if it points to RAM (change for read only memory!)

}
t_base systick_freertos_get_elapsed_ticks() {
    Assert_SYSTICK( ttc_task_is_scheduler_started(), ttc_assert_origin_auto ); // counter will only increment when scheduler is running!

#if TARGET_DATA_WIDTH >= 32 // direct access only allowed for 32-bit architectures
    return ( t_base ) xTickCount;
#else
    return xTaskGetTickCount();
#endif
}
t_base systick_freertos_get_elapsed_ticks_isr() {

    return xTaskGetTickCountFromISR();
}
t_base systick_freertos_get_elapsed_usecs() {

#if TARGET_DATA_WIDTH >= 32 // direct access only allowed for 32-bit architectures
    return ( t_base )( ( 1000000 / ( configTICK_RATE_HZ ) ) * xTickCount );
#else                       // slower but safer for smaller architectures
    return ( t_base )( ( 1000000 / ( configTICK_RATE_HZ ) ) * xTaskGetTickCount() );
#endif
}
t_base systick_freertos_get_us_ticks( t_base Microseconds ) {

    return ( t_base )( Microseconds / ( 1000000 / configTICK_RATE_HZ ) );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
