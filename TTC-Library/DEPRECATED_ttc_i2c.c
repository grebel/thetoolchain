/*{ ttc_i2c::.c ************************************************
 
                      The ToolChain
                      
   Device independent support for Inter-Integrated Circuit (I2C) Interface
   
   
   written by Gregor Rebel 2012
   
 
}*/

#include "DEPRECATED_ttc_i2c.h"

//{ Function definitions *************************************************


inline                u8_t ttc_i2c_get_max_index() {

    return TTC_AMOUNT_I2CS; // -> ttc_i2c_types.h
}
inline  ttc_i2c_config_t* ttc_i2c_get_configuration(u8_t I2C_Index) {

    Assert(I2C_Index > 0, ec_InvalidArgument);                // logical index starts at 1
    Assert(I2C_Index <= TTC_AMOUNT_I2CS, ec_InvalidArgument); // use ttc_i2c_get_max_index() to get max allowed value

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_i2c_get_configuration(I2C_Index);
#endif
#ifdef TARGET_ARCHITECTURE_STM32W1xx
    return stm32w_i2c_get_configuration(I2C_Index);
#endif
#ifdef TARGET_ARCHITECTURE_STM32L1xx
    return i2c_stm32l1_get_configuration(I2C_Index);
#endif

  return NULL;
}
inline ttc_i2c_errorcode_e ttc_i2c_get_defaults(u8_t I2C_Index, ttc_i2c_config_t* Config) {
    Assert(I2C_Index > 0, ec_InvalidArgument);                // logical index starts at 1
    Assert(I2C_Index <= TTC_AMOUNT_I2CS, ec_InvalidArgument); // use ttc_i2c_get_max_index() to get max allowed value

    memset(Config, 0, sizeof(Config));

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_i2c_get_defaults(I2C_Index, Config);
#endif
#ifdef TARGET_ARCHITECTURE_STM32L1xx
    return i2c_stm32l1_load_defaults(I2C_Index, Config);
#endif
  return tie_NotImplemented;
}
inline ttc_i2c_errorcode_e ttc_i2c_get_features(u8_t I2C_Index, ttc_i2c_config_t* Config) {
    Assert(I2C_Index > 0, ec_InvalidArgument);                // logical index starts at 1
    Assert(I2C_Index <= TTC_AMOUNT_I2CS, ec_InvalidArgument); // use ttc_i2c_get_max_index() to get max allowed value

    memset(Config, 0, sizeof(Config));
    
#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_i2c_get_features(I2C_Index, Config);
#endif
#ifdef TARGET_ARCHITECTURE_STM32L1xx
    return i2c_stm32l1_get_features(I2C_Index, Config);
#endif
  return tie_NotImplemented;
}
inline ttc_i2c_errorcode_e ttc_i2c_init(u8_t I2C_Index) {

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_i2c_init(I2C_Index);
#endif
#ifdef TARGET_ARCHITECTURE_STM32L1xx
    return i2c_stm32l1_init(I2C_Index);
#endif
    return tie_NotImplemented;
}
inline ttc_i2c_errorcode_e ttc_i2c_write_register(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, const char Byte) {

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_i2c_write_register(I2C_Index, TargetAddress, Register, RegisterType, Byte);
#endif
#ifdef TARGET_ARCHITECTURE_STM32L1xx
    return i2c_stm32l1_write_register(I2C_Index, TargetAddress, Register, RegisterType, Byte);
#endif
    return tie_NotImplemented;
}
inline ttc_i2c_errorcode_e ttc_i2c_write_registers(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, const u8_t* Buffer, u16_t Amount) {

#ifdef TARGET_ARCHITECTURE_STM32F1xx
  return stm32_i2c_write_registers(I2C_Index, TargetAddress, Register, RegisterType, Buffer, Amount);
#endif
#ifdef TARGET_ARCHITECTURE_STM32L1xx
    return i2c_stm32l1_write_registers(I2C_Index, TargetAddress, Register, RegisterType, Buffer, Amount);
#endif
  return tie_NotImplemented;
}
inline ttc_i2c_errorcode_e ttc_i2c_read_register(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, u8_t* Buffer) {

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_i2c_read_register(I2C_Index, TargetAddress, Register, RegisterType, Buffer);
#endif
#ifdef TARGET_ARCHITECTURE_STM32L1xx
    return i2c_stm32l1_read_register(I2C_Index, TargetAddress, Register, RegisterType, Buffer);
#endif
    return tie_NotImplemented;
}
inline ttc_i2c_errorcode_e ttc_i2c_read_registers(u8_t I2C_Index, const u16_t TargetAddress, const u32_t Register, ttc_i2c_register_type_e RegisterType, u8_t* Buffer, u16_t Amount, u16_t* AmountRead) {

#ifdef TARGET_ARCHITECTURE_STM32F1xx
    return stm32_i2c_read_registers(I2C_Index, TargetAddress, Register, RegisterType, Buffer, Amount, AmountRead);
#endif
#ifdef TARGET_ARCHITECTURE_STM32L1xx
    return i2c_stm32l1_read_registers(I2C_Index, TargetAddress, Register, RegisterType, Buffer, Amount, AmountRead);
#endif
    Assert(AmountRead != NULL, ec_NULL);
    *AmountRead = 0;
    return tie_NotImplemented;
}

//} Function definitions
