#ifndef LAYER_PHY_SPI_TYPES_H
#define LAYER_PHY_SPI_TYPES_H

/** { layer_phy_spi.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *                     
 *  Low-Level datatypes for LAYER_PHY devices on spi architectures.
 *  Structures, Enums and Defines being required by ttc_layer_phy_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150304 08:22:38 UTC
 *
 *  Note: See ttc_layer_phy.h for description of architecture independent LAYER_PHY implementation.
 * 
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_layer_phy_types.h *************************

//t_s8 nombre[]={valor_0, valor_1, valor_2};

typedef struct { // register description (adapt according to spi registers)
    unsigned Date_Rate         :2;
    unsigned Franme_Lenght     :7;
    unsigned Ranging_Packet    :1;
    unsigned Header_Extension  :1;
    unsigned Preamble_Durantion:2;
    unsigned SECDED_check_bits :6;
} t_phr;


typedef struct  { // register description (adapt according to spi registers)
    //t_s8 Index_1[]={-1,0,0,0};
} SHR_Preamble_31_ternary_codes;

typedef struct { // register description (adapt according to spi registers)
  unsigned Reserved2 : 16;
} t_shr;

typedef struct { // register description (adapt according to spi registers)
  unsigned Reserved1 : 16;
  unsigned Reserved2 : 16;
} t_sfd;

typedef struct { // register description (adapt according to spi registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} t_layer_phy_register;

typedef struct {  // spi specific configuration data of single LAYER_PHY device
  t_layer_phy_register* BaseRegister;       // base address of LAYER_PHY device registers
} __attribute__((__packed__)) t_layer_phy_spi_config;

// t_ttc_layer_phy_architecture is required by ttc_layer_phy_types.h
#define t_ttc_layer_phy_architecture t_layer_phy_spi_config

//} Structures/ Enums


#endif //LAYER_PHY_SPI_TYPES_H
