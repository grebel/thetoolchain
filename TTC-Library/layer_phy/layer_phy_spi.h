#ifndef LAYER_PHY_SPI_H
#define LAYER_PHY_SPI_H

/** { layer_phy_spi.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for layer_phy devices on spi architectures.
 *  Structures, Enums and Defines being required by high-level layer_phy and application.
 *
 *  Created from template device_architecture.h revision 22 at 20150304 08:22:38 UTC
 *
 *  Note: See ttc_layer_phy.h for description of spi independent LAYER_PHY implementation.
 *  
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_LAYER_PHY_SPI
//
// Implementation status of low-level driver support for layer_phy devices on spi
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_LAYER_PHY_SPI '?'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_LAYER_PHY_SPI == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_LAYER_PHY_SPI to one from '-', 'o', '+' 
#endif

//}EXTENSION_STATUS_TTC_LAYER_PHY_SPI

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "layer_phy_spi.c"
//
#include "layer_phy_spi_types.h"
#include "../ttc_layer_phy_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_layer_phy_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_layer_phy_foo
//
#define ttc_driver_layer_phy_deinit(Config) layer_phy_spi_deinit(Config)
#define ttc_driver_layer_phy_get_features(Config) layer_phy_spi_get_features(Config)
#define ttc_driver_layer_phy_init(Config) layer_phy_spi_init(Config)
#define ttc_driver_layer_phy_load_defaults(Config) layer_phy_spi_load_defaults(Config)
#define ttc_driver_layer_phy_prepare() layer_phy_spi_prepare()
#define ttc_driver_layer_phy_reset(Config) layer_phy_spi_reset(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_layer_phy.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_layer_phy.h for details.
 *
 * You normally should not need to add functions here. 
 * Add your private functions in the private section below. 
 */


/** shutdown single LAYER_PHY unit device
 * @param Config        pointer to struct t_ttc_layer_phy_config (must have valid value for PhysicalIndex)
 * @return              == 0: LAYER_PHY has been shutdown successfully; != 0: error-code
 */
e_ttc_layer_phy_errorcode layer_phy_spi_deinit(t_ttc_layer_phy_config* Config);


/** fills out given Config with maximum valid values for indexed LAYER_PHY
 * @param Config        = pointer to struct t_ttc_layer_phy_config (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_layer_phy_errorcode layer_phy_spi_get_features(t_ttc_layer_phy_config* Config);


/** initializes single LAYER_PHY unit for operation
 * @param Config        pointer to struct t_ttc_layer_phy_config (must have valid value for PhysicalIndex)
 * @return              == 0: LAYER_PHY has been initialized successfully; != 0: error-code
 */
e_ttc_layer_phy_errorcode layer_phy_spi_init(t_ttc_layer_phy_config* Config);


/** loads configuration of indexed LAYER_PHY unit with default values
 * @param Config        pointer to struct t_ttc_layer_phy_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_layer_phy_errorcode layer_phy_spi_load_defaults(t_ttc_layer_phy_config* Config);


/** Prepares layer_phy Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void layer_phy_spi_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_layer_phy_config (must have valid value for PhysicalIndex)
 */
void layer_phy_spi_reset(t_ttc_layer_phy_config* Config);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _layer_phy_spi_foo(t_ttc_layer_phy_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //LAYER_PHY_SPI_H