/** { layer_phy_spi.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for layer_phy devices on spi architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150304 08:22:38 UTC
 *
 *  Note: See ttc_layer_phy.h for description of spi independent LAYER_PHY implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "layer_phy_spi.h".
//
#include "layer_phy_spi.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

e_ttc_layer_phy_errorcode layer_phy_spi_deinit(t_ttc_layer_phy_config* Config) {
    Assert_LAYER_PHY(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function layer_phy_spi_deinit() is empty.
    
    return (e_ttc_layer_phy_errorcode) 0;
}
e_ttc_layer_phy_errorcode layer_phy_spi_get_features(t_ttc_layer_phy_config* Config) {
    Assert_LAYER_PHY(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function layer_phy_spi_get_features() is empty.
    
    return (e_ttc_layer_phy_errorcode) 0;
}
e_ttc_layer_phy_errorcode layer_phy_spi_init(t_ttc_layer_phy_config* Config) {
    Assert_LAYER_PHY(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function layer_phy_spi_init() is empty.
    
    return (e_ttc_layer_phy_errorcode) 0;
}
e_ttc_layer_phy_errorcode layer_phy_spi_load_defaults(t_ttc_layer_phy_config* Config) {
    Assert_LAYER_PHY(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function layer_phy_spi_load_defaults() is empty.
    
    return (e_ttc_layer_phy_errorcode) 0;
}
void layer_phy_spi_prepare() {


#warning Function layer_phy_spi_prepare() is empty.
    

}
void layer_phy_spi_reset(t_ttc_layer_phy_config* Config) {
    Assert_LAYER_PHY(Config, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function layer_phy_spi_reset() is empty.
    

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
