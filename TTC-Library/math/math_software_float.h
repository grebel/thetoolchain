#ifndef MATH_SOFTWARE_FLOAT_H
#define MATH_SOFTWARE_FLOAT_H

/** { math_software_float.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for mathematic operations using software algorithms in
 *  single floating point precision.
 *  This source contains only datatypes. The implementation can be found in
 *  ttc_math_interface.c.
 *
 *  Authors: Gregor Rebel
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_MATH_DRIVER_AVAILABLE
#define EXTENSION_MATH_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_SOFTWARE_DOUBLE_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_MATH_SOFTWARE_FLOAT
//
// Implementation status of low-level driver support for math devices on software_float
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_MATH_SOFTWARE_FLOAT '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_MATH_SOFTWARE_FLOAT == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_MATH_SOFTWARE_FLOAT to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_MATH_SOFTWARE_FLOAT

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "math_software_float.c"
//
#include "../ttc_math_types.h" // will include math_software_float_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_math_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_math_foo
//

// using default implementation in interface
#define ttc_driver_math_sqrt math_software_float_sqrt
#define ttc_driver_math_abs  math_software_float_abs
#define ttc_driver_math_prepare math_software_float_prepare

// undefine functions not beinig implemented in this low-level driver
#undef ttc_driver_math_cartesian2polar
#undef ttc_driver_math_polar2cartesian
#undef ttc_driver_math_arccos
#undef ttc_driver_math_arcsin
#undef ttc_driver_math_arctan
#undef ttc_driver_math_cos
#undef ttc_driver_math_sin
#undef ttc_driver_math_tan
#undef ttc_driver_math_pow
#undef ttc_driver_math_angle_reduce
#undef ttc_driver_math_modulo
#undef ttc_driver_math_log_int
#undef ttc_driver_math_int_rank2
#undef ttc_driver_math_int_rankN
#undef ttc_driver_math_inv_sqrt
#undef ttc_driver_math_vector2d_angle
#undef ttc_driver_math_to_int
#undef ttc_driver_math_to_double
#undef ttc_driver_math_to_float
#undef ttc_driver_math_from_int
#undef ttc_driver_math_from_double
#undef ttc_driver_math_from_float
#undef ttc_driver_math_distance_2d
#undef ttc_driver_math_vector3d_distance
#undef ttc_driver_math_length_2d
#undef ttc_driver_math_vector3d_length
#undef ttc_driver_math_intersection_2d
#undef ttc_driver_math_vector2d_incline
#undef ttc_driver_math_length_3d
#define ttc_driver_math_vector3d_valid math_software_float_vector3d_valid
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_math.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_math.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Prepares math Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void math_software_float_prepare();


/** Calculates inverse cosinus of given argument
 *
 * @param Precision (t_u8)        amount of taylor polynoms to calculate (1 = fast ... 10 = precise)
 * @param X         (ttm_number)  argument to calculate arcus cosinus for
 * @return          (ttm_number)  arccos(X) at desired precision
 */
ttm_number math_software_float_arccos( t_u8 Precision, ttm_number X );


/** Calculates inverse sinus of given argument
 *
 * @param Precision (t_u8)        amount of taylor polynoms to calculate (1 = fast ... 10 = precise)
 * @param X         (ttm_number)  argument to calculate arcus sinus for
 * @return          (ttm_number)  arcsin(X) at desired precision
 */
ttm_number math_software_float_arcsin( t_u8 Precision, ttm_number X );


/** Calculate inverse of square root of X
 *
 * @param X (ttm_number)  argument to function
 * @return  (ttm_number)  1 / sqrt(X)
 */
ttm_number math_software_float_inv_sqrt( ttm_number X );


/** Calculate square root of X
 *
 * @param X (ttm_number)  argument to function
 * @return  (ttm_number)  sqrt(X)
 */
ttm_number math_software_float_sqrt( ttm_number X );




/** Return absolute positive value of given positive or negative value
 *
 * @param X (ttm_number)  positive or negativ value
 * @return  (ttm_number)  abs(X)
 */
ttm_number math_software_float_abs( ttm_number X );


/** ADD_SHORT_DESCRIPTION_HERE
*
 * ADD_MORE_DESCRIPTION_HERE
 *
 * @param Line1_Point   (t_ttc_math_vector2d_xy*)
 * @param Line1_Incline (t_ttc_math_vector2d_xy*)
 * @param Line2_Point   (t_ttc_math_vector2d_xy*)
 * @param Line2_Incline (t_ttc_math_vector2d_xy*)
 * @param Intersection  (t_ttc_math_vector2d_xy*)
 */
void math_software_float_intersection_2d( t_ttc_math_vector2d_xy* Line1_Point, t_ttc_math_vector2d_xy* Line1_Incline, t_ttc_math_vector2d_xy* Line2_Point, t_ttc_math_vector2d_xy* Line2_Incline, t_ttc_math_vector2d_xy* Intersection );


/** Calculate length of 3D vector
 *
 * @param X (ttm_number)  x-coordinate of 3D vector
 * @param Y (ttm_number)  y-coordinate of 3D vector
 * @param Z (ttm_number)  z-coordinate of 3D vector
 * @return  (ttm_number)  length of given vector
 * @param DistanceX   =
 * @param DistanceY   =
 * @param DistanceZ   =
 */
ttm_number math_software_float_length_3d( ttm_number DistanceX, ttm_number DistanceY, ttm_number DistanceZ );


/** ADD_SHORT_DESCRIPTION_HERE
*
 * ADD_MORE_DESCRIPTION_HERE
 *
 * @param A (t_ttc_math_vector2d_xy*)
 * @param B (t_ttc_math_vector2d_xy*)
 * @return  (ttm_number)
 */
ttm_number math_software_float_vector2d_incline( t_ttc_math_vector2d_xy* A, t_ttc_math_vector2d_xy* B );


/** Checks all coordinates of given vector are valid (!= TTC_MATH_CONST_NAN)
 *
 * @param Vector (t_ttc_math_vector3d_xyz*)  vector to check
 * @return       (BOOL)                      ==TRUE: Vector->X, Vector->Y and Vector->Z are valid numbers
 */
BOOL math_software_float_vector3d_valid( const t_ttc_math_vector3d_xyz* Vector );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //MATH_SOFTWARE_FLOAT_H
