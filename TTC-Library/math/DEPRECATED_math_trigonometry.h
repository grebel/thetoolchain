/*{ math_trigonometry.h ************************************************

  SOME TEXT

}*/

//{ Includes *************************************************************

#include "ttc_memory.h"
//} Includes

//{ Defines/ TypeDefs ****************************************************

#define PI 3.141592653589793

//} Defines

//{ Structures/ Enums ****************************************************
//} Structures/ Enums

//{ Global Variables *****************************************************
//} Global Variables

//{ Function prototypes **************************************************

double math_sin( double x );
double math_cos( double x );

/** calculates inverse of tan(x)
  *
  * @param X    input
  * @return     arcus tangens of x
  */
double math_arctan( double x );

/** converts from cartesia- to polar-coordinates
  *
  * @param X      cartesian X-coordinate
  * @param Y      cartesian Y-coordinate
  * @param Angle  polar Angle (output)
  * @param Length polar Length (output)
*/
void math_cartesian2polar( double X, double Y, double* Angle, double* Length );

/** converts from polar- to cartesia-coordinates
  *
  * @param Angle  polar Angle
  * @param Length polar Length
  * @param X      cartesian X-coordinate (output)
  * @param Y      cartesian Y-coordinate (output)
*/
void math_polar2cartesian( double Angle, double Length, double* X, double* Y );

//} Function prototypes
