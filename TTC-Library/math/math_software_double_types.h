#ifndef MATH_SOFTWARE_DOUBLE_TYPES_H
#define MATH_SOFTWARE_DOUBLE_TYPES_H

/** { math_software_double.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for MATH devices on software_double architectures.
 *  Structures, Enums and Defines being required by ttc_math_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150724 10:58:07 UTC
 *
 *  Note: See ttc_math.h for description of architecture independent MATH implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs *******************************************************

#ifndef TTC_MATH1   // device not defined in makefile
    #define TTC_MATH1    ta_math_software_double   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes ****************************************************************

#include "../ttc_basic_types.h"
#include <float.h>
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_math_types.h **************************


typedef struct { // msf_number_t structure of double precision floating points according to IEEE754 binary64
    unsigned FractionL: 32; // low part of fraction (complete fraction is too big for one unsigned)
    unsigned FractionH: 20; // high part of fraction
    unsigned Exponent : 11; // exponent for Base B=2
    unsigned Sign     :  1; // Sign = 0: Value >= 0; Sign = 1: Value < 0
} t_math_double;

extern double math_double_NAN; // defined in math_double.c


// ttm_number is base data format and required by ttc_math_types.h
#define ttm_number                    double                              // basic datatype for all floating/ fixed point ttc_math_*() functions
#define ttm_number_t                  t_math_double                       // structure of underlying format (only usable with pointers!)
#define TTC_MATH_CONST_MAX_VALUE      (double)  DBL_MAX                   // maximum positive value (->https://en.wikipedia.org/wiki/Double-precision_floating-point_format)
#define TTC_MATH_CONST_MIN_VALUE      (double) -DBL_MAX                   // maximum negative value
#define TTC_MATH_CONST_NAN            math_double_NAN                      // not a number
#define TTC_MATH_CONST_PLUS_INFINITY  (double)  1.0/0.0                   // cheap approximation for +infinity
#define TTC_MATH_CONST_MINUS_INFINITY (double) -1.0/0.0                   // cheap approximation for -infinity
#define TTC_MATH_CONST_SMALLEST       (double)  DBL_MIN //1.7749261414927983e-308   // smallest positive value

//} Structures/ Enums


#endif //MATH_SOFTWARE_DOUBLE_TYPES_H
