/** math_software_double.c ***************************************{
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for mathematic operations using software algorithms in
 *  double floating point precision.
 *  This source contains only datatypes. The implementation can be found in
 *  ttc_math_interface.c.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "math_software_double.h".
 */

#include "math_software_double.h"
#include "../ttc_memory.h"        // basic memory checks
#include "math_common.h"          // generic functions shared by low-level drivers of all architectures
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

extern e_ttc_math_precision ttc_math_Precision;
extern ttm_number           ttc_math_PrecisionValue;
//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

double math_double_NAN; // stores not a number value (not possible to use a constant)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */
ttm_number math_software_double_sqrt( ttm_number X ) {
    Assert_MATH( X >= 0 , ttc_assert_origin_auto); // must be a positive value

    ttm_number SQRT;

    if ( 1 ) { // using Heron method for floating point numbers based on 2-exponent
        // -> https://en.wikipedia.org/wiki/Methods_of_computing_square_roots

        /* Benchmark results (source code: example_ttc_math.c 'benchmark square root')
         *
         * Compiler: arm-none-eabi-gcc 5.3.1 20160307 (release)
         * All Assert_*() enabled
         * Reported time was measured for 1000 function calls including for-loop and divided by 1000.
         *
         * + STM32F107 @72MHz (Protoboard Olimex STM32-H107)
         *   + Low-Level Driver: math_software_double
         *     - 55.0us  (gcc -O0)
         *     - 52.0us  (gcc -Os)
         *     - 52.0us  (gcc -O9)
         */

        ttm_number_t* X_t = ( ttm_number_t* ) &X;
        const t_u16 BIAS = 1023; // IEEE 754-1989 exponent bias value (->https://de.wikipedia.org/wiki/IEEE_754#Arithmetik_und_Quadratwurzel)

        // reduce exponent to 1 or 0 (scales X into [0.5, 2] where sqrt() can be linearly approximated)
        t_s16 ExponentCorrection = X_t->Exponent - BIAS; // Subtract exponent bias
        if ( ExponentCorrection & 1 ) {
            ExponentCorrection --;    // subtracted exponent value must be even
            X_t->Exponent = 1 + BIAS;  // Exponent = 1
        }
        else {
            X_t->Exponent = 0 + BIAS;  // Exponent = 0
        }

        if ( 1 ) { // use linear sqrt-approximation to calculate starting point
            // A good starting point increases precision or reduces amount of required iterations

            if ( X < 1 ) { // x € [0.5, 1[: g(x) = (2 - sqrt(2)) x + sqrt(2) - 1
                SQRT = 0.585786437627 * X + 0.414213562373;
            }
            else         { // x € [1, 2]: g(x) = (sqrt(2) - 1) x + 2 - sqrt(2)
                SQRT = 0.414213562373 * X + 0.585786437627;
            }
        }

        // three iterations should be exact egnough
        SQRT = ( SQRT + ( X / SQRT ) ) / 2;
        SQRT = ( SQRT + ( X / SQRT ) ) / 2;
        SQRT = ( SQRT + ( X / SQRT ) ) / 2;
        if ( ttc_math_Precision >= tmp_1m ) // add another step for improved precision
        { SQRT = ( SQRT + ( X / SQRT ) ) / 2; }

        // correct exponent to get real result
        ttm_number_t* SQRT_t = ( ttm_number_t* ) &SQRT;
        SQRT_t->Exponent += ExponentCorrection >> 1;
    }

    return SQRT;
}
ttm_number math_software_double_abs( ttm_number X ) {
    t_math_double* Double = ( t_math_double* ) &X;
    Double->Sign = 0; // clear sign bit

    return X;
}
void       math_software_double_prepare() {

    // we have to use a trick to load this special value (using a constant define did not work)
    *(   (  ( t_u32* ) &math_double_NAN  ) + 0  ) = 0x7ff00001;
    *(   (  ( t_u32* ) &math_double_NAN  ) + 1  ) = 1;

}
BOOL       math_software_double_vector3d_valid(const t_ttc_math_vector3d_xyz* Vector) {
    Assert_MATH_Readable( Vector, ttc_assert_origin_auto ); // always check incoming pointer arguments

    // treat each double as two 32 bit values
    return (  *(  ( (t_u32*) &(Vector->X) ) + 0  ) != *(  ( (t_u32*) &(TTC_MATH_CONST_NAN) ) + 0 )  ) &&
           (  *(  ( (t_u32*) &(Vector->X) ) + 1  ) != *(  ( (t_u32*) &(TTC_MATH_CONST_NAN) ) + 1 )  ) &&
           (  *(  ( (t_u32*) &(Vector->Y) ) + 0  ) != *(  ( (t_u32*) &(TTC_MATH_CONST_NAN) ) + 0 )  ) &&
           (  *(  ( (t_u32*) &(Vector->Y) ) + 1  ) != *(  ( (t_u32*) &(TTC_MATH_CONST_NAN) ) + 1 )  ) &&
           (  *(  ( (t_u32*) &(Vector->Z) ) + 0  ) != *(  ( (t_u32*) &(TTC_MATH_CONST_NAN) ) + 0 )  ) &&
           (  *(  ( (t_u32*) &(Vector->Z) ) + 1  ) != *(  ( (t_u32*) &(TTC_MATH_CONST_NAN) ) + 1 )  );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
