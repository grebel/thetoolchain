/*{ math_trigonometry.c ******************************* ToDo: replace all float operations by integer ones!



}*/
//{ Defines/ TypDefs *****************************************************
//} Defines

//{ Includes *************************************************************

#include "../ttc_basic.h"
#include "math_trigonometry.h"

//} Includes

//{ Global Variables *****************************************************
//} Global Variables

//{ Function prototypes **************************************************
//} Function prototypes

static double math_pow(double base, u8_t exp);
static double math_reduceAngle(double a);
static double math_modulus(double a, double b);

//{ Function definitions *************************************************

void math_cartesian2polar(double X, double Y, double* Angle, double* Length) {
    Assert(ttc_memory_is_writable(Angle), ec_NULL);
    Assert(ttc_memory_is_writable(Length), ec_NULL);

    *Angle = math_arctan(1.0 * Y / X);
    *Length = X/math_cos(*Angle);
}
void math_polar2cartesian(double Angle, double Length, double* X, double* Y) {
    Assert(ttc_memory_is_writable(X), ec_NULL);
    Assert(ttc_memory_is_writable(Y), ec_NULL);

    *X = Length * math_cos(Angle);
    *Y = Length * math_sin(Angle);
}
double math_arctan(double x) {

    // taylor series of arctan(x)
    return x - math_pow(x,3) / 3 + math_pow(x,5) / 5;
}
double math_sin(double x) {
    double y = 0;

    x = math_reduceAngle(x);

    // taylor series of sin
    y = x - math_pow(x,3)/6 + math_pow(x,5)/120;

    return y;
}
double math_cos(double x) {
    // cos(x) = sin( x+(PI/2) )
    return math_sin(x+(PI/2.0));
}
static double math_pow( double base,  u8_t exp) {
    double result = 1;

    if(exp == 0) return 0;
    if(base == 0) return 0;

    while (exp)
      {
          if (exp & 1)
              result *= base;
          exp >>= 1;
          base *= base;
      }
    return result;
}
static inline double math_reduceAngle(double a) {

    a = math_modulus(a, 2.0*PI);

    if( a > PI)
        a -=  (2.0 * PI);
    else if( a < -PI)
        a += (2.0 * PI);

    if (a > (PI / 2))
        a = PI - a;
    else if (a < -(PI / 2))
        a = -PI - a;

    return a;
}
static inline double math_modulus(double a, double b) {
    int result = (int)( a / b );
    return a - (double)(result) * b;
}

//}FunctionDefinitions
