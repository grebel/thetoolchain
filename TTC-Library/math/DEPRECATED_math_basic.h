/*{ math_basic.h ************************************************

  SOME TEXT

}*/

//{ Includes *************************************************************

#include "ttc_basic.h"

//} Includes
//{ Defines/ TypeDefs ****************************************************

//} Defines
//{ Macros ***************************************************************

Base_t AssertNotZero( Base_t Value );

#ifndef TTC_ASSERT_MATH    // any previous definition set (Makefile)?
#define TTC_ASSERT_MATH 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_MATH == 1)  // use Assert()s in MATH code (somewhat slower but alot easier to debug)
#define Assert_MATH(Condition, ErrorCode) Assert(Condition, ErrorCode)
#define Assert_NOT_ZERO(VALUE) AssertNotZero( (Base_t) VALUE)
#else  // us no Assert()s in MATH code (somewhat smaller + faster, but crashes are hard to debug)
#define Assert_MATH(Condition, ErrorCode)
#define Assert_NOT_ZERO(VALUE) VALUE
#endif

/** replacement for division Divident / Divisor (asserts if DIVISOR == 0)
  *
  * @param DIVIDENT
  * @param DIVISOR
  * @return DIVISOR!=0: DIVIDENT / DIVISOR
 */
#ifndef DIV
#define DIV(DIVIDENT,DIVISOR) ( DIVIDENT / Assert_NOT_ZERO(DIVISOR) )
#else
#warning DIV() already defined!
#endif

/** returns smallest of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @return  either A or B
  */
#ifndef MIN
#define MIN(A,B) ( (A < B) ? A : B )
#else
#warning MIN() already defined!
#endif

/** returns smallest of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @param C  any type of signed/ unsigned number
  * @param D  any type of signed/ unsigned number
  * @return  either A, B, C or D
  */
#ifndef MIN4
#define MIN4(A,B,C,D) MIN( MIN(A,B), MIN(C,D) )
#else
#warning MIN4() already defined!
#endif

/** returns number with smallest absolute value of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @return  either A or B
  */
#ifndef MIN_ABS
#define MIN_ABS(A,B) ( (ABS(A) < ABS(B)) ? A : B )
#else
#warning MIN_ABS() already defined!
#endif

/** returns number with smallest absolute value of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @param C  any type of signed/ unsigned number
  * @param D  any type of signed/ unsigned number
  * @return  either A, B, C or D
  */
#ifndef MIN_ABS4
#define MIN_ABS4(A,B,C,D) MIN_ABS( MIN_ABS(A,B), MIN_ABS(C,D) )
#else
#warning MIN_ABS4() already defined!
#endif

/** returns biggest of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @return  either A or B
  */
#ifndef MAX
#define MAX(A,B) ( (A > B) ? A : B )
#else
#warning MAX() already defined!
#endif

/** returns biggest of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @param C  any type of signed/ unsigned number
  * @param D  any type of signed/ unsigned number
  * @return  either A, B, C or D
  */
#ifndef MAX4
#define MAX4(A,B,C,D) MAX( MAX(A,B), MAX(C,D) )
#else
#warning MAX4() already defined!
#endif

/** returns number with biggest absolute value of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @return  either A or B
  */
#ifndef MAX_ABS
#define MAX_ABS(A,B) ( (ABS(A) > ABS(B)) ? A : B )
#else
#warning MAX_ABS() already defined!
#endif

/** returns number with biggest absolute value of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @param C  any type of signed/ unsigned number
  * @param D  any type of signed/ unsigned number
  * @return  either A, B, C or D
  */
#ifndef MAX_ABS4
#define MAX_ABS4(A,B,C,D) MAX_ABS( MAX_ABS(A,B), MAX_ABS(C,D) )
#else
#warning MAX_ABS4() already defined!
#endif

//} Macros
//{ Structures/ Enums ****************************************************
//} Structures/ Enums
//{ Global Variables *****************************************************
//} Global Variables
//{ Function prototypes **************************************************


//} Function prototypes
