#ifndef MATH_SOFTWARE_FLOAT_TYPES_H
#define MATH_SOFTWARE_FLOAT_TYPES_H

/** { math_software_float.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for MATH devices on software_float architectures.
 *  Structures, Enums and Defines being required by ttc_math_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150724 10:58:07 UTC
 *
 *  Note: See ttc_math.h for description of architecture independent MATH implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_MATH1   // device not defined in makefile
    #define TTC_MATH1    ta_math_software_float   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include <float.h>
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_math_types.h *************************


typedef struct { // msf_number_t structure of single precision float according to IEEE754 binary32
    unsigned Fraction : 23;
    unsigned Exponent :  8;
    unsigned Sign     :  1;
} t_math_float;

extern float math_float_NAN; // defined in math_float.c

// ttm_number is base data format and required by ttc_math_types.h
#define ttm_number                    float             // basic datatype for all floating/ fixed point ttc_math_*() functions
#define ttm_number_t                  t_math_float      // structure of underlying format (only usable with pointers!)
#define TTC_MATH_CONST_MAX_VALUE      (float)  DBL_MAX  // maximum positive value
#define TTC_MATH_CONST_MIN_VALUE      (float) -DBL_MAX  // maximum negative value
#define TTC_MATH_CONST_NAN            math_float_NAN    // not a number (better than: (float)  0.0/0.0  // not a number)
#define TTC_MATH_CONST_PLUS_INFINITY  (float)  1.0/0.0  // cheap approximation for +infinity
#define TTC_MATH_CONST_MINUS_INFINITY (float) -1.0/0.0  // cheap approximation for -infinity
#define TTC_MATH_CONST_SMALLEST       (float) DBL_MIN   //1.40129846e-45    // smallest positive value
//} Structures/ Enums


#endif //MATH_SOFTWARE_FLOAT_TYPES_H
