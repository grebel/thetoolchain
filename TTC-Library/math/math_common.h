#ifndef math_common_h
#define math_common_h

/** math_common.h *****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to math low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.h revision 12 at 20150724 10:58:07 UTC
 *
 *  Authors: Gregor Rebel
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_MATH_COMMON
//
// Implementation status of low-level driver support for math devices on common
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_MATH_COMMON '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_MATH_COMMON == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_MATH_COMMON to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_MATH_COMMON

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "math_software_float.c"
 */

#include "../ttc_math_types.h" // will include math_software_float_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_math_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_math_foo
 */

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

t_base AssertNotZero( t_base Value );

#ifndef TTC_ASSERT_MATH    // any previous definition set (Makefile)?
    #define TTC_ASSERT_MATH 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_MATH == 1)  // use Assert()s in MATH code (somewhat slower but alot easier to debug)
    #define Assert_NOT_ZERO(VALUE) AssertNotZero( (t_base) VALUE)
#else  // us no Assert()s in MATH code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_NOT_ZERO(VALUE) VALUE
#endif

/** replacement for division Divident / Divisor (asserts if DIVISOR == 0)
  *
  * @param DIVIDENT
  * @param DIVISOR
  * @return DIVISOR!=0: DIVIDENT / DIVISOR
 */
#ifndef DIV
    #define DIV(DIVIDENT,DIVISOR) ( DIVIDENT / Assert_NOT_ZERO(DIVISOR) )
#else
    #warning DIV() already defined!
#endif

/** returns smallest of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @return  either A or B
  */
#ifndef MIN
    #define MIN(A,B) ( (A < B) ? A : B )
#else
    #warning MIN() already defined!
#endif

/** returns smallest of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @param C  any type of signed/ unsigned number
  * @param D  any type of signed/ unsigned number
  * @return  either A, B, C or D
  */
#ifndef MIN4
    #define MIN4(A,B,C,D) MIN( MIN(A,B), MIN(C,D) )
#else
    #warning MIN4() already defined!
#endif

/** returns number with smallest absolute value of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @return  either A or B
  */
#ifndef MIN_ABS
    #define MIN_ABS(A,B) ( (ABS(A) < ABS(B)) ? A : B )
#else
    #warning MIN_ABS() already defined!
#endif

/** returns number with smallest absolute value of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @param C  any type of signed/ unsigned number
  * @param D  any type of signed/ unsigned number
  * @return  either A, B, C or D
  */
#ifndef MIN_ABS4
    #define MIN_ABS4(A,B,C,D) MIN_ABS( MIN_ABS(A,B), MIN_ABS(C,D) )
#else
    #warning MIN_ABS4() already defined!
#endif

/** returns biggest of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @return  either A or B
  */
#ifndef MAX
    #define MAX(A,B) ( (A > B) ? A : B )
#else
    #warning MAX() already defined!
#endif

/** returns biggest of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @param C  any type of signed/ unsigned number
  * @param D  any type of signed/ unsigned number
  * @return  either A, B, C or D
  */
#ifndef MAX4
    #define MAX4(A,B,C,D) MAX( MAX(A,B), MAX(C,D) )
#else
    #warning MAX4() already defined!
#endif

/** returns number with biggest absolute value of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @return  either A or B
  */
#ifndef MAX_ABS
    #define MAX_ABS(A,B) ( (ABS(A) > ABS(B)) ? A : B )
#else
    #warning MAX_ABS() already defined!
#endif

/** returns number with biggest absolute value of given numbers
  *
  * @param A  any type of signed/ unsigned number
  * @param B  any type of signed/ unsigned number
  * @param C  any type of signed/ unsigned number
  * @param D  any type of signed/ unsigned number
  * @return  either A, B, C or D
  */
#ifndef MAX_ABS4
    #define MAX_ABS4(A,B,C,D) MAX_ABS( MAX_ABS(A,B), MAX_ABS(C,D) )
#else
    #warning MAX_ABS4() already defined!
#endif

//}MacroDefinitions
/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_math.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_math.h for details.
 *
 * You normally should not need to add functions here.
 * Global feature functions must be added in ttc_math.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl math UPDATE".
 */

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //math_common_h
