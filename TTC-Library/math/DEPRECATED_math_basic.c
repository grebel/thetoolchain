/** { math_basic.c *******************************
 *
 * The ToolChain
 *
 * written by Gregor Rebel 2013
 *
 * Basic mathematical functions and macros.
 *
}*/
//{ Defines/ TypDefs *****************************************************
//} Defines

//{ Includes *************************************************************

#include "math_basic.h"

//} Includes

//{ Global Variables *****************************************************
//} Global Variables

//{ Function prototypes **************************************************
//} Function prototypes


//{ Function definitions *************************************************

Base_t AssertNotZero(Base_t Value) {
  Assert(Value, ec_NULL);
  return Value;
}

//}FunctionDefinitions
