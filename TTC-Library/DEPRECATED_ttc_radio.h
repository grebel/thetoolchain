#ifndef TTC_RADIO_H
#define TTC_RADIO_H

/*{ ttc_radio.h **********************************************************
 
                      The ToolChain
                      
   Device independent support for 
   radio transceivers
   
   Currently implemented architectures: cc1101_spi

   
   written by Gregor Rebel 2012
   

   How it works
   
   Each makefile.100_board* can define one or more radios being connected.
   This is done via lines of this type:
     COMPILE_OPTS += -D<OPTION>=<VALUE>
   This sets compile option <OPTION> to value <VALUE> just as if a define directive
   was used in the source code like this:
     #define <OPTION> <VALUE>
   
   TTC_RADIOn are enumerated starting at indices 1..5 (e.g. TTC_RADIO1, TTC_RADIO2, ..).
   Each RADIOn can be configured via these constants:
   TTC_RADIOn                <RADIO>                sets RADIO driver to use (one from ttc_radio_types.h/ttc_radio_driver_e)
   TTC_RADIOn_SPI_INDEX
   TTC_RADIOn_USART          <USART>                sets internal USART device to use (TTC_USART1, ...)
   TTC_RADIOn_USART_BITRATE  <BITRATE>              USART bitrate to use
   
   Note: Instead of  architecture dependent "<GPIO_BANK>,<PIN_NO>", the PIN_P<x><n> macros 
         should be used. These are defined for each uC architecture.
   Example: "GPIOC,7" can be given as "PIN_PC7"


   Radio specific configuration

   Individual radio drivers will require additional configuration such as certain gpio pins or other settings.
   Have a look at the driver sources and examples provided for the individual driver.

   # Example excerpt from makefile.100_board_xxx

   # builtin radio in stm32w/ stm32w108
   # (low-level driver exactly knows how to communicate)
   COMPILE_OPTS += -DTTC_RADIO1=trd_stm32w

   # transceiver cc1101 connected via SPI
   # (low-level driver needs to know where pins are connected to)
   COMPILE_OPTS += -DTTC_RADIO2=trd_cc1101_spi
   COMPILE_OPTS += -DTTC_RADIO2_SPI_INDEX=1          # use first configured SPI bus (requires a configured TTC_SPI1 as described in ttc_spi.h)
   COMPILE_OPTS += -DTTC_RADIO2_PIN_SPI_NSS=PIN_PB4  # pin used as spi slave-select
   COMPILE_OPTS += -DTTC_RADIO2_PIN_GDO0=PIN_PC13    # pin connected to GDO0-pin on cc1101
   COMPILE_OPTS += -DTTC_RADIO2_PIN_GDO1=PIN_PC2     # pin connected to GDO1-pin on cc1101
   COMPILE_OPTS += -DTTC_RADIO2_PIN_GDO2=PIN_PC8     # pin connected to GDO2-pin on cc1101

   # some transceiver connected via USART
   COMPILE_OPTS += -DTTC_RADIO3=some_radio_usart
   COMPILE_OPTS += -DTTC_RADIO3_USART_INDEX=1        # using first configured USART (requires a configured TTC_USART1 as described in ttc_usart.h)


   Building a new radio driver

   Adding a new radio driver XXX to The ToolChain requires
   (1) Define a set of additional constants that are required to configure your radio
     - Convention: Use a name prefix TTC_RADIOn_ (n = 1..5) for each of your configuration constants
     - write an example makefile demonstrating the use of all of your configuration constants

   (2) implement these low-level functions in radio_XXX.c/ .h
     radio_XXX_load_defaults()
     radio_XXX_get_features()
     radio_XXX_get_max_channel()
     radio_XXX_get_max_level()
     radio_XXX_init()
     radio_XXX_set_operating_mode()
     radio_XXX_set_power_tx()
     radio_XXX_set_channel_rx()
     radio_XXX_set_channel_tx()
     radio_XXX_check_bytes_received()
     radio_XXX_check_mode()
     radio_XXX_packet_send()
     radio_XXX_read_packet()

   (3) add calls to your low-level functions in ttc_radio.c:
     ttc_radio_load_defaults()          -> radio_XXX_load_defaults()
     ttc_radio_get_features()           -> radio_XXX_get_features()
     ttc_radio_get_max_channel()        -> radio_XXX_get_max_channel()
     ttc_radio_get_max_level()          -> radio_XXX_get_max_level()
     ttc_radio_init()                   -> radio_XXX_init()
     ttc_radio_set_operating_mode()     -> radio_XXX_set_operating_mode()
     ttc_radio_set_power_tx()           -> radio_XXX_set_power_tx()
     ttc_radio_set_channel_rx()         -> radio_XXX_set_channel_rx()
     ttc_radio_set_channel_tx()         -> radio_XXX_set_channel_tx()
     _ttc_radio_check_bytes_received()  -> radio_XXX_check_bytes_received()
     _ttc_radio_check_mode()            -> radio_XXX_check_mode()
     _ttc_radio_task_tx()               -> radio_XXX_packet_send()
     _ttc_radio_read_packet()           -> radio_XXX_read_packet()

  (4) create an example source
    - name it example_radio_XXX.c/ .h
    - place it into InstallData/Template/examples/

  (5) write an installscript
    - copy an existing install_*.sh file as a template
    - name it install_N_radio_XXX.sh (N = some number)
    - place it in InstallData/ (where all other install_*.sh files are located)
    + inside the installscript
      - create a makefile + activate script to allow activation of your low-level driver
      - create a makefile + activate script to allow activation of your example_radio_XXX.c

   Send me your added files for review and addition to The ToolChain.
   Enjoy the fame of being a contributor to the open source community.
}*/
//{ Defines/ TypeDefs ****************************************************

// amount of extra bytes to automatically add as header to every network packet
// (max over all activated radios)
#ifndef TTC_NETWORK_MAX_HEADER_SIZE
#define TTC_NETWORK_MAX_HEADER_SIZE 0
#endif

// amount of extra bytes to automatically add as footer to every network packet
// (max over all activated radios)
#ifndef TTC_NETWORK_MAX_FOOTER_SIZE
#define TTC_NETWORK_MAX_FOOTER_SIZE 0
#endif

// maximum allowed payload size
// (max over all activated radios)
#ifndef TTC_NETWORK_MAX_PAYLOAD_SIZE
#ifdef EXTENSION_400_radio_cc1101_spi
#define TTC_NETWORK_MAX_PAYLOAD_SIZE 64
#elif EXTENSION_400_radio_cc1120_spi
#define TTC_NETWORK_MAX_PAYLOAD_SIZE 128
#else
#define TTC_NETWORK_MAX_PAYLOAD_SIZE  0
#endif
#endif

//} Defines
//{ Includes *************************************************************

#include "ttc_basic.h"
#include "ttc_memory.h"
#include "ttc_string.h"
#include "ttc_task.h"
#include "ttc_radio_types.h"

// low-level radio drivers
#ifdef EXTENSION_400_radio_stm32w
  #ifndef TTC_RADIO_LOWLEVEL_DRIVER
    #define TTC_RADIO_LOWLEVEL_DRIVER
  #endif
  #include "stm32w_radio.h"
#endif
#ifdef EXTENSION_400_radio_cc1101_spi
  #ifndef TTC_RADIO_LOWLEVEL_DRIVER
    #define TTC_RADIO_LOWLEVEL_DRIVER
  #endif
  #include "radio_cc1101.h"
#endif
#ifdef EXTENSION_400_radio_cc1120_spi
  #ifndef TTC_RADIO_LOWLEVEL_DRIVER
    #define TTC_RADIO_LOWLEVEL_DRIVER
  #endif
  #include "radio_cc1120.h"
#endif
#ifdef EXTENSION_400_radio_serial
  #ifndef TTC_RADIO_LOWLEVEL_DRIVER
    #define TTC_RADIO_LOWLEVEL_DRIVER
  #endif
  #include "radio_serial.h"
#endif

#ifndef TTC_RADIO_LOWLEVEL_DRIVER
  #warning No low-level radio driver activated!
#endif

#ifdef EXTENSION_400_radio_cc1190
  #include "radio_cc1190.h"
#endif

#ifdef EXTENSION_500_ttc_watchdog
  #include "ttc_watchdog.h"
#endif
//} Includes
//{ Structures/ Enums 2 **************************************************

//} Structures/ Enums
//{ Function prototypes **************************************************


/** returns amount of RADIO devices available on current uC
 * @return amount of available RADIO devices
 */
u8_t ttc_radio_get_max_index();

/** delivers valid generic configuration data of indexed radio
 *
 * Note: A default configuration is loaded automatically if called first time.
 *
 * @param LogicalIndex   >0: logical device index of RADIO to use (1..ttc_radio_get_max_index()); 1 = TTC_RADIO1, ...
 * @return               reference to generic radio configuration (no exclusive access guaranteed!)
 */
ttc_radio_generic_t* ttc_radio_get_configuration(u8_t LogicalIndex);


/** fills out given Radio_Generic with default values for indexed RADIO
 *
 * Note: It is safe to call this function with invalid LogicalIndex to probe for existing radios (will not assert)
 *
 * @param LogicalIndex    device index of RADIO to init (1..ttc_radio_get_max_index())
 * @return  == 0:         *Radio_Generic has been initialized successfully; != 0: error-code
 */
ttc_radio_errorcode_e ttc_radio_load_defaults(u8_t LogicalIndex);

/** fills out given Radio_Generic with maximum valid values for indexed RADIO
 *
 * Note: It is safe to call this function with invalid LogicalIndex to probe for existing radios (will not assert)
 *
 * @param LogicalIndex    device index of RADIO to init (1..ttc_radio_get_max_index()); 1 = TTC_RADIO1, ...
 * @param Radio_Generic   pointer to struct ttc_radio_generic_t
 * @return  == 0:         *Radio_Generic has been initialized successfully; != 0: error-code
 */
ttc_radio_errorcode_e ttc_radio_get_features(u8_t LogicalIndex, ttc_radio_generic_t* Radio_Generic);

/** returns maximum supported channel indices for transmit and receive
 *
 * @param LogicalIndex   device index of radio to init (1..ttc_radio_get_max_index())
 * @param MaxChannelTX   will be loaded with maximum allowed channel index for transmit
 * @param MaxChannelRX   will be loaded with maximum allowed channel index for receive
 * @return               != NULL: pointer to new or reused network packet;
 *                       == NULL: currently not available (-> increase Radio_Generic->Size_Queue_Tx or wait until a packet is released)
 */
ttc_radio_errorcode_e ttc_radio_get_max_channel(u8_t LogicalIndex, u8_t* MaxChannelTX, u8_t* MaxChannelRX);

/** returns maximum supported levels for transmit and receive
 *
 * @param LogicalIndex   device index of radio (1..ttc_radio_get_max_index())
 * @param MaxLevelTX     will be loaded with maximum allowed channel index for transmit
 * @param MaxLevelRX     will be loaded with maximum allowed channel index for receive
 * @return               != NULL: pointer to new or reused network packet;
 *                       == NULL: currently not available (-> increase Radio_Generic->Size_Queue_Tx or wait until a packet is released)
 */
ttc_radio_errorcode_e ttc_radio_get_max_level(u8_t LogicalIndex, u8_t* MaxLevelTX, u8_t* MaxLevelRX);

/** delivers a memory block usable as transmit or receive buffer
 *
 * @return               != NULL: pointer to new or reused network packet;
 *                       == NULL: currently not available (-> increase Radio_Generic->Size_Queue_Tx or wait until a packet is released)
 */
ttc_heap_block_t* ttc_radio_get_empty_block();
/** returns the datarate configured for the selected radio
 *
 * @return               != NULL: Datarate in Bits/second
 *                       == NULL: not implemented by radio
 */
u32_t ttc_radio_getDatarate(u8_t LogicalIndex);
/** returns the amount of bytes that is added for each packet transmission by the radio (Pramble, Sync Words Address...)
 *
 * @return               != NULL: Amount bytes added to  the payload by the radio
 *                       == NULL: not implemented by radio
 */
u8_t ttc_radio_getPacketHeaderSize(u8_t LogicalIndex);
/** initializes single radio device for fully operation
 *
 * Note: Will assert if invalid LogicalIndex is given!
 *
 * @param LogicalIndex    device index of RADIO to init (1..ttc_radio_get_max_index()); 1 = TTC_RADIO1, ...
 * @param DeviceAddress   the device address used for packet filtering
 * @return                == 0: RADIO has been initialized successfully; != 0: error-code
 */
ttc_radio_errorcode_e ttc_radio_init(u8_t LogicalIndex, u8_t DeviceAddress);

/** Send out data from given network packet (send-function with least processing overhead)
 *
 * Packets are the native memory structure for radio communication.
 * In most cases, packets are created and maintained by ttc_radio on the fly and passed to the
 * underlying low-level driver. An application can create and send out packets on its own to reduce
 * copy overhead and memory usage.
 *
 * Note: RADIO must be initialized before!
 * Note: This function is partially thread safe (use it for each RADIO from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param LogicalIndex    device index of RADIO to init (1..ttc_radio_get_max_index()); 1 = TTC_RADIO1, ...
 * @param Packet          memory block being allocated by ttc_network_alloc_packet()
 *                        Packet goes into radio custody and must not be used by caller before it has been released!
 * @return                == 0: Buffer has been queued for transmission successfully; != 0: error-code
 */
ttc_radio_errorcode_e ttc_radio_send_packet(u8_t LogicalIndex, ttc_heap_block_t* Packet);

/** Send out data from given memory block
 *
 * Note: RADIO must be initialized before!
 * Note: This function is partially thread safe (use it for each RADIO from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param LogicalIndex    device index of RADIO to init (1..ttc_radio_get_max_index()); 1 = TTC_RADIO1, ...
 * @param Block           memory block being allocated by ttc_heap_alloc_block()/ ttc_heap_alloc_virtual_block()
 *                        Block goes into radio custody and must not be used by caller before it has been released!
 */
void ttc_radio_send_block(u8_t LogicalIndex, ttc_heap_block_t* Block);

/** Send out given amount of raw bytes from buffer.
 * Will not interpret bytes in buffer.
 *
 * Note: RADIO must be initialized before!
 * Note: This function is partially thread safe (use it for each RADIO from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * 
 * @param LogicalIndex     device index of RADIO to init (1..ttc_radio_get_max_index()); 1 = TTC_RADIO1, ...
 * @param Buffer           memory location from which to read data
 * @param Amount           amount of bytes to send from Buffer[]
 * @return                 == 0: Buffer has been successfully queued for transmit; != 0: error-code
 */
ttc_radio_errorcode_e ttc_radio_send_raw(u8_t LogicalIndex, const u8_t* Buffer, Base_t Amount);

/** Send out given bytes from buffer until first zero byte or MaxLength reached.
 *
 * Note: RADIO must be initialized before!
 * Note: This function is partially thread safe (use it for each RADIO from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * 
 * @param LogicalIndex     device index of RADIO to init (1..ttc_radio_get_max_index()); 1 = TTC_RADIO1, ...
 * @param Buffer           memory location from which to read data
 * @param MaxLength        no more than this amount of bytes will be send
 * @return                 == 0: Buffer has been successfully queued for transmit; != 0: error-code
 */
ttc_radio_errorcode_e ttc_radio_send_string(u8_t LogicalIndex, const char* String, Base_t MaxSize);

/** will send given constant string to default radio being set by ttc_radio_stdout_set()
 *
 * Note: This function will not copy string to temporary buffer, thus saving memory + cpu time
 *
 * @param LogicalIndex   device index of RADIO to use (1..ttc_radio_get_max_index()); 1 = TTC_RADIO1, ...
 * @param String         pointer to constant string (must not change during transmission!)
 * @param MaxSize        size of String[] buffer (contained, zero-terminated string may be shorter)
 * @return                 == 0: Buffer has been successfully queued for transmit; != 0: error-code
 */
ttc_radio_errorcode_e ttc_radio_send_string_const(u8_t LogicalIndex, const char* String, Base_t MaxSize);

/** switches indexed radio into given operating mode
 *
 * @param LogicalIndex   device index of RADIO to use (1..ttc_radio_get_max_index()); 1 = TTC_RADIO1, ...
 * @param Mode           one of ttc_radio_mode_e
 */
ttc_radio_errorcode_e ttc_radio_set_operating_mode(u8_t LogicalIndex, ttc_radio_mode_e Mode);

/** Registers a function to be called whenever a complete packet has been received
 *
 * @param LogicalIndex    device index of RADIO to init (1..ttc_radio_get_max_index()); 1 = TTC_RADIO1, ...
 * @param RxFunction      points to function which will be called to process received data;
 *                        RxFunction() has to return memory block immediately or pass it to ttc_heap_block_release() later!
 * @param Mode            specifies, when to call RxFunction()
 */
void ttc_radio_register_rx_function(u8_t LogicalIndex, ttc_heap_block_t* (*RxFunction)(struct ttc_radio_generic_s* RadioGeneric, ttc_heap_block_t* Block, u8_t RSSI, u8_t TargetAddress));

/** Registers a function to be called whenever a complete packet has been received
 *
 * @param LogicalIndex    device index of RADIO to init (1..ttc_radio_get_max_index()); 1 = TTC_RADIO1, ...
 * @param TxFunction      points to function which will be called whenever a packet is about to be send out
 *                        This function will see the raw data being send out
 * @param Mode            specifies, when to call RxFunction()
 * @return                == 0: function has been registered successfully; != 0: error-code
 */
void ttc_radio_register_tx_function(u8_t LogicalIndex, void (*TxFunction)(ttc_radio_generic_t* RadioGeneric, u8_t Size, const u8_t* Buffer));

/** Defines which logical device shall be used by ttc_radio_stdout_send_string()/ ttc_radio_stdout_send_block()
 *
 * @param LogicalIndex   >0: logical device index of RADIO to use (1..ttc_radio_get_max_index()); 1 = TTC_RADIO1, ...
 * @return               != tre_OK: error message
 */
ttc_radio_errorcode_e ttc_radio_stdout_set(u8_t LogicalIndex);

/** will send given memory block to default radio being set by ttc_radio_stdout_set()
 *
 * @param Block         pointer to a ttc_heap_block_t (will be released after sending automatically)
 */
void ttc_radio_stdout_send_block(ttc_heap_block_t* Block);

/** will send given volatile string to default radio being set by ttc_radio_stdout_set()
 *
 * @param String         pointer to constant string (must not change during transmission!)
 * @param MaxSize        size of String[] buffer (contained, zero-terminated string may be shorter)
 */
void ttc_radio_stdout_send_string(const char* String, Base_t MaxSize);

/** changes channel used for receive
 *
 * Note: Many radios do not provide independent channels for tx/rx.
 *       Check ttc_radio_generic_t.Flags.Bit.IndepentendTransmitterReceiver to be sure.
 *
 * @param ChannelIndex  1..ttc_radio_generic_t.MaxChannelRx
 * @return               != tre_OK: error message
 */
void ttc_radio_set_channel_rx(u8_t LogicalIndex, u8_t ChannelIndex);

/** changes channel used for transmission
 *
 * Note: Many radios do not provide independent channels for tx/rx.
 *       Check ttc_radio_generic_t.Flags.Bit.IndepentendTransmitterReceiver to be sure.
 *
 * @param ChannelIndex  1..ttc_radio_generic_t.MaxChannelTx
 */
void ttc_radio_set_channel_tx(u8_t LogicalIndex, u8_t ChannelIndex);

/** Waits until transmit queue has run empty on given radio
 *
 * Note: radio must be initialized before!
 * Note: This function blocks until all bytes have been sent!
 * Note: You may want to call ttc_task_begin_criticalsection() before to be sure that no other task transmits in parallel.
 *
 * @param LogicalIndex      device index of radio to init (1..ttc_radio_get_max_index())
 */
void ttc_radio_flush_tx(u8_t LogicalIndex);

/** releases a used Tx/ Rx packet for later reuse
 *
 * Note: This function is private and should not be called from outside
 *
 * @param Packet        pointer to a ttc_heap_block_t
 */
void _ttc_radio_release_block(ttc_heap_block_t* Packet);

/** returns type of low-level driver that is responsible for indexed radio
 *
 * @param  LogicalIndex  logical index of radio device (1=TTC_RADIO1, ...)
 * @return each low-level radio driver has its own unique enum
 */
ttc_radio_driver_e ttc_radio_get_driver_type(u8_t LogicalIndex);

/** controls external radio amplifier for indexed radio (if defined)
 *
 * @param LogicalIndex  logical index of radio device (1=TTC_RADIO1, ...)
 * @param Mode          new operating mode for this amplifier
 */
void ttc_radio_set_amplifier(u8_t LogicalIndex, ttc_radio_amplifier_mode_e Mode);

/** sets target node address for all following transmissions
 *
 * @param LogicalIndex  logical index of radio device (1=TTC_RADIO1, ...)
 * @param Address       logical address of destination node (255=Broadcast address)
 */
void ttc_radio_set_destination(u8_t LogicalIndex, u8_t Address);

/** sets power level to use for transmit
 *
 * @param LogicalIndex  logical index of radio device (1=TTC_RADIO1, ...)
 * @param Level         output level in dBm
 */
void ttc_radio_set_power_tx(u8_t LogicalIndex, s8_t Level);

/** sets channel to use for receive
 *
 * Note: Will Assert if invalid ChannelIndex is given!
 *
 * @param LogicalIndex  logical index of radio device (1=TTC_RADIO1, ...)
 * @param ChannelIndex  1..ttc_radio_generic_t.MaxChannelRx
 */
void ttc_radio_set_channel_rx(u8_t LogicalIndex, u8_t ChannelIndex);

/** sets channel to use for transmit
 *
 * Note: Will Assert if invalid ChannelIndex is given!
 *
 * @param LogicalIndex  logical index of radio device (1=TTC_RADIO1, ...)
 * @param ChannelIndex  1..ttc_radio_generic_t.MaxChannelTx
 */
void ttc_radio_set_channel_tx(u8_t LogicalIndex, u8_t ChannelIndex);

/** configure Duty Cycle
 *
 * Note: Will Assert if invalid ChannelIndex is given!
 *
 * @param LogicalIndex  logical index of radio device (1=TTC_RADIO1, ...)
 * @param Limit         ttc_duty_cycle_limit_e the duty cycle limit for the selected channel (0.1%, 1%)
 * @param Timeslot      ttc_duty_cycle_Timeslot_e the timeslot used for calculation of duty cycle
 * @param Datarate      the datarate the transmitter is programmed to (must match the current radio settings!!!!)
 */
ttc_radio_errorcode_e ttc_radio_duty_cycle_config(u8_t LogicalIndex, ttc_duty_cycle_limit_e Limit, ttc_duty_cycle_Timeslot_e Timeslot, u32_t Datarate);

/** configure Duty Cycle User callback functions
 *Note: functions not beeing used can be NULL
 *
 * Note: Will Assert if invalid LogicalIndex is given!
 *
 * @param LogicalIndex               logical index of radio device (1=TTC_RADIO1, ...)
 * @param UCB_dc_25_used             user callback function - duty cycle has been 25% used
 * @param UCB_dc_50_used             user callback function - duty cycle has been 50% used
 * @param UCB_dc_75_used             user callback function - duty cycle has been 75% used
 * @param UCB_dc_expired             user callback function - duty cycle has been expired (completely used)
 * @param UCB_dc_timeframe_expired   user callback function - duty cycle timeframe has expired / new slot is going to be started
 */
ttc_radio_errorcode_e ttc_radio_duty_cycle_UCB_config(u8_t LogicalIndex,
                                                      void (*UCB_dc_25_used)(u8_t), //user callback - duty cycle has been 25% used
                                                      void (*UCB_dc_50_used)(u8_t), //user callback - duty cycle has been 50% used
                                                      void (*UCB_dc_75_used)(u8_t), //user callback - duty cycle has been 75% used
                                                      void (*UCB_dc_exceed)(u8_t), //user callback - duty cycle has been expired (completely used)
                                                      void (*UCB_dc_timeframe_expired)(u8_t) //user callback - duty cycle timeframe has expired / new slot is going to be started
                                                      );

/** This function adds the bytes transmitted to be able to calculate the usage level within the current duty cycle
 *
 * Note: This function is private and should not be called from outside!
 *
 * @param Argument  - pointer to LogicalIndex of Radio
 */
void _ttc_radio_duty_cycle_add_tx_bytes(u8_t LogicalIndex,u8_t Amount);
/** This function checks if the duty cycle limits are violated and calls the according User callback functions for:
 *  25%, 50%, 75% or expired
 * Note: This function is private and should not be called from outside!
 *
 * @param Argument  - pointer to LogicalIndex of Radio
 * @return == 0 no Limit has been violated
 *         == 1 25 % criteria has been violated
 *         == 2 50 % criteria has been violated
 *         == 3 75 % criteria has been violated
 *         == 4  duty cycle has exeeded its limit (more bytes transmitted than allowed)
 */
u8_t _ttc_radio_duty_cycle_check_limits(u8_t LogicalIndex);
/** This function the amount of bytes that can be transmitted within the current timeframe.
 *
 *
 * @param Argument  - pointer to LogicalIndex of Radio
 * @return == 0 no more bytes are allowed to be transmitted
 *         != Number of bytes that are left(availabel) for the current time frame
 */
u32_t ttc_radio_duty_cycle_getFreeBytes(u8_t LogicalIndex);
/** task that will increase the duty cycle timer
 *
 * Note: This function is private and should not be called from outside!
 *
 * @param Argument  - pointer to LogicalIndex of Radio
 */
void _task_ttc_radio_duty_cycle_timer(void * TaskArgument);
/** Function that will be called whenever the configured duty cycle timeframe has expired
 *
 * Note: Will Assert if invalid Radio LogicalIndex is given!
 *
 * @param LogicalIndex  logical index of radio device (1=TTC_RADIO1, ...)
 */
ttc_radio_errorcode_e _ttc_radio_duty_cycle_timeslot_expired(u8_t LogicalIndex);
/** Reads received packet from transceiver into given memory block
 *
 * Note: RADIO must be initialized before!
 * Note: This function is partially thread safe (use it for each RADIO from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 * Note: This function is private and must not be called from outside!
 *
 * @param Radio_Generic   pointer to configuration of already initialized radio
 * @param Buffer          will be loaded with pointer to receive buffer
 * @param Amount          will be loaded with amount of valid bytes in Buffer[]
 * @return                != NULL: given memory block can be reused; ==NULL: memory block is still in use
 */
ttc_heap_block_t* _ttc_radio_read_packet(ttc_radio_generic_t* Radio_Generic, ttc_heap_block_t* Block);

/** checks if radio transceiver still runs in correct operating mode
 *
 * @param Radio_Generic   pointer to configuration of already initialized radio
 * @return  == 0: radio runs in expected operating mode
 *          != 0: radio is in unexpected operating mode (use ttc_radio_set_operating_mode() to fix)
 */
BOOL _ttc_radio_check_mode(ttc_radio_generic_t* Radio_Generic);

/** returns amount of bytes waiting in receive queue of radio transceiver
 *
 * Note: This function is private and must not be called from outside!
 *
 * @param Radio_Generic  pointer to configuration of already initialized radio
 * @return               amount of bytes waiting in receive queue
 */
u8_t _ttc_radio_check_bytes_received(ttc_radio_generic_t* Radio_Generic);

/** task that will send out all entries from Queue_TX
 *
 * Note: This function is private and should not be called from outside!
 *
 * @param Argument  - pointer to LogicalIndex of Radio
 */
void _ttc_radio_task_tx(void* Argument);

/** task that will receive data from single radio and push them as packets to Queue_RX
 *
 * Note: This function is private and should not be called from outside!
 *
 * @param Argument  - pointer to LogicalIndex of Radio
 */
void _ttc_radio_task_rx(void* Argument);

/** central task-yield function; break here if you don't know, who's blocking
  */
void ttc_radio_task_yield();


//}Function prototypes

#endif //TTC_RADIO_H
