#ifndef TTC_ASSERT_H
#define TTC_ASSERT_H
/** { ttc_assert.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Authors: Gregor Rebel
 */
/** Description of ttc_assert (Do not delete this line!)
 *
 *  The use of asserts is the basic key for stable code in embedded software.
 *  Every function should check all given arguments for plausibility.
 *  Calling any assert function with a zero (false) condition argument will lead
 *  program flow execution into an endless loop inside ttc_assert_halt_origin().
 *  This condition can be detected and analyzed in different ways.
 *
 *
 *  Analyze using GDB Debug Session
 *
 *  The classic way to use asserts during debugging your code is in a gdb session.
 *  GDB is an acronym for the GNU Debugger. See provided readme.TheToolChain for details how
 *  to start a debug session.
 *  The default configuration of every TTC project establishes a default breakpoint inside
 *  ttc_assert_halt_origin(). Whenever this function is called during a debug session, the
 *  program flow will be stopped and the user can inspect the reasons causing the assertion.
 *  One interesting information is which individual test failed. In many cases, you will be able
 *  to display the backtrace of program flow. That is the history of function calls in reverse
 *  order starting with ttc_assert_halt_origin.
 *  In gdb simply enter bt command to display the backtrace:
 *  <CODE>
 *    bt
 *
 *  </CODE>
 *
 *
 *  Analyze using code Output
 *
 *  In case of an assert outside of a debug session, an code can help to find the
 *  reason for failure. For this, ttc_assert_halt_origin() can send the given code
 *  to a gpio pin. The code can be read by attaching an oscilloscope to the output pin.
 *  Simply define TTC_ASSERT_OUTPUT_PIN as one from e_ttc_gpio_pin in your makefile.
 *  Example makefile line that defines A9 as output pin:
 *  COMPILE_OPTS += -DTTC_ASSERT_OUTPUT_PIN=E_ttc_gpio_pin_a9
 *  See implementation of ttc_assert_halt_origin() for details.
 *
 *
 *  Driver Specific Asserts
 *
 *  TTC high- and low-level drivers typically do not called ttc_assert_*() functions directly.
 *  Every device driver provides its own Assert_{DEVICE}() function.
 *  This allows to switch off the use of assert code for each device driver from your makefile.
 *  Two basic strategies can be applied:
 *  1) Disable asserts of each driver individually
 *     To disable asserts in driver foo, add the following line to your makefile.700_extra_settings:
 *      COMPILE_OPTS += -DTTC_ASSERT_FOO 0  # disable assert handling for driver foo
 *  2) Globally disable all asserts and reenable them for individual drivers.
 *     a) Enable this line in your activate_project.sh:
 *        activate.060_basic_extensions_disable_asserts.sh
 *     b) Reenable assert for individual drivers (here we use driver foo as an example):
 *        COMPILE_OPTS += -DTTC_ASSERT_FOO 1
 *
 *
 *  Generic Assert Macros
 *
 *  These macros are always available for generic and simple use by applications.
 *  Definitions can be found in ttc_assert_types.h.
 *
 *  Assert( ((Condition) != 0), Origin)
 *      Checks given condition. Will assert with given code if Condition==0.
 *      Do not use a pointer as Condition! Use (Pointer!=NULL) instead
 *      or use Assert_NULL(), Assert_Readable() or Assert_Writable() to test pointers!
 *
 *  Assert_NULL(Pointer, Origin)
 *      Checks if given pointer is NOT the NULL pointer. Will assert if it is.
 *      This is a weak but fast test to check pointers.
 *
 *  Assert_Readable(Pointer, Origin)
 *      Checks if given pointers points to a readable address.
 *      This check is slower than checking for a NULL but it will detect more error conditions.
 *
 *      Readable addresses are architecture dependent and may be (among others):
 *      - ROM (FLASH)
 *      - EEPROM
 *      - RAM
 *      - Peripheral (memory mapped functional units)
 *      - External memory
 *
 *  Assert_Writable(Pointer, Origin)
 *      Checks if given pointers points to a writable address.
 *      This check is slower than checking for a NULL but it will detect more error conditions.
 *
 *      Writable addresses are architecture dependent and may be (among others):
 *      - RAM
 *      - Peripheral (memory mapped functional units)
 *      - External memory
 *
}*/

#ifndef EXTENSION_ttc_assert
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_assert.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_assert.c"
//
//? #include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_assert_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************



//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level assert only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function Declarations ************************************************{
 *
 * The functions declared below provide the main interface for
 * assert devices on all supported architectures.
 * Check assert/assert_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares assert Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_assert_prepare();


// This function ensures that given two addresses are the same.
// It is required to safe check register addresses (direct comparison created some strange errors)
void ttc_assert_address_matches( volatile void* Address1, volatile void* Address2 );


/** Central breakpoint. Call to initiate a break.
 *
 * Note: Add a breakpoint for this function in your configs/debug2_general.gdb file to enable this feature!
 * @param Origin   unique code to identify origin of assert
 *
 */
void __attribute__( ( noinline ) ) ttc_assert_break_origin( volatile e_ttc_assert_origin Origin );

/** Unconditionally halts with unknown or given origin. Call if Built In Self Test has failed.
 *
 *  Question: Why do we need asserts, could there be a better error reporting?
 *  Quick Answer: No!
 *  Long  Answer: Hardly any:
 *                a) Self tests (asserts) are mainly used to test function arguments and sometimes even own return values.
 *                b) The basic assumption: Data generated inside a function is safe as long as it is a small and simple function.
 *                   Long or complex functions (those who call other functions) should also check their return value.
 *                c) It is better to test your own return value than that of every function you call (simply much less overhead).
 *                d) Device asserts used in The ToolChain (e.g. Assert_USART*()) can be disabled completely by simple constant define.
 *                   This can completely remove overhead in productive code (typically run on uC with smallest FLASH).
 *                e) Use of auto enumerated origin codes allows to uniquely report place of error via UART.
 *                   If disabled, all origin codes are removed from code (enable compiler optimization!).
 *                f) It is much better to stop in an assert than to crash somewhen later.
 *                g) Every failed self test teaches developer to correct his/her code.
 *                   You may also see self tests as a kind of documentation for everything that cannot be checked at compile time.
 *
 *  @param Origin   unique code to identify origin of assert
 */
void ttc_assert_halt( );
void ttc_assert_halt_origin( volatile e_ttc_assert_origin Origin );

/** Will assert if given pointer contains no writable address
 *
 *  @param Address  must point to a writable memory address (mainly RAM or Peripheral)
 *  @param Origin   unique code to identify origin of assert
 */
void ttc_assert_address_writable( void* Address );
void ttc_assert_address_writable_origin( void* Address, volatile e_ttc_assert_origin Origin );

/** Will assert if given pointer does not point to executable code
 *
 *  @param Address  must point to a memory address being able to run program code (typically ROM or RAM)
 *  @param Origin   unique code to identify origin of assert
 */
void ttc_assert_address_executable( void* Address );
void ttc_assert_address_executable_origin( void* Address, volatile e_ttc_assert_origin Origin );

/** Will assert if given pointer contains no readable address
 *
 *  @param Address  must point to a writable memory address (mainly RAM, FLASH or Peripheral)
 *  @param Origin   unique code to identify origin of assert
 */
void ttc_assert_address_readable( const void* Address );
void ttc_assert_address_readable_origin( const void* Address, volatile e_ttc_assert_origin Origin );

/** Generic Test function Will assert if given test argument is zero.
 *
 * @param TestOK ==0: test has failed => calls ttc_assert_halt_origin(Origin)
 * @param Origin      unique code to identify origin of assert
 */
void ttc_assert_test( BOOL TestOK );
void ttc_assert_test_origin( BOOL TestOK, volatile e_ttc_assert_origin Origin );


/** Will assert if given pointer is the NULL pointer.
 *
 *  @param void* (const)  pointer value (!= 0x0)
 *  @param Origin   unique code to identify origin of assert
 */
void ttc_assert_address_not_null( const void* Pointer );
void ttc_assert_address_not_null_origin( const void* Pointer, volatile e_ttc_assert_origin Origin );


/** Assert function for STM32 Standard Peripheral Library
 *
 * Note: Set define USE_FULL_ASSERT 1 to enable asserts in your stm32xxxx_conf.h file.
 */
void assert_failed( t_u8* File, t_u32 Line );


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_assert_ are passed to interfaces/ttc_assert_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl assert UPDATE
 */

//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_ASSERT_H
