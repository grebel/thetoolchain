#ifndef NETWORK_LAYER_USART_TYPES_H
#define NETWORK_LAYER_USART_TYPES_H

/** { network_layer_usart.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *                     
 *  Low-Level datatypes for NETWORK_LAYER devices on usart architectures.
 *  Structures, Enums and Defines being required by ttc_network_layer_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140224 16:21:32 UTC
 *
 *  Note: See ttc_network_layer.h for description of architecture independent NETWORK_LAYER implementation.
 * 
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_network_layer_types.h *************************

typedef struct { // register description (adapt according to usart registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} t_network_layer_register;

typedef struct {  // usart specific configuration data of single NETWORK_LAYER device
  t_network_layer_register* BaseRegister;       // base address of NETWORK_LAYER device registers
} __attribute__((__packed__)) t_network_layer_usart_config;

// t_ttc_network_layer_architecture is required by ttc_network_layer_types.h
#define t_ttc_network_layer_architecture t_network_layer_usart_config

//} Structures/ Enums


#endif //NETWORK_LAYER_USART_TYPES_H
