/** { network_layer_usart.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for network_layer devices on usart architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 20 at 20140224 16:21:32 UTC
 *
 *  Note: See ttc_network_layer.h for description of usart independent NETWORK_LAYER implementation.
 * 
 *  Authors: Gregor Rebel
}*/

#include "network_layer_usart.h"

//{ Function definitions *******************************************************

e_ttc_network_layer_errorcode network_layer_usart_deinit(t_ttc_network_layer_config* Config) {
    Assert_NETWORK_LAYER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (e_ttc_network_layer_errorcode) 0;
}
e_ttc_network_layer_errorcode network_layer_usart_get_features(t_ttc_network_layer_config* Config) {
    Assert_NETWORK_LAYER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (e_ttc_network_layer_errorcode) 0;
}
e_ttc_network_layer_errorcode network_layer_usart_init(t_ttc_network_layer_config* Config) {
    Assert_NETWORK_LAYER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (e_ttc_network_layer_errorcode) 0;
}
e_ttc_network_layer_errorcode network_layer_usart_load_defaults(t_ttc_network_layer_config* Config) {
    Assert_NETWORK_LAYER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (e_ttc_network_layer_errorcode) 0;
}
void network_layer_usart_prepare() {


    

}
e_ttc_network_layer_errorcode network_layer_usart_reset(t_ttc_network_layer_config* Config) {
    Assert_NETWORK_LAYER(Config, ttc_assert_origin_auto); // pointers must not be NULL

    
    return (e_ttc_network_layer_errorcode) 0;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
