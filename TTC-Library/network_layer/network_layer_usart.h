#ifndef NETWORK_LAYER_USART_H
#define NETWORK_LAYER_USART_H

/** { network_layer_usart.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for network_layer devices on usart architectures.
 *  Structures, Enums and Defines being required by high-level network_layer and application.
 *
 *  Created from template device_architecture.h revision 21 at 20140224 16:21:32 UTC
 *
 *  Note: See ttc_network_layer.h for description of usart independent NETWORK_LAYER implementation.
 *  
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_NETWORK_LAYER_USART
//
// Implementation status of low-level driver support for network_layer devices on usart
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_NETWORK_LAYER_USART '?'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_NETWORK_LAYER_USART == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_NETWORK_LAYER_USART to one from '-', 'o', '+' 
#endif

//}EXTENSION_STATUS_TTC_NETWORK_LAYER_USART

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "network_layer_usart_types.h"
#include "../ttc_network_layer_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_network_layer_interface.h
#define ttc_driver_network_layer_deinit(Config) network_layer_usart_deinit(Config)
#define ttc_driver_network_layer_get_features(Config) network_layer_usart_get_features(Config)
#define ttc_driver_network_layer_load_defaults(Config) network_layer_usart_load_defaults(Config)
#define ttc_driver_network_layer_prepare() network_layer_usart_prepare()
#define ttc_driver_network_layer_reset(Config) network_layer_usart_reset(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_network_layer.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_network_layer.h for details.
 *
 * You normally should not need to add functions here. 
 * Add your private functions in the private section below. 
 */


/** shutdown single NETWORK_LAYER unit device
 * @param Config        pointer to struct t_ttc_network_layer_config (must have valid value for PhysicalIndex)
 * @return              == 0: NETWORK_LAYER has been shutdown successfully; != 0: error-code
 */
e_ttc_network_layer_errorcode network_layer_usart_deinit(t_ttc_network_layer_config* Config);


/** fills out given Config with maximum valid values for indexed NETWORK_LAYER
 * @param Config        = pointer to struct t_ttc_network_layer_config (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_network_layer_errorcode network_layer_usart_get_features(t_ttc_network_layer_config* Config);


/** initializes single NETWORK_LAYER unit for operation
 * @param Config        pointer to struct t_ttc_network_layer_config (must have valid value for PhysicalIndex)
 * @return              == 0: NETWORK_LAYER has been initialized successfully; != 0: error-code
 */
e_ttc_network_layer_errorcode network_layer_usart_init(t_ttc_network_layer_config* Config);


/** loads configuration of indexed NETWORK_LAYER unit with default values
 * @param Config        pointer to struct t_ttc_network_layer_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_network_layer_errorcode network_layer_usart_load_defaults(t_ttc_network_layer_config* Config);


/** Prepares network_layer driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void network_layer_usart_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_network_layer_config (must have valid value for PhysicalIndex)
 * @return   = 
 */
e_ttc_network_layer_errorcode network_layer_usart_reset(t_ttc_network_layer_config* Config);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _network_layer_usart_foo(t_ttc_network_layer_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //NETWORK_LAYER_USART_H