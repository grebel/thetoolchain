/** { ttc_pwm_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for PWM device.
 *  Structures, Enums and Defines being required by both, high- and low-level pwm.
 *  This file does not provide function declarations!
 *
 *  Created from template ttc_device_types.h revision 43 at 20180502 11:57:47 UTC
 *
 *  Authors: <AUTHOR>
 *
}*/

#ifndef TTC_PWM_TYPES_H
#define TTC_PWM_TYPES_H

//{ Includes1 ************************************************************
//
// Includes being required for static configuration.
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_pwm.h" or "ttc_pwm.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h"

//} Includes
/** { Static Configuration *************************************************
 *
 * Most ttc drivers are statically configured by constant definitions in makefiles.
 * A static configuration allows to hide certain code fragments and variables for the
 * compilation process. If used wisely, your code will be lean and fast and do only
 * what is required for the current configuration.
 */

/** {  TTC_PWMn has to be defined as constant in one of your makefiles
 *
 * Here we count how much devices have been defined and if all lower index devices have been defined too.
 */
#ifndef TTC_PWM_AMOUNT //{ 10
#  ifdef TTC_PWM10
#    ifndef TTC_PWM9
#      error TTC_PWM10 is defined, but not TTC_PWM9 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM8
#      error TTC_PWM10 is defined, but not TTC_PWM8 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM7
#      error TTC_PWM10 is defined, but not TTC_PWM7 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM6
#      error TTC_PWM10 is defined, but not TTC_PWM6 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM5
#      error TTC_PWM10 is defined, but not TTC_PWM5 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM4
#      error TTC_PWM10 is defined, but not TTC_PWM4 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM3
#      error TTC_PWM10 is defined, but not TTC_PWM3 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM2
#      error TTC_PWM10 is defined, but not TTC_PWM2 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM1
#      error TTC_PWM10 is defined, but not TTC_PWM1 - all lower TTC_PWMn must be defined!
#    endif

#    define TTC_PWM_AMOUNT 10
#  endif
#endif //}
#ifndef TTC_PWM_AMOUNT //{ 9
#  ifdef TTC_PWM9
#    ifndef TTC_PWM8
#      error TTC_PWM9 is defined, but not TTC_PWM8 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM7
#      error TTC_PWM9 is defined, but not TTC_PWM7 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM6
#      error TTC_PWM9 is defined, but not TTC_PWM6 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM5
#      error TTC_PWM9 is defined, but not TTC_PWM5 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM4
#      error TTC_PWM9 is defined, but not TTC_PWM4 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM3
#      error TTC_PWM9 is defined, but not TTC_PWM3 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM2
#      error TTC_PWM9 is defined, but not TTC_PWM2 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM1
#      error TTC_PWM9 is defined, but not TTC_PWM1 - all lower TTC_PWMn must be defined!
#    endif

#    define TTC_PWM_AMOUNT 9
#  endif
#endif //}
#ifndef TTC_PWM_AMOUNT //{ 8
#  ifdef TTC_PWM8
#    ifndef TTC_PWM7
#      error TTC_PWM8 is defined, but not TTC_PWM7 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM6
#      error TTC_PWM8 is defined, but not TTC_PWM6 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM5
#      error TTC_PWM8 is defined, but not TTC_PWM5 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM4
#      error TTC_PWM8 is defined, but not TTC_PWM4 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM3
#      error TTC_PWM8 is defined, but not TTC_PWM3 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM2
#      error TTC_PWM8 is defined, but not TTC_PWM2 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM1
#      error TTC_PWM8 is defined, but not TTC_PWM1 - all lower TTC_PWMn must be defined!
#    endif

#    define TTC_PWM_AMOUNT 8
#  endif
#endif //}
#ifndef TTC_PWM_AMOUNT //{ 7
#  ifdef TTC_PWM7
#    ifndef TTC_PWM6
#      error TTC_PWM7 is defined, but not TTC_PWM6 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM5
#      error TTC_PWM7 is defined, but not TTC_PWM5 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM4
#      error TTC_PWM7 is defined, but not TTC_PWM4 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM3
#      error TTC_PWM7 is defined, but not TTC_PWM3 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM2
#      error TTC_PWM7 is defined, but not TTC_PWM2 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM1
#      error TTC_PWM7 is defined, but not TTC_PWM1 - all lower TTC_PWMn must be defined!
#    endif

#    define TTC_PWM_AMOUNT 7
#  endif
#endif //}
#ifndef TTC_PWM_AMOUNT //{ 6
#  ifdef TTC_PWM6
#    ifndef TTC_PWM5
#      error TTC_PWM6 is defined, but not TTC_PWM5 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM4
#      error TTC_PWM6 is defined, but not TTC_PWM4 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM3
#      error TTC_PWM6 is defined, but not TTC_PWM3 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM2
#      error TTC_PWM6 is defined, but not TTC_PWM2 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM1
#      error TTC_PWM6 is defined, but not TTC_PWM1 - all lower TTC_PWMn must be defined!
#    endif

#    define TTC_PWM_AMOUNT 6
#  endif
#endif //}
#ifndef TTC_PWM_AMOUNT //{ 5
#  ifdef TTC_PWM5
#    ifndef TTC_PWM4
#      error TTC_PWM5 is defined, but not TTC_PWM4 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM3
#      error TTC_PWM5 is defined, but not TTC_PWM3 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM2
#      error TTC_PWM5 is defined, but not TTC_PWM2 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM1
#      error TTC_PWM5 is defined, but not TTC_PWM1 - all lower TTC_PWMn must be defined!
#    endif

#    define TTC_PWM_AMOUNT 5
#  endif
#endif //}
#ifndef TTC_PWM_AMOUNT //{ 4
#  ifdef TTC_PWM4
#    ifndef TTC_PWM3
#      error TTC_PWM4 is defined, but not TTC_PWM3 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM2
#      error TTC_PWM4 is defined, but not TTC_PWM2 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM1
#      error TTC_PWM4 is defined, but not TTC_PWM1 - all lower TTC_PWMn must be defined!
#    endif

#    define TTC_PWM_AMOUNT 4
#  endif
#endif //}
#ifndef TTC_PWM_AMOUNT //{ 3
#  ifdef TTC_PWM3

#    ifndef TTC_PWM2
#      error TTC_PWM3 is defined, but not TTC_PWM2 - all lower TTC_PWMn must be defined!
#    endif
#    ifndef TTC_PWM1
#      error TTC_PWM3 is defined, but not TTC_PWM1 - all lower TTC_PWMn must be defined!
#    endif

#    define TTC_PWM_AMOUNT 3
#  endif
#endif //}
#ifndef TTC_PWM_AMOUNT //{ 2
#  ifdef TTC_PWM2

#    ifndef TTC_PWM1
#      error TTC_PWM2 is defined, but not TTC_PWM1 - all lower TTC_PWMn must be defined!
#    endif

#    define TTC_PWM_AMOUNT 2
#  endif
#endif //}
#ifndef TTC_PWM_AMOUNT //{ 1
#  ifdef TTC_PWM1
#    define TTC_PWM_AMOUNT 1
#  endif
#endif //}
#ifndef TTC_PWM_AMOUNT //{ no devices defined at all
#  error Missing definition for TTC_PWM1. Using default type. Add valid definition to your makefile to get rid if this message!
// option 1: no devices available
#  define TTC_PWM_AMOUNT 0
// option 2: define a default device (enable lines below)
#endif

//}
//}
/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_PWM 0#         disable default asserts for pwm driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_PWM_EXTRA 1#   enable extra asserts for pwm driver
 *
 * When debugging code that has been compiled with optimization (gcc option -O) then
 * certain variables may be optimized away. Declaring variables as VOLATILE_PWM makes them
 * visible in debug session when asserts are enabled. You have no overhead at all if asserts are disabled.
 + foot_t* VOLATILE_PWM VisiblePointer;
 *
 */
#ifndef TTC_ASSERT_PWM    // any previous definition set (Makefile)?
#define TTC_ASSERT_PWM 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_PWM == 1)  // use Assert()s in PWM code (somewhat slower but alot easier to debug)
#  define Assert_PWM(Condition, Origin)        if (! (Condition) ) { ttc_assert_halt_origin(Origin); }
#  define Assert_PWM_Writable(Address, Origin) Assert_Writable(Address, Origin)
#  define Assert_PWM_Readable(Address, Origin) Assert_Readable(Address, Origin)
#  define VOLATILE_PWM                         volatile
#else  // use no default Assert()s in PWM code (somewhat smaller + faster, but crashes are hard to debug)
#  define Assert_PWM(Condition, Origin)
#  define Assert_PWM_Writable(Address, Origin)
#  define Assert_PWM_Readable(Address, Origin)
#  define VOLATILE_PWM
#endif

#ifndef TTC_ASSERT_PWM_EXTRA    // any previous definition set (Makefile)?
#define TTC_ASSERT_PWM_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_PWM_EXTRA == 1)  // use Assert()s in PWM code (somewhat slower but alot easier to debug)
#  define Assert_PWM_EXTRA(Condition, Origin) if (! (Condition) ) { ttc_assert_halt_origin(Origin); }
#  define Assert_PWM_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
#  define Assert_PWM_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no extra Assert()s in PWM code (somewhat smaller + faster, but crashes are hard to debug)
#  define Assert_PWM_EXTRA(Condition, Origin)
#  define Assert_PWM_EXTRA_Writable(Address, Origin)
#  define Assert_PWM_EXTRA_Readable(Address, Origin)
#endif
//}
/** { Check static configuration of pwm devices
 *
 * Each TTC_PWMn must be defined as one from e_ttc_pwm_architecture.
 * Additional defines of type TTC_PWMn_* may be added.
 *
 * The constants being checked here must be defined in your makefile.
 * Most constants are defined by EXTENSION_100_board_* or EXTENSION_150_board_extension_* files.
 */

#ifdef TTC_PWM1

// check extra defines for pwm device #1 here

#endif
#ifdef TTC_PWM2

// check extra defines for pwm device #2 here

#endif
#ifdef TTC_PWM3

// check extra defines for pwm device #3 here

#endif
#ifdef TTC_PWM4

// check extra defines for pwm device #4 here

#endif
#ifdef TTC_PWM5

// check extra defines for pwm device #5 here

#endif
#ifdef TTC_PWM6

// check extra defines for pwm device #6 here

#endif
#ifdef TTC_PWM7

// check extra defines for pwm device #7 here

#endif
#ifdef TTC_PWM8

// check extra defines for pwm device #8 here

#endif
#ifdef TTC_PWM9

// check extra defines for pwm device #9 here

#endif
#ifdef TTC_PWM10

// check extra defines for pwm device #10 here

#endif
//}

//}Static Configuration

//{ Includes2 ************************************************************
//
// _types.h Header files should include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_pwm.h" or "ttc_pwm.c"
//

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_pwm_stm32l1xx
#  include "pwm/pwm_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

/** { Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_pwm_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_pwm_architecture
#  warning Missing low-level definition for t_ttc_pwm_architecture (using default)
#  define t_ttc_pwm_architecture void
#endif
//}

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_pwm_errorcode       return codes of PWM devices
    ec_pwm_OK = 0,

    // other warnings go here..

    ec_pwm_ERROR,                  // general failure
    ec_pwm_NULL,                   // NULL pointer not accepted
    ec_pwm_DeviceNotFound,         // corresponding device could not be found
    ec_pwm_InvalidConfiguration,   // sanity check of device configuration failed
    ec_pwm_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..

    ec_pwm_unknown                // no valid errorcodes past this entry
} e_ttc_pwm_errorcode;
typedef enum {   // e_ttc_pwm_architecture    types of architectures supported by PWM driver
    ta_pwm_None = 0,       // no architecture selected


    ta_pwm_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
//InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_pwm_unknown        // architecture not supported
} e_ttc_pwm_architecture;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef union {  // architecture dependent configuration

    #ifdef EXTENSION_pwm_stm32l1xx
    t_pwm_stm32l1xx_config stm32l1xx;  // automatically added by ./create_DeviceDriver.pl
    #endif
//InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_pwm_architecture;
typedef struct s_ttc_pwm_features { // static minimum, maximum and default values of features of single pwm

    // Add any amount of architecture independent values describing pwm devices here.
    // You may also want to add a plausibility check for each value to _ttc_pwm_configuration_check().

    /** Example Features

    t_u16 MinBitRate;   // minimum allowed transfer speed
    t_u16 MaxBitRate;   // maximum allowed transfer speed
    */

    const t_u8 unused; // remove me!

} __attribute__( ( __packed__ ) ) t_ttc_pwm_features;
typedef struct s_ttc_pwm_config { //          architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_pwm_init()
    //       and after ttc_pwm_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // Hint: Order structure elements for optimal alignment.
    //       You should place smaller datatypes like t_u8 and t_u16 after all t_u32 and pointer types
    //       to prevent insertion of pad-bytes. Most architectures don't support placing 32-bit datatypes at
    //       unaligned address.

    struct { // Init: fields to be set by application before first calling ttc_pwm_init() --------------
        // Do not change these values after calling ttc_pwm_init()!

        t_u16 DutyCycle; // desired duty cycle in promille (1/1000)
        t_u32 Frequency; // set desired frequency in Hertz

        struct { // generic initial configuration bits common for all low-level Drivers
            unsigned Enabled : 1;  // ==1: PWM output is enabled; ==0: disabled

        } Flags;
    } Init;

    // Fields below are read-only for application! ----------------------------------------------------------

    u_ttc_pwm_architecture* LowLevelConfig; // low-level configuration (structure defined by low-level driver)

    const t_ttc_pwm_features* Features;     // constant features of this pwm

    struct { // status flags common for all architectures
        unsigned Initialized        : 1;  // ==1: device has been initialized successfully
        // ToDo: additional high-level flags go here..
    } Flags;

    // Last returned error code
    // IMPORTANT: Every ttc_pwm_*() function that returns e_ttc_pwm_errorcode has to update this value if it returns an error!
    e_ttc_pwm_errorcode LastError;

    t_u8  LogicalIndex;                          // automatically set: logical index of device to use (1 = TTC_PWM1, ...)
    t_u8  PhysicalIndex;                         // automatically set: index of physical device to use (0 = pwm #1, ...)
    e_ttc_pwm_architecture  Architecture;   // type of architecture used for current pwm device

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_pwm_config;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_PWM_TYPES_H
