/** { ttc_tcpip_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for TCPIP device.
 *  Structures, Enums and Defines being required by both, high- and low-level tcpip.
 *  This file does not provide function declarations!
 *  
 *  Created from template ttc_device_types.h revision 27 at 20150306 13:00:16 UTC
 *
 *  Authors: Gregor Rebel
 * 
}*/

#ifndef TTC_TCPIP_TYPES_H
#define TTC_TCPIP_TYPES_H

//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_tcpip.h" or "ttc_tcpip.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h" // only required to help IDE to know constant definitions
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
#ifdef EXTENSION_tcpip_uip
#  include "tcpip/tcpip_uip_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)
 
//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

//{  TTC_TCPIPn has to be defined as constant by makefile.100_board_*
#ifdef TTC_TCPIP5
  #ifndef TTC_TCPIP4
    #error TTC_TCPIP5 is defined, but not TTC_TCPIP4 - all lower TTC_TCPIPn must be defined!
  #endif
  #ifndef TTC_TCPIP3
    #error TTC_TCPIP5 is defined, but not TTC_TCPIP3 - all lower TTC_TCPIPn must be defined!
  #endif
  #ifndef TTC_TCPIP2
    #error TTC_TCPIP5 is defined, but not TTC_TCPIP2 - all lower TTC_TCPIPn must be defined!
  #endif
  #ifndef TTC_TCPIP1
    #error TTC_TCPIP5 is defined, but not TTC_TCPIP1 - all lower TTC_TCPIPn must be defined!
  #endif

  #define TTC_TCPIP_AMOUNT 5
#else
  #ifdef TTC_TCPIP4
    #define TTC_TCPIP_AMOUNT 4

    #ifndef TTC_TCPIP3
      #error TTC_TCPIP5 is defined, but not TTC_TCPIP3 - all lower TTC_TCPIPn must be defined!
    #endif
    #ifndef TTC_TCPIP2
      #error TTC_TCPIP5 is defined, but not TTC_TCPIP2 - all lower TTC_TCPIPn must be defined!
    #endif
    #ifndef TTC_TCPIP1
      #error TTC_TCPIP5 is defined, but not TTC_TCPIP1 - all lower TTC_TCPIPn must be defined!
    #endif
  #else
    #ifdef TTC_TCPIP3

      #ifndef TTC_TCPIP2
        #error TTC_TCPIP5 is defined, but not TTC_TCPIP2 - all lower TTC_TCPIPn must be defined!
      #endif
      #ifndef TTC_TCPIP1
        #error TTC_TCPIP5 is defined, but not TTC_TCPIP1 - all lower TTC_TCPIPn must be defined!
      #endif

      #define TTC_TCPIP_AMOUNT 3
    #else
      #ifdef TTC_TCPIP2

        #ifndef TTC_TCPIP1
          #error TTC_TCPIP5 is defined, but not TTC_TCPIP1 - all lower TTC_TCPIPn must be defined!
        #endif

        #define TTC_TCPIP_AMOUNT 2
      #else
        #ifdef TTC_TCPIP1
          #define TTC_TCPIP_AMOUNT 1
        #else
          #define TTC_TCPIP_AMOUNT 0
        #endif
      #endif
    #endif
  #endif
#endif
//}
/**{ Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop. 
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_TCPIP 0  # disable assert handling for tcpip devices
 *
 */
#ifndef TTC_ASSERT_TCPIP    // any previous definition set (Makefile)?
#define TTC_ASSERT_TCPIP 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_TCPIP == 1)  // use Assert()s in TCPIP code (somewhat slower but alot easier to debug)
  #define Assert_TCPIP(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in TCPIP code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_TCPIP(Condition, ErrorCode)
#endif
//}
/**{ Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_tcpip_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_tcpip_architecture
#warning Missing low-level definition for t_ttc_tcpip_architecture (using default)
#define t_ttc_tcpip_architecture void
#endif
//}
/**{ Check if definitions of logical indices are taken from e_ttc_tcpip_physical_index
 *
 * See definition of e_ttc_tcpip_physical_index below for details.
 *
 */

#define TTC_INDEX_TCPIP_MAX 10  // must have same value as TTC_INDEX_TCPIP_ERROR
#ifdef TTC_TCPIP1
#  if TTC_TCPIP1 >= TTC_INDEX_TCPIP_MAX
#    error Wrong definition of TTC_TCPIP1, check your makefile (must be one from e_ttc_tcpip_physical_index)!
#  endif
#  ifndef TTC_TCPIP1_DRIVER
#    warning Missing definition, check your makefile: TTC_TCPIP1_DRIVER (choose one from e_ttc_tcpip_architecture)
#    define TTC_TCPIP1_DRIVER ta_tcpip_None
#  endif
#endif
#ifdef TTC_TCPIP2
#  if TTC_TCPIP2 >= TTC_INDEX_TCPIP_MAX
#    error Wrong definition of TTC_TCPIP2, check your makefile (must be one from e_ttc_tcpip_physical_index)!
#  endif
#  ifndef TTC_TCPIP2_DRIVER
#    warning Missing definition, check your makefile: TTC_TCPIP2_DRIVER (choose one from e_ttc_tcpip_architecture)
#    define TTC_TCPIP2_DRIVER ta_tcpip_None
#  endif
#endif
#ifdef TTC_TCPIP3
#  if TTC_TCPIP3 >= TTC_INDEX_TCPIP_MAX
#    error Wrong definition of TTC_TCPIP3, check your makefile (must be one from e_ttc_tcpip_physical_index)!
#  endif
#  ifndef TTC_TCPIP3_DRIVER
#    warning Missing definition, check your makefile: TTC_TCPIP3_DRIVER (choose one from e_ttc_tcpip_architecture)
#    define TTC_TCPIP3_DRIVER ta_tcpip_None
#  endif
#endif
#ifdef TTC_TCPIP4
#  if TTC_TCPIP4 >= TTC_INDEX_TCPIP_MAX
#    error Wrong definition of TTC_TCPIP4, check your makefile (must be one from e_ttc_tcpip_physical_index)!
#  endif
#  ifndef TTC_TCPIP4_DRIVER
#    warning Missing definition, check your makefile: TTC_TCPIP4_DRIVER (choose one from e_ttc_tcpip_architecture)
#    define TTC_TCPIP4_DRIVER ta_tcpip_None
#  endif
#endif
#ifdef TTC_TCPIP5
#  if TTC_TCPIP5 >= TTC_INDEX_TCPIP_MAX
#    error Wrong definition of TTC_TCPIP5, check your makefile (must be one from e_ttc_tcpip_physical_index)!
#  endif
#  ifndef TTC_TCPIP5_DRIVER
#    warning Missing definition, check your makefile: TTC_TCPIP5_DRIVER (choose one from e_ttc_tcpip_architecture)
#    define TTC_TCPIP5_DRIVER ta_tcpip_None
#  endif
#endif
#ifdef TTC_TCPIP6
#  if TTC_TCPIP6 >= TTC_INDEX_TCPIP_MAX
#    error Wrong definition of TTC_TCPIP6, check your makefile (must be one from e_ttc_tcpip_physical_index)!
#  endif
#  ifndef TTC_TCPIP6_DRIVER
#    warning Missing definition, check your makefile: TTC_TCPIP6_DRIVER (choose one from e_ttc_tcpip_architecture)
#    define TTC_TCPIP6_DRIVER ta_tcpip_None
#  endif
#endif
#ifdef TTC_TCPIP7
#  if TTC_TCPIP7 >= TTC_INDEX_TCPIP_MAX
#    error Wrong definition of TTC_TCPIP7, check your makefile (must be one from e_ttc_tcpip_physical_index)!
#  endif
#  ifndef TTC_TCPIP7_DRIVER
#    warning Missing definition, check your makefile: TTC_TCPIP7_DRIVER (choose one from e_ttc_tcpip_architecture)
#    define TTC_TCPIP7_DRIVER ta_tcpip_None
#  endif
#endif
#ifdef TTC_TCPIP8
#  if TTC_TCPIP8 >= TTC_INDEX_TCPIP_MAX
#    error Wrong definition of TTC_TCPIP8, check your makefile (must be one from e_ttc_tcpip_physical_index)!
#  endif
#  ifndef TTC_TCPIP8_DRIVER
#    warning Missing definition, check your makefile: TTC_TCPIP8_DRIVER (choose one from e_ttc_tcpip_architecture)
#    define TTC_TCPIP8_DRIVER ta_tcpip_None
#  endif
#endif
#ifdef TTC_TCPIP9
#  if TTC_TCPIP9 >= TTC_INDEX_TCPIP_MAX
#    error Wrong definition of TTC_TCPIP9, check your makefile (must be one from e_ttc_tcpip_physical_index)!
#  endif
#  ifndef TTC_TCPIP9_DRIVER
#    warning Missing definition, check your makefile: TTC_TCPIP9_DRIVER (choose one from e_ttc_tcpip_architecture)
#    define TTC_TCPIP9_DRIVER ta_tcpip_None
#  endif
#endif
#ifdef TTC_TCPIP10
#  if TTC_TCPIP10 >= TTC_INDEX_TCPIP_MAX
#    error Wrong definition of TTC_TCPIP10, check your makefile (must be one from e_ttc_tcpip_physical_index)!
#  endif
#  ifndef TTC_TCPIP10_DRIVER
#    warning Missing definition, check your makefile: TTC_TCPIP10_DRIVER (choose one from e_ttc_tcpip_architecture)
#    define TTC_TCPIP10_DRIVER ta_tcpip_None
#  endif
#endif
//}

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef e_ttc_physical_index e_ttc_tcpip_physical_index;
typedef enum {   // e_ttc_tcpip_errorcode      return codes of TCPIP devices
  ec_tcpip_OK = 0, 
  
  // other warnings go here..

  ec_tcpip_ERROR,                  // general failure
  ec_tcpip_NULL,                   // NULL pointer not accepted
  ec_tcpip_DeviceNotFound,         // corresponding device could not be found
  ec_tcpip_InvalidConfiguration,   // sanity check of device configuration failed
  ec_tcpip_InvalidImplementation,  // your code does not behave as expected

  // other failures go here..
  
  ec_tcpip_unknown                // no valid errorcodes past this entry 
} e_ttc_tcpip_errorcode;
typedef enum {   // e_ttc_tcpip_architecture  types of architectures supported by TCPIP driver
  ta_tcpip_None,           // no architecture selected
  
  ta_tcpip_uip, // automatically added by ./create_DeviceDriver.pl
//InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)
  
  ta_tcpip_unknown        // architecture not supported
} e_ttc_tcpip_architecture;

typedef union  { // 32-bit IP address (IPv4)
    t_u32 All;
    struct {
        t_u8 a;
        t_u8 b;
        t_u8 c;
        t_u8 d;
    } Bytes;
} u_ttc_tcpip_address_IP4;

typedef struct s_ttc_tcpip_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_tcpip_init()
    //       and after ttc_tcpip_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    e_ttc_tcpip_architecture Architecture; // type of architecture used for current tcpip device
    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_TCPIP1, ...)
    t_u8  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)
    
    // low-level configuration (structure defined by low-level driver)
    t_ttc_tcpip_architecture* LowLevelConfig;      
    
    union  { // generic configuration bits common for all low-level Drivers
        t_u16 All;
        struct {
          unsigned Initialized        : 1;  // ==1: device has been initialized successfully
          // ToDo: additional high-level flags go here..
            
            unsigned Reserved1        : 15; // pad to 16 bits
        } Bits;
    } Flags;

    // Additional high-level attributes go here..
    u_ttc_tcpip_address_IP4 IP_Address;         // IP address of local interface

    
} __attribute__((__packed__)) t_ttc_tcpip_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_TCPIP_TYPES_H
