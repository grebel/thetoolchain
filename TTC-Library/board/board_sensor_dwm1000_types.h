#ifndef BOARD_SENSOR_DWM1000_TYPES_H
#define BOARD_SENSOR_DWM1000_TYPES_H

/** { board_sensor_dwm1000.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_board.h providing
 *  Structures, Enums and Defines being required by ttc_board_types.h
 *
 *  wireless sensor node combining stm32l100 + decawave dwm1000 ranging ultrawideband transceiver - architectures
 *
 *  Created from template device_architecture_types.h revision 27 at 20181211 21:03:17 UTC
 *
 *  Note: See ttc_board.h for description of high-level board implementation!
 *  Note: See board_sensor_dwm1000.h for description of sensor_dwm1000 specific board implementation!
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_BOARD1   // device not defined in makefile
    #define TTC_BOARD1    E_ttc_board_architecture_sensor_dwm1000   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#ifndef TTC_BOARD_TYPES_H
    #  error Do not include board_common_types.h directly. Include ttc_board_types.h instead!
#endif

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_board_types.h *************************

typedef struct {  // sensor_dwm1000 specific configuration data of single board device
    t_u8 unused;         // remove me (C structs may not be empty)!
    // add architecture specific configuration fields here..
} __attribute__( ( __packed__ ) ) t_board_sensor_dwm1000_config;

// t_ttc_board_architecture is required by ttc_board_types.h
#define t_ttc_board_architecture t_board_sensor_dwm1000_config

//} Structures/ Enums

#endif //BOARD_SENSOR_DWM1000_TYPES_H
