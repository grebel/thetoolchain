#ifndef BOARD_OLIMEX_STM32_LCD_TYPES_H
#define BOARD_OLIMEX_STM32_LCD_TYPES_H

/** { board_olimex_stm32_lcd.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_board.h providing
 *  Structures, Enums and Defines being required by ttc_board_types.h
 *
 *  prototype board olimex stm32 lcd with 320x240 pixel color touch display and 144 pin stm32f103
 *
 *  Created from template device_architecture_types.h revision 27 at 20180119 01:41:26 UTC
 *
 *  Note: See ttc_board.h for description of high-level board implementation!
 *  Note: See board_olimex_stm32_lcd.h for description of olimex_stm32_lcd specific board implementation!
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_BOARD1   // device not defined in makefile
    #define TTC_BOARD1    E_ttc_board_architecture_olimex_stm32_lcd   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#ifndef TTC_BOARD_TYPES_H
    #  error Do not include board_common_types.h directly. Include ttc_board_types.h instead!
#endif

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_board_types.h *************************

typedef struct {  // olimex_stm32_lcd specific configuration data of single board device
    t_u8 unused;         // remove me (C structs may not be empty)!
    // add architecture specific configuration fields here..
} __attribute__( ( __packed__ ) ) t_board_olimex_stm32_lcd_config;

// t_ttc_board_architecture is required by ttc_board_types.h
#define t_ttc_board_architecture t_board_olimex_stm32_lcd_config

//} Structures/ Enums

#endif //BOARD_OLIMEX_STM32_LCD_TYPES_H
