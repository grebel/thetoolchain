#ifndef BOARD_OLIMEX_STM32_LCD_H
#define BOARD_OLIMEX_STM32_LCD_H

/** { board_olimex_stm32_lcd.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_board.h providing
 *  function prototypes being required by ttc_board.h
 *
 *  Prototype board olimex stm32 lcd with 320x240 pixel color touch display and 144 pin stm32f103.
 *
 *  Created from template device_architecture.h revision 31 at 20180119 01:41:26 UTC
 *
 *  Note: See ttc_board.h for description of olimex_stm32_lcd independent BOARD implementation.
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of board_olimex_stm32_lcd (Do not delete this line!)
 *
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_BOARD_DRIVER_AVAILABLE
#define EXTENSION_BOARD_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_board_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "board_olimex_stm32_lcd.c"
//
#include "../ttc_board_types.h" // will include board_olimex_stm32_lcd_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_board_interface.h
//
// Some driver functions may be optional to implement.
// Check ttc_board_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_board_foo
//
#define ttc_driver_board_configuration_check board_olimex_stm32_lcd_configuration_check
#define ttc_driver_board_deinit board_olimex_stm32_lcd_deinit
#define ttc_driver_board_init board_olimex_stm32_lcd_init
#define ttc_driver_board_load_defaults board_olimex_stm32_lcd_load_defaults
#define ttc_driver_board_prepare board_olimex_stm32_lcd_prepare
#define ttc_driver_board_reset board_olimex_stm32_lcd_reset
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

// Define low-level register access to ili93xx graphic display controller
#define LCDBUS_RSLOW_ADDR   ((volatile t_u16 *) 0x60000000)
#define LCDBUS_RSHIGH_ADDR  ((volatile t_u16 *)(0x60000000 | (1<<(19+1))))
#define gfx_ili93xx_board_write_command(Command)             *LCDBUS_RSLOW_ADDR  = (t_u16) Command
#define gfx_ili93xx_board_write_data08(Data)                 *LCDBUS_RSHIGH_ADDR = (t_u8)  Data;
#define gfx_ili93xx_board_write_data16(Data)                 *LCDBUS_RSHIGH_ADDR = (t_u16) Data;
#define gfx_ili93xx_board_write_register16(Register, Value)  gfx_ili93xx_board_write_command(Register); gfx_ili93xx_board_write_data16(Value)
#define gfx_ili93xx_board_read_register08(Register)          (t_u8) board_olimex_stm32_lcd_ili93xx_read_register16(Register)
#define gfx_ili93xx_board_read_register16(Register)          board_olimex_stm32_lcd_ili93xx_read_register16(Register)
#define gfx_ili93xx_board_read_register32(Register)          board_olimex_stm32_lcd_ili93xx_read_register32(Register)

//} Macro definitions
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_board.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_board.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_board_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_board_features.
 *
 * @param Config        Configuration of board device
 * @return              Fields of *Config have been adjusted if necessary
 */
void board_olimex_stm32_lcd_configuration_check( t_ttc_board_config* Config );


/** shutdown single BOARD unit device
 * @param Config        Configuration of board device
 * @return              == 0: BOARD has been shutdown successfully; != 0: error-code
 */
e_ttc_board_errorcode board_olimex_stm32_lcd_deinit( t_ttc_board_config* Config );


/** initializes single BOARD unit for operation
 * @param Config        Configuration of board device
 * @return              == 0: BOARD has been initialized successfully; != 0: error-code
 */
e_ttc_board_errorcode board_olimex_stm32_lcd_init( t_ttc_board_config* Config );


/** loads configuration of indexed BOARD unit with default values
 * @param Config        Configuration of board device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_board_errorcode board_olimex_stm32_lcd_load_defaults( t_ttc_board_config* Config );


/** Prepares board Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void board_olimex_stm32_lcd_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of board device
 */
void board_olimex_stm32_lcd_reset( t_ttc_board_config* Config );

/** read register content from ili93xx chip
 *
 * Note: This function is required by gfx_ili93xx.h
 *
 * @param Number register address to read from
 * @return content of desired register
 */
t_u16 board_olimex_stm32_lcd_ili93xx_read_register16( t_u8 Number );

/** read register content from ili93xx chip
 *
 * Note: This function is required by gfx_ili93xx.h
 *
 * @param Number register address to read from
 * @return content of desired register
 */
t_u32 board_olimex_stm32_lcd_ili93xx_read_register32( t_u8 Number );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //BOARD_OLIMEX_STM32_LCD_H
