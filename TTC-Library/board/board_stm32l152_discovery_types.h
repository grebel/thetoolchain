#ifndef BOARD_STM32L152_DISCOVERY_TYPES_H
#define BOARD_STM32L152_DISCOVERY_TYPES_H

/** { board_stm32l152_discovery.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_board.h providing
 *  Structures, Enums and Defines being required by ttc_board_types.h
 *
 *  discovery board from st microelectronics with low power stm32l152rbt6, simple lcd and four sensor fields
 *
 *  Created from template device_architecture_types.h revision 25 at 20180420 15:55:39 UTC
 *
 *  Note: See ttc_board.h for description of high-level board implementation.
 *  Note: See board_stm32l152_discovery.h for description of stm32l152_discovery board implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_BOARD1   // device not defined in makefile
    #define TTC_BOARD1    E_ttc_board_architecture_stm32l152_discovery   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_board_types.h *************************

typedef struct {  // stm32l152_discovery specific configuration data of single board device
    t_u8 unused;         // remove me (C structs may not be empty)!
    // add architecture specific configuration fields here..
} __attribute__( ( __packed__ ) ) t_board_stm32l152_discovery_config;

// t_ttc_board_architecture is required by ttc_board_types.h
#define t_ttc_board_architecture t_board_stm32l152_discovery_config

//} Structures/ Enums

#endif //BOARD_STM32L152_DISCOVERY_TYPES_H
