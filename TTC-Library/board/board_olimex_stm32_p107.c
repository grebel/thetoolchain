/** board_olimex_stm32_p107.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_board.c.
 *
 *  prototype board with stm32f107vct6 256kb flash 64 kb ram usb2.0 10/100 ethernet mac sdcard
 *
 *  Created from template device_architecture.c revision 38 at 20180123 14:03:24 UTC
 *
 *  Note: See ttc_board.h for description of high-level board implementation.
 *  Note: See board_olimex_stm32_p107.h for description of olimex_stm32_p107 board implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "board_olimex_stm32_p107.h".
 */

#include "board_olimex_stm32_p107.h"
#include "../ttc_heap.h"              // dynamic memory allocation (e.g. malloc() )
#include "../ttc_sdcard.h"            // secure digital card data storage
#include "../ttc_spi.h"               // serial peripheral interface to connect to sdcard
#include "../ttc_memory.h"            // basic memory checks
#include "../ttc_task.h"              // stack checking (provides TTC_TASK_RETURN() macro)
#include "board_common.h"             // generic functions shared by low-level drivers of all architectures
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_board_features board_olimex_stm32_p107_Features;

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

e_ttc_board_errorcode board_olimex_stm32_p107_load_defaults( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = E_ttc_board_architecture_olimex_stm32_p107;         // set type of architecture
    Config->Features     = &board_olimex_stm32_p107_Features;  // assign feature set

    if ( 0 ) { // enable for memory mapped devices (everything that has a base register)
        switch ( Config->PhysicalIndex ) { // load address of peripheral base register
            //case 0: Config->BaseRegister = & register_olimex_stm32_p107_BOARD1; break;
            //case 1: Config->BaseRegister = & register_olimex_stm32_p107_BOARD2; break;
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid physical index: check your makefile and high-level driver implementation!
        }
    }

    // allocate memory for olimex_stm32_p107 specific configuration
    Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_ttc_board_architecture ) );

    // reset all flags
    ttc_memory_set( &( Config->Flags ), 0, sizeof( Config->Flags ) );

    // set individual feature flags
    //Config->Flags.Foo = 1;
    // add more flags...


    TTC_TASK_RETURN( 0 ); // will perform stack overflow check and return given value
}
void                  board_olimex_stm32_p107_configuration_check( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)


}
e_ttc_board_errorcode board_olimex_stm32_p107_deinit( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
e_ttc_board_errorcode board_olimex_stm32_p107_init( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
void                  board_olimex_stm32_p107_prepare() {

    // initialize SPI bus for use wit SDCARD interface
    t_ttc_spi_config* SPI = ttc_spi_get_configuration( TTC_SDCARD1_SPI_LOGICAL_INDEX );
    SPI->Init.BaudRate = 2000000;
    ttc_spi_init( TTC_SDCARD1_SPI_LOGICAL_INDEX );

#ifdef TTC_SDCARD1
    ttc_sdcard_init( 1 ); // this board has only one sdcard slot
#endif

    TODO( "initialize ethernet interface" );
}
void                  board_olimex_stm32_p107_reset( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    board_olimex_stm32_p107_prepare();
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
