/** board_stm32l100c_discovery.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_board.c.
 *
 *  stm32l100c-disco from st microelectronics
 *
 *  Created from template device_architecture.c revision 36 at 20180420 13:26:22 UTC
 *
 *  Note: See ttc_board.h for description of high-level board implementation.
 *  Note: See board_stm32l100c_discovery.h for description of stm32l100c_discovery board implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "board_stm32l100c_discovery.h".
 */

#include "board_stm32l100c_discovery.h"
#include "../ttc_memory.h"            // basic memory checks
#include "../ttc_task.h"              // stack checking (provides TTC_TASK_RETURN() macro)
#include "../ttc_heap.h"
#include "board_common.h"          // generic functions shared by low-level drivers of all architectures 
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_board_features board_stm32l100c_discovery_Features = {
    /** example features
      .MinBitRate       = 4800,
      .MaxBitRate       = 230400,
    */
    // set more feature values...

    .Unused = 0
};

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

e_ttc_board_errorcode board_stm32l100c_discovery_load_defaults( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = E_ttc_board_architecture_stm32l100c_discovery;         // set type of architecture
    Config->Features     = &board_stm32l100c_discovery_Features;  // assign feature set

    if ( 0 ) { // enable for memory mapped devices (everything that has a base register)
        switch ( Config->PhysicalIndex ) { // load address of peripheral base register
            //case 0: Config->BaseRegister = & register_stm32l100c_discovery_BOARD1; break;
            //case 1: Config->BaseRegister = & register_stm32l100c_discovery_BOARD2; break;
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid physical index: check your makefile and high-level driver implementation!
        }
    }

    // allocate memory for stm32l100c_discovery specific configuration
    Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_ttc_board_architecture ) );

    // reset all flags
    ttc_memory_set( &( Config->Flags ), 0, sizeof( Config->Flags ) );

    // set individual feature flags
    //Config->Flags.Foo = 1;
    // add more flags...


    TTC_TASK_RETURN( E_ttc_board_errorcode_OK ); // will perform stack overflow check + return value
}
void                  board_stm32l100c_discovery_configuration_check( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)



}
e_ttc_board_errorcode board_stm32l100c_discovery_deinit( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)


    return ( e_ttc_board_errorcode ) 0;
}
e_ttc_board_errorcode board_stm32l100c_discovery_init( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)


    return ( e_ttc_board_errorcode ) 0;
}
void                  board_stm32l100c_discovery_prepare() {




}
void                  board_stm32l100c_discovery_reset( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)



}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
