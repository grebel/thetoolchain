#ifndef BOARD_SENSOR_DWM1000_H
#define BOARD_SENSOR_DWM1000_H

/** { board_sensor_dwm1000.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_board.h providing
 *  function prototypes being required by ttc_board.h
 *
 *  wireless sensor node combining stm32l100 + decawave dwm1000 ranging ultrawideband transceiver - architectures
 *
 *  Created from template device_architecture.h revision 31 at 20181211 21:55:54 UTC
 *
 *  Note: See ttc_board.h for description of sensor_dwm1000 independent BOARD implementation.
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of board_sensor_dwm1000 (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_BOARD_DRIVER_AVAILABLE
#define EXTENSION_BOARD_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_board_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "board_sensor_dwm1000.c"
//
#include "../ttc_board_types.h" // will include board_sensor_dwm1000_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_board_interface.h
//
// Some driver functions may be optional to implement.
// Check ttc_board_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_board_foo
//
#define ttc_driver_board_configuration_check board_sensor_dwm1000_configuration_check
#define ttc_driver_board_deinit board_sensor_dwm1000_deinit
#define ttc_driver_board_init board_sensor_dwm1000_init
#define ttc_driver_board_load_defaults board_sensor_dwm1000_load_defaults
#define ttc_driver_board_prepare board_sensor_dwm1000_prepare
#define ttc_driver_board_reset board_sensor_dwm1000_reset
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_board.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_board.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_board_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_board_features.
 *
 * @param Config        Configuration of board device
 * @return              Fields of *Config have been adjusted if necessary
 */
void board_sensor_dwm1000_configuration_check( t_ttc_board_config* Config );


/** shutdown single BOARD unit device
 * @param Config        Configuration of board device
 * @return              == 0: BOARD has been shutdown successfully; != 0: error-code
 */
e_ttc_board_errorcode board_sensor_dwm1000_deinit( t_ttc_board_config* Config );


/** initializes single BOARD unit for operation
 * @param Config        Configuration of board device
 * @return              == 0: BOARD has been initialized successfully; != 0: error-code
 */
e_ttc_board_errorcode board_sensor_dwm1000_init( t_ttc_board_config* Config );


/** loads configuration of indexed BOARD unit with default values
 * @param Config        Configuration of board device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_board_errorcode board_sensor_dwm1000_load_defaults( t_ttc_board_config* Config );


/** Prepares board Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void board_sensor_dwm1000_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of board device
 */
void board_sensor_dwm1000_reset( t_ttc_board_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //BOARD_SENSOR_DWM1000_H
