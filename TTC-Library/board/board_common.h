#ifndef board_common_h
#define board_common_h

/** board_common.h ****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to board low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.h revision 14 at 20180420 13:26:22 UTC
 *
 *  Authors: GREGOR REBEL
 *
 *  Description of board_common (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "board_stm32l100c_discovery.c"
 */

#include "../ttc_basic.h"
#include "../ttc_board_types.h" // will include board_common_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_board_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_board_foo
 */

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions
/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_board.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_board.h for details.
 *
 * You normally should not need to add functions here.
 * Global feature functions must be added in ttc_board.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl board UPDATE".
 */

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //board_common_h
