/** board_olimex_stm32_lcd.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_board.c.
 *
 *  prototype board olimex stm32 lcd with 320x240 pixel color touch display and 144 pin stm32f103
 *
 *  Created from template device_architecture.c revision 38 at 20180119 01:41:26 UTC
 *
 *  Note: See ttc_board.h for description of high-level board implementation.
 *  Note: See board_olimex_stm32_lcd.h for description of olimex_stm32_lcd board implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "board_olimex_stm32_lcd.h".
 */

#include "board_olimex_stm32_lcd.h"
#include "../ttc_gpio.h"              // dynamic memory allocation (e.g. malloc() )
#include "../ttc_heap.h"              // dynamic memory allocation (e.g. malloc() )
#include "../ttc_memory.h"            // basic memory checks
#include "../ttc_task.h"              // stack checking (provides TTC_TASK_RETURN() macro)
#include "board_common.h"             // generic functions shared by low-level drivers of all architectures
#include "additionals/250_stm_std_peripherals/inc/stm32f10x_fsmc.h"           // standard peripheral driver for Flexible Static Memory Controller
#include "compile_options.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

/*
const t_ttc_board_features board_olimex_stm32_lcd_Features = {
    // set more feature values...

    .unused = 0
};
*/

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

/** Initialize Flexible Static Memory Controller for attached ili93xx graphic controller
 *
 * This board provides an ili93xx graphic controller with 320x240 pixel color touch display.
 * Communication with ili93xx uses fsmc feature of stm31f1xx.
 * This function will initialize fsmc to allow board_olimex_stm32_lcd_ili93xx_board_write_*() to be used.
 */
void _board_olimex_stm32_lcd_init_fsmc();
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

e_ttc_board_errorcode  board_olimex_stm32_lcd_load_defaults( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = E_ttc_board_architecture_olimex_stm32_lcd;         // set type of architecture
    //X Config->Features     = &board_olimex_stm32_lcd_Features;  // assign feature set


    // allocate memory for olimex_stm32_lcd specific configuration
    Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_board_olimex_stm32_lcd_config ) );

    // reset all flags
    ttc_memory_set( &( Config->Flags ), 0, sizeof( Config->Flags ) );

    // set individual feature flags
    //Config->Flags.Foo = 1;
    // add more flags...


    TTC_TASK_RETURN( 0 ); // will perform stack overflow check and return given value
}
void                   board_olimex_stm32_lcd_configuration_check( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)



}
e_ttc_board_errorcode  board_olimex_stm32_lcd_deinit( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)


    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
e_ttc_board_errorcode  board_olimex_stm32_lcd_init( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
void                   board_olimex_stm32_lcd_prepare() {

    _board_olimex_stm32_lcd_init_fsmc();
}
void                   board_olimex_stm32_lcd_reset( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)



}
t_u16                  board_olimex_stm32_lcd_ili93xx_read_register16( t_u8 Number )  {

    *LCDBUS_RSLOW_ADDR = Number;

    return *LCDBUS_RSHIGH_ADDR;
}
t_u32                  board_olimex_stm32_lcd_ili93xx_read_register32( t_u8 Number )  {

    *LCDBUS_RSLOW_ADDR = Number;

    t_u32 Data;
    Data  = (*LCDBUS_RSHIGH_ADDR) << 16;
    Data |= (*LCDBUS_RSHIGH_ADDR);

    return Data;
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

void _board_olimex_stm32_lcd_init_fsmc() {

#define FSMC_STM32F1XX_D00 E_ttc_gpio_pin_d14
#define FSMC_STM32F1XX_D01 E_ttc_gpio_pin_d15
#define FSMC_STM32F1XX_D02 E_ttc_gpio_pin_d0
#define FSMC_STM32F1XX_D03 E_ttc_gpio_pin_d1
#define FSMC_STM32F1XX_D04 E_ttc_gpio_pin_e7
#define FSMC_STM32F1XX_D05 E_ttc_gpio_pin_e8
#define FSMC_STM32F1XX_D06 E_ttc_gpio_pin_e9
#define FSMC_STM32F1XX_D07 E_ttc_gpio_pin_e10
#define FSMC_STM32F1XX_D08 E_ttc_gpio_pin_e11
#define FSMC_STM32F1XX_D09 E_ttc_gpio_pin_e12
#define FSMC_STM32F1XX_D10 E_ttc_gpio_pin_e13
#define FSMC_STM32F1XX_D11 E_ttc_gpio_pin_e14
#define FSMC_STM32F1XX_D12 E_ttc_gpio_pin_e15
#define FSMC_STM32F1XX_D13 E_ttc_gpio_pin_d8
#define FSMC_STM32F1XX_D14 E_ttc_gpio_pin_d9
#define FSMC_STM32F1XX_D15 E_ttc_gpio_pin_d10
#define FSMC_STM32F1XX_A00 E_ttc_gpio_pin_f0
#define FSMC_STM32F1XX_A01 E_ttc_gpio_pin_f1
#define FSMC_STM32F1XX_A02 E_ttc_gpio_pin_f2
#define FSMC_STM32F1XX_A03 E_ttc_gpio_pin_f3
#define FSMC_STM32F1XX_A04 E_ttc_gpio_pin_f4
#define FSMC_STM32F1XX_A05 E_ttc_gpio_pin_f5
#define FSMC_STM32F1XX_A06 E_ttc_gpio_pin_f12
#define FSMC_STM32F1XX_A07 E_ttc_gpio_pin_f13
#define FSMC_STM32F1XX_A08 E_ttc_gpio_pin_f14
#define FSMC_STM32F1XX_A09 E_ttc_gpio_pin_f15
#define FSMC_STM32F1XX_A10 E_ttc_gpio_pin_g0
#define FSMC_STM32F1XX_A11 E_ttc_gpio_pin_g1
#define FSMC_STM32F1XX_A12 E_ttc_gpio_pin_g2
#define FSMC_STM32F1XX_A13 E_ttc_gpio_pin_g3
#define FSMC_STM32F1XX_A14 E_ttc_gpio_pin_g4
#define FSMC_STM32F1XX_A15 E_ttc_gpio_pin_g5
#define FSMC_STM32F1XX_NWE E_ttc_gpio_pin_d5
#define FSMC_STM32F1XX_NOE E_ttc_gpio_pin_d4
#define FSMC_STM32F1XX_NE1 E_ttc_gpio_pin_d7

    #if (TTC_GFX1_ILI93XX_PIN_D00 != FSMC_STM32F1XX_D00)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D00. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D01 != FSMC_STM32F1XX_D01)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D01. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D02 != FSMC_STM32F1XX_D02)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D02. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D03 != FSMC_STM32F1XX_D03)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D03. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D04 != FSMC_STM32F1XX_D04)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D04. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D05 != FSMC_STM32F1XX_D05)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D05. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D06 != FSMC_STM32F1XX_D06)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D06. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D07 != FSMC_STM32F1XX_D07)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D07. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D08 != FSMC_STM32F1XX_D08)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D08. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D09 != FSMC_STM32F1XX_D09)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D09. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D10 != FSMC_STM32F1XX_D10)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D10. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D11 != FSMC_STM32F1XX_D11)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D11. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D12 != FSMC_STM32F1XX_D12)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D12. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D13 != FSMC_STM32F1XX_D13)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D13. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D14 != FSMC_STM32F1XX_D14)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D14. Select pin usable for FSMC or disable FSMC!
    #endif
    #if (TTC_GFX1_ILI93XX_PIN_D15 != FSMC_STM32F1XX_D15)
#  error Invalid pin configured for GFX_ILI93xxX_PIN_D15. Select pin usable for FSMC or disable FSMC!
    #endif

    // SRAM Data lines configuration

    const e_ttc_gpio_pin SRAM_GPIOs[] = {
        // Layout Prototypeboard Olimex STM32-LCD
        TTC_GFX1_ILI93XX_PIN_REGISTER_SELECT,
        TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE,
        TTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE,
        TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT,
        TTC_GFX1_ILI93XX_PIN_NBL0,
        TTC_GFX1_ILI93XX_PIN_NBL1,
        TTC_GFX1_ILI93XX_PIN_D00, TTC_GFX1_ILI93XX_PIN_D01, TTC_GFX1_ILI93XX_PIN_D02, TTC_GFX1_ILI93XX_PIN_D03,
        TTC_GFX1_ILI93XX_PIN_D04, TTC_GFX1_ILI93XX_PIN_D05, TTC_GFX1_ILI93XX_PIN_D06, TTC_GFX1_ILI93XX_PIN_D07,
        TTC_GFX1_ILI93XX_PIN_D08, TTC_GFX1_ILI93XX_PIN_D09, TTC_GFX1_ILI93XX_PIN_D10, TTC_GFX1_ILI93XX_PIN_D11,
        TTC_GFX1_ILI93XX_PIN_D12, TTC_GFX1_ILI93XX_PIN_D13, TTC_GFX1_ILI93XX_PIN_D14, TTC_GFX1_ILI93XX_PIN_D15,
        0
    };
    const e_ttc_gpio_pin* SRAM_GPIO = SRAM_GPIOs;
    while ( *SRAM_GPIO ) {
        ttc_gpio_init( *SRAM_GPIO++, E_ttc_gpio_mode_alternate_function_push_pull, E_ttc_gpio_speed_max );
    }

    // Enable the FSMC Clock
    register_stm32f1xx_RCC.AHBENR.FSMC_EN = 1;

    // FSMC Configuration
    FSMC_NORSRAMInitTypeDef  FSMC_NORSRAMInitStructure;
    FSMC_NORSRAMTimingInitTypeDef  pw, pr;

#define T_HCK   1 // ?

    pr.FSMC_AddressSetupTime      = ( 5 / T_HCK + 1 );
    pr.FSMC_AddressHoldTime       = ( 5 / T_HCK + 1 );
    pr.FSMC_DataSetupTime         = ( 100 / T_HCK + 1 );
    pr.FSMC_BusTurnAroundDuration = 0;
    pr.FSMC_CLKDivision           = 1;
    pr.FSMC_DataLatency           = 0;
    pr.FSMC_AccessMode            = FSMC_AccessMode_A;

    pw.FSMC_AddressSetupTime      = 0;
    pw.FSMC_AddressHoldTime       = 1;
    pw.FSMC_DataSetupTime         = 1;
    pw.FSMC_BusTurnAroundDuration = 0;
    pw.FSMC_CLKDivision           = 1;
    pw.FSMC_DataLatency           = 0;
    pw.FSMC_AccessMode            = FSMC_AccessMode_A;

    FSMC_NORSRAMInitStructure.FSMC_Bank                     = FSMC_Bank1_NORSRAM2;
    FSMC_NORSRAMInitStructure.FSMC_DataAddressMux           = FSMC_DataAddressMux_Disable;
    FSMC_NORSRAMInitStructure.FSMC_MemoryType               = FSMC_MemoryType_SRAM;
    FSMC_NORSRAMInitStructure.FSMC_MemoryDataWidth          = FSMC_MemoryDataWidth_16b;
    FSMC_NORSRAMInitStructure.FSMC_BurstAccessMode          = FSMC_BurstAccessMode_Disable;
    FSMC_NORSRAMInitStructure.FSMC_WaitSignalPolarity       = FSMC_WaitSignalPolarity_Low;
    FSMC_NORSRAMInitStructure.FSMC_WrapMode                 = FSMC_WrapMode_Disable;
    FSMC_NORSRAMInitStructure.FSMC_WaitSignalActive         = FSMC_WaitSignalActive_BeforeWaitState;
    FSMC_NORSRAMInitStructure.FSMC_WriteOperation           = FSMC_WriteOperation_Enable;
    FSMC_NORSRAMInitStructure.FSMC_WaitSignal               = FSMC_WaitSignal_Disable;
    FSMC_NORSRAMInitStructure.FSMC_ExtendedMode             = FSMC_ExtendedMode_Enable;
    FSMC_NORSRAMInitStructure.FSMC_WriteBurst               = FSMC_WriteBurst_Enable;
    FSMC_NORSRAMInitStructure.FSMC_AsynchronousWait         = FSMC_AsynchronousWait_Disable;
    FSMC_NORSRAMInitStructure.FSMC_ReadWriteTimingStruct    = &pr;
    FSMC_NORSRAMInitStructure.FSMC_WriteTimingStruct        = &pw;

    FSMC_NORSRAMInit( &FSMC_NORSRAMInitStructure );

    // Enable FSMC Bank1_SRAM Bank
    FSMC_NORSRAMCmd( FSMC_Bank1_NORSRAM2, ENABLE );
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
