#ifndef BOARD_DSO_0138_TYPES_H
#define BOARD_DSO_0138_TYPES_H

/** { board_dso_0138.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_board.h providing
 *  Structures, Enums and Defines being required by ttc_board_types.h
 *
 *  digital signal oscilloscope with stm31f103c8t6, lcd 320x240 (), precision analog input, 4 buttons, uart, mini-usb
 *
 *  Created from template device_architecture_types.h revision 27 at 20180122 03:20:31 UTC
 *
 *  Note: See ttc_board.h for description of high-level board implementation!
 *  Note: See board_dso_0138.h for description of dso_0138 specific board implementation!
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_BOARD1   // device not defined in makefile
    #define TTC_BOARD1    E_ttc_board_architecture_dso_0138   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#ifndef TTC_BOARD_TYPES_H
    #  error Do not include board_common_types.h directly. Include ttc_board_types.h instead!
#endif

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_board_types.h *************************

typedef struct {  // dso_0138 specific configuration data of single board device
    t_u16 LCD_IdCode;         // ID_CODE being read from connected LCD controller (should be 0x9341)
    // add architecture specific configuration fields here..
} __attribute__( ( __packed__ ) ) t_board_dso_0138_config;

// t_ttc_board_architecture is required by ttc_board_types.h
#define t_ttc_board_architecture t_board_dso_0138_config

//} Structures/ Enums

#endif //BOARD_DSO_0138_TYPES_H
