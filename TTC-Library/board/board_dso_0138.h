#ifndef BOARD_DSO_0138_H
#define BOARD_DSO_0138_H

/** { board_dso_0138.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_board.h providing
 *  function prototypes being required by ttc_board.h
 *
 *  digital signal oscilloscope with stm31f103c8t6, lcd 320x240 (), precision analog input, 4 buttons, uart, mini-usb
 *
 *  Created from template device_architecture.h revision 32 at 20180122 03:20:31 UTC
 *
 *  Note: See ttc_board.h for description of dso_0138 independent BOARD implementation.
 *
 *  Authors: GREGOR REBEL
 *
 */
/** Description of board_dso_0138 (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_BOARD_DRIVER_AVAILABLE
#define EXTENSION_BOARD_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_board_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "board_dso_0138.c"
//
#include "../ttc_board_types.h" // will include board_dso_0138_types.h (do not include it directly!)
#include "../ttc_gpio.h"
#include "../ttc_gfx_types.h"
#include "compile_options.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************


// LCD support macros (see ttc-lib/gfx/gfx_ili93xx.h for descriptions!)
#define gfx_ili93xx_board_write_command(Command)             board_dso_0138_ili93xx_write_command(Command)
#define gfx_ili93xx_board_write_data08(Data)                 board_dso_0138_ili93xx_write_data08(Data)
#define gfx_ili93xx_board_write_data16(Data)                 board_dso_0138_ili93xx_write_data16(Data)
#define gfx_ili93xx_board_read_register08(Register)          board_dso_0138_ili93xx_read_register08(Register)
#define gfx_ili93xx_board_read_register16(Register)          board_dso_0138_ili93xx_read_register16(Register)
#define gfx_ili93xx_board_read_register32(Register)          board_dso_0138_ili93xx_read_register32(Register)

#if (TTC_ASSERT_GFX_EXTRA == 1)  // use functions for all low-level macros (allows to use them in gdb session)
    #define gfx_ili93xx_board_write_register08(Register, Value)  board_dso_0138_ili93xx_write_register08(Register, Value)
    #define gfx_ili93xx_board_write_register16(Register, Value)  board_dso_0138_ili93xx_write_register16(Register, Value)
    #define gfx_ili93xx_board_select(Config)                     board_dso_0138_ili93xx_select()
    #define gfx_ili93xx_board_deselect(Config)                   board_dso_0138_ili93xx_deselect()
#else
    #define gfx_ili93xx_board_write_register08(Register, Value)  gfx_ili93xx_board_write_command(Register); gfx_ili93xx_board_write_data08(Value)
    #define gfx_ili93xx_board_write_register16(Register, Value)  gfx_ili93xx_board_write_command(Register); gfx_ili93xx_board_write_data16(Value)
    #define gfx_ili93xx_board_select(Config, Select)             ( ttc_gpio_put(TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT, (1-(Select)) ) )
#endif

// define all supported low-level functions for ttc_board_interface.h
//
// Some driver functions may be optional to implement.
// Check ttc_board_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_board_foo
//
#define ttc_driver_board_configuration_check board_dso_0138_configuration_check
#define ttc_driver_board_deinit board_dso_0138_deinit
#define ttc_driver_board_init board_dso_0138_init
#define ttc_driver_board_load_defaults board_dso_0138_load_defaults
#define ttc_driver_board_prepare board_dso_0138_prepare
#define ttc_driver_board_reset board_dso_0138_reset
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Macro definitions
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_board.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_board.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_board_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_board_features.
 *
 * @param Config        Configuration of board device
 * @return              Fields of *Config have been adjusted if necessary
 */
void board_dso_0138_configuration_check( t_ttc_board_config* Config );

/** shutdown single BOARD unit device
 * @param Config        Configuration of board device
 * @return              == 0: BOARD has been shutdown successfully; != 0: error-code
 */
e_ttc_board_errorcode board_dso_0138_deinit( t_ttc_board_config* Config );

/** initializes single BOARD unit for operation
 * @param Config        Configuration of board device
 * @return              == 0: BOARD has been initialized successfully; != 0: error-code
 */
e_ttc_board_errorcode board_dso_0138_init( t_ttc_board_config* Config );

/** loads configuration of indexed BOARD unit with default values
 * @param Config        Configuration of board device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_board_errorcode board_dso_0138_load_defaults( t_ttc_board_config* Config );

/** Prepares board Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void board_dso_0138_prepare();

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of board device
 */
void board_dso_0138_reset( t_ttc_board_config* Config );

/** Send command to ili93xx
 *
 * @param Command
 */
void board_dso_0138_ili93xx_write_command( t_u8 Command );

/** Send 8 bit data to ili93xx
 *
 * @param Data   byte to send
 */
void  board_dso_0138_ili93xx_write_data08( t_u8 Data );

/** Send 16 bit data to ili93xx
 *
 * @param Data   word to send
 */
void  board_dso_0138_ili93xx_write_data16( t_u16 Data );

/** Read 8 bit data from ili93xx register
 *
 * @param Number number of register to read from (-> ttc-lib/gfx/gfx_ili93xx_types.h)
 * @return  register content read from ili93xx
 */
t_u8  board_dso_0138_ili93xx_read_register08( t_u8 Number );

/** Read 16 bit data from ili93xx register
 *
 * @param Number number of register to read from (-> ttc-lib/gfx/gfx_ili93xx_types.h)
 * @return  register content read from ili93xx
 */
t_u16 board_dso_0138_ili93xx_read_register16( t_u8 Number );

/** Read 32 bit data from ili93xx register
 *
 * @param Number number of register to read from (-> ttc-lib/gfx/gfx_ili93xx_types.h)
 * @return  register content read from ili93xx
 */
t_u32 board_dso_0138_ili93xx_read_register32( t_u8 Number );

/** Enable/ disable chipselect line to single ili93xx
 *
 * @param Config     pointer to struct t_ttc_gfx_config
 * @param Select     ==1: ili93xx is listening to data bus; ==0: ili93xx now is passive
*/
//void board_dso_0138_ili93xx_select();
#define board_dso_0138_ili93xx_select()   ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT )

//void board_dso_0138_ili93xx_deselect();
#define board_dso_0138_ili93xx_deselect() ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT )

/** Send 8 bit data to ili93xx
 *
 * @param Number  number of register to read from (-> ttc-lib/gfx/gfx_ili93xx_types.h)
 * @param Value   data to send to ili93xx
 */
void board_dso_0138_ili93xx_write_register08( e_gfx_ili93xx_register Register, t_u8 Value );

/** Send 17 bit data to ili93xx
 *
 * @param Number  number of register to read from (-> ttc-lib/gfx/gfx_ili93xx_types.h)
 * @param Value   data to send to ili93xx
 */
void board_dso_0138_ili93xx_write_register16( e_gfx_ili93xx_register Register, t_u16 Value );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //BOARD_DSO_0138_H
