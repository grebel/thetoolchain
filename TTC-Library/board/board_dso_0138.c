/** board_dso_0138.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_board.c.
 *
 *  digital signal oscilloscope with stm31f103c8t6, lcd 320x240 (), precision analog input, 4 buttons, uart, mini-usb
 *
 *  Created from template device_architecture.c revision 38 at 20180122 03:20:31 UTC
 *
 *  Note: See ttc_board.h for description of high-level board implementation.
 *  Note: See board_dso_0138.h for description of dso_0138 board implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "board_dso_0138.h".
 */

#include "board_dso_0138.h"
#include "compile_options.h"
#include "../ttc_heap.h"           // Dynamic memory allocation (e.g. malloc() )
#include "../ttc_memory.h"         // Basic memory checks
#include "../ttc_gpio.h"           // GPIO pin configuration
#include "../ttc_usart.h"          // Serial port configuration
#include "../ttc_sysclock.h"       // small delays with low overhead
#include "../ttc_task.h"           // Stack checking (provides TTC_TASK_RETURN() macro)
#include "board_common.h"          // Generic functions shared by low-level drivers of all architectures
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_board_features board_dso_0138_Features;

const e_ttc_gpio_pin board_dso_0138_Pins_Output[] = { // all individual GPIO pins to configure as output push pull
    TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE,     // b10
    TTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE,    // c15
    TTC_GFX1_ILI93XX_PIN_NOT_RESET,           // b11
    TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT,      // c13
    TTC_GFX1_ILI93XX_PIN_REGISTER_SELECT,     // c14
    TTC_GFX1_ILI93XX_PIN_D00,                 // b0
    TTC_GFX1_ILI93XX_PIN_D01,                 // b1
    TTC_GFX1_ILI93XX_PIN_D02,                 // b2
    TTC_GFX1_ILI93XX_PIN_D03,                 // b3
    TTC_GFX1_ILI93XX_PIN_D04,                 // b4
    TTC_GFX1_ILI93XX_PIN_D05,                 // b5
    TTC_GFX1_ILI93XX_PIN_D06,                 // b6
    TTC_GFX1_ILI93XX_PIN_D07,                 // b7
    BOARD_DSO_0138_PIN_TESTSIG,               // a7
    BOARD_DSO_0138_PIN_TL_PWM,                // b8
    BOARD_DSO_0138_PIN_VGEN,                  // b9
    TTC_LED1,                                 // a15
    0
};

const e_ttc_gpio_pin board_dso_0138_Pins_Input[] = { // all individual GPIO pins to configure as input
    BOARD_DSO_0138_PIN_TP11,                 // a4
    BOARD_DSO_0138_PIN_TP12,                 // a5
    BOARD_DSO_0138_PIN_TP13,                 // a6
    BOARD_DSO_0138_PIN_TRIG,                 // a8
    TTC_SWITCH1,                             // b15
    TTC_SWITCH2,                             // b14
    TTC_SWITCH3,                             // b13
    TTC_SWITCH4,                             // b12
    TTC_ADC1,                                // a0
    TTC_ADC2,                                // a1
    TTC_ADC3,                                // a2
    0
};
//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

// send 8 bit data to ili93xx (bus must already being switched to output mode!)
void _board_dso_0138_write8( t_u8 Data ) {
    ttc_gpio_parallel08_1_put( Data );
    ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE );
    ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE );
}

// read 8 bit data from ili93xx (bus must already being switched to input mode!)
t_u8 _board_dso_0138_read8() {
    ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE );
    ttc_sysclock_udelay( 50 );

    t_u8 Data = ttc_gpio_parallel08_1_get();
    ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE );

    return Data;
}

#define BOARD_DSO_0138_PIN_EXTRA_DATA 0 // ==0: debug only current value of each used gpio pin ==1: current value + complete GPIO pin configurations
#if (BOARD_DSO_0138_PIN_EXTRA_DATA==1)
typedef struct { // t_board_dso_0138_lcd_pin_gdb  - structure storing current configuration of single LCD GPIO pin (for use in gdb session only)
    e_ttc_gpio_pin                 Pin;
    e_ttc_gpio_mode                Mode;
    e_ttc_gpio_speed               Speed;
    e_ttc_gpio_alternate_function  Function;
    t_u8                           Value;
} t_board_dso_0138_lcd_pin_gdb;
#else
typedef t_u8 t_board_dso_0138_lcd_pin_gdb;
#endif

typedef struct { // t_board_dso_0138_PinStates  - structure storing current configuration of all LCD GPIO pins (for use in gdb session only)
    t_board_dso_0138_lcd_pin_gdb  NotReadStrobe;
    t_board_dso_0138_lcd_pin_gdb  NotWriteStrobe;
    t_board_dso_0138_lcd_pin_gdb  NotReset;
    t_board_dso_0138_lcd_pin_gdb  NotChipSelect;
    t_board_dso_0138_lcd_pin_gdb  RegisterSelect;
    t_board_dso_0138_lcd_pin_gdb  D0;
    t_board_dso_0138_lcd_pin_gdb  D1;
    t_board_dso_0138_lcd_pin_gdb  D2;
    t_board_dso_0138_lcd_pin_gdb  D3;
    t_board_dso_0138_lcd_pin_gdb  D4;
    t_board_dso_0138_lcd_pin_gdb  D5;
    t_board_dso_0138_lcd_pin_gdb  D6;
    t_board_dso_0138_lcd_pin_gdb  D7;
    t_board_dso_0138_lcd_pin_gdb  TestSig;
    t_board_dso_0138_lcd_pin_gdb  TL_PWM;
    t_board_dso_0138_lcd_pin_gdb  VGEN;
    t_board_dso_0138_lcd_pin_gdb  LED1;
    t_board_dso_0138_lcd_pin_gdb  TP11;
    t_board_dso_0138_lcd_pin_gdb  TP12;
    t_board_dso_0138_lcd_pin_gdb  TP13;
    t_board_dso_0138_lcd_pin_gdb  TRIG;
    t_board_dso_0138_lcd_pin_gdb  Switch1;
    t_board_dso_0138_lcd_pin_gdb  Switch2;
    t_board_dso_0138_lcd_pin_gdb  Switch3;
    t_board_dso_0138_lcd_pin_gdb  Switch4;
    t_board_dso_0138_lcd_pin_gdb  Analog1;
    t_board_dso_0138_lcd_pin_gdb  Analog2;
    t_board_dso_0138_lcd_pin_gdb  Analog3;
} t_board_dso_0138_PinStates;

/** helper function to be called from _board_dso_0138_pins_gdb() only
 *
 * @param Source  current gpio pin configuration as returned by ttc_gpio_get_configuration()
 * @param Target  where to store interesting data
 */
void _board_dso_0138_copy_pin_data( t_ttc_gpio_config* Source, t_board_dso_0138_lcd_pin_gdb* Target );

/** collects configuration of all LCD gpio pins to be displayed in a gdb session
 *
 * Example usage:
 * gdb> p *_board_dso_0138_pins_gdb()
 */
t_board_dso_0138_PinStates* _board_dso_0138_pins_gdb();

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

e_ttc_board_errorcode  board_dso_0138_load_defaults( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = E_ttc_board_architecture_dso_0138;         // set type of architecture
    Config->Features     = &board_dso_0138_Features;  // assign feature set

    if ( 0 ) { // enable for memory mapped devices (everything that has a base register)
        switch ( Config->PhysicalIndex ) { // load address of peripheral base register
            //case 0: Config->BaseRegister = & register_dso_0138_BOARD1; break;
            //case 1: Config->BaseRegister = & register_dso_0138_BOARD2; break;
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid physical index: check your makefile and high-level driver implementation!
        }
    }

    // allocate memory for dso_0138 specific configuration
    Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_ttc_board_architecture ) );

    // reset all flags
    ttc_memory_set( &( Config->Flags ), 0, sizeof( Config->Flags ) );

    // set individual feature flags
    //Config->Flags.Foo = 1;
    // add more flags...


    TTC_TASK_RETURN( 0 ); // will perform stack overflow check and return given value
}
void                   board_dso_0138_configuration_check( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)


}
e_ttc_board_errorcode  board_dso_0138_deinit( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    board_dso_0138_reset( Config );
    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
e_ttc_board_errorcode  board_dso_0138_init( t_ttc_board_config* Config ) {
    Assert_BOARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    board_dso_0138_reset( Config );

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
void                   board_dso_0138_prepare() {
    ttc_gpio_prepare(); // reset gpio port configuration

    if ( 1 ) { // disable JTAG interface to free some gpio pins (required to pass gpio pin tests below))
        register_stm32f1xx_RCC.APB2ENR.AFIO_EN = 1;     // enable Alternate Function IO configuration interface
        register_stm32f1xx_AFIO .MAPR.SWJ_CFG  = 0b010; // disable JTAG but keep SWD active
    }
    if ( 1 ) { // configure + selftest gpio pins
        const e_ttc_gpio_pin* PinOutput = board_dso_0138_Pins_Output;
        while ( *PinOutput ) { // initialize + test all output ports and set them to 3.3V
            ttc_gpio_init( *PinOutput, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );

            ttc_gpio_clr( *PinOutput );
            ttc_sysclock_udelay( 10000 ); // give line 10ms to accept new voltage
            if ( ttc_gpio_get( *PinOutput ) ) { // ERROR: Cannot clear gpio pin
                ttc_gpio_init( *PinOutput, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_max );
                Assert_BOARD( 0, ttc_assert_origin_auto ); // Cannot lower voltage on output port. Is there an electrical defect?
            }

            ttc_gpio_set( *PinOutput );
            ttc_sysclock_udelay( 10000 ); // give line 10ms to accept new voltage
            if ( !ttc_gpio_get( *PinOutput ) ) { // ERROR: Cannot set gpio pin
                ttc_gpio_init( *PinOutput, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_max );
                Assert_BOARD( 0, ttc_assert_origin_auto ); // Cannot rise voltage on output port. Is there an electrical defect?
            }

            PinOutput++;
        }

        ttc_gpio_init( TTC_LED1,                   E_ttc_gpio_mode_output_open_drain, E_ttc_gpio_speed_max );
        ttc_gpio_init( BOARD_DSO_0138_PIN_TP11,    E_ttc_gpio_mode_input_floating,    E_ttc_gpio_speed_max );
        ttc_gpio_init( BOARD_DSO_0138_PIN_TP12,    E_ttc_gpio_mode_input_floating,    E_ttc_gpio_speed_max );
        ttc_gpio_init( BOARD_DSO_0138_PIN_TP13,    E_ttc_gpio_mode_input_floating,    E_ttc_gpio_speed_max );
        ttc_gpio_init( BOARD_DSO_0138_PIN_TRIG,    E_ttc_gpio_mode_input_floating,    E_ttc_gpio_speed_max );
        ttc_gpio_init( TTC_SWITCH1,                E_ttc_gpio_mode_input_pull_up,     E_ttc_gpio_speed_max );
        ttc_gpio_init( TTC_SWITCH2,                E_ttc_gpio_mode_input_pull_up,     E_ttc_gpio_speed_max );
        ttc_gpio_init( TTC_SWITCH3,                E_ttc_gpio_mode_input_pull_up,     E_ttc_gpio_speed_max );
        ttc_gpio_init( TTC_SWITCH4,                E_ttc_gpio_mode_input_pull_up,     E_ttc_gpio_speed_max );
        ttc_gpio_init( TTC_ADC1,                   E_ttc_gpio_mode_analog_in,         E_ttc_gpio_speed_max );
        ttc_gpio_init( TTC_ADC2,                   E_ttc_gpio_mode_analog_in,         E_ttc_gpio_speed_max );
        ttc_gpio_init( TTC_ADC3,                   E_ttc_gpio_mode_analog_in,         E_ttc_gpio_speed_max );

        // configure 8-bit data bus to lcd
        ttc_gpio_parallel08_init( 1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
        ttc_gpio_parallel08_1_put( 0xff );
    }
}
void                   board_dso_0138_reset( t_ttc_board_config* Config ) {
    ( void ) Config;

    board_dso_0138_prepare();
    if ( 1 ) { // initialize serial port (115200 bit/s, 8n1)
        t_ttc_usart_config* USART = ttc_usart_get_configuration( 1 );
        USART->Init.BaudRate            = 115200;
        USART->Init.HalfStopBits        = 2;
        USART->Init.Flags.ControlParity = 0;
        USART->Init.Flags.Transmit      = 1;
        USART->Init.Flags.Receive       = 1;
        ttc_usart_init( 1 );
    }

    if ( 1 ) { // reset ili93xx
        ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_RESET );
        ttc_sysclock_udelay( 10000 ); // 10ms
        ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_NOT_RESET );
        ttc_sysclock_udelay( 10000 ); // 10ms
        ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_RESET );
        ttc_sysclock_udelay( 500000 ); // 50ms
    }
    if ( 1 ) { // detect current ili93xx chip version
        ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT );
        Config->LowLevelConfig->dso_0138.LCD_IdCode = ( t_u16 ) board_dso_0138_ili93xx_read_register32( 0xd3 );
        ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT );
    }
    if ( Config->LowLevelConfig->dso_0138.LCD_IdCode == 0x9341 ) { // LCD found: basic LCD setup
        board_dso_0138_ili93xx_select();
        board_dso_0138_ili93xx_write_command( 0xcf );
        board_dso_0138_ili93xx_write_data08( 0x00 );
        board_dso_0138_ili93xx_write_data08( 0xC1 );
        board_dso_0138_ili93xx_write_data08( 0x30 );

        board_dso_0138_ili93xx_write_command( 0xed );
        board_dso_0138_ili93xx_write_data08( 0x67 );
        board_dso_0138_ili93xx_write_data08( 0x03 );
        board_dso_0138_ili93xx_write_data08( 0x12 );
        board_dso_0138_ili93xx_write_data08( 0x81 );

        board_dso_0138_ili93xx_write_command( 0xcb );
        board_dso_0138_ili93xx_write_data08( 0x39 );
        board_dso_0138_ili93xx_write_data08( 0x2c );
        board_dso_0138_ili93xx_write_data08( 0x00 );
        board_dso_0138_ili93xx_write_data08( 0x34 );
        board_dso_0138_ili93xx_write_data08( 0x02 );

        board_dso_0138_ili93xx_write_command( 0xea );
        board_dso_0138_ili93xx_write_data08( 0x00 );
        board_dso_0138_ili93xx_write_data08( 0x00 );

        board_dso_0138_ili93xx_write_command( 0xe8 );
        board_dso_0138_ili93xx_write_data08( 0x85 );
        board_dso_0138_ili93xx_write_data08( 0x0a );
        board_dso_0138_ili93xx_write_data08( 0x78 );

        board_dso_0138_ili93xx_write_command( 0xF7 );
        board_dso_0138_ili93xx_write_data08( 0x20 );

        board_dso_0138_ili93xx_write_command( 0xC0 ); //Power control
        board_dso_0138_ili93xx_write_data08( 0x26 ); //VRH[5:0]

        board_dso_0138_ili93xx_write_command( 0xC1 ); //Power control
        board_dso_0138_ili93xx_write_data08( 0x01 ); //SAP[2:0];BT[3:0]

        board_dso_0138_ili93xx_write_command( 0xC5 ); //VCM control
        board_dso_0138_ili93xx_write_data08( 0x2b );
        board_dso_0138_ili93xx_write_data08( 0x2F );

        board_dso_0138_ili93xx_write_command( 0xc7 );
        board_dso_0138_ili93xx_write_data08( 0xc7 );

        board_dso_0138_ili93xx_write_command( 0x3A );
        board_dso_0138_ili93xx_write_data08( 0x55 );

        board_dso_0138_ili93xx_write_command( 0x36 ); // Memory Access Control
        //  board_dso_0138_ili93xx_write_data08(0x08);
        board_dso_0138_ili93xx_write_data08( 0x20 );

        board_dso_0138_ili93xx_write_command( 0xB1 ); // Frame Rate Control
        board_dso_0138_ili93xx_write_data08( 0x00 );
        board_dso_0138_ili93xx_write_data08( 0x18 );

        board_dso_0138_ili93xx_write_command( 0xB6 ); // Display Function Control
        board_dso_0138_ili93xx_write_data08( 0x0a );
        board_dso_0138_ili93xx_write_data08( 0xE2 );

        board_dso_0138_ili93xx_write_command( 0xF2 ); // 3Gamma Function Disable
        board_dso_0138_ili93xx_write_data08( 0x00 );

        board_dso_0138_ili93xx_write_command( 0x26 ); //Gamma curve selected
        board_dso_0138_ili93xx_write_data08( 0x01 );

        board_dso_0138_ili93xx_write_command( 0xE0 ); //Set Gamma
        board_dso_0138_ili93xx_write_data08( 0x0f );
        board_dso_0138_ili93xx_write_data08( 0x1d );
        board_dso_0138_ili93xx_write_data08( 0x1a );
        board_dso_0138_ili93xx_write_data08( 0x09 );
        board_dso_0138_ili93xx_write_data08( 0x0f );
        board_dso_0138_ili93xx_write_data08( 0x09 );
        board_dso_0138_ili93xx_write_data08( 0x46 );
        board_dso_0138_ili93xx_write_data08( 0x88 );
        board_dso_0138_ili93xx_write_data08( 0x39 );
        board_dso_0138_ili93xx_write_data08( 0x05 );
        board_dso_0138_ili93xx_write_data08( 0x0f );
        board_dso_0138_ili93xx_write_data08( 0x03 );
        board_dso_0138_ili93xx_write_data08( 0x07 );
        board_dso_0138_ili93xx_write_data08( 0x05 );
        board_dso_0138_ili93xx_write_data08( 0x00 );

        board_dso_0138_ili93xx_write_command( 0XE1 ); //Set Gamma
        board_dso_0138_ili93xx_write_data08( 0x00 );
        board_dso_0138_ili93xx_write_data08( 0x22 );
        board_dso_0138_ili93xx_write_data08( 0x25 );
        board_dso_0138_ili93xx_write_data08( 0x06 );
        board_dso_0138_ili93xx_write_data08( 0x10 );
        board_dso_0138_ili93xx_write_data08( 0x06 );
        board_dso_0138_ili93xx_write_data08( 0x39 );
        board_dso_0138_ili93xx_write_data08( 0x22 );
        board_dso_0138_ili93xx_write_data08( 0x4a );
        board_dso_0138_ili93xx_write_data08( 0x0a );
        board_dso_0138_ili93xx_write_data08( 0x10 );
        board_dso_0138_ili93xx_write_data08( 0x0c );
        board_dso_0138_ili93xx_write_data08( 0x38 );
        board_dso_0138_ili93xx_write_data08( 0x3a );
        board_dso_0138_ili93xx_write_data08( 0x0F );

        board_dso_0138_ili93xx_write_command( 0x11 ); //Exit Sleep
        board_dso_0138_ili93xx_deselect();
    }

}
void                   board_dso_0138_ili93xx_write_command( t_u8 Command ) {
    Assert_BOARD( ttc_gpio_get( TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT ) == 0, ttc_assert_origin_auto ); // chip select line not active. Call gfx_ili93xx_board_select() before!
    ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_REGISTER_SELECT ); // select command mode
    _board_dso_0138_write8( 0 );
    _board_dso_0138_write8( Command );
    ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_REGISTER_SELECT ); // (back to data mode)
}
void                   board_dso_0138_ili93xx_write_data08( t_u8 Data ) {

    // assuming that data mode is enabled as default (faster)
    //X _board_dso_0138_write8( 0 );
    _board_dso_0138_write8( Data );
}
void                   board_dso_0138_ili93xx_write_data16( t_u16 Data ) {

    _board_dso_0138_write8( ( t_u8 )( ( Data & 0xff00 ) >> 8 ) );
    _board_dso_0138_write8( ( t_u8 )( Data & 0xff ) );
}
t_u8                   board_dso_0138_ili93xx_read_register08( e_gfx_ili93xx_register Register ) {
    Assert_BOARD( ttc_gpio_get( TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT ) == 0, ttc_assert_origin_auto ); // chip select line not active. Call gfx_ili93xx_board_select() before!
    board_dso_0138_ili93xx_write_command( Register );

    // switch data bus to input mode
    ttc_gpio_parallel08_init( 1, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_max );

    _board_dso_0138_read8( ); // ignoring first byte
    t_u8 Data = _board_dso_0138_read8( );

    // switch data bus back to output mode
    ttc_gpio_parallel08_init( 1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );

    return Data;
}
t_u16                  board_dso_0138_ili93xx_read_register16( e_gfx_ili93xx_register Register ) {
    Assert_BOARD( ttc_gpio_get( TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT ) == 0, ttc_assert_origin_auto ); // chip select line not active. Call gfx_ili93xx_board_select() before!

    board_dso_0138_ili93xx_write_command( Register );

    // switch data bus to input mode
    ttc_gpio_parallel08_init( 1, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_max );

    t_u16 Data = _board_dso_0138_read8( ) << 8;
    Data |= _board_dso_0138_read8( );

    // switch data bus back to output mode
    ttc_gpio_parallel08_init( 1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );

    return Data;
}
t_u32                  board_dso_0138_ili93xx_read_register32( e_gfx_ili93xx_register Register ) {
    Assert_BOARD( ttc_gpio_get( TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT ) == 0, ttc_assert_origin_auto ); // chip select line not active. Call gfx_ili93xx_board_select() before!
    t_u32 Data;
    if ( 0 ) { // use board_*() functions (currently does not work)
        board_dso_0138_ili93xx_write_command( Register );

        // switch data bus to input mode
        ttc_gpio_parallel08_init( 1, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_max );

        Data  = _board_dso_0138_read8( ) << 24;
        Data |= _board_dso_0138_read8( ) << 16;
        Data |= _board_dso_0138_read8( ) << 8;
        Data |= _board_dso_0138_read8( ) << 0;

        // switch data bus back to output mode
        ttc_gpio_parallel08_init( 1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    }
    else {   // do everything manually (testing)
        // writeCommand(r);
        ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_REGISTER_SELECT ); // select command mode
        ttc_gpio_parallel08_1_put( 0 );
        ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE );
        ttc_sysclock_udelay( 50 );
        ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE );
        ttc_sysclock_udelay( 50 );

        ttc_gpio_parallel08_1_put( Register );
        ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE );
        ttc_sysclock_udelay( 50 );
        ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE );
        ttc_sysclock_udelay( 50 );
        ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_REGISTER_SELECT ); // (back to data mode)

        // switch data bus to input mode
        ttc_gpio_parallel08_init( 1, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_max );

        ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE );
        ttc_sysclock_udelay( 50 );
        Data  = ttc_gpio_parallel08_1_get() << 24;
        ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE );
        ttc_sysclock_udelay( 50 );
        ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE );
        ttc_sysclock_udelay( 50 );
        Data |= ttc_gpio_parallel08_1_get() << 16;
        ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE );
        ttc_sysclock_udelay( 50 );

        ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE );
        ttc_sysclock_udelay( 50 );
        Data |= ttc_gpio_parallel08_1_get() <<  8;
        ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE );
        ttc_sysclock_udelay( 50 );

        ttc_gpio_clr( TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE );
        ttc_sysclock_udelay( 50 );
        Data |= ttc_gpio_parallel08_1_get() <<  0;
        ttc_gpio_set( TTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE );
        ttc_sysclock_udelay( 50 );

        // switch data bus back to output mode
        ttc_gpio_parallel08_init( 1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    }

    return Data;
}
void                   board_dso_0138_ili93xx_write_register08( e_gfx_ili93xx_register Register, t_u8 Value ) {
    Assert_BOARD( ttc_gpio_get( TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT ) == 0, ttc_assert_origin_auto ); // chip select line not active. Call gfx_ili93xx_board_select() before!
    board_dso_0138_ili93xx_write_command( Register );
    board_dso_0138_ili93xx_write_data08( Value );
}
void                   board_dso_0138_ili93xx_write_register16( e_gfx_ili93xx_register Register, t_u16 Value ) {
    Assert_BOARD( ttc_gpio_get( TTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT ) == 0, ttc_assert_origin_auto ); // chip select line not active. Call gfx_ili93xx_board_select() before!
    board_dso_0138_ili93xx_write_command( Register );
    board_dso_0138_ili93xx_write_data16( Value );
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

void                        _board_dso_0138_copy_pin_data( t_ttc_gpio_config* Source, t_board_dso_0138_lcd_pin_gdb* Target ) {
#if (BOARD_DSO_0138_PIN_EXTRA_DATA==1)
    Target->Function = Source->AlternateFunction;
    Target->Pin      = Source->PhysicalIndex;
    Target->Speed    = Source->Speed;
    Target->Mode     = Source->Mode;
    Target->Value    = ttc_gpio_get( Source->PhysicalIndex );
#else
    *Target = ttc_gpio_get( Source->PhysicalIndex );
#endif

}
t_board_dso_0138_PinStates* _board_dso_0138_pins_gdb() {
    static t_board_dso_0138_PinStates* PinsData = NULL;
    if ( !PinsData ) { PinsData = ttc_heap_alloc_zeroed( sizeof( t_board_dso_0138_PinStates ) ); }

    t_board_dso_0138_lcd_pin_gdb* volatile PinData = ( t_board_dso_0138_lcd_pin_gdb* ) PinsData;

    const e_ttc_gpio_pin* volatile LCD_Pin = &( board_dso_0138_Pins_Output[0] );
    while ( *LCD_Pin ) { // read current configuration of all output pins
        t_ttc_gpio_config volatile PinConfig;
        ttc_gpio_get_configuration( *LCD_Pin, ( t_ttc_gpio_config* ) &PinConfig );
        _board_dso_0138_copy_pin_data( ( t_ttc_gpio_config* ) &PinConfig, PinData );
        PinData++;
        LCD_Pin++;
    }

    LCD_Pin = &( board_dso_0138_Pins_Input[0] );
    while ( *LCD_Pin ) { // read current configuration of all input pins
        t_ttc_gpio_config volatile PinConfig;
        ttc_gpio_get_configuration( *LCD_Pin, ( t_ttc_gpio_config* ) &PinConfig );
        _board_dso_0138_copy_pin_data( ( t_ttc_gpio_config* ) &PinConfig, PinData );
        PinData++;
        LCD_Pin++;
    }

    return PinsData;
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
