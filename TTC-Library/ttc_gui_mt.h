#ifndef TTC_GUI_MT
#define TTC_GUI_MT
/*{ DEPRECATED_ttc_input.h ************************************************
}*/


//{ Includes *************************************************************
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "DEPRECATED_ttc_input_types.h"
#include "DEPRECATED_ttc_input.h"
#include "ttc_gfx_mt.h"
#include "ttc_gfx.h"
//} Includes
//{ Defines/ TypeDefs ****************************************************
#define ADD_DELAY 100
#define SUB_DELAY 100
#define TTC_GUI_MAX_KEYBOARD_RETURN 6
//} Defines
//{ Structures/ Enums ****************************************************

typedef struct gni_mt_NumberArea_s{
    t_u16* number;
    t_u16 OldNumber;
    t_u16 X;
    t_u16 Y;
    t_u8 DecPoint;
    t_base Size;
    char Buffer[7];
    struct gni_mt_NumberArea_s* Next;
} __attribute__((__packed__)) t_gni_mt_numberarea;

typedef struct gui_mt_Keyboard_s{
    char Return[TTC_GUI_MAX_KEYBOARD_RETURN];
    char Password[TTC_GUI_MAX_KEYBOARD_RETURN];
    t_u8 Counter;
    BOOL Lock;
    void (*Source)(void* Argument);
    void (*Destination) (void* Argument);
} __attribute__((__packed__)) t_gui_mt_keyboard;

//} Structures/ Enums

//{ Global Variables *****************************************************


//} Global Variables

//{ Function prototypes **************************************************

void ttc_gui_mt_create_push_button(void (*TouchHandler)(void* Argument), void* Argument, t_u16 Left, t_u16 Top, t_u16 Right, t_u16 Bottom, const char* Text);

void ttc_gui_mt_clear();

void ttc_gui_mt_plus_button(t_u16 Left, t_u16 Top, t_u16 Right, t_u16 Bottom, t_gni_mt_numberarea *value);

void ttc_gui_mt_minus_button(t_u16 Left, t_u16 Top, t_u16 Right, t_u16 Bottom, t_gni_mt_numberarea *value);

void ttc_gui_mt_create_number_field(t_u16 X, t_u16 Y, t_u16* number, t_base Size);

t_gni_mt_numberarea *ttc_gui_mt_create_number_field_floating(t_u16 X, t_u16 Y, t_u16* number, t_u8 DecDigit, t_base Size);

void ttc_gui_mt_number_refresh();

void _ttc_gui_mt_add_one(void* data);

void _ttc_gui_mt_sub_one(void* data);

void ttc_gui_mt_delete_all_number_areas();

void ttc_gui_mt_keyboard_1_9(void (*Source)(void* Argument), void (*Destination)(void* Argument),const char Password[TTC_GUI_MAX_KEYBOARD_RETURN]);

void _ttc_gui_mt_keyboard_add(void* number);

void ttc_gui_mt_create_input_line(t_u8 X, t_u8 Y,t_u8 XNumber, const char* Text, t_u16* Variable, t_u8 DecPoint, t_base Size);

void ttc_gui_mt_create_output_line(t_u8 X, t_u8 Y, t_u8 Xnum, const char* Text, t_u16* Variable, t_u8 DecPoint, t_base Size);
#endif
