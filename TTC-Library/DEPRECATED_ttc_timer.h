/** { ttc_timer.h *******************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  High-Level interface and documentation for TIMER devices.
 *  The functions defined in this file provide a hardware independent interface 
 *  for your application.
 *                                          
 *  The basic usage scenario for devices:
 *  1) check:       Assert_TIMER(tc_timer_get_max_index() > 0, ec_DeviceNotFound);
 *  2) configure:   ttc_timer_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_timer_init(LogicalIndex);
 *  4) use:         ttc_timer_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level timer and application.
 *
 *  Created from template ttc_device.h revision 20 at 20140204 09:25:54 UTC
 *
 *  Authors: Francisco Estevez 2014
 *  
}*/

#ifndef TTC_TIMER_H
#define TTC_TIMER_H

//{ Includes *************************************************************

#include "ttc_basic.h"
#include "interfaces/ttc_timer_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level timer only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * timer devices on all supported architectures.
 * Check timer/timer_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your 
 * activate_project.sh file inside your project folder.
 *
 */
 
/** Prepares timer Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_timer_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
u8_t ttc_timer_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 * @return                configuration of indexed device
 */
ttc_timer_config_t* ttc_timer_get_configuration(u8_t LogicalIndex);

/** initialize indexed device for use
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 */
void ttc_timer_init(u8_t LogicalIndex);

/** shutdown indexed device
 *
 * @param LogicalIndex    device index of device to shutdown (1..ttc_TIMER_get_max_LogicalIndex())
 */
void ttc_timer_deinit(u8_t LogicalIndex);

/** maps from logical to physical device index
 *
 * High-level timers (ttc_timer_*() functions) use logical device indices.
 * Logical index n is mapped via "COMPILE_OPTS += -DTTC_TIMERn=p" lines in the makefile to physical index p.
 *
 * Boards usually connect only some of the available devices to connectors. Therefore, some devices may not be 
 * usable. The mapping from logical to physical indices allows to create a consecutive range of indices in any 
 * order.
 *
 * @param LogicalIndex  logical index of timer device (1..ttc_timer_get_max_index() )
 * @return              physical index of timer device (0 = first physical timer device, ...)
 */
u8_t ttc_timer_logical_2_physical_index(u8_t LogicalIndex);

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 */
void ttc_timer_reset(u8_t LogicalIndex);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

/** set a timer with a given value
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 * @param TaskFunction    CallBack Function that will be executed. TaskFunction == NULL, do not use interruptions
 * @param Argument        passed as argument to Function()
 * @param Period          timer value (us)
 */
ttc_timer_errorcode_e ttc_timer_set(u8_t LogicalIndex, void TaskFunction(void*), void* Argument, u32_t Period);

/** set a new reload value for given timer
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 * @param Period          reload value (us)
 */
void ttc_timer_change_period(u8_t LogicalIndex, u32_t Period);

/** read current timer value
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 * @return                current elapsed time in given timer (us)
 */
u32_t ttc_timer_read_value(u8_t LogicalIndex);

//} Function prototypes
//{ private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_timer(u8_t LogicalIndex)

//InsertPrivatePrototypes above (DO NOT DELETE THIS LINE!)

//}private functions
/** Prototypes of low-level driver functions ****************************{
 *
 * Each function is given as a prototype and checked for a matching _driver_*() definition.
 * The more functions are implemented by low-level driver, the more features can be used on current architecture.
 *
 * Note: functions declared below are passed to interfaces/ttc_timer_interface.h
 *
 * Note: If you add a _driver_* prototype here, use create_DeviceDriver.pl to automatically add 
 *       empty functions in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 *       cd templates/; ./create_DeviceDriver.pl timer UPDATE
 */

/** Prepares timer driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void _driver_timer_prepare();

/** fills out given Config with maximum valid values for indexed TIMER
 * @param Config        = pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
ttc_timer_errorcode_e _driver_timer_get_features(ttc_timer_config_t* Config);

/** shutdown single TIMER unit device
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been shutdown successfully; != 0: error-code
 */
ttc_timer_errorcode_e _driver_timer_deinit(ttc_timer_config_t* Config);

/** initializes single TIMER unit for operation
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return              == 0: TIMER has been initialized successfully; != 0: error-code
 */
ttc_timer_errorcode_e _driver_timer_init(ttc_timer_config_t* Config);

/** loads configuration of indexed TIMER unit with default values
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
ttc_timer_errorcode_e _driver_timer_load_defaults(ttc_timer_config_t* Config);

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_TIMER_get_max_LogicalIndex())
 */
void _driver_timer_reset(ttc_timer_config_t* Config);
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

/** set a timer with a given value
 *
 * @param Config          pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @param TaskFunction    CallBack Function that will be executed
 * @param Argument        passed as argument to Function()
 * @param Period          reload value (us)
 */
ttc_timer_errorcode_e _driver_timer_set(ttc_timer_config_t* Config);

/** set a new reload value for given timer
 *
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @param Period        reload value (us)
 */
void _driver_timer_change_period(ttc_timer_config_t* Config, u32_t Period);

/** read current timer value
 *
 * @param Config        pointer to struct ttc_timer_config_t (must have valid value for LogicalIndex)
 * @return   current elapsed time in given timer (us)
 */
u32_t _driver_timer_read_value(ttc_timer_config_t* Config);

//}

#endif //TTC_TIMER_H
