#ifndef ttc_task_H
#define ttc_task_H

/** ttc_task.h ***********************************************{
 *
 * Written by Gregor Rebel 2010-2018
 *
 */
/** Description of ttc_task (Do not delete this line!)
 *
 * ttc_task provides architecture independent support for multitasking and singletasking.
 *
 * Multitasking means to run more than one function as parallel tasks.
 * A task consists of two things:
 * - a control block structure holding stack, and state variables
 * - a task function that implements an endless loop that will never return
 *
 * If only one processor core is available, then each task gets a fixed time slot to run.
 * In its time slot, the task is running its task function. At the end of the time slot, a
 * timer will interrupt the function and switch program flow to a task scheduler. This task
 * scheduler saves the current processor state and switches to another task.
 * So every task function gets the CPU from time to time.
 * Each task may end its time slot immediately or extend it if required.
 *
 * ttc_task provides basic multitasking services
 * 1) ttc_task_create()
 *    Takes a function pointer and creates a task block structure for it.
 *    When the scheduler is started, this function will be run with its own stack.
 *    Task functions have to implement an infinite while (1) loop so that they never return.
 *
 * 2) ttc_task_yield(), ttc_task_msleep(), ttc_task_usleep()
 *    Explicitly give CPU to other tasks. It is important to free the CPU for other task
 *    while busy waiting for external events.
 *
 * 3) ttc_task_is_scheduler_started(), ttc_task_is_switching_available()
 *    Check if scheduler has already been started and if it is currently active.
 *
 * 4) ttc_task_critical_begin(), ttc_task_critical_end()
 *    Disable multitasking temporarily. _begin_ and _end_ can be nested any times.
 *
 * 5) ttc_task_check_stack()
 *    Check for stack-overflows.
 *
 *
 * Using ttc_task without multitasking
 *     Most public functions of ttc_task can be safely called even when no multitasking scheduler
 *     is available. This allows to use the same application code for single- and multitasking.
 *
 *     Main difference in singletasking mode:
 *     1) ttc_task_create() will act as a function call to given task function.
 *     2) ttc_task_msleep(), ttc_task_usleep() act as software tuned busy loops. Their delay time may get inaccurate.
 *     3) ttc_task_critical_begin() will disable interrupts to avoid interruptions
 *     4) ttc_task_yield() will do nothing
 *     5) ttc_task_check_stack() will check main stack instead of task stack
 *
 *  Code that relies on multitasking can be disabled dynamically.
 *  This allows to adapt if scheduler has been disabled via ttc_task_critical_begin() temporarily.
 *    if (ttc_task_is_switching_available() )
 *    { ... } // execute multitasking code
 *    else
 *    { ... } // execute singletasking code
 *
 *  Code can also be disabled by use of pragmas. This allows to remove code blocks from compilation.
 *  #if TTC_TASK_SCHEDULER_AVAILABLE!=0
 *    ... // compile in multitasking code
 *  #else
 *    ... // compile in singletasking code
 *  #endif
}*/

//{ includes

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "ttc_interrupt.h"
#include "ttc_task_types.h"
#include "ttc_sysclock.h"

#ifdef EXTENSION_300_scheduler_freertos
    #include "scheduler/freertos_task.h"
#endif


//}includes

//{ Function prototypes **************************************************

void* ttc_task_not_implemented();

/** produces software PWM usable to calibrate uSleep()
 * Idea: Connect oscilloscope to pin PA1 (pin 15 on STM32F103) an check for correct period (1,1ms) and duty cycle.
 *        Adjust uSleep() accordingly, recompile, reflash and retest.
 * Note: Activate ttc_500_gpio to use gpio output!
 */
void ttc_task_calibrate_usleep();

/** Set task to sleep for some amount of time
 *
 * This function works with and without scheduler.
 *
 * @param Delay    delay time (msecs)
*/
void ttc_task_msleep( t_base Delay );
void _ttc_task_msleep( t_base Delay );
#ifdef _driver_task_msleep
    #define ttc_task_msleep(Delay)  _driver_task_msleep(Delay)
#else
    #define ttc_task_msleep(Delay)  _ttc_task_msleep(Delay)
#endif

/** Set task to sleep for some amount of time.
 *
 * This function works with and without scheduler.
 * If scheduler is not started or not avtivated, Delay is passed to ttc_sysclock_udelay().
 *
 * @param Delay    delay time (usecs)
 */
void ttc_task_usleep( t_base Delay );
#ifdef _driver_task_usleep
    #define ttc_task_usleep(Delay) _driver_task_usleep(Delay)
#else
    #define ttc_task_usleep(Delay) ttc_task_udelay(Delay)
    //#define ttc_task_usleep(Delay) ttc_sysclock_udelay(Delay)
#endif
#define uSleep(Delay) ttc_task_usleep(Delay)

/** Busy wait until given time has passed.
 *
 * Note: This function is normally not called directly. Use ttc_task_usleep() instead!
 *
 * @param Delay_us  time to wait (microseconds)
 */
void ttc_task_udelay( t_base Delay_us );

/** Busy wait until given time has passed in an interrupt service routine.
 *
 * Note: Delays inside ISRs are totally against the idea of being short and fast. Think twice before you use this function!
 *
 * @param Delay_us  time to wait (microseconds)
 */
void ttc_task_udelay_isr( t_base Delay_us );

/** returns smallest supported delay of ttc_task_usleep() for current configuration
 * @return smallest supported delay time (usecs)
 */
#ifdef _driver_task_min_delay
    #define ttc_task_min_delay _driver_task_min_delay
#else
    #define ttc_task_min_delay 1
#endif
#define MinimumDelay ttc_task_min_delay

/** Prepares task driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_task_prepare();

/** spawns a function as separate task
 * @param TaskFunction  function to start as task (must never return!)
 * @param TaskName      Used for debugging purposes only
 *                      Note: RAM from heap will be allocated and TaskName being copied if TaskName points to writable memory.
 *                            Use pointer to outside RAM or NULL to reduce RAM usage!
 * @param StackSize     Each task has its own fixed stack for local variables and function return addresses.
 *                      Its size cannot be increased later. Choose wisely!
 *                      Note: If multitasking is disabled, all tasks share the big global stack and this value is ignored.
 * @param Argument      Passed as argument to TaskFunction()
 * @param Priority      Tasks with higher priority get more cpu time
 * @param TaskInfoPtr   !=NULL: Info configuration of current task control block is copied into given variable
 * @return              == TRUE: task was created successfully
 */
BOOL ttc_task_create( void TaskFunction( void* ), const char* TaskName, t_u16 StackSize, void* Argument, t_u8 Priority, t_ttc_task_info** TaskInfoPtr );
//BOOL _driver_task_create( void TaskFunction( void* ), t_u16 StackSize, void* Argument, t_u8 Priority, t_ttc_task_info* TaskInfo );

/** Checks if ttc_task_start_scheduler() has already been started (multitasking is available)  *
  * Some functions require a running task scheduler. Use this function
  * with Assert() to avoid system crashes before the scheduler has started.
  *
  * @return  == TRUE: ttc_task_start_scheduler() has been called
  */
#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    BOOL ttc_task_is_scheduler_started();
#else
    #define ttc_task_is_scheduler_started() (0)
#endif

/** Checks if cpu can be given to another task (Scheduler is running + outside of critical section).
  *
  * @return  == TRUE: scheduler is available and we're outside of a critical section
  */
BOOL ttc_task_is_switching_available();

/** Switches scheduler to next process providing a short, undetermined sleep
 *
 * Some applications require a busy waiting for resources like:
 *   while (...);
 * During this loop, the cpu is completely busy with this loop. This blocks or at least slows down other tasks.
 * Even if this task can be interrupted by the task scheduler, it fills its time slice with checking its condition (...).
 *
 * A more multitask-friendly version is to yield cpu time to other processes while waiting:
 *    while (! checkEvent() )  // check for some event
 *    { ttc_task_yield(); }    // free CPU
 *
 * For every checked condition (...), every task gets one slice of cpu time. The processing overhead is minimal.
 *
 * Note: This function will do nothing if scheduler is not started or disabled.
 */
//extern volatile t_s16 ttc_task_CriticalSection_Counter;
#ifdef _driver_task_yield
//static void ttc_task_yield() { if ( ttc_task_CriticalSection_Counter == 0 ) { _driver_task_yield(); } }
#define ttc_task_yield() if ( ttc_interrupt_critical_level() == 0 ) { _driver_task_yield(); } (void) (1==1)
#else
static inline void ttc_task_yield() {}
#endif

/** similar to ttc_task_yield() for use in interrupt service routines only
  */
void ttc_task_yield_isr();
#ifndef _driver_task_yield_isr
    #define ttc_task_yield_isr() _driver_task_yield_isr()
#else
    #define ttc_task_yield_isr()
#endif

// check definition of low-level critical section drivers
#if TTC_TASK_SCHEDULER_AVAILABLE
    #ifndef _driver_task_critical_begin
        #define _driver_task_critical_begin() ttc_interrupt_critical_begin()
        #    warning Missing definition for _driver_task_critical_begin(). Define as a function that supports current multitasking scheduler AND calls ttc_interrupt_critical_begin()!
    #endif
    #ifndef _driver_task_critical_end
        #define _driver_task_critical_end() ttc_interrupt_critical_end()
        #    warning Missing definition for _driver_task_critical_end(). Define as a function that supports current multitasking scheduler AND calls ttc_interrupt_critical_end()!
    #endif
    #ifndef _driver_task_critical_all_end
        #define _driver_task_critical_all_end(StoreCounter)  *StoreCounter = ttc_interrupt_critical_level(); while (ttc_interrupt_critical_level()) { ttc_task_critical_end(); }
        #    warning Missing definition for _driver_task_critical_all_end(). Define as a function that supports current multitasking scheduler AND calls ttc_interrupt_critical_end()!
    #endif
#else // no multitasking scheduler available: use ttc_interrupt to handle critical sections
    #define _driver_task_critical_begin()                 ttc_interrupt_critical_begin()
    #define _driver_task_critical_end()                   ttc_interrupt_critical_end()
    #define _driver_task_critical_all_end(StoreCounter)  *StoreCounter = ttc_interrupt_critical_level(); while (ttc_interrupt_critical_level()) { ttc_interrupt_critical_end(); }
#endif


/** Temporarily disables task scheduler and interrupts
 *
 * Disabling the task scheduler is required whenever a section of code
 * must not be interrupted by a task switch.
 * Note: Interrupts may still occur! (-> ttc_interrupt_all_disable() )
 * Each call will increase a corresponding critical section counter.
 * Call ttc_task_critical_end() to reenable scheduler.
 *
 *  Example:
 *    ttc_task_critical_begin();
 *      function call
 *      ttc_task_critical_begin();
 *      ... // no task switch will occur
 *      ttc_task_critical_end();
 *      ... // still no task switches
 *    ttc_task_critical_end();
 *    ... // may be interrupted by task switch again
 */
void ttc_task_critical_begin();
void _ttc_task_critical_begin(); // private function: call ttc_task_critical_begin() instead!
#if TTC_ASSERT_TASK==1  // use slow but debuggable implementation (stores meta data of caller to support debugging)
    //? #  undef  ttc_task_critical_begin
    #define ttc_task_critical_begin() _ttc_task_critical_begin()
#else                   // use fast implementation without debug support
    #define ttc_task_critical_begin() _driver_task_critical_begin();
    /* DEPRECATED
    #  ifdef EXTENSION_300_scheduler_freertos
        #    define ttc_task_critical_begin()   if (ttc_task_is_scheduler_started() ) { ulPortSetInterruptMask(); uxCriticalNesting++; }
    #  else
    #  endif
    */
#endif

/** Reenables task scheduler
 *
 * Each call decreases a critical section counter.
 * If counter == 0 then task scheduler is reenabled
 */
void ttc_task_critical_end();
void _ttc_task_critical_end(); // private function: call ttc_task_critical_end() instead!
#if TTC_ASSERT_TASK==1
    //? #  undef  ttc_task_critical_end
    #define ttc_task_critical_end() _ttc_task_critical_end()
#else
    #define ttc_task_critical_end() _driver_task_critical_end()
#endif

/** Resets critical section counter and immediately enables task scheduler
 *
 * This function is required if you want to force a task switch and to restore the original state afterwards.
 *
 * Note: Always use this function in conjunction with ttc_task_critical_all_restore()
 *       1) ttc_task_critical_all_end()
 *       2) ...
 *       3) ttc_task_critical_all_restore()
 *
 * @param StoreCounter  stores old value of critical section counter for later restore
 */
void ttc_task_critical_all_end( t_u8* StoreCounter );
void _ttc_task_critical_all_end( t_u8* StoreCounter ); // private function: call ttc_task_critical_end() instead!
#ifdef _driver_task_critical_all_end
    #define ttc_task_critical_all_end _driver_task_critical_all_end
#else
    #define ttc_task_critical_all_end _ttc_task_critical_all_end
#endif

/** Restores previously ended value of critical sections
 *
 * @param StoreCounter  storage provided to ttc_task_critical_all_end()
 */
void ttc_task_critical_all_restore( t_u8 StoreCounter );
void _ttc_task_critical_all_restore( t_u8 StoreCounter );
#ifdef _driver_task_critical_all_restore
    #define ttc_task_critical_all_restore _driver_task_critical_all_restore
#else
    #define ttc_task_critical_all_restore _ttc_task_critical_all_restore
#endif


/** Checks if stack of current task or any other task has been overflowed in the past.
 *
 *  Every task has its own stack. The size of its stack has to be defined when calling ttc_task_create().
 *  When more local variables and nested function calls are added to a task function, it's stack usage may
 *  exceed its stack size. This condition is not detected automatically. Data will be corrupted and strange
 *  effects occur in other tasks.
 *
 *  Example:
 *
 *  void taskTest(void* Arg) {
 *
 *     t_u8 Buffer[100];
 *
 *     while(1) {
 *        ...
 *
 *        // it is a good idea to always check your stack during development
 *        Assert(! ttc_task_check_stack(), ttc_assert_origin_auto); // increase stack size or reduce local variables!
 *     }
 *  }
 *
 *
 * Find out remaining stack space
 *
 * ttc_task_check_stack() will not return the current remaining stack space because a return value would make it slower.
 * Determining the current remaining stack space is not the intention of this function.
 * You may use a gdb debug session to find out, how much space is left on your stack:
 * 1) place a break point somewhere deep in your code (preferrably a place with deepest call stack)
 * 2) run to this breakpoint
 * 3) Run stack check:
 *    p ttc_task_check_stack()
 * 4) Print current value of global variable
 *    p ttc_task_StackBytesFree
 *
 * Note: If a task switch occured between 3) and 4) and if ttc_task_check_stack() was called during this switch, then
 *       the value of ttc_task_StackBytesFree has been corrupted!
 *
 * @param != 0: handle of task which stack should be inspected; ==0: inspect current task
 * @return minimum seen stack bytes free so far
 */
void ttc_task_check_stack();
#if TTC_TASK_STACK_CHECKS_ENABLED == 1
    #ifndef _driver_task_check_stack
        #define _driver_task_check_stack() 1
    #endif
    #ifndef TTC_TASK_SCHEDULER_AVAILABLE
        //   no tasks available: check main stack (as this is the stack of the one and only task running)
        #define ttc_task_check_stack() ttc_task_check_main_stack()
    #endif
#else
    #define ttc_task_check_stack()
#endif

/** Stack-check macro usable as replacement for return statement
 *
 * Instead of explicitly calling ttc_task_check_stack() at the end of a function,
 * you may use this macro.
 *
 * The macro automatically adapts to the current debug settings:
 * 1) Stack checks are enabled
 *    TTC_TASK_RETURN(VALUE)   will check current stack usage and then return VALUE
 *    TTC_TASK_RETURN;         will check current stack usage and then return from function
 * 2) Stack checks are disabled
 *    TTC_TASK_RETURN(VALUE);  will just return VALUE without any overhead
 *    TTC_TASK_RETURN;         will just return from function
 */
#if TTC_TASK_STACK_CHECKS_ENABLED == 1
    #define TTC_TASK_RETURN  ttc_task_check_stack(); return
#else
    #define TTC_TASK_RETURN  return
#endif

/** The main stack is the stack used by main() function
 *
 * Hint: It is a good idea to call this function at the end of a function that is called by
 *       a function that is called by ... and does not call any other functions.
 *
 * Understanding stack usage of your application is vital to
 * 1) fully make use of rare RAM
 * 2) avoid memory corruption
 *
 * The main stack typically starts at one end of the RAM region. It grows to the opposite
 * end of the RAM region. Depending on your uC architecture, the stack will grow to increasing
 * or decreasing addresses. Directly after the end of main stack, starts the heap. The heap
 * provides dynamic memory via ttc_heap_alloc(). It also provides memory for the stack of all
 * tasks being created via ttc_task_create(). If the main stack gets overrun, then the data being
 * allocated on the heap is corrupted first.
 *
 * Size of the main stack is configured in your configs/memory_project.ld file.
 * While your application grows, you will have to adjust the size of your main stack!
 *
 *
 * Stack usage with and without multitasking scheduler
 *
 * During system start, all start functions are run one after another using the main stack.
 * At end of system start up, a multitasking scheduler may be started:
 * 1) Multitasking scheduler is started
 *    The main stack is used by the multitasking scheduler.
 *    All unused memory on the main stack is lost for your application.
 *    The scheduler is typically tuned to use very low amount of main stack.
 *    Check description/ source code of your favoured scheduler to get an overview of its
 *    main stack usage.
 * 2) Program flow jumps to taskMain() or to a user defined function
 *    This scenario is also known as single tasking scenario. Only the main task is running.
 *    This task is using the main stack only. No other stacks are created.
 *    You should increase the main stack so that it can store all local variables + return addresses
 *    of your whole application.
 */
void ttc_task_check_main_stack();

/** runs all created tasks (will not return)
 *
 * The behaviour of this function depends on the availability of a multitasking scheduler:
 *
 * 1) _driver_task_start_scheduler() is defined
 *    The defined function is called to run all created tasks.
 *    Each task function must implement an endless while loop to ensure that it does not return.
 *
 * 2) _driver_task_start_scheduler() is NOT defined
 *    The functions of all created tasks are called one after another in an endless loop.
 *    Each task function must implement a statemachine and return immediately to provide
 *    cpu time to other tasks (kind of cooperative multitasking).
 *
 * Note: Task functions can be implemented in a way that supports both operations.
 *
 * // function is run once at system start
 * void startFoo() {
 *   static foo_t Argument; // prepare Argument
 *
 *   ... basic initialization
 *
 *    ttc_task_create( taskFoo,   // function to start as task
 *                     "tFoo",    // task name (just for debugging)
 *                     128,       // stack size (adjust to amount of local variables of _example_<device>_task() AND ALL ITS CALLED FUNTIONS!)
 *                     &Argument, // passed as argument to _example_<device>_task()
 *                     1,         // task priority (higher values mean more process time)
 *                     NULL       // can return a handle to created task
 *                   );
 * }
 *
 * // multitasking scheduler available: function is run once for each ttc_task_creat(taskFoo, ...) call.
 * //                    not available: function is run periodically
 * void taskFoo(void * Argument) {
 *   // use Argument ...
 *
 *   // variables used for exact delays in multi- and singletasking
 *   t_base NextTime = ttc_systick_get_elapsed_usecs() + 1;
 *   t_base PreviousTime = NextTime;
 *
 *   // while loop is endless if multitasking scheduler is available
 *   while (TTC_TASK_SCHEDULER_AVAILABLE) {
 *     if (PreviousTime > NextTime) { // time stamp experienced overrun: test for counter overrun
 *       if (ttc_systick_get_elapsed_usecs() < NextTime)  // system clock overrun occured
 *         { PreviousTime = NextTime; }
 *     }
 *     else {
 *       if (NextTime < ttc_systick_get_elapsed_usecs()) { // delay passed: activate statemachine
 *         t_base NextTime = NextTime + 500000; // next action in 500ms
 *
 *         .. statemachine processes events
 *       }
 *     }
 *     ttc_task_yield(); // if scheduler available, give cpu to other processes
 *   }
 * }
 */
void ttc_task_start_scheduler();

/** checks if multitasking has been temporarily disabled and task switches are prevented
  *
  * Use this function to ensure that no task switches may occur during your next steps.
  *
  * @return  == TRUE: ttc_task_critical_begin() has been called
  */
BOOL ttc_task_is_inside_critical_section();
#ifdef _driver_task_is_inside_critical_section
    #define ttc_task_is_inside_critical_section()     _driver_task_is_inside_critical_section()
#else
    #define ttc_task_is_inside_critical_section()     0
#endif

/** resumes a suspended task
  *
  * Note: Do not call this function from an interrupt service routine!
  *
  * @param !=NULL: Handle task identifier as returned by ttc_task_create
  */
//void ttc_task_resume(TaskHandle_t Handle);
#if (TTC_ASSERT_TASK == 1)
    #define ttc_task_resume(Handle) _ttc_task_resume(Handle)
#else
    #ifdef _driver_task_resume
        #define ttc_task_resume(Handle) _driver_task_resume(Handle)
    #else
        #define ttc_task_resume(Handle) _ttc_task_resume(Handle)
    #endif
#endif
#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    void _ttc_task_resume( TaskHandle_t Handle );
#else
    #define _ttc_task_resume(Handle)
#endif

/** resumes a suspended task from an interrupt service routine
  *
  * @param   Handle task identifier as returned by ttc_task_create
  * @return  == TRUE: you must call ttc_task_yield_isr() at end of your isr!
  */
BOOL ttc_task_resume_isr( TaskHandle_t Handle );
#ifdef _driver_task_resume_isr
    //#  define ttc_task_resume_isr(Handle)     (Handle) ? _driver_task_resume_isr(Handle) : ttc_assert_halt_origin(ttc_assert_origin_auto)
    #define ttc_task_resume_isr(Handle)     _driver_task_resume_isr(Handle)
#else
    #define ttc_task_resume_isr(Handle)
#endif

/** Low latency wakeup for given suspended task
  *
  * The given task will be set to be executed first after next context switch.
  * If given task has higher priority than current one then switch will be immediately most times.
  *
  * Note: This function must be called inside critical section!
  *       Call ttc_task_critical_begin() before and ttc_task_critical_end() after.
  * Note: Do not call this function from an interrupt service routine!
  *
  * @param !=NULL: Handle task identifier as returned by ttc_task_create
  */
//void ttc_task_resume(TaskHandle_t Handle);
#if (TTC_ASSERT_TASK == 1)
    #ifdef _driver_task_resume_now
        #define ttc_task_resume_now(Handle) _ttc_task_resume_now(Handle)
    #else
        #define ttc_task_resume_now(Handle)
    #endif
#else
    #ifdef _driver_task_resume_now
        #define ttc_task_resume_now(Handle) _driver_task_resume_now(Handle)
    #else
        #ifdef _driver_task_resume
            #define ttc_task_resume_now(Handle) _driver_task_resume(Handle)
        #else
            #define ttc_task_resume_now(Handle)
        #endif
    #endif
#endif
#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    void _ttc_task_resume_now( TaskHandle_t Handle );
#else
    #define _ttc_task_resume_now(Handle)
#endif

/** Low latency wakeup for given suspended task
  *
  * The given task will be set to be executed first after next context switch.
  * If given task has higher priority than current one then switch will be immediately most times.
  *
  * Note: This function must be called inside critical section!
  *       Call ttc_task_critical_begin() before and ttc_task_critical_end() after.
  * Note: Do not call this function from an interrupt service routine!
  *
  * @param !=NULL: Handle task identifier as returned by ttc_task_create
  */
//void ttc_task_resume(TaskHandle_t Handle);
#if (TTC_ASSERT_TASK == 1)
    #ifdef _driver_task_resume_now_isr
        #define ttc_task_resume_now_isr(Handle) _ttc_task_resume_now_isr(Handle)
    #else
        #define ttc_task_resume_now_isr(Handle)
    #endif
#else
    #ifdef _driver_task_resume_now_isr
        #define ttc_task_resume_now_isr(Handle) _driver_task_resume_now_isr(Handle)
    #else
        #ifdef _driver_task_resume_isr
            #define ttc_task_resume_now_isr(Handle) _driver_task_resume_isr(Handle)
        #else
            #define ttc_task_resume_now_isr(Handle)
        #endif
    #endif
#endif
#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    void _ttc_task_resume_now_isr( TaskHandle_t Handle );
#else
    #define _ttc_task_resume_now_isr(Handle)
#endif

/** puts a task to sleep.
  *
  * @param Handle != NULL: task identifier as returned by ttc_task_create
  *               == NULL: put current task to sleep
  */
#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    void ttc_task_suspend( TaskHandle_t Handle );
#else
    #define ttc_task_suspend(Handle)
#endif

/** taskhandle of current task
  *
  * TaskHandle_t stores scheduler dependent data of a single task.
  * This low-level pointer may not be fully defined.
  * Use ttc_task_current_info() to obtain scheduler independent data of a task.
  *
  */
#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    TaskHandle_t ttc_task_current_handle();
    #ifdef _driver_task_current_handle
        #define ttc_task_current_handle() _driver_task_current_handle()
    #endif
#else
    #define ttc_task_current_handle() (NULL)
#endif

/** info data of current task
  *
  * The t_ttc_task_info stores more scheduler independent informations than TaskHandle_t.
  */
#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    t_ttc_task_info* ttc_task_current_info();
    #define ttc_task_current_info() ttc_task_get_task_info(ttc_task_current_handle())
#else
    #define ttc_task_current_info()  (NULL)
#endif

/** returns latest configuration info of current task
  *
  * Note: This function does not update the info data and is quite fast.
  *
  * @param Handle  !=NULL: get info of corresponding task
  *                ==NULL: get info of current task
  * @return pointer to task info structure
  */
#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    t_ttc_task_info* ttc_task_get_task_info( TaskHandle_t Handle );
    #ifdef _driver_task_get_info
        #define ttc_task_get_task_info(Handle)  _driver_task_get_info(Handle)
    #else
        #define ttc_task_get_task_info(Handle)  (NULL)
    #endif
#else
    #define ttc_task_get_task_info(Handle)  (NULL)
#endif

/** updates configuration info of current task
  *
  * Note: This function is specially designed for online debugging.
  *       To find out more about your current task, you may want to issue this in your gdb session:
  *       (gdb) p *(ttc_task_update_info(NULL, NULL))
  *
  * Note: This function may traverse through several lists to gather all data and is relatively slow.
  *
  * Note: shortcuts are defined in ttc_basic.h
  *
  * @param Handle  !=NULL: get info of corresponding task
  *                ==NULL: get info of current task
  * @param Info    !=NULL: Info configuration of current task control block is copied into given variable
  *                ==NULL: a local static buffer will be allocated (do not use from multiple threads!)
  * @return pointer to task info structure
  */
#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    volatile t_ttc_task_info* ttc_task_update_info( TaskHandle_t Handle );
    #ifndef _driver_task_update_info
        #define _driver_task_update_info(Handle, Info) (NULL)
    #endif
#else
    #define ttc_task_update_info(Handle)  (NULL)
#endif

/** scans list of all task infos and returns entry corresponding to given task handle
  *
  * Note: This function performs a linear search in ttc_task_TaskInfos
  *
  *
  * @param Handle  task handle used by multitasking scheduler
  * @return        !=NULL: task info data of given task handle
  */
#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    volatile t_ttc_task_info* ttc_task_find_info( TaskHandle_t Handle );
#else
    #define ttc_task_find_info( TaskInfos ) (NULL)
#endif

/** returns configuration info of all tasks
  *
  * Note: This function is specially designed for online debugging.
  *        To find out more about your current task, you may want to issue this in your gdb session:
  *        (gdb) p *(ttc_task_get_all(NULL))
  *
  * Note: shortcuts are defined in ttc_basic.h
  *
  * @param !=NULL: Info configuration of current task control block is copied into given variable
  *        ==NULL: a local static buffer will be allocated (do not use from multiple threads!)
  * @return pointer to array of task info structures
  */
#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    t_ttc_task_all* ttc_task_get_all( t_ttc_task_all* TaskInfos );
    #ifndef _driver_task_get_all
        #define _driver_task_get_all(TaskInfos)
    #endif
#else
    #define ttc_task_get_all( TaskInfos ) (NULL)
#endif

/** returns total amount of currently registered tasks
  *
  * @return amount of tasks currently registered
  */
t_base ttc_task_get_amount();
#ifdef _driver_task_get_amount
    #define ttc_task_get_amount() _driver_task_get_amount()
#else
    #define ttc_task_get_amount() 1
#endif

/** Concept of waiting lists
  *
  * Whenever a task must wait for a resource to become available, a waiting list can be used to
  * minimize CPU usage while waiting:
  * 1) create an instance of t_ttc_task_waiting_list WaitingList for each resource
  * 2a) if a task wants to use a locked resource, call ttc_task_waitinglist_add(&WaitingList, TimeOut)
  * 2b) if a task releases the resource, call ttc_task_waitinglist_awake(&WaitingList)
  *
  */

/** adds current task to given list of waiting tasks + goes to sleep
  *
  * Note: This function must be called inside critical section!
  *       Call ttc_task_critical_begin() before and ttc_task_critical_end() after.
  *
  * @param WaitingList           list of tasks waiting for a resource
  * @param TimeOut               ==0: return immediately, == -1: wait endles, >0: amount of microseconds to wait maximum
  */

#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    void ttc_task_waitinglist_wait( t_ttc_task_waiting_list* WaitingList, t_base TimeOut );
#else
    #define ttc_task_waitinglist_wait( WaitingList, TimeOut )
#endif

/** wakes up first task waiting in given list
  *
  * Note: This function must be called inside critical section!
  *       Call ttc_task_critical_begin() before and ttc_task_critical_end() after.
  * Note: Caller must check that WaitingList->First is not NULL before calling this function!
  *
  * Example usage:
  *
  *
  * @param WaitingList  list of tasks waiting for a resource
  */
#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    #if (TTC_ASSERT_TASK == 1)
        #define ttc_task_waitinglist_awake(WaitingList)  _ttc_task_waitinglist_awake(WaitingList)
    #else
        #define ttc_task_waitinglist_awake(WaitingList)  ttc_task_resume_now((WaitingList)->First->Task->Handle)
    #endif
    void _ttc_task_waitinglist_awake( t_ttc_task_waiting_list* WaitingList );
#else
    #define ttc_task_waitinglist_awake(WaitingList)
#endif

/** wakes up first task waiting in given list
  *
  * Note: This function may only be called from an interrupt service routine (ISR).
  * Note: Caller must check that WaitingList->First is not NULL before calling this function!
  *
  * @param WaitingList  list of tasks waiting for a resource
  */
#if TTC_TASK_SCHEDULER_AVAILABLE!=0
    #if (TTC_ASSERT_TASK == 1)
        #define ttc_task_waitinglist_awake_isr(WaitingList)  _ttc_task_waitinglist_awake_isr(WaitingList)
    #else
        #define ttc_task_waitinglist_awake_isr(WaitingList)  ttc_task_resume_now_isr((WaitingList)->First->Task->Handle)
    #endif
    void _ttc_task_waitinglist_awake_isr( t_ttc_task_waiting_list* WaitingList );
#else
    #define ttc_task_waitinglist_awake_isr(WaitingList)
#endif


//InsertFunctionDeclarations  above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif
