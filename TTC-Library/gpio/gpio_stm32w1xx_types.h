#ifndef GPIO_STM32W1XX_TYPES_H
#define GPIO_STM32W1XX_TYPES_H

/** { gpio_stm32w1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for GPIO devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by ttc_gpio_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140208 06:39:49 UTC
 *
 *  Note: See ttc_gpio.h for description of architecture independent GPIO implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "../register/register_stm32w1xx_types.h"

/* defined in register_stm32w1xx_types.h
#ifndef PERIPH_BASE
#define PERIPH_BASE                 ((t_u32) 0x40000000)
#define GPIOA_BASE                  (PERIPH_BASE + 0xB000)
#define GPIOB_BASE                  (PERIPH_BASE + 0xB400)
#define GPIOC_BASE                  (PERIPH_BASE + 0xB800)
#endif
*/

#include "../register/register_stm32w1xx_types.h"

//{ Defines/ TypeDefs ******************************************************

#define t_ttc_gpio_bank          e_gpio_stm32w1xx_bank // datatype used to identify GPIO banks
#define TTC_GPIO_MAX_AMOUNT      3                     // amount of gpio banks (A..G)
#define TTC_GPIO_MAX_PINS        16                    // amount of port pins per bank
#define t_ttc_gpio_register  t_register_stm32w1xx_gpiox*

/** bitband address calculation
 *
 * Each GPIO+Pin pair is identified by the corresponding bit band address of bit Pin in GPIO base register.
 * The offsets below have to be added to t_ttc_gpio to get the bitband address for individual register operations.
 *
 * Register offsets are calculated according to t_register_stm32w1xx_gpiox
 */
#define TTC_GPIO_OFFSET_CFGL (0 * 4 * 32)      // adddress offset of bitband address for register CFGL
#define TTC_GPIO_OFFSET_CFGH (1 * 4 * 32)      // adddress offset of bitband address for register CFGH
#define TTC_GPIO_OFFSET_IDR  (2 * 4 * 32)      // adddress offset of bitband address for register IDR
#define TTC_GPIO_OFFSET_ODR  (3 * 4 * 32)      // adddress offset of bitband address for register ODR
#define TTC_GPIO_OFFSET_SETR (4 * 4 * 32)      // adddress offset of bitband address for register SETR
#define TTC_GPIO_OFFSET_CLR  (5 * 4 * 32)      // adddress offset of bitband address for register CLR

/** Parallel Port Definitions
 *
 * Parallel Ports make use of GPIO banks.
 * All pins of each GPIO bank can be set/ read in parallel.
 *
 */
#define TTC_GPIO_BANK_A GPIOA_BASE
#define TTC_GPIO_BANK_B GPIOB_BASE
#define TTC_GPIO_BANK_C GPIOC_BASE
#ifndef TTC_GPIO1   // device not defined in makefile
    #define TTC_GPIO1    E_ttc_gpio_architecture_stm32w1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

typedef enum {
    /** Address of each port bit is calculated as bitband address of corresponding bit in GPIO Base register.
     *  This makes it very easy to calculate bitband address of BSRR, BRR, IDR and ODR addresses for fast
     *  port bit operations.
     *  Using an enum for register addresses makes debugging with GDB a lot easier.
     *  See implementation of gpio_stm32w1xx_set() for details.
     */

    E_ttc_gpio_pin_none = 0, // item mandatory for each architecture

#ifdef TTC_GPIO_BANK_A
    E_ttc_gpio_pin_a0     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_a1     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_a2     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_a3     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_a4     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_a5     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_a6     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_a7     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_a8     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_a9     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_a10    = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_a11    = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_a12    = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_a13    = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_a14    = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_a15    = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 15 * 4,
#endif
#ifdef TTC_GPIO_BANK_B
    E_ttc_gpio_pin_b0     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_b1     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_b2     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_b3     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_b4     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_b5     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_b6     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_b7     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_b8     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_b9     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_b10    = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_b11    = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_b12    = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_b13    = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_b14    = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_b15    = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 15 * 4,
#endif
#ifdef TTC_GPIO_BANK_C
    E_ttc_gpio_pin_c0     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_c1     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_c2     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_c3     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_c4     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_c5     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_c6     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_c7     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_c8     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_c9     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_c10    = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_c11    = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_c12    = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_c13    = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_c14    = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_c15    = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 15 * 4,
#endif
    E_ttc_gpio_pin_unknown
} e_gpio_stm32w1xx_pin;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

#define e_ttc_gpio_pin          e_gpio_stm32w1xx_pin
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic_types.h"
//X #include "stm32w108xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_gpio_types.h ***********************

typedef enum {
    E_ttc_gpio_bank_none,
    E_ttc_gpio_bank_a = ( t_u32 ) GPIOA_BASE,
    E_ttc_gpio_bank_b = ( t_u32 ) GPIOB_BASE,
    E_ttc_gpio_bank_c = ( t_u32 ) GPIOC_BASE,
    E_ttc_gpio_bank_unknown
} e_gpio_stm32w1xx_bank;

typedef enum {
    E_ttc_gpio_alternate_function_None

} e_gpio_stm32w1xx_alternate_function;

#define e_ttc_gpio_alternate_function e_gpio_stm32w1xx_alternate_function

typedef struct {  // stm32l1xx specific configuration data of single GPIO device
    e_gpio_stm32w1xx_bank Bank;
    //ToDo:    gpio_stm32w1xx_pull_e Pull; // push/pull resistor
    //ToDo:    gpio_stm32w1xx_mode_e Mode; // port mode
} __attribute__( ( __packed__ ) ) t_gpio_stm32w1xx_values;

// t_ttc_gpio_values is required by ttc_gpio_types.h
#define t_ttc_gpio_values t_gpio_stm32w1xx_values

//} Structures/ Enums


#endif //GPIO_STM32W1XX_TYPES_H
