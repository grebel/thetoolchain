#ifndef GPIO_STM32L0XX_TYPES_H
#define GPIO_STM32L0XX_TYPES_H

/** { gpio_stm32l0xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for GPIO devices on stm32l0xx architectures.
 *  Structures, Enums and Defines being required by ttc_gpio_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150305 09:34:47 UTC
 *
 *  Note: See ttc_gpio.h for description of architecture independent GPIO implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_GPIO1   // device not defined in makefile
    #define TTC_GPIO1    E_ttc_gpio_architecture_stm32l0xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "../../additionals/270_CPAL_STM32L0xx_StdPeriph_Driver/inc/stm32l0xx_hal.h"
#include "../register/register_stm32l0xx_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_gpio_types.h *************************

typedef struct { // register description (adapt according to stm32l0xx registers)
    unsigned Reserved1 : 16;
    unsigned Reserved2 : 16;
} t_gpio_register;

typedef struct {  // stm32l0xx specific configuration data of single GPIO device
    t_gpio_register* BaseRegister;       // base address of GPIO device registers
} __attribute__( ( __packed__ ) ) t_gpio_stm32l0xx_config;

#define t_ttc_gpio_bank        e_gpio_stm32l0xx_bank // datatype used to identify GPIO banks
#define TTC_GPIO_MAX_AMOUNT  5            // amount of gpio banks (A..E & H)
#define TTC_GPIO_MAX_PINS    16           // amount of port pins per bank
#define t_ttc_gpio_register  t_register_stm32l0xx_gpio*

/** bitband address calculation
 *
 * Each GPIO+Pin pair is identified by the corresponding bit band address of bit Pin in GPIO base register.
 * The offsets below have to be added to t_ttc_gpio to get the bitband address for individual register operations.
 */
//X #define t_ttc_gpio           e_ttc_gpio_pin  // bitband address of single Portbit in GPIO register base
#define TTC_GPIO_OFFSET_MODER   (0 * 4 * 32)      // adddress offset of bitband address for register MODER
#define TTC_GPIO_OFFSET_OTYPER  (1 * 4 * 32)      // adddress offset of bitband address for register OTYPER
#define TTC_GPIO_OFFSET_OSPEED  (2 * 4 * 32)      // adddress offset of bitband address for register OSPEED
#define TTC_GPIO_OFFSET_PUPDR   (3 * 4 * 32)      // adddress offset of bitband address for register PUPDR
#define TTC_GPIO_OFFSET_IDR     (4 * 4 * 32)      // adddress offset of bitband address for register IDR
#define TTC_GPIO_OFFSET_ODR     (5 * 4 * 32)      // adddress offset of bitband address for register ODR
#define TTC_GPIO_OFFSET_BSRR    (6 * 4 * 32)      // adddress offset of bitband address for register BSRR
#define TTC_GPIO_OFFSET_LCKR    (7 * 4 * 32)      // adddress offset of bitband address for register LCKR
#define TTC_GPIO_OFFSET_AFRL    (8 * 4 * 32)      // adddress offset of bitband address for register AFRL
#define TTC_GPIO_OFFSET_AFRH    (9 * 4 * 32)      // adddress offset of bitband address for register AFRH
#define TTC_GPIO_OFFSET_BRR     (10* 4 * 32)      // adddress offset of bitband address for register BRR

/** Parallel Port Definitions
 *
 * Parallel Ports make use of GPIO banks.
 * All pins of each GPIO bank can be set/ read in parallel.
 *
 */
#define TTC_GPIO_BANK_A E_ttc_gpio_bank_a
#define TTC_GPIO_BANK_B E_ttc_gpio_bank_b
#define TTC_GPIO_BANK_C E_ttc_gpio_bank_c
#define TTC_GPIO_BANK_D E_ttc_gpio_bank_d
#ifdef GPIOH
    #define TTC_GPIO_BANK_H tgb_bank_h
#endif


typedef enum {
    /** Address of each port bit is calculated as bitband address of corresponding bit in GPIO Base register.
     *  This makes it very easy to calculate bitband address of BSRR, BRR, IDR and ODR addresses for fast
     *  port bit operations.
     *  Using an enum for register addresses makes debugging with GDB a lot easier.
     *  See implementation of gpio_stm32f1xx_set() for details.
     */
    E_ttc_gpio_pin_none = 0, // item mandatory for each architecture

#if TTC_GPIO_MAX_AMOUNT > 0
    E_ttc_gpio_pin_a0     = ( ( GPIOA_BASE - 0x50000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_a1     = ( ( GPIOA_BASE - 0x50000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_a2     = ( ( GPIOA_BASE - 0x50000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_a3     = ( ( GPIOA_BASE - 0x50000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_a4     = ( ( GPIOA_BASE - 0x50000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_a5     = ( ( GPIOA_BASE - 0x50000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_a6     = ( ( GPIOA_BASE - 0x50000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_a7     = ( ( GPIOA_BASE - 0x50000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_a8     = ( ( GPIOA_BASE - 0x50000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_a9     = ( ( GPIOA_BASE - 0x50000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_a10    = ( ( GPIOA_BASE - 0x50000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_a11    = ( ( GPIOA_BASE - 0x50000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_a12    = ( ( GPIOA_BASE - 0x50000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_a13    = ( ( GPIOA_BASE - 0x50000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_a14    = ( ( GPIOA_BASE - 0x50000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_a15    = ( ( GPIOA_BASE - 0x50000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 1
    E_ttc_gpio_pin_b0     = ( ( GPIOB_BASE - 0x50000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_b1     = ( ( GPIOB_BASE - 0x50000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_b2     = ( ( GPIOB_BASE - 0x50000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_b3     = ( ( GPIOB_BASE - 0x50000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_b4     = ( ( GPIOB_BASE - 0x50000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_b5     = ( ( GPIOB_BASE - 0x50000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_b6     = ( ( GPIOB_BASE - 0x50000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_b7     = ( ( GPIOB_BASE - 0x50000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_b8     = ( ( GPIOB_BASE - 0x50000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_b9     = ( ( GPIOB_BASE - 0x50000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_b10    = ( ( GPIOB_BASE - 0x50000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_b11    = ( ( GPIOB_BASE - 0x50000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_b12    = ( ( GPIOB_BASE - 0x50000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_b13    = ( ( GPIOB_BASE - 0x50000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_b14    = ( ( GPIOB_BASE - 0x50000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_b15    = ( ( GPIOB_BASE - 0x50000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 2
    E_ttc_gpio_pin_c0     = ( ( GPIOC_BASE - 0x50000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_c1     = ( ( GPIOC_BASE - 0x50000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_c2     = ( ( GPIOC_BASE - 0x50000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_c3     = ( ( GPIOC_BASE - 0x50000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_c4     = ( ( GPIOC_BASE - 0x50000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_c5     = ( ( GPIOC_BASE - 0x50000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_c6     = ( ( GPIOC_BASE - 0x50000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_c7     = ( ( GPIOC_BASE - 0x50000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_c8     = ( ( GPIOC_BASE - 0x50000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_c9     = ( ( GPIOC_BASE - 0x50000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_c10    = ( ( GPIOC_BASE - 0x50000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_c11    = ( ( GPIOC_BASE - 0x50000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_c12    = ( ( GPIOC_BASE - 0x50000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_c13    = ( ( GPIOC_BASE - 0x50000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_c14    = ( ( GPIOC_BASE - 0x50000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_c15    = ( ( GPIOC_BASE - 0x50000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 3
    E_ttc_gpio_pin_d0     = ( ( GPIOD_BASE - 0x50000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_d1     = ( ( GPIOD_BASE - 0x50000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_d2     = ( ( GPIOD_BASE - 0x50000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_d3     = ( ( GPIOD_BASE - 0x50000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_d4     = ( ( GPIOD_BASE - 0x50000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_d5     = ( ( GPIOD_BASE - 0x50000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_d6     = ( ( GPIOD_BASE - 0x50000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_d7     = ( ( GPIOD_BASE - 0x50000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_d8     = ( ( GPIOD_BASE - 0x50000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_d9     = ( ( GPIOD_BASE - 0x50000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_d10    = ( ( GPIOD_BASE - 0x50000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_d11    = ( ( GPIOD_BASE - 0x50000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_d12    = ( ( GPIOD_BASE - 0x50000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_d13    = ( ( GPIOD_BASE - 0x50000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_d14    = ( ( GPIOD_BASE - 0x50000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_d15    = ( ( GPIOD_BASE - 0x50000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 4
    E_ttc_gpio_pin_h0     = ( ( GPIOH_BASE - 0x50000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_h1     = ( ( GPIOH_BASE - 0x50000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_h2     = ( ( GPIOH_BASE - 0x50000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_h3     = ( ( GPIOH_BASE - 0x50000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_h4     = ( ( GPIOH_BASE - 0x50000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_h5     = ( ( GPIOH_BASE - 0x50000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_h6     = ( ( GPIOH_BASE - 0x50000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_h7     = ( ( GPIOH_BASE - 0x50000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_h8     = ( ( GPIOH_BASE - 0x50000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_h9     = ( ( GPIOH_BASE - 0x50000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_h10    = ( ( GPIOH_BASE - 0x50000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_h11    = ( ( GPIOH_BASE - 0x50000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_h12    = ( ( GPIOH_BASE - 0x50000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_h13    = ( ( GPIOH_BASE - 0x50000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_h14    = ( ( GPIOH_BASE - 0x50000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_h15    = ( ( GPIOH_BASE - 0x50000000 ) ) + 15 * 4,
#endif
    E_ttc_gpio_pin_unknown
} e_gpio_stm32l0xx_pin;

typedef enum {
    E_ttc_gpio_bank_none,
    E_ttc_gpio_bank_a = ( t_u32 ) GPIOA,
    E_ttc_gpio_bank_b = ( t_u32 ) GPIOB,
    E_ttc_gpio_bank_c = ( t_u32 ) GPIOC,
    E_ttc_gpio_bank_d = ( t_u32 ) GPIOD,
    tgb_bank_h = ( t_u32 ) GPIOH,
    E_ttc_gpio_bank_unknown
} e_gpio_stm32l0xx_bank;

typedef struct {  // stm32l1xx specific configuration data of single GPIO device
    e_gpio_stm32l0xx_bank Bank;
    //ToDo:    gpio_stm32l0xx_pull_e Pull; // push/pull resistor
    //ToDo:    gpio_stm32l0xx_mode_e Mode; // port mode
} __attribute__( ( __packed__ ) ) t_gpio_stm32l0xx_values;

// t_ttc_gpio_values is required by ttc_gpio_types.h
#define t_ttc_gpio_values t_gpio_stm32l0xx_values

//} Structures/ Enums


#endif //GPIO_STM32L0XX_TYPES_H
