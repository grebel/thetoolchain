/** { gpio_stm32f1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for gpio devices on stm32f1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 20 at 20140205 06:29:17 UTC
 *
 *  Note: See ttc_gpio.h for description of stm32f1xx independent GPIO implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "gpio_stm32f1xx.h"

//{ Global Variables ***********************************************************
const t_u32 ParallelPortConfigs[16] = {  0x00000000, // all possible configuration settings of an 8-bit wide parallel port
                                         0x11111111,
                                         0x22222222,
                                         0x33333333,
                                         0x44444444,
                                         0x55555555,
                                         0x66666666,
                                         0x77777777,
                                         0x88888888,
                                         0x99999999,
                                         0xaaaaaaaa,
                                         0xbbbbbbbb,
                                         0xcccccccc,
                                         0xdddddddd,
                                         0xeeeeeeee,
                                         0xffffffff
                                      };

//}
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _gpio_stm32f1xx_foo(t_ttc_gpio_config* Config)

/** compiles 4 bit configuration setting suitable for MODEx+CNFx fields of  t_gpiox_crl/ t_gpiox_crh
 *
 * Note: This function is private and normally should not be called from outside
 *
 * @param Type           mode to use for all 8 pins  (-> ttc_gpio_types.h:e_ttc_gpio_mode)
 * @param Speed          speed to use for all 8 pins (-> ttc_gpio_types.h:e_ttc_gpio_speed)
 * @return               4 bit value to be written at position corresponding to desired pin into GPIOx->CRL/ GPIOx->CRH
 */
t_u8 _gpio_stm32f1xx_compile_port_config( e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );

/** enable/ disable a complete GPIO bank
 *
 * @param Bank     GPIO bank to enable/disable (GPIO_BANKA, GPIO_BANKB, ...)
 */
void _gpio_stm32f1xx_bank_enable( t_ttc_gpio_bank GPIOx );
void _gpio_stm32f1xx_bank_disable( t_ttc_gpio_bank GPIOx );

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
//{ Function definitions *******************************************************

// single ports
void   gpio_stm32f1xx_deinit( e_gpio_stm32f1xx_pin PortPin ) {

    const t_u32 CR_ResetState = 0x44444444;

    t_register_stm32f1xx_gpio* GPIOx;
    t_u8 Pin;
    gpio_stm32f1xx_from_index( ( t_base ) PortPin, ( t_ttc_gpio_bank* ) &GPIOx, &Pin );
    volatile t_register_stm32f1xx_gpio_32* GPIOx_32 = ( volatile t_register_stm32f1xx_gpio_32* ) GPIOx;

    t_u32 PortConfig = _gpio_stm32f1xx_compile_port_config( E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
    if ( Pin < 8 ) { // pin configuration is in CRL
        t_u8 Shift = Pin * 4;
        t_u32 CRL = GPIOx_32->CRL & ( ~( 0x0f << Shift ) ); // load register + clear configuration of Pin
        CRL |= ( PortConfig << Shift );
        GPIOx_32->CRL = CRL;                           // activate setting
    }
    else {         // pin configuration is in CRH
        t_u8 Shift = ( Pin - 8 ) * 4;
        t_u32 CRH = GPIOx_32->CRH & ( ~( 0x0f << Shift ) ); // load register + clear configuration of Pin
        CRH |= ( PortConfig << Shift );
        GPIOx_32->CRH = CRH;                           // activate setting
    }


    if ( ( GPIOx_32->CRL == CR_ResetState ) &&
            ( GPIOx_32->CRH == CR_ResetState )
       ) { // all pins in this bank disabled: disable GPIO bank
        _gpio_stm32f1xx_bank_disable( ( e_gpio_stm32f1xx_bank ) GPIOx );
    }
}
void   gpio_stm32f1xx_init( e_gpio_stm32f1xx_pin PortPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ) {

    e_gpio_stm32f1xx_bank GPIOx;
    t_u8 Pin;
    gpio_stm32f1xx_from_index( ( t_base ) PortPin, ( t_ttc_gpio_bank* ) &GPIOx, &Pin );
    _gpio_stm32f1xx_bank_enable( GPIOx );
    volatile t_register_stm32f1xx_gpio_32* GPIOx_32 = ( volatile t_register_stm32f1xx_gpio_32* ) GPIOx;

    t_u32 PortConfig = _gpio_stm32f1xx_compile_port_config( Mode, Speed );
    if ( Pin < 8 ) { // pin configuration is in CRL
        t_u8 Shift = Pin * 4;
        t_u32 CRL = GPIOx_32->CRL & ( ~( 0x0f << Shift ) ); // load register + clear configuration of Pin
        CRL |= ( PortConfig << Shift );
        GPIOx_32->CRL = CRL;                           // activate setting
    }
    else {           // pin configuration is in CRH
        t_u8 Shift = ( Pin - 8 ) * 4;
        t_u32 CRH = GPIOx_32->CRH & ( ~( 0x0f << Shift ) ); // load register + clear configuration of Pin
        CRH |= ( PortConfig << Shift );
        GPIOx_32->CRH = CRH;                           // activate setting
    }

    if ( Mode == E_ttc_gpio_mode_input_pull_down ) // activate pull down resistor
    { GPIOx_32->BRR  = 1 << Pin; }
    if ( Mode == E_ttc_gpio_mode_input_pull_up ) // activate pull down resistor
    { GPIOx_32->BSRR = 1 << Pin; }
}
void   gpio_stm32f1xx_prepare() {

    // disable clocks to all gpio ports (will be reenabled individually)
    register_stm32f1xx_RCC.APB2ENR.IOPA_EN = 0;
    register_stm32f1xx_RCC.APB2ENR.IOPB_EN = 0;
    register_stm32f1xx_RCC.APB2ENR.IOPC_EN = 0;
    register_stm32f1xx_RCC.APB2ENR.IOPD_EN = 0;
    register_stm32f1xx_RCC.APB2ENR.IOPE_EN = 0;
    register_stm32f1xx_RCC.APB2ENR.IOPF_EN = 0;
    register_stm32f1xx_RCC.APB2ENR.IOPG_EN = 0;
}
void   gpio_stm32f1xx_get_configuration( e_gpio_stm32f1xx_pin PortPin, t_ttc_gpio_config* Configuration ) {
    Assert_GPIO_Writable( Configuration, ttc_assert_origin_auto );  // pointers must not be NULL

    t_ttc_gpio_bank GPIOx = ( t_ttc_gpio_bank ) cm3_calc_peripheral_Word( PortPin );
    t_u8            Pin   = ( t_u8 )            cm3_calc_peripheral_BitNumber( PortPin );
    volatile t_register_stm32f1xx_gpio_32* GPIOx_32 = ( volatile t_register_stm32f1xx_gpio_32* ) GPIOx;

    Configuration->PhysicalIndex  = PortPin;
    Configuration->Register       = ( t_register_stm32f1xx_gpio* ) GPIOx;
    Configuration->Architecture   = E_ttc_gpio_architecture_stm32f1xx;
    Configuration->Values.Bank    = ( t_u32 ) GPIOx;

    t_u32 CR = 0;
    if ( Pin < 8 ) { // pin configuration is in CRL
        t_u8 Shift = Pin * 4;
        CR = GPIOx_32->CRL >> Shift; // load register
    }
    else {           // pin configuration is in CRH
        t_u8 Shift = ( Pin - 8 ) * 4;
        CR = GPIOx_32->CRH >> Shift; // load register
    }
    t_u8 MODE = CR & 0b0011;
    t_u8 CNF  = ( CR & 0b1100 ) >> 2;
    Configuration->Values.ModeSpeed = CR;

    switch ( MODE ) { // according RM0008 p. 166-167
        case 0b00: { // input mode
            switch ( CNF ) {
                case 0b00:
                    Configuration->Mode  = E_ttc_gpio_mode_analog_in;
                    Configuration->Speed = E_ttc_gpio_speed_max;
                    break;
                case 0b01:
                    Configuration->Mode  = E_ttc_gpio_mode_input_floating;
                    Configuration->Speed = E_ttc_gpio_speed_max;
                    break;
                case 0b10:
                    if ( Configuration->Register->ODR.DATA & ( 1 << Pin ) ) // pull up resistor active
                    { Configuration->Mode  = E_ttc_gpio_mode_input_pull_up; }
                    else                                             // pull down resistor active
                    { Configuration->Mode  = E_ttc_gpio_mode_input_pull_down; }

                    Configuration->Speed = E_ttc_gpio_speed_max;
                    break;
                case 0b11:
                    Configuration->Mode  = E_ttc_gpio_mode_unknown;
                    Configuration->Speed = E_ttc_gpio_speed_unknown;
                    break;
            }
            break;
        }
        default: {   // output mode
            switch ( MODE ) {
                case 0b01: Configuration->Speed = E_ttc_gpio_speed_10mhz;   break;
                case 0b10: Configuration->Speed = E_ttc_gpio_speed_2mhz;    break;
                case 0b11: Configuration->Speed = E_ttc_gpio_speed_50mhz;   break;
                default:   Configuration->Speed = E_ttc_gpio_speed_unknown; break;
            }
            switch ( CNF ) {
                case 0b00: Configuration->Mode  = E_ttc_gpio_mode_output_push_pull;              break;
                case 0b01: Configuration->Mode  = E_ttc_gpio_mode_output_open_drain;             break;
                case 0b10: Configuration->Mode  = E_ttc_gpio_mode_alternate_function_push_pull;  break;
                case 0b11: Configuration->Mode  = E_ttc_gpio_mode_alternate_function_open_drain; break;
            }
            break;
        }
    }
}

// Parallel Ports
void   gpio_stm32f1xx_parallel08_init( t_ttc_gpio_bank GPIOx, t_u8 FirstPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ) {
    Assert( FirstPin < 9, ttc_assert_origin_auto ); // 8-bit wide parallel ports may start at pins 0..8 only

    _gpio_stm32f1xx_bank_enable( GPIOx );
    volatile t_register_stm32f1xx_gpio_32* GPIOx_32 = ( volatile t_register_stm32f1xx_gpio_32* ) GPIOx;

    // compile single port configuration
    t_u8  ShiftLeft  = FirstPin * 4;
    t_u8  ShiftRight = ( 8 - FirstPin ) * 4;

    // read current pin configurations + clear configurations of parallel port pins
    volatile t_u32 MaskL = ( 0xffffffff ^ ( 0xffffffff << ShiftLeft ) );
    volatile t_u32 MaskH = ( 0xffffffff ^ ( 0xffffffff >> ShiftRight ) );
    t_u32 CRL = GPIOx_32->CRL & MaskL;
    t_u32 CRH = GPIOx_32->CRH & MaskH;

    t_u8  PortConfig = _gpio_stm32f1xx_compile_port_config( Mode, Speed );
    t_u32 ParallelPortConfig = ParallelPortConfigs[PortConfig];

    // write new configuration of all 8 bits in parallel
    CRL |= ( ParallelPortConfig << ShiftLeft );
    CRH |= ( ParallelPortConfig >> ShiftRight );

    // activate new configuration
    GPIOx_32->CRL = CRL;
    GPIOx_32->CRH = CRH;
}
void   gpio_stm32f1xx_parallel16_init( t_ttc_gpio_bank GPIOx, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ) {

    volatile t_register_stm32f1xx_gpio_32* GPIOx_32 = ( volatile t_register_stm32f1xx_gpio_32* ) GPIOx;

    // compile single port configuration
    t_u8 PortConfig = _gpio_stm32f1xx_compile_port_config( Mode, Speed );

    _gpio_stm32f1xx_bank_enable( GPIOx );

    // write new configuration of all 16 bits in parallel
    t_u32 ParallelPortConfig = ParallelPortConfigs[PortConfig];
    GPIOx_32->CRL = ParallelPortConfig;
    GPIOx_32->CRH = ParallelPortConfig;
}
void   gpio_stm32f1xx_from_index( t_base PhysicalIndex, t_ttc_gpio_bank* GPIOx, t_u8* Pin ) {
    Assert_GPIO_Writable( GPIOx, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_GPIO_Writable( Pin, ttc_assert_origin_auto );  // pointers must not be NULL

    *GPIOx = ( t_ttc_gpio_bank ) cm3_calc_peripheral_Word( PhysicalIndex );
    *Pin   = ( t_u8 )            cm3_calc_peripheral_BitNumber( PhysicalIndex );
}
t_base gpio_stm32f1xx_create_index( t_ttc_gpio_bank GPIOx, t_u8 Pin ) {
    return ( t_base ) cm3_calc_peripheral_BitBandAddress( GPIOx, Pin );
}
void   gpio_stm32f1xx_alternate_function( e_ttc_gpio_pin PortPin, e_ttc_gpio_alternate_function AlternateFunction ) {

    // enable clock to alternate function block
    register_stm32f1xx_RCC.APB2ENR.AFIO_EN = 1;

    TODO( "implement gpio_stm32f1xx_alternate_function()" )

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************

t_u8 _gpio_stm32f1xx_compile_port_config( e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ) {
    t_u8 PortConfig = 0;

    switch ( Mode ) {
        case E_ttc_gpio_mode_analog_in:
            PortConfig = ( gpc_input_analog << 2 )       | gpm_input;
            break;
        case E_ttc_gpio_mode_input_floating:
            PortConfig = ( gpc_input_floating << 2 )     | gpm_input;
            break;
        case E_ttc_gpio_mode_input_pull_down:
            PortConfig = ( gpc_input_pull_up_down << 2 ) | gpm_input;
            break;
        case E_ttc_gpio_mode_input_pull_up:
            PortConfig = ( gpc_input_pull_up_down << 2 ) | gpm_input;
            break;
        case E_ttc_gpio_mode_output_open_drain:
            if ( Speed < E_ttc_gpio_speed_2mhz )       { PortConfig = ( gpc_output_open_drain << 2 ) | gpm_output_2mhz; }
            else if ( Speed > E_ttc_gpio_speed_10mhz ) { PortConfig = ( gpc_output_open_drain << 2 ) | gpm_output_50mhz; }
            else                                       { PortConfig = ( gpc_output_open_drain << 2 ) | gpm_output_10mhz; }
            break;
        case E_ttc_gpio_mode_output_push_pull:
            if ( Speed < E_ttc_gpio_speed_2mhz )       { PortConfig = ( gpc_output_push_pull << 2 ) | gpm_output_2mhz; }
            else if ( Speed > E_ttc_gpio_speed_10mhz ) { PortConfig = ( gpc_output_push_pull << 2 ) | gpm_output_50mhz; }
            else                                       { PortConfig = ( gpc_output_push_pull << 2 ) | gpm_output_10mhz; }
            break;
        case E_ttc_gpio_mode_alternate_function_push_pull:
            if ( Speed < E_ttc_gpio_speed_2mhz )       { PortConfig = ( gpc_output_alternate_push_pull << 2 ) | gpm_output_2mhz; }
            else if ( Speed > E_ttc_gpio_speed_10mhz ) { PortConfig = ( gpc_output_alternate_push_pull << 2 ) | gpm_output_50mhz; }
            else                                       { PortConfig = ( gpc_output_alternate_push_pull << 2 ) | gpm_output_10mhz; }
            break;
        case E_ttc_gpio_mode_alternate_function_open_drain:
            if ( Speed < E_ttc_gpio_speed_2mhz )       { PortConfig = ( gpc_output_alternate_open_drain << 2 ) | gpm_output_2mhz; }
            else if ( Speed > E_ttc_gpio_speed_10mhz ) { PortConfig = ( gpc_output_alternate_open_drain << 2 ) | gpm_output_50mhz; }
            else                                       { PortConfig = ( gpc_output_alternate_open_drain << 2 ) | gpm_output_10mhz; }
            break;
        default:
            ttc_assert_halt_origin( ttc_assert_origin_auto );
    }
    return PortConfig;
}
void _gpio_stm32f1xx_bank_enable( t_ttc_gpio_bank GPIOx ) {
    switch ( GPIOx ) { // enable peripheral clock to this port
        case E_ttc_gpio_bank_a: register_stm32f1xx_RCC.APB2ENR.IOPA_EN = 1; break;
        case E_ttc_gpio_bank_b: register_stm32f1xx_RCC.APB2ENR.IOPB_EN = 1; break;
        case E_ttc_gpio_bank_c: register_stm32f1xx_RCC.APB2ENR.IOPC_EN = 1; break;
        case E_ttc_gpio_bank_d: register_stm32f1xx_RCC.APB2ENR.IOPD_EN = 1; break;
        case E_ttc_gpio_bank_e: register_stm32f1xx_RCC.APB2ENR.IOPE_EN = 1; break;
#ifdef GPIOF
        case E_ttc_gpio_bank_f: register_stm32f1xx_RCC.APB2ENR.IOPF_EN = 1; break;
#endif
#ifdef GPIOG
        case E_ttc_gpio_bank_g: register_stm32f1xx_RCC.APB2ENR.IOPG_EN = 1; break;
#endif
        default: Assert( 0, ttc_assert_origin_auto ); break; // ERROR: unknown GPIO given!
    }
}
void _gpio_stm32f1xx_bank_disable( t_ttc_gpio_bank GPIOx ) {
    switch ( GPIOx ) { // enable peripheral clock to this port
        case E_ttc_gpio_bank_a: register_stm32f1xx_RCC.APB2ENR.IOPA_EN = 0; break;
        case E_ttc_gpio_bank_b: register_stm32f1xx_RCC.APB2ENR.IOPB_EN = 0; break;
        case E_ttc_gpio_bank_c: register_stm32f1xx_RCC.APB2ENR.IOPC_EN = 0; break;
        case E_ttc_gpio_bank_d: register_stm32f1xx_RCC.APB2ENR.IOPD_EN = 0; break;
        case E_ttc_gpio_bank_e: register_stm32f1xx_RCC.APB2ENR.IOPE_EN = 0; break;
#ifdef GPIOF
        case E_ttc_gpio_bank_f: register_stm32f1xx_RCC.APB2ENR.IOPF_EN = 0; break;
#endif
#ifdef GPIOG
        case E_ttc_gpio_bank_g: register_stm32f1xx_RCC.APB2ENR.IOPG_EN = 0; break;
#endif
        default: Assert( 0, ttc_assert_origin_auto ); break; // ERROR: unknown GPIO given!
    }
}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
