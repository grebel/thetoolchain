#ifndef GPIO_STM32L0XX_H
#define GPIO_STM32L0XX_H

/** { gpio_stm32l0xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for gpio devices on stm32l0xx architectures.
 *  Structures, Enums and Defines being required by high-level gpio and application.
 *
 *  Created from template device_architecture.h revision 22 at 20150305 09:34:47 UTC
 *
 *  Note: See ttc_gpio.h for description of stm32l0xx independent GPIO implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

#define gpio_MODER_MODER0          ((t_u32)0x00000003)
#define gpio_OTYPER_OT_0           ((t_u32)0x00000001)
#define gpio_PUPDR_PUPDR0          ((t_u32)0x00000003)
#define gpio_OSPEEDER_OSPEEDR0     ((t_u32)0x00000003)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_GPIO_STM32L0XX
//
// Implementation status of low-level driver support for gpio devices on stm32l0xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_GPIO_STM32L0XX '?'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_GPIO_STM32L0XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_GPIO_STM32L0XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_GPIO_STM32L0XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "gpio_stm32l0xx.c"
//
#include "gpio_stm32l0xx_types.h"
#include "../ttc_gpio_types.h"
#include "../register/register_stm32l0xx.h"
#include "../basic/basic_stm32l0xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_gpio_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_gpio_foo
//

#define ttc_driver_gpio_init(PortPin, Mode, Speed)                               gpio_stm32l0xx_init(PortPin, Mode, Speed)
#define ttc_driver_gpio_create_index(GPIOx, Pin)                                 gpio_stm32l0xx_create_index(GPIOx, Pin)
#define ttc_driver_gpio_deinit(PortPin)                                          gpio_stm32l0xx_deinit(PortPin)
#define ttc_driver_gpio_from_index(PhysicalIndex, GPIOx, Pin)                    gpio_stm32l0xx_from_index(PhysicalIndex, GPIOx, Pin)
#define ttc_driver_gpio_get(PortPin)                                             gpio_stm32l0xx_get(PortPin)
#define ttc_driver_gpio_clr(PortPin)                                             gpio_stm32l0xx_clr(PortPin)
#define ttc_driver_gpio_set(PortPin)                                             gpio_stm32l0xx_set(PortPin)
#define ttc_driver_gpio_get_configuration(PortPin, Configuration)                gpio_stm32l0xx_get_configuration(PortPin, Configuration)
#define ttc_driver_gpio_prepare()                                                gpio_stm32l0xx_prepare()
#define ttc_driver_gpio_parallel08_init(GPIOx, FirstPin, Mode, Speed)      gpio_stm32l0xx_parallel08_init(GPIOx, FirstPin, Mode, Speed)
#define ttc_driver_gpio_parallel16_init(GPIOx, Mode, Speed)                      gpio_stm32l0xx_parallel16_init(GPIOx, Mode, Speed)
#define ttc_driver_gpio_alternate_function(PortPin, AlternateFunction)           gpio_stm32l0xx_alternate_function(PortPin, AlternateFunction)
#define ttc_driver_gpio_put(PortPin, Value)                                      gpio_stm32l0xx_put(PortPin, Value)
#define ttc_driver_gpio_parallel08_1_get()                                       ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_1_BANK)->IDR )
#define ttc_driver_gpio_parallel08_2_get()                                       ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_2_BANK)->IDR )
#define ttc_driver_gpio_parallel08_3_get()                                       ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_3_BANK)->IDR )
#define ttc_driver_gpio_parallel08_4_get()                                       ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_4_BANK)->IDR )
#define ttc_driver_gpio_parallel08_5_get()                                       ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_5_BANK)->IDR )
#define ttc_driver_gpio_parallel08_6_get()                                       ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_6_BANK)->IDR )
#define ttc_driver_gpio_parallel08_7_get()                                       ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_7_BANK)->IDR )
#define ttc_driver_gpio_parallel08_8_get()                                       ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_8_BANK)->IDR )
#define ttc_driver_gpio_parallel08_9_get()                                       ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_9_BANK)->IDR )
#define ttc_driver_gpio_parallel08_10_get()                                      ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_10_BANK)->IDR )
#define ttc_driver_gpio_parallel08_1_put(Value)                                  ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_1_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_2_put(Value)                                  ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_2_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_3_put(Value)                                  ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_3_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_4_put(Value)                                  ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_4_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_5_put(Value)                                  ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_5_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_6_put(Value)                                  ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_6_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_7_put(Value)                                  ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_7_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_8_put(Value)                                  ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_8_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_9_put(Value)                                  ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_9_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_10_put(Value)                                 ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_10_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel16_1_get gpio_stm32l0xx_parallel16_1_get
#define ttc_driver_gpio_parallel16_1_put gpio_stm32l0xx_parallel16_1_put
#define ttc_driver_gpio_parallel16_10_get gpio_stm32l0xx_parallel16_10_get
#define ttc_driver_gpio_parallel16_10_put gpio_stm32l0xx_parallel16_10_put
#define ttc_driver_gpio_parallel16_2_get gpio_stm32l0xx_parallel16_2_get
#define ttc_driver_gpio_parallel16_2_put gpio_stm32l0xx_parallel16_2_put
#define ttc_driver_gpio_parallel16_3_get gpio_stm32l0xx_parallel16_3_get
#define ttc_driver_gpio_parallel16_3_put gpio_stm32l0xx_parallel16_3_put
#define ttc_driver_gpio_parallel16_4_get gpio_stm32l0xx_parallel16_4_get
#define ttc_driver_gpio_parallel16_4_put gpio_stm32l0xx_parallel16_4_put
#define ttc_driver_gpio_parallel16_5_get gpio_stm32l0xx_parallel16_5_get
#define ttc_driver_gpio_parallel16_5_put gpio_stm32l0xx_parallel16_5_put
#define ttc_driver_gpio_parallel16_6_get gpio_stm32l0xx_parallel16_6_get
#define ttc_driver_gpio_parallel16_6_put gpio_stm32l0xx_parallel16_6_put
#define ttc_driver_gpio_parallel16_7_get gpio_stm32l0xx_parallel16_7_get
#define ttc_driver_gpio_parallel16_7_put gpio_stm32l0xx_parallel16_7_put
#define ttc_driver_gpio_parallel16_8_get gpio_stm32l0xx_parallel16_8_get
#define ttc_driver_gpio_parallel16_8_put gpio_stm32l0xx_parallel16_8_put
#define ttc_driver_gpio_parallel16_9_get gpio_stm32l0xx_parallel16_9_get
#define ttc_driver_gpio_parallel16_9_put gpio_stm32l0xx_parallel16_9_put
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_gpio.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_gpio.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */



/** { creates numeric index representing given GPIO-bank and pin number
 *
 * Interrupt sources are identified by their type plus a numerical index.
 * The type simply names a kind of interrupt source (e.g. receive buffer of an usart is not empty).
 * Often more than one identical physical devices of equal type are present. The physical index
 * identifies which device is meant (e.g. 0 = USART1, 1 = USART2, ...)
 * Conversion functions convert between physical index and extra information required to access hardware.
 *
 * @param GPIOx  GPIO bank to use (-> ttc_gpio_types.h:t_ttc_gpio_bank)
 *               Note: GPIOx, Pin can be given as one argument by use of Pin_Pxn macro (TTC_GPIO_BANK_A, 7) = Pin_PA7
 * @param Pin    0..15
 * @return       32-bit value representing given GPIO-pin in a scalar datatype
}*/
t_base gpio_stm32l0xx_create_index( t_ttc_gpio_bank GPIOx, t_u8 Pin );


/** shutdown single GPIO unit device
 * @param PortPin bit definition
 */
void gpio_stm32l0xx_deinit( e_ttc_gpio_pin PortPin );


/** { restores GPIO-bank and pin number from index value
 *
 * @param PhysicalIndex  32-bit value representing GPIO-bank + pin
 * @param GPIOx          loaded with GPIO bank (-> ttc_gpio_types.h:t_ttc_gpio_bank)
 * @param Pin            loaded with pin number
}*/
void gpio_stm32l0xx_from_index( t_base PhysicalIndex, t_ttc_gpio_bank* GPIOx, t_u8* Pin );


/** get value of single input port bit (voltage level at input pin)
 *
 * @param PortPin bit definition
 * @return   =
 */
BOOL gpio_stm32l0xx_get( e_gpio_stm32l0xx_pin PortPin );
#define gpio_stm32l0xx_get(Pin) *((volatile t_u8*) ( ((t_u32) Pin) + TTC_GPIO_OFFSET_IDR  )  )


/** initializes single GPIO unit for operation
 * @param PortPin         port bit definition
 * @param Configuration  loaded with current configuration of given PortPin
 */
void gpio_stm32l0xx_get_configuration( e_ttc_gpio_pin PortPin, t_ttc_gpio_config* Configuration );


/** initialize 8 bit wide parallel port
 *
 * Note: Runtime measured on STM32F107 @72MHz as < 2.2us
 *
 * @param Bank+FirstPin  Bank and Pin given as one argument by use of Pin_Pxn macro: E.g. (GPIOA, 7) = Pin_PA7
 * @param Bank           GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 * @param FirstPin       0..8  lowest of 8 consecutive pins to use
 * @param Type           mode to use for all 8 pins  (-> ttc_gpio_types.h:e_ttc_gpio_mode)
 * @param Speed          speed to use for all 8 pins (-> ttc_gpio_types.h:e_ttc_gpio_speed)
 * @param PortPin   =
 * @param Mode      =
 */
void gpio_stm32l0xx_init( e_ttc_gpio_pin PortPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );


/** initialize all pins of 16-bit wide parallel port at once
 *
 * Note: Runtime measured on STM32F107 @72MHz as < 3us
 *
 * @param Bank     GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 * @param GPIOx   =
 * @param Mode    =
 * @param Speed   =
 */
void gpio_stm32l0xx_parallel16_init( t_ttc_gpio_bank GPIOx, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );


/** initialize 8 bit wide parallel port
 *
 * Note: Runtime measured on STM32F107 @72MHz as < 2.2us
 *
 * @param Bank+FirstPin  Bank and Pin given as one argument by use of Pin_Pxn macro: E.g. (GPIOA, 7) = Pin_PA7
 * @param Bank           GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 * @param FirstPin       0..8  lowest of 8 consecutive pins to use
 * @param Type           mode to use for all 8 pins  (-> ttc_gpio_types.h:e_ttc_gpio_mode)
 * @param Speed          speed to use for all 8 pins (-> ttc_gpio_types.h:e_ttc_gpio_speed)
 * @param GPIOx   =
 * @param Mode    =
 */
void gpio_stm32l0xx_parallel08_init( t_ttc_gpio_bank GPIOx, t_u8 FirstPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );


/** Prepares gpio driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void gpio_stm32l0xx_prepare();


/** set single output port bit to logical 1 (usually high level)
 *
 * @param PortPin bit definition
 */
void gpio_stm32l0xx_set( e_ttc_gpio_pin PortPin );
//#define gpio_stm32l0xx_set(Pin) *((volatile t_u8*) ( ((t_u32) Pin) + TTC_GPIO_OFFSET_BSRR)  ) = 1

/** set single output port bit to logical 0 (usually high level)
 *
 * @param PortPin bit definition
 */
void gpio_stm32l0xx_clr( e_ttc_gpio_pin PortPin );
//#define gpio_stm32l0xx_clr(Pin) *((volatile t_u8*) ( ((t_u32) Pin) + TTC_GPIO_OFFSET_BRR )  ) = 1

/** Changes the mapping of the specified pin.
 *
 * @param PortPin       Port bit definition
 * @param AlternateFunction     Selects the pin to used as Alternat function.
 */
void gpio_stm32l0xx_alternate_function( e_gpio_stm32l0xx_pin PortPin, t_u8 AlternateFunction );


/** Set single output port bit to either logical high or Low.
 *
 * @param PortPin bit definition
 * @param Value   new logical state of output pin (==true: set to high; ==false: set to low)
 */
void gpio_stm32l0xx_put( e_ttc_gpio_pin PortPin, BOOL Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32l0xx_parallel16_10_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32l0xx_parallel16_10_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32l0xx_parallel16_2_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32l0xx_parallel16_2_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32l0xx_parallel16_3_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32l0xx_parallel16_3_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32l0xx_parallel16_4_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32l0xx_parallel16_4_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32l0xx_parallel16_5_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32l0xx_parallel16_5_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32l0xx_parallel16_6_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32l0xx_parallel16_6_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32l0xx_parallel16_7_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32l0xx_parallel16_7_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32l0xx_parallel16_8_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32l0xx_parallel16_8_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32l0xx_parallel16_9_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32l0xx_parallel16_9_put( t_u16 Value );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _gpio_stm32l0xx_foo(t_ttc_gpio_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //GPIO_STM32L0XX_H
