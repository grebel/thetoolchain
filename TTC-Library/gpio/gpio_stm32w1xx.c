/** { gpio_stm32w1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for gpio devices on stm32w1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 20 at 20140208 06:39:49 UTC
 *
 *  Note: See ttc_gpio.h for description of stm32w1xx independent GPIO implementation.
 * 
 *  Authors: Gregor Rebel
}*/

#include "gpio_stm32w1xx.h"

//{ Global Variables ***********************************************************

//}Global Variables
//{ Function definitions *******************************************************

#ifdef EXTENSION_255_stm32w1xx_standard_peripherals
  void gpio_stm32w1xx_prepare() {




}
  void gpio_stm32w1xx_deinit_all() {
           GPIO_DeInit(GPIOA);
           GPIO_DeInit(GPIOB);
           GPIO_DeInit(GPIOC);
}
  void gpio_stm32w1xx_init(e_gpio_stm32w1xx_pin PortPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed) {
    (void) Speed; // unused

    e_gpio_stm32w1xx_bank GPIOx;
    t_u8 Pin;
    gpio_stm32w1xx_from_index( (t_base) PortPin, &GPIOx, &Pin );

    t_u32 PortConfig = _gpio_stm32w1xx_compile_port_config(Mode);
    if (Pin < 4){ //write designator into GPIO_PxCFGL if Pin 0-3
        t_u8 Shift = Pin * 4; //offset from base address
        t_u32* CFGL = (t_u32*) &( ((t_register_stm32w1xx_gpiox *) GPIOx)->CFGL ); // assign type-casted address to pointer (t_u32 pointer)
        *CFGL = *CFGL & (~(0x0f<<Shift));                                         // then use pointer in bool expression
        *CFGL |= (PortConfig <<Shift);                                            // shift portconfig, use binary OR, then put back in register address
    }
    else { //or GPIO_PxCFGH if Pin 4-7
        t_u8 Shift = (Pin-4) * 4; //offset from base address for high 4 pins
        t_u32* CFGH = (t_u32*) &(((t_register_stm32w1xx_gpiox *) GPIOx)->CFGH);
        *CFGH = *CFGH & (~(0x0f<<Shift));
        *CFGH |= (PortConfig <<Shift);
    }
}
#else
void   gpio_stm32w1xx_prepare() {




}
void   gpio_stm32w1xx_deinit_all() {
//         GPIO_DeInit(GPIOA);
//         GPIO_DeInit(GPIOB);
//         GPIO_DeInit(GPIOC);
}
void   gpio_stm32w1xx_init(e_gpio_stm32w1xx_pin PortPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed) {
  (void) Speed; // unused

  e_gpio_stm32w1xx_bank GPIOx;
  t_u8 Pin;
  gpio_stm32w1xx_from_index( (t_base) PortPin, &GPIOx, &Pin );

  t_u32 PortConfig = _gpio_stm32w1xx_compile_port_config(Mode);
  if (Pin < 4){ //write designator into GPIO_PxCFGL if Pin 0-3
      t_u8 Shift = Pin * 4; //offset from base address
      t_u32* CFGL = (t_u32*) &( ((t_register_stm32w1xx_gpiox *) GPIOx)->CFGL ); // assign type-casted address to pointer (t_u32 pointer)
      *CFGL = *CFGL & (~(0x0f<<Shift));                                         // then use pointer in bool expression
      *CFGL |= (PortConfig <<Shift);                                            // shift portconfig, use binary OR, then put back in register address
  }
  else { //or GPIO_PxCFGH if Pin 4-7
      t_u8 Shift = (Pin-4) * 4; //offset from base address for high 4 pins
      t_u32* CFGH = (t_u32*) &(((t_register_stm32w1xx_gpiox *) GPIOx)->CFGH);
      *CFGH = *CFGH & (~(0x0f<<Shift));
      *CFGH |= (PortConfig <<Shift);
  }
}
#endif
void   gpio_stm32w1xx_get_configuration(e_gpio_stm32w1xx_pin PortPin, t_ttc_gpio_config* Configuration) {
    Assert_GPIO(Configuration, ttc_assert_origin_auto); // pointers must not be NULL

    ttc_assert_halt_origin(ttc_assert_origin_auto); // ToDo: retrieve configuration from current register settings

}
void   gpio_stm32w1xx_deinit(e_gpio_stm32w1xx_pin PortPin) {
    gpio_stm32w1xx_init(PortPin, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min);
}
t_base gpio_stm32w1xx_create_index(t_ttc_gpio_bank GPIOx, t_u8 Pin) {
           return (t_base) cm3_calc_peripheral_BitBandAddress(GPIOx, Pin);
       }
void   gpio_stm32w1xx_from_index(t_base PhysicalIndex, t_ttc_gpio_bank* GPIOx, t_u8* Pin) {
           Assert_GPIO(GPIOx, ttc_assert_origin_auto); // pointers must not be NULL
           Assert_GPIO(Pin, ttc_assert_origin_auto);   // pointers must not be NULL

           *GPIOx = (t_ttc_gpio_bank) cm3_calc_peripheral_Word(PhysicalIndex);
           *Pin   = (t_u8)            cm3_calc_peripheral_BitNumber(PhysicalIndex);
       }
void   gpio_stm32w1xx_alternate_function(e_ttc_gpio_pin PortPin, e_ttc_gpio_alternate_function AlternateFunction) {


#warning Function gpio_stm32w1xx_alternate_function() is empty.
    

}
void   gpio_stm32w1xx_put(e_ttc_gpio_pin PortPin, BOOL Value) {


#warning Function gpio_stm32w1xx_put() is empty.
    

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************

t_u8 _gpio_stm32w1xx_compile_port_config(e_ttc_gpio_mode Mode) {
    t_u8 PortConfig = 0;

    switch (Mode) {
    case E_ttc_gpio_mode_analog_in:
        PortConfig = gpmode_analog;
        break;
    case E_ttc_gpio_mode_input_floating:
        PortConfig = gpmode_input_floating;
        break;
    case E_ttc_gpio_mode_input_pull_down:
        PortConfig = gpmode_input_pull_upORdown;
        break;
    case E_ttc_gpio_mode_input_pull_up:
        PortConfig = gpmode_input_pull_upORdown;
        break;
    case E_ttc_gpio_mode_output_open_drain:
        PortConfig = gpmode_output_open_drain;
        break;
    case E_ttc_gpio_mode_output_push_pull:
        PortConfig = gpmode_output_push_pull;
        break;
    case E_ttc_gpio_mode_alternate_function_push_pull:
        PortConfig = gpmode_output_alternate_push_pull;
        break;
    case E_ttc_gpio_mode_alternate_function_open_drain:
        PortConfig = gpmode_output_alternate_open_drain;
        break;
    case E_ttc_gpio_mode_alternate_function_special_sclk:
        PortConfig = gpmode_output_alternate_pp_SPI_SCLK;
        break;
    default:
        ttc_assert_halt_origin(ttc_assert_origin_auto);
    }
    return PortConfig;
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
