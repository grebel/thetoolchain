#ifndef GPIO_STM32L1XX_TYPES_H
#define GPIO_STM32L1XX_TYPES_H

/** { gpio_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for GPIO devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_gpio_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140206 15:39:14 UTC
 *
 *  Note: See ttc_gpio.h for description of architecture independent GPIO implementation.
 *
 *  Authors: Greg Knoll 2014, Gregor Rebel
 *
}*/

#ifndef USE_STDPERIPH_DRIVER
    #define USE_STDPERIPH_DRIVER
#endif
#if (0)  // obtaining memory adresses from CPAL library header file (must lie in some include path!)
    #include "stm32l1xx.h"

    #define TTC_GPIO_BASE_A  GPIOA_BASE
    #define TTC_GPIO_BASE_B  GPIOB_BASE
    #define TTC_GPIO_BASE_C  GPIOC_BASE
    #define TTC_GPIO_BASE_D  GPIOD_BASE
    #define TTC_GPIO_BASE_E  GPIOE_BASE
    //#  define TTC_GPIO_BASE_F
    //#  define TTC_GPIO_BASE_G
    #define TTC_GPIO_BASE_H  GPIOH_BASE

#else    // defining memory adresses on our own (one less external dependency)
    #define TTC_GPIO_BASE_A  0x40020000
    #define TTC_GPIO_BASE_B  0x40020400
    #define TTC_GPIO_BASE_C  0x40020800
    #define TTC_GPIO_BASE_D  0x40020c00
    #define TTC_GPIO_BASE_E  0x40021000
    //#  define TTC_GPIO_BASE_F
    //#  define TTC_GPIO_BASE_G
    #define TTC_GPIO_BASE_H  0x40021400
#endif
#include "../register/register_stm32l1xx_types.h"

//{ Defines/ TypeDefs ******************************************************
#define t_ttc_gpio_bank        e_gpio_stm32l1xx_bank // datatype used to identify GPIO banks
#define TTC_GPIO_MAX_AMOUNT  6            // amount of gpio banks (A..E & H)
#define TTC_GPIO_MAX_PINS    16           // amount of port pins per bank
#define t_ttc_gpio_register  t_register_stm32l1xx_gpio*

/** bitband address calculation
 *
 * Each GPIO+Pin pair is identified by the corresponding bit band address of bit Pin in GPIO base register.
 * The offsets below have to be added to t_ttc_gpio to get the bitband address for individual register operations.
 * -> RM0038 p.139
 */
//X #define t_ttc_gpio           e_ttc_gpio_pin  // bitband address of single Portbit in GPIO register base
#define TTC_GPIO_OFFSET_MODER   (0 * 4 * 32)      // adddress offset of bitband address for register MODER
#define TTC_GPIO_OFFSET_OTYPER  (1 * 4 * 32)      // adddress offset of bitband address for register OTYPER
#define TTC_GPIO_OFFSET_OSPEED  (2 * 4 * 32)      // adddress offset of bitband address for register OSPEED
#define TTC_GPIO_OFFSET_PUPDR   (3 * 4 * 32)      // adddress offset of bitband address for register PUPDR
#define TTC_GPIO_OFFSET_IDR     (4 * 4 * 32)      // adddress offset of bitband address for register IDR
#define TTC_GPIO_OFFSET_ODR     (5 * 4 * 32)      // adddress offset of bitband address for register ODR
#define TTC_GPIO_OFFSET_BSRR    (6 * 4 * 32)      // adddress offset of bitband address for register BSRR
#define TTC_GPIO_OFFSET_LCKR    (7 * 4 * 32)      // adddress offset of bitband address for register LCKR
#define TTC_GPIO_OFFSET_AFRL    (8 * 4 * 32)      // adddress offset of bitband address for register AFRL
#define TTC_GPIO_OFFSET_AFRH    (9 * 4 * 32)      // adddress offset of bitband address for register AFRH

/** Parallel Port Definitions
 *
 * Parallel Ports make use of GPIO banks.
 * All pins of each GPIO bank can be set/ read in parallel.
 *
 */
#define TTC_GPIO_BANK_A E_ttc_gpio_bank_a
#define TTC_GPIO_BANK_B E_ttc_gpio_bank_b
#define TTC_GPIO_BANK_C E_ttc_gpio_bank_c
#define TTC_GPIO_BANK_D E_ttc_gpio_bank_d
#define TTC_GPIO_BANK_E E_ttc_gpio_bank_e
#ifdef TTC_GPIO_BASE_H
    #define TTC_GPIO_BANK_H tgb_bank_h
#endif
#ifndef TTC_GPIO1   // device not defined in makefile
    #define TTC_GPIO1    E_ttc_gpio_architecture_stm32l1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

typedef enum {
    /** Address of each port bit is calculated as bitband address of corresponding bit in GPIO Base register.
     *  This makes it very easy to calculate bitband address of BSRR, BRR, IDR and ODR addresses for fast
     *  port bit operations.
     *  Using an enum for register addresses makes debugging with GDB a lot easier.
     *  See implementation of gpio_stm32f1xx_set() for details.
     */
    E_ttc_gpio_pin_none = 0, // item mandatory for each architecture

#if TTC_GPIO_MAX_AMOUNT > 0
    E_ttc_gpio_pin_a0     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_a1     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_a2     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_a3     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_a4     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_a5     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_a6     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_a7     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_a8     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_a9     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_a10    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_a11    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_a12    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_a13    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_a14    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_a15    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_A - 0x40000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 1
    E_ttc_gpio_pin_b0     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_b1     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_b2     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_b3     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_b4     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_b5     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_b6     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_b7     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_b8     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_b9     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_b10    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_b11    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_b12    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_b13    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_b14    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_b15    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_B - 0x40000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 2
    E_ttc_gpio_pin_c0     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_c1     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_c2     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_c3     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_c4     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_c5     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_c6     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_c7     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_c8     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_c9     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_c10    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_c11    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_c12    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_c13    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_c14    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_c15    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_C - 0x40000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 3
    E_ttc_gpio_pin_d0     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_d1     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_d2     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_d3     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_d4     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_d5     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_d6     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_d7     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_d8     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_d9     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_d10    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_d11    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_d12    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_d13    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_d14    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_d15    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_D - 0x40000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 4
    E_ttc_gpio_pin_e0     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_e1     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_e2     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_e3     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_e4     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_e5     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_e6     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_e7     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_e8     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_e9     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_e10    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_e11    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_e12    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_e13    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_e14    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_e15    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_E - 0x40000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 5
    E_ttc_gpio_pin_h0     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_h1     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_h2     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_h3     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_h4     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_h5     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_h6     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_h7     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_h8     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_h9     = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_h10    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_h11    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_h12    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_h13    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_h14    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_h15    = ( 0x42000000 + 32 * ( TTC_GPIO_BASE_H - 0x40000000 ) ) + 15 * 4,
#endif
    E_ttc_gpio_pin_unknown
} e_gpio_stm32l1xx_pin;

typedef enum { // -> RM0038 p.131
    E_ttc_gpio_alternate_function_AF0    = 0,  // System
    E_ttc_gpio_alternate_function_System = E_ttc_gpio_alternate_function_AF0,

    E_ttc_gpio_alternate_function_AF1    = 1,  // TIM2
    E_ttc_gpio_alternate_function_TIM2   = E_ttc_gpio_alternate_function_AF1,

    E_ttc_gpio_alternate_function_AF2    = 2,  // TIM3..5
    E_ttc_gpio_alternate_function_TIM3   = E_ttc_gpio_alternate_function_AF2,
    E_ttc_gpio_alternate_function_TIM4   = E_ttc_gpio_alternate_function_AF2,
    E_ttc_gpio_alternate_function_TIM5   = E_ttc_gpio_alternate_function_AF2,

    E_ttc_gpio_alternate_function_AF3    = 3,  // TIM9..11
    E_ttc_gpio_alternate_function_TIM9   = E_ttc_gpio_alternate_function_AF3,
    E_ttc_gpio_alternate_function_TIM10  = E_ttc_gpio_alternate_function_AF3,
    E_ttc_gpio_alternate_function_TIM11  = E_ttc_gpio_alternate_function_AF3,

    E_ttc_gpio_alternate_function_AF4    = 4,  // I2C1, I2C2
    E_ttc_gpio_alternate_function_I2C1   = E_ttc_gpio_alternate_function_AF4,
    E_ttc_gpio_alternate_function_I2C2   = E_ttc_gpio_alternate_function_AF4,

    E_ttc_gpio_alternate_function_AF5    = 5,  // SPI1, SPI2
    E_ttc_gpio_alternate_function_SPI1   = E_ttc_gpio_alternate_function_AF5,
    E_ttc_gpio_alternate_function_SPI2   = E_ttc_gpio_alternate_function_AF5,

    E_ttc_gpio_alternate_function_AF6    = 6,  // SPI3
    E_ttc_gpio_alternate_function_SPI3   = E_ttc_gpio_alternate_function_AF6,

    E_ttc_gpio_alternate_function_AF7    = 7,  // USART1..3
    E_ttc_gpio_alternate_function_USART1 = E_ttc_gpio_alternate_function_AF7,
    E_ttc_gpio_alternate_function_USART2 = E_ttc_gpio_alternate_function_AF7,
    E_ttc_gpio_alternate_function_USART3 = E_ttc_gpio_alternate_function_AF7,

    E_ttc_gpio_alternate_function_AF8    = 8,  // UART4..5
    E_ttc_gpio_alternate_function_UART4  = E_ttc_gpio_alternate_function_AF8,
    E_ttc_gpio_alternate_function_UART5  = E_ttc_gpio_alternate_function_AF8,

    E_ttc_gpio_alternate_function_AF9    = 9,
    E_ttc_gpio_alternate_function_AF10   = 10, // USB
    E_ttc_gpio_alternate_function_USB    = E_ttc_gpio_alternate_function_AF10,

    E_ttc_gpio_alternate_function_AF11   = 11, // LCD
    E_ttc_gpio_alternate_function_LCD    = E_ttc_gpio_alternate_function_AF11,

    E_ttc_gpio_alternate_function_AF12   = 12, // 5FSMC
    E_ttc_gpio_alternate_function_5FSMC  = E_ttc_gpio_alternate_function_AF12,

    E_ttc_gpio_alternate_function_AF13   = 13,
    E_ttc_gpio_alternate_function_AF14   = 14, // RI
    E_ttc_gpio_alternate_function_RI     = E_ttc_gpio_alternate_function_AF14,

    E_ttc_gpio_alternate_function_AF15   = 15, // EVENTOUT
    E_ttc_gpio_alternate_function_EVENTOUT = E_ttc_gpio_alternate_function_AF11,

    E_ttc_gpio_alternate_function_unknown
} e_gpio_stm32l1xx_alternate_function;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

#define e_ttc_gpio_alternate_function  e_gpio_stm32l1xx_alternate_function
#define e_ttc_gpio_pin                 e_gpio_stm32l1xx_pin
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes



//{ Structures/ Enums required by ttc_gpio_types.h ***********************

typedef enum {
    E_ttc_gpio_bank_none,
    E_ttc_gpio_bank_a = ( t_u32 ) TTC_GPIO_BASE_A,
    E_ttc_gpio_bank_b = ( t_u32 ) TTC_GPIO_BASE_B,
    E_ttc_gpio_bank_c = ( t_u32 ) TTC_GPIO_BASE_C,
    E_ttc_gpio_bank_d = ( t_u32 ) TTC_GPIO_BASE_D,
    E_ttc_gpio_bank_e = ( t_u32 ) TTC_GPIO_BASE_E,
    //E_ttc_gpio_bank_f = ( t_u32 ) TTC_GPIO_BASE_F, <-- not available on stm32l1xx!
    //E_ttc_gpio_bank_g = ( t_u32 ) TTC_GPIO_BASE_G, <-- not available on stm32l1xx!
    tgb_bank_h = ( t_u32 ) TTC_GPIO_BASE_H,
    E_ttc_gpio_bank_unknown

} e_gpio_stm32l1xx_bank;

typedef enum { // pull-up/-down resistor setting (-> RM0038 p.138)
    gsp_floating   = 0b00, // no pull-up or -down resistor active
    gsp_pull_up    = 0b01,
    gsp_pull_down  = 0b10,
    gsp_unknown

} e_gpio_stm32l1xx_pull;

typedef enum { // port mode setting (-> RM0038 p.137)
    gsm_input              = 0b00, // no pull-up or -down resistor active
    gsm_gpio               = 0b01,
    gsm_alternate_function = 0b10,
    gsm_analog             = 0b11,
    E_gpio_stm32l1xx_mode_unknown

} e_gpio_stm32l1xx_mode;

typedef struct {  // stm32l1xx specific configuration data of single GPIO device
    e_gpio_stm32l1xx_bank Bank;
    e_gpio_stm32l1xx_pull Pull; // push/pull resistor
    e_gpio_stm32l1xx_mode Mode; // port mode
} __attribute__( ( __packed__ ) ) t_gpio_stm32l1xx_values;

typedef struct {  // stm32l1xx specific gpio configuration for fast interrupt access
    t_u32 Mask;  // bitmask for fast 32.-bit register access of a single pin
} __attribute__( ( __packed__ ) ) t_interrupt_stm32l1xx_config_gpio;

// t_ttc_gpio_values is required by ttc_gpio_types.h
#define t_ttc_gpio_values               t_gpio_stm32l1xx_values
#define interrupt_driver_config_gpio_t  t_interrupt_stm32l1xx_config_gpio

//} Structures/ Enums


#endif //GPIO_STM32L1XX_TYPES_H
