/** { gpio_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for gpio devices on stm32l1xx architectures.
 *  Implementation of low-level driver.
 *
 *  Created from template device_architecture.c revision 20 at 20140206 15:39:14 UTC
 *
 *  Note: See ttc_gpio.h for description of stm32l1xx independent GPIO implementation.
 *
 *  Authors: Gregor Rebel
}*/

#include "gpio_stm32l1xx.h"
#include "../basic/basic_cm3.h"
#include "../sysclock/sysclock_stm32l1xx.h"
#include "../ttc_memory.h"

//{ Global Variables ***********************************************************
const t_u32 ParallelPortConfigs[16] = {  0x00000000, // all possible configuration settings of an 8-bit wide parallel port
                                         0x11111111,
                                         0x22222222,
                                         0x33333333,
                                         0x44444444,
                                         0x55555555,
                                         0x66666666,
                                         0x77777777,
                                         0x88888888,
                                         0x99999999,
                                         0xaaaaaaaa,
                                         0xbbbbbbbb,
                                         0xcccccccc,
                                         0xdddddddd,
                                         0xeeeeeeee,
                                         0xffffffff
                                      };
//}

//{ Function definitions *******************************************************

void   gpio_stm32l1xx_init( e_gpio_stm32l1xx_pin PortPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ) {

    //when interrupt_stm32l1xx is created, these should be performed in interrupt_stm32l1xx_gpio_from_index()
    e_gpio_stm32l1xx_bank GPIOx = ( t_ttc_gpio_bank ) cm3_calc_peripheral_Word( PortPin );
    t_u8                    Pin = ( t_u8 )            cm3_calc_peripheral_BitNumber( PortPin );

    t_register_stm32l1xx_gpio* Base = ( t_register_stm32l1xx_gpio* ) GPIOx;

    //give power to the GPIO bank via the RCC clocks
    _gpio_stm32l1xx_bank_enable( GPIOx );

    e_stm32l1_gpio_mode  PortMode = 0;
    e_stm32l1_gpio_pupd  PortPUPD = 0;
    e_stm32l1_gpio_otype PortType = 0;

    //Set the mode
    switch ( Mode ) {
        case ( E_ttc_gpio_mode_analog_in ): {
            PortMode = gpc_analog;
            PortPUPD = gpio_noPULL;
            PortType = gpio_otype_push_pull;
            break;
        }
        case ( E_ttc_gpio_mode_input_floating ): {
            PortMode = gpio_input;
            PortPUPD = gpio_noPULL;
            PortType = gpio_otype_push_pull;
            break;
        }
        case ( E_ttc_gpio_mode_input_pull_down ): {
            PortMode = gpio_input;
            PortPUPD = gpio_pull_down;
            PortType = gpio_otype_push_pull;
            break;
        }
        case ( E_ttc_gpio_mode_input_pull_up ): {
            PortMode = gpio_input;
            PortPUPD = gpio_pull_up;
            PortType = gpio_otype_push_pull;
            break;
        }
        case ( E_ttc_gpio_mode_output_open_drain ): {
            PortMode = gpio_general_purpose_output;
            PortPUPD = gpio_noPULL;
            PortType = gpio_otype_open_drain;
            break;
        }
        case ( E_ttc_gpio_mode_output_open_drain_pull_up_resistor ): {
            PortMode = gpio_general_purpose_output;
            PortPUPD = gpio_pull_up;
            PortType = gpio_otype_open_drain;
            break;
        }
        case ( E_ttc_gpio_mode_output_open_drain_pull_down_resistor ): {
            PortMode = gpio_general_purpose_output;
            PortPUPD = gpio_pull_down;
            PortType = gpio_otype_open_drain;
            break;
        }
        case ( E_ttc_gpio_mode_output_push_pull ): {
            PortMode = gpio_general_purpose_output;
            PortPUPD = gpio_noPULL;
            PortType = gpio_otype_push_pull;
            break;
        }
        case ( E_ttc_gpio_mode_output_push_pull_pull_up_resistor ): {
            PortMode = gpio_general_purpose_output;
            PortPUPD = gpio_pull_up;
            PortType = gpio_otype_push_pull;
            break;
        }
        case ( E_ttc_gpio_mode_output_push_pull_pull_down_resistor ): {
            PortMode = gpio_general_purpose_output;
            PortPUPD = gpio_pull_down;
            PortType = gpio_otype_push_pull;
            break;
        }
        case ( E_ttc_gpio_mode_alternate_function_special_sclk ):
        case ( E_ttc_gpio_mode_alternate_function_push_pull ): {
            PortMode = gpc_alternate_function;
            PortPUPD = gpio_noPULL;
            PortType = gpio_otype_push_pull;
            break;
        }
        case ( E_ttc_gpio_mode_alternate_function_push_pull_pull_up_resistor ): {
            PortMode = gpc_alternate_function;
            PortPUPD = gpio_pull_up;
            PortType = gpio_otype_push_pull;
            break;
        }
        case ( E_ttc_gpio_mode_alternate_function_push_pull_pull_down_resistor ): {
            PortMode = gpc_alternate_function;
            PortPUPD = gpio_pull_down;
            PortType = gpio_otype_push_pull;
            break;
        }
        case ( E_ttc_gpio_mode_alternate_function_open_drain ): {
            PortMode = gpc_alternate_function;
            PortPUPD = gpio_noPULL;
            PortType = gpio_otype_open_drain;
            break;
        }
        case ( E_ttc_gpio_mode_alternate_function_open_drain_pull_up_resistor ): {
            PortMode = gpc_alternate_function;
            PortPUPD = gpio_pull_up;
            PortType = gpio_otype_open_drain;
            break;
        }
        case ( E_ttc_gpio_mode_alternate_function_open_drain_pull_down_resistor ): {
            PortMode = gpc_alternate_function;
            PortPUPD = gpio_pull_down;
            PortType = gpio_otype_open_drain;
            break;
        }
        case ( E_ttc_gpio_mode_unknown ):
            break;
        default:
            break; //have to put this here or will get a warning for unhandled cases.
    }

    e_stm32l1_gpio_speed PortSpeed;
    if ( Speed <= E_ttc_gpio_speed_400khz )
    { PortSpeed = gpio_speed_400kHz; }
    else {
        if ( Speed <= E_ttc_gpio_speed_2mhz )
        { PortSpeed = gpio_speed_2MHz; }
        else {
            if ( Speed <= E_ttc_gpio_speed_10mhz )
            { PortSpeed = gpio_speed_10MHz; }
            else
            { PortSpeed = gpio_speed_40MHz; }
        }
    }

    // write data into GPIO registers
    Base->PUPD.All  = ( Base->PUPD.All  & ( ~( 0b11 << ( Pin * 2 ) ) ) )  | ( ( t_u32 )( PortPUPD )  << ( Pin * 2 ) );
    Base->TYPE.All  = ( Base->TYPE.All  & ( ~( 0b01 << ( Pin * 1 ) ) ) )  | ( ( t_u32 )( PortType )  << ( Pin * 1 ) );
    Base->MODER.All = ( Base->MODER.All & ( ~( 0b11 << ( Pin * 2 ) ) ) )  | ( ( t_u32 )( PortMode )  << ( Pin * 2 ) );
    Base->SPEED.All = ( Base->SPEED.All & ( ~( 0b11 << ( Pin * 2 ) ) ) )  | ( ( t_u32 )( PortSpeed ) << ( Pin * 2 ) );
}
void   gpio_stm32l1xx_deinit( e_gpio_stm32l1xx_pin PortPin ) {

    ( void ) PortPin;
    e_gpio_stm32l1xx_bank GPIOx  = ( t_ttc_gpio_bank ) cm3_calc_peripheral_Word( PortPin );

    if ( GPIOx == ( e_gpio_stm32l1xx_bank )GPIOA ) {
        register_stm32l1xx_RCC.AHBRSTR.Bits.GPIOARST = 1;
        register_stm32l1xx_RCC.AHBRSTR.Bits.GPIOARST = 0;
    }
    else if ( GPIOx == ( e_gpio_stm32l1xx_bank )GPIOB ) {
        register_stm32l1xx_RCC.AHBRSTR.Bits.GPIOBRST = 1;
        register_stm32l1xx_RCC.AHBRSTR.Bits.GPIOBRST = 0;
    }
    else if ( GPIOx == ( e_gpio_stm32l1xx_bank )GPIOC ) {
        register_stm32l1xx_RCC.AHBRSTR.Bits.GPIOCRST = 1;
        register_stm32l1xx_RCC.AHBRSTR.Bits.GPIOCRST = 0;
    }
    else if ( GPIOx == ( e_gpio_stm32l1xx_bank )GPIOD ) {
        register_stm32l1xx_RCC.AHBRSTR.Bits.GPIODRST = 1;
        register_stm32l1xx_RCC.AHBRSTR.Bits.GPIODRST = 0;
    }
    else if ( GPIOx == ( e_gpio_stm32l1xx_bank )GPIOE ) {
        register_stm32l1xx_RCC.AHBRSTR.Bits.GPIOERST = 1;
        register_stm32l1xx_RCC.AHBRSTR.Bits.GPIOERST = 0;
    }
    else {
        if ( GPIOx == ( e_gpio_stm32l1xx_bank )GPIOH ) {
            register_stm32l1xx_RCC.AHBRSTR.Bits.GPIOHRST = 1;
            register_stm32l1xx_RCC.AHBRSTR.Bits.GPIOHRST = 0;
        }
    }
}
void   gpio_stm32l1xx_get_configuration( e_gpio_stm32l1xx_pin PortPin, t_ttc_gpio_config* Configuration ) {
    Assert_GPIO_Writable( Configuration, ttc_assert_origin_auto );  // pointers must not be NULL

    t_ttc_gpio_bank GPIOx = ( t_ttc_gpio_bank ) cm3_calc_peripheral_Word( PortPin );
    t_u8            Pin   = ( t_u8 )            cm3_calc_peripheral_BitNumber( PortPin );

    t_register_stm32l1xx_gpio* Base = ( t_register_stm32l1xx_gpio* ) GPIOx;

    Configuration->PhysicalIndex  = PortPin;
    Configuration->Architecture   = E_ttc_gpio_architecture_stm32l1xx;
    Configuration->Register       = Base;
    Configuration->Values.Bank      = ( e_gpio_stm32l1xx_bank ) Base;

    t_u8 Pinx2 = Pin * 2;
    t_u8 Pinx4 = Pin * 4;

    // reading according to RM0038 p. 137-138
    Configuration->Values.Mode = ( Base->MODER.All & ( 0b11 << Pinx2 ) ) >> Pinx2;
    t_u8 PortType  = ( Base->TYPE.All  & ( 1 << Pin ) ) ? 1 : 0;
    t_u8 PortSpeed = ( Base->SPEED.All & ( 0b11 << Pinx2 ) ) >> Pinx2;

    // push-/ pull-resistor
    Configuration->Values.Pull  = ( Base->PUPD.All  & ( 0b11 << Pinx2 ) ) >> Pinx2;

    // alternate function
    Configuration->AlternateFunction = ( Pin < 8 ) ? ( Base->AFRL.All  & ( 0b1111 << Pinx4 ) ) >> Pinx4 :
                                       ( Base->AFRH.All  & ( 0b1111 << Pinx4 ) ) >> Pinx4 ;

    switch ( Configuration->Values.Mode ) {
        case 0b00: { // input
            switch ( Configuration->Values.Pull ) {
                case 0b00: Configuration->Mode = E_ttc_gpio_mode_input_floating;  break;
                case 0b01: Configuration->Mode = E_ttc_gpio_mode_input_pull_up;   break;
                case 0b10: Configuration->Mode = E_ttc_gpio_mode_input_pull_down; break;
                default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid register value!
            }
            break;
        }
        case 0b01: { // GPIO
            switch ( Configuration->Values.Pull ) {
                case 0b00: Configuration->Mode = E_ttc_gpio_mode_output_push_pull;                     break;
                case 0b01: Configuration->Mode = E_ttc_gpio_mode_output_open_drain_pull_up_resistor;   break;
                case 0b10: Configuration->Mode = E_ttc_gpio_mode_output_open_drain_pull_down_resistor; break;
                default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid register value!
            }
            break;
        }
        case 0b10: { // alternate function
            if ( PortType ) { // open drain
                switch ( Configuration->Values.Pull ) {
                    case 0b00: Configuration->Mode = E_ttc_gpio_mode_alternate_function_push_pull;                     break;
                    case 0b01: Configuration->Mode = E_ttc_gpio_mode_alternate_function_open_drain_pull_up_resistor;   break;
                    case 0b10: Configuration->Mode = E_ttc_gpio_mode_alternate_function_open_drain_pull_down_resistor; break;
                    default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid register value!
                }
            }
            else { // push/pull
                switch ( Configuration->Values.Pull ) {
                    case 0b00: Configuration->Mode = E_ttc_gpio_mode_alternate_function_push_pull;                    break;
                    case 0b01: Configuration->Mode = E_ttc_gpio_mode_alternate_function_push_pull_pull_up_resistor;   break;
                    case 0b10: Configuration->Mode = E_ttc_gpio_mode_alternate_function_push_pull_pull_down_resistor; break;
                    default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid register value!
                }
            }
            break;
        }
        case 0b11: { // analog
            Configuration->Mode = E_ttc_gpio_mode_analog_in;
            break;
        }
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid register value!
    }

    switch ( PortSpeed ) {
        case 0b00: Configuration->Speed = E_ttc_gpio_speed_400khz; break;
        case 0b01: Configuration->Speed = E_ttc_gpio_speed_2mhz;   break;
        case 0b10: Configuration->Speed = E_ttc_gpio_speed_10mhz;  break;
        case 0b11: Configuration->Speed = E_ttc_gpio_speed_40mhz;  break;
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid register value!
    }
}
void   gpio_stm32l1xx_parallel08_init( t_ttc_gpio_bank GPIOx, t_u8 FirstPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ) {

    e_gpio_stm32l1xx_pin PortPin;

    switch ( GPIOx ) {
        case ( E_ttc_gpio_bank_a ): {
            switch ( FirstPin ) {
                case ( 0 ): {PortPin = E_ttc_gpio_pin_a0; break;}
                case ( 1 ): {PortPin = E_ttc_gpio_pin_a1; break;}
                case ( 2 ): {PortPin = E_ttc_gpio_pin_a2; break;}
                case ( 3 ): {PortPin = E_ttc_gpio_pin_a3; break;}
                case ( 4 ): {PortPin = E_ttc_gpio_pin_a4; break;}
                case ( 5 ): {PortPin = E_ttc_gpio_pin_a5; break;}
                case ( 6 ): {PortPin = E_ttc_gpio_pin_a6; break;}
                case ( 7 ): {PortPin = E_ttc_gpio_pin_a7; break;}
                case ( 8 ): {PortPin = E_ttc_gpio_pin_a8; break;}
                default:
                    break;
            }
        }
        case ( E_ttc_gpio_bank_b ): {
            switch ( FirstPin ) {
                case ( 0 ): {PortPin = E_ttc_gpio_pin_b0; break;}
                case ( 1 ): {PortPin = E_ttc_gpio_pin_b1; break;}
                case ( 2 ): {PortPin = E_ttc_gpio_pin_b2; break;}
                case ( 3 ): {PortPin = E_ttc_gpio_pin_b3; break;}
                case ( 4 ): {PortPin = E_ttc_gpio_pin_b4; break;}
                case ( 5 ): {PortPin = E_ttc_gpio_pin_b5; break;}
                case ( 6 ): {PortPin = E_ttc_gpio_pin_b6; break;}
                case ( 7 ): {PortPin = E_ttc_gpio_pin_b7; break;}
                case ( 8 ): {PortPin = E_ttc_gpio_pin_b8; break;}
                default:
                    break;
            }
        }
        case ( E_ttc_gpio_bank_c ): {
            switch ( FirstPin ) {
                case ( 0 ): {PortPin = E_ttc_gpio_pin_c0; break;}
                case ( 1 ): {PortPin = E_ttc_gpio_pin_c1; break;}
                case ( 2 ): {PortPin = E_ttc_gpio_pin_c2; break;}
                case ( 3 ): {PortPin = E_ttc_gpio_pin_c3; break;}
                case ( 4 ): {PortPin = E_ttc_gpio_pin_c4; break;}
                case ( 5 ): {PortPin = E_ttc_gpio_pin_c5; break;}
                case ( 6 ): {PortPin = E_ttc_gpio_pin_c6; break;}
                case ( 7 ): {PortPin = E_ttc_gpio_pin_c7; break;}
                case ( 8 ): {PortPin = E_ttc_gpio_pin_c8; break;}
                default:
                    break;
            }
        }
        case ( E_ttc_gpio_bank_d ): {
            switch ( FirstPin ) {
                case ( 0 ): {PortPin = E_ttc_gpio_pin_d0; break;}
                case ( 1 ): {PortPin = E_ttc_gpio_pin_d1; break;}
                case ( 2 ): {PortPin = E_ttc_gpio_pin_d2; break;}
                case ( 3 ): {PortPin = E_ttc_gpio_pin_d3; break;}
                case ( 4 ): {PortPin = E_ttc_gpio_pin_d4; break;}
                case ( 5 ): {PortPin = E_ttc_gpio_pin_d5; break;}
                case ( 6 ): {PortPin = E_ttc_gpio_pin_d6; break;}
                case ( 7 ): {PortPin = E_ttc_gpio_pin_d7; break;}
                case ( 8 ): {PortPin = E_ttc_gpio_pin_d8; break;}
                default:
                    break;
            }
        }
        case ( E_ttc_gpio_bank_e ): {
            switch ( FirstPin ) {
                case ( 0 ): {PortPin = E_ttc_gpio_pin_e0; break;}
                case ( 1 ): {PortPin = E_ttc_gpio_pin_e1; break;}
                case ( 2 ): {PortPin = E_ttc_gpio_pin_e2; break;}
                case ( 3 ): {PortPin = E_ttc_gpio_pin_e3; break;}
                case ( 4 ): {PortPin = E_ttc_gpio_pin_e4; break;}
                case ( 5 ): {PortPin = E_ttc_gpio_pin_e5; break;}
                case ( 6 ): {PortPin = E_ttc_gpio_pin_e6; break;}
                case ( 7 ): {PortPin = E_ttc_gpio_pin_e7; break;}
                case ( 8 ): {PortPin = E_ttc_gpio_pin_e8; break;}
                default:
                    break;
            }
        }
        case ( tgb_bank_h ): {
            switch ( FirstPin ) {
                case ( 0 ): {PortPin = E_ttc_gpio_pin_h0; break;}
                case ( 1 ): {PortPin = E_ttc_gpio_pin_h1; break;}
                case ( 2 ): {PortPin = E_ttc_gpio_pin_h2; break;}
                case ( 3 ): {PortPin = E_ttc_gpio_pin_h3; break;}
                case ( 4 ): {PortPin = E_ttc_gpio_pin_h4; break;}
                case ( 5 ): {PortPin = E_ttc_gpio_pin_h5; break;}
                case ( 6 ): {PortPin = E_ttc_gpio_pin_h6; break;}
                case ( 7 ): {PortPin = E_ttc_gpio_pin_h7; break;}
                case ( 8 ): {PortPin = E_ttc_gpio_pin_h8; break;}
                default:
                    break;
            }
        }
        default:
            break;
    }

    //initialize all bank pins
    for ( int i = 0; i < 8; i++ ) {
        gpio_stm32l1xx_init( PortPin, Mode, Speed );
        PortPin = PortPin + 4;
    }
}
void   gpio_stm32l1xx_parallel16_init( t_ttc_gpio_bank GPIOx, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ) {

    e_gpio_stm32l1xx_pin PortPin;

    switch ( GPIOx ) {
        case ( E_ttc_gpio_bank_a ): { PortPin = E_ttc_gpio_pin_a0; break;}
        case ( E_ttc_gpio_bank_b ): { PortPin = E_ttc_gpio_pin_b0; break;}
        case ( E_ttc_gpio_bank_c ): { PortPin = E_ttc_gpio_pin_c0; break;}
        case ( E_ttc_gpio_bank_d ): { PortPin = E_ttc_gpio_pin_d0; break;}
        case ( E_ttc_gpio_bank_e ): { PortPin = E_ttc_gpio_pin_e0; break;}
        case ( tgb_bank_h ): { PortPin = E_ttc_gpio_pin_h0; break;}
        default:
            PortPin = 0;
            break;
    }

    //initialize all bank pins
    for ( int i = 0; i < 16; i++ ) {
        gpio_stm32l1xx_init( PortPin, Mode, Speed );
        PortPin = PortPin + 4;
    }
}
void   gpio_stm32l1xx_prepare() {

}
t_base gpio_stm32l1xx_create_index( t_ttc_gpio_bank GPIOx, t_u8 Pin ) {
    return ( t_base ) cm3_calc_peripheral_BitBandAddress( GPIOx, Pin );
}
void   gpio_stm32l1xx_from_index( t_base PhysicalIndex, t_ttc_gpio_bank* GPIOx, t_u8* Pin ) {
    Assert_GPIO_Writable( GPIOx, ttc_assert_origin_auto );  // pointers must not be NULL
    Assert_GPIO_Writable( Pin, ttc_assert_origin_auto );  // pointers must not be NULL

    *GPIOx = ( t_ttc_gpio_bank ) cm3_calc_peripheral_Word( PhysicalIndex );
    *Pin   = ( t_u8 )            cm3_calc_peripheral_BitNumber( PhysicalIndex );

    Assert_GPIO( *Pin < 32, ttc_assert_origin_auto );  // invalid pin number!
}
void   gpio_stm32l1xx_alternate_function( e_gpio_stm32l1xx_pin PortPin, e_ttc_gpio_alternate_function AlternateFunction ) {

    e_gpio_stm32l1xx_bank GPIOx;
    t_u8                    Pin;

    gpio_stm32l1xx_from_index( PortPin, &GPIOx, &Pin );

    if ( 0 ) { // DEPRECATED
        //when interrupt_stm32l1xx is created, these should be performed in interrupt_stm32l1xx_gpio_from_index()
        GPIOx = ( t_ttc_gpio_bank ) cm3_calc_peripheral_Word( PortPin );
        Pin   = ( t_u8 )            cm3_calc_peripheral_BitNumber( PortPin );
    }

    //give power to the GPIO bank via the RCC clocks
    _gpio_stm32l1xx_bank_enable( GPIOx );
    t_register_stm32l1xx_gpio* Base = ( t_register_stm32l1xx_gpio* ) GPIOx;

    t_u32 temp = 0x00;
    //X t_u32 temp_2 = 0x00;

    t_register_stm32l1xx_gpiox_afrl    AFRL;
    t_register_stm32l1xx_gpiox_afrh    AFRH;

    temp = ( ( t_u32 )( AlternateFunction ) << ( ( t_u32 )( ( t_u32 )Pin & ( t_u32 )0x07 ) * 4 ) ) ;
    if ( Pin < 8 ) {
        AFRL = Base->AFRL;
        AFRL.All &= ~( ( t_u32 )0xF << ( ( t_u32 )( ( t_u32 )Pin & ( t_u32 )0x07 ) * 4 ) ) ;
        AFRL.All  = AFRL.All | temp;
        Base->AFRL = AFRL;
    }
    else {
        AFRH = Base->AFRH;
        AFRH.All &= ~( ( t_u32 )0xF << ( ( t_u32 )( ( t_u32 )Pin & ( t_u32 )0x07 ) * 4 ) ) ;
        AFRH.All  = AFRH.All | temp;
        Base->AFRH = AFRH;
    }
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private functions (ideally) ************************************************

void _gpio_stm32l1xx_bank_enable( t_ttc_gpio_bank GPIOx ) {
    switch ( GPIOx ) { // enable peripheral clock to this port
        case E_ttc_gpio_bank_a: register_stm32l1xx_RCC.AHBENR.Bits.GPIOA_EN = 1; break;
        case E_ttc_gpio_bank_b: register_stm32l1xx_RCC.AHBENR.Bits.GPIOB_EN = 1; break;
        case E_ttc_gpio_bank_c: register_stm32l1xx_RCC.AHBENR.Bits.GPIOC_EN = 1; break;
        case E_ttc_gpio_bank_d: register_stm32l1xx_RCC.AHBENR.Bits.GPIOD_EN = 1; break;
        case E_ttc_gpio_bank_e: register_stm32l1xx_RCC.AHBENR.Bits.GPIOE_EN = 1; break;
#ifdef GPIOH
        case tgb_bank_h: register_stm32l1xx_RCC.AHBENR.Bits.GPIOH_EN = 1; break;
#endif
        default: Assert_GPIO( 0, ttc_assert_origin_auto ); break;  // ERROR: unknown GPIO given!
    }
}
t_u8 _gpio_stm32l1xx_compile_port_config( e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed ) {
    //This implementation is for STM32F.  Needs to be ported to STM32L
    /*
    t_u8 PortConfig = 0;

    switch (Mode) {
    case E_ttc_gpio_mode_analog_in:
        PortConfig = (gpc_input_analog << 2)       | gpm_input;
        break;
    case E_ttc_gpio_mode_input_floating:
        PortConfig = (gpc_input_floating << 2)     | gpm_input;
        break;
    case E_ttc_gpio_mode_input_pull_down:
        PortConfig = (gpc_input_pull_up_down << 2) | gpm_input;
        break;
    case E_ttc_gpio_mode_input_pull_up:
        PortConfig = (gpc_input_pull_up_down << 2) | gpm_input;
        break;
    case E_ttc_gpio_mode_output_open_drain:
        if (Speed < E_ttc_gpio_speed_2mhz)       { PortConfig = (gpc_output_open_drain << 2) | gpm_output_2mhz; }
        else if (Speed > E_ttc_gpio_speed_10mhz) { PortConfig = (gpc_output_open_drain << 2) | gpm_output_50mhz; }
        else                        { PortConfig = (gpc_output_open_drain << 2) | gpm_output_10mhz; }
        break;
    case E_ttc_gpio_mode_output_push_pull:
        if (Speed < E_ttc_gpio_speed_2mhz)       { PortConfig = (gpc_output_push_pull << 2) | gpm_output_2mhz; }
        else if (Speed > E_ttc_gpio_speed_10mhz) { PortConfig = (gpc_output_push_pull << 2) | gpm_output_50mhz; }
        else                        { PortConfig = (gpc_output_push_pull << 2) | gpm_output_10mhz; }
        break;
    case E_ttc_gpio_mode_alternate_function_push_pull:
        if (Speed < E_ttc_gpio_speed_2mhz)       { PortConfig = (gpc_output_alternate_push_pull << 2) | gpm_output_2mhz; }
        else if (Speed > E_ttc_gpio_speed_10mhz) { PortConfig = (gpc_output_alternate_push_pull << 2) | gpm_output_50mhz; }
        else                        { PortConfig = (gpc_output_alternate_push_pull << 2) | gpm_output_10mhz; }
        break;
    case E_ttc_gpio_mode_alternate_function_open_drain:
        if (Speed < E_ttc_gpio_speed_2mhz)       { PortConfig = (gpc_output_alternate_open_drain << 2) | gpm_output_2mhz; }
        else if (Speed > E_ttc_gpio_speed_10mhz) { PortConfig = (gpc_output_alternate_open_drain << 2) | gpm_output_50mhz; }
        else                        { PortConfig = (gpc_output_alternate_open_drain << 2) | gpm_output_10mhz; }
        break;
    default:
        ttc_assert_halt_origin(ttc_assert_origin_auto);
    }
    return PortConfig;

    */
    ttc_assert_halt_origin( ttc_assert_origin_auto ); // ToDo: is this function still required?
    return 0;
}
void _gpio_stm32l1xx_bank_disable( t_ttc_gpio_bank GPIOx ) {
    switch ( GPIOx ) { // enable peripheral clock to this port
        case E_ttc_gpio_bank_a: register_stm32l1xx_RCC.AHBENR.Bits.GPIOA_EN = 0; break;
        case E_ttc_gpio_bank_b: register_stm32l1xx_RCC.AHBENR.Bits.GPIOB_EN = 0; break;
        case E_ttc_gpio_bank_c: register_stm32l1xx_RCC.AHBENR.Bits.GPIOC_EN = 0; break;
        case E_ttc_gpio_bank_d: register_stm32l1xx_RCC.AHBENR.Bits.GPIOD_EN = 0; break;
        case E_ttc_gpio_bank_e: register_stm32l1xx_RCC.AHBENR.Bits.GPIOE_EN = 0; break;
#ifdef GPIOH
        case tgb_bank_h: register_stm32l1xx_RCC.AHBENR.Bits.GPIOH_EN = 0; break;
#endif
        default: Assert_GPIO( 0, ttc_assert_origin_auto ); break;  // ERROR: unknown GPIO given!
    }

}

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//} private functions
