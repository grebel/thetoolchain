#ifndef ARCHITECTURE_GPIO_H
#define ARCHITECTURE_GPIO_H

/** { gpio_stm32f1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for gpio devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by high-level gpio and application.
 *
 *  Created from template device_architecture.h revision 21 at 20140205 06:34:47 UTC
 *
 *  Note: See ttc_gpio.h for description of stm32f1xx independent GPIO implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_GPIO_STM32F1XX
//
// Implementation status of low-level driver support for gpio devices on stm32f1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_GPIO_STM32F1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_GPIO_STM32F1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_GPIO_STM32F1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_GPIO_STM32F1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "../register/register_stm32f1xx.h"
#include "gpio_stm32f1xx_types.h"
#include "gpio_common.h"
#include "../ttc_gpio_types.h"
#include "../basic/basic_cm3.h"
//X#include "additionals/270_CPAL/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h"
//? #include "stm32f10x_rcc.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_gpio_interface.h
#define ttc_driver_gpio_init(PortPin, Type, Speed)                            gpio_stm32f1xx_init(PortPin, Type, Speed)
#define ttc_driver_gpio_get(PortPin)                                          gpio_stm32f1xx_get(PortPin)
#define ttc_driver_gpio_clr(PortPin)                                          gpio_stm32f1xx_clr(PortPin)
#define ttc_driver_gpio_set(PortPin)                                          gpio_stm32f1xx_set(PortPin)
#define ttc_driver_gpio_parallel08_init(GPIOx, FirstPin, Type, Speed)         gpio_stm32f1xx_parallel08_init(GPIOx, FirstPin, Type, Speed)
#define ttc_driver_gpio_parallel16_init(GPIOx, Type, Speed)                   gpio_stm32f1xx_parallel16_init(GPIOx, Type, Speed)
#define ttc_driver_gpio_deinit(PortPin)                                       gpio_stm32f1xx_deinit(PortPin)
#define ttc_driver_gpio_get_configuration(PortPin, Configuration)             gpio_stm32f1xx_get_configuration(PortPin, Configuration)
#define ttc_driver_gpio_prepare()                                             gpio_stm32f1xx_prepare()
#define ttc_driver_gpio_create_index(GPIOx, Pin)                              gpio_stm32f1xx_create_index(GPIOx, Pin)
#define ttc_driver_gpio_from_index(PhysicalIndex, GPIOx, Pin)                 gpio_stm32f1xx_from_index(PhysicalIndex, GPIOx, Pin)
#define ttc_driver_gpio_alternate_function(PortPin, AlternateFunction)        gpio_stm32f1xx_alternate_function(PortPin, AlternateFunction)
#define ttc_driver_gpio_put(PortPin, Value)                                   gpio_stm32f1xx_put(PortPin, Value)

/* Super fast shifted 8 bit parallel port access
 *
 * The use of macros allows to reduce instruction count for each access to a minimum.
 * As each ttc_driver_gpio_parallel08_*() is completely static, the compiler can optimize a lot.
 * If bits are not shifted, no shift operations will be used.
 */
#define ttc_driver_gpio_parallel08_1_get()                                    ( (t_u8) ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_1_BANK)->IDR >> TTC_GPIO_PARALLEL08_1_FIRSTPIN) )
#define ttc_driver_gpio_parallel08_1_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_1_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_1_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_1_FIRSTPIN + 16) )
#define ttc_driver_gpio_parallel08_2_get()                                    ( (t_u8) ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_2_BANK)->IDR >> TTC_GPIO_PARALLEL08_2_FIRSTPIN) )
#define ttc_driver_gpio_parallel08_2_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_2_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_2_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_2_FIRSTPIN + 16) )
#define ttc_driver_gpio_parallel08_3_get()                                    ( (t_u8) ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_3_BANK)->IDR >> TTC_GPIO_PARALLEL08_3_FIRSTPIN) )
#define ttc_driver_gpio_parallel08_3_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_3_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_3_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_3_FIRSTPIN + 16) )
#define ttc_driver_gpio_parallel08_4_get()                                    ( (t_u8) ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_4_BANK)->IDR >> TTC_GPIO_PARALLEL08_4_FIRSTPIN) )
#define ttc_driver_gpio_parallel08_4_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_4_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_4_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_4_FIRSTPIN + 16) )
#define ttc_driver_gpio_parallel08_5_get()                                    ( (t_u8) ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_5_BANK)->IDR >> TTC_GPIO_PARALLEL08_5_FIRSTPIN) )
#define ttc_driver_gpio_parallel08_5_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_5_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_5_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_5_FIRSTPIN + 16) )
#define ttc_driver_gpio_parallel08_6_get()                                    ( (t_u8) ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_6_BANK)->IDR >> TTC_GPIO_PARALLEL08_6_FIRSTPIN) )
#define ttc_driver_gpio_parallel08_6_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_6_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_6_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_6_FIRSTPIN + 16) )
#define ttc_driver_gpio_parallel08_7_get()                                    ( (t_u8) ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_7_BANK)->IDR >> TTC_GPIO_PARALLEL08_7_FIRSTPIN) )
#define ttc_driver_gpio_parallel08_7_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_7_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_7_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_7_FIRSTPIN + 16) )
#define ttc_driver_gpio_parallel08_8_get()                                    ( (t_u8) ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_8_BANK)->IDR >> TTC_GPIO_PARALLEL08_8_FIRSTPIN) )
#define ttc_driver_gpio_parallel08_8_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_8_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_8_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_8_FIRSTPIN + 16) )
#define ttc_driver_gpio_parallel08_9_get()                                    ( (t_u8) ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_9_BANK)->IDR >> TTC_GPIO_PARALLEL08_9_FIRSTPIN) )
#define ttc_driver_gpio_parallel08_9_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_9_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_9_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_9_FIRSTPIN + 16) )
#define ttc_driver_gpio_parallel08_10_get()                                   ( (t_u8) ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_10_BANK)->IDR >> TTC_GPIO_PARALLEL08_10_FIRSTPIN) )
#define ttc_driver_gpio_parallel08_10_put(Value)                              ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL08_10_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_10_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_10_FIRSTPIN + 16) )

#define ttc_driver_gpio_parallel16_1_get()                                    ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_1_BANK)->IDR )
#define ttc_driver_gpio_parallel16_2_get()                                    ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_2_BANK)->IDR )
#define ttc_driver_gpio_parallel16_3_get()                                    ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_3_BANK)->IDR )
#define ttc_driver_gpio_parallel16_4_get()                                    ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_4_BANK)->IDR )
#define ttc_driver_gpio_parallel16_5_get()                                    ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_5_BANK)->IDR )
#define ttc_driver_gpio_parallel16_6_get()                                    ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_6_BANK)->IDR )
#define ttc_driver_gpio_parallel16_7_get()                                    ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_7_BANK)->IDR )
#define ttc_driver_gpio_parallel16_8_get()                                    ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_8_BANK)->IDR )
#define ttc_driver_gpio_parallel16_9_get()                                    ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_9_BANK)->IDR )
#define ttc_driver_gpio_parallel16_10_get()                                   ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_10_BANK)->IDR )
#define ttc_driver_gpio_parallel16_1_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_1_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_2_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_2_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_3_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_3_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_4_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_4_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_5_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_5_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_6_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_6_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_7_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_7_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_8_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_8_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_9_put(Value)                               ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_9_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_10_put(Value)                              ( ((t_register_stm32f1xx_gpio_32*) TTC_GPIO_PARALLEL16_10_BANK)->ODR ) = (t_u16) (Value)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_gpio.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_gpio.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** set single output port bit to logical 0 (usually high level)
 *
 * Measured runtime of set/clr functions on stm32f107 @72MHz was 410ns (-Og)
 * Runtime of set/clr macros on STM32f107 @72MHz is 105 ns (-Og) and 83ns (-O9)
 * (Measured with oscilloscope for 50 set()/clr() pairs on same pin)
 *
 * @param PortPin bit definition
 */
void gpio_stm32f1xx_clr( volatile e_gpio_stm32f1xx_pin PortPin );
#define gpio_stm32f1xx_clr(Pin) *(  (volatile t_u8*) ( ((t_u32) Pin) + TTC_GPIO_OFFSET_BRR  )  ) = 1

/** set single output port bit to logical 1 (usually high level)
 *
 * Measured runtime of set/clr functions on stm32f107 @72MHz was 410ns (-Og)
 * Runtime of set/clr macros on STM32f107 @72MHz is 105 ns (-Og) and 83ns (-O9)
 * (Measured with oscilloscope for 50 set()/clr() pairs on same pin)
 *
 * @param PortPin bit definition
 */
void gpio_stm32f1xx_set( volatile e_gpio_stm32f1xx_pin PortPin );
#define gpio_stm32f1xx_set(Pin) *(  (volatile t_u8*) ( ((t_u32) Pin) + TTC_GPIO_OFFSET_BSRR )  ) = 1

/** Set single output port bit to either logical high or Low.
 *
 * @param PortPin bit definition
 * @param Value   new logical state of output pin (==true: set to high; ==false: set to low)
 */
void gpio_stm32f1xx_put( e_ttc_gpio_pin PortPin, BOOL Value );
#define gpio_stm32f1xx_put(Pin, Value) *(  (volatile t_u8*) ( ((t_u32) Pin) + TTC_GPIO_OFFSET_ODR )  ) = Value

/** Read 8-bit value from parallel port
 *
 * @return 8-bit value being read from gpio pins
 */
t_u8 gpio_stm32f1xx_parallel08_1_get();

/** write 8-bit value to parallel port
 *
 * @param Value   8-bit data to write to gpio pins
 */
void gpio_stm32f1xx_parallel08_1_put( t_u8 Value );

/** get value of single input port bit (voltage level at input pin)
 *
 * Measured runtime of set/clr functions on stm32f107 @72MHz was 410ns (-Og)
 * Runtime of set/clr macros on STM32f107 @72MHz is 105 ns (-Og) and 83ns (-O9)
 * (Measured with oscilloscope for 50 set()/clr() pairs on same pin)
 *
 * @param  PortPin bit definition
 * @return true = high level detected on input pin; low level otherwise
 */
BOOL gpio_stm32f1xx_get( volatile e_gpio_stm32f1xx_pin PortPin );
#define gpio_stm32f1xx_get(Pin) *(  (volatile t_u8*) ( ((t_u32) Pin) + TTC_GPIO_OFFSET_IDR  )  )

/** shutdown single GPIO unit device
 * @param PortPin bit definition
 */
void gpio_stm32f1xx_deinit( e_gpio_stm32f1xx_pin PortPin );


/** Prepares gpio driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void gpio_stm32f1xx_prepare();


/** creates numeric representing given GPIO-bank and pin number
 *
 * @param GPIOx  GPIO bank to use (-> ttc_gpio_types.h:t_ttc_gpio_bank)
 *               Note: GPIOx, Pin can be given as one argument by use of Pin_Pxn macro (TTC_GPIO_BANK_A, 7) = Pin_PA7
 * @param Pin    0..15
 * @return       32-bit value representing given GPIO-pin in a scalar datatype
 */
t_base gpio_stm32f1xx_create_index( t_ttc_gpio_bank GPIOx, t_u8 Pin );

/** restores GPIO-bank and pin number from 8-bit value
 *
 * This is a compatibility macro required by ttc_interrupt
 *
 * @param PhysicalIndex  32-bit value representing GPIO-bank + pin
 * @param GPIOx          loaded with GPIO bank (-> ttc_gpio_types.h:t_ttc_gpio_bank)
 * @param Pin            loaded with pin number
 */
//? void interrupt_stm32f1xx_gpio_from_index(t_base PhysicalIndex, t_ttc_gpio_bank* GPIOx, t_u8* Pin);
//? #define interrupt_stm32f1xx_gpio_from_index(PhysicalIndex, GPIOx, Pin) gpio_stm32f1xx_from_index(PhysicalIndex, GPIOx, Pin)

/** initializes single GPIO pin for operation
 * @param PortPin port bit definition
 * @param Type   operation mode to use
 * @param Speed  GPIOs support different driver speeds
 */
void gpio_stm32f1xx_init( e_gpio_stm32f1xx_pin PortPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );


/** initializes single GPIO unit for operation
 * @param PortPin         port bit definition
 * @param Configuration  loaded with current configuration of given PortPin
 */
void gpio_stm32f1xx_get_configuration( e_gpio_stm32f1xx_pin PortPin, t_ttc_gpio_config* Configuration );


/** { restores GPIO-bank and pin number from index value
 *
 * @param PhysicalIndex  32-bit value representing GPIO-bank + pin
 * @param GPIOx          loaded with GPIO bank (-> ttc_gpio_types.h:t_ttc_gpio_bank)
 * @param Pin            loaded with pin number
}*/
void gpio_stm32f1xx_from_index( t_base PhysicalIndex, t_ttc_gpio_bank* GPIOx, t_u8* Pin );


/** Changes mapping of specified pin to alternate function.
 *
 * Note: Configure PortPin as alternate function pin via ttc_gpio_init() before or after calling this function!
 *
 * @param PortPin            Port bit definition
 * @param AlternateFunction  Selects alternate function to use on given PortPin
 */
void gpio_stm32f1xx_alternate_function( e_ttc_gpio_pin PortPin, e_ttc_gpio_alternate_function AlternateFunction );


//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)


// Parallel Ports ********************************************************

/** Initialize all pins of 16-bit wide parallel port at once.
 *
 * Runtime measured on STM32F107 @72MHz as < 3us
 *
 * @param Bank     GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 */
void gpio_stm32f1xx_parallel16_init( t_ttc_gpio_bank GPIOx, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );

/** Read 16 bit value from gpio bank at once-
 *
 * @param GPIOx     GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 */
t_u16 gpio_stm32f1xx_get_u16( t_ttc_gpio_bank GPIOx );
#define gpio_stm32f1xx_get_u16(GPIOx)  ((t_register_stm32f1xx_gpio_32*) GPIOx)->IDR

/** Write 16 bit value into gpio bank at once.
 *
 * Runtime measured on stm32f107 @72MHz was 240ns
 *
 * @param GPIOx     GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 */
void gpio_stm32f1xx_set_u16( t_ttc_gpio_bank GPIOx, t_u16 Value );
#define gpio_stm32f1xx_set_u16(GPIOx, Value)  ((t_register_stm32f1xx_gpio_32*) GPIOx)->ODR = Value

/** Initialize 8 bit wide parallel port.
 *
 * On architectures with 16 bit wide GPIO banks, 8 Bit ports can start at any
 * pin from 0 to 8. The first pin denotes how many bits to shift the 8-bit value.
 *
 * Runtime measured on STM32F107 @72MHz as < 2.2us
 *
 * @param Bank           GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 * @param FirstPin       = (0..8)  Lowest of 8 consecutive pins to use
 * @param Type           mode to use for all 8 pins  (-> ttc_gpio_types.h:e_ttc_gpio_mode)
 * @param Speed          speed to use for all 8 pins (-> ttc_gpio_types.h:e_ttc_gpio_speed)
 */
void gpio_stm32f1xx_parallel08_init( t_ttc_gpio_bank GPIOx, t_u8 FirstPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );


//} Function prototypes

#endif //ARCHITECTURE_GPIO_H
