#ifndef ARCHITECTURE_GPIO_H
#define ARCHITECTURE_GPIO_H

/** { gpio_stm32w1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for gpio devices on stm32w1xx architectures.
 *  Structures, Enums and Defines being required by high-level gpio and application.
 *
 *  Created from template device_architecture.h revision 21 at 20140208 06:39:49 UTC
 *
 *  Note: See ttc_gpio.h for description of stm32w1xx independent GPIO implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_GPIO_STM32W1XX
//
// Implementation status of low-level driver support for gpio devices on stm32w1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_GPIO_STM32W1XX '?'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_GPIO_STM32W1XX == '+')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_GPIO_STM32W1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_GPIO_STM32W1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#ifdef EXTENSION_255_stm32w1xx_standard_peripherals
    #include "stm32w108xx_gpio.h" // STM Standard Peripherals Library
#endif
#include "gpio_stm32w1xx_types.h"
#include "../register/register_stm32w1xx.h"
#include "../ttc_gpio_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_gpio_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_<device>_prepare
//
#define ttc_driver_gpio_init(PortPin, Type, Speed)                     gpio_stm32w1xx_init(PortPin, Type, Speed)
#define ttc_driver_gpio_clr(PortPin)                                   gpio_stm32w1xx_clr(PortPin)
#define ttc_driver_gpio_get(PortPin)                                   gpio_stm32w1xx_get(PortPin)
#define ttc_driver_gpio_set(PortPin)                                   gpio_stm32w1xx_set(PortPin)
#define ttc_driver_gpio_deinit(PortPin)                                gpio_stm32w1xx_deinit(PortPin)
#define ttc_driver_gpio_get_configuration(PortPin, Configuration)      gpio_stm32w1xx_get_configuration(PortPin, Configuration)
#define ttc_driver_gpio_prepare()                                      gpio_stm32w1xx_prepare()
#define ttc_driver_gpio_create_index(GPIOx, Pin)                       gpio_stm32w1xx_create_index(GPIOx, Pin)
#define ttc_driver_gpio_from_index(PhysicalIndex, GPIOx, Pin)          gpio_stm32w1xx_from_index(PhysicalIndex, GPIOx, Pin)
#define ttc_driver_gpio_alternate_function(PortPin, AlternateFunction) gpio_stm32w1xx_alternate_function(PortPin, AlternateFunction)
#define ttc_driver_gpio_put(PortPin, Value)                            gpio_stm32w1xx_put(PortPin, Value)
#define ttc_driver_gpio_parallel16_1_get gpio_stm32w1xx_parallel16_1_get
#define ttc_driver_gpio_parallel16_1_put gpio_stm32w1xx_parallel16_1_put
#define ttc_driver_gpio_parallel16_10_get gpio_stm32w1xx_parallel16_10_get
#define ttc_driver_gpio_parallel16_10_put gpio_stm32w1xx_parallel16_10_put
#define ttc_driver_gpio_parallel16_2_get gpio_stm32w1xx_parallel16_2_get
#define ttc_driver_gpio_parallel16_2_put gpio_stm32w1xx_parallel16_2_put
#define ttc_driver_gpio_parallel16_3_get gpio_stm32w1xx_parallel16_3_get
#define ttc_driver_gpio_parallel16_3_put gpio_stm32w1xx_parallel16_3_put
#define ttc_driver_gpio_parallel16_4_get gpio_stm32w1xx_parallel16_4_get
#define ttc_driver_gpio_parallel16_4_put gpio_stm32w1xx_parallel16_4_put
#define ttc_driver_gpio_parallel16_5_get gpio_stm32w1xx_parallel16_5_get
#define ttc_driver_gpio_parallel16_5_put gpio_stm32w1xx_parallel16_5_put
#define ttc_driver_gpio_parallel16_6_get gpio_stm32w1xx_parallel16_6_get
#define ttc_driver_gpio_parallel16_6_put gpio_stm32w1xx_parallel16_6_put
#define ttc_driver_gpio_parallel16_7_get gpio_stm32w1xx_parallel16_7_get
#define ttc_driver_gpio_parallel16_7_put gpio_stm32w1xx_parallel16_7_put
#define ttc_driver_gpio_parallel16_8_get gpio_stm32w1xx_parallel16_8_get
#define ttc_driver_gpio_parallel16_8_put gpio_stm32w1xx_parallel16_8_put
#define ttc_driver_gpio_parallel16_9_get gpio_stm32w1xx_parallel16_9_get
#define ttc_driver_gpio_parallel16_9_put gpio_stm32w1xx_parallel16_9_put
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

// not implemented
#undef ttc_driver_gpio_parallel16_init
#undef ttc_driver_gpio_parallel08_init
#undef ttc_driver_gpio_parallel08_1_get
#undef ttc_driver_gpio_parallel08_2_get
#undef ttc_driver_gpio_parallel08_3_get
#undef ttc_driver_gpio_parallel08_4_get
#undef ttc_driver_gpio_parallel08_5_get
#undef ttc_driver_gpio_parallel08_6_get
#undef ttc_driver_gpio_parallel08_7_get
#undef ttc_driver_gpio_parallel08_8_get
#undef ttc_driver_gpio_parallel08_9_get
#undef ttc_driver_gpio_parallel08_10_get
#undef ttc_driver_gpio_parallel08_1_put
#undef ttc_driver_gpio_parallel08_2_put
#undef ttc_driver_gpio_parallel08_3_put
#undef ttc_driver_gpio_parallel08_4_put
#undef ttc_driver_gpio_parallel08_5_put
#undef ttc_driver_gpio_parallel08_6_put
#undef ttc_driver_gpio_parallel08_7_put
#undef ttc_driver_gpio_parallel08_8_put
#undef ttc_driver_gpio_parallel08_9_put
#undef ttc_driver_gpio_parallel08_10_put

/** Fast gpio macros
 *
 * Runtime of set/clr macros on STM32W108 @24MHz is <1100 ns
 * (Measured with oscilloscope for 50 set()/clr() pairs on same pin)
 *
 */

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_gpio.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_gpio.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


// Single Bit Ports ******************************************************

/** set single output port bit to logical 1 (usually high level)
 *
 * Runtime on STM32W108 @12mHz is 4600 ns
 *
 * @param PortPin bit definition
 */
// void gpio_stm32w1xx_set(e_gpio_stm32w1xx_pin PortPin);
#define gpio_stm32w1xx_set(Pin) *(  (t_u32*) ( ((t_u32) Pin) + TTC_GPIO_OFFSET_SETR )  ) = 1

/** set single output port bit to logical 0 (usually high level)
 *
 * Runtime on STM32W108 @12mHz is 4600 ns
 *
 * @param PortPin bit definition
 */
// void gpio_stm32w1xx_clr(e_gpio_stm32w1xx_pin PortPin);
#define gpio_stm32w1xx_clr(Pin) *(  (t_u32*) ( ((t_u32) Pin) + TTC_GPIO_OFFSET_CLR  )  ) = 1

/** get value of single input port bit (voltage level at input pin)
 *
 * @param PortPin bit definition
 * @return  true = high level detected ad input port
 */
// BOOL gpio_stm32w1xx_get(e_gpio_stm32w1xx_pin PortPin);
#define gpio_stm32w1xx_get(Pin) *(  (t_u32*) ( ((t_u32) Pin) + TTC_GPIO_OFFSET_IDR  )  )

/** shutdown single GPIO unit device
 * @param PortPin bit definition
 */
void gpio_stm32w1xx_deinit( e_gpio_stm32w1xx_pin PortPin );

/** initializes single GPIO unit for operation
 * @param PortPin         port bit definition
 * @param Configuration  loaded with current configuration of given PortPin
 */
void gpio_stm32w1xx_get_configuration( e_gpio_stm32w1xx_pin PortPin, t_ttc_gpio_config* Configuration );

/** initializes single GPIO unit for operation
 * @param PortPin bit definition
 * @param Speed  GPIOs support different driver speeds
 * @param Type   operation mode to use
 */
void gpio_stm32w1xx_init( e_gpio_stm32w1xx_pin PortPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );

/** Prepares gpio driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void gpio_stm32w1xx_prepare();

/** creates numeric representing given GPIO-bank and pin number
 *
 * @param GPIOx  GPIO bank to use (-> ttc_gpio_types.h:t_ttc_gpio_bank)
 *               Note: GPIOx, Pin can be given as one argument by use of Pin_Pxn macro (TTC_GPIO_BANK_A, 7) = Pin_PA7
 * @param Pin    0..15
 * @return       32-bit value representing given GPIO-pin in a scalar datatype
 */
t_base gpio_stm32w1xx_create_index( t_ttc_gpio_bank GPIOx, t_u8 Pin );

/** restores GPIO-bank and pin number from 8-bit value
 *
 * @param PhysicalIndex  32-bit value representing GPIO-bank + pin
 * @param GPIOx          loaded with GPIO bank (-> ttc_gpio_types.h:t_ttc_gpio_bank)
 * @param Pin            loaded with pin number
 */
void gpio_stm32w1xx_from_index( t_base PhysicalIndex, t_ttc_gpio_bank* GPIOx, t_u8* Pin );


/** Changes mapping of specified pin to alternate function.
 *
 * Note: Configure PortPin as alternate function pin via ttc_gpio_init() before or after calling this function!
 *
 * @param PortPin            Port bit definition
 * @param AlternateFunction  Selects alternate function to use on given PortPin
 */
void gpio_stm32w1xx_alternate_function( e_ttc_gpio_pin PortPin, e_ttc_gpio_alternate_function AlternateFunction );


/** Set single output port bit to either logical high or Low.
 *
 * @param PortPin bit definition
 * @param Value   new logical state of output pin (==true: set to high; ==false: set to low)
 */
void gpio_stm32w1xx_put( e_ttc_gpio_pin PortPin, BOOL Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32w1xx_parallel16_10_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32w1xx_parallel16_10_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32w1xx_parallel16_2_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32w1xx_parallel16_2_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32w1xx_parallel16_3_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32w1xx_parallel16_3_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32w1xx_parallel16_4_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32w1xx_parallel16_4_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32w1xx_parallel16_5_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32w1xx_parallel16_5_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32w1xx_parallel16_6_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32w1xx_parallel16_6_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32w1xx_parallel16_7_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32w1xx_parallel16_7_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32w1xx_parallel16_8_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32w1xx_parallel16_8_put( t_u16 Value );


/** read 8 bit value from an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @return          16 bit value read from register of given gpio bank
 */
t_u16 gpio_stm32w1xx_parallel16_9_get();


/** write 8 bit value to an 8 bit parallel port
 *
 * For maximum speed, one function exists for each supported logical index (1..10).
 * Most gpio_* low-level drivers can simply define each function as a single memory access with no function overhead.
 *
 * @param   Value    16 bit data to write into output data register of configured gpio bank
 */
void gpio_stm32w1xx_parallel16_9_put( t_u16 Value );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _gpio_stm32w1xx_foo(t_ttc_gpio_config* Config)

t_u8 _gpio_stm32w1xx_compile_port_config( e_ttc_gpio_mode Mode );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //ARCHITECTURE_GPIO_H
