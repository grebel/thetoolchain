/** gpio_common.c ***********************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to gpio low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.c revision 12 at 20180202
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * Put all includes here that are required to compile this source code.
 */

#include "gpio_common.h"
#include "../ttc_heap.h"      // dynamic memory and safe arrays  
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)
//}PrivateFunctions
/** Function Definitions ********************************************{
 */

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
