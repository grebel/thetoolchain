/** { gpio_stm32l0xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for gpio devices on stm32l0xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150305 09:34:47 UTC
 *
 *  Note: See ttc_gpio.h for description of stm32l0xx independent GPIO implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "gpio_stm32l0xx.h".
//
#include "gpio_stm32l0xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

t_base gpio_stm32l0xx_create_index(t_ttc_gpio_bank GPIOx, t_u8 Pin) {


#warning Function gpio_stm32l0xx_create_index() is empty.
    
    return (t_base) 0;
}
void gpio_stm32l0xx_deinit(e_ttc_gpio_pin PortPin) {


#warning Function gpio_stm32l0xx_deinit() is empty.
    

}
void gpio_stm32l0xx_from_index(t_base PhysicalIndex, t_ttc_gpio_bank* GPIOx, t_u8* Pin) {
    Assert_GPIO(GPIOx, ttc_assert_origin_auto); // pointers must not be NULL
    Assert_GPIO(Pin, ttc_assert_origin_auto); // pointers must not be NULL


    *GPIOx = (t_ttc_gpio_bank) cm0_calc_peripheral_Word(PhysicalIndex);
    *Pin   = (t_u8)            cm0_calc_peripheral_BitNumber(PhysicalIndex);
    

}
void gpio_stm32l0xx_get_configuration(e_ttc_gpio_pin PortPin, t_ttc_gpio_config* Configuration) {
    Assert_GPIO(Configuration, ttc_assert_origin_auto); // pointers must not be NULL

#warning Function gpio_stm32l0xx_get_configuration() is empty.
    

}
void gpio_stm32l0xx_init(e_ttc_gpio_pin PortPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed) {

    //when interrupt_stm32l0xx is created, these should be performed in interrupt_stm32l0xx_gpio_from_index()

    e_gpio_stm32l0xx_bank GPIOx = (t_ttc_gpio_bank) cm0_calc_peripheral_Word(PortPin);
    t_u8                    Pin = (t_u8)            cm0_calc_peripheral_BitNumber(PortPin);

    //give power to the GPIO bank via the RCC clocks
    _e_gpio_stm32l0xx_bank_enable(GPIOx);

     t_stm32l0_gpiox_moder MODER;
     t_stm32l0_gpiox_otyper TYPE;
     t_stm32l0_gpiox_pupdr PUPD;

    //Set the mode
    switch(Mode)
    {
        case(E_ttc_gpio_mode_analog_in):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpc_analog) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;
            break;
        }
        case(E_ttc_gpio_mode_input_floating):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpio_input) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Pull-up Pull down resistor configuration */
            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All &= ~(gpio_PUPDR_PUPDR0 << ((t_u16)Pin * 2));
            PUPD.All |= (((t_u32)gpio_noPULL) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;

            break;
        }
        case(E_ttc_gpio_mode_input_pull_down):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpio_input) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Pull-up Pull down resistor configuration */
            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All &= ~(gpio_PUPDR_PUPDR0 << ((t_u16)Pin * 2));
            PUPD.All |= (((t_u32)gpio_pull_down) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;

            break;
        }
        case(E_ttc_gpio_mode_input_pull_up):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpio_input) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Pull-up Pull down resistor configuration */
            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All &= ~(gpio_PUPDR_PUPDR0 << ((t_u16)Pin * 2));
            PUPD.All |= (((t_u32)gpio_pull_up) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;
            break;
        }
        case(E_ttc_gpio_mode_output_open_drain):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpio_general_purpose_output) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Output mode configuration */
            TYPE = ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE;
            TYPE.All &= ~((gpio_OTYPER_OT_0) << ((t_u16)Pin));
            TYPE.All |= (t_u16)(((t_u16)gpio_otype_open_drain) << ((t_u16)Pin));
            ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE = TYPE;

            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All |= (((t_u32)gpio_noPULL) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;

            break;
        }
        case(E_ttc_gpio_mode_output_open_drain_pull_up_resistor):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpio_general_purpose_output) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Output mode configuration */
            TYPE = ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE;
            TYPE.All &= ~((gpio_OTYPER_OT_0) << ((t_u16)Pin));
            TYPE.All |= (t_u16)(((t_u16)gpio_otype_open_drain) << ((t_u16)Pin));
            ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE = TYPE;

            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All |= (((t_u32)gpio_pull_up) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;

            break;
        }
        case(E_ttc_gpio_mode_output_open_drain_pull_down_resistor):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpio_general_purpose_output) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Output mode configuration */
            TYPE = ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE;
            TYPE.All &= ~((gpio_OTYPER_OT_0) << ((t_u16)Pin));
            TYPE.All |= (t_u16)(((t_u16)gpio_otype_open_drain) << ((t_u16)Pin));
            ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE = TYPE;

            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All |= (((t_u32)gpio_pull_down) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;

            break;
        }
        case(E_ttc_gpio_mode_output_push_pull):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpio_general_purpose_output) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Output mode configuration */
            TYPE = ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE;
            TYPE.All &= ~((gpio_OTYPER_OT_0) << ((t_u16)Pin));
            TYPE.All |= (t_u16)(((t_u16)gpio_otype_push_pull) << ((t_u16)Pin));
            ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE = TYPE;

            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All |= (((t_u32)gpio_noPULL) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;
            break;
        }
        case(E_ttc_gpio_mode_output_push_pull_pull_up_resistor):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpio_general_purpose_output) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Output mode configuration */
            TYPE = ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE;
            TYPE.All &= ~((gpio_OTYPER_OT_0) << ((t_u16)Pin));
            TYPE.All |= (t_u16)(((t_u16)gpio_otype_push_pull) << ((t_u16)Pin));
            ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE = TYPE;

            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All |= (((t_u32)gpio_pull_up) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;
            break;
        }
        case(E_ttc_gpio_mode_output_push_pull_pull_down_resistor):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpio_general_purpose_output) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Output mode configuration */
            TYPE = ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE;
            TYPE.All &= ~((gpio_OTYPER_OT_0) << ((t_u16)Pin));
            TYPE.All |= (t_u16)(((t_u16)gpio_otype_push_pull) << ((t_u16)Pin));
            ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE = TYPE;

            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All |= (((t_u32)gpio_pull_down) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;
            break;
        }
        case(E_ttc_gpio_mode_alternate_function_push_pull):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpc_alternate_function) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Output mode configuration */
            TYPE = ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE;
            TYPE.All &= ~((gpio_OTYPER_OT_0) << ((t_u16)Pin));
            TYPE.All |= (t_u16)(((t_u16)gpio_otype_push_pull) << ((t_u16)Pin));
            ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE = TYPE;

            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All |= (((t_u32)gpio_noPULL) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;
            break;
        }
        case(E_ttc_gpio_mode_alternate_function_push_pull_pull_up_resistor):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpc_alternate_function) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Output mode configuration */
            TYPE = ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE;
            TYPE.All &= ~((gpio_OTYPER_OT_0) << ((t_u16)Pin));
            TYPE.All |= (t_u16)(((t_u16)gpio_otype_push_pull) << ((t_u16)Pin));
            ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE = TYPE;

            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All |= (((t_u32)gpio_pull_up) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;
            break;
        }
        case(E_ttc_gpio_mode_alternate_function_push_pull_pull_down_resistor):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpc_alternate_function) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Output mode configuration */
            TYPE = ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE;
            TYPE.All &= ~((gpio_OTYPER_OT_0) << ((t_u16)Pin));
            TYPE.All |= (t_u32)(((t_u16)gpio_otype_push_pull) << ((t_u16)Pin));
            ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE = TYPE;

            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All |= (((t_u32)gpio_pull_down) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;

            break;
        }
        case(E_ttc_gpio_mode_alternate_function_open_drain):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpc_alternate_function) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Output mode configuration */
            TYPE = ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE;
            TYPE.All &= ~((gpio_OTYPER_OT_0) << ((t_u16)Pin));
            TYPE.All |= (t_u32)(((t_u16)gpio_otype_open_drain) << ((t_u16)Pin));
            ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE = TYPE;

            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All |= (((t_u32)gpio_noPULL) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;
            break;
        }
        case(E_ttc_gpio_mode_alternate_function_open_drain_pull_up_resistor):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpc_alternate_function) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Output mode configuration */
            TYPE = ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE;
            TYPE.All &= ~((gpio_OTYPER_OT_0) << ((t_u16)Pin));
            TYPE.All |= (t_u32)(((t_u16)gpio_otype_open_drain) << ((t_u16)Pin));
            ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE = TYPE;

            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All |= (((t_u32)gpio_pull_up) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;
            break;
        }
        case(E_ttc_gpio_mode_alternate_function_open_drain_pull_down_resistor):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpc_alternate_function) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Output mode configuration */
            TYPE = ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE;
            TYPE.All &= ~((gpio_OTYPER_OT_0) << ((t_u16)Pin));
            TYPE.All |= (t_u32)(((t_u16)gpio_otype_open_drain) << ((t_u16)Pin));
            ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE = TYPE;

            PUPD = ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD;
            PUPD.All |= (((t_u32)gpio_pull_down) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->PUPD = PUPD;
            break;
        }
        case(E_ttc_gpio_mode_alternate_function_special_sclk):
        {
            //all the above cases are input, so clear 2 bits in MODER corresponding to pin
            MODER = ((t_register_stm32l0xx_gpio*)GPIOx)->MODER;
            MODER.All &= ~(gpio_MODER_MODER0 << (Pin * 2));
            MODER.All |= ((gpc_alternate_function) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->MODER = MODER;

            /* Output mode configuration */
            TYPE = ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE;
            TYPE.All &= ~((gpio_OTYPER_OT_0) << ((t_u16)Pin));
            ((t_register_stm32l0xx_gpio*)GPIOx)->TYPE = TYPE;

            break;
        }
        case(E_ttc_gpio_mode_unknown):
            break;
        default:
            break; //have to put this here or will get a warning for unhandled cases.
    }

    t_stm32l0_gpiox_ospeedr OSPEEDR;
    OSPEEDR.All &= ~(gpio_OSPEEDER_OSPEEDR0 << (Pin * 2));

    switch(Speed)
    {
        case E_ttc_gpio_speed_400khz:
        {
            OSPEEDR = ((t_register_stm32l0xx_gpio*)GPIOx)->SPEED;
            OSPEEDR.All |= ((t_u32)(gpio_speed_400kHz) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->SPEED = OSPEEDR;
            break;
        }
        case E_ttc_gpio_speed_2mhz:
        {
            OSPEEDR = ((t_register_stm32l0xx_gpio*)GPIOx)->SPEED;
            OSPEEDR.All |= ((t_u32)(gpio_speed_2MHz) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->SPEED = OSPEEDR;
            break;
        }
        case E_ttc_gpio_speed_10mhz:
        {
            OSPEEDR = ((t_register_stm32l0xx_gpio*)GPIOx)->SPEED;
            OSPEEDR.All |= ((t_u32)(gpio_speed_10MHz) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->SPEED = OSPEEDR;
            break;
        }
        case E_ttc_gpio_speed_40mhz:
        {
            OSPEEDR = ((t_register_stm32l0xx_gpio*)GPIOx)->SPEED;
            OSPEEDR.All |= ((t_u32)(gpio_speed_40MHz) << (Pin * 2));
            ((t_register_stm32l0xx_gpio*)GPIOx)->SPEED = OSPEEDR;
            break;
        }
        default:break; //have to put this here or will get a warning for unhandled cases.
    }
    

}
void gpio_stm32l0xx_parallel16_init(t_ttc_gpio_bank GPIOx, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed) {

    e_gpio_stm32l0xx_pin PortPin;

    switch(GPIOx){
        case(E_ttc_gpio_bank_a):{ PortPin = E_ttc_gpio_pin_a0; break;}
        case(E_ttc_gpio_bank_b):{ PortPin = E_ttc_gpio_pin_b0; break;}
        case(E_ttc_gpio_bank_c):{ PortPin = E_ttc_gpio_pin_c0; break;}
        case(E_ttc_gpio_bank_d):{ PortPin = E_ttc_gpio_pin_d0; break;}
        case(tgb_bank_h):{ PortPin = E_ttc_gpio_pin_h0; break;}
        default:
            break;
    }

    //initialize all bank pins
    for(int i=0; i<16; i++){
        gpio_stm32l0xx_init(PortPin, Mode, Speed);
        PortPin = PortPin + 4;
    }

}
void gpio_stm32l0xx_parallel08_init(t_ttc_gpio_bank GPIOx, t_u8 FirstPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed) {


    e_gpio_stm32l0xx_pin PortPin;

    switch(GPIOx){
        case(E_ttc_gpio_bank_a):{
            switch(FirstPin){
                case(0):{PortPin = E_ttc_gpio_pin_a0; break;}
                case(1):{PortPin = E_ttc_gpio_pin_a1; break;}
                case(2):{PortPin = E_ttc_gpio_pin_a2; break;}
                case(3):{PortPin = E_ttc_gpio_pin_a3; break;}
                case(4):{PortPin = E_ttc_gpio_pin_a4; break;}
                case(5):{PortPin = E_ttc_gpio_pin_a5; break;}
                case(6):{PortPin = E_ttc_gpio_pin_a6; break;}
                case(7):{PortPin = E_ttc_gpio_pin_a7; break;}
                case(8):{PortPin = E_ttc_gpio_pin_a8; break;}
                default:
                    break;
            }
        }
        case(E_ttc_gpio_bank_b):{
            switch(FirstPin){
                case(0):{PortPin = E_ttc_gpio_pin_b0; break;}
                case(1):{PortPin = E_ttc_gpio_pin_b1; break;}
                case(2):{PortPin = E_ttc_gpio_pin_b2; break;}
                case(3):{PortPin = E_ttc_gpio_pin_b3; break;}
                case(4):{PortPin = E_ttc_gpio_pin_b4; break;}
                case(5):{PortPin = E_ttc_gpio_pin_b5; break;}
                case(6):{PortPin = E_ttc_gpio_pin_b6; break;}
                case(7):{PortPin = E_ttc_gpio_pin_b7; break;}
                case(8):{PortPin = E_ttc_gpio_pin_b8; break;}
                default:
                    break;
            }
        }
        case(E_ttc_gpio_bank_c):{
            switch(FirstPin){
                case(0):{PortPin = E_ttc_gpio_pin_c0; break;}
                case(1):{PortPin = E_ttc_gpio_pin_c1; break;}
                case(2):{PortPin = E_ttc_gpio_pin_c2; break;}
                case(3):{PortPin = E_ttc_gpio_pin_c3; break;}
                case(4):{PortPin = E_ttc_gpio_pin_c4; break;}
                case(5):{PortPin = E_ttc_gpio_pin_c5; break;}
                case(6):{PortPin = E_ttc_gpio_pin_c6; break;}
                case(7):{PortPin = E_ttc_gpio_pin_c7; break;}
                case(8):{PortPin = E_ttc_gpio_pin_c8; break;}
                default:
                    break;
            }
        }
        case(E_ttc_gpio_bank_d):{
            switch(FirstPin){
                case(0):{PortPin = E_ttc_gpio_pin_d0; break;}
                case(1):{PortPin = E_ttc_gpio_pin_d1; break;}
                case(2):{PortPin = E_ttc_gpio_pin_d2; break;}
                case(3):{PortPin = E_ttc_gpio_pin_d3; break;}
                case(4):{PortPin = E_ttc_gpio_pin_d4; break;}
                case(5):{PortPin = E_ttc_gpio_pin_d5; break;}
                case(6):{PortPin = E_ttc_gpio_pin_d6; break;}
                case(7):{PortPin = E_ttc_gpio_pin_d7; break;}
                case(8):{PortPin = E_ttc_gpio_pin_d8; break;}
                default:
                    break;
            }
        }
        case(tgb_bank_h):{
            switch(FirstPin){
                case(0):{PortPin = E_ttc_gpio_pin_h0; break;}
                case(1):{PortPin = E_ttc_gpio_pin_h1; break;}
                case(2):{PortPin = E_ttc_gpio_pin_h2; break;}
                case(3):{PortPin = E_ttc_gpio_pin_h3; break;}
                case(4):{PortPin = E_ttc_gpio_pin_h4; break;}
                case(5):{PortPin = E_ttc_gpio_pin_h5; break;}
                case(6):{PortPin = E_ttc_gpio_pin_h6; break;}
                case(7):{PortPin = E_ttc_gpio_pin_h7; break;}
                case(8):{PortPin = E_ttc_gpio_pin_h8; break;}
                default:
                    break;
            }
        }
        default:
            break;
    }

    //initialize all bank pins
    for(int i=0; i<8; i++){
        gpio_stm32l0xx_init(PortPin, Mode, Speed);
        PortPin = PortPin + 4;
    }
    

}
void gpio_stm32l0xx_prepare() {


#warning Function gpio_stm32l0xx_prepare() is empty.
    

}
void gpio_stm32l0xx_set(e_ttc_gpio_pin PortPin) {

    e_gpio_stm32l0xx_bank GPIOx = (t_ttc_gpio_bank) cm0_calc_peripheral_Word(PortPin);
    t_u8                    Pin = (t_u8)            cm0_calc_peripheral_BitNumber(PortPin);

    stm32l0_t_gpiox_bsrr BSRR;

//      HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 1);

    BSRR.All = 0;
    BSRR.All = 1 << Pin;
    ((t_register_stm32l0xx_gpio*)GPIOx)->BSRR = BSRR;
}
void gpio_stm32l0xx_clr(e_ttc_gpio_pin PortPin) {

    e_gpio_stm32l0xx_bank GPIOx = (t_ttc_gpio_bank) cm0_calc_peripheral_Word(PortPin);
    t_u8                    Pin = (t_u8)            cm0_calc_peripheral_BitNumber(PortPin);

    stm32l0_t_gpiox_brr BRR;

//     HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 0);

    BRR.All = 0;
    BRR.All = 1 << Pin;
    ((t_register_stm32l0xx_gpio*)GPIOx)->BRR = BRR;
}
void gpio_stm32l0xx_alternate_function(e_gpio_stm32l0xx_pin PortPin, t_u8 AlternateFunction){

    //when interrupt_stm32l0xx is created, these should be performed in interrupt_stm32l0xx_gpio_from_index()
    e_gpio_stm32l0xx_bank GPIOx = (t_ttc_gpio_bank) cm0_calc_peripheral_Word(PortPin);
    t_u8                    Pin = (t_u8)            cm0_calc_peripheral_BitNumber(PortPin);

    //give power to the GPIO bank via the RCC clocks
    _e_gpio_stm32l0xx_bank_enable(GPIOx);

    t_u32 temp = 0x00;
    //X t_u32 temp_2 = 0x00;

    t_stm32l0_gpiox_afrl    AFRL;
    t_stm32l0_gpiox_afrh    AFRH;


    temp = ((t_u32)(AlternateFunction) << ((t_u32)((t_u32)Pin & (t_u32)0x07) * 4)) ;
    if(Pin<8){
        AFRL = ((t_register_stm32l0xx_gpio*)GPIOx)->AFRL;
        AFRL.All &= ~((t_u32)0xF << ((t_u32)((t_u32)Pin & (t_u32)0x07) * 4)) ;
        AFRL.All  = AFRL.All | temp;
        ((t_register_stm32l0xx_gpio*)GPIOx)->AFRL = AFRL;
    }
    else{
        AFRH = ((t_register_stm32l0xx_gpio*)GPIOx)->AFRH;
        AFRH.All &= ~((t_u32)0xF << ((t_u32)((t_u32)Pin & (t_u32)0x07) * 4)) ;
        AFRH.All  = AFRH.All | temp;
        ((t_register_stm32l0xx_gpio*)GPIOx)->AFRH = AFRH;
    }
}
void gpio_stm32l0xx_put(e_ttc_gpio_pin PortPin, BOOL Value) {


#warning Function gpio_stm32l0xx_put() is empty.
    

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

void _e_gpio_stm32l0xx_bank_enable(t_ttc_gpio_bank GPIOx) {
    switch ( GPIOx) { // enable peripheral clock to this port
      case E_ttc_gpio_bank_a: __GPIOA_CLK_ENABLE(); break;
      case E_ttc_gpio_bank_b: __GPIOB_CLK_ENABLE(); break;
      case E_ttc_gpio_bank_c: __GPIOC_CLK_ENABLE(); break;
      case E_ttc_gpio_bank_d: __GPIOD_CLK_ENABLE(); break;
#ifdef GPIOH
      case tgb_bank_h: __GPIOH_CLK_ENABLE(); break;
#endif
      default: Assert(0, ttc_assert_origin_auto); break; // ERROR: unknown GPIO given!
    }
}


//}Private Functions










