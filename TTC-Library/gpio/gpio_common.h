#ifndef gpio_common_h
#define gpio_common_h

/** gpio_common.h ****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to gpio low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.h revision 14 at <DATE>
 *
 *  Authors: Gregor Rebel
 *
 *  Description of gpio_common (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "gpio_common.c"
 */

#include "../ttc_basic.h"
#include "../ttc_gpio_types.h" // will include gpio_common_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_gpio_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_gpio_foo
 */

/* create bitmasks for fast 8 bit parallel port access (usable by all architectures with 16 - or 32- bit wide GPIO banks)
 *
 * TTC_GPIO_PARALLEL08_<n>_MASK_SET   bitmask with all valid bits set (use to extract shifted 8 bit data from 16 bit value)
 * TTC_GPIO_PARALLEL08_<n>_MASK_CLR   bitmask with all valid bits cleared (use to clear shifted 8 bit data in 16 bit value)
 *
 * Example usage (read 8-bit value from parallel port #1):
 * t_u8  Byte = (GPIOB->IDR & TTC_GPIO_PARALLEL08_1_MASK_SET) >> TTC_GPIO_PARALLEL08_1_FIRSTPIN;
 *
 * Example usage (write 8-bit value to parallel port #1):
 * t_u16  Byte = 0x42;
 * GPIOB->ODR = (GPIOB->ODR & TTC_GPIO_PARALLEL08_1_MASK_CLR) | (Byte << TTC_GPIO_PARALLEL08_1_FIRSTPIN);
 */
#ifdef TTC_GPIO_PARALLEL08_1_BANK
    #define TTC_GPIO_PARALLEL08_1_MASK_SET ( (t_u16) (0x00ff << TTC_GPIO_PARALLEL08_1_FIRSTPIN) )
    #define TTC_GPIO_PARALLEL08_1_MASK_CLR ( (t_u16) (TTC_GPIO_PARALLEL08_1_MASK_SET ^ 0xffff) )
#else
    #define TTC_GPIO_PARALLEL08_1_MASK_SET 0xffff
    #define TTC_GPIO_PARALLEL08_1_MASK_CLR 0x0000
#endif

TODO( "2..10" )

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions
/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_gpio.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_gpio.h for details.
 *
 * You normally should not need to add functions here.
 * Global feature functions must be added in ttc_gpio.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl gpio UPDATE".
 */

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //gpio_common_h
