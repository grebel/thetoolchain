#ifndef ARCHITECTURE_GPIO_H
#define ARCHITECTURE_GPIO_H

/** { gpio_stm32l1xx.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for gpio devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by high-level gpio and application.
 *
 *  Created from template device_architecture.h revision 21 at 20140206 15:39:14 UTC
 *
 *  Note: See ttc_gpio.h for description of stm32l1xx independent GPIO implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

#define gpio_MODER_MODER0          ((t_u32)0x00000003)
#define gpio_OTYPER_OT_0           ((t_u32)0x00000001)
#define gpio_PUPDR_PUPDR0          ((t_u32)0x00000003)
#define gpio_OSPEEDER_OSPEEDR0     ((t_u32)0x00000003)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_GPIO_STM32L1XX
//
// Implementation status of low-level driver support for gpio devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_GPIO_STM32L1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_GPIO_STM32L1XX == '?')
    #  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_GPIO_STM32L1XX to one from '-', 'o', '+'
#endif

//}EXTENSION_STATUS_TTC_GPIO_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************

#include "../register/register_stm32l1xx.h"
#include "gpio_stm32l1xx_types.h"
#include "../ttc_gpio_types.h"
#include "../ttc_basic_types.h"
#include "additionals/270_CPAL/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32L1xx/stm32l1xx.h"
//? #include "stm32l1xx_rcc.h"
//? #include "stm32l1xx_gpio.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_gpio_interface.h
#define ttc_driver_gpio_init(PortPin, Type, Speed)                             gpio_stm32l1xx_init(PortPin, Type, Speed)
#define ttc_driver_gpio_deinit(PortPin)                                        gpio_stm32l1xx_deinit(PortPin)
#define ttc_driver_gpio_get(PortPin)                                           gpio_stm32l1xx_get(PortPin)
#define ttc_driver_gpio_set(PortPin)                                           gpio_stm32l1xx_set(PortPin)
#define ttc_driver_gpio_clr(PortPin)                                           gpio_stm32l1xx_clr(PortPin)
#define ttc_driver_gpio_put(PortPin, Value)                                    gpio_stm32l1xx_put(PortPin, Value)
#define ttc_driver_gpio_create_index(GPIOx, Pin)                               gpio_stm32l1xx_create_index(GPIOx, Pin)
#define ttc_driver_gpio_from_index(PhysicalIndex, GPIOx, Pin)                  gpio_stm32l1xx_from_index(PhysicalIndex, GPIOx, Pin)
#define ttc_driver_gpio_parallel08_init(GPIOx, FirstPin, Mode, Speed)          gpio_stm32l1xx_parallel08_init(GPIOx, FirstPin, Mode, Speed)
#define ttc_driver_gpio_parallel16_init(GPIOx, Mode, Speed)                    gpio_stm32l1xx_parallel16_init(GPIOx, Mode, Speed)
#define ttc_driver_gpio_get_configuration(PortPin, Configuration)              gpio_stm32l1xx_get_configuration(PortPin, Configuration)
#define ttc_driver_gpio_prepare()                                              gpio_stm32l1xx_prepare()
#define ttc_driver_gpio_alternate_function(PortPin, AlternateFunction)         gpio_stm32l1xx_alternate_function(PortPin, AlternateFunction)
/* DEPRECATED
#define ttc_driver_gpio_parallel08_1_get()                                     ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_1_BANK)->IDR )
#define ttc_driver_gpio_parallel08_2_get()                                     ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_2_BANK)->IDR )
#define ttc_driver_gpio_parallel08_3_get()                                     ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_3_BANK)->IDR )
#define ttc_driver_gpio_parallel08_4_get()                                     ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_4_BANK)->IDR )
#define ttc_driver_gpio_parallel08_5_get()                                     ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_5_BANK)->IDR )
#define ttc_driver_gpio_parallel08_6_get()                                     ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_6_BANK)->IDR )
#define ttc_driver_gpio_parallel08_7_get()                                     ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_7_BANK)->IDR )
#define ttc_driver_gpio_parallel08_8_get()                                     ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_8_BANK)->IDR )
#define ttc_driver_gpio_parallel08_9_get()                                     ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_9_BANK)->IDR )
#define ttc_driver_gpio_parallel08_10_get()                                    ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_10_BANK)->IDR )
#define ttc_driver_gpio_parallel08_1_put(Value)                                ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_1_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_2_put(Value)                                ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_2_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_3_put(Value)                                ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_3_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_4_put(Value)                                ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_4_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_5_put(Value)                                ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_5_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_6_put(Value)                                ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_6_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_7_put(Value)                                ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_7_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_8_put(Value)                                ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_8_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_9_put(Value)                                ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_9_BANK)->ODR ) = Value
#define ttc_driver_gpio_parallel08_10_put(Value)                               ( ((GPIO_TypeDef *) TTC_GPIO_PARALLEL08_10_BANK)->ODR ) = Value
*/
#  define ttc_driver_gpio_parallel08_1_get()                                    ( (t_u8) ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_1_BANK)->IDR >> TTC_GPIO_PARALLEL08_1_FIRSTPIN) )
#  define ttc_driver_gpio_parallel08_1_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_1_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_1_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_1_FIRSTPIN + 16) )
#  define ttc_driver_gpio_parallel08_2_get()                                    ( (t_u8) ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_2_BANK)->IDR >> TTC_GPIO_PARALLEL08_2_FIRSTPIN) )
#  define ttc_driver_gpio_parallel08_2_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_2_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_2_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_2_FIRSTPIN + 16) )
#  define ttc_driver_gpio_parallel08_3_get()                                    ( (t_u8) ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_3_BANK)->IDR >> TTC_GPIO_PARALLEL08_3_FIRSTPIN) )
#  define ttc_driver_gpio_parallel08_3_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_3_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_3_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_3_FIRSTPIN + 16) )
#  define ttc_driver_gpio_parallel08_4_get()                                    ( (t_u8) ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_4_BANK)->IDR >> TTC_GPIO_PARALLEL08_4_FIRSTPIN) )
#  define ttc_driver_gpio_parallel08_4_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_4_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_4_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_4_FIRSTPIN + 16) )
#  define ttc_driver_gpio_parallel08_5_get()                                    ( (t_u8) ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_5_BANK)->IDR >> TTC_GPIO_PARALLEL08_5_FIRSTPIN) )
#  define ttc_driver_gpio_parallel08_5_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_5_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_5_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_5_FIRSTPIN + 16) )
#  define ttc_driver_gpio_parallel08_6_get()                                    ( (t_u8) ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_6_BANK)->IDR >> TTC_GPIO_PARALLEL08_6_FIRSTPIN) )
#  define ttc_driver_gpio_parallel08_6_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_6_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_6_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_6_FIRSTPIN + 16) )
#  define ttc_driver_gpio_parallel08_7_get()                                    ( (t_u8) ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_7_BANK)->IDR >> TTC_GPIO_PARALLEL08_7_FIRSTPIN) )
#  define ttc_driver_gpio_parallel08_7_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_7_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_7_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_7_FIRSTPIN + 16) )
#  define ttc_driver_gpio_parallel08_8_get()                                    ( (t_u8) ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_8_BANK)->IDR >> TTC_GPIO_PARALLEL08_8_FIRSTPIN) )
#  define ttc_driver_gpio_parallel08_8_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_8_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_8_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_8_FIRSTPIN + 16) )
#  define ttc_driver_gpio_parallel08_9_get()                                    ( (t_u8) ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_9_BANK)->IDR >> TTC_GPIO_PARALLEL08_9_FIRSTPIN) )
#  define ttc_driver_gpio_parallel08_9_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_9_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_9_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_9_FIRSTPIN + 16) )
#  define ttc_driver_gpio_parallel08_10_get()                                   ( (t_u8) ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_10_BANK)->IDR >> TTC_GPIO_PARALLEL08_10_FIRSTPIN) )
#  define ttc_driver_gpio_parallel08_10_put(Value)                              ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL08_10_BANK)->BSRR) = ( ((t_u32) (Value)) << TTC_GPIO_PARALLEL08_10_FIRSTPIN ) | ( ((t_u32) (0xff ^ Value)) << (TTC_GPIO_PARALLEL08_10_FIRSTPIN + 16) )

#define ttc_driver_gpio_parallel16_1_get()                                    ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_1_BANK)->IDR )
#define ttc_driver_gpio_parallel16_2_get()                                    ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_2_BANK)->IDR )
#define ttc_driver_gpio_parallel16_3_get()                                    ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_3_BANK)->IDR )
#define ttc_driver_gpio_parallel16_4_get()                                    ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_4_BANK)->IDR )
#define ttc_driver_gpio_parallel16_5_get()                                    ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_5_BANK)->IDR )
#define ttc_driver_gpio_parallel16_6_get()                                    ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_6_BANK)->IDR )
#define ttc_driver_gpio_parallel16_7_get()                                    ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_7_BANK)->IDR )
#define ttc_driver_gpio_parallel16_8_get()                                    ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_8_BANK)->IDR )
#define ttc_driver_gpio_parallel16_9_get()                                    ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_9_BANK)->IDR )
#define ttc_driver_gpio_parallel16_10_get()                                   ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_10_BANK)->IDR )
#define ttc_driver_gpio_parallel16_1_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_1_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_2_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_2_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_3_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_3_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_4_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_4_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_5_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_5_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_6_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_6_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_7_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_7_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_8_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_8_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_9_put(Value)                               ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_9_BANK)->ODR ) = (t_u16) (Value)
#define ttc_driver_gpio_parallel16_10_put(Value)                              ( ((t_register_stm32l1xx_gpio_32*) TTC_GPIO_PARALLEL16_10_BANK)->ODR ) = (t_u16) (Value)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_gpio.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_gpio.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */

/** initializes single GPIO unit for operation
 * @param PortPin bit definition
 * @param Speed  GPIOs support different driver speeds
 * @param Type   operation mode to use
 */
void gpio_stm32l1xx_init( e_gpio_stm32l1xx_pin PortPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );

/** shutdown single GPIO unit device
 * @param PortPin bit definition
 */
void gpio_stm32l1xx_deinit( e_gpio_stm32l1xx_pin  PortPin );

/** get value of single input port bit (voltage level at input pin)
 *
 * @param PortPin bit definition
 * @return   =
 */
BOOL gpio_stm32l1xx_get( e_gpio_stm32l1xx_pin PortPin );
#define gpio_stm32l1xx_get(Pin) *((volatile t_u8*) ( ((t_u32) Pin) + TTC_GPIO_OFFSET_IDR  )  )

/** set single output port bit to logical 1 (usually high level)
 *
 * @param PortPin bit definition
 * Note: set bits are bits 0-15 of the BSRR
 */
void gpio_stm32l1xx_set( volatile e_gpio_stm32l1xx_pin PortPin );
#define gpio_stm32l1xx_set(Pin) *((volatile t_u8*) ( ((t_u32) Pin) + TTC_GPIO_OFFSET_BSRR)  ) = 1

/** set single output port bit to logical 0 (usually high level)
 *
 * @param PortPin bit definition
 * Note: reset bits are bits 16-31 of the BSRR
 */
void gpio_stm32l1xx_clr( volatile e_gpio_stm32l1xx_pin PortPin );
#define gpio_stm32l1xx_clr(Pin) *((volatile t_u8*) ( ((t_u32) Pin) + TTC_GPIO_OFFSET_BSRR + 16 * 4  )  ) = 1

/** Set single output port bit to either logical high or Low.
 *
 * @param PortPin bit definition
 * @param Value   new logical state of output pin (==true: set to high; ==false: set to low)
 */
void gpio_stm32l1xx_put( e_ttc_gpio_pin PortPin, BOOL Value );
#define gpio_stm32l1xx_put(Pin, Value) *((volatile t_u8*) ( ((t_u32) Pin) + TTC_GPIO_OFFSET_ODR  )  ) = Value

/** creates numeric representing given GPIO-bank and pin number
 *
 * @param GPIOx  GPIO bank to use (-> ttc_gpio_types.h:t_ttc_gpio_bank)
 *               Note: GPIOx, Pin can be given as one argument by use of Pin_Pxn macro (TTC_GPIO_BANK_A, 7) = Pin_PA7
 * @param Pin    0..15
 * @return       32-bit value representing given GPIO-pin in a scalar datatype
 */
t_base gpio_stm32l1xx_create_index( t_ttc_gpio_bank GPIOx, t_u8 Pin );

/** restores GPIO-bank and pin number from 8-bit value
 *
 * @param PhysicalIndex  32-bit value representing GPIO-bank + pin
 * @param GPIOx          loaded with GPIO bank (-> ttc_gpio_types.h:t_ttc_gpio_bank)
 * @param Pin            loaded with pin number
 */
void gpio_stm32l1xx_from_index( t_base PhysicalIndex, e_gpio_stm32l1xx_bank* GPIOx, t_u8* Pin );

/** initializes single GPIO unit for operation
 * @param PortPin         port bit definition
 * @param Configuration  loaded with current configuration of given PortPin
 */
void gpio_stm32l1xx_get_configuration( volatile e_gpio_stm32l1xx_pin  PortPin, t_ttc_gpio_config* Configuration );

/** Prepares gpio driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void gpio_stm32l1xx_prepare();

/** initialize all pins of 16-bit wide parallel port at once
 *
 * Note: Runtime measured on STM32F107 @72MHz as < 3us
 *
 * @param Bank     GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 * @param GPIOx   =
 * @param Type    =
 * @param Speed   =
 */
void gpio_stm32l1xx_parallel16_init( t_ttc_gpio_bank GPIOx, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );

/** initialize 8 bit wide parallel port
 *
 * Note: Runtime measured on STM32F107 @72MHz as < 2.2us
 *
 * @param Bank+FirstPin  Bank and Pin given as one argument by use of Pin_Pxn macro: E.g. (GPIOA, 7) = Pin_PA7
 * @param Bank           GPIO bank to use (GPIO_BANKA, GPIO_BANKB, ...)
 * @param FirstPin       0..8  lowest of 8 consecutive pins to use
 * @param Type           mode to use for all 8 pins  (-> ttc_gpio_types.h:e_ttc_gpio_mode)
 * @param Speed          speed to use for all 8 pins (-> ttc_gpio_types.h:e_ttc_gpio_speed)
 * @param GPIOx   =
 */
void gpio_stm32l1xx_parallel08_init( t_ttc_gpio_bank GPIOx, t_u8 FirstPin, e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );

/** Changes the mapping of the specified pin.
 *
 * @param PortPin       Port bit definition
 * @param AlternateFunction     Selects the pin to used as Alternat function.
 */
void gpio_stm32l1xx_alternate_function( e_gpio_stm32l1xx_pin PortPin, e_ttc_gpio_alternate_function AlternateFunction );


//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _gpio_stm32l1xx_foo(t_ttc_gpio_config* Config)
t_u8 _gpio_stm32l1xx_compile_port_config( e_ttc_gpio_mode Mode, e_ttc_gpio_speed Speed );
void _gpio_stm32l1xx_bank_enable( t_ttc_gpio_bank GPIOx );
void _gpio_stm32l1xx_bank_disable( t_ttc_gpio_bank GPIOx );
//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //ARCHITECTURE_GPIO_H
