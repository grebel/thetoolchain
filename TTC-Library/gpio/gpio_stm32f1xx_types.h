#ifndef GPIO_STM32F1XX_TYPES_H
#define GPIO_STM32F1XX_TYPES_H

/** { gpio_stm32f1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for GPIO devices on stm32f1xx architectures.
 *  Structures, Enums and Defines being required by ttc_gpio_types.h
 *
 *  Created from template device_architecture_types.h revision 20 at 20140205 06:29:17 UTC
 *
 *  Note: See ttc_gpio.h for description of architecture independent GPIO implementation.
 *
 *  Authors: Gregor Rebel
 *
}*/

#define USE_STDPERIPH_DRIVER
#include "additionals/270_CPAL/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h"
#include "../register/register_stm32f1xx_types.h"

//{ Defines/ TypeDefs ******************************************************

#define t_ttc_gpio_bank      e_gpio_stm32f1xx_bank // datatype used to identify GPIO banks
#define TTC_GPIO_MAX_AMOUNT  7            // amount of gpio banks (A..G)
#define TTC_GPIO_MAX_PINS    16           // amount of port pins per bank
#define t_ttc_gpio_register  t_register_stm32f1xx_gpio*

/** bitband address calculation
 *
 * Each GPIO+Pin pair is identified by the corresponding bit band address of bit Pin in GPIO base register.
 * The offsets below have to be added to t_ttc_gpio to get the bitband address for individual register operations.
 */
//X #define t_ttc_gpio           e_ttc_gpio_pin  // bitband address of single Portbit in GPIO register base
#define TTC_GPIO_OFFSET_CRL  (0 * 4 * 32)      // adddress offset of bitband address for register CRL
#define TTC_GPIO_OFFSET_CRH  (1 * 4 * 32)      // adddress offset of bitband address for register CRH
#define TTC_GPIO_OFFSET_IDR  (2 * 4 * 32)      // adddress offset of bitband address for register IDR
#define TTC_GPIO_OFFSET_ODR  (3 * 4 * 32)      // adddress offset of bitband address for register ODR
#define TTC_GPIO_OFFSET_BSRR (4 * 4 * 32)      // adddress offset of bitband address for register BSRR
#define TTC_GPIO_OFFSET_BRR  (5 * 4 * 32)      // adddress offset of bitband address for register BRR
#define TTC_GPIO_OFFSET_LCKR (6 * 4 * 32)      // adddress offset of bitband address for register LCKR

/** Parallel Port Definitions
 *
 * Parallel Ports make use of GPIO banks.
 * All pins of each GPIO bank can be set/ read in parallel.
 *
 */
#define TTC_GPIO_BANK_A E_ttc_gpio_bank_a
#define TTC_GPIO_BANK_B E_ttc_gpio_bank_b
#define TTC_GPIO_BANK_C E_ttc_gpio_bank_c
#define TTC_GPIO_BANK_D E_ttc_gpio_bank_d
#define TTC_GPIO_BANK_E E_ttc_gpio_bank_e
#ifdef GPIOF
    #define TTC_GPIO_BANK_F E_ttc_gpio_bank_f
#endif
#ifdef GPIOG
    #define TTC_GPIO_BANK_G E_ttc_gpio_bank_g
#endif
#ifdef GPIOH
    #define TTC_GPIO_BANK_H E_ttc_gpio_bank_h
#endif

#ifndef TTC_GPIO1   // device not defined in makefile
    #define TTC_GPIO1    E_ttc_gpio_architecture_stm32f1xx   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)


//}Structures/ Enums
//{ Architecture-Defines (required by ttc_gpio_types.h)

typedef enum {
    /** Address of each port bit is calculated as bitband address of corresponding bit in GPIO Base register.
     *  This makes it very easy to calculate bitband address of BSRR, BRR, IDR and ODR addresses for fast
     *  port bit operations.
     *  Using an enum for register addresses makes debugging with GDB a lot easier.
     *  See implementation of gpio_stm32f1xx_set() for details.
     */
    E_ttc_gpio_pin_none = 0, // item mandatory for each architecture

    E_ttc_gpio_pin_adc_vref        = 1, // virtual pin for ADC Vref selection
    E_ttc_gpio_pin_adc_temp = 2, // virtual pin for ADC Temperature sensor selection

#if TTC_GPIO_MAX_AMOUNT > 0
    E_ttc_gpio_pin_a0     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_a1     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_a2     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_a3     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_a4     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_a5     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_a6     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_a7     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_a8     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_a9     = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_a10    = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_a11    = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_a12    = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_a13    = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_a14    = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_a15    = ( 0x42000000 + 32 * ( GPIOA_BASE - 0x40000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 1
    E_ttc_gpio_pin_b0     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_b1     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_b2     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_b3     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_b4     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_b5     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_b6     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_b7     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_b8     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_b9     = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_b10    = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_b11    = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_b12    = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_b13    = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_b14    = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_b15    = ( 0x42000000 + 32 * ( GPIOB_BASE - 0x40000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 2
    E_ttc_gpio_pin_c0     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_c1     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_c2     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_c3     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_c4     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_c5     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_c6     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_c7     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_c8     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_c9     = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_c10    = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_c11    = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_c12    = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_c13    = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_c14    = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_c15    = ( 0x42000000 + 32 * ( GPIOC_BASE - 0x40000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 3
    E_ttc_gpio_pin_d0     = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_d1     = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_d2     = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_d3     = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_d4     = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_d5     = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_d6     = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_d7     = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_d8     = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_d9     = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_d10    = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_d11    = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_d12    = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_d13    = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_d14    = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_d15    = ( 0x42000000 + 32 * ( GPIOD_BASE - 0x40000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 4
    E_ttc_gpio_pin_e0     = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_e1     = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_e2     = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_e3     = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_e4     = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_e5     = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_e6     = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_e7     = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_e8     = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_e9     = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_e10    = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_e11    = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_e12    = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_e13    = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_e14    = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_e15    = ( 0x42000000 + 32 * ( GPIOE_BASE - 0x40000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 5
    E_ttc_gpio_pin_f0     = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_f1     = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_f2     = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_f3     = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_f4     = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_f5     = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_f6     = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_f7     = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_f8     = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_f9     = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_f10    = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_f11    = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_f12    = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_f13    = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_f14    = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_f15    = ( 0x42000000 + 32 * ( GPIOF_BASE - 0x40000000 ) ) + 15 * 4,
#endif
#if TTC_GPIO_MAX_AMOUNT > 6
    E_ttc_gpio_pin_g0     = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 0  * 4,
    E_ttc_gpio_pin_g1     = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 1  * 4,
    E_ttc_gpio_pin_g2     = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 2  * 4,
    E_ttc_gpio_pin_g3     = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 3  * 4,
    E_ttc_gpio_pin_g4     = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 4  * 4,
    E_ttc_gpio_pin_g5     = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 5  * 4,
    E_ttc_gpio_pin_g6     = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 6  * 4,
    E_ttc_gpio_pin_g7     = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 7  * 4,
    E_ttc_gpio_pin_g8     = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 8  * 4,
    E_ttc_gpio_pin_g9     = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 9  * 4,
    E_ttc_gpio_pin_g10    = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 10 * 4,
    E_ttc_gpio_pin_g11    = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 11 * 4,
    E_ttc_gpio_pin_g12    = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 12 * 4,
    E_ttc_gpio_pin_g13    = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 13 * 4,
    E_ttc_gpio_pin_g14    = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 14 * 4,
    E_ttc_gpio_pin_g15    = ( 0x42000000 + 32 * ( GPIOG_BASE - 0x40000000 ) ) + 15 * 4,
#endif

    E_ttc_gpio_pin_unknown
} e_gpio_stm32f1xx_pin;


typedef enum { //
    /** Note: stm32f1xx allows only partial remapping of alternate functions.
     *        Selecting a function that is not supported by GPIO pin will assert.
     *        Check RM0008 p.179 for details!
     */
    E_ttc_gpio_alternate_function_System,     // pin is used as softwar controlled GPIO

    E_ttc_gpio_alternate_function_TIM2,       // use GPIO for TIMER2
    E_ttc_gpio_alternate_function_TIM3,       // use GPIO for TIMER3
    E_ttc_gpio_alternate_function_TIM4,       // use GPIO for TIMER4
    E_ttc_gpio_alternate_function_TIM5,       // use GPIO for TIMER5


    E_ttc_gpio_alternate_function_I2C1,       //
    E_ttc_gpio_alternate_function_I2C2,       //

    E_ttc_gpio_alternate_function_SPI1,       //
    E_ttc_gpio_alternate_function_SPI2,       //
    E_ttc_gpio_alternate_function_SPI3,       //

    E_ttc_gpio_alternate_function_USART1,     //
    E_ttc_gpio_alternate_function_USART2,     //
    E_ttc_gpio_alternate_function_USART3,     //

    E_ttc_gpio_alternate_function_UART4,      //
    E_ttc_gpio_alternate_function_USART4 = E_ttc_gpio_alternate_function_UART4,
    E_ttc_gpio_alternate_function_UART5,      //
    E_ttc_gpio_alternate_function_USART5 = E_ttc_gpio_alternate_function_UART5,

    E_ttc_gpio_alternate_function_USB,        //

    E_ttc_gpio_alternate_function_LCD,        //

    E_ttc_gpio_alternate_function_5FSMC,      //

    E_ttc_gpio_alternate_function_RI,         //

    E_ttc_gpio_alternate_function_EVENTOUT,   //

    E_ttc_gpio_alternate_function_unknown

} e_gpio_stm32f1xx_alternate_function;

//InsertEnums above (DO NOT REMOVE THIS LINE!)

#define e_ttc_gpio_pin                e_gpio_stm32f1xx_pin
#define e_ttc_gpio_alternate_function e_gpio_stm32f1xx_alternate_function


//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes ***************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_gpio_types.h ***********************

typedef enum {
    E_ttc_gpio_bank_none,
    E_ttc_gpio_bank_a = ( t_u32 ) GPIOA,
    E_ttc_gpio_bank_b = ( t_u32 ) GPIOB,
    E_ttc_gpio_bank_c = ( t_u32 ) GPIOC,
    E_ttc_gpio_bank_d = ( t_u32 ) GPIOD,
    E_ttc_gpio_bank_e = ( t_u32 ) GPIOE,
    E_ttc_gpio_bank_f = ( t_u32 ) GPIOF,
    E_ttc_gpio_bank_g = ( t_u32 ) GPIOG,
#ifdef GPIOH
    E_ttc_gpio_bank_g = ( t_u32 ) GPIOH,
#endif
    E_ttc_gpio_bank_unknown
} e_gpio_stm32f1xx_bank;

typedef enum { // port CNFy+MODEy register values (-> RM0008 p.167)
    E_gpio_stm32f1xx_mode_input_analog                      = 0b0000, // no pull-up or -down resistor active
    E_gpio_stm32f1xx_mode_input_floating                    = 0b0100,
    E_gpio_stm32f1xx_mode_input_pull_updown                 = 0b1000,
    E_gpio_stm32f1xx_mode_input_reserved                    = 0b1100,

    E_gpio_stm32f1xx_mode_output_push_pull_10mhz            = 0b0001, // no pull-up or -down resistor active
    E_gpio_stm32f1xx_mode_output_push_pull_2mhz             = 0b0010, // no pull-up or -down resistor active
    E_gpio_stm32f1xx_mode_output_push_pull_50mhz            = 0b0011, // no pull-up or -down resistor active
    E_gpio_stm32f1xx_mode_output_open_drain_10mhz           = 0b0101,
    E_gpio_stm32f1xx_mode_output_open_drain_2mhz            = 0b0110,
    E_gpio_stm32f1xx_mode_output_open_drain_50mhz           = 0b0111,
    E_gpio_stm32f1xx_mode_output_alternate_push_pull_10mhz  = 0b1001,
    E_gpio_stm32f1xx_mode_output_alternate_push_pull_2mhz   = 0b1010,
    E_gpio_stm32f1xx_mode_output_alternate_push_pull_50mhz  = 0b1011,
    E_gpio_stm32f1xx_mode_output_alternate_open_drain_10mhz = 0b1101,
    E_gpio_stm32f1xx_mode_output_alternate_open_drain_2mhz  = 0b1110,
    E_gpio_stm32f1xx_mode_output_alternate_open_drain_50mhz = 0b1111,

    E_gpio_stm32f1xx_mode_unknown
} e_gpio_stm32f1xx_mode;

typedef struct {  // stm32f1xx specific configuration data of single GPIO device
    e_gpio_stm32f1xx_bank Bank;
    e_gpio_stm32f1xx_mode ModeSpeed; // port mode = CNFy+MODEy
} __attribute__( ( __packed__ ) ) t_gpio_stm32f1xx_values;

// t_ttc_gpio_values is required by ttc_gpio_types.h
#define t_ttc_gpio_values t_gpio_stm32f1xx_values

//} Structures/ Enums


#endif //GPIO_STM32F1XX_TYPES_H
