#ifndef TTC_GFX_TYPES_H
#define TTC_GFX_TYPES_H

#include "ttc_basic.h"
#include "ttc_font_types.h"

#ifdef EXTENSION_400_lcd_320x240_ili9320
#  include "gfx/lcd_ili9320.h"
#endif

//X //24 to 16 Bit RGB converter Definition
//X #define RGB24TO16(C) ((u16_t) (((C)>>3&0x1F) | ((C)>>5&0x7E0) | ((C)>>8&0xF800)))

// Static Configuration **************************************

// TTC_GFXn_DRIVER has to be defined as constant by makefile.100_board_*
#ifdef TTC_GFX5_DRIVER
  #ifndef TTC_GFX4_DRIVER
    #error TTC_GFX5_DRIVER is defined, but not TTC_GFX4_DRIVER - all lower TTC_GFXn_DRIVER must be defined!
  #endif
  #ifndef TTC_GFX3_DRIVER
    #error TTC_GFX5_DRIVER is defined, but not TTC_GFX3_DRIVER - all lower TTC_GFXn_DRIVER must be defined!
  #endif
  #ifndef TTC_GFX2_DRIVER
    #error TTC_GFX5_DRIVER is defined, but not TTC_GFX2_DRIVER - all lower TTC_GFXn_DRIVER must be defined!
  #endif
  #ifndef TTC_GFX1_DRIVER
    #error TTC_GFX5_DRIVER is defined, but not TTC_GFX1_DRIVER - all lower TTC_GFXn_DRIVER must be defined!
  #endif

  #define TTC_AMOUNT_GFXS 5
#else
  #ifdef TTC_GFX4_DRIVER
    #define TTC_AMOUNT_GFXS 4

    #ifndef TTC_GFX3_DRIVER
      #error TTC_GFX5_DRIVER is defined, but not TTC_GFX3_DRIVER - all lower TTC_GFXn_DRIVER must be defined!
    #endif
    #ifndef TTC_GFX2_DRIVER
      #error TTC_GFX5_DRIVER is defined, but not TTC_GFX2_DRIVER - all lower TTC_GFXn_DRIVER must be defined!
    #endif
    #ifndef TTC_GFX1_DRIVER
      #error TTC_GFX5_DRIVER is defined, but not TTC_GFX1_DRIVER - all lower TTC_GFXn_DRIVER must be defined!
    #endif
  #else
    #ifdef TTC_GFX3_DRIVER

      #ifndef TTC_GFX2_DRIVER
        #error TTC_GFX5_DRIVER is defined, but not TTC_GFX2_DRIVER - all lower TTC_GFXn_DRIVER must be defined!
      #endif
      #ifndef TTC_GFX1_DRIVER
        #error TTC_GFX5_DRIVER is defined, but not TTC_GFX1_DRIVER - all lower TTC_GFXn_DRIVER must be defined!
      #endif

      #define TTC_AMOUNT_GFXS 3
    #else
      #ifdef TTC_GFX2_DRIVER

        #ifndef TTC_GFX1_DRIVER
          #error TTC_GFX5_DRIVER is defined, but not TTC_GFX1_DRIVER - all lower TTC_GFXn_DRIVER must be defined!
        #endif

        #define TTC_AMOUNT_GFXS 2
      #else
        #ifdef TTC_GFX1_DRIVER
          #define TTC_AMOUNT_GFXS 1
        #else
          #define TTC_AMOUNT_GFXS 0
        #endif
      #endif
    #endif
  #endif
#endif

#ifndef TTC_ASSERT_GFX    // any previous definition set (Makefile)?
#define TTC_ASSERT_GFX 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_GFX == 1)  // use Assert()s in GFX code (somewhat slower but alot easier to debug)
  #define Assert_GFX(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in GFX code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_GFX(Condition, ErrorCode)
#endif

#ifndef TTC_GFX_SINGLE_THREADED // == 1: functions can use static variables to speed up function calls + reduce stack usage
#define TTC_GFX_SINGLE_THREADED 1
#endif
#if (TTC_GFX_SINGLE_THREADED == 1)
  #define GFX_STATIC static
#else
  #define GFX_STATIC
#endif

#define ABS(a) ((a)<0 ? -(a) : (a))

// Color defines *********************************************

#define GFX_COLOR24_BLACK   0x000000
#define GFX_COLOR24_SILVER  0xC0C0C0
#define GFX_COLOR24_GRAY    0x808080
#define GFX_COLOR24_WHITE   0xFFFFFF
#define GFX_COLOR24_MAROON  0x800000
#define GFX_COLOR24_RED     0xFF0000
#define GFX_COLOR24_PURPLE  0x800080
#define GFX_COLOR24_FUCHSIA 0xFF00FF
#define GFX_COLOR24_GREEN   0x008000
#define GFX_COLOR24_LIME    0x00FF00
#define GFX_COLOR24_OLIVE   0x808000
#define GFX_COLOR24_YELLOW  0xFFFF00
#define GFX_COLOR24_NAVY    0x000080
#define GFX_COLOR24_BLUE    0x0000FF
#define GFX_COLOR24_TEAL    0x008080
#define GFX_COLOR24_AQUA    0x00FFFF

// Structures ************************************************

typedef union {
/** DEPRECATED
#ifdef EXTENSION_400_lcd_320x240_ili9320
    lcd_ili9320_config_t ILI9320;
#endif
*/
    u8_t None;
} ttc_gfx_architecture_u;

typedef enum {
    tgd_None,
    tgd_ILI9320, // -> gfx/lcd_ili9320.h (display driver on Mini-STM32 prototype board)
    tgd_K320QVD, // -> gfx/lcd_k320qvb.h (display driver on ARM Olimex LCD prototype board)
    tgd_Invalid
} ttc_gfx_driver_e;

typedef struct ttc_gfx_generic_s { // architecture independent configuration data of single display

    // Note: Write-access to this structure is only allowed before first ttc_gfx_init() call!
    //       After initialization, only read accesses are allowed for high-level code.

    u8_t LogicalIndex;               // automatically set: logical index of display to use (1 = TTC_GFX1, ...)

    union  {
        u32_t All;
        struct {
            // Bits
            unsigned Reserved1        : 32; // pad to 32 bits
        } Bits;
    } Flags;

    // dimensions of display
    u16_t Width;
    u16_t Height;
    u16_t Depth;

    u16_t CursorX;          // X-coordinate of current drawing position for pixel operations
    u16_t CursorY;          // Y-coordinate of current drawing position for pixel operations

    u16_t  ColorFg;         // current 16-bit color
    u16_t  ColorBg;         // current 16-bit background color
    u16_t* Palette;         // current palette
    u16_t  PaletteSize;     // size of current palette

    u16_t TextX;            // X-coordinate of current drawing position for text operations
    u16_t TextY;            // Y-coordinate of current drawing position for text operations
    s16_t TextMinX;         // pixel x-coordinate where TextX == 0
    s16_t TextMinY;         // pixel y-coordinate where TextY == 0
    u16_t TextColumns;      // amount of characters of current font that fit in one row
    u16_t TextRows;         // amount of characters of current font that fit in one column

    // type of driver used for this display
    ttc_gfx_driver_e Driver;

    // architecture dependend display configuration
    ttc_gfx_architecture_u GFX_Arch;

    // functions being provided by low-level driver
    // Note: These are set during ttc_gfx_reset()

    // functions that must be provided by low-level driver (set during ttc_gfx_reset() )
    void (*function_set_color_fg24)(u32_t Color);
    void (*function_set_color_bg24)(u32_t Color);

    // functions that can be provided by low-level driver (set during ttc_gfx_reset() )
    void (*function_circle)(u16_t CenterX, u16_t CenterY, u16_t Radius);
    void (*function_circle_segment)(u16_t CenterX, u16_t CenterY, u16_t Radius, u16_t StartAngle, u16_t EndAngle);
    void (*function_circle_fill)(u16_t CenterX, u16_t CenterY, u16_t Radius);
    void (*function_clear)();
    void (*function_line)(u16_t X1, u16_t Y1, u16_t X2, u16_t Y2);
    void (*function_line_horizontal)(u16_t X, u16_t Y, s16_t Length);
    void (*function_line_vertical)(u16_t X, u16_t Y, s16_t Length);
    void (*function_rect)(u16_t X,u16_t Y, s16_t Width, s16_t Height);
    void (*function_rect_fill)(u16_t X, u16_t Y, s16_t Width, s16_t Height);
    void (*function_set_backlight)(u8_t Level);

#ifdef EXTENSION_510_ttc_font  // font attributes
    // attributes for text operations
    const ttc_font_data_t* Font;  // !=NULL: current font used for text-operations

    void (*function_put_char)(u8_t Char);
    void (*function_put_char_solid)(u8_t Char);
#endif

} __attribute__((__packed__)) ttc_gfx_generic_t;

#endif // TTC_GFX_TYPES_H
