/** { ttc_can_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for CAN device.
 *  Structures, Enums and Defines being required by both, high- and low-level can.
 *
 *  Created from template ttc_device_types.h revision 22 at 20140322 11:01:08 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_CAN_TYPES_H
#define TTC_CAN_TYPES_H

//{ Includes *************************************************************

#include "compile_options.h"
#ifdef EXTENSION_can_stm32f1xx
    #include "can/can_stm32f1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif

#include "ttc_basic_types.h"
#include "ttc_queue_types.h"
#include "ttc_memory_types.h"

//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************
#define TTC_NO_MAILBOX -1

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

// TTC_CANn has to be defined as constant by makefile.100_board_*
#ifdef TTC_CAN2
    #ifndef TTC_CAN1
        #error TTC_CAN2 is defined, but not TTC_CAN1 - all lower TTC_CANn must be defined!
    #endif

    #define TTC_CAN_AMOUNT 2
#else
    #ifdef TTC_CAN1
        #define TTC_CAN_AMOUNT 1
    #else
        #define TTC_CAN_AMOUNT 0
    #endif
#endif

/* Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_CAN 0  # disable assert handling for can devices
 *
 */
#ifndef TTC_ASSERT_CAN    // any previous definition set (Makefile)?
    #define TTC_ASSERT_CAN 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_CAN == 1)  // use Assert()s in CAN code (somewhat slower but alot easier to debug)
    #define Assert_CAN(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in CAN code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_CAN(Condition, ErrorCode)
#endif

/** Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_can_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_can_architecture
    #warning Missing low-level definition for t_ttc_can_architecture (using default)
    #define t_ttc_can_architecture void
#endif

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_can_physical_index (starts at zero!)
    //
    // You may use this enum to assign physical index to logical TTC_CANn constants in your makefile
    //
    // E.g.: Defining physical device #3 of can as first logical device TTC_CAN1
    //       COMPILE_OPTS += -DTTC_CAN1=INDEX_CAN3
    //
    INDEX_CAN1,
    INDEX_CAN2,
    // add more if needed

    INDEX_CAN_ERROR
} e_ttc_can_physical_index;
typedef enum {    // e_ttc_can_errorcode     return codes of CAN devices
    ec_can_OK = 0,

    // other warnings go here..
    ec_can_ERROR,                  // general failure
    ec_can_NULL,                   // NULL pointer not accepted
    ec_can_DeviceNotFound,         // corresponding device could not be found
    ec_can_InvalidImplementation,  // internal self-test failed
    ec_can_InvalidConfiguration,   // sanity check of device configuration failed
    ec_can_StuffError,             // Stuff Error
    ec_can_FormError,              // Form Error
    ec_can_ACKError,               // Acknowledgment Error
    ec_can_BitRecessiveError,      // Bit Recessive Error
    ec_can_BitDominantError,       // Bit Dominant Error
    ec_can_CRCError,               // CRC Error
    ec_can_SoftwareSetError,       // Software Set Error
    ec_can_TX_NoMailBox,           // CAN cell did not provide an empty mailbox
    ec_can_FilterError,            // CAN Filter are completely activated

    // CAN Sleep Warnings
    ec_can_Sleep_OK,               // CAN entered the sleep mode
    ec_can_Sleep_Failed,           // CAN did not enter the sleep mode

    // CAN Wake-up Warnings
    ec_can_WakeUp_OK,              // CAN leaved the sleep mode
    ec_can_WakeUP_Failed,          // CAN did not leave the sleep mode

    // other failures go here..
    ec_can_unknown                // no valid errorcodes past this entry
} e_ttc_can_errorcode;
typedef enum {    // e_ttc_can_errorcode     return codes of CAN devices
    // CAN Transmission Warnings
    tx_status_Failed,              // CAN Transmission Failed
    tx_status_OK,                  // CAN Transmission OK
    tx_status_Pending             // CAN Transmission pending
} e_ttc_can_tx_status;
typedef enum {    // e_ttc_can_architecture  types of architectures supported by CAN driver
    ta_can_None,           // no architecture selected

    ta_can_stm32f1xx, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_can_unknown        // architecture not supported
} e_ttc_can_architecture;
typedef enum {   // e_ttc_can_bit_segment1 (starts at zero!)
    bs1_can_1tq,
    bs1_can_2tq,
    bs1_can_3tq,
    bs1_can_4tq,
    bs1_can_5tq,
    bs1_can_6tq,
    bs1_can_7tq,
    bs1_can_8tq,
    bs1_can_9tq,
    bs1_can_10tq,
    bs1_can_11tq,
    bs1_can_12tq,
    bs1_can_13tq,
    bs1_can_14tq,
    bs1_can_15tq,
    bs1_can_16tq
} e_ttc_can_bit_segment1;
typedef enum {   // e_ttc_can_bit_segment2 (starts at zero!)
    bs2_can_1tq,
    bs2_can_2tq,
    bs2_can_3tq,
    bs2_can_4tq,
    bs2_can_5tq,
    bs2_can_6tq,
    bs2_can_7tq,
    bs2_can_8tq
} e_ttc_can_bit_segment2;
typedef struct s_ttc_can_filter { //         filter independent configuration data
    t_u16 FilterAddress;
    t_u16 IdHigh;
    t_u16 IdLow;
    t_u16 MaskIdHigh;
    t_u16 MaskIdLow;
    union  { // generic configuration bits common for all low-level Drivers
        t_u16 All;
        struct {
            unsigned FifoAssign        : 1;  // Specifies the FIFO (0 or 1) which will be assigned to the filter
            unsigned FilterNumber      : 5;  // Specifies the filter which will be initialized. It ranges from 0 to 13 for STM32F10X, otherwise from 0 to 27
            unsigned FilterMode        : 1;  // Specifies the filter mode to be initialized : ==0: IdMask Mode; ==1: IdList Mode
            unsigned FilterScale       : 1;  // Specifies the filter scale : ==0: Two 16-bits filters; ==1: One 32-bit filter
            unsigned FilterActivation  : 1;  // Enable or disable the filter

            unsigned Reserved1         : 7; // pad to 16 bits
        } Bits;
    } Flags;

} t_ttc_can_filter;
typedef struct s_ttc_can_message { //         message independent configuration data
    t_u32 StdId;                           // Standard identifier
    t_u32 ExtId;                           // Extended identifier
    union  { // generic configuration bits common for all low-level Drivers
        t_u8 All;
        struct {
            unsigned IDE               : 1;  // Type of identifier for the message : ==0: Standard ID; ==1: (Should be casted to 0x04) Extended ID
            unsigned RTR               : 1;  // Type of frame for the message : ==0: Data Frame; ==1: (Should be casted to 0x02) Remote Frame
            unsigned DLC               : 4;  // Number of Data Bytes. Value between 0 to 8

            unsigned Reserved1         : 2;  // pad to 8 bits
        } Bits;
    } Flags;

    t_u32 Data[2];                         // Data
    t_u8 FMI;                              // Only used in Reception!! - Specifies the index of the filter the message stored in the mailbox passes through

} t_ttc_can_message;
typedef struct s_ttc_can_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_can_init()
    //       and after ttc_can_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    e_ttc_can_architecture Architecture; // type of architecture used for current can device
    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_CAN1, ...)
    t_u8  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)

    // low-level configuration (structure defined by low-level driver)
    t_ttc_can_architecture* LowLevelConfig;

    union  { // generic configuration bits common for all low-level Drivers
        t_u16 All;
        struct {
            unsigned Initialized        : 1;  // ==1: device has been initialized successfully
            // ToDo: additional high-level flags go here..
            unsigned Mode               : 2;  // ==0: Normal Mode; ==1: Loopback Mode; ==2: Silent Mode; ==3: Loopback + Silent Mode
            unsigned SJW                : 2;  // ==0: 1 Time Quantum; ==1: 2 Time Quantum; ==3: 3 Time Quantum; ==4: 4 Time Quantum
            unsigned TTCM               : 1;  // Time Triggered Communiaction Mode : ==0: Diable ; ==1: Enable
            unsigned ABOM               : 1;  // Automatic Bus-Off : ==0: Diable ; ==1: Enable
            unsigned AWUM               : 1;  // Automatic Wake-Up Mode : ==0: Diable ; ==1: Enable
            unsigned NART               : 1;  // No Automatic Retransmission Mode : ==0: Diable ; ==1: Enable
            unsigned RFLM               : 1;  // Receive FIFO Locked Mode : ==0: Diable ; ==1: Enable
            unsigned TXFP               : 1;  // Transmit FIFO Locked Mode : ==0: Diable ; ==1: Enable

            unsigned Layout             : 2;  // Pin Remapping for CANx Devices

            unsigned Reserved1          : 3; // pad to 16 bits
        } Bits;
    } Flags;

    // Additional high-level attributes go here..
    t_u16    Prescaler;               // symbol rate of connection
    t_u8     BS1;                     // Quantum in bit segment 1
    t_u8     BS2;                     // Quantum in bit segment 2

    // architecture dependend CAN configuration (each CAN requires its own!)
    t_ttc_can_architecture CAN_Arch;

    // message depended CAN configuration (each CAN requires Tx and Rx)
    t_ttc_can_message CAN_Tx_Message;
    t_ttc_can_message CAN_Rx_Message;

#ifndef STM32F10X_CL
    t_ttc_can_filter CAN_Filter[14];
#else
    t_ttc_can_filter CAN_Filter[28];
#endif

    // activity functions for byte receive
    // != NULL: registered function is called from _ttc_can_rx_isr() after single byte has been received
    // Note: Registered function is called directly from interrupt service routine!
    void ( *activity_rx_isr )( struct s_ttc_can_config* );
    // handlers of registered interrupts
    void* Interrupt_FIFO0;       // triggered when receive buffer has bytes
    void* Interrupt_FIFO1; // triggered after successfull transmission

} __attribute__( ( __packed__ ) ) t_ttc_can_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_CAN_TYPES_H
