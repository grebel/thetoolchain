#ifndef TTC_GFX_MT
#define TTC_GFX_MT
/*{ Template_FreeRTOS::.h ************************************************
}*/

//{ Includes *************************************************************
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "ttc_queue.h"
#include "ttc_task.h"
#include "ttc_gfx.h"

#define TTC_GFX_MT_QUEUE_SIZE 70

typedef struct s_ttc_gfx_mt_rect {
    t_u16 X;
    t_u16 Y;
    t_u16 Height;
    t_u16 Width;

}__attribute__((__packed__)) t_ttc_gfx_mt_rect;

typedef struct s_ttc_gfx_mt_circle {
    t_u16 Center_X;
    t_u16 Center_Y;
    t_u16 Radius;

}__attribute__((__packed__)) t_ttc_gfx_mt_circle;

typedef struct s_ttc_gfx_mt_line {
    t_u16 X1;
    t_u16 Y1;
    t_u16 X2;
    t_u16 Y2;

}__attribute__((__packed__)) t_ttc_gfx_mt_line;

typedef struct s_ttc_gfx_mt_clear {
}__attribute__((__packed__)) t_ttc_gfx_mt_clear;

typedef struct s_ttc_gfx_mt_string {
    t_u16 X;
    t_u16 Y;
    char* Text;
    t_base MaxSize;

}__attribute__((__packed__)) t_ttc_gfx_mt_string;

typedef struct s_ttc_gfx_mt_char {
    t_u16 X;
    t_u16 Y;
    t_u8 Char;

}__attribute__((__packed__)) t_ttc_gfx_mt_char;

typedef struct s_ttc_gfx_mt_color {
    t_u32 Color;

}__attribute__((__packed__)) t_ttc_gfx_mt_color;

typedef struct s_ttc_gfx_mt_position {
    t_u16 X;
    t_u16 Y;

}__attribute__((__packed__)) t_ttc_gfx_mt_position;

typedef struct ttc_gfx_mt_union_s{
    t_u8 Display;
    void* MultiTaskingFunction;
    union {
        t_ttc_gfx_mt_clear Clear;
        t_ttc_gfx_mt_line Line;
        t_ttc_gfx_mt_circle Circle;
        t_ttc_gfx_mt_rect Rect;
        t_ttc_gfx_mt_string String;
        t_ttc_gfx_mt_char Char;
        t_ttc_gfx_mt_color Color;
        t_ttc_gfx_mt_position Position;
    }Type;
} t_ttc_gfx_mt_union;

t_u8 ttc_gfx_mt_get_max_display_index();

t_ttc_gfx_generic* ttc_gfx_mt_get_configuration(t_u8 DisplayIndex);
/**
 * @brief ttc_gfx_mt_init    Initialization of the display and used queue
 * @param DisplayIndex       number of the initialized display
 */
void ttc_gfx_mt_init(t_u8 DisplayIndex);
/**
 * @brief ttc_gfx_mt_maintask  the task where all elements of the queue will be popped and executed
 * @param TaskArgument         no function
 */
void ttc_gfx_mt_maintask(void* TaskArgument);
/**
 * @brief ttc_gfx_mt_* the follwing function have the same properties like the ttc_gfx* functions with the difference, that they will be executed by the maintask
 */
void ttc_gfx_mt_clear();

void ttc_gfx_mt_rect(t_u16 X,t_u16 Y, t_s16 Width, t_s16 Height);

void ttc_gfx_mt_rect_fill(t_u16 X,t_u16 Y, t_s16 Width, t_s16 Height);

void ttc_gfx_mt_circle(t_u16 CenterX, t_u16 CenterY, t_u16 Radius);

void ttc_gfx_mt_circle_fill(t_u16 CenterX, t_u16 CenterY, t_u16 Radius);

void ttc_gfx_mt_line(t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2);

void ttc_gfx_mt_color_bg24(t_u32 Color);

void ttc_gfx_mt_color_fg24(t_u32 Color);

void ttc_gfx_mt_text_cursor_set(t_u16 X, t_u16 Y);

void ttc_gfx_mt_char_solid(t_u8 Char);

void ttc_gfx_mt_print(const char* Text, t_base MaxSize);

void ttc_gfx_mt_print_solid_at(t_u16 X, t_u16 Y, const char* Text, t_base MaxSize);

void ttc_gfx_mt_print_at(t_u16 X, t_u16 Y, const char* Text, t_base MaxSize);

void ttc_gfx_mt_print_between(t_u16 X, t_u16 Y, const char* Text, t_base MaxSize);

#endif



