/** { dma_stm32l1xx.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for dma devices on stm32l1xx architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150108 10:24:10 UTC
 *
 *  Note: See ttc_dma.h for description of stm32l1xx independent DMA implementation.
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "dma_stm32l1xx.h".
//
#include "dma_stm32l1xx.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

e_ttc_dma_errorcode dma_stm32l1xx_deinit(t_ttc_dma_config* Config) {
    Assert_DMA(Config, ttc_assert_origin_auto); // pointers must not be NULL

    /* Call to private function to change the register */
    e_ttc_dma_errorcode error = _dma_stm32l1xx_deinit(Config);

    if(error)
        return ec_dma_ERROR;

    /* everything is ok */
    return (e_ttc_dma_errorcode) 0;
}


e_ttc_dma_errorcode dma_stm32l1xx_get_features(t_ttc_dma_config* Config) {
    Assert_DMA(Config, ttc_assert_origin_auto); // pointers must not be NULL


    return (e_ttc_dma_errorcode) 0;
}


e_ttc_dma_errorcode dma_stm32l1xx_init(t_ttc_dma_config* Config) {

    Assert_DMA(Config, ttc_assert_origin_auto); // pointers must not be NULL

    /* Call to private function to change the register */
    e_ttc_dma_errorcode error = _dma_stm32l1xx_init(Config);

    if(error) /* Something is bad */
        return ec_dma_ERROR;

    /* Everything is ok */
    return (e_ttc_dma_errorcode) 0;
}


e_ttc_dma_errorcode dma_stm32l1xx_load_defaults(t_ttc_dma_config* Config) {


    Assert_DMA(Config, ttc_assert_origin_auto); // pointers must not be NULL

    /* Initialize the DMA_PeripheralBaseAddr member */
    Config->PeripheralBaseAddr = 0;

    /* Initialize the DMA_MemoryBaseAddr member */
    Config->MemoryBaseAddr = 0;

    /* Initialize the DMA_DIR member */
    Config->DIR = (t_u32)0x00000000;

    /* Initialize the DMA_BufferSize member */
    Config->BufferSize = 0;

    /* Initialize the DMA_PeripheralInc member */
    Config->PeripheralInc = (t_u32)0x00000000;;

    /* Initialize the DMA_MemoryInc member */
    Config->MemoryInc = (t_u32)0x00000000;;

    /* Initialize the DMA_PeripheralDataSize member */
    Config->PeripheralDataSize = (t_u32)0x00000000;;

    /* Initialize the DMA_MemoryDataSize member */
    Config->MemoryDataSize = (t_u32)0x00000000;;

    /* Initialize the DMA_Mode member */
    Config->Mode = (t_u32)0x00000000;;

    /* Initialize the DMA_Priority member */
    Config->Priority = (t_u32)0x00000000;;

    /* Initialize the DMA_M2M member */
    Config->M2M = (t_u32)0x00000000;

    t_ttc_dma_architecture* ConfigArch = Config->LowLevelConfig;

    if (ConfigArch == NULL) {
        ConfigArch = (t_ttc_dma_architecture*)ttc_heap_alloc_zeroed(sizeof(t_ttc_dma_architecture) );
        Config->LowLevelConfig = ConfigArch;
    }

    ConfigArch->BaseRegister = 0;


    return (e_ttc_dma_errorcode) 0;
}
void dma_stm32l1xx_prepare() {


}
void dma_stm32l1xx_reset(t_ttc_dma_config* Config) {
    Assert_DMA(Config, ttc_assert_origin_auto); // pointers must not be NULL    

}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

e_ttc_dma_errorcode _dma_stm32l1xx_deinit(t_ttc_dma_config* Config) {

    /* Disable the selected DMAy Channelx */
    (Config->LowLevelConfig->BaseRegister)->CCR &= (t_u16)(~0x0001);

    /* Reset DMAy Channelx control register */
    (Config->LowLevelConfig->BaseRegister)->CCR  = 0;

    /* Reset DMAy Channelx remaining bytes register */
    (Config->LowLevelConfig->BaseRegister)->CNDTR = 0;

    /* Reset DMAy Channelx peripheral address register */
    (Config->LowLevelConfig->BaseRegister)->CPAR  = 0;

    /* Reset DMAy Channelx memory address register */
    (Config->LowLevelConfig->BaseRegister)->CMAR = 0;

    return ec_dma_OK;

}

e_ttc_dma_errorcode _dma_stm32l1xx_init(t_ttc_dma_config* Config) {

    Assert_DMA(Config, ttc_assert_origin_auto); // pointers must not be NULL

    t_ttc_dma_architecture* Config_dma = (Config->LowLevelConfig);

    if (1) {

        switch (Config->Channel) {

            case 1:

                Config_dma->BaseRegister = (t_register_stm32l1xx_dma_channel*) DMA1_Channel1;
                break;

            case 2:

                Config_dma->BaseRegister = (t_register_stm32l1xx_dma_channel*) DMA1_Channel2;
                break;

            case 3:

                Config_dma->BaseRegister = (t_register_stm32l1xx_dma_channel*) DMA1_Channel3;
                break;

            case 4:

                Config_dma->BaseRegister = (t_register_stm32l1xx_dma_channel*) DMA1_Channel4;
                break;

            case 5:

                Config_dma->BaseRegister = (t_register_stm32l1xx_dma_channel*) DMA1_Channel5;
                break;

            case 6:

                Config_dma->BaseRegister = (t_register_stm32l1xx_dma_channel*) DMA1_Channel6;
                break;

            case 7:

                Config_dma->BaseRegister = (t_register_stm32l1xx_dma_channel*) DMA1_Channel7;
                break;

            default: Assert_DMA(0, ttc_assert_origin_auto);
                break; // No TTC_USARTn defined! Check your makefile.100_board_* file!
        }

    }

    sysclock_stm32l1xx_RCC_AHBPeriphClockCmd(0x01000000, ENABLE);

    t_u32 tmpreg = 0;


    /* Get the DMAy_Channelx CCR value */
    tmpreg = (Config_dma->BaseRegister)->CCR;

    /* Clear MEM2MEM, PL, MSIZE, PSIZE, MINC, PINC, CIRC and DIR bits */
    tmpreg &= 0xFFFF800F;

    tmpreg |= Config->DIR | Config->Mode | Config->PeripheralInc | Config->MemoryInc |Config->PeripheralDataSize | Config->MemoryDataSize | Config->Priority | Config->M2M;

    /* Write to DMAy Channelx CCR */
    (Config_dma->BaseRegister)->CCR = tmpreg;

    /*--------------------------- DMAy Channelx CNDTR Configuration ---------------*/
    /* Write to DMAy Channelx CNDTR */
    (Config_dma->BaseRegister)->CNDTR = Config->BufferSize;

    /*--------------------------- DMAy Channelx CPAR Configuration ----------------*/
    /* Write to DMAy Channelx CPAR */
    (Config_dma->BaseRegister)->CPAR = Config->PeripheralBaseAddr;

    /*--------------------------- DMAy Channelx CMAR Configuration ----------------*/
    /* Write to DMAy Channelx CMAR */
    (Config_dma->BaseRegister)->CMAR = Config->MemoryBaseAddr;

    (Config_dma->BaseRegister)->CCR |= DMA_CCR1_EN;

    return ec_dma_OK;

}

//}Private Functions