#ifndef DMA_STM32L1XX_H
#define DMA_STM32L1XX_H

/** { dma_stm32l1xx.h **********************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for dma devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by high-level dma and application.
 *
 *  Created from template device_architecture.h revision 22 at 20150108 10:24:10 UTC
 *
 *  Note: See ttc_dma.h for description of stm32l1xx independent DMA implementation.
 *  
 *  Authors: <Adrian Romero>
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)


//{EXTENSION_STATUS_TTC_DMA_STM32L1XX
//
// Implementation status of low-level driver support for dma devices on stm32l1xx
// Note: This block has been automatically inserted by InstallData/scripts/compile_ReadMe.TheToolChain.pl

#define EXTENSION_STATUS_TTC_DMA_STM32L1XX '+'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if (EXTENSION_STATUS_TTC_DMA_STM32L1XX == '?')
#  warning Missing low level support status. Please set value of EXTENSION_STATUS_TTC_DMA_STM32L1XX to one from '-', 'o', '+' 
#endif

//}EXTENSION_STATUS_TTC_DMA_STM32L1XX

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "dma_stm32l1xx.c"
//
#include "dma_stm32l1xx_types.h"
#include "../ttc_dma_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_dma_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_dma_foo
//
#define ttc_driver_dma_deinit(Config) dma_stm32l1xx_deinit(Config)
#define ttc_driver_dma_get_features(Config) dma_stm32l1xx_get_features(Config)
#define ttc_driver_dma_init(Config) dma_stm32l1xx_init(Config)
#define ttc_driver_dma_load_defaults(Config) dma_stm32l1xx_load_defaults(Config)
#define ttc_driver_dma_prepare() dma_stm32l1xx_prepare()
#define ttc_driver_dma_reset(Config) dma_stm32l1xx_reset(Config)
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_dma.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_dma.h for details.
 *
 * You normally should not need to add functions here. 
 * Add your private functions in the private section below. 
 */


/** shutdown single DMA unit device
 * @param Config        pointer to struct t_ttc_dma_config (must have valid value for PhysicalIndex)
 * @return              == 0: DMA has been shutdown successfully; != 0: error-code
 */
e_ttc_dma_errorcode dma_stm32l1xx_deinit(t_ttc_dma_config* Config);


/** fills out given Config with maximum valid values for indexed DMA
 * @param Config        = pointer to struct t_ttc_dma_config (must have valid value for PhysicalIndex)
 * @return  == 0:       *Config has been initialized successfully; != 0: error-code
 */
e_ttc_dma_errorcode dma_stm32l1xx_get_features(t_ttc_dma_config* Config);


/** initializes single DMA unit for operation
 * @param Config        pointer to struct t_ttc_dma_config (must have valid value for PhysicalIndex)
 * @return              == 0: DMA has been initialized successfully; != 0: error-code
 */
e_ttc_dma_errorcode dma_stm32l1xx_init(t_ttc_dma_config* Config);


/** loads configuration of indexed DMA unit with default values
 * @param Config        pointer to struct t_ttc_dma_config (must have valid value for PhysicalIndex)
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_dma_errorcode dma_stm32l1xx_load_defaults(t_ttc_dma_config* Config);


/** Prepares dma Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void dma_stm32l1xx_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        pointer to struct t_ttc_dma_config (must have valid value for PhysicalIndex)
 */
void dma_stm32l1xx_reset(t_ttc_dma_config* Config);

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _dma_stm32l1xx_foo(t_ttc_dma_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

e_ttc_dma_errorcode _dma_stm32l1xx_init(t_ttc_dma_config* Config);

e_ttc_dma_errorcode _dma_stm32l1xx_deinit(t_ttc_dma_config* Config);

//}PrivateFunctions

#endif //DMA_STM32L1XX_H