#ifndef DMA_STM32L1XX_TYPES_H
#define DMA_STM32L1XX_TYPES_H

/** { dma_stm32l1xx.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *                     
 *  Low-Level datatypes for DMA devices on stm32l1xx architectures.
 *  Structures, Enums and Defines being required by ttc_dma_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150108 10:24:10 UTC
 *
 *  Note: See ttc_dma.h for description of architecture independent DMA implementation.
 * 
 *  Authors: <Adrian Romero>
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
#include "../ttc_register.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

#define TTC_NUMBER_OF_CHANNELS 7

//} Includes
//{ Structures/ Enums required by ttc_dma_types.h *************************

typedef struct { // register description (adapt according to stm32l1xx registers)
  unsigned Reserved1 : 16; 
  unsigned Reserved2 : 16; 
} t_dma_register;

typedef struct {  // stm32l1xx specific configuration data of single DMA device

    t_register_stm32l1xx_dma_channel* BaseRegister;       // base address of DMA device registers

} __attribute__((__packed__)) t_dma_stm32l1xx_config;

// t_ttc_dma_architecture is required by ttc_dma_types.h
#define t_ttc_dma_architecture t_dma_stm32l1xx_config

//} Structures/ Enums


#endif //DMA_STM32L1XX_TYPES_H
