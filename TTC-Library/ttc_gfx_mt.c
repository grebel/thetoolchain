/*{ ttc_gfx_mt.c *******************************
ttc_gfx_mt is created for using the ttc_gfx from different tasks, all functions calls are pushed and popped in and from a queue


}*/

#include "ttc_gfx_mt.h"

//{ Global Variables *****************************************************

// updated by _ttc_gfx_update_configuration()

t_ttc_queue_generic ttc_gfx_mt_queue_handle;
static t_ttc_gfx_mt_union ttc_gfx_mt_current_function;



void ttc_gfx_mt_init(t_u8 DisplayIndex) {



    ttc_task_create(ttc_gfx_mt_maintask,
                    "Gfx Multitaskting Maintask",
                    (sizeof(t_ttc_gfx_mt_union)*TTC_GFX_MT_QUEUE_SIZE+128),
                    NULL,
                    1,
                    NULL);
     ttc_gfx_init(DisplayIndex);

}

void ttc_gfx_mt_maintask(void* TaskArgument){

    ttc_gfx_mt_queue_handle = ttc_queue_generic_create(TTC_GFX_MT_QUEUE_SIZE,sizeof(t_ttc_gfx_mt_union));

    while(1){

        for(t_u8 i = 0;ttc_queue_amount_waiting(ttc_gfx_mt_queue_handle)&&i<20;i++){
            ttc_queue_generic_pull_front(ttc_gfx_mt_queue_handle,(void*) &ttc_gfx_mt_current_function,1000);
            if(ttc_gfx_mt_current_function.MultiTaskingFunction==function_clear)
                function_clear();
            if(ttc_gfx_mt_current_function.MultiTaskingFunction==function_rect)
                function_rect(ttc_gfx_mt_current_function.Type.Rect.X,ttc_gfx_mt_current_function.Type.Rect.Y,ttc_gfx_mt_current_function.Type.Rect.Width,ttc_gfx_mt_current_function.Type.Rect.Height);
            if(ttc_gfx_mt_current_function.MultiTaskingFunction==function_rect_fill)
                function_rect_fill(ttc_gfx_mt_current_function.Type.Rect.X,ttc_gfx_mt_current_function.Type.Rect.Y,ttc_gfx_mt_current_function.Type.Rect.Width,ttc_gfx_mt_current_function.Type.Rect.Height);
            if(ttc_gfx_mt_current_function.MultiTaskingFunction==function_circle)
                function_circle(ttc_gfx_mt_current_function.Type.Circle.Center_X,ttc_gfx_mt_current_function.Type.Circle.Center_Y,ttc_gfx_mt_current_function.Type.Circle.Radius);
            if(ttc_gfx_mt_current_function.MultiTaskingFunction==function_circle_fill)
                function_circle_fill(ttc_gfx_mt_current_function.Type.Circle.Center_X,ttc_gfx_mt_current_function.Type.Circle.Center_Y,ttc_gfx_mt_current_function.Type.Circle.Radius);
            if(ttc_gfx_mt_current_function.MultiTaskingFunction==ttc_gfx_text)
                ttc_gfx_text(ttc_gfx_mt_current_function.Type.String.Text,ttc_gfx_mt_current_function.Type.String.MaxSize);
            if(ttc_gfx_mt_current_function.MultiTaskingFunction==ttc_gfx_text_solid_at)
                ttc_gfx_text_solid_at(ttc_gfx_mt_current_function.Type.String.X,ttc_gfx_mt_current_function.Type.String.Y,ttc_gfx_mt_current_function.Type.String.Text,ttc_gfx_mt_current_function.Type.String.MaxSize);
            if(ttc_gfx_mt_current_function.MultiTaskingFunction==ttc_gfx_text_at)
                ttc_gfx_text_at(ttc_gfx_mt_current_function.Type.String.X,ttc_gfx_mt_current_function.Type.String.Y,ttc_gfx_mt_current_function.Type.String.Text,ttc_gfx_mt_current_function.Type.String.MaxSize);
            if(ttc_gfx_mt_current_function.MultiTaskingFunction==ttc_gfx_text_between)
                ttc_gfx_text_between(ttc_gfx_mt_current_function.Type.String.X,ttc_gfx_mt_current_function.Type.String.Y,ttc_gfx_mt_current_function.Type.String.Text,ttc_gfx_mt_current_function.Type.String.MaxSize);
            if(ttc_gfx_mt_current_function.MultiTaskingFunction==function_char_solid)
                function_char_solid(ttc_gfx_mt_current_function.Type.Char.Char);
            if(ttc_gfx_mt_current_function.MultiTaskingFunction==ttc_gfx_color_fg24)
                ttc_gfx_color_fg24(ttc_gfx_mt_current_function.Type.Color.Color);
            if(ttc_gfx_mt_current_function.MultiTaskingFunction==ttc_gfx_color_bg24)
                ttc_gfx_color_bg24(ttc_gfx_mt_current_function.Type.Color.Color);
            if(ttc_gfx_mt_current_function.MultiTaskingFunction==function_line)
                function_line(ttc_gfx_mt_current_function.Type.Line.X1,ttc_gfx_mt_current_function.Type.Line.Y1,ttc_gfx_mt_current_function.Type.Line.X2,ttc_gfx_mt_current_function.Type.Line.Y2);
            if(ttc_gfx_mt_current_function.MultiTaskingFunction==&ttc_gfx_mt_text_cursor_set)
               ttc_gfx_mt_text_cursor_set(ttc_gfx_mt_current_function.Type.Position.X,ttc_gfx_mt_current_function.Type.Position.Y);

        }

        ttc_task_msleep(100);
    }
}

void ttc_gfx_mt_clear(){

    t_ttc_gfx_mt_union* Clear;
    Clear=&ttc_gfx_mt_current_function;
    Clear->MultiTaskingFunction = function_clear;
    ttc_queue_generic_push_back(ttc_gfx_mt_queue_handle,(void*)Clear,10000);

}

void ttc_gfx_mt_rect(t_u16 X,t_u16 Y, t_s16 Width, t_s16 Height){

    t_ttc_gfx_mt_union* Rect;
    Rect=&ttc_gfx_mt_current_function;
    Rect->MultiTaskingFunction = function_rect;
    Rect->Type.Rect.X=X;
    Rect->Type.Rect.Y=Y;
    Rect->Type.Rect.Width=Width;
    Rect->Type.Rect.Height=Height;
    ttc_queue_generic_push_back(ttc_gfx_mt_queue_handle,(void*)Rect,10000);

}

void ttc_gfx_mt_rect_fill(t_u16 X,t_u16 Y, t_s16 Width, t_s16 Height){

    t_ttc_gfx_mt_union* RectFill;
    RectFill=&ttc_gfx_mt_current_function;
    RectFill->MultiTaskingFunction = function_rect_fill;
    RectFill->Type.Rect.X=X;
    RectFill->Type.Rect.Y=Y;
    RectFill->Type.Rect.Width=Width;
    RectFill->Type.Rect.Height=Height;
    ttc_queue_generic_push_back(ttc_gfx_mt_queue_handle,(void*)RectFill,10000);

}

void ttc_gfx_mt_circle(t_u16 CenterX, t_u16 CenterY, t_u16 Radius){

    t_ttc_gfx_mt_union* Circle;
    Circle=&ttc_gfx_mt_current_function;
    Circle->MultiTaskingFunction = function_circle;
    Circle->Type.Circle.Center_X=CenterX;
    Circle->Type.Circle.Center_Y=CenterY;
    Circle->Type.Circle.Radius=Radius;
    ttc_queue_generic_push_back(ttc_gfx_mt_queue_handle,(void*)Circle,10000);

}

void ttc_gfx_mt_circle_fill(t_u16 CenterX, t_u16 CenterY, t_u16 Radius){
    t_ttc_gfx_mt_union* CircleFill;
    CircleFill=&ttc_gfx_mt_current_function;
    CircleFill->MultiTaskingFunction = function_circle_fill;
    CircleFill->Type.Circle.Center_X=CenterX;
    CircleFill->Type.Circle.Center_Y=CenterY;
    CircleFill->Type.Circle.Radius=Radius;
    ttc_queue_generic_push_back(ttc_gfx_mt_queue_handle,(void*)CircleFill,10000);
}

void ttc_gfx_mt_line(t_u16 X1, t_u16 Y1, t_u16 X2, t_u16 Y2)
{
    t_ttc_gfx_mt_union* Line;
    Line=&ttc_gfx_mt_current_function;
    Line->MultiTaskingFunction = function_line;
    Line->Type.Line.X1=X1;
    Line->Type.Line.Y1=Y1;
    Line->Type.Line.X2=X2;
    Line->Type.Line.Y2=Y2;
    ttc_queue_generic_push_back(ttc_gfx_mt_queue_handle,(void*)Line,10000);
}

void ttc_gfx_mt_color_bg24(t_u32 Color) {
    t_ttc_gfx_mt_union* SetColor;
    SetColor=&ttc_gfx_mt_current_function;
    SetColor->MultiTaskingFunction = ttc_gfx_color_bg24;
    SetColor->Type.Color.Color=Color;
    ttc_queue_generic_push_back(ttc_gfx_mt_queue_handle,(void*)SetColor,10000);
}

void ttc_gfx_mt_color_fg24(t_u32 Color){
    t_ttc_gfx_mt_union* SetColor;
    SetColor=&ttc_gfx_mt_current_function;
    SetColor->MultiTaskingFunction = ttc_gfx_color_fg24;
    SetColor->Type.Color.Color=Color;
    ttc_queue_generic_push_back(ttc_gfx_mt_queue_handle,(void*)SetColor,10000);
}

void ttc_gfx_mt_text_cursor_set(t_u16 X, t_u16 Y)
{

    t_ttc_gfx_mt_union* SetPosition;
    SetPosition=&ttc_gfx_mt_current_function;
    SetPosition->MultiTaskingFunction = &ttc_gfx_text_cursor_set;
    SetPosition->Type.Position.X=X;
    SetPosition->Type.Position.Y=Y;
    ttc_queue_generic_push_back(ttc_gfx_mt_queue_handle,(void*)SetPosition,10000);

}

void ttc_gfx_mt_char_solid(t_u8 Char)
{
    t_ttc_gfx_mt_union* PutChar;
    PutChar=&ttc_gfx_mt_current_function;
    PutChar->MultiTaskingFunction = function_char_solid;
    PutChar->Type.Char.Char=Char;
    ttc_queue_generic_push_back(ttc_gfx_mt_queue_handle,(void*)PutChar,10000);
}

void ttc_gfx_mt_print(const char* Text, t_base MaxSize)
{
    t_ttc_gfx_mt_union* String;
    String=&ttc_gfx_mt_current_function;
    String->MultiTaskingFunction = ttc_gfx_text;
    String->Type.String.Text=(const char*)Text;
    String->Type.String.MaxSize=MaxSize;
    ttc_queue_generic_push_back(ttc_gfx_mt_queue_handle,(void*)String,10000);
}

void ttc_gfx_mt_print_solid_at(t_u16 X, t_u16 Y, const char* Text, t_base MaxSize) {
    t_ttc_gfx_mt_union* String;
    String=&ttc_gfx_mt_current_function;
    String->MultiTaskingFunction = ttc_gfx_text_solid_at;
    String->Type.String.X=X;
    String->Type.String.Y=Y;
    String->Type.String.Text=(const char*)Text;
    String->Type.String.MaxSize=MaxSize;
    ttc_queue_generic_push_back(ttc_gfx_mt_queue_handle,(void*)String,10000);

}

void ttc_gfx_mt_print_at(t_u16 X, t_u16 Y, const char* Text, t_base MaxSize) {
    t_ttc_gfx_mt_union* String;
    String=&ttc_gfx_mt_current_function;
    String->MultiTaskingFunction = ttc_gfx_text_at;
    String->Type.String.X=X;
    String->Type.String.Y=Y;
    String->Type.String.Text=(const char*)Text;
    String->Type.String.MaxSize=MaxSize;
    ttc_queue_generic_push_back(ttc_gfx_mt_queue_handle,(void*)String,10000);

}

void ttc_gfx_mt_print_between(t_u16 X, t_u16 Y, const char* Text, t_base MaxSize){
    t_ttc_gfx_mt_union* String;
    String=&ttc_gfx_mt_current_function;
    String->MultiTaskingFunction = ttc_gfx_text_between;
    String->Type.String.X=X;
    String->Type.String.Y=Y;
    String->Type.String.Text=Text;
    String->Type.String.MaxSize=MaxSize;
    ttc_queue_generic_push_back(ttc_gfx_mt_queue_handle,(void*)String,10000);

}


//}FunctionDefinitions
