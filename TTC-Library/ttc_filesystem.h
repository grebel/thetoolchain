#ifndef TTC_FILESYSTEM_H
#define TTC_FILESYSTEM_H
/** { ttc_filesystem.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for filesystem devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_FILESYSTEM(tc_filesystem_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_filesystem_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_filesystem_init(LogicalIndex);
 *  4) use:         ttc_filesystem_XXX(LogicalIndex);
 *
 *  Device Index
 *  Devices are addressed by a logical index with no correlation to the order of physical devices.
 *  Logical indices start at 1 to simply detect uninitialized values.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level filesystem and application.
 *
 *  Created from template ttc_device.h revision 39 at 20180413 11:21:12 UTC
 *
 *  Authors: <AUTHOR>
 *
 */
/** Description of ttc_filesystem (Do not delete this line!)
 *
 * Universal filesystem layer for multiple filesystem implementations.
 * Currently only one low-level driver is supported at a time.
 *
 * Basic Usage (using sdcard as storage device):
 *
 * + activate_project.sh
 *   # enable board with sdcard support (or add sdcard configuration manually)
 *   enableFeature 110_board_olimex_stm32_p107_revA
 *
 *   # enable one supported low-level filesystem driver
 *   enableFeature 450_filesystem_dosfs
 *
 *   # activate high-level drivers
 *   activate.500_ttc_sdcard.sh
 *   activate.500_ttc_filesystem.sh
 *
 *
 * + foo.c
 *     // allocate central buffer required by filesystem (can be used for other purposes when filesystem is not active)
 *     #define SIZE_BUFFER 512
 *     static t_u8 filesystem_Buffer[SIZE_FILESYSTEM_BUFFER];
 *
 *     // obtain configuration of first filesystem device
 *     t_ttc_filesystem_config* ConfigFS = filesystem_Data.Config_filesystem = ttc_filesystem_get_configuration( 1 );
 *
 *     // configure memory buffer to use
 *     ConfigFS->Init.Buffer           = Buffer;
 *     ConfigFS->Init.Buffer_Amount512 = SIZE_FILESYSTEM_BUFFER / 512;
 *
 *     // initialize first ttc_filesystem to use first ttc_sdcard as storage device
 *     ttc_filesystem_init_sdcard( 1, 1 );
 *
 *     e_ttc_storage_event Event = ttc_filesystem_medium_detect( Data->Idx_filesystem );
 *     if ( Event == E_ttc_storage_event_media_inserted ) {
 *       t_u8 Error = ttc_filesystem_medium_mount( Data->Idx_filesystem );
 *       if ( !Error ) { // medium In,KidM
 * mounted successfully: read
 *         Error = ttc_filesystem_open_directory( Data->Idx_filesystem, "", 0 );
 *
 *         ttc_filesystem_medium_unmount( Data->Idx_filesystem );
 *       }
 *     }
}*/

#ifndef EXTENSION_ttc_filesystem
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_filesystem.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_filesystem.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_filesystem_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level filesystem only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function Declarations ************************************************{
 *
 * The functions declared below provide the main interface for
 * filesystem devices on all supported architectures.
 * Check filesystem/filesystem_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** allocates a new filesystem instance and returns pointer to its configuration
 *
 * @return (t_ttc_filesystem_config*)  pointer to new configuration. Set all Init fields before calling ttc_filesystem_init() on it.
 */
t_ttc_filesystem_config* ttc_filesystem_create();

/** Prepares filesystem Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_filesystem_prepare();
void _driver_filesystem_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_filesystem_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    logical index of FILESYSTEM device. Each logical device <n> is defined via TTC_FILESYSTEM<n>* constants in compile_options.h and extensions.active/makefile
 * @return                configuration of indexed device
 */
t_ttc_filesystem_config* ttc_filesystem_get_configuration( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    logical index of FILESYSTEM device. Each logical device <n> is defined via TTC_FILESYSTEM<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: filesystem device has been initialized successfully; != 0: error-code
 */
e_ttc_filesystem_errorcode ttc_filesystem_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    logical index of FILESYSTEM device. Each logical device <n> is defined via TTC_FILESYSTEM<n>* constants in compile_options.h and extensions.active/makefile
 */
void ttc_filesystem_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_filesystem_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of FILESYSTEM device. Each logical device <n> is defined via TTC_FILESYSTEM<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: default values have been loaded successfully for indexed filesystem device; != 0: error-code
 */
e_ttc_filesystem_errorcode  ttc_filesystem_load_defaults( t_u8 LogicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    logical index of FILESYSTEM device. Each logical device <n> is defined via TTC_FILESYSTEM<n>* constants in compile_options.h and extensions.active/makefile

 */
void ttc_filesystem_reset( t_u8 LogicalIndex );

/** Reinitializes all initialized devices after changed systemcloc profile.
 *
 * Note: This function is called automatically from _ttc_sysclock_call_update_functions()
 */
void ttc_filesystem_sysclock_changed();

/** Read one or more data blocks from storage device into given buffer
 *
 * Real implementation is provided by Config->Init.Storage.blocks_read().
 * User must set this pointer to a function that provides the required functionality
 *
 * @param LogicalIndex (t_u8)   Logical index of filesystem instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_FILESYSTEM<n> lines in extensions.active/makefile
 * @param Buffer       (t_u8*)  Storage in RAM big egnough for Config->Init.BlockSize * AmountBlocks
 * @param Address      (t_u32)  Address of first block to read (will read addresses Address..Address+AmountBlocks-1
 * @param AmountBlocks (t_u8)   Amount of consecutive data blocks to read
 * @return             (e_ttc_filesystem_errorcode)   ==0: desired amount of data blocks was read successfully; >0: error code
 */
e_ttc_filesystem_errorcode ttc_filesystem_blocks_read( t_u8 LogicalIndex, t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks );

/** Write one or more data blocks from given buffer onto storage device
 *
 * Real implementation is provided by Config->Init.Storage.blocks_read().
 * User must set this pointer to a function that provides the required functionality
 *
 * @param LogicalIndex (t_u8)         Logical index of filesystem instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_FILESYSTEM<n> lines in extensions.active/makefile
 * @param Buffer       (const t_u8*)  Storage in RAM big egnough for Config->Init.BlockSize * AmountBlocks
 * @param Address      (t_u32)        Address of first block to read (will read addresses Address..Address+AmountBlocks-1
 * @param AmountBlocks (t_u8)         Amount of consecutive data blocks to read
 * @return             (e_ttc_filesystem_errorcode)   ==0: desired amount of data blocks was read successfully; >0: error code
 */
e_ttc_filesystem_errorcode ttc_filesystem_blocks_write( t_u8 LogicalIndex, const t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks );

/** Initialize filesystem for use with a ttc_sdcard instance.
 *
 * This helper function will initialize index filesystem to use an already initialized ttc_sdcard instance as storage device.
 * Use of ttc_sdcard does not require to call this function if configuration is done by external application.
 * Note: ttc_sdcard instance to use must be already initialized!
 * Note: Activate extension 500_ttc_sdcard in activate_project.sh and recompile to use this function!
 *
 * Example usage:
 *
 * + activate_project.sh
 *   activate.500_ttc_sdcard.sh QUIET "$0"
 *
 * + foo.c
 *   // initialize ttc_sdcard instance #2
 *   // See ttc_sdcard.h for details!
 *   ttc_sdcard_config_t* SdCard = ttc_sdcard_get_configuration( 2 );
 *   // configure SdCard.Init ...
 *   ttc_sdcard_init( 2 );
 *
 *   // initialize ttc_filesystem instance #1 to use ttc_sdcard instance #2
 *   ttc_filesystem_config_t* FileSystem = ttc_filesystem_get_configuration( 1 );
 *   // configure FileSystem.Init ...
 *   ttc_filesystem_init_sdcard( 1, 2 );
 *
 * @param LogicalIndex        (t_u8)  Logical index of filesystem instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_FILESYSTEM<n> lines in extensions.active/makefile
 * @param LogicalIndex_SdCard (t_u8)  Logical index of sdcard instance to use as storage. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_FILESYSTEM<n> lines in extensions.active/makefile
 * @return                    (e_ttc_filesystem_errorcode)
 */
#ifdef EXTENSION_ttc_sdcard
    e_ttc_filesystem_errorcode ttc_filesystem_init_sdcard( t_u8 LogicalIndex, t_u8 LogicalIndex_SdCard );
#endif

/** Fast check to see if n storage media has been inserted or removed in indexed device
 *
 * This check should be implemented as fast as possible as it will probably
 * be called regular at a high frequency.
 * Note: Implementation is provided by Config->Init.Storage.medium_detect()
 *
 * @param LogicalIndex (t_u8)  Logical index of filesystem instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_FILESYSTEM<n> lines in extensions.active/makefile
 * @return             ==0: no storage media available; ==1: storage media detected; >1: error code
 */
e_ttc_storage_event ttc_filesystem_medium_detect( t_u8 LogicalIndex );

/** Powers up storage media in slot and prepares it for usage
 *
 * Will do nothing, if no storage media is detected.
 * Note: Implementation is provided by Config->Init.Storage.medium_mount()
 *
 * @param LogicalIndex (t_u8)  Logical index of filesystem instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_FILESYSTEM<n> lines in extensions.active/makefile
 * @return             == 0: storage media device has been mounted successfully and can be used; != 0: error-code
 */
e_ttc_filesystem_errorcode ttc_filesystem_medium_mount( t_u8 LogicalIndex );

/** Flushes filesystem buffers + powers down storage media to be removed safely
 *
 * Will do nothing if storage media is currently not mounted.
 * Note: Implementation is provided by Config->Init.Storage.medium_unmount()
 *
 * @param LogicalIndex (t_u8)  Logical index of filesystem instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_FILESYSTEM<n> lines in extensions.active/makefile
 * @return             == 0: storage media device has been unmounted successfully; != 0: error-code (inform user not to remove card before unmounting it)
 */
t_u8 ttc_filesystem_medium_unmount( t_u8 LogicalIndex );

/** Opens named directory in connected storage device using current filesystem
 *
 * @param Config       (t_ttc_filesystem_config*)    Configuration of filesystem device as returned by ttc_filesystem_get_configuration()
 * @param LogicalIndex (t_u8)                        Logical index of filesystem instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_FILESYSTEM<n> lines in extensions.active/makefile
 * @param Name         (const t_u8*)                 name or path of directory to open
 * @param Handle       (t_u8)                        ==0..TTC_FILESYSTEM_MAX_DIRECTORIES-1 each directory index can point to a different directory
 * @return             (e_ttc_filesystem_errorcode)  ==0: directory was found and opened successfully; >0: error code
 */
e_ttc_filesystem_errorcode ttc_filesystem_open_directory( t_u8 LogicalIndex, const t_u8* Name, t_u8 Handle );
e_ttc_filesystem_errorcode _driver_filesystem_open_directory( t_ttc_filesystem_config* Config, const t_u8* Name, t_u8 Handle );


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_filesystem_ are passed to interfaces/ttc_filesystem_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl filesystem UPDATE
 */

/** Let filesystem connect to a freshly powered up storage device
 *
 * This function is called after connected storage device has been inserted and powered up.
 * Filesystem should now check data integrity and prepare itself for file operations.
 *
 * @param Config        Configuration of filesystem device
 * @return              == 0: storage media device has been unmounted successfully; != 0: error-code (inform user not to remove card before unmounting it)
 */
e_ttc_filesystem_errorcode _driver_filesystem_medium_mount( t_ttc_filesystem_config* Config );

/** Flushes filesystem buffers and closes all open files
 *
 * This function is called just before storage device is powered down.
 * Filesystem should close all open files and flush its write buffers.
 *
 * @param Config        Configuration of filesystem device
 * @return              == 0: storage media device has been unmounted successfully; != 0: error-code (inform user not to remove card before unmounting it)
 */
e_ttc_filesystem_errorcode _driver_filesystem_medium_unmount( t_ttc_filesystem_config* Config );

/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_filesystem_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_filesystem_features.
 *
 * @param Config        Configuration of filesystem device
 * @return              Fields of *Config have been adjusted if necessary
 */
void _driver_filesystem_configuration_check( t_ttc_filesystem_config* Config );

/** shutdown single FILESYSTEM unit device
 * @param Config        Configuration of filesystem device
 * @return              == 0: FILESYSTEM has been shutdown successfully; != 0: error-code
 */
e_ttc_filesystem_errorcode _driver_filesystem_deinit( t_ttc_filesystem_config* Config );

/** initializes single FILESYSTEM unit for operation
 * @param Config        Configuration of filesystem device
 * @return              == 0: FILESYSTEM has been initialized successfully; != 0: error-code
 */
e_ttc_filesystem_errorcode _driver_filesystem_init( t_ttc_filesystem_config* Config );

/** loads configuration of indexed FILESYSTEM unit with default values
 * @param Config        Configuration of filesystem device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_filesystem_errorcode _driver_filesystem_load_defaults( t_ttc_filesystem_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of filesystem device
 */
void _driver_filesystem_reset( t_ttc_filesystem_config* Config );
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_FILESYSTEM_H
