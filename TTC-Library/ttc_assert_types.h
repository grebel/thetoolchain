/** { ttc_assert_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for ASSERT device.
 *  Structures, Enums and Defines being required by both, high- and low-level assert.
 *  This file does not provide function declarations!
 *
 *  Created from template ttc_device_types.h revision 38 at 20161107 16:02:14 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_ASSERT_TYPES_H
#define TTC_ASSERT_TYPES_H

//{ Includes *************************************************************
//
// _types.h Header files mayshould include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_assert.h" or "ttc_assert.c"
//

#include "ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

/* Choose implementation of assert macros depending on current optimization settings
 *
 * Assert macros below are described in detail in ttc_assert.h.
 *
 */
#ifdef EXTENSION_basic_extensions_optimize_2_fastest
    #define TTC_ASSERT_MODE_FAST 1
#endif
#ifdef EXTENSION_basic_extensions_optimize_1_debug
    #define TTC_ASSERT_MODE_ORIGINS 1  // enable use of origin codes (required to identify origin of assert if not connected to a debugger
#endif

#undef TTC_ASSERT_MODE_ORIGINS //DEBUG
#  define TTC_ASSERT_MODE_ORIGINS 0 //DEBUG
#if TTC_ASSERT_MODE_FAST
    //   Asserts place test into calling function.
    //   Fastest way of providing asserts (increases compiled binary and flash usage!)

    #if TTC_ASSERT_MODE_ORIGINS  // store and pass origin codes to identify origin of assert even in productive use
        #define Assert(Condition,        Origin)   if (! (Condition) )  ttc_assert_halt_origin(Origin)
        #define Assert_NULL(Pointer,     Origin)   if (Pointer==NULL)   ttc_assert_halt_origin(Origin)
        #define Assert_Readable(Pointer, Origin)   ttc_assert_address_readable_origin(Pointer, Origin)
        #define Assert_Writable(Pointer, Origin)   ttc_assert_address_writable_origin(Pointer, Origin)
        #define Assert_Executable(Pointer, Origin) ttc_assert_address_executable(Pointer, Origin)

    #else                        // skip all origin codes for reduced binary size and max speed
        #define Assert(Condition,        Origin)   if (! (Condition) ) ttc_assert_halt_origin( ttc_assert_origin_auto )
        #define Assert_NULL(Pointer,     Origin)   if (Pointer==NULL)  ttc_assert_halt_origin( ttc_assert_origin_auto )
        #define Assert_Readable(Pointer, Origin)   ttc_assert_address_readable(Pointer)
        #define Assert_Writable(Pointer, Origin)   ttc_assert_address_writable(Pointer)
        #define Assert_Executable(Pointer, Origin) ttc_assert_address_executable(Pointer)
    #endif
#else

    // Every assert is a function call.
    // This increases runtime overhead but reduces overall binary size (and flash usage).

    #if TTC_ASSERT_MODE_ORIGINS  // store and pass origin codes to identify origin of assert even in productive use
        #define Assert(Condition,        Origin)     ttc_assert_test_origin(Condition, Origin)
        #define Assert_NULL(Pointer,     Origin)     ttc_assert_address_not_null_origin(Pointer, Origin)
        #define Assert_Readable(Pointer, Origin)     ttc_assert_address_readable_origin(Pointer, Origin)
        #define Assert_Writable(Pointer, Origin)     ttc_assert_address_writable(Pointer, Origin)
        #define Assert_Executable(Pointer, Origin)   ttc_assert_address_executable(Pointer, Origin)

    #else                        // skip all origin codes for minimum binary size (with assert checks)
        #define Assert(Condition,        Origin)     ttc_assert_test_origin(Condition, Origin)
        #define Assert_NULL(Pointer,     Origin)     ttc_assert_address_not_null(Pointer)
        #define Assert_Readable(Pointer, Origin)     ttc_assert_address_readable(Pointer)
        #define Assert_Writable(Pointer, Origin)     ttc_assert_address_writable(Pointer)
        #define Assert_Executable(Pointer, Origin)   ttc_assert_address_executable(Pointer)

    #endif
#endif

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_assert_origin       original codes identifying individual asserts
    ttc_assert_origin_OK = 0,                         // no error at all

    /* List of constant generic errors
     *
     * This list just shows some generic error codes.
     * You may use your own individual errors as described below.
     */

    ttc_assert_hardfault,                    // assert caused by hardfault handler

    // include dynamically created list of error codes
    ttc_assert_origin_auto,                  // replaced by preprocessor with a unique identifier
    //#include "error_codes.h"

    ttc_assert_origin_unknown                // no valid Origins past this entry
} e_ttc_assert_origin;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures
/** { External functions ************************************************
 *
 *  Some files cannot include ttc_assert.h and require this extern declarations.
 *  See ttc_assert.h for detailed descriptions of these functions!
 *
 */
extern void ttc_assert_halt( );
extern void ttc_assert_halt_origin( volatile e_ttc_assert_origin Origin );
extern void ttc_assert_test( BOOL Condition );
extern void ttc_assert_test_origin( BOOL TestOK, volatile e_ttc_assert_origin Origin );
extern void ttc_assert_address_matches( volatile void* Address1, volatile void* Address2 );
extern void ttc_assert_address_matches_origin( volatile void* Address1, volatile void* Address2, volatile e_ttc_assert_origin Origin );
extern void ttc_assert_address_not_null( const void* Pointer );
extern void ttc_assert_address_not_null_origin( const void* Pointer, volatile e_ttc_assert_origin Origin );
extern void ttc_assert_address_readable( const void* Address );
extern void ttc_assert_address_readable_origin( const void* Address, volatile e_ttc_assert_origin Origin );
extern void ttc_assert_address_writable( void* Address );
extern void ttc_assert_address_writable_origin( void* Address, volatile e_ttc_assert_origin Origin );



//}External functions

#endif // TTC_ASSERT_TYPES_H
