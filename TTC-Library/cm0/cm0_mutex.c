/** { cm0_mutex.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for cm0 devices on mutex architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150303 12:18:23 UTC
 *
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "cm0_mutex.h".
//
#include "cm0_mutex.h"
#include "../ttc_heap.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

volatile cm0_Mutex* cm0_mutex_create() {

    volatile cm0_Mutex* NewMutex = (volatile cm0_Mutex*) ttc_heap_alloc( sizeof(cm0_Mutex) );
    cm0_mutex_init(NewMutex);

    return NewMutex;
}
void cm0_mutex_init(volatile cm0_Mutex* Lock) {
    Assert_MUTEXES(Lock, ttc_assert_origin_auto);
    // pretty simple
    *Lock = 0;
}
e_ttc_mutex_error cm0_mutex_lock(volatile cm0_Mutex* Mutex, t_base TimeOut) {
//    Assert_MUTEXES(Mutex != 0, ttc_assert_origin_auto);
//    Assert_MUTEXES( (((t_u32) Mutex) & 3) == 0, ttc_assert_origin_auto); // 32-bit alignment required

//    if ( cm0_mutex_lock_try(Mutex) == tme_OK ) // general case first: try to obtain lock
//        return tme_OK;                        // got lock: simple and fast

//    if      (TimeOut == -1) { // lock with endless timeout
//        // wait until lock is free
//        while (cm0_mutex_lock_try(Mutex) != tme_OK)
//            _ttc_mutex_wait();

//        return tme_OK;
//    }
//    else if (TimeOut ==  0) // no timeout: we already tried to obtain mutex without timeout
//        return tme_MutexAlreadyLocked;
//    else {                    // lock within timeout
//        t_base CurrentTime = ttc_systick_get_elapsed_usecs();
//        t_base EndTime     = CurrentTime + TimeOut;
//        e_ttc_mutex_error LockFailed = tme_MutexAlreadyLocked;

//        while ( (EndTime < CurrentTime) && // tick-timer can overflow during wait: wait until overflow
//                LockFailed                 // or until successfull lock
//                ) {
//            LockFailed = cm0_mutex_lock_try(Mutex);
//            if (LockFailed) {
//                CurrentTime = ttc_systick_get_elapsed_usecs();
//                _ttc_mutex_wait();
//            }
//        }

//        while ( LockFailed &&
//                (CurrentTime < EndTime)
//              ) {         // loop until successfull lock or timeout
//            LockFailed = cm0_mutex_lock_try(Mutex);
//            if (LockFailed) {
//                CurrentTime = ttc_systick_get_elapsed_usecs();
//                _ttc_mutex_wait();
//            }
//        };

//        if (!LockFailed)
//            return tme_OK;

//        return tme_TimeOut;
//    }

//    ttc_assert_halt_origin(ttc_assert_origin_auto);
//    return tme_UNKNOWN; // this line should not be reachable
}
           inline e_ttc_mutex_error cm0_mutex_lock_try(volatile cm0_Mutex* Mutex) {
//    Assert_MUTEXES(Mutex != 0, ttc_assert_origin_auto);

//    // ToDo: speed up operation by replacing function calls with single inline assembly
//    // __ASM volatile ("ldrexb %0, [%1]" : "=r" (result) : "r" (addr) );
//    // __ASM volatile ("strexb %0, %2, [%1]" : "=&r" (result) : "r" (addr), "r" (value) );
//    // __ASM volatile ("clrex");

//    if (1) { // C-Code
//        volatile BOOL MutexAlreadyLocked;
//        MUTEX_CM0_LDREX(Mutex, MutexAlreadyLocked);
//        if (MutexAlreadyLocked) {
//            return tme_MutexAlreadyLocked;
//        }

//        MUTEX_CM0_STREX(1, Mutex, MutexAlreadyLocked);
//        if (MutexAlreadyLocked) {
//            __CLREX(); // remove lock created by __LDREXB()
//            return tme_MutexAlreadyLocked;
//        }
//    }
//    else { // Inline Assembly
//        // ToDo
//    }

//    return tme_OK;
}

//}functions
//{ private functions ******************************************

           void _cm0_mutex_lock_endless(volatile cm0_Mutex* Mutex) {
Assert_MUTEXES(Mutex != 0, ttc_assert_origin_auto);
Assert_MUTEXES( (((t_u32) Mutex) & 3) == 0, ttc_assert_origin_auto); // 32-bit alignment required

//if (0) {
//    while (cm0_mutex_lock_try(Mutex) != tme_OK)
//        _ttc_mutex_wait();
//}
//else { // slightly faster implementation
//    BOOL LockFailed;
//    do {
//        MUTEX_CM0_LDREX(Mutex, LockFailed);
//        if (!LockFailed) {
//            // try to obtain lock
//            MUTEX_CM0_STREX(1, Mutex, LockFailed);
//            if (LockFailed) {
//                __CLREX();          // remove __LDREXB() lock
//            }
//        }
//        else _ttc_mutex_wait();     // locked by other task: give away cpu
//    } while (LockFailed);
//}
}

           void _cm0_mutex_unlock(volatile cm0_Mutex* Mutex) {
//  Assert_MUTEXES(Mutex != 0, ttc_assert_origin_auto);
//  //? Assert_MUTEXES(*Mutex, ttc_assert_origin_auto); // unlocking a mutex that is not locked!

//  *Mutex = 0;
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
