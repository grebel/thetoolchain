/** { cm0_semaphore.c ************************************************
 *
 *                          The ToolChain
 *                     
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for cm0 devices on semaphore architectures.
 *  Implementation of low-level driver. 
 *    
 *  Created from template device_architecture.c revision 22 at 20150303 12:17:18 UTC
 *
 * 
 *  Authors: Gregor Rebel
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "cm0_semaphore.h".
//
#include "cm0_semaphore.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
//{ Global Variables ***********************************************************

//}Global Variables
//{ Function Definitions *******************************************************

void cm0_semaphore_init(t_cm0_semaphore* Semaphore) {

    #warning Function is empty.

}

t_cm0_semaphore* cm0_semaphore_create(){

    #warning Function is empty.

}

e_ttc_semaphore_error cm0_semaphore_take(t_cm0_semaphore* Semaphore, t_base Amount, t_base TimeOut){

     #warning Function is empty.

}

e_ttc_semaphore_error cm0_semaphore_take_try(t_cm0_semaphore* Semaphore, t_base Amount) {

    #warning Function is empty.

}

void cm0_semaphore_give(t_cm0_semaphore* Semaphore, t_base Amount) {

    #warning Function is empty.

}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Function Definitions
//{ Private Functions (ideally) ************************************************

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
