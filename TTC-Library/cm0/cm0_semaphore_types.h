#ifndef CM0_SEMAPHORE_TYPES_H
#define CM0_SEMAPHORE_TYPES_H

/** { cm0_semaphore.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for CM0 devices on semaphore architectures.
 *  Structures, Enums and Defines being required by ttc_cm0_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150303 12:17:18 UTC
 *
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"

//}includes
//{ Types & Structures ***************************************************

// datatype used to store individual semaphore
#define CM0_SEMAPHORE_SIZE 8

#if CM0_SEMAPHORE_SIZE == 8
    typedef t_u8 t_cm0_semaphore;    // smallest memory usage but often requires pad bytes to align following values
#endif
#if CM0_SEMAPHORE_SIZE == 16
    typedef t_u16 t_cm0_semaphore;
#endif
#if CM0_SEMAPHORE_SIZE == 32
    typedef t_u32 t_cm0_semaphore; // biggest memory usage but does not alter memory alignement
#endif

#define t_driver_semaphore t_cm0_semaphore

//}

#endif
