#ifndef CM0_SEMAPHORE_H
#define CM0_SEMAPHORE_H

/** { cm0_semaphore.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for cm0 devices on semaphore architectures.
 *  Structures, Enums and Defines being required by high-level cm0 and application.
 *
 *  Created from template device_architecture.h revision 22 at 20150303 12:17:18 UTC
 *
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "cm0_semaphore.c"
//
#include "cm0_semaphore_types.h"
#include "../ttc_semaphore_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_cm0_interface.h
//
// Note: ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_cm0_foo
//
#define _driver_semaphore_create()                             cm0_semaphore_create()
#define _driver_semaphore_init(Semaphore)                      cm0_semaphore_init(Semaphore)
#define _driver_semaphore_take(Semaphore, Amount, TimeOut)     cm0_semaphore_take(Semaphore, Amount, TimeOut)
#define _driver_semaphore_take_try(Semaphore, Amount)          cm0_semaphore_take_try(Semaphore, Amount)
#define _driver_semaphore_take_isr(Semaphore, Amount)          cm0_semaphore_take_try_isr(Semaphore, Amount)
#define _driver_semaphore_can_provide(Semaphore, Amount)       cm0_semaphore_can_provide(Semaphore, Amount)
#define _driver_semaphore_give(Semaphore, Amount)              cm0_semaphore_give(Semaphore, Amount)
#define _driver_semaphore_give_isr(Semaphore, Amount)          cm0_semaphore_give(Semaphore, Amount)
#define _driver_semaphore_available(Semaphore)                 (*(Semaphore))

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_cm0.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_cm0.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** allocates + initializes a new non-recursive semaphore Amount
 * @return                 =! NULL: semaphore has been created successfully (use handle to operate on semaphore)
 */
t_cm0_semaphore* cm0_semaphore_create();


/** initializes single CM0 unit for operation
 * @param Config        pointer to struct t_ttc_cm0_config (must have valid value for PhysicalIndex)
 * @return              == 0: CM0 has been initialized successfully; != 0: error-code
 */
void cm0_semaphore_init( t_cm0_semaphore* Semaphore );

/** Tries to take given amount from semaphore within timeout
 * @param Semaphore  handle of already created semaphore
 * @param TimeOut  !=-1: amount of system ticks to wait for Amount
 *                 ==-1: wait forever
 * @return         == tsme_OK: Amount has been successfully obtained within TimeOut
 */
e_ttc_semaphore_error cm0_semaphore_take( t_cm0_semaphore* Semaphore, t_base Amount, t_base TimeOut );

/** Tries to take given amount from semaphore; this function will never block and must only be called with disabled interrupts!
 * @param Semaphore                handle of already created semaphore
 * @return         == tsme_OK: Amount has been successfully obtained within TimeOut
 *                 >= tsme_Error: Error condition
 */
e_ttc_semaphore_error cm0_semaphore_take_try( t_cm0_semaphore* Semaphore, t_base Amount );

/** Tries to take given amount from semaphore; this function will never block and must only be called with disabled interrupts! (only to be called by interrupt service routines!)
 * @param Semaphore    handle of already created semaphore
 * @return         == tsme_OK: Amount has been successfully obtained within TimeOut
 *                 >= tsme_Error: Error condition
 */
e_ttc_semaphore_error cm0_semaphore_take_try_isr( t_cm0_semaphore* Semaphore, t_base Amount );
#define cm0_semaphore_take_try_isr(Semaphore, Amount) cm0_semaphore_take_try(Semaphore, Amount)


/** Gives amount of tokens back to semaphore
 * @param Semaphore  handle of already created smart semaphore
 */
void cm0_semaphore_give( t_cm0_semaphore* Semaphore, t_base Amount );


/** Checks if value of given Semaphore >= Amount
 * @param Semaphore    handle of already created semaphore
 * @return         == TRUE: given Semaphore can provide given amount; FALSE otherwise
 */
#define cm0_semaphore_can_provide(Semaphore, Amount) ( ( *(Semaphore) >= Amount ) ? 1 : 0 )

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _cm0_semaphore_foo(t_ttc_cm0_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //CM0_SEMAPHORE_H
