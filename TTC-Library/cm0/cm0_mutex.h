#ifndef CM0_MUTEX_H
#define CM0_MUTEX_H

/** { cm0_mutex.h **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level driver for cm0 devices on mutex architectures.
 *  Structures, Enums and Defines being required by high-level cm0 and application.
 *
 *  Created from template device_architecture.h revision 22 at 20150303 12:18:23 UTC
 *
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "cm0_mutex.c"
//
#include "cm0_mutex_types.h"
#include "../ttc_mutex_types.h"
#include "../ttc_memory_types.h"
#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

#define _driver_mutex_create()               cm0_mutex_create()
#define _driver_mutex_init(Mutex)            cm0_mutex_init(Mutex)
#define _driver_mutex_lock(Mutex, TimeOut)   cm0_mutex_lock(Mutex, TimeOut)
#define _driver_mutex_lock_endless(Mutex)    cm0_mutex_lock_endless(Mutex)
#define _driver_mutex_lock_try(Mutex)         cm0_mutex_lock_try(Mutex)
#define _driver_mutex_lock_isr(Mutex)        cm0_mutex_lock_try_isr(Mutex)
#define _driver_mutex_is_locked(Mutex)       cm0_mutex_is_locked(Mutex)

#if TTC_ASSERT_MUTEXES == 1
    #define _driver_mutex_unlock(Mutex)          cm0_mutex_unlock(Mutex)
    #define _driver_mutex_unlock_isr(Mutex)      cm0_mutex_unlock(Mutex)

#else
    #define _driver_mutex_unlock(Mutex)          (*Mutex = 0)
    #define _driver_mutex_unlock_isr(Mutex)      (*Mutex = 0)

#endif


// define exclusive access commands depending on configured mutex size
//#if CM0_MUTEX_SIZE == 8 // byte sized mutexes
//#  define MUTEX_CM0_LDREX(Mutex, Result)         __asm volatile ("ldrexb %0, [%1]" : "=r" (Result) : "r" (Mutex) )
//#  define MUTEX_CM0_STREX(Value, Mutex, Result)  __asm volatile ("strexb %0, %2, [%1]" : "=&r" (Result) : "r" (Mutex), "r" (Value) )
//#endif

//#if CM0_MUTEX_SIZE == 16 // half word sized mutexes
//#  define MUTEX_CM0_LDREX(Mutex, Result)         __asm volatile ("ldrexh %0, [%1]" : "=r" (Result) : "r" (Mutex) )
//#  define MUTEX_CM0_STREX(Value, Mutex, Result)  __asm volatile ("strexh %0, %2, [%1]" : "=&r" (Result) : "r" (Mutex), "r" (Value) )
//#endif

//#if CM0_MUTEX_SIZE == 32 // word sized mutexes
//#  define MUTEX_CM0_LDREX(Mutex, Result)         __asm volatile ("ldrexw %0, [%1]" : "=r" (Result) : "r" (Mutex) )
//#  define MUTEX_CM0_STREX(Value, Mutex, Result)  __asm volatile ("strexw %0, %2, [%1]" : "=&r" (Result) : "r" (Mutex), "r" (Value) )
//#endif

//} Macro

//{ Function prototypes **************************************************

/** allocates + initializes a new non-recursive mutex lock
 * @return                 =! NULL: mutex has been created successfully (use handle to operate on mutex)
 */
volatile cm0_Mutex* cm0_mutex_create();

/** initializes an existing non-recursive mutex lock to the unlocked state
 * @Lock                 storage to be initialized as mutex
 */
void cm0_mutex_init( volatile cm0_Mutex* Lock );

/** Tries to obtain lock on mutex within timeout
 * @param Mutex  handle of already created mutex
 * @param TimeOut  !=-1: amount of system ticks to wait for lock
 *                 ==-1: wait forever
 * @return         == tme_OK: Lock has been successfully obtained within TimeOut
 */
e_ttc_mutex_error cm0_mutex_lock( volatile cm0_Mutex* Mutex, t_base TimeOut );

/** Blocks until lock has been obtained on mutex
 * @param Mutex  handle of already created mutex
 */
void cm0_mutex_lock_endless( volatile cm0_Mutex* Mutex );
#if TTC_ASSERT_MUTEXES == 1
    #define cm0_mutex_lock_endless(Mutex) _cm0_mutex_lock_endless(Mutex)
#else
    //#  define cm0_mutex_lock_endless(Mutex) _cm0_mutex_lock_endless(Mutex)
    #define cm0_mutex_lock_endless(Mutex) while (cm0_mutex_lock_try(Mutex) != tme_OK) _ttc_mutex_wait()
#endif
void _cm0_mutex_lock_endless( volatile cm0_Mutex* Mutex );


/** Tries to obtain lock on mutex; this function will never block
 * @param Mutex                handle of already created mutex
 * @return         == tme_OK: Lock has been successfully obtained within TimeOut
 *                 == tme_WasLockedByTask: - a task of higher priority has been interrupted => call cm0_task_yield() at end of your interrupt service routine!
 *                 >= tme_Error: Error condition
 */
e_ttc_mutex_error cm0_mutex_lock_try( volatile cm0_Mutex* Mutex );

/** Tries to obtain lock on mutex; this function will never block (only to be called by interrupt service routines!)
 * @param Mutex    handle of already created mutex
 * @return         == tme_OK: Lock has been successfully obtained within TimeOut
 *                 == tme_WasLockedByTask: - a task of higher priority has been interrupted => call cm0_task_yield() at end of your interrupt service routine!
 *                 >= tme_Error: Error condition
 */
e_ttc_mutex_error cm0_mutex_lock_try_isr( volatile cm0_Mutex* Mutex );
#define cm0_mutex_lock_try_isr(Mutex) cm0_mutex_lock_try(Mutex)

/** Checks if given Mutex is currently locked
 * @param Mutex    handle of already created mutex
 * @return         == TRUE: given Mutex is currently in locked state; FALSE otherwise
 */
// BOOL cm0_mutex_is_locked(volatile cm0_Mutex* Mutex);
#define cm0_mutex_is_locked(Mutex) (Mutex && (*Mutex != 0))

/** Releases an already obtained lock from mutex
 * @param Mutex  handle of already created smart mutex
 * @return         == tme_OK: Lock has been released successfully
 */
void cm0_mutex_unlock( volatile cm0_Mutex* Mutex );
#if TTC_ASSERT_MUTEXES == 1
    #define cm0_mutex_unlock(Mutex) _cm0_mutex_unlock(Mutex)
#else
    #define cm0_mutex_unlock(Mutex) (*Mutex = 0)
#endif
void _cm0_mutex_unlock( volatile cm0_Mutex* Mutex );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
//{ Private Function prototypes ******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _cm0_mutex_foo(t_ttc_cm0_config* Config)

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions

#endif //CM0_MUTEX_H
