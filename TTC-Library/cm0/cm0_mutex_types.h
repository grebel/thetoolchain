#ifndef CM0_MUTEX_TYPES_H
#define CM0_MUTEX_TYPES_H

/** { cm0_mutex.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-Level datatypes for CM0 devices on mutex architectures.
 *  Structures, Enums and Defines being required by ttc_cm0_types.h
 *
 *  Created from template device_architecture_types.h revision 21 at 20150303 12:18:23 UTC
 *
 *
 *  Authors: Victor
 *
}*/
//{ Defines/ TypeDefs **********************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes

// datatype used to store individual mutex
#define CM0_MUTEX_SIZE 8

#if CM0_MUTEX_SIZE == 8
    typedef t_u8 cm0_Mutex; // smallest size but often creates misalignement of consecutive struct fields
#endif
#if CM0_MUTEX_SIZE == 16
    typedef t_u16 cm0_Mutex;
#endif
#if CM0_MUTEX_SIZE == 32
    typedef t_u32 cm0_Mutex; // does not change alignement; best debuggability (watchpoints always check 32 bits)
#endif

#define t_ttc_mutex volatile cm0_Mutex

//} Structures/ Enums


#endif //CM0_MUTEX_TYPES_H
