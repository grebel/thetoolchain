#ifndef FILESYSTEM_DOSFS_H
#define FILESYSTEM_DOSFS_H

/** { filesystem_dosfs.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_filesystem.h providing
 *  function prototypes being required by ttc_filesystem.h
 *
 *  dosfs free fat12/fat16/fat32 filesystem for low-end embedded applications (1kb ram, 4kb rom)
 *
 *  Created from template device_architecture.h revision 32 at 20180413 11:21:12 UTC
 *
 *  Note: See ttc_filesystem.h for description of dosfs independent FILESYSTEM implementation.
 *
 *  Authors: <AUTHOR>
 *
 */
/** Description of filesystem_dosfs (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_FILESYSTEM_DRIVER_AVAILABLE
#define EXTENSION_FILESYSTEM_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_filesystem_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "filesystem_dosfs.c"
//
#include "../ttc_filesystem_types.h" // will include filesystem_dosfs_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_filesystem_interface.h
//
// Some driver functions may be optional to implement.
// Check ttc_filesystem_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_filesystem_foo
//
#define ttc_driver_filesystem_configuration_check filesystem_dosfs_configuration_check
#define ttc_driver_filesystem_deinit              filesystem_dosfs_deinit
#define ttc_driver_filesystem_init                filesystem_dosfs_init
#define ttc_driver_filesystem_load_defaults       filesystem_dosfs_load_defaults
#define ttc_driver_filesystem_prepare             filesystem_dosfs_prepare
#define ttc_driver_filesystem_reset               filesystem_dosfs_reset
#define ttc_driver_filesystem_medium_mount        filesystem_dosfs_medium_mount
#define ttc_driver_filesystem_medium_unmount      filesystem_dosfs_medium_unmount
#define ttc_driver_filesystem_open_directory      filesystem_dosfs_open_directory
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Macro definitions
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_filesystem.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_filesystem.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_filesystem_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_filesystem_features.
 *
 * @param Config        Configuration of filesystem device
 * @return              Fields of *Config have been adjusted if necessary
 */
void filesystem_dosfs_configuration_check( t_ttc_filesystem_config* Config );

/** shutdown single FILESYSTEM unit device
 * @param Config        Configuration of filesystem device
 * @return              == 0: FILESYSTEM has been shutdown successfully; != 0: error-code
 */
e_ttc_filesystem_errorcode filesystem_dosfs_deinit( t_ttc_filesystem_config* Config );

/** initializes single FILESYSTEM unit for operation
 * @param Config        Configuration of filesystem device
 * @return              == 0: FILESYSTEM has been initialized successfully; != 0: error-code
 */
e_ttc_filesystem_errorcode filesystem_dosfs_init( t_ttc_filesystem_config* Config );

/** loads configuration of indexed FILESYSTEM unit with default values
 * @param Config        Configuration of filesystem device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_filesystem_errorcode filesystem_dosfs_load_defaults( t_ttc_filesystem_config* Config );

/** Prepares filesystem Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void filesystem_dosfs_prepare();

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of filesystem device
 */
void filesystem_dosfs_reset( t_ttc_filesystem_config* Config );

/** Let filesystem connect to a freshly powered up storage device
 *
 * This function is called after connected storage device has been inserted and powered up.
 * Filesystem should now check data integrity and prepare itself for file operations.
 *
 * @param Config       Configuration of filesystem device
 * @return             == 0: storage media device has been mounted successfully and can be used; != 0: error-code
 */
e_ttc_filesystem_errorcode filesystem_dosfs_medium_mount( t_ttc_filesystem_config* Config );

/** Flushes filesystem buffers and closes all open files
 *
 * This function is called just before storage device is powered down.
 * Filesystem should close all open files and flush its write buffers.
 *
 * @param Config       Configuration of filesystem device
 * @return             == 0: storage media device has been unmounted successfully; != 0: error-code (inform user not to remove card before unmounting it)
 */
e_ttc_filesystem_errorcode filesystem_dosfs_medium_unmount( t_ttc_filesystem_config* Config );


/** Opens named directory in connected storage device using current filesystem
 *
 * @param Config       (t_ttc_filesystem_config*)    Configuration of filesystem device as returned by ttc_filesystem_get_configuration()
 * @param LogicalIndex (t_u8)                        Logical index of filesystem instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_FILESYSTEM<n> lines in extensions.active/makefile
 * @param Name         (t_u8*)                       name or path of directory to open
 * @param Handle       (t_u8)                        ==0..TTC_FILESYSTEM_MAX_DIRECTORIES-1 each directory index can point to a different directory
 * @return             (e_ttc_filesystem_errorcode)  ==0: directory was found and opened successfully; >0: error code
 */
e_ttc_filesystem_errorcode filesystem_dosfs_open_directory( t_ttc_filesystem_config* Config, const t_u8* Name, t_u8 Handle );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //FILESYSTEM_DOSFS_H
