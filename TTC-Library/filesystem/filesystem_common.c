/** filesystem_common.c ***********************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to filesystem low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.c revision 13 at 20180413 11:21:12 UTC
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * Put all includes here that are required to compile this source code.
 */

#include "filesystem_common.h"
#include "../interfaces/ttc_filesystem_interface.h" // allows to call low-level driver functions via  _driver_filesystem_*()
#include "../ttc_heap.h"      // dynamic memory and safe arrays  
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)
//}PrivateFunctions
/** Function Definitions ********************************************{
 */

e_ttc_filesystem_errorcode filesystem_common_blocks_read( t_ttc_filesystem_config* Config, t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks ) {
    Assert_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_Readable( Buffer, ttc_assert_origin_auto ); // always check incoming pointer arguments

    e_ttc_filesystem_errorcode Result;

    if ( Config->Init.Storage.blocks_read ) {
        Assert_FILESYSTEM_Readable( Config->Init.Storage.blocks_read, ttc_assert_origin_auto ); // configured function pointer seems to be invalid. Check configuration!
        Result = Config->Init.Storage.blocks_read( Config->Init.Storage.Config,
                                                   Buffer,
                                                   Address,
                                                   AmountBlocks
                                                 );
    }
    else { Result = E_ttc_filesystem_errorcode_InvalidConfiguration; }

    return Result; // e_ttc_filesystem_errorcode
}
e_ttc_filesystem_errorcode filesystem_common_blocks_write( t_ttc_filesystem_config* Config, const t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks ) {
    Assert_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_Writable( ( void* ) Buffer, ttc_assert_origin_auto ); // always check incoming pointer arguments

    e_ttc_filesystem_errorcode Result;

    if ( Config->Init.Storage.blocks_write ) {
        Assert_FILESYSTEM_Readable( Config->Init.Storage.blocks_write, ttc_assert_origin_auto ); // configured function pointer seems to be invalid. Check configuration!
        Result = Config->Init.Storage.blocks_write( Config->Init.Storage.Config,
                                                    Buffer,
                                                    Address,
                                                    AmountBlocks
                                                  );
    }
    else { Result = E_ttc_filesystem_errorcode_InvalidConfiguration; }

    return Result; // e_ttc_filesystem_errorcode
}
e_ttc_storage_event        filesystem_common_medium_detect( t_ttc_filesystem_config* Config ) {
    Assert_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    e_ttc_storage_event Event = 0;

    if ( Config->Init.Storage.medium_detect ) {
        Assert_FILESYSTEM_Readable( Config->Init.Storage.medium_unmount, ttc_assert_origin_auto ); // configured function pointer seems to be invalid. Check configuration!
        Event = Config->Init.Storage.medium_detect( Config->Init.Storage.Config );
    }
    else { Event = E_ttc_storage_event_invalid_storage_configuration; }
    Config->LastEvent = Event;

    return Event; // e_ttc_filesystem_errorcode
}
e_ttc_filesystem_errorcode filesystem_common_medium_mount( t_ttc_filesystem_config* Config ) {
    Assert_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    e_ttc_filesystem_errorcode Error;

    if ( Config->Init.Storage.medium_mount ) {
        Assert_FILESYSTEM_Readable( Config->Init.Storage.medium_mount, ttc_assert_origin_auto ); // configured function pointer seems to be invalid. Check configuration!
        Error = Config->Init.Storage.medium_mount( Config->Init.Storage.Config, &( Config->BlockSize ) );
    }
    else { Error = E_ttc_filesystem_errorcode_InvalidConfiguration; }

    return Error; // e_ttc_filesystem_errorcode
}
e_ttc_filesystem_errorcode filesystem_common_medium_unmount( t_ttc_filesystem_config* Config ) {
    Assert_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    e_ttc_filesystem_errorcode Error;

    if ( Config->Init.Storage.medium_unmount ) {
        Assert_FILESYSTEM_Readable( Config->Init.Storage.medium_unmount, ttc_assert_origin_auto ); // configured function pointer seems to be invalid. Check configuration!
        Error = Config->Init.Storage.medium_unmount( Config->Init.Storage.Config );
    }
    else { Error = E_ttc_filesystem_errorcode_InvalidConfiguration; }

    return Error; // e_ttc_filesystem_errorcode
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
