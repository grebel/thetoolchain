/** filesystem_dosfs.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_filesystem.c.
 *
 *  dosfs free fat12/fat16/fat32 filesystem for low-end embedded applications (1kb ram, 4kb rom)
 *
 *  Created from template device_architecture.c revision 40 at 20180413 11:21:12 UTC
 *
 *  Note: See ttc_filesystem.h for description of high-level filesystem implementation.
 *  Note: See filesystem_dosfs.h for description of dosfs filesystem implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "filesystem_dosfs.h".
 */

#include "filesystem_dosfs.h"
#include "../ttc_heap.h"              // dynamic memory allocation (e.g. malloc() )
#include "../ttc_memory.h"            // basic memory checks
#include "../ttc_task.h"              // stack checking (provides TTC_TASK_RETURN() macro)
#include "filesystem_common.h"          // generic functions shared by low-level drivers of all architectures 
#include "additionals/500_ttc_filesystem_dosfs/dosfs.h" // using external library dosfs from Lewin A.R.W. Edwards
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_filesystem_features filesystem_dosfs_Features = {

    .Scratch_AmountBlocks_Minimum = 1,
    .Scratch_AmountBlocks_Maximum = 1,
    .Cache_AmountBlocks_Minimum   = 0,
    .Cache_AmountBlocks_Maximum   = 0, // ToDO( "implement caching!" );
    .BlockSize_Minimum            = 512,
    .BlockSize_Maximum            = 512,
    .Supports_FAT12               = 1,
    .Supports_FAT16               = 1,
    .Supports_FAT32               = 1
};
//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

/** return pointer to a valid directory info buffer
 *
 * Directory info buffers are required to store results from filesystem_dosfs_open_directory().
 *
 * @param Config (t_ttc_filesystem_config*)
 * @param Handle (t_u8)
 * @return       (DIRINFO*)
 */
DIRINFO* _filesystem_dosfs_get_directory_info( t_ttc_filesystem_config* Config, t_u8 Handle );

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

extern t_ttc_filesystem_config* ttc_filesystem_get_configuration( t_u8 LogicalIndex );

// Compatibility functions for dosfs (-> dosfs.h)
t_u32 DFS_ReadSector( t_u8 LogicalIndex, t_u8* Buffer, t_u32 Sector, t_u32 Amount ) {
    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );

    return filesystem_common_blocks_read( Config, Buffer, Sector, Amount );
}
t_u32 DFS_WriteSector( t_u8 LogicalIndex, t_u8* Buffer, t_u32 Sector, t_u32 Amount ) {
    t_ttc_filesystem_config* Config = ttc_filesystem_get_configuration( LogicalIndex );

    return filesystem_common_blocks_read( Config, Buffer, Sector, Amount );
}

e_ttc_filesystem_errorcode filesystem_dosfs_load_defaults( t_ttc_filesystem_config* Config ) {
    Assert_FILESYSTEM_EXTRA_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = E_ttc_filesystem_architecture_dosfs;  // set type of architecture
    Config->Features     = &filesystem_dosfs_Features;           // assign feature set

    if ( 0 ) { // enable for memory mapped devices (everything that has a base register)
        switch ( Config->PhysicalIndex ) { // load address of peripheral base register
            //case 0: Config->BaseRegister = & register_dosfs_FILESYSTEM1; break;
            //case 1: Config->BaseRegister = & register_dosfs_FILESYSTEM2; break;
            default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // invalid physical index: check your makefile and high-level driver implementation!
        }
    }

    if ( Config->LowLevelConfig == NULL ) { // allocate memory for spi specific configuration
        { Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_ttc_filesystem_architecture ) ); }

        // reset all flags
        ttc_memory_set( &( Config->Flags ), 0, sizeof( Config->Flags ) );

    }

    TTC_TASK_RETURN( 0 ); // will perform stack overflow check and return given value
}
void                       filesystem_dosfs_configuration_check( t_ttc_filesystem_config* Config ) {
    Assert_FILESYSTEM_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    // check all required init fields which are not yet checked by _ttc_filesystem_configuration_check():

}
e_ttc_filesystem_errorcode filesystem_dosfs_deinit( t_ttc_filesystem_config* Config ) {
    Assert_FILESYSTEM_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    filesystem_common_medium_unmount( Config );

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
e_ttc_filesystem_errorcode filesystem_dosfs_init( t_ttc_filesystem_config* Config ) {
    Assert_FILESYSTEM_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    filesystem_dosfs_reset( Config );

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
void                       filesystem_dosfs_prepare() {

}
void                       filesystem_dosfs_reset( t_ttc_filesystem_config* Config ) {
    Assert_FILESYSTEM_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    ttc_memory_set( &( Config->LowLevelConfig->dosfs ), 0, sizeof( Config->LowLevelConfig->dosfs ) );

}
e_ttc_filesystem_errorcode filesystem_dosfs_medium_mount( t_ttc_filesystem_config* Config ) {
    t_u8    sector[SECTOR_SIZE];
    t_u32   PartitionStart, PartitionSize;
    t_u8    PartitionActive, PartitionType;
    DIRINFO DirInfo;

    e_ttc_filesystem_errorcode Error = 0;
    PartitionStart = DFS_GetPtnStart( 0, sector, 0, &PartitionActive, &PartitionType, &PartitionSize );
    if ( PartitionStart == 0xffffffff )
    { Error = E_ttc_filesystem_errorcode_InvalidPartionTable; }

    if ( !Error && !DFS_GetVolInfo( 0, sector, PartitionStart, &Config->LowLevelConfig->dosfs.VolumeInfo ) )
    { Error = E_ttc_filesystem_errorcode_CannotReadVolumeInfo; }

    t_u8 Root = 0; // empty string
    if ( !Error && DFS_OpenDir( &Config->LowLevelConfig->dosfs.VolumeInfo, &Root, &DirInfo ) )
    { Error = E_ttc_filesystem_errorcode_CannotReadDirectory; }

    TTC_TASK_RETURN( Error );   // will perform stack overflow check and return given value
}
e_ttc_filesystem_errorcode filesystem_dosfs_medium_unmount( t_ttc_filesystem_config* Config ) {

    // dosfs does not cache any data this makes unmounting easy

    // reset low-level data
    filesystem_dosfs_reset( Config );

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
e_ttc_filesystem_errorcode filesystem_dosfs_open_directory( t_ttc_filesystem_config* Config, const t_u8* Name, t_u8 Handle ) {
    Assert_FILESYSTEM_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_FILESYSTEM_EXTRA_Readable( Name, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    Config->LowLevelConfig->dosfs.VolumeInfo.unit = Config->LogicalIndex;
    DIRINFO* DirectoryInfo = _filesystem_dosfs_get_directory_info( Config, Handle );

    t_u32 Error = DFS_OpenDir( &( Config->LowLevelConfig->dosfs.VolumeInfo ),
                               ( t_u8* ) Name, // sadly we have to typecast away the const feature
                               DirectoryInfo
                             );
    if ( Error ) {
        TTC_TASK_RETURN( E_ttc_filesystem_errorcode_CannotReadDirectory );
    }
    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

DIRINFO* _filesystem_dosfs_get_directory_info( t_ttc_filesystem_config* Config, t_u8 Handle ) {
    Assert_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_FILESYSTEM( Handle < TTC_FILESYSTEM_MAX_DIRECTORIES, ttc_assert_origin_auto ); // this index value cannot be ok. Check implementation of caller!

    DIRINFO* DirectoryInfo = &( Config->LowLevelConfig->dosfs.DirectoryInfo[Handle] );
    if ( !DirectoryInfo->scratch ) { // no scratch buffer: assign a single sector buffer to use as scratch memory
        // spread the amount of available scratch buffers over all allocated directory infos
        t_u8 BufferIndex = Handle % Config->Init.Scratch_AmountBlocks;
        Assert_FILESYSTEM( ( Config->BlockSize  >= Config->Features->BlockSize_Minimum ) && ( Config->BlockSize <= Config->Features->BlockSize_Maximum ), ttc_assert_origin_auto ); // illegal block size value. Check implementation of caller!
        DirectoryInfo->scratch = Config->Init.Scratch + ( BufferIndex * Config->BlockSize );
    }
    return DirectoryInfo; // DIRINFO*
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
