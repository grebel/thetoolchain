#ifndef FILESYSTEM_DOSFS_TYPES_H
#define FILESYSTEM_DOSFS_TYPES_H

/** { filesystem_dosfs.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_filesystem.h providing
 *  Structures, Enums and Defines being required by ttc_filesystem_types.h
 *
 *  dosfs free fat12/fat16/fat32 filesystem for low-end embedded applications (1kb ram, 4kb rom)
 *
 *  Created from template device_architecture_types.h revision 27 at 20180413 11:21:12 UTC
 *
 *  Note: See ttc_filesystem.h for description of high-level filesystem implementation!
 *  Note: See filesystem_dosfs.h for description of dosfs specific filesystem implementation!
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

#ifndef TTC_FILESYSTEM1   // device not defined in makefile
    #define TTC_FILESYSTEM1    E_ttc_filesystem_architecture_dosfs   // example device definition for current architecture (Disable line if not desired!)
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#ifndef TTC_FILESYSTEM_TYPES_H
    #  error Do not include filesystem_common_types.h directly. Include ttc_filesystem_types.h instead!
#endif

#include "../ttc_basic_types.h"
#include "additionals/500_ttc_filesystem_dosfs/dosfs.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_filesystem_types.h *************************

typedef struct {  // dosfs specific configuration data of single filesystem device
    VOLINFO VolumeInfo;                                    // volume information    (filled by filesystem_dosfs_medium_mount() )
    DIRINFO DirectoryInfo[TTC_FILESYSTEM_MAX_DIRECTORIES]; // directory information required by all Directory functions
} __attribute__( ( __packed__ ) ) t_filesystem_dosfs_config;

// t_ttc_filesystem_architecture is required by ttc_filesystem_types.h
#define t_ttc_filesystem_architecture t_filesystem_dosfs_config

//} Structures/ Enums

#endif //FILESYSTEM_DOSFS_TYPES_H
