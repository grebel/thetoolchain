#ifndef filesystem_common_h
#define filesystem_common_h

/** filesystem_common.h ****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to filesystem low-level drivers of all architectures.
 *  The functions provided here are common in two ways:
 *  1) They can be called from high-level driver ttc_filesystem.c and from current low-level driver filesystem_*.c.
 *  2) All common_filesystem_*() functions can call low-level driver functions as _driver_filesystem_*().
 *
 *  Created from template ttc-lib/templates/device_common.h revision 15 at 20180413 11:21:12 UTC
 *
 *  Authors: <AUTHOR>
 *
 *  Description of filesystem_common (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "filesystem_dosfs.c"
 */

#include "../ttc_basic.h"
#include "../ttc_filesystem_types.h" // will include filesystem_common_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_filesystem_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_filesystem_foo
 */

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions
/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_filesystem.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_filesystem.h for details.
 *
 * You normally should not need to add functions here.
 * Global feature functions must be added in ttc_filesystem.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl filesystem UPDATE".
 */


/** Read one or more data blocks from storage device into given buffer
 *
 * Real implementation is provided by Config->Init.Storage.blocks_read().
 * User must set this pointer to a function that provides the required functionality
 *
 * @param Config       (t_ttc_filesystem_config*) Configuration of filesystem device
 * @param Buffer       (t_u8*)  Storage in RAM big egnough for Config->Init.BlockSize * AmountBlocks
 * @param Address      (t_u32)  Address of first block to read (will read addresses Address..Address+AmountBlocks-1
 * @param AmountBlocks (t_u8)   Amount of consecutive data blocks to read
 * @return             (e_ttc_filesystem_errorcode)   ==0: desired amount of data blocks was read successfully; >0: error code
 */
e_ttc_filesystem_errorcode filesystem_common_blocks_read( t_ttc_filesystem_config* Config, t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks );

/** Write one or more data blocks from given buffer onto storage device
 *
 * Real implementation is provided by Config->Init.Storage.blocks_write().
 * User must set this pointer to a function that provides the required functionality
 *
 * @param Config       (t_ttc_filesystem_config*) Configuration of filesystem device
 * @param Buffer       (const t_u8*)  Storage in RAM big egnough for Config->Init.BlockSize * AmountBlocks
 * @param Address      (t_u32)  Address of first block to read (will read addresses Address..Address+AmountBlocks-1
 * @param AmountBlocks (t_u8)   Amount of consecutive data blocks to read
 * @return             (e_ttc_filesystem_errorcode)   ==0: desired amount of data blocks was read successfully; >0: error code
 */
e_ttc_filesystem_errorcode filesystem_common_blocks_write( t_ttc_filesystem_config* Config, const t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks );

/** Fast check to see if n storage media has been inserted or removed in indexed device
 *
 * This check should be implemented as fast as possible as it will probably
 * be called regular at a high frequency.
 * Note: Implementation is provided by Config->Init.Storage.medium_detect()
 *
 * @param Config       (t_ttc_filesystem_config*) Configuration of filesystem device
 * @return             ==0: no storage media available; ==1: storage media detected; >1: error code
 */
e_ttc_storage_event filesystem_common_medium_detect( t_ttc_filesystem_config* Config );

/** Powers up storage media in slot and prepares it for usage
 *
 * Will do nothing, if no storage media is detected.
 * Note: Implementation is provided by Config->Init.Storage.medium_mount()
 *
 * @param Config       Configuration of storage media device
 * @return             == 0: storage media device has been mounted successfully and can be used; != 0: error-code
 */
e_ttc_filesystem_errorcode filesystem_common_medium_mount( t_ttc_filesystem_config* Config );

/** Flushes filesystem buffers + powers down storage media to be removed safely
 *
 * Will do nothing if storage media is currently not mounted.
 * Note: Implementation is provided by Config->Init.Storage.medium_unmount()
 *
 * @param Config       Configuration of storage media device
 * @return             == 0: storage media device has been unmounted successfully; != 0: error-code (inform user not to remove card before unmounting it)
 */
e_ttc_filesystem_errorcode filesystem_common_medium_unmount( t_ttc_filesystem_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //filesystem_common_h
