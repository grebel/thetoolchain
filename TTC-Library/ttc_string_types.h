/** { ttc_string_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for STRING device.
 *  Structures, Enums and Defines being required by both, high- and low-level string.
 *  This file does not provide function declarations!
 *  54 at 20151120 13:30:25 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_STRING_TYPES_H
#define TTC_STRING_TYPES_H

//{ Includes *************************************************************
//
// _types.h Header files mayshould include only other *_types.h header files to avoid circular includes.
// Includes that provide function declarations should be placed
// in "ttc_string.h" or "ttc_string.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h"

//InsertIncludes above (DO NOT REMOVE THIS LINE!)

#ifdef EXTENSION_string_ascii
    #include "string/string_ascii_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************


//}
/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides two own assert functions:
 *
 * 1) The default assert is enabled by default and must be disabled explicitly.
 *    This assert is to be used for all arguments provided from outside of this driver.
 *
 * 2) The extra assert is disabled by default and must be enabled explicitly.
 *    The idea of an extra assert is to provide extra internal checks while the driver code is under development.
 *    Once the driver is assumed to be stable, extra checks can be disabled.
 *
 * Asserts for each driver can be enabled or disabled from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to change assert settings:
 *
 * #1 Disable default asserts for slight performance improvement
 *    COMPILE_OPTS += -DTTC_ASSERT_STRING 0        # disable default asserts for string driver
 *
 * #2 Enable extra checks for more intense checks
 *    COMPILE_OPTS += -DTTC_ASSERT_STRING_EXTRA 1  # enable extra asserts for string driver
 *
 */
#ifndef TTC_ASSERT_STRING    // any previous definition set (Makefile)?
    #define TTC_ASSERT_STRING 1  // default asserts are enabled by default
#endif
#if (TTC_ASSERT_STRING == 1)  // use Assert()s in STRING code (somewhat slower but alot easier to debug)
    #define Assert_STRING(Condition, Origin) Assert( ((Condition) != 0), Origin)
    #define Assert_STRING_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_STRING_Readable(Address, Origin) Assert_Readable(Address, Origin)
#else  // use no default Assert()s in STRING code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_STRING(Condition, Origin)
    #define Assert_STRING_Writable(Address, Origin)
    #define Assert_STRING_Readable(Address, Origin)
#endif


#ifndef TTC_ASSERT_STRING_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_STRING_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_STRING_EXTRA == 1)  // use Assert()s in STRING code (somewhat slower but alot easier to debug)
    #define Assert_STRING_EXTRA(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // use no extra Assert()s in STRING code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_STRING_EXTRA(Condition, ErrorCode)
#endif
//}

// size of temporary buffer used by ttc_string_printf()
#ifndef TTC_STRING_PRINTF_BUFFERSIZE
    #define TTC_STRING_PRINTF_BUFFERSIZE 100
#endif

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef enum {   // e_ttc_string_errorcode       return codes of STRING devices
    ec_string_OK = 0,

    // other warnings go here..

    ec_string_ERROR,                  // general failure
    ec_string_NULL,                   // NULL pointer not accepted
    ec_string_DeviceNotFound,         // corresponding device could not be found
    ec_string_InvalidConfiguration,   // sanity check of device configuration failed
    ec_string_InvalidImplementation,  // your code does not behave as expected

    // other failures go here..

    ec_string_unknown                // no valid errorcodes past this entry
} e_ttc_string_errorcode;

typedef enum {   // e_ttc_string_architecture    types of architectures supported by STRING driver
    ta_string_None = 0,       // no architecture selected


    ta_string_ascii, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_string_unknown        // architecture not supported
} e_ttc_string_architecture;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef union {  // architecture dependent configuration

#ifdef EXTENSION_string_ascii
    t_string_ascii_config ascii;  // automatically added by ./create_DeviceDriver.pl
#endif
    //InsertArchitectureStructs above (DO NOT REMOVE THIS LINE!)
} u_ttc_string_architecture;
typedef struct s_ttc_string_config { //          architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_string_init()
    //       and after ttc_string_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    // Hint: Order structure elements for optimal alignment.
    //       You should place smaller datatypes like t_u8 and t_u16 after all t_u32 and pointer types
    //       to prevent insertion of pad-bytes. Most architectures don't support placing 32-bit datatypes at
    //       unaligned address.

    u_ttc_string_architecture* LowLevelConfig; // low-level configuration (structure defined by low-level driver)

    union  { // generic configuration bits common for all low-level Drivers
        t_u8 All;
        struct {
            unsigned Initialized        : 1;  // ==1: device has been initialized successfully
            // ToDo: additional high-level flags go here..

            unsigned Reserved1        : 7; // pad to 8 bits
        } Bits;
    } Flags;

    e_ttc_string_architecture  Architecture;   // type of architecture used for current string device

    // Additional high-level attributes go here..

} __attribute__( ( __packed__ ) ) t_ttc_string_config;
//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_STRING_TYPES_H
