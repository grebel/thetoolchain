/** { ttc_pwr.c ****************************************************
 *
 *                          The ToolChain
 *                          
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver for pwr devices.
 *
 *  Implementation of high-level interface.
 *  This file implements all functionality that is common to all supported 
 *  architectures.
 *
 *  See corresponding header file for documentation.
 *
 *  Created from template ttc_device.c revision 27 at 20150130 10:43:12 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

//{ Includes *******************************************************************
//
// C-Sources include their corresponding header file and all other header files
// that are required to compile this source file.
// Includes that provide datatypes being used by function prototypes belong into
// "ttc_pwr.h".
//
#include "ttc_pwr.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes


#if TTC_PWR_AMOUNT == 0
#  undef TTC_PWR_AMOUNT
#  define TTC_PWR_AMOUNT 1
//#  warning No PWR devices defined, check your makefile! (did you forget to activate something?)
#endif

/** { Global Variables *****************************************************
 *
 * Global variables defined here are common for all architectures of pwr devices.
 *
 */
 

// for each initialized device, a pointer to its generic and stm32l1xx definitions is stored
A_define(t_ttc_pwr_config*, ttc_pwr_configs, TTC_PWR_AMOUNT);

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//} Global Variables
//{ Function definitions ***************************************************

t_u8                ttc_pwr_get_max_index() {
    return TTC_PWR_AMOUNT;
}
t_ttc_pwr_config*   ttc_pwr_get_configuration(t_u8 LogicalIndex) {
    Assert_PWR(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_pwr_config* Config = A(ttc_pwr_configs, LogicalIndex-1); // will assert if outside array bounds

    if (!Config) {
        Config = A(ttc_pwr_configs, LogicalIndex-1) = ttc_heap_alloc_zeroed(sizeof(t_ttc_pwr_config));
        Config->Flags.Bits.Initialized = 0;       // make sure that deinit() is not called during load_defaults()
        ttc_pwr_load_defaults(LogicalIndex);
    }

    return Config;
}
void                ttc_pwr_deinit(t_u8 LogicalIndex) {
    Assert_PWR(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_pwr_config* Config = ttc_pwr_get_configuration(LogicalIndex);
  
    e_ttc_pwr_errorcode Result = _driver_pwr_deinit(Config);
    if (Result == ec_pwr_OK)
      Config->Flags.Bits.Initialized = 0;
}
e_ttc_pwr_errorcode ttc_pwr_init(t_u8 LogicalIndex) {
    Assert_PWR(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_pwr_config* Config = ttc_pwr_get_configuration(LogicalIndex);

    e_ttc_pwr_errorcode Result = _driver_pwr_init(Config);
    if (Result == ec_pwr_OK)
      Config->Flags.Bits.Initialized = 1;
    
    return Result;
}
e_ttc_pwr_errorcode ttc_pwr_load_defaults(t_u8 LogicalIndex) {
    Assert_PWR(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_pwr_config* Config = ttc_pwr_get_configuration(LogicalIndex);

    if (Config->Flags.Bits.Initialized)
      ttc_pwr_deinit(LogicalIndex);
    
    ttc_memory_set( Config, 0, sizeof(t_ttc_pwr_config) );

//    // load generic default values
//    switch (LogicalIndex) { // load type of low-level driver for current architecture
//#ifdef TTC_PWR1_DRIVER
//    case  1: Config->Architecture = TTC_PWR1_DRIVER; break;
//#endif
//#ifdef TTC_PWR2_DRIVER
//    case  2: Config->Architecture = TTC_PWR2_DRIVER; break;
//#endif
//#ifdef TTC_PWR3_DRIVER
//    case  3: Config->Architecture = TTC_PWR3_DRIVER; break;
//#endif
//#ifdef TTC_PWR4_DRIVER
//    case  4: Config->Architecture = TTC_PWR4_DRIVER; break;
//#endif
//#ifdef TTC_PWR5_DRIVER
//    case  5: Config->Architecture = TTC_PWR5_DRIVER; break;
//#endif
//#ifdef TTC_PWR6_DRIVER
//    case  6: Config->Architecture = TTC_PWR6_DRIVER; break;
//#endif
//#ifdef TTC_PWR7_DRIVER
//    case  7: Config->Architecture = TTC_PWR7_DRIVER; break;
//#endif
//#ifdef TTC_PWR8_DRIVER
//    case  8: Config->Architecture = TTC_PWR8_DRIVER; break;
//#endif
//#ifdef TTC_PWR9_DRIVER
//    case  9: Config->Architecture = TTC_PWR9_DRIVER; break;
//#endif
//#ifdef TTC_PWR10_DRIVER
//    case 10: Config->Architecture = TTC_PWR10_DRIVER; break;
//#endif
//    default: ttc_assert_halt_origin(ec_pwr_InvalidImplementation); break; // unknown architecture driver (maybe you missed to activate a low-level driver?
//    }
//    Assert_PWR( (Config->Architecture > ta_pwr_None) && (Config->Architecture < ta_pwr_unknown), ttc_assert_origin_auto); // architecture not set properly! Check makefile for TTC_PWR<n>_DRIVER and compare with e_ttc_pwr_architecture

//    Config->LogicalIndex = LogicalIndex;
//    Config->PhysicalIndex = ttc_pwr_logical_2_physical_index(LogicalIndex);
    
    //Insert additional generic default values here ...

    return _driver_pwr_load_defaults(Config);    
}
t_u8                ttc_pwr_logical_2_physical_index(t_u8 LogicalIndex) {
  
  switch (LogicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_PWR1
           case 1: return TTC_PWR1;
#endif
#ifdef TTC_PWR2
           case 2: return TTC_PWR2;
#endif
#ifdef TTC_PWR3
           case 3: return TTC_PWR3;
#endif
#ifdef TTC_PWR4
           case 4: return TTC_PWR4;
#endif
#ifdef TTC_PWR5
           case 5: return TTC_PWR5;
#endif
#ifdef TTC_PWR6
           case 6: return TTC_PWR6;
#endif
#ifdef TTC_PWR7
           case 7: return TTC_PWR7;
#endif
#ifdef TTC_PWR8
           case 8: return TTC_PWR8;
#endif
#ifdef TTC_PWR9
           case 9: return TTC_PWR9;
#endif
#ifdef TTC_PWR10
           case 10: return TTC_PWR10;
#endif
      // extend as required
      
      default: break;
    }
    
  Assert_PWR(0, ttc_assert_origin_auto); // No TTC_PWRn defined! Check your makefile.100_board_* file!
  return -1; // program flow should never go here
}
t_u8                ttc_pwr_physical_2_logical_index(t_u8 PhysicalIndex) {
  
  switch (PhysicalIndex) {           // determine base register and other low-level configuration data
#ifdef TTC_PWR1
           case TTC_PWR1: return 1;
#endif
#ifdef TTC_PWR2
           case TTC_PWR2: return 2;
#endif
#ifdef TTC_PWR3
           case TTC_PWR3: return 3;
#endif
#ifdef TTC_PWR4
           case TTC_PWR4: return 4;
#endif
#ifdef TTC_PWR5
           case TTC_PWR5: return 5;
#endif
#ifdef TTC_PWR6
           case TTC_PWR6: return 6;
#endif
#ifdef TTC_PWR7
           case TTC_PWR7: return 7;
#endif
#ifdef TTC_PWR8
           case TTC_PWR8: return 8;
#endif
#ifdef TTC_PWR9
           case TTC_PWR9: return 9;
#endif
#ifdef TTC_PWR10
           case TTC_PWR10: return 10;
#endif
      // extend as required
      
      default: break;
    }
    
  Assert_PWR(0, ttc_assert_origin_auto); // No TTC_PWRn defined! Check your makefile.100_board_* file!
  return -1; // program flow should never go here
}
void                ttc_pwr_prepare() {
  // add your startup code here (Singletasking!)
  _driver_pwr_prepare();
}
void                ttc_pwr_reset(t_u8 LogicalIndex) {
    Assert_PWR(LogicalIndex, ttc_assert_origin_auto); // logical index starts at 1
    t_ttc_pwr_config* Config = ttc_pwr_get_configuration(LogicalIndex);

    _driver_pwr_reset(Config);    
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
//{ private Function definitions *******************************************
//
// Convention: Private functions start with an underscore "_".
// Example:    void _ttc_pwr(t_u8 LogicalIndex) {  }

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
