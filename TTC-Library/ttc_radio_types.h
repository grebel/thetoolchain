/** { ttc_radio_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for RADIO device.
 *  Structures, Enums and Defines being required by both, high- and low-level radio.
 *
 *  Created from template ttc_device_types.h revision 24 at 20140514 04:32:30 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#ifndef TTC_RADIO_TYPES_H
#define TTC_RADIO_TYPES_H

//{ Includes *************************************************************
#include "compile_options.h"

#ifdef EXTENSION_radio_stm32w1xx
    #include "radio/radio_stm32w1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_radio_cc1101
    #include "radio/radio_cc1101_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
#ifdef EXTENSION_radio_dw1000
    #include "radio/radio_dw1000_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes

#include "ttc_list.h"             // list element structures
#include "ttc_basic_types.h"      // basic datatypes
#include "ttc_interrupt_types.h"  // interrupt handles
#include "ttc_packet_types.h"     // network packet structures
#include "ttc_math_types.h"       // 3D vectors
#include "ttc_systick.h"          // precise delays

//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

//{  TTC_RADIOn has to be defined as constant by makefile.100_board_*
#ifdef TTC_RADIO5
    #ifndef TTC_RADIO4
        #error TTC_RADIO5 is defined, but not TTC_RADIO4 - all lower TTC_RADIOn must be defined!
    #endif
    #ifndef TTC_RADIO3
        #error TTC_RADIO5 is defined, but not TTC_RADIO3 - all lower TTC_RADIOn must be defined!
    #endif
    #ifndef TTC_RADIO2
        #error TTC_RADIO5 is defined, but not TTC_RADIO2 - all lower TTC_RADIOn must be defined!
    #endif
    #ifndef TTC_RADIO1
        #error TTC_RADIO5 is defined, but not TTC_RADIO1 - all lower TTC_RADIOn must be defined!
    #endif

    #define TTC_RADIO_AMOUNT 5
#else
    #ifdef TTC_RADIO4
        #define TTC_RADIO_AMOUNT 4

        #ifndef TTC_RADIO3
            #error TTC_RADIO5 is defined, but not TTC_RADIO3 - all lower TTC_RADIOn must be defined!
        #endif
        #ifndef TTC_RADIO2
            #error TTC_RADIO5 is defined, but not TTC_RADIO2 - all lower TTC_RADIOn must be defined!
        #endif
        #ifndef TTC_RADIO1
            #error TTC_RADIO5 is defined, but not TTC_RADIO1 - all lower TTC_RADIOn must be defined!
        #endif
    #else
        #ifdef TTC_RADIO3

            #ifndef TTC_RADIO2
                #error TTC_RADIO5 is defined, but not TTC_RADIO2 - all lower TTC_RADIOn must be defined!
            #endif
            #ifndef TTC_RADIO1
                #error TTC_RADIO5 is defined, but not TTC_RADIO1 - all lower TTC_RADIOn must be defined!
            #endif

            #define TTC_RADIO_AMOUNT 3
        #else
            #ifdef TTC_RADIO2

                #ifndef TTC_RADIO1
                    #error TTC_RADIO5 is defined, but not TTC_RADIO1 - all lower TTC_RADIOn must be defined!
                #endif

                #define TTC_RADIO_AMOUNT 2
            #else
                #ifdef TTC_RADIO1
                    #define TTC_RADIO_AMOUNT 1
                #else
                    #define TTC_RADIO_AMOUNT 0
                #endif
            #endif
        #endif
    #endif
#endif
//}
/** { Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop.
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_RADIO 0  # disable assert handling for radio devices
 *
 */
#ifndef TTC_ASSERT_RADIO    // any previous definition set (Makefile)?
    #define TTC_ASSERT_RADIO 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_RADIO == 1)  // use Assert()s in RADIO code (somewhat slower but alot easier to debug)
    #define Assert_RADIO(Condition, Origin)        Assert(Condition, Origin)
    #define Assert_RADIO_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_RADIO_Readable(Address, Origin) Assert_Readable(Address, Origin)
    #define VOLATILE_RADIO volatile      // add to variable declarations to make them visible in debugger (no overhead when asserts are disabled)
#else  // us no Assert()s in RADIO code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_RADIO(Condition, Origin)
    #define Assert_RADIO_Writable(Address, Origin)
    #define Assert_RADIO_Readable(Address, Origin)
    #define VOLATILE_RADIO
#endif

#ifndef TTC_ASSERT_RADIO_EXTRA    // any previous definition set (Makefile)?
    #define TTC_ASSERT_RADIO_EXTRA 1  // define as zero after completing your driver!
#endif
#if (TTC_ASSERT_RADIO_EXTRA == 1)  // use Assert()s in RADIO code (somewhat slower but alot easier to debug)
    #define Assert_RADIO_EXTRA(Condition, Origin)        Assert(Condition, Origin)
    #define Assert_RADIO_EXTRA_Writable(Address, Origin) Assert_Writable(Address, Origin)
    #define Assert_RADIO_EXTRA_Readable(Address, Origin) Assert_Readable(Address, Origin)
    #define TTC_RADIO_STATISTICS 1     // gather some extra statistics in t_ttc_radio_config
#else  // use no extra Assert()s in RADIO code (somewhat smaller + faster, but crashes are hard to debug)
    #define Assert_RADIO_EXTRA(Condition, Origin)
    #define Assert_RADIO_EXTRA_Writable(Address, Origin)
    #define Assert_RADIO_EXTRA_Readable(Address, Origin)
    #define TTC_RADIO_STATISTICS 0
#endif
//}
/** { t_ttc_radio_architecture - Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_radio_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_radio_architecture
    #warning Missing low-level definition for t_ttc_radio_architecture (using default)
    #define t_ttc_radio_architecture t_u8
#endif
//}
/** { ttc_radio_virtual_channel_e - Architecture independent channel enumerator
 *
 * For a simple radio, the low-level driver may map each virtual channel directly onto one frequency.
 * Example for a pseudo radio_foo_types.h file:
 * typedef enum { radio_foo_virtual1_frequency_433_1, radio_foo_virtual2_frequency_434_1, ... } e_radio_foo_virtualchannel;
 * #define ttc_radio_virtual_channel_e e_radio_foo_virtualchannel
 *
 * Some radios use special techniques to avoid interference of different networks on the same frequency.
 * Such techniques may be (among others):
 * - different preamble codes
 * - different channel hopping permutations
 * - different bandwidth
 * - different symbol encoding
 *
 * A more complex radio can define a virtual channel as a combination of carrier frequency and
 * any parameter of above techniques. See e_radio_dw1000_virtual_channel for an example.
 */
#ifndef ttc_radio_virtual_channel_e
    #warning Missing low-level definition for ttc_radio_virtual_channel_e (using default type)
    #define ttc_radio_virtual_channel_e t_u8
#endif
//}
/** { Check if definitions of logical indices are taken from e_ttc_radio_physical_index
 *
 * See definition of e_ttc_radio_physical_index below for details.
 *
 */

#define TTC_INDEX_RADIO_MAX 10  // maximum amount of supported radios

#ifdef TTC_RADIO1
    #if TTC_RADIO1 >= TTC_INDEX_RADIO_MAX
        #    error Wrong definition of TTC_RADIO1, check your makefile (must be one from e_ttc_radio_physical_index)!
    #endif

    // check for static radio configuration (set in makefile)
    #ifndef TTC_RADIO1_DRIVER
        #    warning Missing definition, check your makefile: TTC_RADIO1_DRIVER (choose one from e_ttc_radio_architecture)
        #define TTC_RADIO1_DRIVER ta_radio_None
    #endif
    #ifndef TTC_RADIO1_ALTERNATE_TX_PATH
        #define TTC_RADIO1_ALTERNATE_TX_PATH 0
    #endif
    #ifndef TTC_RADIO1_BOOST_MODE
        #define TTC_RADIO1_BOOST_MODE 0
    #endif
#endif
#ifdef TTC_RADIO2
    #if TTC_RADIO2 >= TTC_INDEX_RADIO_MAX
        #    error Wrong definition of TTC_RADIO2, check your makefile (must be one from e_ttc_radio_physical_index)!
    #endif

    // check for static radio configuration (set in makefile)
    #ifndef TTC_RADIO2_DRIVER
        #    warning Missing definition, check your makefile: TTC_RADIO2_DRIVER (choose one from e_ttc_radio_architecture)
        #define TTC_RADIO2_DRIVER ta_radio_None
    #endif
    #ifndef TTC_RADIO2_ALTERNATE_TX_PATH
        #define TTC_RADIO2_ALTERNATE_TX_PATH 0
    #endif
    #ifndef TTC_RADIO2_BOOST_MODE
        #define TTC_RADIO2_BOOST_MODE 0
    #endif
#endif
#ifdef TTC_RADIO3
    #if TTC_RADIO3 >= TTC_INDEX_RADIO_MAX
        #    error Wrong definition of TTC_RADIO3, check your makefile (must be one from e_ttc_radio_physical_index)!
    #endif

    // check for static radio configuration (set in makefile)
    #ifndef TTC_RADIO3_DRIVER
        #    warning Missing definition, check your makefile: TTC_RADIO3_DRIVER (choose one from e_ttc_radio_architecture)
        #define TTC_RADIO3_DRIVER ta_radio_None
    #endif
    #ifndef TTC_RADIO3_ALTERNATE_TX_PATH
        #define TTC_RADIO3_ALTERNATE_TX_PATH 0
    #endif
    #ifndef TTC_RADIO3_BOOST_MODE
        #define TTC_RADIO3_BOOST_MODE 0
    #endif
#endif
#ifdef TTC_RADIO4
    #if TTC_RADIO4 >= TTC_INDEX_RADIO_MAX
        #    error Wrong definition of TTC_RADIO4, check your makefile (must be one from e_ttc_radio_physical_index)!
    #endif

    // check for static radio configuration (set in makefile)
    #ifndef TTC_RADIO4_DRIVER
        #    warning Missing definition, check your makefile: TTC_RADIO4_DRIVER (choose one from e_ttc_radio_architecture)
        #define TTC_RADIO4_DRIVER ta_radio_None
    #endif
    #ifndef TTC_RADIO4_ALTERNATE_TX_PATH
        #define TTC_RADIO4_ALTERNATE_TX_PATH 0
    #endif
    #ifndef TTC_RADIO4_BOOST_MODE
        #define TTC_RADIO4_BOOST_MODE 0
    #endif
#endif
#ifdef TTC_RADIO5
    #if TTC_RADIO5 >= TTC_INDEX_RADIO_MAX
        #    error Wrong definition of TTC_RADIO5, check your makefile (must be one from e_ttc_radio_physical_index)!
    #endif

    // check for static radio configuration (set in makefile)
    #ifndef TTC_RADIO5_DRIVER
        #    warning Missing definition, check your makefile: TTC_RADIO5_DRIVER (choose one from e_ttc_radio_architecture)
        #define TTC_RADIO5_DRIVER ta_radio_None
    #endif
    #ifndef TTC_RADIO5_ALTERNATE_TX_PATH
        #define TTC_RADIO5_ALTERNATE_TX_PATH 0
    #endif
    #ifndef TTC_RADIO5_BOOST_MODE
        #define TTC_RADIO5_BOOST_MODE 0
    #endif
#endif
#ifdef TTC_RADIO6
    #if TTC_RADIO6 >= TTC_INDEX_RADIO_MAX
        #    error Wrong definition of TTC_RADIO6, check your makefile (must be one from e_ttc_radio_physical_index)!
    #endif

    // check for static radio configuration (set in makefile)
    #ifndef TTC_RADIO6_DRIVER
        #    warning Missing definition, check your makefile: TTC_RADIO6_DRIVER (choose one from e_ttc_radio_architecture)
        #define TTC_RADIO6_DRIVER ta_radio_None
    #endif
    #ifndef TTC_RADIO6_ALTERNATE_TX_PATH
        #define TTC_RADIO6_ALTERNATE_TX_PATH 0
    #endif
    #ifndef TTC_RADIO6_BOOST_MODE
        #define TTC_RADIO6_BOOST_MODE 0
    #endif
#endif
#ifdef TTC_RADIO7
    #if TTC_RADIO7 >= TTC_INDEX_RADIO_MAX
        #    error Wrong definition of TTC_RADIO7, check your makefile (must be one from e_ttc_radio_physical_index)!
    #endif

    // check for static radio configuration (set in makefile)
    #ifndef TTC_RADIO7_DRIVER
        #    warning Missing definition, check your makefile: TTC_RADIO7_DRIVER (choose one from e_ttc_radio_architecture)
        #define TTC_RADIO7_DRIVER ta_radio_None
    #endif
    #ifndef TTC_RADIO7_ALTERNATE_TX_PATH
        #define TTC_RADIO7_ALTERNATE_TX_PATH 0
    #endif
    #ifndef TTC_RADIO7_BOOST_MODE
        #define TTC_RADIO7_BOOST_MODE 0
    #endif
#endif
#ifdef TTC_RADIO8
    #if TTC_RADIO8 >= TTC_INDEX_RADIO_MAX
        #    error Wrong definition of TTC_RADIO8, check your makefile (must be one from e_ttc_radio_physical_index)!
    #endif

    // check for static radio configuration (set in makefile)
    #ifndef TTC_RADIO8_DRIVER
        #    warning Missing definition, check your makefile: TTC_RADIO8_DRIVER (choose one from e_ttc_radio_architecture)
        #define TTC_RADIO8_DRIVER ta_radio_None
    #endif
    #ifndef TTC_RADIO8_ALTERNATE_TX_PATH
        #define TTC_RADIO8_ALTERNATE_TX_PATH 0
    #endif
    #ifndef TTC_RADIO8_BOOST_MODE
        #define TTC_RADIO8_BOOST_MODE 0
    #endif
#endif
#ifdef TTC_RADIO9
    #if TTC_RADIO9 >= TTC_INDEX_RADIO_MAX
        #    error Wrong definition of TTC_RADIO9, check your makefile (must be one from e_ttc_radio_physical_index)!
    #endif

    // check for static radio configuration (set in makefile)
    #ifndef TTC_RADIO9_DRIVER
        #    warning Missing definition, check your makefile: TTC_RADIO9_DRIVER (choose one from e_ttc_radio_architecture)
        #define TTC_RADIO9_DRIVER ta_radio_None
    #endif
    #ifndef TTC_RADIO9_ALTERNATE_TX_PATH
        #define TTC_RADIO9_ALTERNATE_TX_PATH 0
    #endif
    #ifndef TTC_RADIO9_BOOST_MODE
        #define TTC_RADIO9_BOOST_MODE 0
    #endif
#endif
#ifdef TTC_RADIO10
    #if TTC_RADIO10 >= TTC_INDEX_RADIO_MAX
        #    error Wrong definition of TTC_RADIO10, check your makefile (must be one from e_ttc_radio_physical_index)!
    #endif

    // check for static radio configuration (set in makefile)
    #ifndef TTC_RADIO10_DRIVER
        #    warning Missing definition, check your makefile: TTC_RADIO10_DRIVER (choose one from e_ttc_radio_architecture)
        #define TTC_RADIO10_DRIVER ta_radio_None
    #endif
    #ifndef TTC_RADIO10_ALTERNATE_TX_PATH
        #define TTC_RADIO10_ALTERNATE_TX_PATH 0
    #endif
    #ifndef TTC_RADIO10_BOOST_MODE
        #define TTC_RADIO10_BOOST_MODE 0
    #endif
#endif
//}

#define TTC_RADIO_MAX_PATTERNS 4  // maximum amount of supported search/ remove patterns

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef struct s_ttc_radio_distance { // distance to a remote node
    t_ttc_packet_address RemoteID;     // network identifier of remote node
    t_base               Distance_cm;  // distance to remote node in centimeter
} t_ttc_radio_distance;

typedef e_ttc_physical_index e_ttc_radio_physical_index;

typedef enum {   // e_ttc_radio_errorcode      return codes of RADIO devices
    ec_radio_OK = 0,

    // other warnings go here..
    ec_radio_NoChange,               // no action taken

    // errors below
    ec_radio_ERROR,                  // general failure
    ec_radio_NotImplemented,         // requested feature is not implemented for current architecture
    ec_radio_OutOfBuffers,           // Cannot allocate buffer for new packet. Either someone is not releasing buffers or the memory pool is too small.
    ec_radio_IsLocked,               // Radio has been locked by another user. Your buffer has not been stored or released. Try again later!
    ec_radio_RangingDeactivated,     // ranging feature has been deactivated in radio configuration
    ec_radio_Busy,                   // transceiver is busy; action canceld
    ec_radio_NULL,                   // NULL pointer not accepted
    ec_radio_DeviceNotFound,         // corresponding device could not be found
    ec_radio_InvalidConfiguration,   // sanity check of device configuration failed
    ec_radio_InvalidImplementation,  // your code does not behave as expected
    ec_radio_NoMetaData,             // no meta data could be found for given packet
    ec_radio_InvalidArgument,        // check your arguments
    ec_radio_UnsupportedPacketType,  // given packet type is currently not supported: Try another type or implement it!
    ec_radio_DeviceInitialiseFailed, // device initialise has failed
    ec_radio_DelayedReceiveFailed,   // a delayed receive could not be started  (delay time to short?)
    ec_radio_DelayedTransmitFailed,  // a delayed transmission could not be started (delay time to short?)
    ec_radio_TransmissionFailed,     // an immediate transmission failed (unknown reason)
    ec_radio_NodeIDerror,
    ec_radio_TimeOut_unknown,        // generic timeout occured while waiting for radio (unknown reason)
    ec_radio_TimeOut_ACK,            // timeout occured while waiting for acknowledge packet from remote node
    // other failures go here..

    ec_radio_unknown                 // no valid errorcodes past this entry
} e_ttc_radio_errorcode;
typedef enum {   // e_ttc_radio_architecture  types of architectures supported by RADIO driver
    ta_radio_None,           // no architecture selected


    ta_radio_stm32w1xx, // automatically added by ./create_DeviceDriver.pl
    ta_radio_cc1101, // automatically added by ./create_DeviceDriver.pl
    ta_radio_dw1000, // automatically added by ./create_DeviceDriver.pl
    E_ttc_radio_architecture_dw1000, // automatically added by ./create_DeviceDriver.pl
    E_ttc_radio_architecture_stm32w1xx, // automatically added by ./create_DeviceDriver.pl
    //InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

    ta_radio_unknown        // architecture not supported
} e_ttc_radio_architecture;
typedef enum {   // e_ttc_radio_status        status codes of radio transceiver
    E_ttc_radio_status_none = 0,      // status reset code
    E_ttc_radio_status_tx_started,    // a packet transmission has started
    E_ttc_radio_status_tx_too_late,   // a delayed transmission was delivered too late (-> increase delay time) (Note: must be placed after E_ttc_radio_status_tx_started!)
    E_ttc_radio_status_tx_complete,   // a packet was transmitted successfully

    E_ttc_radio_status_unknown                 // no valid codes past this entry
} e_ttc_radio_status;
typedef enum { // e_ttc_radio_gpio            named gpio pins connected to radio transceiver

    // Some radio transceivers provide software configurable gpio pins.
    // This list gives some names of pins which might be available
    E_ttc_radio_gpio_none,        //
    E_ttc_radio_gpio_status_tx,   // Transmit Status pin (often connected to a LED)
    E_ttc_radio_gpio_status_rx,   // Receive Status pin (often connected to a LED)
    // add more entries here!

    E_ttc_radio_gpio_unknown      // invalid values below
} e_ttc_radio_gpio;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

typedef struct s_ttc_radio_features { // static features of single radio
    // Note: Content is ordered by itemsize for optimal aligned memory addresses

    const t_base MinBitRate;       // minimum supported bitrate
    const t_base DefaultBitRate;   // default bitrate (bits/s)
    const t_base MaxBitRate;       // maximum supported bitrate
    const t_s16  DefaultTxLevel;   // default transmit level (1/10 dB)
    const t_s16  DefaultRxLevel;   // default receive  level (1/10 dB)
    const t_s16  MaxTxLevel;       // maximum supported Tx level (1/10 dB)
    const t_s16  MaxRxLevel;       // maximum supported Rx sensitivity (1/10 dB)
    const t_s16  MinTxLevel;       // minimum supported Tx level (1/10 dB)
    const t_s16  MinRxLevel;       // minimum supported Rx sensitivity (1/10 dB)
    const t_u8   MaxChannel;       // highest supported channel
    const t_u8   MinChannel;       // lowest supported channel
    const t_u8   DefaultRxChannel; // receive channel to use at startup
    const t_u8   DefaultTxChannel; // transmit channel to use at startup
    const t_u16  MaxPacketSize;    // maximum supported raw packet size


    // features below apply to ranging radios only (e.g. radio_dw1000)

    /* bitmask being applied for transmit time of delayed transmits
     * Ranging radios typically have a limited precision for delayed tx/rx.
     * This mask is required to calculate exact delays before a transmission takes place.
     * E.g. if lowest 9 bits are ignored: DelayedTimeMask = 0xffffffff - 0b111111111
     */
    const t_u32  DelayedTimeMask;

    /* Minimum usefull amount of clock cycles of round trip time used in two way ranging.
     * All ranging radios have a minimum usefull ranging distance dependending on their hardware.
     * Round trip times below this value will simply being set to zero.
     * See radio_common_ranging_measure() for details.
     */
    const t_u32  MinRoundTripTimeClockCycles;

    const t_u32 DelayTimerFrequency_kHz; // frequency of system clock used to take time stamps (kHz)
    const t_u32 DelayTimerPeriod_fs;     // amount of femtoseconds (1e-15s) of single sytem time counter step

} __attribute__( ( __packed__ ) ) t_ttc_radio_features;
typedef union { // aechitecture independent frame filter configuration
    t_u8 All;
    struct {
        unsigned WithoutDestinationID : 1; // frames with no Destination ID
        unsigned ForeignDestinationID : 1; // frames with non-matching Destination ID
        unsigned ForeignPanID         : 1; // frames with non-matching PAN ID
        unsigned Beacon               : 1; // beacon type frames
        unsigned Data                 : 1; // data type frames
        unsigned Acknowledge          : 1; // acknowledgement frames
        unsigned MAC_Control          : 1; // MAC control frames
        unsigned ReservedTypes        : 1; // Reserved frame types
    } Allowed;
} u_ttc_radio_frame_filter;
typedef struct { // input and return data of packet list iterators

    struct { // input data to list iterator
        // protocol type patterns to look for (arguments for ttc_packet_pattern_matches())
        e_ttc_packet_pattern Patterns[TTC_RADIO_MAX_PATTERNS]; // Patterns[i]==0: pattern inactive; >0: search for this protocol type
        t_u8                 Patterns_Amount;                  // amount of valid entries in Patterns[]
        struct {
            unsigned OnlyFirst   : 1;  // ==TRUE: ignore all list items after first matching pattern
            unsigned RemoveAll   : 1;  // ==TRUE: list iterator will return TRUE for every list item for which at least one pattern matches
            unsigned RemoveFirst : 1;  // ==TRUE: list iterator will return TRUE for first list item for which at least one pattern matches
        } Flags;
    } Input;

    struct { // return data from list iterator
        t_u8             Amount;            // amount of packets in single linked list (Must be set to 0 before calling ttc_list_iterate()!)
        t_ttc_list_item* PacketFound_First; // !=NULL: pointer to first packet of single linked list of all packets being found
        t_ttc_list_item* PacketFound_Last;  // !=NULL: pointer to last  packet of single linked list of all packets being found
    } Return;
} __attribute__( ( packed ) ) t_radio_common_socket_iterator;
typedef struct { // t_ttc_radio_job_socket - statemachine data for optimized lookup for radio_common_packet_received_tryget()
    struct { // data fields to be initialized by function caller before first call
        e_ttc_packet_pattern Pattern;     // certain socket identifier or search pattern
        BOOL                 Initialized; // set to 0!
    } Init;

    struct { // data fields being private to radio_common_packet_received_tryget()
        t_radio_common_socket_iterator SearchJob;   // configuration of search job for list iterator
    } Private;
} __attribute__( ( packed ) ) t_ttc_radio_job_socket;
typedef struct { // t_ttc_radio_payload_handshake - payload area of a 3-way handshake
    t_u8  UserID;      // identifier of ttc_radio user that requested this handshake
    t_u16 TimeOut_MS;  // timeout (milliseconds) during which originator expects an answer
} __attribute__( ( packed ) ) t_ttc_radio_payload_handshake;
typedef struct s_ttc_radio_config { //         architecture independent configuration data
    // Note: Content is ordered by itemsize to allow optimal aligned memory addresses with smallest structure size

    struct { // Init: fields to be set by application before first calling ttc_states_init() --------------
        // Do not change these values after calling ttc_states_init()!

        struct { // generic configuration bits common for all low-level Drivers
            unsigned DelayedTransmits       : 1;  // ==1: queues are used to allow immediate return of transmit functions
            unsigned DelayedReceives        : 1;  // ==1: ???
            unsigned AppendCRC              : 1;  // ==1: append checksum to every packet (recommended)
            unsigned AutoAcknowledge        : 1;  // ==1: send/ receive auto acknowledgements
            unsigned AddressFilter          : 1;  // ==1: hardware address filter will pass only messages with correct pan/ node id
            unsigned UseAlternateTxPath     : 1;  // ==1: alternate transmit path will be used (if available)
            unsigned PinTxActive_LowLevel   : 1;  // ==1: a low-level radio_DEVIC_init() can set this bit to block ttc_radio from controlling it
            unsigned PinTxActive_LowActive  : 1;  // ==1: gpio pin must be cleared to activate transmit mode of amplifier
            unsigned PinAmp_LowActive       : 1;  // ==1: gpio pin must be cleared to enable amplifier
            unsigned RxAutoReenable         : 1;  // ==1: automatically put radio into receive mode after transmission or error (see also ReturnDelay_uSecs)
            unsigned EnableRX               : 1;  // ==1: enable receiving data from radio;   ==0: ignore incoming packets
            unsigned EnableTX               : 1;  // ==1: enable transmitting data via radio; ==0: ignore outgoing packets
            unsigned EnableDoubleBuffering  : 1;  // ==1: enable double buffered transmit/ receive for higher data throughput; ==0: use single buffering
            unsigned EnableRanging          : 1;  // ==1: enable range measuring (requires radio with special capability)
            unsigned EnableRangingReplies   : 1;  // ==1: enable automatic replies to ranging requests (implemented in radio_common.c)
            unsigned EnableRSSI             : 1;  // ==1: enable Received Signal Strength Indication (may reduce packet rate)

            // some transceivers provide configurable pins for special purposes
            unsigned Enable_LED_RXOK        : 1;  // ==1: enable LED output pin for successfully received packets     (if supported by current transceiver)
            unsigned Enable_LED_RX          : 1;  // ==1: enable LED output pin for any received packets              (if supported by current transceiver)
            unsigned Enable_LED_TX          : 1;  // ==1: enable LED output pin for outgoing packets                  (if supported by current transceiver)
            unsigned Enable_Output_PA       : 1;  // ==1: enable GPIO output pin to activate external power amplifier (if supported by current transceiver)
            unsigned Enable_Output_IRQ      : 1;  // ==1: enable GPIO output pin to signal an interrupt request       (if supported by current transceiver)

        } Flags;

#if TTC_PACKET_RANGING==1
        // !=NULL: pointer to a vector storing current location of this node
        // Setting this value is required to reply to incoming localization requests
        t_ttc_math_vector3d_xyz* MyLocation;
#endif

        // >0: amount of retries for certain functions using acknowledge for 2-way handshaking (e.g. ttc_radio_transmit_buffer() )
        t_u8  Transmit_Amount_Retries;

        // Level of Transmit Amplifier (1/10 dB) = Features->MinTxLevel ..  Features->MaxTxLevel
        t_s16 LevelTX;

        // Level of Receive Amplifier (1/10 dB)  = Features->MinRxLevel ..  Features->MaxRxLevel
        t_s16 LevelRX;

        /* maximum allowed packet size
         * some radios (e.g. dw1000) support larger, non-standard packets
         *
         *  Note: May only be set once before first ttc_radio_init() call!
         */
        t_u16 MaxPacketSize;

        /* Amount of memory buffers to allocate for ttc_radio_packet_get_empty()
         *
         * The total amount of allocated packet buffers can be increased by calling ttc_radio_user_new().
         * All packet buffers are stored in Pool_Packets below.
         * After usage, packet buffers have to be released back to the pool for reuse:
         * - Packets to be transmitted are released automatically by ttc_radio.
         * - Packtes to be transmitted which could not be transmitted now may be held until successfull
         *   transmission if egnough packet buffers are available for operation.
         * - Packets being received via () have to be released by calling ttc_radio_packet_release().
         *
         * Note: ttc_radio may assert if Pool_Packets runs empty!
         */
        t_u16 MaxAmountPackets;

        // pin controlling external power amplifier (activated immediately before transmit)
        e_ttc_gpio_pin Pin_TxActive;

        // pin controlling power of external power amplifier (activated at wakeup)
        e_ttc_gpio_pin Pin_Enable_Amplifier;

        // raw data rate used for transmissions (bits/second)
        t_u32 RawBitrate;

        // >0: general timeout used to detect hanging radio operations; ==0: no timeouts (faster transmits but software cannot check if packets have been transmitted)
        // Tipp: Set to zero when software runs stable to improve bandwith
        t_u32 Transceiver_Timeout_usecs;

        // >0: time to wait for ack packet for transmitted packet with enabled ACK flag; ==0: disable waiting for acknowledge packets
        t_u32 Acknowledge_Timeout_usecs;

        /** Activity function that will be called whenever receiver detects a preamble.
         *
         * Preamble detection indicates an ongoing transmission. If a fram can be decoded,
         * it still may be rejected by an enabled frame filter.
         *
         * Note: Registered function is called directly from interrupt service routine. Keep it short and return fast!
         *
         * @param  Config   configuration of radio  that has received this packet
         */
        BOOL ( *function_preamble_detected_isr )( struct s_ttc_radio_config* Config );

        /** Activity function that will be called whenever frame receiving has started.
         *
         * Note: Registered function is called directly from interrupt service routine. Keep it short and return fast!
         *
         * @param  Config   configuration of radio  that has received this packet
         */
        void ( *function_start_rx_isr )( struct s_ttc_radio_config* Config );

        /** Activity function that will be called after successfull receival of single packet before it is pushed to List_PacketsRx.
         *
         * Note: If function_start_rx_isr() was called but not function_end_rx_isr(), then frame receival had to be canceled.
         * Note: Registered function is called directly from interrupt service routine. Keep it short and return fast!
         *
         * @param  Config   configuration of radio  that has received this packet
         * @param  Packet   binary data being received
         * @return ==TRUE:  function has processed and released Packet via ttc_heap_pool_block_free()
         *         ==FALSE: Packet will be added to List_PacketsRx for later processing
         */
        BOOL ( *function_end_rx_isr )( struct s_ttc_radio_config* Config, t_ttc_packet* Packet );

        /** Activity functions for outgoing packets
         * function reference: !=NULL: registered function is called before packet is send out
         * Note: Registered function is called directly from interrupt service routine
         *       before/after transmitting the packet. Keep it short and return fast!
         *
         * @param  Config   configuration of radio  that has received this packet
         * @param  Packet   binary data being received
         * @return ==TRUE:  function has processed and released Packet via ttc_heap_pool_block_free()
         *         ==FALSE: Packet will be added to List_PacketsRx for later processing
         */
        BOOL ( *function_start_tx_isr )( struct s_ttc_radio_config* Config, t_ttc_packet* Packet );
        BOOL ( *function_end_tx_isr )( struct s_ttc_radio_config* Config, t_ttc_packet* Packet );

    } Init;

    // Fields below are read-only for application! __________________________________________________________

    /** ID-Field LocalID
     *
     * This ID-field is used for outgoing and incoming packets.
     * Applications must not not change these fields directly.
     * Use ttc_radio_change_destination_address() and ttc_radio_change_local_address() instead!
     */
    t_ttc_packet_address LocalID;

    // !=NULL: pointer to packet meta header where to store transmission status (PacketMeta->StatusTX will be updated)
    t_ttc_packet_meta* volatile PacketTX_Meta;

    // ==1: Transmission is ongoing; ==0: no transmission active (TX queue is empty)
    t_ttc_mutex_smart FlagRunningTX;

    // ==1: Receiver is active; ==0: Receiver has been disabled
    t_ttc_mutex_smart Flag_ReceiverOn;

    // list of packets being queued for transmit
    t_ttc_list List_PacketsTx __attribute__( ( aligned( 4 ) ) );

    // list of packets being received and not yet processed
    t_ttc_list List_PacketsRx __attribute__( ( aligned( 4 ) ) );

    // general delay used for timeout safety checks (configured via Init.Transceiver_Timeout_usecs)
    t_ttc_systick_delay TimeOut;

    // Handle of external interrupt input from radio
    // Most external radio transceivers provide an extra interrupt request line to signal the uC
    t_ttc_interrupt_handle RadioInterrupt;

    /* pool of equal sized memory blocks used for data packets
     *
     * Size of memory blocks in this pool is calculated automatically from MaxPacketSize
     * as the payload of each packet must be able to store biggest binary packet frame.
     *
     * Note: Memory pools cannot be resized or reallocated during runtime.
     */
    t_ttc_heap_pool Pool_Packets;

    // constant features of this radio
    const t_ttc_radio_features* Features;

    // low-level configuration (structure defined by low-level driver)
    t_ttc_radio_architecture LowLevelConfig;

    union  { // generic configuration bits common for all low-level Drivers
        unsigned Initialized : 1;                 // ==1: device has been initialized successfully
        // additional high-level flags go here..
    } Flags;

    // frame filter configuration (low-level driver has to configure radio accordingly)
    u_ttc_radio_frame_filter FrameFilter;

    e_ttc_radio_architecture Architecture; // type of architecture used for current radio device
    t_u8 LogicalIndex;             // automatically set: logical index of device to use (1 = TTC_RADIO1, ...)
    t_u8 PhysicalIndex;            // automatically set: physical index of device to use (0 = first hardware device, ...)
    t_u8 UserID_Lock;              // ==0: ttc_radio can be used by everyone; >0: only function calls with matching UserID are processed
    t_u8 UserID_LockCount;         // increased on every lock; decreased on every unlock
    t_u8 UserID_Max;               // highest registered UserID (set by ttc_radio_user_new())
    t_u8 SequenceNo_Beacons;       // current sequence number used for outgoing beacon type packets (increased by 1 on every use)
    t_u8 SequenceNo_Generic;       // current sequence number used for other outgoing packet types  (increased by 1 on every use)
    t_u8 MaxProtocol_Application;  // >0: latest dynamically assigned e_ttc_packet_pattern.ttc_packet_pattern_protocol_AnyOf_Application* value (->ttc_radio_socket_new())
    t_u8 Received_HandshakeUserID; // UserID from latest received handshake answer (-> _ttc_radio_handle_special() )

    /* Status of latest transmission as reported from radio transceiver.
     *
     * This variable must be updated by interrupt service routine of low-level driver.
     * Its mainly used by radio_common_ranging_reply_isr() to detect delayed transmissions which were given to transceiver too late.
     *
     */
    e_ttc_radio_status StatusTX;


    // current channel used for transmission (Features->MinChannel .. Features->MaxChannel)
    ttc_radio_virtual_channel_e  ChannelTX;

    // current channel used for receive (Features->MinChannel .. Features->MaxChannel)
    ttc_radio_virtual_channel_e  ChannelRX;

    /* Delay return messages to this exact time period (microseconds).
     * Some applications require a constant answer period. The time required to process a message
     * may not ensure this constant delay. Some radios (like DW1000) provide exact transmit delays
     * in hardware.
     * The lower this delay is set, the harder it will be for the software to process an answer in time.
     * If the software cannot start transmission of answer within return delay, transmission will fail.
     * If this happens, you either have to optimize your software or increase the ReturnDelay value.
     * The optimal value depends on the currently used microcontroller and the application that has
     * to process incomming data and prepare answers.
     *
     * Note: Change these values via ttc_radio_change_delay_time() / ttc_radio_change_delay_time() !
     */
    t_u16 Delay_TX_us;  // >0: automatically switch on transceiver after Delay_TX usecs
    t_u16 Delay_RX_us;  // >0: automatically switch on receiver after Delay_RX usecs

#if (TTC_RADIO_STATISTICS == 1)
    struct {
        // Note: Increas counter sizes if required!
        t_u8 Amount_Tx;                 // amount of transmitted packets since initialisation
        t_u8 Amount_Rx;                 // amount of received packets since initialisation
        t_u8 Amount_DroppedRx;          // amount of incoming packets dropped because no empty packet buffers were available (Application must call ttc_radio_packet_received_tryget() regulary and pass received buffers to ttc_radio_packet_release() for reuse to keep this value small!)
        t_u8 Amount_DroppedTx;          // amount of outgoing packets dropped
        t_u8 Amount_FailedTx;           // amount of outgoing packets that could not be sent by radio
        t_u8 Amount_IncreasedDelayRX;   // whenever receiver shall be activated at a time in the past, then Delay_RX_us is increased automatically.
        t_u8 Amount_IncreasedDelayTX;   // whenever packet has to be sent at a time and its data is too late, then Delay_TX_us is increased automatically.
        t_u8 Amount_UnidentifiedRx;     // amount of incoming packets dropped because they could not be identified
        t_u8 Amount_FailedCRC;          // amount of received packets dropped because of failed checksum
        t_u8 Amount_FailedACKs;         // amount of requested acknowledge packets that did not arrive within timeout
        t_u8 Amount_ReceiverRestart;    // amount of forced restarts of receiver (may indicate communication failure with radio device)
        t_u8 Amount_ListRX_Pop;         // amount of successfull radio_common_pop_list_rx_try() calls
        t_u8 Amount_ListRX_PopISR;      // amount of successfull radio_common_pop_list_rx_try_isr() calls
        t_u8 Amount_ListRX_Push;        // amount of successfull radio_common_push_list_rx() calls
        t_u8 Amount_ListRX_PushISR;     // amount of successfull radio_common_push_list_rx_isr() calls
        t_u8 Amount_ListTX_Pop;         // amount of successfull radio_common_pop_list_tx() calls
        t_u8 Amount_ListTX_PopISR;      // amount of successfull radio_common_pop_list_tx_isr() calls
        t_u8 Amount_ListTX_Push;        // amount of successfull radio_common_push_list_tx() calls
        t_u8 Amount_ListTX_PushISR;     // amount of successfull radio_common_pus_list_tx_isr() calls
        t_u8 Amount_ListTX_Prepend;     // amount of successfull radio_common_prepend_list_tx() calls
        t_u8 Amount_ListTX_PrependISR;  // amount of successfull radio_common_prepend_list_tx_isr() calls

    } Statistics;
#endif

#if TTC_PACKET_RANGING==1

    /* After transmission, the signal has to reach the antenna.
     * The delay must be included in transmission time that is to be sent in each
     * ranging reply message BEFORE it is sent.
     * Accurate ranging requires that low-level driver updates this value whenever switching channel!
     */
    t_u16 TransmitTime_AntennaDelay;

    // timestamp of last transmitted ranging frame (RNG bit in PHY Header was set)
    u_ttc_packetimestamp_40 LastTransmitTime;

    // timestamp of last received ranging frame (RNG bit in PHY Header was set)
    u_ttc_packetimestamp_40 LastReceiveTime; //DEBUG

    // Radio specific constants required to calculate distance between two nodes from returned round trip time
    // Values have to be set by _driver_radio_load_defaults().
    // DistanceMM = (RoundTripTime_picoseconds - RoundTripTime2Distance_Subtract)
    //              * RoundTripTime2Distance_Multiplier
    //              /  RoundTripTime2Distance_Divider;
    //
    // Note: It may be necessary to adjust these values for each type of radio pcb!
    // Note: see radio_common_ranging_measure() for usage!
    t_base RoundTripTime2Distance_Subtract;
    t_base RoundTripTime2Distance_Multiplier; // note: keeping this value small can make computation faster
    t_base RoundTripTime2Distance_Divider;    // note: keeping this value small can make computation faster

    // amount of ranging requests being replied so far (->radio_common_push_list_rx_isr() )
    t_u16  Amount_RangingReplies;

    // TX-delay between receiving a ranging request and sending out a reply message (microseconds)
    // use set_ranging_delay() to set this value!
    t_u32  Delay_RangingReply_us;

    // architecture specific delay value as calculated by set_ranging_delay()
    t_u32  Delay_RangingReply_clocks;

#endif

    // Additional high-level attributes go here..
} __attribute__( ( __packed__ ) ) t_ttc_radio_config;

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_RADIO_TYPES_H
