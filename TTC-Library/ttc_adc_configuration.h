#ifndef TTC_ADC_CONFIGURATION_H
#define TTC_ADC_CONFIGURATION_H

/** { ttc_adc_configuration.h *******************************************************
 *
 * Static adc configuration has been placed in extra header file to keep ttc_adc_types.h readable
 *
 * TTC_ADCn has to be defined as constant by makefile.100_board_* or makefile.150_board_extension_*
 *
 */

// Determine amount of static configured analog inputs
//{ TTC_ADC64
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC64
        #define TTC_ADC_AMOUNT_STATIC 64
    #endif
#endif
//}
//{ TTC_ADC63
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC63
        #define TTC_ADC_AMOUNT_STATIC 63
    #endif
#endif
//}
//{ TTC_ADC62
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC62
        #define TTC_ADC_AMOUNT_STATIC 62
    #endif
#endif
//}
//{ TTC_ADC61
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC61
        #define TTC_ADC_AMOUNT_STATIC 61
    #endif
#endif
//}
//{ TTC_ADC60
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC60
        #define TTC_ADC_AMOUNT_STATIC 60
    #endif
#endif
//}
//{ TTC_ADC59
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC59
        #define TTC_ADC_AMOUNT_STATIC 59
    #endif
#endif
//}
//{ TTC_ADC58
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC58
        #define TTC_ADC_AMOUNT_STATIC 58
    #endif
#endif
//}
//{ TTC_ADC57
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC57
        #define TTC_ADC_AMOUNT_STATIC 57
    #endif
#endif
//}
//{ TTC_ADC56
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC56
        #define TTC_ADC_AMOUNT_STATIC 56
    #endif
#endif
//}
//{ TTC_ADC55
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC55
        #define TTC_ADC_AMOUNT_STATIC 55
    #endif
#endif
//}
//{ TTC_ADC54
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC54
        #define TTC_ADC_AMOUNT_STATIC 54
    #endif
#endif
//}
//{ TTC_ADC53
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC53
        #define TTC_ADC_AMOUNT_STATIC 53
    #endif
#endif
//}
//{ TTC_ADC52
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC52
        #define TTC_ADC_AMOUNT_STATIC 52
    #endif
#endif
//}
//{ TTC_ADC51
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC51
        #define TTC_ADC_AMOUNT_STATIC 51
    #endif
#endif
//}
//{ TTC_ADC50
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC50
        #define TTC_ADC_AMOUNT_STATIC 50
    #endif
#endif
//}
//{ TTC_ADC49
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC49
        #define TTC_ADC_AMOUNT_STATIC 49
    #endif
#endif
//}
//{ TTC_ADC48
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC48
        #define TTC_ADC_AMOUNT_STATIC 48
    #endif
#endif
//}
//{ TTC_ADC47
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC47
        #define TTC_ADC_AMOUNT_STATIC 47
    #endif
#endif
//}
//{ TTC_ADC46
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC46
        #define TTC_ADC_AMOUNT_STATIC 46
    #endif
#endif
//}
//{ TTC_ADC45
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC45
        #define TTC_ADC_AMOUNT_STATIC 45
    #endif
#endif
//}
//{ TTC_ADC44
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC44
        #define TTC_ADC_AMOUNT_STATIC 44
    #endif
#endif
//}
//{ TTC_ADC43
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC43
        #define TTC_ADC_AMOUNT_STATIC 43
    #endif
#endif
//}
//{ TTC_ADC42
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC42
        #define TTC_ADC_AMOUNT_STATIC 42
    #endif
#endif
//}
//{ TTC_ADC41
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC41
        #define TTC_ADC_AMOUNT_STATIC 41
    #endif
#endif
//}
//{ TTC_ADC40
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC40
        #define TTC_ADC_AMOUNT_STATIC 40
    #endif
#endif
//}
//{ TTC_ADC39
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC39
        #define TTC_ADC_AMOUNT_STATIC 39
    #endif
#endif
//}
//{ TTC_ADC38
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC38
        #define TTC_ADC_AMOUNT_STATIC 38
    #endif
#endif
//}
//{ TTC_ADC37
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC37
        #define TTC_ADC_AMOUNT_STATIC 37
    #endif
#endif
//}
//{ TTC_ADC36
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC36
        #define TTC_ADC_AMOUNT_STATIC 36
    #endif
#endif
//}
//{ TTC_ADC35
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC35
        #define TTC_ADC_AMOUNT_STATIC 35
    #endif
#endif
//}
//{ TTC_ADC34
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC34
        #define TTC_ADC_AMOUNT_STATIC 34
    #endif
#endif
//}
//{ TTC_ADC33
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC33
        #define TTC_ADC_AMOUNT_STATIC 33
    #endif
#endif
//}
//{ TTC_ADC32
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC32
        #define TTC_ADC_AMOUNT_STATIC 32
    #endif
#endif
//}
//{ TTC_ADC31
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC31
        #define TTC_ADC_AMOUNT_STATIC 31
    #endif
#endif
//}
//{ TTC_ADC30
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC30
        #define TTC_ADC_AMOUNT_STATIC 30
    #endif
#endif
//}
//{ TTC_ADC29
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC29
        #define TTC_ADC_AMOUNT_STATIC 29
    #endif
#endif
//}
//{ TTC_ADC28
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC28
        #define TTC_ADC_AMOUNT_STATIC 28
    #endif
#endif
//}
//{ TTC_ADC27
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC27
        #define TTC_ADC_AMOUNT_STATIC 27
    #endif
#endif
//}
//{ TTC_ADC26
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC26
        #define TTC_ADC_AMOUNT_STATIC 26
    #endif
#endif
//}
//{ TTC_ADC25
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC25
        #define TTC_ADC_AMOUNT_STATIC 25
    #endif
#endif
//}
//{ TTC_ADC24
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC24
        #define TTC_ADC_AMOUNT_STATIC 24
    #endif
#endif
//}
//{ TTC_ADC23
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC23
        #define TTC_ADC_AMOUNT_STATIC 23
    #endif
#endif
//}
//{ TTC_ADC22
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC22
        #define TTC_ADC_AMOUNT_STATIC 22
    #endif
#endif
//}
//{ TTC_ADC21
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC21
        #define TTC_ADC_AMOUNT_STATIC 21
    #endif
#endif
//}
//{ TTC_ADC20
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC20
        #define TTC_ADC_AMOUNT_STATIC 20
    #endif
#endif
//}
//{ TTC_ADC19
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC19
        #define TTC_ADC_AMOUNT_STATIC 19
    #endif
#endif
//}
//{ TTC_ADC18
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC18
        #define TTC_ADC_AMOUNT_STATIC 18
    #endif
#endif
//}
//{ TTC_ADC17
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC17
        #define TTC_ADC_AMOUNT_STATIC 17
    #endif
#endif
//}
//{ TTC_ADC16
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC16
        #define TTC_ADC_AMOUNT_STATIC 16
    #endif
#endif
//}
//{ TTC_ADC15
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC15
        #define TTC_ADC_AMOUNT_STATIC 15
    #endif
#endif
//}
//{ TTC_ADC14
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC14
        #define TTC_ADC_AMOUNT_STATIC 14
    #endif
#endif
//}
//{ TTC_ADC13
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC13
        #define TTC_ADC_AMOUNT_STATIC 13
    #endif
#endif
//}
//{ TTC_ADC12
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC12
        #define TTC_ADC_AMOUNT_STATIC 12
    #endif
#endif
//}
//{ TTC_ADC11
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC11
        #define TTC_ADC_AMOUNT_STATIC 11
    #endif
#endif
//}
//{ TTC_ADC10
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC10
        #define TTC_ADC_AMOUNT_STATIC 10
    #endif
#endif
//}
//{ TTC_ADC9
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC9
        #define TTC_ADC_AMOUNT_STATIC 9
    #endif
#endif
//}
//{ TTC_ADC8
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC8
        #define TTC_ADC_AMOUNT_STATIC 8
    #endif
#endif
//}
//{ TTC_ADC7
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC7
        #define TTC_ADC_AMOUNT_STATIC 7
    #endif
#endif
//}
//{ TTC_ADC6
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC6
        #define TTC_ADC_AMOUNT_STATIC 6
    #endif
#endif
//}
//{ TTC_ADC5
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC5
        #define TTC_ADC_AMOUNT_STATIC 5
    #endif
#endif
//}
//{ TTC_ADC4
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC4
        #define TTC_ADC_AMOUNT_STATIC 4
    #endif
#endif
//}
//{ TTC_ADC3
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC3
        #define TTC_ADC_AMOUNT_STATIC 3
    #endif
#endif
//}
//{ TTC_ADC2
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC2
        #define TTC_ADC_AMOUNT_STATIC 2
    #endif
#endif
//}
//{ TTC_ADC1
#ifndef TTC_ADC_AMOUNT_STATIC
    #ifdef TTC_ADC1
        #define TTC_ADC_AMOUNT_STATIC 1
    #endif
#endif
//}
#ifndef TTC_ADC_AMOUNT_STATIC
    #define TTC_ADC_AMOUNT_STATIC 0
#endif



#endif //TTC_ADC_CONFIGURATION_H
