/** { ttc_dac_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level datatype definitions for DAC device.
 *  Structures, Enums and Defines being required by both, high- and low-level dac.
 *  This file does not provide function declarations!
 *  
 *  Created from template ttc_device_types.h revision 27 at 20141215 11:13:24 UTC
 *
 *  Authors: Gregor Rebel
 * 
}*/

#ifndef TTC_DAC_TYPES_H
#define TTC_DAC_TYPES_H

//{ Includes *************************************************************
// 
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_dac.h" or "ttc_dac.c"
//
#include "ttc_basic_types.h"
#include "compile_options.h" // only required to help IDE to know constant definitions
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
#ifdef EXTENSION_dac_stm32l1xx
#  include "dac/dac_stm32l1xx_types.h" // automatically added by ./create_DeviceDriver.pl
#endif
//InsertTypesIncludes above (DO NOT REMOVE THIS LINE!)
 
//} Includes
//{ Defines **************************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Static Configuration *************************************************

//{  TTC_DACn has to be defined as constant by makefile.100_board_*
#ifdef TTC_DAC5
  #ifndef TTC_DAC4
    #error TTC_DAC5 is defined, but not TTC_DAC4 - all lower TTC_DACn must be defined!
  #endif
  #ifndef TTC_DAC3
    #error TTC_DAC5 is defined, but not TTC_DAC3 - all lower TTC_DACn must be defined!
  #endif
  #ifndef TTC_DAC2
    #error TTC_DAC5 is defined, but not TTC_DAC2 - all lower TTC_DACn must be defined!
  #endif
  #ifndef TTC_DAC1
    #error TTC_DAC5 is defined, but not TTC_DAC1 - all lower TTC_DACn must be defined!
  #endif

  #define TTC_DAC_AMOUNT 5
#else
  #ifdef TTC_DAC4
    #define TTC_DAC_AMOUNT 4

    #ifndef TTC_DAC3
      #error TTC_DAC5 is defined, but not TTC_DAC3 - all lower TTC_DACn must be defined!
    #endif
    #ifndef TTC_DAC2
      #error TTC_DAC5 is defined, but not TTC_DAC2 - all lower TTC_DACn must be defined!
    #endif
    #ifndef TTC_DAC1
      #error TTC_DAC5 is defined, but not TTC_DAC1 - all lower TTC_DACn must be defined!
    #endif
  #else
    #ifdef TTC_DAC3

      #ifndef TTC_DAC2
        #error TTC_DAC5 is defined, but not TTC_DAC2 - all lower TTC_DACn must be defined!
      #endif
      #ifndef TTC_DAC1
        #error TTC_DAC5 is defined, but not TTC_DAC1 - all lower TTC_DACn must be defined!
      #endif

      #define TTC_DAC_AMOUNT 3
    #else
      #ifdef TTC_DAC2

        #ifndef TTC_DAC1
          #error TTC_DAC5 is defined, but not TTC_DAC1 - all lower TTC_DACn must be defined!
        #endif

        #define TTC_DAC_AMOUNT 2
      #else
        #ifdef TTC_DAC1
          #define TTC_DAC_AMOUNT 1
        #else
          #define TTC_DAC_AMOUNT 0
        #endif
      #endif
    #endif
  #endif
#endif
//}
/**{ Device specific Assert function
 *
 * The use of Assert() is the basic key for stable code in embedded software.
 * Every function should check all given arguments for plausibility.
 * Calling Assert() with a zero (false) argument will hold program execution in an endless loop. 
 * This prevents any possibly dangerous operations.
 *
 * Every device driver provides its own Assert_{DEVICE}() function.
 * This allows to switch off the use of assert code for each device driver from your makefile.
 * Simply add the following line to your makefile.700_extra_settings to see a slight performance improvement:
 *   COMPILE_OPTS += -DTTC_ASSERT_DAC 0  # disable assert handling for dac devices
 *
 */
#ifndef TTC_ASSERT_DAC    // any previous definition set (Makefile)?
#define TTC_ASSERT_DAC 1  // string asserts are enabled by default
#endif
#if (TTC_ASSERT_DAC == 1)  // use Assert()s in DAC code (somewhat slower but alot easier to debug)
  #define Assert_DAC(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in DAC code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_DAC(Condition, ErrorCode)
#endif
//}
/**{ Architecture dependent device configuration
 *
 * Each low-level driver may define this structure to increase visibility of t_ttc_dac_config during debugging.
 * Simply copy the define line below into your _types.h and change void* to any structure you like.
 */
#ifndef t_ttc_dac_architecture
#warning Missing low-level definition for t_ttc_dac_architecture (using default)
#define t_ttc_dac_architecture void
#endif
//}


//}

//}Static Configuration
//{ Enums/ Structures ****************************************************

typedef e_ttc_physical_index e_ttc_dac_physical_index;
typedef enum {   // e_ttc_dac_errorcode      return codes of DAC devices
  ec_dac_OK = 0, 
  
  // other warnings go here..

  ec_dac_ERROR,                  // general failure
  ec_dac_NULL,                   // NULL pointer not accepted
  ec_dac_DeviceNotFound,         // corresponding device could not be found
  ec_dac_InvalidConfiguration,   // sanity check of device configuration failed
  ec_dac_InvalidImplementation,  // your code does not behave as expected

  // other failures go here..
  
  ec_dac_unknown                // no valid errorcodes past this entry 
} e_ttc_dac_errorcode;
typedef enum {   // e_ttc_dac_architecture  types of architectures supported by DAC driver
  ta_dac_None,           // no architecture selected
  

  ta_dac_stm32l1xx, // automatically added by ./create_DeviceDriver.pl
//InsertArchitectureEnum above (DO NOT REMOVE THIS LINE!)

  ta_dac_unknown        // architecture not supported
} e_ttc_dac_architecture;





typedef struct s_ttc_dac_config { //         architecture independent configuration data

    // Note: Write-access to this structure is only allowed before ttc_dac_init()
    //       and after ttc_dac_deinit() call!
    //       if Flags.Bits.Initialized == 1 then only read accesses are allowed for high-level code.

    e_ttc_dac_architecture Architecture; // type of architecture used for current dac device
    t_u8  LogicalIndex;        // automatically set: logical index of device to use (1 = TTC_DAC1, ...)
    t_u8  PhysicalIndex;       // automatically set: physical index of device to use (0 = first hardware device, ...)

    t_u32 Channel; // We have to initialize this in some place
    // low-level configuration (structure defined by low-level driver)
    t_ttc_dac_architecture* LowLevelConfig;      
    
    union  { // generic configuration bits common for all low-level Drivers
        t_u16 All;
        struct {
          unsigned Initialized        : 1;  // ==1: device has been initialized successfully
          // ToDo: additional high-level flags go here..
            
            unsigned Reserved1        : 15; // pad to 16 bits
        } Bits;
    } Flags;

    // Additional high-level attributes go here..
    
} __attribute__((__packed__)) t_ttc_dac_config;
//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//}Structures

#endif // TTC_DAC_TYPES_H
