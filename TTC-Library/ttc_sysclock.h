/** { ttc_sysclock.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for SYSCLOCK devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_SYSCLOCK(tc_sysclock_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_sysclock_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_sysclock_init(LogicalIndex);
 *  4) use:         ttc_sysclock_XXX(LogicalIndex);
 *
 *  Device Index
 *  Every device has a physical and a logical index.
 *  The physical index ist simply its hardware address.
 *  Not all physical devices can be used in real hardware.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level sysclock and application.
 *
 *  Created from template ttc_device.h revision 23 at 20140218 17:50:25 UTC
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of ttc_sysclock (Do not delete this line!)
 *
 *  The main function of ttc_sysclock is ttc_sysclock_profile_switch(). It implements a concept of
 *  system clock profiles. Different profiles can be implemented like maximum speed,
 *  highest energy efficiency and others.
 *  Application software and other high- and low-level drivers may register themself to get
 *  informed when the system clock frequency is changed (-> ttc_sysclock_register_for_update() ).
 *
 *  See declaration of e_ttc_sysclock_profile and implementation of current architecture
 *  driver for further details.
 *
}*/

#ifndef TTC_SYSCLOCK_H
#define TTC_SYSCLOCK_H

/** Prepares sysclock Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_sysclock_prepare();


//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "interfaces/ttc_sysclock_interface.h"
#include "ttc_memory.h"
#include "ttc_task.h"
#include "ttc_list_types.h"
#include "interfaces/ttc_sysclock_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************
#ifndef EXTENSION_ttc_sysclock
    #warning Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_sysclock.sh
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level sysclock only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums

/** Converts a generic list item */
t_sysclock_update_item* sysclock_list_item_2_item( t_ttc_list_item* ListItem );
#define sysclock_list_item_2_item(ListItem) ( (t_sysclock_update_item*) ListItem )


/** Function prototypes *************************************************{
 *
 * The functions declared below provide the main interface for
 * sysclock devices on all supported architectures.
 * Check sysclock/sysclock_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

void ttc_sysclock_prepare();

/** returns current configuration of all clock domains
 *
 * The returned structure stores two types of configuration:
 *
 * + High Level (architecture independent)
 *   These fields can be accessed by an application on all microcontroller architectures.
 *   See structure declaration of t_ttc_sysclock_config for details!
 *
 *   Examples:
 *   t_base SystemClockFrequency    = ttc_sysclock_get_configuration()->SystemClockFrequency;
 *   t_base SystemClockFrequencyMax = ttc_sysclock_get_configuration()->SystemClockFrequencyMax;
 *
 * + Low Level  (architecture dependent)
 *   The returned configuration stores a sub struct LowLevelConfig.
 *   LowLevelConfig is defined different by every low-level sysclock_* driver.
 *   Access will only work if you are certain that this driver is currently activated.
 *   See structure declaration of t_ttc_sysclock_architecture for details!
 *
 *   Examples:
 *   #ifdef EXTENSION_sysclock_stm32f1xx
 *     t_base Frequency_APB1 = ttc_sysclock_get_configuration()->LowLevelConfig.frequency_apb1
 *   #endif
 *
 * @return  Config = pointer to configuration of all clock domains (must not be written to by any foreign function!)
 */
t_ttc_sysclock_config* ttc_sysclock_get_configuration();

/** returns scale factor for delays according to current system clock frequency
 *
 * If uC runs at lower frequency than its maximum frequency, manual delays can be adjusted
 * by this formula:
 * AdjustedDelay = OriginalDelay * ttc_sysclock_get_delay_scale() / TTC_SYSCLOCK_SCALE_UNIT;
 *
 * @return                DelayScale
 */
t_base ttc_sysclock_get_delay_scale();

/** returns current system clock frequency
 *
 * @return                current system clock frequency (Hz)
 */
t_u32 ttc_sysclock_frequency_get();

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    device index of device to init (1..ttc_SYSCLOCK_get_max_LogicalIndex())
 */
void ttc_sysclock_reset();

/** Switches current system clock profile.
 *
 * Each architecture and board may provide its own clocking profile implementation.
 * The concept of clocking profiles allows to write applications that can control the
 * system clock frequency on any microcontroller architecture in a uniform approach.
 *
 * These three main profiles should be implemented for all architectures:
 *   E_ttc_sysclock_profile_MaxSpeed   = maximum stable clock frequency
 *   E_ttc_sysclock_profile_MinSpeed   = minimum stable clock frequency with lowest possible energy consumption
 *   E_ttc_sysclock_profile_Efficient  = clock frequency providing highest computing performance per joule
 *
 * @param NewProfile  new clocking profile to switch to
 */
void ttc_sysclock_profile_switch( e_ttc_sysclock_profile NewProfile );

/** Busy waits for given amount of microseconds.
 *
 * Provides a certain time delay even in single tasking situations like during system initialization.
 * Note: This wait function has to be calibrated for each target platform.
 *
 * @param Microseconds  time to wait (for-loop)
 */
void ttc_sysclock_udelay( t_base Microseconds );

/** Update configuration from current register settings.
 *
 * Call this function only if you expect the clock registers being changed from outside of ttc_sysclock.
 * Normally it should not be necessary to call this function.
 *
 */
void ttc_sysclock_update();


/** Registers an update function to be called whenever the system clock changes
 *
 * @param functionUpdateToSysclock  pointer to function to be callede whenever the system clock has changed
 */
void ttc_sysclock_register_for_update( void ( *functionUpdateToSysclock )() );


/** Switches single oscillator or clock input on or off
 *
 * @param Oscillator (e_ttc_sysclock_oscillator)  Not all oscillators may be available on current architecture
 * @param On         (BOOL)                       ==0: switch off oscillator, !=0: switch on oscillator
 * @return           (e_ttc_sysclock_errorcode)   ==0: switch was successfull, error code otherwise
 */
e_ttc_sysclock_errorcode ttc_sysclock_enable_oscillator( e_ttc_sysclock_oscillator Oscillator, BOOL On );
e_ttc_sysclock_errorcode _driver_sysclock_enable_oscillator( e_ttc_sysclock_oscillator Oscillator, BOOL On );
#define ttc_sysclock_enable_oscillator(Oscillator, On)  _driver_sysclock_enable_oscillator(Oscillator, On)

/** Returns a list of currently available system clock frequencies
 *
 * The current low-level driver knows which frequencies are available on current hardware.
 * Format of return array is check at system boot by ttc_sysclock_prepare():
 * - Array contains at least one non-zero entry.
 * - No double entries are allowed.
 * - Values are increasing with increasing array index.
 * - Last array entry is zero (0) to terminate list.
 *
 * @return (base_t*)  zero terminated array of valid, increasing clock frequencies for ttc_sysclock_set_frequency()
 */
const t_base* ttc_sysclock_frequency_get_all();
const t_base* _driver_sysclock_frequency_get_all( t_ttc_sysclock_config* Config );
// #define ttc_sysclock_frequency_get_all  _driver_sysclock_frequency_get_all  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Set new system clock frequency.
 *
 * Note: Depending on current hardware configuration and ttc_sysclock configuration, different clock frequencies may be available.
 *       The low-level driver will choose the best matching frequency.
 *
 * @param NewFrequency (t_base)  new frequency (Hz) should be taken from list of available frequencies as returned by ttc_sysclock_frequency_get_all()
 */
void ttc_sysclock_frequency_set( t_base NewFrequency );
void _driver_sysclock_frequency_set( t_ttc_sysclock_config* Config, t_base NewFrequency );
//#define ttc_sysclock_frequency_set  _driver_sysclock_frequency_set  // enable to directly forward function call to low-level driver (remove function definition before!)

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_heap_ are passed to interfaces/ttc_heap_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl heap UPDATE
 */

/** Prepares sysclock driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void _driver_sysclock_prepare( t_ttc_sysclock_config* Config );

/** Reset given configuration according to current hardware platform
 *
 * @param Config        pointer to struct t_ttc_sysclock_config (must have valid value for LogicalIndex)
 */
void _driver_sysclock_reset( t_ttc_sysclock_config* Config );
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

/** Switches current system clock profile.
 *
 * Each architecture and board may provide its own clocking profile implementation.
 *
 * @param Config      pointer to struct t_ttc_sysclock_config (field Profile stores the current profile)
 * @param NewProfile  new clocking profile to switch to
 * @return Config->Profile is loaded with updated clocking profile (unchanged if NewProfile is not available on current architecture)
 */
void _driver_sysclock_profile_switch( t_ttc_sysclock_config* Config, e_ttc_sysclock_profile NewProfile );

/** Busy waits for given amount of microseconds.
 *
 * Note: This wait function has to be calibrated for each target platform and clocking profile.
 *
 * @param Microseconds  time to wait (for-loop)
 */
void _driver_sysclock_udelay( t_ttc_sysclock_config* Config, t_base Microseconds );

/** Updates given Config with current hardware settings.
 *
 * Hardware configuration may have changed by third party libraries.
 * Low-Level driver will read current register settings and update Config accordingly.
 *
 * @param Config  Referenced configuration will be changed.
 */
void _driver_sysclock_update( t_ttc_sysclock_config* Config );

//}

#endif //TTC_SYSCLOCK_H
