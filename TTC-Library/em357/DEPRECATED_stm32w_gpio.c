/*{ DEPRECATED_stm32w_gpio.c ***********************************************
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Low Level Driver for General Purpose Input Output (GPIO) on STM32 architecture.
 *
}*/

#include "DEPRECATED_stm32w_gpio.h"

GPIOMode_TypeDef stm32w_map_Type(ttc_gpio_mode_e Mode) {
    switch (Type) { // find best matching type for stm32-architecture
    case tgm_analog_in:                     return GPIOCFG_ANALOG;
    case tgm_input_floating:                return GPIOCFG_IN;
    case tgm_input_pull_down:               return GPIOCFG_IN_PUD;
    case tgm_input_pull_up:                 return GPIOCFG_IN_PUD;
    case tgm_output_open_drain:             return GPIOCFG_OUT_OD;
    case tgm_output_push_pull:              return GPIOCFG_OUT;
    case tgm_alternate_function_push_pull:  return GPIOCFG_OUT_ALT;
    case tgm_alternate_function_open_drain: return GPIOCFG_OUT_ALT_OD;
    case tgm_alternate_function_SPECIAL_SCLK: return GPIOCFG_OUT_ALT_SPECIAL_SCLK ;
        
    default: Assert(0, ec_InvalidArgument);
    }

    return 0;
}
GPIOSpeed_TypeDef stm32w_map_Speed(ttc_gpio_speed_e Speed) {
    switch (Speed) { // find best matching speed for stm32-architecture
    case tgs_Min:
    case tgs_2MHz:
    case tgs_10MHz:
    case tgs_50MHz:
    case tgs_Max:   return 0;
    default: Assert(0, ec_InvalidArgument);
    }

    return 0;
}

inline void stm32w_loadPort(tgp_e* Port, GPIO_TypeDef* GPIOx, u32_t Pin) {
        Port->GPIOx=GPIOx;
        Port->Pin  =Pin;
}

void stm32w_gpio_init2_variable(tgp_e* Port, GPIO_TypeDef GPIOx, u8_t Pin, ttc_gpio_mode_e Mode, ttc_gpio_speed_e Speed) {

    stm32w_loadPort_variable(Port, GPIOx, Pin);
    stm32w_gpio_init_variable( Port, stm32w_map_Type(Type), stm32w_map_Speed(Speed) );
}
void stm32w_initPort2(tgp_e* Port, GPIOMode_TypeDef Type, GPIOSpeed_TypeDef Speed) {

       switch (Type) {

       case GPIOCFG_OUT_mode: {
           switch ( (u32_t) Port->GPIOx) { // enable peripheral clock to this port
             case (u32_t) GPIOA:  halGpioConfig(PORTA_PIN(Port->Pin), GPIOCFG_OUT); break;
             case (u32_t) GPIOB:  halGpioConfig(PORTB_PIN(Port->Pin), GPIOCFG_OUT); break;
             case (u32_t) GPIOC:  halGpioConfig(PORTC_PIN(Port->Pin), GPIOCFG_OUT); break;

             default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!
           }
              break;
           }
       case GPIOCFG_OUT_OD_mode: {
       break;
    }
       case GPIOCFG_OUT_ALT_mode: {
       break;
    }
       case GPIOCFG_OUT_ALT_OD_mode: {
       break;
    }
       case GPIOCFG_ANALOG_mode: {
       break;
    }
       case GPIOCFG_IN_mode: {
       break;
    }
       case GPIOCFG_IN_PUD_mode: {
       break;
    }
       case GPIOCFG_OUT_ALT_SPECIAL_SCLK_mode: {

           switch ( (u32_t) Port->GPIOx) { // enable peripheral clock to this port
             case (u32_t) GPIOA:  halGpioConfig(PORTA_PIN(Port->Pin), GPIOCFG_OUT_ALT_SPECIAL_SCLK); break;
             case (u32_t) GPIOB:  halGpioConfig(PORTB_PIN(Port->Pin), GPIOCFG_OUT_ALT_SPECIAL_SCLK); break;
             case (u32_t) GPIOC:  halGpioConfig(PORTC_PIN(Port->Pin), GPIOCFG_OUT_ALT_SPECIAL_SCLK); break;

             default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!

           }

       break;
    }
       default: Assert(0, ec_InvalidArgument);

      }

}

inline bool stm32w_portGet(tgp_e* Port) {

    //must use Registers, because hal Library does not support reading GPIOs
    switch ( (u32_t) Port->GPIOx) { // enable peripheral clock to this port
    case (u32_t) GPIOA:  {
        return (bool)(GPIO_PAIN_REG&(1<<Port->Pin))>>(Port->Pin);
        break;
    }
      case (u32_t) GPIOB:  {
        return (bool)(GPIO_PBIN_REG&(1<<Port->Pin))>>(Port->Pin);
        break;
    }
      case (u32_t) GPIOC:  {
        return (bool)(GPIO_PCIN_REG&(1<<Port->Pin))>>(Port->Pin);
        break;
    }

      default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!
    }
}
inline void stm32w_portSet(tgp_e* Port) {

     switch ( (u32_t) Port->GPIOx) { // enable peripheral clock to this port
       case (u32_t) GPIOA:  halGpioSet(PORTA_PIN(Port->Pin), 1); break;
       case (u32_t) GPIOB:  halGpioSet(PORTB_PIN(Port->Pin), 1); break;
       case (u32_t) GPIOC:  halGpioSet(PORTC_PIN(Port->Pin), 1); break;

       default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!
     }
}
inline void stm32w_portClr(tgp_e* Port) {

    switch ( (u32_t) Port->GPIOx) { // enable peripheral clock to this port
      case (u32_t) GPIOA:  halGpioSet(PORTA_PIN(Port->Pin), 0); break;
      case (u32_t) GPIOB:  halGpioSet(PORTB_PIN(Port->Pin), 0); break;
      case (u32_t) GPIOC:  halGpioSet(PORTC_PIN(Port->Pin), 0); break;

      default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!
    }
}
