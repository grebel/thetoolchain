/*{ GPIO.c ***********************************************
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Low Level Driver for General Purpose Input Output (GPIO) on EM357/EM357 architecture.
 * 
}*/

#include "DEPRECATED_stm32w_gpio.h"

#ifdef EXTENSION_200_cpu_stm32w1xx

GPIOMode_TypeDef stm32w_map_Type(ttc_gpio_mode_e Mode) {
    switch (Type) { // find best matching type for stm32-architecture

    case tgm_analog_in:
      return GPIO_Mode_AN | GPIO_Mode_IN;
    case tgm_input_floating:
      return GPIO_Mode_IN;
    case tgm_input_pull_down:
      return GPIO_Mode_IN_PUD;
    case tgm_input_pull_up:
      return GPIO_Mode_IN_PUD;
    case tgm_output_open_drain:
      return GPIO_Mode_OUT_OD;
    case tgm_output_push_pull:
      return GPIO_Mode_OUT_PP;
    case tgm_alternate_function_push_pull:
      return GPIO_Mode_AF_PP;
    case tgm_alternate_function_open_drain:
      return GPIO_Mode_AF_OD;
    case tgm_alternate_function_SPECIAL_SCLK:
      return GPIO_Mode_AF_PP_SPI ;

    default: Assert(0, ec_InvalidArgument);
    }

    return 0;
}
#else // using defines provided by SimpleMAC-Library <= v2.0.0
GPIOMode_TypeDef stm32w_map_Type(ttc_gpio_mode_e Mode) {
    switch (Type) { // find best matching type for stm32-architecture

    case tgm_analog_in:
      return GPIOCFG_ANALOG;
    case tgm_input_floating:
      return GPIOCFG_IN;
    case tgm_input_pull_down:
      return GPIOCFG_IN_PUD;
    case tgm_input_pull_up:
      return GPIOCFG_IN_PUD;
    case tgm_output_open_drain:
      return GPIOCFG_OUT_OD;
    case tgm_output_push_pull:
      return GPIOCFG_OUT_mode;
    case tgm_alternate_function_push_pull:
      return GPIOCFG_OUT_ALT;
    case tgm_alternate_function_open_drain:
      return GPIOCFG_OUT_ALT_OD;
    case tgm_alternate_function_SPECIAL_SCLK:
      return GPIOCFG_OUT_ALT_SPECIAL_SCLK ;

    default: Assert(0, ec_InvalidArgument);
    }

    return 0;
}
#endif

stm32w_gpio_speed_t stm32w_map_Speed(ttc_gpio_speed_e Speed) {
    switch (Speed) { // find best matching speed for stm32-architecture
    case tgs_Min:
    case tgs_2MHz:
    case tgs_10MHz:
    case tgs_50MHz:
    case tgs_Max:   return 0;
    default: Assert(0, ec_InvalidArgument);
    }

    return 0;
}
inline void stm32w_loadPort_variable(stm32w_Port_t* Port, GPIO_TypeDef* GPIOx, u8_t Pin) {
        Port->GPIOx=GPIOx;
        Port->Pin  =Pin;
}
void stm32w_gpio_init_variable(stm32w_Port_t* Port, GPIO_TypeDef* GPIOx, u8_t Pin, GPIOMode_TypeDef Type, stm32w_gpio_speed_t Speed) {
    stm32w_loadPort_variable(Port, GPIOx, Pin);
    stm32w_gpio_init(Port->GPIOx, Port->Pin, Type, Speed);
}
void stm32w_initPort(GPIO_TypeDef* GPIOx, u8_t Pin, GPIOMode_TypeDef Type) {
    stm32w_gpio_init(GPIOx, Pin, Type, tgs_Max);
}
void stm32w_gpio_init(GPIO_TypeDef* GPIOx, u8_t Pin, ttc_gpio_mode_e Mode, stm32w_gpio_speed_t Speed) {
    (void) Speed;

    switch (Type) {
    case tgm_analog_in:
        Assert_Halt_EC(ec_NotImplemented); // ToDo: implement
        break;
    case tgm_input_floating:
        Assert_Halt_EC(ec_NotImplemented); // ToDo: implement
        break;
    case tgm_input_pull_down:
        Assert_Halt_EC(ec_NotImplemented); // ToDo: implement
        break;
    case tgm_input_pull_up:
        Assert_Halt_EC(ec_NotImplemented); // ToDo: implement

        break;
    case tgm_output_open_drain:
        switch ( (u32_t) GPIOx) { // enable peripheral clock to this port
        case (u32_t) GPIOA:  halGpioConfig(PORTA_PIN(Pin), GPIO_Mode_OUT_OD); break;
        case (u32_t) GPIOB:  halGpioConfig(PORTB_PIN(Pin), GPIO_Mode_OUT_OD); break;
        case (u32_t) GPIOC:  halGpioConfig(PORTC_PIN(Pin), GPIO_Mode_OUT_OD); break;

        default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!
        }
        break;
    case tgm_output_push_pull:
        switch ( (u32_t) GPIOx) { // enable peripheral clock to this port
        case (u32_t) GPIOA:  halGpioConfig(PORTA_PIN(Pin), GPIO_Mode_OUT_PP); break;
        case (u32_t) GPIOB:  halGpioConfig(PORTB_PIN(Pin), GPIO_Mode_OUT_PP); break;
        case (u32_t) GPIOC:  halGpioConfig(PORTC_PIN(Pin), GPIO_Mode_OUT_PP); break;

        default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!
        }
        break;
    case tgm_alternate_function_push_pull:
        switch ( (u32_t) GPIOx) { // enable peripheral clock to this port
        case (u32_t) GPIOA:  halGpioConfig(PORTA_PIN(Pin), GPIO_Mode_AF_PP); break;
        case (u32_t) GPIOB:  halGpioConfig(PORTB_PIN(Pin), GPIO_Mode_AF_PP); break;
        case (u32_t) GPIOC:  halGpioConfig(PORTC_PIN(Pin), GPIO_Mode_AF_PP); break;

        default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!
        }
        break;
    case tgm_alternate_function_open_drain:
        Assert_Halt_EC(ec_NotImplemented); // ToDo: implement

        break;
    default:
        Assert_Halt_EC(ec_InvalidArgument);
        break;
    }

}
inline BOOL stm32w_gpio_get_variable(stm32w_Port_t* Port) {
    return stm32w_gpio_get(Port->GPIOx, Port->Pin);
}
BOOL stm32w_gpio_get(GPIO_TypeDef* GPIOx, u8_t Pin) {

#ifdef EXTENSION_255_cpu_stm32w1xx_std_peripherals_stm32w108xx_gpio
    return GPIO_ReadInputDataBit(GPIOx, Pin);
#else
    u32_t Mask = (u32_t) 1 << Pin;

    //must use Registers, because hal Library does not support reading GPIOs
    switch ( (u32_t) GPIOx) {
    case (u32_t) GPIOA:  {
        if ( ( (u32_t) GPIO_PAIN_REG) &  Mask)
            return TRUE;
        else
            return FALSE;
        break;
    }
    case (u32_t) GPIOB:  {
        if ( ( (u32_t) GPIO_PBIN_REG) &  Mask)
            return TRUE;
        else
            return FALSE;
        break;
    }
    case (u32_t) GPIOC:  {
        if ( ( (u32_t) GPIO_PCIN_REG) &  Mask)
            return TRUE;
        else
            return FALSE;
        break;
    }

    default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!
    }
    return 0;
#endif
}
inline void stm32w_gpio_set_variable(stm32w_Port_t* Port) {
    stm32w_gpio_set(Port->GPIOx, Port->Pin);
}
void stm32w_gpio_set(GPIO_TypeDef* GPIOx, u8_t Pin) {

#ifdef EXTENSION_255_cpu_stm32w1xx_std_peripherals_stm32w108xx_gpio
    if (0) {
       GPIO_WriteBit(GPIOx, Pin, 1);
    }
    else { // direct register access
        GPIOx->BSR = Pin;
    }
#else
     switch ( (u32_t) GPIOx) { // enable peripheral clock to this port
     case (u32_t) GPIOA:  halGpioSet(PORTA_PIN(Pin), 1); break;
     case (u32_t) GPIOB:  halGpioSet(PORTB_PIN(Pin), 1); break;
     case (u32_t) GPIOC:  halGpioSet(PORTC_PIN(Pin), 1); break;

     default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!
     }
#endif
}
inline void DEPRECATED_stm32w_gpio.clr_variable(stm32w_Port_t* Port) {
    DEPRECATED_stm32w_gpio.clr(Port->GPIOx, Port->Pin);
}
void DEPRECATED_stm32w_gpio.clr(GPIO_TypeDef* GPIOx, u8_t Pin) {

#ifdef EXTENSION_255_cpu_stm32w1xx_std_peripherals_stm32w108xx_gpio
    if (0) {
       GPIO_WriteBit(GPIOx, Pin, 0);
    }
    else { // direct register access
        GPIOx->BRR = Pin;
    }
#else // using defines provided by SimpleMAC-Library <= v2.0.0
    switch ( (u32_t) GPIOx) { // enable peripheral clock to this port
    case (u32_t) GPIOA:  halGpioSet(PORTA_PIN(Pin), 0); break;
    case (u32_t) GPIOB:  halGpioSet(PORTB_PIN(Pin), 0); break;
    case (u32_t)   GPIOC:  halGpioSet(PORTC_PIN(Pin), 0); break;

    default: Assert(0, ec_InvalidArgument); break; // ERROR: unknown GPIO given!
  }
#endif
}
u8_t DEPRECATED_stm32w_gpio.create_index8(ttc_gpio_bank_t GPIOx, u8_t Pin) {

    switch ( (Base_t) GPIOx) {

#ifdef TTC_GPIO_BANK_A
    case (Base_t) TTC_GPIO_BANK_A: return 0x10 | (0x0f & Pin);
#endif
#ifdef TTC_GPIO_BANK_B
    case (Base_t) TTC_GPIO_BANK_B: return 0x20 | (0x0f & Pin);
#endif
#ifdef TTC_GPIO_BANK_C
    case (Base_t) TTC_GPIO_BANK_C: return 0x30 | (0x0f & Pin);
#endif

    default: break;
    }

    return 0;
}
void stm32w_gpio_from_index8(u8_t PhysicalIndex, ttc_gpio_bank_t* GPIOx, u8_t* Pin) {

    switch (PhysicalIndex & 0xf0) {

#ifdef TTC_GPIO_BANK_A
    case 0x10: *GPIOx = TTC_GPIO_BANK_A; break;
#endif
#ifdef TTC_GPIO_BANK_B
    case 0x20: *GPIOx = TTC_GPIO_BANK_B; break;
#endif
#ifdef TTC_GPIO_BANK_C
    case 0x30: *GPIOx = TTC_GPIO_BANK_C; break;
#endif

    default: *GPIOx = NULL;
    }

    *Pin = PhysicalIndex & 0x0f;
}
