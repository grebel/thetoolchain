#ifndef STM32_GPIO_H
#define STM32_GPIO_H

/*{ ../gpio/gpio_stm32f1xx.h ***********************************************

 * Written by Gregor Rebel 2010-2012
 *
 * Low Level Driver for General Purpose Input Output (GPIO) on STM32 architecture.
 *
}*/
//{ includes1

#include "ttc_basic.h"
//#include "stm32f10x_gpio.h"

#include "gnu.h"
#include "regs.h"
#include "STM32W_regs_patch.h"
#include "stm32w108_type.h"
#include "STM32W_types_patch.h"
#include "micro-common.h"

//}includes
//{ Structures/ Enums
typedef enum {
    GPIOCFG_OUT_mode = 0x1u,
    GPIOCFG_OUT_OD_mode = 0x5u,
    GPIOCFG_OUT_ALT_mode = 0x9u,
    GPIOCFG_OUT_ALT_OD_mode = 0xDu,
    GPIOCFG_ANALOG_mode = 0x0u,
    GPIOCFG_IN_mode = 0x4u,
    GPIOCFG_IN_PUD_mode = 0x8u,
    GPIOCFG_OUT_ALT_SPECIAL_SCLK_mode = 0xBu
}GPIOMode_TypeDef;

typedef enum {
    GPIOSpeed_not_implemented= 0
}GPIOSpeed_TypeDef;

typedef enum {
    GPIOA, GPIOB, GPIOC
}GPIO_TypeDef;

typedef struct { // tgp_e
    GPIO_TypeDef *GPIOx;
    u8_t Pin;
} __attribute__((__packed__)) tgp_e;

//}Structures/ Enums
//{ Architecture-Defines (required by ttc_gpio_types.h)

//#define ttc_gpio_bank_t  GPIO_TypeDef*
//#define ttc_Port_t     tgp_e

////{ Define shortcuts for all pins of GPIO Bank A
//#define PIN_PA0  GPIOA,0  //must be defined because another file puts an error if PIN_PA0 is not define


//}Defines
//{ includes2

#include "ttc_gpio_types.h"  // must be included AFTER defining some ttc_XXXX thingies
#include <stdlib.h>

// FreeRTOS
#include <string.h>
#include "stddef.h"




//}includes2

#define stm32w_gpio_init2_variable(Port, GPIOx, Pin, Type, Speed);
//{ Prototypes

/* converts from architecture independent pin type to stm32 pendant
 * @param    Type   one of tgm_analog_in, tgm_input_floating, tgm_input_pull_down, tgm_input_pull_up, tgm_output_open_drain, tgm_output_push_pull, tgm_alternate_function_push_pull, tgm_alternate_function_open_drain
 * @return   one of GPIO_Mode_AIN, GPIO_Mode_IN_FLOATING, GPIO_Mode_IPD, GPIO_Mode_IPU, GPIO_Mode_Out_OD, GPIO_Mode_Out_PP, GPIO_Mode_Out_OD, GPIO_Mode_AF_PP, GPIO_Mode_AF_OD
 */
GPIOMode_TypeDef stm32w_map_Type(ttc_gpio_mode_e Mode);

/* converts from architecture independent pin speed to stm32 pendant
 * @param    Type   one of tgs_Min, tgs_2MHz, tgs_10MHz, tgs_50MHz, tgs_Max
 * @return   one of GPIO_Speed_2MHz, GPIO_Speed_10MHz, tgs_Max
 */
GPIOSpeed_TypeDef stm32w_map_Speed(ttc_gpio_speed_e Speed);

/* initializes single port bit for use as input/ output
 * @param Port  filled out struct as being returned by stm32_loadPort()
 * @param Type   one of GPIO_Mode_AIN, GPIO_Mode_IN_FLOATING, GPIO_Mode_IPD, GPIO_Mode_IPU, GPIO_Mode_Out_OD, GPIO_Mode_Out_PP, GPIO_Mode_Out_OD, GPIO_Mode_AF_PP, GPIO_Mode_AF_OD
 * @param Speed  one of GPIO_Speed_2MHz, GPIO_Speed_10MHz, tgs_Max
 */
void stm32w_initPort2(tgp_e* Port, GPIOMode_TypeDef Type, GPIOSpeed_TypeDef Speed);

/* allows to load struct from list type definition
 *
 * @param Port   points to structure to be filled with given data
 * @param GPIOx  one of GPIOA, GPIOB, GPIOC, GPIOD, GPIOE, GPIOF
 * @param Pin    0..15
 *
 * Example:
 * #define PB_LED1=GPIOC,6
 * ttc_gpio_pin_e Port;
 * stm32_loadPort(&Port, PB_LED1);
 * stm32_initPort(&Port, GPIO_Mode_Out_PP);
 * stm32_portSet(&Port);
 */
void stm32w_loadPort(tgp_e* Port, GPIO_TypeDef* GPIOx, u32_t Pin);

/* read current value of input pin
 * @param Port  filled out struct as being returned by stm32_loadPort()
 */
bool stm32w_portGet(tgp_e* Port);

/* set output pin to logical one
 * @param Port  filled out struct as being returned by stm32_loadPort()
 */
void stm32w_portSet(tgp_e* Port);

/* set output pin to logical zero
 * @param Port  filled out struct as being returned by stm32_loadPort()
 */
void stm32w_portClr(tgp_e* Port);

//} Prototypes

#endif
