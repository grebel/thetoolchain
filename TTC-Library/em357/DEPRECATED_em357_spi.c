/*{ stm32w_spi::.c ************************************************

                      The ToolChain
                      
   Universal Synchronous Asynchronous Receiver and Transmitters (stm32w_SPIs)
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Gregor Rebel 2012
   
   Note: See ttc_spi.h for description of architecture independent SPI implementation.

}*/

#include "stm32w_spi.h"

register_stm32f1xx_spi_t* stm32w_SPIs[TTC_AMOUNT_SPIS];
u8_t ss_Initialized=0;

//{ Function definitions *************************************************

void stm32w_SPI_ResetAll() {
    memset(stm32w_SPIs, 0, sizeof(SPI_TypeDef*) * TTC_AMOUNT_SPIS);
    ss_Initialized=1;
}
ttc_spi_errorcode_e stm32w_getSPI_Defaults(u8_t SPI_Index, ttc_spi_generic_t* SPI_Generic) {
    if (!ss_Initialized) stm32w_SPI_ResetAll();
#ifndef TTC_NO_ARGUMENT_CHECKS //{
    // assumption: *SPI_Generic has been zeroed

    Assert(SPI_Index > 0,    ec_InvalidArgument);
    Assert(SPI_Generic != NULL, ec_NULL);
    if (SPI_Index > TTC_AMOUNT_SPIS) return tue_DeviceNotFound;
#endif //}

    SPI_Generic->Flags.All                        = 0;
    SPI_Generic->Flags.Bits.Master                = 1;
    SPI_Generic->Flags.Bits.ClockIdleHigh         = 1;
    SPI_Generic->Flags.Bits.ClockPhase2ndEdge     = 1;
    SPI_Generic->Flags.Bits.Simplex               = 0;
    SPI_Generic->Flags.Bits.Bidirectional         = 1;
    SPI_Generic->Flags.Bits.Receive               = 1;
    SPI_Generic->Flags.Bits.Transmit              = 1;
    SPI_Generic->Flags.Bits.WordSize16            = 0;
    SPI_Generic->Flags.Bits.SoftNSS               = 1;
    SPI_Generic->Flags.Bits.CRC8                  = 0;
    SPI_Generic->Flags.Bits.CRC16                 = 0;
    SPI_Generic->Flags.Bits.TxDMA                 = 0;
    SPI_Generic->Flags.Bits.RxDMA                 = 0;
    SPI_Generic->Flags.Bits.FirstBitMSB           = 1;
    SPI_Generic->Flags.Bits.Irq_Error             = 0;
    SPI_Generic->Flags.Bits.Irq_Received          = 0;
    SPI_Generic->Flags.Bits.Irq_Transmitted       = 0;

    SPI_Generic->CRC_Polynom = 0x7;
    SPI_Generic->Layout      = 0;
    
    //    RCC_ClocksTypeDef RCC_Clocks;
    //    RCC_GetClocksFreq(&RCC_Clocks);
    
    SPI_Generic->BaudRatePrescaler   = 16;        // medium speed
    //  SPI_Generic->BaudRate  = RCC_Clocks.PCLK2_Frequency / SPI_Generic->BaudRatePrescaler;

    return tue_OK;
}
ttc_spi_errorcode_e stm32w_getSPI_Features(u8_t SPI_Index, ttc_spi_generic_t* SPI_Generic) {
    if (!ss_Initialized) stm32w_SPI_ResetAll();
#ifndef TTC_NO_ARGUMENT_CHECKS //{
    // assumption: *SPI_Generic has been zeroed

    Assert(SPI_Index > 0,    ec_InvalidArgument);
    Assert(SPI_Generic != NULL, ec_NULL);
    if (SPI_Index > TTC_AMOUNT_SPIS) return tue_DeviceNotFound;
#endif //}

    SPI_Generic->Flags.All                        = 0;
    SPI_Generic->Flags.Bits.Master                = 1;
    SPI_Generic->Flags.Bits.ClockIdleHigh         = 1;
    SPI_Generic->Flags.Bits.ClockPhase2ndEdge     = 1;
    SPI_Generic->Flags.Bits.Simplex               = 1;
    SPI_Generic->Flags.Bits.Bidirectional         = 1;
    SPI_Generic->Flags.Bits.Receive               = 1;
    SPI_Generic->Flags.Bits.Transmit              = 1;
    SPI_Generic->Flags.Bits.WordSize16            = 1;
    SPI_Generic->Flags.Bits.SoftNSS               = 1;
    SPI_Generic->Flags.Bits.CRC8                  = 1;
    SPI_Generic->Flags.Bits.CRC16                 = 1;
    SPI_Generic->Flags.Bits.FirstBitMSB           = 1;
    SPI_Generic->Flags.Bits.TxDMA                 = 0; // ToDo
    SPI_Generic->Flags.Bits.RxDMA                 = 0; // ToDo
    SPI_Generic->Flags.Bits.Irq_Error             = 0; // ToDo
    SPI_Generic->Flags.Bits.Irq_Received          = 0; // ToDo
    SPI_Generic->Flags.Bits.Irq_Transmitted       = 0; // ToDo

    SPI_Generic->CRC_Polynom = 0xffff;
    SPI_Generic->Layout      = 0;
    
    //   RCC_ClocksTypeDef RCC_Clocks;
    //  RCC_GetClocksFreq(&RCC_Clocks);
    
    SPI_Generic->BaudRatePrescaler   = 255;                             // max divider
    //  SPI_Generic->BaudRate  = RCC_Clocks.PCLK2_Frequency / 2;  // max frequency

    //    SPI_TypeDef* SPI_Base = NULL;
    //    switch (SPI_Index) {           // find corresponding SPI as defined by makefile.100_board_*
    //#ifdef TTC_SPI1
    //      case 1: SPI_Base = TTC_SPI1; break;
    //#endif
    //#ifdef TTC_SPI2
    //      case 2: SPI_Base = TTC_SPI2; break;
    //#endif
    //#ifdef TTC_SPI3
    //      case 3: SPI_Base = TTC_SPI3; break;
    //#endif
    //      default: Assert(0, ec_UNKNOWN); break; // No TTC_SPIn defined! Check your makefile.100_board_* file!
    //    }
    //    switch ( (u32_t) SPI_Base ) {  // find amount of available remapping-layouts (-> RM0008 p. 176)
    //    case (u32_t) SPI1:
    //        SPI_Generic->Layout=1;     // SPI1 provides 1 alternate pin layout
    //        break;
    //    case (u32_t) SPI2:
    //        SPI_Generic->Layout=0;     // SPI2 provides 0 alternate pin layouts
    //    case (u32_t) SPI3:
    //#ifdef STM32F10X_CL
    //            SPI_Generic->Layout=1; // SPI3 provides 1 alternate pin layout in Connection Line Devices
    //#else
    //            SPI_Generic->Layout=0; // SPI3 provides 0 alternate pin layout
    //#endif
    //        break;
    //      default: Assert(0, ec_UNKNOWN);
    //    }
    
    //    switch (SPI_Index) {           // disable features requiring unconfigured pins
    //      case 1: {
    //          #ifndef TTC_SPI1_MOSI
    //            SPI_Generic->Flags.Bits.Transmit = 0;
    //          #endif
    //          #ifndef TTC_SPI1_MISO
    //            SPI_Generic->Flags.Bits.Receive  = 0;
    //          #endif
    //          #ifndef TTC_SPI1_NSS
    //            SPI_Generic->Flags.Bits.SoftNSS  = 1;
    //          #endif
    //          #ifndef TTC_SPI1_SCK
    //            SPI_Generic->Flags.Bits.Transmit = 0;
    //            SPI_Generic->Flags.Bits.Receive  = 0;
    //          #endif

    //          break;
    //      }
    //      case 2: {
    //          #ifndef TTC_SPI2_MOSI
    //            SPI_Generic->Flags.Bits.Transmit = 0;
    //          #endif
    //          #ifndef TTC_SPI2_MISO
    //            SPI_Generic->Flags.Bits.Receive  = 0;
    //          #endif
    //          #ifndef TTC_SPI2_NSS
    //            SPI_Generic->Flags.Bits.SoftNSS  = 1;
    //          #endif
    //          #ifndef TTC_SPI2_SCK
    //            SPI_Generic->Flags.Bits.Transmit = 0;
    //            SPI_Generic->Flags.Bits.Receive  = 0;
    //          #endif

    //          break;
    //      }
    //      case 3: {
    //          #ifndef TTC_SPI3_MOSI
    //            SPI_Generic->Flags.Bits.Transmit = 0;
    //          #endif
    //          #ifndef TTC_SPI3_MISO
    //            SPI_Generic->Flags.Bits.Receive  = 0;
    //          #endif
    //          #ifndef TTC_SPI3_NSS
    //            SPI_Generic->Flags.Bits.SoftNSS  = 1;
    //          #endif
    //          #ifndef TTC_SPI3_SCK
    //            SPI_Generic->Flags.Bits.Transmit = 0;
    //            SPI_Generic->Flags.Bits.Receive  = 0;
    //          #endif

    //          break;
    //      }
    //      default: Assert(0, ec_UNKNOWN); return tue_DeviceNotFound;; // SPI misconfigured! Check your makefile.100_board_* file!
    //    }
    
    return tue_OK;
}
ttc_spi_errorcode_e stm32w_initSPI(u8_t SPI_Index, ttc_spi_generic_t* SPI_Generic, stm32w_spi_architecture_t* SPI_Arch) {
    if (!ss_Initialized) stm32w_SPI_ResetAll();
#ifndef TTC_NO_ARGUMENT_CHECKS //{
    // assumption: *SPI_Arch has been zeroed

  //  Assert(SPI_Generic != NULL, ec_NULL);
#endif //}



    //if(STM32W_SPIx==STM32W_SPI1){
    if(SPI_Index == SC1){
        SC1_MODE_REG = SC1_MODE_DISABLED;

        SC1_SPICFG_REG |= SC_SPIMST;//(1<<SC_SPIMST_BIT); //set mode master
        SC1_TWICTRL2_REG |=SC_TWIACK;//(1<<SC_TWIACK_BIT); //dont know what this is for
        SC1_RATELIN_REG = 1; //set speed
        SC1_RATEEXP_REG =1; //set speed


        //SPI 1
        halGpioConfig (PORTB_PIN(1), GPIOCFG_OUT_ALT); //MOSI
        halGpioConfig (PORTB_PIN(2), GPIOCFG_IN); //MISO
        halGpioConfig (PORTB_PIN(3), GPIOCFG_OUT_ALT_SPECIAL_SCLK); //SCLK  //GPIOCFG not defined in regs.h

        SC1_MODE_REG = SC1_MODE_SPI;


        //        INT_SC1CFG |= (INT_SCRXVAL   |
        //                       INT_SCRXOVF   |
        //                       INT_SC1FRMERR |
        //                       INT_SC1PARERR);
        //        INT_SC1FLAG = 0xFFFF; // Clear any stale interrupts
        //        INT_CFGSET = INT_SC1;

        //        To enable CPU interrupts, set the desired interrupt bits in the second level INT_SCxCFG
        //        register, and enable the top level SCx interrupt in the NVIC by writing the INT_SCx bit in the
        //        INT_CFGSET register.

        //        SC1_INTMODE |= SC_SPIRXVAL

        INT_SC1CFG |= (INT_SCRXVAL);


        //}else if(STM32W_SPIx==STM32W_SPI2){
    }else if(SPI_Index == SC2 ){
        SC2_MODE_REG = SC2_MODE_DISABLED;

        SC2_SPICFG_REG |= SC_SPIMST;//(1<<SC_SPIMST_BIT); //set mode master
        SC2_TWICTRL2_REG |=SC_TWIACK;//(1<<SC_TWIACK_BIT); //dont know what this is for
        SC2_RATELIN_REG = 1; //set speed
        SC2_RATEEXP_REG =1; //set speed


        //SPI 2
        halGpioConfig (PORTA_PIN(0), GPIOCFG_OUT_ALT); //MOSI
        halGpioConfig (PORTA_PIN(1), GPIOCFG_IN); //MISO
        halGpioConfig (PORTA_PIN(2), GPIOCFG_OUT_ALT_SPECIAL_SCLK); //SCLK  //GPIOCFG not defined in regs.h

        SC2_MODE_REG = SC2_MODE_SPI;
    }else {

        Assert(0, ec_InvalidArgument);
    }
    
    return tue_OK;
}
ttc_spi_errorcode_e stm32w_spi_sendBytes(u8_t SPI_Index, const char* Buffer, u16_t Amount) {
    register_stm32f1xx_spi_t* MySPI ;//= stm32w_SPIs[SPI_Index];
    //////////////////////////////////////
    //bypass some selection stuff for spi
    MySPI = SC1;
    //////////////////////////////////////
    // Assert(MySPI != NULL, ec_NULL);
    //assert can not be used because register_stm32f1xx_spi_t defined as enum with SC1 = 0

    while((Amount-- )>0) {
        // _stm32w_spi_sendWord(MySPI, (u16_t) *Buffer);
        _stm32w_spi_send_uint8(MySPI, (u8_t) *Buffer++);


    }
    return tue_OK;
}
ttc_spi_errorcode_e stm32w_spi_sendASCII(u8_t SPI_Index, const char* Buffer, u16_t MaxLength) {
    register_stm32f1xx_spi_t* MySPI = stm32w_SPIs[SPI_Index];
    // Assert(MySPI != NULL, ec_NULL);
    //assert can not be used because register_stm32f1xx_spi_t defined as enum with SC1 = 0
    //////////////////////////////////////
    //bypass some selection stuff for spi
    MySPI = SC1;
    //////////////////////////////////////

    char C=0;
    while ( (MaxLength-- > 0) && ((C=*Buffer++) != 0)  ) {
       _stm32w_spi_send_uint8(MySPI, (u8_t) *Buffer++);
    }
    return tue_OK;
}
ttc_spi_errorcode_e stm32w_spi_sendWord(u8_t SPI_Index, const u16_t Word) {
    register_stm32f1xx_spi_t* MySPI = stm32w_SPIs[SPI_Index];
    Assert(MySPI != NULL, ec_NULL);

    //can not send uint16_t at once, have to split into two actions
    //_stm32w_spi_sendWord(MySPI, Word);
    _stm32w_spi_send_uint8(MySPI, (u8_t) Word&0xFF);
    _stm32w_spi_send_uint8(MySPI, (u8_t) (Word>>8)&0xFF);
    return tue_OK;
}
ttc_spi_errorcode_e stm32w_spi_readByte(u8_t SPI_Index, char* Byte, u32_t TimeOut) {
    register_stm32f1xx_spi_t* MySPI = stm32w_SPIs[SPI_Index];
    // Assert(MySPI != NULL, ec_NULL);

    if ( _stm32w_spi_waitForRXNE(MySPI, TimeOut) ) return tue_TimeOut;

    // *Byte=(char) 0xff & SPI_I2S_ReceiveData( (SPI_TypeDef*) MySPI );
    // MySPI->SPI_SR.RXNE=0;

    return tue_OK;
}
ttc_spi_errorcode_e stm32w_spi_readWord(u8_t SPI_Index, u16_t* Word, u32_t TimeOut) {
    register_stm32f1xx_spi_t* MySPI = stm32w_SPIs[SPI_Index];
    Assert(MySPI != NULL, ec_NULL);

    if ( _stm32w_spi_waitForRXNE(MySPI, TimeOut) ) return tue_TimeOut;

    // *Word=SPI_I2S_ReceiveData( (SPI_TypeDef*) MySPI  );
    // MySPI->SPI_SR.RXNE=0;

    return tue_OK;
}

//} Function definitions
//{ private functions (ideally) -------------------------------------------------

void _stm32w_spi_sendWord(register_stm32f1xx_spi_t* MySPI, const u16_t Word) {
    //Assert(MySPI != NULL, ec_NULL);
    //assert can not be used because register_stm32f1xx_spi_t defined as enum with SC1 = 0
//    if(MySPI== SC1){
//        while ((SC1_SPISTAT&SC_SPITXFREE)!=SC_SPITXFREE) {}
//        SC1_DATA = Word;
//    }else if(MySPI == SC2){
//        while ((SC2_SPISTAT&SC_SPITXFREE)!=SC_SPITXFREE) {}
//        SC2_DATA = Word;
//    }else {
//        Assert(0, ec_InvalidArgument);
//    }
}
void _stm32w_spi_send_uint8(register_stm32f1xx_spi_t* MySPI, const u8_t data) {
    //Assert(MySPI != NULL, ec_NULL);
    //assert can not be used because register_stm32f1xx_spi_t defined as enum with SC1 = 0

    if(MySPI== SC1){
        while ((SC1_SPISTAT&SC_SPITXFREE)!=SC_SPITXFREE) {}
        SC1_DATA = (u8_t)data;
    }else if(MySPI == SC2){
        while ((SC2_SPISTAT&SC_SPITXFREE)!=SC_SPITXFREE) {}
        SC2_DATA = (u8_t)data;
    }else {
        Assert(0, ec_InvalidArgument);
    }
}
ttc_spi_errorcode_e _stm32w_spi_waitForRXNE(register_stm32f1xx_spi_t* MySPI, u32_t TimeOut) {
    //    if (TimeOut > 0) { // wait until word received or timeout
    //        while ( (TimeOut > 0) && (MySPI->SR.RXNE == 0) ) {
    //            if (TimeOut > 9) TimeOut -= 10;
    //            else             TimeOut=0;
    //            uSleep(10);
    //        }
    //        if (TimeOut == 0)  return tue_TimeOut;
    //    }
    //    else {             // wait until word received (no timeout)
    //        while (MySPI->SR.RXNE == 0)
    //            uSleep(10);
    //    }

    return tue_OK;
}

//} private functions

//} Function definitions