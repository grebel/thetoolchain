#ifndef EM357_GPIO_H
#define EM357_GPIO_H

/*{ DEPRECATED_stm32w_gpio.h ***********************************************
 
 * Written by Gregor Rebel, Sascha Poggemann 2010-2012
 *
 * Low Level Driver for General Purpose Input Output (GPIO) on STM32W (former EM357) architecture.
 * 
}*/
//{ defines required to include HAL-library

//}
//{ includes1

#include "ttc_basic.h"

#ifdef EXTENSION_200_cpu_stm32w1xx
#include "stm32w108xx_gpio.h"
#endif

//}includes
//{ low-level driver interface for ttc_gpio.h

/** Macro Arguments
 *
 * PORT      pointer to type ttc_Port_t
 * BANK      pointer to type ttc_gpio_bank_t
 * PIN       0..15 pin number in GPIO bank
 * FIRST_PIN 0..8 pin number of first pin of an 8-bit wide parallel port
 * TYPE      type to configure pin to  (one from ttc_gpio_types.h:ttc_gpio_mode_e)
 * SPEED     speed to configure pin to (one from ttc_gpio_types.h:ttc_gpio_speed_e)
 * BANK_PIN  BANK,PIN  (allows use of PIN_Pxn definitions like PIN_A1)
 * INDEX     u8_t Value storing GPIO-Bank index (0..15) and Pin index (0..15) in high- and low-nibble
 *
 */
#define _driver_ttc_gpio_init(GPIOx, Pin, Type, Speed)                stm32w_gpio_init(GPIOx, Pin, Type, Speed)
//X #define _driver_ttc_gpio_init2(GPIOx, Pin, Type, Speed)               stm32w_gpio_init(GPIOx, Pin, Type, Speed)
#define _driver_ttc_gpio_init_variable(Port, Bank, Pin, Type, Speed)  stm32w_gpio_init_variable(Port, Bank, Pin, Type, Speed)
#define _driver_ttc_gpio_get(BANK,PIN)                                stm32w_gpio_get(BANK,PIN)
#define _driver_ttc_gpio_set(BANK,PIN)                                stm32w_gpio_set(BANK,PIN)
#define _driver_ttc_gpio_clr(BANK,PIN)                                DEPRECATED_stm32w_gpio.clr(BANK,PIN)
#define _driver_ttc_gpio_set2(BANK,PIN)                               stm32w_gpio_set(BANK,PIN)
#define _driver_ttc_gpio_get2(BANK,PIN)                               stm32w_gpio_get(BANK,PIN)
#define _driver_ttc_gpio_clr2(BANK,PIN)                               DEPRECATED_stm32w_gpio.clr(BANK,PIN)
#define _driver_ttc_gpio_create_index8(BANK,PIN)                      DEPRECATED_stm32w_gpio.create_index8(BANK,PIN)
#define _driver_ttc_gpio_from_index8(Index,BANK,PIN)                  stm32w_gpio_from_index8(Index,BANK,PIN)
#define _driver_ttc_gpio_get_variable(Port)                           stm32w_gpio_get_variable(Port)
#define _driver_ttc_gpio_set_variable(Port)                           stm32w_gpio_set_variable(Port)
#define _driver_ttc_gpio_clr_variable(Port)                           DEPRECATED_stm32w_gpio.clr_variable(Port)
#define _driver_ttc_gpio_variable(Port,BANK,PIN)                      stm32w_gpio_variable(Port,BANK,PIN)

//} low-level driver interface for ttc_gpio.h
//{ Defines missing in HAL-library

/** GPIO_PxCFGx Bit Field Values */
// This is a patch for the STM32W Register Mapping in the SimpleMAC library
// regs.h line 4756 ff
// Alternate Output (push-pull) Special SCLK mode
#define GPIOCFG_OUT_ALT_SPECIAL_SCLK                (0xBu)


//}
//{ Architecture-Defines (required by ttc_gpio_types.h)

#define ttc_gpio_bank_t  GPIO_TypeDef*
#define ttc_Port_t     stm32w_Port_t
#define TTC_GPIO_BANK_A    GPIOA
#define TTC_GPIO_BANK_B    GPIOB
#define TTC_GPIO_BANK_C    GPIOC


//{ Define shortcuts for all pins of GPIO Bank A
#define PIN_PA0  TTC_GPIO_BANK_A,0
#define PIN_PA1  TTC_GPIO_BANK_A,1
#define PIN_PA2  TTC_GPIO_BANK_A,2
#define PIN_PA3  TTC_GPIO_BANK_A,3
#define PIN_PA4  TTC_GPIO_BANK_A,4
#define PIN_PA5  TTC_GPIO_BANK_A,5
#define PIN_PA6  TTC_GPIO_BANK_A,6
#define PIN_PA7  TTC_GPIO_BANK_A,7
#define PIN_PA8  TTC_GPIO_BANK_A,8
#define PIN_PA9  TTC_GPIO_BANK_A,9
#define PIN_PA10 TTC_GPIO_BANK_A,10
#define PIN_PA11 TTC_GPIO_BANK_A,11
#define PIN_PA12 TTC_GPIO_BANK_A,12
#define PIN_PA13 TTC_GPIO_BANK_A,13
#define PIN_PA14 TTC_GPIO_BANK_A,14
#define PIN_PA15 TTC_GPIO_BANK_A,15
//}BankA
//{ Define shortcuts for all pins of GPIO Bank B
#define PIN_PB0  TTC_GPIO_BANK_B,0
#define PIN_PB1  TTC_GPIO_BANK_B,1
#define PIN_PB2  TTC_GPIO_BANK_B,2
#define PIN_PB3  TTC_GPIO_BANK_B,3
#define PIN_PB4  TTC_GPIO_BANK_B,4
#define PIN_PB5  TTC_GPIO_BANK_B,5
#define PIN_PB6  TTC_GPIO_BANK_B,6
#define PIN_PB7  TTC_GPIO_BANK_B,7
#define PIN_PB8  TTC_GPIO_BANK_B,8
#define PIN_PB9  TTC_GPIO_BANK_B,9
#define PIN_PB10 TTC_GPIO_BANK_B,10
#define PIN_PB11 TTC_GPIO_BANK_B,11
#define PIN_PB12 TTC_GPIO_BANK_B,12
#define PIN_PB13 TTC_GPIO_BANK_B,13
#define PIN_PB14 TTC_GPIO_BANK_B,14
#define PIN_PB15 TTC_GPIO_BANK_B,15
//}BankB
//{ Define shortcuts for all pins of GPIO Bank C
#define PIN_PC0  TTC_GPIO_BANK_C,0
#define PIN_PC1  TTC_GPIO_BANK_C,1
#define PIN_PC2  TTC_GPIO_BANK_C,2
#define PIN_PC3  TTC_GPIO_BANK_C,3
#define PIN_PC4  TTC_GPIO_BANK_C,4
#define PIN_PC5  TTC_GPIO_BANK_C,5
#define PIN_PC6  TTC_GPIO_BANK_C,6
#define PIN_PC7  TTC_GPIO_BANK_C,7
#define PIN_PC8  TTC_GPIO_BANK_C,8
#define PIN_PC9  TTC_GPIO_BANK_C,9
#define PIN_PC10 TTC_GPIO_BANK_C,10
#define PIN_PC11 TTC_GPIO_BANK_C,11
#define PIN_PC12 TTC_GPIO_BANK_C,12
#define PIN_PC13 TTC_GPIO_BANK_C,13
#define PIN_PC14 TTC_GPIO_BANK_C,14
#define PIN_PC15 TTC_GPIO_BANK_C,15
//}BankC

//}Defines
//{ Structures/ Enums

/* DEPRECATED
#ifndef EXTENSION_200_cpu_stm32w1xx
 typedef enum { // GPIOMode_TypeDef
    GPIOCFG_OUT_mode = 0x1u,
    GPIOCFG_OUT_OD_mode = 0x5u,
    GPIOCFG_OUT_ALT_mode = 0x9u,
    GPIOCFG_OUT_ALT_OD_mode = 0xDu,
    GPIOCFG_ANALOG_mode = 0x0u,
    GPIOCFG_IN_mode = 0x4u,
    GPIOCFG_IN_PUD_mode = 0x8u,
    GPIOCFG_OUT_ALT_SPECIAL_SCLK_mode = 0xBu
} GPIOMode_TypeDef;
#else
// us defines provided by STM32w1xx SimpleMAC-Library
#define GPIOCFG_ANALOG_mode                GPIO_Mode_AN         // = 0x00, GPIO Analog Mode
#define GPIOCFG_OUT_mode                   GPIO_Mode_OUT_PP     // = 0x01, GPIO Output Mode PP
#define GPIOCFG_IN_mode                    GPIO_Mode_IN         // = 0x04, GPIO Input Mode NOPULL
#define GPIOCFG_OUT_OD_mode                GPIO_Mode_OUT_OD     // = 0x05, GPIO Output Mode OD
#define GPIOCFG_IN_PUD_mode                GPIO_Mode_IN_PUD     // = 0x08, GPIO Input Mode PuPd
#define GPIOCFG_OUT_ALT_mode               GPIO_Mode_AF_PP      // = 0x09, GPIO Alternate function Mode PP
#define GPIOCFG_OUT_ALT_SPECIAL_SCLK_mode  GPIO_Mode_AF_PP_SPI  // = 0x0B, GPIO Alternate function Mode SPI SCLK PP
#define GPIOCFG_OUT_ALT_OD_mode            GPIO_Mode_AF_OD      // = 0x0D  GPIO Alternate function Mode OD

#endif

DEPRECATED*/

typedef enum { // GPIOSpeed_TypeDef
    GPIOSpeed_not_implemented= 0
} stm32w_gpio_speed_t;

// amount of gpio banks (A..G)
#define TTC_GPIO_MAX_AMOUNT 3

typedef struct { // stm32w_Port_t
    GPIO_TypeDef* GPIOx;
    u8_t Pin;
} __attribute__((__packed__)) stm32w_Port_t;

//}Structures/ Enums
//{ includes2

#include "ttc_gpio_types.h"  // must be included AFTER defining some ttc_XXXX thingies
//? #include "core_cm3.h"

//}includes2
//{ Prototypes

/** converts from architecture independent pin type to stm32w pendant
 * @param    Type   one of tgm_analog_in, tgm_input_floating, tgm_input_pull_down, tgm_input_pull_up, tgm_output_open_drain, tgm_output_push_pull, tgm_alternate_function_push_pull, tgm_alternate_function_open_drain
 * @return   one of GPIO_Mode_AIN, GPIO_Mode_IN_FLOATING, GPIO_Mode_IPD, GPIO_Mode_IPU, GPIO_Mode_Out_OD, GPIO_Mode_Out_PP, GPIO_Mode_Out_OD, GPIO_Mode_AF_PP, GPIO_Mode_AF_OD
 */
GPIOMode_TypeDef stm32w_map_Type(ttc_gpio_mode_e Mode);

/** converts from architecture independent pin speed to stm32w pendant
 * @param    Type   one of tgs_Min, tgs_2MHz, tgs_10MHz, tgs_50MHz, tgs_Max
 * @return   one of GPIO_Speed_2MHz, GPIO_Speed_10MHz, tgs_Max
 */
stm32w_gpio_speed_t stm32w_map_Speed(ttc_gpio_speed_e Speed);

/** initializes single port bit for use as input/ output
 * @param Port  filled out struct as being returned by stm32w_loadPort_variable()
 * @param Type   one of GPIO_Mode_AIN, GPIO_Mode_IN_FLOATING, GPIO_Mode_IPD, GPIO_Mode_IPU, GPIO_Mode_Out_OD, GPIO_Mode_Out_PP, GPIO_Mode_Out_OD, GPIO_Mode_AF_PP, GPIO_Mode_AF_OD
 * @param Speed  one of GPIO_Speed_2MHz, GPIO_Speed_10MHz, tgs_Max
 */
void stm32w_gpio_init_variable(stm32w_Port_t* Port, GPIO_TypeDef* GPIOx, u8_t Pin, GPIOMode_TypeDef Type, stm32w_gpio_speed_t Speed);


/** initializes single port bit for use as input/ output
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, TTC_GPIO_BANK_D, TTC_GPIO_BANK_E, TTC_GPIO_BANK_F
 * @param Pin    0..7
 * @param Type   one from ttc_gpio_mode_e as defined in ttc_gpio_types.h
 * @param Speed  one from ttc_gpio_speed_e as defined in ttc_gpio_types.h
 */
//void stm32w_gpio_init(GPIO_TypeDef* GPIOx, u8_t Pin, GPIOMode_TypeDef Type, GPIOSpeed_TypeDef Speed);
void stm32w_gpio_init(GPIO_TypeDef* GPIOx, u8_t Pin, ttc_gpio_mode_e Mode, stm32w_gpio_speed_t Speed);

/** allows to load struct from list type definition
 *
 * @param Port   points to structure to be filled with given data
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, GPIOD, GPIOE, GPIOF
 * @param Pin    0..15
 *
 * Example:
 * #define PB_LED1=TTC_GPIO_BANK_C,6
 * stm32w_Port_t Port;
 * stm32w_loadPort_variable(&Port, PB_LED1);
 * stm32w_initPort_variable(&Port, GPIO_Mode_Out_PP);
 * stm32w_gpio_set_variable(&Port);
 */
void stm32w_loadPort_variable(stm32w_Port_t* Port, GPIO_TypeDef* GPIOx, u8_t Pin);

/** read/ set to 1/ set to 0 current port pin
 * These functions provide a more dynamic and debuggable way to access port bits.
 *
 * @param Port  filled out struct as being returned by stm32w_loadPort_variable()
 */
BOOL stm32w_gpio_get_variable(stm32w_Port_t* Port);
void stm32w_gpio_set_variable(stm32w_Port_t* Port);
void DEPRECATED_stm32w_gpio.clr_variable(stm32w_Port_t* Port);

/** read/ set to 1/ set to 0 current port pin
 * These are the real low level port bit functions with smallest overhead,
 * but they require to provide GPIOx and Pin on every function call.
 *
 * @param GPIOx  one of TTC_GPIO_BANK_A, TTC_GPIO_BANK_B, TTC_GPIO_BANK_C, GPIOD, GPIOE, GPIOF
 * @param Pin    0..15
 */
BOOL stm32w_gpio_get(GPIO_TypeDef* GPIOx, u8_t Pin);
void stm32w_gpio_set(GPIO_TypeDef* GPIOx, u8_t Pin);
void DEPRECATED_stm32w_gpio.clr(GPIO_TypeDef* GPIOx, u8_t Pin);


/** creates numeric 8-bit representing given GPIO-bank and pin number
 *
 * @param GPIOx  GPIO bank to use (-> ttc_gpio_types.h:ttc_gpio_bank_t)
 *               Note: GPIOx, Pin can be given as one argument by use of Pin_Pxn macro (TTC_GPIO_BANK_A, 7) = Pin_PA7
 * @param Pin    0..15
 * @return       8-bit value representing given GPIO-pin in a memory friendly format
 */
u8_t DEPRECATED_stm32w_gpio.create_index8(ttc_gpio_bank_t GPIOx, u8_t Pin);

/** restores GPIO-bank and pin number from 8-bit value
 *
 * @param PhysicalIndex  8-bit value representing GPIO-bank + pin
 * @param GPIOx          loaded with GPIO bank (-> ttc_gpio_types.h:ttc_gpio_bank_t)
 * @param Pin            loaded with pin number
 */
void stm32w_gpio_from_index8(u8_t PhysicalIndex, ttc_gpio_bank_t* GPIOx, u8_t* Pin);

//} Prototypes

#endif

