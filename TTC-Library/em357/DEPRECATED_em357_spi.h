#ifndef STM32_SPI_H
#define STM32_SPI_H

/*{ stm32w_spi.h **********************************************************
 
                      The ToolChain
                      
   Universal Synchronous Asynchronous Receiver and Transmitters (SPIs)
   for ARM CortexM3 microcontrollers.
   Currently tested to comply to STM32F10x types.
   
   written by Gregor Rebel 2012

 
}*/
//{ Defines/ TypeDefs ****************************************************


//} Defines
//{ Includes *************************************************************

#include "ttc_spi_types.h"
//#include "ttc_multitask.h"

 // Basic set of helper functions
#include "DEPRECATED_stm32w_gpio.h"
//#include "DEPRECATED_stm32w_registers.h"

#include "gnu.h"
#include "regs.h"
#include "STM32W_regs_patch.h"
#include "stm32w108_type.h"
#include "STM32W_types_patch.h"
#include "micro-common.h"



//X #include <stdlib.h>

//#define USE_STDPERIPH_DRIVER
//#include "stm32wf10x.h"
//#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
//#include "stm32wf10x_conf.h"
//#include "stm32wf10x_spi.h"
//#include "misc.h"

/*{ optional
// ADC
#include "stm32wf10x_adc.h"
#include "stm32wf10x_dma.h"

// Timers
#include "stm32wf10x_tim.h"

#include "stm32wf10x_it.h"
#include "system_stm32wf10x.h"

#include "bits.h"
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
}*/

//} Includes
//{ Structures/ Enums ****************************************************
typedef enum {SC1, SC2}register_stm32f1xx_spi_t;
typedef struct {void * SPI1;
                void * SPI2;
               }SPI_TypeDef;
typedef struct {  // architecture specific configuration data of single SPI 
  SPI_TypeDef* Base;     // base address of SPI registers
  stm32w_Port_t PortMISO;   // port pin for MISO
  stm32w_Port_t PortMOSI;   // port pin for MOSI
  stm32w_Port_t PortSCK;    // port pin for SCK
  stm32w_Port_t PortNSS;    // port pin for NSS
} __attribute__((__packed__)) stm32w_spi_architecture_t;

//} Structures/ Enums
//{ Global Variables *****************************************************



//} Global Variables
//{ Function prototypes **************************************************

/* resets library. Automatically called.
 */
void stm32w_SPI_ResetAll();

/* fills out given SPI_ with default values for indexed SPI
 * @param SPI_Index     device index of SPI to init (1..stm32w_getMax_SPI_Index())
 * @param SPI_Generic   pointer to struct ttc_spi_generic_t
 * @return  == 0:         *SPI_Generic has been initialized successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32w_getSPI_Defaults(u8_t SPI_Index, ttc_spi_generic_t* SPI_Generic);

/* fills out given SPI_Generic with maximum valid values for indexed SPI
 * @param SPI_Index     device index of SPI to init (1..stm32w_getMax_SPI_Index())
 * @param SPI_Generic   pointer to struct ttc_spi_generic_t
 * @return  == 0:         *SPI_Generic has been initialized successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32w_getSPI_Features(u8_t SPI_Index, ttc_spi_generic_t* SPI_Generic);

/* initializes single SPI
 * @param SPI_Index     device index of SPI to init (1..stm32w_getMax_SPI_Index())
 * @param SPI_Generic   filled out struct ttc_spi_generic_t
 * @return  == 0:         SPI has been initialized successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32w_initSPI(u8_t SPI_Index, ttc_spi_generic_t* SPI_Generic, stm32w_spi_architecture_t* SPI_Arch);

/* Send out given amount of raw bytes from buffer.
 * Will not interpret bytes in buffer.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * 
 * @param SPI_Index    device index of SPI to init (0..stm32w_getMax_SPI_Index()-1)
 * @param Buffer         memory location from which to read data
 * @param Amount         amount of bytes to send from Buffer[]
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32w_spi_sendBytes(u8_t SPI_Index, const char* Buffer, u16_t Amount);

/* Send out given bytes from buffer until first zero byte or MaxLength reached.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 * 
 * @param SPI_Index    device index of SPI to init (0..stm32w_getMax_SPI_Index()-1)
 * @param Buffer         memory location from which to read data
 * @param MaxLength      no more than this amount of bytes will be send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32w_spi_sendASCII(u8_t SPI_Index, const char* Buffer, u16_t MaxLength);

/* Send out given data word (8 or 9 bits)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until all bytes have been sent!
 *
 * @param SPI_Index    device index of SPI to init (0..ttc_getMax_SPI_Index()-1)
 * @param Byte           8 or 9 bit value to send
 * @return               == 0: Buffer has been sent successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32w_spi_sendWord(u8_t SPI_Index, const u16_t Word);

/* Reads single data word from input buffer (8 or 9 bits)
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid word has been received or timeout occurs
 *
 * @param SPI_Index    device index of SPI to init (0..ttc_getMax_SPI_Index()-1)
 * @param Word           pointer to 16 bit buffer where to store Word
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32w_spi_readWord(u8_t SPI_Index, u16_t* Word, u32_t TimeOut);

/* Reads single data byte from input buffer.
 *
 * Note: SPI must be initialized before!
 * Note: This function is partially thread safe (use it for each SPI from one thread only)!
 * Note: This function blocks until a valid byte has been received or timeout occurs
 *
 * @param SPI_Index    device index of SPI to init (0..ttc_getMax_SPI_Index()-1)
 * @param Word           pointer to 16 bit buffer where to store Word
 * @param TimeOut        >0: max microseconds to wait; ==0: no timeout
 * @return               == 0: Word has been read successfully; != 0: error-code
 */
ttc_spi_errorcode_e stm32w_spi_readByte(u8_t SPI_Index, char* Byte, u32_t TimeOut);

/* Sends out given byte.
 * Note: This low-level function should not be called from outside!
 */
void _stm32w_spi_sendWord(register_stm32f1xx_spi_t* MySPI, const u16_t Word);
void _stm32w_spi_send_uint8(register_stm32f1xx_spi_t* MySPI, const u8_t Word);

/* Wait until word has been received or timeout occured.
 * Note: This low-level function should not be called from outside!
 */
ttc_spi_errorcode_e _stm32w_spi_waitForRXNE(register_stm32f1xx_spi_t* MySPI, u32_t TimeOut);

//} Function prototypes

#endif //STM32_SPI_H
