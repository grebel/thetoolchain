#ifndef SDCARD_SPI_H
#define SDCARD_SPI_H

/** { sdcard_spi.h **********************************************
 *
 *                               The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_sdcard.h providing
 *  function prototypes being required by ttc_sdcard.h
 *
 *  secure digital card storage connected via spi bus
 *
 *  Created from template device_architecture.h revision 32 at 20180130 09:59:56 UTC
 *
 *  Note: See ttc_sdcard.h for description of spi independent SDCARD implementation.
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of sdcard_spi (Do not delete this line!)
 *
 *  Low-level driver for Secure Digital Cards being connected via Serial Peripheral Interface (SPI).
 *
 *  This sdcard driver requires a preconfigured ttc_spi device to communicate with sdcard.
 *  The current low level spi_* driver must be able to communicate at low speed (<200kHz) and at high
 *  speed (<20MHz). This is not trivial as many spi drivers are only tested at high speed.
 *  The sdcard_spi driver was tested successfully with ttc-lib/spi/spi_stm32f1xx.c on an Olimex STM32-P107
 *  Rev. A prototype board. Check this driver as a reference implementation!
 *
 *  To use an SDCARD via SPI as logical index <SDCARD_INDEX>, define these constants in your board makefile:
 *  - TTC_SDCARD<SDCARD_INDEX>                   = E_ttc_sdcard_architecture_spi
 *  - TTC_SDCARD<SDCARD_INDEX>_SPI_LOGICAL_INDEX = <SPI_INDEX>
 *  - TTC_SDCARD<SDCARD_INDEX>_SPI_SLAVE_INDEX   = <SLAVE_INDEX>
 *  with
 *    <SDCARD_INDEX> = Logical index (1..10) of ttc_sdcard device to use
 *    <SPI_INDEX>    = Logical index (1..10) of ttc_spi device to use
 *    <SLAVE_INDEX>  = Logical index (1..10) of slave select pin connected to SDCARD connector.
 *
 *  Used ttc_spi device must be configured with an extra spi slave pin connected to slave select pin of sdcard slot:
 *    TTC_SPI<SPI_INDEX>_NSS<SLAVE_INDEX>=<SDCARD_PIN>
 *      <SPI_INDEX>   = TTC_SDCARD<SDCARD_INDEX>_SPI_LOGICAL_INDEX
 *      <SLAVE_INDEX> = TTC_SDCARD<SDCARD_INDEX>_SPI_SLAVE_INDEX
 *      <SDCARD_PIN>  = one from e_ttc_gpio_pin; gpio pin connected to chip select input of sdcard slot
 *
 *  Example board makefile excerpt configuring TTC_SDCARD1 via TTC_SPI1 using first slave select pin E_ttc_gpio_pin_b15

    // example configuration of TTC_SPI1
    COMPILE_OPTS += -DTTC_SPI1=ttc_device_3            # SPI bus connected to SDCARD slot #1
    COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_c12 # GPIO pin: Master Out Slave In
    COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_c11 # GPIO pin: Master In Slave Out
    COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_c10  # GPIO pin: SPI Clock
    COMPILE_OPTS += -DTTC_SPI1_NSS1=E_ttc_gpio_pin_b15  # GPIO pin: Slave Select #1 (Low Active)

    // example configuration of TTC_SDCARD1 using TTC_SPI1
    COMPILE_OPTS += -DTTC_SDCARD1=E_ttc_sdcard_architecture_spi   # using spi bus to connect to SDCARD #1
    COMPILE_OPTS += -DTTC_SDCARD1_SPI_LOGICAL_INDEX=1             # using TTC_SPI1 to connect to SDCARD
    COMPILE_OPTS += -DTTC_SDCARD1_SPI_SLAVE_INDEX=1               # using spi bus slave #1 (TTC_SPI1_NSS1) to select of SDCARD

}*/
//{ Defines/ TypeDefs ****************************************************

#undef EXTENSION_SDCARD_DRIVER_AVAILABLE
#define EXTENSION_SDCARD_DRIVER_AVAILABLE // signal existence of at least one low-level driver (checked by ttc_sdcard_interface.c)

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "sdcard_spi.c"
//
#include "../ttc_sdcard_types.h" // will include sdcard_spi_types.h (do not include it directly!)
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macro definitions ****************************************************

// define all supported low-level functions for ttc_sdcard_interface.h
//
// Some driver functions may be optional to implement.
// Check ttc_sdcard_interface.c for default implementations.
// ttc_driver_*() functions not being implemented must be undefined via #undef
// Example: #undef ttc_driver_sdcard_foo
//
#define ttc_driver_sdcard_configuration_check sdcard_spi_configuration_check
#define ttc_driver_sdcard_deinit              sdcard_spi_deinit
#define ttc_driver_sdcard_init                sdcard_spi_init
#define ttc_driver_sdcard_load_defaults       sdcard_spi_load_defaults
#define ttc_driver_sdcard_prepare             sdcard_spi_prepare
#define ttc_driver_sdcard_reset               sdcard_spi_reset
#define ttc_driver_sdcard_medium_detect       sdcard_spi_medium_detect
#define ttc_driver_sdcard_command_r1          sdcard_spi_command_r1
#define ttc_driver_sdcard_medium_go_idle      sdcard_spi_medium_go_idle
#define ttc_driver_sdcard_medium_unmount      sdcard_spi_medium_unmount
#define ttc_driver_sdcard_blocks_count        sdcard_spi_blocks_count
#define ttc_driver_sdcard_blocks_size         sdcard_spi_blocks_size
#define ttc_driver_sdcard_command_rn          sdcard_spi_command_rn
#define ttc_driver_sdcard_set_speed           sdcard_spi_set_speed
#define ttc_driver_sdcard_block_read          sdcard_spi_block_read
#define ttc_driver_sdcard_block_write         sdcard_spi_block_write
#define ttc_driver_sdcard_wait_busy sdcard_spi_wait_busy
//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//} Macro definitions
/** Function prototypes *************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_sdcard.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_sdcard.h for details.
 *
 * You normally should not need to add functions here.
 * Add your private functions in the private section below.
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_sdcard_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_sdcard_features.
 *
 * @param Config        Configuration of sdcard device
 * @return              Fields of *Config have been adjusted if necessary
 */
void sdcard_spi_configuration_check( t_ttc_sdcard_config* Config );


/** shutdown single SDCARD unit device
 * @param Config        Configuration of sdcard device
 * @return              == 0: SDCARD has been shutdown successfully; != 0: error-code
 */
e_ttc_sdcard_errorcode sdcard_spi_deinit( t_ttc_sdcard_config* Config );


/** initializes single SDCARD unit for operation
 * @param Config        Configuration of sdcard device
 * @return              == 0: SDCARD has been initialized successfully; != 0: error-code
 */
e_ttc_sdcard_errorcode sdcard_spi_init( t_ttc_sdcard_config* Config );


/** loads configuration of indexed SDCARD unit with default values
 * @param Config        Configuration of sdcard device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_sdcard_errorcode sdcard_spi_load_defaults( t_ttc_sdcard_config* Config );


/** Prepares sdcard Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void sdcard_spi_prepare();


/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of sdcard device
 */
void sdcard_spi_reset( t_ttc_sdcard_config* Config );


/** Fast check to see if an sdcard has been inserted or removed in indexed slot
 *
 * This check should be implemented as fast as possible as it will probably
 * be called regular at a high frequency.
 *
 * Note: Low-level driver must only return codes E_ttc_storage_event_media_present or E_ttc_storage_event_no_card!
 *       All other event codes are handled by ttc_sdcard_medium_detect().
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <SDCARD_INDEX> is defined via COMPILE_OPTS += -DTTC_SDCARD<SDCARD_INDEX> lines in extensions.active/makefile
 * @return             (e_ttc_storage_event)    ==0: no sdcard available; >0: see e_ttc_storage_event for details!
 */
e_ttc_storage_event sdcard_spi_medium_detect( t_ttc_sdcard_config* Config );



/** Send a command and argument to sdcard via curren low-level driver
 *
 * Note: Card response is stored in Config->Card.LastResponse.
 *
 * @param Config       (t_ttc_sdcard_config*)          Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                          Logical index of sdcard instance. Each logical device <SDCARD_INDEX> is defined via COMPILE_OPTS += -DTTC_SDCARD<SDCARD_INDEX> lines in extensions.active/makefile
 * @param Command      (e_ttc_sdcard_command)          A valid sdcard command
 * @param Argument     (t_u32)                         Value depends on actual command
 * @return             Config->Card.LastResponse.byte  ==0xff: no response from card; !=0xff: response code from card
 */
void sdcard_spi_command_r1( t_ttc_sdcard_config* Config, const t_ttc_sdcard_command_argument* CommandArgument );


/** Powers up sdcard in slot and prepares it for usage
 *
 * Will do nothing, if no sdcard is detected
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <SDCARD_INDEX> is defined via COMPILE_OPTS += -DTTC_SDCARD<SDCARD_INDEX> lines in extensions.active/makefile
 * @return             == 0: sdcard device has been awaken successfully and can be used; != 0: error-code
 */
e_ttc_sdcard_errorcode sdcard_spi_medium_go_idle( t_ttc_sdcard_config* Config );


/** Powers down sdcard to be removed safely
 *
 * Will do nothing if sdcard is currently not mounted.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <SDCARD_INDEX> is defined via COMPILE_OPTS += -DTTC_SDCARD<SDCARD_INDEX> lines in extensions.active/makefile
 */
void sdcard_spi_medium_unmount( t_ttc_sdcard_config* Config );


/** Read data of single block from sdcard into given buffer
 *
 * Note: If BufferSize < ttc_sdcard_blocks_size() then remaining bytes in block will be ignored.
 * Note: Will call ttc_sdcard_medium_go_idle() if required.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <SDCARD_INDEX> is defined via COMPILE_OPTS += -DTTC_SDCARD<SDCARD_INDEX> lines in extensions.active/makefile
 * @param BlockAddress (t_u32)                 Address of block on sdcard (0..ttc_sdcard_blocks_count()-1)
 * @param Buffer       (t_u8*)                 Memory buffer where to store data
 * @param BufferSize   (t_u16)                 Amount of valid bytes in Buffer (will write to Buffer[0..BufferSize-1]
 * @return             (e_ttc_sdcard_errorcode)   ==0: block was read into Buffer[] successfully; >0: error code
 */
e_ttc_sdcard_errorcode sdcard_spi_block_read( t_ttc_sdcard_config* Config, t_u32 BlockAddress, t_u8* Buffer, t_u16 BufferSize );


/** Returns amount of data blocks on inserted sdcard.
 *
 * Note: Will call ttc_sdcard_medium_go_idle() if required.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <SDCARD_INDEX> is defined via COMPILE_OPTS += -DTTC_SDCARD<SDCARD_INDEX> lines in extensions.active/makefile
 * @return             (t_u32)                 amount of available data blocks on current sdcard
 */
t_u32 sdcard_spi_blocks_count( t_ttc_sdcard_config* Config );


/** Returns size of data blocks on current sdcard
 *
 * All blocks on an sdcard have same size. SDCARDs with larger capacity may have larger block sizes.
 * Note: Will call ttc_sdcard_medium_go_idle() if required.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <SDCARD_INDEX> is defined via COMPILE_OPTS += -DTTC_SDCARD<SDCARD_INDEX> lines in extensions.active/makefile
 * @return             (t_u16)                 block size in bytes
 */
t_u16 sdcard_spi_blocks_size( t_ttc_sdcard_config* Config );


/** Write data from given buffer into single block onto sdcard
 *
 * If BufferSize is less than current block size, then rest of block will be filled with zeroes.
 * Notes for implementing low-level driver:
 * - High-level driver ensures that sdcard is present and mounted
 * - Do not wait until card has finished writing. Set Config->Flags.CardBusyWriting=1 instead!
 * - Do not call sdcard_common_status() or issue CMD13 after writing! This is done by high-level driver.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param BlockAddress (t_u32)                 Address of block on sdcard (0..ttc_sdcard_blocks_count()-1)
 * @param Buffer       (t_u8*)                 Memory buffer storing data to be written onto sdcard
 * @param BufferSize   (t_u16)                 Amount of valid bytes in Buffer (will send Buffer[0..BufferSize-1]
 * @return             (e_ttc_sdcard_errorcode)   ==0: block was written successfully; >0: error code
 */
e_ttc_sdcard_errorcode sdcard_spi_block_write( t_ttc_sdcard_config* Config, t_u32 BlockAddress, const t_u8* Buffer, t_u16 BufferSize );


/** Send one commands and its argument to sdcard
 *
 * The use of a struct pointer allows to reduce ram usage and data copy operations.
 *
 * @param Config           (t_ttc_sdcard_config*)            Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param CommandArgument  (t_ttc_sdcard_command_argument*)  Pointer to a set of command number and its argument (-> sdcard_common_prepare_command() )
 * @param Buffer           (t_u8*)                           Storage where to store return values
 * @param Amount           (t_u16)                           Amount of bytes to read after sending command + argument
 * @return                 (t_u16)                           Amount of bytes being read
 */
t_u16 sdcard_spi_command_rn( t_ttc_sdcard_config* Config, const t_ttc_sdcard_command_argument* CommandArgument, t_u8* Buffer, t_u16 Amount );



/** ADD_SHORT_DESCRIPTION_HERE
 *
 * ADD_MORE_DESCRIPTION_HERE
 *
 * @param Config         (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex   (t_u8)                  Logical index of sdcard instance. Each logical device <SDCARD_INDEX> is defined via COMPILE_OPTS += -DTTC_SDCARD<SDCARD_INDEX> lines in extensions.active/makefile
 * @param InterfaceSpeed (e_ttc_sdcard_speed)
 * @return               (e_ttc_sdcard_errorcode)
 */
e_ttc_sdcard_errorcode sdcard_spi_set_speed( t_ttc_sdcard_config* Config, e_ttc_sdcard_speed InterfaceSpeed );


/** Wait while card is busy.
 *
 * This will wait if one of the busy flags (Config->Flags.Busy*) is set.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @return             (BOOL)                  ==TRUE: card is ready now; ==FALSE: card did not finish within timeout (Config->Init.Retries)
 */
BOOL sdcard_spi_wait_busy( t_ttc_sdcard_config* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//} Function prototypes

#endif //SDCARD_SPI_H
