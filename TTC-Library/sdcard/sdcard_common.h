#ifndef sdcard_common_h
#define sdcard_common_h

/** sdcard_common.h ****************************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to sdcard low-level drivers of all architectures.
 *  The functions provided here are common in two ways:
 *  1) They can be called from high-level driver ttc_sdcard.c and from current low-level driver sdcard_*.c.
 *  2) All common_sdcard_*() functions can call low-level driver functions as _driver_sdcard_*().
 *
 *  Created from template ttc-lib/templates/device_common.h revision 14 at 20180130 09:59:56 UTC
 *
 *  Authors: Gregor Rebel
 *
 *  Description of sdcard_common (Do not delete this line!)
 *
 *  This file provides common functions usefull for all kind of sdcard drivers.
 *
}*/
//  Defines/ TypeDefs ****************************************************{

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
/** Includes *************************************************************{
 *
 * Header files include all header files that are required to include this
 * header file. Includes that provide function declarations should be placed
 * in "sdcard_spi.c"
 */

#include "../ttc_basic.h"
#include "../ttc_sdcard_types.h"  // will include sdcard_common_types.h (do not include it directly!)
#include "../ttc_storage_types.h" // we support generic storage support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
/** Macro definitions ****************************************************{
 *
 * define all supported low-level functions for ttc_sdcard_interface.h
 *
 * Note: ttc_driver_*() functions not being implemented must be undefined via #undef
 * Example: #undef ttc_driver_sdcard_foo
 */

//InsertLowLevelMacros above (DO NOT REMOVE THIS LINE!)

//}MacroDefinitions

extern const t_u8 sdcard_common_token_StartSingleBlock;

/** Function prototypes **************************************************{
 *
 * The prototypes listed below have been created automatically by create_Driver.pl based
 * on section "Prototypes of low-level driver functions" in file ttc_sdcard.h.
 * You may remove all prototypes here and have them reconstructed automatically.
 * Check file ttc_sdcard.h for details.
 *
 * You normally should not need to add functions here.
 * Global feature functions must be added in ttc_sdcard.h and propagated into
 * all low-level drivers via "./create_DeviceDriver.pl sdcard UPDATE".
 */


/** Calculate checksum for sdcard commands
 *
 * @param Buffer (t_u8*) Data buffer to send or check
 * @param Amount (t_u8)  amount of valid bytes in Buffer[] (except crc byte)
 * @param CRC7   (t_u8)  initial CRC7 value (=0 to calculate new crc7; return value from previous call when continuing calculation)
 * @return       (t_u8)  checksum calculated for given bytes
 */
t_u8 sdcard_common_crc7_calculate( const t_u8* Buffer, t_u16 Amount, t_u8 CRC7 );

/** Calculate 7 bit checksum for sdcard commands
 *
 * @param Buffer (t_u8*) Data buffer to send or check
 * @param Amount (t_u8)  amount of valid bytes in Buffer[] (except crc byte)
 * @param CRC7   (t_u8)  CRC7 value to be checked (typically last byte in buffer)
 * @return       (BOOL)  ==TRUE: Given checksum matches calculated checksum for given Buffer[]; ==FALSE: checksum mismatch
 */
BOOL sdcard_common_crc7_check( const t_u8* Buffer, t_u16 Amount, t_u8 CRC7 );

/** Calculate 16 bit checksum for sdcard data blocks
 *
 * @param Buffer (t_u8*) Data buffer to send or check
 * @param Amount (t_u8)  amount of valid bytes in Buffer[] (except crc byte)
 * @param CRC16  (t_u16) initial CRC16 value (=0 to calculate new checksum; return value from previous call when continuing calculation)
 * @return       (t_u8)  checksum calculated for given bytes
 */
t_u16 sdcard_common_crc16_calculate( const t_u8* Buffer, t_u16 Amount, t_u16 CRC16 );

/** Returns amount of data blocks on inserted sdcard.
 *
 * Note: Will call ttc_sdcard_medium_mount() if required.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @return             (t_u32)                 amount of available data blocks on current sdcard
 */
t_u32 sdcard_common_blocks_count( t_ttc_sdcard_config* Config );

/** Returns size of data blocks on current sdcard
 *
 * All blocks on an sdcard have same size. SDCARDs with larger capacity may have larger block sizes.
 * Note: Will call ttc_sdcard_medium_mount() if required.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @return             (t_u16)   block size in bytes
 */
t_u16 sdcard_common_blocks_size( t_ttc_sdcard_config* Config );

/** Read all relevant data from current sdcard
 *
 * Note: Function will just return if (Config->Flags.CardDataValid == 1)
 *
 * @param Config          (t_ttc_sdcard_config*) Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @return                (t_ttc_sdcard_data*)   ==NULL: no sdcard available; !=NULL: pointer to relevant card data
 */
t_ttc_sdcard_data* sdcard_common_card_data( t_ttc_sdcard_config* Config );

/** Disables Cyclic Redundancy Checksums in communication with SDCARD
 *
 * CRC is enabled in SDCARD by default during power up.
 * Disabling CRC will make communication faster but less secure.
 * Check Config->Flags.CardDisabledCRC for latest state.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                  Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @param DisableCRC   (BOOL)                  ==TRUE: disable checksum bytes; ==FALSE: enable checksum bytes
 * @return             (u_ttc_sdcard_response)        ==0x00: command executed successfully;==0xff: no answer from sdcard; !=0x00: error returned by sdcard
 */
void sdcard_common_disable_crc( t_ttc_sdcard_config* Config, BOOL DisableCRC );

/** Prepare given buffer to be used as command + argument for calling _driver_sdcard_*() functions
 *
 * sdcard_*() low-level functions required a pointer to an initialized structure instead of individual values.
 * This function will fill referenced buffer with all required data including checksum byte.
 *
 * @param Config          (t_ttc_sdcard_config*)            Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param CommandArgument (t_ttc_sdcard_command_argument*)  Buffer that is to be initialized as valid command + argument
 * @param Command         (e_ttc_sdcard_command)            A valid sdcard command
 * @param Argument        (t_u32)                           Value depends on actual command
 */
void sdcard_common_prepare_command( t_ttc_sdcard_config* Config, t_ttc_sdcard_command_argument* CommandArgument, e_ttc_sdcard_command Command, t_u32 Argument );

/** Send a command and argument to sdcard and return single value
 *
 * Note: Card response is stored in Config->Card.LastResponse.
 *
 * @param Config       (t_ttc_sdcard_config*)          Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param Command      (e_ttc_sdcard_command)          A valid sdcard command
 * @param Argument     (t_u32)                         Value depends on actual command
 * @return             Config->Card.LastResponse.byte  ==0xff: no response from card; !=0xff: response code from card
 */
void sdcard_common_command_r1( t_ttc_sdcard_config* Config, e_ttc_sdcard_command Command, t_u32 Argument );

/** Send an application specific command and argument to sdcard and return single value
 *
 * Note: Card response is stored in Config->Card.LastResponse.
 *
 * @param Config              (t_ttc_sdcard_config*)          Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param ApplicationCommand  (e_ttc_sdcard_command_application)      A valid sdcard command
 * @param Argument            (t_u32)                         Value depends on actual command
 * @return                    Config->Card.LastResponse.byte  ==0xff: no response from card; !=0xff: response code from card
 */
void sdcard_common_command_application_r1( t_ttc_sdcard_config* Config, e_ttc_sdcard_command_application ApplicationCommand, t_u32 Argument );

/** Send an application specific commandand read given amount of return values
 *
 * Note: Card response is stored in Config->Card.LastResponse.
 *
 * @param Config              (t_ttc_sdcard_config*)               Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param ApplicationCommand  (e_ttc_sdcard_command_application)   A valid sdcard command
 * @param Argument            (t_u32)                              Value depends on actual command
 * @param Buffer              (t_u8*)                              Storage where to store return values
 * @param Amount              (t_u16)                              Amount of bytes to read after sending command + argument
 * @return                    Config->Card.LastResponse.byte  ==0xff: no response from card; !=0xff: response code from card
 */
void sdcard_common_command_application_rn( t_ttc_sdcard_config* Config, e_ttc_sdcard_command_application ApplicationCommand, t_u32 Argument, t_u8* Buffer, t_u16 Amount );

/** Send a command and argument to sdcard and read given amount of return values
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param Command      (e_ttc_sdcard_command)  A valid sdcard command
 * @param Argument     (t_u32)                 Value depends on actual command
 * @param Buffer       (t_u8*)                 Storage where to store return values
 * @param Amount       (t_u16)                 Amount of bytes to read after sending command + argument
 * @return             (t_u16)                 Amount of bytes being read
 */
t_u16 sdcard_common_command_rn( t_ttc_sdcard_config* Config, e_ttc_sdcard_command Command, t_u32 Argument, t_u8* Buffer, t_u16 Amount );

/** Requests current card status
 *
 * @param Config       (t_ttc_sdcard_config*)   Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @return                                      Config->Card.LastResponse = current status returned from card
 */
void sdcard_common_status( t_ttc_sdcard_config* Config );

/** Wake up card from idle and identify it
 *
 * @param Config       (t_ttc_sdcard_config*)   Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @return             (e_ttc_sdcard_errorcode) ==0: successfully woke up card from idlde; >0: error code
 */
e_ttc_sdcard_errorcode sdcard_common_wake_up( t_ttc_sdcard_config* Config );

/** Wait while card is busy.
 *
 * This will wait if one of the busy flags (Config->Flags.Busy*) is set.
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @return             (BOOL)                  ==TRUE: card is ready now; ==FALSE: card did not finish within timeout (Config->Init.Retries)
 */
BOOL sdcard_common_wait_busy( t_ttc_sdcard_config* Config );

/** prepare common part of sdcard implementation
 *
 * Note: This function should be called once in single tasking configuration before any
 *       other sdcard_common_*() function is called!
 */
void sdcard_common_prepare();

/** Write one or more data blocks from given buffer onto storage device
 *
 * Note: This function has matching signature to be referenced in t_ttc_filesystem_config.Init.Storage
 *
 * @param Config       (t_ttc_sdcard_config*)   Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param Buffer       (const t_u8*)            Storage in RAM big egnough for Config->Init.BlockSize * AmountBlocks
 * @param Address      (t_u32)                  Address of first block to read (will read addresses Address..Address+AmountBlocks-1
 * @param AmountBlocks (t_u8)                   Amount of consecutive data blocks to write
 * @return             (e_ttc_sdcard_errorcode) ==0: desired amount of data blocks was read successfully; >0: error code
 */
t_u8 sdcard_common_blocks_write( void* Config, const t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks );

/** Read one or more data blocks from storage device into given buffer
 *
 * Note: This function has matching signature to be referenced in t_ttc_filesystem_config.Init.Storage.blocks_read
 *
 * @param Config       (t_ttc_sdcard_config*)   Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param Buffer       (t_u8*)                  Storage in RAM big egnough for Config->Init.BlockSize * AmountBlocks
 * @param Address      (t_u32)                  Address of first block to read (will read addresses Address..Address+AmountBlocks-1
 * @param AmountBlocks (t_u8)                   Amount of consecutive data blocks to read
 * @return             (e_ttc_sdcard_errorcode) ==0: desired amount of data blocks was read successfully; >0: error code
 */
t_u8 sdcard_common_blocks_read( void* Config, t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks );

/** Write data from given buffer into single block onto sdcard
 *
 * If BufferSize is less than current block size, then rest of block will be filled with zeroes.
 * Note: Call ttc_sdcard_medium_mount() before calling this function!
 * Note: Will return error-code if sdcard has been removed meanwhile or is not mounted.
 *
 * @param Config       (t_ttc_sdcard_config*)   Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param BlockAddress (t_u32)                  Address of block on sdcard (0..ttc_sdcard_blocks_count()-1)
 * @param Buffer       (const t_u8*)            Memory buffer storing data to be written onto sdcard
 * @param BufferSize   (t_u16)                  Amount of valid bytes in Buffer (will send Buffer[0..BufferSize-1]
 * @return             (e_ttc_sdcard_errorcode) ==0: block was written successfully; >0: error code
 */
e_ttc_sdcard_errorcode sdcard_common_block_write( t_ttc_sdcard_config* Config, t_u32 Address, const t_u8* Buffer, t_u16 BufferSize );

/** Read data of single block from sdcard into given buffer
 *
 * Note: If BufferSize < ttc_sdcard_blocks_size() then remaining bytes in block will be ignored.
 * Note: Will call ttc_sdcard_medium_mount() if required.
 *
 * @param Config       (t_ttc_sdcard_config*)   Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param LogicalIndex (t_u8)                   Logical index of sdcard instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_SDCARD<n> lines in extensions.active/makefile
 * @param BlockAddress (t_u32)                  Address of block on sdcard (0..ttc_sdcard_blocks_count()-1)
 * @param Buffer       (t_u8*)                  Memory buffer where to store data
 * @param BufferSize   (t_u16)                  remaining amount of bytes available in Buffer[] (must bee >=512)
 * @return             (e_ttc_sdcard_errorcode) ==0: block was read into Buffer[] successfully; >0: error code
 */
e_ttc_sdcard_errorcode sdcard_common_block_read( t_ttc_sdcard_config* Config, t_u32 Address, t_u8* Buffer, t_u16 BufferSize );

/** Fast check to see if an sdcard has been inserted or removed in indexed slot
 *
 * This check should be implemented as fast as possible as it will probably
 * be called regular at a high frequency.
 * Note: This function has matching signature to be referenced in t_ttc_filesystem_config.Init.Storage
 *
 * Note: Low-level driver must only return codes E_ttc_storage_event_media_present or E_ttc_storage_event_no_card!
 *       All other event codes are handled by ttc_sdcard_medium_detect().
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @return             (e_ttc_storage_event)    ==0: no sdcard available; >0: see e_ttc_storage_event for details!
 */
e_ttc_storage_event sdcard_common_medium_detect( void* Config );

/** Powers up sdcard in slot and prepares it for usage
 *
 * Will do nothing, if no sdcard is detected.
 * Note: This function has matching signature to be referenced in t_ttc_filesystem_config.Init.Storage
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param BlockSize    (t_u16*)                !=NULL: storage where to write detected block size
 * @return             == 0: sdcard device has been mounted successfully and can be used; != 0: error-code
 */
t_u8 sdcard_common_medium_mount( void* Config, t_u16* BlockSize );

/** Powers down sdcard to be removed safely
 *
 * Will do nothing if sdcard is currently not mounted.
 * Note: This function has matching signature to be referenced in t_ttc_filesystem_config.Init.Storage
 *
 * @param Config       (t_ttc_sdcard_config*)  Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @return             == 0: sdcard device has been unmounted successfully; != 0: error-code (inform user not to remove card before unmounting it)
 */
t_u8 sdcard_common_medium_unmount( void* Config );

//InsertFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}Function prototypes

#endif //sdcard_common_h
