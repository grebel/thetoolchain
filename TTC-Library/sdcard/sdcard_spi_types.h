#ifndef SDCARD_SPI_TYPES_H
#define SDCARD_SPI_TYPES_H

/** { sdcard_spi_types.h ************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_sdcard.h providing
 *  Structures, Enums and Defines being required by ttc_sdcard_types.h
 *
 *  secure digital card storage connected via spi bus
 *
 *  Created from template device_architecture_types.h revision 27 at 20180130 09:59:56 UTC
 *
 *  Note: See ttc_sdcard.h for description of high-level sdcard implementation!
 *  Note: See sdcard_spi.h for description of spi specific sdcard implementation!
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Defines/ TypeDefs **********************************************************

// See sdcard_spi.h for a description of required constant defines!
#ifdef TTC_SDCARD1_SPI_LOGICAL_INDEX
    #ifndef TTC_SDCARD1_SPI_SLAVE_INDEX
        #    error Missing definition for TTC_SDCARD1_SPI_SLAVE_INDEX. Define as gpio pin being connected to slave select input of TTC_SDCARD1!
    #endif
#endif

#ifdef TTC_SDCARD2_SPI_LOGICAL_INDEX
    #ifndef TTC_SDCARD2_SPI_SLAVE_INDEX
        #    error Missing definition for TTC_SDCARD2_SPI_SLAVE_INDEX. Define as gpio pin being connected to slave select input of TTC_SDCARD2!
    #endif
#endif

#ifdef TTC_SDCARD3_SPI_LOGICAL_INDEX
    #ifndef TTC_SDCARD3_SPI_SLAVE_INDEX
        #    error Missing definition for TTC_SDCARD3_SPI_SLAVE_INDEX. Define as gpio pin being connected to slave select input of TTC_SDCARD3!
    #endif
#endif

#ifdef TTC_SDCARD4_SPI_LOGICAL_INDEX
    #ifndef TTC_SDCARD4_SPI_SLAVE_INDEX
        #    error Missing definition for TTC_SDCARD4_SPI_SLAVE_INDEX. Define as gpio pin being connected to slave select input of TTC_SDCARD4!
    #endif
#endif

#ifdef TTC_SDCARD5_SPI_LOGICAL_INDEX
    #ifndef TTC_SDCARD5_SPI_SLAVE_INDEX
        #    error Missing definition for TTC_SDCARD5_SPI_SLAVE_INDEX. Define as gpio pin being connected to slave select input of TTC_SDCARD5!
    #endif
#endif

#ifdef TTC_SDCARD6_SPI_LOGICAL_INDEX
    #ifndef TTC_SDCARD6_SPI_SLAVE_INDEX
        #    error Missing definition for TTC_SDCARD6_SPI_SLAVE_INDEX. Define as gpio pin being connected to slave select input of TTC_SDCARD6!
    #endif
#endif

#ifdef TTC_SDCARD7_SPI_LOGICAL_INDEX
    #ifndef TTC_SDCARD7_SPI_SLAVE_INDEX
        #    error Missing definition for TTC_SDCARD7_SPI_SLAVE_INDEX. Define as gpio pin being connected to slave select input of TTC_SDCARD7!
    #endif
#endif

#ifdef TTC_SDCARD8_SPI_LOGICAL_INDEX
    #ifndef TTC_SDCARD8_SPI_SLAVE_INDEX
        #    error Missing definition for TTC_SDCARD8_SPI_SLAVE_INDEX. Define as gpio pin being connected to slave select input of TTC_SDCARD8!
    #endif
#endif

#ifdef TTC_SDCARD9_SPI_LOGICAL_INDEX
    #ifndef TTC_SDCARD9_SPI_SLAVE_INDEX
        #    error Missing definition for TTC_SDCARD9_SPI_SLAVE_INDEX. Define as gpio pin being connected to slave select input of TTC_SDCARD9!
    #endif
#endif

#ifdef TTC_SDCARD10_SPI_LOGICAL_INDEX
    #ifndef TTC_SDCARD10_SPI_SLAVE_INDEX
        #    error Missing definition for TTC_SDCARD10_SPI_SLAVE_INDEX. Define as gpio pin being connected to slave select input of TTC_SDCARD10!
    #endif
#endif
//InsertDefines above (DO NOT REMOVE THIS LINE!)

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Includes *******************************************************************

#ifndef TTC_SDCARD_TYPES_H
    #  error Do not include sdcard_common_types.h directly. Include ttc_sdcard_types.h instead!
#endif

#include "../ttc_basic_types.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Structures/ Enums required by ttc_sdcard_types.h *************************

typedef struct {  // spi specific configuration data of single sdcard device
    t_u8              Index_SPI;       // logical index of ttc_spi device used to connect to sdcard
    t_u8              Index_Slave;     // logical index of slave select line on spi bus
    t_u32             HighSpeed;       // spi bus speed to use for high speed transfers (set during sdcard_spi_init() )
} __attribute__( ( __packed__ ) ) t_sdcard_spi_config;

// t_ttc_sdcard_architecture is required by ttc_sdcard_types.h
#define t_ttc_sdcard_architecture t_sdcard_spi_config

//} Structures/ Enums

#endif //SDCARD_SPI_TYPES_H
