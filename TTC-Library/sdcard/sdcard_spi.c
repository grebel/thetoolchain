/** sdcard_spi.c ***************************************{
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Low-level driver for ttc_sdcard.c.
 *
 *  secure digital card storage connected via spi bus
 *
 *  Created from template device_architecture.c revision 38 at 20180130 09:59:56 UTC
 *
 *  Note: See ttc_sdcard.h for description of high-level sdcard implementation.
 *  Note: See sdcard_spi.h for description of spi sdcard implementation.
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * C-Sources include their corresponding header file and all other header files
 * that are required to compile this source file.
 * Includes that provide datatypes being used by function prototypes belong into
 * "sdcard_spi.h".
 */

#include "sdcard_spi.h"
#include "../ttc_heap.h"              // dynamic memory allocation (e.g. malloc() )
#include "../ttc_memory.h"            // basic memory checks
#include "../ttc_gpio.h"
#include "../ttc_spi.h"
#include "../ttc_sdcard.h"
#include "../ttc_systick.h"
#include "../ttc_task.h"            // stack checking (provides TTC_TASK_RETURN() macro), critical sections
#include "sdcard_common.h"          // generic functions shared by low-level drivers of all architectures 
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_ttc_sdcard_features sdcard_spi_Features = {
    /** example features
      .MinBitRate       = 4800,
      .MaxBitRate       = 230400,
    */
    // set more feature values...

    .unused = 0
};
const t_u32                 sdcard_spi_TimeOut_us = 500000; // timeout while waiting for sdcard (microseconds)
//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

/** wait until card sends 0xff
 *
 * Note: This function is intended to be called inside sdcard_spi.c only!
 * Note: This function does not handle chip select. Activate chip select before and disable it afterwards!
 * Note: Card response is stored in Config->Card.LastResponse.
 *
 * @param Config           (t_ttc_sdcard_config*)            Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @return                 (BOOL)                            ==TRUE: card now returns 0xff (-> Config->Card.LastResponse.Byte); ==FALSE: card did not answer within timeout
 */
BOOL _sdcard_spi_wait_until_0xff( t_ttc_sdcard_config* Config );

/** wait while card sends 0xff
 *
 * Note: This function is intended to be called inside sdcard_spi.c only!
 * Note: This function does not handle chip select. Activate chip select before and disable it afterwards!
 * Note: Card response is stored in Config->Card.LastResponse.
 *
 * @param Config           (t_ttc_sdcard_config*)            Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @return                 (BOOL)                            ==TRUE: card stopped returning 0xff (-> Config->Card.LastResponse.Byte); ==FALSE: card did not answer within timeout
 */
BOOL _sdcard_spi_wait_while_0xff( t_ttc_sdcard_config* Config );

/** wait while card returns buys (bit 7 set in response
 *
 * Note: This function is intended to be called inside sdcard_spi.c only!
 * Note: This function does not handle chip select. Activate chip select before and disable it afterwards!
 * Note: Card response is stored in Config->Card.LastResponse.
 *
 * @param Config           (t_ttc_sdcard_config*)            Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @return                 (BOOL)                            ==TRUE: card stopped returning 0xff (-> Config->Card.LastResponse.Byte); ==FALSE: card did not answer within timeout
 */
BOOL  _sdcard_spi_wait_while_busy( t_ttc_sdcard_config* Config );

/** Send one commands and its argument to sdcard without touching chip-select line
 *
 * Note: This function is intended to be called inside sdcard_spi.c only!
 * Note: Card response is stored in Config->Card.LastResponse.
 *
 * @param Config           (t_ttc_sdcard_config*)            Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param CommandArgument  (t_ttc_sdcard_command_argument*)  Pointer to a set of command number and its argument (-> sdcard_common_prepare_command() )
 * @param Buffer           (t_u8*)                           Storage where to store return values
 * @param Amount           (t_u8)                            Amount of bytes to read after sending command + argument
 * @return                 Config->Card.LastResponse.Byte    ==0xff: no response from card; !=0xff: response code from card
 */
void _sdcard_spi_command_r1( t_ttc_sdcard_config* Config, const t_ttc_sdcard_command_argument* CommandArgument );

/** Send one commands and its argument to sdcard + read given amount of return values
 *
 * The use of a struct pointer allows to reduce ram usage and data copy operations.
 *
 * @param Config           (t_ttc_sdcard_config*)            Configuration of sdcard device as returned by ttc_sdcard_get_configuration()
 * @param CommandArgument  (t_ttc_sdcard_command_argument*)  Pointer to a set of command number and its argument (-> sdcard_common_prepare_command() )
 * @param Buffer           (t_u8*)                           Storage where to store return values
 * @param Amount           (t_u16)                           Amount of bytes to read after sending command + argument
 * @return                 (t_u16)                           Amount of bytes being read
 */
t_u16 _sdcard_spi_command_rn( t_ttc_sdcard_config* Config, const t_ttc_sdcard_command_argument* CommandArgument, t_u8* Buffer, t_u16 Amount );

/** Send given buffer to sdcard via SPI bus (thread safe)
 *
 * This is just a wrapper function that ensures that no task switches occur during spi bus operation.
 *
 * see description of ttc_spi_master_send_read() for all arguments!
 */
e_ttc_spi_errorcode _sdcard_spi_master_send( t_u8 IndexSPI, t_u8 IndexSlave, const t_u8* BufferTx, t_u16 AmountTx );

/** Read given amount of bytes from sdcard via SPI bus (thread safe)
 *
 * This is just a wrapper function that ensures that no task switches occur during spi bus operation.
 *
 * see description of ttc_spi_master_send_read() for all arguments!
 */
t_u16 _sdcard_spi_master_read( t_u8 IndexSPI, t_u8 IndexSlave, t_u8* BufferRx, t_u16 AmountRx );

/** Read + write given amount of bytes to and from sdcard via SPI bus (thread safe)
 *
 * This is just a wrapper function that ensures that no task switches occur during spi bus operation.
 *
 * see description of ttc_spi_master_send_read() for all arguments!
 */
e_ttc_spi_errorcode _sdcard_spi_master_send_read( t_u8 IndexSPI, t_u8 IndexSlave, const t_u8* BufferTx, t_u16 AmountTx, t_u16 AmountZerosTx, t_u8* BufferRx, t_u16 AmountRx );

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** Function Definitions ********************************************{
 */

e_ttc_sdcard_errorcode   sdcard_spi_block_read( t_ttc_sdcard_config* Config, t_u32 BlockAddress, t_u8* Buffer, t_u16 BufferSize ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_EXTRA_Writable( Buffer, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    e_ttc_sdcard_errorcode Error = 0;

    t_u8 IndexSPI   = Config->LowLevelConfig->spi.Index_SPI;
    t_u8 IndexSlave = Config->LowLevelConfig->spi.Index_Slave;
    t_u16 CRC16_Local  = 0; // locally computed crc16
    t_u16 CRC16_SDCARD = 0; // crc16 received from sdcard

    // hold chip select low during whole operation
    ttc_gpio_clr( ttc_spi_get_pin_slave_select( IndexSPI, IndexSlave ) );

    if ( BufferSize > Config->Card.BlockSize )
    { BufferSize = Config->Card.BlockSize; }

    t_u32 Address = ( Config->Flags.CardBlockAddressing ) ?
                    BlockAddress :                          // high-capacity card: using block address
                    BlockAddress * Config->Card.BlockSize;  // low-capacity card: calculating memory address

    // issue block read command
    t_ttc_sdcard_command_argument CommandArgument;
    sdcard_common_prepare_command( Config, &CommandArgument, E_ttc_sdcard_cmd17_block_read_single, Address );
    _sdcard_spi_command_r1( Config, &CommandArgument );
    if ( Config->Card.LastResponse.Byte )
    { Error = E_ttc_sdcard_errorcode_card_not_answering; }
    if ( !Error ) { // read single block start token
        _sdcard_spi_wait_while_0xff( Config );
        t_u8 Token = Config->Card.LastResponse.Byte;
        if ( Token != sdcard_common_token_StartSingleBlock ) // got wrong start token: abort
        { Error = E_ttc_sdcard_errorcode_card_reading; }
    }
    if ( !Error ) { // read complete block into buffer
        t_u16 AmountRead = _sdcard_spi_master_read( IndexSPI,
                                                    0, // no chipselect handling by ttc_spi
                                                    Buffer,
                                                    BufferSize
                                                  );

        if ( AmountRead != BufferSize )
        { Error = E_ttc_sdcard_errorcode_card_reading; }
    }
    if ( !Error ) { // check CRC
        t_u16 RemainingBytes = Config->Card.BlockSize - BufferSize;
        if ( RemainingBytes ) {         // ignore remaining bytes
#define SSBR_BLOCK_SIZE 16
            t_u8 Ignore[SSBR_BLOCK_SIZE];

            if ( !Config->Flags.CardDisabledCRC ) { // ignore remaining bytes but update crc to check integrity
                CRC16_Local = sdcard_common_crc7_calculate( Buffer, BufferSize, CRC16_Local );

                while ( RemainingBytes >= SSBR_BLOCK_SIZE ) { // ignore remaining bytes (several bytes at a time)
                    _sdcard_spi_master_read( IndexSPI, 0, ( t_u8* ) &Ignore, SSBR_BLOCK_SIZE );
                    CRC16_Local = sdcard_common_crc7_calculate( ( t_u8* ) &Ignore, SSBR_BLOCK_SIZE, CRC16_Local );
                    RemainingBytes -= SSBR_BLOCK_SIZE;
                }
                if ( BufferSize < Config->Card.BlockSize ) {         // ignore remaining bytes (1 byte at a time)
                    RemainingBytes = Config->Card.BlockSize - BufferSize;
                    _sdcard_spi_master_read( IndexSPI, 0, ( t_u8* ) &Ignore, RemainingBytes );
                    CRC16_Local = sdcard_common_crc7_calculate( ( t_u8* ) &Ignore, RemainingBytes, CRC16_Local );
                    RemainingBytes = 0;
                }
            }
            else {                               // ignore remaining bytes without updating crc
                while ( RemainingBytes >= SSBR_BLOCK_SIZE ) { // ignore remaining bytes (several bytes at a time)
                    _sdcard_spi_master_read( IndexSPI, 0, ( t_u8* ) &Ignore, SSBR_BLOCK_SIZE );
                    RemainingBytes -= SSBR_BLOCK_SIZE;
                }
                if ( RemainingBytes ) {         // ignore remaining bytes (1 byte at a time)
                    _sdcard_spi_master_read( IndexSPI, 0, ( t_u8* ) &Ignore, RemainingBytes );
                    RemainingBytes = 0;
                }
            }
        }
    }
    if ( !Error ) { // read CRC
        _sdcard_spi_master_read( IndexSPI, 0, ( t_u8* ) &CRC16_SDCARD, 2 );
    }

    // release chip select line
    ttc_gpio_set( ttc_spi_get_pin_slave_select( IndexSPI, IndexSlave ) );

    if ( !Config->Flags.CardDisabledCRC ) { // calculate local crc16 and compare to received one
        if ( CRC16_Local != CRC16_SDCARD )
        { Error = E_ttc_sdcard_errorcode_crc_reading; }
    }

    return Error;
}
e_ttc_sdcard_errorcode   sdcard_spi_block_write( t_ttc_sdcard_config* Config, t_u32 BlockAddress, const t_u8* Buffer, t_u16 BufferSize ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_EXTRA_Readable( ( void* ) Buffer, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    VOLATILE_SDCARD e_ttc_sdcard_errorcode Error    = 0;
    VOLATILE_SDCARD e_ttc_spi_errorcode    ErrorSPI = 0;

    t_u8 IndexSPI   = Config->LowLevelConfig->spi.Index_SPI;
    t_u8 IndexSlave = Config->LowLevelConfig->spi.Index_Slave;
    t_u16 CRC16 = 0;

    // hold chip select low during whole operation
    ttc_gpio_clr( ttc_spi_get_pin_slave_select( IndexSPI, IndexSlave ) );

    t_u32 Address = ( Config->Flags.CardBlockAddressing ) ?
                    BlockAddress :                          // high-capacity card: using block address
                    BlockAddress * Config->Card.BlockSize;  // low-capacity card: calculating memory address


    t_ttc_sdcard_command_argument CommandArgument;

    sdcard_common_prepare_command( Config,
                                   &CommandArgument,
                                   E_ttc_sdcard_cmd24_block_write_single,
                                   Address
                                   //X WORKAROUND: For strange reason we have to send this in little endian ( sdcard_common_prepare_command() will convert it again )
                                 );

    _sdcard_spi_command_r1( Config, &CommandArgument );
    if ( Config->Card.LastResponse.Byte )
    { Error = E_ttc_sdcard_errorcode_card_writing; } // error returned after issuing write command: abort
    if ( !Error ) { // send start token
        ErrorSPI = _sdcard_spi_master_send( IndexSPI, 0, &sdcard_common_token_StartSingleBlock, 1 );
        if ( ErrorSPI ) {
            Error = E_ttc_sdcard_errorcode_card_writing;
        }
    }
    if ( !Error ) { // send data from buffer
        ErrorSPI = _sdcard_spi_master_send( IndexSPI, 0, Buffer, BufferSize );
        if ( ErrorSPI ) {
            Error = E_ttc_sdcard_errorcode_card_writing;
        }
    }
    if ( !Error ) { // maybe we have to fill up to 512 bytes
        if ( Config->Flags.CardIsHighCapacity && ( BufferSize < Config->Card.BlockSize ) ) { // every block is 512 bytes: fill will zeroes
            _sdcard_spi_master_send_read( IndexSPI, 0,
                                          NULL, 0,
                                          Config->Card.BlockSize - BufferSize, // just send out zeroes
                                          NULL, 0
                                        );
            if ( !Config->Flags.CardDisabledCRC ) { // calculate crc16
#warning ToDo: update crc16
            }
        }
    }
    if ( !Error ) { // send out crc16
        if ( !Config->Flags.CardDisabledCRC ) { // calculate crc16
            //CRC16 =
#warning ToDo: update crc16
        }
        ErrorSPI = _sdcard_spi_master_send( IndexSPI, 0, ( t_u8* ) &CRC16, 2 );
        if ( ErrorSPI ) {
            Error = E_ttc_sdcard_errorcode_card_writing;
        }
    }
    if ( !Error ) { // Check data response to write command
        VOLATILE_SDCARD t_ttc_sdcard_data_response_token DataResponse;
        _sdcard_spi_master_read( IndexSPI, 0, ( t_u8* ) &DataResponse, 1 );
        if ( DataResponse.Status != 0b010 )
        { Error = E_ttc_sdcard_errorcode_card_writing; }
        if ( DataResponse.Status == 0b101 )
        { Error = E_ttc_sdcard_errorcode_card_writing_crc; }
        if ( DataResponse.Status == 0b110 )
        { Error = E_ttc_sdcard_errorcode_card_writing_write; }
    }
    if ( !Error ) { // read first response
        if ( _sdcard_spi_wait_while_busy( Config ) ) { // card is ready now: read response
            if ( Config->Card.LastResponse.Byte )
            { Error = E_ttc_sdcard_errorcode_card_writing; } // error occured while writing to card
        }
    }
    if ( !Error ) { // wait until card has finished writing block
        Config->Flags.CardBusyWriting = 1;
    }

    // release chip select line
    ttc_gpio_set( ttc_spi_get_pin_slave_select( IndexSPI, IndexSlave ) );

    return Error;
}
void                     sdcard_spi_configuration_check( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

}
void                     sdcard_spi_command_r1( t_ttc_sdcard_config* Config, const t_ttc_sdcard_command_argument* CommandArgument ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_EXTRA_Readable( CommandArgument, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    t_u8 IndexSPI   = Config->LowLevelConfig->spi.Index_SPI;
    t_u8 IndexSlave = Config->LowLevelConfig->spi.Index_Slave;

    // hold chip select low during whole operation
    ttc_gpio_clr( ttc_spi_get_pin_slave_select( IndexSPI, IndexSlave ) );

    _sdcard_spi_command_r1( Config, CommandArgument );

    // release chip select line
    ttc_gpio_set( ttc_spi_get_pin_slave_select( IndexSPI, IndexSlave ) );
}
t_u16                    sdcard_spi_command_rn( t_ttc_sdcard_config* Config, const t_ttc_sdcard_command_argument* CommandArgument, t_u8* Buffer, t_u16 Amount ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto );          // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_EXTRA_Readable( CommandArgument, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_EXTRA_Writable( Buffer, ttc_assert_origin_auto );          // check if it points to RAM (change for read only memory!)

    t_u8 IndexSPI   = Config->LowLevelConfig->spi.Index_SPI;
    t_u8 IndexSlave = Config->LowLevelConfig->spi.Index_Slave;

    // hold chip select low during whole operation
    ttc_gpio_clr( ttc_spi_get_pin_slave_select( IndexSPI, IndexSlave ) );

    t_u8 AmountRead = _sdcard_spi_command_rn( Config, CommandArgument, Buffer, Amount );

    // release chip select line
    ttc_gpio_set( ttc_spi_get_pin_slave_select( IndexSPI, IndexSlave ) );

    TTC_TASK_RETURN( AmountRead );   // will perform stack overflow check and return given value
}
e_ttc_sdcard_errorcode   sdcard_spi_deinit( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    ttc_spi_deinit( TTC_SDCARD1_SPI_LOGICAL_INDEX );
    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
e_ttc_sdcard_errorcode   sdcard_spi_init( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    // ensure that our spi interface already has been initialized
    t_u8 IndexSPI   = Config->LowLevelConfig->spi.Index_SPI;
    t_u8 IndexSlave = Config->LowLevelConfig->spi.Index_Slave;

    t_ttc_spi_config* ConfigSPI = ttc_spi_get_configuration( IndexSPI );
    Assert_SPI( ConfigSPI->Flags.Initialized, ttc_assert_origin_auto ); // spi bus must be initialized before calling ttc_sdcard_init(). Call ttc_spi_init() for IndexSPI before!

    // ensure correct SPI bus configuration for sdcard use
    ConfigSPI->Init.Flags.ClockIdleHigh     = 1;
    ConfigSPI->Init.Flags.ClockPhase2ndEdge = 1;
    ConfigSPI->Init.SendAsZero              = 0xff; // must send 0xff to sdcard when waiting for an answer

    // We will have to lower SPI speed during card initialization and then return to this value.
    Config->LowLevelConfig->spi.HighSpeed = ConfigSPI->Init.BaudRate;

    // reinitialize spi unit with new settings
    ttc_spi_init( IndexSPI );

    // will use chip select line to detect if sdcard is inserted or not
    ttc_gpio_init( ttc_spi_get_pin_slave_select( IndexSPI, IndexSlave ), E_ttc_gpio_mode_output_open_drain, E_ttc_gpio_speed_min );

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
e_ttc_sdcard_errorcode   sdcard_spi_load_defaults( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointers!

    // Note: high-level driver has zeroed *Config
    Config->Architecture = E_ttc_sdcard_architecture_spi;         // set type of architecture
    Config->Features     = &sdcard_spi_Features;  // assign feature set

    if ( Config->LowLevelConfig == NULL ) { // allocate memory for spi specific configuration
        Config->LowLevelConfig = ttc_heap_alloc_zeroed( sizeof( t_ttc_sdcard_architecture ) );
    }

    // reset all flags
    ttc_memory_set( &( Config->Flags ), 0, sizeof( Config->Flags ) );

    if ( 0 ) { Config->Init.Flags.Support_HighCapacity = 1; } TODO( "Add support for high-capacity cards" );

    t_u8 Index_SPI   = 0;
    t_u8 Index_Slave = 0;
    switch ( Config->LogicalIndex ) { // extract configuration data for ttc_spi device
#ifdef TTC_SDCARD1_SPI_LOGICAL_INDEX
        case 1: {
            Index_SPI   = TTC_SDCARD1_SPI_LOGICAL_INDEX;
            Index_Slave = TTC_SDCARD1_SPI_SLAVE_INDEX;
            break;
        }
#endif
#ifdef TTC_SDCARD2_SPI_LOGICAL_INDEX
        case 2: {
            Index_SPI   = TTC_SDCARD2_SPI_LOGICAL_INDEX;
            Index_Slave = TTC_SDCARD2_SPI_SLAVE_INDEX;
            break;
        }
#endif
#ifdef TTC_SDCARD3_SPI_LOGICAL_INDEX
        case 3: {
            Index_SPI   = TTC_SDCARD3_SPI_LOGICAL_INDEX;
            Index_Slave = TTC_SDCARD3_SPI_SLAVE_INDEX;
            break;
        }
#endif
#ifdef TTC_SDCARD4_SPI_LOGICAL_INDEX
        case 4: {
            Index_SPI   = TTC_SDCARD4_SPI_LOGICAL_INDEX;
            Index_Slave = TTC_SDCARD4_SPI_SLAVE_INDEX;
            break;
        }
#endif
#ifdef TTC_SDCARD5_SPI_LOGICAL_INDEX
        case 5: {
            Index_SPI   = TTC_SDCARD5_SPI_LOGICAL_INDEX;
            Index_Slave = TTC_SDCARD5_SPI_SLAVE_INDEX;
            break;
        }
#endif
#ifdef TTC_SDCARD6_SPI_LOGICAL_INDEX
        case 6: {
            Index_SPI   = TTC_SDCARD6_SPI_LOGICAL_INDEX;
            Index_Slave = TTC_SDCARD6_SPI_SLAVE_INDEX;
            break;
        }
#endif
#ifdef TTC_SDCARD7_SPI_LOGICAL_INDEX
        case 7: {
            Index_SPI   = TTC_SDCARD7_SPI_LOGICAL_INDEX;
            Index_Slave = TTC_SDCARD7_SPI_SLAVE_INDEX;
            break;
        }
#endif
#ifdef TTC_SDCARD8_SPI_LOGICAL_INDEX
        case 8: {
            Index_SPI   = TTC_SDCARD8_SPI_LOGICAL_INDEX;
            Index_Slave = TTC_SDCARD8_SPI_SLAVE_INDEX;
            break;
        }
#endif
#ifdef TTC_SDCARD9_SPI_LOGICAL_INDEX
        case 9: {
            Index_SPI   = TTC_SDCARD9_SPI_LOGICAL_INDEX;
            Index_Slave = TTC_SDCARD9_SPI_SLAVE_INDEX;
            break;
        }
#endif
#ifdef TTC_SDCARD10_SPI_LOGICAL_INDEX
        case 10: {
            Index_SPI   = TTC_SDCARD10_SPI_LOGICAL_INDEX;
            Index_Slave = TTC_SDCARD10_SPI_SLAVE_INDEX;
            break;
        }
#endif
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // unknown/ invalid logical index given. Check TTC_SDCARD* defines in your makefile!
    }
    Config->LowLevelConfig->spi.Index_SPI   = Index_SPI;
    Config->LowLevelConfig->spi.Index_Slave = Index_Slave;

    TTC_TASK_RETURN( 0 ); // will perform stack overflow check and return given value
}
e_ttc_storage_event      sdcard_spi_medium_detect( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    t_u8 IndexSPI   = Config->LowLevelConfig->spi.Index_SPI;
    t_u8 IndexSlave = Config->LowLevelConfig->spi.Index_Slave;

    /* Check chip select line to detect if sdcard has been inserted.
     * - GPIO has been configured as output open drain
     * - Inserted SDCARD will pull up chip select line
     */
    BOOL CardPresent = ttc_gpio_get( ttc_spi_get_pin_slave_select( IndexSPI, IndexSlave ) );

    return ( CardPresent ) ? E_ttc_storage_event_media_present : E_ttc_storage_event_media_missing;
}
e_ttc_sdcard_errorcode   sdcard_spi_medium_go_idle( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_EXTRA( Config->Flags.CardMounted == 0, ttc_assert_origin_auto ); // this case should have been handled in ttc_sdcard_medium_mount()!
    Assert_SDCARD_EXTRA( Config->Flags.CardPresent == 1, ttc_assert_origin_auto ); // this case should have been handled in ttc_sdcard_medium_mount()!
    e_ttc_sdcard_errorcode Error = E_ttc_sdcard_errorcode_card_not_answering;

    sdcard_spi_set_speed( Config, E_ttc_sdcard_speed_minimum );

    t_u8 IndexSPI   = Config->LowLevelConfig->spi.Index_SPI;
    //X t_u8 IndexSlave = Config->LowLevelConfig->spi.Index_Slave;

    static const t_u8 FF = 0xff;
    for ( t_u8 Times = 16; Times > 0; --Times ) // send some clocks while chip-select is inactive to wake up sdcard
    { _sdcard_spi_master_send( IndexSPI, 0, &FF, 1 ); }

    BOOL CardDisabledCRC = Config->Flags.CardDisabledCRC;
    Config->Flags.CardDisabledCRC = 0; // crc must be enabled during wakeup
    sdcard_common_command_r1( Config, E_ttc_sdcard_cmd00_go_idle_state, 0 );
    if ( Config->Card.LastResponse.Byte == 0x01 )
    { Error = E_ttc_sdcard_errorcode_OK; }

    Config->Flags.CardDisabledCRC = CardDisabledCRC;
    TTC_TASK_RETURN( Error );
}
void                     sdcard_spi_medium_unmount( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_EXTRA( Config->Flags.CardMounted, ttc_assert_origin_auto ); // this condition should have been handled by ttc_sdcard_medium_unmount()!

    Config->Flags.CardMounted = 0;
    if ( ! sdcard_spi_medium_detect( Config ) ) { // someone stole our card: Cleanup structures + exit
        // maybe we have to do something here in the future?
        return;
    }
    if ( 1 ) { // Brave user left card in slot: Clean up structures and flush write buffers (if any)
        // maybe we have to do something here in the future?
    }
}
void                     sdcard_spi_prepare() {



}
void                     sdcard_spi_reset( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    ttc_spi_reset( Config->LowLevelConfig->spi.Index_SPI );

    // will use chip select line to detect if sdcard is inserted or not
    t_u8 IndexSPI   = Config->LowLevelConfig->spi.Index_SPI;
    t_u8 IndexSlave = Config->LowLevelConfig->spi.Index_Slave;
    ttc_gpio_init( ttc_spi_get_pin_slave_select( IndexSPI, IndexSlave ), E_ttc_gpio_mode_output_open_drain, E_ttc_gpio_speed_min );
}
e_ttc_sdcard_errorcode   sdcard_spi_set_speed( t_ttc_sdcard_config* Config, e_ttc_sdcard_speed InterfaceSpeed ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_EXTRA( InterfaceSpeed < E_ttc_sdcard_speed_unknown, ttc_assert_origin_auto ); // invalid argument given. Check implementation of caller!

    t_u8 IndexSPI   = Config->LowLevelConfig->spi.Index_SPI;
    t_u8 IndexSlave = Config->LowLevelConfig->spi.Index_Slave;
    e_ttc_gpio_pin PinSlaveSelect = ttc_spi_get_pin_slave_select( IndexSPI, IndexSlave );

    if ( InterfaceSpeed == E_ttc_sdcard_speed_minimum ) { // lower spi bus clock <400kHz
        /* First sdcard initialization steps must be down with SPI clock <400kHz
         * This is, why we must implement this inside low-level driver and cannot move it up into high-level driver.
         * Source: http://nerdclub-uk.blogspot.de/2012/11/how-spi-works-with-sd-card.html
         */
        t_ttc_spi_config* ConfigSPI = ttc_spi_get_configuration( IndexSPI );
        ConfigSPI->Init.BaudRate = 200000;
        ttc_spi_init( IndexSPI );

        // will use chip select line to detect if sdcard is inserted or not
        ttc_gpio_init( PinSlaveSelect, E_ttc_gpio_mode_output_open_drain, E_ttc_gpio_speed_min );
        ttc_systick_delay_simple( 1000 );

        Config->InterfaceSpeed = E_ttc_sdcard_speed_minimum;
    }
    else {
        t_ttc_spi_config* ConfigSPI = ttc_spi_get_configuration( IndexSPI );
        ConfigSPI->Init.BaudRate = Config->LowLevelConfig->spi.HighSpeed;
        ttc_spi_init( IndexSPI );

        // reconfigure chip select line to detect if sdcard is inserted or not
        ttc_gpio_init( PinSlaveSelect, E_ttc_gpio_mode_output_open_drain, E_ttc_gpio_speed_min );

        Config->InterfaceSpeed = E_ttc_sdcard_speed_normal;
    }
    ttc_gpio_set( PinSlaveSelect ); // ensure that chip select line is released (pulled high by sdcard)

    TTC_TASK_RETURN( 0 );   // will perform stack overflow check and return given value
}
BOOL                     sdcard_spi_wait_busy( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    BOOL CardIsAvailable = TRUE;

    if ( Config->Flags.CardBusyWriting ) {
        if ( _sdcard_spi_wait_until_0xff( Config ) ) {
            Config->Flags.CardBusyWriting = 0;
            CardIsAvailable = TRUE;
        }
        else
        { CardIsAvailable = FALSE; }
    }

    return CardIsAvailable;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

BOOL                _sdcard_spi_wait_until_0xff( t_ttc_sdcard_config* Config ) {
    t_u8 IndexSPI = Config->LowLevelConfig->spi.Index_SPI;

    _sdcard_spi_master_read( IndexSPI,
                             0, // no chipselect handling by ttc_spi
                             & ( Config->Card.LastResponse.Byte ),
                             1
                           );

    t_u8 Retries  = Config->Init.Retries;
    while ( ( Config->Card.LastResponse.Byte != 0xff ) && ( Retries ) ) { // wait until sdcard is ready (or absent)
        ttc_systick_delay_simple( 1000 );

        _sdcard_spi_master_read( IndexSPI,
                                 0, // no chipselect handling by ttc_spi
                                 & ( Config->Card.LastResponse.Byte ),
                                 1
                               );
        Retries--;
    }

    return ( Retries ) ? TRUE : FALSE;
}
BOOL                _sdcard_spi_wait_while_0xff( t_ttc_sdcard_config* Config ) {
    t_u8 IndexSPI = Config->LowLevelConfig->spi.Index_SPI;

    // GDB> p ttc_spi_master_read( IndexSPI, 0, & ( Config->Card.LastResponse.Byte ), 1)

    _sdcard_spi_master_read( IndexSPI,
                             0, // no chipselect handling by ttc_spi
                             & ( Config->Card.LastResponse.Byte ),
                             1
                           );

    t_u8 Retries  = Config->Init.Retries;
    while ( ( Config->Card.LastResponse.Byte == 0xff ) && ( Retries ) ) { // wait until sdcard is ready (or absent)
        ttc_systick_delay_simple( 1000 );

        _sdcard_spi_master_read( IndexSPI,
                                 0, // no chipselect handling by ttc_spi
                                 & ( Config->Card.LastResponse.Byte ),
                                 1
                               );
        Retries--;
    }

    return ( Retries ) ? TRUE : FALSE;
}
BOOL                _sdcard_spi_wait_while_busy( t_ttc_sdcard_config* Config ) {
    t_u8 IndexSPI = Config->LowLevelConfig->spi.Index_SPI;

    // GDB> p ttc_spi_master_read( IndexSPI, 0, & ( Config->Card.LastResponse.Byte ), 1)

    _sdcard_spi_master_read( IndexSPI,
                             0, // no chipselect handling by ttc_spi
                             & ( Config->Card.LastResponse.Byte ),
                             1
                           );

    t_u8 Retries  = Config->Init.Retries;
    while ( ( Config->Card.LastResponse.Byte & 0x80 ) && ( Retries ) ) { // wait until sdcard is ready (or absent)
        ttc_systick_delay_simple( 1000 );

        _sdcard_spi_master_read( IndexSPI,
                                 0, // no chipselect handling by ttc_spi
                                 & ( Config->Card.LastResponse.Byte ),
                                 1
                               );
        Retries--;
    }

    return ( Retries ) ? TRUE : FALSE;
}
void                _sdcard_spi_command_r1( t_ttc_sdcard_config* Config, const t_ttc_sdcard_command_argument* CommandArgument ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_EXTRA_Readable( CommandArgument, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    t_u8 IndexSPI  = Config->LowLevelConfig->spi.Index_SPI;

    if ( _sdcard_spi_wait_until_0xff( Config ) ) { // card is ready now: send command
        _sdcard_spi_master_send( IndexSPI,
                                 0, // no chipselect handling by ttc_spi
                                 ( t_u8* ) CommandArgument,
                                 sizeof( *CommandArgument )
                               );
        _sdcard_spi_wait_while_busy( Config );
    }
}
t_u16               _sdcard_spi_command_rn( t_ttc_sdcard_config* Config, const t_ttc_sdcard_command_argument* CommandArgument, t_u8* Buffer, t_u16 Amount ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto );          // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_EXTRA_Readable( CommandArgument, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    Assert_SDCARD_EXTRA_Writable( Buffer, ttc_assert_origin_auto );          // check if it points to RAM (change for read only memory!)

    t_u8 IndexSPI   = Config->LowLevelConfig->spi.Index_SPI;
    _sdcard_spi_command_r1( Config, CommandArgument );

    t_u8 AmountRead = 0;
    if ( Config->Card.LastResponse.Byte != 0xff ) { // response reported no error: read in more data

        if ( 1 ) {
            AmountRead += _sdcard_spi_master_read( IndexSPI,
                                                   0, // no chipselect handling by ttc_spi
                                                   Buffer,
                                                   Amount
                                                 );
        }
        else { // test: use LastResponse as first byte of Buffer[]
            *Buffer = Config->Card.LastResponse.Byte; // response is first byte of requested data buffer
            AmountRead = 1;

            if ( AmountRead < Amount ) { // read remaining bytes
                AmountRead += _sdcard_spi_master_read( IndexSPI,
                                                       0, // no chipselect handling by ttc_spi
                                                       Buffer + 1,
                                                       Amount - 1
                                                     );
            }
        }
    }

    TTC_TASK_RETURN( AmountRead );   // will perform stack overflow check and return given value
}
e_ttc_spi_errorcode _sdcard_spi_master_send( t_u8 IndexSPI, t_u8 IndexSlave, const t_u8* BufferTx, t_u16 AmountTx ) {
    e_ttc_spi_errorcode Result = 0;

    ttc_task_critical_begin();
    Result = ttc_spi_master_send( IndexSPI, IndexSlave, BufferTx, AmountTx );
    ttc_task_critical_end();

    return Result; // e_ttc_spi_errorcode
}
t_u16               _sdcard_spi_master_read( t_u8 IndexSPI, t_u8 IndexSlave, t_u8* BufferRx, t_u16 AmountRx ) {
    t_u16 Result = 0;

    ttc_task_critical_begin();
    Result = ttc_spi_master_read( IndexSPI, IndexSlave, BufferRx, AmountRx );
    ttc_task_critical_end();

    return Result; // t_u16
}
e_ttc_spi_errorcode _sdcard_spi_master_send_read( t_u8 IndexSPI, t_u8 IndexSlave, const t_u8* BufferTx, t_u16 AmountTx, t_u16 AmountZerosTx, t_u8* BufferRx, t_u16 AmountRx ) {
    e_ttc_spi_errorcode Result = 0;

    ttc_task_critical_begin();
    Result = ttc_spi_master_send_read( IndexSPI, IndexSlave, BufferTx, AmountTx, AmountZerosTx, BufferRx, AmountRx );
    ttc_task_critical_end();

    return Result; // e_ttc_spi_errorcode
}
//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
