/** sdcard_common.c ***********************************************{
 *
 *                          The ToolChain
 *
 *  Common source code available to sdcard low-level drivers of all architectures.
 *
 *  Created from template ttc-lib/templates/device_common.c revision 12 at 20180130 09:59:56 UTC
 *
 *  Authors: Gregor Rebel
}*/

/** Includes ********************************************************{
 *
 * Put all includes here that are required to compile this source code.
 */

#include "sdcard_common.h"
#include "../interfaces/ttc_sdcard_interface.h" // allows to call functions of current low-level driver
#include "../ttc_systick.h"                     // precise delays from single systick timer
#include "../ttc_heap.h"                        // dynamic memory and safe arrays
#include "../ttc_basic.h"
#include "../ttc_crc.h"                         // cyclic redundancy checksum implementations
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//}Includes
/** Global Variables ************************************************{
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

const t_u8 sdcard_common_token_StartSingleBlock   = 0b11111110; // token starting every single data block (-> Documentation/Devices/SDCARD_Physical_Layer_Spec.pdf p.111)
const t_u8 sdcard_common_token_StartMultipleBlock = 0b11111100; // token starting a multiple block transmission (-> Documentation/Devices/SDCARD_Physical_Layer_Spec.pdf p.111)
const t_u8 sdcard_common_token_StopTransmission   = 0b11111101; // token stopping a multiple block transmission (-> Documentation/Devices/SDCARD_Physical_Layer_Spec.pdf p.111)
//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** Private Function Declarations ***********************************{
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)
//}PrivateFunctions
/** Function Definitions ********************************************{
 */

e_ttc_sdcard_errorcode   sdcard_common_block_write( t_ttc_sdcard_config* Config, t_u32 Address, const t_u8* Buffer, t_u16 BufferSize ) {
    Assert_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_Readable( Buffer, ttc_assert_origin_auto ); // always check incoming pointer arguments

    if ( !Config->Flags.CardMounted )
    { return E_ttc_sdcard_errorcode_card_not_found; }

    if ( Config->Flags.CardWriteProtected )
    { return E_ttc_sdcard_errorcode_card_not_writable; }

    VOLATILE_SDCARD e_ttc_sdcard_errorcode Error = E_ttc_sdcard_errorcode_card_writing_timeout;
    if ( sdcard_common_wait_busy( Config ) ) {
        Error = _driver_sdcard_block_write( Config, Address, Buffer, BufferSize );

        sdcard_common_status( Config );
        if ( Config->Card.LastResponse.Word ) {
            Error = E_ttc_sdcard_errorcode_card_writing;

            if ( Config->Card.LastResponse.R2.Bits.CardIsLocked )
            { Error = E_ttc_sdcard_errorcode_card_writing_locked; }
            else if ( Config->Card.LastResponse.R2.Bits.CardECCfailed )
            { Error = E_ttc_sdcard_errorcode_card_writing_ecc; }
            else if ( Config->Card.LastResponse.R2.Bits.CC_Error )
            { Error = E_ttc_sdcard_errorcode_card_writing_cc; }
            else if ( Config->Card.LastResponse.R2.Bits.EraseParam )
            { Error = E_ttc_sdcard_errorcode_card_writing_eraseparam; }
            else if ( Config->Card.LastResponse.R2.Bits.Error_Address )
            { Error = E_ttc_sdcard_errorcode_card_writing_address; }
            else if ( Config->Card.LastResponse.R2.Bits.Error_CRC )
            { Error = E_ttc_sdcard_errorcode_card_writing_crc; }
            else if ( Config->Card.LastResponse.R2.Bits.Error_EraseSequence )
            { Error = E_ttc_sdcard_errorcode_card_writing_eraseseq; }
            else if ( Config->Card.LastResponse.R2.Bits.Error_IllegalCommand )
            { Error = E_ttc_sdcard_errorcode_card_illegalcommand; }
        }
        else
        { Error = 0; }
    }

    return Error;
}
e_ttc_sdcard_errorcode   sdcard_common_block_read( t_ttc_sdcard_config* Config, t_u32 Address, t_u8* Buffer, t_u16 BufferSize ) {
    Assert_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_Writable( Buffer, ttc_assert_origin_auto ); // always check incoming pointer arguments
    VOLATILE_SDCARD e_ttc_sdcard_errorcode Result = E_ttc_sdcard_errorcode_card_reading_timeout;

    Assert_SDCARD( BufferSize >= Config->Card.BlockSize, ttc_assert_origin_auto ); // buffer must be able to store complete block. Increase size of buffer to Config->Card.BlockSize!

    if ( !Config->Flags.CardMounted )
    { Result = E_ttc_sdcard_errorcode_card_not_found; }
    else {
        if ( sdcard_common_wait_busy( Config ) )
        { Result = _driver_sdcard_block_read( Config, Address, Buffer, BufferSize ); }
    }

    return Result;
}
t_u32                    sdcard_common_blocks_count( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    if ( !Config->Card.BlockCount ) { // try to read block count from sdcard
        sdcard_common_card_data( Config ); // will update Config->Card.BlockCount
    }
    return Config->Card.BlockCount;
}
t_u8                     sdcard_common_blocks_write( void* Config, const t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks ) {
    t_ttc_sdcard_config* ConfigSdCard = ( t_ttc_sdcard_config* ) Config;
    Assert_Writable( ConfigSdCard, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_Readable( Buffer, ttc_assert_origin_auto ); // always check incoming pointer arguments
    e_ttc_sdcard_errorcode Result = 0;

    while ( AmountBlocks-- ) {
        Result = sdcard_common_block_write( ConfigSdCard, Address, Buffer, ConfigSdCard->Card.BlockSize );
        if ( Result )
        { break; }
        if ( AmountBlocks ) {
            Address++;
            Buffer += ConfigSdCard->Card.BlockSize;
        }
    }

    return Result; // e_ttc_sdcard_errorcode
}
t_u8                     sdcard_common_blocks_read( void* Config, t_u8* Buffer, t_u32 Address, t_u8 AmountBlocks ) {
    t_ttc_sdcard_config* ConfigSdCard = ( t_ttc_sdcard_config* ) Config;

    Assert_Writable( ConfigSdCard, ttc_assert_origin_auto ); // always check incoming pointer arguments
    Assert_Writable( Buffer, ttc_assert_origin_auto ); // always check incoming pointer arguments
    e_ttc_sdcard_errorcode Result = 0;

    while ( AmountBlocks-- ) {
        Result = sdcard_common_block_read( ConfigSdCard, Address, Buffer, ConfigSdCard->Card.BlockSize );
        if ( Result )
        { break; }
        if ( AmountBlocks ) {
            Address++;
            Buffer += ConfigSdCard->Card.BlockSize;
        }
    }
    return Result; // e_ttc_sdcard_errorcode
}
t_u16                    sdcard_common_blocks_size( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    if ( !Config->Card.BlockSize ) { // try to read block size from sdcard
        sdcard_common_card_data( Config ); // will update Config->Card.BlockSize
    }

    return Config->Card.BlockSize;
}
t_ttc_sdcard_data*       sdcard_common_card_data( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    volatile t_ttc_sdcard_csd_response ResponseCSD; // response to CMD9
    volatile t_ttc_sdcard_cid_response ResponseCID; // response to CMD10

    if ( !Config->Flags.CardDataValid ) { // read all relevant data from sdcard
        Config->Card.BlockCount     = 0;
        Config->Card.BlockMaxLength = 0;

        if ( 1 ) { // read csd register or return NULL
            t_u16 Amount = ( Config->Flags.CardVersion == 1 ) ?
                           sizeof( ResponseCSD.CSD.V1 ) :
                           sizeof( ResponseCSD.CSD.V2 );

            t_u8 AmountRead;
            AmountRead = sdcard_common_command_rn( Config,
                                                   E_ttc_sdcard_cmd09_get_card_specific_data,
                                                   0,
                                                   ( ( t_u8* ) & ( ResponseCSD ) ),
                                                   Amount
                                                 );

            if ( 0 ) {
                TODO( "fix crc check of SpecificData!" )
                if ( !Config->Flags.CardDisabledCRC ) { // crc checks enabled: check integrity of data
                    t_u8 CRC7 = ( Config->Flags.CardVersion == 1 ) ?
                                ResponseCSD.CSD.V1.CRC7 :
                                ResponseCSD.CSD.V2.CRC7;
                    if ( !sdcard_common_crc7_check( ( t_u8* ) & ( ResponseCSD ), Amount - 1, CRC7 ) )
                    { AmountRead = 0; }  // crc check failed: force abort
                }
            }
            volatile u_ttc_sdcard_specific_data* CSD = &( ResponseCSD.CSD );

            if ( AmountRead == Amount ) { // we got as many bytes as we expected: extract some special data
                sdcard_common_command_r1( Config, E_ttc_sdcard_cmd58_get_ocr, 0 );
                if ( Config->Card.LastResponse.Byte ) { // SDHC Card found (> 2GB)
                    Config->Flags.CardIsHighCapacity  = 1;
                    Config->Flags.CardBlockAddressing = 1; // high capacity cards always use block addressing
                }
                else {                                   // SD Card found (<= 2GB)
                    Config->Flags.CardIsHighCapacity  = 0;
                    Config->Flags.CardBlockAddressing = 0;
                    sdcard_common_command_r1( Config, E_ttc_sdcard_cmd16_set_blocklength, 512 );
                }

                if ( CSD->V1.Version == 0 ) { // read csd structure V1

                    if ( ( ( CSD->V1.MaxDataTransferRate == 0x32 ) || ( CSD->V1.MaxDataTransferRate == 0x5a ) ) &&
                            ( ( CSD->Bytes[15] & 1 ) == 1 )
                       ) { // data seems usable: calculate block count and other
                        Config->Flags.CardIsHighCapacity = 0;

                        // calculate capacity
                        t_u8 Shift = ( CSD->V1.CapacitySize_Multiply_High << 1 ) |
                                     CSD->V1.CapacitySize_Multiply_Low;
                        t_u16 Multiply = 4 << Shift;
                        t_u32 DeviceSize = ( CSD->V1.DeviceSize_High << 10 ) |
                                           ( CSD->V1.DeviceSize_Mid << 2 )   |
                                           ( CSD->V1.DeviceSize_Low << 0 );

                        Config->Card.BlockCount = ( DeviceSize + 1 ) * Multiply;

                        // calculate maximum supported block length (-> p.84)
                        t_u8 BL_LEN = ( CSD->V1.WriteBlockLengthMax_High << 2 ) |
                                      CSD->V1.WriteBlockLengthMax_Low;
                        Config->Card.BlockMaxLength = 1 << BL_LEN;
                        Config->Flags.CardDataValid = 1;
                    }
                }
                else {                        // read csd structure V2
                    if ( ( ( CSD->V2.MaxDataTransferRate == 0x32 ) || ( CSD->V2.MaxDataTransferRate == 0x5a ) ) &&
                            ( ( CSD->Bytes[15] & 1 ) == 1 ) &&
                            ( CSD->V2.Version == 1 )
                       ) { // data seems usable: calculate block count and other
                        Config->Flags.CardIsHighCapacity = 1;

                        // calculate capacity
                        t_u32 DeviceSize = ( CSD->V2.DeviceSize_High << 16 ) |
                                           ( CSD->V2.DeviceSize_Mid << 8 )   |
                                           ( CSD->V2.DeviceSize_Low << 0 );

                        // calculate maximum supported block length (-> p.84)
                        t_u8 BL_LEN = ( CSD->V2.WriteBlockLengthMax_High << 2 ) |
                                      CSD->V2.WriteBlockLengthMax_Low;
                        Config->Card.BlockMaxLength = 1 << BL_LEN;

                        // smart calculation avoids overflow for huge cards
                        Config->Card.BlockCount = ( DeviceSize + 1 ) / Config->Card.BlockMaxLength * 512 * 1024;

                        Config->Flags.CardDataValid = 1;
                    }
                }

                if ( Config->Flags.CardDataValid ) {
                    if ( Config->Card.BlockSize > Config->Card.BlockMaxLength )
                    { Config->Card.BlockSize = Config->Card.BlockMaxLength; }
                }
            }
            else
            { return NULL; }
        }
        if ( 1 ) { // read cid register
            t_u8 Retries = 100;
            do {
                if ( Config->Card.LastResponse.R1.Bits.InIdleState ) {
                    ttc_systick_delay_simple( 10000 );
                    sdcard_common_wake_up( Config );
                }
                sdcard_common_command_rn( Config,
                                          E_ttc_sdcard_cmd10_get_card_identification,
                                          0,
                                          ( t_u8* ) & ResponseCID,
                                          sizeof( ResponseCID )
                                        );
            }
            while ( Config->Card.LastResponse.R1.Bits.InIdleState && --Retries ); // card went back to idle state: wake it up again (seen with a 1GB and a 2GB SDCARD)

            if ( Retries ) { // successfully read CID register
                Config->Card.SerialNumber = ttc_basic_32_read_big_endian( ( t_u8* ) & ( ResponseCID.CID.SerialNumber ) );
                Config->Card.Year  = 2000 + ( ResponseCID.CID.Date_Year_High << 4 ) + ResponseCID.CID.Date_Year_Low;
                Config->Card.Month = ResponseCID.CID.Date_Month;
            }
            else {           // timeout while trying to read CID
                Config->Card.SerialNumber = 0xffffffff;
                Config->Card.Year         = 0;
                Config->Card.Month        = 0;
            }
        }
    }

    return &( Config->Card );
}
t_u16                    sdcard_common_command_rn( t_ttc_sdcard_config* Config, e_ttc_sdcard_command Command, t_u32 Argument, t_u8* Buffer, t_u16 Amount ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto );
    Assert_SDCARD( ( Command & 0x40 ) != 0, ttc_assert_origin_auto ); // command value must have bit 6 set. Check implementation of caller!

    t_ttc_sdcard_command_argument CommandArgument;
    sdcard_common_prepare_command( Config, &CommandArgument, Command, Argument );

    // pass function call to interface or low-level driver function
    return _driver_sdcard_command_rn( Config, &CommandArgument, Buffer, Amount );
}
void                     sdcard_common_command_r1( t_ttc_sdcard_config* Config, e_ttc_sdcard_command Command, t_u32 Argument ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto );
    Assert_SDCARD( ( Command & 0x40 ) != 0, ttc_assert_origin_auto ); // command value must have bit 6 set. Check implementation of caller!

    t_ttc_sdcard_command_argument CommandArgument;
    sdcard_common_prepare_command( Config, &CommandArgument, Command, Argument );

    // pass function call to interface or low-level driver function
    _driver_sdcard_command_r1( Config, &CommandArgument );
}
void                     sdcard_common_command_r2( t_ttc_sdcard_config* Config, e_ttc_sdcard_command Command, t_u32 Argument ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto );
    Assert_SDCARD( ( Command & 0x40 ) != 0, ttc_assert_origin_auto ); // command value must have bit 6 set. Check implementation of caller!

    t_ttc_sdcard_command_argument CommandArgument;
    sdcard_common_prepare_command( Config, &CommandArgument, Command, Argument );

    _driver_sdcard_command_rn( Config, &CommandArgument,
                               ( ( t_u8* ) & ( Config->Card.LastResponse ) ) + 1, // first byte received implicitly: specify only second byte
                               1
                             );
}
void                     sdcard_common_command_application_r1( t_ttc_sdcard_config* Config, e_ttc_sdcard_command_application ApplicationCommand, t_u32 Argument ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto );
    Assert_SDCARD( ( ApplicationCommand & 0x40 ) != 0, ttc_assert_origin_auto ); // command value must have bit 6 set. Check implementation of caller!

    t_ttc_sdcard_command_argument CommandArgument;
    sdcard_common_prepare_command( Config, &CommandArgument, E_ttc_sdcard_cmd55_application_cmd, Argument );
    // pass function call to interface or low-level driver function
    _driver_sdcard_command_r1( Config, &CommandArgument );

    if ( Config->Card.LastResponse.Byte == 0x01 ) { // card is now awaiting application command
        sdcard_common_prepare_command( Config, &CommandArgument, ApplicationCommand, Argument );

        // pass function call to interface or low-level driver function
        _driver_sdcard_command_r1( Config, &CommandArgument );
    }
}
void                     sdcard_common_command_application_rn( t_ttc_sdcard_config* Config, e_ttc_sdcard_command_application ApplicationCommand, t_u32 Argument, t_u8* Buffer, t_u16 Amount ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto );
    Assert_SDCARD( ( ApplicationCommand & 0x40 ) != 0, ttc_assert_origin_auto ); // command value must have bit 6 set. Check implementation of caller!

    t_ttc_sdcard_command_argument CommandArgument;
    sdcard_common_prepare_command( Config, &CommandArgument, E_ttc_sdcard_cmd55_application_cmd, Argument );
    // pass function call to interface or low-level driver function
    _driver_sdcard_command_r1( Config, &CommandArgument );

    if ( Config->Card.LastResponse.Byte == 0x01 ) { // card is now awaiting application command
        sdcard_common_prepare_command( Config, &CommandArgument, ApplicationCommand, Argument );

        // pass function call to interface or low-level driver function
        _driver_sdcard_command_rn( Config, &CommandArgument, Buffer, Amount );
    }
}
t_u8                     sdcard_common_crc7_calculate( const t_u8* Buffer, t_u16 Amount, t_u8 CRC7 ) {
    Assert_Readable( ( void* ) Buffer, ttc_assert_origin_auto ); // always check incoming pointer arguments

    if ( CRC7 )
    { CRC7 >>= 1; } // previous checksum given: must restore CRC7 from 8 bit sdcard style crc7

    CRC7 = ttc_crc_crc7_calculate( Buffer, Amount, CRC7 );

    // Special formatting of return value (-> http://www.avrfreaks.net/forum/crc7-cmd8-sd-card)
    return ( ( CRC7 << 1 ) | 1 );
}
BOOL                     sdcard_common_crc7_check( const t_u8* Buffer, t_u16 Amount, t_u8 CRC7 ) {
    Assert_Readable( ( void* ) Buffer, ttc_assert_origin_auto ); // always check incoming pointer arguments

    t_u8 CalculatedCRC7 = sdcard_common_crc7_calculate( ( void* ) Buffer, Amount, 0 );

    return ( CRC7 == CalculatedCRC7 ) ? TRUE : FALSE;
}
t_u16                    sdcard_common_crc16_calculate( const t_u8* Buffer, t_u16 Amount, t_u16 CRC16 ) {
    Assert_Readable( ( void* ) Buffer, ttc_assert_origin_auto ); // always check incoming pointer arguments

    /* must use the non CCITT compliant CRC16 implementation with polynomial 0x1021
     * to calculate same checksum 0x7fa1 for 512 bytes of 0xff as stated in
     * Documentation/Devices/SDCARD_Physical_Layer_Spec.pdf p.44.
     * Also check
    */
    CRC16 = ttc_crc_crc16_calculate_0x1021( Buffer, Amount, CRC16 );

    return CRC16;
}
void                     sdcard_common_disable_crc( t_ttc_sdcard_config* Config, BOOL DisableCRC ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    t_u32 Argument = ( DisableCRC ) ? 0 : 1;
    sdcard_common_command_r1( Config, E_ttc_sdcard_cmd59_crc_on_off, Argument );
    if ( Config->Card.LastResponse.Byte == 0 ) { // command executed successfully
        Config->Flags.CardDisabledCRC = 1;
    }
    else {                                       // sdcard returned error code
        Config->Flags.CardDisabledCRC = 0;
    }
}
void                     sdcard_common_prepare() {

    if ( TTC_ASSERT_SDCARD ) { // self test crc implementations ->  Documentation/Devices/SDCARD_Physical_Layer_Spec.pdf p.43)
        VOLATILE_SDCARD t_u8  CRC7  = 0;
        VOLATILE_SDCARD t_u16 CRC16 = 0;
        VOLATILE_SDCARD t_u8  Buffer[9];

        ttc_memory_set( ( void* ) Buffer, 0, 5 );
        if ( 1 ) { // testing CRC7 checksum of CMD0 with argument 0
            Buffer[0] = 0b01000000;
            CRC7 = sdcard_common_crc7_calculate( ( const t_u8* ) Buffer, 5, 0 );
            Assert_SDCARD( CRC7 == 0b10010101, ttc_assert_origin_auto );

            CRC7 = 0;
            t_u8* Reader = ( t_u8* ) Buffer;
            for ( t_u8 Loops = 5; Loops > 0; Loops-- ) { // update CRC for every byte
                CRC7 = sdcard_common_crc7_calculate( Reader++, 1, CRC7 );
            }
            Assert_SDCARD( CRC7 == 0b10010101, ttc_assert_origin_auto );
        }
        if ( 1 ) { // testing CRC7 checksum of CMD17 with argument 0
            Buffer[0] = 0b01010001;
            CRC7 = sdcard_common_crc7_calculate( ( const t_u8* ) Buffer, 5, 0 );
            Assert_SDCARD( CRC7 == 0b01010101, ttc_assert_origin_auto );

            CRC7 = 0;
            t_u8* Reader = ( t_u8* ) Buffer;
            for ( t_u8 Loops = 5; Loops > 0; Loops-- ) { // update CRC for every byte
                CRC7 = sdcard_common_crc7_calculate( Reader++, 1, CRC7 );
            }
            Assert_SDCARD( CRC7 == 0b01010101, ttc_assert_origin_auto );
        }
        if ( 1 ) { // testing CRC7 checksum of response to CMD17
            Buffer[0] = 0b00010001;
            Buffer[3] = 0b00001001;
            CRC7 = sdcard_common_crc7_calculate( ( const t_u8* ) Buffer, 5, 0 );
            Assert_SDCARD( CRC7 == 0b01100111, ttc_assert_origin_auto );

            CRC7 = 0;
            t_u8* Reader = ( t_u8* ) Buffer;
            for ( t_u8 Loops = 5; Loops > 0; Loops-- ) { // update CRC for every byte
                CRC7 = sdcard_common_crc7_calculate( Reader++, 1, CRC7 );
            }
            Assert_SDCARD( CRC7 == 0b01100111, ttc_assert_origin_auto );
        }

        if ( 1 ) { // testing CRC16-CCITT checksum for "123456789" (-> http://srecord.sourceforge.net/crc16-ccitt.html)
            Buffer[0] = '1';
            Buffer[1] = '2';
            Buffer[2] = '3';
            Buffer[3] = '4';
            Buffer[4] = '5';
            Buffer[5] = '6';
            Buffer[6] = '7';
            Buffer[7] = '8';
            Buffer[8] = '9';
            CRC16 = sdcard_common_crc16_calculate( ( const t_u8* ) Buffer, 9, 0xffff );
            //Assert_SDCARD( CRC16 == 0xe5cc, ttc_assert_origin_auto ); // as claimed to be the "correct crc16-ccitt" here: http://srecord.sourceforge.net/crc16-ccitt.html
            Assert_SDCARD( CRC16 == 0x29b1, ttc_assert_origin_auto ); // this value is expected for sdcard usage (though it seems to be based on an invalid crc16-ccitt implementation)
        }
        if ( 1 ) { // testing CRC16 checksum of 512 bytes of 0xff
            ttc_memory_set( ( void* ) Buffer, 0xff, 8 );
            t_u16 Remaining = 512;
            CRC16 = 0;
            t_u8 StepSize = 1;

            while ( Remaining ) { // calculate CRC16 as a series of 4 byte blocks
                CRC16 = sdcard_common_crc16_calculate( ( const t_u8* ) Buffer, StepSize, CRC16 );
                Remaining -= StepSize;
            }
            Assert_SDCARD( CRC16 == 0x7fa1, ttc_assert_origin_auto );
        }
    }
}
void                     sdcard_common_prepare_command( t_ttc_sdcard_config* Config, t_ttc_sdcard_command_argument* CommandArgument, e_ttc_sdcard_command Command, t_u32 Argument ) {
    Assert_SDCARD_Writable( Config, ttc_assert_origin_auto );
    CommandArgument->Command  = Command;
    CommandArgument->Argument = ttc_basic_32_read_big_endian( ( t_u8* ) &Argument );
    if ( Config->Flags.CardDisabledCRC ) // checksumming disabled: load dummy value
    { CommandArgument->CRC7 = 0xff; }
    else {                               // checksumming disabled: calculate checksum
        /* DEPRECATED
        if ( Command == E_ttc_sdcard_cmd00_go_idle_state )   // special handling for some known values required (dunno why! *-/)
        { CommandArgument->CRC7 = 0x95; }
        //? { CommandArgument->CRC7 = 0x4A; }
        else if ( Command == E_ttc_sdcard_cmd08_get_interface_condition )
        { CommandArgument->CRC7 = 0x43; }
        else if ( Command == E_ttc_sdcard_cmd01_get_operating_condition )
        { CommandArgument->CRC7 = 0xF9; }
        else if ( Command == E_ttc_sdcard_cmd58_get_ocr )
        { CommandArgument->CRC7 = 0x25; }
        else if ( Command == E_ttc_sdcard_cmd09_get_card_specific_data )
        { CommandArgument->CRC7 = 0xAF; }
        else if ( Command == E_ttc_sdcard_cmd59_crc_on_off )
        { CommandArgument->CRC7 = 0x25; }
        else if ( Command == E_ttc_sdcard_cmd16_set_blocklength )
        { CommandArgument->CRC7 = 0xFF; }
        else {
        */
        CommandArgument->CRC7 = sdcard_common_crc7_calculate( ( t_u8* ) CommandArgument, sizeof( *CommandArgument ) - 1, 0 );
        //}
    }
}
void                     sdcard_common_status( t_ttc_sdcard_config* Config ) {
    Assert_Writable( Config, ttc_assert_origin_auto ); // always check incoming pointer arguments

    if ( sdcard_common_wait_busy( Config ) ) {
        // -> Documentation/Devices/SDCARD_Physical_Layer_Spec.pdf p.112)
        sdcard_common_command_r2( Config, E_ttc_sdcard_cmd13_sd_status, 0 );
    }
    else {
        Config->Card.LastResponse.R2.Word = 0;
    }
}
BOOL                     sdcard_common_wait_busy( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)

    return _driver_sdcard_wait_busy( Config );
}
e_ttc_sdcard_errorcode   sdcard_common_wake_up( t_ttc_sdcard_config* Config ) {
    Assert_SDCARD_EXTRA_Writable( Config, ttc_assert_origin_auto ); // check if it points to RAM (change for read only memory!)
    e_ttc_sdcard_errorcode Error = 0;

    /* Card initialization and identification process
     * -> ~/Source/TheToolChain/Documentation/Devices/SDCARD_Physical_Layer_Spec.pdf p.14
     */

    Config->Flags.CardIsMMC   = 0;
    Config->Flags.CardVersion = 0;
    u_ttc_sdcard_response* Response = &( Config->Card.LastResponse );
    sdcard_common_command_r1( Config, E_ttc_sdcard_cmd08_get_interface_condition, 0x1aa ); // (-> http://elm-chan.org/docs/mmc/mmc_e.html)

    if ( Response->Byte > 0x01 ) { // no response or error: >V2 SDCARD with voltage mismatch, V1 SDCARD or MMC
        t_u8 Retries = 100; // wait up to 1000ms

        do { // send ACMD41 to set voltage range until card is ready
            sdcard_common_command_application_r1( Config,
                                                  E_ttc_sdcard_acmd41_sd_send_operating_condition,
                                                  0
                                                );

            ttc_systick_delay_simple( 10000 );
        }
        while ( ( --Retries ) && ( Response->Byte > 0x00 ) );

        if ( !Retries ) { // this is not an sdcard: check if it is an MMC
            sdcard_common_command_r1( Config,
                                      E_ttc_sdcard_cmd01_get_operating_condition,
                                      0
                                    );
            if ( Response->Byte > 0x01 )
            { Error = E_ttc_sdcard_errorcode_card_not_compatible; }
            else
            { Config->Flags.CardIsMMC = 1; } // woken up an MMC v3
        }
        else {                      // woken up an SDCARD v1.x
            Config->Flags.CardVersion = 1;
        }
    }
    else {                          // got response: >V2 SDCARD
        Config->Flags.CardVersion = 2;
    }
    if ( Config->Flags.CardVersion >= 2 ) { // found SDCARD >=v2: try to power up sdcard to supported voltage
        t_u8 Retries = 100; // wait up to 1000ms

        do { // send ACMD41 to set voltage range until card is ready
            t_u32 Argument = 0;
            if ( Config->Init.Flags.Support_HighCapacity )
            { Argument = 1 << 30; }

            sdcard_common_command_application_r1( Config,
                                                  E_ttc_sdcard_acmd41_sd_send_operating_condition,
                                                  Argument
                                                );

            ttc_systick_delay_simple( 10000 );
        }
        while ( ( --Retries ) && ( Response->Byte > 0x00 ) );
        if ( !Retries )
        { Error = E_ttc_sdcard_errorcode_card_not_compatible; }
        else { // SDCARD >=V2 is awake: further initialize it

            volatile t_ttc_sdcard_ocr OperatingCondition; OperatingCondition.Word = 0;
            sdcard_common_command_rn( Config,
                                      E_ttc_sdcard_cmd58_get_ocr,
                                      0,
                                      ( t_u8* ) &OperatingCondition,
                                      sizeof( OperatingCondition )
                                    );
            if ( Response->Byte )  { // invalid response: set error code
                if ( !Config->Init.Flags.Support_HighCapacity )
                { Error = E_ttc_sdcard_errorcode_card_too_large; } // current driver does not support high capacity cards
                else
                { Error = E_ttc_sdcard_errorcode_card_not_compatible; }
            }
            else {
                Config->Flags.CardBlockAddressing = OperatingCondition.Bits.BlockAddressing;
                Config->Flags.CardIsHighCapacity  = OperatingCondition.Bits.BlockAddressing;
            }
        }
    }
    if ( !Error && !Config->Flags.CardBlockAddressing ) { // force block size to 512 bytes for FAT file system
        sdcard_common_command_r1( Config, E_ttc_sdcard_cmd16_set_blocklength, 512 );
        if ( Response->Byte )
        { Error = E_ttc_sdcard_errorcode_card_blocklength; }

        // CMD2 not supported over SPI
        // CMD3 not supported over SPI
    }

    return Error;
}
e_ttc_storage_event      sdcard_common_medium_detect( void* Config ) {
    t_ttc_sdcard_config* ConfigSdCard = ( t_ttc_sdcard_config* ) Config;
    Assert_Writable( ConfigSdCard, ttc_assert_origin_auto ); // always check incoming pointer arguments
    e_ttc_storage_event Event = 0;

    // pass function call to interface or low-level driver function
    Event = _driver_sdcard_medium_detect( ConfigSdCard );

    switch ( Event ) { // create inserted/removed events
        case E_ttc_storage_event_media_present:
            if ( ( ConfigSdCard->LastEvent != E_ttc_storage_event_media_present ) &&
                    ( ConfigSdCard->LastEvent != E_ttc_storage_event_media_inserted )
               ) {
                ConfigSdCard->Flags.CardPresent = 1;
                Event = E_ttc_storage_event_media_inserted;
            }
            break;
        case E_ttc_storage_event_media_missing:
            if ( ConfigSdCard->LastEvent != Event ) {
                ConfigSdCard->Flags.CardPresent = 0;
                if ( ConfigSdCard->Flags.CardMounted ) {
                    Event = E_ttc_storage_event_media_removed_medium_mounted;
                }
                else
                { Event = E_ttc_storage_event_media_removed; }
            }
            break;
        default : break;
    }

    // remember this event
    ConfigSdCard->LastEvent = Event;

    return Event; // t_u8
}
t_u8                     sdcard_common_medium_mount( void* Config, t_u16* BlockSize ) {
    t_ttc_sdcard_config* ConfigSdCard = ( t_ttc_sdcard_config* ) Config;
    Assert_Writable( ConfigSdCard, ttc_assert_origin_auto ); // always check incoming pointer arguments

    e_ttc_sdcard_errorcode Error = 0;

    sdcard_common_medium_detect( ConfigSdCard );
    if ( ConfigSdCard->Flags.CardPresent ) { // sdcard is present but not mounted: mount it
        if ( ConfigSdCard->Flags.CardMounted )
        { sdcard_common_medium_unmount( ConfigSdCard ); }

        ttc_memory_set( &( ConfigSdCard->Card ),  0, sizeof( ConfigSdCard->Card ) );

        Error = _driver_sdcard_medium_go_idle( ConfigSdCard );
        if ( !Error ) { // initialize + identify sdcard
            ConfigSdCard->Flags.CardMounted = 1; // flag must be set for further initialization

            Error = sdcard_common_wake_up( ConfigSdCard );
        }
        if ( !Error ) { // set block size
            ConfigSdCard->Card.BlockSize = 512;
            sdcard_common_command_r1( ConfigSdCard, E_ttc_sdcard_cmd16_set_blocklength, ConfigSdCard->Card.BlockSize );
            if ( ConfigSdCard->Card.LastResponse.Byte ) {
                Error = E_ttc_sdcard_errorcode_card_blocklength;
            }
        }
        if ( 1 && !Error ) { // disable checksumming (faster)
            sdcard_common_disable_crc( ConfigSdCard, TRUE );
            if ( ConfigSdCard->Card.LastResponse.Byte ) {
                Error = E_ttc_sdcard_errorcode_card_crc_on_off;
            }
        }
        if ( !Error ) { // read all relevant data from card
            if ( !sdcard_common_card_data( ConfigSdCard ) )
            { Error = E_ttc_sdcard_errorcode_card_invalid_csd; }
            if ( ConfigSdCard->Card.LastResponse.R1.Bits.InIdleState )
            { Error = sdcard_common_wake_up( ConfigSdCard ); }  // card went back to idle state: wake it up again
        }
        if ( !Error ) { // switch interface to high-speed
            sdcard_spi_set_speed( ConfigSdCard, E_ttc_sdcard_speed_normal ); // resume spi bus clock to highspeed
        }

        if ( ConfigSdCard->Flags.CardIsHighCapacity )
        { Error = E_ttc_sdcard_errorcode_card_too_large; }
    }
    else
    { Error = E_ttc_sdcard_errorcode_card_not_found; }

    if ( Error )
    { sdcard_common_medium_unmount( ConfigSdCard ); }
    else {
        if ( BlockSize ) { // update block size
            Assert_SDCARD_Writable( BlockSize, ttc_assert_origin_auto ); // must point to RAM. check implementation of caller!
            *BlockSize = ConfigSdCard->Card.BlockSize;
        }
    }
    return Error;
}
t_u8                     sdcard_common_medium_unmount( void* Config ) {
    t_ttc_sdcard_config* ConfigSdCard = ( t_ttc_sdcard_config* ) Config;
    Assert_Writable( ConfigSdCard, ttc_assert_origin_auto ); // always check incoming pointer arguments
    e_ttc_sdcard_errorcode Error = 0;
    if ( ConfigSdCard->Flags.CardMounted ) { // sdcard is mounted: try to unmount it
        sdcard_common_wait_busy( ConfigSdCard );

        sdcard_common_medium_detect( ConfigSdCard );
        _driver_sdcard_medium_unmount( ConfigSdCard );
        if ( !ConfigSdCard->Flags.CardPresent ) // sdcard was removed before we could unmount it: return error
        { Error = E_ttc_sdcard_errorcode_card_not_found; }

        ConfigSdCard->Card.BlockSize           = 0;
        ConfigSdCard->Card.BlockCount          = 0;
        ConfigSdCard->Flags.CardMounted        = 0;
        ConfigSdCard->Flags.CardVersion        = 0;
        ConfigSdCard->Flags.CardIsHighCapacity = 0;
        ConfigSdCard->Flags.CardWriteProtected = 0;
    }

    return Error;
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
/** Private Functions ***********************************************{
 *
 * Private functions are declared in this c-file. They can be called from outside
 * via an extern declaration (or by ignoring the corresponding compiler warning).
 * Though it is possible to call them from outside, it is not intended to do so!
 * Private functions may be changed, renamed or deleted without notification by
 * the maintainer of this file.
 */

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}Private Functions
