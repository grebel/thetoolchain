#ifndef TTC_ETHERNET_H
#define TTC_ETHERNET_H
/** { ttc_ethernet.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for ethernet devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_ETHERNET(tc_ethernet_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_ethernet_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_ethernet_init(LogicalIndex);
 *  4) use:         ttc_ethernet_XXX(LogicalIndex);
 *
 *  Device Index
 *  Devices are addressed by a logical index with no correlation to the order of physical devices.
 *  Logical indices start at 1 to simply detect uninitialized values.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level ethernet and application.
 *
 *  Created from template ttc_device.h revision 39 at 20180130 11:09:47 UTC
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of ttc_ethernet (Do not delete this line!)
 *
 *  PLACE_YOUR_DESCRIPTION_HERE
}*/

#ifndef EXTENSION_ttc_ethernet
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_ethernet.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_ethernet.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_ethernet_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level ethernet only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function Declarations ************************************************{
 *
 * The functions declared below provide the main interface for
 * ethernet devices on all supported architectures.
 * Check ethernet/ethernet_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** allocates a new ethernet instance and returns pointer to its configuration
 *
 * @return (t_ttc_ethernet_config*)  pointer to new configuration. Set all Init fields before calling ttc_ethernet_init() on it.
 */
t_ttc_ethernet_config* ttc_ethernet_create();

/** Prepares ethernet Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_ethernet_prepare();
void _driver_ethernet_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_ethernet_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    logical index of ETHERNET device. Each logical device <n> is defined via TTC_ETHERNET<n>* constants in compile_options.h and extensions.active/makefile
 * @return                configuration of indexed device
 */
t_ttc_ethernet_config* ttc_ethernet_get_configuration( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    logical index of ETHERNET device. Each logical device <n> is defined via TTC_ETHERNET<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: ethernet device has been initialized successfully; != 0: error-code
 */
e_ttc_ethernet_errorcode ttc_ethernet_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    logical index of ETHERNET device. Each logical device <n> is defined via TTC_ETHERNET<n>* constants in compile_options.h and extensions.active/makefile
 */
void ttc_ethernet_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_ethernet_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of ETHERNET device. Each logical device <n> is defined via TTC_ETHERNET<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: default values have been loaded successfully for indexed ethernet device; != 0: error-code
 */
e_ttc_ethernet_errorcode  ttc_ethernet_load_defaults( t_u8 LogicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    logical index of ETHERNET device. Each logical device <n> is defined via TTC_ETHERNET<n>* constants in compile_options.h and extensions.active/makefile

 */
void ttc_ethernet_reset( t_u8 LogicalIndex );

/** Reinitializes all initialized devices after changed systemcloc profile.
 *
 * Note: This function is called automatically from _ttc_sysclock_call_update_functions()
 */
void ttc_ethernet_sysclock_changed();

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_ethernet_ are passed to interfaces/ttc_ethernet_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl ethernet UPDATE
 */


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_ethernet_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_ethernet_features.
 *
 * @param Config        Configuration of ethernet device
 * @return              Fields of *Config have been adjusted if necessary
 */
void _driver_ethernet_configuration_check( t_ttc_ethernet_config* Config );

/** shutdown single ETHERNET unit device
 * @param Config        Configuration of ethernet device
 * @return              == 0: ETHERNET has been shutdown successfully; != 0: error-code
 */
e_ttc_ethernet_errorcode _driver_ethernet_deinit( t_ttc_ethernet_config* Config );

/** initializes single ETHERNET unit for operation
 * @param Config        Configuration of ethernet device
 * @return              == 0: ETHERNET has been initialized successfully; != 0: error-code
 */
e_ttc_ethernet_errorcode _driver_ethernet_init( t_ttc_ethernet_config* Config );

/** loads configuration of indexed ETHERNET unit with default values
 * @param Config        Configuration of ethernet device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_ethernet_errorcode _driver_ethernet_load_defaults( t_ttc_ethernet_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of ethernet device
 */
void _driver_ethernet_reset( t_ttc_ethernet_config* Config );
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_ETHERNET_H
