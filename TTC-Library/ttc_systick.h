#ifndef TTC_SYSTICK_H
#define TTC_SYSTICK_H
/** { ttc_systick.h *******************************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  High-Level driver and documentation for systick devices.
 *  The functions defined in this file provide a hardware independent interface
 *  for your application.
 *
 *  The basic usage scenario for devices:
 *  1) check:       Assert_SYSTICK(tc_systick_get_max_index() > 0, ttc_assert_origin_auto);
 *  2) configure:   ttc_systick_get_configuration(LogicalIndex);
 *  3) initialize:  ttc_systick_init(LogicalIndex);
 *  4) use:         ttc_systick_XXX(LogicalIndex);
 *
 *  Device Index
 *  Devices are addressed by a logical index with no correlation to the order of physical devices.
 *  Logical indices start at 1 to simply detect uninitialized values.
 *  Every board defines, which devices can be used under which logical index.
 *  The logical indices are defined in board makefiles (makefile.100_board_*).
 *
 *  Structures, Enums and Defines being required by high-level systick and application.
 *
 *  Created from template ttc_device.h revision 36 at 20160926 13:55:21 UTC
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of ttc_systick (Do not delete this line!)
 *
 * ttc_systick provides a system wide realtime clock and precise timed delays.
 *
 * H2: System Wide Realtime Clock
 *
 * Simply call ->ttc_systick_get_elapsed_ticks() to get the actual system time in ticks.
 * Calling ->ttc_systick_get_elapsed_usecs() will return system time in microseconds.
 * Both system times will overrun after a while. Your application should be able to deal with this.
 *
 * H2: Precise Timed Delays
 *
 * A more convenient use of the system timer is to use the ttc_systick_delay_*() functions.
 * Whenever a task or a statemachine has to wait for a while without blocking, they can
 * initialize a t_ttc_systick_delay variable and periodically check if it has expired.
 * * Initialize delay via ->ttc_systick_delay_init()
 * * Periodically check if delay has expired via ->ttc_systick_delay_expired()
 *
 * Using the system timer for multiple delays has advantages over using individual hardware timers:
 * * Amount of active delays is not limited by amount of hardware timers.
 * * No allocation of hardware time required (system timer is typically always allocated).
 * * No interrupt service routines required (simpler source code).
 *
 * Limitations of ttc_systick_delay_*()
 * * Not as precise timing as with hardware timers (especially for small delays)
 * * Currently does not adapt when system clock frequency is changed (ToDo)
 * * Extra polling overhead.
 *
 *
 * H3: Basic Delay Usage
 * <CODE>
 *   // Allocate variable storing delay data
 *   t_ttc_systick_delay Delay;
 *
 *   // Delay expires in 1000 us == 1 ms
 *   ttc_systick_delay_init(&Delay, 1000);
 *
 *   // wait until delay expires
 *   ttc_systick_delay_wait(&Delay);
 * </CODE>
 *
 * H3: Do something until timeout
 * This example shows how to do something and abort when a 1 millisecond timeout has expired.
 * One also may initialize and observe multiple delays.
 * <CODE>
 *   // Allocate variable storing delay data
 *   t_ttc_systick_delay TimeOut;
 *
 *   // Delay expires in 1000 us == 1 ms
 *   ttc_systick_delay_init(&TimeOut, 1000);
 *
 *   // wait until delay expires
 *   while (SOME_CONDITION) {
 *     ... do something ...
 *
 *     if ( ttc_systick_delay_expired(&TimeOut) ) {
 *       break; // Timeout: leave loop
 *     }
 *   }
 * </CODE>
}*/

#ifndef EXTENSION_ttc_systick
    #error Missing extension! Please add this to your activate_project.sh script: activate.500_ttc_systick.sh
#endif

//{ Includes *************************************************************
//
// Header files include all header files that are required to include this
// header file. Includes that provide function declarations should be placed
// in "ttc_systick.c"
//
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_memory.h"
#include "interfaces/ttc_systick_interface.h" // multi architecture support
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

//} Includes
//{ Macros ***************************************************************

//InsertMacros above (DO NOT REMOVE THIS LINE!)

//}Macros
//{ Defines/ TypeDefs ****************************************************

//InsertDefines above (DO NOT REMOVE THIS LINE!)

//} Defines
//{ Structures/ Enums ****************************************************
// Structures and Enums required by High-Level systick only go here...

//InsertEnums above (DO NOT REMOVE THIS LINE!)

//InsertTypeDefs above (DO NOT REMOVE THIS LINE!)

//} Structures/ Enums
/** Function Declarations ************************************************{
 *
 * The functions declared below provide the main interface for
 * systick devices on all supported architectures.
 * Check systick/systick_* files for a list of low-level drivers.
 * In most cases, only one low-level driver may be active during compilation.
 * Low-level drivers get activated by calling an activate.450_*.sh script.
 * A complete list of all available activate scripts can be found in your
 * activate_project.sh file inside your project folder.
 *
 */

/** Prepares systick Driver for basic operation.
 *
 * Note: This function is called automatically at system start before the scheduler has been started.
 */
void ttc_systick_prepare();
void _driver_systick_prepare();

/** returns amount of devices available in current configuration
 *
 * @return amount of available devices
 */
t_u8 ttc_systick_get_max_index();

/** returns configuration of indexed device (asserts if no valid configuration was found)
 *
 * @param LogicalIndex    logical index of SYSTICK device. Each logical device <n> is defined via TTC_SYSTICK<n>* constants in compile_options.h and extensions.active/makefile
 * @return                configuration of indexed device
 */
t_ttc_systick_config* ttc_systick_get_configuration( t_u8 LogicalIndex );

/** initialize indexed device for use
 *
 * @param LogicalIndex    logical index of SYSTICK device. Each logical device <n> is defined via TTC_SYSTICK<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: systick device has been initialized successfully; != 0: error-code
 */
e_ttc_systick_errorcode ttc_systick_init( t_u8 LogicalIndex );

/** shutdown indexed device
 *
 * @param LogicalIndex    logical index of SYSTICK device. Each logical device <n> is defined via TTC_SYSTICK<n>* constants in compile_options.h and extensions.active/makefile
 */
void ttc_systick_deinit( t_u8 LogicalIndex );

/** load default setting values for indexed device
 *
 * Settings of indexed device are being reset to their default values.
 * Will automatically call ttc_systick_deinit() if device has been initialized.
 *
 * @param LogicalIndex  logical index of SYSTICK device. Each logical device <n> is defined via TTC_SYSTICK<n>* constants in compile_options.h and extensions.active/makefile
 * @return                == 0: default values have been loaded successfully for indexed systick device; != 0: error-code
 */
e_ttc_systick_errorcode  ttc_systick_load_defaults( t_u8 LogicalIndex );

/** reset configuration of indexed device and connected hardware
 *
 * @param LogicalIndex    logical index of SYSTICK device. Each logical device <n> is defined via TTC_SYSTICK<n>* constants in compile_options.h and extensions.active/makefile

 */
void ttc_systick_reset( t_u8 LogicalIndex );

/** Reinitializes all initialized devices after changed systemclock profile.
 *
 * Note: This function is called automatically from _ttc_sysclock_call_update_functions()
 */
void ttc_systick_sysclock_changed();

/** Initialize a delay (works in single- or multitasking setup)
 *
 * Note: Function will return immediately if TimeUS <  ttc_systick_get_configuration()->MinimumDelayUS
 *
 * @param Delay  (t_ttc_systick_delay*)  delay structure must be allocated by caller
 * @param TimeUS (t_base)                delay will expire this amount of microseconds in the future (>= ttc_systick_get_configuration()->MinimumDelayUS)
 */
void ttc_systick_delay_init( t_ttc_systick_delay* Delay, t_base TimeUS );

/** Returns maximum available delay period for current architecture and configuration
 *
 * @return  Maximum valid TimeUS value for ttc_systick_delay_init() calls (microseconds)
 */
t_base ttc_systick_delay_get_maximum();
#define ttc_systick_delay_get_maximum() 1000000

/** Initialize a delay from an interrupt service routine.
 *
 * Note: Use of delays inside interrupt service routines is against the idea of short ISRs.
 *       Don't expect this to work on every architecture!
 *
 * Note: Function will return immediately if TimeUS <  ttc_systick_get_configuration()->MinimumDelayUS
 *
 * @param Delay  (t_ttc_systick_delay*)  delay structure must be allocated by caller
 * @param TimeUS (t_base)                delay will expire this amount of microseconds in the future (>= ttc_systick_get_configuration()->MinimumDelayUS)
 */
void ttc_systick_delay_init_isr( t_ttc_systick_delay* Delay, t_base TimeUS );

/** Check if given delay has expired (works in single- or multitasking setup)
 *
 * @param Delay (t_ttc_systick_delay*)  delay data being initialized by ttc_systick_delay_init() before
 * @return      (BOOL)                  ==TRUE: delay has expired (given time has passed); ==FALSE: delay has not yet been expired
 */
BOOL ttc_systick_delay_expired( t_ttc_systick_delay* Delay );

/** Check if given delay has expired inside interrupt service routine
 *
 * Note: Use of delays inside interrupt service routines is against the idea of short ISRs.
 *       Don't expect this to work on every architecture!
 *
 * @param Delay (t_ttc_systick_delay*)
 * @return      (BOOL)                  ==TRUE: delay has expired (given time has passed); ==FALSE: delay has not yet been expired
 */
BOOL ttc_systick_delay_expired_isr( t_ttc_systick_delay* Delay );

/** Returns current minimum allowed delay time
 *
 * The minimum allowed delay time depends on
 * - architecture (differen microcontroller architectures provide different compute speed)
 * - system clock frequency (may have changed in between)
 * - systick profile (may have been changed)
 *
 * @return (t_base) minimum available delay period for current architecture and configuration (microseconds)
 */
t_base ttc_systick_delay_get_minimum();

/** Returns current recommended minimum delay time
 *
 * Delays using the absolute minimum timer period are very imprecise.
 * This recommendations allows to use minimum delays with a good precision and low overhead
 * for current architecture and configuration.
 *
 * @return (t_base) minimum available delay period for current architecture and configuration (microseconds)
 */
t_base ttc_systick_delay_get_recommended();

/** Wait until given delay expires
 *
 * Note: If multitasking is enabled, CPU is given to other tasks while waiting. This may lead to inaccurate delays,
 *       especially for short delays.
 *       Enter a critical section before calling this function if CPU should not be given to other tasks.
 *       -> ttc_task_critical_begin();
 *
 * @param Delay   (t_ttc_systick_delay*)  !=NULL: pointer to allocated and pre-initialized delay data; ==NULL: a new delay is allocated on stack
 */
void ttc_systick_delay_wait( t_ttc_systick_delay* Delay );

/** Function creates new delay and waits until given time expires
 *
 * Note: If multitasking is enabled, CPU is given to other tasks while waiting. This may lead to inaccurate delays,
 *       especially for short delays.
 *       Enter a critical section before calling this function if CPU should not be given to other tasks.
 *       -> ttc_task_critical_begin();
 *
 * @param TimeUS  (t_base)                >0: Delay is initialized with given value; ==0: Delay is used unchanged
 */
void ttc_systick_delay_simple( t_base TimeUS );

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
/** Prototypes of low-level driver functions ****************************{
 *
 * Functions declared with prefix _driver_systick_ are passed to interfaces/ttc_systick_interface.h and to
 * all currently activated low-level drivers.
 * If a driver declaration is placed directly below its ttc_* pendant, it will share its documentation.
 * Driver declarations with different argument list should be bundled with their own documentation in this section.
 *
 * If you add a _driver_* prototype, use create_DeviceDriver.pl to automatically add empty functions
 * in all existing low-level drivers by issuing this inside git/TheToolChain/TTC-Library/:
 * cd templates/; ./create_DeviceDriver.pl systick UPDATE
 */


/** Returns current system clock value
 *
 * Note: Unit of returned value hardware dependent. Use ttc_systick_get_us_ticks() to convert
 * Note: Check configuration of your scheduler to achieve real world usecs! (E.g. FreeRTOSConfig.h:configTICK_RATE_HZ)
 * Note: See ttc_systick_delay_expired() for delays that work with multiple tasks in single- and multitasking setup!
 *
 * Example usage:
 *
 * // Need to store start time in case of variable overrun
 * t_base TimeNow = ttc_systick_get_elapsed_ticks();
 *
 * // Calculate wake up time 1 ms in the future in ticks (1 tick is NOT a microsecond)
 * t_base TimeWakeUp = TimeNow + ttc_systick_get_us_ticks(1000);
 *
 * if (TimeWakeUp < TimeNow) { // handle overrun
 *
 *   // wait until system counter overruns
 *   while (ttc_systick_get_elapsed_ticks() > TimeWakeUp)
 *     ttc_task_yield(); // give cpu to other tasks
 * }
 * while (ttc_systick_get_elapsed_ticks() < TimeWakeUp)
 *   ttc_task_yield(); // give cpu to other tasks
 *
 * 1ms has passed ...
 *
 * @return amount of counter events (ticks) since start of system/ scheduler (may have been overrun since last call!)
 */
t_base ttc_systick_get_elapsed_ticks();
t_base _driver_systick_get_elapsed_ticks();
#define ttc_systick_get_elapsed_ticks  _driver_systick_get_elapsed_ticks  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Returns current system clock value for an interrupt service routine
 *
 * During an interrupt service routine, the systick counter may not be running.
 * This function will provide similar functionality as ttc_systick_get_elapsed_ticks().
 * Normally the only reason to know the system time is if you want to wait inside an
 * interrupt service routine. This "feature" was required during development of the radio_dw1000 driver.
 * This driver may use very long interrupt service routines with precise timeouts.
 *
 * @return amount of counter events (ticks) since start of system/ scheduler (may have been overrun since last call!)
 */
t_base ttc_systick_get_elapsed_ticks_isr();
t_base _driver_systick_get_elapsed_ticks_isr();
#define ttc_systick_get_elapsed_ticks_isr  _driver_systick_get_elapsed_ticks_isr  // enable to directly forward function call to low-level driver (remove function definition before!)

/** Calculates amount of system clock ticks that correspond to 1 microseconds
 *
 * Using hardware ticks instead of microseconds allows to write faster delay functions:
 * 1) Calculate amount of ticks to pass that correspond to desired microseconds delay (requires multiplications)
 * 2) Wait until amount of calculated ticks has passed (no further multiplications required)
 *
 * Note: Check configuration of low-level driver to achieve real world usecs! (E.g. FreeRTOSConfig.h:configTICK_RATE_HZ)
 *
 * @param Microseconds (t_base) time period in microseconds
 * @return             (t_base) amount of counter events (ticks) since start of system/ scheduler
 */
t_base ttc_systick_get_us_ticks( t_base Microseconds );
t_base _driver_systick_get_us_ticks( t_base Microseconds );
#define ttc_systick_get_us_ticks  _driver_systick_get_us_ticks  // enable to directly forward function call to low-level driver (remove function definition before!)

/** returns current system clock time in microseconds
 *
 * Note: Every call to this function implies a multiplication. See ttc_systick_get_elapsed_ticks() for a faster implementation!
 * Note: Check configuration of current low-level driver to achieve real world usecs! (E.g. FreeRTOSConfig.h:configTICK_RATE_HZ)
 * Note: See ttc_systick_delay_expired() for delays that work with multiple tasks in single- and multitasking setup!
 *
 * @return amount of usecs since start of system/ scheduler (may have been overrun since last call!)
 */
t_base ttc_systick_get_elapsed_usecs();
t_base _driver_systick_get_elapsed_usecs();
#define ttc_systick_get_elapsed_usecs  _driver_systick_get_elapsed_usecs  // enable to directly forward function call to low-level driver (remove function definition before!)


/** Checks + corrects entries in given Config to meet limits of current architecture
 *
 * Note: High-level function ttc_systick_configuration_check() has already checked Config against
 *       all fields being found in t_ttc_systick_features.
 *
 * @param Config        Configuration of systick device
 * @return              Fields of *Config have been adjusted if necessary
 */
void _driver_systick_configuration_check( t_ttc_systick_config* Config );

/** shutdown single SYSTICK unit device
 * @param Config        Configuration of systick device
 * @return              == 0: SYSTICK has been shutdown successfully; != 0: error-code
 */
e_ttc_systick_errorcode _driver_systick_deinit( t_ttc_systick_config* Config );

/** initializes single SYSTICK unit for operation
 * @param Config        Configuration of systick device
 * @return              == 0: SYSTICK has been initialized successfully; != 0: error-code
 */
e_ttc_systick_errorcode _driver_systick_init( t_ttc_systick_config* Config );

/** loads configuration of indexed SYSTICK unit with default values
 * @param Config        Configuration of systick device
 * @return  == 0:       configuration was loaded successfully
 */
e_ttc_systick_errorcode _driver_systick_load_defaults( t_ttc_systick_config* Config );

/** reset configuration of indexed device and connected hardware
 *
 * @param Config        Configuration of systick device
 */
void _driver_systick_reset( t_ttc_systick_config* Config );
//InsertDriverPrototypes above (DO NOT DELETE THIS LINE!)

//}

#endif //TTC_SYSTICK_H
