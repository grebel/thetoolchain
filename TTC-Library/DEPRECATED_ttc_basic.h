/** ttc_basic.h ***********************************************{
 *
 * TheToolChain.com
 *
 * Written by Gregor Rebel 2010-2012
 *
 * Basic datatypes, enums and functions
 * 
}*/

#ifndef TTC_BASIC_H
#define TTC_BASIC_H

#include "ttc_basic_types.h"

// basic macros required even by low-level drivers
// using these macros is faster than calling Assert_On_False() directly

#define Assert(Condition, ErrorCode)  if (! (Condition) ) Assert_Halt_EC(ErrorCode);
#define Assert_NULL(Pointer)          if (Pointer==NULL)  Assert_Halt_NULL();

#ifndef TTC_ASSERT_BASIC    // any previous definition set (Makefile)?
#define TTC_ASSERT_BASIC 1  // asserts in _basic.c files are enabled by default
#endif
#if (TTC_ASSERT_BASIC == 1)  // use Assert()s in BASIC code (somewhat slower but alot easier to debug)
  #define Assert_Basic(Condition, ErrorCode) Assert(Condition, ErrorCode)
#else  // us no Assert()s in TASK code (somewhat smaller + faster, but crashes are hard to debug)
  #define Assert_Basic(Condition, ErrorCode)
#endif
#ifdef TARGET_ARCHITECTURE_CM0
#include "cm3/cm3_basic.h"
#endif
#ifdef TARGET_ARCHITECTURE_CM3
#include "cm3/cm3_basic.h"
#endif
#ifdef TARGET_ARCHITECTURE_STM32W1xx
#include "stm32w/stm32w_basic.h"
#endif
#ifdef TARGET_ARCHITECTURE_STM32F1xx
#include "stm32/stm32_basic.h"
#endif
#ifdef TARGET_ARCHITECTURE_STM32L1xx
#include "stm32l1/stm32l1_basic.h"
#endif
#ifndef Base_t
  #error No architecture definition for Base_t!
#endif

#include "compile_options.h"  // contains all constant defines from makefile

/**{ basic macros *************************************************************
  *
  */

#ifndef abs
#define abs(VALUE)  ((VALUE < 0) ? -VALUE : VALUE)
#endif

/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * Not: This macro will automatically use native data-size for current architecture.
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
Base_t ttc_atomic_read_write(void* Address, Base_t NewValue);
#if TARGET_DATA_WIDTH == 32
#define ttc_atomic_read_write(Address, NewValue) ttc_atomic_read_write_32(Address, NewValue)
#endif
#if TARGET_DATA_WIDTH == 16
#define ttc_atomic_read_write(Address, NewValue) ttc_atomic_read_write_16(Address, NewValue)
#endif
#if TARGET_DATA_WIDTH == 8
#define ttc_atomic_read_write(Address, NewValue) ttc_atomic_read_write_8(Address, NewValue)
#endif
#ifndef ttc_atomic_read_write
#error TARGET_DATA_WIDTH not defined!
#endif

/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
Base_t ttc_atomic_read_write_32(Base_t* Address, Base_t NewValue);
#ifdef _driver_atomic_read_write_32
#define ttc_atomic_read_write_32(Address, NewValue) _driver_atomic_read_write_32(Address, NewValue)
#else
#warning Missing low-level implementation for ttc_atomic_read_write_32()!
#define ttc_atomic_read_write_32(Address, NewValue) ttc_atomic_read_write_32(Address, NewValue)
#endif

/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
Base_t ttc_atomic_read_write_16(Base_t* Address, Base_t NewValue);
#ifdef _driver_atomic_read_write_16
#define ttc_atomic_read_write_16(Address, NewValue) _driver_atomic_read_write_16(Address, NewValue)
#else
#warning Missing low-level implementation for ttc_atomic_read_write_16()!
#define ttc_atomic_read_write_16(Address, NewValue) ttc_atomic_read_write_16(Address, NewValue)
#endif

/** Atomic read+write operation required for thread safe memory operations (E.g. for mutexes, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
Base_t ttc_atomic_read_write_8(Base_t* Address, Base_t NewValue);
#ifdef _driver_atomic_read_write_8
#define ttc_atomic_read_write_8(Address, NewValue) _driver_atomic_read_write_8(Address, NewValue)
#else
#warning Missing low-level implementation for ttc_atomic_read_write_8()!
#define ttc_atomic_read_write_8(Address, NewValue) ttc_atomic_read_write_8(Address, NewValue)
#endif

/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * Not: This macro will automatically use native data-size for current architecture.
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
Base_t ttc_atomic_add(void* Address, Base_t NewValue);
#if TARGET_DATA_WIDTH == 32
#define ttc_atomic_add(Address, NewValue) ttc_atomic_add_32(Address, NewValue)
#endif
#if TARGET_DATA_WIDTH == 16
#define ttc_atomic_add(Address, NewValue) ttc_atomic_add_16(Address, NewValue)
#endif
#if TARGET_DATA_WIDTH == 8
#define ttc_atomic_add(Address, NewValue) ttc_atomic_add_8(Address, NewValue)
#endif
#ifndef ttc_atomic_add
#error TARGET_DATA_WIDTH not defined!
#endif

/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
Base_t ttc_atomic_add_32(Base_t* Address, Base_t NewValue);
#ifdef _driver_atomic_add_32
#define ttc_atomic_add_32(Address, NewValue) _driver_atomic_add_32(Address, NewValue)
#else
#warning Missing low-level implementation for ttc_atomic_add_32()!
#define ttc_atomic_add_32(Address, NewValue) ttc_atomic_add_32(Address, NewValue)
#endif

/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
Base_t ttc_atomic_add_16(Base_t* Address, Base_t NewValue);
#ifdef _driver_atomic_add_16
#define ttc_atomic_add_16(Address, NewValue) _driver_atomic_add_16(Address, NewValue)
#else
#warning Missing low-level implementation for ttc_atomic_add_16()!
#define ttc_atomic_add_16(Address, NewValue) ttc_atomic_add_16(Address, NewValue)
#endif

/** Atomic add/subtract operation required for thread safe memory operations (E.g. for semaphores, ...)
  *
  * @param Address  memory location to read and write
  * @param NewValue *Address = NewValue
  * @return         previous value of *Address
  */
Base_t ttc_atomic_add_8(Base_t* Address, Base_t NewValue);
#ifdef _driver_atomic_add_8
#define ttc_atomic_add_8(Address, NewValue) _driver_atomic_add_8(Address, NewValue)
#else
#warning Missing low-level implementation for ttc_atomic_add_8()!
#define ttc_atomic_add_8(Address, NewValue) ttc_atomic_add_8(Address, NewValue)
#endif

/** Default atomic implementations (thread safe but can be interrupted by interrupt service routines!)
  */
u32_t _ttc_atomic_read_write_32(u32_t* Address, u32_t NewValue);
u16_t _ttc_atomic_read_write_16(u16_t* Address, u16_t NewValue);
u8_t  _ttc_atomic_read_write_8(u8_t* Address, u8_t NewValue);
u32_t _ttc_atomic_add_32(s32_t* Address, s32_t Amount);
u16_t _ttc_atomic_add_16(s16_t* Address, s16_t Amount);
u8_t  _ttc_atomic_add_8(s8_t* Address, s8_t Amount);

//}
/** { macros for online task debugging ****************************************
  *
  * All task debugging macros start with Task_.
  * You may use double tab-key in GDB to get a full list in every source that includes ttc_task.h
  */

//{TASKS_MACROS
/** get information about current task
  *
  * Note: This function is specially designed for online debugging.
  *        To find out more about your current task, you may want to issue this in your gdb session:
  *        (gdb) p Tasks_This
  */
#define Tasks_This          (* ttc_task_update_info(NULL, NULL) )

/** get information about single task
  *
  * Note: This function is specially designed for online debugging.
  *        To find out more about your current task, you may want to issue this in your gdb session:
  *        (gdb) p Tasks_Other(HANDLE)
  *
  * @param TASK_HANDLE   handle of task as returned by ttc_task_create()
  */
#define Tasks_Other(TASK_HANDLE) (* ttc_task_update_info(TASK_HANDLE, NULL) )

/** get information about single task
  *
  * Note: This function is specially designed for online debugging.
  *        To find out more about your current task, you may want to issue this in your gdb session:
  *        E.g.: show summary of all tasks
  *        (gdb) p Tasks_All
  *        E.g.: show first running task (array index = 0..Tasks_All.AmountRunning)
  *        (gdb) p * Tasks_All.Running[0]
  *
  * @param TASK_HANDLE   handle of task as returned by ttc_task_create()
  */
#define Tasks_All         (* ttc_task_get_all(NULL) )

/** get information about single task
  *
  * Note: This function is specially designed for online debugging.
  *        To find out more about your current task, you may want to issue this in your gdb session:
  *        (gdb) p Tasks_Amount
  *
  * @param TASK_HANDLE   handle of task as returned by ttc_task_create()
  */
#define Tasks_Amount      ttc_task_get_amount()

//}TASKS_MACROS

//}macros
//{ Function prototypes *******************************************************


/** Will block endlessly. Use it to check arguments and system states.
 *
 * @param ErrorCode  developers hint why condition has failed
 */
void Assert_Halt_EC(volatile ErrorCode_e ErrorCode);

/** Will block endlessly. Use it to check arguments and system states.
 *
 * Note: This function is only a helper where macro Assert() cannot be used!
 *
 * @param Condition !=0 <=> TRUE; FALSE otherwise
 * @return value of Condition
 */
bool Assert_On_False(BOOL Condition);

/** calls Assert_Halt_EC(ec_NULL); (less code-overhead than Assert_Halt_EC() )
 */
void Assert_Halt_NULL();

/** Assert function for STM32 Standard Peripheral Library
 *
 * Note: Set define USE_FULL_ASSERT 1 to enable asserts in your stm32xxxx_conf.h file.
 */
void assert_failed(u8_t* File, u32_t Line);

/** basic hardware initialization (required by most microcontrollers)
 */
void ttc_hardware_init();

/** Used for automatic code tests.
 *
 * Call from a place in your code that must be reached to successfully pass the test.
 * Place a breakpoint inside ttc_Testpoint() to check if your test passes.
 *
 */
void ttc_testpoint();

//}Function prototypes

//? #include "ttc_sysclock.h"     // required to initialize system clocks

#endif
