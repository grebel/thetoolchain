#!/bin/bash
#
#  Install script for ST Standard Peripherals Library
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="200"
EXTENSION_SHORT="stm32f1xx_std_peripherals"
EXTENSION_PREFIX="cpu"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
DocFolder="uC/STM/StandardPeripheralsLibrary"
if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/        CD00004585.pdf STANDARD_PERIPHERAL_USERS_MANUAL_V1.1.pdf                             uC/STM32F1xx/StdPeripheralLibrary/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00201397.pdf AN2790_TFT_LCD_interfacing_with_the_high-density_STM32F10xxx_FSMC.pdf uC/STM32F1xx/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}


if [ ! -e OK.Install ]; then #{

  if [ ! -e OK.Packages ]; then #{
    installPackage libgmp3-dev
    installPackage libmpfr-dev
    installPackage xchm
    installPackage chmlib
    installPackage chmsee
    installPackage kchmviewer
    touch OK.Packages
  fi #}
  
  Archive="stm32f10x_stdperiph_lib.zip"
  # getFile http://www.st.com/stonline/products/support/micro/files/ $Archive
  getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ $Archive
  
  # find name ofu only directory
  # std_peripherals_library/STM32F10x_StdPeriph_Lib_V3.4.0/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h
  
  unZIP STM32F10x_StdPeriph_Lib $Archive || mv -f $Archive ${Archive}_bad
  rm current 2>/dev/null
  LibraryDir=`find ./  -maxdepth 1 -mindepth 1 -type d -print`
  createLink $LibraryDir current
  cd current
  for File in `ls *.chm`; do
    addDocumentationFile "$File" $DocFolder
  done
  
  File="Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h"
  if [ -e ${File}_orig ]; then
      mv -f ${File}_orig $File 
  fi
  mv -f $File ${File}_orig
  
  echo "  creating macro assert_param in $File"
  Note="  Note: Manually remove assert_param from your stm32f10x_conf.h !"
  echo $Note
  addLine ../Notes.txt $Note
  addLine ../Notes.txt "StdPeripheralLibrary-> http://www.mikrocontroller.net/articles/STM32F10x_Standard_Peripherals_Library  (german) folder InstallData/documentation"
  
  DestFolder="Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x"
  if [ ! -e $DestFolder/system_stm32f10x.c ]; then
    # system_stm32f10x.c is required to be found by the compiler so we put it aside its header file
    cp -v Project/STM32F10x_StdPeriph_Template/system_stm32f10x.c $DestFolder
    addLine ../Notes.txt "system_stm32f10x.c moved beside its header file (-> https://my.st.com/public/STe2ecommunities/mcu/Lists/ARM%20CortexM3%20STM32/Flat.aspx?RootFolder=%2Fpublic%2FSTe2ecommunities%2Fmcu%2FLists%2FARM%20CortexM3%20STM32%2FMissing%20file%20in%20new%203.4.0%20libraries%20system_stm32f10x.c&FolderCTID=0x01200200770978C69A1141439FE559EB459D758000626BE2B829C32145B9EB5739142DC17E&currentviews=320)"
  fi

  cat >script.sed <<"END_OF_TEXT"
/#include <stdint.h>/a\
#ifdef  USE_FULL_ASSERT\n
\n
// This assert_param macro has been copied here from stm32f10x_conf.h to\n
// circumvent gcc compiler errors.\n
\n
/**\n
  * @brief  The assert_param macro is used for function's parameters check.\n
  * @param  expr: If expr is false, it calls assert_failed function\n
  *   which reports the name of the source file and the source\n
  *   line number of the call that failed.\n 
  *   If expr is true, it returns no value.\n
  * @retval None\n
  */\n
  #define assert_param(expr) ((expr) ? (void)0 : assert_failed((t_u8 *)__FILE__, __LINE__))\n
  void assert_failed(t_u8* file, t_u32 line);\n
#else\n
  #define assert_param(expr) ((void)0)\n
#endif /* USE_FULL_ASSERT */\n

END_OF_TEXT

  # concatenate all lines which contain \n (sed requires this)
  awk '/\\n/{printf "%s",$ScriptName;next}{print}' script.sed >script2.sed
  
  # insert lines after "#include <stdint.h>"
  sed -f script2.sed ${File}_orig >$File

  cd ..

  echo "" >OK.Install

fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then #{
  MyFolder="StdPeripheral_Examples"
  addDocumentationFolder current/Project/STM32F10x_StdPeriph_Examples uC/STM32F1xx/StdPeripheralLibrary/Examples
  addDocumentationFolder current/Utilities/STM32_EVAL/Common          uC/STM32F1xx/StdPeripheralLibrary/Common
  
  Name="${Install_Dir}"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

COMPILE_OPTS += -DEXTENSION_${Name}=1

INCLUDE_DIRS += -I additionals/$Name/STM32F10x_StdPeriph_Driver/inc/ \\
                -I additionals/$Name/CMSIS/CM3/DeviceSupport/ST/STM32F10x/ \\
                -I additionals/$Name/CMSIS/CM3/CoreSupport/

vpath %.c additionals/$Name/STM32F10x_StdPeriph_Driver/src/ \\
          additionals/$Name/CMSIS/CM3/DeviceSupport/ST/STM32F10x/

vpath %.s additionals/$Name/CMSIS/CM3/DeviceSupport/ST/STM32F10x/startup/gcc_ride7

MAIN_OBJS += stm32f10x_rcc.o \
             stm32f10x_gpio.o \
             stm32f10x_dma.o \
             stm32f10x_tim.o \
             stm32f10x_i2c.o \
             stm32f10x_usart.o \
             stm32f10x_exti.o \

#{ Startup codes for all members of STM32 device family

startup_stm32f10x_md.o:    startup_stm32f10x_md.s
startup_stm32f10x_cl.o:    startup_stm32f10x_cl.s
startup_stm32f10x_hd.o:    startup_stm32f10x_hd.s
startup_stm32f10x_hd_vl.o: startup_stm32f10x_hd_vl.s
startup_stm32f10x_ld.o:    startup_stm32f10x_ld.s
startup_stm32f10x_ld_vl.o: startup_stm32f10x_ld_vl.s
startup_stm32f10x_md_vl.o: startup_stm32f10x_md_vl.s
startup_stm32f10x_xl.o:    startup_stm32f10x_xl.s

#}
#{ define uController to use (required to include stm32f10x.h) ---------------

# activate only one uC!
# - Low-density devices are STM32F101xx, STM32F102xx and STM32F103xx microcontrollers
#   where the Flash memory density ranges between 16 and 32 Kbytes.
# - Low-density value line devices are STM32F100xx microcontrollers where the Flash
#   memory density ranges between 16 and 32 Kbytes.
# - Medium-density devices are STM32F101xx, STM32F102xx and STM32F103xx microcontrollers
#   where the Flash memory density ranges between 64 and 128 Kbytes.
# - Medium-density value line devices are STM32F100xx microcontrollers where the
#   Flash memory density ranges between 64 and 128 Kbytes.
# - High-density devices are STM32F101xx and STM32F103xx microcontrollers where
#   the Flash memory density ranges between 256 and 512 Kbytes.
# - High-density value line devices are STM32F100xx microcontrollers where the
#   Flash memory density ranges between 256 and 512 Kbytes.
# - XL-density devices are STM32F101xx and STM32F103xx microcontrollers where
#   the Flash memory density ranges between 512 and 1024 Kbytes.
# - Connectivity line devices are STM32F105xx and STM32F107xx microcontrollers.
#
# Examples for some prototype boards
# Olimex STM32-P107  DSTM32F10X_HD
# Olimex STM32-H107  DSTM32F10X_HD
# Olimex STM32-LCD   DSTM32F10X_CL
#
#STM32F10X_LD   =1       # Low density devices
#STM32F10X_LD_VL=1       # Low density Value Line devices
#STM32F10X_MD   =1       # Medium density devices
#STM32F10X_MD_VL=1       # Medium density Value Line devices
#STM32F10X_HD   =1       # High density devices
#STM32F10X_HD_VL=1       # High density value line devices
#STM32F10X_XL   =1       # XL-density devices
#STM32F10X_CL   =1       # Connectivity line devices

ifdef STM32F10X_LD
  ifdef uCONTROLLER
    ERROR: STM32F10X_LD=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_LD
  COMPILE_OPTS += -DSTM32F10X_LD
  MAIN_OBJS += startup_stm32f10x_ld.o
endif
ifdef STM32F10X_LD_VL
  ifdef uCONTROLLER
    ERROR: STM32F10X_LD_VL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_LD_VL
  COMPILE_OPTS += -DSTM32F10X_LD_VL
  MAIN_OBJS += startup_stm32f10x_ld_vl.o
endif
ifdef STM32F10X_MD
  ifdef uCONTROLLER
    ERROR: STM32F10X_MD=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_MD
  COMPILE_OPTS += -DSTM32F10X_MD
  MAIN_OBJS += startup_stm32f10x_cl.o
endif
ifdef STM32F10X_MD_VL
  ifdef uCONTROLLER
    ERROR: STM32F10X_MD_VL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_MD_VL
  COMPILE_OPTS += -DSTM32F10X_MD_VL
  MAIN_OBJS += startup_stm32f10x_md.o
endif
ifdef STM32F10X_HD
  ifdef uCONTROLLER
    ERROR: STM32F10X_HD=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_HD
  COMPILE_OPTS += -DSTM32F10X_HD
  MAIN_OBJS += startup_stm32f10x_hd.o
endif
ifdef STM32F10X_HD_VL
  ifdef uCONTROLLER
    ERROR: STM32F10X_HD_VL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_HD_VL
  COMPILE_OPTS += -DSTM32F10X_HD_VL
  MAIN_OBJS += startup_stm32f10x_hd_vl.o
endif
ifdef STM32F10X_XL
  ifdef uCONTROLLER
    ERROR: STM32F10X_XL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_XL
  COMPILE_OPTS += -DSTM32F10X_XL
  MAIN_OBJS += startup_stm32f10x_xl.o
endif
ifdef STM32F10X_CL
  ifdef uCONTROLLER
    ERROR: STM32F10X_CL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_CL
  COMPILE_OPTS += -DSTM32F10X_CL
  MAIN_OBJS += startup_stm32f10x_cl.o
endif

#}

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} #{

  # add createLinks for each link required for this extension
  echo "createLink \$ScriptPath/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET" >>$ActivateScriptFile
  
  createActivateScriptTail $Name ${Dir_Extensions} 
  #}

  addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; createLink \$Source/TheToolChain/InstallData/$Install_Dir/current/Libraries/  $Install_Dir"
  echo "Installed successfully: $Install_Dir"
#}
else
  echo "failed to install $Install_Dir"
  exit 10
fi

#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
