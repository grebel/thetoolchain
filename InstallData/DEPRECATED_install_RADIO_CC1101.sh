#!/bin/bash
#
#
#  Install script for cc1101 radio support 
#
#  Initial Script written by Gregor Rebel 2010-2018.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="400"
EXTENSION_SHORT="radio_cc1101_spi"
EXTENSION_PREFIX="ttc"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

  Name="$Install_Dir"
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# append source-folder for includes + source codes
INCLUDE_DIRS += -Ittc-lib/radio/
vpath %.c ttc-lib/radio/

# external radio connected via SPI
MAIN_OBJS += radio_cc1101_spi.o

# define size of header, footer and payload required for each network packet (required by ttc_radio_types.h)

RADIO_CC1101_HEADER_SIZE  = 1
RADIO_CC1101_FOOTER_SIZE  = 0
RADIO_CC1101_PAYLOAD_SIZE = 60

COMPILE_OPTS += -DRADIO_CC1101_HEADER_SIZE=\$(RADIO_CC1101_HEADER_SIZE)
COMPILE_OPTS += -DRADIO_CC1101_FOOTER_SIZE=\$(RADIO_CC1101_FOOTER_SIZE)
COMPILE_OPTS += -DRADIO_CC1101_PAYLOAD_SIZE=\$(RADIO_CC1101_PAYLOAD_SIZE)

#{ ensure that header size is large egnough to store cc1101 headers
ifndef TTC_NETWORK_MAX_HEADER_SIZE
  TTC_NETWORK_MAX_HEADER_SIZE=\$(RADIO_CC1101_HEADER_SIZE)
endif
INCREASE_SIZE := \$(shell echo \${TTC_NETWORK_MAX_HEADER_SIZE}\<\${TTC_NETWORK_MAX_HEADER_SIZE} | bc)
ifeq "\$(INCREASE_SIZE)" "1"
  undef TTC_NETWORK_MAX_HEADER_SIZE
  TTC_NETWORK_MAX_HEADER_SIZE=\$(RADIO_CC1101_HEADER_SIZE)
endif
#}
#{ ensure that footer size is large egnough to store cc1101 footers
ifndef TTC_NETWORK_MAX_FOOTER_SIZE
  TTC_NETWORK_MAX_FOOTER_SIZE=\$(RADIO_CC1101_FOOTER_SIZE)
endif
INCREASE_SIZE := \$(shell echo \${TTC_NETWORK_MAX_FOOTER_SIZE}\<\${RADIO_CC1101_FOOTER_SIZE} | bc)
ifeq "\$(INCREASE_SIZE)" "1"
  undef TTC_NETWORK_MAX_FOOTER_SIZE
  TTC_NETWORK_MAX_FOOTER_SIZE=\$(RADIO_CC1101_FOOTER_SIZE)
endif
#}
#{ ensure that payload size is large egnough to store cc1101 payloads
ifndef TTC_NETWORK_MAX_PAYLOAD_SIZE
  TTC_NETWORK_MAX_PAYLOAD_SIZE=\$(RADIO_CC1101_PAYLOAD_SIZE)
endif
INCREASE_SIZE := \$(shell echo \${TTC_NETWORK_MAX_PAYLOAD_SIZE}\<\${RADIO_CC1101_PAYLOAD_SIZE} | bc)
ifeq "\$(INCREASE_SIZE)" "1"
  undef TTC_NETWORK_MAX_PAYLOAD_SIZE
  TTC_NETWORK_MAX_PAYLOAD_SIZE=\$(RADIO_CC1101_PAYLOAD_SIZE)
endif
#}

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Architecture independent support for Universal Synchronous Asynchronous Serial Receiver Transmitter (RADIO)" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_Additionals/${Name}_*

# create links into extensions.active/
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
  echo "Installed successfully: $Name"

  for Index in 1 2 3 4 5; do    
    Name="150_board_extension_radio_${Index}_cc1101"
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

#{ TTC_RADIO${Index} pin configuration for  external radio transceiver #_${Index} cc1101 connected via SPI bus

# set type of driver to use (one from ttc_radio_types.h/e_ttc_radio_driver)
COMPILE_OPTS += -DTTC_RADIO${Index}=trd_cc1101_spi

# define spi interface to use (only required if connected via SPI)
COMPILE_OPTS += -DTTC_RADIO${Index}_SPI_INDEX=1

# define usart interface to use (only required if connected via USART)
#COMPILE_OPTS += -DTTC_RADIO${Index}_USART=TTC_USART1

# define pin configuration of GPIOs used for first radio
COMPILE_OPTS += -DTTC_RADIO${Index}_PIN_SPI_NSS=E_ttc_gpio_pin_c9
COMPILE_OPTS += -DTTC_RADIO${Index}_PIN_GDO0=E_ttc_gpio_pin_c13
COMPILE_OPTS += -DTTC_RADIO${Index}_PIN_GDO1=E_ttc_gpio_pin_c11
COMPILE_OPTS += -DTTC_RADIO${Index}_PIN_GDO2=E_ttc_gpio_pin_c8

#}TTC_RADIO${Index}

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Example extension for external radio transceiver cc1101 connected via SPI bus" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.600_example_*

activate.400_radio_cc1101_spi.sh QUIET \"\$ScriptName\"
activate.500_ttc_usart.sh        QUIET \"\$ScriptName\"
activate.500_ttc_radio.sh        QUIET \"\$ScriptName\"
activate.500_ttc_gpio.sh         QUIET \"\$ScriptName\"
activate.500_ttc_spi.sh          QUIET \"\$ScriptName\"

# create links into extensions.active/
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
    echo "Installed successfully: $Name"
  done

    Name="150_board_extension_dualradio_cc1101_cc1190"
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

#{ TTC_RADIO1 pin configuration for  external radio transceiver #1 cc1101 connected via SPI bus

# set type of driver to use (one from ttc_radio_types.h/e_ttc_radio_driver)
COMPILE_OPTS += -DTTC_RADIO1=trd_cc1101_spi

# define spi interface to use (only required if connected via SPI)
COMPILE_OPTS += -DTTC_RADIO1_INDEX_SPI_INDEX=1

# define pin configuration of GPIOs used for first radio
COMPILE_OPTS += -DTTC_RADIO1_PIN_SPI_NSS=E_ttc_gpio_pin_c9
COMPILE_OPTS += -DTTC_RADIO1_PIN_GDO0=E_ttc_gpio_pin_c13
COMPILE_OPTS += -DTTC_RADIO1_PIN_GDO1=E_ttc_gpio_pin_c11
COMPILE_OPTS += -DTTC_RADIO1_PIN_GDO2=E_ttc_gpio_pin_c8

#}TTC_RADIO1
#{ TTC_RADIO2 pin configuration for  external radio transceiver #2 cc1101 connected via SPI bus

# set type of driver to use (one from ttc_radio_types.h/e_ttc_radio_driver)
COMPILE_OPTS += -DTTC_RADIO2=trd_cc1101_spi

# define spi interface to use (only required if connected via SPI)
COMPILE_OPTS += -DTTC_RADIO2_INDEX_SPI_INDEX=1

# define usart interface to use (only required if connected via USART)
#COMPILE_OPTS += -DTTC_RADIO2_USART=TTC_USART1

# define pin configuration of GPIOs used for first radio
COMPILE_OPTS += -DTTC_RADIO2_PIN_SPI_NSS=E_ttc_gpio_pin_b0
COMPILE_OPTS += -DTTC_RADIO2_PIN_GDO0=E_ttc_gpio_pin_b5
COMPILE_OPTS += -DTTC_RADIO2_PIN_GDO1=E_ttc_gpio_pin_c11
COMPILE_OPTS += -DTTC_RADIO2_PIN_GDO2=E_ttc_gpio_pin_b1

#}TTC_RADIO2
#{ TTC_RADIO2_AMPLIFIER pin configuration for amplifier of first radio transceiver
#
# example configuration of a cc1190 radio amplifier as an extension board
# connect amplifier exactly like described here or define these constants in your extensions.local/makefile.700_extra_settings

# set type of driver to use (one from ttc_radio_types.h/e_ttc_radio_driver)
COMPILE_OPTS += -DTTC_RADIO2_AMPLIFIER=trd_cc1190

# pin configurations below are highly dependent on current low-level driver (here cc1190)
COMPILE_OPTS += -DTTC_RADIO2_AMPLIFIER_HGM=E_ttc_gpio_pin_c0
COMPILE_OPTS += -DTTC_RADIO2_AMPLIFIER_HGM_TYPE=E_ttc_gpio_mode_output_push_pull # -> ttc_gpio_types.h

COMPILE_OPTS += -DTTC_RADIO2_AMPLIFIER_PIN_LNA_ENABLE=E_ttc_gpio_pin_c2
COMPILE_OPTS += -DTTC_RADIO2_AMPLIFIER_PIN_LNA_ENABLE_TYPE=E_ttc_gpio_mode_output_push_pull # -> ttc_gpio_types.h/e_ttc_gpio_mode

COMPILE_OPTS += -DTTC_RADIO2_AMPLIFIER_TTC_GPIO_A_ENABLE=E_ttc_gpio_pin_c3
COMPILE_OPTS += -DTTC_RADIO2_AMPLIFIER_TTC_GPIO_A_ENABLE_TYPE=E_ttc_gpio_mode_output_push_pull # -> ttc_gpio_types.h/e_ttc_gpio_mode

#}TTC_RADIO2_AMPLIFIER

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Example extension for external radio transceiver cc1101 connected via SPI bus" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.600_example_*

activate.400_radio_cc1101_spi.sh QUIET \"\$ScriptName\"
activate.400_radio_cc1190.sh     QUIET \"\$ScriptName\"
activate.500_ttc_usart.sh        QUIET \"\$ScriptName\"
activate.500_ttc_radio.sh        QUIET \"\$ScriptName\"
activate.500_ttc_gpio.sh         QUIET \"\$ScriptName\"
activate.500_ttc_spi.sh          QUIET \"\$ScriptName\"
activate.500_ttc_interrupt.sh    QUIET \"\$ScriptName\"

# create links into extensions.active/
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
    echo "Installed successfully: $Name"
  


cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
