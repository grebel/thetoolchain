#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="100"
EXTENSION_SHORT="wsn3_etrx357"
EXTENSION_PREFIX="board"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

  Dir=`pwd`

  Name="${Install_Dir}_no_pa"
  createExtensionMakefileHead ${Name} #{
  File="${Dir_Extensions}makefile.${Name}" 
  cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DTTC_BOARD=$Name
BOARD=$Name

#X STM32W108   = 1
#X COMPILE_OPTS += -DuCONTROLLER=STM32W108

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32w108ccu = 1

# set define required by stm32w108xx.h in STM32W108xx_SimpleMAC_V2.0.1/
COMPILE_OPTS += -DSTM32W108CB

# set define required by memmap_stm32w108.h in STM32W108xx_SimpleMAC_V1.1.0/
COMPILE_OPTS += -DCORTEXM3_STM32W108xB

# set define required by hal.h in STM32W108xx_SimpleMAC_V2.0.1/
COMPILE_OPTS += -DUSE_MB851_REVA_REVB


# set some well known port defines
# Note: Using the Constant names listed here makes it easier to reuse example code! 


#BoardRev10=1
#BoardRev11=1
BoardRev12=1

ifdef BoardRev10  #{
  # LED-pins (push pull output)
  COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c6
  COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_c7

  # Switches (input with pull up resistor)
  COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_b0   
  COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  COMPILE_OPTS += -DTTC_SWITCH2=E_ttc_gpio_pin_b6
  COMPILE_OPTS += -DTTC_SWITCH2_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h

  COMPILE_OPTS += -DTTC_SWITCH3=E_ttc_gpio_pin_a1
  COMPILE_OPTS += -DTTC_SWITCH3_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  COMPILE_OPTS += -DTTC_SWITCH4=E_ttc_gpio_pin_a2
  COMPILE_OPTS += -DTTC_SWITCH4_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
endif
#}
ifdef BoardRev11  #{
  # LED-pins (push pull output)
  COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c6
  COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_c7

  # Switches (input with pull up resistor)
  COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_b0   
  COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  COMPILE_OPTS += -DTTC_SWITCH2=E_ttc_gpio_pin_b6
  COMPILE_OPTS += -DTTC_SWITCH2_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h

  COMPILE_OPTS += -DTTC_SWITCH3=E_ttc_gpio_pin_a1
  COMPILE_OPTS += -DTTC_SWITCH3_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  COMPILE_OPTS += -DTTC_SWITCH4=E_ttc_gpio_pin_a2
  COMPILE_OPTS += -DTTC_SWITCH4_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
endif
#}
ifdef BoardRev12  #{
  # LED-pins (push pull output)
  COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c5
  COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_a7
  COMPILE_OPTS += -DPIN_CHARGE_EN=E_ttc_gpio_pin_b6
  COMPILE_OPTS += -DPIN_HIGHPOWER_EN=E_ttc_gpio_pin_b5
  COMPILE_OPTS += -DPIN_ADC_BATTERY=E_ttc_gpio_pin_b7
  COMPILE_OPTS += -DPIN_GPIO14=E_ttc_gpio_pin_b3
  COMPILE_OPTS += -DPIN_GPIO13=E_ttc_gpio_pin_b4
  COMPILE_OPTS += -DPIN_GPIO12=E_ttc_gpio_pin_a0
  COMPILE_OPTS += -DPIN_GPIO11=E_ttc_gpio_pin_a1
  COMPILE_OPTS += -DPIN_GPIO10=E_ttc_gpio_pin_a2
  COMPILE_OPTS += -DPIN_GPIO9=E_ttc_gpio_pin_a3
  COMPILE_OPTS += -DPIN_GPIO8=E_ttc_gpio_pin_a4
  COMPILE_OPTS += -DPIN_GPIO7=E_ttc_gpio_pin_a5
  COMPILE_OPTS += -DPIN_GPIO6=E_ttc_gpio_pin_a6
  COMPILE_OPTS += -DPIN_GPIO5=E_ttc_gpio_pin_b1
  COMPILE_OPTS += -DPIN_GPIO4=E_ttc_gpio_pin_b2
  COMPILE_OPTS += -DPIN_GPIO3=E_ttc_gpio_pin_b0
  COMPILE_OPTS += -DPIN_GPIO2=E_ttc_gpio_pin_c1
  COMPILE_OPTS += -DPIN_GPIO1=E_ttc_gpio_pin_b7
  COMPILE_OPTS += -DPIN_TIMER1=E_ttc_gpio_pin_a6
  COMPILE_OPTS += -DPIN_OSC32A=E_ttc_gpio_pin_c7
  COMPILE_OPTS += -DPIN_OSC32B=E_ttc_gpio_pin_c6
  
  # Switches (input with pull up resistor)
  COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_c1
  COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  COMPILE_OPTS += -DTTC_SWITCH2=E_ttc_gpio_pin_b0
  COMPILE_OPTS += -DTTC_SWITCH2_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h

  COMPILE_OPTS += -DTTC_SWITCH3=E_ttc_gpio_pin_a1
  COMPILE_OPTS += -DTTC_SWITCH3_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  COMPILE_OPTS += -DTTC_SWITCH4=E_ttc_gpio_pin_a2
  COMPILE_OPTS += -DTTC_SWITCH4_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
endif
#}

#{ RADIO1 - define radio transceiver
  COMPILE_OPTS += -DTTC_RADIO1=0
  COMPILE_OPTS += -DTTC_RADIO1_DRIVER=ta_radio_stm32w1xx
  COMPILE_OPTS += -DTTC_RADIO1_BOOST_MODE=0
  COMPILE_OPTS += -DTTC_RADIO1_ALTERNATE_TX_PATH=0
  COMPILE_OPTS += -DTTC_RADIO1_PIN_TX_ACTIVE=E_ttc_gpio_pin_c5
  COMPILE_OPTS += -DTTC_RADIO1_PIN_TX_LOWACTIVE=0
#}
#{ USART1 - define serial port #1
  COMPILE_OPTS += -DTTC_USART1=0
  COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_b1
  COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_b2
  COMPILE_OPTS += -DTTC_USART1_RTS=E_ttc_gpio_pin_b4 
  COMPILE_OPTS += -DTTC_USART1_CTS=E_ttc_gpio_pin_b3
#}USART1
#{ SPI1   - define serial peripheral interface port #1
  COMPILE_OPTS += -DTTC_SPI1=1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a0
  COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a1
  COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_a2
  COMPILE_OPTS += -DTTC_SPI1_NSS=E_ttc_gpio_pin_a3
#}SPI1
#{ I2C1   - (Inter-Integrated Circuit-) Interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1
  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b1  # CONN_30-16
  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b2  # CONN_30-15
#}I2C1
#{ I2C2   - (Inter-Integrated Circuit-) Interface #2
  COMPILE_OPTS += -DTTC_I2C2=ttc_device_2
  COMPILE_OPTS += -DTTC_I2C2_SDA=E_ttc_gpio_pin_a1  # CONN_30-22
  COMPILE_OPTS += -DTTC_I2C2_SCL=E_ttc_gpio_pin_a2  # CONN_30-21
#}I2C1

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Wireless Sensor Node rev 3 with EM357 without Power Amplifier"  #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
  
  enableFeature 450_cpu_stm32w1xx

  #X activate.200_cpu_stm32w1xx.sh ONLY simplemac110 QUIET "\$ScriptName"
  if [ "1" == "1" ]; then #{ activate SimpleMAC v1.1.0
    activate.251_stm32w1xx_simple_mac110.sh   QUIET "\$ScriptName"
    activate.253_stm32w1xx_hal110.sh          QUIET "\$ScriptName"
  #}
  else #{
    activate.251_stm32w1xx_simple_mac201.sh   QUIET "\$ScriptName"
    activate.253_stm32w1xx_hal201.sh          QUIET "\$ScriptName"

    activate.250_stm_std_peripherals__sc.sh   QUIET "\$ScriptName"
    activate.250_stm_std_peripherals__clk.sh  QUIET "\$ScriptName"
    activate.250_stm_std_peripherals__gpio.sh QUIET "\$ScriptName"
    activate.250_stm_std_peripherals__adc.sh  QUIET "\$ScriptName"
  fi #}

END_OF_ACTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  Name="${Install_Dir}_with_pa"
  createExtensionMakefileHead ${Name} #{
  File="${Dir_Extensions}makefile.${Name}" 
  cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DTTC_BOARD=$Name
BOARD=$Name

# uController to use
#X STM32W108   = 1
#X COMPILE_OPTS += -DuCONTROLLER=STM32W108

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32w108ccu = 1 

# set define required by stm32w108xx.h in STM32W108xx_SimpleMAC_V2.0.1/
COMPILE_OPTS += -DSTM32W108CB

# set define required by memmap_stm32w108.h in STM32W108xx_SimpleMAC_V1.1.0/
COMPILE_OPTS += -DCORTEXM3_STM32W108xB

# set define required by hal.h in STM32W108xx_SimpleMAC_V2.0.1/
COMPILE_OPTS += -DUSE_MB851_REVA_REVB

# set some well known port defines
# Note: Using the Constant names listed here makes it easier to reuse example code! 


#BoardRev10=1
#BoardRev11=1
BoardRev12=1

ifdef BoardRev10  #{
  # LED-pins (push pull output)
  COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c6
  COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_c7

  # Switches (input with pull up resistor)
  COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_b0   
  COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  COMPILE_OPTS += -DTTC_SWITCH2=E_ttc_gpio_pin_b6
  COMPILE_OPTS += -DTTC_SWITCH2_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h

  COMPILE_OPTS += -DTTC_SWITCH3=E_ttc_gpio_pin_a1
  COMPILE_OPTS += -DTTC_SWITCH3_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  COMPILE_OPTS += -DTTC_SWITCH4=E_ttc_gpio_pin_a2
  COMPILE_OPTS += -DTTC_SWITCH4_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
endif
#}
ifdef BoardRev11  #{
  # LED-pins (push pull output)
  COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c6
  COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_c7

  # Switches (input with pull up resistor)
  COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_b0   
  COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  COMPILE_OPTS += -DTTC_SWITCH2=E_ttc_gpio_pin_b6
  COMPILE_OPTS += -DTTC_SWITCH2_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h

  COMPILE_OPTS += -DTTC_SWITCH3=E_ttc_gpio_pin_a1
  COMPILE_OPTS += -DTTC_SWITCH3_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  COMPILE_OPTS += -DTTC_SWITCH4=E_ttc_gpio_pin_a2
  COMPILE_OPTS += -DTTC_SWITCH4_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
endif
#}
ifdef BoardRev12  #{
  # LED-pins (push pull output)
  COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c5
  COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_a7
  COMPILE_OPTS += -DPIN_CHARGE_EN=E_ttc_gpio_pin_b6
  COMPILE_OPTS += -DPIN_HIGHPOWER_EN=E_ttc_gpio_pin_b5
  COMPILE_OPTS += -DPIN_ADC_BATTERY=E_ttc_gpio_pin_b7
  COMPILE_OPTS += -DPIN_GPIO14=E_ttc_gpio_pin_b3
  COMPILE_OPTS += -DPIN_GPIO13=E_ttc_gpio_pin_b4
  COMPILE_OPTS += -DPIN_GPIO12=E_ttc_gpio_pin_a0
  COMPILE_OPTS += -DPIN_GPIO11=E_ttc_gpio_pin_a1
  COMPILE_OPTS += -DPIN_GPIO10=E_ttc_gpio_pin_a2
  COMPILE_OPTS += -DPIN_GPIO9=E_ttc_gpio_pin_a3
  COMPILE_OPTS += -DPIN_GPIO8=E_ttc_gpio_pin_a4
  COMPILE_OPTS += -DPIN_GPIO7=E_ttc_gpio_pin_a5
  COMPILE_OPTS += -DPIN_GPIO6=E_ttc_gpio_pin_a6
  COMPILE_OPTS += -DPIN_GPIO5=E_ttc_gpio_pin_b1
  COMPILE_OPTS += -DPIN_GPIO4=E_ttc_gpio_pin_b2
  COMPILE_OPTS += -DPIN_GPIO3=E_ttc_gpio_pin_b0
  COMPILE_OPTS += -DPIN_GPIO2=E_ttc_gpio_pin_c1
  COMPILE_OPTS += -DPIN_GPIO1=E_ttc_gpio_pin_b7
  COMPILE_OPTS += -DPIN_TIMER1=E_ttc_gpio_pin_a6
  COMPILE_OPTS += -DPIN_OSC32A=E_ttc_gpio_pin_c7
  COMPILE_OPTS += -DPIN_OSC32B=E_ttc_gpio_pin_c6
  
  # Switches (input with pull up resistor)
  COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_c1
  COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  COMPILE_OPTS += -DTTC_SWITCH2=E_ttc_gpio_pin_b0
  COMPILE_OPTS += -DTTC_SWITCH2_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h

  COMPILE_OPTS += -DTTC_SWITCH3=E_ttc_gpio_pin_a1
  COMPILE_OPTS += -DTTC_SWITCH3_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  COMPILE_OPTS += -DTTC_SWITCH4=E_ttc_gpio_pin_a2
  COMPILE_OPTS += -DTTC_SWITCH4_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
endif
#}

#{ RADIO1 - define radio transceiver
  COMPILE_OPTS += -DTTC_RADIO1=0
  COMPILE_OPTS += -DTTC_RADIO1_DRIVER=ta_radio_stm32w1xx
  COMPILE_OPTS += -DTTC_RADIO1_BOOST_MODE=0
  COMPILE_OPTS += -DTTC_RADIO1_ALTERNATE_TX_PATH=1
  COMPILE_OPTS += -DTTC_RADIO1_PIN_TX_ACTIVE=E_ttc_gpio_pin_c5
  COMPILE_OPTS += -DTTC_RADIO1_PIN_TX_LOWACTIVE=0
  COMPILE_OPTS += -DTTC_RADIO1_PIN_AMP_ACTIVE=E_ttc_gpio_pin_b0
  COMPILE_OPTS += -DTTC_RADIO1_PIN_AMP_LOWACTIVE=0
#}
#{ USART1 - define serial port #1
  COMPILE_OPTS += -DTTC_USART1=0
  COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_b1
  COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_b2
  COMPILE_OPTS += -DTTC_USART1_RTS=E_ttc_gpio_pin_b4 
  COMPILE_OPTS += -DTTC_USART1_CTS=E_ttc_gpio_pin_b3
#}USART1
#{ SPI1   - define serial peripheral interface port #1
  COMPILE_OPTS += -DTTC_SPI1=1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a0
  COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a1
  COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_a2
  COMPILE_OPTS += -DTTC_SPI1_NSS=E_ttc_gpio_pin_a3
#}SPI1
#{ I2C1   - (Inter-Integrated Circuit-) Interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1
  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b1  # CONN_30-16
  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b2  # CONN_30-15
#}I2C1
#{ I2C2   - (Inter-Integrated Circuit-) Interface #2
  COMPILE_OPTS += -DTTC_I2C2=ttc_device_2
  COMPILE_OPTS += -DTTC_I2C2_SDA=E_ttc_gpio_pin_a1  # CONN_30-22
  COMPILE_OPTS += -DTTC_I2C2_SCL=E_ttc_gpio_pin_a2  # CONN_30-21
#}I2C1

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Wireless Sensor Node rev 3 with EM357 with builtin Power Amplifier (LRS type module)"  #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
  
  enableFeature 450_cpu_stm32w1xx
  
  #X activate.200_cpu_stm32w1xx.sh ONLY simplemac110 QUIET "\$ScriptName"
  if [ "1" == "1" ]; then #{ activate SimpleMAC v1.1.0
    activate.251_stm32w1xx_simple_mac110.sh   QUIET "\$ScriptName"
    activate.253_stm32w1xx_hal110.sh          QUIET "\$ScriptName"
  #}
  else #{
    activate.251_stm32w1xx_simple_mac201.sh   QUIET "\$ScriptName"
    activate.253_stm32w1xx_hal201.sh          QUIET "\$ScriptName"

    activate.250_stm_std_peripherals__sc.sh   QUIET "\$ScriptName"
    activate.250_stm_std_peripherals__clk.sh  QUIET "\$ScriptName"
    activate.250_stm_std_peripherals__gpio.sh QUIET "\$ScriptName"
    activate.250_stm_std_peripherals__adc.sh  QUIET "\$ScriptName"
  fi #}

END_OF_ACTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  if [ "1" == "0" ]; then #{ moved installation of kernel module + rules.d entry to install_03_OpenOCD.sh

  BootLocal=""
  for File in /etc/rc.local /etc/init.d/boot.local; do 
    if [ -e "$File" ]; then
      BootLocal="$File"
    fi
  done
  echo "BootLocal='$BootLocal'"
  
  if [ "$BootLocal" != "" ]; then
    Line="rmmod ftdi_sio; modprobe ftdi_sio product=0xbafa vendor=0x0403; echo 0403 bafa >/sys/bus/usb-serial/drivers/ftdi_sio//new_id # added by $ScriptName for ToolChainSTM32"
    AlreadyPatched=`grep "$Line" $BootLocal`
    if [ "$AlreadyPatched" == "" ]; then #{
      cp $BootLocal boot.local
      replaceInFile boot.local "exit 0" "#exit 0"
      addLine boot.local $Line
      echo "patching file ${BootLocal}..."
      sudo cp boot.local $BootLocal
      sudo chown :root $BootLocal
    fi #}
  fi 
    Rules="45-openglink-permissions.rules"
    if [ ! -e "/etc/udev/rules.d/$Rules" ]; then #{ create udev-rule
      cat <<"END_OF_RULES" >$Rules #{

# Note: read info of current udev device
# udevadm info -a -p $(udevadm info -q path -n ttyUSB0)

SUBSYSTEMS=="usb-serial", DRIVERS=="ftdi_sio", SYMLINK+="ttyUSB_FTDI"

# OpenGLink Programming Adapter v1.1 (-> http://thetoolchain.com)
SUBSYSTEM=="usb", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="bafa", MODE="0664", GROUP="dialout", DRIVERS=="ftdi_sio", NAME="ttyWSN"

END_OF_RULES
#}
  
      sudo cp $Rules /etc/udev/rules.d/
      sudo udevadm control --reload-rules
    fi #}udev-rule
  fi #}
  
  echo "Installed successfully: $Install_Dir"

cd ..
rmdir 2>/dev/null $Install_Dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
