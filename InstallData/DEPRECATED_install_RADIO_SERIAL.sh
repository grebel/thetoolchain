#!/bin/bash
#
#
#  Install script for cc1120 radio support 
#
#  Initial Script written by Gregor Rebel 2010-2018.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="400"
EXTENSION_SHORT="serial"
EXTENSION_PREFIX="radio"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

  Name="$Install_Dir"
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

COMPILE_OPTS += -DRADIO_SERIAL_AMOUNT_INTERFACES=5
COMPILE_OPTS += -DRADIO_SERIAL_BLOCK_SIZE=50

COMPILE_OPTS += -DRADIO_SERIAL_BAUDRATE_DEF=115200
COMPILE_OPTS += -DRADIO_SERIAL_MAX_MEMORY_BUFFERS=5


# append source-folder for includes + source codes
INCLUDE_DIRS += -Ittc-lib/radio/
vpath %.c ttc-lib/radio/

# external radio connected via SPI
MAIN_OBJS += radio_serial.o 

# define size of header, footer and payload required for each network packet (required by ttc_radio_types.h)




END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Architecture independent support for Universal Synchronous Asynchronous Serial Receiver Transmitter (RADIO)" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_Additionals/${Name}_*

# create links into extensions.active/
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
  echo "Installed successfully: $Name"

  for Index in 1 2 3 4 5; do    
    Name="150_board_extension_radio_${Index}_serial"
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

COMPILE_OPTS += -DRADIO_SERIAL_USART_LOGICALINDEX${Index}=TTC_USART${Index}


#{ TTC_RADIO${Index} pin configuration for  external radio transceiver #_${Index} cc1120 connected via SPI bus

# set type of driver to use (one from ttc_radio_types.h/e_ttc_radio_driver)
COMPILE_OPTS += -DTTC_RADIO${Index}=trd_serial

# define usart interface to use (only required if connected via USART)
#COMPILE_OPTS += -DTTC_RADIO${Index}_USART_INDEX=TTC_USART1



#}TTC_RADIO${Index}

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Example extension for external radio transceiver connected via Serial Interface (USART)" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.600_example_*


activate.500_ttc_usart.sh        QUIET \"\$ScriptName\"
activate.500_ttc_radio.sh        QUIET \"\$ScriptName\"
activate.500_ttc_gpio.sh         QUIET \"\$ScriptName\"


# create links into extensions.active/
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
    echo "Installed successfully: $Name"
  done

    
cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
