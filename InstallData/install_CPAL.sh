#!/bin/bash

#
#  Install script for Communication Application Peripheral Library (CPAL) 
#  The CPAL library has been released by ST Microelectronics Corp. as
#  a high-level interface for the different standard peripheral libaries
#  for STM32 microcontrollers
#
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="270"
EXTENSION_SHORT="CPAL"
EXTENSION_NAME="$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library                     STM32F10x_Standard_Peripherals_Library.html                  STM
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/ CD00291090.pdf UM1029-Communication_peripheral_application_library_CPAL.pdf STM 
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

if [ ! -e OK.Install ];    then #{ download + install software
  if [ ! -e OK.Packages ]; then #{ install packages
    installPackage libgmp3-dev
    installPackage libmpfr-dev
    installPackage xchm
    installPackage chmlib
    installPackage chmsee
    installPackage kchmviewer
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir ..."
    Install_Path=`pwd`
    
    if [ "1" == "1" ]; then #{ download + unpack CPAL library for STM32F1, STM32F2, STM32L1
	    getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
	    unZIP "UNKNOWN" stm32_i2c_cpal.zip
	
	    Current=`find ./ -name STM32_I2C_CPAL* -type d | sort | tail -n 1`
	    if [ "$Current" != "" ]; then #{ create link CPAL
	      rm 2>/dev/null CPAL
	      createLink $Current CPAL
	    fi #}
	
	    cd CPAL/Libraries/CMSIS/CM3/CoreSupport/
	    
	    # fix compiler issue with arm-gcc4.7 due to invalid register use (bug in CMSIS)
	    # Ref=https://github.com/texane/stlink/issues/65
	    File="core_cm3.c"
	    chmod +w $File
	    mv $File ${File}_orig
	    cat <<"END_OF_PATCH" >${File} #{ legacy DOS file format cannot be here-script patched from UNIX format script
/**************************************************************************//**
 * @file     core_cm3.c
 * @brief    CMSIS Cortex-M3 Core Peripheral Access Layer Source File
 * @version  V1.30
 * @date     30. October 2009
 *
 * @note
 * Copyright (C) 2009 ARM Limited. All rights reserved.
 *
 * @par
 * ARM Limited (ARM) is supplying this software for use with Cortex-M 
 * processor based microcontrollers.  This file can be freely distributed 
 * within development tools that are supporting such ARM based processors. 
 *
 * @par
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * ARM SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 ******************************************************************************/

#include <stdint.h>

/* define compiler specific symbols */
#if defined ( __CC_ARM   )
  #define __ASM            __asm                                      /*!< asm keyword for ARM Compiler          */
  #define __INLINE         __inline                                   /*!< inline keyword for ARM Compiler       */

#elif defined ( __ICCARM__ )
  #define __ASM           __asm                                       /*!< asm keyword for IAR Compiler          */
  #define __INLINE        inline                                      /*!< inline keyword for IAR Compiler. Only avaiable in High optimization mode! */

#elif defined   (  __GNUC__  )
  #define __ASM            __asm                                      /*!< asm keyword for GNU Compiler          */
  #define __INLINE         inline                                     /*!< inline keyword for GNU Compiler       */

#elif defined   (  __TASKING__  )
  #define __ASM            __asm                                      /*!< asm keyword for TASKING Compiler      */
  #define __INLINE         inline                                     /*!< inline keyword for TASKING Compiler   */

#endif


/* ###################  Compiler specific Intrinsics  ########################### */

#if defined ( __CC_ARM   ) /*------------------RealView Compiler -----------------*/
/* ARM armcc specific functions */

/**
 * @brief  Return the Process Stack Pointer
 *
 * @return ProcessStackPointer
 *
 * Return the actual process stack pointer
 */
__ASM uint32_t __get_PSP(void)
{
  mrs r0, psp
  bx lr
}

/**
 * @brief  Set the Process Stack Pointer
 *
 * @param  topOfProcStack  Process Stack Pointer
 *
 * Assign the value ProcessStackPointer to the MSP 
 * (process stack pointer) Cortex processor register
 */
__ASM void __set_PSP(uint32_t topOfProcStack)
{
  msr psp, r0
  bx lr
}

/**
 * @brief  Return the Main Stack Pointer
 *
 * @return Main Stack Pointer
 *
 * Return the current value of the MSP (main stack pointer)
 * Cortex processor register
 */
__ASM uint32_t __get_MSP(void)
{
  mrs r0, msp
  bx lr
}

/**
 * @brief  Set the Main Stack Pointer
 *
 * @param  topOfMainStack  Main Stack Pointer
 *
 * Assign the value mainStackPointer to the MSP 
 * (main stack pointer) Cortex processor register
 */
__ASM void __set_MSP(uint32_t mainStackPointer)
{
  msr msp, r0
  bx lr
}

/**
 * @brief  Reverse byte order in unsigned short value
 *
 * @param   value  value to reverse
 * @return         reversed value
 *
 * Reverse byte order in unsigned short value
 */
__ASM uint32_t __REV16(uint16_t value)
{
  rev16 r0, r0
  bx lr
}

/**
 * @brief  Reverse byte order in signed short value with sign extension to integer
 *
 * @param   value  value to reverse
 * @return         reversed value
 *
 * Reverse byte order in signed short value with sign extension to integer
 */
__ASM int32_t __REVSH(int16_t value)
{
  revsh r0, r0
  bx lr
}


#if (__ARMCC_VERSION < 400000)

/**
 * @brief  Remove the exclusive lock created by ldrex
 *
 * Removes the exclusive lock which is created by ldrex.
 */
__ASM void __CLREX(void)
{
  clrex
}

/**
 * @brief  Return the Base Priority value
 *
 * @return BasePriority
 *
 * Return the content of the base priority register
 */
__ASM uint32_t  __get_BASEPRI(void)
{
  mrs r0, basepri
  bx lr
}

/**
 * @brief  Set the Base Priority value
 *
 * @param  basePri  BasePriority
 *
 * Set the base priority register
 */
__ASM void __set_BASEPRI(uint32_t basePri)
{
  msr basepri, r0
  bx lr
}

/**
 * @brief  Return the Priority Mask value
 *
 * @return PriMask
 *
 * Return state of the priority mask bit from the priority mask register
 */
__ASM uint32_t __get_PRIMASK(void)
{
  mrs r0, primask
  bx lr
}

/**
 * @brief  Set the Priority Mask value
 *
 * @param  priMask  PriMask
 *
 * Set the priority mask bit in the priority mask register
 */
__ASM void __set_PRIMASK(uint32_t priMask)
{
  msr primask, r0
  bx lr
}

/**
 * @brief  Return the Fault Mask value
 *
 * @return FaultMask
 *
 * Return the content of the fault mask register
 */
__ASM uint32_t  __get_FAULTMASK(void)
{
  mrs r0, faultmask
  bx lr
}

/**
 * @brief  Set the Fault Mask value
 *
 * @param  faultMask  faultMask value
 *
 * Set the fault mask register
 */
__ASM void __set_FAULTMASK(uint32_t faultMask)
{
  msr faultmask, r0
  bx lr
}

/**
 * @brief  Return the Control Register value
 * 
 * @return Control value
 *
 * Return the content of the control register
 */
__ASM uint32_t __get_CONTROL(void)
{
  mrs r0, control
  bx lr
}

/**
 * @brief  Set the Control Register value
 *
 * @param  control  Control value
 *
 * Set the control register
 */
__ASM void __set_CONTROL(uint32_t control)
{
  msr control, r0
  bx lr
}

#endif /* __ARMCC_VERSION  */ 



#elif (defined (__ICCARM__)) /*------------------ ICC Compiler -------------------*/
/* IAR iccarm specific functions */
#pragma diag_suppress=Pe940

/**
 * @brief  Return the Process Stack Pointer
 *
 * @return ProcessStackPointer
 *
 * Return the actual process stack pointer
 */
uint32_t __get_PSP(void)
{
  __ASM("mrs r0, psp");
  __ASM("bx lr");
}

/**
 * @brief  Set the Process Stack Pointer
 *
 * @param  topOfProcStack  Process Stack Pointer
 *
 * Assign the value ProcessStackPointer to the MSP 
 * (process stack pointer) Cortex processor register
 */
void __set_PSP(uint32_t topOfProcStack)
{
  __ASM("msr psp, r0");
  __ASM("bx lr");
}

/**
 * @brief  Return the Main Stack Pointer
 *
 * @return Main Stack Pointer
 *
 * Return the current value of the MSP (main stack pointer)
 * Cortex processor register
 */
uint32_t __get_MSP(void)
{
  __ASM("mrs r0, msp");
  __ASM("bx lr");
}

/**
 * @brief  Set the Main Stack Pointer
 *
 * @param  topOfMainStack  Main Stack Pointer
 *
 * Assign the value mainStackPointer to the MSP 
 * (main stack pointer) Cortex processor register
 */
void __set_MSP(uint32_t topOfMainStack)
{
  __ASM("msr msp, r0");
  __ASM("bx lr");
}

/**
 * @brief  Reverse byte order in unsigned short value
 *
 * @param  value  value to reverse
 * @return        reversed value
 *
 * Reverse byte order in unsigned short value
 */
uint32_t __REV16(uint16_t value)
{
  __ASM("rev16 r0, r0");
  __ASM("bx lr");
}

/**
 * @brief  Reverse bit order of value
 *
 * @param  value  value to reverse
 * @return        reversed value
 *
 * Reverse bit order of value
 */
uint32_t __RBIT(uint32_t value)
{
  __ASM("rbit r0, r0");
  __ASM("bx lr");
}

/**
 * @brief  LDR Exclusive (8 bit)
 *
 * @param  *addr  address pointer
 * @return        value of (*address)
 *
 * Exclusive LDR command for 8 bit values)
 */
uint8_t __LDREXB(uint8_t *addr)
{
  __ASM("ldrexb r0, [r0]");
  __ASM("bx lr"); 
}

/**
 * @brief  LDR Exclusive (16 bit)
 *
 * @param  *addr  address pointer
 * @return        value of (*address)
 *
 * Exclusive LDR command for 16 bit values
 */
uint16_t __LDREXH(uint16_t *addr)
{
  __ASM("ldrexh r0, [r0]");
  __ASM("bx lr");
}

/**
 * @brief  LDR Exclusive (32 bit)
 *
 * @param  *addr  address pointer
 * @return        value of (*address)
 *
 * Exclusive LDR command for 32 bit values
 */
uint32_t __LDREXW(uint32_t *addr)
{
  __ASM("ldrex r0, [r0]");
  __ASM("bx lr");
}

/**
 * @brief  STR Exclusive (8 bit)
 *
 * @param  value  value to store
 * @param  *addr  address pointer
 * @return        successful / failed
 *
 * Exclusive STR command for 8 bit values
 */
uint32_t __STREXB(uint8_t value, uint8_t *addr)
{
  __ASM("strexb r0, r0, [r1]");
  __ASM("bx lr");
}

/**
 * @brief  STR Exclusive (16 bit)
 *
 * @param  value  value to store
 * @param  *addr  address pointer
 * @return        successful / failed
 *
 * Exclusive STR command for 16 bit values
 */
uint32_t __STREXH(uint16_t value, uint16_t *addr)
{
  __ASM("strexh r0, r0, [r1]");
  __ASM("bx lr");
}

/**
 * @brief  STR Exclusive (32 bit)
 *
 * @param  value  value to store
 * @param  *addr  address pointer
 * @return        successful / failed
 *
 * Exclusive STR command for 32 bit values
 */
uint32_t __STREXW(uint32_t value, uint32_t *addr)
{
  __ASM("strex r0, r0, [r1]");
  __ASM("bx lr");
}

#pragma diag_default=Pe940


#elif (defined (__GNUC__)) /*------------------ GNU Compiler ---------------------*/
/* GNU gcc specific functions */

/**
 * @brief  Return the Process Stack Pointer
 *
 * @return ProcessStackPointer
 *
 * Return the actual process stack pointer
 */
uint32_t __get_PSP(void) __attribute__( ( naked ) );
uint32_t __get_PSP(void)
{
  uint32_t result=0;

  __ASM volatile ("MRS %0, psp\n\t" 
                  "MOV r0, %0 \n\t"
                  "BX  lr     \n\t"  : "=r" (result) );
  return(result);
}

/**
 * @brief  Set the Process Stack Pointer
 *
 * @param  topOfProcStack  Process Stack Pointer
 *
 * Assign the value ProcessStackPointer to the MSP 
 * (process stack pointer) Cortex processor register
 */
void __set_PSP(uint32_t topOfProcStack) __attribute__( ( naked ) );
void __set_PSP(uint32_t topOfProcStack)
{
  __ASM volatile ("MSR psp, %0\n\t"
                  "BX  lr     \n\t" : : "r" (topOfProcStack) );
}

/**
 * @brief  Return the Main Stack Pointer
 *
 * @return Main Stack Pointer
 *
 * Return the current value of the MSP (main stack pointer)
 * Cortex processor register
 */
uint32_t __get_MSP(void) __attribute__( ( naked ) );
uint32_t __get_MSP(void)
{
  uint32_t result=0;

  __ASM volatile ("MRS %0, msp\n\t" 
                  "MOV r0, %0 \n\t"
                  "BX  lr     \n\t"  : "=r" (result) );
  return(result);
}

/**
 * @brief  Set the Main Stack Pointer
 *
 * @param  topOfMainStack  Main Stack Pointer
 *
 * Assign the value mainStackPointer to the MSP 
 * (main stack pointer) Cortex processor register
 */
void __set_MSP(uint32_t topOfMainStack) __attribute__( ( naked ) );
void __set_MSP(uint32_t topOfMainStack)
{
  __ASM volatile ("MSR msp, %0\n\t"
                  "BX  lr     \n\t" : : "r" (topOfMainStack) );
}

/**
 * @brief  Return the Base Priority value
 *
 * @return BasePriority
 *
 * Return the content of the base priority register
 */
uint32_t __get_BASEPRI(void)
{
  uint32_t result=0;
  
  __ASM volatile ("MRS %0, basepri_max" : "=r" (result) );
  return(result);
}

/**
 * @brief  Set the Base Priority value
 *
 * @param  basePri  BasePriority
 *
 * Set the base priority register
 */
void __set_BASEPRI(uint32_t value)
{
  __ASM volatile ("MSR basepri, %0" : : "r" (value) );
}

/**
 * @brief  Return the Priority Mask value
 *
 * @return PriMask
 *
 * Return state of the priority mask bit from the priority mask register
 */
uint32_t __get_PRIMASK(void)
{
  uint32_t result=0;

  __ASM volatile ("MRS %0, primask" : "=r" (result) );
  return(result);
}

/**
 * @brief  Set the Priority Mask value
 *
 * @param  priMask  PriMask
 *
 * Set the priority mask bit in the priority mask register
 */
void __set_PRIMASK(uint32_t priMask)
{
  __ASM volatile ("MSR primask, %0" : : "r" (priMask) );
}

/**
 * @brief  Return the Fault Mask value
 *
 * @return FaultMask
 *
 * Return the content of the fault mask register
 */
uint32_t __get_FAULTMASK(void)
{
  uint32_t result=0;
  
  __ASM volatile ("MRS %0, faultmask" : "=r" (result) );
  return(result);
}

/**
 * @brief  Set the Fault Mask value
 *
 * @param  faultMask  faultMask value
 *
 * Set the fault mask register
 */
void __set_FAULTMASK(uint32_t faultMask)
{
  __ASM volatile ("MSR faultmask, %0" : : "r" (faultMask) );
}

/**
 * @brief  Return the Control Register value
* 
*  @return Control value
 *
 * Return the content of the control register
 */
uint32_t __get_CONTROL(void)
{
  uint32_t result=0;

  __ASM volatile ("MRS %0, control" : "=r" (result) );
  return(result);
}

/**
 * @brief  Set the Control Register value
 *
 * @param  control  Control value
 *
 * Set the control register
 */
void __set_CONTROL(uint32_t control)
{
  __ASM volatile ("MSR control, %0" : : "r" (control) );
}


/**
 * @brief  Reverse byte order in integer value
 *
 * @param  value  value to reverse
 * @return        reversed value
 *
 * Reverse byte order in integer value
 */
uint32_t __REV(uint32_t value)
{
  uint32_t result=0;
  
  __ASM volatile ("rev %0, %1" : "=r" (result) : "r" (value) );
  return(result);
}

/**
 * @brief  Reverse byte order in unsigned short value
 *
 * @param  value  value to reverse
 * @return        reversed value
 *
 * Reverse byte order in unsigned short value
 */
uint32_t __REV16(uint16_t value)
{
  uint32_t result=0;
  
  __ASM volatile ("rev16 %0, %1" : "=r" (result) : "r" (value) );
  return(result);
}

/**
 * @brief  Reverse byte order in signed short value with sign extension to integer
 *
 * @param  value  value to reverse
 * @return        reversed value
 *
 * Reverse byte order in signed short value with sign extension to integer
 */
int32_t __REVSH(int16_t value)
{
  uint32_t result=0;
  
  __ASM volatile ("revsh %0, %1" : "=r" (result) : "r" (value) );
  return(result);
}

/**
 * @brief  Reverse bit order of value
 *
 * @param  value  value to reverse
 * @return        reversed value
 *
 * Reverse bit order of value
 */
uint32_t __RBIT(uint32_t value)
{
  uint32_t result=0;
  
   __ASM volatile ("rbit %0, %1" : "=r" (result) : "r" (value) );
   return(result);
}

/**
 * @brief  LDR Exclusive (8 bit)
 *
 * @param  *addr  address pointer
 * @return        value of (*address)
 *
 * Exclusive LDR command for 8 bit value
 */
uint8_t __LDREXB(uint8_t *addr)
{
    uint8_t result=0;
  
   __ASM volatile ("ldrexb %0, [%1]" : "=r" (result) : "r" (addr) );
   return(result);
}

/**
 * @brief  LDR Exclusive (16 bit)
 *
 * @param  *addr  address pointer
 * @return        value of (*address)
 *
 * Exclusive LDR command for 16 bit values
 */
uint16_t __LDREXH(uint16_t *addr)
{
    uint16_t result=0;
  
   __ASM volatile ("ldrexh %0, [%1]" : "=r" (result) : "r" (addr) );
   return(result);
}

/**
 * @brief  LDR Exclusive (32 bit)
 *
 * @param  *addr  address pointer
 * @return        value of (*address)
 *
 * Exclusive LDR command for 32 bit values
 */
uint32_t __LDREXW(uint32_t *addr)
{
    uint32_t result=0;
  
   __ASM volatile ("ldrex %0, [%1]" : "=r" (result) : "r" (addr) );
   return(result);
}

/**
 * @brief  STR Exclusive (8 bit)
 *
 * @param  value  value to store
 * @param  *addr  address pointer
 * @return        successful / failed
 *
 * Exclusive STR command for 8 bit values
 */
uint32_t __STREXB(uint8_t value, uint8_t *addr)
{
   uint32_t result=0;
  
   __ASM volatile ("strexb %0, %2, [%1]" : "=&r" (result) : "r" (addr), "r" (value) );
   return(result);
}

/**
 * @brief  STR Exclusive (16 bit)
 *
 * @param  value  value to store
 * @param  *addr  address pointer
 * @return        successful / failed
 *
 * Exclusive STR command for 16 bit values
 */
uint32_t __STREXH(uint16_t value, uint16_t *addr)
{
   uint32_t result=0;
  
   __ASM volatile ("strexh %0, %2, [%1]" : "=&r" (result) : "r" (addr), "r" (value) );
   return(result);
}

/**
 * @brief  STR Exclusive (32 bit)
 *
 * @param  value  value to store
 * @param  *addr  address pointer
 * @return        successful / failed
 *
 * Exclusive STR command for 32 bit values
 */
uint32_t __STREXW(uint32_t value, uint32_t *addr)
{
   uint32_t result=0;
  
   __ASM volatile ("strex %0, %2, [%1]" : "=&r" (result) : "r" (addr), "r" (value) );
   return(result);
}


#elif (defined (__TASKING__)) /*------------------ TASKING Compiler ---------------------*/
/* TASKING carm specific functions */

/*
 * The CMSIS functions have been implemented as intrinsics in the compiler.
 * Please use "carm -?i" to get an up to date list of all instrinsics,
 * Including the CMSIS ones.
 */

#endif
END_OF_PATCH
    #}
      cd "$Install_Path"
    fi #}
    if [ "1" == "1" ]; then #{ download + unpack CPAL library for STM32L0
      
      Archive="STM32CubeL0.zip"
      getFile http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/firmware/ stm32cubel0.zip $Archive
      
      if [ ! -e $Archive ]; then #{
        echo "$ScriptName - ERROR: Cannot download $Archive!"
        exit 21
      fi #}
      
      DirFW_L0="STM32Cube_FW_L0_V1.1.0"
      unZIP $DirFW_L0 $Archive || mv -f $Archive ${Archive}_bad
      
      DirHAL=`find ./ -name STM32L0xx_HAL_Driver`
      if [ "$DirHAL" == "" ]; then #{
        echo "$ScriptName - ERROR: Cannot find folder STM32L0xx_HAL_Driver somewhere inside `pwd`!"
        exit 20
      fi #}
      createLink $DirHAL HAL_STM32L0xx
      
      DirCMSIS=`find $DirFW_L0/ -name CMSIS`
      if [ "$DirCMSIS" == "" ]; then #{
        echo "$ScriptName - ERROR: Cannot find folder DirCMSIS somewhere inside `pwd`/$DirFW_L0/!"
        exit 20
      fi #}
      createLink $DirCMSIS CMSIS_STM32L0
      
      DriverDir="CPAL/Libraries/STM32L0xx_StdPeriph_Driver/"
      if [ ! -d "$DriverDir" ]; then #{ create fake driver directory
        dir "$DriverDir"
        
        # Strange people think that 'Inc' == 'inc'.
        # Maybe they think of themself being C-coders...
        createLink ../../../HAL_STM32L0xx/Inc "${DriverDir}/inc" 
        createLink ../../../HAL_STM32L0xx/Src "${DriverDir}/src" 
      fi #}


      cd "$Install_Path"
    fi #}
    
    if [ -e CPAL/Release_Notes.html ]; then
      echo "" >OK.Install
    fi
    #cd ..
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #}
if [   -e OK.Install ];    then #{ create makefiles and activate scripts
  Name="${Install_Dir}"
  
  if [ "" == "DISABLED" ]; then #{ Currently not used
  createExtensionMakefileHead ${Name}      #{ makefile CPAL
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

INCLUDE_DIRS += -I additionals/$Name/

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Communication Peripheral Access Library is a superset of ST's standard peripheral libraries" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
#!/bin/bash

rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.250_stm_std_peripherals_*
rm 2>/dev/null \$Dir_Additionals/250_stm_std_peripherals

OldPWD=\`pwd\`
cd "\$Dir_Additionals"
LinkScript="createLink.sh"

rm "\$LinkScript"
cd "\$OldPWD"

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
  fi #}
  
  addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir;  createLink \$Source/TheToolChain/InstallData/$Install_Dir/CPAL  $Install_Dir"
  addLine ../scripts/createLinks.sh "rm 2>/dev/null 250_STM32L0xx_CMSIS;  createLink \$Source/TheToolChain/InstallData/$Install_Dir/CMSIS_STM32L0  250_STM32L0xx_CMSIS"

  OldPWD="`pwd`"
  Dir_CoreSupport=`../scripts/findDirectory.pl core_cm3.h CPAL/`
  cd CPAL/Libraries/

    #{ create macro assert_param in stm32f10x.h to fix a compilation issue ToDo: Needed anymore?
    
    File="CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h"
    if [ -e ${File}_orig ]; then
        mv -f ${File}_orig $File 
    fi
    mv -f $File ${File}_orig
    echo "  creating macro assert_param in $File"
    Note="  Note: Manually remove assert_param from your stm32f10x_conf.h !"
    echo $Note
    addLine ../Notes.txt $Note
    addLine ../Notes.txt "StdPeripheralLibrary-> http://www.mikrocontroller.net/articles/STM32F10x_Standard_Peripherals_Library"
    
    cat >script.sed <<"END_OF_TEXT" #{
/#include <stdint.h>/a\
#ifdef  USE_FULL_ASSERT\n
\n
// This assert_param macro has been copied here from stm32f10x_conf.h to\n
// circumvent gcc compiler errors.\n
\n
/**\n
  * @brief  The assert_param macro is used for function's parameters check.\n
  * @param  expr: If expr is false, it calls assert_failed function\n
  *   which reports the name of the source file and the source\n
  *   line number of the call that failed.\n 
  *   If expr is true, it returns no value.\n
  * @retval None\n
  */\n
  #define assert_param(expr) ((expr) ? (void)0 : assert_failed((uint8_t *)__FILE__, __LINE__))\n
  void assert_failed(uint8_t* file, uint32_t line);\n
#else\n
  #define assert_param(expr) ((void)0)\n
#endif /* USE_FULL_ASSERT */\n

END_OF_TEXT
#}
    
    # concatenate all lines which contain \n (sed requires this)
    awk '/\\n/{printf "%s",$ScriptName;next}{print}' script.sed >script2.sed
    
    # insert lines after "#include <stdint.h>"
    sed -f script2.sed ${File}_orig >$File
    #} fix a bug
  
  SubDirs=`ls */ -d`
  for SubDir in $SubDirs; do #{ create link into additionals/ for each sub directory
    SubDir=`perl -e "print substr('${SubDir}', 0, -1);"` # remove trailing /
    echo "sub library: $SubDir"
    echo $SubDirs
    echo $Link
    Link="${Install_Dir}_$SubDir"
    addLine $OldPWD/../scripts/createLinks.sh "rm 2>/dev/null $Link;  createLink $Install_Dir/Libraries/$SubDir  $Link"
  done #}
  
  SubDirs=`ls -d *_StdPeriph_Driver`
  for SubDir in $SubDirs; do #{ create makefile + activate script for each *_StdPeriph_Driver directory
    Name="250_CPAL_${SubDir}" # std peripheral uses different rank than CPAL!
    Name2="270_CPAL_${SubDir}"
    Architecture=`perl -e "print substr('${SubDir}', 0, 9);"`

    createExtensionMakefileHead ${Name} $OldPWD/${Dir_Extensions} #{      create makefile for std_peripheral_library
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile

COMPILE_OPTS += -DEXTENSION_250_stm_std_peripherals=1

INCLUDE_DIRS += -I additionals/250_stm_std_peripherals/inc/ \\
                -I additionals/270_CPAL_CMSIS/CM3/CoreSupport/ \\
                -I additionals/270_CPAL_CMSIS/CM3/DeviceSupport/ST/$Architecture/

vpath %.c additionals/250_stm_std_peripherals/src/ \\
          additionals/270_CPAL_CMSIS/CM3/DeviceSupport/ST/$Architecture/

vpath %.s additionals/270_CPAL_CMSIS/CM3/DeviceSupport/ST/$Architecture/startup/gcc_ride7

MAIN_OBJS += misc.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} $OldPWD/${Dir_Extensions} #}
    createActivateScriptHead $Name "$OldPWD/${Dir_Extensions}" "$ScriptName 1" "Architecture dependent standard peripheral library for $SubDir microcontrollers" #{ 
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
#!/bin/bash

Module_ERROR=""

# remove existing links
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Name}
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Install_Dir}_*_StdPeriph_Driver


OldPWD=\`pwd\`
cd "\$Dir_Additionals"

rm 2>/dev/null 250_stm_std_peripherals
createLink $Name2 250_stm_std_peripherals
cd "\$OldPWD"


# create links into extensions/
cd "\${Dir_Extensions}"
rm 2>/dev/null *250_stm_std_peripherals_*

DriversFound=\`ls $Architecture/*\`
if [ "\$DriversFound" != "" ]; then
  ln -sf $Architecture/* .
else
  Module_ERROR="Cannot find drivers in \`pwd\`/$Architecture/ !"
fi

cd "\$CurrentPath"
if [ "\$Module_ERROR" == "" ]; then
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
else
  echo "\$ScriptName - ERROR: \$Module_ERROR"
fi

END_OF_ACTIVATE
#}
    createActivateScriptTail $Name "$OldPWD/${Dir_Extensions}"
    
    #{ create one activate-script per c-file
    TargetDir="../../${Dir_Extensions}$Architecture"
    dir "$TargetDir"

    LibPrefix="250_stm_std_peripherals"
    for SourceFile in `find ${SubDir}/src/ -name "*\.c" -execdir echo -n "{} " \;` ; do #{
      SourceName=`perl -e "print substr('$SourceFile', 2, -2);"`
      Source=`perl -e "print substr('$SourceFile', 12, -2);"`
      if [ "$Source" != "" ]; then
        DestName="${LibPrefix}__${Source}"
        DestName2=`perl -e "my \\$F='$DestName'; \\$F =~ s/_hal_/_/; print \\$F;"` # remove hal_ from name to provide uniform activate scripts (introduced by Std Peripheral Library for stm32l0)
        createExtensionMakefileHead ${DestName2} $TargetDir/ #{
        cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # set by createExtensionMakefileHead() 
  
MAIN_OBJS += ${SourceName}.o

END_OF_MAKEFILE
        createExtensionMakefileTail ${DestName2} $TargetDir/ #}
        createActivateScriptHead $DestName2 $TargetDir/ $ScriptName "adds c-source to list of compiled objects: ${SourceName}.c" #{
        cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# add createL.inks for each link required for this extension
createLink ${Dir_Extensions}makefile.$DestName2 \${Dir_ExtensionsActive}/makefile.$DestName2 '' QUIET

END_OF_ACTIVATE
#}
        createActivateScriptTail $DestName2 $TargetDir/
        #}
      fi
    done #}
    #} create one activate-script per c-file
  done #}
  
  #{ STM32_CPAL_Driver: create makefiles + activate scripts
  Device_IncludeDirs=`find STM32_CPAL_Driver/devices/ -mindepth 1 -type d -exec echo "                -I additionals/${Install_Dir}_{} \\\\" \;`
  Device_SourceDirs=`find STM32_CPAL_Driver/devices/ -mindepth 1 -type d -exec echo "          additionals/${Install_Dir}_{} \\\\" \;`
  Name="${Install_Dir}_STM32_CPAL_Driver"
  createExtensionMakefileHead ${Name} "$OldPWD/${Dir_Extensions}" #{ create makefile for STM32_CPAL_Driver
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile

INCLUDE_DIRS += -I additionals/$Install_Dir/$Dir_CoreSupport
INCLUDE_DIRS += -I additionals/270_CPAL_STM32_CPAL_Driver/inc/ \\
$Device_IncludeDirs
                
                
vpath %.c additionals/$Install_Dir/$Dir_CoreSupport
vpath %.c additionals/270_CPAL_STM32_CPAL_Driver/src/ \\
$Device_SourceDirs


END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} $OldPWD/${Dir_Extensions} #}
  createActivateScriptHead $Name "$OldPWD/${Dir_Extensions}" "$ScriptName 1" "Driver required to use architecture independent features of ST's Common Peripheral Access Library (CPAL)" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{
#!/bin/bash

#rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Name}_*

OldPWD=\`pwd\`
cd "\$Dir_Additionals"

cd "\$OldPWD"

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

END_OF_ACTIVATE
#}
  createActivateScriptTail $Name "$OldPWD/../extensions"
  #}
  
  #{ create one activate-script per c-file
  TargetDir="../../../extensions"
  #echo "TargetDir> ls `pwd`/$TargetDir"
  BaseName=$Name
  for SourceFile in `find STM32_CPAL_Driver/src/ -name "*\.c" -execdir echo -n "{} " \;` ; do #{
    SourceName=`perl -e "print substr('$SourceFile', 2, -2);"`
    if [ "$SourceName" != "" ]; then
      DestName="${BaseName}__${SourceName}"
      DestName2=`perl -e "my \\$F='$DestName'; \\$F =~ s/_hal_/_/; print \\$F;"` # remove hal_ from name to provide uniform activate scripts (introduced by Std Peripheral Library for stm32l0)
      echo "new extension: '$DestName2'"
      createExtensionMakefileHead ${DestName2} $TargetDir/ #{
      cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # set by createExtensionMakefileHead() 

MAIN_OBJS += ${SourceName}.o

END_OF_MAKEFILE
      createExtensionMakefileTail ${DestName2} $TargetDir/ #}
      createActivateScriptHead $DestName2 $TargetDir/ $ScriptName "adds c-source to list of compiled objects: ${SourceName}.c" #{
      cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# activate CPAL
activate.${Install_Dir}_STM32_CPAL_Driver.sh QUIET \"\$ScriptName\"

# add createLinks for each link required for this extension
createLink ${Dir_Extensions}makefile.$DestName2 \${Dir_ExtensionsActive}/makefile.$DestName2 '' QUIET

END_OF_ACTIVATE
#}
      createActivateScriptTail $DestName2 $TargetDir/
      #}
    fi
  done #}
  #}
  
  #}STM32_CPAL_Driver

  cd "$OldPWD"

  if [ "1" == "1" ]; then #{ add extra include paths for stm32l0
    
    addLine "../extensions/makefile.250_CPAL_STM32L0xx_StdPeriph_Driver" "#{add extra include paths for stm32l0"
   
    addLine "../extensions/makefile.250_CPAL_STM32L0xx_StdPeriph_Driver" "INCLUDE_DIRS += -I additionals/250_STM32L0xx_CMSIS/Device/ST/STM32L0xx/Include/" 
    addLine "../extensions/makefile.250_CPAL_STM32L0xx_StdPeriph_Driver" "INCLUDE_DIRS += -I additionals/250_STM32L0xx_CMSIS/Include/"
    addLine "../extensions/makefile.250_CPAL_STM32L0xx_StdPeriph_Driver" "vpath %.c additionals/250_STM32L0xx_CMSIS/Device/ST/STM32L0xx/Source/Templates/"
    addLine "../extensions/makefile.250_CPAL_STM32L0xx_StdPeriph_Driver" "vpath %.s additionals/250_STM32L0xx_CMSIS/Device/ST/STM32L0xx/Source/Templates/gcc"
    addLine "../extensions/makefile.250_CPAL_STM32L0xx_StdPeriph_Driver" "MAIN_OBJS += stm32l0xx_hal.o"
    addLine "../extensions/makefile.250_CPAL_STM32L0xx_StdPeriph_Driver" "MAIN_OBJS += stm32l0xx_hal_cortex.o"
    
    addLine "../extensions/makefile.250_CPAL_STM32L0xx_StdPeriph_Driver" "#}"
    
    
    
  fi #}
  
  echo "Installed successfully: $Install_Dir"
#}
else                            #{ Install failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
