                       The ToolChain


This folder stores individual install scripts for all supported extensions.
In ancient times, install scripts have been created manually and grew with each low-level driver.
Install scripts for low-level drivers started to be separated from high-level install scripts since 
release 1.0.53. 
Check Documentation/chapter_Extensions.tex, Documentation/chapter_Drivers.tex for details.


