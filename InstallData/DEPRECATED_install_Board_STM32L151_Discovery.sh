#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Greg Knoll 2013
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"
                          
RANK="100"
EXTENSION_SHORT="stm32l152_discovery"
EXTENSION_PREFIX="board"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
  if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/ DM00027954.pdf Board_STM32-L152_Discovery.pdf Boards
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

  Dir=`pwd`
    
  Name="${Install_Dir}" #{Write Board Makefile and Activate Script
  
    createExtensionMakefileHead ${Name} #{
    	File="${Dir_Extensions}makefile.${Name}"
cat <<END_OF_MAKEFILE >>$File
  
  COMPILE_OPTS += -DTTC_BOARD=$Name
  BOARD=$Name

  #X STM32L1XX_MD=1
  #X COMPILE_OPTS += -DTTC_SYSCLOCK_HSI=1
  
  # Define exactly one CPU variant used on current board
  # See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
  TTC_CPU_VARIANT_stm32l152rbt6 = 1

	#{ Define port pins
	  # reference ../../Documentation/Boards/Board_STM32-L1Disc.pdf
	
	  #{LEDs
	  COMPILE_OPTS += -DTTC_LED1=tgp_b7
	  COMPILE_OPTS += -DTTC_LED2=tgp_b6
	  #}
	
	  #{Buttons
	  #COMPILE_OPTS += -DTTC_SWITCH1=PIN_NRST                # press button switch 1 (named Tamper)
	  #COMPILE_OPTS += -DTTC_SWITCH1_TYPE=tgm_input_floating # -> ttc_gpio_types.h
	  #COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=1
	  COMPILE_OPTS += -DTTC_SWITCH1=tgp_a0                 # press button switch 2 (named WKUP)
	  COMPILE_OPTS += -DTTC_SWITCH1_TYPE=tgm_input_floating # -> ttc_gpio_types.h
	  COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=0
	  #}
  
	  #{Oszillator
	  COMPILE_OPTS += -DTTC_RTC_CLOCK_SOURCE_LSE           #Use external 32.768 kHz Crystal
	  #}
	  
	  #{TODO: define pin configuration of Serial peripherals
		#{USART1
		COMPILE_OPTS += -DTTC_USART1=INDEX_USART1
		COMPILE_OPTS += -DTTC_USART1_TX=tgp_b6   
		COMPILE_OPTS += -DTTC_USART1_RX=tgp_b7   
		COMPILE_OPTS += -DTTC_USART1_RTS=tgp_a12  
		COMPILE_OPTS += -DTTC_USART1_CTS=tgp_a11 
		COMPILE_OPTS += -DTTC_USART1_CK=tgp_a8
		#}
		
		#{USART2
		COMPILE_OPTS += -DTTC_USART2=INDEX_USART2
		COMPILE_OPTS += -DTTC_USART2_TX=tgp_a2  
		COMPILE_OPTS += -DTTC_USART2_RX=tgp_a3   
		COMPILE_OPTS += -DTTC_USART2_RTS=tgp_a1  
		COMPILE_OPTS += -DTTC_USART2_CTS=tgp_a0 
		COMPILE_OPTS += -DTTC_USART2_CK=tgp_a4 
		#}
		
		#{USART3
		COMPILE_OPTS += -DTTC_USART3=INDEX_USART3
		COMPILE_OPTS += -DTTC_USART3_TX=tgp_b10  
		COMPILE_OPTS += -DTTC_USART3_RX=tgp_b11   
		COMPILE_OPTS += -DTTC_USART3_RTS=tgp_b14  
		COMPILE_OPTS += -DTTC_USART3_CTS=tgp_b13 
		COMPILE_OPTS += -DTTC_USART3_CK=tgp_b12 

		#}
		
	
		#{SPI1 - define serial peripheral interface #1
		  COMPILE_OPTS += -DTTC_SPI1=ttc_device_1          # 
		  COMPILE_OPTS += -DTTC_SPI1_MOSI=tgp_a12  # P2-19
		  COMPILE_OPTS += -DTTC_SPI1_MISO=tgp_a11  # P2-20
		  COMPILE_OPTS += -DTTC_SPI1_SCK=tgp_b3   # P2-11
		  COMPILE_OPTS += -DTTC_SPI1_NSS=tgp_a15   # P2-16
		#}
		
		#{SPI2 - define serial peripheral interface #2
		  COMPILE_OPTS += -DTTC_SPI2=ttc_device_2           # 
		  COMPILE_OPTS += -DTTC_SPI2_MOSI=tgp_b15  # P1-27
		  COMPILE_OPTS += -DTTC_SPI2_MISO=tgp_b14  # P1-26
		  COMPILE_OPTS += -DTTC_SPI2_SCK=tgp_b13   # P1-25
		  COMPILE_OPTS += -DTTC_SPI2_NSS=tgp_b12   # P1-24
		#}
		COMPILE_OPTS += -DTTC_I2C_AMOUNT=2
		#{ I2C1    - define inter-integrated circuit interface #1
		COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
		COMPILE_OPTS += -DTTC_I2C1_SDA=tgp_b7    # P2-7
		COMPILE_OPTS += -DTTC_I2C1_SCL=tgp_b6    # P2-8
		COMPILE_OPTS += -DTTC_I2C1_SMBAL=tgp_b5  # P2-9
		#}I2C1
		
		#{ I2C2    - define inter-integrated circuit interface #2
		COMPILE_OPTS += -DTTC_I2C2=ttc_device_2           # internal device being used for this port
  		COMPILE_OPTS += -DTTC_I2C2_SDA=tgp_b11   # P1-23
  		COMPILE_OPTS += -DTTC_I2C2_SCL=tgp_b10   # P1-22
  		COMPILE_OPTS += -DTTC_I2C2_SMBAL=tgp_b12 # P1-24
  		#}I2C2
		#{CAN
		#}
		#{DMA
    COMPILE_OPTS += -DTTC_DMA1=ttc_device_1
		COMPILE_OPTS += -DTTC_NCHANNELS=TTC_NUMBER_OF_CHANNELS 
    #}
		#{USB
		#}
	  #}
	
	#} END Define port pins
  
END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} 
    #}END createExtensionMakefile
    
    createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Discovery Board for STM32L100C-DISCO from STM " #{
	File="${Dir_Extensions}activate.${Name}.sh"
cat <<END_OF_ACTIVATE >>$ActivateScriptFile    

	#{ From install_11_Board_STM32L100CDiscovery.sh
	  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
	  
	  # create links into extensions.active/
	  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
	  
	  #X activate.200_cpu_stm32l1xx.sh QUIET "\$ScriptName"
	#}END From install_11_Board_STM32L100CDiscovery.sh
  enableFeature 450_cpu_stm32l1xx

END_OF_ACTIVATE
    createActivateScriptTail $Name ${Dir_Extensions}
    #}END createActivateScript
    
  echo "Installed successfully: $Install_Dir"
  #} END Makefile and Activate Script

  
#} 
#  IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd ..
rmdir 2>/dev/null $Install_Dir

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
