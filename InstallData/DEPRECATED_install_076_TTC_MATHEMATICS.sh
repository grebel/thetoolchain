#!/bin/bash

#
#  Install script for basic mathematic support
#
#  Initial Script written by Gregor Rebel 2010-2012.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "500_ttc_math" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ download + install software
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe 'which autoreconf' autoreconf
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts
  Name="${Install_Dir}_basic"
  createExtensionMakefileHead ${Name}      #{ create makefile for basic mathematic functions
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
INCLUDE_DIRS += -I ttc-lib/math

# additional directories to search for C-Sources
vpath %.c ttc-lib/math 

# additional object files to be compiled
MAIN_OBJS += math_basic.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$0 2" "basic mathematic functions" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_Additionals/$Install_Dir_*

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  Name="${Install_Dir}_trigonometry"
  createExtensionMakefileHead ${Name}      #{ create makefile for trigonometric functions
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
INCLUDE_DIRS += -I ttc-lib/math

# additional directories to search for C-Sources
vpath %.c ttc-lib/math 

# additional object files to be compiled
MAIN_OBJS += DEPRECATED_math_trigonometry.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$0 2" "basic mathematic functions" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_Additionals/$Install_Dir_*

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  cd "$OldPWD"
  echo "Installed successfully: $Install_Dir"
#}
else                            #{ Install failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
