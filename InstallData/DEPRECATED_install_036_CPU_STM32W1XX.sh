#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
#
#  Install script for a set of High- and Low-Level Drivers
#  for stm32w1xx devices.
#
#  Created from template _install_NN_TTC_DEVICE.sh revision 22 at 20140528 16:08:39 UTC
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

StartPath="`pwd`"

# This install scripts provides extensions in different ranks
Rank_CPU="200" # rank of cpu STM32W1xx
Rank_MAC="251" # rank of SimpleMAC library for STM32W1xx
Rank_HAL="253" # rank of Hardware Abstraction Layer library for STM32W1xx

# High-Level extension names (low-level extensions will add similar extension names showing a version number)
DriverName_CPU="${Rank_CPU}_cpu_stm32w1xx"
DriverName_MAC="${Rank_MAC}_stm32w1xx_simple_mac"
DriverName_HAL="${Rank_HAL}_stm32w1xx_hal"

EXTENSION_NAME="cpu_stm32w1xx"
setInstallDir "${Rank_CPU}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
Install_Path=`pwd`
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   exit 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/PROGRAMMING_MANUAL/ CD00280769.pdf PM0073-STM32W1xx_Programming_Manual.pdf                                         uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/        CD00262415.pdf UM0894-STM32W-SK_and_STM32W-EXT_starter_and_extension_kits.pdf                  uC/STM32W1xx/
  getDocumentation http://www.st.com/web/en/resource/technical/document/user_manual/                           CD00262415.pdf UM0894-STM32W-SK_and_STM32W-EXT_starter_and_extension_kits.pdf
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/        CD00280375.pdf UM0978-Using_the_Simple_MAC_nodetest_application.pdf                            uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/        CD00264812.pdf UM0909-STM32W108xx_ZigBee_RF4CE_library.pdf                                     uC/STM32W1xx/
    #Not available anymore: getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00271698.pdf AN3211-Using_the_simulated_EEPROM_with_the_STM32W108_platform.pdf               uC/STM32W1xx/
  getDocumentation http://www.po-star.com/public/uploads/                                              20120227121858_397.pdf AN3211-Using_the_simulated_EEPROM_with_the_STM32W108_platform.pdf               uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00282958.pdf AN3262-Using_the_over-the-air_bootloader_with_STM32W108_devices.pdf             uC/STM32W1xx/
    #Not available anymore: getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00269334.pdf AN3188-Preparing_custom_devices_for_the_STM32W108_platform.pdf                  uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00270908.pdf AN3206-PCB_design_guidelines_for_the_STM32W108_platform.                        uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   DM00024648.pdf AN3359-Low_cost_PCB_antenna_for_2.4GHz_radio_Meander_design.pdf                 uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00004479.pdf AN1709-EMC_design_guide_for_ST_microcontrollers.pdf                             uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00274443.pdf AN3218-Adjacent_channel_rejection_measurements_for_the_STM32W108_platform.pdf   uC/STM32W1xx/
  getDocumentation http://images.icbuy.com/tec/STM2011/STM32W_Docs/                                            CD00269334.pdf AN3188-Preparing_custom_devices_for_the_STM32W108_platform.pdf                  uC/STM32W1xx/                                            
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/DATA_BRIEF/         CD00237927.pdf DB0837-High-performance_802.15.4_wireless_system-on-chip_databrief.pdf          uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/DATASHEET/          CD00248316.pdf DS6473-High-performance_IEEE_802.15.4_wireless_system-on-chip_Datasheet.pdf     uC/STM32W1xx/
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/       CD00262339.pdf UM0893-STM32W108xx_SimpleMAC_library.pdf                                        uC/STM32W1xx/
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/       DM00065100.pdf UM1576-Description_of_STM32W108xx_Standard_Peripheral_Library.pdf               uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/SALES_AND_MARKETING_RESOURCES/MARKETING_PRESENTATIONS/PRODUCT_PRESENTATION/ stm32w_marketing_pres.pdf  STM32W-Product_presentation.pdf                      uC/STM32W1xx/
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/release_note/      CD00262343.pdf RN0046_SimpleMAC_library_for_STM32W108xx_kits.pdf                               uC/STM32W1xx/
  getDocumentation http://www.st.com/web/en/resource/technical/document/application_note/                      DM00060426.pdf AN4142-STM32W108xx_datasheet_bit_and_register_naming_migration.pdf
  getDocumentation http://www.st.com/web/en/resource/technical/document/errata_sheet/                          DM00032130.pdf STM32W108xx-Errata_sheet-STM32W108xx_device_limitations
  getDocumentation http://www.st.com/web/en/resource/technical/document/application_note/                      DM00059814.pdf AN4141-Migration_and_compatibility_guidelines_for_STM32W108xx_microcontroller_applications_based_on_the_HAL.pdf uC/STM32W1xx/
    
  getDocumentation http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    EM35x.pdf       Ember_Manual_EM35x_rev13.pdf                                                    uC/STM32W1xx/
  getDocumentation http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    UG103.pdf       Ember_UG103.0_Application_Development_Fundamentals.pdf                          uC/STM32W1xx/
	getDocumentation http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN698.pdf       Ember_AN698_PCB_Design_with_an_EM35x.pdf                                        uC/STM32W1xx/
	getDocumentation http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN703.pdf       Ember_AN703_Using_the_Simulated_EEPROM.pdf                                      uC/STM32W1xx/
	getDocumentation http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN710.pdf       Ember_AN710_Bringing_up_Custom_Devices_for_the_EM35x_SoC_Platform.pdf           uC/STM32W1xx/
	getDocumentation http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN713.pdf       Ember_AN713_Measuring_EM35x_Power_Consumption.pdf                               uC/STM32W1xx/
	getDocumentation http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN715.pdf       Ember_AN715_Using_the_EM35x_ADC.pdf                                             uC/STM32W1xx/
	getDocumentation http://www.telegesis.com/downloads/general/                                     tg-etrx35x-pm-010-107.pdf   Telegesis_Product_Manual_ETRX35x_ZigBee_Module.pdf                              uC/STM32W1xx/
	getDocumentation http://www.telegesis.com/downloads/general/                      TG-PM-0505-CI-AT-Command-Manual%20r1.pdf   Telegesis_AT_Command_Set_for_Combined_Interface.pdf                             uC/STM32W1xx/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

if [ ! -e OK.Install ]; then    #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    # everything done in low-level installs
    
    touch OK.Install
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ high-level driver for stm32w1xx
    Name="${Rank_CPU}_cpu_em357"  
  
    createExtensionMakefileHead ${Name}      #{ (create makefile)
    
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

# declare high-level extension names for libraries
# Different versions of HAL- and MAC-library are available as low-level extensions.
# These high-level extension definitions make it easier for C-code to find out if any version
# of HAL- or MAC-library is installed.
COMPILE_OPTS += -DEXTENSION_${DriverName_MAC}
COMPILE_OPTS += -DEXTENSION_${DriverName_HAL}

# additional directories to search for header files
INCLUDE_DIRS += -Ittc-lib/basic/

# additional directories to search for C-Sources
vpath %.c ttc-lib/basic/

ARM_CPU     = arm7tdmi
TARGET      = -mcpu=cortex-m3 -mthumb
TARGET_ARCHITECTURE_STM32W1xx=1
TARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_PREFIX=EM357
COMPILE_OPTS += -DTARGET_ARCHITECTURE_STM32W1xx

# list of available devices (activate only one!)
STM32W108   =1       # STM32W Device

ifdef uCONTROLLER
  ERROR: STM32W108=1 - Only one uController may be selected!
endif
uCONTROLLER=STM32W108
COMPILE_OPTS += -DuCONTROLLER=STM32W108
COMPILE_OPTS += -DSTM32W108
COMPILE_OPTS += -DCORTEXM3

COMPILE_OPTS += -Ittc-lib/stm32w/
vpath %.c ttc-lib/stm32w/

# define linker scripts to use
LDFLAGS+= -Tconfigs/memory_stm32w1xx.ld

# extra settings for SimpleMAC library
COMPILE_OPTS += -DPHY_STM32w108XX
COMPILE_OPTS += -DENABLE_OSC32K
COMPILE_OPTS += -DDISABLE_WATCHDOG
COMPILE_OPTS += -DCORTEXM3_STM32W108xB
COMPILE_OPTS += -DCORTEXM3
COMPILE_OPTS += -DCORTEXM3_STM32W108


END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $Dir_Extensions "$0 2" "high-level driver for em357 cpu" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/
#   \$File_LowLevelInstall  !="": user wants to run only this low level install script 


# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

if [ "\$File_LowLevelInstall" != "" ]; then
  \$File_LowLevelInstall QUIET "\$0"

  # extract driver name from script name
  LowLevelDriver=\`perl -e "print substr('\$File_LowLevelInstall', 9, -3);"\`
  
  #X #DISABLED echo "\$0 - LowLevelDriver='\$LowLevelDriver'"
  DriverFound="1"

else
  for LowLevelActivate in \`ls \$Dir_Extensions/activate.450_stm32w1xx_*\`; do
    
    # try to activate this low-level driver
    \$LowLevelActivate QUIET "\$0"
    
    # extract driver name from script name
    LowLevelDriver=\`perl -e "my \\\\\$P=rindex('\$LowLevelActivate', 'activate.')+9; print substr('\$LowLevelActivate', \\\\\$P, -3);"\`
    #DISABLED echo "\$0 - LowLevelDriver='\$LowLevelDriver'"
    Makefile="\$Dir_ExtensionsActive/makefile.\$LowLevelDriver"
    if [ -e \$Makefile ]; then
      DriverFound="1"
    fi
  done
fi

if [ "\$DriverFound" != "" ]; then

  # ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
  createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

  # ACTIVATE_SECTION_C activate initialization source-code for this extension 
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

  # ACTIVATE_SECTION_D call other activate-scripts
  #X activate.500_ttc_memory.sh QUIET "\$0"
  activate.190_cpu_cortexm3.sh                   QUIET \"\$0\"
#  activate.250_STM32W1XX_StdPeriph_Driver.sh     QUIET "\$0" # (SimpleMAC201)
  activate.270_CPAL_STM32_CPAL_Driver.sh         QUIET \"\$0\"
#  #X activate.500_ttc_register.sh                   QUIET \"\$0\"
  
  # create links to allow flashing this uC
# These links should have been created by CPU extension
#  createLink ../additionals/999_open_ocd/target/stm32w1xx_jtag.cfg   configs/openocd_target_jtag.cfg
#  createLink ../additionals/999_open_ocd/target/stm32w108_stlink.cfg configs/openocd_target_stlinkv1.cfg
#  createLink ../additionals/999_open_ocd/target/stm32w108_stlink.cfg configs/openocd_target_stlinkv2.cfg
 if [ -e ../additionals/999_open_ocd/target/em357_flash.cfg ]; then
   createLink ../additionals/999_open_ocd/target/em357_flash.cfg  configs/openocd_flash.cfg
 fi
 if [ -e ../additionals/999_open_ocd/target/em357_jtag.cfg ]; then
   createLink ../additionals/999_open_ocd/target/em357_jtag.cfg  configs/openocd_jtag.cfg
 fi

else
  echo "\$0 - ERROR: No low-level stm32w1xx Driver found: Skipping activation!"
fi

END_OF_ACTIVATE
    createActivateScriptTail $Name $Dir_Extensions

    #}

    Name="$DriverName_CPU"  
  
    createExtensionMakefileHead ${Name}      #{ (create makefile)
    
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

# declare high-level extension names for libraries
# Different versions of HAL- and MAC-library are available as low-level extensions.
# These high-level extension definitions make it easier for C-code to find out if any version
# of HAL- or MAC-library is installed.
COMPILE_OPTS += -DEXTENSION_${DriverName_MAC}
COMPILE_OPTS += -DEXTENSION_${DriverName_HAL}

# additional directories to search for header files
INCLUDE_DIRS += -Ittc-lib/basic/

# additional directories to search for C-Sources
vpath %.c ttc-lib/basic/

ARM_CPU     = arm7tdmi
TARGET      = -mcpu=cortex-m3 -mthumb
TARGET_ARCHITECTURE_STM32W1xx=1
TARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_PREFIX=EM357
COMPILE_OPTS += -DTARGET_ARCHITECTURE_STM32W1xx

# list of available devices (activate only one!)
STM32W108   =1       # STM32W Device

ifdef uCONTROLLER
  ERROR: STM32W108=1 - Only one uController may be selected!
endif
uCONTROLLER=STM32W108
COMPILE_OPTS += -DuCONTROLLER=STM32W108
COMPILE_OPTS += -DSTM32W108
COMPILE_OPTS += -DCORTEXM3

COMPILE_OPTS += -Ittc-lib/stm32w/
vpath %.c ttc-lib/stm32w/

# define linker scripts to use
LDFLAGS+= -Tconfigs/memory_stm32w1xx.ld

# extra settings for SimpleMAC library
COMPILE_OPTS += -DPHY_STM32w108XX
COMPILE_OPTS += -DENABLE_OSC32K
COMPILE_OPTS += -DDISABLE_WATCHDOG
COMPILE_OPTS += -DCORTEXM3_STM32W108xB
COMPILE_OPTS += -DCORTEXM3
COMPILE_OPTS += -DCORTEXM3_STM32W108


END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $Dir_Extensions "$0 2" "high-level driver for stm32w1xx cpu" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/
#   \$File_LowLevelInstall  !="": user wants to run only this low level install script 


# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

if [ "\$File_LowLevelInstall" != "" ]; then
  \$File_LowLevelInstall QUIET "\$0"

  # extract driver name from script name
  LowLevelDriver=\`perl -e "print substr('\$File_LowLevelInstall', 9, -3);"\`
  
  #X #DISABLED echo "\$0 - LowLevelDriver='\$LowLevelDriver'"
  DriverFound="1"

else
  for LowLevelActivate in \`ls \$Dir_Extensions/activate.450_stm32w1xx_*\`; do
    
    # try to activate this low-level driver
    \$LowLevelActivate QUIET "\$0"
    
    # extract driver name from script name
    LowLevelDriver=\`perl -e "my \\\\\$P=rindex('\$LowLevelActivate', 'activate.')+9; print substr('\$LowLevelActivate', \\\\\$P, -3);"\`
    #DISABLED echo "\$0 - LowLevelDriver='\$LowLevelDriver'"
    Makefile="\$Dir_ExtensionsActive/makefile.\$LowLevelDriver"
    if [ -e \$Makefile ]; then
      DriverFound="1"
    fi
  done
fi

if [ "\$DriverFound" != "" ]; then

  # ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
  createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

  # ACTIVATE_SECTION_C activate initialization source-code for this extension 
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

  # ACTIVATE_SECTION_D call other activate-scripts
  #X activate.500_ttc_memory.sh QUIET "\$0"
  activate.190_cpu_cortexm3.sh                   QUIET \"\$0\"
#  activate.250_STM32W1XX_StdPeriph_Driver.sh     QUIET "\$0" # (SimpleMAC201)
  activate.270_CPAL_STM32_CPAL_Driver.sh         QUIET \"\$0\"
  #X activate.500_ttc_register.sh                   QUIET \"\$0\"
  
  # create links to allow flashing this uC
# These links should have been created by CPU extension
#  createLink ../additionals/999_open_ocd/target/stm32w1xx_jtag.cfg   configs/openocd_target_jtag.cfg
#  createLink ../additionals/999_open_ocd/target/stm32w108_stlink.cfg configs/openocd_target_stlinkv1.cfg
#  createLink ../additionals/999_open_ocd/target/stm32w108_stlink.cfg configs/openocd_target_stlinkv2.cfg
#  if [ -e ../additionals/999_open_ocd/target/stm32w1xx_flash.cfg ]; then
#    createLink ../additionals/999_open_ocd/target/stm32w1xx_flash.cfg  configs/openocd_flash.cfg
#  else
#    createLink ../additionals/999_open_ocd/target/stm32w108xx.cfg      configs/openocd_flash.cfg
#  fi
  #X createLink openocd_target_jtag.cfg                                 configs/openocd_target.cfg

# create link to target script being required by openocd to initialize target cpu
cd \$Dir_Configs
createLink ../additionals/999_open_ocd/target/stm32f108xx.cfg     openocd_target.cfg
createLink ../additionals/999_open_ocd/target/stm32w1xx_flash.cfg openocd_flash.cfg
createLink ../additionals/999_open_ocd/target/stm32w1xx_jtag.cfg  openocd_target_jtag.cfg
cd ..

else
  echo "\$0 - ERROR: No low-level stm32w1xx Driver found: Skipping activation!"
fi

END_OF_ACTIVATE
    createActivateScriptTail $Name $Dir_Extensions

    #}

  fi #}high-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ recommended: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $Dir_Extensions "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${Install_Dir}.sh QUIET "\$0"


END_OF_ACTIVATE
    createActivateScriptTail $Name $Dir_Extensions
    #}

  fi #}
  if [ "1" == "0" ]; then #{ recommended: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "../examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $Dir_Extensions "$0 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${Install_Dir}.sh QUIET "\$0"
activate.500_ttc_memory.sh QUIET "\$0"

END_OF_ACTIVATE
    createActivateScriptTail $Name $Dir_Extensions
    #}

  fi #}
  if [ "1" == "1" ]; then #{ call install scripts of low-level drivers for stm32w1xx
    cd "$Install_Path"
    cd ../installs_low_level
    LLInstallDir=`pwd`
    LowLevelInstalls=`ls 2>/dev/null install_*_STM32W1XX_*.sh`
    echo "$LLInstallDir> found LowLevelInstalls: $LowLevelInstalls"
    for LowLevelInstall in $LowLevelInstalls
    do
      cd "$Install_Path"
      echo ""
      echo    "running $LLInstallDir/$LowLevelInstall ...  "
      echo -n "        "
      source $LLInstallDir/$LowLevelInstall
    done
    cd "$Install_Path"
  fi #}low-level driver
  
  findFolderUpwards 999_open_ocd; Folder_OpenOCD="$FoundFolder"
  ConfigFile="$Folder_OpenOCD/share/openocd/scripts/target/em357_jtag.cfg"
  if [ ! -e "$ConfigFile" ]; then #{ create OpenOCD target configuration for OnChip Debugging
    #if [ ! -e "$ConfigFile" ]; then
      echo "creating cfg-file '`pwd`/$ConfigFile'.."
      cat <<END_OF_CONFIG >$ConfigFile #{
# http://TheToolchain.com
#
# OpenOCD target definition stm32w108 (former EM357)
# created by $0

if { [info exists CHIPNAME] } {
   set  _CHIPNAME \$CHIPNAME
} else {
   set  _CHIPNAME em357
}

if { [info exists ENDIAN] } {
   set  _ENDIAN \$ENDIAN
} else {
   set  _ENDIAN little
}

# Work-area is a space in RAM used for flash programming
# By default use 16kB
if { [info exists WORKAREASIZE] } {
   set  _WORKAREASIZE \$WORKAREASIZE
} else {
   #set  _WORKAREASIZE 0x4000
   set  _WORKAREASIZE 0x2000
}

# JTAG speed should be <= F_CPU/6. F_CPU after reset is 8MHz, so use F_JTAG = 1MHz
adapter_khz 200

#adapter_nsrst_delay 400
jtag_ntrst_delay 100

#verify_ircapture disable 
#verify_jtag disable


#jtag scan chain
if { [info exists CPUTAPID ] } {
   set _CPUTAPID \$CPUTAPID
} else {
  # See STM Document RM0008
  # Section 26.6.3
   set _CPUTAPID 0x3ba00477
}
jtag newtap \$_CHIPNAME cpu -irlen 4 -ircapture 0x1 -irmask 0xf -expected-id \$_CPUTAPID

if { [info exists BSTAPID ] } {
   # FIXME this never gets used to override defaults...
   set _BSTAPID \$BSTAPID
} else {
  # See STM Document RM0008
  # Section 29.6.2
  # Low density devices, Rev A
  #set _BSTAPID1 0x06412041
  # Medium density devices, Rev A
  #set _BSTAPID2 0x06410041
  # Medium density devices, Rev B and Rev Z
  #set _BSTAPID3 0x16410041
  #set _BSTAPID4 0x06420041
  # High density devices, Rev A
  #set _BSTAPID5 0x06414041
  #set _BSTAPID5 0x06418041
  # Connectivity line devices, Rev A and Rev Z
  #set _BSTAPID6 0x06418041
  # XL line devices, Rev A
  #set _BSTAPID7 0x06430041
  
  
  # set tap ID for stm32w1xx.bs
  # set _BSTAPID 0x269a862b
  
  # set tap ID for em357.bs in Telegesis ETRX357 radio module
  set _BSTAPID 0x069a962b
}

#jtag newtap \$_CHIPNAME bs -irlen 4 -ircapture 0x0e -irmask 0xf  -expected-id \$_BSTAPID
jtag newtap \$_CHIPNAME bs -irlen 4 -ircapture 0x01 -irmask 0xf  -expected-id \$_BSTAPID

	#	jtag newtap \$_CHIPNAME bs -irlen 5 -expected-id \$_BSTAPID1 \
#	-expected-id \$_BSTAPID2 -expected-id \$_BSTAPID3 \
#	-expected-id \$_BSTAPID4 -expected-id \$_BSTAPID5 \
#	-expected-id \$_BSTAPID6 -expected-id \$_BSTAPID7



set _TARGETNAME \$_CHIPNAME.cpu
#\$_TARGETNAME configure -work-area-phys 0x20000000 -work-area-size \$_WORKAREASIZE -work-area-backup 0

target create \$_TARGETNAME cortex_m -endian \$_ENDIAN -chain-position \$_TARGETNAME

\$_TARGETNAME configure -work-area-phys 0x20000000 -work-area-size \$_WORKAREASIZE -work-area-backup 0

# flash size will be probed
set _FLASHNAME \$_CHIPNAME.flash
#flash bank \$_FLASHNAME stm32f1x 0x08000000 0 0 0 \$_TARGETNAME
flash bank \$_FLASHNAME em357 0x08000000 0x00040000 0 0 \$_TARGETNAME

# if srst is not fitted use SYSRESETREQ to
# perform a soft reset
cortex_m reset_config sysresetreq

END_OF_CONFIG
#}
    #else
    #  echo "cfg-file already exists: '`pwd`/$ConfigFile'.."
    #fi
  fi #}

  ConfigFile="$Folder_OpenOCD/share/openocd/scripts/target/em357_flash.cfg"
  if [ ! -e "$ConfigFile" ]; then #{ create OpenOCD configuration to flash STM32W1xx
    echo "creating cfg-file '`pwd`/$ConfigFile'.."
    cat <<END_OF_CONFIG >$ConfigFile #{
# http://TheToolchain.com
#
# OpenOCD flash script for  stm32w108 (former EM357)
# created by $0


# finish configuration stage + proceed to run stage
init

# hardware reset processor with halt (2 times is safer than only once)
reset
soft_reset_halt

# check target state stm32w
poll

# list all found flash banks
flash banks

# identify the flash
flash probe 0

em357 unlock 0

# erasing all flash
em357 mass_erase 0

# write file to flash number 0
flash write_bank 0 main.bin 0

# hardware processor reset with run
reset run

#shutdown openocd daemon
shutdown

END_OF_CONFIG
#}
#  else
#    echo "cfg-file already exists: '`pwd`/$ConfigFile'.."
  fi #}

    ConfigFile="$Folder_OpenOCD/share/openocd/scripts/target/stm32w1xx_jtag.cfg"
#D  if [ ! -e "$ConfigFile" ]; then #{ create OpenOCD target configuration for OnChip Debugging
    #if [ ! -e "$ConfigFile" ]; then
      echo "creating cfg-file '`pwd`/$ConfigFile'.."
      cat <<END_OF_CONFIG >$ConfigFile #{
# http://TheToolchain.com
#
# OpenOCD target definition stm32w108 (former EM357)
# created by $0

if { [info exists CHIPNAME] } {
   set  _CHIPNAME \$CHIPNAME
} else {
   set  _CHIPNAME em357
}

if { [info exists ENDIAN] } {
   set  _ENDIAN \$ENDIAN
} else {
   set  _ENDIAN little
}

# Work-area is a space in RAM used for flash programming
# By default use 16kB
if { [info exists WORKAREASIZE] } {
   set  _WORKAREASIZE \$WORKAREASIZE
} else {
   #set  _WORKAREASIZE 0x4000
   set  _WORKAREASIZE 0x2000
}
set  _WORKAREASIZE 0x1000

# JTAG speed should be <= F_CPU/6. F_CPU after reset is 8MHz, so use F_JTAG = 1MHz
adapter_khz 200

#adapter_nsrst_delay 400
jtag_ntrst_delay 100

#verify_ircapture disable 
#verify_jtag disable


#jtag scan chain
if { [info exists CPUTAPID ] } {
   set _CPUTAPID \$CPUTAPID
} else {
  # See STM Document RM0008
  # Section 26.6.3
   set _CPUTAPID 0x3ba00477
}
jtag newtap \$_CHIPNAME cpu -irlen 4 -ircapture 0x1 -irmask 0xf -expected-id \$_CPUTAPID

if { [info exists BSTAPID ] } {
   # FIXME this never gets used to override defaults...
   set _BSTAPID \$BSTAPID
} else {
  # See STM Document RM0008
  # Section 29.6.2
  # Low density devices, Rev A
  #set _BSTAPID1 0x06412041
  # Medium density devices, Rev A
  #set _BSTAPID2 0x06410041
  # Medium density devices, Rev B and Rev Z
  #set _BSTAPID3 0x16410041
  #set _BSTAPID4 0x06420041
  # High density devices, Rev A
  #set _BSTAPID5 0x06414041
  #set _BSTAPID5 0x06418041
  # Connectivity line devices, Rev A and Rev Z
  #set _BSTAPID6 0x06418041
  # XL line devices, Rev A
  #set _BSTAPID7 0x06430041
  
  
  set _BSTAPID1 0x269a862b
  # set tap ID for stm32w.bs in Telegesis ETRX357 radio module
  set _BSTAPID 0x269a862b
  
  # set tap ID for em357.bs in Telegesis ETRX357 radio module
  set _BSTAPID2 0x069a962b
}

jtag newtap \$_CHIPNAME bs -irlen 4 -ircapture 0x0e -irmask 0xf  -expected-id \$_BSTAPID -expected-id \$_BSTAPID2

	#	jtag newtap \$_CHIPNAME bs -irlen 5 -expected-id \$_BSTAPID1 \
#	-expected-id \$_BSTAPID2 -expected-id \$_BSTAPID3 \
#	-expected-id \$_BSTAPID4 -expected-id \$_BSTAPID5 \
#	-expected-id \$_BSTAPID6 -expected-id \$_BSTAPID7



set _TARGETNAME \$_CHIPNAME.cpu
#\$_TARGETNAME configure -work-area-phys 0x20000000 -work-area-size \$_WORKAREASIZE -work-area-backup 0

target create \$_TARGETNAME cortex_m -endian \$_ENDIAN -chain-position \$_TARGETNAME

\$_TARGETNAME configure -work-area-phys 0x20000000 -work-area-size \$_WORKAREASIZE -work-area-backup 0

# flash size will be probed
set _FLASHNAME \$_CHIPNAME.flash

# stm32w108 has 128kB Flash
#flash bank \$_FLASHNAME em357 0x08000000 0 0 0 \$_TARGETNAME
flash bank \$_FLASHNAME em357 0x08000000 0x00020000 0 0 \$_TARGETNAME

# em357 has 128kB Flash
#flash bank \$_FLASHNAME em357 0x08000000 0x00040000 0 0 \$_TARGETNAME

# if srst is not fitted use SYSRESETREQ to
# perform a soft reset
cortex_m reset_config sysresetreq

END_OF_CONFIG
#}
    #else
    #  echo "cfg-file already exists: '`pwd`/$ConfigFile'.."
    #fi
#D  fi #}

  ConfigFile="$Folder_OpenOCD/share/openocd/scripts/target/stm32w1xx_flash.cfg"
#D  if [ ! -e "$ConfigFile" ]; then #{ create OpenOCD configuration to flash STM32W1xx
    echo "creating cfg-file '`pwd`/$ConfigFile'.."
    cat <<END_OF_CONFIG >$ConfigFile #{
# http://TheToolchain.com
#
# OpenOCD flash script for  stm32w108 (former EM357)
# created by $0


# finish configuration stage + proceed to run stage
init

# hardware reset processor with halt (2 times is safer than only once)
reset
soft_reset_halt

# check target state stm32w
poll

# list all found flash banks
flash banks


# identify the flash
flash probe 0

em357 unlock 0

# erasing all flash
em357 mass_erase 0

# write file to flash number 0
flash write_bank 0 main.bin 0

# hardware processor reset with run
reset run

#shutdown openocd daemon
shutdown

END_OF_CONFIG
#}
#  else
#    echo "cfg-file already exists: '`pwd`/$ConfigFile'.."
#D  fi #}

  cd "$Install_Path"
  echo "Installed successfully: $Install_Dir"
#}
else                            #{ Install failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$StartPath"
applyTargetUserAndGroup "$Install_Dir"

exit 0
