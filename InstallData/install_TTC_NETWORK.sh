#!/bin/bash

#
#  Install script for a set of High- and Low-Level Drivers
#  for network devices.
#
#  Supported Architectures: 6lowpan
#
#  Initial Script written by Gregor Rebel 2010-2018.
# 
#  Published under GNU Lesser General Public License
#  ChecL	k LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="500"
EXTENSION_SHORT="network"
EXTENSION_PREFIX="ttc"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$ScriptName: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$ScriptName - ERROR: Cannot find newest GCC version!"
#   exit 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe 'which autoreconf' autoreconf
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ high-level driver for network
    Name="${Install_Dir}"  
  
    createExtensionSourcefileHead ${Name}    #{ (create initializer code)
    
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

    #include "ttc-lib/ttc_network.h"
    ttc_network_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
INCLUDE_DIRS += -Ittc-lib/network/ -Ittc-lib/network/mac_ieee_802_15_4 -Ittc-lib/network/support

# additional directories to search for C-Sources
vpath %.c ttc-lib/network/ ttc-lib/network/mac_ieee_802_15_4/ ttc-lib/network/support/

# additional object files to be compiled
MAIN_OBJS += ttc_network.o ttc_network_interface.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "high-level network Driver" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/*${Install_Dir}_*

for LowLevelActivate in \`ls \${Dir_Extensions}/activate.450_network_*\`; do
  
  # try to activate this low-level driver
  \$LowLevelActivate QUIET "\$ScriptName"
  
  # extract driver name from script name
  LowLevelDriver=\`perl -e "print substr('\$LowLevelActivate', 9, -3);"\`
  
  #DISABLED echo "\$ScriptName - LowLevelDriver='\$LowLevelDriver'"
  DriverFound="1"
done

if [ "\$DriverFound" != "" ]; then

  # ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

  # ACTIVATE_SECTION_C activate initialization source-code for this extension 
  createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

  # ACTIVATE_SECTION_D call other activate-scripts
  # activate.500_ttc_memory.sh QUIET "\$ScriptName"
else
  echo "\$ScriptName - ERROR: No low-level network Driver found: Skipping activation!"
fi

END_OF_ACTIVATE
    createActivateScriptTail $Name ${Dir_Extensions}
    #}
  
  fi #}high-level driver
  if [ "1" == "1" ]; then #{ low-level driver for network on IEEE 802.15.4
  
    Name2="450_network_IEEE_802_15_4"
    createExtensionMakefileHead ${Name2}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

EXTENSION_MAC_CSMA=1	# activate to include this feature

COMPILE_OPTS += -DTTC_NETWORK1_MAC=1
    
MAIN_OBJS += network_adapt_random.o network_adapt_memb.o network_adapt_list.o \
	     network_adapt_ctimer.o network_adapt_rtimer.o 

MAIN_OBJS += mac_ieee802_15_4_nullrdc.o mac_ieee802_15_4.o mac_ieee802_15_4_rdc.o \
	     mac_ieee802_15_4_frame802154.o mac_ieee802_15_4_framer_802154.o     

ifdef EXTENSION_MAC_CSMA
  MAIN_OBJS += mac_ieee802_15_4_csma.o
else
  MAIN_OBJS += mac_ieee802_15_4_nullmac.o mac_ieee802_15_4_framer_nullmac.o
endif
    
MAIN_OBJS += network_rime_packetbuf.o network_rimeaddr.o network_rime_queuebuf.o

MAIN_OBJS += network_serialradio.o
#MAIN_OBJS += network_nullradio.o

ifndef EXTENSION_network_6lowpan_uip
  MAIN_OBJS += mac_ieee802_15_4_macstack.o 
  #network_6lowpan_framer_nullmac.o 
endif

MAIN_OBJS += network_mac.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name2} #}
    createActivateScriptHead $Name2 ${Dir_Extensions} "$ScriptName 2" "low-level network Driver for IEEE 802.15.4 architecture" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name2 \${Dir_ExtensionsActive}/makefile.$Name2 '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
#activate.500_ttc_memory.sh QUIET "\$ScriptName"
#activate.500_ttc_list.sh QUIET "\$ScriptName"
#activate.500_ttc_timer.sh QUIET "\$ScriptName"
#activate.500_ttc_random.sh QUIET "\$ScriptName"

END_OF_ACTIVATE
    createActivateScriptTail $Name2 ${Dir_Extensions}
  #}
 
fi #}low-level driver
  if [ "1" == "0" ]; then #{ low-level driver for network on 6lowpan
    Name3="450_network_6lowpan_uip"
    createExtensionMakefileHead ${Name3}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

COMPILE_OPTS += -DTTC_NETWORK1_NETWORK=1

ifndef EXTENSION_network_IEEE_802_15_4
MAIN_OBJS += network_adapt_random.o network_adapt_memb.o network_adapt_list.o \
	     network_adapt_ctimer.o network_adapt_rtimer.o	    
MAIN_OBJS += mac_ieee802_15_4_nullrdc.o mac_ieee802_15_4_nullrdc-noframer.o \
	     mac_ieee802_15_4.o mac_ieee802_15_4_rdc.o mac_ieee802_15_4_nullmac.o
MAIN_OBJS += network_rimeaddr.o network_rime_packetbuf.o
MAIN_OBJS += network_serialradio.o		#network_nullradio.o
MAIN_OBJS += network_6lowpan_framer_nullmac.o
else
MAIN_OBJS += network_6lowpan_framer_802154.o
endif	     

MAIN_OBJS += network_6lowpan_netstack.o network_6lowpan_queuebuf.o 
#MAIN_OBJS +=


ifdef DISABLED_FOO
MAIN_OBJS +=  6lowpan_net_neighbor_attr.o 6lowpan_net_neighbor_info.o \
              6lowpan_net_sicslowpan.o 6lowpan_net_tcpip.o 6lowpan_net_uip_arp.o \
              6lowpan_net_uip.o 6lowpan_net_uip_debug.o 6lowpan_net_uip_ds6.o \
              6lowpan_net_uip_fw.o 6lowpan_net_uip_icmp6.o 6lowpan_net_uip_nd6.o \
              6lowpan_net_uip_packetqueue.o 6lowpan_net_uip_split.o 6lowpan_adapt_watchdog.o \
              6lowpan_adapt_etimer.o

                         
# 6lowpan_adapt_compower.o 6lowpan_adapt_energest.o  \
# 6lowpan_adapt_leds.o 6lowpan_mac_contikimac.o
# 6lowpan_rime_rime.o 6lowpan_rime_rimestats.o 6lowpan_mac_nullrdc_framer.o 
    
endif 
    
MAIN_OBJS += network_6lowpan.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name3} #}
    createActivateScriptHead $Name3 ${Dir_Extensions} "$ScriptName 2" "low-level network Driver for 6lowpan architecture" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name3 \${Dir_ExtensionsActive}/makefile.$Name3 '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts

#ifndef EXTENSION_network_6lowpaqn_uip
#activate.500_ttc_memory.sh QUIET "\$ScriptName"
#activate.500_ttc_list.sh QUIET "\$ScriptName"
#activate.500_ttc_timer.sh QUIET "\$ScriptName"
#activate.500_ttc_random.sh QUIET "\$ScriptName"
#endif

#activate.500_ttc_watchdog.sh QUIET "\$ScriptName"

END_OF_ACTIVATE
    createActivateScriptTail $Name3 ${Dir_Extensions}
  #}

fi #}low-level driver
  if [ "1" == "0" ]; then #{ create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "PLACE YOUR INFO HERE FOR $ScriptName" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${Install_Dir}.sh QUIET "\$ScriptName"


END_OF_ACTIVATE
    createActivateScriptTail $Name ${Dir_Extensions}
    #}

  fi #}
  if [ "1" == "1" ]; then #{ create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${Install_Dir}.sh QUIET "\$ScriptName"

END_OF_ACTIVATE
    createActivateScriptTail $Name ${Dir_Extensions}
    #}

  fi #}

  cd "$OldPWD"
  echo "Installed successfully: $Install_Dir"
#}
else                            #{ Install failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
