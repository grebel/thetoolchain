#!/bin/bash

Files=`ls install_*.sh`
  for Script in $Files; do
    IsUpgradable=`grep FINDNEWEST $Script`
    if [ "$IsUpgradable" != "" ]; then
      #echo "updating: $Script"
      ./$Script FINDNEWEST
    fi
  done
