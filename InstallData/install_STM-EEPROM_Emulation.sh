#!/bin/bash
#
#  Install script for ST Standard Peripherals Library
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "400_stm32f1xx_eeprom_emulation" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00165693.pdf         AN2594_EEPROM_emulation_in_STM32F10x_microcontrollers.pdf    uC/STM/EEPROM_Emulation
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then #{

  if [ ! -e OK.Packages ]; then #{
    installPackage libgmp3-dev
    installPackage libmpfr-dev
    installPackage xchm
    installPackage chmlib
    installPackage chmsee
    installPackage kchmviewer
    touch OK.Packages
  fi #}
  DirName="STM_EEPROM_Emulation"
  Archive="stsw-stm32010.zip"
  # getFile http://www.st.com/stonline/products/support/micro/files/ $Archive
  #getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ $Archive
 getFile http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/firmware/ $Archive
  # find name ofu only directory
  # std_peripherals_library/STM32F10x_StdPeriph_Lib_V3.4.0/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h
  unZIP $DirName $Archive || mv -f $Archive ${Archive}_bad
  

  
  rm current 2>/dev/null
  LibraryDir=`find ./  -maxdepth 1 -mindepth 1 -type d -print`
  createLink "$LibraryDir/Project/EEPROM_Emulation/" current
  cd current
  dir ../${DocDir}
  mv *.chm ../${DocDir}/

  cd ..

  echo "" >OK.Install

fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then #{
  Folder="StdPeripheral_Examples"
#  createLink $HOME/TheToolChain/InstallData/200_cpu_stm32f1xx_std_peripherals/current/Project/STM32F10x_StdPeriph_Examples $HOME/TheToolChain/Documentation/ST-StandardPeripheralsLibrary/$Folder
# createLink $HOME/TheToolChain/InstallData/$Install_Dir/current/          $HOME/TheToolChain/Documentation/ST-StandardPeripheralsLibrary/$Folder/Common
  
  Name="${Install_Dir}"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

COMPILE_OPTS += -DEXTENSION_${Name}=1

INCLUDE_DIRS += -I additionals/$Name/inc/ 

vpath %.c additionals/$Name/src/ 

MAIN_OBJS += stm32_eeprom_emulation.o \\
             eeprom.o \
             

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}

  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Example of how to use architecture independent EEPROM emulation" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.600_example_*

# create links into extensions.active/
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

activate.250_CPAL_STM32F10x_StdPeriph_Driver.sh QUIET \"\$ScriptName\" || exit 10
activate.250_stm_std_peripherals__flash.sh      QUIET \"\$ScriptName\" || exit 10

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
#}

  Name="500_ttc_emulation"
    createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

  #include "ttc-lib/ttc_eeprom_emulation.h"
  ttc_eeprom_emulation_init();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

COMPILE_OPTS += -DEXTENSION_${Name}=1

INCLUDE_DIRS += -I additionals/$Name/ 

vpath %.c additionals/$Name/

MAIN_OBJS += ttc_eeprom_emulation.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
#  createActivateScriptHead $Name ${Dir_Extensions} #{
#
#  # add createLinks for each link required for this extension
#  echo "createLink \$ScriptPath/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET" >>$ActivateScriptFile
#  
#  createActivateScriptTail $Name ${Dir_Extensions} 
#  #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Example of how to use architecture independent EEPROM emulation" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.600_example_*

# create links into extensions.active/
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

#activate.250_CPAL_STM32F10x_StdPeriph_Driver.sh QUIET \"\$ScriptName\" || exit 10
#activate.250_stm_std_peripherals__flash.sh      QUIET \"\$ScriptName\" || exit 10

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
#}
  addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; createLink \$Source/TheToolChain/InstallData/$Install_Dir/current/  $Install_Dir"
  echo "Installed successfully: $Install_Dir"
#}
else
  echo "failed to install $Install_Dir"
  exit 10
fi

#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
