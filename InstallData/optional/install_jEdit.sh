#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a set of High- and Low-Level Drivers
#  for jEdit text editor.
#
#  Created from template <TEMPLATE_FILE> revision 21 at <DATE>
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

StartPath="`pwd`"
RANK="999"
EXTENSION_NAME="jEdit"
Install_Dir="${RANK}_${EXTENSION_NAME}"
dir "$Install_Dir"
cd "$Install_Dir"
Install_Path=`pwd`

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   exit 10
# fi #}
#}look for newest versions

if [ ! -e OK\.Documentation ];    then    #{ (download documentation)

  Get_Fails=""
  
  # download doc-file + move to Documentation/ + create symbolic link into Documentation/ right here 
  # File="STM32F10x_Standard_Peripherals_Library.html"
  # getFile http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library $File
  # addDocumentationFile $File STM
  
  if [ "$Get_Fails" == "" ]; then #{ check get() inside scripts/installFuncs.sh 
    touch OK\.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    installPackageSafe "which java" java
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
  getFile http://cznic.dl.sourceforge.net/project/jedit/jedit/5.1.0/ jedit5.1.0install.jar jedit-install.jar
  java -jar jedit-install.jar auto `pwd` unix-script=`pwd`/../bin/
  getFile http://www.thetoolchain.com/mirror/ jedit_settings.tgj
  cd 
  if [ -d .jedit ]; then
    if [ ! -d .jedit_old ]; then
      mv .jedit .jedit_old
    fi
  fi
  if [ ! -d ".jedit" ]; then
    untgj ".jedti" ${Install_Path}/jedit_settings.tgj
    sudo chown -R $USER:$USER .jedit
  fi

    if [ -d ".jedit" ]; then
      echo "" >OK.Install
    fi
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts

  cd "$Install_Path"
  echo "Installed successfully: $Install_Dir"
#}
else                            #{ Install failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$StartPath"
applyTargetUserAndGroup "$Install_Dir"

exit 0
