#!/bin/bash
#
#  Install script for open source GCC G++ compiler for CortexM3 MicroControllers. 
#  Script written by Gregor Rebel 2010-2018.
#
# Reference: http://michaldemin.wordpress.com/2010/02/09/arm-toolchain-newlib-part1/
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:

export PATH="`pwd`/scripts:$PATH"
source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

if [ "$1" = "RECOMPILE" ]; then
  RECOMPILE="1"  # gcc recompilation forced
else
  RECOMPILE=""
fi

RANK="050"
EXTENSION_NAME="compiler_gcc_cortex_m3_wchar16"

StartPath="`pwd`"
Install_Dir="${RANK}_${EXTENSION_NAME}"
dir "$Install_Dir"
cd "$Install_Dir"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------


if [ ! -e OK\.Documentation ];    then    #{ (download documentation)

  Get_Fails=""
  
  # download doc-file + move to Documentation/ + create symbolic link into Documentation/ right here 
  # File="STM32F10x_Standard_Peripherals_Library.html"
  # getFile http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library $File
  # addDocumentationFile $File STM
  getFile https://launchpadlibrarian.net/151487323/                  How-to-build-toolchain.pdf                 Build_process_of_GNU_Tools_for_ARM_Embedded_Processors.pdf ../../Documentation/Compiler/ARM/ARM_Compiler_Toolchain/
  getFile http://infocenter.arm.com/help/topic/com.arm.doc.ihi0036b/ IHI0036B_bsabi.pdf                         ABI_for_the_ARM_Architecture-Base_Standard.pdf             ../../Documentation/Compiler/ARM/ARM_Compiler_Toolchain/
  getFile http://infocenter.arm.com/help/topic/com.arm.doc.ihi0044e/ IHI0044E_aaelf.pdf                         ELF_for_the_ARM_Architecture.pdf                           ../../Documentation/Compiler/ARM/ARM_Compiler_Toolchain/
  getFile http://infocenter.arm.com/help/topic/com.arm.doc.dui0492i/ DUI0492I_arm_libraries_reference.pdf       ARM_C_and_C++_Libraries_and_Floating-Point_Support.pdf     ../../Documentation/Compiler/ARM/ARM_Compiler_Toolchain/
  getFile http://infocenter.arm.com/help/topic/com.arm.doc.dui0471i/ DUI0471I_developing_for_arm_processors.pdf Developing_Software_for_ARM_Processors.pdf                 ../../Documentation/Compiler/ARM/ARM_Compiler_Toolchain/
  getFile http://infocenter.arm.com/help/topic/com.arm.doc.dui0552a/ DUI0552A_cortex_m3_dgug.pdf                Cortex-M3_Devices-Generic_User_Guide.pdf                   ../../Documentation/uC/CortexM3/
  getFile http://infocenter.arm.com/help/topic/com.arm.doc.dui0552a/ DUI0552A_cortex_m3_dgug.pdf                Cortex-M3_Devices-Technical_Reference_Manual.pdf           ../../Documentation/uC/CortexM3/
  getFile http://infocenter.arm.com/help/topic/com.arm.doc.dui0497a/ DUI0497A_cortex_m0_r0p0_generic_ug.pdf     Cortex-M0_Devices-Generic_User_Guide.pdf                   ../../Documentation/uC/CortexM0/
  getFile http://infocenter.arm.com/help/topic/com.arm.doc.ddi0432c/ DDI0432C_cortex_m0_r0p0_trm.pdf            Cortex-M0_Devices-Technical_Reference_Manual.pdf           ../../Documentation/uC/CortexM0/
  
  if [ "$Get_Fails" == "" ]; then #{ check get() inside scripts/installFuncs.sh 
    touch OK\.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}


#? if [ "`sudo grep -R xorg-edgers /etc/apt/`" == "" ]; then
#?   sudo add-apt-repository -y ppa:xorg-edgers/ppa 
#?   sudo apt-get update -y
#?   sudo apt-get dist-upgrade -y
#? fi

ARM_GCC="ARM-NONE-EABI-GCC"
Dir_ARM_GCC="`pwd`/$ARM_GCC"
Prefix_ARM_GCC="gcc-arm-none-eabi-wchar16-current"
Archive_GCC_ARM="$Prefix_ARM_GCC-linux.tar.bz2"
Link_ARM_GCC_Current="`pwd`/$Prefix_ARM_GCC"

if [ "$RECOMPILE" == "1" ]; then
  echo "$0 forcing recompilation of arm gcc toolchain"
  # remove some OK flags to force building toolchain
  for File in OK.Install OK.BuildToolChain OK.$ARM_GCC OK.CompilerInstall; do
    echo "deleting OK-flag '$File' ..."
    find ./ -name $File -exec rm -v {} \;
  done
fi

if [ ! -e OK.Packages ]; then #{
  for Run in 1 2 3; do # some installs may fail in first run
    echo "Installation run #$Run ..."
    FailedInstalls=""
    installPackageSafe "which guile"                                      guile-1.6
    installPackageSafe "which guile"                                      guile-1.8  # retry v1.8
    installPackageSafe "which pp64"                                       polylib-utils
    installPackageSafe "ls /usr/lib/libpolylib64.so"                      libpolylib64-dev
    installPackageSafe "which ppl-config"                                 ppl-dev
    installPackageSafe "find /usr/include/ -name isl"                     libisl-dev
    installPackageSafe "which bison"                                      bison
    installPackageSafe "which apt-src"                                    apt-src
    installPackageSafe "which gawk"                                       gawk
    installPackageSafe "which gzip"                                       gzip
    installPackageSafe "which perl"                                       perl
    installPackageSafe "which autoconf"                                   autoconf
    installPackageSafe "which m4"                                         m4
    installPackageSafe "which automake"                                   automake
    installPackageSafe "which libtool"                                    libtool
    installPackageSafe "find /usr/include/ -name curses.h"                libncurses5-dev:i386
    installPackageSafe "find /usr/include/ -name curses.h"                ncurses-dev
    installPackageSafe "which gettext"                                    gettext
    installPackageSafe "which gperf"                                      gperf
    installPackageSafe "find /usr/include/ -name dejagnu.h"               dejagnu
    installPackageSafe "which expect"                                     expect
    installPackageSafe "which tclsh"                                      tcl
    installPackageSafe "which autogen"                                    autogen
    installPackageSafe "which flex"                                       flex
    installPackageSafe "which flip"                                       flip
    installPackageSafe "which bison"                                      bison
    installPackageSafe "which todos"                                      tofrodos
    installPackageSafe "which makeinfo"                                   texinfo
    installPackageSafe "which g++"                                        g++
    installPackageSafe "ls /usr/share/doc/gcc-multilib"                   gcc-multilib
    installPackageSafe "which i586-mingw32msvc-cc"                        mingw32
    installPackageSafe "which i586-mingw32msvc-size"                      mingw32-binutils
    installPackageSafe "ls /usr/share/doc/mingw32-runtime"                mingw32-runtime
    installPackageSafe "find /usr/include/ -name gmp.h"                   libgmp3-dev
    installPackageSafe "find /usr/include/ -name mpfr.h"                  libmpfr-dev
    installPackageSafe "which dh_clean"                                   debhelper
    installPackageSafe "ls /usr/share/doc/texlive"                        texlive
    installPackageSafe "which bibtex8"                                    texlive-extra-utils
    installPackageSafe "which gcc"                                        gcc
    installPackageSafe "which g++"                                        g++
    IsInstalled="`ls -l /usr/share/doc/ | grep libgcc | grep dev`"
    if [ "$IsInstalled" == "" ]; then
      installPackageSafe "" libgcc-4.8-dev
    fi
  done
  
  if [ "$FailedInstalls" == "" ]; then
    touch OK.Packages
  else
    echo "$0 - ERROR: Failed to install $FailedInstalls"
    exit 10
  fi
fi #}
if [ ! -e OK.Install ]; then #{

echo "installing in $Install_Dir ..."
CleanUpBatch="`pwd`/cleanup.sh"

dir $Dir_ARM_GCC

if [ "$RECOMPILE" == "" ]; then #{ try to download + install prebuilt compiler binary
Section="CompilerInstall"
if [ ! -e "OK.$Section" ]; then #{
  cd $Dir_ARM_GCC
  getFile http://thetoolchain.com/mirror/ $Archive_GCC_ARM $Archive_GCC_ARM 
  
  Archive="$Archive_GCC_ARM"
  if [ -e "$Archive" ]; then
    echo "unpacking archive $Archive ..."
    ERROR=""
    tar xjf $Archive || ERROR="`pwd` > Cannot unpack archive '$Archive'!"
    if [ "$ERROR" != "" ]; then
      echo "$0 ERROR: $ERROR"
      exit 24
    else
      Dir="`find ./ -maxdepth 1 -type d | sort | tail -1`"
      createLink "$Dir" $Prefix_ARM_GCC
      touch "../OK.$Section"
    fi
    cd ..
  fi
  else
    echo "already complete: `pwd`/OK.$Section"
fi #}
cd $Dir_ARM_GCC
# echo "cd `pwd`; ./$Prefix_ARM_GCC/bin/arm-none-eabi-gcc --version | grep arm-none-eabi-gcc"
ArmCompilerVersion="`./$Prefix_ARM_GCC/bin/arm-none-eabi-gcc --version | grep arm-none-eabi-gcc`"
cd ..
fi #}RECOMPILE

if [ "$ArmCompilerVersion" == "" ]; then #{ compiler was not downloaded: Try to compile from source (must run on Ubuntu 8.10!)
  identifyOS
  if [ "$OS_VERSION" != "8.10" ]; then #{ wrong OS for building native gcc
    getFile https://launchpadlibrarian.net/151487323/ How-to-build-toolchain.pdf
    DocFile="`pwd`/How-to-build-toolchain.pdf"
  
    UbuntuISO="ubuntu-8.10-desktop-i386.iso"
    getFile http://old-releases.ubuntu.com/releases/8.10/ $UbuntuISO
    UbuntuISO="`pwd`/$UbuntuISO"
    
    cat <<END_OF_INFO


=========================================================================================================================
$0 - ERROR: Building the arm gcc toolchain is only supported if running on Ubuntu 8.10!

It is advised to install Ubuntu 8.10 in a virtual machine as described by
$DocFile

You may use this image for installation:
$UbuntuISO

Copy TheToolChain into this virtual machine and rerun this script!

END_OF_INFO
      exit 10
  fi #}wrong OS
  
  Section="GCC-Native"
  if [ ! -e OK.${Section} ]; then  #{ need to build a native gcc in order to build toolchain..
    SectionDir="`pwd`"
    Dir_Target="`pwd`/$Section/"
    echo ""
    echo "---------------------------------------------------------"
    echo "$0 - Installing section ${Section} {"
    echo "---------------------------------------------------------"
    echo ""
  
    dir ${Section}.temp
    cd ${Section}.temp
  
    identifyOS # loads $OS_TYPE, $OS_SUBTYPE, $OS_VERSION, $OS_ARCHITECTURE according to current operating system
    if [ "$OS_ARCHITECTURE" == "x86_64" ]; then
      Folder="/usr/lib/gcc/x86_64-linux-gnu"
    else
      Folder="/usr/lib/gcc/i686-linux-gnu"
    fi
    GCC_LibVersion="`ls -1 $Folder | sort | tail -n 1`"
    GCC_Library="$Folder/$GCC_LibVersion"
    
    GCC_version="4.3.6"
    GCC_Dir="gcc-${GCC_version}"
    getFile ftp://ftp.gnu.org/gnu/gcc/${GCC_Dir}/ ${GCC_Dir}.tar.bz2 gcc-native.tar.bz2
    untgj "${GCC_Dir}" gcc-native.tar.bz2
    if [ ! -d "${GCC_Dir}" ]; then
      echo "$0 - ERROR: Cannot download plain ${GCC_Dir} (missing folder '`pwd`/${GCC_Dir}/')"
    fi
    
    cd ${GCC_Dir}
    dir build
    rm -Rf build/* # delete objects from previous build
    cd build
    createLink $Dir_GCC_Native_Prerequisites prerequisites
    
    Configure="../configure --build=i686-linux-gnu --host=i686-linux-gnu --target=i686-linux-gnu --enable-languages=c,c++"
    Configure="$Configure --enable-shared --enable-threads=posix --disable-decimal-float --disable-libffi --disable-libgomp"
    Configure="$Configure --disable-libmudflap --disable-libssp --disable-libstdcxx-pch --disable-multilib --disable-nls"
    Configure="$Configure --with-gnu-as --with-gnu-ld --enable-libstdcxx-debug --enable-targets=all --enable-checking=release"
    Configure="$Configure --prefix='$Dir_Target' --with-host-libstdcxx='-static-libgcc -L $GCC_Library/ -lstdc++ -lsupc++ -lm' "
    # Configure="$Configure --with-gmp=prerequisites --with-mpfr=prerequisites --with-mpc=prerequisites --with-ppl=prerequisites --with-cloog=prerequisites"
    
    echo "$Configure" >configure.sh
    Make="make -j$AmountCPUs"
    Install="make install"
    
    OK=""
    Last="Configure" && echo "`pwd`> $Configure" && bash ./configure.sh >log.Configure 2>&1 && OK="1"
    if [ "$OK" == "1" ]; then
      OK=""
      
      # disable building texinfo (syntax incompatible with texinfo >= 5.0.1) 
      replaceInFile Makefile "MAKEINFO = makeinfo" "MAKEINFO = ./makeinfo"
      echo "#!/bin/bash" >makeinfo
      chmod +x makeinfo
      
      Last="Make"      && echo "`pwd`> $Make"      && `$Make      >log.Make      2>&1` && \
      Last="Install"   && echo "`pwd`> $Install"   && `$Install   >log.Install   2>&1` && \
      OK="1"
      
    fi
    
    cd .. # build/
    cd .. # ${GCC_Dir}
    
    cd "$SectionDir"
    if [ "$OK" == "1" ]; then #{
      Line="export PATH=\"${Dir_Target}/bin:\$PATH\""
      if [ "`grep '${Dir_Target}' $HOME/.bashrc`" == "" ]; then
        echo "appending '$Line' to $HOME/.bashrc .."
        echo "$Line" >>$HOME/.bashrc
      fi
      
      echo "$0 - ${Section} compiled successfully"
      rm -Rf ${GCC_Dir}
      rm -Rf ${Section}.temp
      touch OK.${Section}
      echo "$0 - Finished section ${Section} }"
      echo "---------------------------------------------------------"
      echo ""
    else
      echo "$0 - Error while compiling GCC-Native (Last=$Last)!"
      exit 11
    fi #}
  fi #}
  source $HOME/.bashrc
  
  GCC_Installed=`gcc --version | grep 4.3.6`
  if [ "$GCC_Installed" != "" ]; then #{ check for correct native gcc
    echo "Compiler installed successfully: $GCC_Installed"
  else
    echo "$0 - ERROR: Wrong compiler found! (`which gcc` is `gcc --version`)"
    exit 13
  fi #}
  
  Section="$ARM_GCC"
  if [ ! -e OK.${Section} ]; then  #{ need to build a native gcc in order to build toolchain..
    SectionDir="`pwd`"
    echo ""
    echo "---------------------------------------------------------"
    echo "$0 - Installing section ${Section} {"
    echo "---------------------------------------------------------"
    echo ""
    dir $Dir_ARM_GCC
    dir ${Section}.temp
    cd ${Section}.temp
    addLine "$CleanUpBatch" "rm -Rf `pwd`"
  
    findNewest https://launchpad.net/gcc-arm-embedded/+download "gcc-arm-none-eabi-.*-src.tar.bz2"
    getFile $NewestURL $NewestFile
    Archive_GCC_Source="$NewestFile"
    #getFile https://launchpad.net/gcc-arm-embedded/4.7/4.7-2013-q3-update/+download/ gcc-arm-none-eabi-4_7-2013q3-20130916-src.tar.bz2 gcc-arm-none-eabi-src.tar.bz2
    
    Folder=`perl -e "my \\$P=rindex('$NewestFile', '-src'); print substr('$NewestFile', 0, \\$P).\"\n\";"`
    untgj "$Folder" $Archive_GCC_Source
    cd "$Folder"
    if [ ! -e OK.UnpackPrerequisites ]; then #{
      cd  src
      echo "unpacking gcc prerequisits..."
      find -name "*.tar.*" | xargs -I% tar -xf %
      cd zlib-1.2.5
      patch -p1 <../zlib-1.2.5.patch
      cd ../
      cd ../
      touch OK.UnpackPrerequisites
    fi #}
  
    Stage="BuildPrerequisites"
    if [ ! -e OK.$Stage ]; then #{
      echo "$Stage..."
      LogFile="log.$Stage"
     ./build-prerequisites.sh --skip_mingw32 >$LogFile 2>&1
      Errors=`grep Error $LogFile`
      if [ "$Errors" != "" ]; then
        echo "$0 - Error occured: $Errors"
        echo "Check log-file for details: `pwd`/$LogFile"
        exit 21
      else
        touch OK.$Stage
      fi
    else
      echo "already complete: `pwd`/OK.$Stage"
    fi #}
    Stage="BuildToolChain"
    if [ ! -e OK.$Stage ]; then #{
      echo "$Stage..."
      LogFile="log.$Stage"
      export CFLAGS="$CFLAGS -fshort-wchar"
      replaceInFile build-toolchain.sh "CFLAGS_FOR_TARGET '-g" "CFLAGS_FOR_TARGET '-fshort-wchar -g"
      replaceInFile build-toolchain.sh "make -j\$JOBS" "make -j\$JOBS CFLAGS=\"-fshort-wchar\" "
      chmod +x build-toolchain.sh
      ./build-toolchain.sh --skip_mingw32     >$LogFile 2>&1
      if [ -e pkg/md5.txt ]; then
        cd pkg
        ToolChainBinary=`ls *linux*bz2`
        ToolChainSource=`ls *src*bz2`
        ln $ToolChainBinary $Dir_ARM_GCC/$ToolChainBinary
        ln $ToolChainSource $Dir_ARM_GCC/$ToolChainSource
        cat <<END_OF_INFO

ARM-GCC toolchain package has been compiled successfully.

END_OF_INFO
        ln -sfv $ToolChainBinary $Dir_ARM_GCC/$Archive_GCC_ARM
        ln -sfv $ToolChainSource $Dir_ARM_GCC/$Prefix_ARM_GCC-src.tar.bz2
        touch OK.$Stage
        OK="1"
      else
        Errors=`grep Error $LogFile`
        echo "$0 - Error occured: $Errors"
        echo "Check log-file for details: `pwd`/$LogFile"
        exit 22
      fi
    else
      echo "already complete: `pwd`/OK.$Stage"
    fi #}
  
    cd "$SectionDir"
    if [ "$OK" == "1" ]; then #{
      
      echo "$0 - ${Section} compiled successfully"
      rm -Rf ${GCC_Dir}
      #rm -Rf ${Section}.temp
      touch OK.${Section}
      echo "$0 - Finished section ${Section} }"
      echo "---------------------------------------------------------"
      echo ""
    else
      echo "$0 - Error during section ${Section}!"
      exit 11
    fi #}
    else
      echo "already complete: `pwd`/OK.$Section"
  fi #}
  
  echo ""
  echo ""
  echo "compilation successfull: installing compiler in next recursion..." 
  echo ""
  cd "$StartPath"
  $0
  exit 0

fi #}

  touch OK.Install

fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then #{
  Dir_ARM_GCC_Install="$Install_Dir/$ARM_GCC/$Prefix_ARM_GCC"
  Dir_ARM_GCC_Binaries="$Dir_ARM_GCC_Install/bin"
  echo "Found compiler '$ArmCompilerVersion'"
  echo "ARM-GCC binaries in '$Dir_ARM_GCC_Binaries'"
  # install symlink into additionals/
  addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/$Dir_ARM_GCC_Install $Install_Dir"

  dir ../extensions
  Name="${Install_Dir}"  
  createExtensionMakefileHead ${Name}      #{ (create makefile)
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
#MAIN_OBJS += 

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ "$0 2" "ARM GCC Compiler using 16 bit wide WCHAR datatype" #{ (create activate script)
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/
#   \$Dir_Bin               -> bin/

# ACTIVATE_SECTION_A remove activated variants of same type
rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
# createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

cd \$Dir_Bin
find ../additionals/$Install_Dir/bin/ -name "arm-none-eabi*" -exec ln -sfv {} . \;

# ACTIVATE_SECTION_D call other activate-scripts
# activate.500_ttc_memory.sh QUIET "\$0"

./arm-none-eabi-gcc --version
./arm-none-eabi-gcc -dM -E - < /dev/null | grep WCHAR

END_OF_ACTIVATE
  createActivateScriptTail $Name ../extensions/
  #}
  
  if [ "1" == "0" ]; then #{ create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/$ARM_GCC/$Prefix_ARM_GCC $Install_Dir"
  fi #} 
  if [ "1" == "0" ]; then #{ create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ../extensions/ "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${Install_Dir}.sh QUIET "\$0"


END_OF_ACTIVATE
    createActivateScriptTail $Name ../extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ../extensions/ "$0 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${Install_Dir}.sh QUIET "\$0"

END_OF_ACTIVATE
    createActivateScriptTail $Name ../extensions/
    #}

  fi #}
    
  cd "$OldPWD"
  echo "Installed successfully: $Install_Dir"
#}
else
  echo "failed to install $Install_Dir"
  exit 10
fi

#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$StartPath"
applyTargetUserAndGroup "$Install_Dir"

exit 0

if [ "1" == "0" ]; then #{ DEPRECATED

echo "-------------------------------" #D

if [ "1" == "0" ]; then #{ DEPRECATED (old stuff)

export TOOLPATH="`pwd`/bin/"
#export ARMTARGET="arm-softfloat-elf"
export ARMTARGET="arm-none-eabi"


InstallFile="binutils.tar.bz2"
if [ ! -e "OK.$InstallFile" ]; then #{
  rm -Rf binutils-*
  getNewestFile http://ftp.gnu.org/gnu/binutils/ "binutils-..*bz2\$" "$InstallFile"
  if [ -e "$InstallFile" ]; then #{
    tar xjf "$InstallFile"
    Dir=`find ./ -maxdepth 1 -type d -name "binutils-*"`
    if [ "$Dir" != "" ]; then
      echo "compiling $NewestFile ..."
      createLink $Dir binutils
      cd $Dir
      sed -i -e 's/@colophon/@@colophon/' \
             -e 's/doc@cygnus.com/doc@@cygnus.com/' bfd/doc/bfd.texinfo
      dir build
      cd build
      make distclean >../log.clean 2>&1
      OK=""
      CONFIGURE="../configure --target=$ARMTARGET --prefix=$TOOLPATH --enable-interwork --enable-multilib --with-gnu-as --with-gnu-ld --disable-nls"
      COMPILE="make -j$AmountCPUs all"
      #COMPILE="make all"
      echo "`pwd`> $CONFIGURE"
      $CONFIGURE >../log.configure 2>&1 && \
      echo "`pwd`> $COMPILE" && \
      $COMPILE >../log.compile 2>&1 && \
      make install >../log.install 2>&1 && OK="1"
      cd ..
      if [ "$OK" == "" ]; then
        echo "$0 - Error occured during compiling binutils."
        echo "Check log-files in `pwd` for details!"
        exit 11
      fi
      cd ..
    fi
    if [ -e bin/bin/${ARMTARGET}-objdump ]; then
      rm -Rf $Dir binutils
      touch "OK.$InstallFile"
      echo "Successfully compiled $NewestFile"
    fi
  else
    echo "$0 - ERROR: missing file '$InstallFile'!"
    exit 10
  fi #}
fi #}

InstallFile="gmp.tar.bz2"
if [ ! -e "OK.$InstallFile" ]; then #{
  findNewest "ftp://ftp.gmplib.org/pub/" "gmp-.+" "$InstallFile"
  if [ "$Error_findNewest" != "" ]; then #{
    echo "$0 - ERROR: $Error_findNewest"
    exit 11
  fi #}
  getFile "ftp://ftp.gmplib.org/pub/${NewestDir}/" "${NewestDir}.tar.bz2" "$InstallFile"

  if [ -e "$InstallFile" ]; then #{
    tar xjf "$InstallFile"
    Dir=`find ./ -maxdepth 1 -type d -name "gmp-*"`
    if [ "$Dir" != "" ]; then
      echo "compiling $NewestDir ..."
      createLink $Dir gmp
      cd $Dir
      ./configure --enable-cxx >log.configure 2>&1 && \
      make -j$AmountCPUs >log.compile 2>&1 && \
      make -j$AmountCPUs check >log.install 2>&1 && \
      touch ../OK.$InstallFile
      cd ..
    fi
    if [ -e OK.$InstallFile ]; then
      echo "compiled successfully: $NewestDir"
    else
      echo "$0 - ERROR: failed to install '$InstallFile'!"
      exit 11
    fi
  else
    echo "$0 - ERROR: missing file '$InstallFile'!"
    exit 10
  fi #}
fi #}
Dir_GMP="`pwd`/gmp/"

InstallFile="mpfr.tar.bz2"
if [ ! -e "OK.$InstallFile" ]; then #{
  getNewestFile "http://www.mpfr.org/mpfr-current/" "mpfr-.+\.tar\.bz2\$" "$InstallFile"

  if [ -e "$InstallFile" ]; then #{
    tar xjf "$InstallFile"

    Dir=`find ./ -maxdepth 1 -type d -name "mpfr-*"`
    if [ "$Dir" != "" ]; then
      echo "compiling $Dir ..."
      createLink $Dir mpfr
      cd $Dir
      get http://www.mpfr.org/mpfr-current/ allpatches
      if [ -e allpatches ]; then
        patch -N -Z -p1 < allpatches
      fi
      Configure="./configure --prefix=$TOOLPATH --with-gmp-build=$Dir_GMP"
      Compile="make -j$AmountCPUs"
      
      make clean
      echo $Configure && $Configure >log.configure 2>&1 && \
      echo $Compile && $Compile >log.compile 2>&1 && \
      make -j$AmountCPUs check >log.compile 2>&1 && \
      make install >log.install 2>&1 &&
      touch "../OK.$InstallFile"
      
      cd ..
    fi
    
    if [ -e "OK.$InstallFile" ]; then
      echo "successfully compiled $Dir"
    else
      echo "$0 - ERROR: failed to install '$InstallFile' (Check log-files inside `pwd`/$Dir/)!"
      exit 11
    fi
  else
    echo "$0 - ERROR: missing file '$InstallFile'!"
    exit 10
  fi #}
fi #}
Dir_MPFR="`pwd`/mpfr/"


InstallFile="mpc.tar.gz"
if [ ! -e "OK.$InstallFile" ]; then #{
  getNewestFile "http://www.multiprecision.org/index.php?prog=mpc&page=download" "Version" "$InstallFile"

  if [ -e "$InstallFile" ]; then #{
    tar xzf "$InstallFile"
    Dir=`find ./ -maxdepth 1 -type d -name "mpc-*"`
    if [ "$Dir" != "" ]; then
      echo "compiling $Dir ..."
      createLink $Dir mpc
      cd $Dir

      export CFLAGS='-Wall -Wmissing-prototypes -Wpointer-arith -Wall -g  -Os'
      Configure="./configure --prefix=$TOOLPATH --with-gmp=$Dir_GMP"
      Compile="make -j$AmountCPUs"
      
      echo $Configure && $Configure >log.configure 2>&1
      
      # compilation of mpc-1.0.1 with gcc-4.6.3 failed with option -O3, but succeeded with option -Os
      replaceInFile "libtool" "-O3" "-Os"
      replaceInFile "Makefile" "-O3" "-Os"

      echo $Compile && $Compile >log.compile 2>&1 && \
      make -j$AmountCPUs check >log.compile 2>&1 && \
      make install >log.install 2>&1 &&
      touch "../OK.$InstallFile"

      cd ..
    fi
    if [ -e OK.$InstallFile ]; then
      echo "successfully compiled $Dir"
    else
      echo "$0 - ERROR: failed to install '$InstallFile'!"
      exit 11
    fi
  else
    echo "$0 - ERROR: missing file '$InstallFile'!"
    exit 10
  fi #}
fi #}
Dir_MPC="`pwd`/mpc/"

InstallFile="ppl.tar.gz"
if [ ! -e "OK.$InstallFile" ]; then #{
  getNewestFile "http://bugseng.com/products/ppl/download/ftp/releases/LATEST/" "ppl-.+\.tar\.gz\$" "$InstallFile"

  if [ -e "$InstallFile" ]; then #{
    tar xzf "$InstallFile"
    Dir=`find ./ -maxdepth 1 -type d -name "ppl-*"`
    if [ "$Dir" != "" ]; then
      echo "compiling $NewestFile ..."
      createLink $Dir ppl
      cd $Dir
      mkdir build
      cd build
      Configure="../configure --prefix=$TOOLPATH"
      Compile="make -j$AmountCPUs"
      echo $Configure && $Configure  >../log.configure 2>&1 && \
      echo $Compile   && $Compile    >../log.compile   2>&1 && \
      make install                   >../log.install   2>&1 && \
      touch "../../OK.$InstallFile"
      
      cd ..
      cd ..
    fi
    if [ -e OK.$InstallFile ]; then
      echo "successfully compiled $Dir"
    else
      echo "$0 - ERROR occured during compiling $InstallFile (check log-files in `pwd`/$Dir/)!"
      exit 10
    fi
  else
    echo "$0 - ERROR: missing file '$InstallFile'!"
    exit 10
  fi #}
fi #}
PPL_Dir="`pwd`/ppl/"

InstallFile="cloog.tar.gz"
if [ ! -e "OK.$InstallFile" ]; then #{ 
  getNewestFile "http://www.bastoul.net/cloog/pages/download/" "cloog-0.+\.tar\.gz" "$InstallFile"
  if [ -e "$InstallFile" ]; then #{
    tar xzf "$InstallFile"
    Dir=`find ./ -maxdepth 1 -type d -name "cloog-*"`
    if [ "$Dir" != "" ]; then
      echo "compiling $NewestFile ..."
      createLink $Dir cloog
      cd $Dir

      dir m4
      if [ ! -e configure.in_orig ]; then #{
        mv configure.in configure.in_orig
        echo "AC_CONFIG_MACRO_DIR(m4)" >configure.in
        cat configure.in_orig >>configure.in
      fi #}

      AutoGen="./autogen.sh"
      Configure="./configure --prefix=$TOOLPATH --with-ppl==$PPL_Dir"
      Compile="make -j$AmountCPUs"
      
      echo $AutoGen   && $AutoGen    >log.autogen   2>&1 && \
      echo $Configure && $Configure  >log.configure 2>&1 && \
      echo $Compile   && $Compile    >log.compile   2>&1 && \
      make install                   >log.install   2>&1 && \
      touch "../OK.$InstallFile"
      
      cd ..
    fi
    if [ -e OK.$InstallFile ]; then
      echo "successfully compiled $Dir"
    else
      echo "$0 - ERROR occured during compiling $InstallFile (check log-files in `pwd`/$Dir/)!"
      exit 10
    fi
  else
    echo "$0 - ERROR: missing file '$InstallFile'!"
    exit 10
  fi #}
fi #}
CLOOG_Dir="`pwd`/cloog/"

if [ "1" == "0" ]; then #{ DEPRECATED (old version)
  InstallFile="cloog-ppl"
  if [ ! -e "OK.$InstallFile" ]; then #{ 
    gitClone git://repo.or.cz/cloog-ppl.git cloog-ppl.git
  
    Dir="cloog-ppl"
    if [ -d "$Dir" ]; then
      echo "compiling $Dir ..."
      cd $Dir
      
      dir m4
      if [ ! -e configure.in_orig ]; then #{
        mv configure.in configure.in_orig
        echo "AC_CONFIG_MACRO_DIR(m4)" >configure.in
        cat configure.in_orig >>configure.in
      fi #}
      
      AutoGen="./autogen.sh"
      Configure="./configure --prefix=$TOOLPATH --with-ppl==$PPL_Dir"
      Compile="make" # 
      
      echo $AutoGen   && $AutoGen    >log.autogen   2>&1 && \
      echo $Configure && $Configure  >log.configure 2>&1 && \
      echo $Compile   && $Compile    >log.compile   2>&1 && \
      make install                   >log.install   2>&1 && \
      touch "../OK.$InstallFile"
  
      cd ..
    fi
    
    if [ -e OK.$InstallFile ]; then
      echo "successfully compiled $Dir"
    else
      echo "$0 - ERROR occured during compiling $InstallFile (check log-files in `pwd`/$Dir/)!"
      exit 10
    fi
  fi #}
fi #}


# ToDo: Compile toolchain!
find -name "*.tar.*" | xargs -I% tar -xf %
cd zlib-1.2.5
patch -p1 <../zlib-1.2.5.patch
cd ../../
echo "build-prerequisites.sh"; ./build-prerequisites.sh >log.prerequisites 2>&1
echo "build-toolchain.sh";     ./build-toolchain.sh     >log.toolchain     2>&1

cd ..

InstallFile="gcc.tar.gz"
touch "OK.$InstallFile" # building gcc from vanilla gcc is broken (unsupported target arm-*-elf)
if [ ! -e "OK.$InstallFile" ]; then #{
  rm -Rf "gcc-*"
  findNewest "ftp://ftp.gwdg.de/pub/misc/gcc/releases/" "gcc-.*"
  if [ "$Error_findNewest" ]; then #{ ERROR
    echo "$0 - ERROR: $Error_findNewest"
    exit 11
  fi #}
  getFile "ftp://ftp.gwdg.de/pub/misc/gcc/releases/${NewestDir}/" "${NewestDir}.tar.bz2" "$InstallFile"
  if [ -e "$InstallFile" ]; then #{
    tar xjf "$InstallFile"
    Dir=`find ./ -maxdepth 1 -type d -name "gcc-*"`
    if [ "$Dir" != "" ]; then
      echo "compiling $NewestFile ..."
      createLink $Dir gcc
      cd $Dir
      replaceInFile "gcc/config/arm/elf.h" \
                    "#define SUBTARGET_ASM_FLOAT_SPEC" \
                    "#define SUBTARGET_ASM_FLOAT_SPEC %{mhard-float:-mfpu=fpa} %{!mhard-float:-mfpu=softfpa} "

      File="gcc/config/arm/t-arm-elf"

      replaceInFile "$File" \
                    "#MULTILIB_OPTIONS      += march=armv7" \
                    "MULTILIB_OPTIONS      += march=armv7"
      replaceInFile "$File" \
                    "#MULTILIB_DIRNAMES     += thumb2" \
                    "MULTILIB_DIRNAMES     += thumb2"
      replaceInFile "$File" \
                    "#MULTILIB_EXCEPTIONS   += march=armv7\* marm/\*march=armv7\*" \
                    "MULTILIB_EXCEPTIONS   += march=armv7\* marm/\*march=armv7\*"
      replaceInFile "$File" \
                    "#MULTILIB_MATCHES      += march?armv7=march?armv7-a" \
                    "MULTILIB_MATCHES      += march?armv7=march?armv7-a"
      replaceInFile "$File" \
                    "#MULTILIB_MATCHES      += march?armv7=march?armv7-r" \
                    "MULTILIB_MATCHES      += march?armv7=march?armv7-r"
      replaceInFile "$File" \
                    "#MULTILIB_MATCHES      += march?armv7=march?armv7-m" \
                    "MULTILIB_MATCHES      += march?armv7=march?armv7-m"
      replaceInFile "$File" \
                    "#MULTILIB_MATCHES      += march?armv7=mcpu?cortex-a8" \
                    "MULTILIB_MATCHES      += march?armv7=mcpu?cortex-a8"
      replaceInFile "$File" \
                    "#MULTILIB_MATCHES      += march?armv7=mcpu?cortex-r4" \
                    "MULTILIB_MATCHES      += march?armv7=mcpu?cortex-r4"
      replaceInFile "$File" \
                    "#MULTILIB_MATCHES      += march?armv7=mcpu?cortex-m3" \
                    "MULTILIB_MATCHES      += march?armv7=mcpu?cortex-m3"

      File="gcc/config.gcc"                    
      replaceInFile "$File" \
                    "\| arm\*-\*-elf" \
                    "\| arm\*-\*-elf_DISABLED"

      dir build
      cd build
      createLink ../../bin  mpfr
      createLink ../../bin  mpc
      createLink ../../bin  gmp
      createLink ../../bin  ppl
      createLink ../../bin  cloog

      Configure="../configure --target=$ARMTARGET --prefix=$TOOLPATH --enable-interwork --enable-multilib --enable-languages='c,c++'"
      Configure="$Configure --with-newlib --without-headers --disable-shared --with-gnu-as --with-gnu-ld --with-float=soft"
      Configure="$Configure --with-gmp=gmp --with-mpfr=mpfr --with-mpc=mpc --with-ppl=ppl --with-cloog=cloog"
      Compile="make -j$AmountCPUs all-gcc"
      echo "`pwd`> compiling $InstallFile..."
      #../configure --enable-obsolete --target=$ARMTARGET --prefix=$TOOLPATH --enable-interwork --enable-multilib --enable-languages="c,c++" --with-newlib --without-headers --disable-shared --with-gnu-as --with-gnu-ld --with-float=soft  --with-gmp=gmp --with-mpfr=mpfr --with-mpc=mpc >../log.configure 2>&1 && \
      echo $Configure && $Configure  >../log.configure 2>&1 && \
      echo $Compile   && $Compile    >../log.compile   2>&1 && \
      make -j$AmountCPUs            >>../log.compile   2>&1 && \
      make install >../log.install 2>&1 && \
      touch "../OK.$InstallFile"
      cd ..
      
      cd ..
    fi
    if [ -e OK.$InstallFile ]; then
      echo "successfully compiled $Dir"
    else
      echo "$0 - ERROR occured during compiling $InstallFile (check log-files in `pwd`/$Dir/)!"
      exit 10
    fi
  else
    echo "$0 - ERROR: missing file '$InstallFile'!"
    exit 10
  fi #}
fi #}

echo "DEBUG-EXIT"; exit 0 #D


InstallFile="newlib.tar.gz"
if [ ! -e "OK.$InstallFile" ]; then #{
# taken from: http://michaldemin.wordpress.com/2010/02/09/arm-toolchain-newlib-part1/
#
# make CFLAGS_FOR_TARGET="-msoft-float -DMALLOC_PROVIDED" CCASFLAGS="-msoft-float -DMALLOC_PROVIDED"

  getNewestFile "ftp://sources.redhat.com/pub/newlib/" "newlib-.+\.tar\.gz\$" "$InstallFile"

  if [ -e "$InstallFile" ]; then #{
    tar xjf "$InstallFile"
    Dir=`find ./ -maxdepth 1 -type d -name "gcc-*"`
    if [ "$Dir" != "" ]; then
      echo "compiling $NewestFile ..."
      createLink $Dir gcc
      cd $Dir

      cd ..
    fi
    
    if [ -e OK.$InstallFile ]; then
      echo "successfully compiled $Dir"
    else
      echo "$0 - ERROR occured during compiling $InstallFile (check log-files in `pwd`/$Dir/)!"
      exit 10
    fi
  else
    echo "$0 - ERROR: missing file '$InstallFile'!"
    exit 10
  fi #}
fi #}

echo "DEBUG-EXIT"; exit 0 #D

#X getFile "http://ftp.gnu.org/gnu/binutils/"                   "binutils-2.20.1.tar.bz2"
#X getFile "ftp://ftp.gwdg.de/pub/misc/gcc/releases/gcc-4.4.3/" "gcc-4.4.3.tar.bz2"
#X getFile "ftp://sources.redhat.com/pub/newlib/"               "newlib-1.18.0.tar.gz"

fi #}DEPRECATED

Section="GCC-Native_Prerequisites"
Dir_GCC_Native_Prerequisites="`pwd`/${Section}/"
if [ ! -e OK.${Section} ]; then
  SectionDir="`pwd`"
  echo ""
  echo "---------------------------------------------------------"
  echo "$0 - Installing section ${Section} {"
  echo "---------------------------------------------------------"
  echo ""
  
  export TOOLPATH="$Dir_GCC_Native_Prerequisites/"
  dir $TOOLPATH
  dir ${Section}.temp
  cd  ${Section}.temp

  sudo ln -sfv /bin/bash /bin/sh
  
  InstallFile="binutils.tar.bz2"
  if [ ! -e "OK.$InstallFile" ]; then #{
    rm -Rf binutils-*
    getNewestFile http://ftp.gnu.org/gnu/binutils/ "binutils-..*bz2\$" "$InstallFile"
    if [ -e "$InstallFile" ]; then #{
      tar xjf "$InstallFile"
      Dir=`find ./ -maxdepth 1 -type d -name "binutils-*"`
      if [ "$Dir" != "" ]; then
        echo "compiling $NewestFile ..."
        createLink $Dir binutils
        cd $Dir
        sed -i -e 's/@colophon/@@colophon/' \
               -e 's/doc@cygnus.com/doc@@cygnus.com/' bfd/doc/bfd.texinfo
        dir build
        cd build
        make distclean >../log.clean 2>&1
        OK=""
        #CONFIGURE="../configure --target=$ARMTARGET --prefix=$TOOLPATH --enable-interwork --enable-multilib --with-gnu-as --with-gnu-ld --disable-nls"
        CONFIGURE="../configure --prefix=$TOOLPATH"
        COMPILE="make -j$AmountCPUs all"
        #COMPILE="make all"
        echo "`pwd`> $CONFIGURE"
        $CONFIGURE >../log.configure 2>&1 && \
        echo "`pwd`> $COMPILE" && \
        $COMPILE >../log.compile 2>&1 && \
        make install >../log.install 2>&1 && OK="1"
        cd ..
        if [ "$OK" == "" ]; then
          echo "$0 - Error occured during compiling binutils."
          echo "Check log-files in `pwd` for details!"
          exit 11
        fi
        cd ..
      fi
      if [ -e bin/bin/${ARMTARGET}-objdump ]; then
        rm -Rf $Dir binutils
        touch "OK.$InstallFile"
        echo "Successfully compiled $NewestFile"
      fi
    else
      echo "$0 - ERROR: missing file '$InstallFile'!"
      exit 10
    fi #}
  fi #}
  
  InstallFile="gmp.tar.bz2"
  if [ ! -e "OK.$InstallFile" ]; then #{
    findNewest "ftp://ftp.gmplib.org/pub/" "gmp-.+" "$InstallFile"
    if [ "$Error_findNewest" != "" ]; then #{
      echo "$0 - ERROR: $Error_findNewest"
      exit 11
    fi #}
    getFile "ftp://ftp.gmplib.org/pub/${NewestDir}/" "${NewestDir}.tar.bz2" "$InstallFile"
  
    if [ -e "$InstallFile" ]; then #{
      tar xjf "$InstallFile"
      Dir=`find ./ -maxdepth 1 -type d -name "gmp-*"`
      if [ "$Dir" != "" ]; then
        echo "compiling $NewestDir ..."
        createLink $Dir gmp
        cd $Dir
        ./configure --enable-cxx >log.configure 2>&1 && \
        make -j$AmountCPUs >log.compile 2>&1 && \
        make -j$AmountCPUs check >log.install 2>&1 && \
        touch ../OK.$InstallFile
        cd ..
      fi
      if [ -e OK.$InstallFile ]; then
        echo "compiled successfully: $NewestDir"
      else
        echo "$0 - ERROR: failed to install '$InstallFile'!"
        exit 11
      fi
    else
      echo "$0 - ERROR: missing file '$InstallFile'!"
      exit 10
    fi #}
  fi #}
  Dir_GMP="`pwd`/gmp/"
  
  InstallFile="mpfr.tar.bz2"
  if [ ! -e "OK.$InstallFile" ]; then #{
    getNewestFile "http://www.mpfr.org/mpfr-current/" "mpfr-.+\.tar\.bz2\$" "$InstallFile"
  
    if [ -e "$InstallFile" ]; then #{
      tar xjf "$InstallFile"
  
      Dir=`find ./ -maxdepth 1 -type d -name "mpfr-*"`
      if [ "$Dir" != "" ]; then
        echo "compiling $Dir ..."
        createLink $Dir mpfr
        cd $Dir
        get http://www.mpfr.org/mpfr-current/ allpatches
        if [ -e allpatches ]; then
          patch -N -Z -p1 < allpatches
        fi
        Configure="./configure --prefix=$TOOLPATH --with-gmp-build=$Dir_GMP"
        Compile="make -j$AmountCPUs"
        
        make clean
        echo $Configure && $Configure >log.configure 2>&1 && \
        echo $Compile && $Compile >log.compile 2>&1 && \
        make -j$AmountCPUs check >log.compile 2>&1 && \
        make install >log.install 2>&1 &&
        touch "../OK.$InstallFile"
        
        cd ..
      fi
      
      if [ -e "OK.$InstallFile" ]; then
        echo "successfully compiled $Dir"
      else
        echo "$0 - ERROR: failed to install '$InstallFile' (Check log-files inside `pwd`/$Dir/)!"
        exit 11
      fi
    else
      echo "$0 - ERROR: missing file '$InstallFile'!"
      exit 10
    fi #}
  fi #}
  Dir_MPFR="`pwd`/mpfr/"
  
  InstallFile="mpc.tar.gz"
  if [ ! -e "OK.$InstallFile" ]; then #{
    getNewestFile "http://www.multiprecision.org/index.php?prog=mpc&page=download" "Version" "$InstallFile"
  
    if [ -e "$InstallFile" ]; then #{
      tar xzf "$InstallFile"
      Dir=`find ./ -maxdepth 1 -type d -name "mpc-*"`
      if [ "$Dir" != "" ]; then
        echo "compiling $Dir ..."
        createLink $Dir mpc
        cd $Dir
  
        export CFLAGS='-Wall -Wmissing-prototypes -Wpointer-arith -Wall -g  -Os'
        Configure="./configure --prefix=$TOOLPATH --with-gmp=$Dir_GMP"
        Compile="make -j$AmountCPUs"
        
        echo $Configure && $Configure >log.configure 2>&1
        
        # compilation of mpc-1.0.1 with gcc-4.6.3 failed with option -O3, but succeeded with option -Os
        replaceInFile "libtool" "-O3" "-Os"
        replaceInFile "Makefile" "-O3" "-Os"
  
        echo $Compile && $Compile >log.compile 2>&1 && \
        make -j$AmountCPUs check >log.compile 2>&1 && \
        make install >log.install 2>&1 &&
        touch "../OK.$InstallFile"
  
        cd ..
      fi
      if [ -e OK.$InstallFile ]; then
        echo "successfully compiled $Dir"
      else
        echo "$0 - ERROR: failed to install '$InstallFile'!"
        exit 11
      fi
    else
      echo "$0 - ERROR: missing file '$InstallFile'!"
      exit 10
    fi #}
  fi #}
  Dir_MPC="`pwd`/mpc/"
  
  InstallFile="ppl.tar.gz"
  if [ ! -e "OK.$InstallFile" ]; then #{
    getNewestFile "http://bugseng.com/products/ppl/download/ftp/releases/LATEST/" "ppl-.+\.tar\.gz\$" "$InstallFile"
  
    if [ -e "$InstallFile" ]; then #{
      tar xzf "$InstallFile"
      Dir=`find ./ -maxdepth 1 -type d -name "ppl-*"`
      if [ "$Dir" != "" ]; then
        echo "compiling $NewestFile ..."
        createLink $Dir ppl
        cd $Dir
        mkdir build
        cd build
        Configure="../configure --prefix=$TOOLPATH"
        Compile="make -j$AmountCPUs"
        echo $Configure && $Configure  >../log.configure 2>&1 && \
        echo $Compile   && $Compile    >../log.compile   2>&1 && \
        make install                   >../log.install   2>&1 && \
        touch "../../OK.$InstallFile"
        
        cd ..
        cd ..
      fi
      if [ -e OK.$InstallFile ]; then
        echo "successfully compiled $Dir"
      else
        echo "$0 - ERROR occured during compiling $InstallFile (check log-files in `pwd`/$Dir/)!"
        exit 10
      fi
    else
      echo "$0 - ERROR: missing file '$InstallFile'!"
      exit 10
    fi #}
  fi #}
  PPL_Dir="`pwd`/ppl/"
  
  InstallFile="cloog.tar.gz"
  if [ ! -e "OK.$InstallFile" ]; then #{ 
    getNewestFile "http://www.bastoul.net/cloog/pages/download/" "cloog-0.+\.tar\.gz" "$InstallFile"
    if [ -e "$InstallFile" ]; then #{
      tar xzf "$InstallFile"
      Dir=`find ./ -maxdepth 1 -type d -name "cloog-*"`
      if [ "$Dir" != "" ]; then
        echo "compiling $NewestFile ..."
        createLink $Dir cloog
        cd $Dir
  
        dir m4
        if [ ! -e configure.in_orig ]; then #{
          mv configure.in configure.in_orig
          echo "AC_CONFIG_MACRO_DIR(m4)" >configure.in
          cat configure.in_orig >>configure.in
        fi #}
  
        AutoGen="./autogen.sh"
        Configure="./configure --prefix=$TOOLPATH --with-ppl==$PPL_Dir"
        Compile="make -j$AmountCPUs"
        
        echo $AutoGen   && $AutoGen    >log.autogen   2>&1 && \
        echo $Configure && $Configure  >log.configure 2>&1 && \
        echo $Compile   && $Compile    >log.compile   2>&1 && \
        make install                   >log.install   2>&1 && \
        touch "../OK.$InstallFile"
        
        cd ..
      fi
      if [ -e OK.$InstallFile ]; then
        echo "successfully compiled $Dir"
      else
        echo "$0 - ERROR occured during compiling $InstallFile (check log-files in `pwd`/$Dir/)!"
        exit 10
      fi
    else
      echo "$0 - ERROR: missing file '$InstallFile'!"
      exit 10
    fi #}
  fi #}
  CLOOG_Dir="`pwd`/cloog/"

  cd "$SectionDir"
  touch OK.${Section}
  rm -Rf ${Section}.temp
  echo "$0 - Finished section ${Section} }"
  echo "---------------------------------------------------------"
  echo ""
fi
fi #}DEPRECATED
