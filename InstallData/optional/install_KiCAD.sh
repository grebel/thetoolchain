#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Initial Script written by Gregor Rebel 2010-2018.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

SupportScript="`pwd`/scripts/installFuncs.sh"
source "$SupportScript"

if [ "$USER" == "root" ]; then
  echo "$0: ERROR this script should not been run as user root!"
  exit 10
fi

Install_Dir="999_kicad"
dir "$Install_Dir"
cd "$Install_Dir"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK\.Documentation ];    then    #{ download documentation

  Get_Fails=""
  
  # download doc-file + move to Documentation/ + create symbolic link into Documentation/ right here 
  # File="STM32F10x_Standard_Peripherals_Library.html"
  # getFile http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library $File
  # addDocumentationFile $File STM
  File="kicad_3d_vrml.pdf"
  getFile http://xa.yimg.com/kq/groups/16027698/2129937827/name/ $File $File
  addDocumentationFile $File PCB-Design
  File="tutorial-3d-kicad-parts-using-openscad-and-wings3d.html"
  getFile http://happyrobotlabs.com/posts/tutorials/ tutorial-3d-kicad-parts-using-openscad-and-wings3d $File
  addDocumentationFile $File PCB-Design
  
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/application_note/ CD00221665.pdf AN2867_Oscillator_design_guide_for_STM8-32_microcontrollers.pdf PCB-Design
  
  if [ "$Get_Fails" == "" ]; then #{ check get() inside scripts/installFuncs.sh 
    touch OK\.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ download + install software
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe 'which autoreconf' autoreconf
      AptGetAvailable=`which apt-get`
      if [ "$AptGetAvailable" ]; then
        installPackage libgtk2.0-0:i386
        installPackageSafe "which bzr" bzr
        
        installPackageSafe "which locate" findutils
        sudo updatedb
    
        installPackageSafe "" cmake-data
        installPackageSafe "" doxygen-latex
        installPackageSafe "" emacsen-common
        installPackageSafe "" libcairo-script-interpreter2
        installPackageSafe "" libdrm-dev
        installPackageSafe "" libdrm-intel1
        installPackageSafe "" libdrm-nouveau2
        installPackageSafe "" libdrm-radeon1
        installPackageSafe "" libdrm2
        installPackageSafe "" libexpat1-dev
        installPackageSafe "" libfontconfig1-dev
        installPackageSafe "" libfreetype6-dev
        installPackageSafe "" libgl1-mesa-dev
        installPackageSafe "" libglib2.0-0
        installPackageSafe "" libglib2.0-bin
        installPackageSafe "" libglib2.0-dev
        installPackageSafe "" libglu1-mesa-dev
        installPackageSafe "" libice-dev
        installPackageSafe "" libjs-jquery
        installPackageSafe "" libkms1
        installPackageSafe "" libpcre3-dev
        installPackageSafe "" libpcrecpp0
        installPackageSafe "" libpixman-1-0
        installPackageSafe "" libpixman-1-dev
        installPackageSafe "" libpng12-dev
        installPackageSafe "" libpthread-stubs0
        installPackageSafe "" libpthread-stubs0-dev
        installPackageSafe "" libsm-dev
        installPackageSafe "" libssl-doc
        installPackageSafe "" libssl1.0.0
        installPackageSafe "" libwxbase2.8-dev
        installPackageSafe "" libwxgtk-media2.8-0
        installPackageSafe "" libwxgtk-webview3.0-0
        installPackageSafe "" libx11-dev
        installPackageSafe "" libx11-doc
        installPackageSafe "" libx11-xcb-dev
        installPackageSafe "" libxau-dev
        installPackageSafe "" libxcb-dri2-0-dev
        installPackageSafe "" libxcb-glx0-dev
        installPackageSafe "" libxcb-render0-dev
        installPackageSafe "" libxcb-shm0-dev
        installPackageSafe "" libxcb1-dev
        installPackageSafe "" libxdamage-dev
        installPackageSafe "" libxdmcp-dev
        installPackageSafe "" libxext-dev
        installPackageSafe "" libxfixes-dev
        installPackageSafe "" libxrender-dev
        installPackageSafe "" libxxf86vm-dev
        installPackageSafe "" mesa-common-dev
        installPackageSafe "" preview-latex-style
        installPackageSafe "" python-bzrlib
        installPackageSafe "" python-configobj
        installPackageSafe "" python-gpgme
        installPackageSafe "" python-keyring
        installPackageSafe "" python-launchpadlib
        installPackageSafe "" python-lazr.restfulclient
        installPackageSafe "" python-lazr.uri
        installPackageSafe "" python-oauth
        installPackageSafe "" python-paramiko
        installPackageSafe "" python-secretstorage
        installPackageSafe "" python-simplejson
        installPackageSafe "" python-wadllib
        installPackageSafe "" python-wxversion
        installPackageSafe "" texlive-latex-extra
        installPackageSafe "" texlive-latex-extra-doc
        installPackageSafe "" texlive-pictures
        installPackageSafe "" texlive-pictures-doc
        sudo apt-get remove wx2.8*
        installPackageSafe "" wx-common
        installPackageSafe "locate version.h | grep wx-3" wx3.0-headers
        installPackageSafe "" libwxbase3.0-dev
        installPackageSafe "" libwx-3*
        installPackageSafe "" libwxgtk-webview*
        installPackageSafe "" wx-3*
        installPackageSafe "" x11proto-core-dev
        installPackageSafe "" x11proto-damage-dev
        installPackageSafe "" x11proto-dri2-dev
        installPackageSafe "" x11proto-fixes-dev
        installPackageSafe "" x11proto-gl-dev
        installPackageSafe "" x11proto-input-dev
        installPackageSafe "" x11proto-kb-dev
        installPackageSafe "" x11proto-render-dev
        installPackageSafe "" x11proto-xext-dev
        installPackageSafe "" x11proto-xf86vidmode-dev
        installPackageSafe "" xorg-sgml-doctools
        installPackageSafe "" xtrans-dev
        installPackageSafe "" zlib1g-dev
        installPackageSafe "" python-numpy 
        installPackageSafe "" python-pyside
        sudo apt-get -f install
      fi
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir ..."
    Install_Path=`pwd`
    BinFolder="../bin"

    if [ "1" == "1" ]; then #{ try to install from ppa
        #sudo add-apt-repository --yes ppa:adamwolf/kicad-testing-daily # no more available
        sudo add-apt-repository --yes ppa:js-reynaud/ppa-kicad
        sudo DEBIAN_FRONTEND=noninteractive apt-get --install-suggests --force-yes --fix-missing -y update
        sudo DEBIAN_FRONTEND=noninteractive apt-get --install-suggests --force-yes --fix-missing -y install kicad kicad-doc-en && touch OK.Install
        sudo apt-get dist-upgrade -y
    fi #}
    if [ ! -e OK.Install ]; then    #{ Installation from ppa did not work: try to compile from source
      if [ ! -e "$Install_Dir/OK.Compile" ]; then #{ download + install kicad via install script
        ../scripts/findNewestDownload.pl http://bazaar.launchpad.net/~kicad-product-committers/kicad/product/view/head:/scripts/kicad-install.sh download newest.url_prefix newest.filename
        URL_Prefix=`cat newest.url_prefix` 
        FileName=`cat newest.filename`
        getFile $URL_Prefix $FileName kicad-install.sh
        if [ -e kicad-install.sh ]; then #{
          MissingName=`bzr whoami 2>&1 | grep ERROR`
          if [ "$MissingName" != "" ]; then
            echo "setting bazaar user name.."
            Name="${USER}@${HOSTNAME}"
            bzr whoami "$Name <$Name>"
          fi
          dir kicad_sources
          cat <<END_OF_SCRIPT >clean.sh #{
#!/bin/bash

cd "`pwd`"
echo "deleting `pwd`/kicad_sources"
sudo rm -Rf kicad_sources
END_OF_SCRIPT
#}
          chmod +x clean.sh
          if [ ! -d kicad_sources/kicad.bzr/build ]; then #{ no source code downloaded yet: patch + run kicad_install.sh
            if [ -d kicad_sources ]; then
              echo "removing incomplete folder `pwd`/kicad_sources ..." 
              
              rm -Rf "$HOME/kicad_sources"
              mv -v kicad_sources $HOME/
            fi
            replaceInFile kicad-install.sh "apt-get install" "apt-get install -y"
            PatchScript="`pwd`/patch_KiCAD_install.sh" #{
            # -> https://forum.kicad.info/t/solution-error-building-because-of-wxwidgets-3-0-0/287
            cat <<END_OF_SCRIPT >$PatchScript
#!/bin/bash

echo "patching install files..."

source "$SupportScript"
VersionPath="`locate /version.h | grep wx-3`"
FilePath="`find \$HOME/kicad_sources/ -name FindwxWidgets.cmake`"
#replaceInFile \$FilePath "wx/version.h PATHS \\\${wxWidgets_INCLUDE_DIRS} NO_DEFAULT_PATH" "\"\$VersionPath\""
replaceInFile \$FilePath "find_file(_filename wx/version.h" "set(_filename \"\$VersionPath\") # find_file(_filename wx/version.h"
echo "FilePath=\$FilePath"
echo "found wx-widgets: \$VersionPath"

END_OF_SCRIPT
#}
            chmod +x "$PatchScript"
            replaceInFile kicad-install.sh 'compiling source code..."' "compiling source code...\"; source \"$PatchScript\""
            chmod +x kicad-install.sh
            echo "running kicad-install.sh.."
            cat <<END_OF_TEXT >cr.txt #{



































END_OF_TEXT
  #}
            
#X          sudo ./kicad-install.sh --remove-sources
            sudo ./kicad-install.sh --install-or-update
            if [ -d "$HOME/kicad_sources" ]; then
              sudo chown -R $USER: "$HOME/kicad_sources"
              sudo rm -Rf kicad_sources
              mv "$HOME/kicad_sources" .
            fi
            rm -f "$HOME/kicad_sources"
            createLink  `pwd`/kicad_sources "$HOME/kicad_sources"
          fi #}
          if [ ! -x /usr/local/bin/kicad ]; then #{
            echo ""
            cd "$HOME/kicad_sources"
            for Part in kicad.bzr kicad-lib.bzr kicad-doc.bzr; do
              Flag="$Install_Dir/OK.Compile_$Part"
              if [ ! -e "$Flag" ]; then
                Dir $Part/build/
                echo "trying to compile $Part ..."
                cd $Part/build/
                cmake ../ && make && sudo make install && echo "" >"$Flag"
              else
                echo "Skipping compilation because of: $Flag"
              fi
            done
          fi #}
        fi #}
        #http://bazaar.launchpad.net/~kicad-product-committers/kicad/product/download/head:/kicadinstall.sh-20131017152909-uyumxtw74s418lqr-1/kicad-install.sh
      else
        echo "$0 - Compilation stage already finished (file exists: $Install_Dir/OK.Compile)"
      fi #}
    fi #}
    if [ "`which kicad`" == "" ]; then #{
      echo "$0 ERROR: Could NOT install KiCAD!"
      rm -fv "$Install_Dir/OK.Compile"
      exit 10
    fi #}
    
    #{ meshconv allows to repair STL-files before loading them into Wings3D
    getFile http://www.cs.princeton.edu/~min/meshconv/linux32/ meshconv meshconv_x86
    chmod +x meshconv_x86
    getFile http://www.cs.princeton.edu/~min/meshconv/linux64/ meshconv meshconv_64
    chmod +x meshconv_64
    Is64Bit=`uname -a | grep x86_64`
    if [ "$Is64Bit" != "" ]; then
      createLink `pwd`/meshconv_64 $BinFolder/meshconv
    else
      createLink `pwd`/meshconv_x86 $BinFolder/meshconv
    fi
    if [ -x $BinFolder/meshconv ]; then
      cat <<"END_OF_SCRIPT" >$BinFolder/meshconv_stl2stl.sh #{
#!/bin/bash

if [ "$1" == "" ]; then
  echo "$0 FILE.stl"
  echo "Tries to repair given stl-file for loading it into Wings3D."
  exit 0
fi

meshconv -c stl ${1} -o ${1}_fixed


END_OF_SCRIPT
      chmod +x $BinFolder/meshconv_wrl2wrl.sh
      #}
      cat <<"END_OF_SCRIPT" >$BinFolder/meshconv_wrl2wrl.sh #{
#!/bin/bash

if [ "$1" == "" ]; then
  echo "$0 FILE.stl"
  echo "Tries to repair given wrl-file for using it as 3D-object in KiCAD."
  exit 0
fi

meshconv -c wrl ${1} -o ${1}_fixed


END_OF_SCRIPT
      chmod +x $BinFolder/meshconv_wrl2wrl.sh
#}
    fi
    #}
    
    if [ "1" == "1" ]; then #{ clone kicad-libs repository with additional schematics + footprints
      cd $HOME/Source
      gitClone git://hlb-labor.de/kicad-libs kicad-libs
      FileFP="$HOME/.config/kicad/fp-lib-table"
      if [ ! -e "$FileFP" ]; then #{ create missing file
        cat <<END_OF_FILE >$FileFP
(fp_lib_table
  (lib (name Air_Coils_SML_NEOSID)(type Github)(uri \${KIGITHUB}/Air_Coils_SML_NEOSID.pretty)(options "")(descr HAMxx31A_HDMxx31A))
  (lib (name Buzzers_Beepers)(type Github)(uri \${KIGITHUB}/Buzzers_Beepers.pretty)(options "")(descr "The way you like them."))
  (lib (name Capacitors_Elko_ThroughHole)(type Github)(uri \${KIGITHUB}/Capacitors_Elko_ThroughHole.pretty)(options "")(descr "The way you like them."))
  (lib (name Capacitors_SMD)(type Github)(uri \${KIGITHUB}/Capacitors_SMD.pretty)(options "")(descr "The way you like them."))
  (lib (name Capacitors_Tantalum_SMD)(type Github)(uri \${KIGITHUB}/Capacitors_Tantalum_SMD.pretty)(options "")(descr "The way you like them."))
  (lib (name Capacitors_ThroughHole)(type Github)(uri \${KIGITHUB}/Capacitors_ThroughHole.pretty)(options "")(descr "The way you like them."))
  (lib (name Choke_Axial_ThroughHole)(type Github)(uri \${KIGITHUB}/Choke_Axial_ThroughHole.pretty)(options "")(descr "The way you like them."))
  (lib (name Choke_Common-Mode_Wurth)(type Github)(uri \${KIGITHUB}/Choke_Common-Mode_Wurth.pretty)(options "")(descr "The way you like them."))
  (lib (name Choke_Radial_ThroughHole)(type Github)(uri \${KIGITHUB}/Choke_Radial_ThroughHole.pretty)(options "")(descr "The way you like them."))
  (lib (name Choke_SMD)(type Github)(uri \${KIGITHUB}/Choke_SMD.pretty)(options "")(descr "The way you like them."))
  (lib (name Choke_Toroid_ThroughHole)(type Github)(uri \${KIGITHUB}/Choke_Toroid_ThroughHole.pretty)(options "")(descr "The way you like them."))
  (lib (name Connectors_Molex)(type Github)(uri \${KIGITHUB}/Connectors_Molex.pretty)(options "")(descr 53047-A123))
  (lib (name Connect)(type Github)(uri \${KIGITHUB}/Connect.pretty)(options "")(descr "The way you like them."))
  (lib (name Converters_DCDC_ACDC)(type Github)(uri \${KIGITHUB}/Converters_DCDC_ACDC.pretty)(options "")(descr "The way you like them."))
  (lib (name Crystals_Oscillators_SMD)(type Github)(uri \${KIGITHUB}/Crystals_Oscillators_SMD.pretty)(options "")(descr "The way you like them."))
  (lib (name Crystals)(type Github)(uri \${KIGITHUB}/Crystals.pretty)(options "")(descr "The way you like them."))
  (lib (name Diodes_SMD)(type Github)(uri \${KIGITHUB}/Diodes_SMD.pretty)(options "")(descr "The way you like them."))
  (lib (name Diodes_ThroughHole)(type Github)(uri \${KIGITHUB}/Diodes_ThroughHole.pretty)(options "")(descr "The way you like them."))
  (lib (name Discret)(type Github)(uri \${KIGITHUB}/Discret.pretty)(options "")(descr "The way you like them."))
  (lib (name Displays_7-Segment)(type Github)(uri \${KIGITHUB}/Displays_7-Segment.pretty)(options "")(descr "The way you like them."))
  (lib (name Display)(type Github)(uri \${KIGITHUB}/Display.pretty)(options "")(descr "The way you like them."))
  (lib (name Divers)(type Github)(uri \${KIGITHUB}/Divers.pretty)(options "")(descr "The way you like them."))
  (lib (name EuroBoard_Outline)(type Github)(uri \${KIGITHUB}/EuroBoard_Outline.pretty)(options "")(descr "The way you like them."))
  (lib (name Fiducials)(type Github)(uri \${KIGITHUB}/Fiducials.pretty)(options "")(descr "The way you like them."))
  (lib (name Filters_HF_Coils_NEOSID)(type Github)(uri \${KIGITHUB}/Filters_HF_Coils_NEOSID.pretty)(options "")(descr "The way you like them."))
  (lib (name Fuse_Holders_and_Fuses)(type Github)(uri \${KIGITHUB}/Fuse_Holders_and_Fuses.pretty)(options "")(descr "The way you like them."))
  (lib (name Hall-Effect_Transducers_LEM)(type Github)(uri \${KIGITHUB}/Hall-Effect_Transducers_LEM.pretty)(options "")(descr "The way you like them."))
  (lib (name Heatsinks)(type Github)(uri \${KIGITHUB}/Heatsinks.pretty)(options "")(descr "The way you like them."))
  (lib (name Housings_QFP)(type Github)(uri \${KIGITHUB}/Housings_QFP.pretty)(options "")(descr "The way you like them."))
  (lib (name Housings_ROHM)(type Github)(uri \${KIGITHUB}/Housings_ROHM.pretty)(options "")(descr VML0806))
  (lib (name Housings_SIP)(type Github)(uri \${KIGITHUB}/Housings_SIP.pretty)(options "")(descr "The way you like them."))
  (lib (name Housings_SOIC)(type Github)(uri \${KIGITHUB}/Housings_SOIC.pretty)(options "")(descr "The way you like them."))
  (lib (name Housings_SOT-23_SOT-143_TSOT-6)(type Github)(uri \${KIGITHUB}/Housings_SOT-23_SOT-143_TSOT-6.pretty)(options "")(descr "The way you like them."))
  (lib (name Housings_SOT-89)(type Github)(uri \${KIGITHUB}/Housings_SOT-89.pretty)(options "")(descr "The way you like them."))
  (lib (name Housings_SOT)(type Github)(uri \${KIGITHUB}/Housings_SOT.pretty)(options "")(descr "SOT126, SOT32"))
  (lib (name Housings_SSOP)(type Github)(uri \${KIGITHUB}/Housings_SSOP.pretty)(options "")(descr "The way you like them."))
  (lib (name Housings_TO-50)(type Github)(uri \${KIGITHUB}/Housings_TO-50.pretty)(options "")(descr "The way you like them."))
  (lib (name Housings_TO-78)(type Github)(uri \${KIGITHUB}/Housings_TO-78.pretty)(options "")(descr "The way you like them."))
  (lib (name Housings_TO-92)(type Github)(uri \${KIGITHUB}/Housings_TO-92.pretty)(options "")(descr "The way you like them."))
  (lib (name Inductors_NEOSID)(type Github)(uri \${KIGITHUB}/Inductors_NEOSID.pretty)(options "")(descr "The way you like them."))
  (lib (name Inductors)(type Github)(uri \${KIGITHUB}/Inductors.pretty)(options "")(descr "The way you like them."))
  (lib (name IR-DirectFETs)(type Github)(uri \${KIGITHUB}/IR-DirectFETs.pretty)(options "")(descr "The way you like them."))
  (lib (name Labels)(type Github)(uri \${KIGITHUB}/Labels.pretty)(options "")(descr "The way you like them."))
  (lib (name LEDs)(type Github)(uri \${KIGITHUB}/LEDs.pretty)(options "")(descr "The way you like them."))
  (lib (name Measurement_Points)(type Github)(uri \${KIGITHUB}/Measurement_Points.pretty)(options "")(descr "The way you like them."))
  (lib (name Measurement_Scales)(type Github)(uri \${KIGITHUB}/Measurement_Scales.pretty)(options "")(descr "The way you like them."))
  (lib (name Mechanical_Sockets)(type Github)(uri \${KIGITHUB}/Mechanical_Sockets.pretty)(options "")(descr DIN41612))
  (lib (name Mounting_Holes)(type Github)(uri \${KIGITHUB}/Mounting_Holes.pretty)(options "")(descr "The way you like them."))
  (lib (name Muonde)(type Github)(uri \${KIGITHUB}/Muonde.pretty)(options "")(descr "The way you like them."))
  (lib (name NF-Transformers_ETAL)(type Github)(uri \${KIGITHUB}/NF-Transformers_ETAL.pretty)(options "")(descr "The way you like them."))
  (lib (name Oddities)(type Github)(uri \${KIGITHUB}/Oddities.pretty)(options "")(descr "The way you like them."))
  (lib (name Opto-Devices)(type Github)(uri \${KIGITHUB}/Opto-Devices.pretty)(options "")(descr "The way you like them."))
  (lib (name Oscillator-Modules)(type Github)(uri \${KIGITHUB}/Oscillator-Modules.pretty)(options "")(descr "The way you like them."))
  (lib (name Oscillators)(type Github)(uri \${KIGITHUB}/Oscillators.pretty)(options "")(descr "SI570, SI571"))
  (lib (name Pentawatts)(type Github)(uri \${KIGITHUB}/Pentawatts.pretty)(options "")(descr "The way you like them."))
  (lib (name PFF_PSF_PSS_Leadforms)(type Github)(uri \${KIGITHUB}/PFF_PSF_PSS_Leadforms.pretty)(options "")(descr Allegro_ACS754_ACS755_ACS756_HallCurrentSensor))
  (lib (name Pin_Headers)(type Github)(uri \${KIGITHUB}/Pin_Headers.pretty)(options "")(descr "2.54mm pin headers."))
  (lib (name Potentiometers)(type Github)(uri \${KIGITHUB}/Potentiometers.pretty)(options "")(descr "The way you like them."))
  (lib (name Power_Integrations)(type Github)(uri \${KIGITHUB}/Power_Integrations.pretty)(options "")(descr "The way you like them."))
  (lib (name Printtrafo_CHK)(type Github)(uri \${KIGITHUB}/Printtrafo_CHK.pretty)(options "")(descr "The way you like them."))
  (lib (name Relays_ThroughHole)(type Github)(uri \${KIGITHUB}/Relays_ThroughHole.pretty)(options "")(descr "The way you like them."))
  (lib (name Resistors_SMD)(type Github)(uri \${KIGITHUB}/Resistors_SMD.pretty)(options "")(descr "The way you like them."))
  (lib (name Resistors_ThroughHole)(type Github)(uri \${KIGITHUB}/Resistors_ThroughHole.pretty)(options "")(descr "The way you like them."))
  (lib (name Resistors_Universal)(type Github)(uri \${KIGITHUB}/Resistors_Universal.pretty)(options "")(descr Experimental))
  (lib (name SMD_Packages)(type Github)(uri \${KIGITHUB}/SMD_Packages.pretty)(options "")(descr "The way you like them."))
  (lib (name Sockets_BNC)(type Github)(uri \${KIGITHUB}/Sockets_BNC.pretty)(options "")(descr "The way you like them."))
  (lib (name Sockets_DIP)(type Github)(uri \${KIGITHUB}/Sockets_DIP.pretty)(options "")(descr "The way you like them."))
  (lib (name Sockets_Mini-Universal)(type Github)(uri \${KIGITHUB}/Sockets_Mini-Universal.pretty)(options "")(descr Mate-N-Lok))
  (lib (name Sockets_MOLEX_KK-System)(type Github)(uri \${KIGITHUB}/Sockets_MOLEX_KK-System.pretty)(options "")(descr "The way you like them."))
  (lib (name Sockets_PGA)(type Github)(uri \${KIGITHUB}/Sockets_PGA.pretty)(options "")(descr "The way you like them."))
  (lib (name Socket_Strips)(type Github)(uri \${KIGITHUB}/Socket_Strips.pretty)(options "")(descr "2.54mm socket strips."))
  (lib (name Sockets)(type Github)(uri \${KIGITHUB}/Sockets.pretty)(options "")(descr "The way you like them."))
  (lib (name Sockets_WAGO734)(type Github)(uri \${KIGITHUB}/Sockets_WAGO734.pretty)(options "")(descr "The way you like them."))
  (lib (name Symbols)(type Github)(uri \${KIGITHUB}/Symbols.pretty)(options "")(descr "The way you like them."))
  (lib (name Terminal_Blocks)(type Github)(uri \${KIGITHUB}/Terminal_Blocks.pretty)(options "")(descr WAGO236-RM5mm))
  (lib (name Transformers_SMPS_ThroughHole)(type Github)(uri \${KIGITHUB}/Transformers_SMPS_ThroughHole.pretty)(options "")(descr "The way you like them."))
  (lib (name Transistors_OldSowjetAera)(type Github)(uri \${KIGITHUB}/Transistors_OldSowjetAera.pretty)(options "")(descr "The way you like them."))
  (lib (name Transistors_SMD)(type Github)(uri \${KIGITHUB}/Transistors_SMD.pretty)(options "")(descr "The way you like them."))
  (lib (name Transistors_TO-220)(type Github)(uri \${KIGITHUB}/Transistors_TO-220.pretty)(options "")(descr "The way you like them."))
  (lib (name Transistors_TO-247)(type Github)(uri \${KIGITHUB}/Transistors_TO-247.pretty)(options "")(descr "The way you like them."))
  (lib (name Valves)(type Github)(uri \${KIGITHUB}/Valves.pretty)(options "")(descr "The way you like them."))
  (lib (name Wire_Connections_Bridges)(type Github)(uri \${KIGITHUB}/Wire_Connections_Bridges.pretty)(options "")(descr "The way you like them."))
  (lib (name Wire_Pads)(type Github)(uri \${KIGITHUB}/Wire_Pads.pretty)(options "")(descr "The way you like them."))
  (lib (name HLB)(type KiCad)(uri \$HOME/Source/kicad-libs/FootPrints)(options "")(descr ""))
)
END_OF_FILE
      fi #}
      if [ "`grep HLB $FileFP`" == "" ]; then
        echo "Adding footprint library kicad-libs to $FileFP"
        
        head -n -1 $FileFP >"${FileFP}_new"
        cat <<END_OF_FILE >>"${FileFP}_new"

  (lib (name HLB)(type KiCad)(uri \$HOME/Source/kicad-libs/FootPrints)(options "")(descr ""))
)
END_OF_FILE
        mv "${FileFP}" "${FileFP}_old"
        mv "${FileFP}_new" "${FileFP}"
      fi
    fi #}
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    dir libs
    cd libs

    getFile http://smisioto.no-ip.org/elettronica/kicad/ kicad-en.htm
    for File in `grep -i '<img src' kicad-en.htm  | awk -F\" '{ print $2 }' | awk -F/ '{ print $5 }'`; do
      getFile http://smisioto.no-ip.org/elettronica/kicad/imgs/ $File
    done
    cp kicad-en.htm index.html
    replaceInFile index.html /elettronica/kicad/imgs/ ""
    replaceInFile index.html "bgColor=#000000" "bgColor=#e5e5e5"
    replaceInFile index.html "BACKGROUND: #000000" "BACKGROUND: #e5e5e5"
    
    for File in lib_w_analog.zip lib_w_connectors.zip lib_w_device.zip lib_w_logic.zip lib_w_microcontrollers.zip lib_w_opto.zip lib_w_rtx.zip; do
      getFile http://smisioto.no-ip.org/elettronica/kicad/libs/eesch/ $File
    done
    
    for File in mod_capacitors.zip mod_conn_9159.zip mod_conn_av.zip mod_conn_d-sub.zip mod_conn_df13.zip mod_conn_jst-ph.zip mod_conn_misc.zip mod_conn_modu.zip mod_conn_panelmate.zip mod_conn_pc.zip mod_conn_screw.zip mod_conn_strip.zip mod_details.zip mod_indicators.zip mod_misc_comp.zip mod_pin_strip.zip mod_pth_circuits.zip mod_pth_diodes.zip mod_pth_plcc.zip mod_pth_resistors.zip mod_relay.zip mod_to.zip mod_smd_bga.zip mod_smd_cap.zip mod_smd_dil.zip mod_smd_diode.zip mod_smd_inductors.zip mod_smd_leds.zip mod_smd_lqfp.zip mod_smd_plcc.zip mod_smd_qfn.zip mod_smd_resistors.zip mod_smd_trans.zip; do 
      getFile http://smisioto.no-ip.org/elettronica/kicad/libs/pcbnew/ $File
    done
    
    for File in 3d_capacitors.zip 3d_conn_9159.zip 3d_conn_av.zip 3d_conn_d-sub 3d_conn_df13.zip 3d_conn_jst-ph 3d_conn_misc.zip 3d_conn_modu.zip 3d_conn_panelmate.zip 3d_conn_pc.zip 3d_conn_screw.zip 3d_conn_strip.zip 3d_details.zip 3d_indicators.zip 3d_misc_comp.zip 3d_pin_strip.zip 3d_pth_circuits.zip 3d_pth_diodes.zip 3d_pth_plcc.zip 3d_pth_resistors.zip 3d_relay.zip 3d_to.zip 3d_smd_bga.zip 3d_smd_cap.zip 3d_smd_dil.zip 3d_smd_diode.zip 3d_smd_inductors.zip 3d_smd_leds.zip 3d_smd_lqfp.zip 3d_smd_plcc.zip 3d_smd_qfn.zip 3d_smd_resistors.zip 3d_smd_trans.zip; do
      getFile http://smisioto.no-ip.org/elettronica/kicad/libs/3d/ $File
    done
    
    echo "" >unzip.log
    for File in *.zip; do
      echo "extracting $File..."
      unzip -o $File >>unzip.log 2>&1
    done
 
    cd ..
    
    cd "$Install_Path"
    echo "" >OK.Install
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts
  Name="${Install_Dir}"

  if [ -d /usr/share/kicad/library/ ]; then
    sudo rm -f /usr/share/kicad/library/walter_lain
    sudo ln -sv "`pwd`/libs" /usr/share/kicad/library/walter_lain

    sudo rm -f /usr/share/kicad/modules/walter_lain
    sudo ln -sv "`pwd`/libs" /usr/share/kicad/modules/walter_lain
  fi
  cd "$OldPWD"
  echo "Installed successfully: $Install_Dir"
#}
else                            #{ Install failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

exit 0
