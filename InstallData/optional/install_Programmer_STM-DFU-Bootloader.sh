#!/bin/bash
#
#  Install script for ST USB FS Device Library
#  Script written by Sascha Poggemann 2013.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on: Ubuntu 12.10


source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="350"
EXTENSION_SHORT="stm_dfu_bootloader"
EXTENSION_PREFIX="programmer"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

autoreconf

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Packages ]; then #{ install packages
  FailedInstalls=""
  
  installPackageSafe "which autoreconf" autoconf
  installPackageSafe "which gcc"        gcc
  installPackageSafe "which pkg-config" pkg-config
  installPackageSafe "which libtoolize" libtool #?
  installPackageSafe "which libftdi-config" libftdi* #?
  installPackageSafe "ls /usr/include/libusb-1.0/libusb.h" libusb-dev
  #? sudo apt-get remove libusb-1.0.0
  
  
  if [ "$FailedInstalls" == "" ]; then
    touch OK.Packages
  else
    echo "$ScriptName - ERROR: Cannot install required software packages: $FailedInstalls"
    exit 10
  fi
fi #}
if [ ! -e OK.Install ]; then #{

	DirName="dfu-util"
  
	getFile http://thetoolchain.com/mirror/ dfu-util.tar.bz2
	untgj "$DirName" dfu-util.tar.bz2
	if [ ! -d $DirName ]; then
	  gitClone 	git://git.openezx.org/dfu-util $DirName
	fi
	cd "$DirName"
	echo "trying to get updates from git-server.."
	git pull
	echo "compiling source files..."
	./autogen.sh && ./configure && make
	if [ -e src/dfu-util ]; then #{ compiled successfully
    createLink ../$Install_Dir/$DirName/src/dfu-util ../../bin/

    Rules="45-stm_dfu_bootloader-permissions.rules"
    if [ ! -e "/etc/udev/rules.d/$Rules" ]; then #{ create udev-rule
      cat <<"END_OF_RULES" >$Rules #{

# Note: read info of current udev device
# udevadm info -a -p $(udevadm info -q path -n ttyUSB0)

SYSFS{idVendor}=="0483", SYSFS{idProduct}=="df11", MODE="666" GROUP="plugdev" SYMLINK+="usb/stm32_dfu"

END_OF_RULES
#}
      sudo cp $Rules /etc/udev/rules.d/
      sudo udevadm control --reload-rules
    fi #}udev-rule

    cd ..
    touch OK.Install
  #}
  else #{
    echo "$ScriptName - ERROR: Could not compile dfu-util!"
    cd ..
  fi #}
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then #{

  Name="${Install_Dir}"  
  createExtensionMakefileHead ${Name}      #{ (create makefile)
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
#MAIN_OBJS += 

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "STLinkV2 programmer/debugger using stlink software" #{ (create activate script)
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# ACTIVATE_SECTION_A remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
# activate.500_ttc_memory.sh QUIET "\$ScriptName"

# update link to current flash script
createLink flash_stm32_dfu_bootloader.sh _/flash_current.sh
rm _/debug_current.sh


END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

	echo "installed successfully $Install_Dir"
	
#}
else
  echo "failed to install $Install_Dir"
  exit 10
fi


#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
