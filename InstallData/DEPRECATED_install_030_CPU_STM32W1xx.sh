#!/bin/bash

#
#  Install script for combined Microcontroller + 2,4GHz radio STM32 Wxxx 
#  with ARM Cortex M3 core.
#
#  Script written by Gregor Rebel 2012-2014
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

CPURank="200"  # rank of cpu
LibsRank1="251" # rank of cpu libraries (simplemac)
LibsRank2="253" # rank of cpu libraries (hal)
LibsRank3="250" # rank of cpu libraries (std_peripheral)

INSTALL_PREFIX="cpu_stm32w1xx"
setInstallDir "${CPURank}_$INSTALL_PREFIX" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
  LinkFirmware="SimpleMac-Firmware"
  LinkLibrary201="SimpleMAC_201"

  if [ ! -e OK.Documentation ]; then #{ download documentation
    Dir=`pwd`
    Get_Fails=""
    dir ../../Documentation/uC
    dir ../../Documentation/uC/STM32W1xx
    
    Error=""
    #{ download docs..
    getFile http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/PROGRAMMING_MANUAL/ CD00280769.pdf PM0073-STM32W1xx_Programming_Manual.pdf                                         ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/        CD00262415.pdf UM0894-STM32W-SK_and_STM32W-EXT_starter_and_extension_kits.pdf                  ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/web/en/resource/technical/document/user_manual/                           CD00262415.pdf UM0894-STM32W-SK_and_STM32W-EXT_starter_and_extension_kits.pdf
    getFile http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/        CD00280375.pdf UM0978-Using_the_Simple_MAC_nodetest_application.pdf                            ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/        CD00264812.pdf UM0909-STM32W108xx_ZigBee_RF4CE_library.pdf                                     ../../Documentation/uC/STM32W1xx/
    #Not available anymore: getFile http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00271698.pdf AN3211-Using_the_simulated_EEPROM_with_the_STM32W108_platform.pdf               ../../Documentation/uC/STM32W1xx/
    getFile http://www.po-star.com/public/uploads/                                              20120227121858_397.pdf AN3211-Using_the_simulated_EEPROM_with_the_STM32W108_platform.pdf               ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00282958.pdf AN3262-Using_the_over-the-air_bootloader_with_STM32W108_devices.pdf             ../../Documentation/uC/STM32W1xx/
    #Not available anymore: getFile http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00269334.pdf AN3188-Preparing_custom_devices_for_the_STM32W108_platform.pdf                  ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00270908.pdf AN3206-PCB_design_guidelines_for_the_STM32W108_platform.                        ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   DM00024648.pdf AN3359-Low_cost_PCB_antenna_for_2.4GHz_radio_Meander_design.pdf                 ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00004479.pdf AN1709-EMC_design_guide_for_ST_microcontrollers.pdf                             ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00274443.pdf AN3218-Adjacent_channel_rejection_measurements_for_the_STM32W108_platform.pdf   ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/DATA_BRIEF/         CD00237927.pdf DB0837-High-performance_802.15.4_wireless_system-on-chip_databrief.pdf          ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/DATASHEET/          CD00248316.pdf DS6473-High-performance_IEEE_802.15.4_wireless_system-on-chip_Datasheet.pdf     ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/       CD00262339.pdf UM0893-STM32W108xx_SimpleMAC_library.pdf                                        ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/       DM00065100.pdf UM1576-Description_of_STM32W108xx_Standard_Peripheral_Library.pdf               ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/internet/com/SALES_AND_MARKETING_RESOURCES/MARKETING_PRESENTATIONS/PRODUCT_PRESENTATION/ stm32w_marketing_pres.pdf  STM32W-Product_presentation.pdf                      ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/st-web-ui/static/active/en/resource/technical/document/release_note/      CD00262343.pdf RN0046_SimpleMAC_library_for_STM32W108xx_kits.pdf                               ../../Documentation/uC/STM32W1xx/
    getFile http://www.st.com/web/en/resource/technical/document/application_note/                      DM00060426.pdf AN4142-STM32W108xx_datasheet_bit_and_register_naming_migration.pdf
    getFile http://www.st.com/web/en/resource/technical/document/errata_sheet/                          DM00032130.pdf STM32W108xx-Errata_sheet-STM32W108xx_device_limitations
    getFile http://www.st.com/web/en/resource/technical/document/application_note/                      DM00059814.pdf AN4141-Migration_and_compatibility_guidelines_for_STM32W108xx_microcontroller_applications_based_on_the_HAL.pdf ../../Documentation/uC/STM32W1xx/
    
    getFile http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    EM35x.pdf      Ember_Manual_EM35x.pdf                                                          ../../Documentation/uC/STM32W1xx/
    getFile http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    UG103.pdf      Ember_UG103.0_Application_Development_Fundamentals.pdf                          ../../Documentation/uC/STM32W1xx/
	  getFile http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN698.pdf      Ember_AN698_PCB_Design_with_an_EM35x.pdf                                        ../../Documentation/uC/STM32W1xx/
	  getFile http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN703.pdf      Ember_AN703_Using_the_Simulated_EEPROM.pdf                                      ../../Documentation/uC/STM32W1xx/
	  getFile http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN710.pdf      Ember_AN710_Bringing_up_Custom_Devices_for_the_EM35x_SoC_Platform.pdf           ../../Documentation/uC/STM32W1xx/
	  getFile http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN713.pdf      Ember_AN713_Measuring_EM35x_Power_Consumption.pdf                               ../../Documentation/uC/STM32W1xx/
	  getFile http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN715.pdf      Ember_AN715_Using_the_EM35x_ADC.pdf                                             ../../Documentation/uC/STM32W1xx/
	  getFile http://www.telegesis.com/downloads/general/                                     tg-etrx35x-pm-010-107.pdf  Telegesis_Product_Manual_ETRX35x_ZigBee_Module.pdf                              ../../Documentation/uC/STM32W1xx/
	  getFile http://www.telegesis.com/downloads/general/                      TG-PM-0505-CI-AT-Command-Manual%20r1.pdf  Telegesis_AT_Command_Set_for_Combined_Interface.pdf                             ../../Documentation/uC/STM32W1xx/
#}

    if [ "$Get_Fails" == "" ]; then
      echo "Documentation complete: $Install_Dir."
      touch OK.Documentation
    else
      echo "ERROR: Documentations not being received: $Get_Fails"
    fi
  fi #}
  if [ ! -e OK.Install ]; then #{       download + install library via zipped library
    # find -L $HOME/.wine/drive_c -name "*SimpleMAC-1*" -and -type d 2>/dev/null
    
    if [ ! -d "$LinkFirmware" ]; then #{ download + extract archive + run microsoft installer to obtain SimpleMAC library
      echo "missing folder '$LinkFirmware'"
      File="STM32W108_SimpleMAC_firmware"
      getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32w_simplemac_fw.zip $File.zip
      unZIP "e" "${File}.zip"
      
      LinkTarget=`find ./ -maxdepth 1 -iname "STM32W108xx_SimpleMAC_*" -type d`
      createLink $LinkTarget $LinkFirmware
      
      #{
      #{
      replaceInFile "${LinkFirmware}/Utilities/STM32W108xx_HAL_Driver/micro/cortexm3/micro.c"  "#pragma pack"           "// TheToolChain (causes compiler warning) #pragma pack"
      replaceInFile "${LinkFirmware}/Utilities/STM32W108xx_HAL_Driver/micro/cortexm3/micro.c"  "\} appSwitchStructType;" "\} __attribute__((__packed__)) appSwitchStructType; // TheToolChain GCC compliant packed structure"
    fi #}
    if [ ! -d "$LinkLibrary201" ]; then
      getFile http://www.thetoolchain.com/mirror/ ${LinkLibrary201}.tgj ${LinkLibrary201}.tgj
      untgj "${LinkLibrary201}" ${LinkLibrary201}.tgj
    fi
    if [ -d "$LinkFirmware" ]; then   #{ create links to libraries
      MissingFiles="" # updated by createLink()
      #X createLink "$LinkFirmware/Libraries/SimpleMAC" SimpleMac-Library
      createLink "$LinkLibrary201/" SimpleMac-Library
      createLink "$LinkFirmware/Utilities/STM32W108xx_HAL_Driver" hal_library
      createLink "$LinkFirmware/Libraries/STM32W108xx_StdPeriph_Driver" stm_std_peripherals
      if [ "$MissingFiles" == "" ]; then
        touch OK.Install
      else
        echo "$0 - ERROR: missing files: $MissingFiles"
      fi
    else  
      echo "$0 - ERROR: Cannot find installed SimpleMAC-library in $LinkFirmware !"
      exit 0
    fi #}
  fi #}
  if [ ! -e OK.Install ]; then #{       download + install library via Windows Installer package (DEPRECATED)
    # find -L $HOME/.wine/drive_c -name "*SimpleMAC-1*" -and -type d 2>/dev/null
    
    if [ ! -e OK.Wine ]; then #{      install required software packages

  # make sure that eula is accepted automatically    
cat <<END_OF_TEXT |  sudo /usr/bin/debconf-set-selections
ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true
END_OF_TEXT

    installPackage wine
    Wine=`which wine 2>/dev/null`
    if [ "$Wine" != "" ]; then 
      touch OK.Wine
    else
      echo "$0 - ERROR: Cannot install wine!"
      exit 10
    fi
  fi #}

    Folder="$HOME/.wine/drive_c/Program Files (x86)/STMicroelectronics"
    if [ ! -d "$Folder" ]; then
      Folder="$HOME/.wine/drive_c/Program Files/STMicroelectronics"
    fi
    if [ ! -d "$Folder" ]; then #{ download + extract archive + run microsoft installer to obtain SimpleMAC library
      echo "missing folder '$Folder'"
      File="STM32W108_SimpleMAC_firmware"

      getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ st_zigbee_simplemac.zip $File.zip
      unZIP "e" "${File}.zip"
      echo "Starting windows installer for STM32Wxxx SimpleMAC-library. Please install into default directory (press OK in every dialog)!"
      Archive=`ls *SimpleMAC*exe`
      createLink "$Archive" SimpleMAC.exe
      if [ -e SimpleMAC.exe ]; then
        wine ./SimpleMAC.exe
      else
        echo "$0 - ERROR: Cannot find SimpleMAC.exe!"
        exit 13
      fi
    fi #}
    if [ -d "$Folder" ]; then   #{ create links to libraries
     
      Version=`ls "$Folder/"`
      MissingFiles="" # updated by createLink()
      createLink "$Folder/$Version" .
      createLink "$Version" SimpleMac-Library
      createLink "$Version/STM32W108/hal/" hal_library

      if [ "$MissingFiles" == "" ]; then
        touch OK.Install
      else
        echo "$0 - ERROR: missing files: $MissingFiles"
      fi
    #}
    else  
      echo "$0 - ERROR: Cannot find installed SimpleMAC-library in $Folder !"
      exit 0
    fi
  fi #}
  if [ ! -e OK.PatchFiles ]; then #{    patch source files
    echo "`pwd` > patching source files..."
    ERROR=""
    PatchFiles=`find ./ -follow -name "*.c" -exec grep -l "#include BOARD_HEADER" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        echo "replacing BOARD_HEADER in $PatchFile.."
        replaceInFile $PatchFile " BOARD_HEADER"  '"hal/micro/cortexm3/stm32w108/board.h" //BOARD_HEADER (patched for TheToolChain)'
      fi
    done #}

    PatchFiles=`find ./ -follow -name "*.c" -exec grep -l "#include PLATFORM_HEADER" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        echo "replacing PLATFORM_HEADER in $PatchFile.."
        replaceInFile $PatchFile " PLATFORM_HEADER"  '"micro/cortexm3/compiler/gnu.h" //PLATFORM_HEADER (patched for TheToolChain)'
      fi
    done #}
    
    PatchFiles=`find SimpleMac-Firmware/ -follow -name "*" -exec grep -l "RAM_region" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        echo "replacing RAM_region in $PatchFile.."
        replaceInFile $PatchFile RAM_region  'ram'
      fi
    done #}

    PatchFiles=`find SimpleMac-Firmware/ -follow -name "*" -exec grep -l "ROM_region" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        echo "replacing RAM_region in $PatchFile.."
        replaceInFile $PatchFile ROM_region  'rom'
      fi
    done #}
    
    PatchFiles=`find ./ -follow -name "*" -exec grep -l "FIB_region" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        echo "replacing FIB_region in $PatchFile.."
        replaceInFile $PatchFile FIB_region  'fib'
      fi
    done #}

    # for some strange reason, several source-files do not include required headers
    PatchFiles=`find ${LinkFirmware}/Utilities/STM32_EVAL/ -name "*.[ch]" -exec grep -l "UART_InitTypeDef" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        echo "adding missing SC-include to `pwd`/$PatchFile:.."
        chmod +w ${PatchFile}
        cat >${PatchFile}_new <<"END_OF_PATCH"
#include "stm32w108xx_sc.h" // missing include added for The ToolChain'

END_OF_PATCH
        cat ${PatchFile} >>${PatchFile}_new
        mv ${PatchFile}_new ${PatchFile}
      fi
    done #}

    PatchFiles=`find ${LinkFirmware}/ -name "*.c" -exec grep -l "GPIO_Mode_" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        echo "adding missing GPIO-include to `pwd`/$PatchFile:.."
        chmod +w ${PatchFile}
        cat >${PatchFile}_new <<"END_OF_PATCH"
#include "stm32w108xx_gpio.h" // missing include added for The ToolChain'

END_OF_PATCH
        cat ${PatchFile} >>${PatchFile}_new
        mv ${PatchFile}_new ${PatchFile}
      fi
    done #}

    PatchFiles=`find ${LinkFirmware}/ -name "*.c" -exec grep -l "assert_param" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        echo "adding missing ttc_basic-include to `pwd`/$PatchFile:.."
        chmod +w ${PatchFile}
        cat >${PatchFile}_new <<"END_OF_PATCH"
#include "../ttc_basic.h" // basic definitions required by The ToolChain'
END_OF_PATCH
        cat ${PatchFile} >>${PatchFile}_new
        mv ${PatchFile}_new ${PatchFile}
      fi
    done #}

    PatchFiles=`find ${LinkFirmware}/ -name "*.c" -exec grep -l "CLK_GetClocksFreq" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        echo "adding missing ttc_basic-include to `pwd`/$PatchFile:.."
        chmod +w ${PatchFile}
        cat >${PatchFile}_new <<"END_OF_PATCH"
#include "stm32w108xx_clk.h" // missing include added by The ToolChain'
END_OF_PATCH
        cat ${PatchFile} >>${PatchFile}_new
        mv ${PatchFile}_new ${PatchFile}
      fi
    done #}
    
    if [ "$ERROR" == "" ]; then
      touch OK.PatchFiles
    fi
  fi #}
  if [   -e OK.Install ]; then #{       create activate-scripts + makefiles
    ConfigFile="../999_open_ocd/share/openocd/scripts/target/stm32w1xx.cfg"
    #if [ ! -e "$ConfigFile" ]; then
      echo "creating cfg-file '`pwd`/$ConfigFile'.."
      cat <<END_OF_CONFIG >$ConfigFile #{
# http://TheToolchain.com
#
# OpenOCD target definition stm32w108 (former EM357)
# created by $0

if { [info exists CHIPNAME] } {
   set  _CHIPNAME \$CHIPNAME
} else {
   set  _CHIPNAME em357
}

if { [info exists ENDIAN] } {
   set  _ENDIAN \$ENDIAN
} else {
   set  _ENDIAN little
}

# Work-area is a space in RAM used for flash programming
# By default use 16kB
if { [info exists WORKAREASIZE] } {
   set  _WORKAREASIZE \$WORKAREASIZE
} else {
   #set  _WORKAREASIZE 0x4000
   set  _WORKAREASIZE 0x2000
}

# JTAG speed should be <= F_CPU/6. F_CPU after reset is 8MHz, so use F_JTAG = 1MHz
adapter_khz 200

#adapter_nsrst_delay 400
jtag_ntrst_delay 100

#verify_ircapture disable 
#verify_jtag disable


#jtag scan chain
if { [info exists CPUTAPID ] } {
   set _CPUTAPID \$CPUTAPID
} else {
  # See STM Document RM0008
  # Section 26.6.3
   set _CPUTAPID 0x3ba00477
}
jtag newtap \$_CHIPNAME cpu -irlen 4 -ircapture 0x1 -irmask 0xf -expected-id \$_CPUTAPID

if { [info exists BSTAPID ] } {
   # FIXME this never gets used to override defaults...
   set _BSTAPID \$BSTAPID
} else {
  # See STM Document RM0008
  # Section 29.6.2
  # Low density devices, Rev A
  #set _BSTAPID1 0x06412041
  # Medium density devices, Rev A
  #set _BSTAPID2 0x06410041
  # Medium density devices, Rev B and Rev Z
  #set _BSTAPID3 0x16410041
  #set _BSTAPID4 0x06420041
  # High density devices, Rev A
  #set _BSTAPID5 0x06414041
  #set _BSTAPID5 0x06418041
  # Connectivity line devices, Rev A and Rev Z
  #set _BSTAPID6 0x06418041
  # XL line devices, Rev A
  #set _BSTAPID7 0x06430041
  
  
  set _BSTAPID1 0x269a862b
  # set _BSTAPID 0x269a862b
  
  # set tap ID for stm32w.bs in Telegesis ETRX357 radio module
  set _BSTAPID 0x069a962b
}

jtag newtap \$_CHIPNAME bs -irlen 4 -ircapture 0x0e -irmask 0xf  -expected-id \$_BSTAPID

	#	jtag newtap \$_CHIPNAME bs -irlen 5 -expected-id \$_BSTAPID1 \
#	-expected-id \$_BSTAPID2 -expected-id \$_BSTAPID3 \
#	-expected-id \$_BSTAPID4 -expected-id \$_BSTAPID5 \
#	-expected-id \$_BSTAPID6 -expected-id \$_BSTAPID7



set _TARGETNAME \$_CHIPNAME.cpu
#\$_TARGETNAME configure -work-area-phys 0x20000000 -work-area-size \$_WORKAREASIZE -work-area-backup 0

target create \$_TARGETNAME cortex_m -endian \$_ENDIAN -chain-position \$_TARGETNAME

\$_TARGETNAME configure -work-area-phys 0x20000000 -work-area-size \$_WORKAREASIZE -work-area-backup 0

# flash size will be probed
set _FLASHNAME \$_CHIPNAME.flash
#flash bank \$_FLASHNAME stm32f1x 0x08000000 0 0 0 \$_TARGETNAME
flash bank \$_FLASHNAME em357 0x08000000 0x00040000 0 0 \$_TARGETNAME

# if srst is not fitted use SYSRESETREQ to
# perform a soft reset
cortex_m reset_config sysresetreq

END_OF_CONFIG
#}
    #else
    #  echo "cfg-file already exists: '`pwd`/$ConfigFile'.."
    #fi
    cd "$LinkFirmware"
    IncludeDirs_Boards=`find Utilities/STM32_EVAL/ -mindepth 1 -maxdepth 1 -type d -exec echo "COMPILE_OPTS += -I additionals/$Install_Dir/{}" \;`
    SourceDirs_Boards=`find Utilities/STM32_EVAL/ -mindepth 1 -maxdepth 1 -type d -exec echo "vpath %.c  additionals/$Install_Dir/{}" \;`
    cd ..

    ConfigFile="../999_open_ocd/share/openocd/scripts/target/stm32w1xx_flash.cfg"
    #? if [ ! -e "$ConfigFile" ]; then
      echo "creating cfg-file '`pwd`/$ConfigFile'.."
      cat <<END_OF_CONFIG >$ConfigFile #{
# http://TheToolchain.com
#
# OpenOCD flash script for  stm32w108 (former EM357)
# created by $0


# finish configuration stage + proceed to run stage
init

# hardware reset processor with halt (2 times is safer than only once)
reset
soft_reset_halt

# check target state stm32w
poll

# list all found flash banks
flash banks

# identify the flash
flash probe 0

em357 unlock 0

# erasing all flash
em357 mass_erase 0

# write file to flash number 0
flash write_bank 0 main.bin 0

# hardware processor reset with run
reset run

#shutdown openocd daemon
shutdown

END_OF_CONFIG
#}
    #? else
    #?   echo "cfg-file already exists: '`pwd`/$ConfigFile'.."
    #? fi

    Name="${CPURank}_${INSTALL_PREFIX}" #{                   makefile + activate-script for CPU STM32W1xx
    createExtensionMakefileHead ${Name} #{
    File="${Dir_Extensions}makefile.${Name}"
    cat <<END_OF_MAKEFILE >>$File #{

ARM_CPU     =arm7tdmi
TARGET      =-mcpu=cortex-m3 -mthumb
TARGET_ARCHITECTURE_STM32W1xx=1
TARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_PREFIX=EM357
COMPILE_OPTS += -DTARGET_ARCHITECTURE_STM32W1xx
COMPILE_OPTS += -DTARGET_ARCHITECTURE_STM32W1xx

# list of available devices (activate only one!)
STM32W108   =1       # STM32W Device

ifdef uCONTROLLER
  ERROR: STM32W108=1 - Only one uController may be selected!
endif
uCONTROLLER=STM32W108
COMPILE_OPTS += -DuCONTROLLER=STM32W108
COMPILE_OPTS += -DSTM32W108
# SimpleMac Library v2.0.0
#  MAIN_OBJS += stm32w_basic.o startup_stm32w108.o context-switch.o spmr.o micro-common-internal.o micro-common.o
# SimpleMac Library v2.0.1
MAIN_OBJS += stm32w_basic.o startup_stm32w108.o context-switch_gnu.o micro-common-internal.o micro-common.o hal_clocks.o


# v2.0.1 brought up drivers for several ST demo boards of which at least one must be included
$IncludeDirs_Boards
$SourceDirs_Boards


COMPILE_OPTS += -Ittc-lib/stm32w/
vpath %.c ttc-lib/stm32w/

COMPILE_OPTS += -Ittc-lib/stm32w/
vpath %.c ttc-lib/stm32w/

# some settings which may be required by HAL or SimpleMAC
COMPILE_OPTS += -DCORTEXM3_STM32W108
COMPILE_OPTS += -DPHY_STM32W108XX -DDISABLE_WATCHDOG -DCORTEXM3_STM32W108xB -DCORTEXM3

# define linker scripts to use
LDFLAGS+= -Tconfigs/memory_stm32w1xx.ld

# SimpleMAC Library <=v2.0.0
# LDFLAGS+= -Tadditionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/cortexm3/stm32w108/gnu-stm32w108.ld

# define rules for assembler-files which may be compiled into objects
context-switch.o: context-switch.s
context-switch_gnu.o: context-switch_gnu.s
spmr.o: spmr.s

END_OF_MAKEFILE
#}
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} $0 "CPU STM32W1xx" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.200_cpu_*


# create link to target script being required by openocd to initialize target cpu
cd \$Dir_Configs
createLink ../additionals/999_open_ocd/target/stm32w1xx.cfg       openocd_target.cfg
createLink ../additionals/999_open_ocd/target/stm32w1xx_flash.cfg openocd_flash.cfg
cd ..

cd "\$Dir_Additionals"
rm -fv ${CPURank}_cpu_* ${LibsRank1}_cpu_* ${LibsRank2}_cpu_*
createLink \$HOME/Source/TheToolChain/InstallData/$Install_Dir/SimpleMac-Library/  $Install_Dir
cd ..

activate.190_cpu_cortexm3.sh                   QUIET \"\$0\"
activate.250_STM32W1XX_StdPeriph_Driver.sh     QUIET "\$0"
activate.270_CPAL_STM32_CPAL_Driver.sh         QUIET \"\$0\"

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

#X activate.500_ttc_register.sh                    QUIET \"\$0\"

END_OF_ACTIVATE
#}
    createActivateScriptTail $Name ${Dir_Extensions}
    #}
    #}
    Name="${LibsRank1}_${INSTALL_PREFIX}_simple_mac" #{      makefile + activate-script for SimpleMAC-library
     createExtensionMakefileHead ${Name} #{
    File="${Dir_Extensions}makefile.${Name}"
    
    IncludeDir_CPU=`../scripts/findDirectory.pl stm32w108xx.h SimpleMac-Firmware`

    cat <<END_OF_MAKEFILE >>$File #{

EM357_SIMPLEMAC = 1
INCLUDE_DIRS += -I additionals/${LibsRank1}_cpu_stm32w1xx_SimpleMAC_library/Include/
INCLUDE_DIRS += -I additionals/${CPURank}_${INSTALL_PREFIX}/$IncludeDir_CPU

# SimpleMAC library is only available as precompiled binary :-(
#X LD_LIBS += -mthumb additionals/${LibsRank1}_cpu_stm32w1xx_SimpleMAC_library/library/libsimplemac-library-gnu.a
# v2.0.0
# LD_LIBS += additionals/${LibsRank1}_cpu_stm32w1xx_SimpleMAC_library/library/libsimplemac-library-gnu.a
# v2.0.1
# LD_LIBS += additionals/${LibsRank1}_cpu_stm32w1xx_SimpleMAC_library/Binary/simplemac-library.a # incompatible 16 bit wchar
LD_LIBS += additionals/${LibsRank1}_cpu_stm32w1xx_SimpleMAC_library/Binary/libsimplemac-library.a

#? # Binary only simplemac-library uses 16 bit wchars. We must force gcc to use them too!
#? COMPILE_OPTS += -fshort-wchar

END_OF_MAKEFILE
#}
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} $0 "STM32W1xx Simple MAC Library - provides access to on-chip transceiver" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# create links into extensions.active/
createLink ${Dir_Extensions}makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

cd "\$Dir_Additionals"
createLink $Install_Dir/STM32W108/simplemac/                                   ${Name}_SimpleMAC_library
cd ..

activate.${CPURank}_cpu_stm32w1xx.sh QUIET \"\$0\"

END_OF_ACTIVATE
#}
    createActivateScriptTail $Name ${Dir_Extensions}
    #}
    #}_simple_mac
    Name="${LibsRank2}_${INSTALL_PREFIX}_hal" #{             makefile + activate-script for HAL-library
    createExtensionMakefileHead ${Name} #{
    File="${Dir_Extensions}makefile.${Name}" 
    cat <<END_OF_MAKEFILE >>$File #{

ifndef EM357_SIMPLEMAC
  COMPILE_OPTS += -DHAL_STANDALONE   # This is a simplified version of the full stCalibrateVref
                                     # It is meant to be used on build for hal only application not linked with
                                     # simplemac library. The full version is embedded in the simplemac library.
                                     # The simplified version does not support calibration when manufacturing tokens are missing
                                     # or radio boost mode is activated
                                     # File: micro-common-internal.c
endif


INCLUDE_DIRS += -I additionals/${CPURank}_cpu_stm32w1xx/STM32W108/

INCLUDE_DIRS += -I additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/
INCLUDE_DIRS += -I additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/
INCLUDE_DIRS += -I additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/cortexm3/stm32w108/
INCLUDE_DIRS += -I additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/cortexm3/
INCLUDE_DIRS += -I additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/cortexm3/compiler/
INCLUDE_DIRS += -I additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/generic/compiler
INCLUDE_DIRS += -I additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/cortexm3/compiler
INCLUDE_DIRS += -I additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/cortexm3/bootloader/

INCLUDE_DIRS += -I additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/


#? vpath additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/cortexm3/stm32w108/

# where to find files of different types
INCLUDE_DIRS += -Iadditionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/cortexm3/stm32w108/
vpath %.c additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/cortexm3/
vpath %.c additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/
vpath %.c additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/cortexm3/stm32w108/
vpath %.s additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/cortexm3/stm32w108/
vpath %.s additionals/${LibsRank2}_cpu_stm32w1xx_hal_library/micro/cortexm3/

# SimpleMaC-Library v2.0.0
#MAIN_OBJS += micro.o board.o clocks.o mfg-token.o adc.o flash.o nvm.o

# SimpleMaC-Library v2.0.1
MAIN_OBJS += micro.o hal_adc.o low_level_init.o mfg-token.o nvm.o hal_flash.o 

# Objects provided by The ToolChain for compatibility reasons
MAIN_OBJS += stm32w_dummyboard.o

END_OF_MAKEFILE
#}
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} $0 "STM32W1xx Hardware Abstraction Layer library (HAL)" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# create links into extensions.active/
createLink ${Dir_Extensions}makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

cd "\$Dir_Additionals"
createLink $Install_Dir/hal_library/ ${Name}_hal_library
cd ..

activate.${CPURank}_cpu_stm32w1xx.sh  QUIET "\$0"
#?? activate.500_ttc_string.sh QUIET "\$0"  # ttc_string_printf() is required

END_OF_ACTIVATE
#}
    createActivateScriptTail $Name ${Dir_Extensions}
    #}
    #}_hal
    StdPeriphDriver="250_STM32W1XX_StdPeriph_Driver"                 #{ makefile + activate-script for STM Standard Peripherals Library
    Name=${StdPeriphDriver}
    createExtensionMakefileHead ${Name} #{
    File="${Dir_Extensions}makefile.${Name}" 
    cat <<END_OF_MAKEFILE >>$File #{

# include files of STM standard peripherals library
INCLUDE_DIRS += -I additionals/${StdPeriphDriver}/inc/

# source files of STM standard peripherals library
vpath %.c additionals/${StdPeriphDriver}/src/

END_OF_MAKEFILE
#}
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} $0 "STM32W1xx Hardware Abstraction Layer library (HAL)" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# create links into extensions.active/
createLink ${Dir_Extensions}makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

cd "\$Dir_Additionals"
createLink $Install_Dir/stm_std_peripherals/ ${Name}
cd ..

# create links into extensions/
cd "\${Dir_Extensions}"
rm 2>/dev/null *250_stm_std_peripherals_*

Architecture="STM32W1XX"
DriversFound=\`ls \$Architecture/*\`
if [ "\$DriversFound" != "" ]; then
  ln -sv \$Architecture/* .
else
  echo "ERROR: Cannot find drivers in `pwd`/\$Architecture/ !"
fi
cd "\$CurrentPath"

activate.${CPURank}_cpu_stm32w1xx.sh  QUIET "\$0"
#?? activate.500_ttc_string.sh QUIET "\$0"  # ttc_string_printf() is required

END_OF_ACTIVATE
#}
    createActivateScriptTail $Name ${Dir_Extensions}
    
    #}_std_peripherals
    #}
    Name="250_stm_std_peripherals"                        #{ makefiles + activate-scripts for STM Standard Peripherals Library drivers
    
    SubDir='stm_std_peripherals/src/'
    TargetDir="${Dir_Extensions}/STM32W1XX"
    dir "$TargetDir"
    for SourceFile in `find $SubDir -name "*.c"`; do #{      makefile + activate-script for individual drivers of STM Standard Peripherals Library
      Driver=`perl -e "print substr('$SourceFile', length('$SubDir'), -2);"`
      DriverShortName=`perl -e "print substr('$Driver', 12);"` # remove stm32w108xx_ prefix
      DriverName="${Name}__$DriverShortName"
      echo "creating activate for driver '$DriverName'..."
      
      createExtensionMakefileHead ${DriverName} $TargetDir/     #{ create makefile
      cat <<END_OF_MAKEFILE >>$TargetDir/makefile.$DriverName 

# additional object files to be compiled
MAIN_OBJS += ${Driver}.o

END_OF_MAKEFILE
      createExtensionMakefileTail ${DriverName} $TargetDir/ #}
      createActivateScriptHead $DriverName $TargetDir/ "$0 2" "low-level driver for STM32W1xx functional unit $Driver (MAIN_OBJS+=${Driver}.o)" #{
      cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/$Install_Dir_*

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$DriverName \${Dir_ExtensionsActive}/makefile.$DriverName '' QUIET

activate.250_STM32W1XX_StdPeriph_Driver.sh QUIET "\$0"

END_OF_ACTIVATE
      createActivateScriptTail $DriverName $TargetDir/
      #}

    done
    #}
    
    #}std_peripherals
    if [ "1" == "0" ]; then #{ DEPRECATED
    #{ create one activate-script per c-file
    TargetDir="${Dir_Extensions}$Architecture"
    dir "$TargetDir"

    LibPrefix="250_stm_std_peripherals"
    for SourceFile in `find stm_std_peripherals/src/ -name "*\.c" -execdir echo -n "{} " \;` ; do #{
      SourceName=`perl -e "print substr('$SourceFile', 2, -2);"`
      Source=`perl -e "print substr('$SourceFile', 12, -2);"` # remove stm32w108xx_ prefix
      if [ "$Source" != "" ]; then
        DestName="${LibPrefix}__${Source}"
        echo ">>>>>>>DestName='$DestName'" #D
        createExtensionMakefileHead ${DestName} $TargetDir/ #{
        cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # set by createExtensionMakefileHead() 
  
MAIN_OBJS += ${SourceName}.o

END_OF_MAKEFILE
        createExtensionMakefileTail ${DestName} $TargetDir/ #}
        createActivateScriptHead $DestName $TargetDir/ $0 "adds c-source to list of compiled objects: ${SourceName}.c" #{
        cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# add createL.inks for each link required for this extension
createLink ${Dir_Extensions}makefile.$DestName \${Dir_ExtensionsActive}/makefile.$DestName '' QUIET

END_OF_ACTIVATE
#}
        createActivateScriptTail $DestName $TargetDir/
        #}
      fi
    done #}
    #} create one activate-script per c-file
  fi #}DEPRECATED
    
    # create permanent symbolic links to source-directories
    addLine ../scripts/createLinks.sh "rm 2>/dev/null ${CPURank}_${INSTALL_PREFIX};                      createLink \$HOME/Source/TheToolChain/InstallData/${CPURank}_${INSTALL_PREFIX}/SimpleMac-Firmware  ${CPURank}_${INSTALL_PREFIX}"
    addLine ../scripts/createLinks.sh "rm 2>/dev/null ${LibsRank1}_${INSTALL_PREFIX}_SimpleMAC_library;  createLink \$HOME/Source/TheToolChain/InstallData/${CPURank}_${INSTALL_PREFIX}/SimpleMac-Library   ${LibsRank1}_${INSTALL_PREFIX}_SimpleMAC_library"
    addLine ../scripts/createLinks.sh "rm 2>/dev/null ${LibsRank2}_${INSTALL_PREFIX}_hal_library;        createLink \$HOME/Source/TheToolChain/InstallData/${CPURank}_${INSTALL_PREFIX}/hal_library         ${LibsRank2}_${INSTALL_PREFIX}_hal_library"
    addLine ../scripts/createLinks.sh "rm 2>/dev/null ${StdPeriphDriver};    createLink \$HOME/Source/TheToolChain/InstallData/${CPURank}_${INSTALL_PREFIX}/stm_std_peripherals ${StdPeriphDriver}"

    echo "Installed successfully: ${CPURank}_${INSTALL_PREFIX}"
  fi #}
  cd ..
  rmdir 2>/dev/null $Install_Dir  # removes only empty install dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
