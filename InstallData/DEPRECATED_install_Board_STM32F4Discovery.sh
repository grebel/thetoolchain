#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Initial Script written by Gregor Rebel 2010-2017.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="100"
EXTENSION_SHORT="stm32f4_discovery"
EXTENSION_PREFIX="board"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$ScriptName: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
source $NewestDownloads
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$ScriptName - ERROR: Cannot find newest GCC version!"
#   exit 10
# fi #}

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://de.system-failures.org/projects/stm32f4discovery-board-unter-linux/ index.html stm32f4discovery-board-unter-linux.html Boards/STM32F4Discovery/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

if [ ! -e OK.Install ]; then    #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe 'which autoreconf' autoreconf
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts

  Name="${Install_Dir}"  
  createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

//  #include "YOUR_SOURCE_HERE.h"
//  YOUR_START_HERE();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name}      #{ (create makefile)
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f4.... = 1 ToDo!!!

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
#MAIN_OBJS += 

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "PLACE YOUR INFO HERE FOR $ScriptName" #{ (create activate script)
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
# activate.500_ttc_memory.sh QUIET "\$ScriptName"

enableFeature 450_cpu_stm32f4xx

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
  
  # (addLine) 
  # create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
  #
  # Activate line below and change SUBFOLDER to your needs
  #addLine _/createLinks.sh "rm 2>/dev/null $Install_Dir; createLink \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"


  if [ "1" == "0" ]; then #{ create regression test for your extension
  
    Name="${Install_Dir}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

//  #include "YOUR_SOURCE_HERE.h"
//  YOUR_START_HERE();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
#MAIN_OBJS += 

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "PLACE YOUR INFO HERE FOR $ScriptName" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Install_Dir}_*

# ACTIVATE_SECTION_B create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
#addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; createLink \$Source/TheToolChain/InstallData/${Install_Dir}/ $Install_Dir"

# ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_E call other activate-scripts
# activate.500_ttc_memory.sh QUIET "\$ScriptName"


END_OF_ACTIVATE
    createActivateScriptTail $Name ${Dir_Extensions}
    #}

  fi #}
    
  cd "$OldPWD"
  echo "Installed successfully: $Install_Dir"
#}
else                            #{ Install failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
