#!/bin/bash
#
#
#  Install script for generic SPI support 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Initial Script written by Gregor Rebel 2010-2012.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="500"
EXTENSION_NAME="ttc_memory"

StartPath="`pwd`"
INSTALLPATH="${RANK}_${EXTENSION_NAME}"
dir "$INSTALLPATH"
cd "$INSTALLPATH"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

  Name="$INSTALLPATH"
  createExtensionSourcefileHead "499_${Name}"    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

  #include "../ttc_memory.h"
  ttc_memory_init_heap();

END_OF_SOURCEFILE
  createExtensionSourcefileTail "499_${Name}" #}    
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += ttc_memory.o sbrk.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ "$0" "Architecture independent support for memory operations" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_Additionals/${Name}_*

# create links into extensions.active/
createLink "\$Dir_Extensions/makefile.$Name" "\$Dir_ExtensionsActive/makefile.$Name" '' QUIET

activate.500_ttc_mutex.sh     QUIET "\$0"
activate.500_ttc_semaphore.sh QUIET "\$0"
activate.500_ttc_list.sh      QUIET "\$0"

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# activate these after creating symbolic link (avoids endless loop if called from one of these scripts)
activate.500_ttc_memory_heap_freertos.sh QUIET "\$0"
activate.500_ttc_memory_heap_simple.sh   QUIET "\$0"
# activate.500_ttc_memory_heap_sdma.sh     QUIET "\$0"

END_OF_ACTIVATE
  createActivateScriptTail $Name ../extensions/
  #}

  Name="${INSTALLPATH}_pool_regression"  
  createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

  #include "regression_memory_pools.h"
  regression_memory_pools_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name}      #{ create makefile for memory pool regression
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional directories to search for header files
INCLUDE_DIRS += -I regressions/

# additional directories to search for C-Sources
vpath %.c regressions/ 

# additional object files to be compiled
MAIN_OBJS += regression_memory_pools.o

# we have to define at least one font to make ttc_font.h happy
COMPILE_OPTS += -DTTC_FONT1=font_type1_16x24

# activate debug-pins in ttc_memory.c
COMPILE_OPTS += -DTTC_MEMORY_DEBUG_POOL_RELEASE_PIN=tgp_e5
COMPILE_OPTS += -DTTC_MEMORY_DEBUG_POOL_ALLOC_PIN=tgp_e0

# activate extra checks for regression tests
COMPILE_OPTS += -DTTC_REGRESSION

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ "$0 2" "regression test for memory pools" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/$INSTALLPATH_*

activate.300_scheduler_freertos_heap4.sh QUIET "\$0"
activate.300_scheduler_freertos_stack_check_intense.sh QUIET "\$0"
activate.500_ttc_gfx.sh        QUIET "\$0"
activate.500_ttc_string.sh     QUIET "\$0"
activate.500_ttc_list.sh       QUIET "\$0"
activate.500_ttc_queue.sh      QUIET "\$0"
activate.500_ttc_semaphore.sh  QUIET "\$0"
activate.500_ttc_mutex.sh      QUIET "\$0"
activate.500_ttc_random.sh     QUIET "\$0"
activate.500_ttc_math_basic.sh QUIET "\$0"
activate.500_ttc_task.sh       QUIET "\$0"

# at least one font must be activated
activate.510_ttc_font__font_type1_16x24.sh QUIET "\$0"

# create links into extensions.active/
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ../extensions/
  #}

  Name="${INSTALLPATH}_heap_freertos"  
  createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name}      #{ create makefile for memory pool regression
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional directories to search for header files
INCLUDE_DIRS += -I ttc-lib/heap/

# additional directories to search for C-Sources
vpath %.c ttc-lib/heap/ 

# additional object files to be compiled
MAIN_OBJS += heap_freertos.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ "$0 2" "use dynamic memory allocator provided by FreeRTOS (check heap-settings of FreeRTOS)" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

Required="\$Dir_ExtensionsActive/makefile.300_scheduler_freertos"
if [ "\`ls \$Required\`" != "" ]; then
  # remove activated variants of same type
  rm 2>/dev/null \$Dir_ExtensionsActive/*${INSTALLPATH}_heap_*
  
  # create links into extensions.active/
  createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET
  
  activate.500_ttc_memory.sh        QUIET "\$0"
  
  # ACTIVATE_SECTION_C activate initialization source-code for this extension 
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
else
  echo "\$0 not activated (missing file '\$Required')"
fi

END_OF_ACTIVATE
  createActivateScriptTail $Name ../extensions/
  #}

    Name="${INSTALLPATH}_heap_simple"  
  createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name}      #{ create makefile for memory pool regression
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional directories to search for header files
INCLUDE_DIRS += -I ttc-lib/heap/

# additional directories to search for C-Sources
vpath %.c ttc-lib/heap/ 

# additional object files to be compiled
MAIN_OBJS += heap_simple.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ "$0 2" "use simple heap implementation (single-task, can only allocate)" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

Forbidden="\$Dir_ExtensionsActive/makefile.300_scheduler_freertos"
if [ "\`ls \$Forbidden\`" == "" ]; then
  # remove activated variants of same type
  rm 2>/dev/null \$Dir_ExtensionsActive/*${INSTALLPATH}_heap_*
  
  # create links into extensions.active/
  createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET
  
  activate.500_ttc_memory.sh        QUIET "\$0"
  
  # ACTIVATE_SECTION_C activate initialization source-code for this extension 
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
else
  echo "\$0 not activated (detected scheduler "\$Forbidden")"
fi

END_OF_ACTIVATE
  createActivateScriptTail $Name ../extensions/
  #}

  Name="${INSTALLPATH}_heap_sdma"  
  createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

  #include "heap_sdma.h"
  heap_sdma_init();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name}      #{ create makefile for memory pool regression
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional directories to search for header files
INCLUDE_DIRS += -I ttc-lib/heap/

# additional directories to search for C-Sources
vpath %.c ttc-lib/heap/ 

# additional object files to be compiled
MAIN_OBJS += heap_sdma.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ "$0 2" "implementation of a Smart Dynamic Memory Allocator" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
rm 2>/dev/null \$Dir_ExtensionsActive/*${INSTALLPATH}_heap_*

activate.500_ttc_memory.sh        QUIET "\$0"

# create links into extensions.active/
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ../extensions/
  #}

  if [ "1" == "1" ]; then #{ create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}_pool"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "../examples/example_${EXTENSION_NAME}_pool.h"
example_${EXTENSION_NAME}_pool_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}_pool.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ../extensions/ "$0 2" "Example on memory pool allocation and conversion to list items" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${INSTALLPATH}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${INSTALLPATH}.sh QUIET "\$0"
activate.500_ttc_gpio.sh QUIET "\$0"

END_OF_ACTIVATE
    createActivateScriptTail $Name ../extensions/
    #}

  fi #}

  echo "Installed successfully: $Name"
  cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$StartPath"
applyTargetUserAndGroup "$INSTALLPATH"

exit 0
