#!/bin/bash

#{  Meta install script for The ToolChain.
#  written by Gregor Rebel 2011-04-11 - 2014
#  
#  This script will automatically run all install_* scripts in current folder.
# 
#  Feel free do distribute and change to your own needs!
#}

source scripts/installFuncs.sh
Present=`grep '#!/bin/bash' scripts/SourceMe.sh`
if [ "$Present" == "" ]; then #{ prepent file with header
  if [ -e scripts/SourceMe.sh ]; then
    mv scripts/SourceMe.sh scripts/SourceMe.sh_
  fi
  cat <<END_OF_HEADER >>scripts/SourceMe.sh
#!/bin/bash

#                The ToolChain
#
#             Basic Evironment Setup
#
#            http://thetoolchain.com
#
# file created by $0 on `date`


END_OF_HEADER
  if [ -e scripts/SourceMe.sh_ ]; then
    cat scripts/SourceMe.sh_ >>scripts/SourceMe.sh
    rm -f scripts/SourceMe.sh_
  fi
fi #}

function extractFilename() {
  Path="$1"

  export FileName=`perl -e "print substr('$Path', rindex('$Path', '/')+1);"`  
}

Option="$1"
if [ "$Option" == "" ]; then #{ print help + error + exit
  cat <<END_OF_HELP 
$0 BASIC|ALL|MODULE [USERNAME[ GROUP]]

The Toolchain
Starts installation of extension packages.

BASIC     will run all install_XXX.sh scripts in current folder.
ALL       will also run install_XXX.sh scripts in folder optional/

USERNAME  if given, all files will be chowned to this user
          Note: Script must be run by user root for this to work!
GROUP     if given, all files will be chowned to this group

Examples:

$0 BASIC
sudo $0 ALL user1 users

$Error
END_OF_HELP

  if [ "$Error" != "" ]; then
    echo ""
    exit 10
  fi
  exit 0
fi #}
if [ -e "../.git" ]; then #{
  cat <<END_OF_HELP
	
$0 - ERROR: YOU MUST NOT RUN THIS SCRIPT FROM A GIT-REPOSITORY!

Create a development version instead:

> cd ..; InstallData/scripts/createDevelopmentVersion.pl

END_OF_HELP
  exit 11
fi #}

checkUserRoot "$2" "$3"

if [ "$Option" == "BASIC" ]; then #{
  Install_Basic="1"
fi #}
if [ "$Option" == "ALL" ]; then #{
  Install_Basic="1"
  Install_Optional="1"
fi #}


if [ "$Install_Basic" == "1" ]; then #{ run all install-files
  Files=`ls install_*.sh`
  WGET_QUIET="1" # read by installFuncs.sh/get()
  
  if [ "$Install_Optional" == "1" ]; then # add optional files
    Files="$Files `ls optional/install_*.sh`"
  fi

  echo "removing all OK.Documentation files as a workaround for broken documentation folder when installing TTC again  (ToDo: fix issue with broken Documentation folder!)"
  find ./ -name "OK.Documentation" -exec rm {} \;
  rm -Rf Documentation # This folder should not exist inside InstallData/

  touch scripts/features.UPDATE
  
  #{ prepare for installation
  #X rm -f   2>/dev/null ErrorOccured OK.AllInstalls
  rm -f   2>/dev/null ErrorOccured
  rm -Rf 2>/dev/null extensions/*
  #X echo "" >cleanup.sh
  dir Logs
  dir Logs/testing
  dir Logs/optional
  for Script in $Files; do
    extractFilename $Script
    if [ ! -e Logs/$FileName.log ]; then
      echo "" >Logs/$FileName.log
    fi
  done
  
  #}prepare for installation
  identifyOS
  if [ "$OS_TYPE" == "opensuse" ]; then #{ add some repositories for openSUSE
    PackManAvailable=`sudo zypper lr | grep -i packman`
    if [ "$PackManAvailable" == "" ]; then
      echo "adding PackMan repository for openSuSE v$OS_VERSION"
      sudo zypper addrepo -f http://packman.inode.at/suse/$OS_VERSION/ PackMan
    fi
  fi #}
  if [ "`which apt-get`" != "" ]; then #{ try to bring packet management into stable state
    echo "Cleaning apt database (tail -f `pwd`/log.apt)"
    echo "> sudo apt-get install -f"; sudo apt-get install -f >log.apt 2>&1
    echo "> sudo apt autoremove -y";  sudo apt autoremove -y >log.apt 2>&1
  fi #}

  InstallAllDir=`pwd`
  PreviousToolChainFound=`ls $HOME/Source/TheToolChain_* -d`
  if [ "$PreviousToolChainFound" == "" ]; then
    if [ ! -e OK.SoftwareUpdates ]; then #{ get system updates + install software packages
      echo "missing file ${InstallAllDir}/OK.AllInstalls"
      if [ -e /usr/bin/apt-get ]; then
        echo "getting required system updates..."
        updateSoftware
      fi
      if [ ! -e OK.Packages ]; then #{ install some general software packages
        FailedInstalls="" 
        installPackageSafe "which git"        git gitosis
        installPackageSafe "which svn"        svn subversion
        installPackageSafe "which jedit"        jedit
#X        installPackageSafe "which xterm"      xterm
#X        installPackageSafe "which aterm"      aterm
        installPackageSafe "which pkg-config" pkg-config
        if [ "$FailedInstalls" == "" ]; then
          touch OK.Packages
          touch OK.SoftwareUpdates
        else
          echo "$0 - ERROR: Cannot install required software packages: $FailedInstalls"
          exit 10
        fi
      fi #}
    fi #}
  fi
  #{ createLinks
  cat <<END_OF_LINKS >scripts/createLinks.sh #{
#!/bin/bash
# createLinks.sh created `date` by $0
#
# This script is part of TheToolChain

PWD=\`pwd\`
ScriptDir=\`dirname \$PWD/\$0\`
source \$ScriptDir/installFuncs.sh

if [ "\$Source" == "" ]; then
  Source="\$HOME/Source"
fi
if [ -e _/qtcreator.sh ]; then
  rm -f qtcreator.sh
  ln -s _/qtcreator.sh .
fi
if [ ! -d extensions.active ]; then
  mkdir extensions.active
fi
cd extensions.active
if [ ! -e readme.txt ]; then
  echo <<"END_OF_README" >readme.txt

                               TheToolChain

                         Extension to central makefile

                       written by Gregor Rebel 2010-`date +%Y`


All makefile.* files in this folder are automatically being included by ../makefile

The idea of this folder is to activate certain extensions by creating symbolic
links to files inside ../extensions

These links get created by several activate-scripts from ../activate_project.sh
You can find these activate scripts inside ${Dir_Extensions}

END_OF_README
fi

MakefilesPresent=\`ls | grep makefile.\`
cd ..

rm -f 2>/dev/null extensions;  createLink \$Source/TheToolChain/InstallData/extensions  extensions

dir additionals
cd additionals

# Create  entry point for third party includes to allow IDE to find header- and source-files
# Problem: includes in files inside ttc-library/ can only use relative paths if folder additionals/
#         resides next to folder TTC-Library/
rm -f ~/Source/TheToolChain/additionals
ln -sv \`pwd\` ~/Source/TheToolChain/additionals


END_OF_LINKS
#}
  chmod +x scripts/createLinks.sh  
  if [ -d ../Template/additionals/ ]; then
    rm -f 2>/dev/null ../Template/additionals/*
  else
    mkdir ../Template/additionals
  fi
  if [ -d extensions/ ]; then
    rm -f 2>/dev/null extensions/*
  else
    mkdir extensions
  fi
  
  ActivateScript="../Template/activate_project.sh"
  if [ -e "$ActivateScript" ]; then #{
    echo "removing cached `pwd`/$ActivateScript.."
    rm -f "$ActivateScript"
  fi #}
  
  echo "making some scripts globally available.."
  rm -f bin/findNewestDownload.pl
  createLink ../scripts/findNewestDownload.pl bin/
  
  #} createLinks
  #{ copy QtCreator.ini (basic settings)
  
  if [ ! -d ~/.config/ ]; then
    mkdir ~/.config
  fi
  if [ ! -d ~/.config/Nokia/ ]; then
    mkdir ~/.config/Nokia/
  fi
  if [ ! -e ~/.config/Nokia/QtCreator.ini ]; then
    rm -f ~/.config/Nokia/QtCreator.ini
    cp ../Template/QtCreator/QtCreator.ini ~/.config/Nokia/QtCreator.ini
  fi
  
  #}QtCreator.ini
  #{ run all install scripts
  if [ ! -e ../Version ]; then #{
    cat <<END_OF_ERROR
`pwd`> $0 - ERROR: Cannot find file ../Version
END_OF_ERROR
    exit 11
  fi #}
  CurrentVersion=`cat ../Version`
  NewToolChain="TheToolChain_$CurrentVersion"
  echo "installing $NewToolChain'..."
  LogFolder="`pwd`/Logs/*"

  if [ "1" == "0" ]; then #{ spawn xterm to show current log messages.. (disabled because xterm is very slow on newer window managers like KDE5) 
    Title="Installing TheToolChain v$CurrentVersion"
    XtermIsRunning=`ps -Af | grep "$Title" | grep -v grep`
    if [ "$XtermIsRunning" == "" ]; then #{
      Available=`which aterm`
      if [ "$Available" != "" ]; then
        aterm -T "$Title" -geometry 1920 -e /usr/bin/tail -f Logs/* &
      else
        Available=`which gnome-terminal`
        if [ "$Available" != "" ]; then
          gnome-terminal -x /usr/bin/tail -f Logs/* &
        else
          Available=`which xterm`
          if [ "$Available" == "" ]; then
            installPackage xterm
          fi
          xterm -T "$Title" -geometry 1920 -e "tail -f $LogFolder" &
        fi
      fi
    fi #}
  fi #}
  
  cat <<END_OF_INFO
Runing all install scripts.

NOTE: More than 1 GB of data will be downloaded and installed. This may take several hours, depending on your connection speed.

You may check progress of individual scripts by inspecting log files from a second shell:
  tail -f $LogFolder


END_OF_INFO

  if [ -d extensions/ ]; then
    rm -f -R extensions/*
  fi
  echo -n "clearing log-files"
  for Script in $Files; do #{ create all log-files in advance (allows to tail them from start)
    echo "" >Logs/$Script.log 
    echo "" >Logs/$Script.err
    echo -n "."
  done #}
  echo "OK"

  cp ../Version scripts/
  createLink $NewToolChain $HOME/Source/TheToolChain.InstallAll # link is required during installation
  FailedScript=""
  for Script in $Files; do #{ run all install scripts in order
    if [ ! -e ErrorOccured ]; then
      Error=""
      cd "$InstallAllDir"
      $0 $Script "$TargetUser" "$TargetGroup" || Error="1"
      if [ -d Documentation ]; then
        Error="Documentation/ folder inside '`pwd`'. Check script!"
      fi
      if [ "$Error" != "" ]; then #{ ERROR + exit 6
        echo ""
        echo "ERROR occured during $Script ($Error)!"
        echo ""
        echo "You may rerun the failing script like this:"
        echo "  cd \"`pwd`\""
        echo "  ./$Script"
        echo ""
        FailedScript="$Script"
        export FailedScript

        exit 6
      fi #}
    fi
  done #}
  cd "$InstallAllDir"
  #} run all install scripts
  
  cd scripts
  for TTC_Script in `ls ttc_*`; do
    createLink ../scripts/$TTC_Script $InstallAllDir/bin/$TTC_Script
  done
  cd ..

  #{ remove temporary files
  #X chmod +x cleanup.sh
  #X ./cleanup.sh
  #}remove temporary files
  
  #{ installation finished successfully
  
  cd $HOME/Source
  createLink $NewToolChain $HOME/Source/TheToolChain # enable this ToolChain as current version
  rm -f $HOME/Source/TheToolChain.InstallAll                           # not used anymore
  applyTargetUserAndGroup TheToolChain
  applyTargetUserAndGroup $NewToolChain

  cd "$InstallAllDir"
  echo "" >OK.AllInstalls


  addLine scripts/SourceMe.sh  '# ensure that $PATH contains no . (brings find into trouble)'
  AlreadyPresent=`grep "export PATH=" scripts/SourceMe.sh | grep split | grep join`
  if [ "$AlreadyPresent" == "" ]; then
  cat <<'END_OF_TEXT' >>scripts/SourceMe.sh
export PATH=`perl -e "my @A=split(':', '\$PATH');  print join(':', grep { \\\$_ ne '.' } @A );"`
END_OF_TEXT
  fi
  
  FinalDestination="$HOME/Source/$NewToolChain"
  if [ "`pwd | grep TheToolChain.install`" != "" ]; then #{ installation finished inside .install folder: move new ToolChain to its final position
    cd ../../
    echo "moving The ToolChain to its final destination..."
    if [ -d "$FinalDestination" ]; then
      echo "deleting old ToolChain at final destination $FinalDestination"
      sudo rm -Rf "$FinalDestination"
    fi
    mv -v "$NewToolChain" "$HOME/Source/"
    cd
    rm -Rf "$HOME/Source/TheToolChain.install"
  fi #}
  cd "$FinalDestination/InstallData"
  
  OldDir="`pwd`"
  addLine scripts/SourceMe.sh "alias commit='git pull && git commit' #$0"
  addLine scripts/SourceMe.sh "alias comment='replace comment' #$0"
  addLine scripts/SourceMe.sh "export PATH=\"./bin/:./extensions.local/:\$HOME/Source/TheToolChain/InstallData/extensions/:\$HOME/Source/TheToolChain/InstallData/bin/:\$PATH\" #$0"
  source scripts/SourceMe.sh
  cd ../Template
  cp ../Version required_version
  ./createLinks.sh
  cd "$OldDir"
  cd ..
  addLine $HOME/.bashrc "source $HOME/Source/TheToolChain/InstallData/scripts/SourceMe.sh"

  applyTargetUserAndGroup .
  
  cd ..
  echo ""
  echo "All tools have been installed successfully."
  echo "Note: Your .bashrc has been updated."
  echo ""
  echo "Next steps:"
  echo "  (1) Connect JTAG programmer (e.g. ARM-USB-TINY-H) to your computer"
  echo "  (2) Connect your ARM prototype board (e.g. STM32-P107) to JTAG programmer"
  echo "  (3) Start a new shell!"
  echo "  (4) Copy into Shell:"
  echo "      cd '$FinalDestination'"
  echo "      ./createNewProject.pl Test ItsMe"
  
  KernelRelease=`uname -r`
  KernelUnchanged=`ls /boot/vmlinuz* | sort | tail -n1 | grep "$KernelRelease"`
  if [ "$KernelUnchanged" == "" ]; then
    echo ""
    echo "Note: Kernel has been upgraded. Reboot your computer before proceeding to above steps!"
  fi
  echo ""
  echo "  Have fun! :-)"
  
  #} installation finished successfully
#} run all install-files
else                                 #{ run single install-file
  Script="$1"

  FreeSpace=`df -k . |  awk '{ print $4 }' | tail -n1`
  if [ "$FreeSpace" \< 10000 ]; then
    echo "$0 - ERROR: Disk space less than 10MB! Aborting installation at `pwd`/${Script}."
    exit 30
  else
    FreeSpace=`df -h . |  awk '{ print $4 }' | tail -n1`
    echo -n "running `pwd`/$Script ($FreeSpace free).."
  fi
  dir Logs
  extractFilename $Script
  sudo date >/dev/null # renew sudo password to avoid loosing it
  Seconds_Start=$SECONDS
  ./$Script "$TargetUser" "$TargetGroup" >>Logs/$FileName.log 2>&1 && OK="1"
  Seconds_End=$SECONDS
  Length=$(( $Seconds_End - $Seconds_Start ))
  echo -n "${Length}s "
  echo "install script $Script finished after ${Length} seconds." >>Logs/$FileName.log
  
  if [ "$OK" == "1" ]; then
    echo "OK"
  else
    echo "ERROR"
    echo ""
    echo "Check logfiles:"
    find `pwd`/Logs/ -name *.log -exec echo "less {}" \; | sort
    touch ErrorOccured
    exit 5
  fi
fi
#}run single install-file
