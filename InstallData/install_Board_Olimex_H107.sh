#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="100"
EXTENSION_SHORT="olimex_h107"
EXTENSION_PREFIX="board"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://olimex.com/dev/pdf/ARM/ST/ STM32-H107.pdf Board_STM32-H107.pdf Boards/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

  Dir=`pwd`
  echo "installing in $Install_Dir ..."

  Name="${Install_Dir}" #{
  createExtensionMakefileHead ${Name} #{
  File="${Dir_Extensions}makefile.${Name}" 
  cat <<END_OF_MAKEFILE >>$File 

  COMPILE_OPTS += -DTTC_BOARD=$Name
  BOARD=$Name

  # Define exactly one CPU variant used on current board
  # See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
  TTC_CPU_VARIANT_stm32f107vct6 = 1 

  # system clock setup (-> ttc_sysclock_types.h)
  COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=25000000
  COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL=32768

  #{ single port pins
  # reference: http://olimex.com/dev/pdf/ARM/ST/STM32-H107-schematic.pdf

  COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c6
  COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_c7
  
  COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_a0    # press button switch 1 (named WKUP)
  COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=0

  #}port pins
  #{ parallel ports (8 or 16 consecutive pins in same bank)
  #
  # Every 8 available, consecutive bits in same GPIO bank can form an 8-bit wide port.
  # A 16-bit wide port requires 16 available, consecutive bits (obviously).
  # A pin is available if it not driven externally or used for something else.
  # Check circuit of your board for details.
  
  # 16-bit wide parallel port #1 using whole bank E (16-bit always starts a pin 0)
  COMPILE_OPTS += -DTTC_GPIO_PARALLEL16_1_BANK=TTC_GPIO_BANK_E
  
  # 8-bit wide parallel port #1 starting at pin 0 of bank E
  COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_BANK=TTC_GPIO_BANK_E
  COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_FIRSTPIN=0

  # 8-bit wide parallel port #2 starting at pin 8 of bank E
  COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_BANK=TTC_GPIO_BANK_E
  COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_FIRSTPIN=8
  
  # 8-bit wide parallel port #3 starting at pin 0 of bank A
  COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_3_BANK=TTC_GPIO_BANK_A
  COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_3_FIRSTPIN=0
  
  # 8-bit wide parallel port #4 starting at pin 5 of bank B
  COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_4_BANK=TTC_GPIO_BANK_B
  COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_4_FIRSTPIN=5
  
  # 8-bit wide parallel port #5 starting at pin 8 of bank D
  COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_5_BANK=TTC_GPIO_BANK_D
  COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_5_FIRSTPIN=8
  
  
  #}parallel ports
  #{ USART1
  # Note: Default layout of USART1 is occupied by USB OTG
  COMPILE_OPTS += -DTTC_USART1=INDEX_USART1
  COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_b6
  COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_b7
  #}USART1
  #{ USART2
  COMPILE_OPTS += -DTTC_USART2=INDEX_USART2
  COMPILE_OPTS += -DTTC_USART2_TX=E_ttc_gpio_pin_a2
  COMPILE_OPTS += -DTTC_USART2_RX=E_ttc_gpio_pin_a3
  COMPILE_OPTS += -DTTC_USART2_CK=E_ttc_gpio_pin_a4
  COMPILE_OPTS += -DTTC_USART2_RTS=E_ttc_gpio_pin_a1
  # COMPILE_OPTS += -DTTC_USART2_CTS=E_ttc_gpio_pin_b13  # Note: pin used for WKUP-button
  #}USART2
  #{ USART3
  COMPILE_OPTS += -DTTC_USART3=INDEX_USART3
  COMPILE_OPTS += -DTTC_USART3_TX=E_ttc_gpio_pin_b10
  COMPILE_OPTS += -DTTC_USART3_RX=E_ttc_gpio_pin_b11
  COMPILE_OPTS += -DTTC_USART3_CK=E_ttc_gpio_pin_b12
  COMPILE_OPTS += -DTTC_USART3_CTS=E_ttc_gpio_pin_b13
  COMPILE_OPTS += -DTTC_USART3_RTS=E_ttc_gpio_pin_b14
  #}USART3
  #{ USART4
  COMPILE_OPTS += -DTTC_USART4=INDEX_USART4
  COMPILE_OPTS += -DTTC_USART4_TX=E_ttc_gpio_pin_c10
  COMPILE_OPTS += -DTTC_USART4_RX=E_ttc_gpio_pin_c11
  #}USART4
  #{ USART5
  COMPILE_OPTS += -DTTC_USART5=INDEX_USART5
  COMPILE_OPTS += -DTTC_USART5_TX=E_ttc_gpio_pin_c12
  COMPILE_OPTS += -DTTC_USART5_RX=E_ttc_gpio_pin_d2
  #}USART5
  #{ SPI1
  COMPILE_OPTS += -DTTC_SPI1=ttc_device_1          # 
  COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a7  # EXT2-25
  COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a6  # EXT2-26 
  COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_a5   # EXT2-27
  COMPILE_OPTS += -DTTC_SPI1_NSS=E_ttc_gpio_pin_a4   # EXT2-28
  #}SPI1
  #{ SPI2
  COMPILE_OPTS += -DTTC_SPI2=ttc_device_2          # 
  COMPILE_OPTS += -DTTC_SPI2_MOSI=E_ttc_gpio_pin_b15 # EXT1-27
  COMPILE_OPTS += -DTTC_SPI2_MISO=E_ttc_gpio_pin_b14 # EXT1-26
  COMPILE_OPTS += -DTTC_SPI2_SCK=E_ttc_gpio_pin_b13  # EXT1-25
  COMPILE_OPTS += -DTTC_SPI2_NSS=E_ttc_gpio_pin_b12  # EXT1-24
  #}SPI2
  #{ I2C1 - define inter-integrated circuit interface #1
    COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
    COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b7    # EXT1-19
    COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b6    # EXT1-18
    COMPILE_OPTS += -DTTC_I2C1_SMBAL=E_ttc_gpio_pin_b5  # EXT1-17
  #}I2C1
  #{ I2C2 - define inter-integrated circuit interface #2
    COMPILE_OPTS += -DTTC_I2C2=ttc_device_2           # internal device being used for this port
    COMPILE_OPTS += -DTTC_I2C2_SDA=E_ttc_gpio_pin_b11   # EXT1-23
    COMPILE_OPTS += -DTTC_I2C2_SCL=E_ttc_gpio_pin_b10   # EXT1-22
    COMPILE_OPTS += -DTTC_I2C2_SMBAL=E_ttc_gpio_pin_b12 # EXT1-24
  #}I2C2
  #{ USB1 
  COMPILE_OPTS += -DTTC_USB1=INDEX_USB1
  COMPILE_OPTS += -DTTC_USB1_VBUS=E_ttc_gpio_pin_a9
  COMPILE_OPTS += -DTTC_USB1_ID=E_ttc_gpio_pin_a10
  COMPILE_OPTS += -DTTC_USB1_DM=E_ttc_gpio_pin_a11
  COMPILE_OPTS += -DTTC_USB1_DP=E_ttc_gpio_pin_a12  
  #}USB1
  #{ define Controller Area Network (CAN) devices
  COMPILE_OPTS += -DTTC_CAN1=0               # physical device index
  COMPILE_OPTS += -DTTC_CAN1_PIN_RX=E_ttc_gpio_pin_b8  # pin used for receive
  COMPILE_OPTS += -DTTC_CAN1_PIN_TX=E_ttc_gpio_pin_b9  # pin used for transmit
  COMPILE_OPTS += -DTTC_CAN2=1               # physical device index
  COMPILE_OPTS += -DTTC_CAN2_PIN_RX=E_ttc_gpio_pin_b12 # pin used for receive
  COMPILE_OPTS += -DTTC_CAN2_PIN_TX=E_ttc_gpio_pin_b13 # pin used for transmit
  #}CAN

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Protoboard H107 from Olimex -> http://olimex.com/dev/stm32-h107.html"  #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
  
  enableFeature 450_cpu_stm32f1xx

END_OF_ACTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

#}
  echo "Installed successfully: $Install_Dir"

cd ..
rmdir 2>/dev/null $Install_Dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
