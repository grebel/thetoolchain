#!/bin/bash

#
#  Install script for Microcontroller STM32 F1xx 
#  with ARM Cortex M3 core + Std Peripherals Library for STM32F1xx.
#
#  Script written by Gregor Rebel 2012
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "200_cpu_stm32f2xx" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
  if [ ! -e OK.Install ];       then #{
    if [ ! -e OK.Packages ]; then #{
      touch OK.Packages
    fi #}
    if [ -e OK.Packages ]; then #{
      Error=""
      
      Archive="stm32f2xx_stdperiph_lib.zip"
      # getFile http://www.st.com/stonline/products/support/micro/files/ $Archive
      getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ $Archive

      if [ ! -e $Archive ]; then
        Error="Cannot download $Archive!"
      fi
      unZIP STM32F10x_StdPeriph_Lib $Archive || mv -f $Archive ${Archive}_bad
      rm current 2>/dev/null

      # find name of only directory
      LibraryDir=`find ./  -maxdepth 1 -mindepth 1 -type d -print`
      createLink $LibraryDir current
      cd current

      #X dir ../../../Documentation/STM
      #X mv *.chm ../../../Documentation/STM/
      for File in `ls *.chm`; do
        addDocumentationFile "$File" uC/STM32F2xx
      done

      cd ..
    
      if [ "$Error" == "" ]; then
        touch OK.Install
      else
        echo "$0 - ERROR: $ERROR!"
        exit 10
      fi
    fi #}
  fi #}
  if [   -e OK.Install ];       then #{
    Architecture="stm32f2xx"
    Name="${Install_Dir}"
    LibPrefix="250_stm_std_peripherals_"
    LibName="${LibPrefix}f2xx"
    createExtensionMakefileHead ${Name} #{          create makefile for cpu
    File="${Dir_Extensions}makefile.${Name}" 
    cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DEXTENSION_${Name}=1

ARM_CPU     =arm7tdmi
TARGET      =-mcpu=cortex-m3 -mthumb
TARGET_ARCHITECTURE_STM32F1xx=1
TARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_STM32F1xx

# architecture specific support code
INCLUDE_DIRS += -Ittc-lib/stm32/
vpath %.c ttc-lib/stm32/ 

# define linker script to use
LDFLAGS += -Tconfigs/memory_stm32f1xx.ld 

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} $0 "CPU STM32F2xx" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.200_cpu_*

activate.190_cpu_cortexm3.sh QUIET \"\$0\"

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# activate corresponding std_peripheral_library
activate.250_CPAL_STM32F2xx_StdPeriph_Driver.sh QUIET

# create link to target script being required by openocd to initialize target cpu
cd \$Dir_Configs
createLink ../additionals/999_open_ocd/target/stm32f2x.cfg openocd_target.cfg 
cd ..
# create link to target script being required by openocd to initialize target cpu
cd \$Dir_Configs
createLink ../additionals/999_open_ocd/target/stm32f2x.cfg openocd_target.cfg
cd ..


END_OF_ACTIVATE
#}
    createActivateScriptTail $Name ${Dir_Extensions}
    #}

    if [ "0" == "1" ]; then #{ DISABLED: std_peripheral_library_f2xx
    createExtensionMakefileHead ${LibName} #{       create makefile for std_peripheral_library
    File="${Dir_Extensions}makefile.${LibName}" 
    cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DEXTENSION_250_stm_std_peripherals=1

INCLUDE_DIRS += -I additionals/250_stm_std_peripherals/STM32F10x_StdPeriph_Driver/inc/ \\
                -I additionals/250_stm_std_peripherals/CMSIS/CM3/DeviceSupport/ST/STM32F2xx/ \\
                -I additionals/250_stm_std_peripherals/CMSIS/CM3/CoreSupport/

vpath %.c additionals/250_stm_std_peripherals/STM32F2xx_StdPeriph_Driver/src/ \\
          additionals/250_stm_std_peripherals/CMSIS/CM3/DeviceSupport/ST/STM32F2xx/

vpath %.s additionals/250_stm_std_peripherals/CMSIS/CM3/DeviceSupport/ST/STM32F2xx/startup/gcc_ride7

END_OF_MAKEFILE
    createExtensionMakefileTail ${LibName} #}
    createActivateScriptHead $LibName ${Dir_Extensions} $0 "Standard Peripheral Library for CPU STM32F2xx" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${LibPrefix}*

OldPWD="\`pwd\`"
cd "\${Dir_Extensions}/"
rm 2>/dev/null makefile.${LibPrefix}_*
rm 2>/dev/null activate.${LibPrefix}_*
ln -sv $Architecture/* .
cd "\$OldPWD"

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$LibName \${Dir_ExtensionsActive}/makefile.$LibName '' QUIET

# we must create a new link in additionals
if [ -d "\$Dir_Additionals" ]; then
  cd "\$Dir_Additionals"
  rm 2>/dev/null 250_stm_std_peripherals
  createLink 250_STM32F2xx_StdPeriph_Driver  \$Dir_Additionals/250_stm_std_peripherals
  cd ..
else
  echo "\$0 - ERROR: Cannot find directory additionals/ (\`pwd\`/\$Dir_Additionals)!"
fi

END_OF_ACTIVATE
#}
    createActivateScriptTail $LibName ${Dir_Extensions}
    #}

    BaseName=$LibName
    dir ${Dir_Extensions}$Architecture

    # create one activate-script per feature
    for Feature in `find current/Libraries/STM32F2xx_StdPeriph_Driver/src/ -name "${Architecture}_*" -execdir echo -n "{} " \;` ; do #{
      Feature=`perl -e "print substr('$Feature', 12, -2);"`

      FeatureName="${LibPrefix}_${Feature}"
      #echo "FeatureName='$FeatureName'" #D
      createExtensionMakefileHead ${FeatureName} ${Dir_Extensions}${Architecture}/ #{
      cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # set by createExtensionMakefileHead() 

MAIN_OBJS += ${Architecture}_${Feature}.o

END_OF_MAKEFILE
      createExtensionMakefileTail ${FeatureName} ${Dir_Extensions}${Architecture}/ #}
      createActivateScriptHead $FeatureName ${Dir_Extensions}${Architecture}/ $0 "append C-source from standar peripheral library for compilation: ${FeatureName}.c" #{
      cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# add createL.inks for each link required for this extension
createLink ${Dir_Extensions}makefile.$FeatureName \${Dir_ExtensionsActive}/makefile.$FeatureName '' QUIET

END_OF_ACTIVATE
#}
      createActivateScriptTail $FeatureName ${Dir_Extensions}
      #}
    done #}
    fi #}std_peripheral_library_f2xx
    
    echo "Installed successfully: $Install_Dir"
  fi #}
  cd ..
  rmdir 2>/dev/null $Install_Dir  # removes only empty install dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
