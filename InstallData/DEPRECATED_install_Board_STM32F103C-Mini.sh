#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2017.
#  modified by Maik Brueggemann and Markus Gettrup 2014

#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Kubuntu 14.04 x64


source scripts/installFuncs.sh
checkUserRoot "$1" "$2"
                          
RANK="100"
EXTENSION_SHORT="stm32f103c_mini"
EXTENSION_PREFIX="board"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

OldDir=`pwd`
  if [ ! -e OK.Documentation ]; then #{
  
    Get_Fails=""

    # no datasheet found
    #getFile http://olimex.com/dev/pdf/ARM/ST/ STM32-P107.pdf Board_STM32-P107.pdf
    #getFile http://pdf1.alldatasheet.com/datasheet-pdf/view/162452/STMICROELECTRONICS/STE101P/+0W3W7WVPSLpETSpPKIbaZZTOfqgjUppKk+/ datasheet.pdf Board_STM32-P107_EthernetPhy_STE101P.pdf
    getFile http://thetoolchain.com/mirror/ STM32F103C_Board.rar STM32F103C_Board.rar
    TargetDir="STM32F103C-Mini"
    if [ ! -d "$TargetDir" ]; then
      unRAR "STM32F103Cdevelopment board" STM32F103C_Board.rar
      mv "STM32F103Cdevelopment board" "$TargetDir"
      rm -Rf "$TargetDir/relative material" # remove unrelated stuff
    fi
    addDocumentationFolder $TargetDir $TargetDir Boards
  
    if [ "$Get_Fails" == "" ]; then
      touch OK.Documentation
    else
      echo "$ScriptName - ERROR: missing files: $Get_Fails"
    fi
  fi #}

  Name="${Install_Dir}" #{
  createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "../ttc-lib/ttc_sysclock.h"

// set clocking profile to maximum speed
ttc_sysclock_profile_switch(tsp_MaxSpeed);
END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name} #{
  File="${Dir_Extensions}makefile.${Name}" 
  cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DTTC_BOARD=$Name
BOARD=$Name

# define microcontroller used on board
#X STM32F10X_MD  =1
#X COMPILE_OPTS += -DSTM32F10X_MD

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f103rbt6 = 1

#{ USART
  COMPILE_OPTS += -DTTC_USART1=INDEX_USART1 # USART connected to 24-pin male header connector
  COMPILE_OPTS += -DTTC_USART1_TX=tgp_a9  
  COMPILE_OPTS += -DTTC_USART1_RX=tgp_10   
  COMPILE_OPTS += -DTTC_USART1_RTS=tgp_a12  
  COMPILE_OPTS += -DTTC_USART1_CTS=tgp_a11  

  COMPILE_OPTS += -DTTC_USART2=INDEX_USART2 # USART connected to 24-pin male header connector
  COMPILE_OPTS += -DTTC_USART2_TX=tgp_a2  
  COMPILE_OPTS += -DTTC_USART2_RX=tgp_a3   
  COMPILE_OPTS += -DTTC_USART2_RTS=tgp_a1  
  COMPILE_OPTS += -DTTC_USART2_CTS=tgp_a0  

  COMPILE_OPTS += -DTTC_USART3=INDEX_USART3 # USART connected to 24-pin male header connector
  COMPILE_OPTS += -DTTC_USART3_TX=tgp_b10   
  COMPILE_OPTS += -DTTC_USART3_RX=tgp_b11   
  COMPILE_OPTS += -DTTC_USART3_RTS=tgp_b14  
  COMPILE_OPTS += -DTTC_USART3_CTS=tgp_b13  
#}USART


#{ define Controller Area Network (CAN) devices
  COMPILE_OPTS += -DTTC_CAN1=0               # physical device index
  COMPILE_OPTS += -DTTC_CAN1_PIN_RX=tgp_a11  # pin used for receive
  COMPILE_OPTS += -DTTC_CAN1_PIN_TX=tgp_a12  # pin used for transmit
#}CAN

#{ parallel ports (8 or 16 consecutive pins in same bank)
  #
  # Every 8 available, consecutive bits in same GPIO bank can form an 8-bit wide port.
  # A 16-bit wide port requires 16 available, consecutive bits (obviously).
  # A pin is available if it not driven externally or used for something else.
  # Check circuit of your board for details.

  # 8-bit wide parallel port #1 starting at pin 0 of bank E
  COMPILE_OPTS += -DTTC_PARALLEL08_1_BANK=TTC_GPIO_BANK_E
  COMPILE_OPTS += -DTTC_PARALLEL08_1_FIRSTPIN=0

  # 8-bit wide parallel port #2 starting at pin 8 of bank D (overlaps with USART2, USART3)
  COMPILE_OPTS += -DTTC_PARALLEL08_2_BANK=TTC_GPIO_BANK_D
  COMPILE_OPTS += -DTTC_PARALLEL08_2_FIRSTPIN=8
#}parallel ports

#{ SPI
  COMPILE_OPTS += -DTTC_SPI1=ttc_device_1      
  COMPILE_OPTS += -DTTC_SPI1_MOSI=tgp_a7 
  COMPILE_OPTS += -DTTC_SPI1_MISO=tgp_a6 
  COMPILE_OPTS += -DTTC_SPI1_SCK=tgp_a5  
  COMPILE_OPTS += -DTTC_SPI1_NSS=tgp_a4  

  COMPILE_OPTS += -DTTC_SPI2=ttc_device_2      
  COMPILE_OPTS += -DTTC_SPI2_MOSI=tgp_b15 
  COMPILE_OPTS += -DTTC_SPI2_MISO=tgp_b14 
  COMPILE_OPTS += -DTTC_SPI2_SCK=tgp_b13  
  COMPILE_OPTS += -DTTC_SPI2_NSS=tgp_b12
#}SPI


#{ I2C - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           
  COMPILE_OPTS += -DTTC_I2C1_SDA=tgp_b7     
  COMPILE_OPTS += -DTTC_I2C1_SCL=tgp_b6    
  COMPILE_OPTS += -DTTC_I2C1_SMBAL=tgp_b5 
  
  COMPILE_OPTS += -DTTC_I2C2=ttc_device_2           
  COMPILE_OPTS += -DTTC_I2C2_SDA=tgp_b11     
  COMPILE_OPTS += -DTTC_I2C2_SCL=tgp_b10    
  COMPILE_OPTS += -DTTC_I2C2_SMBAL=tgp_b12
#}I2C

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  File="${Dir_Extensions}activate.${Name}.sh" #{
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "STM32f103C Mini -> http://www.lctech-inc.com/Hardware/Detail.aspx?id=0172e854-77b0-43d5-b300-68e570c914fd"
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
  
  #X activate.200_cpu_stm32f10x.sh QUIET "\$ScriptName"
  
  # ACTIVATE_SECTION_C activate initialization source-code for this extension 
  createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

  enableFeature 450_cpu_stm32f1xx

END_OF_ACTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

#}
  echo "Installed successfully: $Install_Dir"

cd ..
rmdir 2>/dev/null $Install_Dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
