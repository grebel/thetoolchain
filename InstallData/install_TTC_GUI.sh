#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Initial Script written by Gregor Rebel 2010-2018.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="500"
EXTENSION_SHORT="gui"
EXTENSION_PREFIX="ttc"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ download + install software
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe 'which autoreconf' autoreconf
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir ..."
    Install_Path=`pwd`
    
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts
  Name="${Install_Dir}"
  
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 
  
# Every extra folder containing header files must be added to INCLUDE_DIRS
# E.g.: INCLUDE_DIRS += -I cool_library/includes/
INCLUDE_DIRS += -I ttc-lib/gfx  


# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# additional directories to search for C-Sources
#vpath %.c 
vpath %.c ttc-lib/gfx  

# additional object files to be compiled
MAIN_OBJS += ttc_gui.o

# we have to define at least one font to make ttc_font.h happy
COMPILE_OPTS += -DTTC_FONT1=font_type1_16x24

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "architecture independent support for gui devices (ToDo)" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_Additionals/$Install_Dir_*

activate.510_ttc_font__font_type1_16x24.sh QUIET "\$ScriptName"
activate.500_ttc_memory.sh QUIET "\$ScriptName"
activate.500_ttc_gfx.sh  QUIET "\$ScriptName"

if [ -e extensions.active/makefile.500_ttc_gfx ]; then # ttc_gfx is available: activate ttc_gui
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
fi
END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  #? cd "$Install_Path"
  echo "Installed successfully: $Install_Dir"
  
  Name="600_example_gui"
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "example_gui.h"
example_gui_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

INCLUDE_DIRS += -I examples/
vpath %.c examples/

MAIN_OBJS += DEPRECATED_example_gui.o

# we have to define at least one font to make ttc_font.h happy
COMPILE_OPTS += -DTTC_FONT1=font_type1_16x24

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Example of how to use touch-input to use buttons" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.600_example_*

activate.500_ttc_gui.sh                    QUIET "\$ScriptName"
activate.500_ttc_adc.sh                    QUIET "\$ScriptName"
activate.500_ttc_input.sh                  QUIET "\$ScriptName"
activate.500_ttc_gpio.sh                   QUIET "\$ScriptName"
activate.500_ttc_string.sh                 QUIET "\$ScriptName"
activate.510_ttc_font__font_type1_16x24.sh QUIET "\$ScriptName"
activate.500_ttc_gfx.sh                    QUIET "\$ScriptName"

# create links into extensions.active/     QUIET "\$ScriptName"
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
  echo "Installed successfully: $Name"
  
#}
else                            #{ Install failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
