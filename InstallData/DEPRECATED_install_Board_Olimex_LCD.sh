#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2017.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="100"
EXTENSION_SHORT="olimex_lcd"
EXTENSION_PREFIX="board"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://olimex.com/dev/pdf/ARM/ST/                           STM32-LCD.pdf                   Board_STM32-LCD.pdf          Boards/Olimex_STM32-LCD
  getDocumentation http://www.olimex.com/dev/pdf/PIC/                          "FS-K320QVB-V1-01-3T%20(3).pdf" LCD_FS-K320QVB-V1-01-3T.pdf  Boards/Olimex_STM32-LCD
  getDocumentation https://www.olimex.com/Products/ARM/ST/STM32-LCD/resources/ ILI9325_new_displays.pdf        ILI9325_new_displays.pdf     Boards/Olimex_STM32-LCD
  getDocumentation http://olimex.com/dev/                                      stm32-lcd.html                  stm32-lcd.html               Boards/Olimex_STM32-LCD
  getDocumentation https://www.olimex.com/Products/ARM/ST/STM32-LCD/resources/ STM32-LCD_demo.zip              STM32-LCD_demo.zip           Boards/Olimex_STM32-LCD
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

Dir=`pwd`

  Driver1="450_ttc_gfx_driver_$Install_Dir"             #{ gfx driver for this board
  createExtensionMakefileHead ${Driver1} #{
  File="${Dir_Extensions}makefile.${Driver1}" 
  cat <<END_OF_MAKEFILE >>$File 

# ttc_gfx driver settings for board Olimex-LCD
# reference: http://olimex.com/dev/ARM/ST/STM32-LCD/STM32-LCD-schematic.pdf

# LCD-Display 320x240x24 - define settings required by low-level driver to run on current board
COMPILE_OPTS += -DTTC_GFX1=1
COMPILE_OPTS += -DTTC_GFX1_WIDTH=240
COMPILE_OPTS += -DTTC_GFX1_HEIGHT=320
COMPILE_OPTS += -DTTC_GFX1_DEPTH=24
COMPILE_OPTS += -DTTC_GFX1_DRIVER=tgd_ILI9320    # one from ttc_gfx_types.h:e_ttc_gfx_driver
COMPILE_OPTS += -DTTC_GFX_STM32_FSMC=1

# define gpio pins connected to ili9320 gfx controller (->lcd_ili9320.h)
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_CHIPSELECT=E_ttc_gpio_pin_d7
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_WRITE_STROBE=E_ttc_gpio_pin_d5
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_REGISTER=E_ttc_gpio_pin_e3
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_RESET=E_ttc_gpio_pin_e2
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_LCDLED=E_ttc_gpio_pin_d13
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_NBL0=E_ttc_gpio_pin_e0
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_NBL1=E_ttc_gpio_pin_e1
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_READ_STROBE=E_ttc_gpio_pin_d4

END_OF_MAKEFILE
  createExtensionMakefileTail ${Driver1} #}
  createActivateScriptHead $Driver1 ${Dir_Extensions} $ScriptName "gfx driver for Protoboard LCD from Olimex -> http://olimex.com/dev/stm32-lcd.html"
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  #rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*

  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Driver1 \${Dir_ExtensionsActive}/makefile.$Driver1 '' QUIET

  activate.450_ttc_gfx_driver_100_board_olimex_lcd.sh QUIET "\$ScriptName"
  enableFeature 450_cpu_stm32f1xx

END_OF_ACTIVATE
#}
  createActivateScriptTail $Driver1 ${Dir_Extensions}
  #}Driver1
  Driver2="450_ttc_input_driver_$Install_Dir"           #{ touch-handler driver for this board
  createExtensionMakefileHead ${Driver2} #{
  File="${Dir_Extensions}makefile.${Driver2}" 
  cat <<END_OF_MAKEFILE >>$File 

# ttc_input driver settings for board Olimex-LCD
# reference: http://olimex.com/dev/ARM/ST/STM32-LCD/STM32-LCD-schematic.pdf

# Configure TTC_INPUT
COMPILE_OPTS += -DTTC_INPUT1=ta_input_touchpad

# Configure TTC_TOUCHPAD

COMPILE_OPTS += -DTTC_TOUCHPAD1=ta_touchpad_analog4 # one from ttc_touchpad_types.h:e_ttc_touchpad_architecture
COMPILE_OPTS += -DTTC_TOUCHPAD1_WIDTH=240           # scale horizontal touchpad value to this size 
COMPILE_OPTS += -DTTC_TOUCHPAD1_HEIGHT=320          # scale vertical touchpad value to this size
COMPILE_OPTS += -DTTC_TOUCHPAD1_DEPTH=24            # scale touchpad pressure value to this size

# For digital output + analog input pins allow to apply positive or negative voltage in X and Y direction
COMPILE_OPTS += -DTTC_TOUCHPAD1_PIN_XR=E_ttc_gpio_pin_c3
COMPILE_OPTS += -DTTC_TOUCHPAD1_PIN_XL=E_ttc_gpio_pin_c2
COMPILE_OPTS += -DTTC_TOUCHPAD1_PIN_YU=E_ttc_gpio_pin_c1
COMPILE_OPTS += -DTTC_TOUCHPAD1_PIN_YD=E_ttc_gpio_pin_c0

END_OF_MAKEFILE
  createExtensionMakefileTail ${Driver2} #}
  createActivateScriptHead $Driver2 ${Dir_Extensions} $ScriptName "touch-handler driver for Protoboard LCD from Olimex -> http://olimex.com/dev/stm32-lcd.html"
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  #rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Driver2 \${Dir_ExtensionsActive}/makefile.$Driver2 '' QUIET
  
  #activate.400_input_analog.sh QUIET "\$ScriptName"

  # enable features of this board (allows activation of low-level drivers)
  enableFeature 450_touchpad_analog4  
  enableFeature 450_input_touchpad
END_OF_ACTIVATE
#}
  createActivateScriptTail $Driver2 ${Dir_Extensions}
  #}Driver2
  
  if [ "1" == "0" ]; then #{ DEPRECATED drivers
    
  Driver3="450_ttc_accelerometer_driver_$Install_Dir"   #{ accelerometer driver for this board
  createExtensionMakefileHead ${Driver3} #{
  File="${Dir_Extensions}makefile.${Driver3}" 
  cat <<END_OF_MAKEFILE >>$File 

# ttc_input driver settings for board Olimex-LCD
# reference: http://olimex.com/dev/ARM/ST/STM32-LCD/STM32-LCD-schematic.pdf

# Accelerometer - define settings required by low-level driver to run on current board



END_OF_MAKEFILE
  createExtensionMakefileTail ${Driver3} #}
  createActivateScriptHead $Driver3 ${Dir_Extensions} $ScriptName "accelerometer driver for Protoboard LCD from Olimex -> http://olimex.com/dev/stm32-lcd.html"
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  #rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Driver3 \${Dir_ExtensionsActive}/makefile.$Driver3 '' QUIET
  
  activate.400_sensor_lis3lv02dl.sh QUIET "\$ScriptName"

END_OF_ACTIVATE
#}
  createActivateScriptTail $Driver3 ${Dir_Extensions}
  #}Driver3
  
fi #}DEPRECATED drivers
  Name="${Install_Dir}"                          #{ board definitions
  createExtensionMakefileHead ${Name} #{
  File="${Dir_Extensions}makefile.${Name}" 
  cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DTTC_BOARD=$Name
BOARD=$Name

#X STM32F10X_HD  =1
#X COMPILE_OPTS += -DSTM32F10X_HD

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f103zet6 = 1

# system clock setup (-> ttc_sysclock_types.h)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=8000000
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL=32768

# Define port pins
# reference: http://olimex.com/dev/ARM/ST/STM32-LCD/STM32-LCD-schematic.pdf
#{ ADC     - define Analog Input Pins

COMPILE_OPTS += -DTTC_ADC1=E_ttc_gpio_pin_c4 # pin 38 on connector UEXT40
COMPILE_OPTS += -DTTC_ADC2=E_ttc_gpio_pin_c5 # pin 40 on connector UEXT40

#}
#{ parallel ports (8 or 16 consecutive pins in same bank)
#
# Every 8 available, consecutive bits in same GPIO bank can form an 8-bit wide port.
# A 16-bit wide port requires 16 available, consecutive bits (obviously).
# A pin is available if it not driven externally or used for something else.
# Check circuit of your board for details.

# 16-bit wide parallel port #1 using whole bank F (16-bit always starts a pin 0)
COMPILE_OPTS += -DTTC_PARALLEL16_1_BANK=TTC_GPIO_BANK_F

# 16-bit wide parallel port #2 using whole bank G (16-bit always starts a pin 0)
COMPILE_OPTS += -DTTC_PARALLEL16_2_BANK=TTC_GPIO_BANK_G


# 8-bit wide parallel port #1 starting at pin 0 of bank F
COMPILE_OPTS += -DTTC_PARALLEL08_1_BANK=TTC_GPIO_BANK_F
COMPILE_OPTS += -DTTC_PARALLEL08_1_FIRSTPIN=0

# 8-bit wide parallel port #2 starting at pin 8 of bank F
COMPILE_OPTS += -DTTC_PARALLEL08_2_BANK=TTC_GPIO_BANK_F
COMPILE_OPTS += -DTTC_PARALLEL08_2_FIRSTPIN=8

# 8-bit wide parallel port #3 starting at pin 0 of bank G
COMPILE_OPTS += -DTTC_PARALLEL08_3_BANK=TTC_GPIO_BANK_G
COMPILE_OPTS += -DTTC_PARALLEL08_3_FIRSTPIN=0

# 8-bit wide parallel port #4 starting at pin 8 of bank G
COMPILE_OPTS += -DTTC_PARALLEL08_4_BANK=TTC_GPIO_BANK_G
COMPILE_OPTS += -DTTC_PARALLEL08_4_FIRSTPIN=8

#}parallel ports
#{ USART1  - define universal synchronous asynchronous receiver transmitter #1
COMPILE_OPTS += -DTTC_USART1=INDEX_USART1       # RS232 on 10-pin male header connector UEXT1 connected USART
COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_a9   # UEXT1-3, UEXT40-3
COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_a10  # UEXT1-4, UEXT40-4
COMPILE_OPTS += -DTTC_USART1_CK=E_ttc_gpio_pin_a8   # EXT-40
#}USART1
#{ USART2  - define universal synchronous asynchronous receiver transmitter #2
  COMPILE_OPTS += -DTTC_USART2=INDEX_USART2      # RS232 on 10-pin male header connector UEXT2 connected USART
  COMPILE_OPTS += -DTTC_USART2_TX=E_ttc_gpio_pin_a2  # UEXT2-3
  COMPILE_OPTS += -DTTC_USART2_RX=E_ttc_gpio_pin_a3  # UEXT2-4
  COMPILE_OPTS += -DTTC_USART2_CK=E_ttc_gpio_pin_a4  # UEXT1-10
#}USART2
#{ SPI1    - define serial peripheral interface #1
  COMPILE_OPTS += -DTTC_SPI1=ttc_device_1          # 
  COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a7  # UEXT1-8,  UEXT40-8
  COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a6  # UEXT1-7,  UEXT40-7
  COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_a5   # UEXT1-9,  UEXT40-9
  COMPILE_OPTS += -DTTC_SPI1_NSS=E_ttc_gpio_pin_a4   # UEXT1-10, UEXT40-10
#}SPI1
#{ SPI2    - define serial peripheral interface #2
  COMPILE_OPTS += -DTTC_SPI2=ttc_device_2          # 
  COMPILE_OPTS += -DTTC_SPI2_MOSI=E_ttc_gpio_pin_b15 # UEXT1-8
  COMPILE_OPTS += -DTTC_SPI2_MISO=E_ttc_gpio_pin_b14 # UEXT1-7
  COMPILE_OPTS += -DTTC_SPI2_SCK=E_ttc_gpio_pin_b13  # UEXT1-9
  COMPILE_OPTS += -DTTC_SPI2_NSS=E_ttc_gpio_pin_b12  # UEXT2-10
#}SPI2
#{ I2C1    - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_AMOUNT_I2CS=3       #
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b7    # UEXT2-5/ UEXT40-6
  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b6    # UEXT2-6/ UEXT40-5
  COMPILE_OPTS += -DTTC_I2C1_SMBAL=E_ttc_gpio_pin_b5  # EXT-10
#}I2C1
#{ I2C2    - define inter-integrated circuit interface #2
  COMPILE_OPTS += -DTTC_I2C2=ttc_device_2           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C2_SDA=E_ttc_gpio_pin_b11   # connected to accelerometer
  COMPILE_OPTS += -DTTC_I2C2_SCL=E_ttc_gpio_pin_b10   # connected to accelerometer
#}I2C2
#{ CAN1    - define Controller Area Network (CAN) devices
  COMPILE_OPTS += -DTTC_CAN1=0               # physical device index
  COMPILE_OPTS += -DTTC_CAN1_PIN_RX=E_ttc_gpio_pin_b8  # pin used for receive
  COMPILE_OPTS += -DTTC_CAN1_PIN_TX=E_ttc_gpio_pin_b9  # pin used for transmit
#}CAN

#{ define pin configuration of GPIOs on the UEXT1 Conncector
  COMPILE_OPTS += -DUEXT1_USART_TX=E_ttc_gpio_pin_a9
  COMPILE_OPTS += -DUEXT1_USART_RX=E_ttc_gpio_pin_a10
  COMPILE_OPTS += -DUEXT1_I2C_SCL=E_ttc_gpio_pin_b6
  COMPILE_OPTS += -DUEXT1_I2C_SDA=E_ttc_gpio_pin_b7
  COMPILE_OPTS += -DUEXT1_SPI_MISO=E_ttc_gpio_pin_a6
  COMPILE_OPTS += -DUEXT1_SPI_MOSI=E_ttc_gpio_pin_a7
  COMPILE_OPTS += -DUEXT1_SPI_SCK=E_ttc_gpio_pin_a5
  COMPILE_OPTS += -DUEXT1_SPI_NSS=E_ttc_gpio_pin_a4
  
  COMPILE_OPTS += -DUEXT1_SPI_INDEX=1
  COMPILE_OPTS += -DUEXT1_USART_INDEX=1
  COMPILE_OPTS += -DUEXT1_I2C_INDEX=1
#}
#{ define pin configuration of GPIOs on the UEXT2 Conncector
  COMPILE_OPTS += -DUEXT2_USART_TX=E_ttc_gpio_pin_a3
  COMPILE_OPTS += -DUEXT2_USART_RX=E_ttc_gpio_pin_a2
  COMPILE_OPTS += -DUEXT2_I2C_SCL=E_ttc_gpio_pin_b8
  COMPILE_OPTS += -DUEXT2_I2C_SDA=E_ttc_gpio_pin_b9
  COMPILE_OPTS += -DUEXT2_SPI_MISO=E_ttc_gpio_pin_b14
  COMPILE_OPTS += -DUEXT2_SPI_MOSI=E_ttc_gpio_pin_b15
  COMPILE_OPTS += -DUEXT2_SPI_SCK=E_ttc_gpio_pin_a13
  COMPILE_OPTS += -DUEXT2_SPI_NSS=E_ttc_gpio_pin_a12
  COMPILE_OPTS += -DUEXT2_SPI_INDEX=2
  COMPILE_OPTS += -DUEXT2_USART_INDEX=2
  COMPILE_OPTS += -DUEXT2_I2C_INDEX=1
#}
#{ ACCELEROMETER MPU6050
 COMPILE_OPTS += -DTTC_ACCELEROMETER1=ttc_device_1
#}
#{ GYROSCOPE
COMPILE_OPTS += -DTTC_GYROSCOPE1=ttc_device_1
#}
#{ LCD Graphic Display 320x240 using ILI9320 controller

  COMPILE_OPTS += -DGFX_ILI932X_PIN_D00=E_ttc_gpio_pin_d14
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D01=E_ttc_gpio_pin_d15
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D02=E_ttc_gpio_pin_d0
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D03=E_ttc_gpio_pin_d1
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D04=E_ttc_gpio_pin_e7
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D05=E_ttc_gpio_pin_e8
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D06=E_ttc_gpio_pin_e9
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D07=E_ttc_gpio_pin_e10
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D08=E_ttc_gpio_pin_e11
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D09=E_ttc_gpio_pin_e12
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D10=E_ttc_gpio_pin_e13
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D11=E_ttc_gpio_pin_e14
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D12=E_ttc_gpio_pin_e15
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D13=E_ttc_gpio_pin_d8
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D14=E_ttc_gpio_pin_d9
  COMPILE_OPTS += -DGFX_ILI932X_PIN_D15=E_ttc_gpio_pin_d10

#}LCD
END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "touch-handler driver for Protoboard LCD from Olimex -> http://olimex.com/dev/stm32-lcd.html"
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
  
  #X activate.200_cpu_stm32f10x.sh QUIET "\$ScriptName"
  #X activate.400_lcd_320x240_K320QVB.sh QUIET "\$ScriptName"
  #X activate.400_input_analog.sh QUIET "\$ScriptName"
  #X activate.400_sensor_lis3lv02dl.sh QUIET "\$ScriptName"
  
  # activate drivers for board components
  activate.${Driver1}.sh QUIET "\$ScriptName"
  activate.${Driver2}.sh QUIET "\$ScriptName"
#X  activate.${Driver3}.sh QUIET "\$ScriptName"
  activate.450_gfx_ili9320.sh QUIET "\$ScriptName"

  enableFeature 450_cpu_stm32f1xx
  enableFeature 450_gfx_ili9320
  enableFeature 450_gfx_ili9320_fsmc # ToDo: Create extra driver for fsmc operation
  enableFeature 450_accelerometer_lis3lv02dl

END_OF_ACTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions}
  #}board definitions

#}
  echo "Installed successfully: $Install_Dir"

cd ..
rmdir 2>/dev/null $Install_Dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
