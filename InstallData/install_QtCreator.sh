#!/bin/bash
#
#
#  Install script for generic radio support 
#
#  Initial Script written by Gregor Rebel 2010-2018.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# XUbuntu 11.10 x86_64
# KUbuntu 17.10 x86_64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="999"
EXTENSION_SHORT="QtCreator"
EXTENSION_NAME="$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

installPackageSafe "find /usr/lib/ -name libgstreamer-*.so" "libgstreamer0*"
installPackageSafe "find /usr/lib/ -name libgstapp-*.so.0"  "libgstreamer-plugins-base0*"

#X Binary=`which qtcreator`
#X if [ "$Binary" == "" ]; then
#X   Binary=`find /opt/ -name qtcreator 2>/dev/null | grep bin`
#X fi
DirStyles="$HOME/.config/QtProject/qtcreator/styles"
dir $DirStyles
if [ ! -e "$DirStyles/TheToolChain.xml" ]; then #{ installing cool colorset
  echo "installing cool colorset for QtCreator..."
  getFile http://thetoolchain.com/mirror/ QtCreatorStyles.tar.bz2
  untgj QtCreatorStyles QtCreatorStyles.tar.bz2

  if [ -d QtCreatorStyles ]; then
    cd QtCreatorStyles 
    for File in *; do 
      if [ -e $File ]; then
        if [ ! -e $DirStyles/$File ]; then
          cp -v $File $DirStyles/
        fi
      fi
    done
    cd ..
  else
    echo "$ScriptName - ERROR: Downloaded archive seems to be corrupt!"
  fi
fi #}

Binary="`ls $Install_Path/qt-creator-linux-opensource/bin/qtcreator`"
if [ "$Binary" == "" ]; then #{ installing new QtCreator
  echo "Binary qtcreator not found: installing new QtCreator.."
  
  # HowTo provide newer Versions
  #
  # 1) Goto http://qt-project.org/downloads
  #    - download offline installers of QtCreator for x86 and x86_64
  #    - install both into folders (here Version=3.1.1) (Do not launch QtCreator at end of installation!)
  #      cd ~/Source/TheToolChain/InstallData/999_QtCreator/
  #      mkdir qt-creator-linux-x86-${Version}/
  #      mkdir qt-creator-linux-x86_64-${Version}/
  # 2) copy content of newly installed folder into folders created above 
  # 3) create tarballs from both directories
  #    tar cjf qt-creator-linux-x86-opensource-${Version}.tbz    qt-creator-linux-x86-${Version}/
  #    tar cjf qt-creator-linux-x86_64-opensource-${Version}.tbz qt-creator-linux-x86_64-${Version}/
  # 4) Upload both tarballs to thetoolchain.com/mirror/
  
  
  #getFile http://ftp.fau.de/qtproject/development_releases/qtcreator/3.0/3.0.0-beta/ qt-creator-linux-x86-opensource-3.0.0-beta.run
  #getFile http://ftp.fau.de/qtproject/development_releases/qtcreator/3.0/3.0.0-beta/ qt-creator-linux-x86_64-opensource-3.0.0-beta.run
  
  # installers above do not allow automated installation
  getNewestFile http://www.thetoolchain.com/mirror/ "qt-creator-linux-x86-opensource-*"
  getNewestFile http://www.thetoolchain.com/mirror/ "qt-creator-linux-x86_64-opensource-*"
  
  identifyOS
  if [ "$OS_ARCHITECTURE" == "x86_32" ]; then
    #getNewestFile "http://origin.releases.qt-project.org/qtcreator/current_stable/" ".*linux-x86-.*" qt-creator-linux-opensource.bin
    ArchiveCreator="`ls qt-creator-linux-x86-*tbz`"
    #? ArchiveQt="`ls qt-opensource-linux-x86-*tbz`"
  fi
  if [ "$OS_ARCHITECTURE" == "x86_64" ]; then
    #getNewestFile "http://origin.releases.qt-project.org/qtcreator/current_stable/" ".*linux-x86_64-.*" qt-creator-linux-opensource.bin
    ArchiveCreator="`ls qt-creator-linux-x86_64*tbz`"
    #? ArchiveQt="`ls qt-opensource-linux-x86_64-*tbz`"
  fi
  if [ "$ArchiveCreator" == "" ]; then
    echo "$ScriptName - ERROR: Cannot find a stable release of QtCreator for Architecture '$OS_ARCHITECTURE' !"
    exit 10
  else
    #X chmod +x $Binary
    #X ./$Binary --installdir `pwd`
    unTBZ "foo" $ArchiveCreator qt-creator-linux-opensource

    rm -Rf qt-creator-linux-opensource
    find ./ -maxdepth 1 -mindepth 1 -name "qt-creator-linux*" -type d -exec ln -sfv {} qt-creator-linux-opensource \;
    Binary=`ls $Install_Path/qt-creator-linux-opensource/bin/qtcreator`

    if [ "$Binary" != "" ]; then
      
      #? addLine ../scripts/SourceMe.sh "export PATH=\"\$PATH:\$HOME/$Install_Path/\" #$ScriptName"
      #? echo "QtCreator successfully installed in $HOME/$Install_Path/qtcreator"
      echo "QtCreator successfully installed in $Binary"
    else
      echo "$ScriptName - ERROR: Could not install QtCreator (missing file 'qtcreator')"
      exit 10
    fi
  fi
else
  echo "$ScriptName - QtCreator already installed: $Binary"
fi #}

for Plugin in Help QbsProjectManager Android Qnx Ios QmlDesigner QmlJSEditor QmlProfiler QmlProjectManager Valgrind; do #{ Disable some QtCreator Plugins
  File="qt-creator-linux-opensource/lib/qtcreator/plugins/QtProject/${Plugin}.pluginspec"
  if [ -e $File ]; then
    echo "disabling QtCreator plugin $Plugin (may interfere with The ToolChain)"
    mv $File ${File}_DISABLED_TTC
  fi
done #}

if [ ! -e OK.MissingLibraries ]; then #{ install missing libraries
  echo "pwd: $Binary" #D
  find qt-creator-linux-opensource/ -name "*\.so" -exec ldd {} \; | grep 'not found' | awk '{ print $1 }' >missing.files
  ldd $Binary \; | grep 'not found' | awk '{ print $1 }' >>missing.files
  
  rm -Rf MissingFiles
  mkdir MissingFiles
  for Missing in `cat missing.files`; do # remove double entries
    touch MissingFiles/$Missing
  done
  
  # extract library names (everything before first . or -)
  ls MissingFiles/ | sed -e "s|\.| |g"  | awk '{ print $1 }' | sed -e "s|-| |g"  | awk '{ print $1 }' >missing.files
  
  cat <<END_OF_FILE >DoNotInstall.files #{
libAnalyzerBase
libCppTools
libDebugger
libDiffEditor
libgstapp
libgstbase
libgstinterfaces
libgstpbutils
libgstreamer
libgstvideo
libmysqlclient_r
libProjectExplorer
libQmakeProjectManager
libQmlJSEditor
libQmlJSTools
libQtSupport
libRemoteLinux
libResourceEditor
libTextEditor
libVcsBase
END_OF_FILE
#}

  Failed_Packages=""
  for Missing in `cat missing.files`; do
    if [ "`grep $Missing DoNotInstall.files`" == "" ]; then
      echo "trying to install missing library '$Missing'"
      installPackage "${Missing}*"
    fi
  done
  if [ "$Failed_Packages" != "" ]; then
    echo ""
    echo "Failed to install these packages being required by shared object files (*.so):"
    echo "    $Failed_Packages"
  else 
    echo "All required packages installed successfully."
    touch OK.MissingLibraries
  fi
fi #}

if [ "$Binary" != "" ]; then
  createLink "../$Install_Dir/qt-creator-linux-opensource/bin/qtcreator" ../bin/
fi
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd ..

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
