#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a set of High- and Low-Level Drivers
#  for tcpip devices.
#
#  Created from template _install_NN_TTC_DEVICE.sh revision 25 at 20150308 12:00:04 UTC
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

#X StartPath="`pwd`"
RANK="500"
EXTENSION_SHORT="tcpip"
EXTENSION_PREFIX="ttc"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$ScriptName: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$ScriptName - ERROR: Cannot find newest GCC version!"
#   exit 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then       #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe "which git" git
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then       #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ high-level driver for tcpip
    Name="${Install_Dir}"  
  
    SourceName="${Name}" # one may prefix name with different number to define start position in ttc_extensions_active.c
    createExtensionSourcefileHead ${SourceName}    #{ (create initializer code)
    
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

    #include "ttc-lib/ttc_tcpip.h"
    ttc_tcpip_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${SourceName} #}    
    createExtensionMakefileHead ${Name}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
INCLUDE_DIRS += -Ittc-lib/tcpip/

# additional directories to search for C-Sources
vpath %.c ttc-lib/tcpip/

# additional object files to be compiled
MAIN_OBJS += ttc_tcpip.o ttc_tcpip_interface.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $Dir_Extensions "$ScriptName 2" "high-level tcpip Driver" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/
#   \$File_LowLevelInstall  !="": user wants to run only this low level install script 


#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#{ ACTIVATE_SECTION_B call install scripts of low-level drivers
if [ "\$File_LowLevelInstall" != "" ]; then
  \$File_LowLevelInstall QUIET "\$ScriptName"

  # extract driver name from script name
  LowLevelDriver=\`perl -e "print substr('\$File_LowLevelInstall', 9, -3);"\`
  
  #X #DISABLED echo "\$ScriptName - LowLevelDriver='\$LowLevelDriver'"
  DriverFound="1"

else
  for LowLevelActivate in \`ls \$Dir_Extensions/activate.450_tcpip_*\`; do
    
    # try to activate this low-level driver
    \$LowLevelActivate QUIET "\$ScriptName"
    
    # extract driver name from script name
    LowLevelDriver=\`perl -e "my \\\\\$P=rindex('\$LowLevelActivate', 'activate.')+9; print substr('\$LowLevelActivate', \\\\\$P, -3);"\`
    #DISABLED echo "\$ScriptName - LowLevelDriver='\$LowLevelDriver'"
    Makefile="\$Dir_ExtensionsActive/makefile.\$LowLevelDriver"
    if [ -e \$Makefile ]; then
      DriverFound="1"
    fi
  done
fi
#}

if [ "\$DriverFound" != "" ]; then

  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D create link to your initialization source-code for this extension 
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers require a certain file to exist inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.450_ethernet_ste101p -> allows activate.500_ethernet.sh to enable 450_ethernet_ste101p.
  # enableFeature 450_ethernet_ste101p

  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  # activate.500_ttc_memory.sh QUIET "\$ScriptName"
  activate.500_ttc_ethernet.sh QUIET "\$ScriptName"
  #}
else
  echo "\$ScriptName - ERROR: No low-level tcpip Driver found: Skipping activation!"
fi

END_OF_ACTIVATE
    createActivateScriptTail $Name $Dir_Extensions

    #}
  
  fi #}high-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ recommended: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $Dir_Extensions "$ScriptName 2" "PLACE YOUR INFO HERE FOR $ScriptName" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

#}
#  ACTIVATE_SECTION_E regressions do not provide features
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$ScriptName"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $Dir_Extensions
    #}

  fi #}
  if [ "1" == "1" ]; then #{ recommended: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $Dir_Extensions "$ScriptName 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

#}
#  ACTIVATE_SECTION_E examples do not provide features
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$ScriptName"
activate.500_ttc_memory.sh QUIET "\$ScriptName"
activate.500_ttc_gpio.sh   QUIET "\$ScriptName"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $Dir_Extensions
    #}

  fi #}
  if [ "1" == "1" ]; then #{ call install scripts of low-level drivers for tcpip
    cd "$Install_Path"
    cd ../low_level
    LLInstallDir=`pwd`
    LowLevelInstalls=`ls 2>/dev/null install_*_TCPIP_*.sh`
    echo "$LLInstallDir> found LowLevelInstalls: $LowLevelInstalls"
    for LowLevelInstall in $LowLevelInstalls
    do
      cd "$Install_Path"
      echo ""
      echo    "running $LLInstallDir/$LowLevelInstall ...  "
      echo -n "        "
      source $LLInstallDir/$LowLevelInstall
    done
    cd "$Install_Path"
  fi #}low-level driver

  cd "$Install_Path"
  echo "Installed successfully: $Install_Dir"
#}
else                               #{ Install failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path" #CHANGED
applyTargetUserAndGroup "$Install_Dir"

exit 0
