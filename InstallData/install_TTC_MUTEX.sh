#!/bin/bash
#
#
#  Install script for generic inter task communication support 
#
#  Initial Script written by Gregor Rebel 2010-2018.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="500"
EXTENSION_SHORT="mutex"
EXTENSION_PREFIX="ttc"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

  Name="$Install_Dir"
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += ttc_mutex.o

ifdef EXTENSION_300_scheduler_freertos
  
  # activate certain features of FreeRTOS to support queuing
  COMPILE_OPTS += -DconfigUSE_MUTEXES=1
  COMPILE_OPTS += -DconfigUSE_COUNTING_SEMAPHORES=1
endif

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Scheduler independent support for inter task communication via mutexes" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Install_Dir}_*

ArchitectureSupportAvailable=""

# create links into extensions.active/
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

# try to activate at least one low-level driver
Activated=""

if [ "\$Activated" == "" ]; then

  checkFeature "450_cpu_cortexm3"
  if [ "\$?" == "1" ]; then #{
    # check if file already exists to avoid endless loop
    checkFeature "500_ttc_mutex_cortexm3"
    if [ "\$?" == "0" ]; then #{
      activate.500_ttc_mutex_cortexm3.sh QUIET "\$ScriptName"
      Activated="1"
    fi #}
  fi #}
fi
if [ "\$Activated" == "" ]; then
  checkFeature "450_cpu_cortexm3"
  if [ "\$?" == "1" ]; then #{
    # check if file already exists to avoid endless loop
    checkFeature "500_ttc_mutex_cortexm0"
    if [ "\$?" == "0" ]; then #{
      activate.500_ttc_mutex_cortexm0.sh QUIET "\$ScriptName"
      Activated="1"
    fi #}
  fi #}
fi
if [ "\$Activated" == "" ]; then #{
  if [ -e "\${Dir_ExtensionsActive}/makefile.300_scheduler_freertos" ]; then
    # check if file already exists to avoid endless loop
    if [ ! -e "\${Dir_ExtensionsActive}/makefile.500_ttc_mutex_freertos" ]; then
      activate.500_ttc_mutex_freertos.sh QUIET "\$ScriptName"
      Activated="1"
    fi
  fi
fi #}


activate.500_ttc_semaphore.sh QUIET "\$ScriptName"
activate.500_ttc_systick.sh   QUIET "\$ScriptName"

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  Name="${Install_Dir}_freertos"
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# activate certain features of FreeRTOS to support queuing
COMPILE_OPTS += -DconfigUSE_MUTEXES=1
COMPILE_OPTS += -DconfigUSE_COUNTING_SEMAPHORES=1

MAIN_OBJS += freertos_mutex.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "mutex implementation based on FreeRTOS" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Install_Dir}_*


# create links into extensions.active/
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

# check if file already exists to avoid endless loop
if [ ! -e "\${Dir_ExtensionsActive}/makefile.500_ttc_mutex" ]; then
  activate.500_ttc_mutex.sh           QUIET "\$ScriptName"
fi
# check if file already exists to avoid endless loop
if [ ! -e "\${Dir_ExtensionsActive}/makefile.300_scheduler_freertos" ]; then
  activate.300_scheduler_freertos.sh QUIET "\$ScriptName"
fi

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  Name="${Install_Dir}_cortexm3"
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += cm3_mutex.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Mutex low-level driver for CortexM3 architecture" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Install_Dir}_*

#X activate.190_cpu_cortexm3.sh QUIET "\$ScriptName"

# create links into extensions.active/
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

# check if file already exists to avoid endless loop
if [ ! -e "\${Dir_ExtensionsActive}/makefile.500_ttc_mutex" ]; then
  activate.500_ttc_mutex.sh           QUIET "\$ScriptName"
fi

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  Name="${Install_Dir}_cortexm0"
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += cm0_mutex.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Mutex low-level driver for CortexM3 architecture" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Install_Dir}_*

#X activate.190_cpu_cortexm0.sh QUIET "\$ScriptName"

# create links into extensions.active/
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

# check if file already exists to avoid endless loop
if [ ! -e "\${Dir_ExtensionsActive}/makefile.500_ttc_mutex" ]; then
  activate.500_ttc_mutex.sh           QUIET "\$ScriptName"
fi

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
  
  echo "Installed successfully: $Name"

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
