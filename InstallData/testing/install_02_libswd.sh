#!/bin/bash

#{  Install script for libSWD,  an open source library for Serial Wire Debug support written by Tomasz Boleslaw CEDRO
#
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# KUbuntu 12.10 x64
# XUbuntu 12.10 x64
# openSuSe 12.2 (untested but should work)
#}

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

StartPath="`pwd`"
EXTENSION_NAME="990_libswd"
dir "$EXTENSION_NAME"
cd "$EXTENSION_NAME"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

InstallInHomeDir="1"
if [ "$InstallInHomeDir" == "1" ]; then
  Prefix=`pwd`  # install into subfolder of InstallData/ for current user only
else
  findPrefix    # install for all users
fi

Install_Path=`pwd`

DocFolder="../../Documentation/Interfaces"
if [ ! -e OK.Documentation ]; then #{
  Error=""
  dir $DocFolder

  
  if [ "$Error" == "" ]; then
    echo "Documentation complete: $EXTENSION_NAME."
    touch OK.Documentation
  fi
fi #}
if [ ! -e OK.Install ]; then #{
  InstallPrefix=`pwd`

  if [ "$AmountCPUs" == "" ]; then
    AmountCPUs="1"
  fi
  AmountJobs=$(( $AmountCPUs * 2 ))
  
  echo "installing in $EXTENSION_NAME ..."
  
  if [ ! -e OK.Packages ]; then #{
    installPackageSafe "which autoreconf" autoconf autoreconf
    touch OK.Packages
  fi #}

  DirName="libswd"
  
  gitClone git://libswd.git.sourceforge.net/gitroot/libswd/libswd libswd
  if [ ! -d ${DirName} ]; then
    echo "ERROR: Cannot download file (folder ${DirName} not found in `pwd`)!"
    exit 5
  fi
  cd "${DirName}/"
  replaceInFile configure.ac "AM_PROG_AR" "#AM_PROG_AR"
  echo "generating configuration for $DirName .."
  ./autogen.sh >autogen.log 2>&1
  echo "configuring $DirName .."
  Error=""
  ./configure --prefix="$InstallPrefix" >configure.log 2>&1 || Error="1"
  if [ "$Error" == "" ]; then
    echo "compiling $DirName .."
    make -j$AmountJobs install clean >compile.log 2>&1
  fi
  cd ..
  
  if [ -e  lib/libswd.so ]; then
    cd lib
    for Library in `ls *.so*`; do
      Source="$HOME/Source/TheToolChain/InstallData/$EXTENSION_NAME/lib/$Library"
      Dest="/usr/lib/$Library"
      createLink "$Source" "$Dest" root
    done
    cd ..
    
    cp libswd/README $DocFolder/readme.libswd
    echo "" >OK.Install
  else
    echo "Error during compiling ${DirName}!"
    echo "Check log files:"
    find `pwd`/ -name "*.log" -exec echo "  less {}" \;
    find `pwd`/ -name "*.err" -exec echo "  less {}" \;
    exit 10
  fi #}

  
fi #} [ ! -e OK.Install ]; then

if [ -e OK.Install ]; then #{
  #if [ -d libswd ]; then
  #  rm -Rf libswd  # source codes not needed anymore
  #fi
  #addLine ../scripts/createLinks.sh "rm 2>/dev/null $EXTENSION_NAME;  createLinks.sh \$Source/TheToolChain/InstallData/$EXTENSION_NAME/lib  $EXTENSION_NAME"

  echo "Installed successfully: $EXTENSION_NAME"
#}
else
  echo "failed to install $EXTENSION_NAME"
  exit 10
fi

cd ..

#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$StartPath"
applyTargetUserAndGroup "$Install_Dir"

exit 0
