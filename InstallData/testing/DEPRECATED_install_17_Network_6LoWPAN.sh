#!/bin/bash

#
#  Install script for Ethernet module STE101P on Olimex prototype board STM32-P107.
#  URL: http://olimex.com/dev/stm32-p107.html
#
#  Script written by Gregor Rebel 2010-2017.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

StartPath="`pwd`"
Install_Dir="400_network_6lowpan"
dir "$Install_Dir"
cd "$Install_Dir"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

#if [ ! -e OK\.Documentation ]; then #{
  #Get_Fails=""
  #dir ../../Documentation/Network
  #getFile 'http://www.sics.se/~adam/download/?f=' uip-1.0-refman.pdf "TCP-IP_Stack_uIP_Manual.pdf" "../../Documentation/Network/"
  #if [ "$Get_Fails" == "" ]; then
  #  touch OK\.Documentation
  #else
  #  echo "$0 - ERROR: missing files: $Get_Fails"
  #fi
#fi #}
if [ ! -e OK.Install ]; then #{
  echo "installing in $Install_Dir ..."
  Install_Path=`pwd`
  
  InstallInHomeDir="1"
  if [ "$InstallInHomeDir" == "1" ]; then
    Prefix=`pwd`  # install into subfolder of InstallData/ for current user only
  else
    findPrefix    # install for all users
  fi

  DirName="isolated-network-contiki-2.6"
  ZipFile=${DirName}\.zip
  ZipRoute="\$Source/TheToolChain.Contents/"
  #getFile http://olimex.com/dev/soft/arm/ST/STM-P107/ $ZipFile
  
  
  unZIP $ZipRoute $DirName $ZipFile
  #add2CleanUp ../cleanup.sh "rm -Rf $Install_Dir/$File"
  
  if [ ! -d ${DirName} ]; then
    echo "ERROR: Cannot find/download file (folder '${DirName}/' not found in `pwd`)!"
    exit 5
  fi
  
    if [ -d ${DirName} ]; then
    cd "${DirName}"
    LibRoute="${DirName}/adaptLayer/inc"
    File="net/"
    if [ ! -e "${File}_orig" ]; then #{
        makeOrig ${File}
        echo "`pwd`> modifing ${File}.. (linking to right libraries)"
    
        ln -s "${LibRoute}/cfs.h"  "cfs/cfs.h"    
    	ln -s "${LibRoute}/leds.h"  "dev/leds.h"
    	ln -s "${LibRoute}/watchdog.h"  "dev/watchdog.h"
    	ln -s "${LibRoute}/list.h"  "lib/list.h"
    	ln -s "${LibRoute}/memb.h"  "lib/memb.h"
    	ln -s "${LibRoute}/random.h"  "lib/random.h"
    	ln -s "${LibRoute}/clock.h"  "sys/clock.h"
    	ln -s "${LibRoute}/ctimer.h"  "sys/ctimer.h"
    	ln -s "${LibRoute}/gen_task.h"  "sys/pt.h"
    	ln -s "${LibRoute}/rtimer.h"  "sys/rtimer.h"
    	
        #replaceInFile ${File} 'typedef t_u8 t_u8;'     '#include "stm32_io.h" //TheToolChain already define in STM32_io.h  typedef t_u8 t_u8;'
        
    fi #}
    cd "$Install_Path"
    
    echo "" >OK.Install
  fi
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then #{
  Name="${Install_Dir}"
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

//  #include "stm32_eth.h"
//  #include "ethernet.h"

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

#EXTENSION_HTTP_SERVER=1   # activate to include this feature!

ifdef EXTENSION_$Name
#   ifndef STM32F10X_CL
#     ERROR: STM32F10X_CL_must_be_set!
#   endif
 
INCLUDE_DIRS += -I additionals/$Name/adaptLayer/inc/ \\
                -I additionals/$Name/net/ \\
                -I additionals/$Name/net/mac/ \\
                -I additionals/$Name/net/rime/ \\
		-I additionals/$Name/net/rpl/ \\
		-I additionals/$Name/sys/ \\
		-I additionals/$Name/lib/ \\
		-I additionals/$Name/dev/ \\
		-I additionals/$Name/cfs/ \\

VPATH += additionals/$Name/adaptLayer/ \\
         additionals/$Name/net/ \\
         additionals/$Name/net/mac/ \\
         additionals/$Name/net/rime/ \\
	 additionals/$Name/net/rpl/ \\
	 additionals/$Name/sys/ \\
	 additionals/$Name/lib/ \\
	 additionals/$Name/dev/ \\
	 additionals/$Name/cfs/ \\

MAIN_OBJS += *.o

#ifdef EXTENSION_HTTP_SERVER

#COMPILE_OPTS += -DEXTENSION_HTTP_SERVER

#VPATH += $Name/Ethernet/uip/httpd/
#MAIN_OBJS += httpd-fs.o http-strings.o httpd.o httpd-cgi.o psock.o

#endif

endif
END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ $0 "6LoWPAN-stack" #{
      cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
rm 2>/dev/null \$Dir_ExtensionsActive/makefile.400_network_*

# ACTIVATE_SECTION_B create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
#addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; createLink \$Source/TheToolChain/InstallData/${Install_Dir}/ $Install_Dir"

# ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_E call other activate-scripts
# activate.500_ttc_memory.sh QUIET "\$0"
# activate.500_ttc_task.sh QUIET "\$0"
# activate.500_ttc_timer.sh QUIET "\$0"

END_OF_ACTIVATE

  # create links into extensions.active/
  echo "createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET" >>$ActivateScriptFile
  
  createActivateScriptTail $Name ../extensions/ 
  #}

  addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir;  createLink \$Source/TheToolChain/InstallData/$Install_Dir/isolated-network-contiki-2.6/  $Install_Dir"
  echo "Installed successfully: $Install_Dir"
#}
else
  echo "failed to install $Install_Dir"
  exit 10
fi

cd ..

#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$StartPath"
applyTargetUserAndGroup "$Install_Dir"

exit 0
