#!/bin/bash

#
#  Install script for USB serial port chip FTDI
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "999_libftd2xx" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then #{
  
  echo "installing in $Install_Dir ..."
  
  OldFolder=`pwd`
  if [ ! -e OK.Packages ]; then #{
    installPackage gcc
    installPackage make
    installPackage libusb-1.0-0-dev 
    installPackage libusb-dev
    touch OK.Packages
  fi #}
  
  Version="1.1.12"
  Folder="libftd2xx${Version}"
  getFile http://www.ftdichip.com/Drivers/D2XX/Linux/ $Folder.tar.gz
  untgz $Folder $Folder.tar.gz
  add2CleanUp ../cleanup.sh "rm -Rf $Install_Dir/$Folder"
  if [ -d $Folder ]; then
    cd "$Folder"
  fi
  
  findPrefix
  
  sudo cp -v ftd2xx.h $Prefix/include
  sudo cp -v WinTypes.h $Prefix/include
  sudo chmod a+r $Prefix/include/ftd2xx.h $Prefix/include/WinTypes.h
  
  ArchitectureIs32Bit=`uname -a | grep i686`
  if [ "$ArchitectureIs32Bit" != "" ]; then
    echo "installing for 32 bit architecture.."
  
    sudo find ./ -wholename "*i386/*.so.*" -exec cp -v {} $Prefix/lib/ \;
    cd "$Prefix/lib"
    createLink libftd2xx.so.${Version} libftd2xx.so root
    cd /usr/lib
    createLink $Prefix/lib/libftd2xx.so.${Version} libftd2xx.so root
  else  # 64 Bit OS
    echo "installing for 64 bit architecture.."
  
    sudo find ./ -wholename "*x86_64/*.so.*" -exec cp -v {} $Prefix/lib/ \;
    cd "$Prefix/lib64"
    createLink libftd2xx.so.${Version} libftd2xx.so root
    cd /usr/lib
    createLink $Prefix/lib/libftd2xx.so.${Version} libftd2xx.so root
  fi
  sudo -i ldconfig
  
  cd "$OldFolder"

  echo "" >OK.Install

fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then #{

  echo "Installed successfully: $Install_Dir"
#}
else
  echo "failed to install $Install_Dir"
  exit 10
fi

#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
