#!/bin/bash
#
#
#  Install script for generic inter task communication support 
#
#  Initial Script written by Gregor Rebel 2010-2018.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="500"
EXTENSION_SHORT="queue"
EXTENSION_PREFIX="ttc"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

  Name="$Install_Dir"
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += ttc_queue.o

ifdef EXTENSION_300_scheduler_freertos
  # activate certain features of FreeRTOS to support queuing
  COMPILE_OPTS += -DconfigUSE_MUTEXES=1
  COMPILE_OPTS += -DconfigUSE_COUNTING_SEMAPHORES=1
endif

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Scheduler independent support for inter task communication via queues, semaphores and mutexes" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_Additionals/${Name}_*

activate.500_ttc_mutex.sh QUIET \"\$ScriptName\"
activate.500_ttc_memory.sh QUIET \"\$ScriptName\"

ArchitectureSupportAvailable=""

# create links into extensions.active/
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  Name="${Install_Dir}_regression"  
  createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

  #include "regression_queue.h"
  regression_queue_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name}      #{ create makefile for regression
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional directories to search for header files
INCLUDE_DIRS += -I regressions/

# additional directories to search for C-Sources
vpath %.c regressions/ 

# additional object files to be compiled
MAIN_OBJS += regression_queue.o

# we have to define at least one font to make ttc_font.h happy
COMPILE_OPTS += -DTTC_FONT1=font_type1_16x24

# activate extra checks for regression tests
COMPILE_OPTS += -DTTC_REGRESSION

# define GPIO pins to be used for runtime analysis
COMPILE_OPTS += -DREGRESSION_PIN_QUEUE_PUSH=E_ttc_gpio_pin_d0
COMPILE_OPTS += -DREGRESSION_PIN_QUEUE_PULL=E_ttc_gpio_pin_d1
COMPILE_OPTS += -DREGRESSION_PIN_QUEUE_PUSH_ISR=E_ttc_gpio_pin_d2
COMPILE_OPTS += -DREGRESSION_PIN_QUEUE_PULL_ISR=E_ttc_gpio_pin_d3
COMPILE_OPTS += -DREGRESSION_PIN_QUEUE_PUSH_ACTIVITY=E_ttc_gpio_pin_d4

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "regression test for simple queues" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/$Install_Dir_*

#activate.300_scheduler_freertos_heap4.sh QUIET "\$ScriptName"
activate.300_scheduler_freertos_stack_check_intense.sh QUIET "\$ScriptName"
activate.500_ttc_gfx.sh        QUIET "\$ScriptName"
activate.500_ttc_string.sh     QUIET "\$ScriptName"
#? activate.500_ttc_list.sh       QUIET "\$ScriptName"
activate.500_ttc_queue.sh      QUIET "\$ScriptName"
activate.500_ttc_semaphore.sh  QUIET "\$ScriptName"
activate.500_ttc_mutex.sh      QUIET "\$ScriptName"
activate.500_ttc_random.sh     QUIET "\$ScriptName"
activate.500_ttc_math_basic.sh QUIET "\$ScriptName"
activate.500_ttc_task.sh       QUIET "\$ScriptName"
activate.500_ttc_timer.sh      QUIET "\$ScriptName"
activate.500_ttc_gpio.sh       QUIET "\$ScriptName"

if [ -e \${Dir_ExtensionsActive}/makefile.500_ttc_gfx ]; then
  # at least one font must be activated
  activate.510_ttc_font__font_type1_16x24.sh QUIET "\$ScriptName"
fi

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

    Name="${Install_Dir}_speed_test"  
  createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

  #include "regression_queue.h"
  regression_queue_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name}      #{ create makefile for regression
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional directories to search for header files
INCLUDE_DIRS += -I regressions/

# additional directories to search for C-Sources
vpath %.c regressions/ 

# additional object files to be compiled
MAIN_OBJS += regression_queue.o

# we have to define at least one font to make ttc_font.h happy
COMPILE_OPTS += -DTTC_FONT1=font_type1_16x24

# define GPIO pins to be used for runtime analysis
COMPILE_OPTS += -DREGRESSION_PIN_QUEUE_PUSH=E_ttc_gpio_pin_d0
COMPILE_OPTS += -DREGRESSION_PIN_QUEUE_PULL=E_ttc_gpio_pin_d1
COMPILE_OPTS += -DREGRESSION_PIN_QUEUE_PUSH_ISR=E_ttc_gpio_pin_d2
COMPILE_OPTS += -DREGRESSION_PIN_QUEUE_PULL_ISR=E_ttc_gpio_pin_d3
COMPILE_OPTS += -DREGRESSION_PIN_QUEUE_PUSH_ACTIVITY=E_ttc_gpio_pin_d4
COMPILE_OPTS += -DREGRESSION_PIN_QUEUE_BYTE_TRANSFER=E_ttc_gpio_pin_d5

# disable smart sanity checks for maximum speed
COMPILE_OPTS += -DTTC_SMART_MUTEXES=0        # fast mutexes
COMPILE_OPTS += -DTTC_SMART_SEMAPHORES=0     # fast semaphores
COMPILE_OPTS += -DTTC_MUTEX_TIMEOUT_USECS=0  # no forced timeout

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "performance test for simple queues" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/$Install_Dir_*

#activate.300_scheduler_freertos_heap4.sh QUIET "\$ScriptName"
activate.300_scheduler_freertos_stack_check_intense.sh QUIET "\$ScriptName"
activate.500_ttc_gfx.sh        QUIET "\$ScriptName"
activate.500_ttc_string.sh     QUIET "\$ScriptName"
#? activate.500_ttc_list.sh       QUIET "\$ScriptName"
activate.500_ttc_queue.sh      QUIET "\$ScriptName"
activate.500_ttc_semaphore.sh  QUIET "\$ScriptName"
activate.500_ttc_mutex.sh      QUIET "\$ScriptName"
activate.500_ttc_random.sh     QUIET "\$ScriptName"
activate.500_ttc_math_basic.sh QUIET "\$ScriptName"
activate.500_ttc_task.sh       QUIET "\$ScriptName"
activate.500_ttc_timer.sh      QUIET "\$ScriptName"
activate.500_ttc_gpio.sh       QUIET "\$ScriptName"

# disable all Assert_XXX() macros for maximum speed
activate.060_basic_extensions_disable_asserts.sh QUIET "\$ScriptName"

if [ -e \${Dir_ExtensionsActive}/makefile.500_ttc_gfx ]; then
  # at least one font must be activated
  activate.510_ttc_font__font_type1_16x24.sh QUIET "\$ScriptName"
fi

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  echo "Installed successfully: $Name"

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
