#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="400"
1EXTENSION_SHORT="fix_point_lib"
EXTENSION_NAME="$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.isep.pw.edu.pl/ZakladNapedu/lab-skp/ IQmath.pdf                       FixPointLib_IQMath_Manual.pdf    Math/
  getDocumentation http://thetoolchain.com/mirror/                 FixPointLib_IQMath_Overview.html FixPointLib_IQMath_Overview.html Math/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then #{
  echo "installing in $Install_Dir ..."
  Install_Path=`pwd`
  
  InstallInHomeDir="1"
  if [ "$InstallInHomeDir" == "1" ]; then
    Prefix=`pwd`  # install into subfolder of InstallData/ for current user only
  else
    findPrefix    # install for all users
  fi
  
  if [ ! -e OK.Packages ]; then #{
    installPackage svn
    installPackage subversion
    if [ -e /usr/bin/svn ]; then
      touch OK.Packages
    else
      echo "ERROR: Failed to install packages!"
      exit 10
    fi
  fi #}
  
  # Google requires a valid user account even to access read-only, open source repositories 
  #DISABLED: svn checkout http://fixpointlib.googlecode.com/svn/trunk/ fixpointlib-read-only
  getFile http://thetoolchain.com/mirror/ fixpointlib.tgj fixpointlib.tgj
  untgj "foo" fixpointlib.tgj
  
  Folder="fixpointlib-read-only/CortexM3/Project/FixPointLibTest/uVisionGcc"
  if [ -d ${Folder} ]; then
    cp -R $Folder .
    add2CleanUp ../cleanup.sh "rm -Rf $Install_Dir/fixpointlib-read-only"
    if [ -e IQmath.pdf ]; then
      rm 2>/dev/null uVisionGcc/IQmath.pdf
      ln IQmath.pdf uVisionGcc/
    fi
  fi
  
  if [ -d uVisionGcc ]; then
    echo "" >OK.Install
  fi
  #cd ..
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then #{
  Name="${Install_Dir}"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

INCLUDE_DIRS += -I additionals/$Name/
LD_LIBS += additionals/$Name/FixPointLib.lib

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Fixpoint Library (faster calculations than software floating point)" #{
  
  # create links into extensions.active/
  echo "createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET" >>$ActivateScriptFile
  
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir;  createLink \$Source/TheToolChain/InstallData/$Install_Dir/uVisionGcc/  $Install_Dir"
  echo "Installed successfully: $Install_Dir"
#}
else
  echo "failed to install $Install_Dir"
  exit 10
fi

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
