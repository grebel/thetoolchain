#!/bin/bash
#
#
#  Install script for cc1190 power amplifier support 
#
#  Initial Script written by Gregor Rebel 2010-2018.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "400_radio_cc1190" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

  Name="$Install_Dir"
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# external radio connected via SPI
MAIN_OBJS += radio_cc1190.o

# Note: Check radio_cc1190.h for a list of additional required compile options!

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Architecture independent support for Universal Synchronous Asynchronous Serial Receiver Transmitter (RADIO)" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_Additionals/${Name}_*

# create links into extensions.active/
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
  echo "Installed successfully: $Name"

  for Index in 1 2 3 4 5; do    
    Name="150_board_extension_radio_${Index}_cc1190"
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

#{ TTC_RADIO${Index}_AMPLIFIER pin configuration for amplifier of first radio transceiver
#
# example configuration of a cc1190 radio amplifier as an extension board
# connect amplifier exactly like described here or define these constants in your extensions.local/makefile.700_extra_settings 

# set type of driver to use (one from ttc_radio_types.h/e_ttc_radio_driver)
COMPILE_OPTS += -DTTC_RADIO${Index}_AMPLIFIER=trd_cc1190

# pin configurations below are highly dependent on current low-level driver (here cc1190)
COMPILE_OPTS += -DTTC_RADIO${Index}_AMPLIFIER_HGM=E_ttc_gpio_pin_c8
COMPILE_OPTS += -DTTC_RADIO${Index}_AMPLIFIER_HGM_TYPE=E_ttc_gpio_mode_output_push_pull # -> ttc_gpio_types.h

COMPILE_OPTS += -DTTC_RADIO${Index}_AMPLIFIER_PIN_LNA_ENABLE=E_ttc_gpio_pin_c8
COMPILE_OPTS += -DTTC_RADIO${Index}_AMPLIFIER_PIN_LNA_ENABLE_TYPE=E_ttc_gpio_mode_output_push_pull # -> ttc_gpio_types.h/e_ttc_gpio_mode

COMPILE_OPTS += -DTTC_RADIO${Index}_AMPLIFIER_E_ttc_gpio_pin_a_ENABLE=E_ttc_gpio_pin_c8
COMPILE_OPTS += -DTTC_RADIO${Index}_AMPLIFIER_E_ttc_gpio_pin_a_ENABLE_TYPE=E_ttc_gpio_mode_output_push_pull # -> ttc_gpio_types.h/e_ttc_gpio_mode

#}TTC_RADIO${Index}_AMPLIFIER

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Example extension for external radio amplifier cc1190" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.600_example_*

activate.400_radio_cc1190.sh     QUIET \"\$ScriptName\"
activate.500_ttc_radio.sh        QUIET \"\$ScriptName\"
activate.500_ttc_gpio.sh         QUIET \"\$ScriptName\"
activate.500_ttc_spi.sh          QUIET \"\$ScriptName\"

# create links into extensions.active/
createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
    echo "Installed successfully: $Name"
  done

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
