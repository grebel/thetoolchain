#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="600"
EXTENSION_SHORT="threading"
EXTENSION_PREFIX="example"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

  Name="${Install_Dir}_queues" #{ 
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "example_threading_queues.h"
  example_threading_queues_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

INCLUDE_DIRS += -I examples/
vpath %.c examples/

MAIN_OBJS += example_threading_queues.o

# define font to use
COMPILE_OPTS += -DTTC_FONT1=font_type1_16x24

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Examples of using Multitasking: Thread-synchronization/ -communication via Queues for boards with LCD-screen" #{

  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.600_example_*
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

activate.500_ttc_gpio.sh   QUIET \"\$ScriptName\"
#activate.500_ttc_i2c.sh    QUIET \"\$ScriptName\"
activate.500_ttc_task.sh   QUIET \"\$ScriptName\"
activate.500_ttc_queue.sh  QUIET \"\$ScriptName\"
activate.500_ttc_mutex.sh  QUIET \"\$ScriptName\"
activate.500_ttc_memory.sh QUIET \"\$ScriptName\"
activate.500_ttc_gfx.sh    QUIET "\$ScriptName"

# at least one font must be activated
activate.510_ttc_font__font_type1_16x24.sh QUIET "\$ScriptName"

END_OF_ACTIVATE
#}
  
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

#}queues
  Name="${Install_Dir}_semaphores" #{ 
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "example_threading_semaphores.h"
  example_threading_semaphores_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

COMPILE_OPTS += -DconfigUSE_COUNTING_SEMAPHORES
COMPILE_OPTS += -DconfigUSE_MUTEXES

INCLUDE_DIRS += -I examples/
vpath %.c examples/

MAIN_OBJS += example_threading_semaphores.o

# define font to use
COMPILE_OPTS += -DTTC_FONT1=font_type1_16x24

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}

  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Examples of using Multitasking: Thread-synchronization/ -communication via Semaphores for boards with LCD-screen" #{
  echo 'rm 2>/dev/null ${Dir_ExtensionsActive}/makefile.600_example_*' >>$ActivateScriptFile
  
  # create links into extensions.active/
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.600_example_*
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

activate.500_ttc_gpio.sh QUIET \"\$ScriptName\"
activate.500_ttc_i2c.sh QUIET \"\$ScriptName\"
activate.500_ttc_task.sh QUIET \"\$ScriptName\"
activate.500_ttc_queue.sh QUIET \"\$ScriptName\"
activate.500_ttc_mutex.sh  QUIET \"\$ScriptName\"
activate.500_ttc_memory.sh QUIET \"\$ScriptName\"
activate.500_ttc_gfx.sh    QUIET "\$ScriptName"

# at least one font must be activated
activate.510_ttc_font__font_type1_16x24.sh QUIET "\$ScriptName"

END_OF_ACTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
  
#}semaphores
  echo "Installed successfully: $Install_Dir"

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
