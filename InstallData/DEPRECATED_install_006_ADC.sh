#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2011.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "500_ttc_adc" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
  Name="$Install_Dir"
  createExtensionSourcefileHead ${Name}    #{ (create initializer code)

  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

  #include "../ttc_adc.h"


END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

# Check for used architecture and activate .o file
ifdef TARGET_ARCHITECTURE_STM32F1xx
  MAIN_OBJS += stm32_adc.o
endif

ifdef EXTENSION_200_cpu_stm32f10x
  MAIN_OBJS += stm32f10x_dma.o
endif
  
  MAIN_OBJS += ttc_adc.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $0 "architecture independent support of Analog Digital Converters (ToDo: INCOMPLETE)" #{ 
  
  # create links into extensions.active/
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile

createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

activate.250_stm_std_peripherals__adc.sh QUIET "\$0"
activate.500_ttc_gpio.sh                 QUIET "\$0"
#? activate.500_ttc_list.sh                 QUIET "\$0"

END_OF_ACTIVATE
  
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

#}

  echo "Installed successfully: $Install_Dir"

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
