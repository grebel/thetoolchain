#!/bin/bash

#
#  Install script for ARM GNU C-Compiler Collection  
#
#  Initial Script written by Gregor Rebel 2010-2018.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64
#
# Debugging arm with QtCreator:
# http://electronics.stackexchange.com/questions/212018/debugging-an-arm-stm32-microcontroller-using-qt-creator


source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="050"
EXTENSION_SHORT="gcc_arm_binary"
EXTENSION_PREFIX="compiler"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ -e "$NewestDownloads" ]; then #{ 
  source $NewestDownloads
  if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{
    rm  $NewestDownloads # will retry download
  fi #}
fi #}

if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  echo -n "$ScriptName: looking for new GCC... "
  updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  else
    echo "trying with old version.."
  fi
fi #}
source $NewestDownloads
if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{
  echo "$ScriptName - ERROR: Cannot find newest GCC version!"
  #exit 10
fi #}

FailedInstalls=""
installPackageSafe "ls /usr/lib/i386-linux-gnu/libncurses.so"       libncurses5-dev:i386
if [ "$FailedInstalls" == "" ]; then
  touch OK.Packages
else
  echo "$ScriptName - ERROR: Failed package installs: $FailedInstalls"
fi
if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ download + install software
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir ..."
    Install_Path=`pwd`
    
    getFile $GCC_URL $GCC_File $GCC_File
    untgj "current" $GCC_File
    Dir=`find ./ -maxdepth 1 -mindepth 1 -type d -name "gcc-arm-none-eabi*"`
    createLink "$Dir" "current"
    Version=`ls current/lib/gcc/arm-none-eabi/`
    PrevPWD=`pwd`;
    cd current/lib/gcc/arm-none-eabi/
    createLink $Version current
    cd "$PrevPWD"
    
    if [ -e "current/bin/arm-none-eabi-gcc" ]; then
      echo "" >OK.Install
    fi
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts

  BinFolder="`pwd`/current/bin"
  ToolChainBinFolder="Source/TheToolChain/InstallData/bin"

  Name="${Install_Dir}"  
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

//  #include "YOUR_SOURCE_HERE.h"
//  YOUR_START_HERE();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

COMPILE_OPTS += -DSOURCE_COMPILER=$Name

INCLUDE_DIRS += -I additionals/$Name/arm-none-eabi/include/ \\
                -I additionals/$Name/lib/gcc/arm-none-eabi/4.4.1/include/

# binaries from CodeSourcery must be somewhere in $PATH
CC =arm-none-eabi-gcc
AR =arm-none-eabi-ar
CP =arm-none-eabi-objcopy
OD =arm-none-eabi-objdump
GS =arm-none-eabi-size
#LD =arm-none-eabi-ld -v
LD =arm-none-eabi-gcc                                       # workaround: floating-point bug (http://fun-tech.se/stm32/OlimexBlinky/ld_float.php)
#AS =arm-none-eabi-as
AS =\$(CC) \$(AS_OPTS) -x assembler-with-cpp -c \$(TARGET)  # workaround: .o-files compiled by as cannot be linked by ld

CFLAGS =-std=gnu99 -c -fno-common -fno-builtin -ffreestanding -funsigned-char \$(COMPILE_OPTS) \$(TARGET) 
LDFLAGS=-Wl,-Map=\$(MAIN_MAP) -nostartfiles \$(TARGET)
#X LDFLAGS += -Wl,-\( \${filter %.a,\$^} \$(LD_LIBS) -Wl,-\\)      # workaround: make pre-compiled libraries available

# 
#? LD_LIBS += --specs=nano.specs

# link compiler provided libraries (ToDo: adapt for non-thumb microcontrollers!)
LD_LIBS +=  additionals/050_compiler_gcc_arm_binary/arm-none-eabi/lib/thumb/libc_nano.a
LD_LIBS +=  additionals/050_compiler_gcc_arm_binary/arm-none-eabi/lib/thumb/libg_nano.a
#LD_LIBS +=  additionals/050_compiler_gcc_arm_binary/lib/gcc/arm-none-eabi/current/libgcc.a # current/ is a symbolic link created by $ScriptName during installation 
LD_LIBS +=  additionals/050_compiler_gcc_arm_binary/arm-none-eabi/lib/thumb/libm.a
COMPILE_OPTS += -msoft-float

CPFLAGS=-Obinary
ODFLAGS	= -S
ASFLAGS=\$(COMPILE_OPTS)
# ASFLAGS=-gdwarf2 -mcpu=\$(ARM_CPU) -mthumb-interwork -o\$@

# make enums as small as possible
COMPILE_OPTS += -fshort-enums 

# pack all structure members together without holes
COMPILE_OPTS += -fpack-struct

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "binary distribution of recent GCC for CortexM3 published by ARM Corp." #{ create activate script
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals         -> additionals/
#   \$Dir_Bin                 -> bin/

# ACTIVATE_SECTION_A remove activated variants of same type
rm -f 2>/dev/null \${Dir_ExtensionsActive}/*.050_compiler_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
#createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# faked sbrk will allow to compile functions that want to use virtual memory (though they will fail to run!)
activate.450_fake_sbrk_support.sh        QUIET "\$ScriptName"

Dir_SourceBin="\$HOME/Source/TheToolChain/InstallData/$Install_Dir/current/bin/"

if [ ! -d "\$Dir_Bin" ]; then
  mkdir -v "\$Dir_Bin"
fi
cd "\$Dir_Bin"
if [ ! -d "\$Dir_SourceBin" ]; then
  echo "\$ScriptName - ERROR: Cannot find \$Dir_SourceBin"
  exit 11
fi

find \$Dir_SourceBin -name "arm-none-eabi*" -exec ln -sfv {} . \;

# create TTC compiler set (certain scripts require binaries with fixed names)
ln -sfv \${Dir_SourceBin}arm-none-eabi-as      assembler
ln -sfv \${Dir_SourceBin}arm-none-eabi-gcc     compiler
ln -sfv \${Dir_SourceBin}arm-none-eabi-gcc     linker
ln -sfv \${Dir_SourceBin}arm-none-eabi-objcopy objcopy
ln -sfv \${Dir_SourceBin}arm-none-eabi-objdump objdump

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
  # create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
  addLine ../scripts/createLinks.sh "rm -f 2>/dev/null $Install_Dir; createLink \$Source/TheToolChain/InstallData/${Install_Dir}/current/ $Install_Dir"
    
  cd "$OldPWD"
  echo "Installed successfully: $Install_Dir"
#}
else                            #{ Install failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
