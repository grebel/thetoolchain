#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2017.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"
                          
RANK="100"
EXTENSION_SHORT="olimex_p107"
EXTENSION_PREFIX="board"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://olimex.com/dev/pdf/ARM/ST/ STM32-P107.pdf                                                                       Board_STM32-P107.pdf  Boards
  #getDocumentation http://pdf1.alldatasheet.com/datasheet-pdf/view/162452/STMICROELECTRONICS/STE101P/+0W3W7WVPSLpETSpPKIbaZZTOfqgjUppKk+/ datasheet.pdf         Board_STM32-P107_EthernetPhy_STE101P.pdf Boards
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

Dir=`pwd`

  Name="${Install_Dir}"
  NameBoard="${Install_Dir}_RevA" #{
  createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "../ttc-lib/ttc_sysclock.h"

// set clocking profile to maximum speed
ttc_sysclock_profile_switch(tsp_Board_Olimex_STM32f1xx);
END_OF_SOURCEFILE
  createExtensionSourcefileTail ${NameBoard} #}    
  createExtensionMakefileHead ${NameBoard} #{
  File="${Dir_Extensions}makefile.${NameBoard}" 
  cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DTTC_BOARD=$Name
BOARD=$Name

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f107vct6 = 1

# system clock setup (-> ttc_sysclock_types.h)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=25000000
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL=32768


# Define port pins
# reference RevA: http://olimex.com/dev/pdf/ARM/ST/STM32-P107-Rev.A-schematic.pdf

COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c6
COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_c7

COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_c13                # press button switch 1 (named Tamper)
COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=1
COMPILE_OPTS += -DTTC_SWITCH2=E_ttc_gpio_pin_a0                 # press button switch 2 (named WKUP)
COMPILE_OPTS += -DTTC_SWITCH2_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH2_LOWACTIVE=0

COMPILE_OPTS += -DTTC_USART1=INDEX_USART3       # USART connected to RS232 connector 9-pin female
COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_d8   # RS232 connector 9-pin female TX-pin
COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_d9   # RS232 connector 9-pin female RX-pin
COMPILE_OPTS += -DTTC_USART1_RTS=E_ttc_gpio_pin_d12 # RS232 connector 9-pin female RTS-pin
COMPILE_OPTS += -DTTC_USART1_CTS=E_ttc_gpio_pin_d11 # RS232 connector 9-pin female CTS-pin
  
COMPILE_OPTS += -DTTC_USART2=INDEX_USART2       # USART connected to 10-pin male header connector
COMPILE_OPTS += -DTTC_USART2_TX=E_ttc_gpio_pin_d5   # RS232 on 10-pin male header connector TX-Pin
COMPILE_OPTS += -DTTC_USART2_RX=E_ttc_gpio_pin_d6   # RS232 on 10-pin male header connector RX-Pin


#{ define pin configuration of GPIOs on the UEXT Conncector
  COMPILE_OPTS += -DUEXT1_USART_TX=E_ttc_gpio_pin_d5 # USART2
  COMPILE_OPTS += -DUEXT1_USART_RX=E_ttc_gpio_pin_d6 # USART2
  COMPILE_OPTS += -DUEXT1_I2C_SCL=E_ttc_gpio_pin_b6
  COMPILE_OPTS += -DUEXT1_I2C_SDA=E_ttc_gpio_pin_b7
  COMPILE_OPTS += -DUEXT1_SPI_MISO=E_ttc_gpio_pin_c11
  COMPILE_OPTS += -DUEXT1_SPI_MOSI=E_ttc_gpio_pin_c12
  COMPILE_OPTS += -DUEXT1_SPI_NSCK=E_ttc_gpio_pin_c10
  COMPILE_OPTS += -DUEXT1_SPI_NSS=E_ttc_gpio_pin_b15
  
  COMPILE_OPTS += -DUEXT1_SPI_INDEX=3
  COMPILE_OPTS += -DUEXT1_USART_INDEX=2
  COMPILE_OPTS += -DUEXT1_I2C_INDEX=1
#}
#{ define Controller Area Network (CAN) devices
  COMPILE_OPTS += -DTTC_CAN1=0               # physical device index
  COMPILE_OPTS += -DTTC_CAN1_PIN_RX=E_ttc_gpio_pin_b8  # pin used for receive
  COMPILE_OPTS += -DTTC_CAN1_PIN_TX=E_ttc_gpio_pin_b9  # pin used for transmit
  COMPILE_OPTS += -DTTC_CAN2=1               # physical device index
  COMPILE_OPTS += -DTTC_CAN2_PIN_RX=E_ttc_gpio_pin_b12 # pin used for receive
  COMPILE_OPTS += -DTTC_CAN2_PIN_TX=E_ttc_gpio_pin_b13 # pin used for transmit
#}CAN

#{ parallel ports (8 or 16 consecutive pins in same bank)
#
# Every 8 available, consecutive bits in same GPIO bank can form an 8-bit wide port.
# A 16-bit wide port requires 16 available, consecutive bits (obviously).
# A pin is available if it not driven externally or used for something else.
# Check circuit of your board for details.

# 8-bit wide parallel port #1 starting at pin 0 of bank E
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_BANK=TTC_GPIO_BANK_E
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_FIRSTPIN=0

# 8-bit wide parallel port #2 starting at pin 8 of bank D (overlaps with USART2, USART3)
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_BANK=TTC_GPIO_BANK_D
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_FIRSTPIN=8


#}parallel ports
#{ SPI1
COMPILE_OPTS += -DTTC_SPI1=ttc_device_3          # SPI connected to RS232 10-pin male header UEXT and SDCARD slot
COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_c12 # UEXT pin 8
COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_c11 # UEXT pin 7
COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_c10  # UEXT pin 9
COMPILE_OPTS += -DTTC_SPI1_NSS=E_ttc_gpio_pin_b15  # UEXT pin 10 (SoftwareNSS)
#}SPI1
#{ I2C1 - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b9    # UEXT-6
  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b8    # UEXT-5
  COMPILE_OPTS += -DTTC_I2C1_SMBAL=E_ttc_gpio_pin_b5  # 
#}I2C1
#{ Ethernet Interface

# configure ethernet transceiver
COMPILE_OPTS += -DTTC_ETHERNET1=0                                 # there is one ethernet interface on this board (->ttc_ethernet_types.h)
COMPILE_OPTS += -DTTC_ETHERNET1_DRIVER=ta_ethernet_stm32f107      # type of ethernet transceicver     (->ttc_ethernet_types.h:e_ttc_ethernet_architecture)
COMPILE_OPTS += -DTTC_ETHERNET1_INTERFACE=ethernet_interface_rmii # type of interface to transceicver (->ttc_ethernet_types.h:e_ttc_ethernet_interface)
#}

COMPILE_OPTS += -DTTC_SDCARD1=1 # Define at least TTC_SDCARD1 as logical index of SPI device to use (1=TTC_SPI1, ...)

BoardRevA=1

END_OF_MAKEFILE
  createExtensionMakefileTail ${NameBoard} #}
  File="${Dir_Extensions}activate.${NameBoard}.sh" #{
  createActivateScriptHead $NameBoard ${Dir_Extensions} $ScriptName "Protoboard P107 Rev. A from Olimex -> http://olimex.com/dev/stm32-p107.html"
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$NameBoard \${Dir_ExtensionsActive}/makefile.$NameBoard '' QUIET
  
  # ACTIVATE_SECTION_D enable features provided by this board
  enableFeature 450_ethernet_ste101p
  enableFeature 450_sdcard_spi
  enableFeature 450_cpu_stm32f1xx

  #X activate.200_cpu_stm32f10x.sh       QUIET "\$ScriptName"
  
  # ACTIVATE_SECTION_C activate initialization source-code for this extension 
  createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

END_OF_ACTIVATE
#}
  createActivateScriptTail $NameBoard ${Dir_Extensions}
  #}
  #}RevA
  NameBoard="${Install_Dir}_RevB" #{
  createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "../ttc-lib/ttc_sysclock.h"

// set clocking profile to maximum speed
ttc_sysclock_profile_switch(tsp_Board_Olimex_STM32f1xx);
END_OF_SOURCEFILE
  createExtensionSourcefileTail ${NameBoard} #}    
  createExtensionMakefileHead ${NameBoard} #{
  File="${Dir_Extensions}makefile.${NameBoard}" 
  cat <<END_OF_MAKEFILE >>$File

COMPILE_OPTS += -DTTC_BOARD=$Name
BOARD=$Name

# define microcontroller used on board
#X STM32F10X_CL  =1
#X COMPILE_OPTS += -DSTM32F10X_CL

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f107vct6 = 1


# system clock setup (-> ttc_sysclock_types.h)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=25000000
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL=32768

# Define port pins
# reference RevB: http://olimex.com/dev/ARM/ST/STM32-P107/STM32-P107-REV-B-SCH.pdf

COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c6
COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_c7

COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_c13                # press button switch 1 (named Tamper)
COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=1
COMPILE_OPTS += -DTTC_SWITCH2=E_ttc_gpio_pin_a0                 # press button switch 2 (named WKUP)
COMPILE_OPTS += -DTTC_SWITCH2_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH2_LOWACTIVE=0

  
  COMPILE_OPTS += -DTTC_USART1=INDEX_USART2       # USART connected to RS232 connector 9-pin female
  COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_d5   # RS232 connector 9-pin female TX-pin
  COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_d6   # RS232 connector 9-pin female RX-pin
  COMPILE_OPTS += -DTTC_USART1_RTS=E_ttc_gpio_pin_d4  # RS232 connector 9-pin female RTS-pin
  COMPILE_OPTS += -DTTC_USART1_CTS=E_ttc_gpio_pin_d3  # RS232 connector 9-pin female CTS-pin
  
  COMPILE_OPTS += -DTTC_USART2=INDEX_USART3       # USART connected to 10-pin male header connector
  COMPILE_OPTS += -DTTC_USART2_TX=E_ttc_gpio_pin_d8   # RS232 on 10-pin male header connector TX-Pin
  COMPILE_OPTS += -DTTC_USART2_RX=E_ttc_gpio_pin_d9   # RS232 on 10-pin male header connector RX-Pin
  
#{ define pin configuration of GPIOs on the UEXT Conncector
  COMPILE_OPTS += -DUEXT1_USART_TX=E_ttc_gpio_pin_d8 # USART3
  COMPILE_OPTS += -DUEXT1_USART_RX=E_ttc_gpio_pin_d9 # USART3
  COMPILE_OPTS += -DUEXT1_I2C_SCL=E_ttc_gpio_pin_b8
  COMPILE_OPTS += -DUEXT1_I2C_SDA=E_ttc_gpio_pin_b9
  COMPILE_OPTS += -DUEXT1_SPI_MISO=E_ttc_gpio_pin_c11
  COMPILE_OPTS += -DUEXT1_SPI_MOSI=E_ttc_gpio_pin_c12
  COMPILE_OPTS += -DUEXT1_SPI_SCK=E_ttc_gpio_pin_c10
  COMPILE_OPTS += -DUEXT1_SPI_NSS=E_ttc_gpio_pin_b15
  
  COMPILE_OPTS += -DUEXT1_SPI_INDEX=3
  COMPILE_OPTS += -DUEXT1_USART_INDEX=3
  COMPILE_OPTS += -DUEXT1_I2C_INDEX=1
#}
#{ define Controller Area Network (CAN) devices
  COMPILE_OPTS += -DTTC_CAN1=0               # physical device index
  COMPILE_OPTS += -DTTC_CAN1_PIN_RX=E_ttc_gpio_pin_b8  # pin used for receive
  COMPILE_OPTS += -DTTC_CAN1_PIN_TX=E_ttc_gpio_pin_b9  # pin used for transmit
  COMPILE_OPTS += -DTTC_CAN2=1               # physical device index
  COMPILE_OPTS += -DTTC_CAN2_PIN_RX=E_ttc_gpio_pin_b12 # pin used for receive
  COMPILE_OPTS += -DTTC_CAN2_PIN_TX=E_ttc_gpio_pin_b13 # pin used for transmit
#}CAN

#{ parallel ports (8 or 16 consecutive pins in same bank)
#
# Every 8 available, consecutive bits in same GPIO bank can form an 8-bit wide port.
# A 16-bit wide port requires 16 available, consecutive bits (obviously).
# A pin is available if it not driven externally or used for something else.
# Check circuit of your board for details.

# 8-bit wide parallel port #1 starting at pin 0 of bank E
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_BANK=TTC_GPIO_BANK_E
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_FIRSTPIN=0

# 8-bit wide parallel port #2 starting at pin 8 of bank D (overlaps with USART2, USART3)
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_BANK=TTC_GPIO_BANK_D
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_FIRSTPIN=8


#}parallel ports
#{ SPI1
COMPILE_OPTS += -DTTC_SPI1=ttc_device_3          # SPI connected to RS232 10-pin male header UEXT and SDCARD slot
COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_c12 # UEXT pin 8
COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_c11 # UEXT pin 7
COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_c10  # UEXT pin 9
COMPILE_OPTS += -DTTC_SPI1_NSS=E_ttc_gpio_pin_b15  # UEXT pin 10 (SoftwareNSS)
#}SPI1
#{ I2C1 - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b9    # UEXT-6
  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b8    # UEXT-5
  COMPILE_OPTS += -DTTC_I2C1_SMBAL=E_ttc_gpio_pin_b5  # 
#}I2C1
#{ configure ethernet transceiver
COMPILE_OPTS += -DTTC_ETHERNET1=0                                 # there is one ethernet interface on this board (->ttc_ethernet_types.h)
COMPILE_OPTS += -DTTC_ETHERNET1_DRIVER=ta_ethernet_stm32f107      # type of ethernet transceicver     (->ttc_ethernet_types.h:e_ttc_ethernet_architecture)
COMPILE_OPTS += -DTTC_ETHERNET1_INTERFACE=ethernet_interface_rmii # type of interface to transceicver (->ttc_ethernet_types.h:e_ttc_ethernet_interface)
#}

COMPILE_OPTS += -DTTC_SDCARD1=1 # Define at least TTC_SDCARD1 as logical index of SPI device to use (1=TTC_SPI1, ...)

BoardRevB=1

END_OF_MAKEFILE
  createExtensionMakefileTail ${NameBoard} 
  #}
  File="${Dir_Extensions}activate.${NameBoard}.sh" #{
  createActivateScriptHead $NameBoard ${Dir_Extensions} $ScriptName "Protoboard P107 Rev. B from Olimex -> http://olimex.com/dev/stm32-p107.html"
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$NameBoard \${Dir_ExtensionsActive}/makefile.$NameBoard '' QUIET
  
  # ACTIVATE_SECTION_D enable features provided by this board
  enableFeature 450_ethernet_ste101p
  enableFeature 450_sdcard_spi
  enableFeature 450_cpu_stm32f1xx

  #X activate.200_cpu_stm32f10x.sh       QUIET "\$ScriptName"
  
  # ACTIVATE_SECTION_C activate initialization source-code for this extension 
  createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

END_OF_ACTIVATE
#}
  createActivateScriptTail $NameBoard ${Dir_Extensions}
  #}RevB
  #}
  NameBoard="${Install_Dir}_RevC" #{
  createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "../ttc-lib/ttc_sysclock.h"

// set clocking profile to maximum speed
ttc_sysclock_profile_switch(tsp_Board_Olimex_STM32f1xx);
END_OF_SOURCEFILE
  createExtensionSourcefileTail ${NameBoard} #}    
  createExtensionMakefileHead ${NameBoard} #{
  File="${Dir_Extensions}makefile.${NameBoard}" 
  cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DTTC_BOARD=$Name
BOARD=$Name

# define microcontroller used on board
#X STM32F10X_CL  =1
#X COMPILE_OPTS += -DSTM32F10X_CL

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f107vct6 = 1


# system clock setup (-> ttc_sysclock_types.h)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=25000000
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL=32768

# Define port pins
# reference RevC: http://olimex.com/dev/ARM/ST/STM32-P107/STM32-P107-REV-C-SCH.pdf

COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c4
COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_c5

COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_c13                # press button switch 1 (named Tamper)
COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=1
COMPILE_OPTS += -DTTC_SWITCH2=E_ttc_gpio_pin_a0                 # press button switch 2 (named WKUP)
COMPILE_OPTS += -DTTC_SWITCH2_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH2_LOWACTIVE=0
  
COMPILE_OPTS += -DTTC_USART1=INDEX_USART2       # USART connected to RS232 connector 9-pin female
COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_d5   # RS232 connector 9-pin female TX-pin
COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_d6   # RS232 connector 9-pin female RX-pin
COMPILE_OPTS += -DTTC_USART1_RTS=E_ttc_gpio_pin_d4  # RS232 connector 9-pin female RTS-pin
COMPILE_OPTS += -DTTC_USART1_CTS=E_ttc_gpio_pin_d3  # RS232 connector 9-pin female CTS-pin
  
COMPILE_OPTS += -DTTC_USART2=INDEX_USART3       # USART connected to 10-pin male header connector
COMPILE_OPTS += -DTTC_USART2_TX=E_ttc_gpio_pin_d8   # RS232 on 10-pin male header connector TX-Pin
COMPILE_OPTS += -DTTC_USART2_RX=E_ttc_gpio_pin_d9   # RS232 on 10-pin male header connector RX-Pin
  
#{ define pin configuration of GPIOs on the UEXT Conncector
  COMPILE_OPTS += -DUEXT1_USART_TX=E_ttc_gpio_pin_d8 # USART3
  COMPILE_OPTS += -DUEXT1_USART_RX=E_ttc_gpio_pin_d9 # USART3
  COMPILE_OPTS += -DUEXT1_I2C_SCL=E_ttc_gpio_pin_b7
  COMPILE_OPTS += -DUEXT1_I2C_SDA=E_ttc_gpio_pin_b8
  COMPILE_OPTS += -DUEXT1_SPI_MISO=E_ttc_gpio_pin_c11
  COMPILE_OPTS += -DUEXT1_SPI_MOSI=E_ttc_gpio_pin_c12
  COMPILE_OPTS += -DUEXT1_SPI_SCK=E_ttc_gpio_pin_c10
  COMPILE_OPTS += -DUEXT1_SPI_NSS=E_ttc_gpio_pin_b11
  
  COMPILE_OPTS += -DUEXT1_SPI_INDEX=3
  COMPILE_OPTS += -DUEXT1_USART_INDEX=3
  COMPILE_OPTS += -DUEXT1_I2C_INDEX=1
#}
#{ define Controller Area Network (CAN) devices
  COMPILE_OPTS += -DTTC_CAN1=0               # physical device index
  COMPILE_OPTS += -DTTC_CAN1_PIN_RX=E_ttc_gpio_pin_b8  # pin used for receive
  COMPILE_OPTS += -DTTC_CAN1_PIN_TX=E_ttc_gpio_pin_b9  # pin used for transmit
  COMPILE_OPTS += -DTTC_CAN2=1               # physical device index
  COMPILE_OPTS += -DTTC_CAN2_PIN_RX=E_ttc_gpio_pin_b5 # pin used for receive
  COMPILE_OPTS += -DTTC_CAN2_PIN_TX=E_ttc_gpio_pin_b6 # pin used for transmit
#}CAN
#{ parallel ports (8 or 16 consecutive pins in same bank)
#
# Every 8 available, consecutive bits in same GPIO bank can form an 8-bit wide port.
# A 16-bit wide port requires 16 available, consecutive bits (obviously).
# A pin is available if it not driven externally or used for something else.
# Check circuit of your board for details.

# 8-bit wide parallel port #1 starting at pin 0 of bank E
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_BANK=TTC_GPIO_BANK_E
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_FIRSTPIN=0

# 8-bit wide parallel port #2 starting at pin 8 of bank D (overlaps with USART2, USART3)
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_BANK=TTC_GPIO_BANK_D
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_FIRSTPIN=8


#}parallel ports
#{ SPI1
COMPILE_OPTS += -DTTC_SPI1=ttc_device_3          # SPI connected to RS232 10-pin male header UEXT and SDCARD slot
COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a7 # UEXT pin 8
COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a6 # UEXT pin 7
COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_a5  # UEXT pin 9
COMPILE_OPTS += -DTTC_SPI1_NSS=E_ttc_gpio_pin_a15  # UEXT pin 10 (SoftwareNSS)
#}SPI1
#{ I2C1 - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b7    # UEXT-6
  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b6    # UEXT-5
  COMPILE_OPTS += -DTTC_I2C1_SMBAL=E_ttc_gpio_pin_b5  # 
#}I2C1
#{ configure ethernet transceiver
COMPILE_OPTS += -DTTC_ETHERNET1=0                                 # there is one ethernet interface on this board (->ttc_ethernet_types.h)
COMPILE_OPTS += -DTTC_ETHERNET1_DRIVER=ta_ethernet_stm32f107      # type of ethernet transceicver     (->ttc_ethernet_types.h:e_ttc_ethernet_architecture)
COMPILE_OPTS += -DTTC_ETHERNET1_INTERFACE=ethernet_interface_rmii # type of interface to transceicver (->ttc_ethernet_types.h:e_ttc_ethernet_interface)
#}

COMPILE_OPTS += -DTTC_SDCARD1=1 # Define at least TTC_SDCARD1 as logical index of SPI device to use (1=TTC_SPI1, ...)

BoardRevC=1

END_OF_MAKEFILE
  createExtensionMakefileTail ${NameBoard} #}
  File="${Dir_Extensions}activate.${NameBoard}.sh" #{
  createActivateScriptHead $NameBoard ${Dir_Extensions} $ScriptName "Protoboard P107 Rev. C from Olimex -> http://olimex.com/dev/stm32-p107.html"
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$NameBoard \${Dir_ExtensionsActive}/makefile.$NameBoard '' QUIET
  
  # ACTIVATE_SECTION_D enable features provided by this board
  enableFeature 450_ethernet_ste101p
  enableFeature 450_sdcard_spi
  enableFeature 450_cpu_stm32f1xx

  #X activate.200_cpu_stm32f10x.sh       QUIET "\$ScriptName"
  
  # ACTIVATE_SECTION_C activate initialization source-code for this extension 
  createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

END_OF_ACTIVATE
#}
  createActivateScriptTail $NameBoard ${Dir_Extensions}
  #}
  #}RevC
  NameBoard="${Install_Dir}_RevD" #{
  createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "../ttc-lib/ttc_sysclock.h"

// set clocking profile to maximum speed
ttc_sysclock_profile_switch(tsp_Board_Olimex_STM32f1xx);
END_OF_SOURCEFILE
  createExtensionSourcefileTail ${NameBoard} #}    
  createExtensionMakefileHead ${NameBoard} #{
  File="${Dir_Extensions}makefile.${NameBoard}" 
  cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DTTC_BOARD=$Name
BOARD=$Name

# define microcontroller used on board
#X STM32F10X_CL  =1
#X COMPILE_OPTS += -DSTM32F10X_CL

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f107vct6 = 1


# system clock setup (-> ttc_sysclock_types.h)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=25000000
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL=32768

# Define port pins
# reference RevC: http://olimex.com/dev/ARM/ST/STM32-P107/STM32-P107-REV-C-SCH.pdf

COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c4
COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_c5

COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_c13                # press button switch 1 (named Tamper)
COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=1
COMPILE_OPTS += -DTTC_SWITCH2=E_ttc_gpio_pin_a0                 # press button switch 2 (named WKUP)
COMPILE_OPTS += -DTTC_SWITCH2_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH2_LOWACTIVE=0
  
COMPILE_OPTS += -DTTC_USART1=INDEX_USART2       # USART connected to RS232 connector 9-pin female
COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_d5   # RS232 connector 9-pin female TX-pin
COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_d6   # RS232 connector 9-pin female RX-pin
COMPILE_OPTS += -DTTC_USART1_RTS=E_ttc_gpio_pin_d4  # RS232 connector 9-pin female RTS-pin
COMPILE_OPTS += -DTTC_USART1_CTS=E_ttc_gpio_pin_d3  # RS232 connector 9-pin female CTS-pin
  
COMPILE_OPTS += -DTTC_USART2=INDEX_USART3       # USART connected to 10-pin male header connector
COMPILE_OPTS += -DTTC_USART2_TX=E_ttc_gpio_pin_d8   # RS232 on 10-pin male header connector TX-Pin
COMPILE_OPTS += -DTTC_USART2_RX=E_ttc_gpio_pin_d9   # RS232 on 10-pin male header connector RX-Pin
  
#{ define pin configuration of GPIOs on the UEXT Conncector
  COMPILE_OPTS += -DUEXT1_USART_TX=E_ttc_gpio_pin_d8 # USART3
  COMPILE_OPTS += -DUEXT1_USART_RX=E_ttc_gpio_pin_d9 # USART3
  COMPILE_OPTS += -DUEXT1_I2C_SCL=E_ttc_gpio_pin_b7
  COMPILE_OPTS += -DUEXT1_I2C_SDA=E_ttc_gpio_pin_b8
  COMPILE_OPTS += -DUEXT1_SPI_MISO=E_ttc_gpio_pin_c11
  COMPILE_OPTS += -DUEXT1_SPI_MOSI=E_ttc_gpio_pin_c12
  COMPILE_OPTS += -DUEXT1_SPI_SCK=E_ttc_gpio_pin_c10
  COMPILE_OPTS += -DUEXT1_SPI_NSS=E_ttc_gpio_pin_b11
  
  COMPILE_OPTS += -DUEXT1_SPI_INDEX=3
  COMPILE_OPTS += -DUEXT1_USART_INDEX=3
  COMPILE_OPTS += -DUEXT1_I2C_INDEX=1
#}
#{ define Controller Area Network (CAN) devices
  COMPILE_OPTS += -DTTC_CAN1=0               # physical device index
  COMPILE_OPTS += -DTTC_CAN1_PIN_RX=E_ttc_gpio_pin_b8  # pin used for receive
  COMPILE_OPTS += -DTTC_CAN1_PIN_TX=E_ttc_gpio_pin_b9  # pin used for transmit
  COMPILE_OPTS += -DTTC_CAN2=1               # physical device index
  COMPILE_OPTS += -DTTC_CAN2_PIN_RX=E_ttc_gpio_pin_b5 # pin used for receive
  COMPILE_OPTS += -DTTC_CAN2_PIN_TX=E_ttc_gpio_pin_b6 # pin used for transmit
#}CAN
#{ parallel ports (8 or 16 consecutive pins in same bank)
#
# Every 8 available, consecutive bits in same GPIO bank can form an 8-bit wide port.
# A 16-bit wide port requires 16 available, consecutive bits (obviously).
# A pin is available if it not driven externally or used for something else.
# Check circuit of your board for details.

# 8-bit wide parallel port #1 starting at pin 0 of bank E
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_BANK=TTC_GPIO_BANK_E
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_FIRSTPIN=0

# 8-bit wide parallel port #2 starting at pin 8 of bank D (overlaps with USART2, USART3)
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_BANK=TTC_GPIO_BANK_D
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_FIRSTPIN=8


#}parallel ports
#{ SPI1
COMPILE_OPTS += -DTTC_SPI1=ttc_device_3          # SPI connected to RS232 10-pin male header UEXT and SDCARD slot
COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a7 # UEXT pin 8
COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a6 # UEXT pin 7
COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_a5  # UEXT pin 9
COMPILE_OPTS += -DTTC_SPI1_NSS=E_ttc_gpio_pin_a15  # UEXT pin 10 (SoftwareNSS)
#}SPI1
#{ I2C1 - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b7    # UEXT-6
  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b6    # UEXT-5
  COMPILE_OPTS += -DTTC_I2C1_SMBAL=E_ttc_gpio_pin_b5  # 
#}I2C1
#{ configure ethernet transceiver
COMPILE_OPTS += -DTTC_ETHERNET1=0                                 # there is one ethernet interface on this board (->ttc_ethernet_types.h)
COMPILE_OPTS += -DTTC_ETHERNET1_DRIVER=ta_ethernet_stm32f107      # type of ethernet transceicver     (->ttc_ethernet_types.h:e_ttc_ethernet_architecture)
COMPILE_OPTS += -DTTC_ETHERNET1_INTERFACE=ethernet_interface_rmii # type of interface to transceicver (->ttc_ethernet_types.h:e_ttc_ethernet_interface)
#}

COMPILE_OPTS += -DTTC_SDCARD1=1 # Define at least TTC_SDCARD1 as logical index of SPI device to use (1=TTC_SPI1, ...)

BoardRevC=1

END_OF_MAKEFILE
  createExtensionMakefileTail ${NameBoard} #}
  File="${Dir_Extensions}activate.${NameBoard}.sh" #{
  createActivateScriptHead $NameBoard ${Dir_Extensions} $ScriptName "Protoboard P107 Rev. C from Olimex -> http://olimex.com/dev/stm32-p107.html"
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$NameBoard \${Dir_ExtensionsActive}/makefile.$NameBoard '' QUIET
  
  # ACTIVATE_SECTION_D enable features provided by this board
  enableFeature 450_ethernet_ste101p
  enableFeature 450_sdcard_spi
  enableFeature 450_cpu_stm32f1xx

  #X activate.200_cpu_stm32f10x.sh       QUIET "\$ScriptName"
  
  # ACTIVATE_SECTION_C activate initialization source-code for this extension 
  createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

END_OF_ACTIVATE
#}
  createActivateScriptTail $NameBoard ${Dir_Extensions}
  #}
  #}RevD

  echo "Installed successfully: $Install_Dir"

cd ..
rmdir 2>/dev/null $Install_Dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
