#!/bin/bash

#
#  Install script for USB serial port chip driver as required to compile OpenOCD
#  Script written by Gregor Rebel 2010-2018.
#  libFTDI - FTDI USB driver with bitbang mode
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="999"
EXTENSION_SHORT="libftdi"
EXTENSION_NAME="$EXTENSION_SHORT"
setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

  LibUSB_Found=`which libusb-config`
  if [ "$LibUSB_Found" == "" ]; then #{ install missing packages
    installPackageSafe "which libusb-config" libusb-dev
  fi #}
  LibUSB_Found=`which libusb-config`
  if [ "$LibUSB_Found" != "" ]; then #{ check if installation was successfull
    touch OK.Packages
  else
    echo "$ScriptName - ERROR: Cannot install libusb which is required to compile libftdi!"
    exit 10
  fi #}
  
  LibUSB_Version=`libusb-config --version`
  LibUSB_Major=`perl -e "print substr('$LibUSB_Version',0,1);"`
  #? if [ "$LibUSB_Major" == "0" ]; then
    #? echo "looking for newest libusb from http://www.libusb.org/..."
    #? findNewest http://www.libusb.org/ "libusb-1.*bz2"
  #? fi

if [ ! -e OK.Install ]; then #{

  echo "installing in $Install_Dir ..."
  
  #Version="0.18"
  #Version="0.19"
  Version="0.20"
  
  LibUSB_Found=`which libusb-config`
  if [ "$LibUSB_Found" == "" ]; then #{
    echo "installing missing packages..."
    installPackage "libusb-devel"
    installPackage "libusb-dev"
    installPackage "libusb-1_0-devel"
    installPackage "libusb*-dev*"
    installPackage "libusb-compat-devel"
    installPackage "libusb"
    installPackage "libusb-dev"

    # on sime distros (e.g. Xubuntu 11.10, libusb does not get installed correctly at first run)
    installPackage "libusb-devel"
    installPackage "libusb-dev"
    installPackage "libusb-1_0-devel"
    installPackage "libusb*-dev*"
    installPackage "libusb-compat-devel"
    installPackage "libusb"
    installPackage "libusb-dev"
    
    #X updatedb

  fi #}
  
  Error=""
  if [ ! -e OK.Install_libftdi ]; then #{
    echo "installing libftdi..."
    Folder="libftdi-${Version}"
    getFile http://www.intra2net.com/de/produkte/opensource/ftdi/TGZ/ $Folder.tar.gz
    untgz $Folder $Folder.tar.gz
    add2CleanUp ../cleanup.sh "rm -Rf $Install_Dir/$Folder"
    
    InstallInHomeDir="0"
    if [ "$InstallInHomeDir" == "1" ]; then
      Prefix=`pwd`  # install into subfolder of InstallData/ for current user only
    else
      findPrefix   # install for all users
    fi
    cd "$Folder"
    Error="libftdi"
    ./configure --prefix=$Prefix   >configure.log 2>configure.err && \
                make -j2           >make.log      2>make.err && \
                sudo make install  >install.log   2>install.err && \
                Error=""
    cd ..
    if [ "$Error" == "" ]; then
      #X cp -R $Folder/doc ../../Documentation/libftdi
      createLink "$Folder/doc" libftdi-documentation
      addDocumentationFolder libftdi-documentation Programmer
      touch OK.Install_libftdi
    fi
    fi #}Install_libftdi
  if [ ! -e OK.Install_ft232r_prog ]; then #{
    echo "installing ft232r_prog..."
    #Version="1.07"
    Version="1.10"
    Folder="ft232r_prog-${Version}"
    getFile http://rtr.ca/ft232r/ ${Folder}.tar.gz
    untgz "v" ${Folder}.tar.gz
    cd "ft232r_prog-${Version}"
    replaceInFile Makefile "-Werror" ""
    make
    if [ -e ft232r_prog ]; then
      touch ../OK.Install_ft232r_prog
      createLink ../$Install_Dir/$Folder/ft232r_prog ../../bin/ft232r_prog
    fi
    
    cd ..
    getDocumentation http://rtr.ca/ft232r/ index.html ft232r.html Programmer/

  fi #}Install_ft232r_prog

  
  if [ "$Error" == "" ]; then #{
    if [ "$InstallInHomeDir" != "1" ]; then
      sudo /sbin/ldconfig
    fi

    echo "" >OK.Install
  else
    echo "Error occured during compilation ($Error)!"
    echo "check logfiles:"
    find `pwd`/ -name "*.log" -exec echo "  less {}" \;
    find `pwd`/ -name "*.err" -exec echo "  less {}" \;
  fi #}

fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then #{
  if [ "$InstallInHomeDir" == "1" ]; then

    addLine ../scripts/SourceMe.sh "export LD_LIBRARY_PATH=\"$Prefix/lib:\$LD_LIBRARY_PATH\" #$ScriptName"
    addLine ../scripts/SourceMe.sh "export PATH=\"$Prefix/bin:\$PATH\" #$ScriptName"
  fi
  echo "Installed successfully: $Install_Dir"
#}
else
  echo "failed to install $Install_Dir"
  exit 10
fi

#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0