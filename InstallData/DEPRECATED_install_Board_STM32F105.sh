#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2017.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"
                          
RANK="100"
EXTENSION_SHORT="stm32f105"
EXTENSION_PREFIX="board"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  #getDocumentation http://olimex.com/dev/pdf/ARM/ST/ STM32-P107.pdf                                                                       Board_STM32-P107.pdf  Boards
  #getDocumentation http://pdf1.alldatasheet.com/datasheet-pdf/view/162452/STMICROELECTRONICS/STE101P/+0W3W7WVPSLpETSpPKIbaZZTOfqgjUppKk+/ datasheet.pdf         Board_STM32-P107_EthernetPhy_STE101P.pdf Boards
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

Dir=`pwd`

  Name="${Install_Dir}"
  NameBoard="${Install_Dir}" #{
  createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "../ttc-lib/ttc_sysclock.h"

// set clocking profile to maximum speed ttc_sysclock_profile_switch(tsp_Board_Olimex_STM32f1xx);
END_OF_SOURCEFILE
  createExtensionSourcefileTail ${NameBoard} #}    
  createExtensionMakefileHead ${NameBoard} #{
  File="${Dir_Extensions}makefile.${NameBoard}" 
  cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DTTC_BOARD=$Name
BOARD=$Name

# define microcontroller used on board
#X STM32F10X_CL  =1
#X COMPILE_OPTS += -DSTM32F10X_CL

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f105rct6 = 1

# system clock setup (-> ttc_sysclock_types.h)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=25000000
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL=32768

#X COMPILE_OPTS += -DUC_PACKAGE_PINS=64
COMPILE_OPTS += -DTTC_INTERRUPT_RADIO_AMOUNT=0
COMPILE_OPTS += -DTTC_INTERRUPT_RTC_AMOUNT=0

COMPILE_OPTS += -DTTC_LED1=tgp_c6
COMPILE_OPTS += -DTTC_LED2=tgp_c7
COMPILE_OPTS += -DTTC_LED3=tgp_c8
COMPILE_OPTS += -DTTC_LED4=tgp_c9

# Usart
COMPILE_OPTS += -DTTC_USART1=INDEX_USART3       # USART connected to RS232 connector 9-pin female
COMPILE_OPTS += -DTTC_USART1_TX=tgp_d8   # RS232 connector 9-pin female TX-pin
COMPILE_OPTS += -DTTC_USART1_RX=tgp_d9   # RS232 connector 9-pin female RX-pin
COMPILE_OPTS += -DTTC_USART1_RTS=tgp_d12 # RS232 connector 9-pin female RTS-pin
COMPILE_OPTS += -DTTC_USART1_CTS=tgp_d11 # RS232 connector 9-pin female CTS-pin
  
COMPILE_OPTS += -DTTC_USART2=INDEX_USART2       # USART connected to 10-pin male header connector
COMPILE_OPTS += -DTTC_USART2_TX=tgp_d5   # RS232 on 10-pin male header connector TX-Pin
COMPILE_OPTS += -DTTC_USART2_RX=tgp_d6   # RS232 on 10-pin male header connector RX-Pin




#{ parallel ports (8 or 16 consecutive pins in same bank)
#
# Every 8 available, consecutive bits in same GPIO bank can form an 8-bit wide port.
# A 16-bit wide port requires 16 available, consecutive bits (obviously).
# A pin is available if it not driven externally or used for something else.
# Check circuit of your board for details.

# 8-bit wide parallel port #1 starting at pin 0 of bank E
COMPILE_OPTS += -DTTC_PARALLEL08_1_BANK=TTC_GPIO_BANK_E
COMPILE_OPTS += -DTTC_PARALLEL08_1_FIRSTPIN=0

# 8-bit wide parallel port #2 starting at pin 8 of bank D (overlaps with USART2, USART3)
COMPILE_OPTS += -DTTC_PARALLEL08_2_BANK=TTC_GPIO_BANK_D
COMPILE_OPTS += -DTTC_PARALLEL08_2_FIRSTPIN=8


#}parallel ports
#{ SPI
#  # SPI connected to the radio
COMPILE_OPTS += -DTTC_SPI1=ttc_device_1         
COMPILE_OPTS += -DTTC_SPI1_MOSI=tgp_a7
COMPILE_OPTS += -DTTC_SPI1_MISO=tgp_a6
COMPILE_OPTS += -DTTC_SPI1_SCK=tgp_a5
COMPILE_OPTS += -DTTC_SPI1_NSS=tgp_a4


# SPI connected to LCD
COMPILE_OPTS += -DTTC_SPI2=ttc_device_2         
COMPILE_OPTS += -DTTC_SPI2_MOSI=tgp_b15 # UEXT pin 8
COMPILE_OPTS += -DTTC_SPI2_MISO=tgp_b14 # UEXT pin 7
COMPILE_OPTS += -DTTC_SPI2_SCK=tgp_b13  # UEXT pin 9
COMPILE_OPTS += -DTTC_SPI2_NSS=tgp_b12  # UEXT pin 10 (SoftwareNSS)
#}SPI
#{ I2C1 - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C1_SDA=tgp_b9    # UEXT-6
  COMPILE_OPTS += -DTTC_I2C1_SCL=tgp_b8    # UEXT-5
  COMPILE_OPTS += -DTTC_I2C1_SMBAL=tgp_b5  # 
#}I2C1

END_OF_MAKEFILE
  createExtensionMakefileTail ${NameBoard} #}
  File="${Dir_Extensions}activate.${NameBoard}.sh" #{
  createActivateScriptHead $NameBoard ${Dir_Extensions} $ScriptName "Protoboard P105"
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$NameBoard \${Dir_ExtensionsActive}/makefile.$NameBoard '' QUIET
  
  #X activate.200_cpu_stm32f10x.sh QUIET "\$ScriptName"
  
  # ACTIVATE_SECTION_C activate initialization source-code for this extension 
  createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET
  
  enableFeature 450_cpu_stm32f1xx

END_OF_ACTIVATE
#}
  createActivateScriptTail $NameBoard ${Dir_Extensions}
  #}

#}
  echo "Installed successfully: $Install_Dir"

cd ..
rmdir 2>/dev/null $Install_Dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
