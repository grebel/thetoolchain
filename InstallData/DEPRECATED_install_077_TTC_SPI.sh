#!/bin/bash
#
#
#  Install script for generic SPI support 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Initial Script written by Gregor Rebel 2010-2012.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

StartPath="`pwd`"
INSTALLPATH="500_ttc_spi"
dir "$INSTALLPATH"
cd "$INSTALLPATH"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

  Name="$INSTALLPATH"
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += ttc_spi.o

ifdef TARGET_ARCHITECTURE_STM32F1xx
  MAIN_OBJS += stm32_spi.o
endif

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ "$0" "Architecture independent support for Serial Peripheral Interface (SPI)" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_Additionals/${Name}_*

ArchitectureSupportAvailable=""

# check if stm32fxxx-support is available
if [ -e extensions/activate.250_stm_std_peripherals__gpio.sh ]; then
  activate.250_stm_std_peripherals__spi.sh QUIET \"\$0\"
  ArchitectureSupportAvailable="1"
fi

if [ "\$ArchitectureSupportAvailable" == "1" ]; then
  activate.500_ttc_gpio.sh QUIET \"\$0\"
  activate.500_ttc_mutex.sh  QUIET \"\$0\"

  # create links into extensions.active/
  createLink "\$Dir_Extensions/makefile.$Name" "\$Dir_ExtensionsActive/makefile.$Name" '' QUIET
else
  echo "\$0 - ERROR: No support for current architecture available!"
fi

END_OF_ACTIVATE
  createActivateScriptTail $Name ../extensions/
  #}

  echo "Installed successfully: $Name"

  for BaseName in example_spi_master example_spi_slave; do
    Name="600_$BaseName"
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "example_spi.h"
  example_spi_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    

    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += example_spi.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ../extensions/ "$0" "Example of how to use architecture independent Serial Peripheral Interface (SPI) support" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/makefile.600_example_*


# create links into extensions.active/
createLink "\$Dir_Extensions/makefile.$Name" "\$Dir_ExtensionsActive/makefile.$Name" '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

activate.500_ttc_spi.sh QUIET

END_OF_ACTIVATE
    createActivateScriptTail $Name ../extensions/
    #}
    echo "Installed successfully: $Name"
  done
cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$StartPath"
applyTargetUserAndGroup "$INSTALLPATH"

exit 0
