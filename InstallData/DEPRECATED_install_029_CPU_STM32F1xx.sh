#!/bin/bash

#
#  Install script for Microcontroller STM32 F1xx 
#  with ARM Cortex M3 core + Std Peripherals Library for STM32F1xx.
#
#  Script written by Gregor Rebel 2012-2015
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "200_cpu_stm32f10x" "$1" # sets $Start_Path, $Install_Dir, $Install_Path

findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
  if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.eecs.umich.edu/~prabal/teaching/eecs373-f11/readings/                             ARMv7-M_ARM.pdf            ARMv7-M_Architecture_Reference_Manual.pdf                                    uC/ARM7
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/         CD00298482.pdf             Advanced_developers_guide_for-STM32F103xx-STM32F100xx.pdf                    uC/STM32F1xx
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/REFERENCE_MANUAL/    CD00246267.pdf             STM32_RM0041_ReferenceManual_STM32F100xx.pdf                                 uC/STM32F1xx
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/REFERENCE_MANUAL/    CD00171190.pdf             STM32_RM0008_STM32F101xx_STM32F102xx_STM32F103xx_STM32F105xx_STM32F107xx.pdf uC/STM32F1xx
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/    CD00209826.pdf             STM32_AN2824_STM32F10xxx_I2C_optimized_examples.pdf                          uC/STM32F1xx
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/DATASHEET/           CD00220364.pdf             STM32_STM32F105xx_STM32F107xx_Connectivity_line.pdf                          uC/STM32F1xx
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/         CD00251227.pdf             UM0819_EthernetPhy_ST802RT1.pdf                                              uC/STM32F1xx
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/         CD00298482.pdf             UM1053_STM32_Motor_Control_Software_Development_Kit.pdf                      uC/STM32Fxxx
  getDocumentation http://www.cs.indiana.edu/~geobrown/                                                         book.pdf                   Discovering_the_STM32_Microcontroller.pdf                                    uC/STM32F1xx
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/programming_manual/ CD00283419.pdf             PM0075_STM32F10xxx_Flash_Memory_Programming.pdf                              uC/STM32F1xx
  getDocumentation http://www.diller-technologies.de/                                                           stm32.html                 diller-stm32.html                                                            uC/STM32xxxx
  getDocumentation http://www.hitex.com/fileadmin/pdf/insiders-guides/stm32/                                    isg-stm32-v18d-scr.pdf     The_Insiders_Guide_to_the_STM32_Microcontroller_Hitex.pdf                    uC/STM32xxxx
  getDocumentation http://www.st.com/web/en/resource/technical/document/datasheet/                              CD00191185.pdf             STM32F103x-Datasheet.pdf                                                     uC/STM32F1xx
  getDocumentation http://www.cs.indiana.edu/~geobrown/                                                         book.pdf                   Discovering_the_STM32_Microcontroller.pdf                                    uC/STM32xxxx

  TargetDir="Example_PeripheralInitialization"
  if [ ! -d "${TargetDir}" ]; then
    getFile http://www.keil.com/download/files/                                                          stm32_exti.zip         ${TargetDir}.zip
    unzip ${TargetDir}.zip
    mv STM32_EXTI/ ${TargetDir}
  fi
  addDocumentationFolder ${TargetDir} ${TargetDir} uC/STM32F1xx

  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
  if [ ! -e OK.Install ];       then #{
    if [ ! -e OK.Packages ]; then #{
      installPackage libgmp3-dev
      installPackage libmpfr-dev
      installPackage xchm
      installPackage chmlib
      installPackage chmsee
      installPackage kchmviewer
      touch OK.Packages
    fi #}
    if [ -e OK.Packages ]; then #{
      Error=""
      
      Archive="stm32f10x_stdperiph_lib.zip"
      # getFile http://www.st.com/stonline/products/support/micro/files/ $Archive
      getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ $Archive
      
      # find name of only directory
      # std_peripherals_library/STM32F10x_StdPeriph_Lib_V3.4.0/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h
      
      if [ ! -e $Archive ]; then
        Error="Cannot download $Archive!"
      fi
      unZIP STM32F10x_StdPeriph_Lib $Archive || mv -f $Archive ${Archive}_bad
      rm current 2>/dev/null
      LibraryDir=`find ./  -maxdepth 1 -mindepth 1 -type d -name "STM32F10x*" -print`
      createLink $LibraryDir current
      cd current
      #X dir ../../../Documentation/STM
      #X mv *.chm ../../../Documentation/STM/
      for File in `ls *.chm`; do
        addDocumentationFile "$File" uC/STM32F1xx
      done

      
      File="Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h"
      if [ -e ${File}_orig ]; then
          mv -f ${File}_orig $File 
      fi
      mv -f $File ${File}_orig
      
      echo "  creating macro assert_param in $File"
      Note="  Note: Manually remove assert_param from your stm32f10x_conf.h !"
      echo $Note
      addLine ../Notes.txt $Note
      addLine ../Notes.txt "StdPeripheralLibrary-> http://www.mikrocontroller.net/articles/STM32F10x_Standard_Peripherals_Library"
      
      DestFolder="Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x"
      if [ ! -e $DestFolder/system_stm32f10x.c ]; then
        # system_stm32f10x.c is required to be found by the compiler so we put it aside its header file
        cp -v Project/STM32F10x_StdPeriph_Template/system_stm32f10x.c $DestFolder
        addLine ../Notes.txt "system_stm32f10x.c moved beside its header file (-> https://my.st.com/public/STe2ecommunities/mcu/Lists/ARM%20CortexM3%20STM32/Flat.aspx?RootFolder=%2Fpublic%2FSTe2ecommunities%2Fmcu%2FLists%2FARM%20CortexM3%20STM32%2FMissing%20file%20in%20new%203.4.0%20libraries%20system_stm32f10x.c&FolderCTID=0x01200200770978C69A1141439FE559EB459D758000626BE2B829C32145B9EB5739142DC17E&currentviews=320)"
      fi
    
      cat >script.sed <<"END_OF_TEXT" #{
/#include <stdint.h>/a\
#ifdef  USE_FULL_ASSERT\n
\n
// This assert_param macro has been copied here from stm32f10x_conf.h to\n
// circumvent gcc compiler errors.\n
\n
/**\n
  * @brief  The assert_param macro is used for function's parameters check.\n
  * @param  expr: If expr is false, it calls assert_failed function\n
  *   which reports the name of the source file and the source\n
  *   line number of the call that failed.\n 
  *   If expr is true, it returns no value.\n
  * @retval None\n
  */\n
  #define assert_param(expr) ((expr) ? (void)0 : assert_failed((uint8_t *)__FILE__, __LINE__))\n
  void assert_failed(uint8_t* file, uint32_t line);\n
#else\n
  #define assert_param(expr) ((void)0)\n
#endif /* USE_FULL_ASSERT */\n

END_OF_TEXT
#}
  
      # concatenate all lines which contain \n (sed requires this)
      awk '/\\n/{printf "%s",$0;next}{print}' script.sed >script2.sed
      
      # insert lines after "#include <stdint.h>"
      sed -f script2.sed ${File}_orig >$File
    
      cd ..
    
      if [ "$Error" == "" ]; then
        touch OK.Install
      else
        echo "$0 - ERROR: $ERROR!"
        exit 10
      fi
    fi #}
  fi #}
  if [   -e OK.Install ];       then #{
    Architecture="stm32f10x"
    Name="${Install_Dir}"
    
    ConfigFile="../999_open_ocd/share/openocd/scripts/target/stm32f10x_flash.cfg"
    if [ ! -e "_$ConfigFile" ]; then
      echo "creating cfg-file '`pwd`/$ConfigFile'.."
      cat <<'END_OF_CONFIG' >$ConfigFile #{
# openocd flash script for stm32f10x


# finish configuration stage + proceed to run stage
init

# hardware reset processor with halt
reset halt

# check target state
poll

# list all found falsh banks
flash banks

# identify the flash
flash probe 0

# unprotect flash for writing
#DEPRECATED flash protect_check 0 0

# erasing all flash
#DEPRECATED stm32x mass_erase 0
stm32f1x mass_erase 0

# write file to flash number 0
flash write_bank 0 main.bin 0

# hardware processor reset with run
reset run

#shutdown openocd daemon
shutdown

END_OF_CONFIG
#}
    else
      echo "cfg-file already exists: '`pwd`/$ConfigFile'.."
    fi
    
    createExtensionMakefileHead ${Name} #{         create makefile for cpu
    File="${Dir_Extensions}makefile.${Name}" 
    cat <<END_OF_MAKEFILE >>$File 

TARGET_ARCHITECTURE_STM32F1xx=1
TARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_STM32F1xx
COMPILE_OPTS += -DTARGET_DATA_WIDTH=32

# architecture specific support code
INCLUDE_DIRS += -Ittc-lib/stm32/ -Ittc-lib/stm32f1/
vpath %.c ttc-lib/stm32/ ttc-lib/stm32f1/

MAIN_OBJS += system_stm32f10x.o stm32_basic.o

# define linker script to use
LDFLAGS += -Tconfigs/memory_stm32f1xx.ld 

#{ Startup codes for all members of STM32 device family

startup_stm32f10x_md.o:    startup_stm32f10x_md.s
startup_stm32f10x_cl.o:    startup_stm32f10x_cl.s
startup_stm32f10x_hd.o:    startup_stm32f10x_hd.s
startup_stm32f10x_hd_vl.o: startup_stm32f10x_hd_vl.s
startup_stm32f10x_ld.o:    startup_stm32f10x_ld.s
startup_stm32f10x_ld_vl.o: startup_stm32f10x_ld_vl.s
startup_stm32f10x_md_vl.o: startup_stm32f10x_md_vl.s
startup_stm32f10x_xl.o:    startup_stm32f10x_xl.s

#X	@ echo ".assembling"
#X	\$(AS) \$(ASFLAGS) \$^

#}
#{ define uController to use (required to include stm32f10x.h) ---------------

# activate only one uC!
# - Low-density devices are STM32F101xx, STM32F102xx and STM32F103xx microcontrollers
#   where the Flash memory density ranges between 16 and 32 Kbytes.
# - Low-density value line devices are STM32F100xx microcontrollers where the Flash
#   memory density ranges between 16 and 32 Kbytes.
# - Medium-density devices are STM32F101xx, STM32F102xx and STM32F103xx microcontrollers
#   where the Flash memory density ranges between 64 and 128 Kbytes.
# - Medium-density value line devices are STM32F100xx microcontrollers where the
#   Flash memory density ranges between 64 and 128 Kbytes.
# - High-density devices are STM32F101xx and STM32F103xx microcontrollers where
#   the Flash memory density ranges between 256 and 512 Kbytes.
# - High-density value line devices are STM32F100xx microcontrollers where the
#   Flash memory density ranges between 256 and 512 Kbytes.
# - XL-density devices are STM32F101xx and STM32F103xx microcontrollers where
#   the Flash memory density ranges between 512 and 1024 Kbytes.
# - Connectivity line devices are STM32F105xx and STM32F107xx microcontrollers.
#
# Examples for some prototype boards
# Olimex STM32-P107  DSTM32F10X_HD
# Olimex STM32-H107  DSTM32F10X_HD
# Olimex STM32-LCD   DSTM32F10X_CL
#
#STM32F10X_LD   =1       # Low density devices
#STM32F10X_LD_VL=1       # Low density Value Line devices
#STM32F10X_MD   =1       # Medium density devices
#STM32F10X_MD_VL=1       # Medium density Value Line devices
#STM32F10X_HD   =1       # High density devices
#STM32F10X_HD_VL=1       # High density value line devices
#STM32F10X_XL   =1       # XL-density devices
#STM32F10X_CL   =1       # Connectivity line devices

ifdef STM32F10X_LD
  ifdef uCONTROLLER
    ERROR: STM32F10X_LD=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_LD
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_LD
  COMPILE_OPTS += -DSTM32F10X_LD
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
#}TIMER
  MAIN_OBJS += startup_stm32f10x_ld.o
endif
ifdef STM32F10X_LD_VL
  ifdef uCONTROLLER
    ERROR: STM32F10X_LD_VL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_LD_VL
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_LD_VL
  COMPILE_OPTS += -DSTM32F10X_LD_VL
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
#}TIMER
  MAIN_OBJS += startup_stm32f10x_ld_vl.o
endif
ifdef STM32F10X_MD
  ifdef uCONTROLLER
    ERROR: STM32F10X_MD=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_MD
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_MD
  COMPILE_OPTS += -DSTM32F10X_MD
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
#}TIMER
  MAIN_OBJS += startup_stm32f10x_cl.o
endif
ifdef STM32F10X_MD_VL
  ifdef uCONTROLLER
    ERROR: STM32F10X_MD_VL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_MD_VL
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_MD_VL
  COMPILE_OPTS += -DSTM32F10X_MD_VL
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
#}TIMER
  MAIN_OBJS += startup_stm32f10x_md.o
endif
ifdef STM32F10X_HD
  ifdef uCONTROLLER
    ERROR: STM32F10X_HD=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_HD
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_HD
  COMPILE_OPTS += -DSTM32F10X_HD
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
#}TIMER
  MAIN_OBJS += startup_stm32f10x_hd.o
endif
ifdef STM32F10X_HD_VL
  ifdef uCONTROLLER
    ERROR: STM32F10X_HD_VL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_HD_VL
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_HD_VL
  COMPILE_OPTS += -DSTM32F10X_HD_VL
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
#}TIMER
  MAIN_OBJS += startup_stm32f10x_hd_vl.o
endif
ifdef STM32F10X_XL
  ifdef uCONTROLLER
    ERROR: STM32F10X_XL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_XL
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_XL
  COMPILE_OPTS += -DSTM32F10X_XL
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
  COMPILE_OPTS += -DTTC_TIMER5=9               # physical device index
  COMPILE_OPTS += -DTTC_TIMER6=10               # physical device index
  COMPILE_OPTS += -DTTC_TIMER7=11               # physical device index
  COMPILE_OPTS += -DTTC_TIMER8=12               # physical device index
  COMPILE_OPTS += -DTTC_TIMER9=13               # physical device index
  COMPILE_OPTS += -DTTC_TIMER10=14               # physical device index
  #}TIMER
  MAIN_OBJS += startup_stm32f10x_xl.o
endif
ifdef STM32F10X_CL
  ifdef uCONTROLLER
    ERROR: STM32F10X_CL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_CL
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_CL
  COMPILE_OPTS += -DSTM32F10X_CL
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
#}TIMER
  MAIN_OBJS += startup_stm32f10x_cl.o
endif

#}

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} $0 "CPU STM32F10x" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.200_cpu_*

activate.190_cpu_cortexm3.sh QUIET \"\$0\"

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET


# activate corresponding std_peripheral_library
activate.250_CPAL_STM32F10x_StdPeriph_Driver.sh QUIET \"\$0\"

# create link to target script being required by openocd to initialize target cpu
cd \$Dir_Configs
createLink ../additionals/999_open_ocd/target/stm32f1x.cfg openocd_target.cfg
createLink ../additionals/999_open_ocd/target/stm32f10x_flash.cfg openocd_flash.cfg
cd ..

#X activate.500_ttc_register.sh                    QUIET \"\$0\"

END_OF_ACTIVATE
#}
    createActivateScriptTail $Name ${Dir_Extensions}
    #}

    if [ "0" == "1" ]; then #{ DISABLED (moved to CPAL): std_peripheral_library_f10x
    
    LibPrefix="250_stm_std_peripherals_"
    LibName="${LibPrefix}f10x"
    createExtensionMakefileHead ${LibName} #{      create makefile for std_peripheral_library
    File="${Dir_Extensions}makefile.${LibName}" 
    cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DEXTENSION_250_stm_std_peripherals=1

INCLUDE_DIRS += -I additionals/250_stm_std_peripherals/STM32F10x_StdPeriph_Driver/inc/ \\
                -I additionals/250_stm_std_peripherals/CMSIS/CM3/DeviceSupport/ST/STM32F10x/ \\
                -I additionals/250_stm_std_peripherals/CMSIS/CM3/CoreSupport/

vpath %.c additionals/250_stm_std_peripherals/STM32F10x_StdPeriph_Driver/src/ \\
          additionals/250_stm_std_peripherals/CMSIS/CM3/DeviceSupport/ST/STM32F10x/

vpath %.s additionals/250_stm_std_peripherals/CMSIS/CM3/DeviceSupport/ST/STM32F10x/startup/gcc_ride7

END_OF_MAKEFILE
    createExtensionMakefileTail ${LibName} #}
    createActivateScriptHead $LibName ${Dir_Extensions} $0 "Standard Peripheral Library for CPU STM32F10x" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${LibPrefix}*

OldPWD="\`pwd\`"
cd "\${Dir_Extensions}/"
rm 2>/dev/null makefile.${LibPrefix}_*
rm 2>/dev/null activate.${LibPrefix}_*
ln -sv  $Architecture/* .
cd "\$OldPWD"

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$LibName \${Dir_ExtensionsActive}/makefile.$LibName '' QUIET

# we must create a new link in additionals
if [ -d "\$Dir_Additionals" ]; then
  cd "\$Dir_Additionals"
  rm 2>/dev/null 250_stm_std_peripherals
  createLink 250_STM32F10x_StdPeriph_Driver  \$Dir_Additionals/250_stm_std_peripherals
  cd ..
else
  echo "\$0 - ERROR: Cannot find directory additionals/ (\`pwd\`/\$Dir_Additionals)!"
fi

END_OF_ACTIVATE
#}
    createActivateScriptTail $LibName ${Dir_Extensions}
    #}

    BaseName=$Name
    dir ${Dir_Extensions}$Architecture

    # create one activate-script per feature
    FilePrefix="stm32f10x"
    for Feature in `find current/Libraries/STM32F10x_StdPeriph_Driver/src/ -name "${FilePrefix}_*" -execdir echo -n "{} " \;` ; do #{
      Feature=`perl -e "print substr('$Feature', 12, -2);"`

      FeatureName="${LibPrefix}_${Feature}"
      #echo "FeatureName='$FeatureName'" #D
      createExtensionMakefileHead ${FeatureName} ${Dir_Extensions}${Architecture}/ #{
      cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # set by createExtensionMakefileHead() 

MAIN_OBJS += ${Architecture}_${Feature}.o

END_OF_MAKEFILE
      createExtensionMakefileTail ${FeatureName} ${Dir_Extensions}${Architecture}/ #}
      createActivateScriptHead $FeatureName ${Dir_Extensions} $0 "append C-source from standar peripheral library for compilation: ${FeatureName}.c" #{
      cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# first check if std_peripherals_library has been activated
if [ -e \${Dir_ExtensionsActive}/makefile.250_CPAL_STM32F10x_StdPeriph_Driver ]; then

  # add createLinks for each link required for this extension
  createLink ${Dir_Extensions}makefile.$FeatureName \${Dir_ExtensionsActive}/makefile.$FeatureName '' QUIET

fi

END_OF_ACTIVATE
#}
      createActivateScriptTail $FeatureName ${Dir_Extensions}
      #}
    done #}
    
    fi #}std_peripheral_library_f10x
    echo "Installed successfully: $Install_Dir"
  fi #}
  cd ..
  rmdir 2>/dev/null $Install_Dir  # removes only empty install dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
