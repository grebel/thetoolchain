 #!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2017.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="100"
EXTENSION_SHORT="stm32f3_nucleo"
EXTENSION_PREFIX="board"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/ DM00105823.pdf Board_STM32F3_Nucleo.pdf   Boards/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

Dir=`pwd`

  Name="${Install_Dir}" #{
  createExtensionMakefileHead ${Name} #{
  File="${Dir_Extensions}makefile.${Name}" 
  cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DTTC_BOARD=$Name
BOARD=$Name

#X STM32F30X  =1

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f302r8t6 = 1

#{ individual port pins

COMPILE_OPTS += -DTTC_LED2=tgp_b6 

#COMPILE_OPTS += -DTTC_SWITCH1=tgp_c13    // press button switch 1 (named USER)
#COMPILE_OPTS += -DTTC_SWITCH1_TYPE=tgm_input_floating # -> ttc_gpio_types.h
#COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=1

#}

#{ USART1  - define universal synchronous asynchronous receiver transmitter #1
  COMPILE_OPTS += -DTTC_USART1=INDEX_USART1       # 
  COMPILE_OPTS += -DTTC_USART1_TX=tgp_a9   # EXT1-4
  COMPILE_OPTS += -DTTC_USART1_RX=tgp_a10  # EXT1-7
  
  # Pins for RTS,CTS are also used for USBDM,USBDP!
  #COMPILE_OPTS += -DTTC_USART1_CTS=tgp_a11 # EXT1-1
  #COMPILE_OPTS += -DTTC_USART1_RTS=tgp_a12 # EXT1-3
#}USART1
#{ USART2  - define universal synchronous asynchronous receiver transmitter #2
  COMPILE_OPTS += -DTTC_USART2=INDEX_USART2       # RS232 connector connected USART
  COMPILE_OPTS += -DTTC_USART2_TX=tgp_a2   # EXT2-7
  COMPILE_OPTS += -DTTC_USART2_RX=tgp_a3   # EXT2-10
  COMPILE_OPTS += -DTTC_USART2_CTS=tgp_a0  # EXT2-4
  COMPILE_OPTS += -DTTC_USART2_RTS=tgp_a1  # EXT2-8
#}USART2
#{ SPI1    - define serial peripheral interface #1
  COMPILE_OPTS += -DTTC_SPI1=ttc_device_1          # 
  COMPILE_OPTS += -DTTC_SPI1_MOSI=tgp_a7  # 
  COMPILE_OPTS += -DTTC_SPI1_MISO=tgp_a6  # 
  COMPILE_OPTS += -DTTC_SPI1_SCK=tgp_a5   # 
  COMPILE_OPTS += -DTTC_SPI1_NSS=tgp_a4   # 
#}SPI1
#{ I2C1    - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C1_SDA=tgp_b7    # EXT1-15
  COMPILE_OPTS += -DTTC_I2C1_SCL=tgp_b6    # EXT1-13
  COMPILE_OPTS += -DTTC_I2C1_SMBAL=tgp_b5  # EXT1-12
#}I2C1
#{ I2C2    - define inter-integrated circuit interface #2
  COMPILE_OPTS += -DTTC_I2C2=ttc_device_2           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C2_SDA=tgp_b11   # EXT2-15
  COMPILE_OPTS += -DTTC_I2C2_SCL=tgp_b10   # EXT2-14
#}I2C2

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "STM32F0 Discovery Board from ST"  #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
  
  #X activate.200_cpu_stm32f30x.sh QUIET "\$ScriptName"

  enableFeature 450_cpu_stm32f3xx
END_OF_ACTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

#}
  echo "Installed successfully: $Install_Dir"

cd ..
rmdir 2>/dev/null $Install_Dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
