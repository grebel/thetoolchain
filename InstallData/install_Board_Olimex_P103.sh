#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="100"
EXTENSION_SHORT="olimex_p103"
EXTENSION_PREFIX="board"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
  Dir=`pwd`
  if [ ! -e OK.Documentation ]; then #{
    Get_Fails=""

    getDocumentation http://olimex.com/dev/pdf/ARM/ST/                            STM32-P103.pdf          Board_STM32-P103_RevA.pdf Boards/
    getDocumentation https://www.olimex.com/Products/ARM/ST/STM32-P405/resources/ STM32-P103_P405_sch.pdf Board_STM32-P103_RevD.pdf Boards/

    if [ "$Get_Fails" == "" ]; then
      touch OK.Documentation
    else
      echo "$ScriptName - ERROR: missing files: $Get_Fails"
    fi
  fi #}

  Name="${Install_Dir}_RevA" #{
  createExtensionMakefileHead ${Name} #{
  File="${Dir_Extensions}makefile.${Name}" 
  cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DTTC_BOARD=$Name
BOARD=$Name

#X STM32F10X_MD  =1
#X COMPILE_OPTS += -DSTM32F10X_MD

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f103rbt6 = 1 

# system clock setup (-> ttc_sysclock_types.h)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=8000000
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL=32768

# Define port pins
# reference: http://olimex.com/dev/images/ARM/ST/STM32-P103-sch.gif

COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c12
COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_c12

COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_a0    # press button switch 1 (named WKUP)
COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=0

#{INDEX_ USART1  - define universal synchronous asynchronous receiver transmitter #1
  COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_a9    # UEXT-3 connector 10-pin
  COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_a10   # UEXT-4 connector 10-pin
  COMPILE_OPTS += -DTTC_USART1_CTS=E_ttc_gpio_pin_a11  
  COMPILE_OPTS += -DTTC_USART1_RTS=E_ttc_gpio_pin_a12  
  COMPILE_OPTS += -DTTC_USART1=INDEX_USART1       # UEXT connector connected USART
#}USART1
#{ USART2  - define universal synchronous asynchronous receiver transmitter #2
  COMPILE_OPTS += -DTTC_USART2_TX=E_ttc_gpio_pin_a2   # RS232 connector 9-pin female TX-pin
  COMPILE_OPTS += -DTTC_USART2_RX=E_ttc_gpio_pin_a3   # RS232 connector 9-pin female RX-pin
  COMPILE_OPTS += -DTTC_USART2_RTS=E_ttc_gpio_pin_a1  # RS232 connector 9-pin female RTS-pin
  COMPILE_OPTS += -DTTC_USART2_CTS=E_ttc_gpio_pin_a0  # RS232 connector 9-pin female CTS-pin
  COMPILE_OPTS += -DTTC_USART2=INDEX_USART2 # RS232 on 9-pin male header connector connected USART
#}USART2
#{ SPI1    - define serial peripheral interface #1
  COMPILE_OPTS += -DTTC_SPI1=ttc_device_1          # 
  COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a7  # UEXT-8 
  COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a6  # UEXT-7 
  COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_a5   # UEXT-9
  COMPILE_OPTS += -DTTC_SPI1_NSS=E_ttc_gpio_pin_a4   # UEXT-10
#}SPI1
#{ SPI2    - define serial peripheral interface #2
  COMPILE_OPTS += -DTTC_SPI2=ttc_device_2          # 
  COMPILE_OPTS += -DTTC_SPI2_MOSI=E_ttc_gpio_pin_b15  # 
  COMPILE_OPTS += -DTTC_SPI2_MISO=E_ttc_gpio_pin_b14  #  
  COMPILE_OPTS += -DTTC_SPI2_SCK=E_ttc_gpio_pin_b13   # 
  COMPILE_OPTS += -DTTC_SPI2_NSS=E_ttc_gpio_pin_b12   # 
#}SPI1
#{ I2C1    - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b7    # UEXT-6
  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b6    # UEXT-7
  COMPILE_OPTS += -DTTC_I2C1_SMBAL=E_ttc_gpio_pin_b5  # PB5 
#}I2C1
#{ I2C2    - define inter-integrated circuit interface #2
  COMPILE_OPTS += -DTTC_I2C2=ttc_device_2           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C2_SDA=E_ttc_gpio_pin_b11   # PB11
  COMPILE_OPTS += -DTTC_I2C2_SCL=E_ttc_gpio_pin_b10   # PB10
  COMPILE_OPTS += -DTTC_I2C2_SMBAL=E_ttc_gpio_pin_b12 # PB12
#}I2C2
#{ define Controller Area Network (CAN) devices
  COMPILE_OPTS += -DTTC_CAN1=0               # physical device index
  COMPILE_OPTS += -DTTC_CAN1_PIN_RX=E_ttc_gpio_pin_b8  # pin used for receive
  COMPILE_OPTS += -DTTC_CAN1_PIN_TX=E_ttc_gpio_pin_b9  # pin used for transmit
#}CAN

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Protoboard P103 revision A from Olimex -> http://olimex.com/dev/stm32-p103.html"  #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
  
  #X activate.200_cpu_stm32f10x.sh QUIET "\$ScriptName"

  enableFeature 450_cpu_stm32f1xx

END_OF_ACTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
#}

  Name="${Install_Dir}_RevD" #{
  createExtensionMakefileHead ${Name} #{
  File="${Dir_Extensions}makefile.${Name}" 
  cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DTTC_BOARD=$Name
BOARD=$Name

# define microcontroller used on board
#X STM32F10X_MD  =1
#X COMPILE_OPTS += -DSTM32F10X_MD

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f103rbt6 = 1

# system clock setup (-> ttc_sysclock_types.h)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=8000000
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL=32768

# Define port pins
# reference: http://olimex.com/dev/images/ARM/ST/STM32-P103-sch.gif

COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c12
COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_c12

COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_a0    # press button switch 1 (named WKUP)
COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=0


#{INDEX_ USART1  - define universal synchronous asynchronous receiver transmitter #1
  COMPILE_OPTS += -DTTC_USART2_TX=E_ttc_gpio_pin_a9    # UEXT-3 connector 10-pin
  COMPILE_OPTS += -DTTC_USART2_RX=E_ttc_gpio_pin_a10   # UEXT-4 connector 10-pin
  COMPILE_OPTS += -DTTC_USART2_CTS=E_ttc_gpio_pin_a11  
  COMPILE_OPTS += -DTTC_USART2_RTS=E_ttc_gpio_pin_a12  
  COMPILE_OPTS += -DTTC_USART2=INDEX_USART1       # UEXT connector connected USART
#}USART1
#{ USART2  - define universal synchronous asynchronous receiver transmitter #2
  COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_a2   # RS232 connector 9-pin female TX-pin
  COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_a3   # RS232 connector 9-pin female RX-pin
  COMPILE_OPTS += -DTTC_USART1_RTS=E_ttc_gpio_pin_a1  # RS232 connector 9-pin female RTS-pin
  COMPILE_OPTS += -DTTC_USART1_CTS=E_ttc_gpio_pin_a0  # RS232 connector 9-pin female CTS-pin
  COMPILE_OPTS += -DTTC_USART1=INDEX_USART2 # RS232 on 9-pin male header connector connected USART
#}USART2
#{ SPI1    - define serial peripheral interface #1
  COMPILE_OPTS += -DTTC_SPI1=ttc_device_1          # 
  COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a7  # UEXT-8 
  COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a6  # UEXT-7 
  COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_a5   # UEXT-9
  COMPILE_OPTS += -DTTC_SPI1_NSS=E_ttc_gpio_pin_a4   # UEXT-10
#}SPI1
#{ SPI2    - define serial peripheral interface #2
  COMPILE_OPTS += -DTTC_SPI2=ttc_device_2          # 
  COMPILE_OPTS += -DTTC_SPI2_MOSI=E_ttc_gpio_pin_b15  # 
  COMPILE_OPTS += -DTTC_SPI2_MISO=E_ttc_gpio_pin_b14  #  
  COMPILE_OPTS += -DTTC_SPI2_SCK=E_ttc_gpio_pin_b13   # 
  COMPILE_OPTS += -DTTC_SPI2_NSS=E_ttc_gpio_pin_b12   # 
#}SPI1
#{ I2C1    - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b7    # UEXT-6
  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b6    # UEXT-7
  COMPILE_OPTS += -DTTC_I2C1_SMBAL=E_ttc_gpio_pin_b5  # PB5 
#}I2C1
#{ I2C2    - define inter-integrated circuit interface #2
  COMPILE_OPTS += -DTTC_I2C2=ttc_device_2           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C2_SDA=E_ttc_gpio_pin_b11   # PB11
  COMPILE_OPTS += -DTTC_I2C2_SCL=E_ttc_gpio_pin_b10   # PB10
  COMPILE_OPTS += -DTTC_I2C2_SMBAL=E_ttc_gpio_pin_b12 # PB12
#}I2C2
#{ define Controller Area Network (CAN) devices
  COMPILE_OPTS += -DTTC_CAN1=0               # physical device index
  COMPILE_OPTS += -DTTC_CAN1_PIN_RX=E_ttc_gpio_pin_b8  # pin used for receive
  COMPILE_OPTS += -DTTC_CAN1_PIN_TX=E_ttc_gpio_pin_b9  # pin used for transmit
#}CAN

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Protoboard P103 revision D from Olimex -> http://olimex.com/dev/stm32-p103.html"  #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
  
  #X activate.200_cpu_stm32f10x.sh QUIET "\$ScriptName"

  enableFeature 450_cpu_stm32f1xx

END_OF_ACTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

#}
  echo "Installed successfully: $Install_Dir"

cd ..
rmdir 2>/dev/null $Install_Dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
