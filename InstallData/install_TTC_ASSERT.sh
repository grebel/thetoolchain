#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a set of High- and Low-Level Drivers
#  for assert devices.
#
#  Created from template _install_NN_TTC_DEVICE.sh revision 31 at 20161107 19:16:20 UTC
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: <AUTHOR>
#

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

StartPath="`pwd`"
RANK="500"
EXTENSION_SHORT="assert"
EXTENSION_PREFIX="ttc"
EXTENSION_NAME="${EXTENSION_PREFIX}_${EXTENSION_SHORT}"
setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# You may define a default low-level driver here. 
# It will be activated in case that no low-level driver has been enabled or activated before.
# 
#InsertDefaultLowLevelDriversAbove (only last one counts) (DO NOT DELETE THIS LINE!)
DefaultLowLevelDriver="" # remove this line to activate a default low-level driver

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$ScriptName: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$ScriptName - ERROR: Cannot find newest GCC version!"
#   exit 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then       #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe "which git" git
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then       #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ high-level driver for assert
    Name="${Install_Dir}"  
  
    SourceName="${Name}" # one may prefix name with different number to define start position in ttc_extensions_active.c
    createExtensionSourcefileHead ${SourceName}    #{ (create initializer code)
    
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

    #include "ttc-lib/ttc_assert.h"
    ttc_assert_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${SourceName} #}    
    createExtensionMakefileHead ${Name}      #{ (create makefile)

    MainObjects=""    
    for Object in ttc_assert_interface ttc_assert assert_common; do # add object files for available C-sources 
      if [ "`ls ../../TTC-Library/interfaces/${Object}.c 2>/dev/null`" != "" ]; then # found in ttc-lib/intferfaces/
        MainObjects="$MainObjects ${Object}.o"
      fi
      if [ "`ls ../../TTC-Library/${Object}.c 2>/dev/null`" != "" ]; then # found in ttc-lib/
        MainObjects="$MainObjects ${Object}.o"
      fi
    done
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
INCLUDE_DIRS += -Ittc-lib/assert/

# additional directories to search for C-Sources
vpath %.c ttc-lib/assert/

# additional object files to be compiled
MAIN_OBJS += $MainObjects

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $Dir_Extensions "$ScriptName 2" "high-level assert driver" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/
#   \$File_LowLevelInstall  !="": user wants to run only this low level install script 

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#{ ACTIVATE_SECTION_B check if default low-level driver has to be enabled

if [ "$DefaultLowLevelDriver" != "" ]; then #{ enable default low-level driver if no low-level driver has been enabled
  LowLevelDriverEnabled=\`ls 2>/dev/null extensions.active/*450_${EXTENSION_SHORT}_*\`
  if [ "\$LowLevelDriverEnabled" == "" ]; then 
    echo "\$ScriptName: no low-level driver enabled: enabling default driver"
    enableFeature "$DefaultLowLevelDriver"
  fi
fi
if [ "\$File_LowLevelInstall" != "" ]; then
  \$File_LowLevelInstall QUIET "\$ScriptName"

  # extract driver name from script name
  LowLevelDriver=\`perl -e "print substr('\$File_LowLevelInstall', 9, -3);"\`
  
  #X #DISABLED echo "\$ScriptName - LowLevelDriver='\$LowLevelDriver'"
  DriverFound="1"

else
  for LowLevelActivate in \`ls \$Dir_Extensions/activate.450_assert_*\`; do
    
    # try to activate this low-level driver
    \$LowLevelActivate QUIET "\$ScriptName"
    
    # extract driver name from script name
    LowLevelDriver=\`perl -e "my \\\\\$P=rindex('\$LowLevelActivate', 'activate.')+9; print substr('\$LowLevelActivate', \\\\\$P, -3);"\`
    #DISABLED echo "\$ScriptName - LowLevelDriver='\$LowLevelDriver'"
    Makefile="\$Dir_ExtensionsActive/makefile.\$LowLevelDriver"
    if [ -e \$Makefile ]; then
      DriverFound="1"
    fi
  done
fi
#}

#}ACTIVATE_SECTION_B
#{ ACTIVATE_SECTION_C call install scripts of low-level drivers
if [ "\$File_LowLevelInstall" != "" ]; then
  \$File_LowLevelInstall QUIET "\$ScriptName"

  # extract driver name from script name
  LowLevelDriver=\`perl -e "print substr('\$File_LowLevelInstall', 9, -3);"\`
  
  DriverFound="1"

else
  LowLevelActivates="\`ls \$Dir_Extensions/activate.450_assert_*\`"
  if [ "\$LowLevelActivates" != "" ]; then # low-level activates exist: run them all
    for LowLevelActivate in \$LowLevelActivates; do
      
      # try to activate this low-level driver
      \$LowLevelActivate QUIET "\$ScriptName"
      
      # extract driver name from script name
      LowLevelDriver=\`perl -e "my \\\\\$P=rindex('\$LowLevelActivate', 'activate.')+9; print substr('\$LowLevelActivate', \\\\\$P, -3);"\`
      #DISABLED echo "\$ScriptName - LowLevelDriver='\$LowLevelDriver'"
      Makefile="\$Dir_ExtensionsActive/makefile.\$LowLevelDriver"
      if [ -e \$Makefile ]; then
        DriverFound="1"
      fi
    done
  else # no low-level activates available: enable high-level driver by default
    DriverFound="1"
  fi
fi
#}ACTIVATE_SECTION_C

if [ "\$DriverFound" != "" ]; then

  #{ ACTIVATE_SECTION_D create link to your makefile into extensions.active/ to add makefile to your project
  createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET
  
  #}ACTIVATE_SECTION_D
  #{ ACTIVATE_SECTION_E create link to your initialization source-code for this extension 
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

  #}ACTIVATE_SECTION_E
  #{ ACTIVATE_SECTION_F enable features provided by this extension
  #
  # Most low-level drivers require a certain file to exist inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.450_ethernet_ste101p -> allows activate.500_ethernet.sh to enable 450_ethernet_ste101p.
  # enableFeature NAME  # creates extensions.active/feature.NAME 
  
  # enableFeature 450_ethernet_ste101p

  #}ACTIVATE_SECTION_F
  #{ ACTIVATE_SECTION_G call other activate-scripts
  
  # activate.500_ttc_memory.sh QUIET "\$ScriptName"
  
  #}ACTIVATE_SECTION_G
else
  echo "\$ScriptName - ERROR: No low-level assert driver found: Skipping activation!"
fi

END_OF_ACTIVATE
    createActivateScriptTail $Name $Dir_Extensions

    #}
  
  fi #}high-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "1" ]; then #{ recommended: create regression test for your extension

    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "regression_${EXTENSION_NAME}.h"
regression_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $Dir_Extensions "$ScriptName 2" "compilation and implementation tests for $ScriptName" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}

#  activate high-level driver under test
activate.${Install_Dir}.sh QUIET "\$ScriptName"
Activated=\`ls 2>/dev/null extensions.active/*${Install_Dir}\`
if [ "\$Activated" != "" ]; then #{ high-level driver to test has been activated successfully: activate regression 
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 
  
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_E provide default features required to compile this example
  
    # Read Documentation/TheToolChain-Manual/chapter_Extensions.tml for a description of feature enabling.
    
    # enableFeature 450_math_software_float   # enable math driver (single precision)
    # enableFeature 450_math_software_double  # enable math driver (double precision)
  
  enableFeature 450_assert_none   # central self testing and error reporting
    #<INSERT_LOW_LEVEL_FEATURES_ABOVE> (DO NOT DELETE THIS LINE!)
  
  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  activate.${Install_Dir}.sh QUIET "\$ScriptName"
  
  #}
#}
else
  echo "\$ScriptName - ERROR: Driver ${Install_Dir} could not be activated. Check messages above to find out why!"
fi

END_OF_ACTIVATE
    createActivateScriptTail $Name $Dir_Extensions
    #}

  fi #}
  if [ "1" == "1" ]; then #{ recommended: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $Dir_Extensions "$ScriptName 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

#}
#{ ACTIVATE_SECTION_E provide default features required to compile this example

  # Read Documentation/TheToolChain-Manual/chapter_Extensions.tml for a description of feature enabling.  

  # we need at least one low-level driver to activate ttc_assert:

  enableFeature 450_assert_none   # central self testing and error reporting
#<INSERT_LOW_LEVEL_FEATURES_ABOVE> (DO NOT DELETE THIS LINE!)

#}
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$ScriptName" # activate our design under test
activate.500_ttc_memory.sh QUIET "\$ScriptName" # mandatory for most ttc_* extensions
activate.500_ttc_gpio.sh   QUIET "\$ScriptName" # nearly everyone likes GPIOs

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $Dir_Extensions
    #}

  fi #}
  if [ "1" == "1" ]; then #{ call install scripts of low-level drivers for assert
    cd "$Install_Path"
    cd ../low_level
    LLInstallDir=`pwd`
    LowLevelInstalls=`ls 2>/dev/null install_*_ASSERT_*.sh`
    echo "$LLInstallDir> found LowLevelInstalls: $LowLevelInstalls"
    for LowLevelInstall in $LowLevelInstalls
    do
      cd "$Install_Path"
      echo ""
      echo    "running $LLInstallDir/$LowLevelInstall ...  "
      echo -n "        "
      source $LLInstallDir/$LowLevelInstall
    done
    cd "$Install_Path"
  fi #}low-level driver

  cd "$Install_Path"
  echo "Installed successfully: $Install_Dir"
#}
else                               #{ Install failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$StartPath"
applyTargetUserAndGroup "$Install_Dir"

exit 0
