#!/bin/bash
#
#
#  Install script for generic USART support 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Initial Script written by Gregor Rebel 2010-2012.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

StartPath="`pwd`"
INSTALLPATH="500_ttc_usart"
dir "$INSTALLPATH"
cd "$INSTALLPATH"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

  Name="$INSTALLPATH"
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "../ttc_usart.h"
  ttc_usart_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += ttc_usart.o

ifdef TARGET_ARCHITECTURE_STM32F1xx
  MAIN_OBJS += stm32_usart.o
endif
ifdef TARGET_ARCHITECTURE_STM32L1xx
  MAIN_OBJS += stm32l1_usart.o 
endif
ifdef TARGET_ARCHITECTURE_STM32W1xx
  MAIN_OBJS += stm32w_usart.o 
endif

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ "$0" "Architecture independent support for Universal Synchronous Asynchronous Serial Receiver Transmitter (USART)" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_Additionals/${Name}_*

Architecture=""
if [ -e extensions/activate.250_stm_std_peripherals__usart.sh ]; then
  activate.250_stm_std_peripherals__usart.sh QUIET \"\$0\"
  Architecture="stm32"
fi

if [ "\$Architecture" != "" ]; then
  activate.500_ttc_interrupt.sh QUIET \"\$0\"
  activate.500_ttc_gpio.sh      QUIET \"\$0\"
  activate.500_ttc_queue.sh     QUIET \"\$0\"
  activate.500_ttc_mutex.sh     QUIET \"\$0\"
  activate.500_ttc_task.sh      QUIET \"\$0\"
  activate.500_ttc_string.sh    QUIET \"\$0\"

  # create links into extensions.active/
  createLink "\$Dir_Extensions/makefile.$Name" "\$Dir_ExtensionsActive/makefile.$Name" '' QUIET

  # ACTIVATE_SECTION_D activate initialization source-code for this extension 
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

else
  echo "$0 - ERROR: no supported architecture found!"
fi

END_OF_ACTIVATE
  createActivateScriptTail $Name ../extensions/
  #}

  echo "Installed successfully: $Name"

  Name="600_example_ttc_usart"
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "DEPRECATED_example_usart.h"
  example_usart_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += DEPRECATED_example_usart.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ "$0" "Example of how to use architecture independent USART support" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/makefile.600_example_*

activate.500_ttc_usart.sh QUIET \"\$0\"
activate.500_ttc_gpio.sh  QUIET \"\$0\"

# create links into extensions.active/
createLink "\$Dir_Extensions/makefile.$Name" "\$Dir_ExtensionsActive/makefile.$Name" '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ../extensions/
  #}
  echo "Installed successfully: $Name"

  if [ "1" == "1" ]; then #{ create regression test for your extension
  
    Name="${INSTALLPATH}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

    #include "regression_usart.h"
    regression_start_usart();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
INCLUDE_DIRS += -I regressions/

# additional directories to search for C-Sources
vpath %.c regressions/ 

# additional object files to be compiled
MAIN_OBJS +=  regression_usart.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ../extensions/ "$0 2" "regression test for TTC_USART implementation" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/makefile.${INSTALLPATH}_*

# ACTIVATE_SECTION_B create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
#addLine ../scripts/createLinks.sh "rm 2>/dev/null $INSTALLPATH; createLink \$Source/TheToolChain/InstallData/${INSTALLPATH}/ $INSTALLPATH"

# ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_E call other activate-scripts
# activate.500_ttc_memory.sh QUIET "\$0"
activate.500_ttc_usart.sh QUIET \"\$0\"
activate.500_ttc_gpio.sh  QUIET \"\$0\"

END_OF_ACTIVATE
    createActivateScriptTail $Name ../extensions/
    #}

  fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$StartPath"
applyTargetUserAndGroup "$INSTALLPATH"

exit 0
