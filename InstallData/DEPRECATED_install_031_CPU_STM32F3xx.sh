#!/bin/bash

#
#  Install script for Microcontroller STM32F30x 
#  with ARM Cortex M4 core.
#
#  Script written by Greg Knoll 2013, reused for STM32F3 by Patrick von Poblotzki
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "200_cpu_stm32f30x" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/        DM00093333.pdf STM32F302x6-Datasheet.pdf        uC/STM32F30x/
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/reference_manual/ DM00094349.pdf STM32F302x-Reference_manual.pdf  uC/STM32F30x/
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/      DM00068049.pdf STM32F302x-Std_Peripheral.pdf    uC/STM32F30x/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
  if [ ! -e OK.Install ]; then #{
    Error=""
      
    Archive="stm32f30x_dsp_stdperiph_lib.zip"
    getFile http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/firmware/ $Archive $Archive
    
    if [ ! -e $Archive ]; then
        Error="Cannot download $Archive!"
    fi
    
    unZIP STM32F30x_StdPeriph_Lib_V1.1.1 $Archive || mv -f $Archive ${Archive}_bad
    
    rm current 2>/dev/null
    
    LibraryDir=`find ./  -maxdepth 1 -mindepth 1 -type d -print`
    createLink $LibraryDir current
    cd current
    #X dir ../../../Documentation/STM
    #X mv *.chm ../../../Documentation/STM/
    for File in `ls *.chm`; do
      addDocumentationFile "$File" uC/STM32F30x
    done
    
    File="Libraries/CMSIS/Device/ST/STM32F30x/Include/stm32f30x.h"
    if [ -e ${File}_orig ]; then
          mv -f ${File}_orig $File 
    fi
    mv -f $File ${File}_orig
    
    echo "  May need to create macro assert_param in $File"
    Note="  Note: If this is done, manually remove assert_param from your stm32f30x_conf.h !"
      echo $Note
      addLine ../Notes.txt $Note
      addLine ../Notes.txt "StdPeripheralLibrary-> http://www.mikrocontroller.net/articles/STM32F10x_Standard_Peripherals_Library"
      
    
    DestFolder="Libraries/CMSIS/Device/ST/STM32F30x/Include"
    if [ ! -e $DestFolder/system_stm32f3xx.c ]; then
        # system_stm32f3xx.c is required to be found by the compiler so we put it aside its header file
        #cp -v Project/STM32F10x_StdPeriph_Template/system_stm32f30x.c $DestFolder
        addLine ../Notes.txt "system_stm32f30x.c NOT FOUND. Move beside its header file (-> https://my.st.com/public/STe2ecommunities/mcu/Lists/ARM%20CortexM3%20STM32/Flat.aspx?RootFolder=%2Fpublic%2FSTe2ecommunities%2Fmcu%2FLists%2FARM%20CortexM3%20STM32%2FMissing%20file%20in%20new%203.4.0%20libraries%20system_stm32f30x.c&FolderCTID=0x01200200770978C69A1141439FE559EB459D758000626BE2B829C32145B9EB5739142DC17E&currentviews=320)"
    fi
    
    
    cd ..
    
    #{Was install successful?
    if [ "$Error" == "" ]; then
        touch OK.Install
      else
        echo "$0 - ERROR: $ERROR!"
        exit 10
    fi
    #}------------------------
      
  fi #}END ! -e OK.Install
  if [   -e OK.Install ]; then #{
    Architecture="stm32f30x"
    Name="${Install_Dir}"
    
    #{Write config file for openocd
    ConfigFile="../999_open_ocd/share/openocd/scripts/target/stm32f3xx_flash.cfg"
    if [ ! -e "_$ConfigFile" ]; then
      echo "creating cfg-file '`pwd`/$ConfigFile'.."
      cat <<'END_OF_CONFIG' >$ConfigFile #{Code added by install_15_CPU_STM32F30x.sh
# openocd flash script for stm32f30x


# finish configuration stage + proceed to run stage
init

# hardware reset processor with halt
reset halt

# check target state
poll

# list all found falsh banks
flash banks

# identify the flash
flash probe 0

# unprotect flash for writing
#DEPRECATED flash protect_check 0 0

# erasing all flash
#DEPRECATED stm32x mass_erase 0
stm32f1x mass_erase 0

# write file to flash number 0
flash write_bank 0 main.bin 0

# hardware processor reset with run
reset run

#shutdown openocd daemon
shutdown
#}END Code added by install_15_CPU_STM32F30x.sh

END_OF_CONFIG
    else
      echo "cfg-file already exists: '`pwd`/$ConfigFile'.."
    fi
    #}END Write config file for openocd
    
    LibPrefix="250_stm_std_peripherals_"
    LibName="${LibPrefix}f3xx"
    
    createExtensionMakefileHead ${Name} #{ ----Create Makefile
    File="${Dir_Extensions}makefile.${Name}" 
    cat <<END_OF_MAKEFILE >>$File 

#ARM_CPU     =arm7tdmi
TARGET      =-mcpu=cortex-m3 -mthumb
TARGET_ARCHITECTURE_STM32F30x=1
TARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_STM32F30x
COMPILE_OPTS += -DTARGET_DATA_WIDTH=32
COMPILE_OPTS += -DUSE_STDPERIPH_DRIVER
COMPILE_OPTS += -DUSE_FULL_ASSERT=1
COMPILE_OPTS += -DTTC_INTERRUPT_GPIO_AMOUNT=16
# architecture specific support code
#INCLUDE_DIRS += -Ittc-lib/stm32/ -Ittc-lib/stm32l1/ DEPRECATED
vpath %.c ttc-lib/stm32/ ttc-lib/register/
vpath %.s additionals/200_cpu_stm32f3xx/STM32F30x_DSP_StdPeriph_Lib_V1.1.0/Libraries/CMSIS/Device/ST/STM32F30x/Source/Templates/gcc_ride7




MAIN_OBJS += system_stm32f3xx.o 

# define linker script to use
LDFLAGS += -Tconfigs/memory_stm32f3xx.ld -mthumb -mcpu=cortex-m3

#{Startup codes for STM32F30x family

startup_stm32f30x.o:		startup_stm32f30x.s


#}

#{ define uController to use (required to include stm32f3xx.h) ---------------

# Microcontroller Class: activate only one uC!
# - Medium density plus STM32F30xxC devices are microcontrollers with a Flash memory density of 256 Kbytes.

STM32F30X   =1       # F302 Device


ifdef uCONTROLLER
  ERROR: STM32F30x=1 - Only one uController may be selected!
endif
uCONTROLLER=STM32F30x
COMPILE_OPTS += -DuCONTROLLER=STM32F30x
COMPILE_OPTS += -DSTM32F30x
MAIN_OBJS += startup_stm32f30x.o

ifndef uCONTROLLER
  ERROR: No microcontroler class selected. Define one from (STM32F30x)
endif


#}END define uController


END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}---- END Create Makefile
    
    createActivateScriptHead $Name ${Dir_Extensions} $0 "CPU STM32F30x" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{Code added by install_15_CPU_STM32F30x.sh

# remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.200_cpu_*

activate.190_cpu_cortexm3.sh QUIET \"\$0\"

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# activate corresponding std_peripheral_library
activate.250_STM32F30x_StdPeriph_Driver.sh QUIET

# create link to target script being required by openocd to initialize target cpu
cd \$Dir_Configs
createLink ../additionals/999_open_ocd/target/stm32l.cfg openocd_target.cfg
createLink ../additionals/999_open_ocd/target/stm32f3xx_flash.cfg openocd_flash.cfg
cd ..
#}END Code added by install_15_CPU_STM32F30x.sh

#X activate.500_ttc_register.sh                    QUIET "\$0"
activate.250_STM32F30x_StdPeriph_Driver.sh      QUIET "\$0"
activate.250_stm_std_peripherals__rcc.sh        QUIET "\$0"
activate.250_stm_std_peripherals__exti.sh       QUIET "\$0"
#activate.450_interrupt_cortexm3.sh             QUIET "\$0"
activate.190_cpu_cortexm3.sh                    QUIET "\$0"

# create link to target script being required by openocd to initialize target cpu
cd \$Dir_Configs
createLink ../additionals/999_open_ocd/target/stm32f3x.cfg openocd_target.cfg
createLink ../additionals/999_open_ocd/target/stm32f3xx_flash.cfg openocd_flash.cfg
cd ..

END_OF_ACTIVATE
    createActivateScriptTail $Name ${Dir_Extensions}
    #}

      cd current/Libraries/
      mv CMSIS/ STM32F30x_CMSIS/
      SubDirs=`ls */ -d`
  for SubDir in $SubDirs; do #{ create link into additionals/ for each sub directory
    SubDir=`perl -e "print substr('${SubDir}', 0, -1);"` # remove trailing /
    echo "sub library: $SubDir"
    echo $SubDirs
    Link="250_$SubDir"
    echo $Install_Dir/Libraries/$SubDir
    addLine $Start_Path/scripts/createLinks.sh "rm 2>/dev/null $Link;  createLink \$Source/TheToolChain/InstallData/200_cpu_stm32f30x/current/Libraries/$SubDir  $Link"
done #}
  
  SubDirs=`ls -d *_StdPeriph_Driver`
  for SubDir in $SubDirs; do #{ create makefile + activate script for each *_StdPeriph_Driver directory
    Name="250_${SubDir}" # std peripheral uses different rank than CPAL!
    Architecture=`perl -e "print substr('${SubDir}', 0, 9);"`

    createExtensionMakefileHead ${Name} $Start_Path/extensions/ #{      create makefile for std_peripheral_library
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile

COMPILE_OPTS += -DEXTENSION_250_stm_std_peripherals=1

INCLUDE_DIRS += -I additionals/250_STM32F30x_CMSIS/Include/ \\
                -I additionals/250_STM32F30x_CPAL_Driver/inc/ \\
                -I additionals/250_STM32F30x_StdPeriph_Driver/inc/ \\
                -I additionals/250_STM32F30x_CMSIS/Device/ST/STM32F30x/Include
vpath %.c additionals/250_STM32F30x_StdPeriph_Driver/src/ \\
          additionals/250_STM32F30x_CPAL_Driver/src/

vpath %.s additionals/250_STM32F30x_CMSIS/Device/ST/STM32F30x/Source/Templates/gcc_ride7/

MAIN_OBJS += misc.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} $Start_Path/extensions/ #}
    createActivateScriptHead $Name "$Start_Path/extensions/" "$0 1" "Architecture dependent standard peripheral library for $SubDir microcontrollers" #{ 
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
#!/bin/bash

Module_ERROR=""

# remove existing links
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Name}
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Install_Dir}_*_StdPeriph_Driver


OldPWD=\`pwd\`

# create links into extensions/
cd "\${Dir_Extensions}"
rm 2>/dev/null *250_stm_std_peripherals_*

DriversFound=\`ls $Architecture/*\`
if [ "\$DriversFound" != "" ]; then
  ln -sv $Architecture/* .
else
  Module_ERROR="Cannot find drivers in \`pwd\`/$Architecture/ !"
fi

cd "\$CurrentPath"
if [ "\$Module_ERROR" == "" ]; then
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
else
  echo "\$0 - ERROR: \$Module_ERROR"
fi

END_OF_ACTIVATE
#}
    createActivateScriptTail $Name "$Start_Path/extensions/"
    
    #{ create one activate-script per c-file
    TargetDir="../../${Dir_Extensions}$Architecture"
    dir "$TargetDir"

    LibPrefix="250_stm_std_peripherals"
    for SourceFile in `find ${SubDir}/src/ -name "*\.c" -execdir echo -n "{} " \;` ; do #{
      SourceName=`perl -e "print substr('$SourceFile', 2, -2);"`
      Source=`perl -e "print substr('$SourceFile', 12, -2);"`
      if [ "$Source" != "" ]; then
        DestName="${LibPrefix}__${Source}"
        createExtensionMakefileHead ${DestName} $TargetDir/ #{
        cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # set by createExtensionMakefileHead() 
  
MAIN_OBJS += ${SourceName}.o

END_OF_MAKEFILE
        createExtensionMakefileTail ${DestName} $TargetDir/ #}
        createActivateScriptHead $DestName $TargetDir/ $0 "adds c-source to list of compiled objects: ${SourceName}.c" #{
        cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# add createL.inks for each link required for this extension
createLink ${Dir_Extensions}makefile.$DestName \${Dir_ExtensionsActive}/makefile.$DestName '' QUIET

END_OF_ACTIVATE
#}
        createActivateScriptTail $DestName $TargetDir/
        #}
      fi
    done #}
    #} create one activate-script per c-file
  done #}
    
    echo "Installed successfully: $Install_Dir"
  fi #}END OK.Install
  
  cd ..
  rmdir 2>/dev/null $Install_Dir  # removes only empty install dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
