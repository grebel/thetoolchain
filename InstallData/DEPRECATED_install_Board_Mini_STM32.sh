#!/bin/bash

#
#  Install script for protoboard Mini-STM32 v3.0 with 2,8" Color Touch Display
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="100"
EXTENSION_SHORT="mini_stm32"
EXTENSION_PREFIX="board"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.cutedigi.com/pub/ARMCORTEX/ STM32-FD-HX.pdf Schematics.pdf Boards/Mini-STM32
  getDocumentation http://www.rcos.eu/targets/stm32tfth-backend/Datasheets/ Mini%20STM32%20Entwicklungsboard%20Anleitung.pdf Mini-STM32_Entwicklungsboard_Anleitung.pdf Boards/Mini-STM32
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

  if [ ! -e OK.Install ]; then #{
    getFile "http://thetoolchain.com/ProtoBoards/MiniSTM32/" "FreeRTOS_on_MINI-STM32.tar.bz2"
    getFile "http://thetoolchain.com/ProtoBoards/MiniSTM32/" "MINI-STM32.tar.bz2"
    untgj FreeRTOS_on_MINI-STM32 FreeRTOS_on_MINI-STM32.tar.bz2
    untgj MINI-STM32 MINI-STM32.tar.bz2
  
    if [ -d FreeRTOS_on_MINI-STM32/ ]; then #{
      chmod -R +rw FreeRTOS_on_MINI-STM32
      if [ -d MINI-STM32/ ]; then
        chmod -R +rw MINI-STM32
        cd MINI-STM32/
        for File in "MINI-STM32 Schematic" "TFT Driver Information" "TouchScreen Information" "usermanual"; do
          echo "copying docmentation '$File'"
          if [ -d "$File" ]; then
            addDocumentationFolder "$File" Boards/Mini-STM32/
            #echo "cp -Rv \"$File\" $DocDir/"
          else
            addDocumentationFile "$File" Boards/Mini-STM32/
          fi
        done
        cd ..
        touch OK.Install
      fi
    fi #}
  fi #}
  if [ -e OK.Install ]; then #{

    Driver1="450_ttc_gfx_driver_$Install_Dir"             #{ gfx driver for this board
    createExtensionMakefileHead ${Driver1} #{
    File="${Dir_Extensions}makefile.${Driver1}" 
    cat <<END_OF_MAKEFILE >>$File 

# ttc_gfx driver settings for board Olimex-LCD
# reference: http://olimex.com/dev/ARM/ST/STM32-LCD/STM32-LCD-schematic.pdf

# LCD-Display 320x240x24 - define settings required by low-level driver to run on current board

COMPILE_OPTS += -DTTC_GFX1_WIDTH=240
COMPILE_OPTS += -DTTC_GFX1_HEIGHT=320
COMPILE_OPTS += -DTTC_GFX1_DEPTH=24
COMPILE_OPTS += -DTTC_GFX1_DRIVER=tgd_ILI9320    # one from ttc_gfx_types.h:e_ttc_gfx_driver

END_OF_MAKEFILE
    createExtensionMakefileTail ${Driver1} #}
    createActivateScriptHead $Driver1 ${Dir_Extensions} $ScriptName "gfx driver for Protoboard LCD from Olimex -> http://olimex.com/dev/stm32-lcd.html"
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

#rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Driver1 \${Dir_ExtensionsActive}/makefile.$Driver1 '' QUIET

#activate.400_lcd_320x240_ili9320.sh QUIET "\$ScriptName"

END_OF_ACTIVATE
  #}
    createActivateScriptTail $Driver1 ${Dir_Extensions}
    #}Driver1
    Driver2="450_ttc_input_driver_$Install_Dir"           #{ touch-handler driver for this board
    createExtensionMakefileHead ${Driver2} #{
    File="${Dir_Extensions}makefile.${Driver2}" 
    cat <<END_OF_MAKEFILE >>$File 

# ttc_input driver settings for board MiniSTM32

# Touch-Display - define settings required by low-level driver to run on current board
COMPILE_OPTS += -DTTC_INPUT1_DRIVER=tid_ADS7843 # one from DEPRECATED_ttc_input_types.h:e_ttc_input_driver
COMPILE_OPTS += -DTTC_INPUT1_WIDTH=240
COMPILE_OPTS += -DTTC_INPUT1_HEIGHT=320
COMPILE_OPTS += -DTTC_INPUT1_DEPTH=24
COMPILE_OPTS += -DTTC_INPUT_DIGITAL_XR=E_ttc_gpio_pin_c3
COMPILE_OPTS += -DTTC_INPUT_DIGITAL_XL=E_ttc_gpio_pin_c2
COMPILE_OPTS += -DTTC_INPUT_DIGITAL_YU=E_ttc_gpio_pin_c1
COMPILE_OPTS += -DTTC_INPUT_DIGITAL_YD=E_ttc_gpio_pin_c0
COMPILE_OPTS += -DTTC_INPUT_ANALOG_XR=ADC_Channel_13
COMPILE_OPTS += -DTTC_INPUT_ANALOG_XL=ADC_Channel_12
COMPILE_OPTS += -DTTC_INPUT_ANALOG_YU=ADC_Channel_11
COMPILE_OPTS += -DTTC_INPUT_ANALOG_YD=ADC_Channel_10
COMPILE_OPTS += -DTTC_INPUT_ANALOG_ADC=ADC1

END_OF_MAKEFILE
    createExtensionMakefileTail ${Driver2} #}
    createActivateScriptHead $Driver2 ${Dir_Extensions} $ScriptName "touch-handler driver for Protoboard LCD from Olimex -> http://olimex.com/dev/stm32-lcd.html"
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

 # rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Driver2 \${Dir_ExtensionsActive}/makefile.$Driver2 '' QUIET
  
  #activate.400_input_analog.sh QUIET "\$ScriptName"

END_OF_ACTIVATE
  #}
    createActivateScriptTail $Driver2 ${Dir_Extensions}
    #}Driver2

    for BoardRev in 20 30 40; do
      Name="${Install_Dir}_rev${BoardRev}"
      addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; createLink \$Source/TheToolChain/InstallData/$Install_Dir/FreeRTOS_on_MINI-STM32/Source/Mini_STM32/  $Install_Dir"
      createExtensionMakefileHead ${Name} #{
      File="${Dir_Extensions}makefile.${Name}"
      cat <<END_OF_MAKEFILE >>$File #{

COMPILE_OPTS += -DTTC_BOARD=$Name
COMPILE_OPTS += -DTTC_BOARD_REVISION=${BoardRev}
BOARD=$Name
BOARD_REVISION=${BoardRev}

# define microcontroller used on board
#X STM32F10X_MD  =1
#X COMPILE_OPTS += -DSTM32F10X_MD

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f103rbt6 = 1

INCLUDE_DIRS += -I additionals/$Name/

# system clock setup (-> ttc_sysclock_types.h)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=12000000
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL=32768

# LCD pin configuration
# -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_WRITE_STROBE=E_ttc_gpio_pin_c10
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_READ_STROBE=E_ttc_gpio_pin_c11

vpath %.c additionals/$Name/

#{ single port pins
# Note: Several Mini-STM32 boards are being offered at eBay with different board layouts.
#       You may have to identify the routing on your PCV manually. 
#       Use an ohmmeter (with beeper) to find connected uC pin and look up corresponding GPIO pin in uC datasheet.

COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_a2       # status LED 1
COMPILE_OPTS += -DTTC_LED1_LOWACTIVE=1  # status LED is shining when gpio pin is pulled low
COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_b2       # status LED 2
COMPILE_OPTS += -DTTC_LED2_LOWACTIVE=1  # status LED is shining when gpio pin is pulled low
COMPILE_OPTS += -DTTC_LED3=E_ttc_gpio_pin_a3       # status LED 3
COMPILE_OPTS += -DTTC_LED3_LOWACTIVE=1  # status LED is shining when gpio pin is pulled low
COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_a0                  # press button switch 1 (named SW3 on my board)
COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=1             # button pressed => gpio pin voltage = 0V 
COMPILE_OPTS += -DTTC_SWITCH2=E_ttc_gpio_pin_a1                  # press button switch 2 (named SW4 on my board)
COMPILE_OPTS += -DTTC_SWITCH2_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH2_LOWACTIVE=1             # button pressed => gpio pin voltage = 0V 
#? COMPILE_OPTS += -DPB_VR1=E_ttc_gpio_pin_b0        # potentiometer 3V3 -|R33|- GND

#}single port pins
#{ parallel ports (8 or 16 consecutive pins in same bank)
#
# Every 8 available, consecutive bits in same GPIO bank can form an 8-bit wide port.
# A 16-bit wide port requires 16 available, consecutive bits (obviously).
# A pin is available if it not driven externally or used for something else.
# Check circuit of your board for details.

# define standard Input Driver
COMPILE_OPTS += -DTTC_INPUT1_DRIVER=tid_ADS7843

# 8-bit wide parallel port #1 providing DB00..DB07
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_BANK=TTC_GPIO_BANK_B
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_FIRSTPIN=8

# 8-bit wide parallel port #2 providing DB08..DB15
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_BANK=TTC_GPIO_BANK_C
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_FIRSTPIN=0

# 8-bit wide parallel port #3 for external use (overlaps some leds and switches)
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_3_BANK=TTC_GPIO_BANK_A
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_3_FIRSTPIN=0

#}parallel ports
#{ USART1  - define universal synchronous asynchronous receiver transmitter #1
  COMPILE_OPTS += -DTTC_USART1=INDEX_USART1       # Connected to PL2303HX Serial->USB converter
  COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_a9   # UEXT1-3, UEXT40-3
  COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_a10  # UEXT1-4, UEXT40-4
  COMPILE_OPTS += -DTTC_USART1_CK=E_ttc_gpio_pin_a8   # EXT-40
#}USART1
#{ USART2  - define universal synchronous asynchronous receiver transmitter #2
  COMPILE_OPTS += -DTTC_USART2=INDEX_USART3       # Connected to male header
  COMPILE_OPTS += -DTTC_USART2_TX=E_ttc_gpio_pin_b10  # XS5-27
  COMPILE_OPTS += -DTTC_USART2_RX=E_ttc_gpio_pin_b11  # XS5-22
  COMPILE_OPTS += -DTTC_USART2_CK=E_ttc_gpio_pin_b12  # XS5-34
  COMPILE_OPTS += -DTTC_USART2_CTS=E_ttc_gpio_pin_b13 # XS5-33
  COMPILE_OPTS += -DTTC_USART2_RTS=E_ttc_gpio_pin_b14 # XS5-32
#}USART2
#{ SPI1    - define serial peripheral interface #1
  COMPILE_OPTS += -DTTC_SPI1=ttc_device_1          # Connected to male header
  COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_b15 # XS5-35
  COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_b14 # XS5-32
  COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_b13  # XS5-33
  COMPILE_OPTS += -DTTC_SPI1_NSS=E_ttc_gpio_pin_b12  # XS5-34
#}SPI1
#{ SPI2    - define serial peripheral interface #2
  COMPILE_OPTS += -DTTC_SPI2ttc_device_1          # Connected to TFT
  COMPILE_OPTS += -DTTC_SPI2_MOSI=E_ttc_gpio_pin_a7  # XS2-6
  COMPILE_OPTS += -DTTC_SPI2_MISO=E_ttc_gpio_pin_a6  # XS2-8
  COMPILE_OPTS += -DTTC_SPI2_SCK=E_ttc_gpio_pin_a5   # XS2-2
  COMPILE_OPTS += -DTTC_SPI2_NSS=E_ttc_gpio_pin_a4   # XS2-4
#}SPI2
#{ I2C1    - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b9    # XS5-20
  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b8    # XS5-30
#}I2C1
#{ I2C2    - define inter-integrated circuit interface #2

  COMPILE_OPTS += -DTTC_I2C2=ttc_device_2           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C2_SDA=E_ttc_gpio_pin_b11   # XS5-22
  COMPILE_OPTS += -DTTC_I2C2_SCL=E_ttc_gpio_pin_b10   # XS5-27
  COMPILE_OPTS += -DTTC_I2C2_SMBAL=E_ttc_gpio_pin_b12 # XS5-34
#}I2C2

#{ define Controller Area Network (CAN) devices
  COMPILE_OPTS += -DTTC_CAN1=0               # physical device index
  COMPILE_OPTS += -DTTC_CAN1_PIN_RX=E_ttc_gpio_pin_a11  # pin used for receive
  COMPILE_OPTS += -DTTC_CAN1_PIN_TX=E_ttc_gpio_pin_a12  # pin used for transmit
#}CAN

#{ define Universal Serial Bus (USB) devices
  COMPILE_OPTS += -DTTC_USB1=1               # physical device index
  COMPILE_OPTS += -DTTC_USB1_DISCONNECT=E_ttc_gpio_pin_d2  # pin used for disconnect and connect
#}CAN

END_OF_MAKEFILE
#}
      if [ "$BoardRev" == "20" ]; then #{
      	 cat <<END_OF_EXTRAS >>$File
# Extra settings for board rev $BoardRev
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_RESET=E_ttc_gpio_pin_c9
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_CHIPSELECT=E_ttc_gpio_pin_c8
END_OF_EXTRAS
      fi #}
      if [ "$BoardRev" == "30" ]; then #{
      	 cat <<END_OF_EXTRAS >>$File
# Extra settings for board rev $BoardRev
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_RESET=E_ttc_gpio_pin_c9
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_CHIPSELECT=E_ttc_gpio_pin_c8
END_OF_EXTRAS
      fi #}
      if [ "$BoardRev" == "40" ]; then #{
      	 cat <<END_OF_EXTRAS >>$File
# Extra settings for board rev $BoardRev
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_RESET=E_ttc_gpio_pin_c8
COMPILE_OPTS += -DTTC_GFX1_ILI9320_PIN_CHIPSELECT=E_ttc_gpio_pin_c9
END_OF_EXTRAS
      fi   #}

      createExtensionMakefileTail ${Name} #}
      createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Protoboard Mini-STM32 with 2,8\" 320x240 Color Touch LCD and STM32F103RBT6 (Hardware revision ${BoardRev})"  #{
      cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  #rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
  
  #X activate.200_cpu_stm32f10x.sh QUIET "\$ScriptName"

  enableFeature 450_cpu_stm32f1xx
  activate.500_ttc_cpu.sh          QUIET "\$ScriptName"
  
  #X activate.400_lcd_320x240_ili9320.sh QUIET "\$ScriptName"
  #X activate.400_input_ads7843.sh QUIET "\$ScriptName"
  activate.${Driver1}.sh QUIET "\$ScriptName"
  activate.${Driver2}.sh QUIET "\$ScriptName"
  activate.450_gfx_ili9320.sh QUIET "\$ScriptName"

  enableFeature 450_cpu_stm32f1xx
END_OF_ACTIVATE
#}
      createActivateScriptTail $Name ${Dir_Extensions}
      #}

    done
    echo "Installed successfully: $Install_Dir"
  else
    echo "ERROR occured during install: $Install_Dir"
    exit 10
  fi #}
  cd ..
  rmdir 2>/dev/null $Install_Dir  # removes only empty install dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0

