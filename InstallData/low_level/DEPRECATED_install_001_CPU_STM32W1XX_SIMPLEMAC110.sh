#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
#
#  Install script for a single Low-Level Driver
#  for stm32w1xx devices.
#
#  Created from template _install_NN_TTC_DEVICE_ARCHITECTURE.sh revision 22 at 20140528 16:09:26 UTC
#
#  Supported Architecture: simplemac110
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$0 - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="simplemac110"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards extensions; DirLL_Extensions="$MyStartDir/$ThisArchitecture/$FoundFolder"
findFolderUpwards scripts;    Start_Path="$MyStartDir/$ThisArchitecture/${FoundFolder}.."

Install_Path=`pwd`
echo "installing in $Install_Path ..."

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

LinkFirmware="SimpleMac-Firmware"
LinkLibrary="SimpleMAC_110"
DriverName_CPU="${Rank_CPU}_cpu_stm32w1xx"
DriverName_MAC="${Rank_MAC}_stm32w1xx_simple_mac110"
DriverName_HAL="${Rank_HAL}_stm32w1xx_hal110"
Path2Library_CPU="$Install_Path/SimpleMac-Firmware"
Path2Library_MAC="$Install_Path/SimpleMac-Library"
Path2Library_HAL="$Install_Path/hal_library"

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    
    cd "$Install_Path"
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    echo "LinkLibrary='$LinkLibrary'" #D
    if [ ! -d "$LinkLibrary" ]; then
      getFile http://www.thetoolchain.com/mirror/ ${LinkLibrary}.tgj ${LinkLibrary}.tgj
      untgj "${LinkLibrary}" ${LinkLibrary}.tgj
    fi
    if [ ! -d "${LinkLibrary}" ]; then
      echo "$0 - ERROR: Could not download file ${LinkLibrary}.tgj (missing directory ${LinkLibrary})"
      exit 12
    fi
    createLink SimpleMAC_110/STM32W108/simplemac/      ${Path2Library_MAC}
    createLink SimpleMAC_110/STM32W108/hal/            ${Path2Library_HAL}
    createLink SimpleMAC_110/STM32W108/                $LinkFirmware
    addDocumentationFolder SimpleMAC_110/STM32W108/docs/hal/html   HAL-Library_110        uC STM32W1xx
    addDocumentationFolder SimpleMAC_110/STM32W108/docs/simplemac  SimpleMAC-Library_110  uC STM32W1xx
  
    if [ "1" == "1" ]; then #{ unpack simple mac library
      PrevDir=`pwd`
      cd "$Path2Library_MAC/library"
      mkdir gnu
      cd gnu
      ar x ../libsimplemac-library-gnu.a
      cd "$PrevDir"
    fi #}
    function moveDuplicateFile() { # renames given file to a unique filename
      BaseDir="$1"      # no trailing slash!
      SubDir="$2"       # no trailing/ leading slash!
      OldFileName="$3"
      NewFileName="$4"
      
      OldFile="$BaseDir/$SubDir/$OldFileName"
      NewFile="$BaseDir/$NewFileName"
      
      if [ -e "$OldFile" ]; then #{
        echo "Renaming file with duplicate filename '${OldFileName}' -> '${NewFileName}':" 
        echo -n "    "
        mv -v "$OldFile" "$NewFile"
        PatchFiles=`find ./ -type f -name *.[ch] -exec grep -l $SubDir/$OldFileName {} \;`
        #echo "    PatchFiles='find `pwd` -type f -name *.[ch] -exec grep -l $SubDir/$OldFileName {} \;'"; echo " = '$PatchFiles'" #D 
        for PatchFile in $PatchFiles; do #{ replace all "stm32w108/${Name}.h" -> "${Name}_stm32w108.h"
          if [ ! -e $PatchFile ]; then
            ERROR="1"
            echo "ERROR: File not found: '$PatchFile'!"
          else
            echo "    patching file '$PatchFile': '$SubDir/$OldFileName' -> '$NewFileName'"
            replaceInFile "$PatchFile" "$SubDir/$OldFileName" "$NewFileName"
          fi
        done #}
      else
        if [ ! -e "$NewFile" ]; then
          echo "$0 - ERROR: moveDuplicateFile($BaseDir, $SubDir, $OldFileName, $NewFileName)"
          echo "    Cannot find file '$OldFile'!"
          echo "    moveDuplicateFile() was called from line `caller`"
          exit 31
        else
          echo "File already moved -> '$NewFile'"
        fi
      fi #}
    }
    
    # Files with duplicate filenames are a NOGO in TTC world!
    moveDuplicateFile ${Path2Library_HAL}/micro          cortexm3  adc.h          adc_cortexm3.h
    moveDuplicateFile ${Path2Library_HAL}/micro          cortexm3  micro-common.c micro-common_cortexm3.c
    moveDuplicateFile ${Path2Library_HAL}/micro          cortexm3  micro-common.h micro-common_cortexm3.h
    moveDuplicateFile ${Path2Library_HAL}/micro/cortexm3 stm32w108 memmap.h       memmap_stm32w108.h
    
    PatchFile=`find ./ -name adc.c`
    replaceInFile "$PatchFile"   '"hal/micro/adc.h"' '"adc.h" //TTC fixed include'

    PatchFile=`find ./ -name micro.c`
    replaceInFile "$PatchFile"  "#pragma pack"           "// TheToolChain (causes compiler warning) #pragma pack"
    replaceInFile "$PatchFile"  "#pragma pack"           "// TheToolChain (causes compiler warning) #pragma pack"
    replaceInFile "$PatchFile"  "halInternalBlockUntilXtal();" "#ifndef DISABLE_IDLING_DELAY   //TTC\nhalInternalBlockUntilXtal();\n#endif //TTC\n"
    #{
    #{
    replaceInFile "$PatchFile"  "\} appSwitchStructType;" "\} __attribute__((__packed__)) appSwitchStructType; // TheToolChain GCC compliant packed structure"
    
    if [ "$MissingFiles" == "" ]; then #{
      touch OK.Install
    else
      echo "$0 - ERROR: missing files: $MissingFiles"
    fi #}
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts

  if [ ! -e OK.PatchFiles ]; then #{    patch source files
    echo "`pwd` > patching source files..."
    ERROR=""
    
    PatchFiles=`find ./ -follow -type f -name "gnu.h"`
    for PatchFile in $PatchFiles; do #{ replace all "hal/micro/ -> "micro/
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        replaceInFile $PatchFile "int abs(int I);" "//int abs(int I) //TTC (already defined in ttc_basic.h"
        addLine2Header "${PatchFile}" '#include "compile_options.h" //TTC (improves visibility in IDE)'
      fi
    done #}

    #? PatchFile=`find ./ -follow -name micro-common.h`
    
    PatchFiles=`find ./ -follow -name *.[ch]`
    for PatchFile in $PatchFiles; do #{ replace all "hal/micro/ -> "micro/
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        #replaceInFile "$PatchFile" '"hal/micro/'   '"micro/'
        replaceInFile "$PatchFile" '"hal/micro/'   '"'
        replaceInFile "$PatchFile" '"micro/'       '"'
        replaceInFile "$PatchFile" '"cortexm3/'    '"'
        replaceInFile "$PatchFile" '"hal/hal.h"'   '"hal.h" //TTC'
        replaceInFile "$PatchFile" '"hal/error.h"' '"error.h" //TTC'
        
      fi
    done #}
    
    
    PatchFiles=`find ./ -follow -name *.c`
    for PatchFile in $PatchFiles; do #{ replace all "hal/micro/ -> "micro/
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        replaceInFile "$PatchFile" '"hal/micro/'   '"micro/'
        replaceInFile "$PatchFile" '"hal/hal.h"'   '"hal.h" //TTC'
        replaceInFile "$PatchFile" '"hal/error.h"' '"error.h" //TTC'
      fi
    done #}

    PatchFiles=`find ./ -follow -type f -name "*.c" -exec grep -l "#include BOARD_HEADER" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        echo "replacing BOARD_HEADER in $PatchFile.."
        #X replaceInFile $PatchFile " BOARD_HEADER"  '"hal/micro/cortexm3/stm32w108/board.h" //BOARD_HEADER (patched for TheToolChain)'
        replaceInFile $PatchFile " BOARD_HEADER"  ' "stm32w/stm32w_dummyboard.h" //TTC (BOARD_HEADER)'
      fi
    done #}

    PatchFiles=`find ./ -follow -type f -name "*.c" -exec grep -l "#include PLATFORM_HEADER" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        echo "replacing PLATFORM_HEADER in $PatchFile.."
        replaceInFile $PatchFile " PLATFORM_HEADER"  ' "gnu.h"  //TTC (PLATFORM_HEADER)'
      fi
    done #}
    
    PatchFiles=`find ./ -follow -type f -name "*.c" -exec grep -l "stdlib.h" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        AlreadyPatched=`grep '//#include <stdlib.h>' "$PatchFile"`
        if [ "$AlreadyPatched" == "" ]; then
          echo "replacing include stdlib in $PatchFile.."
          replaceInFile $PatchFile "#include <stdlib.h>"  '//#include <stdlib.h>  //TTC (not required)'
        fi
      fi
    done #}
    
    PatchFiles=`find SimpleMac-Firmware/ -type f -follow -name "*" -exec grep -l "RAM_region" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        echo "replacing RAM_region in $PatchFile.."
        replaceInFile $PatchFile RAM_region  'ram'
      fi
    done #}

    PatchFiles=`find SimpleMac-Firmware/ -type f -follow -name "*" -exec grep -l "ROM_region" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        echo "replacing RAM_region in $PatchFile.."
        replaceInFile $PatchFile ROM_region  'rom'
      fi
    done #}
    
    PatchFiles=`find ./ -follow -type f -name "*" -exec grep -l "FIB_region" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        echo "replacing FIB_region in $PatchFile.."
        replaceInFile $PatchFile FIB_region  'fib'
      fi
    done #}
    
    if [ "1" == "0" ]; then #{ # for some strange reason, several source-files do not include required headers (bug seems to be fixed in SimpleMAC 201)
      PatchFiles=`find ${LinkFirmware}/Utilities/STM32_EVAL/ -type f -name "*.[ch]" -exec grep -l "UART_InitTypeDef" {} \;`
      for PatchFile in $PatchFiles; do #{
        if [ ! -e $PatchFile ]; then
          ERROR="1"
          echo "ERROR: File not found: '$PatchFile'!"
        else
          echo "adding missing SC-include to `pwd`/$PatchFile:.."
          chmod +w "${PatchFile}"
          addLine2Header "${PatchFile}" '#include "stm32w108xx_sc.h" //TTC'
        fi
      done #}
    fi #}

    for File in micro-common.h phy-library.h; do #{
      PatchFiles=`find ./ -name "$File"`
      for PatchFile in $PatchFiles; do #{ adding missing include of gnu.h
        if [ ! -e $PatchFile ]; then
          ERROR="1"
          echo "ERROR: File not found: '$PatchFile'!"
        else
          addLine2Header "${PatchFile}" '#include "gnu.h" //TTC'
        fi
      done #}
    done #}

    PatchFiles=`find ${LinkFirmware}/ -type f -name "*.c" -exec grep -l "GPIO_Mode_" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        addLine2Header "${PatchFile}" '#include "stm32w108xx_gpio.h" //TTC'
      fi
    done #}

    PatchFiles=`find ${LinkFirmware}/ -type f -name "*.c" -exec grep -l "assert_param" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        addLine2Header "${PatchFile}" '#include "../ttc_basic.h" //TTC'
      fi
    done #}

    PatchFiles=`find ${LinkFirmware}/ -type f -name "*.c" -exec grep -l "CLK_GetClocksFreq" {} \;`
    for PatchFile in $PatchFiles; do #{
      if [ ! -e $PatchFile ]; then
        ERROR="1"
        echo "ERROR: File not found: '$PatchFile'!"
      else
        addLine2Header "${PatchFile}" '#include "stm32w108xx_clk.h" //TTC'
      fi
    done #}
    
    if [ "$ERROR" == "" ]; then
      touch OK.PatchFiles
    fi
  fi #}
  if [ "1" == "1" ]; then #{ low-level driver for stm32w1xx on simplemac110
  
    findFolderUpwards scripts; Folder_scripts="$FoundFolder"

    if [ "1" == "1" ]; then #{ cpu extension based on SimpleMAC library rev 1.1.0
    DriverName="450_stm32w1xx_simplemac110"
    createExtensionMakefileHead ${DriverName}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 


END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$0 2" "low-level stm32w1xx Driver based on SimpleMAC library rev 1.1.0" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Rank_CPU}_cpu_*


# create link to target script being required by openocd to initialize target cpu
cd \$Dir_Configs
if [ -e ../additionals/999_open_ocd/target/stm32w1xx_jtag.cfg ]; then # openocd >= 0.7.0
  createLink ../additionals/999_open_ocd/target/stm32w1xx_jtag.cfg    openocd_target_jtag.cfg
  createLink ../additionals/999_open_ocd/target/stm32w108_stlink.cfg  openocd_target_stlinkv1.cfg
  createLink ../additionals/999_open_ocd/target/stm32w108_stlink.cfg  openocd_target_stlinkv2.cfg
  createLink ../additionals/999_open_ocd/target/stm32w1xx_flash.cfg   openocd_flash.cfg
  createLink ../additionals/999_open_ocd/target/stm32w108xx.cfg       openocd_cpu.cfg
else                                                                  # openocd <= 0.7.0
  createLink ../additionals/999_open_ocd/target/stm32w1xx.cfg       openocd_target_jtag.cfg
  createLink ../additionals/999_open_ocd/target/stm32w1xx_flash.cfg openocd_flash.cfg
fi
cd ..

cd "\$Dir_Additionals"
rm -fv ${Rank_CPU}_cpu_* ${Rank_MAC}_cpu_* ${Rank_HAL}_cpu_*
createLink ${Path2Library_MAC}  $Install_Dir
cd ..

activate.190_cpu_cortexm3.sh             QUIET \"\$0\"
activate.200_cpu_stm32w1xx.sh            QUIET \"\$0\"
activate.270_CPAL_STM32_CPAL_Driver.sh   QUIET \"\$0\"

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

activate.$DriverName_MAC.sh              QUIET \"\$0\"
activate.$DriverName_HAL.sh              QUIET \"\$0\"
activate.500_ttc_register.sh             QUIET \"\$0\"

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
    fi #}
    
    DriverName="$DriverName_MAC"
    createExtensionMakefileHead ${DriverName}      #{ (create makefile)
    
#(Missing in v1.10)    IncludeDir_CPU=`../scripts/findDirectory.pl stm32w108xx.h SimpleMac-Firmware`

    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

EM357_SIMPLEMAC = 1
INCLUDE_DIRS += -I additionals/$DriverName_MAC/include/
#(Missing in v1.10)    INCLUDE_DIRS += -I additionals/${DriverName_CPU}/$IncludeDir_CPU

# SimpleMAC library is only available as precompiled binary :-(
LD_LIBS += -mthumb additionals/$DriverName_MAC/library/libsimplemac-library-gnu.a

#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/phy-common.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/phy.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/aes.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/analogue-filter.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/analogue-mod-dac.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/analogue-vco.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/eui64.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/phy-library.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/phy-util.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/analogue-cell-bias.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/analogue-lna.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/analogue.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/byte-utilities.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/weak_defs.o

# SimpleMAC Library <=v2.0.0
#X (We already have our own linker script!) LDFLAGS+= -Tadditionals/$DriverName_HAL/micro/cortexm3/stm32w108/gnu-stm32w108.ld

# define rules for assembler-files which may be compiled into objects
context-switch.o: context-switch.s
context-switch_gnu.o: context-switch_gnu.s
MAIN_OBJS += context-switch.o

# Special Purpose Mask Registers manipulation routines
spmr.o: spmr.s
MAIN_OBJS += spmr.o

MAIN_OBJS += crt_stm32w108.o

# Setting this switch is a workaround for endless loop in halCommonSwitchToXtal() on ETRX357 boards
#? COMPILE_OPTS += -DDISABLE_IDLING_DELAY

END_OF_MAKEFILE

    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$0 2" "low-level stm32w1xx Driver Simple Media Access Control for SimpleMAC library rev. 1.1.0" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/


# create links into extensions.active/
createLink ${Dir_Extensions}makefile.$DriverName \${Dir_ExtensionsActive}/makefile.$DriverName '' QUIET

cd "\$Dir_Additionals"
createLink ${Path2Library_MAC} $DriverName_MAC
#X createLink $Install_Path/SimpleMac-Library ${DriverName}_SimpleMAC_library
cd ..

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}

    DriverName="$DriverName_HAL"
    createExtensionMakefileHead ${DriverName}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

#? ifndef EM357_SIMPLEMAC
#?   COMPILE_OPTS += -DHAL_STANDALONE   # This is a simplified version of the full stCalibrateVref
#?                                      # It is meant to be used on build for hal only application not linked with
#?                                      # simplemac library. The full version is embedded in the simplemac library.
#?                                      # The simplified version does not support calibration when manufacturing tokens are missing
#?                                      # or radio boost mode is activated
#?                                      # File: micro-common-internal.c
#? endif

INCLUDE_DIRS += -I additionals/${DriverName_CPU}/STM32W108/

INCLUDE_DIRS += -I additionals/${DriverName_HAL}/
INCLUDE_DIRS += -I additionals/${DriverName_HAL}/micro/
INCLUDE_DIRS += -I additionals/${DriverName_HAL}/micro/cortexm3/
INCLUDE_DIRS += -I additionals/${DriverName_HAL}/micro/cortexm3/bootloader/
INCLUDE_DIRS += -I additionals/${DriverName_HAL}/micro/cortexm3/compiler/
INCLUDE_DIRS += -I additionals/${DriverName_HAL}/micro/cortexm3/stm32w108/
INCLUDE_DIRS += -I additionals/${DriverName_HAL}/micro/generic/compiler

#? vpath additionals/${DriverName_HAL}/micro/cortexm3/stm32w108/

# where to find files of different types
vpath %.c additionals/${DriverName_HAL}/micro/
vpath %.c additionals/${DriverName_HAL}/micro/cortexm3/
vpath %.s additionals/${DriverName_HAL}/micro/cortexm3/
vpath %.c additionals/${DriverName_HAL}/micro/cortexm3/stm32w108/
vpath %.s additionals/${DriverName_HAL}/micro/cortexm3/stm32w108/

MAIN_OBJS += micro.o micro-common_cortexm3.o micro-common-internal.o clocks.o mfg-token.o adc.o flash.o nvm.o

#? Are these object files required?
MAIN_OBJS += system-timer.o sleep.o

# Objects provided by The ToolChain for compatibility reasons
MAIN_OBJS += stm32w_dummyboard.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$0 2" "low-level stm32w1xx Driver Hardware Abstraction Layer (HAL) for SimpleMAC library rev. 1.1.0" #{ (create activate script) 
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# create links into extensions.active/
createLink ${Dir_Extensions}makefile.$DriverName \${Dir_ExtensionsActive}/makefile.$DriverName '' QUIET

cd "\$Dir_Additionals"
createLink ${Path2Library_HAL} $DriverName_HAL
cd ..

activate.${DriverName_CPU}.sh  QUIET "\$0"
#?? activate.500_ttc_string.sh QUIET "\$0"  # ttc_string_printf() is required
  
  # this is also a Cortex M3 CPU  
  provideFeature 450_cpu_cortexm3
  provideFeature 450_cpu_stm32w1xx
  activate.500_ttc_cpu.sh           "\$0" QUIET
  activate.200_cpu_stm32w1xx.sh     "\$0" QUIET

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}

  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
#activate.500_ttc_gpio.sh QUIET "\$0"


END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ optional: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "../examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${Install_Dir}.sh QUIET "\$0"

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  addLine $Folder_scripts/createLinks.sh "rm 2>/dev/null ${DriverName_CPU};   createLink $Path2Library_CPU  $DriverName_CPU"
  addLine $Folder_scripts/createLinks.sh "rm 2>/dev/null ${DriverName_MAC};   createLink $Path2Library_MAC  $DriverName_MAC"
  addLine $Folder_scripts/createLinks.sh "rm 2>/dev/null ${DriverName_HAL};   createLink $Path2Library_HAL  $DriverName_HAL"
  #ToDo: include std-peripheral from SimpleMAC v2.0.1 - addLine $Folder_scripts/createLinks.sh "rm 2>/dev/null ${StdPeriphDriver};  createLink \$HOME/Source/TheToolChain/InstallData/${CPURank}_${INSTALL_PREFIX}/stm_std_peripherals ${StdPeriphDriver}"

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                            #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------
