#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a single Low-Level Driver
#  for cpu devices.
#
#  Created from template _install_NN_TTC_DEVICE_ARCHITECTURE.sh revision 25 at 20150318 12:33:43 UTC
#
#  Supported Architecture: stm32f1xx
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$0 - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="stm32f1xx"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards extensions; DirLL_Extensions="$MyStartDir/$ThisArchitecture/$FoundFolder"
findFolderUpwards scripts;    Start_Path="$MyStartDir/$ThisArchitecture/${FoundFolder}.."
#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.eecs.umich.edu/~prabal/teaching/eecs373-f11/readings/                             ARMv7-M_ARM.pdf            ARMv7-M_Architecture_Reference_Manual.pdf                                    uC/ARM7
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/         CD00298482.pdf             Advanced_developers_guide_for-STM32F103xx-STM32F100xx.pdf                    uC/STM32F1xx
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/REFERENCE_MANUAL/    CD00246267.pdf             STM32_RM0041_ReferenceManual_STM32F100xx.pdf                                 uC/STM32F1xx
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/REFERENCE_MANUAL/    CD00171190.pdf             STM32_RM0008_STM32F101xx_STM32F102xx_STM32F103xx_STM32F105xx_STM32F107xx.pdf uC/STM32F1xx
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/    CD00209826.pdf             STM32_AN2824_STM32F10xxx_I2C_optimized_examples.pdf                          uC/STM32F1xx
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/DATASHEET/           CD00220364.pdf             STM32_STM32F105xx_STM32F107xx_Connectivity_line.pdf                          uC/STM32F1xx
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/         CD00251227.pdf             UM0819_EthernetPhy_ST802RT1.pdf                                              uC/STM32F1xx
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/         CD00298482.pdf             UM1053_STM32_Motor_Control_Software_Development_Kit.pdf                      uC/STM32Fxxx
  getDocumentation http://www.cs.indiana.edu/~geobrown/                                                         book.pdf                   Discovering_the_STM32_Microcontroller.pdf                                    uC/STM32F1xx
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/programming_manual/ CD00283419.pdf             PM0075_STM32F10xxx_Flash_Memory_Programming.pdf                              uC/STM32F1xx
  getDocumentation http://www.diller-technologies.de/                                                           stm32.html                 diller-stm32.html                                                            uC/STM32xxxx
  getDocumentation http://www.hitex.com/fileadmin/pdf/insiders-guides/stm32/                                    isg-stm32-v18d-scr.pdf     The_Insiders_Guide_to_the_STM32_Microcontroller_Hitex.pdf                    uC/STM32xxxx
  getDocumentation http://www.st.com/web/en/resource/technical/document/datasheet/                              CD00191185.pdf             STM32F103x-Datasheet.pdf                                                     uC/STM32F1xx
  getDocumentation http://www.cs.indiana.edu/~geobrown/                                                         book.pdf                   Discovering_the_STM32_Microcontroller.pdf                                    uC/STM32xxxx

  TargetDir="Example_PeripheralInitialization"
  if [ ! -d "${TargetDir}" ]; then
    getFile http://www.keil.com/download/files/                                                          stm32_exti.zip         ${TargetDir}.zip
    unzip ${TargetDir}.zip
    mv STM32_EXTI/ ${TargetDir}
  fi
  addDocumentationFolder ${TargetDir} ${TargetDir} uC/STM32F1xx
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
  fi #}
if [ ! -e OK.Install ]; then       #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    installPackage libgmp3-dev
    installPackage libmpfr-dev
    installPackage xchm
    installPackage chmlib
    installPackage chmsee
    installPackage kchmviewer
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir/$ThisArchitecture ..."
    Install_Path=`pwd`
    
    
    Error=""
    
    Archive="stm32f10x_stdperiph_lib.zip"
    # getFile http://www.st.com/stonline/products/support/micro/files/ $Archive
    getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ $Archive
    
    # find name of only directory
    # std_peripherals_library/STM32F10x_StdPeriph_Lib_V3.4.0/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h
    
    if [ ! -e $Archive ]; then
      Error="Cannot download $Archive!"
    fi
    unZIP STM32F10x_StdPeriph_Lib $Archive || mv -f $Archive ${Archive}_bad
    rm current 2>/dev/null
    LibraryDir=`find ./  -maxdepth 1 -mindepth 1 -type d -name "STM32F10x*" -print`
    createLink $LibraryDir current
    cd current
    #X dir ../../../Documentation/STM
    #X mv *.chm ../../../Documentation/STM/
    for File in `ls *.chm`; do
      addDocumentationFile "$File" uC/STM32F1xx
    done

    
    File="Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h"
    if [ -e ${File}_orig ]; then
        mv -f ${File}_orig $File 
    fi
    mv -f $File ${File}_orig
    
    echo "  creating macro assert_param in $File"
    Note="  Note: Manually remove assert_param from your stm32f10x_conf.h !"
    echo $Note
    addLine ../Notes.txt $Note
    addLine ../Notes.txt "StdPeripheralLibrary-> http://www.mikrocontroller.net/articles/STM32F10x_Standard_Peripherals_Library"
    
    DestFolder="Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x"
    if [ ! -e $DestFolder/system_stm32f10x.c ]; then
      # system_stm32f10x.c is required to be found by the compiler so we put it aside its header file
      cp -v Project/STM32F10x_StdPeriph_Template/system_stm32f10x.c $DestFolder
      addLine ../Notes.txt "system_stm32f10x.c moved beside its header file (-> https://my.st.com/public/STe2ecommunities/mcu/Lists/ARM%20CortexM3%20STM32/Flat.aspx?RootFolder=%2Fpublic%2FSTe2ecommunities%2Fmcu%2FLists%2FARM%20CortexM3%20STM32%2FMissing%20file%20in%20new%203.4.0%20libraries%20system_stm32f10x.c&FolderCTID=0x01200200770978C69A1141439FE559EB459D758000626BE2B829C32145B9EB5739142DC17E&currentviews=320)"
    fi
  
    cat >script.sed <<"END_OF_TEXT" #{
/#include <stdint.h>/a\
#ifdef  USE_FULL_ASSERT\n
\n
// This assert_param macro has been copied here from stm32f10x_conf.h to\n
// circumvent gcc compiler errors.\n
\n
/**\n
* @brief  The assert_param macro is used for function's parameters check.\n
* @param  expr: If expr is false, it calls assert_failed function\n
*   which reports the name of the source file and the source\n
*   line number of the call that failed.\n 
*   If expr is true, it returns no value.\n
* @retval None\n
*/\n
#define assert_param(expr) ((expr) ? (void)0 : assert_failed((t_u8 *)__FILE__, __LINE__))\n
void assert_failed(t_u8* file, t_u32 line);\n
#else\n
#define assert_param(expr) ((void)0)\n
#endif /* USE_FULL_ASSERT */\n

END_OF_TEXT
#}

    # concatenate all lines which contain \n (sed requires this)
    awk '/\\n/{printf "%s",$0;next}{print}' script.sed >script2.sed
    
    # insert lines after "#include <stdint.h>"
    sed -f script2.sed ${File}_orig >$File
  
    cd ..

  #if [ -e "SOME_FILE" ]; then
    echo "" >OK.Install
  #fi
else
  echo "$0 - ERROR: Packages were not installed correctly!"
fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then       #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ Third party STM Standard Peripheral Library for stm32f1xx
    Architecture="stm32f10x"
    Name="${Install_Dir}"

    ConfigFile="../../999_open_ocd/share/openocd/scripts/target/stm32f10x_flash.cfg"
    if [ ! -e "_$ConfigFile" ]; then
      echo "creating cfg-file '`pwd`/$ConfigFile'.."
      cat <<'END_OF_CONFIG' >$ConfigFile #{
# openocd flash script for stm32f10x


# finish configuration stage + proceed to run stage
init

# hardware reset processor with halt
reset halt

# check target state
poll

# list all found falsh banks
flash banks

# identify the flash
flash probe 0

# unprotect flash for writing
#DEPRECATED flash protect_check 0 0

# erasing all flash
#DEPRECATED stm32x mass_erase 0
stm32f1x mass_erase 0

# write file to flash number 0
flash write_bank 0 main.bin 0

# hardware processor reset with run
reset run

#shutdown openocd daemon
shutdown

END_OF_CONFIG
#}
    else
      echo "cfg-file already exists: '`pwd`/$ConfigFile'.."
    fi
    
  fi #}
  if [ "1" == "1" ]; then #{ low-level driver for cpu on stm32f1xx
  
    DriverName="450_cpu_stm32f1xx"
    createExtensionMakefileHead ${DriverName}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += cpu_stm32f1xx.o

TARGET_ARCHITECTURE_STM32F1xx=1
TARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_STM32F1xx
COMPILE_OPTS += -DTARGET_DATA_WIDTH=32

# architecture specific support code
INCLUDE_DIRS += -Ittc-lib/stm32/ -Ittc-lib/stm32f1/
vpath %.c ttc-lib/stm32/ ttc-lib/stm32f1/

MAIN_OBJS += system_stm32f10x.o
#X MAIN_OBJS += stm32_basic.o

# define linker script to use
LDFLAGS += -Tttc-lib/_linker/memory_stm32f1xx.ld 

#{ Startup codes for all members of STM32 device family

startup_stm32f10x_md.o:    startup_stm32f10x_md.s
startup_stm32f10x_cl.o:    startup_stm32f10x_cl.s
startup_stm32f10x_hd.o:    startup_stm32f10x_hd.s
startup_stm32f10x_hd_vl.o: startup_stm32f10x_hd_vl.s
startup_stm32f10x_ld.o:    startup_stm32f10x_ld.s
startup_stm32f10x_ld_vl.o: startup_stm32f10x_ld_vl.s
startup_stm32f10x_md_vl.o: startup_stm32f10x_md_vl.s
startup_stm32f10x_xl.o:    startup_stm32f10x_xl.s

#X	@ echo ".assembling"
#X	\$(AS) \$(ASFLAGS) \$^

#}
#{ define uController to use (required to include stm32f10x.h) ---------------

# activate only one uC!
# - Low-density devices are STM32F101xx, STM32F102xx and STM32F103xx microcontrollers
#   where the Flash memory density ranges between 16 and 32 Kbytes.
# - Low-density value line devices are STM32F100xx microcontrollers where the Flash
#   memory density ranges between 16 and 32 Kbytes.
# - Medium-density devices are STM32F101xx, STM32F102xx and STM32F103xx microcontrollers
#   where the Flash memory density ranges between 64 and 128 Kbytes.
# - Medium-density value line devices are STM32F100xx microcontrollers where the
#   Flash memory density ranges between 64 and 128 Kbytes.
# - High-density devices are STM32F101xx and STM32F103xx microcontrollers where
#   the Flash memory density ranges between 256 and 512 Kbytes.
# - High-density value line devices are STM32F100xx microcontrollers where the
#   Flash memory density ranges between 256 and 512 Kbytes.
# - XL-density devices are STM32F101xx and STM32F103xx microcontrollers where
#   the Flash memory density ranges between 512 and 1024 Kbytes.
# - Connectivity line devices are STM32F105xx and STM32F107xx microcontrollers.
#
# Examples for some prototype boards
# Olimex STM32-P107  DSTM32F10X_HD
# Olimex STM32-H107  DSTM32F10X_HD
# Olimex STM32-LCD   DSTM32F10X_CL
#
#STM32F10X_LD   =1       # Low density devices
#STM32F10X_LD_VL=1       # Low density Value Line devices
#STM32F10X_MD   =1       # Medium density devices
#STM32F10X_MD_VL=1       # Medium density Value Line devices
#STM32F10X_HD   =1       # High density devices
#STM32F10X_HD_VL=1       # High density value line devices
#STM32F10X_XL   =1       # XL-density devices
#STM32F10X_CL   =1       # Connectivity line devices

# All STM32F1xx have same memory start addresses
COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_START=0x20000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_ACCESS=rxw
COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_START=0x08000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_ACCESS=rx

#{ Define memory regions for each CPU Variant here to allow automatic linker script generation
# Note: This information is taken from cpu_stm32l1xx_types.h and must be kept in sync!

ifdef TTC_CPU_VARIANT_stm32f100rbt6
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32f100rbt6
  STM32F10X_MD   =1       # Medium density devices
  COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_SIZEK=8
  COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_SIZEK=128
endif

ifdef TTC_CPU_VARIANT_stm32f103r6t6a
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32f103r6t6a
  STM32F10X_MD   =1       # Medium density devices
  COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_SIZEK=10
  COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_SIZEK=32
endif

ifdef TTC_CPU_VARIANT_stm32f103r8t6
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32f103r8t6
  STM32F10X_MD   =1       # Medium density devices
  COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_SIZEK=20
  COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_SIZEK=64
endif

ifdef TTC_CPU_VARIANT_stm32f103rbt6
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32f103rbt6
  STM32F10X_MD   =1       # Medium density devices
  COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_SIZEK=20
  COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_SIZEK=128
endif


ifdef TTC_CPU_VARIANT_stm32f103rdy6tr
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32f103rdy6tr
  STM32F10X_HD   =1       # High density devices
  COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_SIZEK=64
  COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_SIZEK=384
endif

ifdef TTC_CPU_VARIANT_stm32f103zet6
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32f103zet6
  STM32F10X_HD   =1       # High density devices
  COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_SIZEK=64
  COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_SIZEK=512
endif

ifdef TTC_CPU_VARIANT_stm32f105rct6
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32f105rct6
  STM32F10X_CL   =1       # Connectivity line devices
  COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_SIZEK=64
  COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_SIZEK=256
endif

ifdef TTC_CPU_VARIANT_stm32f107vct6
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32f107vct6
  STM32F10X_CL   =1       # Connectivity line devices
  COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_SIZEK=64
  COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_SIZEK=256
endif

#}

ifdef STM32F10X_LD
  ifdef uCONTROLLER
    ERROR: STM32F10X_LD=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_LD
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_LD
  COMPILE_OPTS += -DSTM32F10X_LD
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
#}TIMER
  MAIN_OBJS += startup_stm32f10x_ld.o
endif
ifdef STM32F10X_LD_VL
  ifdef uCONTROLLER
    ERROR: STM32F10X_LD_VL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_LD_VL
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_LD_VL
  COMPILE_OPTS += -DSTM32F10X_LD_VL
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
#}TIMER
  MAIN_OBJS += startup_stm32f10x_ld_vl.o
endif
ifdef STM32F10X_MD
  ifdef uCONTROLLER
    ERROR: STM32F10X_MD=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_MD
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_MD
  COMPILE_OPTS += -DSTM32F10X_MD
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
#}TIMER
  MAIN_OBJS += startup_stm32f10x_cl.o
endif
ifdef STM32F10X_MD_VL
  ifdef uCONTROLLER
    ERROR: STM32F10X_MD_VL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_MD_VL
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_MD_VL
  COMPILE_OPTS += -DSTM32F10X_MD_VL
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
#}TIMER
  MAIN_OBJS += startup_stm32f10x_md.o
endif
ifdef STM32F10X_HD
  ifdef uCONTROLLER
    ERROR: STM32F10X_HD=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_HD
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_HD
  COMPILE_OPTS += -DSTM32F10X_HD
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
#}TIMER
  MAIN_OBJS += startup_stm32f10x_hd.o
endif
ifdef STM32F10X_HD_VL
  ifdef uCONTROLLER
    ERROR: STM32F10X_HD_VL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_HD_VL
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_HD_VL
  COMPILE_OPTS += -DSTM32F10X_HD_VL
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
#}TIMER
  MAIN_OBJS += startup_stm32f10x_hd_vl.o
endif
ifdef STM32F10X_XL
  ifdef uCONTROLLER
    ERROR: STM32F10X_XL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_XL
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_XL
  COMPILE_OPTS += -DSTM32F10X_XL
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
  COMPILE_OPTS += -DTTC_TIMER5=9               # physical device index
  COMPILE_OPTS += -DTTC_TIMER6=10               # physical device index
  COMPILE_OPTS += -DTTC_TIMER7=11               # physical device index
  COMPILE_OPTS += -DTTC_TIMER8=12               # physical device index
  COMPILE_OPTS += -DTTC_TIMER9=13               # physical device index
  COMPILE_OPTS += -DTTC_TIMER10=14               # physical device index
  #}TIMER
  MAIN_OBJS += startup_stm32f10x_xl.o
endif
ifdef STM32F10X_CL
  ifdef uCONTROLLER
    ERROR: STM32F10X_CL=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F10X_CL
  COMPILE_OPTS += -DuCONTROLLER=STM32F10X_CL
  COMPILE_OPTS += -DSTM32F10X_CL
  #{ define available TIMER
  COMPILE_OPTS += -DTTC_TIMER1=2               # physical device index
  COMPILE_OPTS += -DTTC_TIMER2=3               # physical device index
  COMPILE_OPTS += -DTTC_TIMER3=4               # physical device index
  COMPILE_OPTS += -DTTC_TIMER4=5               # physical device index
#}TIMER
  MAIN_OBJS += startup_stm32f10x_cl.o
endif

#}

END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$0 2" "low-level cpu Driver for stm32f1xx architecture" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level Driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature "450_cpu_stm32f1xx" "Driver for STM32F1xx microcontrollers"
if [ "\$?" == "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  #X if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
  #X   createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ 'FOO' QUIET
  #X fi
  

  #}
  #{ create link to target script being required by openocd to initialize target cpu
  cd \$Dir_Configs
  createLink ../additionals/999_open_ocd/target/stm32f1x.cfg openocd_target.cfg
  createLink ../additionals/999_open_ocd/target/stm32f10x_flash.cfg openocd_flash.cfg
  cd ..
  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers require a certain file to exist inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.450_ethernet_ste101p -> allows activate.500_ethernet.sh to enable 450_ethernet_ste101p.

  enableFeature 450_i2c_stm32f1xx
  
  # this is also a Cortex M3 CPU  
  enableFeature 450_cpu_cortexm3
  enableFeature 450_cpu_stm32f1xx

  
  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  #X activate.190_cpu_cortexm3.sh QUIET \"\$0\"
  activate.250_CPAL_STM32F10x_StdPeriph_Driver.sh QUIET \"\$0\" # activate corresponding std_peripheral_library
  #X activate.200_cpu_stm32f10x.sh     "\$0" QUIET
  activate.500_ttc_cpu.sh "\$0" QUIET
  #}
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "CPU driver for ARM CortexM3 bases STM32F1xx architecture" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

#X if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
#X   createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
#X fi

#}
#  ACTIVATE_SECTION_E regressions do not provide features 
#{ ACTIVATE_SECTION_F call other activate-scripts

#activate.500_ttc_gpio.sh QUIET "\$0"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ optional: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#  ACTIVATE_SECTION_E examples do not provide features 
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$0"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                               #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------
