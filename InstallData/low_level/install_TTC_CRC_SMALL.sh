#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a single Low-Level Driver
#  for crc devices.
#
#  Created from template _install_TTC_DEVICE_ARCHITECTURE.sh revision 38 at 20180323 10:28:15 UTC
#
#  Supported Architecture: small
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$ScriptName - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="small"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards "extensions"
findFolderUpwards extensions; DirLL_Extensions="$MyStartDir/$ThisArchitecture/$FoundFolder"
findFolderUpwards scripts;    Start_Path="$MyStartDir/$ThisArchitecture/${FoundFolder}.."

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$ScriptName: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$ScriptName - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then       #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir/$ThisArchitecture ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then       #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ low-level driver for crc on small
  
    DriverNameBase="${RANK_LOWLEVEL}_crc_small"
    DriverRevision=""                            # you may add a suffix to the driver name (e.g. "_RevA")
    DriverName="${DriverNameBase}${DriverRevision}"
    createExtensionSourcefileHead ${DriverName}  #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile


END_OF_SOURCEFILE
    createExtensionSourcefileTail ${DriverName} #}    
    createExtensionMakefileHead ${DriverName} "" ${DriverNameBase}   #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += crc_small.o  # ttc-lib/crc/crc_small.c

END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$ScriptName 2" "cyclic redundancy checksum implementations optimized for smallest ram and flash usage" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature "${DriverName}" "$cASH_InfoText"
if [ "\$?" == "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  # crc_small_prepare() typically ist called ttc_crc_prepare() during preparation of all extensions.
  # It should not be required to activate source.${RANK_LOWLEVEL}_*.c here.

  #X if [ -e \$Dir_Extensions/source.${DriverName}.c ]; then
  #X   createLink \$Dir_Extensions/source.${DriverName}.c \$Dir_ExtensionsActive/ '' QUIET
  #X fi

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers activate only if a certain file exists inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.${RANK_LOWLEVEL}_ethernet_ste101p -> allows activate.500_ethernet.sh to enable its low-level driver ${RANK_LOWLEVEL}_ethernet_ste101p.
  # enableFeature NAME  # creates extensions.active/feature.NAME
  # See activate_project.sh of a fresh created project for a list of all available features! 
  
  # enableFeature 450_ethernet_ste101p
  
  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  # activate.500_ttc_memory.sh QUIET "\$ScriptName"
  
  #}
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression_${ThisArchitecture}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# define devices to use in this regression
COMPILE_OPTS += -DTTC_CRC1=E_ttc_crc_architecture_small # Line generated automatically. Remove if not wanted!

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$ScriptName 2" "PLACE YOUR INFO HERE FOR $ScriptName" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  if [ -e \$Dir_Extensions/source.${Name}.c ]; then
    createLink \$Dir_Extensions/source.${Name}.c \$Dir_ExtensionsActive/ '' QUIET
  fi

#}
#{ ACTIVATE_SECTION_E provide default features required to compile this example

  # Read Documentation/TheToolChain-Manual/chapter_Extensions.tml for a description of feature enabling.  

  # enableFeature 450_math_software_float   # enable math driver (single precision)
  # enableFeature 450_math_software_double  # enable math driver (double precision)

#}
#{ ACTIVATE_SECTION_F call other activate-scripts

#activate.500_ttc_gpio.sh QUIET "\$ScriptName"

#}
#{ ACTIVATE_SECTION_G copy source code into project folder if required

if [ ! -d regressions ]; then
  mkdir regressions
fi
ExampleFound=\`ls regressions/regression_${EXTENSION_NAME}_${ThisArchitecture}*\`
if [ "\$ExampleFound" == "" ]; then
  echo "copying missing regression_${EXTENSION_NAME}_${ThisArchitecture}*.."
  cp -v ~/Source/TheToolChain/Template/regressions/regression_${EXTENSION_NAME}_${ThisArchitecture}* regressions/
fi
#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ recommended: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}_${ThisArchitecture}_${ThisArchitecture}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}_${ThisArchitecture}.h"
example_${EXTENSION_NAME}_${ThisArchitecture}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

# additional constant defines
#COMPILE_OPTS += -D

# define devices to use in this example
COMPILE_OPTS += -DTTC_CRC1=E_ttc_crc_architecture_small # Line generated automatically. Remove if not wanted!

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}_${ThisArchitecture}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$ScriptName 2" "Usage example for extension ${EXTENSION_NAME}_${ThisArchitecture}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/source.${Name}.c ]; then
  createLink \$Dir_Extensions/source.${Name}.c \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#{ ACTIVATE_SECTION_E provide default features required to compile this example

  # Read Documentation/TheToolChain-Manual/chapter_Extensions.tml for a description of feature enabling.
  enableFeature $DriverName  # enable low-level driver to be tested

  # enableFeature 450_math_software_float   # enable math driver (single precision)
  # enableFeature 450_math_software_double  # enable math driver (double precision)

#}
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$ScriptName" # activate high-level driver

#}
#{ ACTIVATE_SECTION_G copy source code into project folder if required

if [ ! -d examples ]; then
  mkdir examples
fi
ExampleFound=\`ls examples/example_${EXTENSION_NAME}_${ThisArchitecture}*\`
if [ "\$ExampleFound" == "" ]; then
  echo "copying missing example_${EXTENSION_NAME}.."
  cp -v ~/Source/TheToolChain/Template/examples/example_${EXTENSION_NAME}_${ThisArchitecture}* examples/
fi
#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                               #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------
