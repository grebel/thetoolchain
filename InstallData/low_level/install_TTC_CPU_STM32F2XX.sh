#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a single Low-Level Driver
#  for cpu devices.
#
#  Created from template _install_NN_TTC_DEVICE_ARCHITECTURE.sh revision 27 at 20150819 17:55:07 UTC
#
#  Supported Architecture: stm32f2xx
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$0 - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="stm32f2xx"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards "extensions"
DirLL_Extensions="$FoundFolder"

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then       #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir/$ThisArchitecture ..."
    Install_Path=`pwd`
    
    Error=""
    
    Archive="stm32f2xx_stdperiph_lib.zip"
    getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ $Archive

    if [ ! -e $Archive ]; then
      Error="Cannot download $Archive!"
    fi
    unZIP STM32F10x_StdPeriph_Lib $Archive || mv -f $Archive ${Archive}_bad
    rm current 2>/dev/null

    # find name of only directory
    LibraryDir=`find ./  -maxdepth 1 -mindepth 1 -type d -print`
    createLink $LibraryDir current
    cd current

    for File in `ls *.chm`; do
      addDocumentationFile "$File" uC/STM32F2xx
    done
    cd $Install_Path
  
    if [ "$Error" == "" ]; then
      touch OK.Install
    else
      echo "$0 - ERROR: $ERROR!"
      exit 10
    fi

  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then       #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ low-level driver for cpu on stm32f2xx
  
    DriverName="450_cpu_stm32f2xx"
    Name="${Install_Dir}"
    LibPrefix="250_stm_std_peripherals_"
    LibName="${LibPrefix}f2xx"

    if [ "0" == "1" ]; then #{ DISABLED: std_peripheral_library_f2xx ToDo: Enable it
      createExtensionMakefileHead ${LibName} #{       create makefile for std_peripheral_library
      File="${Dir_Extensions}makefile.${LibName}" 
      cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DEXTENSION_250_stm_std_peripherals=1

INCLUDE_DIRS += -I additionals/250_stm_std_peripherals/STM32F10x_StdPeriph_Driver/inc/ \\
                -I additionals/250_stm_std_peripherals/CMSIS/CM3/DeviceSupport/ST/STM32F2xx/ \\
                -I additionals/250_stm_std_peripherals/CMSIS/CM3/CoreSupport/

vpath %.c additionals/250_stm_std_peripherals/STM32F2xx_StdPeriph_Driver/src/ \\
          additionals/250_stm_std_peripherals/CMSIS/CM3/DeviceSupport/ST/STM32F2xx/

vpath %.s additionals/250_stm_std_peripherals/CMSIS/CM3/DeviceSupport/ST/STM32F2xx/startup/gcc_ride7

END_OF_MAKEFILE
      createExtensionMakefileTail ${LibName} #}
      createActivateScriptHead $LibName ${Dir_Extensions} $0 "Standard Peripheral Library for CPU STM32F2xx" #{
      cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${LibPrefix}*

OldPWD="\`pwd\`"
cd "\${Dir_Extensions}/"
rm 2>/dev/null makefile.${LibPrefix}_*
rm 2>/dev/null activate.${LibPrefix}_*
ln -sv $ThisArchitecture/* .
cd "\$OldPWD"

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$LibName \${Dir_ExtensionsActive}/makefile.$LibName '' QUIET

# we must create a new link in additionals
if [ -d "\$Dir_Additionals" ]; then
  cd "\$Dir_Additionals"
  rm 2>/dev/null 250_stm_std_peripherals
  createLink 250_STM32F2xx_StdPeriph_Driver  \$Dir_Additionals/250_stm_std_peripherals
  cd ..
else
  echo "\$0 - ERROR: Cannot find directory additionals/ (\`pwd\`/\$Dir_Additionals)!"
fi

END_OF_ACTIVATE
#}
      createActivateScriptTail $LibName ${Dir_Extensions}
      #}

      BaseName=$LibName
      dir ${Dir_Extensions}$ThisArchitecture
  
      # create one activate-script per feature
      for Feature in `find current/Libraries/STM32F2xx_StdPeriph_Driver/src/ -name "${Architecture}_*" -execdir echo -n "{} " \;` ; do #{
        Feature=`perl -e "print substr('$Feature', 12, -2);"`
  
        FeatureName="${LibPrefix}_${Feature}"
        #echo "FeatureName='$FeatureName'" #D
        createExtensionMakefileHead ${FeatureName} ${Dir_Extensions}${Architecture}/ #{
        cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # set by createExtensionMakefileHead() 

MAIN_OBJS += ${Architecture}_${Feature}.o

END_OF_MAKEFILE
        createExtensionMakefileTail ${FeatureName} ${Dir_Extensions}${Architecture}/ #}
        createActivateScriptHead $FeatureName ${Dir_Extensions}${Architecture}/ $0 "append C-source from standar peripheral library for compilation: ${FeatureName}.c" #{
        cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# add createL.inks for each link required for this extension
createLink ${Dir_Extensions}makefile.$FeatureName \${Dir_ExtensionsActive}/makefile.$FeatureName '' QUIET

END_OF_ACTIVATE
#}
        createActivateScriptTail $FeatureName ${Dir_Extensions}
        #}
      done #}
    fi #}std_peripheral_library_f2xx

    createExtensionSourcefileHead ${DriverName}  #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile


END_OF_SOURCEFILE
    createExtensionSourcefileTail ${DriverName} #}    
    createExtensionMakefileHead ${DriverName}    #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += cpu_stm32f2xx.o  # ttc-lib/cpu/cpu_stm32f2xx.c

COMPILE_OPTS += -DEXTENSION_${Name}=1

ARM_CPU     =arm7tdmi
TARGET      =-mcpu=cortex-m3 -mthumb
TARGET_ARCHITECTURE_STM32F1xx=1
TARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_STM32F1xx

# architecture specific support code
#X INCLUDE_DIRS += -Ittc-lib/stm32/
#X vpath %.c ttc-lib/stm32/ 

# define linker script to use
LDFLAGS += -Tttc-lib/_linker/memory_stm32f1xx.ld 

END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$0 2" "low-level cpu driver for stm32f2xx architecture" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature ""
if [ "\$?" != "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
    createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
  fi

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers require a certain file to exist inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.450_ethernet_ste101p -> allows activate.500_ethernet.sh to enable 450_ethernet_ste101p.
  # enableFeature NAME  # creates extensions.active/feature.NAME 
  
  # enableFeature 450_ethernet_ste101p
  
  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  # activate.500_ttc_memory.sh QUIET "\$0"
  # activate corresponding std_peripheral_library
  activate.250_CPAL_STM32F2xx_StdPeriph_Driver.sh QUIET

  #}
  #{ create link to target script being required by openocd to initialize target cpu
    cd \$Dir_Configs
    createLink ../additionals/999_open_ocd/target/stm32f2x.cfg openocd_target.cfg 
    cd ..
  #}
  
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#  ACTIVATE_SECTION_E regressions do not provide features 
#{ ACTIVATE_SECTION_F call other activate-scripts

#activate.500_ttc_gpio.sh QUIET "\$0"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ optional: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#  ACTIVATE_SECTION_E examples do not provide features 
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$0"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                               #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------
