#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a single Low-Level Driver
#  for cpu devices.
#
#  Created from template _install_NN_TTC_DEVICE_ARCHITECTURE.sh revision 25 at 20150322 21:39:33 UTC
#
#  Supported Architecture: stm32l1xx
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$0 - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="stm32l1xx"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards extensions; DirLL_Extensions="$MyStartDir/$ThisArchitecture/$FoundFolder"
findFolderUpwards scripts;    Start_Path="$MyStartDir/$ThisArchitecture/${FoundFolder}.."

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  getDocumentation http://www.st.com/web/en/resource/technical/document/datasheet/                            CD00277537.pdf STM32L15xxx-Datasheet.pdf                                       uC/STM32L1xx/
  getDocumentation http://www.st.com/web/en/resource/technical/document/datasheet/                            DM00078075.pdf STM32L100xx-Datasheet.pdf                                       uC/STM32L1xx/
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/reference_manual/ CD00240193.pdf RM0038_STM32l1xx-Reference-Manual                               uC/STM32L1xx/
  getDocumentation http://www.st.com/web/en/resource/technical/document/application_note/                     CD00273528.pdf AN3216_Getting_started_with_STM32L1xxx_hardware_development.pdf uC/STM32L1xx/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then       #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir/$ThisArchitecture ..."
    Install_Path=`pwd`
    
    Archive="stsw-stm32077.zip"
    getFile http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/firmware/ $Archive $Archive
    
    if [ ! -e $Archive ]; then
        Error="Cannot download $Archive!"
    fi
    
    unZIP STM32L1xx_StdPeriph_Lib_V1.2.0 $Archive || mv -f $Archive ${Archive}_bad
    
    rm current 2>/dev/null
    
    LibraryDir=`find ./  -maxdepth 1 -mindepth 1 -type d -print`
    createLink $LibraryDir current
    cd current
    #X dir ../../../Documentation/STM
    #X mv *.chm ../../../Documentation/STM/
    for File in `ls *.chm`; do
      addDocumentationFile "$File" uC/STM32L1xx
    done
    
    File="Libraries/CMSIS/Device/ST/STM32L1xx/Include/stm32l1xx.h"
    if [ -e ${File}_orig ]; then
          mv -f ${File}_orig $File 
    fi
    mv -f $File ${File}_orig
    
    echo "  May need to create macro assert_param in $File"
    Note="  Note: If this is done, manually remove assert_param from your stm32l1xx_conf.h !"
      echo $Note
      addLine ../Notes.txt $Note
      addLine ../Notes.txt "StdPeripheralLibrary-> http://www.mikrocontroller.net/articles/STM32F10x_Standard_Peripherals_Library"
      
    
    DestFolder="Libraries/CMSIS/Device/ST/STM32L1xx/Include"
    if [ ! -e $DestFolder/system_stm32l1xx.c ]; then
        # system_stm32l1xx.c is required to be found by the compiler so we put it aside its header file
        #cp -v Project/STM32F10x_StdPeriph_Template/system_stm32f10x.c $DestFolder
        addLine ../Notes.txt "system_stm32f10x.c NOT FOUND. Move beside its header file (-> https://my.st.com/public/STe2ecommunities/mcu/Lists/ARM%20CortexM3%20STM32/Flat.aspx?RootFolder=%2Fpublic%2FSTe2ecommunities%2Fmcu%2FLists%2FARM%20CortexM3%20STM32%2FMissing%20file%20in%20new%203.4.0%20libraries%20system_stm32f10x.c&FolderCTID=0x01200200770978C69A1141439FE559EB459D758000626BE2B829C32145B9EB5739142DC17E&currentviews=320)"
    fi
    
    
    cd ..
    
    #{Was install successful?
    if [ "$Error" == "" ]; then
        touch OK.Install
      else
        echo "$0 - ERROR: $ERROR!"
        exit 10
    fi
    #}------------------------
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then       #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ low-level driver for cpu on stm32l1xx
  
    #{ Write config file for openocd
    ConfigFile="../../999_open_ocd/share/openocd/scripts/target/stm32l1xx_flash.cfg"
    if [ ! -e "_$ConfigFile" ]; then
      echo "creating cfg-file '`pwd`/$ConfigFile'.."
      cat <<'END_OF_CONFIG' >$ConfigFile #{Code added by $0
# openocd flash script for stm32l1xx

# finish configuration stage + proceed to run stage
init

# hardware reset processor with halt
reset halt

# check target state
poll

# list all found falsh banks
flash banks

# identify the flash
flash probe 0

# unprotect flash for writing
#DEPRECATED flash protect_check 0 0

# erasing all flash
stm32lx mass_erase 0

# write file to flash number 0
flash write_bank 0 main.bin 0

# hardware processor reset with run
reset run

#shutdown openocd daemon
shutdown
#}END Code added by $0

END_OF_CONFIG
    else
      echo "cfg-file already exists: '`pwd`/$ConfigFile'.."
    fi
    #}END Write config file for openocd

    LibPrefix="250_stm_std_peripherals_"
    LibName="${LibPrefix}l1xx"

    DriverName="450_cpu_stm32l1xx"
    createExtensionMakefileHead ${DriverName}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += cpu_stm32l1xx.o

#ARM_CPU     =arm7tdmi
TARGET      =-mcpu=cortex-m3 -mthumb
TARGET_ARCHITECTURE_STM32L1xx=1
TARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_STM32L1xx
COMPILE_OPTS += -DTARGET_DATA_WIDTH=32
COMPILE_OPTS += -DUSE_STDPERIPH_DRIVER
COMPILE_OPTS += -DUSE_FULL_ASSERT=1
COMPILE_OPTS += -DTTC_REAL_TIME_CLOCK_AMOUNT=1
COMPILE_OPTS += -DTTC_REAL_TIME_CLOCK1
COMPILE_OPTS += -DTTC_INTERRUPT_GPIO_AMOUNT=16

#X INCLUDE_DIRS += -Ittc-lib/stm32/ -Ittc-lib/stm32l1/
#X vpath %.c ttc-lib/stm32/ ttc-lib/resgister/
#X vpath %.s additionals/270_CPAL_CMSIS/CM3/DeviceSupport/ST/STM32L1xx/startup/gcc_ride7

# All STM32L1xx have same memory start addresses
COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_START=0x20000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_ACCESS=rxw
COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_START=0x08000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_ACCESS=rx
COMPILE_OPTS += -DTTC_MEMORY_REGION_EEPROM_START=0x08080000
COMPILE_OPTS += -DTTC_MEMORY_REGION_EEPROM_ACCESS=rx
COMPILE_OPTS += -DTTC_MEMORY_REGION_MB1_SIZEK=0
COMPILE_OPTS += -DTTC_MEMORY_REGION_MB1_START=0x60000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_MB1_ACCESS=rx

#{ Define memory regions for each CPU Variant here to allow automatic linker script generation
# Note: This information is taken from cpu_stm32l1xx_types.h and must be kept in sync!

ifdef TTC_CPU_VARIANT_stm32l100rbt6
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32l100rbt6
  COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_SIZEK=10
  COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_SIZEK=128
  COMPILE_OPTS += -DTTC_MEMORY_REGION_EEPROM_SIZEK=2
endif

ifdef TTC_CPU_VARIANT_stm32l100rct6
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32l100rct6
  COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_SIZEK=16
  COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_SIZEK=256
  COMPILE_OPTS += -DTTC_MEMORY_REGION_EEPROM_SIZEK=2
endif

ifdef TTC_CPU_VARIANT_stm32l152rbt6
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32l152rbt6
  COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_SIZEK=16
  COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_SIZEK=128
  COMPILE_OPTS += -DTTC_MEMORY_REGION_EEPROM_SIZEK=4
endif

#}

#{ Timer #1
# okular ~/Source/TheToolChain/Documentation/uC/STM32L1xx/STM32L100xx-Datasheet.pdf (DocID024295 Rev.3) p.32-34

COMPILE_OPTS += -DTTC_TIMER1=ttc_device_2       # no TIM1 available on STM32
COMPILE_OPTS += -DTTC_TIMER1_CHANNELS=4         # amount of channels available
COMPILE_OPTS += -DTTC_TIMER1_WIDTH=16           # 16-bit wide timer counter
COMPILE_OPTS += -DTTC_TIMER1_UP=1               # timer can count upwards
COMPILE_OPTS += -DTTC_TIMER1_DOWN=1             # timer can count downwards
COMPILE_OPTS += -DTTC_TIMER1_UP_DOWN=1          # timer can count up- and downwards
COMPILE_OPTS += -DTTC_TIMER1_PRESCALE_MIN=1     # minimum prescaler value
COMPILE_OPTS += -DTTC_TIMER1_PRESCALE_MAX=65535 # maximum prescaler value

COMPILE_OPTS += -DTTC_TIMER1_PWM=4              # amount of PWM output pins connected to this timer
COMPILE_OPTS += -DTTC_TIMER1_PWM1_PIN1=E_ttc_gpio_pin_a15 # gpio pin #1 connected to pwm channel #1
COMPILE_OPTS += -DTTC_TIMER1_PWM2_PIN1=E_ttc_gpio_pin_b3  # gpio pin #1 connected to pwm channel #2
COMPILE_OPTS += -DTTC_TIMER1_PWM3_PIN1=E_ttc_gpio_pin_b10 # gpio pin #1 connected to pwm channel #3
COMPILE_OPTS += -DTTC_TIMER1_PWM4_PIN1=E_ttc_gpio_pin_b11 # gpio pin #1 connected to pwm channel #4
# STM32L1xx may change pin layout via alternate function:
COMPILE_OPTS += -DTTC_TIMER1_PWM1_PIN2=E_ttc_gpio_pin_a0  # gpio pin #2 connected to pwm channel #1
COMPILE_OPTS += -DTTC_TIMER1_PWM2_PIN2=E_ttc_gpio_pin_a1  # gpio pin #2 connected to pwm channel #2
COMPILE_OPTS += -DTTC_TIMER1_PWM3_PIN2=E_ttc_gpio_pin_a2  # gpio pin #2 connected to pwm channel #3
COMPILE_OPTS += -DTTC_TIMER1_PWM4_PIN2=E_ttc_gpio_pin_a3  # gpio pin #2 connected to pwm channel #4

COMPILE_OPTS += -DTTC_TIMER1_CAPTURES=4         # amount of capture channels of this timer
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE1_PIN1=E_ttc_gpio_pin_a15 # gpio pin #1 connected to capture channel #1
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE2_PIN1=E_ttc_gpio_pin_b3  # gpio pin #1 connected to capture channel #2
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE3_PIN1=E_ttc_gpio_pin_b10 # gpio pin #1 connected to capture channel #3
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE4_PIN1=E_ttc_gpio_pin_b11 # gpio pin #1 connected to capture channel #4
# STM32L1xx may change pin layout via alternate function:
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE1_PIN2=E_ttc_gpio_pin_a0  # gpio pin #2 connected to capture channel #1
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE2_PIN2=E_ttc_gpio_pin_a1  # gpio pin #2 connected to capture channel #2
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE3_PIN2=E_ttc_gpio_pin_a2  # gpio pin #2 connected to capture channel #3
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE4_PIN2=E_ttc_gpio_pin_a3  # gpio pin #2 connected to capture channel #4

#}
# ToDo: Complete timer configuration for all timers...

COMPILE_OPTS += -DTTC_TIMER2=ttc_device_3
COMPILE_OPTS += -DTTC_TIMER3=ttc_device_4
COMPILE_OPTS += -DTTC_TIMER4=ttc_device_6
COMPILE_OPTS += -DTTC_TIMER5=ttc_device_7
COMPILE_OPTS += -DTTC_TIMER6=ttc_device_9


COMPILE_OPTS += -DTTC_TIMER7=ttc_device_10
COMPILE_OPTS += -DTTC_TIMER7_PWM=1              # amount of PWM output pins connected to this timer
COMPILE_OPTS += -DTTC_TIMER7_PWM1_PIN1=E_ttc_gpio_pin_b12 # gpio pin #1 connected to pwm channel #1
# STM32L1xx may change pin layout via alternate function:
COMPILE_OPTS += -DTTC_TIMER7_PWM1_PIN2=E_ttc_gpio_pin_b98  # gpio pin #2 connected to pwm channel #1

COMPILE_OPTS += -DTTC_TIMER7_CAPTURES=1         # amount of capture channels of this timer
COMPILE_OPTS += -DTTC_TIMER7_CAPTURE1_PIN1=E_ttc_gpio_pin_b12 # gpio pin #1 connected to capture channel #1
COMPILE_OPTS += -DTTC_TIMER7_CAPTURE1_PIN2=E_ttc_gpio_pin_b8  # gpio pin #2 connected to capture channel #1

COMPILE_OPTS += -DTTC_TIMER8=ttc_device_11
COMPILE_OPTS += -DTTC_TIMER8_PWM=1              # amount of PWM output pins connected to this timer
COMPILE_OPTS += -DTTC_TIMER8_PWM1_PIN1=E_ttc_gpio_pin_b15 # gpio pin #1 connected to pwm channel #1
# STM32L1xx may change pin layout via alternate function:
COMPILE_OPTS += -DTTC_TIMER8_PWM1_PIN2=E_ttc_gpio_pin_b9  # gpio pin #2 connected to pwm channel #1

COMPILE_OPTS += -DTTC_TIMER8_CAPTURES=1         # amount of capture channels of this timer
COMPILE_OPTS += -DTTC_TIMER8_CAPTURE1_PIN1=E_ttc_gpio_pin_b15 # gpio pin #1 connected to capture channel #1
# STM32L1xx may change pin layout via alternate function:
COMPILE_OPTS += -DTTC_TIMER8_CAPTURE1_PIN2=E_ttc_gpio_pin_b9  # gpio pin #2 connected to capture channel #1

MAIN_OBJS += system_stm32l1xx.o 

# define linker script to use
LDFLAGS += -Tttc-lib/_linker/memory_stm32l1xx.ld -mthumb -mcpu=cortex-m3

#{Startup codes for STM32L1xx family

startup_stm32l1xx_md.o:		startup_stm32l1xx_md.s
startup_stm32l1xx_mdp.o:	startup_stm32l1xx_mdp.s
startup_stm32l1xx_hd.o:		startup_stm32l1xx_hd.s

#}

#{ define uController to use (required to include stm32l1xx.h) ---------------

ifdef TTC_CPU_VARIANT_stm32l100rbt6  
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32l100rbt6
  STM32L1XX_MD   =1       # Medium density devices
endif

ifdef TTC_CPU_VARIANT_stm32l100rct6  
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32l100rct6  
  STM32L1XX_MD   =1       # Medium density devices
endif

ifdef TTC_CPU_VARIANT_stm32l152rbt6  
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32l152rbt6  
  STM32L1XX_MD   =1       # Medium density devices
endif


# Microcontroller Class: activate only one uC!
# - Medium density plus STM32L15xxC devices are microcontrollers with a Flash memory density of 256 Kbytes.

#STM32L1XX_MD   =1       # Medium density devices
#STM32L1XX_MDP  =1       # Medium density plus Line devices
#STM32L1XX_HD   =1       # High density devices


ifdef STM32L1XX_MD
  ifdef uCONTROLLER
    ERROR: STM32L1XX_MD=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32L1XX_MD
  COMPILE_OPTS += -DuCONTROLLER=STM32L1XX_MD
  COMPILE_OPTS += -DSTM32L1XX_MD
  MAIN_OBJS += startup_stm32l1xx_md.o
endif

ifdef STM32L1XX_MDP
  ifdef uCONTROLLER
    ERROR: STM32L1XX_MDP=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32L1XX_MDP
  COMPILE_OPTS += -DuCONTROLLER=STM32L1XX_MDP
  COMPILE_OPTS += -DSTM32L1XX_MDP
  MAIN_OBJS += startup_stm32l1xx_mdp.o
endif

ifdef STM32L1XX_HD
  ifdef uCONTROLLER
    ERROR: STM32L1XX_HD=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32L1XX_HD
  COMPILE_OPTS += -DuCONTROLLER=STM32L1XX_HD
  COMPILE_OPTS += -DSTM32L1XX_HD
  MAIN_OBJS += startup_stm32l1xx_hd.o
endif

ifndef uCONTROLLER
  ERROR: No microcontroler class selected. Define one from (STM32L1XX_MD, STM32L1XX_MDP, STM32L1XX_HD)
endif

#}END define uController

END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$0 2" "low-level cpu driver for stm32l1xx architecture" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature "450_cpu_stm32l1xx" "Driver for STM32L1xx microcontrollers"
if [ "\$?" == "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

#X   if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
#X     createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
#X   fi

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers require a certain file to exist inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.450_ethernet_ste101p -> allows activate.500_ethernet.sh to enable 450_ethernet_ste101p.

  # this is also a Cortex M3 CPU
  enableFeature 450_cpu_cortexm3
  enableFeature 450_cpu_stm32l1xx
  activate.450_cpu_cortexm3.sh      "\$0" QUIET
  #X activate.200_cpu_stm32l1xx.sh     "\$0" QUIET

  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
    # activate.500_ttc_memory.sh QUIET "\$0"
  #X activate.190_cpu_cortexm3.sh                    QUIET "\$0"
  activate.250_CPAL_STM32L1xx_StdPeriph_Driver.sh QUIET "\$0"
  #DEPRECATED (should not longer be required) activate.250_stm_std_peripherals__rcc.sh        QUIET "\$0"
  #DEPRECATED (should not longer be required) activate.250_stm_std_peripherals__exti.sh       QUIET "\$0"
  #DEPRECATED (should not longer be required) activate.250_stm_std_peripherals__syscfg.sh     QUIET "\$0"
  
  # create link to target script being required by openocd to initialize target cpu
  cd \$Dir_Configs
  createLink ../additionals/999_open_ocd/target/stm32l1.cfg openocd_target.cfg
  createLink ../additionals/999_open_ocd/target/stm32l1xx_flash.cfg openocd_flash.cfg
  cd ..
  #}
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#  ACTIVATE_SECTION_E regressions do not provide features 
#{ ACTIVATE_SECTION_F call other activate-scripts

#activate.500_ttc_gpio.sh QUIET "\$0"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ optional: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#  ACTIVATE_SECTION_E examples do not provide features 
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$0"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                               #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------
