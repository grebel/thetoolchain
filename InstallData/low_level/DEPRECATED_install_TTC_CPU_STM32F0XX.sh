#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2017.
#
#  Install script for a single Low-Level Driver
#  for cpu devices.
#
#  Created from template _install_NN_TTC_DEVICE_ARCHITECTURE.sh revision 27 at 20150819 17:13:10 UTC
#
#  Supported Architecture: stm32f0xx
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$0 - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="stm32f0xx"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards extensions; DirLL_Extensions="$MyStartDir/$ThisArchitecture/$FoundFolder"
findFolderUpwards scripts;    Start_Path="$MyStartDir/$ThisArchitecture/${FoundFolder}.."

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.st.com/web/en/resource/technical/document/programming_manual/                    DM00051352.pdf  STM32F0xxxCortex-M0_programming_manual.pdf          uC/STM32F0xx 
  getDocumentation http://www.st.com/web/en/resource/technical/document/datasheet/                             DM00088500.pdf  STM32F030x4_STM32F030x6_STM32F030x8.pdf             uC/STM32F0xx
  getDocumentation http://www.st.com/web/en/resource/technical/document/datasheet/                             DM00065136.pdf  STM32F050x4_STM32F050x6.pdf                         uC/STM32F0xx
  getDocumentation http://www.st.com/web/en/resource/technical/document/datasheet/                             DM00039193.pdf  STM32F051x4_STM32F051x6_STM32F051x8                 uC/STM32F0xx
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/REFERENCE_MANUAL/   CD00240193.pdf  STM32_RM0038_Advanced_ARM-based_32bit_MCUs.pdf      uC/STM32F0xx
  getDocumentation http://www.st.com/web/en/resource/technical/document/reference_manual/                      DM00031936.pdf  Advanced_developers_guide_for-STM32F05xxx/06xxx.pdf uC/STM32F0xx
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/application_note/  DM00052530.pdf  Migrating_from_STM32F1_to_STM32F0.pdf               uC/STM32F0xx

  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then       #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir/$ThisArchitecture ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    Error=""
      
    Archive="stm32f0_stdperiph_lib.zip"
    getFile http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/firmware/ $Archive $Archive
    
    if [ ! -e $Archive ]; then
        Error="Cannot download $Archive!"
    fi
    
    unZIP STM32F0xx_StdPeriph_Lib_V1.0.0 $Archive || mv -f $Archive ${Archive}_bad
    
    rm current 2>/dev/null
    LibraryDir=`find ./  -maxdepth 1 -mindepth 1 -type d -print`
    createLink $LibraryDir current
    cd current
    for File in `ls *.chm`; do
      addDocumentationFile "$File" uC/STM32F0xx
    done

    
    echo "  May need to create macro assert_param in $File"
    Note="  Note: If this is done, manually remove assert_param from your stm32f0xx_conf.h !"
    echo $Note
    addLine ../Notes.txt $Note
    addLine ../Notes.txt "StdPeripheralLibrary-> http://www.mikrocontroller.net/articles/STM32F10x_Standard_Peripherals_Library"
      
    
    DestFolder="Libraries/CMSIS/Device/ST/STM32F0xx/Include"
    if [ ! -e $DestFolder/system_stm32f0xx.c ]; then
      # system_stm32f0xx.c is required to be found by the compiler so we put it aside its header file
      #cp -v Project/STM32F10x_StdPeriph_Template/system_stm32f10x.c $DestFolder
      addLine ../Notes.txt "system_stm32f10x.c NOT FOUND. Move beside its header file (-> https://my.st.com/public/STe2ecommunities/mcu/Lists/ARM%20CortexM3%20STM32/Flat.aspx?RootFolder=%2Fpublic%2FSTe2ecommunities%2Fmcu%2FLists%2FARM%20CortexM3%20STM32%2FMissing%20file%20in%20new%203.4.0%20libraries%20system_stm32f10x.c&FolderCTID=0x01200200770978C69A1141439FE559EB459D758000626BE2B829C32145B9EB5739142DC17E&currentviews=320)"
    fi
    
    cd $Install_Path

    if [ "$Error" == "" ]; then
      touch OK.Install
    else
      echo "$0 - ERROR: $ERROR!"
      exit 10
    fi
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then       #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ low-level driver for cpu on stm32f0xx
    DriverName="450_cpu_stm32f0xx"
    #? Architecture="stm32f0xx"
    Name="${Install_Dir}"
    
    #{Write config file for openocd
    ConfigFile="../../999_open_ocd/share/openocd/scripts/target/stm32f0xx_flash.cfg"
    if [ ! -e "_$ConfigFile" ]; then
      echo "creating cfg-file '`pwd`/$ConfigFile'.."
      cat <<'END_OF_CONFIG' >$ConfigFile #{Code added by install_15_CPU_STM32F0xx.sh
# openocd flash script for stm32f10x


# finish configuration stage + proceed to run stage
init

# hardware reset processor with halt
reset halt

# check target state
poll

# list all found falsh banks
flash banks

# identify the flash
flash probe 0

# unprotect flash for writing
#DEPRECATED flash protect_check 0 0

# erasing all flash
#DEPRECATED stm32x mass_erase 0
stm32f1x mass_erase 0

# write file to flash number 0
flash write_bank 0 main.bin 0

# hardware processor reset with run
reset run

#shutdown openocd daemon
shutdown
#}END Code added by install_13_CPU_STM32F0xx.sh

END_OF_CONFIG
    else
      echo "cfg-file already exists: '`pwd`/$ConfigFile'.."
    fi
    #}END Write config file for openocd
    
    LibPrefix="250_stm_std_peripherals_"
    LibName="${LibPrefix}f0xx"

    cd current/Libraries/
    if [ -d CMSIS/ ]; then
      mv CMSIS/ STM32F0xx_CMSIS/
    fi
    SubDirs=`ls */ -d`
    for SubDir in $SubDirs; do #{ create link into additionals/ for each sub directory
      SubDir=`perl -e "print substr('${SubDir}', 0, -1);"` # remove trailing /
      echo "sub library: $SubDir"
      echo $SubDirs
      Link="250_$SubDir"
      echo $Install_Dir/Libraries/$SubDir
      addLine $Start_Path/scripts/createLinks.sh "rm 2>/dev/null $Link;  createLink \$Source/TheToolChain/InstallData/200_cpu_stm32f0xx/current/Libraries/$SubDir  $Link"
    done #}
    
    SubDirs=`ls -d *_StdPeriph_Driver`
    for SubDir in $SubDirs; do #{ create makefile + activate script for each *_StdPeriph_Driver directory
      Name="250_${SubDir}" # std peripheral uses different rank than CPAL!
      Architecture=`perl -e "print substr('${SubDir}', 0, 9);"`
  
      createExtensionMakefileHead ${Name} #{      create makefile for std_peripheral_library
      cat <<END_OF_MAKEFILE >>$ExtensionMakeFile

COMPILE_OPTS += -DEXTENSION_250_stm_std_peripherals=1

INCLUDE_DIRS += -I additionals/250_STM32F0xx_CMSIS/Include/ \\
              -I additionals/250_STM32F0xx_CPAL_Driver/inc/ \\
              -I additionals/250_STM32F0xx_StdPeriph_Driver/inc/ \\
              -I additionals/250_STM32F0xx_CMSIS/Device/ST/STM32F0xx/Include
vpath %.c additionals/250_STM32F0xx_StdPeriph_Driver/src/ \\
        additionals/250_STM32F0xx_CPAL_Driver/src/

vpath %.s additionals/250_STM32F0xx_CMSIS/Device/ST/STM32F0xx/Source/Templates/gcc_ride7/

MAIN_OBJS += misc.o

END_OF_MAKEFILE
      createExtensionMakefileTail ${Name} #}
      createActivateScriptHead $Name $DirLL_Extensions/ "$0 1" "Architecture dependent standard peripheral library for $SubDir microcontrollers" #{ 
      cat <<END_OF_ACTIVATE >>$ActivateScriptFile
#!/bin/bash

Module_ERROR=""

# remove existing links
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Name}
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Install_Dir}_*_StdPeriph_Driver


OldPWD=\`pwd\`

# create links into extensions/
cd "\${Dir_Extensions}"
rm 2>/dev/null *250_stm_std_peripherals_*

DriversFound=\`ls $Architecture/*\`
if [ "\$DriversFound" != "" ]; then
ln -sv $Architecture/* .
else
Module_ERROR="Cannot find drivers in \`pwd\`/$Architecture/ !"
fi

cd "\$CurrentPath"
if [ "\$Module_ERROR" == "" ]; then
# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
else
echo "\$0 - ERROR: \$Module_ERROR"
fi

END_OF_ACTIVATE
#}
      createActivateScriptTail $Name $DirLL_Extensions/
      
      #{ create one activate-script per c-file
      #XTargetDir="../../${Dir_Extensions}$Architecture"
      TargetDir="$DirLL_Extensions/$Architecture"
      dir "$TargetDir"
  
      LibPrefix="250_stm_std_peripherals"
      for SourceFile in `find ${SubDir}/src/ -name "*\.c" -execdir echo -n "{} " \;` ; do #{
        SourceName=`perl -e "print substr('$SourceFile', 2, -2);"`
        Source=`perl -e "print substr('$SourceFile', 12, -2);"`
        if [ "$Source" != "" ]; then
          DestName="${LibPrefix}__${Source}"
          createExtensionMakefileHead ${DestName} $TargetDir/ #{
          cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # set by createExtensionMakefileHead() 

MAIN_OBJS += ${SourceName}.o

END_OF_MAKEFILE
          createExtensionMakefileTail ${DestName} $TargetDir/ #}
          createActivateScriptHead $DestName $TargetDir/ $0 "adds c-source to list of compiled objects: ${SourceName}.c" #{
          cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# add createL.inks for each link required for this extension
createLink ${Dir_Extensions}makefile.$DestName \${Dir_ExtensionsActive}/makefile.$DestName '' QUIET

END_OF_ACTIVATE
#}
          createActivateScriptTail $DestName $TargetDir/
          #}
        fi
      done #}
      #} create one activate-script per c-file
    done #}

    cd "$OldPWD"

    createExtensionSourcefileHead ${DriverName}   #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile


END_OF_SOURCEFILE
    createExtensionSourcefileTail ${DriverName} #}    
    createExtensionMakefileHead ${DriverName}     #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += cpu_stm32f0xx.o # ttc-lib/cpu/cpu_stm32f0xx.c

#ARM_CPU     =arm7tdmi
TARGET      =-mcpu=cortex-m3 -mthumb
TARGET_ARCHITECTURE_STM32F0xx=1
TARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_STM32F0xx
COMPILE_OPTS += -DTARGET_DATA_WIDTH=32
COMPILE_OPTS += -DUSE_STDPERIPH_DRIVER
COMPILE_OPTS += -DUSE_FULL_ASSERT

# architecture specific support code
INCLUDE_DIRS += -Ittc-lib/stm32/ -Ittc-lib/stm32f0/

#X vpath %.c ttc-lib/stm32/ ttc-lib/stm32f0/
                
MAIN_OBJS += system_stm32f0xx.o stm32f0_registers.o
#X MAIN_OBJS stm32_basic.o 

# define linker script to use
LDFLAGS += -Tconfigs/stm32f0xx.ld -mthumb -mcpu=cortex-m0

# Startup codes for STM32F0xx family
startup_stm32f0xx.o:		startup_stm32f0xx.s
MAIN_OBJS += startup_stm32f0xx.o

# All STM32L1xx have same memory start addresses
COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_START=0x20000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_ACCESS=rxw
COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_START=0x08000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_ACCESS=rx

#{ Define memory regions for each CPU Variant here to allow automatic linker script generation
# Note: This information is taken from cpu_stm32l1xx_types.h and must be kept in sync!

ifdef TTC_CPU_VARIANT_stm32f051r8t6
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32f051r8t6
  COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_SIZEK=8
  COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_SIZEK=64
endif

#}

#{ define uController to use (required to include stm32f0xx.h) ---------------
  ifdef uCONTROLLER
    ERROR: STM32F0XX - Only one uController may be selected!
  endif
  uCONTROLLER=STM32F0XX
  COMPILE_OPTS += -DuCONTROLLER=STM32F0XX
  COMPILE_OPTS += -DSTM32F0XX
#}END define uController

END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$0 2" "low-level cpu driver for stm32f0xx architecture" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature ""
if [ "\$?" != "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
    createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
  fi

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers require a certain file to exist inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.450_ethernet_ste101p -> allows activate.500_ethernet.sh to enable 450_ethernet_ste101p.
  # enableFeature NAME  # creates extensions.active/feature.NAME 
  
  # enableFeature 450_ethernet_ste101p
  enableFeature 450_cpu_cortexm0
  
  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  # activate corresponding std_peripheral_library
  #X activate.190_cpu_cortexm0.sh               QUIET "\$0"
  activate.250_STM32F0xx_StdPeriph_Driver.sh QUIET  "\$0"

  #}
  #{ create link to target script being required by openocd to initialize target cpu
    cd \$Dir_Configs
    createLink ../additionals/999_open_ocd/target/stm32f.cfg openocd_target.cfg
    createLink ../additionals/999_open_ocd/target/stm32f0xx_flash.cfg openocd_flash.cfg
    cd ..
  #}
  
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#  ACTIVATE_SECTION_E regressions do not provide features 
#{ ACTIVATE_SECTION_F call other activate-scripts

#activate.500_ttc_gpio.sh QUIET "\$0"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ optional: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#  ACTIVATE_SECTION_E examples do not provide features 
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$0"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                               #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------
