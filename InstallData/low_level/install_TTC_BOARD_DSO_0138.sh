#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a single Low-Level Driver
#  for board devices.
#
#  Created from template _install_TTC_DEVICE_ARCHITECTURE.sh revision 37 at 20180122 03:20:31 UTC
#
#  Supported Architecture: dso_0138
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$ScriptName - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="dso_0138"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards "extensions"
findFolderUpwards extensions; DirLL_Extensions="$MyStartDir/$ThisArchitecture/$FoundFolder"
findFolderUpwards scripts;    Start_Path="$MyStartDir/$ThisArchitecture/${FoundFolder}.."

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$ScriptName: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$ScriptName - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://thetoolchain.com/mirror/                                           S95417-AAA-iteadstudio.pdf S95417-AAA-iteadstudio.pdf Boards/DSO_0138/
  getDocumentation http://thetoolchain.com/mirror/                                           DSO138-master.zip          DSO138-sources.zip         Boards/DSO_0138/
  getDocumentation https://codeload.github.com/ardyesp/DLO-138/zip/                          master                     DLO138-sources.zip         Boards/DSO_0138/
  getDocumentation http://akizukidenshi.com/download/ds/jye/                                 UserManual_138_new_s.pdf   UserManual_138_new_s.pdf   Boards/DSO_0138/
  #getDocumentation http://thetoolchain.com/mirror/                                           ILI9341_DS_V1.11.pdf       ILI9341_DS_V1.11.pdf       Boards/DSO_0138/
  getDocumentation https://www.newhavendisplay.com/app_notes/                                ILI9341.pdf                ILI9341_DS_V1.11.pdf       Boards/DSO_0138/
  createLink DSO138-master.zip Sources_DSO0138.zip
  getDocumentation https://media.digikey.com/pdf/Data%20Sheets/ST%20Microelectronics%20PDFS/ STM32F103x8,B.pdf          STM32F103x8,B.pdf          Boards/DSO_0138/
  getDocumentation https://media.digikey.com/pdf/Data%20Sheets/ST%20Microelectronics%20PDFS/ STM32F103x8,B.pdf          STM32F103x8,B.pdf          uC/STM32F1xx/
  getDocumentation https://raw.githubusercontent.com/ardyesp/DLO-138/master/binaries/        DLO-138_encoder_1.0.bin    DLO-138_encoder_1.0.bin    Boards/DSO_0138/Firmwares/
  getDocumentation https://raw.githubusercontent.com/ardyesp/DLO-138/master/binaries/        DLO-138_switches_1.0.bin   DLO-138_switches_1.0.bin   Boards/DSO_0138/Firmwares/
  getDocumentation https://raw.githubusercontent.com/ardyesp/DLO-138/master/                 README.md                  DLO-138_README.md          Boards/DSO_0138/Firmwares/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then       #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir/$ThisArchitecture ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then       #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ low-level driver for board on dso_0138
  
    DriverName="${RANK_LOWLEVEL}_board_dso_0138"
    createExtensionSourcefileHead ${DriverName}  #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile


END_OF_SOURCEFILE
    createExtensionSourcefileTail ${DriverName} #}    
    createExtensionMakefileHead ${DriverName}    #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += board_dso_0138.o  # ttc-lib/board/board_dso_0138.c

COMPILE_OPTS += -DTTC_BOARD1=E_ttc_board_architecture_olimex_stm32_lcd

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f103rbt6 = 1

#{ TTC_SYSCLOCK_EXTERNAL - System Clock Setup (-> ttc_sysclock_types.h)

COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=8000000
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL=32768

#}
#{ BOARD_DSO_0138        - Board specific Pins

COMPILE_OPTS += -DBOARD_DSO_0138_PIN_TP11=E_ttc_gpio_pin_a4
COMPILE_OPTS += -DBOARD_DSO_0138_PIN_TP12=E_ttc_gpio_pin_a5
COMPILE_OPTS += -DBOARD_DSO_0138_PIN_TP13=E_ttc_gpio_pin_a6
COMPILE_OPTS += -DBOARD_DSO_0138_PIN_TESTSIG=E_ttc_gpio_pin_a7
COMPILE_OPTS += -DBOARD_DSO_0138_PIN_TRIG=E_ttc_gpio_pin_a8
COMPILE_OPTS += -DBOARD_DSO_0138_PIN_TL_PWM=E_ttc_gpio_pin_b8
COMPILE_OPTS += -DBOARD_DSO_0138_PIN_VGEN=E_ttc_gpio_pin_b9

#}
#{ TTC_LED               - LEDs and similar signal lights

COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_a15
COMPILE_OPTS += -DTTC_LED1_LOWACTIVE=1          # ==1: gpio pin must be pulled low (cleared) to activate LED; ==0: pushed high (set)

#}
#{ TTC_SWITCH            - Switch Buttons

COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_b15
COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=1
COMPILE_OPTS += -DTTC_SWITCH2=E_ttc_gpio_pin_b14
COMPILE_OPTS += -DTTC_SWITCH2_LOWACTIVE=1
COMPILE_OPTS += -DTTC_SWITCH3=E_ttc_gpio_pin_b13
COMPILE_OPTS += -DTTC_SWITCH3_LOWACTIVE=1
COMPILE_OPTS += -DTTC_SWITCH4=E_ttc_gpio_pin_b12
COMPILE_OPTS += -DTTC_SWITCH4_LOWACTIVE=1
#}
#{ TTC_ADC               - Analog Input Pins

COMPILE_OPTS += -DTTC_ADC1=E_ttc_gpio_pin_a0 # ADCIN
COMPILE_OPTS += -DTTC_ADC2=E_ttc_gpio_pin_a1 # VSENSE2
COMPILE_OPTS += -DTTC_ADC3=E_ttc_gpio_pin_a2 # VSENSE1

#}
#{ TTC_GPIO_PARALLEL     - Parallel Ports (8 or 16 consecutive pins in same bank)
#
# Every 8 available, consecutive bits in same GPIO bank can form an 8-bit wide port.
# A 16-bit wide port requires 16 available, consecutive bits (obviously).
# A pin is available if it not driven externally or used for something else.
# Check circuit of your board for details.

# 8-bit wide parallel port #1 starting at certain pin of a bank
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_BANK=TTC_GPIO_BANK_B
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_FIRSTPIN=0

# 16-bit wide parallel port #1 starting at pin 0 of a bank
# Note: Using 16-bit port on top over 8-bit port for some very fast transactions.
COMPILE_OPTS += -DTTC_GPIO_PARALLEL16_1_BANK=TTC_GPIO_BANK_B

#}parallel ports
#{ TTC_USART1            - Serial Port #1

COMPILE_OPTS += -DTTC_USART1=INDEX_USART1       # RS232 on 10-pin male header connector UEXT1 connected USART
COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_a9   
COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_a10

#}USART1
#{ TTC_GFX1              - LCD-Display 320x240x24

COMPILE_OPTS += -DTTC_GFX1=1
COMPILE_OPTS += -DTTC_GFX1_WIDTH=240
COMPILE_OPTS += -DTTC_GFX1_HEIGHT=320
COMPILE_OPTS += -DTTC_GFX1_DEPTH=24
COMPILE_OPTS += -DTTC_GFX1_DRIVER=tgd_ILI93xx    # one from ttc_gfx_types.h:e_ttc_gfx_driver

# define gpio pins connected to ili93xx gfx controller (->lcd_ili93xx.h)
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT=E_ttc_gpio_pin_c13       # LCD_nCS
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE=E_ttc_gpio_pin_b10      # LCD_nRD
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE=E_ttc_gpio_pin_c15     # LCD_nWR
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_REGISTER_SELECT=E_ttc_gpio_pin_c14  # LCD_nRS
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_NOT_RESET=E_ttc_gpio_pin_b11            # LCD_nRESET
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D00=E_ttc_gpio_pin_b0                    # data bus bit
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D01=E_ttc_gpio_pin_b1                    # data bus bit
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D02=E_ttc_gpio_pin_b2                    # data bus bit
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D03=E_ttc_gpio_pin_b3                    # data bus bit
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D04=E_ttc_gpio_pin_b4                    # data bus bit
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D05=E_ttc_gpio_pin_b5                    # data bus bit
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D06=E_ttc_gpio_pin_b6                    # data bus bit
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D07=E_ttc_gpio_pin_b7                    # data bus bit

#X  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D00=E_ttc_gpio_pin_b0
#X  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D01=E_ttc_gpio_pin_b1
#X  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D02=E_ttc_gpio_pin_b2
#X  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D03=E_ttc_gpio_pin_b3
#X  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D04=E_ttc_gpio_pin_b4
#X  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D05=E_ttc_gpio_pin_b5
#X  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D06=E_ttc_gpio_pin_b6
#X  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D07=E_ttc_gpio_pin_b7

# do not define unused pins (not connected on this board)
#COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_LCDLED=
#COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_NBL0=
#COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_NBL1=

#}

END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$ScriptName 2" "digital signal oscilloscope with stm31f103c8t6, lcd 320x240 (ILI9341), precision analog input, 4 buttons, uart, mini-usb" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature "${RANK_LOWLEVEL}_board_dso_0138" "$cASH_InfoText"
if [ "\$?" == "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  # board_dso_0138_prepare() typically ist called ttc_board_prepare() during preparation of all extensions.
  # It should not be required to activate source.${RANK_LOWLEVEL}_*.c here.

  #X if [ -e \$Dir_Extensions/source.${DriverName}.c ]; then
  #X   createLink \$Dir_Extensions/source.${DriverName}.c \$Dir_ExtensionsActive/ '' QUIET
  #X fi

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers activate only if a certain file exists inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.${RANK_LOWLEVEL}_ethernet_ste101p -> allows activate.500_ethernet.sh to enable its low-level driver ${RANK_LOWLEVEL}_ethernet_ste101p.
  # enableFeature NAME  # creates extensions.active/feature.NAME
  # See activate_project.sh of a fresh created project for a list of all available features! 
  
  enableFeature 450_gfx_ili93xx                             # driver for graphic controller ILI93xx
  enableFeature 450_cpu_stm32f1xx

  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  # activate.500_ttc_memory.sh QUIET "\$ScriptName"
  activate.500_ttc_usart.sh    QUIET "\$ScriptName"
  activate.500_ttc_gfx.sh      QUIET "\$ScriptName"
  activate.500_ttc_adc.sh      QUIET "\$ScriptName"
  
  #}
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# define devices to use in this regression
COMPILE_OPTS += -DTTC_BOARD1=E_ttc_board_architecture_dso_0138 # Line generated automatically. Remove if not wanted!

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$ScriptName 2" "PLACE YOUR INFO HERE FOR $ScriptName" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  if [ -e \$Dir_Extensions/source.${Name}.c ]; then
    createLink \$Dir_Extensions/source.${Name}.c \$Dir_ExtensionsActive/ '' QUIET
  fi

#}
#{ ACTIVATE_SECTION_E provide default features required to compile this example

  # Read Documentation/TheToolChain-Manual/chapter_Extensions.tml for a description of feature enabling.  

  # enableFeature 450_math_software_float   # enable math driver (single precision)
  # enableFeature 450_math_software_double  # enable math driver (double precision)

#}
#{ ACTIVATE_SECTION_F call other activate-scripts

#activate.500_ttc_gpio.sh QUIET "\$ScriptName"

#}
#{ ACTIVATE_SECTION_G copy source code into project folder if required

if [ ! -d regressions ]; then
  mkdir regressions
fi
ExampleFound=\`ls regressions/regression_${EXTENSION_NAME}_${ThisArchitecture}*\`
if [ "\$ExampleFound" == "" ]; then
  echo "copying missing regression_${EXTENSION_NAME}_${ThisArchitecture}*.."
  cp -v ~/Source/TheToolChain/Template/regressions/regression_${EXTENSION_NAME}_${ThisArchitecture}* regressions/
fi
#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ recommended: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}_${ThisArchitecture}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}_${ThisArchitecture}.h"
example_${EXTENSION_NAME}_${ThisArchitecture}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

# additional constant defines
#COMPILE_OPTS += -D

# define devices to use in this example
COMPILE_OPTS += -DTTC_BOARD1=E_ttc_board_architecture_dso_0138 # Line generated automatically. Remove if not wanted!

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}_${ThisArchitecture}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$ScriptName 2" "Usage example for extension ${EXTENSION_NAME}_${ThisArchitecture}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/source.${Name}.c ]; then
  createLink \$Dir_Extensions/source.${Name}.c \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#{ ACTIVATE_SECTION_E provide default features required to compile this example

  # Read Documentation/TheToolChain-Manual/chapter_Extensions.tml for a description of feature enabling.
  enableFeature $DriverName  # enable low-level driver to be tested

  # enableFeature 450_math_software_float   # enable math driver (single precision)
  # enableFeature 450_math_software_double  # enable math driver (double precision)

#}
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$ScriptName" # activate high-level driver

#}
#{ ACTIVATE_SECTION_G copy source code into project folder if required

if [ ! -d examples ]; then
  mkdir examples
fi
ExampleFound=\`ls examples/example_${EXTENSION_NAME}_${ThisArchitecture}*\`
if [ "\$ExampleFound" == "" ]; then
  echo "copying missing example_${EXTENSION_NAME}.."
  cp -v ~/Source/TheToolChain/Template/examples/example_${EXTENSION_NAME}_${ThisArchitecture}* examples/
fi
#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                               #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------
