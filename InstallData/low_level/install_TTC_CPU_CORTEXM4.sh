#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a single Low-Level Driver
#  for cpu devices.
#
#  Created from template _install_NN_TTC_DEVICE_ARCHITECTURE.sh revision 27 at 20150819 18:31:48 UTC
#
#  Supported Architecture: cortexm4
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$0 - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="cortexm4"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards "extensions"
DirLL_Extensions="$FoundFolder"

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  #XgetDocumentation http://www.arm.com/files/pdf/                                                                IntroToCortex-M4.pdf                 An_Introduction_to_the_ARM_Cortex-M4_Processor.pdf     uC/CortexM4
  getDocumentation https://static.docs.arm.com/100166_0001/00/                                                  arm_cortexm4_processor_trm_100166_0001_00_en.pdf ARM_Cortex-M4_Technical_Reference_Manual.pdf uC/CortexM4
  getDocumentation http://www.st.com/content/ccc/resource/technical/document/programming_manual/6c/3a/cb/e7/e4/ea/44/9b/DM00046982.pdf/files/DM00046982.pdf/jcr:content/translations/ en.DM00046982.pdf PM0214_Cortex-M4_Programming_Manual.pdf uC/CortexM4
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/programming_manual/ CD00228163.pdf                       STM32_PM0056_Cortex-M4_Programming_Manual.pdf          uC/CortexM4
  getDocumentation http://www.peter-cockerell.net/aalp/resources/pdf/                                           all.pdf                              ARM-Assembly_Language_Programming.pdf                  uC/ARM
  getDocumentation http://www.state-machine.com/arm/                                                            Building_bare-metal_ARM_with_GNU.pdf Building_Bare-Metal_ARM-Systems_with_GNU.pdf           uC/CortexM4
  getDocumentation http://infocenter.arm.com/help/topic/com.arm.doc.dui0552a/                                   DUI0552A_cortex_m3_dgug.pdf          Cortex-M4_Devices_Generic_User_Guide.pdf               uC/CortexM4

  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then       #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir/$ThisArchitecture ..."
    Install_Path=`pwd`

    Error=""
    
    Archive="CMSIS-SP-00300-r3p1-00rel0.zip"
    #getFile https://silver.arm.com/download/Development_Tools/Keil/Keil:_generic/CMSIS-SP-00300-r3p1-00rel0/ $Archive $Archive
    getFile http://thetoolchain.com/mirror/ $Archive $Archive
    if [ "$Error" == "" ]; then
      testZIP $Archive
    fi
    if [ "$Error" == "" ]; then
      unZIP "foo" $Archive
    else
      Error="Corrupt archive $Archive!"
      mv -v $Archive ${Archive}_broken
    fi
    if [ ! -d CMSIS/ ]; then
      Error=" Cannot download CMSIS library! (missing $Archive)"
    fi
  
    if [ "$Error" == "" ]; then
      touch OK.Install
    else
      echo "$0 - ERROR: $Error!"
      exit 10
    fi
    
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then       #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ low-level driver for cpu on cortexm4
  
    DriverName="450_cpu_cortexm4"
    
    if [ "1" == "0" ]; then # { CMSIS (currently not usable due to missing C-sources in downloaded archive)

      Name="${Install_Dir}_cmsis"  
      # (addLine) 
      # create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
      #
      # Activate line below and change SUBFOLDER to your needs
      addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; createLink \$Source/TheToolChain/InstallData/${Install_Dir}/CMSIS $Name"

      createExtensionSourcefileHead ${Name}    #{ (create initializer code)
      
      # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
      # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
      #
      cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

//  #include "YOUR_SOURCE_HERE.h"
//  YOUR_START_HERE();

END_OF_SOURCEFILE
      createExtensionSourcefileTail ${Name} #}    
      createExtensionMakefileHead ${Name}      #{ (create makefile)
      cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
INCLUDE_DIRS += -I additionals/$Name/Include/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
#MAIN_OBJS += 

END_OF_MAKEFILE
      createExtensionMakefileTail ${Name} #}
      createActivateScriptHead $Name ${Dir_Extensions} "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ (create activate script)
      cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
#createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
#X activate.500_ttc_memory.sh QUIET "\$0"


END_OF_ACTIVATE
      createActivateScriptTail $Name ${Dir_Extensions}
      #}
    fi #}

    createExtensionSourcefileHead ${DriverName}  #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile


END_OF_SOURCEFILE
    createExtensionSourcefileTail ${DriverName} #}    
    createExtensionMakefileHead ${DriverName}    #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += cpu_cortexm4.o  # ttc-lib/cpu/cpu_cortexm4.c

COMPILE_OPTS += -DEXTENSION_${Name}=1

ARM_CPU     =arm7tdmi
TARGET      =-mcpu=cortex-m3 -mthumb
TARGET_ARCHITECTURE_CM4=1
COMPILE_OPTS += -DTARGET_ARCHITECTURE_CM4
COMPILE_OPTS += -DTARGET_DATA_WIDTH=32

# define CortexM4 memory regions

COMPILE_OPTS += -DTTC_MEMORY_REGION_CODE_START=0x60000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_CODE_SIZEK=1048576
COMPILE_OPTS += -DTTC_MEMORY_REGION_CODE_ACCESS=rwx

COMPILE_OPTS += -DTTC_MEMORY_REGION_SRAM_START=0x60000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_SRAM_SIZEK=1048576
COMPILE_OPTS += -DTTC_MEMORY_REGION_SRAM_ACCESS=rwx

COMPILE_OPTS += -DTTC_MEMORY_REGION_SRAM_BITBAND_START=0x22000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_SRAM_BITBAND_SIZEK=32768
COMPILE_OPTS += -DTTC_MEMORY_REGION_SRAM_BITBAND_ACCESS=rw

COMPILE_OPTS += -DTTC_MEMORY_REGION_PERIPHERAL_START=0x40000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_PERIPHERAL_SIZEK=1024
COMPILE_OPTS += -DTTC_MEMORY_REGION_PERIPHERAL_ACCESS=rw

COMPILE_OPTS += -DTTC_MEMORY_REGION_PERIPHERAL_BITBAND_START=0x42000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_PERIPHERAL_BITBAND_SIZEK=32768
COMPILE_OPTS += -DTTC_MEMORY_REGION_PERIPHERAL_BITBAND_ACCESS=rw

COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_EXTERNAL_START=0x60000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_EXTERNAL_SIZEK=1048576
COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_EXTERNAL_ACCESS=rwx

COMPILE_OPTS += -DTTC_MEMORY_REGION_DEVICE_EXTERNAL_START=0xa0000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_DEVICE_EXTERNAL_SIZEK=1048576
COMPILE_OPTS += -DTTC_MEMORY_REGION_DEVICE_EXTERNAL_ACCESS=rw

COMPILE_OPTS += -DTTC_MEMORY_REGION_PRIVATE_PERIPHERAL_START=0xe0000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_PRIVATE_PERIPHERAL_SIZEK=1024
COMPILE_OPTS += -DTTC_MEMORY_REGION_PRIVATE_PERIPHERAL_ACCESS=rw

COMPILE_OPTS += -DTTC_MEMORY_REGION_VENDOR_MEMORY_START=0xe0100000
COMPILE_OPTS += -DTTC_MEMORY_REGION_VENDOR_MEMORY_SIZEK=523264
COMPILE_OPTS += -DTTC_MEMORY_REGION_VENDOR_MEMORY_ACCESS=rw

# architecture specific support code
INCLUDE_DIRS += -Ittc-lib/cm3
vpath %.c ttc-lib/cm3

# Basic Cortex M4 support library
INCLUDE_DIRS += -Iadditionals/270_CPAL_CMSIS/CM4/CoreSupport/
vpath %.c additionals/270_CPAL_CMSIS/CM4/CoreSupport/

MAIN_OBJS += core_cm3.o

# suppress warnings about invalid wchar sizes during linker stage
# (not supported by gcc-arm v4.7) LDFLAGS += --no-wchar-size-warning

END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$0 2" "low-level cpu driver for cortexm4 architecture" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature ""
if [ "\$?" != "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
    createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
  fi

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers require a certain file to exist inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.450_ethernet_ste101p -> allows activate.500_ethernet.sh to enable 450_ethernet_ste101p.
  # enableFeature NAME  # creates extensions.active/feature.NAME 
  
  # enableFeature 450_ethernet_ste101p
  
  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  # activate.500_ttc_memory.sh QUIET "\$0"
  
  #}

  cd _
  createLink gdb_cortexm4.sh gdb.sh
  cd ..

fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#  ACTIVATE_SECTION_E regressions do not provide features 
#{ ACTIVATE_SECTION_F call other activate-scripts

#activate.500_ttc_gpio.sh QUIET "\$0"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ optional: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#  ACTIVATE_SECTION_E examples do not provide features 
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$0"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                               #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------
